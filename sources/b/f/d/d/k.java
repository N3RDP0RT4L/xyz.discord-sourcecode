package b.f.d.d;

import com.facebook.common.internal.Supplier;
/* compiled from: Suppliers.java */
/* loaded from: classes2.dex */
public final class k implements Supplier<T> {
    public final /* synthetic */ Object a;

    public k(Object obj) {
        this.a = obj;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [T, java.lang.Object] */
    @Override // com.facebook.common.internal.Supplier
    public T get() {
        return this.a;
    }
}
