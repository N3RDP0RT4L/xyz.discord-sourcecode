package b.f.d.d;
/* compiled from: Throwables.java */
/* loaded from: classes2.dex */
public final class m {
    public static void a(Throwable th) {
        if (Error.class.isInstance(th)) {
            throw ((Throwable) Error.class.cast(th));
        } else if (RuntimeException.class.isInstance(th)) {
            throw ((Throwable) RuntimeException.class.cast(th));
        }
    }
}
