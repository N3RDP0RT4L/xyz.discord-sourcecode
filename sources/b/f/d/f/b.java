package b.f.d.f;

import android.webkit.MimeTypeMap;
import b.f.d.d.f;
import java.util.Map;
/* compiled from: MimeTypeMapWrapper.java */
/* loaded from: classes2.dex */
public class b {
    public static final MimeTypeMap a = MimeTypeMap.getSingleton();

    /* renamed from: b  reason: collision with root package name */
    public static final Map<String, String> f467b = f.of("image/heif", "heif", "image/heic", "heic");
    public static final Map<String, String> c = f.of("heif", "image/heif", "heic", "image/heic");
}
