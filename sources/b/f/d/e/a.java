package b.f.d.e;

import android.util.Log;
import java.io.PrintWriter;
import java.io.StringWriter;
/* compiled from: FLog.java */
/* loaded from: classes2.dex */
public class a {
    public static final /* synthetic */ int a = 0;

    public static void a(Class<?> cls, String str) {
        String simpleName = cls.getSimpleName();
        Log.println(6, "unknown:" + simpleName, str);
    }

    public static void b(Class<?> cls, String str, Throwable th) {
        String w = b.d.b.a.a.w("unknown", ":", cls.getSimpleName());
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append('\n');
        StringWriter stringWriter = new StringWriter();
        th.printStackTrace(new PrintWriter(stringWriter));
        sb.append(stringWriter.toString());
        Log.println(6, w, sb.toString());
    }

    public static void c(Class<?> cls, String str, Object... objArr) {
        String simpleName = cls.getSimpleName();
        String g = g(str, objArr);
        Log.println(6, "unknown:" + simpleName, g);
    }

    public static void d(Class<?> cls, Throwable th, String str, Object... objArr) {
        String simpleName = cls.getSimpleName();
        String g = g(str, objArr);
        String w = b.d.b.a.a.w("unknown", ":", simpleName);
        StringBuilder sb = new StringBuilder();
        sb.append(g);
        sb.append('\n');
        StringWriter stringWriter = new StringWriter();
        th.printStackTrace(new PrintWriter(stringWriter));
        sb.append(stringWriter.toString());
        Log.println(6, w, sb.toString());
    }

    public static void e(String str, String str2) {
        Log.println(6, "unknown:" + str, str2);
    }

    public static void f(String str, String str2, Throwable th) {
        String str3;
        String w = b.d.b.a.a.w("unknown", ":", str);
        StringBuilder sb = new StringBuilder();
        sb.append(str2);
        sb.append('\n');
        if (th == null) {
            str3 = "";
        } else {
            StringWriter stringWriter = new StringWriter();
            th.printStackTrace(new PrintWriter(stringWriter));
            str3 = stringWriter.toString();
        }
        sb.append(str3);
        Log.println(6, w, sb.toString());
    }

    public static String g(String str, Object... objArr) {
        return String.format(null, str, objArr);
    }

    public static boolean h(int i) {
        return 5 <= i;
    }

    public static void i(Class<?> cls, String str) {
        if (0 != 0) {
            String simpleName = cls.getSimpleName();
            Log.println(2, "unknown:" + simpleName, str);
        }
    }

    public static void j(Class<?> cls, String str, Object obj, Object obj2, Object obj3) {
        if (h(2)) {
            i(cls, g(str, obj, obj2, obj3));
        }
    }

    public static void k(Class<?> cls, String str) {
        String simpleName = cls.getSimpleName();
        Log.println(5, "unknown:" + simpleName, str);
    }

    public static void l(Class<?> cls, String str, Throwable th) {
        String str2;
        if (1 != 0) {
            String w = b.d.b.a.a.w("unknown", ":", cls.getSimpleName());
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append('\n');
            if (th == null) {
                str2 = "";
            } else {
                StringWriter stringWriter = new StringWriter();
                th.printStackTrace(new PrintWriter(stringWriter));
                str2 = stringWriter.toString();
            }
            sb.append(str2);
            Log.println(5, w, sb.toString());
        }
    }

    public static void m(Class<?> cls, String str, Object... objArr) {
        String simpleName = cls.getSimpleName();
        String g = g(str, objArr);
        Log.println(5, "unknown:" + simpleName, g);
    }

    public static void n(Class<?> cls, Throwable th, String str, Object... objArr) {
        if (h(5)) {
            l(cls, g(str, objArr), th);
        }
    }

    public static void o(String str, String str2, Object... objArr) {
        String g = g(str2, objArr);
        Log.println(5, "unknown:" + str, g);
    }

    public static void p(String str, String str2, Object... objArr) {
        String g = g(str2, objArr);
        Log.println(6, "unknown:" + str, g);
    }
}
