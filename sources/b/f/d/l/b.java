package b.f.d.l;

import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import com.adjust.sdk.Constants;
/* compiled from: UriUtil.java */
/* loaded from: classes2.dex */
public class b {
    public static final Uri a = Uri.withAppendedPath(ContactsContract.AUTHORITY_URI, "display_photo");

    public static String a(Uri uri) {
        if (uri == null) {
            return null;
        }
        return uri.getScheme();
    }

    public static boolean b(Uri uri) {
        String uri2 = uri.toString();
        return uri2.startsWith(MediaStore.Images.Media.EXTERNAL_CONTENT_URI.toString()) || uri2.startsWith(MediaStore.Images.Media.INTERNAL_CONTENT_URI.toString());
    }

    public static boolean c(Uri uri) {
        return "content".equals(a(uri));
    }

    public static boolean d(Uri uri) {
        return "file".equals(a(uri));
    }

    public static boolean e(Uri uri) {
        String a2 = a(uri);
        return Constants.SCHEME.equals(a2) || "http".equals(a2);
    }
}
