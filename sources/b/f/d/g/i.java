package b.f.d.g;

import b.f.d.d.m;
import java.io.IOException;
import java.io.OutputStream;
/* compiled from: PooledByteBufferOutputStream.java */
/* loaded from: classes2.dex */
public abstract class i extends OutputStream {
    @Override // java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        try {
            super.close();
        } catch (IOException e) {
            m.a(e);
            throw new RuntimeException(e);
        }
    }
}
