package b.f.d.g;

import b.c.a.a0.d;
import b.f.d.e.a;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
/* compiled from: PooledByteArrayBufferedInputStream.java */
/* loaded from: classes2.dex */
public class f extends InputStream {
    public final InputStream j;
    public final byte[] k;
    public final b.f.d.h.f<byte[]> l;
    public int m = 0;
    public int n = 0;
    public boolean o = false;

    public f(InputStream inputStream, byte[] bArr, b.f.d.h.f<byte[]> fVar) {
        this.j = inputStream;
        Objects.requireNonNull(bArr);
        this.k = bArr;
        Objects.requireNonNull(fVar);
        this.l = fVar;
    }

    public final boolean a() throws IOException {
        if (this.n < this.m) {
            return true;
        }
        int read = this.j.read(this.k);
        if (read <= 0) {
            return false;
        }
        this.m = read;
        this.n = 0;
        return true;
    }

    @Override // java.io.InputStream
    public int available() throws IOException {
        d.B(this.n <= this.m);
        b();
        return this.j.available() + (this.m - this.n);
    }

    public final void b() throws IOException {
        if (this.o) {
            throw new IOException("stream already closed");
        }
    }

    @Override // java.io.InputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        if (!this.o) {
            this.o = true;
            this.l.release(this.k);
            super.close();
        }
    }

    public void finalize() throws Throwable {
        if (!this.o) {
            a.e("PooledByteInputStream", "Finalized without closing");
            close();
        }
        super.finalize();
    }

    @Override // java.io.InputStream
    public int read() throws IOException {
        d.B(this.n <= this.m);
        b();
        if (!a()) {
            return -1;
        }
        byte[] bArr = this.k;
        int i = this.n;
        this.n = i + 1;
        return bArr[i] & 255;
    }

    @Override // java.io.InputStream
    public long skip(long j) throws IOException {
        d.B(this.n <= this.m);
        b();
        int i = this.m;
        int i2 = this.n;
        long j2 = i - i2;
        if (j2 >= j) {
            this.n = (int) (i2 + j);
            return j;
        }
        this.n = i;
        return this.j.skip(j - j2) + j2;
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) throws IOException {
        d.B(this.n <= this.m);
        b();
        if (!a()) {
            return -1;
        }
        int min = Math.min(this.m - this.n, i2);
        System.arraycopy(this.k, this.n, bArr, i, min);
        this.n += min;
        return min;
    }
}
