package b.f.d.g;

import androidx.annotation.VisibleForTesting;
import b.c.a.a0.d;
import b.d.b.a.a;
import com.facebook.common.memory.PooledByteBuffer;
import java.io.InputStream;
/* compiled from: PooledByteBufferInputStream.java */
/* loaded from: classes2.dex */
public class h extends InputStream {
    @VisibleForTesting
    public final PooledByteBuffer j;
    @VisibleForTesting
    public int k = 0;
    @VisibleForTesting
    public int l = 0;

    public h(PooledByteBuffer pooledByteBuffer) {
        d.i(Boolean.valueOf(!pooledByteBuffer.isClosed()));
        this.j = pooledByteBuffer;
    }

    @Override // java.io.InputStream
    public int available() {
        return this.j.size() - this.k;
    }

    @Override // java.io.InputStream
    public void mark(int i) {
        this.l = this.k;
    }

    @Override // java.io.InputStream
    public boolean markSupported() {
        return true;
    }

    @Override // java.io.InputStream
    public int read() {
        if (available() <= 0) {
            return -1;
        }
        PooledByteBuffer pooledByteBuffer = this.j;
        int i = this.k;
        this.k = i + 1;
        return pooledByteBuffer.h(i) & 255;
    }

    @Override // java.io.InputStream
    public void reset() {
        this.k = this.l;
    }

    @Override // java.io.InputStream
    public long skip(long j) {
        d.i(Boolean.valueOf(j >= 0));
        int min = Math.min((int) j, available());
        this.k += min;
        return min;
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) {
        if (i < 0 || i2 < 0 || i + i2 > bArr.length) {
            StringBuilder R = a.R("length=");
            R.append(bArr.length);
            R.append("; regionStart=");
            R.append(i);
            R.append("; regionLength=");
            R.append(i2);
            throw new ArrayIndexOutOfBoundsException(R.toString());
        }
        int available = available();
        if (available <= 0) {
            return -1;
        }
        if (i2 <= 0) {
            return 0;
        }
        int min = Math.min(available, i2);
        this.j.i(this.k, bArr, i, min);
        this.k += min;
        return min;
    }
}
