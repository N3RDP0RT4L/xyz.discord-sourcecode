package b.f.d.g;

import b.f.d.h.f;
/* compiled from: Pool.java */
/* loaded from: classes2.dex */
public interface e<V> extends f<V>, b {
    V get(int i);

    @Override // b.f.d.h.f
    void release(V v);
}
