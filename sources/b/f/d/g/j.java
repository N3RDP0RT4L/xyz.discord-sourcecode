package b.f.d.g;

import b.c.a.a0.d;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
/* compiled from: PooledByteStreams.java */
/* loaded from: classes2.dex */
public class j {
    public final int a = 16384;

    /* renamed from: b  reason: collision with root package name */
    public final a f468b;

    public j(a aVar) {
        d.i(true);
        this.f468b = aVar;
    }

    public long a(InputStream inputStream, OutputStream outputStream) throws IOException {
        byte[] bArr = this.f468b.get(this.a);
        long j = 0;
        while (true) {
            try {
                int read = inputStream.read(bArr, 0, this.a);
                if (read == -1) {
                    return j;
                }
                outputStream.write(bArr, 0, read);
                j += read;
            } finally {
                this.f468b.release(bArr);
            }
        }
    }
}
