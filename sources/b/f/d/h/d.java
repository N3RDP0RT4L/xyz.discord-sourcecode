package b.f.d.h;

import com.facebook.common.references.CloseableReference;
/* compiled from: NoOpCloseableReference.java */
/* loaded from: classes2.dex */
public class d<T> extends CloseableReference<T> {
    public d(T t, f<T> fVar, CloseableReference.c cVar, Throwable th) {
        super(t, fVar, cVar, th);
    }

    @Override // com.facebook.common.references.CloseableReference
    public CloseableReference<T> b() {
        return this;
    }

    @Override // com.facebook.common.references.CloseableReference
    public Object clone() throws CloneNotSupportedException {
        return this;
    }

    @Override // com.facebook.common.references.CloseableReference, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
    }
}
