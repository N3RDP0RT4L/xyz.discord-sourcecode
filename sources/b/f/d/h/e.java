package b.f.d.h;

import b.c.a.a0.d;
import com.facebook.common.references.CloseableReference;
import com.facebook.common.references.SharedReference;
/* compiled from: RefCountCloseableReference.java */
/* loaded from: classes2.dex */
public class e<T> extends CloseableReference<T> {
    public e(SharedReference<T> sharedReference, CloseableReference.c cVar, Throwable th) {
        super(sharedReference, cVar, th);
    }

    @Override // com.facebook.common.references.CloseableReference
    /* renamed from: b */
    public CloseableReference<T> clone() {
        d.B(x());
        return new e(this.o, this.p, this.q);
    }

    public e(T t, f<T> fVar, CloseableReference.c cVar, Throwable th) {
        super(t, fVar, cVar, th);
    }
}
