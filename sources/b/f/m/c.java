package b.f.m;

import android.os.StrictMode;
import android.os.Trace;
import android.util.Log;
import b.c.a.a0.d;
import b.d.b.a.a;
import com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel;
import com.facebook.soloader.Api18TraceUtils;
import com.facebook.soloader.SoLoader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.channels.ClosedByInterruptException;
import java.util.Arrays;
/* compiled from: DirectorySoSource.java */
/* loaded from: classes2.dex */
public class c extends l {
    public final File a;

    /* renamed from: b  reason: collision with root package name */
    public final int f649b;

    public c(File file, int i) {
        this.a = file;
        this.f649b = i;
    }

    @Override // b.f.m.l
    public int a(String str, int i, StrictMode.ThreadPolicy threadPolicy) throws IOException {
        return c(str, i, this.a, threadPolicy);
    }

    public int c(String str, int i, File file, StrictMode.ThreadPolicy threadPolicy) throws IOException {
        File file2 = new File(file, str);
        if (!file2.exists()) {
            StringBuilder V = a.V(str, " not found on ");
            V.append(file.getCanonicalPath());
            Log.d("SoLoader", V.toString());
            return 0;
        }
        StringBuilder V2 = a.V(str, " found on ");
        V2.append(file.getCanonicalPath());
        Log.d("SoLoader", V2.toString());
        if ((i & 1) == 0 || (this.f649b & 2) == 0) {
            if ((this.f649b & 1) != 0) {
                if (SoLoader.a) {
                    Api18TraceUtils.a("SoLoader.getElfDependencies[", file2.getName(), "]");
                }
                int i2 = 0;
                while (true) {
                    try {
                        FileInputStream fileInputStream = new FileInputStream(file2);
                        try {
                            String[] j02 = d.j0(fileInputStream.getChannel());
                            fileInputStream.close();
                            StringBuilder R = a.R("Loading lib dependencies: ");
                            R.append(Arrays.toString(j02));
                            Log.d("SoLoader", R.toString());
                            for (String str2 : j02) {
                                if (!str2.startsWith(AutocompleteViewModel.COMMAND_DISCOVER_TOKEN)) {
                                    SoLoader.d(str2, null, null, i | 1, threadPolicy);
                                }
                            }
                        } catch (ClosedByInterruptException e) {
                            i2++;
                            if (i2 <= 3) {
                                Thread.interrupted();
                                Log.e("MinElf", "retrying extract_DT_NEEDED due to ClosedByInterruptException", e);
                                fileInputStream.close();
                            } else {
                                throw e;
                            }
                        }
                    } finally {
                        if (SoLoader.a) {
                            Trace.endSection();
                        }
                    }
                }
            } else {
                Log.d("SoLoader", "Not resolving dependencies for " + str);
            }
            try {
                ((SoLoader.a) SoLoader.f2878b).b(file2.getAbsolutePath(), i);
                return 1;
            } catch (UnsatisfiedLinkError e2) {
                if (e2.getMessage().contains("bad ELF magic")) {
                    Log.d("SoLoader", "Corrupted lib file detected");
                    return 3;
                }
                throw e2;
            }
        } else {
            Log.d("SoLoader", str + " loaded implicitly");
            return 2;
        }
    }

    @Override // b.f.m.l
    public String toString() {
        String str;
        try {
            str = String.valueOf(this.a.getCanonicalPath());
        } catch (IOException unused) {
            str = this.a.getName();
        }
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getName());
        sb.append("[root = ");
        sb.append(str);
        sb.append(" flags = ");
        return a.z(sb, this.f649b, ']');
    }
}
