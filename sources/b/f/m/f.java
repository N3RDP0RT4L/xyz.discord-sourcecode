package b.f.m;

import android.content.Context;
import b.f.m.m;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
/* compiled from: ExtractFromZipSoSource.java */
/* loaded from: classes2.dex */
public class f extends m {
    public final File f;
    public final String g;

    /* compiled from: ExtractFromZipSoSource.java */
    /* loaded from: classes2.dex */
    public static final class b extends m.b implements Comparable {
        public final ZipEntry l;
        public final int m;

        public b(String str, ZipEntry zipEntry, int i) {
            super(str, String.format("pseudo-zip-hash-1-%s-%s-%s-%s", zipEntry.getName(), Long.valueOf(zipEntry.getSize()), Long.valueOf(zipEntry.getCompressedSize()), Long.valueOf(zipEntry.getCrc())));
            this.l = zipEntry;
            this.m = i;
        }

        @Override // java.lang.Comparable
        public int compareTo(Object obj) {
            return this.j.compareTo(((b) obj).j);
        }
    }

    /* compiled from: ExtractFromZipSoSource.java */
    /* loaded from: classes2.dex */
    public class c extends m.f {
        public b[] j;
        public final ZipFile k;
        public final m l;

        /* compiled from: ExtractFromZipSoSource.java */
        /* loaded from: classes2.dex */
        public final class a extends m.e {
            public int j;

            public a(a aVar) {
            }

            @Override // b.f.m.m.e
            public boolean a() {
                c.this.c();
                return this.j < c.this.j.length;
            }

            @Override // b.f.m.m.e
            public m.d b() throws IOException {
                c.this.c();
                c cVar = c.this;
                b[] bVarArr = cVar.j;
                int i = this.j;
                this.j = i + 1;
                b bVar = bVarArr[i];
                InputStream inputStream = cVar.k.getInputStream(bVar.l);
                try {
                    return new m.d(bVar, inputStream);
                } catch (Throwable th) {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    throw th;
                }
            }
        }

        public c(m mVar) throws IOException {
            this.k = new ZipFile(f.this.f);
            this.l = mVar;
        }

        @Override // b.f.m.m.f
        public final m.c a() throws IOException {
            return new m.c(c());
        }

        @Override // b.f.m.m.f
        public final m.e b() throws IOException {
            return new a(null);
        }

        /* JADX WARN: Removed duplicated region for block: B:43:0x0120  */
        /* JADX WARN: Removed duplicated region for block: B:44:0x0123  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public final b.f.m.f.b[] c() {
            /*
                Method dump skipped, instructions count: 320
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: b.f.m.f.c.c():b.f.m.f$b[]");
        }

        @Override // b.f.m.m.f, java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
            this.k.close();
        }
    }

    public f(Context context, String str, File file, String str2) {
        super(context, str);
        this.f = file;
        this.g = str2;
    }
}
