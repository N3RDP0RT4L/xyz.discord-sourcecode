package b.f.m;

import android.util.Log;
import b.f.m.n.b;
import com.facebook.soloader.SoLoader;
import java.io.IOException;
import java.util.concurrent.locks.ReentrantReadWriteLock;
/* compiled from: NativeLoaderToSoLoaderDelegate.java */
/* loaded from: classes2.dex */
public class j implements b {
    /* JADX WARN: Finally extract failed */
    @Override // b.f.m.n.b
    public boolean a(String str, int i) {
        boolean z2;
        boolean contains;
        int i2 = ((i & 1) != 0 ? 16 : 0) | 0;
        ReentrantReadWriteLock reentrantReadWriteLock = SoLoader.c;
        reentrantReadWriteLock.readLock().lock();
        try {
            if (SoLoader.d == null) {
                if ("http://www.android.com/".equals(System.getProperty("java.vendor.url"))) {
                    reentrantReadWriteLock.readLock().lock();
                    boolean z3 = SoLoader.d != null;
                    reentrantReadWriteLock.readLock().unlock();
                    if (!z3) {
                        throw new IllegalStateException("SoLoader.init() not yet called");
                    }
                } else {
                    synchronized (SoLoader.class) {
                        contains = true ^ SoLoader.h.contains(str);
                        if (contains) {
                            System.loadLibrary(str);
                        }
                    }
                    reentrantReadWriteLock.readLock().unlock();
                    return contains;
                }
            }
            reentrantReadWriteLock.readLock().unlock();
            boolean z4 = SoLoader.l;
            String mapLibraryName = System.mapLibraryName(str);
            boolean z5 = false;
            do {
                try {
                    z5 = SoLoader.d(mapLibraryName, str, null, i2, null);
                    z2 = false;
                    continue;
                } catch (UnsatisfiedLinkError e) {
                    int i3 = SoLoader.e;
                    SoLoader.c.writeLock().lock();
                    try {
                        try {
                            if (SoLoader.g == null || !SoLoader.g.c()) {
                                z2 = false;
                            } else {
                                Log.w("SoLoader", "sApplicationSoSource updated during load: " + mapLibraryName + ", attempting load again.");
                                SoLoader.e = SoLoader.e + 1;
                                z2 = true;
                            }
                            SoLoader.c.writeLock().unlock();
                            if (SoLoader.e == i3) {
                                throw e;
                            }
                        } catch (IOException e2) {
                            throw new RuntimeException(e2);
                        }
                    } catch (Throwable th) {
                        SoLoader.c.writeLock().unlock();
                        throw th;
                    }
                }
            } while (z2);
            return z5;
        } catch (Throwable th2) {
            SoLoader.c.readLock().unlock();
            throw th2;
        }
    }
}
