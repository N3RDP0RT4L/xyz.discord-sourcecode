package b.f.m;

import android.content.Context;
import b.f.m.f;
import b.f.m.m;
import java.io.File;
import java.io.IOException;
/* compiled from: ApkSoSource.java */
/* loaded from: classes2.dex */
public class a extends f {
    public final int h;

    /* compiled from: ApkSoSource.java */
    /* renamed from: b.f.m.a$a  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class C0077a extends f.c {
        public File n;
        public final int o;

        public C0077a(f fVar) throws IOException {
            super(fVar);
            this.n = new File(a.this.c.getApplicationInfo().nativeLibraryDir);
            this.o = a.this.h;
        }
    }

    public a(Context context, File file, String str, int i) {
        super(context, str, file, "^lib/([^/]+)/([^/]+\\.so)$");
        this.h = i;
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x003a A[Catch: all -> 0x008d, TRY_LEAVE, TryCatch #1 {all -> 0x008d, blocks: (B:3:0x000b, B:6:0x0025, B:8:0x0031, B:10:0x003a, B:13:0x0045, B:15:0x004f, B:18:0x005a, B:20:0x0069, B:23:0x0074), top: B:32:0x000b }] */
    /* JADX WARN: Removed duplicated region for block: B:13:0x0045 A[Catch: all -> 0x008d, TRY_ENTER, TryCatch #1 {all -> 0x008d, blocks: (B:3:0x000b, B:6:0x0025, B:8:0x0031, B:10:0x003a, B:13:0x0045, B:15:0x004f, B:18:0x005a, B:20:0x0069, B:23:0x0074), top: B:32:0x000b }] */
    @Override // b.f.m.m
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public byte[] g() throws java.io.IOException {
        /*
            r5 = this;
            java.io.File r0 = r5.f
            java.io.File r0 = r0.getCanonicalFile()
            android.os.Parcel r1 = android.os.Parcel.obtain()
            r2 = 2
            r1.writeByte(r2)     // Catch: java.lang.Throwable -> L8d
            java.lang.String r3 = r0.getPath()     // Catch: java.lang.Throwable -> L8d
            r1.writeString(r3)     // Catch: java.lang.Throwable -> L8d
            long r3 = r0.lastModified()     // Catch: java.lang.Throwable -> L8d
            r1.writeLong(r3)     // Catch: java.lang.Throwable -> L8d
            android.content.Context r0 = r5.c     // Catch: java.lang.Throwable -> L8d
            android.content.pm.PackageManager r3 = r0.getPackageManager()     // Catch: java.lang.Throwable -> L8d
            r4 = 0
            if (r3 == 0) goto L30
            java.lang.String r0 = r0.getPackageName()     // Catch: java.lang.Throwable -> L30 java.lang.Throwable -> L8d
            android.content.pm.PackageInfo r0 = r3.getPackageInfo(r0, r4)     // Catch: java.lang.Throwable -> L30 java.lang.Throwable -> L8d
            int r0 = r0.versionCode     // Catch: java.lang.Throwable -> L30 java.lang.Throwable -> L8d
            goto L31
        L30:
            r0 = 0
        L31:
            r1.writeInt(r0)     // Catch: java.lang.Throwable -> L8d
            int r0 = r5.h     // Catch: java.lang.Throwable -> L8d
            r3 = 1
            r0 = r0 & r3
            if (r0 != 0) goto L45
            r1.writeByte(r4)     // Catch: java.lang.Throwable -> L8d
            byte[] r0 = r1.marshall()     // Catch: java.lang.Throwable -> L8d
            r1.recycle()
            return r0
        L45:
            android.content.Context r0 = r5.c     // Catch: java.lang.Throwable -> L8d
            android.content.pm.ApplicationInfo r0 = r0.getApplicationInfo()     // Catch: java.lang.Throwable -> L8d
            java.lang.String r0 = r0.nativeLibraryDir     // Catch: java.lang.Throwable -> L8d
            if (r0 != 0) goto L5a
            r1.writeByte(r3)     // Catch: java.lang.Throwable -> L8d
            byte[] r0 = r1.marshall()     // Catch: java.lang.Throwable -> L8d
            r1.recycle()
            return r0
        L5a:
            java.io.File r4 = new java.io.File     // Catch: java.lang.Throwable -> L8d
            r4.<init>(r0)     // Catch: java.lang.Throwable -> L8d
            java.io.File r0 = r4.getCanonicalFile()     // Catch: java.lang.Throwable -> L8d
            boolean r4 = r0.exists()     // Catch: java.lang.Throwable -> L8d
            if (r4 != 0) goto L74
            r1.writeByte(r3)     // Catch: java.lang.Throwable -> L8d
            byte[] r0 = r1.marshall()     // Catch: java.lang.Throwable -> L8d
            r1.recycle()
            return r0
        L74:
            r1.writeByte(r2)     // Catch: java.lang.Throwable -> L8d
            java.lang.String r2 = r0.getPath()     // Catch: java.lang.Throwable -> L8d
            r1.writeString(r2)     // Catch: java.lang.Throwable -> L8d
            long r2 = r0.lastModified()     // Catch: java.lang.Throwable -> L8d
            r1.writeLong(r2)     // Catch: java.lang.Throwable -> L8d
            byte[] r0 = r1.marshall()     // Catch: java.lang.Throwable -> L8d
            r1.recycle()
            return r0
        L8d:
            r0 = move-exception
            r1.recycle()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.f.m.a.g():byte[]");
    }

    @Override // b.f.m.m
    public m.f i() throws IOException {
        return new C0077a(this);
    }
}
