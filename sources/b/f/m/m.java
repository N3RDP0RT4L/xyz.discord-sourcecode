package b.f.m;

import android.content.Context;
import android.os.Parcel;
import android.os.StrictMode;
import android.util.Log;
import com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel;
import com.facebook.soloader.SysUtil$LollipopSysdeps;
import java.io.Closeable;
import java.io.DataInput;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
/* compiled from: UnpackingSoSource.java */
/* loaded from: classes2.dex */
public abstract class m extends b.f.m.c {
    public final Context c;
    public String d;
    public final Map<String, Object> e = new HashMap();

    /* compiled from: UnpackingSoSource.java */
    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public final /* synthetic */ File j;
        public final /* synthetic */ byte[] k;
        public final /* synthetic */ c l;
        public final /* synthetic */ File m;
        public final /* synthetic */ g n;

        public a(File file, byte[] bArr, c cVar, File file2, g gVar) {
            this.j = file;
            this.k = bArr;
            this.l = cVar;
            this.m = file2;
            this.n = gVar;
        }

        @Override // java.lang.Runnable
        public void run() {
            try {
                Log.v("fb-UnpackingSoSource", "starting syncer worker");
                RandomAccessFile randomAccessFile = new RandomAccessFile(this.j, "rw");
                try {
                    randomAccessFile.write(this.k);
                    randomAccessFile.setLength(randomAccessFile.getFilePointer());
                    randomAccessFile.close();
                    randomAccessFile = new RandomAccessFile(new File(m.this.a, "dso_manifest"), "rw");
                    try {
                        c cVar = this.l;
                        Objects.requireNonNull(cVar);
                        randomAccessFile.writeByte(1);
                        randomAccessFile.writeInt(cVar.a.length);
                        int i = 0;
                        while (true) {
                            b[] bVarArr = cVar.a;
                            if (i < bVarArr.length) {
                                randomAccessFile.writeUTF(bVarArr[i].j);
                                randomAccessFile.writeUTF(cVar.a[i].k);
                                i++;
                            } else {
                                randomAccessFile.close();
                                b.c.a.a0.d.m0(m.this.a);
                                m.l(this.m, (byte) 1);
                                Log.v("fb-UnpackingSoSource", "releasing dso store lock for " + m.this.a + " (from syncer thread)");
                                this.n.close();
                                return;
                            }
                        }
                    } finally {
                        try {
                            throw th;
                        } catch (Throwable th) {
                        }
                    }
                } finally {
                    try {
                        throw th;
                    } catch (Throwable th2) {
                        try {
                            randomAccessFile.close();
                        } catch (Throwable th3) {
                            th.addSuppressed(th3);
                        }
                    }
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /* compiled from: UnpackingSoSource.java */
    /* loaded from: classes2.dex */
    public static class b {
        public final String j;
        public final String k;

        public b(String str, String str2) {
            this.j = str;
            this.k = str2;
        }
    }

    /* compiled from: UnpackingSoSource.java */
    /* loaded from: classes2.dex */
    public static final class c {
        public final b[] a;

        public c(b[] bVarArr) {
            this.a = bVarArr;
        }

        public static final c a(DataInput dataInput) throws IOException {
            if (dataInput.readByte() == 1) {
                int readInt = dataInput.readInt();
                if (readInt >= 0) {
                    b[] bVarArr = new b[readInt];
                    for (int i = 0; i < readInt; i++) {
                        bVarArr[i] = new b(dataInput.readUTF(), dataInput.readUTF());
                    }
                    return new c(bVarArr);
                }
                throw new RuntimeException("illegal number of shared libraries");
            }
            throw new RuntimeException("wrong dso manifest version");
        }
    }

    /* compiled from: UnpackingSoSource.java */
    /* loaded from: classes2.dex */
    public static final class d implements Closeable {
        public final b j;
        public final InputStream k;

        public d(b bVar, InputStream inputStream) {
            this.j = bVar;
            this.k = inputStream;
        }

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
            this.k.close();
        }
    }

    /* compiled from: UnpackingSoSource.java */
    /* loaded from: classes2.dex */
    public static abstract class e implements Closeable {
        public abstract boolean a();

        public abstract d b() throws IOException;

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
        }
    }

    /* compiled from: UnpackingSoSource.java */
    /* loaded from: classes2.dex */
    public static abstract class f implements Closeable {
        public abstract c a() throws IOException;

        public abstract e b() throws IOException;

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
        }
    }

    public m(Context context, String str) {
        super(new File(b.d.b.a.a.J(new StringBuilder(), context.getApplicationInfo().dataDir, AutocompleteViewModel.COMMAND_DISCOVER_TOKEN, str)), 1);
        this.c = context;
    }

    public static void l(File file, byte b2) throws IOException {
        RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
        try {
            randomAccessFile.seek(0L);
            randomAccessFile.write(b2);
            randomAccessFile.setLength(randomAccessFile.getFilePointer());
            randomAccessFile.getFD().sync();
            randomAccessFile.close();
        } catch (Throwable th) {
            try {
                throw th;
            } catch (Throwable th2) {
                try {
                    randomAccessFile.close();
                } catch (Throwable th3) {
                    th.addSuppressed(th3);
                }
                throw th2;
            }
        }
    }

    @Override // b.f.m.c, b.f.m.l
    public int a(String str, int i, StrictMode.ThreadPolicy threadPolicy) throws IOException {
        int c2;
        synchronized (h(str)) {
            c2 = c(str, i, this.a, threadPolicy);
        }
        return c2;
    }

    @Override // b.f.m.l
    public void b(int i) throws IOException {
        File file = this.a;
        if (file.mkdirs() || file.isDirectory()) {
            g gVar = new g(new File(this.a, "dso_lock"));
            try {
                Log.v("fb-UnpackingSoSource", "locked dso store " + this.a);
                if (j(gVar, i, g())) {
                    gVar = null;
                } else {
                    Log.i("fb-UnpackingSoSource", "dso store is up-to-date: " + this.a);
                }
                if (gVar == null) {
                    StringBuilder R = b.d.b.a.a.R("not releasing dso store lock for ");
                    R.append(this.a);
                    R.append(" (syncer thread started)");
                    Log.v("fb-UnpackingSoSource", R.toString());
                }
            } finally {
                StringBuilder R2 = b.d.b.a.a.R("releasing dso store lock for ");
                R2.append(this.a);
                Log.v("fb-UnpackingSoSource", R2.toString());
                gVar.close();
            }
        } else {
            throw new IOException("cannot mkdir: " + file);
        }
    }

    public final void d(b[] bVarArr) throws IOException {
        String[] list = this.a.list();
        if (list != null) {
            for (String str : list) {
                if (!str.equals("dso_state") && !str.equals("dso_lock") && !str.equals("dso_deps") && !str.equals("dso_manifest")) {
                    boolean z2 = false;
                    for (int i = 0; !z2 && i < bVarArr.length; i++) {
                        if (bVarArr[i].j.equals(str)) {
                            z2 = true;
                        }
                    }
                    if (!z2) {
                        File file = new File(this.a, str);
                        Log.v("fb-UnpackingSoSource", "deleting unaccounted-for file " + file);
                        b.c.a.a0.d.b0(file);
                    }
                }
            }
            return;
        }
        StringBuilder R = b.d.b.a.a.R("unable to list directory ");
        R.append(this.a);
        throw new IOException(R.toString());
    }

    public final void e(d dVar, byte[] bArr) throws IOException {
        boolean writable;
        StringBuilder R = b.d.b.a.a.R("extracting DSO ");
        R.append(dVar.j.j);
        Log.i("fb-UnpackingSoSource", R.toString());
        try {
            if (this.a.setWritable(true)) {
                f(dVar, bArr);
                if (writable) {
                    return;
                }
                return;
            }
            throw new IOException("cannot make directory writable for us: " + this.a);
        } finally {
            if (!this.a.setWritable(false)) {
                StringBuilder R2 = b.d.b.a.a.R("error removing ");
                R2.append(this.a.getCanonicalPath());
                R2.append(" write permission");
                Log.w("fb-UnpackingSoSource", R2.toString());
            }
        }
    }

    public final void f(d dVar, byte[] bArr) throws IOException {
        RandomAccessFile randomAccessFile;
        File file = new File(this.a, dVar.j.j);
        RandomAccessFile randomAccessFile2 = null;
        try {
            try {
                if (file.exists() && !file.setWritable(true)) {
                    Log.w("fb-UnpackingSoSource", "error adding write permission to: " + file);
                }
                try {
                    randomAccessFile = new RandomAccessFile(file, "rw");
                } catch (IOException e2) {
                    Log.w("fb-UnpackingSoSource", "error overwriting " + file + " trying to delete and start over", e2);
                    b.c.a.a0.d.b0(file);
                    randomAccessFile = new RandomAccessFile(file, "rw");
                }
                randomAccessFile2 = randomAccessFile;
                int available = dVar.k.available();
                if (available > 1) {
                    SysUtil$LollipopSysdeps.fallocateIfSupported(randomAccessFile2.getFD(), available);
                }
                InputStream inputStream = dVar.k;
                int i = 0;
                while (i < Integer.MAX_VALUE) {
                    int read = inputStream.read(bArr, 0, Math.min(bArr.length, Integer.MAX_VALUE - i));
                    if (read == -1) {
                        break;
                    }
                    randomAccessFile2.write(bArr, 0, read);
                    i += read;
                }
                randomAccessFile2.setLength(randomAccessFile2.getFilePointer());
                if (file.setExecutable(true, false)) {
                    if (!file.setWritable(false)) {
                        Log.w("fb-UnpackingSoSource", "error removing " + file + " write permission");
                    }
                    randomAccessFile2.close();
                    return;
                }
                throw new IOException("cannot make file executable: " + file);
            } catch (IOException e3) {
                b.c.a.a0.d.b0(file);
                throw e3;
            }
        } catch (Throwable th) {
            if (!file.setWritable(false)) {
                Log.w("fb-UnpackingSoSource", "error removing " + file + " write permission");
            }
            if (randomAccessFile2 != null) {
                randomAccessFile2.close();
            }
            throw th;
        }
    }

    public byte[] g() throws IOException {
        Parcel obtain = Parcel.obtain();
        f i = i();
        try {
            b[] bVarArr = i.a().a;
            obtain.writeByte((byte) 1);
            obtain.writeInt(bVarArr.length);
            for (int i2 = 0; i2 < bVarArr.length; i2++) {
                obtain.writeString(bVarArr[i2].j);
                obtain.writeString(bVarArr[i2].k);
            }
            i.close();
            byte[] marshall = obtain.marshall();
            obtain.recycle();
            return marshall;
        } catch (Throwable th) {
            try {
                throw th;
            } catch (Throwable th2) {
                if (i != null) {
                    try {
                        i.close();
                    } catch (Throwable th3) {
                        th.addSuppressed(th3);
                    }
                }
                throw th2;
            }
        }
    }

    public final Object h(String str) {
        Object obj;
        synchronized (this.e) {
            obj = this.e.get(str);
            if (obj == null) {
                obj = new Object();
                this.e.put(str, obj);
            }
        }
        return obj;
    }

    public abstract f i() throws IOException;

    /* JADX WARN: Removed duplicated region for block: B:32:0x009d A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:33:0x009e  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final boolean j(b.f.m.g r12, int r13, byte[] r14) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 235
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.f.m.m.j(b.f.m.g, int, byte[]):boolean");
    }

    /* JADX WARN: Removed duplicated region for block: B:11:0x003f A[Catch: all -> 0x0032, TryCatch #7 {all -> 0x0032, blocks: (B:4:0x002d, B:7:0x0036, B:11:0x003f, B:12:0x0046, B:13:0x0050, B:15:0x0056, B:29:0x009e, B:18:0x005e, B:20:0x0063, B:22:0x0071, B:25:0x0082, B:27:0x0089), top: B:38:0x002d }] */
    /* JADX WARN: Removed duplicated region for block: B:15:0x0056 A[Catch: all -> 0x0032, TRY_LEAVE, TryCatch #7 {all -> 0x0032, blocks: (B:4:0x002d, B:7:0x0036, B:11:0x003f, B:12:0x0046, B:13:0x0050, B:15:0x0056, B:29:0x009e, B:18:0x005e, B:20:0x0063, B:22:0x0071, B:25:0x0082, B:27:0x0089), top: B:38:0x002d }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void k(byte r8, b.f.m.m.c r9, b.f.m.m.e r10) throws java.io.IOException {
        /*
            r7 = this;
            java.lang.String r0 = "regenerating DSO store "
            java.lang.StringBuilder r0 = b.d.b.a.a.R(r0)
            java.lang.Class r1 = r7.getClass()
            java.lang.String r1 = r1.getName()
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.String r1 = "fb-UnpackingSoSource"
            android.util.Log.v(r1, r0)
            java.io.File r0 = new java.io.File
            java.io.File r2 = r7.a
            java.lang.String r3 = "dso_manifest"
            r0.<init>(r2, r3)
            java.io.RandomAccessFile r2 = new java.io.RandomAccessFile
            java.lang.String r3 = "rw"
            r2.<init>(r0, r3)
            r0 = 1
            if (r8 != r0) goto L3b
            b.f.m.m$c r8 = b.f.m.m.c.a(r2)     // Catch: java.lang.Throwable -> L32 java.lang.Exception -> L35
            goto L3c
        L32:
            r8 = move-exception
            goto Lc4
        L35:
            r8 = move-exception
            java.lang.String r0 = "error reading existing DSO manifest"
            android.util.Log.i(r1, r0, r8)     // Catch: java.lang.Throwable -> L32
        L3b:
            r8 = 0
        L3c:
            r0 = 0
            if (r8 != 0) goto L46
            b.f.m.m$c r8 = new b.f.m.m$c     // Catch: java.lang.Throwable -> L32
            b.f.m.m$b[] r0 = new b.f.m.m.b[r0]     // Catch: java.lang.Throwable -> L32
            r8.<init>(r0)     // Catch: java.lang.Throwable -> L32
        L46:
            b.f.m.m$b[] r9 = r9.a     // Catch: java.lang.Throwable -> L32
            r7.d(r9)     // Catch: java.lang.Throwable -> L32
            r9 = 32768(0x8000, float:4.5918E-41)
            byte[] r9 = new byte[r9]     // Catch: java.lang.Throwable -> L32
        L50:
            boolean r0 = r10.a()     // Catch: java.lang.Throwable -> L32
            if (r0 == 0) goto La4
            b.f.m.m$d r0 = r10.b()     // Catch: java.lang.Throwable -> L32
            r3 = 1
            r4 = 0
        L5c:
            if (r3 == 0) goto L87
            b.f.m.m$b[] r5 = r8.a     // Catch: java.lang.Throwable -> L85
            int r6 = r5.length     // Catch: java.lang.Throwable -> L85
            if (r4 >= r6) goto L87
            r5 = r5[r4]     // Catch: java.lang.Throwable -> L85
            java.lang.String r5 = r5.j     // Catch: java.lang.Throwable -> L85
            b.f.m.m$b r6 = r0.j     // Catch: java.lang.Throwable -> L85
            java.lang.String r6 = r6.j     // Catch: java.lang.Throwable -> L85
            boolean r5 = r5.equals(r6)     // Catch: java.lang.Throwable -> L85
            if (r5 == 0) goto L82
            b.f.m.m$b[] r5 = r8.a     // Catch: java.lang.Throwable -> L85
            r5 = r5[r4]     // Catch: java.lang.Throwable -> L85
            java.lang.String r5 = r5.k     // Catch: java.lang.Throwable -> L85
            b.f.m.m$b r6 = r0.j     // Catch: java.lang.Throwable -> L85
            java.lang.String r6 = r6.k     // Catch: java.lang.Throwable -> L85
            boolean r5 = r5.equals(r6)     // Catch: java.lang.Throwable -> L85
            if (r5 == 0) goto L82
            r3 = 0
        L82:
            int r4 = r4 + 1
            goto L5c
        L85:
            r8 = move-exception
            goto L8d
        L87:
            if (r3 == 0) goto L9c
            r7.e(r0, r9)     // Catch: java.lang.Throwable -> L85
            goto L9c
        L8d:
            throw r8     // Catch: java.lang.Throwable -> L8e
        L8e:
            r9 = move-exception
            if (r0 == 0) goto L9b
            java.io.InputStream r10 = r0.k     // Catch: java.lang.Throwable -> L97
            r10.close()     // Catch: java.lang.Throwable -> L97
            goto L9b
        L97:
            r10 = move-exception
            r8.addSuppressed(r10)     // Catch: java.lang.Throwable -> L32
        L9b:
            throw r9     // Catch: java.lang.Throwable -> L32
        L9c:
            if (r0 == 0) goto L50
            java.io.InputStream r0 = r0.k     // Catch: java.lang.Throwable -> L32
            r0.close()     // Catch: java.lang.Throwable -> L32
            goto L50
        La4:
            r2.close()
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "Finished regenerating DSO store "
            r8.append(r9)
            java.lang.Class r9 = r7.getClass()
            java.lang.String r9 = r9.getName()
            r8.append(r9)
            java.lang.String r8 = r8.toString()
            android.util.Log.v(r1, r8)
            return
        Lc4:
            throw r8     // Catch: java.lang.Throwable -> Lc5
        Lc5:
            r9 = move-exception
            r2.close()     // Catch: java.lang.Throwable -> Lca
            goto Lce
        Lca:
            r10 = move-exception
            r8.addSuppressed(r10)
        Lce:
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: b.f.m.m.k(byte, b.f.m.m$c, b.f.m.m$e):void");
    }
}
