package b.f.m;

import android.content.Context;
import b.f.m.m;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
/* compiled from: ExoSoSource.java */
/* loaded from: classes2.dex */
public final class e extends m {

    /* compiled from: ExoSoSource.java */
    /* loaded from: classes2.dex */
    public final class b extends m.f {
        public final c[] j;

        /* compiled from: ExoSoSource.java */
        /* loaded from: classes2.dex */
        public final class a extends m.e {
            public int j;

            public a(a aVar) {
            }

            @Override // b.f.m.m.e
            public boolean a() {
                return this.j < b.this.j.length;
            }

            @Override // b.f.m.m.e
            public m.d b() throws IOException {
                c[] cVarArr = b.this.j;
                int i = this.j;
                this.j = i + 1;
                c cVar = cVarArr[i];
                FileInputStream fileInputStream = new FileInputStream(cVar.l);
                try {
                    return new m.d(cVar, fileInputStream);
                } catch (Throwable th) {
                    fileInputStream.close();
                    throw th;
                }
            }
        }

        /* JADX WARN: Code restructure failed: missing block: B:28:0x00d8, code lost:
            throw new java.lang.RuntimeException("illegal line in exopackage metadata: [" + r10 + "]");
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public b(b.f.m.e r17, b.f.m.m r18) throws java.io.IOException {
            /*
                Method dump skipped, instructions count: 286
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: b.f.m.e.b.<init>(b.f.m.e, b.f.m.m):void");
        }

        @Override // b.f.m.m.f
        public m.c a() throws IOException {
            return new m.c(this.j);
        }

        @Override // b.f.m.m.f
        public m.e b() throws IOException {
            return new a(null);
        }
    }

    /* compiled from: ExoSoSource.java */
    /* loaded from: classes2.dex */
    public static final class c extends m.b {
        public final File l;

        public c(String str, String str2, File file) {
            super(str, str2);
            this.l = file;
        }
    }

    public e(Context context, String str) {
        super(context, str);
    }

    @Override // b.f.m.m
    public m.f i() throws IOException {
        return new b(this, this);
    }
}
