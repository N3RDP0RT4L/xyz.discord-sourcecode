package b.f.m;

import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileLock;
/* compiled from: FileLocker.java */
/* loaded from: classes2.dex */
public final class g implements Closeable {
    public final FileOutputStream j;
    public final FileLock k;

    public g(File file) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        this.j = fileOutputStream;
        try {
            FileLock lock = fileOutputStream.getChannel().lock();
            if (lock == null) {
                fileOutputStream.close();
            }
            this.k = lock;
        } catch (Throwable th) {
            this.j.close();
            throw th;
        }
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        try {
            FileLock fileLock = this.k;
            if (fileLock != null) {
                fileLock.release();
            }
        } finally {
            this.j.close();
        }
    }
}
