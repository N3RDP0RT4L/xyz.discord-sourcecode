package b.f.m;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.StrictMode;
import android.util.Log;
import java.io.File;
import java.io.IOException;
/* compiled from: ApplicationSoSource.java */
/* loaded from: classes2.dex */
public class b extends l {
    public Context a;

    /* renamed from: b  reason: collision with root package name */
    public int f648b;
    public c c;

    public b(Context context, int i) {
        Context applicationContext = context.getApplicationContext();
        this.a = applicationContext;
        if (applicationContext == null) {
            Log.w("SoLoader", "context.getApplicationContext returned null, holding reference to original context.");
            this.a = context;
        }
        this.f648b = i;
        this.c = new c(new File(this.a.getApplicationInfo().nativeLibraryDir), i);
    }

    public static File d(Context context) {
        return new File(context.getApplicationInfo().nativeLibraryDir);
    }

    @Override // b.f.m.l
    public int a(String str, int i, StrictMode.ThreadPolicy threadPolicy) throws IOException {
        return this.c.a(str, i, threadPolicy);
    }

    @Override // b.f.m.l
    public void b(int i) throws IOException {
        this.c.b(i);
    }

    public boolean c() throws IOException {
        File file = this.c.a;
        Context e = e();
        File d = d(e);
        if (file.equals(d)) {
            return false;
        }
        Log.d("SoLoader", "Native library directory updated from " + file + " to " + d);
        int i = this.f648b | 1;
        this.f648b = i;
        this.c = new c(d, i);
        this.a = e;
        return true;
    }

    public Context e() {
        try {
            Context context = this.a;
            return context.createPackageContext(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override // b.f.m.l
    public String toString() {
        return this.c.toString();
    }
}
