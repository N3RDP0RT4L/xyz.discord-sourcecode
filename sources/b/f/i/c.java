package b.f.i;
/* compiled from: ImageFormat.java */
/* loaded from: classes2.dex */
public class c {
    public static final c a = new c("UNKNOWN", null);

    /* renamed from: b  reason: collision with root package name */
    public final String f537b;

    /* compiled from: ImageFormat.java */
    /* loaded from: classes2.dex */
    public interface a {
        int a();

        c b(byte[] bArr, int i);
    }

    public c(String str, String str2) {
        this.f537b = str;
    }

    public String toString() {
        return this.f537b;
    }
}
