package b.f.i;

import b.f.d.d.m;
import b.f.i.c;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;
/* compiled from: ImageFormatChecker.java */
/* loaded from: classes2.dex */
public class d {
    public static d a;

    /* renamed from: b  reason: collision with root package name */
    public int f538b;
    public List<c.a> c;
    public final a d = new a();

    public d() {
        d();
    }

    public static c a(InputStream inputStream) throws IOException {
        int B1;
        d c = c();
        Objects.requireNonNull(c);
        Objects.requireNonNull(inputStream);
        int i = c.f538b;
        byte[] bArr = new byte[i];
        b.c.a.a0.d.i(Boolean.valueOf(i >= i));
        if (inputStream.markSupported()) {
            try {
                inputStream.mark(i);
                B1 = b.c.a.a0.d.B1(inputStream, bArr, 0, i);
            } finally {
                inputStream.reset();
            }
        } else {
            B1 = b.c.a.a0.d.B1(inputStream, bArr, 0, i);
        }
        c b2 = c.d.b(bArr, B1);
        if (b2 != c.a) {
            return b2;
        }
        List<c.a> list = c.c;
        if (list != null) {
            for (c.a aVar : list) {
                c b3 = aVar.b(bArr, B1);
                if (!(b3 == null || b3 == c.a)) {
                    return b3;
                }
            }
        }
        return c.a;
    }

    public static c b(InputStream inputStream) {
        try {
            return a(inputStream);
        } catch (IOException e) {
            m.a(e);
            throw new RuntimeException(e);
        }
    }

    public static synchronized d c() {
        d dVar;
        synchronized (d.class) {
            if (a == null) {
                a = new d();
            }
            dVar = a;
        }
        return dVar;
    }

    public final void d() {
        this.f538b = this.d.p;
        List<c.a> list = this.c;
        if (list != null) {
            for (c.a aVar : list) {
                this.f538b = Math.max(this.f538b, aVar.a());
            }
        }
    }
}
