package b.f.i;

import androidx.exifinterface.media.ExifInterface;
import b.c.a.a0.d;
import b.f.i.c;
/* compiled from: DefaultImageFormatChecker.java */
/* loaded from: classes2.dex */
public class a implements c.a {
    public static final byte[] a;

    /* renamed from: b  reason: collision with root package name */
    public static final int f535b;
    public static final byte[] c;
    public static final int d;
    public static final byte[] g;
    public static final int h;
    public static final byte[] i;
    public static final int j;
    public static final byte[] m;
    public static final int o;
    public final int p;
    public static final byte[] e = d.e("GIF87a");
    public static final byte[] f = d.e("GIF89a");
    public static final byte[] k = d.e("ftyp");
    public static final byte[][] l = {d.e("heic"), d.e("heix"), d.e("hevc"), d.e("hevx"), d.e("mif1"), d.e("msf1")};
    public static final byte[] n = {77, 77, 0, ExifInterface.START_CODE};

    static {
        byte[] bArr = {-1, -40, -1};
        a = bArr;
        f535b = bArr.length;
        byte[] bArr2 = {-119, 80, 78, 71, 13, 10, 26, 10};
        c = bArr2;
        d = bArr2.length;
        byte[] e2 = d.e("BM");
        g = e2;
        h = e2.length;
        byte[] bArr3 = {0, 0, 1, 0};
        i = bArr3;
        j = bArr3.length;
        byte[] bArr4 = {73, 73, ExifInterface.START_CODE, 0};
        m = bArr4;
        o = bArr4.length;
    }

    public a() {
        int[] iArr = {21, 20, f535b, d, 6, h, j, 12};
        d.i(Boolean.TRUE);
        int i2 = iArr[0];
        for (int i3 = 1; i3 < 8; i3++) {
            if (iArr[i3] > i2) {
                i2 = iArr[i3];
            }
        }
        this.p = i2;
    }

    public static c c(byte[] bArr, int i2) {
        boolean z2 = false;
        d.i(Boolean.valueOf(b.f.d.m.c.b(bArr, 0, i2)));
        if (b.f.d.m.c.d(bArr, 12, b.f.d.m.c.e)) {
            return b.f;
        }
        if (b.f.d.m.c.d(bArr, 12, b.f.d.m.c.f)) {
            return b.g;
        }
        if (!(i2 >= 21 && b.f.d.m.c.d(bArr, 12, b.f.d.m.c.g))) {
            return c.a;
        }
        byte[] bArr2 = b.f.d.m.c.g;
        if (b.f.d.m.c.d(bArr, 12, bArr2) && ((bArr[20] & 2) == 2)) {
            return b.j;
        }
        boolean d2 = b.f.d.m.c.d(bArr, 12, bArr2);
        boolean z3 = (bArr[20] & 16) == 16;
        if (d2 && z3) {
            z2 = true;
        }
        if (z2) {
            return b.i;
        }
        return b.h;
    }

    @Override // b.f.i.c.a
    public int a() {
        return this.p;
    }

    @Override // b.f.i.c.a
    public final c b(byte[] bArr, int i2) {
        boolean z2;
        boolean z3 = false;
        if (b.f.d.m.c.b(bArr, 0, i2)) {
            return c(bArr, i2);
        }
        byte[] bArr2 = a;
        if (i2 >= bArr2.length && d.I0(bArr, bArr2, 0)) {
            return b.a;
        }
        byte[] bArr3 = c;
        if (i2 >= bArr3.length && d.I0(bArr, bArr3, 0)) {
            return b.f536b;
        }
        if (i2 >= 6 && (d.I0(bArr, e, 0) || d.I0(bArr, f, 0))) {
            return b.c;
        }
        byte[] bArr4 = g;
        if (i2 < bArr4.length ? false : d.I0(bArr, bArr4, 0)) {
            return b.d;
        }
        byte[] bArr5 = i;
        if (i2 < bArr5.length ? false : d.I0(bArr, bArr5, 0)) {
            return b.e;
        }
        if (i2 >= 12 && bArr[3] >= 8 && d.I0(bArr, k, 4)) {
            for (byte[] bArr6 : l) {
                if (d.I0(bArr, bArr6, 8)) {
                    z2 = true;
                    break;
                }
            }
        }
        z2 = false;
        if (z2) {
            return b.k;
        }
        if (i2 >= o && (d.I0(bArr, m, 0) || d.I0(bArr, n, 0))) {
            z3 = true;
        }
        if (z3) {
            return b.l;
        }
        return c.a;
    }
}
