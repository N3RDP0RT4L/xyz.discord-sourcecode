package b.f.b.b;

import com.facebook.cache.common.CacheKey;
/* compiled from: SettableCacheEvent.java */
/* loaded from: classes2.dex */
public class i {
    public static final Object a = new Object();

    /* renamed from: b  reason: collision with root package name */
    public static i f464b;
    public static int c;
    public CacheKey d;
    public i e;

    public static i a() {
        synchronized (a) {
            i iVar = f464b;
            if (iVar == null) {
                return new i();
            }
            f464b = iVar.e;
            iVar.e = null;
            c--;
            return iVar;
        }
    }

    public void b() {
        synchronized (a) {
            int i = c;
            if (i < 5) {
                c = i + 1;
                i iVar = f464b;
                if (iVar != null) {
                    this.e = iVar;
                }
                f464b = this;
            }
        }
    }
}
