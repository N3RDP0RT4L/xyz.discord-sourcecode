package b.f.b.b;

import androidx.annotation.VisibleForTesting;
import b.f.b.b.c;
import b.f.j.c.h;
import com.facebook.common.file.FileUtils$CreateDirectoryException;
import com.facebook.common.file.FileUtils$ParentDirNotFoundException;
import com.facebook.common.file.FileUtils$RenameException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
/* compiled from: DefaultDiskStorage.java */
/* loaded from: classes2.dex */
public class a implements b.f.b.b.c {
    public static final long a = TimeUnit.MINUTES.toMillis(30);

    /* renamed from: b  reason: collision with root package name */
    public static final /* synthetic */ int f453b = 0;
    public final File c;
    public final boolean d;
    public final File e;
    public final b.f.b.a.a f;
    public final b.f.d.k.a g;

    /* compiled from: DefaultDiskStorage.java */
    /* loaded from: classes2.dex */
    public class b implements b.f.d.c.a {
        public final List<c.a> a = new ArrayList();

        public b(C0064a aVar) {
        }

        @Override // b.f.d.c.a
        public void a(File file) {
            d h = a.h(a.this, file);
            if (h != null && h.a == ".cnt") {
                this.a.add(new c(h.f456b, file, null));
            }
        }

        @Override // b.f.d.c.a
        public void b(File file) {
        }

        @Override // b.f.d.c.a
        public void c(File file) {
        }
    }

    /* compiled from: DefaultDiskStorage.java */
    @VisibleForTesting
    /* loaded from: classes2.dex */
    public static class c implements c.a {
        public final String a;

        /* renamed from: b  reason: collision with root package name */
        public final b.f.a.a f455b;
        public long c = -1;
        public long d = -1;

        public c(String str, File file, C0064a aVar) {
            Objects.requireNonNull(str);
            this.a = str;
            this.f455b = new b.f.a.a(file);
        }

        @Override // b.f.b.b.c.a
        public long a() {
            if (this.d < 0) {
                this.d = this.f455b.a.lastModified();
            }
            return this.d;
        }

        @Override // b.f.b.b.c.a
        public String getId() {
            return this.a;
        }

        @Override // b.f.b.b.c.a
        public long getSize() {
            if (this.c < 0) {
                this.c = this.f455b.a();
            }
            return this.c;
        }
    }

    /* compiled from: DefaultDiskStorage.java */
    /* loaded from: classes2.dex */
    public static class d {
        public final String a;

        /* renamed from: b  reason: collision with root package name */
        public final String f456b;

        public d(String str, String str2) {
            this.a = str;
            this.f456b = str2;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(this.a);
            sb.append("(");
            return b.d.b.a.a.H(sb, this.f456b, ")");
        }
    }

    /* compiled from: DefaultDiskStorage.java */
    /* loaded from: classes2.dex */
    public static class e extends IOException {
        public e(long j, long j2) {
            super("File was not written completely. Expected: " + j + ", found: " + j2);
        }
    }

    /* compiled from: DefaultDiskStorage.java */
    @VisibleForTesting
    /* loaded from: classes2.dex */
    public class f implements c.b {
        public final String a;
        @VisibleForTesting

        /* renamed from: b  reason: collision with root package name */
        public final File f457b;

        public f(String str, File file) {
            this.a = str;
            this.f457b = file;
        }

        public boolean a() {
            return !this.f457b.exists() || this.f457b.delete();
        }

        public b.f.a.a b(Object obj) throws IOException {
            Objects.requireNonNull((b.f.d.k.c) a.this.g);
            long currentTimeMillis = System.currentTimeMillis();
            File j = a.this.j(this.a);
            try {
                b.c.a.a0.d.Q1(this.f457b, j);
                if (j.exists()) {
                    j.setLastModified(currentTimeMillis);
                }
                return new b.f.a.a(j);
            } catch (FileUtils$RenameException e) {
                Throwable cause = e.getCause();
                if (cause != null && !(cause instanceof FileUtils$ParentDirNotFoundException)) {
                    boolean z2 = cause instanceof FileNotFoundException;
                }
                b.f.b.a.a aVar = a.this.f;
                int i = a.f453b;
                Objects.requireNonNull((b.f.b.a.d) aVar);
                throw e;
            }
        }

        /* JADX WARN: Finally extract failed */
        public void c(b.f.b.a.g gVar, Object obj) throws IOException {
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(this.f457b);
                try {
                    b.f.d.d.b bVar = new b.f.d.d.b(fileOutputStream);
                    h hVar = (h) gVar;
                    InputStream e = hVar.a.e();
                    Objects.requireNonNull(e);
                    hVar.f552b.c.a(e, bVar);
                    bVar.flush();
                    long j = bVar.j;
                    fileOutputStream.close();
                    if (this.f457b.length() != j) {
                        throw new e(j, this.f457b.length());
                    }
                } catch (Throwable th) {
                    fileOutputStream.close();
                    throw th;
                }
            } catch (FileNotFoundException e2) {
                b.f.b.a.a aVar = a.this.f;
                int i = a.f453b;
                Objects.requireNonNull((b.f.b.a.d) aVar);
                throw e2;
            }
        }
    }

    /* compiled from: DefaultDiskStorage.java */
    /* loaded from: classes2.dex */
    public class g implements b.f.d.c.a {
        public boolean a;

        public g(C0064a aVar) {
        }

        /* JADX WARN: Code restructure failed: missing block: B:9:0x002b, code lost:
            if (r3 > (java.lang.System.currentTimeMillis() - b.f.b.b.a.a)) goto L14;
         */
        @Override // b.f.d.c.a
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public void a(java.io.File r10) {
            /*
                r9 = this;
                boolean r0 = r9.a
                if (r0 == 0) goto L39
                b.f.b.b.a r0 = b.f.b.b.a.this
                b.f.b.b.a$d r0 = b.f.b.b.a.h(r0, r10)
                r1 = 0
                r2 = 1
                if (r0 != 0) goto Lf
                goto L37
            Lf:
                java.lang.String r0 = r0.a
                java.lang.String r3 = ".tmp"
                if (r0 != r3) goto L2e
                long r3 = r10.lastModified()
                b.f.b.b.a r0 = b.f.b.b.a.this
                b.f.d.k.a r0 = r0.g
                b.f.d.k.c r0 = (b.f.d.k.c) r0
                java.util.Objects.requireNonNull(r0)
                long r5 = java.lang.System.currentTimeMillis()
                long r7 = b.f.b.b.a.a
                long r5 = r5 - r7
                int r0 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r0 <= 0) goto L37
                goto L36
            L2e:
                java.lang.String r3 = ".cnt"
                if (r0 != r3) goto L33
                r1 = 1
            L33:
                b.c.a.a0.d.B(r1)
            L36:
                r1 = 1
            L37:
                if (r1 != 0) goto L3c
            L39:
                r10.delete()
            L3c:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.f.b.b.a.g.a(java.io.File):void");
        }

        @Override // b.f.d.c.a
        public void b(File file) {
            if (!this.a && file.equals(a.this.e)) {
                this.a = true;
            }
        }

        @Override // b.f.d.c.a
        public void c(File file) {
            if (!a.this.c.equals(file) && !this.a) {
                file.delete();
            }
            if (this.a && file.equals(a.this.e)) {
                this.a = false;
            }
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:12:0x005b  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0069 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public a(java.io.File r6, int r7, b.f.b.a.a r8) {
        /*
            r5 = this;
            r5.<init>()
            r5.c = r6
            r0 = 0
            java.io.File r1 = android.os.Environment.getExternalStorageDirectory()     // Catch: java.lang.Exception -> L20
            if (r1 == 0) goto L26
            java.lang.String r1 = r1.toString()     // Catch: java.lang.Exception -> L20
            java.lang.String r6 = r6.getCanonicalPath()     // Catch: java.io.IOException -> L19 java.lang.Exception -> L20
            boolean r6 = r6.contains(r1)     // Catch: java.io.IOException -> L19 java.lang.Exception -> L20
            goto L27
        L19:
            r6 = r8
            b.f.b.a.d r6 = (b.f.b.a.d) r6     // Catch: java.lang.Exception -> L20
            java.util.Objects.requireNonNull(r6)     // Catch: java.lang.Exception -> L20
            goto L26
        L20:
            r6 = r8
            b.f.b.a.d r6 = (b.f.b.a.d) r6
            java.util.Objects.requireNonNull(r6)
        L26:
            r6 = 0
        L27:
            r5.d = r6
            java.io.File r6 = new java.io.File
            java.io.File r1 = r5.c
            r2 = 3
            java.lang.Object[] r2 = new java.lang.Object[r2]
            java.lang.String r3 = "v2"
            r2[r0] = r3
            r3 = 100
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r4 = 1
            r2[r4] = r3
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)
            r3 = 2
            r2[r3] = r7
            r7 = 0
            java.lang.String r3 = "%s.ols%d.%d"
            java.lang.String r7 = java.lang.String.format(r7, r3, r2)
            r6.<init>(r1, r7)
            r5.e = r6
            r5.f = r8
            java.io.File r7 = r5.c
            boolean r7 = r7.exists()
            if (r7 != 0) goto L5b
            goto L66
        L5b:
            boolean r7 = r6.exists()
            if (r7 != 0) goto L67
            java.io.File r7 = r5.c
            b.c.a.a0.d.Z(r7)
        L66:
            r0 = 1
        L67:
            if (r0 == 0) goto L82
            b.c.a.a0.d.l1(r6)     // Catch: com.facebook.common.file.FileUtils$CreateDirectoryException -> L6d
            goto L82
        L6d:
            b.f.b.a.a r6 = r5.f
            java.lang.String r7 = "version directory could not be created: "
            java.lang.StringBuilder r7 = b.d.b.a.a.R(r7)
            java.io.File r8 = r5.e
            r7.append(r8)
            r7.toString()
            b.f.b.a.d r6 = (b.f.b.a.d) r6
            java.util.Objects.requireNonNull(r6)
        L82:
            b.f.d.k.c r6 = b.f.d.k.c.a
            r5.g = r6
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.f.b.b.a.<init>(java.io.File, int, b.f.b.a.a):void");
    }

    public static d h(a aVar, File file) {
        d dVar;
        Objects.requireNonNull(aVar);
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(46);
        if (lastIndexOf > 0) {
            String substring = name.substring(lastIndexOf);
            String str = ".cnt";
            if (!str.equals(substring)) {
                str = ".tmp".equals(substring) ? ".tmp" : null;
            }
            if (str != null) {
                String substring2 = name.substring(0, lastIndexOf);
                if (str.equals(".tmp")) {
                    int lastIndexOf2 = substring2.lastIndexOf(46);
                    if (lastIndexOf2 > 0) {
                        substring2 = substring2.substring(0, lastIndexOf2);
                    }
                }
                dVar = new d(str, substring2);
                if (dVar == null && new File(aVar.k(dVar.f456b)).equals(file.getParentFile())) {
                    return dVar;
                }
                return null;
            }
        }
        dVar = null;
        if (dVar == null) {
            return null;
        }
        return dVar;
    }

    @Override // b.f.b.b.c
    public void a() {
        b.c.a.a0.d.o2(this.c, new g(null));
    }

    @Override // b.f.b.b.c
    public c.b b(String str, Object obj) throws IOException {
        File file = new File(k(str));
        if (!file.exists()) {
            try {
                b.c.a.a0.d.l1(file);
            } catch (FileUtils$CreateDirectoryException e2) {
                Objects.requireNonNull((b.f.b.a.d) this.f);
                throw e2;
            }
        }
        try {
            return new f(str, File.createTempFile(str + ".", ".tmp", file));
        } catch (IOException e3) {
            Objects.requireNonNull((b.f.b.a.d) this.f);
            throw e3;
        }
    }

    @Override // b.f.b.b.c
    public boolean c(String str, Object obj) {
        File j = j(str);
        boolean exists = j.exists();
        if (exists) {
            Objects.requireNonNull((b.f.d.k.c) this.g);
            j.setLastModified(System.currentTimeMillis());
        }
        return exists;
    }

    @Override // b.f.b.b.c
    public b.f.a.a d(String str, Object obj) {
        File j = j(str);
        if (!j.exists()) {
            return null;
        }
        Objects.requireNonNull((b.f.d.k.c) this.g);
        j.setLastModified(System.currentTimeMillis());
        return new b.f.a.a(j);
    }

    @Override // b.f.b.b.c
    public Collection e() throws IOException {
        b bVar = new b(null);
        b.c.a.a0.d.o2(this.e, bVar);
        return Collections.unmodifiableList(bVar.a);
    }

    @Override // b.f.b.b.c
    public long f(String str) {
        return i(j(str));
    }

    @Override // b.f.b.b.c
    public long g(c.a aVar) {
        return i(((c) aVar).f455b.a);
    }

    public final long i(File file) {
        if (!file.exists()) {
            return 0L;
        }
        long length = file.length();
        if (file.delete()) {
            return length;
        }
        return -1L;
    }

    @Override // b.f.b.b.c
    public boolean isExternal() {
        return this.d;
    }

    @VisibleForTesting
    public File j(String str) {
        return new File(b.d.b.a.a.J(b.d.b.a.a.R(k(str)), File.separator, str, ".cnt"));
    }

    public final String k(String str) {
        String valueOf = String.valueOf(Math.abs(str.hashCode() % 100));
        StringBuilder sb = new StringBuilder();
        sb.append(this.e);
        return b.d.b.a.a.H(sb, File.separator, valueOf);
    }
}
