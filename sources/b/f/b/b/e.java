package b.f.b.b;

import androidx.annotation.VisibleForTesting;
import b.c.a.a0.d;
import b.f.b.b.c;
import com.facebook.common.file.FileUtils$CreateDirectoryException;
import com.facebook.common.internal.Supplier;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Objects;
/* compiled from: DynamicDefaultDiskStorage.java */
/* loaded from: classes2.dex */
public class e implements c {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final Supplier<File> f462b;
    public final String c;
    public final b.f.b.a.a d;
    @VisibleForTesting
    public volatile a e = new a(null, null);

    /* compiled from: DynamicDefaultDiskStorage.java */
    @VisibleForTesting
    /* loaded from: classes2.dex */
    public static class a {
        public final c a;

        /* renamed from: b  reason: collision with root package name */
        public final File f463b;

        @VisibleForTesting
        public a(File file, c cVar) {
            this.a = cVar;
            this.f463b = file;
        }
    }

    public e(int i, Supplier<File> supplier, String str, b.f.b.a.a aVar) {
        this.a = i;
        this.d = aVar;
        this.f462b = supplier;
        this.c = str;
    }

    @Override // b.f.b.b.c
    public void a() {
        try {
            i().a();
        } catch (IOException e) {
            b.f.d.e.a.b(e.class, "purgeUnexpectedResources", e);
        }
    }

    @Override // b.f.b.b.c
    public c.b b(String str, Object obj) throws IOException {
        return i().b(str, obj);
    }

    @Override // b.f.b.b.c
    public boolean c(String str, Object obj) throws IOException {
        return i().c(str, obj);
    }

    @Override // b.f.b.b.c
    public b.f.a.a d(String str, Object obj) throws IOException {
        return i().d(str, obj);
    }

    @Override // b.f.b.b.c
    public Collection<c.a> e() throws IOException {
        return i().e();
    }

    @Override // b.f.b.b.c
    public long f(String str) throws IOException {
        return i().f(str);
    }

    @Override // b.f.b.b.c
    public long g(c.a aVar) throws IOException {
        return i().g(aVar);
    }

    public final void h() throws IOException {
        File file = new File(this.f462b.get(), this.c);
        try {
            d.l1(file);
            file.getAbsolutePath();
            int i = b.f.d.e.a.a;
            this.e = new a(file, new b.f.b.b.a(file, this.a, this.d));
        } catch (FileUtils$CreateDirectoryException e) {
            Objects.requireNonNull((b.f.b.a.d) this.d);
            throw e;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:13:0x0017 A[Catch: all -> 0x0036, TryCatch #0 {, blocks: (B:3:0x0001, B:5:0x0007, B:7:0x000b, B:13:0x0017, B:15:0x001d, B:17:0x0023, B:18:0x002a, B:19:0x002d), top: B:24:0x0001 }] */
    @androidx.annotation.VisibleForTesting
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public synchronized b.f.b.b.c i() throws java.io.IOException {
        /*
            r2 = this;
            monitor-enter(r2)
            b.f.b.b.e$a r0 = r2.e     // Catch: java.lang.Throwable -> L36
            b.f.b.b.c r1 = r0.a     // Catch: java.lang.Throwable -> L36
            if (r1 == 0) goto L14
            java.io.File r0 = r0.f463b     // Catch: java.lang.Throwable -> L36
            if (r0 == 0) goto L14
            boolean r0 = r0.exists()     // Catch: java.lang.Throwable -> L36
            if (r0 != 0) goto L12
            goto L14
        L12:
            r0 = 0
            goto L15
        L14:
            r0 = 1
        L15:
            if (r0 == 0) goto L2d
            b.f.b.b.e$a r0 = r2.e     // Catch: java.lang.Throwable -> L36
            b.f.b.b.c r0 = r0.a     // Catch: java.lang.Throwable -> L36
            if (r0 == 0) goto L2a
            b.f.b.b.e$a r0 = r2.e     // Catch: java.lang.Throwable -> L36
            java.io.File r0 = r0.f463b     // Catch: java.lang.Throwable -> L36
            if (r0 == 0) goto L2a
            b.f.b.b.e$a r0 = r2.e     // Catch: java.lang.Throwable -> L36
            java.io.File r0 = r0.f463b     // Catch: java.lang.Throwable -> L36
            b.c.a.a0.d.Z(r0)     // Catch: java.lang.Throwable -> L36
        L2a:
            r2.h()     // Catch: java.lang.Throwable -> L36
        L2d:
            b.f.b.b.e$a r0 = r2.e     // Catch: java.lang.Throwable -> L36
            b.f.b.b.c r0 = r0.a     // Catch: java.lang.Throwable -> L36
            java.util.Objects.requireNonNull(r0)     // Catch: java.lang.Throwable -> L36
            monitor-exit(r2)
            return r0
        L36:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.f.b.b.e.i():b.f.b.b.c");
    }

    @Override // b.f.b.b.c
    public boolean isExternal() {
        try {
            return i().isExternal();
        } catch (IOException unused) {
            return false;
        }
    }
}
