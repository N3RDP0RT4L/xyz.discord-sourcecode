package b.f.b.b;

import b.f.b.b.c;
/* compiled from: DefaultEntryEvictionComparatorSupplier.java */
/* loaded from: classes2.dex */
public class b implements g {

    /* compiled from: DefaultEntryEvictionComparatorSupplier.java */
    /* loaded from: classes2.dex */
    public class a implements f {
        public a(b bVar) {
        }

        @Override // java.util.Comparator
        public int compare(c.a aVar, c.a aVar2) {
            long a = aVar.a();
            long a2 = aVar2.a();
            if (a < a2) {
                return -1;
            }
            return a2 == a ? 0 : 1;
        }
    }

    @Override // b.f.b.b.g
    public f get() {
        return new a(this);
    }
}
