package b.f.n.a.a;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.style.ReplacementSpan;
import androidx.annotation.Nullable;
/* compiled from: BetterImageSpan.java */
/* loaded from: classes2.dex */
public class a extends ReplacementSpan {
    public int j;
    public int k;
    public Rect l;
    public final int m;
    public final Paint.FontMetricsInt n = new Paint.FontMetricsInt();
    public final Drawable o;

    public a(Drawable drawable, int i) {
        this.o = drawable;
        this.m = i;
        b();
    }

    public final int a(Paint.FontMetricsInt fontMetricsInt) {
        int i = this.m;
        if (i == 0) {
            return fontMetricsInt.descent - this.k;
        }
        if (i != 2) {
            return -this.k;
        }
        int i2 = fontMetricsInt.descent;
        int i3 = fontMetricsInt.ascent;
        return (((i2 - i3) - this.k) / 2) + i3;
    }

    public void b() {
        Rect bounds = this.o.getBounds();
        this.l = bounds;
        this.j = bounds.width();
        this.k = this.l.height();
    }

    @Override // android.text.style.ReplacementSpan
    public void draw(Canvas canvas, CharSequence charSequence, int i, int i2, float f, int i3, int i4, int i5, Paint paint) {
        paint.getFontMetricsInt(this.n);
        int a = a(this.n) + i4;
        canvas.translate(f, a);
        this.o.draw(canvas);
        canvas.translate(-f, -a);
    }

    @Override // android.text.style.ReplacementSpan
    public int getSize(Paint paint, CharSequence charSequence, int i, int i2, @Nullable Paint.FontMetricsInt fontMetricsInt) {
        b();
        if (fontMetricsInt == null) {
            return this.j;
        }
        int a = a(fontMetricsInt);
        int i3 = this.k + a;
        if (a < fontMetricsInt.ascent) {
            fontMetricsInt.ascent = a;
        }
        if (a < fontMetricsInt.top) {
            fontMetricsInt.top = a;
        }
        if (i3 > fontMetricsInt.descent) {
            fontMetricsInt.descent = i3;
        }
        if (i3 > fontMetricsInt.bottom) {
            fontMetricsInt.bottom = i3;
        }
        return this.j;
    }
}
