package b.f.e;

import android.util.Pair;
import com.facebook.datasource.DataSource;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
/* compiled from: AbstractDataSource.java */
/* loaded from: classes2.dex */
public abstract class c<T> implements DataSource<T> {
    public Map<String, Object> a;
    public T d = null;
    public Throwable e = null;
    public float f = 0.0f;
    public boolean c = false;

    /* renamed from: b  reason: collision with root package name */
    public int f471b = 1;
    public final ConcurrentLinkedQueue<Pair<f<T>, Executor>> g = new ConcurrentLinkedQueue<>();

    @Override // com.facebook.datasource.DataSource
    public Map<String, Object> a() {
        return this.a;
    }

    @Override // com.facebook.datasource.DataSource
    public synchronized boolean b() {
        return this.d != null;
    }

    @Override // com.facebook.datasource.DataSource
    public synchronized boolean c() {
        boolean z2;
        z2 = true;
        if (this.f471b == 1) {
            z2 = false;
        }
        return z2;
    }

    @Override // com.facebook.datasource.DataSource
    public boolean close() {
        synchronized (this) {
            if (this.c) {
                return false;
            }
            this.c = true;
            T t = this.d;
            this.d = null;
            if (t != null) {
                g(t);
            }
            if (!c()) {
                j();
            }
            synchronized (this) {
                this.g.clear();
            }
            return true;
        }
    }

    @Override // com.facebook.datasource.DataSource
    public synchronized Throwable d() {
        return this.e;
    }

    @Override // com.facebook.datasource.DataSource
    public boolean e() {
        return false;
    }

    @Override // com.facebook.datasource.DataSource
    public void f(f<T> fVar, Executor executor) {
        Objects.requireNonNull(executor);
        synchronized (this) {
            if (!this.c) {
                boolean z2 = true;
                if (this.f471b == 1) {
                    this.g.add(Pair.create(fVar, executor));
                }
                if (!b() && !c() && !n()) {
                    z2 = false;
                }
                if (z2) {
                    executor.execute(new a(this, h(), fVar, n()));
                }
            }
        }
    }

    public void g(T t) {
    }

    @Override // com.facebook.datasource.DataSource
    public synchronized float getProgress() {
        return this.f;
    }

    @Override // com.facebook.datasource.DataSource
    public synchronized T getResult() {
        return this.d;
    }

    public synchronized boolean h() {
        return this.f471b == 3;
    }

    public synchronized boolean i() {
        return this.c;
    }

    public final void j() {
        boolean h = h();
        boolean n = n();
        Iterator<Pair<f<T>, Executor>> it = this.g.iterator();
        while (it.hasNext()) {
            Pair<f<T>, Executor> next = it.next();
            ((Executor) next.second).execute(new a(this, h, (f) next.first, n));
        }
    }

    public boolean k(Throwable th, Map<String, Object> map) {
        boolean z2;
        synchronized (this) {
            z2 = true;
            if (!this.c && this.f471b == 1) {
                this.f471b = 3;
                this.e = th;
                this.a = map;
            }
            z2 = false;
        }
        if (z2) {
            j();
        }
        return z2;
    }

    public boolean l(float f) {
        boolean z2;
        synchronized (this) {
            z2 = false;
            if (!this.c && this.f471b == 1) {
                if (f >= this.f) {
                    this.f = f;
                    z2 = true;
                }
            }
        }
        if (z2) {
            Iterator<Pair<f<T>, Executor>> it = this.g.iterator();
            while (it.hasNext()) {
                Pair<f<T>, Executor> next = it.next();
                ((Executor) next.second).execute(new b(this, (f) next.first));
            }
        }
        return z2;
    }

    /* JADX WARN: Code restructure failed: missing block: B:22:0x0028, code lost:
        if (r4 != null) goto L23;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public boolean m(T r4, boolean r5, java.util.Map<java.lang.String, java.lang.Object> r6) {
        /*
            r3 = this;
            r3.a = r6
            r6 = 0
            monitor-enter(r3)     // Catch: java.lang.Throwable -> L42
            boolean r0 = r3.c     // Catch: java.lang.Throwable -> L39
            r1 = 1
            if (r0 != 0) goto L26
            int r0 = r3.f471b     // Catch: java.lang.Throwable -> L39
            if (r0 == r1) goto Le
            goto L26
        Le:
            if (r5 == 0) goto L17
            r5 = 2
            r3.f471b = r5     // Catch: java.lang.Throwable -> L39
            r5 = 1065353216(0x3f800000, float:1.0)
            r3.f = r5     // Catch: java.lang.Throwable -> L39
        L17:
            T r5 = r3.d     // Catch: java.lang.Throwable -> L39
            if (r5 == r4) goto L21
            r3.d = r4     // Catch: java.lang.Throwable -> L1f
            r4 = r5
            goto L22
        L1f:
            r4 = move-exception
            goto L37
        L21:
            r4 = r6
        L22:
            monitor-exit(r3)     // Catch: java.lang.Throwable -> L33
            if (r4 == 0) goto L2d
            goto L2a
        L26:
            r1 = 0
            monitor-exit(r3)     // Catch: java.lang.Throwable -> L33
            if (r4 == 0) goto L2d
        L2a:
            r3.g(r4)
        L2d:
            if (r1 == 0) goto L32
            r3.j()
        L32:
            return r1
        L33:
            r5 = move-exception
            r2 = r5
            r5 = r4
            r4 = r2
        L37:
            r6 = r5
            goto L3a
        L39:
            r4 = move-exception
        L3a:
            r5 = r6
        L3b:
            monitor-exit(r3)     // Catch: java.lang.Throwable -> L40
            throw r4     // Catch: java.lang.Throwable -> L3d
        L3d:
            r4 = move-exception
            r6 = r5
            goto L43
        L40:
            r4 = move-exception
            goto L3b
        L42:
            r4 = move-exception
        L43:
            if (r6 == 0) goto L48
            r3.g(r6)
        L48:
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: b.f.e.c.m(java.lang.Object, boolean, java.util.Map):boolean");
    }

    public final synchronized boolean n() {
        boolean z2;
        if (i()) {
            if (!c()) {
                z2 = true;
            }
        }
        z2 = false;
        return z2;
    }
}
