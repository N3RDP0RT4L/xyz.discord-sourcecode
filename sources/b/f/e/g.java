package b.f.e;

import b.c.a.a0.d;
import b.f.d.d.i;
import com.facebook.common.internal.Supplier;
import com.facebook.datasource.DataSource;
import java.util.List;
/* compiled from: FirstAvailableDataSourceSupplier.java */
/* loaded from: classes2.dex */
public class g<T> implements Supplier<DataSource<T>> {
    public final List<Supplier<DataSource<T>>> a;

    /* compiled from: FirstAvailableDataSourceSupplier.java */
    /* loaded from: classes2.dex */
    public class b extends c<T> {
        public int h = 0;
        public DataSource<T> i = null;
        public DataSource<T> j = null;

        /* compiled from: FirstAvailableDataSourceSupplier.java */
        /* loaded from: classes2.dex */
        public class a implements f<T> {
            public a(a aVar) {
            }

            @Override // b.f.e.f
            public void onCancellation(DataSource<T> dataSource) {
            }

            @Override // b.f.e.f
            public void onFailure(DataSource<T> dataSource) {
                b.o(b.this, dataSource);
            }

            /* JADX WARN: Removed duplicated region for block: B:18:0x0029  */
            @Override // b.f.e.f
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public void onNewResult(com.facebook.datasource.DataSource<T> r6) {
                /*
                    r5 = this;
                    boolean r0 = r6.b()
                    if (r0 == 0) goto L41
                    b.f.e.g$b r0 = b.f.e.g.b.this
                    java.util.Objects.requireNonNull(r0)
                    r1 = r6
                    b.f.e.c r1 = (b.f.e.c) r1
                    boolean r2 = r1.c()
                    monitor-enter(r0)
                    com.facebook.datasource.DataSource<T> r3 = r0.i     // Catch: java.lang.Throwable -> L3e
                    r4 = 0
                    if (r6 != r3) goto L2d
                    com.facebook.datasource.DataSource<T> r3 = r0.j     // Catch: java.lang.Throwable -> L3e
                    if (r6 != r3) goto L1d
                    goto L2d
                L1d:
                    if (r3 == 0) goto L24
                    if (r2 == 0) goto L22
                    goto L24
                L22:
                    r3 = r4
                    goto L26
                L24:
                    r0.j = r6     // Catch: java.lang.Throwable -> L3e
                L26:
                    monitor-exit(r0)     // Catch: java.lang.Throwable -> L3e
                    if (r3 == 0) goto L2e
                    r3.close()
                    goto L2e
                L2d:
                    monitor-exit(r0)     // Catch: java.lang.Throwable -> L3e
                L2e:
                    com.facebook.datasource.DataSource r2 = r0.p()
                    if (r6 != r2) goto L4f
                    boolean r6 = r1.c()
                    java.util.Map<java.lang.String, java.lang.Object> r1 = r1.a
                    r0.m(r4, r6, r1)
                    goto L4f
                L3e:
                    r6 = move-exception
                    monitor-exit(r0)     // Catch: java.lang.Throwable -> L3e
                    throw r6
                L41:
                    r0 = r6
                    b.f.e.c r0 = (b.f.e.c) r0
                    boolean r0 = r0.c()
                    if (r0 == 0) goto L4f
                    b.f.e.g$b r0 = b.f.e.g.b.this
                    b.f.e.g.b.o(r0, r6)
                L4f:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: b.f.e.g.b.a.onNewResult(com.facebook.datasource.DataSource):void");
            }

            @Override // b.f.e.f
            public void onProgressUpdate(DataSource<T> dataSource) {
                b.this.l(Math.max(b.this.getProgress(), ((c) dataSource).getProgress()));
            }
        }

        public b() {
            if (!q()) {
                k(new RuntimeException("No data source supplier or supplier returned null."), null);
            }
        }

        public static void o(b bVar, DataSource dataSource) {
            boolean z2;
            synchronized (bVar) {
                if (!bVar.i() && dataSource == bVar.i) {
                    bVar.i = null;
                    z2 = true;
                }
                z2 = false;
            }
            if (z2) {
                if (!(dataSource == bVar.p() || dataSource == null)) {
                    dataSource.close();
                }
                if (!bVar.q()) {
                    bVar.k(dataSource.d(), dataSource.a());
                }
            }
        }

        @Override // b.f.e.c, com.facebook.datasource.DataSource
        public synchronized boolean b() {
            boolean z2;
            DataSource<T> p = p();
            if (p != null) {
                if (p.b()) {
                    z2 = true;
                }
            }
            z2 = false;
            return z2;
        }

        @Override // b.f.e.c, com.facebook.datasource.DataSource
        public boolean close() {
            synchronized (this) {
                if (!super.close()) {
                    return false;
                }
                DataSource<T> dataSource = this.i;
                this.i = null;
                DataSource<T> dataSource2 = this.j;
                this.j = null;
                if (dataSource2 != null) {
                    dataSource2.close();
                }
                if (dataSource == null) {
                    return true;
                }
                dataSource.close();
                return true;
            }
        }

        @Override // b.f.e.c, com.facebook.datasource.DataSource
        public synchronized T getResult() {
            DataSource<T> p;
            p = p();
            return p != null ? p.getResult() : null;
        }

        public final synchronized DataSource<T> p() {
            return this.j;
        }

        public final boolean q() {
            Supplier<DataSource<T>> supplier;
            boolean z2;
            synchronized (this) {
                if (i() || this.h >= g.this.a.size()) {
                    supplier = null;
                } else {
                    List<Supplier<DataSource<T>>> list = g.this.a;
                    int i = this.h;
                    this.h = i + 1;
                    supplier = list.get(i);
                }
            }
            DataSource<T> dataSource = supplier != null ? supplier.get() : null;
            synchronized (this) {
                if (i()) {
                    z2 = false;
                } else {
                    this.i = dataSource;
                    z2 = true;
                }
            }
            if (!z2 || dataSource == null) {
                if (dataSource != null) {
                    dataSource.close();
                }
                return false;
            }
            dataSource.f(new a(null), b.f.d.b.a.j);
            return true;
        }
    }

    public g(List<Supplier<DataSource<T>>> list) {
        d.k(!list.isEmpty(), "List of suppliers is empty!");
        this.a = list;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof g)) {
            return false;
        }
        return d.g0(this.a, ((g) obj).a);
    }

    @Override // com.facebook.common.internal.Supplier
    public Object get() {
        return new b();
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    public String toString() {
        i h2 = d.h2(this);
        h2.c("list", this.a);
        return h2.toString();
    }
}
