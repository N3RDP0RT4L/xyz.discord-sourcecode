package b.f.e;

import b.c.a.a0.d;
import com.facebook.common.internal.Supplier;
import com.facebook.datasource.DataSource;
/* compiled from: DataSources.java */
/* loaded from: classes2.dex */
public final class e implements Supplier<DataSource<T>> {
    public final /* synthetic */ Throwable a;

    public e(Throwable th) {
        this.a = th;
    }

    @Override // com.facebook.common.internal.Supplier
    public Object get() {
        return d.N0(this.a);
    }
}
