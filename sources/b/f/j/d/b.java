package b.f.j.d;

import android.graphics.Bitmap;
import b.c.a.a0.d;
import b.d.b.a.a;
import b.f.d.d.i;
/* compiled from: ImageDecodeOptions.java */
/* loaded from: classes2.dex */
public class b {
    public static final b a = new b(new c());

    /* renamed from: b  reason: collision with root package name */
    public final int f560b = 100;
    public final int c = Integer.MAX_VALUE;
    public final Bitmap.Config d;
    public final Bitmap.Config e;

    public b(c cVar) {
        this.d = cVar.a;
        this.e = cVar.f561b;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || b.class != obj.getClass()) {
            return false;
        }
        b bVar = (b) obj;
        return this.f560b == bVar.f560b && this.c == bVar.c && this.d == bVar.d && this.e == bVar.e;
    }

    public int hashCode() {
        int ordinal = (this.d.ordinal() + (((((((((((this.f560b * 31) + this.c) * 31) + 0) * 31) + 0) * 31) + 0) * 31) + 0) * 31)) * 31;
        Bitmap.Config config = this.e;
        return ((((((ordinal + (config != null ? config.ordinal() : 0)) * 31) + 0) * 31) + 0) * 31) + 0;
    }

    public String toString() {
        StringBuilder R = a.R("ImageDecodeOptions{");
        i h2 = d.h2(this);
        h2.a("minDecodeIntervalMs", this.f560b);
        h2.a("maxDimensionPx", this.c);
        h2.b("decodePreviewFrame", false);
        h2.b("useLastFrameForPreview", false);
        h2.b("decodeAllFrames", false);
        h2.b("forceStaticImage", false);
        h2.c("bitmapConfigName", this.d.name());
        h2.c("animatedBitmapConfigName", this.e.name());
        h2.c("customImageDecoder", null);
        h2.c("bitmapTransformation", null);
        h2.c("colorSpace", null);
        return a.H(R, h2.toString(), "}");
    }
}
