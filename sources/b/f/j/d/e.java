package b.f.j.d;

import b.c.a.a0.d;
/* compiled from: ResizeOptions.java */
/* loaded from: classes2.dex */
public class e {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final int f562b;
    public final float c;
    public final float d;

    public e(int i, int i2) {
        boolean z2 = true;
        d.i(Boolean.valueOf(i > 0));
        d.i(Boolean.valueOf(i2 <= 0 ? false : z2));
        this.a = i;
        this.f562b = i2;
        this.c = 2048.0f;
        this.d = 0.6666667f;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof e)) {
            return false;
        }
        e eVar = (e) obj;
        return this.a == eVar.a && this.f562b == eVar.f562b;
    }

    public int hashCode() {
        return d.J0(this.a, this.f562b);
    }

    public String toString() {
        return String.format(null, "%dx%d", Integer.valueOf(this.a), Integer.valueOf(this.f562b));
    }
}
