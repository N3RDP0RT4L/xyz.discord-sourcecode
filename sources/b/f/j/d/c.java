package b.f.j.d;

import android.graphics.Bitmap;
import b.f.j.d.c;
/* compiled from: ImageDecodeOptionsBuilder.java */
/* loaded from: classes2.dex */
public class c<T extends c> {
    public Bitmap.Config a;

    /* renamed from: b  reason: collision with root package name */
    public Bitmap.Config f561b;

    public c() {
        Bitmap.Config config = Bitmap.Config.ARGB_8888;
        this.a = config;
        this.f561b = config;
    }
}
