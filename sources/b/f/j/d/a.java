package b.f.j.d;

import b.c.a.a0.d;
/* compiled from: BytesRange.java */
/* loaded from: classes2.dex */
public class a {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final int f559b;

    public a(int i, int i2) {
        this.a = i;
        this.f559b = i2;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof a)) {
            return false;
        }
        a aVar = (a) obj;
        return this.a == aVar.a && this.f559b == aVar.f559b;
    }

    public int hashCode() {
        return d.J0(this.a, this.f559b);
    }

    public String toString() {
        Object[] objArr = new Object[2];
        int i = this.a;
        String str = "";
        objArr[0] = i == Integer.MAX_VALUE ? str : Integer.toString(i);
        int i2 = this.f559b;
        if (i2 != Integer.MAX_VALUE) {
            str = Integer.toString(i2);
        }
        objArr[1] = str;
        return String.format(null, "%s-%s", objArr);
    }
}
