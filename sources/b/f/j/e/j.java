package b.f.j.e;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import androidx.appcompat.widget.ActivityChooserModel;
import b.f.d.g.c;
import b.f.j.c.i;
import b.f.j.c.k;
import b.f.j.c.m;
import b.f.j.c.n;
import b.f.j.c.o;
import b.f.j.c.r;
import b.f.j.c.w;
import b.f.j.c.z;
import b.f.j.e.l;
import b.f.j.h.d;
import b.f.j.h.f;
import b.f.j.k.e;
import b.f.j.l.w;
import b.f.j.l.x;
import b.f.j.p.a0;
import b.f.j.p.o0;
import com.facebook.cache.common.CacheKey;
import com.facebook.cache.disk.DiskCacheConfig;
import com.facebook.common.internal.Supplier;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.imagepipeline.cache.DefaultBitmapMemoryCacheParamsSupplier;
import com.facebook.imagepipeline.cache.MemoryCacheParams;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
/* compiled from: ImagePipelineConfig.java */
/* loaded from: classes2.dex */
public class j implements k {
    public static b a = new b(null);

    /* renamed from: b  reason: collision with root package name */
    public final Supplier<MemoryCacheParams> f566b;
    public final w.a c;
    public final i d;
    public final Context e;
    public final boolean f;
    public final f g;
    public final Supplier<MemoryCacheParams> h;
    public final e i;
    public final r j;
    public final Supplier<Boolean> k;
    public final DiskCacheConfig l;
    public final c m;
    public final o0 n;
    public final int o;
    public final x p;
    public final d q;
    public final Set<e> r;

    /* renamed from: s  reason: collision with root package name */
    public final Set<b.f.j.k.d> f567s;
    public final boolean t;
    public final DiskCacheConfig u;
    public final l v;
    public final boolean w;

    /* renamed from: x  reason: collision with root package name */
    public final b.f.j.g.a f568x;

    /* renamed from: y  reason: collision with root package name */
    public final b.f.j.c.a f569y;

    /* compiled from: ImagePipelineConfig.java */
    /* loaded from: classes2.dex */
    public static class a {
        public Supplier<MemoryCacheParams> a;

        /* renamed from: b  reason: collision with root package name */
        public final Context f570b;
        public DiskCacheConfig d;
        public DiskCacheConfig e;
        public boolean c = false;
        public final l.b f = new l.b(this);
        public boolean g = true;
        public b.f.j.g.a h = new b.f.j.g.a();

        public a(Context context, i iVar) {
            Objects.requireNonNull(context);
            this.f570b = context;
        }
    }

    /* compiled from: ImagePipelineConfig.java */
    /* loaded from: classes2.dex */
    public static class b {
        public b(i iVar) {
        }
    }

    public j(a aVar, i iVar) {
        n nVar;
        z zVar;
        b.f.j.r.b.b();
        this.v = new l(aVar.f, null);
        Supplier<MemoryCacheParams> supplier = aVar.a;
        if (supplier == null) {
            Object systemService = aVar.f570b.getSystemService(ActivityChooserModel.ATTRIBUTE_ACTIVITY);
            Objects.requireNonNull(systemService);
            supplier = new DefaultBitmapMemoryCacheParamsSupplier((ActivityManager) systemService);
        }
        this.f566b = supplier;
        this.c = new b.f.j.c.c();
        Bitmap.Config config = Bitmap.Config.ARGB_8888;
        synchronized (n.class) {
            if (n.a == null) {
                n.a = new n();
            }
            nVar = n.a;
        }
        this.d = nVar;
        Context context = aVar.f570b;
        Objects.requireNonNull(context);
        this.e = context;
        this.g = new c(new d());
        this.f = aVar.c;
        this.h = new o();
        synchronized (z.class) {
            if (z.a == null) {
                z.a = new z();
            }
            zVar = z.a;
        }
        this.j = zVar;
        this.k = new i(this);
        DiskCacheConfig diskCacheConfig = aVar.d;
        if (diskCacheConfig == null) {
            Context context2 = aVar.f570b;
            try {
                b.f.j.r.b.b();
                diskCacheConfig = new DiskCacheConfig(new DiskCacheConfig.b(context2, null));
                b.f.j.r.b.b();
            } finally {
                b.f.j.r.b.b();
            }
        }
        this.l = diskCacheConfig;
        this.m = b.f.d.g.d.b();
        this.o = 30000;
        b.f.j.r.b.b();
        this.n = new a0(30000);
        b.f.j.r.b.b();
        x xVar = new x(new b.f.j.l.w(new w.b(null), null));
        this.p = xVar;
        this.q = new f();
        this.r = new HashSet();
        this.f567s = new HashSet();
        this.t = true;
        DiskCacheConfig diskCacheConfig2 = aVar.e;
        this.u = diskCacheConfig2 != null ? diskCacheConfig2 : diskCacheConfig;
        this.i = new b.f.j.e.b(xVar.b());
        this.w = aVar.g;
        this.f568x = aVar.h;
        this.f569y = new k();
    }

    @Override // b.f.j.e.k
    public Supplier<MemoryCacheParams> A() {
        return this.f566b;
    }

    @Override // b.f.j.e.k
    public b.f.j.h.b B() {
        return null;
    }

    @Override // b.f.j.e.k
    public Supplier<MemoryCacheParams> C() {
        return this.h;
    }

    @Override // b.f.j.e.k
    public e D() {
        return this.i;
    }

    @Override // b.f.j.e.k
    public x a() {
        return this.p;
    }

    @Override // b.f.j.e.k
    public Set<b.f.j.k.d> b() {
        return Collections.unmodifiableSet(this.f567s);
    }

    @Override // b.f.j.e.k
    public int c() {
        return 0;
    }

    @Override // b.f.j.e.k
    public Supplier<Boolean> d() {
        return this.k;
    }

    @Override // b.f.j.e.k
    public f e() {
        return this.g;
    }

    @Override // b.f.j.e.k
    public b.f.j.g.a f() {
        return this.f568x;
    }

    @Override // b.f.j.e.k
    public b.f.j.c.a g() {
        return this.f569y;
    }

    @Override // b.f.j.e.k
    public Context getContext() {
        return this.e;
    }

    @Override // b.f.j.e.k
    public l getExperiments() {
        return this.v;
    }

    @Override // b.f.j.e.k
    public o0 h() {
        return this.n;
    }

    @Override // b.f.j.e.k
    public b.f.j.c.w<CacheKey, PooledByteBuffer> i() {
        return null;
    }

    @Override // b.f.j.e.k
    public DiskCacheConfig j() {
        return this.l;
    }

    @Override // b.f.j.e.k
    public Set<e> k() {
        return Collections.unmodifiableSet(this.r);
    }

    @Override // b.f.j.e.k
    public i l() {
        return this.d;
    }

    @Override // b.f.j.e.k
    public boolean m() {
        return this.t;
    }

    @Override // b.f.j.e.k
    public w.a n() {
        return this.c;
    }

    @Override // b.f.j.e.k
    public d o() {
        return this.q;
    }

    @Override // b.f.j.e.k
    public DiskCacheConfig p() {
        return this.u;
    }

    @Override // b.f.j.e.k
    public r q() {
        return this.j;
    }

    @Override // b.f.j.e.k
    public m.b<CacheKey> r() {
        return null;
    }

    @Override // b.f.j.e.k
    public boolean s() {
        return this.f;
    }

    @Override // b.f.j.e.k
    public b.f.d.b.f t() {
        return null;
    }

    @Override // b.f.j.e.k
    public Integer u() {
        return null;
    }

    @Override // b.f.j.e.k
    public b.f.j.s.c v() {
        return null;
    }

    @Override // b.f.j.e.k
    public c w() {
        return this.m;
    }

    @Override // b.f.j.e.k
    public b.f.j.h.c x() {
        return null;
    }

    @Override // b.f.j.e.k
    public boolean y() {
        return this.w;
    }

    @Override // b.f.j.e.k
    public b.f.c.a z() {
        return null;
    }
}
