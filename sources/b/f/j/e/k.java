package b.f.j.e;

import android.content.Context;
import b.f.d.b.f;
import b.f.j.c.i;
import b.f.j.c.m;
import b.f.j.c.r;
import b.f.j.c.w;
import b.f.j.g.a;
import b.f.j.h.b;
import b.f.j.k.d;
import b.f.j.k.e;
import b.f.j.l.x;
import b.f.j.p.o0;
import b.f.j.s.c;
import com.facebook.cache.common.CacheKey;
import com.facebook.cache.disk.DiskCacheConfig;
import com.facebook.common.internal.Supplier;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.imagepipeline.cache.MemoryCacheParams;
import java.util.Set;
/* compiled from: ImagePipelineConfigInterface.java */
/* loaded from: classes2.dex */
public interface k {
    Supplier<MemoryCacheParams> A();

    b B();

    Supplier<MemoryCacheParams> C();

    e D();

    x a();

    Set<d> b();

    int c();

    Supplier<Boolean> d();

    f e();

    a f();

    b.f.j.c.a g();

    Context getContext();

    l getExperiments();

    o0 h();

    w<CacheKey, PooledByteBuffer> i();

    DiskCacheConfig j();

    Set<e> k();

    i l();

    boolean m();

    w.a n();

    b.f.j.h.d o();

    DiskCacheConfig p();

    r q();

    m.b<CacheKey> r();

    boolean s();

    f t();

    Integer u();

    c v();

    b.f.d.g.c w();

    b.f.j.h.c x();

    boolean y();

    b.f.c.a z();
}
