package b.f.j.e;

import android.content.ContentResolver;
import android.content.Context;
import android.os.Build;
import androidx.core.util.Pools;
import b.f.b.b.h;
import b.f.d.g.g;
import b.f.j.a.b.a;
import b.f.j.c.f;
import b.f.j.c.i;
import b.f.j.c.j;
import b.f.j.c.k;
import b.f.j.c.m;
import b.f.j.c.p;
import b.f.j.c.q;
import b.f.j.c.r;
import b.f.j.c.s;
import b.f.j.c.t;
import b.f.j.c.v;
import b.f.j.c.w;
import b.f.j.c.y;
import b.f.j.c.z;
import b.f.j.e.l;
import b.f.j.h.b;
import b.f.j.j.c;
import b.f.j.l.x;
import b.f.j.n.d;
import b.f.j.p.g1;
import b.f.j.p.h1;
import b.f.j.p.o0;
import b.f.j.s.e;
import com.facebook.cache.common.CacheKey;
import com.facebook.common.internal.Supplier;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.common.references.CloseableReference;
import com.facebook.imagepipeline.bitmaps.PlatformBitmapFactory;
import com.facebook.imagepipeline.cache.MemoryCacheParams;
import java.util.Objects;
import java.util.Set;
/* compiled from: ImagePipelineFactory.java */
/* loaded from: classes2.dex */
public class m {
    public static m a;

    /* renamed from: b  reason: collision with root package name */
    public final g1 f573b;
    public final k c;
    public final a d;
    public b.f.j.c.m<CacheKey, c> e;
    public s<CacheKey, c> f;
    public b.f.j.c.m<CacheKey, PooledByteBuffer> g;
    public s<CacheKey, PooledByteBuffer> h;
    public f i;
    public h j;
    public b k;
    public h l;
    public b.f.j.s.c m;
    public p n;
    public q o;
    public f p;
    public h q;
    public PlatformBitmapFactory r;

    /* renamed from: s  reason: collision with root package name */
    public d f574s;
    public a t;

    public m(k kVar) {
        b.f.j.r.b.b();
        Objects.requireNonNull(kVar);
        this.c = kVar;
        Objects.requireNonNull(kVar.getExperiments());
        this.f573b = new h1(kVar.D().b());
        Objects.requireNonNull(kVar.getExperiments());
        CloseableReference.k = 0;
        this.d = new a(kVar.f());
        b.f.j.r.b.b();
    }

    public static synchronized void j(k kVar) {
        synchronized (m.class) {
            if (a != null) {
                b.f.d.e.a.k(m.class, "ImagePipelineFactory has already been initialized! `ImagePipelineFactory.initialize(...)` should only be called once to avoid unexpected behavior.");
            }
            a = new m(kVar);
        }
    }

    public final h a() {
        b bVar;
        b bVar2;
        if (Build.VERSION.SDK_INT >= 24) {
            Objects.requireNonNull(this.c.getExperiments());
        }
        if (this.o == null) {
            ContentResolver contentResolver = this.c.getContext().getApplicationContext().getContentResolver();
            if (this.n == null) {
                l.d dVar = this.c.getExperiments().f571b;
                Context context = this.c.getContext();
                b.f.d.g.a e = this.c.a().e();
                if (this.k == null) {
                    if (this.c.B() != null) {
                        this.k = this.c.B();
                    } else {
                        a b2 = b();
                        if (b2 != null) {
                            bVar = b2.b();
                            bVar2 = b2.c();
                        } else {
                            bVar2 = null;
                            bVar = null;
                        }
                        if (this.c.x() == null) {
                            this.k = new b.f.j.h.a(bVar, bVar2, h());
                        } else {
                            h();
                            Objects.requireNonNull(this.c.x());
                            throw null;
                        }
                    }
                }
                b bVar3 = this.k;
                b.f.j.h.d o = this.c.o();
                boolean s2 = this.c.s();
                boolean m = this.c.m();
                Objects.requireNonNull(this.c.getExperiments());
                e D = this.c.D();
                g c = this.c.a().c(this.c.c());
                this.c.a().d();
                s<CacheKey, c> d = d();
                s<CacheKey, PooledByteBuffer> e2 = e();
                f f = f();
                f i = i();
                i l = this.c.l();
                PlatformBitmapFactory g = g();
                Objects.requireNonNull(this.c.getExperiments());
                Objects.requireNonNull(this.c.getExperiments());
                Objects.requireNonNull(this.c.getExperiments());
                int i2 = this.c.getExperiments().a;
                a aVar = this.d;
                Objects.requireNonNull(this.c.getExperiments());
                int i3 = this.c.getExperiments().g;
                Objects.requireNonNull((l.c) dVar);
                this.n = new p(context, e, bVar3, o, s2, m, false, D, c, d, e2, f, i, l, g, 0, 0, false, i2, aVar, false, i3);
            }
            p pVar = this.n;
            o0 h = this.c.h();
            boolean m2 = this.c.m();
            Objects.requireNonNull(this.c.getExperiments());
            g1 g1Var = this.f573b;
            boolean s3 = this.c.s();
            Objects.requireNonNull(this.c.getExperiments());
            boolean y2 = this.c.y();
            if (this.m == null) {
                if (this.c.v() == null && this.c.u() == null) {
                    Objects.requireNonNull(this.c.getExperiments());
                }
                int i4 = this.c.getExperiments().a;
                Objects.requireNonNull(this.c.getExperiments());
                this.m = new e(i4, false, this.c.v(), this.c.u(), this.c.getExperiments().f);
            }
            b.f.j.s.c cVar = this.m;
            Objects.requireNonNull(this.c.getExperiments());
            Objects.requireNonNull(this.c.getExperiments());
            Objects.requireNonNull(this.c.getExperiments());
            this.o = new q(contentResolver, pVar, h, m2, false, g1Var, s3, false, false, y2, cVar, false, false, false);
        }
        q qVar = this.o;
        Set<b.f.j.k.e> k = this.c.k();
        Set<b.f.j.k.d> b3 = this.c.b();
        Supplier<Boolean> d2 = this.c.d();
        s<CacheKey, c> d3 = d();
        s<CacheKey, PooledByteBuffer> e3 = e();
        f f2 = f();
        f i5 = i();
        i l2 = this.c.l();
        g1 g1Var2 = this.f573b;
        Supplier<Boolean> supplier = this.c.getExperiments().d;
        Objects.requireNonNull(this.c.getExperiments());
        return new h(qVar, k, b3, d2, d3, e3, f2, i5, l2, g1Var2, supplier, null, this.c.z(), this.c);
    }

    public final a b() {
        if (this.t == null) {
            PlatformBitmapFactory g = g();
            e D = this.c.D();
            b.f.j.c.m<CacheKey, c> c = c();
            boolean z2 = this.c.getExperiments().c;
            b.f.d.b.f t = this.c.t();
            if (!b.f.j.a.b.b.a) {
                try {
                    b.f.j.a.b.b.f542b = (a) Class.forName("com.facebook.fresco.animation.factory.AnimatedFactoryV2Impl").getConstructor(PlatformBitmapFactory.class, e.class, b.f.j.c.m.class, Boolean.TYPE, b.f.d.b.f.class).newInstance(g, D, c, Boolean.valueOf(z2), t);
                } catch (Throwable unused) {
                }
                if (b.f.j.a.b.b.f542b != null) {
                    b.f.j.a.b.b.a = true;
                }
            }
            this.t = b.f.j.a.b.b.f542b;
        }
        return this.t;
    }

    public b.f.j.c.m<CacheKey, c> c() {
        if (this.e == null) {
            b.f.j.c.a g = this.c.g();
            Supplier<MemoryCacheParams> A = this.c.A();
            b.f.d.g.c w = this.c.w();
            w.a n = this.c.n();
            Objects.requireNonNull(this.c.getExperiments());
            Objects.requireNonNull(this.c.getExperiments());
            m.b<CacheKey> r = this.c.r();
            k kVar = (k) g;
            Objects.requireNonNull(kVar);
            v vVar = new v(new j(kVar), n, A, r, false, false);
            w.a(vVar);
            this.e = vVar;
        }
        return this.e;
    }

    public s<CacheKey, c> d() {
        if (this.f == null) {
            b.f.j.c.m<CacheKey, c> c = c();
            r q = this.c.q();
            Objects.requireNonNull((z) q);
            this.f = new s<>(c, new t(q));
        }
        return this.f;
    }

    public s<CacheKey, PooledByteBuffer> e() {
        w<CacheKey, PooledByteBuffer> wVar;
        if (this.h == null) {
            if (this.c.i() != null) {
                wVar = this.c.i();
            } else {
                if (this.g == null) {
                    Supplier<MemoryCacheParams> C = this.c.C();
                    b.f.d.g.c w = this.c.w();
                    v vVar = new v(new p(), new y(), C, null, false, false);
                    w.a(vVar);
                    this.g = vVar;
                }
                wVar = this.g;
            }
            r q = this.c.q();
            Objects.requireNonNull((z) q);
            this.h = new s<>(wVar, new q(q));
        }
        return this.h;
    }

    public f f() {
        if (this.i == null) {
            if (this.j == null) {
                this.j = ((c) this.c.e()).a(this.c.j());
            }
            this.i = new f(this.j, this.c.a().c(this.c.c()), this.c.a().d(), this.c.D().e(), this.c.D().d(), this.c.q());
        }
        return this.i;
    }

    public PlatformBitmapFactory g() {
        if (this.r == null) {
            x a2 = this.c.a();
            h();
            this.r = new b.f.j.b.a(a2.a(), this.d);
        }
        return this.r;
    }

    public d h() {
        d dVar;
        if (this.f574s == null) {
            x a2 = this.c.a();
            Objects.requireNonNull(this.c.getExperiments());
            if (Build.VERSION.SDK_INT >= 26) {
                int b2 = a2.b();
                dVar = new b.f.j.n.c(a2.a(), b2, new Pools.SynchronizedPool(b2));
            } else {
                int b3 = a2.b();
                dVar = new b.f.j.n.a(a2.a(), b3, new Pools.SynchronizedPool(b3));
            }
            this.f574s = dVar;
        }
        return this.f574s;
    }

    public final f i() {
        if (this.p == null) {
            if (this.q == null) {
                this.q = ((c) this.c.e()).a(this.c.p());
            }
            this.p = new f(this.q, this.c.a().c(this.c.c()), this.c.a().d(), this.c.D().e(), this.c.D().d(), this.c.q());
        }
        return this.p;
    }
}
