package b.f.j.e;

import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
/* compiled from: ExecutorSupplier.java */
/* loaded from: classes2.dex */
public interface e {
    Executor a();

    Executor b();

    Executor c();

    Executor d();

    Executor e();

    Executor f();

    ScheduledExecutorService g();
}
