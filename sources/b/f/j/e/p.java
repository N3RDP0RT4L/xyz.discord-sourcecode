package b.f.j.e;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import b.f.d.g.a;
import b.f.d.g.g;
import b.f.j.c.f;
import b.f.j.c.i;
import b.f.j.c.w;
import b.f.j.h.b;
import b.f.j.h.d;
import b.f.j.j.c;
import b.f.j.j.e;
import b.f.j.p.c1;
import b.f.j.p.w0;
import com.facebook.cache.common.CacheKey;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.imagepipeline.bitmaps.PlatformBitmapFactory;
/* compiled from: ProducerFactory.java */
/* loaded from: classes2.dex */
public class p {
    public ContentResolver a;

    /* renamed from: b  reason: collision with root package name */
    public Resources f575b;
    public AssetManager c;
    public final a d;
    public final b e;
    public final d f;
    public final boolean g;
    public final boolean h;
    public final boolean i;
    public final e j;
    public final g k;
    public final f l;
    public final f m;
    public final w<CacheKey, PooledByteBuffer> n;
    public final w<CacheKey, c> o;
    public final i p;
    public final b.f.j.c.d<CacheKey> q;
    public final b.f.j.c.d<CacheKey> r;

    /* renamed from: s  reason: collision with root package name */
    public final PlatformBitmapFactory f576s;
    public final int t;
    public final int u;
    public boolean v;
    public final a w;

    /* renamed from: x  reason: collision with root package name */
    public final int f577x;

    /* renamed from: y  reason: collision with root package name */
    public final boolean f578y;

    public p(Context context, a aVar, b bVar, d dVar, boolean z2, boolean z3, boolean z4, e eVar, g gVar, w<CacheKey, c> wVar, w<CacheKey, PooledByteBuffer> wVar2, f fVar, f fVar2, i iVar, PlatformBitmapFactory platformBitmapFactory, int i, int i2, boolean z5, int i3, a aVar2, boolean z6, int i4) {
        this.a = context.getApplicationContext().getContentResolver();
        this.f575b = context.getApplicationContext().getResources();
        this.c = context.getApplicationContext().getAssets();
        this.d = aVar;
        this.e = bVar;
        this.f = dVar;
        this.g = z2;
        this.h = z3;
        this.i = z4;
        this.j = eVar;
        this.k = gVar;
        this.o = wVar;
        this.n = wVar2;
        this.l = fVar;
        this.m = fVar2;
        this.p = iVar;
        this.f576s = platformBitmapFactory;
        this.q = new b.f.j.c.d<>(i4);
        this.r = new b.f.j.c.d<>(i4);
        this.t = i;
        this.u = i2;
        this.v = z5;
        this.f577x = i3;
        this.w = aVar2;
        this.f578y = z6;
    }

    public c1 a(w0<e> w0Var, boolean z2, b.f.j.s.c cVar) {
        return new c1(this.j.c(), this.k, w0Var, z2, cVar);
    }
}
