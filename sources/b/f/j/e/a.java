package b.f.j.e;

import com.facebook.common.references.CloseableReference;
import com.facebook.common.references.SharedReference;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Objects;
/* compiled from: CloseableReferenceFactory.java */
/* loaded from: classes2.dex */
public class a {
    public final CloseableReference.c a;

    /* compiled from: CloseableReferenceFactory.java */
    /* renamed from: b.f.j.e.a$a  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class C0070a implements CloseableReference.c {
        public final /* synthetic */ b.f.j.g.a a;

        public C0070a(a aVar, b.f.j.g.a aVar2) {
            this.a = aVar2;
        }

        @Override // com.facebook.common.references.CloseableReference.c
        public void a(SharedReference<Object> sharedReference, Throwable th) {
            String str;
            Objects.requireNonNull(this.a);
            Object c = sharedReference.c();
            String name = c != null ? c.getClass().getName() : "<value is null>";
            Object[] objArr = new Object[4];
            objArr[0] = Integer.valueOf(System.identityHashCode(this));
            objArr[1] = Integer.valueOf(System.identityHashCode(sharedReference));
            objArr[2] = name;
            if (th == null) {
                str = "";
            } else {
                StringWriter stringWriter = new StringWriter();
                th.printStackTrace(new PrintWriter(stringWriter));
                str = stringWriter.toString();
            }
            objArr[3] = str;
            b.f.d.e.a.o("Fresco", "Finalized without closing: %x %x (type = %s).\nStack:\n%s", objArr);
        }

        @Override // com.facebook.common.references.CloseableReference.c
        public boolean b() {
            Objects.requireNonNull(this.a);
            return false;
        }
    }

    public a(b.f.j.g.a aVar) {
        this.a = new C0070a(this, aVar);
    }
}
