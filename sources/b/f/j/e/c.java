package b.f.j.e;

import b.f.b.b.d;
import b.f.b.b.e;
import b.f.b.b.h;
import com.facebook.cache.disk.DiskCacheConfig;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
/* compiled from: DiskStorageCacheFactory.java */
/* loaded from: classes2.dex */
public class c implements f {
    public d a;

    public c(d dVar) {
        this.a = dVar;
    }

    public h a(DiskCacheConfig diskCacheConfig) {
        Objects.requireNonNull(this.a);
        e eVar = new e(diskCacheConfig.a, diskCacheConfig.c, diskCacheConfig.f2854b, diskCacheConfig.h);
        ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor();
        return new d(eVar, diskCacheConfig.g, new d.c(diskCacheConfig.f, diskCacheConfig.e, diskCacheConfig.d), diskCacheConfig.i, diskCacheConfig.h, diskCacheConfig.j, newSingleThreadExecutor, false);
    }
}
