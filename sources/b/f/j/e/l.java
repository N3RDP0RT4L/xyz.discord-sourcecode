package b.f.j.e;

import b.f.d.d.k;
import b.f.j.e.j;
import com.facebook.common.internal.Supplier;
import java.util.Objects;
/* compiled from: ImagePipelineExperiments.java */
/* loaded from: classes2.dex */
public class l {
    public final boolean c;
    public final Supplier<Boolean> d;
    public final int a = 2048;

    /* renamed from: b  reason: collision with root package name */
    public final d f571b = new c();
    public boolean e = true;
    public final boolean f = true;
    public final int g = 20;

    /* compiled from: ImagePipelineExperiments.java */
    /* loaded from: classes2.dex */
    public static class b {
        public final j.a a;

        /* renamed from: b  reason: collision with root package name */
        public boolean f572b;
        public Supplier<Boolean> c = new k(Boolean.FALSE);

        public b(j.a aVar) {
            this.a = aVar;
        }
    }

    /* compiled from: ImagePipelineExperiments.java */
    /* loaded from: classes2.dex */
    public static class c implements d {
    }

    /* compiled from: ImagePipelineExperiments.java */
    /* loaded from: classes2.dex */
    public interface d {
    }

    public l(b bVar, a aVar) {
        Objects.requireNonNull(bVar);
        this.c = bVar.f572b;
        this.d = bVar.c;
    }
}
