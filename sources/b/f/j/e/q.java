package b.f.j.e;

import android.content.ContentResolver;
import android.net.Uri;
import android.os.Build;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import b.c.a.a0.d;
import b.f.d.d.l;
import b.f.j.c.w;
import b.f.j.j.e;
import b.f.j.p.a;
import b.f.j.p.b1;
import b.f.j.p.d0;
import b.f.j.p.e0;
import b.f.j.p.f;
import b.f.j.p.f0;
import b.f.j.p.f1;
import b.f.j.p.g;
import b.f.j.p.g1;
import b.f.j.p.h;
import b.f.j.p.h0;
import b.f.j.p.i;
import b.f.j.p.i0;
import b.f.j.p.i1;
import b.f.j.p.j;
import b.f.j.p.j0;
import b.f.j.p.j1;
import b.f.j.p.k;
import b.f.j.p.k0;
import b.f.j.p.k1;
import b.f.j.p.m;
import b.f.j.p.n;
import b.f.j.p.n0;
import b.f.j.p.o;
import b.f.j.p.o0;
import b.f.j.p.r0;
import b.f.j.p.s;
import b.f.j.p.s0;
import b.f.j.p.t;
import b.f.j.p.t0;
import b.f.j.p.u;
import b.f.j.p.v;
import b.f.j.p.w0;
import b.f.j.r.b;
import b.f.j.s.c;
import com.facebook.cache.common.CacheKey;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.common.references.CloseableReference;
import com.facebook.imagepipeline.producers.LocalExifThumbnailProducer;
import com.facebook.imagepipeline.request.ImageRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
/* compiled from: ProducerSequenceFactory.java */
/* loaded from: classes2.dex */
public class q {
    public final ContentResolver a;

    /* renamed from: b  reason: collision with root package name */
    public final p f579b;
    public final o0 c;
    public final boolean d;
    public final boolean e;
    public final g1 f;
    public final boolean g;
    public final boolean h;
    public final boolean i;
    public final c j;
    public final boolean k;
    public final boolean l;
    public final boolean m;
    @Nullable
    @VisibleForTesting
    public w0<CloseableReference<b.f.j.j.c>> n;
    @Nullable
    public w0<e> o;
    @Nullable
    @VisibleForTesting
    public w0<CloseableReference<b.f.j.j.c>> p;
    @Nullable
    @VisibleForTesting
    public w0<CloseableReference<b.f.j.j.c>> q;
    @Nullable
    @VisibleForTesting
    public w0<CloseableReference<b.f.j.j.c>> r;
    @Nullable
    @VisibleForTesting

    /* renamed from: s  reason: collision with root package name */
    public w0<CloseableReference<b.f.j.j.c>> f580s;
    @Nullable
    @VisibleForTesting
    public w0<CloseableReference<b.f.j.j.c>> t;
    @Nullable
    @VisibleForTesting
    public w0<CloseableReference<b.f.j.j.c>> u;
    @Nullable
    @VisibleForTesting
    public w0<CloseableReference<b.f.j.j.c>> v;
    @Nullable
    @VisibleForTesting
    public w0<CloseableReference<b.f.j.j.c>> w;
    @VisibleForTesting

    /* renamed from: x  reason: collision with root package name */
    public Map<w0<CloseableReference<b.f.j.j.c>>, w0<CloseableReference<b.f.j.j.c>>> f581x = new HashMap();
    @VisibleForTesting

    /* renamed from: y  reason: collision with root package name */
    public Map<w0<CloseableReference<b.f.j.j.c>>, w0<CloseableReference<b.f.j.j.c>>> f582y = new HashMap();

    public q(ContentResolver contentResolver, p pVar, o0 o0Var, boolean z2, boolean z3, g1 g1Var, boolean z4, boolean z5, boolean z6, boolean z7, c cVar, boolean z8, boolean z9, boolean z10) {
        this.a = contentResolver;
        this.f579b = pVar;
        this.c = o0Var;
        this.d = z2;
        new HashMap();
        this.f = g1Var;
        this.g = z4;
        this.h = z5;
        this.e = z6;
        this.i = z7;
        this.j = cVar;
        this.k = z8;
        this.l = z9;
        this.m = z10;
    }

    public final synchronized w0<e> a() {
        b.b();
        if (this.o == null) {
            b.b();
            p pVar = this.f579b;
            a aVar = new a(m(new n0(pVar.k, pVar.d, this.c)));
            this.o = aVar;
            this.o = this.f579b.a(aVar, this.d && !this.g, this.j);
            b.b();
        }
        b.b();
        return this.o;
    }

    public final synchronized w0<CloseableReference<b.f.j.j.c>> b() {
        if (this.u == null) {
            m mVar = new m(this.f579b.k);
            b.f.d.m.b bVar = b.f.d.m.c.a;
            this.u = j(this.f579b.a(new a(mVar), true, this.j));
        }
        return this.u;
    }

    public w0<CloseableReference<b.f.j.j.c>> c(ImageRequest imageRequest) {
        s0 s0Var;
        o oVar;
        b.b();
        try {
            b.b();
            Objects.requireNonNull(imageRequest);
            Uri uri = imageRequest.c;
            d.y(uri, "Uri is null.");
            int i = imageRequest.d;
            if (i != 0) {
                boolean z2 = false;
                switch (i) {
                    case 2:
                        s0Var = g();
                        break;
                    case 3:
                        synchronized (this) {
                            if (this.p == null) {
                                p pVar = this.f579b;
                                this.p = k(new h0(pVar.j.e(), pVar.k));
                            }
                            s0Var = this.p;
                        }
                        break;
                    case 4:
                        if (imageRequest.h && Build.VERSION.SDK_INT >= 29) {
                            synchronized (this) {
                                if (this.v == null) {
                                    p pVar2 = this.f579b;
                                    this.v = i(new j0(pVar2.j.c(), pVar2.a));
                                }
                                s0Var = this.v;
                            }
                            break;
                        } else {
                            String type = this.a.getType(uri);
                            Map<String, String> map = b.f.d.f.a.a;
                            if (type != null && type.startsWith("video/")) {
                                z2 = true;
                            }
                            if (!z2) {
                                s0Var = e();
                                break;
                            } else {
                                s0Var = g();
                                break;
                            }
                        }
                        break;
                    case 5:
                        s0Var = d();
                        break;
                    case 6:
                        s0Var = f();
                        break;
                    case 7:
                        s0Var = b();
                        break;
                    case 8:
                        s0Var = h();
                        break;
                    default:
                        StringBuilder sb = new StringBuilder();
                        sb.append("Unsupported uri scheme! Uri is: ");
                        String valueOf = String.valueOf(uri);
                        if (valueOf.length() > 30) {
                            valueOf = valueOf.substring(0, 30) + "...";
                        }
                        sb.append(valueOf);
                        throw new IllegalArgumentException(sb.toString());
                }
            } else {
                synchronized (this) {
                    b.b();
                    if (this.n == null) {
                        b.b();
                        this.n = j(a());
                        b.b();
                    }
                    b.b();
                    s0Var = this.n;
                }
            }
            if (imageRequest.f2875s != null) {
                synchronized (this) {
                    w0<CloseableReference<b.f.j.j.c>> w0Var = this.f581x.get(s0Var);
                    if (w0Var == null) {
                        p pVar3 = this.f579b;
                        t0 t0Var = new t0(s0Var, pVar3.f576s, pVar3.j.c());
                        p pVar4 = this.f579b;
                        s0 s0Var2 = new s0(pVar4.o, pVar4.p, t0Var);
                        this.f581x.put(s0Var, s0Var2);
                        s0Var = s0Var2;
                    } else {
                        s0Var = w0Var;
                    }
                }
            }
            if (this.h) {
                synchronized (this) {
                    w0<CloseableReference<b.f.j.j.c>> w0Var2 = this.f582y.get(s0Var);
                    if (w0Var2 == null) {
                        p pVar5 = this.f579b;
                        i iVar = new i(s0Var, pVar5.t, pVar5.u, pVar5.v);
                        this.f582y.put(s0Var, iVar);
                        s0Var = iVar;
                    } else {
                        s0Var = w0Var2;
                    }
                }
            }
            if (this.m && imageRequest.u > 0) {
                synchronized (this) {
                    oVar = new o(s0Var, this.f579b.j.g());
                }
                s0Var = oVar;
            }
            return s0Var;
        } finally {
            b.b();
        }
    }

    public final synchronized w0<CloseableReference<b.f.j.j.c>> d() {
        if (this.t == null) {
            p pVar = this.f579b;
            this.t = k(new d0(pVar.j.e(), pVar.k, pVar.c));
        }
        return this.t;
    }

    public final synchronized w0<CloseableReference<b.f.j.j.c>> e() {
        if (this.r == null) {
            p pVar = this.f579b;
            e0 e0Var = new e0(pVar.j.e(), pVar.k, pVar.a);
            p pVar2 = this.f579b;
            Objects.requireNonNull(pVar2);
            p pVar3 = this.f579b;
            this.r = l(e0Var, new k1[]{new f0(pVar2.j.e(), pVar2.k, pVar2.a), new LocalExifThumbnailProducer(pVar3.j.f(), pVar3.k, pVar3.a)});
        }
        return this.r;
    }

    public final synchronized w0<CloseableReference<b.f.j.j.c>> f() {
        if (this.f580s == null) {
            p pVar = this.f579b;
            this.f580s = k(new i0(pVar.j.e(), pVar.k, pVar.f575b));
        }
        return this.f580s;
    }

    public final synchronized w0<CloseableReference<b.f.j.j.c>> g() {
        if (this.q == null) {
            p pVar = this.f579b;
            this.q = i(new k0(pVar.j.e(), pVar.a));
        }
        return this.q;
    }

    public final synchronized w0<CloseableReference<b.f.j.j.c>> h() {
        if (this.w == null) {
            p pVar = this.f579b;
            this.w = k(new b1(pVar.j.e(), pVar.k, pVar.a));
        }
        return this.w;
    }

    public final w0<CloseableReference<b.f.j.j.c>> i(w0<CloseableReference<b.f.j.j.c>> w0Var) {
        p pVar = this.f579b;
        w<CacheKey, b.f.j.j.c> wVar = pVar.o;
        b.f.j.c.i iVar = pVar.p;
        g gVar = new g(iVar, new h(wVar, iVar, w0Var));
        p pVar2 = this.f579b;
        g1 g1Var = this.f;
        Objects.requireNonNull(pVar2);
        f1 f1Var = new f1(gVar, g1Var);
        if (this.k || this.l) {
            p pVar3 = this.f579b;
            w<CacheKey, b.f.j.j.c> wVar2 = pVar3.o;
            b.f.j.c.i iVar2 = pVar3.p;
            return new j(pVar3.n, pVar3.l, pVar3.m, iVar2, pVar3.q, pVar3.r, new f(wVar2, iVar2, f1Var));
        }
        p pVar4 = this.f579b;
        return new f(pVar4.o, pVar4.p, f1Var);
    }

    public final w0<CloseableReference<b.f.j.j.c>> j(w0<e> w0Var) {
        b.b();
        p pVar = this.f579b;
        w0<CloseableReference<b.f.j.j.c>> i = i(new n(pVar.d, pVar.j.a(), pVar.e, pVar.f, pVar.g, pVar.h, pVar.i, w0Var, pVar.f577x, pVar.w, null, l.a));
        b.b();
        return i;
    }

    public final w0<CloseableReference<b.f.j.j.c>> k(w0<e> w0Var) {
        p pVar = this.f579b;
        return l(w0Var, new k1[]{new LocalExifThumbnailProducer(pVar.j.f(), pVar.k, pVar.a)});
    }

    public final w0<CloseableReference<b.f.j.j.c>> l(w0<e> w0Var, k1<e>[] k1VarArr) {
        i1 i1Var = new i1(5, this.f579b.j.b(), this.f579b.a(new a(m(w0Var)), true, this.j));
        Objects.requireNonNull(this.f579b);
        return j(new k(this.f579b.a(new j1(k1VarArr), true, this.j), i1Var));
    }

    public final w0<e> m(w0<e> w0Var) {
        t tVar;
        b.f.d.m.b bVar = b.f.d.m.c.a;
        if (this.i) {
            b.b();
            if (this.e) {
                p pVar = this.f579b;
                b.f.j.c.f fVar = pVar.l;
                b.f.j.c.i iVar = pVar.p;
                tVar = new t(fVar, pVar.m, iVar, new r0(fVar, iVar, pVar.k, pVar.d, w0Var));
            } else {
                p pVar2 = this.f579b;
                tVar = new t(pVar2.l, pVar2.m, pVar2.p, w0Var);
            }
            p pVar3 = this.f579b;
            s sVar = new s(pVar3.l, pVar3.m, pVar3.p, tVar);
            b.b();
            w0Var = sVar;
        }
        p pVar4 = this.f579b;
        w<CacheKey, PooledByteBuffer> wVar = pVar4.n;
        b.f.j.c.i iVar2 = pVar4.p;
        v vVar = new v(wVar, iVar2, w0Var);
        if (!this.l) {
            return new u(iVar2, pVar4.f578y, vVar);
        }
        return new u(iVar2, pVar4.f578y, new b.f.j.p.w(pVar4.l, pVar4.m, iVar2, pVar4.q, pVar4.r, vVar));
    }
}
