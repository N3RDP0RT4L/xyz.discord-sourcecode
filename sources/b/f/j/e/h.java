package b.f.j.e;

import b.f.c.a;
import b.f.j.c.f;
import b.f.j.c.i;
import b.f.j.c.w;
import b.f.j.j.c;
import b.f.j.k.b;
import b.f.j.k.d;
import b.f.j.k.e;
import b.f.j.p.b0;
import b.f.j.p.d1;
import b.f.j.p.g1;
import b.f.j.p.w0;
import com.facebook.cache.common.CacheKey;
import com.facebook.common.internal.Supplier;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.imagepipeline.request.ImageRequest;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicLong;
/* compiled from: ImagePipeline.java */
/* loaded from: classes2.dex */
public class h {
    public final q a;

    /* renamed from: b  reason: collision with root package name */
    public final e f565b;
    public final d c;
    public final Supplier<Boolean> d;
    public final w<CacheKey, c> e;
    public final w<CacheKey, PooledByteBuffer> f;
    public final i g;
    public final Supplier<Boolean> h;
    public AtomicLong i = new AtomicLong();
    public final Supplier<Boolean> j = null;
    public final a k;
    public final k l;

    static {
        new CancellationException("Prefetching is not enabled");
    }

    public h(q qVar, Set<e> set, Set<d> set2, Supplier<Boolean> supplier, w<CacheKey, c> wVar, w<CacheKey, PooledByteBuffer> wVar2, f fVar, f fVar2, i iVar, g1 g1Var, Supplier<Boolean> supplier2, Supplier<Boolean> supplier3, a aVar, k kVar) {
        this.a = qVar;
        this.f565b = new b.f.j.k.c(set);
        this.c = new b(set2);
        this.d = supplier;
        this.e = wVar;
        this.f = wVar2;
        this.g = iVar;
        this.h = supplier2;
        this.k = aVar;
        this.l = kVar;
    }

    public DataSource<CloseableReference<c>> a(ImageRequest imageRequest, Object obj, ImageRequest.c cVar, e eVar, String str) {
        try {
            return b(this.a.c(imageRequest), imageRequest, cVar, obj, eVar, str);
        } catch (Exception e) {
            return b.c.a.a0.d.N0(e);
        }
    }

    public final <T> DataSource<CloseableReference<T>> b(w0<CloseableReference<T>> w0Var, ImageRequest imageRequest, ImageRequest.c cVar, Object obj, e eVar, String str) {
        e eVar2;
        boolean z2;
        b.f.j.k.c cVar2;
        b.f.j.r.b.b();
        if (eVar == null) {
            e eVar3 = imageRequest.t;
            if (eVar3 == null) {
                eVar2 = this.f565b;
            } else {
                cVar2 = new b.f.j.k.c(this.f565b, eVar3);
                eVar2 = cVar2;
            }
        } else {
            e eVar4 = imageRequest.t;
            if (eVar4 == null) {
                eVar2 = new b.f.j.k.c(this.f565b, eVar);
            } else {
                cVar2 = new b.f.j.k.c(this.f565b, eVar, eVar4);
                eVar2 = cVar2;
            }
        }
        b0 b0Var = new b0(eVar2, this.c);
        a aVar = this.k;
        if (aVar != null) {
            aVar.a(obj, false);
        }
        try {
            ImageRequest.c f = ImageRequest.c.f(imageRequest.n, cVar);
            String valueOf = String.valueOf(this.i.getAndIncrement());
            if (!imageRequest.f && b.f.d.l.b.e(imageRequest.c)) {
                z2 = false;
                d1 d1Var = new d1(imageRequest, valueOf, str, b0Var, obj, f, false, z2, imageRequest.m, this.l);
                b.f.j.r.b.b();
                b.f.j.f.d dVar = new b.f.j.f.d(w0Var, d1Var, b0Var);
                b.f.j.r.b.b();
                return dVar;
            }
            z2 = true;
            d1 d1Var2 = new d1(imageRequest, valueOf, str, b0Var, obj, f, false, z2, imageRequest.m, this.l);
            b.f.j.r.b.b();
            b.f.j.f.d dVar2 = new b.f.j.f.d(w0Var, d1Var2, b0Var);
            b.f.j.r.b.b();
            return dVar2;
        } catch (Exception e) {
            return b.c.a.a0.d.N0(e);
        } finally {
            b.f.j.r.b.b();
        }
    }
}
