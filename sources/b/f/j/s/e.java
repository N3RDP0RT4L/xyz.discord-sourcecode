package b.f.j.s;

import b.f.i.c;
import b.f.j.e.n;
import java.lang.reflect.InvocationTargetException;
/* compiled from: MultiImageTranscoderFactory.java */
/* loaded from: classes2.dex */
public class e implements c {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final c f640b;
    public final Integer c;
    public final boolean d;

    public e(int i, boolean z2, c cVar, Integer num, boolean z3) {
        this.a = i;
        this.f640b = cVar;
        this.c = num;
        this.d = z3;
    }

    public final b a(c cVar, boolean z2) {
        int i = this.a;
        boolean z3 = this.d;
        try {
            Class<?> cls = Class.forName("com.facebook.imagepipeline.nativecode.NativeJpegTranscoderFactory");
            Class<?> cls2 = Boolean.TYPE;
            return ((c) cls.getConstructor(Integer.TYPE, cls2, cls2).newInstance(Integer.valueOf(i), Boolean.FALSE, Boolean.valueOf(z3))).createImageTranscoder(cVar, z2);
        } catch (ClassNotFoundException | IllegalAccessException | IllegalArgumentException | InstantiationException | NoSuchMethodException | SecurityException | InvocationTargetException e) {
            throw new RuntimeException("Dependency ':native-imagetranscoder' is needed to use the default native image transcoder.", e);
        }
    }

    @Override // b.f.j.s.c
    public b createImageTranscoder(c cVar, boolean z2) {
        c cVar2 = this.f640b;
        b bVar = null;
        b createImageTranscoder = cVar2 == null ? null : cVar2.createImageTranscoder(cVar, z2);
        if (createImageTranscoder == null) {
            Integer num = this.c;
            if (num != null) {
                int intValue = num.intValue();
                if (intValue == 0) {
                    bVar = a(cVar, z2);
                } else if (intValue == 1) {
                    bVar = new f(z2, this.a);
                } else {
                    throw new IllegalArgumentException("Invalid ImageTranscoderType");
                }
            }
            createImageTranscoder = bVar;
        }
        if (createImageTranscoder == null && n.a) {
            createImageTranscoder = a(cVar, z2);
        }
        return createImageTranscoder == null ? new f(z2, this.a) : createImageTranscoder;
    }
}
