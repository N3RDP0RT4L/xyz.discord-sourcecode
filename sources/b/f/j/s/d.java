package b.f.j.s;

import b.f.d.d.e;
import b.f.j.d.f;
import java.util.Collections;
/* compiled from: JpegTranscoderUtils.java */
/* loaded from: classes2.dex */
public class d {
    public static final e<Integer> a;

    static {
        e<Integer> eVar = new e<>(4);
        Collections.addAll(eVar, 2, 7, 4, 5);
        a = eVar;
    }

    public static int a(f fVar, b.f.j.j.e eVar) {
        eVar.x();
        int i = eVar.n;
        e<Integer> eVar2 = a;
        int indexOf = eVar2.indexOf(Integer.valueOf(i));
        if (indexOf >= 0) {
            int i2 = 0;
            if (!fVar.c()) {
                i2 = fVar.a();
            }
            return eVar2.get(((i2 / 90) + indexOf) % eVar2.size()).intValue();
        }
        throw new IllegalArgumentException("Only accepts inverted exif orientations");
    }

    public static int b(f fVar, b.f.j.j.e eVar) {
        int i = 0;
        if (!fVar.b()) {
            return 0;
        }
        eVar.x();
        int i2 = eVar.m;
        if (i2 == 90 || i2 == 180 || i2 == 270) {
            eVar.x();
            i = eVar.m;
        }
        return fVar.c() ? i : (fVar.a() + i) % 360;
    }

    public static int c(f fVar, b.f.j.d.e eVar, b.f.j.j.e eVar2, boolean z2) {
        int i;
        int i2;
        if (!z2 || eVar == null) {
            return 8;
        }
        int b2 = b(fVar, eVar2);
        e<Integer> eVar3 = a;
        eVar2.x();
        boolean z3 = false;
        int a2 = eVar3.contains(Integer.valueOf(eVar2.n)) ? a(fVar, eVar2) : 0;
        if (b2 == 90 || b2 == 270 || a2 == 5 || a2 == 7) {
            z3 = true;
        }
        if (z3) {
            eVar2.x();
            i = eVar2.p;
        } else {
            eVar2.x();
            i = eVar2.o;
        }
        if (z3) {
            eVar2.x();
            i2 = eVar2.o;
        } else {
            eVar2.x();
            i2 = eVar2.p;
        }
        float f = i;
        float f2 = i2;
        float max = Math.max(eVar.a / f, eVar.f562b / f2);
        float f3 = eVar.c;
        if (f * max > f3) {
            max = f3 / f;
        }
        if (f2 * max > f3) {
            max = f3 / f2;
        }
        int i3 = (int) ((max * 8.0f) + eVar.d);
        if (i3 > 8) {
            return 8;
        }
        if (i3 < 1) {
            return 1;
        }
        return i3;
    }
}
