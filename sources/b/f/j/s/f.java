package b.f.j.s;

import android.graphics.Bitmap;
import b.c.a.a0.d;
import b.f.i.b;
import b.f.i.c;
import b.f.j.j.e;
/* compiled from: SimpleImageTranscoder.java */
/* loaded from: classes2.dex */
public class f implements b {
    public final boolean a;

    /* renamed from: b  reason: collision with root package name */
    public final int f641b;

    public f(boolean z2, int i) {
        this.a = z2;
        this.f641b = i;
    }

    public static Bitmap.CompressFormat e(c cVar) {
        if (cVar == null) {
            return Bitmap.CompressFormat.JPEG;
        }
        if (cVar == b.a) {
            return Bitmap.CompressFormat.JPEG;
        }
        if (cVar == b.f536b) {
            return Bitmap.CompressFormat.PNG;
        }
        if (b.a(cVar)) {
            return Bitmap.CompressFormat.WEBP;
        }
        return Bitmap.CompressFormat.JPEG;
    }

    @Override // b.f.j.s.b
    public String a() {
        return "SimpleImageTranscoder";
    }

    @Override // b.f.j.s.b
    public boolean b(e eVar, b.f.j.d.f fVar, b.f.j.d.e eVar2) {
        if (fVar == null) {
            fVar = b.f.j.d.f.a;
        }
        return this.a && d.a0(fVar, eVar2, eVar, this.f641b) > 1;
    }

    /* JADX WARN: Removed duplicated region for block: B:38:0x00a5  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x00be  */
    /* JADX WARN: Removed duplicated region for block: B:48:0x00d0  */
    @Override // b.f.j.s.b
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.f.j.s.a c(b.f.j.j.e r20, java.io.OutputStream r21, b.f.j.d.f r22, b.f.j.d.e r23, b.f.i.c r24, java.lang.Integer r25) {
        /*
            Method dump skipped, instructions count: 254
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.f.j.s.f.c(b.f.j.j.e, java.io.OutputStream, b.f.j.d.f, b.f.j.d.e, b.f.i.c, java.lang.Integer):b.f.j.s.a");
    }

    @Override // b.f.j.s.b
    public boolean d(c cVar) {
        return cVar == b.k || cVar == b.a;
    }
}
