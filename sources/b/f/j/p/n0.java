package b.f.j.p;

import android.os.SystemClock;
import b.f.d.g.g;
import b.f.d.g.i;
import b.f.j.j.e;
import b.f.j.j.f;
import b.f.j.p.a0;
import b.f.j.p.o0;
import b.f.j.r.b;
import com.facebook.common.references.CloseableReference;
import com.facebook.imagepipeline.memory.MemoryPooledByteBufferOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Objects;
/* compiled from: NetworkFetchProducer.java */
/* loaded from: classes2.dex */
public class n0 implements w0<e> {
    public final g a;

    /* renamed from: b  reason: collision with root package name */
    public final b.f.d.g.a f625b;
    public final o0 c;

    /* compiled from: NetworkFetchProducer.java */
    /* loaded from: classes2.dex */
    public class a implements o0.a {
        public final /* synthetic */ x a;

        public a(x xVar) {
            this.a = xVar;
        }

        public void a(Throwable th) {
            n0 n0Var = n0.this;
            x xVar = this.a;
            Objects.requireNonNull(n0Var);
            xVar.a().k(xVar.f638b, "NetworkFetchProducer", th, null);
            xVar.a().c(xVar.f638b, "NetworkFetchProducer", false);
            xVar.f638b.n("network");
            xVar.a.c(th);
        }

        /* JADX WARN: Finally extract failed */
        public void b(InputStream inputStream, int i) throws IOException {
            i iVar;
            b.b();
            n0 n0Var = n0.this;
            x xVar = this.a;
            if (i > 0) {
                iVar = n0Var.a.e(i);
            } else {
                iVar = n0Var.a.a();
            }
            byte[] bArr = n0Var.f625b.get(16384);
            while (true) {
                try {
                    int read = inputStream.read(bArr);
                    if (read < 0) {
                        o0 o0Var = n0Var.c;
                        int i2 = ((MemoryPooledByteBufferOutputStream) iVar).l;
                        a0 a0Var = (a0) o0Var;
                        Objects.requireNonNull(a0Var);
                        ((a0.a) xVar).f = a0Var.c.now();
                        n0Var.c(iVar, xVar);
                        n0Var.f625b.release(bArr);
                        iVar.close();
                        b.b();
                        return;
                    } else if (read > 0) {
                        iVar.write(bArr, 0, read);
                        n0Var.d(iVar, xVar);
                        int i3 = ((MemoryPooledByteBufferOutputStream) iVar).l;
                        xVar.a.a(i > 0 ? i3 / i : 1.0f - ((float) Math.exp((-i3) / 50000.0d)));
                    }
                } catch (Throwable th) {
                    n0Var.f625b.release(bArr);
                    iVar.close();
                    throw th;
                }
            }
        }
    }

    public n0(g gVar, b.f.d.g.a aVar, o0 o0Var) {
        this.a = gVar;
        this.f625b = aVar;
        this.c = o0Var;
    }

    public static void e(i iVar, int i, b.f.j.d.a aVar, l<e> lVar, x0 x0Var) {
        Throwable th;
        CloseableReference A = CloseableReference.A(((MemoryPooledByteBufferOutputStream) iVar).b());
        e eVar = null;
        try {
            e eVar2 = new e(A);
            try {
                eVar2.f586s = null;
                eVar2.q();
                x0Var.h(f.NETWORK);
                lVar.b(eVar2, i);
                eVar2.close();
                if (A != null) {
                    A.close();
                }
            } catch (Throwable th2) {
                th = th2;
                eVar = eVar2;
                if (eVar != null) {
                    eVar.close();
                }
                if (A != null) {
                    A.close();
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
        }
    }

    @Override // b.f.j.p.w0
    public void b(l<e> lVar, x0 x0Var) {
        x0Var.o().e(x0Var, "NetworkFetchProducer");
        Objects.requireNonNull((a0) this.c);
        a0.a aVar = new a0.a(lVar, x0Var);
        o0 o0Var = this.c;
        a aVar2 = new a(aVar);
        a0 a0Var = (a0) o0Var;
        Objects.requireNonNull(a0Var);
        aVar.d = a0Var.c.now();
        x0Var.f(new z(a0Var, a0Var.f600b.submit(new y(a0Var, aVar, aVar2)), aVar2));
    }

    public void c(i iVar, x xVar) {
        HashMap hashMap;
        int i = ((MemoryPooledByteBufferOutputStream) iVar).l;
        if (!xVar.a().g(xVar.f638b, "NetworkFetchProducer")) {
            hashMap = null;
        } else {
            Objects.requireNonNull((a0) this.c);
            a0.a aVar = (a0.a) xVar;
            hashMap = new HashMap(4);
            hashMap.put("queue_time", Long.toString(aVar.e - aVar.d));
            hashMap.put("fetch_time", Long.toString(aVar.f - aVar.e));
            hashMap.put("total_time", Long.toString(aVar.f - aVar.d));
            hashMap.put("image_size", Integer.toString(i));
        }
        z0 a2 = xVar.a();
        a2.j(xVar.f638b, "NetworkFetchProducer", hashMap);
        a2.c(xVar.f638b, "NetworkFetchProducer", true);
        xVar.f638b.n("network");
        e(iVar, 1, null, xVar.a, xVar.f638b);
    }

    public void d(i iVar, x xVar) {
        boolean z2;
        long uptimeMillis = SystemClock.uptimeMillis();
        if (!xVar.f638b.p()) {
            z2 = false;
        } else {
            Objects.requireNonNull(this.c);
            z2 = true;
        }
        if (z2 && uptimeMillis - xVar.c >= 100) {
            xVar.c = uptimeMillis;
            xVar.a().a(xVar.f638b, "NetworkFetchProducer", "intermediate_result");
            e(iVar, 0, null, xVar.a, xVar.f638b);
        }
    }
}
