package b.f.j.p;

import b.f.j.c.d;
import b.f.j.c.f;
import b.f.j.c.i;
import b.f.j.c.n;
import b.f.j.c.w;
import b.f.j.j.c;
import b.f.j.r.b;
import com.facebook.cache.common.CacheKey;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.common.references.CloseableReference;
import com.facebook.imagepipeline.request.ImageRequest;
import java.util.Objects;
/* compiled from: BitmapProbeProducer.java */
/* loaded from: classes2.dex */
public class j implements w0<CloseableReference<c>> {
    public final w<CacheKey, PooledByteBuffer> a;

    /* renamed from: b  reason: collision with root package name */
    public final f f613b;
    public final f c;
    public final i d;
    public final w0<CloseableReference<c>> e;
    public final d<CacheKey> f;
    public final d<CacheKey> g;

    /* compiled from: BitmapProbeProducer.java */
    /* loaded from: classes2.dex */
    public static class a extends p<CloseableReference<c>, CloseableReference<c>> {
        public final x0 c;
        public final w<CacheKey, PooledByteBuffer> d;
        public final f e;
        public final f f;
        public final i g;
        public final d<CacheKey> h;
        public final d<CacheKey> i;

        public a(l<CloseableReference<c>> lVar, x0 x0Var, w<CacheKey, PooledByteBuffer> wVar, f fVar, f fVar2, i iVar, d<CacheKey> dVar, d<CacheKey> dVar2) {
            super(lVar);
            this.c = x0Var;
            this.d = wVar;
            this.e = fVar;
            this.f = fVar2;
            this.g = iVar;
            this.h = dVar;
            this.i = dVar2;
        }

        @Override // b.f.j.p.b
        public void i(Object obj, int i) {
            CloseableReference closeableReference = (CloseableReference) obj;
            try {
                b.b();
                if (!b.f(i) && closeableReference != null && !b.l(i, 8)) {
                    ImageRequest e = this.c.e();
                    ((n) this.g).b(e, this.c.b());
                    String str = (String) this.c.l("origin");
                    if (str != null && str.equals("memory_bitmap")) {
                        Objects.requireNonNull(this.c.g().getExperiments());
                        Objects.requireNonNull(this.c.g().getExperiments());
                    }
                    this.f628b.b(closeableReference, i);
                }
                this.f628b.b(closeableReference, i);
            } finally {
                b.b();
            }
        }
    }

    public j(w<CacheKey, PooledByteBuffer> wVar, f fVar, f fVar2, i iVar, d<CacheKey> dVar, d<CacheKey> dVar2, w0<CloseableReference<c>> w0Var) {
        this.a = wVar;
        this.f613b = fVar;
        this.c = fVar2;
        this.d = iVar;
        this.f = dVar;
        this.g = dVar2;
        this.e = w0Var;
    }

    @Override // b.f.j.p.w0
    public void b(l<CloseableReference<c>> lVar, x0 x0Var) {
        try {
            b.b();
            z0 o = x0Var.o();
            o.e(x0Var, "BitmapProbeProducer");
            a aVar = new a(lVar, x0Var, this.a, this.f613b, this.c, this.d, this.f, this.g);
            o.j(x0Var, "BitmapProbeProducer", null);
            b.b();
            this.e.b(aVar, x0Var);
            b.b();
        } finally {
            b.b();
        }
    }
}
