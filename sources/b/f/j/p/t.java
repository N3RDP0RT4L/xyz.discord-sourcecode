package b.f.j.p;

import b.f.i.c;
import b.f.j.c.f;
import b.f.j.c.i;
import b.f.j.c.n;
import b.f.j.j.e;
import com.facebook.cache.common.CacheKey;
import com.facebook.imagepipeline.request.ImageRequest;
/* compiled from: DiskCacheWriteProducer.java */
/* loaded from: classes2.dex */
public class t implements w0<e> {
    public final f a;

    /* renamed from: b  reason: collision with root package name */
    public final f f634b;
    public final i c;
    public final w0<e> d;

    /* compiled from: DiskCacheWriteProducer.java */
    /* loaded from: classes2.dex */
    public static class b extends p<e, e> {
        public final x0 c;
        public final f d;
        public final f e;
        public final i f;

        public b(l lVar, x0 x0Var, f fVar, f fVar2, i iVar, a aVar) {
            super(lVar);
            this.c = x0Var;
            this.d = fVar;
            this.e = fVar2;
            this.f = iVar;
        }

        @Override // b.f.j.p.b
        public void i(Object obj, int i) {
            e eVar = (e) obj;
            this.c.o().e(this.c, "DiskCacheWriteProducer");
            if (!b.f.j.p.b.f(i) && eVar != null && !b.f.j.p.b.l(i, 10)) {
                eVar.x();
                if (eVar.l != c.a) {
                    ImageRequest e = this.c.e();
                    CacheKey b2 = ((n) this.f).b(e, this.c.b());
                    if (e.f2874b == ImageRequest.b.SMALL) {
                        this.e.f(b2, eVar);
                    } else {
                        this.d.f(b2, eVar);
                    }
                    this.c.o().j(this.c, "DiskCacheWriteProducer", null);
                    this.f628b.b(eVar, i);
                    return;
                }
            }
            this.c.o().j(this.c, "DiskCacheWriteProducer", null);
            this.f628b.b(eVar, i);
        }
    }

    public t(f fVar, f fVar2, i iVar, w0<e> w0Var) {
        this.a = fVar;
        this.f634b = fVar2;
        this.c = iVar;
        this.d = w0Var;
    }

    @Override // b.f.j.p.w0
    public void b(l<e> lVar, x0 x0Var) {
        if (x0Var.q().g() >= ImageRequest.c.DISK_CACHE.g()) {
            x0Var.i("disk", "nil-result_write");
            lVar.b(null, 1);
            return;
        }
        if (x0Var.e().b(32)) {
            lVar = new b(lVar, x0Var, this.a, this.f634b, this.c, null);
        }
        this.d.b(lVar, x0Var);
    }
}
