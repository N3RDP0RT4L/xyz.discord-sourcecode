package b.f.j.p;

import android.util.Pair;
import androidx.annotation.VisibleForTesting;
import b.f.j.d.d;
import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArraySet;
/* compiled from: MultiplexProducer.java */
/* loaded from: classes2.dex */
public abstract class l0<K, T extends Closeable> implements w0<T> {
    @VisibleForTesting
    public final Map<K, l0<K, T>.b> a;

    /* renamed from: b  reason: collision with root package name */
    public final w0<T> f618b;
    public final boolean c;
    public final String d;
    public final String e;

    /* compiled from: MultiplexProducer.java */
    @VisibleForTesting
    /* loaded from: classes2.dex */
    public class b {
        public final K a;

        /* renamed from: b  reason: collision with root package name */
        public final CopyOnWriteArraySet<Pair<l<T>, x0>> f619b = new CopyOnWriteArraySet<>();
        public T c;
        public float d;
        public int e;
        public d f;
        public l0<K, T>.b.a g;

        /* compiled from: MultiplexProducer.java */
        /* loaded from: classes2.dex */
        public class a extends b.f.j.p.b<T> {
            public a(a aVar) {
            }

            @Override // b.f.j.p.b
            public void g() {
                try {
                    b.f.j.r.b.b();
                    b bVar = b.this;
                    synchronized (bVar) {
                        if (bVar.g == this) {
                            bVar.g = null;
                            bVar.f = null;
                            bVar.b(bVar.c);
                            bVar.c = null;
                            bVar.i(b.f.d.l.a.UNSET);
                        }
                    }
                } finally {
                    b.f.j.r.b.b();
                }
            }

            @Override // b.f.j.p.b
            public void h(Throwable th) {
                try {
                    b.f.j.r.b.b();
                    b.this.f(this, th);
                } finally {
                    b.f.j.r.b.b();
                }
            }

            /* JADX WARN: Multi-variable type inference failed */
            @Override // b.f.j.p.b
            public void i(Object obj, int i) {
                Closeable closeable = (Closeable) obj;
                try {
                    b.f.j.r.b.b();
                    b.this.g(this, closeable, i);
                } finally {
                    b.f.j.r.b.b();
                }
            }

            @Override // b.f.j.p.b
            public void j(float f) {
                try {
                    b.f.j.r.b.b();
                    b.this.h(this, f);
                } finally {
                    b.f.j.r.b.b();
                }
            }
        }

        public b(K k) {
            this.a = k;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public boolean a(l<T> lVar, x0 x0Var) {
            l0<K, T>.b bVar;
            Pair<l<T>, x0> create = Pair.create(lVar, x0Var);
            synchronized (this) {
                l0 l0Var = l0.this;
                K k = this.a;
                synchronized (l0Var) {
                    bVar = l0Var.a.get(k);
                }
                if (bVar != this) {
                    return false;
                }
                this.f619b.add(create);
                List<y0> k2 = k();
                List<y0> l = l();
                List<y0> j = j();
                Closeable closeable = this.c;
                float f = this.d;
                int i = this.e;
                d.s(k2);
                d.t(l);
                d.r(j);
                synchronized (create) {
                    synchronized (this) {
                        if (closeable != this.c) {
                            closeable = null;
                        } else if (closeable != null) {
                            closeable = l0.this.c(closeable);
                        }
                    }
                    if (closeable != null) {
                        if (f > 0.0f) {
                            lVar.a(f);
                        }
                        lVar.b(closeable, i);
                        b(closeable);
                    }
                }
                x0Var.f(new m0(this, create));
                return true;
            }
        }

        public final void b(Closeable closeable) {
            if (closeable != null) {
                try {
                    closeable.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        public final synchronized boolean c() {
            Iterator<Pair<l<T>, x0>> it = this.f619b.iterator();
            while (it.hasNext()) {
                if (((x0) it.next().second).p()) {
                    return true;
                }
            }
            return false;
        }

        public final synchronized boolean d() {
            Iterator<Pair<l<T>, x0>> it = this.f619b.iterator();
            while (it.hasNext()) {
                if (!((x0) it.next().second).k()) {
                    return false;
                }
            }
            return true;
        }

        public final synchronized d e() {
            d dVar;
            dVar = d.LOW;
            Iterator<Pair<l<T>, x0>> it = this.f619b.iterator();
            while (it.hasNext()) {
                d c = ((x0) it.next().second).c();
                if (dVar.ordinal() <= c.ordinal()) {
                    dVar = c;
                }
            }
            return dVar;
        }

        public void f(l0<K, T>.b.a aVar, Throwable th) {
            synchronized (this) {
                if (this.g == aVar) {
                    Iterator<Pair<l<T>, x0>> it = this.f619b.iterator();
                    this.f619b.clear();
                    l0.this.e(this.a, this);
                    b(this.c);
                    this.c = null;
                    while (it.hasNext()) {
                        Pair<l<T>, x0> next = it.next();
                        synchronized (next) {
                            ((x0) next.second).o().k((x0) next.second, l0.this.d, th, null);
                            ((l) next.first).c(th);
                        }
                    }
                }
            }
        }

        public void g(l0<K, T>.b.a aVar, T t, int i) {
            synchronized (this) {
                if (this.g == aVar) {
                    b(this.c);
                    this.c = null;
                    Iterator<Pair<l<T>, x0>> it = this.f619b.iterator();
                    int size = this.f619b.size();
                    if (b.f.j.p.b.f(i)) {
                        this.c = (T) l0.this.c(t);
                        this.e = i;
                    } else {
                        this.f619b.clear();
                        l0.this.e(this.a, this);
                    }
                    while (it.hasNext()) {
                        Pair<l<T>, x0> next = it.next();
                        synchronized (next) {
                            if (b.f.j.p.b.e(i)) {
                                ((x0) next.second).o().j((x0) next.second, l0.this.d, null);
                                d dVar = this.f;
                                if (dVar != null) {
                                    ((x0) next.second).j(dVar.h);
                                }
                                ((x0) next.second).d(l0.this.e, Integer.valueOf(size));
                            }
                            ((l) next.first).b(t, i);
                        }
                    }
                }
            }
        }

        public void h(l0<K, T>.b.a aVar, float f) {
            synchronized (this) {
                if (this.g == aVar) {
                    this.d = f;
                    Iterator<Pair<l<T>, x0>> it = this.f619b.iterator();
                    while (it.hasNext()) {
                        Pair<l<T>, x0> next = it.next();
                        synchronized (next) {
                            ((l) next.first).a(f);
                        }
                    }
                }
            }
        }

        public final void i(b.f.d.l.a aVar) {
            synchronized (this) {
                boolean z2 = true;
                b.c.a.a0.d.i(Boolean.valueOf(this.f == null));
                b.c.a.a0.d.i(Boolean.valueOf(this.g == null));
                if (this.f619b.isEmpty()) {
                    l0.this.e(this.a, this);
                    return;
                }
                x0 x0Var = (x0) this.f619b.iterator().next().second;
                d dVar = new d(x0Var.e(), x0Var.getId(), null, x0Var.o(), x0Var.b(), x0Var.q(), d(), c(), e(), x0Var.g());
                this.f = dVar;
                dVar.j(x0Var.a());
                Objects.requireNonNull(aVar);
                if (aVar != b.f.d.l.a.UNSET) {
                    d dVar2 = this.f;
                    int ordinal = aVar.ordinal();
                    if (ordinal != 0) {
                        if (ordinal == 1) {
                            z2 = false;
                        } else if (ordinal != 2) {
                            throw new IllegalStateException("Unrecognized TriState value: " + aVar);
                        } else {
                            throw new IllegalStateException("No boolean equivalent for UNSET");
                        }
                    }
                    dVar2.d("started_as_prefetch", Boolean.valueOf(z2));
                }
                l0<K, T>.b.a aVar2 = new a(null);
                this.g = aVar2;
                l0.this.f618b.b(aVar2, this.f);
            }
        }

        public final synchronized List<y0> j() {
            d dVar = this.f;
            ArrayList arrayList = null;
            if (dVar == null) {
                return null;
            }
            boolean c = c();
            synchronized (dVar) {
                if (c != dVar.k) {
                    dVar.k = c;
                    arrayList = new ArrayList(dVar.m);
                }
            }
            return arrayList;
        }

        public final synchronized List<y0> k() {
            d dVar = this.f;
            ArrayList arrayList = null;
            if (dVar == null) {
                return null;
            }
            boolean d = d();
            synchronized (dVar) {
                if (d != dVar.i) {
                    dVar.i = d;
                    arrayList = new ArrayList(dVar.m);
                }
            }
            return arrayList;
        }

        public final synchronized List<y0> l() {
            d dVar = this.f;
            if (dVar == null) {
                return null;
            }
            return dVar.v(e());
        }
    }

    public l0(w0<T> w0Var, String str, String str2) {
        this.f618b = w0Var;
        this.a = new HashMap();
        this.c = false;
        this.d = str;
        this.e = str2;
    }

    @Override // b.f.j.p.w0
    public void b(l<T> lVar, x0 x0Var) {
        boolean z2;
        l0<K, T>.b bVar;
        try {
            b.f.j.r.b.b();
            x0Var.o().e(x0Var, this.d);
            K d = d(x0Var);
            do {
                z2 = false;
                synchronized (this) {
                    synchronized (this) {
                        bVar = this.a.get(d);
                    }
                }
                if (bVar == null) {
                    synchronized (this) {
                        bVar = new b(d);
                        this.a.put(d, bVar);
                        z2 = true;
                    }
                }
            } while (!bVar.a(lVar, x0Var));
            if (z2) {
                bVar.i(b.f.d.l.a.f(x0Var.k()));
            }
        } finally {
            b.f.j.r.b.b();
        }
    }

    public abstract T c(T t);

    public abstract K d(x0 x0Var);

    public synchronized void e(K k, l0<K, T>.b bVar) {
        if (this.a.get(k) == bVar) {
            this.a.remove(k);
        }
    }

    public l0(w0<T> w0Var, String str, String str2, boolean z2) {
        this.f618b = w0Var;
        this.a = new HashMap();
        this.c = z2;
        this.d = str;
        this.e = str2;
    }
}
