package b.f.j.p;

import androidx.annotation.NonNull;
import java.util.Map;
/* compiled from: ProducerListener2.java */
/* loaded from: classes2.dex */
public interface z0 {
    void a(@NonNull x0 x0Var, @NonNull String str, @NonNull String str2);

    void c(@NonNull x0 x0Var, @NonNull String str, boolean z2);

    void d(@NonNull x0 x0Var, @NonNull String str, Map<String, String> map);

    void e(@NonNull x0 x0Var, @NonNull String str);

    boolean g(@NonNull x0 x0Var, @NonNull String str);

    void j(@NonNull x0 x0Var, @NonNull String str, Map<String, String> map);

    void k(@NonNull x0 x0Var, String str, Throwable th, Map<String, String> map);
}
