package b.f.j.p;

import androidx.annotation.VisibleForTesting;
import b.f.j.c.f;
import b.f.j.c.i;
import b.f.j.c.n;
import b.f.j.j.e;
import com.facebook.cache.common.CacheKey;
import com.facebook.imagepipeline.request.ImageRequest;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
/* compiled from: DiskCacheReadProducer.java */
/* loaded from: classes2.dex */
public class s implements w0<e> {
    public final f a;

    /* renamed from: b  reason: collision with root package name */
    public final f f632b;
    public final i c;
    public final w0<e> d;

    public s(f fVar, f fVar2, i iVar, w0<e> w0Var) {
        this.a = fVar;
        this.f632b = fVar2;
        this.c = iVar;
        this.d = w0Var;
    }

    @VisibleForTesting
    public static Map<String, String> c(z0 z0Var, x0 x0Var, boolean z2, int i) {
        if (!z0Var.g(x0Var, "DiskCacheProducer")) {
            return null;
        }
        if (z2) {
            return b.f.d.d.f.of("cached_value_found", String.valueOf(z2), "encodedImageSize", String.valueOf(i));
        }
        return b.f.d.d.f.of("cached_value_found", String.valueOf(z2));
    }

    @Override // b.f.j.p.w0
    public void b(l<e> lVar, x0 x0Var) {
        ImageRequest e = x0Var.e();
        boolean z2 = true;
        if (x0Var.e().b(16)) {
            x0Var.o().e(x0Var, "DiskCacheProducer");
            CacheKey b2 = ((n) this.c).b(e, x0Var.b());
            if (e.f2874b != ImageRequest.b.SMALL) {
                z2 = false;
            }
            f fVar = z2 ? this.f632b : this.a;
            AtomicBoolean atomicBoolean = new AtomicBoolean(false);
            fVar.e(b2, atomicBoolean).b(new q(this, x0Var.o(), x0Var, lVar));
            x0Var.f(new r(this, atomicBoolean));
        } else if (x0Var.q().g() >= ImageRequest.c.DISK_CACHE.g()) {
            x0Var.i("disk", "nil-result_read");
            lVar.b(null, 1);
        } else {
            this.d.b(lVar, x0Var);
        }
    }
}
