package b.f.j.p;

import android.content.ContentResolver;
import b.c.a.a0.d;
import b.f.d.g.g;
import b.f.j.j.e;
import com.facebook.imagepipeline.request.ImageRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Executor;
/* compiled from: QualifiedResourceFetchProducer.java */
/* loaded from: classes2.dex */
public class b1 extends g0 {
    public final ContentResolver c;

    public b1(Executor executor, g gVar, ContentResolver contentResolver) {
        super(executor, gVar);
        this.c = contentResolver;
    }

    @Override // b.f.j.p.g0
    public e d(ImageRequest imageRequest) throws IOException {
        InputStream openInputStream = this.c.openInputStream(imageRequest.c);
        d.y(openInputStream, "ContentResolver returned null InputStream");
        return c(openInputStream, -1);
    }

    @Override // b.f.j.p.g0
    public String e() {
        return "QualifiedResourceFetchProducer";
    }
}
