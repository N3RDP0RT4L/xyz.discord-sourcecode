package b.f.j.p;

import b.f.d.d.f;
import b.f.i.c;
import b.f.j.c.i;
import b.f.j.c.n;
import b.f.j.c.w;
import b.f.j.j.e;
import b.f.j.r.b;
import com.facebook.cache.common.CacheKey;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.common.references.CloseableReference;
import com.facebook.imagepipeline.request.ImageRequest;
import java.util.Map;
/* compiled from: EncodedMemoryCacheProducer.java */
/* loaded from: classes2.dex */
public class v implements w0<e> {
    public final w<CacheKey, PooledByteBuffer> a;

    /* renamed from: b  reason: collision with root package name */
    public final i f636b;
    public final w0<e> c;

    /* compiled from: EncodedMemoryCacheProducer.java */
    /* loaded from: classes2.dex */
    public static class a extends p<e, e> {
        public final w<CacheKey, PooledByteBuffer> c;
        public final CacheKey d;
        public final boolean e;
        public final boolean f;

        public a(l<e> lVar, w<CacheKey, PooledByteBuffer> wVar, CacheKey cacheKey, boolean z2, boolean z3) {
            super(lVar);
            this.c = wVar;
            this.d = cacheKey;
            this.e = z2;
            this.f = z3;
        }

        @Override // b.f.j.p.b
        public void i(Object obj, int i) {
            e eVar = (e) obj;
            try {
                b.b();
                if (!b.f(i) && eVar != null && !b.l(i, 10)) {
                    eVar.x();
                    if (eVar.l != c.a) {
                        CloseableReference<PooledByteBuffer> c = eVar.c();
                        if (c != null) {
                            CloseableReference<PooledByteBuffer> closeableReference = null;
                            if (this.f && this.e) {
                                closeableReference = this.c.a(this.d, c);
                            }
                            c.close();
                            if (closeableReference != null) {
                                e eVar2 = new e(closeableReference);
                                eVar2.b(eVar);
                                closeableReference.close();
                                this.f628b.a(1.0f);
                                this.f628b.b(eVar2, i);
                                eVar2.close();
                            }
                        }
                        this.f628b.b(eVar, i);
                    }
                }
                this.f628b.b(eVar, i);
            } finally {
                b.b();
            }
        }
    }

    public v(w<CacheKey, PooledByteBuffer> wVar, i iVar, w0<e> w0Var) {
        this.a = wVar;
        this.f636b = iVar;
        this.c = w0Var;
    }

    @Override // b.f.j.p.w0
    public void b(l<e> lVar, x0 x0Var) {
        try {
            b.b();
            z0 o = x0Var.o();
            o.e(x0Var, "EncodedMemoryCacheProducer");
            CacheKey b2 = ((n) this.f636b).b(x0Var.e(), x0Var.b());
            Map<String, String> map = null;
            CloseableReference<PooledByteBuffer> closeableReference = x0Var.e().b(4) ? this.a.get(b2) : null;
            if (closeableReference != null) {
                e eVar = new e(closeableReference);
                if (o.g(x0Var, "EncodedMemoryCacheProducer")) {
                    map = f.of("cached_value_found", "true");
                }
                o.j(x0Var, "EncodedMemoryCacheProducer", map);
                o.c(x0Var, "EncodedMemoryCacheProducer", true);
                x0Var.n("memory_encoded");
                lVar.a(1.0f);
                lVar.b(eVar, 1);
                eVar.close();
                closeableReference.close();
            } else if (x0Var.q().g() >= ImageRequest.c.ENCODED_MEMORY_CACHE.g()) {
                o.j(x0Var, "EncodedMemoryCacheProducer", o.g(x0Var, "EncodedMemoryCacheProducer") ? f.of("cached_value_found", "false") : null);
                o.c(x0Var, "EncodedMemoryCacheProducer", false);
                x0Var.i("memory_encoded", "nil-result");
                lVar.b(null, 1);
                Class<CloseableReference> cls = CloseableReference.j;
                if (closeableReference != null) {
                    closeableReference.close();
                }
            } else {
                a aVar = new a(lVar, this.a, b2, x0Var.e().b(8), x0Var.g().getExperiments().e);
                if (o.g(x0Var, "EncodedMemoryCacheProducer")) {
                    map = f.of("cached_value_found", "false");
                }
                o.j(x0Var, "EncodedMemoryCacheProducer", map);
                this.c.b(aVar, x0Var);
                Class<CloseableReference> cls2 = CloseableReference.j;
                if (closeableReference != null) {
                    closeableReference.close();
                }
            }
        } finally {
            b.b();
        }
    }
}
