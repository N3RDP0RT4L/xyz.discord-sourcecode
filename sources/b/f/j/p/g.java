package b.f.j.p;

import android.util.Pair;
import b.f.j.c.i;
import b.f.j.c.n;
import b.f.j.j.c;
import com.facebook.cache.common.CacheKey;
import com.facebook.common.references.CloseableReference;
import com.facebook.imagepipeline.request.ImageRequest;
/* compiled from: BitmapMemoryCacheKeyMultiplexProducer.java */
/* loaded from: classes2.dex */
public class g extends l0<Pair<CacheKey, ImageRequest.c>, CloseableReference<c>> {
    public final i f;

    public g(i iVar, w0 w0Var) {
        super(w0Var, "BitmapMemoryCacheKeyMultiplexProducer", "multiplex_bmp_cnt");
        this.f = iVar;
    }

    @Override // b.f.j.p.l0
    public CloseableReference<c> c(CloseableReference<c> closeableReference) {
        return CloseableReference.n(closeableReference);
    }

    @Override // b.f.j.p.l0
    public Pair<CacheKey, ImageRequest.c> d(x0 x0Var) {
        return Pair.create(((n) this.f).a(x0Var.e(), x0Var.b()), x0Var.q());
    }
}
