package b.f.j.p;

import android.util.Pair;
import b.f.j.c.i;
import b.f.j.c.n;
import b.f.j.j.e;
import com.facebook.cache.common.CacheKey;
import com.facebook.imagepipeline.request.ImageRequest;
/* compiled from: EncodedCacheKeyMultiplexProducer.java */
/* loaded from: classes2.dex */
public class u extends l0<Pair<CacheKey, ImageRequest.c>, e> {
    public final i f;

    public u(i iVar, boolean z2, w0 w0Var) {
        super(w0Var, "EncodedCacheKeyMultiplexProducer", "multiplex_enc_cnt", z2);
        this.f = iVar;
    }

    @Override // b.f.j.p.l0
    public e c(e eVar) {
        return e.a(eVar);
    }

    @Override // b.f.j.p.l0
    public Pair<CacheKey, ImageRequest.c> d(x0 x0Var) {
        return Pair.create(((n) this.f).b(x0Var.e(), x0Var.b()), x0Var.q());
    }
}
