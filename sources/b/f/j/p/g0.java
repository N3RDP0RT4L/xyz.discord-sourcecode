package b.f.j.p;

import b.f.d.g.g;
import b.f.j.j.e;
import com.facebook.common.references.CloseableReference;
import com.facebook.imagepipeline.request.ImageRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Executor;
/* compiled from: LocalFetchProducer.java */
/* loaded from: classes2.dex */
public abstract class g0 implements w0<e> {
    public final Executor a;

    /* renamed from: b  reason: collision with root package name */
    public final g f608b;

    /* compiled from: LocalFetchProducer.java */
    /* loaded from: classes2.dex */
    public class a extends e1<e> {
        public final /* synthetic */ ImageRequest o;
        public final /* synthetic */ z0 p;
        public final /* synthetic */ x0 q;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(l lVar, z0 z0Var, x0 x0Var, String str, ImageRequest imageRequest, z0 z0Var2, x0 x0Var2) {
            super(lVar, z0Var, x0Var, str);
            this.o = imageRequest;
            this.p = z0Var2;
            this.q = x0Var2;
        }

        @Override // b.f.j.p.e1
        public void b(e eVar) {
            e eVar2 = eVar;
            if (eVar2 != null) {
                eVar2.close();
            }
        }

        @Override // b.f.j.p.e1
        public e d() throws Exception {
            e d = g0.this.d(this.o);
            if (d == null) {
                this.p.c(this.q, g0.this.e(), false);
                this.q.n("local");
                return null;
            }
            d.q();
            this.p.c(this.q, g0.this.e(), true);
            this.q.n("local");
            return d;
        }
    }

    /* compiled from: LocalFetchProducer.java */
    /* loaded from: classes2.dex */
    public class b extends e {
        public final /* synthetic */ e1 a;

        public b(g0 g0Var, e1 e1Var) {
            this.a = e1Var;
        }

        @Override // b.f.j.p.y0
        public void a() {
            this.a.a();
        }
    }

    public g0(Executor executor, g gVar) {
        this.a = executor;
        this.f608b = gVar;
    }

    @Override // b.f.j.p.w0
    public void b(l<e> lVar, x0 x0Var) {
        z0 o = x0Var.o();
        ImageRequest e = x0Var.e();
        x0Var.i("local", "fetch");
        a aVar = new a(lVar, o, x0Var, e(), e, o, x0Var);
        x0Var.f(new b(this, aVar));
        this.a.execute(aVar);
    }

    public e c(InputStream inputStream, int i) throws IOException {
        CloseableReference closeableReference;
        CloseableReference closeableReference2 = null;
        try {
            if (i <= 0) {
                closeableReference = CloseableReference.A(this.f608b.c(inputStream));
            } else {
                closeableReference = CloseableReference.A(this.f608b.d(inputStream, i));
            }
            closeableReference2 = closeableReference;
            e eVar = new e(closeableReference2);
            b.f.d.d.a.b(inputStream);
            if (closeableReference2 != null) {
                closeableReference2.close();
            }
            return eVar;
        } catch (Throwable th) {
            b.f.d.d.a.b(inputStream);
            Class<CloseableReference> cls = CloseableReference.j;
            if (closeableReference2 != null) {
                closeableReference2.close();
            }
            throw th;
        }
    }

    public abstract e d(ImageRequest imageRequest) throws IOException;

    public abstract String e();
}
