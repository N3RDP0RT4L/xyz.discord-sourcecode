package b.f.j.p;

import b.f.i.c;
import b.f.j.c.d;
import b.f.j.c.f;
import b.f.j.c.i;
import b.f.j.c.n;
import b.f.j.j.e;
import b.f.j.r.b;
import com.facebook.cache.common.CacheKey;
import com.facebook.imagepipeline.request.ImageRequest;
/* compiled from: EncodedProbeProducer.java */
/* loaded from: classes2.dex */
public class w implements w0<e> {
    public final f a;

    /* renamed from: b  reason: collision with root package name */
    public final f f637b;
    public final i c;
    public final w0<e> d;
    public final d<CacheKey> e;
    public final d<CacheKey> f;

    /* compiled from: EncodedProbeProducer.java */
    /* loaded from: classes2.dex */
    public static class a extends p<e, e> {
        public final x0 c;
        public final f d;
        public final f e;
        public final i f;
        public final d<CacheKey> g;
        public final d<CacheKey> h;

        public a(l<e> lVar, x0 x0Var, f fVar, f fVar2, i iVar, d<CacheKey> dVar, d<CacheKey> dVar2) {
            super(lVar);
            this.c = x0Var;
            this.d = fVar;
            this.e = fVar2;
            this.f = iVar;
            this.g = dVar;
            this.h = dVar2;
        }

        @Override // b.f.j.p.b
        public void i(Object obj, int i) {
            e eVar = (e) obj;
            try {
                b.b();
                if (!b.f(i) && eVar != null && !b.l(i, 10)) {
                    eVar.x();
                    if (eVar.l != c.a) {
                        ImageRequest e = this.c.e();
                        CacheKey b2 = ((n) this.f).b(e, this.c.b());
                        this.g.a(b2);
                        if ("memory_encoded".equals(this.c.l("origin"))) {
                            if (!this.h.b(b2)) {
                                (e.f2874b == ImageRequest.b.SMALL ? this.e : this.d).c(b2);
                                this.h.a(b2);
                            }
                        } else if ("disk".equals(this.c.l("origin"))) {
                            this.h.a(b2);
                        }
                        this.f628b.b(eVar, i);
                    }
                }
                this.f628b.b(eVar, i);
            } finally {
                b.b();
            }
        }
    }

    public w(f fVar, f fVar2, i iVar, d dVar, d dVar2, w0<e> w0Var) {
        this.a = fVar;
        this.f637b = fVar2;
        this.c = iVar;
        this.e = dVar;
        this.f = dVar2;
        this.d = w0Var;
    }

    @Override // b.f.j.p.w0
    public void b(l<e> lVar, x0 x0Var) {
        try {
            b.b();
            z0 o = x0Var.o();
            o.e(x0Var, "EncodedProbeProducer");
            a aVar = new a(lVar, x0Var, this.a, this.f637b, this.c, this.e, this.f);
            o.j(x0Var, "EncodedProbeProducer", null);
            b.b();
            this.d.b(aVar, x0Var);
            b.b();
        } finally {
            b.b();
        }
    }
}
