package b.f.j.p;

import b.f.j.d.d;
import b.f.j.e.k;
import com.facebook.imagepipeline.request.ImageRequest;
/* compiled from: SettableProducerContext.java */
/* loaded from: classes2.dex */
public class d1 extends d {
    public d1(ImageRequest imageRequest, x0 x0Var) {
        super(imageRequest, x0Var.getId(), x0Var.m(), x0Var.o(), x0Var.b(), x0Var.q(), x0Var.k(), x0Var.p(), x0Var.c(), x0Var.g());
    }

    public d1(ImageRequest imageRequest, String str, String str2, z0 z0Var, Object obj, ImageRequest.c cVar, boolean z2, boolean z3, d dVar, k kVar) {
        super(imageRequest, str, str2, z0Var, obj, cVar, z2, z3, dVar, kVar);
    }
}
