package b.f.j.p;

import android.content.res.Resources;
import b.f.d.g.g;
import java.util.concurrent.Executor;
/* compiled from: LocalResourceFetchProducer.java */
/* loaded from: classes2.dex */
public class i0 extends g0 {
    public final Resources c;

    public i0(Executor executor, g gVar, Resources resources) {
        super(executor, gVar);
        this.c = resources;
    }

    /* JADX WARN: Code restructure failed: missing block: B:10:0x003e, code lost:
        if (r1 == null) goto L12;
     */
    @Override // b.f.j.p.g0
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.f.j.j.e d(com.facebook.imagepipeline.request.ImageRequest r5) throws java.io.IOException {
        /*
            r4 = this;
            android.content.res.Resources r0 = r4.c
            android.net.Uri r1 = r5.c
            java.lang.String r1 = r1.getPath()
            java.util.Objects.requireNonNull(r1)
            r2 = 1
            java.lang.String r1 = r1.substring(r2)
            int r1 = java.lang.Integer.parseInt(r1)
            java.io.InputStream r0 = r0.openRawResource(r1)
            r1 = 0
            android.content.res.Resources r3 = r4.c     // Catch: java.lang.Throwable -> L36 android.content.res.Resources.NotFoundException -> L3d
            android.net.Uri r5 = r5.c     // Catch: java.lang.Throwable -> L36 android.content.res.Resources.NotFoundException -> L3d
            java.lang.String r5 = r5.getPath()     // Catch: java.lang.Throwable -> L36 android.content.res.Resources.NotFoundException -> L3d
            java.util.Objects.requireNonNull(r5)     // Catch: java.lang.Throwable -> L36 android.content.res.Resources.NotFoundException -> L3d
            java.lang.String r5 = r5.substring(r2)     // Catch: java.lang.Throwable -> L36 android.content.res.Resources.NotFoundException -> L3d
            int r5 = java.lang.Integer.parseInt(r5)     // Catch: java.lang.Throwable -> L36 android.content.res.Resources.NotFoundException -> L3d
            android.content.res.AssetFileDescriptor r1 = r3.openRawResourceFd(r5)     // Catch: java.lang.Throwable -> L36 android.content.res.Resources.NotFoundException -> L3d
            long r2 = r1.getLength()     // Catch: java.lang.Throwable -> L36 android.content.res.Resources.NotFoundException -> L3d
            int r5 = (int) r2
            goto L40
        L36:
            r5 = move-exception
            if (r1 == 0) goto L3c
            r1.close()     // Catch: java.io.IOException -> L3c
        L3c:
            throw r5
        L3d:
            r5 = -1
            if (r1 == 0) goto L43
        L40:
            r1.close()     // Catch: java.io.IOException -> L43
        L43:
            b.f.j.j.e r5 = r4.c(r0, r5)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: b.f.j.p.i0.d(com.facebook.imagepipeline.request.ImageRequest):b.f.j.j.e");
    }

    @Override // b.f.j.p.g0
    public String e() {
        return "LocalResourceFetchProducer";
    }
}
