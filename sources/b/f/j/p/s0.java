package b.f.j.p;

import b.f.d.d.f;
import b.f.j.c.i;
import b.f.j.c.n;
import b.f.j.c.w;
import b.f.j.j.c;
import b.f.j.q.b;
import com.facebook.cache.common.CacheKey;
import com.facebook.common.references.CloseableReference;
import com.facebook.imagepipeline.request.ImageRequest;
import java.util.Map;
/* compiled from: PostprocessedBitmapMemoryCacheProducer.java */
/* loaded from: classes2.dex */
public class s0 implements w0<CloseableReference<c>> {
    public final w<CacheKey, c> a;

    /* renamed from: b  reason: collision with root package name */
    public final i f633b;
    public final w0<CloseableReference<c>> c;

    /* compiled from: PostprocessedBitmapMemoryCacheProducer.java */
    /* loaded from: classes2.dex */
    public static class a extends p<CloseableReference<c>, CloseableReference<c>> {
        public final CacheKey c;
        public final boolean d;
        public final w<CacheKey, c> e;
        public final boolean f;

        public a(l<CloseableReference<c>> lVar, CacheKey cacheKey, boolean z2, w<CacheKey, c> wVar, boolean z3) {
            super(lVar);
            this.c = cacheKey;
            this.d = z2;
            this.e = wVar;
            this.f = z3;
        }

        @Override // b.f.j.p.b
        public void i(Object obj, int i) {
            CloseableReference<c> closeableReference = (CloseableReference) obj;
            CloseableReference<c> closeableReference2 = null;
            if (closeableReference == null) {
                if (b.e(i)) {
                    this.f628b.b(null, i);
                }
            } else if (!b.f(i) || this.d) {
                if (this.f) {
                    closeableReference2 = this.e.a(this.c, closeableReference);
                }
                try {
                    this.f628b.a(1.0f);
                    l<O> lVar = this.f628b;
                    if (closeableReference2 != null) {
                        closeableReference = closeableReference2;
                    }
                    lVar.b(closeableReference, i);
                } finally {
                    Class<CloseableReference> cls = CloseableReference.j;
                    if (closeableReference2 != null) {
                        closeableReference2.close();
                    }
                }
            }
        }
    }

    public s0(w<CacheKey, c> wVar, i iVar, w0<CloseableReference<c>> w0Var) {
        this.a = wVar;
        this.f633b = iVar;
        this.c = w0Var;
    }

    @Override // b.f.j.p.w0
    public void b(l<CloseableReference<c>> lVar, x0 x0Var) {
        z0 o = x0Var.o();
        ImageRequest e = x0Var.e();
        Object b2 = x0Var.b();
        b bVar = e.f2875s;
        if (bVar == null || bVar.getPostprocessorCacheKey() == null) {
            this.c.b(lVar, x0Var);
            return;
        }
        o.e(x0Var, "PostprocessedBitmapMemoryCacheProducer");
        CacheKey c = ((n) this.f633b).c(e, b2);
        Map<String, String> map = null;
        CloseableReference<c> closeableReference = x0Var.e().b(1) ? this.a.get(c) : null;
        if (closeableReference != null) {
            if (o.g(x0Var, "PostprocessedBitmapMemoryCacheProducer")) {
                map = f.of("cached_value_found", "true");
            }
            o.j(x0Var, "PostprocessedBitmapMemoryCacheProducer", map);
            o.c(x0Var, "PostprocessedBitmapMemoryCacheProducer", true);
            x0Var.i("memory_bitmap", "postprocessed");
            lVar.a(1.0f);
            lVar.b(closeableReference, 1);
            closeableReference.close();
            return;
        }
        a aVar = new a(lVar, c, bVar instanceof b.f.j.q.c, this.a, x0Var.e().b(2));
        if (o.g(x0Var, "PostprocessedBitmapMemoryCacheProducer")) {
            map = f.of("cached_value_found", "false");
        }
        o.j(x0Var, "PostprocessedBitmapMemoryCacheProducer", map);
        this.c.b(aVar, x0Var);
    }
}
