package b.f.j.p;

import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import b.c.a.a0.d;
import b.f.d.e.a;
import b.f.d.g.g;
import b.f.d.l.b;
import b.f.j.j.e;
import com.facebook.imagepipeline.request.ImageRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.Executor;
/* compiled from: LocalContentUriThumbnailFetchProducer.java */
/* loaded from: classes2.dex */
public class f0 extends g0 implements k1<e> {
    public static final String[] c = {"_id", "_data"};
    public static final String[] d = {"_data"};
    public static final Rect e = new Rect(0, 0, 512, 384);
    public static final Rect f = new Rect(0, 0, 96, 96);
    public final ContentResolver g;

    public f0(Executor executor, g gVar, ContentResolver contentResolver) {
        super(executor, gVar);
        this.g = contentResolver;
    }

    @Override // b.f.j.p.k1
    public boolean a(b.f.j.d.e eVar) {
        Rect rect = e;
        return d.S0(rect.width(), rect.height(), eVar);
    }

    @Override // b.f.j.p.g0
    public e d(ImageRequest imageRequest) throws IOException {
        b.f.j.d.e eVar;
        Cursor query;
        e f2;
        Uri uri = imageRequest.c;
        if (!b.b(uri) || (eVar = imageRequest.j) == null || (query = this.g.query(uri, c, null, null, null)) == null) {
            return null;
        }
        try {
            if (!query.moveToFirst() || (f2 = f(eVar, query.getLong(query.getColumnIndex("_id")))) == null) {
                return null;
            }
            String string = query.getString(query.getColumnIndex("_data"));
            int i = 0;
            if (string != null) {
                try {
                    i = d.s0(new ExifInterface(string).getAttributeInt(androidx.exifinterface.media.ExifInterface.TAG_ORIENTATION, 1));
                } catch (IOException e2) {
                    a.d(f0.class, e2, "Unable to retrieve thumbnail rotation for %s", string);
                }
            }
            f2.m = i;
            return f2;
        } finally {
            query.close();
        }
    }

    @Override // b.f.j.p.g0
    public String e() {
        return "LocalContentUriThumbnailFetchProducer";
    }

    public final e f(b.f.j.d.e eVar, long j) throws IOException {
        int i;
        Cursor queryMiniThumbnail;
        Rect rect = f;
        if (d.S0(rect.width(), rect.height(), eVar)) {
            i = 3;
        } else {
            Rect rect2 = e;
            i = d.S0(rect2.width(), rect2.height(), eVar) ? 1 : 0;
        }
        if (i == 0 || (queryMiniThumbnail = MediaStore.Images.Thumbnails.queryMiniThumbnail(this.g, j, i, d)) == null) {
            return null;
        }
        try {
            if (queryMiniThumbnail.moveToFirst()) {
                String string = queryMiniThumbnail.getString(queryMiniThumbnail.getColumnIndex("_data"));
                Objects.requireNonNull(string);
                if (new File(string).exists()) {
                    return c(new FileInputStream(string), (int) new File(string).length());
                }
            }
            return null;
        } finally {
            queryMiniThumbnail.close();
        }
    }
}
