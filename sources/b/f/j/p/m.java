package b.f.j.p;

import android.net.Uri;
import android.util.Base64;
import b.c.a.a0.d;
import b.f.d.b.a;
import b.f.d.g.g;
import b.f.j.j.e;
import com.facebook.imagepipeline.request.ImageRequest;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Objects;
/* compiled from: DataFetchProducer.java */
/* loaded from: classes2.dex */
public class m extends g0 {
    public m(g gVar) {
        super(a.j, gVar);
    }

    @Override // b.f.j.p.g0
    public e d(ImageRequest imageRequest) throws IOException {
        boolean z2;
        byte[] bArr;
        String uri = imageRequest.c.toString();
        d.i(Boolean.valueOf(uri.substring(0, 5).equals("data:")));
        int indexOf = uri.indexOf(44);
        String substring = uri.substring(indexOf + 1, uri.length());
        String substring2 = uri.substring(0, indexOf);
        if (!substring2.contains(";")) {
            z2 = false;
        } else {
            String[] split = substring2.split(";");
            z2 = split[split.length - 1].equals("base64");
        }
        if (z2) {
            bArr = Base64.decode(substring, 0);
        } else {
            String decode = Uri.decode(substring);
            Objects.requireNonNull(decode);
            bArr = decode.getBytes();
        }
        return c(new ByteArrayInputStream(bArr), bArr.length);
    }

    @Override // b.f.j.p.g0
    public String e() {
        return "DataFetchProducer";
    }
}
