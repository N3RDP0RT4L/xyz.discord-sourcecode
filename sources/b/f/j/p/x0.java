package b.f.j.p;

import b.f.j.d.d;
import b.f.j.e.k;
import b.f.j.j.f;
import com.facebook.imagepipeline.request.ImageRequest;
import java.util.Map;
/* compiled from: ProducerContext.java */
/* loaded from: classes2.dex */
public interface x0 {
    Map<String, Object> a();

    Object b();

    d c();

    <E> void d(String str, E e);

    ImageRequest e();

    void f(y0 y0Var);

    k g();

    String getId();

    void h(f fVar);

    void i(String str, String str2);

    void j(Map<String, ?> map);

    boolean k();

    <E> E l(String str);

    String m();

    void n(String str);

    z0 o();

    boolean p();

    ImageRequest.c q();
}
