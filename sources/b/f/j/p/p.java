package b.f.j.p;
/* compiled from: DelegatingConsumer.java */
/* loaded from: classes2.dex */
public abstract class p<I, O> extends b<I> {

    /* renamed from: b  reason: collision with root package name */
    public final l<O> f628b;

    public p(l<O> lVar) {
        this.f628b = lVar;
    }

    @Override // b.f.j.p.b
    public void g() {
        this.f628b.d();
    }

    @Override // b.f.j.p.b
    public void h(Throwable th) {
        this.f628b.c(th);
    }

    @Override // b.f.j.p.b
    public void j(float f) {
        this.f628b.a(f);
    }
}
