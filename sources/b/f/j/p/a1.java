package b.f.j.p;

import java.util.Map;
/* compiled from: ProducerListener.java */
/* loaded from: classes2.dex */
public interface a1 {
    void b(String str, String str2);

    void d(String str, String str2, Map<String, String> map);

    void e(String str, String str2, boolean z2);

    boolean f(String str);

    void h(String str, String str2, String str3);

    void i(String str, String str2, Map<String, String> map);

    void j(String str, String str2, Throwable th, Map<String, String> map);
}
