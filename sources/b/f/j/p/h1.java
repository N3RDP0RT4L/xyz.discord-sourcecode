package b.f.j.p;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Objects;
import java.util.concurrent.Executor;
/* compiled from: ThreadHandoffProducerQueueImpl.java */
/* loaded from: classes2.dex */
public class h1 implements g1 {
    public final Deque<Runnable> a = new ArrayDeque();

    /* renamed from: b  reason: collision with root package name */
    public final Executor f610b;

    public h1(Executor executor) {
        Objects.requireNonNull(executor);
        this.f610b = executor;
    }
}
