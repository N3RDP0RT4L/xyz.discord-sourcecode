package b.f.j.p;

import android.util.Pair;
import java.util.Objects;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
/* compiled from: ThrottlingProducer.java */
/* loaded from: classes2.dex */
public class i1<T> implements w0<T> {
    public final w0<T> a;
    public final Executor d;
    public final ConcurrentLinkedQueue<Pair<l<T>, x0>> c = new ConcurrentLinkedQueue<>();

    /* renamed from: b  reason: collision with root package name */
    public int f612b = 0;

    /* compiled from: ThrottlingProducer.java */
    /* loaded from: classes2.dex */
    public class b extends p<T, T> {

        /* compiled from: ThrottlingProducer.java */
        /* loaded from: classes2.dex */
        public class a implements Runnable {
            public final /* synthetic */ Pair j;

            public a(Pair pair) {
                this.j = pair;
            }

            @Override // java.lang.Runnable
            public void run() {
                i1 i1Var = i1.this;
                Pair pair = this.j;
                x0 x0Var = (x0) pair.second;
                Objects.requireNonNull(i1Var);
                x0Var.o().j(x0Var, "ThrottlingProducer", null);
                i1Var.a.b(new b((l) pair.first, null), x0Var);
            }
        }

        public b(l lVar, a aVar) {
            super(lVar);
        }

        @Override // b.f.j.p.p, b.f.j.p.b
        public void g() {
            this.f628b.d();
            n();
        }

        @Override // b.f.j.p.p, b.f.j.p.b
        public void h(Throwable th) {
            this.f628b.c(th);
            n();
        }

        @Override // b.f.j.p.b
        public void i(T t, int i) {
            this.f628b.b(t, i);
            if (b.f.j.p.b.e(i)) {
                n();
            }
        }

        public final void n() {
            Pair<l<T>, x0> poll;
            synchronized (i1.this) {
                poll = i1.this.c.poll();
                if (poll == null) {
                    i1 i1Var = i1.this;
                    i1Var.f612b--;
                }
            }
            if (poll != null) {
                i1.this.d.execute(new a(poll));
            }
        }
    }

    public i1(int i, Executor executor, w0<T> w0Var) {
        Objects.requireNonNull(executor);
        this.d = executor;
        Objects.requireNonNull(w0Var);
        this.a = w0Var;
    }

    @Override // b.f.j.p.w0
    public void b(l<T> lVar, x0 x0Var) {
        boolean z2;
        x0Var.o().e(x0Var, "ThrottlingProducer");
        synchronized (this) {
            int i = this.f612b;
            z2 = true;
            if (i >= 5) {
                this.c.add(Pair.create(lVar, x0Var));
            } else {
                this.f612b = i + 1;
                z2 = false;
            }
        }
        if (!z2) {
            x0Var.o().j(x0Var, "ThrottlingProducer", null);
            this.a.b(new b(lVar, null), x0Var);
        }
    }
}
