package b.f.j.p;

import android.util.Pair;
import b.f.j.d.d;
import b.f.j.p.l0;
import java.util.List;
/* compiled from: MultiplexProducer.java */
/* loaded from: classes2.dex */
public class m0 extends e {
    public final /* synthetic */ Pair a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ l0.b f621b;

    public m0(l0.b bVar, Pair pair) {
        this.f621b = bVar;
        this.a = pair;
    }

    @Override // b.f.j.p.y0
    public void a() {
        boolean remove;
        List<y0> list;
        List<y0> list2;
        List<y0> list3;
        d dVar;
        synchronized (this.f621b) {
            remove = this.f621b.f619b.remove(this.a);
            list = null;
            if (!remove) {
                dVar = null;
            } else if (this.f621b.f619b.isEmpty()) {
                dVar = this.f621b.f;
            } else {
                List<y0> k = this.f621b.k();
                list3 = this.f621b.l();
                list2 = this.f621b.j();
                dVar = null;
                list = k;
            }
            list3 = null;
            list2 = null;
        }
        d.s(list);
        d.t(list3);
        d.r(list2);
        if (dVar != null) {
            if (!l0.this.c || dVar.k()) {
                dVar.u();
            } else {
                d.t(dVar.v(d.LOW));
            }
        }
        if (remove) {
            ((l) this.a.first).d();
        }
    }

    @Override // b.f.j.p.e, b.f.j.p.y0
    public void b() {
        d.r(this.f621b.j());
    }

    @Override // b.f.j.p.e, b.f.j.p.y0
    public void c() {
        d.t(this.f621b.l());
    }

    @Override // b.f.j.p.e, b.f.j.p.y0
    public void d() {
        d.s(this.f621b.k());
    }
}
