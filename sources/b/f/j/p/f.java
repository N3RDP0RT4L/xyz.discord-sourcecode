package b.f.j.p;

import b.f.j.c.i;
import b.f.j.c.w;
import b.f.j.j.c;
import com.facebook.cache.common.CacheKey;
import com.facebook.common.references.CloseableReference;
/* compiled from: BitmapMemoryCacheGetProducer.java */
/* loaded from: classes2.dex */
public class f extends h {
    public f(w<CacheKey, c> wVar, i iVar, w0<CloseableReference<c>> w0Var) {
        super(wVar, iVar, w0Var);
    }

    @Override // b.f.j.p.h
    public String c() {
        return "pipe_ui";
    }

    @Override // b.f.j.p.h
    public String d() {
        return "BitmapMemoryCacheGetProducer";
    }

    @Override // b.f.j.p.h
    public l<CloseableReference<c>> e(l<CloseableReference<c>> lVar, CacheKey cacheKey, boolean z2) {
        return lVar;
    }
}
