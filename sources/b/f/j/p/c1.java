package b.f.j.p;

import b.f.d.d.f;
import b.f.d.g.g;
import b.f.d.g.i;
import b.f.j.j.e;
import b.f.j.p.c0;
import b.f.j.s.c;
import com.facebook.common.references.CloseableReference;
import com.facebook.imagepipeline.memory.MemoryPooledByteBufferOutputStream;
import com.facebook.imagepipeline.request.ImageRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executor;
/* compiled from: ResizeAndRotateProducer.java */
/* loaded from: classes2.dex */
public class c1 implements w0<e> {
    public final Executor a;

    /* renamed from: b  reason: collision with root package name */
    public final g f603b;
    public final w0<e> c;
    public final boolean d;
    public final c e;

    /* compiled from: ResizeAndRotateProducer.java */
    /* loaded from: classes2.dex */
    public class a extends p<e, e> {
        public final boolean c;
        public final c d;
        public final x0 e;
        public boolean f = false;
        public final c0 g;

        /* compiled from: ResizeAndRotateProducer.java */
        /* renamed from: b.f.j.p.c1$a$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public class C0073a implements c0.c {
            public C0073a(c1 c1Var) {
            }

            @Override // b.f.j.p.c0.c
            public void a(e eVar, int i) {
                b.f.j.s.a c;
                a aVar = a.this;
                c cVar = aVar.d;
                eVar.x();
                b.f.j.s.b createImageTranscoder = cVar.createImageTranscoder(eVar.l, a.this.c);
                Objects.requireNonNull(createImageTranscoder);
                aVar.e.o().e(aVar.e, "ResizeAndRotateProducer");
                ImageRequest e = aVar.e.e();
                i a = c1.this.f603b.a();
                try {
                    try {
                        c = createImageTranscoder.c(eVar, a, e.k, e.j, null, 85);
                    } catch (Exception e2) {
                        aVar.e.o().k(aVar.e, "ResizeAndRotateProducer", e2, null);
                        if (b.f.j.p.b.e(i)) {
                            aVar.f628b.c(e2);
                        }
                    }
                    if (c.a != 2) {
                        Map<String, String> n = aVar.n(eVar, e.j, c, createImageTranscoder.a());
                        CloseableReference A = CloseableReference.A(((MemoryPooledByteBufferOutputStream) a).b());
                        try {
                            e eVar2 = new e(A);
                            eVar2.l = b.f.i.b.a;
                            eVar2.q();
                            aVar.e.o().j(aVar.e, "ResizeAndRotateProducer", n);
                            if (c.a != 1) {
                                i |= 16;
                            }
                            aVar.f628b.b(eVar2, i);
                            eVar2.close();
                        } finally {
                            if (A != null) {
                                A.close();
                            }
                        }
                    } else {
                        throw new RuntimeException("Error while transcoding the image");
                    }
                } finally {
                    a.close();
                }
            }
        }

        /* compiled from: ResizeAndRotateProducer.java */
        /* loaded from: classes2.dex */
        public class b extends e {
            public final /* synthetic */ l a;

            public b(c1 c1Var, l lVar) {
                this.a = lVar;
            }

            @Override // b.f.j.p.y0
            public void a() {
                a.this.g.a();
                a.this.f = true;
                this.a.d();
            }

            @Override // b.f.j.p.e, b.f.j.p.y0
            public void b() {
                if (a.this.e.p()) {
                    a.this.g.d();
                }
            }
        }

        public a(l<e> lVar, x0 x0Var, boolean z2, c cVar) {
            super(lVar);
            this.e = x0Var;
            Objects.requireNonNull(x0Var.e());
            this.c = z2;
            this.d = cVar;
            this.g = new c0(c1.this.a, new C0073a(c1.this), 100);
            x0Var.f(new b(c1.this, lVar));
        }

        /* JADX WARN: Code restructure failed: missing block: B:24:0x0075, code lost:
            if (r6 != false) goto L25;
         */
        @Override // b.f.j.p.b
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public void i(java.lang.Object r10, int r11) {
            /*
                Method dump skipped, instructions count: 258
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: b.f.j.p.c1.a.i(java.lang.Object, int):void");
        }

        public final Map<String, String> n(e eVar, b.f.j.d.e eVar2, b.f.j.s.a aVar, String str) {
            String str2;
            long j;
            if (!this.e.o().g(this.e, "ResizeAndRotateProducer")) {
                return null;
            }
            StringBuilder sb = new StringBuilder();
            eVar.x();
            sb.append(eVar.o);
            sb.append("x");
            eVar.x();
            sb.append(eVar.p);
            String sb2 = sb.toString();
            if (eVar2 != null) {
                str2 = eVar2.a + "x" + eVar2.f562b;
            } else {
                str2 = "Unspecified";
            }
            HashMap hashMap = new HashMap();
            eVar.x();
            hashMap.put("Image format", String.valueOf(eVar.l));
            hashMap.put("Original size", sb2);
            hashMap.put("Requested size", str2);
            c0 c0Var = this.g;
            synchronized (c0Var) {
                j = c0Var.j - c0Var.i;
            }
            hashMap.put("queueTime", String.valueOf(j));
            hashMap.put("Transcoder id", str);
            hashMap.put("Transcoding result", String.valueOf(aVar));
            return new f(hashMap);
        }
    }

    public c1(Executor executor, g gVar, w0<e> w0Var, boolean z2, c cVar) {
        Objects.requireNonNull(executor);
        this.a = executor;
        Objects.requireNonNull(gVar);
        this.f603b = gVar;
        Objects.requireNonNull(w0Var);
        this.c = w0Var;
        Objects.requireNonNull(cVar);
        this.e = cVar;
        this.d = z2;
    }

    @Override // b.f.j.p.w0
    public void b(l<e> lVar, x0 x0Var) {
        this.c.b(new a(lVar, x0Var, this.d, this.e), x0Var);
    }
}
