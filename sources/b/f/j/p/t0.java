package b.f.j.p;

import android.graphics.Bitmap;
import b.f.d.d.f;
import com.facebook.common.references.CloseableReference;
import com.facebook.imagepipeline.bitmaps.PlatformBitmapFactory;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executor;
/* compiled from: PostprocessorProducer.java */
/* loaded from: classes2.dex */
public class t0 implements w0<CloseableReference<b.f.j.j.c>> {
    public final w0<CloseableReference<b.f.j.j.c>> a;

    /* renamed from: b  reason: collision with root package name */
    public final PlatformBitmapFactory f635b;
    public final Executor c;

    /* compiled from: PostprocessorProducer.java */
    /* loaded from: classes2.dex */
    public class b extends p<CloseableReference<b.f.j.j.c>, CloseableReference<b.f.j.j.c>> {
        public final z0 c;
        public final x0 d;
        public final b.f.j.q.b e;
        public boolean f;
        public CloseableReference<b.f.j.j.c> g = null;
        public int h = 0;
        public boolean i = false;
        public boolean j = false;

        /* compiled from: PostprocessorProducer.java */
        /* loaded from: classes2.dex */
        public class a extends e {
            public a(t0 t0Var) {
            }

            @Override // b.f.j.p.y0
            public void a() {
                b bVar = b.this;
                if (bVar.o()) {
                    bVar.f628b.d();
                }
            }
        }

        public b(l<CloseableReference<b.f.j.j.c>> lVar, z0 z0Var, b.f.j.q.b bVar, x0 x0Var) {
            super(lVar);
            this.c = z0Var;
            this.e = bVar;
            this.d = x0Var;
            x0Var.f(new a(t0.this));
        }

        public static void n(b bVar, CloseableReference closeableReference, int i) {
            Objects.requireNonNull(bVar);
            b.c.a.a0.d.i(Boolean.valueOf(CloseableReference.y(closeableReference)));
            if (!(((b.f.j.j.c) closeableReference.u()) instanceof b.f.j.j.d)) {
                bVar.q(closeableReference, i);
                return;
            }
            bVar.c.e(bVar.d, "PostprocessorProducer");
            CloseableReference<b.f.j.j.c> closeableReference2 = null;
            try {
                try {
                    closeableReference2 = bVar.r((b.f.j.j.c) closeableReference.u());
                    z0 z0Var = bVar.c;
                    x0 x0Var = bVar.d;
                    z0Var.j(x0Var, "PostprocessorProducer", bVar.p(z0Var, x0Var, bVar.e));
                    bVar.q(closeableReference2, i);
                } catch (Exception e) {
                    z0 z0Var2 = bVar.c;
                    x0 x0Var2 = bVar.d;
                    z0Var2.k(x0Var2, "PostprocessorProducer", e, bVar.p(z0Var2, x0Var2, bVar.e));
                    if (bVar.o()) {
                        bVar.f628b.c(e);
                    }
                }
            } finally {
                if (closeableReference2 != null) {
                    closeableReference2.close();
                }
            }
        }

        @Override // b.f.j.p.p, b.f.j.p.b
        public void g() {
            if (o()) {
                this.f628b.d();
            }
        }

        @Override // b.f.j.p.p, b.f.j.p.b
        public void h(Throwable th) {
            if (o()) {
                this.f628b.c(th);
            }
        }

        @Override // b.f.j.p.b
        public void i(Object obj, int i) {
            CloseableReference closeableReference = (CloseableReference) obj;
            if (CloseableReference.y(closeableReference)) {
                synchronized (this) {
                    if (!this.f) {
                        CloseableReference<b.f.j.j.c> closeableReference2 = this.g;
                        this.g = CloseableReference.n(closeableReference);
                        this.h = i;
                        this.i = true;
                        boolean s2 = s();
                        if (closeableReference2 != null) {
                            closeableReference2.close();
                        }
                        if (s2) {
                            t0.this.c.execute(new u0(this));
                        }
                    }
                }
            } else if (b.f.j.p.b.e(i)) {
                q(null, i);
            }
        }

        public final boolean o() {
            synchronized (this) {
                if (this.f) {
                    return false;
                }
                CloseableReference<b.f.j.j.c> closeableReference = this.g;
                this.g = null;
                this.f = true;
                Class<CloseableReference> cls = CloseableReference.j;
                if (closeableReference != null) {
                    closeableReference.close();
                }
                return true;
            }
        }

        public final Map<String, String> p(z0 z0Var, x0 x0Var, b.f.j.q.b bVar) {
            if (!z0Var.g(x0Var, "PostprocessorProducer")) {
                return null;
            }
            return f.of("Postprocessor", bVar.getName());
        }

        /* JADX WARN: Code restructure failed: missing block: B:7:0x000a, code lost:
            if (r1 != false) goto L10;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public final void q(com.facebook.common.references.CloseableReference<b.f.j.j.c> r3, int r4) {
            /*
                r2 = this;
                boolean r0 = b.f.j.p.b.e(r4)
                if (r0 != 0) goto L10
                monitor-enter(r2)
                boolean r1 = r2.f     // Catch: java.lang.Throwable -> Ld
                monitor-exit(r2)
                if (r1 == 0) goto L18
                goto L10
            Ld:
                r3 = move-exception
                monitor-exit(r2)
                throw r3
            L10:
                if (r0 == 0) goto L1d
                boolean r0 = r2.o()
                if (r0 == 0) goto L1d
            L18:
                b.f.j.p.l<O> r0 = r2.f628b
                r0.b(r3, r4)
            L1d:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.f.j.p.t0.b.q(com.facebook.common.references.CloseableReference, int):void");
        }

        public final CloseableReference<b.f.j.j.c> r(b.f.j.j.c cVar) {
            b.f.j.j.d dVar = (b.f.j.j.d) cVar;
            CloseableReference<Bitmap> process = this.e.process(dVar.m, t0.this.f635b);
            try {
                b.f.j.j.d dVar2 = new b.f.j.j.d(process, cVar.b(), dVar.o, dVar.p);
                dVar2.e(dVar.k);
                CloseableReference<b.f.j.j.c> A = CloseableReference.A(dVar2);
                if (process != null) {
                    process.close();
                }
                return A;
            } catch (Throwable th) {
                Class<CloseableReference> cls = CloseableReference.j;
                if (process != null) {
                    process.close();
                }
                throw th;
            }
        }

        public final synchronized boolean s() {
            if (this.f || !this.i || this.j || !CloseableReference.y(this.g)) {
                return false;
            }
            this.j = true;
            return true;
        }
    }

    /* compiled from: PostprocessorProducer.java */
    /* loaded from: classes2.dex */
    public class c extends p<CloseableReference<b.f.j.j.c>, CloseableReference<b.f.j.j.c>> implements b.f.j.q.d {
        public boolean c = false;
        public CloseableReference<b.f.j.j.c> d = null;

        public c(t0 t0Var, b bVar, b.f.j.q.c cVar, x0 x0Var, a aVar) {
            super(bVar);
            cVar.a(this);
            x0Var.f(new v0(this, t0Var));
        }

        @Override // b.f.j.p.p, b.f.j.p.b
        public void g() {
            if (n()) {
                this.f628b.d();
            }
        }

        @Override // b.f.j.p.p, b.f.j.p.b
        public void h(Throwable th) {
            if (n()) {
                this.f628b.c(th);
            }
        }

        @Override // b.f.j.p.b
        public void i(Object obj, int i) {
            CloseableReference closeableReference = (CloseableReference) obj;
            if (!b.f.j.p.b.f(i)) {
                synchronized (this) {
                    if (!this.c) {
                        CloseableReference<b.f.j.j.c> closeableReference2 = this.d;
                        this.d = CloseableReference.n(closeableReference);
                        if (closeableReference2 != null) {
                            closeableReference2.close();
                        }
                    }
                }
                synchronized (this) {
                    if (!this.c) {
                        CloseableReference n = CloseableReference.n(this.d);
                        try {
                            this.f628b.b(n, 0);
                        } finally {
                            if (n != null) {
                                n.close();
                            }
                        }
                    }
                }
            }
        }

        public final boolean n() {
            synchronized (this) {
                if (this.c) {
                    return false;
                }
                CloseableReference<b.f.j.j.c> closeableReference = this.d;
                this.d = null;
                this.c = true;
                Class<CloseableReference> cls = CloseableReference.j;
                if (closeableReference != null) {
                    closeableReference.close();
                }
                return true;
            }
        }
    }

    /* compiled from: PostprocessorProducer.java */
    /* loaded from: classes2.dex */
    public class d extends p<CloseableReference<b.f.j.j.c>, CloseableReference<b.f.j.j.c>> {
        public d(t0 t0Var, b bVar, a aVar) {
            super(bVar);
        }

        @Override // b.f.j.p.b
        public void i(Object obj, int i) {
            CloseableReference closeableReference = (CloseableReference) obj;
            if (!b.f.j.p.b.f(i)) {
                this.f628b.b(closeableReference, i);
            }
        }
    }

    public t0(w0<CloseableReference<b.f.j.j.c>> w0Var, PlatformBitmapFactory platformBitmapFactory, Executor executor) {
        Objects.requireNonNull(w0Var);
        this.a = w0Var;
        this.f635b = platformBitmapFactory;
        Objects.requireNonNull(executor);
        this.c = executor;
    }

    @Override // b.f.j.p.w0
    public void b(l<CloseableReference<b.f.j.j.c>> lVar, x0 x0Var) {
        l<CloseableReference<b.f.j.j.c>> lVar2;
        z0 o = x0Var.o();
        b.f.j.q.b bVar = x0Var.e().f2875s;
        Objects.requireNonNull(bVar);
        b bVar2 = new b(lVar, o, bVar, x0Var);
        if (bVar instanceof b.f.j.q.c) {
            lVar2 = new c(this, bVar2, (b.f.j.q.c) bVar, x0Var, null);
        } else {
            lVar2 = new d(this, bVar2, null);
        }
        this.a.b(lVar2, x0Var);
    }
}
