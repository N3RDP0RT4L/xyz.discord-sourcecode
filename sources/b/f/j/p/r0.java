package b.f.j.p;

import android.net.Uri;
import androidx.annotation.VisibleForTesting;
import b.f.d.g.g;
import b.f.i.c;
import b.f.j.c.f;
import b.f.j.c.i;
import b.f.j.c.n;
import b.f.j.j.e;
import com.facebook.cache.common.CacheKey;
import com.facebook.common.references.CloseableReference;
import com.facebook.imagepipeline.memory.MemoryPooledByteBufferOutputStream;
import com.facebook.imagepipeline.request.ImageRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
/* compiled from: PartialDiskCacheProducer.java */
/* loaded from: classes2.dex */
public class r0 implements w0<e> {
    public final f a;

    /* renamed from: b  reason: collision with root package name */
    public final i f631b;
    public final g c;
    public final b.f.d.g.a d;
    public final w0<e> e;

    /* compiled from: PartialDiskCacheProducer.java */
    /* loaded from: classes2.dex */
    public static class a extends p<e, e> {
        public final f c;
        public final CacheKey d;
        public final g e;
        public final b.f.d.g.a f;
        public final e g;
        public final boolean h;

        public a(l lVar, f fVar, CacheKey cacheKey, g gVar, b.f.d.g.a aVar, e eVar, boolean z2, p0 p0Var) {
            super(lVar);
            this.c = fVar;
            this.d = cacheKey;
            this.e = gVar;
            this.f = aVar;
            this.g = eVar;
            this.h = z2;
        }

        /* JADX WARN: Finally extract failed */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v8, types: [b.f.j.c.f] */
        /* JADX WARN: Type inference failed for: r3v0, types: [b.f.j.p.p, b.f.j.p.r0$a] */
        /* JADX WARN: Type inference failed for: r4v1, types: [b.f.j.j.e, java.lang.Object] */
        /* JADX WARN: Type inference failed for: r4v2, types: [b.f.j.j.e] */
        /* JADX WARN: Type inference failed for: r4v5, types: [java.lang.Object, b.f.j.c.f] */
        @Override // b.f.j.p.b
        public void i(Object obj, int i) {
            ?? r4 = (e) obj;
            if (!b.f(i)) {
                e eVar = this.g;
                if (!(eVar == null || r4 == 0)) {
                    try {
                        if (r4.f586s != null) {
                            try {
                                p(o(eVar, r4));
                            } catch (IOException e) {
                                b.f.d.e.a.f("PartialDiskCacheProducer", "Error while merging image data", e);
                                this.f628b.c(e);
                            }
                            r4.close();
                            this.g.close();
                            r4 = this.c;
                            CacheKey cacheKey = this.d;
                            Objects.requireNonNull(r4);
                            Objects.requireNonNull(cacheKey);
                            r4.f.c(cacheKey);
                            try {
                                z.g.a(new b.f.j.c.g(r4, null, cacheKey), r4.e);
                                return;
                            } catch (Exception e2) {
                                b.f.d.e.a.n(f.class, e2, "Failed to schedule disk-cache remove for %s", cacheKey.b());
                                z.g.c(e2);
                                return;
                            }
                        }
                    } catch (Throwable th) {
                        r4.close();
                        this.g.close();
                        throw th;
                    }
                }
                if (this.h && b.m(i, 8) && b.e(i) && r4 != 0) {
                    r4.x();
                    if (r4.l != c.a) {
                        this.c.f(this.d, r4);
                        this.f628b.b(r4, i);
                        return;
                    }
                }
                this.f628b.b(r4, i);
            }
        }

        public final void n(InputStream inputStream, OutputStream outputStream, int i) throws IOException {
            byte[] bArr = this.f.get(16384);
            int i2 = i;
            while (i2 > 0) {
                try {
                    int read = inputStream.read(bArr, 0, Math.min(16384, i2));
                    if (read < 0) {
                        break;
                    } else if (read > 0) {
                        outputStream.write(bArr, 0, read);
                        i2 -= read;
                    }
                } finally {
                    this.f.release(bArr);
                }
            }
            if (i2 > 0) {
                throw new IOException(String.format(null, "Failed to read %d bytes - finished %d short", Integer.valueOf(i), Integer.valueOf(i2)));
            }
        }

        public final b.f.d.g.i o(e eVar, e eVar2) throws IOException {
            b.f.j.d.a aVar = eVar2.f586s;
            Objects.requireNonNull(aVar);
            int i = aVar.a;
            b.f.d.g.i e = this.e.e(eVar2.n() + i);
            n(eVar.f(), e, i);
            n(eVar2.f(), e, eVar2.n());
            return e;
        }

        public final void p(b.f.d.g.i iVar) {
            Throwable th;
            CloseableReference A = CloseableReference.A(((MemoryPooledByteBufferOutputStream) iVar).b());
            e eVar = null;
            try {
                eVar = new e(A);
                try {
                    eVar.q();
                    this.f628b.b(eVar, 1);
                    eVar.close();
                    if (A != null) {
                        A.close();
                    }
                } catch (Throwable th2) {
                    th = th2;
                    if (eVar != null) {
                        eVar.close();
                    }
                    if (A != null) {
                        A.close();
                    }
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
            }
        }
    }

    public r0(f fVar, i iVar, g gVar, b.f.d.g.a aVar, w0<e> w0Var) {
        this.a = fVar;
        this.f631b = iVar;
        this.c = gVar;
        this.d = aVar;
        this.e = w0Var;
    }

    @VisibleForTesting
    public static Map<String, String> c(z0 z0Var, x0 x0Var, boolean z2, int i) {
        if (!z0Var.g(x0Var, "PartialDiskCacheProducer")) {
            return null;
        }
        if (z2) {
            return b.f.d.d.f.of("cached_value_found", String.valueOf(z2), "encodedImageSize", String.valueOf(i));
        }
        return b.f.d.d.f.of("cached_value_found", String.valueOf(z2));
    }

    @Override // b.f.j.p.w0
    public void b(l<e> lVar, x0 x0Var) {
        ImageRequest e = x0Var.e();
        boolean b2 = x0Var.e().b(16);
        z0 o = x0Var.o();
        o.e(x0Var, "PartialDiskCacheProducer");
        Uri build = e.c.buildUpon().appendQueryParameter("fresco_partial", "true").build();
        i iVar = this.f631b;
        x0Var.b();
        Objects.requireNonNull((n) iVar);
        b.f.b.a.f fVar = new b.f.b.a.f(build.toString());
        if (!b2) {
            o.j(x0Var, "PartialDiskCacheProducer", c(o, x0Var, false, 0));
            d(lVar, x0Var, fVar, null);
            return;
        }
        AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        this.a.e(fVar, atomicBoolean).b(new p0(this, x0Var.o(), x0Var, lVar, fVar));
        x0Var.f(new q0(this, atomicBoolean));
    }

    public final void d(l<e> lVar, x0 x0Var, CacheKey cacheKey, e eVar) {
        this.e.b(new a(lVar, this.a, cacheKey, this.c, this.d, eVar, x0Var.e().b(32), null), x0Var);
    }
}
