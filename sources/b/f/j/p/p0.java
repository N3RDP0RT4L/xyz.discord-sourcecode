package b.f.j.p;

import b.c.a.a0.d;
import b.f.j.d.a;
import b.f.j.j.e;
import com.facebook.cache.common.CacheKey;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import java.util.concurrent.CancellationException;
import z.c;
import z.g;
/* compiled from: PartialDiskCacheProducer.java */
/* loaded from: classes2.dex */
public class p0 implements c<e, Void> {
    public final /* synthetic */ z0 a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ x0 f629b;
    public final /* synthetic */ l c;
    public final /* synthetic */ CacheKey d;
    public final /* synthetic */ r0 e;

    public p0(r0 r0Var, z0 z0Var, x0 x0Var, l lVar, CacheKey cacheKey) {
        this.e = r0Var;
        this.a = z0Var;
        this.f629b = x0Var;
        this.c = lVar;
        this.d = cacheKey;
    }

    @Override // z.c
    public Void a(g<e> gVar) throws Exception {
        boolean z2;
        e eVar;
        synchronized (gVar.g) {
            z2 = gVar.i;
        }
        boolean z3 = false;
        if (z2 || (gVar.e() && (gVar.d() instanceof CancellationException))) {
            this.a.d(this.f629b, "PartialDiskCacheProducer", null);
            this.c.d();
        } else if (gVar.e()) {
            this.a.k(this.f629b, "PartialDiskCacheProducer", gVar.d(), null);
            this.e.d(this.c, this.f629b, this.d, null);
        } else {
            synchronized (gVar.g) {
                eVar = gVar.j;
            }
            e eVar2 = eVar;
            if (eVar2 != null) {
                z0 z0Var = this.a;
                x0 x0Var = this.f629b;
                z0Var.j(x0Var, "PartialDiskCacheProducer", r0.c(z0Var, x0Var, true, eVar2.n()));
                int n = eVar2.n() - 1;
                d.i(Boolean.valueOf(n > 0));
                eVar2.f586s = new a(0, n);
                int n2 = eVar2.n();
                ImageRequest e = this.f629b.e();
                a aVar = e.l;
                if (aVar != null && aVar.a >= 0 && n >= aVar.f559b) {
                    this.f629b.i("disk", "partial");
                    this.a.c(this.f629b, "PartialDiskCacheProducer", true);
                    this.c.b(eVar2, 9);
                } else {
                    this.c.b(eVar2, 8);
                    ImageRequestBuilder b2 = ImageRequestBuilder.b(e.c);
                    b2.f = e.i;
                    b2.o = e.l;
                    b2.g = e.f2874b;
                    b2.i = e.g;
                    b2.j = e.h;
                    b2.f2876b = e.n;
                    b2.c = e.o;
                    b2.l = e.f2875s;
                    b2.h = e.f;
                    b2.k = e.m;
                    b2.d = e.j;
                    b2.n = e.t;
                    b2.e = e.k;
                    b2.m = e.r;
                    b2.p = e.u;
                    int i = n2 - 1;
                    if (i >= 0) {
                        z3 = true;
                    }
                    d.i(Boolean.valueOf(z3));
                    b2.o = new a(i, Integer.MAX_VALUE);
                    this.e.d(this.c, new d1(b2.a(), this.f629b), this.d, eVar2);
                }
            } else {
                z0 z0Var2 = this.a;
                x0 x0Var2 = this.f629b;
                z0Var2.j(x0Var2, "PartialDiskCacheProducer", r0.c(z0Var2, x0Var2, false, 0));
                this.e.d(this.c, this.f629b, this.d, eVar2);
            }
        }
        return null;
    }
}
