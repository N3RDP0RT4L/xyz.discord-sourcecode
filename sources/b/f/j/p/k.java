package b.f.j.p;

import b.c.a.a0.d;
import b.f.j.j.e;
import com.facebook.imagepipeline.request.ImageRequest;
/* compiled from: BranchOnSeparateImagesProducer.java */
/* loaded from: classes2.dex */
public class k implements w0<e> {
    public final w0<e> a;

    /* renamed from: b  reason: collision with root package name */
    public final w0<e> f616b;

    /* compiled from: BranchOnSeparateImagesProducer.java */
    /* loaded from: classes2.dex */
    public class b extends p<e, e> {
        public x0 c;

        public b(l lVar, x0 x0Var, a aVar) {
            super(lVar);
            this.c = x0Var;
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // b.f.j.p.p, b.f.j.p.b
        public void h(Throwable th) {
            k.this.f616b.b(this.f628b, this.c);
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // b.f.j.p.b
        public void i(Object obj, int i) {
            e eVar = (e) obj;
            ImageRequest e = this.c.e();
            boolean e2 = b.f.j.p.b.e(i);
            boolean T0 = d.T0(eVar, e.j);
            if (eVar != null && (T0 || e.g)) {
                if (!e2 || !T0) {
                    this.f628b.b(eVar, i & (-2));
                } else {
                    this.f628b.b(eVar, i);
                }
            }
            if (e2 && !T0 && !e.h) {
                if (eVar != null) {
                    eVar.close();
                }
                k.this.f616b.b(this.f628b, this.c);
            }
        }
    }

    public k(w0<e> w0Var, w0<e> w0Var2) {
        this.a = w0Var;
        this.f616b = w0Var2;
    }

    @Override // b.f.j.p.w0
    public void b(l<e> lVar, x0 x0Var) {
        this.a.b(new b(lVar, x0Var, null), x0Var);
    }
}
