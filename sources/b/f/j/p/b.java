package b.f.j.p;

import android.util.Log;
import b.d.b.a.a;
import java.io.PrintWriter;
import java.io.StringWriter;
/* compiled from: BaseConsumer.java */
/* loaded from: classes2.dex */
public abstract class b<T> implements l<T> {
    public boolean a = false;

    public static boolean e(int i) {
        return (i & 1) == 1;
    }

    public static boolean f(int i) {
        return !e(i);
    }

    public static boolean l(int i, int i2) {
        return (i & i2) != 0;
    }

    public static boolean m(int i, int i2) {
        return (i & i2) == i2;
    }

    @Override // b.f.j.p.l
    public synchronized void a(float f) {
        if (!this.a) {
            try {
                j(f);
            } catch (Exception e) {
                k(e);
            }
        }
    }

    @Override // b.f.j.p.l
    public synchronized void b(T t, int i) {
        if (!this.a) {
            this.a = e(i);
            try {
                i(t, i);
            } catch (Exception e) {
                k(e);
            }
        }
    }

    @Override // b.f.j.p.l
    public synchronized void c(Throwable th) {
        if (!this.a) {
            this.a = true;
            try {
                h(th);
            } catch (Exception e) {
                k(e);
            }
        }
    }

    @Override // b.f.j.p.l
    public synchronized void d() {
        if (!this.a) {
            this.a = true;
            try {
                g();
            } catch (Exception e) {
                k(e);
            }
        }
    }

    public abstract void g();

    public abstract void h(Throwable th);

    public abstract void i(T t, int i);

    public abstract void j(float f);

    public void k(Exception exc) {
        String w = a.w("unknown", ":", getClass().getSimpleName());
        StringBuilder sb = new StringBuilder();
        sb.append("unhandled exception");
        sb.append('\n');
        StringWriter stringWriter = new StringWriter();
        exc.printStackTrace(new PrintWriter(stringWriter));
        sb.append(stringWriter.toString());
        Log.println(6, w, sb.toString());
    }
}
