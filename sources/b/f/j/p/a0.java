package b.f.j.p;

import b.f.d.k.b;
import b.f.j.j.e;
import com.facebook.common.time.RealtimeSinceBootClock;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
/* compiled from: HttpUrlConnectionNetworkFetcher.java */
/* loaded from: classes2.dex */
public class a0 extends c<a> {
    public int a;

    /* renamed from: b  reason: collision with root package name */
    public final ExecutorService f600b = Executors.newFixedThreadPool(3);
    public final b c;

    /* compiled from: HttpUrlConnectionNetworkFetcher.java */
    /* loaded from: classes2.dex */
    public static class a extends x {
        public long d;
        public long e;
        public long f;

        public a(l<e> lVar, x0 x0Var) {
            super(lVar, x0Var);
        }
    }

    public a0(int i) {
        RealtimeSinceBootClock realtimeSinceBootClock = RealtimeSinceBootClock.get();
        this.c = realtimeSinceBootClock;
        this.a = i;
    }

    /* JADX WARN: Removed duplicated region for block: B:23:0x0040  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x009b  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.net.HttpURLConnection a(android.net.Uri r8, int r9) throws java.io.IOException {
        /*
            r7 = this;
            android.net.Uri r0 = b.f.d.l.b.a
            r0 = 0
            if (r8 != 0) goto L7
            r1 = r0
            goto L10
        L7:
            java.net.URL r1 = new java.net.URL     // Catch: java.net.MalformedURLException -> Lb8
            java.lang.String r2 = r8.toString()     // Catch: java.net.MalformedURLException -> Lb8
            r1.<init>(r2)     // Catch: java.net.MalformedURLException -> Lb8
        L10:
            java.net.URLConnection r1 = r1.openConnection()
            java.net.HttpURLConnection r1 = (java.net.HttpURLConnection) r1
            int r2 = r7.a
            r1.setConnectTimeout(r2)
            int r2 = r1.getResponseCode()
            r3 = 200(0xc8, float:2.8E-43)
            r4 = 1
            r5 = 0
            if (r2 < r3) goto L2b
            r3 = 300(0x12c, float:4.2E-43)
            if (r2 >= r3) goto L2b
            r3 = 1
            goto L2c
        L2b:
            r3 = 0
        L2c:
            if (r3 == 0) goto L2f
            return r1
        L2f:
            r3 = 307(0x133, float:4.3E-43)
            if (r2 == r3) goto L3c
            r3 = 308(0x134, float:4.32E-43)
            if (r2 == r3) goto L3c
            switch(r2) {
                case 300: goto L3c;
                case 301: goto L3c;
                case 302: goto L3c;
                case 303: goto L3c;
                default: goto L3a;
            }
        L3a:
            r3 = 0
            goto L3d
        L3c:
            r3 = 1
        L3d:
            r6 = 2
            if (r3 == 0) goto L9b
            java.lang.String r3 = "Location"
            java.lang.String r3 = r1.getHeaderField(r3)
            r1.disconnect()
            if (r3 != 0) goto L4c
            goto L50
        L4c:
            android.net.Uri r0 = android.net.Uri.parse(r3)
        L50:
            java.lang.String r1 = r8.getScheme()
            if (r9 <= 0) goto L68
            if (r0 == 0) goto L68
            java.lang.String r3 = r0.getScheme()
            boolean r1 = b.c.a.a0.d.g0(r3, r1)
            if (r1 != 0) goto L68
            int r9 = r9 - r4
            java.net.HttpURLConnection r8 = r7.a(r0, r9)
            return r8
        L68:
            if (r9 != 0) goto L7d
            java.lang.Object[] r9 = new java.lang.Object[r4]
            java.lang.String r8 = r8.toString()
            r9[r5] = r8
            java.util.Locale r8 = java.util.Locale.getDefault()
            java.lang.String r0 = "URL %s follows too many redirects"
            java.lang.String r8 = java.lang.String.format(r8, r0, r9)
            goto L95
        L7d:
            java.lang.Object[] r9 = new java.lang.Object[r6]
            java.lang.String r8 = r8.toString()
            r9[r5] = r8
            java.lang.Integer r8 = java.lang.Integer.valueOf(r2)
            r9[r4] = r8
            java.util.Locale r8 = java.util.Locale.getDefault()
            java.lang.String r0 = "URL %s returned %d without a valid redirect"
            java.lang.String r8 = java.lang.String.format(r8, r0, r9)
        L95:
            java.io.IOException r9 = new java.io.IOException
            r9.<init>(r8)
            throw r9
        L9b:
            r1.disconnect()
            java.io.IOException r9 = new java.io.IOException
            java.lang.Object[] r0 = new java.lang.Object[r6]
            java.lang.String r8 = r8.toString()
            r0[r5] = r8
            java.lang.Integer r8 = java.lang.Integer.valueOf(r2)
            r0[r4] = r8
            java.lang.String r8 = "Image URL %s returned HTTP code %d"
            java.lang.String r8 = java.lang.String.format(r8, r0)
            r9.<init>(r8)
            throw r9
        Lb8:
            r8 = move-exception
            java.lang.RuntimeException r9 = new java.lang.RuntimeException
            r9.<init>(r8)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: b.f.j.p.a0.a(android.net.Uri, int):java.net.HttpURLConnection");
    }
}
