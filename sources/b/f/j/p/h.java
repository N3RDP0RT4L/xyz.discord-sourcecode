package b.f.j.p;

import b.f.d.d.f;
import b.f.j.c.i;
import b.f.j.c.n;
import b.f.j.c.w;
import b.f.j.j.c;
import b.f.j.r.b;
import com.facebook.cache.common.CacheKey;
import com.facebook.common.references.CloseableReference;
import com.facebook.imagepipeline.request.ImageRequest;
import java.util.Map;
/* compiled from: BitmapMemoryCacheProducer.java */
/* loaded from: classes2.dex */
public class h implements w0<CloseableReference<c>> {
    public final w<CacheKey, c> a;

    /* renamed from: b  reason: collision with root package name */
    public final i f609b;
    public final w0<CloseableReference<c>> c;

    /* compiled from: BitmapMemoryCacheProducer.java */
    /* loaded from: classes2.dex */
    public class a extends p<CloseableReference<c>, CloseableReference<c>> {
        public final /* synthetic */ CacheKey c;
        public final /* synthetic */ boolean d;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(l lVar, CacheKey cacheKey, boolean z2) {
            super(lVar);
            this.c = cacheKey;
            this.d = z2;
        }

        @Override // b.f.j.p.b
        public void i(Object obj, int i) {
            CloseableReference<c> closeableReference;
            CloseableReference<c> closeableReference2 = (CloseableReference) obj;
            try {
                b.b();
                boolean e = b.e(i);
                CloseableReference<c> closeableReference3 = null;
                if (closeableReference2 != null) {
                    if (!closeableReference2.u().d() && !b.m(i, 8)) {
                        if (!e && (closeableReference = h.this.a.get(this.c)) != null) {
                            b.f.j.j.i b2 = closeableReference2.u().b();
                            b.f.j.j.i b3 = closeableReference.u().b();
                            if (((b.f.j.j.h) b3).d || ((b.f.j.j.h) b3).f587b >= ((b.f.j.j.h) b2).f587b) {
                                this.f628b.b(closeableReference, i);
                                closeableReference.close();
                            } else {
                                closeableReference.close();
                            }
                        }
                        if (this.d) {
                            closeableReference3 = h.this.a.a(this.c, closeableReference2);
                        }
                        if (e) {
                            this.f628b.a(1.0f);
                        }
                        l<O> lVar = this.f628b;
                        if (closeableReference3 != null) {
                            closeableReference2 = closeableReference3;
                        }
                        lVar.b(closeableReference2, i);
                        if (closeableReference3 != null) {
                            closeableReference3.close();
                        }
                    }
                    this.f628b.b(closeableReference2, i);
                } else if (e) {
                    this.f628b.b(null, i);
                }
            } finally {
                b.b();
            }
        }
    }

    public h(w<CacheKey, c> wVar, i iVar, w0<CloseableReference<c>> w0Var) {
        this.a = wVar;
        this.f609b = iVar;
        this.c = w0Var;
    }

    @Override // b.f.j.p.w0
    public void b(l<CloseableReference<c>> lVar, x0 x0Var) {
        try {
            b.b();
            z0 o = x0Var.o();
            o.e(x0Var, d());
            CacheKey a2 = ((n) this.f609b).a(x0Var.e(), x0Var.b());
            Map<String, String> map = null;
            CloseableReference<c> closeableReference = x0Var.e().b(1) ? this.a.get(a2) : null;
            if (closeableReference != null) {
                x0Var.j(closeableReference.u().a());
                boolean z2 = ((b.f.j.j.h) closeableReference.u().b()).d;
                if (z2) {
                    o.j(x0Var, d(), o.g(x0Var, d()) ? f.of("cached_value_found", "true") : null);
                    o.c(x0Var, d(), true);
                    x0Var.i("memory_bitmap", c());
                    lVar.a(1.0f);
                }
                lVar.b(closeableReference, z2 ? 1 : 0);
                closeableReference.close();
                if (z2) {
                    return;
                }
            }
            if (x0Var.q().g() >= ImageRequest.c.BITMAP_MEMORY_CACHE.g()) {
                o.j(x0Var, d(), o.g(x0Var, d()) ? f.of("cached_value_found", "false") : null);
                o.c(x0Var, d(), false);
                x0Var.i("memory_bitmap", c());
                lVar.b(null, 1);
                return;
            }
            l<CloseableReference<c>> e = e(lVar, a2, x0Var.e().b(2));
            String d = d();
            if (o.g(x0Var, d())) {
                map = f.of("cached_value_found", "false");
            }
            o.j(x0Var, d, map);
            b.b();
            this.c.b(e, x0Var);
            b.b();
        } finally {
            b.b();
        }
    }

    public String c() {
        return "pipe_bg";
    }

    public String d() {
        return "BitmapMemoryCacheProducer";
    }

    public l<CloseableReference<c>> e(l<CloseableReference<c>> lVar, CacheKey cacheKey, boolean z2) {
        return new a(lVar, cacheKey, z2);
    }
}
