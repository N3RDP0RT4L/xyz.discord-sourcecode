package b.f.j.p;

import b.f.j.p.a0;
import b.f.j.p.o0;
/* compiled from: HttpUrlConnectionNetworkFetcher.java */
/* loaded from: classes2.dex */
public class y implements Runnable {
    public final /* synthetic */ a0.a j;
    public final /* synthetic */ o0.a k;
    public final /* synthetic */ a0 l;

    public y(a0 a0Var, a0.a aVar, o0.a aVar2) {
        this.l = a0Var;
        this.j = aVar;
        this.k = aVar2;
    }

    /* JADX WARN: Removed duplicated region for block: B:28:0x0059  */
    /* JADX WARN: Removed duplicated region for block: B:32:0x0052 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    @Override // java.lang.Runnable
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void run() {
        /*
            r7 = this;
            b.f.j.p.a0 r0 = r7.l
            b.f.j.p.a0$a r1 = r7.j
            b.f.j.p.o0$a r2 = r7.k
            java.util.Objects.requireNonNull(r0)
            r3 = 0
            b.f.j.p.x0 r4 = r1.f638b     // Catch: java.lang.Throwable -> L38 java.io.IOException -> L3b
            com.facebook.imagepipeline.request.ImageRequest r4 = r4.e()     // Catch: java.lang.Throwable -> L38 java.io.IOException -> L3b
            android.net.Uri r4 = r4.c     // Catch: java.lang.Throwable -> L38 java.io.IOException -> L3b
            r5 = 5
            java.net.HttpURLConnection r4 = r0.a(r4, r5)     // Catch: java.lang.Throwable -> L38 java.io.IOException -> L3b
            b.f.d.k.b r0 = r0.c     // Catch: java.io.IOException -> L36 java.lang.Throwable -> L4f
            long r5 = r0.now()     // Catch: java.io.IOException -> L36 java.lang.Throwable -> L4f
            r1.e = r5     // Catch: java.io.IOException -> L36 java.lang.Throwable -> L4f
            if (r4 == 0) goto L2c
            java.io.InputStream r3 = r4.getInputStream()     // Catch: java.io.IOException -> L36 java.lang.Throwable -> L4f
            r0 = -1
            r1 = r2
            b.f.j.p.n0$a r1 = (b.f.j.p.n0.a) r1     // Catch: java.io.IOException -> L36 java.lang.Throwable -> L4f
            r1.b(r3, r0)     // Catch: java.io.IOException -> L36 java.lang.Throwable -> L4f
        L2c:
            if (r3 == 0) goto L33
            r3.close()     // Catch: java.io.IOException -> L32
            goto L33
        L32:
        L33:
            if (r4 == 0) goto L4e
            goto L4b
        L36:
            r0 = move-exception
            goto L3d
        L38:
            r0 = move-exception
            r4 = r3
            goto L50
        L3b:
            r0 = move-exception
            r4 = r3
        L3d:
            b.f.j.p.n0$a r2 = (b.f.j.p.n0.a) r2     // Catch: java.lang.Throwable -> L4f
            r2.a(r0)     // Catch: java.lang.Throwable -> L4f
            if (r3 == 0) goto L49
            r3.close()     // Catch: java.io.IOException -> L48
            goto L49
        L48:
        L49:
            if (r4 == 0) goto L4e
        L4b:
            r4.disconnect()
        L4e:
            return
        L4f:
            r0 = move-exception
        L50:
            if (r3 == 0) goto L57
            r3.close()     // Catch: java.io.IOException -> L56
            goto L57
        L56:
        L57:
            if (r4 == 0) goto L5c
            r4.disconnect()
        L5c:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.f.j.p.y.run():void");
    }
}
