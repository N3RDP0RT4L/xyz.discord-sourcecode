package b.f.j.p;

import b.f.d.g.g;
import b.f.j.j.e;
import com.facebook.imagepipeline.request.ImageRequest;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.Executor;
/* compiled from: LocalFileFetchProducer.java */
/* loaded from: classes2.dex */
public class h0 extends g0 {
    public h0(Executor executor, g gVar) {
        super(executor, gVar);
    }

    @Override // b.f.j.p.g0
    public e d(ImageRequest imageRequest) throws IOException {
        return c(new FileInputStream(imageRequest.a().toString()), (int) imageRequest.a().length());
    }

    @Override // b.f.j.p.g0
    public String e() {
        return "LocalFileFetchProducer";
    }
}
