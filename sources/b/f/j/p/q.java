package b.f.j.p;

import b.f.j.j.e;
import java.util.concurrent.CancellationException;
import z.c;
import z.g;
/* compiled from: DiskCacheReadProducer.java */
/* loaded from: classes2.dex */
public class q implements c<e, Void> {
    public final /* synthetic */ z0 a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ x0 f630b;
    public final /* synthetic */ l c;
    public final /* synthetic */ s d;

    public q(s sVar, z0 z0Var, x0 x0Var, l lVar) {
        this.d = sVar;
        this.a = z0Var;
        this.f630b = x0Var;
        this.c = lVar;
    }

    @Override // z.c
    public Void a(g<e> gVar) throws Exception {
        boolean z2;
        e eVar;
        synchronized (gVar.g) {
            z2 = gVar.i;
        }
        if (z2 || (gVar.e() && (gVar.d() instanceof CancellationException))) {
            this.a.d(this.f630b, "DiskCacheProducer", null);
            this.c.d();
        } else if (gVar.e()) {
            this.a.k(this.f630b, "DiskCacheProducer", gVar.d(), null);
            this.d.d.b(this.c, this.f630b);
        } else {
            synchronized (gVar.g) {
                eVar = gVar.j;
            }
            e eVar2 = eVar;
            if (eVar2 != null) {
                z0 z0Var = this.a;
                x0 x0Var = this.f630b;
                z0Var.j(x0Var, "DiskCacheProducer", s.c(z0Var, x0Var, true, eVar2.n()));
                this.a.c(this.f630b, "DiskCacheProducer", true);
                this.f630b.n("disk");
                this.c.a(1.0f);
                this.c.b(eVar2, 1);
                eVar2.close();
            } else {
                z0 z0Var2 = this.a;
                x0 x0Var2 = this.f630b;
                z0Var2.j(x0Var2, "DiskCacheProducer", s.c(z0Var2, x0Var2, false, 0));
                this.d.d.b(this.c, this.f630b);
            }
        }
        return null;
    }
}
