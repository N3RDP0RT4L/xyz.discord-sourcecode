package b.f.j.p;

import android.content.res.AssetManager;
import b.f.d.g.g;
import java.util.concurrent.Executor;
/* compiled from: LocalAssetFetchProducer.java */
/* loaded from: classes2.dex */
public class d0 extends g0 {
    public final AssetManager c;

    public d0(Executor executor, g gVar, AssetManager assetManager) {
        super(executor, gVar);
        this.c = assetManager;
    }

    /* JADX WARN: Code restructure failed: missing block: B:10:0x0031, code lost:
        if (r1 == null) goto L12;
     */
    @Override // b.f.j.p.g0
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.f.j.j.e d(com.facebook.imagepipeline.request.ImageRequest r5) throws java.io.IOException {
        /*
            r4 = this;
            android.content.res.AssetManager r0 = r4.c
            android.net.Uri r1 = r5.c
            java.lang.String r1 = r1.getPath()
            r2 = 1
            java.lang.String r1 = r1.substring(r2)
            r3 = 2
            java.io.InputStream r0 = r0.open(r1, r3)
            r1 = 0
            android.content.res.AssetManager r3 = r4.c     // Catch: java.lang.Throwable -> L29 java.io.IOException -> L30
            android.net.Uri r5 = r5.c     // Catch: java.lang.Throwable -> L29 java.io.IOException -> L30
            java.lang.String r5 = r5.getPath()     // Catch: java.lang.Throwable -> L29 java.io.IOException -> L30
            java.lang.String r5 = r5.substring(r2)     // Catch: java.lang.Throwable -> L29 java.io.IOException -> L30
            android.content.res.AssetFileDescriptor r1 = r3.openFd(r5)     // Catch: java.lang.Throwable -> L29 java.io.IOException -> L30
            long r2 = r1.getLength()     // Catch: java.lang.Throwable -> L29 java.io.IOException -> L30
            int r5 = (int) r2
            goto L33
        L29:
            r5 = move-exception
            if (r1 == 0) goto L2f
            r1.close()     // Catch: java.io.IOException -> L2f
        L2f:
            throw r5
        L30:
            r5 = -1
            if (r1 == 0) goto L36
        L33:
            r1.close()     // Catch: java.io.IOException -> L36
        L36:
            b.f.j.j.e r5 = r4.c(r0, r5)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: b.f.j.p.d0.d(com.facebook.imagepipeline.request.ImageRequest):b.f.j.j.e");
    }

    @Override // b.f.j.p.g0
    public String e() {
        return "LocalAssetFetchProducer";
    }
}
