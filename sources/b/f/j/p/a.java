package b.f.j.p;

import b.f.j.j.e;
/* compiled from: AddImageTransformMetaDataProducer.java */
/* loaded from: classes2.dex */
public class a implements w0<e> {
    public final w0<e> a;

    /* compiled from: AddImageTransformMetaDataProducer.java */
    /* loaded from: classes2.dex */
    public static class b extends p<e, e> {
        public b(l lVar, C0072a aVar) {
            super(lVar);
        }

        @Override // b.f.j.p.b
        public void i(Object obj, int i) {
            e eVar = (e) obj;
            if (eVar == null) {
                this.f628b.b(null, i);
                return;
            }
            if (!e.s(eVar)) {
                eVar.q();
            }
            this.f628b.b(eVar, i);
        }
    }

    public a(w0<e> w0Var) {
        this.a = w0Var;
    }

    @Override // b.f.j.p.w0
    public void b(l<e> lVar, x0 x0Var) {
        this.a.b(new b(lVar, null), x0Var);
    }
}
