package b.f.j.p;

import android.graphics.Bitmap;
import b.f.d.d.f;
import b.f.j.h.d;
import b.f.j.j.e;
import b.f.j.j.h;
import b.f.j.j.i;
import b.f.j.p.c0;
import com.facebook.common.internal.Supplier;
import com.facebook.common.references.CloseableReference;
import com.facebook.common.util.ExceptionWithNoStacktrace;
import java.io.Closeable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executor;
/* compiled from: DecodeProducer.java */
/* loaded from: classes2.dex */
public class n implements w0<CloseableReference<b.f.j.j.c>> {
    public final b.f.d.g.a a;

    /* renamed from: b  reason: collision with root package name */
    public final Executor f622b;
    public final b.f.j.h.b c;
    public final d d;
    public final w0<e> e;
    public final boolean f;
    public final boolean g;
    public final boolean h;
    public final int i;
    public final b.f.j.e.a j;
    public final Runnable k = null;
    public final Supplier<Boolean> l;

    /* compiled from: DecodeProducer.java */
    /* loaded from: classes2.dex */
    public class a extends c {
        public a(n nVar, l<CloseableReference<b.f.j.j.c>> lVar, x0 x0Var, boolean z2, int i) {
            super(lVar, x0Var, z2, i);
        }

        @Override // b.f.j.p.n.c
        public int o(e eVar) {
            return eVar.n();
        }

        @Override // b.f.j.p.n.c
        public i p() {
            return new h(0, false, false);
        }

        @Override // b.f.j.p.n.c
        public synchronized boolean w(e eVar, int i) {
            if (b.f.j.p.b.f(i)) {
                return false;
            }
            return this.g.f(eVar, i);
        }
    }

    /* compiled from: DecodeProducer.java */
    /* loaded from: classes2.dex */
    public class b extends c {
        public final b.f.j.h.e i;
        public final d j;
        public int k = 0;

        public b(n nVar, l<CloseableReference<b.f.j.j.c>> lVar, x0 x0Var, b.f.j.h.e eVar, d dVar, boolean z2, int i) {
            super(lVar, x0Var, z2, i);
            this.i = eVar;
            Objects.requireNonNull(dVar);
            this.j = dVar;
        }

        @Override // b.f.j.p.n.c
        public int o(e eVar) {
            return this.i.f;
        }

        @Override // b.f.j.p.n.c
        public i p() {
            return this.j.a(this.i.e);
        }

        @Override // b.f.j.p.n.c
        public synchronized boolean w(e eVar, int i) {
            boolean f = this.g.f(eVar, i);
            if ((b.f.j.p.b.f(i) || b.f.j.p.b.m(i, 8)) && !b.f.j.p.b.m(i, 4) && e.u(eVar)) {
                eVar.x();
                if (eVar.l == b.f.i.b.a) {
                    if (!this.i.b(eVar)) {
                        return false;
                    }
                    int i2 = this.i.e;
                    int i3 = this.k;
                    if (i2 <= i3) {
                        return false;
                    }
                    if (i2 < this.j.b(i3) && !this.i.g) {
                        return false;
                    }
                    this.k = i2;
                }
            }
            return f;
        }
    }

    /* compiled from: DecodeProducer.java */
    /* loaded from: classes2.dex */
    public abstract class c extends p<e, CloseableReference<b.f.j.j.c>> {
        public final x0 c;
        public final z0 d;
        public final b.f.j.d.b e;
        public boolean f = false;
        public final c0 g;

        /* compiled from: DecodeProducer.java */
        /* loaded from: classes2.dex */
        public class a implements c0.c {
            public final /* synthetic */ x0 a;

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ int f623b;

            public a(n nVar, x0 x0Var, int i) {
                this.a = x0Var;
                this.f623b = i;
            }

            /* JADX WARN: Can't wrap try/catch for region: R(19:86|38|ea|43|(14:47|(12:52|54|81|55|84|56|57|(1:59)|60|61|71|88)|53|54|81|55|84|56|57|(0)|60|61|71|88)|48|(12:52|54|81|55|84|56|57|(0)|60|61|71|88)|53|54|81|55|84|56|57|(0)|60|61|71|88) */
            /* JADX WARN: Code restructure failed: missing block: B:62:0x0146, code lost:
                r0 = e;
             */
            /* JADX WARN: Code restructure failed: missing block: B:63:0x0148, code lost:
                r0 = e;
             */
            /* JADX WARN: Code restructure failed: missing block: B:68:0x017c, code lost:
                r1 = null;
             */
            /* JADX WARN: Code restructure failed: missing block: B:70:0x0180, code lost:
                r3.d.k(r3.c, "DecodeProducer", r0, r3.n(r1, r6, r15, r9, r10, r11, r12, r13));
                r3.u(true);
                r3.f628b.c(r0);
             */
            /* JADX WARN: Removed duplicated region for block: B:59:0x012d  */
            @Override // b.f.j.p.c0.c
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public void a(b.f.j.j.e r20, int r21) {
                /*
                    Method dump skipped, instructions count: 423
                    To view this dump add '--comments-level debug' option
                */
                throw new UnsupportedOperationException("Method not decompiled: b.f.j.p.n.c.a.a(b.f.j.j.e, int):void");
            }
        }

        /* compiled from: DecodeProducer.java */
        /* loaded from: classes2.dex */
        public class b extends e {
            public final /* synthetic */ boolean a;

            public b(n nVar, boolean z2) {
                this.a = z2;
            }

            @Override // b.f.j.p.y0
            public void a() {
                if (this.a) {
                    c cVar = c.this;
                    cVar.u(true);
                    cVar.f628b.d();
                }
            }

            @Override // b.f.j.p.e, b.f.j.p.y0
            public void b() {
                if (c.this.c.p()) {
                    c.this.g.d();
                }
            }
        }

        public c(l<CloseableReference<b.f.j.j.c>> lVar, x0 x0Var, boolean z2, int i) {
            super(lVar);
            this.c = x0Var;
            this.d = x0Var.o();
            b.f.j.d.b bVar = x0Var.e().i;
            this.e = bVar;
            this.g = new c0(n.this.f622b, new a(n.this, x0Var, i), bVar.f560b);
            x0Var.f(new b(n.this, z2));
        }

        @Override // b.f.j.p.p, b.f.j.p.b
        public void g() {
            q();
        }

        @Override // b.f.j.p.p, b.f.j.p.b
        public void h(Throwable th) {
            r(th);
        }

        @Override // b.f.j.p.b
        public void i(Object obj, int i) {
            e eVar = (e) obj;
            try {
                b.f.j.r.b.b();
                boolean e = b.f.j.p.b.e(i);
                if (e) {
                    if (eVar == null) {
                        ExceptionWithNoStacktrace exceptionWithNoStacktrace = new ExceptionWithNoStacktrace("Encoded image is null.");
                        u(true);
                        this.f628b.c(exceptionWithNoStacktrace);
                    } else if (!eVar.t()) {
                        ExceptionWithNoStacktrace exceptionWithNoStacktrace2 = new ExceptionWithNoStacktrace("Encoded image is not valid.");
                        u(true);
                        this.f628b.c(exceptionWithNoStacktrace2);
                    }
                }
                if (w(eVar, i)) {
                    boolean m = b.f.j.p.b.m(i, 4);
                    if (e || m || this.c.p()) {
                        this.g.d();
                    }
                }
            } finally {
                b.f.j.r.b.b();
            }
        }

        @Override // b.f.j.p.p, b.f.j.p.b
        public void j(float f) {
            this.f628b.a(f * 0.99f);
        }

        public final Map<String, String> n(b.f.j.j.c cVar, long j, i iVar, boolean z2, String str, String str2, String str3, String str4) {
            Bitmap bitmap;
            if (!this.d.g(this.c, "DecodeProducer")) {
                return null;
            }
            String valueOf = String.valueOf(j);
            String valueOf2 = String.valueOf(((h) iVar).c);
            String valueOf3 = String.valueOf(z2);
            if (cVar instanceof b.f.j.j.d) {
                Objects.requireNonNull(((b.f.j.j.d) cVar).m);
                HashMap hashMap = new HashMap(8);
                hashMap.put("bitmapSize", bitmap.getWidth() + "x" + bitmap.getHeight());
                hashMap.put("queueTime", valueOf);
                hashMap.put("hasGoodQuality", valueOf2);
                hashMap.put("isFinal", valueOf3);
                hashMap.put("encodedImageSize", str2);
                hashMap.put("imageFormat", str);
                hashMap.put("requestedImageSize", str3);
                hashMap.put("sampleSize", str4);
                hashMap.put("byteCount", bitmap.getByteCount() + "");
                return new f(hashMap);
            }
            HashMap hashMap2 = new HashMap(7);
            hashMap2.put("queueTime", valueOf);
            hashMap2.put("hasGoodQuality", valueOf2);
            hashMap2.put("isFinal", valueOf3);
            hashMap2.put("encodedImageSize", str2);
            hashMap2.put("imageFormat", str);
            hashMap2.put("requestedImageSize", str3);
            hashMap2.put("sampleSize", str4);
            return new f(hashMap2);
        }

        public abstract int o(e eVar);

        public abstract i p();

        public final void q() {
            u(true);
            this.f628b.d();
        }

        public final void r(Throwable th) {
            u(true);
            this.f628b.c(th);
        }

        public final void s(b.f.j.j.c cVar, int i) {
            CloseableReference.c cVar2 = n.this.j.a;
            Class<CloseableReference> cls = CloseableReference.j;
            CloseableReference closeableReference = null;
            Throwable th = null;
            if (cVar != null) {
                b.f.d.h.f<Closeable> fVar = CloseableReference.l;
                if (cVar2.b()) {
                    th = new Throwable();
                }
                closeableReference = CloseableReference.I(cVar, fVar, cVar2, th);
            }
            try {
                u(b.f.j.p.b.e(i));
                this.f628b.b(closeableReference, i);
            } finally {
                if (closeableReference != null) {
                    closeableReference.close();
                }
            }
        }

        public final b.f.j.j.c t(e eVar, int i, i iVar) {
            n nVar = n.this;
            boolean z2 = nVar.k != null && nVar.l.get().booleanValue();
            try {
                return n.this.c.a(eVar, i, iVar, this.e);
            } catch (OutOfMemoryError e) {
                if (z2) {
                    n.this.k.run();
                    System.gc();
                    return n.this.c.a(eVar, i, iVar, this.e);
                }
                throw e;
            }
        }

        public final void u(boolean z2) {
            synchronized (this) {
                if (z2) {
                    if (!this.f) {
                        this.f628b.a(1.0f);
                        this.f = true;
                        this.g.a();
                    }
                }
            }
        }

        public final void v(e eVar, b.f.j.j.c cVar) {
            x0 x0Var = this.c;
            eVar.x();
            x0Var.d("encoded_width", Integer.valueOf(eVar.o));
            x0 x0Var2 = this.c;
            eVar.x();
            x0Var2.d("encoded_height", Integer.valueOf(eVar.p));
            this.c.d("encoded_size", Integer.valueOf(eVar.n()));
            if (cVar instanceof b.f.j.j.b) {
                Bitmap f = ((b.f.j.j.b) cVar).f();
                this.c.d("bitmap_config", String.valueOf(f == null ? null : f.getConfig()));
            }
            if (cVar != null) {
                cVar.e(this.c.a());
            }
        }

        public abstract boolean w(e eVar, int i);
    }

    public n(b.f.d.g.a aVar, Executor executor, b.f.j.h.b bVar, d dVar, boolean z2, boolean z3, boolean z4, w0<e> w0Var, int i, b.f.j.e.a aVar2, Runnable runnable, Supplier<Boolean> supplier) {
        Objects.requireNonNull(aVar);
        this.a = aVar;
        Objects.requireNonNull(executor);
        this.f622b = executor;
        Objects.requireNonNull(bVar);
        this.c = bVar;
        Objects.requireNonNull(dVar);
        this.d = dVar;
        this.f = z2;
        this.g = z3;
        Objects.requireNonNull(w0Var);
        this.e = w0Var;
        this.h = z4;
        this.i = i;
        this.j = aVar2;
        this.l = supplier;
    }

    @Override // b.f.j.p.w0
    public void b(l<CloseableReference<b.f.j.j.c>> lVar, x0 x0Var) {
        l<e> lVar2;
        try {
            b.f.j.r.b.b();
            if (!b.f.d.l.b.e(x0Var.e().c)) {
                lVar2 = new a(this, lVar, x0Var, this.h, this.i);
            } else {
                lVar2 = new b(this, lVar, x0Var, new b.f.j.h.e(this.a), this.d, this.h, this.i);
            }
            this.e.b(lVar2, x0Var);
        } finally {
            b.f.j.r.b.b();
        }
    }
}
