package b.f.j.p;

import android.content.ContentResolver;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.ContactsContract;
import b.f.d.g.g;
import b.f.d.l.b;
import b.f.j.j.e;
import com.facebook.imagepipeline.request.ImageRequest;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.concurrent.Executor;
/* compiled from: LocalContentUriFetchProducer.java */
/* loaded from: classes2.dex */
public class e0 extends g0 {
    public final ContentResolver c;

    public e0(Executor executor, g gVar, ContentResolver contentResolver) {
        super(executor, gVar);
        this.c = contentResolver;
    }

    @Override // b.f.j.p.g0
    public e d(ImageRequest imageRequest) throws IOException {
        e eVar;
        InputStream inputStream;
        Uri uri = imageRequest.c;
        Uri uri2 = b.a;
        boolean z2 = false;
        if (uri.getPath() != null && b.c(uri) && "com.android.contacts".equals(uri.getAuthority()) && !uri.getPath().startsWith(b.a.getPath())) {
            z2 = true;
        }
        if (z2) {
            if (uri.toString().endsWith("/photo")) {
                inputStream = this.c.openInputStream(uri);
            } else if (uri.toString().endsWith("/display_photo")) {
                try {
                    AssetFileDescriptor openAssetFileDescriptor = this.c.openAssetFileDescriptor(uri, "r");
                    Objects.requireNonNull(openAssetFileDescriptor);
                    inputStream = openAssetFileDescriptor.createInputStream();
                } catch (IOException unused) {
                    throw new IOException("Contact photo does not exist: " + uri);
                }
            } else {
                InputStream openContactPhotoInputStream = ContactsContract.Contacts.openContactPhotoInputStream(this.c, uri);
                if (openContactPhotoInputStream != null) {
                    inputStream = openContactPhotoInputStream;
                } else {
                    throw new IOException("Contact photo does not exist: " + uri);
                }
            }
            Objects.requireNonNull(inputStream);
            return c(inputStream, -1);
        }
        if (b.b(uri)) {
            try {
                ParcelFileDescriptor openFileDescriptor = this.c.openFileDescriptor(uri, "r");
                Objects.requireNonNull(openFileDescriptor);
                eVar = c(new FileInputStream(openFileDescriptor.getFileDescriptor()), (int) openFileDescriptor.getStatSize());
            } catch (FileNotFoundException unused2) {
                eVar = null;
            }
            if (eVar != null) {
                return eVar;
            }
        }
        InputStream openInputStream = this.c.openInputStream(uri);
        Objects.requireNonNull(openInputStream);
        return c(openInputStream, -1);
    }

    @Override // b.f.j.p.g0
    public String e() {
        return "LocalContentUriFetchProducer";
    }
}
