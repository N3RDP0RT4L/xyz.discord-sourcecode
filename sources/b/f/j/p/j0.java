package b.f.j.p;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.CancellationSignal;
import android.util.Size;
import androidx.annotation.RequiresApi;
import b.f.d.d.f;
import b.f.j.d.e;
import b.f.j.j.c;
import b.f.j.j.d;
import b.f.j.j.h;
import com.facebook.common.references.CloseableReference;
import com.facebook.imagepipeline.request.ImageRequest;
import java.util.Map;
import java.util.concurrent.Executor;
/* compiled from: LocalThumbnailBitmapProducer.java */
@RequiresApi(29)
/* loaded from: classes2.dex */
public class j0 implements w0<CloseableReference<c>> {
    public final Executor a;

    /* renamed from: b  reason: collision with root package name */
    public final ContentResolver f614b;

    /* compiled from: LocalThumbnailBitmapProducer.java */
    /* loaded from: classes2.dex */
    public class a extends e1<CloseableReference<c>> {
        public final /* synthetic */ z0 o;
        public final /* synthetic */ x0 p;
        public final /* synthetic */ ImageRequest q;
        public final /* synthetic */ CancellationSignal r;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(l lVar, z0 z0Var, x0 x0Var, String str, z0 z0Var2, x0 x0Var2, ImageRequest imageRequest, CancellationSignal cancellationSignal) {
            super(lVar, z0Var, x0Var, str);
            this.o = z0Var2;
            this.p = x0Var2;
            this.q = imageRequest;
            this.r = cancellationSignal;
        }

        @Override // b.f.j.p.e1
        public void b(CloseableReference<c> closeableReference) {
            CloseableReference<c> closeableReference2 = closeableReference;
            Class<CloseableReference> cls = CloseableReference.j;
            if (closeableReference2 != null) {
                closeableReference2.close();
            }
        }

        @Override // b.f.j.p.e1
        public Map c(CloseableReference<c> closeableReference) {
            return f.of("createdThumbnail", String.valueOf(closeableReference != null));
        }

        @Override // b.f.j.p.e1
        public CloseableReference<c> d() throws Exception {
            ContentResolver contentResolver = j0.this.f614b;
            Uri uri = this.q.c;
            e eVar = this.q.j;
            int i = 2048;
            int i2 = eVar != null ? eVar.a : 2048;
            if (eVar != null) {
                i = eVar.f562b;
            }
            Bitmap loadThumbnail = contentResolver.loadThumbnail(uri, new Size(i2, i), this.r);
            if (loadThumbnail == null) {
                return null;
            }
            d dVar = new d(loadThumbnail, b.f.j.b.b.a(), h.a, 0);
            this.p.d("image_format", "thumbnail");
            dVar.e(this.p.a());
            return CloseableReference.A(dVar);
        }

        @Override // b.f.j.p.e1
        public void e() {
            super.e();
            this.r.cancel();
        }

        @Override // b.f.j.p.e1
        public void f(Exception exc) {
            super.f(exc);
            this.o.c(this.p, "LocalThumbnailBitmapProducer", false);
            this.p.n("local");
        }

        @Override // b.f.j.p.e1
        public void g(CloseableReference<c> closeableReference) {
            CloseableReference<c> closeableReference2 = closeableReference;
            super.g(closeableReference2);
            this.o.c(this.p, "LocalThumbnailBitmapProducer", closeableReference2 != null);
            this.p.n("local");
        }
    }

    /* compiled from: LocalThumbnailBitmapProducer.java */
    /* loaded from: classes2.dex */
    public class b extends e {
        public final /* synthetic */ e1 a;

        public b(j0 j0Var, e1 e1Var) {
            this.a = e1Var;
        }

        @Override // b.f.j.p.y0
        public void a() {
            this.a.a();
        }
    }

    public j0(Executor executor, ContentResolver contentResolver) {
        this.a = executor;
        this.f614b = contentResolver;
    }

    @Override // b.f.j.p.w0
    public void b(l<CloseableReference<c>> lVar, x0 x0Var) {
        z0 o = x0Var.o();
        ImageRequest e = x0Var.e();
        x0Var.i("local", "thumbnail_bitmap");
        a aVar = new a(lVar, o, x0Var, "LocalThumbnailBitmapProducer", o, x0Var, e, new CancellationSignal());
        x0Var.f(new b(this, aVar));
        this.a.execute(aVar);
    }
}
