package b.f.j.n;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ColorSpace;
import android.graphics.Rect;
import android.os.Build;
import androidx.annotation.VisibleForTesting;
import androidx.core.util.Pools;
import androidx.exifinterface.media.ExifInterface;
import b.f.j.j.e;
import b.f.j.l.d;
import com.facebook.common.references.CloseableReference;
import com.facebook.imagepipeline.platform.PreverificationHelper;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Objects;
/* compiled from: DefaultDecoder.java */
@TargetApi(11)
/* loaded from: classes2.dex */
public abstract class b implements d {
    public static final byte[] a = {-1, ExifInterface.MARKER_EOI};

    /* renamed from: b  reason: collision with root package name */
    public final d f599b;
    public final PreverificationHelper c;
    @VisibleForTesting
    public final Pools.SynchronizedPool<ByteBuffer> d;

    public b(d dVar, int i, Pools.SynchronizedPool synchronizedPool) {
        this.c = Build.VERSION.SDK_INT >= 26 ? new PreverificationHelper() : null;
        this.f599b = dVar;
        this.d = synchronizedPool;
        for (int i2 = 0; i2 < i; i2++) {
            this.d.release(ByteBuffer.allocate(16384));
        }
    }

    public static BitmapFactory.Options e(e eVar, Bitmap.Config config) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = eVar.q;
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(eVar.e(), null, options);
        if (options.outWidth == -1 || options.outHeight == -1) {
            throw new IllegalArgumentException();
        }
        options.inJustDecodeBounds = false;
        options.inDither = true;
        options.inPreferredConfig = config;
        options.inMutable = true;
        return options;
    }

    @Override // b.f.j.n.d
    public CloseableReference<Bitmap> a(e eVar, Bitmap.Config config, Rect rect, ColorSpace colorSpace) {
        BitmapFactory.Options e = e(eVar, config);
        boolean z2 = e.inPreferredConfig != Bitmap.Config.ARGB_8888;
        try {
            InputStream e2 = eVar.e();
            Objects.requireNonNull(e2);
            return c(e2, e, rect, colorSpace);
        } catch (RuntimeException e3) {
            if (z2) {
                return a(eVar, Bitmap.Config.ARGB_8888, rect, colorSpace);
            }
            throw e3;
        }
    }

    /* JADX WARN: Can't wrap try/catch for region: R(6:(2:8|(11:13|14|(1:16)|(1:18)|19|(1:21)|44|22|42|23|26))|44|22|42|23|26) */
    /* JADX WARN: Code restructure failed: missing block: B:24:0x0065, code lost:
        r11 = move-exception;
     */
    /* JADX WARN: Code restructure failed: missing block: B:25:0x0066, code lost:
        r11.printStackTrace();
     */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0046  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x004e  */
    /* JADX WARN: Removed duplicated region for block: B:21:0x005c  */
    @Override // b.f.j.n.d
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public com.facebook.common.references.CloseableReference<android.graphics.Bitmap> b(b.f.j.j.e r10, android.graphics.Bitmap.Config r11, android.graphics.Rect r12, int r13, android.graphics.ColorSpace r14) {
        /*
            r9 = this;
            b.f.i.c r0 = r10.l
            b.f.i.c r1 = b.f.i.b.a
            r2 = 0
            r3 = 1
            if (r0 == r1) goto Ld
            b.f.i.c r1 = b.f.i.b.l
            if (r0 == r1) goto Ld
            goto L32
        Ld:
            com.facebook.common.internal.Supplier<java.io.FileInputStream> r0 = r10.k
            if (r0 == 0) goto L12
            goto L32
        L12:
            com.facebook.common.references.CloseableReference<com.facebook.common.memory.PooledByteBuffer> r0 = r10.j
            java.util.Objects.requireNonNull(r0)
            com.facebook.common.references.CloseableReference<com.facebook.common.memory.PooledByteBuffer> r0 = r10.j
            java.lang.Object r0 = r0.u()
            com.facebook.common.memory.PooledByteBuffer r0 = (com.facebook.common.memory.PooledByteBuffer) r0
            int r1 = r13 + (-2)
            byte r1 = r0.h(r1)
            r4 = -1
            if (r1 != r4) goto L34
            int r1 = r13 + (-1)
            byte r0 = r0.h(r1)
            r1 = -39
            if (r0 != r1) goto L34
        L32:
            r0 = 1
            goto L35
        L34:
            r0 = 0
        L35:
            android.graphics.BitmapFactory$Options r11 = e(r10, r11)
            java.io.InputStream r1 = r10.e()
            java.util.Objects.requireNonNull(r1)
            int r4 = r10.n()
            if (r4 <= r13) goto L4c
            b.f.d.j.a r4 = new b.f.d.j.a
            r4.<init>(r1, r13)
            r1 = r4
        L4c:
            if (r0 != 0) goto L56
            b.f.d.j.b r0 = new b.f.d.j.b
            byte[] r4 = b.f.j.n.b.a
            r0.<init>(r1, r4)
            r1 = r0
        L56:
            android.graphics.Bitmap$Config r0 = r11.inPreferredConfig
            android.graphics.Bitmap$Config r4 = android.graphics.Bitmap.Config.ARGB_8888
            if (r0 == r4) goto L5d
            r2 = 1
        L5d:
            com.facebook.common.references.CloseableReference r10 = r9.c(r1, r11, r12, r14)     // Catch: java.lang.Throwable -> L6a java.lang.RuntimeException -> L6c
            r1.close()     // Catch: java.io.IOException -> L65
            goto L69
        L65:
            r11 = move-exception
            r11.printStackTrace()
        L69:
            return r10
        L6a:
            r10 = move-exception
            goto L84
        L6c:
            r11 = move-exception
            if (r2 == 0) goto L83
            android.graphics.Bitmap$Config r5 = android.graphics.Bitmap.Config.ARGB_8888     // Catch: java.lang.Throwable -> L6a
            r3 = r9
            r4 = r10
            r6 = r12
            r7 = r13
            r8 = r14
            com.facebook.common.references.CloseableReference r10 = r3.b(r4, r5, r6, r7, r8)     // Catch: java.lang.Throwable -> L6a
            r1.close()     // Catch: java.io.IOException -> L7e
            goto L82
        L7e:
            r11 = move-exception
            r11.printStackTrace()
        L82:
            return r10
        L83:
            throw r11     // Catch: java.lang.Throwable -> L6a
        L84:
            r1.close()     // Catch: java.io.IOException -> L88
            goto L8c
        L88:
            r11 = move-exception
            r11.printStackTrace()
        L8c:
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: b.f.j.n.b.b(b.f.j.j.e, android.graphics.Bitmap$Config, android.graphics.Rect, int, android.graphics.ColorSpace):com.facebook.common.references.CloseableReference");
    }

    /* JADX WARN: Removed duplicated region for block: B:43:0x009e A[Catch: all -> 0x00c8, RuntimeException -> 0x00ca, IllegalArgumentException -> 0x00d3, TryCatch #8 {IllegalArgumentException -> 0x00d3, RuntimeException -> 0x00ca, blocks: (B:29:0x006b, B:34:0x0082, B:39:0x0096, B:43:0x009e, B:44:0x00a1, B:47:0x00a5), top: B:76:0x006b, outer: #5 }] */
    /* JADX WARN: Removed duplicated region for block: B:47:0x00a5 A[Catch: all -> 0x00c8, RuntimeException -> 0x00ca, IllegalArgumentException -> 0x00d3, TRY_LEAVE, TryCatch #8 {IllegalArgumentException -> 0x00d3, RuntimeException -> 0x00ca, blocks: (B:29:0x006b, B:34:0x0082, B:39:0x0096, B:43:0x009e, B:44:0x00a1, B:47:0x00a5), top: B:76:0x006b, outer: #5 }] */
    /* JADX WARN: Removed duplicated region for block: B:50:0x00b0 A[ADDED_TO_REGION] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final com.facebook.common.references.CloseableReference<android.graphics.Bitmap> c(java.io.InputStream r10, android.graphics.BitmapFactory.Options r11, android.graphics.Rect r12, android.graphics.ColorSpace r13) {
        /*
            Method dump skipped, instructions count: 250
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.f.j.n.b.c(java.io.InputStream, android.graphics.BitmapFactory$Options, android.graphics.Rect, android.graphics.ColorSpace):com.facebook.common.references.CloseableReference");
    }

    public abstract int d(int i, int i2, BitmapFactory.Options options);
}
