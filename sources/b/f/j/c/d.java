package b.f.j.c;

import java.util.LinkedHashSet;
/* compiled from: BoundedLinkedHashSet.java */
/* loaded from: classes2.dex */
public class d<E> {
    public int a;

    /* renamed from: b  reason: collision with root package name */
    public LinkedHashSet<E> f550b;

    public d(int i) {
        this.f550b = new LinkedHashSet<>(i);
        this.a = i;
    }

    public synchronized boolean a(E e) {
        if (this.f550b.size() == this.a) {
            LinkedHashSet<E> linkedHashSet = this.f550b;
            linkedHashSet.remove(linkedHashSet.iterator().next());
        }
        this.f550b.remove(e);
        return this.f550b.add(e);
    }

    public synchronized boolean b(E e) {
        return this.f550b.contains(e);
    }
}
