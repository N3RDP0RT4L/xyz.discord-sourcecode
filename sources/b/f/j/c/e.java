package b.f.j.c;

import b.f.d.e.a;
import com.facebook.cache.common.CacheKey;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.common.references.CloseableReference;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicBoolean;
/* compiled from: BufferedDiskCache.java */
/* loaded from: classes2.dex */
public class e implements Callable<b.f.j.j.e> {
    public final /* synthetic */ AtomicBoolean j;
    public final /* synthetic */ CacheKey k;
    public final /* synthetic */ f l;

    public e(f fVar, Object obj, AtomicBoolean atomicBoolean, CacheKey cacheKey) {
        this.l = fVar;
        this.j = atomicBoolean;
        this.k = cacheKey;
    }

    @Override // java.util.concurrent.Callable
    public b.f.j.j.e call() throws Exception {
        PooledByteBuffer a;
        try {
            if (!this.j.get()) {
                b.f.j.j.e a2 = this.l.f.a(this.k);
                if (a2 != null) {
                    this.k.b();
                    int i = a.a;
                    Objects.requireNonNull((z) this.l.g);
                } else {
                    this.k.b();
                    int i2 = a.a;
                    Objects.requireNonNull((z) this.l.g);
                    a2 = null;
                    try {
                        a = f.a(this.l, this.k);
                    } catch (Exception unused) {
                    }
                    if (a == null) {
                        return a2;
                    }
                    CloseableReference A = CloseableReference.A(a);
                    try {
                        a2 = new b.f.j.j.e(A);
                    } finally {
                        if (A != null) {
                            A.close();
                        }
                    }
                }
                if (!Thread.interrupted()) {
                    return a2;
                }
                a.i(f.class, "Host thread was interrupted, decreasing reference count");
                a2.close();
                throw new InterruptedException();
            }
            throw new CancellationException();
        } finally {
            throw th;
        }
    }
}
