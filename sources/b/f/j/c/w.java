package b.f.j.c;

import b.f.d.d.j;
import b.f.d.g.b;
import com.facebook.common.references.CloseableReference;
/* compiled from: MemoryCache.java */
/* loaded from: classes2.dex */
public interface w<K, V> extends b {

    /* compiled from: MemoryCache.java */
    /* loaded from: classes2.dex */
    public interface a {
    }

    CloseableReference<V> a(K k, CloseableReference<V> closeableReference);

    boolean contains(K k);

    int d(j<K> jVar);

    CloseableReference<V> get(K k);
}
