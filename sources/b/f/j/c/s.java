package b.f.j.c;

import b.f.d.d.j;
import com.facebook.common.references.CloseableReference;
/* compiled from: InstrumentedMemoryCache.java */
/* loaded from: classes2.dex */
public class s<K, V> implements w<K, V> {
    public final w<K, V> a;

    /* renamed from: b  reason: collision with root package name */
    public final x f555b;

    public s(w<K, V> wVar, x xVar) {
        this.a = wVar;
        this.f555b = xVar;
    }

    @Override // b.f.j.c.w
    public CloseableReference<V> a(K k, CloseableReference<V> closeableReference) {
        this.f555b.c(k);
        return this.a.a(k, closeableReference);
    }

    @Override // b.f.j.c.w
    public int d(j<K> jVar) {
        return this.a.d(jVar);
    }

    @Override // b.f.j.c.w
    public CloseableReference<V> get(K k) {
        CloseableReference<V> closeableReference = this.a.get(k);
        if (closeableReference == null) {
            this.f555b.b(k);
        } else {
            this.f555b.a(k);
        }
        return closeableReference;
    }
}
