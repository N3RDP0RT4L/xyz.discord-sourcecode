package b.f.j.c;

import b.f.j.c.m;
/* compiled from: LruCountingMemoryCache.java */
/* loaded from: classes2.dex */
public class u implements b0<m.a<K, V>> {
    public final /* synthetic */ b0 a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ v f556b;

    public u(v vVar, b0 b0Var) {
        this.f556b = vVar;
        this.a = b0Var;
    }

    @Override // b.f.j.c.b0
    public int a(Object obj) {
        m.a aVar = (m.a) obj;
        if (this.f556b.h) {
            return aVar.f;
        }
        return this.a.a(aVar.f554b.u());
    }
}
