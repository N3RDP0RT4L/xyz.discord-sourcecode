package b.f.j.c;

import androidx.annotation.VisibleForTesting;
import com.facebook.common.references.CloseableReference;
import java.util.Objects;
/* compiled from: CountingMemoryCache.java */
/* loaded from: classes2.dex */
public interface m<K, V> extends w<K, V>, b.f.d.g.b {

    /* compiled from: CountingMemoryCache.java */
    @VisibleForTesting
    /* loaded from: classes2.dex */
    public static class a<K, V> {
        public final K a;

        /* renamed from: b  reason: collision with root package name */
        public final CloseableReference<V> f554b;
        public int c = 0;
        public boolean d = false;
        public final b<K> e;
        public int f;

        public a(K k, CloseableReference<V> closeableReference, b<K> bVar, int i) {
            Objects.requireNonNull(k);
            this.a = k;
            CloseableReference<V> n = CloseableReference.n(closeableReference);
            Objects.requireNonNull(n);
            this.f554b = n;
            this.e = bVar;
            this.f = i;
        }
    }

    /* compiled from: CountingMemoryCache.java */
    /* loaded from: classes2.dex */
    public interface b<K> {
    }

    CloseableReference<V> b(K k);

    CloseableReference<V> c(K k, CloseableReference<V> closeableReference, b<K> bVar);
}
