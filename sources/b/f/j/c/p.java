package b.f.j.c;

import com.facebook.common.memory.PooledByteBuffer;
/* compiled from: EncodedCountingMemoryCacheFactory.java */
/* loaded from: classes2.dex */
public final class p implements b0<PooledByteBuffer> {
    @Override // b.f.j.c.b0
    public int a(PooledByteBuffer pooledByteBuffer) {
        return pooledByteBuffer.size();
    }
}
