package b.f.j.c;

import b.f.d.d.j;
import com.facebook.cache.common.CacheKey;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
/* compiled from: CountingLruMap.java */
/* loaded from: classes2.dex */
public class l<K, V> {
    public final b0<V> a;

    /* renamed from: b  reason: collision with root package name */
    public final LinkedHashMap<K, V> f553b = new LinkedHashMap<>();
    public int c = 0;

    public l(b0<V> b0Var) {
        this.a = b0Var;
    }

    public synchronized int a() {
        return this.f553b.size();
    }

    public synchronized int b() {
        return this.c;
    }

    public final int c(V v) {
        if (v == null) {
            return 0;
        }
        return this.a.a(v);
    }

    public synchronized V d(K k, V v) {
        V remove;
        remove = this.f553b.remove(k);
        this.c -= c(remove);
        this.f553b.put(k, v);
        this.c += c(v);
        return remove;
    }

    public synchronized V e(K k) {
        V remove;
        remove = this.f553b.remove(k);
        this.c -= c(remove);
        return remove;
    }

    public synchronized ArrayList<V> f(j<K> jVar) {
        ArrayList<V> arrayList;
        arrayList = new ArrayList<>();
        Iterator<Map.Entry<K, V>> it = this.f553b.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<K, V> next = it.next();
            if (jVar != null) {
                CacheKey cacheKey = (CacheKey) next.getKey();
            }
            arrayList.add(next.getValue());
            this.c -= c(next.getValue());
            it.remove();
        }
        return arrayList;
    }
}
