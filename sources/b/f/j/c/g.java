package b.f.j.c;

import b.f.b.b.d;
import com.facebook.cache.common.CacheKey;
import java.util.concurrent.Callable;
/* compiled from: BufferedDiskCache.java */
/* loaded from: classes2.dex */
public class g implements Callable<Void> {
    public final /* synthetic */ CacheKey j;
    public final /* synthetic */ f k;

    public g(f fVar, Object obj, CacheKey cacheKey) {
        this.k = fVar;
        this.j = cacheKey;
    }

    @Override // java.util.concurrent.Callable
    public Void call() throws Exception {
        try {
            this.k.f.c(this.j);
            ((d) this.k.a).f(this.j);
            return null;
        } finally {
            throw th;
        }
    }
}
