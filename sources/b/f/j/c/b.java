package b.f.j.c;

import b.c.a.a0.d;
import b.f.j.d.e;
import b.f.j.d.f;
import com.facebook.cache.common.CacheKey;
import com.facebook.common.time.RealtimeSinceBootClock;
import java.util.Objects;
/* compiled from: BitmapMemoryCacheKey.java */
/* loaded from: classes2.dex */
public class b implements CacheKey {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final e f549b;
    public final f c;
    public final b.f.j.d.b d;
    public final CacheKey e;
    public final String f;
    public final int g;
    public final Object h;

    public b(String str, e eVar, f fVar, b.f.j.d.b bVar, CacheKey cacheKey, String str2, Object obj) {
        Objects.requireNonNull(str);
        this.a = str;
        this.f549b = eVar;
        this.c = fVar;
        this.d = bVar;
        this.e = cacheKey;
        this.f = str2;
        Integer valueOf = Integer.valueOf(str.hashCode());
        int i = 0;
        Integer valueOf2 = Integer.valueOf(eVar != null ? eVar.hashCode() : 0);
        Integer valueOf3 = Integer.valueOf(fVar.hashCode());
        int hashCode = valueOf == null ? 0 : valueOf.hashCode();
        int hashCode2 = valueOf2 == null ? 0 : valueOf2.hashCode();
        int hashCode3 = valueOf3 == null ? 0 : valueOf3.hashCode();
        int hashCode4 = bVar == null ? 0 : bVar.hashCode();
        this.g = ((((((((((hashCode + 31) * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + (cacheKey == null ? 0 : cacheKey.hashCode())) * 31) + (str2 != null ? str2.hashCode() : i);
        this.h = obj;
        RealtimeSinceBootClock.get().now();
    }

    @Override // com.facebook.cache.common.CacheKey
    public boolean a() {
        return false;
    }

    @Override // com.facebook.cache.common.CacheKey
    public String b() {
        return this.a;
    }

    @Override // com.facebook.cache.common.CacheKey
    public boolean equals(Object obj) {
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        return this.g == bVar.g && this.a.equals(bVar.a) && d.g0(this.f549b, bVar.f549b) && d.g0(this.c, bVar.c) && d.g0(this.d, bVar.d) && d.g0(this.e, bVar.e) && d.g0(this.f, bVar.f);
    }

    @Override // com.facebook.cache.common.CacheKey
    public int hashCode() {
        return this.g;
    }

    public String toString() {
        return String.format(null, "%s_%s_%s_%s_%s_%s_%d", this.a, this.f549b, this.c, this.d, this.e, this.f, Integer.valueOf(this.g));
    }
}
