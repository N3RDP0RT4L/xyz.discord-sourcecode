package b.f.j.c;

import b.f.b.b.d;
import b.f.b.b.h;
import b.f.b.b.i;
import b.f.d.g.g;
import b.f.d.g.j;
import b.f.j.j.e;
import b.f.j.r.b;
import com.facebook.cache.common.CacheKey;
import com.facebook.common.memory.PooledByteBuffer;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
/* compiled from: BufferedDiskCache.java */
/* loaded from: classes2.dex */
public class f {
    public final h a;

    /* renamed from: b  reason: collision with root package name */
    public final g f551b;
    public final j c;
    public final Executor d;
    public final Executor e;
    public final a0 f = new a0();
    public final r g;

    /* compiled from: BufferedDiskCache.java */
    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public final /* synthetic */ CacheKey j;
        public final /* synthetic */ e k;

        public a(Object obj, CacheKey cacheKey, e eVar) {
            this.j = cacheKey;
            this.k = eVar;
        }

        @Override // java.lang.Runnable
        public void run() {
            try {
                f.b(f.this, this.j, this.k);
            } catch (Throwable th) {
                try {
                    throw th;
                } finally {
                    f.this.f.d(this.j, this.k);
                    e eVar = this.k;
                    if (eVar != null) {
                        eVar.close();
                    }
                }
            }
        }
    }

    public f(h hVar, g gVar, j jVar, Executor executor, Executor executor2, r rVar) {
        this.a = hVar;
        this.f551b = gVar;
        this.c = jVar;
        this.d = executor;
        this.e = executor2;
        this.g = rVar;
    }

    public static PooledByteBuffer a(f fVar, CacheKey cacheKey) throws IOException {
        Objects.requireNonNull(fVar);
        try {
            cacheKey.b();
            int i = b.f.d.e.a.a;
            b.f.a.a b2 = ((d) fVar.a).b(cacheKey);
            if (b2 == null) {
                cacheKey.b();
                Objects.requireNonNull((z) fVar.g);
                return null;
            }
            cacheKey.b();
            Objects.requireNonNull((z) fVar.g);
            FileInputStream fileInputStream = new FileInputStream(b2.a);
            PooledByteBuffer d = fVar.f551b.d(fileInputStream, (int) b2.a());
            fileInputStream.close();
            cacheKey.b();
            return d;
        } catch (IOException e) {
            b.f.d.e.a.n(f.class, e, "Exception reading from cache for %s", cacheKey.b());
            Objects.requireNonNull((z) fVar.g);
            throw e;
        }
    }

    public static void b(f fVar, CacheKey cacheKey, e eVar) {
        Objects.requireNonNull(fVar);
        cacheKey.b();
        int i = b.f.d.e.a.a;
        try {
            ((d) fVar.a).d(cacheKey, new h(fVar, eVar));
            Objects.requireNonNull((z) fVar.g);
            cacheKey.b();
        } catch (IOException e) {
            b.f.d.e.a.n(f.class, e, "Failed to write to disk-cache for key %s", cacheKey.b());
        }
    }

    public void c(CacheKey cacheKey) {
        d dVar = (d) this.a;
        Objects.requireNonNull(dVar);
        try {
            synchronized (dVar.q) {
                List<String> z0 = b.c.a.a0.d.z0(cacheKey);
                int i = 0;
                while (true) {
                    ArrayList arrayList = (ArrayList) z0;
                    if (i < arrayList.size()) {
                        String str = (String) arrayList.get(i);
                        if (dVar.k.c(str, cacheKey)) {
                            dVar.h.add(str);
                            return;
                        }
                        i++;
                    } else {
                        return;
                    }
                }
            }
        } catch (IOException unused) {
            i a2 = i.a();
            a2.d = cacheKey;
            Objects.requireNonNull((b.f.b.a.e) dVar.g);
            a2.b();
        }
    }

    public final z.g<e> d(CacheKey cacheKey, e eVar) {
        cacheKey.b();
        int i = b.f.d.e.a.a;
        Objects.requireNonNull((z) this.g);
        ExecutorService executorService = z.g.a;
        if (eVar instanceof Boolean) {
            return ((Boolean) eVar).booleanValue() ? z.g.e : z.g.f;
        }
        z.g<e> gVar = new z.g<>();
        if (gVar.h(eVar)) {
            return gVar;
        }
        throw new IllegalStateException("Cannot set the result of a completed task.");
    }

    public z.g<e> e(CacheKey cacheKey, AtomicBoolean atomicBoolean) {
        z.g<e> gVar;
        try {
            b.b();
            e a2 = this.f.a(cacheKey);
            if (a2 != null) {
                return d(cacheKey, a2);
            }
            try {
                gVar = z.g.a(new e(this, null, atomicBoolean, cacheKey), this.d);
            } catch (Exception e) {
                b.f.d.e.a.n(f.class, e, "Failed to schedule disk-cache read for %s", ((b.f.b.a.f) cacheKey).a);
                gVar = z.g.c(e);
            }
            return gVar;
        } finally {
            b.b();
        }
    }

    public void f(CacheKey cacheKey, e eVar) {
        try {
            b.b();
            Objects.requireNonNull(cacheKey);
            b.c.a.a0.d.i(Boolean.valueOf(e.u(eVar)));
            this.f.b(cacheKey, eVar);
            e a2 = e.a(eVar);
            try {
                this.e.execute(new a(null, cacheKey, a2));
            } catch (Exception e) {
                b.f.d.e.a.n(f.class, e, "Failed to schedule disk-cache write for %s", cacheKey.b());
                this.f.d(cacheKey, eVar);
                if (a2 != null) {
                    a2.close();
                }
            }
        } finally {
            b.b();
        }
    }
}
