package b.f.j.c;

import android.os.SystemClock;
import androidx.annotation.VisibleForTesting;
import b.c.a.a0.d;
import b.f.d.d.j;
import b.f.d.h.f;
import b.f.j.a.c.c;
import b.f.j.c.m;
import b.f.j.c.w;
import com.facebook.common.internal.Supplier;
import com.facebook.common.references.CloseableReference;
import com.facebook.imagepipeline.cache.MemoryCacheParams;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;
import java.util.WeakHashMap;
/* compiled from: LruCountingMemoryCache.java */
/* loaded from: classes2.dex */
public class v<K, V> implements m<K, V>, w<K, V> {
    public final m.b<K> a;
    @VisibleForTesting

    /* renamed from: b  reason: collision with root package name */
    public final l<K, m.a<K, V>> f557b;
    @VisibleForTesting
    public final l<K, m.a<K, V>> c;
    public final b0<V> d;
    public final Supplier<MemoryCacheParams> e;
    public MemoryCacheParams f;
    public long g = SystemClock.uptimeMillis();
    public final boolean h;
    public final boolean i;

    /* compiled from: LruCountingMemoryCache.java */
    /* loaded from: classes2.dex */
    public class a implements f<V> {
        public final /* synthetic */ m.a a;

        public a(m.a aVar) {
            this.a = aVar;
        }

        /* JADX WARN: Removed duplicated region for block: B:24:0x003d  */
        @Override // b.f.d.h.f
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public void release(V r5) {
            /*
                r4 = this;
                b.f.j.c.v r5 = b.f.j.c.v.this
                b.f.j.c.m$a r0 = r4.a
                java.util.Objects.requireNonNull(r5)
                java.util.Objects.requireNonNull(r0)
                monitor-enter(r5)
                monitor-enter(r5)     // Catch: java.lang.Throwable -> L58
                int r1 = r0.c     // Catch: java.lang.Throwable -> L55
                r2 = 0
                r3 = 1
                if (r1 <= 0) goto L14
                r1 = 1
                goto L15
            L14:
                r1 = 0
            L15:
                b.c.a.a0.d.B(r1)     // Catch: java.lang.Throwable -> L55
                int r1 = r0.c     // Catch: java.lang.Throwable -> L55
                int r1 = r1 - r3
                r0.c = r1     // Catch: java.lang.Throwable -> L55
                monitor-exit(r5)     // Catch: java.lang.Throwable -> L58
                monitor-enter(r5)     // Catch: java.lang.Throwable -> L58
                boolean r1 = r0.d     // Catch: java.lang.Throwable -> L52
                if (r1 != 0) goto L31
                int r1 = r0.c     // Catch: java.lang.Throwable -> L52
                if (r1 != 0) goto L31
                b.f.j.c.l<K, b.f.j.c.m$a<K, V>> r1 = r5.f557b     // Catch: java.lang.Throwable -> L52
                K r2 = r0.a     // Catch: java.lang.Throwable -> L52
                r1.d(r2, r0)     // Catch: java.lang.Throwable -> L52
                monitor-exit(r5)     // Catch: java.lang.Throwable -> L58
                r2 = 1
                goto L32
            L31:
                monitor-exit(r5)     // Catch: java.lang.Throwable -> L58
            L32:
                com.facebook.common.references.CloseableReference r1 = r5.o(r0)     // Catch: java.lang.Throwable -> L58
                monitor-exit(r5)     // Catch: java.lang.Throwable -> L58
                com.facebook.common.references.CloseableReference.s(r1)
                if (r2 == 0) goto L3d
                goto L3e
            L3d:
                r0 = 0
            L3e:
                if (r0 == 0) goto L4b
                b.f.j.c.m$b<K> r1 = r0.e
                if (r1 == 0) goto L4b
                K r0 = r0.a
                b.f.j.a.c.c$a r1 = (b.f.j.a.c.c.a) r1
                r1.a(r0, r3)
            L4b:
                r5.m()
                r5.j()
                return
            L52:
                r0 = move-exception
                monitor-exit(r5)     // Catch: java.lang.Throwable -> L58
                throw r0     // Catch: java.lang.Throwable -> L58
            L55:
                r0 = move-exception
                monitor-exit(r5)     // Catch: java.lang.Throwable -> L58
                throw r0     // Catch: java.lang.Throwable -> L58
            L58:
                r0 = move-exception
                monitor-exit(r5)     // Catch: java.lang.Throwable -> L58
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: b.f.j.c.v.a.release(java.lang.Object):void");
        }
    }

    public v(b0<V> b0Var, w.a aVar, Supplier<MemoryCacheParams> supplier, m.b<K> bVar, boolean z2, boolean z3) {
        new WeakHashMap();
        this.d = b0Var;
        this.f557b = new l<>(new u(this, b0Var));
        this.c = new l<>(new u(this, b0Var));
        this.e = supplier;
        MemoryCacheParams memoryCacheParams = supplier.get();
        d.y(memoryCacheParams, "mMemoryCacheParamsSupplier returned null");
        this.f = memoryCacheParams;
        this.a = bVar;
        this.h = z2;
        this.i = z3;
    }

    public static <K, V> void k(m.a<K, V> aVar) {
        m.b<K> bVar;
        if (aVar != null && (bVar = aVar.e) != null) {
            ((c.a) bVar).a(aVar.a, false);
        }
    }

    @Override // b.f.j.c.w
    public CloseableReference<V> a(K k, CloseableReference<V> closeableReference) {
        return c(k, closeableReference, this.a);
    }

    @Override // b.f.j.c.m
    public CloseableReference<V> b(K k) {
        m.a<K, V> e;
        boolean z2;
        CloseableReference<V> closeableReference;
        Objects.requireNonNull(k);
        synchronized (this) {
            e = this.f557b.e(k);
            z2 = false;
            if (e != null) {
                m.a<K, V> e2 = this.c.e(k);
                Objects.requireNonNull(e2);
                if (e2.c == 0) {
                    z2 = true;
                }
                d.B(z2);
                closeableReference = e2.f554b;
                z2 = true;
            } else {
                closeableReference = null;
            }
        }
        if (z2) {
            k(e);
        }
        return closeableReference;
    }

    /* JADX WARN: Removed duplicated region for block: B:27:0x006f  */
    @Override // b.f.j.c.m
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public com.facebook.common.references.CloseableReference<V> c(K r8, com.facebook.common.references.CloseableReference<V> r9, b.f.j.c.m.b<K> r10) {
        /*
            r7 = this;
            java.util.Objects.requireNonNull(r8)
            java.util.Objects.requireNonNull(r9)
            r7.m()
            monitor-enter(r7)
            b.f.j.c.l<K, b.f.j.c.m$a<K, V>> r0 = r7.f557b     // Catch: java.lang.Throwable -> L7c
            java.lang.Object r0 = r0.e(r8)     // Catch: java.lang.Throwable -> L7c
            b.f.j.c.m$a r0 = (b.f.j.c.m.a) r0     // Catch: java.lang.Throwable -> L7c
            b.f.j.c.l<K, b.f.j.c.m$a<K, V>> r1 = r7.c     // Catch: java.lang.Throwable -> L7c
            java.lang.Object r1 = r1.e(r8)     // Catch: java.lang.Throwable -> L7c
            b.f.j.c.m$a r1 = (b.f.j.c.m.a) r1     // Catch: java.lang.Throwable -> L7c
            r2 = 0
            if (r1 == 0) goto L25
            r7.g(r1)     // Catch: java.lang.Throwable -> L7c
            com.facebook.common.references.CloseableReference r1 = r7.o(r1)     // Catch: java.lang.Throwable -> L7c
            goto L26
        L25:
            r1 = r2
        L26:
            java.lang.Object r3 = r9.u()     // Catch: java.lang.Throwable -> L7c
            b.f.j.c.b0<V> r4 = r7.d     // Catch: java.lang.Throwable -> L7c
            int r3 = r4.a(r3)     // Catch: java.lang.Throwable -> L7c
            monitor-enter(r7)     // Catch: java.lang.Throwable -> L7c
            com.facebook.imagepipeline.cache.MemoryCacheParams r4 = r7.f     // Catch: java.lang.Throwable -> L79
            int r4 = r4.e     // Catch: java.lang.Throwable -> L79
            r5 = 1
            if (r3 > r4) goto L4f
            int r4 = r7.e()     // Catch: java.lang.Throwable -> L79
            com.facebook.imagepipeline.cache.MemoryCacheParams r6 = r7.f     // Catch: java.lang.Throwable -> L79
            int r6 = r6.f2867b     // Catch: java.lang.Throwable -> L79
            int r6 = r6 - r5
            if (r4 > r6) goto L4f
            int r4 = r7.f()     // Catch: java.lang.Throwable -> L79
            com.facebook.imagepipeline.cache.MemoryCacheParams r6 = r7.f     // Catch: java.lang.Throwable -> L79
            int r6 = r6.a     // Catch: java.lang.Throwable -> L79
            int r6 = r6 - r3
            if (r4 > r6) goto L4f
            goto L50
        L4f:
            r5 = 0
        L50:
            monitor-exit(r7)     // Catch: java.lang.Throwable -> L7c
            if (r5 == 0) goto L6c
            boolean r2 = r7.h     // Catch: java.lang.Throwable -> L7c
            if (r2 == 0) goto L5d
            b.f.j.c.m$a r2 = new b.f.j.c.m$a     // Catch: java.lang.Throwable -> L7c
            r2.<init>(r8, r9, r10, r3)     // Catch: java.lang.Throwable -> L7c
            goto L63
        L5d:
            b.f.j.c.m$a r2 = new b.f.j.c.m$a     // Catch: java.lang.Throwable -> L7c
            r3 = -1
            r2.<init>(r8, r9, r10, r3)     // Catch: java.lang.Throwable -> L7c
        L63:
            b.f.j.c.l<K, b.f.j.c.m$a<K, V>> r9 = r7.c     // Catch: java.lang.Throwable -> L7c
            r9.d(r8, r2)     // Catch: java.lang.Throwable -> L7c
            com.facebook.common.references.CloseableReference r2 = r7.n(r2)     // Catch: java.lang.Throwable -> L7c
        L6c:
            monitor-exit(r7)     // Catch: java.lang.Throwable -> L7c
            if (r1 == 0) goto L72
            r1.close()
        L72:
            k(r0)
            r7.j()
            return r2
        L79:
            r8 = move-exception
            monitor-exit(r7)     // Catch: java.lang.Throwable -> L7c
            throw r8     // Catch: java.lang.Throwable -> L7c
        L7c:
            r8 = move-exception
            monitor-exit(r7)     // Catch: java.lang.Throwable -> L7c
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: b.f.j.c.v.c(java.lang.Object, com.facebook.common.references.CloseableReference, b.f.j.c.m$b):com.facebook.common.references.CloseableReference");
    }

    @Override // b.f.j.c.w
    public synchronized boolean contains(K k) {
        boolean containsKey;
        l<K, m.a<K, V>> lVar = this.c;
        synchronized (lVar) {
            containsKey = lVar.f553b.containsKey(k);
        }
        return containsKey;
    }

    @Override // b.f.j.c.w
    public int d(j<K> jVar) {
        ArrayList<m.a<K, V>> f;
        ArrayList<m.a<K, V>> f2;
        synchronized (this) {
            f = this.f557b.f(jVar);
            f2 = this.c.f(jVar);
            h(f2);
        }
        i(f2);
        l(f);
        m();
        j();
        return f2.size();
    }

    public synchronized int e() {
        return this.c.a() - this.f557b.a();
    }

    public synchronized int f() {
        return this.c.b() - this.f557b.b();
    }

    public final synchronized void g(m.a<K, V> aVar) {
        Objects.requireNonNull(aVar);
        d.B(!aVar.d);
        aVar.d = true;
    }

    @Override // b.f.j.c.w
    public CloseableReference<V> get(K k) {
        m.a<K, V> e;
        m.a<K, V> aVar;
        Objects.requireNonNull(k);
        CloseableReference<V> closeableReference = null;
        synchronized (this) {
            e = this.f557b.e(k);
            l<K, m.a<K, V>> lVar = this.c;
            synchronized (lVar) {
                aVar = lVar.f553b.get(k);
            }
            m.a<K, V> aVar2 = aVar;
            if (aVar2 != null) {
                closeableReference = n(aVar2);
            }
        }
        k(e);
        m();
        j();
        return closeableReference;
    }

    public final synchronized void h(ArrayList<m.a<K, V>> arrayList) {
        if (arrayList != null) {
            Iterator<m.a<K, V>> it = arrayList.iterator();
            while (it.hasNext()) {
                g(it.next());
            }
        }
    }

    public final void i(ArrayList<m.a<K, V>> arrayList) {
        if (arrayList != null) {
            Iterator<m.a<K, V>> it = arrayList.iterator();
            while (it.hasNext()) {
                CloseableReference.s(o(it.next()));
            }
        }
    }

    public void j() {
        ArrayList<m.a<K, V>> p;
        synchronized (this) {
            MemoryCacheParams memoryCacheParams = this.f;
            int min = Math.min(memoryCacheParams.d, memoryCacheParams.f2867b - e());
            MemoryCacheParams memoryCacheParams2 = this.f;
            p = p(min, Math.min(memoryCacheParams2.c, memoryCacheParams2.a - f()));
            h(p);
        }
        i(p);
        l(p);
    }

    public final void l(ArrayList<m.a<K, V>> arrayList) {
        if (arrayList != null) {
            Iterator<m.a<K, V>> it = arrayList.iterator();
            while (it.hasNext()) {
                k(it.next());
            }
        }
    }

    public final synchronized void m() {
        if (this.g + this.f.f <= SystemClock.uptimeMillis()) {
            this.g = SystemClock.uptimeMillis();
            MemoryCacheParams memoryCacheParams = this.e.get();
            d.y(memoryCacheParams, "mMemoryCacheParamsSupplier returned null");
            this.f = memoryCacheParams;
        }
    }

    public final synchronized CloseableReference<V> n(m.a<K, V> aVar) {
        synchronized (this) {
            d.B(!aVar.d);
            aVar.c++;
        }
        return CloseableReference.D(aVar.f554b.u(), new a(aVar));
        return CloseableReference.D(aVar.f554b.u(), new a(aVar));
    }

    public final synchronized CloseableReference<V> o(m.a<K, V> aVar) {
        Objects.requireNonNull(aVar);
        return (!aVar.d || aVar.c != 0) ? null : aVar.f554b;
    }

    public final synchronized ArrayList<m.a<K, V>> p(int i, int i2) {
        K next;
        int max = Math.max(i, 0);
        int max2 = Math.max(i2, 0);
        if (this.f557b.a() <= max && this.f557b.b() <= max2) {
            return null;
        }
        ArrayList<m.a<K, V>> arrayList = new ArrayList<>();
        while (true) {
            if (this.f557b.a() <= max && this.f557b.b() <= max2) {
                break;
            }
            l<K, m.a<K, V>> lVar = this.f557b;
            synchronized (lVar) {
                next = lVar.f553b.isEmpty() ? null : lVar.f553b.keySet().iterator().next();
            }
            if (next != null) {
                this.f557b.e(next);
                arrayList.add(this.c.e(next));
            } else if (this.i) {
                l<K, m.a<K, V>> lVar2 = this.f557b;
                synchronized (lVar2) {
                    if (lVar2.f553b.isEmpty()) {
                        lVar2.c = 0;
                    }
                }
            } else {
                throw new IllegalStateException(String.format("key is null, but exclusiveEntries count: %d, size: %d", Integer.valueOf(this.f557b.a()), Integer.valueOf(this.f557b.b())));
            }
        }
        return arrayList;
    }
}
