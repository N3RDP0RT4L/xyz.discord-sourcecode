package b.f.j.c;

import b.f.b.a.f;
import b.f.j.q.b;
import com.facebook.cache.common.CacheKey;
import com.facebook.imagepipeline.request.ImageRequest;
/* compiled from: DefaultCacheKeyFactory.java */
/* loaded from: classes2.dex */
public class n implements i {
    public static n a;

    public CacheKey a(ImageRequest imageRequest, Object obj) {
        return new b(imageRequest.c.toString(), imageRequest.j, imageRequest.k, imageRequest.i, null, null, obj);
    }

    public CacheKey b(ImageRequest imageRequest, Object obj) {
        return new f(imageRequest.c.toString());
    }

    public CacheKey c(ImageRequest imageRequest, Object obj) {
        String str;
        b bVar = imageRequest.f2875s;
        CacheKey cacheKey = null;
        if (bVar != null) {
            CacheKey postprocessorCacheKey = bVar.getPostprocessorCacheKey();
            str = bVar.getClass().getName();
            cacheKey = postprocessorCacheKey;
        } else {
            str = null;
        }
        return new b(imageRequest.c.toString(), imageRequest.j, imageRequest.k, imageRequest.i, cacheKey, str, obj);
    }
}
