package b.f.j.c;

import b.c.a.a0.d;
import b.f.d.e.a;
import b.f.j.j.e;
import com.facebook.cache.common.CacheKey;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
/* compiled from: StagingArea.java */
/* loaded from: classes2.dex */
public class a0 {
    public Map<CacheKey, e> a = new HashMap();

    public synchronized e a(CacheKey cacheKey) {
        Objects.requireNonNull(cacheKey);
        e eVar = this.a.get(cacheKey);
        if (eVar != null) {
            synchronized (eVar) {
                if (!e.u(eVar)) {
                    this.a.remove(cacheKey);
                    a.m(a0.class, "Found closed reference %d for key %s (%d)", Integer.valueOf(System.identityHashCode(eVar)), cacheKey.b(), Integer.valueOf(System.identityHashCode(cacheKey)));
                    return null;
                }
                eVar = e.a(eVar);
            }
        }
        return eVar;
    }

    public synchronized void b(CacheKey cacheKey, e eVar) {
        d.i(Boolean.valueOf(e.u(eVar)));
        e put = this.a.put(cacheKey, e.a(eVar));
        if (put != null) {
            put.close();
        }
        synchronized (this) {
            this.a.size();
            int i = a.a;
        }
    }

    public boolean c(CacheKey cacheKey) {
        e remove;
        Objects.requireNonNull(cacheKey);
        synchronized (this) {
            remove = this.a.remove(cacheKey);
        }
        if (remove == null) {
            return false;
        }
        try {
            return remove.t();
        } finally {
            remove.close();
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:23:0x0060, code lost:
        r7.close();
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public synchronized boolean d(com.facebook.cache.common.CacheKey r6, b.f.j.j.e r7) {
        /*
            r5 = this;
            monitor-enter(r5)
            java.util.Objects.requireNonNull(r6)     // Catch: java.lang.Throwable -> L6d
            java.util.Objects.requireNonNull(r7)     // Catch: java.lang.Throwable -> L6d
            boolean r0 = b.f.j.j.e.u(r7)     // Catch: java.lang.Throwable -> L6d
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch: java.lang.Throwable -> L6d
            b.c.a.a0.d.i(r0)     // Catch: java.lang.Throwable -> L6d
            java.util.Map<com.facebook.cache.common.CacheKey, b.f.j.j.e> r0 = r5.a     // Catch: java.lang.Throwable -> L6d
            java.lang.Object r0 = r0.get(r6)     // Catch: java.lang.Throwable -> L6d
            b.f.j.j.e r0 = (b.f.j.j.e) r0     // Catch: java.lang.Throwable -> L6d
            r1 = 0
            if (r0 != 0) goto L1f
            monitor-exit(r5)
            return r1
        L1f:
            com.facebook.common.references.CloseableReference r2 = r0.c()     // Catch: java.lang.Throwable -> L6d
            com.facebook.common.references.CloseableReference r7 = r7.c()     // Catch: java.lang.Throwable -> L6d
            if (r2 == 0) goto L5e
            if (r7 == 0) goto L5e
            java.lang.Object r3 = r2.u()     // Catch: java.lang.Throwable -> L53
            java.lang.Object r4 = r7.u()     // Catch: java.lang.Throwable -> L53
            if (r3 == r4) goto L36
            goto L5e
        L36:
            java.util.Map<com.facebook.cache.common.CacheKey, b.f.j.j.e> r1 = r5.a     // Catch: java.lang.Throwable -> L53
            r1.remove(r6)     // Catch: java.lang.Throwable -> L53
            r7.close()     // Catch: java.lang.Throwable -> L6d
            r2.close()     // Catch: java.lang.Throwable -> L6d
            r0.close()     // Catch: java.lang.Throwable -> L6d
            monitor-enter(r5)     // Catch: java.lang.Throwable -> L6d
            java.util.Map<com.facebook.cache.common.CacheKey, b.f.j.j.e> r6 = r5.a     // Catch: java.lang.Throwable -> L50
            r6.size()     // Catch: java.lang.Throwable -> L50
            int r6 = b.f.d.e.a.a     // Catch: java.lang.Throwable -> L50
            monitor-exit(r5)     // Catch: java.lang.Throwable -> L6d
            r6 = 1
            monitor-exit(r5)
            return r6
        L50:
            r6 = move-exception
            monitor-exit(r5)     // Catch: java.lang.Throwable -> L6d
            throw r6     // Catch: java.lang.Throwable -> L6d
        L53:
            r6 = move-exception
            r7.close()     // Catch: java.lang.Throwable -> L6d
            r2.close()     // Catch: java.lang.Throwable -> L6d
            r0.close()     // Catch: java.lang.Throwable -> L6d
            throw r6     // Catch: java.lang.Throwable -> L6d
        L5e:
            if (r7 == 0) goto L63
            r7.close()     // Catch: java.lang.Throwable -> L6d
        L63:
            if (r2 == 0) goto L68
            r2.close()     // Catch: java.lang.Throwable -> L6d
        L68:
            r0.close()     // Catch: java.lang.Throwable -> L6d
            monitor-exit(r5)
            return r1
        L6d:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: b.f.j.c.a0.d(com.facebook.cache.common.CacheKey, b.f.j.j.e):boolean");
    }
}
