package b.f.j.h;

import b.f.j.j.h;
import b.f.j.j.i;
import java.util.Collections;
import java.util.List;
/* compiled from: SimpleProgressiveJpegConfig.java */
/* loaded from: classes2.dex */
public class f implements d {
    public final c a = new b(null);

    /* compiled from: SimpleProgressiveJpegConfig.java */
    /* loaded from: classes2.dex */
    public static class b implements c {
        public b(a aVar) {
        }

        @Override // b.f.j.h.f.c
        public List<Integer> a() {
            return Collections.EMPTY_LIST;
        }

        @Override // b.f.j.h.f.c
        public int b() {
            return 0;
        }
    }

    /* compiled from: SimpleProgressiveJpegConfig.java */
    /* loaded from: classes2.dex */
    public interface c {
        List<Integer> a();

        int b();
    }

    @Override // b.f.j.h.d
    public i a(int i) {
        return new h(i, i >= this.a.b(), false);
    }

    @Override // b.f.j.h.d
    public int b(int i) {
        List<Integer> a2 = this.a.a();
        if (a2 == null || a2.isEmpty()) {
            return i + 1;
        }
        for (int i2 = 0; i2 < a2.size(); i2++) {
            if (a2.get(i2).intValue() > i) {
                return a2.get(i2).intValue();
            }
        }
        return Integer.MAX_VALUE;
    }
}
