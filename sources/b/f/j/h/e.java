package b.f.j.h;

import b.c.a.a0.d;
import b.f.d.d.m;
import b.f.d.g.a;
import b.f.d.g.f;
import java.io.IOException;
import java.util.Objects;
/* compiled from: ProgressiveJpegParser.java */
/* loaded from: classes2.dex */
public class e {
    public boolean g;
    public final a h;
    public int c = 0;

    /* renamed from: b  reason: collision with root package name */
    public int f585b = 0;
    public int d = 0;
    public int f = 0;
    public int e = 0;
    public int a = 0;

    public e(a aVar) {
        Objects.requireNonNull(aVar);
        this.h = aVar;
    }

    /* JADX WARN: Code restructure failed: missing block: B:52:0x0097, code lost:
        r11.a = 4;
     */
    /* JADX WARN: Code restructure failed: missing block: B:64:0x00b7, code lost:
        if (r11.a == 6) goto L90;
     */
    /* JADX WARN: Code restructure failed: missing block: B:66:0x00bb, code lost:
        if (r11.e == r0) goto L91;
     */
    /* JADX WARN: Code restructure failed: missing block: B:67:0x00bd, code lost:
        return true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:90:?, code lost:
        return false;
     */
    /* JADX WARN: Code restructure failed: missing block: B:91:?, code lost:
        return false;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final boolean a(java.io.InputStream r12) {
        /*
            Method dump skipped, instructions count: 201
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.f.j.h.e.a(java.io.InputStream):boolean");
    }

    public boolean b(b.f.j.j.e eVar) {
        if (this.a == 6 || eVar.n() <= this.c) {
            return false;
        }
        f fVar = new f(eVar.f(), this.h.get(16384), this.h);
        try {
            try {
                d.c2(fVar, this.c);
                return a(fVar);
            } catch (IOException e) {
                m.a(e);
                throw new RuntimeException(e);
            }
        } finally {
            b.f.d.d.a.b(fVar);
        }
    }
}
