package b.f.j.h;

import android.graphics.Bitmap;
import b.f.j.d.b;
import b.f.j.j.c;
import b.f.j.j.e;
import b.f.j.j.h;
import b.f.j.j.i;
import b.f.j.n.d;
import com.facebook.common.references.CloseableReference;
import com.facebook.imagepipeline.decoder.DecodeException;
import java.io.InputStream;
import java.util.Objects;
/* compiled from: DefaultImageDecoder.java */
/* loaded from: classes2.dex */
public class a implements b {
    public final b a;

    /* renamed from: b  reason: collision with root package name */
    public final b f584b;
    public final d c;
    public final b d = new C0071a();

    /* compiled from: DefaultImageDecoder.java */
    /* renamed from: b.f.j.h.a$a  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class C0071a implements b {
        public C0071a() {
        }

        @Override // b.f.j.h.b
        public c a(e eVar, int i, i iVar, b bVar) {
            eVar.x();
            b.f.i.c cVar = eVar.l;
            if (cVar == b.f.i.b.a) {
                CloseableReference<Bitmap> b2 = a.this.c.b(eVar, bVar.d, null, i, null);
                try {
                    b.c.a.a0.d.g1(null, b2);
                    eVar.x();
                    int i2 = eVar.m;
                    eVar.x();
                    b.f.j.j.d dVar = new b.f.j.j.d(b2, iVar, i2, eVar.n);
                    Boolean bool = Boolean.FALSE;
                    if (c.j.contains("is_rounded")) {
                        dVar.k.put("is_rounded", bool);
                    }
                    return dVar;
                } finally {
                    b2.close();
                }
            } else if (cVar == b.f.i.b.c) {
                a aVar = a.this;
                Objects.requireNonNull(aVar);
                eVar.x();
                if (eVar.o != -1) {
                    eVar.x();
                    if (eVar.p != -1) {
                        Objects.requireNonNull(bVar);
                        b bVar2 = aVar.a;
                        if (bVar2 != null) {
                            return bVar2.a(eVar, i, iVar, bVar);
                        }
                        return aVar.b(eVar, bVar);
                    }
                }
                throw new DecodeException("image width or height is incorrect", eVar);
            } else if (cVar == b.f.i.b.j) {
                b bVar3 = a.this.f584b;
                if (bVar3 != null) {
                    return bVar3.a(eVar, i, iVar, bVar);
                }
                throw new DecodeException("Animated WebP support not set up!", eVar);
            } else if (cVar != b.f.i.c.a) {
                return a.this.b(eVar, bVar);
            } else {
                throw new DecodeException("unknown image format", eVar);
            }
        }
    }

    public a(b bVar, b bVar2, d dVar) {
        this.a = bVar;
        this.f584b = bVar2;
        this.c = dVar;
    }

    @Override // b.f.j.h.b
    public c a(e eVar, int i, i iVar, b bVar) {
        InputStream e;
        Objects.requireNonNull(bVar);
        eVar.x();
        b.f.i.c cVar = eVar.l;
        if ((cVar == null || cVar == b.f.i.c.a) && (e = eVar.e()) != null) {
            eVar.l = b.f.i.d.b(e);
        }
        return this.d.a(eVar, i, iVar, bVar);
    }

    public b.f.j.j.d b(e eVar, b bVar) {
        CloseableReference<Bitmap> a = this.c.a(eVar, bVar.d, null, null);
        try {
            b.c.a.a0.d.g1(null, a);
            i iVar = h.a;
            eVar.x();
            int i = eVar.m;
            eVar.x();
            b.f.j.j.d dVar = new b.f.j.j.d(a, iVar, i, eVar.n);
            Boolean bool = Boolean.FALSE;
            if (c.j.contains("is_rounded")) {
                dVar.k.put("is_rounded", bool);
            }
            return dVar;
        } finally {
            a.close();
        }
    }
}
