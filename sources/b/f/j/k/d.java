package b.f.j.k;

import androidx.annotation.NonNull;
import b.f.j.p.x0;
import b.f.j.p.z0;
/* compiled from: RequestListener2.java */
/* loaded from: classes2.dex */
public interface d extends z0 {
    void b(@NonNull x0 x0Var);

    void f(@NonNull x0 x0Var);

    void h(@NonNull x0 x0Var, Throwable th);

    void i(@NonNull x0 x0Var);
}
