package b.f.j.f;

import b.c.a.a0.d;
import b.f.j.p.b;
import b.f.j.p.d1;
import com.facebook.common.references.CloseableReference;
import java.util.Objects;
/* compiled from: AbstractProducerToDataSourceAdapter.java */
/* loaded from: classes2.dex */
public class a extends b<T> {

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ b f583b;

    public a(b bVar) {
        this.f583b = bVar;
    }

    @Override // b.f.j.p.b
    public void g() {
        b bVar = this.f583b;
        synchronized (bVar) {
            d.B(bVar.i());
        }
    }

    @Override // b.f.j.p.b
    public void h(Throwable th) {
        b bVar = this.f583b;
        if (bVar.k(th, bVar.h.a())) {
            bVar.i.h(bVar.h, th);
        }
    }

    @Override // b.f.j.p.b
    public void i(T t, int i) {
        b bVar = this.f583b;
        d1 d1Var = bVar.h;
        d dVar = (d) bVar;
        Objects.requireNonNull(dVar);
        CloseableReference n = CloseableReference.n((CloseableReference) t);
        boolean e = b.e(i);
        if (dVar.m(n, e, d1Var.a()) && e) {
            dVar.i.f(dVar.h);
        }
    }

    @Override // b.f.j.p.b
    public void j(float f) {
        this.f583b.l(f);
    }
}
