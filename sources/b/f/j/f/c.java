package b.f.j.f;

import android.graphics.Bitmap;
import b.f.e.d;
import b.f.j.j.b;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
/* compiled from: BaseBitmapDataSubscriber.java */
/* loaded from: classes2.dex */
public abstract class c extends d<CloseableReference<b.f.j.j.c>> {
    public abstract void onNewResultImpl(Bitmap bitmap);

    @Override // b.f.e.d
    public void onNewResultImpl(DataSource<CloseableReference<b.f.j.j.c>> dataSource) {
        if (dataSource.c()) {
            CloseableReference<b.f.j.j.c> result = dataSource.getResult();
            Bitmap bitmap = null;
            if (result != null && (result.u() instanceof b)) {
                bitmap = ((b) result.u()).f();
            }
            try {
                onNewResultImpl(bitmap);
            } finally {
                Class<CloseableReference> cls = CloseableReference.j;
                if (result != null) {
                    result.close();
                }
            }
        }
    }
}
