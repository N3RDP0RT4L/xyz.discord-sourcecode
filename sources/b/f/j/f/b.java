package b.f.j.f;

import b.f.e.c;
import b.f.j.k.d;
import b.f.j.p.d1;
import b.f.j.p.w0;
/* compiled from: AbstractProducerToDataSourceAdapter.java */
/* loaded from: classes2.dex */
public abstract class b<T> extends c<T> {
    public final d1 h;
    public final d i;

    public b(w0<T> w0Var, d1 d1Var, d dVar) {
        b.f.j.r.b.b();
        this.h = d1Var;
        this.i = dVar;
        this.a = d1Var.h;
        b.f.j.r.b.b();
        dVar.b(d1Var);
        b.f.j.r.b.b();
        b.f.j.r.b.b();
        w0Var.b(new a(this), d1Var);
        b.f.j.r.b.b();
        b.f.j.r.b.b();
    }

    @Override // b.f.e.c, com.facebook.datasource.DataSource
    public boolean close() {
        if (!super.close()) {
            return false;
        }
        if (c()) {
            return true;
        }
        this.i.i(this.h);
        this.h.u();
        return true;
    }
}
