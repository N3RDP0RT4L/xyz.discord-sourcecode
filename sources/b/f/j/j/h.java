package b.f.j.j;
/* compiled from: ImmutableQualityInfo.java */
/* loaded from: classes2.dex */
public class h implements i {
    public static final i a = new h(Integer.MAX_VALUE, true, true);

    /* renamed from: b  reason: collision with root package name */
    public int f587b;
    public boolean c;
    public boolean d;

    public h(int i, boolean z2, boolean z3) {
        this.f587b = i;
        this.c = z2;
        this.d = z3;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof h)) {
            return false;
        }
        h hVar = (h) obj;
        return this.f587b == hVar.f587b && this.c == hVar.c && this.d == hVar.d;
    }

    public int hashCode() {
        int i = 0;
        int i2 = this.f587b ^ (this.c ? 4194304 : 0);
        if (this.d) {
            i = 8388608;
        }
        return i2 ^ i;
    }
}
