package b.f.j.j;

import android.graphics.Bitmap;
import b.f.j.a.a.e;
import com.facebook.common.references.CloseableReference;
/* compiled from: CloseableAnimatedImage.java */
/* loaded from: classes2.dex */
public class a extends c {
    public e l;
    public boolean m = true;

    public a(e eVar) {
        this.l = eVar;
    }

    @Override // b.f.j.j.c
    public synchronized int c() {
        e eVar;
        eVar = this.l;
        return eVar == null ? 0 : eVar.a.j();
    }

    @Override // b.f.j.j.c, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        synchronized (this) {
            e eVar = this.l;
            if (eVar != null) {
                this.l = null;
                synchronized (eVar) {
                    CloseableReference<Bitmap> closeableReference = eVar.f540b;
                    Class<CloseableReference> cls = CloseableReference.j;
                    if (closeableReference != null) {
                        closeableReference.close();
                    }
                    eVar.f540b = null;
                    CloseableReference.t(eVar.c);
                    eVar.c = null;
                }
            }
        }
    }

    @Override // b.f.j.j.c
    public boolean d() {
        return this.m;
    }

    @Override // com.facebook.imagepipeline.image.ImageInfo
    public synchronized int getHeight() {
        e eVar;
        eVar = this.l;
        return eVar == null ? 0 : eVar.a.getHeight();
    }

    @Override // com.facebook.imagepipeline.image.ImageInfo
    public synchronized int getWidth() {
        e eVar;
        eVar = this.l;
        return eVar == null ? 0 : eVar.a.getWidth();
    }

    @Override // b.f.j.j.c
    public synchronized boolean isClosed() {
        return this.l == null;
    }
}
