package b.f.j.j;

import android.graphics.ColorSpace;
import b.c.a.a0.d;
import b.f.d.g.h;
import b.f.i.c;
import b.f.j.d.a;
import com.facebook.common.internal.Supplier;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.common.references.CloseableReference;
import java.io.Closeable;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Objects;
/* compiled from: EncodedImage.java */
/* loaded from: classes2.dex */
public class e implements Closeable {
    public final CloseableReference<PooledByteBuffer> j;
    public final Supplier<FileInputStream> k;
    public c l;
    public int m;
    public int n;
    public int o;
    public int p;
    public int q;
    public int r;

    /* renamed from: s  reason: collision with root package name */
    public a f586s;
    public ColorSpace t;
    public boolean u;

    public e(CloseableReference<PooledByteBuffer> closeableReference) {
        this.l = c.a;
        this.m = -1;
        this.n = 0;
        this.o = -1;
        this.p = -1;
        this.q = 1;
        this.r = -1;
        d.i(Boolean.valueOf(CloseableReference.y(closeableReference)));
        this.j = closeableReference.clone();
        this.k = null;
    }

    public static e a(e eVar) {
        e eVar2 = null;
        if (eVar != null) {
            Supplier<FileInputStream> supplier = eVar.k;
            if (supplier != null) {
                eVar2 = new e(supplier, eVar.r);
            } else {
                CloseableReference n = CloseableReference.n(eVar.j);
                if (n != null) {
                    try {
                        eVar2 = new e(n);
                    } finally {
                        n.close();
                    }
                }
                if (n != null) {
                }
            }
            if (eVar2 != null) {
                eVar2.b(eVar);
            }
        }
        return eVar2;
    }

    public static boolean s(e eVar) {
        return eVar.m >= 0 && eVar.o >= 0 && eVar.p >= 0;
    }

    public static boolean u(e eVar) {
        return eVar != null && eVar.t();
    }

    public void b(e eVar) {
        eVar.x();
        this.l = eVar.l;
        eVar.x();
        this.o = eVar.o;
        eVar.x();
        this.p = eVar.p;
        eVar.x();
        this.m = eVar.m;
        eVar.x();
        this.n = eVar.n;
        this.q = eVar.q;
        this.r = eVar.n();
        this.f586s = eVar.f586s;
        eVar.x();
        this.t = eVar.t;
        this.u = eVar.u;
    }

    public CloseableReference<PooledByteBuffer> c() {
        return CloseableReference.n(this.j);
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        CloseableReference<PooledByteBuffer> closeableReference = this.j;
        Class<CloseableReference> cls = CloseableReference.j;
        if (closeableReference != null) {
            closeableReference.close();
        }
    }

    /* JADX WARN: Finally extract failed */
    public String d(int i) {
        CloseableReference<PooledByteBuffer> c = c();
        if (c == null) {
            return "";
        }
        int min = Math.min(n(), i);
        byte[] bArr = new byte[min];
        try {
            c.u().i(0, bArr, 0, min);
            c.close();
            StringBuilder sb = new StringBuilder(min * 2);
            for (int i2 = 0; i2 < min; i2++) {
                sb.append(String.format("%02X", Byte.valueOf(bArr[i2])));
            }
            return sb.toString();
        } catch (Throwable th) {
            c.close();
            throw th;
        }
    }

    public InputStream e() {
        Supplier<FileInputStream> supplier = this.k;
        if (supplier != null) {
            return supplier.get();
        }
        CloseableReference n = CloseableReference.n(this.j);
        if (n == null) {
            return null;
        }
        try {
            return new h((PooledByteBuffer) n.u());
        } finally {
            n.close();
        }
    }

    public InputStream f() {
        InputStream e = e();
        Objects.requireNonNull(e);
        return e;
    }

    public int n() {
        CloseableReference<PooledByteBuffer> closeableReference = this.j;
        if (closeableReference == null) {
            return this.r;
        }
        closeableReference.u();
        return this.j.u().size();
    }

    /* JADX WARN: Can't wrap try/catch for region: R(7:10|156|11|(1:13)(2:14|(1:16)(5:17|(1:19)|171|20|(1:22)(2:23|(1:25)(2:26|(5:28|159|29|32|(1:41))))))|169|37|(0)) */
    /* JADX WARN: Code restructure failed: missing block: B:36:0x00a6, code lost:
        if (r1 == null) goto L40;
     */
    /* JADX WARN: Code restructure failed: missing block: B:38:0x00ac, code lost:
        r1 = move-exception;
     */
    /* JADX WARN: Code restructure failed: missing block: B:39:0x00ad, code lost:
        r1.printStackTrace();
     */
    /* JADX WARN: Removed duplicated region for block: B:111:0x01ab  */
    /* JADX WARN: Removed duplicated region for block: B:112:0x01ac A[Catch: IOException -> 0x01e5, TryCatch #5 {IOException -> 0x01e5, blocks: (B:58:0x010a, B:59:0x010d, B:63:0x011a, B:76:0x0135, B:79:0x0142, B:81:0x014a, B:91:0x016a, B:95:0x017a, B:100:0x0188, B:102:0x0190, B:104:0x0194, B:105:0x019a, B:109:0x01a0, B:112:0x01ac, B:113:0x01b2, B:117:0x01ba, B:120:0x01c3, B:125:0x01d2, B:128:0x01da, B:131:0x01e1), top: B:163:0x010a }] */
    /* JADX WARN: Removed duplicated region for block: B:41:0x00b2  */
    /* JADX WARN: Removed duplicated region for block: B:87:0x0161  */
    /* JADX WARN: Removed duplicated region for block: B:88:0x0163  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void q() {
        /*
            Method dump skipped, instructions count: 552
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.f.j.j.e.q():void");
    }

    public synchronized boolean t() {
        boolean z2;
        if (!CloseableReference.y(this.j)) {
            if (this.k == null) {
                z2 = false;
            }
        }
        z2 = true;
        return z2;
    }

    public final void x() {
        if (this.o < 0 || this.p < 0) {
            q();
        }
    }

    public e(Supplier<FileInputStream> supplier, int i) {
        this.l = c.a;
        this.m = -1;
        this.n = 0;
        this.o = -1;
        this.p = -1;
        this.q = 1;
        this.r = -1;
        Objects.requireNonNull(supplier);
        this.j = null;
        this.k = supplier;
        this.r = i;
    }
}
