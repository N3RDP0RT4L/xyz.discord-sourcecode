package b.f.j.a.c;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import b.f.j.a.a.b;
import b.f.j.a.a.c;
import b.f.j.a.a.d;
import b.f.j.a.a.e;
import java.util.Objects;
/* compiled from: AnimatedDrawableBackendImpl.java */
/* loaded from: classes2.dex */
public class a implements b.f.j.a.a.a {
    public final b.f.j.a.d.a a;

    /* renamed from: b  reason: collision with root package name */
    public final e f544b;
    public final c c;
    public final Rect d;
    public final int[] e;
    public final b[] f;
    public final Rect g = new Rect();
    public final Rect h = new Rect();
    public final boolean i;
    public Bitmap j;

    public a(b.f.j.a.d.a aVar, e eVar, Rect rect, boolean z2) {
        this.a = aVar;
        this.f544b = eVar;
        c cVar = eVar.a;
        this.c = cVar;
        int[] i = cVar.i();
        this.e = i;
        Objects.requireNonNull(aVar);
        for (int i2 = 0; i2 < i.length; i2++) {
            if (i[i2] < 11) {
                i[i2] = 100;
            }
        }
        b.f.j.a.d.a aVar2 = this.a;
        int[] iArr = this.e;
        Objects.requireNonNull(aVar2);
        for (int i3 : iArr) {
        }
        b.f.j.a.d.a aVar3 = this.a;
        int[] iArr2 = this.e;
        Objects.requireNonNull(aVar3);
        int[] iArr3 = new int[iArr2.length];
        int i4 = 0;
        for (int i5 = 0; i5 < iArr2.length; i5++) {
            iArr3[i5] = i4;
            i4 += iArr2[i5];
        }
        this.d = a(this.c, rect);
        this.i = z2;
        this.f = new b[this.c.a()];
        for (int i6 = 0; i6 < this.c.a(); i6++) {
            this.f[i6] = this.c.g(i6);
        }
    }

    public static Rect a(c cVar, Rect rect) {
        if (rect == null) {
            return new Rect(0, 0, cVar.getWidth(), cVar.getHeight());
        }
        return new Rect(0, 0, Math.min(rect.width(), cVar.getWidth()), Math.min(rect.height(), cVar.getHeight()));
    }

    public int b() {
        return this.c.a();
    }

    public final synchronized Bitmap c(int i, int i2) {
        Bitmap bitmap = this.j;
        if (bitmap != null && (bitmap.getWidth() < i || this.j.getHeight() < i2)) {
            synchronized (this) {
                Bitmap bitmap2 = this.j;
                if (bitmap2 != null) {
                    bitmap2.recycle();
                    this.j = null;
                }
            }
        }
        if (this.j == null) {
            this.j = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        }
        this.j.eraseColor(0);
        return this.j;
    }

    public void d(int i, Canvas canvas) {
        d e = this.c.e(i);
        try {
            if (e.getWidth() > 0 && e.getHeight() > 0) {
                if (this.c.f()) {
                    f(canvas, e);
                } else {
                    e(canvas, e);
                }
            }
        } finally {
            e.dispose();
        }
    }

    public final void e(Canvas canvas, d dVar) {
        int i;
        int i2;
        int i3;
        int i4;
        if (this.i) {
            float max = Math.max(dVar.getWidth() / Math.min(dVar.getWidth(), canvas.getWidth()), dVar.getHeight() / Math.min(dVar.getHeight(), canvas.getHeight()));
            i3 = (int) (dVar.getWidth() / max);
            i2 = (int) (dVar.getHeight() / max);
            i = (int) (dVar.b() / max);
            i4 = (int) (dVar.c() / max);
        } else {
            i3 = dVar.getWidth();
            i2 = dVar.getHeight();
            i = dVar.b();
            i4 = dVar.c();
        }
        synchronized (this) {
            Bitmap c = c(i3, i2);
            this.j = c;
            dVar.a(i3, i2, c);
            canvas.save();
            canvas.translate(i, i4);
            canvas.drawBitmap(this.j, 0.0f, 0.0f, (Paint) null);
            canvas.restore();
        }
    }

    public final void f(Canvas canvas, d dVar) {
        double width = this.d.width() / this.c.getWidth();
        double height = this.d.height() / this.c.getHeight();
        int round = (int) Math.round(dVar.getWidth() * width);
        int round2 = (int) Math.round(dVar.getHeight() * height);
        int b2 = (int) (dVar.b() * width);
        int c = (int) (dVar.c() * height);
        synchronized (this) {
            int width2 = this.d.width();
            int height2 = this.d.height();
            c(width2, height2);
            Bitmap bitmap = this.j;
            if (bitmap != null) {
                dVar.a(round, round2, bitmap);
            }
            this.g.set(0, 0, width2, height2);
            this.h.set(b2, c, width2 + b2, height2 + c);
            Bitmap bitmap2 = this.j;
            if (bitmap2 != null) {
                canvas.drawBitmap(bitmap2, this.g, this.h, (Paint) null);
            }
        }
    }
}
