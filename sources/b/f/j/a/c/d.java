package b.f.j.a.c;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import b.f.j.a.a.b;
import com.facebook.common.references.CloseableReference;
/* compiled from: AnimatedImageCompositor.java */
/* loaded from: classes2.dex */
public class d {
    public final b.f.j.a.a.a a;

    /* renamed from: b  reason: collision with root package name */
    public final a f547b;
    public final Paint c;

    /* compiled from: AnimatedImageCompositor.java */
    /* loaded from: classes2.dex */
    public interface a {
        void a(int i, Bitmap bitmap);

        CloseableReference<Bitmap> b(int i);
    }

    public d(b.f.j.a.a.a aVar, a aVar2) {
        this.a = aVar;
        this.f547b = aVar2;
        Paint paint = new Paint();
        this.c = paint;
        paint.setColor(0);
        paint.setStyle(Paint.Style.FILL);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
    }

    public final void a(Canvas canvas, b bVar) {
        int i = bVar.a;
        int i2 = bVar.f539b;
        canvas.drawRect(i, i2, i + bVar.c, i2 + bVar.d, this.c);
    }

    public final boolean b(b bVar) {
        return bVar.a == 0 && bVar.f539b == 0 && bVar.c == ((b.f.j.a.c.a) this.a).d.width() && bVar.d == ((b.f.j.a.c.a) this.a).d.height();
    }

    public final boolean c(int i) {
        if (i == 0) {
            return true;
        }
        b[] bVarArr = ((b.f.j.a.c.a) this.a).f;
        b bVar = bVarArr[i];
        b bVar2 = bVarArr[i - 1];
        if (bVar.e != 2 || !b(bVar)) {
            return bVar2.f == 2 && b(bVar2);
        }
        return true;
    }

    /* JADX WARN: Removed duplicated region for block: B:18:0x003c  */
    /* JADX WARN: Removed duplicated region for block: B:21:0x0044  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void d(int r10, android.graphics.Bitmap r11) {
        /*
            Method dump skipped, instructions count: 207
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.f.j.a.c.d.d(int, android.graphics.Bitmap):void");
    }
}
