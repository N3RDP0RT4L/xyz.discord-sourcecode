package b.f.j.a.a;

import android.graphics.Bitmap;
import b.f.j.t.a;
import com.facebook.common.references.CloseableReference;
import java.util.List;
/* compiled from: AnimatedImageResultBuilder.java */
/* loaded from: classes2.dex */
public class f {
    public final c a;

    /* renamed from: b  reason: collision with root package name */
    public CloseableReference<Bitmap> f541b;
    public List<CloseableReference<Bitmap>> c;
    public int d;
    public a e;

    public f(c cVar) {
        this.a = cVar;
    }

    public e a() {
        try {
            e eVar = new e(this);
            CloseableReference<Bitmap> closeableReference = this.f541b;
            if (closeableReference != null) {
                closeableReference.close();
            }
            this.f541b = null;
            CloseableReference.t(this.c);
            this.c = null;
            return eVar;
        } catch (Throwable th) {
            CloseableReference<Bitmap> closeableReference2 = this.f541b;
            Class<CloseableReference> cls = CloseableReference.j;
            if (closeableReference2 != null) {
                closeableReference2.close();
            }
            this.f541b = null;
            CloseableReference.t(this.c);
            this.c = null;
            throw th;
        }
    }
}
