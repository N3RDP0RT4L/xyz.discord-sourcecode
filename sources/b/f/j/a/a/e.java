package b.f.j.a.a;

import android.graphics.Bitmap;
import b.f.j.t.a;
import com.facebook.common.references.CloseableReference;
import java.util.List;
import java.util.Objects;
/* compiled from: AnimatedImageResult.java */
/* loaded from: classes2.dex */
public class e {
    public final c a;

    /* renamed from: b  reason: collision with root package name */
    public CloseableReference<Bitmap> f540b;
    public List<CloseableReference<Bitmap>> c;
    public a d;

    public e(f fVar) {
        c cVar = fVar.a;
        Objects.requireNonNull(cVar);
        this.a = cVar;
        this.f540b = CloseableReference.n(fVar.f541b);
        this.c = CloseableReference.q(fVar.c);
        this.d = fVar.e;
    }
}
