package b.f.j.a.a;

import android.graphics.Bitmap;
/* compiled from: AnimatedImage.java */
/* loaded from: classes2.dex */
public interface c {
    int a();

    int b();

    Bitmap.Config d();

    d e(int i);

    boolean f();

    b g(int i);

    int getHeight();

    int getWidth();

    int[] i();

    int j();
}
