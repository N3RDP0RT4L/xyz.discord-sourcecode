package b.f.j.a.a;

import android.graphics.Bitmap;
/* compiled from: AnimatedImageFrame.java */
/* loaded from: classes2.dex */
public interface d {
    void a(int i, int i2, Bitmap bitmap);

    int b();

    int c();

    void dispose();

    int getHeight();

    int getWidth();
}
