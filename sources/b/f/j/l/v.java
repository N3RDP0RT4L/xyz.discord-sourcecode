package b.f.j.l;

import com.facebook.imagepipeline.memory.BasePool;
/* compiled from: NoOpPoolStatsTracker.java */
/* loaded from: classes2.dex */
public class v implements z {
    public static v a;

    public static synchronized v h() {
        v vVar;
        synchronized (v.class) {
            if (a == null) {
                a = new v();
            }
            vVar = a;
        }
        return vVar;
    }

    @Override // b.f.j.l.z
    public void a(int i) {
    }

    @Override // b.f.j.l.z
    public void b(int i) {
    }

    @Override // b.f.j.l.z
    public void c(BasePool basePool) {
    }

    @Override // b.f.j.l.z
    public void d() {
    }

    @Override // b.f.j.l.z
    public void e(int i) {
    }

    @Override // b.f.j.l.z
    public void f() {
    }

    @Override // b.f.j.l.z
    public void g(int i) {
    }
}
