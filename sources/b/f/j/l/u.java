package b.f.j.l;

import b.f.d.d.m;
import b.f.d.g.g;
import b.f.d.g.i;
import b.f.d.g.j;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.imagepipeline.memory.MemoryPooledByteBufferOutputStream;
import java.io.IOException;
import java.io.InputStream;
/* compiled from: MemoryPooledByteBufferFactory.java */
/* loaded from: classes2.dex */
public class u implements g {
    public final j a;

    /* renamed from: b  reason: collision with root package name */
    public final s f595b;

    public u(s sVar, j jVar) {
        this.f595b = sVar;
        this.a = jVar;
    }

    @Override // b.f.d.g.g
    public i a() {
        s sVar = this.f595b;
        return new MemoryPooledByteBufferOutputStream(sVar, sVar.k[0]);
    }

    @Override // b.f.d.g.g
    public PooledByteBuffer b(byte[] bArr) {
        MemoryPooledByteBufferOutputStream memoryPooledByteBufferOutputStream = new MemoryPooledByteBufferOutputStream(this.f595b, bArr.length);
        try {
            try {
                memoryPooledByteBufferOutputStream.write(bArr, 0, bArr.length);
                return memoryPooledByteBufferOutputStream.b();
            } catch (IOException e) {
                m.a(e);
                throw new RuntimeException(e);
            }
        } finally {
            memoryPooledByteBufferOutputStream.close();
        }
    }

    @Override // b.f.d.g.g
    public PooledByteBuffer c(InputStream inputStream) throws IOException {
        s sVar = this.f595b;
        MemoryPooledByteBufferOutputStream memoryPooledByteBufferOutputStream = new MemoryPooledByteBufferOutputStream(sVar, sVar.k[0]);
        try {
            this.a.a(inputStream, memoryPooledByteBufferOutputStream);
            return memoryPooledByteBufferOutputStream.b();
        } finally {
            memoryPooledByteBufferOutputStream.close();
        }
    }

    @Override // b.f.d.g.g
    public PooledByteBuffer d(InputStream inputStream, int i) throws IOException {
        MemoryPooledByteBufferOutputStream memoryPooledByteBufferOutputStream = new MemoryPooledByteBufferOutputStream(this.f595b, i);
        try {
            this.a.a(inputStream, memoryPooledByteBufferOutputStream);
            return memoryPooledByteBufferOutputStream.b();
        } finally {
            memoryPooledByteBufferOutputStream.close();
        }
    }

    @Override // b.f.d.g.g
    public i e(int i) {
        return new MemoryPooledByteBufferOutputStream(this.f595b, i);
    }
}
