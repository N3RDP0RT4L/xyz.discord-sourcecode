package b.f.j.l;

import android.util.SparseArray;
import androidx.annotation.VisibleForTesting;
import java.util.LinkedList;
/* compiled from: BucketMap.java */
/* loaded from: classes2.dex */
public class g<T> {
    public final SparseArray<b<T>> a = new SparseArray<>();
    @VisibleForTesting

    /* renamed from: b  reason: collision with root package name */
    public b<T> f591b;
    @VisibleForTesting
    public b<T> c;

    /* compiled from: BucketMap.java */
    @VisibleForTesting
    /* loaded from: classes2.dex */
    public static class b<I> {

        /* renamed from: b  reason: collision with root package name */
        public int f592b;
        public LinkedList<I> c;
        public b<I> a = null;
        public b<I> d = null;

        public b(b bVar, int i, LinkedList linkedList, b bVar2, a aVar) {
            this.f592b = i;
            this.c = linkedList;
        }

        public String toString() {
            return b.d.b.a.a.A(b.d.b.a.a.R("LinkedEntry(key: "), this.f592b, ")");
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final void a(b<T> bVar) {
        if (this.f591b != bVar) {
            b(bVar);
            b bVar2 = (b<T>) this.f591b;
            if (bVar2 == null) {
                this.f591b = bVar;
                this.c = bVar;
                return;
            }
            bVar.d = bVar2;
            bVar2.a = bVar;
            this.f591b = bVar;
        }
    }

    public final synchronized void b(b<T> bVar) {
        b bVar2 = (b<T>) bVar.a;
        b bVar3 = (b<T>) bVar.d;
        if (bVar2 != null) {
            bVar2.d = bVar3;
        }
        if (bVar3 != null) {
            bVar3.a = bVar2;
        }
        bVar.a = null;
        bVar.d = null;
        if (bVar == this.f591b) {
            this.f591b = bVar3;
        }
        if (bVar == this.c) {
            this.c = bVar2;
        }
    }
}
