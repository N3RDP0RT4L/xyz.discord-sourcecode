package b.f.j.l;

import android.graphics.Bitmap;
import b.c.a.a0.d;
import b.f.d.h.f;
/* compiled from: BitmapCounter.java */
/* loaded from: classes2.dex */
public class b {
    public int a;

    /* renamed from: b  reason: collision with root package name */
    public long f588b;
    public final int c;
    public final int d;
    public final f<Bitmap> e;

    /* compiled from: BitmapCounter.java */
    /* loaded from: classes2.dex */
    public class a implements f<Bitmap> {
        public a() {
        }

        @Override // b.f.d.h.f
        public void release(Bitmap bitmap) {
            Bitmap bitmap2 = bitmap;
            try {
                b.this.a(bitmap2);
            } finally {
                bitmap2.recycle();
            }
        }
    }

    public b(int i, int i2) {
        boolean z2 = true;
        d.i(Boolean.valueOf(i > 0));
        d.i(Boolean.valueOf(i2 <= 0 ? false : z2));
        this.c = i;
        this.d = i2;
        this.e = new a();
    }

    public synchronized void a(Bitmap bitmap) {
        int d = b.f.k.a.d(bitmap);
        d.k(this.a > 0, "No bitmaps registered.");
        long j = d;
        boolean z2 = j <= this.f588b;
        Object[] objArr = {Integer.valueOf(d), Long.valueOf(this.f588b)};
        if (z2) {
            this.f588b -= j;
            this.a--;
        } else {
            throw new IllegalArgumentException(d.k0("Bitmap size bigger than the total registered size: %d, %d", objArr));
        }
    }

    public synchronized int b() {
        return this.d;
    }
}
