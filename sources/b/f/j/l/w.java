package b.f.j.l;

import android.util.SparseIntArray;
import b.f.d.g.c;
import b.f.d.g.d;
/* compiled from: PoolConfig.java */
/* loaded from: classes2.dex */
public class w {
    public final y a = j.a();

    /* renamed from: b  reason: collision with root package name */
    public final z f596b = v.h();
    public final y c;
    public final c d;
    public final y e;
    public final z f;
    public final y g;
    public final z h;
    public final String i;
    public final int j;

    /* compiled from: PoolConfig.java */
    /* loaded from: classes2.dex */
    public static class b {
        public b(a aVar) {
        }
    }

    public w(b bVar, a aVar) {
        int i;
        b.f.j.r.b.b();
        int i2 = k.a;
        int i3 = i2 * 4194304;
        SparseIntArray sparseIntArray = new SparseIntArray();
        for (int i4 = 131072; i4 <= 4194304; i4 *= 2) {
            sparseIntArray.put(i4, i2);
        }
        this.c = new y(4194304, i3, sparseIntArray, 131072, 4194304, k.a);
        this.d = d.b();
        SparseIntArray sparseIntArray2 = new SparseIntArray();
        sparseIntArray2.put(1024, 5);
        sparseIntArray2.put(2048, 5);
        sparseIntArray2.put(4096, 5);
        sparseIntArray2.put(8192, 5);
        sparseIntArray2.put(16384, 5);
        sparseIntArray2.put(32768, 5);
        sparseIntArray2.put(65536, 5);
        sparseIntArray2.put(131072, 5);
        sparseIntArray2.put(262144, 2);
        sparseIntArray2.put(524288, 2);
        sparseIntArray2.put(1048576, 2);
        int min = (int) Math.min(Runtime.getRuntime().maxMemory(), 2147483647L);
        int i5 = min < 16777216 ? 3145728 : min < 33554432 ? 6291456 : 12582912;
        int min2 = (int) Math.min(Runtime.getRuntime().maxMemory(), 2147483647L);
        if (min2 < 16777216) {
            i = min2 / 2;
        } else {
            i = (min2 / 4) * 3;
        }
        this.e = new y(i5, i, sparseIntArray2);
        this.f = v.h();
        SparseIntArray sparseIntArray3 = new SparseIntArray();
        sparseIntArray3.put(16384, 5);
        this.g = new y(81920, 1048576, sparseIntArray3);
        this.h = v.h();
        this.i = "legacy";
        this.j = 4194304;
        b.f.j.r.b.b();
    }
}
