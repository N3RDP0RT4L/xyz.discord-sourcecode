package b.f.j.l;

import android.util.Log;
import androidx.annotation.VisibleForTesting;
import b.c.a.a0.d;
import b.f.d.e.a;
import java.util.LinkedList;
import java.util.Queue;
/* compiled from: Bucket.java */
@VisibleForTesting
/* loaded from: classes2.dex */
public class f<V> {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final int f590b;
    public final Queue c;
    public final boolean d;
    public int e;

    public f(int i, int i2, int i3, boolean z2) {
        boolean z3 = true;
        d.B(i > 0);
        d.B(i2 >= 0);
        d.B(i3 < 0 ? false : z3);
        this.a = i;
        this.f590b = i2;
        this.c = new LinkedList();
        this.e = i3;
        this.d = z2;
    }

    public void a(V v) {
        this.c.add(v);
    }

    public void b() {
        d.B(this.e > 0);
        this.e--;
    }

    public V c() {
        return (V) this.c.poll();
    }

    public void d(V v) {
        boolean z2 = false;
        if (this.d) {
            if (this.e > 0) {
                z2 = true;
            }
            d.B(z2);
            this.e--;
            a(v);
            return;
        }
        int i = this.e;
        if (i > 0) {
            this.e = i - 1;
            a(v);
            return;
        }
        Object[] objArr = {v};
        int i2 = a.a;
        Log.println(6, "unknown:BUCKET", a.g("Tried to release value %s from an empty bucket!", objArr));
    }
}
