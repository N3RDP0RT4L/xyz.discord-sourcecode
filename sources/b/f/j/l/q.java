package b.f.j.l;

import b.f.j.l.g;
import java.util.HashSet;
import java.util.Set;
/* compiled from: LruBucketsPoolBackend.java */
/* loaded from: classes2.dex */
public abstract class q<T> {
    public final Set<T> a = new HashSet();

    /* renamed from: b  reason: collision with root package name */
    public final g<T> f594b = new g<>();

    public abstract T a(int i);

    public abstract int b(T t);

    public T c() {
        T t;
        g<T> gVar = this.f594b;
        synchronized (gVar) {
            g.b<T> bVar = gVar.c;
            if (bVar == null) {
                t = null;
            } else {
                t = bVar.c.pollLast();
                if (bVar.c.isEmpty()) {
                    gVar.b(bVar);
                    gVar.a.remove(bVar.f592b);
                }
            }
        }
        if (t != null) {
            synchronized (this) {
                this.a.remove(t);
            }
        }
        return t;
    }
}
