package b.f.j.l;

import android.util.SparseIntArray;
import b.c.a.a0.d;
/* compiled from: PoolParams.java */
/* loaded from: classes2.dex */
public class y {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final int f598b;
    public final SparseIntArray c;
    public final int d;

    public y(int i, int i2, SparseIntArray sparseIntArray) {
        this(i, i2, sparseIntArray, 0, Integer.MAX_VALUE, -1);
    }

    public y(int i, int i2, SparseIntArray sparseIntArray, int i3, int i4, int i5) {
        d.B(i >= 0 && i2 >= i);
        this.f598b = i;
        this.a = i2;
        this.c = sparseIntArray;
        this.d = i5;
    }
}
