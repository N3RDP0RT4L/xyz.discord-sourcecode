package b.f.j.l;

import android.graphics.Bitmap;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Objects;
import java.util.Set;
/* compiled from: DummyTrackingInUseBitmapPool.java */
/* loaded from: classes2.dex */
public class m implements d {
    public final Set<Bitmap> a = Collections.newSetFromMap(new IdentityHashMap());

    @Override // b.f.d.g.e
    public Bitmap get(int i) {
        Bitmap createBitmap = Bitmap.createBitmap(1, (int) Math.ceil(i / 2.0d), Bitmap.Config.RGB_565);
        this.a.add(createBitmap);
        return createBitmap;
    }

    @Override // b.f.d.g.e, b.f.d.h.f
    public void release(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        Objects.requireNonNull(bitmap);
        this.a.remove(bitmap);
        bitmap.recycle();
    }
}
