package b.f.j.l;

import android.graphics.Bitmap;
import b.f.d.g.c;
import b.f.j.l.g;
import java.util.LinkedList;
import java.util.Objects;
/* compiled from: LruBitmapPool.java */
/* loaded from: classes2.dex */
public class p implements d {
    public final q<Bitmap> a = new e();

    /* renamed from: b  reason: collision with root package name */
    public final int f593b;
    public int c;
    public final z d;
    public int e;

    public p(int i, int i2, z zVar, c cVar) {
        this.f593b = i;
        this.c = i2;
        this.d = zVar;
    }

    @Override // b.f.d.g.e
    public Bitmap get(int i) {
        Bitmap a;
        Bitmap c;
        synchronized (this) {
            int i2 = this.e;
            int i3 = this.f593b;
            if (i2 > i3) {
                synchronized (this) {
                    while (this.e > i3 && (c = this.a.c()) != null) {
                        int b2 = this.a.b(c);
                        this.e -= b2;
                        this.d.e(b2);
                    }
                }
            }
            a = this.a.a(i);
            if (a != null) {
                int b3 = this.a.b(a);
                this.e -= b3;
                this.d.b(b3);
            } else {
                this.d.a(i);
                a = Bitmap.createBitmap(1, i, Bitmap.Config.ALPHA_8);
            }
        }
        return a;
    }

    @Override // b.f.d.g.e, b.f.d.h.f
    public void release(Object obj) {
        boolean add;
        Bitmap bitmap = (Bitmap) obj;
        int b2 = this.a.b(bitmap);
        if (b2 <= this.c) {
            this.d.g(b2);
            e eVar = (e) this.a;
            Objects.requireNonNull(eVar);
            if (eVar.d(bitmap)) {
                synchronized (eVar) {
                    add = eVar.a.add(bitmap);
                }
                if (add) {
                    g<T> gVar = eVar.f594b;
                    int b3 = eVar.b(bitmap);
                    synchronized (gVar) {
                        g.b bVar = (g.b) gVar.a.get(b3);
                        if (bVar == null) {
                            g.b bVar2 = new g.b(null, b3, new LinkedList(), null, null);
                            gVar.a.put(b3, bVar2);
                            bVar = bVar2;
                        }
                        bVar.c.addLast(bitmap);
                        gVar.a(bVar);
                    }
                }
            }
            synchronized (this) {
                this.e += b2;
            }
        }
    }
}
