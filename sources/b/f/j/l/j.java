package b.f.j.l;

import android.util.SparseIntArray;
/* compiled from: DefaultBitmapPoolParams.java */
/* loaded from: classes2.dex */
public class j {
    public static final SparseIntArray a = new SparseIntArray(0);

    public static y a() {
        int i;
        int min = (int) Math.min(Runtime.getRuntime().maxMemory(), 2147483647L);
        if (min > 16777216) {
            i = (min / 4) * 3;
        } else {
            i = min / 2;
        }
        return new y(0, i, a);
    }
}
