package b.f.j.l;

import android.util.SparseIntArray;
import b.f.d.g.c;
import com.facebook.imagepipeline.memory.BasePool;
import java.util.Objects;
/* compiled from: MemoryChunkPool.java */
/* loaded from: classes2.dex */
public abstract class s extends BasePool<r> {
    public final int[] k;

    public s(c cVar, y yVar, z zVar) {
        super(cVar, yVar, zVar);
        SparseIntArray sparseIntArray = yVar.c;
        Objects.requireNonNull(sparseIntArray);
        this.k = new int[sparseIntArray.size()];
        int i = 0;
        while (true) {
            int[] iArr = this.k;
            if (i < iArr.length) {
                iArr[i] = sparseIntArray.keyAt(i);
                i++;
            } else {
                m();
                return;
            }
        }
    }

    @Override // com.facebook.imagepipeline.memory.BasePool
    public void g(r rVar) {
        rVar.close();
    }

    @Override // com.facebook.imagepipeline.memory.BasePool
    public int i(int i) {
        int[] iArr;
        if (i > 0) {
            for (int i2 : this.k) {
                if (i2 >= i) {
                    return i2;
                }
            }
            return i;
        }
        throw new BasePool.InvalidSizeException(Integer.valueOf(i));
    }

    @Override // com.facebook.imagepipeline.memory.BasePool
    public int j(r rVar) {
        return rVar.getSize();
    }

    @Override // com.facebook.imagepipeline.memory.BasePool
    public int k(int i) {
        return i;
    }

    @Override // com.facebook.imagepipeline.memory.BasePool
    public boolean o(r rVar) {
        return !rVar.isClosed();
    }

    /* renamed from: s */
    public abstract r e(int i);
}
