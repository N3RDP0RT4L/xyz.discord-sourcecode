package b.f.j.l;

import b.c.a.a0.d;
import b.f.d.g.a;
import b.f.d.g.c;
import b.f.d.g.g;
import b.f.d.g.j;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Objects;
/* compiled from: PoolFactory.java */
/* loaded from: classes2.dex */
public class x {
    public final w a;

    /* renamed from: b  reason: collision with root package name */
    public s f597b;
    public d c;
    public s d;
    public s e;
    public g f;
    public j g;
    public a h;

    public x(w wVar) {
        this.a = wVar;
    }

    public d a() {
        if (this.c == null) {
            String str = this.a.i;
            char c = 65535;
            switch (str.hashCode()) {
                case -1868884870:
                    if (str.equals("legacy_default_params")) {
                        c = 3;
                        break;
                    }
                    break;
                case -1106578487:
                    if (str.equals("legacy")) {
                        c = 4;
                        break;
                    }
                    break;
                case -404562712:
                    if (str.equals("experimental")) {
                        c = 2;
                        break;
                    }
                    break;
                case -402149703:
                    if (str.equals("dummy_with_tracking")) {
                        c = 1;
                        break;
                    }
                    break;
                case 95945896:
                    if (str.equals("dummy")) {
                        c = 0;
                        break;
                    }
                    break;
            }
            if (c == 0) {
                this.c = new l();
            } else if (c == 1) {
                this.c = new m();
            } else if (c == 2) {
                Objects.requireNonNull(this.a);
                int i = this.a.j;
                v h = v.h();
                Objects.requireNonNull(this.a);
                this.c = new p(0, i, h, null);
            } else if (c != 3) {
                w wVar = this.a;
                this.c = new h(wVar.d, wVar.a, wVar.f596b, false);
            } else {
                this.c = new h(this.a.d, j.a(), this.a.f596b, false);
            }
        }
        return this.c;
    }

    public int b() {
        return this.a.c.d;
    }

    public g c(int i) {
        s sVar;
        if (this.f == null) {
            if (i == 0) {
                if (this.e == null) {
                    try {
                        Constructor<?> constructor = Class.forName("com.facebook.imagepipeline.memory.NativeMemoryChunkPool").getConstructor(c.class, y.class, z.class);
                        w wVar = this.a;
                        this.e = (s) constructor.newInstance(wVar.d, wVar.e, wVar.f);
                    } catch (ClassNotFoundException e) {
                        b.f.d.e.a.f("PoolFactory", "", e);
                        this.e = null;
                    } catch (IllegalAccessException e2) {
                        b.f.d.e.a.f("PoolFactory", "", e2);
                        this.e = null;
                    } catch (InstantiationException e3) {
                        b.f.d.e.a.f("PoolFactory", "", e3);
                        this.e = null;
                    } catch (NoSuchMethodException e4) {
                        b.f.d.e.a.f("PoolFactory", "", e4);
                        this.e = null;
                    } catch (InvocationTargetException e5) {
                        b.f.d.e.a.f("PoolFactory", "", e5);
                        this.e = null;
                    }
                }
                sVar = this.e;
            } else if (i == 1) {
                if (this.d == null) {
                    try {
                        Constructor<?> constructor2 = Class.forName("com.facebook.imagepipeline.memory.BufferMemoryChunkPool").getConstructor(c.class, y.class, z.class);
                        w wVar2 = this.a;
                        this.d = (s) constructor2.newInstance(wVar2.d, wVar2.e, wVar2.f);
                    } catch (ClassNotFoundException unused) {
                        this.d = null;
                    } catch (IllegalAccessException unused2) {
                        this.d = null;
                    } catch (InstantiationException unused3) {
                        this.d = null;
                    } catch (NoSuchMethodException unused4) {
                        this.d = null;
                    } catch (InvocationTargetException unused5) {
                        this.d = null;
                    }
                }
                sVar = this.d;
            } else if (i == 2) {
                if (this.f597b == null) {
                    try {
                        Constructor<?> constructor3 = Class.forName("com.facebook.imagepipeline.memory.AshmemMemoryChunkPool").getConstructor(c.class, y.class, z.class);
                        w wVar3 = this.a;
                        this.f597b = (s) constructor3.newInstance(wVar3.d, wVar3.e, wVar3.f);
                    } catch (ClassNotFoundException unused6) {
                        this.f597b = null;
                    } catch (IllegalAccessException unused7) {
                        this.f597b = null;
                    } catch (InstantiationException unused8) {
                        this.f597b = null;
                    } catch (NoSuchMethodException unused9) {
                        this.f597b = null;
                    } catch (InvocationTargetException unused10) {
                        this.f597b = null;
                    }
                }
                sVar = this.f597b;
            } else {
                throw new IllegalArgumentException("Invalid MemoryChunkType");
            }
            d.y(sVar, "failed to get pool for chunk type: " + i);
            this.f = new u(sVar, d());
        }
        return this.f;
    }

    public j d() {
        if (this.g == null) {
            this.g = new j(e());
        }
        return this.g;
    }

    public a e() {
        if (this.h == null) {
            w wVar = this.a;
            this.h = new o(wVar.d, wVar.g, wVar.h);
        }
        return this.h;
    }
}
