package b.f.j.l;

import android.graphics.Bitmap;
import b.f.j.l.g;
import b.f.k.a;
/* compiled from: BitmapPoolBackend.java */
/* loaded from: classes2.dex */
public class e extends q<Bitmap> {
    @Override // b.f.j.l.q
    public Bitmap a(int i) {
        Object obj;
        g<T> gVar = this.f594b;
        synchronized (gVar) {
            g.b bVar = (g.b) gVar.a.get(i);
            if (bVar == null) {
                obj = null;
            } else {
                obj = bVar.c.pollFirst();
                gVar.a(bVar);
            }
        }
        if (obj != null) {
            synchronized (this) {
                this.a.remove(obj);
            }
        }
        Bitmap bitmap = (Bitmap) obj;
        if (bitmap == null || !d(bitmap)) {
            return null;
        }
        bitmap.eraseColor(0);
        return bitmap;
    }

    @Override // b.f.j.l.q
    public int b(Bitmap bitmap) {
        return a.d(bitmap);
    }

    public boolean d(Bitmap bitmap) {
        if (bitmap == null) {
            return false;
        }
        if (bitmap.isRecycled()) {
            b.f.d.e.a.p("BitmapPoolBackend", "Cannot reuse a recycled bitmap: %s", bitmap);
            return false;
        } else if (bitmap.isMutable()) {
            return true;
        } else {
            b.f.d.e.a.p("BitmapPoolBackend", "Cannot reuse an immutable bitmap: %s", bitmap);
            return false;
        }
    }
}
