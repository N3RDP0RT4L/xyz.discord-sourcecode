package b.f.j.l;

import com.discord.api.permission.Permission;
/* compiled from: BitmapCounterProvider.java */
/* loaded from: classes2.dex */
public class c {
    public static final int a;

    /* renamed from: b  reason: collision with root package name */
    public static int f589b;
    public static volatile b c;

    static {
        int i;
        int min = (int) Math.min(Runtime.getRuntime().maxMemory(), 2147483647L);
        if (min > Permission.MOVE_MEMBERS) {
            i = (min / 4) * 3;
        } else {
            i = min / 2;
        }
        a = i;
        f589b = 384;
    }
}
