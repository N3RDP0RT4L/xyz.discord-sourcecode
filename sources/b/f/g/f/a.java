package b.f.g.f;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import b.f.g.e.s;
import b.f.g.e.t;
import com.facebook.drawee.drawable.ScalingUtils$ScaleType;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
/* compiled from: GenericDraweeHierarchyBuilder.java */
/* loaded from: classes2.dex */
public class a {
    public static final ScalingUtils$ScaleType a = t.l;

    /* renamed from: b  reason: collision with root package name */
    public static final ScalingUtils$ScaleType f518b = s.l;
    public Resources c;
    public ScalingUtils$ScaleType g;
    public ScalingUtils$ScaleType i;
    public ScalingUtils$ScaleType k;
    public ScalingUtils$ScaleType m;
    public int d = 300;
    public float e = 0.0f;
    public Drawable f = null;
    public Drawable h = null;
    public Drawable j = null;
    public Drawable l = null;
    public ScalingUtils$ScaleType n = f518b;
    public Drawable o = null;
    public List<Drawable> p = null;
    public Drawable q = null;
    public c r = null;

    static {
        ScalingUtils$ScaleType scalingUtils$ScaleType = ScalingUtils$ScaleType.a;
    }

    public a(Resources resources) {
        this.c = resources;
        ScalingUtils$ScaleType scalingUtils$ScaleType = a;
        this.g = scalingUtils$ScaleType;
        this.i = scalingUtils$ScaleType;
        this.k = scalingUtils$ScaleType;
        this.m = scalingUtils$ScaleType;
    }

    public GenericDraweeHierarchy a() {
        List<Drawable> list = this.p;
        if (list != null) {
            for (Drawable drawable : list) {
                Objects.requireNonNull(drawable);
            }
        }
        return new GenericDraweeHierarchy(this);
    }

    public a b(Drawable drawable) {
        if (drawable == null) {
            this.p = null;
        } else {
            this.p = Arrays.asList(drawable);
        }
        return this;
    }
}
