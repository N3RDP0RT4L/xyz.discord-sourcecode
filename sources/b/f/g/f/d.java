package b.f.g.f;

import android.content.res.Resources;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import b.f.d.e.a;
import b.f.g.e.g;
import b.f.g.e.j;
import b.f.g.e.k;
import b.f.g.e.l;
import b.f.g.e.m;
import b.f.g.e.o;
import b.f.g.e.p;
import b.f.j.r.b;
import com.facebook.drawee.drawable.ScalingUtils$ScaleType;
/* compiled from: WrappingUtils.java */
/* loaded from: classes2.dex */
public class d {
    public static final Drawable a = new ColorDrawable(0);

    public static Drawable a(Drawable drawable, c cVar, Resources resources) {
        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            k kVar = new k(resources, bitmapDrawable.getBitmap(), bitmapDrawable.getPaint());
            b(kVar, cVar);
            return kVar;
        } else if (drawable instanceof NinePatchDrawable) {
            o oVar = new o((NinePatchDrawable) drawable);
            b(oVar, cVar);
            return oVar;
        } else if (drawable instanceof ColorDrawable) {
            l lVar = new l(((ColorDrawable) drawable).getColor());
            b(lVar, cVar);
            return lVar;
        } else {
            a.o("WrappingUtils", "Don't know how to round that drawable: %s", drawable);
            return drawable;
        }
    }

    public static void b(j jVar, c cVar) {
        jVar.c(cVar.f519b);
        jVar.m(cVar.c);
        jVar.a(cVar.f, cVar.e);
        jVar.i(cVar.g);
        jVar.f(false);
        jVar.e(cVar.h);
    }

    public static Drawable c(Drawable drawable, c cVar, Resources resources) {
        try {
            b.b();
            if (!(drawable == null || cVar == null || cVar.a != 2)) {
                if (!(drawable instanceof g)) {
                    return a(drawable, cVar, resources);
                }
                b.f.g.e.d dVar = (g) drawable;
                while (true) {
                    Drawable l = dVar.l();
                    if (l == dVar || !(l instanceof b.f.g.e.d)) {
                        break;
                    }
                    dVar = (b.f.g.e.d) l;
                }
                dVar.g(a(dVar.g(a), cVar, resources));
                return drawable;
            }
            return drawable;
        } finally {
            b.b();
        }
    }

    public static Drawable d(Drawable drawable, c cVar) {
        try {
            b.b();
            if (!(drawable == null || cVar == null || cVar.a != 1)) {
                m mVar = new m(drawable);
                b(mVar, cVar);
                mVar.f510x = cVar.d;
                mVar.invalidateSelf();
                return mVar;
            }
            return drawable;
        } finally {
            b.b();
        }
    }

    public static Drawable e(Drawable drawable, ScalingUtils$ScaleType scalingUtils$ScaleType, PointF pointF) {
        b.b();
        if (drawable == null || scalingUtils$ScaleType == null) {
            b.b();
            return drawable;
        }
        p pVar = new p(drawable, scalingUtils$ScaleType);
        if (pointF != null && !b.c.a.a0.d.g0(pVar.p, pointF)) {
            if (pVar.p == null) {
                pVar.p = new PointF();
            }
            pVar.p.set(pointF);
            pVar.p();
            pVar.invalidateSelf();
        }
        b.b();
        return pVar;
    }
}
