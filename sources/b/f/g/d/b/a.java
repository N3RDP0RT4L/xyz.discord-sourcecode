package b.f.g.d.b;

import android.graphics.drawable.Animatable;
import b.f.g.c.c;
/* compiled from: ImageLoadingTimeControllerListener.java */
/* loaded from: classes2.dex */
public class a extends c {
    public long a = -1;

    /* renamed from: b  reason: collision with root package name */
    public long f501b = -1;
    public b c;

    public a(b bVar) {
        this.c = bVar;
    }

    @Override // b.f.g.c.c, com.facebook.drawee.controller.ControllerListener
    public void onFinalImageSet(String str, Object obj, Animatable animatable) {
        long currentTimeMillis = System.currentTimeMillis();
        this.f501b = currentTimeMillis;
        b bVar = this.c;
        if (bVar != null) {
            b.f.g.d.a aVar = (b.f.g.d.a) bVar;
            aVar.B = currentTimeMillis - this.a;
            aVar.invalidateSelf();
        }
    }

    @Override // b.f.g.c.c, com.facebook.drawee.controller.ControllerListener
    public void onSubmit(String str, Object obj) {
        this.a = System.currentTimeMillis();
    }
}
