package b.f.g.d;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import b.f.g.d.b.b;
import com.facebook.drawee.drawable.ScalingUtils$ScaleType;
import java.util.HashMap;
import java.util.Locale;
/* compiled from: DebugControllerOverlayDrawable.java */
/* loaded from: classes2.dex */
public class a extends Drawable implements b {
    public int A;
    public long B;
    public String C;
    public String j;
    public int k;
    public int l;
    public int m;
    public ScalingUtils$ScaleType n;
    public int p;
    public int q;
    public int w;

    /* renamed from: x  reason: collision with root package name */
    public int f498x;

    /* renamed from: y  reason: collision with root package name */
    public int f499y;

    /* renamed from: z  reason: collision with root package name */
    public int f500z;
    public HashMap<String, String> o = new HashMap<>();
    public int r = 80;

    /* renamed from: s  reason: collision with root package name */
    public final Paint f497s = new Paint(1);
    public final Matrix t = new Matrix();
    public final Rect u = new Rect();
    public final RectF v = new RectF();
    public int D = -1;

    public a() {
        c();
    }

    public static String b(String str, Object... objArr) {
        return String.format(Locale.US, str, objArr);
    }

    public final void a(Canvas canvas, String str, String str2, int i) {
        String v = b.d.b.a.a.v(str, ": ");
        float measureText = this.f497s.measureText(v);
        float measureText2 = this.f497s.measureText(str2);
        this.f497s.setColor(1711276032);
        int i2 = this.f500z;
        int i3 = this.A;
        canvas.drawRect(i2 - 4, i3 + 8, i2 + measureText + measureText2 + 4.0f, i3 + this.f499y + 8, this.f497s);
        this.f497s.setColor(-1);
        canvas.drawText(v, this.f500z, this.A, this.f497s);
        this.f497s.setColor(i);
        canvas.drawText(str2, this.f500z + measureText, this.A, this.f497s);
        this.A += this.f499y;
    }

    public void c() {
        this.k = -1;
        this.l = -1;
        this.m = -1;
        this.o = new HashMap<>();
        this.p = -1;
        this.q = -1;
        this.j = "none";
        invalidateSelf();
        this.B = -1L;
        this.C = null;
        this.D = -1;
        invalidateSelf();
    }

    /* JADX WARN: Removed duplicated region for block: B:23:0x0175  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x0193  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x0198  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x01a9  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x01c2  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x01d9 A[LOOP:0: B:35:0x01d3->B:37:0x01d9, LOOP_END] */
    @Override // android.graphics.drawable.Drawable
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void draw(android.graphics.Canvas r20) {
        /*
            Method dump skipped, instructions count: 497
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.f.g.d.a.draw(android.graphics.Canvas):void");
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    @Override // android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        int min = Math.min(40, Math.max(10, Math.min(rect.width() / 8, rect.height() / 9)));
        this.f497s.setTextSize(min);
        int i = min + 8;
        this.f499y = i;
        int i2 = this.r;
        if (i2 == 80) {
            this.f499y = i * (-1);
        }
        this.w = rect.left + 10;
        this.f498x = i2 == 80 ? rect.bottom - 10 : rect.top + 10 + 10;
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
    }
}
