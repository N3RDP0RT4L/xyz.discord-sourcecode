package b.f.g.b;

import android.os.Handler;
import android.os.Looper;
import androidx.annotation.AnyThread;
import androidx.annotation.MainThread;
import b.f.g.b.a;
import java.util.ArrayList;
/* compiled from: DeferredReleaserConcurrentImpl.java */
/* loaded from: classes2.dex */
public class b extends b.f.g.b.a {

    /* renamed from: b  reason: collision with root package name */
    public final Object f489b = new Object();
    public final Runnable f = new a();
    public ArrayList<a.AbstractC0067a> d = new ArrayList<>();
    public ArrayList<a.AbstractC0067a> e = new ArrayList<>();
    public final Handler c = new Handler(Looper.getMainLooper());

    /* compiled from: DeferredReleaserConcurrentImpl.java */
    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        @MainThread
        public void run() {
            ArrayList<a.AbstractC0067a> arrayList;
            synchronized (b.this.f489b) {
                b bVar = b.this;
                ArrayList<a.AbstractC0067a> arrayList2 = bVar.e;
                arrayList = bVar.d;
                bVar.e = arrayList;
                bVar.d = arrayList2;
            }
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                b.this.e.get(i).release();
            }
            b.this.e.clear();
        }
    }

    @Override // b.f.g.b.a
    @AnyThread
    public void a(a.AbstractC0067a aVar) {
        synchronized (this.f489b) {
            this.d.remove(aVar);
        }
    }
}
