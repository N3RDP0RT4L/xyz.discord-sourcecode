package b.f.g.a.a.h;

import b.f.j.k.a;
/* compiled from: ImageOriginRequestListener.java */
/* loaded from: classes2.dex */
public class c extends a {
    public String a;

    /* renamed from: b  reason: collision with root package name */
    public final b f479b;

    public c(String str, b bVar) {
        this.f479b = bVar;
        this.a = str;
    }

    @Override // b.f.j.k.a, b.f.j.p.a1
    public void e(String str, String str2, boolean z2) {
        b bVar = this.f479b;
        if (bVar != null) {
            String str3 = this.a;
            str2.hashCode();
            char c = 65535;
            int i = 1;
            switch (str2.hashCode()) {
                case -1917159454:
                    if (str2.equals("QualifiedResourceFetchProducer")) {
                        c = 0;
                        break;
                    }
                    break;
                case -1914072202:
                    if (str2.equals("BitmapMemoryCacheGetProducer")) {
                        c = 1;
                        break;
                    }
                    break;
                case -1683996557:
                    if (str2.equals("LocalResourceFetchProducer")) {
                        c = 2;
                        break;
                    }
                    break;
                case -1579985851:
                    if (str2.equals("LocalFileFetchProducer")) {
                        c = 3;
                        break;
                    }
                    break;
                case -1307634203:
                    if (str2.equals("EncodedMemoryCacheProducer")) {
                        c = 4;
                        break;
                    }
                    break;
                case -1224383234:
                    if (str2.equals("NetworkFetchProducer")) {
                        c = 5;
                        break;
                    }
                    break;
                case 473552259:
                    if (str2.equals("VideoThumbnailProducer")) {
                        c = 6;
                        break;
                    }
                    break;
                case 656304759:
                    if (str2.equals("DiskCacheProducer")) {
                        c = 7;
                        break;
                    }
                    break;
                case 957714404:
                    if (str2.equals("BitmapMemoryCacheProducer")) {
                        c = '\b';
                        break;
                    }
                    break;
                case 1019542023:
                    if (str2.equals("LocalAssetFetchProducer")) {
                        c = '\t';
                        break;
                    }
                    break;
                case 1023071510:
                    if (str2.equals("PostprocessedBitmapMemoryCacheProducer")) {
                        c = '\n';
                        break;
                    }
                    break;
                case 1721672898:
                    if (str2.equals("DataFetchProducer")) {
                        c = 11;
                        break;
                    }
                    break;
                case 1793127518:
                    if (str2.equals("LocalContentUriThumbnailFetchProducer")) {
                        c = '\f';
                        break;
                    }
                    break;
                case 2109593398:
                    if (str2.equals("PartialDiskCacheProducer")) {
                        c = '\r';
                        break;
                    }
                    break;
                case 2113652014:
                    if (str2.equals("LocalContentUriFetchProducer")) {
                        c = 14;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                case 2:
                case 3:
                case 6:
                case '\t':
                case 11:
                case '\f':
                case 14:
                    i = 7;
                    break;
                case 1:
                case '\b':
                case '\n':
                    i = 5;
                    break;
                case 4:
                    i = 4;
                    break;
                case 5:
                    i = 2;
                    break;
                case 7:
                case '\r':
                    i = 3;
                    break;
            }
            bVar.a(str3, i, z2, str2);
        }
    }
}
