package b.f.g.a.a.h;

import b.f.h.b.a.b;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.ImageRequest;
/* compiled from: ImagePerfState.java */
/* loaded from: classes2.dex */
public class h {
    public b.a A;
    public String a;

    /* renamed from: b  reason: collision with root package name */
    public String f482b;
    public ImageRequest c;
    public Object d;
    public ImageInfo e;
    public ImageRequest f;
    public ImageRequest g;
    public ImageRequest[] h;
    public String q;
    public boolean r;
    public Throwable u;
    public long i = -1;
    public long j = -1;
    public long k = -1;
    public long l = -1;
    public long m = -1;
    public long n = -1;
    public long o = -1;
    public int p = 1;

    /* renamed from: s  reason: collision with root package name */
    public int f483s = -1;
    public int t = -1;
    public int v = -1;
    public int w = -1;

    /* renamed from: x  reason: collision with root package name */
    public long f484x = -1;

    /* renamed from: y  reason: collision with root package name */
    public long f485y = -1;

    /* renamed from: z  reason: collision with root package name */
    public long f486z = -1;

    public void a() {
        this.f482b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.p = 1;
        this.q = null;
        this.r = false;
        this.f483s = -1;
        this.t = -1;
        this.u = null;
        this.v = -1;
        this.w = -1;
        this.A = null;
        b();
    }

    public void b() {
        this.n = -1L;
        this.o = -1L;
        this.i = -1L;
        this.k = -1L;
        this.l = -1L;
        this.m = -1L;
        this.f484x = -1L;
        this.f485y = -1L;
        this.f486z = -1L;
    }

    public d c() {
        return new d(this.a, this.f482b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o, this.p, this.q, this.r, this.f483s, this.t, this.u, this.w, this.f484x, this.f485y, null, this.f486z, this.A);
    }
}
