package b.f.g.a.a.h.i;

import b.f.d.k.b;
import b.f.g.a.a.h.h;
import b.f.j.k.a;
import com.facebook.imagepipeline.request.ImageRequest;
/* compiled from: ImagePerfRequestListener.java */
/* loaded from: classes2.dex */
public class c extends a {
    public final b a;

    /* renamed from: b  reason: collision with root package name */
    public final h f488b;

    public c(b bVar, h hVar) {
        this.a = bVar;
        this.f488b = hVar;
    }

    @Override // b.f.j.k.a, b.f.j.k.e
    public void a(ImageRequest imageRequest, Object obj, String str, boolean z2) {
        this.f488b.n = this.a.now();
        h hVar = this.f488b;
        hVar.c = imageRequest;
        hVar.d = obj;
        hVar.f482b = str;
        hVar.r = z2;
    }

    @Override // b.f.j.k.a, b.f.j.k.e
    public void c(ImageRequest imageRequest, String str, boolean z2) {
        this.f488b.o = this.a.now();
        h hVar = this.f488b;
        hVar.c = imageRequest;
        hVar.f482b = str;
        hVar.r = z2;
    }

    @Override // b.f.j.k.a, b.f.j.k.e
    public void g(ImageRequest imageRequest, String str, Throwable th, boolean z2) {
        this.f488b.o = this.a.now();
        h hVar = this.f488b;
        hVar.c = imageRequest;
        hVar.f482b = str;
        hVar.r = z2;
    }

    @Override // b.f.j.k.a, b.f.j.k.e
    public void k(String str) {
        this.f488b.o = this.a.now();
        this.f488b.f482b = str;
    }
}
