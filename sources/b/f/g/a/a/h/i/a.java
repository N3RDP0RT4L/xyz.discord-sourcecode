package b.f.g.a.a.h.i;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import androidx.annotation.NonNull;
import b.f.d.k.b;
import b.f.g.a.a.h.f;
import b.f.g.a.a.h.g;
import b.f.g.a.a.h.h;
import b.f.h.b.a.b;
import com.facebook.common.internal.Supplier;
import com.facebook.imagepipeline.image.ImageInfo;
import java.io.Closeable;
import java.util.Objects;
/* compiled from: ImagePerfControllerListener2.java */
/* loaded from: classes2.dex */
public class a extends b.f.h.b.a.a<ImageInfo> implements Closeable {
    public static Handler j;
    public final b k;
    public final h l;
    public final g m;
    public final Supplier<Boolean> n;
    public final Supplier<Boolean> o;

    /* compiled from: ImagePerfControllerListener2.java */
    /* renamed from: b.f.g.a.a.h.i.a$a  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public static class HandlerC0066a extends Handler {
        public final g a;

        public HandlerC0066a(@NonNull Looper looper, @NonNull g gVar) {
            super(looper);
            this.a = gVar;
        }

        @Override // android.os.Handler
        public void handleMessage(@NonNull Message message) {
            Object obj = message.obj;
            Objects.requireNonNull(obj);
            h hVar = (h) obj;
            int i = message.what;
            if (i == 1) {
                ((f) this.a).b(hVar, message.arg1);
            } else if (i == 2) {
                ((f) this.a).a(hVar, message.arg1);
            }
        }
    }

    public a(b bVar, h hVar, g gVar, Supplier<Boolean> supplier, Supplier<Boolean> supplier2) {
        this.k = bVar;
        this.l = hVar;
        this.m = gVar;
        this.n = supplier;
        this.o = supplier2;
    }

    @Override // b.f.h.b.a.b
    public void a(String str, Object obj, b.a aVar) {
        long now = this.k.now();
        h e = e();
        e.b();
        e.i = now;
        e.a = str;
        e.d = obj;
        e.A = aVar;
        n(e, 0);
        e.w = 1;
        e.f484x = now;
        q(e, 1);
    }

    @Override // b.f.h.b.a.b
    public void b(String str, Throwable th, b.a aVar) {
        long now = this.k.now();
        h e = e();
        e.A = aVar;
        e.l = now;
        e.a = str;
        e.u = th;
        n(e, 5);
        e.w = 2;
        e.f485y = now;
        q(e, 2);
    }

    @Override // b.f.h.b.a.b
    public void c(String str, b.a aVar) {
        long now = this.k.now();
        h e = e();
        e.A = aVar;
        e.a = str;
        int i = e.v;
        if (!(i == 3 || i == 5 || i == 6)) {
            e.m = now;
            n(e, 4);
        }
        e.w = 2;
        e.f485y = now;
        q(e, 2);
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        e().a();
    }

    @Override // b.f.h.b.a.b
    public void d(String str, Object obj, b.a aVar) {
        long now = this.k.now();
        h e = e();
        e.A = aVar;
        e.k = now;
        e.o = now;
        e.a = str;
        e.e = (ImageInfo) obj;
        n(e, 3);
    }

    public final h e() {
        return Boolean.FALSE.booleanValue() ? new h() : this.l;
    }

    public final boolean f() {
        boolean booleanValue = this.n.get().booleanValue();
        if (booleanValue && j == null) {
            synchronized (this) {
                if (j == null) {
                    HandlerThread handlerThread = new HandlerThread("ImagePerfControllerListener2Thread");
                    handlerThread.start();
                    Looper looper = handlerThread.getLooper();
                    Objects.requireNonNull(looper);
                    j = new HandlerC0066a(looper, this.m);
                }
            }
        }
        return booleanValue;
    }

    public final void n(h hVar, int i) {
        if (f()) {
            Handler handler = j;
            Objects.requireNonNull(handler);
            Message obtainMessage = handler.obtainMessage();
            obtainMessage.what = 1;
            obtainMessage.arg1 = i;
            obtainMessage.obj = hVar;
            j.sendMessage(obtainMessage);
            return;
        }
        ((f) this.m).b(hVar, i);
    }

    public final void q(h hVar, int i) {
        if (f()) {
            Handler handler = j;
            Objects.requireNonNull(handler);
            Message obtainMessage = handler.obtainMessage();
            obtainMessage.what = 2;
            obtainMessage.arg1 = i;
            obtainMessage.obj = hVar;
            j.sendMessage(obtainMessage);
            return;
        }
        ((f) this.m).a(hVar, i);
    }
}
