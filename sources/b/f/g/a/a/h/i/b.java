package b.f.g.a.a.h.i;

import b.f.g.a.a.h.f;
import b.f.g.a.a.h.h;
/* compiled from: ImagePerfImageOriginListener.java */
/* loaded from: classes2.dex */
public class b implements b.f.g.a.a.h.b {
    public final h a;

    /* renamed from: b  reason: collision with root package name */
    public final f f487b;

    public b(h hVar, f fVar) {
        this.a = hVar;
        this.f487b = fVar;
    }

    @Override // b.f.g.a.a.h.b
    public void a(String str, int i, boolean z2, String str2) {
        h hVar = this.a;
        hVar.p = i;
        hVar.q = str2;
        this.f487b.b(hVar, 1);
    }
}
