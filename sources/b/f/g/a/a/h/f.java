package b.f.g.a.a.h;

import android.graphics.Rect;
import b.f.d.d.l;
import b.f.d.k.b;
import b.f.g.a.a.c;
import b.f.g.a.a.h.i.a;
import com.facebook.common.internal.Supplier;
import java.util.List;
/* compiled from: ImagePerfMonitor.java */
/* loaded from: classes2.dex */
public class f implements g {
    public final c a;

    /* renamed from: b  reason: collision with root package name */
    public final b f481b;
    public final h c = new h();
    public final Supplier<Boolean> d;
    public c e;
    public b f;
    public b.f.g.a.a.h.i.c g;
    public a h;
    public b.f.j.k.c i;
    public List<e> j;
    public boolean k;

    public f(b bVar, c cVar, Supplier<Boolean> supplier) {
        this.f481b = bVar;
        this.a = cVar;
        this.d = supplier;
    }

    public void a(h hVar, int i) {
        List<e> list;
        if (!(!this.k || (list = this.j) == null || list.isEmpty())) {
            d c = hVar.c();
            for (e eVar : this.j) {
                eVar.b(c, i);
            }
        }
    }

    public void b(h hVar, int i) {
        List<e> list;
        b.f.g.h.a aVar;
        hVar.v = i;
        if (!(!this.k || (list = this.j) == null || list.isEmpty())) {
            if (!(i != 3 || (aVar = this.a.k) == null || aVar.e() == null)) {
                Rect bounds = aVar.e().getBounds();
                this.c.f483s = bounds.width();
                this.c.t = bounds.height();
            }
            d c = hVar.c();
            for (e eVar : this.j) {
                eVar.a(c, i);
            }
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public void c(boolean z2) {
        this.k = z2;
        if (z2) {
            if (this.h == null) {
                this.h = new a(this.f481b, this.c, this, this.d, l.a);
            }
            if (this.g == null) {
                this.g = new b.f.g.a.a.h.i.c(this.f481b, this.c);
            }
            if (this.f == null) {
                this.f = new b.f.g.a.a.h.i.b(this.c, this);
            }
            c cVar = this.e;
            if (cVar == null) {
                this.e = new c(this.a.m, this.f);
            } else {
                cVar.a = this.a.m;
            }
            if (this.i == null) {
                this.i = new b.f.j.k.c(this.g, this.e);
            }
            b bVar = this.f;
            if (bVar != null) {
                this.a.F(bVar);
            }
            a aVar = this.h;
            if (aVar != null) {
                b.f.h.b.a.c<INFO> cVar2 = this.a.j;
                synchronized (cVar2) {
                    cVar2.j.add(aVar);
                }
            }
            b.f.j.k.c cVar3 = this.i;
            if (cVar3 != null) {
                this.a.G(cVar3);
                return;
            }
            return;
        }
        b bVar2 = this.f;
        if (bVar2 != null) {
            c cVar4 = this.a;
            synchronized (cVar4) {
                b bVar3 = cVar4.G;
                if (bVar3 instanceof a) {
                    a aVar2 = (a) bVar3;
                    synchronized (aVar2) {
                        aVar2.a.remove(bVar2);
                    }
                } else if (bVar3 == bVar2) {
                    cVar4.G = null;
                }
            }
        }
        a aVar3 = this.h;
        if (aVar3 != null) {
            b.f.h.b.a.c<INFO> cVar5 = this.a.j;
            synchronized (cVar5) {
                int indexOf = cVar5.j.indexOf(aVar3);
                if (indexOf != -1) {
                    cVar5.j.remove(indexOf);
                }
            }
        }
        b.f.j.k.c cVar6 = this.i;
        if (cVar6 != null) {
            this.a.L(cVar6);
        }
    }
}
