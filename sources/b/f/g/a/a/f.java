package b.f.g.a.a;

import android.content.res.Resources;
import b.f.d.d.e;
import b.f.g.b.a;
import b.f.j.c.w;
import b.f.j.j.c;
import com.facebook.cache.common.CacheKey;
import com.facebook.common.internal.Supplier;
import java.util.concurrent.Executor;
/* compiled from: PipelineDraweeControllerFactory.java */
/* loaded from: classes2.dex */
public class f {
    public Resources a;

    /* renamed from: b  reason: collision with root package name */
    public a f478b;
    public b.f.j.i.a c;
    public Executor d;
    public w<CacheKey, c> e;
    public e<b.f.j.i.a> f;
    public Supplier<Boolean> g;
}
