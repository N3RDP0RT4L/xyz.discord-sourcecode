package b.f.g.a.a;

import android.content.Context;
import android.net.Uri;
import b.f.d.d.l;
import b.f.g.a.a.h.e;
import b.f.h.b.a.b;
import b.f.j.c.i;
import b.f.j.c.n;
import b.f.j.d.f;
import b.f.j.e.h;
import b.f.j.j.c;
import com.facebook.cache.common.CacheKey;
import com.facebook.common.internal.Supplier;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.controller.AbstractDraweeController;
import com.facebook.drawee.controller.AbstractDraweeControllerBuilder;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import java.util.Set;
/* compiled from: PipelineDraweeControllerBuilder.java */
/* loaded from: classes2.dex */
public class d extends AbstractDraweeControllerBuilder<d, ImageRequest, CloseableReference<c>, ImageInfo> {
    public final h o;
    public final f p;
    public e q;

    public d(Context context, f fVar, h hVar, Set<ControllerListener> set, Set<b> set2) {
        super(context, set, set2);
        this.o = hVar;
        this.p = fVar;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.facebook.drawee.controller.AbstractDraweeControllerBuilder
    public DataSource<CloseableReference<c>> b(DraweeController draweeController, String str, ImageRequest imageRequest, Object obj, AbstractDraweeControllerBuilder.b bVar) {
        ImageRequest.c cVar;
        b.f.g.a.a.h.c cVar2;
        ImageRequest imageRequest2 = imageRequest;
        h hVar = this.o;
        int ordinal = bVar.ordinal();
        if (ordinal == 0) {
            cVar = ImageRequest.c.FULL_FETCH;
        } else if (ordinal == 1) {
            cVar = ImageRequest.c.DISK_CACHE;
        } else if (ordinal == 2) {
            cVar = ImageRequest.c.BITMAP_MEMORY_CACHE;
        } else {
            throw new RuntimeException("Cache level" + bVar + "is not supported. ");
        }
        ImageRequest.c cVar3 = cVar;
        b.f.g.a.a.h.c cVar4 = null;
        if (draweeController instanceof c) {
            c cVar5 = (c) draweeController;
            synchronized (cVar5) {
                b.f.g.a.a.h.b bVar2 = cVar5.G;
                if (bVar2 != null) {
                    cVar4 = new b.f.g.a.a.h.c(cVar5.m, bVar2);
                }
                Set<b.f.j.k.e> set = cVar5.F;
                if (set != null) {
                    b.f.j.k.c cVar6 = new b.f.j.k.c(set);
                    if (cVar4 != null) {
                        cVar6.a.add(cVar4);
                    }
                    cVar2 = cVar6;
                }
            }
            return hVar.a(imageRequest2, obj, cVar3, cVar2, str);
        }
        cVar2 = cVar4;
        return hVar.a(imageRequest2, obj, cVar3, cVar2, str);
    }

    @Override // com.facebook.drawee.controller.AbstractDraweeControllerBuilder
    public AbstractDraweeController d() {
        c cVar;
        CacheKey cacheKey;
        b.f.j.r.b.b();
        try {
            DraweeController draweeController = this.n;
            String valueOf = String.valueOf(AbstractDraweeControllerBuilder.c.getAndIncrement());
            if (draweeController instanceof c) {
                cVar = (c) draweeController;
            } else {
                f fVar = this.p;
                c cVar2 = new c(fVar.a, fVar.f478b, fVar.c, fVar.d, fVar.e, fVar.f);
                Supplier<Boolean> supplier = fVar.g;
                if (supplier != null) {
                    cVar2.C = supplier.get().booleanValue();
                }
                cVar = cVar2;
            }
            Supplier<DataSource<CloseableReference<c>>> e = e(cVar, valueOf);
            ImageRequest imageRequest = (ImageRequest) this.h;
            i iVar = this.o.g;
            CacheKey cacheKey2 = null;
            if (iVar != null && imageRequest != null) {
                if (imageRequest.f2875s != null) {
                    cacheKey = ((n) iVar).c(imageRequest, this.g);
                } else {
                    cacheKey = ((n) iVar).a(imageRequest, this.g);
                }
                cacheKey2 = cacheKey;
            }
            cVar.H(e, valueOf, cacheKey2, this.g, null, null);
            cVar.I(this.q, this, l.a);
            return cVar;
        } finally {
            b.f.j.r.b.b();
        }
    }

    /* JADX WARN: Type inference failed for: r2v2, types: [REQUEST, com.facebook.imagepipeline.request.ImageRequest] */
    public d f(Uri uri) {
        if (uri == null) {
            this.h = null;
            return this;
        }
        ImageRequestBuilder b2 = ImageRequestBuilder.b(uri);
        b2.e = f.f563b;
        this.h = b2.a();
        return this;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public d g(String str) {
        Uri parse;
        if (str != null && !str.isEmpty()) {
            return f(Uri.parse(str));
        }
        REQUEST request = 0;
        request = 0;
        request = 0;
        if (!(str == null || str.length() == 0 || (parse = Uri.parse(str)) == null)) {
            request = ImageRequestBuilder.b(parse).a();
        }
        this.h = request;
        return this;
    }
}
