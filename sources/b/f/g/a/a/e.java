package b.f.g.a.a;

import android.content.Context;
import android.content.res.Resources;
import b.c.a.a0.d;
import b.f.d.b.g;
import b.f.g.b.a;
import b.f.g.b.b;
import b.f.j.c.w;
import b.f.j.e.h;
import b.f.j.e.m;
import b.f.j.j.c;
import com.facebook.cache.common.CacheKey;
import com.facebook.common.internal.Supplier;
/* compiled from: PipelineDraweeControllerBuilderSupplier.java */
/* loaded from: classes2.dex */
public class e implements Supplier<d> {
    public final Context a;

    /* renamed from: b  reason: collision with root package name */
    public final h f477b;
    public final f c;

    public e(Context context) {
        a aVar;
        m mVar = m.a;
        d.y(mVar, "ImagePipelineFactory was not initialized!");
        this.a = context;
        if (mVar.l == null) {
            mVar.l = mVar.a();
        }
        h hVar = mVar.l;
        this.f477b = hVar;
        f fVar = new f();
        this.c = fVar;
        Resources resources = context.getResources();
        synchronized (a.class) {
            if (a.a == null) {
                a.a = new b();
            }
            aVar = a.a;
        }
        b.f.j.a.b.a b2 = mVar.b();
        b.f.j.i.a a = b2 == null ? null : b2.a(context);
        if (g.k == null) {
            g.k = new g();
        }
        g gVar = g.k;
        w<CacheKey, c> wVar = hVar.e;
        fVar.a = resources;
        fVar.f478b = aVar;
        fVar.c = a;
        fVar.d = gVar;
        fVar.e = wVar;
        fVar.f = null;
        fVar.g = null;
    }

    /* renamed from: a */
    public d get() {
        d dVar = new d(this.a, this.c, this.f477b, null, null);
        dVar.q = null;
        return dVar;
    }
}
