package b.f.g.a.a;

import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import b.f.g.e.i;
import b.f.j.j.c;
import b.f.j.j.d;
import b.f.j.r.b;
/* compiled from: DefaultDrawableFactory.java */
/* loaded from: classes2.dex */
public class a implements b.f.j.i.a {
    public final Resources a;

    /* renamed from: b  reason: collision with root package name */
    public final b.f.j.i.a f472b;

    public a(Resources resources, b.f.j.i.a aVar) {
        this.a = resources;
        this.f472b = aVar;
    }

    @Override // b.f.j.i.a
    public boolean a(c cVar) {
        return true;
    }

    @Override // b.f.j.i.a
    public Drawable b(c cVar) {
        try {
            b.b();
            if (cVar instanceof d) {
                d dVar = (d) cVar;
                BitmapDrawable bitmapDrawable = new BitmapDrawable(this.a, dVar.m);
                int i = dVar.o;
                boolean z2 = true;
                if (!((i == 0 || i == -1) ? false : true)) {
                    int i2 = dVar.p;
                    if (i2 == 1 || i2 == 0) {
                        z2 = false;
                    }
                    if (!z2) {
                        return bitmapDrawable;
                    }
                }
                return new i(bitmapDrawable, dVar.o, dVar.p);
            }
            b.f.j.i.a aVar = this.f472b;
            if (aVar != null && aVar.a(cVar)) {
                return this.f472b.b(cVar);
            }
            return null;
        } finally {
            b.b();
        }
    }
}
