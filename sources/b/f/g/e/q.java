package b.f.g.e;

import android.graphics.Matrix;
import android.graphics.Rect;
import com.facebook.drawee.drawable.ScalingUtils$ScaleType;
/* compiled from: ScalingUtils.java */
/* loaded from: classes2.dex */
public abstract class q implements ScalingUtils$ScaleType {
    @Override // com.facebook.drawee.drawable.ScalingUtils$ScaleType
    public Matrix a(Matrix matrix, Rect rect, int i, int i2, float f, float f2) {
        b(matrix, rect, i, i2, f, f2, rect.width() / i, rect.height() / i2);
        return matrix;
    }

    public abstract void b(Matrix matrix, Rect rect, int i, int i2, float f, float f2, float f3, float f4);
}
