package b.f.g.e;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import androidx.annotation.VisibleForTesting;
import java.util.Objects;
/* compiled from: AutoRotateDrawable.java */
/* loaded from: classes2.dex */
public class c extends g implements Runnable {
    public int n;
    @VisibleForTesting
    public float p = 0.0f;
    public boolean q = false;
    public boolean o = true;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public c(Drawable drawable, int i) {
        super(drawable);
        Objects.requireNonNull(drawable);
        this.n = i;
    }

    @Override // b.f.g.e.g, android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        int save = canvas.save();
        Rect bounds = getBounds();
        int i = bounds.right;
        int i2 = bounds.left;
        int i3 = i - i2;
        int i4 = bounds.bottom;
        int i5 = bounds.top;
        int i6 = i4 - i5;
        float f = this.p;
        if (!this.o) {
            f = 360.0f - f;
        }
        canvas.rotate(f, (i3 / 2) + i2, (i6 / 2) + i5);
        Drawable drawable = this.k;
        if (drawable != null) {
            drawable.draw(canvas);
        }
        canvas.restoreToCount(save);
        if (!this.q) {
            this.q = true;
            scheduleSelf(this, SystemClock.uptimeMillis() + 20);
        }
    }

    @Override // java.lang.Runnable
    public void run() {
        this.q = false;
        this.p += (int) ((20.0f / this.n) * 360.0f);
        invalidateSelf();
    }
}
