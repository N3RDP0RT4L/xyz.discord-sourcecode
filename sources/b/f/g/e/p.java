package b.f.g.e;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import androidx.annotation.VisibleForTesting;
import b.c.a.a0.d;
import com.facebook.drawee.drawable.ScalingUtils$ScaleType;
/* compiled from: ScaleTypeDrawable.java */
/* loaded from: classes2.dex */
public class p extends g {
    @VisibleForTesting
    public ScalingUtils$ScaleType n;
    @VisibleForTesting
    public Object o;
    @VisibleForTesting

    /* renamed from: s  reason: collision with root package name */
    public Matrix f517s;
    @VisibleForTesting
    public PointF p = null;
    @VisibleForTesting
    public int q = 0;
    @VisibleForTesting
    public int r = 0;
    public Matrix t = new Matrix();

    public p(Drawable drawable, ScalingUtils$ScaleType scalingUtils$ScaleType) {
        super(drawable);
        this.n = scalingUtils$ScaleType;
    }

    @Override // b.f.g.e.g, b.f.g.e.e0
    public void d(Matrix matrix) {
        n(matrix);
        q();
        Matrix matrix2 = this.f517s;
        if (matrix2 != null) {
            matrix.preConcat(matrix2);
        }
    }

    @Override // b.f.g.e.g, android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        q();
        if (this.f517s != null) {
            int save = canvas.save();
            canvas.clipRect(getBounds());
            canvas.concat(this.f517s);
            Drawable drawable = this.k;
            if (drawable != null) {
                drawable.draw(canvas);
            }
            canvas.restoreToCount(save);
            return;
        }
        Drawable drawable2 = this.k;
        if (drawable2 != null) {
            drawable2.draw(canvas);
        }
    }

    @Override // b.f.g.e.g
    public Drawable o(Drawable drawable) {
        Drawable o = super.o(drawable);
        p();
        return o;
    }

    @Override // b.f.g.e.g, android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        p();
    }

    @VisibleForTesting
    public void p() {
        Drawable drawable = this.k;
        if (drawable == null) {
            this.r = 0;
            this.q = 0;
            this.f517s = null;
            return;
        }
        Rect bounds = getBounds();
        int width = bounds.width();
        int height = bounds.height();
        int intrinsicWidth = drawable.getIntrinsicWidth();
        this.q = intrinsicWidth;
        int intrinsicHeight = drawable.getIntrinsicHeight();
        this.r = intrinsicHeight;
        if (intrinsicWidth <= 0 || intrinsicHeight <= 0) {
            drawable.setBounds(bounds);
            this.f517s = null;
        } else if (intrinsicWidth == width && intrinsicHeight == height) {
            drawable.setBounds(bounds);
            this.f517s = null;
        } else {
            ScalingUtils$ScaleType scalingUtils$ScaleType = this.n;
            ScalingUtils$ScaleType scalingUtils$ScaleType2 = ScalingUtils$ScaleType.a;
            if (scalingUtils$ScaleType == z.l) {
                drawable.setBounds(bounds);
                this.f517s = null;
                return;
            }
            drawable.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
            ScalingUtils$ScaleType scalingUtils$ScaleType3 = this.n;
            Matrix matrix = this.t;
            PointF pointF = this.p;
            scalingUtils$ScaleType3.a(matrix, bounds, intrinsicWidth, intrinsicHeight, pointF != null ? pointF.x : 0.5f, pointF != null ? pointF.y : 0.5f);
            this.f517s = this.t;
        }
    }

    public final void q() {
        boolean z2;
        ScalingUtils$ScaleType scalingUtils$ScaleType = this.n;
        boolean z3 = true;
        if (scalingUtils$ScaleType instanceof c0) {
            Object state = ((c0) scalingUtils$ScaleType).getState();
            z2 = state == null || !state.equals(this.o);
            this.o = state;
        } else {
            z2 = false;
        }
        Drawable drawable = this.k;
        if (drawable != null) {
            if (this.q == drawable.getIntrinsicWidth() && this.r == drawable.getIntrinsicHeight()) {
                z3 = false;
            }
            if (z3 || z2) {
                p();
            }
        }
    }

    public void r(ScalingUtils$ScaleType scalingUtils$ScaleType) {
        if (!d.g0(this.n, scalingUtils$ScaleType)) {
            this.n = scalingUtils$ScaleType;
            this.o = null;
            p();
            invalidateSelf();
        }
    }
}
