package b.f.g.e;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import androidx.annotation.VisibleForTesting;
import b.c.a.a0.d;
import b.c.a.y.b;
import java.util.Arrays;
/* compiled from: RoundedCornersDrawable.java */
/* loaded from: classes2.dex */
public class m extends g implements j {
    public RectF p;
    public Matrix q;
    @VisibleForTesting
    public int n = 1;
    public final RectF o = new RectF();
    public final float[] r = new float[8];
    @VisibleForTesting

    /* renamed from: s  reason: collision with root package name */
    public final float[] f509s = new float[8];
    @VisibleForTesting
    public final Paint t = new Paint(1);
    public boolean u = false;
    public float v = 0.0f;
    public int w = 0;

    /* renamed from: x  reason: collision with root package name */
    public int f510x = 0;

    /* renamed from: y  reason: collision with root package name */
    public float f511y = 0.0f;

    /* renamed from: z  reason: collision with root package name */
    public boolean f512z = false;
    public boolean A = false;
    public final Path B = new Path();
    public final Path C = new Path();
    public final RectF D = new RectF();

    public m(Drawable drawable) {
        super(drawable);
    }

    @Override // b.f.g.e.j
    public void a(int i, float f) {
        this.w = i;
        this.v = f;
        p();
        invalidateSelf();
    }

    @Override // b.f.g.e.j
    public void c(boolean z2) {
        this.u = z2;
        p();
        invalidateSelf();
    }

    @Override // b.f.g.e.g, android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        this.o.set(getBounds());
        int h = b.h(this.n);
        if (h == 0) {
            if (this.f512z) {
                RectF rectF = this.p;
                if (rectF == null) {
                    this.p = new RectF(this.o);
                    this.q = new Matrix();
                } else {
                    rectF.set(this.o);
                }
                RectF rectF2 = this.p;
                float f = this.v;
                rectF2.inset(f, f);
                this.q.setRectToRect(this.o, this.p, Matrix.ScaleToFit.FILL);
                int save = canvas.save();
                canvas.clipRect(this.o);
                canvas.concat(this.q);
                Drawable drawable = this.k;
                if (drawable != null) {
                    drawable.draw(canvas);
                }
                canvas.restoreToCount(save);
            } else {
                Drawable drawable2 = this.k;
                if (drawable2 != null) {
                    drawable2.draw(canvas);
                }
            }
            this.t.setStyle(Paint.Style.FILL);
            this.t.setColor(this.f510x);
            this.t.setStrokeWidth(0.0f);
            this.t.setFilterBitmap(this.A);
            this.B.setFillType(Path.FillType.EVEN_ODD);
            canvas.drawPath(this.B, this.t);
            if (this.u) {
                float width = ((this.o.width() - this.o.height()) + this.v) / 2.0f;
                float height = ((this.o.height() - this.o.width()) + this.v) / 2.0f;
                if (width > 0.0f) {
                    RectF rectF3 = this.o;
                    float f2 = rectF3.left;
                    canvas.drawRect(f2, rectF3.top, f2 + width, rectF3.bottom, this.t);
                    RectF rectF4 = this.o;
                    float f3 = rectF4.right;
                    canvas.drawRect(f3 - width, rectF4.top, f3, rectF4.bottom, this.t);
                }
                if (height > 0.0f) {
                    RectF rectF5 = this.o;
                    float f4 = rectF5.left;
                    float f5 = rectF5.top;
                    canvas.drawRect(f4, f5, rectF5.right, f5 + height, this.t);
                    RectF rectF6 = this.o;
                    float f6 = rectF6.left;
                    float f7 = rectF6.bottom;
                    canvas.drawRect(f6, f7 - height, rectF6.right, f7, this.t);
                }
            }
        } else if (h == 1) {
            int save2 = canvas.save();
            canvas.clipPath(this.B);
            Drawable drawable3 = this.k;
            if (drawable3 != null) {
                drawable3.draw(canvas);
            }
            canvas.restoreToCount(save2);
        }
        if (this.w != 0) {
            this.t.setStyle(Paint.Style.STROKE);
            this.t.setColor(this.w);
            this.t.setStrokeWidth(this.v);
            this.B.setFillType(Path.FillType.EVEN_ODD);
            canvas.drawPath(this.C, this.t);
        }
    }

    @Override // b.f.g.e.j
    public void e(boolean z2) {
        if (this.A != z2) {
            this.A = z2;
            invalidateSelf();
        }
    }

    @Override // b.f.g.e.j
    public void f(boolean z2) {
        this.f512z = z2;
        p();
        invalidateSelf();
    }

    @Override // b.f.g.e.j
    public void i(float f) {
        this.f511y = f;
        p();
        invalidateSelf();
    }

    @Override // b.f.g.e.j
    public void j(float f) {
        Arrays.fill(this.r, f);
        p();
        invalidateSelf();
    }

    @Override // b.f.g.e.j
    public void m(float[] fArr) {
        if (fArr == null) {
            Arrays.fill(this.r, 0.0f);
        } else {
            d.k(fArr.length == 8, "radii should have exactly 8 values");
            System.arraycopy(fArr, 0, this.r, 0, 8);
        }
        p();
        invalidateSelf();
    }

    @Override // b.f.g.e.g, android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        Drawable drawable = this.k;
        if (drawable != null) {
            drawable.setBounds(rect);
        }
        p();
    }

    public final void p() {
        float[] fArr;
        this.B.reset();
        this.C.reset();
        this.D.set(getBounds());
        RectF rectF = this.D;
        float f = this.f511y;
        rectF.inset(f, f);
        if (this.n == 1) {
            this.B.addRect(this.D, Path.Direction.CW);
        }
        if (this.u) {
            this.B.addCircle(this.D.centerX(), this.D.centerY(), Math.min(this.D.width(), this.D.height()) / 2.0f, Path.Direction.CW);
        } else {
            this.B.addRoundRect(this.D, this.r, Path.Direction.CW);
        }
        RectF rectF2 = this.D;
        float f2 = -this.f511y;
        rectF2.inset(f2, f2);
        RectF rectF3 = this.D;
        float f3 = this.v / 2.0f;
        rectF3.inset(f3, f3);
        if (this.u) {
            this.C.addCircle(this.D.centerX(), this.D.centerY(), Math.min(this.D.width(), this.D.height()) / 2.0f, Path.Direction.CW);
        } else {
            int i = 0;
            while (true) {
                fArr = this.f509s;
                if (i >= fArr.length) {
                    break;
                }
                fArr[i] = (this.r[i] + this.f511y) - (this.v / 2.0f);
                i++;
            }
            this.C.addRoundRect(this.D, fArr, Path.Direction.CW);
        }
        RectF rectF4 = this.D;
        float f4 = (-this.v) / 2.0f;
        rectF4.inset(f4, f4);
    }
}
