package b.f.g.e;

import android.graphics.Matrix;
import android.graphics.Rect;
import com.facebook.drawee.drawable.ScalingUtils$ScaleType;
/* compiled from: ScalingUtils.java */
/* loaded from: classes2.dex */
public class a0 extends q {
    public static final ScalingUtils$ScaleType l = new a0();

    @Override // b.f.g.e.q
    public void b(Matrix matrix, Rect rect, int i, int i2, float f, float f2, float f3, float f4) {
        float width = (rect.width() - (i * f4)) * 0.5f;
        matrix.setScale(f4, f4);
        matrix.postTranslate((int) (width + rect.left + 0.5f), (int) (rect.top + 0.5f));
    }

    public String toString() {
        return "fit_y";
    }
}
