package b.f.g.e;

import android.graphics.Matrix;
import android.graphics.Rect;
import com.facebook.drawee.drawable.ScalingUtils$ScaleType;
/* compiled from: ScalingUtils.java */
/* loaded from: classes2.dex */
public class y extends q {
    public static final ScalingUtils$ScaleType l = new y();

    @Override // b.f.g.e.q
    public void b(Matrix matrix, Rect rect, int i, int i2, float f, float f2, float f3, float f4) {
        float height = (rect.height() - (i2 * f3)) * 0.5f;
        matrix.setScale(f3, f3);
        matrix.postTranslate((int) (rect.left + 0.5f), (int) (height + rect.top + 0.5f));
    }

    public String toString() {
        return "fit_x";
    }
}
