package b.f.g.c;

import b.c.a.a0.d;
import b.f.d.d.i;
import com.facebook.common.internal.Supplier;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.controller.AbstractDraweeControllerBuilder;
import com.facebook.drawee.interfaces.DraweeController;
/* compiled from: AbstractDraweeControllerBuilder.java */
/* loaded from: classes2.dex */
public class b implements Supplier<DataSource<IMAGE>> {
    public final /* synthetic */ DraweeController a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ String f496b;
    public final /* synthetic */ Object c;
    public final /* synthetic */ Object d;
    public final /* synthetic */ AbstractDraweeControllerBuilder.b e;
    public final /* synthetic */ AbstractDraweeControllerBuilder f;

    public b(AbstractDraweeControllerBuilder abstractDraweeControllerBuilder, DraweeController draweeController, String str, Object obj, Object obj2, AbstractDraweeControllerBuilder.b bVar) {
        this.f = abstractDraweeControllerBuilder;
        this.a = draweeController;
        this.f496b = str;
        this.c = obj;
        this.d = obj2;
        this.e = bVar;
    }

    @Override // com.facebook.common.internal.Supplier
    public Object get() {
        return this.f.b(this.a, this.f496b, this.c, this.d, this.e);
    }

    public String toString() {
        i h2 = d.h2(this);
        h2.c("request", this.c.toString());
        return h2.toString();
    }
}
