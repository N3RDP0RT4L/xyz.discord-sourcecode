package b.f.g.g;

import android.content.Context;
import android.view.ViewConfiguration;
import androidx.annotation.VisibleForTesting;
/* compiled from: GestureDetector.java */
/* loaded from: classes2.dex */
public class a {
    @VisibleForTesting

    /* renamed from: b  reason: collision with root package name */
    public final float f520b;
    @VisibleForTesting
    public long e;
    @VisibleForTesting
    public float f;
    @VisibleForTesting
    public float g;
    @VisibleForTesting
    public AbstractC0068a a = null;
    @VisibleForTesting
    public boolean c = false;
    @VisibleForTesting
    public boolean d = false;

    /* compiled from: GestureDetector.java */
    /* renamed from: b.f.g.g.a$a  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public interface AbstractC0068a {
    }

    public a(Context context) {
        this.f520b = ViewConfiguration.get(context).getScaledTouchSlop();
    }
}
