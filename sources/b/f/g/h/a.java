package b.f.g.h;

import android.graphics.drawable.Drawable;
import com.facebook.drawee.interfaces.DraweeHierarchy;
/* compiled from: SettableDraweeHierarchy.java */
/* loaded from: classes2.dex */
public interface a extends DraweeHierarchy {
    void a(Drawable drawable);

    void b(Throwable th);

    void c(Throwable th);

    void d(float f, boolean z2);

    void f(Drawable drawable, float f, boolean z2);

    void reset();
}
