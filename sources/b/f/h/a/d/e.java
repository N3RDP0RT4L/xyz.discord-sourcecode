package b.f.h.a.d;

import b.f.j.a.c.b;
import b.f.j.c.m;
import b.f.j.i.a;
import b.f.j.j.c;
import com.facebook.cache.common.CacheKey;
import com.facebook.common.internal.Supplier;
import com.facebook.imagepipeline.bitmaps.PlatformBitmapFactory;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
/* compiled from: ExperimentalBitmapAnimationDrawableFactory.java */
/* loaded from: classes2.dex */
public class e implements a {
    public final b a;

    /* renamed from: b  reason: collision with root package name */
    public final ScheduledExecutorService f532b;
    public final ExecutorService c;
    public final b.f.d.k.b d;
    public final PlatformBitmapFactory e;
    public final m<CacheKey, c> f;
    public final Supplier<Integer> g;
    public final Supplier<Integer> h;
    public final Supplier<Boolean> i;

    public e(b bVar, ScheduledExecutorService scheduledExecutorService, ExecutorService executorService, b.f.d.k.b bVar2, PlatformBitmapFactory platformBitmapFactory, m<CacheKey, c> mVar, Supplier<Integer> supplier, Supplier<Integer> supplier2, Supplier<Boolean> supplier3) {
        this.a = bVar;
        this.f532b = scheduledExecutorService;
        this.c = executorService;
        this.d = bVar2;
        this.e = platformBitmapFactory;
        this.f = mVar;
        this.g = supplier;
        this.h = supplier2;
        this.i = supplier3;
    }

    @Override // b.f.j.i.a
    public boolean a(c cVar) {
        return cVar instanceof b.f.j.j.a;
    }

    /* JADX WARN: Removed duplicated region for block: B:30:0x007d  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x0093  */
    @Override // b.f.j.i.a
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public android.graphics.drawable.Drawable b(b.f.j.j.c r12) {
        /*
            r11 = this;
            b.f.j.j.a r12 = (b.f.j.j.a) r12
            monitor-enter(r12)
            b.f.j.a.a.e r0 = r12.l     // Catch: java.lang.Throwable -> Lb2
            r1 = 0
            if (r0 != 0) goto La
            r0 = r1
            goto Lc
        La:
            b.f.j.a.a.c r0 = r0.a     // Catch: java.lang.Throwable -> Lb2
        Lc:
            monitor-exit(r12)
            b.f.h.a.c.a r2 = new b.f.h.a.c.a
            monitor-enter(r12)
            b.f.j.a.a.e r3 = r12.l     // Catch: java.lang.Throwable -> Laf
            monitor-exit(r12)
            java.util.Objects.requireNonNull(r3)
            if (r0 == 0) goto L1d
            android.graphics.Bitmap$Config r12 = r0.d()
            goto L1e
        L1d:
            r12 = r1
        L1e:
            b.f.j.a.a.c r0 = r3.a
            android.graphics.Rect r4 = new android.graphics.Rect
            int r5 = r0.getWidth()
            int r0 = r0.getHeight()
            r6 = 0
            r4.<init>(r6, r6, r5, r0)
            b.f.j.a.c.b r0 = r11.a
            b.f.j.a.a.a r0 = r0.a(r3, r4)
            com.facebook.common.internal.Supplier<java.lang.Integer> r4 = r11.g
            java.lang.Object r4 = r4.get()
            java.lang.Integer r4 = (java.lang.Integer) r4
            int r4 = r4.intValue()
            r5 = 1
            if (r4 == r5) goto L5f
            r5 = 2
            if (r4 == r5) goto L55
            r3 = 3
            if (r4 == r3) goto L4f
            b.f.h.a.b.d.d r3 = new b.f.h.a.b.d.d
            r3.<init>()
            goto L69
        L4f:
            b.f.h.a.b.d.c r3 = new b.f.h.a.b.d.c
            r3.<init>()
            goto L69
        L55:
            b.f.h.a.b.d.b r4 = new b.f.h.a.b.d.b
            b.f.j.a.c.c r3 = r11.c(r3)
            r4.<init>(r3, r6)
            goto L68
        L5f:
            b.f.h.a.b.d.b r4 = new b.f.h.a.b.d.b
            b.f.j.a.c.c r3 = r11.c(r3)
            r4.<init>(r3, r5)
        L68:
            r3 = r4
        L69:
            r6 = r3
            b.f.h.a.b.f.b r8 = new b.f.h.a.b.f.b
            r8.<init>(r6, r0)
            com.facebook.common.internal.Supplier<java.lang.Integer> r3 = r11.h
            java.lang.Object r3 = r3.get()
            java.lang.Integer r3 = (java.lang.Integer) r3
            int r3 = r3.intValue()
            if (r3 <= 0) goto L93
            b.f.h.a.b.e.d r1 = new b.f.h.a.b.e.d
            r1.<init>(r3)
            b.f.h.a.b.e.c r3 = new b.f.h.a.b.e.c
            com.facebook.imagepipeline.bitmaps.PlatformBitmapFactory r4 = r11.e
            if (r12 == 0) goto L89
            goto L8b
        L89:
            android.graphics.Bitmap$Config r12 = android.graphics.Bitmap.Config.ARGB_8888
        L8b:
            java.util.concurrent.ExecutorService r5 = r11.c
            r3.<init>(r4, r8, r12, r5)
            r9 = r1
            r10 = r3
            goto L95
        L93:
            r9 = r1
            r10 = r9
        L95:
            b.f.h.a.b.a r12 = new b.f.h.a.b.a
            com.facebook.imagepipeline.bitmaps.PlatformBitmapFactory r5 = r11.e
            b.f.h.a.b.f.a r7 = new b.f.h.a.b.f.a
            r7.<init>(r0)
            r4 = r12
            r4.<init>(r5, r6, r7, r8, r9, r10)
            b.f.d.k.b r0 = r11.d
            java.util.concurrent.ScheduledExecutorService r1 = r11.f532b
            b.f.h.a.a.c r3 = new b.f.h.a.a.c
            r3.<init>(r12, r12, r0, r1)
            r2.<init>(r3)
            return r2
        Laf:
            r0 = move-exception
            monitor-exit(r12)
            throw r0
        Lb2:
            r0 = move-exception
            monitor-exit(r12)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.f.h.a.d.e.b(b.f.j.j.c):android.graphics.drawable.Drawable");
    }

    public final b.f.j.a.c.c c(b.f.j.a.a.e eVar) {
        return new b.f.j.a.c.c(new b.f.h.a.b.d.a(eVar.hashCode(), Boolean.FALSE.booleanValue()), this.f);
    }
}
