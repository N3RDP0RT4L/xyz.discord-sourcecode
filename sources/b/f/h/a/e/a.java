package b.f.h.a.e;

import androidx.annotation.VisibleForTesting;
import b.f.h.a.a.d;
/* compiled from: DropFramesFrameScheduler.java */
/* loaded from: classes2.dex */
public class a {
    public final d a;

    /* renamed from: b  reason: collision with root package name */
    public long f533b = -1;

    public a(d dVar) {
        this.a = dVar;
    }

    @VisibleForTesting
    public int a(long j) {
        int i = 0;
        long j2 = 0;
        do {
            j2 += this.a.e(i);
            i++;
        } while (j >= j2);
        return i - 1;
    }

    public long b() {
        long j = this.f533b;
        if (j != -1) {
            return j;
        }
        this.f533b = 0L;
        int a = this.a.a();
        for (int i = 0; i < a; i++) {
            this.f533b += this.a.e(i);
        }
        return this.f533b;
    }
}
