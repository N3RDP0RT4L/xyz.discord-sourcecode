package b.f.h.a.a;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import b.f.h.a.a.a;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
/* compiled from: AnimationBackendDelegateWithInactivityCheck.java */
/* loaded from: classes2.dex */
public class c<T extends b.f.h.a.a.a> extends b.f.h.a.a.b<T> {

    /* renamed from: b  reason: collision with root package name */
    public final b.f.d.k.b f521b;
    public final ScheduledExecutorService c;
    public long e;
    public b f;
    public boolean d = false;
    public final Runnable g = new a();

    /* compiled from: AnimationBackendDelegateWithInactivityCheck.java */
    /* loaded from: classes2.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            synchronized (c.this) {
                c cVar = c.this;
                boolean z2 = false;
                cVar.d = false;
                if (cVar.f521b.now() - cVar.e > 2000) {
                    z2 = true;
                }
                if (z2) {
                    b bVar = c.this.f;
                    if (bVar != null) {
                        bVar.c();
                    }
                } else {
                    c.this.c();
                }
            }
        }
    }

    /* compiled from: AnimationBackendDelegateWithInactivityCheck.java */
    /* loaded from: classes2.dex */
    public interface b {
        void c();
    }

    public c(T t, b bVar, b.f.d.k.b bVar2, ScheduledExecutorService scheduledExecutorService) {
        super(t);
        this.f = bVar;
        this.f521b = bVar2;
        this.c = scheduledExecutorService;
    }

    public final synchronized void c() {
        if (!this.d) {
            this.d = true;
            this.c.schedule(this.g, 1000L, TimeUnit.MILLISECONDS);
        }
    }

    @Override // b.f.h.a.a.b, b.f.h.a.a.a
    public boolean j(Drawable drawable, Canvas canvas, int i) {
        this.e = this.f521b.now();
        boolean j = super.j(drawable, canvas, i);
        c();
        return j;
    }
}
