package b.f.h.a.a;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import androidx.annotation.IntRange;
/* compiled from: AnimationBackend.java */
/* loaded from: classes2.dex */
public interface a extends d {
    void clear();

    void d(ColorFilter colorFilter);

    void f(@IntRange(from = 0, to = 255) int i);

    int g();

    void h(Rect rect);

    int i();

    boolean j(Drawable drawable, Canvas canvas, int i);
}
