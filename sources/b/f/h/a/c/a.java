package b.f.h.a.c;

import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import b.f.g.e.e;
import java.util.Objects;
/* compiled from: AnimatedDrawable2.java */
/* loaded from: classes2.dex */
public class a extends Drawable implements Animatable, b.f.f.a.a {
    public static final Class<?> j = a.class;
    public static final b k = new b();
    public b.f.h.a.a.a l;
    public b.f.h.a.e.a m;
    public volatile boolean n;
    public long o;
    public long p;
    public long q;
    public int r;

    /* renamed from: s  reason: collision with root package name */
    public long f528s;
    public long t;
    public int u;
    public long v;
    public int w;

    /* renamed from: x  reason: collision with root package name */
    public volatile b f529x;

    /* renamed from: y  reason: collision with root package name */
    public e f530y;

    /* renamed from: z  reason: collision with root package name */
    public final Runnable f531z;

    /* compiled from: AnimatedDrawable2.java */
    /* renamed from: b.f.h.a.c.a$a  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public class RunnableC0069a implements Runnable {
        public RunnableC0069a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            a aVar = a.this;
            aVar.unscheduleSelf(aVar.f531z);
            a.this.invalidateSelf();
        }
    }

    public a() {
        this(null);
    }

    @Override // b.f.f.a.a
    public void a() {
        b.f.h.a.a.a aVar = this.l;
        if (aVar != null) {
            aVar.clear();
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:54:0x00f4  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x0104  */
    @Override // android.graphics.drawable.Drawable
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void draw(android.graphics.Canvas r19) {
        /*
            Method dump skipped, instructions count: 270
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.f.h.a.c.a.draw(android.graphics.Canvas):void");
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        b.f.h.a.a.a aVar = this.l;
        if (aVar == null) {
            return super.getIntrinsicHeight();
        }
        return aVar.g();
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        b.f.h.a.a.a aVar = this.l;
        if (aVar == null) {
            return super.getIntrinsicWidth();
        }
        return aVar.i();
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    @Override // android.graphics.drawable.Animatable
    public boolean isRunning() {
        return this.n;
    }

    @Override // android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        b.f.h.a.a.a aVar = this.l;
        if (aVar != null) {
            aVar.h(rect);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public boolean onLevelChange(int i) {
        if (this.n) {
            return false;
        }
        long j2 = i;
        if (this.p == j2) {
            return false;
        }
        this.p = j2;
        invalidateSelf();
        return true;
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        if (this.f530y == null) {
            this.f530y = new e();
        }
        this.f530y.a = i;
        b.f.h.a.a.a aVar = this.l;
        if (aVar != null) {
            aVar.f(i);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        if (this.f530y == null) {
            this.f530y = new e();
        }
        e eVar = this.f530y;
        eVar.c = colorFilter;
        eVar.f502b = colorFilter != null;
        b.f.h.a.a.a aVar = this.l;
        if (aVar != null) {
            aVar.d(colorFilter);
        }
    }

    @Override // android.graphics.drawable.Animatable
    public void start() {
        b.f.h.a.a.a aVar;
        if (!this.n && (aVar = this.l) != null && aVar.a() > 1) {
            this.n = true;
            long uptimeMillis = SystemClock.uptimeMillis();
            long j2 = uptimeMillis - this.f528s;
            this.o = j2;
            this.q = j2;
            this.p = uptimeMillis - this.t;
            this.r = this.u;
            invalidateSelf();
            Objects.requireNonNull(this.f529x);
        }
    }

    @Override // android.graphics.drawable.Animatable
    public void stop() {
        if (this.n) {
            long uptimeMillis = SystemClock.uptimeMillis();
            this.f528s = uptimeMillis - this.o;
            this.t = uptimeMillis - this.p;
            this.u = this.r;
            this.n = false;
            this.o = 0L;
            this.q = 0L;
            this.p = -1L;
            this.r = -1;
            unscheduleSelf(this.f531z);
            Objects.requireNonNull(this.f529x);
        }
    }

    public a(b.f.h.a.a.a aVar) {
        this.v = 8L;
        this.f529x = k;
        this.f531z = new RunnableC0069a();
        this.l = aVar;
        this.m = aVar == null ? null : new b.f.h.a.e.a(aVar);
    }
}
