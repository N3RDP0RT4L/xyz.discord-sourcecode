package b.f.h.a.b.f;

import b.f.h.a.a.d;
/* compiled from: AnimatedDrawableBackendAnimationInformation.java */
/* loaded from: classes2.dex */
public class a implements d {
    public final b.f.j.a.a.a a;

    public a(b.f.j.a.a.a aVar) {
        this.a = aVar;
    }

    @Override // b.f.h.a.a.d
    public int a() {
        return ((b.f.j.a.c.a) this.a).b();
    }

    @Override // b.f.h.a.a.d
    public int b() {
        return ((b.f.j.a.c.a) this.a).c.b();
    }

    @Override // b.f.h.a.a.d
    public int e(int i) {
        return ((b.f.j.a.c.a) this.a).e[i];
    }
}
