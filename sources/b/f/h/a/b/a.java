package b.f.h.a.b;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import androidx.annotation.IntRange;
import b.f.h.a.a.c;
import b.f.h.a.a.d;
import b.f.h.a.b.e.b;
import b.f.h.a.b.e.c;
import com.facebook.common.references.CloseableReference;
import com.facebook.imagepipeline.bitmaps.PlatformBitmapFactory;
import java.util.Objects;
/* compiled from: BitmapAnimationBackend.java */
/* loaded from: classes2.dex */
public class a implements b.f.h.a.a.a, c.b {
    public final PlatformBitmapFactory a;

    /* renamed from: b  reason: collision with root package name */
    public final b f522b;
    public final d c;
    public final c d;
    public final b.f.h.a.b.e.a e;
    public final b f;
    public Rect h;
    public int i;
    public int j;
    public Bitmap.Config k = Bitmap.Config.ARGB_8888;
    public final Paint g = new Paint(6);

    public a(PlatformBitmapFactory platformBitmapFactory, b bVar, d dVar, c cVar, b.f.h.a.b.e.a aVar, b bVar2) {
        this.a = platformBitmapFactory;
        this.f522b = bVar;
        this.c = dVar;
        this.d = cVar;
        this.e = aVar;
        this.f = bVar2;
        n();
    }

    @Override // b.f.h.a.a.d
    public int a() {
        return this.c.a();
    }

    @Override // b.f.h.a.a.d
    public int b() {
        return this.c.b();
    }

    @Override // b.f.h.a.a.c.b
    public void c() {
        this.f522b.clear();
    }

    @Override // b.f.h.a.a.a
    public void clear() {
        this.f522b.clear();
    }

    @Override // b.f.h.a.a.a
    public void d(ColorFilter colorFilter) {
        this.g.setColorFilter(colorFilter);
    }

    @Override // b.f.h.a.a.d
    public int e(int i) {
        return this.c.e(i);
    }

    @Override // b.f.h.a.a.a
    public void f(@IntRange(from = 0, to = 255) int i) {
        this.g.setAlpha(i);
    }

    @Override // b.f.h.a.a.a
    public int g() {
        return this.j;
    }

    @Override // b.f.h.a.a.a
    public void h(Rect rect) {
        this.h = rect;
        b.f.h.a.b.f.b bVar = (b.f.h.a.b.f.b) this.d;
        b.f.j.a.c.a aVar = (b.f.j.a.c.a) bVar.f527b;
        if (!b.f.j.a.c.a.a(aVar.c, rect).equals(aVar.d)) {
            aVar = new b.f.j.a.c.a(aVar.a, aVar.f544b, rect, aVar.i);
        }
        if (aVar != bVar.f527b) {
            bVar.f527b = aVar;
            bVar.c = new b.f.j.a.c.d(aVar, bVar.d);
        }
        n();
    }

    @Override // b.f.h.a.a.a
    public int i() {
        return this.i;
    }

    @Override // b.f.h.a.a.a
    public boolean j(Drawable drawable, Canvas canvas, int i) {
        b bVar;
        int i2 = i;
        boolean l = l(canvas, i2, 0);
        b.f.h.a.b.e.a aVar = this.e;
        if (!(aVar == null || (bVar = this.f) == null)) {
            b bVar2 = this.f522b;
            b.f.h.a.b.e.d dVar = (b.f.h.a.b.e.d) aVar;
            int i3 = 1;
            while (i3 <= dVar.a) {
                int a = (i2 + i3) % a();
                b.f.d.e.a.h(2);
                b.f.h.a.b.e.c cVar = (b.f.h.a.b.e.c) bVar;
                Objects.requireNonNull(cVar);
                int hashCode = (hashCode() * 31) + a;
                synchronized (cVar.e) {
                    if (cVar.e.get(hashCode) != null) {
                        int i4 = b.f.d.e.a.a;
                    } else if (bVar2.c(a)) {
                        int i5 = b.f.d.e.a.a;
                    } else {
                        c.a aVar2 = new c.a(this, bVar2, a, hashCode);
                        cVar.e.put(hashCode, aVar2);
                        cVar.d.execute(aVar2);
                    }
                }
                i3++;
                i2 = i;
            }
        }
        return l;
    }

    public final boolean k(int i, CloseableReference<Bitmap> closeableReference, Canvas canvas, int i2) {
        if (!CloseableReference.y(closeableReference)) {
            return false;
        }
        if (this.h == null) {
            canvas.drawBitmap(closeableReference.u(), 0.0f, 0.0f, this.g);
        } else {
            canvas.drawBitmap(closeableReference.u(), (Rect) null, this.h, this.g);
        }
        if (i2 == 3) {
            return true;
        }
        this.f522b.e(i, closeableReference, i2);
        return true;
    }

    public final boolean l(Canvas canvas, int i, int i2) {
        boolean z2;
        CloseableReference d;
        int i3 = 2;
        boolean z3 = true;
        CloseableReference closeableReference = null;
        try {
            if (i2 == 0) {
                d = this.f522b.d(i);
                z2 = k(i, d, canvas, 0);
                i3 = 1;
            } else if (i2 == 1) {
                d = this.f522b.a(i, this.i, this.j);
                if (!m(i, d) || !k(i, d, canvas, 1)) {
                    z3 = false;
                }
                z2 = z3;
            } else if (i2 == 2) {
                try {
                    d = this.a.a(this.i, this.j, this.k);
                    if (!m(i, d) || !k(i, d, canvas, 2)) {
                        z3 = false;
                    }
                    z2 = z3;
                    i3 = 3;
                } catch (RuntimeException e) {
                    b.f.d.e.a.l(a.class, "Failed to create frame bitmap", e);
                    Class<CloseableReference> cls = CloseableReference.j;
                    return false;
                }
            } else if (i2 != 3) {
                Class<CloseableReference> cls2 = CloseableReference.j;
                return false;
            } else {
                d = this.f522b.f(i);
                z2 = k(i, d, canvas, 3);
                i3 = -1;
            }
            return (z2 || i3 == -1) ? z2 : l(canvas, i, i3);
        } finally {
            Class<CloseableReference> cls3 = CloseableReference.j;
            if (closeableReference != null) {
                closeableReference.close();
            }
        }
    }

    public final boolean m(int i, CloseableReference<Bitmap> closeableReference) {
        if (!CloseableReference.y(closeableReference)) {
            return false;
        }
        boolean a = ((b.f.h.a.b.f.b) this.d).a(i, closeableReference.u());
        if (!a) {
            closeableReference.close();
        }
        return a;
    }

    public final void n() {
        int width = ((b.f.j.a.c.a) ((b.f.h.a.b.f.b) this.d).f527b).c.getWidth();
        this.i = width;
        int i = -1;
        if (width == -1) {
            Rect rect = this.h;
            this.i = rect == null ? -1 : rect.width();
        }
        int height = ((b.f.j.a.c.a) ((b.f.h.a.b.f.b) this.d).f527b).c.getHeight();
        this.j = height;
        if (height == -1) {
            Rect rect2 = this.h;
            if (rect2 != null) {
                i = rect2.height();
            }
            this.j = i;
        }
    }
}
