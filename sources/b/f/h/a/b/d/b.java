package b.f.h.a.b.d;

import android.graphics.Bitmap;
import android.util.SparseArray;
import androidx.annotation.VisibleForTesting;
import b.f.d.e.a;
import b.f.j.a.c.c;
import b.f.j.j.d;
import b.f.j.j.h;
import com.facebook.cache.common.CacheKey;
import com.facebook.common.references.CloseableReference;
import java.util.Iterator;
import java.util.Objects;
/* compiled from: FrescoFrameCache.java */
/* loaded from: classes2.dex */
public class b implements b.f.h.a.b.b {
    public final c a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f524b;
    public final SparseArray<CloseableReference<b.f.j.j.c>> c = new SparseArray<>();
    public CloseableReference<b.f.j.j.c> d;

    public b(c cVar, boolean z2) {
        this.a = cVar;
        this.f524b = z2;
    }

    @VisibleForTesting
    public static CloseableReference<Bitmap> g(CloseableReference<b.f.j.j.c> closeableReference) {
        CloseableReference<Bitmap> n;
        try {
            if (!CloseableReference.y(closeableReference) || !(closeableReference.u() instanceof d)) {
                if (closeableReference != null) {
                    closeableReference.close();
                }
                return null;
            }
            d dVar = (d) closeableReference.u();
            synchronized (dVar) {
                n = CloseableReference.n(dVar.l);
            }
            closeableReference.close();
            return n;
        } catch (Throwable th) {
            Class<CloseableReference> cls = CloseableReference.j;
            if (closeableReference != null) {
                closeableReference.close();
            }
            throw th;
        }
    }

    @Override // b.f.h.a.b.b
    public synchronized CloseableReference<Bitmap> a(int i, int i2, int i3) {
        CacheKey cacheKey;
        CloseableReference<b.f.j.j.c> closeableReference = null;
        if (!this.f524b) {
            return null;
        }
        c cVar = this.a;
        while (true) {
            synchronized (cVar) {
                Iterator<CacheKey> it = cVar.d.iterator();
                if (it.hasNext()) {
                    cacheKey = it.next();
                    it.remove();
                } else {
                    cacheKey = null;
                }
            }
            if (cacheKey == null) {
                break;
            }
            CloseableReference<b.f.j.j.c> b2 = cVar.f545b.b(cacheKey);
            if (b2 != null) {
                closeableReference = b2;
                break;
            }
        }
        return g(closeableReference);
    }

    @Override // b.f.h.a.b.b
    public synchronized void b(int i, CloseableReference<Bitmap> closeableReference, int i2) {
        CloseableReference<b.f.j.j.c> A = CloseableReference.A(new d(closeableReference, h.a, 0, 0));
        if (A == null) {
            if (A != null) {
                A.close();
            }
            return;
        }
        c cVar = this.a;
        CloseableReference<b.f.j.j.c> c = cVar.f545b.c(new c.b(cVar.a, i), A, cVar.c);
        if (CloseableReference.y(c)) {
            CloseableReference<b.f.j.j.c> closeableReference2 = this.c.get(i);
            if (closeableReference2 != null) {
                closeableReference2.close();
            }
            this.c.put(i, c);
            int i3 = a.a;
        }
        A.close();
    }

    @Override // b.f.h.a.b.b
    public synchronized boolean c(int i) {
        c cVar;
        cVar = this.a;
        return cVar.f545b.contains(new c.b(cVar.a, i));
    }

    @Override // b.f.h.a.b.b
    public synchronized void clear() {
        CloseableReference<b.f.j.j.c> closeableReference = this.d;
        Class<CloseableReference> cls = CloseableReference.j;
        if (closeableReference != null) {
            closeableReference.close();
        }
        this.d = null;
        for (int i = 0; i < this.c.size(); i++) {
            CloseableReference<b.f.j.j.c> valueAt = this.c.valueAt(i);
            if (valueAt != null) {
                valueAt.close();
            }
        }
        this.c.clear();
    }

    @Override // b.f.h.a.b.b
    public synchronized CloseableReference<Bitmap> d(int i) {
        c cVar;
        cVar = this.a;
        return g(cVar.f545b.get(new c.b(cVar.a, i)));
    }

    @Override // b.f.h.a.b.b
    public synchronized void e(int i, CloseableReference<Bitmap> closeableReference, int i2) {
        Objects.requireNonNull(closeableReference);
        synchronized (this) {
            CloseableReference<b.f.j.j.c> closeableReference2 = this.c.get(i);
            if (closeableReference2 != null) {
                this.c.delete(i);
                Class<CloseableReference> cls = CloseableReference.j;
                closeableReference2.close();
                int i3 = a.a;
            }
        }
        CloseableReference<b.f.j.j.c> A = CloseableReference.A(new d(closeableReference, h.a, 0, 0));
        if (A != null) {
            CloseableReference<b.f.j.j.c> closeableReference3 = this.d;
            if (closeableReference3 != null) {
                closeableReference3.close();
            }
            c cVar = this.a;
            this.d = cVar.f545b.c(new c.b(cVar.a, i), A, cVar.c);
        }
        if (A != null) {
            A.close();
        }
    }

    @Override // b.f.h.a.b.b
    public synchronized CloseableReference<Bitmap> f(int i) {
        return g(CloseableReference.n(this.d));
    }
}
