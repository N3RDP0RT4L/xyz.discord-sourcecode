package b.f.h.a.b.d;

import android.graphics.Bitmap;
import b.f.h.a.b.b;
import com.facebook.common.references.CloseableReference;
/* compiled from: KeepLastFrameCache.java */
/* loaded from: classes2.dex */
public class c implements b {
    public int a = -1;

    /* renamed from: b  reason: collision with root package name */
    public CloseableReference<Bitmap> f525b;

    @Override // b.f.h.a.b.b
    public synchronized CloseableReference<Bitmap> a(int i, int i2, int i3) {
        CloseableReference<Bitmap> n;
        n = CloseableReference.n(this.f525b);
        g();
        return n;
    }

    @Override // b.f.h.a.b.b
    public void b(int i, CloseableReference<Bitmap> closeableReference, int i2) {
    }

    @Override // b.f.h.a.b.b
    public synchronized boolean c(int i) {
        boolean z2;
        if (i == this.a) {
            if (CloseableReference.y(this.f525b)) {
                z2 = true;
            }
        }
        z2 = false;
        return z2;
    }

    @Override // b.f.h.a.b.b
    public synchronized void clear() {
        g();
    }

    @Override // b.f.h.a.b.b
    public synchronized CloseableReference<Bitmap> d(int i) {
        if (this.a != i) {
            return null;
        }
        return CloseableReference.n(this.f525b);
    }

    @Override // b.f.h.a.b.b
    public synchronized void e(int i, CloseableReference<Bitmap> closeableReference, int i2) {
        if (closeableReference != null) {
            if (this.f525b != null && closeableReference.u().equals(this.f525b.u())) {
                return;
            }
        }
        CloseableReference<Bitmap> closeableReference2 = this.f525b;
        Class<CloseableReference> cls = CloseableReference.j;
        if (closeableReference2 != null) {
            closeableReference2.close();
        }
        this.f525b = CloseableReference.n(closeableReference);
        this.a = i;
    }

    @Override // b.f.h.a.b.b
    public synchronized CloseableReference<Bitmap> f(int i) {
        return CloseableReference.n(this.f525b);
    }

    public final synchronized void g() {
        CloseableReference<Bitmap> closeableReference = this.f525b;
        Class<CloseableReference> cls = CloseableReference.j;
        if (closeableReference != null) {
            closeableReference.close();
        }
        this.f525b = null;
        this.a = -1;
    }
}
