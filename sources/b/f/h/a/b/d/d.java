package b.f.h.a.b.d;

import android.graphics.Bitmap;
import b.f.h.a.b.b;
import com.facebook.common.references.CloseableReference;
/* compiled from: NoOpCache.java */
/* loaded from: classes2.dex */
public class d implements b {
    @Override // b.f.h.a.b.b
    public CloseableReference<Bitmap> a(int i, int i2, int i3) {
        return null;
    }

    @Override // b.f.h.a.b.b
    public void b(int i, CloseableReference<Bitmap> closeableReference, int i2) {
    }

    @Override // b.f.h.a.b.b
    public boolean c(int i) {
        return false;
    }

    @Override // b.f.h.a.b.b
    public void clear() {
    }

    @Override // b.f.h.a.b.b
    public CloseableReference<Bitmap> d(int i) {
        return null;
    }

    @Override // b.f.h.a.b.b
    public void e(int i, CloseableReference<Bitmap> closeableReference, int i2) {
    }

    @Override // b.f.h.a.b.b
    public CloseableReference<Bitmap> f(int i) {
        return null;
    }
}
