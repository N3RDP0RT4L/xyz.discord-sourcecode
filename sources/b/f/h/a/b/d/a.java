package b.f.h.a.b.d;

import com.facebook.cache.common.CacheKey;
/* compiled from: AnimationFrameCacheKey.java */
/* loaded from: classes2.dex */
public class a implements CacheKey {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f523b;

    public a(int i, boolean z2) {
        this.a = b.d.b.a.a.p("anim://", i);
        this.f523b = z2;
    }

    @Override // com.facebook.cache.common.CacheKey
    public boolean a() {
        return false;
    }

    @Override // com.facebook.cache.common.CacheKey
    public String b() {
        return this.a;
    }

    @Override // com.facebook.cache.common.CacheKey
    public boolean equals(Object obj) {
        if (!this.f523b) {
            return super.equals(obj);
        }
        if (this == obj) {
            return true;
        }
        if (obj == null || a.class != obj.getClass()) {
            return false;
        }
        return this.a.equals(((a) obj).a);
    }

    @Override // com.facebook.cache.common.CacheKey
    public int hashCode() {
        if (!this.f523b) {
            return super.hashCode();
        }
        return this.a.hashCode();
    }
}
