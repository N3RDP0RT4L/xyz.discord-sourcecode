package b.f.h.b.a;

import java.util.Map;
/* compiled from: ControllerListener2.java */
/* loaded from: classes2.dex */
public interface b<INFO> {

    /* compiled from: ControllerListener2.java */
    /* loaded from: classes2.dex */
    public static class a {
        public Map<String, Object> a;

        /* renamed from: b  reason: collision with root package name */
        public Map<String, Object> f534b;
        public Map<String, Object> c;
        public Map<String, Object> d;
        public Object e;
    }

    void a(String str, Object obj, a aVar);

    void b(String str, Throwable th, a aVar);

    void c(String str, a aVar);

    void d(String str, INFO info, a aVar);
}
