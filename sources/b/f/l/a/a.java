package b.f.l.a;

import b.f.l.a.b;
import b.f.l.b.c;
/* compiled from: MultiPointerGestureDetector.java */
/* loaded from: classes2.dex */
public class a {
    public boolean a;

    /* renamed from: b  reason: collision with root package name */
    public int f643b;
    public final int[] c = new int[2];
    public final float[] d = new float[2];
    public final float[] e = new float[2];
    public final float[] f = new float[2];
    public final float[] g = new float[2];
    public AbstractC0075a h = null;

    /* compiled from: MultiPointerGestureDetector.java */
    /* renamed from: b.f.l.a.a$a  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public interface AbstractC0075a {
    }

    public a() {
        a();
    }

    public void a() {
        this.a = false;
        this.f643b = 0;
        for (int i = 0; i < 2; i++) {
            this.c[i] = -1;
        }
    }

    public final void b() {
        b bVar;
        b.a aVar;
        if (!this.a) {
            AbstractC0075a aVar2 = this.h;
            if (!(aVar2 == null || (aVar = (bVar = (b) aVar2).f644b) == null)) {
                aVar.c(bVar);
            }
            this.a = true;
        }
    }

    public final void c() {
        b.a aVar;
        if (this.a) {
            this.a = false;
            AbstractC0075a aVar2 = this.h;
            if (aVar2 != null && (aVar = ((b) aVar2).f644b) != null) {
                c cVar = (c) aVar;
                b.f.d.e.a.i(c.class, "onGestureEnd");
            }
        }
    }
}
