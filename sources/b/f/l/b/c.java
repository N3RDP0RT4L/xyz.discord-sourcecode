package b.f.l.b;

import android.graphics.Matrix;
import android.graphics.RectF;
import b.f.d.e.a;
import b.f.l.a.b;
import b.f.l.b.e;
import com.discord.models.domain.ModelAuditLogEntry;
import com.facebook.samples.zoomable.ZoomableDraweeView;
/* compiled from: DefaultZoomableController.java */
/* loaded from: classes2.dex */
public class c implements e, b.a {
    public b a;

    /* renamed from: b  reason: collision with root package name */
    public e.a f647b = null;
    public boolean c = false;
    public final RectF d = new RectF();
    public final RectF e = new RectF();
    public final RectF f = new RectF();
    public final Matrix g = new Matrix();
    public final Matrix h = new Matrix();
    public final Matrix i = new Matrix();
    public final float[] j = new float[9];
    public final RectF k = new RectF();
    public boolean l;

    static {
        new RectF(0.0f, 0.0f, 1.0f, 1.0f);
    }

    public c(b bVar) {
        this.a = bVar;
        bVar.f644b = this;
    }

    public static boolean l(int i, int i2) {
        return (i & i2) != 0;
    }

    @Override // b.f.l.b.e
    public boolean a() {
        this.h.getValues(this.j);
        float[] fArr = this.j;
        fArr[0] = fArr[0] - 1.0f;
        fArr[4] = fArr[4] - 1.0f;
        fArr[8] = fArr[8] - 1.0f;
        for (int i = 0; i < 9; i++) {
            if (Math.abs(this.j[i]) > 0.001f) {
                return false;
            }
        }
        return true;
    }

    public void b(b bVar) {
        float f;
        a.i(c.class, "onGestureUpdate");
        Matrix matrix = this.h;
        b bVar2 = this.a;
        matrix.set(this.g);
        b.f.l.a.a aVar = bVar2.a;
        if (aVar.f643b < 2) {
            f = 1.0f;
        } else {
            float[] fArr = aVar.d;
            float f2 = fArr[1] - fArr[0];
            float[] fArr2 = aVar.e;
            float f3 = fArr2[1] - fArr2[0];
            float[] fArr3 = aVar.f;
            float f4 = fArr3[1] - fArr3[0];
            float[] fArr4 = aVar.g;
            float f5 = fArr4[1] - fArr4[0];
            f = ((float) Math.hypot(f4, f5)) / ((float) Math.hypot(f2, f3));
        }
        matrix.postScale(f, f, bVar2.b(), bVar2.c());
        b.f.l.a.a aVar2 = bVar2.a;
        float a = bVar2.a(aVar2.f, aVar2.f643b);
        b.f.l.a.a aVar3 = bVar2.a;
        float a2 = a - bVar2.a(aVar3.d, aVar3.f643b);
        b.f.l.a.a aVar4 = bVar2.a;
        float a3 = bVar2.a(aVar4.g, aVar4.f643b);
        b.f.l.a.a aVar5 = bVar2.a;
        matrix.postTranslate(a2, a3 - bVar2.a(aVar5.e, aVar5.f643b));
        boolean g = g(matrix, 7) | f(matrix, bVar2.b(), bVar2.c(), 7) | false;
        i();
        if (g) {
            this.a.d();
        }
        this.l = g;
    }

    public void c(b bVar) {
        a.i(c.class, "onGestureBegin");
        this.g.set(this.h);
        RectF rectF = this.f;
        float f = rectF.left;
        RectF rectF2 = this.d;
        this.l = !(f < rectF2.left - 0.001f && rectF.top < rectF2.top - 0.001f && rectF.right > rectF2.right + 0.001f && rectF.bottom > rectF2.bottom + 0.001f);
    }

    public final float d(float f, float f2, float f3, float f4, float f5) {
        float f6 = f2 - f;
        float f7 = f4 - f3;
        if (f6 < Math.min(f5 - f3, f4 - f5) * 2.0f) {
            return f5 - ((f2 + f) / 2.0f);
        }
        if (f6 < f7) {
            return f5 < (f3 + f4) / 2.0f ? f3 - f : f4 - f2;
        }
        if (f > f3) {
            return f3 - f;
        }
        if (f2 < f4) {
            return f4 - f2;
        }
        return 0.0f;
    }

    public float e() {
        this.h.getValues(this.j);
        return this.j[0];
    }

    public final boolean f(Matrix matrix, float f, float f2, int i) {
        if (!l(i, 4)) {
            return false;
        }
        matrix.getValues(this.j);
        float f3 = this.j[0];
        float min = Math.min(Math.max(1.0f, f3), 2.0f);
        if (min == f3) {
            return false;
        }
        float f4 = min / f3;
        matrix.postScale(f4, f4, f, f2);
        return true;
    }

    public final boolean g(Matrix matrix, int i) {
        float f;
        float f2;
        if (!l(i, 3)) {
            return false;
        }
        RectF rectF = this.k;
        rectF.set(this.e);
        matrix.mapRect(rectF);
        if (l(i, 1)) {
            float f3 = rectF.left;
            float f4 = rectF.right;
            RectF rectF2 = this.d;
            f = d(f3, f4, rectF2.left, rectF2.right, this.e.centerX());
        } else {
            f = 0.0f;
        }
        if (l(i, 2)) {
            float f5 = rectF.top;
            float f6 = rectF.bottom;
            RectF rectF3 = this.d;
            f2 = d(f5, f6, rectF3.top, rectF3.bottom, this.e.centerY());
        } else {
            f2 = 0.0f;
        }
        if (f == 0.0f && f2 == 0.0f) {
            return false;
        }
        matrix.postTranslate(f, f2);
        return true;
    }

    /* JADX WARN: Code restructure failed: missing block: B:12:0x0025, code lost:
        if (r2 != 6) goto L48;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public boolean h(android.view.MotionEvent r11) {
        /*
            r10 = this;
            r11.getAction()
            int r0 = b.f.d.e.a.a
            boolean r0 = r10.c
            r1 = 0
            if (r0 == 0) goto Lcb
            b.f.l.a.b r0 = r10.a
            b.f.l.a.a r0 = r0.a
            java.util.Objects.requireNonNull(r0)
            int r2 = r11.getActionMasked()
            r3 = 6
            r4 = 2
            r5 = -1
            r6 = 1
            if (r2 == 0) goto L6d
            if (r2 == r6) goto L6d
            if (r2 == r4) goto L31
            r7 = 3
            if (r2 == r7) goto L29
            r7 = 5
            if (r2 == r7) goto L6d
            if (r2 == r3) goto L6d
            goto Lca
        L29:
            r0.c()
            r0.a()
            goto Lca
        L31:
            if (r1 >= r4) goto L50
            int[] r2 = r0.c
            r2 = r2[r1]
            int r2 = r11.findPointerIndex(r2)
            if (r2 == r5) goto L4d
            float[] r3 = r0.f
            float r7 = r11.getX(r2)
            r3[r1] = r7
            float[] r3 = r0.g
            float r2 = r11.getY(r2)
            r3[r1] = r2
        L4d:
            int r1 = r1 + 1
            goto L31
        L50:
            boolean r11 = r0.a
            if (r11 != 0) goto L5b
            int r11 = r0.f643b
            if (r11 <= 0) goto L5b
            r0.b()
        L5b:
            boolean r11 = r0.a
            if (r11 == 0) goto Lca
            b.f.l.a.a$a r11 = r0.h
            if (r11 == 0) goto Lca
            b.f.l.a.b r11 = (b.f.l.a.b) r11
            b.f.l.a.b$a r0 = r11.f644b
            if (r0 == 0) goto Lca
            r0.b(r11)
            goto Lca
        L6d:
            r11.getPointerCount()
            r11.getActionMasked()
            r0.c()
            r0.f643b = r1
        L78:
            if (r1 >= r4) goto Lc3
            int r2 = r11.getPointerCount()
            int r7 = r11.getActionMasked()
            int r8 = r11.getActionIndex()
            if (r7 == r6) goto L8a
            if (r7 != r3) goto L8f
        L8a:
            if (r1 < r8) goto L8f
            int r7 = r1 + 1
            goto L90
        L8f:
            r7 = r1
        L90:
            if (r7 >= r2) goto L93
            goto L94
        L93:
            r7 = -1
        L94:
            if (r7 != r5) goto L9b
            int[] r2 = r0.c
            r2[r1] = r5
            goto Lc0
        L9b:
            int[] r2 = r0.c
            int r8 = r11.getPointerId(r7)
            r2[r1] = r8
            float[] r2 = r0.f
            float[] r8 = r0.d
            float r9 = r11.getX(r7)
            r8[r1] = r9
            r2[r1] = r9
            float[] r2 = r0.g
            float[] r8 = r0.e
            float r7 = r11.getY(r7)
            r8[r1] = r7
            r2[r1] = r7
            int r2 = r0.f643b
            int r2 = r2 + r6
            r0.f643b = r2
        Lc0:
            int r1 = r1 + 1
            goto L78
        Lc3:
            int r11 = r0.f643b
            if (r11 <= 0) goto Lca
            r0.b()
        Lca:
            return r6
        Lcb:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: b.f.l.b.c.h(android.view.MotionEvent):boolean");
    }

    public final void i() {
        this.h.mapRect(this.f, this.e);
        e.a aVar = this.f647b;
        if (aVar != null && this.c) {
            ZoomableDraweeView zoomableDraweeView = ZoomableDraweeView.this;
            zoomableDraweeView.getLogTag();
            zoomableDraweeView.hashCode();
            int i = a.a;
            if (zoomableDraweeView.n != null && ((c) zoomableDraweeView.o).e() > 1.1f) {
                zoomableDraweeView.a(zoomableDraweeView.n, null);
            }
            zoomableDraweeView.invalidate();
        }
    }

    public void j() {
        a.i(c.class, ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_RESET);
        this.a.a.a();
        this.g.reset();
        this.h.reset();
        i();
    }

    public void k(boolean z2) {
        this.c = z2;
        if (!z2) {
            j();
        }
    }
}
