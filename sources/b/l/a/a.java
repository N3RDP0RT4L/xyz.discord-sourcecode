package b.l.a;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.animation.AnimationUtils;
import androidx.annotation.IntRange;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.WorkerThread;
import androidx.vectordrawable.graphics.drawable.Animatable2Compat;
import com.linecorp.apng.decoder.Apng;
import com.linecorp.apng.decoder.ApngException;
import d0.t.k;
import d0.z.d.m;
import d0.z.d.o;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import kotlin.jvm.functions.Function0;
import org.objectweb.asm.Opcodes;
/* compiled from: ApngDrawable.kt */
/* loaded from: classes3.dex */
public final class a extends Drawable implements Animatable2Compat {
    @IntRange(from = 0, to = 2147483647L)
    public final int j;
    @IntRange(from = 1, to = 2147483647L)
    public final int k;
    public final List<Integer> l;
    @IntRange(from = 0, to = 2147483647L)
    public final int m;
    @IntRange(from = -1, to = 2147483647L)
    public int n;
    public final Paint o = new Paint(6);
    public final List<Animatable2Compat.AnimationCallback> p = new ArrayList();
    public final List<b> q = new ArrayList();
    public final int[] r;

    /* renamed from: s  reason: collision with root package name */
    public int f1896s;
    public int t;
    public boolean u;
    public long v;
    public Long w;

    /* renamed from: x  reason: collision with root package name */
    public C0161a f1897x;

    /* compiled from: ApngDrawable.kt */
    /* renamed from: b.l.a.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0161a extends Drawable.ConstantState {
        public final Apng a;

        /* renamed from: b  reason: collision with root package name */
        public final int f1898b;
        public final int c;
        public final int d;
        public final Function0<Long> e;

        /* compiled from: ApngDrawable.kt */
        /* renamed from: b.l.a.a$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0162a extends o implements Function0<Long> {
            public static final C0162a j = new C0162a();

            public C0162a() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            public Long invoke() {
                return Long.valueOf(AnimationUtils.currentAnimationTimeMillis());
            }
        }

        public C0161a(Apng apng, @IntRange(from = 1, to = 2147483647L) int i, @IntRange(from = 1, to = 2147483647L) int i2, int i3, Function0<Long> function0) {
            m.checkNotNullParameter(apng, "apng");
            m.checkNotNullParameter(function0, "currentTimeProvider");
            this.a = apng;
            this.f1898b = i;
            this.c = i2;
            this.d = i3;
            this.e = function0;
        }

        @Override // android.graphics.drawable.Drawable.ConstantState
        public int getChangingConfigurations() {
            return 0;
        }

        @Override // android.graphics.drawable.Drawable.ConstantState
        public Drawable newDrawable() {
            return new a(new C0161a(this));
        }

        /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
        public C0161a(C0161a aVar) {
            this(aVar.a.copy(), aVar.f1898b, aVar.c, aVar.d, aVar.e);
            m.checkNotNullParameter(aVar, "apngState");
        }
    }

    @VisibleForTesting
    public a(C0161a aVar) {
        m.checkNotNullParameter(aVar, "apngState");
        this.f1897x = aVar;
        this.j = aVar.a.getDuration();
        int frameCount = this.f1897x.a.getFrameCount();
        this.k = frameCount;
        this.l = k.toList(this.f1897x.a.getFrameDurations());
        this.m = this.f1897x.a.getByteCount();
        this.f1897x.a.getAllFrameByteCount();
        this.n = this.f1897x.a.getLoopCount();
        this.f1897x.a.isRecycled();
        this.r = new int[frameCount];
        C0161a aVar2 = this.f1897x;
        this.f1896s = aVar2.f1898b;
        this.t = aVar2.c;
        for (int i = 1; i < frameCount; i++) {
            int[] iArr = this.r;
            int i2 = i - 1;
            iArr[i] = iArr[i2] + this.f1897x.a.getFrameDurations()[i2];
        }
        Rect bounds = getBounds();
        C0161a aVar3 = this.f1897x;
        bounds.set(0, 0, aVar3.f1898b, aVar3.c);
    }

    @WorkerThread
    public static final a a(InputStream inputStream, @IntRange(from = 1, to = 2147483647L) Integer num, @IntRange(from = 1, to = 2147483647L) Integer num2) throws ApngException {
        m.checkNotNullParameter(inputStream, "stream");
        boolean z2 = true;
        if (!((num == null) ^ (num2 == null))) {
            if (num == null || num.intValue() > 0) {
                if (num2 != null && num2.intValue() <= 0) {
                    z2 = false;
                }
                if (z2) {
                    int i = (num == null && num2 == null) ? Opcodes.IF_ICMPNE : 0;
                    Apng decode = Apng.Companion.decode(inputStream);
                    return new a(new C0161a(decode, num != null ? num.intValue() : decode.getWidth(), num2 != null ? num2.intValue() : decode.getHeight(), i, C0161a.C0162a.j));
                }
                throw new IllegalArgumentException(("Can not specify 0 or negative as height value. height = " + num2).toString());
            }
            throw new IllegalArgumentException(("Can not specify 0 or negative as width value. width = " + num).toString());
        }
        throw new IllegalArgumentException(("Can not specify only one side of size. width = " + num + ", height = " + num2).toString());
    }

    public final boolean b() {
        return this.n != 0 && d() > this.n - 1;
    }

    public final int c() {
        int i;
        int[] iArr;
        int i2 = 0;
        long j = (this.v % this.j) + (b() ? this.j : 0);
        int i3 = this.k - 1;
        while (true) {
            i = (i2 + i3) / 2;
            int i4 = i + 1;
            if (this.r.length > i4 && j >= iArr[i4]) {
                i2 = i4;
            } else if (i2 == i3 || j >= iArr[i]) {
                break;
            } else {
                i3 = i;
            }
        }
        return i;
    }

    @Override // androidx.vectordrawable.graphics.drawable.Animatable2Compat
    public void clearAnimationCallbacks() {
        this.p.clear();
    }

    public final int d() {
        return (int) (this.v / this.j);
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        long j;
        m.checkNotNullParameter(canvas, "canvas");
        if (this.u) {
            int c = c();
            long longValue = this.f1897x.e.invoke().longValue();
            Long l = this.w;
            if (l == null) {
                j = this.v;
            } else {
                j = (this.v + longValue) - l.longValue();
            }
            this.v = j;
            this.w = Long.valueOf(longValue);
            boolean z2 = c() != c;
            if (this.u) {
                if (c() == 0) {
                    if ((d() == 0) && l == null) {
                        for (Animatable2Compat.AnimationCallback animationCallback : this.p) {
                            animationCallback.onAnimationStart(this);
                        }
                    }
                }
                if (c() == this.k - 1) {
                    if ((this.n == 0 || d() < this.n - 1) && z2) {
                        for (b bVar : this.q) {
                            bVar.b(this, d() + 2);
                            bVar.a(this, d() + 1);
                        }
                    }
                }
            }
            if (b()) {
                this.u = false;
                for (Animatable2Compat.AnimationCallback animationCallback2 : this.p) {
                    animationCallback2.onAnimationEnd(this);
                }
            }
        }
        Apng apng = this.f1897x.a;
        int c2 = c();
        Rect bounds = getBounds();
        m.checkNotNullExpressionValue(bounds, "bounds");
        apng.drawWithIndex(c2, canvas, null, bounds, this.o);
        if (this.u) {
            invalidateSelf();
        }
    }

    @Override // android.graphics.drawable.Drawable
    public Drawable.ConstantState getConstantState() {
        return this.f1897x;
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        return this.t;
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        return this.f1896s;
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -2;
    }

    @Override // android.graphics.drawable.Animatable
    public boolean isRunning() {
        return this.u;
    }

    @Override // android.graphics.drawable.Drawable
    public Drawable mutate() {
        this.f1897x = new C0161a(this.f1897x);
        return this;
    }

    @Override // androidx.vectordrawable.graphics.drawable.Animatable2Compat
    public void registerAnimationCallback(Animatable2Compat.AnimationCallback animationCallback) {
        m.checkNotNullParameter(animationCallback, "callback");
        this.p.add(animationCallback);
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        this.o.setAlpha(i);
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        this.o.setColorFilter(colorFilter);
    }

    @Override // android.graphics.drawable.Animatable
    public void start() {
        this.u = true;
        this.w = null;
        invalidateSelf();
    }

    @Override // android.graphics.drawable.Animatable
    public void stop() {
        this.u = false;
        invalidateSelf();
    }

    @Override // androidx.vectordrawable.graphics.drawable.Animatable2Compat
    public boolean unregisterAnimationCallback(Animatable2Compat.AnimationCallback animationCallback) {
        m.checkNotNullParameter(animationCallback, "callback");
        return this.p.remove(animationCallback);
    }
}
