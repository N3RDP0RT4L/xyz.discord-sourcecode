package b.d.a.a;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import androidx.annotation.AnyThread;
import androidx.annotation.Nullable;
import b.i.a.f.h.n.a;
import b.i.a.f.h.n.d;
import b.i.a.f.h.n.k;
import b.i.a.f.h.n.l;
import b.i.a.f.h.n.m;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ProxyBillingActivity;
import com.android.billingclient.api.SkuDetails;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONException;
import org.json.JSONObject;
/* compiled from: com.android.billingclient:billing@@4.0.0 */
/* loaded from: classes.dex */
public class a extends BillingClient {
    public volatile int a;

    /* renamed from: b  reason: collision with root package name */
    public final String f444b;
    public final Handler c;
    public volatile s d;
    public Context e;
    public volatile d f;
    public volatile n g;
    public boolean h;
    public int i;
    public boolean j;
    public boolean k;
    public boolean l;
    public boolean m;
    public boolean n;
    public boolean o;
    public boolean p;
    public ExecutorService q;

    @AnyThread
    public a(@Nullable String str, boolean z2, Context context, g gVar) {
        String str2;
        try {
            str2 = (String) Class.forName("b.d.a.b.a").getField("VERSION_NAME").get(null);
        } catch (Exception unused) {
            str2 = "4.0.0";
        }
        this.a = 0;
        this.c = new Handler(Looper.getMainLooper());
        this.i = 0;
        this.f444b = str2;
        Context applicationContext = context.getApplicationContext();
        this.e = applicationContext;
        this.d = new s(applicationContext, gVar);
        this.p = z2;
    }

    @Override // com.android.billingclient.api.BillingClient
    public final void a(final d dVar, final e eVar) {
        if (!d()) {
            eVar.onConsumeResponse(p.j, dVar.a);
        } else if (l(new Callable() { // from class: b.d.a.a.t
            @Override // java.util.concurrent.Callable
            public final Object call() {
                int i;
                String str;
                a aVar = a.this;
                d dVar2 = dVar;
                e eVar2 = eVar;
                Objects.requireNonNull(aVar);
                String str2 = dVar2.a;
                try {
                    String valueOf = String.valueOf(str2);
                    a.e("BillingClient", valueOf.length() != 0 ? "Consuming purchase with token: ".concat(valueOf) : new String("Consuming purchase with token: "));
                    if (aVar.k) {
                        d dVar3 = aVar.f;
                        String packageName = aVar.e.getPackageName();
                        boolean z2 = aVar.k;
                        String str3 = aVar.f444b;
                        Bundle bundle = new Bundle();
                        if (z2) {
                            bundle.putString("playBillingLibraryVersion", str3);
                        }
                        Bundle r = dVar3.r(9, packageName, str2, bundle);
                        i = r.getInt("RESPONSE_CODE");
                        str = a.d(r, "BillingClient");
                    } else {
                        i = aVar.f.q(3, aVar.e.getPackageName(), str2);
                        str = "";
                    }
                    BillingResult billingResult = new BillingResult();
                    billingResult.a = i;
                    billingResult.f1999b = str;
                    if (i == 0) {
                        a.e("BillingClient", "Successfully consumed purchase.");
                        eVar2.onConsumeResponse(billingResult, str2);
                        return null;
                    }
                    StringBuilder sb = new StringBuilder(63);
                    sb.append("Error consuming purchase with token. Response code: ");
                    sb.append(i);
                    a.f("BillingClient", sb.toString());
                    eVar2.onConsumeResponse(billingResult, str2);
                    return null;
                } catch (Exception e) {
                    String valueOf2 = String.valueOf(e);
                    StringBuilder sb2 = new StringBuilder(valueOf2.length() + 30);
                    sb2.append("Error consuming purchase; ex: ");
                    sb2.append(valueOf2);
                    a.f("BillingClient", sb2.toString());
                    eVar2.onConsumeResponse(p.j, str2);
                    return null;
                }
            }
        }, 30000L, new Runnable() { // from class: b.d.a.a.x
            @Override // java.lang.Runnable
            public final void run() {
                e.this.onConsumeResponse(p.k, dVar.a);
            }
        }, i()) == null) {
            eVar.onConsumeResponse(k(), dVar.a);
        }
    }

    @Override // com.android.billingclient.api.BillingClient
    public final void b() {
        try {
            this.d.a();
            if (this.g != null) {
                n nVar = this.g;
                synchronized (nVar.j) {
                    nVar.l = null;
                    nVar.k = true;
                }
            }
            if (!(this.g == null || this.f == null)) {
                b.i.a.f.h.n.a.e("BillingClient", "Unbinding from service.");
                this.e.unbindService(this.g);
                this.g = null;
            }
            this.f = null;
            ExecutorService executorService = this.q;
            if (executorService != null) {
                executorService.shutdownNow();
                this.q = null;
            }
        } catch (Exception e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(valueOf.length() + 48);
            sb.append("There was an exception while ending connection: ");
            sb.append(valueOf);
            b.i.a.f.h.n.a.f("BillingClient", sb.toString());
        } finally {
            this.a = 3;
        }
    }

    @Override // com.android.billingclient.api.BillingClient
    public final int c() {
        return this.a;
    }

    @Override // com.android.billingclient.api.BillingClient
    public final boolean d() {
        return (this.a != 2 || this.f == null || this.g == null) ? false : true;
    }

    @Override // com.android.billingclient.api.BillingClient
    public final BillingResult e(Activity activity, final BillingFlowParams billingFlowParams) {
        String str;
        String str2;
        String str3;
        String str4;
        Future future;
        String str5;
        String str6;
        String str7;
        Bundle bundle;
        boolean z2;
        String str8;
        final int i;
        if (!d()) {
            BillingResult billingResult = p.j;
            j(billingResult);
            return billingResult;
        }
        Objects.requireNonNull(billingFlowParams);
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(billingFlowParams.f);
        final SkuDetails skuDetails = (SkuDetails) arrayList.get(0);
        final String e = skuDetails.e();
        String str9 = "BillingClient";
        if (e.equals("subs") && !this.h) {
            b.i.a.f.h.n.a.f(str9, "Current client doesn't support subscriptions.");
            BillingResult billingResult2 = p.l;
            j(billingResult2);
            return billingResult2;
        } else if (((!billingFlowParams.g && billingFlowParams.f1997b == null && billingFlowParams.d == null && billingFlowParams.e == 0 && !billingFlowParams.a) ? false : true) && !this.j) {
            b.i.a.f.h.n.a.f(str9, "Current client doesn't support extra params for buy intent.");
            BillingResult billingResult3 = p.f;
            j(billingResult3);
            return billingResult3;
        } else if (arrayList.size() <= 1 || this.o) {
            String str10 = "";
            String str11 = str10;
            for (int i2 = 0; i2 < arrayList.size(); i2++) {
                String valueOf = String.valueOf(str11);
                String valueOf2 = String.valueOf(arrayList.get(i2));
                str10 = str10;
                String H = b.d.b.a.a.H(new StringBuilder(valueOf.length() + valueOf2.length()), valueOf, valueOf2);
                if (i2 < arrayList.size() - 1) {
                    H = String.valueOf(H).concat(", ");
                }
                str11 = H;
            }
            String str12 = str10;
            StringBuilder sb = new StringBuilder(String.valueOf(str11).length() + 41 + e.length());
            sb.append("Constructing buy intent for ");
            sb.append(str11);
            sb.append(", item type: ");
            sb.append(e);
            b.i.a.f.h.n.a.e(str9, sb.toString());
            if (this.j) {
                boolean z3 = this.k;
                boolean z4 = this.p;
                String str13 = this.f444b;
                final Bundle bundle2 = new Bundle();
                bundle2.putString("playBillingLibraryVersion", str13);
                int i3 = billingFlowParams.e;
                if (i3 != 0) {
                    bundle2.putInt("prorationMode", i3);
                }
                if (!TextUtils.isEmpty(billingFlowParams.f1997b)) {
                    bundle2.putString("accountId", billingFlowParams.f1997b);
                }
                if (!TextUtils.isEmpty(billingFlowParams.d)) {
                    bundle2.putString("obfuscatedProfileId", billingFlowParams.d);
                }
                if (billingFlowParams.g) {
                    bundle2.putBoolean("vr", true);
                }
                if (!TextUtils.isEmpty(null)) {
                    bundle2.putStringArrayList("skusToReplace", new ArrayList<>(Arrays.asList(null)));
                }
                if (!TextUtils.isEmpty(billingFlowParams.c)) {
                    bundle2.putString("oldSkuPurchaseToken", billingFlowParams.c);
                }
                if (!TextUtils.isEmpty(null)) {
                    bundle2.putString("oldSkuPurchaseId", null);
                }
                if (!TextUtils.isEmpty(null)) {
                    bundle2.putString("paymentsPurchaseParams", null);
                }
                if (z3 && z4) {
                    bundle2.putBoolean("enablePendingPurchases", true);
                }
                ArrayList<String> arrayList2 = new ArrayList<>();
                ArrayList<String> arrayList3 = new ArrayList<>();
                ArrayList<String> arrayList4 = new ArrayList<>();
                ArrayList<Integer> arrayList5 = new ArrayList<>();
                str4 = "; try to reconnect";
                ArrayList<String> arrayList6 = new ArrayList<>();
                str3 = str11;
                int size = arrayList.size();
                boolean z5 = false;
                boolean z6 = false;
                boolean z7 = false;
                boolean z8 = false;
                str = "BUY_INTENT";
                for (int i4 = 0; i4 < size; i4++) {
                    size = size;
                    SkuDetails skuDetails2 = (SkuDetails) arrayList.get(i4);
                    str9 = str9;
                    if (!skuDetails2.f2002b.optString("skuDetailsToken").isEmpty()) {
                        arrayList2.add(skuDetails2.f2002b.optString("skuDetailsToken"));
                    }
                    try {
                        str12 = new JSONObject(skuDetails2.a).optString("offer_id_token");
                    } catch (JSONException unused) {
                    }
                    e = e;
                    String optString = skuDetails2.f2002b.optString("offer_id");
                    int optInt = skuDetails2.f2002b.optInt("offer_type");
                    String optString2 = skuDetails2.f2002b.optString("serializedDocid");
                    arrayList3.add(str12);
                    z5 |= !TextUtils.isEmpty(str12);
                    arrayList4.add(optString);
                    z6 |= !TextUtils.isEmpty(optString);
                    arrayList5.add(Integer.valueOf(optInt));
                    z7 |= optInt != 0;
                    z8 |= !TextUtils.isEmpty(optString2);
                    arrayList6.add(optString2);
                }
                final String str14 = e;
                str2 = str9;
                if (!arrayList2.isEmpty()) {
                    bundle2.putStringArrayList("skuDetailsTokens", arrayList2);
                }
                if (z5) {
                    if (!this.m) {
                        BillingResult billingResult4 = p.g;
                        j(billingResult4);
                        return billingResult4;
                    }
                    bundle2.putStringArrayList("SKU_OFFER_ID_TOKEN_LIST", arrayList3);
                }
                if (z6) {
                    bundle2.putStringArrayList("SKU_OFFER_ID_LIST", arrayList4);
                }
                if (z7) {
                    bundle2.putIntegerArrayList("SKU_OFFER_TYPE_LIST", arrayList5);
                }
                if (z8) {
                    bundle2.putStringArrayList("SKU_SERIALIZED_DOCID_LIST", arrayList6);
                }
                if (!TextUtils.isEmpty(skuDetails.f())) {
                    bundle2.putString("skuPackageName", skuDetails.f());
                    str8 = null;
                    z2 = true;
                } else {
                    str8 = null;
                    z2 = false;
                }
                if (!TextUtils.isEmpty(str8)) {
                    bundle2.putString("accountName", str8);
                }
                if (arrayList.size() > 1) {
                    ArrayList<String> arrayList7 = new ArrayList<>(arrayList.size() - 1);
                    ArrayList<String> arrayList8 = new ArrayList<>(arrayList.size() - 1);
                    for (int i5 = 1; i5 < arrayList.size(); i5++) {
                        arrayList7.add(((SkuDetails) arrayList.get(i5)).d());
                        arrayList8.add(((SkuDetails) arrayList.get(i5)).e());
                    }
                    bundle2.putStringArrayList("additionalSkus", arrayList7);
                    bundle2.putStringArrayList("additionalSkuTypes", arrayList8);
                }
                if (!TextUtils.isEmpty(activity.getIntent().getStringExtra("PROXY_PACKAGE"))) {
                    String stringExtra = activity.getIntent().getStringExtra("PROXY_PACKAGE");
                    bundle2.putString("proxyPackage", stringExtra);
                    try {
                        bundle2.putString("proxyPackageVersion", this.e.getPackageManager().getPackageInfo(stringExtra, 0).versionName);
                    } catch (PackageManager.NameNotFoundException unused2) {
                        bundle2.putString("proxyPackageVersion", "package not found");
                    }
                }
                if (this.n && z2) {
                    i = 15;
                } else if (this.k) {
                    i = 9;
                } else {
                    i = billingFlowParams.g ? 7 : 6;
                }
                future = l(new Callable() { // from class: b.d.a.a.b0
                    @Override // java.util.concurrent.Callable
                    public final Object call() {
                        a aVar = a.this;
                        int i6 = i;
                        SkuDetails skuDetails3 = skuDetails;
                        return aVar.f.c0(i6, aVar.e.getPackageName(), skuDetails3.d(), str14, null, bundle2);
                    }
                }, 5000L, null, this.c);
            } else {
                str = "BUY_INTENT";
                str4 = "; try to reconnect";
                str2 = str9;
                str3 = str11;
                future = l(new Callable() { // from class: b.d.a.a.u
                    @Override // java.util.concurrent.Callable
                    public final Object call() {
                        a aVar = a.this;
                        SkuDetails skuDetails3 = skuDetails;
                        return aVar.f.F(3, aVar.e.getPackageName(), skuDetails3.d(), e, null);
                    }
                }, 5000L, null, this.c);
            }
            try {
                try {
                    try {
                        bundle = (Bundle) future.get(5000L, TimeUnit.MILLISECONDS);
                        str6 = str2;
                    } catch (CancellationException | TimeoutException unused3) {
                        str6 = str2;
                    }
                } catch (Exception unused4) {
                    str6 = str2;
                }
            } catch (CancellationException | TimeoutException unused5) {
                str7 = str4;
                str5 = str3;
                str6 = str2;
            }
            try {
                int a = b.i.a.f.h.n.a.a(bundle, str6);
                String d = b.i.a.f.h.n.a.d(bundle, str6);
                if (a != 0) {
                    StringBuilder sb2 = new StringBuilder(52);
                    sb2.append("Unable to buy item, Error response code: ");
                    sb2.append(a);
                    b.i.a.f.h.n.a.f(str6, sb2.toString());
                    BillingResult billingResult5 = new BillingResult();
                    billingResult5.a = a;
                    billingResult5.f1999b = d;
                    j(billingResult5);
                    return billingResult5;
                }
                Intent intent = new Intent(activity, ProxyBillingActivity.class);
                String str15 = str;
                intent.putExtra(str15, (PendingIntent) bundle.getParcelable(str15));
                activity.startActivity(intent);
                return p.i;
            } catch (CancellationException | TimeoutException unused6) {
                str7 = str4;
                str5 = str3;
                StringBuilder sb3 = new StringBuilder(String.valueOf(str5).length() + 68);
                sb3.append("Time out while launching billing flow: ; for sku: ");
                sb3.append(str5);
                sb3.append(str7);
                b.i.a.f.h.n.a.f(str6, sb3.toString());
                BillingResult billingResult6 = p.k;
                j(billingResult6);
                return billingResult6;
            } catch (Exception unused7) {
                StringBuilder sb4 = new StringBuilder(String.valueOf(str3).length() + 69);
                sb4.append("Exception while launching billing flow: ; for sku: ");
                sb4.append(str3);
                sb4.append(str4);
                b.i.a.f.h.n.a.f(str6, sb4.toString());
                BillingResult billingResult7 = p.j;
                j(billingResult7);
                return billingResult7;
            }
        } else {
            b.i.a.f.h.n.a.f(str9, "Current client doesn't support multi-item purchases.");
            BillingResult billingResult8 = p.m;
            j(billingResult8);
            return billingResult8;
        }
    }

    @Override // com.android.billingclient.api.BillingClient
    public void f(String str, final f fVar) {
        if (!d()) {
            BillingResult billingResult = p.j;
            m<Object> mVar = k.k;
            fVar.onQueryPurchasesResponse(billingResult, l.l);
        } else if (TextUtils.isEmpty(str)) {
            b.i.a.f.h.n.a.f("BillingClient", "Please provide a valid SKU type.");
            BillingResult billingResult2 = p.e;
            m<Object> mVar2 = k.k;
            fVar.onQueryPurchasesResponse(billingResult2, l.l);
        } else if (l(new j(this, str, fVar), 30000L, new Runnable() { // from class: b.d.a.a.y
            @Override // java.lang.Runnable
            public final void run() {
                f fVar2 = f.this;
                BillingResult billingResult3 = p.k;
                m<Object> mVar3 = k.k;
                fVar2.onQueryPurchasesResponse(billingResult3, l.l);
            }
        }, i()) == null) {
            BillingResult k = k();
            m<Object> mVar3 = k.k;
            fVar.onQueryPurchasesResponse(k, l.l);
        }
    }

    @Override // com.android.billingclient.api.BillingClient
    public final void g(h hVar, final i iVar) {
        if (!d()) {
            iVar.onSkuDetailsResponse(p.j, null);
            return;
        }
        final String str = hVar.a;
        List<String> list = hVar.f446b;
        if (TextUtils.isEmpty(str)) {
            b.i.a.f.h.n.a.f("BillingClient", "Please fix the input params. SKU type can't be empty.");
            iVar.onSkuDetailsResponse(p.e, null);
        } else if (list != null) {
            final ArrayList arrayList = new ArrayList();
            for (String str2 : list) {
                if (!TextUtils.isEmpty(str2)) {
                    arrayList.add(new q(str2));
                } else {
                    throw new IllegalArgumentException("SKU must be set.");
                }
            }
            if (l(new Callable() { // from class: b.d.a.a.v
                /* JADX WARN: Code restructure failed: missing block: B:27:0x00cd, code lost:
                    r14 = 4;
                    r0 = "Item is unavailable for purchase.";
                 */
                @Override // java.util.concurrent.Callable
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct add '--show-bad-code' argument
                */
                public final java.lang.Object call() {
                    /*
                        Method dump skipped, instructions count: 332
                        To view this dump add '--comments-level debug' option
                    */
                    throw new UnsupportedOperationException("Method not decompiled: b.d.a.a.v.call():java.lang.Object");
                }
            }, 30000L, new Runnable() { // from class: b.d.a.a.z
                @Override // java.lang.Runnable
                public final void run() {
                    i.this.onSkuDetailsResponse(p.k, null);
                }
            }, i()) == null) {
                iVar.onSkuDetailsResponse(k(), null);
            }
        } else {
            b.i.a.f.h.n.a.f("BillingClient", "Please fix the input params. The list of SKUs can't be empty - set SKU list or SkuWithOffer list.");
            iVar.onSkuDetailsResponse(p.d, null);
        }
    }

    @Override // com.android.billingclient.api.BillingClient
    public final void h(b bVar) {
        ServiceInfo serviceInfo;
        if (d()) {
            b.i.a.f.h.n.a.e("BillingClient", "Service connection is valid. No need to re-initialize.");
            bVar.onBillingSetupFinished(p.i);
        } else if (this.a == 1) {
            b.i.a.f.h.n.a.f("BillingClient", "Client is already in the process of connecting to billing service.");
            bVar.onBillingSetupFinished(p.c);
        } else if (this.a == 3) {
            b.i.a.f.h.n.a.f("BillingClient", "Client was already closed and can't be reused. Please create another instance.");
            bVar.onBillingSetupFinished(p.j);
        } else {
            this.a = 1;
            s sVar = this.d;
            r rVar = sVar.f449b;
            Context context = sVar.a;
            IntentFilter intentFilter = new IntentFilter("com.android.vending.billing.PURCHASES_UPDATED");
            if (!rVar.f448b) {
                context.registerReceiver(rVar.c.f449b, intentFilter);
                rVar.f448b = true;
            }
            b.i.a.f.h.n.a.e("BillingClient", "Starting in-app billing setup.");
            this.g = new n(this, bVar);
            Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
            intent.setPackage("com.android.vending");
            List<ResolveInfo> queryIntentServices = this.e.getPackageManager().queryIntentServices(intent, 0);
            if (!(queryIntentServices == null || queryIntentServices.isEmpty() || (serviceInfo = queryIntentServices.get(0).serviceInfo) == null)) {
                String str = serviceInfo.packageName;
                String str2 = serviceInfo.name;
                if (!"com.android.vending".equals(str) || str2 == null) {
                    b.i.a.f.h.n.a.f("BillingClient", "The device doesn't have valid Play Store.");
                } else {
                    ComponentName componentName = new ComponentName(str, str2);
                    Intent intent2 = new Intent(intent);
                    intent2.setComponent(componentName);
                    intent2.putExtra("playBillingLibraryVersion", this.f444b);
                    if (this.e.bindService(intent2, this.g, 1)) {
                        b.i.a.f.h.n.a.e("BillingClient", "Service was bonded successfully.");
                        return;
                    }
                    b.i.a.f.h.n.a.f("BillingClient", "Connection to Billing service is blocked.");
                }
            }
            this.a = 0;
            b.i.a.f.h.n.a.e("BillingClient", "Billing service unavailable on device.");
            bVar.onBillingSetupFinished(p.f447b);
        }
    }

    public final Handler i() {
        return Looper.myLooper() == null ? this.c : new Handler(Looper.myLooper());
    }

    public final BillingResult j(final BillingResult billingResult) {
        if (Thread.interrupted()) {
            return billingResult;
        }
        this.c.post(new Runnable() { // from class: b.d.a.a.w
            @Override // java.lang.Runnable
            public final void run() {
                a aVar = a.this;
                aVar.d.f449b.a.onPurchasesUpdated(billingResult, null);
            }
        });
        return billingResult;
    }

    public final BillingResult k() {
        if (this.a == 0 || this.a == 3) {
            return p.j;
        }
        return p.h;
    }

    @Nullable
    public final <T> Future<T> l(Callable<T> callable, long j, @Nullable final Runnable runnable, Handler handler) {
        long j2 = (long) (j * 0.95d);
        if (this.q == null) {
            this.q = Executors.newFixedThreadPool(b.i.a.f.h.n.a.a, new k(this));
        }
        try {
            final Future<T> submit = this.q.submit(callable);
            handler.postDelayed(new Runnable() { // from class: b.d.a.a.a0
                @Override // java.lang.Runnable
                public final void run() {
                    Future future = submit;
                    Runnable runnable2 = runnable;
                    if (!future.isDone() && !future.isCancelled()) {
                        future.cancel(true);
                        a.f("BillingClient", "Async task is taking too long, cancel it!");
                        if (runnable2 != null) {
                            runnable2.run();
                        }
                    }
                }
            }, j2);
            return submit;
        } catch (Exception e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(valueOf.length() + 28);
            sb.append("Async task throws exception ");
            sb.append(valueOf);
            b.i.a.f.h.n.a.f("BillingClient", sb.toString());
            return null;
        }
    }
}
