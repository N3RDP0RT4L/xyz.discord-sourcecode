package b.d.a.a;

import android.content.Context;
import b.i.a.f.h.n.a;
/* compiled from: com.android.billingclient:billing@@4.0.0 */
/* loaded from: classes.dex */
public final class s {
    public final Context a;

    /* renamed from: b  reason: collision with root package name */
    public final r f449b;

    public s(Context context, g gVar) {
        this.a = context;
        this.f449b = new r(this, gVar);
    }

    public final void a() {
        r rVar = this.f449b;
        Context context = this.a;
        if (rVar.f448b) {
            context.unregisterReceiver(rVar.c.f449b);
            rVar.f448b = false;
            return;
        }
        a.f("BillingBroadcastManager", "Receiver is not registered.");
    }
}
