package b.d.a.a;

import android.os.Bundle;
import android.text.TextUtils;
import b.i.a.f.h.n.a;
import b.i.a.f.h.n.k;
import b.i.a.f.h.n.l;
import b.i.a.f.h.n.m;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import org.json.JSONException;
/* compiled from: com.android.billingclient:billing@@4.0.0 */
/* loaded from: classes.dex */
public final class j implements Callable<Void> {
    public final /* synthetic */ String j;
    public final /* synthetic */ f k;
    public final /* synthetic */ a l;

    public j(a aVar, String str, f fVar) {
        this.l = aVar;
        this.j = str;
        this.k = fVar;
    }

    @Override // java.util.concurrent.Callable
    public final Void call() throws Exception {
        Purchase.a aVar;
        Bundle bundle;
        a aVar2 = this.l;
        String str = this.j;
        String valueOf = String.valueOf(str);
        a.e("BillingClient", valueOf.length() != 0 ? "Querying owned items, item type: ".concat(valueOf) : new String("Querying owned items, item type: "));
        ArrayList arrayList = new ArrayList();
        boolean z2 = aVar2.k;
        boolean z3 = aVar2.p;
        String str2 = aVar2.f444b;
        Bundle bundle2 = new Bundle();
        bundle2.putString("playBillingLibraryVersion", str2);
        int i = 1;
        if (z2 && z3) {
            bundle2.putBoolean("enablePendingPurchases", true);
        }
        String str3 = null;
        while (true) {
            try {
                if (aVar2.k) {
                    bundle = aVar2.f.o0(9, aVar2.e.getPackageName(), str, str3, bundle2);
                } else {
                    bundle = aVar2.f.E(3, aVar2.e.getPackageName(), str, str3);
                }
                BillingResult billingResult = p.h;
                if (bundle == null) {
                    Object[] objArr = new Object[i];
                    objArr[0] = "getPurchase()";
                    a.f("BillingClient", String.format("%s got null owned items list", objArr));
                } else {
                    int a = a.a(bundle, "BillingClient");
                    String d = a.d(bundle, "BillingClient");
                    BillingResult billingResult2 = new BillingResult();
                    billingResult2.a = a;
                    billingResult2.f1999b = d;
                    if (a != 0) {
                        a.f("BillingClient", String.format("%s failed. Response code: %s", "getPurchase()", Integer.valueOf(a)));
                        billingResult = billingResult2;
                        i = 1;
                    } else if (!bundle.containsKey("INAPP_PURCHASE_ITEM_LIST") || !bundle.containsKey("INAPP_PURCHASE_DATA_LIST") || !bundle.containsKey("INAPP_DATA_SIGNATURE_LIST")) {
                        i = 1;
                        a.f("BillingClient", String.format("Bundle returned from %s doesn't contain required fields.", "getPurchase()"));
                    } else {
                        ArrayList<String> stringArrayList = bundle.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
                        ArrayList<String> stringArrayList2 = bundle.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                        ArrayList<String> stringArrayList3 = bundle.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
                        if (stringArrayList == null) {
                            i = 1;
                            a.f("BillingClient", String.format("Bundle returned from %s contains null SKUs list.", "getPurchase()"));
                        } else {
                            i = 1;
                            if (stringArrayList2 == null) {
                                a.f("BillingClient", String.format("Bundle returned from %s contains null purchases list.", "getPurchase()"));
                            } else if (stringArrayList3 == null) {
                                a.f("BillingClient", String.format("Bundle returned from %s contains null signatures list.", "getPurchase()"));
                            } else {
                                billingResult = p.i;
                            }
                        }
                    }
                }
                if (billingResult != p.i) {
                    aVar = new Purchase.a(billingResult, null);
                    break;
                }
                ArrayList<String> stringArrayList4 = bundle.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
                ArrayList<String> stringArrayList5 = bundle.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                ArrayList<String> stringArrayList6 = bundle.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
                for (int i2 = 0; i2 < stringArrayList5.size(); i2++) {
                    String str4 = stringArrayList5.get(i2);
                    String str5 = stringArrayList6.get(i2);
                    String valueOf2 = String.valueOf(stringArrayList4.get(i2));
                    a.e("BillingClient", valueOf2.length() != 0 ? "Sku is owned: ".concat(valueOf2) : new String("Sku is owned: "));
                    try {
                        Purchase purchase = new Purchase(str4, str5);
                        if (TextUtils.isEmpty(purchase.a())) {
                            a.f("BillingClient", "BUG: empty/null token!");
                        }
                        arrayList.add(purchase);
                    } catch (JSONException e) {
                        String valueOf3 = String.valueOf(e);
                        StringBuilder sb = new StringBuilder(valueOf3.length() + 48);
                        sb.append("Got an exception trying to decode the purchase: ");
                        sb.append(valueOf3);
                        a.f("BillingClient", sb.toString());
                        aVar = new Purchase.a(p.h, null);
                    }
                }
                str3 = bundle.getString("INAPP_CONTINUATION_TOKEN");
                String valueOf4 = String.valueOf(str3);
                a.e("BillingClient", valueOf4.length() != 0 ? "Continuation token: ".concat(valueOf4) : new String("Continuation token: "));
                if (TextUtils.isEmpty(str3)) {
                    aVar = new Purchase.a(p.i, arrayList);
                    break;
                }
            } catch (Exception e2) {
                String valueOf5 = String.valueOf(e2);
                StringBuilder sb2 = new StringBuilder(valueOf5.length() + 57);
                sb2.append("Got exception trying to get purchases: ");
                sb2.append(valueOf5);
                sb2.append("; try to reconnect");
                a.f("BillingClient", sb2.toString());
                aVar = new Purchase.a(p.j, null);
            }
        }
        List<Purchase> list = aVar.a;
        if (list != null) {
            this.k.onQueryPurchasesResponse(aVar.f2001b, list);
            return null;
        }
        f fVar = this.k;
        BillingResult billingResult3 = aVar.f2001b;
        m<Object> mVar = k.k;
        fVar.onQueryPurchasesResponse(billingResult3, l.l);
        return null;
    }
}
