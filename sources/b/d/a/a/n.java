package b.d.a.a;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.IInterface;
import b.i.a.f.h.n.a;
import b.i.a.f.h.n.b;
import b.i.a.f.h.n.c;
import b.i.a.f.h.n.d;
import com.android.billingclient.api.BillingResult;
import java.util.concurrent.Callable;
/* compiled from: com.android.billingclient:billing@@4.0.0 */
/* loaded from: classes.dex */
public final class n implements ServiceConnection {
    public final Object j = new Object();
    public boolean k = false;
    public b l;
    public final /* synthetic */ a m;

    public /* synthetic */ n(a aVar, b bVar) {
        this.m = aVar;
        this.l = bVar;
    }

    public final void a(BillingResult billingResult) {
        synchronized (this.j) {
            b bVar = this.l;
            if (bVar != null) {
                bVar.onBillingSetupFinished(billingResult);
            }
        }
    }

    @Override // android.content.ServiceConnection
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        d dVar;
        a.e("BillingClient", "Billing service connected.");
        a aVar = this.m;
        int i = c.a;
        if (iBinder == null) {
            dVar = null;
        } else {
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.android.vending.billing.IInAppBillingService");
            if (queryLocalInterface instanceof d) {
                dVar = (d) queryLocalInterface;
            } else {
                dVar = new b(iBinder);
            }
        }
        aVar.f = dVar;
        a aVar2 = this.m;
        if (aVar2.l(new Callable() { // from class: b.d.a.a.m
            /* JADX WARN: Removed duplicated region for block: B:64:0x00c6  */
            /* JADX WARN: Removed duplicated region for block: B:65:0x00cc  */
            @Override // java.util.concurrent.Callable
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public final java.lang.Object call() {
                /*
                    Method dump skipped, instructions count: 213
                    To view this dump add '--comments-level debug' option
                */
                throw new UnsupportedOperationException("Method not decompiled: b.d.a.a.m.call():java.lang.Object");
            }
        }, 30000L, new Runnable() { // from class: b.d.a.a.l
            @Override // java.lang.Runnable
            public final void run() {
                n nVar = n.this;
                nVar.m.a = 0;
                nVar.m.f = null;
                nVar.a(p.k);
            }
        }, aVar2.i()) == null) {
            a(this.m.k());
        }
    }

    @Override // android.content.ServiceConnection
    public final void onServiceDisconnected(ComponentName componentName) {
        a.f("BillingClient", "Billing service disconnected.");
        this.m.f = null;
        this.m.a = 0;
        synchronized (this.j) {
            b bVar = this.l;
            if (bVar != null) {
                bVar.onBillingServiceDisconnected();
            }
        }
    }
}
