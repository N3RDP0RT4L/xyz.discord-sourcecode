package b.b.a.a;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.tabs.TabLayout;
import com.lytefast.flexinput.fragment.FlexInputFragment;
import com.lytefast.flexinput.viewmodel.FlexInputViewModel;
import d0.z.d.m;
/* compiled from: AddContentDialogFragment.kt */
/* loaded from: classes3.dex */
public final class b implements TabLayout.OnTabSelectedListener {
    public final /* synthetic */ a a;

    public b(a aVar) {
        this.a = aVar;
    }

    @Override // com.google.android.material.tabs.TabLayout.BaseOnTabSelectedListener
    public void onTabReselected(TabLayout.Tab tab) {
        m.checkNotNullParameter(tab, "tab");
    }

    @Override // com.google.android.material.tabs.TabLayout.BaseOnTabSelectedListener
    public void onTabSelected(TabLayout.Tab tab) {
        FlexInputViewModel flexInputViewModel;
        m.checkNotNullParameter(tab, "tab");
        int position = tab.getPosition();
        if (position == 3) {
            this.a.h(false);
            Fragment parentFragment = this.a.getParentFragment();
            if ((parentFragment instanceof FlexInputFragment) && (flexInputViewModel = ((FlexInputFragment) parentFragment).f3137s) != null) {
                flexInputViewModel.onCreateThreadSelected();
                return;
            }
            return;
        }
        ViewPager viewPager = this.a.k;
        if (viewPager != null) {
            viewPager.setCurrentItem(position);
        }
    }

    @Override // com.google.android.material.tabs.TabLayout.BaseOnTabSelectedListener
    public void onTabUnselected(TabLayout.Tab tab) {
        m.checkNotNullParameter(tab, "tab");
    }
}
