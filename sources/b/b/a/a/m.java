package b.b.a.a;

import com.lytefast.flexinput.fragment.FlexInputFragment;
import com.lytefast.flexinput.model.Attachment;
import com.lytefast.flexinput.utils.SelectionCoordinator;
import com.lytefast.flexinput.viewmodel.FlexInputViewModel;
/* compiled from: FlexInputFragment.kt */
/* loaded from: classes3.dex */
public final class m implements SelectionCoordinator.ItemSelectionListener<Attachment<? extends Object>> {
    public final /* synthetic */ FlexInputFragment a;

    public m(FlexInputFragment flexInputFragment) {
        this.a = flexInputFragment;
    }

    @Override // com.lytefast.flexinput.utils.SelectionCoordinator.ItemSelectionListener
    public void onItemSelected(Attachment<? extends Object> attachment) {
        d0.z.d.m.checkNotNullParameter(attachment, "item");
        FlexInputFragment flexInputFragment = this.a;
        FlexInputViewModel flexInputViewModel = flexInputFragment.f3137s;
        if (flexInputViewModel != null) {
            flexInputViewModel.onAttachmentsUpdated(flexInputFragment.b().getAttachments());
        }
    }

    @Override // com.lytefast.flexinput.utils.SelectionCoordinator.ItemSelectionListener
    public void onItemUnselected(Attachment<? extends Object> attachment) {
        d0.z.d.m.checkNotNullParameter(attachment, "item");
        FlexInputFragment flexInputFragment = this.a;
        FlexInputViewModel flexInputViewModel = flexInputFragment.f3137s;
        if (flexInputViewModel != null) {
            flexInputViewModel.onAttachmentsUpdated(flexInputFragment.b().getAttachments());
        }
    }

    @Override // com.lytefast.flexinput.utils.SelectionCoordinator.ItemSelectionListener
    public void unregister() {
    }
}
