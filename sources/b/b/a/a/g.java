package b.b.a.a;

import android.content.DialogInterface;
import b.i.a.f.e.o.f;
import com.lytefast.flexinput.fragment.FlexInputFragment;
import com.lytefast.flexinput.viewmodel.FlexInputViewModel;
/* compiled from: FlexInputFragment.kt */
/* loaded from: classes3.dex */
public final class g implements DialogInterface.OnDismissListener {
    public final /* synthetic */ FlexInputFragment j;

    public g(FlexInputFragment flexInputFragment) {
        this.j = flexInputFragment;
    }

    @Override // android.content.DialogInterface.OnDismissListener
    public final void onDismiss(DialogInterface dialogInterface) {
        FlexInputViewModel flexInputViewModel;
        if (this.j.isAdded() && !this.j.isHidden() && (flexInputViewModel = this.j.f3137s) != null) {
            f.O0(flexInputViewModel, false, 1, null);
        }
    }
}
