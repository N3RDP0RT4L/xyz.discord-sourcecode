package b.b.a.a;

import androidx.viewpager.widget.ViewPager;
import com.google.android.material.tabs.TabLayout;
/* compiled from: AddContentDialogFragment.kt */
/* loaded from: classes3.dex */
public final class c implements ViewPager.OnPageChangeListener {
    public final /* synthetic */ a a;

    public c(a aVar) {
        this.a = aVar;
    }

    @Override // androidx.viewpager.widget.ViewPager.OnPageChangeListener
    public void onPageScrollStateChanged(int i) {
    }

    @Override // androidx.viewpager.widget.ViewPager.OnPageChangeListener
    public void onPageScrolled(int i, float f, int i2) {
    }

    @Override // androidx.viewpager.widget.ViewPager.OnPageChangeListener
    public void onPageSelected(int i) {
        TabLayout.Tab tabAt;
        TabLayout tabLayout = this.a.l;
        if (tabLayout != null && (tabAt = tabLayout.getTabAt(i)) != null) {
            tabAt.select();
        }
    }
}
