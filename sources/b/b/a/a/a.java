package b.b.a.a;

import andhook.lib.HookHelper;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatDialog;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.AccessibilityDelegateCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;
import b.b.a.d.d;
import com.discord.utilities.color.ColorCompat;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.lytefast.flexinput.R;
import com.lytefast.flexinput.fragment.FlexInputFragment;
import com.lytefast.flexinput.model.Attachment;
import com.lytefast.flexinput.utils.SelectionAggregator;
import com.lytefast.flexinput.utils.SelectionCoordinator;
import com.lytefast.flexinput.viewmodel.FlexInputViewModel;
import d0.t.c0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.UninitializedPropertyAccessException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.ranges.IntRange;
/* compiled from: AddContentDialogFragment.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u009d\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005*\u0001:\b\u0016\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\bF\u0010\u0010J\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0017¢\u0006\u0004\b\u0005\u0010\u0006J!\u0010\f\u001a\u00020\u000b2\u0006\u0010\b\u001a\u00020\u00072\b\u0010\n\u001a\u0004\u0018\u00010\tH\u0017¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000f\u001a\u00020\u000eH\u0016¢\u0006\u0004\b\u000f\u0010\u0010J-\u0010\u0016\u001a\u0004\u0018\u00010\u00152\u0006\u0010\u0012\u001a\u00020\u00112\b\u0010\u0014\u001a\u0004\u0018\u00010\u00132\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0016¢\u0006\u0004\b\u0016\u0010\u0017J\u000f\u0010\u0018\u001a\u00020\u000eH\u0016¢\u0006\u0004\b\u0018\u0010\u0010J\u000f\u0010\u0019\u001a\u00020\u000eH\u0016¢\u0006\u0004\b\u0019\u0010\u0010J\u0015\u0010\u001c\u001a\u00020\u000e2\u0006\u0010\u001b\u001a\u00020\u001a¢\u0006\u0004\b\u001c\u0010\u001dJ)\u0010\"\u001a\u00020\u000e2\u0006\u0010\u001e\u001a\u00020\u000b2\u0006\u0010\u001f\u001a\u00020\u000b2\b\u0010!\u001a\u0004\u0018\u00010 H\u0016¢\u0006\u0004\b\"\u0010#R$\u0010'\u001a\u0010\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u000e\u0018\u00010$8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b%\u0010&R\u0018\u0010+\u001a\u0004\u0018\u00010(8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b)\u0010*R\u0018\u0010/\u001a\u0004\u0018\u00010,8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b-\u0010.R$\u00105\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020201\u0018\u0001008\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b3\u00104R\u0018\u00109\u001a\u0004\u0018\u0001068\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b7\u00108R\u0016\u0010=\u001a\u00020:8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b;\u0010<R\u0018\u0010A\u001a\u0004\u0018\u00010>8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b?\u0010@R\u0018\u0010E\u001a\u0004\u0018\u00010B8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bC\u0010D¨\u0006G"}, d2 = {"Lb/b/a/a/a;", "Landroidx/appcompat/app/AppCompatDialogFragment;", "Landroid/os/Bundle;", "savedInstanceState", "Landroid/app/Dialog;", "onCreateDialog", "(Landroid/os/Bundle;)Landroid/app/Dialog;", "Landroidx/fragment/app/FragmentTransaction;", "transaction", "", "tag", "", "show", "(Landroidx/fragment/app/FragmentTransaction;Ljava/lang/String;)I", "", "onStart", "()V", "Landroid/view/LayoutInflater;", "inflater", "Landroid/view/ViewGroup;", "container", "Landroid/view/View;", "onCreateView", "(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;", "onResume", "onDestroyView", "", "openKeyboard", "h", "(Z)V", "requestCode", "resultCode", "Landroid/content/Intent;", "intentData", "onActivityResult", "(IILandroid/content/Intent;)V", "Lkotlin/Function1;", "p", "Lkotlin/jvm/functions/Function1;", "onKeyboardSelectedListener", "Lcom/google/android/material/tabs/TabLayout;", "l", "Lcom/google/android/material/tabs/TabLayout;", "contentTabs", "Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;", "q", "Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;", "onPageChangeListener", "Lcom/lytefast/flexinput/utils/SelectionAggregator;", "Lcom/lytefast/flexinput/model/Attachment;", "", "o", "Lcom/lytefast/flexinput/utils/SelectionAggregator;", "selectionAggregator", "Landroid/widget/ImageView;", "n", "Landroid/widget/ImageView;", "launchButton", "b/b/a/a/a$c", "r", "Lb/b/a/a/a$c;", "itemSelectionListener", "Lcom/google/android/material/floatingactionbutton/FloatingActionButton;", "m", "Lcom/google/android/material/floatingactionbutton/FloatingActionButton;", "actionButton", "Landroidx/viewpager/widget/ViewPager;", "k", "Landroidx/viewpager/widget/ViewPager;", "contentPager", HookHelper.constructorName, "flexinput_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes3.dex */
public class a extends AppCompatDialogFragment {
    public static final /* synthetic */ int j = 0;
    public ViewPager k;
    public TabLayout l;
    public FloatingActionButton m;
    public ImageView n;
    public SelectionAggregator<Attachment<Object>> o;
    public Function1<? super View, Unit> p;
    public ViewPager.OnPageChangeListener q;
    public final c r = new c();

    /* compiled from: java-style lambda group */
    /* renamed from: b.b.a.a.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class View$OnClickListenerC0057a implements View.OnClickListener {
        public final /* synthetic */ int j;
        public final /* synthetic */ Object k;

        public View$OnClickListenerC0057a(int i, Object obj) {
            this.j = i;
            this.k = obj;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            int i = this.j;
            if (i == 0) {
                a aVar = (a) this.k;
                int i2 = a.j;
                if (aVar.isCancelable()) {
                    aVar.h(true);
                }
            } else if (i == 1) {
                a aVar2 = (a) this.k;
                Objects.requireNonNull(aVar2);
                Intent intent = new Intent("android.intent.action.OPEN_DOCUMENT");
                intent.setType("*/*");
                intent.addCategory("android.intent.category.OPENABLE");
                intent.putExtra("android.intent.extra.ALLOW_MULTIPLE", true);
                try {
                    aVar2.startActivityForResult(intent, 5968);
                } catch (ActivityNotFoundException unused) {
                    Toast.makeText(aVar2.getContext(), aVar2.getString(R.h.error_generic_title), 0).show();
                }
            } else if (i == 2) {
                FlexInputFragment flexInputFragment = (FlexInputFragment) ((Fragment) this.k);
                FlexInputViewModel flexInputViewModel = flexInputFragment.f3137s;
                if (flexInputViewModel != null) {
                    flexInputViewModel.onSendButtonClicked(flexInputFragment.o);
                }
            } else {
                throw null;
            }
        }
    }

    /* compiled from: AddContentDialogFragment.kt */
    /* loaded from: classes3.dex */
    public static final class b implements Animation.AnimationListener {
        public final /* synthetic */ boolean k;

        public b(boolean z2) {
            this.k = z2;
        }

        @Override // android.view.animation.Animation.AnimationListener
        public void onAnimationEnd(Animation animation) {
            View view;
            Function1<? super View, Unit> function1;
            m.checkNotNullParameter(animation, "animation");
            a.this.dismissAllowingStateLoss();
            if (this.k && (view = a.this.getView()) != null && (function1 = a.this.p) != null) {
                m.checkNotNullExpressionValue(view, "it");
                function1.invoke(view);
            }
        }

        @Override // android.view.animation.Animation.AnimationListener
        public void onAnimationRepeat(Animation animation) {
            m.checkNotNullParameter(animation, "animation");
        }

        @Override // android.view.animation.Animation.AnimationListener
        public void onAnimationStart(Animation animation) {
            m.checkNotNullParameter(animation, "animation");
        }
    }

    /* compiled from: AddContentDialogFragment.kt */
    /* loaded from: classes3.dex */
    public static final class c implements SelectionCoordinator.ItemSelectionListener<Attachment<?>> {
        public c() {
        }

        @Override // com.lytefast.flexinput.utils.SelectionCoordinator.ItemSelectionListener
        public void onItemSelected(Attachment<?> attachment) {
            m.checkNotNullParameter(attachment, "item");
            a.g(a.this);
        }

        @Override // com.lytefast.flexinput.utils.SelectionCoordinator.ItemSelectionListener
        public void onItemUnselected(Attachment<?> attachment) {
            m.checkNotNullParameter(attachment, "item");
            a.g(a.this);
        }

        @Override // com.lytefast.flexinput.utils.SelectionCoordinator.ItemSelectionListener
        public void unregister() {
        }
    }

    /* compiled from: AddContentDialogFragment.kt */
    /* loaded from: classes3.dex */
    public static final class d extends AppCompatDialog {
        public d(a aVar, Context context, int i) {
            super(context, i);
        }

        @Override // android.app.Dialog
        public void show() {
            super.show();
            Window window = getWindow();
            if (window != null) {
                window.setLayout(-1, -1);
            }
        }
    }

    /* compiled from: AddContentDialogFragment.kt */
    /* loaded from: classes3.dex */
    public static final class e extends o implements Function0<Unit> {
        public final /* synthetic */ Fragment $flexInputFragment;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public e(Fragment fragment) {
            super(0);
            this.$flexInputFragment = fragment;
        }

        @Override // kotlin.jvm.functions.Function0
        public Unit invoke() {
            TabLayout tabLayout;
            TabLayout.Tab tabAt;
            FragmentManager childFragmentManager = a.this.getChildFragmentManager();
            m.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
            d.a[] k = ((FlexInputFragment) this.$flexInputFragment).k();
            b.b.a.d.d dVar = new b.b.a.d.d(childFragmentManager, (d.a[]) Arrays.copyOf(k, k.length));
            a aVar = a.this;
            Objects.requireNonNull(aVar);
            m.checkNotNullParameter(dVar, "pagerAdapter");
            Context context = aVar.getContext();
            if (!(context == null || (tabLayout = aVar.l) == null)) {
                m.checkNotNullExpressionValue(context, "context");
                m.checkNotNullParameter(context, "context");
                m.checkNotNullParameter(tabLayout, "tabLayout");
                ColorStateList colorStateList = AppCompatResources.getColorStateList(context, R.c.tab_color_selector);
                int tabCount = tabLayout.getTabCount();
                for (int i = 0; i < tabCount; i++) {
                    TabLayout.Tab tabAt2 = tabLayout.getTabAt(i);
                    if (tabAt2 != null) {
                        m.checkNotNullExpressionValue(colorStateList, "iconColors");
                        Drawable icon = tabAt2.getIcon();
                        if (icon != null) {
                            Drawable wrap = DrawableCompat.wrap(icon);
                            DrawableCompat.setTintList(wrap, colorStateList);
                            tabAt2.setIcon(wrap);
                        }
                    }
                }
                d.a[] aVarArr = dVar.a;
                ArrayList arrayList = new ArrayList(aVarArr.length);
                for (d.a aVar2 : aVarArr) {
                    TabLayout.Tab icon2 = tabLayout.newTab().setIcon(aVar2.getIcon());
                    m.checkNotNullExpressionValue(icon2, "tabLayout.newTab()\n              .setIcon(it.icon)");
                    m.checkNotNullExpressionValue(colorStateList, "iconColors");
                    Drawable icon3 = icon2.getIcon();
                    if (icon3 != null) {
                        Drawable wrap2 = DrawableCompat.wrap(icon3);
                        DrawableCompat.setTintList(wrap2, colorStateList);
                        icon2.setIcon(wrap2);
                    }
                    arrayList.add(icon2.setContentDescription(aVar2.getContentDesc()));
                }
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    tabLayout.addTab((TabLayout.Tab) it.next());
                }
                ViewPager viewPager = aVar.k;
                if (viewPager != null) {
                    viewPager.setAdapter(dVar);
                }
                TabLayout tabLayout2 = aVar.l;
                if (tabLayout2 != null) {
                    tabLayout2.addOnTabSelectedListener((TabLayout.OnTabSelectedListener) new b.b.a.a.b(aVar));
                }
                ViewPager viewPager2 = aVar.k;
                if (viewPager2 != null) {
                    viewPager2.addOnPageChangeListener(new b.b.a.a.c(aVar));
                }
                TabLayout tabLayout3 = aVar.l;
                if (!(tabLayout3 == null || (tabAt = tabLayout3.getTabAt(0)) == null)) {
                    tabAt.select();
                }
            }
            return Unit.a;
        }
    }

    /* compiled from: AddContentDialogFragment.kt */
    /* loaded from: classes3.dex */
    public static final class f extends AccessibilityDelegateCompat {
        public f() {
        }

        @Override // androidx.core.view.AccessibilityDelegateCompat
        public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            m.checkNotNullParameter(view, "host");
            m.checkNotNullParameter(accessibilityNodeInfoCompat, "info");
            ImageView imageView = a.this.n;
            Objects.requireNonNull(imageView, "null cannot be cast to non-null type android.view.View");
            accessibilityNodeInfoCompat.setTraversalAfter(imageView);
            super.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat);
        }
    }

    /* compiled from: AddContentDialogFragment.kt */
    /* loaded from: classes3.dex */
    public static final class g implements Runnable {
        public g() {
        }

        @Override // java.lang.Runnable
        public final void run() {
            a.g(a.this);
        }
    }

    public static final void g(a aVar) {
        FloatingActionButton floatingActionButton = aVar.m;
        if (floatingActionButton != null) {
            floatingActionButton.post(new b.b.a.a.d(aVar));
        }
    }

    public final void h(boolean z2) {
        Context context = getContext();
        if (context != null) {
            m.checkNotNullExpressionValue(context, "context ?: return dismissAllowingStateLoss()");
            Animation loadAnimation = AnimationUtils.loadAnimation(context, com.google.android.material.R.anim.design_bottom_sheet_slide_out);
            m.checkNotNullExpressionValue(loadAnimation, "animation");
            loadAnimation.setDuration(getResources().getInteger(com.google.android.material.R.integer.bottom_sheet_slide_duration));
            loadAnimation.setInterpolator(context, 17432580);
            FloatingActionButton floatingActionButton = this.m;
            if (floatingActionButton != null) {
                floatingActionButton.hide();
            }
            TabLayout tabLayout = this.l;
            if (tabLayout != null) {
                tabLayout.startAnimation(loadAnimation);
            }
            ViewPager viewPager = this.k;
            if (viewPager != null) {
                viewPager.startAnimation(loadAnimation);
            }
            ImageView imageView = this.n;
            if (imageView != null) {
                imageView.startAnimation(loadAnimation);
            }
            loadAnimation.setAnimationListener(new b(z2));
            return;
        }
        dismissAllowingStateLoss();
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        ContentResolver contentResolver;
        super.onActivityResult(i, i2, intent);
        if (5968 == i && i2 != 0) {
            if (-1 != i2 || intent == null) {
                Toast.makeText(getContext(), "Error loading files", 0).show();
                return;
            }
            Context context = getContext();
            if (!(context == null || (contentResolver = context.getContentResolver()) == null)) {
                ClipData clipData = intent.getClipData();
                Fragment parentFragment = getParentFragment();
                Objects.requireNonNull(parentFragment, "null cannot be cast to non-null type com.lytefast.flexinput.FlexInputCoordinator<kotlin.Any>");
                b.b.a.b bVar = (b.b.a.b) parentFragment;
                if (clipData == null) {
                    Uri data = intent.getData();
                    if (data != null) {
                        bVar.f(Attachment.Companion.b(data, contentResolver));
                        return;
                    }
                    return;
                }
                IntRange until = d0.d0.f.until(0, clipData.getItemCount());
                ArrayList<Uri> arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(until, 10));
                Iterator<Integer> it = until.iterator();
                while (it.hasNext()) {
                    ClipData.Item itemAt = clipData.getItemAt(((c0) it).nextInt());
                    m.checkNotNullExpressionValue(itemAt, "clipData.getItemAt(it)");
                    arrayList.add(itemAt.getUri());
                }
                for (Uri uri : arrayList) {
                    Attachment.Companion companion = Attachment.Companion;
                    m.checkNotNullExpressionValue(uri, "it");
                    bVar.f(companion.b(uri, contentResolver));
                }
            }
        }
    }

    @Override // androidx.appcompat.app.AppCompatDialogFragment, androidx.fragment.app.DialogFragment
    @SuppressLint({"PrivateResource"})
    public Dialog onCreateDialog(Bundle bundle) {
        d dVar = new d(this, getContext(), R.i.FlexInput_DialogWhenLarge);
        dVar.supportRequestWindowFeature(1);
        Window window = dVar.getWindow();
        if (window != null) {
            window.setWindowAnimations(com.google.android.material.R.style.Animation_AppCompat_Dialog);
            window.setBackgroundDrawableResource(17170445);
        }
        return dVar;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        m.checkNotNullParameter(layoutInflater, "inflater");
        View inflate = layoutInflater.inflate(R.g.dialog_add_content_pager_with_fab, viewGroup, false);
        if (inflate != null) {
            inflate.setOnClickListener(new View$OnClickListenerC0057a(0, this));
            this.k = (ViewPager) inflate.findViewById(R.f.content_pager);
            this.l = (TabLayout) inflate.findViewById(R.f.content_tabs);
            this.m = (FloatingActionButton) inflate.findViewById(R.f.action_btn);
            ImageView imageView = (ImageView) inflate.findViewById(R.f.launch_btn);
            this.n = imageView;
            if (imageView != null) {
                imageView.setOnClickListener(new View$OnClickListenerC0057a(1, this));
            }
            FloatingActionButton floatingActionButton = this.m;
            Objects.requireNonNull(floatingActionButton, "null cannot be cast to non-null type com.google.android.material.floatingactionbutton.FloatingActionButton");
            floatingActionButton.setBackgroundTintList(ColorStateList.valueOf(ColorCompat.getThemedColor(inflate.getContext(), R.b.color_brand_500)));
        }
        Fragment parentFragment = getParentFragment();
        if (parentFragment instanceof FlexInputFragment) {
            FlexInputFragment flexInputFragment = (FlexInputFragment) parentFragment;
            e eVar = new e(parentFragment);
            Objects.requireNonNull(flexInputFragment);
            m.checkNotNullParameter(eVar, "onContentPagesInitialized");
            try {
                flexInputFragment.k();
                eVar.invoke();
            } catch (UninitializedPropertyAccessException unused) {
                flexInputFragment.f3138x.add(eVar);
            }
            FloatingActionButton floatingActionButton2 = this.m;
            if (floatingActionButton2 != null) {
                floatingActionButton2.setOnClickListener(new View$OnClickListenerC0057a(2, parentFragment));
            }
            this.o = flexInputFragment.b().addItemSelectionListener(this.r);
            FloatingActionButton floatingActionButton3 = this.m;
            Objects.requireNonNull(floatingActionButton3, "null cannot be cast to non-null type android.view.View");
            ViewCompat.setAccessibilityDelegate(floatingActionButton3, new f());
        }
        return inflate;
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onDestroyView() {
        SelectionAggregator<Attachment<Object>> selectionAggregator = this.o;
        if (selectionAggregator != null) {
            selectionAggregator.removeItemSelectionListener(this.r);
        }
        super.onDestroyView();
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FloatingActionButton floatingActionButton = this.m;
        if (floatingActionButton != null) {
            floatingActionButton.post(new g());
        }
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        Context context = getContext();
        if (context != null) {
            m.checkNotNullExpressionValue(context, "it");
            Animation loadAnimation = AnimationUtils.loadAnimation(context, com.google.android.material.R.anim.design_bottom_sheet_slide_in);
            m.checkNotNullExpressionValue(loadAnimation, "animation");
            loadAnimation.setDuration(getResources().getInteger(com.google.android.material.R.integer.bottom_sheet_slide_duration));
            loadAnimation.setInterpolator(context, 17432580);
            TabLayout tabLayout = this.l;
            if (tabLayout != null) {
                tabLayout.startAnimation(loadAnimation);
            }
            ViewPager viewPager = this.k;
            if (viewPager != null) {
                viewPager.startAnimation(loadAnimation);
            }
            ImageView imageView = this.n;
            if (imageView != null) {
                imageView.startAnimation(loadAnimation);
            }
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    @SuppressLint({"PrivateResource"})
    public int show(FragmentTransaction fragmentTransaction, String str) {
        m.checkNotNullParameter(fragmentTransaction, "transaction");
        fragmentTransaction.setCustomAnimations(com.google.android.material.R.anim.abc_grow_fade_in_from_bottom, com.google.android.material.R.anim.abc_shrink_fade_out_from_bottom);
        return super.show(fragmentTransaction, str);
    }
}
