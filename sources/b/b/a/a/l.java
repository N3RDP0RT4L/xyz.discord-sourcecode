package b.b.a.a;

import android.widget.FrameLayout;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatImageButton;
import com.lytefast.flexinput.R;
import com.lytefast.flexinput.fragment.FlexInputFragment;
import d0.z.d.m;
import d0.z.d.o;
import java.util.concurrent.TimeUnit;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Action1;
/* compiled from: FlexInputFragment.kt */
/* loaded from: classes3.dex */
public final class l extends o implements Function0<Unit> {
    public final /* synthetic */ FlexInputFragment this$0;

    /* compiled from: FlexInputFragment.kt */
    /* loaded from: classes3.dex */
    public static final class a<T> implements Action1<Long> {
        public a() {
        }

        @Override // rx.functions.Action1
        public void call(Long l) {
            FlexInputFragment flexInputFragment = l.this.this$0;
            KProperty[] kPropertyArr = FlexInputFragment.j;
            if (flexInputFragment.m()) {
                FrameLayout frameLayout = l.this.this$0.j().k;
                m.checkNotNullExpressionValue(frameLayout, "binding.expressionTrayContainer");
                frameLayout.setVisibility(0);
                FlexInputFragment.h(l.this.this$0, true);
                l.this.this$0.j().i.setImageResource(R.e.ic_keyboard_24dp);
                ImageView imageView = l.this.this$0.j().j;
                m.checkNotNullExpressionValue(imageView, "binding.expressionBtnBadge");
                imageView.setVisibility(8);
                AppCompatImageButton appCompatImageButton = l.this.this$0.j().i;
                m.checkNotNullExpressionValue(appCompatImageButton, "binding.expressionBtn");
                appCompatImageButton.setContentDescription(l.this.this$0.getString(R.h.show_keyboard));
                FrameLayout frameLayout2 = l.this.this$0.j().g;
                m.checkNotNullExpressionValue(frameLayout2, "binding.defaultWindowInsetsHandler");
                frameLayout2.setVisibility(8);
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public l(FlexInputFragment flexInputFragment) {
        super(0);
        this.this$0 = flexInputFragment;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        this.this$0.v = Observable.d0(300L, TimeUnit.MILLISECONDS).I(j0.j.b.a.a()).V(new a());
    }
}
