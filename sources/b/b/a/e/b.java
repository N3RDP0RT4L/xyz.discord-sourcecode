package b.b.a.e;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.facebook.drawee.view.SimpleDraweeView;
/* compiled from: ViewAttachmentPreviewItemBinding.java */
/* loaded from: classes3.dex */
public final class b implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final SimpleDraweeView f319b;
    @NonNull
    public final SimpleDraweeView c;
    @NonNull
    public final View d;
    @NonNull
    public final SimpleDraweeView e;

    public b(@NonNull ConstraintLayout constraintLayout, @NonNull SimpleDraweeView simpleDraweeView, @NonNull SimpleDraweeView simpleDraweeView2, @NonNull View view, @NonNull SimpleDraweeView simpleDraweeView3) {
        this.a = constraintLayout;
        this.f319b = simpleDraweeView;
        this.c = simpleDraweeView2;
        this.d = view;
        this.e = simpleDraweeView3;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
