package b.b.a.e;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
import com.lytefast.flexinput.widget.FlexEditText;
/* compiled from: FlexInputWidgetBinding.java */
/* loaded from: classes3.dex */
public final class a implements ViewBinding {
    @NonNull
    public final LinearLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final AppCompatImageButton f318b;
    @NonNull
    public final LinearLayout c;
    @NonNull
    public final RecyclerView d;
    @NonNull
    public final TextView e;
    @NonNull
    public final LinearLayout f;
    @NonNull
    public final FrameLayout g;
    @NonNull
    public final AppCompatImageButton h;
    @NonNull
    public final AppCompatImageButton i;
    @NonNull
    public final ImageView j;
    @NonNull
    public final FrameLayout k;
    @NonNull
    public final AppCompatImageButton l;
    @NonNull
    public final AppCompatImageButton m;
    @NonNull
    public final LinearLayout n;
    @NonNull
    public final FrameLayout o;
    @NonNull
    public final ImageView p;
    @NonNull
    public final FlexEditText q;

    public a(@NonNull LinearLayout linearLayout, @NonNull AppCompatImageButton appCompatImageButton, @NonNull LinearLayout linearLayout2, @NonNull RecyclerView recyclerView, @NonNull TextView textView, @NonNull LinearLayout linearLayout3, @NonNull FrameLayout frameLayout, @NonNull AppCompatImageButton appCompatImageButton2, @NonNull AppCompatImageButton appCompatImageButton3, @NonNull ImageView imageView, @NonNull FrameLayout frameLayout2, @NonNull FrameLayout frameLayout3, @NonNull AppCompatImageButton appCompatImageButton4, @NonNull AppCompatImageButton appCompatImageButton5, @NonNull LinearLayout linearLayout4, @NonNull LinearLayout linearLayout5, @NonNull FrameLayout frameLayout4, @NonNull ImageView imageView2, @NonNull FlexEditText flexEditText) {
        this.a = linearLayout;
        this.f318b = appCompatImageButton;
        this.c = linearLayout2;
        this.d = recyclerView;
        this.e = textView;
        this.f = linearLayout3;
        this.g = frameLayout;
        this.h = appCompatImageButton2;
        this.i = appCompatImageButton3;
        this.j = imageView;
        this.k = frameLayout3;
        this.l = appCompatImageButton4;
        this.m = appCompatImageButton5;
        this.n = linearLayout5;
        this.o = frameLayout4;
        this.p = imageView2;
        this.q = flexEditText;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
