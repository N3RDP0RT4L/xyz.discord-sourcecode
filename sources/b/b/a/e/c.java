package b.b.a.e;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.facebook.drawee.view.SimpleDraweeView;
/* compiled from: ViewGridImageBinding.java */
/* loaded from: classes3.dex */
public final class c implements ViewBinding {
    @NonNull
    public final FrameLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final FrameLayout f320b;
    @NonNull
    public final SimpleDraweeView c;
    @NonNull
    public final SimpleDraweeView d;
    @NonNull
    public final TextView e;

    public c(@NonNull FrameLayout frameLayout, @NonNull FrameLayout frameLayout2, @NonNull SimpleDraweeView simpleDraweeView, @NonNull SimpleDraweeView simpleDraweeView2, @NonNull TextView textView) {
        this.a = frameLayout;
        this.f320b = frameLayout2;
        this.c = simpleDraweeView;
        this.d = simpleDraweeView2;
        this.e = textView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
