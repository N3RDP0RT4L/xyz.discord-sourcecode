package b.b.a.d;

import com.lytefast.flexinput.model.Attachment;
import d0.u.a;
import java.util.Comparator;
/* compiled from: Comparisons.kt */
/* loaded from: classes3.dex */
public final class e<T> implements Comparator {
    @Override // java.util.Comparator
    public final int compare(T t, T t2) {
        return a.compareValues(((Attachment) t).getUri(), ((Attachment) t2).getUri());
    }
}
