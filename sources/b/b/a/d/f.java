package b.b.a.d;

import com.lytefast.flexinput.adapters.FileListAdapter;
import com.lytefast.flexinput.model.Attachment;
import d0.u.a;
import java.io.File;
import java.util.Comparator;
import java.util.Objects;
/* compiled from: Comparisons.kt */
/* loaded from: classes3.dex */
public final class f<T> implements Comparator {
    public final /* synthetic */ FileListAdapter.a j;

    public f(FileListAdapter.a aVar) {
        this.j = aVar;
    }

    @Override // java.util.Comparator
    public final int compare(T t, T t2) {
        Objects.requireNonNull(this.j);
        File file = (File) ((Attachment) t2).getData();
        long j = 0;
        Long valueOf = Long.valueOf(file != null ? file.lastModified() : 0L);
        Objects.requireNonNull(this.j);
        File file2 = (File) ((Attachment) t).getData();
        if (file2 != null) {
            j = file2.lastModified();
        }
        return a.compareValues(valueOf, Long.valueOf(j));
    }
}
