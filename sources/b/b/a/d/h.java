package b.b.a.d;

import android.animation.AnimatorSet;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import b.b.a.e.c;
import com.discord.utilities.time.TimeUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.lytefast.flexinput.R;
import com.lytefast.flexinput.model.Media;
import com.lytefast.flexinput.utils.SelectionCoordinator;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import org.objectweb.asm.Opcodes;
/* compiled from: MediaCursorAdapter.kt */
/* loaded from: classes3.dex */
public final class h extends RecyclerView.Adapter<a> {
    public final SelectionCoordinator<?, Media> a;

    /* renamed from: b  reason: collision with root package name */
    public Cursor f315b;
    public int c;
    public int d;
    public int e;
    public int f;
    public Integer g;
    public final int h;
    public final int i;

    /* compiled from: MediaCursorAdapter.kt */
    /* loaded from: classes3.dex */
    public final class a extends i implements View.OnClickListener {
        public Media p;
        public final AnimatorSet q;
        public final AnimatorSet r;

        /* renamed from: s  reason: collision with root package name */
        public final c f316s;
        public final /* synthetic */ h t;

        /* compiled from: MediaCursorAdapter.kt */
        /* renamed from: b.b.a.d.h$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0058a extends o implements Function1<AnimatorSet, Unit> {
            public final /* synthetic */ boolean $isAnimationRequested;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C0058a(boolean z2) {
                super(1);
                this.$isAnimationRequested = z2;
            }

            public final void a(AnimatorSet animatorSet) {
                m.checkNotNullParameter(animatorSet, "animation");
                animatorSet.start();
                if (!this.$isAnimationRequested) {
                    animatorSet.end();
                }
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(AnimatorSet animatorSet) {
                a(animatorSet);
                return Unit.a;
            }
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public a(b.b.a.d.h r4, b.b.a.e.c r5) {
            /*
                r3 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r5, r0)
                r3.t = r4
                android.widget.FrameLayout r4 = r5.a
                java.lang.String r0 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r4, r0)
                r3.<init>(r4)
                r3.f316s = r5
                android.widget.FrameLayout r4 = r5.a
                r4.setOnClickListener(r3)
                android.widget.FrameLayout r4 = r5.a
                d0.z.d.m.checkNotNullExpressionValue(r4, r0)
                android.content.Context r4 = r4.getContext()
                int r1 = com.lytefast.flexinput.R.a.selection_shrink
                android.animation.Animator r4 = android.animation.AnimatorInflater.loadAnimator(r4, r1)
                java.lang.String r1 = "null cannot be cast to non-null type android.animation.AnimatorSet"
                java.util.Objects.requireNonNull(r4, r1)
                android.animation.AnimatorSet r4 = (android.animation.AnimatorSet) r4
                r3.q = r4
                android.widget.FrameLayout r2 = r5.f320b
                r4.setTarget(r2)
                android.widget.FrameLayout r4 = r5.a
                d0.z.d.m.checkNotNullExpressionValue(r4, r0)
                android.content.Context r4 = r4.getContext()
                int r0 = com.lytefast.flexinput.R.a.selection_grow
                android.animation.Animator r4 = android.animation.AnimatorInflater.loadAnimator(r4, r0)
                java.util.Objects.requireNonNull(r4, r1)
                android.animation.AnimatorSet r4 = (android.animation.AnimatorSet) r4
                r3.r = r4
                android.widget.FrameLayout r5 = r5.f320b
                r4.setTarget(r5)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.b.a.d.h.a.<init>(b.b.a.d.h, b.b.a.e.c):void");
        }

        @Override // b.b.a.d.i
        public SimpleDraweeView a() {
            SimpleDraweeView simpleDraweeView = this.f316s.c;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.contentIv");
            return simpleDraweeView;
        }

        public final void d(boolean z2, boolean z3) {
            FrameLayout frameLayout = this.f316s.a;
            m.checkNotNullExpressionValue(frameLayout, "binding.root");
            frameLayout.setSelected(z2);
            C0058a aVar = new C0058a(z3);
            if (z2) {
                SimpleDraweeView simpleDraweeView = this.f316s.d;
                m.checkNotNullExpressionValue(simpleDraweeView, "binding.itemCheckIndicator");
                simpleDraweeView.setVisibility(0);
                FrameLayout frameLayout2 = this.f316s.f320b;
                m.checkNotNullExpressionValue(frameLayout2, "binding.contentContainer");
                if (frameLayout2.getScaleX() == 1.0f) {
                    aVar.a(this.q);
                    return;
                }
                return;
            }
            SimpleDraweeView simpleDraweeView2 = this.f316s.d;
            m.checkNotNullExpressionValue(simpleDraweeView2, "binding.itemCheckIndicator");
            simpleDraweeView2.setVisibility(8);
            FrameLayout frameLayout3 = this.f316s.f320b;
            m.checkNotNullExpressionValue(frameLayout3, "binding.contentContainer");
            if (frameLayout3.getScaleX() != 1.0f) {
                aVar.a(this.r);
            }
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            m.checkNotNullParameter(view, "v");
            SelectionCoordinator<?, Media> selectionCoordinator = this.t.a;
            Media media = this.p;
            int adapterPosition = getAdapterPosition();
            Objects.requireNonNull(selectionCoordinator);
            if (media != null && !selectionCoordinator.d(media)) {
                selectionCoordinator.c(media, adapterPosition);
            }
        }
    }

    /* compiled from: MediaCursorAdapter.kt */
    /* loaded from: classes3.dex */
    public static final class b extends AsyncQueryHandler {
        public b(ContentResolver contentResolver) {
            super(contentResolver);
        }

        @Override // android.content.AsyncQueryHandler
        public void onQueryComplete(int i, Object obj, Cursor cursor) {
            m.checkNotNullParameter(obj, "cookie");
            if (cursor != null) {
                h hVar = h.this;
                hVar.c = cursor.getColumnIndex("_id");
                hVar.d = cursor.getColumnIndex("_data");
                hVar.e = cursor.getColumnIndex("_display_name");
                hVar.f = cursor.getColumnIndex("media_type");
                if (Build.VERSION.SDK_INT >= 29) {
                    hVar.g = Integer.valueOf(cursor.getColumnIndex("duration"));
                }
                hVar.f315b = cursor;
                h.this.notifyDataSetChanged();
            }
        }
    }

    public h(SelectionCoordinator<?, Media> selectionCoordinator, int i, int i2) {
        m.checkNotNullParameter(selectionCoordinator, "selectionCoordinator");
        this.h = i;
        this.i = i2;
        Objects.requireNonNull(selectionCoordinator);
        m.checkNotNullParameter(this, "adapter");
        selectionCoordinator.a = this;
        this.a = selectionCoordinator;
        setHasStableIds(true);
    }

    public final Media a(int i) {
        Uri uri;
        String str;
        Cursor cursor = this.f315b;
        Media media = null;
        r1 = null;
        Long l = null;
        if (cursor != null) {
            cursor.moveToPosition(i);
            long j = cursor.getLong(this.c);
            boolean z2 = cursor.getInt(this.f) == 3;
            if (z2) {
                uri = Uri.withAppendedPath(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, String.valueOf(j));
            } else {
                uri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, String.valueOf(j));
            }
            Uri uri2 = uri;
            String str2 = z2 ? "vid" : "img";
            Integer num = this.g;
            if (z2 && num != null) {
                l = Long.valueOf(cursor.getLong(num.intValue()));
            }
            Long l2 = l;
            m.checkNotNullExpressionValue(uri2, "fileUri");
            String string = cursor.getString(this.e);
            if (string != null) {
                str = string;
            } else {
                str = str2 + '-' + j;
            }
            media = new Media(j, uri2, str, cursor.getString(this.d), z2, l2);
        }
        return media;
    }

    public final void b(ContentResolver contentResolver) {
        String[] strArr;
        m.checkNotNullParameter(contentResolver, "contentResolver");
        int i = Build.VERSION.SDK_INT;
        String str = i >= 29 ? "media_type = 1 OR media_type = 3" : "media_type = 1";
        if (i >= 29) {
            strArr = new String[]{"_id", "_data", "_display_name", "media_type", "duration"};
        } else {
            strArr = new String[]{"_id", "_data", "_display_name", "media_type"};
        }
        new b(contentResolver).startQuery(1, this, MediaStore.Files.getContentUri("external"), strArr, str, null, "date_added DESC");
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        Cursor cursor = this.f315b;
        if (cursor != null) {
            return cursor.getCount();
        }
        return 0;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public long getItemId(int i) {
        Media a2 = a(i);
        if (a2 != null) {
            return a2.getId();
        }
        return -1L;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        m.checkNotNullParameter(recyclerView, "recyclerView");
        super.onAttachedToRecyclerView(recyclerView);
        Context context = recyclerView.getContext();
        m.checkNotNullExpressionValue(context, "recyclerView.context");
        ContentResolver contentResolver = context.getContentResolver();
        m.checkNotNullExpressionValue(contentResolver, "recyclerView.context.contentResolver");
        b(contentResolver);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onBindViewHolder(a aVar, int i) {
        CharSequence charSequence;
        Long l;
        a aVar2 = aVar;
        m.checkNotNullParameter(aVar2, "holder");
        Media a2 = a(i);
        aVar2.p = a2;
        FrameLayout frameLayout = aVar2.f316s.a;
        m.checkNotNullExpressionValue(frameLayout, "binding.root");
        Context context = frameLayout.getContext();
        int i2 = 0;
        if (a2 != null) {
            aVar2.d(aVar2.t.a.a(a2, aVar2.getAdapterPosition()), false);
        }
        aVar2.c(a2, Integer.valueOf(aVar2.t.h), Integer.valueOf(aVar2.t.i));
        boolean z2 = true;
        if (a2 == null || !a2.j) {
            z2 = false;
        }
        TextView textView = aVar2.f316s.e;
        m.checkNotNullExpressionValue(textView, "binding.itemVideoIndicator");
        if (!z2) {
            i2 = 8;
        }
        textView.setVisibility(i2);
        if (z2) {
            TextView textView2 = aVar2.f316s.e;
            m.checkNotNullExpressionValue(textView2, "binding.itemVideoIndicator");
            Drawable background = textView2.getBackground();
            m.checkNotNullExpressionValue(background, "binding.itemVideoIndicator.background");
            background.setAlpha(Opcodes.LAND);
            long longValue = (a2 == null || (l = a2.k) == null) ? 0L : l.longValue();
            TextView textView3 = aVar2.f316s.e;
            m.checkNotNullExpressionValue(textView3, "binding.itemVideoIndicator");
            if (longValue > 0) {
                charSequence = TimeUtils.toFriendlyStringSimple$default(TimeUtils.INSTANCE, longValue, null, null, 6, null);
            } else {
                charSequence = context.getString(R.h.video);
            }
            textView3.setText(charSequence);
        }
        SimpleDraweeView simpleDraweeView = aVar2.f316s.c;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.contentIv");
        simpleDraweeView.setContentDescription(context.getString(z2 ? R.h.video : R.h.image));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.g.view_grid_image, viewGroup, false);
        int i2 = R.f.content_container;
        FrameLayout frameLayout = (FrameLayout) inflate.findViewById(i2);
        if (frameLayout != null) {
            i2 = R.f.content_iv;
            SimpleDraweeView simpleDraweeView = (SimpleDraweeView) inflate.findViewById(i2);
            if (simpleDraweeView != null) {
                i2 = R.f.item_check_indicator;
                SimpleDraweeView simpleDraweeView2 = (SimpleDraweeView) inflate.findViewById(i2);
                if (simpleDraweeView2 != null) {
                    i2 = R.f.item_video_indicator;
                    TextView textView = (TextView) inflate.findViewById(i2);
                    if (textView != null) {
                        c cVar = new c((FrameLayout) inflate, frameLayout, simpleDraweeView, simpleDraweeView2, textView);
                        m.checkNotNullExpressionValue(cVar, "ViewGridImageBinding.inf….context), parent, false)");
                        return new a(this, cVar);
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i2)));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        m.checkNotNullParameter(recyclerView, "recyclerView");
        Cursor cursor = this.f315b;
        if (cursor != null) {
            cursor.close();
        }
        super.onDetachedFromRecyclerView(recyclerView);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onViewRecycled(a aVar) {
        a aVar2 = aVar;
        m.checkNotNullParameter(aVar2, "holder");
        super.onViewRecycled(aVar2);
        aVar2.b();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onBindViewHolder(a aVar, int i, List list) {
        SelectionCoordinator.a aVar2;
        Object obj;
        a aVar3 = aVar;
        m.checkNotNullParameter(aVar3, "holder");
        m.checkNotNullParameter(list, "payloads");
        Iterator it = list.iterator();
        while (true) {
            aVar2 = null;
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (obj instanceof SelectionCoordinator.a) {
                break;
            }
        }
        if (obj != null) {
            if (obj instanceof SelectionCoordinator.a) {
                aVar2 = obj;
            }
            SelectionCoordinator.a aVar4 = aVar2;
            if (aVar4 != null) {
                aVar3.d(aVar4.f3143b, true);
                return;
            }
        }
        super.onBindViewHolder(aVar3, i, list);
    }
}
