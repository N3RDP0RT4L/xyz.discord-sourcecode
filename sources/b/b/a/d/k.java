package b.b.a.d;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Size;
import b.b.a.d.i;
import d0.l;
import d0.w.h.c;
import d0.w.i.a.e;
import d0.z.d.m;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineScope;
/* compiled from: ThumbnailViewHolder.kt */
@e(c = "com.lytefast.flexinput.adapters.ThumbnailViewHolder$ThumbnailBitmapGenerator$getThumbnailQ$2", f = "ThumbnailViewHolder.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes3.dex */
public final class k extends d0.w.i.a.k implements Function2<CoroutineScope, Continuation<? super Bitmap>, Object> {
    public final /* synthetic */ ContentResolver $contentResolver;
    public final /* synthetic */ Uri $uri;
    public int label;
    public final /* synthetic */ i.b this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public k(i.b bVar, ContentResolver contentResolver, Uri uri, Continuation continuation) {
        super(2, continuation);
        this.this$0 = bVar;
        this.$contentResolver = contentResolver;
        this.$uri = uri;
    }

    @Override // d0.w.i.a.a
    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        m.checkNotNullParameter(continuation, "completion");
        return new k(this.this$0, this.$contentResolver, this.$uri, continuation);
    }

    @Override // kotlin.jvm.functions.Function2
    public final Object invoke(CoroutineScope coroutineScope, Continuation<? super Bitmap> continuation) {
        Continuation<? super Bitmap> continuation2 = continuation;
        m.checkNotNullParameter(continuation2, "completion");
        return new k(this.this$0, this.$contentResolver, this.$uri, continuation2).invokeSuspend(Unit.a);
    }

    @Override // d0.w.i.a.a
    public final Object invokeSuspend(Object obj) {
        c.getCOROUTINE_SUSPENDED();
        if (this.label == 0) {
            l.throwOnFailure(obj);
            try {
                return this.$contentResolver.loadThumbnail(this.$uri, new Size(i.j, i.k), this.this$0.f317b);
            } catch (Exception unused) {
                return null;
            }
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
