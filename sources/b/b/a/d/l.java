package b.b.a.d;

import andhook.lib.xposed.ClassUtils;
import android.content.ContentResolver;
import android.provider.MediaStore;
import android.util.Log;
import b.d.b.a.a;
import com.lytefast.flexinput.model.Media;
/* compiled from: ThumbnailViewHolder.kt */
/* loaded from: classes3.dex */
public final class l implements Runnable {
    public final /* synthetic */ m j;
    public final /* synthetic */ ContentResolver k;

    public l(m mVar, ContentResolver contentResolver) {
        this.j = mVar;
        this.k = contentResolver;
    }

    @Override // java.lang.Runnable
    public final void run() {
        try {
            MediaStore.Images.Thumbnails.getThumbnail(this.k, this.j.$id, 1, null);
        } catch (Exception unused) {
            String name = Media.class.getName();
            StringBuilder R = a.R("Error generating thumbnail for photo ");
            R.append(this.j.$id);
            R.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
            Log.v(name, R.toString());
        }
    }
}
