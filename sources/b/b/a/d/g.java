package b.b.a.d;

import d0.f0.n;
import d0.t.k;
import d0.z.d.m;
import d0.z.d.o;
import java.io.File;
import kotlin.jvm.functions.Function1;
import kotlin.sequences.Sequence;
/* compiled from: FileListAdapter.kt */
/* loaded from: classes3.dex */
public final class g extends o implements Function1<File, Sequence<? extends File>> {
    public static final g j = new g();

    public g() {
        super(1);
    }

    /* renamed from: a */
    public final Sequence<File> invoke(File file) {
        Sequence<File> asSequence;
        m.checkNotNullParameter(file, "$this$getFileList");
        File[] listFiles = file.listFiles();
        return (listFiles == null || (asSequence = k.asSequence(listFiles)) == null) ? n.emptySequence() : asSequence;
    }
}
