package b.b.a.g;

import android.net.Uri;
import com.lytefast.flexinput.model.Attachment;
import d0.z.d.m;
import java.io.File;
/* compiled from: FileUtils.kt */
/* loaded from: classes3.dex */
public final class a {
    public static final Attachment<File> a(File file) {
        m.checkNotNullParameter(file, "$this$toAttachment");
        Uri fromFile = Uri.fromFile(file);
        m.checkNotNullExpressionValue(fromFile, "Uri.fromFile(this)");
        String name = file.getName();
        m.checkNotNullExpressionValue(name, "this.name");
        return new Attachment<>(file.hashCode(), fromFile, name, file, false, 16, null);
    }
}
