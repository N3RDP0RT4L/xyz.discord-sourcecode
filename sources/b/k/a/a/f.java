package b.k.a.a;

import androidx.annotation.ColorInt;
/* compiled from: ColorPickerDialogListener.java */
/* loaded from: classes3.dex */
public interface f {
    void onColorReset(int i);

    void onColorSelected(int i, @ColorInt int i2);

    void onDialogDismissed(int i);
}
