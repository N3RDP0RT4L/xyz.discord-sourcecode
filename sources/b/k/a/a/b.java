package b.k.a.a;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import androidx.core.graphics.ColorUtils;
import androidx.core.view.ViewCompat;
import com.jaredrummler.android.colorpicker.ColorPanelView;
import com.jaredrummler.android.colorpicker.R;
/* compiled from: ColorPaletteAdapter.java */
/* loaded from: classes3.dex */
public class b extends BaseAdapter {
    public final a j;
    public final int[] k;
    public int l;
    public int m;

    /* compiled from: ColorPaletteAdapter.java */
    /* loaded from: classes3.dex */
    public interface a {
    }

    /* compiled from: ColorPaletteAdapter.java */
    /* renamed from: b.k.a.a.b$b  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public final class C0160b {
        public View a;

        /* renamed from: b  reason: collision with root package name */
        public ColorPanelView f1895b;
        public ImageView c;
        public int d;

        public C0160b(Context context) {
            int i;
            if (b.this.m == 0) {
                i = R.d.cpv_color_item_square;
            } else {
                i = R.d.cpv_color_item_circle;
            }
            View inflate = View.inflate(context, i, null);
            this.a = inflate;
            this.f1895b = (ColorPanelView) inflate.findViewById(R.c.cpv_color_panel_view);
            this.c = (ImageView) this.a.findViewById(R.c.cpv_color_image_view);
            this.d = this.f1895b.getBorderColor();
            this.a.setTag(this);
        }
    }

    public b(a aVar, int[] iArr, int i, int i2) {
        this.j = aVar;
        this.k = iArr;
        this.l = i;
        this.m = i2;
    }

    @Override // android.widget.Adapter
    public int getCount() {
        return this.k.length;
    }

    @Override // android.widget.Adapter
    public Object getItem(int i) {
        return Integer.valueOf(this.k[i]);
    }

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        return i;
    }

    @Override // android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        C0160b bVar;
        if (view == null) {
            bVar = new C0160b(viewGroup.getContext());
            view2 = bVar.a;
        } else {
            bVar = (C0160b) view.getTag();
            view2 = view;
        }
        int i2 = b.this.k[i];
        int alpha = Color.alpha(i2);
        bVar.f1895b.setColor(i2);
        bVar.c.setImageResource(b.this.l == i ? R.b.cpv_preset_checked : 0);
        if (alpha == 255) {
            b bVar2 = b.this;
            if (i != bVar2.l || ColorUtils.calculateLuminance(bVar2.k[i]) < 0.65d) {
                bVar.c.setColorFilter((ColorFilter) null);
            } else {
                bVar.c.setColorFilter(ViewCompat.MEASURED_STATE_MASK, PorterDuff.Mode.SRC_IN);
            }
        } else if (alpha <= 165) {
            bVar.f1895b.setBorderColor(i2 | ViewCompat.MEASURED_STATE_MASK);
            bVar.c.setColorFilter(ViewCompat.MEASURED_STATE_MASK, PorterDuff.Mode.SRC_IN);
        } else {
            bVar.f1895b.setBorderColor(bVar.d);
            bVar.c.setColorFilter(-1, PorterDuff.Mode.SRC_IN);
        }
        bVar.f1895b.setOnClickListener(new c(bVar, i));
        bVar.f1895b.setOnLongClickListener(new d(bVar));
        return view2;
    }
}
