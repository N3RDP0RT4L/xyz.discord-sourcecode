package b.i.e;

import b.i.e.n.a;
import b.i.e.n.b;
import b.i.e.n.h;
import com.google.zxing.NotFoundException;
import java.lang.reflect.Array;
/* compiled from: BinaryBitmap.java */
/* loaded from: classes3.dex */
public final class c {
    public final b a;

    /* renamed from: b  reason: collision with root package name */
    public b f1817b;

    public c(b bVar) {
        this.a = bVar;
    }

    public b a() throws NotFoundException {
        int i;
        c cVar = this;
        if (cVar.f1817b == null) {
            h hVar = (h) cVar.a;
            b bVar = hVar.e;
            if (bVar == null) {
                f fVar = hVar.a;
                int i2 = fVar.a;
                int i3 = fVar.f1819b;
                if (i2 < 40 || i3 < 40) {
                    b bVar2 = new b(i2, i3);
                    hVar.b(i2);
                    int[] iArr = hVar.d;
                    for (int i4 = 1; i4 < 5; i4++) {
                        byte[] b2 = fVar.b((i3 * i4) / 5, hVar.c);
                        int i5 = (i2 << 2) / 5;
                        for (int i6 = i2 / 5; i6 < i5; i6++) {
                            int i7 = (b2[i6] & 255) >> 3;
                            iArr[i7] = iArr[i7] + 1;
                        }
                    }
                    int a = h.a(iArr);
                    byte[] a2 = fVar.a();
                    for (int i8 = 0; i8 < i3; i8++) {
                        int i9 = i8 * i2;
                        for (int i10 = 0; i10 < i2; i10++) {
                            if ((a2[i9 + i10] & 255) < a) {
                                bVar2.j(i10, i8);
                            }
                        }
                    }
                    hVar.e = bVar2;
                } else {
                    byte[] a3 = fVar.a();
                    int i11 = i2 >> 3;
                    if ((i2 & 7) != 0) {
                        i11++;
                    }
                    int i12 = i3 >> 3;
                    if ((i3 & 7) != 0) {
                        i12++;
                    }
                    int i13 = i3 - 8;
                    int i14 = i2 - 8;
                    int[][] iArr2 = (int[][]) Array.newInstance(int.class, i12, i11);
                    int i15 = 0;
                    while (true) {
                        int i16 = 8;
                        if (i15 >= i12) {
                            break;
                        }
                        int i17 = i15 << 3;
                        if (i17 > i13) {
                            i17 = i13;
                        }
                        int i18 = 0;
                        while (i18 < i11) {
                            int i19 = i18 << 3;
                            if (i19 > i14) {
                                i19 = i14;
                            }
                            int i20 = (i17 * i2) + i19;
                            int i21 = 0;
                            int i22 = 0;
                            int i23 = 255;
                            int i24 = 0;
                            while (i21 < i16) {
                                i17 = i17;
                                int i25 = i21;
                                int i26 = i23;
                                int i27 = i24;
                                int i28 = 0;
                                while (i28 < i16) {
                                    int i29 = a3[i20 + i28] & 255;
                                    i22 += i29;
                                    if (i29 < i26) {
                                        i26 = i29;
                                    }
                                    if (i29 > i27) {
                                        i27 = i29;
                                    }
                                    i28++;
                                    i16 = 8;
                                }
                                if (i27 - i26 > 24) {
                                    i = i25;
                                    while (true) {
                                        i++;
                                        i20 += i2;
                                        if (i >= 8) {
                                            break;
                                        }
                                        int i30 = 0;
                                        for (int i31 = 8; i30 < i31; i31 = 8) {
                                            i26 = i26;
                                            i22 += a3[i20 + i30] & 255;
                                            i30++;
                                        }
                                    }
                                    i23 = i26;
                                } else {
                                    i23 = i26;
                                    i = i25;
                                }
                                i21 = i + 1;
                                i20 += i2;
                                i16 = 8;
                                i24 = i27;
                            }
                            i17 = i17;
                            int i32 = i22 >> 6;
                            int i33 = i23;
                            if (i24 - i33 <= 24) {
                                i32 = i33 / 2;
                                if (i15 > 0 && i18 > 0) {
                                    int i34 = i15 - 1;
                                    int i35 = i18 - 1;
                                    int i36 = (((iArr2[i15][i35] * 2) + iArr2[i34][i18]) + iArr2[i34][i35]) / 4;
                                    if (i33 < i36) {
                                        i32 = i36;
                                    }
                                }
                            }
                            iArr2[i15][i18] = i32;
                            i18++;
                            i16 = 8;
                        }
                        i15++;
                    }
                    b bVar3 = new b(i2, i3);
                    for (int i37 = 0; i37 < i12; i37++) {
                        int i38 = i37 << 3;
                        if (i38 > i13) {
                            i38 = i13;
                        }
                        int i39 = i12 - 3;
                        if (i37 < 2) {
                            i39 = 2;
                        } else if (i37 <= i39) {
                            i39 = i37;
                        }
                        for (int i40 = 0; i40 < i11; i40++) {
                            int i41 = i40 << 3;
                            if (i41 > i14) {
                                i41 = i14;
                            }
                            int i42 = i11 - 3;
                            if (i40 < 2) {
                                i42 = 2;
                            } else if (i40 <= i42) {
                                i42 = i40;
                            }
                            i11 = i11;
                            int i43 = -2;
                            int i44 = 0;
                            for (int i45 = 2; i43 <= i45; i45 = 2) {
                                int[] iArr3 = iArr2[i39 + i43];
                                i44 = iArr3[i42 - 2] + iArr3[i42 - 1] + iArr3[i42] + iArr3[i42 + 1] + iArr3[i42 + 2] + i44;
                                i43++;
                            }
                            int i46 = i44 / 25;
                            int i47 = (i38 * i2) + i41;
                            i39 = i39;
                            int i48 = 8;
                            int i49 = 0;
                            while (i49 < i48) {
                                i12 = i12;
                                int i50 = 0;
                                while (i50 < i48) {
                                    byte b3 = a3[i47 + i50];
                                    a3 = a3;
                                    if ((b3 & 255) <= i46) {
                                        bVar3.j(i41 + i50, i38 + i49);
                                    }
                                    i50++;
                                    i48 = 8;
                                }
                                i49++;
                                i47 += i2;
                                i48 = 8;
                            }
                        }
                    }
                    hVar.e = bVar3;
                }
                bVar = hVar.e;
            }
            cVar = this;
            cVar.f1817b = bVar;
        }
        return cVar.f1817b;
    }

    public a b(int i, a aVar) throws NotFoundException {
        int i2;
        h hVar = (h) this.a;
        f fVar = hVar.a;
        int i3 = fVar.a;
        if (aVar.k < i3) {
            aVar = new a(i3);
        } else {
            int length = aVar.j.length;
            for (int i4 = 0; i4 < length; i4++) {
                aVar.j[i4] = 0;
            }
        }
        hVar.b(i3);
        byte[] b2 = fVar.b(i, hVar.c);
        int[] iArr = hVar.d;
        int i5 = 0;
        while (true) {
            i2 = 1;
            if (i5 >= i3) {
                break;
            }
            int i6 = (b2[i5] & 255) >> 3;
            iArr[i6] = iArr[i6] + 1;
            i5++;
        }
        int a = h.a(iArr);
        if (i3 < 3) {
            for (int i7 = 0; i7 < i3; i7++) {
                if ((b2[i7] & 255) < a) {
                    aVar.j(i7);
                }
            }
        } else {
            int i8 = b2[0] & 255;
            int i9 = b2[1] & 255;
            while (i2 < i3 - 1) {
                int i10 = i2 + 1;
                int i11 = b2[i10] & 255;
                if ((((i9 << 2) - i8) - i11) / 2 < a) {
                    aVar.j(i2);
                }
                i8 = i9;
                i2 = i10;
                i9 = i11;
            }
        }
        return aVar;
    }

    public String toString() {
        try {
            return a().toString();
        } catch (NotFoundException unused) {
            return "";
        }
    }
}
