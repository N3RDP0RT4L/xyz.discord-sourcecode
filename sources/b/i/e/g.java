package b.i.e;

import b.i.e.m.b;
import b.i.e.q.i;
import b.i.e.s.a;
import com.google.zxing.NotFoundException;
import com.google.zxing.ReaderException;
import com.google.zxing.Result;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
/* compiled from: MultiFormatReader.java */
/* loaded from: classes3.dex */
public final class g implements i {
    public Map<d, ?> a;

    /* renamed from: b  reason: collision with root package name */
    public i[] f1820b;

    @Override // b.i.e.i
    public Result a(c cVar, Map<d, ?> map) throws NotFoundException {
        c(map);
        return b(cVar);
    }

    public final Result b(c cVar) throws NotFoundException {
        i[] iVarArr = this.f1820b;
        if (iVarArr != null) {
            for (i iVar : iVarArr) {
                try {
                    return iVar.a(cVar, this.a);
                } catch (ReaderException unused) {
                }
            }
        }
        throw NotFoundException.l;
    }

    public void c(Map<d, ?> map) {
        this.a = map;
        boolean z2 = false;
        boolean z3 = map != null && map.containsKey(d.TRY_HARDER);
        Collection collection = map == null ? null : (Collection) map.get(d.POSSIBLE_FORMATS);
        ArrayList arrayList = new ArrayList();
        if (collection != null) {
            if (collection.contains(a.UPC_A) || collection.contains(a.UPC_E) || collection.contains(a.EAN_13) || collection.contains(a.EAN_8) || collection.contains(a.CODABAR) || collection.contains(a.CODE_39) || collection.contains(a.CODE_93) || collection.contains(a.CODE_128) || collection.contains(a.ITF) || collection.contains(a.RSS_14) || collection.contains(a.RSS_EXPANDED)) {
                z2 = true;
            }
            if (z2 && !z3) {
                arrayList.add(new i(map));
            }
            if (collection.contains(a.QR_CODE)) {
                arrayList.add(new a());
            }
            if (collection.contains(a.DATA_MATRIX)) {
                arrayList.add(new b.i.e.o.a());
            }
            if (collection.contains(a.AZTEC)) {
                arrayList.add(new b());
            }
            if (collection.contains(a.PDF_417)) {
                arrayList.add(new b.i.e.r.b());
            }
            if (collection.contains(a.MAXICODE)) {
                arrayList.add(new b.i.e.p.a());
            }
            if (z2 && z3) {
                arrayList.add(new i(map));
            }
        }
        if (arrayList.isEmpty()) {
            if (!z3) {
                arrayList.add(new i(map));
            }
            arrayList.add(new a());
            arrayList.add(new b.i.e.o.a());
            arrayList.add(new b());
            arrayList.add(new b.i.e.r.b());
            arrayList.add(new b.i.e.p.a());
            if (z3) {
                arrayList.add(new i(map));
            }
        }
        this.f1820b = (i[]) arrayList.toArray(new i[arrayList.size()]);
    }

    @Override // b.i.e.i
    public void reset() {
        i[] iVarArr = this.f1820b;
        if (iVarArr != null) {
            for (i iVar : iVarArr) {
                iVar.reset();
            }
        }
    }
}
