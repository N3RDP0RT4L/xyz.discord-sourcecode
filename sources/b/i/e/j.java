package b.i.e;
/* compiled from: ResultMetadataType.java */
/* loaded from: classes3.dex */
public enum j {
    OTHER,
    ORIENTATION,
    BYTE_SEGMENTS,
    ERROR_CORRECTION_LEVEL,
    ISSUE_NUMBER,
    SUGGESTED_PRICE,
    POSSIBLE_COUNTRY,
    UPC_EAN_EXTENSION,
    PDF417_EXTRA_METADATA,
    STRUCTURED_APPEND_SEQUENCE,
    STRUCTURED_APPEND_PARITY
}
