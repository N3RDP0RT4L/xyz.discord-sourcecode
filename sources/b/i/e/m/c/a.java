package b.i.e.m.c;

import androidx.exifinterface.media.ExifInterface;
import b.i.e.n.b;
import b.i.e.n.e;
import b.i.e.n.l.c;
import com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel;
import com.google.android.material.badge.BadgeDrawable;
import com.google.zxing.FormatException;
import com.google.zxing.ReaderException;
import com.google.zxing.common.reedsolomon.ReedSolomonException;
import java.util.Arrays;
/* compiled from: Decoder.java */
/* loaded from: classes3.dex */
public final class a {
    public static final String[] a = {"CTRL_PS", " ", ExifInterface.GPS_MEASUREMENT_IN_PROGRESS, "B", "C", "D", ExifInterface.LONGITUDE_EAST, "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", ExifInterface.LATITUDE_SOUTH, ExifInterface.GPS_DIRECTION_TRUE, "U", ExifInterface.GPS_MEASUREMENT_INTERRUPTED, ExifInterface.LONGITUDE_WEST, "X", "Y", "Z", "CTRL_LL", "CTRL_ML", "CTRL_DL", "CTRL_BS"};

    /* renamed from: b  reason: collision with root package name */
    public static final String[] f1823b = {"CTRL_PS", " ", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "CTRL_US", "CTRL_ML", "CTRL_DL", "CTRL_BS"};
    public static final String[] c = {"CTRL_PS", " ", "\u0001", "\u0002", "\u0003", "\u0004", "\u0005", "\u0006", "\u0007", "\b", "\t", "\n", "\u000b", "\f", "\r", "\u001b", "\u001c", "\u001d", "\u001e", "\u001f", "@", "\\", "^", "_", "`", "|", "~", "\u007f", "CTRL_LL", "CTRL_UL", "CTRL_PL", "CTRL_BS"};
    public static final String[] d = {"", "\r", "\r\n", ". ", ", ", ": ", "!", "\"", "#", "$", "%", "&", "'", "(", ")", "*", BadgeDrawable.DEFAULT_EXCEED_MAX_BADGE_NUMBER_SUFFIX, ",", "-", ".", AutocompleteViewModel.COMMAND_DISCOVER_TOKEN, ":", ";", "<", "=", ">", "?", "[", "]", "{", "}", "CTRL_UL"};
    public static final String[] e = {"CTRL_PS", " ", "0", "1", ExifInterface.GPS_MEASUREMENT_2D, ExifInterface.GPS_MEASUREMENT_3D, "4", "5", "6", "7", "8", "9", ",", ".", "CTRL_UL", "CTRL_US"};
    public b.i.e.m.a f;

    public static int b(boolean[] zArr, int i, int i2) {
        int i3 = 0;
        for (int i4 = i; i4 < i + i2; i4++) {
            i3 <<= 1;
            if (zArr[i4]) {
                i3 |= 1;
            }
        }
        return i3;
    }

    public e a(b.i.e.m.a aVar) throws FormatException {
        int i;
        b.i.e.n.l.a aVar2;
        String str;
        int i2;
        int i3;
        this.f = aVar;
        b bVar = aVar.a;
        boolean z2 = aVar.c;
        int i4 = aVar.e;
        int i5 = (z2 ? 11 : 14) + (i4 << 2);
        int[] iArr = new int[i5];
        int i6 = ((z2 ? 88 : 112) + (i4 << 4)) * i4;
        boolean[] zArr = new boolean[i6];
        int i7 = 2;
        if (z2) {
            for (int i8 = 0; i8 < i5; i8++) {
                iArr[i8] = i8;
            }
        } else {
            int i9 = i5 / 2;
            int i10 = ((((i9 - 1) / 15) * 2) + (i5 + 1)) / 2;
            for (int i11 = 0; i11 < i9; i11++) {
                iArr[(i9 - i11) - 1] = (i10 - i3) - 1;
                iArr[i9 + i11] = (i11 / 15) + i11 + i10 + 1;
            }
        }
        int i12 = 0;
        int i13 = 0;
        while (true) {
            i = 12;
            if (i12 >= i4) {
                break;
            }
            int i14 = (i4 - i12) << i7;
            if (z2) {
                i = 9;
            }
            int i15 = i14 + i;
            int i16 = i12 << 1;
            int i17 = (i5 - 1) - i16;
            int i18 = 0;
            while (i18 < i15) {
                int i19 = i18 << 1;
                int i20 = 0;
                while (i20 < i7) {
                    int i21 = i16 + i20;
                    int i22 = i16 + i18;
                    zArr[i13 + i19 + i20] = bVar.f(iArr[i21], iArr[i22]);
                    int i23 = i17 - i20;
                    i4 = i4;
                    zArr[(i15 * 2) + i13 + i19 + i20] = bVar.f(iArr[i22], iArr[i23]);
                    int i24 = i17 - i18;
                    z2 = z2;
                    zArr[(i15 * 4) + i13 + i19 + i20] = bVar.f(iArr[i23], iArr[i24]);
                    zArr[(i15 * 6) + i13 + i19 + i20] = bVar.f(iArr[i24], iArr[i21]);
                    i20++;
                    i7 = 2;
                }
                i18++;
                i7 = 2;
            }
            i4 = i4;
            i13 += i15 << 3;
            i12++;
            i7 = 2;
        }
        b.i.e.m.a aVar3 = this.f;
        int i25 = aVar3.e;
        int i26 = 8;
        if (i25 <= 2) {
            aVar2 = b.i.e.n.l.a.c;
            i = 6;
        } else if (i25 <= 8) {
            aVar2 = b.i.e.n.l.a.g;
            i = 8;
        } else if (i25 <= 22) {
            i = 10;
            aVar2 = b.i.e.n.l.a.f1837b;
        } else {
            aVar2 = b.i.e.n.l.a.a;
        }
        int i27 = aVar3.d;
        int i28 = i6 / i;
        if (i28 >= i27) {
            int i29 = i6 % i;
            int[] iArr2 = new int[i28];
            int i30 = 0;
            while (i30 < i28) {
                iArr2[i30] = b(zArr, i29, i);
                i30++;
                i29 += i;
            }
            try {
                new c(aVar2).a(iArr2, i28 - i27);
                int i31 = 1;
                int i32 = (1 << i) - 1;
                int i33 = 0;
                int i34 = 0;
                while (i33 < i27) {
                    int i35 = iArr2[i33];
                    if (i35 == 0 || i35 == i32) {
                        throw FormatException.a();
                    }
                    if (i35 == i31 || i35 == i32 - 1) {
                        i34++;
                    }
                    i33++;
                    i31 = 1;
                }
                int i36 = (i27 * i) - i34;
                boolean[] zArr2 = new boolean[i36];
                int i37 = 0;
                for (int i38 = 0; i38 < i27; i38++) {
                    int i39 = iArr2[i38];
                    int i40 = 1;
                    if (i39 == 1 || i39 == i32 - 1) {
                        Arrays.fill(zArr2, i37, (i37 + i) - 1, i39 > 1);
                        i37 = (i - 1) + i37;
                    } else {
                        int i41 = i - 1;
                        while (i41 >= 0) {
                            i37++;
                            zArr2[i37] = ((i40 << i41) & i39) != 0;
                            i41--;
                            i40 = 1;
                        }
                    }
                }
                int i42 = (i36 + 7) / 8;
                byte[] bArr = new byte[i42];
                for (int i43 = 0; i43 < i42; i43++) {
                    int i44 = i43 << 3;
                    int i45 = i36 - i44;
                    if (i45 >= 8) {
                        i2 = b(zArr2, i44, 8);
                    } else {
                        i2 = b(zArr2, i44, i45) << (8 - i45);
                    }
                    bArr[i43] = (byte) i2;
                }
                StringBuilder sb = new StringBuilder(20);
                int i46 = 1;
                int i47 = 1;
                int i48 = 0;
                while (i48 < i36) {
                    if (i46 == 6) {
                        if (i36 - i48 < 5) {
                            break;
                        }
                        int b2 = b(zArr2, i48, 5);
                        i48 += 5;
                        if (b2 == 0) {
                            if (i36 - i48 < 11) {
                                break;
                            }
                            b2 = b(zArr2, i48, 11) + 31;
                            i48 += 11;
                        }
                        int i49 = 0;
                        while (true) {
                            if (i49 >= b2) {
                                break;
                            } else if (i36 - i48 < i26) {
                                i48 = i36;
                                break;
                            } else {
                                sb.append((char) b(zArr2, i48, i26));
                                i48 += 8;
                                i49++;
                            }
                        }
                        i46 = i47;
                    } else {
                        int i50 = i46 == 4 ? 4 : 5;
                        if (i36 - i48 < i50) {
                            break;
                        }
                        int b3 = b(zArr2, i48, i50);
                        i48 += i50;
                        int h = b.c.a.y.b.h(i46);
                        int i51 = 3;
                        if (h == 0) {
                            str = a[b3];
                        } else if (h == 1) {
                            str = f1823b[b3];
                        } else if (h == 2) {
                            str = c[b3];
                        } else if (h == 3) {
                            str = e[b3];
                        } else if (h == 4) {
                            str = d[b3];
                        } else {
                            throw new IllegalStateException("Bad table");
                        }
                        if (str.startsWith("CTRL_")) {
                            char charAt = str.charAt(5);
                            if (charAt == 'B') {
                                i51 = 6;
                            } else if (charAt == 'D') {
                                i51 = 4;
                            } else if (charAt == 'P') {
                                i51 = 5;
                            } else if (charAt == 'L') {
                                i51 = 2;
                            } else if (charAt != 'M') {
                                i51 = 1;
                            }
                            if (str.charAt(6) == 'L') {
                                i47 = i51;
                            } else {
                                i47 = i46;
                                i46 = i51;
                            }
                        } else {
                            sb.append(str);
                        }
                        i46 = i47;
                    }
                    i26 = 8;
                }
                e eVar = new e(bArr, sb.toString(), null, null);
                eVar.f1831b = i36;
                return eVar;
            } catch (ReedSolomonException e2) {
                if (ReaderException.j) {
                    throw new FormatException(e2);
                }
                throw FormatException.l;
            }
        } else {
            throw FormatException.a();
        }
    }
}
