package b.i.e.m.d;

import b.i.a.f.e.o.f;
import b.i.e.k;
import b.i.e.n.b;
import b.i.e.n.i;
import b.i.e.n.l.c;
import com.google.zxing.NotFoundException;
import com.google.zxing.common.reedsolomon.ReedSolomonException;
import org.objectweb.asm.Opcodes;
/* compiled from: Detector.java */
/* loaded from: classes3.dex */
public final class a {
    public static final int[] a = {3808, 476, 2107, 1799};

    /* renamed from: b  reason: collision with root package name */
    public final b f1824b;
    public boolean c;
    public int d;
    public int e;
    public int f;
    public int g;

    /* compiled from: Detector.java */
    /* renamed from: b.i.e.m.d.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0157a {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final int f1825b;

        public C0157a(int i, int i2) {
            this.a = i;
            this.f1825b = i2;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder("<");
            sb.append(this.a);
            sb.append(' ');
            return b.d.b.a.a.z(sb, this.f1825b, '>');
        }
    }

    public a(b bVar) {
        this.f1824b = bVar;
    }

    public static k[] b(k[] kVarArr, int i, int i2) {
        float f = i2 / (i * 2.0f);
        float f2 = kVarArr[0].a - kVarArr[2].a;
        float f3 = kVarArr[0].f1822b - kVarArr[2].f1822b;
        float f4 = (kVarArr[0].a + kVarArr[2].a) / 2.0f;
        float f5 = (kVarArr[0].f1822b + kVarArr[2].f1822b) / 2.0f;
        float f6 = f2 * f;
        float f7 = f3 * f;
        k kVar = new k(f4 + f6, f5 + f7);
        k kVar2 = new k(f4 - f6, f5 - f7);
        float f8 = kVarArr[1].a - kVarArr[3].a;
        float f9 = kVarArr[1].f1822b - kVarArr[3].f1822b;
        float f10 = (kVarArr[1].a + kVarArr[3].a) / 2.0f;
        float f11 = (kVarArr[1].f1822b + kVarArr[3].f1822b) / 2.0f;
        float f12 = f8 * f;
        float f13 = f * f9;
        return new k[]{kVar, new k(f10 + f12, f11 + f13), kVar2, new k(f10 - f12, f11 - f13)};
    }

    public b.i.e.m.a a(boolean z2) throws NotFoundException {
        k kVar;
        k kVar2;
        k kVar3;
        k kVar4;
        k kVar5;
        k kVar6;
        k kVar7;
        k kVar8;
        int i;
        int i2;
        long j;
        C0157a aVar;
        int i3 = 2;
        int i4 = -1;
        int i5 = 1;
        try {
            b bVar = this.f1824b;
            k[] b2 = new b.i.e.n.k.a(bVar, 10, bVar.j / 2, bVar.k / 2).b();
            kVar3 = b2[0];
            kVar2 = b2[1];
            kVar = b2[2];
            kVar4 = b2[3];
        } catch (NotFoundException unused) {
            b bVar2 = this.f1824b;
            int i6 = bVar2.j / 2;
            int i7 = bVar2.k / 2;
            int i8 = i7 - 7;
            int i9 = i6 + 7 + 1;
            int i10 = i9;
            int i11 = i8;
            while (true) {
                i11--;
                if (!f(i10, i11) || this.f1824b.f(i10, i11)) {
                    break;
                }
                i10++;
            }
            int i12 = i10 - 1;
            int i13 = i11 + 1;
            while (f(i12, i13) && !this.f1824b.f(i12, i13)) {
                i12++;
            }
            int i14 = i12 - 1;
            while (f(i14, i13) && !this.f1824b.f(i14, i13)) {
                i13--;
            }
            k kVar9 = new k(i14, i13 + 1);
            int i15 = i7 + 7;
            int i16 = i15;
            while (true) {
                i16++;
                if (!f(i9, i16) || this.f1824b.f(i9, i16)) {
                    break;
                }
                i9++;
            }
            int i17 = i9 - 1;
            int i18 = i16 - 1;
            while (f(i17, i18) && !this.f1824b.f(i17, i18)) {
                i17++;
            }
            int i19 = i17 - 1;
            while (f(i19, i18) && !this.f1824b.f(i19, i18)) {
                i18++;
            }
            k kVar10 = new k(i19, i18 - 1);
            int i20 = i6 - 7;
            int i21 = i20 - 1;
            while (true) {
                i15++;
                if (!f(i21, i15) || this.f1824b.f(i21, i15)) {
                    break;
                }
                i21--;
            }
            int i22 = i21 + 1;
            int i23 = i15 - 1;
            while (f(i22, i23) && !this.f1824b.f(i22, i23)) {
                i22--;
            }
            int i24 = i22 + 1;
            while (f(i24, i23) && !this.f1824b.f(i24, i23)) {
                i23++;
            }
            k kVar11 = new k(i24, i23 - 1);
            do {
                i20--;
                i8--;
                if (!f(i20, i8)) {
                    break;
                }
            } while (!this.f1824b.f(i20, i8));
            int i25 = i20 + 1;
            int i26 = i8 + 1;
            while (f(i25, i26) && !this.f1824b.f(i25, i26)) {
                i25--;
            }
            int i27 = i25 + 1;
            while (f(i27, i26) && !this.f1824b.f(i27, i26)) {
                i26--;
            }
            kVar4 = new k(i27, i26 + 1);
            kVar = kVar11;
            kVar2 = kVar10;
            kVar3 = kVar9;
        }
        int Z0 = f.Z0((((kVar3.a + kVar4.a) + kVar2.a) + kVar.a) / 4.0f);
        int Z02 = f.Z0((((kVar3.f1822b + kVar4.f1822b) + kVar2.f1822b) + kVar.f1822b) / 4.0f);
        try {
            k[] b3 = new b.i.e.n.k.a(this.f1824b, 15, Z0, Z02).b();
            kVar7 = b3[0];
            kVar6 = b3[1];
            kVar5 = b3[2];
            kVar8 = b3[3];
        } catch (NotFoundException unused2) {
            int i28 = Z02 - 7;
            int i29 = Z0 + 7 + 1;
            int i30 = i29;
            int i31 = i28;
            while (true) {
                i31--;
                if (!f(i30, i31) || this.f1824b.f(i30, i31)) {
                    break;
                }
                i30++;
            }
            int i32 = i30 - 1;
            int i33 = i31 + 1;
            while (f(i32, i33) && !this.f1824b.f(i32, i33)) {
                i32++;
            }
            int i34 = i32 - 1;
            while (f(i34, i33) && !this.f1824b.f(i34, i33)) {
                i33--;
            }
            k kVar12 = new k(i34, i33 + 1);
            int i35 = Z02 + 7;
            int i36 = i35;
            while (true) {
                i36++;
                if (!f(i29, i36) || this.f1824b.f(i29, i36)) {
                    break;
                }
                i29++;
            }
            int i37 = i29 - 1;
            int i38 = i36 - 1;
            while (f(i37, i38) && !this.f1824b.f(i37, i38)) {
                i37++;
            }
            int i39 = i37 - 1;
            while (f(i39, i38) && !this.f1824b.f(i39, i38)) {
                i38++;
            }
            k kVar13 = new k(i39, i38 - 1);
            int i40 = Z0 - 7;
            int i41 = i40 - 1;
            while (true) {
                i35++;
                if (!f(i41, i35) || this.f1824b.f(i41, i35)) {
                    break;
                }
                i41--;
            }
            int i42 = i41 + 1;
            int i43 = i35 - 1;
            while (f(i42, i43) && !this.f1824b.f(i42, i43)) {
                i42--;
            }
            int i44 = i42 + 1;
            while (f(i44, i43) && !this.f1824b.f(i44, i43)) {
                i43++;
            }
            k kVar14 = new k(i44, i43 - 1);
            do {
                i40--;
                i28--;
                if (!f(i40, i28)) {
                    break;
                }
            } while (!this.f1824b.f(i40, i28));
            int i45 = i40 + 1;
            int i46 = i28 + 1;
            while (f(i45, i46) && !this.f1824b.f(i45, i46)) {
                i45--;
            }
            int i47 = i45 + 1;
            while (f(i47, i46) && !this.f1824b.f(i47, i46)) {
                i46--;
            }
            kVar8 = new k(i47, i46 + 1);
            kVar7 = kVar12;
            kVar5 = kVar14;
            kVar6 = kVar13;
        }
        C0157a aVar2 = new C0157a(f.Z0((((kVar7.a + kVar8.a) + kVar6.a) + kVar5.a) / 4.0f), f.Z0((((kVar7.f1822b + kVar8.f1822b) + kVar6.f1822b) + kVar5.f1822b) / 4.0f));
        this.f = 1;
        C0157a aVar3 = aVar2;
        C0157a aVar4 = aVar3;
        C0157a aVar5 = aVar4;
        boolean z3 = true;
        while (this.f < 9) {
            C0157a e = e(aVar2, z3, i5, i4);
            C0157a e2 = e(aVar3, z3, i5, i5);
            C0157a e3 = e(aVar4, z3, i4, i5);
            C0157a e4 = e(aVar5, z3, i4, i4);
            if (this.f > i3) {
                double R = (f.R(e4.a, e4.f1825b, e.a, e.f1825b) * this.f) / (f.R(aVar5.a, aVar5.f1825b, aVar2.a, aVar2.f1825b) * (this.f + i3));
                if (R < 0.75d || R > 1.25d) {
                    break;
                }
                C0157a aVar6 = new C0157a(e.a - 3, e.f1825b + 3);
                C0157a aVar7 = new C0157a(e2.a - 3, e2.f1825b - 3);
                C0157a aVar8 = new C0157a(e3.a + 3, e3.f1825b - 3);
                aVar = e;
                C0157a aVar9 = new C0157a(e4.a + 3, e4.f1825b + 3);
                int c = c(aVar9, aVar6);
                if (!(c != 0 && c(aVar6, aVar7) == c && c(aVar7, aVar8) == c && c(aVar8, aVar9) == c)) {
                    break;
                }
            } else {
                aVar = e;
            }
            z3 = !z3;
            this.f++;
            aVar5 = e4;
            aVar3 = e2;
            aVar4 = e3;
            aVar2 = aVar;
            i3 = 2;
            i4 = -1;
            i5 = 1;
        }
        int i48 = this.f;
        if (i48 == 5 || i48 == 7) {
            this.c = i48 == 5;
            int i49 = i48 * 2;
            k[] b4 = b(new k[]{new k(aVar2.a + 0.5f, aVar2.f1825b - 0.5f), new k(aVar3.a + 0.5f, aVar3.f1825b + 0.5f), new k(aVar4.a - 0.5f, aVar4.f1825b + 0.5f), new k(aVar5.a - 0.5f, aVar5.f1825b - 0.5f)}, i49 - 3, i49);
            if (z2) {
                k kVar15 = b4[0];
                b4[0] = b4[2];
                b4[2] = kVar15;
            }
            if (!g(b4[0]) || !g(b4[1]) || !g(b4[2]) || !g(b4[3])) {
                throw NotFoundException.l;
            }
            int i50 = this.f * 2;
            int[] iArr = {h(b4[0], b4[1], i50), h(b4[1], b4[2], i50), h(b4[2], b4[3], i50), h(b4[3], b4[0], i50)};
            int i51 = 0;
            for (int i52 = 0; i52 < 4; i52++) {
                int i53 = iArr[i52];
                i51 = (i51 << 3) + ((i53 >> (i50 - 2)) << 1) + (i53 & 1);
            }
            int i54 = ((i51 & 1) << 11) + (i51 >> 1);
            for (int i55 = 0; i55 < 4; i55++) {
                if (Integer.bitCount(a[i55] ^ i54) <= 2) {
                    this.g = i55;
                    long j2 = 0;
                    for (int i56 = 0; i56 < 4; i56++) {
                        int i57 = iArr[(this.g + i56) % 4];
                        if (this.c) {
                            j = j2 << 7;
                            i2 = (i57 >> 1) & Opcodes.LAND;
                        } else {
                            j = j2 << 10;
                            i2 = ((i57 >> 1) & 31) + ((i57 >> 2) & 992);
                        }
                        j2 = j + i2;
                    }
                    int i58 = 7;
                    if (this.c) {
                        i = 2;
                    } else {
                        i = 4;
                        i58 = 10;
                    }
                    int i59 = i58 - i;
                    int[] iArr2 = new int[i58];
                    while (true) {
                        i58--;
                        if (i58 >= 0) {
                            iArr2[i58] = ((int) j2) & 15;
                            j2 >>= 4;
                        } else {
                            try {
                                break;
                            } catch (ReedSolomonException unused3) {
                                throw NotFoundException.l;
                            }
                        }
                    }
                    new c(b.i.e.n.l.a.d).a(iArr2, i59);
                    int i60 = 0;
                    for (int i61 = 0; i61 < i; i61++) {
                        i60 = (i60 << 4) + iArr2[i61];
                    }
                    if (this.c) {
                        this.d = (i60 >> 6) + 1;
                        this.e = (i60 & 63) + 1;
                    } else {
                        this.d = (i60 >> 11) + 1;
                        this.e = (i60 & 2047) + 1;
                    }
                    b bVar3 = this.f1824b;
                    int i62 = this.g;
                    k kVar16 = b4[i62 % 4];
                    k kVar17 = b4[(i62 + 1) % 4];
                    k kVar18 = b4[(i62 + 2) % 4];
                    k kVar19 = b4[(i62 + 3) % 4];
                    b.i.e.n.f fVar = b.i.e.n.f.a;
                    int d = d();
                    float f = d / 2.0f;
                    float f2 = this.f;
                    float f3 = f - f2;
                    float f4 = f + f2;
                    return new b.i.e.m.a(fVar.a(bVar3, d, d, i.a(f3, f3, f4, f3, f4, f4, f3, f4, kVar16.a, kVar16.f1822b, kVar17.a, kVar17.f1822b, kVar18.a, kVar18.f1822b, kVar19.a, kVar19.f1822b)), b(b4, this.f * 2, d()), this.c, this.e, this.d);
                }
            }
            throw NotFoundException.l;
        }
        throw NotFoundException.l;
    }

    public final int c(C0157a aVar, C0157a aVar2) {
        float R = f.R(aVar.a, aVar.f1825b, aVar2.a, aVar2.f1825b);
        int i = aVar2.a;
        int i2 = aVar.a;
        float f = (i - i2) / R;
        int i3 = aVar2.f1825b;
        int i4 = aVar.f1825b;
        float f2 = (i3 - i4) / R;
        float f3 = i2;
        float f4 = i4;
        boolean f5 = this.f1824b.f(i2, i4);
        int ceil = (int) Math.ceil(R);
        boolean z2 = false;
        int i5 = 0;
        for (int i6 = 0; i6 < ceil; i6++) {
            f3 += f;
            f4 += f2;
            if (this.f1824b.f(f.Z0(f3), f.Z0(f4)) != f5) {
                i5++;
            }
        }
        float f6 = i5 / R;
        if (f6 > 0.1f && f6 < 0.9f) {
            return 0;
        }
        if (f6 <= 0.1f) {
            z2 = true;
        }
        return z2 == f5 ? 1 : -1;
    }

    public final int d() {
        if (this.c) {
            return (this.d * 4) + 11;
        }
        int i = this.d;
        if (i <= 4) {
            return (i * 4) + 15;
        }
        return ((((i - 4) / 8) + 1) * 2) + (i * 4) + 15;
    }

    public final C0157a e(C0157a aVar, boolean z2, int i, int i2) {
        int i3 = aVar.a + i;
        int i4 = aVar.f1825b;
        while (true) {
            i4 += i2;
            if (!f(i3, i4) || this.f1824b.f(i3, i4) != z2) {
                break;
            }
            i3 += i;
        }
        int i5 = i3 - i;
        int i6 = i4 - i2;
        while (f(i5, i6) && this.f1824b.f(i5, i6) == z2) {
            i5 += i;
        }
        int i7 = i5 - i;
        while (f(i7, i6) && this.f1824b.f(i7, i6) == z2) {
            i6 += i2;
        }
        return new C0157a(i7, i6 - i2);
    }

    public final boolean f(int i, int i2) {
        if (i < 0) {
            return false;
        }
        b bVar = this.f1824b;
        return i < bVar.j && i2 > 0 && i2 < bVar.k;
    }

    public final boolean g(k kVar) {
        return f(f.Z0(kVar.a), f.Z0(kVar.f1822b));
    }

    public final int h(k kVar, k kVar2, int i) {
        float Q = f.Q(kVar.a, kVar.f1822b, kVar2.a, kVar2.f1822b);
        float f = Q / i;
        float f2 = kVar.a;
        float f3 = kVar.f1822b;
        float f4 = ((kVar2.a - f2) * f) / Q;
        float f5 = ((kVar2.f1822b - f3) * f) / Q;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            float f6 = i3;
            if (this.f1824b.f(f.Z0((f6 * f4) + f2), f.Z0((f6 * f5) + f3))) {
                i2 |= 1 << ((i - i3) - 1);
            }
        }
        return i2;
    }
}
