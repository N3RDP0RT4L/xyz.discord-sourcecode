package b.i.e.m;

import b.i.e.i;
/* compiled from: AztecReader.java */
/* loaded from: classes3.dex */
public final class b implements i {
    /* JADX WARN: Removed duplicated region for block: B:15:0x002f  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x004d  */
    /* JADX WARN: Removed duplicated region for block: B:29:0x005a A[LOOP:0: B:28:0x0058->B:29:0x005a, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:32:0x0078  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x0081  */
    @Override // b.i.e.i
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public com.google.zxing.Result a(b.i.e.c r11, java.util.Map<b.i.e.d, ?> r12) throws com.google.zxing.NotFoundException, com.google.zxing.FormatException {
        /*
            r10 = this;
            b.i.e.m.d.a r0 = new b.i.e.m.d.a
            b.i.e.n.b r11 = r11.a()
            r0.<init>(r11)
            r11 = 0
            r1 = 0
            b.i.e.m.a r2 = r0.a(r11)     // Catch: com.google.zxing.FormatException -> L23 com.google.zxing.NotFoundException -> L29
            b.i.e.k[] r3 = r2.f1832b     // Catch: com.google.zxing.FormatException -> L23 com.google.zxing.NotFoundException -> L29
            b.i.e.m.c.a r4 = new b.i.e.m.c.a     // Catch: com.google.zxing.FormatException -> L1f com.google.zxing.NotFoundException -> L21
            r4.<init>()     // Catch: com.google.zxing.FormatException -> L1f com.google.zxing.NotFoundException -> L21
            b.i.e.n.e r2 = r4.a(r2)     // Catch: com.google.zxing.FormatException -> L1f com.google.zxing.NotFoundException -> L21
            r4 = r3
            r3 = r1
            r1 = r2
            r2 = r3
            goto L2d
        L1f:
            r2 = move-exception
            goto L25
        L21:
            r2 = move-exception
            goto L2b
        L23:
            r2 = move-exception
            r3 = r1
        L25:
            r4 = r3
            r3 = r2
            r2 = r1
            goto L2d
        L29:
            r2 = move-exception
            r3 = r1
        L2b:
            r4 = r3
            r3 = r1
        L2d:
            if (r1 != 0) goto L4a
            r1 = 1
            b.i.e.m.a r0 = r0.a(r1)     // Catch: com.google.zxing.FormatException -> L40 com.google.zxing.NotFoundException -> L42
            b.i.e.k[] r4 = r0.f1832b     // Catch: com.google.zxing.FormatException -> L40 com.google.zxing.NotFoundException -> L42
            b.i.e.m.c.a r1 = new b.i.e.m.c.a     // Catch: com.google.zxing.FormatException -> L40 com.google.zxing.NotFoundException -> L42
            r1.<init>()     // Catch: com.google.zxing.FormatException -> L40 com.google.zxing.NotFoundException -> L42
            b.i.e.n.e r1 = r1.a(r0)     // Catch: com.google.zxing.FormatException -> L40 com.google.zxing.NotFoundException -> L42
            goto L4a
        L40:
            r11 = move-exception
            goto L43
        L42:
            r11 = move-exception
        L43:
            if (r2 != 0) goto L49
            if (r3 == 0) goto L48
            throw r3
        L48:
            throw r11
        L49:
            throw r2
        L4a:
            r6 = r4
            if (r12 == 0) goto L62
            b.i.e.d r0 = b.i.e.d.NEED_RESULT_POINT_CALLBACK
            java.lang.Object r12 = r12.get(r0)
            b.i.e.l r12 = (b.i.e.l) r12
            if (r12 == 0) goto L62
            int r0 = r6.length
        L58:
            if (r11 >= r0) goto L62
            r2 = r6[r11]
            r12.a(r2)
            int r11 = r11 + 1
            goto L58
        L62:
            com.google.zxing.Result r11 = new com.google.zxing.Result
            java.lang.String r3 = r1.c
            byte[] r4 = r1.a
            int r5 = r1.f1831b
            b.i.e.a r7 = b.i.e.a.AZTEC
            long r8 = java.lang.System.currentTimeMillis()
            r2 = r11
            r2.<init>(r3, r4, r5, r6, r7, r8)
            java.util.List<byte[]> r12 = r1.d
            if (r12 == 0) goto L7d
            b.i.e.j r0 = b.i.e.j.BYTE_SEGMENTS
            r11.b(r0, r12)
        L7d:
            java.lang.String r12 = r1.e
            if (r12 == 0) goto L86
            b.i.e.j r0 = b.i.e.j.ERROR_CORRECTION_LEVEL
            r11.b(r0, r12)
        L86:
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.e.m.b.a(b.i.e.c, java.util.Map):com.google.zxing.Result");
    }

    @Override // b.i.e.i
    public void reset() {
    }
}
