package b.i.e;

import b.i.a.f.e.o.f;
/* compiled from: ResultPoint.java */
/* loaded from: classes3.dex */
public class k {
    public final float a;

    /* renamed from: b  reason: collision with root package name */
    public final float f1822b;

    public k(float f, float f2) {
        this.a = f;
        this.f1822b = f2;
    }

    public static float a(k kVar, k kVar2) {
        return f.Q(kVar.a, kVar.f1822b, kVar2.a, kVar2.f1822b);
    }

    public static void b(k[] kVarArr) {
        k kVar;
        k kVar2;
        k kVar3;
        float a = a(kVarArr[0], kVarArr[1]);
        float a2 = a(kVarArr[1], kVarArr[2]);
        float a3 = a(kVarArr[0], kVarArr[2]);
        if (a2 >= a && a2 >= a3) {
            kVar3 = kVarArr[0];
            kVar2 = kVarArr[1];
            kVar = kVarArr[2];
        } else if (a3 < a2 || a3 < a) {
            kVar3 = kVarArr[2];
            kVar2 = kVarArr[0];
            kVar = kVarArr[1];
        } else {
            kVar3 = kVarArr[1];
            kVar2 = kVarArr[0];
            kVar = kVarArr[2];
        }
        float f = kVar3.a;
        float f2 = kVar3.f1822b;
        if (((kVar2.f1822b - f2) * (kVar.a - f)) - ((kVar2.a - f) * (kVar.f1822b - f2)) < 0.0f) {
            kVar2 = kVar;
            kVar = kVar2;
        }
        kVarArr[0] = kVar2;
        kVarArr[1] = kVar3;
        kVarArr[2] = kVar;
    }

    public final boolean equals(Object obj) {
        if (obj instanceof k) {
            k kVar = (k) obj;
            if (this.a == kVar.a && this.f1822b == kVar.f1822b) {
                return true;
            }
        }
        return false;
    }

    public final int hashCode() {
        return Float.floatToIntBits(this.f1822b) + (Float.floatToIntBits(this.a) * 31);
    }

    public final String toString() {
        return "(" + this.a + ',' + this.f1822b + ')';
    }
}
