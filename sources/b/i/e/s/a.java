package b.i.e.s;

import b.i.e.c;
import b.i.e.d;
import b.i.e.i;
import b.i.e.k;
import b.i.e.l;
import b.i.e.n.b;
import b.i.e.s.b.e;
import b.i.e.s.b.j;
import b.i.e.s.c.e;
import b.i.e.s.c.f;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import java.util.Collections;
import java.util.List;
import java.util.Map;
/* compiled from: QRCodeReader.java */
/* loaded from: classes3.dex */
public class a implements i {
    public static final k[] a = new k[0];

    /* renamed from: b  reason: collision with root package name */
    public final e f1881b = new e();

    @Override // b.i.e.i
    public final Result a(c cVar, Map<d, ?> map) throws NotFoundException, ChecksumException, FormatException {
        k[] kVarArr;
        b.i.e.n.e eVar;
        int i;
        b.i.e.s.c.a aVar;
        float f;
        float f2;
        float f3;
        float f4;
        int i2;
        int i3;
        if (map == null || !map.containsKey(d.PURE_BARCODE)) {
            b a2 = cVar.a();
            b.i.e.s.c.c cVar2 = new b.i.e.s.c.c(a2);
            l lVar = map == null ? null : (l) map.get(d.NEED_RESULT_POINT_CALLBACK);
            cVar2.f1890b = lVar;
            b.i.e.s.c.e eVar2 = new b.i.e.s.c.e(a2, lVar);
            boolean z2 = map != null && map.containsKey(d.TRY_HARDER);
            int i4 = a2.k;
            int i5 = a2.j;
            int i6 = (i4 * 3) / 388;
            if (i6 < 3 || z2) {
                i6 = 3;
            }
            int[] iArr = new int[5];
            int i7 = i6 - 1;
            boolean z3 = false;
            while (true) {
                int i8 = 4;
                if (i7 >= i4 || z3) {
                    break;
                }
                eVar2.b(iArr);
                int i9 = 0;
                int i10 = 0;
                while (i9 < i5) {
                    if (eVar2.a.f(i9, i7)) {
                        if ((i10 & 1) == 1) {
                            i10++;
                        }
                        iArr[i10] = iArr[i10] + 1;
                    } else if ((i10 & 1) != 0) {
                        iArr[i10] = iArr[i10] + 1;
                    } else if (i10 == i8) {
                        if (!b.i.e.s.c.e.c(iArr)) {
                            eVar2.g(iArr);
                        } else if (eVar2.e(iArr, i7, i9)) {
                            if (eVar2.c) {
                                z3 = eVar2.f();
                            } else {
                                if (eVar2.f1891b.size() > 1) {
                                    b.i.e.s.c.d dVar = null;
                                    for (b.i.e.s.c.d dVar2 : eVar2.f1891b) {
                                        if (dVar2.d >= 2) {
                                            if (dVar != null) {
                                                eVar2.c = true;
                                                i2 = 2;
                                                i3 = ((int) (Math.abs(dVar.a - dVar2.a) - Math.abs(dVar.f1822b - dVar2.f1822b))) / 2;
                                                break;
                                            }
                                            dVar = dVar2;
                                        }
                                    }
                                }
                                i2 = 2;
                                i3 = 0;
                                if (i3 > iArr[i2]) {
                                    i7 += (i3 - iArr[i2]) - i2;
                                    i9 = i5 - 1;
                                }
                            }
                            eVar2.b(iArr);
                            i6 = 2;
                            i10 = 0;
                        } else {
                            eVar2.g(iArr);
                        }
                        i10 = 3;
                    } else {
                        i10++;
                        iArr[i10] = iArr[i10] + 1;
                    }
                    i9++;
                    i8 = 4;
                }
                if (b.i.e.s.c.e.c(iArr) && eVar2.e(iArr, i7, i5)) {
                    i6 = iArr[0];
                    if (eVar2.c) {
                        z3 = eVar2.f();
                    }
                }
                i7 += i6;
            }
            int size = eVar2.f1891b.size();
            if (size >= 3) {
                float f5 = 0.0f;
                if (size > 3) {
                    float f6 = 0.0f;
                    float f7 = 0.0f;
                    for (b.i.e.s.c.d dVar3 : eVar2.f1891b) {
                        float f8 = dVar3.c;
                        f6 += f8;
                        f7 += f8 * f8;
                    }
                    float f9 = f6 / size;
                    Collections.sort(eVar2.f1891b, new e.c(f9, null));
                    float max = Math.max(0.2f * f9, (float) Math.sqrt((f7 / f4) - (f9 * f9)));
                    int i11 = 0;
                    while (i11 < eVar2.f1891b.size() && eVar2.f1891b.size() > 3) {
                        if (Math.abs(eVar2.f1891b.get(i11).c - f9) > max) {
                            eVar2.f1891b.remove(i11);
                            i11--;
                        }
                        i11++;
                    }
                }
                if (eVar2.f1891b.size() > 3) {
                    for (b.i.e.s.c.d dVar4 : eVar2.f1891b) {
                        f5 += dVar4.c;
                    }
                    Collections.sort(eVar2.f1891b, new e.b(f5 / eVar2.f1891b.size(), null));
                    List<b.i.e.s.c.d> list = eVar2.f1891b;
                    i = 3;
                    list.subList(3, list.size()).clear();
                } else {
                    i = 3;
                }
                b.i.e.s.c.d[] dVarArr = new b.i.e.s.c.d[i];
                dVarArr[0] = eVar2.f1891b.get(0);
                dVarArr[1] = eVar2.f1891b.get(1);
                dVarArr[2] = eVar2.f1891b.get(2);
                k.b(dVarArr);
                f fVar = new f(dVarArr);
                b.i.e.s.c.d dVar5 = fVar.f1892b;
                b.i.e.s.c.d dVar6 = fVar.c;
                b.i.e.s.c.d dVar7 = fVar.a;
                float a3 = (cVar2.a(dVar5, dVar7) + cVar2.a(dVar5, dVar6)) / 2.0f;
                if (a3 >= 1.0f) {
                    int Z0 = ((b.i.a.f.e.o.f.Z0(b.i.a.f.e.o.f.Q(dVar5.a, dVar5.f1822b, dVar7.a, dVar7.f1822b) / a3) + b.i.a.f.e.o.f.Z0(b.i.a.f.e.o.f.Q(dVar5.a, dVar5.f1822b, dVar6.a, dVar6.f1822b) / a3)) / 2) + 7;
                    int i12 = Z0 & 3;
                    if (i12 == 0) {
                        Z0++;
                    } else if (i12 == 2) {
                        Z0--;
                    } else if (i12 == 3) {
                        throw NotFoundException.l;
                    }
                    int[] iArr2 = j.a;
                    if (Z0 % 4 == 1) {
                        try {
                            j d = j.d((Z0 - 17) / 4);
                            int c = d.c() - 7;
                            if (d.d.length > 0) {
                                float f10 = dVar6.a;
                                float f11 = dVar5.a;
                                float f12 = (f10 - f11) + dVar7.a;
                                float f13 = dVar6.f1822b;
                                float f14 = dVar5.f1822b;
                                float f15 = 1.0f - (3.0f / c);
                                int a4 = (int) b.d.b.a.a.a(f12, f11, f15, f11);
                                int a5 = (int) b.d.b.a.a.a((f13 - f14) + dVar7.f1822b, f14, f15, f14);
                                for (int i13 = 4; i13 <= 16; i13 <<= 1) {
                                    try {
                                        aVar = cVar2.b(a3, a4, a5, i13);
                                        break;
                                    } catch (NotFoundException unused) {
                                    }
                                }
                            }
                            aVar = null;
                            float f16 = Z0 - 3.5f;
                            if (aVar != null) {
                                f3 = f16 - 3.0f;
                                f2 = aVar.a;
                                f = aVar.f1822b;
                            } else {
                                f2 = (dVar6.a - dVar5.a) + dVar7.a;
                                f = (dVar6.f1822b - dVar5.f1822b) + dVar7.f1822b;
                                f3 = f16;
                            }
                            b a6 = b.i.e.n.f.a.a(cVar2.a, Z0, Z0, b.i.e.n.i.a(3.5f, 3.5f, f16, 3.5f, f3, f3, 3.5f, f16, dVar5.a, dVar5.f1822b, dVar6.a, dVar6.f1822b, f2, f, dVar7.a, dVar7.f1822b));
                            k[] kVarArr2 = aVar == null ? new k[]{dVar7, dVar5, dVar6} : new k[]{dVar7, dVar5, dVar6, aVar};
                            eVar = this.f1881b.a(a6, map);
                            kVarArr = kVarArr2;
                        } catch (IllegalArgumentException unused2) {
                            throw FormatException.a();
                        }
                    } else {
                        throw FormatException.a();
                    }
                } else {
                    throw NotFoundException.l;
                }
            } else {
                throw NotFoundException.l;
            }
        } else {
            b a7 = cVar.a();
            int[] i14 = a7.i();
            int[] g = a7.g();
            if (i14 == null || g == null) {
                throw NotFoundException.l;
            }
            int i15 = a7.k;
            int i16 = a7.j;
            int i17 = i14[0];
            int i18 = i14[1];
            boolean z4 = true;
            int i19 = 0;
            while (i17 < i16 && i18 < i15) {
                if (z4 != a7.f(i17, i18)) {
                    i19++;
                    if (i19 == 5) {
                        break;
                    }
                    z4 = !z4;
                }
                i17++;
                i18++;
            }
            if (i17 == i16 || i18 == i15) {
                throw NotFoundException.l;
            }
            float f17 = (i17 - i14[0]) / 7.0f;
            int i20 = i14[1];
            int i21 = g[1];
            int i22 = i14[0];
            int i23 = g[0];
            if (i22 >= i23 || i20 >= i21) {
                throw NotFoundException.l;
            }
            int i24 = i21 - i20;
            if (i24 == i23 - i22 || (i23 = i22 + i24) < a7.j) {
                int round = Math.round(((i23 - i22) + 1) / f17);
                int round2 = Math.round((i24 + 1) / f17);
                if (round <= 0 || round2 <= 0) {
                    throw NotFoundException.l;
                } else if (round2 == round) {
                    int i25 = (int) (f17 / 2.0f);
                    int i26 = i20 + i25;
                    int i27 = i22 + i25;
                    int i28 = (((int) ((round - 1) * f17)) + i27) - i23;
                    if (i28 > 0) {
                        if (i28 <= i25) {
                            i27 -= i28;
                        } else {
                            throw NotFoundException.l;
                        }
                    }
                    int i29 = (((int) ((round2 - 1) * f17)) + i26) - i21;
                    if (i29 > 0) {
                        if (i29 <= i25) {
                            i26 -= i29;
                        } else {
                            throw NotFoundException.l;
                        }
                    }
                    b bVar = new b(round, round2);
                    for (int i30 = 0; i30 < round2; i30++) {
                        int i31 = ((int) (i30 * f17)) + i26;
                        for (int i32 = 0; i32 < round; i32++) {
                            if (a7.f(((int) (i32 * f17)) + i27, i31)) {
                                bVar.j(i32, i30);
                            }
                        }
                    }
                    eVar = this.f1881b.a(bVar, map);
                    kVarArr = a;
                } else {
                    throw NotFoundException.l;
                }
            } else {
                throw NotFoundException.l;
            }
        }
        Object obj = eVar.f;
        if ((obj instanceof b.i.e.s.b.i) && ((b.i.e.s.b.i) obj).a && kVarArr.length >= 3) {
            k kVar = kVarArr[0];
            kVarArr[0] = kVarArr[2];
            kVarArr[2] = kVar;
        }
        Result result = new Result(eVar.c, eVar.a, kVarArr, b.i.e.a.QR_CODE);
        List<byte[]> list2 = eVar.d;
        if (list2 != null) {
            result.b(b.i.e.j.BYTE_SEGMENTS, list2);
        }
        String str = eVar.e;
        if (str != null) {
            result.b(b.i.e.j.ERROR_CORRECTION_LEVEL, str);
        }
        if (eVar.g >= 0 && eVar.h >= 0) {
            result.b(b.i.e.j.STRUCTURED_APPEND_SEQUENCE, Integer.valueOf(eVar.h));
            result.b(b.i.e.j.STRUCTURED_APPEND_PARITY, Integer.valueOf(eVar.g));
        }
        return result;
    }

    @Override // b.i.e.i
    public void reset() {
    }
}
