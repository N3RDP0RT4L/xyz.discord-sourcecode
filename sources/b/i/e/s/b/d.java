package b.i.e.s.b;

import b.i.e.n.c;
import com.google.zxing.FormatException;
import java.io.UnsupportedEncodingException;
import org.objectweb.asm.Opcodes;
/* compiled from: DecodedBitStreamParser.java */
/* loaded from: classes3.dex */
public final class d {
    public static final char[] a = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ $%*+-./:".toCharArray();

    public static void a(c cVar, StringBuilder sb, int i, boolean z2) throws FormatException {
        while (i > 1) {
            if (cVar.a() >= 11) {
                int b2 = cVar.b(11);
                sb.append(f(b2 / 45));
                sb.append(f(b2 % 45));
                i -= 2;
            } else {
                throw FormatException.a();
            }
        }
        if (i == 1) {
            if (cVar.a() >= 6) {
                sb.append(f(cVar.b(6)));
            } else {
                throw FormatException.a();
            }
        }
        if (z2) {
            for (int length = sb.length(); length < sb.length(); length++) {
                if (sb.charAt(length) == '%') {
                    if (length < sb.length() - 1) {
                        int i2 = length + 1;
                        if (sb.charAt(i2) == '%') {
                            sb.deleteCharAt(i2);
                        }
                    }
                    sb.setCharAt(length, (char) 29);
                }
            }
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:119:0x0143, code lost:
        if (r1 == 2) goto L125;
     */
    /* JADX WARN: Code restructure failed: missing block: B:121:0x0147, code lost:
        if ((r17 * 10) >= r23) goto L125;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static void b(b.i.e.n.c r21, java.lang.StringBuilder r22, int r23, b.i.e.n.d r24, java.util.Collection<byte[]> r25, java.util.Map<b.i.e.d, ?> r26) throws com.google.zxing.FormatException {
        /*
            Method dump skipped, instructions count: 382
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.e.s.b.d.b(b.i.e.n.c, java.lang.StringBuilder, int, b.i.e.n.d, java.util.Collection, java.util.Map):void");
    }

    public static void c(c cVar, StringBuilder sb, int i) throws FormatException {
        if (i * 13 <= cVar.a()) {
            byte[] bArr = new byte[i * 2];
            int i2 = 0;
            while (i > 0) {
                int b2 = cVar.b(13);
                int i3 = (b2 % 96) | ((b2 / 96) << 8);
                int i4 = i3 + (i3 < 959 ? 41377 : 42657);
                bArr[i2] = (byte) (i4 >> 8);
                bArr[i2 + 1] = (byte) i4;
                i2 += 2;
                i--;
            }
            try {
                sb.append(new String(bArr, "GB2312"));
            } catch (UnsupportedEncodingException unused) {
                throw FormatException.a();
            }
        } else {
            throw FormatException.a();
        }
    }

    public static void d(c cVar, StringBuilder sb, int i) throws FormatException {
        if (i * 13 <= cVar.a()) {
            byte[] bArr = new byte[i * 2];
            int i2 = 0;
            while (i > 0) {
                int b2 = cVar.b(13);
                int i3 = (b2 % Opcodes.CHECKCAST) | ((b2 / Opcodes.CHECKCAST) << 8);
                int i4 = i3 + (i3 < 7936 ? 33088 : 49472);
                bArr[i2] = (byte) (i4 >> 8);
                bArr[i2 + 1] = (byte) i4;
                i2 += 2;
                i--;
            }
            try {
                sb.append(new String(bArr, "SJIS"));
            } catch (UnsupportedEncodingException unused) {
                throw FormatException.a();
            }
        } else {
            throw FormatException.a();
        }
    }

    public static void e(c cVar, StringBuilder sb, int i) throws FormatException {
        while (i >= 3) {
            if (cVar.a() >= 10) {
                int b2 = cVar.b(10);
                if (b2 < 1000) {
                    sb.append(f(b2 / 100));
                    sb.append(f((b2 / 10) % 10));
                    sb.append(f(b2 % 10));
                    i -= 3;
                } else {
                    throw FormatException.a();
                }
            } else {
                throw FormatException.a();
            }
        }
        if (i == 2) {
            if (cVar.a() >= 7) {
                int b3 = cVar.b(7);
                if (b3 < 100) {
                    sb.append(f(b3 / 10));
                    sb.append(f(b3 % 10));
                    return;
                }
                throw FormatException.a();
            }
            throw FormatException.a();
        } else if (i != 1) {
        } else {
            if (cVar.a() >= 4) {
                int b4 = cVar.b(4);
                if (b4 < 10) {
                    sb.append(f(b4));
                    return;
                }
                throw FormatException.a();
            }
            throw FormatException.a();
        }
    }

    public static char f(int i) throws FormatException {
        char[] cArr = a;
        if (i < cArr.length) {
            return cArr[i];
        }
        throw FormatException.a();
    }
}
