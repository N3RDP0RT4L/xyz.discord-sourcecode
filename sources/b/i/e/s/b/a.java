package b.i.e.s.b;

import b.i.e.n.b;
import com.google.zxing.FormatException;
/* compiled from: BitMatrixParser.java */
/* loaded from: classes3.dex */
public final class a {
    public final b a;

    /* renamed from: b  reason: collision with root package name */
    public j f1882b;
    public g c;
    public boolean d;

    public a(b bVar) throws FormatException {
        int i = bVar.k;
        if (i < 21 || (i & 3) != 1) {
            throw FormatException.a();
        }
        this.a = bVar;
    }

    public final int a(int i, int i2, int i3) {
        return this.d ? this.a.f(i2, i) : this.a.f(i, i2) ? (i3 << 1) | 1 : i3 << 1;
    }

    public void b() {
        int i = 0;
        while (i < this.a.j) {
            int i2 = i + 1;
            int i3 = i2;
            while (true) {
                b bVar = this.a;
                if (i3 < bVar.k) {
                    if (bVar.f(i, i3) != this.a.f(i3, i)) {
                        this.a.b(i3, i);
                        this.a.b(i, i3);
                    }
                    i3++;
                }
            }
            i = i2;
        }
    }

    public g c() throws FormatException {
        g gVar = this.c;
        if (gVar != null) {
            return gVar;
        }
        int i = 0;
        int i2 = 0;
        for (int i3 = 0; i3 < 6; i3++) {
            i2 = a(i3, 8, i2);
        }
        int a = a(8, 7, a(8, 8, a(7, 8, i2)));
        for (int i4 = 5; i4 >= 0; i4--) {
            a = a(8, i4, a);
        }
        int i5 = this.a.k;
        int i6 = i5 - 7;
        for (int i7 = i5 - 1; i7 >= i6; i7--) {
            i = a(8, i7, i);
        }
        for (int i8 = i5 - 8; i8 < i5; i8++) {
            i = a(i8, 8, i);
        }
        g a2 = g.a(a, i);
        if (a2 == null) {
            a2 = g.a(a ^ 21522, i ^ 21522);
        }
        this.c = a2;
        if (a2 != null) {
            return a2;
        }
        throw FormatException.a();
    }

    public j d() throws FormatException {
        j jVar = this.f1882b;
        if (jVar != null) {
            return jVar;
        }
        int i = this.a.k;
        int i2 = (i - 17) / 4;
        if (i2 <= 6) {
            return j.d(i2);
        }
        int i3 = i - 11;
        int i4 = 0;
        int i5 = 0;
        for (int i6 = 5; i6 >= 0; i6--) {
            for (int i7 = i - 9; i7 >= i3; i7--) {
                i5 = a(i7, i6, i5);
            }
        }
        j b2 = j.b(i5);
        if (b2 == null || b2.c() != i) {
            for (int i8 = 5; i8 >= 0; i8--) {
                for (int i9 = i - 9; i9 >= i3; i9--) {
                    i4 = a(i8, i9, i4);
                }
            }
            j b3 = j.b(i4);
            if (b3 == null || b3.c() != i) {
                throw FormatException.a();
            }
            this.f1882b = b3;
            return b3;
        }
        this.f1882b = b2;
        return b2;
    }

    public void e() {
        if (this.c != null) {
            c cVar = c.values()[this.c.c];
            b bVar = this.a;
            cVar.g(bVar, bVar.k);
        }
    }
}
