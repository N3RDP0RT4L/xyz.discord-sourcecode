package b.i.e.s.b;

import b.i.e.d;
import b.i.e.n.b;
import b.i.e.n.l.a;
import b.i.e.n.l.c;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import java.util.Map;
/* compiled from: Decoder.java */
/* loaded from: classes3.dex */
public final class e {
    public final c a = new c(a.e);

    public b.i.e.n.e a(b bVar, Map<d, ?> map) throws FormatException, ChecksumException {
        ChecksumException e;
        FormatException e2;
        a aVar = new a(bVar);
        try {
            return b(aVar, map);
        } catch (ChecksumException e3) {
            e = e3;
            e2 = null;
            try {
                aVar.e();
                aVar.f1882b = null;
                aVar.c = null;
                aVar.d = true;
                aVar.d();
                aVar.c();
                aVar.b();
                b.i.e.n.e b2 = b(aVar, map);
                b2.f = new i(true);
                return b2;
            } catch (ChecksumException | FormatException unused) {
                if (e2 != null) {
                    throw e2;
                }
                throw e;
            }
        } catch (FormatException e4) {
            e2 = e4;
            e = null;
            aVar.e();
            aVar.f1882b = null;
            aVar.c = null;
            aVar.d = true;
            aVar.d();
            aVar.c();
            aVar.b();
            b.i.e.n.e b22 = b(aVar, map);
            b22.f = new i(true);
            return b22;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:137:0x0257  */
    /* JADX WARN: Removed duplicated region for block: B:195:0x0343 A[LOOP:19: B:204:0x0204->B:195:0x0343, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:236:0x0329 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final b.i.e.n.e b(b.i.e.s.b.a r28, java.util.Map<b.i.e.d, ?> r29) throws com.google.zxing.FormatException, com.google.zxing.ChecksumException {
        /*
            Method dump skipped, instructions count: 860
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.e.s.b.e.b(b.i.e.s.b.a, java.util.Map):b.i.e.n.e");
    }
}
