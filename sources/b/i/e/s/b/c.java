package b.i.e.s.b;
/* JADX WARN: Failed to restore enum class, 'enum' modifier removed */
/* compiled from: DataMask.java */
/* loaded from: classes3.dex */
public abstract class c extends Enum<c> {
    public static final c j;
    public static final c k;
    public static final c l;
    public static final c m;
    public static final c n;
    public static final c o;
    public static final c p;
    public static final c q;
    public static final /* synthetic */ c[] r;

    /* JADX WARN: Failed to restore enum class, 'enum' modifier removed */
    /* compiled from: DataMask.java */
    /* loaded from: classes3.dex */
    public static class a extends c {
        public a(String str, int i) {
            super(str, i, null);
        }

        @Override // b.i.e.s.b.c
        public boolean f(int i, int i2) {
            return ((i + i2) & 1) == 0;
        }
    }

    static {
        a aVar = new a("DATA_MASK_000", 0);
        j = aVar;
        c bVar = new c("DATA_MASK_001", 1) { // from class: b.i.e.s.b.c.b
            @Override // b.i.e.s.b.c
            public boolean f(int i, int i2) {
                return (i & 1) == 0;
            }
        };
        k = bVar;
        c cVar = new c("DATA_MASK_010", 2) { // from class: b.i.e.s.b.c.c
            @Override // b.i.e.s.b.c
            public boolean f(int i, int i2) {
                return i2 % 3 == 0;
            }
        };
        l = cVar;
        c dVar = new c("DATA_MASK_011", 3) { // from class: b.i.e.s.b.c.d
            @Override // b.i.e.s.b.c
            public boolean f(int i, int i2) {
                return (i + i2) % 3 == 0;
            }
        };
        m = dVar;
        c eVar = new c("DATA_MASK_100", 4) { // from class: b.i.e.s.b.c.e
            @Override // b.i.e.s.b.c
            public boolean f(int i, int i2) {
                return (((i2 / 3) + (i / 2)) & 1) == 0;
            }
        };
        n = eVar;
        c fVar = new c("DATA_MASK_101", 5) { // from class: b.i.e.s.b.c.f
            @Override // b.i.e.s.b.c
            public boolean f(int i, int i2) {
                return (i * i2) % 6 == 0;
            }
        };
        o = fVar;
        c gVar = new c("DATA_MASK_110", 6) { // from class: b.i.e.s.b.c.g
            @Override // b.i.e.s.b.c
            public boolean f(int i, int i2) {
                return (i * i2) % 6 < 3;
            }
        };
        p = gVar;
        c hVar = new c("DATA_MASK_111", 7) { // from class: b.i.e.s.b.c.h
            @Override // b.i.e.s.b.c
            public boolean f(int i, int i2) {
                return ((((i * i2) % 3) + (i + i2)) & 1) == 0;
            }
        };
        q = hVar;
        r = new c[]{aVar, bVar, cVar, dVar, eVar, fVar, gVar, hVar};
    }

    public c(String str, int i, a aVar) {
    }

    public static c valueOf(String str) {
        return (c) Enum.valueOf(c.class, str);
    }

    public static c[] values() {
        return (c[]) r.clone();
    }

    public abstract boolean f(int i, int i2);

    public final void g(b.i.e.n.b bVar, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            for (int i3 = 0; i3 < i; i3++) {
                if (f(i2, i3)) {
                    bVar.b(i3, i2);
                }
            }
        }
    }
}
