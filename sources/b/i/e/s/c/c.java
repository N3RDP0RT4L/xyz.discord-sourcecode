package b.i.e.s.c;

import b.i.a.f.e.o.f;
import b.i.e.k;
import b.i.e.l;
import b.i.e.n.b;
import com.google.zxing.NotFoundException;
/* compiled from: Detector.java */
/* loaded from: classes3.dex */
public class c {
    public final b a;

    /* renamed from: b  reason: collision with root package name */
    public l f1890b;

    public c(b bVar) {
        this.a = bVar;
    }

    public final float a(k kVar, k kVar2) {
        float d = d((int) kVar.a, (int) kVar.f1822b, (int) kVar2.a, (int) kVar2.f1822b);
        float d2 = d((int) kVar2.a, (int) kVar2.f1822b, (int) kVar.a, (int) kVar.f1822b);
        return Float.isNaN(d) ? d2 / 7.0f : Float.isNaN(d2) ? d / 7.0f : (d + d2) / 14.0f;
    }

    public final a b(float f, int i, int i2, float f2) throws NotFoundException {
        a c;
        a c2;
        int i3 = (int) (f2 * f);
        int max = Math.max(0, i - i3);
        int min = Math.min(this.a.j - 1, i + i3) - max;
        float f3 = 3.0f * f;
        if (min >= f3) {
            int max2 = Math.max(0, i2 - i3);
            int min2 = Math.min(this.a.k - 1, i2 + i3) - max2;
            if (min2 >= f3) {
                b bVar = new b(this.a, max, max2, min, min2, f, this.f1890b);
                int i4 = bVar.c;
                int i5 = bVar.f;
                int i6 = bVar.e + i4;
                int i7 = (i5 / 2) + bVar.d;
                int[] iArr = new int[3];
                for (int i8 = 0; i8 < i5; i8++) {
                    int i9 = ((i8 & 1) == 0 ? (i8 + 1) / 2 : -((i8 + 1) / 2)) + i7;
                    iArr[0] = 0;
                    iArr[1] = 0;
                    iArr[2] = 0;
                    int i10 = i4;
                    while (i10 < i6 && !bVar.a.f(i10, i9)) {
                        i10++;
                    }
                    int i11 = 0;
                    while (i10 < i6) {
                        if (!bVar.a.f(i10, i9)) {
                            if (i11 == 1) {
                                i11++;
                            }
                            iArr[i11] = iArr[i11] + 1;
                        } else if (i11 == 1) {
                            iArr[1] = iArr[1] + 1;
                        } else if (i11 != 2) {
                            i11++;
                            iArr[i11] = iArr[i11] + 1;
                        } else if (bVar.b(iArr) && (c2 = bVar.c(iArr, i9, i10)) != null) {
                            return c2;
                        } else {
                            iArr[0] = iArr[2];
                            iArr[1] = 1;
                            iArr[2] = 0;
                            i11 = 1;
                        }
                        i10++;
                    }
                    if (bVar.b(iArr) && (c = bVar.c(iArr, i9, i6)) != null) {
                        return c;
                    }
                }
                if (!bVar.f1889b.isEmpty()) {
                    return bVar.f1889b.get(0);
                }
                throw NotFoundException.l;
            }
            throw NotFoundException.l;
        }
        throw NotFoundException.l;
    }

    public final float c(int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        boolean z2;
        c cVar;
        boolean z3;
        int i10 = 1;
        boolean z4 = Math.abs(i4 - i2) > Math.abs(i3 - i);
        if (z4) {
            i7 = i;
            i8 = i2;
            i5 = i3;
            i6 = i4;
        } else {
            i8 = i;
            i7 = i2;
            i6 = i3;
            i5 = i4;
        }
        int abs = Math.abs(i6 - i8);
        int abs2 = Math.abs(i5 - i7);
        int i11 = (-abs) / 2;
        int i12 = -1;
        int i13 = i8 < i6 ? 1 : -1;
        if (i7 < i5) {
            i12 = 1;
        }
        int i14 = i6 + i13;
        int i15 = i8;
        int i16 = i7;
        int i17 = 0;
        while (true) {
            if (i15 == i14) {
                i9 = i14;
                break;
            }
            int i18 = z4 ? i16 : i15;
            int i19 = z4 ? i15 : i16;
            if (i17 == i10) {
                cVar = this;
                z2 = z4;
                i9 = i14;
                z3 = true;
            } else {
                cVar = this;
                z2 = z4;
                i9 = i14;
                z3 = false;
            }
            if (z3 == cVar.a.f(i18, i19)) {
                if (i17 == 2) {
                    return f.R(i15, i16, i8, i7);
                }
                i17++;
            }
            i11 += abs2;
            if (i11 > 0) {
                if (i16 == i5) {
                    break;
                }
                i16 += i12;
                i11 -= abs;
            }
            i15 += i13;
            i14 = i9;
            z4 = z2;
            i10 = 1;
        }
        if (i17 == 2) {
            return f.R(i9, i5, i8, i7);
        }
        return Float.NaN;
    }

    public final float d(int i, int i2, int i3, int i4) {
        float f;
        float f2;
        float c = c(i, i2, i3, i4);
        int i5 = i - (i3 - i);
        int i6 = 0;
        if (i5 < 0) {
            f = i / (i - i5);
            i5 = 0;
        } else {
            int i7 = this.a.j;
            if (i5 >= i7) {
                f = ((i7 - 1) - i) / (i5 - i);
                i5 = i7 - 1;
            } else {
                f = 1.0f;
            }
        }
        float f3 = i2;
        int i8 = (int) (f3 - ((i4 - i2) * f));
        if (i8 < 0) {
            f2 = f3 / (i2 - i8);
        } else {
            int i9 = this.a.k;
            if (i8 >= i9) {
                f2 = ((i9 - 1) - i2) / (i8 - i2);
                i6 = i9 - 1;
            } else {
                i6 = i8;
                f2 = 1.0f;
            }
        }
        return (c(i, i2, (int) (((i5 - i) * f2) + i), i6) + c) - 1.0f;
    }
}
