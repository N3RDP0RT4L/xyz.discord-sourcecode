package b.i.e.s.c;

import b.i.e.l;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
/* compiled from: FinderPatternFinder.java */
/* loaded from: classes3.dex */
public class e {
    public final b.i.e.n.b a;
    public boolean c;
    public final l e;

    /* renamed from: b  reason: collision with root package name */
    public final List<d> f1891b = new ArrayList();
    public final int[] d = new int[5];

    /* compiled from: FinderPatternFinder.java */
    /* loaded from: classes3.dex */
    public static final class b implements Serializable, Comparator<d> {
        private final float average;

        public b(float f, a aVar) {
            this.average = f;
        }

        @Override // java.util.Comparator
        public int compare(d dVar, d dVar2) {
            d dVar3 = dVar;
            d dVar4 = dVar2;
            int compare = Integer.compare(dVar4.d, dVar3.d);
            return compare == 0 ? Float.compare(Math.abs(dVar3.c - this.average), Math.abs(dVar4.c - this.average)) : compare;
        }
    }

    /* compiled from: FinderPatternFinder.java */
    /* loaded from: classes3.dex */
    public static final class c implements Serializable, Comparator<d> {
        private final float average;

        public c(float f, a aVar) {
            this.average = f;
        }

        @Override // java.util.Comparator
        public int compare(d dVar, d dVar2) {
            return Float.compare(Math.abs(dVar2.c - this.average), Math.abs(dVar.c - this.average));
        }
    }

    public e(b.i.e.n.b bVar, l lVar) {
        this.a = bVar;
        this.e = lVar;
    }

    public static float a(int[] iArr, int i) {
        return ((i - iArr[4]) - iArr[3]) - (iArr[2] / 2.0f);
    }

    public static boolean c(int[] iArr) {
        int i = 0;
        for (int i2 = 0; i2 < 5; i2++) {
            int i3 = iArr[i2];
            if (i3 == 0) {
                return false;
            }
            i += i3;
        }
        if (i < 7) {
            return false;
        }
        float f = i / 7.0f;
        float f2 = f / 2.0f;
        return Math.abs(f - ((float) iArr[0])) < f2 && Math.abs(f - ((float) iArr[1])) < f2 && Math.abs((f * 3.0f) - ((float) iArr[2])) < 3.0f * f2 && Math.abs(f - ((float) iArr[3])) < f2 && Math.abs(f - ((float) iArr[4])) < f2;
    }

    public final void b(int[] iArr) {
        for (int i = 0; i < iArr.length; i++) {
            iArr[i] = 0;
        }
    }

    public final int[] d() {
        b(this.d);
        return this.d;
    }

    /* JADX WARN: Code restructure failed: missing block: B:100:0x017e, code lost:
        r13[4] = r13[4] + 1;
        r8 = r8 + 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:102:0x0188, code lost:
        if (r13[4] < r1) goto L103;
     */
    /* JADX WARN: Code restructure failed: missing block: B:104:0x01a1, code lost:
        if ((java.lang.Math.abs(((((r13[0] + r13[1]) + r13[2]) + r13[3]) + r13[4]) - r3) * 5) < r3) goto L105;
     */
    /* JADX WARN: Code restructure failed: missing block: B:106:0x01a8, code lost:
        if (c(r13) == false) goto L108;
     */
    /* JADX WARN: Code restructure failed: missing block: B:107:0x01aa, code lost:
        r14 = a(r13, r8);
     */
    /* JADX WARN: Code restructure failed: missing block: B:17:0x0057, code lost:
        if (r12[1] <= r9) goto L18;
     */
    /* JADX WARN: Code restructure failed: missing block: B:18:0x005b, code lost:
        if (r13 < 0) goto L209;
     */
    /* JADX WARN: Code restructure failed: missing block: B:20:0x0061, code lost:
        if (r10.f(r8, r13) == false) goto L207;
     */
    /* JADX WARN: Code restructure failed: missing block: B:22:0x0065, code lost:
        if (r12[0] > r9) goto L208;
     */
    /* JADX WARN: Code restructure failed: missing block: B:23:0x0067, code lost:
        r12[0] = r12[0] + 1;
        r13 = r13 - 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:25:0x0071, code lost:
        if (r12[0] <= r9) goto L26;
     */
    /* JADX WARN: Code restructure failed: missing block: B:26:0x0075, code lost:
        r13 = r19 + 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:27:0x0077, code lost:
        if (r13 >= r11) goto L210;
     */
    /* JADX WARN: Code restructure failed: missing block: B:29:0x007d, code lost:
        if (r10.f(r8, r13) == false) goto L211;
     */
    /* JADX WARN: Code restructure failed: missing block: B:30:0x007f, code lost:
        r12[2] = r12[2] + 1;
        r13 = r13 + 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:31:0x0087, code lost:
        if (r13 != r11) goto L32;
     */
    /* JADX WARN: Code restructure failed: missing block: B:32:0x008a, code lost:
        if (r13 >= r11) goto L214;
     */
    /* JADX WARN: Code restructure failed: missing block: B:34:0x0090, code lost:
        if (r10.f(r8, r13) != false) goto L212;
     */
    /* JADX WARN: Code restructure failed: missing block: B:36:0x0094, code lost:
        if (r12[3] >= r9) goto L213;
     */
    /* JADX WARN: Code restructure failed: missing block: B:37:0x0096, code lost:
        r12[3] = r12[3] + 1;
        r13 = r13 + 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:38:0x009e, code lost:
        if (r13 == r11) goto L54;
     */
    /* JADX WARN: Code restructure failed: missing block: B:40:0x00a2, code lost:
        if (r12[3] < r9) goto L41;
     */
    /* JADX WARN: Code restructure failed: missing block: B:41:0x00a5, code lost:
        if (r13 >= r11) goto L215;
     */
    /* JADX WARN: Code restructure failed: missing block: B:43:0x00ab, code lost:
        if (r10.f(r8, r13) == false) goto L216;
     */
    /* JADX WARN: Code restructure failed: missing block: B:45:0x00af, code lost:
        if (r12[4] >= r9) goto L217;
     */
    /* JADX WARN: Code restructure failed: missing block: B:46:0x00b1, code lost:
        r12[4] = r12[4] + 1;
        r13 = r13 + 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:48:0x00bb, code lost:
        if (r12[4] < r9) goto L49;
     */
    /* JADX WARN: Code restructure failed: missing block: B:50:0x00d5, code lost:
        if ((java.lang.Math.abs(((((r12[0] + r12[1]) + r12[2]) + r12[3]) + r12[4]) - r3) * 5) < (r3 * 2)) goto L51;
     */
    /* JADX WARN: Code restructure failed: missing block: B:52:0x00dc, code lost:
        if (c(r12) == false) goto L54;
     */
    /* JADX WARN: Code restructure failed: missing block: B:53:0x00de, code lost:
        r9 = a(r12, r13);
     */
    /* JADX WARN: Code restructure failed: missing block: B:71:0x0125, code lost:
        if (r13[1] <= r1) goto L72;
     */
    /* JADX WARN: Code restructure failed: missing block: B:72:0x0129, code lost:
        if (r14 < 0) goto L224;
     */
    /* JADX WARN: Code restructure failed: missing block: B:74:0x012f, code lost:
        if (r11.f(r14, r10) == false) goto L225;
     */
    /* JADX WARN: Code restructure failed: missing block: B:76:0x0133, code lost:
        if (r13[0] > r1) goto L223;
     */
    /* JADX WARN: Code restructure failed: missing block: B:77:0x0135, code lost:
        r13[0] = r13[0] + 1;
        r14 = r14 - 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:79:0x013f, code lost:
        if (r13[0] <= r1) goto L80;
     */
    /* JADX WARN: Code restructure failed: missing block: B:80:0x0143, code lost:
        r8 = r8 + 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:81:0x0144, code lost:
        if (r8 >= r12) goto L226;
     */
    /* JADX WARN: Code restructure failed: missing block: B:83:0x014a, code lost:
        if (r11.f(r8, r10) == false) goto L227;
     */
    /* JADX WARN: Code restructure failed: missing block: B:84:0x014c, code lost:
        r13[2] = r13[2] + 1;
        r8 = r8 + 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:85:0x0154, code lost:
        if (r8 != r12) goto L86;
     */
    /* JADX WARN: Code restructure failed: missing block: B:86:0x0157, code lost:
        if (r8 >= r12) goto L229;
     */
    /* JADX WARN: Code restructure failed: missing block: B:88:0x015d, code lost:
        if (r11.f(r8, r10) != false) goto L230;
     */
    /* JADX WARN: Code restructure failed: missing block: B:90:0x0161, code lost:
        if (r13[3] >= r1) goto L228;
     */
    /* JADX WARN: Code restructure failed: missing block: B:91:0x0163, code lost:
        r13[3] = r13[3] + 1;
        r8 = r8 + 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:92:0x016b, code lost:
        if (r8 == r12) goto L108;
     */
    /* JADX WARN: Code restructure failed: missing block: B:94:0x016f, code lost:
        if (r13[3] < r1) goto L95;
     */
    /* JADX WARN: Code restructure failed: missing block: B:95:0x0172, code lost:
        if (r8 >= r12) goto L231;
     */
    /* JADX WARN: Code restructure failed: missing block: B:97:0x0178, code lost:
        if (r11.f(r8, r10) == false) goto L232;
     */
    /* JADX WARN: Code restructure failed: missing block: B:99:0x017c, code lost:
        if (r13[4] >= r1) goto L233;
     */
    /* JADX WARN: Removed duplicated region for block: B:194:0x0346 A[LOOP:19: B:180:0x02e1->B:194:0x0346, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:255:0x0322 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final boolean e(int[] r18, int r19, int r20) {
        /*
            Method dump skipped, instructions count: 864
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.e.s.c.e.e(int[], int, int):boolean");
    }

    public final boolean f() {
        int size = this.f1891b.size();
        float f = 0.0f;
        int i = 0;
        float f2 = 0.0f;
        for (d dVar : this.f1891b) {
            if (dVar.d >= 2) {
                i++;
                f2 += dVar.c;
            }
        }
        if (i < 3) {
            return false;
        }
        float f3 = f2 / size;
        for (d dVar2 : this.f1891b) {
            f += Math.abs(dVar2.c - f3);
        }
        return f <= f2 * 0.05f;
    }

    public final void g(int[] iArr) {
        iArr[0] = iArr[2];
        iArr[1] = iArr[3];
        iArr[2] = iArr[4];
        iArr[3] = 1;
        iArr[4] = 0;
    }
}
