package b.i.e.s.c;

import b.i.e.k;
/* compiled from: FinderPattern.java */
/* loaded from: classes3.dex */
public final class d extends k {
    public final float c;
    public final int d;

    public d(float f, float f2, float f3) {
        super(f, f2);
        this.c = f3;
        this.d = 1;
    }

    public d(float f, float f2, float f3, int i) {
        super(f, f2);
        this.c = f3;
        this.d = i;
    }
}
