package b.i.e.s.c;

import b.i.e.l;
import java.util.ArrayList;
import java.util.List;
/* compiled from: AlignmentPatternFinder.java */
/* loaded from: classes3.dex */
public final class b {
    public final b.i.e.n.b a;
    public final int c;
    public final int d;
    public final int e;
    public final int f;
    public final float g;
    public final l i;

    /* renamed from: b  reason: collision with root package name */
    public final List<a> f1889b = new ArrayList(5);
    public final int[] h = new int[3];

    public b(b.i.e.n.b bVar, int i, int i2, int i3, int i4, float f, l lVar) {
        this.a = bVar;
        this.c = i;
        this.d = i2;
        this.e = i3;
        this.f = i4;
        this.g = f;
        this.i = lVar;
    }

    public static float a(int[] iArr, int i) {
        return (i - iArr[2]) - (iArr[1] / 2.0f);
    }

    public final boolean b(int[] iArr) {
        float f = this.g;
        float f2 = f / 2.0f;
        for (int i = 0; i < 3; i++) {
            if (Math.abs(f - iArr[i]) >= f2) {
                return false;
            }
        }
        return true;
    }

    /* JADX WARN: Code restructure failed: missing block: B:30:0x0071, code lost:
        if (r8[1] <= r5) goto L31;
     */
    /* JADX WARN: Code restructure failed: missing block: B:31:0x0074, code lost:
        if (r14 >= r7) goto L75;
     */
    /* JADX WARN: Code restructure failed: missing block: B:33:0x007a, code lost:
        if (r6.f(r4, r14) != false) goto L76;
     */
    /* JADX WARN: Code restructure failed: missing block: B:35:0x007e, code lost:
        if (r8[2] > r5) goto L77;
     */
    /* JADX WARN: Code restructure failed: missing block: B:36:0x0080, code lost:
        r8[2] = r8[2] + 1;
        r14 = r14 + 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:38:0x008a, code lost:
        if (r8[2] <= r5) goto L39;
     */
    /* JADX WARN: Code restructure failed: missing block: B:40:0x009e, code lost:
        if ((java.lang.Math.abs(((r8[0] + r8[1]) + r8[2]) - r1) * 5) < (r1 * 2)) goto L41;
     */
    /* JADX WARN: Code restructure failed: missing block: B:42:0x00a5, code lost:
        if (b(r8) == false) goto L44;
     */
    /* JADX WARN: Code restructure failed: missing block: B:43:0x00a7, code lost:
        r10 = a(r8, r14);
     */
    /* JADX WARN: Removed duplicated region for block: B:79:0x0100 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final b.i.e.s.c.a c(int[] r13, int r14, int r15) {
        /*
            Method dump skipped, instructions count: 295
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.e.s.c.b.c(int[], int, int):b.i.e.s.c.a");
    }
}
