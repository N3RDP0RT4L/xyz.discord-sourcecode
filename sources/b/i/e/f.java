package b.i.e;

import andhook.lib.xposed.ClassUtils;
import com.discord.widgets.chat.input.MentionUtilsKt;
/* compiled from: LuminanceSource.java */
/* loaded from: classes3.dex */
public abstract class f {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final int f1819b;

    public f(int i, int i2) {
        this.a = i;
        this.f1819b = i2;
    }

    public abstract byte[] a();

    public abstract byte[] b(int i, byte[] bArr);

    public boolean c() {
        return false;
    }

    public f d() {
        throw new UnsupportedOperationException("This luminance source does not support rotation by 90 degrees.");
    }

    public final String toString() {
        int i = this.a;
        byte[] bArr = new byte[i];
        StringBuilder sb = new StringBuilder((i + 1) * this.f1819b);
        for (int i2 = 0; i2 < this.f1819b; i2++) {
            bArr = b(i2, bArr);
            for (int i3 = 0; i3 < this.a; i3++) {
                int i4 = bArr[i3] & 255;
                sb.append(i4 < 64 ? MentionUtilsKt.CHANNELS_CHAR : i4 < 128 ? '+' : i4 < 192 ? ClassUtils.PACKAGE_SEPARATOR_CHAR : ' ');
            }
            sb.append('\n');
        }
        return sb.toString();
    }
}
