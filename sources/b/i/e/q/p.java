package b.i.e.q;

import b.i.e.d;
import b.i.e.j;
import b.i.e.k;
import b.i.e.l;
import b.i.e.n.a;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.ReaderException;
import com.google.zxing.Result;
import java.util.Arrays;
import java.util.Map;
/* compiled from: UPCEANReader.java */
/* loaded from: classes3.dex */
public abstract class p extends k {
    public static final int[] a = {1, 1, 1};

    /* renamed from: b  reason: collision with root package name */
    public static final int[] f1857b = {1, 1, 1, 1, 1};
    public static final int[][] c;
    public static final int[][] d;
    public final StringBuilder e = new StringBuilder(20);
    public final o f = new o();
    public final g g = new g();

    static {
        int[][] iArr = {new int[]{3, 2, 1, 1}, new int[]{2, 2, 2, 1}, new int[]{2, 1, 2, 2}, new int[]{1, 4, 1, 1}, new int[]{1, 1, 3, 2}, new int[]{1, 2, 3, 1}, new int[]{1, 1, 1, 4}, new int[]{1, 3, 1, 2}, new int[]{1, 2, 1, 3}, new int[]{3, 1, 1, 2}};
        c = iArr;
        int[][] iArr2 = new int[20];
        d = iArr2;
        System.arraycopy(iArr, 0, iArr2, 0, 10);
        for (int i = 10; i < 20; i++) {
            int[] iArr3 = c[i - 10];
            int[] iArr4 = new int[iArr3.length];
            for (int i2 = 0; i2 < iArr3.length; i2++) {
                iArr4[i2] = iArr3[(iArr3.length - i2) - 1];
            }
            d[i] = iArr4;
        }
    }

    public static int h(a aVar, int[] iArr, int i, int[][] iArr2) throws NotFoundException {
        k.e(aVar, i, iArr);
        int length = iArr2.length;
        float f = 0.48f;
        int i2 = -1;
        for (int i3 = 0; i3 < length; i3++) {
            float d2 = k.d(iArr, iArr2[i3], 0.7f);
            if (d2 < f) {
                i2 = i3;
                f = d2;
            }
        }
        if (i2 >= 0) {
            return i2;
        }
        throw NotFoundException.l;
    }

    public static int[] l(a aVar, int i, boolean z2, int[] iArr, int[] iArr2) throws NotFoundException {
        int i2 = aVar.k;
        int g = z2 ? aVar.g(i) : aVar.f(i);
        int length = iArr.length;
        boolean z3 = z2;
        int i3 = 0;
        int i4 = g;
        while (g < i2) {
            if (aVar.b(g) != z3) {
                iArr2[i3] = iArr2[i3] + 1;
            } else {
                if (i3 != length - 1) {
                    i3++;
                } else if (k.d(iArr2, iArr, 0.7f) < 0.48f) {
                    return new int[]{i4, g};
                } else {
                    i4 += iArr2[0] + iArr2[1];
                    int i5 = i3 - 1;
                    System.arraycopy(iArr2, 2, iArr2, 0, i5);
                    iArr2[i5] = 0;
                    iArr2[i3] = 0;
                    i3 = i5;
                }
                iArr2[i3] = 1;
                z3 = !z3;
            }
            g++;
        }
        throw NotFoundException.l;
    }

    public static int[] m(a aVar) throws NotFoundException {
        int[] iArr = new int[a.length];
        int[] iArr2 = null;
        boolean z2 = false;
        int i = 0;
        while (!z2) {
            int[] iArr3 = a;
            Arrays.fill(iArr, 0, iArr3.length, 0);
            iArr2 = l(aVar, i, false, iArr3, iArr);
            int i2 = iArr2[0];
            int i3 = iArr2[1];
            int i4 = i2 - (i3 - i2);
            if (i4 >= 0) {
                z2 = aVar.h(i4, i2, false);
            }
            i = i3;
        }
        return iArr2;
    }

    @Override // b.i.e.q.k
    public Result b(int i, a aVar, Map<d, ?> map) throws NotFoundException, ChecksumException, FormatException {
        return k(i, aVar, m(aVar), map);
    }

    /* JADX WARN: Code restructure failed: missing block: B:21:0x004c, code lost:
        throw com.google.zxing.FormatException.a();
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public boolean g(java.lang.String r10) throws com.google.zxing.FormatException {
        /*
            r9 = this;
            int r0 = r10.length()
            r1 = 0
            if (r0 != 0) goto L8
            goto L53
        L8:
            r2 = 1
            int r0 = r0 - r2
            char r3 = r10.charAt(r0)
            r4 = 10
            int r3 = java.lang.Character.digit(r3, r4)
            java.lang.CharSequence r10 = r10.subSequence(r1, r0)
            int r0 = r10.length()
            int r5 = r0 + (-1)
            r6 = 0
        L1f:
            r7 = 9
            if (r5 < 0) goto L36
            char r8 = r10.charAt(r5)
            int r8 = r8 + (-48)
            if (r8 < 0) goto L31
            if (r8 > r7) goto L31
            int r6 = r6 + r8
            int r5 = r5 + (-2)
            goto L1f
        L31:
            com.google.zxing.FormatException r10 = com.google.zxing.FormatException.a()
            throw r10
        L36:
            int r6 = r6 * 3
        L38:
            int r0 = r0 + (-2)
            if (r0 < 0) goto L4d
            char r5 = r10.charAt(r0)
            int r5 = r5 + (-48)
            if (r5 < 0) goto L48
            if (r5 > r7) goto L48
            int r6 = r6 + r5
            goto L38
        L48:
            com.google.zxing.FormatException r10 = com.google.zxing.FormatException.a()
            throw r10
        L4d:
            int r10 = 1000 - r6
            int r10 = r10 % r4
            if (r10 != r3) goto L53
            r1 = 1
        L53:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.e.q.p.g(java.lang.String):boolean");
    }

    public int[] i(a aVar, int i) throws NotFoundException {
        int[] iArr = a;
        return l(aVar, i, false, iArr, new int[iArr.length]);
    }

    public abstract int j(a aVar, int[] iArr, StringBuilder sb) throws NotFoundException;

    public Result k(int i, a aVar, int[] iArr, Map<d, ?> map) throws NotFoundException, ChecksumException, FormatException {
        l lVar;
        int i2;
        int[] iArr2;
        boolean z2;
        String str = null;
        if (map == null) {
            lVar = null;
        } else {
            lVar = (l) map.get(d.NEED_RESULT_POINT_CALLBACK);
        }
        if (lVar != null) {
            lVar.a(new k((iArr[0] + iArr[1]) / 2.0f, i));
        }
        StringBuilder sb = this.e;
        sb.setLength(0);
        int j = j(aVar, iArr, sb);
        if (lVar != null) {
            lVar.a(new k(j, i));
        }
        int[] i3 = i(aVar, j);
        if (lVar != null) {
            lVar.a(new k((i3[0] + i3[1]) / 2.0f, i));
        }
        int i4 = i3[1];
        int i5 = (i4 - i3[0]) + i4;
        if (i5 >= aVar.k || !aVar.h(i4, i5, false)) {
            throw NotFoundException.l;
        }
        String sb2 = sb.toString();
        if (sb2.length() < 8) {
            throw FormatException.a();
        } else if (g(sb2)) {
            b.i.e.a n = n();
            float f = i;
            Result result = new Result(sb2, null, new k[]{new k((iArr[1] + iArr[0]) / 2.0f, f), new k((i3[1] + i3[0]) / 2.0f, f)}, n);
            try {
                Result a2 = this.f.a(i, aVar, i3[1]);
                result.b(j.UPC_EAN_EXTENSION, a2.a);
                result.a(a2.e);
                k[] kVarArr = a2.c;
                k[] kVarArr2 = result.c;
                if (kVarArr2 == null) {
                    result.c = kVarArr;
                } else if (kVarArr != null && kVarArr.length > 0) {
                    k[] kVarArr3 = new k[kVarArr2.length + kVarArr.length];
                    System.arraycopy(kVarArr2, 0, kVarArr3, 0, kVarArr2.length);
                    System.arraycopy(kVarArr, 0, kVarArr3, kVarArr2.length, kVarArr.length);
                    result.c = kVarArr3;
                }
                i2 = a2.a.length();
            } catch (ReaderException unused) {
                i2 = 0;
            }
            if (map == null) {
                iArr2 = null;
            } else {
                iArr2 = (int[]) map.get(d.ALLOWED_EAN_EXTENSIONS);
            }
            if (iArr2 != null) {
                int length = iArr2.length;
                int i6 = 0;
                while (true) {
                    if (i6 >= length) {
                        z2 = false;
                        break;
                    } else if (i2 == iArr2[i6]) {
                        z2 = true;
                        break;
                    } else {
                        i6++;
                    }
                }
                if (!z2) {
                    throw NotFoundException.l;
                }
            }
            if (n == b.i.e.a.EAN_13 || n == b.i.e.a.UPC_A) {
                g gVar = this.g;
                gVar.b();
                int parseInt = Integer.parseInt(sb2.substring(0, 3));
                int size = gVar.a.size();
                int i7 = 0;
                while (true) {
                    if (i7 < size) {
                        int[] iArr3 = gVar.a.get(i7);
                        int i8 = iArr3[0];
                        if (parseInt < i8) {
                            break;
                        }
                        if (iArr3.length != 1) {
                            i8 = iArr3[1];
                        }
                        if (parseInt <= i8) {
                            str = gVar.f1852b.get(i7);
                            break;
                        }
                        i7++;
                    } else {
                        break;
                    }
                }
                if (str != null) {
                    result.b(j.POSSIBLE_COUNTRY, str);
                }
            }
            return result;
        } else {
            throw ChecksumException.a();
        }
    }

    public abstract b.i.e.a n();
}
