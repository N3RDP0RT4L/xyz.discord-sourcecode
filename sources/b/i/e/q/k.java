package b.i.e.q;

import b.i.e.c;
import b.i.e.d;
import b.i.e.f;
import b.i.e.i;
import b.i.e.j;
import b.i.e.n.a;
import b.i.e.n.h;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
/* compiled from: OneDReader.java */
/* loaded from: classes3.dex */
public abstract class k implements i {
    public static float d(int[] iArr, int[] iArr2, float f) {
        int length = iArr.length;
        int i = 0;
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3++) {
            i += iArr[i3];
            i2 += iArr2[i3];
        }
        if (i < i2) {
            return Float.POSITIVE_INFINITY;
        }
        float f2 = i;
        float f3 = f2 / i2;
        float f4 = f * f3;
        float f5 = 0.0f;
        for (int i4 = 0; i4 < length; i4++) {
            float f6 = iArr2[i4] * f3;
            float f7 = iArr[i4];
            float f8 = f7 > f6 ? f7 - f6 : f6 - f7;
            if (f8 > f4) {
                return Float.POSITIVE_INFINITY;
            }
            f5 += f8;
        }
        return f5 / f2;
    }

    public static void e(a aVar, int i, int[] iArr) throws NotFoundException {
        int length = iArr.length;
        int i2 = 0;
        Arrays.fill(iArr, 0, length, 0);
        int i3 = aVar.k;
        if (i < i3) {
            boolean z2 = !aVar.b(i);
            while (i < i3) {
                if (aVar.b(i) == z2) {
                    i2++;
                    if (i2 == length) {
                        break;
                    }
                    iArr[i2] = 1;
                    z2 = !z2;
                } else {
                    iArr[i2] = iArr[i2] + 1;
                }
                i++;
            }
            if (i2 == length) {
                return;
            }
            if (i2 != length - 1 || i != i3) {
                throw NotFoundException.l;
            }
            return;
        }
        throw NotFoundException.l;
    }

    public static void f(a aVar, int i, int[] iArr) throws NotFoundException {
        int length = iArr.length;
        boolean b2 = aVar.b(i);
        while (i > 0 && length >= 0) {
            i--;
            if (aVar.b(i) != b2) {
                length--;
                b2 = !b2;
            }
        }
        if (length < 0) {
            e(aVar, i + 1, iArr);
            return;
        }
        throw NotFoundException.l;
    }

    @Override // b.i.e.i
    public Result a(c cVar, Map<d, ?> map) throws NotFoundException, FormatException {
        j jVar = j.ORIENTATION;
        try {
            return c(cVar, map);
        } catch (NotFoundException e) {
            if (!(map != null && map.containsKey(d.TRY_HARDER)) || !cVar.a.a.c()) {
                throw e;
            }
            f d = cVar.a.a.d();
            Objects.requireNonNull((h) cVar.a);
            Result c = c(new c(new h(d)), map);
            Map<j, Object> map2 = c.e;
            int i = 270;
            if (map2 != null && map2.containsKey(jVar)) {
                i = (((Integer) map2.get(jVar)).intValue() + 270) % 360;
            }
            c.b(jVar, Integer.valueOf(i));
            b.i.e.k[] kVarArr = c.c;
            if (kVarArr != null) {
                int i2 = d.f1819b;
                for (int i3 = 0; i3 < kVarArr.length; i3++) {
                    kVarArr[i3] = new b.i.e.k((i2 - kVarArr[i3].f1822b) - 1.0f, kVarArr[i3].a);
                }
            }
            return c;
        }
    }

    public abstract Result b(int i, a aVar, Map<d, ?> map) throws NotFoundException, ChecksumException, FormatException;

    /* JADX WARN: Removed duplicated region for block: B:39:0x007d A[Catch: ReaderException -> 0x00ba, TryCatch #0 {ReaderException -> 0x00ba, blocks: (B:37:0x0077, B:39:0x007d, B:41:0x008c), top: B:60:0x0077 }] */
    /* JADX WARN: Removed duplicated region for block: B:75:0x00b9 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final com.google.zxing.Result c(b.i.e.c r20, java.util.Map<b.i.e.d, ?> r21) throws com.google.zxing.NotFoundException {
        /*
            Method dump skipped, instructions count: 219
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.e.q.k.c(b.i.e.c, java.util.Map):com.google.zxing.Result");
    }

    @Override // b.i.e.i
    public void reset() {
    }
}
