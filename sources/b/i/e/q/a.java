package b.i.e.q;
/* compiled from: CodaBarReader.java */
/* loaded from: classes3.dex */
public final class a extends k {
    public static final char[] a = "0123456789-$:/.+ABCD".toCharArray();

    /* renamed from: b  reason: collision with root package name */
    public static final int[] f1849b = {3, 6, 9, 96, 18, 66, 33, 36, 48, 72, 12, 24, 69, 81, 84, 21, 26, 41, 11, 14};
    public static final char[] c = {'A', 'B', 'C', 'D'};
    public final StringBuilder d = new StringBuilder(20);
    public int[] e = new int[80];
    public int f = 0;

    public static boolean g(char[] cArr, char c2) {
        if (cArr != null) {
            for (char c3 : cArr) {
                if (c3 == c2) {
                    return true;
                }
            }
        }
        return false;
    }

    /* JADX WARN: Code restructure failed: missing block: B:34:0x008e, code lost:
        r8 = r3 - 1;
        r7 = r19.e[r8];
        r9 = -8;
        r10 = 0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:35:0x0096, code lost:
        if (r9 >= (-1)) goto L113;
     */
    /* JADX WARN: Code restructure failed: missing block: B:36:0x0098, code lost:
        r10 = r10 + r19.e[r3 + r9];
        r9 = r9 + 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:37:0x00a2, code lost:
        r11 = 2;
     */
    /* JADX WARN: Code restructure failed: missing block: B:38:0x00a5, code lost:
        if (r3 >= r19.f) goto L43;
     */
    /* JADX WARN: Code restructure failed: missing block: B:40:0x00a8, code lost:
        if (r7 < (r10 / 2)) goto L41;
     */
    /* JADX WARN: Code restructure failed: missing block: B:42:0x00ad, code lost:
        throw com.google.zxing.NotFoundException.l;
     */
    /* JADX WARN: Code restructure failed: missing block: B:43:0x00ae, code lost:
        r7 = new int[]{0, 0, 0, 0};
        r9 = new int[]{0, 0, 0, 0};
        r10 = r19.d.length() - 1;
        r12 = r1;
        r5 = 0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:44:0x00c2, code lost:
        r13 = b.i.e.q.a.f1849b[r19.d.charAt(r5)];
        r15 = 6;
     */
    /* JADX WARN: Code restructure failed: missing block: B:45:0x00cd, code lost:
        if (r15 < 0) goto L115;
     */
    /* JADX WARN: Code restructure failed: missing block: B:46:0x00cf, code lost:
        r16 = (r15 & 1) + ((r13 & 1) << 1);
        r7[r16] = r7[r16] + r19.e[r12 + r15];
        r9[r16] = r9[r16] + 1;
        r13 = r13 >> 1;
        r15 = r15 - 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:47:0x00ec, code lost:
        if (r5 >= r10) goto L114;
     */
    /* JADX WARN: Code restructure failed: missing block: B:48:0x00ee, code lost:
        r12 = r12 + 8;
        r5 = r5 + 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:49:0x00f3, code lost:
        r5 = new float[4];
        r3 = new float[4];
        r12 = 0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:50:0x00f8, code lost:
        if (r12 >= r11) goto L116;
     */
    /* JADX WARN: Code restructure failed: missing block: B:51:0x00fa, code lost:
        r3[r12] = 0.0f;
        r13 = r12 + 2;
        r3[r13] = ((r7[r13] / r9[r13]) + (r7[r12] / r9[r12])) / 2.0f;
        r5[r12] = r3[r13];
        r5[r13] = ((r7[r13] * 2.0f) + 1.5f) / r9[r13];
        r12 = r12 + 1;
        r11 = 2;
     */
    /* JADX WARN: Code restructure failed: missing block: B:52:0x0129, code lost:
        r9 = r1;
        r7 = 0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:53:0x012b, code lost:
        r11 = b.i.e.q.a.f1849b[r19.d.charAt(r7)];
        r12 = 6;
     */
    /* JADX WARN: Code restructure failed: missing block: B:54:0x0136, code lost:
        if (r12 < 0) goto L120;
     */
    /* JADX WARN: Code restructure failed: missing block: B:55:0x0138, code lost:
        r13 = (r12 & 1) + ((r11 & 1) << 1);
        r14 = r19.e[r9 + r12];
     */
    /* JADX WARN: Code restructure failed: missing block: B:56:0x0149, code lost:
        if (r14 < r3[r13]) goto L118;
     */
    /* JADX WARN: Code restructure failed: missing block: B:58:0x014f, code lost:
        if (r14 > r5[r13]) goto L119;
     */
    /* JADX WARN: Code restructure failed: missing block: B:59:0x0151, code lost:
        r11 = r11 >> 1;
        r12 = r12 - 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:61:0x0158, code lost:
        throw com.google.zxing.NotFoundException.l;
     */
    /* JADX WARN: Code restructure failed: missing block: B:62:0x0159, code lost:
        if (r7 >= r10) goto L117;
     */
    /* JADX WARN: Code restructure failed: missing block: B:63:0x015b, code lost:
        r9 = r9 + 8;
        r7 = r7 + 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:64:0x0160, code lost:
        r3 = 0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:66:0x0167, code lost:
        if (r3 >= r19.d.length()) goto L121;
     */
    /* JADX WARN: Code restructure failed: missing block: B:67:0x0169, code lost:
        r5 = r19.d;
        r5.setCharAt(r3, b.i.e.q.a.a[r5.charAt(r3)]);
        r3 = r3 + 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:68:0x0179, code lost:
        r3 = r19.d.charAt(0);
        r5 = b.i.e.q.a.c;
     */
    /* JADX WARN: Code restructure failed: missing block: B:69:0x0185, code lost:
        if (g(r5, r3) == false) goto L90;
     */
    /* JADX WARN: Code restructure failed: missing block: B:70:0x0187, code lost:
        r3 = r19.d;
     */
    /* JADX WARN: Code restructure failed: missing block: B:71:0x0196, code lost:
        if (g(r5, r3.charAt(r3.length() - 1)) == false) goto L88;
     */
    /* JADX WARN: Code restructure failed: missing block: B:73:0x019f, code lost:
        if (r19.d.length() <= 3) goto L86;
     */
    /* JADX WARN: Code restructure failed: missing block: B:74:0x01a1, code lost:
        if (r22 == null) goto L77;
     */
    /* JADX WARN: Code restructure failed: missing block: B:76:0x01a9, code lost:
        if (r22.containsKey(b.i.e.d.RETURN_CODABAR_START_END) != false) goto L78;
     */
    /* JADX WARN: Code restructure failed: missing block: B:77:0x01ab, code lost:
        r2 = r19.d;
        r2.deleteCharAt(r2.length() - 1);
        r19.d.deleteCharAt(0);
     */
    /* JADX WARN: Code restructure failed: missing block: B:78:0x01ba, code lost:
        r2 = 0;
        r3 = 0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:79:0x01bc, code lost:
        if (r2 >= r1) goto L122;
     */
    /* JADX WARN: Code restructure failed: missing block: B:80:0x01be, code lost:
        r3 = r3 + r19.e[r2];
        r2 = r2 + 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:81:0x01c6, code lost:
        r2 = r3;
     */
    /* JADX WARN: Code restructure failed: missing block: B:82:0x01c7, code lost:
        if (r1 >= r8) goto L123;
     */
    /* JADX WARN: Code restructure failed: missing block: B:83:0x01c9, code lost:
        r3 = r3 + r19.e[r1];
        r1 = r1 + 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:84:0x01d1, code lost:
        r10 = r20;
     */
    /* JADX WARN: Code restructure failed: missing block: B:85:0x01f4, code lost:
        return new com.google.zxing.Result(r19.d.toString(), null, new b.i.e.k[]{new b.i.e.k(r2, r10), new b.i.e.k(r3, r10)}, b.i.e.a.CODABAR);
     */
    /* JADX WARN: Code restructure failed: missing block: B:87:0x01f7, code lost:
        throw com.google.zxing.NotFoundException.l;
     */
    /* JADX WARN: Code restructure failed: missing block: B:89:0x01fa, code lost:
        throw com.google.zxing.NotFoundException.l;
     */
    /* JADX WARN: Code restructure failed: missing block: B:91:0x01fd, code lost:
        throw com.google.zxing.NotFoundException.l;
     */
    @Override // b.i.e.q.k
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public com.google.zxing.Result b(int r20, b.i.e.n.a r21, java.util.Map<b.i.e.d, ?> r22) throws com.google.zxing.NotFoundException {
        /*
            Method dump skipped, instructions count: 554
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.e.q.a.b(int, b.i.e.n.a, java.util.Map):com.google.zxing.Result");
    }

    public final void h(int i) {
        int[] iArr = this.e;
        int i2 = this.f;
        iArr[i2] = i;
        int i3 = i2 + 1;
        this.f = i3;
        if (i3 >= iArr.length) {
            int[] iArr2 = new int[i3 << 1];
            System.arraycopy(iArr, 0, iArr2, 0, i3);
            this.e = iArr2;
        }
    }

    public final int i(int i) {
        int i2 = i + 7;
        if (i2 >= this.f) {
            return -1;
        }
        int[] iArr = this.e;
        int i3 = Integer.MAX_VALUE;
        int i4 = 0;
        int i5 = Integer.MAX_VALUE;
        int i6 = 0;
        for (int i7 = i; i7 < i2; i7 += 2) {
            int i8 = iArr[i7];
            if (i8 < i5) {
                i5 = i8;
            }
            if (i8 > i6) {
                i6 = i8;
            }
        }
        int i9 = (i5 + i6) / 2;
        int i10 = 0;
        for (int i11 = i + 1; i11 < i2; i11 += 2) {
            int i12 = iArr[i11];
            if (i12 < i3) {
                i3 = i12;
            }
            if (i12 > i10) {
                i10 = i12;
            }
        }
        int i13 = (i3 + i10) / 2;
        int i14 = 128;
        int i15 = 0;
        for (int i16 = 0; i16 < 7; i16++) {
            i14 >>= 1;
            if (iArr[i + i16] > ((i16 & 1) == 0 ? i9 : i13)) {
                i15 |= i14;
            }
        }
        while (true) {
            int[] iArr2 = f1849b;
            if (i4 >= iArr2.length) {
                return -1;
            }
            if (iArr2[i4] == i15) {
                return i4;
            }
            i4++;
        }
    }
}
