package b.i.e.q;

import b.i.e.a;
import b.i.e.c;
import b.i.e.d;
import b.i.e.j;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import java.util.Map;
/* compiled from: UPCAReader.java */
/* loaded from: classes3.dex */
public final class l extends p {
    public final p h = new e();

    public static Result o(Result result) throws FormatException {
        String str = result.a;
        if (str.charAt(0) == '0') {
            Result result2 = new Result(str.substring(1), null, result.c, a.UPC_A);
            Map<j, Object> map = result.e;
            if (map != null) {
                result2.a(map);
            }
            return result2;
        }
        throw FormatException.a();
    }

    @Override // b.i.e.q.k, b.i.e.i
    public Result a(c cVar, Map<d, ?> map) throws NotFoundException, FormatException {
        return o(this.h.a(cVar, map));
    }

    @Override // b.i.e.q.p, b.i.e.q.k
    public Result b(int i, b.i.e.n.a aVar, Map<d, ?> map) throws NotFoundException, FormatException, ChecksumException {
        return o(this.h.b(i, aVar, map));
    }

    @Override // b.i.e.q.p
    public int j(b.i.e.n.a aVar, int[] iArr, StringBuilder sb) throws NotFoundException {
        return this.h.j(aVar, iArr, sb);
    }

    @Override // b.i.e.q.p
    public Result k(int i, b.i.e.n.a aVar, int[] iArr, Map<d, ?> map) throws NotFoundException, FormatException, ChecksumException {
        return o(this.h.k(i, aVar, iArr, map));
    }

    @Override // b.i.e.q.p
    public a n() {
        return a.UPC_A;
    }
}
