package b.i.e.q;

import b.i.e.j;
import b.i.e.k;
import b.i.e.n.a;
import com.google.zxing.NotFoundException;
import com.google.zxing.ReaderException;
import com.google.zxing.Result;
import java.util.EnumMap;
/* compiled from: UPCEANExtensionSupport.java */
/* loaded from: classes3.dex */
public final class o {
    public static final int[] a = {1, 1, 2};

    /* renamed from: b  reason: collision with root package name */
    public final m f1856b = new m();
    public final n c = new n();

    public Result a(int i, a aVar, int i2) throws NotFoundException {
        EnumMap enumMap;
        int[] iArr = a;
        int[] l = p.l(aVar, i2, false, iArr, new int[iArr.length]);
        try {
            return this.c.a(i, aVar, l);
        } catch (ReaderException unused) {
            m mVar = this.f1856b;
            StringBuilder sb = mVar.f1854b;
            sb.setLength(0);
            int[] iArr2 = mVar.a;
            iArr2[0] = 0;
            iArr2[1] = 0;
            iArr2[2] = 0;
            iArr2[3] = 0;
            int i3 = aVar.k;
            int i4 = l[1];
            int i5 = 0;
            for (int i6 = 0; i6 < 2 && i4 < i3; i6++) {
                int h = p.h(aVar, iArr2, i4, p.d);
                sb.append((char) ((h % 10) + 48));
                for (int i7 : iArr2) {
                    i4 += i7;
                }
                if (h >= 10) {
                    i5 |= 1 << (1 - i6);
                }
                if (i6 != 1) {
                    i4 = aVar.g(aVar.f(i4));
                }
            }
            if (sb.length() != 2) {
                throw NotFoundException.l;
            } else if (Integer.parseInt(sb.toString()) % 4 == i5) {
                String sb2 = sb.toString();
                if (sb2.length() != 2) {
                    enumMap = null;
                } else {
                    enumMap = new EnumMap(j.class);
                    enumMap.put((EnumMap) j.ISSUE_NUMBER, (j) Integer.valueOf(sb2));
                }
                float f = i;
                Result result = new Result(sb2, null, new k[]{new k((l[0] + l[1]) / 2.0f, f), new k(i4, f)}, b.i.e.a.UPC_EAN_EXTENSION);
                if (enumMap != null) {
                    result.a(enumMap);
                }
                return result;
            } else {
                throw NotFoundException.l;
            }
        }
    }
}
