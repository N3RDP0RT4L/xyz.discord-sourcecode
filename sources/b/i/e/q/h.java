package b.i.e.q;

import b.i.e.d;
import b.i.e.k;
import b.i.e.n.a;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import java.util.Map;
/* compiled from: ITFReader.java */
/* loaded from: classes3.dex */
public final class h extends k {
    public static final int[] a = {6, 8, 10, 12, 14};

    /* renamed from: b  reason: collision with root package name */
    public static final int[] f1853b = {1, 1, 1, 1};
    public static final int[][] c = {new int[]{1, 1, 2}, new int[]{1, 1, 3}};
    public static final int[][] d = {new int[]{1, 1, 2, 2, 1}, new int[]{2, 1, 1, 1, 2}, new int[]{1, 2, 1, 1, 2}, new int[]{2, 2, 1, 1, 1}, new int[]{1, 1, 2, 1, 2}, new int[]{2, 1, 2, 1, 1}, new int[]{1, 2, 2, 1, 1}, new int[]{1, 1, 1, 2, 2}, new int[]{2, 1, 1, 2, 1}, new int[]{1, 2, 1, 2, 1}, new int[]{1, 1, 3, 3, 1}, new int[]{3, 1, 1, 1, 3}, new int[]{1, 3, 1, 1, 3}, new int[]{3, 3, 1, 1, 1}, new int[]{1, 1, 3, 1, 3}, new int[]{3, 1, 3, 1, 1}, new int[]{1, 3, 3, 1, 1}, new int[]{1, 1, 1, 3, 3}, new int[]{3, 1, 1, 3, 1}, new int[]{1, 3, 1, 3, 1}};
    public int e = -1;

    public static int g(int[] iArr) throws NotFoundException {
        int length = d.length;
        float f = 0.38f;
        int i = -1;
        for (int i2 = 0; i2 < length; i2++) {
            float d2 = k.d(iArr, d[i2], 0.5f);
            if (d2 < f) {
                i = i2;
                f = d2;
            } else if (d2 == f) {
                i = -1;
            }
        }
        if (i >= 0) {
            return i % 10;
        }
        throw NotFoundException.l;
    }

    public static int[] h(a aVar, int i, int[] iArr) throws NotFoundException {
        int length = iArr.length;
        int[] iArr2 = new int[length];
        int i2 = aVar.k;
        int i3 = i;
        boolean z2 = false;
        int i4 = 0;
        while (i < i2) {
            if (aVar.b(i) != z2) {
                iArr2[i4] = iArr2[i4] + 1;
            } else {
                if (i4 != length - 1) {
                    i4++;
                } else if (k.d(iArr2, iArr, 0.5f) < 0.38f) {
                    return new int[]{i3, i};
                } else {
                    i3 += iArr2[0] + iArr2[1];
                    int i5 = i4 - 1;
                    System.arraycopy(iArr2, 2, iArr2, 0, i5);
                    iArr2[i5] = 0;
                    iArr2[i4] = 0;
                    i4 = i5;
                }
                iArr2[i4] = 1;
                z2 = !z2;
            }
            i++;
        }
        throw NotFoundException.l;
    }

    /* JADX WARN: Finally extract failed */
    @Override // b.i.e.q.k
    public Result b(int i, a aVar, Map<d, ?> map) throws FormatException, NotFoundException {
        int[] iArr;
        boolean z2;
        int i2 = aVar.k;
        int f = aVar.f(0);
        if (f != i2) {
            int[] h = h(aVar, f, f1853b);
            this.e = (h[1] - h[0]) / 4;
            i(aVar, h[0]);
            aVar.i();
            try {
                int i3 = aVar.k;
                int f2 = aVar.f(0);
                if (f2 != i3) {
                    try {
                        iArr = h(aVar, f2, c[0]);
                    } catch (NotFoundException unused) {
                        iArr = h(aVar, f2, c[1]);
                    }
                    i(aVar, iArr[0]);
                    int i4 = iArr[0];
                    int i5 = aVar.k;
                    iArr[0] = i5 - iArr[1];
                    iArr[1] = i5 - i4;
                    aVar.i();
                    StringBuilder sb = new StringBuilder(20);
                    int i6 = h[1];
                    int i7 = iArr[0];
                    int[] iArr2 = new int[10];
                    int[] iArr3 = new int[5];
                    int[] iArr4 = new int[5];
                    while (i6 < i7) {
                        k.e(aVar, i6, iArr2);
                        for (int i8 = 0; i8 < 5; i8++) {
                            int i9 = i8 * 2;
                            iArr3[i8] = iArr2[i9];
                            iArr4[i8] = iArr2[i9 + 1];
                        }
                        sb.append((char) (g(iArr3) + 48));
                        sb.append((char) (g(iArr4) + 48));
                        for (int i10 = 0; i10 < 10; i10++) {
                            i6 += iArr2[i10];
                        }
                    }
                    String sb2 = sb.toString();
                    int[] iArr5 = map != null ? (int[]) map.get(d.ALLOWED_LENGTHS) : null;
                    if (iArr5 == null) {
                        iArr5 = a;
                    }
                    int length = sb2.length();
                    int length2 = iArr5.length;
                    int i11 = 0;
                    int i12 = 0;
                    while (true) {
                        if (i11 >= length2) {
                            z2 = false;
                            break;
                        }
                        int i13 = iArr5[i11];
                        if (length == i13) {
                            z2 = true;
                            break;
                        }
                        if (i13 > i12) {
                            i12 = i13;
                        }
                        i11++;
                    }
                    if (!z2 && length > i12) {
                        z2 = true;
                    }
                    if (z2) {
                        float f3 = i;
                        return new Result(sb2, null, new k[]{new k(h[1], f3), new k(iArr[0], f3)}, b.i.e.a.ITF);
                    }
                    throw FormatException.a();
                }
                throw NotFoundException.l;
            } catch (Throwable th) {
                aVar.i();
                throw th;
            }
        } else {
            throw NotFoundException.l;
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:12:0x0019, code lost:
        return;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void i(b.i.e.n.a r3, int r4) throws com.google.zxing.NotFoundException {
        /*
            r2 = this;
            int r0 = r2.e
            int r0 = r0 * 10
            if (r0 >= r4) goto L7
            goto L8
        L7:
            r0 = r4
        L8:
            int r4 = r4 + (-1)
            if (r0 <= 0) goto L17
            if (r4 < 0) goto L17
            boolean r1 = r3.b(r4)
            if (r1 != 0) goto L17
            int r0 = r0 + (-1)
            goto L8
        L17:
            if (r0 != 0) goto L1a
            return
        L1a:
            com.google.zxing.NotFoundException r3 = com.google.zxing.NotFoundException.l
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.e.q.h.i(b.i.e.n.a, int):void");
    }
}
