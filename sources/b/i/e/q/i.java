package b.i.e.q;

import b.i.e.a;
import b.i.e.d;
import b.i.e.q.r.e;
import b.i.e.q.r.f.c;
import com.google.zxing.NotFoundException;
import com.google.zxing.ReaderException;
import com.google.zxing.Result;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
/* compiled from: MultiFormatOneDReader.java */
/* loaded from: classes3.dex */
public final class i extends k {
    public final k[] a;

    public i(Map<d, ?> map) {
        Collection collection = map == null ? null : (Collection) map.get(d.POSSIBLE_FORMATS);
        boolean z2 = (map == null || map.get(d.ASSUME_CODE_39_CHECK_DIGIT) == null) ? false : true;
        ArrayList arrayList = new ArrayList();
        if (collection != null) {
            if (collection.contains(a.EAN_13) || collection.contains(a.UPC_A) || collection.contains(a.EAN_8) || collection.contains(a.UPC_E)) {
                arrayList.add(new j(map));
            }
            if (collection.contains(a.CODE_39)) {
                arrayList.add(new c(z2));
            }
            if (collection.contains(a.CODE_93)) {
                arrayList.add(new d());
            }
            if (collection.contains(a.CODE_128)) {
                arrayList.add(new b());
            }
            if (collection.contains(a.ITF)) {
                arrayList.add(new h());
            }
            if (collection.contains(a.CODABAR)) {
                arrayList.add(new a());
            }
            if (collection.contains(a.RSS_14)) {
                arrayList.add(new e());
            }
            if (collection.contains(a.RSS_EXPANDED)) {
                arrayList.add(new c());
            }
        }
        if (arrayList.isEmpty()) {
            arrayList.add(new j(map));
            arrayList.add(new c(false));
            arrayList.add(new a());
            arrayList.add(new d());
            arrayList.add(new b());
            arrayList.add(new h());
            arrayList.add(new e());
            arrayList.add(new c());
        }
        this.a = (k[]) arrayList.toArray(new k[arrayList.size()]);
    }

    @Override // b.i.e.q.k
    public Result b(int i, b.i.e.n.a aVar, Map<d, ?> map) throws NotFoundException {
        for (k kVar : this.a) {
            try {
                return kVar.b(i, aVar, map);
            } catch (ReaderException unused) {
            }
        }
        throw NotFoundException.l;
    }

    @Override // b.i.e.q.k, b.i.e.i
    public void reset() {
        for (k kVar : this.a) {
            kVar.reset();
        }
    }
}
