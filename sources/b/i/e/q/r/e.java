package b.i.e.q.r;

import b.i.e.d;
import b.i.e.k;
import b.i.e.l;
import b.i.e.n.a;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.objectweb.asm.Opcodes;
/* compiled from: RSS14Reader.java */
/* loaded from: classes3.dex */
public final class e extends a {
    public static final int[] g = {1, 10, 34, 70, 126};
    public static final int[] h = {4, 20, 48, 81};
    public static final int[] i = {0, Opcodes.IF_ICMPLT, 961, 2015, 2715};
    public static final int[] j = {0, 336, 1036, 1516};
    public static final int[] k = {8, 6, 4, 3, 1};
    public static final int[] l = {2, 4, 6, 8};
    public static final int[][] m = {new int[]{3, 8, 2, 1}, new int[]{3, 5, 5, 1}, new int[]{3, 3, 7, 1}, new int[]{3, 1, 9, 1}, new int[]{2, 7, 4, 1}, new int[]{2, 5, 6, 1}, new int[]{2, 3, 8, 1}, new int[]{1, 5, 7, 1}, new int[]{1, 3, 9, 1}};
    public final List<d> n = new ArrayList();
    public final List<d> o = new ArrayList();

    public static void k(Collection<d> collection, d dVar) {
        if (dVar != null) {
            boolean z2 = false;
            Iterator<d> it = collection.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                d next = it.next();
                if (next.a == dVar.a) {
                    next.d++;
                    z2 = true;
                    break;
                }
            }
            if (!z2) {
                collection.add(dVar);
            }
        }
    }

    @Override // b.i.e.q.k
    public Result b(int i2, a aVar, Map<d, ?> map) throws NotFoundException {
        k(this.n, m(aVar, false, i2, map));
        aVar.i();
        k(this.o, m(aVar, true, i2, map));
        aVar.i();
        for (d dVar : this.n) {
            if (dVar.d > 1) {
                for (d dVar2 : this.o) {
                    if (dVar2.d > 1) {
                        int i3 = ((dVar2.f1859b * 16) + dVar.f1859b) % 79;
                        int i4 = (dVar.c.a * 9) + dVar2.c.a;
                        if (i4 > 72) {
                            i4--;
                        }
                        if (i4 > 8) {
                            i4--;
                        }
                        if (i3 == i4) {
                            String valueOf = String.valueOf((dVar.a * 4537077) + dVar2.a);
                            StringBuilder sb = new StringBuilder(14);
                            for (int length = 13 - valueOf.length(); length > 0; length--) {
                                sb.append('0');
                            }
                            sb.append(valueOf);
                            int i5 = 0;
                            for (int i6 = 0; i6 < 13; i6++) {
                                int charAt = sb.charAt(i6) - '0';
                                if ((i6 & 1) == 0) {
                                    charAt *= 3;
                                }
                                i5 += charAt;
                            }
                            int i7 = 10 - (i5 % 10);
                            if (i7 == 10) {
                                i7 = 0;
                            }
                            sb.append(i7);
                            k[] kVarArr = dVar.c.c;
                            k[] kVarArr2 = dVar2.c.c;
                            return new Result(sb.toString(), null, new k[]{kVarArr[0], kVarArr[1], kVarArr2[0], kVarArr2[1]}, b.i.e.a.RSS_14);
                        }
                    }
                }
                continue;
            }
        }
        throw NotFoundException.l;
    }

    /* JADX WARN: Code restructure failed: missing block: B:37:0x009c, code lost:
        if (r4 < 4) goto L49;
     */
    /* JADX WARN: Code restructure failed: missing block: B:48:0x00b2, code lost:
        if (r4 < 4) goto L49;
     */
    /* JADX WARN: Code restructure failed: missing block: B:49:0x00b4, code lost:
        r14 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:50:0x00b6, code lost:
        r14 = false;
     */
    /* JADX WARN: Code restructure failed: missing block: B:51:0x00b7, code lost:
        r15 = false;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final b.i.e.q.r.b l(b.i.e.n.a r18, b.i.e.q.r.c r19, boolean r20) throws com.google.zxing.NotFoundException {
        /*
            Method dump skipped, instructions count: 448
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.e.q.r.e.l(b.i.e.n.a, b.i.e.q.r.c, boolean):b.i.e.q.r.b");
    }

    public final d m(a aVar, boolean z2, int i2, Map<d, ?> map) {
        int[] n;
        l lVar;
        try {
            c o = o(aVar, i2, z2, n(aVar, z2));
            if (map == null) {
                lVar = null;
            } else {
                lVar = (l) map.get(d.NEED_RESULT_POINT_CALLBACK);
            }
            if (lVar != null) {
                float f = (n[0] + n[1]) / 2.0f;
                if (z2) {
                    f = (aVar.k - 1) - f;
                }
                lVar.a(new k(f, i2));
            }
            b l2 = l(aVar, o, true);
            b l3 = l(aVar, o, false);
            return new d((l2.a * 1597) + l3.a, (l3.f1859b * 4) + l2.f1859b, o);
        } catch (NotFoundException unused) {
            return null;
        }
    }

    public final int[] n(a aVar, boolean z2) throws NotFoundException {
        int[] iArr = this.a;
        iArr[0] = 0;
        iArr[1] = 0;
        iArr[2] = 0;
        iArr[3] = 0;
        int i2 = aVar.k;
        int i3 = 0;
        boolean z3 = false;
        while (i3 < i2) {
            z3 = !aVar.b(i3);
            if (z2 == z3) {
                break;
            }
            i3++;
        }
        int i4 = i3;
        int i5 = 0;
        while (i3 < i2) {
            if (aVar.b(i3) != z3) {
                iArr[i5] = iArr[i5] + 1;
            } else {
                if (i5 != 3) {
                    i5++;
                } else if (a.i(iArr)) {
                    return new int[]{i4, i3};
                } else {
                    i4 += iArr[0] + iArr[1];
                    iArr[0] = iArr[2];
                    iArr[1] = iArr[3];
                    iArr[2] = 0;
                    iArr[3] = 0;
                    i5--;
                }
                iArr[i5] = 1;
                z3 = !z3;
            }
            i3++;
        }
        throw NotFoundException.l;
    }

    public final c o(a aVar, int i2, boolean z2, int[] iArr) throws NotFoundException {
        int i3;
        int i4;
        boolean b2 = aVar.b(iArr[0]);
        int i5 = iArr[0] - 1;
        while (i5 >= 0 && b2 != aVar.b(i5)) {
            i5--;
        }
        int i6 = i5 + 1;
        int[] iArr2 = this.a;
        System.arraycopy(iArr2, 0, iArr2, 1, iArr2.length - 1);
        iArr2[0] = iArr[0] - i6;
        int j2 = a.j(iArr2, m);
        int i7 = iArr[1];
        if (z2) {
            int i8 = aVar.k;
            i4 = (i8 - 1) - i6;
            i3 = (i8 - 1) - i7;
        } else {
            i3 = i7;
            i4 = i6;
        }
        return new c(j2, new int[]{i6, iArr[1]}, i4, i3, i2);
    }

    @Override // b.i.e.q.k, b.i.e.i
    public void reset() {
        this.n.clear();
        this.o.clear();
    }
}
