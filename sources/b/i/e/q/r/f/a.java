package b.i.e.q.r.f;

import b.i.e.q.r.b;
import b.i.e.q.r.c;
/* compiled from: ExpandedPair.java */
/* loaded from: classes3.dex */
public final class a {
    public final b a;

    /* renamed from: b  reason: collision with root package name */
    public final b f1861b;
    public final c c;

    public a(b bVar, b bVar2, c cVar, boolean z2) {
        this.a = bVar;
        this.f1861b = bVar2;
        this.c = cVar;
    }

    public static boolean a(Object obj, Object obj2) {
        if (obj == null) {
            return obj2 == null;
        }
        return obj.equals(obj2);
    }

    public static int b(Object obj) {
        if (obj == null) {
            return 0;
        }
        return obj.hashCode();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof a)) {
            return false;
        }
        a aVar = (a) obj;
        return a(this.a, aVar.a) && a(this.f1861b, aVar.f1861b) && a(this.c, aVar.c);
    }

    public int hashCode() {
        return (b(this.a) ^ b(this.f1861b)) ^ b(this.c);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("[ ");
        sb.append(this.a);
        sb.append(" , ");
        sb.append(this.f1861b);
        sb.append(" : ");
        c cVar = this.c;
        sb.append(cVar == null ? "null" : Integer.valueOf(cVar.a));
        sb.append(" ]");
        return sb.toString();
    }
}
