package b.i.e.q.r.f.d;
/* compiled from: DecodedInformation.java */
/* loaded from: classes3.dex */
public final class o extends q {

    /* renamed from: b  reason: collision with root package name */
    public final String f1867b;
    public final int c;
    public final boolean d;

    public o(int i, String str) {
        super(i);
        this.f1867b = str;
        this.d = false;
        this.c = 0;
    }

    public o(int i, String str, int i2) {
        super(i);
        this.d = true;
        this.c = i2;
        this.f1867b = str;
    }
}
