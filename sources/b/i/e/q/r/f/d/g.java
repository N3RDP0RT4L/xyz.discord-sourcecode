package b.i.e.q.r.f.d;

import b.i.e.n.a;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
/* compiled from: AI01AndOtherAIs.java */
/* loaded from: classes3.dex */
public final class g extends h {
    public g(a aVar) {
        super(aVar);
    }

    @Override // b.i.e.q.r.f.d.j
    public String a() throws NotFoundException, FormatException {
        StringBuilder R = b.d.b.a.a.R("(01)");
        int length = R.length();
        R.append(this.f1863b.c(4, 4));
        c(R, 8, length);
        return this.f1863b.a(R, 48);
    }
}
