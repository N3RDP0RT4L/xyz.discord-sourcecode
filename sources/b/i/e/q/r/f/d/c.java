package b.i.e.q.r.f.d;

import b.i.e.n.a;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
/* compiled from: AI01392xDecoder.java */
/* loaded from: classes3.dex */
public final class c extends h {
    public c(a aVar) {
        super(aVar);
    }

    @Override // b.i.e.q.r.f.d.j
    public String a() throws NotFoundException, FormatException {
        if (this.a.k >= 48) {
            StringBuilder sb = new StringBuilder();
            b(sb, 8);
            int c = this.f1863b.c(48, 2);
            sb.append("(392");
            sb.append(c);
            sb.append(')');
            sb.append(this.f1863b.b(50, null).f1867b);
            return sb.toString();
        }
        throw NotFoundException.l;
    }
}
