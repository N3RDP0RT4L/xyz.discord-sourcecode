package b.i.e.q.r.f.d;

import b.i.e.n.a;
import com.google.zxing.NotFoundException;
/* compiled from: AI013x0xDecoder.java */
/* loaded from: classes3.dex */
public abstract class f extends i {
    public f(a aVar) {
        super(aVar);
    }

    @Override // b.i.e.q.r.f.d.j
    public String a() throws NotFoundException {
        if (this.a.k == 60) {
            StringBuilder sb = new StringBuilder();
            b(sb, 5);
            f(sb, 45, 15);
            return sb.toString();
        }
        throw NotFoundException.l;
    }
}
