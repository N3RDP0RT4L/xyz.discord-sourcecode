package b.i.e.q.r.f.d;

import b.i.e.n.a;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
/* compiled from: GeneralAppIdDecoder.java */
/* loaded from: classes3.dex */
public final class s {
    public final a a;

    /* renamed from: b  reason: collision with root package name */
    public final m f1870b = new m();
    public final StringBuilder c = new StringBuilder();

    public s(a aVar) {
        this.a = aVar;
    }

    public static int d(a aVar, int i, int i2) {
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            if (aVar.b(i + i4)) {
                i3 |= 1 << ((i2 - i4) - 1);
            }
        }
        return i3;
    }

    public String a(StringBuilder sb, int i) throws NotFoundException, FormatException {
        String str;
        String str2 = null;
        while (true) {
            o b2 = b(i, str2);
            String a = r.a(b2.f1867b);
            if (a != null) {
                sb.append(a);
            }
            if (b2.d) {
                str = String.valueOf(b2.c);
            } else {
                str = null;
            }
            int i2 = b2.a;
            if (i == i2) {
                return sb.toString();
            }
            i = i2;
            str2 = str;
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:213:0x03a0, code lost:
        r1 = r2.a;
     */
    /* JADX WARN: Code restructure failed: missing block: B:214:0x03a2, code lost:
        if (r1 == null) goto L219;
     */
    /* JADX WARN: Code restructure failed: missing block: B:216:0x03a6, code lost:
        if (r1.d == false) goto L219;
     */
    /* JADX WARN: Code restructure failed: missing block: B:218:0x03b5, code lost:
        return new b.i.e.q.r.f.d.o(r7, r16.c.toString(), r1.c);
     */
    /* JADX WARN: Code restructure failed: missing block: B:21:0x005a, code lost:
        if (r1 >= 63) goto L23;
     */
    /* JADX WARN: Code restructure failed: missing block: B:220:0x03c1, code lost:
        return new b.i.e.q.r.f.d.o(r7, r16.c.toString());
     */
    /* JADX WARN: Code restructure failed: missing block: B:84:0x0174, code lost:
        if (r1 >= 253) goto L86;
     */
    /* JADX WARN: Removed duplicated region for block: B:163:0x02b9  */
    /* JADX WARN: Removed duplicated region for block: B:203:0x037e  */
    /* JADX WARN: Removed duplicated region for block: B:204:0x0387  */
    /* JADX WARN: Removed duplicated region for block: B:209:0x0399  */
    /* JADX WARN: Removed duplicated region for block: B:211:0x039c A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:227:0x00ed A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:229:0x0249 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:231:0x0358 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0061  */
    /* JADX WARN: Removed duplicated region for block: B:88:0x017b  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.i.e.q.r.f.d.o b(int r17, java.lang.String r18) throws com.google.zxing.FormatException {
        /*
            Method dump skipped, instructions count: 1026
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.e.q.r.f.d.s.b(int, java.lang.String):b.i.e.q.r.f.d.o");
    }

    public int c(int i, int i2) {
        return d(this.a, i, i2);
    }

    public final boolean e(int i) {
        int i2 = i + 3;
        if (i2 > this.a.k) {
            return false;
        }
        while (i < i2) {
            if (this.a.b(i)) {
                return false;
            }
            i++;
        }
        return true;
    }

    public final boolean f(int i) {
        if (i + 1 > this.a.k) {
            return false;
        }
        for (int i2 = 0; i2 < 5; i2++) {
            int i3 = i2 + i;
            a aVar = this.a;
            if (i3 >= aVar.k) {
                return true;
            }
            if (i2 == 2) {
                if (!aVar.b(i + 2)) {
                    return false;
                }
            } else if (aVar.b(i3)) {
                return false;
            }
        }
        return true;
    }
}
