package b.i.e.q.r.f.d;

import com.google.zxing.FormatException;
/* compiled from: DecodedNumeric.java */
/* loaded from: classes3.dex */
public final class p extends q {

    /* renamed from: b  reason: collision with root package name */
    public final int f1868b;
    public final int c;

    public p(int i, int i2, int i3) throws FormatException {
        super(i);
        if (i2 < 0 || i2 > 10 || i3 < 0 || i3 > 10) {
            throw FormatException.a();
        }
        this.f1868b = i2;
        this.c = i3;
    }
}
