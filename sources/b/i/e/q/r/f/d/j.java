package b.i.e.q.r.f.d;

import b.i.e.n.a;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
/* compiled from: AbstractExpandedDecoder.java */
/* loaded from: classes3.dex */
public abstract class j {
    public final a a;

    /* renamed from: b  reason: collision with root package name */
    public final s f1863b;

    public j(a aVar) {
        this.a = aVar;
        this.f1863b = new s(aVar);
    }

    public abstract String a() throws NotFoundException, FormatException;
}
