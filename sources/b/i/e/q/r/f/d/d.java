package b.i.e.q.r.f.d;

import b.i.e.n.a;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
/* compiled from: AI01393xDecoder.java */
/* loaded from: classes3.dex */
public final class d extends h {
    public d(a aVar) {
        super(aVar);
    }

    @Override // b.i.e.q.r.f.d.j
    public String a() throws NotFoundException, FormatException {
        if (this.a.k >= 48) {
            StringBuilder sb = new StringBuilder();
            b(sb, 8);
            int c = this.f1863b.c(48, 2);
            sb.append("(393");
            sb.append(c);
            sb.append(')');
            int c2 = this.f1863b.c(50, 10);
            if (c2 / 100 == 0) {
                sb.append('0');
            }
            if (c2 / 10 == 0) {
                sb.append('0');
            }
            sb.append(c2);
            sb.append(this.f1863b.b(60, null).f1867b);
            return sb.toString();
        }
        throw NotFoundException.l;
    }
}
