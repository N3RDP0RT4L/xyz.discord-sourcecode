package b.i.e.q.r.f.d;

import b.i.e.n.a;
/* compiled from: AI01weightDecoder.java */
/* loaded from: classes3.dex */
public abstract class i extends h {
    public i(a aVar) {
        super(aVar);
    }

    public abstract void d(StringBuilder sb, int i);

    public abstract int e(int i);

    public final void f(StringBuilder sb, int i, int i2) {
        int d = s.d(this.f1863b.a, i, i2);
        d(sb, d);
        int e = e(d);
        int i3 = 100000;
        for (int i4 = 0; i4 < 5; i4++) {
            if (e / i3 == 0) {
                sb.append('0');
            }
            i3 /= 10;
        }
        sb.append(e);
    }
}
