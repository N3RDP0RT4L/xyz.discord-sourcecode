package b.i.e.q.r.f.d;

import b.i.e.n.a;
import com.google.zxing.NotFoundException;
/* compiled from: AI013x0x1xDecoder.java */
/* loaded from: classes3.dex */
public final class e extends i {
    public final String c;
    public final String d;

    public e(a aVar, String str, String str2) {
        super(aVar);
        this.c = str2;
        this.d = str;
    }

    @Override // b.i.e.q.r.f.d.j
    public String a() throws NotFoundException {
        if (this.a.k == 84) {
            StringBuilder sb = new StringBuilder();
            b(sb, 8);
            f(sb, 48, 20);
            int d = s.d(this.f1863b.a, 68, 16);
            if (d != 38400) {
                sb.append('(');
                sb.append(this.c);
                sb.append(')');
                int i = d % 32;
                int i2 = d / 32;
                int i3 = (i2 % 12) + 1;
                int i4 = i2 / 12;
                if (i4 / 10 == 0) {
                    sb.append('0');
                }
                sb.append(i4);
                if (i3 / 10 == 0) {
                    sb.append('0');
                }
                sb.append(i3);
                if (i / 10 == 0) {
                    sb.append('0');
                }
                sb.append(i);
            }
            return sb.toString();
        }
        throw NotFoundException.l;
    }

    @Override // b.i.e.q.r.f.d.i
    public void d(StringBuilder sb, int i) {
        sb.append('(');
        sb.append(this.d);
        sb.append(i / 100000);
        sb.append(')');
    }

    @Override // b.i.e.q.r.f.d.i
    public int e(int i) {
        return i % 100000;
    }
}
