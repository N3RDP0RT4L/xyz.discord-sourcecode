package b.i.e.q;

import b.i.e.d;
import b.i.e.k;
import b.i.e.n.a;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import java.util.Arrays;
import java.util.Map;
import org.objectweb.asm.Opcodes;
/* compiled from: Code39Reader.java */
/* loaded from: classes3.dex */
public final class c extends k {
    public static final int[] a = {52, 289, 97, 352, 49, 304, 112, 37, 292, 100, 265, 73, 328, 25, 280, 88, 13, 268, 76, 28, 259, 67, 322, 19, 274, 82, 7, 262, 70, 22, 385, Opcodes.INSTANCEOF, 448, Opcodes.I2B, 400, 208, Opcodes.I2L, 388, 196, Opcodes.JSR, Opcodes.IF_ICMPGE, Opcodes.L2D, 42};

    /* renamed from: b  reason: collision with root package name */
    public final boolean f1850b;
    public final StringBuilder c = new StringBuilder(20);
    public final int[] d = new int[9];

    public c(boolean z2) {
        this.f1850b = z2;
    }

    /* JADX WARN: Code restructure failed: missing block: B:18:0x002f, code lost:
        if (r1 >= r0) goto L41;
     */
    /* JADX WARN: Code restructure failed: missing block: B:19:0x0031, code lost:
        if (r4 <= 0) goto L39;
     */
    /* JADX WARN: Code restructure failed: missing block: B:20:0x0033, code lost:
        r2 = r10[r1];
     */
    /* JADX WARN: Code restructure failed: missing block: B:21:0x0035, code lost:
        if (r2 <= r3) goto L42;
     */
    /* JADX WARN: Code restructure failed: missing block: B:22:0x0037, code lost:
        r4 = r4 - 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:23:0x003b, code lost:
        if ((r2 << 1) < r6) goto L43;
     */
    /* JADX WARN: Code restructure failed: missing block: B:24:0x003d, code lost:
        return -1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:25:0x003e, code lost:
        r1 = r1 + 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:26:0x0041, code lost:
        return r5;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static int g(int[] r10) {
        /*
            int r0 = r10.length
            r1 = 0
            r2 = 0
        L3:
            r3 = 2147483647(0x7fffffff, float:NaN)
            int r4 = r10.length
            r5 = 0
        L8:
            if (r5 >= r4) goto L14
            r6 = r10[r5]
            if (r6 >= r3) goto L11
            if (r6 <= r2) goto L11
            r3 = r6
        L11:
            int r5 = r5 + 1
            goto L8
        L14:
            r2 = 0
            r4 = 0
            r5 = 0
            r6 = 0
        L18:
            if (r2 >= r0) goto L2b
            r7 = r10[r2]
            if (r7 <= r3) goto L28
            int r8 = r0 + (-1)
            int r8 = r8 - r2
            r9 = 1
            int r8 = r9 << r8
            r5 = r5 | r8
            int r4 = r4 + 1
            int r6 = r6 + r7
        L28:
            int r2 = r2 + 1
            goto L18
        L2b:
            r2 = 3
            r7 = -1
            if (r4 != r2) goto L42
        L2f:
            if (r1 >= r0) goto L41
            if (r4 <= 0) goto L41
            r2 = r10[r1]
            if (r2 <= r3) goto L3e
            int r4 = r4 + (-1)
            int r2 = r2 << 1
            if (r2 < r6) goto L3e
            return r7
        L3e:
            int r1 = r1 + 1
            goto L2f
        L41:
            return r5
        L42:
            if (r4 > r2) goto L45
            return r7
        L45:
            r2 = r3
            goto L3
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.e.q.c.g(int[]):int");
    }

    @Override // b.i.e.q.k
    public Result b(int i, a aVar, Map<d, ?> map) throws NotFoundException, ChecksumException, FormatException {
        int[] iArr;
        char c;
        int[] iArr2 = this.d;
        Arrays.fill(iArr2, 0);
        StringBuilder sb = this.c;
        sb.setLength(0);
        int i2 = aVar.k;
        int f = aVar.f(0);
        int length = iArr2.length;
        int i3 = f;
        boolean z2 = false;
        int i4 = 0;
        while (f < i2) {
            if (aVar.b(f) != z2) {
                iArr2[i4] = iArr2[i4] + 1;
            } else {
                if (i4 != length - 1) {
                    i4++;
                } else if (g(iArr2) != 148 || !aVar.h(Math.max(0, i3 - ((f - i3) / 2)), i3, false)) {
                    i3 += iArr2[0] + iArr2[1];
                    int i5 = i4 - 1;
                    System.arraycopy(iArr2, 2, iArr2, 0, i5);
                    iArr2[i5] = 0;
                    iArr2[i4] = 0;
                    i4 = i5;
                } else {
                    int f2 = aVar.f(new int[]{i3, f}[1]);
                    int i6 = aVar.k;
                    while (true) {
                        k.e(aVar, f2, iArr2);
                        int g = g(iArr2);
                        if (g >= 0) {
                            int i7 = 0;
                            while (true) {
                                int[] iArr3 = a;
                                if (i7 < iArr3.length) {
                                    if (iArr3[i7] == g) {
                                        c = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%".charAt(i7);
                                        break;
                                    }
                                    i7++;
                                } else if (g == 148) {
                                    c = '*';
                                } else {
                                    throw NotFoundException.l;
                                }
                            }
                            sb.append(c);
                            int i8 = f2;
                            for (int i9 : iArr2) {
                                i8 += i9;
                            }
                            int f3 = aVar.f(i8);
                            if (c == '*') {
                                sb.setLength(sb.length() - 1);
                                int i10 = 0;
                                for (int i11 : iArr2) {
                                    i10 += i11;
                                }
                                int i12 = (f3 - f2) - i10;
                                if (f3 == i6 || (i12 << 1) >= i10) {
                                    if (this.f1850b) {
                                        int length2 = sb.length() - 1;
                                        int i13 = 0;
                                        for (int i14 = 0; i14 < length2; i14++) {
                                            i13 += "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%".indexOf(this.c.charAt(i14));
                                        }
                                        if (sb.charAt(length2) == "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%".charAt(i13 % 43)) {
                                            sb.setLength(length2);
                                        } else {
                                            throw ChecksumException.a();
                                        }
                                    }
                                    if (sb.length() != 0) {
                                        float f4 = i;
                                        return new Result(sb.toString(), null, new k[]{new k((iArr[1] + iArr[0]) / 2.0f, f4), new k((i10 / 2.0f) + f2, f4)}, b.i.e.a.CODE_39);
                                    }
                                    throw NotFoundException.l;
                                }
                                throw NotFoundException.l;
                            }
                            f2 = f3;
                        } else {
                            throw NotFoundException.l;
                        }
                    }
                }
                iArr2[i4] = 1;
                z2 = !z2;
            }
            f++;
        }
        throw NotFoundException.l;
    }
}
