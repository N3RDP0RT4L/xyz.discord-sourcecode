package b.i.e.q;

import b.i.e.a;
import b.i.e.d;
import com.google.zxing.NotFoundException;
import com.google.zxing.ReaderException;
import com.google.zxing.Result;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
/* compiled from: MultiFormatUPCEANReader.java */
/* loaded from: classes3.dex */
public final class j extends k {
    public final p[] a;

    public j(Map<d, ?> map) {
        Collection collection = map == null ? null : (Collection) map.get(d.POSSIBLE_FORMATS);
        ArrayList arrayList = new ArrayList();
        if (collection != null) {
            if (collection.contains(a.EAN_13)) {
                arrayList.add(new e());
            } else if (collection.contains(a.UPC_A)) {
                arrayList.add(new l());
            }
            if (collection.contains(a.EAN_8)) {
                arrayList.add(new f());
            }
            if (collection.contains(a.UPC_E)) {
                arrayList.add(new q());
            }
        }
        if (arrayList.isEmpty()) {
            arrayList.add(new e());
            arrayList.add(new f());
            arrayList.add(new q());
        }
        this.a = (p[]) arrayList.toArray(new p[arrayList.size()]);
    }

    @Override // b.i.e.q.k
    public Result b(int i, b.i.e.n.a aVar, Map<d, ?> map) throws NotFoundException {
        boolean z2;
        a aVar2 = a.UPC_A;
        int[] m = p.m(aVar);
        for (p pVar : this.a) {
            try {
                Result k = pVar.k(i, aVar, m, map);
                boolean z3 = k.d == a.EAN_13 && k.a.charAt(0) == '0';
                Collection collection = map == null ? null : (Collection) map.get(d.POSSIBLE_FORMATS);
                if (collection != null && !collection.contains(aVar2)) {
                    z2 = false;
                    if (z3 || !z2) {
                        return k;
                    }
                    Result result = new Result(k.a.substring(1), k.f3112b, k.c, aVar2);
                    result.a(k.e);
                    return result;
                }
                z2 = true;
                if (z3) {
                }
                return k;
            } catch (ReaderException unused) {
            }
        }
        throw NotFoundException.l;
    }

    @Override // b.i.e.q.k, b.i.e.i
    public void reset() {
        for (p pVar : this.a) {
            Objects.requireNonNull(pVar);
        }
    }
}
