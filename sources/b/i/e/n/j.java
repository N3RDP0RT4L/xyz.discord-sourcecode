package b.i.e.n;

import java.nio.charset.Charset;
/* compiled from: StringUtils.java */
/* loaded from: classes3.dex */
public final class j {
    public static final String a;

    /* renamed from: b  reason: collision with root package name */
    public static final boolean f1835b;

    static {
        String name = Charset.defaultCharset().name();
        a = name;
        f1835b = "SJIS".equalsIgnoreCase(name) || "EUC_JP".equalsIgnoreCase(name);
    }
}
