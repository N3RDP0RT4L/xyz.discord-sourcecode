package b.i.e.n.l;

import java.util.Objects;
/* compiled from: GenericGFPoly.java */
/* loaded from: classes3.dex */
public final class b {
    public final a a;

    /* renamed from: b  reason: collision with root package name */
    public final int[] f1838b;

    public b(a aVar, int[] iArr) {
        if (iArr.length != 0) {
            this.a = aVar;
            int length = iArr.length;
            if (length <= 1 || iArr[0] != 0) {
                this.f1838b = iArr;
                return;
            }
            int i = 1;
            while (i < length && iArr[i] == 0) {
                i++;
            }
            if (i == length) {
                this.f1838b = new int[]{0};
                return;
            }
            int[] iArr2 = new int[length - i];
            this.f1838b = iArr2;
            System.arraycopy(iArr, i, iArr2, 0, iArr2.length);
            return;
        }
        throw new IllegalArgumentException();
    }

    public b a(b bVar) {
        if (!this.a.equals(bVar.a)) {
            throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
        } else if (e()) {
            return bVar;
        } else {
            if (bVar.e()) {
                return this;
            }
            int[] iArr = this.f1838b;
            int[] iArr2 = bVar.f1838b;
            if (iArr.length <= iArr2.length) {
                iArr = iArr2;
                iArr2 = iArr;
            }
            int[] iArr3 = new int[iArr.length];
            int length = iArr.length - iArr2.length;
            System.arraycopy(iArr, 0, iArr3, 0, length);
            for (int i = length; i < iArr.length; i++) {
                iArr3[i] = iArr2[i - length] ^ iArr[i];
            }
            return new b(this.a, iArr3);
        }
    }

    public int b(int i) {
        int[] iArr;
        if (i == 0) {
            return c(0);
        }
        if (i == 1) {
            int i2 = 0;
            for (int i3 : this.f1838b) {
                a aVar = a.a;
                i2 ^= i3;
            }
            return i2;
        }
        int[] iArr2 = this.f1838b;
        int i4 = iArr2[0];
        int length = iArr2.length;
        for (int i5 = 1; i5 < length; i5++) {
            i4 = this.a.c(i, i4) ^ this.f1838b[i5];
        }
        return i4;
    }

    public int c(int i) {
        int[] iArr = this.f1838b;
        return iArr[(iArr.length - 1) - i];
    }

    public int d() {
        return this.f1838b.length - 1;
    }

    public boolean e() {
        return this.f1838b[0] == 0;
    }

    public b f(int i) {
        if (i == 0) {
            return this.a.k;
        }
        if (i == 1) {
            return this;
        }
        int length = this.f1838b.length;
        int[] iArr = new int[length];
        for (int i2 = 0; i2 < length; i2++) {
            iArr[i2] = this.a.c(this.f1838b[i2], i);
        }
        return new b(this.a, iArr);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(d() * 8);
        for (int d = d(); d >= 0; d--) {
            int c = c(d);
            if (c != 0) {
                if (c < 0) {
                    sb.append(" - ");
                    c = -c;
                } else if (sb.length() > 0) {
                    sb.append(" + ");
                }
                if (d == 0 || c != 1) {
                    a aVar = this.a;
                    Objects.requireNonNull(aVar);
                    if (c != 0) {
                        int i = aVar.j[c];
                        if (i == 0) {
                            sb.append('1');
                        } else if (i == 1) {
                            sb.append('a');
                        } else {
                            sb.append("a^");
                            sb.append(i);
                        }
                    } else {
                        throw new IllegalArgumentException();
                    }
                }
                if (d != 0) {
                    if (d == 1) {
                        sb.append('x');
                    } else {
                        sb.append("x^");
                        sb.append(d);
                    }
                }
            }
        }
        return sb.toString();
    }
}
