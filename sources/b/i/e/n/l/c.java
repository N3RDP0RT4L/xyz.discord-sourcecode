package b.i.e.n.l;

import com.google.zxing.common.reedsolomon.ReedSolomonException;
import java.util.Objects;
/* compiled from: ReedSolomonDecoder.java */
/* loaded from: classes3.dex */
public final class c {
    public final a a;

    public c(a aVar) {
        this.a = aVar;
    }

    public void a(int[] iArr, int i) throws ReedSolomonException {
        int[] iArr2;
        b bVar;
        b bVar2;
        int i2 = i;
        b bVar3 = new b(this.a, iArr);
        int[] iArr3 = new int[i2];
        boolean z2 = true;
        for (int i3 = 0; i3 < i2; i3++) {
            a aVar = this.a;
            int b2 = bVar3.b(aVar.i[aVar.o + i3]);
            iArr3[(i2 - 1) - i3] = b2;
            if (b2 != 0) {
                z2 = false;
            }
        }
        if (!z2) {
            b bVar4 = new b(this.a, iArr3);
            b a = this.a.a(i2, 1);
            if (a.d() >= bVar4.d()) {
                a = bVar4;
                bVar4 = a;
            }
            a aVar2 = this.a;
            b bVar5 = aVar2.k;
            b bVar6 = a;
            b bVar7 = bVar4;
            b bVar8 = aVar2.l;
            b bVar9 = bVar5;
            while (bVar6.d() >= i2 / 2) {
                if (!bVar6.e()) {
                    b bVar10 = this.a.k;
                    int b3 = this.a.b(bVar6.c(bVar6.d()));
                    while (bVar7.d() >= bVar6.d() && !bVar7.e()) {
                        int d = bVar7.d() - bVar6.d();
                        int c = this.a.c(bVar7.c(bVar7.d()), b3);
                        bVar10 = bVar10.a(this.a.a(d, c));
                        if (d >= 0) {
                            if (c == 0) {
                                bVar2 = bVar6.a.k;
                            } else {
                                int length = bVar6.f1838b.length;
                                int[] iArr4 = new int[d + length];
                                for (int i4 = 0; i4 < length; i4++) {
                                    iArr4[i4] = bVar6.a.c(bVar6.f1838b[i4], c);
                                }
                                bVar2 = new b(bVar6.a, iArr4);
                            }
                            bVar7 = bVar7.a(bVar2);
                        } else {
                            throw new IllegalArgumentException();
                        }
                    }
                    if (bVar10.a.equals(bVar8.a)) {
                        if (bVar10.e() || bVar8.e()) {
                            bVar = bVar10.a.k;
                        } else {
                            int[] iArr5 = bVar10.f1838b;
                            int length2 = iArr5.length;
                            int[] iArr6 = bVar8.f1838b;
                            int length3 = iArr6.length;
                            int[] iArr7 = new int[(length2 + length3) - 1];
                            for (int i5 = 0; i5 < length2; i5++) {
                                int i6 = iArr5[i5];
                                for (int i7 = 0; i7 < length3; i7++) {
                                    int i8 = i5 + i7;
                                    iArr5 = iArr5;
                                    iArr7[i8] = iArr7[i8] ^ bVar10.a.c(i6, iArr6[i7]);
                                }
                            }
                            bVar = new b(bVar10.a, iArr7);
                        }
                        b a2 = bVar.a(bVar9);
                        if (bVar7.d() < bVar6.d()) {
                            bVar9 = bVar8;
                            bVar8 = a2;
                            i2 = i;
                            bVar6 = bVar7;
                            bVar7 = bVar6;
                        } else {
                            throw new IllegalStateException("Division algorithm failed to reduce polynomial?");
                        }
                    } else {
                        throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
                    }
                } else {
                    throw new ReedSolomonException("r_{i-1} was zero");
                }
            }
            int c2 = bVar8.c(0);
            if (c2 != 0) {
                int b4 = this.a.b(c2);
                b[] bVarArr = {bVar8.f(b4), bVar6.f(b4)};
                b bVar11 = bVarArr[0];
                b bVar12 = bVarArr[1];
                int d2 = bVar11.d();
                if (d2 == 1) {
                    iArr2 = new int[]{bVar11.c(1)};
                } else {
                    int[] iArr8 = new int[d2];
                    int i9 = 0;
                    for (int i10 = 1; i10 < this.a.m && i9 < d2; i10++) {
                        if (bVar11.b(i10) == 0) {
                            iArr8[i9] = this.a.b(i10);
                            i9++;
                        }
                    }
                    if (i9 == d2) {
                        iArr2 = iArr8;
                    } else {
                        throw new ReedSolomonException("Error locator degree does not match number of roots");
                    }
                }
                int length4 = iArr2.length;
                int[] iArr9 = new int[length4];
                for (int i11 = 0; i11 < length4; i11++) {
                    int b5 = this.a.b(iArr2[i11]);
                    int i12 = 1;
                    for (int i13 = 0; i13 < length4; i13++) {
                        if (i11 != i13) {
                            int c3 = this.a.c(iArr2[i13], b5);
                            i12 = this.a.c(i12, (c3 & 1) == 0 ? c3 | 1 : c3 & (-2));
                        }
                    }
                    iArr9[i11] = this.a.c(bVar12.b(b5), this.a.b(i12));
                    a aVar3 = this.a;
                    if (aVar3.o != 0) {
                        iArr9[i11] = aVar3.c(iArr9[i11], b5);
                    }
                }
                for (int i14 = 0; i14 < iArr2.length; i14++) {
                    int length5 = iArr.length - 1;
                    a aVar4 = this.a;
                    int i15 = iArr2[i14];
                    Objects.requireNonNull(aVar4);
                    if (i15 != 0) {
                        int i16 = length5 - aVar4.j[i15];
                        if (i16 >= 0) {
                            iArr[i16] = iArr[i16] ^ iArr9[i14];
                        } else {
                            throw new ReedSolomonException("Bad error location");
                        }
                    } else {
                        throw new IllegalArgumentException();
                    }
                }
                return;
            }
            throw new ReedSolomonException("sigmaTilde(0) was zero");
        }
    }
}
