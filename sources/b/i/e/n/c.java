package b.i.e.n;
/* compiled from: BitSource.java */
/* loaded from: classes3.dex */
public final class c {
    public final byte[] a;

    /* renamed from: b  reason: collision with root package name */
    public int f1826b;
    public int c;

    public c(byte[] bArr) {
        this.a = bArr;
    }

    public int a() {
        return ((this.a.length - this.f1826b) * 8) - this.c;
    }

    public int b(int i) {
        if (i <= 0 || i > 32 || i > a()) {
            throw new IllegalArgumentException(String.valueOf(i));
        }
        int i2 = this.c;
        int i3 = 0;
        if (i2 > 0) {
            int i4 = 8 - i2;
            int i5 = i < i4 ? i : i4;
            int i6 = i4 - i5;
            byte[] bArr = this.a;
            int i7 = this.f1826b;
            i3 = (((255 >> (8 - i5)) << i6) & bArr[i7]) >> i6;
            i -= i5;
            int i8 = i2 + i5;
            this.c = i8;
            if (i8 == 8) {
                this.c = 0;
                this.f1826b = i7 + 1;
            }
        }
        if (i <= 0) {
            return i3;
        }
        while (i >= 8) {
            int i9 = i3 << 8;
            byte[] bArr2 = this.a;
            int i10 = this.f1826b;
            i3 = (bArr2[i10] & 255) | i9;
            this.f1826b = i10 + 1;
            i -= 8;
        }
        if (i <= 0) {
            return i3;
        }
        int i11 = 8 - i;
        int i12 = (i3 << i) | ((((255 >> i11) << i11) & this.a[this.f1826b]) >> i11);
        this.c += i;
        return i12;
    }
}
