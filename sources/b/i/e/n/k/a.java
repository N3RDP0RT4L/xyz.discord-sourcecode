package b.i.e.n.k;

import b.i.a.f.e.o.f;
import b.i.e.k;
import b.i.e.n.b;
import com.google.zxing.NotFoundException;
/* compiled from: WhiteRectangleDetector.java */
/* loaded from: classes3.dex */
public final class a {
    public final b a;

    /* renamed from: b  reason: collision with root package name */
    public final int f1836b;
    public final int c;
    public final int d;
    public final int e;
    public final int f;
    public final int g;

    public a(b bVar, int i, int i2, int i3) throws NotFoundException {
        this.a = bVar;
        int i4 = bVar.k;
        this.f1836b = i4;
        int i5 = bVar.j;
        this.c = i5;
        int i6 = i / 2;
        int i7 = i2 - i6;
        this.d = i7;
        int i8 = i2 + i6;
        this.e = i8;
        int i9 = i3 - i6;
        this.g = i9;
        int i10 = i3 + i6;
        this.f = i10;
        if (i9 < 0 || i7 < 0 || i10 >= i4 || i8 >= i5) {
            throw NotFoundException.l;
        }
    }

    public final boolean a(int i, int i2, int i3, boolean z2) {
        if (z2) {
            while (i <= i2) {
                if (this.a.f(i, i3)) {
                    return true;
                }
                i++;
            }
            return false;
        }
        while (i <= i2) {
            if (this.a.f(i3, i)) {
                return true;
            }
            i++;
        }
        return false;
    }

    public k[] b() throws NotFoundException {
        boolean z2;
        int i = this.d;
        int i2 = this.e;
        int i3 = this.g;
        int i4 = this.f;
        boolean z3 = true;
        boolean z4 = false;
        boolean z5 = false;
        boolean z6 = false;
        boolean z7 = false;
        boolean z8 = false;
        while (z3) {
            boolean z9 = true;
            boolean z10 = false;
            while (true) {
                if ((z9 || !z4) && i2 < this.c) {
                    z9 = a(i3, i4, i2, false);
                    if (z9) {
                        i2++;
                        z4 = true;
                        z10 = true;
                    } else if (!z4) {
                        i2++;
                    }
                }
            }
            if (i2 < this.c) {
                boolean z11 = true;
                while (true) {
                    if ((z11 || !z5) && i4 < this.f1836b) {
                        z11 = a(i, i2, i4, true);
                        if (z11) {
                            i4++;
                            z5 = true;
                            z10 = true;
                        } else if (!z5) {
                            i4++;
                        }
                    }
                }
                if (i4 < this.f1836b) {
                    boolean z12 = true;
                    while (true) {
                        if ((z12 || !z6) && i >= 0) {
                            z12 = a(i3, i4, i, false);
                            if (z12) {
                                i--;
                                z6 = true;
                                z10 = true;
                            } else if (!z6) {
                                i--;
                            }
                        }
                    }
                    if (i >= 0) {
                        z3 = z10;
                        boolean z13 = true;
                        while (true) {
                            if ((z13 || !z8) && i3 >= 0) {
                                z13 = a(i, i2, i3, true);
                                if (z13) {
                                    i3--;
                                    z3 = true;
                                    z8 = true;
                                } else if (!z8) {
                                    i3--;
                                }
                            }
                        }
                        if (i3 >= 0) {
                            if (z3) {
                                z7 = true;
                            }
                        }
                    }
                }
            }
            z2 = true;
            break;
        }
        z2 = false;
        if (z2 || !z7) {
            throw NotFoundException.l;
        }
        int i5 = i2 - i;
        k kVar = null;
        k kVar2 = null;
        for (int i6 = 1; kVar2 == null && i6 < i5; i6++) {
            kVar2 = c(i, i4 - i6, i + i6, i4);
        }
        if (kVar2 != null) {
            k kVar3 = null;
            for (int i7 = 1; kVar3 == null && i7 < i5; i7++) {
                kVar3 = c(i, i3 + i7, i + i7, i3);
            }
            if (kVar3 != null) {
                k kVar4 = null;
                for (int i8 = 1; kVar4 == null && i8 < i5; i8++) {
                    kVar4 = c(i2, i3 + i8, i2 - i8, i3);
                }
                if (kVar4 != null) {
                    for (int i9 = 1; kVar == null && i9 < i5; i9++) {
                        kVar = c(i2, i4 - i9, i2 - i9, i4);
                    }
                    if (kVar != null) {
                        float f = kVar.a;
                        float f2 = kVar.f1822b;
                        float f3 = kVar2.a;
                        float f4 = kVar2.f1822b;
                        float f5 = kVar4.a;
                        float f6 = kVar4.f1822b;
                        float f7 = kVar3.a;
                        float f8 = kVar3.f1822b;
                        return f < ((float) this.c) / 2.0f ? new k[]{new k(f7 - 1.0f, f8 + 1.0f), new k(f3 + 1.0f, f4 + 1.0f), new k(f5 - 1.0f, f6 - 1.0f), new k(f + 1.0f, f2 - 1.0f)} : new k[]{new k(f7 + 1.0f, f8 + 1.0f), new k(f3 + 1.0f, f4 - 1.0f), new k(f5 - 1.0f, f6 + 1.0f), new k(f - 1.0f, f2 - 1.0f)};
                    }
                    throw NotFoundException.l;
                }
                throw NotFoundException.l;
            }
            throw NotFoundException.l;
        }
        throw NotFoundException.l;
    }

    public final k c(float f, float f2, float f3, float f4) {
        int Z0 = f.Z0(f.Q(f, f2, f3, f4));
        float f5 = Z0;
        float f6 = (f3 - f) / f5;
        float f7 = (f4 - f2) / f5;
        for (int i = 0; i < Z0; i++) {
            float f8 = i;
            int Z02 = f.Z0((f8 * f6) + f);
            int Z03 = f.Z0((f8 * f7) + f2);
            if (this.a.f(Z02, Z03)) {
                return new k(Z02, Z03);
            }
        }
        return null;
    }
}
