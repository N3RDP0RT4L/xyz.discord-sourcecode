package b.i.e.o;

import b.i.e.c;
import b.i.e.i;
import b.i.e.j;
import b.i.e.k;
import b.i.e.n.b;
import b.i.e.n.e;
import b.i.e.o.b.d;
import b.i.e.o.c.a;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/* compiled from: DataMatrixReader.java */
/* loaded from: classes3.dex */
public final class a implements i {
    public static final k[] a = new k[0];

    /* renamed from: b  reason: collision with root package name */
    public final d f1839b = new d();

    @Override // b.i.e.i
    public Result a(c cVar, Map<b.i.e.d, ?> map) throws NotFoundException, ChecksumException, FormatException {
        k[] kVarArr;
        e eVar;
        b bVar;
        if (map == null || !map.containsKey(b.i.e.d.PURE_BARCODE)) {
            b.i.e.o.c.a aVar = new b.i.e.o.c.a(cVar.a());
            k[] b2 = aVar.f1846b.b();
            k kVar = b2[0];
            k kVar2 = b2[1];
            k kVar3 = b2[2];
            k kVar4 = b2[3];
            ArrayList arrayList = new ArrayList(4);
            arrayList.add(aVar.e(kVar, kVar2));
            arrayList.add(aVar.e(kVar, kVar3));
            arrayList.add(aVar.e(kVar2, kVar4));
            arrayList.add(aVar.e(kVar3, kVar4));
            Collections.sort(arrayList, new a.c(null));
            a.b bVar2 = (a.b) arrayList.get(0);
            a.b bVar3 = (a.b) arrayList.get(1);
            HashMap hashMap = new HashMap();
            b.i.e.o.c.a.b(hashMap, bVar2.a);
            b.i.e.o.c.a.b(hashMap, bVar2.f1847b);
            b.i.e.o.c.a.b(hashMap, bVar3.a);
            b.i.e.o.c.a.b(hashMap, bVar3.f1847b);
            k kVar5 = null;
            k kVar6 = null;
            k kVar7 = null;
            for (Map.Entry entry : hashMap.entrySet()) {
                k kVar8 = (k) entry.getKey();
                if (((Integer) entry.getValue()).intValue() == 2) {
                    kVar6 = kVar8;
                } else if (kVar5 == null) {
                    kVar5 = kVar8;
                } else {
                    kVar7 = kVar8;
                }
            }
            if (kVar5 == null || kVar6 == null || kVar7 == null) {
                throw NotFoundException.l;
            }
            k[] kVarArr2 = {kVar5, kVar6, kVar7};
            k.b(kVarArr2);
            k kVar9 = kVarArr2[0];
            k kVar10 = kVarArr2[1];
            k kVar11 = kVarArr2[2];
            if (hashMap.containsKey(kVar)) {
                if (!hashMap.containsKey(kVar2)) {
                    kVar = kVar2;
                } else {
                    kVar = !hashMap.containsKey(kVar3) ? kVar3 : kVar4;
                }
            }
            int i = aVar.e(kVar11, kVar).c;
            int i2 = aVar.e(kVar9, kVar).c;
            if ((i & 1) == 1) {
                i++;
            }
            int i3 = i + 2;
            if ((i2 & 1) == 1) {
                i2++;
            }
            int i4 = i2 + 2;
            if (i3 * 4 >= i4 * 7 || i4 * 4 >= i3 * 7) {
                float a2 = b.i.e.o.c.a.a(kVar10, kVar9) / i3;
                int a3 = b.i.e.o.c.a.a(kVar11, kVar);
                float f = kVar.a;
                float f2 = a3;
                float f3 = kVar.f1822b;
                k kVar12 = new k((((f - kVar11.a) / f2) * a2) + f, (a2 * ((f3 - kVar11.f1822b) / f2)) + f3);
                float a4 = b.i.e.o.c.a.a(kVar10, kVar11) / i4;
                int a5 = b.i.e.o.c.a.a(kVar9, kVar);
                float f4 = kVar.a;
                float f5 = a5;
                float f6 = kVar.f1822b;
                k kVar13 = new k((((f4 - kVar9.a) / f5) * a4) + f4, (a4 * ((f6 - kVar9.f1822b) / f5)) + f6);
                if (!aVar.c(kVar12)) {
                    if (!aVar.c(kVar13)) {
                        kVar13 = null;
                    }
                } else if (!aVar.c(kVar13) || Math.abs(i4 - aVar.e(kVar9, kVar12).c) + Math.abs(i3 - aVar.e(kVar11, kVar12).c) <= Math.abs(i4 - aVar.e(kVar9, kVar13).c) + Math.abs(i3 - aVar.e(kVar11, kVar13).c)) {
                    kVar13 = kVar12;
                }
                if (kVar13 != null) {
                    kVar = kVar13;
                }
                int i5 = aVar.e(kVar11, kVar).c;
                int i6 = aVar.e(kVar9, kVar).c;
                if ((i5 & 1) == 1) {
                    i5++;
                }
                int i7 = i5;
                if ((i6 & 1) == 1) {
                    i6++;
                }
                bVar = b.i.e.o.c.a.d(aVar.a, kVar11, kVar10, kVar9, kVar, i7, i6);
            } else {
                float min = Math.min(i4, i3);
                float a6 = b.i.e.o.c.a.a(kVar10, kVar9) / min;
                int a7 = b.i.e.o.c.a.a(kVar11, kVar);
                float f7 = kVar.a;
                float f8 = a7;
                float f9 = kVar.f1822b;
                k kVar14 = new k((((f7 - kVar11.a) / f8) * a6) + f7, (a6 * ((f9 - kVar11.f1822b) / f8)) + f9);
                float a8 = b.i.e.o.c.a.a(kVar10, kVar11) / min;
                int a9 = b.i.e.o.c.a.a(kVar9, kVar);
                float f10 = kVar.a;
                float f11 = a9;
                float f12 = kVar.f1822b;
                k kVar15 = new k((((f10 - kVar9.a) / f11) * a8) + f10, (a8 * ((f12 - kVar9.f1822b) / f11)) + f12);
                if (!aVar.c(kVar14)) {
                    if (!aVar.c(kVar15)) {
                        kVar15 = null;
                    }
                } else if (!aVar.c(kVar15) || Math.abs(aVar.e(kVar11, kVar14).c - aVar.e(kVar9, kVar14).c) <= Math.abs(aVar.e(kVar11, kVar15).c - aVar.e(kVar9, kVar15).c)) {
                    kVar15 = kVar14;
                }
                if (kVar15 != null) {
                    kVar = kVar15;
                }
                int max = Math.max(aVar.e(kVar11, kVar).c, aVar.e(kVar9, kVar).c) + 1;
                if ((max & 1) == 1) {
                    max++;
                }
                int i8 = max;
                bVar = b.i.e.o.c.a.d(aVar.a, kVar11, kVar10, kVar9, kVar, i8, i8);
            }
            kVarArr = new k[]{kVar11, kVar10, kVar9, kVar};
            eVar = this.f1839b.a(bVar);
        } else {
            b a10 = cVar.a();
            int[] i9 = a10.i();
            int[] g = a10.g();
            if (i9 == null || g == null) {
                throw NotFoundException.l;
            }
            int i10 = a10.j;
            int i11 = i9[0];
            int i12 = i9[1];
            while (i11 < i10 && a10.f(i11, i12)) {
                i11++;
            }
            if (i11 != i10) {
                int i13 = i11 - i9[0];
                if (i13 != 0) {
                    int i14 = i9[1];
                    int i15 = g[1];
                    int i16 = i9[0];
                    int i17 = ((g[0] - i16) + 1) / i13;
                    int i18 = ((i15 - i14) + 1) / i13;
                    if (i17 <= 0 || i18 <= 0) {
                        throw NotFoundException.l;
                    }
                    int i19 = i13 / 2;
                    int i20 = i14 + i19;
                    int i21 = i16 + i19;
                    b bVar4 = new b(i17, i18);
                    for (int i22 = 0; i22 < i18; i22++) {
                        int i23 = (i22 * i13) + i20;
                        for (int i24 = 0; i24 < i17; i24++) {
                            if (a10.f((i24 * i13) + i21, i23)) {
                                bVar4.j(i24, i22);
                            }
                        }
                    }
                    eVar = this.f1839b.a(bVar4);
                    kVarArr = a;
                } else {
                    throw NotFoundException.l;
                }
            } else {
                throw NotFoundException.l;
            }
        }
        Result result = new Result(eVar.c, eVar.a, kVarArr, b.i.e.a.DATA_MATRIX);
        List<byte[]> list = eVar.d;
        if (list != null) {
            result.b(j.BYTE_SEGMENTS, list);
        }
        String str = eVar.e;
        if (str != null) {
            result.b(j.ERROR_CORRECTION_LEVEL, str);
        }
        return result;
    }

    @Override // b.i.e.i
    public void reset() {
    }
}
