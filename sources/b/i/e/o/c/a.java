package b.i.e.o.c;

import b.i.a.f.e.o.f;
import b.i.e.k;
import b.i.e.n.i;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel;
import com.google.zxing.NotFoundException;
import java.io.Serializable;
import java.util.Comparator;
import java.util.Map;
/* compiled from: Detector.java */
/* loaded from: classes3.dex */
public final class a {
    public final b.i.e.n.b a;

    /* renamed from: b  reason: collision with root package name */
    public final b.i.e.n.k.a f1846b;

    /* compiled from: Detector.java */
    /* loaded from: classes3.dex */
    public static final class b {
        public final k a;

        /* renamed from: b  reason: collision with root package name */
        public final k f1847b;
        public final int c;

        public b(k kVar, k kVar2, int i, C0158a aVar) {
            this.a = kVar;
            this.f1847b = kVar2;
            this.c = i;
        }

        public String toString() {
            return this.a + AutocompleteViewModel.COMMAND_DISCOVER_TOKEN + this.f1847b + MentionUtilsKt.SLASH_CHAR + this.c;
        }
    }

    /* compiled from: Detector.java */
    /* loaded from: classes3.dex */
    public static final class c implements Serializable, Comparator<b> {
        public c(C0158a aVar) {
        }

        @Override // java.util.Comparator
        public int compare(b bVar, b bVar2) {
            return bVar.c - bVar2.c;
        }
    }

    public a(b.i.e.n.b bVar) throws NotFoundException {
        this.a = bVar;
        this.f1846b = new b.i.e.n.k.a(bVar, 10, bVar.j / 2, bVar.k / 2);
    }

    public static int a(k kVar, k kVar2) {
        return f.Z0(f.Q(kVar.a, kVar.f1822b, kVar2.a, kVar2.f1822b));
    }

    public static void b(Map<k, Integer> map, k kVar) {
        Integer num = map.get(kVar);
        int i = 1;
        if (num != null) {
            i = 1 + num.intValue();
        }
        map.put(kVar, Integer.valueOf(i));
    }

    public static b.i.e.n.b d(b.i.e.n.b bVar, k kVar, k kVar2, k kVar3, k kVar4, int i, int i2) throws NotFoundException {
        float f = i - 0.5f;
        float f2 = i2 - 0.5f;
        return b.i.e.n.f.a.a(bVar, i, i2, i.a(0.5f, 0.5f, f, 0.5f, f, f2, 0.5f, f2, kVar.a, kVar.f1822b, kVar4.a, kVar4.f1822b, kVar3.a, kVar3.f1822b, kVar2.a, kVar2.f1822b));
    }

    public final boolean c(k kVar) {
        float f = kVar.a;
        if (f < 0.0f) {
            return false;
        }
        b.i.e.n.b bVar = this.a;
        if (f >= bVar.j) {
            return false;
        }
        float f2 = kVar.f1822b;
        return f2 > 0.0f && f2 < ((float) bVar.k);
    }

    public final b e(k kVar, k kVar2) {
        a aVar = this;
        int i = (int) kVar.a;
        int i2 = (int) kVar.f1822b;
        int i3 = (int) kVar2.a;
        int i4 = (int) kVar2.f1822b;
        int i5 = 1;
        boolean z2 = Math.abs(i4 - i2) > Math.abs(i3 - i);
        if (z2) {
            i2 = i;
            i = i2;
            i4 = i3;
            i3 = i4;
        }
        int abs = Math.abs(i3 - i);
        int abs2 = Math.abs(i4 - i2);
        int i6 = (-abs) / 2;
        int i7 = i2 < i4 ? 1 : -1;
        if (i >= i3) {
            i5 = -1;
        }
        boolean f = aVar.a.f(z2 ? i2 : i, z2 ? i : i2);
        int i8 = 0;
        while (i != i3) {
            boolean f2 = aVar.a.f(z2 ? i2 : i, z2 ? i : i2);
            if (f2 != f) {
                i8++;
                f = f2;
            }
            i6 += abs2;
            if (i6 > 0) {
                if (i2 == i4) {
                    break;
                }
                i2 += i7;
                i6 -= abs;
            }
            i += i5;
            aVar = this;
        }
        return new b(kVar, kVar2, i8, null);
    }
}
