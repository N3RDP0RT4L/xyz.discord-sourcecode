package b.i.e.r.d;

import b.i.e.k;
import b.i.e.n.b;
import com.google.zxing.NotFoundException;
/* compiled from: BoundingBox.java */
/* loaded from: classes3.dex */
public final class c {
    public final b a;

    /* renamed from: b  reason: collision with root package name */
    public final k f1873b;
    public final k c;
    public final k d;
    public final k e;
    public final int f;
    public final int g;
    public final int h;
    public final int i;

    public c(b bVar, k kVar, k kVar2, k kVar3, k kVar4) throws NotFoundException {
        boolean z2 = false;
        boolean z3 = kVar == null || kVar2 == null;
        z2 = (kVar3 == null || kVar4 == null) ? true : z2;
        if (!z3 || !z2) {
            if (z3) {
                kVar = new k(0.0f, kVar3.f1822b);
                kVar2 = new k(0.0f, kVar4.f1822b);
            } else if (z2) {
                int i = bVar.j;
                kVar3 = new k(i - 1, kVar.f1822b);
                kVar4 = new k(i - 1, kVar2.f1822b);
            }
            this.a = bVar;
            this.f1873b = kVar;
            this.c = kVar2;
            this.d = kVar3;
            this.e = kVar4;
            this.f = (int) Math.min(kVar.a, kVar2.a);
            this.g = (int) Math.max(kVar3.a, kVar4.a);
            this.h = (int) Math.min(kVar.f1822b, kVar3.f1822b);
            this.i = (int) Math.max(kVar2.f1822b, kVar4.f1822b);
            return;
        }
        throw NotFoundException.l;
    }

    public c(c cVar) {
        this.a = cVar.a;
        this.f1873b = cVar.f1873b;
        this.c = cVar.c;
        this.d = cVar.d;
        this.e = cVar.e;
        this.f = cVar.f;
        this.g = cVar.g;
        this.h = cVar.h;
        this.i = cVar.i;
    }
}
