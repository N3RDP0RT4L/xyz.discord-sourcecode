package b.i.e.r.d;

import b.i.e.k;
import b.i.e.n.b;
import b.i.e.r.d.k.a;
/* compiled from: PDF417ScanningDecoder.java */
/* loaded from: classes3.dex */
public final class j {
    public static final a a = new a();

    /* JADX WARN: Removed duplicated region for block: B:77:0x00f2  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static b.i.e.r.d.c a(b.i.e.r.d.h r15) throws com.google.zxing.NotFoundException {
        /*
            Method dump skipped, instructions count: 285
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.e.r.d.j.a(b.i.e.r.d.h):b.i.e.r.d.c");
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Removed duplicated region for block: B:113:0x0299  */
    /* JADX WARN: Removed duplicated region for block: B:130:0x0334  */
    /* JADX WARN: Removed duplicated region for block: B:160:0x03ad A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:253:0x03c4 A[ADDED_TO_REGION, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:98:0x024e  */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:110:0x0294 -> B:111:0x0295). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static b.i.e.n.e b(int[] r25, int r26, int[] r27) throws com.google.zxing.FormatException, com.google.zxing.ChecksumException {
        /*
            Method dump skipped, instructions count: 1272
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.e.r.d.j.b(int[], int, int[]):b.i.e.n.e");
    }

    /* JADX WARN: Code restructure failed: missing block: B:111:0x0032, code lost:
        continue;
     */
    /* JADX WARN: Code restructure failed: missing block: B:112:0x0032, code lost:
        continue;
     */
    /* JADX WARN: Code restructure failed: missing block: B:113:0x0032, code lost:
        continue;
     */
    /* JADX WARN: Removed duplicated region for block: B:117:0x0060 A[EDGE_INSN: B:117:0x0060->B:32:0x0060 ?: BREAK  , SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0025  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x004e  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static b.i.e.r.d.d c(b.i.e.n.b r18, int r19, int r20, boolean r21, int r22, int r23, int r24, int r25) {
        /*
            Method dump skipped, instructions count: 370
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.e.r.d.j.c(b.i.e.n.b, int, int, boolean, int, int, int, int):b.i.e.r.d.d");
    }

    public static h d(b bVar, c cVar, k kVar, boolean z2, int i, int i2) {
        int i3;
        h hVar = new h(cVar, z2);
        int i4 = 0;
        while (i4 < 2) {
            int i5 = i4 == 0 ? 1 : -1;
            int i6 = (int) kVar.a;
            for (int i7 = (int) kVar.f1822b; i7 <= cVar.i && i7 >= cVar.h; i7 += i5) {
                d c = c(bVar, 0, bVar.j, z2, i6, i7, i, i2);
                if (c != null) {
                    hVar.f1877b[i7 - hVar.a.h] = c;
                    if (z2) {
                        i3 = c.a;
                    } else {
                        i3 = c.f1874b;
                    }
                    i6 = i3;
                }
            }
            i4++;
        }
        return hVar;
    }

    public static boolean e(f fVar, int i) {
        return i >= 0 && i <= fVar.d + 1;
    }
}
