package b.i.e.r.d.k;
/* compiled from: ModulusGF.java */
/* loaded from: classes3.dex */
public final class b {
    public static final b a = new b(929, 3);

    /* renamed from: b  reason: collision with root package name */
    public final int[] f1878b;
    public final int[] c;
    public final c d;
    public final c e;

    public b(int i, int i2) {
        this.f1878b = new int[i];
        this.c = new int[i];
        int i3 = 1;
        for (int i4 = 0; i4 < i; i4++) {
            this.f1878b[i4] = i3;
            i3 = (i3 * i2) % i;
        }
        for (int i5 = 0; i5 < i - 1; i5++) {
            this.c[this.f1878b[i5]] = i5;
        }
        this.d = new c(this, new int[]{0});
        this.e = new c(this, new int[]{1});
    }

    public int a(int i, int i2) {
        return (i + i2) % 929;
    }

    public c b(int i, int i2) {
        if (i < 0) {
            throw new IllegalArgumentException();
        } else if (i2 == 0) {
            return this.d;
        } else {
            int[] iArr = new int[i + 1];
            iArr[0] = i2;
            return new c(this, iArr);
        }
    }

    public int c(int i) {
        if (i != 0) {
            return this.f1878b[(929 - this.c[i]) - 1];
        }
        throw new ArithmeticException();
    }

    public int d(int i, int i2) {
        if (i == 0 || i2 == 0) {
            return 0;
        }
        int[] iArr = this.f1878b;
        int[] iArr2 = this.c;
        return iArr[(iArr2[i] + iArr2[i2]) % 928];
    }

    public int e(int i, int i2) {
        return ((i + 929) - i2) % 929;
    }
}
