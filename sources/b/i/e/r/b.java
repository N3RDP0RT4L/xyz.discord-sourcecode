package b.i.e.r;

import b.i.e.i;
import b.i.e.k;
/* compiled from: PDF417Reader.java */
/* loaded from: classes3.dex */
public final class b implements i {
    public static int b(k kVar, k kVar2) {
        if (kVar == null || kVar2 == null) {
            return 0;
        }
        return (int) Math.abs(kVar.a - kVar2.a);
    }

    public static int c(k kVar, k kVar2) {
        if (kVar == null || kVar2 == null) {
            return Integer.MAX_VALUE;
        }
        return (int) Math.abs(kVar.a - kVar2.a);
    }

    /* JADX WARN: Code restructure failed: missing block: B:313:0x064c, code lost:
        r3 = new com.google.zxing.Result(r2.c, r2.a, r4, b.i.e.a.PDF_417);
        r3.b(b.i.e.j.ERROR_CORRECTION_LEVEL, r2.e);
        r2 = (b.i.e.r.c) r2.f;
     */
    /* JADX WARN: Code restructure failed: missing block: B:314:0x0662, code lost:
        if (r2 == null) goto L355;
     */
    /* JADX WARN: Code restructure failed: missing block: B:315:0x0664, code lost:
        r3.b(b.i.e.j.PDF417_EXTRA_METADATA, r2);
     */
    /* JADX WARN: Code restructure failed: missing block: B:316:0x0669, code lost:
        r0.add(r3);
        r2 = 0;
        r5 = 2;
     */
    /* JADX WARN: Code restructure failed: missing block: B:33:0x015e, code lost:
        if (r6.e != r7.e) goto L35;
     */
    /* JADX WARN: Removed duplicated region for block: B:129:0x02c2  */
    /* JADX WARN: Removed duplicated region for block: B:134:0x0310  */
    /* JADX WARN: Removed duplicated region for block: B:351:0x01c5 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:39:0x016f  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x01a2  */
    @Override // b.i.e.i
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public com.google.zxing.Result a(b.i.e.c r33, java.util.Map<b.i.e.d, ?> r34) throws com.google.zxing.NotFoundException, com.google.zxing.FormatException, com.google.zxing.ChecksumException {
        /*
            Method dump skipped, instructions count: 1728
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.e.r.b.a(b.i.e.c, java.util.Map):com.google.zxing.Result");
    }

    @Override // b.i.e.i
    public void reset() {
    }
}
