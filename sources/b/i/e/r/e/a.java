package b.i.e.r.e;

import b.i.e.k;
import b.i.e.n.b;
import java.util.Arrays;
/* compiled from: Detector.java */
/* loaded from: classes3.dex */
public final class a {
    public static final int[] a = {0, 4, 1, 5};

    /* renamed from: b  reason: collision with root package name */
    public static final int[] f1880b = {6, 2, 7, 3};
    public static final int[] c = {8, 1, 1, 1, 1, 1, 1, 3};
    public static final int[] d = {7, 1, 1, 3, 1, 1, 1, 2, 1};

    /* JADX WARN: Code restructure failed: missing block: B:22:0x0068, code lost:
        if (r12 == false) goto L43;
     */
    /* JADX WARN: Code restructure failed: missing block: B:23:0x006a, code lost:
        r1 = r7.iterator();
     */
    /* JADX WARN: Code restructure failed: missing block: B:25:0x0072, code lost:
        if (r1.hasNext() == false) goto L49;
     */
    /* JADX WARN: Code restructure failed: missing block: B:26:0x0074, code lost:
        r2 = (b.i.e.k[]) r1.next();
     */
    /* JADX WARN: Code restructure failed: missing block: B:27:0x007c, code lost:
        if (r2[1] == null) goto L29;
     */
    /* JADX WARN: Code restructure failed: missing block: B:28:0x007e, code lost:
        r10 = (int) java.lang.Math.max(r10, r2[1].f1822b);
     */
    /* JADX WARN: Code restructure failed: missing block: B:30:0x008a, code lost:
        if (r2[3] == null) goto L52;
     */
    /* JADX WARN: Code restructure failed: missing block: B:31:0x008c, code lost:
        r10 = java.lang.Math.max(r10, (int) r2[3].f1822b);
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static java.util.List<b.i.e.k[]> a(boolean r17, b.i.e.n.b r18) {
        /*
            Method dump skipped, instructions count: 189
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.e.r.e.a.a(boolean, b.i.e.n.b):java.util.List");
    }

    public static int[] b(b bVar, int i, int i2, int i3, boolean z2, int[] iArr, int[] iArr2) {
        Arrays.fill(iArr2, 0, iArr2.length, 0);
        int i4 = 0;
        while (bVar.f(i, i2) && i > 0) {
            i4++;
            if (i4 >= 3) {
                break;
            }
            i--;
        }
        int length = iArr.length;
        boolean z3 = z2;
        int i5 = 0;
        int i6 = i;
        while (i < i3) {
            if (bVar.f(i, i2) != z3) {
                iArr2[i5] = iArr2[i5] + 1;
            } else {
                if (i5 != length - 1) {
                    i5++;
                } else if (d(iArr2, iArr, 0.8f) < 0.42f) {
                    return new int[]{i6, i};
                } else {
                    i6 += iArr2[0] + iArr2[1];
                    int i7 = i5 - 1;
                    System.arraycopy(iArr2, 2, iArr2, 0, i7);
                    iArr2[i7] = 0;
                    iArr2[i5] = 0;
                    i5 = i7;
                }
                iArr2[i5] = 1;
                z3 = !z3;
            }
            i++;
        }
        if (i5 != length - 1 || d(iArr2, iArr, 0.8f) >= 0.42f) {
            return null;
        }
        return new int[]{i6, i - 1};
    }

    public static k[] c(b bVar, int i, int i2, int i3, int i4, int[] iArr) {
        boolean z2;
        int i5;
        int i6;
        int i7;
        k[] kVarArr = new k[4];
        int[] iArr2 = new int[iArr.length];
        int i8 = i3;
        while (true) {
            if (i8 >= i) {
                z2 = false;
                break;
            }
            int[] b2 = b(bVar, i4, i8, i2, false, iArr, iArr2);
            if (b2 != null) {
                int i9 = i8;
                int[] iArr3 = b2;
                while (true) {
                    if (i9 <= 0) {
                        i7 = i9;
                        break;
                    }
                    int i10 = i9 - 1;
                    int[] b3 = b(bVar, i4, i10, i2, false, iArr, iArr2);
                    if (b3 == null) {
                        i7 = i10 + 1;
                        break;
                    }
                    iArr3 = b3;
                    i9 = i10;
                }
                float f = i7;
                kVarArr[0] = new k(iArr3[0], f);
                kVarArr[1] = new k(iArr3[1], f);
                i8 = i7;
                z2 = true;
            } else {
                i8 += 5;
            }
        }
        int i11 = i8 + 1;
        if (z2) {
            int[] iArr4 = {(int) kVarArr[0].a, (int) kVarArr[1].a};
            int i12 = i11;
            int i13 = 0;
            while (true) {
                if (i12 >= i) {
                    i5 = i13;
                    i6 = i12;
                    break;
                }
                i5 = i13;
                i6 = i12;
                int[] b4 = b(bVar, iArr4[0], i12, i2, false, iArr, iArr2);
                if (b4 == null || Math.abs(iArr4[0] - b4[0]) >= 5 || Math.abs(iArr4[1] - b4[1]) >= 5) {
                    if (i5 > 25) {
                        break;
                    }
                    i13 = i5 + 1;
                } else {
                    iArr4 = b4;
                    i13 = 0;
                }
                i12 = i6 + 1;
            }
            i11 = i6 - (i5 + 1);
            float f2 = i11;
            kVarArr[2] = new k(iArr4[0], f2);
            kVarArr[3] = new k(iArr4[1], f2);
        }
        if (i11 - i8 < 10) {
            Arrays.fill(kVarArr, (Object) null);
        }
        return kVarArr;
    }

    public static float d(int[] iArr, int[] iArr2, float f) {
        int length = iArr.length;
        int i = 0;
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3++) {
            i += iArr[i3];
            i2 += iArr2[i3];
        }
        if (i < i2) {
            return Float.POSITIVE_INFINITY;
        }
        float f2 = i;
        float f3 = f2 / i2;
        float f4 = f * f3;
        float f5 = 0.0f;
        for (int i4 = 0; i4 < length; i4++) {
            float f6 = iArr2[i4] * f3;
            float f7 = iArr[i4];
            float f8 = f7 > f6 ? f7 - f6 : f6 - f7;
            if (f8 > f4) {
                return Float.POSITIVE_INFINITY;
            }
            f5 += f8;
        }
        return f5 / f2;
    }
}
