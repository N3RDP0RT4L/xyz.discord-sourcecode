package b.i.c.j.a;

import java.util.concurrent.Executor;
/* compiled from: com.google.android.gms:play-services-measurement-api@@18.0.0 */
/* loaded from: classes3.dex */
public final /* synthetic */ class e implements Executor {
    public static final Executor j = new e();

    @Override // java.util.concurrent.Executor
    public final void execute(Runnable runnable) {
        runnable.run();
    }
}
