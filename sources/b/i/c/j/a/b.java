package b.i.c.j.a;

import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;
import b.i.c.j.a.a;
import b.i.c.j.a.c.c;
import b.i.c.j.a.c.d;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
/* compiled from: com.google.android.gms:play-services-measurement-api@@18.0.0 */
/* loaded from: classes3.dex */
public class b implements b.i.c.j.a.a {
    public static volatile b.i.c.j.a.a a;

    /* renamed from: b  reason: collision with root package name */
    public final b.i.a.f.i.a.a f1652b;
    public final Map<String, ?> c = new ConcurrentHashMap();

    /* compiled from: com.google.android.gms:play-services-measurement-api@@18.0.0 */
    /* loaded from: classes3.dex */
    public class a implements a.AbstractC0135a {
        public a(b bVar, String str) {
        }
    }

    public b(b.i.a.f.i.a.a aVar) {
        Objects.requireNonNull(aVar, "null reference");
        this.f1652b = aVar;
    }

    /* JADX WARN: Removed duplicated region for block: B:51:0x00b6 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:52:0x00b7  */
    @Override // b.i.c.j.a.a
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void a(@androidx.annotation.NonNull java.lang.String r8, @androidx.annotation.NonNull java.lang.String r9, android.os.Bundle r10) {
        /*
            Method dump skipped, instructions count: 219
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.c.j.a.b.a(java.lang.String, java.lang.String, android.os.Bundle):void");
    }

    /* JADX WARN: Code restructure failed: missing block: B:14:0x002f, code lost:
        if (r9.equals("fiam") == false) goto L26;
     */
    /* JADX WARN: Code restructure failed: missing block: B:25:0x0060, code lost:
        if (r9.equals("frc") == false) goto L26;
     */
    /* JADX WARN: Removed duplicated region for block: B:28:0x0066 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:29:0x0067  */
    @Override // b.i.c.j.a.a
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void b(@androidx.annotation.NonNull java.lang.String r9, @androidx.annotation.NonNull java.lang.String r10, java.lang.Object r11) {
        /*
            r8 = this;
            boolean r0 = b.i.c.j.a.c.c.a(r9)
            if (r0 != 0) goto L7
            return
        L7:
            java.lang.String r0 = "_ce1"
            boolean r0 = r0.equals(r10)
            java.lang.String r1 = "fcm"
            r2 = 1
            if (r0 != 0) goto L54
            java.lang.String r0 = "_ce2"
            boolean r0 = r0.equals(r10)
            if (r0 == 0) goto L1b
            goto L54
        L1b:
            java.lang.String r0 = "_ln"
            boolean r0 = r0.equals(r10)
            if (r0 == 0) goto L32
            boolean r0 = r9.equals(r1)
            if (r0 != 0) goto L64
            java.lang.String r0 = "fiam"
            boolean r0 = r9.equals(r0)
            if (r0 == 0) goto L63
            goto L64
        L32:
            java.util.List<java.lang.String> r0 = b.i.c.j.a.c.c.e
            boolean r0 = r0.contains(r10)
            if (r0 == 0) goto L3b
            goto L63
        L3b:
            java.util.List<java.lang.String> r0 = b.i.c.j.a.c.c.f
            java.util.Iterator r0 = r0.iterator()
        L41:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L64
            java.lang.Object r1 = r0.next()
            java.lang.String r1 = (java.lang.String) r1
            boolean r1 = r10.matches(r1)
            if (r1 == 0) goto L41
            goto L63
        L54:
            boolean r0 = r9.equals(r1)
            if (r0 != 0) goto L64
            java.lang.String r0 = "frc"
            boolean r0 = r9.equals(r0)
            if (r0 == 0) goto L63
            goto L64
        L63:
            r2 = 0
        L64:
            if (r2 != 0) goto L67
            return
        L67:
            b.i.a.f.i.a.a r0 = r8.f1652b
            b.i.a.f.h.l.g r0 = r0.a
            java.util.Objects.requireNonNull(r0)
            b.i.a.f.h.l.b0 r7 = new b.i.a.f.h.l.b0
            r6 = 1
            r1 = r7
            r2 = r0
            r3 = r9
            r4 = r10
            r5 = r11
            r1.<init>(r2, r3, r4, r5, r6)
            java.util.concurrent.ExecutorService r9 = r0.e
            r9.execute(r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.c.j.a.b.b(java.lang.String, java.lang.String, java.lang.Object):void");
    }

    @Override // b.i.c.j.a.a
    @WorkerThread
    public a.AbstractC0135a c(@NonNull String str, a.b bVar) {
        Object obj;
        Objects.requireNonNull(bVar, "null reference");
        if (!c.a(str)) {
            return null;
        }
        if (!str.isEmpty() && this.c.containsKey(str) && this.c.get(str) != null) {
            return null;
        }
        b.i.a.f.i.a.a aVar = this.f1652b;
        if ("fiam".equals(str)) {
            obj = new b.i.c.j.a.c.b(aVar, bVar);
        } else {
            obj = ("crash".equals(str) || "clx".equals(str)) ? new d(aVar, bVar) : null;
        }
        if (obj == null) {
            return null;
        }
        this.c.put(str, obj);
        return new a(this, str);
    }
}
