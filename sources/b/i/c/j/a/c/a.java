package b.i.c.j.a.c;

import android.content.Context;
import android.os.Bundle;
import b.i.a.f.h.l.g;
import b.i.c.c;
import b.i.c.j.a.b;
import b.i.c.l.e;
import b.i.c.l.f;
import b.i.c.q.d;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-measurement-api@@18.0.0 */
/* loaded from: classes3.dex */
public final /* synthetic */ class a implements f {
    public static final f a = new a();

    @Override // b.i.c.l.f
    public final Object a(e eVar) {
        c cVar = (c) eVar.a(c.class);
        Context context = (Context) eVar.a(Context.class);
        d dVar = (d) eVar.a(d.class);
        Objects.requireNonNull(cVar, "null reference");
        Objects.requireNonNull(context, "null reference");
        Objects.requireNonNull(dVar, "null reference");
        Objects.requireNonNull(context.getApplicationContext(), "null reference");
        if (b.a == null) {
            synchronized (b.class) {
                if (b.a == null) {
                    Bundle bundle = new Bundle(1);
                    if (cVar.h()) {
                        dVar.b(b.i.c.a.class, b.i.c.j.a.e.j, b.i.c.j.a.d.a);
                        bundle.putBoolean("dataCollectionDefaultEnabled", cVar.g());
                    }
                    b.a = new b(g.a(context, null, null, null, bundle).f);
                }
            }
        }
        return b.a;
    }
}
