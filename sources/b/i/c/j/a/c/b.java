package b.i.c.j.a.c;

import b.i.c.j.a.a;
import java.util.HashSet;
import java.util.Set;
/* compiled from: com.google.android.gms:play-services-measurement-api@@18.0.0 */
/* loaded from: classes3.dex */
public final class b {
    public Set<String> a = new HashSet();

    /* renamed from: b  reason: collision with root package name */
    public a.b f1653b;
    public b.i.a.f.i.a.a c;
    public e d;

    public b(b.i.a.f.i.a.a aVar, a.b bVar) {
        this.f1653b = bVar;
        this.c = aVar;
        e eVar = new e(this);
        this.d = eVar;
        aVar.a(eVar);
    }
}
