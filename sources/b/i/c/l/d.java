package b.i.c.l;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
/* compiled from: Component.java */
/* loaded from: classes3.dex */
public final class d<T> {
    public final Set<Class<? super T>> a;

    /* renamed from: b  reason: collision with root package name */
    public final Set<o> f1658b;
    public final int c;
    public final int d;
    public final f<T> e;
    public final Set<Class<?>> f;

    /* compiled from: Component.java */
    /* loaded from: classes3.dex */
    public static class b<T> {
        public final Set<Class<? super T>> a;
        public f<T> e;

        /* renamed from: b  reason: collision with root package name */
        public final Set<o> f1659b = new HashSet();
        public int c = 0;
        public int d = 0;
        public Set<Class<?>> f = new HashSet();

        public b(Class cls, Class[] clsArr, a aVar) {
            HashSet hashSet = new HashSet();
            this.a = hashSet;
            Objects.requireNonNull(cls, "Null interface");
            hashSet.add(cls);
            for (Class cls2 : clsArr) {
                Objects.requireNonNull(cls2, "Null interface");
            }
            Collections.addAll(this.a, clsArr);
        }

        public b<T> a(o oVar) {
            if (!this.a.contains(oVar.a)) {
                this.f1659b.add(oVar);
                return this;
            }
            throw new IllegalArgumentException("Components are not allowed to depend on interfaces they themselves provide.");
        }

        public d<T> b() {
            if (this.e != null) {
                return new d<>(new HashSet(this.a), new HashSet(this.f1659b), this.c, this.d, this.e, this.f, null);
            }
            throw new IllegalStateException("Missing required property: factory.");
        }

        public b<T> c(f<T> fVar) {
            this.e = fVar;
            return this;
        }

        public final b<T> d(int i) {
            if (this.c == 0) {
                this.c = i;
                return this;
            }
            throw new IllegalStateException("Instantiation type has already been set.");
        }
    }

    public d(Set set, Set set2, int i, int i2, f fVar, Set set3, a aVar) {
        this.a = Collections.unmodifiableSet(set);
        this.f1658b = Collections.unmodifiableSet(set2);
        this.c = i;
        this.d = i2;
        this.e = fVar;
        this.f = Collections.unmodifiableSet(set3);
    }

    public static <T> b<T> a(Class<T> cls) {
        return new b<>(cls, new Class[0], null);
    }

    @SafeVarargs
    public static <T> d<T> c(final T t, Class<T> cls, Class<? super T>... clsArr) {
        b bVar = new b(cls, clsArr, null);
        bVar.c(new f(t) { // from class: b.i.c.l.b
            public final Object a;

            {
                this.a = t;
            }

            @Override // b.i.c.l.f
            public Object a(e eVar) {
                return this.a;
            }
        });
        return bVar.b();
    }

    public boolean b() {
        return this.d == 0;
    }

    public String toString() {
        return "Component<" + Arrays.toString(this.a.toArray()) + ">{" + this.c + ", type=" + this.d + ", deps=" + Arrays.toString(this.f1658b.toArray()) + "}";
    }
}
