package b.i.c.l;

import b.i.c.q.c;
import b.i.c.q.d;
import b.i.c.t.a;
import com.google.firebase.components.DependencyCycleException;
import com.google.firebase.components.MissingDependencyException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Executor;
/* compiled from: ComponentRuntime.java */
/* loaded from: classes3.dex */
public class k extends a {
    public static final /* synthetic */ int a = 0;

    /* renamed from: b  reason: collision with root package name */
    public final Map<d<?>, r<?>> f1661b = new HashMap();
    public final Map<Class<?>, r<?>> c = new HashMap();
    public final Map<Class<?>, r<Set<?>>> d = new HashMap();
    public final q e;

    public k(Executor executor, Iterable<g> iterable, d<?>... dVarArr) {
        Set<m> set;
        q qVar = new q(executor);
        this.e = qVar;
        ArrayList arrayList = new ArrayList();
        arrayList.add(d.c(qVar, q.class, d.class, c.class));
        for (g gVar : iterable) {
            arrayList.addAll(gVar.getComponents());
        }
        for (d<?> dVar : dVarArr) {
            if (dVar != null) {
                arrayList.add(dVar);
            }
        }
        HashMap hashMap = new HashMap(arrayList.size());
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            d dVar2 = (d) it.next();
            m mVar = new m(dVar2);
            Iterator it2 = dVar2.a.iterator();
            while (it2.hasNext()) {
                Class cls = (Class) it2.next();
                boolean z2 = !dVar2.b();
                n nVar = new n(cls, z2, null);
                if (!hashMap.containsKey(nVar)) {
                    hashMap.put(nVar, new HashSet());
                }
                Set set2 = (Set) hashMap.get(nVar);
                if (set2.isEmpty() || z2) {
                    set2.add(mVar);
                } else {
                    throw new IllegalArgumentException(String.format("Multiple components provide %s.", cls));
                }
            }
        }
        for (Set<m> set3 : hashMap.values()) {
            for (m mVar2 : set3) {
                for (o oVar : mVar2.a.f1658b) {
                    if ((oVar.c == 0) && (set = (Set) hashMap.get(new n(oVar.a, oVar.a(), null))) != null) {
                        for (m mVar3 : set) {
                            mVar2.f1662b.add(mVar3);
                            mVar3.c.add(mVar2);
                        }
                    }
                }
            }
        }
        HashSet hashSet = new HashSet();
        for (Set set4 : hashMap.values()) {
            hashSet.addAll(set4);
        }
        HashSet hashSet2 = new HashSet();
        Iterator it3 = hashSet.iterator();
        while (it3.hasNext()) {
            m mVar4 = (m) it3.next();
            if (mVar4.a()) {
                hashSet2.add(mVar4);
            }
        }
        int i = 0;
        while (!hashSet2.isEmpty()) {
            m mVar5 = (m) hashSet2.iterator().next();
            hashSet2.remove(mVar5);
            i++;
            for (m mVar6 : mVar5.f1662b) {
                mVar6.c.remove(mVar5);
                if (mVar6.a()) {
                    hashSet2.add(mVar6);
                }
            }
        }
        if (i == arrayList.size()) {
            Iterator it4 = arrayList.iterator();
            while (it4.hasNext()) {
                final d<?> dVar3 = (d) it4.next();
                this.f1661b.put(dVar3, new r<>(new a(this, dVar3) { // from class: b.i.c.l.h
                    public final k a;

                    /* renamed from: b  reason: collision with root package name */
                    public final d f1660b;

                    {
                        this.a = this;
                        this.f1660b = dVar3;
                    }

                    @Override // b.i.c.t.a
                    public Object get() {
                        k kVar = this.a;
                        d dVar4 = this.f1660b;
                        int i2 = k.a;
                        return dVar4.e.a(new s(dVar4, kVar));
                    }
                }));
            }
            for (Map.Entry<d<?>, r<?>> entry : this.f1661b.entrySet()) {
                d<?> key = entry.getKey();
                if (key.b()) {
                    r<?> value = entry.getValue();
                    for (Class<? super Object> cls2 : key.a) {
                        this.c.put(cls2, value);
                    }
                }
            }
            for (d<?> dVar4 : this.f1661b.keySet()) {
                for (o oVar2 : dVar4.f1658b) {
                    if ((oVar2.f1664b == 1) && !this.c.containsKey(oVar2.a)) {
                        throw new MissingDependencyException(String.format("Unsatisfied dependency for component %s: %s", dVar4, oVar2.a));
                    }
                }
            }
            HashMap hashMap2 = new HashMap();
            for (Map.Entry<d<?>, r<?>> entry2 : this.f1661b.entrySet()) {
                d<?> key2 = entry2.getKey();
                if (!key2.b()) {
                    r<?> value2 = entry2.getValue();
                    for (Class<? super Object> cls3 : key2.a) {
                        if (!hashMap2.containsKey(cls3)) {
                            hashMap2.put(cls3, new HashSet());
                        }
                        ((Set) hashMap2.get(cls3)).add(value2);
                    }
                }
            }
            for (Map.Entry entry3 : hashMap2.entrySet()) {
                final Set set5 = (Set) entry3.getValue();
                this.d.put((Class) entry3.getKey(), new r<>(new a(set5) { // from class: b.i.c.l.i
                    public final Set a;

                    {
                        this.a = set5;
                    }

                    @Override // b.i.c.t.a
                    public Object get() {
                        Set<r> set6 = this.a;
                        int i2 = k.a;
                        HashSet hashSet3 = new HashSet();
                        for (r rVar : set6) {
                            hashSet3.add(rVar.get());
                        }
                        return Collections.unmodifiableSet(hashSet3);
                    }
                }));
            }
            return;
        }
        ArrayList arrayList2 = new ArrayList();
        Iterator it5 = hashSet.iterator();
        while (it5.hasNext()) {
            m mVar7 = (m) it5.next();
            if (!mVar7.a() && !mVar7.f1662b.isEmpty()) {
                arrayList2.add(mVar7.a);
            }
        }
        throw new DependencyCycleException(arrayList2);
    }

    @Override // b.i.c.l.e
    public <T> a<T> b(Class<T> cls) {
        Objects.requireNonNull(cls, "Null interface requested.");
        return this.c.get(cls);
    }

    @Override // b.i.c.l.e
    public <T> a<Set<T>> c(Class<T> cls) {
        r<Set<?>> rVar = this.d.get(cls);
        return rVar != null ? rVar : j.a;
    }
}
