package b.i.c.l;

import java.util.Set;
/* compiled from: AbstractComponentContainer.java */
/* loaded from: classes3.dex */
public abstract class a implements e {
    @Override // b.i.c.l.e
    public <T> T a(Class<T> cls) {
        b.i.c.t.a<T> b2 = b(cls);
        if (b2 == null) {
            return null;
        }
        return b2.get();
    }

    @Override // b.i.c.l.e
    public <T> Set<T> d(Class<T> cls) {
        return c(cls).get();
    }
}
