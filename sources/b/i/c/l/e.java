package b.i.c.l;

import b.i.c.t.a;
import java.util.Set;
/* compiled from: ComponentContainer.java */
/* loaded from: classes3.dex */
public interface e {
    <T> T a(Class<T> cls);

    <T> a<T> b(Class<T> cls);

    <T> a<Set<T>> c(Class<T> cls);

    <T> Set<T> d(Class<T> cls);
}
