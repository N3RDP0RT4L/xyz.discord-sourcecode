package b.i.c.l;

import b.d.b.a.a;
import java.util.Objects;
/* compiled from: Dependency.java */
/* loaded from: classes3.dex */
public final class o {
    public final Class<?> a;

    /* renamed from: b  reason: collision with root package name */
    public final int f1664b;
    public final int c;

    public o(Class<?> cls, int i, int i2) {
        Objects.requireNonNull(cls, "Null dependency anInterface.");
        this.a = cls;
        this.f1664b = i;
        this.c = i2;
    }

    public boolean a() {
        return this.f1664b == 2;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof o)) {
            return false;
        }
        o oVar = (o) obj;
        return this.a == oVar.a && this.f1664b == oVar.f1664b && this.c == oVar.c;
    }

    public int hashCode() {
        return ((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.f1664b) * 1000003) ^ this.c;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Dependency{anInterface=");
        sb.append(this.a);
        sb.append(", type=");
        int i = this.f1664b;
        boolean z2 = true;
        sb.append(i == 1 ? "required" : i == 0 ? "optional" : "set");
        sb.append(", direct=");
        if (this.c != 0) {
            z2 = false;
        }
        return a.M(sb, z2, "}");
    }
}
