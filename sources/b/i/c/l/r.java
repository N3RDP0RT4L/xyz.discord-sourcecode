package b.i.c.l;

import b.i.c.t.a;
/* compiled from: Lazy.java */
/* loaded from: classes3.dex */
public class r<T> implements a<T> {
    public static final Object a = new Object();

    /* renamed from: b  reason: collision with root package name */
    public volatile Object f1666b = a;
    public volatile a<T> c;

    public r(a<T> aVar) {
        this.c = aVar;
    }

    @Override // b.i.c.t.a
    public T get() {
        T t = (T) this.f1666b;
        Object obj = a;
        if (t == obj) {
            synchronized (this) {
                t = this.f1666b;
                if (t == obj) {
                    t = this.c.get();
                    this.f1666b = t;
                    this.c = null;
                }
            }
        }
        return t;
    }
}
