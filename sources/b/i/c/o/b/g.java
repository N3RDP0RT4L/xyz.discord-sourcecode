package b.i.c.o.b;

import androidx.annotation.Nullable;
import b.i.c.j.a.a;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
/* compiled from: com.google.firebase:firebase-dynamic-links@@19.1.1 */
/* loaded from: classes3.dex */
public final class g extends k {
    public final TaskCompletionSource<PendingDynamicLinkData> a;
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public final a f1755b;

    public g(a aVar, TaskCompletionSource<PendingDynamicLinkData> taskCompletionSource) {
        this.f1755b = aVar;
        this.a = taskCompletionSource;
    }
}
