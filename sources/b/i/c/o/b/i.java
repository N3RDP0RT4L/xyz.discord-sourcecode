package b.i.c.o.b;

import android.os.RemoteException;
import androidx.annotation.Nullable;
import b.i.a.f.e.h.j.p;
import b.i.c.j.a.a;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
/* compiled from: com.google.firebase:firebase-dynamic-links@@19.1.1 */
/* loaded from: classes3.dex */
public final class i extends p<d, PendingDynamicLinkData> {
    public final String c;
    @Nullable
    public final a d;

    public i(a aVar, String str) {
        this.c = str;
        this.d = aVar;
    }

    @Override // b.i.a.f.e.h.j.p
    public final /* synthetic */ void c(d dVar, TaskCompletionSource<PendingDynamicLinkData> taskCompletionSource) throws RemoteException {
        d dVar2 = dVar;
        try {
            ((j) dVar2.w()).b0(new g(this.d, taskCompletionSource), this.c);
        } catch (RemoteException unused) {
        }
    }
}
