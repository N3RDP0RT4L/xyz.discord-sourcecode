package b.i.c.o.b;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.a.f.e.h.a;
import b.i.a.f.e.h.b;
import b.i.a.f.e.o.f;
import b.i.c.c;
import b.i.c.o.a;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.dynamiclinks.internal.DynamicLinkData;
import java.util.Objects;
/* compiled from: com.google.firebase:firebase-dynamic-links@@19.1.1 */
/* loaded from: classes3.dex */
public final class e extends a {
    public final b<a.d.c> a;
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public final b.i.c.j.a.a f1754b;

    public e(c cVar, @Nullable b.i.c.j.a.a aVar) {
        cVar.a();
        this.a = new c(cVar.d);
        this.f1754b = aVar;
        if (aVar == null) {
            Log.w("FDL", "FDL logging failed. Add a dependency for Firebase Analytics to your app to enable logging of Dynamic Link events.");
        }
    }

    @Override // b.i.c.o.a
    public final Task<PendingDynamicLinkData> a(@NonNull Intent intent) {
        DynamicLinkData dynamicLinkData;
        Task c = this.a.c(new i(this.f1754b, intent.getDataString()));
        Parcelable.Creator<DynamicLinkData> creator = DynamicLinkData.CREATOR;
        byte[] byteArrayExtra = intent.getByteArrayExtra("com.google.firebase.dynamiclinks.DYNAMIC_LINK_DATA");
        PendingDynamicLinkData pendingDynamicLinkData = null;
        if (byteArrayExtra == null) {
            dynamicLinkData = null;
        } else {
            Objects.requireNonNull(creator, "null reference");
            Parcel obtain = Parcel.obtain();
            obtain.unmarshall(byteArrayExtra, 0, byteArrayExtra.length);
            obtain.setDataPosition(0);
            dynamicLinkData = creator.createFromParcel(obtain);
            obtain.recycle();
        }
        DynamicLinkData dynamicLinkData2 = dynamicLinkData;
        if (dynamicLinkData2 != null) {
            pendingDynamicLinkData = new PendingDynamicLinkData(dynamicLinkData2);
        }
        return pendingDynamicLinkData != null ? f.Z(pendingDynamicLinkData) : c;
    }
}
