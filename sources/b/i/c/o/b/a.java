package b.i.c.o.b;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.firebase.dynamiclinks.internal.DynamicLinkData;
/* compiled from: com.google.firebase:firebase-dynamic-links@@19.1.1 */
/* loaded from: classes3.dex */
public final class a implements Parcelable.Creator<DynamicLinkData> {
    @Override // android.os.Parcelable.Creator
    public final DynamicLinkData createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        String str = null;
        long j = 0;
        String str2 = null;
        Bundle bundle = null;
        Uri uri = null;
        int i = 0;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    str = d.R(parcel, readInt);
                    break;
                case 2:
                    str2 = d.R(parcel, readInt);
                    break;
                case 3:
                    i = d.G1(parcel, readInt);
                    break;
                case 4:
                    j = d.H1(parcel, readInt);
                    break;
                case 5:
                    bundle = d.M(parcel, readInt);
                    break;
                case 6:
                    uri = (Uri) d.Q(parcel, readInt, Uri.CREATOR);
                    break;
                default:
                    d.d2(parcel, readInt);
                    break;
            }
        }
        d.f0(parcel, m2);
        return new DynamicLinkData(str, str2, i, j, bundle, uri);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ DynamicLinkData[] newArray(int i) {
        return new DynamicLinkData[i];
    }
}
