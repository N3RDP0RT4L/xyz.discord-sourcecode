package b.i.c.o.b;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import b.i.a.f.h.h.a;
import b.i.a.f.h.h.b;
/* compiled from: com.google.firebase:firebase-dynamic-links@@19.1.1 */
/* loaded from: classes3.dex */
public final class l implements j, IInterface {
    public final IBinder a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1756b = "com.google.firebase.dynamiclinks.internal.IDynamicLinksService";

    public l(IBinder iBinder) {
        this.a = iBinder;
    }

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this.a;
    }

    @Override // b.i.c.o.b.j
    public final void b0(h hVar, String str) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.f1756b);
        int i = b.a;
        obtain.writeStrongBinder((a) hVar);
        obtain.writeString(str);
        Parcel obtain2 = Parcel.obtain();
        try {
            this.a.transact(1, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }
}
