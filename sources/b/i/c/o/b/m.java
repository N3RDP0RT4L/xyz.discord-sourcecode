package b.i.c.o.b;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.firebase.dynamiclinks.internal.zzo;
import com.google.firebase.dynamiclinks.internal.zzr;
import java.util.ArrayList;
/* compiled from: com.google.firebase:firebase-dynamic-links@@19.1.1 */
/* loaded from: classes3.dex */
public final class m implements Parcelable.Creator<zzo> {
    @Override // android.os.Parcelable.Creator
    public final zzo createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        Uri uri = null;
        Uri uri2 = null;
        ArrayList arrayList = null;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                uri = (Uri) d.Q(parcel, readInt, Uri.CREATOR);
            } else if (c == 2) {
                uri2 = (Uri) d.Q(parcel, readInt, Uri.CREATOR);
            } else if (c != 3) {
                d.d2(parcel, readInt);
            } else {
                arrayList = d.V(parcel, readInt, zzr.CREATOR);
            }
        }
        d.f0(parcel, m2);
        return new zzo(uri, uri2, arrayList);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzo[] newArray(int i) {
        return new zzo[i];
    }
}
