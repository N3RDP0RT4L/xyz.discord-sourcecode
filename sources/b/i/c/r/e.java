package b.i.c.r;

import android.content.Context;
import android.content.SharedPreferences;
/* compiled from: HeartBeatInfoStorage.java */
/* loaded from: classes3.dex */
public class e {
    public static e a;

    /* renamed from: b  reason: collision with root package name */
    public final SharedPreferences f1759b;

    public e(Context context) {
        this.f1759b = context.getSharedPreferences("FirebaseAppHeartBeat", 0);
    }

    public synchronized boolean a(String str, long j) {
        if (!this.f1759b.contains(str)) {
            this.f1759b.edit().putLong(str, j).apply();
            return true;
        } else if (j - this.f1759b.getLong(str, -1L) < 86400000) {
            return false;
        } else {
            this.f1759b.edit().putLong(str, j).apply();
            return true;
        }
    }
}
