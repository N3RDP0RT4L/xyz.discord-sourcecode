package b.i.c.r;

import android.content.Context;
import androidx.annotation.NonNull;
import b.i.c.l.r;
import b.i.c.r.d;
import b.i.c.t.a;
/* compiled from: DefaultHeartBeatInfo.java */
/* loaded from: classes3.dex */
public class c implements d {
    public a<e> a;

    public c(final Context context) {
        this.a = new r(new a(context) { // from class: b.i.c.r.a
            public final Context a;

            {
                this.a = context;
            }

            @Override // b.i.c.t.a
            public Object get() {
                e eVar;
                Context context2 = this.a;
                synchronized (e.class) {
                    if (e.a == null) {
                        e.a = new e(context2);
                    }
                    eVar = e.a;
                }
                return eVar;
            }
        });
    }

    @Override // b.i.c.r.d
    @NonNull
    public d.a a(@NonNull String str) {
        boolean a;
        long currentTimeMillis = System.currentTimeMillis();
        boolean a2 = this.a.get().a(str, currentTimeMillis);
        e eVar = this.a.get();
        synchronized (eVar) {
            a = eVar.a("fire-global", currentTimeMillis);
        }
        if (a2 && a) {
            return d.a.COMBINED;
        }
        if (a) {
            return d.a.GLOBAL;
        }
        if (a2) {
            return d.a.SDK;
        }
        return d.a.NONE;
    }
}
