package b.i.c.p;

import androidx.annotation.NonNull;
import java.io.IOException;
/* compiled from: Encoder.java */
/* loaded from: classes3.dex */
public interface b<TValue, TContext> {
    void a(@NonNull TValue tvalue, @NonNull TContext tcontext) throws IOException;
}
