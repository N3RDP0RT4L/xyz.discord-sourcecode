package b.i.c.p.h;

import android.util.Base64;
import android.util.JsonWriter;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.c.p.c;
import b.i.c.p.d;
import b.i.c.p.e;
import com.google.firebase.encoders.EncodingException;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
/* compiled from: JsonValueObjectEncoderContext.java */
/* loaded from: classes3.dex */
public final class f implements d, b.i.c.p.f {
    public boolean a = true;

    /* renamed from: b  reason: collision with root package name */
    public final JsonWriter f1758b;
    public final Map<Class<?>, c<?>> c;
    public final Map<Class<?>, e<?>> d;
    public final c<Object> e;
    public final boolean f;

    public f(@NonNull Writer writer, @NonNull Map<Class<?>, c<?>> map, @NonNull Map<Class<?>, e<?>> map2, c<Object> cVar, boolean z2) {
        this.f1758b = new JsonWriter(writer);
        this.c = map;
        this.d = map2;
        this.e = cVar;
        this.f = z2;
    }

    @Override // b.i.c.p.d
    @NonNull
    public d a(@NonNull String str, boolean z2) throws IOException {
        i();
        this.f1758b.name(str);
        i();
        this.f1758b.value(z2);
        return this;
    }

    @Override // b.i.c.p.d
    @NonNull
    public d b(@NonNull String str, long j) throws IOException {
        i();
        this.f1758b.name(str);
        i();
        this.f1758b.value(j);
        return this;
    }

    @Override // b.i.c.p.d
    @NonNull
    public d c(@NonNull String str, int i) throws IOException {
        i();
        this.f1758b.name(str);
        i();
        this.f1758b.value(i);
        return this;
    }

    @Override // b.i.c.p.f
    @NonNull
    public b.i.c.p.f d(@Nullable String str) throws IOException {
        i();
        this.f1758b.value(str);
        return this;
    }

    @Override // b.i.c.p.f
    @NonNull
    public b.i.c.p.f e(boolean z2) throws IOException {
        i();
        this.f1758b.value(z2);
        return this;
    }

    @NonNull
    public f g(@Nullable Object obj, boolean z2) throws IOException {
        int[] iArr;
        int i = 0;
        if (z2) {
            if (obj == null || obj.getClass().isArray() || (obj instanceof Collection) || (obj instanceof Date) || (obj instanceof Enum) || (obj instanceof Number)) {
                Object[] objArr = new Object[1];
                objArr[0] = obj == null ? null : obj.getClass();
                throw new EncodingException(String.format("%s cannot be encoded inline", objArr));
            }
        }
        if (obj == null) {
            this.f1758b.nullValue();
            return this;
        } else if (obj instanceof Number) {
            this.f1758b.value((Number) obj);
            return this;
        } else if (obj.getClass().isArray()) {
            if (obj instanceof byte[]) {
                i();
                this.f1758b.value(Base64.encodeToString((byte[]) obj, 2));
                return this;
            }
            this.f1758b.beginArray();
            if (obj instanceof int[]) {
                int length = ((int[]) obj).length;
                while (i < length) {
                    this.f1758b.value(iArr[i]);
                    i++;
                }
            } else if (obj instanceof long[]) {
                long[] jArr = (long[]) obj;
                int length2 = jArr.length;
                while (i < length2) {
                    long j = jArr[i];
                    i();
                    this.f1758b.value(j);
                    i++;
                }
            } else if (obj instanceof double[]) {
                double[] dArr = (double[]) obj;
                int length3 = dArr.length;
                while (i < length3) {
                    this.f1758b.value(dArr[i]);
                    i++;
                }
            } else if (obj instanceof boolean[]) {
                boolean[] zArr = (boolean[]) obj;
                int length4 = zArr.length;
                while (i < length4) {
                    this.f1758b.value(zArr[i]);
                    i++;
                }
            } else if (obj instanceof Number[]) {
                for (Number number : (Number[]) obj) {
                    g(number, false);
                }
            } else {
                for (Object obj2 : (Object[]) obj) {
                    g(obj2, false);
                }
            }
            this.f1758b.endArray();
            return this;
        } else if (obj instanceof Collection) {
            this.f1758b.beginArray();
            for (Object obj3 : (Collection) obj) {
                g(obj3, false);
            }
            this.f1758b.endArray();
            return this;
        } else if (obj instanceof Map) {
            this.f1758b.beginObject();
            for (Map.Entry entry : ((Map) obj).entrySet()) {
                Object key = entry.getKey();
                try {
                    f((String) key, entry.getValue());
                } catch (ClassCastException e) {
                    throw new EncodingException(String.format("Only String keys are currently supported in maps, got %s of type %s instead.", key, key.getClass()), e);
                }
            }
            this.f1758b.endObject();
            return this;
        } else {
            c<?> cVar = this.c.get(obj.getClass());
            if (cVar != null) {
                if (!z2) {
                    this.f1758b.beginObject();
                }
                cVar.a(obj, this);
                if (!z2) {
                    this.f1758b.endObject();
                }
                return this;
            }
            e<?> eVar = this.d.get(obj.getClass());
            if (eVar != null) {
                eVar.a(obj, this);
                return this;
            } else if (obj instanceof Enum) {
                String name = ((Enum) obj).name();
                i();
                this.f1758b.value(name);
                return this;
            } else {
                c<Object> cVar2 = this.e;
                if (!z2) {
                    this.f1758b.beginObject();
                }
                cVar2.a(obj, this);
                if (!z2) {
                    this.f1758b.endObject();
                }
                return this;
            }
        }
    }

    @NonNull
    /* renamed from: h */
    public f f(@NonNull String str, @Nullable Object obj) throws IOException {
        if (!this.f) {
            i();
            this.f1758b.name(str);
            if (obj != null) {
                return g(obj, false);
            }
            this.f1758b.nullValue();
            return this;
        } else if (obj == null) {
            return this;
        } else {
            i();
            this.f1758b.name(str);
            return g(obj, false);
        }
    }

    public final void i() throws IOException {
        if (!this.a) {
            throw new IllegalStateException("Parent context used since this context was created. Cannot use this context anymore.");
        }
    }
}
