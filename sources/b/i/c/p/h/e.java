package b.i.c.p.h;

import androidx.annotation.NonNull;
import b.i.c.p.c;
import b.i.c.p.f;
import b.i.c.p.g.b;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
/* compiled from: JsonDataEncoderBuilder.java */
/* loaded from: classes3.dex */
public final class e implements b<e> {
    public static final a a = new a(null);

    /* renamed from: b  reason: collision with root package name */
    public final Map<Class<?>, c<?>> f1757b;
    public final Map<Class<?>, b.i.c.p.e<?>> c;
    public c<Object> d = b.i.c.p.h.a.a;
    public boolean e = false;

    /* compiled from: JsonDataEncoderBuilder.java */
    /* loaded from: classes3.dex */
    public static final class a implements b.i.c.p.e<Date> {
        public static final DateFormat a;

        static {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
            a = simpleDateFormat;
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        }

        public a(d dVar) {
        }

        @Override // b.i.c.p.b
        public void a(@NonNull Object obj, @NonNull f fVar) throws IOException {
            fVar.d(a.format((Date) obj));
        }
    }

    public e() {
        HashMap hashMap = new HashMap();
        this.f1757b = hashMap;
        HashMap hashMap2 = new HashMap();
        this.c = hashMap2;
        hashMap2.put(String.class, b.a);
        hashMap.remove(String.class);
        hashMap2.put(Boolean.class, c.a);
        hashMap.remove(Boolean.class);
        hashMap2.put(Date.class, a);
        hashMap.remove(Date.class);
    }
}
