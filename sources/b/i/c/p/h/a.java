package b.i.c.p.h;

import b.i.c.p.c;
import b.i.c.p.d;
import b.i.c.p.h.e;
import com.google.firebase.encoders.EncodingException;
/* compiled from: JsonDataEncoderBuilder.java */
/* loaded from: classes3.dex */
public final /* synthetic */ class a implements c {
    public static final a a = new a();

    @Override // b.i.c.p.b
    public void a(Object obj, d dVar) {
        e.a aVar = e.a;
        StringBuilder R = b.d.b.a.a.R("Couldn't find encoder for type ");
        R.append(obj.getClass().getCanonicalName());
        throw new EncodingException(R.toString());
    }
}
