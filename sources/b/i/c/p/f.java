package b.i.c.p;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.io.IOException;
/* compiled from: ValueEncoderContext.java */
/* loaded from: classes3.dex */
public interface f {
    @NonNull
    f d(@Nullable String str) throws IOException;

    @NonNull
    f e(boolean z2) throws IOException;
}
