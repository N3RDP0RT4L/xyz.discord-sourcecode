package b.i.c.p;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.io.IOException;
/* compiled from: ObjectEncoderContext.java */
/* loaded from: classes3.dex */
public interface d {
    @NonNull
    d a(@NonNull String str, boolean z2) throws IOException;

    @NonNull
    d b(@NonNull String str, long j) throws IOException;

    @NonNull
    d c(@NonNull String str, int i) throws IOException;

    @NonNull
    d f(@NonNull String str, @Nullable Object obj) throws IOException;
}
