package b.i.c;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.util.Base64;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.collection.ArrayMap;
import androidx.core.os.UserManagerCompat;
import b.i.a.f.e.h.j.c;
import b.i.a.f.e.k.j;
import b.i.a.f.e.o.g;
import b.i.c.l.k;
import b.i.c.l.q;
import b.i.c.l.r;
import b.i.c.q.a;
import b.i.c.q.b;
import com.discord.models.domain.ModelAuditLogEntry;
import com.google.android.material.badge.BadgeDrawable;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
/* compiled from: FirebaseApp.java */
/* loaded from: classes3.dex */
public class c {
    public static final Object a = new Object();

    /* renamed from: b  reason: collision with root package name */
    public static final Executor f1649b = new d(null);
    public static final Map<String, c> c = new ArrayMap();
    public final Context d;
    public final String e;
    public final i f;
    public final k g;
    public final r<b.i.c.v.a> j;
    public final AtomicBoolean h = new AtomicBoolean(false);
    public final AtomicBoolean i = new AtomicBoolean();
    public final List<b> k = new CopyOnWriteArrayList();

    /* compiled from: FirebaseApp.java */
    /* loaded from: classes3.dex */
    public interface b {
        void a(boolean z2);
    }

    /* compiled from: FirebaseApp.java */
    @TargetApi(14)
    /* renamed from: b.i.c.c$c  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static class C0134c implements c.a {
        public static AtomicReference<C0134c> a = new AtomicReference<>();

        @Override // b.i.a.f.e.h.j.c.a
        public void a(boolean z2) {
            Object obj = c.a;
            synchronized (c.a) {
                Iterator it = new ArrayList(c.c.values()).iterator();
                while (it.hasNext()) {
                    c cVar = (c) it.next();
                    if (cVar.h.get()) {
                        Log.d("FirebaseApp", "Notifying background state change listeners.");
                        for (b bVar : cVar.k) {
                            bVar.a(z2);
                        }
                    }
                }
            }
        }
    }

    /* compiled from: FirebaseApp.java */
    /* loaded from: classes3.dex */
    public static class d implements Executor {
        public static final Handler j = new Handler(Looper.getMainLooper());

        public d(a aVar) {
        }

        @Override // java.util.concurrent.Executor
        public void execute(@NonNull Runnable runnable) {
            j.post(runnable);
        }
    }

    /* compiled from: FirebaseApp.java */
    @TargetApi(24)
    /* loaded from: classes3.dex */
    public static class e extends BroadcastReceiver {
        public static AtomicReference<e> a = new AtomicReference<>();

        /* renamed from: b  reason: collision with root package name */
        public final Context f1650b;

        public e(Context context) {
            this.f1650b = context;
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            Object obj = c.a;
            synchronized (c.a) {
                for (c cVar : c.c.values()) {
                    cVar.d();
                }
            }
            this.f1650b.unregisterReceiver(this);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:14:0x006b  */
    /* JADX WARN: Removed duplicated region for block: B:15:0x0075  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x00c0  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public c(final android.content.Context r12, java.lang.String r13, b.i.c.i r14) {
        /*
            Method dump skipped, instructions count: 374
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.c.c.<init>(android.content.Context, java.lang.String, b.i.c.i):void");
    }

    @NonNull
    public static c b() {
        c cVar;
        synchronized (a) {
            cVar = c.get("[DEFAULT]");
            if (cVar == null) {
                throw new IllegalStateException("Default FirebaseApp is not initialized in this process " + g.a() + ". Make sure to call FirebaseApp.initializeApp(Context) first.");
            }
        }
        return cVar;
    }

    @Nullable
    public static c e(@NonNull Context context) {
        synchronized (a) {
            if (c.containsKey("[DEFAULT]")) {
                return b();
            }
            i a2 = i.a(context);
            if (a2 == null) {
                Log.w("FirebaseApp", "Default FirebaseApp failed to initialize because no default options were found. This usually means that com.google.gms:google-services was not applied to your gradle project.");
                return null;
            }
            return f(context, a2);
        }
    }

    @NonNull
    public static c f(@NonNull Context context, @NonNull i iVar) {
        c cVar;
        AtomicReference<C0134c> atomicReference = C0134c.a;
        if (context.getApplicationContext() instanceof Application) {
            Application application = (Application) context.getApplicationContext();
            if (C0134c.a.get() == null) {
                C0134c cVar2 = new C0134c();
                if (C0134c.a.compareAndSet(null, cVar2)) {
                    b.i.a.f.e.h.j.c.a(application);
                    b.i.a.f.e.h.j.c cVar3 = b.i.a.f.e.h.j.c.j;
                    Objects.requireNonNull(cVar3);
                    synchronized (cVar3) {
                        cVar3.m.add(cVar2);
                    }
                }
            }
        }
        if (context.getApplicationContext() != null) {
            context = context.getApplicationContext();
        }
        synchronized (a) {
            Map<String, c> map = c;
            b.c.a.a0.d.G(!map.containsKey("[DEFAULT]"), "FirebaseApp name [DEFAULT] already exists!");
            b.c.a.a0.d.z(context, "Application context cannot be null.");
            cVar = new c(context, "[DEFAULT]", iVar);
            map.put("[DEFAULT]", cVar);
        }
        cVar.d();
        return cVar;
    }

    public final void a() {
        b.c.a.a0.d.G(!this.i.get(), "FirebaseApp was deleted");
    }

    public String c() {
        StringBuilder sb = new StringBuilder();
        a();
        byte[] bytes = this.e.getBytes(Charset.defaultCharset());
        String str = null;
        sb.append(bytes == null ? null : Base64.encodeToString(bytes, 11));
        sb.append(BadgeDrawable.DEFAULT_EXCEED_MAX_BADGE_NUMBER_SUFFIX);
        a();
        byte[] bytes2 = this.f.f1651b.getBytes(Charset.defaultCharset());
        if (bytes2 != null) {
            str = Base64.encodeToString(bytes2, 11);
        }
        sb.append(str);
        return sb.toString();
    }

    public final void d() {
        Queue<b.i.c.q.a<?>> queue;
        Set<Map.Entry<b.i.c.q.b<Object>, Executor>> emptySet;
        if (!UserManagerCompat.isUserUnlocked(this.d)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Device in Direct Boot Mode: postponing initialization of Firebase APIs for app ");
            a();
            sb.append(this.e);
            Log.i("FirebaseApp", sb.toString());
            Context context = this.d;
            if (e.a.get() == null) {
                e eVar = new e(context);
                if (e.a.compareAndSet(null, eVar)) {
                    context.registerReceiver(eVar, new IntentFilter("android.intent.action.USER_UNLOCKED"));
                    return;
                }
                return;
            }
            return;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Device unlocked: initializing all Firebase APIs for app ");
        a();
        sb2.append(this.e);
        Log.i("FirebaseApp", sb2.toString());
        k kVar = this.g;
        boolean h = h();
        for (Map.Entry<b.i.c.l.d<?>, r<?>> entry : kVar.f1661b.entrySet()) {
            r<?> value = entry.getValue();
            int i = entry.getKey().c;
            boolean z2 = false;
            if (!(i == 1)) {
                if (i == 2) {
                    z2 = true;
                }
                if (z2 && h) {
                }
            }
            value.get();
        }
        q qVar = kVar.e;
        synchronized (qVar) {
            queue = qVar.f1665b;
            if (queue != null) {
                qVar.f1665b = null;
            } else {
                queue = null;
            }
        }
        if (queue != null) {
            for (final b.i.c.q.a<?> aVar : queue) {
                Objects.requireNonNull(aVar);
                synchronized (qVar) {
                    Queue<b.i.c.q.a<?>> queue2 = qVar.f1665b;
                    if (queue2 != null) {
                        queue2.add(aVar);
                    } else {
                        synchronized (qVar) {
                            ConcurrentHashMap<b.i.c.q.b<Object>, Executor> concurrentHashMap = qVar.a.get(null);
                            emptySet = concurrentHashMap == null ? Collections.emptySet() : concurrentHashMap.entrySet();
                        }
                        for (final Map.Entry<b.i.c.q.b<Object>, Executor> entry2 : emptySet) {
                            entry2.getValue().execute(new Runnable(entry2, aVar) { // from class: b.i.c.l.p
                                public final Map.Entry j;
                                public final a k;

                                {
                                    this.j = entry2;
                                    this.k = aVar;
                                }

                                @Override // java.lang.Runnable
                                public void run() {
                                    Map.Entry entry3 = this.j;
                                    ((b) entry3.getKey()).a(this.k);
                                }
                            });
                        }
                    }
                }
            }
        }
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof c)) {
            return false;
        }
        String str = this.e;
        c cVar = (c) obj;
        cVar.a();
        return str.equals(cVar.e);
    }

    public boolean g() {
        boolean z2;
        a();
        b.i.c.v.a aVar = this.j.get();
        synchronized (aVar) {
            z2 = aVar.d;
        }
        return z2;
    }

    @VisibleForTesting
    public boolean h() {
        a();
        return "[DEFAULT]".equals(this.e);
    }

    public int hashCode() {
        return this.e.hashCode();
    }

    public String toString() {
        j jVar = new j(this);
        jVar.a(ModelAuditLogEntry.CHANGE_KEY_NAME, this.e);
        jVar.a("options", this.f);
        return jVar.toString();
    }
}
