package b.i.c.m;

import android.os.Bundle;
import androidx.annotation.Nullable;
import b.i.c.j.a.a;
import b.i.c.m.d.i.b;
import com.discord.models.domain.ModelAuditLogEntry;
/* compiled from: CrashlyticsAnalyticsListener.java */
/* loaded from: classes3.dex */
public class a implements a.b {
    public b a;

    /* renamed from: b  reason: collision with root package name */
    public b f1668b;

    public void a(int i, @Nullable Bundle bundle) {
        b bVar;
        b.i.c.m.d.b bVar2 = b.i.c.m.d.b.a;
        bVar2.b("Received Analytics message: " + i + " " + bundle);
        String string = bundle.getString(ModelAuditLogEntry.CHANGE_KEY_NAME);
        if (string != null) {
            Bundle bundle2 = bundle.getBundle("params");
            if (bundle2 == null) {
                bundle2 = new Bundle();
            }
            if ("clx".equals(bundle2.getString("_o"))) {
                bVar = this.a;
            } else {
                bVar = this.f1668b;
            }
            if (bVar != null) {
                bVar.b(string, bundle2);
            }
        }
    }
}
