package b.i.c.m.e;

import java.io.File;
/* compiled from: SessionFiles.java */
/* loaded from: classes3.dex */
public final class g {
    public final File a;

    /* renamed from: b  reason: collision with root package name */
    public final File f1752b;
    public final File c;
    public final File d;
    public final File e;
    public final File f;

    /* compiled from: SessionFiles.java */
    /* loaded from: classes3.dex */
    public static final class b {
        public File a;

        /* renamed from: b  reason: collision with root package name */
        public File f1753b;
        public File c;
        public File d;
        public File e;
        public File f;
    }

    public g(b bVar, a aVar) {
        this.a = bVar.a;
        this.f1752b = bVar.f1753b;
        this.c = bVar.c;
        this.d = bVar.d;
        this.e = bVar.e;
        this.f = bVar.f;
    }
}
