package b.i.c.m.e;

import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.NonNull;
import b.i.c.m.d.a;
import b.i.c.m.d.b;
import b.i.c.m.d.d;
import com.google.firebase.crashlytics.ndk.JniNativeApi;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;
import org.json.JSONObject;
/* compiled from: FirebaseCrashlyticsNdk.java */
/* loaded from: classes3.dex */
public class c implements a {
    public final e a;

    public c(@NonNull e eVar) {
        this.a = eVar;
    }

    @Override // b.i.c.m.d.a
    public boolean a(@NonNull String str) {
        f fVar = ((a) this.a).d;
        Objects.requireNonNull(fVar);
        f.b(new File(fVar.a, str));
        return true;
    }

    @Override // b.i.c.m.d.a
    @NonNull
    public d b(@NonNull String str) {
        return new h(((a) this.a).a(str));
    }

    @Override // b.i.c.m.d.a
    public void c(@NonNull String str, int i, @NonNull String str2, int i2, long j, long j2, boolean z2, int i3, @NonNull String str3, @NonNull String str4) {
        a aVar = (a) this.a;
        Objects.requireNonNull(aVar);
        HashMap hashMap = new HashMap();
        hashMap.put("arch", Integer.valueOf(i));
        hashMap.put("build_model", str2);
        hashMap.put("available_processors", Integer.valueOf(i2));
        hashMap.put("total_ram", Long.valueOf(j));
        hashMap.put("disk_space", Long.valueOf(j2));
        hashMap.put("is_emulator", Boolean.valueOf(z2));
        hashMap.put("state", Integer.valueOf(i3));
        hashMap.put("build_manufacturer", str3);
        hashMap.put("build_product", str4);
        aVar.c(str, new JSONObject(hashMap).toString(), "device.json");
    }

    @Override // b.i.c.m.d.a
    public void d(@NonNull String str, @NonNull String str2, long j) {
        a aVar = (a) this.a;
        Objects.requireNonNull(aVar);
        HashMap hashMap = new HashMap();
        hashMap.put("session_id", str);
        hashMap.put("generator", str2);
        hashMap.put("started_at_seconds", Long.valueOf(j));
        aVar.c(str, new JSONObject(hashMap).toString(), "session.json");
    }

    @Override // b.i.c.m.d.a
    public boolean e(@NonNull String str) {
        File file;
        a aVar = (a) this.a;
        f fVar = aVar.d;
        Objects.requireNonNull(fVar);
        return new File(fVar.a, str).exists() && (file = aVar.a(str).a) != null && file.exists();
    }

    @Override // b.i.c.m.d.a
    public void f(@NonNull String str, @NonNull String str2, @NonNull String str3, @NonNull String str4, @NonNull String str5, int i, @NonNull String str6) {
        a aVar = (a) this.a;
        Objects.requireNonNull(aVar);
        if (TextUtils.isEmpty(str6)) {
            str6 = "";
        }
        HashMap hashMap = new HashMap();
        hashMap.put("app_identifier", str2);
        hashMap.put("version_code", str3);
        hashMap.put("version_name", str4);
        hashMap.put("install_uuid", str5);
        hashMap.put("delivery_mechanism", Integer.valueOf(i));
        hashMap.put("unity_version", str6);
        aVar.c(str, new JSONObject(hashMap).toString(), "app.json");
    }

    @Override // b.i.c.m.d.a
    public void g(@NonNull String str, @NonNull String str2, @NonNull String str3, boolean z2) {
        a aVar = (a) this.a;
        Objects.requireNonNull(aVar);
        HashMap hashMap = new HashMap();
        hashMap.put("version", str2);
        hashMap.put("build_version", str3);
        hashMap.put("is_rooted", Boolean.valueOf(z2));
        aVar.c(str, new JSONObject(hashMap).toString(), "os.json");
    }

    @Override // b.i.c.m.d.a
    public boolean h(String str) {
        a aVar = (a) this.a;
        File a = aVar.d.a(str);
        boolean z2 = false;
        if (a != null) {
            try {
                z2 = ((JniNativeApi) aVar.c).a(a.getCanonicalPath(), aVar.f1751b.getAssets());
            } catch (IOException e) {
                if (b.a.a(6)) {
                    Log.e("FirebaseCrashlytics", "Error initializing CrashlyticsNdk", e);
                }
            }
        }
        b bVar = b.a;
        StringBuilder R = b.d.b.a.a.R("Crashlytics NDK initialization ");
        R.append(z2 ? "successful" : "FAILED");
        bVar.f(R.toString());
        return z2;
    }
}
