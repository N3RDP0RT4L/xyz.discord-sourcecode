package b.i.c.m.e;

import b.i.c.m.d.d;
import java.io.File;
/* compiled from: SessionFilesProvider.java */
/* loaded from: classes3.dex */
public class h implements d {
    public final g a;

    public h(g gVar) {
        this.a = gVar;
    }

    @Override // b.i.c.m.d.d
    public File a() {
        return this.a.d;
    }

    @Override // b.i.c.m.d.d
    public File b() {
        return this.a.f;
    }

    @Override // b.i.c.m.d.d
    public File c() {
        return this.a.e;
    }

    @Override // b.i.c.m.d.d
    public File d() {
        return this.a.a;
    }

    @Override // b.i.c.m.d.d
    public File e() {
        return this.a.c;
    }

    @Override // b.i.c.m.d.d
    public File f() {
        return this.a.f1752b;
    }
}
