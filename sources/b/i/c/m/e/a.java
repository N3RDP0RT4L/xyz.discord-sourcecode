package b.i.c.m.e;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.c.m.d.b;
import b.i.c.m.d.k.h;
import b.i.c.m.e.g;
import com.adjust.sdk.Constants;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
/* compiled from: BreakpadController.java */
/* loaded from: classes3.dex */
public class a implements e {
    public static final Charset a = Charset.forName(Constants.ENCODING);

    /* renamed from: b  reason: collision with root package name */
    public final Context f1751b;
    public final d c;
    public final f d;

    public a(Context context, d dVar, f fVar) {
        this.f1751b = context;
        this.c = dVar;
        this.d = fVar;
    }

    @Nullable
    public static File b(File file, String str) {
        File[] listFiles = file.listFiles();
        if (listFiles == null) {
            return null;
        }
        for (File file2 : listFiles) {
            if (file2.getName().endsWith(str)) {
                return file2;
            }
        }
        return null;
    }

    @NonNull
    public g a(String str) {
        File a2 = this.d.a(str);
        File file = new File(a2, "pending");
        b bVar = b.a;
        StringBuilder R = b.d.b.a.a.R("Minidump directory: ");
        R.append(file.getAbsolutePath());
        bVar.b(R.toString());
        File b2 = b(file, ".dmp");
        StringBuilder R2 = b.d.b.a.a.R("Minidump ");
        R2.append((b2 == null || !b2.exists()) ? "does not exist" : "exists");
        bVar.b(R2.toString());
        g.b bVar2 = new g.b();
        if (a2 != null && a2.exists() && file.exists()) {
            bVar2.a = b(file, ".dmp");
            bVar2.f1753b = b(a2, ".device_info");
            bVar2.c = new File(a2, "session.json");
            bVar2.d = new File(a2, "app.json");
            bVar2.e = new File(a2, "device.json");
            bVar2.f = new File(a2, "os.json");
        }
        return new g(bVar2, null);
    }

    public final void c(String str, String str2, String str3) {
        Throwable th;
        File file = new File(this.d.a(str), str3);
        BufferedWriter bufferedWriter = null;
        try {
            BufferedWriter bufferedWriter2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), a));
            try {
                bufferedWriter2.write(str2);
                h.c(bufferedWriter2, "Failed to close " + file);
            } catch (IOException unused) {
                bufferedWriter = bufferedWriter2;
                h.c(bufferedWriter, "Failed to close " + file);
            } catch (Throwable th2) {
                th = th2;
                bufferedWriter = bufferedWriter2;
                h.c(bufferedWriter, "Failed to close " + file);
                throw th;
            }
        } catch (IOException unused2) {
        } catch (Throwable th3) {
            th = th3;
        }
    }
}
