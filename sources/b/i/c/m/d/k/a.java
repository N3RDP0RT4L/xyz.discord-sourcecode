package b.i.c.m.d.k;

import b.i.c.m.d.n.b;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;
/* compiled from: AbstractSpiCall.java */
/* loaded from: classes3.dex */
public abstract class a {
    public static final Pattern a = Pattern.compile("http(s?)://[^\\/]+", 2);

    /* renamed from: b  reason: collision with root package name */
    public final String f1672b;
    public final b c;
    public final int d;
    public final String e;

    public a(String str, String str2, b bVar, int i) {
        if (str2 == null) {
            throw new IllegalArgumentException("url must not be null.");
        } else if (bVar != null) {
            this.e = str;
            this.f1672b = !h.s(str) ? a.matcher(str2).replaceFirst(str) : str2;
            this.c = bVar;
            this.d = i;
        } else {
            throw new IllegalArgumentException("requestFactory must not be null.");
        }
    }

    public b.i.c.m.d.n.a b() {
        return c(Collections.emptyMap());
    }

    public b.i.c.m.d.n.a c(Map<String, String> map) {
        b bVar = this.c;
        int i = this.d;
        String str = this.f1672b;
        Objects.requireNonNull(bVar);
        b.i.c.m.d.n.a aVar = new b.i.c.m.d.n.a(i, str, map);
        aVar.e.put("User-Agent", "Crashlytics Android SDK/17.3.0");
        aVar.e.put("X-CRASHLYTICS-DEVELOPER-TOKEN", "470fa2b4ae81cd56ecbcda9735803434cec591fa");
        return aVar;
    }
}
