package b.i.c.m.d.k;

import androidx.annotation.NonNull;
import b.i.c.m.d.b;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
/* compiled from: UserMetadata.java */
/* loaded from: classes3.dex */
public class f1 {
    public String a = null;

    /* renamed from: b  reason: collision with root package name */
    public final ConcurrentHashMap<String, String> f1679b = new ConcurrentHashMap<>();

    public static String b(String str) {
        if (str == null) {
            return str;
        }
        String trim = str.trim();
        return trim.length() > 1024 ? trim.substring(0, 1024) : trim;
    }

    @NonNull
    public Map<String, String> a() {
        return Collections.unmodifiableMap(this.f1679b);
    }

    public void c(String str, String str2) {
        if (str != null) {
            String b2 = b(str);
            if (this.f1679b.size() < 64 || this.f1679b.containsKey(b2)) {
                this.f1679b.put(b2, str2 == null ? "" : b(str2));
            } else {
                b.a.b("Exceeded maximum number of custom attributes (64)");
            }
        } else {
            throw new IllegalArgumentException("Custom attribute key must not be null.");
        }
    }
}
