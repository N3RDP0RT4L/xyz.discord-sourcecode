package b.i.c.m.d.k;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
/* compiled from: FileBackedNativeSessionFile.java */
/* loaded from: classes3.dex */
public class u0 implements a1 {
    @NonNull
    public final File a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final String f1692b;
    @NonNull
    public final String c;

    public u0(@NonNull String str, @NonNull String str2, @NonNull File file) {
        this.f1692b = str;
        this.c = str2;
        this.a = file;
    }

    @Override // b.i.c.m.d.k.a1
    @NonNull
    public String a() {
        return this.c;
    }

    /* JADX WARN: Removed duplicated region for block: B:20:0x0051  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0075 A[ORIG_RETURN, RETURN] */
    @Override // b.i.c.m.d.k.a1
    @androidx.annotation.Nullable
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.i.c.m.d.m.v.c.a b() {
        /*
            r7 = this;
            r0 = 8192(0x2000, float:1.14794E-41)
            byte[] r0 = new byte[r0]
            r1 = 0
            java.io.InputStream r2 = r7.h()     // Catch: java.io.IOException -> L4e
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch: java.lang.Throwable -> L47
            r3.<init>()     // Catch: java.lang.Throwable -> L47
            java.util.zip.GZIPOutputStream r4 = new java.util.zip.GZIPOutputStream     // Catch: java.lang.Throwable -> L42
            r4.<init>(r3)     // Catch: java.lang.Throwable -> L42
            if (r2 != 0) goto L21
            r4.close()     // Catch: java.lang.Throwable -> L42
            r3.close()     // Catch: java.lang.Throwable -> L47
            if (r2 == 0) goto L4e
            r2.close()     // Catch: java.io.IOException -> L4e
            goto L4e
        L21:
            int r5 = r2.read(r0)     // Catch: java.lang.Throwable -> L3d
            if (r5 <= 0) goto L2c
            r6 = 0
            r4.write(r0, r6, r5)     // Catch: java.lang.Throwable -> L3d
            goto L21
        L2c:
            r4.finish()     // Catch: java.lang.Throwable -> L3d
            byte[] r0 = r3.toByteArray()     // Catch: java.lang.Throwable -> L3d
            r4.close()     // Catch: java.lang.Throwable -> L42
            r3.close()     // Catch: java.lang.Throwable -> L47
            r2.close()     // Catch: java.io.IOException -> L4e
            goto L4f
        L3d:
            r0 = move-exception
            r4.close()     // Catch: java.lang.Throwable -> L41
        L41:
            throw r0     // Catch: java.lang.Throwable -> L42
        L42:
            r0 = move-exception
            r3.close()     // Catch: java.lang.Throwable -> L46
        L46:
            throw r0     // Catch: java.lang.Throwable -> L47
        L47:
            r0 = move-exception
            if (r2 == 0) goto L4d
            r2.close()     // Catch: java.lang.Throwable -> L4d
        L4d:
            throw r0     // Catch: java.io.IOException -> L4e
        L4e:
            r0 = r1
        L4f:
            if (r0 == 0) goto L75
            java.lang.String r2 = "Null contents"
            java.util.Objects.requireNonNull(r0, r2)
            java.lang.String r2 = r7.f1692b
            java.lang.String r3 = "Null filename"
            java.util.Objects.requireNonNull(r2, r3)
            r3 = 1
            java.lang.String r4 = ""
            if (r3 == 0) goto L69
            b.i.c.m.d.m.e r3 = new b.i.c.m.d.m.e
            r3.<init>(r2, r0, r1)
            r1 = r3
            goto L75
        L69:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Missing required properties:"
            java.lang.String r1 = b.d.b.a.a.v(r1, r4)
            r0.<init>(r1)
            throw r0
        L75:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.c.m.d.k.u0.b():b.i.c.m.d.m.v$c$a");
    }

    @Override // b.i.c.m.d.k.a1
    @Nullable
    public InputStream h() {
        if (this.a.exists() && this.a.isFile()) {
            try {
                return new FileInputStream(this.a);
            } catch (FileNotFoundException unused) {
            }
        }
        return null;
    }
}
