package b.i.c.m.d.k;

import android.os.Build;
import b.i.c.m.d.k.x;
import b.i.c.m.d.p.a;
import b.i.c.m.d.p.c;
import b.i.c.m.d.p.d;
/* compiled from: CrashlyticsController.java */
/* loaded from: classes3.dex */
public class y implements x.g {
    public final /* synthetic */ int a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ int f1701b;
    public final /* synthetic */ long c;
    public final /* synthetic */ long d;
    public final /* synthetic */ boolean e;
    public final /* synthetic */ int f;

    public y(x xVar, int i, String str, int i2, long j, long j2, boolean z2, int i3, String str2, String str3) {
        this.a = i;
        this.f1701b = i2;
        this.c = j;
        this.d = j2;
        this.e = z2;
        this.f = i3;
    }

    @Override // b.i.c.m.d.k.x.g
    public void a(c cVar) throws Exception {
        int i = this.a;
        String str = Build.MODEL;
        int i2 = this.f1701b;
        long j = this.c;
        long j2 = this.d;
        boolean z2 = this.e;
        int i3 = this.f;
        String str2 = Build.MANUFACTURER;
        String str3 = Build.PRODUCT;
        a j3 = d.j(str);
        a j4 = d.j(str3);
        a j5 = d.j(str2);
        cVar.r(9, 2);
        int c = c.c(3, i) + 0;
        int b2 = j3 == null ? 0 : c.b(4, j3);
        int f = c.f(5, i2);
        int h = c.h(6, j);
        int h2 = c.h(7, j2);
        cVar.o(c.f(12, i3) + c.a(10, z2) + h2 + h + f + c + b2 + (j5 == null ? 0 : c.b(13, j5)) + (j4 == null ? 0 : c.b(14, j4)));
        cVar.m(3, i);
        cVar.l(4, j3);
        cVar.s(5, i2);
        cVar.t(6, j);
        cVar.t(7, j2);
        cVar.k(10, z2);
        cVar.s(12, i3);
        if (j5 != null) {
            cVar.l(13, j5);
        }
        if (j4 != null) {
            cVar.l(14, j4);
        }
    }
}
