package b.i.c.m.d.k;

import b.i.c.m.d.b;
import b.i.c.m.d.k.x;
import java.io.File;
import java.util.HashSet;
import java.util.Objects;
/* compiled from: CrashlyticsController.java */
/* loaded from: classes3.dex */
public class s implements Runnable {
    public final /* synthetic */ x j;

    public s(x xVar) {
        this.j = xVar;
    }

    @Override // java.lang.Runnable
    public void run() {
        File[] r;
        x xVar = this.j;
        File[] r2 = x.r(xVar.l(), new x.i());
        Objects.requireNonNull(xVar);
        HashSet hashSet = new HashSet();
        for (File file : r2) {
            b.a.b("Found invalid session part file: " + file);
            hashSet.add(x.o(file));
        }
        if (!hashSet.isEmpty()) {
            for (File file2 : x.r(xVar.l(), new t(xVar, hashSet))) {
                b.a.b("Deleting invalid session file: " + file2);
                file2.delete();
            }
        }
    }
}
