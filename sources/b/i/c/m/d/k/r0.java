package b.i.c.m.d.k;
/* compiled from: DeliveryMechanism.java */
/* loaded from: classes3.dex */
public enum r0 {
    DEVELOPER(1),
    USER_SIDELOAD(2),
    TEST_DISTRIBUTION(3),
    APP_STORE(4);
    

    /* renamed from: id  reason: collision with root package name */
    private final int f1690id;

    r0(int i) {
        this.f1690id = i;
    }

    public static r0 f(String str) {
        return str != null ? APP_STORE : DEVELOPER;
    }

    public int g() {
        return this.f1690id;
    }

    @Override // java.lang.Enum
    public String toString() {
        return Integer.toString(this.f1690id);
    }
}
