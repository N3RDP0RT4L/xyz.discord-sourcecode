package b.i.c.m.d.k;

import b.i.a.f.e.o.f;
import b.i.c.m.d.b;
import b.i.c.m.d.k.x;
import b.i.c.m.d.q.a;
import b.i.c.m.d.q.c.c;
import b.i.c.m.d.q.c.d;
import com.google.android.gms.tasks.Task;
import java.io.File;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
/* compiled from: CrashlyticsController.java */
/* loaded from: classes3.dex */
public class g0 implements Callable<Task<Void>> {
    public final /* synthetic */ Boolean j;
    public final /* synthetic */ x.e k;

    public g0(x.e eVar, Boolean bool) {
        this.k = eVar;
        this.j = bool;
    }

    @Override // java.util.concurrent.Callable
    public Task<Void> call() throws Exception {
        a aVar = x.this.u;
        Objects.requireNonNull(aVar);
        b.a.b("Checking for crash reports...");
        File[] q = x.this.q();
        File[] listFiles = x.this.m().listFiles();
        if (listFiles == null) {
            listFiles = new File[0];
        }
        LinkedList<c> linkedList = new LinkedList();
        if (q != null) {
            for (File file : q) {
                b bVar = b.a;
                StringBuilder R = b.d.b.a.a.R("Found crash report ");
                R.append(file.getPath());
                bVar.b(R.toString());
                linkedList.add(new d(file, Collections.emptyMap()));
            }
        }
        for (File file2 : listFiles) {
            linkedList.add(new b.i.c.m.d.q.c.b(file2));
        }
        if (linkedList.isEmpty()) {
            b.a.b("No reports found.");
        }
        if (!this.j.booleanValue()) {
            b.a.b("Reports are being deleted.");
            for (File file3 : x.r(x.this.l(), m.a)) {
                file3.delete();
            }
            Objects.requireNonNull(x.this.u);
            for (c cVar : linkedList) {
                cVar.remove();
            }
            x.this.A.f1676b.b();
            x.this.E.b(null);
            return f.Z(null);
        }
        b.a.b("Reports are being sent.");
        boolean booleanValue = this.j.booleanValue();
        q0 q0Var = x.this.j;
        Objects.requireNonNull(q0Var);
        if (booleanValue) {
            q0Var.h.b(null);
            x.e eVar = this.k;
            Executor executor = x.this.m.a;
            return eVar.a.r(executor, new f0(this, linkedList, booleanValue, executor));
        }
        throw new IllegalStateException("An invalid data collection token was used.");
    }
}
