package b.i.c.m.d.k;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.c.m.d.m.v;
import java.io.InputStream;
/* compiled from: NativeSessionFile.java */
/* loaded from: classes3.dex */
public interface a1 {
    @NonNull
    String a();

    @Nullable
    v.c.a b();

    @Nullable
    InputStream h();
}
