package b.i.c.m.d.k;

import androidx.annotation.NonNull;
import b.d.b.a.a;
import com.adjust.sdk.Constants;
import java.io.File;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;
/* compiled from: MetaDataStore.java */
/* loaded from: classes3.dex */
public class z0 {
    public static final Charset a = Charset.forName(Constants.ENCODING);

    /* renamed from: b  reason: collision with root package name */
    public final File f1702b;

    public z0(File file) {
        this.f1702b = file;
    }

    public static f1 c(String str) throws JSONException {
        JSONObject jSONObject = new JSONObject(str);
        f1 f1Var = new f1();
        String str2 = null;
        if (!jSONObject.isNull("userId")) {
            str2 = jSONObject.optString("userId", null);
        }
        f1Var.a = f1.b(str2);
        return f1Var;
    }

    @NonNull
    public File a(String str) {
        return new File(this.f1702b, a.w(str, "keys", ".meta"));
    }

    @NonNull
    public File b(String str) {
        return new File(this.f1702b, a.w(str, "user", ".meta"));
    }
}
