package b.i.c.m.d.k;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.annotation.Nullable;
import b.i.a.f.n.c0;
import b.i.c.c;
import b.i.c.m.d.b;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.io.FilenameFilter;
/* compiled from: DataCollectionArbiter.java */
/* loaded from: classes3.dex */
public class q0 {
    public final SharedPreferences a;

    /* renamed from: b  reason: collision with root package name */
    public final c f1689b;
    public final Object c;
    public boolean e;
    public boolean f;
    @Nullable
    public Boolean g;
    public TaskCompletionSource<Void> d = new TaskCompletionSource<>();
    public TaskCompletionSource<Void> h = new TaskCompletionSource<>();

    public q0(c cVar) {
        Boolean bool;
        Object obj = new Object();
        this.c = obj;
        this.e = false;
        this.f = false;
        cVar.a();
        Context context = cVar.d;
        this.f1689b = cVar;
        SharedPreferences o = h.o(context);
        this.a = o;
        if (o.contains("firebase_crashlytics_collection_enabled")) {
            this.f = false;
            bool = Boolean.valueOf(o.getBoolean("firebase_crashlytics_collection_enabled", true));
        } else {
            bool = null;
        }
        this.g = bool == null ? a(context) : bool;
        synchronized (obj) {
            if (b()) {
                this.d.b(null);
                this.e = true;
            }
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:18:0x003e  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x0042  */
    @androidx.annotation.Nullable
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.lang.Boolean a(android.content.Context r5) {
        /*
            r4 = this;
            java.lang.String r0 = "firebase_crashlytics_collection_enabled"
            r1 = 0
            android.content.pm.PackageManager r2 = r5.getPackageManager()     // Catch: android.content.pm.PackageManager.NameNotFoundException -> L2a
            if (r2 == 0) goto L3b
            java.lang.String r5 = r5.getPackageName()     // Catch: android.content.pm.PackageManager.NameNotFoundException -> L2a
            r3 = 128(0x80, float:1.794E-43)
            android.content.pm.ApplicationInfo r5 = r2.getApplicationInfo(r5, r3)     // Catch: android.content.pm.PackageManager.NameNotFoundException -> L2a
            if (r5 == 0) goto L3b
            android.os.Bundle r2 = r5.metaData     // Catch: android.content.pm.PackageManager.NameNotFoundException -> L2a
            if (r2 == 0) goto L3b
            boolean r2 = r2.containsKey(r0)     // Catch: android.content.pm.PackageManager.NameNotFoundException -> L2a
            if (r2 == 0) goto L3b
            android.os.Bundle r5 = r5.metaData     // Catch: android.content.pm.PackageManager.NameNotFoundException -> L2a
            boolean r5 = r5.getBoolean(r0)     // Catch: android.content.pm.PackageManager.NameNotFoundException -> L2a
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch: android.content.pm.PackageManager.NameNotFoundException -> L2a
            goto L3c
        L2a:
            r5 = move-exception
            b.i.c.m.d.b r0 = b.i.c.m.d.b.a
            r2 = 3
            boolean r0 = r0.a(r2)
            if (r0 == 0) goto L3b
            java.lang.String r0 = "FirebaseCrashlytics"
            java.lang.String r2 = "Unable to get PackageManager. Falling through"
            android.util.Log.d(r0, r2, r5)
        L3b:
            r5 = r1
        L3c:
            if (r5 != 0) goto L42
            r5 = 0
            r4.f = r5
            return r1
        L42:
            r0 = 1
            r4.f = r0
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
            boolean r5 = r0.equals(r5)
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.c.m.d.k.q0.a(android.content.Context):java.lang.Boolean");
    }

    public synchronized boolean b() {
        boolean z2;
        String str;
        Boolean bool = this.g;
        if (bool != null) {
            z2 = bool.booleanValue();
        } else {
            z2 = this.f1689b.g();
        }
        String str2 = z2 ? "ENABLED" : "DISABLED";
        if (this.g == null) {
            str = "global Firebase setting";
        } else {
            str = this.f ? "firebase_crashlytics_collection_enabled manifest flag" : "API";
        }
        b.a.b(String.format("Crashlytics automatic data collection %s by %s.", str2, str));
        return z2;
    }

    public Task<Void> c() {
        c0<Void> c0Var;
        c0<Void> c0Var2 = this.h.a;
        synchronized (this.c) {
            c0Var = this.d.a;
        }
        FilenameFilter filenameFilter = h1.a;
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        i1 i1Var = new i1(taskCompletionSource);
        c0Var2.h(i1Var);
        c0Var.h(i1Var);
        return taskCompletionSource.a;
    }
}
