package b.i.c.m.d.k;

import androidx.annotation.NonNull;
import b.i.a.f.n.a;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
/* compiled from: Utils.java */
/* loaded from: classes3.dex */
public class i1 implements a<T, Void> {
    public final /* synthetic */ TaskCompletionSource a;

    public i1(TaskCompletionSource taskCompletionSource) {
        this.a = taskCompletionSource;
    }

    @Override // b.i.a.f.n.a
    public Void a(@NonNull Task task) throws Exception {
        if (task.p()) {
            this.a.b(task.l());
            return null;
        }
        this.a.a(task.k());
        return null;
    }
}
