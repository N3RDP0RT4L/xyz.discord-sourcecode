package b.i.c.m.d.k;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.a.f.n.f;
import b.i.c.m.d.k.x;
import b.i.c.m.d.q.c.d;
import b.i.c.m.d.s.h.b;
import com.google.android.gms.tasks.Task;
import java.io.File;
import java.util.Arrays;
import java.util.concurrent.Executor;
/* compiled from: CrashlyticsController.java */
/* loaded from: classes3.dex */
public class c0 implements f<b, Void> {
    public final /* synthetic */ Executor a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ d0 f1675b;

    public c0(d0 d0Var, Executor executor) {
        this.f1675b = d0Var;
        this.a = executor;
    }

    @Override // b.i.a.f.n.f
    @NonNull
    public Task<Void> a(@Nullable b bVar) throws Exception {
        File[] q;
        b bVar2 = bVar;
        if (bVar2 == null) {
            b.i.c.m.d.b.a.g("Received null app settings, cannot send reports at crash time.");
            return b.i.a.f.e.o.f.Z(null);
        }
        x xVar = this.f1675b.n;
        Context context = xVar.i;
        b.i.c.m.d.q.b a = ((h0) xVar.r).a(bVar2);
        for (File file : xVar.q()) {
            x.c(bVar2.e, file);
            d dVar = new d(file, x.f);
            i iVar = xVar.m;
            iVar.b(new j(iVar, new x.m(context, dVar, a, true)));
        }
        return b.i.a.f.e.o.f.B1(Arrays.asList(x.b(this.f1675b.n), this.f1675b.n.A.b(this.a, b.c.a.y.b.m(bVar2))));
    }
}
