package b.i.c.m.d.k;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Debug;
import android.os.StatFs;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import androidx.appcompat.widget.ActivityChooserModel;
import androidx.constraintlayout.solver.widgets.analyzer.BasicMeasure;
import b.i.c.m.d.p.c;
import com.adjust.sdk.Constants;
import java.io.Closeable;
import java.io.File;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
/* compiled from: CommonUtils.java */
/* loaded from: classes3.dex */
public class h {
    public static final char[] a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    /* renamed from: b  reason: collision with root package name */
    public static long f1681b = -1;
    public static final Comparator<File> c = new a();

    /* compiled from: CommonUtils.java */
    /* loaded from: classes3.dex */
    public class a implements Comparator<File> {
        @Override // java.util.Comparator
        public int compare(File file, File file2) {
            return (int) (file.lastModified() - file2.lastModified());
        }
    }

    /* compiled from: CommonUtils.java */
    /* loaded from: classes3.dex */
    public enum b {
        X86_32,
        X86_64,
        ARM_UNKNOWN,
        PPC,
        PPC64,
        ARMV6,
        ARMV7,
        UNKNOWN,
        ARMV7S,
        ARM64;
        
        public static final Map<String, b> t;

        static {
            b bVar = X86_32;
            b bVar2 = ARMV6;
            b bVar3 = ARMV7;
            b bVar4 = ARM64;
            HashMap hashMap = new HashMap(4);
            t = hashMap;
            hashMap.put("armeabi-v7a", bVar3);
            hashMap.put("armeabi", bVar2);
            hashMap.put("arm64-v8a", bVar4);
            hashMap.put("x86", bVar);
        }
    }

    public static long a(String str) {
        StatFs statFs;
        long blockSize = new StatFs(str).getBlockSize();
        return (statFs.getBlockCount() * blockSize) - (blockSize * statFs.getAvailableBlocks());
    }

    @SuppressLint({"MissingPermission"})
    public static boolean b(Context context) {
        if (!(context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == 0)) {
            return true;
        }
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public static void c(Closeable closeable, String str) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                if (b.i.c.m.d.b.a.a(6)) {
                    Log.e("FirebaseCrashlytics", str, e);
                }
            }
        }
    }

    public static void d(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception unused) {
            }
        }
    }

    public static long e(String str, String str2, int i) {
        return Long.parseLong(str.split(str2)[0].trim()) * i;
    }

    public static String f(String... strArr) {
        if (strArr.length != 0) {
            ArrayList arrayList = new ArrayList();
            for (String str : strArr) {
                if (str != null) {
                    arrayList.add(str.replace("-", "").toLowerCase(Locale.US));
                }
            }
            Collections.sort(arrayList);
            StringBuilder sb = new StringBuilder();
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                sb.append((String) it.next());
            }
            String sb2 = sb.toString();
            if (sb2.length() > 0) {
                return v(sb2);
            }
        }
        return null;
    }

    /* JADX WARN: Code restructure failed: missing block: B:12:0x0035, code lost:
        r2 = r3[1];
     */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v0, types: [boolean] */
    /* JADX WARN: Type inference failed for: r1v1 */
    /* JADX WARN: Type inference failed for: r1v2, types: [java.io.Closeable] */
    /* JADX WARN: Type inference failed for: r1v3 */
    /* JADX WARN: Type inference failed for: r1v4 */
    /* JADX WARN: Type inference failed for: r1v5, types: [java.io.BufferedReader] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static java.lang.String g(java.io.File r6, java.lang.String r7) {
        /*
            java.lang.String r0 = "Failed to close system file reader."
            boolean r1 = r6.exists()
            r2 = 0
            if (r1 == 0) goto L5d
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch: java.lang.Throwable -> L39 java.lang.Exception -> L3b
            java.io.FileReader r3 = new java.io.FileReader     // Catch: java.lang.Throwable -> L39 java.lang.Exception -> L3b
            r3.<init>(r6)     // Catch: java.lang.Throwable -> L39 java.lang.Exception -> L3b
            r4 = 1024(0x400, float:1.435E-42)
            r1.<init>(r3, r4)     // Catch: java.lang.Throwable -> L39 java.lang.Exception -> L3b
        L15:
            java.lang.String r3 = r1.readLine()     // Catch: java.lang.Exception -> L37 java.lang.Throwable -> L57
            if (r3 == 0) goto L53
            java.lang.String r4 = "\\s*:\\s*"
            java.util.regex.Pattern r4 = java.util.regex.Pattern.compile(r4)     // Catch: java.lang.Exception -> L37 java.lang.Throwable -> L57
            r5 = 2
            java.lang.String[] r3 = r4.split(r3, r5)     // Catch: java.lang.Exception -> L37 java.lang.Throwable -> L57
            int r4 = r3.length     // Catch: java.lang.Exception -> L37 java.lang.Throwable -> L57
            r5 = 1
            if (r4 <= r5) goto L15
            r4 = 0
            r4 = r3[r4]     // Catch: java.lang.Exception -> L37 java.lang.Throwable -> L57
            boolean r4 = r4.equals(r7)     // Catch: java.lang.Exception -> L37 java.lang.Throwable -> L57
            if (r4 == 0) goto L15
            r6 = r3[r5]     // Catch: java.lang.Exception -> L37 java.lang.Throwable -> L57
            r2 = r6
            goto L53
        L37:
            r7 = move-exception
            goto L3d
        L39:
            r6 = move-exception
            goto L59
        L3b:
            r7 = move-exception
            r1 = r2
        L3d:
            b.i.c.m.d.b r3 = b.i.c.m.d.b.a     // Catch: java.lang.Throwable -> L57
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch: java.lang.Throwable -> L57
            r4.<init>()     // Catch: java.lang.Throwable -> L57
            java.lang.String r5 = "Error parsing "
            r4.append(r5)     // Catch: java.lang.Throwable -> L57
            r4.append(r6)     // Catch: java.lang.Throwable -> L57
            java.lang.String r6 = r4.toString()     // Catch: java.lang.Throwable -> L57
            r3.e(r6, r7)     // Catch: java.lang.Throwable -> L57
        L53:
            c(r1, r0)
            goto L5d
        L57:
            r6 = move-exception
            r2 = r1
        L59:
            c(r2, r0)
            throw r6
        L5d:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.c.m.d.k.h.g(java.io.File, java.lang.String):java.lang.String");
    }

    public static void h(Flushable flushable, String str) {
        if (flushable != null) {
            try {
                ((c) flushable).flush();
            } catch (IOException e) {
                if (b.i.c.m.d.b.a.a(6)) {
                    Log.e("FirebaseCrashlytics", str, e);
                }
            }
        }
    }

    public static ActivityManager.RunningAppProcessInfo i(String str, Context context) {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService(ActivityChooserModel.ATTRIBUTE_ACTIVITY)).getRunningAppProcesses();
        if (runningAppProcesses != null) {
            for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
                if (runningAppProcessInfo.processName.equals(str)) {
                    return runningAppProcessInfo;
                }
            }
        }
        return null;
    }

    public static boolean j(Context context, String str, boolean z2) {
        Resources resources;
        if (!(context == null || (resources = context.getResources()) == null)) {
            int n = n(context, str, "bool");
            if (n > 0) {
                return resources.getBoolean(n);
            }
            int n2 = n(context, str, "string");
            if (n2 > 0) {
                return Boolean.parseBoolean(context.getString(n2));
            }
        }
        return z2;
    }

    public static int k(Context context) {
        boolean z2 = true;
        int i = r(context) ? 1 : 0;
        if (t(context)) {
            i |= 2;
        }
        if (!Debug.isDebuggerConnected() && !Debug.waitingForDebugger()) {
            z2 = false;
        }
        return z2 ? i | 4 : i;
    }

    public static String l(Context context) {
        int n = n(context, "com.google.firebase.crashlytics.mapping_file_id", "string");
        if (n == 0) {
            n = n(context, "com.crashlytics.android.build_id", "string");
        }
        if (n != 0) {
            return context.getResources().getString(n);
        }
        return null;
    }

    public static boolean m(Context context) {
        return !r(context) && ((SensorManager) context.getSystemService("sensor")).getDefaultSensor(8) != null;
    }

    public static int n(Context context, String str, String str2) {
        String packageName;
        Resources resources = context.getResources();
        int i = context.getApplicationContext().getApplicationInfo().icon;
        if (i > 0) {
            try {
                packageName = context.getResources().getResourcePackageName(i);
                if ("android".equals(packageName)) {
                    packageName = context.getPackageName();
                }
            } catch (Resources.NotFoundException unused) {
                packageName = context.getPackageName();
            }
        } else {
            packageName = context.getPackageName();
        }
        return resources.getIdentifier(str, str2, packageName);
    }

    public static SharedPreferences o(Context context) {
        return context.getSharedPreferences("com.google.firebase.crashlytics", 0);
    }

    public static synchronized long p() {
        long j;
        synchronized (h.class) {
            if (f1681b == -1) {
                long j2 = 0;
                String g = g(new File("/proc/meminfo"), "MemTotal");
                if (!TextUtils.isEmpty(g)) {
                    String upperCase = g.toUpperCase(Locale.US);
                    try {
                        if (upperCase.endsWith("KB")) {
                            j2 = e(upperCase, "KB", 1024);
                        } else if (upperCase.endsWith("MB")) {
                            j2 = e(upperCase, "MB", 1048576);
                        } else if (upperCase.endsWith("GB")) {
                            j2 = e(upperCase, "GB", BasicMeasure.EXACTLY);
                        } else {
                            b.i.c.m.d.b bVar = b.i.c.m.d.b.a;
                            bVar.b("Unexpected meminfo format while computing RAM: " + upperCase);
                        }
                    } catch (NumberFormatException e) {
                        b.i.c.m.d.b bVar2 = b.i.c.m.d.b.a;
                        bVar2.e("Unexpected meminfo format while computing RAM: " + upperCase, e);
                    }
                }
                f1681b = j2;
            }
            j = f1681b;
        }
        return j;
    }

    public static String q(byte[] bArr) {
        char[] cArr = new char[bArr.length * 2];
        for (int i = 0; i < bArr.length; i++) {
            int i2 = bArr[i] & 255;
            int i3 = i * 2;
            char[] cArr2 = a;
            cArr[i3] = cArr2[i2 >>> 4];
            cArr[i3 + 1] = cArr2[i2 & 15];
        }
        return new String(cArr);
    }

    public static boolean r(Context context) {
        String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
        String str = Build.PRODUCT;
        return "sdk".equals(str) || "google_sdk".equals(str) || string == null;
    }

    public static boolean s(String str) {
        return str == null || str.length() == 0;
    }

    public static boolean t(Context context) {
        boolean r = r(context);
        String str = Build.TAGS;
        if ((!r && str != null && str.contains("test-keys")) || new File("/system/app/Superuser.apk").exists()) {
            return true;
        }
        return !r && new File("/system/xbin/su").exists();
    }

    public static String u(int i) {
        if (i >= 0) {
            return String.format(Locale.US, "%1$10s", Integer.valueOf(i)).replace(' ', '0');
        }
        throw new IllegalArgumentException("value must be zero or greater");
    }

    public static String v(String str) {
        byte[] bytes = str.getBytes();
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(Constants.SHA1);
            messageDigest.update(bytes);
            return q(messageDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            b.i.c.m.d.b bVar = b.i.c.m.d.b.a;
            bVar.e("Could not create hashing algorithm: " + Constants.SHA1 + ", returning empty string.", e);
            return "";
        }
    }

    public static String w(InputStream inputStream) throws IOException {
        Scanner useDelimiter = new Scanner(inputStream).useDelimiter("\\A");
        return useDelimiter.hasNext() ? useDelimiter.next() : "";
    }
}
