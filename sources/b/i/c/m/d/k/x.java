package b.i.c.m.d.k;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.a.f.n.c0;
import b.i.c.m.d.k.h;
import b.i.c.m.d.l.b;
import b.i.c.m.d.m.b;
import b.i.c.m.d.m.f;
import b.i.c.m.d.m.i;
import b.i.c.m.d.m.t;
import b.i.c.m.d.m.v;
import b.i.c.m.d.p.b;
import b.i.c.m.d.q.b;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;
/* compiled from: CrashlyticsController.java */
/* loaded from: classes3.dex */
public class x {
    public static final FilenameFilter a = new a("BeginSession");

    /* renamed from: b  reason: collision with root package name */
    public static final FilenameFilter f1695b = new b();
    public static final Comparator<File> c = new c();
    public static final Comparator<File> d = new d();
    public static final Pattern e = Pattern.compile("([\\d|A-Z|a-z]{12}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{12}).+");
    public static final Map<String, String> f = Collections.singletonMap("X-CRASHLYTICS-SEND-FLAGS", "1");
    public static final String[] g = {"SessionUser", "SessionApp", "SessionOS", "SessionDevice"};
    public final d1 A;
    public p0 B;
    public final Context i;
    public final q0 j;
    public final m0 k;
    public final f1 l;
    public final b.i.c.m.d.k.i m;
    public final b.i.c.m.d.n.b n;
    public final v0 o;
    public final b.i.c.m.d.o.h p;
    public final b.i.c.m.d.k.b q;

    /* renamed from: s  reason: collision with root package name */
    public final j f1696s;
    public final b.i.c.m.d.l.b t;
    public final b.i.c.m.d.a w;

    /* renamed from: x  reason: collision with root package name */
    public final b.i.c.m.d.t.d f1697x;

    /* renamed from: y  reason: collision with root package name */
    public final String f1698y;

    /* renamed from: z  reason: collision with root package name */
    public final b.i.c.m.d.i.a f1699z;
    public final AtomicInteger h = new AtomicInteger(0);
    public TaskCompletionSource<Boolean> C = new TaskCompletionSource<>();
    public TaskCompletionSource<Boolean> D = new TaskCompletionSource<>();
    public TaskCompletionSource<Void> E = new TaskCompletionSource<>();
    public AtomicBoolean F = new AtomicBoolean(false);
    public final b.AbstractC0149b r = new h0(this);
    public final b.i.c.m.d.q.a u = new b.i.c.m.d.q.a(new k(null));
    public final b.a v = new l(null);

    /* compiled from: CrashlyticsController.java */
    /* loaded from: classes3.dex */
    public class a extends h {
        public a(String str) {
            super(str);
        }

        @Override // b.i.c.m.d.k.x.h, java.io.FilenameFilter
        public boolean accept(File file, String str) {
            return super.accept(file, str) && str.endsWith(".cls");
        }
    }

    /* compiled from: CrashlyticsController.java */
    /* loaded from: classes3.dex */
    public class b implements FilenameFilter {
        @Override // java.io.FilenameFilter
        public boolean accept(File file, String str) {
            return str.length() == 39 && str.endsWith(".cls");
        }
    }

    /* compiled from: CrashlyticsController.java */
    /* loaded from: classes3.dex */
    public class c implements Comparator<File> {
        @Override // java.util.Comparator
        public int compare(File file, File file2) {
            return file2.getName().compareTo(file.getName());
        }
    }

    /* compiled from: CrashlyticsController.java */
    /* loaded from: classes3.dex */
    public class d implements Comparator<File> {
        @Override // java.util.Comparator
        public int compare(File file, File file2) {
            return file.getName().compareTo(file2.getName());
        }
    }

    /* compiled from: CrashlyticsController.java */
    /* loaded from: classes3.dex */
    public class e implements b.i.a.f.n.f<Boolean, Void> {
        public final /* synthetic */ Task a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ float f1700b;

        public e(Task task, float f) {
            this.a = task;
            this.f1700b = f;
        }

        @Override // b.i.a.f.n.f
        @NonNull
        public Task<Void> a(@Nullable Boolean bool) throws Exception {
            return x.this.m.c(new g0(this, bool));
        }
    }

    /* compiled from: CrashlyticsController.java */
    /* loaded from: classes3.dex */
    public static class f implements FilenameFilter {
        public f(a aVar) {
        }

        @Override // java.io.FilenameFilter
        public boolean accept(File file, String str) {
            return !((b) x.f1695b).accept(file, str) && x.e.matcher(str).matches();
        }
    }

    /* compiled from: CrashlyticsController.java */
    /* loaded from: classes3.dex */
    public interface g {
        void a(b.i.c.m.d.p.c cVar) throws Exception;
    }

    /* compiled from: CrashlyticsController.java */
    /* loaded from: classes3.dex */
    public static class h implements FilenameFilter {
        public final String a;

        public h(String str) {
            this.a = str;
        }

        @Override // java.io.FilenameFilter
        public boolean accept(File file, String str) {
            return str.contains(this.a) && !str.endsWith(".cls_temp");
        }
    }

    /* compiled from: CrashlyticsController.java */
    /* loaded from: classes3.dex */
    public static class i implements FilenameFilter {
        @Override // java.io.FilenameFilter
        public boolean accept(File file, String str) {
            return ((b.a) b.i.c.m.d.p.b.j).accept(file, str) || str.contains("SessionMissingBinaryImages");
        }
    }

    /* compiled from: CrashlyticsController.java */
    /* loaded from: classes3.dex */
    public static final class j implements b.AbstractC0137b {
        public final b.i.c.m.d.o.h a;

        public j(b.i.c.m.d.o.h hVar) {
            this.a = hVar;
        }

        public File a() {
            File file = new File(this.a.a(), "log-files");
            if (!file.exists()) {
                file.mkdirs();
            }
            return file;
        }
    }

    /* compiled from: CrashlyticsController.java */
    /* loaded from: classes3.dex */
    public final class k implements b.c {
        public k(a aVar) {
        }
    }

    /* compiled from: CrashlyticsController.java */
    /* loaded from: classes3.dex */
    public final class l implements b.a {
        public l(a aVar) {
        }
    }

    /* compiled from: CrashlyticsController.java */
    /* loaded from: classes3.dex */
    public static final class m implements Runnable {
        public final Context j;
        public final b.i.c.m.d.q.c.c k;
        public final b.i.c.m.d.q.b l;
        public final boolean m;

        public m(Context context, b.i.c.m.d.q.c.c cVar, b.i.c.m.d.q.b bVar, boolean z2) {
            this.j = context;
            this.k = cVar;
            this.l = bVar;
            this.m = z2;
        }

        @Override // java.lang.Runnable
        public void run() {
            if (b.i.c.m.d.k.h.b(this.j)) {
                b.i.c.m.d.b.a.b("Attempting to send crash report at time of crash...");
                this.l.a(this.k, this.m);
            }
        }
    }

    /* compiled from: CrashlyticsController.java */
    /* loaded from: classes3.dex */
    public static class n implements FilenameFilter {
        public final String a;

        public n(String str) {
            this.a = str;
        }

        @Override // java.io.FilenameFilter
        public boolean accept(File file, String str) {
            StringBuilder sb = new StringBuilder();
            sb.append(this.a);
            sb.append(".cls");
            return !str.equals(sb.toString()) && str.contains(this.a) && !str.endsWith(".cls_temp");
        }
    }

    public x(Context context, b.i.c.m.d.k.i iVar, b.i.c.m.d.n.b bVar, v0 v0Var, q0 q0Var, b.i.c.m.d.o.h hVar, m0 m0Var, b.i.c.m.d.k.b bVar2, b.i.c.m.d.q.a aVar, b.AbstractC0149b bVar3, b.i.c.m.d.a aVar2, b.i.c.m.d.i.a aVar3, b.i.c.m.d.s.d dVar) {
        this.i = context;
        this.m = iVar;
        this.n = bVar;
        this.o = v0Var;
        this.j = q0Var;
        this.p = hVar;
        this.k = m0Var;
        this.q = bVar2;
        this.w = aVar2;
        this.f1698y = bVar2.g.a();
        this.f1699z = aVar3;
        f1 f1Var = new f1();
        this.l = f1Var;
        j jVar = new j(hVar);
        this.f1696s = jVar;
        b.i.c.m.d.l.b bVar4 = new b.i.c.m.d.l.b(context, jVar);
        this.t = bVar4;
        b.i.c.m.d.t.a aVar4 = new b.i.c.m.d.t.a(1024, new b.i.c.m.d.t.c(10));
        this.f1697x = aVar4;
        File file = new File(new File(hVar.a.getFilesDir(), ".com.google.firebase.crashlytics").getPath());
        n0 n0Var = new n0(context, v0Var, bVar2, aVar4);
        b.i.c.m.d.o.g gVar = new b.i.c.m.d.o.g(file, dVar);
        b.i.c.m.d.m.x.h hVar2 = b.i.c.m.d.r.c.a;
        b.i.a.b.j.n.b(context);
        b.i.a.b.g c2 = b.i.a.b.j.n.a().c(new b.i.a.b.i.a(b.i.c.m.d.r.c.f1742b, b.i.c.m.d.r.c.c));
        b.i.a.b.b bVar5 = new b.i.a.b.b("json");
        b.i.a.b.e<v, byte[]> eVar = b.i.c.m.d.r.c.d;
        this.A = new d1(n0Var, gVar, new b.i.c.m.d.r.c(((b.i.a.b.j.j) c2).a("FIREBASE_CRASHLYTICS_REPORT", v.class, bVar5, eVar), eVar), bVar4, f1Var);
    }

    public static void A(b.i.c.m.d.p.c cVar, File file) throws IOException {
        Throwable th;
        if (!file.exists()) {
            b.i.c.m.d.b bVar = b.i.c.m.d.b.a;
            StringBuilder R = b.d.b.a.a.R("Tried to include a file that doesn't exist: ");
            R.append(file.getName());
            bVar.d(R.toString());
            return;
        }
        FileInputStream fileInputStream = null;
        try {
            FileInputStream fileInputStream2 = new FileInputStream(file);
            try {
                e(fileInputStream2, cVar, (int) file.length());
                b.i.c.m.d.k.h.c(fileInputStream2, "Failed to close file input stream.");
            } catch (Throwable th2) {
                th = th2;
                fileInputStream = fileInputStream2;
                b.i.c.m.d.k.h.c(fileInputStream, "Failed to close file input stream.");
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
        }
    }

    public static void a(x xVar) throws Exception {
        String str;
        Integer num;
        Objects.requireNonNull(xVar);
        long j2 = j();
        new b.i.c.m.d.k.g(xVar.o);
        String str2 = b.i.c.m.d.k.g.f1680b;
        b.i.c.m.d.b bVar = b.i.c.m.d.b.a;
        b.d.b.a.a.m0("Opening a new session with ID ", str2, bVar);
        xVar.w.h(str2);
        Locale locale = Locale.US;
        String format = String.format(locale, "Crashlytics Android SDK/%s", "17.3.0");
        xVar.z(str2, "BeginSession", new u(xVar, str2, format, j2));
        xVar.w.d(str2, format, j2);
        v0 v0Var = xVar.o;
        String str3 = v0Var.e;
        b.i.c.m.d.k.b bVar2 = xVar.q;
        String str4 = bVar2.e;
        String str5 = bVar2.f;
        String b2 = v0Var.b();
        int g2 = r0.f(xVar.q.c).g();
        xVar.z(str2, "SessionApp", new v(xVar, str3, str4, str5, b2, g2));
        xVar.w.f(str2, str3, str4, str5, b2, g2, xVar.f1698y);
        String str6 = Build.VERSION.RELEASE;
        String str7 = Build.VERSION.CODENAME;
        boolean t = b.i.c.m.d.k.h.t(xVar.i);
        xVar.z(str2, "SessionOS", new w(xVar, str6, str7, t));
        xVar.w.g(str2, str6, str7, t);
        Context context = xVar.i;
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        h.b bVar3 = h.b.UNKNOWN;
        String str8 = Build.CPU_ABI;
        if (TextUtils.isEmpty(str8)) {
            bVar.b("Architecture#getValue()::Build.CPU_ABI returned null or empty");
        } else {
            h.b bVar4 = h.b.t.get(str8.toLowerCase(locale));
            if (bVar4 != null) {
                bVar3 = bVar4;
            }
        }
        int ordinal = bVar3.ordinal();
        String str9 = Build.MODEL;
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        long p = b.i.c.m.d.k.h.p();
        long blockCount = statFs.getBlockCount() * statFs.getBlockSize();
        boolean r = b.i.c.m.d.k.h.r(context);
        int k2 = b.i.c.m.d.k.h.k(context);
        String str10 = Build.MANUFACTURER;
        String str11 = Build.PRODUCT;
        xVar.z(str2, "SessionDevice", new y(xVar, ordinal, str9, availableProcessors, p, blockCount, r, k2, str10, str11));
        xVar.w.c(str2, ordinal, str9, availableProcessors, p, blockCount, r, k2, str10, str11);
        xVar.t.a(str2);
        d1 d1Var = xVar.A;
        String t2 = t(str2);
        n0 n0Var = d1Var.a;
        Objects.requireNonNull(n0Var);
        Charset charset = v.a;
        b.C0140b bVar5 = new b.C0140b();
        bVar5.a = "17.3.0";
        String str12 = n0Var.e.a;
        Objects.requireNonNull(str12, "Null gmpAppId");
        bVar5.f1710b = str12;
        String b3 = n0Var.d.b();
        Objects.requireNonNull(b3, "Null installationUuid");
        bVar5.d = b3;
        String str13 = n0Var.e.e;
        Objects.requireNonNull(str13, "Null buildVersion");
        bVar5.e = str13;
        String str14 = n0Var.e.f;
        Objects.requireNonNull(str14, "Null displayVersion");
        bVar5.f = str14;
        bVar5.c = 4;
        f.b bVar6 = new f.b();
        bVar6.b(false);
        bVar6.c = Long.valueOf(j2);
        Objects.requireNonNull(t2, "Null identifier");
        bVar6.f1715b = t2;
        String str15 = n0.a;
        Objects.requireNonNull(str15, "Null generator");
        bVar6.a = str15;
        String str16 = n0Var.d.e;
        Objects.requireNonNull(str16, "Null identifier");
        String str17 = n0Var.e.e;
        Objects.requireNonNull(str17, "Null version");
        String str18 = n0Var.e.f;
        String b4 = n0Var.d.b();
        String a2 = n0Var.e.g.a();
        String str19 = null;
        if (a2 != null) {
            str19 = "Unity";
            str = a2;
        } else {
            str = null;
        }
        bVar6.f = new b.i.c.m.d.m.g(str16, str17, str18, null, b4, str19, str, null);
        Integer num2 = 3;
        Objects.requireNonNull(str6, "Null version");
        Objects.requireNonNull(str7, "Null buildVersion");
        Boolean valueOf = Boolean.valueOf(b.i.c.m.d.k.h.t(n0Var.c));
        String str20 = num2 == null ? " platform" : "";
        if (valueOf == null) {
            str20 = b.d.b.a.a.v(str20, " jailbroken");
        }
        if (str20.isEmpty()) {
            bVar6.h = new t(num2.intValue(), str6, str7, valueOf.booleanValue(), null);
            StatFs statFs2 = new StatFs(Environment.getDataDirectory().getPath());
            int i2 = 7;
            if (!TextUtils.isEmpty(str8) && (num = n0.f1687b.get(str8.toLowerCase(locale))) != null) {
                i2 = num.intValue();
            }
            int availableProcessors2 = Runtime.getRuntime().availableProcessors();
            long p2 = b.i.c.m.d.k.h.p();
            long blockCount2 = statFs2.getBlockCount() * statFs2.getBlockSize();
            boolean r2 = b.i.c.m.d.k.h.r(n0Var.c);
            int k3 = b.i.c.m.d.k.h.k(n0Var.c);
            i.b bVar7 = new i.b();
            bVar7.a = Integer.valueOf(i2);
            Objects.requireNonNull(str9, "Null model");
            bVar7.f1718b = str9;
            bVar7.c = Integer.valueOf(availableProcessors2);
            bVar7.d = Long.valueOf(p2);
            bVar7.e = Long.valueOf(blockCount2);
            bVar7.f = Boolean.valueOf(r2);
            bVar7.g = Integer.valueOf(k3);
            Objects.requireNonNull(str10, "Null manufacturer");
            bVar7.h = str10;
            Objects.requireNonNull(str11, "Null modelClass");
            bVar7.i = str11;
            bVar6.i = bVar7.a();
            bVar6.k = 3;
            bVar5.g = bVar6.a();
            v a3 = bVar5.a();
            b.i.c.m.d.o.g gVar = d1Var.f1676b;
            Objects.requireNonNull(gVar);
            v.d h2 = a3.h();
            if (h2 == null) {
                bVar.b("Could not get session for report");
                return;
            }
            String g3 = h2.g();
            try {
                File h3 = gVar.h(g3);
                b.i.c.m.d.o.g.i(h3);
                b.i.c.m.d.o.g.l(new File(h3, "report"), b.i.c.m.d.o.g.c.g(a3));
            } catch (IOException e2) {
                b.i.c.m.d.b.a.c("Could not persist report for session " + g3, e2);
            }
        } else {
            throw new IllegalStateException(b.d.b.a.a.v("Missing required properties:", str20));
        }
    }

    public static Task b(x xVar) {
        File[] r;
        boolean z2;
        Task task;
        Objects.requireNonNull(xVar);
        ArrayList arrayList = new ArrayList();
        for (File file : r(xVar.l(), b.i.c.m.d.k.m.a)) {
            try {
                long parseLong = Long.parseLong(file.getName().substring(3));
                try {
                    Class.forName("com.google.firebase.crash.FirebaseCrash");
                    z2 = true;
                } catch (ClassNotFoundException unused) {
                    z2 = false;
                }
                if (z2) {
                    b.i.c.m.d.b.a.b("Skipping logging Crashlytics event to Firebase, FirebaseCrash exists");
                    task = b.i.a.f.e.o.f.Z(null);
                } else {
                    task = b.i.a.f.e.o.f.o(new ScheduledThreadPoolExecutor(1), new a0(xVar, parseLong));
                }
                arrayList.add(task);
            } catch (NumberFormatException unused2) {
                b.i.c.m.d.b bVar = b.i.c.m.d.b.a;
                StringBuilder R = b.d.b.a.a.R("Could not parse timestamp from file ");
                R.append(file.getName());
                bVar.b(R.toString());
            }
            file.delete();
        }
        return b.i.a.f.e.o.f.B1(arrayList);
    }

    public static void c(@Nullable String str, @NonNull File file) throws Exception {
        Throwable th;
        FileOutputStream fileOutputStream;
        if (str != null) {
            b.i.c.m.d.p.c cVar = null;
            try {
                fileOutputStream = new FileOutputStream(file, true);
                try {
                    cVar = b.i.c.m.d.p.c.i(fileOutputStream);
                    b.i.c.m.d.p.a aVar = b.i.c.m.d.p.d.a;
                    b.i.c.m.d.p.a a2 = b.i.c.m.d.p.a.a(str);
                    cVar.r(7, 2);
                    int b2 = b.i.c.m.d.p.c.b(2, a2);
                    cVar.o(b.i.c.m.d.p.c.d(b2) + b.i.c.m.d.p.c.e(5) + b2);
                    cVar.r(5, 2);
                    cVar.o(b2);
                    cVar.l(2, a2);
                    StringBuilder R = b.d.b.a.a.R("Failed to flush to append to ");
                    R.append(file.getPath());
                    b.i.c.m.d.k.h.h(cVar, R.toString());
                    b.i.c.m.d.k.h.c(fileOutputStream, "Failed to close " + file.getPath());
                } catch (Throwable th2) {
                    th = th2;
                    StringBuilder R2 = b.d.b.a.a.R("Failed to flush to append to ");
                    R2.append(file.getPath());
                    b.i.c.m.d.k.h.h(cVar, R2.toString());
                    b.i.c.m.d.k.h.c(fileOutputStream, "Failed to close " + file.getPath());
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                fileOutputStream = null;
            }
        }
    }

    public static void e(InputStream inputStream, b.i.c.m.d.p.c cVar, int i2) throws IOException {
        byte[] bArr = new byte[i2];
        int i3 = 0;
        while (i3 < i2) {
            int read = inputStream.read(bArr, i3, i2 - i3);
            if (read < 0) {
                break;
            }
            i3 += read;
        }
        Objects.requireNonNull(cVar);
        int i4 = cVar.k;
        int i5 = cVar.l;
        int i6 = i4 - i5;
        if (i6 >= i2) {
            System.arraycopy(bArr, 0, cVar.j, i5, i2);
            cVar.l += i2;
            return;
        }
        System.arraycopy(bArr, 0, cVar.j, i5, i6);
        int i7 = i6 + 0;
        int i8 = i2 - i6;
        cVar.l = cVar.k;
        cVar.j();
        if (i8 <= cVar.k) {
            System.arraycopy(bArr, i7, cVar.j, 0, i8);
            cVar.l = i8;
            return;
        }
        cVar.m.write(bArr, i7, i8);
    }

    public static long j() {
        return new Date().getTime() / 1000;
    }

    public static String o(File file) {
        return file.getName().substring(0, 35);
    }

    public static File[] r(File file, FilenameFilter filenameFilter) {
        File[] listFiles = file.listFiles(filenameFilter);
        return listFiles == null ? new File[0] : listFiles;
    }

    @NonNull
    public static String t(@NonNull String str) {
        return str.replaceAll("-", "");
    }

    public static void x(b.i.c.m.d.p.c cVar, File[] fileArr, String str) {
        Arrays.sort(fileArr, b.i.c.m.d.k.h.c);
        for (File file : fileArr) {
            try {
                b.i.c.m.d.b.a.b(String.format(Locale.US, "Found Non Fatal for session ID %s in %s ", str, file.getName()));
                A(cVar, file);
            } catch (Exception e2) {
                if (b.i.c.m.d.b.a.a(6)) {
                    Log.e("FirebaseCrashlytics", "Error writting non-fatal to session.", e2);
                }
            }
        }
    }

    public final void d(b.i.c.m.d.p.b bVar) {
        if (bVar != null) {
            try {
                bVar.a();
            } catch (IOException e2) {
                if (b.i.c.m.d.b.a.a(6)) {
                    Log.e("FirebaseCrashlytics", "Error closing session file stream in the presence of an exception", e2);
                }
            }
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:141:0x04a2 A[LOOP:6: B:140:0x04a0->B:141:0x04a2, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:147:0x04bf  */
    /* JADX WARN: Removed duplicated region for block: B:148:0x04cb  */
    /* JADX WARN: Removed duplicated region for block: B:151:0x04ed  */
    /* JADX WARN: Removed duplicated region for block: B:159:0x0518  */
    /* JADX WARN: Removed duplicated region for block: B:202:0x0628  */
    /* JADX WARN: Removed duplicated region for block: B:203:0x062b  */
    /* JADX WARN: Removed duplicated region for block: B:206:0x0646 A[Catch: IOException -> 0x0685, TryCatch #14 {IOException -> 0x0685, blocks: (B:204:0x062d, B:206:0x0646, B:209:0x0669, B:210:0x067d, B:211:0x0684), top: B:236:0x062d }] */
    /* JADX WARN: Removed duplicated region for block: B:210:0x067d A[Catch: IOException -> 0x0685, TryCatch #14 {IOException -> 0x0685, blocks: (B:204:0x062d, B:206:0x0646, B:209:0x0669, B:210:0x067d, B:211:0x0684), top: B:236:0x062d }] */
    /* JADX WARN: Removed duplicated region for block: B:217:0x06c0  */
    /* JADX WARN: Removed duplicated region for block: B:221:0x06d8 A[ORIG_RETURN, RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:50:0x0142  */
    /* JADX WARN: Removed duplicated region for block: B:89:0x030e  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void f(int r19, boolean r20) throws java.lang.Exception {
        /*
            Method dump skipped, instructions count: 1758
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.c.m.d.k.x.f(int, boolean):void");
    }

    public final void g(long j2) {
        try {
            File l2 = l();
            new File(l2, ".ae" + j2).createNewFile();
        } catch (IOException unused) {
            b.i.c.m.d.b.a.b("Could not write app exception marker.");
        }
    }

    public boolean h(int i2) {
        this.m.a();
        if (p()) {
            b.i.c.m.d.b.a.b("Skipping session finalization because a crash has already occurred.");
            return false;
        }
        b.i.c.m.d.b bVar = b.i.c.m.d.b.a;
        bVar.b("Finalizing previously open sessions.");
        try {
            f(i2, true);
            bVar.b("Closed all previously open sessions");
            return true;
        } catch (Exception e2) {
            if (b.i.c.m.d.b.a.a(6)) {
                Log.e("FirebaseCrashlytics", "Unable to finalize previously open sessions.", e2);
            }
            return false;
        }
    }

    @Nullable
    public final String i() {
        File[] s2 = s();
        if (s2.length > 0) {
            return o(s2[0]);
        }
        return null;
    }

    public File k() {
        return new File(l(), "fatal-sessions");
    }

    public File l() {
        return this.p.a();
    }

    public File m() {
        return new File(l(), "native-sessions");
    }

    public File n() {
        return new File(l(), "nonfatal-sessions");
    }

    public boolean p() {
        p0 p0Var = this.B;
        return p0Var != null && p0Var.d.get();
    }

    public File[] q() {
        LinkedList linkedList = new LinkedList();
        File k2 = k();
        FilenameFilter filenameFilter = f1695b;
        File[] listFiles = k2.listFiles(filenameFilter);
        if (listFiles == null) {
            listFiles = new File[0];
        }
        Collections.addAll(linkedList, listFiles);
        File[] listFiles2 = n().listFiles(filenameFilter);
        if (listFiles2 == null) {
            listFiles2 = new File[0];
        }
        Collections.addAll(linkedList, listFiles2);
        Collections.addAll(linkedList, r(l(), filenameFilter));
        return (File[]) linkedList.toArray(new File[linkedList.size()]);
    }

    public final File[] s() {
        File[] r = r(l(), a);
        Arrays.sort(r, c);
        return r;
    }

    public Task<Void> u(float f2, Task<b.i.c.m.d.s.h.b> task) {
        Task task2;
        c0<Void> c0Var;
        b.i.c.m.d.q.a aVar = this.u;
        File[] q = x.this.q();
        File[] listFiles = x.this.m().listFiles();
        boolean z2 = false;
        if (listFiles == null) {
            listFiles = new File[0];
        }
        if ((q != null && q.length > 0) || listFiles.length > 0) {
            z2 = true;
        }
        if (!z2) {
            b.i.c.m.d.b.a.b("No reports are available.");
            this.C.b(Boolean.FALSE);
            return b.i.a.f.e.o.f.Z(null);
        }
        b.i.c.m.d.b bVar = b.i.c.m.d.b.a;
        bVar.b("Unsent reports are available.");
        if (this.j.b()) {
            bVar.b("Automatic data collection is enabled. Allowing upload.");
            this.C.b(Boolean.FALSE);
            task2 = b.i.a.f.e.o.f.Z(Boolean.TRUE);
        } else {
            bVar.b("Automatic data collection is disabled.");
            bVar.b("Notifying that unsent reports are available.");
            this.C.b(Boolean.TRUE);
            q0 q0Var = this.j;
            synchronized (q0Var.c) {
                c0Var = q0Var.d.a;
            }
            Task<TContinuationResult> q2 = c0Var.q(new e0(this));
            bVar.b("Waiting for send/deleteUnsentReports to be called.");
            c0<Boolean> c0Var2 = this.D.a;
            FilenameFilter filenameFilter = h1.a;
            TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
            i1 i1Var = new i1(taskCompletionSource);
            q2.h(i1Var);
            c0Var2.h(i1Var);
            task2 = taskCompletionSource.a;
        }
        return task2.q(new e(task, f2));
    }

    public final void v(String str, int i2) {
        h1.b(l(), new h(b.d.b.a.a.v(str, "SessionEvent")), i2, d);
    }

    public final void w(b.i.c.m.d.p.c cVar, String str) throws IOException {
        String[] strArr;
        for (String str2 : g) {
            File[] r = r(l(), new h(b.d.b.a.a.w(str, str2, ".cls")));
            if (r.length == 0) {
                b.i.c.m.d.b.a.b("Can't find " + str2 + " data for session ID " + str);
            } else {
                b.i.c.m.d.b.a.b("Collecting " + str2 + " data for session ID " + str);
                A(cVar, r[0]);
            }
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:21:0x00f1  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x00f3  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x0100  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0112  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x0198  */
    /* JADX WARN: Removed duplicated region for block: B:31:0x01f0 A[LOOP:1: B:30:0x01ee->B:31:0x01f0, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:34:0x023a  */
    /* JADX WARN: Removed duplicated region for block: B:41:0x0253  */
    /* JADX WARN: Removed duplicated region for block: B:47:0x0295  */
    /* JADX WARN: Removed duplicated region for block: B:53:0x02bd  */
    /* JADX WARN: Removed duplicated region for block: B:56:0x0306  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void y(b.i.c.m.d.p.c r34, java.lang.Thread r35, java.lang.Throwable r36, long r37, java.lang.String r39, boolean r40) throws java.lang.Exception {
        /*
            Method dump skipped, instructions count: 798
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.c.m.d.k.x.y(b.i.c.m.d.p.c, java.lang.Thread, java.lang.Throwable, long, java.lang.String, boolean):void");
    }

    public final void z(String str, String str2, g gVar) throws Exception {
        Throwable th;
        b.i.c.m.d.p.b bVar;
        b.i.c.m.d.p.c cVar = null;
        try {
            bVar = new b.i.c.m.d.p.b(l(), str + str2);
            try {
                b.i.c.m.d.p.c i2 = b.i.c.m.d.p.c.i(bVar);
                try {
                    gVar.a(i2);
                    b.i.c.m.d.k.h.h(i2, "Failed to flush to session " + str2 + " file.");
                    b.i.c.m.d.k.h.c(bVar, "Failed to close session " + str2 + " file.");
                } catch (Throwable th2) {
                    th = th2;
                    cVar = i2;
                    b.i.c.m.d.k.h.h(cVar, "Failed to flush to session " + str2 + " file.");
                    b.i.c.m.d.k.h.c(bVar, "Failed to close session " + str2 + " file.");
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
            }
        } catch (Throwable th4) {
            th = th4;
            bVar = null;
        }
    }
}
