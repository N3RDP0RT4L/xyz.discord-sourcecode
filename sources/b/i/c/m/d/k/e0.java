package b.i.c.m.d.k;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.a.f.n.f;
import com.google.android.gms.tasks.Task;
/* compiled from: CrashlyticsController.java */
/* loaded from: classes3.dex */
public class e0 implements f<Void, Boolean> {
    public e0(x xVar) {
    }

    @Override // b.i.a.f.n.f
    @NonNull
    public Task<Boolean> a(@Nullable Void r1) throws Exception {
        return b.i.a.f.e.o.f.Z(Boolean.TRUE);
    }
}
