package b.i.c.m.d.k;

import b.d.b.a.a;
import b.i.c.m.d.b;
import b.i.c.m.d.o.h;
import java.io.File;
import java.io.IOException;
/* compiled from: CrashlyticsFileMarker.java */
/* loaded from: classes3.dex */
public class m0 {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final h f1686b;

    public m0(String str, h hVar) {
        this.a = str;
        this.f1686b = hVar;
    }

    public boolean a() {
        try {
            return b().createNewFile();
        } catch (IOException e) {
            b bVar = b.a;
            StringBuilder R = a.R("Error creating marker: ");
            R.append(this.a);
            bVar.e(R.toString(), e);
            return false;
        }
    }

    public final File b() {
        return new File(this.f1686b.a(), this.a);
    }
}
