package b.i.c.m.d.k;

import b.i.c.m.d.m.v;
import java.util.Comparator;
/* compiled from: SessionReportingCoordinator.java */
/* loaded from: classes3.dex */
public final /* synthetic */ class c1 implements Comparator {
    public static final c1 j = new c1();

    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        return ((v.b) obj).a().compareTo(((v.b) obj2).a());
    }
}
