package b.i.c.m.d.k;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.a.f.n.f;
import b.i.c.m.d.q.b;
import b.i.c.m.d.q.c.c;
import b.i.c.m.d.s.h.b;
import com.google.android.gms.tasks.Task;
import java.util.List;
import java.util.concurrent.Executor;
/* compiled from: CrashlyticsController.java */
/* loaded from: classes3.dex */
public class f0 implements f<b, Void> {
    public final /* synthetic */ List a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ boolean f1678b;
    public final /* synthetic */ Executor c;
    public final /* synthetic */ g0 d;

    public f0(g0 g0Var, List list, boolean z2, Executor executor) {
        this.d = g0Var;
        this.a = list;
        this.f1678b = z2;
        this.c = executor;
    }

    @Override // b.i.a.f.n.f
    @NonNull
    public Task<Void> a(@Nullable b bVar) throws Exception {
        b bVar2 = bVar;
        if (bVar2 == null) {
            b.i.c.m.d.b.a.g("Received null app settings, cannot send reports during app startup.");
            return b.i.a.f.e.o.f.Z(null);
        }
        for (c cVar : this.a) {
            if (cVar.d() == 1) {
                x.c(bVar2.e, cVar.c());
            }
        }
        x.b(x.this);
        b.i.c.m.d.q.b a = ((h0) x.this.r).a(bVar2);
        List list = this.a;
        boolean z2 = this.f1678b;
        float f = this.d.k.f1700b;
        synchronized (a) {
            if (a.h != null) {
                b.i.c.m.d.b.a.b("Report upload has already been started.");
            } else {
                Thread thread = new Thread(new b.d(list, z2, f), "Crashlytics Report Uploader");
                a.h = thread;
                thread.start();
            }
        }
        x.this.A.b(this.c, b.c.a.y.b.m(bVar2));
        x.this.E.b(null);
        return b.i.a.f.e.o.f.Z(null);
    }
}
