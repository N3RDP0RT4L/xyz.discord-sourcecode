package b.i.c.m.d.k;

import android.content.Context;
import androidx.annotation.NonNull;
import b.i.c.m.d.q.b;
import b.i.c.m.d.q.d.a;
import b.i.c.m.d.q.d.c;
import b.i.c.m.d.q.d.d;
/* compiled from: CrashlyticsController.java */
/* loaded from: classes3.dex */
public class h0 implements b.AbstractC0149b {
    public final /* synthetic */ x a;

    public h0(x xVar) {
        this.a = xVar;
    }

    public b a(@NonNull b.i.c.m.d.s.h.b bVar) {
        String str = bVar.c;
        String str2 = bVar.d;
        String str3 = bVar.e;
        x xVar = this.a;
        Context context = xVar.i;
        int n = h.n(context, "com.crashlytics.ApiEndpoint", "string");
        String string = n > 0 ? context.getString(n) : "";
        a aVar = new a(new c(string, str, xVar.n, "17.3.0"), new d(string, str2, xVar.n, "17.3.0"));
        String str4 = this.a.q.a;
        int m = b.c.a.y.b.m(bVar);
        x xVar2 = this.a;
        return new b(str3, str4, m, xVar2.u, aVar, xVar2.v);
    }
}
