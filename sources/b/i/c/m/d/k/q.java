package b.i.c.m.d.k;

import android.util.Log;
import b.i.c.m.d.b;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.Map;
import java.util.concurrent.Callable;
import org.json.JSONObject;
/* compiled from: CrashlyticsController.java */
/* loaded from: classes3.dex */
public class q implements Callable<Void> {
    public final /* synthetic */ Map j;
    public final /* synthetic */ x k;

    public q(x xVar, Map map) {
        this.k = xVar;
        this.j = map;
    }

    @Override // java.util.concurrent.Callable
    public Void call() throws Exception {
        BufferedWriter bufferedWriter;
        Exception e;
        String i = this.k.i();
        z0 z0Var = new z0(this.k.l());
        Map map = this.j;
        File a = z0Var.a(i);
        BufferedWriter bufferedWriter2 = null;
        try {
            String jSONObject = new JSONObject(map).toString();
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(a), z0.a));
            try {
                bufferedWriter.write(jSONObject);
                bufferedWriter.flush();
            } catch (Exception e2) {
                e = e2;
                try {
                    if (b.a.a(6)) {
                        Log.e("FirebaseCrashlytics", "Error serializing key/value metadata.", e);
                    }
                    h.c(bufferedWriter, "Failed to close key/value metadata file.");
                    return null;
                } catch (Throwable th) {
                    th = th;
                    bufferedWriter2 = bufferedWriter;
                    bufferedWriter = bufferedWriter2;
                    h.c(bufferedWriter, "Failed to close key/value metadata file.");
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                h.c(bufferedWriter, "Failed to close key/value metadata file.");
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            bufferedWriter = null;
        } catch (Throwable th3) {
            th = th3;
            bufferedWriter = bufferedWriter2;
            h.c(bufferedWriter, "Failed to close key/value metadata file.");
            throw th;
        }
        h.c(bufferedWriter, "Failed to close key/value metadata file.");
        return null;
    }
}
