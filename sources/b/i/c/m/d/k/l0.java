package b.i.c.m.d.k;

import b.i.c.m.d.b;
import java.util.concurrent.Callable;
/* compiled from: CrashlyticsCore.java */
/* loaded from: classes3.dex */
public class l0 implements Callable<Boolean> {
    public final /* synthetic */ k0 j;

    public l0(k0 k0Var) {
        this.j = k0Var;
    }

    @Override // java.util.concurrent.Callable
    public Boolean call() throws Exception {
        x xVar = this.j.h;
        boolean z2 = true;
        if (!xVar.k.b().exists()) {
            String i = xVar.i();
            if (i == null || !xVar.w.e(i)) {
                z2 = false;
            }
        } else {
            b.a.b("Found previous crash marker.");
            xVar.k.b().delete();
        }
        return Boolean.valueOf(z2);
    }
}
