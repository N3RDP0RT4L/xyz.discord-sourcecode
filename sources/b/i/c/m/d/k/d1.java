package b.i.c.m.d.k;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Environment;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.ActivityChooserModel;
import b.d.b.a.a;
import b.i.a.f.e.o.f;
import b.i.c.m.d.l.b;
import b.i.c.m.d.m.j;
import b.i.c.m.d.m.k;
import b.i.c.m.d.m.l;
import b.i.c.m.d.m.m;
import b.i.c.m.d.m.o;
import b.i.c.m.d.m.r;
import b.i.c.m.d.m.s;
import b.i.c.m.d.m.v;
import b.i.c.m.d.m.w;
import b.i.c.m.d.m.x.h;
import b.i.c.m.d.o.g;
import b.i.c.m.d.r.c;
import b.i.c.m.d.t.e;
import b.i.c.p.h.d;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executor;
/* compiled from: SessionReportingCoordinator.java */
/* loaded from: classes3.dex */
public class d1 {
    public final n0 a;

    /* renamed from: b  reason: collision with root package name */
    public final g f1676b;
    public final c c;
    public final b d;
    public final f1 e;

    public d1(n0 n0Var, g gVar, c cVar, b bVar, f1 f1Var) {
        this.a = n0Var;
        this.f1676b = gVar;
        this.c = cVar;
        this.d = bVar;
        this.e = f1Var;
    }

    public final void a(@NonNull Throwable th, @NonNull Thread thread, @NonNull String str, @NonNull String str2, long j, boolean z2) {
        Boolean bool;
        s sVar;
        boolean equals = str2.equals("crash");
        n0 n0Var = this.a;
        int i = n0Var.c.getResources().getConfiguration().orientation;
        e eVar = new e(th, n0Var.f);
        Long valueOf = Long.valueOf(j);
        ActivityManager.RunningAppProcessInfo i2 = h.i(n0Var.e.d, n0Var.c);
        if (i2 != null) {
            bool = Boolean.valueOf(i2.importance != 100);
        } else {
            bool = null;
        }
        Boolean bool2 = bool;
        Integer valueOf2 = Integer.valueOf(i);
        ArrayList arrayList = new ArrayList();
        arrayList.add(n0Var.c(thread, eVar.c, 4));
        if (z2) {
            for (Map.Entry<Thread, StackTraceElement[]> entry : Thread.getAllStackTraces().entrySet()) {
                Thread key = entry.getKey();
                if (!key.equals(thread)) {
                    arrayList.add(n0Var.c(key, n0Var.f.a(entry.getValue()), 0));
                }
            }
        }
        w wVar = new w(arrayList);
        v.d.AbstractC0142d.a.b.AbstractC0145b a = n0Var.a(eVar, 4, 8, 0);
        Long l = 0L;
        String str3 = "";
        String str4 = l == null ? " address" : str3;
        if (str4.isEmpty()) {
            o oVar = new o("0", "0", l.longValue(), null);
            v.d.AbstractC0142d.a.b.AbstractC0144a[] aVarArr = new v.d.AbstractC0142d.a.b.AbstractC0144a[1];
            Long l2 = 0L;
            Long l3 = 0L;
            String str5 = n0Var.e.d;
            Objects.requireNonNull(str5, "Null name");
            String str6 = n0Var.e.f1673b;
            String str7 = l2 == null ? " baseAddress" : str3;
            if (l3 == null) {
                str7 = a.v(str7, " size");
            }
            if (str7.isEmpty()) {
                aVarArr[0] = new m(l2.longValue(), l3.longValue(), str5, str6, null);
                l lVar = new l(wVar, a, oVar, new w(Arrays.asList(aVarArr)), null);
                String str8 = valueOf2 == null ? " uiOrientation" : str3;
                if (str8.isEmpty()) {
                    k kVar = new k(lVar, null, bool2, valueOf2.intValue(), null);
                    e a2 = e.a(n0Var.c);
                    Float f = a2.a;
                    Double valueOf3 = f != null ? Double.valueOf(f.doubleValue()) : null;
                    int b2 = a2.b();
                    boolean m = h.m(n0Var.c);
                    long p = h.p();
                    Context context = n0Var.c;
                    ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
                    ((ActivityManager) context.getSystemService(ActivityChooserModel.ATTRIBUTE_ACTIVITY)).getMemoryInfo(memoryInfo);
                    long j2 = p - memoryInfo.availMem;
                    long a3 = h.a(Environment.getDataDirectory().getPath());
                    r.b bVar = new r.b();
                    bVar.a = valueOf3;
                    bVar.f1730b = Integer.valueOf(b2);
                    bVar.c = Boolean.valueOf(m);
                    bVar.d = Integer.valueOf(i);
                    bVar.e = Long.valueOf(j2);
                    bVar.f = Long.valueOf(a3);
                    v.d.AbstractC0142d.b a4 = bVar.a();
                    String str9 = " timestamp";
                    String str10 = valueOf == null ? str9 : str3;
                    if (str10.isEmpty()) {
                        Long valueOf4 = Long.valueOf(valueOf.longValue());
                        String b3 = this.d.d.b();
                        if (b3 != null) {
                            sVar = new s(b3, null);
                        } else {
                            b.i.c.m.d.b.a.b("No log data to include with this event.");
                            sVar = null;
                        }
                        Map<String, String> a5 = this.e.a();
                        ArrayList arrayList2 = new ArrayList();
                        arrayList2.ensureCapacity(a5.size());
                        for (Map.Entry<String, String> entry2 : a5.entrySet()) {
                            String key2 = entry2.getKey();
                            Objects.requireNonNull(key2, "Null key");
                            String value = entry2.getValue();
                            Objects.requireNonNull(value, "Null value");
                            arrayList2.add(new b.i.c.m.d.m.c(key2, value, null));
                        }
                        Collections.sort(arrayList2, c1.j);
                        k kVar2 = kVar;
                        if (!arrayList2.isEmpty()) {
                            k.b bVar2 = (k.b) kVar.e();
                            bVar2.f1721b = new w<>(arrayList2);
                            kVar2 = bVar2.a();
                        }
                        k kVar3 = kVar2;
                        g gVar = this.f1676b;
                        if (valueOf4 != null) {
                            str9 = str3;
                        }
                        if (str9.isEmpty()) {
                            j jVar = new j(valueOf4.longValue(), str2, kVar3, a4, sVar, null);
                            int i3 = ((b.i.c.m.d.s.c) gVar.k).c().b().a;
                            File h = gVar.h(str);
                            Objects.requireNonNull(g.c);
                            String a6 = ((d) h.a).a(jVar);
                            String format = String.format(Locale.US, "%010d", Integer.valueOf(gVar.f.getAndIncrement()));
                            if (equals) {
                                str3 = "_";
                            }
                            try {
                                g.l(new File(h, a.w("event", format, str3)), a6);
                            } catch (IOException e) {
                                b.i.c.m.d.b.a.c("Could not persist event for session " + str, e);
                            }
                            List<File> g = g.g(h, b.i.c.m.d.o.c.a);
                            Collections.sort(g, b.i.c.m.d.o.d.j);
                            int size = g.size();
                            for (File file : g) {
                                if (size > i3) {
                                    g.k(file);
                                    size--;
                                } else {
                                    return;
                                }
                            }
                            return;
                        }
                        throw new IllegalStateException(a.v("Missing required properties:", str9));
                    }
                    throw new IllegalStateException(a.v("Missing required properties:", str10));
                }
                throw new IllegalStateException(a.v("Missing required properties:", str8));
            }
            throw new IllegalStateException(a.v("Missing required properties:", str7));
        }
        throw new IllegalStateException(a.v("Missing required properties:", str4));
    }

    /* JADX WARN: Incorrect types in method signature: (Ljava/util/concurrent/Executor;Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task<Ljava/lang/Void;>; */
    public Task b(@NonNull Executor executor, @NonNull int i) {
        char c;
        if (i == 1) {
            b.i.c.m.d.b.a.b("Send via DataTransport disabled. Removing DataTransport reports.");
            this.f1676b.b();
            return f.Z(null);
        }
        g gVar = this.f1676b;
        List<File> e = gVar.e();
        ArrayList arrayList = new ArrayList();
        arrayList.ensureCapacity(((ArrayList) e).size());
        Iterator it = ((ArrayList) gVar.e()).iterator();
        while (it.hasNext()) {
            File file = (File) it.next();
            try {
                arrayList.add(new c(g.c.f(g.j(file)), file.getName()));
            } catch (IOException e2) {
                b.i.c.m.d.b bVar = b.i.c.m.d.b.a;
                bVar.c("Could not load report file " + file + "; deleting", e2);
                file.delete();
            }
        }
        ArrayList arrayList2 = new ArrayList();
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            o0 o0Var = (o0) it2.next();
            v a = o0Var.a();
            if (a.h() != null) {
                c = 2;
            } else {
                c = a.e() != null ? (char) 3 : (char) 1;
            }
            if (c != 3 || i == 3) {
                c cVar = this.c;
                Objects.requireNonNull(cVar);
                v a2 = o0Var.a();
                TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
                cVar.e.b(new b.i.a.b.a(null, a2, b.i.a.b.d.HIGHEST), new b.i.c.m.d.r.a(taskCompletionSource, o0Var));
                arrayList2.add(taskCompletionSource.a.i(executor, new b.i.a.f.n.a(this) { // from class: b.i.c.m.d.k.b1
                    public final d1 a;

                    {
                        this.a = this;
                    }

                    @Override // b.i.a.f.n.a
                    public Object a(Task task) {
                        boolean z2;
                        d1 d1Var = this.a;
                        Objects.requireNonNull(d1Var);
                        if (task.p()) {
                            o0 o0Var2 = (o0) task.l();
                            b.i.c.m.d.b bVar2 = b.i.c.m.d.b.a;
                            StringBuilder R = a.R("Crashlytics report successfully enqueued to DataTransport: ");
                            R.append(o0Var2.b());
                            bVar2.b(R.toString());
                            d1Var.f1676b.c(o0Var2.b());
                            z2 = true;
                        } else {
                            b.i.c.m.d.b bVar3 = b.i.c.m.d.b.a;
                            Exception k = task.k();
                            if (bVar3.a(3)) {
                                Log.d("FirebaseCrashlytics", "Crashlytics report could not be enqueued to DataTransport", k);
                            }
                            z2 = false;
                        }
                        return Boolean.valueOf(z2);
                    }
                }));
            } else {
                b.i.c.m.d.b.a.b("Send native reports via DataTransport disabled. Removing DataTransport reports.");
                this.f1676b.c(o0Var.b());
            }
        }
        return f.B1(arrayList2);
    }
}
