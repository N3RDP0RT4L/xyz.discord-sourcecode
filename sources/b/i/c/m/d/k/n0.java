package b.i.c.m.d.k;

import android.content.Context;
import b.d.b.a.a;
import b.i.c.m.d.m.n;
import b.i.c.m.d.m.p;
import b.i.c.m.d.m.q;
import b.i.c.m.d.m.v;
import b.i.c.m.d.m.w;
import b.i.c.m.d.t.d;
import b.i.c.m.d.t.e;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
/* compiled from: CrashlyticsReportDataCapture.java */
/* loaded from: classes3.dex */
public class n0 {
    public static final String a = String.format(Locale.US, "Crashlytics Android SDK/%s", "17.3.0");

    /* renamed from: b  reason: collision with root package name */
    public static final Map<String, Integer> f1687b;
    public final Context c;
    public final v0 d;
    public final b e;
    public final d f;

    static {
        HashMap hashMap = new HashMap();
        f1687b = hashMap;
        a.h0(5, hashMap, "armeabi", 6, "armeabi-v7a", 9, "arm64-v8a", 0, "x86");
        hashMap.put("x86_64", 1);
    }

    public n0(Context context, v0 v0Var, b bVar, d dVar) {
        this.c = context;
        this.d = v0Var;
        this.e = bVar;
        this.f = dVar;
    }

    public final v.d.AbstractC0142d.a.b.AbstractC0145b a(e eVar, int i, int i2, int i3) {
        String str = eVar.f1749b;
        String str2 = eVar.a;
        StackTraceElement[] stackTraceElementArr = eVar.c;
        int i4 = 0;
        if (stackTraceElementArr == null) {
            stackTraceElementArr = new StackTraceElement[0];
        }
        e eVar2 = eVar.d;
        if (i3 >= i2) {
            e eVar3 = eVar2;
            while (eVar3 != null) {
                eVar3 = eVar3.d;
                i4++;
            }
        }
        v.d.AbstractC0142d.a.b.AbstractC0145b bVar = null;
        Objects.requireNonNull(str, "Null type");
        w wVar = new w(b(stackTraceElementArr, i));
        Integer valueOf = Integer.valueOf(i4);
        if (eVar2 != null && i4 == 0) {
            bVar = a(eVar2, i, i2, i3 + 1);
        }
        String str3 = valueOf == null ? " overflowCount" : "";
        if (str3.isEmpty()) {
            return new n(str, str2, wVar, bVar, valueOf.intValue(), null);
        }
        throw new IllegalStateException(a.v("Missing required properties:", str3));
    }

    public final w<v.d.AbstractC0142d.a.b.AbstractC0146d.AbstractC0147a> b(StackTraceElement[] stackTraceElementArr, int i) {
        ArrayList arrayList = new ArrayList();
        for (StackTraceElement stackTraceElement : stackTraceElementArr) {
            q.b bVar = new q.b();
            bVar.e = Integer.valueOf(i);
            long j = 0;
            long max = stackTraceElement.isNativeMethod() ? Math.max(stackTraceElement.getLineNumber(), 0L) : 0L;
            String str = stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName();
            String fileName = stackTraceElement.getFileName();
            if (!stackTraceElement.isNativeMethod() && stackTraceElement.getLineNumber() > 0) {
                j = stackTraceElement.getLineNumber();
            }
            bVar.a = Long.valueOf(max);
            Objects.requireNonNull(str, "Null symbol");
            bVar.f1728b = str;
            bVar.c = fileName;
            bVar.d = Long.valueOf(j);
            arrayList.add(bVar.a());
        }
        return new w<>(arrayList);
    }

    public final v.d.AbstractC0142d.a.b.AbstractC0146d c(Thread thread, StackTraceElement[] stackTraceElementArr, int i) {
        String name = thread.getName();
        Objects.requireNonNull(name, "Null name");
        Integer valueOf = Integer.valueOf(i);
        w wVar = new w(b(stackTraceElementArr, i));
        String str = valueOf == null ? " importance" : "";
        if (str.isEmpty()) {
            return new p(name, valueOf.intValue(), wVar, null);
        }
        throw new IllegalStateException(a.v("Missing required properties:", str));
    }
}
