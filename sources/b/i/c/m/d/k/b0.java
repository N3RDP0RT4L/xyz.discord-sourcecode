package b.i.c.m.d.k;

import androidx.annotation.NonNull;
import b.i.c.m.d.b;
import b.i.c.m.d.k.p0;
import b.i.c.m.d.s.d;
import java.util.Date;
/* compiled from: CrashlyticsController.java */
/* loaded from: classes3.dex */
public class b0 implements p0.a {
    public final /* synthetic */ x a;

    public b0(x xVar) {
        this.a = xVar;
    }

    public void a(@NonNull d dVar, @NonNull Thread thread, @NonNull Throwable th) {
        x xVar = this.a;
        synchronized (xVar) {
            b bVar = b.a;
            bVar.b("Crashlytics is handling uncaught exception \"" + th + "\" from thread " + thread.getName());
            try {
                h1.a(xVar.m.c(new d0(xVar, new Date(), th, thread, dVar)));
            } catch (Exception unused) {
            }
        }
    }
}
