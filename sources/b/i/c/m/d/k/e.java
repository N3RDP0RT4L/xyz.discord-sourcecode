package b.i.c.m.d.k;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
/* compiled from: BatteryState.java */
/* loaded from: classes3.dex */
public class e {
    public final Float a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f1677b;

    public e(Float f, boolean z2) {
        this.f1677b = z2;
        this.a = f;
    }

    public static e a(Context context) {
        Float f = null;
        Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        boolean z2 = false;
        if (registerReceiver != null) {
            int intExtra = registerReceiver.getIntExtra("status", -1);
            if (intExtra != -1 && (intExtra == 2 || intExtra == 5)) {
                z2 = true;
            }
            int intExtra2 = registerReceiver.getIntExtra("level", -1);
            int intExtra3 = registerReceiver.getIntExtra("scale", -1);
            if (!(intExtra2 == -1 || intExtra3 == -1)) {
                f = Float.valueOf(intExtra2 / intExtra3);
            }
        }
        return new e(f, z2);
    }

    public int b() {
        Float f;
        if (!this.f1677b || (f = this.a) == null) {
            return 1;
        }
        return ((double) f.floatValue()) < 0.99d ? 2 : 3;
    }
}
