package b.i.c.m.d.k;

import b.d.b.a.a;
import b.i.c.m.d.m.v;
import java.util.Objects;
/* compiled from: AutoValue_CrashlyticsReportWithSessionId.java */
/* loaded from: classes3.dex */
public final class c extends o0 {
    public final v a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1674b;

    public c(v vVar, String str) {
        Objects.requireNonNull(vVar, "Null report");
        this.a = vVar;
        Objects.requireNonNull(str, "Null sessionId");
        this.f1674b = str;
    }

    @Override // b.i.c.m.d.k.o0
    public v a() {
        return this.a;
    }

    @Override // b.i.c.m.d.k.o0
    public String b() {
        return this.f1674b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof o0)) {
            return false;
        }
        o0 o0Var = (o0) obj;
        return this.a.equals(o0Var.a()) && this.f1674b.equals(o0Var.b());
    }

    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ this.f1674b.hashCode();
    }

    public String toString() {
        StringBuilder R = a.R("CrashlyticsReportWithSessionId{report=");
        R.append(this.a);
        R.append(", sessionId=");
        return a.H(R, this.f1674b, "}");
    }
}
