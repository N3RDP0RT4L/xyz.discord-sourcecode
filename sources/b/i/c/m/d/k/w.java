package b.i.c.m.d.k;

import android.os.Build;
import b.i.c.m.d.k.x;
import b.i.c.m.d.p.a;
import b.i.c.m.d.p.c;
import b.i.c.m.d.p.d;
/* compiled from: CrashlyticsController.java */
/* loaded from: classes3.dex */
public class w implements x.g {
    public final /* synthetic */ boolean a;

    public w(x xVar, String str, String str2, boolean z2) {
        this.a = z2;
    }

    @Override // b.i.c.m.d.k.x.g
    public void a(c cVar) throws Exception {
        String str = Build.VERSION.RELEASE;
        String str2 = Build.VERSION.CODENAME;
        boolean z2 = this.a;
        a aVar = d.a;
        a a = a.a(str);
        a a2 = a.a(str2);
        cVar.r(8, 2);
        int b2 = c.b(2, a) + c.c(1, 3) + 0;
        cVar.o(c.a(4, z2) + c.b(3, a2) + b2);
        cVar.m(1, 3);
        cVar.l(2, a);
        cVar.l(3, a2);
        cVar.k(4, z2);
    }
}
