package b.i.c.m.d.k;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
/* compiled from: BytesBackedNativeSessionFile.java */
/* loaded from: classes3.dex */
public class f implements a1 {
    @Nullable
    public final byte[] a;

    public f(@NonNull String str, @NonNull String str2, @Nullable byte[] bArr) {
        this.a = bArr;
    }

    @Override // b.i.c.m.d.k.a1
    @NonNull
    public String a() {
        return "logs";
    }

    /* JADX WARN: Removed duplicated region for block: B:17:0x003a  */
    /* JADX WARN: Removed duplicated region for block: B:21:? A[RETURN, SYNTHETIC] */
    @Override // b.i.c.m.d.k.a1
    @androidx.annotation.Nullable
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.i.c.m.d.m.v.c.a b() {
        /*
            r4 = this;
            byte[] r0 = r4.a
            if (r0 == 0) goto La
            int r0 = r0.length
            if (r0 != 0) goto L8
            goto La
        L8:
            r0 = 0
            goto Lb
        La:
            r0 = 1
        Lb:
            r1 = 0
            if (r0 == 0) goto Lf
            goto L36
        Lf:
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch: java.io.IOException -> L36
            r0.<init>()     // Catch: java.io.IOException -> L36
            java.util.zip.GZIPOutputStream r2 = new java.util.zip.GZIPOutputStream     // Catch: java.lang.Throwable -> L31
            r2.<init>(r0)     // Catch: java.lang.Throwable -> L31
            byte[] r3 = r4.a     // Catch: java.lang.Throwable -> L2c
            r2.write(r3)     // Catch: java.lang.Throwable -> L2c
            r2.finish()     // Catch: java.lang.Throwable -> L2c
            byte[] r3 = r0.toByteArray()     // Catch: java.lang.Throwable -> L2c
            r2.close()     // Catch: java.lang.Throwable -> L31
            r0.close()     // Catch: java.io.IOException -> L36
            goto L37
        L2c:
            r3 = move-exception
            r2.close()     // Catch: java.lang.Throwable -> L30
        L30:
            throw r3     // Catch: java.lang.Throwable -> L31
        L31:
            r2 = move-exception
            r0.close()     // Catch: java.lang.Throwable -> L35
        L35:
            throw r2     // Catch: java.io.IOException -> L36
        L36:
            r3 = r1
        L37:
            if (r3 != 0) goto L3a
            goto L42
        L3a:
            b.i.c.m.d.m.e r0 = new b.i.c.m.d.m.e
            java.lang.String r2 = "logs_file"
            r0.<init>(r2, r3, r1)
            r1 = r0
        L42:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.c.m.d.k.f.b():b.i.c.m.d.m.v$c$a");
    }

    @Override // b.i.c.m.d.k.a1
    @Nullable
    public InputStream h() {
        byte[] bArr = this.a;
        if (bArr == null || bArr.length == 0) {
            return null;
        }
        return new ByteArrayInputStream(this.a);
    }
}
