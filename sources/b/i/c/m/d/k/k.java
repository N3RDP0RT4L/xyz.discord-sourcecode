package b.i.c.m.d.k;

import androidx.annotation.NonNull;
import b.i.a.f.n.a;
import com.google.android.gms.tasks.Task;
import java.util.concurrent.Callable;
/* compiled from: CrashlyticsBackgroundWorker.java */
/* loaded from: classes3.dex */
public class k implements a<Void, T> {
    public final /* synthetic */ Callable a;

    public k(i iVar, Callable callable) {
        this.a = callable;
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [T, java.lang.Object] */
    @Override // b.i.a.f.n.a
    public T a(@NonNull Task<Void> task) throws Exception {
        return this.a.call();
    }
}
