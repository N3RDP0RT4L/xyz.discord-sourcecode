package b.i.c.m.d.k;

import b.i.c.m.d.k.x;
import b.i.c.m.d.p.a;
import b.i.c.m.d.p.c;
import b.i.c.m.d.p.d;
/* compiled from: CrashlyticsController.java */
/* loaded from: classes3.dex */
public class v implements x.g {
    public final /* synthetic */ String a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ String f1693b;
    public final /* synthetic */ String c;
    public final /* synthetic */ String d;
    public final /* synthetic */ int e;
    public final /* synthetic */ x f;

    public v(x xVar, String str, String str2, String str3, String str4, int i) {
        this.f = xVar;
        this.a = str;
        this.f1693b = str2;
        this.c = str3;
        this.d = str4;
        this.e = i;
    }

    @Override // b.i.c.m.d.k.x.g
    public void a(c cVar) throws Exception {
        String str = this.a;
        String str2 = this.f1693b;
        String str3 = this.c;
        String str4 = this.d;
        int i = this.e;
        String str5 = this.f.f1698y;
        a aVar = d.a;
        a a = a.a(str);
        a a2 = a.a(str2);
        a a3 = a.a(str3);
        a a4 = a.a(str4);
        a a5 = str5 != null ? a.a(str5) : null;
        cVar.r(7, 2);
        int b2 = c.b(2, a2);
        int b3 = c.b(6, a4) + c.b(3, a3) + b2 + c.b(1, a) + 0;
        if (a5 != null) {
            b3 = c.b(9, a5) + c.b(8, d.f1736b) + b3;
        }
        cVar.o(c.c(10, i) + b3);
        cVar.l(1, a);
        cVar.l(2, a2);
        cVar.l(3, a3);
        cVar.l(6, a4);
        if (a5 != null) {
            cVar.l(8, d.f1736b);
            cVar.l(9, a5);
        }
        cVar.m(10, i);
    }
}
