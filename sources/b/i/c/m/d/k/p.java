package b.i.c.m.d.k;

import android.util.Log;
import b.i.c.m.d.b;
import b.i.c.m.d.o.g;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.concurrent.Callable;
/* compiled from: CrashlyticsController.java */
/* loaded from: classes3.dex */
public class p implements Callable<Void> {
    public final /* synthetic */ f1 j;
    public final /* synthetic */ x k;

    public p(x xVar, f1 f1Var) {
        this.k = xVar;
        this.j = f1Var;
    }

    @Override // java.util.concurrent.Callable
    public Void call() throws Exception {
        BufferedWriter bufferedWriter;
        Exception e;
        String jSONObject;
        String i = this.k.i();
        BufferedWriter bufferedWriter2 = null;
        if (i == null) {
            b.a.b("Tried to cache user data while no session was open.");
        } else {
            d1 d1Var = this.k.A;
            String replaceAll = i.replaceAll("-", "");
            String str = d1Var.e.a;
            if (str == null) {
                b.a.b("Could not persist user ID; no user ID available");
            } else {
                try {
                    g.l(new File(d1Var.f1676b.h(replaceAll), "user"), str);
                } catch (IOException e2) {
                    b.a.c("Could not persist user ID for session " + replaceAll, e2);
                }
            }
            z0 z0Var = new z0(this.k.l());
            f1 f1Var = this.j;
            File b2 = z0Var.b(i);
            try {
                jSONObject = new y0(f1Var).toString();
                bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(b2), z0.a));
            } catch (Exception e3) {
                e = e3;
                bufferedWriter = null;
            } catch (Throwable th) {
                th = th;
                bufferedWriter = bufferedWriter2;
                h.c(bufferedWriter, "Failed to close user metadata file.");
                throw th;
            }
            try {
                bufferedWriter.write(jSONObject);
                bufferedWriter.flush();
            } catch (Exception e4) {
                e = e4;
                try {
                    if (b.a.a(6)) {
                        Log.e("FirebaseCrashlytics", "Error serializing user metadata.", e);
                    }
                    h.c(bufferedWriter, "Failed to close user metadata file.");
                    return null;
                } catch (Throwable th2) {
                    th = th2;
                    bufferedWriter2 = bufferedWriter;
                    bufferedWriter = bufferedWriter2;
                    h.c(bufferedWriter, "Failed to close user metadata file.");
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                h.c(bufferedWriter, "Failed to close user metadata file.");
                throw th;
            }
            h.c(bufferedWriter, "Failed to close user metadata file.");
        }
        return null;
    }
}
