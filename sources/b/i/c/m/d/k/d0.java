package b.i.c.m.d.k;

import b.i.c.m.d.s.d;
import com.google.android.gms.tasks.Task;
import java.util.Date;
import java.util.concurrent.Callable;
/* compiled from: CrashlyticsController.java */
/* loaded from: classes3.dex */
public class d0 implements Callable<Task<Void>> {
    public final /* synthetic */ Date j;
    public final /* synthetic */ Throwable k;
    public final /* synthetic */ Thread l;
    public final /* synthetic */ d m;
    public final /* synthetic */ x n;

    public d0(x xVar, Date date, Throwable th, Thread thread, d dVar) {
        this.n = xVar;
        this.j = date;
        this.k = th;
        this.l = thread;
        this.m = dVar;
    }

    /* JADX WARN: Removed duplicated region for block: B:22:0x00ec  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x00f1  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x0122  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x0127  */
    @Override // java.util.concurrent.Callable
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public com.google.android.gms.tasks.Task<java.lang.Void> call() throws java.lang.Exception {
        /*
            Method dump skipped, instructions count: 328
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.c.m.d.k.d0.call():java.lang.Object");
    }
}
