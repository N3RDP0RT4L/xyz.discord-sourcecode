package b.i.c.m.d.k;

import b.i.a.f.e.o.f;
import com.google.android.gms.tasks.Task;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
/* compiled from: CrashlyticsBackgroundWorker.java */
/* loaded from: classes3.dex */
public class i {
    public final Executor a;

    /* renamed from: b  reason: collision with root package name */
    public Task<Void> f1684b = f.Z(null);
    public final Object c = new Object();
    public ThreadLocal<Boolean> d = new ThreadLocal<>();

    /* compiled from: CrashlyticsBackgroundWorker.java */
    /* loaded from: classes3.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            i.this.d.set(Boolean.TRUE);
        }
    }

    public i(Executor executor) {
        this.a = executor;
        executor.execute(new a());
    }

    public void a() {
        if (!Boolean.TRUE.equals(this.d.get())) {
            throw new IllegalStateException("Not running on background worker thread as intended.");
        }
    }

    public <T> Task<T> b(Callable<T> callable) {
        Task<T> task;
        synchronized (this.c) {
            task = (Task<T>) this.f1684b.i(this.a, new k(this, callable));
            this.f1684b = task.i(this.a, new l(this));
        }
        return task;
    }

    public <T> Task<T> c(Callable<Task<T>> callable) {
        Task<T> task;
        synchronized (this.c) {
            task = (Task<T>) this.f1684b.j(this.a, new k(this, callable));
            this.f1684b = task.i(this.a, new l(this));
        }
        return task;
    }
}
