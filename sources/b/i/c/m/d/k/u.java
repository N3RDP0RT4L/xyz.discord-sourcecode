package b.i.c.m.d.k;

import b.i.c.m.d.k.x;
import b.i.c.m.d.p.a;
import b.i.c.m.d.p.c;
import b.i.c.m.d.p.d;
/* compiled from: CrashlyticsController.java */
/* loaded from: classes3.dex */
public class u implements x.g {
    public final /* synthetic */ String a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ String f1691b;
    public final /* synthetic */ long c;

    public u(x xVar, String str, String str2, long j) {
        this.a = str;
        this.f1691b = str2;
        this.c = j;
    }

    @Override // b.i.c.m.d.k.x.g
    public void a(c cVar) throws Exception {
        String str = this.a;
        String str2 = this.f1691b;
        long j = this.c;
        a aVar = d.a;
        cVar.l(1, a.a(str2));
        cVar.l(2, a.a(str));
        cVar.t(3, j);
    }
}
