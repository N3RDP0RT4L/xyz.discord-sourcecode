package b.i.c.m.d.l;

import b.i.c.m.d.l.c;
import java.io.IOException;
import java.io.InputStream;
/* compiled from: QueueFileLogStore.java */
/* loaded from: classes3.dex */
public class d implements c.d {
    public final /* synthetic */ byte[] a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ int[] f1706b;

    public d(e eVar, byte[] bArr, int[] iArr) {
        this.a = bArr;
        this.f1706b = iArr;
    }

    @Override // b.i.c.m.d.l.c.d
    public void a(InputStream inputStream, int i) throws IOException {
        try {
            inputStream.read(this.a, this.f1706b[0], i);
            int[] iArr = this.f1706b;
            iArr[0] = iArr[0] + i;
        } finally {
            inputStream.close();
        }
    }
}
