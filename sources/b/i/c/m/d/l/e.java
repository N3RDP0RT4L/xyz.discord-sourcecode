package b.i.c.m.d.l;

import android.util.Log;
import b.i.c.m.d.b;
import b.i.c.m.d.k.h;
import com.adjust.sdk.Constants;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Locale;
/* compiled from: QueueFileLogStore.java */
/* loaded from: classes3.dex */
public class e implements b.i.c.m.d.l.a {
    public static final Charset a = Charset.forName(Constants.ENCODING);

    /* renamed from: b  reason: collision with root package name */
    public final File f1707b;
    public final int c;
    public c d;

    /* compiled from: QueueFileLogStore.java */
    /* loaded from: classes3.dex */
    public class a {
        public final byte[] a;

        /* renamed from: b  reason: collision with root package name */
        public final int f1708b;

        public a(e eVar, byte[] bArr, int i) {
            this.a = bArr;
            this.f1708b = i;
        }
    }

    public e(File file, int i) {
        this.f1707b = file;
        this.c = i;
    }

    @Override // b.i.c.m.d.l.a
    public void a() {
        h.c(this.d, "There was a problem closing the Crashlytics log file.");
        this.d = null;
    }

    @Override // b.i.c.m.d.l.a
    public String b() {
        byte[] c = c();
        if (c != null) {
            return new String(c, a);
        }
        return null;
    }

    /* JADX WARN: Removed duplicated region for block: B:15:0x0044 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0045  */
    @Override // b.i.c.m.d.l.a
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public byte[] c() {
        /*
            r7 = this;
            java.io.File r0 = r7.f1707b
            boolean r0 = r0.exists()
            r1 = 0
            r2 = 0
            if (r0 != 0) goto Lc
        La:
            r4 = r1
            goto L42
        Lc:
            r7.f()
            b.i.c.m.d.l.c r0 = r7.d
            if (r0 != 0) goto L14
            goto La
        L14:
            r3 = 1
            int[] r3 = new int[r3]
            r3[r2] = r2
            int r0 = r0.x()
            byte[] r0 = new byte[r0]
            b.i.c.m.d.l.c r4 = r7.d     // Catch: java.io.IOException -> L2a
            b.i.c.m.d.l.d r5 = new b.i.c.m.d.l.d     // Catch: java.io.IOException -> L2a
            r5.<init>(r7, r0, r3)     // Catch: java.io.IOException -> L2a
            r4.e(r5)     // Catch: java.io.IOException -> L2a
            goto L3b
        L2a:
            r4 = move-exception
            b.i.c.m.d.b r5 = b.i.c.m.d.b.a
            r6 = 6
            boolean r5 = r5.a(r6)
            if (r5 == 0) goto L3b
            java.lang.String r5 = "FirebaseCrashlytics"
            java.lang.String r6 = "A problem occurred while reading the Crashlytics log file."
            android.util.Log.e(r5, r6, r4)
        L3b:
            b.i.c.m.d.l.e$a r4 = new b.i.c.m.d.l.e$a
            r3 = r3[r2]
            r4.<init>(r7, r0, r3)
        L42:
            if (r4 != 0) goto L45
            return r1
        L45:
            int r0 = r4.f1708b
            byte[] r1 = new byte[r0]
            byte[] r3 = r4.a
            java.lang.System.arraycopy(r3, r2, r1, r2, r0)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.c.m.d.l.e.c():byte[]");
    }

    @Override // b.i.c.m.d.l.a
    public void d() {
        h.c(this.d, "There was a problem closing the Crashlytics log file.");
        this.d = null;
        this.f1707b.delete();
    }

    @Override // b.i.c.m.d.l.a
    public void e(long j, String str) {
        int i;
        f();
        if (this.d != null) {
            if (str == null) {
                str = "null";
            }
            try {
                if (str.length() > this.c / 4) {
                    str = "..." + str.substring(str.length() - i);
                }
                this.d.b(String.format(Locale.US, "%d %s%n", Long.valueOf(j), str.replaceAll("\r", " ").replaceAll("\n", " ")).getBytes(a));
                while (!this.d.f() && this.d.x() > this.c) {
                    this.d.s();
                }
            } catch (IOException e) {
                if (b.a.a(6)) {
                    Log.e("FirebaseCrashlytics", "There was a problem writing to the Crashlytics log.", e);
                }
            }
        }
    }

    public final void f() {
        if (this.d == null) {
            try {
                this.d = new c(this.f1707b);
            } catch (IOException e) {
                b bVar = b.a;
                StringBuilder R = b.d.b.a.a.R("Could not open log file: ");
                R.append(this.f1707b);
                bVar.e(R.toString(), e);
            }
        }
    }
}
