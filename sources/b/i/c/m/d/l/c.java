package b.i.c.m.d.l;

import com.discord.api.permission.Permission;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
/* compiled from: QueueFile.java */
/* loaded from: classes3.dex */
public class c implements Closeable {
    public static final Logger j = Logger.getLogger(c.class.getName());
    public final RandomAccessFile k;
    public int l;
    public int m;
    public b n;
    public b o;
    public final byte[] p = new byte[16];

    /* compiled from: QueueFile.java */
    /* loaded from: classes3.dex */
    public class a implements d {
        public boolean a = true;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ StringBuilder f1704b;

        public a(c cVar, StringBuilder sb) {
            this.f1704b = sb;
        }

        @Override // b.i.c.m.d.l.c.d
        public void a(InputStream inputStream, int i) throws IOException {
            if (this.a) {
                this.a = false;
            } else {
                this.f1704b.append(", ");
            }
            this.f1704b.append(i);
        }
    }

    /* compiled from: QueueFile.java */
    /* loaded from: classes3.dex */
    public static class b {
        public static final b a = new b(0, 0);

        /* renamed from: b  reason: collision with root package name */
        public final int f1705b;
        public final int c;

        public b(int i, int i2) {
            this.f1705b = i;
            this.c = i2;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(b.class.getSimpleName());
            sb.append("[position = ");
            sb.append(this.f1705b);
            sb.append(", length = ");
            return b.d.b.a.a.A(sb, this.c, "]");
        }
    }

    /* compiled from: QueueFile.java */
    /* loaded from: classes3.dex */
    public interface d {
        void a(InputStream inputStream, int i) throws IOException;
    }

    /* JADX WARN: Finally extract failed */
    public c(File file) throws IOException {
        if (!file.exists()) {
            File file2 = new File(file.getPath() + ".tmp");
            RandomAccessFile randomAccessFile = new RandomAccessFile(file2, "rwd");
            try {
                randomAccessFile.setLength(Permission.SEND_TTS_MESSAGES);
                randomAccessFile.seek(0L);
                byte[] bArr = new byte[16];
                int[] iArr = {4096, 0, 0, 0};
                int i = 0;
                for (int i2 = 0; i2 < 4; i2++) {
                    C(bArr, i, iArr[i2]);
                    i += 4;
                }
                randomAccessFile.write(bArr);
                randomAccessFile.close();
                if (!file2.renameTo(file)) {
                    throw new IOException("Rename failed!");
                }
            } catch (Throwable th) {
                randomAccessFile.close();
                throw th;
            }
        }
        RandomAccessFile randomAccessFile2 = new RandomAccessFile(file, "rwd");
        this.k = randomAccessFile2;
        randomAccessFile2.seek(0L);
        randomAccessFile2.readFully(this.p);
        int q = q(this.p, 0);
        this.l = q;
        if (q <= randomAccessFile2.length()) {
            this.m = q(this.p, 4);
            int q2 = q(this.p, 8);
            int q3 = q(this.p, 12);
            this.n = n(q2);
            this.o = n(q3);
            return;
        }
        StringBuilder R = b.d.b.a.a.R("File is truncated. Expected length: ");
        R.append(this.l);
        R.append(", Actual length: ");
        R.append(randomAccessFile2.length());
        throw new IOException(R.toString());
    }

    public static void C(byte[] bArr, int i, int i2) {
        bArr[i] = (byte) (i2 >> 24);
        bArr[i + 1] = (byte) (i2 >> 16);
        bArr[i + 2] = (byte) (i2 >> 8);
        bArr[i + 3] = (byte) i2;
    }

    public static int a(c cVar, int i) {
        int i2 = cVar.l;
        return i < i2 ? i : (i + 16) - i2;
    }

    public static int q(byte[] bArr, int i) {
        return ((bArr[i] & 255) << 24) + ((bArr[i + 1] & 255) << 16) + ((bArr[i + 2] & 255) << 8) + (bArr[i + 3] & 255);
    }

    public final void A(int i, int i2, int i3, int i4) throws IOException {
        byte[] bArr = this.p;
        int[] iArr = {i, i2, i3, i4};
        int i5 = 0;
        for (int i6 = 0; i6 < 4; i6++) {
            C(bArr, i5, iArr[i6]);
            i5 += 4;
        }
        this.k.seek(0L);
        this.k.write(this.p);
    }

    public void b(byte[] bArr) throws IOException {
        int i;
        int length = bArr.length;
        synchronized (this) {
            if ((0 | length) >= 0) {
                if (length <= bArr.length - 0) {
                    d(length);
                    boolean f = f();
                    if (f) {
                        i = 16;
                    } else {
                        b bVar = this.o;
                        i = y(bVar.f1705b + 4 + bVar.c);
                    }
                    b bVar2 = new b(i, length);
                    C(this.p, 0, length);
                    u(i, this.p, 0, 4);
                    u(i + 4, bArr, 0, length);
                    A(this.l, this.m + 1, f ? i : this.n.f1705b, i);
                    this.o = bVar2;
                    this.m++;
                    if (f) {
                        this.n = bVar2;
                    }
                }
            }
            throw new IndexOutOfBoundsException();
        }
    }

    public synchronized void c() throws IOException {
        A(4096, 0, 0, 0);
        this.m = 0;
        b bVar = b.a;
        this.n = bVar;
        this.o = bVar;
        if (this.l > 4096) {
            this.k.setLength(4096);
            this.k.getChannel().force(true);
        }
        this.l = 4096;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public synchronized void close() throws IOException {
        this.k.close();
    }

    public final void d(int i) throws IOException {
        int i2 = i + 4;
        int x2 = this.l - x();
        if (x2 < i2) {
            int i3 = this.l;
            do {
                x2 += i3;
                i3 <<= 1;
            } while (x2 < i2);
            this.k.setLength(i3);
            this.k.getChannel().force(true);
            b bVar = this.o;
            int y2 = y(bVar.f1705b + 4 + bVar.c);
            if (y2 < this.n.f1705b) {
                FileChannel channel = this.k.getChannel();
                channel.position(this.l);
                long j2 = y2 - 4;
                if (channel.transferTo(16L, j2, channel) != j2) {
                    throw new AssertionError("Copied insufficient number of bytes!");
                }
            }
            int i4 = this.o.f1705b;
            int i5 = this.n.f1705b;
            if (i4 < i5) {
                int i6 = (this.l + i4) - 16;
                A(i3, this.m, i5, i6);
                this.o = new b(i6, this.o.c);
            } else {
                A(i3, this.m, i5, i4);
            }
            this.l = i3;
        }
    }

    public synchronized void e(d dVar) throws IOException {
        int i = this.n.f1705b;
        for (int i2 = 0; i2 < this.m; i2++) {
            b n = n(i);
            dVar.a(new C0138c(n, null), n.c);
            i = y(n.f1705b + 4 + n.c);
        }
    }

    public synchronized boolean f() {
        return this.m == 0;
    }

    public final b n(int i) throws IOException {
        if (i == 0) {
            return b.a;
        }
        this.k.seek(i);
        return new b(i, this.k.readInt());
    }

    public synchronized void s() throws IOException {
        if (f()) {
            throw new NoSuchElementException();
        } else if (this.m == 1) {
            c();
        } else {
            b bVar = this.n;
            int y2 = y(bVar.f1705b + 4 + bVar.c);
            t(y2, this.p, 0, 4);
            int q = q(this.p, 0);
            A(this.l, this.m - 1, y2, this.o.f1705b);
            this.m--;
            this.n = new b(y2, q);
        }
    }

    public final void t(int i, byte[] bArr, int i2, int i3) throws IOException {
        int i4 = this.l;
        if (i >= i4) {
            i = (i + 16) - i4;
        }
        if (i + i3 <= i4) {
            this.k.seek(i);
            this.k.readFully(bArr, i2, i3);
            return;
        }
        int i5 = i4 - i;
        this.k.seek(i);
        this.k.readFully(bArr, i2, i5);
        this.k.seek(16L);
        this.k.readFully(bArr, i2 + i5, i3 - i5);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(c.class.getSimpleName());
        sb.append('[');
        sb.append("fileLength=");
        sb.append(this.l);
        sb.append(", size=");
        sb.append(this.m);
        sb.append(", first=");
        sb.append(this.n);
        sb.append(", last=");
        sb.append(this.o);
        sb.append(", element lengths=[");
        try {
            e(new a(this, sb));
        } catch (IOException e) {
            j.log(Level.WARNING, "read error", (Throwable) e);
        }
        sb.append("]]");
        return sb.toString();
    }

    public final void u(int i, byte[] bArr, int i2, int i3) throws IOException {
        int i4 = this.l;
        if (i >= i4) {
            i = (i + 16) - i4;
        }
        if (i + i3 <= i4) {
            this.k.seek(i);
            this.k.write(bArr, i2, i3);
            return;
        }
        int i5 = i4 - i;
        this.k.seek(i);
        this.k.write(bArr, i2, i5);
        this.k.seek(16L);
        this.k.write(bArr, i2 + i5, i3 - i5);
    }

    public int x() {
        if (this.m == 0) {
            return 16;
        }
        b bVar = this.o;
        int i = bVar.f1705b;
        int i2 = this.n.f1705b;
        if (i >= i2) {
            return (i - i2) + 4 + bVar.c + 16;
        }
        return (((i + 4) + bVar.c) + this.l) - i2;
    }

    public final int y(int i) {
        int i2 = this.l;
        return i < i2 ? i : (i + 16) - i2;
    }

    /* compiled from: QueueFile.java */
    /* renamed from: b.i.c.m.d.l.c$c  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public final class C0138c extends InputStream {
        public int j;
        public int k;

        public C0138c(b bVar, a aVar) {
            int i = bVar.f1705b + 4;
            int i2 = c.this.l;
            this.j = i >= i2 ? (i + 16) - i2 : i;
            this.k = bVar.c;
        }

        @Override // java.io.InputStream
        public int read(byte[] bArr, int i, int i2) throws IOException {
            Objects.requireNonNull(bArr, "buffer");
            if ((i | i2) < 0 || i2 > bArr.length - i) {
                throw new ArrayIndexOutOfBoundsException();
            }
            int i3 = this.k;
            if (i3 <= 0) {
                return -1;
            }
            if (i2 > i3) {
                i2 = i3;
            }
            c.this.t(this.j, bArr, i, i2);
            this.j = c.a(c.this, this.j + i2);
            this.k -= i2;
            return i2;
        }

        @Override // java.io.InputStream
        public int read() throws IOException {
            if (this.k == 0) {
                return -1;
            }
            c.this.k.seek(this.j);
            int read = c.this.k.read();
            this.j = c.a(c.this, this.j + 1);
            this.k--;
            return read;
        }
    }
}
