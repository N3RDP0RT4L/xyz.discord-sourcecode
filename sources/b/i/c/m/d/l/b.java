package b.i.c.m.d.l;

import android.content.Context;
import b.i.c.m.d.k.h;
import b.i.c.m.d.k.x;
import java.io.File;
/* compiled from: LogFileManager.java */
/* loaded from: classes3.dex */
public class b {
    public static final c a = new c(null);

    /* renamed from: b  reason: collision with root package name */
    public final Context f1703b;
    public final AbstractC0137b c;
    public b.i.c.m.d.l.a d = a;

    /* compiled from: LogFileManager.java */
    /* renamed from: b.i.c.m.d.l.b$b  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public interface AbstractC0137b {
    }

    /* compiled from: LogFileManager.java */
    /* loaded from: classes3.dex */
    public static final class c implements b.i.c.m.d.l.a {
        public c(a aVar) {
        }

        @Override // b.i.c.m.d.l.a
        public void a() {
        }

        @Override // b.i.c.m.d.l.a
        public String b() {
            return null;
        }

        @Override // b.i.c.m.d.l.a
        public byte[] c() {
            return null;
        }

        @Override // b.i.c.m.d.l.a
        public void d() {
        }

        @Override // b.i.c.m.d.l.a
        public void e(long j, String str) {
        }
    }

    public b(Context context, AbstractC0137b bVar) {
        this.f1703b = context;
        this.c = bVar;
        a(null);
    }

    public final void a(String str) {
        this.d.a();
        this.d = a;
        if (str != null) {
            if (!h.j(this.f1703b, "com.crashlytics.CollectCustomLogs", true)) {
                b.i.c.m.d.b.a.b("Preferences requested no custom logs. Aborting log file creation.");
                return;
            }
            this.d = new e(new File(((x.j) this.c).a(), b.d.b.a.a.w("crashlytics-userlog-", str, ".temp")), 65536);
        }
    }

    public b(Context context, AbstractC0137b bVar, String str) {
        this.f1703b = context;
        this.c = bVar;
        a(str);
    }
}
