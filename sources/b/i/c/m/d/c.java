package b.i.c.m.d;

import androidx.annotation.NonNull;
import java.io.File;
/* compiled from: MissingNativeComponent.java */
/* loaded from: classes3.dex */
public final class c implements b.i.c.m.d.a {
    public static final d a = new b(null);

    /* compiled from: MissingNativeComponent.java */
    /* loaded from: classes3.dex */
    public static final class b implements d {
        public b(a aVar) {
        }

        @Override // b.i.c.m.d.d
        public File a() {
            return null;
        }

        @Override // b.i.c.m.d.d
        public File b() {
            return null;
        }

        @Override // b.i.c.m.d.d
        public File c() {
            return null;
        }

        @Override // b.i.c.m.d.d
        public File d() {
            return null;
        }

        @Override // b.i.c.m.d.d
        public File e() {
            return null;
        }

        @Override // b.i.c.m.d.d
        public File f() {
            return null;
        }
    }

    @Override // b.i.c.m.d.a
    public boolean a(@NonNull String str) {
        return true;
    }

    @Override // b.i.c.m.d.a
    @NonNull
    public d b(@NonNull String str) {
        return a;
    }

    @Override // b.i.c.m.d.a
    public void c(@NonNull String str, int i, @NonNull String str2, int i2, long j, long j2, boolean z2, int i3, @NonNull String str3, @NonNull String str4) {
    }

    @Override // b.i.c.m.d.a
    public void d(@NonNull String str, @NonNull String str2, long j) {
    }

    @Override // b.i.c.m.d.a
    public boolean e(@NonNull String str) {
        return false;
    }

    @Override // b.i.c.m.d.a
    public void f(@NonNull String str, @NonNull String str2, @NonNull String str3, @NonNull String str4, @NonNull String str5, int i, @NonNull String str6) {
    }

    @Override // b.i.c.m.d.a
    public void g(@NonNull String str, @NonNull String str2, @NonNull String str3, boolean z2) {
    }

    @Override // b.i.c.m.d.a
    public boolean h(@NonNull String str) {
        return true;
    }
}
