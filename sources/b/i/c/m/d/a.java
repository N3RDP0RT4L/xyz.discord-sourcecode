package b.i.c.m.d;

import androidx.annotation.NonNull;
/* compiled from: CrashlyticsNativeComponent.java */
/* loaded from: classes3.dex */
public interface a {
    boolean a(@NonNull String str);

    @NonNull
    d b(@NonNull String str);

    void c(@NonNull String str, int i, @NonNull String str2, int i2, long j, long j2, boolean z2, int i3, @NonNull String str3, @NonNull String str4);

    void d(@NonNull String str, @NonNull String str2, long j);

    boolean e(@NonNull String str);

    void f(@NonNull String str, @NonNull String str2, @NonNull String str3, @NonNull String str4, @NonNull String str5, int i, @NonNull String str6);

    void g(@NonNull String str, @NonNull String str2, @NonNull String str3, boolean z2);

    boolean h(@NonNull String str);
}
