package b.i.c.m.d.m;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.c.m.d.m.b;
import b.i.c.m.d.m.f;
import com.adjust.sdk.Constants;
import com.google.auto.value.AutoValue;
import java.nio.charset.Charset;
/* compiled from: CrashlyticsReport.java */
@AutoValue
/* loaded from: classes3.dex */
public abstract class v {
    public static final Charset a = Charset.forName(Constants.ENCODING);

    /* compiled from: CrashlyticsReport.java */
    @AutoValue.Builder
    /* loaded from: classes3.dex */
    public static abstract class a {
        @NonNull
        public abstract v a();
    }

    /* compiled from: CrashlyticsReport.java */
    @AutoValue
    /* loaded from: classes3.dex */
    public static abstract class b {
        @NonNull
        public abstract String a();

        @NonNull
        public abstract String b();
    }

    /* compiled from: CrashlyticsReport.java */
    @AutoValue
    /* loaded from: classes3.dex */
    public static abstract class c {

        /* compiled from: CrashlyticsReport.java */
        @AutoValue
        /* loaded from: classes3.dex */
        public static abstract class a {
            @NonNull
            public abstract byte[] a();

            @NonNull
            public abstract String b();
        }

        @NonNull
        public abstract w<a> a();

        @Nullable
        public abstract String b();
    }

    /* compiled from: CrashlyticsReport.java */
    @AutoValue
    /* loaded from: classes3.dex */
    public static abstract class d {

        /* compiled from: CrashlyticsReport.java */
        @AutoValue
        /* loaded from: classes3.dex */
        public static abstract class a {

            /* compiled from: CrashlyticsReport.java */
            @AutoValue
            /* renamed from: b.i.c.m.d.m.v$d$a$a  reason: collision with other inner class name */
            /* loaded from: classes3.dex */
            public static abstract class AbstractC0141a {
                @NonNull
                public abstract String a();
            }

            @Nullable
            public abstract String a();

            @Nullable
            public abstract String b();

            @Nullable
            public abstract String c();

            @NonNull
            public abstract String d();

            @Nullable
            public abstract String e();

            @Nullable
            public abstract AbstractC0141a f();

            @NonNull
            public abstract String g();
        }

        /* compiled from: CrashlyticsReport.java */
        @AutoValue.Builder
        /* loaded from: classes3.dex */
        public static abstract class b {
            @NonNull
            public abstract d a();
        }

        /* compiled from: CrashlyticsReport.java */
        @AutoValue
        /* loaded from: classes3.dex */
        public static abstract class c {

            /* compiled from: CrashlyticsReport.java */
            @AutoValue.Builder
            /* loaded from: classes3.dex */
            public static abstract class a {
            }

            @NonNull
            public abstract int a();

            public abstract int b();

            public abstract long c();

            @NonNull
            public abstract String d();

            @NonNull
            public abstract String e();

            @NonNull
            public abstract String f();

            public abstract long g();

            public abstract int h();

            public abstract boolean i();
        }

        /* compiled from: CrashlyticsReport.java */
        @AutoValue
        /* renamed from: b.i.c.m.d.m.v$d$d  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static abstract class AbstractC0142d {

            /* compiled from: CrashlyticsReport.java */
            @AutoValue
            /* renamed from: b.i.c.m.d.m.v$d$d$a */
            /* loaded from: classes3.dex */
            public static abstract class a {

                /* compiled from: CrashlyticsReport.java */
                @AutoValue.Builder
                /* renamed from: b.i.c.m.d.m.v$d$d$a$a  reason: collision with other inner class name */
                /* loaded from: classes3.dex */
                public static abstract class AbstractC0143a {
                }

                /* compiled from: CrashlyticsReport.java */
                @AutoValue
                /* renamed from: b.i.c.m.d.m.v$d$d$a$b */
                /* loaded from: classes3.dex */
                public static abstract class b {

                    /* compiled from: CrashlyticsReport.java */
                    @AutoValue
                    /* renamed from: b.i.c.m.d.m.v$d$d$a$b$a  reason: collision with other inner class name */
                    /* loaded from: classes3.dex */
                    public static abstract class AbstractC0144a {
                        @NonNull
                        public abstract long a();

                        @NonNull
                        public abstract String b();

                        public abstract long c();

                        @Nullable
                        public abstract String d();
                    }

                    /* compiled from: CrashlyticsReport.java */
                    @AutoValue
                    /* renamed from: b.i.c.m.d.m.v$d$d$a$b$b  reason: collision with other inner class name */
                    /* loaded from: classes3.dex */
                    public static abstract class AbstractC0145b {
                        @Nullable
                        public abstract AbstractC0145b a();

                        @NonNull
                        public abstract w<AbstractC0146d.AbstractC0147a> b();

                        public abstract int c();

                        @Nullable
                        public abstract String d();

                        @NonNull
                        public abstract String e();
                    }

                    /* compiled from: CrashlyticsReport.java */
                    @AutoValue
                    /* renamed from: b.i.c.m.d.m.v$d$d$a$b$c */
                    /* loaded from: classes3.dex */
                    public static abstract class c {
                        @NonNull
                        public abstract long a();

                        @NonNull
                        public abstract String b();

                        @NonNull
                        public abstract String c();
                    }

                    /* compiled from: CrashlyticsReport.java */
                    @AutoValue
                    /* renamed from: b.i.c.m.d.m.v$d$d$a$b$d  reason: collision with other inner class name */
                    /* loaded from: classes3.dex */
                    public static abstract class AbstractC0146d {

                        /* compiled from: CrashlyticsReport.java */
                        @AutoValue
                        /* renamed from: b.i.c.m.d.m.v$d$d$a$b$d$a  reason: collision with other inner class name */
                        /* loaded from: classes3.dex */
                        public static abstract class AbstractC0147a {

                            /* compiled from: CrashlyticsReport.java */
                            @AutoValue.Builder
                            /* renamed from: b.i.c.m.d.m.v$d$d$a$b$d$a$a  reason: collision with other inner class name */
                            /* loaded from: classes3.dex */
                            public static abstract class AbstractC0148a {
                            }

                            @Nullable
                            public abstract String a();

                            public abstract int b();

                            public abstract long c();

                            public abstract long d();

                            @NonNull
                            public abstract String e();
                        }

                        @NonNull
                        public abstract w<AbstractC0147a> a();

                        public abstract int b();

                        @NonNull
                        public abstract String c();
                    }

                    @NonNull
                    public abstract w<AbstractC0144a> a();

                    @NonNull
                    public abstract AbstractC0145b b();

                    @NonNull
                    public abstract c c();

                    @NonNull
                    public abstract w<AbstractC0146d> d();
                }

                @Nullable
                public abstract Boolean a();

                @Nullable
                public abstract w<b> b();

                @NonNull
                public abstract b c();

                public abstract int d();
            }

            /* compiled from: CrashlyticsReport.java */
            @AutoValue
            /* renamed from: b.i.c.m.d.m.v$d$d$b */
            /* loaded from: classes3.dex */
            public static abstract class b {

                /* compiled from: CrashlyticsReport.java */
                @AutoValue.Builder
                /* renamed from: b.i.c.m.d.m.v$d$d$b$a */
                /* loaded from: classes3.dex */
                public static abstract class a {
                }

                @Nullable
                public abstract Double a();

                public abstract int b();

                public abstract long c();

                public abstract int d();

                public abstract long e();

                public abstract boolean f();
            }

            /* compiled from: CrashlyticsReport.java */
            @AutoValue
            /* renamed from: b.i.c.m.d.m.v$d$d$c */
            /* loaded from: classes3.dex */
            public static abstract class c {
                @NonNull
                public abstract String a();
            }

            @NonNull
            public abstract a a();

            @NonNull
            public abstract b b();

            @Nullable
            public abstract c c();

            public abstract long d();

            @NonNull
            public abstract String e();
        }

        /* compiled from: CrashlyticsReport.java */
        @AutoValue
        /* loaded from: classes3.dex */
        public static abstract class e {
            @NonNull
            public abstract String a();

            public abstract int b();

            @NonNull
            public abstract String c();

            public abstract boolean d();
        }

        /* compiled from: CrashlyticsReport.java */
        @AutoValue
        /* loaded from: classes3.dex */
        public static abstract class f {
            @NonNull
            public abstract String a();
        }

        @NonNull
        public abstract a a();

        @Nullable
        public abstract c b();

        @Nullable
        public abstract Long c();

        @Nullable
        public abstract w<AbstractC0142d> d();

        @NonNull
        public abstract String e();

        public abstract int f();

        @NonNull
        public abstract String g();

        @Nullable
        public abstract e h();

        public abstract long i();

        @Nullable
        public abstract f j();

        public abstract boolean k();

        @NonNull
        public abstract b l();
    }

    @NonNull
    public abstract String a();

    @NonNull
    public abstract String b();

    @NonNull
    public abstract String c();

    @NonNull
    public abstract String d();

    @Nullable
    public abstract c e();

    public abstract int f();

    @NonNull
    public abstract String g();

    @Nullable
    public abstract d h();

    @NonNull
    public abstract a i();

    @NonNull
    public v j(long j, boolean z2, @Nullable String str) {
        a i = i();
        d dVar = ((b.i.c.m.d.m.b) this).h;
        if (dVar != null) {
            d.b l = dVar.l();
            f.b bVar = (f.b) l;
            bVar.d = Long.valueOf(j);
            bVar.e = Boolean.valueOf(z2);
            if (str != null) {
                bVar.g = new u(str, null);
                bVar.a();
            }
            ((b.C0140b) i).g = l.a();
        }
        return i.a();
    }
}
