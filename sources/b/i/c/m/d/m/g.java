package b.i.c.m.d.m;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.c.m.d.m.v;
/* compiled from: AutoValue_CrashlyticsReport_Session_Application.java */
/* loaded from: classes3.dex */
public final class g extends v.d.a {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1716b;
    public final String c;
    public final v.d.a.AbstractC0141a d = null;
    public final String e;
    public final String f;
    public final String g;

    public g(String str, String str2, String str3, v.d.a.AbstractC0141a aVar, String str4, String str5, String str6, a aVar2) {
        this.a = str;
        this.f1716b = str2;
        this.c = str3;
        this.e = str4;
        this.f = str5;
        this.g = str6;
    }

    @Override // b.i.c.m.d.m.v.d.a
    @Nullable
    public String a() {
        return this.f;
    }

    @Override // b.i.c.m.d.m.v.d.a
    @Nullable
    public String b() {
        return this.g;
    }

    @Override // b.i.c.m.d.m.v.d.a
    @Nullable
    public String c() {
        return this.c;
    }

    @Override // b.i.c.m.d.m.v.d.a
    @NonNull
    public String d() {
        return this.a;
    }

    @Override // b.i.c.m.d.m.v.d.a
    @Nullable
    public String e() {
        return this.e;
    }

    public boolean equals(Object obj) {
        String str;
        v.d.a.AbstractC0141a aVar;
        String str2;
        String str3;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v.d.a)) {
            return false;
        }
        v.d.a aVar2 = (v.d.a) obj;
        if (this.a.equals(aVar2.d()) && this.f1716b.equals(aVar2.g()) && ((str = this.c) != null ? str.equals(aVar2.c()) : aVar2.c() == null) && ((aVar = this.d) != null ? aVar.equals(aVar2.f()) : aVar2.f() == null) && ((str2 = this.e) != null ? str2.equals(aVar2.e()) : aVar2.e() == null) && ((str3 = this.f) != null ? str3.equals(aVar2.a()) : aVar2.a() == null)) {
            String str4 = this.g;
            if (str4 == null) {
                if (aVar2.b() == null) {
                    return true;
                }
            } else if (str4.equals(aVar2.b())) {
                return true;
            }
        }
        return false;
    }

    @Override // b.i.c.m.d.m.v.d.a
    @Nullable
    public v.d.a.AbstractC0141a f() {
        return this.d;
    }

    @Override // b.i.c.m.d.m.v.d.a
    @NonNull
    public String g() {
        return this.f1716b;
    }

    public int hashCode() {
        int hashCode = (((this.a.hashCode() ^ 1000003) * 1000003) ^ this.f1716b.hashCode()) * 1000003;
        String str = this.c;
        int i = 0;
        int hashCode2 = (hashCode ^ (str == null ? 0 : str.hashCode())) * 1000003;
        v.d.a.AbstractC0141a aVar = this.d;
        int hashCode3 = (hashCode2 ^ (aVar == null ? 0 : aVar.hashCode())) * 1000003;
        String str2 = this.e;
        int hashCode4 = (hashCode3 ^ (str2 == null ? 0 : str2.hashCode())) * 1000003;
        String str3 = this.f;
        int hashCode5 = (hashCode4 ^ (str3 == null ? 0 : str3.hashCode())) * 1000003;
        String str4 = this.g;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return hashCode5 ^ i;
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("Application{identifier=");
        R.append(this.a);
        R.append(", version=");
        R.append(this.f1716b);
        R.append(", displayVersion=");
        R.append(this.c);
        R.append(", organization=");
        R.append(this.d);
        R.append(", installationUuid=");
        R.append(this.e);
        R.append(", developmentPlatform=");
        R.append(this.f);
        R.append(", developmentPlatformVersion=");
        return b.d.b.a.a.H(R, this.g, "}");
    }
}
