package b.i.c.m.d.m;

import androidx.annotation.Nullable;
import b.i.c.m.d.m.v;
/* compiled from: AutoValue_CrashlyticsReport_Session_Event_Device.java */
/* loaded from: classes3.dex */
public final class r extends v.d.AbstractC0142d.b {
    public final Double a;

    /* renamed from: b  reason: collision with root package name */
    public final int f1729b;
    public final boolean c;
    public final int d;
    public final long e;
    public final long f;

    /* compiled from: AutoValue_CrashlyticsReport_Session_Event_Device.java */
    /* loaded from: classes3.dex */
    public static final class b extends v.d.AbstractC0142d.b.a {
        public Double a;

        /* renamed from: b  reason: collision with root package name */
        public Integer f1730b;
        public Boolean c;
        public Integer d;
        public Long e;
        public Long f;

        public v.d.AbstractC0142d.b a() {
            String str = this.f1730b == null ? " batteryVelocity" : "";
            if (this.c == null) {
                str = b.d.b.a.a.v(str, " proximityOn");
            }
            if (this.d == null) {
                str = b.d.b.a.a.v(str, " orientation");
            }
            if (this.e == null) {
                str = b.d.b.a.a.v(str, " ramUsed");
            }
            if (this.f == null) {
                str = b.d.b.a.a.v(str, " diskUsed");
            }
            if (str.isEmpty()) {
                return new r(this.a, this.f1730b.intValue(), this.c.booleanValue(), this.d.intValue(), this.e.longValue(), this.f.longValue(), null);
            }
            throw new IllegalStateException(b.d.b.a.a.v("Missing required properties:", str));
        }
    }

    public r(Double d, int i, boolean z2, int i2, long j, long j2, a aVar) {
        this.a = d;
        this.f1729b = i;
        this.c = z2;
        this.d = i2;
        this.e = j;
        this.f = j2;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.b
    @Nullable
    public Double a() {
        return this.a;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.b
    public int b() {
        return this.f1729b;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.b
    public long c() {
        return this.f;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.b
    public int d() {
        return this.d;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.b
    public long e() {
        return this.e;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v.d.AbstractC0142d.b)) {
            return false;
        }
        v.d.AbstractC0142d.b bVar = (v.d.AbstractC0142d.b) obj;
        Double d = this.a;
        if (d != null ? d.equals(bVar.a()) : bVar.a() == null) {
            if (this.f1729b == bVar.b() && this.c == bVar.f() && this.d == bVar.d() && this.e == bVar.e() && this.f == bVar.c()) {
                return true;
            }
        }
        return false;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.b
    public boolean f() {
        return this.c;
    }

    public int hashCode() {
        Double d = this.a;
        int hashCode = ((((d == null ? 0 : d.hashCode()) ^ 1000003) * 1000003) ^ this.f1729b) * 1000003;
        int i = this.c ? 1231 : 1237;
        long j = this.e;
        long j2 = this.f;
        return ((((((hashCode ^ i) * 1000003) ^ this.d) * 1000003) ^ ((int) (j ^ (j >>> 32)))) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)));
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("Device{batteryLevel=");
        R.append(this.a);
        R.append(", batteryVelocity=");
        R.append(this.f1729b);
        R.append(", proximityOn=");
        R.append(this.c);
        R.append(", orientation=");
        R.append(this.d);
        R.append(", ramUsed=");
        R.append(this.e);
        R.append(", diskUsed=");
        return b.d.b.a.a.B(R, this.f, "}");
    }
}
