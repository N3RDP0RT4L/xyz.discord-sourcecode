package b.i.c.m.d.m;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.c.m.d.m.v;
/* compiled from: AutoValue_CrashlyticsReport.java */
/* loaded from: classes3.dex */
public final class b extends v {

    /* renamed from: b  reason: collision with root package name */
    public final String f1709b;
    public final String c;
    public final int d;
    public final String e;
    public final String f;
    public final String g;
    public final v.d h;
    public final v.c i;

    /* compiled from: AutoValue_CrashlyticsReport.java */
    /* renamed from: b.i.c.m.d.m.b$b  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0140b extends v.a {
        public String a;

        /* renamed from: b  reason: collision with root package name */
        public String f1710b;
        public Integer c;
        public String d;
        public String e;
        public String f;
        public v.d g;
        public v.c h;

        public C0140b() {
        }

        @Override // b.i.c.m.d.m.v.a
        public v a() {
            String str = this.a == null ? " sdkVersion" : "";
            if (this.f1710b == null) {
                str = b.d.b.a.a.v(str, " gmpAppId");
            }
            if (this.c == null) {
                str = b.d.b.a.a.v(str, " platform");
            }
            if (this.d == null) {
                str = b.d.b.a.a.v(str, " installationUuid");
            }
            if (this.e == null) {
                str = b.d.b.a.a.v(str, " buildVersion");
            }
            if (this.f == null) {
                str = b.d.b.a.a.v(str, " displayVersion");
            }
            if (str.isEmpty()) {
                return new b(this.a, this.f1710b, this.c.intValue(), this.d, this.e, this.f, this.g, this.h, null);
            }
            throw new IllegalStateException(b.d.b.a.a.v("Missing required properties:", str));
        }

        public C0140b(v vVar, a aVar) {
            b bVar = (b) vVar;
            this.a = bVar.f1709b;
            this.f1710b = bVar.c;
            this.c = Integer.valueOf(bVar.d);
            this.d = bVar.e;
            this.e = bVar.f;
            this.f = bVar.g;
            this.g = bVar.h;
            this.h = bVar.i;
        }
    }

    public b(String str, String str2, int i, String str3, String str4, String str5, v.d dVar, v.c cVar, a aVar) {
        this.f1709b = str;
        this.c = str2;
        this.d = i;
        this.e = str3;
        this.f = str4;
        this.g = str5;
        this.h = dVar;
        this.i = cVar;
    }

    @Override // b.i.c.m.d.m.v
    @NonNull
    public String a() {
        return this.f;
    }

    @Override // b.i.c.m.d.m.v
    @NonNull
    public String b() {
        return this.g;
    }

    @Override // b.i.c.m.d.m.v
    @NonNull
    public String c() {
        return this.c;
    }

    @Override // b.i.c.m.d.m.v
    @NonNull
    public String d() {
        return this.e;
    }

    @Override // b.i.c.m.d.m.v
    @Nullable
    public v.c e() {
        return this.i;
    }

    public boolean equals(Object obj) {
        v.d dVar;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v)) {
            return false;
        }
        v vVar = (v) obj;
        if (this.f1709b.equals(vVar.g()) && this.c.equals(vVar.c()) && this.d == vVar.f() && this.e.equals(vVar.d()) && this.f.equals(vVar.a()) && this.g.equals(vVar.b()) && ((dVar = this.h) != null ? dVar.equals(vVar.h()) : vVar.h() == null)) {
            v.c cVar = this.i;
            if (cVar == null) {
                if (vVar.e() == null) {
                    return true;
                }
            } else if (cVar.equals(vVar.e())) {
                return true;
            }
        }
        return false;
    }

    @Override // b.i.c.m.d.m.v
    public int f() {
        return this.d;
    }

    @Override // b.i.c.m.d.m.v
    @NonNull
    public String g() {
        return this.f1709b;
    }

    @Override // b.i.c.m.d.m.v
    @Nullable
    public v.d h() {
        return this.h;
    }

    public int hashCode() {
        int hashCode = (((((((((((this.f1709b.hashCode() ^ 1000003) * 1000003) ^ this.c.hashCode()) * 1000003) ^ this.d) * 1000003) ^ this.e.hashCode()) * 1000003) ^ this.f.hashCode()) * 1000003) ^ this.g.hashCode()) * 1000003;
        v.d dVar = this.h;
        int i = 0;
        int hashCode2 = (hashCode ^ (dVar == null ? 0 : dVar.hashCode())) * 1000003;
        v.c cVar = this.i;
        if (cVar != null) {
            i = cVar.hashCode();
        }
        return hashCode2 ^ i;
    }

    @Override // b.i.c.m.d.m.v
    public v.a i() {
        return new C0140b(this, null);
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("CrashlyticsReport{sdkVersion=");
        R.append(this.f1709b);
        R.append(", gmpAppId=");
        R.append(this.c);
        R.append(", platform=");
        R.append(this.d);
        R.append(", installationUuid=");
        R.append(this.e);
        R.append(", buildVersion=");
        R.append(this.f);
        R.append(", displayVersion=");
        R.append(this.g);
        R.append(", session=");
        R.append(this.h);
        R.append(", ndkPayload=");
        R.append(this.i);
        R.append("}");
        return R.toString();
    }
}
