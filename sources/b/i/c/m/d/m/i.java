package b.i.c.m.d.m;

import androidx.annotation.NonNull;
import b.i.c.m.d.m.v;
/* compiled from: AutoValue_CrashlyticsReport_Session_Device.java */
/* loaded from: classes3.dex */
public final class i extends v.d.c {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1717b;
    public final int c;
    public final long d;
    public final long e;
    public final boolean f;
    public final int g;
    public final String h;
    public final String i;

    /* compiled from: AutoValue_CrashlyticsReport_Session_Device.java */
    /* loaded from: classes3.dex */
    public static final class b extends v.d.c.a {
        public Integer a;

        /* renamed from: b  reason: collision with root package name */
        public String f1718b;
        public Integer c;
        public Long d;
        public Long e;
        public Boolean f;
        public Integer g;
        public String h;
        public String i;

        public v.d.c a() {
            String str = this.a == null ? " arch" : "";
            if (this.f1718b == null) {
                str = b.d.b.a.a.v(str, " model");
            }
            if (this.c == null) {
                str = b.d.b.a.a.v(str, " cores");
            }
            if (this.d == null) {
                str = b.d.b.a.a.v(str, " ram");
            }
            if (this.e == null) {
                str = b.d.b.a.a.v(str, " diskSpace");
            }
            if (this.f == null) {
                str = b.d.b.a.a.v(str, " simulator");
            }
            if (this.g == null) {
                str = b.d.b.a.a.v(str, " state");
            }
            if (this.h == null) {
                str = b.d.b.a.a.v(str, " manufacturer");
            }
            if (this.i == null) {
                str = b.d.b.a.a.v(str, " modelClass");
            }
            if (str.isEmpty()) {
                return new i(this.a.intValue(), this.f1718b, this.c.intValue(), this.d.longValue(), this.e.longValue(), this.f.booleanValue(), this.g.intValue(), this.h, this.i, null);
            }
            throw new IllegalStateException(b.d.b.a.a.v("Missing required properties:", str));
        }
    }

    public i(int i, String str, int i2, long j, long j2, boolean z2, int i3, String str2, String str3, a aVar) {
        this.a = i;
        this.f1717b = str;
        this.c = i2;
        this.d = j;
        this.e = j2;
        this.f = z2;
        this.g = i3;
        this.h = str2;
        this.i = str3;
    }

    @Override // b.i.c.m.d.m.v.d.c
    @NonNull
    public int a() {
        return this.a;
    }

    @Override // b.i.c.m.d.m.v.d.c
    public int b() {
        return this.c;
    }

    @Override // b.i.c.m.d.m.v.d.c
    public long c() {
        return this.e;
    }

    @Override // b.i.c.m.d.m.v.d.c
    @NonNull
    public String d() {
        return this.h;
    }

    @Override // b.i.c.m.d.m.v.d.c
    @NonNull
    public String e() {
        return this.f1717b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v.d.c)) {
            return false;
        }
        v.d.c cVar = (v.d.c) obj;
        return this.a == cVar.a() && this.f1717b.equals(cVar.e()) && this.c == cVar.b() && this.d == cVar.g() && this.e == cVar.c() && this.f == cVar.i() && this.g == cVar.h() && this.h.equals(cVar.d()) && this.i.equals(cVar.f());
    }

    @Override // b.i.c.m.d.m.v.d.c
    @NonNull
    public String f() {
        return this.i;
    }

    @Override // b.i.c.m.d.m.v.d.c
    public long g() {
        return this.d;
    }

    @Override // b.i.c.m.d.m.v.d.c
    public int h() {
        return this.g;
    }

    public int hashCode() {
        long j = this.d;
        long j2 = this.e;
        return ((((((((((((((((this.a ^ 1000003) * 1000003) ^ this.f1717b.hashCode()) * 1000003) ^ this.c) * 1000003) ^ ((int) (j ^ (j >>> 32)))) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)))) * 1000003) ^ (this.f ? 1231 : 1237)) * 1000003) ^ this.g) * 1000003) ^ this.h.hashCode()) * 1000003) ^ this.i.hashCode();
    }

    @Override // b.i.c.m.d.m.v.d.c
    public boolean i() {
        return this.f;
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("Device{arch=");
        R.append(this.a);
        R.append(", model=");
        R.append(this.f1717b);
        R.append(", cores=");
        R.append(this.c);
        R.append(", ram=");
        R.append(this.d);
        R.append(", diskSpace=");
        R.append(this.e);
        R.append(", simulator=");
        R.append(this.f);
        R.append(", state=");
        R.append(this.g);
        R.append(", manufacturer=");
        R.append(this.h);
        R.append(", modelClass=");
        return b.d.b.a.a.H(R, this.i, "}");
    }
}
