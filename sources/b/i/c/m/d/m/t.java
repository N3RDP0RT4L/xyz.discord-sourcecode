package b.i.c.m.d.m;

import androidx.annotation.NonNull;
import b.i.c.m.d.m.v;
/* compiled from: AutoValue_CrashlyticsReport_Session_OperatingSystem.java */
/* loaded from: classes3.dex */
public final class t extends v.d.e {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1731b;
    public final String c;
    public final boolean d;

    public t(int i, String str, String str2, boolean z2, a aVar) {
        this.a = i;
        this.f1731b = str;
        this.c = str2;
        this.d = z2;
    }

    @Override // b.i.c.m.d.m.v.d.e
    @NonNull
    public String a() {
        return this.c;
    }

    @Override // b.i.c.m.d.m.v.d.e
    public int b() {
        return this.a;
    }

    @Override // b.i.c.m.d.m.v.d.e
    @NonNull
    public String c() {
        return this.f1731b;
    }

    @Override // b.i.c.m.d.m.v.d.e
    public boolean d() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v.d.e)) {
            return false;
        }
        v.d.e eVar = (v.d.e) obj;
        return this.a == eVar.b() && this.f1731b.equals(eVar.c()) && this.c.equals(eVar.a()) && this.d == eVar.d();
    }

    public int hashCode() {
        return ((((((this.a ^ 1000003) * 1000003) ^ this.f1731b.hashCode()) * 1000003) ^ this.c.hashCode()) * 1000003) ^ (this.d ? 1231 : 1237);
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("OperatingSystem{platform=");
        R.append(this.a);
        R.append(", version=");
        R.append(this.f1731b);
        R.append(", buildVersion=");
        R.append(this.c);
        R.append(", jailbroken=");
        return b.d.b.a.a.M(R, this.d, "}");
    }
}
