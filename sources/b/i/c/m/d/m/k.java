package b.i.c.m.d.m;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.c.m.d.m.v;
/* compiled from: AutoValue_CrashlyticsReport_Session_Event_Application.java */
/* loaded from: classes3.dex */
public final class k extends v.d.AbstractC0142d.a {
    public final v.d.AbstractC0142d.a.b a;

    /* renamed from: b  reason: collision with root package name */
    public final w<v.b> f1720b;
    public final Boolean c;
    public final int d;

    /* compiled from: AutoValue_CrashlyticsReport_Session_Event_Application.java */
    /* loaded from: classes3.dex */
    public static final class b extends v.d.AbstractC0142d.a.AbstractC0143a {
        public v.d.AbstractC0142d.a.b a;

        /* renamed from: b  reason: collision with root package name */
        public w<v.b> f1721b;
        public Boolean c;
        public Integer d;

        public b() {
        }

        public v.d.AbstractC0142d.a a() {
            String str = this.a == null ? " execution" : "";
            if (this.d == null) {
                str = b.d.b.a.a.v(str, " uiOrientation");
            }
            if (str.isEmpty()) {
                return new k(this.a, this.f1721b, this.c, this.d.intValue(), null);
            }
            throw new IllegalStateException(b.d.b.a.a.v("Missing required properties:", str));
        }

        public b(v.d.AbstractC0142d.a aVar, a aVar2) {
            k kVar = (k) aVar;
            this.a = kVar.a;
            this.f1721b = kVar.f1720b;
            this.c = kVar.c;
            this.d = Integer.valueOf(kVar.d);
        }
    }

    public k(v.d.AbstractC0142d.a.b bVar, w wVar, Boolean bool, int i, a aVar) {
        this.a = bVar;
        this.f1720b = wVar;
        this.c = bool;
        this.d = i;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a
    @Nullable
    public Boolean a() {
        return this.c;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a
    @Nullable
    public w<v.b> b() {
        return this.f1720b;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a
    @NonNull
    public v.d.AbstractC0142d.a.b c() {
        return this.a;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a
    public int d() {
        return this.d;
    }

    public v.d.AbstractC0142d.a.AbstractC0143a e() {
        return new b(this, null);
    }

    public boolean equals(Object obj) {
        w<v.b> wVar;
        Boolean bool;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v.d.AbstractC0142d.a)) {
            return false;
        }
        v.d.AbstractC0142d.a aVar = (v.d.AbstractC0142d.a) obj;
        return this.a.equals(aVar.c()) && ((wVar = this.f1720b) != null ? wVar.equals(aVar.b()) : aVar.b() == null) && ((bool = this.c) != null ? bool.equals(aVar.a()) : aVar.a() == null) && this.d == aVar.d();
    }

    public int hashCode() {
        int hashCode = (this.a.hashCode() ^ 1000003) * 1000003;
        w<v.b> wVar = this.f1720b;
        int i = 0;
        int hashCode2 = (hashCode ^ (wVar == null ? 0 : wVar.hashCode())) * 1000003;
        Boolean bool = this.c;
        if (bool != null) {
            i = bool.hashCode();
        }
        return ((hashCode2 ^ i) * 1000003) ^ this.d;
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("Application{execution=");
        R.append(this.a);
        R.append(", customAttributes=");
        R.append(this.f1720b);
        R.append(", background=");
        R.append(this.c);
        R.append(", uiOrientation=");
        return b.d.b.a.a.A(R, this.d, "}");
    }
}
