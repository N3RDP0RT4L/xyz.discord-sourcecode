package b.i.c.m.d.m;

import androidx.annotation.NonNull;
import b.i.c.m.d.m.v;
/* compiled from: AutoValue_CrashlyticsReport_Session_Event_Application_Execution_Signal.java */
/* loaded from: classes3.dex */
public final class o extends v.d.AbstractC0142d.a.b.c {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1725b;
    public final long c;

    public o(String str, String str2, long j, a aVar) {
        this.a = str;
        this.f1725b = str2;
        this.c = j;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a.b.c
    @NonNull
    public long a() {
        return this.c;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a.b.c
    @NonNull
    public String b() {
        return this.f1725b;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a.b.c
    @NonNull
    public String c() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v.d.AbstractC0142d.a.b.c)) {
            return false;
        }
        v.d.AbstractC0142d.a.b.c cVar = (v.d.AbstractC0142d.a.b.c) obj;
        return this.a.equals(cVar.c()) && this.f1725b.equals(cVar.b()) && this.c == cVar.a();
    }

    public int hashCode() {
        long j = this.c;
        return ((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.f1725b.hashCode()) * 1000003) ^ ((int) (j ^ (j >>> 32)));
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("Signal{name=");
        R.append(this.a);
        R.append(", code=");
        R.append(this.f1725b);
        R.append(", address=");
        return b.d.b.a.a.B(R, this.c, "}");
    }
}
