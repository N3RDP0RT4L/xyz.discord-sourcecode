package b.i.c.m.d.m;

import androidx.annotation.NonNull;
import b.i.c.m.d.m.v;
/* compiled from: AutoValue_CrashlyticsReport_Session_Event_Application_Execution_Thread.java */
/* loaded from: classes3.dex */
public final class p extends v.d.AbstractC0142d.a.b.AbstractC0146d {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final int f1726b;
    public final w<v.d.AbstractC0142d.a.b.AbstractC0146d.AbstractC0147a> c;

    public p(String str, int i, w wVar, a aVar) {
        this.a = str;
        this.f1726b = i;
        this.c = wVar;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a.b.AbstractC0146d
    @NonNull
    public w<v.d.AbstractC0142d.a.b.AbstractC0146d.AbstractC0147a> a() {
        return this.c;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a.b.AbstractC0146d
    public int b() {
        return this.f1726b;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a.b.AbstractC0146d
    @NonNull
    public String c() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v.d.AbstractC0142d.a.b.AbstractC0146d)) {
            return false;
        }
        v.d.AbstractC0142d.a.b.AbstractC0146d dVar = (v.d.AbstractC0142d.a.b.AbstractC0146d) obj;
        return this.a.equals(dVar.c()) && this.f1726b == dVar.b() && this.c.equals(dVar.a());
    }

    public int hashCode() {
        return ((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.f1726b) * 1000003) ^ this.c.hashCode();
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("Thread{name=");
        R.append(this.a);
        R.append(", importance=");
        R.append(this.f1726b);
        R.append(", frames=");
        R.append(this.c);
        R.append("}");
        return R.toString();
    }
}
