package b.i.c.m.d.m;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.c.m.d.m.v;
/* compiled from: AutoValue_CrashlyticsReport_Session_Event_Application_Execution_BinaryImage.java */
/* loaded from: classes3.dex */
public final class m extends v.d.AbstractC0142d.a.b.AbstractC0144a {
    public final long a;

    /* renamed from: b  reason: collision with root package name */
    public final long f1723b;
    public final String c;
    public final String d;

    public m(long j, long j2, String str, String str2, a aVar) {
        this.a = j;
        this.f1723b = j2;
        this.c = str;
        this.d = str2;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a.b.AbstractC0144a
    @NonNull
    public long a() {
        return this.a;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a.b.AbstractC0144a
    @NonNull
    public String b() {
        return this.c;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a.b.AbstractC0144a
    public long c() {
        return this.f1723b;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a.b.AbstractC0144a
    @Nullable
    public String d() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v.d.AbstractC0142d.a.b.AbstractC0144a)) {
            return false;
        }
        v.d.AbstractC0142d.a.b.AbstractC0144a aVar = (v.d.AbstractC0142d.a.b.AbstractC0144a) obj;
        if (this.a == aVar.a() && this.f1723b == aVar.c() && this.c.equals(aVar.b())) {
            String str = this.d;
            if (str == null) {
                if (aVar.d() == null) {
                    return true;
                }
            } else if (str.equals(aVar.d())) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        long j = this.a;
        long j2 = this.f1723b;
        int hashCode = (((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ ((int) ((j2 >>> 32) ^ j2))) * 1000003) ^ this.c.hashCode()) * 1000003;
        String str = this.d;
        return (str == null ? 0 : str.hashCode()) ^ hashCode;
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("BinaryImage{baseAddress=");
        R.append(this.a);
        R.append(", size=");
        R.append(this.f1723b);
        R.append(", name=");
        R.append(this.c);
        R.append(", uuid=");
        return b.d.b.a.a.H(R, this.d, "}");
    }
}
