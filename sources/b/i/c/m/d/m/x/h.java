package b.i.c.m.d.m.x;

import android.util.Base64;
import android.util.JsonReader;
import androidx.annotation.NonNull;
import b.i.c.m.d.m.b;
import b.i.c.m.d.m.f;
import b.i.c.m.d.m.g;
import b.i.c.m.d.m.i;
import b.i.c.m.d.m.n;
import b.i.c.m.d.m.q;
import b.i.c.m.d.m.t;
import b.i.c.m.d.m.u;
import b.i.c.m.d.m.v;
import b.i.c.m.d.m.w;
import b.i.c.p.h.d;
import b.i.c.p.h.e;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.analytics.ChatInputComponentTypes;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Objects;
/* compiled from: CrashlyticsReportJsonTransform.java */
/* loaded from: classes3.dex */
public class h {
    public static final b.i.c.p.a a;

    /* compiled from: CrashlyticsReportJsonTransform.java */
    /* loaded from: classes3.dex */
    public interface a<T> {
        T a(@NonNull JsonReader jsonReader) throws IOException;
    }

    static {
        e eVar = new e();
        ((b.i.c.m.d.m.a) b.i.c.m.d.m.a.a).a(eVar);
        eVar.e = true;
        a = new d(eVar);
    }

    @NonNull
    public static <T> w<T> a(@NonNull JsonReader jsonReader, @NonNull a<T> aVar) throws IOException {
        ArrayList arrayList = new ArrayList();
        jsonReader.beginArray();
        while (jsonReader.hasNext()) {
            arrayList.add(aVar.a(jsonReader));
        }
        jsonReader.endArray();
        return new w<>(arrayList);
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:263:0x0209 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:269:0x01e9 A[SYNTHETIC] */
    /* JADX WARN: Type inference failed for: r2v1, types: [b.i.c.m.d.m.s$a] */
    /* JADX WARN: Type inference failed for: r2v3 */
    /* JADX WARN: Type inference failed for: r2v7 */
    /* JADX WARN: Type inference failed for: r2v9, types: [java.lang.Integer] */
    @androidx.annotation.NonNull
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static b.i.c.m.d.m.v.d.AbstractC0142d b(@androidx.annotation.NonNull android.util.JsonReader r26) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 1052
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.c.m.d.m.x.h.b(android.util.JsonReader):b.i.c.m.d.m.v$d$d");
    }

    @NonNull
    public static v.d.AbstractC0142d.a.b.AbstractC0145b c(@NonNull JsonReader jsonReader) throws IOException {
        jsonReader.beginObject();
        Integer num = null;
        String str = null;
        String str2 = null;
        w wVar = null;
        v.d.AbstractC0142d.a.b.AbstractC0145b bVar = null;
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            nextName.hashCode();
            nextName.hashCode();
            char c = 65535;
            switch (nextName.hashCode()) {
                case -1266514778:
                    if (nextName.equals("frames")) {
                        c = 0;
                        break;
                    }
                    break;
                case -934964668:
                    if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_REASON)) {
                        c = 1;
                        break;
                    }
                    break;
                case 3575610:
                    if (nextName.equals("type")) {
                        c = 2;
                        break;
                    }
                    break;
                case 91997906:
                    if (nextName.equals("causedBy")) {
                        c = 3;
                        break;
                    }
                    break;
                case 581754413:
                    if (nextName.equals("overflowCount")) {
                        c = 4;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    wVar = a(jsonReader, f.a);
                    break;
                case 1:
                    str2 = jsonReader.nextString();
                    break;
                case 2:
                    str = jsonReader.nextString();
                    Objects.requireNonNull(str, "Null type");
                    break;
                case 3:
                    bVar = c(jsonReader);
                    break;
                case 4:
                    num = Integer.valueOf(jsonReader.nextInt());
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }
        }
        jsonReader.endObject();
        String str3 = str == null ? " type" : "";
        if (wVar == null) {
            str3 = b.d.b.a.a.v(str3, " frames");
        }
        if (num == null) {
            str3 = b.d.b.a.a.v(str3, " overflowCount");
        }
        if (str3.isEmpty()) {
            return new n(str, str2, wVar, bVar, num.intValue(), null);
        }
        throw new IllegalStateException(b.d.b.a.a.v("Missing required properties:", str3));
    }

    @NonNull
    public static v.d.AbstractC0142d.a.b.AbstractC0146d.AbstractC0147a d(@NonNull JsonReader jsonReader) throws IOException {
        q.b bVar = new q.b();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            nextName.hashCode();
            char c = 65535;
            switch (nextName.hashCode()) {
                case -1019779949:
                    if (nextName.equals("offset")) {
                        c = 0;
                        break;
                    }
                    break;
                case -887523944:
                    if (nextName.equals("symbol")) {
                        c = 1;
                        break;
                    }
                    break;
                case 3571:
                    if (nextName.equals("pc")) {
                        c = 2;
                        break;
                    }
                    break;
                case 3143036:
                    if (nextName.equals("file")) {
                        c = 3;
                        break;
                    }
                    break;
                case 2125650548:
                    if (nextName.equals("importance")) {
                        c = 4;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    bVar.d = Long.valueOf(jsonReader.nextLong());
                    break;
                case 1:
                    String nextString = jsonReader.nextString();
                    Objects.requireNonNull(nextString, "Null symbol");
                    bVar.f1728b = nextString;
                    break;
                case 2:
                    bVar.a = Long.valueOf(jsonReader.nextLong());
                    break;
                case 3:
                    bVar.c = jsonReader.nextString();
                    break;
                case 4:
                    bVar.e = Integer.valueOf(jsonReader.nextInt());
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }
        }
        jsonReader.endObject();
        return bVar.a();
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    @NonNull
    public static v e(@NonNull JsonReader jsonReader) throws IOException {
        char c;
        char c2;
        String str;
        char c3;
        char c4;
        String str2;
        char c5;
        Charset charset = v.a;
        b.C0140b bVar = new b.C0140b();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            nextName.hashCode();
            switch (nextName.hashCode()) {
                case -2118372775:
                    if (nextName.equals("ndkPayload")) {
                        c = 0;
                        break;
                    }
                    c = 65535;
                    break;
                case -1962630338:
                    if (nextName.equals("sdkVersion")) {
                        c = 1;
                        break;
                    }
                    c = 65535;
                    break;
                case -911706486:
                    if (nextName.equals("buildVersion")) {
                        c = 2;
                        break;
                    }
                    c = 65535;
                    break;
                case 344431858:
                    if (nextName.equals("gmpAppId")) {
                        c = 3;
                        break;
                    }
                    c = 65535;
                    break;
                case 719853845:
                    if (nextName.equals("installationUuid")) {
                        c = 4;
                        break;
                    }
                    c = 65535;
                    break;
                case 1874684019:
                    if (nextName.equals("platform")) {
                        c = 5;
                        break;
                    }
                    c = 65535;
                    break;
                case 1975623094:
                    if (nextName.equals("displayVersion")) {
                        c = 6;
                        break;
                    }
                    c = 65535;
                    break;
                case 1984987798:
                    if (nextName.equals("session")) {
                        c = 7;
                        break;
                    }
                    c = 65535;
                    break;
                default:
                    c = 65535;
                    break;
            }
            String str3 = "";
            switch (c) {
                case 0:
                    jsonReader.beginObject();
                    String str4 = null;
                    w wVar = null;
                    while (jsonReader.hasNext()) {
                        String nextName2 = jsonReader.nextName();
                        nextName2.hashCode();
                        if (nextName2.equals(ChatInputComponentTypes.FILES)) {
                            wVar = a(jsonReader, b.a);
                        } else if (!nextName2.equals("orgId")) {
                            jsonReader.skipValue();
                        } else {
                            str4 = jsonReader.nextString();
                        }
                    }
                    jsonReader.endObject();
                    String str5 = wVar == null ? " files" : str3;
                    if (str5.isEmpty()) {
                        bVar.h = new b.i.c.m.d.m.d(wVar, str4, null);
                        break;
                    } else {
                        throw new IllegalStateException(b.d.b.a.a.v("Missing required properties:", str5));
                    }
                case 1:
                    String nextString = jsonReader.nextString();
                    Objects.requireNonNull(nextString, "Null sdkVersion");
                    bVar.a = nextString;
                    break;
                case 2:
                    String nextString2 = jsonReader.nextString();
                    Objects.requireNonNull(nextString2, "Null buildVersion");
                    bVar.e = nextString2;
                    break;
                case 3:
                    String nextString3 = jsonReader.nextString();
                    Objects.requireNonNull(nextString3, "Null gmpAppId");
                    bVar.f1710b = nextString3;
                    break;
                case 4:
                    String nextString4 = jsonReader.nextString();
                    Objects.requireNonNull(nextString4, "Null installationUuid");
                    bVar.d = nextString4;
                    break;
                case 5:
                    bVar.c = Integer.valueOf(jsonReader.nextInt());
                    break;
                case 6:
                    String nextString5 = jsonReader.nextString();
                    Objects.requireNonNull(nextString5, "Null displayVersion");
                    bVar.f = nextString5;
                    break;
                case 7:
                    f.b bVar2 = new f.b();
                    bVar2.b(false);
                    jsonReader.beginObject();
                    while (jsonReader.hasNext()) {
                        String nextName3 = jsonReader.nextName();
                        nextName3.hashCode();
                        switch (nextName3.hashCode()) {
                            case -2128794476:
                                if (nextName3.equals("startedAt")) {
                                    c2 = 0;
                                    break;
                                }
                                c2 = 65535;
                                break;
                            case -1618432855:
                                if (nextName3.equals("identifier")) {
                                    c2 = 1;
                                    break;
                                }
                                c2 = 65535;
                                break;
                            case -1606742899:
                                if (nextName3.equals("endedAt")) {
                                    c2 = 2;
                                    break;
                                }
                                c2 = 65535;
                                break;
                            case -1335157162:
                                if (nextName3.equals("device")) {
                                    c2 = 3;
                                    break;
                                }
                                c2 = 65535;
                                break;
                            case -1291329255:
                                if (nextName3.equals("events")) {
                                    c2 = 4;
                                    break;
                                }
                                c2 = 65535;
                                break;
                            case 3556:
                                if (nextName3.equals("os")) {
                                    c2 = 5;
                                    break;
                                }
                                c2 = 65535;
                                break;
                            case 96801:
                                if (nextName3.equals("app")) {
                                    c2 = 6;
                                    break;
                                }
                                c2 = 65535;
                                break;
                            case 3599307:
                                if (nextName3.equals("user")) {
                                    c2 = 7;
                                    break;
                                }
                                c2 = 65535;
                                break;
                            case 286956243:
                                if (nextName3.equals("generator")) {
                                    c2 = '\b';
                                    break;
                                }
                                c2 = 65535;
                                break;
                            case 1025385094:
                                if (nextName3.equals("crashed")) {
                                    c2 = '\t';
                                    break;
                                }
                                c2 = 65535;
                                break;
                            case 2047016109:
                                if (nextName3.equals("generatorType")) {
                                    c2 = '\n';
                                    break;
                                }
                                c2 = 65535;
                                break;
                            default:
                                c2 = 65535;
                                break;
                        }
                        switch (c2) {
                            case 0:
                                str = str3;
                                bVar2.c = Long.valueOf(jsonReader.nextLong());
                                break;
                            case 1:
                                str = str3;
                                bVar2.f1715b = new String(Base64.decode(jsonReader.nextString(), 2), v.a);
                                break;
                            case 2:
                                str = str3;
                                bVar2.d = Long.valueOf(jsonReader.nextLong());
                                break;
                            case 3:
                                str = str3;
                                i.b bVar3 = new i.b();
                                jsonReader.beginObject();
                                while (jsonReader.hasNext()) {
                                    String nextName4 = jsonReader.nextName();
                                    nextName4.hashCode();
                                    switch (nextName4.hashCode()) {
                                        case -1981332476:
                                            if (nextName4.equals("simulator")) {
                                                c3 = 0;
                                                break;
                                            }
                                            c3 = 65535;
                                            break;
                                        case -1969347631:
                                            if (nextName4.equals("manufacturer")) {
                                                c3 = 1;
                                                break;
                                            }
                                            c3 = 65535;
                                            break;
                                        case 112670:
                                            if (nextName4.equals("ram")) {
                                                c3 = 2;
                                                break;
                                            }
                                            c3 = 65535;
                                            break;
                                        case 3002454:
                                            if (nextName4.equals("arch")) {
                                                c3 = 3;
                                                break;
                                            }
                                            c3 = 65535;
                                            break;
                                        case 81784169:
                                            if (nextName4.equals("diskSpace")) {
                                                c3 = 4;
                                                break;
                                            }
                                            c3 = 65535;
                                            break;
                                        case 94848180:
                                            if (nextName4.equals("cores")) {
                                                c3 = 5;
                                                break;
                                            }
                                            c3 = 65535;
                                            break;
                                        case 104069929:
                                            if (nextName4.equals("model")) {
                                                c3 = 6;
                                                break;
                                            }
                                            c3 = 65535;
                                            break;
                                        case 109757585:
                                            if (nextName4.equals("state")) {
                                                c3 = 7;
                                                break;
                                            }
                                            c3 = 65535;
                                            break;
                                        case 2078953423:
                                            if (nextName4.equals("modelClass")) {
                                                c3 = '\b';
                                                break;
                                            }
                                            c3 = 65535;
                                            break;
                                        default:
                                            c3 = 65535;
                                            break;
                                    }
                                    switch (c3) {
                                        case 0:
                                            bVar3.f = Boolean.valueOf(jsonReader.nextBoolean());
                                            break;
                                        case 1:
                                            String nextString6 = jsonReader.nextString();
                                            Objects.requireNonNull(nextString6, "Null manufacturer");
                                            bVar3.h = nextString6;
                                            break;
                                        case 2:
                                            bVar3.d = Long.valueOf(jsonReader.nextLong());
                                            break;
                                        case 3:
                                            bVar3.a = Integer.valueOf(jsonReader.nextInt());
                                            break;
                                        case 4:
                                            bVar3.e = Long.valueOf(jsonReader.nextLong());
                                            break;
                                        case 5:
                                            bVar3.c = Integer.valueOf(jsonReader.nextInt());
                                            break;
                                        case 6:
                                            String nextString7 = jsonReader.nextString();
                                            Objects.requireNonNull(nextString7, "Null model");
                                            bVar3.f1718b = nextString7;
                                            break;
                                        case 7:
                                            bVar3.g = Integer.valueOf(jsonReader.nextInt());
                                            break;
                                        case '\b':
                                            String nextString8 = jsonReader.nextString();
                                            Objects.requireNonNull(nextString8, "Null modelClass");
                                            bVar3.i = nextString8;
                                            break;
                                        default:
                                            jsonReader.skipValue();
                                            break;
                                    }
                                }
                                jsonReader.endObject();
                                bVar2.i = bVar3.a();
                                break;
                            case 4:
                                str = str3;
                                bVar2.j = a(jsonReader, b.i.c.m.d.m.x.a.a);
                                break;
                            case 5:
                                str = str3;
                                jsonReader.beginObject();
                                Boolean bool = null;
                                Integer num = null;
                                String str6 = null;
                                String str7 = null;
                                while (jsonReader.hasNext()) {
                                    String nextName5 = jsonReader.nextName();
                                    nextName5.hashCode();
                                    switch (nextName5.hashCode()) {
                                        case -911706486:
                                            if (nextName5.equals("buildVersion")) {
                                                c4 = 0;
                                                break;
                                            }
                                            c4 = 65535;
                                            break;
                                        case -293026577:
                                            if (nextName5.equals("jailbroken")) {
                                                c4 = 1;
                                                break;
                                            }
                                            c4 = 65535;
                                            break;
                                        case 351608024:
                                            if (nextName5.equals("version")) {
                                                c4 = 2;
                                                break;
                                            }
                                            c4 = 65535;
                                            break;
                                        case 1874684019:
                                            if (nextName5.equals("platform")) {
                                                c4 = 3;
                                                break;
                                            }
                                            c4 = 65535;
                                            break;
                                        default:
                                            c4 = 65535;
                                            break;
                                    }
                                    if (c4 == 0) {
                                        String nextString9 = jsonReader.nextString();
                                        Objects.requireNonNull(nextString9, "Null buildVersion");
                                        str7 = nextString9;
                                    } else if (c4 == 1) {
                                        bool = Boolean.valueOf(jsonReader.nextBoolean());
                                    } else if (c4 == 2) {
                                        String nextString10 = jsonReader.nextString();
                                        Objects.requireNonNull(nextString10, "Null version");
                                        str6 = nextString10;
                                    } else if (c4 != 3) {
                                        jsonReader.skipValue();
                                    } else {
                                        num = Integer.valueOf(jsonReader.nextInt());
                                    }
                                }
                                jsonReader.endObject();
                                String str8 = num == null ? " platform" : str;
                                if (str6 == null) {
                                    str8 = b.d.b.a.a.v(str8, " version");
                                }
                                if (str7 == null) {
                                    str8 = b.d.b.a.a.v(str8, " buildVersion");
                                }
                                if (bool == null) {
                                    str8 = b.d.b.a.a.v(str8, " jailbroken");
                                }
                                if (str8.isEmpty()) {
                                    bVar2.h = new t(num.intValue(), str6, str7, bool.booleanValue(), null);
                                    break;
                                } else {
                                    throw new IllegalStateException(b.d.b.a.a.v("Missing required properties:", str8));
                                }
                            case 6:
                                jsonReader.beginObject();
                                String str9 = null;
                                String str10 = null;
                                String str11 = null;
                                String str12 = null;
                                String str13 = null;
                                String str14 = null;
                                while (jsonReader.hasNext()) {
                                    String nextName6 = jsonReader.nextName();
                                    nextName6.hashCode();
                                    switch (nextName6.hashCode()) {
                                        case -1618432855:
                                            str2 = str3;
                                            if (nextName6.equals("identifier")) {
                                                c5 = 0;
                                                break;
                                            }
                                            c5 = 65535;
                                            break;
                                        case -519438642:
                                            str2 = str3;
                                            if (nextName6.equals("developmentPlatform")) {
                                                c5 = 1;
                                                break;
                                            }
                                            c5 = 65535;
                                            break;
                                        case 213652010:
                                            str2 = str3;
                                            if (nextName6.equals("developmentPlatformVersion")) {
                                                c5 = 2;
                                                break;
                                            }
                                            c5 = 65535;
                                            break;
                                        case 351608024:
                                            if (nextName6.equals("version")) {
                                                c5 = 3;
                                                str2 = str3;
                                                break;
                                            }
                                            str2 = str3;
                                            c5 = 65535;
                                            break;
                                        case 719853845:
                                            if (nextName6.equals("installationUuid")) {
                                                c5 = 4;
                                                str2 = str3;
                                                break;
                                            }
                                            str2 = str3;
                                            c5 = 65535;
                                            break;
                                        case 1975623094:
                                            if (nextName6.equals("displayVersion")) {
                                                c5 = 5;
                                                str2 = str3;
                                                break;
                                            }
                                            str2 = str3;
                                            c5 = 65535;
                                            break;
                                        default:
                                            str2 = str3;
                                            c5 = 65535;
                                            break;
                                    }
                                    if (c5 == 0) {
                                        String nextString11 = jsonReader.nextString();
                                        Objects.requireNonNull(nextString11, "Null identifier");
                                        str9 = nextString11;
                                    } else if (c5 == 1) {
                                        str13 = jsonReader.nextString();
                                    } else if (c5 == 2) {
                                        str14 = jsonReader.nextString();
                                    } else if (c5 == 3) {
                                        String nextString12 = jsonReader.nextString();
                                        Objects.requireNonNull(nextString12, "Null version");
                                        str10 = nextString12;
                                    } else if (c5 == 4) {
                                        str12 = jsonReader.nextString();
                                    } else if (c5 != 5) {
                                        jsonReader.skipValue();
                                    } else {
                                        str11 = jsonReader.nextString();
                                    }
                                    str3 = str2;
                                }
                                str = str3;
                                jsonReader.endObject();
                                String str15 = str9 == null ? " identifier" : str;
                                if (str10 == null) {
                                    str15 = b.d.b.a.a.v(str15, " version");
                                }
                                if (str15.isEmpty()) {
                                    bVar2.f = new g(str9, str10, str11, null, str12, str13, str14, null);
                                    break;
                                } else {
                                    throw new IllegalStateException(b.d.b.a.a.v("Missing required properties:", str15));
                                }
                            case 7:
                                jsonReader.beginObject();
                                String str16 = null;
                                while (jsonReader.hasNext()) {
                                    String nextName7 = jsonReader.nextName();
                                    nextName7.hashCode();
                                    if (!nextName7.equals("identifier")) {
                                        jsonReader.skipValue();
                                    } else {
                                        str16 = jsonReader.nextString();
                                        Objects.requireNonNull(str16, "Null identifier");
                                    }
                                }
                                jsonReader.endObject();
                                String str17 = str16 == null ? " identifier" : str3;
                                if (str17.isEmpty()) {
                                    bVar2.g = new u(str16, null);
                                    str = str3;
                                    break;
                                } else {
                                    throw new IllegalStateException(b.d.b.a.a.v("Missing required properties:", str17));
                                }
                            case '\b':
                                String nextString13 = jsonReader.nextString();
                                Objects.requireNonNull(nextString13, "Null generator");
                                bVar2.a = nextString13;
                                str = str3;
                                break;
                            case '\t':
                                bVar2.b(jsonReader.nextBoolean());
                                str = str3;
                                break;
                            case '\n':
                                bVar2.k = Integer.valueOf(jsonReader.nextInt());
                                str = str3;
                                break;
                            default:
                                str = str3;
                                jsonReader.skipValue();
                                break;
                        }
                        str3 = str;
                    }
                    jsonReader.endObject();
                    bVar.g = bVar2.a();
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }
        }
        jsonReader.endObject();
        return bVar.a();
    }

    @NonNull
    public v f(@NonNull String str) throws IOException {
        try {
            JsonReader jsonReader = new JsonReader(new StringReader(str));
            v e = e(jsonReader);
            jsonReader.close();
            return e;
        } catch (IllegalStateException e2) {
            throw new IOException(e2);
        }
    }

    @NonNull
    public String g(@NonNull v vVar) {
        return ((d) a).a(vVar);
    }
}
