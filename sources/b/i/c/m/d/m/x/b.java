package b.i.c.m.d.m.x;

import android.util.Base64;
import android.util.JsonReader;
import b.i.c.m.d.m.e;
import b.i.c.m.d.m.x.h;
import b.i.c.p.a;
import java.util.Objects;
/* compiled from: CrashlyticsReportJsonTransform.java */
/* loaded from: classes3.dex */
public final /* synthetic */ class b implements h.a {
    public static final b a = new b();

    @Override // b.i.c.m.d.m.x.h.a
    public Object a(JsonReader jsonReader) {
        a aVar = h.a;
        jsonReader.beginObject();
        String str = null;
        byte[] bArr = null;
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            nextName.hashCode();
            if (nextName.equals("filename")) {
                str = jsonReader.nextString();
                Objects.requireNonNull(str, "Null filename");
            } else if (!nextName.equals("contents")) {
                jsonReader.skipValue();
            } else {
                bArr = Base64.decode(jsonReader.nextString(), 2);
                Objects.requireNonNull(bArr, "Null contents");
            }
        }
        jsonReader.endObject();
        String str2 = str == null ? " filename" : "";
        if (bArr == null) {
            str2 = b.d.b.a.a.v(str2, " contents");
        }
        if (str2.isEmpty()) {
            return new e(str, bArr, null);
        }
        throw new IllegalStateException(b.d.b.a.a.v("Missing required properties:", str2));
    }
}
