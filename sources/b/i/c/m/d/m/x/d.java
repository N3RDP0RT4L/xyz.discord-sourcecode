package b.i.c.m.d.m.x;

import android.util.JsonReader;
import b.i.c.m.d.m.p;
import b.i.c.m.d.m.w;
import b.i.c.m.d.m.x.h;
import b.i.c.p.a;
import com.discord.models.domain.ModelAuditLogEntry;
import java.util.Objects;
/* compiled from: CrashlyticsReportJsonTransform.java */
/* loaded from: classes3.dex */
public final /* synthetic */ class d implements h.a {
    public static final d a = new d();

    @Override // b.i.c.m.d.m.x.h.a
    public Object a(JsonReader jsonReader) {
        a aVar = h.a;
        jsonReader.beginObject();
        String str = null;
        Integer num = null;
        w wVar = null;
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            nextName.hashCode();
            nextName.hashCode();
            char c = 65535;
            switch (nextName.hashCode()) {
                case -1266514778:
                    if (nextName.equals("frames")) {
                        c = 0;
                        break;
                    }
                    break;
                case 3373707:
                    if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_NAME)) {
                        c = 1;
                        break;
                    }
                    break;
                case 2125650548:
                    if (nextName.equals("importance")) {
                        c = 2;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    wVar = h.a(jsonReader, g.a);
                    break;
                case 1:
                    str = jsonReader.nextString();
                    Objects.requireNonNull(str, "Null name");
                    break;
                case 2:
                    num = Integer.valueOf(jsonReader.nextInt());
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }
        }
        jsonReader.endObject();
        String str2 = str == null ? " name" : "";
        if (num == null) {
            str2 = b.d.b.a.a.v(str2, " importance");
        }
        if (wVar == null) {
            str2 = b.d.b.a.a.v(str2, " frames");
        }
        if (str2.isEmpty()) {
            return new p(str, num.intValue(), wVar, null);
        }
        throw new IllegalStateException(b.d.b.a.a.v("Missing required properties:", str2));
    }
}
