package b.i.c.m.d.m.x;

import android.util.Base64;
import android.util.JsonReader;
import b.i.c.m.d.m.m;
import b.i.c.m.d.m.v;
import b.i.c.m.d.m.x.h;
import b.i.c.p.a;
import com.discord.models.domain.ModelAuditLogEntry;
import java.util.Objects;
/* compiled from: CrashlyticsReportJsonTransform.java */
/* loaded from: classes3.dex */
public final /* synthetic */ class e implements h.a {
    public static final e a = new e();

    @Override // b.i.c.m.d.m.x.h.a
    public Object a(JsonReader jsonReader) {
        a aVar = h.a;
        jsonReader.beginObject();
        Long l = null;
        Long l2 = null;
        String str = null;
        String str2 = null;
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            nextName.hashCode();
            nextName.hashCode();
            char c = 65535;
            switch (nextName.hashCode()) {
                case 3373707:
                    if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_NAME)) {
                        c = 0;
                        break;
                    }
                    break;
                case 3530753:
                    if (nextName.equals("size")) {
                        c = 1;
                        break;
                    }
                    break;
                case 3601339:
                    if (nextName.equals("uuid")) {
                        c = 2;
                        break;
                    }
                    break;
                case 1153765347:
                    if (nextName.equals("baseAddress")) {
                        c = 3;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    str = jsonReader.nextString();
                    Objects.requireNonNull(str, "Null name");
                    break;
                case 1:
                    l2 = Long.valueOf(jsonReader.nextLong());
                    break;
                case 2:
                    str2 = new String(Base64.decode(jsonReader.nextString(), 2), v.a);
                    break;
                case 3:
                    l = Long.valueOf(jsonReader.nextLong());
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }
        }
        jsonReader.endObject();
        String str3 = l == null ? " baseAddress" : "";
        if (l2 == null) {
            str3 = b.d.b.a.a.v(str3, " size");
        }
        if (str == null) {
            str3 = b.d.b.a.a.v(str3, " name");
        }
        if (str3.isEmpty()) {
            return new m(l.longValue(), l2.longValue(), str, str2, null);
        }
        throw new IllegalStateException(b.d.b.a.a.v("Missing required properties:", str3));
    }
}
