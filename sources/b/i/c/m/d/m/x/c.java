package b.i.c.m.d.m.x;

import android.util.JsonReader;
import b.i.c.m.d.m.x.h;
import b.i.c.p.a;
import java.util.Objects;
/* compiled from: CrashlyticsReportJsonTransform.java */
/* loaded from: classes3.dex */
public final /* synthetic */ class c implements h.a {
    public static final c a = new c();

    @Override // b.i.c.m.d.m.x.h.a
    public Object a(JsonReader jsonReader) {
        a aVar = h.a;
        jsonReader.beginObject();
        String str = null;
        String str2 = null;
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            nextName.hashCode();
            if (nextName.equals("key")) {
                str = jsonReader.nextString();
                Objects.requireNonNull(str, "Null key");
            } else if (!nextName.equals("value")) {
                jsonReader.skipValue();
            } else {
                str2 = jsonReader.nextString();
                Objects.requireNonNull(str2, "Null value");
            }
        }
        jsonReader.endObject();
        String str3 = str == null ? " key" : "";
        if (str2 == null) {
            str3 = b.d.b.a.a.v(str3, " value");
        }
        if (str3.isEmpty()) {
            return new b.i.c.m.d.m.c(str, str2, null);
        }
        throw new IllegalStateException(b.d.b.a.a.v("Missing required properties:", str3));
    }
}
