package b.i.c.m.d.m;

import androidx.annotation.NonNull;
import b.i.c.m.d.m.v;
/* compiled from: AutoValue_CrashlyticsReport_Session_Event_Application_Execution.java */
/* loaded from: classes3.dex */
public final class l extends v.d.AbstractC0142d.a.b {
    public final w<v.d.AbstractC0142d.a.b.AbstractC0146d> a;

    /* renamed from: b  reason: collision with root package name */
    public final v.d.AbstractC0142d.a.b.AbstractC0145b f1722b;
    public final v.d.AbstractC0142d.a.b.c c;
    public final w<v.d.AbstractC0142d.a.b.AbstractC0144a> d;

    public l(w wVar, v.d.AbstractC0142d.a.b.AbstractC0145b bVar, v.d.AbstractC0142d.a.b.c cVar, w wVar2, a aVar) {
        this.a = wVar;
        this.f1722b = bVar;
        this.c = cVar;
        this.d = wVar2;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a.b
    @NonNull
    public w<v.d.AbstractC0142d.a.b.AbstractC0144a> a() {
        return this.d;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a.b
    @NonNull
    public v.d.AbstractC0142d.a.b.AbstractC0145b b() {
        return this.f1722b;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a.b
    @NonNull
    public v.d.AbstractC0142d.a.b.c c() {
        return this.c;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a.b
    @NonNull
    public w<v.d.AbstractC0142d.a.b.AbstractC0146d> d() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v.d.AbstractC0142d.a.b)) {
            return false;
        }
        v.d.AbstractC0142d.a.b bVar = (v.d.AbstractC0142d.a.b) obj;
        return this.a.equals(bVar.d()) && this.f1722b.equals(bVar.b()) && this.c.equals(bVar.c()) && this.d.equals(bVar.a());
    }

    public int hashCode() {
        return ((((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.f1722b.hashCode()) * 1000003) ^ this.c.hashCode()) * 1000003) ^ this.d.hashCode();
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("Execution{threads=");
        R.append(this.a);
        R.append(", exception=");
        R.append(this.f1722b);
        R.append(", signal=");
        R.append(this.c);
        R.append(", binaries=");
        R.append(this.d);
        R.append("}");
        return R.toString();
    }
}
