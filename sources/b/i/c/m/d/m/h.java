package b.i.c.m.d.m;

import androidx.annotation.NonNull;
import b.i.c.m.d.m.v;
/* compiled from: AutoValue_CrashlyticsReport_Session_Application_Organization.java */
/* loaded from: classes3.dex */
public final class h extends v.d.a.AbstractC0141a {
    @Override // b.i.c.m.d.m.v.d.a.AbstractC0141a
    @NonNull
    public String a() {
        return null;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v.d.a.AbstractC0141a)) {
            return false;
        }
        ((v.d.a.AbstractC0141a) obj).a();
        throw null;
    }

    public int hashCode() {
        throw null;
    }

    public String toString() {
        return "Organization{clsId=null}";
    }
}
