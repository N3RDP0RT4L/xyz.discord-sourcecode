package b.i.c.m.d.m;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.c.m.d.m.v;
/* compiled from: AutoValue_CrashlyticsReport_Session_Event_Application_Execution_Exception.java */
/* loaded from: classes3.dex */
public final class n extends v.d.AbstractC0142d.a.b.AbstractC0145b {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1724b;
    public final w<v.d.AbstractC0142d.a.b.AbstractC0146d.AbstractC0147a> c;
    public final v.d.AbstractC0142d.a.b.AbstractC0145b d;
    public final int e;

    public n(String str, String str2, w wVar, v.d.AbstractC0142d.a.b.AbstractC0145b bVar, int i, a aVar) {
        this.a = str;
        this.f1724b = str2;
        this.c = wVar;
        this.d = bVar;
        this.e = i;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a.b.AbstractC0145b
    @Nullable
    public v.d.AbstractC0142d.a.b.AbstractC0145b a() {
        return this.d;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a.b.AbstractC0145b
    @NonNull
    public w<v.d.AbstractC0142d.a.b.AbstractC0146d.AbstractC0147a> b() {
        return this.c;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a.b.AbstractC0145b
    public int c() {
        return this.e;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a.b.AbstractC0145b
    @Nullable
    public String d() {
        return this.f1724b;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a.b.AbstractC0145b
    @NonNull
    public String e() {
        return this.a;
    }

    public boolean equals(Object obj) {
        String str;
        v.d.AbstractC0142d.a.b.AbstractC0145b bVar;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v.d.AbstractC0142d.a.b.AbstractC0145b)) {
            return false;
        }
        v.d.AbstractC0142d.a.b.AbstractC0145b bVar2 = (v.d.AbstractC0142d.a.b.AbstractC0145b) obj;
        return this.a.equals(bVar2.e()) && ((str = this.f1724b) != null ? str.equals(bVar2.d()) : bVar2.d() == null) && this.c.equals(bVar2.b()) && ((bVar = this.d) != null ? bVar.equals(bVar2.a()) : bVar2.a() == null) && this.e == bVar2.c();
    }

    public int hashCode() {
        int hashCode = (this.a.hashCode() ^ 1000003) * 1000003;
        String str = this.f1724b;
        int i = 0;
        int hashCode2 = (((hashCode ^ (str == null ? 0 : str.hashCode())) * 1000003) ^ this.c.hashCode()) * 1000003;
        v.d.AbstractC0142d.a.b.AbstractC0145b bVar = this.d;
        if (bVar != null) {
            i = bVar.hashCode();
        }
        return ((hashCode2 ^ i) * 1000003) ^ this.e;
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("Exception{type=");
        R.append(this.a);
        R.append(", reason=");
        R.append(this.f1724b);
        R.append(", frames=");
        R.append(this.c);
        R.append(", causedBy=");
        R.append(this.d);
        R.append(", overflowCount=");
        return b.d.b.a.a.A(R, this.e, "}");
    }
}
