package b.i.c.m.d.m;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.c.m.d.m.v;
/* compiled from: AutoValue_CrashlyticsReport_Session_Event_Application_Execution_Thread_Frame.java */
/* loaded from: classes3.dex */
public final class q extends v.d.AbstractC0142d.a.b.AbstractC0146d.AbstractC0147a {
    public final long a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1727b;
    public final String c;
    public final long d;
    public final int e;

    /* compiled from: AutoValue_CrashlyticsReport_Session_Event_Application_Execution_Thread_Frame.java */
    /* loaded from: classes3.dex */
    public static final class b extends v.d.AbstractC0142d.a.b.AbstractC0146d.AbstractC0147a.AbstractC0148a {
        public Long a;

        /* renamed from: b  reason: collision with root package name */
        public String f1728b;
        public String c;
        public Long d;
        public Integer e;

        public v.d.AbstractC0142d.a.b.AbstractC0146d.AbstractC0147a a() {
            String str = this.a == null ? " pc" : "";
            if (this.f1728b == null) {
                str = b.d.b.a.a.v(str, " symbol");
            }
            if (this.d == null) {
                str = b.d.b.a.a.v(str, " offset");
            }
            if (this.e == null) {
                str = b.d.b.a.a.v(str, " importance");
            }
            if (str.isEmpty()) {
                return new q(this.a.longValue(), this.f1728b, this.c, this.d.longValue(), this.e.intValue(), null);
            }
            throw new IllegalStateException(b.d.b.a.a.v("Missing required properties:", str));
        }
    }

    public q(long j, String str, String str2, long j2, int i, a aVar) {
        this.a = j;
        this.f1727b = str;
        this.c = str2;
        this.d = j2;
        this.e = i;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a.b.AbstractC0146d.AbstractC0147a
    @Nullable
    public String a() {
        return this.c;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a.b.AbstractC0146d.AbstractC0147a
    public int b() {
        return this.e;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a.b.AbstractC0146d.AbstractC0147a
    public long c() {
        return this.d;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a.b.AbstractC0146d.AbstractC0147a
    public long d() {
        return this.a;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d.a.b.AbstractC0146d.AbstractC0147a
    @NonNull
    public String e() {
        return this.f1727b;
    }

    public boolean equals(Object obj) {
        String str;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v.d.AbstractC0142d.a.b.AbstractC0146d.AbstractC0147a)) {
            return false;
        }
        v.d.AbstractC0142d.a.b.AbstractC0146d.AbstractC0147a aVar = (v.d.AbstractC0142d.a.b.AbstractC0146d.AbstractC0147a) obj;
        return this.a == aVar.d() && this.f1727b.equals(aVar.e()) && ((str = this.c) != null ? str.equals(aVar.a()) : aVar.a() == null) && this.d == aVar.c() && this.e == aVar.b();
    }

    public int hashCode() {
        long j = this.a;
        int hashCode = (((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ this.f1727b.hashCode()) * 1000003;
        String str = this.c;
        int hashCode2 = str == null ? 0 : str.hashCode();
        long j2 = this.d;
        return this.e ^ ((((hashCode ^ hashCode2) * 1000003) ^ ((int) ((j2 >>> 32) ^ j2))) * 1000003);
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("Frame{pc=");
        R.append(this.a);
        R.append(", symbol=");
        R.append(this.f1727b);
        R.append(", file=");
        R.append(this.c);
        R.append(", offset=");
        R.append(this.d);
        R.append(", importance=");
        return b.d.b.a.a.A(R, this.e, "}");
    }
}
