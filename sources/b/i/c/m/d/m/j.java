package b.i.c.m.d.m;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.c.m.d.m.v;
/* compiled from: AutoValue_CrashlyticsReport_Session_Event.java */
/* loaded from: classes3.dex */
public final class j extends v.d.AbstractC0142d {
    public final long a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1719b;
    public final v.d.AbstractC0142d.a c;
    public final v.d.AbstractC0142d.b d;
    public final v.d.AbstractC0142d.c e;

    public j(long j, String str, v.d.AbstractC0142d.a aVar, v.d.AbstractC0142d.b bVar, v.d.AbstractC0142d.c cVar, a aVar2) {
        this.a = j;
        this.f1719b = str;
        this.c = aVar;
        this.d = bVar;
        this.e = cVar;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d
    @NonNull
    public v.d.AbstractC0142d.a a() {
        return this.c;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d
    @NonNull
    public v.d.AbstractC0142d.b b() {
        return this.d;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d
    @Nullable
    public v.d.AbstractC0142d.c c() {
        return this.e;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d
    public long d() {
        return this.a;
    }

    @Override // b.i.c.m.d.m.v.d.AbstractC0142d
    @NonNull
    public String e() {
        return this.f1719b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v.d.AbstractC0142d)) {
            return false;
        }
        v.d.AbstractC0142d dVar = (v.d.AbstractC0142d) obj;
        if (this.a == dVar.d() && this.f1719b.equals(dVar.e()) && this.c.equals(dVar.a()) && this.d.equals(dVar.b())) {
            v.d.AbstractC0142d.c cVar = this.e;
            if (cVar == null) {
                if (dVar.c() == null) {
                    return true;
                }
            } else if (cVar.equals(dVar.c())) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        long j = this.a;
        int hashCode = (((((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ this.f1719b.hashCode()) * 1000003) ^ this.c.hashCode()) * 1000003) ^ this.d.hashCode()) * 1000003;
        v.d.AbstractC0142d.c cVar = this.e;
        return (cVar == null ? 0 : cVar.hashCode()) ^ hashCode;
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("Event{timestamp=");
        R.append(this.a);
        R.append(", type=");
        R.append(this.f1719b);
        R.append(", app=");
        R.append(this.c);
        R.append(", device=");
        R.append(this.d);
        R.append(", log=");
        R.append(this.e);
        R.append("}");
        return R.toString();
    }
}
