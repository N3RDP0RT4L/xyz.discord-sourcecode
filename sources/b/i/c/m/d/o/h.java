package b.i.c.m.d.o;

import android.content.Context;
import b.i.c.m.d.b;
import java.io.File;
/* compiled from: FileStoreImpl.java */
/* loaded from: classes3.dex */
public class h {
    public final Context a;

    public h(Context context) {
        this.a = context;
    }

    public File a() {
        File file = new File(this.a.getFilesDir(), ".com.google.firebase.crashlytics");
        if (file.exists() || file.mkdirs()) {
            return file;
        }
        b.a.g("Couldn't create file");
        return null;
    }
}
