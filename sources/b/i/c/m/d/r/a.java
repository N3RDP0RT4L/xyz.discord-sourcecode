package b.i.c.m.d.r;

import b.i.a.b.h;
import b.i.c.m.d.k.o0;
import com.google.android.gms.tasks.TaskCompletionSource;
/* compiled from: DataTransportCrashlyticsReportSender.java */
/* loaded from: classes3.dex */
public final /* synthetic */ class a implements h {
    public final TaskCompletionSource a;

    /* renamed from: b  reason: collision with root package name */
    public final o0 f1741b;

    public a(TaskCompletionSource taskCompletionSource, o0 o0Var) {
        this.a = taskCompletionSource;
        this.f1741b = o0Var;
    }

    @Override // b.i.a.b.h
    public void a(Exception exc) {
        TaskCompletionSource taskCompletionSource = this.a;
        o0 o0Var = this.f1741b;
        b.i.c.m.d.m.x.h hVar = c.a;
        if (exc != null) {
            taskCompletionSource.a(exc);
        } else {
            taskCompletionSource.b(o0Var);
        }
    }
}
