package b.i.c.m.d.q.d;

import b.c.a.y.b;
/* compiled from: CompositeCreateReportSpiCall.java */
/* loaded from: classes3.dex */
public class a implements b {
    public final c a;

    /* renamed from: b  reason: collision with root package name */
    public final d f1740b;

    public a(c cVar, d dVar) {
        this.a = cVar;
        this.f1740b = dVar;
    }

    @Override // b.i.c.m.d.q.d.b
    public boolean a(b.i.c.m.d.q.c.a aVar, boolean z2) {
        int h = b.h(aVar.c.d());
        if (h == 0) {
            this.a.a(aVar, z2);
            return true;
        } else if (h != 1) {
            return false;
        } else {
            this.f1740b.a(aVar, z2);
            return true;
        }
    }
}
