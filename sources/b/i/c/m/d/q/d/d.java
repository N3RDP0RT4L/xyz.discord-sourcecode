package b.i.c.m.d.q.d;

import b.i.a.f.e.o.f;
import b.i.c.m.d.k.a;
import b.i.c.m.d.n.b;
import b.i.c.m.d.q.c.c;
import java.io.File;
import java.io.IOException;
/* compiled from: NativeCreateReportSpiCall.java */
/* loaded from: classes3.dex */
public class d extends a implements b {
    public final String f;

    public d(String str, String str2, b bVar, String str3) {
        super(str, str2, bVar, 2);
        this.f = str3;
    }

    @Override // b.i.c.m.d.q.d.b
    public boolean a(b.i.c.m.d.q.c.a aVar, boolean z2) {
        File[] e;
        if (z2) {
            b.i.c.m.d.n.a b2 = b();
            String str = aVar.f1738b;
            b2.e.put("User-Agent", "Crashlytics Android SDK/17.3.0");
            b2.e.put("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
            b2.e.put("X-CRASHLYTICS-API-CLIENT-VERSION", this.f);
            b2.e.put("X-CRASHLYTICS-GOOGLE-APP-ID", str);
            String str2 = aVar.a;
            c cVar = aVar.c;
            if (str2 != null) {
                b2.b("org_id", str2);
            }
            b2.b("report_id", cVar.a());
            for (File file : cVar.e()) {
                if (file.getName().equals("minidump")) {
                    b2.c("minidump_file", file.getName(), "application/octet-stream", file);
                } else if (file.getName().equals("metadata")) {
                    b2.c("crash_meta_file", file.getName(), "application/octet-stream", file);
                } else if (file.getName().equals("binaryImages")) {
                    b2.c("binary_images_file", file.getName(), "application/octet-stream", file);
                } else if (file.getName().equals("session")) {
                    b2.c("session_meta_file", file.getName(), "application/octet-stream", file);
                } else if (file.getName().equals("app")) {
                    b2.c("app_meta_file", file.getName(), "application/octet-stream", file);
                } else if (file.getName().equals("device")) {
                    b2.c("device_meta_file", file.getName(), "application/octet-stream", file);
                } else if (file.getName().equals("os")) {
                    b2.c("os_meta_file", file.getName(), "application/octet-stream", file);
                } else if (file.getName().equals("user")) {
                    b2.c("user_meta_file", file.getName(), "application/octet-stream", file);
                } else if (file.getName().equals("logs")) {
                    b2.c("logs_file", file.getName(), "application/octet-stream", file);
                } else if (file.getName().equals("keys")) {
                    b2.c("keys_file", file.getName(), "application/octet-stream", file);
                }
            }
            b.i.c.m.d.b bVar = b.i.c.m.d.b.a;
            StringBuilder R = b.d.b.a.a.R("Sending report to: ");
            R.append(this.f1672b);
            bVar.b(R.toString());
            try {
                int i = b2.a().a;
                bVar.b("Result was: " + i);
                return f.R0(i) == 0;
            } catch (IOException e2) {
                throw new RuntimeException(e2);
            }
        } else {
            throw new RuntimeException("An invalid data collection token was used.");
        }
    }
}
