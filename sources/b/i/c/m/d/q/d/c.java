package b.i.c.m.d.q.d;

import android.util.Log;
import b.i.a.f.e.o.f;
import b.i.c.m.d.k.a;
import b.i.c.m.d.n.b;
import java.io.File;
import java.io.IOException;
import java.util.Map;
/* compiled from: DefaultCreateReportSpiCall.java */
/* loaded from: classes3.dex */
public class c extends a implements b {
    public final String f;

    public c(String str, String str2, b bVar, String str3) {
        super(str, str2, bVar, 2);
        this.f = str3;
    }

    @Override // b.i.c.m.d.q.d.b
    public boolean a(b.i.c.m.d.q.c.a aVar, boolean z2) {
        b.i.c.m.d.n.c a;
        File[] e;
        if (z2) {
            b.i.c.m.d.n.a b2 = b();
            b2.e.put("X-CRASHLYTICS-GOOGLE-APP-ID", aVar.f1738b);
            b2.e.put("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
            b2.e.put("X-CRASHLYTICS-API-CLIENT-VERSION", this.f);
            for (Map.Entry<String, String> entry : aVar.c.b().entrySet()) {
                b2.e.put(entry.getKey(), entry.getValue());
            }
            b.i.c.m.d.q.c.c cVar = aVar.c;
            b2.b("report[identifier]", cVar.a());
            if (cVar.e().length == 1) {
                b.i.c.m.d.b bVar = b.i.c.m.d.b.a;
                StringBuilder R = b.d.b.a.a.R("Adding single file ");
                R.append(cVar.f());
                R.append(" to report ");
                R.append(cVar.a());
                bVar.b(R.toString());
                b2.c("report[file]", cVar.f(), "application/octet-stream", cVar.c());
            } else {
                int i = 0;
                for (File file : cVar.e()) {
                    b.i.c.m.d.b bVar2 = b.i.c.m.d.b.a;
                    StringBuilder R2 = b.d.b.a.a.R("Adding file ");
                    R2.append(file.getName());
                    R2.append(" to report ");
                    R2.append(cVar.a());
                    bVar2.b(R2.toString());
                    b2.c("report[file" + i + "]", file.getName(), "application/octet-stream", file);
                    i++;
                }
            }
            b.i.c.m.d.b bVar3 = b.i.c.m.d.b.a;
            StringBuilder R3 = b.d.b.a.a.R("Sending report to: ");
            R3.append(this.f1672b);
            bVar3.b(R3.toString());
            try {
                int i2 = b2.a().a;
                bVar3.b("Create report request ID: " + a.c.c("X-REQUEST-ID"));
                bVar3.b("Result was: " + i2);
                return f.R0(i2) == 0;
            } catch (IOException e2) {
                if (b.i.c.m.d.b.a.a(6)) {
                    Log.e("FirebaseCrashlytics", "Create report HTTP request failed.", e2);
                }
                throw new RuntimeException(e2);
            }
        } else {
            throw new RuntimeException("An invalid data collection token was used.");
        }
    }
}
