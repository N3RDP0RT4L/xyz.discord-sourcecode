package b.i.c.m.d.q.c;

import androidx.annotation.Nullable;
/* compiled from: CreateReportRequest.java */
/* loaded from: classes3.dex */
public class a {
    @Nullable
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1738b;
    public final c c;

    public a(@Nullable String str, String str2, c cVar) {
        this.a = str;
        this.f1738b = str2;
        this.c = cVar;
    }
}
