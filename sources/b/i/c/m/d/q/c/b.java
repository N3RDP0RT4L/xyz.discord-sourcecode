package b.i.c.m.d.q.c;

import b.d.b.a.a;
import java.io.File;
import java.util.Map;
/* compiled from: NativeSessionReport.java */
/* loaded from: classes3.dex */
public class b implements c {
    public final File a;

    public b(File file) {
        this.a = file;
    }

    @Override // b.i.c.m.d.q.c.c
    public String a() {
        return this.a.getName();
    }

    @Override // b.i.c.m.d.q.c.c
    public Map<String, String> b() {
        return null;
    }

    @Override // b.i.c.m.d.q.c.c
    public File c() {
        return null;
    }

    @Override // b.i.c.m.d.q.c.c
    public int d() {
        return 2;
    }

    @Override // b.i.c.m.d.q.c.c
    public File[] e() {
        return this.a.listFiles();
    }

    @Override // b.i.c.m.d.q.c.c
    public String f() {
        return null;
    }

    @Override // b.i.c.m.d.q.c.c
    public void remove() {
        File[] e;
        for (File file : e()) {
            b.i.c.m.d.b bVar = b.i.c.m.d.b.a;
            StringBuilder R = a.R("Removing native report file at ");
            R.append(file.getPath());
            bVar.b(R.toString());
            file.delete();
        }
        b.i.c.m.d.b bVar2 = b.i.c.m.d.b.a;
        StringBuilder R2 = a.R("Removing native report directory at ");
        R2.append(this.a);
        bVar2.b(R2.toString());
        this.a.delete();
    }
}
