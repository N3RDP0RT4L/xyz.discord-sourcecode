package b.i.c.m.d.q.c;

import b.d.b.a.a;
import b.i.c.m.d.b;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
/* compiled from: SessionReport.java */
/* loaded from: classes3.dex */
public class d implements c {
    public final File a;

    /* renamed from: b  reason: collision with root package name */
    public final File[] f1739b;
    public final Map<String, String> c;

    public d(File file, Map<String, String> map) {
        this.a = file;
        this.f1739b = new File[]{file};
        this.c = new HashMap(map);
    }

    @Override // b.i.c.m.d.q.c.c
    public String a() {
        String f = f();
        return f.substring(0, f.lastIndexOf(46));
    }

    @Override // b.i.c.m.d.q.c.c
    public Map<String, String> b() {
        return Collections.unmodifiableMap(this.c);
    }

    @Override // b.i.c.m.d.q.c.c
    public File c() {
        return this.a;
    }

    @Override // b.i.c.m.d.q.c.c
    public int d() {
        return 1;
    }

    @Override // b.i.c.m.d.q.c.c
    public File[] e() {
        return this.f1739b;
    }

    @Override // b.i.c.m.d.q.c.c
    public String f() {
        return this.a.getName();
    }

    @Override // b.i.c.m.d.q.c.c
    public void remove() {
        b bVar = b.a;
        StringBuilder R = a.R("Removing report at ");
        R.append(this.a.getPath());
        bVar.b(R.toString());
        this.a.delete();
    }
}
