package b.i.c.m.d.q;

import android.util.Log;
import androidx.annotation.Nullable;
import b.i.c.m.d.k.x;
import java.util.ArrayList;
import java.util.List;
/* compiled from: ReportUploader.java */
/* loaded from: classes3.dex */
public class b {
    public static final short[] a = {10, 20, 30, 60, 120, 300};

    /* renamed from: b  reason: collision with root package name */
    public final b.i.c.m.d.q.d.b f1737b;
    @Nullable
    public final String c;
    public final String d;
    public final int e;
    public final b.i.c.m.d.q.a f;
    public final a g;
    public Thread h;

    /* compiled from: ReportUploader.java */
    /* loaded from: classes3.dex */
    public interface a {
    }

    /* compiled from: ReportUploader.java */
    /* renamed from: b.i.c.m.d.q.b$b  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public interface AbstractC0149b {
    }

    /* compiled from: ReportUploader.java */
    /* loaded from: classes3.dex */
    public interface c {
    }

    /* compiled from: ReportUploader.java */
    /* loaded from: classes3.dex */
    public class d extends b.i.c.m.d.k.d {
        public final List<b.i.c.m.d.q.c.c> j;
        public final boolean k;
        public final float l;

        public d(List<b.i.c.m.d.q.c.c> list, boolean z2, float f) {
            this.j = list;
            this.k = z2;
            this.l = f;
        }

        @Override // b.i.c.m.d.k.d
        public void a() {
            try {
                b(this.j, this.k);
            } catch (Exception e) {
                if (b.i.c.m.d.b.a.a(6)) {
                    Log.e("FirebaseCrashlytics", "An unexpected error occurred while attempting to upload crash reports.", e);
                }
            }
            b.this.h = null;
        }

        public final void b(List<b.i.c.m.d.q.c.c> list, boolean z2) {
            float f;
            short[] sArr;
            b.i.c.m.d.b bVar = b.i.c.m.d.b.a;
            StringBuilder R = b.d.b.a.a.R("Starting report processing in ");
            R.append(this.l);
            R.append(" second(s)...");
            bVar.b(R.toString());
            if (this.l > 0.0f) {
                try {
                    Thread.sleep(f * 1000.0f);
                } catch (InterruptedException unused) {
                    Thread.currentThread().interrupt();
                    return;
                }
            }
            if (!x.this.p()) {
                int i = 0;
                while (list.size() > 0 && !x.this.p()) {
                    b.i.c.m.d.b bVar2 = b.i.c.m.d.b.a;
                    StringBuilder R2 = b.d.b.a.a.R("Attempting to send ");
                    R2.append(list.size());
                    R2.append(" report(s)");
                    bVar2.b(R2.toString());
                    ArrayList arrayList = new ArrayList();
                    for (b.i.c.m.d.q.c.c cVar : list) {
                        if (!b.this.a(cVar, z2)) {
                            arrayList.add(cVar);
                        }
                    }
                    if (arrayList.size() > 0) {
                        i++;
                        long j = b.a[Math.min(i, sArr.length - 1)];
                        b.i.c.m.d.b.a.b("Report submission: scheduling delayed retry in " + j + " seconds");
                        try {
                            Thread.sleep(j * 1000);
                        } catch (InterruptedException unused2) {
                            Thread.currentThread().interrupt();
                            return;
                        }
                    }
                    list = arrayList;
                }
            }
        }
    }

    public b(@Nullable String str, String str2, int i, b.i.c.m.d.q.a aVar, b.i.c.m.d.q.d.b bVar, a aVar2) {
        if (bVar != null) {
            this.f1737b = bVar;
            this.c = str;
            this.d = str2;
            this.e = i;
            this.f = aVar;
            this.g = aVar2;
            return;
        }
        throw new IllegalArgumentException("createReportCall must not be null.");
    }

    /* JADX WARN: Removed duplicated region for block: B:19:0x0053 A[Catch: Exception -> 0x005c, TRY_LEAVE, TryCatch #0 {Exception -> 0x005c, blocks: (B:3:0x0001, B:6:0x0011, B:9:0x001a, B:11:0x0020, B:13:0x0027, B:17:0x0040, B:19:0x0053), top: B:23:0x0001 }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public boolean a(b.i.c.m.d.q.c.c r6, boolean r7) {
        /*
            r5 = this;
            r0 = 1
            b.i.c.m.d.q.c.a r1 = new b.i.c.m.d.q.c.a     // Catch: java.lang.Exception -> L5c
            java.lang.String r2 = r5.c     // Catch: java.lang.Exception -> L5c
            java.lang.String r3 = r5.d     // Catch: java.lang.Exception -> L5c
            r1.<init>(r2, r3, r6)     // Catch: java.lang.Exception -> L5c
            int r2 = r5.e     // Catch: java.lang.Exception -> L5c
            r3 = 3
            java.lang.String r4 = "Report configured to be sent via DataTransport."
            if (r2 != r3) goto L17
            b.i.c.m.d.b r7 = b.i.c.m.d.b.a     // Catch: java.lang.Exception -> L5c
            r7.b(r4)     // Catch: java.lang.Exception -> L5c
            goto L25
        L17:
            r3 = 2
            if (r2 != r3) goto L27
            int r2 = r6.d()     // Catch: java.lang.Exception -> L5c
            if (r2 != r0) goto L27
            b.i.c.m.d.b r7 = b.i.c.m.d.b.a     // Catch: java.lang.Exception -> L5c
            r7.b(r4)     // Catch: java.lang.Exception -> L5c
        L25:
            r7 = 1
            goto L51
        L27:
            b.i.c.m.d.q.d.b r2 = r5.f1737b     // Catch: java.lang.Exception -> L5c
            boolean r7 = r2.a(r1, r7)     // Catch: java.lang.Exception -> L5c
            b.i.c.m.d.b r1 = b.i.c.m.d.b.a     // Catch: java.lang.Exception -> L5c
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch: java.lang.Exception -> L5c
            r2.<init>()     // Catch: java.lang.Exception -> L5c
            java.lang.String r3 = "Crashlytics Reports Endpoint upload "
            r2.append(r3)     // Catch: java.lang.Exception -> L5c
            if (r7 == 0) goto L3e
            java.lang.String r3 = "complete: "
            goto L40
        L3e:
            java.lang.String r3 = "FAILED: "
        L40:
            r2.append(r3)     // Catch: java.lang.Exception -> L5c
            java.lang.String r3 = r6.a()     // Catch: java.lang.Exception -> L5c
            r2.append(r3)     // Catch: java.lang.Exception -> L5c
            java.lang.String r2 = r2.toString()     // Catch: java.lang.Exception -> L5c
            r1.f(r2)     // Catch: java.lang.Exception -> L5c
        L51:
            if (r7 == 0) goto L73
            b.i.c.m.d.q.a r7 = r5.f     // Catch: java.lang.Exception -> L5c
            java.util.Objects.requireNonNull(r7)     // Catch: java.lang.Exception -> L5c
            r6.remove()     // Catch: java.lang.Exception -> L5c
            goto L74
        L5c:
            r7 = move-exception
            b.i.c.m.d.b r0 = b.i.c.m.d.b.a
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Error occurred sending report "
            r1.append(r2)
            r1.append(r6)
            java.lang.String r6 = r1.toString()
            r0.e(r6, r7)
        L73:
            r0 = 0
        L74:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.c.m.d.q.b.a(b.i.c.m.d.q.c.c, boolean):boolean");
    }
}
