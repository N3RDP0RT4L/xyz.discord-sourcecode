package b.i.c.m.d.s.h;
/* compiled from: AppSettingsData.java */
/* loaded from: classes3.dex */
public class b {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1745b;
    public final String c;
    public final String d;
    public final String e;
    public final boolean f;
    public final int g;
    public final int h;

    public b(String str, String str2, String str3, String str4, String str5, String str6, boolean z2, int i, int i2) {
        this.a = str;
        this.f1745b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str6;
        this.f = z2;
        this.g = i;
        this.h = i2;
    }

    public b(String str, String str2, String str3, String str4, boolean z2) {
        this.a = str;
        this.f1745b = str2;
        this.c = str3;
        this.d = str4;
        this.e = null;
        this.f = z2;
        this.g = 0;
        this.h = 0;
    }
}
