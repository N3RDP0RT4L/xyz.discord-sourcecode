package b.i.c.m.d.s.h;
/* compiled from: SettingsData.java */
/* loaded from: classes3.dex */
public class f implements e {
    public final b a;

    /* renamed from: b  reason: collision with root package name */
    public final d f1746b;
    public final c c;
    public final long d;

    public f(long j, b bVar, d dVar, c cVar, int i, int i2) {
        this.d = j;
        this.a = bVar;
        this.f1746b = dVar;
        this.c = cVar;
    }

    @Override // b.i.c.m.d.s.h.e
    public c a() {
        return this.c;
    }

    @Override // b.i.c.m.d.s.h.e
    public d b() {
        return this.f1746b;
    }
}
