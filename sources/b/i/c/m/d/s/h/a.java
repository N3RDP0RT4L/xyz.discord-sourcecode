package b.i.c.m.d.s.h;
/* compiled from: AppRequestData.java */
/* loaded from: classes3.dex */
public class a {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1744b;
    public final String c;
    public final String d;
    public final String e;
    public final String f;
    public final String g;
    public final int h;
    public final String i;

    public a(String str, String str2, String str3, String str4, String str5, String str6, String str7, int i, String str8, String str9) {
        this.a = str;
        this.f1744b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = str6;
        this.g = str7;
        this.h = i;
        this.i = str8;
    }
}
