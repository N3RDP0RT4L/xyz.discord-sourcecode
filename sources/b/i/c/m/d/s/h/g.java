package b.i.c.m.d.s.h;

import b.i.c.m.d.k.w0;
/* compiled from: SettingsRequest.java */
/* loaded from: classes3.dex */
public class g {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1747b;
    public final String c;
    public final String d;
    public final w0 e;
    public final String f;
    public final String g;
    public final String h;
    public final int i;

    public g(String str, String str2, String str3, String str4, w0 w0Var, String str5, String str6, String str7, int i) {
        this.a = str;
        this.f1747b = str2;
        this.c = str3;
        this.d = str4;
        this.e = w0Var;
        this.f = str5;
        this.g = str6;
        this.h = str7;
        this.i = i;
    }
}
