package b.i.c.m.d.s.i;

import b.i.c.m.d.b;
import b.i.c.m.d.k.a;
import b.i.c.m.d.k.h;
import b.i.c.m.d.k.v0;
import b.i.c.m.d.s.h.g;
import com.discord.restapi.RestAPIBuilder;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;
/* compiled from: DefaultSettingsSpiCall.java */
/* loaded from: classes3.dex */
public class c extends a implements d {
    public b f;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public c(String str, String str2, b.i.c.m.d.n.b bVar) {
        super(str, str2, bVar, 1);
        b bVar2 = b.a;
        this.f = bVar2;
    }

    public final b.i.c.m.d.n.a d(b.i.c.m.d.n.a aVar, g gVar) {
        e(aVar, "X-CRASHLYTICS-GOOGLE-APP-ID", gVar.a);
        e(aVar, "X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        e(aVar, "X-CRASHLYTICS-API-CLIENT-VERSION", "17.3.0");
        e(aVar, "Accept", RestAPIBuilder.CONTENT_TYPE_JSON);
        e(aVar, "X-CRASHLYTICS-DEVICE-MODEL", gVar.f1747b);
        e(aVar, "X-CRASHLYTICS-OS-BUILD-VERSION", gVar.c);
        e(aVar, "X-CRASHLYTICS-OS-DISPLAY-VERSION", gVar.d);
        e(aVar, "X-CRASHLYTICS-INSTALLATION-ID", ((v0) gVar.e).b());
        return aVar;
    }

    public final void e(b.i.c.m.d.n.a aVar, String str, String str2) {
        if (str2 != null) {
            aVar.e.put(str, str2);
        }
    }

    public final Map<String, String> f(g gVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("build_version", gVar.h);
        hashMap.put("display_version", gVar.g);
        hashMap.put("source", Integer.toString(gVar.i));
        String str = gVar.f;
        if (!h.s(str)) {
            hashMap.put("instance", str);
        }
        return hashMap;
    }

    public JSONObject g(b.i.c.m.d.n.c cVar) {
        int i = cVar.a;
        b bVar = this.f;
        bVar.b("Settings result was: " + i);
        if (i == 200 || i == 201 || i == 202 || i == 203) {
            String str = cVar.f1733b;
            try {
                return new JSONObject(str);
            } catch (Exception e) {
                b bVar2 = this.f;
                StringBuilder R = b.d.b.a.a.R("Failed to parse settings JSON from ");
                R.append(this.f1672b);
                bVar2.c(R.toString(), e);
                b.d.b.a.a.m0("Settings response ", str, this.f);
                return null;
            }
        } else {
            b bVar3 = this.f;
            StringBuilder R2 = b.d.b.a.a.R("Failed to retrieve settings from ");
            R2.append(this.f1672b);
            bVar3.d(R2.toString());
            return null;
        }
    }
}
