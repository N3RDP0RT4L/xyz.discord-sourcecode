package b.i.c.m.d.s.i;

import android.util.Log;
import androidx.browser.trusted.sharing.ShareTarget;
import b.i.a.f.e.o.f;
import b.i.c.m.d.k.h;
import b.i.c.m.d.n.b;
import b.i.c.m.d.n.c;
import java.io.IOException;
/* compiled from: AbstractAppSpiCall.java */
/* loaded from: classes3.dex */
public abstract class a extends b.i.c.m.d.k.a {
    public final String f;

    public a(String str, String str2, b bVar, int i, String str3) {
        super(str, str2, bVar, i);
        this.f = str3;
    }

    public boolean d(b.i.c.m.d.s.h.a aVar, boolean z2) {
        if (z2) {
            b.i.c.m.d.n.a b2 = b();
            b2.e.put("X-CRASHLYTICS-ORG-ID", aVar.a);
            b2.e.put("X-CRASHLYTICS-GOOGLE-APP-ID", aVar.f1744b);
            b2.e.put("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
            b2.e.put("X-CRASHLYTICS-API-CLIENT-VERSION", this.f);
            b2.b("org_id", aVar.a);
            b2.b("app[identifier]", aVar.c);
            b2.b("app[name]", aVar.g);
            b2.b("app[display_version]", aVar.d);
            b2.b("app[build_version]", aVar.e);
            b2.b("app[source]", Integer.toString(aVar.h));
            b2.b("app[minimum_sdk_version]", aVar.i);
            b2.b("app[built_sdk_version]", "0");
            if (!h.s(aVar.f)) {
                b2.b("app[instance_identifier]", aVar.f);
            }
            b.i.c.m.d.b bVar = b.i.c.m.d.b.a;
            StringBuilder R = b.d.b.a.a.R("Sending app info to ");
            R.append(this.f1672b);
            bVar.b(R.toString());
            try {
                c a = b2.a();
                int i = a.a;
                String str = ShareTarget.METHOD_POST.equalsIgnoreCase(b.c.a.y.b.n(b2.f1732b)) ? "Create" : "Update";
                bVar.b(str + " app request ID: " + a.c.c("X-REQUEST-ID"));
                StringBuilder sb = new StringBuilder();
                sb.append("Result was ");
                sb.append(i);
                bVar.b(sb.toString());
                return f.R0(i) == 0;
            } catch (IOException e) {
                if (b.i.c.m.d.b.a.a(6)) {
                    Log.e("FirebaseCrashlytics", "HTTP request failed.", e);
                }
                throw new RuntimeException(e);
            }
        } else {
            throw new RuntimeException("An invalid data collection token was used.");
        }
    }
}
