package b.i.c.m.d.s;

import b.i.c.m.d.k.e1;
import b.i.c.m.d.s.h.b;
import b.i.c.m.d.s.h.c;
import b.i.c.m.d.s.h.d;
import b.i.c.m.d.s.h.f;
import java.util.Locale;
import java.util.Objects;
import org.json.JSONException;
import org.json.JSONObject;
/* compiled from: SettingsV3JsonTransform.java */
/* loaded from: classes3.dex */
public class g implements f {
    @Override // b.i.c.m.d.s.f
    public f a(e1 e1Var, JSONObject jSONObject) throws JSONException {
        long j;
        int optInt = jSONObject.optInt("settings_version", 0);
        int optInt2 = jSONObject.optInt("cache_duration", 3600);
        JSONObject jSONObject2 = jSONObject.getJSONObject("fabric");
        JSONObject jSONObject3 = jSONObject.getJSONObject("app");
        String string = jSONObject3.getString("status");
        boolean equals = "new".equals(string);
        String string2 = jSONObject2.getString("bundle_id");
        String string3 = jSONObject2.getString("org_id");
        String format = equals ? "https://update.crashlytics.com/spi/v1/platforms/android/apps" : String.format(Locale.US, "https://update.crashlytics.com/spi/v1/platforms/android/apps/%s", string2);
        Locale locale = Locale.US;
        b bVar = new b(string, format, String.format(locale, "https://reports.crashlytics.com/spi/v1/platforms/android/apps/%s/reports", string2), String.format(locale, "https://reports.crashlytics.com/sdk-api/v1/platforms/android/apps/%s/minidumps", string2), string2, string3, jSONObject3.optBoolean("update_required", false), jSONObject3.optInt("report_upload_variant", 0), jSONObject3.optInt("native_report_upload_variant", 0));
        d dVar = new d(8, 4);
        c cVar = new c(jSONObject.getJSONObject("features").optBoolean("collect_reports", true));
        long j2 = optInt2;
        if (jSONObject.has("expires_at")) {
            j = jSONObject.optLong("expires_at");
        } else {
            Objects.requireNonNull(e1Var);
            j = (j2 * 1000) + System.currentTimeMillis();
        }
        return new f(j, bVar, dVar, cVar, optInt, optInt2);
    }
}
