package b.i.c.m.d.s;

import b.i.c.m.d.k.e1;
import b.i.c.m.d.s.h.f;
import org.json.JSONException;
import org.json.JSONObject;
/* compiled from: SettingsJsonParser.java */
/* loaded from: classes3.dex */
public class e {
    public final e1 a;

    public e(e1 e1Var) {
        this.a = e1Var;
    }

    public f a(JSONObject jSONObject) throws JSONException {
        f fVar;
        if (jSONObject.getInt("settings_version") != 3) {
            fVar = new b();
        } else {
            fVar = new g();
        }
        return fVar.a(this.a, jSONObject);
    }
}
