package b.i.c.m.d.s;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.a.f.n.f;
import b.i.c.m.d.k.e1;
import b.i.c.m.d.k.q0;
import b.i.c.m.d.o.h;
import b.i.c.m.d.s.h.b;
import b.i.c.m.d.s.h.e;
import b.i.c.m.d.s.h.g;
import b.i.c.m.d.s.i.d;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONException;
import org.json.JSONObject;
/* compiled from: SettingsController.java */
/* loaded from: classes3.dex */
public class c implements d {
    public final Context a;

    /* renamed from: b  reason: collision with root package name */
    public final g f1743b;
    public final e c;
    public final e1 d;
    public final b.i.c.m.d.s.a e;
    public final d f;
    public final q0 g;
    public final AtomicReference<e> h;
    public final AtomicReference<TaskCompletionSource<b>> i = new AtomicReference<>(new TaskCompletionSource());

    /* compiled from: SettingsController.java */
    /* loaded from: classes3.dex */
    public class a implements f<Void, Void> {
        public a() {
        }

        @Override // b.i.a.f.n.f
        @NonNull
        public Task<Void> a(@Nullable Void r11) throws Exception {
            JSONObject jSONObject;
            FileWriter fileWriter;
            Exception e;
            c cVar = c.this;
            d dVar = cVar.f;
            g gVar = cVar.f1743b;
            b.i.c.m.d.s.i.c cVar2 = (b.i.c.m.d.s.i.c) dVar;
            Objects.requireNonNull(cVar2);
            FileWriter fileWriter2 = null;
            try {
                Map<String, String> f = cVar2.f(gVar);
                b.i.c.m.d.n.a c = cVar2.c(f);
                cVar2.d(c, gVar);
                cVar2.f.b("Requesting settings from " + cVar2.f1672b);
                cVar2.f.b("Settings query params were: " + f);
                b.i.c.m.d.n.c a = c.a();
                cVar2.f.b("Settings request ID: " + a.c.c("X-REQUEST-ID"));
                jSONObject = cVar2.g(a);
            } catch (IOException e2) {
                if (cVar2.f.a(6)) {
                    Log.e("FirebaseCrashlytics", "Settings request failed.", e2);
                }
                jSONObject = null;
            }
            if (jSONObject != null) {
                b.i.c.m.d.s.h.f a2 = c.this.c.a(jSONObject);
                b.i.c.m.d.s.a aVar = c.this.e;
                long j = a2.d;
                Objects.requireNonNull(aVar);
                b.i.c.m.d.b.a.b("Writing settings to cache file...");
                try {
                    jSONObject.put("expires_at", j);
                    fileWriter = new FileWriter(new File(new h(aVar.a).a(), "com.crashlytics.settings.json"));
                    try {
                        fileWriter.write(jSONObject.toString());
                        fileWriter.flush();
                    } catch (Exception e3) {
                        e = e3;
                        try {
                            if (b.i.c.m.d.b.a.a(6)) {
                                Log.e("FirebaseCrashlytics", "Failed to cache settings", e);
                            }
                            b.i.c.m.d.k.h.c(fileWriter, "Failed to close settings writer.");
                            c.this.e(jSONObject, "Loaded settings: ");
                            c cVar3 = c.this;
                            String str = cVar3.f1743b.f;
                            SharedPreferences.Editor edit = b.i.c.m.d.k.h.o(cVar3.a).edit();
                            edit.putString("existing_instance_identifier", str);
                            edit.apply();
                            c.this.h.set(a2);
                            c.this.i.get().b(a2.a);
                            TaskCompletionSource<b> taskCompletionSource = new TaskCompletionSource<>();
                            taskCompletionSource.b(a2.a);
                            c.this.i.set(taskCompletionSource);
                            return b.i.a.f.e.o.f.Z(null);
                        } catch (Throwable th) {
                            th = th;
                            fileWriter2 = fileWriter;
                            fileWriter = fileWriter2;
                            b.i.c.m.d.k.h.c(fileWriter, "Failed to close settings writer.");
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        b.i.c.m.d.k.h.c(fileWriter, "Failed to close settings writer.");
                        throw th;
                    }
                } catch (Exception e4) {
                    e = e4;
                    fileWriter = null;
                } catch (Throwable th3) {
                    th = th3;
                    fileWriter = fileWriter2;
                    b.i.c.m.d.k.h.c(fileWriter, "Failed to close settings writer.");
                    throw th;
                }
                b.i.c.m.d.k.h.c(fileWriter, "Failed to close settings writer.");
                c.this.e(jSONObject, "Loaded settings: ");
                c cVar32 = c.this;
                String str2 = cVar32.f1743b.f;
                SharedPreferences.Editor edit2 = b.i.c.m.d.k.h.o(cVar32.a).edit();
                edit2.putString("existing_instance_identifier", str2);
                edit2.apply();
                c.this.h.set(a2);
                c.this.i.get().b(a2.a);
                TaskCompletionSource<b> taskCompletionSource2 = new TaskCompletionSource<>();
                taskCompletionSource2.b(a2.a);
                c.this.i.set(taskCompletionSource2);
            }
            return b.i.a.f.e.o.f.Z(null);
        }
    }

    public c(Context context, g gVar, e1 e1Var, e eVar, b.i.c.m.d.s.a aVar, d dVar, q0 q0Var) {
        AtomicReference<e> atomicReference = new AtomicReference<>();
        this.h = atomicReference;
        this.a = context;
        this.f1743b = gVar;
        this.d = e1Var;
        this.c = eVar;
        this.e = aVar;
        this.f = dVar;
        this.g = q0Var;
        JSONObject jSONObject = new JSONObject();
        atomicReference.set(new b.i.c.m.d.s.h.f(b.b(e1Var, 3600L, jSONObject), null, new b.i.c.m.d.s.h.d(jSONObject.optInt("max_custom_exception_events", 8), 4), new b.i.c.m.d.s.h.c(jSONObject.optBoolean("collect_reports", true)), 0, 3600));
    }

    public Task<b> a() {
        return this.i.get().a;
    }

    public final b.i.c.m.d.s.h.f b(int i) {
        Exception e;
        b.i.c.m.d.s.h.f fVar = null;
        try {
            if (!b.c.a.y.b.g(2, i)) {
                JSONObject a2 = this.e.a();
                if (a2 != null) {
                    b.i.c.m.d.s.h.f a3 = this.c.a(a2);
                    if (a3 != null) {
                        e(a2, "Loaded cached settings: ");
                        Objects.requireNonNull(this.d);
                        long currentTimeMillis = System.currentTimeMillis();
                        if (!b.c.a.y.b.g(3, i)) {
                            if (a3.d < currentTimeMillis) {
                                b.i.c.m.d.b.a.b("Cached settings have expired.");
                            }
                        }
                        try {
                            b.i.c.m.d.b.a.b("Returning cached settings.");
                            fVar = a3;
                        } catch (Exception e2) {
                            e = e2;
                            fVar = a3;
                            if (b.i.c.m.d.b.a.a(6)) {
                                Log.e("FirebaseCrashlytics", "Failed to get cached settings", e);
                            }
                            return fVar;
                        }
                    } else if (b.i.c.m.d.b.a.a(6)) {
                        Log.e("FirebaseCrashlytics", "Failed to parse cached settings data.", null);
                    }
                } else {
                    b.i.c.m.d.b.a.b("No cached settings data found.");
                }
            }
        } catch (Exception e3) {
            e = e3;
        }
        return fVar;
    }

    public e c() {
        return this.h.get();
    }

    /* JADX WARN: Incorrect types in method signature: (Ljava/lang/Object;Ljava/util/concurrent/Executor;)Lcom/google/android/gms/tasks/Task<Ljava/lang/Void;>; */
    public Task d(int i, Executor executor) {
        b.i.c.m.d.s.h.f b2;
        if ((!b.i.c.m.d.k.h.o(this.a).getString("existing_instance_identifier", "").equals(this.f1743b.f)) || (b2 = b(i)) == null) {
            b.i.c.m.d.s.h.f b3 = b(3);
            if (b3 != null) {
                this.h.set(b3);
                this.i.get().b(b3.a);
            }
            return this.g.c().r(executor, new a());
        }
        this.h.set(b2);
        this.i.get().b(b2.a);
        return b.i.a.f.e.o.f.Z(null);
    }

    public final void e(JSONObject jSONObject, String str) throws JSONException {
        b.i.c.m.d.b bVar = b.i.c.m.d.b.a;
        StringBuilder R = b.d.b.a.a.R(str);
        R.append(jSONObject.toString());
        bVar.b(R.toString());
    }
}
