package b.i.c.m.d;

import android.util.Log;
import androidx.annotation.NonNull;
import b.i.a.f.n.a;
import com.google.android.gms.tasks.Task;
/* compiled from: Onboarding.java */
/* loaded from: classes3.dex */
public class g implements a<Void, Object> {
    public g(h hVar) {
    }

    @Override // b.i.a.f.n.a
    public Object a(@NonNull Task<Void> task) throws Exception {
        if (task.p()) {
            return null;
        }
        b bVar = b.a;
        Exception k = task.k();
        if (!bVar.a(6)) {
            return null;
        }
        Log.e("FirebaseCrashlytics", "Error fetching settings.", k);
        return null;
    }
}
