package b.i.c.m.d.i;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.c.m.d.b;
/* compiled from: UnavailableAnalyticsEventLogger.java */
/* loaded from: classes3.dex */
public class f implements a {
    @Override // b.i.c.m.d.i.a
    public void a(@NonNull String str, @Nullable Bundle bundle) {
        b.a.b("Skipping logging Crashlytics event to Firebase, no Firebase Analytics");
    }
}
