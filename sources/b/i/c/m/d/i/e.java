package b.i.c.m.d.i;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.c.j.a.a;
/* compiled from: CrashlyticsOriginAnalyticsEventLogger.java */
/* loaded from: classes3.dex */
public class e implements a {
    @NonNull
    public final a a;

    public e(@NonNull a aVar) {
        this.a = aVar;
    }

    @Override // b.i.c.m.d.i.a
    public void a(@NonNull String str, @Nullable Bundle bundle) {
        this.a.a("clx", str, bundle);
    }
}
