package b.i.c.m.d;

import androidx.annotation.Nullable;
import java.io.File;
/* compiled from: NativeSessionFileProvider.java */
/* loaded from: classes3.dex */
public interface d {
    @Nullable
    File a();

    @Nullable
    File b();

    @Nullable
    File c();

    @Nullable
    File d();

    @Nullable
    File e();

    @Nullable
    File f();
}
