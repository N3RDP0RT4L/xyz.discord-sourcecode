package b.i.c.m.d;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import b.i.c.c;
import b.i.c.m.d.k.q0;
import b.i.c.m.d.k.r0;
import b.i.c.m.d.k.v0;
import b.i.c.m.d.n.b;
import b.i.c.m.d.s.h.a;
import b.i.c.m.d.s.i.e;
import java.util.Objects;
import java.util.concurrent.Executor;
/* compiled from: Onboarding.java */
/* loaded from: classes3.dex */
public class h {
    public final b a = new b();

    /* renamed from: b  reason: collision with root package name */
    public final c f1670b;
    public final Context c;
    public PackageManager d;
    public String e;
    public PackageInfo f;
    public String g;
    public String h;
    public String i;
    public String j;
    public String k;
    public v0 l;
    public q0 m;

    public h(c cVar, Context context, v0 v0Var, q0 q0Var) {
        this.f1670b = cVar;
        this.c = context;
        this.l = v0Var;
        this.m = q0Var;
    }

    public static void a(h hVar, b.i.c.m.d.s.h.b bVar, String str, b.i.c.m.d.s.c cVar, Executor executor, boolean z2) {
        Objects.requireNonNull(hVar);
        if ("new".equals(bVar.a)) {
            if (new b.i.c.m.d.s.i.b(hVar.c(), bVar.f1745b, hVar.a, "17.3.0").d(hVar.b(bVar.e, str), z2)) {
                cVar.d(2, executor);
            } else if (b.a.a(6)) {
                Log.e("FirebaseCrashlytics", "Failed to create app with Crashlytics service.", null);
            }
        } else if ("configured".equals(bVar.a)) {
            cVar.d(2, executor);
        } else if (bVar.f) {
            b.a.b("Server says an update is required - forcing a full App update.");
            new e(hVar.c(), bVar.f1745b, hVar.a, "17.3.0").d(hVar.b(bVar.e, str), z2);
        }
    }

    public final a b(String str, String str2) {
        return new a(str, str2, this.l.e, this.h, this.g, b.i.c.m.d.k.h.f(b.i.c.m.d.k.h.l(this.c), str2, this.h, this.g), this.j, r0.f(this.i).g(), this.k, "0");
    }

    public String c() {
        Context context = this.c;
        int n = b.i.c.m.d.k.h.n(context, "com.crashlytics.ApiEndpoint", "string");
        return n > 0 ? context.getString(n) : "";
    }
}
