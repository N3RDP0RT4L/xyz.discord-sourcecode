package b.i.c.m.d.t;
/* compiled from: MiddleOutFallbackStrategy.java */
/* loaded from: classes3.dex */
public class a implements d {
    public final d[] a;

    /* renamed from: b  reason: collision with root package name */
    public final b f1748b;

    public a(int i, d... dVarArr) {
        this.a = dVarArr;
        this.f1748b = new b(i);
    }

    @Override // b.i.c.m.d.t.d
    public StackTraceElement[] a(StackTraceElement[] stackTraceElementArr) {
        d[] dVarArr;
        if (stackTraceElementArr.length <= 1024) {
            return stackTraceElementArr;
        }
        StackTraceElement[] stackTraceElementArr2 = stackTraceElementArr;
        for (d dVar : this.a) {
            if (stackTraceElementArr2.length <= 1024) {
                break;
            }
            stackTraceElementArr2 = dVar.a(stackTraceElementArr);
        }
        return stackTraceElementArr2.length > 1024 ? this.f1748b.a(stackTraceElementArr2) : stackTraceElementArr2;
    }
}
