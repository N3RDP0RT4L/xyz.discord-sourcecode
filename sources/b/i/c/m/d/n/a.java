package b.i.c.m.d.n;

import b.c.a.y.b;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import f0.d;
import f0.e0.c;
import f0.e0.g.e;
import f0.w;
import f0.x;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
/* compiled from: HttpRequest.java */
/* loaded from: classes3.dex */
public class a {
    public static final x a;

    /* renamed from: b  reason: collision with root package name */
    public final int f1732b;
    public final String c;
    public final Map<String, String> d;
    public MultipartBody.a f = null;
    public final Map<String, String> e = new HashMap();

    static {
        x.a f = new x().f();
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        m.checkParameterIsNotNull(timeUnit, "unit");
        f.w = c.b("timeout", 10000L, timeUnit);
        a = new x(f);
    }

    /* JADX WARN: Incorrect types in method signature: (Ljava/lang/Object;Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;)V */
    public a(int i, String str, Map map) {
        this.f1732b = i;
        this.c = str;
        this.d = map;
    }

    public c a() throws IOException {
        w wVar;
        Request.a aVar = new Request.a();
        d dVar = new d(true, false, -1, -1, false, false, false, -1, -1, false, false, false, null, null);
        m.checkParameterIsNotNull(dVar, "cacheControl");
        String dVar2 = dVar.toString();
        if (dVar2.length() == 0) {
            aVar.d("Cache-Control");
        } else {
            aVar.b("Cache-Control", dVar2);
        }
        String str = this.c;
        m.checkParameterIsNotNull(str, "$this$toHttpUrlOrNull");
        String str2 = null;
        try {
            m.checkParameterIsNotNull(str, "$this$toHttpUrl");
            w.a aVar2 = new w.a();
            aVar2.e(null, str);
            wVar = aVar2.b();
        } catch (IllegalArgumentException unused) {
            wVar = null;
        }
        w.a f = wVar.f();
        for (Map.Entry<String, String> entry : this.d.entrySet()) {
            f.a(entry.getKey(), entry.getValue());
        }
        aVar.g(f.b());
        for (Map.Entry<String, String> entry2 : this.e.entrySet()) {
            aVar.b(entry2.getKey(), entry2.getValue());
        }
        MultipartBody.a aVar3 = this.f;
        aVar.c(b.n(this.f1732b), aVar3 == null ? null : aVar3.b());
        Response execute = ((e) a.b(aVar.a())).execute();
        ResponseBody responseBody = execute.p;
        if (responseBody != null) {
            str2 = responseBody.d();
        }
        return new c(execute.m, str2, execute.o);
    }

    public a b(String str, String str2) {
        if (this.f == null) {
            MultipartBody.a aVar = new MultipartBody.a();
            aVar.c(MultipartBody.f3781b);
            this.f = aVar;
        }
        MultipartBody.a aVar2 = this.f;
        Objects.requireNonNull(aVar2);
        m.checkParameterIsNotNull(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkParameterIsNotNull(str2, "value");
        m.checkParameterIsNotNull(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkParameterIsNotNull(str2, "value");
        aVar2.a(MultipartBody.Part.b(str, null, RequestBody.Companion.a(str2, null)));
        this.f = aVar2;
        return this;
    }

    public a c(String str, String str2, String str3, File file) {
        MediaType.a aVar = MediaType.c;
        RequestBody create = RequestBody.create(MediaType.a.b(str3), file);
        if (this.f == null) {
            MultipartBody.a aVar2 = new MultipartBody.a();
            aVar2.c(MultipartBody.f3781b);
            this.f = aVar2;
        }
        MultipartBody.a aVar3 = this.f;
        Objects.requireNonNull(aVar3);
        m.checkParameterIsNotNull(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkParameterIsNotNull(create, "body");
        aVar3.a(MultipartBody.Part.b(str, str2, create));
        this.f = aVar3;
        return this;
    }
}
