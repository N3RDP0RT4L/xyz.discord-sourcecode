package b.i.c.m.d.p;

import android.app.ActivityManager;
import b.i.c.m.d.t.e;
import java.util.List;
import java.util.Map;
/* compiled from: SessionProtobufHelper.java */
/* loaded from: classes3.dex */
public class d {
    public static final a a = a.a("0");

    /* renamed from: b  reason: collision with root package name */
    public static final a f1736b = a.a("Unity");

    public static int a(a aVar, a aVar2) {
        int b2 = c.b(3, aVar) + c.h(2, 0L) + c.h(1, 0L) + 0;
        return aVar2 != null ? b2 + c.b(4, aVar2) : b2;
    }

    public static int b(String str, String str2) {
        int b2 = c.b(1, a.a(str));
        if (str2 == null) {
            str2 = "";
        }
        return c.b(2, a.a(str2)) + b2;
    }

    public static int c(e eVar, int i, int i2) {
        int i3 = 0;
        int b2 = c.b(1, a.a(eVar.f1749b)) + 0;
        String str = eVar.a;
        if (str != null) {
            b2 += c.b(3, a.a(str));
        }
        for (StackTraceElement stackTraceElement : eVar.c) {
            int h = h(stackTraceElement, true);
            b2 += c.d(h) + c.e(4) + h;
        }
        e eVar2 = eVar.d;
        if (eVar2 == null) {
            return b2;
        }
        if (i < i2) {
            int c = c(eVar2, i + 1, i2);
            return b2 + c.d(c) + c.e(6) + c;
        }
        while (eVar2 != null) {
            eVar2 = eVar2.d;
            i3++;
        }
        return b2 + c.f(7, i3);
    }

    public static int d() {
        a aVar = a;
        return c.h(3, 0L) + c.b(2, aVar) + c.b(1, aVar) + 0;
    }

    public static int e(e eVar, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, a aVar, a aVar2) {
        int e = c.e(1);
        int i2 = i(thread, stackTraceElementArr, 4, true);
        int d = c.d(i2) + e + i2 + 0;
        int length = threadArr.length;
        for (int i3 = 0; i3 < length; i3++) {
            int i4 = i(threadArr[i3], list.get(i3), 0, false);
            d += c.d(i4) + e + i4;
        }
        int c = c(eVar, 1, i);
        int d2 = c.d(c) + c.e(2) + c + d;
        int d3 = d();
        int e2 = c.e(3);
        int a2 = a(aVar, aVar2);
        return c.d(a2) + c.e(3) + a2 + c.d(d3) + e2 + d3 + d2;
    }

    public static int f(e eVar, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, a aVar, a aVar2, Map<String, String> map, ActivityManager.RunningAppProcessInfo runningAppProcessInfo, int i2) {
        int e = e(eVar, thread, stackTraceElementArr, threadArr, list, i, aVar, aVar2);
        boolean z2 = true;
        int d = c.d(e) + c.e(1) + e + 0;
        if (map != null) {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                int b2 = b(entry.getKey(), entry.getValue());
                d += c.d(b2) + c.e(2) + b2;
            }
        }
        if (runningAppProcessInfo != null) {
            if (runningAppProcessInfo.importance == 100) {
                z2 = false;
            }
            d += c.a(3, z2);
        }
        return c.f(4, i2) + d;
    }

    public static int g(Float f, int i, boolean z2, int i2, long j, long j2) {
        int i3 = 0;
        if (f != null) {
            f.floatValue();
            i3 = 0 + c.e(1) + 4;
        }
        int e = c.e(2);
        int i4 = i << 1;
        int a2 = c.a(3, z2);
        return c.h(6, j2) + c.h(5, j) + c.f(4, i2) + a2 + i3 + c.d((i >> 31) ^ i4) + e;
    }

    public static int h(StackTraceElement stackTraceElement, boolean z2) {
        int i;
        int i2 = 0;
        if (stackTraceElement.isNativeMethod()) {
            i = c.h(1, Math.max(stackTraceElement.getLineNumber(), 0));
        } else {
            i = c.h(1, 0L);
        }
        int b2 = c.b(2, a.a(stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName())) + i + 0;
        if (stackTraceElement.getFileName() != null) {
            b2 += c.b(3, a.a(stackTraceElement.getFileName()));
        }
        if (!stackTraceElement.isNativeMethod() && stackTraceElement.getLineNumber() > 0) {
            b2 += c.h(4, stackTraceElement.getLineNumber());
        }
        if (z2) {
            i2 = 2;
        }
        return c.f(5, i2) + b2;
    }

    public static int i(Thread thread, StackTraceElement[] stackTraceElementArr, int i, boolean z2) {
        int f = c.f(2, i) + c.b(1, a.a(thread.getName()));
        for (StackTraceElement stackTraceElement : stackTraceElementArr) {
            int h = h(stackTraceElement, z2);
            f += c.d(h) + c.e(3) + h;
        }
        return f;
    }

    public static a j(String str) {
        if (str == null) {
            return null;
        }
        return a.a(str);
    }

    public static void k(c cVar, int i, StackTraceElement stackTraceElement, boolean z2) throws Exception {
        cVar.o((i << 3) | 2);
        cVar.o(h(stackTraceElement, z2));
        int i2 = 0;
        if (stackTraceElement.isNativeMethod()) {
            cVar.t(1, Math.max(stackTraceElement.getLineNumber(), 0));
        } else {
            cVar.t(1, 0L);
        }
        cVar.l(2, a.a(stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName()));
        if (stackTraceElement.getFileName() != null) {
            cVar.l(3, a.a(stackTraceElement.getFileName()));
        }
        if (!stackTraceElement.isNativeMethod() && stackTraceElement.getLineNumber() > 0) {
            cVar.t(4, stackTraceElement.getLineNumber());
        }
        if (z2) {
            i2 = 4;
        }
        cVar.s(5, i2);
    }

    public static void l(c cVar, e eVar, int i, int i2, int i3) throws Exception {
        cVar.o((i3 << 3) | 2);
        cVar.o(c(eVar, 1, i2));
        cVar.l(1, a.a(eVar.f1749b));
        String str = eVar.a;
        if (str != null) {
            cVar.l(3, a.a(str));
        }
        int i4 = 0;
        for (StackTraceElement stackTraceElement : eVar.c) {
            k(cVar, 4, stackTraceElement, true);
        }
        e eVar2 = eVar.d;
        if (eVar2 == null) {
            return;
        }
        if (i < i2) {
            l(cVar, eVar2, i + 1, i2, 6);
            return;
        }
        while (eVar2 != null) {
            eVar2 = eVar2.d;
            i4++;
        }
        cVar.s(7, i4);
    }

    public static void m(c cVar, Thread thread, StackTraceElement[] stackTraceElementArr, int i, boolean z2) throws Exception {
        cVar.r(1, 2);
        cVar.o(i(thread, stackTraceElementArr, i, z2));
        cVar.l(1, a.a(thread.getName()));
        cVar.s(2, i);
        for (StackTraceElement stackTraceElement : stackTraceElementArr) {
            k(cVar, 3, stackTraceElement, z2);
        }
    }
}
