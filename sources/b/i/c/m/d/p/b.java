package b.i.c.m.d.p;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
/* compiled from: ClsFileOutputStream.java */
/* loaded from: classes3.dex */
public class b extends FileOutputStream {
    public static final FilenameFilter j = new a();
    public final String k;
    public File l;
    public boolean m = false;

    /* compiled from: ClsFileOutputStream.java */
    /* loaded from: classes3.dex */
    public class a implements FilenameFilter {
        @Override // java.io.FilenameFilter
        public boolean accept(File file, String str) {
            return str.endsWith(".cls_temp");
        }
    }

    public b(File file, String str) throws FileNotFoundException {
        super(new File(file, b.d.b.a.a.v(str, ".cls_temp")));
        StringBuilder sb = new StringBuilder();
        sb.append(file);
        String H = b.d.b.a.a.H(sb, File.separator, str);
        this.k = H;
        this.l = new File(b.d.b.a.a.v(H, ".cls_temp"));
    }

    public void a() throws IOException {
        if (!this.m) {
            this.m = true;
            flush();
            super.close();
        }
    }

    @Override // java.io.FileOutputStream, java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
    public synchronized void close() throws IOException {
        if (!this.m) {
            this.m = true;
            flush();
            super.close();
            File file = new File(this.k + ".cls");
            if (this.l.renameTo(file)) {
                this.l = null;
                return;
            }
            String str = "";
            if (file.exists()) {
                str = " (target already exists)";
            } else if (!this.l.exists()) {
                str = " (source does not exist)";
            }
            throw new IOException("Could not rename temp file: " + this.l + " -> " + file + str);
        }
    }
}
