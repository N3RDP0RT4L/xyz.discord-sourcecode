package b.i.c.m.d;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.c.m.d.s.c;
import b.i.c.m.d.s.h.b;
import com.google.android.gms.tasks.Task;
/* compiled from: Onboarding.java */
/* loaded from: classes3.dex */
public class f implements b.i.a.f.n.f<Void, b> {
    public final /* synthetic */ c a;

    public f(h hVar, c cVar) {
        this.a = cVar;
    }

    @Override // b.i.a.f.n.f
    @NonNull
    public Task<b> a(@Nullable Void r1) throws Exception {
        return this.a.a();
    }
}
