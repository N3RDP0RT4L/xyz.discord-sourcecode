package b.i.c.m.d.u;

import android.content.Context;
import b.i.c.m.d.b;
import b.i.c.m.d.k.h;
/* compiled from: ResourceUnityVersionProvider.java */
/* loaded from: classes3.dex */
public class a {
    public final Context a;

    /* renamed from: b  reason: collision with root package name */
    public boolean f1750b = false;
    public String c;

    public a(Context context) {
        this.a = context;
    }

    public String a() {
        String str;
        if (!this.f1750b) {
            Context context = this.a;
            int n = h.n(context, "com.google.firebase.crashlytics.unity_version", "string");
            if (n != 0) {
                str = context.getResources().getString(n);
                b.d.b.a.a.m0("Unity Editor version is: ", str, b.a);
            } else {
                str = null;
            }
            this.c = str;
            this.f1750b = true;
        }
        String str2 = this.c;
        if (str2 != null) {
            return str2;
        }
        return null;
    }
}
