package b.i.c.m.d;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.a.f.n.f;
import b.i.c.m.d.s.c;
import b.i.c.m.d.s.h.b;
import com.google.android.gms.tasks.Task;
import java.util.concurrent.Executor;
/* compiled from: Onboarding.java */
/* loaded from: classes3.dex */
public class e implements f<b, Void> {
    public final /* synthetic */ String a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ c f1669b;
    public final /* synthetic */ Executor c;
    public final /* synthetic */ h d;

    public e(h hVar, String str, c cVar, Executor executor) {
        this.d = hVar;
        this.a = str;
        this.f1669b = cVar;
        this.c = executor;
    }

    @Override // b.i.a.f.n.f
    @NonNull
    public Task<Void> a(@Nullable b bVar) throws Exception {
        try {
            h.a(this.d, bVar, this.a, this.f1669b, this.c, true);
            return null;
        } catch (Exception e) {
            if (b.a.a(6)) {
                Log.e("FirebaseCrashlytics", "Error performing auto configuration.", e);
            }
            throw e;
        }
    }
}
