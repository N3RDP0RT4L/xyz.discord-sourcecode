package b.i.c.k.d;

import android.os.RemoteException;
import b.c.a.a0.d;
import b.i.a.f.e.h.j.p;
import b.i.a.f.e.o.f;
import b.i.a.f.h.k.b;
import b.i.a.f.h.k.g;
import b.i.a.f.h.k.h;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;
/* compiled from: com.google.firebase:firebase-appindexing@@19.1.0 */
/* loaded from: classes3.dex */
public abstract class e extends p<g, Void> implements b.i.a.f.e.h.j.e<Status> {
    public TaskCompletionSource<Void> c;

    public e(d dVar) {
    }

    @Override // b.i.a.f.e.h.j.e
    public void a(Status status) {
        d.o(!status.w0(), "Failed result must not be success.");
        TaskCompletionSource<Void> taskCompletionSource = this.c;
        taskCompletionSource.a.t(f.R1(status, status.q));
    }

    @Override // b.i.a.f.e.h.j.e
    public void b(Object obj) {
        Status status = (Status) obj;
        if (status.w0()) {
            this.c.a.s(null);
            return;
        }
        TaskCompletionSource<Void> taskCompletionSource = this.c;
        taskCompletionSource.a.t(f.R1(status, "User Action indexing error, please try again."));
    }

    @Override // b.i.a.f.e.h.j.p
    public void c(g gVar, TaskCompletionSource<Void> taskCompletionSource) throws RemoteException {
        this.c = taskCompletionSource;
        d dVar = (d) this;
        ((b) gVar.w()).S(new h(dVar), dVar.d);
    }
}
