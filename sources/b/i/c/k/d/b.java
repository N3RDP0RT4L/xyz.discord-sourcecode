package b.i.c.k.d;

import android.content.Context;
import b.i.c.k.a;
import com.google.android.gms.tasks.Task;
import com.google.firebase.appindexing.internal.zza;
/* compiled from: com.google.firebase:firebase-appindexing@@19.1.0 */
/* loaded from: classes3.dex */
public final class b extends b.i.c.k.b {

    /* renamed from: b  reason: collision with root package name */
    public c f1657b;

    public b(Context context) {
        this.f1657b = new c(context);
    }

    @Override // b.i.c.k.b
    public final Task<Void> a(a aVar) {
        zza[] zzaVarArr = {(zza) aVar};
        zzaVarArr[0].n.j = 2;
        return this.f1657b.c(new d(zzaVarArr));
    }
}
