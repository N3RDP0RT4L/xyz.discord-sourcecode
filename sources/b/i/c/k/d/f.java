package b.i.c.k.d;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.firebase.appindexing.internal.zzc;
/* compiled from: com.google.firebase:firebase-appindexing@@19.1.0 */
/* loaded from: classes3.dex */
public final class f implements Parcelable.Creator<zzc> {
    @Override // android.os.Parcelable.Creator
    public final zzc createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        String str = null;
        String str2 = null;
        byte[] bArr = null;
        int i = 0;
        boolean z2 = false;
        boolean z3 = false;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    i = d.G1(parcel, readInt);
                    break;
                case 2:
                    z2 = d.E1(parcel, readInt);
                    break;
                case 3:
                    str = d.R(parcel, readInt);
                    break;
                case 4:
                    str2 = d.R(parcel, readInt);
                    break;
                case 5:
                    bArr = d.N(parcel, readInt);
                    break;
                case 6:
                    z3 = d.E1(parcel, readInt);
                    break;
                default:
                    d.d2(parcel, readInt);
                    break;
            }
        }
        d.f0(parcel, m2);
        return new zzc(i, z2, str, str2, bArr, z3);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzc[] newArray(int i) {
        return new zzc[i];
    }
}
