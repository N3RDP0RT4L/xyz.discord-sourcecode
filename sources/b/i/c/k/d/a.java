package b.i.c.k.d;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.firebase.appindexing.internal.zza;
import com.google.firebase.appindexing.internal.zzc;
/* compiled from: com.google.firebase:firebase-appindexing@@19.1.0 */
/* loaded from: classes3.dex */
public final class a implements Parcelable.Creator<zza> {
    @Override // android.os.Parcelable.Creator
    public final zza createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        zzc zzcVar = null;
        String str5 = null;
        Bundle bundle = null;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    str = d.R(parcel, readInt);
                    break;
                case 2:
                    str2 = d.R(parcel, readInt);
                    break;
                case 3:
                    str3 = d.R(parcel, readInt);
                    break;
                case 4:
                    str4 = d.R(parcel, readInt);
                    break;
                case 5:
                    zzcVar = (zzc) d.Q(parcel, readInt, zzc.CREATOR);
                    break;
                case 6:
                    str5 = d.R(parcel, readInt);
                    break;
                case 7:
                    bundle = d.M(parcel, readInt);
                    break;
                default:
                    d.d2(parcel, readInt);
                    break;
            }
        }
        d.f0(parcel, m2);
        return new zza(str, str2, str3, str4, zzcVar, str5, bundle);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zza[] newArray(int i) {
        return new zza[i];
    }
}
