package b.i.c.u;

import androidx.annotation.NonNull;
import com.google.auto.value.AutoValue;
/* compiled from: com.google.firebase:firebase-installations-interop@@16.0.0 */
@AutoValue
/* loaded from: classes3.dex */
public abstract class k {
    @NonNull
    public abstract String a();

    @NonNull
    public abstract long b();

    @NonNull
    public abstract long c();
}
