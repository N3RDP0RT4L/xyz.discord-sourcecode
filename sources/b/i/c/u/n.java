package b.i.c.u;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import b.i.c.u.o.d;
import b.i.c.u.q.a;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
/* compiled from: Utils.java */
/* loaded from: classes3.dex */
public final class n {
    public static final long a = TimeUnit.HOURS.toSeconds(1);

    /* renamed from: b  reason: collision with root package name */
    public static final Pattern f1779b = Pattern.compile("\\AA[\\w-]{38}\\z");
    public static n c;
    public final a d;

    public n(a aVar) {
        this.d = aVar;
    }

    public static n c() {
        if (a.a == null) {
            a.a = new a();
        }
        a aVar = a.a;
        if (c == null) {
            c = new n(aVar);
        }
        return c;
    }

    public long a() {
        Objects.requireNonNull(this.d);
        return System.currentTimeMillis();
    }

    public long b() {
        return TimeUnit.MILLISECONDS.toSeconds(a());
    }

    public boolean d(@NonNull d dVar) {
        if (TextUtils.isEmpty(dVar.a())) {
            return true;
        }
        return dVar.b() + dVar.g() < b() + a;
    }
}
