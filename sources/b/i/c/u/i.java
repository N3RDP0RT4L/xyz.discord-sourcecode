package b.i.c.u;

import b.d.b.a.a;
import b.i.c.u.o.d;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.Objects;
/* compiled from: GetAuthTokenListener.java */
/* loaded from: classes3.dex */
public class i implements m {
    public final n a;

    /* renamed from: b  reason: collision with root package name */
    public final TaskCompletionSource<k> f1777b;

    public i(n nVar, TaskCompletionSource<k> taskCompletionSource) {
        this.a = nVar;
        this.f1777b = taskCompletionSource;
    }

    @Override // b.i.c.u.m
    public boolean a(Exception exc) {
        this.f1777b.a(exc);
        return true;
    }

    @Override // b.i.c.u.m
    public boolean b(d dVar) {
        if (!dVar.j() || this.a.d(dVar)) {
            return false;
        }
        TaskCompletionSource<k> taskCompletionSource = this.f1777b;
        String a = dVar.a();
        Objects.requireNonNull(a, "Null token");
        Long valueOf = Long.valueOf(dVar.b());
        Long valueOf2 = Long.valueOf(dVar.g());
        String str = "";
        if (valueOf == null) {
            str = a.v(str, " tokenExpirationTimestamp");
        }
        if (valueOf2 == null) {
            str = a.v(str, " tokenCreationTimestamp");
        }
        if (str.isEmpty()) {
            taskCompletionSource.a.s(new a(a, valueOf.longValue(), valueOf2.longValue(), null));
            return true;
        }
        throw new IllegalStateException(a.v("Missing required properties:", str));
    }
}
