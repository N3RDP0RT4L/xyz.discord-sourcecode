package b.i.c.u.p;

import androidx.annotation.Nullable;
import b.i.c.u.p.d;
/* compiled from: AutoValue_InstallationResponse.java */
/* loaded from: classes3.dex */
public final class a extends d {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1784b;
    public final String c;
    public final f d;
    public final d.a e;

    public a(String str, String str2, String str3, f fVar, d.a aVar, C0152a aVar2) {
        this.a = str;
        this.f1784b = str2;
        this.c = str3;
        this.d = fVar;
        this.e = aVar;
    }

    @Override // b.i.c.u.p.d
    @Nullable
    public f a() {
        return this.d;
    }

    @Override // b.i.c.u.p.d
    @Nullable
    public String b() {
        return this.f1784b;
    }

    @Override // b.i.c.u.p.d
    @Nullable
    public String c() {
        return this.c;
    }

    @Override // b.i.c.u.p.d
    @Nullable
    public d.a d() {
        return this.e;
    }

    @Override // b.i.c.u.p.d
    @Nullable
    public String e() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof d)) {
            return false;
        }
        d dVar = (d) obj;
        String str = this.a;
        if (str != null ? str.equals(dVar.e()) : dVar.e() == null) {
            String str2 = this.f1784b;
            if (str2 != null ? str2.equals(dVar.b()) : dVar.b() == null) {
                String str3 = this.c;
                if (str3 != null ? str3.equals(dVar.c()) : dVar.c() == null) {
                    f fVar = this.d;
                    if (fVar != null ? fVar.equals(dVar.a()) : dVar.a() == null) {
                        d.a aVar = this.e;
                        if (aVar == null) {
                            if (dVar.d() == null) {
                                return true;
                            }
                        } else if (aVar.equals(dVar.d())) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = ((str == null ? 0 : str.hashCode()) ^ 1000003) * 1000003;
        String str2 = this.f1784b;
        int hashCode2 = (hashCode ^ (str2 == null ? 0 : str2.hashCode())) * 1000003;
        String str3 = this.c;
        int hashCode3 = (hashCode2 ^ (str3 == null ? 0 : str3.hashCode())) * 1000003;
        f fVar = this.d;
        int hashCode4 = (hashCode3 ^ (fVar == null ? 0 : fVar.hashCode())) * 1000003;
        d.a aVar = this.e;
        if (aVar != null) {
            i = aVar.hashCode();
        }
        return hashCode4 ^ i;
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("InstallationResponse{uri=");
        R.append(this.a);
        R.append(", fid=");
        R.append(this.f1784b);
        R.append(", refreshToken=");
        R.append(this.c);
        R.append(", authToken=");
        R.append(this.d);
        R.append(", responseCode=");
        R.append(this.e);
        R.append("}");
        return R.toString();
    }
}
