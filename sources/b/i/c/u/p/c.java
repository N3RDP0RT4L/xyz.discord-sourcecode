package b.i.c.u.p;

import android.content.Context;
import android.text.TextUtils;
import android.util.JsonReader;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.c.r.d;
import b.i.c.t.a;
import b.i.c.u.p.b;
import b.i.c.u.p.d;
import b.i.c.u.p.f;
import b.i.c.x.h;
import com.adjust.sdk.Constants;
import com.discord.models.domain.ModelAuditLogEntry;
import com.google.firebase.installations.FirebaseInstallationsException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.regex.Pattern;
import java.util.zip.GZIPOutputStream;
import org.json.JSONException;
import org.json.JSONObject;
/* compiled from: FirebaseInstallationServiceClient.java */
/* loaded from: classes3.dex */
public class c {
    public static final Pattern a = Pattern.compile("[0-9]+s");

    /* renamed from: b  reason: collision with root package name */
    public static final Charset f1787b = Charset.forName(Constants.ENCODING);
    public final Context c;
    public final a<h> d;
    public final a<d> e;
    public final e f = new e();

    public c(@NonNull Context context, @NonNull a<h> aVar, @NonNull a<d> aVar2) {
        this.c = context;
        this.d = aVar;
        this.e = aVar2;
    }

    public static void b(HttpURLConnection httpURLConnection, @Nullable String str, @NonNull String str2, @NonNull String str3) {
        InputStream errorStream = httpURLConnection.getErrorStream();
        String str4 = null;
        if (errorStream != null) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(errorStream, f1787b));
            try {
                StringBuilder sb = new StringBuilder();
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    sb.append(readLine);
                    sb.append('\n');
                }
                str4 = String.format("Error when communicating with the Firebase Installations server API. HTTP response: [%d %s: %s]", Integer.valueOf(httpURLConnection.getResponseCode()), httpURLConnection.getResponseMessage(), sb);
            } catch (IOException unused) {
            } catch (Throwable th) {
                try {
                    bufferedReader.close();
                } catch (IOException unused2) {
                }
                throw th;
            }
            try {
                bufferedReader.close();
            } catch (IOException unused3) {
            }
        }
        if (!TextUtils.isEmpty(str4)) {
            Log.w("Firebase-Installations", str4);
            Object[] objArr = new Object[3];
            objArr[0] = str2;
            objArr[1] = str3;
            objArr[2] = TextUtils.isEmpty(str) ? "" : b.d.b.a.a.v(", ", str);
            Log.w("Firebase-Installations", String.format("Firebase options used while communicating with Firebase server APIs: %s, %s%s", objArr));
        }
    }

    public static long d(String str) {
        b.c.a.a0.d.o(a.matcher(str).matches(), "Invalid Expiration Timestamp.");
        if (str == null || str.length() == 0) {
            return 0L;
        }
        return Long.parseLong(str.substring(0, str.length() - 1));
    }

    public static void i(URLConnection uRLConnection, byte[] bArr) throws IOException {
        OutputStream outputStream = uRLConnection.getOutputStream();
        if (outputStream != null) {
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(outputStream);
            try {
                gZIPOutputStream.write(bArr);
            } finally {
                try {
                    gZIPOutputStream.close();
                    outputStream.close();
                } catch (IOException unused) {
                }
            }
        } else {
            throw new IOException("Cannot send request to FIS servers. No OutputStream available.");
        }
    }

    public final URL a(String str) throws FirebaseInstallationsException {
        try {
            return new URL(String.format("https://%s/%s/%s", "firebaseinstallations.googleapis.com", "v1", str));
        } catch (MalformedURLException e) {
            throw new FirebaseInstallationsException(e.getMessage(), FirebaseInstallationsException.a.UNAVAILABLE);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:21:0x00b1 A[Catch: NameNotFoundException -> 0x00d1, TryCatch #1 {NameNotFoundException -> 0x00d1, blocks: (B:11:0x007a, B:13:0x0094, B:15:0x0098, B:18:0x00a1, B:21:0x00b1, B:22:0x00cc), top: B:31:0x007a }] */
    /* JADX WARN: Removed duplicated region for block: B:22:0x00cc A[Catch: NameNotFoundException -> 0x00d1, TRY_LEAVE, TryCatch #1 {NameNotFoundException -> 0x00d1, blocks: (B:11:0x007a, B:13:0x0094, B:15:0x0098, B:18:0x00a1, B:21:0x00b1, B:22:0x00cc), top: B:31:0x007a }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.net.HttpURLConnection c(java.net.URL r7, java.lang.String r8) throws com.google.firebase.installations.FirebaseInstallationsException {
        /*
            r6 = this;
            java.net.URLConnection r7 = r7.openConnection()     // Catch: java.io.IOException -> Lf3
            java.net.HttpURLConnection r7 = (java.net.HttpURLConnection) r7     // Catch: java.io.IOException -> Lf3
            r0 = 10000(0x2710, float:1.4013E-41)
            r7.setConnectTimeout(r0)
            r1 = 0
            r7.setUseCaches(r1)
            r7.setReadTimeout(r0)
            java.lang.String r0 = "application/json"
            java.lang.String r2 = "Content-Type"
            r7.addRequestProperty(r2, r0)
            java.lang.String r2 = "Accept"
            r7.addRequestProperty(r2, r0)
            java.lang.String r0 = "Content-Encoding"
            java.lang.String r2 = "gzip"
            r7.addRequestProperty(r0, r2)
            java.lang.String r0 = "Cache-Control"
            java.lang.String r2 = "no-cache"
            r7.addRequestProperty(r0, r2)
            android.content.Context r0 = r6.c
            java.lang.String r0 = r0.getPackageName()
            java.lang.String r2 = "X-Android-Package"
            r7.addRequestProperty(r2, r0)
            b.i.c.t.a<b.i.c.r.d> r0 = r6.e
            java.lang.Object r0 = r0.get()
            if (r0 == 0) goto L77
            b.i.c.t.a<b.i.c.x.h> r0 = r6.d
            java.lang.Object r0 = r0.get()
            if (r0 == 0) goto L77
            b.i.c.t.a<b.i.c.r.d> r0 = r6.e
            java.lang.Object r0 = r0.get()
            b.i.c.r.d r0 = (b.i.c.r.d) r0
            java.lang.String r2 = "fire-installations-id"
            b.i.c.r.d$a r0 = r0.a(r2)
            b.i.c.r.d$a r2 = b.i.c.r.d.a.NONE
            if (r0 == r2) goto L77
            b.i.c.t.a<b.i.c.x.h> r2 = r6.d
            java.lang.Object r2 = r2.get()
            b.i.c.x.h r2 = (b.i.c.x.h) r2
            java.lang.String r2 = r2.getUserAgent()
            java.lang.String r3 = "x-firebase-client"
            r7.addRequestProperty(r3, r2)
            int r0 = r0.f()
            java.lang.String r0 = java.lang.Integer.toString(r0)
            java.lang.String r2 = "x-firebase-client-log-type"
            r7.addRequestProperty(r2, r0)
        L77:
            java.lang.String r0 = "ContentValues"
            r2 = 0
            android.content.Context r3 = r6.c     // Catch: android.content.pm.PackageManager.NameNotFoundException -> Ld1
            java.lang.String r4 = r3.getPackageName()     // Catch: android.content.pm.PackageManager.NameNotFoundException -> Ld1
            b.i.a.f.e.p.a r3 = b.i.a.f.e.p.b.a(r3)     // Catch: android.content.pm.PackageManager.NameNotFoundException -> Ld1
            android.content.Context r3 = r3.a     // Catch: android.content.pm.PackageManager.NameNotFoundException -> Ld1
            android.content.pm.PackageManager r3 = r3.getPackageManager()     // Catch: android.content.pm.PackageManager.NameNotFoundException -> Ld1
            r5 = 64
            android.content.pm.PackageInfo r3 = r3.getPackageInfo(r4, r5)     // Catch: android.content.pm.PackageManager.NameNotFoundException -> Ld1
            android.content.pm.Signature[] r4 = r3.signatures     // Catch: android.content.pm.PackageManager.NameNotFoundException -> Ld1
            if (r4 == 0) goto Lae
            int r4 = r4.length     // Catch: android.content.pm.PackageManager.NameNotFoundException -> Ld1
            r5 = 1
            if (r4 != r5) goto Lae
            java.lang.String r4 = "SHA1"
            java.security.MessageDigest r4 = b.i.a.f.e.o.a.a(r4)     // Catch: android.content.pm.PackageManager.NameNotFoundException -> Ld1
            if (r4 != 0) goto La1
            goto Lae
        La1:
            android.content.pm.Signature[] r3 = r3.signatures     // Catch: android.content.pm.PackageManager.NameNotFoundException -> Ld1
            r3 = r3[r1]     // Catch: android.content.pm.PackageManager.NameNotFoundException -> Ld1
            byte[] r3 = r3.toByteArray()     // Catch: android.content.pm.PackageManager.NameNotFoundException -> Ld1
            byte[] r3 = r4.digest(r3)     // Catch: android.content.pm.PackageManager.NameNotFoundException -> Ld1
            goto Laf
        Lae:
            r3 = r2
        Laf:
            if (r3 != 0) goto Lcc
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: android.content.pm.PackageManager.NameNotFoundException -> Ld1
            r1.<init>()     // Catch: android.content.pm.PackageManager.NameNotFoundException -> Ld1
            java.lang.String r3 = "Could not get fingerprint hash for package: "
            r1.append(r3)     // Catch: android.content.pm.PackageManager.NameNotFoundException -> Ld1
            android.content.Context r3 = r6.c     // Catch: android.content.pm.PackageManager.NameNotFoundException -> Ld1
            java.lang.String r3 = r3.getPackageName()     // Catch: android.content.pm.PackageManager.NameNotFoundException -> Ld1
            r1.append(r3)     // Catch: android.content.pm.PackageManager.NameNotFoundException -> Ld1
            java.lang.String r1 = r1.toString()     // Catch: android.content.pm.PackageManager.NameNotFoundException -> Ld1
            android.util.Log.e(r0, r1)     // Catch: android.content.pm.PackageManager.NameNotFoundException -> Ld1
            goto Le8
        Lcc:
            java.lang.String r2 = b.i.a.f.e.o.d.a(r3, r1)     // Catch: android.content.pm.PackageManager.NameNotFoundException -> Ld1
            goto Le8
        Ld1:
            r1 = move-exception
            java.lang.String r3 = "No such package: "
            java.lang.StringBuilder r3 = b.d.b.a.a.R(r3)
            android.content.Context r4 = r6.c
            java.lang.String r4 = r4.getPackageName()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            android.util.Log.e(r0, r3, r1)
        Le8:
            java.lang.String r0 = "X-Android-Cert"
            r7.addRequestProperty(r0, r2)
            java.lang.String r0 = "x-goog-api-key"
            r7.addRequestProperty(r0, r8)
            return r7
        Lf3:
            com.google.firebase.installations.FirebaseInstallationsException r7 = new com.google.firebase.installations.FirebaseInstallationsException
            com.google.firebase.installations.FirebaseInstallationsException$a r8 = com.google.firebase.installations.FirebaseInstallationsException.a.UNAVAILABLE
            java.lang.String r0 = "Firebase Installations Service is unavailable. Please try again later."
            r7.<init>(r0, r8)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.c.u.p.c.c(java.net.URL, java.lang.String):java.net.HttpURLConnection");
    }

    public final d e(HttpURLConnection httpURLConnection) throws AssertionError, IOException {
        InputStream inputStream = httpURLConnection.getInputStream();
        JsonReader jsonReader = new JsonReader(new InputStreamReader(inputStream, f1787b));
        f.a a2 = f.a();
        jsonReader.beginObject();
        String str = null;
        String str2 = null;
        String str3 = null;
        f fVar = null;
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_NAME)) {
                str = jsonReader.nextString();
            } else if (nextName.equals("fid")) {
                str2 = jsonReader.nextString();
            } else if (nextName.equals("refreshToken")) {
                str3 = jsonReader.nextString();
            } else if (nextName.equals("authToken")) {
                jsonReader.beginObject();
                while (jsonReader.hasNext()) {
                    String nextName2 = jsonReader.nextName();
                    if (nextName2.equals("token")) {
                        ((b.C0153b) a2).a = jsonReader.nextString();
                    } else if (nextName2.equals("expiresIn")) {
                        a2.b(d(jsonReader.nextString()));
                    } else {
                        jsonReader.skipValue();
                    }
                }
                fVar = a2.a();
                jsonReader.endObject();
            } else {
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();
        jsonReader.close();
        inputStream.close();
        return new a(str, str2, str3, fVar, d.a.OK, null);
    }

    public final f f(HttpURLConnection httpURLConnection) throws AssertionError, IOException {
        InputStream inputStream = httpURLConnection.getInputStream();
        JsonReader jsonReader = new JsonReader(new InputStreamReader(inputStream, f1787b));
        f.a a2 = f.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (nextName.equals("token")) {
                ((b.C0153b) a2).a = jsonReader.nextString();
            } else if (nextName.equals("expiresIn")) {
                a2.b(d(jsonReader.nextString()));
            } else {
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();
        jsonReader.close();
        inputStream.close();
        b.C0153b bVar = (b.C0153b) a2;
        bVar.c = f.b.OK;
        return bVar.a();
    }

    public final void g(HttpURLConnection httpURLConnection, @Nullable String str, @NonNull String str2) throws IOException {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("fid", str);
            jSONObject.put("appId", str2);
            jSONObject.put("authVersion", "FIS_v2");
            jSONObject.put("sdkVersion", "a:16.3.4");
            i(httpURLConnection, jSONObject.toString().getBytes(Constants.ENCODING));
        } catch (JSONException e) {
            throw new IllegalStateException(e);
        }
    }

    public final void h(HttpURLConnection httpURLConnection) throws IOException {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("sdkVersion", "a:16.3.4");
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("installation", jSONObject);
            i(httpURLConnection, jSONObject2.toString().getBytes(Constants.ENCODING));
        } catch (JSONException e) {
            throw new IllegalStateException(e);
        }
    }
}
