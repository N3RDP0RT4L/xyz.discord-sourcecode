package b.i.c.u.p;

import androidx.annotation.GuardedBy;
import b.i.c.u.n;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
/* compiled from: RequestLimiter.java */
/* loaded from: classes3.dex */
public class e {
    public static final long a = TimeUnit.HOURS.toMillis(24);

    /* renamed from: b  reason: collision with root package name */
    public static final long f1788b = TimeUnit.MINUTES.toMillis(30);
    public final n c = n.c();
    @GuardedBy("this")
    public long d;
    @GuardedBy("this")
    public int e;

    public synchronized boolean a() {
        boolean z2;
        if (this.e != 0) {
            if (this.c.a() <= this.d) {
                z2 = false;
            }
        }
        z2 = true;
        return z2;
    }

    public synchronized void b(int i) {
        long j;
        boolean z2 = false;
        if ((i >= 200 && i < 300) || i == 401 || i == 404) {
            synchronized (this) {
                this.e = 0;
            }
            return;
        }
        this.e++;
        synchronized (this) {
            if (i == 429 || (i >= 500 && i < 600)) {
                z2 = true;
            }
            if (!z2) {
                j = a;
            } else {
                double pow = Math.pow(2.0d, this.e);
                Objects.requireNonNull(this.c);
                j = (long) Math.min(pow + ((long) (Math.random() * 1000.0d)), f1788b);
            }
            this.d = this.c.a() + j;
        }
    }
}
