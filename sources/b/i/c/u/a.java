package b.i.c.u;

import androidx.annotation.NonNull;
/* compiled from: com.google.firebase:firebase-installations-interop@@16.0.0 */
/* loaded from: classes3.dex */
public final class a extends k {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final long f1774b;
    public final long c;

    public a(String str, long j, long j2, C0150a aVar) {
        this.a = str;
        this.f1774b = j;
        this.c = j2;
    }

    @Override // b.i.c.u.k
    @NonNull
    public String a() {
        return this.a;
    }

    @Override // b.i.c.u.k
    @NonNull
    public long b() {
        return this.c;
    }

    @Override // b.i.c.u.k
    @NonNull
    public long c() {
        return this.f1774b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof k)) {
            return false;
        }
        k kVar = (k) obj;
        return this.a.equals(kVar.a()) && this.f1774b == kVar.c() && this.c == kVar.b();
    }

    public int hashCode() {
        long j = this.f1774b;
        long j2 = this.c;
        return ((((this.a.hashCode() ^ 1000003) * 1000003) ^ ((int) (j ^ (j >>> 32)))) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)));
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("InstallationTokenResult{token=");
        R.append(this.a);
        R.append(", tokenExpirationTimestamp=");
        R.append(this.f1774b);
        R.append(", tokenCreationTimestamp=");
        return b.d.b.a.a.B(R, this.c, "}");
    }
}
