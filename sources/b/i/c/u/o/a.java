package b.i.c.u.o;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.c.u.o.c;
import b.i.c.u.o.d;
import java.util.Objects;
/* compiled from: AutoValue_PersistedInstallationEntry.java */
/* loaded from: classes3.dex */
public final class a extends d {

    /* renamed from: b  reason: collision with root package name */
    public final String f1780b;
    public final c.a c;
    public final String d;
    public final String e;
    public final long f;
    public final long g;
    public final String h;

    /* compiled from: AutoValue_PersistedInstallationEntry.java */
    /* loaded from: classes3.dex */
    public static final class b extends d.a {
        public String a;

        /* renamed from: b  reason: collision with root package name */
        public c.a f1781b;
        public String c;
        public String d;
        public Long e;
        public Long f;
        public String g;

        public b() {
        }

        @Override // b.i.c.u.o.d.a
        public d a() {
            String str = this.f1781b == null ? " registrationStatus" : "";
            if (this.e == null) {
                str = b.d.b.a.a.v(str, " expiresInSecs");
            }
            if (this.f == null) {
                str = b.d.b.a.a.v(str, " tokenCreationEpochInSecs");
            }
            if (str.isEmpty()) {
                return new a(this.a, this.f1781b, this.c, this.d, this.e.longValue(), this.f.longValue(), this.g, null);
            }
            throw new IllegalStateException(b.d.b.a.a.v("Missing required properties:", str));
        }

        @Override // b.i.c.u.o.d.a
        public d.a b(c.a aVar) {
            Objects.requireNonNull(aVar, "Null registrationStatus");
            this.f1781b = aVar;
            return this;
        }

        public d.a c(long j) {
            this.e = Long.valueOf(j);
            return this;
        }

        public d.a d(long j) {
            this.f = Long.valueOf(j);
            return this;
        }

        public b(d dVar, C0151a aVar) {
            a aVar2 = (a) dVar;
            this.a = aVar2.f1780b;
            this.f1781b = aVar2.c;
            this.c = aVar2.d;
            this.d = aVar2.e;
            this.e = Long.valueOf(aVar2.f);
            this.f = Long.valueOf(aVar2.g);
            this.g = aVar2.h;
        }
    }

    public a(String str, c.a aVar, String str2, String str3, long j, long j2, String str4, C0151a aVar2) {
        this.f1780b = str;
        this.c = aVar;
        this.d = str2;
        this.e = str3;
        this.f = j;
        this.g = j2;
        this.h = str4;
    }

    @Override // b.i.c.u.o.d
    @Nullable
    public String a() {
        return this.d;
    }

    @Override // b.i.c.u.o.d
    public long b() {
        return this.f;
    }

    @Override // b.i.c.u.o.d
    @Nullable
    public String c() {
        return this.f1780b;
    }

    @Override // b.i.c.u.o.d
    @Nullable
    public String d() {
        return this.h;
    }

    @Override // b.i.c.u.o.d
    @Nullable
    public String e() {
        return this.e;
    }

    public boolean equals(Object obj) {
        String str;
        String str2;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof d)) {
            return false;
        }
        d dVar = (d) obj;
        String str3 = this.f1780b;
        if (str3 != null ? str3.equals(dVar.c()) : dVar.c() == null) {
            if (this.c.equals(dVar.f()) && ((str = this.d) != null ? str.equals(dVar.a()) : dVar.a() == null) && ((str2 = this.e) != null ? str2.equals(dVar.e()) : dVar.e() == null) && this.f == dVar.b() && this.g == dVar.g()) {
                String str4 = this.h;
                if (str4 == null) {
                    if (dVar.d() == null) {
                        return true;
                    }
                } else if (str4.equals(dVar.d())) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override // b.i.c.u.o.d
    @NonNull
    public c.a f() {
        return this.c;
    }

    @Override // b.i.c.u.o.d
    public long g() {
        return this.g;
    }

    public int hashCode() {
        String str = this.f1780b;
        int i = 0;
        int hashCode = ((((str == null ? 0 : str.hashCode()) ^ 1000003) * 1000003) ^ this.c.hashCode()) * 1000003;
        String str2 = this.d;
        int hashCode2 = (hashCode ^ (str2 == null ? 0 : str2.hashCode())) * 1000003;
        String str3 = this.e;
        int hashCode3 = str3 == null ? 0 : str3.hashCode();
        long j = this.f;
        long j2 = this.g;
        int i2 = (((((hashCode2 ^ hashCode3) * 1000003) ^ ((int) (j ^ (j >>> 32)))) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)))) * 1000003;
        String str4 = this.h;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return i2 ^ i;
    }

    @Override // b.i.c.u.o.d
    public d.a k() {
        return new b(this, null);
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("PersistedInstallationEntry{firebaseInstallationId=");
        R.append(this.f1780b);
        R.append(", registrationStatus=");
        R.append(this.c);
        R.append(", authToken=");
        R.append(this.d);
        R.append(", refreshToken=");
        R.append(this.e);
        R.append(", expiresInSecs=");
        R.append(this.f);
        R.append(", tokenCreationEpochInSecs=");
        R.append(this.g);
        R.append(", fisError=");
        return b.d.b.a.a.H(R, this.h, "}");
    }
}
