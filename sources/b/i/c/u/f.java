package b.i.c.u;

import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.GuardedBy;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.browser.trusted.sharing.ShareTarget;
import b.i.c.c;
import b.i.c.r.d;
import b.i.c.u.o.a;
import b.i.c.u.o.b;
import b.i.c.u.o.c;
import b.i.c.u.o.d;
import b.i.c.u.p.b;
import b.i.c.u.p.d;
import b.i.c.u.p.f;
import b.i.c.x.h;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.installations.FirebaseInstallationsException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;
/* compiled from: FirebaseInstallations.java */
/* loaded from: classes3.dex */
public class f implements g {
    public static final Object a = new Object();

    /* renamed from: b  reason: collision with root package name */
    public static final ThreadFactory f1776b = new a();
    public final c c;
    public final b.i.c.u.p.c d;
    public final b.i.c.u.o.c e;
    public final n f;
    public final b g;
    public final l h;
    public final ExecutorService j;
    public final ExecutorService k;
    @GuardedBy("this")
    public String l;
    public final Object i = new Object();
    @GuardedBy("lock")
    public final List<m> m = new ArrayList();

    /* compiled from: FirebaseInstallations.java */
    /* loaded from: classes3.dex */
    public class a implements ThreadFactory {
        public final AtomicInteger j = new AtomicInteger(1);

        @Override // java.util.concurrent.ThreadFactory
        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, String.format("firebase-installations-executor-%d", Integer.valueOf(this.j.getAndIncrement())));
        }
    }

    public f(c cVar, @NonNull b.i.c.t.a<h> aVar, @NonNull b.i.c.t.a<d> aVar2) {
        TimeUnit timeUnit = TimeUnit.SECONDS;
        LinkedBlockingQueue linkedBlockingQueue = new LinkedBlockingQueue();
        ThreadFactory threadFactory = f1776b;
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(0, 1, 30L, timeUnit, linkedBlockingQueue, threadFactory);
        cVar.a();
        b.i.c.u.p.c cVar2 = new b.i.c.u.p.c(cVar.d, aVar, aVar2);
        b.i.c.u.o.c cVar3 = new b.i.c.u.o.c(cVar);
        n c = n.c();
        b bVar = new b(cVar);
        l lVar = new l();
        this.c = cVar;
        this.d = cVar2;
        this.e = cVar3;
        this.f = c;
        this.g = bVar;
        this.h = lVar;
        this.j = threadPoolExecutor;
        this.k = new ThreadPoolExecutor(0, 1, 30L, timeUnit, new LinkedBlockingQueue(), threadFactory);
    }

    @NonNull
    public static f f() {
        c b2 = c.b();
        b.c.a.a0.d.o(true, "Null is not a valid value of FirebaseApp.");
        b2.a();
        return (f) b2.g.a(g.class);
    }

    @Override // b.i.c.u.g
    @NonNull
    public Task<k> a(final boolean z2) {
        h();
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        i iVar = new i(this.f, taskCompletionSource);
        synchronized (this.i) {
            this.m.add(iVar);
        }
        Task task = taskCompletionSource.a;
        this.j.execute(new Runnable(this, z2) { // from class: b.i.c.u.d
            public final f j;
            public final boolean k;

            {
                this.j = this;
                this.k = z2;
            }

            @Override // java.lang.Runnable
            public void run() {
                f fVar = this.j;
                boolean z3 = this.k;
                Object obj = f.a;
                fVar.b(z3);
            }
        });
        return task;
    }

    public final void b(final boolean z2) {
        b.i.c.u.o.d b2;
        synchronized (a) {
            c cVar = this.c;
            cVar.a();
            b a2 = b.a(cVar.d, "generatefid.lock");
            b2 = this.e.b();
            if (b2.i()) {
                String i = i(b2);
                b.i.c.u.o.c cVar2 = this.e;
                a.b bVar = (a.b) b2.k();
                bVar.a = i;
                bVar.b(c.a.UNREGISTERED);
                b2 = bVar.a();
                cVar2.a(b2);
            }
            if (a2 != null) {
                a2.b();
            }
        }
        if (z2) {
            a.b bVar2 = (a.b) b2.k();
            bVar2.c = null;
            b2 = bVar2.a();
        }
        l(b2);
        this.k.execute(new Runnable(this, z2) { // from class: b.i.c.u.e
            public final f j;
            public final boolean k;

            {
                this.j = this;
                this.k = z2;
            }

            /* JADX WARN: Removed duplicated region for block: B:52:0x004d A[EXC_TOP_SPLITTER, SYNTHETIC] */
            @Override // java.lang.Runnable
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public void run() {
                /*
                    r6 = this;
                    b.i.c.u.f r0 = r6.j
                    boolean r1 = r6.k
                    java.lang.Object r2 = b.i.c.u.f.a
                    java.util.Objects.requireNonNull(r0)
                    java.lang.Object r2 = b.i.c.u.f.a
                    monitor-enter(r2)
                    b.i.c.c r3 = r0.c     // Catch: java.lang.Throwable -> Lb4
                    r3.a()     // Catch: java.lang.Throwable -> Lb4
                    android.content.Context r3 = r3.d     // Catch: java.lang.Throwable -> Lb4
                    java.lang.String r4 = "generatefid.lock"
                    b.i.c.u.b r3 = b.i.c.u.b.a(r3, r4)     // Catch: java.lang.Throwable -> Lb4
                    b.i.c.u.o.c r4 = r0.e     // Catch: java.lang.Throwable -> Lad
                    b.i.c.u.o.d r4 = r4.b()     // Catch: java.lang.Throwable -> Lad
                    if (r3 == 0) goto L24
                    r3.b()     // Catch: java.lang.Throwable -> Lb4
                L24:
                    monitor-exit(r2)     // Catch: java.lang.Throwable -> Lb4
                    boolean r3 = r4.h()     // Catch: com.google.firebase.installations.FirebaseInstallationsException -> La8
                    if (r3 != 0) goto L48
                    b.i.c.u.o.c$a r3 = r4.f()     // Catch: com.google.firebase.installations.FirebaseInstallationsException -> La8
                    b.i.c.u.o.c$a r5 = b.i.c.u.o.c.a.UNREGISTERED     // Catch: com.google.firebase.installations.FirebaseInstallationsException -> La8
                    if (r3 != r5) goto L35
                    r3 = 1
                    goto L36
                L35:
                    r3 = 0
                L36:
                    if (r3 == 0) goto L39
                    goto L48
                L39:
                    if (r1 != 0) goto L43
                    b.i.c.u.n r1 = r0.f     // Catch: com.google.firebase.installations.FirebaseInstallationsException -> La8
                    boolean r1 = r1.d(r4)     // Catch: com.google.firebase.installations.FirebaseInstallationsException -> La8
                    if (r1 == 0) goto Lac
                L43:
                    b.i.c.u.o.d r1 = r0.c(r4)     // Catch: com.google.firebase.installations.FirebaseInstallationsException -> La8
                    goto L4c
                L48:
                    b.i.c.u.o.d r1 = r0.j(r4)     // Catch: com.google.firebase.installations.FirebaseInstallationsException -> La8
                L4c:
                    monitor-enter(r2)
                    b.i.c.c r3 = r0.c     // Catch: java.lang.Throwable -> La5
                    r3.a()     // Catch: java.lang.Throwable -> La5
                    android.content.Context r3 = r3.d     // Catch: java.lang.Throwable -> La5
                    java.lang.String r4 = "generatefid.lock"
                    b.i.c.u.b r3 = b.i.c.u.b.a(r3, r4)     // Catch: java.lang.Throwable -> La5
                    b.i.c.u.o.c r4 = r0.e     // Catch: java.lang.Throwable -> L9e
                    r4.a(r1)     // Catch: java.lang.Throwable -> L9e
                    if (r3 == 0) goto L64
                    r3.b()     // Catch: java.lang.Throwable -> La5
                L64:
                    monitor-exit(r2)     // Catch: java.lang.Throwable -> La5
                    boolean r2 = r1.j()
                    if (r2 == 0) goto L78
                    r2 = r1
                    b.i.c.u.o.a r2 = (b.i.c.u.o.a) r2
                    java.lang.String r2 = r2.f1780b
                    monitor-enter(r0)
                    r0.l = r2     // Catch: java.lang.Throwable -> L75
                    monitor-exit(r0)
                    goto L78
                L75:
                    r1 = move-exception
                    monitor-exit(r0)
                    throw r1
                L78:
                    boolean r2 = r1.h()
                    if (r2 == 0) goto L89
                    com.google.firebase.installations.FirebaseInstallationsException r1 = new com.google.firebase.installations.FirebaseInstallationsException
                    com.google.firebase.installations.FirebaseInstallationsException$a r2 = com.google.firebase.installations.FirebaseInstallationsException.a.BAD_CONFIG
                    r1.<init>(r2)
                    r0.k(r1)
                    goto Lac
                L89:
                    boolean r2 = r1.i()
                    if (r2 == 0) goto L9a
                    java.io.IOException r1 = new java.io.IOException
                    java.lang.String r2 = "Installation ID could not be validated with the Firebase servers (maybe it was deleted). Firebase Installations will need to create a new Installation ID and auth token. Please retry your last request."
                    r1.<init>(r2)
                    r0.k(r1)
                    goto Lac
                L9a:
                    r0.l(r1)
                    goto Lac
                L9e:
                    r0 = move-exception
                    if (r3 == 0) goto La4
                    r3.b()     // Catch: java.lang.Throwable -> La5
                La4:
                    throw r0     // Catch: java.lang.Throwable -> La5
                La5:
                    r0 = move-exception
                    monitor-exit(r2)     // Catch: java.lang.Throwable -> La5
                    throw r0
                La8:
                    r1 = move-exception
                    r0.k(r1)
                Lac:
                    return
                Lad:
                    r0 = move-exception
                    if (r3 == 0) goto Lb3
                    r3.b()     // Catch: java.lang.Throwable -> Lb4
                Lb3:
                    throw r0     // Catch: java.lang.Throwable -> Lb4
                Lb4:
                    r0 = move-exception
                    monitor-exit(r2)     // Catch: java.lang.Throwable -> Lb4
                    throw r0
                */
                throw new UnsupportedOperationException("Method not decompiled: b.i.c.u.e.run():void");
            }
        });
    }

    public final b.i.c.u.o.d c(@NonNull b.i.c.u.o.d dVar) throws FirebaseInstallationsException {
        int responseCode;
        b.i.c.u.p.f f;
        FirebaseInstallationsException.a aVar = FirebaseInstallationsException.a.UNAVAILABLE;
        b.i.c.u.p.c cVar = this.d;
        String d = d();
        b.i.c.u.o.a aVar2 = (b.i.c.u.o.a) dVar;
        String str = aVar2.f1780b;
        String g = g();
        String str2 = aVar2.e;
        if (cVar.f.a()) {
            URL a2 = cVar.a(String.format("projects/%s/installations/%s/authTokens:generate", g, str));
            for (int i = 0; i <= 1; i++) {
                HttpURLConnection c = cVar.c(a2, d);
                try {
                    c.setRequestMethod(ShareTarget.METHOD_POST);
                    c.addRequestProperty("Authorization", "FIS_v2 " + str2);
                    c.setDoOutput(true);
                    cVar.h(c);
                    responseCode = c.getResponseCode();
                    cVar.f.b(responseCode);
                } catch (IOException | AssertionError unused) {
                } catch (Throwable th) {
                    c.disconnect();
                    throw th;
                }
                if (responseCode >= 200 && responseCode < 300) {
                    f = cVar.f(c);
                } else {
                    b.i.c.u.p.c.b(c, null, d, g);
                    if (!(responseCode == 401 || responseCode == 404)) {
                        if (responseCode == 429) {
                            throw new FirebaseInstallationsException("Firebase servers have received too many requests from this client in a short period of time. Please try again later.", FirebaseInstallationsException.a.TOO_MANY_REQUESTS);
                        } else if (responseCode < 500 || responseCode >= 600) {
                            Log.e("Firebase-Installations", "Firebase Installations can not communicate with Firebase server APIs due to invalid configuration. Please update your Firebase initialization process and set valid Firebase options (API key, Project ID, Application ID) when initializing Firebase.");
                            b.C0153b bVar = (b.C0153b) b.i.c.u.p.f.a();
                            bVar.c = f.b.BAD_CONFIG;
                            f = bVar.a();
                        } else {
                            c.disconnect();
                        }
                    }
                    b.C0153b bVar2 = (b.C0153b) b.i.c.u.p.f.a();
                    bVar2.c = f.b.AUTH_ERROR;
                    f = bVar2.a();
                }
                c.disconnect();
                b.i.c.u.p.b bVar3 = (b.i.c.u.p.b) f;
                int ordinal = bVar3.c.ordinal();
                if (ordinal == 0) {
                    String str3 = bVar3.a;
                    long j = bVar3.f1785b;
                    long b2 = this.f.b();
                    a.b bVar4 = (a.b) dVar.k();
                    bVar4.c = str3;
                    bVar4.e = Long.valueOf(j);
                    bVar4.f = Long.valueOf(b2);
                    return bVar4.a();
                } else if (ordinal == 1) {
                    a.b bVar5 = (a.b) dVar.k();
                    bVar5.g = "BAD CONFIG";
                    bVar5.b(c.a.REGISTER_ERROR);
                    return bVar5.a();
                } else if (ordinal == 2) {
                    synchronized (this) {
                        this.l = null;
                    }
                    d.a k = dVar.k();
                    k.b(c.a.NOT_GENERATED);
                    return k.a();
                } else {
                    throw new FirebaseInstallationsException("Firebase Installations Service is unavailable. Please try again later.", aVar);
                }
            }
            throw new FirebaseInstallationsException("Firebase Installations Service is unavailable. Please try again later.", aVar);
        }
        throw new FirebaseInstallationsException("Firebase Installations Service is unavailable. Please try again later.", aVar);
    }

    @Nullable
    public String d() {
        b.i.c.c cVar = this.c;
        cVar.a();
        return cVar.f.a;
    }

    @VisibleForTesting
    public String e() {
        b.i.c.c cVar = this.c;
        cVar.a();
        return cVar.f.f1651b;
    }

    @Nullable
    public String g() {
        b.i.c.c cVar = this.c;
        cVar.a();
        return cVar.f.g;
    }

    @Override // b.i.c.u.g
    @NonNull
    public Task<String> getId() {
        String str;
        h();
        synchronized (this) {
            str = this.l;
        }
        if (str != null) {
            return b.i.a.f.e.o.f.Z(str);
        }
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        j jVar = new j(taskCompletionSource);
        synchronized (this.i) {
            this.m.add(jVar);
        }
        Task task = taskCompletionSource.a;
        this.j.execute(new Runnable(this) { // from class: b.i.c.u.c
            public final f j;

            {
                this.j = this;
            }

            @Override // java.lang.Runnable
            public void run() {
                f fVar = this.j;
                Object obj = f.a;
                fVar.b(false);
            }
        });
        return task;
    }

    public final void h() {
        b.c.a.a0.d.v(e(), "Please set your Application ID. A valid Firebase App ID is required to communicate with Firebase server APIs: It identifies your application with Firebase.Please refer to https://firebase.google.com/support/privacy/init-options.");
        b.c.a.a0.d.v(g(), "Please set your Project ID. A valid Firebase Project ID is required to communicate with Firebase server APIs: It identifies your application with Firebase.Please refer to https://firebase.google.com/support/privacy/init-options.");
        b.c.a.a0.d.v(d(), "Please set a valid API key. A Firebase API key is required to communicate with Firebase server APIs: It authenticates your project with Google.Please refer to https://firebase.google.com/support/privacy/init-options.");
        String e = e();
        Pattern pattern = n.f1779b;
        b.c.a.a0.d.o(e.contains(":"), "Please set your Application ID. A valid Firebase App ID is required to communicate with Firebase server APIs: It identifies your application with Firebase.Please refer to https://firebase.google.com/support/privacy/init-options.");
        b.c.a.a0.d.o(n.f1779b.matcher(d()).matches(), "Please set a valid API key. A Firebase API key is required to communicate with Firebase server APIs: It authenticates your project with Google.Please refer to https://firebase.google.com/support/privacy/init-options.");
    }

    public final String i(b.i.c.u.o.d dVar) {
        String string;
        b.i.c.c cVar = this.c;
        cVar.a();
        if (cVar.e.equals("CHIME_ANDROID_SDK") || this.c.h()) {
            if (((b.i.c.u.o.a) dVar).c == c.a.ATTEMPT_MIGRATION) {
                b.i.c.u.o.b bVar = this.g;
                synchronized (bVar.f1782b) {
                    synchronized (bVar.f1782b) {
                        string = bVar.f1782b.getString("|S|id", null);
                    }
                    if (string == null) {
                        string = bVar.a();
                    }
                }
                return TextUtils.isEmpty(string) ? this.h.a() : string;
            }
        }
        return this.h.a();
    }

    public final b.i.c.u.o.d j(b.i.c.u.o.d dVar) throws FirebaseInstallationsException {
        int responseCode;
        b.i.c.u.p.d e;
        FirebaseInstallationsException.a aVar = FirebaseInstallationsException.a.UNAVAILABLE;
        b.i.c.u.o.a aVar2 = (b.i.c.u.o.a) dVar;
        String str = aVar2.f1780b;
        String str2 = null;
        if (str != null && str.length() == 11) {
            b.i.c.u.o.b bVar = this.g;
            synchronized (bVar.f1782b) {
                String[] strArr = b.i.c.u.o.b.a;
                int length = strArr.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    }
                    String str3 = strArr[i];
                    String str4 = bVar.c;
                    String string = bVar.f1782b.getString("|T|" + str4 + "|" + str3, null);
                    if (string == null || string.isEmpty()) {
                        i++;
                    } else if (string.startsWith("{")) {
                        try {
                            str2 = new JSONObject(string).getString("token");
                        } catch (JSONException unused) {
                        }
                    } else {
                        str2 = string;
                    }
                }
            }
        }
        b.i.c.u.p.c cVar = this.d;
        String d = d();
        String str5 = aVar2.f1780b;
        String g = g();
        String e2 = e();
        if (cVar.f.a()) {
            URL a2 = cVar.a(String.format("projects/%s/installations", g));
            for (int i2 = 0; i2 <= 1; i2++) {
                HttpURLConnection c = cVar.c(a2, d);
                try {
                    c.setRequestMethod(ShareTarget.METHOD_POST);
                    c.setDoOutput(true);
                    if (str2 != null) {
                        c.addRequestProperty("x-goog-fis-android-iid-migration-auth", str2);
                    }
                    cVar.g(c, str5, e2);
                    responseCode = c.getResponseCode();
                    cVar.f.b(responseCode);
                } catch (IOException | AssertionError unused2) {
                } catch (Throwable th) {
                    c.disconnect();
                    throw th;
                }
                if (responseCode >= 200 && responseCode < 300) {
                    e = cVar.e(c);
                    c.disconnect();
                } else {
                    b.i.c.u.p.c.b(c, e2, d, g);
                    if (responseCode == 429) {
                        throw new FirebaseInstallationsException("Firebase servers have received too many requests from this client in a short period of time. Please try again later.", FirebaseInstallationsException.a.TOO_MANY_REQUESTS);
                    } else if (responseCode < 500 || responseCode >= 600) {
                        Log.e("Firebase-Installations", "Firebase Installations can not communicate with Firebase server APIs due to invalid configuration. Please update your Firebase initialization process and set valid Firebase options (API key, Project ID, Application ID) when initializing Firebase.");
                        b.i.c.u.p.a aVar3 = new b.i.c.u.p.a(null, null, null, null, d.a.BAD_CONFIG, null);
                        c.disconnect();
                        e = aVar3;
                    } else {
                        c.disconnect();
                    }
                }
                b.i.c.u.p.a aVar4 = (b.i.c.u.p.a) e;
                int ordinal = aVar4.e.ordinal();
                if (ordinal == 0) {
                    String str6 = aVar4.f1784b;
                    String str7 = aVar4.c;
                    long b2 = this.f.b();
                    String c2 = aVar4.d.c();
                    long d2 = aVar4.d.d();
                    a.b bVar2 = (a.b) dVar.k();
                    bVar2.a = str6;
                    bVar2.b(c.a.REGISTERED);
                    bVar2.c = c2;
                    bVar2.d = str7;
                    bVar2.e = Long.valueOf(d2);
                    bVar2.f = Long.valueOf(b2);
                    return bVar2.a();
                } else if (ordinal == 1) {
                    a.b bVar3 = (a.b) dVar.k();
                    bVar3.g = "BAD CONFIG";
                    bVar3.b(c.a.REGISTER_ERROR);
                    return bVar3.a();
                } else {
                    throw new FirebaseInstallationsException("Firebase Installations Service is unavailable. Please try again later.", aVar);
                }
            }
            throw new FirebaseInstallationsException("Firebase Installations Service is unavailable. Please try again later.", aVar);
        }
        throw new FirebaseInstallationsException("Firebase Installations Service is unavailable. Please try again later.", aVar);
    }

    public final void k(Exception exc) {
        synchronized (this.i) {
            Iterator<m> it = this.m.iterator();
            while (it.hasNext()) {
                if (it.next().a(exc)) {
                    it.remove();
                }
            }
        }
    }

    public final void l(b.i.c.u.o.d dVar) {
        synchronized (this.i) {
            Iterator<m> it = this.m.iterator();
            while (it.hasNext()) {
                if (it.next().b(dVar)) {
                    it.remove();
                }
            }
        }
    }
}
