package b.i.c.s;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.GuardedBy;
import androidx.collection.ArrayMap;
import androidx.core.content.ContextCompat;
import b.i.c.c;
import com.google.firebase.iid.FirebaseInstanceId;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;
/* compiled from: com.google.firebase:firebase-iid@@21.0.0 */
/* loaded from: classes3.dex */
public class w {
    public final SharedPreferences a;

    /* renamed from: b  reason: collision with root package name */
    public final Context f1771b;
    @GuardedBy("this")
    public final Map<String, Long> c = new ArrayMap();

    /* compiled from: com.google.firebase:firebase-iid@@21.0.0 */
    /* loaded from: classes3.dex */
    public static class a {
        public static final long a = TimeUnit.DAYS.toMillis(7);

        /* renamed from: b  reason: collision with root package name */
        public static final /* synthetic */ int f1772b = 0;
        public final String c;
        public final String d;
        public final long e;

        public a(String str, String str2, long j) {
            this.c = str;
            this.d = str2;
            this.e = j;
        }

        public static String a(String str, String str2, long j) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("token", str);
                jSONObject.put("appVersion", str2);
                jSONObject.put("timestamp", j);
                return jSONObject.toString();
            } catch (JSONException e) {
                String valueOf = String.valueOf(e);
                b.d.b.a.a.f0(valueOf.length() + 24, "Failed to encode token: ", valueOf, "FirebaseInstanceId");
                return null;
            }
        }

        public static a b(String str) {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            if (!str.startsWith("{")) {
                return new a(str, null, 0L);
            }
            try {
                JSONObject jSONObject = new JSONObject(str);
                return new a(jSONObject.getString("token"), jSONObject.getString("appVersion"), jSONObject.getLong("timestamp"));
            } catch (JSONException e) {
                String valueOf = String.valueOf(e);
                b.d.b.a.a.f0(valueOf.length() + 23, "Failed to parse token: ", valueOf, "FirebaseInstanceId");
                return null;
            }
        }
    }

    public w(Context context) {
        boolean isEmpty;
        this.f1771b = context;
        SharedPreferences sharedPreferences = context.getSharedPreferences("com.google.android.gms.appid", 0);
        this.a = sharedPreferences;
        File file = new File(ContextCompat.getNoBackupFilesDir(context), "com.google.android.gms.appid-no-backup");
        if (!file.exists()) {
            try {
                if (file.createNewFile()) {
                    synchronized (this) {
                        isEmpty = sharedPreferences.getAll().isEmpty();
                    }
                    if (!isEmpty) {
                        Log.i("FirebaseInstanceId", "App restored, clearing state");
                        c();
                        w wVar = FirebaseInstanceId.f3083b;
                        synchronized (FirebaseInstanceId.getInstance(c.b())) {
                            FirebaseInstanceId.f3083b.c();
                        }
                    }
                }
            } catch (IOException e) {
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    String valueOf = String.valueOf(e.getMessage());
                    Log.d("FirebaseInstanceId", valueOf.length() != 0 ? "Error creating file in no backup dir: ".concat(valueOf) : new String("Error creating file in no backup dir: "));
                }
            }
        }
    }

    public static String a(String str, String str2) {
        return b.d.b.a.a.j(str2.length() + b.d.b.a.a.b(str, 3), str, "|S|", str2);
    }

    public final String b(String str, String str2, String str3) {
        StringBuilder Q = b.d.b.a.a.Q(b.d.b.a.a.b(str3, b.d.b.a.a.b(str2, b.d.b.a.a.b(str, 4))), str, "|T|", str2, "|");
        Q.append(str3);
        return Q.toString();
    }

    public synchronized void c() {
        this.c.clear();
        this.a.edit().clear().commit();
    }

    public final long d(String str) {
        long currentTimeMillis = System.currentTimeMillis();
        if (!this.a.contains(a(str, "cre"))) {
            SharedPreferences.Editor edit = this.a.edit();
            edit.putString(a(str, "cre"), String.valueOf(currentTimeMillis));
            edit.commit();
            return currentTimeMillis;
        }
        String string = this.a.getString(a(str, "cre"), null);
        if (string != null) {
            try {
                return Long.parseLong(string);
            } catch (NumberFormatException unused) {
            }
        }
        return 0L;
    }
}
