package b.i.c.s;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import androidx.annotation.GuardedBy;
import androidx.annotation.Nullable;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
/* compiled from: com.google.firebase:firebase-iid@@21.0.0 */
/* loaded from: classes3.dex */
public class d0 implements ServiceConnection {
    public final Context j;
    public final Intent k;
    public final ScheduledExecutorService l;
    @Nullable
    public a0 n;
    public final Queue<a> m = new ArrayDeque();
    @GuardedBy("this")
    public boolean o = false;

    /* compiled from: com.google.firebase:firebase-iid@@21.0.0 */
    /* loaded from: classes3.dex */
    public static class a {
        public final Intent a;

        /* renamed from: b  reason: collision with root package name */
        public final TaskCompletionSource<Void> f1761b = new TaskCompletionSource<>();

        public a(Intent intent) {
            this.a = intent;
        }

        public void a() {
            this.f1761b.b(null);
        }
    }

    public d0(Context context, String str) {
        ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(0, new b.i.a.f.e.o.j.a("Firebase-FirebaseInstanceIdServiceConnection"));
        Context applicationContext = context.getApplicationContext();
        this.j = applicationContext;
        this.k = new Intent(str).setPackage(applicationContext.getPackageName());
        this.l = scheduledThreadPoolExecutor;
    }

    @GuardedBy("this")
    public final void a() {
        while (!this.m.isEmpty()) {
            this.m.poll().a();
        }
    }

    public final synchronized void b() {
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "flush queue called");
        }
        while (!this.m.isEmpty()) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                Log.d("FirebaseInstanceId", "found intent to be delivered");
            }
            a0 a0Var = this.n;
            if (a0Var == null || !a0Var.isBinderAlive()) {
                c();
                return;
            }
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                Log.d("FirebaseInstanceId", "binder is alive, sending the intent.");
            }
            this.n.a(this.m.poll());
        }
    }

    @GuardedBy("this")
    public final void c() {
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            StringBuilder sb = new StringBuilder(39);
            sb.append("binder is dead. start connection? ");
            sb.append(!this.o);
            Log.d("FirebaseInstanceId", sb.toString());
        }
        if (!this.o) {
            this.o = true;
            try {
            } catch (SecurityException e) {
                Log.e("FirebaseInstanceId", "Exception while binding the service", e);
            }
            if (!b.i.a.f.e.n.a.b().a(this.j, this.k, this, 65)) {
                Log.e("FirebaseInstanceId", "binding to the service failed");
                this.o = false;
                a();
            }
        }
    }

    @Override // android.content.ServiceConnection
    public synchronized void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String valueOf = String.valueOf(componentName);
            StringBuilder sb = new StringBuilder(valueOf.length() + 20);
            sb.append("onServiceConnected: ");
            sb.append(valueOf);
            Log.d("FirebaseInstanceId", sb.toString());
        }
        this.o = false;
        if (!(iBinder instanceof a0)) {
            String valueOf2 = String.valueOf(iBinder);
            StringBuilder sb2 = new StringBuilder(valueOf2.length() + 28);
            sb2.append("Invalid service connection: ");
            sb2.append(valueOf2);
            Log.e("FirebaseInstanceId", sb2.toString());
            a();
            return;
        }
        this.n = (a0) iBinder;
        b();
    }

    @Override // android.content.ServiceConnection
    public void onServiceDisconnected(ComponentName componentName) {
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String valueOf = String.valueOf(componentName);
            StringBuilder sb = new StringBuilder(valueOf.length() + 23);
            sb.append("onServiceDisconnected: ");
            sb.append(valueOf);
            Log.d("FirebaseInstanceId", sb.toString());
        }
        b();
    }
}
