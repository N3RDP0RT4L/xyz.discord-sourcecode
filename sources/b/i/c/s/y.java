package b.i.c.s;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import b.i.a.f.m.a;
import java.util.concurrent.TimeUnit;
/* compiled from: com.google.firebase:firebase-iid@@21.0.0 */
/* loaded from: classes3.dex */
public final class y {
    public static final long a = TimeUnit.MINUTES.toMillis(1);

    /* renamed from: b  reason: collision with root package name */
    public static final Object f1773b = new Object();
    public static a c;

    public static ComponentName a(@NonNull Context context, @NonNull Intent intent) {
        synchronized (f1773b) {
            if (c == null) {
                a aVar = new a(context, 1, "wake:com.google.firebase.iid.WakeLockHolder");
                c = aVar;
                aVar.c.setReferenceCounted(true);
                aVar.h = true;
            }
            boolean booleanExtra = intent.getBooleanExtra("com.google.firebase.iid.WakeLockHolder.wakefulintent", false);
            intent.putExtra("com.google.firebase.iid.WakeLockHolder.wakefulintent", true);
            ComponentName startService = context.startService(intent);
            if (startService == null) {
                return null;
            }
            if (!booleanExtra) {
                c.a(a);
            }
            return startService;
        }
    }
}
