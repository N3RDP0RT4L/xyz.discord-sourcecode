package b.i.c.s;

import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import androidx.core.app.NotificationCompat;
import b.i.a.f.d.b;
import b.i.a.f.d.r;
import b.i.a.f.d.s;
import b.i.a.f.d.t;
import b.i.a.f.d.z;
import b.i.a.f.e.o.f;
import b.i.c.c;
import b.i.c.r.d;
import b.i.c.t.a;
import b.i.c.u.g;
import b.i.c.u.k;
import b.i.c.x.h;
import com.adjust.sdk.Constants;
import com.google.android.gms.tasks.Task;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
/* compiled from: com.google.firebase:firebase-iid@@21.0.0 */
/* loaded from: classes3.dex */
public class n {
    public final c a;

    /* renamed from: b  reason: collision with root package name */
    public final q f1765b;
    public final b c;
    public final a<h> d;
    public final a<d> e;
    public final g f;

    public n(c cVar, q qVar, a<h> aVar, a<d> aVar2, g gVar) {
        cVar.a();
        b bVar = new b(cVar.d);
        this.a = cVar;
        this.f1765b = qVar;
        this.c = bVar;
        this.d = aVar;
        this.e = aVar2;
        this.f = gVar;
    }

    public final Task<String> a(Task<Bundle> task) {
        Executor executor = h.a;
        return task.i(g.j, new b.i.a.f.n.a(this) { // from class: b.i.c.s.m
            public final n a;

            {
                this.a = this;
            }

            @Override // b.i.a.f.n.a
            public final Object a(Task task2) {
                Objects.requireNonNull(this.a);
                Bundle bundle = (Bundle) task2.m(IOException.class);
                if (bundle != null) {
                    String string = bundle.getString("registration_id");
                    if (string != null || (string = bundle.getString("unregistered")) != null) {
                        return string;
                    }
                    String string2 = bundle.getString("error");
                    if ("RST".equals(string2)) {
                        throw new IOException("INSTANCE_ID_RESET");
                    } else if (string2 != null) {
                        throw new IOException(string2);
                    } else {
                        String valueOf = String.valueOf(bundle);
                        Log.w("FirebaseInstanceId", b.d.b.a.a.i(valueOf.length() + 21, "Unexpected response: ", valueOf), new Throwable());
                        throw new IOException("SERVICE_NOT_AVAILABLE");
                    }
                } else {
                    throw new IOException("SERVICE_NOT_AVAILABLE");
                }
            }
        });
    }

    public final Task<Bundle> b(String str, String str2, String str3, final Bundle bundle) {
        int i;
        String str4;
        String str5;
        int i2;
        int i3;
        PackageInfo b2;
        d.a a;
        PackageInfo c;
        bundle.putString("scope", str3);
        bundle.putString(NotificationCompat.MessagingStyle.Message.KEY_SENDER, str2);
        bundle.putString("subtype", str2);
        bundle.putString("appid", str);
        c cVar = this.a;
        cVar.a();
        bundle.putString("gmp_app_id", cVar.f.f1651b);
        q qVar = this.f1765b;
        synchronized (qVar) {
            if (qVar.d == 0 && (c = qVar.c("com.google.android.gms")) != null) {
                qVar.d = c.versionCode;
            }
            i = qVar.d;
        }
        bundle.putString("gmsv", Integer.toString(i));
        bundle.putString("osv", Integer.toString(Build.VERSION.SDK_INT));
        bundle.putString("app_ver", this.f1765b.a());
        q qVar2 = this.f1765b;
        synchronized (qVar2) {
            if (qVar2.c == null) {
                qVar2.e();
            }
            str4 = qVar2.c;
        }
        bundle.putString("app_ver_name", str4);
        c cVar2 = this.a;
        cVar2.a();
        try {
            str5 = Base64.encodeToString(MessageDigest.getInstance(Constants.SHA1).digest(cVar2.e.getBytes()), 11);
        } catch (NoSuchAlgorithmException unused) {
            str5 = "[HASH-ERROR]";
        }
        bundle.putString("firebase-app-name-hash", str5);
        boolean z2 = false;
        try {
            String a2 = ((k) f.j(this.f.a(false))).a();
            if (!TextUtils.isEmpty(a2)) {
                bundle.putString("Goog-Firebase-Installations-Auth", a2);
            } else {
                Log.w("FirebaseInstanceId", "FIS auth token is empty");
            }
        } catch (InterruptedException | ExecutionException e) {
            Log.e("FirebaseInstanceId", "Failed to get FIS auth token", e);
        }
        bundle.putString("cliv", "fiid-".concat("21.0.0"));
        d dVar = this.e.get();
        h hVar = this.d.get();
        if (!(dVar == null || hVar == null || (a = dVar.a("fire-iid")) == d.a.NONE)) {
            bundle.putString("Firebase-Client-Log-Type", Integer.toString(a.f()));
            bundle.putString("Firebase-Client", hVar.getUserAgent());
        }
        final b bVar = this.c;
        r rVar = bVar.f;
        synchronized (rVar) {
            if (rVar.f1338b == 0 && (b2 = rVar.b("com.google.android.gms")) != null) {
                rVar.f1338b = b2.versionCode;
            }
            i2 = rVar.f1338b;
        }
        if (i2 >= 12000000) {
            b.i.a.f.d.f a3 = b.i.a.f.d.f.a(bVar.e);
            synchronized (a3) {
                i3 = a3.e;
                a3.e = i3 + 1;
            }
            return a3.b(new s(i3, bundle)).i(z.j, t.a);
        }
        if (bVar.f.a() != 0) {
            z2 = true;
        }
        if (!z2) {
            return f.Y(new IOException("MISSING_INSTANCEID_SERVICE"));
        }
        return bVar.b(bundle).j(z.j, new b.i.a.f.n.a(bVar, bundle) { // from class: b.i.a.f.d.v
            public final b a;

            /* renamed from: b  reason: collision with root package name */
            public final Bundle f1339b;

            {
                this.a = bVar;
                this.f1339b = bundle;
            }

            @Override // b.i.a.f.n.a
            public final Object a(Task task) {
                b bVar2 = this.a;
                Bundle bundle2 = this.f1339b;
                Objects.requireNonNull(bVar2);
                if (!task.p()) {
                    return task;
                }
                Bundle bundle3 = (Bundle) task.l();
                return !(bundle3 != null && bundle3.containsKey("google.messenger")) ? task : bVar2.b(bundle2).r(z.j, w.a);
            }
        });
    }
}
