package b.i.c.s;
/* compiled from: com.google.firebase:firebase-iid@@21.0.0 */
/* loaded from: classes3.dex */
public final class p implements o {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1766b;

    public p(String str, String str2) {
        this.a = str;
        this.f1766b = str2;
    }

    @Override // b.i.c.s.o
    public final String a() {
        return this.f1766b;
    }

    @Override // b.i.c.s.o
    public final String getId() {
        return this.a;
    }
}
