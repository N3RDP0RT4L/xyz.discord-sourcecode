package b.i.c.w;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.firebase.messaging.RemoteMessage;
/* compiled from: com.google.firebase:firebase-messaging@@21.0.0 */
/* loaded from: classes3.dex */
public class s implements Parcelable.Creator<RemoteMessage> {
    @Override // android.os.Parcelable.Creator
    public RemoteMessage createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        Bundle bundle = null;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            if (((char) readInt) != 2) {
                d.d2(parcel, readInt);
            } else {
                bundle = d.M(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        return new RemoteMessage(bundle);
    }

    @Override // android.os.Parcelable.Creator
    public RemoteMessage[] newArray(int i) {
        return new RemoteMessage[i];
    }
}
