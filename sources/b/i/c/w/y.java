package b.i.c.w;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.GuardedBy;
import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;
import androidx.collection.ArrayMap;
import b.i.a.f.e.o.f;
import b.i.c.s.n;
import b.i.c.s.o;
import b.i.c.s.q;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.iid.FirebaseInstanceId;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
/* compiled from: com.google.firebase:firebase-messaging@@21.0.0 */
/* loaded from: classes3.dex */
public class y {
    public static final long a = TimeUnit.HOURS.toSeconds(8);

    /* renamed from: b  reason: collision with root package name */
    public static final /* synthetic */ int f1796b = 0;
    public final FirebaseInstanceId c;
    public final Context d;
    public final q e;
    public final n f;
    public final ScheduledExecutorService h;
    public final w j;
    @GuardedBy("pendingOperations")
    public final Map<String, ArrayDeque<TaskCompletionSource<Void>>> g = new ArrayMap();
    @GuardedBy("this")
    public boolean i = false;

    public y(FirebaseInstanceId firebaseInstanceId, q qVar, w wVar, n nVar, Context context, @NonNull ScheduledExecutorService scheduledExecutorService) {
        this.c = firebaseInstanceId;
        this.e = qVar;
        this.j = wVar;
        this.f = nVar;
        this.d = context;
        this.h = scheduledExecutorService;
    }

    @WorkerThread
    public static <T> T a(Task<T> task) throws IOException {
        Throwable e;
        try {
            return (T) f.k(task, 30L, TimeUnit.SECONDS);
        } catch (InterruptedException e2) {
            e = e2;
            throw new IOException("SERVICE_NOT_AVAILABLE", e);
        } catch (ExecutionException e3) {
            Throwable cause = e3.getCause();
            if (cause instanceof IOException) {
                throw ((IOException) cause);
            } else if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else {
                throw new IOException(e3);
            }
        } catch (TimeoutException e4) {
            e = e4;
            throw new IOException("SERVICE_NOT_AVAILABLE", e);
        }
    }

    public static boolean d() {
        if (!Log.isLoggable("FirebaseMessaging", 3)) {
            return Build.VERSION.SDK_INT == 23 && Log.isLoggable("FirebaseMessaging", 3);
        }
        return true;
    }

    @WorkerThread
    public final void b(String str) throws IOException {
        o oVar = (o) a(this.c.f());
        n nVar = this.f;
        String id2 = oVar.getId();
        String a2 = oVar.a();
        Objects.requireNonNull(nVar);
        Bundle bundle = new Bundle();
        String valueOf = String.valueOf(str);
        bundle.putString("gcm.topic", valueOf.length() != 0 ? "/topics/".concat(valueOf) : new String("/topics/"));
        String valueOf2 = String.valueOf(str);
        a(nVar.a(nVar.b(id2, a2, valueOf2.length() != 0 ? "/topics/".concat(valueOf2) : new String("/topics/"), bundle)));
    }

    @WorkerThread
    public final void c(String str) throws IOException {
        o oVar = (o) a(this.c.f());
        n nVar = this.f;
        String id2 = oVar.getId();
        String a2 = oVar.a();
        Objects.requireNonNull(nVar);
        Bundle bundle = new Bundle();
        String valueOf = String.valueOf(str);
        bundle.putString("gcm.topic", valueOf.length() != 0 ? "/topics/".concat(valueOf) : new String("/topics/"));
        bundle.putString("delete", "1");
        String valueOf2 = String.valueOf(str);
        a(nVar.a(nVar.b(id2, a2, valueOf2.length() != 0 ? "/topics/".concat(valueOf2) : new String("/topics/"), bundle)));
    }

    public synchronized void e(boolean z2) {
        this.i = z2;
    }

    /* JADX WARN: Code restructure failed: missing block: B:6:0x000e, code lost:
        if (d() == false) goto L8;
     */
    /* JADX WARN: Code restructure failed: missing block: B:7:0x0010, code lost:
        android.util.Log.d("FirebaseMessaging", "topic sync succeeded");
     */
    /* JADX WARN: Code restructure failed: missing block: B:9:0x0018, code lost:
        return true;
     */
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public boolean f() throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 384
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.c.w.y.f():boolean");
    }

    public void g(long j) {
        this.h.schedule(new z(this, this.d, this.e, Math.min(Math.max(30L, j << 1), a)), j, TimeUnit.SECONDS);
        e(true);
    }
}
