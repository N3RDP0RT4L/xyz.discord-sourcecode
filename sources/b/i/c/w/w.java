package b.i.c.w;

import android.content.SharedPreferences;
import android.text.TextUtils;
import androidx.annotation.GuardedBy;
import androidx.annotation.Nullable;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executor;
import java.util.regex.Pattern;
/* compiled from: com.google.firebase:firebase-messaging@@21.0.0 */
/* loaded from: classes3.dex */
public final class w {
    @GuardedBy("TopicsStore.class")
    public static WeakReference<w> a;

    /* renamed from: b  reason: collision with root package name */
    public final SharedPreferences f1795b;
    public u c;
    public final Executor d;

    public w(SharedPreferences sharedPreferences, Executor executor) {
        this.d = executor;
        this.f1795b = sharedPreferences;
    }

    @Nullable
    public final synchronized v a() {
        String peek;
        v vVar;
        u uVar = this.c;
        synchronized (uVar.d) {
            peek = uVar.d.peek();
        }
        Pattern pattern = v.a;
        vVar = null;
        if (!TextUtils.isEmpty(peek)) {
            String[] split = peek.split("!", -1);
            if (split.length == 2) {
                vVar = new v(split[0], split[1]);
            }
        }
        return vVar;
    }
}
