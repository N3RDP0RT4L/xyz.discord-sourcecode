package b.i.c.w;

import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.GuardedBy;
import androidx.annotation.WorkerThread;
import java.util.ArrayDeque;
import java.util.concurrent.Executor;
/* compiled from: com.google.firebase:firebase-messaging@@21.0.0 */
/* loaded from: classes3.dex */
public final class u {
    public final SharedPreferences a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1793b;
    public final String c;
    @GuardedBy("internalQueue")
    public final ArrayDeque<String> d = new ArrayDeque<>();
    public final Executor e;

    public u(SharedPreferences sharedPreferences, String str, String str2, Executor executor) {
        this.a = sharedPreferences;
        this.f1793b = str;
        this.c = str2;
        this.e = executor;
    }

    @WorkerThread
    public static u a(SharedPreferences sharedPreferences, String str, String str2, Executor executor) {
        u uVar = new u(sharedPreferences, str, str2, executor);
        synchronized (uVar.d) {
            uVar.d.clear();
            String string = uVar.a.getString(uVar.f1793b, "");
            if (!TextUtils.isEmpty(string) && string.contains(uVar.c)) {
                String[] split = string.split(uVar.c, -1);
                if (split.length == 0) {
                    Log.e("FirebaseMessaging", "Corrupted queue. Please check the queue contents and item separator provided");
                }
                for (String str3 : split) {
                    if (!TextUtils.isEmpty(str3)) {
                        uVar.d.add(str3);
                    }
                }
            }
        }
        return uVar;
    }
}
