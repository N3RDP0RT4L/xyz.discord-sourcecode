package b.i.c.w;

import android.content.Context;
import java.util.concurrent.Executor;
/* compiled from: com.google.firebase:firebase-messaging@@21.0.0 */
/* loaded from: classes3.dex */
public class c {
    public final Executor a;

    /* renamed from: b  reason: collision with root package name */
    public final Context f1790b;
    public final r c;

    public c(Context context, r rVar, Executor executor) {
        this.a = executor;
        this.f1790b = context;
        this.c = rVar;
    }

    /* JADX WARN: Can't wrap try/catch for region: R(80:20|(79:256|22|(1:30)|31|244|32|(1:34)|38|39|(3:254|41|(74:43|(68:45|(1:47)|58|(1:60)|61|(1:63)|64|(57:66|(1:74)|88|(1:90)|91|(1:93)(2:94|(1:99)(1:98))|(1:101)|102|(1:104)(5:105|(1:107)|108|(1:110)(1:111)|(1:113)(2:114|(1:116)))|117|(1:119)(5:120|(5:123|(1:130)(1:129)|(3:264|132|267)(1:266)|265|121)|263|133|(1:135))|136|(1:138)(1:139)|(1:141)|142|(40:246|144|(1:152)|153|(1:155)|156|(34:158|(1:162)|(1:165)|166|(30:168|(1:172)|(1:175)|176|(26:178|(1:180)|(1:183)|184|(22:252|186|(1:190)|191|(3:243|193|(19:195|(1:197)|268|(1:203)|204|(4:206|258|207|(2:209|(13:211|(1:221)|222|(1:224)|225|(1:227)|228|(1:230)|(2:259|232)|237|(1:239)|240|241)(2:212|213))(2:214|215))|219|(0)|222|(0)|225|(0)|228|(0)|(0)|237|(0)|240|241)(2:198|199))|201|(0)|204|(0)|219|(0)|222|(0)|225|(0)|228|(0)|(0)|237|(0)|240|241)|188|(0)|191|(0)|201|(0)|204|(0)|219|(0)|222|(0)|225|(0)|228|(0)|(0)|237|(0)|240|241)|181|(0)|184|(0)|188|(0)|191|(0)|201|(0)|204|(0)|219|(0)|222|(0)|225|(0)|228|(0)|(0)|237|(0)|240|241)|173|(0)|176|(0)|181|(0)|184|(0)|188|(0)|191|(0)|201|(0)|204|(0)|219|(0)|222|(0)|225|(0)|228|(0)|(0)|237|(0)|240|241)|163|(0)|166|(0)|173|(0)|176|(0)|181|(0)|184|(0)|188|(0)|191|(0)|201|(0)|204|(0)|219|(0)|222|(0)|225|(0)|228|(0)|(0)|237|(0)|240|241)|146|(40:250|148|(0)|153|(0)|156|(0)|163|(0)|166|(0)|173|(0)|176|(0)|181|(0)|184|(0)|188|(0)|191|(0)|201|(0)|204|(0)|219|(0)|222|(0)|225|(0)|228|(0)|(0)|237|(0)|240|241)|150|(0)|153|(0)|156|(0)|163|(0)|166|(0)|173|(0)|176|(0)|181|(0)|184|(0)|188|(0)|191|(0)|201|(0)|204|(0)|219|(0)|222|(0)|225|(0)|228|(0)|(0)|237|(0)|240|241)|75|(59:248|79|80|(1:87)|88|(0)|91|(0)(0)|(0)|102|(0)(0)|117|(0)(0)|136|(0)(0)|(0)|142|(0)|146|(0)|150|(0)|153|(0)|156|(0)|163|(0)|166|(0)|173|(0)|176|(0)|181|(0)|184|(0)|188|(0)|191|(0)|201|(0)|204|(0)|219|(0)|222|(0)|225|(0)|228|(0)|(0)|237|(0)|240|241)|83|(1:85)|87|88|(0)|91|(0)(0)|(0)|102|(0)(0)|117|(0)(0)|136|(0)(0)|(0)|142|(0)|146|(0)|150|(0)|153|(0)|156|(0)|163|(0)|166|(0)|173|(0)|176|(0)|181|(0)|184|(0)|188|(0)|191|(0)|201|(0)|204|(0)|219|(0)|222|(0)|225|(0)|228|(0)|(0)|237|(0)|240|241)|48|(70:50|(1:52)|58|(0)|61|(0)|64|(0)|75|(1:77)|248|79|80|(0)|87|88|(0)|91|(0)(0)|(0)|102|(0)(0)|117|(0)(0)|136|(0)(0)|(0)|142|(0)|146|(0)|150|(0)|153|(0)|156|(0)|163|(0)|166|(0)|173|(0)|176|(0)|181|(0)|184|(0)|188|(0)|191|(0)|201|(0)|204|(0)|219|(0)|222|(0)|225|(0)|228|(0)|(0)|237|(0)|240|241)(1:53)|54|(1:56)|58|(0)|61|(0)|64|(0)|75|(0)|248|79|80|(0)|87|88|(0)|91|(0)(0)|(0)|102|(0)(0)|117|(0)(0)|136|(0)(0)|(0)|142|(0)|146|(0)|150|(0)|153|(0)|156|(0)|163|(0)|166|(0)|173|(0)|176|(0)|181|(0)|184|(0)|188|(0)|191|(0)|201|(0)|204|(0)|219|(0)|222|(0)|225|(0)|228|(0)|(0)|237|(0)|240|241))|57|58|(0)|61|(0)|64|(0)|75|(0)|248|79|80|(0)|87|88|(0)|91|(0)(0)|(0)|102|(0)(0)|117|(0)(0)|136|(0)(0)|(0)|142|(0)|146|(0)|150|(0)|153|(0)|156|(0)|163|(0)|166|(0)|173|(0)|176|(0)|181|(0)|184|(0)|188|(0)|191|(0)|201|(0)|204|(0)|219|(0)|222|(0)|225|(0)|228|(0)|(0)|237|(0)|240|241)|28|(0)|31|244|32|(0)|38|39|(0)|57|58|(0)|61|(0)|64|(0)|75|(0)|248|79|80|(0)|87|88|(0)|91|(0)(0)|(0)|102|(0)(0)|117|(0)(0)|136|(0)(0)|(0)|142|(0)|146|(0)|150|(0)|153|(0)|156|(0)|163|(0)|166|(0)|173|(0)|176|(0)|181|(0)|184|(0)|188|(0)|191|(0)|201|(0)|204|(0)|219|(0)|222|(0)|225|(0)|228|(0)|(0)|237|(0)|240|241) */
    /* JADX WARN: Code restructure failed: missing block: B:35:0x00b2, code lost:
        if (r6 != null) goto L39;
     */
    /* JADX WARN: Code restructure failed: missing block: B:36:0x00b5, code lost:
        r6 = move-exception;
     */
    /* JADX WARN: Code restructure failed: missing block: B:37:0x00b6, code lost:
        r6 = java.lang.String.valueOf(r6);
        b.d.b.a.a.f0(r6.length() + 35, "Couldn't get own application info: ", r6, "FirebaseMessaging");
     */
    /* JADX WARN: Code restructure failed: missing block: B:81:0x01d3, code lost:
        r12 = move-exception;
     */
    /* JADX WARN: Code restructure failed: missing block: B:82:0x01d4, code lost:
        r12 = java.lang.String.valueOf(r12);
        b.d.b.a.a.f0(r12.length() + 35, "Couldn't get own application info: ", r12, "FirebaseMessaging");
     */
    /* JADX WARN: Removed duplicated region for block: B:101:0x0249  */
    /* JADX WARN: Removed duplicated region for block: B:104:0x0258  */
    /* JADX WARN: Removed duplicated region for block: B:105:0x0266  */
    /* JADX WARN: Removed duplicated region for block: B:119:0x02a3  */
    /* JADX WARN: Removed duplicated region for block: B:120:0x02a6  */
    /* JADX WARN: Removed duplicated region for block: B:138:0x0321  */
    /* JADX WARN: Removed duplicated region for block: B:139:0x0323  */
    /* JADX WARN: Removed duplicated region for block: B:141:0x0338  */
    /* JADX WARN: Removed duplicated region for block: B:152:0x0376  */
    /* JADX WARN: Removed duplicated region for block: B:155:0x0399  */
    /* JADX WARN: Removed duplicated region for block: B:158:0x03a5  */
    /* JADX WARN: Removed duplicated region for block: B:165:0x03c6  */
    /* JADX WARN: Removed duplicated region for block: B:168:0x03d8  */
    /* JADX WARN: Removed duplicated region for block: B:175:0x03fa  */
    /* JADX WARN: Removed duplicated region for block: B:178:0x040a  */
    /* JADX WARN: Removed duplicated region for block: B:183:0x0424  */
    /* JADX WARN: Removed duplicated region for block: B:190:0x045e  */
    /* JADX WARN: Removed duplicated region for block: B:203:0x04a7  */
    /* JADX WARN: Removed duplicated region for block: B:206:0x04b7  */
    /* JADX WARN: Removed duplicated region for block: B:221:0x0519  */
    /* JADX WARN: Removed duplicated region for block: B:224:0x0531  */
    /* JADX WARN: Removed duplicated region for block: B:227:0x053b  */
    /* JADX WARN: Removed duplicated region for block: B:230:0x054d  */
    /* JADX WARN: Removed duplicated region for block: B:239:0x05c1  */
    /* JADX WARN: Removed duplicated region for block: B:243:0x0472 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:246:0x0347 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:250:0x0365 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:252:0x0437 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:254:0x00d8 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:259:0x0567 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:30:0x008b  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x00b0 A[Catch: NameNotFoundException -> 0x00b5, TRY_LEAVE, TryCatch #3 {NameNotFoundException -> 0x00b5, blocks: (B:32:0x00aa, B:34:0x00b0), top: B:244:0x00aa }] */
    /* JADX WARN: Removed duplicated region for block: B:60:0x0168  */
    /* JADX WARN: Removed duplicated region for block: B:63:0x0177  */
    /* JADX WARN: Removed duplicated region for block: B:66:0x0192  */
    /* JADX WARN: Removed duplicated region for block: B:77:0x01c5  */
    /* JADX WARN: Removed duplicated region for block: B:85:0x01e4  */
    /* JADX WARN: Removed duplicated region for block: B:90:0x01fc  */
    /* JADX WARN: Removed duplicated region for block: B:93:0x0209  */
    /* JADX WARN: Removed duplicated region for block: B:94:0x020b  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public boolean a() {
        /*
            Method dump skipped, instructions count: 1497
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.c.w.c.a():boolean");
    }
}
