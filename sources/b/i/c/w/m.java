package b.i.c.w;

import android.content.Intent;
import androidx.annotation.NonNull;
import b.i.c.p.d;
import java.io.IOException;
/* compiled from: com.google.firebase:firebase-messaging@@21.0.0 */
/* loaded from: classes3.dex */
public final class m {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final Intent f1792b;

    /* compiled from: com.google.firebase:firebase-messaging@@21.0.0 */
    /* loaded from: classes3.dex */
    public static class a implements b.i.c.p.c<m> {
        /* JADX WARN: Code restructure failed: missing block: B:28:0x00a6, code lost:
            if (com.adjust.sdk.Constants.NORMAL.equals(r1) != false) goto L29;
         */
        /* JADX WARN: Code restructure failed: missing block: B:60:0x0161, code lost:
            if (r0.isEmpty() != false) goto L61;
         */
        /* JADX WARN: Removed duplicated region for block: B:38:0x00e7  */
        /* JADX WARN: Removed duplicated region for block: B:40:0x00ef  */
        /* JADX WARN: Removed duplicated region for block: B:43:0x00fa  */
        /* JADX WARN: Removed duplicated region for block: B:46:0x0107  */
        /* JADX WARN: Removed duplicated region for block: B:49:0x0114  */
        /* JADX WARN: Removed duplicated region for block: B:52:0x0127  */
        /* JADX WARN: Removed duplicated region for block: B:55:0x0140  */
        /* JADX WARN: Removed duplicated region for block: B:63:0x0166  */
        /* JADX WARN: Removed duplicated region for block: B:69:? A[RETURN, SYNTHETIC] */
        @Override // b.i.c.p.b
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public void a(java.lang.Object r8, b.i.c.p.d r9) throws java.io.IOException {
            /*
                Method dump skipped, instructions count: 364
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: b.i.c.w.m.a.a(java.lang.Object, java.lang.Object):void");
        }
    }

    /* compiled from: com.google.firebase:firebase-messaging@@21.0.0 */
    /* loaded from: classes3.dex */
    public static final class b {
        public final m a;

        public b(@NonNull m mVar) {
            this.a = mVar;
        }
    }

    /* compiled from: com.google.firebase:firebase-messaging@@21.0.0 */
    /* loaded from: classes3.dex */
    public static final class c implements b.i.c.p.c<b> {
        @Override // b.i.c.p.b
        public final void a(Object obj, d dVar) throws IOException {
            dVar.f("messaging_client_event", ((b) obj).a);
        }
    }

    public m(@NonNull String str, @NonNull Intent intent) {
        b.c.a.a0.d.v(str, "evenType must be non-null");
        this.a = str;
        b.c.a.a0.d.z(intent, "intent must be non-null");
        this.f1792b = intent;
    }
}
