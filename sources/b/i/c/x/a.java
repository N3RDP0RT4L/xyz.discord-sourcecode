package b.i.c.x;

import java.util.Objects;
/* compiled from: AutoValue_LibraryVersion.java */
/* loaded from: classes3.dex */
public final class a extends e {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1798b;

    public a(String str, String str2) {
        Objects.requireNonNull(str, "Null libraryName");
        this.a = str;
        Objects.requireNonNull(str2, "Null version");
        this.f1798b = str2;
    }

    @Override // b.i.c.x.e
    public String a() {
        return this.a;
    }

    @Override // b.i.c.x.e
    public String b() {
        return this.f1798b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof e)) {
            return false;
        }
        e eVar = (e) obj;
        return this.a.equals(eVar.a()) && this.f1798b.equals(eVar.b());
    }

    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ this.f1798b.hashCode();
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("LibraryVersion{libraryName=");
        R.append(this.a);
        R.append(", version=");
        return b.d.b.a.a.H(R, this.f1798b, "}");
    }
}
