package b.i.c.x;

import b.i.c.l.e;
import b.i.c.l.f;
import java.util.Set;
/* compiled from: DefaultUserAgentPublisher.java */
/* loaded from: classes3.dex */
public final /* synthetic */ class b implements f {
    public static final b a = new b();

    @Override // b.i.c.l.f
    public Object a(e eVar) {
        Set d = eVar.d(e.class);
        d dVar = d.a;
        if (dVar == null) {
            synchronized (d.class) {
                dVar = d.a;
                if (dVar == null) {
                    dVar = new d();
                    d.a = dVar;
                }
            }
        }
        return new c(d, dVar);
    }
}
