package b.i.b.b;

import b.i.a.f.e.o.f;
import b.i.b.a.l;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/* compiled from: MultimapBuilder.java */
/* loaded from: classes3.dex */
public final class c0<V> implements l<List<V>>, Serializable {
    private final int expectedValuesPerKey;

    public c0(int i) {
        f.A(i, "expectedValuesPerKey");
        this.expectedValuesPerKey = i;
    }

    @Override // b.i.b.a.l
    public Object get() {
        return new ArrayList(this.expectedValuesPerKey);
    }
}
