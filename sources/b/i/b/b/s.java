package b.i.b.b;

import java.util.NoSuchElementException;
/* compiled from: Iterators.java */
/* loaded from: classes3.dex */
public final class s extends s0<T> {
    public boolean j;
    public final /* synthetic */ Object k;

    public s(Object obj) {
        this.k = obj;
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return !this.j;
    }

    /* JADX WARN: Type inference failed for: r0v3, types: [T, java.lang.Object] */
    @Override // java.util.Iterator
    public T next() {
        if (!this.j) {
            this.j = true;
            return this.k;
        }
        throw new NoSuchElementException();
    }
}
