package b.i.b.b;

import androidx.constraintlayout.solver.widgets.analyzer.BasicMeasure;
import b.i.b.a.d;
import b.i.b.b.v.i;
import b.i.b.b.v.n;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.concurrent.GuardedBy;
import com.google.j2objc.annotations.Weak;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.locks.ReentrantLock;
import org.checkerframework.checker.nullness.compatqual.MonotonicNonNullDecl;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
/* compiled from: MapMakerInternalMap.java */
/* loaded from: classes3.dex */
public class v<K, V, E extends i<K, V, E>, S extends n<K, V, E, S>> extends AbstractMap<K, V> implements ConcurrentMap<K, V>, Serializable {
    public static final b0<Object, Object, e> j = new a();
    private static final long serialVersionUID = 5;
    public final int concurrencyLevel;
    public final transient int k;
    public final b.i.b.a.d<Object> keyEquivalence;
    public final transient int l;
    public final transient n<K, V, E, S>[] m;
    public final transient j<K, V, E, S> n;
    @MonotonicNonNullDecl
    public transient Set<K> o;
    @MonotonicNonNullDecl
    public transient Collection<V> p;
    @MonotonicNonNullDecl
    public transient Set<Map.Entry<K, V>> q;

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public static class a implements b0<Object, Object, e> {
        @Override // b.i.b.b.v.b0
        public /* bridge */ /* synthetic */ e a() {
            return null;
        }

        @Override // b.i.b.b.v.b0
        public b0<Object, Object, e> b(ReferenceQueue<Object> referenceQueue, e eVar) {
            return this;
        }

        @Override // b.i.b.b.v.b0
        public void clear() {
        }

        @Override // b.i.b.b.v.b0
        public Object get() {
            return null;
        }
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public interface a0<K, V, E extends i<K, V, E>> extends i<K, V, E> {
        b0<K, V, E> b();
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public static abstract class b<K, V> extends b.i.b.b.k<K, V> implements Serializable {
        private static final long serialVersionUID = 3;
        public final int concurrencyLevel;
        public transient ConcurrentMap<K, V> j;
        public final b.i.b.a.d<Object> keyEquivalence;
        public final p keyStrength;
        public final b.i.b.a.d<Object> valueEquivalence;
        public final p valueStrength;

        public b(p pVar, p pVar2, b.i.b.a.d<Object> dVar, b.i.b.a.d<Object> dVar2, int i, ConcurrentMap<K, V> concurrentMap) {
            this.keyStrength = pVar;
            this.valueStrength = pVar2;
            this.keyEquivalence = dVar;
            this.valueEquivalence = dVar2;
            this.concurrencyLevel = i;
            this.j = concurrentMap;
        }
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public interface b0<K, V, E extends i<K, V, E>> {
        E a();

        b0<K, V, E> b(ReferenceQueue<V> referenceQueue, E e);

        void clear();

        @NullableDecl
        V get();
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public static abstract class c<K, V, E extends i<K, V, E>> implements i<K, V, E> {
        public final K a;

        /* renamed from: b  reason: collision with root package name */
        public final int f1646b;
        @NullableDecl
        public final E c;

        public c(K k, int i, @NullableDecl E e) {
            this.a = k;
            this.f1646b = i;
            this.c = e;
        }

        @Override // b.i.b.b.v.i
        public E a() {
            return this.c;
        }

        @Override // b.i.b.b.v.i
        public int c() {
            return this.f1646b;
        }

        @Override // b.i.b.b.v.i
        public K getKey() {
            return this.a;
        }
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public static final class c0<K, V, E extends i<K, V, E>> extends WeakReference<V> implements b0<K, V, E> {
        @Weak
        public final E a;

        public c0(ReferenceQueue<V> referenceQueue, V v, E e) {
            super(v, referenceQueue);
            this.a = e;
        }

        @Override // b.i.b.b.v.b0
        public E a() {
            return this.a;
        }

        @Override // b.i.b.b.v.b0
        public b0<K, V, E> b(ReferenceQueue<V> referenceQueue, E e) {
            return new c0(referenceQueue, get(), e);
        }
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public static abstract class d<K, V, E extends i<K, V, E>> extends WeakReference<K> implements i<K, V, E> {
        public final int a;
        @NullableDecl

        /* renamed from: b  reason: collision with root package name */
        public final E f1647b;

        public d(ReferenceQueue<K> referenceQueue, K k, int i, @NullableDecl E e) {
            super(k, referenceQueue);
            this.a = i;
            this.f1647b = e;
        }

        @Override // b.i.b.b.v.i
        public E a() {
            return this.f1647b;
        }

        @Override // b.i.b.b.v.i
        public int c() {
            return this.a;
        }

        @Override // b.i.b.b.v.i
        public K getKey() {
            return get();
        }
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public final class d0 extends b.i.b.b.f<K, V> {
        public final K j;
        public V k;

        public d0(K k, V v) {
            this.j = k;
            this.k = v;
        }

        @Override // b.i.b.b.f, java.util.Map.Entry
        public boolean equals(@NullableDecl Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            return this.j.equals(entry.getKey()) && this.k.equals(entry.getValue());
        }

        @Override // b.i.b.b.f, java.util.Map.Entry
        public K getKey() {
            return this.j;
        }

        @Override // b.i.b.b.f, java.util.Map.Entry
        public V getValue() {
            return this.k;
        }

        @Override // b.i.b.b.f, java.util.Map.Entry
        public int hashCode() {
            return this.j.hashCode() ^ this.k.hashCode();
        }

        @Override // java.util.Map.Entry
        public V setValue(V v) {
            V v2 = (V) v.this.put(this.j, v);
            this.k = v;
            return v2;
        }
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public static final class e implements i<Object, Object, e> {
        public e() {
            throw new AssertionError();
        }

        @Override // b.i.b.b.v.i
        public e a() {
            throw new AssertionError();
        }

        @Override // b.i.b.b.v.i
        public int c() {
            throw new AssertionError();
        }

        @Override // b.i.b.b.v.i
        public Object getKey() {
            throw new AssertionError();
        }

        @Override // b.i.b.b.v.i
        public Object getValue() {
            throw new AssertionError();
        }
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public final class f extends v<K, V, E, S>.h<Map.Entry<K, V>> {
        public f(v vVar) {
            super();
        }

        @Override // java.util.Iterator
        public Object next() {
            return c();
        }
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public final class g extends m<Map.Entry<K, V>> {
        public g() {
            super(null);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public void clear() {
            v.this.clear();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean contains(Object obj) {
            Map.Entry entry;
            Object key;
            Object obj2;
            return (obj instanceof Map.Entry) && (key = (entry = (Map.Entry) obj).getKey()) != null && (obj2 = v.this.get(key)) != null && v.this.d().c(entry.getValue(), obj2);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean isEmpty() {
            return v.this.isEmpty();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
        public Iterator<Map.Entry<K, V>> iterator() {
            return new f(v.this);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean remove(Object obj) {
            Map.Entry entry;
            Object key;
            return (obj instanceof Map.Entry) && (key = (entry = (Map.Entry) obj).getKey()) != null && v.this.remove(key, entry.getValue());
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public int size() {
            return v.this.size();
        }
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public abstract class h<T> implements Iterator<T> {
        public int j;
        public int k = -1;
        @MonotonicNonNullDecl
        public n<K, V, E, S> l;
        @MonotonicNonNullDecl
        public AtomicReferenceArray<E> m;
        @NullableDecl
        public E n;
        @NullableDecl
        public v<K, V, E, S>.d0 o;
        @NullableDecl
        public v<K, V, E, S>.d0 p;

        public h() {
            this.j = v.this.m.length - 1;
            a();
        }

        public final void a() {
            this.o = null;
            if (!d() && !e()) {
                while (true) {
                    int i = this.j;
                    if (i >= 0) {
                        n<K, V, E, S>[] nVarArr = v.this.m;
                        this.j = i - 1;
                        n<K, V, E, S> nVar = nVarArr[i];
                        this.l = nVar;
                        if (nVar.count != 0) {
                            AtomicReferenceArray<E> atomicReferenceArray = this.l.table;
                            this.m = atomicReferenceArray;
                            this.k = atomicReferenceArray.length() - 1;
                            if (e()) {
                                return;
                            }
                        }
                    } else {
                        return;
                    }
                }
            }
        }

        public boolean b(E e) {
            boolean z2;
            Object value;
            try {
                Object key = e.getKey();
                Objects.requireNonNull(v.this);
                Object obj = null;
                if (!(e.getKey() == null || (value = e.getValue()) == null)) {
                    obj = value;
                }
                if (obj != null) {
                    this.o = new d0(key, obj);
                    z2 = true;
                } else {
                    z2 = false;
                }
                return z2;
            } finally {
                this.l.h();
            }
        }

        public v<K, V, E, S>.d0 c() {
            v<K, V, E, S>.d0 d0Var = this.o;
            if (d0Var != null) {
                this.p = d0Var;
                a();
                return this.p;
            }
            throw new NoSuchElementException();
        }

        public boolean d() {
            E e = this.n;
            if (e == null) {
                return false;
            }
            while (true) {
                this.n = (E) e.a();
                E e2 = this.n;
                if (e2 == null) {
                    return false;
                }
                if (b(e2)) {
                    return true;
                }
                e = this.n;
            }
        }

        public boolean e() {
            while (true) {
                int i = this.k;
                if (i < 0) {
                    return false;
                }
                AtomicReferenceArray<E> atomicReferenceArray = this.m;
                this.k = i - 1;
                E e = atomicReferenceArray.get(i);
                this.n = e;
                if (e != null && (b(e) || d())) {
                    return true;
                }
            }
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            return this.o != null;
        }

        @Override // java.util.Iterator
        public void remove() {
            b.i.a.f.e.o.f.E(this.p != null);
            v.this.remove(this.p.j);
            this.p = null;
        }
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public interface i<K, V, E extends i<K, V, E>> {
        E a();

        int c();

        K getKey();

        V getValue();
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public interface j<K, V, E extends i<K, V, E>, S extends n<K, V, E, S>> {
        E a(S s2, E e, @NullableDecl E e2);

        p b();

        p c();

        void d(S s2, E e, V v);

        S e(v<K, V, E, S> vVar, int i, int i2);

        E f(S s2, K k, int i, @NullableDecl E e);
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public final class k extends v<K, V, E, S>.h<K> {
        public k(v vVar) {
            super();
        }

        @Override // java.util.Iterator
        public K next() {
            return c().j;
        }
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public final class l extends m<K> {
        public l() {
            super(null);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public void clear() {
            v.this.clear();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean contains(Object obj) {
            return v.this.containsKey(obj);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean isEmpty() {
            return v.this.isEmpty();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
        public Iterator<K> iterator() {
            return new k(v.this);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean remove(Object obj) {
            return v.this.remove(obj) != null;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public int size() {
            return v.this.size();
        }
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public static abstract class m<E> extends AbstractSet<E> {
        public m(a aVar) {
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public Object[] toArray() {
            return v.a(this).toArray();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public <T> T[] toArray(T[] tArr) {
            return (T[]) v.a(this).toArray(tArr);
        }
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public static abstract class n<K, V, E extends i<K, V, E>, S extends n<K, V, E, S>> extends ReentrantLock {
        public static final /* synthetic */ int j = 0;
        public volatile int count;
        @Weak
        public final v<K, V, E, S> map;
        public final int maxSegmentSize;
        public int modCount;
        public final AtomicInteger readCount = new AtomicInteger();
        @MonotonicNonNullDecl
        public volatile AtomicReferenceArray<E> table;
        public int threshold;

        public n(v<K, V, E, S> vVar, int i, int i2) {
            this.map = vVar;
            this.maxSegmentSize = i2;
            AtomicReferenceArray<E> atomicReferenceArray = new AtomicReferenceArray<>(i);
            int length = (atomicReferenceArray.length() * 3) / 4;
            this.threshold = length;
            if (length == i2) {
                this.threshold = length + 1;
            }
            this.table = atomicReferenceArray;
        }

        public <T> void a(ReferenceQueue<T> referenceQueue) {
            do {
            } while (referenceQueue.poll() != null);
        }

        /* JADX WARN: Finally extract failed */
        @GuardedBy("this")
        public void b(ReferenceQueue<K> referenceQueue) {
            int i = 0;
            do {
                Reference<? extends K> poll = referenceQueue.poll();
                if (poll != null) {
                    i iVar = (i) poll;
                    v<K, V, E, S> vVar = this.map;
                    Objects.requireNonNull(vVar);
                    int c = iVar.c();
                    n<K, V, E, S> c2 = vVar.c(c);
                    c2.lock();
                    try {
                        AtomicReferenceArray<E> atomicReferenceArray = c2.table;
                        int length = c & (atomicReferenceArray.length() - 1);
                        E e = atomicReferenceArray.get(length);
                        i iVar2 = e;
                        while (true) {
                            if (iVar2 == null) {
                                break;
                            } else if (iVar2 == iVar) {
                                c2.modCount++;
                                atomicReferenceArray.set(length, c2.j(e, iVar2));
                                c2.count--;
                                break;
                            } else {
                                iVar2 = iVar2.a();
                            }
                        }
                        c2.unlock();
                        i++;
                    } catch (Throwable th) {
                        c2.unlock();
                        throw th;
                    }
                } else {
                    return;
                }
            } while (i != 16);
        }

        /* JADX WARN: Finally extract failed */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r8v7 */
        @GuardedBy("this")
        public void c(ReferenceQueue<V> referenceQueue) {
            int i = 0;
            do {
                Reference<? extends V> poll = referenceQueue.poll();
                if (poll != null) {
                    b0<K, V, E> b0Var = (b0) poll;
                    v<K, V, E, S> vVar = this.map;
                    Objects.requireNonNull(vVar);
                    E a = b0Var.a();
                    int c = a.c();
                    n<K, V, E, S> c2 = vVar.c(c);
                    Object key = a.getKey();
                    c2.lock();
                    try {
                        AtomicReferenceArray<E> atomicReferenceArray = c2.table;
                        int length = (atomicReferenceArray.length() - 1) & c;
                        E e = atomicReferenceArray.get(length);
                        E e2 = e;
                        while (true) {
                            if (e2 == null) {
                                break;
                            }
                            Object key2 = e2.getKey();
                            if (e2.c() != c || key2 == null || !c2.map.keyEquivalence.c(key, key2)) {
                                e2 = (E) e2.a();
                            } else if (e2.b() == b0Var) {
                                c2.modCount++;
                                atomicReferenceArray.set(length, c2.j(e, e2));
                                c2.count--;
                            }
                        }
                        c2.unlock();
                        i++;
                    } catch (Throwable th) {
                        c2.unlock();
                        throw th;
                    }
                } else {
                    return;
                }
            } while (i != 16);
        }

        /* JADX WARN: Multi-variable type inference failed */
        @GuardedBy("this")
        public void d() {
            AtomicReferenceArray<E> atomicReferenceArray = this.table;
            int length = atomicReferenceArray.length();
            if (length < 1073741824) {
                int i = this.count;
                AtomicReferenceArray<E> atomicReferenceArray2 = (AtomicReferenceArray<E>) new AtomicReferenceArray(length << 1);
                this.threshold = (atomicReferenceArray2.length() * 3) / 4;
                int length2 = atomicReferenceArray2.length() - 1;
                for (int i2 = 0; i2 < length; i2++) {
                    E e = atomicReferenceArray.get(i2);
                    if (e != null) {
                        i a = e.a();
                        int c = e.c() & length2;
                        if (a == null) {
                            atomicReferenceArray2.set(c, e);
                        } else {
                            i iVar = e;
                            while (a != null) {
                                int c2 = a.c() & length2;
                                if (c2 != c) {
                                    iVar = a;
                                    c = c2;
                                }
                                a = a.a();
                            }
                            atomicReferenceArray2.set(c, iVar);
                            while (e != iVar) {
                                int c3 = e.c() & length2;
                                i a2 = this.map.n.a(l(), e, (i) atomicReferenceArray2.get(c3));
                                if (a2 != null) {
                                    atomicReferenceArray2.set(c3, a2);
                                } else {
                                    i--;
                                }
                                e = e.a();
                            }
                        }
                    }
                }
                this.table = atomicReferenceArray2;
                this.count = i;
            }
        }

        public E e(Object obj, int i) {
            if (this.count != 0) {
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                for (E e = atomicReferenceArray.get((atomicReferenceArray.length() - 1) & i); e != null; e = (E) e.a()) {
                    if (e.c() == i) {
                        Object key = e.getKey();
                        if (key == null) {
                            n();
                        } else if (this.map.keyEquivalence.c(obj, key)) {
                            return e;
                        }
                    }
                }
            }
            return null;
        }

        public void f() {
        }

        @GuardedBy("this")
        public void g() {
        }

        public void h() {
            if ((this.readCount.incrementAndGet() & 63) == 0) {
                k();
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        public V i(K k, int i, V v, boolean z2) {
            lock();
            try {
                k();
                int i2 = this.count + 1;
                if (i2 > this.threshold) {
                    d();
                    i2 = this.count + 1;
                }
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                E e = atomicReferenceArray.get(length);
                for (i iVar = e; iVar != null; iVar = iVar.a()) {
                    Object key = iVar.getKey();
                    if (iVar.c() == i && key != null && this.map.keyEquivalence.c(k, key)) {
                        V v2 = (V) iVar.getValue();
                        if (v2 == null) {
                            this.modCount++;
                            this.map.n.d(l(), iVar, v);
                            this.count = this.count;
                            return null;
                        } else if (z2) {
                            return v2;
                        } else {
                            this.modCount++;
                            this.map.n.d(l(), iVar, v);
                            return v2;
                        }
                    }
                }
                this.modCount++;
                E f = this.map.n.f(l(), k, i, e);
                m(f, v);
                atomicReferenceArray.set(length, f);
                this.count = i2;
                return null;
            } finally {
                unlock();
            }
        }

        @GuardedBy("this")
        public E j(E e, E e2) {
            int i = this.count;
            E e3 = (E) e2.a();
            while (e != e2) {
                E a = this.map.n.a(l(), e, e3);
                if (a != null) {
                    e3 = a;
                } else {
                    i--;
                }
                e = (E) e.a();
            }
            this.count = i;
            return e3;
        }

        public void k() {
            if (tryLock()) {
                try {
                    g();
                    this.readCount.set(0);
                } finally {
                    unlock();
                }
            }
        }

        public abstract S l();

        public void m(E e, V v) {
            this.map.n.d(l(), e, v);
        }

        public void n() {
            if (tryLock()) {
                try {
                    g();
                } finally {
                    unlock();
                }
            }
        }
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public static final class o<K, V> extends b<K, V> {
        private static final long serialVersionUID = 3;

        public o(p pVar, p pVar2, b.i.b.a.d<Object> dVar, b.i.b.a.d<Object> dVar2, int i, ConcurrentMap<K, V> concurrentMap) {
            super(pVar, pVar2, dVar, dVar2, i, concurrentMap);
        }

        /* JADX WARN: Multi-variable type inference failed */
        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            objectInputStream.defaultReadObject();
            int readInt = objectInputStream.readInt();
            b.i.b.b.u uVar = new b.i.b.b.u();
            int i = uVar.f1645b;
            boolean z2 = false;
            b.i.a.f.e.o.f.F(i == -1, "initial capacity was already set to %s", i);
            b.i.a.f.e.o.f.v(readInt >= 0);
            uVar.f1645b = readInt;
            uVar.d(this.keyStrength);
            p pVar = this.valueStrength;
            p pVar2 = uVar.e;
            b.i.a.f.e.o.f.G(pVar2 == null, "Value strength was already set to %s", pVar2);
            Objects.requireNonNull(pVar);
            uVar.e = pVar;
            if (pVar != p.j) {
                uVar.a = true;
            }
            b.i.b.a.d<Object> dVar = this.keyEquivalence;
            b.i.b.a.d<Object> dVar2 = uVar.f;
            b.i.a.f.e.o.f.G(dVar2 == null, "key equivalence was already set to %s", dVar2);
            Objects.requireNonNull(dVar);
            uVar.f = dVar;
            uVar.a = true;
            int i2 = this.concurrencyLevel;
            int i3 = uVar.c;
            b.i.a.f.e.o.f.F(i3 == -1, "concurrency level was already set to %s", i3);
            if (i2 > 0) {
                z2 = true;
            }
            b.i.a.f.e.o.f.v(z2);
            uVar.c = i2;
            this.j = uVar.c();
            while (true) {
                Object readObject = objectInputStream.readObject();
                if (readObject != null) {
                    this.j.put(readObject, objectInputStream.readObject());
                } else {
                    return;
                }
            }
        }

        private Object readResolve() {
            return this.j;
        }

        private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.defaultWriteObject();
            objectOutputStream.writeInt(this.j.size());
            for (Map.Entry<K, V> entry : this.j.entrySet()) {
                objectOutputStream.writeObject(entry.getKey());
                objectOutputStream.writeObject(entry.getValue());
            }
            objectOutputStream.writeObject(null);
        }
    }

    /* JADX WARN: Failed to restore enum class, 'enum' modifier removed */
    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public static abstract class p extends Enum<p> {
        public static final p j;
        public static final p k;
        public static final /* synthetic */ p[] l;

        /* JADX WARN: Failed to restore enum class, 'enum' modifier removed */
        /* compiled from: MapMakerInternalMap.java */
        /* loaded from: classes3.dex */
        public static class a extends p {
            public a(String str, int i) {
                super(str, i, null);
            }

            @Override // b.i.b.b.v.p
            public b.i.b.a.d<Object> f() {
                return d.a.j;
            }
        }

        /* JADX WARN: Failed to restore enum class, 'enum' modifier removed */
        /* compiled from: MapMakerInternalMap.java */
        /* loaded from: classes3.dex */
        public static class b extends p {
            public b(String str, int i) {
                super(str, i, null);
            }

            @Override // b.i.b.b.v.p
            public b.i.b.a.d<Object> f() {
                return d.b.j;
            }
        }

        static {
            a aVar = new a("STRONG", 0);
            j = aVar;
            b bVar = new b("WEAK", 1);
            k = bVar;
            l = new p[]{aVar, bVar};
        }

        public p(String str, int i, a aVar) {
        }

        public static p valueOf(String str) {
            return (p) Enum.valueOf(p.class, str);
        }

        public static p[] values() {
            return (p[]) l.clone();
        }

        public abstract b.i.b.a.d<Object> f();
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public static final class q<K, V> extends c<K, V, q<K, V>> implements i {
        @NullableDecl
        public volatile V d = null;

        /* compiled from: MapMakerInternalMap.java */
        /* loaded from: classes3.dex */
        public static final class a<K, V> implements j<K, V, q<K, V>, r<K, V>> {
            public static final a<?, ?> a = new a<>();

            @Override // b.i.b.b.v.j
            public i a(n nVar, i iVar, @NullableDecl i iVar2) {
                r rVar = (r) nVar;
                q qVar = (q) iVar;
                q qVar2 = new q(qVar.a, qVar.f1646b, (q) iVar2);
                qVar2.d = qVar.d;
                return qVar2;
            }

            @Override // b.i.b.b.v.j
            public p b() {
                return p.j;
            }

            @Override // b.i.b.b.v.j
            public p c() {
                return p.j;
            }

            /* JADX WARN: Multi-variable type inference failed */
            @Override // b.i.b.b.v.j
            public void d(n nVar, i iVar, Object obj) {
                r rVar = (r) nVar;
                ((q) iVar).d = obj;
            }

            @Override // b.i.b.b.v.j
            public n e(v vVar, int i, int i2) {
                return new r(vVar, i, i2);
            }

            @Override // b.i.b.b.v.j
            public i f(n nVar, Object obj, int i, @NullableDecl i iVar) {
                r rVar = (r) nVar;
                return new q(obj, i, (q) iVar);
            }
        }

        public q(K k, int i, @NullableDecl q<K, V> qVar) {
            super(k, i, qVar);
        }

        @Override // b.i.b.b.v.i
        @NullableDecl
        public V getValue() {
            return this.d;
        }
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public static final class r<K, V> extends n<K, V, q<K, V>, r<K, V>> {
        public r(v<K, V, q<K, V>, r<K, V>> vVar, int i, int i2) {
            super(vVar, i, i2);
        }

        @Override // b.i.b.b.v.n
        public n l() {
            return this;
        }
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public static final class s<K, V> extends c<K, V, s<K, V>> implements a0<K, V, s<K, V>> {
        public volatile b0<K, V, s<K, V>> d = (b0<K, V, s<K, V>>) v.j;

        /* compiled from: MapMakerInternalMap.java */
        /* loaded from: classes3.dex */
        public static final class a<K, V> implements j<K, V, s<K, V>, t<K, V>> {
            public static final a<?, ?> a = new a<>();

            @Override // b.i.b.b.v.j
            public i a(n nVar, i iVar, @NullableDecl i iVar2) {
                t tVar = (t) nVar;
                s sVar = (s) iVar;
                s sVar2 = (s) iVar2;
                int i = n.j;
                if (sVar.getValue() == null) {
                    return null;
                }
                ReferenceQueue<V> referenceQueue = tVar.queueForValues;
                s<K, V> sVar3 = new s<>(sVar.a, sVar.f1646b, sVar2);
                sVar3.d = sVar.d.b(referenceQueue, sVar3);
                return sVar3;
            }

            @Override // b.i.b.b.v.j
            public p b() {
                return p.j;
            }

            @Override // b.i.b.b.v.j
            public p c() {
                return p.k;
            }

            @Override // b.i.b.b.v.j
            public void d(n nVar, i iVar, Object obj) {
                s sVar = (s) iVar;
                ReferenceQueue referenceQueue = ((t) nVar).queueForValues;
                b0<K, V, s<K, V>> b0Var = sVar.d;
                sVar.d = new c0(referenceQueue, obj, sVar);
                b0Var.clear();
            }

            @Override // b.i.b.b.v.j
            public n e(v vVar, int i, int i2) {
                return new t(vVar, i, i2);
            }

            @Override // b.i.b.b.v.j
            public i f(n nVar, Object obj, int i, @NullableDecl i iVar) {
                t tVar = (t) nVar;
                return new s(obj, i, (s) iVar);
            }
        }

        public s(K k, int i, @NullableDecl s<K, V> sVar) {
            super(k, i, sVar);
            b0<Object, Object, e> b0Var = v.j;
        }

        @Override // b.i.b.b.v.a0
        public b0<K, V, s<K, V>> b() {
            return this.d;
        }

        @Override // b.i.b.b.v.i
        public V getValue() {
            return this.d.get();
        }
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public static final class t<K, V> extends n<K, V, s<K, V>, t<K, V>> {
        private final ReferenceQueue<V> queueForValues = new ReferenceQueue<>();

        public t(v<K, V, s<K, V>, t<K, V>> vVar, int i, int i2) {
            super(vVar, i, i2);
        }

        @Override // b.i.b.b.v.n
        public void f() {
            a((ReferenceQueue<V>) this.queueForValues);
        }

        @Override // b.i.b.b.v.n
        public void g() {
            c(this.queueForValues);
        }

        @Override // b.i.b.b.v.n
        public n l() {
            return this;
        }
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public final class u extends v<K, V, E, S>.h<V> {
        public u(v vVar) {
            super();
        }

        @Override // java.util.Iterator
        public V next() {
            return c().k;
        }
    }

    /* compiled from: MapMakerInternalMap.java */
    /* renamed from: b.i.b.b.v$v  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public final class C0133v extends AbstractCollection<V> {
        public C0133v() {
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public void clear() {
            v.this.clear();
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean contains(Object obj) {
            return v.this.containsValue(obj);
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean isEmpty() {
            return v.this.isEmpty();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
        public Iterator<V> iterator() {
            return new u(v.this);
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public int size() {
            return v.this.size();
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public Object[] toArray() {
            return v.a(this).toArray();
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public <T> T[] toArray(T[] tArr) {
            return (T[]) v.a(this).toArray(tArr);
        }
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public static final class w<K, V> extends d<K, V, w<K, V>> implements i {
        @NullableDecl
        public volatile V c = null;

        /* compiled from: MapMakerInternalMap.java */
        /* loaded from: classes3.dex */
        public static final class a<K, V> implements j<K, V, w<K, V>, x<K, V>> {
            public static final a<?, ?> a = new a<>();

            @Override // b.i.b.b.v.j
            public i a(n nVar, i iVar, @NullableDecl i iVar2) {
                x xVar = (x) nVar;
                w wVar = (w) iVar;
                w wVar2 = (w) iVar2;
                if (wVar.get() == null) {
                    return null;
                }
                w wVar3 = new w(xVar.queueForKeys, wVar.get(), wVar.a, wVar2);
                wVar3.c = wVar.c;
                return wVar3;
            }

            @Override // b.i.b.b.v.j
            public p b() {
                return p.k;
            }

            @Override // b.i.b.b.v.j
            public p c() {
                return p.j;
            }

            /* JADX WARN: Multi-variable type inference failed */
            @Override // b.i.b.b.v.j
            public void d(n nVar, i iVar, Object obj) {
                x xVar = (x) nVar;
                ((w) iVar).c = obj;
            }

            @Override // b.i.b.b.v.j
            public n e(v vVar, int i, int i2) {
                return new x(vVar, i, i2);
            }

            @Override // b.i.b.b.v.j
            public i f(n nVar, Object obj, int i, @NullableDecl i iVar) {
                return new w(((x) nVar).queueForKeys, obj, i, (w) iVar);
            }
        }

        public w(ReferenceQueue<K> referenceQueue, K k, int i, @NullableDecl w<K, V> wVar) {
            super(referenceQueue, k, i, wVar);
        }

        @Override // b.i.b.b.v.i
        @NullableDecl
        public V getValue() {
            return this.c;
        }
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public static final class x<K, V> extends n<K, V, w<K, V>, x<K, V>> {
        private final ReferenceQueue<K> queueForKeys = new ReferenceQueue<>();

        public x(v<K, V, w<K, V>, x<K, V>> vVar, int i, int i2) {
            super(vVar, i, i2);
        }

        @Override // b.i.b.b.v.n
        public void f() {
            a((ReferenceQueue<K>) this.queueForKeys);
        }

        @Override // b.i.b.b.v.n
        public void g() {
            b(this.queueForKeys);
        }

        @Override // b.i.b.b.v.n
        public n l() {
            return this;
        }
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public static final class y<K, V> extends d<K, V, y<K, V>> implements a0<K, V, y<K, V>> {
        public volatile b0<K, V, y<K, V>> c = (b0<K, V, y<K, V>>) v.j;

        /* compiled from: MapMakerInternalMap.java */
        /* loaded from: classes3.dex */
        public static final class a<K, V> implements j<K, V, y<K, V>, z<K, V>> {
            public static final a<?, ?> a = new a<>();

            @Override // b.i.b.b.v.j
            public i a(n nVar, i iVar, @NullableDecl i iVar2) {
                z zVar = (z) nVar;
                y yVar = (y) iVar;
                y yVar2 = (y) iVar2;
                if (yVar.get() == null) {
                    return null;
                }
                int i = n.j;
                if (yVar.getValue() == null) {
                    return null;
                }
                ReferenceQueue referenceQueue = zVar.queueForKeys;
                ReferenceQueue<V> referenceQueue2 = zVar.queueForValues;
                y<K, V> yVar3 = new y<>(referenceQueue, yVar.get(), yVar.a, yVar2);
                yVar3.c = yVar.c.b(referenceQueue2, yVar3);
                return yVar3;
            }

            @Override // b.i.b.b.v.j
            public p b() {
                return p.k;
            }

            @Override // b.i.b.b.v.j
            public p c() {
                return p.k;
            }

            @Override // b.i.b.b.v.j
            public void d(n nVar, i iVar, Object obj) {
                y yVar = (y) iVar;
                ReferenceQueue referenceQueue = ((z) nVar).queueForValues;
                b0<K, V, y<K, V>> b0Var = yVar.c;
                yVar.c = new c0(referenceQueue, obj, yVar);
                b0Var.clear();
            }

            @Override // b.i.b.b.v.j
            public n e(v vVar, int i, int i2) {
                return new z(vVar, i, i2);
            }

            @Override // b.i.b.b.v.j
            public i f(n nVar, Object obj, int i, @NullableDecl i iVar) {
                return new y(((z) nVar).queueForKeys, obj, i, (y) iVar);
            }
        }

        public y(ReferenceQueue<K> referenceQueue, K k, int i, @NullableDecl y<K, V> yVar) {
            super(referenceQueue, k, i, yVar);
            b0<Object, Object, e> b0Var = v.j;
        }

        @Override // b.i.b.b.v.a0
        public b0<K, V, y<K, V>> b() {
            return this.c;
        }

        @Override // b.i.b.b.v.i
        public V getValue() {
            return this.c.get();
        }
    }

    /* compiled from: MapMakerInternalMap.java */
    /* loaded from: classes3.dex */
    public static final class z<K, V> extends n<K, V, y<K, V>, z<K, V>> {
        private final ReferenceQueue<K> queueForKeys = new ReferenceQueue<>();
        private final ReferenceQueue<V> queueForValues = new ReferenceQueue<>();

        public z(v<K, V, y<K, V>, z<K, V>> vVar, int i, int i2) {
            super(vVar, i, i2);
        }

        @Override // b.i.b.b.v.n
        public void f() {
            a((ReferenceQueue<K>) this.queueForKeys);
        }

        @Override // b.i.b.b.v.n
        public void g() {
            b(this.queueForKeys);
            c(this.queueForValues);
        }

        @Override // b.i.b.b.v.n
        public n l() {
            return this;
        }
    }

    public v(b.i.b.b.u uVar, j<K, V, E, S> jVar) {
        int i2 = uVar.c;
        this.concurrencyLevel = Math.min(i2 == -1 ? 4 : i2, 65536);
        this.keyEquivalence = (b.i.b.a.d) b.i.a.f.e.o.f.W(uVar.f, uVar.a().f());
        this.n = jVar;
        int i3 = uVar.f1645b;
        int min = Math.min(i3 == -1 ? 16 : i3, (int) BasicMeasure.EXACTLY);
        int i4 = 0;
        int i5 = 1;
        int i6 = 1;
        int i7 = 0;
        while (i6 < this.concurrencyLevel) {
            i7++;
            i6 <<= 1;
        }
        this.l = 32 - i7;
        this.k = i6 - 1;
        this.m = new n[i6];
        int i8 = min / i6;
        while (i5 < (i6 * i8 < min ? i8 + 1 : i8)) {
            i5 <<= 1;
        }
        while (true) {
            n<K, V, E, S>[] nVarArr = this.m;
            if (i4 < nVarArr.length) {
                nVarArr[i4] = this.n.e(this, i5, -1);
                i4++;
            } else {
                return;
            }
        }
    }

    public static ArrayList a(Collection collection) {
        ArrayList arrayList = new ArrayList(collection.size());
        Iterator it = collection.iterator();
        Objects.requireNonNull(it);
        while (it.hasNext()) {
            arrayList.add(it.next());
        }
        return arrayList;
    }

    public int b(Object obj) {
        b.i.b.a.d<Object> dVar = this.keyEquivalence;
        Objects.requireNonNull(dVar);
        int b2 = dVar.b(obj);
        int i2 = b2 + ((b2 << 15) ^ (-12931));
        int i3 = i2 ^ (i2 >>> 10);
        int i4 = i3 + (i3 << 3);
        int i5 = i4 ^ (i4 >>> 6);
        int i6 = (i5 << 2) + (i5 << 14) + i5;
        return (i6 >>> 16) ^ i6;
    }

    public n<K, V, E, S> c(int i2) {
        return this.m[(i2 >>> this.l) & this.k];
    }

    @Override // java.util.AbstractMap, java.util.Map
    public void clear() {
        n<K, V, E, S>[] nVarArr = this.m;
        int length = nVarArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            n<K, V, E, S> nVar = nVarArr[i2];
            if (nVar.count != 0) {
                nVar.lock();
                try {
                    AtomicReferenceArray<E> atomicReferenceArray = nVar.table;
                    for (int i3 = 0; i3 < atomicReferenceArray.length(); i3++) {
                        atomicReferenceArray.set(i3, null);
                    }
                    nVar.f();
                    nVar.readCount.set(0);
                    nVar.modCount++;
                    nVar.count = 0;
                } finally {
                    nVar.unlock();
                }
            }
        }
    }

    @Override // java.util.AbstractMap, java.util.Map
    public boolean containsKey(@NullableDecl Object obj) {
        E e2;
        boolean z2 = false;
        if (obj == null) {
            return false;
        }
        int b2 = b(obj);
        n<K, V, E, S> c2 = c(b2);
        Objects.requireNonNull(c2);
        try {
            if (!(c2.count == 0 || (e2 = c2.e(obj, b2)) == null)) {
                if (e2.getValue() != null) {
                    z2 = true;
                }
            }
            return z2;
        } finally {
            c2.h();
        }
    }

    @Override // java.util.AbstractMap, java.util.Map
    public boolean containsValue(@NullableDecl Object obj) {
        if (obj == null) {
            return false;
        }
        n<K, V, E, S>[] nVarArr = this.m;
        long j2 = -1;
        int i2 = 0;
        while (i2 < 3) {
            long j3 = 0;
            for (n<K, V, E, S> nVar : nVarArr) {
                int i3 = nVar.count;
                AtomicReferenceArray<E> atomicReferenceArray = nVar.table;
                for (int i4 = 0; i4 < atomicReferenceArray.length(); i4++) {
                    for (E e2 = atomicReferenceArray.get(i4); e2 != null; e2 = e2.a()) {
                        Object obj2 = null;
                        if (e2.getKey() == null) {
                            nVar.n();
                        } else {
                            obj2 = e2.getValue();
                            if (obj2 == null) {
                                nVar.n();
                            }
                            if (obj2 == null && d().c(obj, obj2)) {
                                return true;
                            }
                        }
                        if (obj2 == null) {
                        }
                    }
                }
                j3 += nVar.modCount;
            }
            if (j3 == j2) {
                return false;
            }
            i2++;
            j2 = j3;
        }
        return false;
    }

    public b.i.b.a.d<Object> d() {
        return this.n.c().f();
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Set<Map.Entry<K, V>> entrySet() {
        Set<Map.Entry<K, V>> set = this.q;
        if (set != null) {
            return set;
        }
        g gVar = new g();
        this.q = gVar;
        return gVar;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public V get(@NullableDecl Object obj) {
        V v = null;
        if (obj == null) {
            return null;
        }
        int b2 = b(obj);
        n<K, V, E, S> c2 = c(b2);
        Objects.requireNonNull(c2);
        try {
            E e2 = c2.e(obj, b2);
            if (e2 != null && (v = (V) e2.getValue()) == null) {
                c2.n();
            }
            return v;
        } finally {
            c2.h();
        }
    }

    @Override // java.util.AbstractMap, java.util.Map
    public boolean isEmpty() {
        n<K, V, E, S>[] nVarArr = this.m;
        long j2 = 0;
        for (int i2 = 0; i2 < nVarArr.length; i2++) {
            if (nVarArr[i2].count != 0) {
                return false;
            }
            j2 += nVarArr[i2].modCount;
        }
        if (j2 == 0) {
            return true;
        }
        for (int i3 = 0; i3 < nVarArr.length; i3++) {
            if (nVarArr[i3].count != 0) {
                return false;
            }
            j2 -= nVarArr[i3].modCount;
        }
        return j2 == 0;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Set<K> keySet() {
        Set<K> set = this.o;
        if (set != null) {
            return set;
        }
        l lVar = new l();
        this.o = lVar;
        return lVar;
    }

    @Override // java.util.AbstractMap, java.util.Map
    @CanIgnoreReturnValue
    public V put(K k2, V v) {
        Objects.requireNonNull(k2);
        Objects.requireNonNull(v);
        int b2 = b(k2);
        return c(b2).i(k2, b2, v, false);
    }

    @Override // java.util.AbstractMap, java.util.Map
    public void putAll(Map<? extends K, ? extends V> map) {
        for (Map.Entry<? extends K, ? extends V> entry : map.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    @Override // java.util.Map, java.util.concurrent.ConcurrentMap
    @CanIgnoreReturnValue
    public V putIfAbsent(K k2, V v) {
        Objects.requireNonNull(k2);
        Objects.requireNonNull(v);
        int b2 = b(k2);
        return c(b2).i(k2, b2, v, true);
    }

    /* JADX WARN: Code restructure failed: missing block: B:13:0x003a, code lost:
        r11 = (V) r7.getValue();
     */
    /* JADX WARN: Code restructure failed: missing block: B:14:0x003e, code lost:
        if (r11 == null) goto L16;
     */
    /* JADX WARN: Code restructure failed: missing block: B:17:0x0045, code lost:
        if (r7.getValue() != null) goto L19;
     */
    /* JADX WARN: Code restructure failed: missing block: B:18:0x0047, code lost:
        r1 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:19:0x0049, code lost:
        r1 = false;
     */
    /* JADX WARN: Code restructure failed: missing block: B:20:0x004a, code lost:
        if (r1 == false) goto L24;
     */
    /* JADX WARN: Code restructure failed: missing block: B:21:0x004c, code lost:
        r2.modCount++;
        r3.set(r4, r2.j(r6, r7));
        r2.count--;
     */
    /* JADX WARN: Code restructure failed: missing block: B:36:?, code lost:
        return r11;
     */
    @Override // java.util.AbstractMap, java.util.Map
    @com.google.errorprone.annotations.CanIgnoreReturnValue
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public V remove(@org.checkerframework.checker.nullness.compatqual.NullableDecl java.lang.Object r11) {
        /*
            r10 = this;
            r0 = 0
            if (r11 != 0) goto L4
            return r0
        L4:
            int r1 = r10.b(r11)
            b.i.b.b.v$n r2 = r10.c(r1)
            r2.lock()
            r2.k()     // Catch: java.lang.Throwable -> L6b
            java.util.concurrent.atomic.AtomicReferenceArray<E extends b.i.b.b.v$i<K, V, E>> r3 = r2.table     // Catch: java.lang.Throwable -> L6b
            int r4 = r3.length()     // Catch: java.lang.Throwable -> L6b
            r5 = 1
            int r4 = r4 - r5
            r4 = r4 & r1
            java.lang.Object r6 = r3.get(r4)     // Catch: java.lang.Throwable -> L6b
            b.i.b.b.v$i r6 = (b.i.b.b.v.i) r6     // Catch: java.lang.Throwable -> L6b
            r7 = r6
        L22:
            if (r7 == 0) goto L67
            java.lang.Object r8 = r7.getKey()     // Catch: java.lang.Throwable -> L6b
            int r9 = r7.c()     // Catch: java.lang.Throwable -> L6b
            if (r9 != r1) goto L62
            if (r8 == 0) goto L62
            b.i.b.b.v<K, V, E extends b.i.b.b.v$i<K, V, E>, S extends b.i.b.b.v$n<K, V, E, S>> r9 = r2.map     // Catch: java.lang.Throwable -> L6b
            b.i.b.a.d<java.lang.Object> r9 = r9.keyEquivalence     // Catch: java.lang.Throwable -> L6b
            boolean r8 = r9.c(r11, r8)     // Catch: java.lang.Throwable -> L6b
            if (r8 == 0) goto L62
            java.lang.Object r11 = r7.getValue()     // Catch: java.lang.Throwable -> L6b
            if (r11 == 0) goto L41
            goto L4c
        L41:
            java.lang.Object r1 = r7.getValue()     // Catch: java.lang.Throwable -> L6b
            if (r1 != 0) goto L49
            r1 = 1
            goto L4a
        L49:
            r1 = 0
        L4a:
            if (r1 == 0) goto L67
        L4c:
            int r0 = r2.modCount     // Catch: java.lang.Throwable -> L6b
            int r0 = r0 + r5
            r2.modCount = r0     // Catch: java.lang.Throwable -> L6b
            b.i.b.b.v$i r0 = r2.j(r6, r7)     // Catch: java.lang.Throwable -> L6b
            int r1 = r2.count     // Catch: java.lang.Throwable -> L6b
            int r1 = r1 - r5
            r3.set(r4, r0)     // Catch: java.lang.Throwable -> L6b
            r2.count = r1     // Catch: java.lang.Throwable -> L6b
            r2.unlock()
            r0 = r11
            goto L6a
        L62:
            b.i.b.b.v$i r7 = r7.a()     // Catch: java.lang.Throwable -> L6b
            goto L22
        L67:
            r2.unlock()
        L6a:
            return r0
        L6b:
            r11 = move-exception
            r2.unlock()
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.b.b.v.remove(java.lang.Object):java.lang.Object");
    }

    @Override // java.util.Map, java.util.concurrent.ConcurrentMap
    @CanIgnoreReturnValue
    public V replace(K k2, V v) {
        Objects.requireNonNull(k2);
        Objects.requireNonNull(v);
        int b2 = b(k2);
        n<K, V, E, S> c2 = c(b2);
        c2.lock();
        try {
            c2.k();
            AtomicReferenceArray<E> atomicReferenceArray = c2.table;
            int length = (atomicReferenceArray.length() - 1) & b2;
            E e2 = atomicReferenceArray.get(length);
            E e3 = e2;
            while (true) {
                if (e3 == null) {
                    break;
                }
                Object key = e3.getKey();
                if (e3.c() != b2 || key == null || !c2.map.keyEquivalence.c(k2, key)) {
                    e3 = (E) e3.a();
                } else {
                    V v2 = (V) e3.getValue();
                    if (v2 == null) {
                        if (e3.getValue() == null) {
                            c2.modCount++;
                            atomicReferenceArray.set(length, c2.j(e2, e3));
                            c2.count--;
                        }
                    } else {
                        c2.modCount++;
                        c2.map.n.d(c2.l(), e3, v);
                        return v2;
                    }
                }
            }
            return null;
        } finally {
            c2.unlock();
        }
    }

    @Override // java.util.AbstractMap, java.util.Map
    public int size() {
        long j2 = 0;
        for (n<K, V, E, S> nVar : this.m) {
            j2 += nVar.count;
        }
        if (j2 > 2147483647L) {
            return Integer.MAX_VALUE;
        }
        if (j2 < -2147483648L) {
            return Integer.MIN_VALUE;
        }
        return (int) j2;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Collection<V> values() {
        Collection<V> collection = this.p;
        if (collection != null) {
            return collection;
        }
        C0133v vVar = new C0133v();
        this.p = vVar;
        return vVar;
    }

    public Object writeReplace() {
        return new o(this.n.b(), this.n.c(), this.keyEquivalence, this.n.c().f(), this.concurrencyLevel, this);
    }

    /* JADX WARN: Code restructure failed: missing block: B:14:0x004b, code lost:
        if (r2.map.d().c(r12, r7.getValue()) == false) goto L16;
     */
    /* JADX WARN: Code restructure failed: missing block: B:15:0x004d, code lost:
        r0 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:17:0x0053, code lost:
        if (r7.getValue() != null) goto L19;
     */
    /* JADX WARN: Code restructure failed: missing block: B:18:0x0055, code lost:
        r11 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:19:0x0057, code lost:
        r11 = false;
     */
    /* JADX WARN: Code restructure failed: missing block: B:20:0x0058, code lost:
        if (r11 == false) goto L23;
     */
    /* JADX WARN: Code restructure failed: missing block: B:21:0x005a, code lost:
        r2.modCount++;
        r3.set(r4, r2.j(r6, r7));
        r2.count--;
     */
    @Override // java.util.Map, java.util.concurrent.ConcurrentMap
    @com.google.errorprone.annotations.CanIgnoreReturnValue
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public boolean remove(@org.checkerframework.checker.nullness.compatqual.NullableDecl java.lang.Object r11, @org.checkerframework.checker.nullness.compatqual.NullableDecl java.lang.Object r12) {
        /*
            r10 = this;
            r0 = 0
            if (r11 == 0) goto L7a
            if (r12 != 0) goto L7
            goto L7a
        L7:
            int r1 = r10.b(r11)
            b.i.b.b.v$n r2 = r10.c(r1)
            r2.lock()
            r2.k()     // Catch: java.lang.Throwable -> L75
            java.util.concurrent.atomic.AtomicReferenceArray<E extends b.i.b.b.v$i<K, V, E>> r3 = r2.table     // Catch: java.lang.Throwable -> L75
            int r4 = r3.length()     // Catch: java.lang.Throwable -> L75
            r5 = 1
            int r4 = r4 - r5
            r4 = r4 & r1
            java.lang.Object r6 = r3.get(r4)     // Catch: java.lang.Throwable -> L75
            b.i.b.b.v$i r6 = (b.i.b.b.v.i) r6     // Catch: java.lang.Throwable -> L75
            r7 = r6
        L25:
            if (r7 == 0) goto L71
            java.lang.Object r8 = r7.getKey()     // Catch: java.lang.Throwable -> L75
            int r9 = r7.c()     // Catch: java.lang.Throwable -> L75
            if (r9 != r1) goto L6c
            if (r8 == 0) goto L6c
            b.i.b.b.v<K, V, E extends b.i.b.b.v$i<K, V, E>, S extends b.i.b.b.v$n<K, V, E, S>> r9 = r2.map     // Catch: java.lang.Throwable -> L75
            b.i.b.a.d<java.lang.Object> r9 = r9.keyEquivalence     // Catch: java.lang.Throwable -> L75
            boolean r8 = r9.c(r11, r8)     // Catch: java.lang.Throwable -> L75
            if (r8 == 0) goto L6c
            java.lang.Object r11 = r7.getValue()     // Catch: java.lang.Throwable -> L75
            b.i.b.b.v<K, V, E extends b.i.b.b.v$i<K, V, E>, S extends b.i.b.b.v$n<K, V, E, S>> r1 = r2.map     // Catch: java.lang.Throwable -> L75
            b.i.b.a.d r1 = r1.d()     // Catch: java.lang.Throwable -> L75
            boolean r11 = r1.c(r12, r11)     // Catch: java.lang.Throwable -> L75
            if (r11 == 0) goto L4f
            r0 = 1
            goto L5a
        L4f:
            java.lang.Object r11 = r7.getValue()     // Catch: java.lang.Throwable -> L75
            if (r11 != 0) goto L57
            r11 = 1
            goto L58
        L57:
            r11 = 0
        L58:
            if (r11 == 0) goto L71
        L5a:
            int r11 = r2.modCount     // Catch: java.lang.Throwable -> L75
            int r11 = r11 + r5
            r2.modCount = r11     // Catch: java.lang.Throwable -> L75
            b.i.b.b.v$i r11 = r2.j(r6, r7)     // Catch: java.lang.Throwable -> L75
            int r12 = r2.count     // Catch: java.lang.Throwable -> L75
            int r12 = r12 - r5
            r3.set(r4, r11)     // Catch: java.lang.Throwable -> L75
            r2.count = r12     // Catch: java.lang.Throwable -> L75
            goto L71
        L6c:
            b.i.b.b.v$i r7 = r7.a()     // Catch: java.lang.Throwable -> L75
            goto L25
        L71:
            r2.unlock()
            return r0
        L75:
            r11 = move-exception
            r2.unlock()
            throw r11
        L7a:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.b.b.v.remove(java.lang.Object, java.lang.Object):boolean");
    }

    @Override // java.util.Map, java.util.concurrent.ConcurrentMap
    @CanIgnoreReturnValue
    public boolean replace(K k2, @NullableDecl V v, V v2) {
        Objects.requireNonNull(k2);
        Objects.requireNonNull(v2);
        if (v == null) {
            return false;
        }
        int b2 = b(k2);
        n<K, V, E, S> c2 = c(b2);
        c2.lock();
        try {
            c2.k();
            AtomicReferenceArray<E> atomicReferenceArray = c2.table;
            int length = (atomicReferenceArray.length() - 1) & b2;
            E e2 = atomicReferenceArray.get(length);
            E e3 = e2;
            while (true) {
                if (e3 == null) {
                    break;
                }
                Object key = e3.getKey();
                if (e3.c() != b2 || key == null || !c2.map.keyEquivalence.c(k2, key)) {
                    e3 = (E) e3.a();
                } else {
                    Object value = e3.getValue();
                    if (value == null) {
                        if (e3.getValue() == null) {
                            c2.modCount++;
                            atomicReferenceArray.set(length, c2.j(e2, e3));
                            c2.count--;
                        }
                    } else if (c2.map.d().c(v, value)) {
                        c2.modCount++;
                        c2.map.n.d(c2.l(), e3, v2);
                        c2.unlock();
                        return true;
                    }
                }
            }
            return false;
        } finally {
            c2.unlock();
        }
    }
}
