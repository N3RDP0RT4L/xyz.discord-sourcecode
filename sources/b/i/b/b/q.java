package b.i.b.b;

import b.i.a.f.e.o.f;
import b.i.b.b.n;
import com.discord.api.permission.Permission;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.concurrent.LazyInit;
import com.google.j2objc.annotations.RetainedWith;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
/* compiled from: ImmutableMap.java */
/* loaded from: classes3.dex */
public abstract class q<K, V> implements Map<K, V>, Serializable {
    @LazyInit
    public transient r<Map.Entry<K, V>> j;
    @RetainedWith
    @LazyInit
    public transient r<K> k;
    @RetainedWith
    @LazyInit
    public transient n<V> l;

    /* compiled from: ImmutableMap.java */
    /* loaded from: classes3.dex */
    public static class a<K, V> {
        public Object[] a;

        /* renamed from: b  reason: collision with root package name */
        public int f1644b = 0;
        public boolean c = false;

        public a(int i) {
            this.a = new Object[i * 2];
        }

        /* JADX WARN: Code restructure failed: missing block: B:16:0x0058, code lost:
            r5[r10] = r8;
            r4 = r4 + 1;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public b.i.b.b.q<K, V> a() {
            /*
                r13 = this;
                r0 = 1
                r13.c = r0
                int r1 = r13.f1644b
                java.lang.Object[] r2 = r13.a
                if (r1 != 0) goto Lf
                b.i.b.b.q<java.lang.Object, java.lang.Object> r0 = b.i.b.b.i0.m
                b.i.b.b.i0 r0 = (b.i.b.b.i0) r0
                goto La0
            Lf:
                r3 = 0
                r4 = 0
                if (r1 != r0) goto L22
                r1 = r2[r4]
                r4 = r2[r0]
                b.i.a.f.e.o.f.z(r1, r4)
                b.i.b.b.i0 r1 = new b.i.b.b.i0
                r1.<init>(r3, r2, r0)
                r0 = r1
                goto La0
            L22:
                int r5 = r2.length
                int r5 = r5 >> r0
                b.i.a.f.e.o.f.C(r1, r5)
                int r5 = b.i.b.b.r.k(r1)
                if (r1 != r0) goto L35
                r4 = r2[r4]
                r0 = r2[r0]
                b.i.a.f.e.o.f.z(r4, r0)
                goto L9b
            L35:
                int r3 = r5 + (-1)
                int[] r5 = new int[r5]
                r6 = -1
                java.util.Arrays.fill(r5, r6)
            L3d:
                if (r4 >= r1) goto L9a
                int r7 = r4 * 2
                int r8 = r7 + 0
                r9 = r2[r8]
                int r7 = r7 + r0
                r7 = r2[r7]
                b.i.a.f.e.o.f.z(r9, r7)
                int r10 = r9.hashCode()
                int r10 = b.i.a.f.e.o.f.c1(r10)
            L53:
                r10 = r10 & r3
                r11 = r5[r10]
                if (r11 != r6) goto L5d
                r5[r10] = r8
                int r4 = r4 + 1
                goto L3d
            L5d:
                r12 = r2[r11]
                boolean r12 = r12.equals(r9)
                if (r12 != 0) goto L68
                int r10 = r10 + 1
                goto L53
            L68:
                java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "Multiple entries with same key: "
                r3.append(r4)
                r3.append(r9)
                java.lang.String r4 = "="
                r3.append(r4)
                r3.append(r7)
                java.lang.String r5 = " and "
                r3.append(r5)
                r5 = r2[r11]
                r3.append(r5)
                r3.append(r4)
                r0 = r0 ^ r11
                r0 = r2[r0]
                r3.append(r0)
                java.lang.String r0 = r3.toString()
                r1.<init>(r0)
                throw r1
            L9a:
                r3 = r5
            L9b:
                b.i.b.b.i0 r0 = new b.i.b.b.i0
                r0.<init>(r3, r2, r1)
            La0:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: b.i.b.b.q.a.a():b.i.b.b.q");
        }

        public final void b(int i) {
            int i2 = i * 2;
            Object[] objArr = this.a;
            if (i2 > objArr.length) {
                this.a = Arrays.copyOf(objArr, n.b.a(objArr.length, i2));
                this.c = false;
            }
        }

        @CanIgnoreReturnValue
        public a<K, V> c(K k, V v) {
            b(this.f1644b + 1);
            f.z(k, v);
            Object[] objArr = this.a;
            int i = this.f1644b;
            objArr[i * 2] = k;
            objArr[(i * 2) + 1] = v;
            this.f1644b = i + 1;
            return this;
        }
    }

    /* compiled from: ImmutableMap.java */
    /* loaded from: classes3.dex */
    public static class b implements Serializable {
        private static final long serialVersionUID = 0;
        private final Object[] keys;
        private final Object[] values;

        public b(q<?, ?> qVar) {
            this.keys = new Object[qVar.size()];
            this.values = new Object[qVar.size()];
            s0<Map.Entry<?, ?>> j = qVar.entrySet().iterator();
            int i = 0;
            while (j.hasNext()) {
                Map.Entry<?, ?> next = j.next();
                this.keys[i] = next.getKey();
                this.values[i] = next.getValue();
                i++;
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        public Object readResolve() {
            a aVar = new a(this.keys.length);
            int i = 0;
            while (true) {
                Object[] objArr = this.keys;
                if (i >= objArr.length) {
                    return aVar.a();
                }
                aVar.c(objArr[i], this.values[i]);
                i++;
            }
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static <K, V> q<K, V> a(Map<? extends K, ? extends V> map) {
        if ((map instanceof q) && !(map instanceof SortedMap)) {
            q<K, V> qVar = (q) map;
            if (!qVar.f()) {
                return qVar;
            }
        }
        Set<Map.Entry<? extends K, ? extends V>> entrySet = map.entrySet();
        boolean z2 = entrySet instanceof Collection;
        a aVar = new a(z2 ? entrySet.size() : 4);
        if (z2) {
            aVar.b(entrySet.size() + aVar.f1644b);
        }
        Iterator<T> it = entrySet.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            aVar.c(entry.getKey(), entry.getValue());
        }
        return aVar.a();
    }

    public abstract r<Map.Entry<K, V>> b();

    public abstract r<K> c();

    @Override // java.util.Map
    @Deprecated
    public final void clear() {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.Map
    public boolean containsKey(@NullableDecl Object obj) {
        return get(obj) != null;
    }

    @Override // java.util.Map
    public boolean containsValue(@NullableDecl Object obj) {
        n<V> nVar = this.l;
        if (nVar == null) {
            nVar = d();
            this.l = nVar;
        }
        return nVar.contains(obj);
    }

    public abstract n<V> d();

    /* renamed from: e */
    public r<Map.Entry<K, V>> entrySet() {
        r<Map.Entry<K, V>> rVar = this.j;
        if (rVar != null) {
            return rVar;
        }
        r<Map.Entry<K, V>> b2 = b();
        this.j = b2;
        return b2;
    }

    @Override // java.util.Map
    public boolean equals(@NullableDecl Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Map) {
            return entrySet().equals(((Map) obj).entrySet());
        }
        return false;
    }

    public abstract boolean f();

    @Override // java.util.Map
    public abstract V get(@NullableDecl Object obj);

    @Override // java.util.Map
    public final V getOrDefault(@NullableDecl Object obj, @NullableDecl V v) {
        V v2 = get(obj);
        return v2 != null ? v2 : v;
    }

    @Override // java.util.Map
    public int hashCode() {
        return h.a(entrySet());
    }

    @Override // java.util.Map
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override // java.util.Map
    public Set keySet() {
        r<K> rVar = this.k;
        if (rVar != null) {
            return rVar;
        }
        r<K> c = c();
        this.k = c;
        return c;
    }

    @Override // java.util.Map
    @CanIgnoreReturnValue
    @Deprecated
    public final V put(K k, V v) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.Map
    @Deprecated
    public final void putAll(Map<? extends K, ? extends V> map) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.Map
    @CanIgnoreReturnValue
    @Deprecated
    public final V remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    public String toString() {
        int size = size();
        f.A(size, "size");
        StringBuilder sb = new StringBuilder((int) Math.min(size * 8, (long) Permission.MANAGE_EMOJIS_AND_STICKERS));
        sb.append('{');
        boolean z2 = true;
        for (Map.Entry entry : entrySet()) {
            if (!z2) {
                sb.append(", ");
            }
            z2 = false;
            sb.append(entry.getKey());
            sb.append('=');
            sb.append(entry.getValue());
        }
        sb.append('}');
        return sb.toString();
    }

    @Override // java.util.Map
    public Collection values() {
        n<V> nVar = this.l;
        if (nVar != null) {
            return nVar;
        }
        n<V> d = d();
        this.l = d;
        return d;
    }

    public Object writeReplace() {
        return new b(this);
    }
}
