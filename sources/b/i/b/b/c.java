package b.i.b.b;

import b.i.b.b.e;
import java.util.Collection;
import java.util.Map;
import java.util.NavigableMap;
import java.util.SortedMap;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
/* compiled from: AbstractListMultimap.java */
/* loaded from: classes3.dex */
public abstract class c<K, V> extends e<K, V> implements b0 {
    private static final long serialVersionUID = 6588350623831699109L;

    public c(Map<K, Collection<V>> map) {
        super(map);
    }

    @Override // b.i.b.b.g, b.i.b.b.b0
    public Map<K, Collection<V>> a() {
        Map<K, Collection<V>> aVar;
        Map<K, Collection<V>> map = this.l;
        if (map == null) {
            d0 d0Var = (d0) this;
            Map<K, Collection<V>> map2 = d0Var.m;
            if (map2 instanceof NavigableMap) {
                aVar = new e.d((NavigableMap) d0Var.m);
            } else if (map2 instanceof SortedMap) {
                aVar = new e.g((SortedMap) d0Var.m);
            } else {
                aVar = new e.a(d0Var.m);
            }
            map = aVar;
            this.l = map;
        }
        return map;
    }

    @Override // b.i.b.b.g
    public boolean equals(@NullableDecl Object obj) {
        return super.equals(obj);
    }
}
