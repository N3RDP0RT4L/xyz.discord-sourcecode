package b.i.b.b;

import b.i.a.f.e.o.f;
import java.util.Iterator;
import java.util.NoSuchElementException;
/* compiled from: Iterators.java */
/* loaded from: classes3.dex */
public enum t implements Iterator<Object> {
    INSTANCE;

    @Override // java.util.Iterator
    public boolean hasNext() {
        return false;
    }

    @Override // java.util.Iterator
    public Object next() {
        throw new NoSuchElementException();
    }

    @Override // java.util.Iterator
    public void remove() {
        f.E(false);
    }
}
