package b.i.b.b;

import b.i.a.f.e.o.f;
import b.i.b.b.p;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.AbstractCollection;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
/* compiled from: ImmutableCollection.java */
/* loaded from: classes3.dex */
public abstract class n<E> extends AbstractCollection<E> implements Serializable {
    public static final Object[] j = new Object[0];

    /* compiled from: ImmutableCollection.java */
    /* loaded from: classes3.dex */
    public static abstract class a<E> extends b<E> {
        public Object[] a;

        /* renamed from: b  reason: collision with root package name */
        public int f1643b = 0;
        public boolean c;

        public a(int i) {
            f.A(i, "initialCapacity");
            this.a = new Object[i];
        }
    }

    /* compiled from: ImmutableCollection.java */
    /* loaded from: classes3.dex */
    public static abstract class b<E> {
        public static int a(int i, int i2) {
            if (i2 >= 0) {
                int i3 = i + (i >> 1) + 1;
                if (i3 < i2) {
                    i3 = Integer.highestOneBit(i2 - 1) << 1;
                }
                if (i3 < 0) {
                    return Integer.MAX_VALUE;
                }
                return i3;
            }
            throw new AssertionError("cannot store more than MAX_VALUE elements");
        }
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    @Deprecated
    public final boolean add(E e) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    @Deprecated
    public final boolean addAll(Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    public p<E> c() {
        if (!isEmpty()) {
            return p.k(toArray());
        }
        b.i.b.b.a<Object> aVar = p.k;
        return (p<E>) h0.l;
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    @Deprecated
    public final void clear() {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public abstract boolean contains(@NullableDecl Object obj);

    @CanIgnoreReturnValue
    public int d(Object[] objArr, int i) {
        s0<E> j2 = iterator();
        while (j2.hasNext()) {
            i++;
            objArr[i] = j2.next();
        }
        return i;
    }

    public Object[] e() {
        return null;
    }

    public int g() {
        throw new UnsupportedOperationException();
    }

    public int h() {
        throw new UnsupportedOperationException();
    }

    public abstract boolean i();

    /* renamed from: j */
    public abstract s0<E> iterator();

    @Override // java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    @Deprecated
    public final boolean remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    @Deprecated
    public final boolean removeAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    @Deprecated
    public final boolean retainAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public final Object[] toArray() {
        return toArray(j);
    }

    public Object writeReplace() {
        return new p.c(toArray());
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    public final <T> T[] toArray(T[] tArr) {
        Objects.requireNonNull(tArr);
        int size = size();
        if (tArr.length < size) {
            Object[] e = e();
            if (e != null) {
                return (T[]) Arrays.copyOfRange(e, h(), g(), tArr.getClass());
            }
            tArr = (T[]) ((Object[]) Array.newInstance(tArr.getClass().getComponentType(), size));
        } else if (tArr.length > size) {
            tArr[size] = null;
        }
        d(tArr, 0);
        return tArr;
    }
}
