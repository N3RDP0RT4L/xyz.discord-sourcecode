package b.i.b.b;

import java.io.Serializable;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.Objects;
import java.util.RandomAccess;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import org.checkerframework.checker.nullness.compatqual.MonotonicNonNullDecl;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
/* compiled from: AbstractMapBasedMultimap.java */
/* loaded from: classes3.dex */
public abstract class e<K, V> extends b.i.b.b.g<K, V> implements Serializable {
    private static final long serialVersionUID = 2447537837011683357L;
    public transient Map<K, Collection<V>> m;
    public transient int n;

    /* compiled from: AbstractMapBasedMultimap.java */
    /* loaded from: classes3.dex */
    public class a extends a0<K, Collection<V>> {
        public final transient Map<K, Collection<V>> l;

        /* compiled from: AbstractMapBasedMultimap.java */
        /* renamed from: b.i.b.b.e$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public class C0130a extends x<K, Collection<V>> {
            public C0130a() {
            }

            @Override // b.i.b.b.x, java.util.AbstractCollection, java.util.Collection, java.util.Set
            public boolean contains(Object obj) {
                Set<Map.Entry<K, Collection<V>>> entrySet = a.this.l.entrySet();
                Objects.requireNonNull(entrySet);
                try {
                    return entrySet.contains(obj);
                } catch (ClassCastException | NullPointerException unused) {
                    return false;
                }
            }

            @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
            public Iterator<Map.Entry<K, Collection<V>>> iterator() {
                return new b();
            }

            @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
            public boolean remove(Object obj) {
                Collection<V> collection;
                if (!contains(obj)) {
                    return false;
                }
                e eVar = e.this;
                Object key = ((Map.Entry) obj).getKey();
                Map<K, Collection<V>> map = eVar.m;
                Objects.requireNonNull(map);
                try {
                    collection = map.remove(key);
                } catch (ClassCastException | NullPointerException unused) {
                    collection = null;
                }
                Collection<V> collection2 = collection;
                if (collection2 == null) {
                    return true;
                }
                int size = collection2.size();
                collection2.clear();
                eVar.n -= size;
                return true;
            }
        }

        /* compiled from: AbstractMapBasedMultimap.java */
        /* loaded from: classes3.dex */
        public class b implements Iterator<Map.Entry<K, Collection<V>>> {
            public final Iterator<Map.Entry<K, Collection<V>>> j;
            @NullableDecl
            public Collection<V> k;

            public b() {
                this.j = a.this.l.entrySet().iterator();
            }

            @Override // java.util.Iterator
            public boolean hasNext() {
                return this.j.hasNext();
            }

            @Override // java.util.Iterator
            public Object next() {
                Map.Entry<K, Collection<V>> next = this.j.next();
                this.k = next.getValue();
                return a.this.a(next);
            }

            @Override // java.util.Iterator
            public void remove() {
                b.i.a.f.e.o.f.E(this.k != null);
                this.j.remove();
                e.this.n -= this.k.size();
                this.k.clear();
                this.k = null;
            }
        }

        public a(Map<K, Collection<V>> map) {
            this.l = map;
        }

        public Map.Entry<K, Collection<V>> a(Map.Entry<K, Collection<V>> entry) {
            K key = entry.getKey();
            b.i.b.b.c cVar = (b.i.b.b.c) e.this;
            Objects.requireNonNull(cVar);
            List list = (List) entry.getValue();
            return new o(key, list instanceof RandomAccess ? new f(cVar, key, list, null) : new j(key, list, null));
        }

        @Override // java.util.AbstractMap, java.util.Map
        public void clear() {
            Map<K, Collection<V>> map = this.l;
            e eVar = e.this;
            if (map == eVar.m) {
                eVar.d();
                return;
            }
            b bVar = new b();
            while (bVar.hasNext()) {
                bVar.next();
                bVar.remove();
            }
        }

        @Override // java.util.AbstractMap, java.util.Map
        public boolean containsKey(Object obj) {
            Map<K, Collection<V>> map = this.l;
            Objects.requireNonNull(map);
            try {
                return map.containsKey(obj);
            } catch (ClassCastException | NullPointerException unused) {
                return false;
            }
        }

        @Override // java.util.AbstractMap, java.util.Map
        public boolean equals(@NullableDecl Object obj) {
            return this == obj || this.l.equals(obj);
        }

        @Override // java.util.AbstractMap, java.util.Map
        public Object get(Object obj) {
            Collection<V> collection;
            Map<K, Collection<V>> map = this.l;
            Objects.requireNonNull(map);
            try {
                collection = map.get(obj);
            } catch (ClassCastException | NullPointerException unused) {
                collection = null;
            }
            Collection<V> collection2 = collection;
            if (collection2 == null) {
                return null;
            }
            b.i.b.b.c cVar = (b.i.b.b.c) e.this;
            Objects.requireNonNull(cVar);
            List list = (List) collection2;
            return list instanceof RandomAccess ? new f(cVar, obj, list, null) : new j(obj, list, null);
        }

        @Override // java.util.AbstractMap, java.util.Map
        public int hashCode() {
            return this.l.hashCode();
        }

        @Override // java.util.AbstractMap, java.util.Map
        public Set<K> keySet() {
            Set<K> cVar;
            e eVar = e.this;
            Set<K> set = eVar.j;
            if (set == null) {
                d0 d0Var = (d0) eVar;
                Map<K, Collection<V>> map = d0Var.m;
                if (map instanceof NavigableMap) {
                    cVar = new C0131e((NavigableMap) d0Var.m);
                } else if (map instanceof SortedMap) {
                    cVar = new h((SortedMap) d0Var.m);
                } else {
                    cVar = new c(d0Var.m);
                }
                set = cVar;
                eVar.j = set;
            }
            return set;
        }

        @Override // java.util.AbstractMap, java.util.Map
        public Object remove(Object obj) {
            Collection<V> remove = this.l.remove(obj);
            if (remove == null) {
                return null;
            }
            Collection<V> e = e.this.e();
            e.addAll(remove);
            e.this.n -= remove.size();
            remove.clear();
            return e;
        }

        @Override // java.util.AbstractMap, java.util.Map
        public int size() {
            return this.l.size();
        }

        @Override // java.util.AbstractMap
        public String toString() {
            return this.l.toString();
        }
    }

    /* compiled from: AbstractMapBasedMultimap.java */
    /* loaded from: classes3.dex */
    public abstract class b<T> implements Iterator<T> {
        public final Iterator<Map.Entry<K, Collection<V>>> j;
        @NullableDecl
        public K k = null;
        @MonotonicNonNullDecl
        public Collection<V> l = null;
        public Iterator<V> m = t.INSTANCE;

        public b() {
            this.j = e.this.m.entrySet().iterator();
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            return this.j.hasNext() || this.m.hasNext();
        }

        /* JADX WARN: Type inference failed for: r0v3, types: [T, java.lang.Object] */
        @Override // java.util.Iterator
        public T next() {
            if (!this.m.hasNext()) {
                Map.Entry<K, Collection<V>> next = this.j.next();
                this.k = next.getKey();
                Collection<V> value = next.getValue();
                this.l = value;
                this.m = value.iterator();
            }
            return this.m.next();
        }

        @Override // java.util.Iterator
        public void remove() {
            this.m.remove();
            if (this.l.isEmpty()) {
                this.j.remove();
            }
            e.c(e.this);
        }
    }

    /* compiled from: AbstractMapBasedMultimap.java */
    /* loaded from: classes3.dex */
    public class c extends y<K, Collection<V>> {

        /* compiled from: AbstractMapBasedMultimap.java */
        /* loaded from: classes3.dex */
        public class a implements Iterator<K> {
            @NullableDecl
            public Map.Entry<K, Collection<V>> j;
            public final /* synthetic */ Iterator k;

            public a(Iterator it) {
                this.k = it;
            }

            @Override // java.util.Iterator
            public boolean hasNext() {
                return this.k.hasNext();
            }

            @Override // java.util.Iterator
            public K next() {
                Map.Entry<K, Collection<V>> entry = (Map.Entry) this.k.next();
                this.j = entry;
                return entry.getKey();
            }

            @Override // java.util.Iterator
            public void remove() {
                b.i.a.f.e.o.f.E(this.j != null);
                Collection<V> value = this.j.getValue();
                this.k.remove();
                e.this.n -= value.size();
                value.clear();
                this.j = null;
            }
        }

        public c(Map<K, Collection<V>> map) {
            super(map);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public void clear() {
            Iterator<K> it = iterator();
            while (true) {
                a aVar = (a) it;
                if (aVar.hasNext()) {
                    aVar.next();
                    aVar.remove();
                } else {
                    return;
                }
            }
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean containsAll(Collection<?> collection) {
            return this.j.keySet().containsAll(collection);
        }

        @Override // java.util.AbstractSet, java.util.Collection, java.util.Set
        public boolean equals(@NullableDecl Object obj) {
            return this == obj || this.j.keySet().equals(obj);
        }

        @Override // java.util.AbstractSet, java.util.Collection, java.util.Set
        public int hashCode() {
            return this.j.keySet().hashCode();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
        public Iterator<K> iterator() {
            return new a(this.j.entrySet().iterator());
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean remove(Object obj) {
            int i;
            Collection collection = (Collection) this.j.remove(obj);
            if (collection != null) {
                i = collection.size();
                collection.clear();
                e.this.n -= i;
            } else {
                i = 0;
            }
            return i > 0;
        }
    }

    /* compiled from: AbstractMapBasedMultimap.java */
    /* loaded from: classes3.dex */
    public class d extends e<K, V>.g implements NavigableMap<K, Collection<V>> {
        public d(NavigableMap<K, Collection<V>> navigableMap) {
            super(navigableMap);
        }

        @Override // b.i.b.b.e.g
        public SortedSet b() {
            return new C0131e(d());
        }

        @Override // java.util.NavigableMap
        public Map.Entry<K, Collection<V>> ceilingEntry(K k) {
            Map.Entry<K, Collection<V>> ceilingEntry = d().ceilingEntry(k);
            if (ceilingEntry == null) {
                return null;
            }
            return a(ceilingEntry);
        }

        @Override // java.util.NavigableMap
        public K ceilingKey(K k) {
            return d().ceilingKey(k);
        }

        @Override // java.util.NavigableMap
        public NavigableSet<K> descendingKeySet() {
            return ((d) descendingMap()).navigableKeySet();
        }

        @Override // java.util.NavigableMap
        public NavigableMap<K, Collection<V>> descendingMap() {
            return new d(d().descendingMap());
        }

        /* renamed from: e */
        public NavigableSet<K> keySet() {
            SortedSet<K> sortedSet = this.n;
            if (sortedSet == null) {
                sortedSet = b();
                this.n = sortedSet;
            }
            return (NavigableSet) sortedSet;
        }

        public Map.Entry<K, Collection<V>> f(Iterator<Map.Entry<K, Collection<V>>> it) {
            if (!it.hasNext()) {
                return null;
            }
            Map.Entry<K, Collection<V>> next = it.next();
            Collection<V> e = e.this.e();
            e.addAll(next.getValue());
            it.remove();
            K key = next.getKey();
            Objects.requireNonNull((b.i.b.b.c) e.this);
            return new o(key, Collections.unmodifiableList((List) e));
        }

        @Override // java.util.NavigableMap
        public Map.Entry<K, Collection<V>> firstEntry() {
            Map.Entry<K, Collection<V>> firstEntry = d().firstEntry();
            if (firstEntry == null) {
                return null;
            }
            return a(firstEntry);
        }

        @Override // java.util.NavigableMap
        public Map.Entry<K, Collection<V>> floorEntry(K k) {
            Map.Entry<K, Collection<V>> floorEntry = d().floorEntry(k);
            if (floorEntry == null) {
                return null;
            }
            return a(floorEntry);
        }

        @Override // java.util.NavigableMap
        public K floorKey(K k) {
            return d().floorKey(k);
        }

        /* renamed from: g */
        public NavigableMap<K, Collection<V>> d() {
            return (NavigableMap) ((SortedMap) this.l);
        }

        @Override // b.i.b.b.e.g, java.util.SortedMap, java.util.NavigableMap
        public SortedMap headMap(Object obj) {
            return headMap(obj, false);
        }

        @Override // java.util.NavigableMap
        public Map.Entry<K, Collection<V>> higherEntry(K k) {
            Map.Entry<K, Collection<V>> higherEntry = d().higherEntry(k);
            if (higherEntry == null) {
                return null;
            }
            return a(higherEntry);
        }

        @Override // java.util.NavigableMap
        public K higherKey(K k) {
            return d().higherKey(k);
        }

        @Override // java.util.NavigableMap
        public Map.Entry<K, Collection<V>> lastEntry() {
            Map.Entry<K, Collection<V>> lastEntry = d().lastEntry();
            if (lastEntry == null) {
                return null;
            }
            return a(lastEntry);
        }

        @Override // java.util.NavigableMap
        public Map.Entry<K, Collection<V>> lowerEntry(K k) {
            Map.Entry<K, Collection<V>> lowerEntry = d().lowerEntry(k);
            if (lowerEntry == null) {
                return null;
            }
            return a(lowerEntry);
        }

        @Override // java.util.NavigableMap
        public K lowerKey(K k) {
            return d().lowerKey(k);
        }

        @Override // java.util.NavigableMap
        public NavigableSet<K> navigableKeySet() {
            return keySet();
        }

        @Override // java.util.NavigableMap
        public Map.Entry<K, Collection<V>> pollFirstEntry() {
            return f(entrySet().iterator());
        }

        @Override // java.util.NavigableMap
        public Map.Entry<K, Collection<V>> pollLastEntry() {
            return f(((a0) descendingMap()).entrySet().iterator());
        }

        @Override // b.i.b.b.e.g, java.util.SortedMap, java.util.NavigableMap
        public SortedMap subMap(Object obj, Object obj2) {
            return subMap(obj, true, obj2, false);
        }

        @Override // b.i.b.b.e.g, java.util.SortedMap, java.util.NavigableMap
        public SortedMap tailMap(Object obj) {
            return tailMap(obj, true);
        }

        @Override // java.util.NavigableMap
        public NavigableMap<K, Collection<V>> headMap(K k, boolean z2) {
            return new d(d().headMap(k, z2));
        }

        @Override // java.util.NavigableMap
        public NavigableMap<K, Collection<V>> subMap(K k, boolean z2, K k2, boolean z3) {
            return new d(d().subMap(k, z2, k2, z3));
        }

        @Override // java.util.NavigableMap
        public NavigableMap<K, Collection<V>> tailMap(K k, boolean z2) {
            return new d(d().tailMap(k, z2));
        }
    }

    /* compiled from: AbstractMapBasedMultimap.java */
    /* renamed from: b.i.b.b.e$e  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public class C0131e extends e<K, V>.h implements NavigableSet<K> {
        public C0131e(NavigableMap<K, Collection<V>> navigableMap) {
            super(navigableMap);
        }

        @Override // java.util.NavigableSet
        public K ceiling(K k) {
            return c().ceilingKey(k);
        }

        /* renamed from: d */
        public NavigableMap<K, Collection<V>> c() {
            return (NavigableMap) ((SortedMap) this.j);
        }

        @Override // java.util.NavigableSet
        public Iterator<K> descendingIterator() {
            return ((c) descendingSet()).iterator();
        }

        @Override // java.util.NavigableSet
        public NavigableSet<K> descendingSet() {
            return new C0131e(c().descendingMap());
        }

        @Override // java.util.NavigableSet
        public K floor(K k) {
            return c().floorKey(k);
        }

        @Override // b.i.b.b.e.h, java.util.SortedSet, java.util.NavigableSet
        public SortedSet headSet(Object obj) {
            return headSet(obj, false);
        }

        @Override // java.util.NavigableSet
        public K higher(K k) {
            return c().higherKey(k);
        }

        @Override // java.util.NavigableSet
        public K lower(K k) {
            return c().lowerKey(k);
        }

        @Override // java.util.NavigableSet
        public K pollFirst() {
            c.a aVar = (c.a) iterator();
            if (!aVar.hasNext()) {
                return null;
            }
            K k = (K) aVar.next();
            aVar.remove();
            return k;
        }

        @Override // java.util.NavigableSet
        public K pollLast() {
            Iterator<K> descendingIterator = descendingIterator();
            if (!descendingIterator.hasNext()) {
                return null;
            }
            K next = descendingIterator.next();
            descendingIterator.remove();
            return next;
        }

        @Override // b.i.b.b.e.h, java.util.SortedSet, java.util.NavigableSet
        public SortedSet subSet(Object obj, Object obj2) {
            return subSet(obj, true, obj2, false);
        }

        @Override // b.i.b.b.e.h, java.util.SortedSet, java.util.NavigableSet
        public SortedSet tailSet(Object obj) {
            return tailSet(obj, true);
        }

        @Override // java.util.NavigableSet
        public NavigableSet<K> headSet(K k, boolean z2) {
            return new C0131e(c().headMap(k, z2));
        }

        @Override // java.util.NavigableSet
        public NavigableSet<K> subSet(K k, boolean z2, K k2, boolean z3) {
            return new C0131e(c().subMap(k, z2, k2, z3));
        }

        @Override // java.util.NavigableSet
        public NavigableSet<K> tailSet(K k, boolean z2) {
            return new C0131e(c().tailMap(k, z2));
        }
    }

    /* compiled from: AbstractMapBasedMultimap.java */
    /* loaded from: classes3.dex */
    public class f extends e<K, V>.j implements RandomAccess {
        public f(@NullableDecl e eVar, K k, @NullableDecl List<V> list, e<K, V>.i iVar) {
            super(k, list, iVar);
        }
    }

    /* compiled from: AbstractMapBasedMultimap.java */
    /* loaded from: classes3.dex */
    public class g extends e<K, V>.a implements SortedMap<K, Collection<V>> {
        @MonotonicNonNullDecl
        public SortedSet<K> n;

        public g(SortedMap<K, Collection<V>> sortedMap) {
            super(sortedMap);
        }

        public SortedSet<K> b() {
            return new h(d());
        }

        /* renamed from: c */
        public SortedSet<K> keySet() {
            SortedSet<K> sortedSet = this.n;
            if (sortedSet != null) {
                return sortedSet;
            }
            SortedSet<K> b2 = b();
            this.n = b2;
            return b2;
        }

        @Override // java.util.SortedMap
        public Comparator<? super K> comparator() {
            return d().comparator();
        }

        public SortedMap<K, Collection<V>> d() {
            return (SortedMap) this.l;
        }

        @Override // java.util.SortedMap
        public K firstKey() {
            return d().firstKey();
        }

        public SortedMap<K, Collection<V>> headMap(K k) {
            return new g(d().headMap(k));
        }

        @Override // java.util.SortedMap
        public K lastKey() {
            return d().lastKey();
        }

        public SortedMap<K, Collection<V>> subMap(K k, K k2) {
            return new g(d().subMap(k, k2));
        }

        public SortedMap<K, Collection<V>> tailMap(K k) {
            return new g(d().tailMap(k));
        }
    }

    /* compiled from: AbstractMapBasedMultimap.java */
    /* loaded from: classes3.dex */
    public class h extends e<K, V>.c implements SortedSet<K> {
        public h(SortedMap<K, Collection<V>> sortedMap) {
            super(sortedMap);
        }

        public SortedMap<K, Collection<V>> c() {
            return (SortedMap) this.j;
        }

        @Override // java.util.SortedSet
        public Comparator<? super K> comparator() {
            return c().comparator();
        }

        @Override // java.util.SortedSet
        public K first() {
            return c().firstKey();
        }

        public SortedSet<K> headSet(K k) {
            return new h(c().headMap(k));
        }

        @Override // java.util.SortedSet
        public K last() {
            return c().lastKey();
        }

        public SortedSet<K> subSet(K k, K k2) {
            return new h(c().subMap(k, k2));
        }

        public SortedSet<K> tailSet(K k) {
            return new h(c().tailMap(k));
        }
    }

    public e(Map<K, Collection<V>> map) {
        b.i.a.f.e.o.f.v(map.isEmpty());
        this.m = map;
    }

    public static /* synthetic */ int b(e eVar) {
        int i2 = eVar.n;
        eVar.n = i2 + 1;
        return i2;
    }

    public static /* synthetic */ int c(e eVar) {
        int i2 = eVar.n;
        eVar.n = i2 - 1;
        return i2;
    }

    public void d() {
        for (Collection<V> collection : this.m.values()) {
            collection.clear();
        }
        this.m.clear();
        this.n = 0;
    }

    public abstract Collection<V> e();

    /* compiled from: AbstractMapBasedMultimap.java */
    /* loaded from: classes3.dex */
    public class j extends e<K, V>.i implements List<V> {

        /* compiled from: AbstractMapBasedMultimap.java */
        /* loaded from: classes3.dex */
        public class a extends e<K, V>.i.a implements ListIterator<V> {
            public a() {
                super();
            }

            @Override // java.util.ListIterator
            public void add(V v) {
                boolean isEmpty = j.this.isEmpty();
                b().add(v);
                e.b(e.this);
                if (isEmpty) {
                    j.this.c();
                }
            }

            public final ListIterator<V> b() {
                a();
                return (ListIterator) this.j;
            }

            @Override // java.util.ListIterator
            public boolean hasPrevious() {
                return b().hasPrevious();
            }

            @Override // java.util.ListIterator
            public int nextIndex() {
                return b().nextIndex();
            }

            @Override // java.util.ListIterator
            public V previous() {
                return b().previous();
            }

            @Override // java.util.ListIterator
            public int previousIndex() {
                return b().previousIndex();
            }

            @Override // java.util.ListIterator
            public void set(V v) {
                b().set(v);
            }

            public a(int i) {
                super(((List) j.this.k).listIterator(i));
            }
        }

        public j(@NullableDecl K k, List<V> list, @NullableDecl e<K, V>.i iVar) {
            super(k, list, iVar);
        }

        @Override // java.util.List
        public void add(int i, V v) {
            d();
            boolean isEmpty = this.k.isEmpty();
            ((List) this.k).add(i, v);
            e.b(e.this);
            if (isEmpty) {
                c();
            }
        }

        @Override // java.util.List
        public boolean addAll(int i, Collection<? extends V> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            int size = size();
            boolean addAll = ((List) this.k).addAll(i, collection);
            if (addAll) {
                int size2 = this.k.size();
                e eVar = e.this;
                eVar.n = (size2 - size) + eVar.n;
                if (size == 0) {
                    c();
                }
            }
            return addAll;
        }

        @Override // java.util.List
        public V get(int i) {
            d();
            return (V) ((List) this.k).get(i);
        }

        @Override // java.util.List
        public int indexOf(Object obj) {
            d();
            return ((List) this.k).indexOf(obj);
        }

        @Override // java.util.List
        public int lastIndexOf(Object obj) {
            d();
            return ((List) this.k).lastIndexOf(obj);
        }

        @Override // java.util.List
        public ListIterator<V> listIterator() {
            d();
            return new a();
        }

        @Override // java.util.List
        public V remove(int i) {
            d();
            V v = (V) ((List) this.k).remove(i);
            e.c(e.this);
            e();
            return v;
        }

        @Override // java.util.List
        public V set(int i, V v) {
            d();
            return (V) ((List) this.k).set(i, v);
        }

        @Override // java.util.List
        public List<V> subList(int i, int i2) {
            d();
            e eVar = e.this;
            K k = this.j;
            List subList = ((List) this.k).subList(i, i2);
            e<K, V>.i iVar = this.l;
            if (iVar == null) {
                iVar = this;
            }
            Objects.requireNonNull(eVar);
            return subList instanceof RandomAccess ? new f(eVar, k, subList, iVar) : new j(k, subList, iVar);
        }

        @Override // java.util.List
        public ListIterator<V> listIterator(int i) {
            d();
            return new a(i);
        }
    }

    /* compiled from: AbstractMapBasedMultimap.java */
    /* loaded from: classes3.dex */
    public class i extends AbstractCollection<V> {
        @NullableDecl
        public final K j;
        public Collection<V> k;
        @NullableDecl
        public final e<K, V>.i l;
        @NullableDecl
        public final Collection<V> m;

        public i(@NullableDecl K k, Collection<V> collection, @NullableDecl e<K, V>.i iVar) {
            this.j = k;
            this.k = collection;
            this.l = iVar;
            this.m = iVar == null ? null : iVar.k;
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean add(V v) {
            d();
            boolean isEmpty = this.k.isEmpty();
            boolean add = this.k.add(v);
            if (add) {
                e.b(e.this);
                if (isEmpty) {
                    c();
                }
            }
            return add;
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean addAll(Collection<? extends V> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            int size = size();
            boolean addAll = this.k.addAll(collection);
            if (addAll) {
                int size2 = this.k.size();
                e eVar = e.this;
                eVar.n = (size2 - size) + eVar.n;
                if (size == 0) {
                    c();
                }
            }
            return addAll;
        }

        public void c() {
            e<K, V>.i iVar = this.l;
            if (iVar != null) {
                iVar.c();
            } else {
                e.this.m.put(this.j, this.k);
            }
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public void clear() {
            int size = size();
            if (size != 0) {
                this.k.clear();
                e.this.n -= size;
                e();
            }
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean contains(Object obj) {
            d();
            return this.k.contains(obj);
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean containsAll(Collection<?> collection) {
            d();
            return this.k.containsAll(collection);
        }

        public void d() {
            Collection<V> collection;
            e<K, V>.i iVar = this.l;
            if (iVar != null) {
                iVar.d();
                if (this.l.k != this.m) {
                    throw new ConcurrentModificationException();
                }
            } else if (this.k.isEmpty() && (collection = e.this.m.get(this.j)) != null) {
                this.k = collection;
            }
        }

        public void e() {
            e<K, V>.i iVar = this.l;
            if (iVar != null) {
                iVar.e();
            } else if (this.k.isEmpty()) {
                e.this.m.remove(this.j);
            }
        }

        @Override // java.util.Collection
        public boolean equals(@NullableDecl Object obj) {
            if (obj == this) {
                return true;
            }
            d();
            return this.k.equals(obj);
        }

        @Override // java.util.Collection
        public int hashCode() {
            d();
            return this.k.hashCode();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
        public Iterator<V> iterator() {
            d();
            return new a();
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean remove(Object obj) {
            d();
            boolean remove = this.k.remove(obj);
            if (remove) {
                e.c(e.this);
                e();
            }
            return remove;
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean removeAll(Collection<?> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            int size = size();
            boolean removeAll = this.k.removeAll(collection);
            if (removeAll) {
                int size2 = this.k.size();
                e eVar = e.this;
                eVar.n = (size2 - size) + eVar.n;
                e();
            }
            return removeAll;
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean retainAll(Collection<?> collection) {
            Objects.requireNonNull(collection);
            int size = size();
            boolean retainAll = this.k.retainAll(collection);
            if (retainAll) {
                int size2 = this.k.size();
                e eVar = e.this;
                eVar.n = (size2 - size) + eVar.n;
                e();
            }
            return retainAll;
        }

        @Override // java.util.AbstractCollection, java.util.Collection
        public int size() {
            d();
            return this.k.size();
        }

        @Override // java.util.AbstractCollection
        public String toString() {
            d();
            return this.k.toString();
        }

        /* compiled from: AbstractMapBasedMultimap.java */
        /* loaded from: classes3.dex */
        public class a implements Iterator<V> {
            public final Iterator<V> j;
            public final Collection<V> k;

            public a() {
                Iterator<V> it;
                Collection<V> collection = i.this.k;
                this.k = collection;
                if (collection instanceof List) {
                    it = ((List) collection).listIterator();
                } else {
                    it = collection.iterator();
                }
                this.j = it;
            }

            public void a() {
                i.this.d();
                if (i.this.k != this.k) {
                    throw new ConcurrentModificationException();
                }
            }

            @Override // java.util.Iterator
            public boolean hasNext() {
                a();
                return this.j.hasNext();
            }

            @Override // java.util.Iterator
            public V next() {
                a();
                return this.j.next();
            }

            @Override // java.util.Iterator
            public void remove() {
                this.j.remove();
                e.c(e.this);
                i.this.e();
            }

            public a(Iterator<V> it) {
                this.k = i.this.k;
                this.j = it;
            }
        }
    }
}
