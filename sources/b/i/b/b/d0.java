package b.i.b.b;

import b.i.a.f.e.o.f;
import b.i.b.a.l;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
/* compiled from: Multimaps.java */
/* loaded from: classes3.dex */
public class d0<K, V> extends c<K, V> {
    private static final long serialVersionUID = 0;
    public transient l<? extends List<V>> o;

    public d0(Map<K, Collection<V>> map, l<? extends List<V>> lVar) {
        super(map);
        Objects.requireNonNull(lVar);
        this.o = lVar;
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.o = (l) objectInputStream.readObject();
        Map<K, Collection<V>> map = (Map) objectInputStream.readObject();
        this.m = map;
        this.n = 0;
        for (Collection<V> collection : map.values()) {
            f.v(!collection.isEmpty());
            this.n = collection.size() + this.n;
        }
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.o);
        objectOutputStream.writeObject(this.m);
    }

    @Override // b.i.b.b.e
    public Collection e() {
        return this.o.get();
    }
}
