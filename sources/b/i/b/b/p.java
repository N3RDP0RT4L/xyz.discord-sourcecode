package b.i.b.b;

import b.i.a.f.e.o.f;
import b.i.b.b.n;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.RandomAccess;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
/* compiled from: ImmutableList.java */
/* loaded from: classes3.dex */
public abstract class p<E> extends n<E> implements List<E>, RandomAccess {
    public static final b.i.b.b.a<Object> k = new b(h0.l, 0);

    /* compiled from: ImmutableList.java */
    /* loaded from: classes3.dex */
    public static final class a<E> extends n.a<E> {
        public a() {
            super(4);
        }

        @CanIgnoreReturnValue
        public a<E> b(E e) {
            Objects.requireNonNull(e);
            int i = this.f1643b + 1;
            Object[] objArr = this.a;
            if (objArr.length < i) {
                this.a = Arrays.copyOf(objArr, n.b.a(objArr.length, i));
                this.c = false;
            } else if (this.c) {
                this.a = (Object[]) objArr.clone();
                this.c = false;
            }
            Object[] objArr2 = this.a;
            int i2 = this.f1643b;
            this.f1643b = i2 + 1;
            objArr2[i2] = e;
            return this;
        }

        public p<E> c() {
            this.c = true;
            return p.l(this.a, this.f1643b);
        }
    }

    /* compiled from: ImmutableList.java */
    /* loaded from: classes3.dex */
    public static class b<E> extends b.i.b.b.a<E> {
        public final p<E> l;

        public b(p<E> pVar, int i) {
            super(pVar.size(), i);
            this.l = pVar;
        }
    }

    /* compiled from: ImmutableList.java */
    /* loaded from: classes3.dex */
    public static class c implements Serializable {
        private static final long serialVersionUID = 0;
        public final Object[] elements;

        public c(Object[] objArr) {
            this.elements = objArr;
        }

        public Object readResolve() {
            return p.o(this.elements);
        }
    }

    /* compiled from: ImmutableList.java */
    /* loaded from: classes3.dex */
    public class d extends p<E> {
        public final transient int l;
        public final transient int m;

        public d(int i, int i2) {
            this.l = i;
            this.m = i2;
        }

        @Override // b.i.b.b.n
        public Object[] e() {
            return p.this.e();
        }

        @Override // b.i.b.b.n
        public int g() {
            return p.this.h() + this.l + this.m;
        }

        @Override // java.util.List
        public E get(int i) {
            f.x(i, this.m);
            return p.this.get(i + this.l);
        }

        @Override // b.i.b.b.n
        public int h() {
            return p.this.h() + this.l;
        }

        @Override // b.i.b.b.n
        public boolean i() {
            return true;
        }

        @Override // b.i.b.b.p, b.i.b.b.n, java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
        public Iterator iterator() {
            return listIterator();
        }

        @Override // b.i.b.b.p, java.util.List
        public /* bridge */ /* synthetic */ ListIterator listIterator() {
            return listIterator();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
        public int size() {
            return this.m;
        }

        @Override // b.i.b.b.p
        /* renamed from: w */
        public p<E> subList(int i, int i2) {
            f.D(i, i2, this.m);
            p pVar = p.this;
            int i3 = this.l;
            return pVar.subList(i + i3, i2 + i3);
        }

        @Override // b.i.b.b.p, java.util.List
        public /* bridge */ /* synthetic */ ListIterator listIterator(int i) {
            return listIterator(i);
        }
    }

    public static <E> p<E> k(Object[] objArr) {
        return l(objArr, objArr.length);
    }

    public static <E> p<E> l(Object[] objArr, int i) {
        if (i == 0) {
            return (p<E>) h0.l;
        }
        return new h0(objArr, i);
    }

    public static <E> p<E> m(Object... objArr) {
        int length = objArr.length;
        for (int i = 0; i < length; i++) {
            f.y(objArr[i], i);
        }
        return l(objArr, objArr.length);
    }

    public static <E> p<E> n(Collection<? extends E> collection) {
        if (!(collection instanceof n)) {
            return m(collection.toArray());
        }
        p<E> c2 = ((n) collection).c();
        return c2.i() ? k(c2.toArray()) : c2;
    }

    public static <E> p<E> o(E[] eArr) {
        if (eArr.length == 0) {
            return (p<E>) h0.l;
        }
        return m((Object[]) eArr.clone());
    }

    private void readObject(ObjectInputStream objectInputStream) throws InvalidObjectException {
        throw new InvalidObjectException("Use SerializedForm");
    }

    public static <E> p<E> u(E e) {
        return m(e);
    }

    public static <E> p<E> v(E e, E e2, E e3, E e4, E e5) {
        return m(e, e2, e3, e4, e5);
    }

    @Override // java.util.List
    @Deprecated
    public final void add(int i, E e) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.List
    @CanIgnoreReturnValue
    @Deprecated
    public final boolean addAll(int i, Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    @Override // b.i.b.b.n
    public final p<E> c() {
        return this;
    }

    @Override // b.i.b.b.n, java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean contains(@NullableDecl Object obj) {
        return indexOf(obj) >= 0;
    }

    @Override // b.i.b.b.n
    public int d(Object[] objArr, int i) {
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            objArr[i + i2] = get(i2);
        }
        return i + size;
    }

    /* JADX WARN: Removed duplicated region for block: B:19:0x003f  */
    @Override // java.util.Collection, java.util.List
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public boolean equals(@org.checkerframework.checker.nullness.compatqual.NullableDecl java.lang.Object r7) {
        /*
            r6 = this;
            r0 = 0
            r1 = 1
            if (r7 != r6) goto L6
        L4:
            r0 = 1
            goto L5b
        L6:
            boolean r2 = r7 instanceof java.util.List
            if (r2 != 0) goto Lb
            goto L5b
        Lb:
            java.util.List r7 = (java.util.List) r7
            int r2 = r6.size()
            int r3 = r7.size()
            if (r2 == r3) goto L18
            goto L5b
        L18:
            boolean r3 = r7 instanceof java.util.RandomAccess
            if (r3 == 0) goto L31
            r3 = 0
        L1d:
            if (r3 >= r2) goto L4
            java.lang.Object r4 = r6.get(r3)
            java.lang.Object r5 = r7.get(r3)
            boolean r4 = b.i.a.f.e.o.f.V(r4, r5)
            if (r4 != 0) goto L2e
            goto L5b
        L2e:
            int r3 = r3 + 1
            goto L1d
        L31:
            java.util.Iterator r2 = r6.iterator()
            java.util.Iterator r7 = r7.iterator()
        L39:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L55
            boolean r3 = r7.hasNext()
            if (r3 != 0) goto L46
            goto L5b
        L46:
            java.lang.Object r3 = r2.next()
            java.lang.Object r4 = r7.next()
            boolean r3 = b.i.a.f.e.o.f.V(r3, r4)
            if (r3 != 0) goto L39
            goto L5b
        L55:
            boolean r7 = r7.hasNext()
            r0 = r7 ^ 1
        L5b:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.b.b.p.equals(java.lang.Object):boolean");
    }

    @Override // java.util.Collection, java.util.List
    public int hashCode() {
        int size = size();
        int i = 1;
        for (int i2 = 0; i2 < size; i2++) {
            i = ~(~(get(i2).hashCode() + (i * 31)));
        }
        return i;
    }

    @Override // java.util.List
    public int indexOf(@NullableDecl Object obj) {
        if (obj == null) {
            return -1;
        }
        int size = size();
        for (int i = 0; i < size; i++) {
            if (obj.equals(get(i))) {
                return i;
            }
        }
        return -1;
    }

    @Override // b.i.b.b.n, java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
    public Iterator iterator() {
        return listIterator();
    }

    @Override // b.i.b.b.n
    public s0<E> j() {
        return listIterator();
    }

    @Override // java.util.List
    public int lastIndexOf(@NullableDecl Object obj) {
        if (obj == null) {
            return -1;
        }
        for (int size = size() - 1; size >= 0; size--) {
            if (obj.equals(get(size))) {
                return size;
            }
        }
        return -1;
    }

    /* renamed from: p */
    public b.i.b.b.a<E> listIterator() {
        return listIterator(0);
    }

    /* renamed from: r */
    public b.i.b.b.a<E> listIterator(int i) {
        f.C(i, size());
        if (isEmpty()) {
            return (b.i.b.b.a<E>) k;
        }
        return new b(this, i);
    }

    @Override // java.util.List
    @CanIgnoreReturnValue
    @Deprecated
    public final E remove(int i) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.List
    @CanIgnoreReturnValue
    @Deprecated
    public final E set(int i, E e) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: w */
    public p<E> subList(int i, int i2) {
        f.D(i, i2, size());
        int i3 = i2 - i;
        if (i3 == size()) {
            return this;
        }
        if (i3 == 0) {
            return (p<E>) h0.l;
        }
        return new d(i, i3);
    }

    @Override // b.i.b.b.n
    public Object writeReplace() {
        return new c(toArray());
    }
}
