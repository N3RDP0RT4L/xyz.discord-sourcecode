package b.i.b.a;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
/* compiled from: Predicate.java */
/* loaded from: classes3.dex */
public interface h<T> {
    @CanIgnoreReturnValue
    boolean apply(@NullableDecl T t);
}
