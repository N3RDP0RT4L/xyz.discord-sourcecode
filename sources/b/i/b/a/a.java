package b.i.b.a;

import b.c.a.y.b;
import b.i.b.a.k;
import java.util.Iterator;
import java.util.NoSuchElementException;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
/* compiled from: AbstractIterator.java */
/* loaded from: classes3.dex */
public abstract class a<T> implements Iterator<T> {
    public int j = 2;
    @NullableDecl
    public T k;

    @Override // java.util.Iterator
    public final boolean hasNext() {
        T t;
        int a;
        int i = this.j;
        if (i != 4) {
            int h = b.h(i);
            if (h == 0) {
                return true;
            }
            if (h == 2) {
                return false;
            }
            this.j = 4;
            k.a aVar = (k.a) this;
            int i2 = aVar.o;
            while (true) {
                int i3 = aVar.o;
                if (i3 == -1) {
                    aVar.j = 3;
                    t = null;
                    break;
                }
                i iVar = (i) aVar;
                a = iVar.q.a.a(iVar.l, i3);
                if (a == -1) {
                    a = aVar.l.length();
                    aVar.o = -1;
                } else {
                    aVar.o = a + 1;
                }
                int i4 = aVar.o;
                if (i4 == i2) {
                    int i5 = i4 + 1;
                    aVar.o = i5;
                    if (i5 > aVar.l.length()) {
                        aVar.o = -1;
                    }
                } else {
                    while (i2 < a && aVar.m.b(aVar.l.charAt(i2))) {
                        i2++;
                    }
                    while (a > i2) {
                        int i6 = a - 1;
                        if (!aVar.m.b(aVar.l.charAt(i6))) {
                            break;
                        }
                        a = i6;
                    }
                    if (!aVar.n || i2 != a) {
                        break;
                    }
                    i2 = aVar.o;
                }
            }
            int i7 = aVar.p;
            if (i7 == 1) {
                a = aVar.l.length();
                aVar.o = -1;
                while (a > i2) {
                    int i8 = a - 1;
                    if (!aVar.m.b(aVar.l.charAt(i8))) {
                        break;
                    }
                    a = i8;
                }
            } else {
                aVar.p = i7 - 1;
            }
            t = (T) aVar.l.subSequence(i2, a).toString();
            this.k = t;
            if (this.j == 3) {
                return false;
            }
            this.j = 1;
            return true;
        }
        throw new IllegalStateException();
    }

    @Override // java.util.Iterator
    public final T next() {
        if (hasNext()) {
            this.j = 2;
            T t = this.k;
            this.k = null;
            return t;
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Iterator
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
