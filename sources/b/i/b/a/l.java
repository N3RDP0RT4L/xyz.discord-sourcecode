package b.i.b.a;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
/* compiled from: Supplier.java */
/* loaded from: classes3.dex */
public interface l<T> {
    @CanIgnoreReturnValue
    T get();
}
