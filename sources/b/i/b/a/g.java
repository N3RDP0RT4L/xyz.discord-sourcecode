package b.i.b.a;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Arrays;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
/* compiled from: MoreObjects.java */
/* loaded from: classes3.dex */
public final class g {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final a f1639b;
    public a c;

    /* compiled from: MoreObjects.java */
    /* loaded from: classes3.dex */
    public static final class a {
        @NullableDecl
        public String a;
        @NullableDecl

        /* renamed from: b  reason: collision with root package name */
        public Object f1640b;
        @NullableDecl
        public a c;

        public a(f fVar) {
        }
    }

    public g(String str, f fVar) {
        a aVar = new a(null);
        this.f1639b = aVar;
        this.c = aVar;
        this.a = str;
    }

    @CanIgnoreReturnValue
    public g a(String str, int i) {
        String valueOf = String.valueOf(i);
        a aVar = new a(null);
        this.c.c = aVar;
        this.c = aVar;
        aVar.f1640b = valueOf;
        aVar.a = str;
        return this;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(32);
        sb.append(this.a);
        sb.append('{');
        a aVar = this.f1639b.c;
        String str = "";
        while (aVar != null) {
            Object obj = aVar.f1640b;
            sb.append(str);
            String str2 = aVar.a;
            if (str2 != null) {
                sb.append(str2);
                sb.append('=');
            }
            if (obj == null || !obj.getClass().isArray()) {
                sb.append(obj);
            } else {
                String deepToString = Arrays.deepToString(new Object[]{obj});
                sb.append((CharSequence) deepToString, 1, deepToString.length() - 1);
            }
            aVar = aVar.c;
            str = ", ";
        }
        sb.append('}');
        return sb.toString();
    }
}
