package b.i.a.c.e3;

import java.io.IOException;
/* compiled from: DataSink.java */
/* loaded from: classes3.dex */
public interface j {
    void a(n nVar) throws IOException;

    void close() throws IOException;

    void write(byte[] bArr, int i, int i2) throws IOException;
}
