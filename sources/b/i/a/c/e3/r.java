package b.i.a.c.e3;

import android.net.Uri;
import androidx.annotation.Nullable;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.q;
import b.i.b.a.h;
import com.adjust.sdk.Constants;
import com.discord.analytics.utils.RegistrationSteps;
import com.discord.api.permission.Permission;
import com.google.android.exoplayer2.upstream.HttpDataSource$HttpDataSourceException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;
/* compiled from: DefaultHttpDataSource.java */
/* loaded from: classes3.dex */
public class r extends g implements l {
    public final boolean e;
    public final int f;
    public final int g;
    @Nullable
    public final String h;
    @Nullable
    public final u i;
    public final boolean k;
    @Nullable
    public n m;
    @Nullable
    public HttpURLConnection n;
    @Nullable
    public InputStream o;
    public boolean p;
    public int q;
    public long r;

    /* renamed from: s  reason: collision with root package name */
    public long f946s;
    @Nullable
    public h<String> l = null;
    public final u j = new u();

    /* compiled from: DefaultHttpDataSource.java */
    /* loaded from: classes3.dex */
    public static final class b implements t {
        @Nullable

        /* renamed from: b  reason: collision with root package name */
        public String f947b;
        public final u a = new u();
        public int c = 8000;
        public int d = 8000;

        /* renamed from: b */
        public r a() {
            return new r(this.f947b, this.c, this.d, false, this.a, null, false, null);
        }
    }

    public r(String str, int i, int i2, boolean z2, u uVar, h hVar, boolean z3, a aVar) {
        super(true);
        this.h = str;
        this.f = i;
        this.g = i2;
        this.e = z2;
        this.i = uVar;
        this.k = z3;
    }

    public static void x(@Nullable HttpURLConnection httpURLConnection, long j) {
        int i;
        if (httpURLConnection != null && (i = e0.a) >= 19 && i <= 20) {
            try {
                InputStream inputStream = httpURLConnection.getInputStream();
                if (j == -1) {
                    if (inputStream.read() == -1) {
                        return;
                    }
                } else if (j <= Permission.SEND_MESSAGES) {
                    return;
                }
                String name = inputStream.getClass().getName();
                if ("com.android.okhttp.internal.http.HttpTransport$ChunkedInputStream".equals(name) || "com.android.okhttp.internal.http.HttpTransport$FixedLengthInputStream".equals(name)) {
                    Class<? super Object> superclass = inputStream.getClass().getSuperclass();
                    Objects.requireNonNull(superclass);
                    Method declaredMethod = superclass.getDeclaredMethod("unexpectedEndOfInput", new Class[0]);
                    declaredMethod.setAccessible(true);
                    declaredMethod.invoke(inputStream, new Object[0]);
                }
            } catch (Exception unused) {
            }
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:17:0x0050, code lost:
        if (r5 != 0) goto L19;
     */
    /* JADX WARN: Removed duplicated region for block: B:32:0x00b2  */
    /* JADX WARN: Removed duplicated region for block: B:44:0x0146  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x0148  */
    /* JADX WARN: Removed duplicated region for block: B:76:0x01ca  */
    @Override // b.i.a.c.e3.l
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public long a(final b.i.a.c.e3.n r23) throws com.google.android.exoplayer2.upstream.HttpDataSource$HttpDataSourceException {
        /*
            Method dump skipped, instructions count: 532
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.e3.r.a(b.i.a.c.e3.n):long");
    }

    @Override // b.i.a.c.e3.l
    public void close() throws HttpDataSource$HttpDataSourceException {
        try {
            InputStream inputStream = this.o;
            if (inputStream != null) {
                long j = this.r;
                long j2 = -1;
                if (j != -1) {
                    j2 = j - this.f946s;
                }
                x(this.n, j2);
                try {
                    inputStream.close();
                } catch (IOException e) {
                    n nVar = this.m;
                    int i = e0.a;
                    throw new HttpDataSource$HttpDataSourceException(e, nVar, 2000, 3);
                }
            }
        } finally {
            this.o = null;
            t();
            if (this.p) {
                this.p = false;
                q();
            }
        }
    }

    @Override // b.i.a.c.e3.g, b.i.a.c.e3.l
    public Map<String, List<String>> j() {
        HttpURLConnection httpURLConnection = this.n;
        return httpURLConnection == null ? Collections.emptyMap() : httpURLConnection.getHeaderFields();
    }

    @Override // b.i.a.c.e3.l
    @Nullable
    public Uri n() {
        HttpURLConnection httpURLConnection = this.n;
        if (httpURLConnection == null) {
            return null;
        }
        return Uri.parse(httpURLConnection.getURL().toString());
    }

    @Override // b.i.a.c.e3.h
    public int read(byte[] bArr, int i, int i2) throws HttpDataSource$HttpDataSourceException {
        if (i2 == 0) {
            return 0;
        }
        try {
            long j = this.r;
            if (j != -1) {
                long j2 = j - this.f946s;
                if (j2 == 0) {
                    return -1;
                }
                i2 = (int) Math.min(i2, j2);
            }
            InputStream inputStream = this.o;
            int i3 = e0.a;
            int read = inputStream.read(bArr, i, i2);
            if (read != -1) {
                this.f946s += read;
                p(read);
                return read;
            }
            return -1;
        } catch (IOException e) {
            n nVar = this.m;
            int i4 = e0.a;
            throw HttpDataSource$HttpDataSourceException.b(e, nVar, 2);
        }
    }

    public final void t() {
        HttpURLConnection httpURLConnection = this.n;
        if (httpURLConnection != null) {
            try {
                httpURLConnection.disconnect();
            } catch (Exception e) {
                q.b("DefaultHttpDataSource", "Unexpected error while disconnecting", e);
            }
            this.n = null;
        }
    }

    public final URL u(URL url, @Nullable String str, n nVar) throws HttpDataSource$HttpDataSourceException {
        if (str != null) {
            try {
                URL url2 = new URL(url, str);
                String protocol = url2.getProtocol();
                if (!Constants.SCHEME.equals(protocol) && !"http".equals(protocol)) {
                    String valueOf = String.valueOf(protocol);
                    throw new HttpDataSource$HttpDataSourceException(valueOf.length() != 0 ? "Unsupported protocol redirect: ".concat(valueOf) : new String("Unsupported protocol redirect: "), nVar, 2001, 1);
                } else if (this.e || protocol.equals(url.getProtocol())) {
                    return url2;
                } else {
                    String protocol2 = url.getProtocol();
                    StringBuilder Q = b.d.b.a.a.Q(protocol.length() + b.d.b.a.a.b(protocol2, 41), "Disallowed cross-protocol redirect (", protocol2, " to ", protocol);
                    Q.append(")");
                    throw new HttpDataSource$HttpDataSourceException(Q.toString(), nVar, 2001, 1);
                }
            } catch (MalformedURLException e) {
                throw new HttpDataSource$HttpDataSourceException(e, nVar, 2001, 1);
            }
        } else {
            throw new HttpDataSource$HttpDataSourceException("Null location redirect", nVar, 2001, 1);
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:38:0x00ad, code lost:
        return r0;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.net.HttpURLConnection v(b.i.a.c.e3.n r25) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 217
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.e3.r.v(b.i.a.c.e3.n):java.net.HttpURLConnection");
    }

    public final HttpURLConnection w(URL url, int i, @Nullable byte[] bArr, long j, long j2, boolean z2, boolean z3, Map<String, String> map) throws IOException {
        String str;
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setConnectTimeout(this.f);
        httpURLConnection.setReadTimeout(this.g);
        HashMap hashMap = new HashMap();
        u uVar = this.i;
        if (uVar != null) {
            hashMap.putAll(uVar.a());
        }
        hashMap.putAll(this.j.a());
        hashMap.putAll(map);
        for (Map.Entry entry : hashMap.entrySet()) {
            httpURLConnection.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
        }
        Pattern pattern = v.a;
        if (j == 0 && j2 == -1) {
            str = null;
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("bytes=");
            sb.append(j);
            sb.append("-");
            if (j2 != -1) {
                sb.append((j + j2) - 1);
            }
            str = sb.toString();
        }
        if (str != null) {
            httpURLConnection.setRequestProperty("Range", str);
        }
        String str2 = this.h;
        if (str2 != null) {
            httpURLConnection.setRequestProperty("User-Agent", str2);
        }
        httpURLConnection.setRequestProperty("Accept-Encoding", z2 ? "gzip" : RegistrationSteps.IDENTITY);
        httpURLConnection.setInstanceFollowRedirects(z3);
        httpURLConnection.setDoOutput(bArr != null);
        httpURLConnection.setRequestMethod(n.b(i));
        if (bArr != null) {
            httpURLConnection.setFixedLengthStreamingMode(bArr.length);
            httpURLConnection.connect();
            OutputStream outputStream = httpURLConnection.getOutputStream();
            outputStream.write(bArr);
            outputStream.close();
        } else {
            httpURLConnection.connect();
        }
        return httpURLConnection;
    }

    public final void y(long j, n nVar) throws IOException {
        if (j != 0) {
            byte[] bArr = new byte[4096];
            while (j > 0) {
                int min = (int) Math.min(j, 4096);
                InputStream inputStream = this.o;
                int i = e0.a;
                int read = inputStream.read(bArr, 0, min);
                if (Thread.currentThread().isInterrupted()) {
                    throw new HttpDataSource$HttpDataSourceException(new InterruptedIOException(), nVar, 2000, 1);
                } else if (read != -1) {
                    j -= read;
                    p(read);
                } else {
                    throw new HttpDataSource$HttpDataSourceException(nVar, 2008, 1);
                }
            }
        }
    }
}
