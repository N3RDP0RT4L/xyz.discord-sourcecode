package b.i.a.c.e3;

import android.net.Uri;
import androidx.annotation.Nullable;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
/* compiled from: TeeDataSource.java */
/* loaded from: classes3.dex */
public final class z implements l {
    public final l a;

    /* renamed from: b  reason: collision with root package name */
    public final j f953b;
    public boolean c;
    public long d;

    public z(l lVar, j jVar) {
        this.a = lVar;
        this.f953b = jVar;
    }

    @Override // b.i.a.c.e3.l
    public long a(n nVar) throws IOException {
        n nVar2 = nVar;
        long a = this.a.a(nVar2);
        this.d = a;
        if (a == 0) {
            return 0L;
        }
        long j = nVar2.g;
        if (j == -1 && a != -1) {
            nVar2 = j == a ? nVar2 : new n(nVar2.a, nVar2.f938b, nVar2.c, nVar2.d, nVar2.e, nVar2.f + 0, a, nVar2.h, nVar2.i, nVar2.j);
        }
        this.c = true;
        this.f953b.a(nVar2);
        return this.d;
    }

    @Override // b.i.a.c.e3.l
    public void close() throws IOException {
        try {
            this.a.close();
        } finally {
            if (this.c) {
                this.c = false;
                this.f953b.close();
            }
        }
    }

    @Override // b.i.a.c.e3.l
    public void d(a0 a0Var) {
        Objects.requireNonNull(a0Var);
        this.a.d(a0Var);
    }

    @Override // b.i.a.c.e3.l
    public Map<String, List<String>> j() {
        return this.a.j();
    }

    @Override // b.i.a.c.e3.l
    @Nullable
    public Uri n() {
        return this.a.n();
    }

    @Override // b.i.a.c.e3.h
    public int read(byte[] bArr, int i, int i2) throws IOException {
        if (this.d == 0) {
            return -1;
        }
        int read = this.a.read(bArr, i, i2);
        if (read > 0) {
            this.f953b.write(bArr, i, read);
            long j = this.d;
            if (j != -1) {
                this.d = j - read;
            }
        }
        return read;
    }
}
