package b.i.a.c.e3.b0;

import androidx.annotation.Nullable;
import b.d.b.a.a;
import java.io.File;
/* compiled from: CacheSpan.java */
/* loaded from: classes3.dex */
public class h implements Comparable<h> {
    public final String j;
    public final long k;
    public final long l;
    public final boolean m;
    @Nullable
    public final File n;
    public final long o;

    public h(String str, long j, long j2, long j3, @Nullable File file) {
        this.j = str;
        this.k = j;
        this.l = j2;
        this.m = file != null;
        this.n = file;
        this.o = j3;
    }

    /* renamed from: f */
    public int compareTo(h hVar) {
        if (!this.j.equals(hVar.j)) {
            return this.j.compareTo(hVar.j);
        }
        int i = ((this.k - hVar.k) > 0L ? 1 : ((this.k - hVar.k) == 0L ? 0 : -1));
        if (i == 0) {
            return 0;
        }
        return i < 0 ? -1 : 1;
    }

    public String toString() {
        long j = this.k;
        return a.B(a.P(44, "[", j, ", "), this.l, "]");
    }
}
