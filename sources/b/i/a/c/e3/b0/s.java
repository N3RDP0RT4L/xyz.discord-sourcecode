package b.i.a.c.e3.b0;

import androidx.annotation.Nullable;
import java.io.File;
import java.util.regex.Pattern;
/* compiled from: SimpleCacheSpan.java */
/* loaded from: classes3.dex */
public final class s extends h {
    public static final Pattern p = Pattern.compile("^(.+)\\.(\\d+)\\.(\\d+)\\.v1\\.exo$", 32);
    public static final Pattern q = Pattern.compile("^(.+)\\.(\\d+)\\.(\\d+)\\.v2\\.exo$", 32);
    public static final Pattern r = Pattern.compile("^(\\d+)\\.(\\d+)\\.(\\d+)\\.v3\\.exo$", 32);

    public s(String str, long j, long j2, long j3, @Nullable File file) {
        super(str, j, j2, j3, file);
    }

    /* JADX WARN: Code restructure failed: missing block: B:30:0x00d1, code lost:
        if (r16.renameTo(r1) == false) goto L28;
     */
    /* JADX WARN: Removed duplicated region for block: B:29:0x00a4  */
    /* JADX WARN: Removed duplicated region for block: B:32:0x00d6 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:33:0x00d7  */
    @androidx.annotation.Nullable
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static b.i.a.c.e3.b0.s g(java.io.File r16, long r17, long r19, b.i.a.c.e3.b0.j r21) {
        /*
            Method dump skipped, instructions count: 324
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.e3.b0.s.g(java.io.File, long, long, b.i.a.c.e3.b0.j):b.i.a.c.e3.b0.s");
    }

    public static File h(File file, int i, long j, long j2) {
        StringBuilder sb = new StringBuilder(60);
        sb.append(i);
        sb.append(".");
        sb.append(j);
        sb.append(".");
        sb.append(j2);
        sb.append(".v3.exo");
        return new File(file, sb.toString());
    }
}
