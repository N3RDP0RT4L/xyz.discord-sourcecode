package b.i.a.c.e3.b0;

import androidx.annotation.Nullable;
import java.util.ArrayList;
import java.util.TreeSet;
/* compiled from: CachedContent.java */
/* loaded from: classes3.dex */
public final class i {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final String f926b;
    public final TreeSet<s> c = new TreeSet<>();
    public final ArrayList<a> d = new ArrayList<>();
    public n e;

    /* compiled from: CachedContent.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public final long a;

        /* renamed from: b  reason: collision with root package name */
        public final long f927b;

        public a(long j, long j2) {
            this.a = j;
            this.f927b = j2;
        }
    }

    public i(int i, String str, n nVar) {
        this.a = i;
        this.f926b = str;
        this.e = nVar;
    }

    /* JADX WARN: Code restructure failed: missing block: B:16:0x0035, code lost:
        if ((r11 + r13) <= (r5 + r3)) goto L9;
     */
    /* JADX WARN: Code restructure failed: missing block: B:8:0x001f, code lost:
        if (r11 >= r2.a) goto L9;
     */
    /* JADX WARN: Code restructure failed: missing block: B:9:0x0021, code lost:
        r2 = true;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public boolean a(long r11, long r13) {
        /*
            r10 = this;
            r0 = 0
            r1 = 0
        L2:
            java.util.ArrayList<b.i.a.c.e3.b0.i$a> r2 = r10.d
            int r2 = r2.size()
            if (r1 >= r2) goto L3e
            java.util.ArrayList<b.i.a.c.e3.b0.i$a> r2 = r10.d
            java.lang.Object r2 = r2.get(r1)
            b.i.a.c.e3.b0.i$a r2 = (b.i.a.c.e3.b0.i.a) r2
            long r3 = r2.f927b
            r5 = -1
            r7 = 1
            int r8 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r8 != 0) goto L25
            long r2 = r2.a
            int r4 = (r11 > r2 ? 1 : (r11 == r2 ? 0 : -1))
            if (r4 < 0) goto L23
        L21:
            r2 = 1
            goto L38
        L23:
            r2 = 0
            goto L38
        L25:
            int r8 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
            if (r8 != 0) goto L2a
            goto L23
        L2a:
            long r5 = r2.a
            int r2 = (r5 > r11 ? 1 : (r5 == r11 ? 0 : -1))
            if (r2 > 0) goto L23
            long r8 = r11 + r13
            long r5 = r5 + r3
            int r2 = (r8 > r5 ? 1 : (r8 == r5 ? 0 : -1))
            if (r2 > 0) goto L23
            goto L21
        L38:
            if (r2 == 0) goto L3b
            return r7
        L3b:
            int r1 = r1 + 1
            goto L2
        L3e:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.e3.b0.i.a(long, long):boolean");
    }

    public boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || i.class != obj.getClass()) {
            return false;
        }
        i iVar = (i) obj;
        return this.a == iVar.a && this.f926b.equals(iVar.f926b) && this.c.equals(iVar.c) && this.e.equals(iVar.e);
    }

    public int hashCode() {
        return this.e.hashCode() + b.d.b.a.a.m(this.f926b, this.a * 31, 31);
    }
}
