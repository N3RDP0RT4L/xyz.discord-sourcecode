package b.i.a.c.e3.b0;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import b.i.a.c.f3.e;
import b.i.a.c.f3.e0;
import b.i.a.c.u2.d;
import com.discord.models.domain.ModelAuditLogEntry;
import com.google.android.exoplayer2.database.DatabaseIOException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
/* compiled from: CachedContentIndex.java */
/* loaded from: classes3.dex */
public class j {
    public final HashMap<String, i> a;

    /* renamed from: b  reason: collision with root package name */
    public final SparseArray<String> f928b;
    public final SparseBooleanArray c;
    public final SparseBooleanArray d;
    public c e;
    @Nullable
    public c f;

    /* compiled from: CachedContentIndex.java */
    /* loaded from: classes3.dex */
    public static final class a implements c {
        public static final String[] a = {ModelAuditLogEntry.CHANGE_KEY_ID, "key", "metadata"};

        /* renamed from: b  reason: collision with root package name */
        public final b.i.a.c.u2.a f929b;
        public final SparseArray<i> c = new SparseArray<>();
        public String d;
        public String e;

        public a(b.i.a.c.u2.a aVar) {
            this.f929b = aVar;
        }

        public static void j(SQLiteDatabase sQLiteDatabase, String str) {
            String valueOf = String.valueOf(str);
            sQLiteDatabase.execSQL(valueOf.length() != 0 ? "DROP TABLE IF EXISTS ".concat(valueOf) : new String("DROP TABLE IF EXISTS "));
        }

        public static String k(String str) {
            String valueOf = String.valueOf(str);
            return valueOf.length() != 0 ? "ExoPlayerCacheIndex".concat(valueOf) : new String("ExoPlayerCacheIndex");
        }

        @Override // b.i.a.c.e3.b0.j.c
        public void a(i iVar, boolean z2) {
            if (z2) {
                this.c.delete(iVar.a);
            } else {
                this.c.put(iVar.a, null);
            }
        }

        @Override // b.i.a.c.e3.b0.j.c
        public void b(HashMap<String, i> hashMap) throws IOException {
            try {
                SQLiteDatabase writableDatabase = this.f929b.getWritableDatabase();
                writableDatabase.beginTransactionNonExclusive();
                l(writableDatabase);
                for (i iVar : hashMap.values()) {
                    i(writableDatabase, iVar);
                }
                writableDatabase.setTransactionSuccessful();
                this.c.clear();
                writableDatabase.endTransaction();
            } catch (SQLException e) {
                throw new DatabaseIOException(e);
            }
        }

        @Override // b.i.a.c.e3.b0.j.c
        public void c(i iVar) {
            this.c.put(iVar.a, iVar);
        }

        @Override // b.i.a.c.e3.b0.j.c
        public boolean d() throws DatabaseIOException {
            SQLiteDatabase readableDatabase = this.f929b.getReadableDatabase();
            String str = this.d;
            Objects.requireNonNull(str);
            return d.a(readableDatabase, 1, str) != -1;
        }

        @Override // b.i.a.c.e3.b0.j.c
        public void e(HashMap<String, i> hashMap) throws IOException {
            if (this.c.size() != 0) {
                try {
                    SQLiteDatabase writableDatabase = this.f929b.getWritableDatabase();
                    writableDatabase.beginTransactionNonExclusive();
                    for (int i = 0; i < this.c.size(); i++) {
                        i valueAt = this.c.valueAt(i);
                        if (valueAt == null) {
                            int keyAt = this.c.keyAt(i);
                            String str = this.e;
                            Objects.requireNonNull(str);
                            writableDatabase.delete(str, "id = ?", new String[]{Integer.toString(keyAt)});
                        } else {
                            i(writableDatabase, valueAt);
                        }
                    }
                    writableDatabase.setTransactionSuccessful();
                    this.c.clear();
                    writableDatabase.endTransaction();
                } catch (SQLException e) {
                    throw new DatabaseIOException(e);
                }
            }
        }

        @Override // b.i.a.c.e3.b0.j.c
        public void f(long j) {
            String hexString = Long.toHexString(j);
            this.d = hexString;
            this.e = k(hexString);
        }

        @Override // b.i.a.c.e3.b0.j.c
        public void g(HashMap<String, i> hashMap, SparseArray<String> sparseArray) throws IOException {
            b.c.a.a0.d.D(this.c.size() == 0);
            try {
                SQLiteDatabase readableDatabase = this.f929b.getReadableDatabase();
                String str = this.d;
                Objects.requireNonNull(str);
                if (d.a(readableDatabase, 1, str) != 1) {
                    SQLiteDatabase writableDatabase = this.f929b.getWritableDatabase();
                    writableDatabase.beginTransactionNonExclusive();
                    l(writableDatabase);
                    writableDatabase.setTransactionSuccessful();
                    writableDatabase.endTransaction();
                }
                SQLiteDatabase readableDatabase2 = this.f929b.getReadableDatabase();
                String str2 = this.e;
                Objects.requireNonNull(str2);
                Cursor query = readableDatabase2.query(str2, a, null, null, null, null, null);
                while (query.moveToNext()) {
                    int i = query.getInt(0);
                    String string = query.getString(1);
                    Objects.requireNonNull(string);
                    hashMap.put(string, new i(i, string, j.a(new DataInputStream(new ByteArrayInputStream(query.getBlob(2))))));
                    sparseArray.put(i, string);
                }
                query.close();
            } catch (SQLiteException e) {
                hashMap.clear();
                sparseArray.clear();
                throw new DatabaseIOException(e);
            }
        }

        @Override // b.i.a.c.e3.b0.j.c
        public void h() throws DatabaseIOException {
            b.i.a.c.u2.a aVar = this.f929b;
            String str = this.d;
            Objects.requireNonNull(str);
            try {
                String k = k(str);
                SQLiteDatabase writableDatabase = aVar.getWritableDatabase();
                writableDatabase.beginTransactionNonExclusive();
                int i = d.a;
                try {
                    if (e0.J(writableDatabase, "ExoPlayerVersions")) {
                        writableDatabase.delete("ExoPlayerVersions", "feature = ? AND instance_uid = ?", new String[]{Integer.toString(1), str});
                    }
                    j(writableDatabase, k);
                    writableDatabase.setTransactionSuccessful();
                    writableDatabase.endTransaction();
                } catch (SQLException e) {
                    throw new DatabaseIOException(e);
                }
            } catch (SQLException e2) {
                throw new DatabaseIOException(e2);
            }
        }

        public final void i(SQLiteDatabase sQLiteDatabase, i iVar) throws IOException {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            j.b(iVar.e, new DataOutputStream(byteArrayOutputStream));
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            ContentValues contentValues = new ContentValues();
            contentValues.put(ModelAuditLogEntry.CHANGE_KEY_ID, Integer.valueOf(iVar.a));
            contentValues.put("key", iVar.f926b);
            contentValues.put("metadata", byteArray);
            String str = this.e;
            Objects.requireNonNull(str);
            sQLiteDatabase.replaceOrThrow(str, null, contentValues);
        }

        public final void l(SQLiteDatabase sQLiteDatabase) throws DatabaseIOException {
            String str = this.d;
            Objects.requireNonNull(str);
            d.b(sQLiteDatabase, 1, str, 1);
            String str2 = this.e;
            Objects.requireNonNull(str2);
            j(sQLiteDatabase, str2);
            String str3 = this.e;
            sQLiteDatabase.execSQL(b.d.b.a.a.k(b.d.b.a.a.b(str3, 88), "CREATE TABLE ", str3, " ", "(id INTEGER PRIMARY KEY NOT NULL,key TEXT NOT NULL,metadata BLOB NOT NULL)"));
        }
    }

    /* compiled from: CachedContentIndex.java */
    /* loaded from: classes3.dex */
    public static class b implements c {
        public final boolean a;
        @Nullable

        /* renamed from: b  reason: collision with root package name */
        public final Cipher f930b;
        @Nullable
        public final SecretKeySpec c;
        @Nullable
        public final SecureRandom d;
        public final e e;
        public boolean f;
        @Nullable
        public p g;

        public b(File file, @Nullable byte[] bArr, boolean z2) {
            SecretKeySpec secretKeySpec;
            Cipher cipher;
            boolean z3 = false;
            b.c.a.a0.d.D(bArr != null || !z2);
            SecureRandom secureRandom = null;
            if (bArr != null) {
                b.c.a.a0.d.j(bArr.length == 16 ? true : z3);
                try {
                    if (e0.a == 18) {
                        try {
                            cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING", "BC");
                        } catch (Throwable unused) {
                        }
                        secretKeySpec = new SecretKeySpec(bArr, "AES");
                    }
                    cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
                    secretKeySpec = new SecretKeySpec(bArr, "AES");
                } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
                    throw new IllegalStateException(e);
                }
            } else {
                b.c.a.a0.d.j(!z2);
                cipher = null;
                secretKeySpec = null;
            }
            this.a = z2;
            this.f930b = cipher;
            this.c = secretKeySpec;
            this.d = z2 ? new SecureRandom() : secureRandom;
            this.e = new e(file);
        }

        @Override // b.i.a.c.e3.b0.j.c
        public void a(i iVar, boolean z2) {
            this.f = true;
        }

        @Override // b.i.a.c.e3.b0.j.c
        public void b(HashMap<String, i> hashMap) throws IOException {
            Throwable th;
            Throwable e;
            DataOutputStream dataOutputStream = null;
            try {
                OutputStream c = this.e.c();
                p pVar = this.g;
                if (pVar == null) {
                    this.g = new p(c);
                } else {
                    pVar.a(c);
                }
                p pVar2 = this.g;
                DataOutputStream dataOutputStream2 = new DataOutputStream(pVar2);
                try {
                    dataOutputStream2.writeInt(2);
                    dataOutputStream2.writeInt(this.a ? 1 : 0);
                    if (this.a) {
                        byte[] bArr = new byte[16];
                        SecureRandom secureRandom = this.d;
                        int i = e0.a;
                        secureRandom.nextBytes(bArr);
                        dataOutputStream2.write(bArr);
                        try {
                            this.f930b.init(1, this.c, new IvParameterSpec(bArr));
                            dataOutputStream2.flush();
                            dataOutputStream2 = new DataOutputStream(new CipherOutputStream(pVar2, this.f930b));
                        } catch (InvalidAlgorithmParameterException e2) {
                            e = e2;
                            throw new IllegalStateException(e);
                        } catch (InvalidKeyException e3) {
                            e = e3;
                            throw new IllegalStateException(e);
                        }
                    }
                    dataOutputStream2.writeInt(hashMap.size());
                    int i2 = 0;
                    for (i iVar : hashMap.values()) {
                        dataOutputStream2.writeInt(iVar.a);
                        dataOutputStream2.writeUTF(iVar.f926b);
                        j.b(iVar.e, dataOutputStream2);
                        i2 += i(iVar, 2);
                    }
                    dataOutputStream2.writeInt(i2);
                    e eVar = this.e;
                    Objects.requireNonNull(eVar);
                    dataOutputStream2.close();
                    eVar.f963b.delete();
                    int i3 = e0.a;
                    this.f = false;
                } catch (Throwable th2) {
                    th = th2;
                    dataOutputStream = dataOutputStream2;
                    int i4 = e0.a;
                    if (dataOutputStream != null) {
                        try {
                            dataOutputStream.close();
                        } catch (IOException unused) {
                        }
                    }
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
            }
        }

        @Override // b.i.a.c.e3.b0.j.c
        public void c(i iVar) {
            this.f = true;
        }

        @Override // b.i.a.c.e3.b0.j.c
        public boolean d() {
            return this.e.a();
        }

        @Override // b.i.a.c.e3.b0.j.c
        public void e(HashMap<String, i> hashMap) throws IOException {
            if (this.f) {
                b(hashMap);
            }
        }

        @Override // b.i.a.c.e3.b0.j.c
        public void f(long j) {
        }

        /* JADX WARN: Removed duplicated region for block: B:56:0x00c7  */
        /* JADX WARN: Removed duplicated region for block: B:72:? A[RETURN, SYNTHETIC] */
        @Override // b.i.a.c.e3.b0.j.c
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public void g(java.util.HashMap<java.lang.String, b.i.a.c.e3.b0.i> r11, android.util.SparseArray<java.lang.String> r12) {
            /*
                Method dump skipped, instructions count: 218
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.e3.b0.j.b.g(java.util.HashMap, android.util.SparseArray):void");
        }

        @Override // b.i.a.c.e3.b0.j.c
        public void h() {
            e eVar = this.e;
            eVar.a.delete();
            eVar.f963b.delete();
        }

        public final int i(i iVar, int i) {
            int hashCode = iVar.f926b.hashCode() + (iVar.a * 31);
            if (i >= 2) {
                return (hashCode * 31) + iVar.e.hashCode();
            }
            long a = k.a(iVar.e);
            return (hashCode * 31) + ((int) (a ^ (a >>> 32)));
        }

        public final i j(int i, DataInputStream dataInputStream) throws IOException {
            n nVar;
            int readInt = dataInputStream.readInt();
            String readUTF = dataInputStream.readUTF();
            if (i < 2) {
                long readLong = dataInputStream.readLong();
                m mVar = new m();
                m.a(mVar, readLong);
                nVar = n.a.a(mVar);
            } else {
                nVar = j.a(dataInputStream);
            }
            return new i(readInt, readUTF, nVar);
        }
    }

    /* compiled from: CachedContentIndex.java */
    /* loaded from: classes3.dex */
    public interface c {
        void a(i iVar, boolean z2);

        void b(HashMap<String, i> hashMap) throws IOException;

        void c(i iVar);

        boolean d() throws IOException;

        void e(HashMap<String, i> hashMap) throws IOException;

        void f(long j);

        void g(HashMap<String, i> hashMap, SparseArray<String> sparseArray) throws IOException;

        void h() throws IOException;
    }

    public j(@Nullable b.i.a.c.u2.a aVar, @Nullable File file, @Nullable byte[] bArr, boolean z2, boolean z3) {
        b.c.a.a0.d.D((aVar == null && file == null) ? false : true);
        this.a = new HashMap<>();
        this.f928b = new SparseArray<>();
        this.c = new SparseBooleanArray();
        this.d = new SparseBooleanArray();
        b bVar = null;
        a aVar2 = aVar != null ? new a(aVar) : null;
        bVar = file != null ? new b(new File(file, "cached_content_index.exi"), bArr, z2) : bVar;
        if (aVar2 == null || (bVar != null && z3)) {
            int i = e0.a;
            this.e = bVar;
            this.f = aVar2;
            return;
        }
        this.e = aVar2;
        this.f = bVar;
    }

    public static n a(DataInputStream dataInputStream) throws IOException {
        int readInt = dataInputStream.readInt();
        HashMap hashMap = new HashMap();
        for (int i = 0; i < readInt; i++) {
            String readUTF = dataInputStream.readUTF();
            int readInt2 = dataInputStream.readInt();
            if (readInt2 >= 0) {
                int min = Math.min(readInt2, 10485760);
                byte[] bArr = e0.f;
                int i2 = 0;
                while (i2 != readInt2) {
                    int i3 = i2 + min;
                    bArr = Arrays.copyOf(bArr, i3);
                    dataInputStream.readFully(bArr, i2, min);
                    min = Math.min(readInt2 - i3, 10485760);
                    i2 = i3;
                }
                hashMap.put(readUTF, bArr);
            } else {
                throw new IOException(b.d.b.a.a.f(31, "Invalid value size: ", readInt2));
            }
        }
        return new n(hashMap);
    }

    public static void b(n nVar, DataOutputStream dataOutputStream) throws IOException {
        Set<Map.Entry<String, byte[]>> entrySet = nVar.c.entrySet();
        dataOutputStream.writeInt(entrySet.size());
        for (Map.Entry<String, byte[]> entry : entrySet) {
            dataOutputStream.writeUTF(entry.getKey());
            byte[] value = entry.getValue();
            dataOutputStream.writeInt(value.length);
            dataOutputStream.write(value);
        }
    }

    @Nullable
    public i c(String str) {
        return this.a.get(str);
    }

    public i d(String str) {
        i iVar = this.a.get(str);
        if (iVar != null) {
            return iVar;
        }
        SparseArray<String> sparseArray = this.f928b;
        int size = sparseArray.size();
        int i = 0;
        int keyAt = size == 0 ? 0 : sparseArray.keyAt(size - 1) + 1;
        if (keyAt < 0) {
            while (i < size && i == sparseArray.keyAt(i)) {
                i++;
            }
            keyAt = i;
        }
        i iVar2 = new i(keyAt, str, n.a);
        this.a.put(str, iVar2);
        this.f928b.put(keyAt, str);
        this.d.put(keyAt, true);
        this.e.c(iVar2);
        return iVar2;
    }

    @WorkerThread
    public void e(long j) throws IOException {
        c cVar;
        this.e.f(j);
        c cVar2 = this.f;
        if (cVar2 != null) {
            cVar2.f(j);
        }
        if (this.e.d() || (cVar = this.f) == null || !cVar.d()) {
            this.e.g(this.a, this.f928b);
        } else {
            this.f.g(this.a, this.f928b);
            this.e.b(this.a);
        }
        c cVar3 = this.f;
        if (cVar3 != null) {
            cVar3.h();
            this.f = null;
        }
    }

    public void f(String str) {
        i iVar = this.a.get(str);
        if (iVar != null && iVar.c.isEmpty() && iVar.d.isEmpty()) {
            this.a.remove(str);
            int i = iVar.a;
            boolean z2 = this.d.get(i);
            this.e.a(iVar, z2);
            if (z2) {
                this.f928b.remove(i);
                this.d.delete(i);
                return;
            }
            this.f928b.put(i, null);
            this.c.put(i, true);
        }
    }

    @WorkerThread
    public void g() throws IOException {
        this.e.e(this.a);
        int size = this.c.size();
        for (int i = 0; i < size; i++) {
            this.f928b.remove(this.c.keyAt(i));
        }
        this.c.clear();
        this.d.clear();
    }
}
