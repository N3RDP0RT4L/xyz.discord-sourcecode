package b.i.a.c.e3.b0;

import android.net.Uri;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import b.c.a.a0.d;
import b.i.a.c.e3.a0;
import b.i.a.c.e3.j;
import b.i.a.c.e3.l;
import b.i.a.c.e3.n;
import b.i.a.c.e3.z;
import b.i.a.c.f3.e0;
import com.google.android.exoplayer2.upstream.DataSourceException;
import com.google.android.exoplayer2.upstream.cache.Cache;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
/* compiled from: CacheDataSource.java */
/* loaded from: classes3.dex */
public final class c implements l {
    public final Cache a;

    /* renamed from: b  reason: collision with root package name */
    public final l f922b;
    @Nullable
    public final l c;
    public final l d;
    public final g e = b.i.a.c.e3.b0.a.f921b;
    @Nullable
    public final a f;
    public final boolean g;
    public final boolean h;
    public final boolean i;
    @Nullable
    public Uri j;
    @Nullable
    public n k;
    @Nullable
    public n l;
    @Nullable
    public l m;
    public long n;
    public long o;
    public long p;
    @Nullable
    public h q;
    public boolean r;

    /* renamed from: s  reason: collision with root package name */
    public boolean f923s;
    public long t;
    public long u;

    /* compiled from: CacheDataSource.java */
    /* loaded from: classes3.dex */
    public interface a {
        void a(int i);

        void b(long j, long j2);
    }

    public c(Cache cache, @Nullable l lVar, l lVar2, @Nullable j jVar, int i, @Nullable a aVar) {
        this.a = cache;
        this.f922b = lVar2;
        int i2 = g.a;
        boolean z2 = false;
        this.g = (i & 1) != 0;
        this.h = (i & 2) != 0;
        this.i = (i & 4) != 0 ? true : z2;
        this.d = lVar;
        this.c = new z(lVar, jVar);
        this.f = null;
    }

    @Override // b.i.a.c.e3.l
    public long a(n nVar) throws IOException {
        int i;
        a aVar;
        try {
            Objects.requireNonNull((b.i.a.c.e3.b0.a) this.e);
            String str = nVar.h;
            if (str == null) {
                str = nVar.a.toString();
            }
            n.b a2 = nVar.a();
            a2.h = str;
            n a3 = a2.a();
            this.k = a3;
            Cache cache = this.a;
            Uri uri = a3.a;
            byte[] bArr = ((n) cache.b(str)).c.get("exo_redir");
            Uri uri2 = null;
            String str2 = bArr != null ? new String(bArr, b.i.b.a.c.c) : null;
            if (str2 != null) {
                uri2 = Uri.parse(str2);
            }
            if (uri2 != null) {
                uri = uri2;
            }
            this.j = uri;
            this.o = nVar.f;
            boolean z2 = true;
            if (!this.h || !this.r) {
                i = (!this.i || nVar.g != -1) ? -1 : 1;
            } else {
                i = 0;
            }
            if (i == -1) {
                z2 = false;
            }
            this.f923s = z2;
            if (z2 && (aVar = this.f) != null) {
                aVar.a(i);
            }
            if (this.f923s) {
                this.p = -1L;
            } else {
                long a4 = k.a(this.a.b(str));
                this.p = a4;
                if (a4 != -1) {
                    long j = a4 - nVar.f;
                    this.p = j;
                    if (j < 0) {
                        throw new DataSourceException(2008);
                    }
                }
            }
            long j2 = nVar.g;
            if (j2 != -1) {
                long j3 = this.p;
                if (j3 != -1) {
                    j2 = Math.min(j3, j2);
                }
                this.p = j2;
            }
            long j4 = this.p;
            if (j4 > 0 || j4 == -1) {
                t(a3, false);
            }
            long j5 = nVar.g;
            return j5 != -1 ? j5 : this.p;
        } catch (Throwable th) {
            q(th);
            throw th;
        }
    }

    @Override // b.i.a.c.e3.l
    public void close() throws IOException {
        this.k = null;
        this.j = null;
        this.o = 0L;
        a aVar = this.f;
        if (aVar != null && this.t > 0) {
            aVar.b(this.a.h(), this.t);
            this.t = 0L;
        }
        try {
            p();
        } catch (Throwable th) {
            q(th);
            throw th;
        }
    }

    @Override // b.i.a.c.e3.l
    public void d(a0 a0Var) {
        Objects.requireNonNull(a0Var);
        this.f922b.d(a0Var);
        this.d.d(a0Var);
    }

    @Override // b.i.a.c.e3.l
    public Map<String, List<String>> j() {
        if (s()) {
            return this.d.j();
        }
        return Collections.emptyMap();
    }

    @Override // b.i.a.c.e3.l
    @Nullable
    public Uri n() {
        return this.j;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [b.i.a.c.e3.b0.h, b.i.a.c.e3.l, b.i.a.c.e3.n] */
    public final void p() throws IOException {
        l lVar = this.m;
        if (lVar != null) {
            try {
                lVar.close();
            } finally {
                this.l = null;
                this.m = null;
                h hVar = this.q;
                if (hVar != null) {
                    this.a.i(hVar);
                    this.q = null;
                }
            }
        }
    }

    public final void q(Throwable th) {
        if (r() || (th instanceof Cache.CacheException)) {
            this.r = true;
        }
    }

    public final boolean r() {
        return this.m == this.f922b;
    }

    @Override // b.i.a.c.e3.h
    public int read(byte[] bArr, int i, int i2) throws IOException {
        boolean z2 = false;
        if (i2 == 0) {
            return 0;
        }
        if (this.p == 0) {
            return -1;
        }
        n nVar = this.k;
        Objects.requireNonNull(nVar);
        n nVar2 = this.l;
        Objects.requireNonNull(nVar2);
        try {
            if (this.o >= this.u) {
                t(nVar, true);
            }
            l lVar = this.m;
            Objects.requireNonNull(lVar);
            int read = lVar.read(bArr, i, i2);
            if (read != -1) {
                if (r()) {
                    this.t += read;
                }
                long j = read;
                this.o += j;
                this.n += j;
                long j2 = this.p;
                if (j2 != -1) {
                    this.p = j2 - j;
                }
            } else {
                if (s()) {
                    long j3 = nVar2.g;
                    if (j3 == -1 || this.n < j3) {
                        String str = nVar.h;
                        int i3 = e0.a;
                        this.p = 0L;
                        if (this.m == this.c) {
                            z2 = true;
                        }
                        if (z2) {
                            m mVar = new m();
                            m.a(mVar, this.o);
                            this.a.c(str, mVar);
                        }
                    }
                }
                long j4 = this.p;
                if (j4 <= 0) {
                    if (j4 == -1) {
                    }
                }
                p();
                t(nVar, false);
                return read(bArr, i, i2);
            }
            return read;
        } catch (Throwable th) {
            q(th);
            throw th;
        }
    }

    public final boolean s() {
        return !r();
    }

    public final void t(n nVar, boolean z2) throws IOException {
        h hVar;
        n nVar2;
        l lVar;
        Uri n;
        String str = nVar.h;
        int i = e0.a;
        Uri uri = null;
        if (this.f923s) {
            hVar = null;
        } else if (this.g) {
            try {
                hVar = this.a.f(str, this.o, this.p);
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
                throw new InterruptedIOException();
            }
        } else {
            hVar = this.a.e(str, this.o, this.p);
        }
        if (hVar == null) {
            lVar = this.d;
            n.b a2 = nVar.a();
            a2.f = this.o;
            a2.g = this.p;
            nVar2 = a2.a();
        } else if (hVar.m) {
            Uri fromFile = Uri.fromFile(hVar.n);
            long j = hVar.k;
            long j2 = this.o - j;
            long j3 = hVar.l - j2;
            long j4 = this.p;
            if (j4 != -1) {
                j3 = Math.min(j3, j4);
            }
            n.b a3 = nVar.a();
            a3.a = fromFile;
            a3.f939b = j;
            a3.f = j2;
            a3.g = j3;
            nVar2 = a3.a();
            lVar = this.f922b;
        } else {
            long j5 = hVar.l;
            if (j5 == -1) {
                j5 = this.p;
            } else {
                long j6 = this.p;
                if (j6 != -1) {
                    j5 = Math.min(j5, j6);
                }
            }
            n.b a4 = nVar.a();
            a4.f = this.o;
            a4.g = j5;
            nVar2 = a4.a();
            lVar = this.c;
            if (lVar == null) {
                lVar = this.d;
                this.a.i(hVar);
                hVar = null;
            }
        }
        this.u = (this.f923s || lVar != this.d) ? RecyclerView.FOREVER_NS : this.o + 102400;
        if (z2) {
            d.D(this.m == this.d);
            if (lVar != this.d) {
                try {
                    p();
                } catch (Throwable th) {
                    if (!hVar.m) {
                        this.a.i(hVar);
                    }
                    throw th;
                }
            } else {
                return;
            }
        }
        if (hVar != null && (!hVar.m)) {
            this.q = hVar;
        }
        this.m = lVar;
        this.l = nVar2;
        this.n = 0L;
        long a5 = lVar.a(nVar2);
        m mVar = new m();
        if (nVar2.g == -1 && a5 != -1) {
            this.p = a5;
            m.a(mVar, this.o + a5);
        }
        if (s()) {
            this.j = lVar.n();
            if (!nVar.a.equals(n)) {
                uri = this.j;
            }
            if (uri == null) {
                mVar.f931b.add("exo_redir");
                mVar.a.remove("exo_redir");
            } else {
                String uri2 = uri.toString();
                Map<String, Object> map = mVar.a;
                Objects.requireNonNull(uri2);
                map.put("exo_redir", uri2);
                mVar.f931b.remove("exo_redir");
            }
        }
        if (this.m == this.c) {
            this.a.c(str, mVar);
        }
    }
}
