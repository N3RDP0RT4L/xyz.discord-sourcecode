package b.i.a.c.e3.b0;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
/* compiled from: ContentMetadataMutations.java */
/* loaded from: classes3.dex */
public class m {
    public final Map<String, Object> a = new HashMap();

    /* renamed from: b  reason: collision with root package name */
    public final List<String> f931b = new ArrayList();

    public static m a(m mVar, long j) {
        Long valueOf = Long.valueOf(j);
        Map<String, Object> map = mVar.a;
        Objects.requireNonNull(valueOf);
        map.put("exo_len", valueOf);
        mVar.f931b.remove("exo_len");
        return mVar;
    }
}
