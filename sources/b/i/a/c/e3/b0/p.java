package b.i.a.c.e3.b0;

import b.c.a.a0.d;
import b.i.a.c.f3.e0;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
/* compiled from: ReusableBufferedOutputStream.java */
/* loaded from: classes3.dex */
public final class p extends BufferedOutputStream {
    public boolean j;

    public p(OutputStream outputStream) {
        super(outputStream);
    }

    public void a(OutputStream outputStream) {
        d.D(this.j);
        ((BufferedOutputStream) this).out = outputStream;
        ((BufferedOutputStream) this).count = 0;
        this.j = false;
    }

    @Override // java.io.FilterOutputStream, java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        Throwable th;
        this.j = true;
        try {
            flush();
            th = null;
        } catch (Throwable th2) {
            th = th2;
        }
        try {
            ((BufferedOutputStream) this).out.close();
        } catch (Throwable th3) {
            th = th3;
            if (th == null) {
            }
        }
        if (th != null) {
            int i = e0.a;
            throw th;
        }
    }

    public p(OutputStream outputStream, int i) {
        super(outputStream, i);
    }
}
