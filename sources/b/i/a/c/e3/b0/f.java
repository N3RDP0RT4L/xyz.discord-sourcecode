package b.i.a.c.e3.b0;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.WorkerThread;
import b.i.a.c.u2.a;
import b.i.a.c.u2.d;
import com.discord.models.domain.ModelAuditLogEntry;
import com.google.android.exoplayer2.database.DatabaseIOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
/* compiled from: CacheFileMetadataIndex.java */
/* loaded from: classes3.dex */
public final class f {
    public static final String[] a = {ModelAuditLogEntry.CHANGE_KEY_NAME, "length", "last_touch_timestamp"};

    /* renamed from: b  reason: collision with root package name */
    public final a f925b;
    public String c;

    public f(a aVar) {
        this.f925b = aVar;
    }

    @WorkerThread
    public Map<String, e> a() throws DatabaseIOException {
        try {
            Objects.requireNonNull(this.c);
            Cursor query = this.f925b.getReadableDatabase().query(this.c, a, null, null, null, null, null);
            HashMap hashMap = new HashMap(query.getCount());
            while (query.moveToNext()) {
                String string = query.getString(0);
                Objects.requireNonNull(string);
                hashMap.put(string, new e(query.getLong(1), query.getLong(2)));
            }
            query.close();
            return hashMap;
        } catch (SQLException e) {
            throw new DatabaseIOException(e);
        }
    }

    @WorkerThread
    public void b(long j) throws DatabaseIOException {
        try {
            String hexString = Long.toHexString(j);
            String valueOf = String.valueOf(hexString);
            this.c = valueOf.length() != 0 ? "ExoPlayerCacheFileMetadata".concat(valueOf) : new String("ExoPlayerCacheFileMetadata");
            if (d.a(this.f925b.getReadableDatabase(), 2, hexString) != 1) {
                SQLiteDatabase writableDatabase = this.f925b.getWritableDatabase();
                writableDatabase.beginTransactionNonExclusive();
                d.b(writableDatabase, 2, hexString, 1);
                String valueOf2 = String.valueOf(this.c);
                writableDatabase.execSQL(valueOf2.length() != 0 ? "DROP TABLE IF EXISTS ".concat(valueOf2) : new String("DROP TABLE IF EXISTS "));
                String str = this.c;
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 108);
                sb.append("CREATE TABLE ");
                sb.append(str);
                sb.append(" ");
                sb.append("(name TEXT PRIMARY KEY NOT NULL,length INTEGER NOT NULL,last_touch_timestamp INTEGER NOT NULL)");
                writableDatabase.execSQL(sb.toString());
                writableDatabase.setTransactionSuccessful();
                writableDatabase.endTransaction();
            }
        } catch (SQLException e) {
            throw new DatabaseIOException(e);
        }
    }

    @WorkerThread
    public void c(Set<String> set) throws DatabaseIOException {
        Objects.requireNonNull(this.c);
        try {
            SQLiteDatabase writableDatabase = this.f925b.getWritableDatabase();
            writableDatabase.beginTransactionNonExclusive();
            Iterator<String> it = set.iterator();
            while (it.hasNext()) {
                writableDatabase.delete(this.c, "name = ?", new String[]{it.next()});
            }
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
        } catch (SQLException e) {
            throw new DatabaseIOException(e);
        }
    }

    @WorkerThread
    public void d(String str, long j, long j2) throws DatabaseIOException {
        Objects.requireNonNull(this.c);
        try {
            SQLiteDatabase writableDatabase = this.f925b.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(ModelAuditLogEntry.CHANGE_KEY_NAME, str);
            contentValues.put("length", Long.valueOf(j));
            contentValues.put("last_touch_timestamp", Long.valueOf(j2));
            writableDatabase.replaceOrThrow(this.c, null, contentValues);
        } catch (SQLException e) {
            throw new DatabaseIOException(e);
        }
    }
}
