package b.i.a.c.e3.b0;

import androidx.annotation.Nullable;
import b.i.b.a.c;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
/* compiled from: DefaultContentMetadata.java */
/* loaded from: classes3.dex */
public final class n implements l {
    public static final n a = new n(Collections.emptyMap());

    /* renamed from: b  reason: collision with root package name */
    public int f932b;
    public final Map<String, byte[]> c;

    public n() {
        this(Collections.emptyMap());
    }

    public static boolean b(Map<String, byte[]> map, Map<String, byte[]> map2) {
        if (map.size() != map2.size()) {
            return false;
        }
        for (Map.Entry<String, byte[]> entry : map.entrySet()) {
            if (!Arrays.equals(entry.getValue(), map2.get(entry.getKey()))) {
                return false;
            }
        }
        return true;
    }

    public n a(m mVar) {
        byte[] bArr;
        HashMap hashMap = new HashMap(this.c);
        Objects.requireNonNull(mVar);
        List unmodifiableList = Collections.unmodifiableList(new ArrayList(mVar.f931b));
        for (int i = 0; i < unmodifiableList.size(); i++) {
            hashMap.remove(unmodifiableList.get(i));
        }
        HashMap hashMap2 = new HashMap(mVar.a);
        for (Map.Entry entry : hashMap2.entrySet()) {
            Object value = entry.getValue();
            if (value instanceof byte[]) {
                byte[] bArr2 = (byte[]) value;
                entry.setValue(Arrays.copyOf(bArr2, bArr2.length));
            }
        }
        for (Map.Entry entry2 : Collections.unmodifiableMap(hashMap2).entrySet()) {
            String str = (String) entry2.getKey();
            Object value2 = entry2.getValue();
            if (value2 instanceof Long) {
                bArr = ByteBuffer.allocate(8).putLong(((Long) value2).longValue()).array();
            } else if (value2 instanceof String) {
                bArr = ((String) value2).getBytes(c.c);
            } else if (value2 instanceof byte[]) {
                bArr = (byte[]) value2;
            } else {
                throw new IllegalArgumentException();
            }
            hashMap.put(str, bArr);
        }
        return b(this.c, hashMap) ? this : new n(hashMap);
    }

    public boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || n.class != obj.getClass()) {
            return false;
        }
        return b(this.c, ((n) obj).c);
    }

    public int hashCode() {
        if (this.f932b == 0) {
            int i = 0;
            for (Map.Entry<String, byte[]> entry : this.c.entrySet()) {
                i += Arrays.hashCode(entry.getValue()) ^ entry.getKey().hashCode();
            }
            this.f932b = i;
        }
        return this.f932b;
    }

    public n(Map<String, byte[]> map) {
        this.c = Collections.unmodifiableMap(map);
    }
}
