package b.i.a.c.e3.b0;

import android.database.SQLException;
import android.os.ConditionVariable;
import android.util.Log;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.e3.b0.i;
import b.i.a.c.f3.q;
import b.i.a.c.u2.a;
import com.google.android.exoplayer2.database.DatabaseIOException;
import com.google.android.exoplayer2.upstream.cache.Cache;
import java.io.File;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
/* compiled from: SimpleCache.java */
/* loaded from: classes3.dex */
public final class r implements Cache {
    public static final HashSet<File> a = new HashSet<>();

    /* renamed from: b  reason: collision with root package name */
    public final File f934b;
    public final d c;
    public final j d;
    @Nullable
    public final f e;
    public final HashMap<String, ArrayList<Cache.a>> f;
    public final Random g;
    public final boolean h;
    public long i;
    public long j;
    public Cache.CacheException k;

    public r(File file, d dVar, a aVar) {
        boolean add;
        j jVar = new j(aVar, file, null, false, false);
        f fVar = new f(aVar);
        synchronized (r.class) {
            add = a.add(file.getAbsoluteFile());
        }
        if (add) {
            this.f934b = file;
            this.c = dVar;
            this.d = jVar;
            this.e = fVar;
            this.f = new HashMap<>();
            this.g = new Random();
            this.h = true;
            this.i = -1L;
            ConditionVariable conditionVariable = new ConditionVariable();
            new q(this, "ExoPlayer:SimpleCacheInit", conditionVariable).start();
            conditionVariable.block();
            return;
        }
        String valueOf = String.valueOf(file);
        throw new IllegalStateException(b.d.b.a.a.i(valueOf.length() + 46, "Another SimpleCache instance uses the folder: ", valueOf));
    }

    public static void j(r rVar) {
        long j;
        if (!rVar.f934b.exists()) {
            try {
                m(rVar.f934b);
            } catch (Cache.CacheException e) {
                rVar.k = e;
                return;
            }
        }
        File[] listFiles = rVar.f934b.listFiles();
        if (listFiles == null) {
            String valueOf = String.valueOf(rVar.f934b);
            StringBuilder sb = new StringBuilder(valueOf.length() + 38);
            sb.append("Failed to list cache directory files: ");
            sb.append(valueOf);
            String sb2 = sb.toString();
            Log.e("SimpleCache", sb2);
            rVar.k = new Cache.CacheException(sb2);
            return;
        }
        int length = listFiles.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                j = -1;
                break;
            }
            File file = listFiles[i];
            String name = file.getName();
            if (name.endsWith(".uid")) {
                try {
                    j = Long.parseLong(name.substring(0, name.indexOf(46)), 16);
                    break;
                } catch (NumberFormatException unused) {
                    String valueOf2 = String.valueOf(file);
                    StringBuilder sb3 = new StringBuilder(valueOf2.length() + 20);
                    sb3.append("Malformed UID file: ");
                    sb3.append(valueOf2);
                    Log.e("SimpleCache", sb3.toString());
                    file.delete();
                }
            }
            i++;
        }
        rVar.i = j;
        if (j == -1) {
            try {
                rVar.i = n(rVar.f934b);
            } catch (IOException e2) {
                String valueOf3 = String.valueOf(rVar.f934b);
                StringBuilder sb4 = new StringBuilder(valueOf3.length() + 28);
                sb4.append("Failed to create cache UID: ");
                sb4.append(valueOf3);
                String sb5 = sb4.toString();
                q.b("SimpleCache", sb5, e2);
                rVar.k = new Cache.CacheException(sb5, e2);
                return;
            }
        }
        try {
            rVar.d.e(rVar.i);
            f fVar = rVar.e;
            if (fVar != null) {
                fVar.b(rVar.i);
                Map<String, e> a2 = rVar.e.a();
                rVar.p(rVar.f934b, true, listFiles, a2);
                rVar.e.c(((HashMap) a2).keySet());
            } else {
                rVar.p(rVar.f934b, true, listFiles, null);
            }
            j jVar = rVar.d;
            Iterator j2 = b.i.b.b.r.m(jVar.a.keySet()).iterator();
            while (j2.hasNext()) {
                jVar.f((String) j2.next());
            }
            try {
                rVar.d.g();
            } catch (IOException e3) {
                q.b("SimpleCache", "Storing index file failed", e3);
            }
        } catch (IOException e4) {
            String valueOf4 = String.valueOf(rVar.f934b);
            StringBuilder sb6 = new StringBuilder(valueOf4.length() + 36);
            sb6.append("Failed to initialize cache indices: ");
            sb6.append(valueOf4);
            String sb7 = sb6.toString();
            q.b("SimpleCache", sb7, e4);
            rVar.k = new Cache.CacheException(sb7, e4);
        }
    }

    public static void m(File file) throws Cache.CacheException {
        if (!file.mkdirs() && !file.isDirectory()) {
            String valueOf = String.valueOf(file);
            StringBuilder sb = new StringBuilder(valueOf.length() + 34);
            sb.append("Failed to create cache directory: ");
            sb.append(valueOf);
            String sb2 = sb.toString();
            Log.e("SimpleCache", sb2);
            throw new Cache.CacheException(sb2);
        }
    }

    public static long n(File file) throws IOException {
        long nextLong = new SecureRandom().nextLong();
        long abs = nextLong == Long.MIN_VALUE ? 0L : Math.abs(nextLong);
        String valueOf = String.valueOf(Long.toString(abs, 16));
        File file2 = new File(file, ".uid".length() != 0 ? valueOf.concat(".uid") : new String(valueOf));
        if (file2.createNewFile()) {
            return abs;
        }
        String valueOf2 = String.valueOf(file2);
        throw new IOException(b.d.b.a.a.i(valueOf2.length() + 27, "Failed to create UID file: ", valueOf2));
    }

    @Override // com.google.android.exoplayer2.upstream.cache.Cache
    public synchronized File a(String str, long j, long j2) throws Cache.CacheException {
        i iVar;
        File file;
        d.D(true);
        l();
        iVar = this.d.a.get(str);
        Objects.requireNonNull(iVar);
        d.D(iVar.a(j, j2));
        if (!this.f934b.exists()) {
            m(this.f934b);
            r();
        }
        this.c.a(this, str, j, j2);
        file = new File(this.f934b, Integer.toString(this.g.nextInt(10)));
        if (!file.exists()) {
            m(file);
        }
        return s.h(file, iVar.a, j, System.currentTimeMillis());
    }

    @Override // com.google.android.exoplayer2.upstream.cache.Cache
    public synchronized l b(String str) {
        n nVar;
        d.D(true);
        i iVar = this.d.a.get(str);
        if (iVar != null) {
            nVar = iVar.e;
        } else {
            nVar = n.a;
        }
        return nVar;
    }

    @Override // com.google.android.exoplayer2.upstream.cache.Cache
    public synchronized void c(String str, m mVar) throws Cache.CacheException {
        d.D(true);
        l();
        j jVar = this.d;
        i d = jVar.d(str);
        n nVar = d.e;
        n a2 = nVar.a(mVar);
        d.e = a2;
        if (!a2.equals(nVar)) {
            jVar.e.c(d);
        }
        try {
            this.d.g();
        } catch (IOException e) {
            throw new Cache.CacheException(e);
        }
    }

    @Override // com.google.android.exoplayer2.upstream.cache.Cache
    public synchronized void d(h hVar) {
        d.D(true);
        q(hVar);
    }

    @Override // com.google.android.exoplayer2.upstream.cache.Cache
    @Nullable
    public synchronized h e(String str, long j, long j2) throws Cache.CacheException {
        s sVar;
        boolean z2;
        boolean z3;
        d.D(true);
        l();
        s o = o(str, j, j2);
        if (o.m) {
            return s(str, o);
        }
        i d = this.d.d(str);
        long j3 = o.l;
        int i = 0;
        while (true) {
            if (i >= d.d.size()) {
                sVar = o;
                d.d.add(new i.a(j, j3));
                z2 = true;
                break;
            }
            i.a aVar = d.d.get(i);
            long j4 = aVar.a;
            if (j4 <= j) {
                sVar = o;
                long j5 = aVar.f927b;
                if (j5 != -1) {
                    if (j4 + j5 > j) {
                    }
                    z3 = false;
                }
                z3 = true;
            } else {
                sVar = o;
                if (j3 != -1) {
                    if (j + j3 > j4) {
                    }
                    z3 = false;
                }
                z3 = true;
            }
            if (z3) {
                z2 = false;
                break;
            }
            i++;
            o = sVar;
        }
        if (z2) {
            return sVar;
        }
        return null;
    }

    @Override // com.google.android.exoplayer2.upstream.cache.Cache
    public synchronized h f(String str, long j, long j2) throws InterruptedException, Cache.CacheException {
        h e;
        d.D(true);
        l();
        while (true) {
            e = e(str, j, j2);
            if (e == null) {
                wait();
            }
        }
        return e;
    }

    @Override // com.google.android.exoplayer2.upstream.cache.Cache
    public synchronized void g(File file, long j) throws Cache.CacheException {
        boolean z2 = true;
        d.D(true);
        if (file.exists()) {
            if (j == 0) {
                file.delete();
                return;
            }
            s g = s.g(file, j, -9223372036854775807L, this.d);
            Objects.requireNonNull(g);
            i c = this.d.c(g.j);
            Objects.requireNonNull(c);
            d.D(c.a(g.k, g.l));
            long a2 = k.a(c.e);
            if (a2 != -1) {
                if (g.k + g.l > a2) {
                    z2 = false;
                }
                d.D(z2);
            }
            if (this.e != null) {
                try {
                    this.e.d(file.getName(), g.l, g.o);
                } catch (IOException e) {
                    throw new Cache.CacheException(e);
                }
            }
            k(g);
            try {
                this.d.g();
                notifyAll();
            } catch (IOException e2) {
                throw new Cache.CacheException(e2);
            }
        }
    }

    @Override // com.google.android.exoplayer2.upstream.cache.Cache
    public synchronized long h() {
        d.D(true);
        return this.j;
    }

    @Override // com.google.android.exoplayer2.upstream.cache.Cache
    public synchronized void i(h hVar) {
        d.D(true);
        i c = this.d.c(hVar.j);
        Objects.requireNonNull(c);
        long j = hVar.k;
        for (int i = 0; i < c.d.size(); i++) {
            if (c.d.get(i).a == j) {
                c.d.remove(i);
                this.d.f(c.f926b);
                notifyAll();
            }
        }
        throw new IllegalStateException();
    }

    public final void k(s sVar) {
        this.d.d(sVar.j).c.add(sVar);
        this.j += sVar.l;
        ArrayList<Cache.a> arrayList = this.f.get(sVar.j);
        if (arrayList != null) {
            int size = arrayList.size();
            while (true) {
                size--;
                if (size < 0) {
                    break;
                }
                arrayList.get(size).d(this, sVar);
            }
        }
        this.c.d(this, sVar);
    }

    public synchronized void l() throws Cache.CacheException {
        Cache.CacheException cacheException = this.k;
        if (cacheException != null) {
            throw cacheException;
        }
    }

    public final s o(String str, long j, long j2) {
        s floor;
        long j3;
        i iVar = this.d.a.get(str);
        if (iVar == null) {
            return new s(str, j, j2, -9223372036854775807L, null);
        }
        while (true) {
            s sVar = new s(iVar.f926b, j, -1L, -9223372036854775807L, null);
            floor = iVar.c.floor(sVar);
            if (floor == null || floor.k + floor.l <= j) {
                s ceiling = iVar.c.ceiling(sVar);
                if (ceiling != null) {
                    long j4 = ceiling.k - j;
                    if (j2 != -1) {
                        j4 = Math.min(j4, j2);
                    }
                    j3 = j4;
                } else {
                    j3 = j2;
                }
                floor = new s(iVar.f926b, j, j3, -9223372036854775807L, null);
            }
            if (!floor.m || floor.n.length() == floor.l) {
                break;
            }
            r();
        }
        return floor;
    }

    public final void p(File file, boolean z2, @Nullable File[] fileArr, @Nullable Map<String, e> map) {
        if (fileArr != null && fileArr.length != 0) {
            for (File file2 : fileArr) {
                String name = file2.getName();
                if (z2 && name.indexOf(46) == -1) {
                    p(file2, false, file2.listFiles(), map);
                } else if (!z2 || (!name.startsWith("cached_content_index.exi") && !name.endsWith(".uid"))) {
                    long j = -1;
                    long j2 = -9223372036854775807L;
                    e remove = map != null ? map.remove(name) : null;
                    if (remove != null) {
                        j = remove.a;
                        j2 = remove.f924b;
                    }
                    s g = s.g(file2, j, j2, this.d);
                    if (g != null) {
                        k(g);
                    } else {
                        file2.delete();
                    }
                }
            }
        } else if (!z2) {
            file.delete();
        }
    }

    public final void q(h hVar) {
        boolean z2;
        i c = this.d.c(hVar.j);
        if (c != null) {
            if (c.c.remove(hVar)) {
                File file = hVar.n;
                if (file != null) {
                    file.delete();
                }
                z2 = true;
            } else {
                z2 = false;
            }
            if (z2) {
                this.j -= hVar.l;
                if (this.e != null) {
                    String name = hVar.n.getName();
                    try {
                        f fVar = this.e;
                        Objects.requireNonNull(fVar.c);
                        try {
                            fVar.f925b.getWritableDatabase().delete(fVar.c, "name = ?", new String[]{name});
                        } catch (SQLException e) {
                            throw new DatabaseIOException(e);
                        }
                    } catch (IOException unused) {
                        String valueOf = String.valueOf(name);
                        Log.w("SimpleCache", valueOf.length() != 0 ? "Failed to remove file index entry for: ".concat(valueOf) : new String("Failed to remove file index entry for: "));
                    }
                }
                this.d.f(c.f926b);
                ArrayList<Cache.a> arrayList = this.f.get(hVar.j);
                if (arrayList != null) {
                    int size = arrayList.size();
                    while (true) {
                        size--;
                        if (size < 0) {
                            break;
                        }
                        arrayList.get(size).b(this, hVar);
                    }
                }
                this.c.b(this, hVar);
            }
        }
    }

    public final void r() {
        ArrayList arrayList = new ArrayList();
        for (i iVar : Collections.unmodifiableCollection(this.d.a.values())) {
            Iterator<s> it = iVar.c.iterator();
            while (it.hasNext()) {
                s next = it.next();
                if (next.n.length() != next.l) {
                    arrayList.add(next);
                }
            }
        }
        for (int i = 0; i < arrayList.size(); i++) {
            q((h) arrayList.get(i));
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:20:0x00a2  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final b.i.a.c.e3.b0.s s(java.lang.String r17, b.i.a.c.e3.b0.s r18) {
        /*
            r16 = this;
            r0 = r16
            r1 = r18
            boolean r2 = r0.h
            if (r2 != 0) goto L9
            return r1
        L9:
            java.io.File r2 = r1.n
            java.util.Objects.requireNonNull(r2)
            java.lang.String r4 = r2.getName()
            long r5 = r1.l
            long r13 = java.lang.System.currentTimeMillis()
            r2 = 0
            b.i.a.c.e3.b0.f r3 = r0.e
            if (r3 == 0) goto L2a
            r7 = r13
            r3.d(r4, r5, r7)     // Catch: java.io.IOException -> L22
            goto L2b
        L22:
            java.lang.String r3 = "SimpleCache"
            java.lang.String r4 = "Failed to update index with new touch timestamp."
            android.util.Log.w(r3, r4)
            goto L2b
        L2a:
            r2 = 1
        L2b:
            b.i.a.c.e3.b0.j r3 = r0.d
            java.util.HashMap<java.lang.String, b.i.a.c.e3.b0.i> r3 = r3.a
            r4 = r17
            java.lang.Object r3 = r3.get(r4)
            b.i.a.c.e3.b0.i r3 = (b.i.a.c.e3.b0.i) r3
            java.util.TreeSet<b.i.a.c.e3.b0.s> r4 = r3.c
            boolean r4 = r4.remove(r1)
            b.c.a.a0.d.D(r4)
            java.io.File r4 = r1.n
            java.util.Objects.requireNonNull(r4)
            if (r2 == 0) goto L7f
            java.io.File r7 = r4.getParentFile()
            java.util.Objects.requireNonNull(r7)
            long r9 = r1.k
            int r8 = r3.a
            r11 = r13
            java.io.File r2 = b.i.a.c.e3.b0.s.h(r7, r8, r9, r11)
            boolean r5 = r4.renameTo(r2)
            if (r5 == 0) goto L5f
            r15 = r2
            goto L80
        L5f:
            java.lang.String r5 = java.lang.String.valueOf(r4)
            java.lang.String r2 = java.lang.String.valueOf(r2)
            int r6 = r5.length()
            int r6 = r6 + 21
            int r7 = r2.length()
            int r7 = r7 + r6
            java.lang.String r6 = "Failed to rename "
            java.lang.String r8 = " to "
            java.lang.String r2 = b.d.b.a.a.k(r7, r6, r5, r8, r2)
            java.lang.String r5 = "CachedContent"
            android.util.Log.w(r5, r2)
        L7f:
            r15 = r4
        L80:
            boolean r2 = r1.m
            b.c.a.a0.d.D(r2)
            b.i.a.c.e3.b0.s r2 = new b.i.a.c.e3.b0.s
            java.lang.String r8 = r1.j
            long r9 = r1.k
            long r11 = r1.l
            r7 = r2
            r7.<init>(r8, r9, r11, r13, r15)
            java.util.TreeSet<b.i.a.c.e3.b0.s> r3 = r3.c
            r3.add(r2)
            java.util.HashMap<java.lang.String, java.util.ArrayList<com.google.android.exoplayer2.upstream.cache.Cache$a>> r3 = r0.f
            java.lang.String r4 = r1.j
            java.lang.Object r3 = r3.get(r4)
            java.util.ArrayList r3 = (java.util.ArrayList) r3
            if (r3 == 0) goto Lb4
            int r4 = r3.size()
        La6:
            int r4 = r4 + (-1)
            if (r4 < 0) goto Lb4
            java.lang.Object r5 = r3.get(r4)
            com.google.android.exoplayer2.upstream.cache.Cache$a r5 = (com.google.android.exoplayer2.upstream.cache.Cache.a) r5
            r5.c(r0, r1, r2)
            goto La6
        Lb4:
            b.i.a.c.e3.b0.d r3 = r0.c
            r3.c(r0, r1, r2)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.e3.b0.r.s(java.lang.String, b.i.a.c.e3.b0.s):b.i.a.c.e3.b0.s");
    }
}
