package b.i.a.c.e3;

import android.net.Uri;
import androidx.annotation.Nullable;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
/* compiled from: StatsDataSource.java */
/* loaded from: classes3.dex */
public final class y implements l {
    public final l a;

    /* renamed from: b  reason: collision with root package name */
    public long f952b;
    public Uri c = Uri.EMPTY;
    public Map<String, List<String>> d = Collections.emptyMap();

    public y(l lVar) {
        Objects.requireNonNull(lVar);
        this.a = lVar;
    }

    @Override // b.i.a.c.e3.l
    public long a(n nVar) throws IOException {
        this.c = nVar.a;
        this.d = Collections.emptyMap();
        long a = this.a.a(nVar);
        Uri n = n();
        Objects.requireNonNull(n);
        this.c = n;
        this.d = j();
        return a;
    }

    @Override // b.i.a.c.e3.l
    public void close() throws IOException {
        this.a.close();
    }

    @Override // b.i.a.c.e3.l
    public void d(a0 a0Var) {
        Objects.requireNonNull(a0Var);
        this.a.d(a0Var);
    }

    @Override // b.i.a.c.e3.l
    public Map<String, List<String>> j() {
        return this.a.j();
    }

    @Override // b.i.a.c.e3.l
    @Nullable
    public Uri n() {
        return this.a.n();
    }

    @Override // b.i.a.c.e3.h
    public int read(byte[] bArr, int i, int i2) throws IOException {
        int read = this.a.read(bArr, i, i2);
        if (read != -1) {
            this.f952b += read;
        }
        return read;
    }
}
