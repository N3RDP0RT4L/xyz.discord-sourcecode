package b.i.a.c.e3;

import b.c.a.a0.d;
import java.io.IOException;
import java.io.InputStream;
/* compiled from: DataSourceInputStream.java */
/* loaded from: classes3.dex */
public final class m extends InputStream {
    public final l j;
    public final n k;
    public long o;
    public boolean m = false;
    public boolean n = false;
    public final byte[] l = new byte[1];

    public m(l lVar, n nVar) {
        this.j = lVar;
        this.k = nVar;
    }

    @Override // java.io.InputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        if (!this.n) {
            this.j.close();
            this.n = true;
        }
    }

    @Override // java.io.InputStream
    public int read() throws IOException {
        if (read(this.l) == -1) {
            return -1;
        }
        return this.l[0] & 255;
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr) throws IOException {
        return read(bArr, 0, bArr.length);
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) throws IOException {
        d.D(!this.n);
        if (!this.m) {
            this.j.a(this.k);
            this.m = true;
        }
        int read = this.j.read(bArr, i, i2);
        if (read == -1) {
            return -1;
        }
        this.o += read;
        return read;
    }
}
