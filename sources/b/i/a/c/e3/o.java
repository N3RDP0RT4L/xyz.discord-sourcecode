package b.i.a.c.e3;

import b.c.a.a0.d;
import b.i.a.c.f3.e0;
import java.util.Arrays;
/* compiled from: DefaultAllocator.java */
/* loaded from: classes3.dex */
public final class o {
    public final boolean a;

    /* renamed from: b  reason: collision with root package name */
    public final int f940b;
    public final e[] c;
    public int d;
    public int e;
    public int f;
    public e[] g;

    public o(boolean z2, int i) {
        d.j(i > 0);
        d.j(true);
        this.a = z2;
        this.f940b = i;
        this.f = 0;
        this.g = new e[100];
        this.c = new e[1];
    }

    public synchronized void a(e[] eVarArr) {
        int i = this.f;
        int length = eVarArr.length + i;
        e[] eVarArr2 = this.g;
        if (length >= eVarArr2.length) {
            this.g = (e[]) Arrays.copyOf(eVarArr2, Math.max(eVarArr2.length * 2, i + eVarArr.length));
        }
        for (e eVar : eVarArr) {
            e[] eVarArr3 = this.g;
            int i2 = this.f;
            this.f = i2 + 1;
            eVarArr3[i2] = eVar;
        }
        this.e -= eVarArr.length;
        notifyAll();
    }

    public synchronized void b(int i) {
        boolean z2 = i < this.d;
        this.d = i;
        if (z2) {
            c();
        }
    }

    public synchronized void c() {
        int max = Math.max(0, e0.f(this.d, this.f940b) - this.e);
        int i = this.f;
        if (max < i) {
            Arrays.fill(this.g, max, i, (Object) null);
            this.f = max;
        }
    }
}
