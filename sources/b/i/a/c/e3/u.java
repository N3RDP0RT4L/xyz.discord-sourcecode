package b.i.a.c.e3;

import androidx.annotation.Nullable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
/* compiled from: HttpDataSource.java */
/* loaded from: classes3.dex */
public final class u {
    public final Map<String, String> a = new HashMap();
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public Map<String, String> f948b;

    public synchronized Map<String, String> a() {
        if (this.f948b == null) {
            this.f948b = Collections.unmodifiableMap(new HashMap(this.a));
        }
        return this.f948b;
    }
}
