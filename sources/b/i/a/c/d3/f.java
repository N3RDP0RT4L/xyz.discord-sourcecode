package b.i.a.c.d3;

import b.i.a.c.d3.m;
import java.util.Comparator;
/* compiled from: lambda */
/* loaded from: classes3.dex */
public final /* synthetic */ class f implements Comparator {
    public static final /* synthetic */ f j = new f();

    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        m.c cVar = (m.c) obj;
        m.c cVar2 = (m.c) obj2;
        int compare = Integer.compare(cVar2.a, cVar.a);
        if (compare != 0) {
            return compare;
        }
        int compareTo = cVar2.c.compareTo(cVar.c);
        return compareTo != 0 ? compareTo : cVar2.d.compareTo(cVar.d);
    }
}
