package b.i.a.c.d3;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import androidx.annotation.Nullable;
import java.util.Objects;
/* compiled from: SubtitlePainter.java */
/* loaded from: classes3.dex */
public final class n {
    public int A;
    public int B;
    public int C;
    public int D;
    public StaticLayout E;
    public StaticLayout F;
    public int G;
    public int H;
    public int I;
    public Rect J;
    public final float a;

    /* renamed from: b  reason: collision with root package name */
    public final float f914b;
    public final float c;
    public final float d;
    public final float e;
    public final TextPaint f;
    public final Paint g;
    public final Paint h;
    @Nullable
    public CharSequence i;
    @Nullable
    public Layout.Alignment j;
    @Nullable
    public Bitmap k;
    public float l;
    public int m;
    public int n;
    public float o;
    public int p;
    public float q;
    public float r;

    /* renamed from: s  reason: collision with root package name */
    public int f915s;
    public int t;
    public int u;
    public int v;
    public int w;

    /* renamed from: x  reason: collision with root package name */
    public float f916x;

    /* renamed from: y  reason: collision with root package name */
    public float f917y;

    /* renamed from: z  reason: collision with root package name */
    public float f918z;

    public n(Context context) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(null, new int[]{16843287, 16843288}, 0, 0);
        this.e = obtainStyledAttributes.getDimensionPixelSize(0, 0);
        this.d = obtainStyledAttributes.getFloat(1, 1.0f);
        obtainStyledAttributes.recycle();
        float round = Math.round((context.getResources().getDisplayMetrics().densityDpi * 2.0f) / 160.0f);
        this.a = round;
        this.f914b = round;
        this.c = round;
        TextPaint textPaint = new TextPaint();
        this.f = textPaint;
        textPaint.setAntiAlias(true);
        textPaint.setSubpixelText(true);
        Paint paint = new Paint();
        this.g = paint;
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        Paint paint2 = new Paint();
        this.h = paint2;
        paint2.setAntiAlias(true);
        paint2.setFilterBitmap(true);
    }

    public final void a(Canvas canvas, boolean z2) {
        if (z2) {
            StaticLayout staticLayout = this.E;
            StaticLayout staticLayout2 = this.F;
            if (staticLayout != null && staticLayout2 != null) {
                int save = canvas.save();
                canvas.translate(this.G, this.H);
                if (Color.alpha(this.u) > 0) {
                    this.g.setColor(this.u);
                    canvas.drawRect(-this.I, 0.0f, staticLayout.getWidth() + this.I, staticLayout.getHeight(), this.g);
                }
                int i = this.w;
                boolean z3 = true;
                if (i == 1) {
                    this.f.setStrokeJoin(Paint.Join.ROUND);
                    this.f.setStrokeWidth(this.a);
                    this.f.setColor(this.v);
                    this.f.setStyle(Paint.Style.FILL_AND_STROKE);
                    staticLayout2.draw(canvas);
                } else if (i == 2) {
                    TextPaint textPaint = this.f;
                    float f = this.f914b;
                    float f2 = this.c;
                    textPaint.setShadowLayer(f, f2, f2, this.v);
                } else if (i == 3 || i == 4) {
                    if (i != 3) {
                        z3 = false;
                    }
                    int i2 = -1;
                    int i3 = z3 ? -1 : this.v;
                    if (z3) {
                        i2 = this.v;
                    }
                    float f3 = this.f914b / 2.0f;
                    this.f.setColor(this.f915s);
                    this.f.setStyle(Paint.Style.FILL);
                    float f4 = -f3;
                    this.f.setShadowLayer(this.f914b, f4, f4, i3);
                    staticLayout2.draw(canvas);
                    this.f.setShadowLayer(this.f914b, f3, f3, i2);
                }
                this.f.setColor(this.f915s);
                this.f.setStyle(Paint.Style.FILL);
                staticLayout.draw(canvas);
                this.f.setShadowLayer(0.0f, 0.0f, 0.0f, 0);
                canvas.restoreToCount(save);
                return;
            }
            return;
        }
        Objects.requireNonNull(this.J);
        Objects.requireNonNull(this.k);
        canvas.drawBitmap(this.k, (Rect) null, this.J, this.h);
    }
}
