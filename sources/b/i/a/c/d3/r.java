package b.i.a.c.d3;

import android.content.Context;
import android.text.Layout;
import android.webkit.WebView;
import android.widget.FrameLayout;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.b3.b;
import b.i.a.c.f3.e0;
import com.google.android.exoplayer2.ui.SubtitleView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
/* compiled from: WebViewSubtitleOutput.java */
/* loaded from: classes3.dex */
public final class r extends FrameLayout implements SubtitleView.a {
    public final j j;
    public final WebView k;
    public List<b> l = Collections.emptyList();
    public k m = k.a;
    public float n = 0.0533f;
    public int o = 0;
    public float p = 0.08f;

    /* compiled from: WebViewSubtitleOutput.java */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[Layout.Alignment.values().length];
            a = iArr;
            try {
                iArr[Layout.Alignment.ALIGN_NORMAL.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[Layout.Alignment.ALIGN_OPPOSITE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[Layout.Alignment.ALIGN_CENTER.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    public r(Context context) {
        super(context, null);
        j jVar = new j(context, null);
        this.j = jVar;
        q qVar = new q(context, null);
        this.k = qVar;
        qVar.setBackgroundColor(0);
        addView(jVar);
        addView(qVar);
    }

    public static int b(int i) {
        if (i != 1) {
            return i != 2 ? 0 : -100;
        }
        return -50;
    }

    public static String c(@Nullable Layout.Alignment alignment) {
        if (alignment == null) {
            return "center";
        }
        int i = a.a[alignment.ordinal()];
        return i != 1 ? i != 2 ? "center" : "end" : "start";
    }

    @Override // com.google.android.exoplayer2.ui.SubtitleView.a
    public void a(List<b> list, k kVar, float f, int i, float f2) {
        this.m = kVar;
        this.n = f;
        this.o = i;
        this.p = f2;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (int i2 = 0; i2 < list.size(); i2++) {
            b bVar = list.get(i2);
            if (bVar.o != null) {
                arrayList.add(bVar);
            } else {
                arrayList2.add(bVar);
            }
        }
        if (!this.l.isEmpty() || !arrayList2.isEmpty()) {
            this.l = arrayList2;
            e();
        }
        this.j.a(arrayList, kVar, f, i, f2);
        invalidate();
    }

    public final String d(int i, float f) {
        float U1 = d.U1(i, f, getHeight(), (getHeight() - getPaddingTop()) - getPaddingBottom());
        return U1 == -3.4028235E38f ? "unset" : e0.k("%.2fpx", Float.valueOf(U1 / getContext().getResources().getDisplayMetrics().density));
    }

    /* JADX WARN: Code restructure failed: missing block: B:178:0x0462, code lost:
        if (((android.text.style.TypefaceSpan) r9).getFamily() != null) goto L198;
     */
    /* JADX WARN: Code restructure failed: missing block: B:56:0x01b0, code lost:
        if (r5 != 0) goto L59;
     */
    /* JADX WARN: Code restructure failed: missing block: B:57:0x01b3, code lost:
        if (r5 != 0) goto L58;
     */
    /* JADX WARN: Code restructure failed: missing block: B:58:0x01b5, code lost:
        r20 = r21;
     */
    /* JADX WARN: Code restructure failed: missing block: B:59:0x01b7, code lost:
        r3 = 2;
        r21 = r22;
        r22 = r20;
     */
    /* JADX WARN: Removed duplicated region for block: B:201:0x04b6  */
    /* JADX WARN: Removed duplicated region for block: B:225:0x0599  */
    /* JADX WARN: Removed duplicated region for block: B:234:0x0609  */
    /* JADX WARN: Removed duplicated region for block: B:240:0x0629  */
    /* JADX WARN: Removed duplicated region for block: B:243:0x064b  */
    /* JADX WARN: Removed duplicated region for block: B:244:0x0667  */
    /* JADX WARN: Removed duplicated region for block: B:259:0x04f0 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void e() {
        /*
            Method dump skipped, instructions count: 1767
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.d3.r.e():void");
    }

    @Override // android.widget.FrameLayout, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
        super.onLayout(z2, i, i2, i3, i4);
        if (z2 && !this.l.isEmpty()) {
            e();
        }
    }
}
