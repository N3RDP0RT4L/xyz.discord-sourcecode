package b.i.a.c.d3;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import androidx.annotation.Nullable;
import b.i.a.c.b3.b;
import com.google.android.exoplayer2.ui.SubtitleView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
/* compiled from: CanvasSubtitleOutput.java */
/* loaded from: classes3.dex */
public final class j extends View implements SubtitleView.a {
    public final List<n> j = new ArrayList();
    public List<b> k = Collections.emptyList();
    public int l = 0;
    public float m = 0.0533f;
    public k n = k.a;
    public float o = 0.08f;

    public j(Context context, @Nullable AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override // com.google.android.exoplayer2.ui.SubtitleView.a
    public void a(List<b> list, k kVar, float f, int i, float f2) {
        this.k = list;
        this.n = kVar;
        this.m = f;
        this.l = i;
        this.o = f2;
        while (this.j.size() < list.size()) {
            this.j.add(new n(getContext()));
        }
        invalidate();
    }

    /* JADX WARN: Code restructure failed: missing block: B:168:0x03e8, code lost:
        if (r3 < r6) goto L171;
     */
    /* JADX WARN: Removed duplicated region for block: B:166:0x03e3  */
    /* JADX WARN: Removed duplicated region for block: B:167:0x03e6  */
    /* JADX WARN: Removed duplicated region for block: B:185:0x04a7  */
    /* JADX WARN: Removed duplicated region for block: B:186:0x04a9  */
    @Override // android.view.View
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void dispatchDraw(android.graphics.Canvas r40) {
        /*
            Method dump skipped, instructions count: 1247
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.d3.j.dispatchDraw(android.graphics.Canvas):void");
    }
}
