package b.i.a.c.d3;

import android.graphics.Typeface;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
/* compiled from: CaptionStyleCompat.java */
/* loaded from: classes3.dex */
public final class k {
    public static final k a = new k(-1, ViewCompat.MEASURED_STATE_MASK, 0, 0, -1, null);

    /* renamed from: b  reason: collision with root package name */
    public final int f910b;
    public final int c;
    public final int d;
    public final int e;
    public final int f;
    @Nullable
    public final Typeface g;

    public k(int i, int i2, int i3, int i4, int i5, @Nullable Typeface typeface) {
        this.f910b = i;
        this.c = i2;
        this.d = i3;
        this.e = i4;
        this.f = i5;
        this.g = typeface;
    }
}
