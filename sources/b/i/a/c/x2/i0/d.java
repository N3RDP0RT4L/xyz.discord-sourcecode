package b.i.a.c.x2.i0;

import androidx.annotation.Nullable;
import b.i.a.c.f3.x;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/* compiled from: Atom.java */
/* loaded from: classes3.dex */
public abstract class d {
    public final int a;

    /* compiled from: Atom.java */
    /* loaded from: classes3.dex */
    public static final class a extends d {

        /* renamed from: b  reason: collision with root package name */
        public final long f1205b;
        public final List<b> c = new ArrayList();
        public final List<a> d = new ArrayList();

        public a(int i, long j) {
            super(i);
            this.f1205b = j;
        }

        @Nullable
        public a b(int i) {
            int size = this.d.size();
            for (int i2 = 0; i2 < size; i2++) {
                a aVar = this.d.get(i2);
                if (aVar.a == i) {
                    return aVar;
                }
            }
            return null;
        }

        @Nullable
        public b c(int i) {
            int size = this.c.size();
            for (int i2 = 0; i2 < size; i2++) {
                b bVar = this.c.get(i2);
                if (bVar.a == i) {
                    return bVar;
                }
            }
            return null;
        }

        @Override // b.i.a.c.x2.i0.d
        public String toString() {
            String a = d.a(this.a);
            String arrays = Arrays.toString(this.c.toArray());
            String arrays2 = Arrays.toString(this.d.toArray());
            StringBuilder Q = b.d.b.a.a.Q(b.d.b.a.a.b(arrays2, b.d.b.a.a.b(arrays, b.d.b.a.a.b(a, 22))), a, " leaves: ", arrays, " containers: ");
            Q.append(arrays2);
            return Q.toString();
        }
    }

    /* compiled from: Atom.java */
    /* loaded from: classes3.dex */
    public static final class b extends d {

        /* renamed from: b  reason: collision with root package name */
        public final x f1206b;

        public b(int i, x xVar) {
            super(i);
            this.f1206b = xVar;
        }
    }

    public d(int i) {
        this.a = i;
    }

    public static String a(int i) {
        StringBuilder sb = new StringBuilder(4);
        sb.append((char) ((i >> 24) & 255));
        sb.append((char) ((i >> 16) & 255));
        sb.append((char) ((i >> 8) & 255));
        sb.append((char) (i & 255));
        return sb.toString();
    }

    public String toString() {
        return a(this.a);
    }
}
