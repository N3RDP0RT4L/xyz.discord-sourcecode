package b.i.a.c.x2.i0;

import android.util.Log;
import android.util.SparseArray;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.u;
import b.i.a.c.f3.x;
import b.i.a.c.j1;
import b.i.a.c.x2.h;
import b.i.a.c.x2.i;
import b.i.a.c.x2.i0.d;
import b.i.a.c.x2.j;
import b.i.a.c.x2.w;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.drm.DrmInitData;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
/* compiled from: FragmentedMp4Extractor.java */
/* loaded from: classes3.dex */
public class g implements h {
    public static final byte[] a = {-94, 57, 79, 82, 90, -101, 79, 20, -94, 68, 108, 66, 124, 100, -115, -12};

    /* renamed from: b  reason: collision with root package name */
    public static final j1 f1211b;
    public int A;
    public int B;
    public boolean C;
    public boolean G;
    public final int c;
    public final List<j1> d;
    public final byte[] i;
    public final x j;
    public int o;
    public int p;
    public long q;
    public int r;
    @Nullable

    /* renamed from: s  reason: collision with root package name */
    public x f1212s;
    public long t;
    public int u;
    @Nullable

    /* renamed from: y  reason: collision with root package name */
    public b f1214y;

    /* renamed from: z  reason: collision with root package name */
    public int f1215z;
    public final b.i.a.c.z2.i.b k = new b.i.a.c.z2.i.b();
    public final x l = new x(16);
    public final x f = new x(u.a);
    public final x g = new x(5);
    public final x h = new x();
    public final ArrayDeque<d.a> m = new ArrayDeque<>();
    public final ArrayDeque<a> n = new ArrayDeque<>();
    public final SparseArray<b> e = new SparseArray<>();
    public long w = -9223372036854775807L;
    public long v = -9223372036854775807L;

    /* renamed from: x  reason: collision with root package name */
    public long f1213x = -9223372036854775807L;
    public j D = j.d;
    public w[] E = new w[0];
    public w[] F = new w[0];

    /* compiled from: FragmentedMp4Extractor.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public final long a;

        /* renamed from: b  reason: collision with root package name */
        public final int f1216b;

        public a(long j, int i) {
            this.a = j;
            this.f1216b = i;
        }
    }

    /* compiled from: FragmentedMp4Extractor.java */
    /* loaded from: classes3.dex */
    public static final class b {
        public final w a;
        public p d;
        public f e;
        public int f;
        public int g;
        public int h;
        public int i;
        public boolean l;

        /* renamed from: b  reason: collision with root package name */
        public final o f1217b = new o();
        public final x c = new x();
        public final x j = new x(1);
        public final x k = new x();

        public b(w wVar, p pVar, f fVar) {
            this.a = wVar;
            this.d = pVar;
            this.e = fVar;
            this.d = pVar;
            this.e = fVar;
            wVar.e(pVar.a.f);
            e();
        }

        public long a() {
            if (!this.l) {
                return this.d.c[this.f];
            }
            return this.f1217b.f[this.h];
        }

        @Nullable
        public n b() {
            if (!this.l) {
                return null;
            }
            o oVar = this.f1217b;
            f fVar = oVar.a;
            int i = e0.a;
            int i2 = fVar.a;
            n nVar = oVar.m;
            if (nVar == null) {
                nVar = this.d.a.a(i2);
            }
            if (nVar == null || !nVar.a) {
                return null;
            }
            return nVar;
        }

        public boolean c() {
            this.f++;
            if (!this.l) {
                return false;
            }
            int i = this.g + 1;
            this.g = i;
            int[] iArr = this.f1217b.g;
            int i2 = this.h;
            if (i != iArr[i2]) {
                return true;
            }
            this.h = i2 + 1;
            this.g = 0;
            return false;
        }

        public int d(int i, int i2) {
            x xVar;
            n b2 = b();
            if (b2 == null) {
                return 0;
            }
            int i3 = b2.d;
            if (i3 != 0) {
                xVar = this.f1217b.n;
            } else {
                byte[] bArr = b2.e;
                int i4 = e0.a;
                x xVar2 = this.k;
                int length = bArr.length;
                xVar2.a = bArr;
                xVar2.c = length;
                xVar2.f980b = 0;
                i3 = bArr.length;
                xVar = xVar2;
            }
            o oVar = this.f1217b;
            boolean z2 = oVar.k && oVar.l[this.f];
            boolean z3 = z2 || i2 != 0;
            x xVar3 = this.j;
            xVar3.a[0] = (byte) ((z3 ? 128 : 0) | i3);
            xVar3.E(0);
            this.a.f(this.j, 1, 1);
            this.a.f(xVar, i3, 1);
            if (!z3) {
                return i3 + 1;
            }
            if (!z2) {
                this.c.A(8);
                x xVar4 = this.c;
                byte[] bArr2 = xVar4.a;
                bArr2[0] = 0;
                bArr2[1] = 1;
                bArr2[2] = (byte) ((i2 >> 8) & 255);
                bArr2[3] = (byte) (i2 & 255);
                bArr2[4] = (byte) ((i >> 24) & 255);
                bArr2[5] = (byte) ((i >> 16) & 255);
                bArr2[6] = (byte) ((i >> 8) & 255);
                bArr2[7] = (byte) (i & 255);
                this.a.f(xVar4, 8, 1);
                return i3 + 1 + 8;
            }
            x xVar5 = this.f1217b.n;
            int y2 = xVar5.y();
            xVar5.F(-2);
            int i5 = (y2 * 6) + 2;
            if (i2 != 0) {
                this.c.A(i5);
                byte[] bArr3 = this.c.a;
                xVar5.e(bArr3, 0, i5);
                int i6 = (((bArr3[2] & 255) << 8) | (bArr3[3] & 255)) + i2;
                bArr3[2] = (byte) ((i6 >> 8) & 255);
                bArr3[3] = (byte) (i6 & 255);
                xVar5 = this.c;
            }
            this.a.f(xVar5, i5, 1);
            return i3 + 1 + i5;
        }

        public void e() {
            o oVar = this.f1217b;
            oVar.d = 0;
            oVar.p = 0L;
            oVar.q = false;
            oVar.k = false;
            oVar.o = false;
            oVar.m = null;
            this.f = 0;
            this.h = 0;
            this.g = 0;
            this.i = 0;
            this.l = false;
        }
    }

    static {
        j1.b bVar = new j1.b();
        bVar.k = "application/x-emsg";
        f1211b = bVar.a();
    }

    public g(int i) {
        List emptyList = Collections.emptyList();
        this.c = i;
        this.d = Collections.unmodifiableList(emptyList);
        byte[] bArr = new byte[16];
        this.i = bArr;
        this.j = new x(bArr);
    }

    public static int a(int i) throws ParserException {
        if (i >= 0) {
            return i;
        }
        throw b.d.b.a.a.t0(38, "Unexpected negative value: ", i, null);
    }

    @Nullable
    public static DrmInitData h(List<d.b> list) {
        int size = list.size();
        ArrayList arrayList = null;
        for (int i = 0; i < size; i++) {
            d.b bVar = list.get(i);
            if (bVar.a == 1886614376) {
                if (arrayList == null) {
                    arrayList = new ArrayList();
                }
                byte[] bArr = bVar.f1206b.a;
                j v1 = b.c.a.a0.d.v1(bArr);
                UUID uuid = v1 == null ? null : v1.a;
                if (uuid == null) {
                    Log.w("FragmentedMp4Extractor", "Skipped pssh atom (failed to extract uuid)");
                } else {
                    arrayList.add(new DrmInitData.SchemeData(uuid, "video/mp4", bArr));
                }
            }
        }
        if (arrayList == null) {
            return null;
        }
        return new DrmInitData(null, false, (DrmInitData.SchemeData[]) arrayList.toArray(new DrmInitData.SchemeData[0]));
    }

    public static void i(x xVar, int i, o oVar) throws ParserException {
        xVar.E(i + 8);
        int f = xVar.f() & ViewCompat.MEASURED_SIZE_MASK;
        if ((f & 1) == 0) {
            boolean z2 = (f & 2) != 0;
            int w = xVar.w();
            if (w == 0) {
                Arrays.fill(oVar.l, 0, oVar.e, false);
                return;
            }
            int i2 = oVar.e;
            if (w == i2) {
                Arrays.fill(oVar.l, 0, w, z2);
                int a2 = xVar.a();
                x xVar2 = oVar.n;
                byte[] bArr = xVar2.a;
                if (bArr.length < a2) {
                    bArr = new byte[a2];
                }
                xVar2.a = bArr;
                xVar2.c = a2;
                xVar2.f980b = 0;
                oVar.k = true;
                oVar.o = true;
                xVar.e(bArr, 0, a2);
                oVar.n.E(0);
                oVar.o = false;
                return;
            }
            throw ParserException.a(b.d.b.a.a.g(80, "Senc sample count ", w, " is different from fragment sample count", i2), null);
        }
        throw ParserException.b("Overriding TrackEncryptionBox parameters is unsupported.");
    }

    @Override // b.i.a.c.x2.h
    public boolean b(i iVar) throws IOException {
        return l.a(iVar, true, false);
    }

    public final void c() {
        this.o = 0;
        this.r = 0;
    }

    public final f d(SparseArray<f> sparseArray, int i) {
        if (sparseArray.size() == 1) {
            return sparseArray.valueAt(0);
        }
        f fVar = sparseArray.get(i);
        Objects.requireNonNull(fVar);
        return fVar;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:338:0x02cb A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:349:0x0759 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:352:0x0004 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:356:0x0004 A[SYNTHETIC] */
    @Override // b.i.a.c.x2.h
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public int e(b.i.a.c.x2.i r25, b.i.a.c.x2.s r26) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 1897
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.i0.g.e(b.i.a.c.x2.i, b.i.a.c.x2.s):int");
    }

    @Override // b.i.a.c.x2.h
    public void f(j jVar) {
        int i;
        this.D = jVar;
        c();
        w[] wVarArr = new w[2];
        this.E = wVarArr;
        int i2 = 100;
        if ((this.c & 4) != 0) {
            wVarArr[0] = this.D.p(100, 5);
            i2 = 101;
            i = 1;
        } else {
            i = 0;
        }
        w[] wVarArr2 = (w[]) e0.D(this.E, i);
        this.E = wVarArr2;
        for (w wVar : wVarArr2) {
            wVar.e(f1211b);
        }
        this.F = new w[this.d.size()];
        for (int i3 = 0; i3 < this.F.length; i3++) {
            i2++;
            w p = this.D.p(i2, 3);
            p.e(this.d.get(i3));
            this.F[i3] = p;
        }
    }

    @Override // b.i.a.c.x2.h
    public void g(long j, long j2) {
        int size = this.e.size();
        for (int i = 0; i < size; i++) {
            this.e.valueAt(i).e();
        }
        this.n.clear();
        this.u = 0;
        this.v = j2;
        this.m.clear();
        c();
    }

    /* JADX WARN: Removed duplicated region for block: B:143:0x0370  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void j(long r46) throws com.google.android.exoplayer2.ParserException {
        /*
            Method dump skipped, instructions count: 1872
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.i0.g.j(long):void");
    }

    @Override // b.i.a.c.x2.h
    public void release() {
    }
}
