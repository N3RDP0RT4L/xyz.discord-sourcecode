package b.i.a.c.x2.i0;

import b.i.b.a.b;
import b.i.b.a.j;
import com.discord.widgets.chat.input.MentionUtilsKt;
import java.util.ArrayList;
import java.util.List;
/* compiled from: SefReader.java */
/* loaded from: classes3.dex */
public final class k {
    public static final b.i.b.a.k a = new b.i.b.a.k(new j(new b.C0129b(MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR)));

    /* renamed from: b  reason: collision with root package name */
    public static final b.i.b.a.k f1224b = new b.i.b.a.k(new j(new b.C0129b('*')));
    public final List<a> c = new ArrayList();
    public int d = 0;
    public int e;

    /* compiled from: SefReader.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public final long a;

        /* renamed from: b  reason: collision with root package name */
        public final int f1225b;

        public a(int i, long j, int i2) {
            this.a = j;
            this.f1225b = i2;
        }
    }
}
