package b.i.a.c.x2.i0;

import androidx.annotation.Nullable;
import b.i.a.c.f3.x;
/* compiled from: TrackFragment.java */
/* loaded from: classes3.dex */
public final class o {
    public f a;

    /* renamed from: b  reason: collision with root package name */
    public long f1228b;
    public long c;
    public int d;
    public int e;
    public boolean k;
    @Nullable
    public n m;
    public boolean o;
    public long p;
    public boolean q;
    public long[] f = new long[0];
    public int[] g = new int[0];
    public int[] h = new int[0];
    public long[] i = new long[0];
    public boolean[] j = new boolean[0];
    public boolean[] l = new boolean[0];
    public final x n = new x();
}
