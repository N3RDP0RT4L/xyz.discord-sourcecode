package b.i.a.c.x2.i0;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import b.i.a.c.f3.u;
import b.i.a.c.f3.x;
import b.i.a.c.x2.h;
import b.i.a.c.x2.i0.d;
import b.i.a.c.x2.j;
import b.i.a.c.x2.t;
import b.i.a.c.x2.w;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.mp4.MotionPhotoMetadata;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
/* compiled from: Mp4Extractor.java */
/* loaded from: classes3.dex */
public final class i implements h, t {
    public static final /* synthetic */ int a = 0;

    /* renamed from: b  reason: collision with root package name */
    public final int f1218b;
    public final x c;
    public final x d;
    public final x e;
    public final x f;
    public final ArrayDeque<d.a> g;
    public final k h;
    public final List<Metadata.Entry> i;
    public int j;
    public int k;
    public long l;
    public int m;
    @Nullable
    public x n;
    public int o;
    public int p;
    public int q;
    public int r;

    /* renamed from: s  reason: collision with root package name */
    public j f1219s;
    public a[] t;
    public long[][] u;
    public int v;
    public long w;

    /* renamed from: x  reason: collision with root package name */
    public int f1220x;
    @Nullable

    /* renamed from: y  reason: collision with root package name */
    public MotionPhotoMetadata f1221y;

    /* compiled from: Mp4Extractor.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public final m a;

        /* renamed from: b  reason: collision with root package name */
        public final p f1222b;
        public final w c;
        @Nullable
        public final b.i.a.c.x2.x d;
        public int e;

        public a(m mVar, p pVar, w wVar) {
            this.a = mVar;
            this.f1222b = pVar;
            this.c = wVar;
            this.d = "audio/true-hd".equals(mVar.f.w) ? new b.i.a.c.x2.x() : null;
        }
    }

    static {
        b bVar = b.a;
    }

    public i(int i) {
        this.f1218b = i;
        this.j = (i & 4) != 0 ? 3 : 0;
        this.h = new k();
        this.i = new ArrayList();
        this.f = new x(16);
        this.g = new ArrayDeque<>();
        this.c = new x(u.a);
        this.d = new x(4);
        this.e = new x();
        this.o = -1;
    }

    public static long k(p pVar, long j, long j2) {
        int a2 = pVar.a(j);
        if (a2 == -1) {
            a2 = pVar.b(j);
        }
        return a2 == -1 ? j2 : Math.min(pVar.c[a2], j2);
    }

    @Override // b.i.a.c.x2.h
    public boolean b(b.i.a.c.x2.i iVar) throws IOException {
        return l.a(iVar, false, (this.f1218b & 2) != 0);
    }

    @Override // b.i.a.c.x2.t
    public boolean c() {
        return true;
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Removed duplicated region for block: B:344:0x04a5 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:347:0x06b9 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:349:0x0006 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:350:0x0006 A[SYNTHETIC] */
    @Override // b.i.a.c.x2.h
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public int e(b.i.a.c.x2.i r35, b.i.a.c.x2.s r36) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 1752
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.i0.i.e(b.i.a.c.x2.i, b.i.a.c.x2.s):int");
    }

    @Override // b.i.a.c.x2.h
    public void f(j jVar) {
        this.f1219s = jVar;
    }

    @Override // b.i.a.c.x2.h
    public void g(long j, long j2) {
        this.g.clear();
        this.m = 0;
        this.o = -1;
        this.p = 0;
        this.q = 0;
        this.r = 0;
        if (j != 0) {
            a[] aVarArr = this.t;
            if (aVarArr != null) {
                for (a aVar : aVarArr) {
                    p pVar = aVar.f1222b;
                    int a2 = pVar.a(j2);
                    if (a2 == -1) {
                        a2 = pVar.b(j2);
                    }
                    aVar.e = a2;
                    b.i.a.c.x2.x xVar = aVar.d;
                    if (xVar != null) {
                        xVar.f1296b = false;
                        xVar.c = 0;
                    }
                }
            }
        } else if (this.j != 3) {
            j();
        } else {
            k kVar = this.h;
            kVar.c.clear();
            kVar.d = 0;
            this.i.clear();
        }
    }

    @Override // b.i.a.c.x2.t
    public t.a h(long j) {
        long j2;
        long j3;
        long j4;
        long j5;
        int b2;
        long j6 = j;
        a[] aVarArr = this.t;
        Objects.requireNonNull(aVarArr);
        if (aVarArr.length == 0) {
            return new t.a(b.i.a.c.x2.u.a);
        }
        long j7 = -1;
        int i = this.v;
        if (i != -1) {
            p pVar = this.t[i].f1222b;
            int a2 = pVar.a(j6);
            if (a2 == -1) {
                a2 = pVar.b(j6);
            }
            if (a2 == -1) {
                return new t.a(b.i.a.c.x2.u.a);
            }
            long j8 = pVar.f[a2];
            j2 = pVar.c[a2];
            if (j8 >= j6 || a2 >= pVar.f1229b - 1 || (b2 = pVar.b(j6)) == -1 || b2 == a2) {
                j5 = -9223372036854775807L;
            } else {
                j5 = pVar.f[b2];
                j7 = pVar.c[b2];
            }
            j3 = j7;
            j4 = j5;
            j6 = j8;
        } else {
            j2 = RecyclerView.FOREVER_NS;
            j3 = -1;
            j4 = -9223372036854775807L;
        }
        int i2 = 0;
        while (true) {
            a[] aVarArr2 = this.t;
            if (i2 >= aVarArr2.length) {
                break;
            }
            if (i2 != this.v) {
                p pVar2 = aVarArr2[i2].f1222b;
                long k = k(pVar2, j6, j2);
                if (j4 != -9223372036854775807L) {
                    j3 = k(pVar2, j4, j3);
                }
                j2 = k;
            }
            i2++;
        }
        b.i.a.c.x2.u uVar = new b.i.a.c.x2.u(j6, j2);
        if (j4 == -9223372036854775807L) {
            return new t.a(uVar);
        }
        return new t.a(uVar, new b.i.a.c.x2.u(j4, j3));
    }

    @Override // b.i.a.c.x2.t
    public long i() {
        return this.w;
    }

    public final void j() {
        this.j = 0;
        this.m = 0;
    }

    /* JADX WARN: Code restructure failed: missing block: B:101:0x01db, code lost:
        if (r9 != 1851878757) goto L103;
     */
    /* JADX WARN: Code restructure failed: missing block: B:102:0x01dd, code lost:
        r6 = r3.o(r11 - 12);
     */
    /* JADX WARN: Code restructure failed: missing block: B:104:0x01e7, code lost:
        if (r9 != 1684108385) goto L106;
     */
    /* JADX WARN: Code restructure failed: missing block: B:105:0x01e9, code lost:
        r5 = r11;
        r0 = r9;
     */
    /* JADX WARN: Code restructure failed: missing block: B:106:0x01ec, code lost:
        r3.F(r11 - 12);
     */
    /* JADX WARN: Code restructure failed: missing block: B:108:0x01f4, code lost:
        r19 = r8;
     */
    /* JADX WARN: Code restructure failed: missing block: B:109:0x01f6, code lost:
        if (r13 == null) goto L157;
     */
    /* JADX WARN: Code restructure failed: missing block: B:110:0x01f8, code lost:
        if (r6 == null) goto L157;
     */
    /* JADX WARN: Code restructure failed: missing block: B:112:0x01fb, code lost:
        if (r0 != (-1)) goto L114;
     */
    /* JADX WARN: Code restructure failed: missing block: B:114:0x01ff, code lost:
        r3.E(r0);
        r3.F(16);
        r0 = new com.google.android.exoplayer2.metadata.id3.InternalFrame(r13, r6, r3.o(r5 - 16));
     */
    /* JADX WARN: Code restructure failed: missing block: B:115:0x0215, code lost:
        r19 = r8;
     */
    /* JADX WARN: Code restructure failed: missing block: B:116:0x0219, code lost:
        r19 = r8;
        r8 = 16777215 & r5;
     */
    /* JADX WARN: Code restructure failed: missing block: B:117:0x0222, code lost:
        if (r8 != 6516084) goto L121;
     */
    /* JADX WARN: Code restructure failed: missing block: B:118:0x0224, code lost:
        r0 = b.i.a.c.x2.i0.h.a(r5, r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:119:0x022a, code lost:
        r0 = move-exception;
     */
    /* JADX WARN: Code restructure failed: missing block: B:122:0x0230, code lost:
        if (r8 == 7233901) goto L159;
     */
    /* JADX WARN: Code restructure failed: missing block: B:124:0x0235, code lost:
        if (r8 != 7631467) goto L126;
     */
    /* JADX WARN: Code restructure failed: missing block: B:127:0x023c, code lost:
        if (r8 == 6516589) goto L158;
     */
    /* JADX WARN: Code restructure failed: missing block: B:129:0x0241, code lost:
        if (r8 != 7828084) goto L131;
     */
    /* JADX WARN: Code restructure failed: missing block: B:132:0x0248, code lost:
        if (r8 != 6578553) goto L134;
     */
    /* JADX WARN: Code restructure failed: missing block: B:133:0x024a, code lost:
        r0 = b.i.a.c.x2.i0.h.d(r5, "TDRC", r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:135:0x0255, code lost:
        if (r8 != 4280916) goto L137;
     */
    /* JADX WARN: Code restructure failed: missing block: B:136:0x0257, code lost:
        r0 = b.i.a.c.x2.i0.h.d(r5, "TPE1", r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:138:0x0261, code lost:
        if (r8 != 7630703) goto L140;
     */
    /* JADX WARN: Code restructure failed: missing block: B:139:0x0263, code lost:
        r0 = b.i.a.c.x2.i0.h.d(r5, "TSSE", r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:141:0x026d, code lost:
        if (r8 != 6384738) goto L143;
     */
    /* JADX WARN: Code restructure failed: missing block: B:142:0x026f, code lost:
        r0 = b.i.a.c.x2.i0.h.d(r5, "TALB", r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:144:0x0279, code lost:
        if (r8 != 7108978) goto L146;
     */
    /* JADX WARN: Code restructure failed: missing block: B:145:0x027b, code lost:
        r0 = b.i.a.c.x2.i0.h.d(r5, "USLT", r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:147:0x0285, code lost:
        if (r8 != 6776174) goto L149;
     */
    /* JADX WARN: Code restructure failed: missing block: B:148:0x0287, code lost:
        r0 = b.i.a.c.x2.i0.h.d(r5, "TCON", r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:150:0x028f, code lost:
        if (r8 != 6779504) goto L152;
     */
    /* JADX WARN: Code restructure failed: missing block: B:151:0x0291, code lost:
        r0 = b.i.a.c.x2.i0.h.d(r5, "TIT1", r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:152:0x0298, code lost:
        r5 = java.lang.String.valueOf(b.i.a.c.x2.i0.d.a(r5));
     */
    /* JADX WARN: Code restructure failed: missing block: B:153:0x02a6, code lost:
        if (r5.length() == 0) goto L155;
     */
    /* JADX WARN: Code restructure failed: missing block: B:154:0x02a8, code lost:
        r5 = "Skipped unknown metadata entry: ".concat(r5);
     */
    /* JADX WARN: Code restructure failed: missing block: B:155:0x02ad, code lost:
        r5 = new java.lang.String("Skipped unknown metadata entry: ");
     */
    /* JADX WARN: Code restructure failed: missing block: B:156:0x02b2, code lost:
        android.util.Log.d("MetadataUtil", r5);
     */
    /* JADX WARN: Code restructure failed: missing block: B:157:0x02b5, code lost:
        r0 = null;
     */
    /* JADX WARN: Code restructure failed: missing block: B:158:0x02b7, code lost:
        r0 = b.i.a.c.x2.i0.h.d(r5, "TCOM", r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:159:0x02be, code lost:
        r0 = b.i.a.c.x2.i0.h.d(r5, "TIT2", r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:161:0x02c7, code lost:
        if (r0 == null) goto L355;
     */
    /* JADX WARN: Code restructure failed: missing block: B:162:0x02c9, code lost:
        r4.add(r0);
     */
    /* JADX WARN: Code restructure failed: missing block: B:163:0x02cc, code lost:
        r13 = null;
        r8 = r19;
     */
    /* JADX WARN: Code restructure failed: missing block: B:164:0x02d5, code lost:
        r3.E(r7);
     */
    /* JADX WARN: Code restructure failed: missing block: B:165:0x02d8, code lost:
        throw r0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:166:0x02d9, code lost:
        r17 = r0;
        r19 = r8;
     */
    /* JADX WARN: Code restructure failed: missing block: B:167:0x02e1, code lost:
        if (r4.isEmpty() == false) goto L168;
     */
    /* JADX WARN: Code restructure failed: missing block: B:168:0x02e4, code lost:
        r0 = new com.google.android.exoplayer2.metadata.Metadata(r4);
     */
    /* JADX WARN: Code restructure failed: missing block: B:28:0x0097, code lost:
        r3.E(r6);
        r6 = r6 + r7;
        r3.F(r9);
        r4 = new java.util.ArrayList();
     */
    /* JADX WARN: Code restructure failed: missing block: B:29:0x00a3, code lost:
        r5 = r3.f980b;
     */
    /* JADX WARN: Code restructure failed: missing block: B:30:0x00a5, code lost:
        if (r5 >= r6) goto L353;
     */
    /* JADX WARN: Code restructure failed: missing block: B:31:0x00a7, code lost:
        r7 = r3.f() + r5;
        r5 = r3.f();
        r9 = (r5 >> 24) & 255;
        r6 = r6;
        r0 = r0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:32:0x00be, code lost:
        if (r9 == 169) goto L116;
     */
    /* JADX WARN: Code restructure failed: missing block: B:34:0x00c2, code lost:
        if (r9 != 253) goto L35;
     */
    /* JADX WARN: Code restructure failed: missing block: B:36:0x00c9, code lost:
        if (r5 != 1735291493) goto L46;
     */
    /* JADX WARN: Code restructure failed: missing block: B:37:0x00cb, code lost:
        r5 = b.i.a.c.x2.i0.h.f(r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:38:0x00cf, code lost:
        if (r5 <= 0) goto L42;
     */
    /* JADX WARN: Code restructure failed: missing block: B:39:0x00d1, code lost:
        r9 = b.i.a.c.x2.i0.h.a;
     */
    /* JADX WARN: Code restructure failed: missing block: B:40:0x00d4, code lost:
        if (r5 > r9.length) goto L42;
     */
    /* JADX WARN: Code restructure failed: missing block: B:41:0x00d6, code lost:
        r5 = r9[r5 - 1];
     */
    /* JADX WARN: Code restructure failed: missing block: B:42:0x00db, code lost:
        r5 = r13;
     */
    /* JADX WARN: Code restructure failed: missing block: B:43:0x00dc, code lost:
        if (r5 == null) goto L45;
     */
    /* JADX WARN: Code restructure failed: missing block: B:44:0x00de, code lost:
        r0 = new com.google.android.exoplayer2.metadata.id3.TextInformationFrame("TCON", r13, r5);
     */
    /* JADX WARN: Code restructure failed: missing block: B:45:0x00e5, code lost:
        android.util.Log.w("MetadataUtil", "Failed to parse standard genre code");
        r19 = r8;
     */
    /* JADX WARN: Code restructure failed: missing block: B:47:0x00f1, code lost:
        if (r5 != 1684632427) goto L49;
     */
    /* JADX WARN: Code restructure failed: missing block: B:48:0x00f3, code lost:
        r0 = b.i.a.c.x2.i0.h.c(r5, "TPOS", r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:50:0x00fe, code lost:
        if (r5 != 1953655662) goto L52;
     */
    /* JADX WARN: Code restructure failed: missing block: B:51:0x0100, code lost:
        r0 = b.i.a.c.x2.i0.h.c(r5, "TRCK", r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:53:0x010b, code lost:
        if (r5 != 1953329263) goto L55;
     */
    /* JADX WARN: Code restructure failed: missing block: B:54:0x010d, code lost:
        r0 = b.i.a.c.x2.i0.h.e(r5, "TBPM", r3, true, false);
     */
    /* JADX WARN: Code restructure failed: missing block: B:56:0x011b, code lost:
        if (r5 != 1668311404) goto L58;
     */
    /* JADX WARN: Code restructure failed: missing block: B:57:0x011d, code lost:
        r0 = b.i.a.c.x2.i0.h.e(r5, "TCMP", r3, true, true);
     */
    /* JADX WARN: Code restructure failed: missing block: B:59:0x0128, code lost:
        if (r5 != 1668249202) goto L61;
     */
    /* JADX WARN: Code restructure failed: missing block: B:60:0x012a, code lost:
        r0 = b.i.a.c.x2.i0.h.b(r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:62:0x0133, code lost:
        if (r5 != 1631670868) goto L64;
     */
    /* JADX WARN: Code restructure failed: missing block: B:63:0x0135, code lost:
        r0 = b.i.a.c.x2.i0.h.d(r5, "TPE2", r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:65:0x0140, code lost:
        if (r5 != 1936682605) goto L67;
     */
    /* JADX WARN: Code restructure failed: missing block: B:66:0x0142, code lost:
        r0 = b.i.a.c.x2.i0.h.d(r5, "TSOT", r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:68:0x014c, code lost:
        if (r5 != 1936679276) goto L70;
     */
    /* JADX WARN: Code restructure failed: missing block: B:69:0x014e, code lost:
        r0 = b.i.a.c.x2.i0.h.d(r5, "TSO2", r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:71:0x0158, code lost:
        if (r5 != 1936679282) goto L73;
     */
    /* JADX WARN: Code restructure failed: missing block: B:72:0x015a, code lost:
        r0 = b.i.a.c.x2.i0.h.d(r5, "TSOA", r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:74:0x0164, code lost:
        if (r5 != 1936679265) goto L76;
     */
    /* JADX WARN: Code restructure failed: missing block: B:75:0x0166, code lost:
        r0 = b.i.a.c.x2.i0.h.d(r5, "TSOP", r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:77:0x0170, code lost:
        if (r5 != 1936679791) goto L79;
     */
    /* JADX WARN: Code restructure failed: missing block: B:78:0x0172, code lost:
        r0 = b.i.a.c.x2.i0.h.d(r5, "TSOC", r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:80:0x017c, code lost:
        if (r5 != 1920233063) goto L82;
     */
    /* JADX WARN: Code restructure failed: missing block: B:81:0x017e, code lost:
        r0 = b.i.a.c.x2.i0.h.e(r5, "ITUNESADVISORY", r3, false, false);
     */
    /* JADX WARN: Code restructure failed: missing block: B:83:0x018a, code lost:
        if (r5 != 1885823344) goto L85;
     */
    /* JADX WARN: Code restructure failed: missing block: B:84:0x018c, code lost:
        r0 = b.i.a.c.x2.i0.h.e(r5, "ITUNESGAPLESS", r3, false, true);
     */
    /* JADX WARN: Code restructure failed: missing block: B:86:0x0197, code lost:
        if (r5 != 1936683886) goto L88;
     */
    /* JADX WARN: Code restructure failed: missing block: B:87:0x0199, code lost:
        r0 = b.i.a.c.x2.i0.h.d(r5, "TVSHOWSORT", r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:89:0x01a3, code lost:
        if (r5 != 1953919848) goto L92;
     */
    /* JADX WARN: Code restructure failed: missing block: B:90:0x01a5, code lost:
        r0 = b.i.a.c.x2.i0.h.d(r5, "TVSHOW", r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:91:0x01ab, code lost:
        r19 = r8;
     */
    /* JADX WARN: Code restructure failed: missing block: B:93:0x01b2, code lost:
        if (r5 != 757935405) goto L115;
     */
    /* JADX WARN: Code restructure failed: missing block: B:94:0x01b4, code lost:
        r0 = -1;
        r5 = -1;
        r6 = r13;
     */
    /* JADX WARN: Code restructure failed: missing block: B:95:0x01b7, code lost:
        r9 = r3.f980b;
     */
    /* JADX WARN: Code restructure failed: missing block: B:96:0x01b9, code lost:
        if (r9 >= r7) goto L356;
     */
    /* JADX WARN: Code restructure failed: missing block: B:97:0x01bb, code lost:
        r11 = r3.f();
        r9 = r3.f();
        r8 = r8;
        r3.F(4);
     */
    /* JADX WARN: Code restructure failed: missing block: B:98:0x01ce, code lost:
        if (r9 != 1835360622) goto L100;
     */
    /* JADX WARN: Code restructure failed: missing block: B:99:0x01d0, code lost:
        r13 = r3.o(r11 - 12);
     */
    /* JADX WARN: Removed duplicated region for block: B:234:0x049e  */
    /* JADX WARN: Removed duplicated region for block: B:235:0x04a1  */
    /* JADX WARN: Removed duplicated region for block: B:238:0x04c9  */
    /* JADX WARN: Removed duplicated region for block: B:291:0x05b3  */
    /* JADX WARN: Removed duplicated region for block: B:297:0x05c7  */
    /* JADX WARN: Removed duplicated region for block: B:300:0x05d7  */
    /* JADX WARN: Removed duplicated region for block: B:303:0x05e1  */
    /* JADX WARN: Removed duplicated region for block: B:310:0x061e A[LOOP:12: B:308:0x061b->B:310:0x061e, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:314:0x063a  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void l(long r24) throws com.google.android.exoplayer2.ParserException {
        /*
            Method dump skipped, instructions count: 1711
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.i0.i.l(long):void");
    }

    @Override // b.i.a.c.x2.h
    public void release() {
    }
}
