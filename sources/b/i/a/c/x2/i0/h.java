package b.i.a.c.x2.i0;

import android.util.Log;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.core.view.ViewCompat;
import b.d.b.a.a;
import b.i.a.c.f3.x;
import com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel;
import com.google.android.exoplayer2.metadata.id3.ApicFrame;
import com.google.android.exoplayer2.metadata.id3.CommentFrame;
import com.google.android.exoplayer2.metadata.id3.Id3Frame;
import com.google.android.exoplayer2.metadata.id3.TextInformationFrame;
/* compiled from: MetadataUtil.java */
/* loaded from: classes3.dex */
public final class h {
    @VisibleForTesting
    public static final String[] a = {"Blues", "Classic Rock", "Country", "Dance", "Disco", "Funk", "Grunge", "Hip-Hop", "Jazz", "Metal", "New Age", "Oldies", "Other", "Pop", "R&B", "Rap", "Reggae", "Rock", "Techno", "Industrial", "Alternative", "Ska", "Death Metal", "Pranks", "Soundtrack", "Euro-Techno", "Ambient", "Trip-Hop", "Vocal", "Jazz+Funk", "Fusion", "Trance", "Classical", "Instrumental", "Acid", "House", "Game", "Sound Clip", "Gospel", "Noise", "AlternRock", "Bass", "Soul", "Punk", "Space", "Meditative", "Instrumental Pop", "Instrumental Rock", "Ethnic", "Gothic", "Darkwave", "Techno-Industrial", "Electronic", "Pop-Folk", "Eurodance", "Dream", "Southern Rock", "Comedy", "Cult", "Gangsta", "Top 40", "Christian Rap", "Pop/Funk", "Jungle", "Native American", "Cabaret", "New Wave", "Psychadelic", "Rave", "Showtunes", "Trailer", "Lo-Fi", "Tribal", "Acid Punk", "Acid Jazz", "Polka", "Retro", "Musical", "Rock & Roll", "Hard Rock", "Folk", "Folk-Rock", "National Folk", "Swing", "Fast Fusion", "Bebob", "Latin", "Revival", "Celtic", "Bluegrass", "Avantgarde", "Gothic Rock", "Progressive Rock", "Psychedelic Rock", "Symphonic Rock", "Slow Rock", "Big Band", "Chorus", "Easy Listening", "Acoustic", "Humour", "Speech", "Chanson", "Opera", "Chamber Music", "Sonata", "Symphony", "Booty Bass", "Primus", "Porn Groove", "Satire", "Slow Jam", "Club", "Tango", "Samba", "Folklore", "Ballad", "Power Ballad", "Rhythmic Soul", "Freestyle", "Duet", "Punk Rock", "Drum Solo", "A capella", "Euro-House", "Dance Hall", "Goa", "Drum & Bass", "Club-House", "Hardcore", "Terror", "Indie", "BritPop", "Afro-Punk", "Polsk Punk", "Beat", "Christian Gangsta Rap", "Heavy Metal", "Black Metal", "Crossover", "Contemporary Christian", "Christian Rock", "Merengue", "Salsa", "Thrash Metal", "Anime", "Jpop", "Synthpop", "Abstract", "Art Rock", "Baroque", "Bhangra", "Big beat", "Breakbeat", "Chillout", "Downtempo", "Dub", "EBM", "Eclectic", "Electro", "Electroclash", "Emo", "Experimental", "Garage", "Global", "IDM", "Illbient", "Industro-Goth", "Jam Band", "Krautrock", "Leftfield", "Lounge", "Math Rock", "New Romantic", "Nu-Breakz", "Post-Punk", "Post-Rock", "Psytrance", "Shoegaze", "Space Rock", "Trop Rock", "World Music", "Neoclassical", "Audiobook", "Audio theatre", "Neue Deutsche Welle", "Podcast", "Indie-Rock", "G-Funk", "Dubstep", "Garage Rock", "Psybient"};

    @Nullable
    public static CommentFrame a(int i, x xVar) {
        int f = xVar.f();
        if (xVar.f() == 1684108385) {
            xVar.F(8);
            String o = xVar.o(f - 16);
            return new CommentFrame("und", o, o);
        }
        String valueOf = String.valueOf(d.a(i));
        Log.w("MetadataUtil", valueOf.length() != 0 ? "Failed to parse comment attribute: ".concat(valueOf) : new String("Failed to parse comment attribute: "));
        return null;
    }

    @Nullable
    public static ApicFrame b(x xVar) {
        int f = xVar.f();
        if (xVar.f() == 1684108385) {
            int f2 = xVar.f() & ViewCompat.MEASURED_SIZE_MASK;
            String str = f2 == 13 ? "image/jpeg" : f2 == 14 ? "image/png" : null;
            if (str == null) {
                a.d0(41, "Unrecognized cover art flags: ", f2, "MetadataUtil");
                return null;
            }
            xVar.F(4);
            int i = f - 16;
            byte[] bArr = new byte[i];
            System.arraycopy(xVar.a, xVar.f980b, bArr, 0, i);
            xVar.f980b += i;
            return new ApicFrame(str, null, 3, bArr);
        }
        Log.w("MetadataUtil", "Failed to parse cover art attribute");
        return null;
    }

    @Nullable
    public static TextInformationFrame c(int i, String str, x xVar) {
        int f = xVar.f();
        if (xVar.f() == 1684108385 && f >= 22) {
            xVar.F(10);
            int y2 = xVar.y();
            if (y2 > 0) {
                StringBuilder sb = new StringBuilder(11);
                sb.append(y2);
                String sb2 = sb.toString();
                int y3 = xVar.y();
                if (y3 > 0) {
                    String valueOf = String.valueOf(sb2);
                    StringBuilder sb3 = new StringBuilder(valueOf.length() + 12);
                    sb3.append(valueOf);
                    sb3.append(AutocompleteViewModel.COMMAND_DISCOVER_TOKEN);
                    sb3.append(y3);
                    sb2 = sb3.toString();
                }
                return new TextInformationFrame(str, null, sb2);
            }
        }
        String valueOf2 = String.valueOf(d.a(i));
        Log.w("MetadataUtil", valueOf2.length() != 0 ? "Failed to parse index/count attribute: ".concat(valueOf2) : new String("Failed to parse index/count attribute: "));
        return null;
    }

    @Nullable
    public static TextInformationFrame d(int i, String str, x xVar) {
        int f = xVar.f();
        if (xVar.f() == 1684108385) {
            xVar.F(8);
            return new TextInformationFrame(str, null, xVar.o(f - 16));
        }
        String valueOf = String.valueOf(d.a(i));
        Log.w("MetadataUtil", valueOf.length() != 0 ? "Failed to parse text attribute: ".concat(valueOf) : new String("Failed to parse text attribute: "));
        return null;
    }

    @Nullable
    public static Id3Frame e(int i, String str, x xVar, boolean z2, boolean z3) {
        int f = f(xVar);
        if (z3) {
            f = Math.min(1, f);
        }
        if (f < 0) {
            String valueOf = String.valueOf(d.a(i));
            Log.w("MetadataUtil", valueOf.length() != 0 ? "Failed to parse uint8 attribute: ".concat(valueOf) : new String("Failed to parse uint8 attribute: "));
            return null;
        } else if (z2) {
            return new TextInformationFrame(str, null, Integer.toString(f));
        } else {
            return new CommentFrame("und", str, Integer.toString(f));
        }
    }

    public static int f(x xVar) {
        xVar.F(4);
        if (xVar.f() == 1684108385) {
            xVar.F(8);
            return xVar.t();
        }
        Log.w("MetadataUtil", "Failed to parse uint8 attribute value");
        return -1;
    }
}
