package b.i.a.c.x2.i0;

import androidx.annotation.Nullable;
import b.i.a.c.x2.w;
/* compiled from: TrackEncryptionBox.java */
/* loaded from: classes3.dex */
public final class n {
    public final boolean a;
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public final String f1227b;
    public final w.a c;
    public final int d;
    @Nullable
    public final byte[] e;

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Code restructure failed: missing block: B:23:0x0051, code lost:
        if (r6.equals("cbc1") == false) goto L24;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public n(boolean r5, @androidx.annotation.Nullable java.lang.String r6, int r7, byte[] r8, int r9, int r10, @androidx.annotation.Nullable byte[] r11) {
        /*
            r4 = this;
            r4.<init>()
            r0 = 0
            r1 = 1
            if (r7 != 0) goto L9
            r2 = 1
            goto La
        L9:
            r2 = 0
        La:
            if (r11 != 0) goto Le
            r3 = 1
            goto Lf
        Le:
            r3 = 0
        Lf:
            r2 = r2 ^ r3
            b.c.a.a0.d.j(r2)
            r4.a = r5
            r4.f1227b = r6
            r4.d = r7
            r4.e = r11
            b.i.a.c.x2.w$a r5 = new b.i.a.c.x2.w$a
            r7 = 2
            if (r6 != 0) goto L21
            goto L6d
        L21:
            int r11 = r6.hashCode()
            r2 = 3
            switch(r11) {
                case 3046605: goto L4b;
                case 3046671: goto L40;
                case 3049879: goto L35;
                case 3049895: goto L2a;
                default: goto L29;
            }
        L29:
            goto L53
        L2a:
            java.lang.String r11 = "cens"
            boolean r11 = r6.equals(r11)
            if (r11 != 0) goto L33
            goto L53
        L33:
            r0 = 3
            goto L54
        L35:
            java.lang.String r11 = "cenc"
            boolean r11 = r6.equals(r11)
            if (r11 != 0) goto L3e
            goto L53
        L3e:
            r0 = 2
            goto L54
        L40:
            java.lang.String r11 = "cbcs"
            boolean r11 = r6.equals(r11)
            if (r11 != 0) goto L49
            goto L53
        L49:
            r0 = 1
            goto L54
        L4b:
            java.lang.String r11 = "cbc1"
            boolean r11 = r6.equals(r11)
            if (r11 != 0) goto L54
        L53:
            r0 = -1
        L54:
            if (r0 == 0) goto L6c
            if (r0 == r1) goto L6c
            if (r0 == r7) goto L6d
            if (r0 == r2) goto L6d
            int r7 = r6.length()
            int r7 = r7 + 68
            java.lang.String r11 = "Unsupported protection scheme type '"
            java.lang.String r0 = "'. Assuming AES-CTR crypto mode."
            java.lang.String r2 = "TrackEncryptionBox"
            b.d.b.a.a.g0(r7, r11, r6, r0, r2)
            goto L6d
        L6c:
            r1 = 2
        L6d:
            r5.<init>(r1, r8, r9, r10)
            r4.c = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.i0.n.<init>(boolean, java.lang.String, int, byte[], int, int, byte[]):void");
    }
}
