package b.i.a.c.x2.i0;

import b.c.a.a0.d;
import b.i.a.c.f3.e0;
/* compiled from: TrackSampleTable.java */
/* loaded from: classes3.dex */
public final class p {
    public final m a;

    /* renamed from: b  reason: collision with root package name */
    public final int f1229b;
    public final long[] c;
    public final int[] d;
    public final int e;
    public final long[] f;
    public final int[] g;
    public final long h;

    public p(m mVar, long[] jArr, int[] iArr, int i, long[] jArr2, int[] iArr2, long j) {
        boolean z2 = false;
        d.j(iArr.length == jArr2.length);
        d.j(jArr.length == jArr2.length);
        d.j(iArr2.length == jArr2.length ? true : z2);
        this.a = mVar;
        this.c = jArr;
        this.d = iArr;
        this.e = i;
        this.f = jArr2;
        this.g = iArr2;
        this.h = j;
        this.f1229b = jArr.length;
        if (iArr2.length > 0) {
            int length = iArr2.length - 1;
            iArr2[length] = iArr2[length] | 536870912;
        }
    }

    public int a(long j) {
        for (int e = e0.e(this.f, j, true, false); e >= 0; e--) {
            if ((this.g[e] & 1) != 0) {
                return e;
            }
        }
        return -1;
    }

    public int b(long j) {
        for (int b2 = e0.b(this.f, j, true, false); b2 < this.f.length; b2++) {
            if ((this.g[b2] & 1) != 0) {
                return b2;
            }
        }
        return -1;
    }
}
