package b.i.a.c.x2.i0;

import android.util.Log;
import android.util.Pair;
import androidx.annotation.Nullable;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.t;
import b.i.a.c.f3.x;
import b.i.a.c.j1;
import b.i.a.c.x2.i0.d;
import com.google.android.exoplayer2.ParserException;
import org.objectweb.asm.Opcodes;
/* compiled from: AtomParsers.java */
/* loaded from: classes3.dex */
public final class e {
    public static final byte[] a = e0.w("OpusHead");

    /* compiled from: AtomParsers.java */
    /* loaded from: classes3.dex */
    public interface a {
        int a();

        int b();

        int c();
    }

    /* compiled from: AtomParsers.java */
    /* loaded from: classes3.dex */
    public static final class b {
        public final n[] a;
        @Nullable

        /* renamed from: b  reason: collision with root package name */
        public j1 f1207b;
        public int c;
        public int d = 0;

        public b(int i) {
            this.a = new n[i];
        }
    }

    /* compiled from: AtomParsers.java */
    /* loaded from: classes3.dex */
    public static final class c implements a {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final int f1208b;
        public final x c;

        public c(d.b bVar, j1 j1Var) {
            x xVar = bVar.f1206b;
            this.c = xVar;
            xVar.E(12);
            int w = xVar.w();
            if ("audio/raw".equals(j1Var.w)) {
                int s2 = e0.s(j1Var.L, j1Var.J);
                if (w == 0 || w % s2 != 0) {
                    Log.w("AtomParsers", b.d.b.a.a.g(88, "Audio sample size mismatch. stsd sample size: ", s2, ", stsz sample size: ", w));
                    w = s2;
                }
            }
            this.a = w == 0 ? -1 : w;
            this.f1208b = xVar.w();
        }

        @Override // b.i.a.c.x2.i0.e.a
        public int a() {
            return this.a;
        }

        @Override // b.i.a.c.x2.i0.e.a
        public int b() {
            return this.f1208b;
        }

        @Override // b.i.a.c.x2.i0.e.a
        public int c() {
            int i = this.a;
            return i == -1 ? this.c.w() : i;
        }
    }

    /* compiled from: AtomParsers.java */
    /* loaded from: classes3.dex */
    public static final class d implements a {
        public final x a;

        /* renamed from: b  reason: collision with root package name */
        public final int f1209b;
        public final int c;
        public int d;
        public int e;

        public d(d.b bVar) {
            x xVar = bVar.f1206b;
            this.a = xVar;
            xVar.E(12);
            this.c = xVar.w() & 255;
            this.f1209b = xVar.w();
        }

        @Override // b.i.a.c.x2.i0.e.a
        public int a() {
            return -1;
        }

        @Override // b.i.a.c.x2.i0.e.a
        public int b() {
            return this.f1209b;
        }

        @Override // b.i.a.c.x2.i0.e.a
        public int c() {
            int i = this.c;
            if (i == 8) {
                return this.a.t();
            }
            if (i == 16) {
                return this.a.y();
            }
            int i2 = this.d;
            this.d = i2 + 1;
            if (i2 % 2 != 0) {
                return this.e & 15;
            }
            int t = this.a.t();
            this.e = t;
            return (t & 240) >> 4;
        }
    }

    public static Pair<String, byte[]> a(x xVar, int i) {
        xVar.E(i + 8 + 4);
        xVar.F(1);
        b(xVar);
        xVar.F(2);
        int t = xVar.t();
        if ((t & 128) != 0) {
            xVar.F(2);
        }
        if ((t & 64) != 0) {
            xVar.F(xVar.y());
        }
        if ((t & 32) != 0) {
            xVar.F(2);
        }
        xVar.F(1);
        b(xVar);
        String d2 = t.d(xVar.t());
        if ("audio/mpeg".equals(d2) || "audio/vnd.dts".equals(d2) || "audio/vnd.dts.hd".equals(d2)) {
            return Pair.create(d2, null);
        }
        xVar.F(12);
        xVar.F(1);
        int b2 = b(xVar);
        byte[] bArr = new byte[b2];
        System.arraycopy(xVar.a, xVar.f980b, bArr, 0, b2);
        xVar.f980b += b2;
        return Pair.create(d2, bArr);
    }

    public static int b(x xVar) {
        int t = xVar.t();
        int i = t & Opcodes.LAND;
        while ((t & 128) == 128) {
            t = xVar.t();
            i = (i << 7) | (t & Opcodes.LAND);
        }
        return i;
    }

    @Nullable
    public static Pair<Integer, n> c(x xVar, int i, int i2) throws ParserException {
        Pair<Integer, n> pair;
        Integer num;
        n nVar;
        int i3;
        int i4;
        byte[] bArr;
        int i5 = xVar.f980b;
        while (i5 - i < i2) {
            xVar.E(i5);
            int f = xVar.f();
            int i6 = 1;
            b.c.a.a0.d.q(f > 0, "childAtomSize must be positive");
            if (xVar.f() == 1936289382) {
                int i7 = i5 + 8;
                int i8 = -1;
                int i9 = 0;
                String str = null;
                Integer num2 = null;
                while (i7 - i5 < f) {
                    xVar.E(i7);
                    int f2 = xVar.f();
                    int f3 = xVar.f();
                    if (f3 == 1718775137) {
                        num2 = Integer.valueOf(xVar.f());
                    } else if (f3 == 1935894637) {
                        xVar.F(4);
                        str = xVar.q(4);
                    } else if (f3 == 1935894633) {
                        i8 = i7;
                        i9 = f2;
                    }
                    i7 += f2;
                }
                if ("cenc".equals(str) || "cbc1".equals(str) || "cens".equals(str) || "cbcs".equals(str)) {
                    b.c.a.a0.d.q(num2 != null, "frma atom is mandatory");
                    b.c.a.a0.d.q(i8 != -1, "schi atom is mandatory");
                    int i10 = i8 + 8;
                    while (true) {
                        if (i10 - i8 >= i9) {
                            num = num2;
                            nVar = null;
                            break;
                        }
                        xVar.E(i10);
                        int f4 = xVar.f();
                        if (xVar.f() == 1952804451) {
                            int f5 = (xVar.f() >> 24) & 255;
                            xVar.F(i6);
                            if (f5 == 0) {
                                xVar.F(i6);
                                i4 = 0;
                                i3 = 0;
                            } else {
                                int t = xVar.t();
                                i3 = (t & 240) >> 4;
                                i4 = t & 15;
                            }
                            boolean z2 = xVar.t() == i6;
                            int t2 = xVar.t();
                            byte[] bArr2 = new byte[16];
                            System.arraycopy(xVar.a, xVar.f980b, bArr2, 0, 16);
                            xVar.f980b += 16;
                            if (!z2 || t2 != 0) {
                                bArr = null;
                            } else {
                                int t3 = xVar.t();
                                byte[] bArr3 = new byte[t3];
                                System.arraycopy(xVar.a, xVar.f980b, bArr3, 0, t3);
                                xVar.f980b += t3;
                                bArr = bArr3;
                            }
                            num = num2;
                            nVar = new n(z2, str, t2, bArr2, i3, i4, bArr);
                        } else {
                            i10 += f4;
                            i6 = 1;
                        }
                    }
                    b.c.a.a0.d.q(nVar != null, "tenc atom is mandatory");
                    int i11 = e0.a;
                    pair = Pair.create(num, nVar);
                } else {
                    pair = null;
                }
                if (pair != null) {
                    return pair;
                }
            }
            i5 += f;
        }
        return null;
    }

    /* JADX WARN: Code restructure failed: missing block: B:491:0x0a3f, code lost:
        if (r21 == null) goto L497;
     */
    /* JADX WARN: Removed duplicated region for block: B:224:0x0374  */
    /* JADX WARN: Removed duplicated region for block: B:311:0x061e  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static b.i.a.c.x2.i0.e.b d(b.i.a.c.f3.x r42, int r43, int r44, java.lang.String r45, @androidx.annotation.Nullable com.google.android.exoplayer2.drm.DrmInitData r46, boolean r47) throws com.google.android.exoplayer2.ParserException {
        /*
            Method dump skipped, instructions count: 2669
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.i0.e.d(b.i.a.c.f3.x, int, int, java.lang.String, com.google.android.exoplayer2.drm.DrmInitData, boolean):b.i.a.c.x2.i0.e$b");
    }

    /* JADX WARN: Removed duplicated region for block: B:112:0x0277  */
    /* JADX WARN: Removed duplicated region for block: B:117:0x0292  */
    /* JADX WARN: Removed duplicated region for block: B:120:0x02c5  */
    /* JADX WARN: Removed duplicated region for block: B:170:0x03f3  */
    /* JADX WARN: Removed duplicated region for block: B:195:0x04a2  */
    /* JADX WARN: Removed duplicated region for block: B:266:0x0652  */
    /* JADX WARN: Removed duplicated region for block: B:267:0x0664  */
    /* JADX WARN: Removed duplicated region for block: B:71:0x0136  */
    /* JADX WARN: Removed duplicated region for block: B:72:0x0139  */
    /* JADX WARN: Removed duplicated region for block: B:75:0x014c  */
    /* JADX WARN: Removed duplicated region for block: B:76:0x014f  */
    /* JADX WARN: Removed duplicated region for block: B:79:0x0162  */
    /* JADX WARN: Removed duplicated region for block: B:82:0x019c  */
    /* JADX WARN: Removed duplicated region for block: B:83:0x019f  */
    /* JADX WARN: Removed duplicated region for block: B:86:0x01aa  */
    /* JADX WARN: Removed duplicated region for block: B:87:0x01ac  */
    /* JADX WARN: Removed duplicated region for block: B:94:0x0213  */
    /* JADX WARN: Removed duplicated region for block: B:95:0x0215  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static java.util.List<b.i.a.c.x2.i0.p> e(b.i.a.c.x2.i0.d.a r42, b.i.a.c.x2.p r43, long r44, @androidx.annotation.Nullable com.google.android.exoplayer2.drm.DrmInitData r46, boolean r47, boolean r48, b.i.b.a.e<b.i.a.c.x2.i0.m, b.i.a.c.x2.i0.m> r49) throws com.google.android.exoplayer2.ParserException {
        /*
            Method dump skipped, instructions count: 2270
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.i0.e.e(b.i.a.c.x2.i0.d$a, b.i.a.c.x2.p, long, com.google.android.exoplayer2.drm.DrmInitData, boolean, boolean, b.i.b.a.e):java.util.List");
    }
}
