package b.i.a.c.x2;

import b.i.a.c.x2.t;
/* compiled from: ConstantBitrateSeekMap.java */
/* loaded from: classes3.dex */
public class d implements t {
    public final long a;

    /* renamed from: b  reason: collision with root package name */
    public final long f1163b;
    public final int c;
    public final long d;
    public final int e;
    public final long f;
    public final boolean g;

    public d(long j, long j2, int i, int i2, boolean z2) {
        this.a = j;
        this.f1163b = j2;
        this.c = i2 == -1 ? 1 : i2;
        this.e = i;
        this.g = z2;
        if (j == -1) {
            this.d = -1L;
            this.f = -9223372036854775807L;
            return;
        }
        this.d = j - j2;
        this.f = e(j, j2, i);
    }

    public static long e(long j, long j2, int i) {
        return ((Math.max(0L, j - j2) * 8) * 1000000) / i;
    }

    public long b(long j) {
        return e(j, this.f1163b, this.e);
    }

    @Override // b.i.a.c.x2.t
    public boolean c() {
        return this.d != -1 || this.g;
    }

    @Override // b.i.a.c.x2.t
    public t.a h(long j) {
        long j2 = this.d;
        if (j2 == -1 && !this.g) {
            return new t.a(new u(0L, this.f1163b));
        }
        long j3 = this.c;
        long j4 = (((this.e * j) / 8000000) / j3) * j3;
        if (j2 != -1) {
            j4 = Math.min(j4, j2 - j3);
        }
        long max = this.f1163b + Math.max(j4, 0L);
        long b2 = b(max);
        u uVar = new u(b2, max);
        if (this.d != -1 && b2 < j) {
            int i = this.c;
            if (i + max < this.a) {
                long j5 = max + i;
                return new t.a(uVar, new u(b(j5), j5));
            }
        }
        return new t.a(uVar);
    }

    @Override // b.i.a.c.x2.t
    public long i() {
        return this.f;
    }
}
