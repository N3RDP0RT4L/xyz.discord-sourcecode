package b.i.a.c.x2;

import b.i.a.c.f3.x;
import org.objectweb.asm.Opcodes;
/* compiled from: FlacFrameReader.java */
/* loaded from: classes3.dex */
public final class m {

    /* compiled from: FlacFrameReader.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public long a;
    }

    public static boolean a(x xVar, o oVar, boolean z2, a aVar) {
        try {
            long z3 = xVar.z();
            if (!z2) {
                z3 *= oVar.f1288b;
            }
            aVar.a = z3;
            return true;
        } catch (NumberFormatException unused) {
            return false;
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:45:0x008e, code lost:
        if (r7 == r18.f) goto L56;
     */
    /* JADX WARN: Code restructure failed: missing block: B:49:0x009b, code lost:
        if ((r17.t() * 1000) == r3) goto L56;
     */
    /* JADX WARN: Code restructure failed: missing block: B:55:0x00aa, code lost:
        if (r4 == r3) goto L56;
     */
    /* JADX WARN: Removed duplicated region for block: B:59:0x00b1  */
    /* JADX WARN: Removed duplicated region for block: B:73:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static boolean b(b.i.a.c.f3.x r17, b.i.a.c.x2.o r18, int r19, b.i.a.c.x2.m.a r20) {
        /*
            Method dump skipped, instructions count: 212
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.m.b(b.i.a.c.f3.x, b.i.a.c.x2.o, int, b.i.a.c.x2.m$a):boolean");
    }

    public static int c(x xVar, int i) {
        switch (i) {
            case 1:
                return Opcodes.CHECKCAST;
            case 2:
            case 3:
            case 4:
            case 5:
                return 576 << (i - 2);
            case 6:
                return xVar.t() + 1;
            case 7:
                return xVar.y() + 1;
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
                return 256 << (i - 8);
            default:
                return -1;
        }
    }
}
