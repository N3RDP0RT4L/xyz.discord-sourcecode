package b.i.a.c.x2;

import b.d.b.a.a;
import b.i.a.c.f3.e0;
import b.i.a.c.x2.t;
import java.util.Arrays;
/* compiled from: ChunkIndex.java */
/* loaded from: classes3.dex */
public final class c implements t {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final int[] f1160b;
    public final long[] c;
    public final long[] d;
    public final long[] e;
    public final long f;

    public c(int[] iArr, long[] jArr, long[] jArr2, long[] jArr3) {
        this.f1160b = iArr;
        this.c = jArr;
        this.d = jArr2;
        this.e = jArr3;
        int length = iArr.length;
        this.a = length;
        if (length > 0) {
            this.f = jArr2[length - 1] + jArr3[length - 1];
        } else {
            this.f = 0L;
        }
    }

    @Override // b.i.a.c.x2.t
    public boolean c() {
        return true;
    }

    @Override // b.i.a.c.x2.t
    public t.a h(long j) {
        int e = e0.e(this.e, j, true, true);
        long[] jArr = this.e;
        long j2 = jArr[e];
        long[] jArr2 = this.c;
        u uVar = new u(j2, jArr2[e]);
        if (j2 >= j || e == this.a - 1) {
            return new t.a(uVar);
        }
        int i = e + 1;
        return new t.a(uVar, new u(jArr[i], jArr2[i]));
    }

    @Override // b.i.a.c.x2.t
    public long i() {
        return this.f;
    }

    public String toString() {
        int i = this.a;
        String arrays = Arrays.toString(this.f1160b);
        String arrays2 = Arrays.toString(this.c);
        String arrays3 = Arrays.toString(this.e);
        String arrays4 = Arrays.toString(this.d);
        StringBuilder sb = new StringBuilder(a.b(arrays4, a.b(arrays3, a.b(arrays2, a.b(arrays, 71)))));
        sb.append("ChunkIndex(length=");
        sb.append(i);
        sb.append(", sizes=");
        sb.append(arrays);
        a.q0(sb, ", offsets=", arrays2, ", timeUs=", arrays3);
        return a.J(sb, ", durationsUs=", arrays4, ")");
    }
}
