package b.i.a.c.x2;

import androidx.annotation.Nullable;
import b.d.b.a.a;
/* compiled from: SeekPoint.java */
/* loaded from: classes3.dex */
public final class u {
    public static final u a = new u(0, 0);

    /* renamed from: b  reason: collision with root package name */
    public final long f1294b;
    public final long c;

    public u(long j, long j2) {
        this.f1294b = j;
        this.c = j2;
    }

    public boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || u.class != obj.getClass()) {
            return false;
        }
        u uVar = (u) obj;
        return this.f1294b == uVar.f1294b && this.c == uVar.c;
    }

    public int hashCode() {
        return (((int) this.f1294b) * 31) + ((int) this.c);
    }

    public String toString() {
        long j = this.f1294b;
        return a.B(a.P(60, "[timeUs=", j, ", position="), this.c, "]");
    }
}
