package b.i.a.c.x2;

import b.c.a.a0.d;
import b.i.a.c.f3.e0;
import b.i.a.c.x2.o;
import b.i.a.c.x2.t;
/* compiled from: FlacSeekTableSeekMap.java */
/* loaded from: classes3.dex */
public final class n implements t {
    public final o a;

    /* renamed from: b  reason: collision with root package name */
    public final long f1287b;

    public n(o oVar, long j) {
        this.a = oVar;
        this.f1287b = j;
    }

    public final u b(long j, long j2) {
        return new u((j * 1000000) / this.a.e, this.f1287b + j2);
    }

    @Override // b.i.a.c.x2.t
    public boolean c() {
        return true;
    }

    @Override // b.i.a.c.x2.t
    public t.a h(long j) {
        d.H(this.a.k);
        o oVar = this.a;
        o.a aVar = oVar.k;
        long[] jArr = aVar.a;
        long[] jArr2 = aVar.f1289b;
        int e = e0.e(jArr, oVar.g(j), true, false);
        long j2 = 0;
        long j3 = e == -1 ? 0L : jArr[e];
        if (e != -1) {
            j2 = jArr2[e];
        }
        u b2 = b(j3, j2);
        if (b2.f1294b == j || e == jArr.length - 1) {
            return new t.a(b2);
        }
        int i = e + 1;
        return new t.a(b2, b(jArr[i], jArr2[i]));
    }

    @Override // b.i.a.c.x2.t
    public long i() {
        return this.a.d();
    }
}
