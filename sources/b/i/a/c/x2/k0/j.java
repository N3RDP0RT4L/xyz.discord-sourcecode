package b.i.a.c.x2.k0;

import b.i.a.c.f3.w;
import b.i.a.c.f3.x;
import b.i.a.c.x2.h;
import b.i.a.c.x2.i;
import b.i.a.c.x2.k0.i0;
import java.io.IOException;
/* compiled from: AdtsExtractor.java */
/* loaded from: classes3.dex */
public final class j implements h {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final k f1256b;
    public final x c;
    public final x d;
    public final w e;
    public b.i.a.c.x2.j f;
    public long g;
    public long h;
    public int i;
    public boolean j;
    public boolean k;
    public boolean l;

    static {
        c cVar = c.a;
    }

    public j(int i) {
        this.a = (i & 2) != 0 ? i | 1 : i;
        this.f1256b = new k(true, null);
        this.c = new x(2048);
        this.i = -1;
        this.h = -1L;
        x xVar = new x(10);
        this.d = xVar;
        this.e = new w(xVar.a);
    }

    public final int a(i iVar) throws IOException {
        int i = 0;
        while (true) {
            iVar.o(this.d.a, 0, 10);
            this.d.E(0);
            if (this.d.v() != 4801587) {
                break;
            }
            this.d.F(3);
            int s2 = this.d.s();
            i += s2 + 10;
            iVar.g(s2);
        }
        iVar.k();
        iVar.g(i);
        if (this.h == -1) {
            this.h = i;
        }
        return i;
    }

    @Override // b.i.a.c.x2.h
    public boolean b(i iVar) throws IOException {
        int a = a(iVar);
        int i = a;
        int i2 = 0;
        int i3 = 0;
        do {
            iVar.o(this.d.a, 0, 2);
            this.d.E(0);
            if (!k.g(this.d.y())) {
                i++;
                iVar.k();
                iVar.g(i);
            } else {
                i2++;
                if (i2 >= 4 && i3 > 188) {
                    return true;
                }
                iVar.o(this.d.a, 0, 4);
                this.e.k(14);
                int g = this.e.g(13);
                if (g <= 6) {
                    i++;
                    iVar.k();
                    iVar.g(i);
                } else {
                    iVar.g(g - 6);
                    i3 += g;
                }
            }
            i2 = 0;
            i3 = 0;
        } while (i - a < 8192);
        return false;
    }

    /* JADX WARN: Code restructure failed: missing block: B:36:0x0092, code lost:
        r18.j = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:37:0x009b, code lost:
        throw com.google.android.exoplayer2.ParserException.a("Malformed ADTS stream", null);
     */
    @Override // b.i.a.c.x2.h
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public int e(b.i.a.c.x2.i r19, b.i.a.c.x2.s r20) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 314
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.k0.j.e(b.i.a.c.x2.i, b.i.a.c.x2.s):int");
    }

    @Override // b.i.a.c.x2.h
    public void f(b.i.a.c.x2.j jVar) {
        this.f = jVar;
        this.f1256b.e(jVar, new i0.d(Integer.MIN_VALUE, 0, 1));
        jVar.j();
    }

    @Override // b.i.a.c.x2.h
    public void g(long j, long j2) {
        this.k = false;
        this.f1256b.c();
        this.g = j2;
    }

    @Override // b.i.a.c.x2.h
    public void release() {
    }
}
