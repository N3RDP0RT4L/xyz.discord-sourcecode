package b.i.a.c.x2.k0;

import android.util.SparseArray;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.j1;
import b.i.a.c.x2.k0.i0;
import b.i.b.a.c;
import b.i.b.b.a;
import b.i.b.b.h0;
import b.i.b.b.p;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.objectweb.asm.Opcodes;
/* compiled from: DefaultTsPayloadReaderFactory.java */
/* loaded from: classes3.dex */
public final class l implements i0.c {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final List<j1> f1260b;

    public l(int i) {
        a<Object> aVar = p.k;
        p<Object> pVar = h0.l;
        this.a = i;
        this.f1260b = pVar;
    }

    @Override // b.i.a.c.x2.k0.i0.c
    @Nullable
    public i0 a(int i, i0.b bVar) {
        if (i != 2) {
            if (i == 3 || i == 4) {
                return new y(new v(bVar.f1254b));
            }
            if (i == 21) {
                return new y(new t());
            }
            if (i != 27) {
                if (i == 36) {
                    return new y(new s(new e0(c(bVar))));
                }
                if (i == 89) {
                    return new y(new n(bVar.c));
                }
                if (i != 138) {
                    if (i == 172) {
                        return new y(new i(bVar.f1254b));
                    }
                    if (i == 257) {
                        return new d0(new x("application/vnd.dvb.ait"));
                    }
                    if (i != 134) {
                        if (i != 135) {
                            switch (i) {
                                case 15:
                                    if (d(2)) {
                                        return null;
                                    }
                                    return new y(new k(false, bVar.f1254b));
                                case 16:
                                    return new y(new q(new j0(c(bVar))));
                                case 17:
                                    if (d(2)) {
                                        return null;
                                    }
                                    return new y(new u(bVar.f1254b));
                                default:
                                    switch (i) {
                                        case 128:
                                            break;
                                        case Opcodes.LOR /* 129 */:
                                            break;
                                        case 130:
                                            if (!d(64)) {
                                                return null;
                                            }
                                            break;
                                        default:
                                            return null;
                                    }
                            }
                        }
                        return new y(new g(bVar.f1254b));
                    } else if (d(16)) {
                        return null;
                    } else {
                        return new d0(new x("application/x-scte35"));
                    }
                }
                return new y(new m(bVar.f1254b));
            } else if (d(4)) {
                return null;
            } else {
                return new y(new r(new e0(c(bVar)), d(1), d(8)));
            }
        }
        return new y(new p(new j0(c(bVar))));
    }

    public SparseArray<i0> b() {
        return new SparseArray<>();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v2 */
    public final List<j1> c(i0.b bVar) {
        String str;
        int i;
        if (d(32)) {
            return this.f1260b;
        }
        byte[] bArr = bVar.d;
        int length = bArr.length;
        int i2 = 0;
        ArrayList arrayList = this.f1260b;
        while (length - i2 > 0) {
            int i3 = i2 + 1;
            int i4 = bArr[i2] & 255;
            int i5 = i3 + 1;
            int i6 = (bArr[i3] & 255) + i5;
            boolean z2 = true;
            if (i4 == 134) {
                arrayList = new ArrayList();
                int i7 = i5 + 1;
                int i8 = bArr[i5] & 255 & 31;
                int i9 = 0;
                while (i9 < i8) {
                    String str2 = new String(bArr, i7, 3, c.c);
                    int i10 = i7 + 3;
                    int i11 = i10 + 1;
                    int i12 = bArr[i10] & 255;
                    boolean z3 = (i12 & 128) != 0;
                    if (z3) {
                        i = i12 & 63;
                        str = "application/cea-708";
                    } else {
                        str = "application/cea-608";
                        i = 1;
                    }
                    int i13 = i11 + 1;
                    byte b2 = (byte) (bArr[i11] & 255);
                    int i14 = i13 + 1;
                    d.j(i14 >= 0 && i14 <= length);
                    List<byte[]> list = null;
                    if (z3) {
                        list = Collections.singletonList((b2 & 64) != 0 ? new byte[]{1} : new byte[]{0});
                    }
                    j1.b bVar2 = new j1.b();
                    bVar2.k = str;
                    bVar2.c = str2;
                    bVar2.C = i;
                    bVar2.m = list;
                    arrayList.add(bVar2.a());
                    i9++;
                    i7 = i14;
                }
            }
            if (i6 < 0 || i6 > length) {
                z2 = false;
            }
            d.j(z2);
            i2 = i6;
            arrayList = arrayList;
        }
        return arrayList;
    }

    public final boolean d(int i) {
        return (i & this.a) != 0;
    }
}
