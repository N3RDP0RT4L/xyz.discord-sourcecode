package b.i.a.c.x2.k0;

import androidx.annotation.Nullable;
import b.i.a.c.f3.u;
import b.i.a.c.f3.x;
import b.i.a.c.x2.j;
import b.i.a.c.x2.k0.i0;
import b.i.a.c.x2.w;
import java.util.Arrays;
import org.objectweb.asm.Opcodes;
/* compiled from: H263Reader.java */
/* loaded from: classes3.dex */
public final class q implements o {
    public static final float[] a = {1.0f, 1.0f, 1.0909091f, 0.90909094f, 1.4545455f, 1.2121212f, 1.0f};
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public final j0 f1265b;
    public b g;
    public long h;
    public String i;
    public w j;
    public boolean k;
    public final boolean[] d = new boolean[4];
    public final a e = new a(128);
    public long l = -9223372036854775807L;
    @Nullable
    public final w f = new w(Opcodes.GETSTATIC, 128);
    @Nullable
    public final x c = new x();

    /* compiled from: H263Reader.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public static final byte[] a = {0, 0, 1};

        /* renamed from: b  reason: collision with root package name */
        public boolean f1266b;
        public int c;
        public int d;
        public int e;
        public byte[] f;

        public a(int i) {
            this.f = new byte[i];
        }

        public void a(byte[] bArr, int i, int i2) {
            if (this.f1266b) {
                int i3 = i2 - i;
                byte[] bArr2 = this.f;
                int length = bArr2.length;
                int i4 = this.d;
                if (length < i4 + i3) {
                    this.f = Arrays.copyOf(bArr2, (i4 + i3) * 2);
                }
                System.arraycopy(bArr, i, this.f, this.d, i3);
                this.d += i3;
            }
        }

        public void b() {
            this.f1266b = false;
            this.d = 0;
            this.c = 0;
        }
    }

    /* compiled from: H263Reader.java */
    /* loaded from: classes3.dex */
    public static final class b {
        public final w a;

        /* renamed from: b  reason: collision with root package name */
        public boolean f1267b;
        public boolean c;
        public boolean d;
        public int e;
        public int f;
        public long g;
        public long h;

        public b(w wVar) {
            this.a = wVar;
        }

        public void a(byte[] bArr, int i, int i2) {
            if (this.c) {
                int i3 = this.f;
                int i4 = (i + 1) - i3;
                if (i4 < i2) {
                    this.d = ((bArr[i4] & 192) >> 6) == 0;
                    this.c = false;
                    return;
                }
                this.f = (i2 - i) + i3;
            }
        }
    }

    public q(@Nullable j0 j0Var) {
        this.f1265b = j0Var;
    }

    /* JADX WARN: Removed duplicated region for block: B:100:0x023e A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:105:0x024f  */
    /* JADX WARN: Removed duplicated region for block: B:108:0x026d  */
    /* JADX WARN: Removed duplicated region for block: B:111:0x027a  */
    /* JADX WARN: Removed duplicated region for block: B:117:0x0286  */
    /* JADX WARN: Removed duplicated region for block: B:118:0x0288  */
    /* JADX WARN: Removed duplicated region for block: B:52:0x00d2  */
    /* JADX WARN: Removed duplicated region for block: B:68:0x0140  */
    /* JADX WARN: Removed duplicated region for block: B:73:0x017b  */
    /* JADX WARN: Removed duplicated region for block: B:76:0x0192  */
    /* JADX WARN: Removed duplicated region for block: B:86:0x01ec  */
    @Override // b.i.a.c.x2.k0.o
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void b(b.i.a.c.f3.x r26) {
        /*
            Method dump skipped, instructions count: 662
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.k0.q.b(b.i.a.c.f3.x):void");
    }

    @Override // b.i.a.c.x2.k0.o
    public void c() {
        u.a(this.d);
        this.e.b();
        b bVar = this.g;
        if (bVar != null) {
            bVar.f1267b = false;
            bVar.c = false;
            bVar.d = false;
            bVar.e = -1;
        }
        w wVar = this.f;
        if (wVar != null) {
            wVar.c();
        }
        this.h = 0L;
        this.l = -9223372036854775807L;
    }

    @Override // b.i.a.c.x2.k0.o
    public void d() {
    }

    @Override // b.i.a.c.x2.k0.o
    public void e(j jVar, i0.d dVar) {
        dVar.a();
        this.i = dVar.b();
        w p = jVar.p(dVar.c(), 2);
        this.j = p;
        this.g = new b(p);
        j0 j0Var = this.f1265b;
        if (j0Var != null) {
            j0Var.b(jVar, dVar);
        }
    }

    @Override // b.i.a.c.x2.k0.o
    public void f(long j, int i) {
        if (j != -9223372036854775807L) {
            this.l = j;
        }
    }
}
