package b.i.a.c.x2.k0;

import androidx.annotation.Nullable;
import b.i.a.c.f3.w;
import b.i.a.c.f3.x;
import b.i.a.c.j1;
import b.i.a.c.t2.l;
import b.i.a.c.x2.j;
import b.i.a.c.x2.k0.i0;
import com.google.android.exoplayer2.ParserException;
/* compiled from: LatmReader.java */
/* loaded from: classes3.dex */
public final class u implements o {
    @Nullable
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final x f1274b;
    public final w c;
    public b.i.a.c.x2.w d;
    public String e;
    public j1 f;
    public int g;
    public int h;
    public int i;
    public int j;
    public long k = -9223372036854775807L;
    public boolean l;
    public int m;
    public int n;
    public int o;
    public boolean p;
    public long q;
    public int r;

    /* renamed from: s  reason: collision with root package name */
    public long f1275s;
    public int t;
    @Nullable
    public String u;

    public u(@Nullable String str) {
        this.a = str;
        x xVar = new x(1024);
        this.f1274b = xVar;
        this.c = new w(xVar.a);
    }

    public static long a(w wVar) {
        return wVar.g((wVar.g(2) + 1) * 8);
    }

    /* JADX WARN: Code restructure failed: missing block: B:66:0x014c, code lost:
        if (r14.l == false) goto L86;
     */
    @Override // b.i.a.c.x2.k0.o
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void b(b.i.a.c.f3.x r15) throws com.google.android.exoplayer2.ParserException {
        /*
            Method dump skipped, instructions count: 542
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.k0.u.b(b.i.a.c.f3.x):void");
    }

    @Override // b.i.a.c.x2.k0.o
    public void c() {
        this.g = 0;
        this.k = -9223372036854775807L;
        this.l = false;
    }

    @Override // b.i.a.c.x2.k0.o
    public void d() {
    }

    @Override // b.i.a.c.x2.k0.o
    public void e(j jVar, i0.d dVar) {
        dVar.a();
        this.d = jVar.p(dVar.c(), 1);
        this.e = dVar.b();
    }

    @Override // b.i.a.c.x2.k0.o
    public void f(long j, int i) {
        if (j != -9223372036854775807L) {
            this.k = j;
        }
    }

    public final int g(w wVar) throws ParserException {
        int b2 = wVar.b();
        l.b b3 = l.b(wVar, true);
        this.u = b3.c;
        this.r = b3.a;
        this.t = b3.f1118b;
        return b2 - wVar.b();
    }
}
