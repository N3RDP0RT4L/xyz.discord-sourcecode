package b.i.a.c.x2.k0;

import android.util.Log;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.util.SparseIntArray;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.f3.d0;
import b.i.a.c.f3.w;
import b.i.a.c.f3.x;
import b.i.a.c.x2.h;
import b.i.a.c.x2.i;
import b.i.a.c.x2.j;
import b.i.a.c.x2.k0.i0;
import b.i.a.c.x2.s;
import b.i.a.c.x2.t;
import com.google.android.exoplayer2.ParserException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.objectweb.asm.Opcodes;
/* compiled from: TsExtractor.java */
/* loaded from: classes3.dex */
public final class h0 implements h {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final int f1248b;
    public final List<d0> c;
    public final x d;
    public final SparseIntArray e;
    public final i0.c f;
    public final SparseArray<i0> g;
    public final SparseBooleanArray h;
    public final SparseBooleanArray i;
    public final g0 j;
    public f0 k;
    public j l;
    public int m;
    public boolean n;
    public boolean o;
    public boolean p;
    @Nullable
    public i0 q;
    public int r;

    /* renamed from: s  reason: collision with root package name */
    public int f1249s;

    /* compiled from: TsExtractor.java */
    /* loaded from: classes3.dex */
    public class a implements c0 {
        public final w a = new w(new byte[4]);

        public a() {
        }

        @Override // b.i.a.c.x2.k0.c0
        public void a(d0 d0Var, j jVar, i0.d dVar) {
        }

        @Override // b.i.a.c.x2.k0.c0
        public void b(x xVar) {
            if (xVar.t() == 0 && (xVar.t() & 128) != 0) {
                xVar.F(6);
                int a = xVar.a() / 4;
                for (int i = 0; i < a; i++) {
                    xVar.d(this.a, 4);
                    int g = this.a.g(16);
                    this.a.m(3);
                    if (g == 0) {
                        this.a.m(13);
                    } else {
                        int g2 = this.a.g(13);
                        if (h0.this.g.get(g2) == null) {
                            h0 h0Var = h0.this;
                            h0Var.g.put(g2, new d0(new b(g2)));
                            h0.this.m++;
                        }
                    }
                }
                h0 h0Var2 = h0.this;
                if (h0Var2.a != 2) {
                    h0Var2.g.remove(0);
                }
            }
        }
    }

    /* compiled from: TsExtractor.java */
    /* loaded from: classes3.dex */
    public class b implements c0 {
        public final w a = new w(new byte[5]);

        /* renamed from: b  reason: collision with root package name */
        public final SparseArray<i0> f1251b = new SparseArray<>();
        public final SparseIntArray c = new SparseIntArray();
        public final int d;

        public b(int i) {
            this.d = i;
        }

        @Override // b.i.a.c.x2.k0.c0
        public void a(d0 d0Var, j jVar, i0.d dVar) {
        }

        /* JADX WARN: Code restructure failed: missing block: B:50:0x013f, code lost:
            if (r24.t() == r13) goto L51;
         */
        /* JADX WARN: Removed duplicated region for block: B:91:0x0224  */
        /* JADX WARN: Removed duplicated region for block: B:93:0x0231  */
        @Override // b.i.a.c.x2.k0.c0
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public void b(b.i.a.c.f3.x r24) {
            /*
                Method dump skipped, instructions count: 753
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.k0.h0.b.b(b.i.a.c.f3.x):void");
        }
    }

    static {
        e eVar = e.a;
    }

    public h0(int i, int i2, int i3) {
        d0 d0Var = new d0(0L);
        l lVar = new l(i2);
        this.f = lVar;
        this.f1248b = i3;
        this.a = i;
        if (i == 1 || i == 2) {
            this.c = Collections.singletonList(d0Var);
        } else {
            ArrayList arrayList = new ArrayList();
            this.c = arrayList;
            arrayList.add(d0Var);
        }
        this.d = new x(new byte[9400], 0);
        SparseBooleanArray sparseBooleanArray = new SparseBooleanArray();
        this.h = sparseBooleanArray;
        this.i = new SparseBooleanArray();
        SparseArray<i0> sparseArray = new SparseArray<>();
        this.g = sparseArray;
        this.e = new SparseIntArray();
        this.j = new g0(i3);
        this.l = j.d;
        this.f1249s = -1;
        sparseBooleanArray.clear();
        sparseArray.clear();
        SparseArray<i0> b2 = lVar.b();
        int size = b2.size();
        for (int i4 = 0; i4 < size; i4++) {
            this.g.put(b2.keyAt(i4), b2.valueAt(i4));
        }
        this.g.put(0, new d0(new a()));
        this.q = null;
    }

    @Override // b.i.a.c.x2.h
    public boolean b(i iVar) throws IOException {
        boolean z2;
        byte[] bArr = this.d.a;
        iVar.o(bArr, 0, 940);
        for (int i = 0; i < 188; i++) {
            int i2 = 0;
            while (true) {
                if (i2 >= 5) {
                    z2 = true;
                    break;
                } else if (bArr[(i2 * Opcodes.NEWARRAY) + i] != 71) {
                    z2 = false;
                    break;
                } else {
                    i2++;
                }
            }
            if (z2) {
                iVar.l(i);
                return true;
            }
        }
        return false;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v1, types: [boolean, int] */
    /* JADX WARN: Type inference failed for: r0v2 */
    /* JADX WARN: Type inference failed for: r0v23 */
    /* JADX WARN: Type inference failed for: r0v24 */
    /* JADX WARN: Type inference failed for: r1v1, types: [boolean, int] */
    /* JADX WARN: Type inference failed for: r1v2 */
    /* JADX WARN: Type inference failed for: r1v3 */
    /* JADX WARN: Type inference failed for: r1v9 */
    @Override // b.i.a.c.x2.h
    public int e(i iVar, s sVar) throws IOException {
        ?? r1;
        ?? r0;
        boolean z2;
        int i;
        boolean z3;
        boolean z4;
        long b2 = iVar.b();
        int i2 = 1;
        if (this.n) {
            long j = -9223372036854775807L;
            if ((b2 == -1 || this.a == 2) ? false : true) {
                g0 g0Var = this.j;
                if (!g0Var.d) {
                    int i3 = this.f1249s;
                    if (i3 <= 0) {
                        g0Var.a(iVar);
                        return 0;
                    } else if (!g0Var.f) {
                        long b3 = iVar.b();
                        int min = (int) Math.min(g0Var.a, b3);
                        long j2 = b3 - min;
                        if (iVar.getPosition() != j2) {
                            sVar.a = j2;
                            return i2;
                        }
                        g0Var.c.A(min);
                        iVar.k();
                        iVar.o(g0Var.c.a, 0, min);
                        x xVar = g0Var.c;
                        int i4 = xVar.f980b;
                        int i5 = xVar.c;
                        int i6 = i5 - 188;
                        while (true) {
                            if (i6 < i4) {
                                break;
                            }
                            byte[] bArr = xVar.a;
                            int i7 = -4;
                            int i8 = 0;
                            while (true) {
                                if (i7 > 4) {
                                    z4 = false;
                                    break;
                                }
                                int i9 = (i7 * Opcodes.NEWARRAY) + i6;
                                if (i9 < i4 || i9 >= i5 || bArr[i9] != 71) {
                                    i8 = 0;
                                } else {
                                    i8++;
                                    if (i8 == 5) {
                                        z4 = true;
                                        break;
                                    }
                                }
                                i7++;
                            }
                            if (z4) {
                                long K1 = d.K1(xVar, i6, i3);
                                if (K1 != -9223372036854775807L) {
                                    j = K1;
                                    break;
                                }
                            }
                            i6--;
                        }
                        g0Var.h = j;
                        g0Var.f = true;
                        i2 = 0;
                        return i2;
                    } else if (g0Var.h == -9223372036854775807L) {
                        g0Var.a(iVar);
                        return 0;
                    } else if (!g0Var.e) {
                        int min2 = (int) Math.min(g0Var.a, iVar.b());
                        long j3 = 0;
                        if (iVar.getPosition() != j3) {
                            sVar.a = j3;
                            return i2;
                        }
                        g0Var.c.A(min2);
                        iVar.k();
                        iVar.o(g0Var.c.a, 0, min2);
                        x xVar2 = g0Var.c;
                        int i10 = xVar2.f980b;
                        int i11 = xVar2.c;
                        while (true) {
                            if (i10 >= i11) {
                                break;
                            }
                            if (xVar2.a[i10] == 71) {
                                long K12 = d.K1(xVar2, i10, i3);
                                if (K12 != -9223372036854775807L) {
                                    j = K12;
                                    break;
                                }
                            }
                            i10++;
                        }
                        g0Var.g = j;
                        g0Var.e = true;
                        i2 = 0;
                        return i2;
                    } else {
                        long j4 = g0Var.g;
                        if (j4 == -9223372036854775807L) {
                            g0Var.a(iVar);
                            return 0;
                        }
                        long b4 = g0Var.f1246b.b(g0Var.h) - g0Var.f1246b.b(j4);
                        g0Var.i = b4;
                        if (b4 < 0) {
                            Log.w("TsDurationReader", b.d.b.a.a.h(65, "Invalid duration: ", b4, ". Using TIME_UNSET instead."));
                            g0Var.i = -9223372036854775807L;
                        }
                        g0Var.a(iVar);
                        return 0;
                    }
                }
            }
            if (!this.o) {
                this.o = true;
                g0 g0Var2 = this.j;
                long j5 = g0Var2.i;
                if (j5 != -9223372036854775807L) {
                    f0 f0Var = new f0(g0Var2.f1246b, j5, b2, this.f1249s, this.f1248b);
                    this.k = f0Var;
                    this.l.a(f0Var.a);
                } else {
                    this.l.a(new t.b(j5, 0L));
                }
            }
            if (this.p) {
                z3 = false;
                this.p = false;
                g(0L, 0L);
                if (iVar.getPosition() != 0) {
                    sVar.a = 0L;
                    return 1;
                }
            } else {
                z3 = false;
            }
            r1 = 1;
            r1 = 1;
            f0 f0Var2 = this.k;
            r0 = z3;
            if (f0Var2 != null) {
                r0 = z3;
                if (f0Var2.b()) {
                    return this.k.a(iVar, sVar);
                }
            }
        } else {
            r0 = 0;
            r1 = 1;
        }
        x xVar3 = this.d;
        byte[] bArr2 = xVar3.a;
        if (9400 - xVar3.f980b < 188) {
            int a2 = xVar3.a();
            if (a2 > 0) {
                System.arraycopy(bArr2, this.d.f980b, bArr2, r0, a2);
            }
            this.d.C(bArr2, a2);
        }
        while (true) {
            if (this.d.a() >= 188) {
                z2 = true;
                break;
            }
            int i12 = this.d.c;
            int read = iVar.read(bArr2, i12, 9400 - i12);
            if (read == -1) {
                z2 = false;
                break;
            }
            this.d.D(i12 + read);
        }
        if (!z2) {
            return -1;
        }
        x xVar4 = this.d;
        int i13 = xVar4.f980b;
        int i14 = xVar4.c;
        byte[] bArr3 = xVar4.a;
        int i15 = i13;
        while (i15 < i14 && bArr3[i15] != 71) {
            i15++;
        }
        this.d.E(i15);
        int i16 = i15 + Opcodes.NEWARRAY;
        i0 i0Var = null;
        if (i16 > i14) {
            int i17 = (i15 - i13) + this.r;
            this.r = i17;
            i = 2;
            if (this.a == 2 && i17 > 376) {
                throw ParserException.a("Cannot find sync byte. Most likely not a Transport Stream.", null);
            }
        } else {
            i = 2;
            this.r = r0;
        }
        x xVar5 = this.d;
        int i18 = xVar5.c;
        if (i16 > i18) {
            return r0;
        }
        int f = xVar5.f();
        if ((8388608 & f) != 0) {
            this.d.E(i16);
            return r0;
        }
        int i19 = ((4194304 & f) != 0 ? 1 : 0) | 0;
        int i20 = (2096896 & f) >> 8;
        boolean z5 = (f & 32) != 0;
        if ((f & 16) != 0) {
            i0Var = this.g.get(i20);
        }
        if (i0Var == null) {
            this.d.E(i16);
            return r0;
        }
        if (this.a != i) {
            int i21 = f & 15;
            int i22 = this.e.get(i20, i21 - 1);
            this.e.put(i20, i21);
            if (i22 == i21) {
                this.d.E(i16);
                return r0;
            } else if (i21 != ((i22 + r1) & 15)) {
                i0Var.c();
            }
        }
        if (z5) {
            int t = this.d.t();
            i19 |= (this.d.t() & 64) != 0 ? 2 : 0;
            x xVar6 = this.d;
            int i23 = r1 == true ? 1 : 0;
            int i24 = r1 == true ? 1 : 0;
            int i25 = r1 == true ? 1 : 0;
            xVar6.F(t - i23);
        }
        boolean z6 = this.n;
        if (this.a == i || z6 || !this.i.get(i20, r0)) {
            this.d.D(i16);
            i0Var.b(this.d, i19);
            this.d.D(i18);
        }
        if (this.a != i && !z6 && this.n && b2 != -1) {
            this.p = r1;
        }
        this.d.E(i16);
        int i26 = r0 == true ? 1 : 0;
        int i27 = r0 == true ? 1 : 0;
        int i28 = r0 == true ? 1 : 0;
        int i29 = r0 == true ? 1 : 0;
        return i26;
    }

    @Override // b.i.a.c.x2.h
    public void f(j jVar) {
        this.l = jVar;
    }

    @Override // b.i.a.c.x2.h
    public void g(long j, long j2) {
        f0 f0Var;
        d.D(this.a != 2);
        int size = this.c.size();
        for (int i = 0; i < size; i++) {
            d0 d0Var = this.c.get(i);
            boolean z2 = d0Var.d() == -9223372036854775807L;
            if (!z2) {
                long c = d0Var.c();
                z2 = (c == -9223372036854775807L || c == 0 || c == j2) ? false : true;
            }
            if (z2) {
                d0Var.e(j2);
            }
        }
        if (!(j2 == 0 || (f0Var = this.k) == null)) {
            f0Var.e(j2);
        }
        this.d.A(0);
        this.e.clear();
        for (int i2 = 0; i2 < this.g.size(); i2++) {
            this.g.valueAt(i2).c();
        }
        this.r = 0;
    }

    @Override // b.i.a.c.x2.h
    public void release() {
    }
}
