package b.i.a.c.x2.k0;

import android.util.SparseArray;
import androidx.annotation.Nullable;
import b.i.a.c.f3.u;
import b.i.a.c.f3.x;
import b.i.a.c.f3.y;
import b.i.a.c.x2.j;
import b.i.a.c.x2.k0.i0;
import b.i.a.c.x2.w;
/* compiled from: H264Reader.java */
/* loaded from: classes3.dex */
public final class r implements o {
    public final e0 a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f1268b;
    public final boolean c;
    public long g;
    public String i;
    public w j;
    public b k;
    public boolean l;
    public boolean n;
    public final boolean[] h = new boolean[3];
    public final w d = new w(7, 128);
    public final w e = new w(8, 128);
    public final w f = new w(6, 128);
    public long m = -9223372036854775807L;
    public final x o = new x();

    /* compiled from: H264Reader.java */
    /* loaded from: classes3.dex */
    public static final class b {
        public final w a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f1269b;
        public final boolean c;
        public final y f;
        public byte[] g;
        public int h;
        public int i;
        public long j;
        public long l;
        public long p;
        public long q;
        public boolean r;
        public final SparseArray<u.c> d = new SparseArray<>();
        public final SparseArray<u.b> e = new SparseArray<>();
        public a m = new a(null);
        public a n = new a(null);
        public boolean k = false;
        public boolean o = false;

        /* compiled from: H264Reader.java */
        /* loaded from: classes3.dex */
        public static final class a {
            public boolean a;

            /* renamed from: b  reason: collision with root package name */
            public boolean f1270b;
            @Nullable
            public u.c c;
            public int d;
            public int e;
            public int f;
            public int g;
            public boolean h;
            public boolean i;
            public boolean j;
            public boolean k;
            public int l;
            public int m;
            public int n;
            public int o;
            public int p;

            public a(a aVar) {
            }
        }

        public b(w wVar, boolean z2, boolean z3) {
            this.a = wVar;
            this.f1269b = z2;
            this.c = z3;
            byte[] bArr = new byte[128];
            this.g = bArr;
            this.f = new y(bArr, 0, 0);
            a aVar = this.n;
            aVar.f1270b = false;
            aVar.a = false;
        }
    }

    public r(e0 e0Var, boolean z2, boolean z3) {
        this.a = e0Var;
        this.f1268b = z2;
        this.c = z3;
    }

    /* JADX WARN: Removed duplicated region for block: B:46:0x0126  */
    /* JADX WARN: Removed duplicated region for block: B:47:0x0128  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x012b  */
    /* JADX WARN: Removed duplicated region for block: B:52:0x013c  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x0141  */
    /* JADX WARN: Removed duplicated region for block: B:65:0x0171  */
    @org.checkerframework.checker.nullness.qual.RequiresNonNull({"sampleReader"})
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void a(byte[] r17, int r18, int r19) {
        /*
            Method dump skipped, instructions count: 460
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.k0.r.a(byte[], int, int):void");
    }

    /* JADX WARN: Code restructure failed: missing block: B:117:0x0284, code lost:
        if (r6 != 1) goto L119;
     */
    /* JADX WARN: Code restructure failed: missing block: B:54:0x01ac, code lost:
        if (r7.j == r10.j) goto L55;
     */
    /* JADX WARN: Code restructure failed: missing block: B:58:0x01b6, code lost:
        if (r14 != 0) goto L59;
     */
    /* JADX WARN: Code restructure failed: missing block: B:66:0x01ca, code lost:
        if (r7.n == r10.n) goto L67;
     */
    /* JADX WARN: Code restructure failed: missing block: B:74:0x01dd, code lost:
        if (r7.p == r10.p) goto L75;
     */
    /* JADX WARN: Code restructure failed: missing block: B:79:0x01eb, code lost:
        if (r7.l == r10.l) goto L80;
     */
    /* JADX WARN: Code restructure failed: missing block: B:82:0x01f1, code lost:
        if (r7 == false) goto L90;
     */
    /* JADX WARN: Removed duplicated region for block: B:101:0x024b A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:108:0x0259  */
    /* JADX WARN: Removed duplicated region for block: B:111:0x0262  */
    /* JADX WARN: Removed duplicated region for block: B:116:0x0283  */
    /* JADX WARN: Removed duplicated region for block: B:118:0x0287  */
    /* JADX WARN: Removed duplicated region for block: B:121:0x028c  */
    /* JADX WARN: Removed duplicated region for block: B:128:0x02a4 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:34:0x0142  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x0170  */
    /* JADX WARN: Removed duplicated region for block: B:88:0x020c  */
    /* JADX WARN: Removed duplicated region for block: B:92:0x0234  */
    @Override // b.i.a.c.x2.k0.o
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void b(b.i.a.c.f3.x r26) {
        /*
            Method dump skipped, instructions count: 683
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.k0.r.b(b.i.a.c.f3.x):void");
    }

    @Override // b.i.a.c.x2.k0.o
    public void c() {
        this.g = 0L;
        this.n = false;
        this.m = -9223372036854775807L;
        u.a(this.h);
        this.d.c();
        this.e.c();
        this.f.c();
        b bVar = this.k;
        if (bVar != null) {
            bVar.k = false;
            bVar.o = false;
            b.a aVar = bVar.n;
            aVar.f1270b = false;
            aVar.a = false;
        }
    }

    @Override // b.i.a.c.x2.k0.o
    public void d() {
    }

    @Override // b.i.a.c.x2.k0.o
    public void e(j jVar, i0.d dVar) {
        dVar.a();
        this.i = dVar.b();
        w p = jVar.p(dVar.c(), 2);
        this.j = p;
        this.k = new b(p, this.f1268b, this.c);
        this.a.a(jVar, dVar);
    }

    @Override // b.i.a.c.x2.k0.o
    public void f(long j, int i) {
        if (j != -9223372036854775807L) {
            this.m = j;
        }
        this.n |= (i & 2) != 0;
    }
}
