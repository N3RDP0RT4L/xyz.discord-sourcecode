package b.i.a.c.x2.k0;

import b.c.a.a0.d;
import b.i.a.c.f3.d0;
import b.i.a.c.f3.e0;
import b.i.a.c.j1;
import b.i.a.c.x2.j;
import b.i.a.c.x2.k0.i0;
import b.i.a.c.x2.w;
/* compiled from: PassthroughSectionPayloadReader.java */
/* loaded from: classes3.dex */
public final class x implements c0 {
    public j1 a;

    /* renamed from: b  reason: collision with root package name */
    public d0 f1278b;
    public w c;

    public x(String str) {
        j1.b bVar = new j1.b();
        bVar.k = str;
        this.a = bVar.a();
    }

    @Override // b.i.a.c.x2.k0.c0
    public void a(d0 d0Var, j jVar, i0.d dVar) {
        this.f1278b = d0Var;
        dVar.a();
        w p = jVar.p(dVar.c(), 5);
        this.c = p;
        p.e(this.a);
    }

    @Override // b.i.a.c.x2.k0.c0
    public void b(b.i.a.c.f3.x xVar) {
        long j;
        long j2;
        d.H(this.f1278b);
        int i = e0.a;
        d0 d0Var = this.f1278b;
        synchronized (d0Var) {
            long j3 = d0Var.c;
            if (j3 != -9223372036854775807L) {
                j = j3 + d0Var.f962b;
            } else {
                j = d0Var.c();
            }
            j2 = j;
        }
        long d = this.f1278b.d();
        if (j2 != -9223372036854775807L && d != -9223372036854775807L) {
            j1 j1Var = this.a;
            if (d != j1Var.A) {
                j1.b a = j1Var.a();
                a.o = d;
                j1 a2 = a.a();
                this.a = a2;
                this.c.e(a2);
            }
            int a3 = xVar.a();
            this.c.c(xVar, a3);
            this.c.d(j2, 1, a3, 0, null);
        }
    }
}
