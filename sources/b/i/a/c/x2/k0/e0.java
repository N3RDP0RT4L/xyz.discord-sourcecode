package b.i.a.c.x2.k0;

import b.c.a.a0.d;
import b.i.a.c.j1;
import b.i.a.c.x2.j;
import b.i.a.c.x2.k0.i0;
import b.i.a.c.x2.w;
import java.util.List;
/* compiled from: SeiReader.java */
/* loaded from: classes3.dex */
public final class e0 {
    public final List<j1> a;

    /* renamed from: b  reason: collision with root package name */
    public final w[] f1242b;

    public e0(List<j1> list) {
        this.a = list;
        this.f1242b = new w[list.size()];
    }

    public void a(j jVar, i0.d dVar) {
        for (int i = 0; i < this.f1242b.length; i++) {
            dVar.a();
            w p = jVar.p(dVar.c(), 3);
            j1 j1Var = this.a.get(i);
            String str = j1Var.w;
            boolean z2 = "application/cea-608".equals(str) || "application/cea-708".equals(str);
            String valueOf = String.valueOf(str);
            d.m(z2, valueOf.length() != 0 ? "Invalid closed caption mime type provided: ".concat(valueOf) : new String("Invalid closed caption mime type provided: "));
            String str2 = j1Var.l;
            if (str2 == null) {
                str2 = dVar.b();
            }
            j1.b bVar = new j1.b();
            bVar.a = str2;
            bVar.k = str;
            bVar.d = j1Var.o;
            bVar.c = j1Var.n;
            bVar.C = j1Var.O;
            bVar.m = j1Var.f1015y;
            p.e(bVar.a());
            this.f1242b[i] = p;
        }
    }
}
