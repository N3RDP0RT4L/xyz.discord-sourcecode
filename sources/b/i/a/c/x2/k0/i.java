package b.i.a.c.x2.k0;

import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.f3.w;
import b.i.a.c.f3.x;
import b.i.a.c.j1;
import b.i.a.c.t2.n;
import b.i.a.c.x2.j;
import b.i.a.c.x2.k0.i0;
/* compiled from: Ac4Reader.java */
/* loaded from: classes3.dex */
public final class i implements o {
    public final w a;

    /* renamed from: b  reason: collision with root package name */
    public final x f1252b;
    @Nullable
    public final String c;
    public String d;
    public b.i.a.c.x2.w e;
    public long j;
    public j1 k;
    public int l;
    public int f = 0;
    public int g = 0;
    public boolean h = false;
    public boolean i = false;
    public long m = -9223372036854775807L;

    public i(@Nullable String str) {
        w wVar = new w(new byte[16]);
        this.a = wVar;
        this.f1252b = new x(wVar.a);
        this.c = str;
    }

    @Override // b.i.a.c.x2.k0.o
    public void b(x xVar) {
        int i;
        boolean z2;
        int t;
        d.H(this.e);
        while (xVar.a() > 0) {
            int i2 = this.f;
            boolean z3 = true;
            if (i2 == 0) {
                while (true) {
                    i = 65;
                    if (xVar.a() <= 0) {
                        z2 = false;
                        break;
                    } else if (!this.h) {
                        this.h = xVar.t() == 172;
                    } else {
                        t = xVar.t();
                        this.h = t == 172;
                        if (t == 64 || t == 65) {
                            break;
                        }
                    }
                }
                this.i = t == 65;
                z2 = true;
                if (z2) {
                    this.f = 1;
                    byte[] bArr = this.f1252b.a;
                    bArr[0] = -84;
                    if (!this.i) {
                        i = 64;
                    }
                    bArr[1] = (byte) i;
                    this.g = 2;
                }
            } else if (i2 == 1) {
                byte[] bArr2 = this.f1252b.a;
                int min = Math.min(xVar.a(), 16 - this.g);
                System.arraycopy(xVar.a, xVar.f980b, bArr2, this.g, min);
                xVar.f980b += min;
                int i3 = this.g + min;
                this.g = i3;
                if (i3 != 16) {
                    z3 = false;
                }
                if (z3) {
                    this.a.k(0);
                    n.b b2 = n.b(this.a);
                    j1 j1Var = this.k;
                    if (j1Var == null || 2 != j1Var.J || b2.a != j1Var.K || !"audio/ac4".equals(j1Var.w)) {
                        j1.b bVar = new j1.b();
                        bVar.a = this.d;
                        bVar.k = "audio/ac4";
                        bVar.f1019x = 2;
                        bVar.f1020y = b2.a;
                        bVar.c = this.c;
                        j1 a = bVar.a();
                        this.k = a;
                        this.e.e(a);
                    }
                    this.l = b2.f1120b;
                    this.j = (b2.c * 1000000) / this.k.K;
                    this.f1252b.E(0);
                    this.e.c(this.f1252b, 16);
                    this.f = 2;
                }
            } else if (i2 == 2) {
                int min2 = Math.min(xVar.a(), this.l - this.g);
                this.e.c(xVar, min2);
                int i4 = this.g + min2;
                this.g = i4;
                int i5 = this.l;
                if (i4 == i5) {
                    long j = this.m;
                    if (j != -9223372036854775807L) {
                        this.e.d(j, 1, i5, 0, null);
                        this.m += this.j;
                    }
                    this.f = 0;
                }
            }
        }
    }

    @Override // b.i.a.c.x2.k0.o
    public void c() {
        this.f = 0;
        this.g = 0;
        this.h = false;
        this.i = false;
        this.m = -9223372036854775807L;
    }

    @Override // b.i.a.c.x2.k0.o
    public void d() {
    }

    @Override // b.i.a.c.x2.k0.o
    public void e(j jVar, i0.d dVar) {
        dVar.a();
        this.d = dVar.b();
        this.e = jVar.p(dVar.c(), 1);
    }

    @Override // b.i.a.c.x2.k0.o
    public void f(long j, int i) {
        if (j != -9223372036854775807L) {
            this.m = j;
        }
    }
}
