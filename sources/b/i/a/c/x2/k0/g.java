package b.i.a.c.x2.k0;

import androidx.annotation.Nullable;
import b.i.a.c.f3.w;
import b.i.a.c.f3.x;
import b.i.a.c.j1;
import b.i.a.c.x2.j;
import b.i.a.c.x2.k0.i0;
/* compiled from: Ac3Reader.java */
/* loaded from: classes3.dex */
public final class g implements o {
    public final w a;

    /* renamed from: b  reason: collision with root package name */
    public final x f1245b;
    @Nullable
    public final String c;
    public String d;
    public b.i.a.c.x2.w e;
    public int g;
    public boolean h;
    public long i;
    public j1 j;
    public int k;
    public int f = 0;
    public long l = -9223372036854775807L;

    public g(@Nullable String str) {
        w wVar = new w(new byte[128]);
        this.a = wVar;
        this.f1245b = new x(wVar.a);
        this.c = str;
    }

    /* JADX WARN: Removed duplicated region for block: B:125:0x0212  */
    /* JADX WARN: Removed duplicated region for block: B:134:0x022e  */
    @Override // b.i.a.c.x2.k0.o
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void b(b.i.a.c.f3.x r23) {
        /*
            Method dump skipped, instructions count: 915
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.k0.g.b(b.i.a.c.f3.x):void");
    }

    @Override // b.i.a.c.x2.k0.o
    public void c() {
        this.f = 0;
        this.g = 0;
        this.h = false;
        this.l = -9223372036854775807L;
    }

    @Override // b.i.a.c.x2.k0.o
    public void d() {
    }

    @Override // b.i.a.c.x2.k0.o
    public void e(j jVar, i0.d dVar) {
        dVar.a();
        this.d = dVar.b();
        this.e = jVar.p(dVar.c(), 1);
    }

    @Override // b.i.a.c.x2.k0.o
    public void f(long j, int i) {
        if (j != -9223372036854775807L) {
            this.l = j;
        }
    }
}
