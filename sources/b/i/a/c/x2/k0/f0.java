package b.i.a.c.x2.k0;

import b.c.a.a0.d;
import b.i.a.c.f3.d0;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.x;
import b.i.a.c.x2.a;
import b.i.a.c.x2.i;
import java.io.IOException;
import org.objectweb.asm.Opcodes;
/* compiled from: TsBinarySearchSeeker.java */
/* loaded from: classes3.dex */
public final class f0 extends b.i.a.c.x2.a {

    /* compiled from: TsBinarySearchSeeker.java */
    /* loaded from: classes3.dex */
    public static final class a implements a.f {
        public final d0 a;

        /* renamed from: b  reason: collision with root package name */
        public final x f1244b = new x();
        public final int c;
        public final int d;

        public a(int i, d0 d0Var, int i2) {
            this.c = i;
            this.a = d0Var;
            this.d = i2;
        }

        @Override // b.i.a.c.x2.a.f
        public void a() {
            this.f1244b.B(e0.f);
        }

        @Override // b.i.a.c.x2.a.f
        public a.e b(i iVar, long j) throws IOException {
            long position = iVar.getPosition();
            int min = (int) Math.min(this.d, iVar.b() - position);
            this.f1244b.A(min);
            iVar.o(this.f1244b.a, 0, min);
            x xVar = this.f1244b;
            int i = xVar.c;
            long j2 = -1;
            long j3 = -1;
            long j4 = -9223372036854775807L;
            while (xVar.a() >= 188) {
                byte[] bArr = xVar.a;
                int i2 = xVar.f980b;
                while (i2 < i && bArr[i2] != 71) {
                    i2++;
                }
                int i3 = i2 + Opcodes.NEWARRAY;
                if (i3 > i) {
                    break;
                }
                long K1 = d.K1(xVar, i2, this.c);
                if (K1 != -9223372036854775807L) {
                    long b2 = this.a.b(K1);
                    if (b2 > j) {
                        if (j4 == -9223372036854775807L) {
                            return a.e.a(b2, position);
                        }
                        return a.e.b(position + j3);
                    } else if (100000 + b2 > j) {
                        return a.e.b(position + i2);
                    } else {
                        j3 = i2;
                        j4 = b2;
                    }
                }
                xVar.E(i3);
                j2 = i3;
            }
            if (j4 != -9223372036854775807L) {
                return a.e.c(j4, position + j2);
            }
            return a.e.a;
        }
    }

    public f0(d0 d0Var, long j, long j2, int i, int i2) {
        super(new a.b(), new a(i, d0Var, i2), j, 0L, j + 1, 0L, j2, 188L, 940);
    }
}
