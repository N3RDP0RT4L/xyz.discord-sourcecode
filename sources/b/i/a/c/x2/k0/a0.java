package b.i.a.c.x2.k0;

import b.i.a.c.f3.d0;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.x;
import b.i.a.c.x2.i;
/* compiled from: PsDurationReader.java */
/* loaded from: classes3.dex */
public final class a0 {
    public boolean c;
    public boolean d;
    public boolean e;
    public final d0 a = new d0(0);
    public long f = -9223372036854775807L;
    public long g = -9223372036854775807L;
    public long h = -9223372036854775807L;

    /* renamed from: b  reason: collision with root package name */
    public final x f1238b = new x();

    public static long c(x xVar) {
        int i = xVar.f980b;
        if (xVar.a() < 9) {
            return -9223372036854775807L;
        }
        byte[] bArr = new byte[9];
        System.arraycopy(xVar.a, xVar.f980b, bArr, 0, 9);
        xVar.f980b += 9;
        xVar.E(i);
        if (!((bArr[0] & 196) == 68 && (bArr[2] & 4) == 4 && (bArr[4] & 4) == 4 && (bArr[5] & 1) == 1 && (bArr[8] & 3) == 3)) {
            return -9223372036854775807L;
        }
        return (((bArr[0] & 56) >> 3) << 30) | ((bArr[0] & 3) << 28) | ((bArr[1] & 255) << 20) | (((bArr[2] & 248) >> 3) << 15) | ((bArr[2] & 3) << 13) | ((bArr[3] & 255) << 5) | ((bArr[4] & 248) >> 3);
    }

    public final int a(i iVar) {
        this.f1238b.B(e0.f);
        this.c = true;
        iVar.k();
        return 0;
    }

    public final int b(byte[] bArr, int i) {
        return (bArr[i + 3] & 255) | ((bArr[i] & 255) << 24) | ((bArr[i + 1] & 255) << 16) | ((bArr[i + 2] & 255) << 8);
    }
}
