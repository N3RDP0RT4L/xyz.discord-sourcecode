package b.i.a.c.x2.k0;

import androidx.annotation.Nullable;
import b.i.a.c.f3.x;
import b.i.a.c.j1;
import b.i.a.c.x2.g;
import b.i.a.c.x2.j;
import b.i.a.c.x2.k0.i0;
import b.i.a.c.x2.w;
import java.util.Arrays;
/* compiled from: AdtsReader.java */
/* loaded from: classes3.dex */
public final class k implements o {
    public static final byte[] a = {73, 68, 51};

    /* renamed from: b  reason: collision with root package name */
    public final boolean f1258b;
    @Nullable
    public final String e;
    public String f;
    public w g;
    public w h;
    public int i;
    public int j;
    public int k;
    public boolean l;
    public boolean m;
    public int p;
    public boolean q;

    /* renamed from: s  reason: collision with root package name */
    public int f1259s;
    public w u;
    public long v;
    public final b.i.a.c.f3.w c = new b.i.a.c.f3.w(new byte[7]);
    public final x d = new x(Arrays.copyOf(a, 10));
    public int n = -1;
    public int o = -1;
    public long r = -9223372036854775807L;
    public long t = -9223372036854775807L;

    public k(boolean z2, @Nullable String str) {
        h();
        this.f1258b = z2;
        this.e = str;
    }

    public static boolean g(int i) {
        return (i & 65526) == 65520;
    }

    public final boolean a(x xVar, byte[] bArr, int i) {
        int min = Math.min(xVar.a(), i - this.j);
        System.arraycopy(xVar.a, xVar.f980b, bArr, this.j, min);
        xVar.f980b += min;
        int i2 = this.j + min;
        this.j = i2;
        return i2 == i;
    }

    /* JADX WARN: Removed duplicated region for block: B:146:0x0275 A[EDGE_INSN: B:146:0x0275->B:97:0x0275 ?: BREAK  , SYNTHETIC] */
    @Override // b.i.a.c.x2.k0.o
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void b(b.i.a.c.f3.x r18) throws com.google.android.exoplayer2.ParserException {
        /*
            Method dump skipped, instructions count: 751
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.k0.k.b(b.i.a.c.f3.x):void");
    }

    @Override // b.i.a.c.x2.k0.o
    public void c() {
        this.t = -9223372036854775807L;
        this.m = false;
        h();
    }

    @Override // b.i.a.c.x2.k0.o
    public void d() {
    }

    @Override // b.i.a.c.x2.k0.o
    public void e(j jVar, i0.d dVar) {
        dVar.a();
        this.f = dVar.b();
        w p = jVar.p(dVar.c(), 1);
        this.g = p;
        this.u = p;
        if (this.f1258b) {
            dVar.a();
            w p2 = jVar.p(dVar.c(), 5);
            this.h = p2;
            j1.b bVar = new j1.b();
            bVar.a = dVar.b();
            bVar.k = "application/id3";
            p2.e(bVar.a());
            return;
        }
        this.h = new g();
    }

    @Override // b.i.a.c.x2.k0.o
    public void f(long j, int i) {
        if (j != -9223372036854775807L) {
            this.t = j;
        }
    }

    public final void h() {
        this.i = 0;
        this.j = 0;
        this.k = 256;
    }

    public final boolean i(x xVar, byte[] bArr, int i) {
        if (xVar.a() < i) {
            return false;
        }
        System.arraycopy(xVar.a, xVar.f980b, bArr, 0, i);
        xVar.f980b += i;
        return true;
    }
}
