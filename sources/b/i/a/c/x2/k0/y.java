package b.i.a.c.x2.k0;

import android.util.Log;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.d.b.a.a;
import b.i.a.c.f3.d0;
import b.i.a.c.f3.w;
import b.i.a.c.f3.x;
import b.i.a.c.x2.j;
import b.i.a.c.x2.k0.i0;
import com.google.android.exoplayer2.ParserException;
/* compiled from: PesReader.java */
/* loaded from: classes3.dex */
public final class y implements i0 {
    public final o a;

    /* renamed from: b  reason: collision with root package name */
    public final w f1279b = new w(new byte[10]);
    public int c = 0;
    public int d;
    public d0 e;
    public boolean f;
    public boolean g;
    public boolean h;
    public int i;
    public int j;
    public boolean k;
    public long l;

    public y(o oVar) {
        this.a = oVar;
    }

    @Override // b.i.a.c.x2.k0.i0
    public void a(d0 d0Var, j jVar, i0.d dVar) {
        this.e = d0Var;
        this.a.e(jVar, dVar);
    }

    @Override // b.i.a.c.x2.k0.i0
    public final void b(x xVar, int i) throws ParserException {
        boolean z2;
        d.H(this.e);
        int i2 = -1;
        int i3 = 3;
        if ((i & 1) != 0) {
            int i4 = this.c;
            if (!(i4 == 0 || i4 == 1)) {
                if (i4 == 2) {
                    Log.w("PesReader", "Unexpected start indicator reading extended header");
                } else if (i4 == 3) {
                    int i5 = this.j;
                    if (i5 != -1) {
                        StringBuilder sb = new StringBuilder(59);
                        sb.append("Unexpected start indicator: expected ");
                        sb.append(i5);
                        sb.append(" more bytes");
                        Log.w("PesReader", sb.toString());
                    }
                    this.a.d();
                } else {
                    throw new IllegalStateException();
                }
            }
            e(1);
        }
        while (xVar.a() > 0) {
            int i6 = this.c;
            if (i6 != 0) {
                int i7 = 0;
                if (i6 != 1) {
                    if (i6 == 2) {
                        if (d(xVar, this.f1279b.a, Math.min(10, this.i)) && d(xVar, null, this.i)) {
                            this.f1279b.k(0);
                            this.l = -9223372036854775807L;
                            if (this.f) {
                                this.f1279b.m(4);
                                this.f1279b.m(1);
                                this.f1279b.m(1);
                                long g = (this.f1279b.g(i3) << 30) | (this.f1279b.g(15) << 15) | this.f1279b.g(15);
                                this.f1279b.m(1);
                                if (!this.h && this.g) {
                                    this.f1279b.m(4);
                                    this.f1279b.m(1);
                                    this.f1279b.m(1);
                                    this.f1279b.m(1);
                                    this.e.b((this.f1279b.g(i3) << 30) | (this.f1279b.g(15) << 15) | this.f1279b.g(15));
                                    this.h = true;
                                }
                                this.l = this.e.b(g);
                            }
                            i |= this.k ? 4 : 0;
                            this.a.f(this.l, i);
                            e(3);
                        }
                    } else if (i6 == i3) {
                        int a = xVar.a();
                        int i8 = this.j;
                        if (i8 != i2) {
                            i7 = a - i8;
                        }
                        if (i7 > 0) {
                            a -= i7;
                            xVar.D(xVar.f980b + a);
                        }
                        this.a.b(xVar);
                        int i9 = this.j;
                        if (i9 != i2) {
                            int i10 = i9 - a;
                            this.j = i10;
                            if (i10 == 0) {
                                this.a.d();
                                e(1);
                            }
                        }
                    } else {
                        throw new IllegalStateException();
                    }
                } else if (d(xVar, this.f1279b.a, 9)) {
                    this.f1279b.k(0);
                    int g2 = this.f1279b.g(24);
                    if (g2 != 1) {
                        a.d0(41, "Unexpected start code prefix: ", g2, "PesReader");
                        this.j = -1;
                        z2 = false;
                    } else {
                        this.f1279b.m(8);
                        int g3 = this.f1279b.g(16);
                        this.f1279b.m(5);
                        this.k = this.f1279b.f();
                        this.f1279b.m(2);
                        this.f = this.f1279b.f();
                        this.g = this.f1279b.f();
                        this.f1279b.m(6);
                        int g4 = this.f1279b.g(8);
                        this.i = g4;
                        if (g3 == 0) {
                            this.j = -1;
                        } else {
                            int i11 = ((g3 + 6) - 9) - g4;
                            this.j = i11;
                            if (i11 < 0) {
                                a.d0(47, "Found negative packet payload size: ", i11, "PesReader");
                                this.j = -1;
                            }
                        }
                        z2 = true;
                    }
                    e(z2 ? 2 : 0);
                }
            } else {
                xVar.F(xVar.a());
            }
            i2 = -1;
            i3 = 3;
        }
    }

    @Override // b.i.a.c.x2.k0.i0
    public final void c() {
        this.c = 0;
        this.d = 0;
        this.h = false;
        this.a.c();
    }

    public final boolean d(x xVar, @Nullable byte[] bArr, int i) {
        int min = Math.min(xVar.a(), i - this.d);
        if (min <= 0) {
            return true;
        }
        if (bArr == null) {
            xVar.F(min);
        } else {
            System.arraycopy(xVar.a, xVar.f980b, bArr, this.d, min);
            xVar.f980b += min;
        }
        int i2 = this.d + min;
        this.d = i2;
        return i2 == i;
    }

    public final void e(int i) {
        this.c = i;
        this.d = 0;
    }
}
