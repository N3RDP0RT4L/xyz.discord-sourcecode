package b.i.a.c.x2.k0;

import androidx.annotation.Nullable;
import b.i.a.c.f3.d0;
import b.i.a.c.f3.x;
import b.i.a.c.x2.j;
import com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel;
import com.google.android.exoplayer2.ParserException;
import java.util.Collections;
import java.util.List;
/* compiled from: TsPayloadReader.java */
/* loaded from: classes3.dex */
public interface i0 {

    /* compiled from: TsPayloadReader.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public final String a;

        /* renamed from: b  reason: collision with root package name */
        public final byte[] f1253b;

        public a(String str, int i, byte[] bArr) {
            this.a = str;
            this.f1253b = bArr;
        }
    }

    /* compiled from: TsPayloadReader.java */
    /* loaded from: classes3.dex */
    public static final class b {
        public final int a;
        @Nullable

        /* renamed from: b  reason: collision with root package name */
        public final String f1254b;
        public final List<a> c;
        public final byte[] d;

        public b(int i, @Nullable String str, @Nullable List<a> list, byte[] bArr) {
            List<a> list2;
            this.a = i;
            this.f1254b = str;
            if (list == null) {
                list2 = Collections.emptyList();
            } else {
                list2 = Collections.unmodifiableList(list);
            }
            this.c = list2;
            this.d = bArr;
        }
    }

    /* compiled from: TsPayloadReader.java */
    /* loaded from: classes3.dex */
    public interface c {
        @Nullable
        i0 a(int i, b bVar);
    }

    /* compiled from: TsPayloadReader.java */
    /* loaded from: classes3.dex */
    public static final class d {
        public final String a;

        /* renamed from: b  reason: collision with root package name */
        public final int f1255b;
        public final int c;
        public int d;
        public String e;

        public d(int i, int i2, int i3) {
            String str;
            if (i != Integer.MIN_VALUE) {
                StringBuilder sb = new StringBuilder(12);
                sb.append(i);
                sb.append(AutocompleteViewModel.COMMAND_DISCOVER_TOKEN);
                str = sb.toString();
            } else {
                str = "";
            }
            this.a = str;
            this.f1255b = i2;
            this.c = i3;
            this.d = Integer.MIN_VALUE;
            this.e = "";
        }

        public void a() {
            int i = this.d;
            int i2 = i == Integer.MIN_VALUE ? this.f1255b : i + this.c;
            this.d = i2;
            String str = this.a;
            this.e = b.d.b.a.a.f(b.d.b.a.a.b(str, 11), str, i2);
        }

        public String b() {
            if (this.d != Integer.MIN_VALUE) {
                return this.e;
            }
            throw new IllegalStateException("generateNewId() must be called before retrieving ids.");
        }

        public int c() {
            int i = this.d;
            if (i != Integer.MIN_VALUE) {
                return i;
            }
            throw new IllegalStateException("generateNewId() must be called before retrieving ids.");
        }
    }

    void a(d0 d0Var, j jVar, d dVar);

    void b(x xVar, int i) throws ParserException;

    void c();
}
