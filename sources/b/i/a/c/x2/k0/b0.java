package b.i.a.c.x2.k0;

import android.util.SparseArray;
import androidx.annotation.Nullable;
import b.i.a.c.f3.d0;
import b.i.a.c.f3.w;
import b.i.a.c.f3.x;
import b.i.a.c.x2.h;
import b.i.a.c.x2.i;
import b.i.a.c.x2.j;
import java.io.IOException;
/* compiled from: PsExtractor.java */
/* loaded from: classes3.dex */
public final class b0 implements h {
    public boolean e;
    public boolean f;
    public boolean g;
    public long h;
    @Nullable
    public z i;
    public j j;
    public boolean k;
    public final d0 a = new d0(0);
    public final x c = new x(4096);

    /* renamed from: b  reason: collision with root package name */
    public final SparseArray<a> f1239b = new SparseArray<>();
    public final a0 d = new a0();

    /* compiled from: PsExtractor.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public final o a;

        /* renamed from: b  reason: collision with root package name */
        public final d0 f1240b;
        public final w c = new w(new byte[64]);
        public boolean d;
        public boolean e;
        public boolean f;
        public int g;
        public long h;

        public a(o oVar, d0 d0Var) {
            this.a = oVar;
            this.f1240b = d0Var;
        }
    }

    static {
        d dVar = d.a;
    }

    @Override // b.i.a.c.x2.h
    public boolean b(i iVar) throws IOException {
        byte[] bArr = new byte[14];
        iVar.o(bArr, 0, 14);
        if (442 != (((bArr[0] & 255) << 24) | ((bArr[1] & 255) << 16) | ((bArr[2] & 255) << 8) | (bArr[3] & 255)) || (bArr[4] & 196) != 68 || (bArr[6] & 4) != 4 || (bArr[8] & 4) != 4 || (bArr[9] & 1) != 1 || (bArr[12] & 3) != 3) {
            return false;
        }
        iVar.g(bArr[13] & 7);
        iVar.o(bArr, 0, 3);
        return 1 == ((((bArr[0] & 255) << 16) | ((bArr[1] & 255) << 8)) | (bArr[2] & 255));
    }

    /* JADX WARN: Removed duplicated region for block: B:105:0x021d  */
    /* JADX WARN: Removed duplicated region for block: B:67:0x0155  */
    /* JADX WARN: Removed duplicated region for block: B:68:0x015b  */
    /* JADX WARN: Removed duplicated region for block: B:79:0x0186 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:80:0x0187  */
    @Override // b.i.a.c.x2.h
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public int e(b.i.a.c.x2.i r14, b.i.a.c.x2.s r15) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 855
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.k0.b0.e(b.i.a.c.x2.i, b.i.a.c.x2.s):int");
    }

    @Override // b.i.a.c.x2.h
    public void f(j jVar) {
        this.j = jVar;
    }

    @Override // b.i.a.c.x2.h
    public void g(long j, long j2) {
        boolean z2 = true;
        boolean z3 = this.a.d() == -9223372036854775807L;
        if (!z3) {
            long c = this.a.c();
            if (c == -9223372036854775807L || c == 0 || c == j2) {
                z2 = false;
            }
            z3 = z2;
        }
        if (z3) {
            this.a.e(j2);
        }
        z zVar = this.i;
        if (zVar != null) {
            zVar.e(j2);
        }
        for (int i = 0; i < this.f1239b.size(); i++) {
            a valueAt = this.f1239b.valueAt(i);
            valueAt.f = false;
            valueAt.a.c();
        }
    }

    @Override // b.i.a.c.x2.h
    public void release() {
    }
}
