package b.i.a.c.x2.k0;

import android.util.Log;
import b.c.a.a0.d;
import b.i.a.c.f3.x;
import b.i.a.c.j1;
import b.i.a.c.x2.j;
import b.i.a.c.x2.k0.i0;
import b.i.a.c.x2.w;
/* compiled from: Id3Reader.java */
/* loaded from: classes3.dex */
public final class t implements o {

    /* renamed from: b  reason: collision with root package name */
    public w f1273b;
    public boolean c;
    public int e;
    public int f;
    public final x a = new x(10);
    public long d = -9223372036854775807L;

    @Override // b.i.a.c.x2.k0.o
    public void b(x xVar) {
        d.H(this.f1273b);
        if (this.c) {
            int a = xVar.a();
            int i = this.f;
            if (i < 10) {
                int min = Math.min(a, 10 - i);
                System.arraycopy(xVar.a, xVar.f980b, this.a.a, this.f, min);
                if (this.f + min == 10) {
                    this.a.E(0);
                    if (73 == this.a.t() && 68 == this.a.t() && 51 == this.a.t()) {
                        this.a.F(3);
                        this.e = this.a.s() + 10;
                    } else {
                        Log.w("Id3Reader", "Discarding invalid ID3 tag");
                        this.c = false;
                        return;
                    }
                }
            }
            int min2 = Math.min(a, this.e - this.f);
            this.f1273b.c(xVar, min2);
            this.f += min2;
        }
    }

    @Override // b.i.a.c.x2.k0.o
    public void c() {
        this.c = false;
        this.d = -9223372036854775807L;
    }

    @Override // b.i.a.c.x2.k0.o
    public void d() {
        int i;
        d.H(this.f1273b);
        if (this.c && (i = this.e) != 0 && this.f == i) {
            long j = this.d;
            if (j != -9223372036854775807L) {
                this.f1273b.d(j, 1, i, 0, null);
            }
            this.c = false;
        }
    }

    @Override // b.i.a.c.x2.k0.o
    public void e(j jVar, i0.d dVar) {
        dVar.a();
        w p = jVar.p(dVar.c(), 5);
        this.f1273b = p;
        j1.b bVar = new j1.b();
        bVar.a = dVar.b();
        bVar.k = "application/id3";
        p.e(bVar.a());
    }

    @Override // b.i.a.c.x2.k0.o
    public void f(long j, int i) {
        if ((i & 4) != 0) {
            this.c = true;
            if (j != -9223372036854775807L) {
                this.d = j;
            }
            this.e = 0;
            this.f = 0;
        }
    }
}
