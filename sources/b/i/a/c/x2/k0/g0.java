package b.i.a.c.x2.k0;

import b.i.a.c.f3.d0;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.x;
import b.i.a.c.x2.i;
/* compiled from: TsDurationReader.java */
/* loaded from: classes3.dex */
public final class g0 {
    public final int a;
    public boolean d;
    public boolean e;
    public boolean f;

    /* renamed from: b  reason: collision with root package name */
    public final d0 f1246b = new d0(0);
    public long g = -9223372036854775807L;
    public long h = -9223372036854775807L;
    public long i = -9223372036854775807L;
    public final x c = new x();

    public g0(int i) {
        this.a = i;
    }

    public final int a(i iVar) {
        this.c.B(e0.f);
        this.d = true;
        iVar.k();
        return 0;
    }
}
