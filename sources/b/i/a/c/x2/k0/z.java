package b.i.a.c.x2.k0;

import b.i.a.c.f3.d0;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.x;
import b.i.a.c.x2.a;
import b.i.a.c.x2.i;
import java.io.IOException;
/* compiled from: PsBinarySearchSeeker.java */
/* loaded from: classes3.dex */
public final class z extends b.i.a.c.x2.a {

    /* compiled from: PsBinarySearchSeeker.java */
    /* loaded from: classes3.dex */
    public static final class b implements a.f {
        public final d0 a;

        /* renamed from: b  reason: collision with root package name */
        public final x f1280b = new x();

        public b(d0 d0Var, a aVar) {
            this.a = d0Var;
        }

        @Override // b.i.a.c.x2.a.f
        public void a() {
            this.f1280b.B(e0.f);
        }

        @Override // b.i.a.c.x2.a.f
        public a.e b(i iVar, long j) throws IOException {
            int g;
            long position = iVar.getPosition();
            int min = (int) Math.min(20000L, iVar.b() - position);
            this.f1280b.A(min);
            iVar.o(this.f1280b.a, 0, min);
            x xVar = this.f1280b;
            int i = -1;
            long j2 = -9223372036854775807L;
            int i2 = -1;
            while (xVar.a() >= 4) {
                if (z.g(xVar.a, xVar.f980b) != 442) {
                    xVar.F(1);
                } else {
                    xVar.F(4);
                    long c = a0.c(xVar);
                    if (c != -9223372036854775807L) {
                        long b2 = this.a.b(c);
                        if (b2 > j) {
                            if (j2 == -9223372036854775807L) {
                                return a.e.a(b2, position);
                            }
                            return a.e.b(position + i2);
                        } else if (100000 + b2 > j) {
                            return a.e.b(position + xVar.f980b);
                        } else {
                            i2 = xVar.f980b;
                            j2 = b2;
                        }
                    }
                    int i3 = xVar.c;
                    if (xVar.a() >= 10) {
                        xVar.F(9);
                        int t = xVar.t() & 7;
                        if (xVar.a() >= t) {
                            xVar.F(t);
                            if (xVar.a() >= 4) {
                                if (z.g(xVar.a, xVar.f980b) == 443) {
                                    xVar.F(4);
                                    int y2 = xVar.y();
                                    if (xVar.a() < y2) {
                                        xVar.E(i3);
                                    } else {
                                        xVar.F(y2);
                                    }
                                }
                                while (true) {
                                    if (xVar.a() < 4 || (g = z.g(xVar.a, xVar.f980b)) == 442 || g == 441 || (g >>> 8) != 1) {
                                        break;
                                    }
                                    xVar.F(4);
                                    if (xVar.a() < 2) {
                                        xVar.E(i3);
                                        break;
                                    }
                                    xVar.E(Math.min(xVar.c, xVar.f980b + xVar.y()));
                                }
                            } else {
                                xVar.E(i3);
                            }
                        } else {
                            xVar.E(i3);
                        }
                    } else {
                        xVar.E(i3);
                    }
                    i = xVar.f980b;
                }
            }
            if (j2 != -9223372036854775807L) {
                return a.e.c(j2, position + i);
            }
            return a.e.a;
        }
    }

    public z(d0 d0Var, long j, long j2) {
        super(new a.b(), new b(d0Var, null), j, 0L, j + 1, 0L, j2, 188L, 1000);
    }

    public static int g(byte[] bArr, int i) {
        return (bArr[i + 3] & 255) | ((bArr[i] & 255) << 24) | ((bArr[i + 1] & 255) << 16) | ((bArr[i + 2] & 255) << 8);
    }
}
