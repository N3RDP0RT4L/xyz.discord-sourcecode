package b.i.a.c.x2.k0;

import b.i.a.c.f3.u;
import b.i.a.c.f3.x;
import b.i.a.c.x2.j;
import b.i.a.c.x2.k0.i0;
import b.i.a.c.x2.w;
import org.checkerframework.checker.nullness.qual.RequiresNonNull;
/* compiled from: H265Reader.java */
/* loaded from: classes3.dex */
public final class s implements o {
    public final e0 a;

    /* renamed from: b  reason: collision with root package name */
    public String f1271b;
    public w c;
    public a d;
    public boolean e;
    public long l;
    public final boolean[] f = new boolean[3];
    public final w g = new w(32, 128);
    public final w h = new w(33, 128);
    public final w i = new w(34, 128);
    public final w j = new w(39, 128);
    public final w k = new w(40, 128);
    public long m = -9223372036854775807L;
    public final x n = new x();

    /* compiled from: H265Reader.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public final w a;

        /* renamed from: b  reason: collision with root package name */
        public long f1272b;
        public boolean c;
        public int d;
        public long e;
        public boolean f;
        public boolean g;
        public boolean h;
        public boolean i;
        public boolean j;
        public long k;
        public long l;
        public boolean m;

        public a(w wVar) {
            this.a = wVar;
        }

        public final void a(int i) {
            long j = this.l;
            if (j != -9223372036854775807L) {
                boolean z2 = this.m;
                this.a.d(j, z2 ? 1 : 0, (int) (this.f1272b - this.k), i, null);
            }
        }
    }

    public s(e0 e0Var) {
        this.a = e0Var;
    }

    @RequiresNonNull({"sampleReader"})
    public final void a(byte[] bArr, int i, int i2) {
        a aVar = this.d;
        if (aVar.f) {
            int i3 = aVar.d;
            int i4 = (i + 2) - i3;
            if (i4 < i2) {
                aVar.g = (bArr[i4] & 128) != 0;
                aVar.f = false;
            } else {
                aVar.d = (i2 - i) + i3;
            }
        }
        if (!this.e) {
            this.g.a(bArr, i, i2);
            this.h.a(bArr, i, i2);
            this.i.a(bArr, i, i2);
        }
        this.j.a(bArr, i, i2);
        this.k.a(bArr, i, i2);
    }

    /* JADX WARN: Removed duplicated region for block: B:168:0x035d  */
    /* JADX WARN: Removed duplicated region for block: B:169:0x0382  */
    /* JADX WARN: Removed duplicated region for block: B:172:0x038c  */
    /* JADX WARN: Removed duplicated region for block: B:180:0x03d1  */
    /* JADX WARN: Removed duplicated region for block: B:211:0x0419  */
    /* JADX WARN: Removed duplicated region for block: B:219:0x0428 A[SYNTHETIC] */
    @Override // b.i.a.c.x2.k0.o
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void b(b.i.a.c.f3.x r33) {
        /*
            Method dump skipped, instructions count: 1089
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.k0.s.b(b.i.a.c.f3.x):void");
    }

    @Override // b.i.a.c.x2.k0.o
    public void c() {
        this.l = 0L;
        this.m = -9223372036854775807L;
        u.a(this.f);
        this.g.c();
        this.h.c();
        this.i.c();
        this.j.c();
        this.k.c();
        a aVar = this.d;
        if (aVar != null) {
            aVar.f = false;
            aVar.g = false;
            aVar.h = false;
            aVar.i = false;
            aVar.j = false;
        }
    }

    @Override // b.i.a.c.x2.k0.o
    public void d() {
    }

    @Override // b.i.a.c.x2.k0.o
    public void e(j jVar, i0.d dVar) {
        dVar.a();
        this.f1271b = dVar.b();
        w p = jVar.p(dVar.c(), 2);
        this.c = p;
        this.d = new a(p);
        this.a.a(jVar, dVar);
    }

    @Override // b.i.a.c.x2.k0.o
    public void f(long j, int i) {
        if (j != -9223372036854775807L) {
            this.m = j;
        }
    }
}
