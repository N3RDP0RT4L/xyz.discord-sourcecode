package b.i.a.c.x2.k0;

import androidx.annotation.Nullable;
import b.i.a.c.f3.u;
import b.i.a.c.f3.x;
import b.i.a.c.x2.j;
import b.i.a.c.x2.k0.i0;
import b.i.a.c.x2.w;
import java.util.Arrays;
import org.objectweb.asm.Opcodes;
/* compiled from: H262Reader.java */
/* loaded from: classes3.dex */
public final class p implements o {
    public static final double[] a = {23.976023976023978d, 24.0d, 25.0d, 29.97002997002997d, 30.0d, 50.0d, 59.94005994005994d, 60.0d};

    /* renamed from: b  reason: collision with root package name */
    public String f1263b;
    public w c;
    @Nullable
    public final j0 d;
    @Nullable
    public final x e;
    @Nullable
    public final w f;
    public final boolean[] g = new boolean[4];
    public final a h = new a(128);
    public long i;
    public boolean j;
    public boolean k;
    public long l;
    public long m;
    public long n;
    public long o;
    public boolean p;
    public boolean q;

    /* compiled from: H262Reader.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public static final byte[] a = {0, 0, 1};

        /* renamed from: b  reason: collision with root package name */
        public boolean f1264b;
        public int c;
        public int d;
        public byte[] e;

        public a(int i) {
            this.e = new byte[i];
        }

        public void a(byte[] bArr, int i, int i2) {
            if (this.f1264b) {
                int i3 = i2 - i;
                byte[] bArr2 = this.e;
                int length = bArr2.length;
                int i4 = this.c;
                if (length < i4 + i3) {
                    this.e = Arrays.copyOf(bArr2, (i4 + i3) * 2);
                }
                System.arraycopy(bArr, i, this.e, this.c, i3);
                this.c += i3;
            }
        }
    }

    public p(@Nullable j0 j0Var) {
        this.d = j0Var;
        if (j0Var != null) {
            this.f = new w(Opcodes.GETSTATIC, 128);
            this.e = new x();
        } else {
            this.f = null;
            this.e = null;
        }
        this.m = -9223372036854775807L;
        this.o = -9223372036854775807L;
    }

    /* JADX WARN: Removed duplicated region for block: B:31:0x0080  */
    /* JADX WARN: Removed duplicated region for block: B:44:0x00f4  */
    /* JADX WARN: Removed duplicated region for block: B:56:0x0150  */
    /* JADX WARN: Removed duplicated region for block: B:67:0x0193  */
    /* JADX WARN: Removed duplicated region for block: B:69:0x0197  */
    /* JADX WARN: Removed duplicated region for block: B:77:0x01b2  */
    /* JADX WARN: Removed duplicated region for block: B:81:0x01bc  */
    /* JADX WARN: Removed duplicated region for block: B:85:0x01df  */
    /* JADX WARN: Removed duplicated region for block: B:90:0x01f4  */
    /* JADX WARN: Removed duplicated region for block: B:96:0x020b  */
    /* JADX WARN: Removed duplicated region for block: B:97:0x020d  */
    @Override // b.i.a.c.x2.k0.o
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void b(b.i.a.c.f3.x r23) {
        /*
            Method dump skipped, instructions count: 533
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.k0.p.b(b.i.a.c.f3.x):void");
    }

    @Override // b.i.a.c.x2.k0.o
    public void c() {
        u.a(this.g);
        a aVar = this.h;
        aVar.f1264b = false;
        aVar.c = 0;
        aVar.d = 0;
        w wVar = this.f;
        if (wVar != null) {
            wVar.c();
        }
        this.i = 0L;
        this.j = false;
        this.m = -9223372036854775807L;
        this.o = -9223372036854775807L;
    }

    @Override // b.i.a.c.x2.k0.o
    public void d() {
    }

    @Override // b.i.a.c.x2.k0.o
    public void e(j jVar, i0.d dVar) {
        dVar.a();
        this.f1263b = dVar.b();
        this.c = jVar.p(dVar.c(), 2);
        j0 j0Var = this.d;
        if (j0Var != null) {
            j0Var.b(jVar, dVar);
        }
    }

    @Override // b.i.a.c.x2.k0.o
    public void f(long j, int i) {
        this.m = j;
    }
}
