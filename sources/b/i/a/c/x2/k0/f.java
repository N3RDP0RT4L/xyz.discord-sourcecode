package b.i.a.c.x2.k0;

import b.i.a.c.f3.x;
import b.i.a.c.x2.h;
import b.i.a.c.x2.i;
import b.i.a.c.x2.j;
import b.i.a.c.x2.k0.i0;
import b.i.a.c.x2.s;
import b.i.a.c.x2.t;
import java.io.IOException;
/* compiled from: Ac3Extractor.java */
/* loaded from: classes3.dex */
public final class f implements h {
    public final g a = new g(null);

    /* renamed from: b  reason: collision with root package name */
    public final x f1243b = new x(2786);
    public boolean c;

    static {
        a aVar = a.a;
    }

    /* JADX WARN: Code restructure failed: missing block: B:10:0x003d, code lost:
        if ((r5 - r3) < 8192) goto L12;
     */
    /* JADX WARN: Code restructure failed: missing block: B:11:0x003f, code lost:
        return false;
     */
    /* JADX WARN: Code restructure failed: missing block: B:9:0x0034, code lost:
        r14.k();
        r5 = r5 + 1;
     */
    @Override // b.i.a.c.x2.h
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public boolean b(b.i.a.c.x2.i r14) throws java.io.IOException {
        /*
            r13 = this;
            b.i.a.c.f3.x r0 = new b.i.a.c.f3.x
            r1 = 10
            r0.<init>(r1)
            r2 = 0
            r3 = 0
        L9:
            byte[] r4 = r0.a
            r14.o(r4, r2, r1)
            r0.E(r2)
            int r4 = r0.v()
            r5 = 4801587(0x494433, float:6.728456E-39)
            r6 = 3
            if (r4 == r5) goto L85
            r14.k()
            r14.g(r3)
            r5 = r3
        L22:
            r4 = 0
        L23:
            byte[] r7 = r0.a
            r8 = 6
            r14.o(r7, r2, r8)
            r0.E(r2)
            int r7 = r0.y()
            r9 = 2935(0xb77, float:4.113E-42)
            if (r7 == r9) goto L44
            r14.k()
            int r5 = r5 + 1
            int r4 = r5 - r3
            r7 = 8192(0x2000, float:1.14794E-41)
            if (r4 < r7) goto L40
            return r2
        L40:
            r14.g(r5)
            goto L22
        L44:
            r7 = 1
            int r4 = r4 + r7
            r9 = 4
            if (r4 < r9) goto L4a
            return r7
        L4a:
            byte[] r10 = r0.a
            int r11 = r10.length
            r12 = -1
            if (r11 >= r8) goto L52
            r9 = -1
            goto L7c
        L52:
            r11 = 5
            r11 = r10[r11]
            r11 = r11 & 248(0xf8, float:3.48E-43)
            int r11 = r11 >> r6
            if (r11 <= r1) goto L5c
            r11 = 1
            goto L5d
        L5c:
            r11 = 0
        L5d:
            if (r11 == 0) goto L6f
            r8 = 2
            r9 = r10[r8]
            r9 = r9 & 7
            int r9 = r9 << 8
            r10 = r10[r6]
            r10 = r10 & 255(0xff, float:3.57E-43)
            r9 = r9 | r10
            int r9 = r9 + r7
            int r9 = r9 * 2
            goto L7c
        L6f:
            r7 = r10[r9]
            r7 = r7 & 192(0xc0, float:2.69E-43)
            int r7 = r7 >> r8
            r8 = r10[r9]
            r8 = r8 & 63
            int r9 = b.i.a.c.t2.m.a(r7, r8)
        L7c:
            if (r9 != r12) goto L7f
            return r2
        L7f:
            int r9 = r9 + (-6)
            r14.g(r9)
            goto L23
        L85:
            r0.F(r6)
            int r4 = r0.s()
            int r5 = r4 + 10
            int r3 = r3 + r5
            r14.g(r4)
            goto L9
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.k0.f.b(b.i.a.c.x2.i):boolean");
    }

    @Override // b.i.a.c.x2.h
    public int e(i iVar, s sVar) throws IOException {
        int read = iVar.read(this.f1243b.a, 0, 2786);
        if (read == -1) {
            return -1;
        }
        this.f1243b.E(0);
        this.f1243b.D(read);
        if (!this.c) {
            this.a.f(0L, 4);
            this.c = true;
        }
        this.a.b(this.f1243b);
        return 0;
    }

    @Override // b.i.a.c.x2.h
    public void f(j jVar) {
        this.a.e(jVar, new i0.d(Integer.MIN_VALUE, 0, 1));
        jVar.j();
        jVar.a(new t.b(-9223372036854775807L, 0L));
    }

    @Override // b.i.a.c.x2.h
    public void g(long j, long j2) {
        this.c = false;
        this.a.c();
    }

    @Override // b.i.a.c.x2.h
    public void release() {
    }
}
