package b.i.a.c.x2.k0;

import androidx.core.view.InputDeviceCompat;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.x;
import b.i.a.c.x2.j;
import b.i.a.c.x2.k0.i0;
/* compiled from: SectionReader.java */
/* loaded from: classes3.dex */
public final class d0 implements i0 {
    public final c0 a;

    /* renamed from: b  reason: collision with root package name */
    public final x f1241b = new x(32);
    public int c;
    public int d;
    public boolean e;
    public boolean f;

    public d0(c0 c0Var) {
        this.a = c0Var;
    }

    @Override // b.i.a.c.x2.k0.i0
    public void a(b.i.a.c.f3.d0 d0Var, j jVar, i0.d dVar) {
        this.a.a(d0Var, jVar, dVar);
        this.f = true;
    }

    @Override // b.i.a.c.x2.k0.i0
    public void b(x xVar, int i) {
        boolean z2 = (i & 1) != 0;
        int t = z2 ? xVar.f980b + xVar.t() : -1;
        if (this.f) {
            if (z2) {
                this.f = false;
                xVar.E(t);
                this.d = 0;
            } else {
                return;
            }
        }
        while (xVar.a() > 0) {
            int i2 = this.d;
            if (i2 < 3) {
                if (i2 == 0) {
                    int t2 = xVar.t();
                    xVar.E(xVar.f980b - 1);
                    if (t2 == 255) {
                        this.f = true;
                        return;
                    }
                }
                int min = Math.min(xVar.a(), 3 - this.d);
                xVar.e(this.f1241b.a, this.d, min);
                int i3 = this.d + min;
                this.d = i3;
                if (i3 == 3) {
                    this.f1241b.E(0);
                    this.f1241b.D(3);
                    this.f1241b.F(1);
                    int t3 = this.f1241b.t();
                    int t4 = this.f1241b.t();
                    this.e = (t3 & 128) != 0;
                    int i4 = (((t3 & 15) << 8) | t4) + 3;
                    this.c = i4;
                    byte[] bArr = this.f1241b.a;
                    if (bArr.length < i4) {
                        this.f1241b.b(Math.min((int) InputDeviceCompat.SOURCE_TOUCHSCREEN, Math.max(i4, bArr.length * 2)));
                    }
                }
            } else {
                int min2 = Math.min(xVar.a(), this.c - this.d);
                xVar.e(this.f1241b.a, this.d, min2);
                int i5 = this.d + min2;
                this.d = i5;
                int i6 = this.c;
                if (i5 != i6) {
                    continue;
                } else {
                    if (this.e) {
                        byte[] bArr2 = this.f1241b.a;
                        int i7 = e0.a;
                        int i8 = -1;
                        for (int i9 = 0; i9 < i6; i9++) {
                            i8 = e0.k[((i8 >>> 24) ^ (bArr2[i9] & 255)) & 255] ^ (i8 << 8);
                        }
                        if (i8 != 0) {
                            this.f = true;
                            return;
                        }
                        this.f1241b.D(this.c - 4);
                    } else {
                        this.f1241b.D(i6);
                    }
                    this.f1241b.E(0);
                    this.a.b(this.f1241b);
                    this.d = 0;
                }
            }
        }
    }

    @Override // b.i.a.c.x2.k0.i0
    public void c() {
        this.f = true;
    }
}
