package b.i.a.c.x2.g0;

import android.util.Log;
import android.util.SparseArray;
import androidx.annotation.CallSuper;
import androidx.annotation.Nullable;
import androidx.constraintlayout.solver.widgets.analyzer.BasicMeasure;
import b.c.a.a0.d;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.r;
import b.i.a.c.f3.u;
import b.i.a.c.f3.x;
import b.i.a.c.x2.h;
import b.i.a.c.x2.i;
import b.i.a.c.x2.j;
import b.i.a.c.x2.w;
import com.discord.api.permission.Permission;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.drm.DrmInitData;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import org.checkerframework.checker.nullness.qual.EnsuresNonNull;
import org.checkerframework.checker.nullness.qual.RequiresNonNull;
/* compiled from: MatroskaExtractor.java */
/* loaded from: classes3.dex */
public class e implements h {
    public static final byte[] a = {49, 10, 48, 48, 58, 48, 48, 58, 48, 48, 44, 48, 48, 48, 32, 45, 45, 62, 32, 48, 48, 58, 48, 48, 58, 48, 48, 44, 48, 48, 48, 10};

    /* renamed from: b  reason: collision with root package name */
    public static final byte[] f1181b = e0.w("Format: Start, End, ReadOrder, Layer, Style, Name, MarginL, MarginR, MarginV, Effect, Text");
    public static final byte[] c = {68, 105, 97, 108, 111, 103, 117, 101, 58, 32, 48, 58, 48, 48, 58, 48, 48, 58, 48, 48, 44, 48, 58, 48, 48, 58, 48, 48, 58, 48, 48, 44};
    public static final UUID d = new UUID(72057594037932032L, -9223371306706625679L);
    public static final Map<String, Integer> e;
    public boolean A;
    public int B;
    public long C;
    public boolean D;
    @Nullable
    public r H;
    @Nullable
    public r I;
    public boolean J;
    public boolean K;
    public int L;
    public long M;
    public long N;
    public int O;
    public int P;
    public int[] Q;
    public int R;
    public int S;
    public int T;
    public int U;
    public boolean V;
    public int W;
    public int X;
    public int Y;
    public boolean Z;

    /* renamed from: a0  reason: collision with root package name */
    public boolean f1182a0;

    /* renamed from: b0  reason: collision with root package name */
    public boolean f1183b0;

    /* renamed from: c0  reason: collision with root package name */
    public int f1184c0;

    /* renamed from: d0  reason: collision with root package name */
    public byte f1185d0;

    /* renamed from: e0  reason: collision with root package name */
    public boolean f1186e0;
    public final d f;

    /* renamed from: f0  reason: collision with root package name */
    public j f1187f0;
    public final g g;
    public final SparseArray<c> h;
    public final boolean i;
    public final x j;
    public final x k;
    public final x l;
    public final x m;
    public final x n;
    public final x o;
    public final x p;
    public final x q;
    public final x r;

    /* renamed from: s  reason: collision with root package name */
    public final x f1188s;
    public ByteBuffer t;
    public long u;
    @Nullable

    /* renamed from: z  reason: collision with root package name */
    public c f1191z;
    public long v = -1;
    public long w = -9223372036854775807L;

    /* renamed from: x  reason: collision with root package name */
    public long f1189x = -9223372036854775807L;

    /* renamed from: y  reason: collision with root package name */
    public long f1190y = -9223372036854775807L;
    public long E = -1;
    public long F = -1;
    public long G = -9223372036854775807L;

    /* compiled from: MatroskaExtractor.java */
    /* loaded from: classes3.dex */
    public final class b implements b.i.a.c.x2.g0.c {
        public b(a aVar) {
        }
    }

    static {
        b.i.a.c.x2.g0.a aVar = b.i.a.c.x2.g0.a.a;
        HashMap hashMap = new HashMap();
        hashMap.put("htc_video_rotA-000", 0);
        hashMap.put("htc_video_rotA-090", 90);
        hashMap.put("htc_video_rotA-180", 180);
        hashMap.put("htc_video_rotA-270", 270);
        e = Collections.unmodifiableMap(hashMap);
    }

    public e(int i) {
        b.i.a.c.x2.g0.b bVar = new b.i.a.c.x2.g0.b();
        this.f = bVar;
        bVar.d = new b(null);
        this.i = (i & 1) == 0;
        this.g = new g();
        this.h = new SparseArray<>();
        this.l = new x(4);
        this.m = new x(ByteBuffer.allocate(4).putInt(-1).array());
        this.n = new x(4);
        this.j = new x(u.a);
        this.k = new x(4);
        this.o = new x();
        this.p = new x();
        this.q = new x(8);
        this.r = new x();
        this.f1188s = new x();
        this.Q = new int[1];
    }

    public static int[] i(@Nullable int[] iArr, int i) {
        if (iArr == null) {
            return new int[i];
        }
        return iArr.length >= i ? iArr : new int[Math.max(iArr.length * 2, i)];
    }

    public static byte[] j(long j, String str, long j2) {
        d.j(j != -9223372036854775807L);
        int i = (int) (j / 3600000000L);
        long j3 = j - ((i * 3600) * 1000000);
        int i2 = (int) (j3 / 60000000);
        long j4 = j3 - ((i2 * 60) * 1000000);
        int i3 = (int) (j4 / 1000000);
        return e0.w(String.format(Locale.US, str, Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf((int) ((j4 - (i3 * 1000000)) / j2))));
    }

    @EnsuresNonNull({"cueTimesUs", "cueClusterPositions"})
    public final void a(int i) throws ParserException {
        if (this.H == null || this.I == null) {
            StringBuilder sb = new StringBuilder(37);
            sb.append("Element ");
            sb.append(i);
            sb.append(" must be in a Cues");
            throw ParserException.a(sb.toString(), null);
        }
    }

    @Override // b.i.a.c.x2.h
    public final boolean b(i iVar) throws IOException {
        long a2;
        int i;
        f fVar = new f();
        long b2 = iVar.b();
        long j = Permission.VIEW_CHANNEL;
        int i2 = (b2 > (-1L) ? 1 : (b2 == (-1L) ? 0 : -1));
        if (i2 != 0 && b2 <= Permission.VIEW_CHANNEL) {
            j = b2;
        }
        int i3 = (int) j;
        iVar.o(fVar.a.a, 0, 4);
        fVar.f1197b = 4;
        for (long u = fVar.a.u(); u != 440786851; u = ((u << 8) & (-256)) | (fVar.a.a[0] & 255)) {
            int i4 = fVar.f1197b + 1;
            fVar.f1197b = i4;
            if (i4 == i3) {
                return false;
            }
            iVar.o(fVar.a.a, 0, 1);
        }
        long a3 = fVar.a(iVar);
        long j2 = fVar.f1197b;
        if (a3 == Long.MIN_VALUE) {
            return false;
        }
        if (i2 != 0 && j2 + a3 >= b2) {
            return false;
        }
        while (true) {
            int i5 = (fVar.f1197b > (j2 + a3) ? 1 : (fVar.f1197b == (j2 + a3) ? 0 : -1));
            if (i5 >= 0) {
                return i5 == 0;
            }
            if (fVar.a(iVar) == Long.MIN_VALUE || (a2 = fVar.a(iVar)) < 0 || a2 > 2147483647L) {
                return false;
            }
            if (i != 0) {
                int i6 = (int) a2;
                iVar.g(i6);
                fVar.f1197b += i6;
            }
        }
    }

    @EnsuresNonNull({"currentTrack"})
    public final void c(int i) throws ParserException {
        if (this.f1191z == null) {
            StringBuilder sb = new StringBuilder(43);
            sb.append("Element ");
            sb.append(i);
            sb.append(" must be in a TrackEntry");
            throw ParserException.a(sb.toString(), null);
        }
    }

    @RequiresNonNull({"#1.output"})
    public final void d(c cVar, long j, int i, int i2, int i3) {
        int i4;
        byte[] bArr;
        b.i.a.c.x2.x xVar = cVar.T;
        if (xVar != null) {
            xVar.b(cVar.X, j, i, i2, i3, cVar.j);
        } else {
            if ("S_TEXT/UTF8".equals(cVar.f1192b) || "S_TEXT/ASS".equals(cVar.f1192b)) {
                if (this.P > 1) {
                    Log.w("MatroskaExtractor", "Skipping subtitle sample in laced block.");
                } else {
                    long j2 = this.N;
                    if (j2 == -9223372036854775807L) {
                        Log.w("MatroskaExtractor", "Skipping subtitle sample with no duration.");
                    } else {
                        String str = cVar.f1192b;
                        byte[] bArr2 = this.p.a;
                        str.hashCode();
                        if (str.equals("S_TEXT/ASS")) {
                            bArr = j(j2, "%01d:%02d:%02d:%02d", 10000L);
                            i4 = 21;
                        } else if (str.equals("S_TEXT/UTF8")) {
                            bArr = j(j2, "%02d:%02d:%02d,%03d", 1000L);
                            i4 = 19;
                        } else {
                            throw new IllegalArgumentException();
                        }
                        System.arraycopy(bArr, 0, bArr2, i4, bArr.length);
                        int i5 = this.p.f980b;
                        while (true) {
                            x xVar2 = this.p;
                            if (i5 >= xVar2.c) {
                                break;
                            } else if (xVar2.a[i5] == 0) {
                                xVar2.D(i5);
                                break;
                            } else {
                                i5++;
                            }
                        }
                        w wVar = cVar.X;
                        x xVar3 = this.p;
                        wVar.c(xVar3, xVar3.c);
                        i2 += this.p.c;
                    }
                }
            }
            if ((268435456 & i) != 0) {
                if (this.P > 1) {
                    i &= -268435457;
                } else {
                    x xVar4 = this.f1188s;
                    int i6 = xVar4.c;
                    cVar.X.f(xVar4, i6, 2);
                    i2 += i6;
                }
            }
            cVar.X.d(j, i, i2, i3, cVar.j);
        }
        this.K = true;
    }

    /* JADX WARN: Code restructure failed: missing block: B:183:0x0425, code lost:
        throw com.google.android.exoplayer2.ParserException.a("EBML lacing sample size out of range.", null);
     */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:430:0x08d3 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:442:0x08d5 A[SYNTHETIC] */
    /* JADX WARN: Type inference failed for: r3v121 */
    /* JADX WARN: Type inference failed for: r3v146 */
    /* JADX WARN: Type inference failed for: r3v4, types: [boolean, int] */
    /* JADX WARN: Type inference failed for: r4v125 */
    /* JADX WARN: Type inference failed for: r4v137 */
    /* JADX WARN: Type inference failed for: r4v3, types: [boolean, int] */
    @Override // b.i.a.c.x2.h
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final int e(b.i.a.c.x2.i r29, b.i.a.c.x2.s r30) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 2858
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.g0.e.e(b.i.a.c.x2.i, b.i.a.c.x2.s):int");
    }

    @Override // b.i.a.c.x2.h
    public final void f(j jVar) {
        this.f1187f0 = jVar;
    }

    @Override // b.i.a.c.x2.h
    @CallSuper
    public void g(long j, long j2) {
        this.G = -9223372036854775807L;
        this.L = 0;
        b.i.a.c.x2.g0.b bVar = (b.i.a.c.x2.g0.b) this.f;
        bVar.e = 0;
        bVar.f1179b.clear();
        g gVar = bVar.c;
        gVar.c = 0;
        gVar.d = 0;
        g gVar2 = this.g;
        gVar2.c = 0;
        gVar2.d = 0;
        l();
        for (int i = 0; i < this.h.size(); i++) {
            b.i.a.c.x2.x xVar = this.h.valueAt(i).T;
            if (xVar != null) {
                xVar.f1296b = false;
                xVar.c = 0;
            }
        }
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Code restructure failed: missing block: B:403:0x0807, code lost:
        if (r3.m() == r5.getLeastSignificantBits()) goto L404;
     */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:407:0x080e  */
    /* JADX WARN: Removed duplicated region for block: B:411:0x083c  */
    /* JADX WARN: Removed duplicated region for block: B:425:0x0866  */
    /* JADX WARN: Removed duplicated region for block: B:430:0x087d  */
    /* JADX WARN: Removed duplicated region for block: B:431:0x087f  */
    /* JADX WARN: Removed duplicated region for block: B:434:0x088c  */
    /* JADX WARN: Removed duplicated region for block: B:435:0x0899  */
    /* JADX WARN: Removed duplicated region for block: B:492:0x09e5  */
    /* JADX WARN: Removed duplicated region for block: B:493:0x09e7  */
    /* JADX WARN: Removed duplicated region for block: B:521:0x0a69  */
    /* JADX WARN: Type inference failed for: r0v16, types: [java.lang.Throwable] */
    /* JADX WARN: Type inference failed for: r0v18 */
    /* JADX WARN: Type inference failed for: r0v9, types: [java.lang.Object, b.i.a.c.x2.g0.e$c] */
    @androidx.annotation.CallSuper
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void h(int r23) throws com.google.android.exoplayer2.ParserException {
        /*
            Method dump skipped, instructions count: 3212
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.g0.e.h(int):void");
    }

    public final void k(i iVar, int i) throws IOException {
        x xVar = this.l;
        if (xVar.c < i) {
            byte[] bArr = xVar.a;
            if (bArr.length < i) {
                xVar.b(Math.max(bArr.length * 2, i));
            }
            x xVar2 = this.l;
            byte[] bArr2 = xVar2.a;
            int i2 = xVar2.c;
            iVar.readFully(bArr2, i2, i - i2);
            this.l.D(i);
        }
    }

    public final void l() {
        this.W = 0;
        this.X = 0;
        this.Y = 0;
        this.Z = false;
        this.f1182a0 = false;
        this.f1183b0 = false;
        this.f1184c0 = 0;
        this.f1185d0 = (byte) 0;
        this.f1186e0 = false;
        this.o.A(0);
    }

    public final long m(long j) throws ParserException {
        long j2 = this.w;
        if (j2 != -9223372036854775807L) {
            return e0.F(j, j2, 1000L);
        }
        throw ParserException.a("Can't scale timecode prior to timecodeScale being set.", null);
    }

    @RequiresNonNull({"#2.output"})
    public final int n(i iVar, c cVar, int i) throws IOException {
        int i2;
        if ("S_TEXT/UTF8".equals(cVar.f1192b)) {
            o(iVar, a, i);
            int i3 = this.X;
            l();
            return i3;
        } else if ("S_TEXT/ASS".equals(cVar.f1192b)) {
            o(iVar, c, i);
            int i4 = this.X;
            l();
            return i4;
        } else {
            w wVar = cVar.X;
            boolean z2 = true;
            if (!this.Z) {
                if (cVar.h) {
                    this.T &= -1073741825;
                    int i5 = 128;
                    if (!this.f1182a0) {
                        iVar.readFully(this.l.a, 0, 1);
                        this.W++;
                        byte[] bArr = this.l.a;
                        if ((bArr[0] & 128) != 128) {
                            this.f1185d0 = bArr[0];
                            this.f1182a0 = true;
                        } else {
                            throw ParserException.a("Extension bit is set in signal byte", null);
                        }
                    }
                    byte b2 = this.f1185d0;
                    if ((b2 & 1) == 1) {
                        boolean z3 = (b2 & 2) == 2;
                        this.T |= BasicMeasure.EXACTLY;
                        if (!this.f1186e0) {
                            iVar.readFully(this.q.a, 0, 8);
                            this.W += 8;
                            this.f1186e0 = true;
                            x xVar = this.l;
                            byte[] bArr2 = xVar.a;
                            if (!z3) {
                                i5 = 0;
                            }
                            bArr2[0] = (byte) (i5 | 8);
                            xVar.E(0);
                            wVar.f(this.l, 1, 1);
                            this.X++;
                            this.q.E(0);
                            wVar.f(this.q, 8, 1);
                            this.X += 8;
                        }
                        if (z3) {
                            if (!this.f1183b0) {
                                iVar.readFully(this.l.a, 0, 1);
                                this.W++;
                                this.l.E(0);
                                this.f1184c0 = this.l.t();
                                this.f1183b0 = true;
                            }
                            int i6 = this.f1184c0 * 4;
                            this.l.A(i6);
                            iVar.readFully(this.l.a, 0, i6);
                            this.W += i6;
                            short s2 = (short) ((this.f1184c0 / 2) + 1);
                            int i7 = (s2 * 6) + 2;
                            ByteBuffer byteBuffer = this.t;
                            if (byteBuffer == null || byteBuffer.capacity() < i7) {
                                this.t = ByteBuffer.allocate(i7);
                            }
                            this.t.position(0);
                            this.t.putShort(s2);
                            int i8 = 0;
                            int i9 = 0;
                            while (true) {
                                i2 = this.f1184c0;
                                if (i8 >= i2) {
                                    break;
                                }
                                int w = this.l.w();
                                if (i8 % 2 == 0) {
                                    this.t.putShort((short) (w - i9));
                                } else {
                                    this.t.putInt(w - i9);
                                }
                                i8++;
                                i9 = w;
                            }
                            int i10 = (i - this.W) - i9;
                            if (i2 % 2 == 1) {
                                this.t.putInt(i10);
                            } else {
                                this.t.putShort((short) i10);
                                this.t.putInt(0);
                            }
                            this.r.C(this.t.array(), i7);
                            wVar.f(this.r, i7, 1);
                            this.X += i7;
                        }
                    }
                } else {
                    byte[] bArr3 = cVar.i;
                    if (bArr3 != null) {
                        x xVar2 = this.o;
                        int length = bArr3.length;
                        xVar2.a = bArr3;
                        xVar2.c = length;
                        xVar2.f980b = 0;
                    }
                }
                if (cVar.f > 0) {
                    this.T |= 268435456;
                    this.f1188s.A(0);
                    this.l.A(4);
                    x xVar3 = this.l;
                    byte[] bArr4 = xVar3.a;
                    bArr4[0] = (byte) ((i >> 24) & 255);
                    bArr4[1] = (byte) ((i >> 16) & 255);
                    bArr4[2] = (byte) ((i >> 8) & 255);
                    bArr4[3] = (byte) (i & 255);
                    wVar.f(xVar3, 4, 2);
                    this.X += 4;
                }
                this.Z = true;
            }
            int i11 = i + this.o.c;
            if (!"V_MPEG4/ISO/AVC".equals(cVar.f1192b) && !"V_MPEGH/ISO/HEVC".equals(cVar.f1192b)) {
                if (cVar.T != null) {
                    if (this.o.c != 0) {
                        z2 = false;
                    }
                    d.D(z2);
                    cVar.T.c(iVar);
                }
                while (true) {
                    int i12 = this.W;
                    if (i12 >= i11) {
                        break;
                    }
                    int p = p(iVar, wVar, i11 - i12);
                    this.W += p;
                    this.X += p;
                }
            } else {
                byte[] bArr5 = this.k.a;
                bArr5[0] = 0;
                bArr5[1] = 0;
                bArr5[2] = 0;
                int i13 = cVar.Y;
                int i14 = 4 - i13;
                while (this.W < i11) {
                    int i15 = this.Y;
                    if (i15 == 0) {
                        int min = Math.min(i13, this.o.a());
                        iVar.readFully(bArr5, i14 + min, i13 - min);
                        if (min > 0) {
                            x xVar4 = this.o;
                            System.arraycopy(xVar4.a, xVar4.f980b, bArr5, i14, min);
                            xVar4.f980b += min;
                        }
                        this.W += i13;
                        this.k.E(0);
                        this.Y = this.k.w();
                        this.j.E(0);
                        wVar.c(this.j, 4);
                        this.X += 4;
                    } else {
                        int p2 = p(iVar, wVar, i15);
                        this.W += p2;
                        this.X += p2;
                        this.Y -= p2;
                    }
                }
            }
            if ("A_VORBIS".equals(cVar.f1192b)) {
                this.m.E(0);
                wVar.c(this.m, 4);
                this.X += 4;
            }
            int i16 = this.X;
            l();
            return i16;
        }
    }

    public final void o(i iVar, byte[] bArr, int i) throws IOException {
        int length = bArr.length + i;
        x xVar = this.p;
        byte[] bArr2 = xVar.a;
        if (bArr2.length < length) {
            xVar.B(Arrays.copyOf(bArr, length + i));
        } else {
            System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        }
        iVar.readFully(this.p.a, bArr.length, i);
        this.p.E(0);
        this.p.D(length);
    }

    public final int p(i iVar, w wVar, int i) throws IOException {
        int a2 = this.o.a();
        if (a2 <= 0) {
            return wVar.b(iVar, i, false);
        }
        int min = Math.min(i, a2);
        wVar.c(this.o, min);
        return min;
    }

    @Override // b.i.a.c.x2.h
    public final void release() {
    }

    /* compiled from: MatroskaExtractor.java */
    /* loaded from: classes3.dex */
    public static final class c {
        public byte[] N;
        public b.i.a.c.x2.x T;
        public boolean U;
        public w X;
        public int Y;
        public String a;

        /* renamed from: b  reason: collision with root package name */
        public String f1192b;
        public int c;
        public int d;
        public int e;
        public int f;
        public int g;
        public boolean h;
        public byte[] i;
        public w.a j;
        public byte[] k;
        public DrmInitData l;
        public int m = -1;
        public int n = -1;
        public int o = -1;
        public int p = -1;
        public int q = 0;
        public int r = -1;

        /* renamed from: s  reason: collision with root package name */
        public float f1193s = 0.0f;
        public float t = 0.0f;
        public float u = 0.0f;
        public byte[] v = null;
        public int w = -1;

        /* renamed from: x  reason: collision with root package name */
        public boolean f1194x = false;

        /* renamed from: y  reason: collision with root package name */
        public int f1195y = -1;

        /* renamed from: z  reason: collision with root package name */
        public int f1196z = -1;
        public int A = -1;
        public int B = 1000;
        public int C = 200;
        public float D = -1.0f;
        public float E = -1.0f;
        public float F = -1.0f;
        public float G = -1.0f;
        public float H = -1.0f;
        public float I = -1.0f;
        public float J = -1.0f;
        public float K = -1.0f;
        public float L = -1.0f;
        public float M = -1.0f;
        public int O = 1;
        public int P = -1;
        public int Q = 8000;
        public long R = 0;
        public long S = 0;
        public boolean V = true;
        public String W = "eng";

        public c() {
        }

        @EnsuresNonNull({"codecPrivate"})
        public final byte[] a(String str) throws ParserException {
            byte[] bArr = this.k;
            if (bArr != null) {
                return bArr;
            }
            String valueOf = String.valueOf(str);
            throw ParserException.a(valueOf.length() != 0 ? "Missing CodecPrivate for codec ".concat(valueOf) : new String("Missing CodecPrivate for codec "), null);
        }

        public c(a aVar) {
        }
    }
}
