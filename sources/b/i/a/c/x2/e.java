package b.i.a.c.x2;

import b.i.a.c.e3.h;
import b.i.a.c.f3.e0;
import b.i.a.c.i1;
import java.io.EOFException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.Arrays;
/* compiled from: DefaultExtractorInput.java */
/* loaded from: classes3.dex */
public final class e implements i {

    /* renamed from: b  reason: collision with root package name */
    public final h f1166b;
    public final long c;
    public long d;
    public int f;
    public int g;
    public byte[] e = new byte[65536];
    public final byte[] a = new byte[4096];

    static {
        i1.a("goog.exo.extractor");
    }

    public e(h hVar, long j, long j2) {
        this.f1166b = hVar;
        this.d = j;
        this.c = j2;
    }

    @Override // b.i.a.c.x2.i
    public long b() {
        return this.c;
    }

    @Override // b.i.a.c.x2.i
    public boolean c(byte[] bArr, int i, int i2, boolean z2) throws IOException {
        int i3;
        int i4 = this.g;
        if (i4 == 0) {
            i3 = 0;
        } else {
            i3 = Math.min(i4, i2);
            System.arraycopy(this.e, 0, bArr, i, i3);
            s(i3);
        }
        int i5 = i3;
        while (i5 < i2 && i5 != -1) {
            i5 = r(bArr, i, i2, i5, z2);
        }
        p(i5);
        return i5 != -1;
    }

    @Override // b.i.a.c.x2.i
    public boolean e(byte[] bArr, int i, int i2, boolean z2) throws IOException {
        if (!m(i2, z2)) {
            return false;
        }
        System.arraycopy(this.e, this.f - i2, bArr, i, i2);
        return true;
    }

    @Override // b.i.a.c.x2.i
    public long f() {
        return this.d + this.f;
    }

    @Override // b.i.a.c.x2.i
    public void g(int i) throws IOException {
        m(i, false);
    }

    @Override // b.i.a.c.x2.i
    public long getPosition() {
        return this.d;
    }

    @Override // b.i.a.c.x2.i
    public int h(int i) throws IOException {
        int min = Math.min(this.g, i);
        s(min);
        if (min == 0) {
            byte[] bArr = this.a;
            min = r(bArr, 0, Math.min(i, bArr.length), 0, true);
        }
        p(min);
        return min;
    }

    @Override // b.i.a.c.x2.i
    public int i(byte[] bArr, int i, int i2) throws IOException {
        int i3;
        q(i2);
        int i4 = this.g;
        int i5 = this.f;
        int i6 = i4 - i5;
        if (i6 == 0) {
            i3 = r(this.e, i5, i2, 0, true);
            if (i3 == -1) {
                return -1;
            }
            this.g += i3;
        } else {
            i3 = Math.min(i2, i6);
        }
        System.arraycopy(this.e, this.f, bArr, i, i3);
        this.f += i3;
        return i3;
    }

    @Override // b.i.a.c.x2.i
    public void k() {
        this.f = 0;
    }

    @Override // b.i.a.c.x2.i
    public void l(int i) throws IOException {
        int min = Math.min(this.g, i);
        s(min);
        int i2 = min;
        while (i2 < i && i2 != -1) {
            i2 = r(this.a, -i2, Math.min(i, this.a.length + i2), i2, false);
        }
        p(i2);
    }

    @Override // b.i.a.c.x2.i
    public boolean m(int i, boolean z2) throws IOException {
        q(i);
        int i2 = this.g - this.f;
        while (i2 < i) {
            i2 = r(this.e, this.f, i, i2, z2);
            if (i2 == -1) {
                return false;
            }
            this.g = this.f + i2;
        }
        this.f += i;
        return true;
    }

    @Override // b.i.a.c.x2.i
    public void o(byte[] bArr, int i, int i2) throws IOException {
        e(bArr, i, i2, false);
    }

    public final void p(int i) {
        if (i != -1) {
            this.d += i;
        }
    }

    public final void q(int i) {
        int i2 = this.f + i;
        byte[] bArr = this.e;
        if (i2 > bArr.length) {
            this.e = Arrays.copyOf(this.e, e0.h(bArr.length * 2, 65536 + i2, i2 + 524288));
        }
    }

    public final int r(byte[] bArr, int i, int i2, int i3, boolean z2) throws IOException {
        if (!Thread.interrupted()) {
            int read = this.f1166b.read(bArr, i + i3, i2 - i3);
            if (read != -1) {
                return i3 + read;
            }
            if (i3 == 0 && z2) {
                return -1;
            }
            throw new EOFException();
        }
        throw new InterruptedIOException();
    }

    @Override // b.i.a.c.x2.i, b.i.a.c.e3.h
    public int read(byte[] bArr, int i, int i2) throws IOException {
        int i3 = this.g;
        int i4 = 0;
        if (i3 != 0) {
            int min = Math.min(i3, i2);
            System.arraycopy(this.e, 0, bArr, i, min);
            s(min);
            i4 = min;
        }
        if (i4 == 0) {
            i4 = r(bArr, i, i2, 0, true);
        }
        p(i4);
        return i4;
    }

    @Override // b.i.a.c.x2.i
    public void readFully(byte[] bArr, int i, int i2) throws IOException {
        c(bArr, i, i2, false);
    }

    public final void s(int i) {
        int i2 = this.g - i;
        this.g = i2;
        this.f = 0;
        byte[] bArr = this.e;
        byte[] bArr2 = i2 < bArr.length - 524288 ? new byte[65536 + i2] : bArr;
        System.arraycopy(bArr, i, bArr2, 0, i2);
        this.e = bArr2;
    }
}
