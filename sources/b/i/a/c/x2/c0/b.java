package b.i.a.c.x2.c0;

import b.i.a.c.f3.e0;
import b.i.a.c.x2.d;
import b.i.a.c.x2.h;
import b.i.a.c.x2.i;
import b.i.a.c.x2.j;
import b.i.a.c.x2.t;
import b.i.a.c.x2.w;
import java.io.IOException;
import java.util.Arrays;
/* compiled from: AmrExtractor.java */
/* loaded from: classes3.dex */
public final class b implements h {

    /* renamed from: b  reason: collision with root package name */
    public static final int[] f1161b;
    public static final int e;
    public final byte[] f;
    public final int g;
    public boolean h;
    public long i;
    public int j;
    public int k;
    public boolean l;
    public long m;
    public int n;
    public int o;
    public long p;
    public j q;
    public w r;

    /* renamed from: s  reason: collision with root package name */
    public t f1162s;
    public boolean t;
    public static final int[] a = {13, 14, 16, 18, 20, 21, 27, 32, 6, 7, 6, 6, 1, 1, 1, 1};
    public static final byte[] c = e0.w("#!AMR\n");
    public static final byte[] d = e0.w("#!AMR-WB\n");

    static {
        a aVar = a.a;
        int[] iArr = {18, 24, 33, 37, 41, 47, 51, 59, 61, 6, 1, 1, 1, 1, 1, 1};
        f1161b = iArr;
        e = iArr[8];
    }

    public b(int i) {
        this.g = (i & 2) != 0 ? i | 1 : i;
        this.f = new byte[1];
        this.n = -1;
    }

    /* JADX WARN: Code restructure failed: missing block: B:23:0x003a, code lost:
        if ((!r0 && (r5 < 12 || r5 > 14)) != false) goto L24;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final int a(b.i.a.c.x2.i r5) throws java.io.IOException {
        /*
            r4 = this;
            r5.k()
            byte[] r0 = r4.f
            r1 = 0
            r2 = 1
            r5.o(r0, r1, r2)
            byte[] r5 = r4.f
            r5 = r5[r1]
            r0 = r5 & 131(0x83, float:1.84E-43)
            r2 = 0
            if (r0 > 0) goto L7a
            int r5 = r5 >> 3
            r0 = 15
            r5 = r5 & r0
            if (r5 < 0) goto L3d
            if (r5 > r0) goto L3d
            boolean r0 = r4.h
            if (r0 == 0) goto L2a
            r3 = 10
            if (r5 < r3) goto L28
            r3 = 13
            if (r5 <= r3) goto L2a
        L28:
            r3 = 1
            goto L2b
        L2a:
            r3 = 0
        L2b:
            if (r3 != 0) goto L3c
            if (r0 != 0) goto L39
            r0 = 12
            if (r5 < r0) goto L37
            r0 = 14
            if (r5 <= r0) goto L39
        L37:
            r0 = 1
            goto L3a
        L39:
            r0 = 0
        L3a:
            if (r0 == 0) goto L3d
        L3c:
            r1 = 1
        L3d:
            if (r1 != 0) goto L6c
            boolean r0 = r4.h
            if (r0 == 0) goto L46
            java.lang.String r0 = "WB"
            goto L48
        L46:
            java.lang.String r0 = "NB"
        L48:
            int r1 = r0.length()
            int r1 = r1 + 35
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r1)
            java.lang.String r1 = "Illegal AMR "
            r3.append(r1)
            r3.append(r0)
            java.lang.String r0 = " frame type "
            r3.append(r0)
            r3.append(r5)
            java.lang.String r5 = r3.toString()
            com.google.android.exoplayer2.ParserException r5 = com.google.android.exoplayer2.ParserException.a(r5, r2)
            throw r5
        L6c:
            boolean r0 = r4.h
            if (r0 == 0) goto L75
            int[] r0 = b.i.a.c.x2.c0.b.f1161b
            r5 = r0[r5]
            goto L79
        L75:
            int[] r0 = b.i.a.c.x2.c0.b.a
            r5 = r0[r5]
        L79:
            return r5
        L7a:
            r0 = 42
            java.lang.String r1 = "Invalid padding bits for frame header "
            com.google.android.exoplayer2.ParserException r5 = b.d.b.a.a.t0(r0, r1, r5, r2)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.c0.b.a(b.i.a.c.x2.i):int");
    }

    @Override // b.i.a.c.x2.h
    public boolean b(i iVar) throws IOException {
        return c(iVar);
    }

    public final boolean c(i iVar) throws IOException {
        byte[] bArr = c;
        iVar.k();
        byte[] bArr2 = new byte[bArr.length];
        iVar.o(bArr2, 0, bArr.length);
        if (Arrays.equals(bArr2, bArr)) {
            this.h = false;
            iVar.l(bArr.length);
            return true;
        }
        byte[] bArr3 = d;
        iVar.k();
        byte[] bArr4 = new byte[bArr3.length];
        iVar.o(bArr4, 0, bArr3.length);
        if (!Arrays.equals(bArr4, bArr3)) {
            return false;
        }
        this.h = true;
        iVar.l(bArr3.length);
        return true;
    }

    /* JADX WARN: Removed duplicated region for block: B:37:0x00b2  */
    @Override // b.i.a.c.x2.h
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public int e(b.i.a.c.x2.i r19, b.i.a.c.x2.s r20) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 264
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.c0.b.e(b.i.a.c.x2.i, b.i.a.c.x2.s):int");
    }

    @Override // b.i.a.c.x2.h
    public void f(j jVar) {
        this.q = jVar;
        this.r = jVar.p(0, 1);
        jVar.j();
    }

    @Override // b.i.a.c.x2.h
    public void g(long j, long j2) {
        this.i = 0L;
        this.j = 0;
        this.k = 0;
        if (j != 0) {
            t tVar = this.f1162s;
            if (tVar instanceof d) {
                d dVar = (d) tVar;
                this.p = d.e(j, dVar.f1163b, dVar.e);
                return;
            }
        }
        this.p = 0L;
    }

    @Override // b.i.a.c.x2.h
    public void release() {
    }
}
