package b.i.a.c.x2;

import android.net.Uri;
import androidx.annotation.GuardedBy;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.x2.c0.b;
import b.i.a.c.x2.e0.c;
import b.i.a.c.x2.g0.e;
import b.i.a.c.x2.i0.g;
import b.i.a.c.x2.i0.i;
import b.i.a.c.x2.k0.b0;
import b.i.a.c.x2.k0.h;
import b.i.a.c.x2.k0.h0;
import b.i.a.c.x2.k0.j;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
/* compiled from: DefaultExtractorsFactory.java */
/* loaded from: classes3.dex */
public final class f implements l {
    public static final int[] a = {5, 4, 12, 8, 3, 10, 9, 11, 6, 2, 0, 1, 7, 14};

    /* renamed from: b  reason: collision with root package name */
    public static final a f1171b = new a();

    /* compiled from: DefaultExtractorsFactory.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public final AtomicBoolean a = new AtomicBoolean(false);
        @Nullable
        @GuardedBy("extensionLoaded")

        /* renamed from: b  reason: collision with root package name */
        public Constructor<? extends h> f1172b;
    }

    @Override // b.i.a.c.x2.l
    public synchronized h[] a() {
        return b(Uri.EMPTY, new HashMap());
    }

    @Override // b.i.a.c.x2.l
    public synchronized h[] b(Uri uri, Map<String, List<String>> map) {
        ArrayList arrayList;
        int[] iArr;
        arrayList = new ArrayList(14);
        int O0 = d.O0(map);
        if (O0 != -1) {
            c(O0, arrayList);
        }
        int P0 = d.P0(uri);
        if (!(P0 == -1 || P0 == O0)) {
            c(P0, arrayList);
        }
        for (int i : a) {
            if (!(i == O0 || i == P0)) {
                c(i, arrayList);
            }
        }
        return (h[]) arrayList.toArray(new h[arrayList.size()]);
    }

    public final void c(int i, List<h> list) {
        h hVar;
        Constructor<? extends h> constructor;
        switch (i) {
            case 0:
                list.add(new b.i.a.c.x2.k0.f());
                return;
            case 1:
                list.add(new h());
                return;
            case 2:
                list.add(new j(0));
                return;
            case 3:
                list.add(new b(0));
                return;
            case 4:
                a aVar = f1171b;
                synchronized (aVar.a) {
                    hVar = null;
                    if (aVar.a.get()) {
                        constructor = aVar.f1172b;
                    } else {
                        try {
                            if (Boolean.TRUE.equals(Class.forName("com.google.android.exoplayer2.ext.flac.FlacLibrary").getMethod("isAvailable", new Class[0]).invoke(null, new Object[0]))) {
                                aVar.f1172b = Class.forName("com.google.android.exoplayer2.ext.flac.FlacExtractor").asSubclass(h.class).getConstructor(Integer.TYPE);
                            }
                        } catch (ClassNotFoundException unused) {
                        } catch (Exception e) {
                            throw new RuntimeException("Error instantiating FLAC extension", e);
                        }
                        aVar.a.set(true);
                        constructor = aVar.f1172b;
                    }
                }
                if (constructor != null) {
                    try {
                        hVar = constructor.newInstance(0);
                    } catch (Exception e2) {
                        throw new IllegalStateException("Unexpected error creating FLAC extractor", e2);
                    }
                }
                if (hVar != null) {
                    list.add(hVar);
                    return;
                } else {
                    list.add(new b.i.a.c.x2.d0.d(0));
                    return;
                }
            case 5:
                list.add(new c());
                return;
            case 6:
                list.add(new e(0));
                return;
            case 7:
                list.add(new b.i.a.c.x2.h0.f(0));
                return;
            case 8:
                list.add(new g(0));
                list.add(new i(0));
                return;
            case 9:
                list.add(new b.i.a.c.x2.j0.d());
                return;
            case 10:
                list.add(new b0());
                return;
            case 11:
                list.add(new h0(1, 0, 112800));
                return;
            case 12:
                list.add(new b.i.a.c.x2.l0.b());
                return;
            case 13:
            default:
                return;
            case 14:
                list.add(new b.i.a.c.x2.f0.a());
                return;
        }
    }
}
