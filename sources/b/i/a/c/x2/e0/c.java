package b.i.a.c.x2.e0;

import b.i.a.c.f3.x;
import b.i.a.c.x2.h;
import b.i.a.c.x2.i;
import b.i.a.c.x2.j;
import b.i.a.c.x2.t;
import java.io.IOException;
import org.checkerframework.checker.nullness.qual.RequiresNonNull;
/* compiled from: FlvExtractor.java */
/* loaded from: classes3.dex */
public final class c implements h {
    public j f;
    public boolean h;
    public long i;
    public int j;
    public int k;
    public int l;
    public long m;
    public boolean n;
    public b o;
    public e p;
    public final x a = new x(4);

    /* renamed from: b  reason: collision with root package name */
    public final x f1168b = new x(9);
    public final x c = new x(11);
    public final x d = new x();
    public final d e = new d();
    public int g = 1;

    static {
        a aVar = a.a;
    }

    @RequiresNonNull({"extractorOutput"})
    public final void a() {
        if (!this.n) {
            this.f.a(new t.b(-9223372036854775807L, 0L));
            this.n = true;
        }
    }

    @Override // b.i.a.c.x2.h
    public boolean b(i iVar) throws IOException {
        iVar.o(this.a.a, 0, 3);
        this.a.E(0);
        if (this.a.v() != 4607062) {
            return false;
        }
        iVar.o(this.a.a, 0, 2);
        this.a.E(0);
        if ((this.a.y() & 250) != 0) {
            return false;
        }
        iVar.o(this.a.a, 0, 4);
        this.a.E(0);
        int f = this.a.f();
        iVar.k();
        iVar.g(f);
        iVar.o(this.a.a, 0, 4);
        this.a.E(0);
        return this.a.f() == 0;
    }

    public final x c(i iVar) throws IOException {
        int i = this.l;
        x xVar = this.d;
        byte[] bArr = xVar.a;
        if (i > bArr.length) {
            xVar.a = new byte[Math.max(bArr.length * 2, i)];
            xVar.c = 0;
            xVar.f980b = 0;
        } else {
            xVar.E(0);
        }
        this.d.D(this.l);
        iVar.readFully(this.d.a, 0, this.l);
        return this.d;
    }

    /* JADX WARN: Removed duplicated region for block: B:39:0x00a6  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x00aa  */
    /* JADX WARN: Removed duplicated region for block: B:77:0x00b4 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:84:0x0009 A[SYNTHETIC] */
    @Override // b.i.a.c.x2.h
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public int e(b.i.a.c.x2.i r17, b.i.a.c.x2.s r18) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 368
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.e0.c.e(b.i.a.c.x2.i, b.i.a.c.x2.s):int");
    }

    @Override // b.i.a.c.x2.h
    public void f(j jVar) {
        this.f = jVar;
    }

    @Override // b.i.a.c.x2.h
    public void g(long j, long j2) {
        if (j == 0) {
            this.g = 1;
            this.h = false;
        } else {
            this.g = 3;
        }
        this.j = 0;
    }

    @Override // b.i.a.c.x2.h
    public void release() {
    }
}
