package b.i.a.c.x2.e0;

import androidx.annotation.Nullable;
import b.i.a.c.f3.x;
import b.i.a.c.x2.g;
import com.google.android.exoplayer2.extractor.flv.TagPayloadReader;
import com.google.android.material.shadow.ShadowDrawableWrapper;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/* compiled from: ScriptTagPayloadReader.java */
/* loaded from: classes3.dex */
public final class d extends TagPayloadReader {

    /* renamed from: b  reason: collision with root package name */
    public long f1169b = -9223372036854775807L;
    public long[] c = new long[0];
    public long[] d = new long[0];

    public d() {
        super(new g());
    }

    @Nullable
    public static Object d(x xVar, int i) {
        if (i == 0) {
            return Double.valueOf(Double.longBitsToDouble(xVar.m()));
        }
        boolean z2 = false;
        if (i == 1) {
            if (xVar.t() == 1) {
                z2 = true;
            }
            return Boolean.valueOf(z2);
        } else if (i == 2) {
            return f(xVar);
        } else {
            if (i == 3) {
                HashMap hashMap = new HashMap();
                while (true) {
                    String f = f(xVar);
                    int t = xVar.t();
                    if (t == 9) {
                        return hashMap;
                    }
                    Object d = d(xVar, t);
                    if (d != null) {
                        hashMap.put(f, d);
                    }
                }
            } else if (i == 8) {
                return e(xVar);
            } else {
                if (i == 10) {
                    int w = xVar.w();
                    ArrayList arrayList = new ArrayList(w);
                    for (int i2 = 0; i2 < w; i2++) {
                        Object d2 = d(xVar, xVar.t());
                        if (d2 != null) {
                            arrayList.add(d2);
                        }
                    }
                    return arrayList;
                } else if (i != 11) {
                    return null;
                } else {
                    Date date = new Date((long) Double.valueOf(Double.longBitsToDouble(xVar.m())).doubleValue());
                    xVar.F(2);
                    return date;
                }
            }
        }
    }

    public static HashMap<String, Object> e(x xVar) {
        int w = xVar.w();
        HashMap<String, Object> hashMap = new HashMap<>(w);
        for (int i = 0; i < w; i++) {
            String f = f(xVar);
            Object d = d(xVar, xVar.t());
            if (d != null) {
                hashMap.put(f, d);
            }
        }
        return hashMap;
    }

    public static String f(x xVar) {
        int y2 = xVar.y();
        int i = xVar.f980b;
        xVar.F(y2);
        return new String(xVar.a, i, y2);
    }

    @Override // com.google.android.exoplayer2.extractor.flv.TagPayloadReader
    public boolean b(x xVar) {
        return true;
    }

    @Override // com.google.android.exoplayer2.extractor.flv.TagPayloadReader
    public boolean c(x xVar, long j) {
        if (!(xVar.t() == 2 && "onMetaData".equals(f(xVar)) && xVar.t() == 8)) {
            return false;
        }
        HashMap<String, Object> e = e(xVar);
        Object obj = e.get("duration");
        if (obj instanceof Double) {
            double doubleValue = ((Double) obj).doubleValue();
            if (doubleValue > ShadowDrawableWrapper.COS_45) {
                this.f1169b = (long) (doubleValue * 1000000.0d);
            }
        }
        Object obj2 = e.get("keyframes");
        if (obj2 instanceof Map) {
            Map map = (Map) obj2;
            Object obj3 = map.get("filepositions");
            Object obj4 = map.get("times");
            if ((obj3 instanceof List) && (obj4 instanceof List)) {
                List list = (List) obj3;
                List list2 = (List) obj4;
                int size = list2.size();
                this.c = new long[size];
                this.d = new long[size];
                for (int i = 0; i < size; i++) {
                    Object obj5 = list.get(i);
                    Object obj6 = list2.get(i);
                    if (!(obj6 instanceof Double) || !(obj5 instanceof Double)) {
                        this.c = new long[0];
                        this.d = new long[0];
                        break;
                    }
                    this.c[i] = (long) (((Double) obj6).doubleValue() * 1000000.0d);
                    this.d[i] = ((Double) obj5).longValue();
                }
            }
        }
        return false;
    }
}
