package b.i.a.c.x2.e0;

import b.d.b.a.a;
import b.i.a.c.f3.x;
import b.i.a.c.j1;
import b.i.a.c.t2.l;
import b.i.a.c.x2.w;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.extractor.flv.TagPayloadReader;
import java.util.Collections;
/* compiled from: AudioTagPayloadReader.java */
/* loaded from: classes3.dex */
public final class b extends TagPayloadReader {

    /* renamed from: b  reason: collision with root package name */
    public static final int[] f1167b = {5512, 11025, 22050, 44100};
    public boolean c;
    public boolean d;
    public int e;

    public b(w wVar) {
        super(wVar);
    }

    @Override // com.google.android.exoplayer2.extractor.flv.TagPayloadReader
    public boolean b(x xVar) throws TagPayloadReader.UnsupportedFormatException {
        if (!this.c) {
            int t = xVar.t();
            int i = (t >> 4) & 15;
            this.e = i;
            if (i == 2) {
                int i2 = f1167b[(t >> 2) & 3];
                j1.b bVar = new j1.b();
                bVar.k = "audio/mpeg";
                bVar.f1019x = 1;
                bVar.f1020y = i2;
                this.a.e(bVar.a());
                this.d = true;
            } else if (i == 7 || i == 8) {
                String str = i == 7 ? "audio/g711-alaw" : "audio/g711-mlaw";
                j1.b bVar2 = new j1.b();
                bVar2.k = str;
                bVar2.f1019x = 1;
                bVar2.f1020y = 8000;
                this.a.e(bVar2.a());
                this.d = true;
            } else if (i != 10) {
                throw new TagPayloadReader.UnsupportedFormatException(a.f(39, "Audio format not supported: ", this.e));
            }
            this.c = true;
        } else {
            xVar.F(1);
        }
        return true;
    }

    @Override // com.google.android.exoplayer2.extractor.flv.TagPayloadReader
    public boolean c(x xVar, long j) throws ParserException {
        if (this.e == 2) {
            int a = xVar.a();
            this.a.c(xVar, a);
            this.a.d(j, 1, a, 0, null);
            return true;
        }
        int t = xVar.t();
        if (t == 0 && !this.d) {
            int a2 = xVar.a();
            byte[] bArr = new byte[a2];
            System.arraycopy(xVar.a, xVar.f980b, bArr, 0, a2);
            xVar.f980b += a2;
            l.b b2 = l.b(new b.i.a.c.f3.w(bArr), false);
            j1.b bVar = new j1.b();
            bVar.k = "audio/mp4a-latm";
            bVar.h = b2.c;
            bVar.f1019x = b2.f1118b;
            bVar.f1020y = b2.a;
            bVar.m = Collections.singletonList(bArr);
            this.a.e(bVar.a());
            this.d = true;
            return false;
        } else if (this.e == 10 && t != 1) {
            return false;
        } else {
            int a3 = xVar.a();
            this.a.c(xVar, a3);
            this.a.d(j, 1, a3, 0, null);
            return true;
        }
    }
}
