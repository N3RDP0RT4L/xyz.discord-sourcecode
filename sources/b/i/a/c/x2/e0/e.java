package b.i.a.c.x2.e0;

import b.d.b.a.a;
import b.i.a.c.f3.u;
import b.i.a.c.f3.x;
import b.i.a.c.g3.m;
import b.i.a.c.j1;
import b.i.a.c.x2.w;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.extractor.flv.TagPayloadReader;
/* compiled from: VideoTagPayloadReader.java */
/* loaded from: classes3.dex */
public final class e extends TagPayloadReader {

    /* renamed from: b  reason: collision with root package name */
    public final x f1170b = new x(u.a);
    public final x c = new x(4);
    public int d;
    public boolean e;
    public boolean f;
    public int g;

    public e(w wVar) {
        super(wVar);
    }

    @Override // com.google.android.exoplayer2.extractor.flv.TagPayloadReader
    public boolean b(x xVar) throws TagPayloadReader.UnsupportedFormatException {
        int t = xVar.t();
        int i = (t >> 4) & 15;
        int i2 = t & 15;
        if (i2 == 7) {
            this.g = i;
            return i != 5;
        }
        throw new TagPayloadReader.UnsupportedFormatException(a.f(39, "Video format not supported: ", i2));
    }

    @Override // com.google.android.exoplayer2.extractor.flv.TagPayloadReader
    public boolean c(x xVar, long j) throws ParserException {
        int t = xVar.t();
        byte[] bArr = xVar.a;
        int i = xVar.f980b;
        int i2 = i + 1;
        xVar.f980b = i2;
        int i3 = i2 + 1;
        xVar.f980b = i3;
        int i4 = (((bArr[i] & 255) << 24) >> 8) | ((bArr[i2] & 255) << 8);
        xVar.f980b = i3 + 1;
        long j2 = (((bArr[i3] & 255) | i4) * 1000) + j;
        if (t == 0 && !this.e) {
            x xVar2 = new x(new byte[xVar.a()]);
            xVar.e(xVar2.a, 0, xVar.a());
            m b2 = m.b(xVar2);
            this.d = b2.f982b;
            j1.b bVar = new j1.b();
            bVar.k = "video/avc";
            bVar.h = b2.f;
            bVar.p = b2.c;
            bVar.q = b2.d;
            bVar.t = b2.e;
            bVar.m = b2.a;
            this.a.e(bVar.a());
            this.e = true;
            return false;
        } else if (t != 1 || !this.e) {
            return false;
        } else {
            int i5 = this.g == 1 ? 1 : 0;
            if (!this.f && i5 == 0) {
                return false;
            }
            byte[] bArr2 = this.c.a;
            bArr2[0] = 0;
            bArr2[1] = 0;
            bArr2[2] = 0;
            int i6 = 4 - this.d;
            int i7 = 0;
            while (xVar.a() > 0) {
                xVar.e(this.c.a, i6, this.d);
                this.c.E(0);
                int w = this.c.w();
                this.f1170b.E(0);
                this.a.c(this.f1170b, 4);
                this.a.c(xVar, w);
                i7 = i7 + 4 + w;
            }
            this.a.d(j2, i5, i7, 0, null);
            this.f = true;
            return true;
        }
    }
}
