package b.i.a.c.x2.h0;

import androidx.annotation.Nullable;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.x;
import b.i.a.c.t2.a0;
import b.i.a.c.x2.g;
import b.i.a.c.x2.h;
import b.i.a.c.x2.i;
import b.i.a.c.x2.j;
import b.i.a.c.x2.p;
import b.i.a.c.x2.q;
import b.i.a.c.x2.w;
import b.i.a.c.z2.k.b;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.id3.TextInformationFrame;
import java.io.EOFException;
import java.io.IOException;
/* compiled from: Mp3Extractor.java */
/* loaded from: classes3.dex */
public final class f implements h {
    public static final b.a a = b.a;

    /* renamed from: b  reason: collision with root package name */
    public final int f1201b;
    public final long c;
    public final x d;
    public final a0.a e;
    public final p f;
    public final q g;
    public final w h;
    public j i;
    public w j;
    public w k;
    public int l;
    @Nullable
    public Metadata m;
    public long n;
    public long o;
    public long p;
    public int q;
    public g r;

    /* renamed from: s  reason: collision with root package name */
    public boolean f1202s;
    public boolean t;
    public long u;

    static {
        a aVar = a.a;
    }

    public f() {
        this(0);
    }

    public static long d(@Nullable Metadata metadata) {
        if (metadata == null) {
            return -9223372036854775807L;
        }
        int length = metadata.j.length;
        for (int i = 0; i < length; i++) {
            Metadata.Entry entry = metadata.j[i];
            if (entry instanceof TextInformationFrame) {
                TextInformationFrame textInformationFrame = (TextInformationFrame) entry;
                if (textInformationFrame.j.equals("TLEN")) {
                    return e0.B(Long.parseLong(textInformationFrame.l));
                }
            }
        }
        return -9223372036854775807L;
    }

    public static boolean h(int i, long j) {
        return ((long) (i & (-128000))) == (j & (-128000));
    }

    public final long a(long j) {
        return ((j * 1000000) / this.e.d) + this.n;
    }

    @Override // b.i.a.c.x2.h
    public boolean b(i iVar) throws IOException {
        return j(iVar, true);
    }

    public final g c(i iVar, boolean z2) throws IOException {
        iVar.o(this.d.a, 0, 4);
        this.d.E(0);
        this.e.a(this.d.f());
        return new c(iVar.b(), iVar.getPosition(), this.e, z2);
    }

    /* JADX WARN: Code restructure failed: missing block: B:21:0x0061, code lost:
        if (r3 != 1231971951) goto L22;
     */
    /* JADX WARN: Removed duplicated region for block: B:104:0x0253  */
    /* JADX WARN: Removed duplicated region for block: B:116:0x02a9  */
    /* JADX WARN: Removed duplicated region for block: B:117:0x02af  */
    /* JADX WARN: Removed duplicated region for block: B:142:0x032f  */
    /* JADX WARN: Removed duplicated region for block: B:143:0x0331  */
    /* JADX WARN: Removed duplicated region for block: B:182:0x0443  */
    /* JADX WARN: Removed duplicated region for block: B:186:0x0459  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x007f A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:66:0x0179  */
    /* JADX WARN: Removed duplicated region for block: B:83:0x01ee  */
    /* JADX WARN: Removed duplicated region for block: B:97:0x023b  */
    @Override // b.i.a.c.x2.h
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public int e(b.i.a.c.x2.i r33, b.i.a.c.x2.s r34) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 1126
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.h0.f.e(b.i.a.c.x2.i, b.i.a.c.x2.s):int");
    }

    @Override // b.i.a.c.x2.h
    public void f(j jVar) {
        this.i = jVar;
        w p = jVar.p(0, 1);
        this.j = p;
        this.k = p;
        this.i.j();
    }

    @Override // b.i.a.c.x2.h
    public void g(long j, long j2) {
        this.l = 0;
        this.n = -9223372036854775807L;
        this.o = 0L;
        this.q = 0;
        this.u = j2;
        g gVar = this.r;
        if ((gVar instanceof d) && !((d) gVar).b(j2)) {
            this.t = true;
            this.k = this.h;
        }
    }

    public final boolean i(i iVar) throws IOException {
        g gVar = this.r;
        if (gVar != null) {
            long a2 = gVar.a();
            if (a2 != -1 && iVar.f() > a2 - 4) {
                return true;
            }
        }
        try {
            return !iVar.e(this.d.a, 0, 4, true);
        } catch (EOFException unused) {
            return true;
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:49:0x009e, code lost:
        if (r13 == false) goto L51;
     */
    /* JADX WARN: Code restructure failed: missing block: B:50:0x00a0, code lost:
        r12.l(r2 + r4);
     */
    /* JADX WARN: Code restructure failed: missing block: B:51:0x00a5, code lost:
        r12.k();
     */
    /* JADX WARN: Code restructure failed: missing block: B:52:0x00a8, code lost:
        r11.l = r1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:53:0x00aa, code lost:
        return true;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final boolean j(b.i.a.c.x2.i r12, boolean r13) throws java.io.IOException {
        /*
            r11 = this;
            if (r13 == 0) goto L6
            r0 = 32768(0x8000, float:4.5918E-41)
            goto L8
        L6:
            r0 = 131072(0x20000, float:1.83671E-40)
        L8:
            r12.k()
            long r1 = r12.getPosition()
            r3 = 0
            r5 = 0
            r6 = 1
            r7 = 0
            int r8 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r8 != 0) goto L42
            int r1 = r11.f1201b
            r1 = r1 & 8
            if (r1 != 0) goto L20
            r1 = 1
            goto L21
        L20:
            r1 = 0
        L21:
            if (r1 == 0) goto L25
            r1 = r5
            goto L27
        L25:
            b.i.a.c.z2.k.b$a r1 = b.i.a.c.x2.h0.f.a
        L27:
            b.i.a.c.x2.q r2 = r11.g
            com.google.android.exoplayer2.metadata.Metadata r1 = r2.a(r12, r1)
            r11.m = r1
            if (r1 == 0) goto L36
            b.i.a.c.x2.p r2 = r11.f
            r2.b(r1)
        L36:
            long r1 = r12.f()
            int r2 = (int) r1
            if (r13 != 0) goto L40
            r12.l(r2)
        L40:
            r1 = 0
            goto L44
        L42:
            r1 = 0
            r2 = 0
        L44:
            r3 = 0
            r4 = 0
        L46:
            boolean r8 = r11.i(r12)
            if (r8 == 0) goto L55
            if (r3 <= 0) goto L4f
            goto L9e
        L4f:
            java.io.EOFException r12 = new java.io.EOFException
            r12.<init>()
            throw r12
        L55:
            b.i.a.c.f3.x r8 = r11.d
            r8.E(r7)
            b.i.a.c.f3.x r8 = r11.d
            int r8 = r8.f()
            if (r1 == 0) goto L69
            long r9 = (long) r1
            boolean r9 = h(r8, r9)
            if (r9 == 0) goto L70
        L69:
            int r9 = b.i.a.c.t2.a0.a(r8)
            r10 = -1
            if (r9 != r10) goto L90
        L70:
            int r1 = r4 + 1
            if (r4 != r0) goto L7e
            if (r13 == 0) goto L77
            return r7
        L77:
            java.lang.String r12 = "Searched too many bytes."
            com.google.android.exoplayer2.ParserException r12 = com.google.android.exoplayer2.ParserException.a(r12, r5)
            throw r12
        L7e:
            if (r13 == 0) goto L89
            r12.k()
            int r3 = r2 + r1
            r12.g(r3)
            goto L8c
        L89:
            r12.l(r6)
        L8c:
            r4 = r1
            r1 = 0
            r3 = 0
            goto L46
        L90:
            int r3 = r3 + 1
            if (r3 != r6) goto L9b
            b.i.a.c.t2.a0$a r1 = r11.e
            r1.a(r8)
            r1 = r8
            goto Lab
        L9b:
            r8 = 4
            if (r3 != r8) goto Lab
        L9e:
            if (r13 == 0) goto La5
            int r2 = r2 + r4
            r12.l(r2)
            goto La8
        La5:
            r12.k()
        La8:
            r11.l = r1
            return r6
        Lab:
            int r9 = r9 + (-4)
            r12.g(r9)
            goto L46
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.h0.f.j(b.i.a.c.x2.i, boolean):boolean");
    }

    @Override // b.i.a.c.x2.h
    public void release() {
    }

    public f(int i) {
        this.f1201b = (i & 2) != 0 ? i | 1 : i;
        this.c = -9223372036854775807L;
        this.d = new x(10);
        this.e = new a0.a();
        this.f = new p();
        this.n = -9223372036854775807L;
        this.g = new q();
        g gVar = new g();
        this.h = gVar;
        this.k = gVar;
    }
}
