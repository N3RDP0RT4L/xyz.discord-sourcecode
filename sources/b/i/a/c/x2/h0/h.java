package b.i.a.c.x2.h0;

import b.i.a.c.f3.e0;
import b.i.a.c.x2.t;
import b.i.a.c.x2.u;
/* compiled from: VbriSeeker.java */
/* loaded from: classes3.dex */
public final class h implements g {
    public final long[] a;

    /* renamed from: b  reason: collision with root package name */
    public final long[] f1203b;
    public final long c;
    public final long d;

    public h(long[] jArr, long[] jArr2, long j, long j2) {
        this.a = jArr;
        this.f1203b = jArr2;
        this.c = j;
        this.d = j2;
    }

    @Override // b.i.a.c.x2.h0.g
    public long a() {
        return this.d;
    }

    @Override // b.i.a.c.x2.t
    public boolean c() {
        return true;
    }

    @Override // b.i.a.c.x2.h0.g
    public long d(long j) {
        return this.a[e0.e(this.f1203b, j, true, true)];
    }

    @Override // b.i.a.c.x2.t
    public t.a h(long j) {
        int e = e0.e(this.a, j, true, true);
        long[] jArr = this.a;
        long j2 = jArr[e];
        long[] jArr2 = this.f1203b;
        u uVar = new u(j2, jArr2[e]);
        if (j2 >= j || e == jArr.length - 1) {
            return new t.a(uVar);
        }
        int i = e + 1;
        return new t.a(uVar, new u(jArr[i], jArr2[i]));
    }

    @Override // b.i.a.c.x2.t
    public long i() {
        return this.c;
    }
}
