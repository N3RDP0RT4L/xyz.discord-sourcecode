package b.i.a.c.x2.h0;

import b.i.a.c.t2.a0;
import b.i.a.c.x2.d;
/* compiled from: ConstantBitrateSeeker.java */
/* loaded from: classes3.dex */
public final class c extends d implements g {
    public c(long j, long j2, a0.a aVar, boolean z2) {
        super(j, j2, aVar.f, aVar.c, z2);
    }

    @Override // b.i.a.c.x2.h0.g
    public long a() {
        return -1L;
    }

    @Override // b.i.a.c.x2.h0.g
    public long d(long j) {
        return d.e(j, this.f1163b, this.e);
    }
}
