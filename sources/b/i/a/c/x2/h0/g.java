package b.i.a.c.x2.h0;

import b.i.a.c.x2.t;
/* compiled from: Seeker.java */
/* loaded from: classes3.dex */
public interface g extends t {

    /* compiled from: Seeker.java */
    /* loaded from: classes3.dex */
    public static class a extends t.b implements g {
        public a() {
            super(-9223372036854775807L, 0L);
        }

        @Override // b.i.a.c.x2.h0.g
        public long a() {
            return -1L;
        }

        @Override // b.i.a.c.x2.h0.g
        public long d(long j) {
            return 0L;
        }
    }

    long a();

    long d(long j);
}
