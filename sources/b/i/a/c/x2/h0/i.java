package b.i.a.c.x2.h0;

import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.f3.e0;
import b.i.a.c.x2.t;
import b.i.a.c.x2.u;
import com.google.android.material.shadow.ShadowDrawableWrapper;
/* compiled from: XingSeeker.java */
/* loaded from: classes3.dex */
public final class i implements g {
    public final long a;

    /* renamed from: b  reason: collision with root package name */
    public final int f1204b;
    public final long c;
    public final long d;
    public final long e;
    @Nullable
    public final long[] f;

    public i(long j, int i, long j2, long j3, @Nullable long[] jArr) {
        this.a = j;
        this.f1204b = i;
        this.c = j2;
        this.f = jArr;
        this.d = j3;
        this.e = j3 != -1 ? j + j3 : -1L;
    }

    @Override // b.i.a.c.x2.h0.g
    public long a() {
        return this.e;
    }

    @Override // b.i.a.c.x2.t
    public boolean c() {
        return this.f != null;
    }

    @Override // b.i.a.c.x2.h0.g
    public long d(long j) {
        long j2 = j - this.a;
        if (!c() || j2 <= this.f1204b) {
            return 0L;
        }
        long[] jArr = this.f;
        d.H(jArr);
        double d = (j2 * 256.0d) / this.d;
        int e = e0.e(jArr, (long) d, true, true);
        long j3 = this.c;
        long j4 = (e * j3) / 100;
        long j5 = jArr[e];
        int i = e + 1;
        long j6 = (j3 * i) / 100;
        long j7 = e == 99 ? 256L : jArr[i];
        return Math.round((j5 == j7 ? ShadowDrawableWrapper.COS_45 : (d - j5) / (j7 - j5)) * (j6 - j4)) + j4;
    }

    @Override // b.i.a.c.x2.t
    public t.a h(long j) {
        long[] jArr;
        if (!c()) {
            return new t.a(new u(0L, this.a + this.f1204b));
        }
        long i = e0.i(j, 0L, this.c);
        double d = (i * 100.0d) / this.c;
        double d2 = ShadowDrawableWrapper.COS_45;
        if (d > ShadowDrawableWrapper.COS_45) {
            if (d >= 100.0d) {
                d2 = 256.0d;
            } else {
                int i2 = (int) d;
                long[] jArr2 = this.f;
                d.H(jArr2);
                double d3 = jArr2[i2];
                d2 = d3 + (((i2 == 99 ? 256.0d : jArr[i2 + 1]) - d3) * (d - i2));
            }
        }
        return new t.a(new u(i, this.a + e0.i(Math.round((d2 / 256.0d) * this.d), this.f1204b, this.d - 1)));
    }

    @Override // b.i.a.c.x2.t
    public long i() {
        return this.c;
    }
}
