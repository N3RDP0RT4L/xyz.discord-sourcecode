package b.i.a.c.x2.h0;

import b.i.a.c.f3.e0;
import b.i.a.c.f3.r;
import b.i.a.c.x2.t;
import b.i.a.c.x2.u;
/* compiled from: IndexSeeker.java */
/* loaded from: classes3.dex */
public final class d implements g {
    public final long a;

    /* renamed from: b  reason: collision with root package name */
    public final r f1199b;
    public final r c;
    public long d;

    public d(long j, long j2, long j3) {
        this.d = j;
        this.a = j3;
        r rVar = new r();
        this.f1199b = rVar;
        r rVar2 = new r();
        this.c = rVar2;
        rVar.a(0L);
        rVar2.a(j2);
    }

    @Override // b.i.a.c.x2.h0.g
    public long a() {
        return this.a;
    }

    public boolean b(long j) {
        r rVar = this.f1199b;
        return j - rVar.b(rVar.a - 1) < 100000;
    }

    @Override // b.i.a.c.x2.t
    public boolean c() {
        return true;
    }

    @Override // b.i.a.c.x2.h0.g
    public long d(long j) {
        return this.f1199b.b(e0.c(this.c, j, true, true));
    }

    @Override // b.i.a.c.x2.t
    public t.a h(long j) {
        int c = e0.c(this.f1199b, j, true, true);
        long b2 = this.f1199b.b(c);
        u uVar = new u(b2, this.c.b(c));
        if (b2 != j) {
            r rVar = this.f1199b;
            if (c != rVar.a - 1) {
                int i = c + 1;
                return new t.a(uVar, new u(rVar.b(i), this.c.b(i)));
            }
        }
        return new t.a(uVar);
    }

    @Override // b.i.a.c.x2.t
    public long i() {
        return this.d;
    }
}
