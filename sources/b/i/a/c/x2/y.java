package b.i.a.c.x2;

import b.c.a.a0.d;
/* compiled from: VorbisBitArray.java */
/* loaded from: classes3.dex */
public final class y {
    public final byte[] a;

    /* renamed from: b  reason: collision with root package name */
    public final int f1297b;
    public int c;
    public int d;

    public y(byte[] bArr) {
        this.a = bArr;
        this.f1297b = bArr.length;
    }

    public boolean a() {
        boolean z2 = (((this.a[this.c] & 255) >> this.d) & 1) == 1;
        c(1);
        return z2;
    }

    public int b(int i) {
        int i2 = this.c;
        int min = Math.min(i, 8 - this.d);
        int i3 = i2 + 1;
        int i4 = ((this.a[i2] & 255) >> this.d) & (255 >> (8 - min));
        while (min < i) {
            i3++;
            i4 |= (this.a[i3] & 255) << min;
            min += 8;
        }
        int i5 = i4 & ((-1) >>> (32 - i));
        c(i);
        return i5;
    }

    public void c(int i) {
        int i2;
        int i3 = i / 8;
        int i4 = this.c + i3;
        this.c = i4;
        int i5 = (i - (i3 * 8)) + this.d;
        this.d = i5;
        boolean z2 = true;
        if (i5 > 7) {
            this.c = i4 + 1;
            this.d = i5 - 8;
        }
        int i6 = this.c;
        if (i6 < 0 || (i6 >= (i2 = this.f1297b) && !(i6 == i2 && this.d == 0))) {
            z2 = false;
        }
        d.D(z2);
    }
}
