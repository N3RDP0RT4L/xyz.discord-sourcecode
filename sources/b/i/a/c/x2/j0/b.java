package b.i.a.c.x2.j0;

import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.f3.e0;
import b.i.a.c.x2.t;
import b.i.a.c.x2.u;
/* compiled from: DefaultOggSeeker.java */
/* loaded from: classes3.dex */
public final class b implements g {
    public final f a;

    /* renamed from: b  reason: collision with root package name */
    public final long f1230b;
    public final long c;
    public final i d;
    public int e;
    public long f;
    public long g;
    public long h;
    public long i;
    public long j;
    public long k;
    public long l;

    /* compiled from: DefaultOggSeeker.java */
    /* renamed from: b.i.a.c.x2.j0.b$b  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public final class C0103b implements t {
        public C0103b(a aVar) {
        }

        @Override // b.i.a.c.x2.t
        public boolean c() {
            return true;
        }

        @Override // b.i.a.c.x2.t
        public t.a h(long j) {
            b bVar = b.this;
            long j2 = bVar.f1230b;
            long j3 = bVar.c;
            return new t.a(new u(j, e0.i(((((j3 - j2) * ((bVar.d.i * j) / 1000000)) / bVar.f) + j2) - 30000, j2, j3 - 1)));
        }

        @Override // b.i.a.c.x2.t
        public long i() {
            b bVar = b.this;
            return (bVar.f * 1000000) / bVar.d.i;
        }
    }

    public b(i iVar, long j, long j2, long j3, long j4, boolean z2) {
        d.j(j >= 0 && j2 > j);
        this.d = iVar;
        this.f1230b = j;
        this.c = j2;
        if (j3 == j2 - j || z2) {
            this.f = j4;
            this.e = 4;
        } else {
            this.e = 0;
        }
        this.a = new f();
    }

    @Override // b.i.a.c.x2.j0.g
    @Nullable
    public t a() {
        if (this.f != 0) {
            return new C0103b(null);
        }
        return null;
    }

    /* JADX WARN: Removed duplicated region for block: B:40:0x00c2 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:41:0x00c3  */
    @Override // b.i.a.c.x2.j0.g
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public long b(b.i.a.c.x2.i r23) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 379
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.j0.b.b(b.i.a.c.x2.i):long");
    }

    @Override // b.i.a.c.x2.j0.g
    public void c(long j) {
        this.h = e0.i(j, 0L, this.f - 1);
        this.e = 2;
        this.i = this.f1230b;
        this.j = this.c;
        this.k = 0L;
        this.l = this.f;
    }
}
