package b.i.a.c.x2.j0;

import b.i.a.c.f3.e0;
import b.i.a.c.f3.x;
import b.i.a.c.x2.h;
import b.i.a.c.x2.i;
import b.i.a.c.x2.j;
import com.discord.utilities.auth.GoogleSmartLockManager;
import com.google.android.exoplayer2.ParserException;
import java.io.IOException;
import java.util.Arrays;
import org.checkerframework.checker.nullness.qual.EnsuresNonNullIf;
/* compiled from: OggExtractor.java */
/* loaded from: classes3.dex */
public class d implements h {
    public j a;

    /* renamed from: b  reason: collision with root package name */
    public i f1232b;
    public boolean c;

    static {
        a aVar = a.a;
    }

    @EnsuresNonNullIf(expression = {"streamReader"}, result = GoogleSmartLockManager.SET_DISCORD_ACCOUNT_DETAILS)
    public final boolean a(i iVar) throws IOException {
        boolean z2;
        boolean z3;
        f fVar = new f();
        if (fVar.a(iVar, true) && (fVar.f1234b & 2) == 2) {
            int min = Math.min(fVar.f, 8);
            x xVar = new x(min);
            iVar.o(xVar.a, 0, min);
            xVar.E(0);
            if (xVar.a() >= 5 && xVar.t() == 127 && xVar.u() == 1179402563) {
                this.f1232b = new c();
            } else {
                xVar.E(0);
                try {
                    z2 = b.c.a.a0.d.n2(1, xVar, true);
                } catch (ParserException unused) {
                    z2 = false;
                }
                if (z2) {
                    this.f1232b = new j();
                } else {
                    xVar.E(0);
                    int a = xVar.a();
                    byte[] bArr = h.n;
                    if (a < bArr.length) {
                        z3 = false;
                    } else {
                        byte[] bArr2 = new byte[bArr.length];
                        int length = bArr.length;
                        System.arraycopy(xVar.a, xVar.f980b, bArr2, 0, length);
                        xVar.f980b += length;
                        z3 = Arrays.equals(bArr2, bArr);
                    }
                    if (z3) {
                        this.f1232b = new h();
                    }
                }
            }
            return true;
        }
        return false;
    }

    @Override // b.i.a.c.x2.h
    public boolean b(i iVar) throws IOException {
        try {
            return a(iVar);
        } catch (ParserException unused) {
            return false;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:66:0x0180  */
    /* JADX WARN: Removed duplicated region for block: B:73:? A[RETURN, SYNTHETIC] */
    @Override // b.i.a.c.x2.h
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public int e(b.i.a.c.x2.i r21, b.i.a.c.x2.s r22) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 402
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.j0.d.e(b.i.a.c.x2.i, b.i.a.c.x2.s):int");
    }

    @Override // b.i.a.c.x2.h
    public void f(j jVar) {
        this.a = jVar;
    }

    @Override // b.i.a.c.x2.h
    public void g(long j, long j2) {
        i iVar = this.f1232b;
        if (iVar != null) {
            e eVar = iVar.a;
            eVar.a.b();
            eVar.f1233b.A(0);
            eVar.c = -1;
            eVar.e = false;
            if (j == 0) {
                iVar.e(!iVar.l);
            } else if (iVar.h != 0) {
                long j3 = (iVar.i * j2) / 1000000;
                iVar.e = j3;
                g gVar = iVar.d;
                int i = e0.a;
                gVar.c(j3);
                iVar.h = 2;
            }
        }
    }

    @Override // b.i.a.c.x2.h
    public void release() {
    }
}
