package b.i.a.c.x2.j0;

import b.c.a.a0.d;
import b.i.a.c.f3.x;
import b.i.a.c.x2.i;
import com.google.android.exoplayer2.ParserException;
import java.io.IOException;
/* compiled from: OggPageHeader.java */
/* loaded from: classes3.dex */
public final class f {
    public int a;

    /* renamed from: b  reason: collision with root package name */
    public int f1234b;
    public long c;
    public int d;
    public int e;
    public int f;
    public final int[] g = new int[255];
    public final x h = new x(255);

    public boolean a(i iVar, boolean z2) throws IOException {
        int i;
        b();
        this.h.A(27);
        if (!d.x1(iVar, this.h.a, 0, 27, z2) || this.h.u() != 1332176723) {
            return false;
        }
        int t = this.h.t();
        this.a = t;
        if (t == 0) {
            this.f1234b = this.h.t();
            x xVar = this.h;
            byte[] bArr = xVar.a;
            int i2 = xVar.f980b + 1;
            xVar.f980b = i2;
            int i3 = i2 + 1;
            xVar.f980b = i3;
            int i4 = i3 + 1;
            xVar.f980b = i4;
            int i5 = i4 + 1;
            xVar.f980b = i5;
            int i6 = i5 + 1;
            xVar.f980b = i6;
            int i7 = i6 + 1;
            xVar.f980b = i7;
            int i8 = i7 + 1;
            xVar.f980b = i8;
            xVar.f980b = i8 + 1;
            this.c = (bArr[i] & 255) | ((bArr[i2] & 255) << 8) | ((bArr[i3] & 255) << 16) | ((bArr[i4] & 255) << 24) | ((bArr[i5] & 255) << 32) | ((bArr[i6] & 255) << 40) | ((bArr[i7] & 255) << 48) | ((bArr[i8] & 255) << 56);
            xVar.j();
            this.h.j();
            this.h.j();
            int t2 = this.h.t();
            this.d = t2;
            this.e = t2 + 27;
            this.h.A(t2);
            if (!d.x1(iVar, this.h.a, 0, this.d, z2)) {
                return false;
            }
            for (int i9 = 0; i9 < this.d; i9++) {
                this.g[i9] = this.h.t();
                this.f += this.g[i9];
            }
            return true;
        } else if (z2) {
            return false;
        } else {
            throw ParserException.b("unsupported bit stream revision");
        }
    }

    public void b() {
        this.a = 0;
        this.f1234b = 0;
        this.c = 0L;
        this.d = 0;
        this.e = 0;
        this.f = 0;
    }

    public boolean c(i iVar, long j) throws IOException {
        int i;
        d.j(iVar.getPosition() == iVar.f());
        this.h.A(4);
        while (true) {
            i = (j > (-1L) ? 1 : (j == (-1L) ? 0 : -1));
            if ((i == 0 || iVar.getPosition() + 4 < j) && d.x1(iVar, this.h.a, 0, 4, true)) {
                this.h.E(0);
                if (this.h.u() == 1332176723) {
                    iVar.k();
                    return true;
                }
                iVar.l(1);
            }
        }
        do {
            if (i != 0 && iVar.getPosition() >= j) {
                break;
            }
        } while (iVar.h(1) != -1);
        return false;
    }
}
