package b.i.a.c.x2.j0;

import andhook.lib.xposed.callbacks.XCallback;
import b.c.a.a0.d;
import b.i.a.c.f3.x;
import b.i.a.c.j1;
import b.i.a.c.x2.j0.i;
import com.discord.utilities.guilds.GuildConstantsKt;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.checkerframework.checker.nullness.qual.EnsuresNonNullIf;
/* compiled from: OpusReader.java */
/* loaded from: classes3.dex */
public final class h extends i {
    public static final byte[] n = {79, 112, 117, 115, 72, 101, 97, 100};
    public boolean o;

    @Override // b.i.a.c.x2.j0.i
    public long c(x xVar) {
        byte[] bArr = xVar.a;
        int i = bArr[0] & 255;
        int i2 = i & 3;
        int i3 = 2;
        if (i2 == 0) {
            i3 = 1;
        } else if (!(i2 == 1 || i2 == 2)) {
            i3 = bArr[1] & 63;
        }
        int i4 = i >> 3;
        int i5 = i4 & 3;
        return a(i3 * (i4 >= 16 ? GuildConstantsKt.MAX_GUILD_MEMBERS_NOTIFY_ALL_MESSAGES << i5 : i4 >= 12 ? XCallback.PRIORITY_HIGHEST << (i5 & 1) : i5 == 3 ? 60000 : XCallback.PRIORITY_HIGHEST << i5));
    }

    @Override // b.i.a.c.x2.j0.i
    @EnsuresNonNullIf(expression = {"#3.format"}, result = false)
    public boolean d(x xVar, long j, i.b bVar) {
        boolean z2 = true;
        if (!this.o) {
            byte[] copyOf = Arrays.copyOf(xVar.a, xVar.c);
            List<byte[]> g = d.g(copyOf);
            j1.b bVar2 = new j1.b();
            bVar2.k = "audio/opus";
            bVar2.f1019x = copyOf[9] & 255;
            bVar2.f1020y = 48000;
            bVar2.m = g;
            bVar.a = bVar2.a();
            this.o = true;
            return true;
        }
        Objects.requireNonNull(bVar.a);
        if (xVar.f() != 1332770163) {
            z2 = false;
        }
        xVar.E(0);
        return z2;
    }

    @Override // b.i.a.c.x2.j0.i
    public void e(boolean z2) {
        super.e(z2);
        if (z2) {
            this.o = false;
        }
    }
}
