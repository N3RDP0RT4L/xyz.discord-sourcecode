package b.i.a.c.x2.j0;

import android.util.Log;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.f3.x;
import b.i.a.c.j1;
import b.i.a.c.x2.a0;
import b.i.a.c.x2.b0;
import b.i.a.c.x2.j0.i;
import b.i.a.c.x2.y;
import b.i.a.c.x2.z;
import com.google.android.exoplayer2.ParserException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import org.checkerframework.checker.nullness.qual.EnsuresNonNullIf;
/* compiled from: VorbisReader.java */
/* loaded from: classes3.dex */
public final class j extends i {
    @Nullable
    public a n;
    public int o;
    public boolean p;
    @Nullable
    public b0 q;
    @Nullable
    public z r;

    /* compiled from: VorbisReader.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public final b0 a;

        /* renamed from: b  reason: collision with root package name */
        public final byte[] f1237b;
        public final a0[] c;
        public final int d;

        public a(b0 b0Var, z zVar, byte[] bArr, a0[] a0VarArr, int i) {
            this.a = b0Var;
            this.f1237b = bArr;
            this.c = a0VarArr;
            this.d = i;
        }
    }

    @Override // b.i.a.c.x2.j0.i
    public void b(long j) {
        this.g = j;
        int i = 0;
        this.p = j != 0;
        b0 b0Var = this.q;
        if (b0Var != null) {
            i = b0Var.e;
        }
        this.o = i;
    }

    @Override // b.i.a.c.x2.j0.i
    public long c(x xVar) {
        int i;
        byte[] bArr = xVar.a;
        int i2 = 0;
        if ((bArr[0] & 1) == 1) {
            return -1L;
        }
        byte b2 = bArr[0];
        a aVar = this.n;
        d.H(aVar);
        a aVar2 = aVar;
        if (!aVar2.c[(b2 >> 1) & (255 >>> (8 - aVar2.d))].a) {
            i = aVar2.a.e;
        } else {
            i = aVar2.a.f;
        }
        if (this.p) {
            i2 = (this.o + i) / 4;
        }
        long j = i2;
        byte[] bArr2 = xVar.a;
        int length = bArr2.length;
        int i3 = xVar.c + 4;
        if (length < i3) {
            xVar.B(Arrays.copyOf(bArr2, i3));
        } else {
            xVar.D(i3);
        }
        byte[] bArr3 = xVar.a;
        int i4 = xVar.c;
        bArr3[i4 - 4] = (byte) (j & 255);
        bArr3[i4 - 3] = (byte) ((j >>> 8) & 255);
        bArr3[i4 - 2] = (byte) ((j >>> 16) & 255);
        bArr3[i4 - 1] = (byte) ((j >>> 24) & 255);
        this.p = true;
        this.o = i;
        return j;
    }

    @Override // b.i.a.c.x2.j0.i
    @EnsuresNonNullIf(expression = {"#3.format"}, result = false)
    public boolean d(x xVar, long j, i.b bVar) throws IOException {
        a aVar;
        int i;
        int i2;
        int i3;
        int i4;
        long j2;
        int i5;
        if (this.n != null) {
            Objects.requireNonNull(bVar.a);
            return false;
        }
        b0 b0Var = this.q;
        if (b0Var == null) {
            d.n2(1, xVar, false);
            int k = xVar.k();
            int t = xVar.t();
            int k2 = xVar.k();
            int h = xVar.h();
            int i6 = h <= 0 ? -1 : h;
            int h2 = xVar.h();
            int i7 = h2 <= 0 ? -1 : h2;
            int h3 = xVar.h();
            int i8 = h3 <= 0 ? -1 : h3;
            int t2 = xVar.t();
            this.q = new b0(k, t, k2, i6, i7, i8, (int) Math.pow(2.0d, t2 & 15), (int) Math.pow(2.0d, (t2 & 240) >> 4), (xVar.t() & 1) > 0, Arrays.copyOf(xVar.a, xVar.c));
        } else {
            z zVar = this.r;
            if (zVar == null) {
                this.r = d.N1(xVar, true, true);
            } else {
                int i9 = xVar.c;
                byte[] bArr = new byte[i9];
                System.arraycopy(xVar.a, 0, bArr, 0, i9);
                int i10 = b0Var.a;
                int i11 = 5;
                d.n2(5, xVar, false);
                int t3 = xVar.t() + 1;
                y yVar = new y(xVar.a);
                yVar.c(xVar.f980b * 8);
                int i12 = 0;
                while (true) {
                    int i13 = 16;
                    if (i12 >= t3) {
                        z zVar2 = zVar;
                        byte[] bArr2 = bArr;
                        int i14 = 6;
                        int b2 = yVar.b(6) + 1;
                        for (int i15 = 0; i15 < b2; i15++) {
                            if (yVar.b(16) != 0) {
                                throw ParserException.a("placeholder of time domain transforms not zeroed out", null);
                            }
                        }
                        int i16 = 1;
                        int b3 = yVar.b(6) + 1;
                        int i17 = 0;
                        while (true) {
                            int i18 = 3;
                            if (i17 < b3) {
                                int b4 = yVar.b(i13);
                                if (b4 == 0) {
                                    i3 = b3;
                                    int i19 = 8;
                                    yVar.c(8);
                                    yVar.c(16);
                                    yVar.c(16);
                                    yVar.c(6);
                                    yVar.c(8);
                                    int b5 = yVar.b(4) + 1;
                                    int i20 = 0;
                                    while (i20 < b5) {
                                        yVar.c(i19);
                                        i20++;
                                        i19 = 8;
                                    }
                                } else if (b4 == i16) {
                                    int b6 = yVar.b(5);
                                    int[] iArr = new int[b6];
                                    int i21 = -1;
                                    for (int i22 = 0; i22 < b6; i22++) {
                                        iArr[i22] = yVar.b(4);
                                        if (iArr[i22] > i21) {
                                            i21 = iArr[i22];
                                        }
                                    }
                                    int i23 = i21 + 1;
                                    int[] iArr2 = new int[i23];
                                    int i24 = 0;
                                    while (i24 < i23) {
                                        iArr2[i24] = yVar.b(i18) + 1;
                                        int b7 = yVar.b(2);
                                        int i25 = 8;
                                        if (b7 > 0) {
                                            yVar.c(8);
                                        }
                                        b3 = b3;
                                        int i26 = 0;
                                        for (int i27 = 1; i26 < (i27 << b7); i27 = 1) {
                                            yVar.c(i25);
                                            i26++;
                                            i25 = 8;
                                        }
                                        i24++;
                                        i18 = 3;
                                    }
                                    i3 = b3;
                                    yVar.c(2);
                                    int b8 = yVar.b(4);
                                    int i28 = 0;
                                    int i29 = 0;
                                    for (int i30 = 0; i30 < b6; i30++) {
                                        i28 += iArr2[iArr[i30]];
                                        while (i29 < i28) {
                                            yVar.c(b8);
                                            i29++;
                                        }
                                    }
                                } else {
                                    throw b.d.b.a.a.t0(52, "floor type greater than 1 not decodable: ", b4, null);
                                }
                                i17++;
                                i14 = 6;
                                i16 = 1;
                                i13 = 16;
                                b3 = i3;
                            } else {
                                int i31 = 1;
                                int b9 = yVar.b(i14) + 1;
                                int i32 = 0;
                                while (i32 < b9) {
                                    if (yVar.b(16) <= 2) {
                                        yVar.c(24);
                                        yVar.c(24);
                                        yVar.c(24);
                                        int b10 = yVar.b(i14) + i31;
                                        int i33 = 8;
                                        yVar.c(8);
                                        int[] iArr3 = new int[b10];
                                        for (int i34 = 0; i34 < b10; i34++) {
                                            iArr3[i34] = ((yVar.a() ? yVar.b(5) : 0) * 8) + yVar.b(3);
                                        }
                                        int i35 = 0;
                                        while (i35 < b10) {
                                            int i36 = 0;
                                            while (i36 < i33) {
                                                if ((iArr3[i35] & (1 << i36)) != 0) {
                                                    yVar.c(i33);
                                                }
                                                i36++;
                                                i33 = 8;
                                            }
                                            i35++;
                                            i33 = 8;
                                        }
                                        i32++;
                                        i14 = 6;
                                        i31 = 1;
                                    } else {
                                        throw ParserException.a("residueType greater than 2 is not decodable", null);
                                    }
                                }
                                int b11 = yVar.b(i14) + 1;
                                for (int i37 = 0; i37 < b11; i37++) {
                                    int b12 = yVar.b(16);
                                    if (b12 != 0) {
                                        StringBuilder sb = new StringBuilder(52);
                                        sb.append("mapping type other than 0 not supported: ");
                                        sb.append(b12);
                                        Log.e("VorbisUtil", sb.toString());
                                    } else {
                                        if (yVar.a()) {
                                            i = 1;
                                            i2 = yVar.b(4) + 1;
                                        } else {
                                            i = 1;
                                            i2 = 1;
                                        }
                                        if (yVar.a()) {
                                            int b13 = yVar.b(8) + i;
                                            for (int i38 = 0; i38 < b13; i38++) {
                                                int i39 = i10 - 1;
                                                yVar.c(d.M0(i39));
                                                yVar.c(d.M0(i39));
                                            }
                                        }
                                        if (yVar.b(2) == 0) {
                                            if (i2 > 1) {
                                                for (int i40 = 0; i40 < i10; i40++) {
                                                    yVar.c(4);
                                                }
                                            }
                                            for (int i41 = 0; i41 < i2; i41++) {
                                                yVar.c(8);
                                                yVar.c(8);
                                                yVar.c(8);
                                            }
                                        } else {
                                            throw ParserException.a("to reserved bits must be zero after mapping coupling steps", null);
                                        }
                                    }
                                }
                                int b14 = yVar.b(6) + 1;
                                a0[] a0VarArr = new a0[b14];
                                for (int i42 = 0; i42 < b14; i42++) {
                                    a0VarArr[i42] = new a0(yVar.a(), yVar.b(16), yVar.b(16), yVar.b(8));
                                }
                                if (yVar.a()) {
                                    aVar = new a(b0Var, zVar2, bArr2, a0VarArr, d.M0(b14 - 1));
                                } else {
                                    throw ParserException.a("framing bit after modes not set as expected", null);
                                }
                            }
                        }
                    } else if (yVar.b(24) == 5653314) {
                        int b15 = yVar.b(16);
                        int b16 = yVar.b(24);
                        long[] jArr = new long[b16];
                        if (!yVar.a()) {
                            boolean a2 = yVar.a();
                            int i43 = 0;
                            while (i43 < b16) {
                                if (!a2) {
                                    i5 = t3;
                                    jArr[i43] = yVar.b(5) + 1;
                                } else if (yVar.a()) {
                                    i5 = t3;
                                    jArr[i43] = yVar.b(i11) + 1;
                                } else {
                                    i5 = t3;
                                    jArr[i43] = 0;
                                }
                                i43++;
                                i11 = 5;
                                t3 = i5;
                            }
                            i4 = t3;
                        } else {
                            i4 = t3;
                            int b17 = yVar.b(5) + 1;
                            int i44 = 0;
                            while (i44 < b16) {
                                int b18 = yVar.b(d.M0(b16 - i44));
                                for (int i45 = 0; i45 < b18 && i44 < b16; i45++) {
                                    zVar = zVar;
                                    bArr = bArr;
                                    jArr[i44] = b17;
                                    i44++;
                                }
                                zVar = zVar;
                                bArr = bArr;
                                b17++;
                            }
                        }
                        zVar = zVar;
                        bArr = bArr;
                        int b19 = yVar.b(4);
                        if (b19 <= 2) {
                            if (b19 == 1 || b19 == 2) {
                                yVar.c(32);
                                yVar.c(32);
                                int b20 = yVar.b(4) + 1;
                                yVar.c(1);
                                if (b19 == 1) {
                                    j2 = b15 != 0 ? (long) Math.floor(Math.pow(b16, 1.0d / b15)) : 0L;
                                } else {
                                    j2 = b16 * b15;
                                }
                                yVar.c((int) (b20 * j2));
                            }
                            i12++;
                            i11 = 5;
                            t3 = i4;
                        } else {
                            throw b.d.b.a.a.t0(53, "lookup type greater than 2 not decodable: ", b19, null);
                        }
                    } else {
                        throw b.d.b.a.a.t0(66, "expected code book to start with [0x56, 0x43, 0x42] at ", (yVar.c * 8) + yVar.d, null);
                    }
                }
            }
        }
        aVar = null;
        this.n = aVar;
        if (aVar == null) {
            return true;
        }
        b0 b0Var2 = aVar.a;
        ArrayList arrayList = new ArrayList();
        arrayList.add(b0Var2.g);
        arrayList.add(aVar.f1237b);
        j1.b bVar2 = new j1.b();
        bVar2.k = "audio/vorbis";
        bVar2.f = b0Var2.d;
        bVar2.g = b0Var2.c;
        bVar2.f1019x = b0Var2.a;
        bVar2.f1020y = b0Var2.f1159b;
        bVar2.m = arrayList;
        bVar.a = bVar2.a();
        return true;
    }

    @Override // b.i.a.c.x2.j0.i
    public void e(boolean z2) {
        super.e(z2);
        if (z2) {
            this.n = null;
            this.q = null;
            this.r = null;
        }
        this.o = 0;
        this.p = false;
    }
}
