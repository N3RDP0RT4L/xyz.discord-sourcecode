package b.i.a.c.x2.j0;

import androidx.annotation.Nullable;
import b.i.a.c.x2.i;
import b.i.a.c.x2.t;
import java.io.IOException;
/* compiled from: OggSeeker.java */
/* loaded from: classes3.dex */
public interface g {
    @Nullable
    t a();

    long b(i iVar) throws IOException;

    void c(long j);
}
