package b.i.a.c.x2.d0;

import b.c.a.a0.d;
import b.i.a.c.f3.x;
import b.i.a.c.x2.a;
import b.i.a.c.x2.i;
import b.i.a.c.x2.m;
import b.i.a.c.x2.o;
import java.io.IOException;
/* compiled from: FlacBinarySearchSeeker.java */
/* loaded from: classes3.dex */
public final class c extends b.i.a.c.x2.a {

    /* compiled from: FlacBinarySearchSeeker.java */
    /* loaded from: classes3.dex */
    public static final class b implements a.f {
        public final o a;

        /* renamed from: b  reason: collision with root package name */
        public final int f1164b;
        public final m.a c = new m.a();

        public b(o oVar, int i, a aVar) {
            this.a = oVar;
            this.f1164b = i;
        }

        @Override // b.i.a.c.x2.a.f
        public /* synthetic */ void a() {
            b.i.a.c.x2.b.a(this);
        }

        @Override // b.i.a.c.x2.a.f
        public a.e b(i iVar, long j) throws IOException {
            long position = iVar.getPosition();
            long c = c(iVar);
            long f = iVar.f();
            iVar.g(Math.max(6, this.a.c));
            long c2 = c(iVar);
            long f2 = iVar.f();
            if (c <= j && c2 > j) {
                return a.e.b(f);
            }
            if (c2 <= j) {
                return a.e.c(c2, f2);
            }
            return a.e.a(c, position);
        }

        public final long c(i iVar) throws IOException {
            while (iVar.f() < iVar.b() - 6) {
                o oVar = this.a;
                int i = this.f1164b;
                m.a aVar = this.c;
                long f = iVar.f();
                byte[] bArr = new byte[2];
                boolean z2 = false;
                iVar.o(bArr, 0, 2);
                if ((((bArr[0] & 255) << 8) | (bArr[1] & 255)) != i) {
                    iVar.k();
                    iVar.g((int) (f - iVar.getPosition()));
                } else {
                    x xVar = new x(16);
                    System.arraycopy(bArr, 0, xVar.a, 0, 2);
                    xVar.D(d.z1(iVar, xVar.a, 2, 14));
                    iVar.k();
                    iVar.g((int) (f - iVar.getPosition()));
                    z2 = m.b(xVar, oVar, i, aVar);
                }
                if (z2) {
                    break;
                }
                iVar.g(1);
            }
            if (iVar.f() < iVar.b() - 6) {
                return this.c.a;
            }
            iVar.g((int) (iVar.b() - iVar.f()));
            return this.a.j;
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public c(final b.i.a.c.x2.o r17, int r18, long r19, long r21) {
        /*
            r16 = this;
            r0 = r17
            java.util.Objects.requireNonNull(r17)
            b.i.a.c.x2.d0.b r1 = new b.i.a.c.x2.d0.b
            r1.<init>()
            b.i.a.c.x2.d0.c$b r2 = new b.i.a.c.x2.d0.c$b
            r3 = 0
            r4 = r18
            r2.<init>(r0, r4, r3)
            long r3 = r17.d()
            long r7 = r0.j
            int r5 = r0.d
            if (r5 <= 0) goto L27
            long r5 = (long) r5
            int r9 = r0.c
            long r9 = (long) r9
            long r5 = r5 + r9
            r9 = 2
            long r5 = r5 / r9
            r9 = 1
            goto L42
        L27:
            int r5 = r0.a
            int r6 = r0.f1288b
            if (r5 != r6) goto L31
            if (r5 <= 0) goto L31
            long r5 = (long) r5
            goto L33
        L31:
            r5 = 4096(0x1000, double:2.0237E-320)
        L33:
            int r9 = r0.g
            long r9 = (long) r9
            long r5 = r5 * r9
            int r9 = r0.h
            long r9 = (long) r9
            long r5 = r5 * r9
            r9 = 8
            long r5 = r5 / r9
            r9 = 64
        L42:
            long r13 = r5 + r9
            r5 = 6
            int r0 = r0.c
            int r15 = java.lang.Math.max(r5, r0)
            r5 = 0
            r0 = r16
            r9 = r19
            r11 = r21
            r0.<init>(r1, r2, r3, r5, r7, r9, r11, r13, r15)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.d0.c.<init>(b.i.a.c.x2.o, int, long, long):void");
    }
}
