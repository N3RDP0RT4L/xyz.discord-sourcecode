package b.i.a.c.x2.d0;

import androidx.annotation.Nullable;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.x;
import b.i.a.c.x2.h;
import b.i.a.c.x2.i;
import b.i.a.c.x2.j;
import b.i.a.c.x2.m;
import b.i.a.c.x2.n;
import b.i.a.c.x2.o;
import b.i.a.c.x2.s;
import b.i.a.c.x2.t;
import b.i.a.c.x2.w;
import b.i.b.a.c;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.flac.PictureFrame;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;
/* compiled from: FlacExtractor.java */
/* loaded from: classes3.dex */
public final class d implements h {
    public final byte[] a = new byte[42];

    /* renamed from: b  reason: collision with root package name */
    public final x f1165b = new x(new byte[32768], 0);
    public final boolean c;
    public final m.a d;
    public j e;
    public w f;
    public int g;
    @Nullable
    public Metadata h;
    public o i;
    public int j;
    public int k;
    public c l;
    public int m;
    public long n;

    static {
        a aVar = a.a;
    }

    public d(int i) {
        this.c = (i & 1) == 0 ? false : true;
        this.d = new m.a();
        this.g = 0;
    }

    public final void a() {
        o oVar = this.i;
        int i = e0.a;
        this.f.d((this.n * 1000000) / oVar.e, 1, this.m, 0, null);
    }

    @Override // b.i.a.c.x2.h
    public boolean b(i iVar) throws IOException {
        b.c.a.a0.d.y1(iVar, false);
        byte[] bArr = new byte[4];
        iVar.o(bArr, 0, 4);
        return (((((((long) bArr[0]) & 255) << 24) | ((((long) bArr[1]) & 255) << 16)) | ((((long) bArr[2]) & 255) << 8)) | (255 & ((long) bArr[3]))) == 1716281667;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v0 */
    /* JADX WARN: Type inference failed for: r4v11 */
    /* JADX WARN: Type inference failed for: r4v8, types: [boolean, int] */
    @Override // b.i.a.c.x2.h
    public int e(i iVar, s sVar) throws IOException {
        byte[] bArr;
        boolean z2;
        o oVar;
        t tVar;
        long j;
        boolean z3;
        int i = this.g;
        boolean z4 = true;
        ?? r4 = 0;
        if (i == 0) {
            iVar.k();
            long f = iVar.f();
            Metadata y1 = b.c.a.a0.d.y1(iVar, !this.c);
            iVar.l((int) (iVar.f() - f));
            this.h = y1;
            this.g = 1;
            return 0;
        } else if (i != 1) {
            int i2 = 4;
            int i3 = 3;
            if (i != 2) {
                int i4 = 7;
                if (i != 3) {
                    long j2 = 0;
                    if (i == 4) {
                        iVar.k();
                        byte[] bArr2 = new byte[2];
                        iVar.o(bArr2, 0, 2);
                        int i5 = (bArr2[1] & 255) | ((bArr2[0] & 255) << 8);
                        if ((i5 >> 2) == 16382) {
                            iVar.k();
                            this.k = i5;
                            j jVar = this.e;
                            int i6 = e0.a;
                            long position = iVar.getPosition();
                            long b2 = iVar.b();
                            Objects.requireNonNull(this.i);
                            o oVar2 = this.i;
                            if (oVar2.k != null) {
                                tVar = new n(oVar2, position);
                            } else if (b2 == -1 || oVar2.j <= 0) {
                                tVar = new t.b(oVar2.d(), 0L);
                            } else {
                                c cVar = new c(oVar2, this.k, position, b2);
                                this.l = cVar;
                                tVar = cVar.a;
                            }
                            jVar.a(tVar);
                            this.g = 5;
                            return 0;
                        }
                        iVar.k();
                        throw ParserException.a("First frame does not start with sync code.", null);
                    } else if (i == 5) {
                        Objects.requireNonNull(this.f);
                        Objects.requireNonNull(this.i);
                        c cVar2 = this.l;
                        if (cVar2 != null && cVar2.b()) {
                            return this.l.a(iVar, sVar);
                        }
                        if (this.n == -1) {
                            o oVar3 = this.i;
                            iVar.k();
                            iVar.g(1);
                            byte[] bArr3 = new byte[1];
                            iVar.o(bArr3, 0, 1);
                            boolean z5 = (bArr3[0] & 1) == 1;
                            iVar.g(2);
                            if (!z5) {
                                i4 = 6;
                            }
                            x xVar = new x(i4);
                            xVar.D(b.c.a.a0.d.z1(iVar, xVar.a, 0, i4));
                            iVar.k();
                            try {
                                long z6 = xVar.z();
                                if (!z5) {
                                    z6 *= oVar3.f1288b;
                                }
                                j2 = z6;
                            } catch (NumberFormatException unused) {
                                z4 = false;
                            }
                            if (z4) {
                                this.n = j2;
                                return 0;
                            }
                            throw ParserException.a(null, null);
                        }
                        x xVar2 = this.f1165b;
                        int i7 = xVar2.c;
                        if (i7 < 32768) {
                            int read = iVar.read(xVar2.a, i7, 32768 - i7);
                            if (read != -1) {
                                z4 = false;
                            }
                            if (!z4) {
                                this.f1165b.D(i7 + read);
                            } else if (this.f1165b.a() == 0) {
                                a();
                                return -1;
                            }
                        } else {
                            z4 = false;
                        }
                        x xVar3 = this.f1165b;
                        int i8 = xVar3.f980b;
                        int i9 = this.m;
                        int i10 = this.j;
                        if (i9 < i10) {
                            xVar3.F(Math.min(i10 - i9, xVar3.a()));
                        }
                        x xVar4 = this.f1165b;
                        Objects.requireNonNull(this.i);
                        int i11 = xVar4.f980b;
                        while (true) {
                            if (i11 <= xVar4.c - 16) {
                                xVar4.E(i11);
                                if (m.b(xVar4, this.i, this.k, this.d)) {
                                    xVar4.E(i11);
                                    j = this.d.a;
                                    break;
                                }
                                i11++;
                            } else {
                                if (z4) {
                                    while (true) {
                                        int i12 = xVar4.c;
                                        if (i11 > i12 - this.j) {
                                            xVar4.E(i12);
                                            break;
                                        }
                                        xVar4.E(i11);
                                        try {
                                            z3 = m.b(xVar4, this.i, this.k, this.d);
                                        } catch (IndexOutOfBoundsException unused2) {
                                            z3 = false;
                                        }
                                        if (xVar4.f980b > xVar4.c) {
                                            z3 = false;
                                        }
                                        if (z3) {
                                            xVar4.E(i11);
                                            j = this.d.a;
                                            break;
                                        }
                                        i11++;
                                    }
                                } else {
                                    xVar4.E(i11);
                                }
                                j = -1;
                            }
                        }
                        x xVar5 = this.f1165b;
                        int i13 = xVar5.f980b - i8;
                        xVar5.E(i8);
                        this.f.c(this.f1165b, i13);
                        this.m += i13;
                        if (j != -1) {
                            a();
                            this.m = 0;
                            this.n = j;
                        }
                        if (this.f1165b.a() >= 16) {
                            return 0;
                        }
                        int a = this.f1165b.a();
                        x xVar6 = this.f1165b;
                        byte[] bArr4 = xVar6.a;
                        System.arraycopy(bArr4, xVar6.f980b, bArr4, 0, a);
                        this.f1165b.E(0);
                        this.f1165b.D(a);
                        return 0;
                    } else {
                        throw new IllegalStateException();
                    }
                } else {
                    o oVar4 = this.i;
                    boolean z7 = false;
                    while (!z7) {
                        iVar.k();
                        b.i.a.c.f3.w wVar = new b.i.a.c.f3.w(new byte[i2]);
                        byte[] bArr5 = wVar.a;
                        int i14 = r4 == true ? 1 : 0;
                        int i15 = r4 == true ? 1 : 0;
                        iVar.o(bArr5, i14, i2);
                        boolean f2 = wVar.f();
                        int g = wVar.g(i4);
                        int g2 = wVar.g(24) + i2;
                        if (g == 0) {
                            byte[] bArr6 = new byte[38];
                            iVar.readFully(bArr6, r4, 38);
                            oVar4 = new o(bArr6, i2);
                        } else if (oVar4 == null) {
                            throw new IllegalArgumentException();
                        } else if (g == i3) {
                            x xVar7 = new x(g2);
                            iVar.readFully(xVar7.a, r4, g2);
                            oVar4 = oVar4.b(b.c.a.a0.d.L1(xVar7));
                        } else {
                            if (g == i2) {
                                x xVar8 = new x(g2);
                                iVar.readFully(xVar8.a, r4, g2);
                                xVar8.F(i2);
                                z2 = f2;
                                oVar = new o(oVar4.a, oVar4.f1288b, oVar4.c, oVar4.d, oVar4.e, oVar4.g, oVar4.h, oVar4.j, oVar4.k, oVar4.f(o.a(Arrays.asList(b.c.a.a0.d.N1(xVar8, r4, r4).a), Collections.emptyList())));
                            } else {
                                z2 = f2;
                                if (g == 6) {
                                    x xVar9 = new x(g2);
                                    iVar.readFully(xVar9.a, 0, g2);
                                    xVar9.F(i2);
                                    int f3 = xVar9.f();
                                    String r = xVar9.r(xVar9.f(), c.a);
                                    String q = xVar9.q(xVar9.f());
                                    int f4 = xVar9.f();
                                    int f5 = xVar9.f();
                                    int f6 = xVar9.f();
                                    int f7 = xVar9.f();
                                    int f8 = xVar9.f();
                                    byte[] bArr7 = new byte[f8];
                                    System.arraycopy(xVar9.a, xVar9.f980b, bArr7, 0, f8);
                                    xVar9.f980b += f8;
                                    oVar = new o(oVar4.a, oVar4.f1288b, oVar4.c, oVar4.d, oVar4.e, oVar4.g, oVar4.h, oVar4.j, oVar4.k, oVar4.f(o.a(Collections.emptyList(), Collections.singletonList(new PictureFrame(f3, r, q, f4, f5, f6, f7, bArr7)))));
                                } else {
                                    iVar.l(g2);
                                    int i16 = e0.a;
                                    this.i = oVar4;
                                    z7 = z2;
                                    r4 = 0;
                                    i2 = 4;
                                    i3 = 3;
                                    i4 = 7;
                                }
                            }
                            oVar4 = oVar;
                            int i162 = e0.a;
                            this.i = oVar4;
                            z7 = z2;
                            r4 = 0;
                            i2 = 4;
                            i3 = 3;
                            i4 = 7;
                        }
                        z2 = f2;
                        int i1622 = e0.a;
                        this.i = oVar4;
                        z7 = z2;
                        r4 = 0;
                        i2 = 4;
                        i3 = 3;
                        i4 = 7;
                    }
                    Objects.requireNonNull(this.i);
                    this.j = Math.max(this.i.c, 6);
                    w wVar2 = this.f;
                    int i17 = e0.a;
                    wVar2.e(this.i.e(this.a, this.h));
                    this.g = 4;
                    return 0;
                }
            } else {
                iVar.readFully(new byte[4], 0, 4);
                if ((((bArr[0] & 255) << 24) | ((bArr[1] & 255) << 16) | ((bArr[2] & 255) << 8) | (bArr[3] & 255)) == 1716281667) {
                    this.g = 3;
                    return 0;
                }
                throw ParserException.a("Failed to read FLAC stream marker.", null);
            }
        } else {
            byte[] bArr8 = this.a;
            iVar.o(bArr8, 0, bArr8.length);
            iVar.k();
            this.g = 2;
            return 0;
        }
    }

    @Override // b.i.a.c.x2.h
    public void f(j jVar) {
        this.e = jVar;
        this.f = jVar.p(0, 1);
        jVar.j();
    }

    @Override // b.i.a.c.x2.h
    public void g(long j, long j2) {
        long j3 = 0;
        if (j == 0) {
            this.g = 0;
        } else {
            c cVar = this.l;
            if (cVar != null) {
                cVar.e(j2);
            }
        }
        if (j2 != 0) {
            j3 = -1;
        }
        this.n = j3;
        this.m = 0;
        this.f1165b.A(0);
    }

    @Override // b.i.a.c.x2.h
    public void release() {
    }
}
