package b.i.a.c.x2.l0;

import b.c.a.a0.d;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.x;
import b.i.a.c.j1;
import b.i.a.c.x2.h;
import b.i.a.c.x2.i;
import b.i.a.c.x2.j;
import b.i.a.c.x2.w;
import com.google.android.exoplayer2.ParserException;
import java.io.IOException;
import org.objectweb.asm.Opcodes;
/* compiled from: WavExtractor.java */
/* loaded from: classes3.dex */
public final class b implements h {
    public j a;

    /* renamed from: b  reason: collision with root package name */
    public w f1281b;
    public AbstractC0104b d;
    public int c = 0;
    public int e = -1;
    public long f = -1;

    /* compiled from: WavExtractor.java */
    /* loaded from: classes3.dex */
    public static final class a implements AbstractC0104b {
        public static final int[] a = {-1, -1, -1, -1, 2, 4, 6, 8, -1, -1, -1, -1, 2, 4, 6, 8};

        /* renamed from: b  reason: collision with root package name */
        public static final int[] f1282b = {7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 19, 21, 23, 25, 28, 31, 34, 37, 41, 45, 50, 55, 60, 66, 73, 80, 88, 97, 107, 118, 130, Opcodes.D2L, 157, Opcodes.LRETURN, Opcodes.ARRAYLENGTH, 209, 230, 253, 279, 307, 337, 371, 408, 449, 494, 544, 598, 658, 724, 796, 876, 963, 1060, 1166, 1282, 1411, 1552, 1707, 1878, 2066, 2272, 2499, 2749, 3024, 3327, 3660, 4026, 4428, 4871, 5358, 5894, 6484, 7132, 7845, 8630, 9493, 10442, 11487, 12635, 13899, 15289, 16818, 18500, 20350, 22385, 24623, 27086, 29794, 32767};
        public final j c;
        public final w d;
        public final b.i.a.c.x2.l0.c e;
        public final int f;
        public final byte[] g;
        public final x h;
        public final int i;
        public final j1 j;
        public int k;
        public long l;
        public int m;
        public long n;

        public a(j jVar, w wVar, b.i.a.c.x2.l0.c cVar) throws ParserException {
            this.c = jVar;
            this.d = wVar;
            this.e = cVar;
            int max = Math.max(1, cVar.c / 10);
            this.i = max;
            byte[] bArr = cVar.f;
            int length = bArr.length;
            byte b2 = bArr[0];
            byte b3 = bArr[1];
            int i = ((bArr[3] & 255) << 8) | (bArr[2] & 255);
            this.f = i;
            int i2 = cVar.f1284b;
            int i3 = (((cVar.d - (i2 * 4)) * 8) / (cVar.e * i2)) + 1;
            if (i == i3) {
                int f = e0.f(max, i);
                this.g = new byte[cVar.d * f];
                this.h = new x(i * 2 * i2 * f);
                int i4 = cVar.c;
                int i5 = ((cVar.d * i4) * 8) / i;
                j1.b bVar = new j1.b();
                bVar.k = "audio/raw";
                bVar.f = i5;
                bVar.g = i5;
                bVar.l = max * 2 * i2;
                bVar.f1019x = cVar.f1284b;
                bVar.f1020y = i4;
                bVar.f1021z = 2;
                this.j = bVar.a();
                return;
            }
            throw ParserException.a(b.d.b.a.a.g(56, "Expected frames per block: ", i3, "; got: ", i), null);
        }

        @Override // b.i.a.c.x2.l0.b.AbstractC0104b
        public void a(long j) {
            this.k = 0;
            this.l = j;
            this.m = 0;
            this.n = 0L;
        }

        @Override // b.i.a.c.x2.l0.b.AbstractC0104b
        public void b(int i, long j) {
            this.c.a(new e(this.e, this.f, i, j));
            this.d.e(this.j);
        }

        /* JADX WARN: Removed duplicated region for block: B:13:0x0043 A[LOOP:0: B:6:0x0028->B:13:0x0043, LOOP_END] */
        /* JADX WARN: Removed duplicated region for block: B:16:0x0052  */
        /* JADX WARN: Removed duplicated region for block: B:37:0x015c  */
        /* JADX WARN: Removed duplicated region for block: B:45:0x0041 A[SYNTHETIC] */
        /* JADX WARN: Removed duplicated region for block: B:8:0x002b  */
        /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:11:0x003f -> B:12:0x0041). Please submit an issue!!! */
        @Override // b.i.a.c.x2.l0.b.AbstractC0104b
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public boolean c(b.i.a.c.x2.i r19, long r20) throws java.io.IOException {
            /*
                Method dump skipped, instructions count: 364
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.l0.b.a.c(b.i.a.c.x2.i, long):boolean");
        }

        public final int d(int i) {
            return i / (this.e.f1284b * 2);
        }

        public final void e(int i) {
            long F = this.l + e0.F(this.n, 1000000L, this.e.c);
            int i2 = i * 2 * this.e.f1284b;
            this.d.d(F, 1, i2, this.m - i2, null);
            this.n += i;
            this.m -= i2;
        }
    }

    /* compiled from: WavExtractor.java */
    /* renamed from: b.i.a.c.x2.l0.b$b  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public interface AbstractC0104b {
        void a(long j);

        void b(int i, long j) throws ParserException;

        boolean c(i iVar, long j) throws IOException;
    }

    /* compiled from: WavExtractor.java */
    /* loaded from: classes3.dex */
    public static final class c implements AbstractC0104b {
        public final j a;

        /* renamed from: b  reason: collision with root package name */
        public final w f1283b;
        public final b.i.a.c.x2.l0.c c;
        public final j1 d;
        public final int e;
        public long f;
        public int g;
        public long h;

        public c(j jVar, w wVar, b.i.a.c.x2.l0.c cVar, String str, int i) throws ParserException {
            this.a = jVar;
            this.f1283b = wVar;
            this.c = cVar;
            int i2 = (cVar.f1284b * cVar.e) / 8;
            int i3 = cVar.d;
            if (i3 == i2) {
                int i4 = cVar.c * i2;
                int i5 = i4 * 8;
                int max = Math.max(i2, i4 / 10);
                this.e = max;
                j1.b bVar = new j1.b();
                bVar.k = str;
                bVar.f = i5;
                bVar.g = i5;
                bVar.l = max;
                bVar.f1019x = cVar.f1284b;
                bVar.f1020y = cVar.c;
                bVar.f1021z = i;
                this.d = bVar.a();
                return;
            }
            throw ParserException.a(b.d.b.a.a.g(50, "Expected block size: ", i2, "; got: ", i3), null);
        }

        @Override // b.i.a.c.x2.l0.b.AbstractC0104b
        public void a(long j) {
            this.f = j;
            this.g = 0;
            this.h = 0L;
        }

        @Override // b.i.a.c.x2.l0.b.AbstractC0104b
        public void b(int i, long j) {
            this.a.a(new e(this.c, 1, i, j));
            this.f1283b.e(this.d);
        }

        @Override // b.i.a.c.x2.l0.b.AbstractC0104b
        public boolean c(i iVar, long j) throws IOException {
            int i;
            b.i.a.c.x2.l0.c cVar;
            int i2;
            int i3;
            long j2 = j;
            while (true) {
                i = (j2 > 0L ? 1 : (j2 == 0L ? 0 : -1));
                if (i <= 0 || (i2 = this.g) >= (i3 = this.e)) {
                    break;
                }
                int b2 = this.f1283b.b(iVar, (int) Math.min(i3 - i2, j2), true);
                if (b2 == -1) {
                    j2 = 0;
                } else {
                    this.g += b2;
                    j2 -= b2;
                }
            }
            int i4 = this.c.d;
            int i5 = this.g / i4;
            if (i5 > 0) {
                int i6 = i5 * i4;
                int i7 = this.g - i6;
                this.f1283b.d(this.f + e0.F(this.h, 1000000L, cVar.c), 1, i6, i7, null);
                this.h += i5;
                this.g = i7;
            }
            return i <= 0;
        }
    }

    static {
        b.i.a.c.x2.l0.a aVar = b.i.a.c.x2.l0.a.a;
    }

    @Override // b.i.a.c.x2.h
    public boolean b(i iVar) throws IOException {
        return d.r(iVar);
    }

    /* JADX WARN: Removed duplicated region for block: B:65:0x01c0  */
    /* JADX WARN: Removed duplicated region for block: B:68:0x01d8  */
    @Override // b.i.a.c.x2.h
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public int e(b.i.a.c.x2.i r25, b.i.a.c.x2.s r26) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 552
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.l0.b.e(b.i.a.c.x2.i, b.i.a.c.x2.s):int");
    }

    @Override // b.i.a.c.x2.h
    public void f(j jVar) {
        this.a = jVar;
        this.f1281b = jVar.p(0, 1);
        jVar.j();
    }

    @Override // b.i.a.c.x2.h
    public void g(long j, long j2) {
        this.c = j == 0 ? 0 : 3;
        AbstractC0104b bVar = this.d;
        if (bVar != null) {
            bVar.a(j2);
        }
    }

    @Override // b.i.a.c.x2.h
    public void release() {
    }
}
