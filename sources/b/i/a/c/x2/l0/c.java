package b.i.a.c.x2.l0;
/* compiled from: WavFormat.java */
/* loaded from: classes3.dex */
public final class c {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final int f1284b;
    public final int c;
    public final int d;
    public final int e;
    public final byte[] f;

    public c(int i, int i2, int i3, int i4, int i5, int i6, byte[] bArr) {
        this.a = i;
        this.f1284b = i2;
        this.c = i3;
        this.d = i5;
        this.e = i6;
        this.f = bArr;
    }
}
