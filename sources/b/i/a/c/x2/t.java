package b.i.a.c.x2;

import androidx.annotation.Nullable;
/* compiled from: SeekMap.java */
/* loaded from: classes3.dex */
public interface t {

    /* compiled from: SeekMap.java */
    /* loaded from: classes3.dex */
    public static class b implements t {
        public final long a;

        /* renamed from: b  reason: collision with root package name */
        public final a f1293b;

        public b(long j, long j2) {
            this.a = j;
            this.f1293b = new a(j2 == 0 ? u.a : new u(0L, j2));
        }

        @Override // b.i.a.c.x2.t
        public boolean c() {
            return false;
        }

        @Override // b.i.a.c.x2.t
        public a h(long j) {
            return this.f1293b;
        }

        @Override // b.i.a.c.x2.t
        public long i() {
            return this.a;
        }
    }

    boolean c();

    a h(long j);

    long i();

    /* compiled from: SeekMap.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public final u a;

        /* renamed from: b  reason: collision with root package name */
        public final u f1292b;

        public a(u uVar) {
            this.a = uVar;
            this.f1292b = uVar;
        }

        public boolean equals(@Nullable Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || a.class != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            return this.a.equals(aVar.a) && this.f1292b.equals(aVar.f1292b);
        }

        public int hashCode() {
            return this.f1292b.hashCode() + (this.a.hashCode() * 31);
        }

        public String toString() {
            String str;
            String valueOf = String.valueOf(this.a);
            if (this.a.equals(this.f1292b)) {
                str = "";
            } else {
                String valueOf2 = String.valueOf(this.f1292b);
                str = b.d.b.a.a.i(valueOf2.length() + 2, ", ", valueOf2);
            }
            return b.d.b.a.a.k(b.d.b.a.a.b(str, valueOf.length() + 2), "[", valueOf, str, "]");
        }

        public a(u uVar, u uVar2) {
            this.a = uVar;
            this.f1292b = uVar2;
        }
    }
}
