package b.i.a.c.x2;

import b.i.a.c.e3.h;
import java.io.IOException;
/* compiled from: ExtractorInput.java */
/* loaded from: classes3.dex */
public interface i extends h {
    long b();

    boolean c(byte[] bArr, int i, int i2, boolean z2) throws IOException;

    boolean e(byte[] bArr, int i, int i2, boolean z2) throws IOException;

    long f();

    void g(int i) throws IOException;

    long getPosition();

    int h(int i) throws IOException;

    int i(byte[] bArr, int i, int i2) throws IOException;

    void k();

    void l(int i) throws IOException;

    boolean m(int i, boolean z2) throws IOException;

    void o(byte[] bArr, int i, int i2) throws IOException;

    @Override // b.i.a.c.e3.h
    int read(byte[] bArr, int i, int i2) throws IOException;

    void readFully(byte[] bArr, int i, int i2) throws IOException;
}
