package b.i.a.c.x2;
/* compiled from: ExtractorOutput.java */
/* loaded from: classes3.dex */
public interface j {
    public static final j d = new a();

    /* compiled from: ExtractorOutput.java */
    /* loaded from: classes3.dex */
    public class a implements j {
        @Override // b.i.a.c.x2.j
        public void a(t tVar) {
            throw new UnsupportedOperationException();
        }

        @Override // b.i.a.c.x2.j
        public void j() {
            throw new UnsupportedOperationException();
        }

        @Override // b.i.a.c.x2.j
        public w p(int i, int i2) {
            throw new UnsupportedOperationException();
        }
    }

    void a(t tVar);

    void j();

    w p(int i, int i2);
}
