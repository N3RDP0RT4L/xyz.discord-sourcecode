package b.i.a.c.x2.f0;

import android.util.Log;
import androidx.annotation.Nullable;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.x;
import b.i.a.c.j1;
import b.i.a.c.x2.f0.b;
import b.i.a.c.x2.h;
import b.i.a.c.x2.i;
import b.i.a.c.x2.i0.l;
import b.i.a.c.x2.j;
import b.i.a.c.x2.s;
import b.i.a.c.x2.t;
import b.i.a.c.x2.w;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.mp4.MotionPhotoMetadata;
import java.io.IOException;
import java.util.Objects;
import org.xmlpull.v1.XmlPullParserException;
/* compiled from: JpegExtractor.java */
/* loaded from: classes3.dex */
public final class a implements h {

    /* renamed from: b  reason: collision with root package name */
    public j f1173b;
    public int c;
    public int d;
    public int e;
    @Nullable
    public MotionPhotoMetadata g;
    public i h;
    public c i;
    @Nullable
    public b.i.a.c.x2.i0.i j;
    public final x a = new x(6);
    public long f = -1;

    public final void a() {
        c(new Metadata.Entry[0]);
        j jVar = this.f1173b;
        Objects.requireNonNull(jVar);
        jVar.j();
        this.f1173b.a(new t.b(-9223372036854775807L, 0L));
        this.c = 6;
    }

    @Override // b.i.a.c.x2.h
    public boolean b(i iVar) throws IOException {
        if (d(iVar) != 65496) {
            return false;
        }
        int d = d(iVar);
        this.d = d;
        if (d == 65504) {
            this.a.A(2);
            iVar.o(this.a.a, 0, 2);
            iVar.g(this.a.y() - 2);
            this.d = d(iVar);
        }
        if (this.d != 65505) {
            return false;
        }
        iVar.g(2);
        this.a.A(6);
        iVar.o(this.a.a, 0, 6);
        return this.a.u() == 1165519206 && this.a.y() == 0;
    }

    public final void c(Metadata.Entry... entryArr) {
        j jVar = this.f1173b;
        Objects.requireNonNull(jVar);
        w p = jVar.p(1024, 4);
        j1.b bVar = new j1.b();
        bVar.j = "image/jpeg";
        bVar.i = new Metadata(entryArr);
        p.e(bVar.a());
    }

    public final int d(i iVar) throws IOException {
        this.a.A(2);
        iVar.o(this.a.a, 0, 2);
        return this.a.y();
    }

    @Override // b.i.a.c.x2.h
    public int e(i iVar, s sVar) throws IOException {
        String str;
        int i;
        String str2;
        b bVar;
        long j;
        int i2 = this.c;
        if (i2 == 0) {
            this.a.A(2);
            iVar.readFully(this.a.a, 0, 2);
            int y2 = this.a.y();
            this.d = y2;
            if (y2 == 65498) {
                if (this.f != -1) {
                    this.c = 4;
                } else {
                    a();
                }
            } else if ((y2 < 65488 || y2 > 65497) && y2 != 65281) {
                this.c = 1;
            }
            return 0;
        } else if (i2 == 1) {
            this.a.A(2);
            iVar.readFully(this.a.a, 0, 2);
            this.e = this.a.y() - 2;
            this.c = 2;
            return 0;
        } else if (i2 == 2) {
            if (this.d == 65505) {
                int i3 = this.e;
                byte[] bArr = new byte[i3];
                iVar.readFully(bArr, 0, i3);
                if (this.g == null) {
                    MotionPhotoMetadata motionPhotoMetadata = null;
                    if (i3 + 0 == 0) {
                        str = null;
                        i = 0;
                    } else {
                        i = 0;
                        while (i < i3 && bArr[i] != 0) {
                            i++;
                        }
                        str = e0.m(bArr, 0, i + 0);
                        if (i < i3) {
                            i++;
                        }
                    }
                    if ("http://ns.adobe.com/xap/1.0/".equals(str)) {
                        if (i3 - i == 0) {
                            str2 = null;
                        } else {
                            int i4 = i;
                            while (i4 < i3 && bArr[i4] != 0) {
                                i4++;
                            }
                            str2 = e0.m(bArr, i, i4 - i);
                        }
                        if (str2 != null) {
                            long b2 = iVar.b();
                            if (b2 != -1) {
                                try {
                                    bVar = e.a(str2);
                                } catch (ParserException | NumberFormatException | XmlPullParserException unused) {
                                    Log.w("MotionPhotoXmpParser", "Ignoring unexpected XMP metadata");
                                    bVar = null;
                                }
                                if (bVar != null && bVar.f1174b.size() >= 2) {
                                    long j2 = -1;
                                    long j3 = -1;
                                    long j4 = -1;
                                    long j5 = -1;
                                    boolean z2 = false;
                                    for (int size = bVar.f1174b.size() - 1; size >= 0; size--) {
                                        b.a aVar = bVar.f1174b.get(size);
                                        z2 |= "video/mp4".equals(aVar.a);
                                        if (size == 0) {
                                            b2 = 0;
                                            j = b2 - aVar.c;
                                        } else {
                                            b2 -= aVar.f1175b;
                                            j = b2;
                                        }
                                        if (z2 && b2 != j) {
                                            j5 = j - b2;
                                            j4 = b2;
                                            z2 = false;
                                        }
                                        if (size == 0) {
                                            j3 = j;
                                            j2 = b2;
                                        }
                                    }
                                    if (!(j4 == -1 || j5 == -1 || j2 == -1 || j3 == -1)) {
                                        motionPhotoMetadata = new MotionPhotoMetadata(j2, j3, bVar.a, j4, j5);
                                    }
                                }
                            }
                            this.g = motionPhotoMetadata;
                            if (motionPhotoMetadata != null) {
                                this.f = motionPhotoMetadata.m;
                            }
                        }
                    }
                }
            } else {
                iVar.l(this.e);
            }
            this.c = 0;
            return 0;
        } else if (i2 == 4) {
            long position = iVar.getPosition();
            long j6 = this.f;
            if (position != j6) {
                sVar.a = j6;
                return 1;
            }
            if (!iVar.e(this.a.a, 0, 1, true)) {
                a();
            } else {
                iVar.k();
                if (this.j == null) {
                    this.j = new b.i.a.c.x2.i0.i(0);
                }
                c cVar = new c(iVar, this.f);
                this.i = cVar;
                if (l.a(cVar, false, (this.j.f1218b & 2) != 0)) {
                    b.i.a.c.x2.i0.i iVar2 = this.j;
                    long j7 = this.f;
                    j jVar = this.f1173b;
                    Objects.requireNonNull(jVar);
                    iVar2.f1219s = new d(j7, jVar);
                    MotionPhotoMetadata motionPhotoMetadata2 = this.g;
                    Objects.requireNonNull(motionPhotoMetadata2);
                    c(motionPhotoMetadata2);
                    this.c = 5;
                } else {
                    a();
                }
            }
            return 0;
        } else if (i2 == 5) {
            if (this.i == null || iVar != this.h) {
                this.h = iVar;
                this.i = new c(iVar, this.f);
            }
            b.i.a.c.x2.i0.i iVar3 = this.j;
            Objects.requireNonNull(iVar3);
            int e = iVar3.e(this.i, sVar);
            if (e == 1) {
                sVar.a += this.f;
            }
            return e;
        } else if (i2 == 6) {
            return -1;
        } else {
            throw new IllegalStateException();
        }
    }

    @Override // b.i.a.c.x2.h
    public void f(j jVar) {
        this.f1173b = jVar;
    }

    @Override // b.i.a.c.x2.h
    public void g(long j, long j2) {
        if (j == 0) {
            this.c = 0;
            this.j = null;
        } else if (this.c == 5) {
            b.i.a.c.x2.i0.i iVar = this.j;
            Objects.requireNonNull(iVar);
            iVar.g(j, j2);
        }
    }

    @Override // b.i.a.c.x2.h
    public void release() {
        b.i.a.c.x2.i0.i iVar = this.j;
        if (iVar != null) {
            Objects.requireNonNull(iVar);
        }
    }
}
