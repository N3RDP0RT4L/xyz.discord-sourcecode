package b.i.a.c.x2.f0;

import java.util.List;
/* compiled from: MotionPhotoDescription.java */
/* loaded from: classes3.dex */
public final class b {
    public final long a;

    /* renamed from: b  reason: collision with root package name */
    public final List<a> f1174b;

    /* compiled from: MotionPhotoDescription.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public final String a;

        /* renamed from: b  reason: collision with root package name */
        public final long f1175b;
        public final long c;

        public a(String str, String str2, long j, long j2) {
            this.a = str;
            this.f1175b = j;
            this.c = j2;
        }
    }

    public b(long j, List<a> list) {
        this.a = j;
        this.f1174b = list;
    }
}
