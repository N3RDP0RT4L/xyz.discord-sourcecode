package b.i.a.c.x2.f0;

import b.c.a.a0.d;
import b.i.a.c.x2.f0.b;
import b.i.a.f.e.o.f;
import b.i.b.b.a;
import b.i.b.b.h0;
import b.i.b.b.n;
import b.i.b.b.p;
import java.io.IOException;
import java.util.Arrays;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
/* compiled from: XmpMotionPhotoDescriptionParser.java */
/* loaded from: classes3.dex */
public final class e {
    public static final String[] a = {"Camera:MotionPhoto", "GCamera:MotionPhoto", "Camera:MicroVideo", "GCamera:MicroVideo"};

    /* renamed from: b  reason: collision with root package name */
    public static final String[] f1178b = {"Camera:MotionPhotoPresentationTimestampUs", "GCamera:MotionPhotoPresentationTimestampUs", "Camera:MicroVideoPresentationTimestampUs", "GCamera:MicroVideoPresentationTimestampUs"};
    public static final String[] c = {"Camera:MicroVideoOffset", "GCamera:MicroVideoOffset"};

    /* JADX WARN: Code restructure failed: missing block: B:24:0x0069, code lost:
        if (r10 == (-1)) goto L25;
     */
    /* JADX WARN: Code restructure failed: missing block: B:25:0x006b, code lost:
        r10 = -9223372036854775807L;
     */
    @androidx.annotation.Nullable
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static b.i.a.c.x2.f0.b a(java.lang.String r25) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        /*
            Method dump skipped, instructions count: 239
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.x2.f0.e.a(java.lang.String):b.i.a.c.x2.f0.b");
    }

    public static p<b.a> b(XmlPullParser xmlPullParser, String str, String str2) throws XmlPullParserException, IOException {
        a<Object> aVar = p.k;
        f.A(4, "initialCapacity");
        Object[] objArr = new Object[4];
        String concat = str.concat(":Item");
        String concat2 = str.concat(":Directory");
        int i = 0;
        do {
            xmlPullParser.next();
            if (d.Y0(xmlPullParser, concat)) {
                String concat3 = str2.concat(":Mime");
                String concat4 = str2.concat(":Semantic");
                String concat5 = str2.concat(":Length");
                String concat6 = str2.concat(":Padding");
                String r0 = d.r0(xmlPullParser, concat3);
                String r02 = d.r0(xmlPullParser, concat4);
                String r03 = d.r0(xmlPullParser, concat5);
                String r04 = d.r0(xmlPullParser, concat6);
                if (r0 == null || r02 == null) {
                    return h0.l;
                }
                b.a aVar2 = new b.a(r0, r02, r03 != null ? Long.parseLong(r03) : 0L, r04 != null ? Long.parseLong(r04) : 0L);
                int i2 = i + 1;
                if (objArr.length < i2) {
                    objArr = Arrays.copyOf(objArr, n.b.a(objArr.length, i2));
                }
                objArr[i] = aVar2;
                i = i2;
            }
        } while (!d.R0(xmlPullParser, concat2));
        return p.l(objArr, i);
    }
}
