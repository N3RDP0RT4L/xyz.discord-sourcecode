package b.i.a.c.x2;
/* compiled from: VorbisUtil.java */
/* loaded from: classes3.dex */
public final class b0 {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final int f1159b;
    public final int c;
    public final int d;
    public final int e;
    public final int f;
    public final byte[] g;

    public b0(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, boolean z2, byte[] bArr) {
        this.a = i2;
        this.f1159b = i3;
        this.c = i4;
        this.d = i5;
        this.e = i7;
        this.f = i8;
        this.g = bArr;
    }
}
