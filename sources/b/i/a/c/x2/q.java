package b.i.a.c.x2;

import androidx.annotation.Nullable;
import b.i.a.c.f3.x;
import b.i.a.c.z2.k.b;
import com.google.android.exoplayer2.metadata.Metadata;
import java.io.EOFException;
import java.io.IOException;
/* compiled from: Id3Peeker.java */
/* loaded from: classes3.dex */
public final class q {
    public final x a = new x(10);

    @Nullable
    public Metadata a(i iVar, @Nullable b.a aVar) throws IOException {
        Metadata metadata = null;
        int i = 0;
        while (true) {
            try {
                iVar.o(this.a.a, 0, 10);
                this.a.E(0);
                if (this.a.v() != 4801587) {
                    break;
                }
                this.a.F(3);
                int s2 = this.a.s();
                int i2 = s2 + 10;
                if (metadata == null) {
                    byte[] bArr = new byte[i2];
                    System.arraycopy(this.a.a, 0, bArr, 0, 10);
                    iVar.o(bArr, 10, s2);
                    metadata = new b(aVar).d(bArr, i2);
                } else {
                    iVar.g(s2);
                }
                i += i2;
            } catch (EOFException unused) {
            }
        }
        iVar.k();
        iVar.g(i);
        return metadata;
    }
}
