package b.i.a.c.w2;

import android.net.Uri;
import android.text.TextUtils;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.d.b.a.a;
import b.i.a.c.e3.n;
import b.i.a.c.e3.t;
import b.i.a.c.f3.e0;
import b.i.a.c.w2.a0;
import b.i.a.c.x0;
import b.i.b.b.i0;
import com.discord.restapi.RestAPIBuilder;
import com.google.android.exoplayer2.drm.MediaDrmCallbackException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
/* compiled from: HttpMediaDrmCallback.java */
/* loaded from: classes3.dex */
public final class d0 implements e0 {
    public final t a;
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public final String f1148b;
    public final boolean c;
    public final Map<String, String> d;

    public d0(@Nullable String str, boolean z2, t tVar) {
        d.j(!z2 || !TextUtils.isEmpty(str));
        this.a = tVar;
        this.f1148b = str;
        this.c = z2;
        this.d = new HashMap();
    }

    /* JADX WARN: Code restructure failed: missing block: B:31:0x0073, code lost:
        r4 = r4 + 1;
        r0 = r3.a();
        r0.a = android.net.Uri.parse(r7);
        r3 = r0.a();
     */
    /* JADX WARN: Code restructure failed: missing block: B:32:0x0083, code lost:
        r0 = b.i.a.c.f3.e0.a;
     */
    /* JADX WARN: Code restructure failed: missing block: B:33:0x0085, code lost:
        r5.close();
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static byte[] b(b.i.a.c.e3.t r17, java.lang.String r18, @androidx.annotation.Nullable byte[] r19, java.util.Map<java.lang.String, java.lang.String> r20) throws com.google.android.exoplayer2.drm.MediaDrmCallbackException {
        /*
            b.i.a.c.e3.y r1 = new b.i.a.c.e3.y
            r0 = r17
            b.i.a.c.e3.r$b r0 = (b.i.a.c.e3.r.b) r0
            b.i.a.c.e3.r r0 = r0.a()
            r1.<init>(r0)
            java.util.Collections.emptyMap()
            android.net.Uri r3 = android.net.Uri.parse(r18)
            if (r3 == 0) goto La6
            b.i.a.c.e3.n r16 = new b.i.a.c.e3.n
            r4 = 0
            r6 = 2
            r9 = 0
            r11 = -1
            r13 = 0
            r14 = 1
            r15 = 0
            r2 = r16
            r7 = r19
            r8 = r20
            r2.<init>(r3, r4, r6, r7, r8, r9, r11, r13, r14, r15)
            r2 = 0
            r3 = r16
            r4 = 0
        L2f:
            b.i.a.c.e3.m r5 = new b.i.a.c.e3.m     // Catch: java.lang.Exception -> L90
            r5.<init>(r1, r3)     // Catch: java.lang.Exception -> L90
            byte[] r0 = b.i.a.c.f3.e0.K(r5)     // Catch: java.lang.Throwable -> L3e com.google.android.exoplayer2.upstream.HttpDataSource$InvalidResponseCodeException -> L40
            int r2 = b.i.a.c.f3.e0.a     // Catch: java.lang.Exception -> L90
            r5.close()     // Catch: java.io.IOException -> L3d java.lang.Exception -> L90
        L3d:
            return r0
        L3e:
            r0 = move-exception
            goto L8a
        L40:
            r0 = move-exception
            r6 = r0
            int r0 = r6.responseCode     // Catch: java.lang.Throwable -> L3e
            r7 = 307(0x133, float:4.3E-43)
            if (r0 == r7) goto L4c
            r7 = 308(0x134, float:4.32E-43)
            if (r0 != r7) goto L51
        L4c:
            r0 = 5
            if (r4 >= r0) goto L51
            r0 = 1
            goto L52
        L51:
            r0 = 0
        L52:
            r7 = 0
            if (r0 != 0) goto L56
            goto L71
        L56:
            java.util.Map<java.lang.String, java.util.List<java.lang.String>> r0 = r6.headerFields     // Catch: java.lang.Throwable -> L3e
            if (r0 == 0) goto L71
            java.lang.String r8 = "Location"
            java.lang.Object r0 = r0.get(r8)     // Catch: java.lang.Throwable -> L3e
            java.util.List r0 = (java.util.List) r0     // Catch: java.lang.Throwable -> L3e
            if (r0 == 0) goto L71
            boolean r8 = r0.isEmpty()     // Catch: java.lang.Throwable -> L3e
            if (r8 != 0) goto L71
            java.lang.Object r0 = r0.get(r2)     // Catch: java.lang.Throwable -> L3e
            r7 = r0
            java.lang.String r7 = (java.lang.String) r7     // Catch: java.lang.Throwable -> L3e
        L71:
            if (r7 == 0) goto L89
            int r4 = r4 + 1
            b.i.a.c.e3.n$b r0 = r3.a()     // Catch: java.lang.Throwable -> L3e
            android.net.Uri r3 = android.net.Uri.parse(r7)     // Catch: java.lang.Throwable -> L3e
            r0.a = r3     // Catch: java.lang.Throwable -> L3e
            b.i.a.c.e3.n r3 = r0.a()     // Catch: java.lang.Throwable -> L3e
            int r0 = b.i.a.c.f3.e0.a     // Catch: java.lang.Exception -> L90
            r5.close()     // Catch: java.io.IOException -> L2f java.lang.Exception -> L90
            goto L2f
        L89:
            throw r6     // Catch: java.lang.Throwable -> L3e
        L8a:
            int r2 = b.i.a.c.f3.e0.a     // Catch: java.lang.Exception -> L90
            r5.close()     // Catch: java.io.IOException -> L8f java.lang.Exception -> L90
        L8f:
            throw r0     // Catch: java.lang.Exception -> L90
        L90:
            r0 = move-exception
            r10 = r0
            com.google.android.exoplayer2.drm.MediaDrmCallbackException r0 = new com.google.android.exoplayer2.drm.MediaDrmCallbackException
            android.net.Uri r6 = r1.c
            java.util.Objects.requireNonNull(r6)
            java.util.Map r7 = r1.j()
            long r8 = r1.f952b
            r4 = r0
            r5 = r16
            r4.<init>(r5, r6, r7, r8, r10)
            throw r0
        La6:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "The uri must be set."
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.w2.d0.b(b.i.a.c.e3.t, java.lang.String, byte[], java.util.Map):byte[]");
    }

    public byte[] a(UUID uuid, a0.a aVar) throws MediaDrmCallbackException {
        String str;
        String str2 = aVar.f1144b;
        if (this.c || TextUtils.isEmpty(str2)) {
            str2 = this.f1148b;
        }
        if (!TextUtils.isEmpty(str2)) {
            HashMap hashMap = new HashMap();
            UUID uuid2 = x0.e;
            if (uuid2.equals(uuid)) {
                str = "text/xml";
            } else {
                str = x0.c.equals(uuid) ? RestAPIBuilder.CONTENT_TYPE_JSON : "application/octet-stream";
            }
            hashMap.put("Content-Type", str);
            if (uuid2.equals(uuid)) {
                hashMap.put("SOAPAction", "http://schemas.microsoft.com/DRM/2007/03/protocols/AcquireLicense");
            }
            synchronized (this.d) {
                hashMap.putAll(this.d);
            }
            return b(this.a, str2, aVar.a, hashMap);
        }
        Map emptyMap = Collections.emptyMap();
        Uri uri = Uri.EMPTY;
        if (uri != null) {
            throw new MediaDrmCallbackException(new n(uri, 0L, 1, null, emptyMap, 0L, -1L, null, 0, null), Uri.EMPTY, i0.m, 0L, new IllegalStateException("No license URL"));
        }
        throw new IllegalStateException("The uri must be set.");
    }

    public byte[] c(UUID uuid, a0.d dVar) throws MediaDrmCallbackException {
        String str = dVar.f1145b;
        String l = e0.l(dVar.a);
        return b(this.a, a.j(l.length() + a.b(str, 15), str, "&signedRequest=", l), null, Collections.emptyMap());
    }
}
