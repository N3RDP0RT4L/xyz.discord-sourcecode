package b.i.a.c.w2;

import android.media.DeniedByServerException;
import android.media.MediaCrypto;
import android.media.MediaCryptoException;
import android.media.MediaDrm;
import android.media.MediaDrmException;
import android.media.NotProvisionedException;
import android.media.UnsupportedSchemeException;
import androidx.annotation.DoNotInline;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import b.c.a.a0.d;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.q;
import b.i.a.c.v2.b;
import b.i.a.c.w2.a0;
import b.i.a.c.x0;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.google.android.exoplayer2.drm.DefaultDrmSessionManager;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
/* compiled from: FrameworkMediaDrm.java */
@RequiresApi(18)
/* loaded from: classes3.dex */
public final class c0 implements a0 {
    public static final /* synthetic */ int a = 0;

    /* renamed from: b  reason: collision with root package name */
    public final UUID f1147b;
    public final MediaDrm c;
    public int d;

    /* compiled from: FrameworkMediaDrm.java */
    @RequiresApi(31)
    /* loaded from: classes3.dex */
    public static class a {
        @DoNotInline
        public static boolean a(MediaDrm mediaDrm, String str) {
            return mediaDrm.requiresSecureDecoder(str);
        }
    }

    public c0(UUID uuid) throws UnsupportedSchemeException {
        UUID uuid2;
        Objects.requireNonNull(uuid);
        d.m(!x0.f1154b.equals(uuid), "Use C.CLEARKEY_UUID instead");
        this.f1147b = uuid;
        MediaDrm mediaDrm = new MediaDrm((e0.a >= 27 || !x0.c.equals(uuid)) ? uuid : uuid2);
        this.c = mediaDrm;
        this.d = 1;
        if (x0.d.equals(uuid) && "ASUS_Z00AD".equals(e0.d)) {
            mediaDrm.setPropertyString("securityLevel", "L3");
        }
    }

    @Override // b.i.a.c.w2.a0
    public Map<String, String> a(byte[] bArr) {
        return this.c.queryKeyStatus(bArr);
    }

    @Override // b.i.a.c.w2.a0
    public a0.d b() {
        MediaDrm.ProvisionRequest provisionRequest = this.c.getProvisionRequest();
        return new a0.d(provisionRequest.getData(), provisionRequest.getDefaultUrl());
    }

    @Override // b.i.a.c.w2.a0
    public b c(byte[] bArr) throws MediaCryptoException {
        int i = e0.a;
        boolean z2 = i < 21 && x0.d.equals(this.f1147b) && "L3".equals(this.c.getPropertyString("securityLevel"));
        UUID uuid = this.f1147b;
        if (i < 27 && x0.c.equals(uuid)) {
            uuid = x0.f1154b;
        }
        return new b0(uuid, bArr, z2);
    }

    @Override // b.i.a.c.w2.a0
    public byte[] d() throws MediaDrmException {
        return this.c.openSession();
    }

    @Override // b.i.a.c.w2.a0
    public boolean e(byte[] bArr, String str) {
        if (e0.a >= 31) {
            return a.a(this.c, str);
        }
        try {
            MediaCrypto mediaCrypto = new MediaCrypto(this.f1147b, bArr);
            try {
                return mediaCrypto.requiresSecureDecoderComponent(str);
            } finally {
                mediaCrypto.release();
            }
        } catch (MediaCryptoException unused) {
            return true;
        }
    }

    @Override // b.i.a.c.w2.a0
    public void f(byte[] bArr, byte[] bArr2) {
        this.c.restoreKeys(bArr, bArr2);
    }

    @Override // b.i.a.c.w2.a0
    public void g(byte[] bArr) {
        this.c.closeSession(bArr);
    }

    @Override // b.i.a.c.w2.a0
    public void h(@Nullable final a0.b bVar) {
        this.c.setOnEventListener(new MediaDrm.OnEventListener() { // from class: b.i.a.c.w2.o
            @Override // android.media.MediaDrm.OnEventListener
            public final void onEvent(MediaDrm mediaDrm, byte[] bArr, int i, int i2, byte[] bArr2) {
                c0 c0Var = c0.this;
                a0.b bVar2 = bVar;
                Objects.requireNonNull(c0Var);
                DefaultDrmSessionManager.c cVar = ((DefaultDrmSessionManager.b) bVar2).a.f2903x;
                Objects.requireNonNull(cVar);
                cVar.obtainMessage(i, bArr).sendToTarget();
            }
        });
    }

    @Override // b.i.a.c.w2.a0
    @Nullable
    public byte[] i(byte[] bArr, byte[] bArr2) throws NotProvisionedException, DeniedByServerException {
        if (x0.c.equals(this.f1147b) && e0.a < 27) {
            try {
                JSONObject jSONObject = new JSONObject(e0.l(bArr2));
                StringBuilder sb = new StringBuilder("{\"keys\":[");
                JSONArray jSONArray = jSONObject.getJSONArray("keys");
                for (int i = 0; i < jSONArray.length(); i++) {
                    if (i != 0) {
                        sb.append(",");
                    }
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                    sb.append("{\"k\":\"");
                    sb.append(jSONObject2.getString("k").replace('-', '+').replace('_', MentionUtilsKt.SLASH_CHAR));
                    sb.append("\",\"kid\":\"");
                    sb.append(jSONObject2.getString("kid").replace('-', '+').replace('_', MentionUtilsKt.SLASH_CHAR));
                    sb.append("\",\"kty\":\"");
                    sb.append(jSONObject2.getString("kty"));
                    sb.append("\"}");
                }
                sb.append("]}");
                bArr2 = e0.w(sb.toString());
            } catch (JSONException e) {
                String l = e0.l(bArr2);
                q.b("ClearKeyUtil", l.length() != 0 ? "Failed to adjust response data: ".concat(l) : new String("Failed to adjust response data: "), e);
            }
        }
        return this.c.provideKeyResponse(bArr, bArr2);
    }

    @Override // b.i.a.c.w2.a0
    public void j(byte[] bArr) throws DeniedByServerException {
        this.c.provideProvisionResponse(bArr);
    }

    /* JADX WARN: Code restructure failed: missing block: B:87:0x0204, code lost:
        if ("AFTT".equals(r5) == false) goto L90;
     */
    /* JADX WARN: Code restructure failed: missing block: B:89:0x020a, code lost:
        if (r1 != null) goto L91;
     */
    @Override // b.i.a.c.w2.a0
    @android.annotation.SuppressLint({"WrongConstant"})
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.i.a.c.w2.a0.a k(byte[] r17, @androidx.annotation.Nullable java.util.List<com.google.android.exoplayer2.drm.DrmInitData.SchemeData> r18, int r19, @androidx.annotation.Nullable java.util.HashMap<java.lang.String, java.lang.String> r20) throws android.media.NotProvisionedException {
        /*
            Method dump skipped, instructions count: 673
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.w2.c0.k(byte[], java.util.List, int, java.util.HashMap):b.i.a.c.w2.a0$a");
    }

    @Override // b.i.a.c.w2.a0
    public int l() {
        return 2;
    }

    @Override // b.i.a.c.w2.a0
    public synchronized void release() {
        int i = this.d - 1;
        this.d = i;
        if (i == 0) {
            this.c.release();
        }
    }
}
