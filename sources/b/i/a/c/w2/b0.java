package b.i.a.c.w2;

import b.i.a.c.f3.e0;
import b.i.a.c.v2.b;
import java.util.UUID;
/* compiled from: FrameworkCryptoConfig.java */
/* loaded from: classes3.dex */
public final class b0 implements b {
    public static final boolean a;

    /* renamed from: b  reason: collision with root package name */
    public final UUID f1146b;
    public final byte[] c;
    public final boolean d;

    static {
        boolean z2;
        if ("Amazon".equals(e0.c)) {
            String str = e0.d;
            if ("AFTM".equals(str) || "AFTB".equals(str)) {
                z2 = true;
                a = z2;
            }
        }
        z2 = false;
        a = z2;
    }

    public b0(UUID uuid, byte[] bArr, boolean z2) {
        this.f1146b = uuid;
        this.c = bArr;
        this.d = z2;
    }
}
