package b.i.a.c.w2;

import android.media.MediaDrmResetException;
import androidx.annotation.DoNotInline;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
/* compiled from: DrmUtil.java */
@RequiresApi(23)
/* loaded from: classes3.dex */
public final class x {
    @DoNotInline
    public static boolean a(@Nullable Throwable th) {
        return th instanceof MediaDrmResetException;
    }
}
