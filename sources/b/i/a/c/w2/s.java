package b.i.a.c.w2;

import android.os.Handler;
import androidx.annotation.CheckResult;
import androidx.annotation.Nullable;
import b.i.a.c.a3.a0;
import b.i.a.c.f3.e0;
import b.i.a.c.w2.s;
import java.util.Iterator;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;
/* compiled from: DrmSessionEventListener.java */
/* loaded from: classes3.dex */
public interface s {
    void J(int i, @Nullable a0.a aVar);

    void S(int i, @Nullable a0.a aVar);

    void c0(int i, @Nullable a0.a aVar, int i2);

    void d0(int i, @Nullable a0.a aVar);

    void i0(int i, @Nullable a0.a aVar);

    void u(int i, @Nullable a0.a aVar, Exception exc);

    /* compiled from: DrmSessionEventListener.java */
    /* loaded from: classes3.dex */
    public static class a {
        public final int a;
        @Nullable

        /* renamed from: b  reason: collision with root package name */
        public final a0.a f1152b;
        public final CopyOnWriteArrayList<C0100a> c;

        /* compiled from: DrmSessionEventListener.java */
        /* renamed from: b.i.a.c.w2.s$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0100a {
            public Handler a;

            /* renamed from: b  reason: collision with root package name */
            public s f1153b;

            public C0100a(Handler handler, s sVar) {
                this.a = handler;
                this.f1153b = sVar;
            }
        }

        public a() {
            this.c = new CopyOnWriteArrayList<>();
            this.a = 0;
            this.f1152b = null;
        }

        public void a() {
            Iterator<C0100a> it = this.c.iterator();
            while (it.hasNext()) {
                C0100a next = it.next();
                final s sVar = next.f1153b;
                e0.E(next.a, new Runnable() { // from class: b.i.a.c.w2.i
                    @Override // java.lang.Runnable
                    public final void run() {
                        s.a aVar = s.a.this;
                        sVar.S(aVar.a, aVar.f1152b);
                    }
                });
            }
        }

        public void b() {
            Iterator<C0100a> it = this.c.iterator();
            while (it.hasNext()) {
                C0100a next = it.next();
                final s sVar = next.f1153b;
                e0.E(next.a, new Runnable() { // from class: b.i.a.c.w2.h
                    @Override // java.lang.Runnable
                    public final void run() {
                        s.a aVar = s.a.this;
                        sVar.J(aVar.a, aVar.f1152b);
                    }
                });
            }
        }

        public void c() {
            Iterator<C0100a> it = this.c.iterator();
            while (it.hasNext()) {
                C0100a next = it.next();
                final s sVar = next.f1153b;
                e0.E(next.a, new Runnable() { // from class: b.i.a.c.w2.j
                    @Override // java.lang.Runnable
                    public final void run() {
                        s.a aVar = s.a.this;
                        sVar.i0(aVar.a, aVar.f1152b);
                    }
                });
            }
        }

        public void d(final int i) {
            Iterator<C0100a> it = this.c.iterator();
            while (it.hasNext()) {
                C0100a next = it.next();
                final s sVar = next.f1153b;
                e0.E(next.a, new Runnable() { // from class: b.i.a.c.w2.l
                    @Override // java.lang.Runnable
                    public final void run() {
                        s.a aVar = s.a.this;
                        s sVar2 = sVar;
                        int i2 = i;
                        Objects.requireNonNull(aVar);
                        Objects.requireNonNull(sVar2);
                        sVar2.c0(aVar.a, aVar.f1152b, i2);
                    }
                });
            }
        }

        public void e(final Exception exc) {
            Iterator<C0100a> it = this.c.iterator();
            while (it.hasNext()) {
                C0100a next = it.next();
                final s sVar = next.f1153b;
                e0.E(next.a, new Runnable() { // from class: b.i.a.c.w2.k
                    @Override // java.lang.Runnable
                    public final void run() {
                        s.a aVar = s.a.this;
                        sVar.u(aVar.a, aVar.f1152b, exc);
                    }
                });
            }
        }

        public void f() {
            Iterator<C0100a> it = this.c.iterator();
            while (it.hasNext()) {
                C0100a next = it.next();
                final s sVar = next.f1153b;
                e0.E(next.a, new Runnable() { // from class: b.i.a.c.w2.g
                    @Override // java.lang.Runnable
                    public final void run() {
                        s.a aVar = s.a.this;
                        sVar.d0(aVar.a, aVar.f1152b);
                    }
                });
            }
        }

        @CheckResult
        public a g(int i, @Nullable a0.a aVar) {
            return new a(this.c, i, aVar);
        }

        public a(CopyOnWriteArrayList<C0100a> copyOnWriteArrayList, int i, @Nullable a0.a aVar) {
            this.c = copyOnWriteArrayList;
            this.a = i;
            this.f1152b = aVar;
        }
    }
}
