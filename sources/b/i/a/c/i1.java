package b.i.a.c;

import java.util.HashSet;
/* compiled from: ExoPlayerLibraryInfo.java */
/* loaded from: classes3.dex */
public final class i1 {
    public static final HashSet<String> a = new HashSet<>();

    /* renamed from: b  reason: collision with root package name */
    public static String f1012b = "goog.exo.core";

    public static synchronized void a(String str) {
        synchronized (i1.class) {
            if (a.add(str)) {
                String str2 = f1012b;
                StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 2 + str.length());
                sb.append(str2);
                sb.append(", ");
                sb.append(str);
                f1012b = sb.toString();
            }
        }
    }
}
