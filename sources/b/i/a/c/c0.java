package b.i.a.c;

import b.c.a.a0.d;
import b.i.a.c.h1;
import java.util.Arrays;
import java.util.List;
/* compiled from: lambda */
/* loaded from: classes3.dex */
public final /* synthetic */ class c0 implements Runnable {
    public final /* synthetic */ f1 j;
    public final /* synthetic */ h1.d k;

    public /* synthetic */ c0(f1 f1Var, h1.d dVar) {
        this.j = f1Var;
        this.k = dVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        long j;
        boolean z2;
        long j2;
        f1 f1Var = this.j;
        h1.d dVar = this.k;
        int i = f1Var.f956x - dVar.c;
        f1Var.f956x = i;
        boolean z3 = true;
        if (dVar.d) {
            f1Var.f957y = dVar.e;
            f1Var.f958z = true;
        }
        if (dVar.f) {
            f1Var.A = dVar.g;
        }
        if (i == 0) {
            o2 o2Var = dVar.f1008b.f1142b;
            if (!f1Var.F.f1142b.q() && o2Var.q()) {
                f1Var.G = -1;
                f1Var.H = 0L;
            }
            if (!o2Var.q()) {
                List asList = Arrays.asList(((c2) o2Var).r);
                d.D(asList.size() == f1Var.m.size());
                for (int i2 = 0; i2 < asList.size(); i2++) {
                    f1Var.m.get(i2).f959b = (o2) asList.get(i2);
                }
            }
            long j3 = -9223372036854775807L;
            if (f1Var.f958z) {
                if (dVar.f1008b.c.equals(f1Var.F.c) && dVar.f1008b.e == f1Var.F.t) {
                    z3 = false;
                }
                if (z3) {
                    if (o2Var.q() || dVar.f1008b.c.a()) {
                        j2 = dVar.f1008b.e;
                    } else {
                        w1 w1Var = dVar.f1008b;
                        j2 = f1Var.m0(o2Var, w1Var.c, w1Var.e);
                    }
                    j3 = j2;
                }
                j = j3;
                z2 = z3;
            } else {
                j = -9223372036854775807L;
                z2 = false;
            }
            f1Var.f958z = false;
            f1Var.s0(dVar.f1008b, 1, f1Var.A, false, z2, f1Var.f957y, j, -1);
        }
    }
}
