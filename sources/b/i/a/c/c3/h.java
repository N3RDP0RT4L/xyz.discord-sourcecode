package b.i.a.c.c3;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import androidx.annotation.Nullable;
import b.i.a.c.a3.o0;
import b.i.a.c.c3.f;
import b.i.a.c.c3.j;
import b.i.a.c.c3.p;
import b.i.a.c.f3.e0;
import b.i.a.c.j1;
import b.i.a.c.w0;
import b.i.b.b.f0;
import b.i.b.b.g0;
import b.i.b.b.k0;
import b.i.b.b.p;
import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
/* compiled from: DefaultTrackSelector.java */
/* loaded from: classes3.dex */
public class h extends l {

    /* renamed from: b  reason: collision with root package name */
    public static final int[] f894b = new int[0];
    public static final g0<Integer> c = g0.a(b.i.a.c.c3.c.j);
    public static final g0<Integer> d = g0.a(b.i.a.c.c3.b.j);
    public final j.b e;
    public final AtomicReference<d> f;

    /* compiled from: DefaultTrackSelector.java */
    /* loaded from: classes3.dex */
    public static final class b implements Comparable<b> {
        public final boolean j;
        @Nullable
        public final String k;
        public final d l;
        public final boolean m;
        public final int n;
        public final int o;
        public final int p;
        public final int q;
        public final int r;

        /* renamed from: s  reason: collision with root package name */
        public final boolean f895s;
        public final int t;
        public final int u;
        public final int v;
        public final int w;

        public b(j1 j1Var, d dVar, int i) {
            int i2;
            int i3;
            String[] strArr;
            int i4;
            String str;
            this.l = dVar;
            this.k = h.g(j1Var.n);
            int i5 = 0;
            this.m = h.e(i, false);
            int i6 = 0;
            while (true) {
                i2 = Integer.MAX_VALUE;
                if (i6 >= dVar.w.size()) {
                    i6 = Integer.MAX_VALUE;
                    i3 = 0;
                    break;
                }
                i3 = h.c(j1Var, dVar.w.get(i6), false);
                if (i3 > 0) {
                    break;
                }
                i6++;
            }
            this.o = i6;
            this.n = i3;
            this.p = Integer.bitCount(j1Var.p & dVar.f902x);
            this.f895s = (j1Var.o & 1) != 0;
            int i7 = j1Var.J;
            this.t = i7;
            this.u = j1Var.K;
            int i8 = j1Var.f1013s;
            this.v = i8;
            this.j = (i8 == -1 || i8 <= dVar.f904z) && (i7 == -1 || i7 <= dVar.f903y);
            int i9 = e0.a;
            Configuration configuration = Resources.getSystem().getConfiguration();
            int i10 = e0.a;
            if (i10 >= 24) {
                strArr = e0.H(configuration.getLocales().toLanguageTags(), ",");
            } else {
                String[] strArr2 = new String[1];
                Locale locale = configuration.locale;
                if (i10 >= 21) {
                    str = locale.toLanguageTag();
                } else {
                    str = locale.toString();
                }
                strArr2[0] = str;
                strArr = strArr2;
            }
            for (int i11 = 0; i11 < strArr.length; i11++) {
                strArr[i11] = e0.C(strArr[i11]);
            }
            int i12 = 0;
            while (true) {
                if (i12 >= strArr.length) {
                    i12 = Integer.MAX_VALUE;
                    i4 = 0;
                    break;
                }
                i4 = h.c(j1Var, strArr[i12], false);
                if (i4 > 0) {
                    break;
                }
                i12++;
            }
            this.q = i12;
            this.r = i4;
            while (true) {
                if (i5 >= dVar.A.size()) {
                    break;
                }
                String str2 = j1Var.w;
                if (str2 != null && str2.equals(dVar.A.get(i5))) {
                    i2 = i5;
                    break;
                }
                i5++;
            }
            this.w = i2;
        }

        /* renamed from: f */
        public int compareTo(b bVar) {
            g0<Integer> g0Var;
            g0<Integer> g0Var2;
            if (!this.j || !this.m) {
                g0Var = h.c.b();
            } else {
                g0Var = h.c;
            }
            b.i.b.b.j c = b.i.b.b.j.a.c(this.m, bVar.m);
            Integer valueOf = Integer.valueOf(this.o);
            Integer valueOf2 = Integer.valueOf(bVar.o);
            k0 k0Var = k0.j;
            b.i.b.b.j b2 = c.b(valueOf, valueOf2, k0Var).a(this.n, bVar.n).a(this.p, bVar.p).c(this.j, bVar.j).b(Integer.valueOf(this.w), Integer.valueOf(bVar.w), k0Var);
            Integer valueOf3 = Integer.valueOf(this.v);
            Integer valueOf4 = Integer.valueOf(bVar.v);
            if (this.l.E) {
                g0Var2 = h.c.b();
            } else {
                g0Var2 = h.d;
            }
            b.i.b.b.j b3 = b2.b(valueOf3, valueOf4, g0Var2).c(this.f895s, bVar.f895s).b(Integer.valueOf(this.q), Integer.valueOf(bVar.q), k0Var).a(this.r, bVar.r).b(Integer.valueOf(this.t), Integer.valueOf(bVar.t), g0Var).b(Integer.valueOf(this.u), Integer.valueOf(bVar.u), g0Var);
            Integer valueOf5 = Integer.valueOf(this.v);
            Integer valueOf6 = Integer.valueOf(bVar.v);
            if (!e0.a(this.k, bVar.k)) {
                g0Var = h.d;
            }
            return b3.b(valueOf5, valueOf6, g0Var).e();
        }
    }

    /* compiled from: DefaultTrackSelector.java */
    /* loaded from: classes3.dex */
    public static final class c implements Comparable<c> {
        public final boolean j;
        public final boolean k;

        public c(j1 j1Var, int i) {
            this.j = (j1Var.o & 1) == 0 ? false : true;
            this.k = h.e(i, false);
        }

        /* renamed from: f */
        public int compareTo(c cVar) {
            return b.i.b.b.j.a.c(this.k, cVar.k).c(this.j, cVar.j).e();
        }
    }

    /* compiled from: DefaultTrackSelector.java */
    /* loaded from: classes3.dex */
    public static final class d extends p implements w0 {
        public static final d I = new e().d();
        public final int J;
        public final boolean K;
        public final boolean L;
        public final boolean M;
        public final boolean N;
        public final boolean O;
        public final boolean P;
        public final boolean Q;
        public final boolean R;
        public final boolean S;
        public final boolean T;
        public final SparseArray<Map<o0, f>> U;
        public final SparseBooleanArray V;

        public d(e eVar, a aVar) {
            super(eVar);
            this.K = eVar.f896y;
            this.L = eVar.f897z;
            this.M = eVar.A;
            this.N = eVar.B;
            this.O = eVar.C;
            this.P = eVar.D;
            this.Q = eVar.E;
            this.J = eVar.F;
            this.R = eVar.G;
            this.S = eVar.H;
            this.T = eVar.I;
            this.U = eVar.J;
            this.V = eVar.K;
        }

        /* JADX WARN: Removed duplicated region for block: B:43:0x007f  */
        /* JADX WARN: Removed duplicated region for block: B:62:0x00e7 A[LOOP:1: B:47:0x0090->B:62:0x00e7, LOOP_END] */
        /* JADX WARN: Removed duplicated region for block: B:69:0x008d A[SYNTHETIC] */
        /* JADX WARN: Removed duplicated region for block: B:75:? A[RETURN, SYNTHETIC] */
        @Override // b.i.a.c.c3.p
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public boolean equals(@androidx.annotation.Nullable java.lang.Object r11) {
            /*
                Method dump skipped, instructions count: 241
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.c3.h.d.equals(java.lang.Object):boolean");
        }

        @Override // b.i.a.c.c3.p
        public int hashCode() {
            return ((((((((((((((((((((((super.hashCode() + 31) * 31) + (this.K ? 1 : 0)) * 31) + (this.L ? 1 : 0)) * 31) + (this.M ? 1 : 0)) * 31) + (this.N ? 1 : 0)) * 31) + (this.O ? 1 : 0)) * 31) + (this.P ? 1 : 0)) * 31) + (this.Q ? 1 : 0)) * 31) + this.J) * 31) + (this.R ? 1 : 0)) * 31) + (this.S ? 1 : 0)) * 31) + (this.T ? 1 : 0);
        }
    }

    /* compiled from: DefaultTrackSelector.java */
    /* loaded from: classes3.dex */
    public static final class f implements w0 {
        public final int j;
        public final int[] k;
        public final int l;

        public boolean equals(@Nullable Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || f.class != obj.getClass()) {
                return false;
            }
            f fVar = (f) obj;
            return this.j == fVar.j && Arrays.equals(this.k, fVar.k) && this.l == fVar.l;
        }

        public int hashCode() {
            return ((Arrays.hashCode(this.k) + (this.j * 31)) * 31) + this.l;
        }
    }

    /* compiled from: DefaultTrackSelector.java */
    /* loaded from: classes3.dex */
    public static final class g implements Comparable<g> {
        public final boolean j;
        public final boolean k;
        public final boolean l;
        public final boolean m;
        public final int n;
        public final int o;
        public final int p;
        public final int q;
        public final boolean r;

        public g(j1 j1Var, d dVar, int i, @Nullable String str) {
            p<String> pVar;
            int i2;
            boolean z2 = false;
            this.k = h.e(i, false);
            int i3 = j1Var.o & (~dVar.J);
            this.l = (i3 & 1) != 0;
            this.m = (i3 & 2) != 0;
            int i4 = Integer.MAX_VALUE;
            if (dVar.B.isEmpty()) {
                pVar = p.u("");
            } else {
                pVar = dVar.B;
            }
            int i5 = 0;
            while (true) {
                if (i5 >= pVar.size()) {
                    i2 = 0;
                    break;
                }
                i2 = h.c(j1Var, pVar.get(i5), dVar.D);
                if (i2 > 0) {
                    i4 = i5;
                    break;
                }
                i5++;
            }
            this.n = i4;
            this.o = i2;
            int bitCount = Integer.bitCount(j1Var.p & dVar.C);
            this.p = bitCount;
            this.r = (j1Var.p & 1088) != 0;
            int c = h.c(j1Var, str, h.g(str) == null);
            this.q = c;
            if (i2 > 0 || ((dVar.B.isEmpty() && bitCount > 0) || this.l || (this.m && c > 0))) {
                z2 = true;
            }
            this.j = z2;
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r4v0, types: [java.util.Comparator, b.i.b.b.k0] */
        /* renamed from: f */
        public int compareTo(g gVar) {
            b.i.b.b.j c = b.i.b.b.j.a.c(this.k, gVar.k);
            Integer valueOf = Integer.valueOf(this.n);
            Integer valueOf2 = Integer.valueOf(gVar.n);
            f0 f0Var = f0.j;
            ?? r4 = k0.j;
            b.i.b.b.j c2 = c.b(valueOf, valueOf2, r4).a(this.o, gVar.o).a(this.p, gVar.p).c(this.l, gVar.l);
            Boolean valueOf3 = Boolean.valueOf(this.m);
            Boolean valueOf4 = Boolean.valueOf(gVar.m);
            if (this.o != 0) {
                f0Var = r4;
            }
            b.i.b.b.j a = c2.b(valueOf3, valueOf4, f0Var).a(this.q, gVar.q);
            if (this.p == 0) {
                a = a.d(this.r, gVar.r);
            }
            return a.e();
        }
    }

    /* compiled from: DefaultTrackSelector.java */
    /* renamed from: b.i.a.c.c3.h$h  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0097h implements Comparable<C0097h> {
        public final boolean j;
        public final d k;
        public final boolean l;
        public final boolean m;
        public final int n;
        public final int o;
        public final int p;

        /* JADX WARN: Code restructure failed: missing block: B:35:0x0053, code lost:
            if (r10 < r8.q) goto L40;
         */
        /* JADX WARN: Code restructure failed: missing block: B:39:0x005b, code lost:
            if (r10 < r8.r) goto L40;
         */
        /* JADX WARN: Removed duplicated region for block: B:34:0x004e  */
        /* JADX WARN: Removed duplicated region for block: B:38:0x0059  */
        /* JADX WARN: Removed duplicated region for block: B:43:0x006f  */
        /* JADX WARN: Removed duplicated region for block: B:49:0x0083  */
        /* JADX WARN: Removed duplicated region for block: B:57:0x0098 A[EDGE_INSN: B:57:0x0098->B:55:0x0098 ?: BREAK  , SYNTHETIC] */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public C0097h(b.i.a.c.j1 r7, b.i.a.c.c3.h.d r8, int r9, boolean r10) {
            /*
                r6 = this;
                r6.<init>()
                r6.k = r8
                r0 = 0
                r1 = 1
                r2 = -1082130432(0xffffffffbf800000, float:-1.0)
                r3 = -1
                if (r10 == 0) goto L33
                int r4 = r7.B
                if (r4 == r3) goto L14
                int r5 = r8.k
                if (r4 > r5) goto L33
            L14:
                int r4 = r7.C
                if (r4 == r3) goto L1c
                int r5 = r8.l
                if (r4 > r5) goto L33
            L1c:
                float r4 = r7.D
                int r5 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
                if (r5 == 0) goto L29
                int r5 = r8.m
                float r5 = (float) r5
                int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
                if (r4 > 0) goto L33
            L29:
                int r4 = r7.f1013s
                if (r4 == r3) goto L31
                int r5 = r8.n
                if (r4 > r5) goto L33
            L31:
                r4 = 1
                goto L34
            L33:
                r4 = 0
            L34:
                r6.j = r4
                if (r10 == 0) goto L5e
                int r10 = r7.B
                if (r10 == r3) goto L40
                int r4 = r8.o
                if (r10 < r4) goto L5e
            L40:
                int r10 = r7.C
                if (r10 == r3) goto L48
                int r4 = r8.p
                if (r10 < r4) goto L5e
            L48:
                float r10 = r7.D
                int r2 = (r10 > r2 ? 1 : (r10 == r2 ? 0 : -1))
                if (r2 == 0) goto L55
                int r2 = r8.q
                float r2 = (float) r2
                int r10 = (r10 > r2 ? 1 : (r10 == r2 ? 0 : -1))
                if (r10 < 0) goto L5e
            L55:
                int r10 = r7.f1013s
                if (r10 == r3) goto L5f
                int r2 = r8.r
                if (r10 < r2) goto L5e
                goto L5f
            L5e:
                r1 = 0
            L5f:
                r6.l = r1
                boolean r9 = b.i.a.c.c3.h.e(r9, r0)
                r6.m = r9
                int r9 = r7.f1013s
                r6.n = r9
                int r9 = r7.B
                if (r9 == r3) goto L76
                int r10 = r7.C
                if (r10 != r3) goto L74
                goto L76
            L74:
                int r3 = r9 * r10
            L76:
                r6.o = r3
                r9 = 2147483647(0x7fffffff, float:NaN)
            L7b:
                b.i.b.b.p<java.lang.String> r10 = r8.v
                int r10 = r10.size()
                if (r0 >= r10) goto L98
                java.lang.String r10 = r7.w
                if (r10 == 0) goto L95
                b.i.b.b.p<java.lang.String> r1 = r8.v
                java.lang.Object r1 = r1.get(r0)
                boolean r10 = r10.equals(r1)
                if (r10 == 0) goto L95
                r9 = r0
                goto L98
            L95:
                int r0 = r0 + 1
                goto L7b
            L98:
                r6.p = r9
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.c3.h.C0097h.<init>(b.i.a.c.j1, b.i.a.c.c3.h$d, int, boolean):void");
        }

        /* renamed from: f */
        public int compareTo(C0097h hVar) {
            Object obj;
            g0<Integer> g0Var;
            if (!this.j || !this.m) {
                obj = h.c.b();
            } else {
                obj = h.c;
            }
            b.i.b.b.j b2 = b.i.b.b.j.a.c(this.m, hVar.m).c(this.j, hVar.j).c(this.l, hVar.l).b(Integer.valueOf(this.p), Integer.valueOf(hVar.p), k0.j);
            Integer valueOf = Integer.valueOf(this.n);
            Integer valueOf2 = Integer.valueOf(hVar.n);
            if (this.k.E) {
                g0Var = h.c.b();
            } else {
                g0Var = h.d;
            }
            return b2.b(valueOf, valueOf2, g0Var).b(Integer.valueOf(this.o), Integer.valueOf(hVar.o), obj).b(Integer.valueOf(this.n), Integer.valueOf(hVar.n), obj).e();
        }
    }

    public h(Context context) {
        f.b bVar = new f.b();
        d dVar = d.I;
        d d2 = new e(context).d();
        this.e = bVar;
        this.f = new AtomicReference<>(d2);
    }

    public static int c(j1 j1Var, @Nullable String str, boolean z2) {
        if (!TextUtils.isEmpty(str) && str.equals(j1Var.n)) {
            return 4;
        }
        String g2 = g(str);
        String g3 = g(j1Var.n);
        if (g3 == null || g2 == null) {
            return (!z2 || g3 != null) ? 0 : 1;
        }
        if (g3.startsWith(g2) || g2.startsWith(g3)) {
            return 3;
        }
        int i = e0.a;
        return g3.split("-", 2)[0].equals(g2.split("-", 2)[0]) ? 2 : 0;
    }

    /* JADX WARN: Removed duplicated region for block: B:27:0x004d  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x0057  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static java.util.List<java.lang.Integer> d(b.i.a.c.a3.n0 r12, int r13, int r14, boolean r15) {
        /*
            java.util.ArrayList r0 = new java.util.ArrayList
            int r1 = r12.k
            r0.<init>(r1)
            r1 = 0
            r2 = 0
        L9:
            int r3 = r12.k
            if (r2 >= r3) goto L17
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)
            r0.add(r3)
            int r2 = r2 + 1
            goto L9
        L17:
            r2 = 2147483647(0x7fffffff, float:NaN)
            if (r13 == r2) goto Laf
            if (r14 != r2) goto L20
            goto Laf
        L20:
            r3 = 0
            r4 = 2147483647(0x7fffffff, float:NaN)
        L24:
            int r5 = r12.k
            r6 = 1
            if (r3 >= r5) goto L80
            b.i.a.c.j1[] r5 = r12.l
            r5 = r5[r3]
            int r7 = r5.B
            if (r7 <= 0) goto L7d
            int r8 = r5.C
            if (r8 <= 0) goto L7d
            if (r15 == 0) goto L45
            if (r7 <= r8) goto L3b
            r9 = 1
            goto L3c
        L3b:
            r9 = 0
        L3c:
            if (r13 <= r14) goto L3f
            goto L40
        L3f:
            r6 = 0
        L40:
            if (r9 == r6) goto L45
            r6 = r13
            r9 = r14
            goto L47
        L45:
            r9 = r13
            r6 = r14
        L47:
            int r10 = r7 * r6
            int r11 = r8 * r9
            if (r10 < r11) goto L57
            android.graphics.Point r6 = new android.graphics.Point
            int r7 = b.i.a.c.f3.e0.f(r11, r7)
            r6.<init>(r9, r7)
            goto L61
        L57:
            android.graphics.Point r7 = new android.graphics.Point
            int r8 = b.i.a.c.f3.e0.f(r10, r8)
            r7.<init>(r8, r6)
            r6 = r7
        L61:
            int r7 = r5.B
            int r5 = r5.C
            int r8 = r7 * r5
            int r9 = r6.x
            float r9 = (float) r9
            r10 = 1065017672(0x3f7ae148, float:0.98)
            float r9 = r9 * r10
            int r9 = (int) r9
            if (r7 < r9) goto L7d
            int r6 = r6.y
            float r6 = (float) r6
            float r6 = r6 * r10
            int r6 = (int) r6
            if (r5 < r6) goto L7d
            if (r8 >= r4) goto L7d
            r4 = r8
        L7d:
            int r3 = r3 + 1
            goto L24
        L80:
            if (r4 == r2) goto Laf
            int r13 = r0.size()
            int r13 = r13 - r6
        L87:
            if (r13 < 0) goto Laf
            java.lang.Object r14 = r0.get(r13)
            java.lang.Integer r14 = (java.lang.Integer) r14
            int r14 = r14.intValue()
            b.i.a.c.j1[] r15 = r12.l
            r14 = r15[r14]
            int r15 = r14.B
            r1 = -1
            if (r15 == r1) goto La4
            int r14 = r14.C
            if (r14 != r1) goto La1
            goto La4
        La1:
            int r15 = r15 * r14
            goto La5
        La4:
            r15 = -1
        La5:
            if (r15 == r1) goto La9
            if (r15 <= r4) goto Lac
        La9:
            r0.remove(r13)
        Lac:
            int r13 = r13 + (-1)
            goto L87
        Laf:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.c3.h.d(b.i.a.c.a3.n0, int, int, boolean):java.util.List");
    }

    public static boolean e(int i, boolean z2) {
        int i2 = i & 7;
        return i2 == 4 || (z2 && i2 == 3);
    }

    public static boolean f(j1 j1Var, @Nullable String str, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10) {
        int i11;
        if ((j1Var.p & 16384) != 0 || !e(i, false) || (i & i2) == 0) {
            return false;
        }
        if (str != null && !e0.a(j1Var.w, str)) {
            return false;
        }
        int i12 = j1Var.B;
        if (i12 != -1 && (i7 > i12 || i12 > i3)) {
            return false;
        }
        int i13 = j1Var.C;
        if (i13 != -1 && (i8 > i13 || i13 > i4)) {
            return false;
        }
        float f2 = j1Var.D;
        return (f2 == -1.0f || (((float) i9) <= f2 && f2 <= ((float) i5))) && (i11 = j1Var.f1013s) != -1 && i10 <= i11 && i11 <= i6;
    }

    @Nullable
    public static String g(@Nullable String str) {
        if (TextUtils.isEmpty(str) || TextUtils.equals(str, "und")) {
            return null;
        }
        return str;
    }

    /* compiled from: DefaultTrackSelector.java */
    /* loaded from: classes3.dex */
    public static final class e extends p.a {
        public boolean A;
        public boolean B;
        public boolean C;
        public boolean D;
        public boolean E;
        public int F;
        public boolean G;
        public boolean H;
        public boolean I;
        public final SparseArray<Map<o0, f>> J = new SparseArray<>();
        public final SparseBooleanArray K = new SparseBooleanArray();

        /* renamed from: y  reason: collision with root package name */
        public boolean f896y;

        /* renamed from: z  reason: collision with root package name */
        public boolean f897z;

        public e(Context context) {
            a(context);
            c(context, true);
            e();
        }

        @Override // b.i.a.c.c3.p.a
        public p.a a(Context context) {
            super.a(context);
            return this;
        }

        @Override // b.i.a.c.c3.p.a
        public p.a b(int i, int i2, boolean z2) {
            this.i = i;
            this.j = i2;
            this.k = z2;
            return this;
        }

        @Override // b.i.a.c.c3.p.a
        public p.a c(Context context, boolean z2) {
            super.c(context, z2);
            return this;
        }

        public d d() {
            return new d(this, null);
        }

        public final void e() {
            this.f896y = true;
            this.f897z = false;
            this.A = true;
            this.B = true;
            this.C = false;
            this.D = false;
            this.E = false;
            this.F = 0;
            this.G = true;
            this.H = false;
            this.I = true;
        }

        @Deprecated
        public e() {
            e();
        }
    }
}
