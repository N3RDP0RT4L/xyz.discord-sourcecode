package b.i.a.c.c3;

import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import b.i.a.c.a3.o0;
/* compiled from: MappingTrackSelector.java */
/* loaded from: classes3.dex */
public abstract class l extends q {

    /* compiled from: MappingTrackSelector.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final int[] f899b;
        public final o0[] c;
        public final int[] d;
        public final int[][][] e;
        public final o0 f;

        @VisibleForTesting
        public a(String[] strArr, int[] iArr, o0[] o0VarArr, int[] iArr2, int[][][] iArr3, o0 o0Var) {
            this.f899b = iArr;
            this.c = o0VarArr;
            this.e = iArr3;
            this.d = iArr2;
            this.f = o0Var;
            this.a = iArr.length;
        }

        public int a(int i, int i2, int i3) {
            return this.e[i][i2][i3] & 7;
        }
    }

    @Override // b.i.a.c.c3.q
    public final void a(@Nullable Object obj) {
        a aVar = (a) obj;
    }

    /* JADX WARN: Code restructure failed: missing block: B:447:0x095b, code lost:
        if (r7 != 2) goto L448;
     */
    /* JADX WARN: Removed duplicated region for block: B:110:0x028d A[LOOP:8: B:67:0x0159->B:110:0x028d, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:224:0x04bc  */
    /* JADX WARN: Removed duplicated region for block: B:482:0x09cb  */
    /* JADX WARN: Removed duplicated region for block: B:506:0x0a30  */
    /* JADX WARN: Removed duplicated region for block: B:529:0x0287 A[SYNTHETIC] */
    @Override // b.i.a.c.c3.q
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final b.i.a.c.c3.r b(b.i.a.c.g2[] r54, b.i.a.c.a3.o0 r55, b.i.a.c.a3.a0.a r56, b.i.a.c.o2 r57) throws com.google.android.exoplayer2.ExoPlaybackException {
        /*
            Method dump skipped, instructions count: 2682
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.c3.l.b(b.i.a.c.g2[], b.i.a.c.a3.o0, b.i.a.c.a3.a0$a, b.i.a.c.o2):b.i.a.c.c3.r");
    }
}
