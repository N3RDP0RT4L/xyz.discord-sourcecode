package b.i.a.c.c3;

import b.i.a.c.a3.n0;
import b.i.a.c.j1;
/* compiled from: ExoTrackSelection.java */
/* loaded from: classes3.dex */
public interface j extends m {

    /* compiled from: ExoTrackSelection.java */
    /* loaded from: classes3.dex */
    public interface b {
    }

    int b();

    void c(boolean z2);

    void e();

    void g();

    j1 h();

    void i(float f);

    void j();

    void k();

    /* compiled from: ExoTrackSelection.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public final n0 a;

        /* renamed from: b  reason: collision with root package name */
        public final int[] f898b;
        public final int c;

        public a(n0 n0Var, int... iArr) {
            this.a = n0Var;
            this.f898b = iArr;
            this.c = 0;
        }

        public a(n0 n0Var, int[] iArr, int i) {
            this.a = n0Var;
            this.f898b = iArr;
            this.c = i;
        }
    }
}
