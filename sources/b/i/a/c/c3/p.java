package b.i.a.c.c3;

import android.content.Context;
import android.graphics.Point;
import android.hardware.display.DisplayManager;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.view.accessibility.CaptioningManager;
import androidx.annotation.Nullable;
import b.i.a.c.f3.e0;
import b.i.a.c.w0;
import b.i.b.b.h0;
import b.i.b.b.j0;
import b.i.b.b.r;
import java.util.Locale;
import java.util.Objects;
/* compiled from: TrackSelectionParameters.java */
/* loaded from: classes3.dex */
public class p implements w0 {
    public static final p j = new p(new a());
    public final b.i.b.b.p<String> A;
    public final b.i.b.b.p<String> B;
    public final int C;
    public final boolean D;
    public final boolean E;
    public final boolean F;
    public final o G;
    public final r<Integer> H;
    public final int k;
    public final int l;
    public final int m;
    public final int n;
    public final int o;
    public final int p;
    public final int q;
    public final int r;

    /* renamed from: s  reason: collision with root package name */
    public final int f901s;
    public final int t;
    public final boolean u;
    public final b.i.b.b.p<String> v;
    public final b.i.b.b.p<String> w;

    /* renamed from: x  reason: collision with root package name */
    public final int f902x;

    /* renamed from: y  reason: collision with root package name */
    public final int f903y;

    /* renamed from: z  reason: collision with root package name */
    public final int f904z;

    /* compiled from: TrackSelectionParameters.java */
    /* loaded from: classes3.dex */
    public static class a {
        public int e;
        public int f;
        public int g;
        public int h;
        public b.i.b.b.p<String> l;
        public b.i.b.b.p<String> m;
        public b.i.b.b.p<String> q;
        public b.i.b.b.p<String> r;
        public int a = Integer.MAX_VALUE;

        /* renamed from: b  reason: collision with root package name */
        public int f905b = Integer.MAX_VALUE;
        public int c = Integer.MAX_VALUE;
        public int d = Integer.MAX_VALUE;
        public int i = Integer.MAX_VALUE;
        public int j = Integer.MAX_VALUE;
        public boolean k = true;
        public int n = 0;
        public int o = Integer.MAX_VALUE;
        public int p = Integer.MAX_VALUE;

        /* renamed from: s  reason: collision with root package name */
        public int f906s = 0;
        public boolean t = false;
        public boolean u = false;
        public boolean v = false;
        public o w = o.j;

        /* renamed from: x  reason: collision with root package name */
        public r<Integer> f907x = j0.m;

        @Deprecated
        public a() {
            b.i.b.b.a<Object> aVar = b.i.b.b.p.k;
            b.i.b.b.p pVar = h0.l;
            this.l = pVar;
            this.m = pVar;
            this.q = pVar;
            this.r = pVar;
            int i = r.k;
        }

        public a a(Context context) {
            CaptioningManager captioningManager;
            String str;
            int i = e0.a;
            if (i >= 19 && ((i >= 23 || Looper.myLooper() != null) && (captioningManager = (CaptioningManager) context.getSystemService("captioning")) != null && captioningManager.isEnabled())) {
                this.f906s = 1088;
                Locale locale = captioningManager.getLocale();
                if (locale != null) {
                    if (i >= 21) {
                        str = locale.toLanguageTag();
                    } else {
                        str = locale.toString();
                    }
                    this.r = b.i.b.b.p.u(str);
                }
            }
            return this;
        }

        public a b(int i, int i2, boolean z2) {
            this.i = i;
            this.j = i2;
            this.k = z2;
            return this;
        }

        public a c(Context context, boolean z2) {
            Point point;
            String str;
            String[] H;
            DisplayManager displayManager;
            int i = e0.a;
            Display display = (i < 17 || (displayManager = (DisplayManager) context.getSystemService("display")) == null) ? null : displayManager.getDisplay(0);
            if (display == null) {
                WindowManager windowManager = (WindowManager) context.getSystemService("window");
                Objects.requireNonNull(windowManager);
                display = windowManager.getDefaultDisplay();
            }
            if (display.getDisplayId() == 0 && e0.A(context)) {
                if (i < 28) {
                    str = e0.v("sys.display-size");
                } else {
                    str = e0.v("vendor.display-size");
                }
                if (!TextUtils.isEmpty(str)) {
                    try {
                        H = e0.H(str.trim(), "x");
                    } catch (NumberFormatException unused) {
                    }
                    if (H.length == 2) {
                        int parseInt = Integer.parseInt(H[0]);
                        int parseInt2 = Integer.parseInt(H[1]);
                        if (parseInt > 0 && parseInt2 > 0) {
                            point = new Point(parseInt, parseInt2);
                            return b(point.x, point.y, z2);
                        }
                    }
                    String valueOf = String.valueOf(str);
                    Log.e("Util", valueOf.length() != 0 ? "Invalid display size: ".concat(valueOf) : new String("Invalid display size: "));
                }
                if ("Sony".equals(e0.c) && e0.d.startsWith("BRAVIA") && context.getPackageManager().hasSystemFeature("com.sony.dtv.hardware.panel.qfhd")) {
                    point = new Point(3840, 2160);
                    return b(point.x, point.y, z2);
                }
            }
            point = new Point();
            int i2 = e0.a;
            if (i2 >= 23) {
                Display.Mode mode = display.getMode();
                point.x = mode.getPhysicalWidth();
                point.y = mode.getPhysicalHeight();
            } else if (i2 >= 17) {
                display.getRealSize(point);
            } else {
                display.getSize(point);
            }
            return b(point.x, point.y, z2);
        }
    }

    public p(a aVar) {
        this.k = aVar.a;
        this.l = aVar.f905b;
        this.m = aVar.c;
        this.n = aVar.d;
        this.o = aVar.e;
        this.p = aVar.f;
        this.q = aVar.g;
        this.r = aVar.h;
        this.f901s = aVar.i;
        this.t = aVar.j;
        this.u = aVar.k;
        this.v = aVar.l;
        this.w = aVar.m;
        this.f902x = aVar.n;
        this.f903y = aVar.o;
        this.f904z = aVar.p;
        this.A = aVar.q;
        this.B = aVar.r;
        this.C = aVar.f906s;
        this.D = aVar.t;
        this.E = aVar.u;
        this.F = aVar.v;
        this.G = aVar.w;
        this.H = aVar.f907x;
    }

    public boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        p pVar = (p) obj;
        return this.k == pVar.k && this.l == pVar.l && this.m == pVar.m && this.n == pVar.n && this.o == pVar.o && this.p == pVar.p && this.q == pVar.q && this.r == pVar.r && this.u == pVar.u && this.f901s == pVar.f901s && this.t == pVar.t && this.v.equals(pVar.v) && this.w.equals(pVar.w) && this.f902x == pVar.f902x && this.f903y == pVar.f903y && this.f904z == pVar.f904z && this.A.equals(pVar.A) && this.B.equals(pVar.B) && this.C == pVar.C && this.D == pVar.D && this.E == pVar.E && this.F == pVar.F && this.G.equals(pVar.G) && this.H.equals(pVar.H);
    }

    public int hashCode() {
        int hashCode = this.v.hashCode();
        int hashCode2 = this.w.hashCode();
        int hashCode3 = this.A.hashCode();
        int hashCode4 = this.B.hashCode();
        int hashCode5 = this.G.hashCode();
        return this.H.hashCode() + ((hashCode5 + ((((((((((hashCode4 + ((hashCode3 + ((((((((hashCode2 + ((hashCode + ((((((((((((((((((((((this.k + 31) * 31) + this.l) * 31) + this.m) * 31) + this.n) * 31) + this.o) * 31) + this.p) * 31) + this.q) * 31) + this.r) * 31) + (this.u ? 1 : 0)) * 31) + this.f901s) * 31) + this.t) * 31)) * 31)) * 31) + this.f902x) * 31) + this.f903y) * 31) + this.f904z) * 31)) * 31)) * 31) + this.C) * 31) + (this.D ? 1 : 0)) * 31) + (this.E ? 1 : 0)) * 31) + (this.F ? 1 : 0)) * 31)) * 31);
    }
}
