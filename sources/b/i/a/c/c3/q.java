package b.i.a.c.c3;

import androidx.annotation.Nullable;
import b.i.a.c.a3.a0;
import b.i.a.c.a3.o0;
import b.i.a.c.e3.f;
import b.i.a.c.g2;
import b.i.a.c.o2;
import com.google.android.exoplayer2.ExoPlaybackException;
/* compiled from: TrackSelector.java */
/* loaded from: classes3.dex */
public abstract class q {
    @Nullable
    public f a;

    public abstract void a(@Nullable Object obj);

    public abstract r b(g2[] g2VarArr, o0 o0Var, a0.a aVar, o2 o2Var) throws ExoPlaybackException;
}
