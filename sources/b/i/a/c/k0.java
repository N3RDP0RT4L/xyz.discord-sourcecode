package b.i.a.c;

import android.os.Bundle;
import b.i.a.c.o1;
import b.i.a.c.w0;
import java.util.Objects;
/* compiled from: lambda */
/* loaded from: classes3.dex */
public final /* synthetic */ class k0 implements w0.a {
    public static final /* synthetic */ k0 a = new k0();

    @Override // b.i.a.c.w0.a
    public final w0 a(Bundle bundle) {
        o1.g gVar;
        p1 p1Var;
        o1.e eVar;
        String string = bundle.getString(o1.a(0), "");
        Objects.requireNonNull(string);
        Bundle bundle2 = bundle.getBundle(o1.a(1));
        if (bundle2 == null) {
            gVar = o1.g.j;
        } else {
            gVar = o1.g.k.a(bundle2);
        }
        o1.g gVar2 = gVar;
        Bundle bundle3 = bundle.getBundle(o1.a(2));
        if (bundle3 == null) {
            p1Var = p1.j;
        } else {
            p1Var = p1.k.a(bundle3);
        }
        p1 p1Var2 = p1Var;
        Bundle bundle4 = bundle.getBundle(o1.a(3));
        if (bundle4 == null) {
            eVar = o1.e.p;
        } else {
            eVar = o1.d.j.a(bundle4);
        }
        return new o1(string, eVar, null, gVar2, p1Var2);
    }
}
