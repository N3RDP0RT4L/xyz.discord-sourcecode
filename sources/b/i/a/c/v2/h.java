package b.i.a.c.v2;

import androidx.annotation.CallSuper;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.b3.e;
import b.i.a.c.b3.j;
import b.i.a.c.v2.f;
import com.google.android.exoplayer2.decoder.DecoderException;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import java.util.ArrayDeque;
import java.util.Objects;
/* compiled from: SimpleDecoder.java */
/* loaded from: classes3.dex */
public abstract class h<I extends DecoderInputBuffer, O extends f, E extends DecoderException> implements d<I, O, E> {
    public final Thread a;

    /* renamed from: b  reason: collision with root package name */
    public final Object f1141b = new Object();
    public final ArrayDeque<I> c = new ArrayDeque<>();
    public final ArrayDeque<O> d = new ArrayDeque<>();
    public final I[] e;
    public final O[] f;
    public int g;
    public int h;
    @Nullable
    public I i;
    @Nullable
    public E j;
    public boolean k;
    public boolean l;
    public int m;

    /* compiled from: SimpleDecoder.java */
    /* loaded from: classes3.dex */
    public class a extends Thread {
        public a(String str) {
            super(str);
        }

        @Override // java.lang.Thread, java.lang.Runnable
        public void run() {
            h hVar = h.this;
            Objects.requireNonNull(hVar);
            do {
                try {
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }
            } while (hVar.f());
        }
    }

    public h(I[] iArr, O[] oArr) {
        this.e = iArr;
        this.g = iArr.length;
        for (int i = 0; i < this.g; i++) {
            this.e[i] = new j();
        }
        this.f = oArr;
        this.h = oArr.length;
        for (int i2 = 0; i2 < this.h; i2++) {
            this.f[i2] = new e((b.i.a.c.b3.f) this);
        }
        a aVar = new a("ExoPlayer:SimpleDecoder");
        this.a = aVar;
        aVar.start();
    }

    @Override // b.i.a.c.v2.d
    @Nullable
    public Object b() throws DecoderException {
        O removeFirst;
        synchronized (this.f1141b) {
            h();
            removeFirst = this.d.isEmpty() ? null : this.d.removeFirst();
        }
        return removeFirst;
    }

    @Override // b.i.a.c.v2.d
    @Nullable
    public Object c() throws DecoderException {
        I i;
        synchronized (this.f1141b) {
            h();
            d.D(this.i == null);
            int i2 = this.g;
            if (i2 == 0) {
                i = null;
            } else {
                I[] iArr = this.e;
                int i3 = i2 - 1;
                this.g = i3;
                i = iArr[i3];
            }
            this.i = i;
        }
        return i;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // b.i.a.c.v2.d
    public void d(Object obj) throws DecoderException {
        DecoderInputBuffer decoderInputBuffer = (DecoderInputBuffer) obj;
        synchronized (this.f1141b) {
            h();
            d.j(decoderInputBuffer == this.i);
            this.c.addLast(decoderInputBuffer);
            g();
            this.i = null;
        }
    }

    @Nullable
    public abstract E e(I i, O o, boolean z2);

    /* JADX WARN: Removed duplicated region for block: B:33:0x006b  */
    /* JADX WARN: Removed duplicated region for block: B:61:0x0078 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final boolean f() throws java.lang.InterruptedException {
        /*
            r7 = this;
            java.lang.Object r0 = r7.f1141b
            monitor-enter(r0)
        L3:
            boolean r1 = r7.l     // Catch: java.lang.Throwable -> L9e
            r2 = 0
            r3 = 1
            if (r1 != 0) goto L20
            java.util.ArrayDeque<I extends com.google.android.exoplayer2.decoder.DecoderInputBuffer> r1 = r7.c     // Catch: java.lang.Throwable -> L9e
            boolean r1 = r1.isEmpty()     // Catch: java.lang.Throwable -> L9e
            if (r1 != 0) goto L17
            int r1 = r7.h     // Catch: java.lang.Throwable -> L9e
            if (r1 <= 0) goto L17
            r1 = 1
            goto L18
        L17:
            r1 = 0
        L18:
            if (r1 != 0) goto L20
            java.lang.Object r1 = r7.f1141b     // Catch: java.lang.Throwable -> L9e
            r1.wait()     // Catch: java.lang.Throwable -> L9e
            goto L3
        L20:
            boolean r1 = r7.l     // Catch: java.lang.Throwable -> L9e
            if (r1 == 0) goto L26
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L9e
            return r2
        L26:
            java.util.ArrayDeque<I extends com.google.android.exoplayer2.decoder.DecoderInputBuffer> r1 = r7.c     // Catch: java.lang.Throwable -> L9e
            java.lang.Object r1 = r1.removeFirst()     // Catch: java.lang.Throwable -> L9e
            com.google.android.exoplayer2.decoder.DecoderInputBuffer r1 = (com.google.android.exoplayer2.decoder.DecoderInputBuffer) r1     // Catch: java.lang.Throwable -> L9e
            O extends b.i.a.c.v2.f[] r4 = r7.f     // Catch: java.lang.Throwable -> L9e
            int r5 = r7.h     // Catch: java.lang.Throwable -> L9e
            int r5 = r5 - r3
            r7.h = r5     // Catch: java.lang.Throwable -> L9e
            r4 = r4[r5]     // Catch: java.lang.Throwable -> L9e
            boolean r5 = r7.k     // Catch: java.lang.Throwable -> L9e
            r7.k = r2     // Catch: java.lang.Throwable -> L9e
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L9e
            boolean r0 = r1.n()
            if (r0 == 0) goto L47
            r0 = 4
            r4.j(r0)
            goto L75
        L47:
            boolean r0 = r1.m()
            if (r0 == 0) goto L52
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            r4.j(r0)
        L52:
            com.google.android.exoplayer2.decoder.DecoderException r0 = r7.e(r1, r4, r5)     // Catch: java.lang.OutOfMemoryError -> L57 java.lang.RuntimeException -> L60
            goto L69
        L57:
            r0 = move-exception
            com.google.android.exoplayer2.text.SubtitleDecoderException r5 = new com.google.android.exoplayer2.text.SubtitleDecoderException
            java.lang.String r6 = "Unexpected decode error"
            r5.<init>(r6, r0)
            goto L68
        L60:
            r0 = move-exception
            com.google.android.exoplayer2.text.SubtitleDecoderException r5 = new com.google.android.exoplayer2.text.SubtitleDecoderException
            java.lang.String r6 = "Unexpected decode error"
            r5.<init>(r6, r0)
        L68:
            r0 = r5
        L69:
            if (r0 == 0) goto L75
            java.lang.Object r5 = r7.f1141b
            monitor-enter(r5)
            r7.j = r0     // Catch: java.lang.Throwable -> L72
            monitor-exit(r5)     // Catch: java.lang.Throwable -> L72
            return r2
        L72:
            r0 = move-exception
            monitor-exit(r5)     // Catch: java.lang.Throwable -> L72
            throw r0
        L75:
            java.lang.Object r5 = r7.f1141b
            monitor-enter(r5)
            boolean r0 = r7.k     // Catch: java.lang.Throwable -> L9b
            if (r0 == 0) goto L80
            r4.p()     // Catch: java.lang.Throwable -> L9b
            goto L96
        L80:
            boolean r0 = r4.m()     // Catch: java.lang.Throwable -> L9b
            if (r0 == 0) goto L8f
            int r0 = r7.m     // Catch: java.lang.Throwable -> L9b
            int r0 = r0 + r3
            r7.m = r0     // Catch: java.lang.Throwable -> L9b
            r4.p()     // Catch: java.lang.Throwable -> L9b
            goto L96
        L8f:
            r7.m = r2     // Catch: java.lang.Throwable -> L9b
            java.util.ArrayDeque<O extends b.i.a.c.v2.f> r0 = r7.d     // Catch: java.lang.Throwable -> L9b
            r0.addLast(r4)     // Catch: java.lang.Throwable -> L9b
        L96:
            r7.i(r1)     // Catch: java.lang.Throwable -> L9b
            monitor-exit(r5)     // Catch: java.lang.Throwable -> L9b
            return r3
        L9b:
            r0 = move-exception
            monitor-exit(r5)     // Catch: java.lang.Throwable -> L9b
            throw r0
        L9e:
            r1 = move-exception
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L9e
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.v2.h.f():boolean");
    }

    @Override // b.i.a.c.v2.d
    public final void flush() {
        synchronized (this.f1141b) {
            this.k = true;
            this.m = 0;
            I i = this.i;
            if (i != null) {
                i(i);
                this.i = null;
            }
            while (!this.c.isEmpty()) {
                i(this.c.removeFirst());
            }
            while (!this.d.isEmpty()) {
                this.d.removeFirst().p();
            }
        }
    }

    public final void g() {
        if (!this.c.isEmpty() && this.h > 0) {
            this.f1141b.notify();
        }
    }

    public final void h() throws DecoderException {
        E e = this.j;
        if (e != null) {
            throw e;
        }
    }

    public final void i(I i) {
        i.p();
        I[] iArr = this.e;
        int i2 = this.g;
        this.g = i2 + 1;
        iArr[i2] = i;
    }

    @Override // b.i.a.c.v2.d
    @CallSuper
    public void release() {
        synchronized (this.f1141b) {
            this.l = true;
            this.f1141b.notify();
        }
        try {
            this.a.join();
        } catch (InterruptedException unused) {
            Thread.currentThread().interrupt();
        }
    }
}
