package b.i.a.c.v2;

import android.text.TextUtils;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.d.b.a.a;
import b.i.a.c.j1;
import java.util.Objects;
/* compiled from: DecoderReuseEvaluation.java */
/* loaded from: classes3.dex */
public final class g {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final j1 f1140b;
    public final j1 c;
    public final int d;
    public final int e;

    public g(String str, j1 j1Var, j1 j1Var2, int i, int i2) {
        d.j(i == 0 || i2 == 0);
        if (!TextUtils.isEmpty(str)) {
            this.a = str;
            Objects.requireNonNull(j1Var);
            this.f1140b = j1Var;
            this.c = j1Var2;
            this.d = i;
            this.e = i2;
            return;
        }
        throw new IllegalArgumentException();
    }

    public boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || g.class != obj.getClass()) {
            return false;
        }
        g gVar = (g) obj;
        return this.d == gVar.d && this.e == gVar.e && this.a.equals(gVar.a) && this.f1140b.equals(gVar.f1140b) && this.c.equals(gVar.c);
    }

    public int hashCode() {
        int m = a.m(this.a, (((this.d + 527) * 31) + this.e) * 31, 31);
        return this.c.hashCode() + ((this.f1140b.hashCode() + m) * 31);
    }
}
