package b.i.a.c;

import androidx.annotation.CheckResult;
import androidx.annotation.Nullable;
import b.i.a.c.a3.a0;
import b.i.a.c.a3.o0;
import b.i.a.c.c3.r;
import b.i.b.b.a;
import b.i.b.b.h0;
import b.i.b.b.p;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.metadata.Metadata;
import java.util.List;
/* compiled from: PlaybackInfo.java */
/* loaded from: classes3.dex */
public final class w1 {
    public static final a0.a a = new a0.a(new Object());

    /* renamed from: b  reason: collision with root package name */
    public final o2 f1142b;
    public final a0.a c;
    public final long d;
    public final long e;
    public final int f;
    @Nullable
    public final ExoPlaybackException g;
    public final boolean h;
    public final o0 i;
    public final r j;
    public final List<Metadata> k;
    public final a0.a l;
    public final boolean m;
    public final int n;
    public final x1 o;
    public final boolean p;
    public final boolean q;
    public volatile long r;

    /* renamed from: s  reason: collision with root package name */
    public volatile long f1143s;
    public volatile long t;

    public w1(o2 o2Var, a0.a aVar, long j, long j2, int i, @Nullable ExoPlaybackException exoPlaybackException, boolean z2, o0 o0Var, r rVar, List<Metadata> list, a0.a aVar2, boolean z3, int i2, x1 x1Var, long j3, long j4, long j5, boolean z4, boolean z5) {
        this.f1142b = o2Var;
        this.c = aVar;
        this.d = j;
        this.e = j2;
        this.f = i;
        this.g = exoPlaybackException;
        this.h = z2;
        this.i = o0Var;
        this.j = rVar;
        this.k = list;
        this.l = aVar2;
        this.m = z3;
        this.n = i2;
        this.o = x1Var;
        this.r = j3;
        this.f1143s = j4;
        this.t = j5;
        this.p = z4;
        this.q = z5;
    }

    public static w1 h(r rVar) {
        o2 o2Var = o2.j;
        a0.a aVar = a;
        o0 o0Var = o0.j;
        a<Object> aVar2 = p.k;
        return new w1(o2Var, aVar, -9223372036854775807L, 0L, 1, null, false, o0Var, rVar, h0.l, aVar, false, 0, x1.j, 0L, 0L, 0L, false, false);
    }

    @CheckResult
    public w1 a(a0.a aVar) {
        return new w1(this.f1142b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, aVar, this.m, this.n, this.o, this.r, this.f1143s, this.t, this.p, this.q);
    }

    @CheckResult
    public w1 b(a0.a aVar, long j, long j2, long j3, long j4, o0 o0Var, r rVar, List<Metadata> list) {
        return new w1(this.f1142b, aVar, j2, j3, this.f, this.g, this.h, o0Var, rVar, list, this.l, this.m, this.n, this.o, this.r, j4, j, this.p, this.q);
    }

    @CheckResult
    public w1 c(boolean z2) {
        return new w1(this.f1142b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o, this.r, this.f1143s, this.t, z2, this.q);
    }

    @CheckResult
    public w1 d(boolean z2, int i) {
        return new w1(this.f1142b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, z2, i, this.o, this.r, this.f1143s, this.t, this.p, this.q);
    }

    @CheckResult
    public w1 e(@Nullable ExoPlaybackException exoPlaybackException) {
        return new w1(this.f1142b, this.c, this.d, this.e, this.f, exoPlaybackException, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o, this.r, this.f1143s, this.t, this.p, this.q);
    }

    @CheckResult
    public w1 f(int i) {
        return new w1(this.f1142b, this.c, this.d, this.e, i, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o, this.r, this.f1143s, this.t, this.p, this.q);
    }

    @CheckResult
    public w1 g(o2 o2Var) {
        return new w1(o2Var, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o, this.r, this.f1143s, this.t, this.p, this.q);
    }
}
