package b.i.a.c;

import android.os.Looper;
import android.util.Log;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.f3.b0;
import b.i.a.c.f3.g;
import com.google.android.exoplayer2.ExoPlaybackException;
import java.util.concurrent.TimeoutException;
/* compiled from: PlayerMessage.java */
/* loaded from: classes3.dex */
public final class b2 {
    public final b a;

    /* renamed from: b  reason: collision with root package name */
    public final a f833b;
    public final g c;
    public final o2 d;
    public int e;
    @Nullable
    public Object f;
    public Looper g;
    public int h;
    public boolean i;
    public boolean j;
    public boolean k;

    /* compiled from: PlayerMessage.java */
    /* loaded from: classes3.dex */
    public interface a {
    }

    /* compiled from: PlayerMessage.java */
    /* loaded from: classes3.dex */
    public interface b {
        void r(int i, @Nullable Object obj) throws ExoPlaybackException;
    }

    public b2(a aVar, b bVar, o2 o2Var, int i, g gVar, Looper looper) {
        this.f833b = aVar;
        this.a = bVar;
        this.d = o2Var;
        this.g = looper;
        this.c = gVar;
        this.h = i;
    }

    public synchronized boolean a(long j) throws InterruptedException, TimeoutException {
        boolean z2;
        d.D(this.i);
        d.D(this.g.getThread() != Thread.currentThread());
        long d = this.c.d() + j;
        while (true) {
            z2 = this.k;
            if (z2 || j <= 0) {
                break;
            }
            this.c.c();
            wait(j);
            j = d - this.c.d();
        }
        if (!z2) {
            throw new TimeoutException("Message delivery timed out.");
        }
        return this.j;
    }

    public synchronized boolean b() {
        return false;
    }

    public synchronized void c(boolean z2) {
        this.j = z2 | this.j;
        this.k = true;
        notifyAll();
    }

    public b2 d() {
        d.D(!this.i);
        d.j(true);
        this.i = true;
        h1 h1Var = (h1) this.f833b;
        synchronized (h1Var) {
            if (!h1Var.I && h1Var.r.isAlive()) {
                ((b0.b) h1Var.q.i(14, this)).b();
            }
            Log.w("ExoPlayerImplInternal", "Ignoring messages sent after release.");
            c(false);
        }
        return this;
    }

    public b2 e(@Nullable Object obj) {
        d.D(!this.i);
        this.f = obj;
        return this;
    }

    public b2 f(int i) {
        d.D(!this.i);
        this.e = i;
        return this;
    }
}
