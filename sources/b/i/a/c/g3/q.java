package b.i.a.c.g3;

import androidx.annotation.Nullable;
import b.i.a.c.f3.h;
import b.i.a.c.f3.u;
import b.i.a.c.f3.x;
import com.google.android.exoplayer2.ParserException;
import java.util.Collections;
import java.util.List;
import org.objectweb.asm.Opcodes;
/* compiled from: HevcConfig.java */
/* loaded from: classes3.dex */
public final class q {
    public final List<byte[]> a;

    /* renamed from: b  reason: collision with root package name */
    public final int f985b;
    public final float c;
    @Nullable
    public final String d;

    public q(List<byte[]> list, int i, int i2, int i3, float f, @Nullable String str) {
        this.a = list;
        this.f985b = i;
        this.c = f;
        this.d = str;
    }

    public static q a(x xVar) throws ParserException {
        int i;
        int i2;
        try {
            xVar.F(21);
            int t = xVar.t() & 3;
            int t2 = xVar.t();
            int i3 = xVar.f980b;
            int i4 = 0;
            int i5 = 0;
            for (int i6 = 0; i6 < t2; i6++) {
                xVar.F(1);
                int y2 = xVar.y();
                for (int i7 = 0; i7 < y2; i7++) {
                    int y3 = xVar.y();
                    i5 += y3 + 4;
                    xVar.F(y3);
                }
            }
            xVar.E(i3);
            byte[] bArr = new byte[i5];
            String str = null;
            int i8 = 0;
            int i9 = 0;
            int i10 = -1;
            int i11 = -1;
            float f = 1.0f;
            while (i8 < t2) {
                int t3 = xVar.t() & Opcodes.LAND;
                int y4 = xVar.y();
                int i12 = 0;
                while (i12 < y4) {
                    int y5 = xVar.y();
                    byte[] bArr2 = u.a;
                    t2 = t2;
                    System.arraycopy(bArr2, i4, bArr, i9, bArr2.length);
                    int length = i9 + bArr2.length;
                    System.arraycopy(xVar.a, xVar.f980b, bArr, length, y5);
                    if (t3 == 33 && i12 == 0) {
                        u.a c = u.c(bArr, length, length + y5);
                        int i13 = c.g;
                        i11 = c.h;
                        f = c.i;
                        i2 = t3;
                        i = y4;
                        i10 = i13;
                        str = h.b(c.a, c.f975b, c.c, c.d, c.e, c.f);
                    } else {
                        i2 = t3;
                        i = y4;
                    }
                    i9 = length + y5;
                    xVar.F(y5);
                    i12++;
                    t3 = i2;
                    y4 = i;
                    i4 = 0;
                }
                i8++;
                i4 = 0;
            }
            return new q(i5 == 0 ? Collections.emptyList() : Collections.singletonList(bArr), t + 1, i10, i11, f, str);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw ParserException.a("Error parsing HEVC config", e);
        }
    }
}
