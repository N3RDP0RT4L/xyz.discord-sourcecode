package b.i.a.c.g3;

import androidx.annotation.Nullable;
import b.i.a.c.f3.h;
import b.i.a.c.f3.u;
import b.i.a.c.f3.x;
import com.google.android.exoplayer2.ParserException;
import java.util.ArrayList;
import java.util.List;
/* compiled from: AvcConfig.java */
/* loaded from: classes3.dex */
public final class m {
    public final List<byte[]> a;

    /* renamed from: b  reason: collision with root package name */
    public final int f982b;
    public final int c;
    public final int d;
    public final float e;
    @Nullable
    public final String f;

    public m(List<byte[]> list, int i, int i2, int i3, float f, @Nullable String str) {
        this.a = list;
        this.f982b = i;
        this.c = i2;
        this.d = i3;
        this.e = f;
        this.f = str;
    }

    public static byte[] a(x xVar) {
        int y2 = xVar.y();
        int i = xVar.f980b;
        xVar.F(y2);
        byte[] bArr = xVar.a;
        byte[] bArr2 = h.a;
        byte[] bArr3 = new byte[bArr2.length + y2];
        System.arraycopy(bArr2, 0, bArr3, 0, bArr2.length);
        System.arraycopy(bArr, i, bArr3, bArr2.length, y2);
        return bArr3;
    }

    public static m b(x xVar) throws ParserException {
        float f;
        int i;
        int i2;
        try {
            xVar.F(4);
            int t = (xVar.t() & 3) + 1;
            if (t != 3) {
                ArrayList arrayList = new ArrayList();
                int t2 = xVar.t() & 31;
                for (int i3 = 0; i3 < t2; i3++) {
                    arrayList.add(a(xVar));
                }
                int t3 = xVar.t();
                for (int i4 = 0; i4 < t3; i4++) {
                    arrayList.add(a(xVar));
                }
                String str = null;
                if (t2 > 0) {
                    u.c e = u.e((byte[]) arrayList.get(0), t, ((byte[]) arrayList.get(0)).length);
                    int i5 = e.e;
                    int i6 = e.f;
                    float f2 = e.g;
                    str = h.a(e.a, e.f977b, e.c);
                    i2 = i5;
                    i = i6;
                    f = f2;
                } else {
                    i2 = -1;
                    i = -1;
                    f = 1.0f;
                }
                return new m(arrayList, t, i2, i, f, str);
            }
            throw new IllegalStateException();
        } catch (ArrayIndexOutOfBoundsException e2) {
            throw ParserException.a("Error parsing AVC config", e2);
        }
    }
}
