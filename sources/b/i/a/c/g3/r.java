package b.i.a.c.g3;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.util.Pair;
import android.view.Surface;
import androidx.annotation.CallSuper;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import b.c.a.a0.d;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.q;
import b.i.a.c.g3.v;
import b.i.a.c.g3.x;
import b.i.a.c.h2;
import b.i.a.c.j1;
import b.i.a.c.k1;
import b.i.a.c.v2.e;
import b.i.a.c.y2.g;
import b.i.a.c.y2.t;
import b.i.a.c.y2.u;
import b.i.a.c.y2.v;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.mediacodec.MediaCodecDecoderException;
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil;
import com.google.android.exoplayer2.video.DummySurface;
import com.google.android.exoplayer2.video.MediaCodecVideoDecoderException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;
/* compiled from: MediaCodecVideoRenderer.java */
/* loaded from: classes3.dex */
public class r extends MediaCodecRenderer {
    public static final int[] N0 = {1920, 1600, 1440, 1280, 960, 854, 640, 540, 480};
    public static boolean O0;
    public static boolean P0;
    public final Context Q0;
    public final v R0;
    public final x.a S0;
    public final long T0;
    public final int U0;
    public a W0;
    public boolean X0;
    public boolean Y0;
    @Nullable
    public Surface Z0;
    @Nullable
    public DummySurface a1;
    public boolean b1;
    public boolean d1;
    public boolean e1;
    public boolean f1;
    public long g1;
    public long i1;
    public int j1;
    public int k1;
    public int l1;
    public long m1;
    public long n1;
    public long o1;
    public int p1;
    public int s1;
    public boolean v1;
    @Nullable
    public b x1;
    @Nullable
    public u y1;
    public final boolean V0 = "NVIDIA".equals(e0.c);
    public long h1 = -9223372036854775807L;
    public int q1 = -1;
    public int r1 = -1;
    public float t1 = -1.0f;
    public int c1 = 1;
    public int w1 = 0;
    @Nullable
    public y u1 = null;

    /* compiled from: MediaCodecVideoRenderer.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final int f986b;
        public final int c;

        public a(int i, int i2, int i3) {
            this.a = i;
            this.f986b = i2;
            this.c = i3;
        }
    }

    /* compiled from: MediaCodecVideoRenderer.java */
    @RequiresApi(23)
    /* loaded from: classes3.dex */
    public final class b implements t.c, Handler.Callback {
        public final Handler j;

        public b(t tVar) {
            int i = e0.a;
            Looper myLooper = Looper.myLooper();
            d.H(myLooper);
            Handler handler = new Handler(myLooper, this);
            this.j = handler;
            tVar.f(this, handler);
        }

        public final void a(long j) {
            r rVar = r.this;
            if (this == rVar.x1) {
                if (j == RecyclerView.FOREVER_NS) {
                    rVar.H0 = true;
                    return;
                }
                try {
                    rVar.N0(j);
                } catch (ExoPlaybackException e) {
                    r.this.I0 = e;
                }
            }
        }

        public void b(t tVar, long j, long j2) {
            if (e0.a < 30) {
                this.j.sendMessageAtFrontOfQueue(Message.obtain(this.j, 0, (int) (j >> 32), (int) j));
                return;
            }
            a(j);
        }

        @Override // android.os.Handler.Callback
        public boolean handleMessage(Message message) {
            if (message.what != 0) {
                return false;
            }
            a((e0.L(message.arg1) << 32) | e0.L(message.arg2));
            return true;
        }
    }

    public r(Context context, t.b bVar, v vVar, long j, boolean z2, @Nullable Handler handler, @Nullable x xVar, int i) {
        super(2, bVar, vVar, z2, 30.0f);
        this.T0 = j;
        this.U0 = i;
        Context applicationContext = context.getApplicationContext();
        this.Q0 = applicationContext;
        this.R0 = new v(applicationContext);
        this.S0 = new x.a(handler, xVar);
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Code restructure failed: missing block: B:440:0x07cf, code lost:
        if (r0.equals("NX573J") == false) goto L39;
     */
    /* JADX WARN: Code restructure failed: missing block: B:464:0x0831, code lost:
        if (r0.equals("AFTN") == false) goto L462;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static boolean E0() {
        /*
            Method dump skipped, instructions count: 3046
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.g3.r.E0():boolean");
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    public static int F0(u uVar, j1 j1Var) {
        char c;
        int i;
        int intValue;
        int i2 = j1Var.B;
        int i3 = j1Var.C;
        if (i2 == -1 || i3 == -1) {
            return -1;
        }
        String str = j1Var.w;
        if ("video/dolby-vision".equals(str)) {
            Pair<Integer, Integer> c2 = MediaCodecUtil.c(j1Var);
            str = (c2 == null || !((intValue = ((Integer) c2.first).intValue()) == 512 || intValue == 1 || intValue == 2)) ? "video/hevc" : "video/avc";
        }
        str.hashCode();
        int i4 = 4;
        switch (str.hashCode()) {
            case -1664118616:
                if (str.equals("video/3gpp")) {
                    c = 0;
                    break;
                }
                c = 65535;
                break;
            case -1662541442:
                if (str.equals("video/hevc")) {
                    c = 1;
                    break;
                }
                c = 65535;
                break;
            case 1187890754:
                if (str.equals("video/mp4v-es")) {
                    c = 2;
                    break;
                }
                c = 65535;
                break;
            case 1331836730:
                if (str.equals("video/avc")) {
                    c = 3;
                    break;
                }
                c = 65535;
                break;
            case 1599127256:
                if (str.equals("video/x-vnd.on2.vp8")) {
                    c = 4;
                    break;
                }
                c = 65535;
                break;
            case 1599127257:
                if (str.equals("video/x-vnd.on2.vp9")) {
                    c = 5;
                    break;
                }
                c = 65535;
                break;
            default:
                c = 65535;
                break;
        }
        if (c != 0) {
            if (c != 1) {
                if (c != 2) {
                    if (c == 3) {
                        String str2 = e0.d;
                        if ("BRAVIA 4K 2015".equals(str2) || ("Amazon".equals(e0.c) && ("KFSOWI".equals(str2) || ("AFTS".equals(str2) && uVar.f)))) {
                            return -1;
                        }
                        i = e0.f(i3, 16) * e0.f(i2, 16) * 16 * 16;
                        i4 = 2;
                        return (i * 3) / (i4 * 2);
                    } else if (c != 4) {
                        if (c != 5) {
                            return -1;
                        }
                    }
                }
            }
            i = i2 * i3;
            return (i * 3) / (i4 * 2);
        }
        i = i2 * i3;
        i4 = 2;
        return (i * 3) / (i4 * 2);
    }

    public static List<u> G0(v vVar, j1 j1Var, boolean z2, boolean z3) throws MediaCodecUtil.DecoderQueryException {
        Pair<Integer, Integer> c;
        String str = j1Var.w;
        if (str == null) {
            return Collections.emptyList();
        }
        List<u> a2 = vVar.a(str, z2, z3);
        Pattern pattern = MediaCodecUtil.a;
        ArrayList arrayList = new ArrayList(a2);
        MediaCodecUtil.j(arrayList, new g(j1Var));
        if ("video/dolby-vision".equals(str) && (c = MediaCodecUtil.c(j1Var)) != null) {
            int intValue = ((Integer) c.first).intValue();
            if (intValue == 16 || intValue == 256) {
                arrayList.addAll(vVar.a("video/hevc", z2, z3));
            } else if (intValue == 512) {
                arrayList.addAll(vVar.a("video/avc", z2, z3));
            }
        }
        return Collections.unmodifiableList(arrayList);
    }

    public static int H0(u uVar, j1 j1Var) {
        if (j1Var.f1014x == -1) {
            return F0(uVar, j1Var);
        }
        int size = j1Var.f1015y.size();
        int i = 0;
        for (int i2 = 0; i2 < size; i2++) {
            i += j1Var.f1015y.get(i2).length;
        }
        return j1Var.f1014x + i;
    }

    public static boolean I0(long j) {
        return j < -30000;
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer, b.i.a.c.v0
    public void B() {
        this.u1 = null;
        C0();
        this.b1 = false;
        v vVar = this.R0;
        v.b bVar = vVar.f989b;
        if (bVar != null) {
            bVar.unregister();
            v.e eVar = vVar.c;
            Objects.requireNonNull(eVar);
            eVar.l.sendEmptyMessage(2);
        }
        this.x1 = null;
        try {
            super.B();
            final x.a aVar = this.S0;
            final e eVar2 = this.J0;
            Objects.requireNonNull(aVar);
            synchronized (eVar2) {
            }
            Handler handler = aVar.a;
            if (handler != null) {
                handler.post(new Runnable() { // from class: b.i.a.c.g3.c
                    @Override // java.lang.Runnable
                    public final void run() {
                        x.a aVar2 = x.a.this;
                        e eVar3 = eVar2;
                        Objects.requireNonNull(aVar2);
                        synchronized (eVar3) {
                        }
                        x xVar = aVar2.f991b;
                        int i = e0.a;
                        xVar.Z(eVar3);
                    }
                });
            }
        } catch (Throwable th) {
            final x.a aVar2 = this.S0;
            final e eVar3 = this.J0;
            Objects.requireNonNull(aVar2);
            synchronized (eVar3) {
                Handler handler2 = aVar2.a;
                if (handler2 != null) {
                    handler2.post(new Runnable() { // from class: b.i.a.c.g3.c
                        @Override // java.lang.Runnable
                        public final void run() {
                            x.a aVar22 = x.a.this;
                            e eVar32 = eVar3;
                            Objects.requireNonNull(aVar22);
                            synchronized (eVar32) {
                            }
                            x xVar = aVar22.f991b;
                            int i = e0.a;
                            xVar.Z(eVar32);
                        }
                    });
                }
                throw th;
            }
        }
    }

    @Override // b.i.a.c.v0
    public void C(boolean z2, boolean z3) throws ExoPlaybackException {
        this.J0 = new e();
        h2 h2Var = this.l;
        Objects.requireNonNull(h2Var);
        boolean z4 = h2Var.f1011b;
        d.D(!z4 || this.w1 != 0);
        if (this.v1 != z4) {
            this.v1 = z4;
            o0();
        }
        final x.a aVar = this.S0;
        final e eVar = this.J0;
        Handler handler = aVar.a;
        if (handler != null) {
            handler.post(new Runnable() { // from class: b.i.a.c.g3.f
                @Override // java.lang.Runnable
                public final void run() {
                    x.a aVar2 = x.a.this;
                    e eVar2 = eVar;
                    x xVar = aVar2.f991b;
                    int i = e0.a;
                    xVar.P(eVar2);
                }
            });
        }
        v vVar = this.R0;
        if (vVar.f989b != null) {
            v.e eVar2 = vVar.c;
            Objects.requireNonNull(eVar2);
            eVar2.l.sendEmptyMessage(1);
            vVar.f989b.a(new b.i.a.c.g3.b(vVar));
        }
        this.e1 = z3;
        this.f1 = false;
    }

    public final void C0() {
        t tVar;
        this.d1 = false;
        if (e0.a >= 23 && this.v1 && (tVar = this.S) != null) {
            this.x1 = new b(tVar);
        }
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer, b.i.a.c.v0
    public void D(long j, boolean z2) throws ExoPlaybackException {
        super.D(j, z2);
        C0();
        this.R0.b();
        this.m1 = -9223372036854775807L;
        this.g1 = -9223372036854775807L;
        this.k1 = 0;
        if (z2) {
            R0();
        } else {
            this.h1 = -9223372036854775807L;
        }
    }

    public boolean D0(String str) {
        if (str.startsWith("OMX.google")) {
            return false;
        }
        synchronized (r.class) {
            if (!O0) {
                P0 = E0();
                O0 = true;
            }
        }
        return P0;
    }

    @Override // b.i.a.c.v0
    @TargetApi(17)
    public void E() {
        try {
            M();
            o0();
            u0(null);
        } finally {
            if (this.a1 != null) {
                O0();
            }
        }
    }

    @Override // b.i.a.c.v0
    public void F() {
        this.j1 = 0;
        this.i1 = SystemClock.elapsedRealtime();
        this.n1 = SystemClock.elapsedRealtime() * 1000;
        this.o1 = 0L;
        this.p1 = 0;
        v vVar = this.R0;
        vVar.d = true;
        vVar.b();
        vVar.d(false);
    }

    @Override // b.i.a.c.v0
    public void G() {
        this.h1 = -9223372036854775807L;
        J0();
        final int i = this.p1;
        if (i != 0) {
            final x.a aVar = this.S0;
            final long j = this.o1;
            Handler handler = aVar.a;
            if (handler != null) {
                handler.post(new Runnable() { // from class: b.i.a.c.g3.l
                    @Override // java.lang.Runnable
                    public final void run() {
                        x.a aVar2 = x.a.this;
                        long j2 = j;
                        int i2 = i;
                        x xVar = aVar2.f991b;
                        int i3 = e0.a;
                        xVar.h0(j2, i2);
                    }
                });
            }
            this.o1 = 0L;
            this.p1 = 0;
        }
        v vVar = this.R0;
        vVar.d = false;
        vVar.a();
    }

    public final void J0() {
        if (this.j1 > 0) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            final long j = elapsedRealtime - this.i1;
            final x.a aVar = this.S0;
            final int i = this.j1;
            Handler handler = aVar.a;
            if (handler != null) {
                handler.post(new Runnable() { // from class: b.i.a.c.g3.h
                    @Override // java.lang.Runnable
                    public final void run() {
                        x.a aVar2 = x.a.this;
                        int i2 = i;
                        long j2 = j;
                        x xVar = aVar2.f991b;
                        int i3 = e0.a;
                        xVar.F(i2, j2);
                    }
                });
            }
            this.j1 = 0;
            this.i1 = elapsedRealtime;
        }
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    public b.i.a.c.v2.g K(u uVar, j1 j1Var, j1 j1Var2) {
        b.i.a.c.v2.g c = uVar.c(j1Var, j1Var2);
        int i = c.e;
        int i2 = j1Var2.B;
        a aVar = this.W0;
        if (i2 > aVar.a || j1Var2.C > aVar.f986b) {
            i |= 256;
        }
        if (H0(uVar, j1Var2) > this.W0.c) {
            i |= 64;
        }
        int i3 = i;
        return new b.i.a.c.v2.g(uVar.a, j1Var, j1Var2, i3 != 0 ? 0 : c.d, i3);
    }

    public void K0() {
        this.f1 = true;
        if (!this.d1) {
            this.d1 = true;
            x.a aVar = this.S0;
            Surface surface = this.Z0;
            if (aVar.a != null) {
                aVar.a.post(new g(aVar, surface, SystemClock.elapsedRealtime()));
            }
            this.b1 = true;
        }
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    public MediaCodecDecoderException L(Throwable th, @Nullable u uVar) {
        return new MediaCodecVideoDecoderException(th, uVar, this.Z0);
    }

    public final void L0() {
        int i = this.q1;
        if (i != -1 || this.r1 != -1) {
            y yVar = this.u1;
            if (yVar == null || yVar.k != i || yVar.l != this.r1 || yVar.m != this.s1 || yVar.n != this.t1) {
                y yVar2 = new y(i, this.r1, this.s1, this.t1);
                this.u1 = yVar2;
                x.a aVar = this.S0;
                Handler handler = aVar.a;
                if (handler != null) {
                    handler.post(new j(aVar, yVar2));
                }
            }
        }
    }

    public final void M0(long j, long j2, j1 j1Var) {
        u uVar = this.y1;
        if (uVar != null) {
            uVar.e(j, j2, j1Var, this.U);
        }
    }

    public void N0(long j) throws ExoPlaybackException {
        B0(j);
        L0();
        this.J0.e++;
        K0();
        super.i0(j);
        if (!this.v1) {
            this.l1--;
        }
    }

    @RequiresApi(17)
    public final void O0() {
        Surface surface = this.Z0;
        DummySurface dummySurface = this.a1;
        if (surface == dummySurface) {
            this.Z0 = null;
        }
        dummySurface.release();
        this.a1 = null;
    }

    public void P0(t tVar, int i) {
        L0();
        d.f("releaseOutputBuffer");
        tVar.releaseOutputBuffer(i, true);
        d.d0();
        this.n1 = SystemClock.elapsedRealtime() * 1000;
        this.J0.e++;
        this.k1 = 0;
        K0();
    }

    @RequiresApi(21)
    public void Q0(t tVar, int i, long j) {
        L0();
        d.f("releaseOutputBuffer");
        tVar.c(i, j);
        d.d0();
        this.n1 = SystemClock.elapsedRealtime() * 1000;
        this.J0.e++;
        this.k1 = 0;
        K0();
    }

    public final void R0() {
        this.h1 = this.T0 > 0 ? SystemClock.elapsedRealtime() + this.T0 : -9223372036854775807L;
    }

    public final boolean S0(u uVar) {
        return e0.a >= 23 && !this.v1 && !D0(uVar.a) && (!uVar.f || DummySurface.b(this.Q0));
    }

    public void T0(t tVar, int i) {
        d.f("skipVideoBuffer");
        tVar.releaseOutputBuffer(i, false);
        d.d0();
        this.J0.f++;
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    public boolean U() {
        return this.v1 && e0.a < 23;
    }

    public void U0(int i) {
        e eVar = this.J0;
        eVar.g += i;
        this.j1 += i;
        int i2 = this.k1 + i;
        this.k1 = i2;
        eVar.h = Math.max(i2, eVar.h);
        int i3 = this.U0;
        if (i3 > 0 && this.j1 >= i3) {
            J0();
        }
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    public float V(float f, j1 j1Var, j1[] j1VarArr) {
        float f2 = -1.0f;
        for (j1 j1Var2 : j1VarArr) {
            float f3 = j1Var2.D;
            if (f3 != -1.0f) {
                f2 = Math.max(f2, f3);
            }
        }
        if (f2 == -1.0f) {
            return -1.0f;
        }
        return f2 * f;
    }

    public void V0(long j) {
        e eVar = this.J0;
        eVar.j += j;
        eVar.k++;
        this.o1 += j;
        this.p1++;
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    public List<u> W(b.i.a.c.y2.v vVar, j1 j1Var, boolean z2) throws MediaCodecUtil.DecoderQueryException {
        return G0(vVar, j1Var, z2, this.v1);
    }

    /* JADX WARN: Code restructure failed: missing block: B:66:0x0114, code lost:
        if (r12 == false) goto L68;
     */
    /* JADX WARN: Code restructure failed: missing block: B:67:0x0116, code lost:
        r11 = r4;
     */
    /* JADX WARN: Code restructure failed: missing block: B:68:0x0118, code lost:
        r11 = r5;
     */
    /* JADX WARN: Code restructure failed: missing block: B:69:0x0119, code lost:
        if (r12 == false) goto L71;
     */
    /* JADX WARN: Code restructure failed: missing block: B:71:0x011c, code lost:
        r5 = r4;
     */
    /* JADX WARN: Code restructure failed: missing block: B:72:0x011d, code lost:
        r1 = new android.graphics.Point(r11, r5);
     */
    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    @android.annotation.TargetApi(17)
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.i.a.c.y2.t.a Y(b.i.a.c.y2.u r22, b.i.a.c.j1 r23, @androidx.annotation.Nullable android.media.MediaCrypto r24, float r25) {
        /*
            Method dump skipped, instructions count: 611
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.g3.r.Y(b.i.a.c.y2.u, b.i.a.c.j1, android.media.MediaCrypto, float):b.i.a.c.y2.t$a");
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    @TargetApi(29)
    public void Z(DecoderInputBuffer decoderInputBuffer) throws ExoPlaybackException {
        if (this.Y0) {
            ByteBuffer byteBuffer = decoderInputBuffer.o;
            Objects.requireNonNull(byteBuffer);
            if (byteBuffer.remaining() >= 7) {
                byte b2 = byteBuffer.get();
                short s2 = byteBuffer.getShort();
                short s3 = byteBuffer.getShort();
                byte b3 = byteBuffer.get();
                byte b4 = byteBuffer.get();
                byteBuffer.position(0);
                if (b2 == -75 && s2 == 60 && s3 == 1 && b3 == 4 && b4 == 0) {
                    byte[] bArr = new byte[byteBuffer.remaining()];
                    byteBuffer.get(bArr);
                    byteBuffer.position(0);
                    t tVar = this.S;
                    Bundle bundle = new Bundle();
                    bundle.putByteArray("hdr10-plus-info", bArr);
                    tVar.setParameters(bundle);
                }
            }
        }
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer, b.i.a.c.f2
    public boolean d() {
        DummySurface dummySurface;
        if (super.d() && (this.d1 || (((dummySurface = this.a1) != null && this.Z0 == dummySurface) || this.S == null || this.v1))) {
            this.h1 = -9223372036854775807L;
            return true;
        } else if (this.h1 == -9223372036854775807L) {
            return false;
        } else {
            if (SystemClock.elapsedRealtime() < this.h1) {
                return true;
            }
            this.h1 = -9223372036854775807L;
            return false;
        }
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    public void d0(final Exception exc) {
        q.b("MediaCodecVideoRenderer", "Video codec error", exc);
        final x.a aVar = this.S0;
        Handler handler = aVar.a;
        if (handler != null) {
            handler.post(new Runnable() { // from class: b.i.a.c.g3.e
                @Override // java.lang.Runnable
                public final void run() {
                    x.a aVar2 = x.a.this;
                    Exception exc2 = exc;
                    x xVar = aVar2.f991b;
                    int i = e0.a;
                    xVar.V(exc2);
                }
            });
        }
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    public void e0(final String str, final long j, final long j2) {
        final x.a aVar = this.S0;
        Handler handler = aVar.a;
        if (handler != null) {
            handler.post(new Runnable() { // from class: b.i.a.c.g3.i
                @Override // java.lang.Runnable
                public final void run() {
                    x.a aVar2 = x.a.this;
                    String str2 = str;
                    long j3 = j;
                    long j4 = j2;
                    x xVar = aVar2.f991b;
                    int i = e0.a;
                    xVar.n(str2, j3, j4);
                }
            });
        }
        this.X0 = D0(str);
        u uVar = this.Z;
        Objects.requireNonNull(uVar);
        boolean z2 = false;
        if (e0.a >= 29 && "video/x-vnd.on2.vp9".equals(uVar.f1311b)) {
            MediaCodecInfo.CodecProfileLevel[] d = uVar.d();
            int length = d.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                } else if (d[i].profile == 16384) {
                    z2 = true;
                    break;
                } else {
                    i++;
                }
            }
        }
        this.Y0 = z2;
        if (e0.a >= 23 && this.v1) {
            t tVar = this.S;
            Objects.requireNonNull(tVar);
            this.x1 = new b(tVar);
        }
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    public void f0(final String str) {
        final x.a aVar = this.S0;
        Handler handler = aVar.a;
        if (handler != null) {
            handler.post(new Runnable() { // from class: b.i.a.c.g3.d
                @Override // java.lang.Runnable
                public final void run() {
                    x.a aVar2 = x.a.this;
                    String str2 = str;
                    x xVar = aVar2.f991b;
                    int i = e0.a;
                    xVar.l(str2);
                }
            });
        }
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    @Nullable
    public b.i.a.c.v2.g g0(k1 k1Var) throws ExoPlaybackException {
        final b.i.a.c.v2.g g02 = super.g0(k1Var);
        final x.a aVar = this.S0;
        final j1 j1Var = k1Var.f1023b;
        Handler handler = aVar.a;
        if (handler != null) {
            handler.post(new Runnable() { // from class: b.i.a.c.g3.k
                @Override // java.lang.Runnable
                public final void run() {
                    x.a aVar2 = x.a.this;
                    j1 j1Var2 = j1Var;
                    b.i.a.c.v2.g gVar = g02;
                    x xVar = aVar2.f991b;
                    int i = e0.a;
                    xVar.O(j1Var2);
                    aVar2.f991b.Q(j1Var2, gVar);
                }
            });
        }
        return g02;
    }

    @Override // b.i.a.c.f2, b.i.a.c.g2
    public String getName() {
        return "MediaCodecVideoRenderer";
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    public void h0(j1 j1Var, @Nullable MediaFormat mediaFormat) {
        int i;
        int i2;
        t tVar = this.S;
        if (tVar != null) {
            tVar.g(this.c1);
        }
        if (this.v1) {
            this.q1 = j1Var.B;
            this.r1 = j1Var.C;
        } else {
            Objects.requireNonNull(mediaFormat);
            boolean z2 = mediaFormat.containsKey("crop-right") && mediaFormat.containsKey("crop-left") && mediaFormat.containsKey("crop-bottom") && mediaFormat.containsKey("crop-top");
            if (z2) {
                i = (mediaFormat.getInteger("crop-right") - mediaFormat.getInteger("crop-left")) + 1;
            } else {
                i = mediaFormat.getInteger("width");
            }
            this.q1 = i;
            if (z2) {
                i2 = (mediaFormat.getInteger("crop-bottom") - mediaFormat.getInteger("crop-top")) + 1;
            } else {
                i2 = mediaFormat.getInteger("height");
            }
            this.r1 = i2;
        }
        float f = j1Var.F;
        this.t1 = f;
        if (e0.a >= 21) {
            int i3 = j1Var.E;
            if (i3 == 90 || i3 == 270) {
                int i4 = this.q1;
                this.q1 = this.r1;
                this.r1 = i4;
                this.t1 = 1.0f / f;
            }
        } else {
            this.s1 = j1Var.E;
        }
        v vVar = this.R0;
        vVar.f = j1Var.D;
        p pVar = vVar.a;
        pVar.a.c();
        pVar.f983b.c();
        pVar.c = false;
        pVar.d = -9223372036854775807L;
        pVar.e = 0;
        vVar.c();
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    @CallSuper
    public void i0(long j) {
        super.i0(j);
        if (!this.v1) {
            this.l1--;
        }
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    public void j0() {
        C0();
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    @CallSuper
    public void k0(DecoderInputBuffer decoderInputBuffer) throws ExoPlaybackException {
        boolean z2 = this.v1;
        if (!z2) {
            this.l1++;
        }
        if (e0.a < 23 && z2) {
            N0(decoderInputBuffer.n);
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:21:0x0075, code lost:
        if ((r11 == 0 ? false : r13.g[(int) ((r11 - 1) % 15)]) != false) goto L22;
     */
    /* JADX WARN: Code restructure failed: missing block: B:74:0x012f, code lost:
        if ((I0(r5) && r22 > 100000) != false) goto L75;
     */
    /* JADX WARN: Removed duplicated region for block: B:79:0x0138  */
    /* JADX WARN: Removed duplicated region for block: B:85:0x0157  */
    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public boolean m0(long r28, long r30, @androidx.annotation.Nullable b.i.a.c.y2.t r32, @androidx.annotation.Nullable java.nio.ByteBuffer r33, int r34, int r35, int r36, long r37, boolean r39, boolean r40, b.i.a.c.j1 r41) throws com.google.android.exoplayer2.ExoPlaybackException {
        /*
            Method dump skipped, instructions count: 749
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.g3.r.m0(long, long, b.i.a.c.y2.t, java.nio.ByteBuffer, int, int, int, long, boolean, boolean, b.i.a.c.j1):boolean");
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer, b.i.a.c.v0, b.i.a.c.f2
    public void n(float f, float f2) throws ExoPlaybackException {
        this.Q = f;
        this.R = f2;
        z0(this.T);
        v vVar = this.R0;
        vVar.i = f;
        vVar.b();
        vVar.d(false);
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    @CallSuper
    public void q0() {
        super.q0();
        this.l1 = 0;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r6v11, types: [android.view.Surface] */
    @Override // b.i.a.c.v0, b.i.a.c.b2.b
    public void r(int i, @Nullable Object obj) throws ExoPlaybackException {
        x.a aVar;
        Handler handler;
        x.a aVar2;
        Handler handler2;
        if (i == 1) {
            DummySurface dummySurface = obj instanceof Surface ? (Surface) obj : null;
            if (dummySurface == null) {
                DummySurface dummySurface2 = this.a1;
                if (dummySurface2 != null) {
                    dummySurface = dummySurface2;
                } else {
                    u uVar = this.Z;
                    if (uVar != null && S0(uVar)) {
                        dummySurface = DummySurface.c(this.Q0, uVar.f);
                        this.a1 = dummySurface;
                    }
                }
            }
            if (this.Z0 != dummySurface) {
                this.Z0 = dummySurface;
                v vVar = this.R0;
                Objects.requireNonNull(vVar);
                DummySurface dummySurface3 = dummySurface instanceof DummySurface ? null : dummySurface;
                if (vVar.e != dummySurface3) {
                    vVar.a();
                    vVar.e = dummySurface3;
                    vVar.d(true);
                }
                this.b1 = false;
                int i2 = this.n;
                t tVar = this.S;
                if (tVar != null) {
                    if (e0.a < 23 || dummySurface == null || this.X0) {
                        o0();
                        b0();
                    } else {
                        tVar.i(dummySurface);
                    }
                }
                if (dummySurface == null || dummySurface == this.a1) {
                    this.u1 = null;
                    C0();
                    return;
                }
                y yVar = this.u1;
                if (!(yVar == null || (handler2 = (aVar2 = this.S0).a) == null)) {
                    handler2.post(new j(aVar2, yVar));
                }
                C0();
                if (i2 == 2) {
                    R0();
                }
            } else if (dummySurface != null && dummySurface != this.a1) {
                y yVar2 = this.u1;
                if (!(yVar2 == null || (handler = (aVar = this.S0).a) == null)) {
                    handler.post(new j(aVar, yVar2));
                }
                if (this.b1) {
                    x.a aVar3 = this.S0;
                    Surface surface = this.Z0;
                    if (aVar3.a != null) {
                        aVar3.a.post(new g(aVar3, surface, SystemClock.elapsedRealtime()));
                    }
                }
            }
        } else if (i == 7) {
            this.y1 = (u) obj;
        } else if (i == 10) {
            int intValue = ((Integer) obj).intValue();
            if (this.w1 != intValue) {
                this.w1 = intValue;
                if (this.v1) {
                    o0();
                }
            }
        } else if (i == 4) {
            int intValue2 = ((Integer) obj).intValue();
            this.c1 = intValue2;
            t tVar2 = this.S;
            if (tVar2 != null) {
                tVar2.g(intValue2);
            }
        } else if (i == 5) {
            v vVar2 = this.R0;
            int intValue3 = ((Integer) obj).intValue();
            if (vVar2.j != intValue3) {
                vVar2.j = intValue3;
                vVar2.d(true);
            }
        }
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    public boolean w0(u uVar) {
        return this.Z0 != null || S0(uVar);
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    public int y0(b.i.a.c.y2.v vVar, j1 j1Var) throws MediaCodecUtil.DecoderQueryException {
        int i = 0;
        if (!b.i.a.c.f3.t.j(j1Var.w)) {
            return 0;
        }
        boolean z2 = j1Var.f1016z != null;
        List<u> G0 = G0(vVar, j1Var, z2, false);
        if (z2 && G0.isEmpty()) {
            G0 = G0(vVar, j1Var, false, false);
        }
        if (G0.isEmpty()) {
            return 1;
        }
        int i2 = j1Var.P;
        if (!(i2 == 0 || i2 == 2)) {
            return 2;
        }
        u uVar = G0.get(0);
        boolean e = uVar.e(j1Var);
        int i3 = uVar.f(j1Var) ? 16 : 8;
        if (e) {
            List<u> G02 = G0(vVar, j1Var, z2, true);
            if (!G02.isEmpty()) {
                u uVar2 = G02.get(0);
                if (uVar2.e(j1Var) && uVar2.f(j1Var)) {
                    i = 32;
                }
            }
        }
        return (e ? 4 : 3) | i3 | i;
    }
}
