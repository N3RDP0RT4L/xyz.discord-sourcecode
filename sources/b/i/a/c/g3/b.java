package b.i.a.c.g3;

import android.util.Log;
import android.view.Display;
import b.i.a.c.g3.v;
import java.util.Objects;
/* compiled from: lambda */
/* loaded from: classes3.dex */
public final /* synthetic */ class b implements v.b.a {
    public final /* synthetic */ v a;

    public final void a(Display display) {
        v vVar = this.a;
        Objects.requireNonNull(vVar);
        if (display != null) {
            long refreshRate = (long) (1.0E9d / display.getRefreshRate());
            vVar.k = refreshRate;
            vVar.l = (refreshRate * 80) / 100;
            return;
        }
        Log.w("VideoFrameReleaseHelper", "Unable to query display refresh rate");
        vVar.k = -9223372036854775807L;
        vVar.l = -9223372036854775807L;
    }
}
