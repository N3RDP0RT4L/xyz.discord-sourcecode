package b.i.a.c.g3;

import androidx.annotation.Nullable;
import b.i.a.c.f3.x;
/* compiled from: DolbyVisionConfig.java */
/* loaded from: classes3.dex */
public final class o {
    public final String a;

    public o(int i, int i2, String str) {
        this.a = str;
    }

    @Nullable
    public static o a(x xVar) {
        String str;
        xVar.F(2);
        int t = xVar.t();
        int i = t >> 1;
        int t2 = ((xVar.t() >> 3) & 31) | ((t & 1) << 5);
        if (i == 4 || i == 5 || i == 7) {
            str = "dvhe";
        } else if (i == 8) {
            str = "hev1";
        } else if (i != 9) {
            return null;
        } else {
            str = "avc3";
        }
        String str2 = t2 < 10 ? ".0" : ".";
        StringBuilder sb = new StringBuilder(str2.length() + str.length() + 24);
        sb.append(str);
        sb.append(".0");
        sb.append(i);
        sb.append(str2);
        sb.append(t2);
        return new o(i, t2, sb.toString());
    }
}
