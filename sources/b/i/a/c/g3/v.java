package b.i.a.c.g3;

import android.hardware.display.DisplayManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.view.Choreographer;
import android.view.Surface;
import android.view.WindowManager;
import androidx.annotation.DoNotInline;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.q;
import java.util.Objects;
/* compiled from: VideoFrameReleaseHelper.java */
/* loaded from: classes3.dex */
public final class v {
    public final p a = new p();
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public final b f989b;
    @Nullable
    public final e c;
    public boolean d;
    @Nullable
    public Surface e;
    public float f;
    public float g;
    public float h;
    public float i;
    public int j;
    public long k;
    public long l;
    public long m;
    public long n;
    public long o;
    public long p;
    public long q;

    /* compiled from: VideoFrameReleaseHelper.java */
    @RequiresApi(30)
    /* loaded from: classes3.dex */
    public static final class a {
        @DoNotInline
        public static void a(Surface surface, float f) {
            try {
                surface.setFrameRate(f, f == 0.0f ? 0 : 1);
            } catch (IllegalStateException e) {
                q.b("VideoFrameReleaseHelper", "Failed to call Surface.setFrameRate", e);
            }
        }
    }

    /* compiled from: VideoFrameReleaseHelper.java */
    /* loaded from: classes3.dex */
    public interface b {

        /* compiled from: VideoFrameReleaseHelper.java */
        /* loaded from: classes3.dex */
        public interface a {
        }

        void a(a aVar);

        void unregister();
    }

    /* compiled from: VideoFrameReleaseHelper.java */
    /* loaded from: classes3.dex */
    public static final class c implements b {
        public final WindowManager a;

        public c(WindowManager windowManager) {
            this.a = windowManager;
        }

        @Override // b.i.a.c.g3.v.b
        public void a(b.a aVar) {
            ((b.i.a.c.g3.b) aVar).a(this.a.getDefaultDisplay());
        }

        @Override // b.i.a.c.g3.v.b
        public void unregister() {
        }
    }

    /* compiled from: VideoFrameReleaseHelper.java */
    @RequiresApi(17)
    /* loaded from: classes3.dex */
    public static final class d implements b, DisplayManager.DisplayListener {
        public final DisplayManager a;
        @Nullable

        /* renamed from: b  reason: collision with root package name */
        public b.a f990b;

        public d(DisplayManager displayManager) {
            this.a = displayManager;
        }

        @Override // b.i.a.c.g3.v.b
        public void a(b.a aVar) {
            this.f990b = aVar;
            this.a.registerDisplayListener(this, e0.j());
            ((b.i.a.c.g3.b) aVar).a(this.a.getDisplay(0));
        }

        @Override // android.hardware.display.DisplayManager.DisplayListener
        public void onDisplayAdded(int i) {
        }

        @Override // android.hardware.display.DisplayManager.DisplayListener
        public void onDisplayChanged(int i) {
            b.a aVar = this.f990b;
            if (aVar != null && i == 0) {
                ((b.i.a.c.g3.b) aVar).a(this.a.getDisplay(0));
            }
        }

        @Override // android.hardware.display.DisplayManager.DisplayListener
        public void onDisplayRemoved(int i) {
        }

        @Override // b.i.a.c.g3.v.b
        public void unregister() {
            this.a.unregisterDisplayListener(this);
            this.f990b = null;
        }
    }

    /* compiled from: VideoFrameReleaseHelper.java */
    /* loaded from: classes3.dex */
    public static final class e implements Choreographer.FrameCallback, Handler.Callback {
        public static final e j = new e();
        public volatile long k = -9223372036854775807L;
        public final Handler l;
        public final HandlerThread m;
        public Choreographer n;
        public int o;

        public e() {
            HandlerThread handlerThread = new HandlerThread("ExoPlayer:FrameReleaseChoreographer");
            this.m = handlerThread;
            handlerThread.start();
            Looper looper = handlerThread.getLooper();
            int i = e0.a;
            Handler handler = new Handler(looper, this);
            this.l = handler;
            handler.sendEmptyMessage(0);
        }

        @Override // android.view.Choreographer.FrameCallback
        public void doFrame(long j2) {
            this.k = j2;
            Choreographer choreographer = this.n;
            Objects.requireNonNull(choreographer);
            choreographer.postFrameCallbackDelayed(this, 500L);
        }

        @Override // android.os.Handler.Callback
        public boolean handleMessage(Message message) {
            int i = message.what;
            if (i == 0) {
                this.n = Choreographer.getInstance();
                return true;
            } else if (i == 1) {
                int i2 = this.o + 1;
                this.o = i2;
                if (i2 == 1) {
                    Choreographer choreographer = this.n;
                    Objects.requireNonNull(choreographer);
                    choreographer.postFrameCallback(this);
                }
                return true;
            } else if (i != 2) {
                return false;
            } else {
                int i3 = this.o - 1;
                this.o = i3;
                if (i3 == 0) {
                    Choreographer choreographer2 = this.n;
                    Objects.requireNonNull(choreographer2);
                    choreographer2.removeFrameCallback(this);
                    this.k = -9223372036854775807L;
                }
                return true;
            }
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:17:0x003f  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public v(@androidx.annotation.Nullable android.content.Context r4) {
        /*
            r3 = this;
            r3.<init>()
            b.i.a.c.g3.p r0 = new b.i.a.c.g3.p
            r0.<init>()
            r3.a = r0
            r0 = 0
            if (r4 == 0) goto L3a
            android.content.Context r4 = r4.getApplicationContext()
            int r1 = b.i.a.c.f3.e0.a
            r2 = 17
            if (r1 < r2) goto L27
            java.lang.String r1 = "display"
            java.lang.Object r1 = r4.getSystemService(r1)
            android.hardware.display.DisplayManager r1 = (android.hardware.display.DisplayManager) r1
            if (r1 == 0) goto L27
            b.i.a.c.g3.v$d r2 = new b.i.a.c.g3.v$d
            r2.<init>(r1)
            goto L28
        L27:
            r2 = r0
        L28:
            if (r2 != 0) goto L3b
            java.lang.String r1 = "window"
            java.lang.Object r4 = r4.getSystemService(r1)
            android.view.WindowManager r4 = (android.view.WindowManager) r4
            if (r4 == 0) goto L3a
            b.i.a.c.g3.v$c r2 = new b.i.a.c.g3.v$c
            r2.<init>(r4)
            goto L3b
        L3a:
            r2 = r0
        L3b:
            r3.f989b = r2
            if (r2 == 0) goto L41
            b.i.a.c.g3.v$e r0 = b.i.a.c.g3.v.e.j
        L41:
            r3.c = r0
            r0 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            r3.k = r0
            r3.l = r0
            r4 = -1082130432(0xffffffffbf800000, float:-1.0)
            r3.f = r4
            r4 = 1065353216(0x3f800000, float:1.0)
            r3.i = r4
            r4 = 0
            r3.j = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.g3.v.<init>(android.content.Context):void");
    }

    public final void a() {
        Surface surface;
        if (e0.a >= 30 && (surface = this.e) != null && this.j != Integer.MIN_VALUE && this.h != 0.0f) {
            this.h = 0.0f;
            a.a(surface, 0.0f);
        }
    }

    public final void b() {
        this.m = 0L;
        this.p = -1L;
        this.n = -1L;
    }

    /* JADX WARN: Code restructure failed: missing block: B:37:0x0083, code lost:
        if (java.lang.Math.abs(r0 - r10.g) < (!r1 ? 0.02f : 1.0f)) goto L38;
     */
    /* JADX WARN: Code restructure failed: missing block: B:38:0x0086, code lost:
        r5 = false;
     */
    /* JADX WARN: Code restructure failed: missing block: B:41:0x008f, code lost:
        if (r10.a.e >= 30) goto L42;
     */
    /* JADX WARN: Removed duplicated region for block: B:34:0x0073  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x0077  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void c() {
        /*
            r10 = this;
            int r0 = b.i.a.c.f3.e0.a
            r1 = 30
            if (r0 < r1) goto L98
            android.view.Surface r0 = r10.e
            if (r0 != 0) goto Lc
            goto L98
        Lc:
            b.i.a.c.g3.p r0 = r10.a
            boolean r0 = r0.a()
            r2 = -1082130432(0xffffffffbf800000, float:-1.0)
            if (r0 == 0) goto L38
            b.i.a.c.g3.p r0 = r10.a
            boolean r3 = r0.a()
            if (r3 == 0) goto L35
            r3 = 4741671816366391296(0x41cdcd6500000000, double:1.0E9)
            b.i.a.c.g3.p$a r0 = r0.a
            long r5 = r0.e
            r7 = 0
            int r9 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r9 != 0) goto L2e
            goto L31
        L2e:
            long r7 = r0.f
            long r7 = r7 / r5
        L31:
            double r5 = (double) r7
            double r3 = r3 / r5
            float r0 = (float) r3
            goto L3a
        L35:
            r0 = -1082130432(0xffffffffbf800000, float:-1.0)
            goto L3a
        L38:
            float r0 = r10.f
        L3a:
            float r3 = r10.g
            int r4 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r4 != 0) goto L41
            return
        L41:
            r4 = 0
            r5 = 1
            int r6 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r6 == 0) goto L88
            int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
            if (r2 == 0) goto L88
            b.i.a.c.g3.p r1 = r10.a
            boolean r1 = r1.a()
            if (r1 == 0) goto L70
            b.i.a.c.g3.p r1 = r10.a
            boolean r2 = r1.a()
            if (r2 == 0) goto L60
            b.i.a.c.g3.p$a r1 = r1.a
            long r1 = r1.f
            goto L65
        L60:
            r1 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
        L65:
            r6 = 5000000000(0x12a05f200, double:2.470328229E-314)
            int r3 = (r1 > r6 ? 1 : (r1 == r6 ? 0 : -1))
            if (r3 < 0) goto L70
            r1 = 1
            goto L71
        L70:
            r1 = 0
        L71:
            if (r1 == 0) goto L77
            r1 = 1017370378(0x3ca3d70a, float:0.02)
            goto L79
        L77:
            r1 = 1065353216(0x3f800000, float:1.0)
        L79:
            float r2 = r10.g
            float r2 = r0 - r2
            float r2 = java.lang.Math.abs(r2)
            int r1 = (r2 > r1 ? 1 : (r2 == r1 ? 0 : -1))
            if (r1 < 0) goto L86
            goto L91
        L86:
            r5 = 0
            goto L91
        L88:
            if (r6 == 0) goto L8b
            goto L91
        L8b:
            b.i.a.c.g3.p r2 = r10.a
            int r2 = r2.e
            if (r2 < r1) goto L86
        L91:
            if (r5 == 0) goto L98
            r10.g = r0
            r10.d(r4)
        L98:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.g3.v.c():void");
    }

    public final void d(boolean z2) {
        Surface surface;
        if (e0.a >= 30 && (surface = this.e) != null && this.j != Integer.MIN_VALUE) {
            float f = 0.0f;
            if (this.d) {
                float f2 = this.g;
                if (f2 != -1.0f) {
                    f = this.i * f2;
                }
            }
            if (z2 || this.h != f) {
                this.h = f;
                a.a(surface, f);
            }
        }
    }
}
