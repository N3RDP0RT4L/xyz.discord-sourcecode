package b.i.a.c.g3.z;

import android.graphics.SurfaceTexture;
import android.media.MediaFormat;
import android.opengl.GLES20;
import androidx.annotation.Nullable;
import androidx.work.Data;
import b.c.a.a0.d;
import b.i.a.c.f3.c0;
import b.i.a.c.f3.x;
import b.i.a.c.g3.u;
import b.i.a.c.g3.z.h;
import b.i.a.c.j1;
import com.google.android.exoplayer2.util.GlUtil;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
/* compiled from: SceneRenderer.java */
/* loaded from: classes3.dex */
public final class j implements u, d {
    public int r;

    /* renamed from: s  reason: collision with root package name */
    public SurfaceTexture f1000s;
    @Nullable
    public byte[] v;
    public final AtomicBoolean j = new AtomicBoolean();
    public final AtomicBoolean k = new AtomicBoolean(true);
    public final i l = new i();
    public final f m = new f();
    public final c0<Long> n = new c0<>();
    public final c0<h> o = new c0<>();
    public final float[] p = new float[16];
    public final float[] q = new float[16];
    public volatile int t = 0;
    public int u = -1;

    @Override // b.i.a.c.g3.z.d
    public void a(long j, float[] fArr) {
        this.m.c.a(j, fArr);
    }

    public SurfaceTexture b() {
        GLES20.glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
        GlUtil.a();
        i iVar = this.l;
        Objects.requireNonNull(iVar);
        GlUtil.a aVar = new GlUtil.a(i.a, i.f998b);
        iVar.k = aVar;
        iVar.l = GLES20.glGetUniformLocation(aVar.a, "uMvpMatrix");
        iVar.m = GLES20.glGetUniformLocation(iVar.k.a, "uTexMatrix");
        iVar.n = GLES20.glGetAttribLocation(iVar.k.a, "aPosition");
        iVar.o = GLES20.glGetAttribLocation(iVar.k.a, "aTexCoords");
        iVar.p = GLES20.glGetUniformLocation(iVar.k.a, "uTexture");
        GlUtil.a();
        int[] iArr = new int[1];
        GLES20.glGenTextures(1, IntBuffer.wrap(iArr));
        GLES20.glBindTexture(36197, iArr[0]);
        GLES20.glTexParameteri(36197, 10241, 9729);
        GLES20.glTexParameteri(36197, Data.MAX_DATA_BYTES, 9729);
        GLES20.glTexParameteri(36197, 10242, 33071);
        GLES20.glTexParameteri(36197, 10243, 33071);
        GlUtil.a();
        this.r = iArr[0];
        SurfaceTexture surfaceTexture = new SurfaceTexture(this.r);
        this.f1000s = surfaceTexture;
        surfaceTexture.setOnFrameAvailableListener(new SurfaceTexture.OnFrameAvailableListener() { // from class: b.i.a.c.g3.z.a
            @Override // android.graphics.SurfaceTexture.OnFrameAvailableListener
            public final void onFrameAvailable(SurfaceTexture surfaceTexture2) {
                j.this.j.set(true);
            }
        });
        return this.f1000s;
    }

    @Override // b.i.a.c.g3.z.d
    public void c() {
        this.n.b();
        f fVar = this.m;
        fVar.c.b();
        fVar.d = false;
        this.k.set(true);
    }

    @Override // b.i.a.c.g3.u
    public void e(long j, long j2, j1 j1Var, @Nullable MediaFormat mediaFormat) {
        float f;
        float f2;
        int i;
        int i2;
        ArrayList<h.a> arrayList;
        int f3;
        this.n.a(j2, Long.valueOf(j));
        byte[] bArr = j1Var.G;
        int i3 = j1Var.H;
        byte[] bArr2 = this.v;
        int i4 = this.u;
        this.v = bArr;
        if (i3 == -1) {
            i3 = this.t;
        }
        this.u = i3;
        if (i4 != i3 || !Arrays.equals(bArr2, this.v)) {
            byte[] bArr3 = this.v;
            h hVar = null;
            if (bArr3 != null) {
                int i5 = this.u;
                x xVar = new x(bArr3);
                try {
                    xVar.F(4);
                    f3 = xVar.f();
                    xVar.E(0);
                } catch (ArrayIndexOutOfBoundsException unused) {
                }
                if (f3 == 1886547818) {
                    xVar.F(8);
                    int i6 = xVar.f980b;
                    int i7 = xVar.c;
                    while (i6 < i7) {
                        int f4 = xVar.f() + i6;
                        if (f4 <= i6 || f4 > i7) {
                            break;
                        }
                        int f5 = xVar.f();
                        if (f5 != 2037673328 && f5 != 1836279920) {
                            xVar.E(f4);
                            i6 = f4;
                        }
                        xVar.D(f4);
                        arrayList = d.t1(xVar);
                        break;
                    }
                    arrayList = null;
                } else {
                    arrayList = d.t1(xVar);
                }
                if (arrayList != null) {
                    int size = arrayList.size();
                    if (size == 1) {
                        hVar = new h(arrayList.get(0), i5);
                    } else if (size == 2) {
                        hVar = new h(arrayList.get(0), arrayList.get(1), i5);
                    }
                }
            }
            if (hVar == null || !i.a(hVar)) {
                int i8 = this.u;
                d.j(true);
                d.j(true);
                d.j(true);
                d.j(true);
                d.j(true);
                float radians = (float) Math.toRadians(180.0f);
                float radians2 = (float) Math.toRadians(360.0f);
                float f6 = radians / 36;
                float f7 = radians2 / 72;
                float[] fArr = new float[15984];
                float[] fArr2 = new float[10656];
                int i9 = 0;
                int i10 = 0;
                int i11 = 0;
                for (int i12 = 36; i9 < i12; i12 = 36) {
                    float f8 = radians / 2.0f;
                    float f9 = (i9 * f6) - f8;
                    int i13 = i9 + 1;
                    float f10 = (i13 * f6) - f8;
                    int i14 = 0;
                    while (i14 < 73) {
                        i13 = i13;
                        int i15 = 0;
                        for (int i16 = 2; i15 < i16; i16 = 2) {
                            if (i15 == 0) {
                                f = f10;
                                f2 = f9;
                            } else {
                                f2 = f10;
                                f = f2;
                            }
                            float f11 = i14 * f7;
                            f9 = f9;
                            int i17 = i10 + 1;
                            f7 = f7;
                            double d = 50.0f;
                            int i18 = i14;
                            double d2 = (f11 + 3.1415927f) - (radians2 / 2.0f);
                            i8 = i8;
                            float f12 = radians;
                            double d3 = f2;
                            float f13 = f6;
                            fArr[i10] = -((float) (Math.cos(d3) * Math.sin(d2) * d));
                            int i19 = i17 + 1;
                            int i20 = i15;
                            fArr[i17] = (float) (Math.sin(d3) * d);
                            int i21 = i19 + 1;
                            fArr[i19] = (float) (Math.cos(d3) * Math.cos(d2) * d);
                            int i22 = i11 + 1;
                            fArr2[i11] = f11 / radians2;
                            int i23 = i22 + 1;
                            fArr2[i22] = ((i9 + i20) * f13) / f12;
                            if (i18 == 0 && i20 == 0) {
                                i = i20;
                                i2 = i18;
                            } else {
                                i2 = i18;
                                i = i20;
                                if (!(i2 == 72 && i == 1)) {
                                    i11 = i23;
                                    i10 = i21;
                                    i15 = i + 1;
                                    i14 = i2;
                                    f10 = f;
                                    radians = f12;
                                    f6 = f13;
                                }
                            }
                            System.arraycopy(fArr, i21 - 3, fArr, i21, 3);
                            i21 += 3;
                            System.arraycopy(fArr2, i23 - 2, fArr2, i23, 2);
                            i23 += 2;
                            i11 = i23;
                            i10 = i21;
                            i15 = i + 1;
                            i14 = i2;
                            f10 = f;
                            radians = f12;
                            f6 = f13;
                        }
                        i8 = i8;
                        f10 = f10;
                        i14++;
                    }
                    i9 = i13;
                }
                hVar = new h(new h.a(new h.b(0, fArr, fArr2, 1)), i8);
            }
            this.o.a(j2, hVar);
        }
    }
}
