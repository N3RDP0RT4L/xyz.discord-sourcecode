package b.i.a.c.g3.z;

import android.opengl.Matrix;
import b.i.a.c.f3.c0;
/* compiled from: FrameRotationQueue.java */
/* loaded from: classes3.dex */
public final class f {
    public final float[] a = new float[16];

    /* renamed from: b  reason: collision with root package name */
    public final float[] f994b = new float[16];
    public final c0<float[]> c = new c0<>();
    public boolean d;

    public static void a(float[] fArr, float[] fArr2) {
        Matrix.setIdentityM(fArr, 0);
        float sqrt = (float) Math.sqrt((fArr2[8] * fArr2[8]) + (fArr2[10] * fArr2[10]));
        fArr[0] = fArr2[10] / sqrt;
        fArr[2] = fArr2[8] / sqrt;
        fArr[8] = (-fArr2[8]) / sqrt;
        fArr[10] = fArr2[10] / sqrt;
    }
}
