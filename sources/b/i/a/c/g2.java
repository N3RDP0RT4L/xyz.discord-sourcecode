package b.i.a.c;

import com.google.android.exoplayer2.ExoPlaybackException;
/* compiled from: RendererCapabilities.java */
/* loaded from: classes3.dex */
public interface g2 {
    int a(j1 j1Var) throws ExoPlaybackException;

    String getName();

    int p() throws ExoPlaybackException;
}
