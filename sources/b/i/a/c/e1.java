package b.i.a.c;

import android.content.Context;
import android.os.Looper;
import b.i.a.c.a3.c0;
import b.i.a.c.a3.p;
import b.i.a.c.c3.h;
import b.i.a.c.c3.q;
import b.i.a.c.e1;
import b.i.a.c.e3.f;
import b.i.a.c.e3.p;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.g;
import b.i.a.c.s2.g1;
import b.i.a.c.t2.o;
import b.i.b.a.l;
import java.util.Objects;
/* compiled from: ExoPlayer.java */
/* loaded from: classes3.dex */
public interface e1 extends y1 {

    /* compiled from: ExoPlayer.java */
    /* loaded from: classes3.dex */
    public interface a {
        void o(boolean z2);

        void x(boolean z2);
    }

    /* compiled from: ExoPlayer.java */
    /* loaded from: classes3.dex */
    public static final class b {
        public final Context a;
        public l<i2> c;
        public l<c0> d;
        public l<q> e;
        public l<n1> f;
        public l<f> g;

        /* renamed from: s  reason: collision with root package name */
        public boolean f920s;
        public l<g1> h = new l() { // from class: b.i.a.c.i
            @Override // b.i.b.a.l
            public final Object get() {
                g gVar = e1.b.this.f919b;
                Objects.requireNonNull(gVar);
                return new g1(gVar);
            }
        };
        public Looper i = e0.o();
        public o j = o.j;
        public int k = 1;
        public boolean l = true;
        public j2 m = j2.f1022b;
        public long n = 5000;
        public long o = 15000;
        public m1 p = new y0(0.97f, 1.03f, 1000, 1.0E-7f, e0.B(20), e0.B(500), 0.999f, null);

        /* renamed from: b  reason: collision with root package name */
        public g f919b = g.a;
        public long q = 500;
        public long r = 2000;

        public b(final Context context) {
            l<i2> fVar = new l() { // from class: b.i.a.c.f
                @Override // b.i.b.a.l
                public final Object get() {
                    return new b1(context);
                }
            };
            l<c0> hVar = new l() { // from class: b.i.a.c.h
                @Override // b.i.b.a.l
                public final Object get() {
                    return new p(context, new b.i.a.c.x2.f());
                }
            };
            l<q> gVar = new l() { // from class: b.i.a.c.g
                @Override // b.i.b.a.l
                public final Object get() {
                    return new h(context);
                }
            };
            b.i.a.c.a aVar = b.i.a.c.a.j;
            l<f> eVar = new l() { // from class: b.i.a.c.e
                @Override // b.i.b.a.l
                public final Object get() {
                    b.i.a.c.e3.p pVar;
                    Context context2 = context;
                    b.i.b.b.p<Long> pVar2 = b.i.a.c.e3.p.a;
                    synchronized (b.i.a.c.e3.p.class) {
                        if (b.i.a.c.e3.p.g == null) {
                            p.b bVar = new p.b(context2);
                            b.i.a.c.e3.p.g = new b.i.a.c.e3.p(bVar.a, bVar.f943b, bVar.c, bVar.d, bVar.e, null);
                        }
                        pVar = b.i.a.c.e3.p.g;
                    }
                    return pVar;
                }
            };
            this.a = context;
            this.c = fVar;
            this.d = hVar;
            this.e = gVar;
            this.f = aVar;
            this.g = eVar;
        }
    }
}
