package b.i.a.c;

import androidx.annotation.Nullable;
import com.google.android.exoplayer2.drm.DrmSession;
/* compiled from: FormatHolder.java */
/* loaded from: classes3.dex */
public final class k1 {
    @Nullable
    public DrmSession a;
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public j1 f1023b;

    public void a() {
        this.a = null;
        this.f1023b = null;
    }
}
