package b.i.a.c;

import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import b.i.a.c.f3.e0;
import b.i.a.c.w0;
import java.util.Arrays;
/* compiled from: MediaMetadata.java */
/* loaded from: classes3.dex */
public final class p1 implements w0 {
    public static final p1 j = new b().a();
    public static final w0.a<p1> k = l0.a;
    @Nullable
    public final Integer A;
    @Nullable
    public final Boolean B;
    @Nullable
    @Deprecated
    public final Integer C;
    @Nullable
    public final Integer D;
    @Nullable
    public final Integer E;
    @Nullable
    public final Integer F;
    @Nullable
    public final Integer G;
    @Nullable
    public final Integer H;
    @Nullable
    public final Integer I;
    @Nullable
    public final CharSequence J;
    @Nullable
    public final CharSequence K;
    @Nullable
    public final CharSequence L;
    @Nullable
    public final Integer M;
    @Nullable
    public final Integer N;
    @Nullable
    public final CharSequence O;
    @Nullable
    public final CharSequence P;
    @Nullable
    public final Bundle Q;
    @Nullable
    public final CharSequence l;
    @Nullable
    public final CharSequence m;
    @Nullable
    public final CharSequence n;
    @Nullable
    public final CharSequence o;
    @Nullable
    public final CharSequence p;
    @Nullable
    public final CharSequence q;
    @Nullable
    public final CharSequence r;
    @Nullable

    /* renamed from: s  reason: collision with root package name */
    public final Uri f1042s;
    @Nullable
    public final d2 t;
    @Nullable
    public final d2 u;
    @Nullable
    public final byte[] v;
    @Nullable
    public final Integer w;
    @Nullable

    /* renamed from: x  reason: collision with root package name */
    public final Uri f1043x;
    @Nullable

    /* renamed from: y  reason: collision with root package name */
    public final Integer f1044y;
    @Nullable

    /* renamed from: z  reason: collision with root package name */
    public final Integer f1045z;

    /* compiled from: MediaMetadata.java */
    /* loaded from: classes3.dex */
    public static final class b {
        @Nullable
        public Integer A;
        @Nullable
        public Integer B;
        @Nullable
        public CharSequence C;
        @Nullable
        public CharSequence D;
        @Nullable
        public Bundle E;
        @Nullable
        public CharSequence a;
        @Nullable

        /* renamed from: b  reason: collision with root package name */
        public CharSequence f1046b;
        @Nullable
        public CharSequence c;
        @Nullable
        public CharSequence d;
        @Nullable
        public CharSequence e;
        @Nullable
        public CharSequence f;
        @Nullable
        public CharSequence g;
        @Nullable
        public Uri h;
        @Nullable
        public d2 i;
        @Nullable
        public d2 j;
        @Nullable
        public byte[] k;
        @Nullable
        public Integer l;
        @Nullable
        public Uri m;
        @Nullable
        public Integer n;
        @Nullable
        public Integer o;
        @Nullable
        public Integer p;
        @Nullable
        public Boolean q;
        @Nullable
        public Integer r;
        @Nullable

        /* renamed from: s  reason: collision with root package name */
        public Integer f1047s;
        @Nullable
        public Integer t;
        @Nullable
        public Integer u;
        @Nullable
        public Integer v;
        @Nullable
        public Integer w;
        @Nullable

        /* renamed from: x  reason: collision with root package name */
        public CharSequence f1048x;
        @Nullable

        /* renamed from: y  reason: collision with root package name */
        public CharSequence f1049y;
        @Nullable

        /* renamed from: z  reason: collision with root package name */
        public CharSequence f1050z;

        public b() {
        }

        public p1 a() {
            return new p1(this, null);
        }

        public b b(byte[] bArr, int i) {
            if (this.k == null || e0.a(Integer.valueOf(i), 3) || !e0.a(this.l, 3)) {
                this.k = (byte[]) bArr.clone();
                this.l = Integer.valueOf(i);
            }
            return this;
        }

        public b(p1 p1Var, a aVar) {
            this.a = p1Var.l;
            this.f1046b = p1Var.m;
            this.c = p1Var.n;
            this.d = p1Var.o;
            this.e = p1Var.p;
            this.f = p1Var.q;
            this.g = p1Var.r;
            this.h = p1Var.f1042s;
            this.i = p1Var.t;
            this.j = p1Var.u;
            this.k = p1Var.v;
            this.l = p1Var.w;
            this.m = p1Var.f1043x;
            this.n = p1Var.f1044y;
            this.o = p1Var.f1045z;
            this.p = p1Var.A;
            this.q = p1Var.B;
            this.r = p1Var.D;
            this.f1047s = p1Var.E;
            this.t = p1Var.F;
            this.u = p1Var.G;
            this.v = p1Var.H;
            this.w = p1Var.I;
            this.f1048x = p1Var.J;
            this.f1049y = p1Var.K;
            this.f1050z = p1Var.L;
            this.A = p1Var.M;
            this.B = p1Var.N;
            this.C = p1Var.O;
            this.D = p1Var.P;
            this.E = p1Var.Q;
        }
    }

    public p1(b bVar, a aVar) {
        this.l = bVar.a;
        this.m = bVar.f1046b;
        this.n = bVar.c;
        this.o = bVar.d;
        this.p = bVar.e;
        this.q = bVar.f;
        this.r = bVar.g;
        this.f1042s = bVar.h;
        this.t = bVar.i;
        this.u = bVar.j;
        this.v = bVar.k;
        this.w = bVar.l;
        this.f1043x = bVar.m;
        this.f1044y = bVar.n;
        this.f1045z = bVar.o;
        this.A = bVar.p;
        this.B = bVar.q;
        Integer num = bVar.r;
        this.C = num;
        this.D = num;
        this.E = bVar.f1047s;
        this.F = bVar.t;
        this.G = bVar.u;
        this.H = bVar.v;
        this.I = bVar.w;
        this.J = bVar.f1048x;
        this.K = bVar.f1049y;
        this.L = bVar.f1050z;
        this.M = bVar.A;
        this.N = bVar.B;
        this.O = bVar.C;
        this.P = bVar.D;
        this.Q = bVar.E;
    }

    public static String b(int i) {
        return Integer.toString(i, 36);
    }

    public b a() {
        return new b(this, null);
    }

    public boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || p1.class != obj.getClass()) {
            return false;
        }
        p1 p1Var = (p1) obj;
        return e0.a(this.l, p1Var.l) && e0.a(this.m, p1Var.m) && e0.a(this.n, p1Var.n) && e0.a(this.o, p1Var.o) && e0.a(this.p, p1Var.p) && e0.a(this.q, p1Var.q) && e0.a(this.r, p1Var.r) && e0.a(this.f1042s, p1Var.f1042s) && e0.a(this.t, p1Var.t) && e0.a(this.u, p1Var.u) && Arrays.equals(this.v, p1Var.v) && e0.a(this.w, p1Var.w) && e0.a(this.f1043x, p1Var.f1043x) && e0.a(this.f1044y, p1Var.f1044y) && e0.a(this.f1045z, p1Var.f1045z) && e0.a(this.A, p1Var.A) && e0.a(this.B, p1Var.B) && e0.a(this.D, p1Var.D) && e0.a(this.E, p1Var.E) && e0.a(this.F, p1Var.F) && e0.a(this.G, p1Var.G) && e0.a(this.H, p1Var.H) && e0.a(this.I, p1Var.I) && e0.a(this.J, p1Var.J) && e0.a(this.K, p1Var.K) && e0.a(this.L, p1Var.L) && e0.a(this.M, p1Var.M) && e0.a(this.N, p1Var.N) && e0.a(this.O, p1Var.O) && e0.a(this.P, p1Var.P);
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.l, this.m, this.n, this.o, this.p, this.q, this.r, this.f1042s, this.t, this.u, Integer.valueOf(Arrays.hashCode(this.v)), this.w, this.f1043x, this.f1044y, this.f1045z, this.A, this.B, this.D, this.E, this.F, this.G, this.H, this.I, this.J, this.K, this.L, this.M, this.N, this.O, this.P});
    }
}
