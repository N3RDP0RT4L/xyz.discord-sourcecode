package b.i.a.c;

import androidx.annotation.Nullable;
/* compiled from: RendererConfiguration.java */
/* loaded from: classes3.dex */
public final class h2 {
    public static final h2 a = new h2(false);

    /* renamed from: b  reason: collision with root package name */
    public final boolean f1011b;

    public h2(boolean z2) {
        this.f1011b = z2;
    }

    public boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        return obj != null && h2.class == obj.getClass() && this.f1011b == ((h2) obj).f1011b;
    }

    public int hashCode() {
        return !this.f1011b ? 1 : 0;
    }
}
