package b.i.a.c;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.util.Pair;
import androidx.annotation.CheckResult;
import androidx.annotation.Nullable;
import androidx.core.view.PointerIconCompat;
import b.i.a.c.a1;
import b.i.a.c.a3.a0;
import b.i.a.c.a3.k0;
import b.i.a.c.a3.o0;
import b.i.a.c.a3.x;
import b.i.a.c.b2;
import b.i.a.c.b3.m;
import b.i.a.c.c3.j;
import b.i.a.c.c3.q;
import b.i.a.c.c3.r;
import b.i.a.c.f3.b0;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.o;
import b.i.a.c.f3.s;
import b.i.a.c.f3.z;
import b.i.a.c.o1;
import b.i.a.c.o2;
import b.i.a.c.s2.g1;
import b.i.a.c.u1;
import b.i.b.b.h;
import b.i.b.b.h0;
import b.i.b.b.n;
import b.i.b.b.p;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.drm.DrmSession;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.upstream.DataSourceException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
/* compiled from: ExoPlayerImplInternal.java */
/* loaded from: classes3.dex */
public final class h1 implements Handler.Callback, x.a, u1.d, a1.a, b2.a {
    public final e A;
    public final s1 B;
    public final u1 C;
    public final m1 D;
    public final long E;
    public j2 F;
    public w1 G;
    public d H;
    public boolean I;
    public boolean J;
    public boolean K;
    public boolean L;
    public boolean M;
    public int N;
    public boolean O;
    public boolean P;
    public boolean Q;
    public boolean R;
    public int S;
    @Nullable
    public g T;
    public long U;
    public int V;
    public boolean W;
    @Nullable
    public ExoPlaybackException X;
    public final f2[] j;
    public final Set<f2> k;
    public final g2[] l;
    public final q m;
    public final r n;
    public final n1 o;
    public final b.i.a.c.e3.f p;
    public final o q;
    public final HandlerThread r;

    /* renamed from: s  reason: collision with root package name */
    public final Looper f1003s;
    public final o2.c t;
    public final o2.b u;
    public final long v;
    public final boolean w;

    /* renamed from: x  reason: collision with root package name */
    public final a1 f1004x;

    /* renamed from: y  reason: collision with root package name */
    public final ArrayList<c> f1005y;

    /* renamed from: z  reason: collision with root package name */
    public final b.i.a.c.f3.g f1006z;

    /* compiled from: ExoPlayerImplInternal.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public final List<u1.c> a;

        /* renamed from: b  reason: collision with root package name */
        public final k0 f1007b;
        public final int c;
        public final long d;

        public a(List list, k0 k0Var, int i, long j, g1 g1Var) {
            this.a = list;
            this.f1007b = k0Var;
            this.c = i;
            this.d = j;
        }
    }

    /* compiled from: ExoPlayerImplInternal.java */
    /* loaded from: classes3.dex */
    public static class b {
    }

    /* compiled from: ExoPlayerImplInternal.java */
    /* loaded from: classes3.dex */
    public static final class c implements Comparable<c> {
        public final b2 j;
        public int k;
        public long l;
        @Nullable
        public Object m;

        /* JADX WARN: Code restructure failed: missing block: B:12:0x0015, code lost:
            if (r0 != null) goto L13;
         */
        @Override // java.lang.Comparable
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public int compareTo(b.i.a.c.h1.c r9) {
            /*
                r8 = this;
                b.i.a.c.h1$c r9 = (b.i.a.c.h1.c) r9
                java.lang.Object r0 = r8.m
                r1 = 1
                r2 = 0
                if (r0 != 0) goto La
                r3 = 1
                goto Lb
            La:
                r3 = 0
            Lb:
                java.lang.Object r4 = r9.m
                if (r4 != 0) goto L11
                r4 = 1
                goto L12
            L11:
                r4 = 0
            L12:
                r5 = -1
                if (r3 == r4) goto L1a
                if (r0 == 0) goto L18
            L17:
                r1 = -1
            L18:
                r2 = r1
                goto L35
            L1a:
                if (r0 != 0) goto L1d
                goto L35
            L1d:
                int r0 = r8.k
                int r3 = r9.k
                int r0 = r0 - r3
                if (r0 == 0) goto L26
                r2 = r0
                goto L35
            L26:
                long r3 = r8.l
                long r6 = r9.l
                int r9 = b.i.a.c.f3.e0.a
                int r9 = (r3 > r6 ? 1 : (r3 == r6 ? 0 : -1))
                if (r9 >= 0) goto L31
                goto L17
            L31:
                if (r9 != 0) goto L18
                r1 = 0
                goto L18
            L35:
                return r2
            */
            throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.h1.c.compareTo(java.lang.Object):int");
        }

        public void f(int i, long j, Object obj) {
            this.k = i;
            this.l = j;
            this.m = obj;
        }
    }

    /* compiled from: ExoPlayerImplInternal.java */
    /* loaded from: classes3.dex */
    public static final class d {
        public boolean a;

        /* renamed from: b  reason: collision with root package name */
        public w1 f1008b;
        public int c;
        public boolean d;
        public int e;
        public boolean f;
        public int g;

        public d(w1 w1Var) {
            this.f1008b = w1Var;
        }

        public void a(int i) {
            this.a |= i > 0;
            this.c += i;
        }
    }

    /* compiled from: ExoPlayerImplInternal.java */
    /* loaded from: classes3.dex */
    public interface e {
    }

    /* compiled from: ExoPlayerImplInternal.java */
    /* loaded from: classes3.dex */
    public static final class f {
        public final a0.a a;

        /* renamed from: b  reason: collision with root package name */
        public final long f1009b;
        public final long c;
        public final boolean d;
        public final boolean e;
        public final boolean f;

        public f(a0.a aVar, long j, long j2, boolean z2, boolean z3, boolean z4) {
            this.a = aVar;
            this.f1009b = j;
            this.c = j2;
            this.d = z2;
            this.e = z3;
            this.f = z4;
        }
    }

    /* compiled from: ExoPlayerImplInternal.java */
    /* loaded from: classes3.dex */
    public static final class g {
        public final o2 a;

        /* renamed from: b  reason: collision with root package name */
        public final int f1010b;
        public final long c;

        public g(o2 o2Var, int i, long j) {
            this.a = o2Var;
            this.f1010b = i;
            this.c = j;
        }
    }

    public h1(f2[] f2VarArr, q qVar, r rVar, n1 n1Var, b.i.a.c.e3.f fVar, int i, boolean z2, @Nullable g1 g1Var, j2 j2Var, m1 m1Var, long j, boolean z3, Looper looper, b.i.a.c.f3.g gVar, e eVar) {
        this.A = eVar;
        this.j = f2VarArr;
        this.m = qVar;
        this.n = rVar;
        this.o = n1Var;
        this.p = fVar;
        this.N = i;
        this.O = z2;
        this.F = j2Var;
        this.D = m1Var;
        this.E = j;
        this.J = z3;
        this.f1006z = gVar;
        this.v = n1Var.b();
        this.w = n1Var.a();
        w1 h = w1.h(rVar);
        this.G = h;
        this.H = new d(h);
        this.l = new g2[f2VarArr.length];
        for (int i2 = 0; i2 < f2VarArr.length; i2++) {
            f2VarArr[i2].f(i2);
            this.l[i2] = f2VarArr[i2].m();
        }
        this.f1004x = new a1(this, gVar);
        this.f1005y = new ArrayList<>();
        this.k = h.c();
        this.t = new o2.c();
        this.u = new o2.b();
        qVar.a = fVar;
        this.W = true;
        Handler handler = new Handler(looper);
        this.B = new s1(g1Var, handler);
        this.C = new u1(this, g1Var, handler);
        HandlerThread handlerThread = new HandlerThread("ExoPlayer:Playback", -16);
        this.r = handlerThread;
        handlerThread.start();
        Looper looper2 = handlerThread.getLooper();
        this.f1003s = looper2;
        this.q = gVar.b(looper2, this);
    }

    public static boolean K(c cVar, o2 o2Var, o2 o2Var2, int i, boolean z2, o2.c cVar2, o2.b bVar) {
        Object obj = cVar.m;
        if (obj == null) {
            Objects.requireNonNull(cVar.j);
            Objects.requireNonNull(cVar.j);
            long B = e0.B(-9223372036854775807L);
            b2 b2Var = cVar.j;
            Pair<Object, Long> M = M(o2Var, new g(b2Var.d, b2Var.h, B), false, i, z2, cVar2, bVar);
            if (M == null) {
                return false;
            }
            cVar.f(o2Var.b(M.first), ((Long) M.second).longValue(), M.first);
            Objects.requireNonNull(cVar.j);
            return true;
        }
        int b2 = o2Var.b(obj);
        if (b2 == -1) {
            return false;
        }
        Objects.requireNonNull(cVar.j);
        cVar.k = b2;
        o2Var2.h(cVar.m, bVar);
        if (bVar.o && o2Var2.n(bVar.l, cVar2).B == o2Var2.b(cVar.m)) {
            Pair<Object, Long> j = o2Var.j(cVar2, bVar, o2Var.h(cVar.m, bVar).l, cVar.l + bVar.n);
            cVar.f(o2Var.b(j.first), ((Long) j.second).longValue(), j.first);
        }
        return true;
    }

    @Nullable
    public static Pair<Object, Long> M(o2 o2Var, g gVar, boolean z2, int i, boolean z3, o2.c cVar, o2.b bVar) {
        Pair<Object, Long> j;
        Object N;
        o2 o2Var2 = gVar.a;
        if (o2Var.q()) {
            return null;
        }
        o2 o2Var3 = o2Var2.q() ? o2Var : o2Var2;
        try {
            j = o2Var3.j(cVar, bVar, gVar.f1010b, gVar.c);
        } catch (IndexOutOfBoundsException unused) {
        }
        if (o2Var.equals(o2Var3)) {
            return j;
        }
        if (o2Var.b(j.first) != -1) {
            return (!o2Var3.h(j.first, bVar).o || o2Var3.n(bVar.l, cVar).B != o2Var3.b(j.first)) ? j : o2Var.j(cVar, bVar, o2Var.h(j.first, bVar).l, gVar.c);
        }
        if (z2 && (N = N(cVar, bVar, i, z3, j.first, o2Var3, o2Var)) != null) {
            return o2Var.j(cVar, bVar, o2Var.h(N, bVar).l, -9223372036854775807L);
        }
        return null;
    }

    @Nullable
    public static Object N(o2.c cVar, o2.b bVar, int i, boolean z2, Object obj, o2 o2Var, o2 o2Var2) {
        int b2 = o2Var.b(obj);
        int i2 = o2Var.i();
        int i3 = b2;
        int i4 = -1;
        for (int i5 = 0; i5 < i2 && i4 == -1; i5++) {
            i3 = o2Var.d(i3, bVar, cVar, i, z2);
            if (i3 == -1) {
                break;
            }
            i4 = o2Var2.b(o2Var.m(i3));
        }
        if (i4 == -1) {
            return null;
        }
        return o2Var2.m(i4);
    }

    public static j1[] i(j jVar) {
        int length = jVar != null ? jVar.length() : 0;
        j1[] j1VarArr = new j1[length];
        for (int i = 0; i < length; i++) {
            j1VarArr[i] = jVar.d(i);
        }
        return j1VarArr;
    }

    public static boolean w(f2 f2Var) {
        return f2Var.getState() != 0;
    }

    public static boolean y(w1 w1Var, o2.b bVar) {
        a0.a aVar = w1Var.c;
        o2 o2Var = w1Var.f1142b;
        return o2Var.q() || o2Var.h(aVar.a, bVar).o;
    }

    public final void A() {
        d dVar = this.H;
        w1 w1Var = this.G;
        boolean z2 = dVar.a | (dVar.f1008b != w1Var);
        dVar.a = z2;
        dVar.f1008b = w1Var;
        if (z2) {
            f1 f1Var = ((w) this.A).a;
            f1Var.g.b(new c0(f1Var, dVar));
            this.H = new d(this.G);
        }
    }

    public final void B() throws ExoPlaybackException {
        r(this.C.c(), true);
    }

    public final void C(b bVar) throws ExoPlaybackException {
        boolean z2 = true;
        this.H.a(1);
        u1 u1Var = this.C;
        Objects.requireNonNull(bVar);
        Objects.requireNonNull(u1Var);
        if (u1Var.e() < 0) {
            z2 = false;
        }
        b.c.a.a0.d.j(z2);
        u1Var.i = null;
        r(u1Var.c(), false);
    }

    public final void D() {
        this.H.a(1);
        H(false, false, false, true);
        this.o.c();
        f0(this.G.f1142b.q() ? 4 : 2);
        u1 u1Var = this.C;
        b.i.a.c.e3.a0 c2 = this.p.c();
        b.c.a.a0.d.D(!u1Var.j);
        u1Var.k = c2;
        for (int i = 0; i < u1Var.a.size(); i++) {
            u1.c cVar = u1Var.a.get(i);
            u1Var.g(cVar);
            u1Var.h.add(cVar);
        }
        u1Var.j = true;
        this.q.f(2);
    }

    public final void E() {
        H(true, false, true, false);
        this.o.e();
        f0(1);
        this.r.quit();
        synchronized (this) {
            this.I = true;
            notifyAll();
        }
    }

    public final void F(int i, int i2, k0 k0Var) throws ExoPlaybackException {
        boolean z2 = true;
        this.H.a(1);
        u1 u1Var = this.C;
        Objects.requireNonNull(u1Var);
        if (i < 0 || i > i2 || i2 > u1Var.e()) {
            z2 = false;
        }
        b.c.a.a0.d.j(z2);
        u1Var.i = k0Var;
        u1Var.i(i, i2);
        r(u1Var.c(), false);
    }

    /* JADX WARN: Removed duplicated region for block: B:50:0x00fd  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x0046 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void G() throws com.google.android.exoplayer2.ExoPlaybackException {
        /*
            Method dump skipped, instructions count: 262
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.h1.G():void");
    }

    /* JADX WARN: Removed duplicated region for block: B:35:0x00cb  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x00d1  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x00d4  */
    /* JADX WARN: Removed duplicated region for block: B:42:0x00d9  */
    /* JADX WARN: Removed duplicated region for block: B:43:0x00dc  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x00e0  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x00e5  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x010d  */
    /* JADX WARN: Removed duplicated region for block: B:75:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void H(boolean r30, boolean r31, boolean r32, boolean r33) {
        /*
            Method dump skipped, instructions count: 338
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.h1.H(boolean, boolean, boolean, boolean):void");
    }

    public final void I() {
        q1 q1Var = this.B.h;
        this.K = q1Var != null && q1Var.f.h && this.J;
    }

    public final void J(long j) throws ExoPlaybackException {
        f2[] f2VarArr;
        j[] jVarArr;
        q1 q1Var = this.B.h;
        long j2 = j + (q1Var == null ? 1000000000000L : q1Var.o);
        this.U = j2;
        this.f1004x.j.a(j2);
        for (f2 f2Var : this.j) {
            if (w(f2Var)) {
                f2Var.u(this.U);
            }
        }
        for (q1 q1Var2 = this.B.h; q1Var2 != null; q1Var2 = q1Var2.l) {
            for (j jVar : q1Var2.n.c) {
                if (jVar != null) {
                    jVar.j();
                }
            }
        }
    }

    public final void L(o2 o2Var, o2 o2Var2) {
        if (!o2Var.q() || !o2Var2.q()) {
            int size = this.f1005y.size();
            while (true) {
                size--;
                if (size < 0) {
                    Collections.sort(this.f1005y);
                    return;
                } else if (!K(this.f1005y.get(size), o2Var, o2Var2, this.N, this.O, this.t, this.u)) {
                    this.f1005y.get(size).j.c(false);
                    this.f1005y.remove(size);
                }
            }
        }
    }

    public final void O(long j, long j2) {
        this.q.h(2);
        this.q.g(2, j + j2);
    }

    public final void P(boolean z2) throws ExoPlaybackException {
        a0.a aVar = this.B.h.f.a;
        long S = S(aVar, this.G.t, true, false);
        if (S != this.G.t) {
            w1 w1Var = this.G;
            this.G = u(aVar, S, w1Var.d, w1Var.e, z2, 5);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:24:0x00ab A[Catch: all -> 0x0145, TryCatch #1 {all -> 0x0145, blocks: (B:22:0x00a1, B:24:0x00ab, B:27:0x00b1, B:29:0x00b7, B:30:0x00ba, B:32:0x00c0, B:34:0x00ca, B:36:0x00d0, B:40:0x00d8, B:42:0x00e2, B:44:0x00f2, B:48:0x00fc, B:52:0x010e, B:56:0x0117), top: B:73:0x00a1 }] */
    /* JADX WARN: Removed duplicated region for block: B:25:0x00ae  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void Q(b.i.a.c.h1.g r20) throws com.google.android.exoplayer2.ExoPlaybackException {
        /*
            Method dump skipped, instructions count: 342
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.h1.Q(b.i.a.c.h1$g):void");
    }

    public final long R(a0.a aVar, long j, boolean z2) throws ExoPlaybackException {
        s1 s1Var = this.B;
        return S(aVar, j, s1Var.h != s1Var.i, z2);
    }

    public final long S(a0.a aVar, long j, boolean z2, boolean z3) throws ExoPlaybackException {
        s1 s1Var;
        k0();
        this.L = false;
        if (z3 || this.G.f == 3) {
            f0(2);
        }
        q1 q1Var = this.B.h;
        q1 q1Var2 = q1Var;
        while (q1Var2 != null && !aVar.equals(q1Var2.f.a)) {
            q1Var2 = q1Var2.l;
        }
        if (z2 || q1Var != q1Var2 || (q1Var2 != null && q1Var2.o + j < 0)) {
            for (f2 f2Var : this.j) {
                e(f2Var);
            }
            if (q1Var2 != null) {
                while (true) {
                    s1Var = this.B;
                    if (s1Var.h == q1Var2) {
                        break;
                    }
                    s1Var.a();
                }
                s1Var.n(q1Var2);
                q1Var2.o = 1000000000000L;
                g();
            }
        }
        if (q1Var2 != null) {
            this.B.n(q1Var2);
            if (!q1Var2.d) {
                q1Var2.f = q1Var2.f.b(j);
            } else if (q1Var2.e) {
                long f2 = q1Var2.a.f(j);
                q1Var2.a.r(f2 - this.v, this.w);
                j = f2;
            }
            J(j);
            z();
        } else {
            this.B.b();
            J(j);
        }
        q(false);
        this.q.f(2);
        return j;
    }

    public final void T(b2 b2Var) throws ExoPlaybackException {
        if (b2Var.g == this.f1003s) {
            d(b2Var);
            int i = this.G.f;
            if (i == 3 || i == 2) {
                this.q.f(2);
                return;
            }
            return;
        }
        ((b0.b) this.q.i(15, b2Var)).b();
    }

    public final void U(final b2 b2Var) {
        Looper looper = b2Var.g;
        if (!looper.getThread().isAlive()) {
            Log.w("TAG", "Trying to send message on a dead thread.");
            b2Var.c(false);
            return;
        }
        this.f1006z.b(looper, null).b(new Runnable() { // from class: b.i.a.c.g0
            @Override // java.lang.Runnable
            public final void run() {
                h1 h1Var = h1.this;
                b2 b2Var2 = b2Var;
                Objects.requireNonNull(h1Var);
                try {
                    h1Var.d(b2Var2);
                } catch (ExoPlaybackException e2) {
                    b.i.a.c.f3.q.b("ExoPlayerImplInternal", "Unexpected error delivering message on external thread.", e2);
                    throw new RuntimeException(e2);
                }
            }
        });
    }

    public final void V(f2 f2Var, long j) {
        f2Var.l();
        if (f2Var instanceof m) {
            m mVar = (m) f2Var;
            b.c.a.a0.d.D(mVar.f1136s);
            mVar.I = j;
        }
    }

    public final void W(boolean z2, @Nullable AtomicBoolean atomicBoolean) {
        f2[] f2VarArr;
        if (this.P != z2) {
            this.P = z2;
            if (!z2) {
                for (f2 f2Var : this.j) {
                    if (!w(f2Var) && this.k.remove(f2Var)) {
                        f2Var.reset();
                    }
                }
            }
        }
        if (atomicBoolean != null) {
            synchronized (this) {
                atomicBoolean.set(true);
                notifyAll();
            }
        }
    }

    public final void X(a aVar) throws ExoPlaybackException {
        this.H.a(1);
        if (aVar.c != -1) {
            this.T = new g(new c2(aVar.a, aVar.f1007b), aVar.c, aVar.d);
        }
        u1 u1Var = this.C;
        List<u1.c> list = aVar.a;
        k0 k0Var = aVar.f1007b;
        u1Var.i(0, u1Var.a.size());
        r(u1Var.a(u1Var.a.size(), list, k0Var), false);
    }

    public final void Y(boolean z2) {
        if (z2 != this.R) {
            this.R = z2;
            w1 w1Var = this.G;
            int i = w1Var.f;
            if (z2 || i == 4 || i == 1) {
                this.G = w1Var.c(z2);
            } else {
                this.q.f(2);
            }
        }
    }

    public final void Z(boolean z2) throws ExoPlaybackException {
        this.J = z2;
        I();
        if (this.K) {
            s1 s1Var = this.B;
            if (s1Var.i != s1Var.h) {
                P(true);
                q(false);
            }
        }
    }

    @Override // b.i.a.c.a3.j0.a
    public void a(x xVar) {
        ((b0.b) this.q.i(9, xVar)).b();
    }

    public final void a0(boolean z2, int i, boolean z3, int i2) throws ExoPlaybackException {
        j[] jVarArr;
        this.H.a(z3 ? 1 : 0);
        d dVar = this.H;
        dVar.a = true;
        dVar.f = true;
        dVar.g = i2;
        this.G = this.G.d(z2, i);
        this.L = false;
        for (q1 q1Var = this.B.h; q1Var != null; q1Var = q1Var.l) {
            for (j jVar : q1Var.n.c) {
                if (jVar != null) {
                    jVar.c(z2);
                }
            }
        }
        if (!g0()) {
            k0();
            n0();
            return;
        }
        int i3 = this.G.f;
        if (i3 == 3) {
            i0();
            this.q.f(2);
        } else if (i3 == 2) {
            this.q.f(2);
        }
    }

    @Override // b.i.a.c.a3.x.a
    public void b(x xVar) {
        ((b0.b) this.q.i(8, xVar)).b();
    }

    public final void b0(x1 x1Var) throws ExoPlaybackException {
        this.f1004x.i(x1Var);
        x1 c2 = this.f1004x.c();
        t(c2, c2.k, true, true);
    }

    public final void c(a aVar, int i) throws ExoPlaybackException {
        this.H.a(1);
        u1 u1Var = this.C;
        if (i == -1) {
            i = u1Var.e();
        }
        r(u1Var.a(i, aVar.a, aVar.f1007b), false);
    }

    public final void c0(int i) throws ExoPlaybackException {
        this.N = i;
        s1 s1Var = this.B;
        o2 o2Var = this.G.f1142b;
        s1Var.f = i;
        if (!s1Var.q(o2Var)) {
            P(true);
        }
        q(false);
    }

    public final void d(b2 b2Var) throws ExoPlaybackException {
        b2Var.b();
        try {
            b2Var.a.r(b2Var.e, b2Var.f);
        } finally {
            b2Var.c(true);
        }
    }

    public final void d0(boolean z2) throws ExoPlaybackException {
        this.O = z2;
        s1 s1Var = this.B;
        o2 o2Var = this.G.f1142b;
        s1Var.g = z2;
        if (!s1Var.q(o2Var)) {
            P(true);
        }
        q(false);
    }

    public final void e(f2 f2Var) throws ExoPlaybackException {
        if (f2Var.getState() != 0) {
            a1 a1Var = this.f1004x;
            if (f2Var == a1Var.l) {
                a1Var.m = null;
                a1Var.l = null;
                a1Var.n = true;
            }
            if (f2Var.getState() == 2) {
                f2Var.stop();
            }
            f2Var.g();
            this.S--;
        }
    }

    public final void e0(k0 k0Var) throws ExoPlaybackException {
        this.H.a(1);
        u1 u1Var = this.C;
        int e2 = u1Var.e();
        if (k0Var.b() != e2) {
            k0Var = k0Var.h().f(0, e2);
        }
        u1Var.i = k0Var;
        r(u1Var.c(), false);
    }

    /* JADX WARN: Code restructure failed: missing block: B:275:0x0473, code lost:
        if (r36.o.f(m(), r36.f1004x.c().k, r36.L, r32) == false) goto L276;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void f() throws com.google.android.exoplayer2.ExoPlaybackException, java.io.IOException {
        /*
            Method dump skipped, instructions count: 1504
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.h1.f():void");
    }

    public final void f0(int i) {
        w1 w1Var = this.G;
        if (w1Var.f != i) {
            this.G = w1Var.f(i);
        }
    }

    public final void g() throws ExoPlaybackException {
        h(new boolean[this.j.length]);
    }

    public final boolean g0() {
        w1 w1Var = this.G;
        return w1Var.m && w1Var.n == 0;
    }

    public final void h(boolean[] zArr) throws ExoPlaybackException {
        s sVar;
        q1 q1Var = this.B.i;
        r rVar = q1Var.n;
        for (int i = 0; i < this.j.length; i++) {
            if (!rVar.b(i) && this.k.remove(this.j[i])) {
                this.j[i].reset();
            }
        }
        for (int i2 = 0; i2 < this.j.length; i2++) {
            if (rVar.b(i2)) {
                boolean z2 = zArr[i2];
                f2 f2Var = this.j[i2];
                if (!w(f2Var)) {
                    s1 s1Var = this.B;
                    q1 q1Var2 = s1Var.i;
                    boolean z3 = q1Var2 == s1Var.h;
                    r rVar2 = q1Var2.n;
                    h2 h2Var = rVar2.f908b[i2];
                    j1[] i3 = i(rVar2.c[i2]);
                    boolean z4 = g0() && this.G.f == 3;
                    boolean z5 = !z2 && z4;
                    this.S++;
                    this.k.add(f2Var);
                    f2Var.o(h2Var, i3, q1Var2.c[i2], this.U, z5, z3, q1Var2.e(), q1Var2.o);
                    f2Var.r(11, new g1(this));
                    a1 a1Var = this.f1004x;
                    Objects.requireNonNull(a1Var);
                    s w = f2Var.w();
                    if (!(w == null || w == (sVar = a1Var.m))) {
                        if (sVar == null) {
                            a1Var.m = w;
                            a1Var.l = f2Var;
                            w.i(a1Var.j.n);
                        } else {
                            throw ExoPlaybackException.b(new IllegalStateException("Multiple renderer media clocks enabled."), 1000);
                        }
                    }
                    if (z4) {
                        f2Var.start();
                    }
                } else {
                    continue;
                }
            }
        }
        q1Var.g = true;
    }

    public final boolean h0(o2 o2Var, a0.a aVar) {
        if (aVar.a() || o2Var.q()) {
            return false;
        }
        o2Var.n(o2Var.h(aVar.a, this.u).l, this.t);
        if (!this.t.c()) {
            return false;
        }
        o2.c cVar = this.t;
        return cVar.v && cVar.f1038s != -9223372036854775807L;
    }

    @Override // android.os.Handler.Callback
    public boolean handleMessage(Message message) {
        q1 q1Var;
        int i = 1000;
        try {
            switch (message.what) {
                case 0:
                    D();
                    break;
                case 1:
                    a0(message.arg1 != 0, message.arg2, true, 1);
                    break;
                case 2:
                    f();
                    break;
                case 3:
                    Q((g) message.obj);
                    break;
                case 4:
                    b0((x1) message.obj);
                    break;
                case 5:
                    this.F = (j2) message.obj;
                    break;
                case 6:
                    j0(false, true);
                    break;
                case 7:
                    E();
                    return true;
                case 8:
                    s((x) message.obj);
                    break;
                case 9:
                    o((x) message.obj);
                    break;
                case 10:
                    G();
                    break;
                case 11:
                    c0(message.arg1);
                    break;
                case 12:
                    d0(message.arg1 != 0);
                    break;
                case 13:
                    W(message.arg1 != 0, (AtomicBoolean) message.obj);
                    break;
                case 14:
                    b2 b2Var = (b2) message.obj;
                    Objects.requireNonNull(b2Var);
                    T(b2Var);
                    break;
                case 15:
                    U((b2) message.obj);
                    break;
                case 16:
                    x1 x1Var = (x1) message.obj;
                    t(x1Var, x1Var.k, true, false);
                    break;
                case 17:
                    X((a) message.obj);
                    break;
                case 18:
                    c((a) message.obj, message.arg1);
                    break;
                case 19:
                    C((b) message.obj);
                    break;
                case 20:
                    F(message.arg1, message.arg2, (k0) message.obj);
                    break;
                case 21:
                    e0((k0) message.obj);
                    break;
                case 22:
                    B();
                    break;
                case 23:
                    Z(message.arg1 != 0);
                    break;
                case 24:
                    Y(message.arg1 == 1);
                    break;
                case 25:
                    P(true);
                    break;
                default:
                    return false;
            }
        } catch (ExoPlaybackException e2) {
            e = e2;
            if (e.type == 1 && (q1Var = this.B.i) != null) {
                e = e.a(q1Var.f.a);
            }
            if (!e.isRecoverable || this.X != null) {
                ExoPlaybackException exoPlaybackException = this.X;
                if (exoPlaybackException != null) {
                    exoPlaybackException.addSuppressed(e);
                    e = this.X;
                }
                b.i.a.c.f3.q.b("ExoPlayerImplInternal", "Playback error", e);
                j0(true, false);
                this.G = this.G.e(e);
            } else {
                b.i.a.c.f3.q.c("ExoPlayerImplInternal", "Recoverable renderer error", e);
                this.X = e;
                o oVar = this.q;
                oVar.d(oVar.i(25, e));
            }
        } catch (ParserException e3) {
            int i2 = e3.dataType;
            if (i2 == 1) {
                i = e3.contentIsMalformed ? 3001 : 3003;
            } else if (i2 == 4) {
                i = e3.contentIsMalformed ? 3002 : 3004;
            }
            p(e3, i);
        } catch (DrmSession.DrmSessionException e4) {
            p(e4, e4.errorCode);
        } catch (BehindLiveWindowException e5) {
            p(e5, PointerIconCompat.TYPE_HAND);
        } catch (DataSourceException e6) {
            p(e6, e6.reason);
        } catch (IOException e7) {
            p(e7, 2000);
        } catch (RuntimeException e8) {
            if ((e8 instanceof IllegalStateException) || (e8 instanceof IllegalArgumentException)) {
                i = PointerIconCompat.TYPE_WAIT;
            }
            ExoPlaybackException b2 = ExoPlaybackException.b(e8, i);
            b.i.a.c.f3.q.b("ExoPlayerImplInternal", "Playback error", b2);
            j0(true, false);
            this.G = this.G.e(b2);
        }
        A();
        return true;
    }

    public final void i0() throws ExoPlaybackException {
        f2[] f2VarArr;
        this.L = false;
        a1 a1Var = this.f1004x;
        a1Var.o = true;
        a1Var.j.b();
        for (f2 f2Var : this.j) {
            if (w(f2Var)) {
                f2Var.start();
            }
        }
    }

    public final long j(o2 o2Var, Object obj, long j) {
        long j2;
        o2Var.n(o2Var.h(obj, this.u).l, this.t);
        o2.c cVar = this.t;
        if (cVar.f1038s != -9223372036854775807L && cVar.c()) {
            o2.c cVar2 = this.t;
            if (cVar2.v) {
                long j3 = cVar2.t;
                int i = e0.a;
                if (j3 == -9223372036854775807L) {
                    j2 = System.currentTimeMillis();
                } else {
                    j2 = j3 + SystemClock.elapsedRealtime();
                }
                return e0.B(j2 - this.t.f1038s) - (j + this.u.n);
            }
        }
        return -9223372036854775807L;
    }

    public final void j0(boolean z2, boolean z3) {
        H(z2 || !this.P, false, true, false);
        this.H.a(z3 ? 1 : 0);
        this.o.i();
        f0(1);
    }

    public final long k() {
        q1 q1Var = this.B.i;
        if (q1Var == null) {
            return 0L;
        }
        long j = q1Var.o;
        if (!q1Var.d) {
            return j;
        }
        int i = 0;
        while (true) {
            f2[] f2VarArr = this.j;
            if (i >= f2VarArr.length) {
                return j;
            }
            if (w(f2VarArr[i]) && this.j[i].h() == q1Var.c[i]) {
                long t = this.j[i].t();
                if (t == Long.MIN_VALUE) {
                    return Long.MIN_VALUE;
                }
                j = Math.max(t, j);
            }
            i++;
        }
    }

    public final void k0() throws ExoPlaybackException {
        f2[] f2VarArr;
        a1 a1Var = this.f1004x;
        a1Var.o = false;
        z zVar = a1Var.j;
        if (zVar.k) {
            zVar.a(zVar.e());
            zVar.k = false;
        }
        for (f2 f2Var : this.j) {
            if (w(f2Var) && f2Var.getState() == 2) {
                f2Var.stop();
            }
        }
    }

    public final Pair<a0.a, Long> l(o2 o2Var) {
        long j = 0;
        if (o2Var.q()) {
            a0.a aVar = w1.a;
            return Pair.create(w1.a, 0L);
        }
        Pair<Object, Long> j2 = o2Var.j(this.t, this.u, o2Var.a(this.O), -9223372036854775807L);
        a0.a o = this.B.o(o2Var, j2.first, 0L);
        long longValue = ((Long) j2.second).longValue();
        if (o.a()) {
            o2Var.h(o.a, this.u);
            if (o.c == this.u.d(o.f831b)) {
                j = this.u.p.o;
            }
            longValue = j;
        }
        return Pair.create(o, Long.valueOf(longValue));
    }

    public final void l0() {
        q1 q1Var = this.B.j;
        boolean z2 = this.M || (q1Var != null && q1Var.a.h());
        w1 w1Var = this.G;
        if (z2 != w1Var.h) {
            this.G = new w1(w1Var.f1142b, w1Var.c, w1Var.d, w1Var.e, w1Var.f, w1Var.g, z2, w1Var.i, w1Var.j, w1Var.k, w1Var.l, w1Var.m, w1Var.n, w1Var.o, w1Var.r, w1Var.f1143s, w1Var.t, w1Var.p, w1Var.q);
        }
    }

    public final long m() {
        return n(this.G.r);
    }

    public final void m0(o2 o2Var, a0.a aVar, o2 o2Var2, a0.a aVar2, long j) {
        if (o2Var.q() || !h0(o2Var, aVar)) {
            float f2 = this.f1004x.c().k;
            x1 x1Var = this.G.o;
            if (f2 != x1Var.k) {
                this.f1004x.i(x1Var);
                return;
            }
            return;
        }
        o2Var.n(o2Var.h(aVar.a, this.u).l, this.t);
        m1 m1Var = this.D;
        o1.g gVar = this.t.f1039x;
        int i = e0.a;
        y0 y0Var = (y0) m1Var;
        Objects.requireNonNull(y0Var);
        y0Var.d = e0.B(gVar.l);
        y0Var.g = e0.B(gVar.m);
        y0Var.h = e0.B(gVar.n);
        float f3 = gVar.o;
        if (f3 == -3.4028235E38f) {
            f3 = 0.97f;
        }
        y0Var.k = f3;
        float f4 = gVar.p;
        if (f4 == -3.4028235E38f) {
            f4 = 1.03f;
        }
        y0Var.j = f4;
        y0Var.a();
        if (j != -9223372036854775807L) {
            y0 y0Var2 = (y0) this.D;
            y0Var2.e = j(o2Var, aVar.a, j);
            y0Var2.a();
            return;
        }
        Object obj = this.t.n;
        Object obj2 = null;
        if (!o2Var2.q()) {
            obj2 = o2Var2.n(o2Var2.h(aVar2.a, this.u).l, this.t).n;
        }
        if (!e0.a(obj2, obj)) {
            y0 y0Var3 = (y0) this.D;
            y0Var3.e = -9223372036854775807L;
            y0Var3.a();
        }
    }

    public final long n(long j) {
        q1 q1Var = this.B.j;
        if (q1Var == null) {
            return 0L;
        }
        return Math.max(0L, j - (this.U - q1Var.o));
    }

    /* JADX WARN: Finally extract failed */
    /* JADX WARN: Removed duplicated region for block: B:155:0x018d A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:165:0x01c4 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:167:0x01bb A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:62:0x0135  */
    /* JADX WARN: Removed duplicated region for block: B:73:0x015c  */
    /* JADX WARN: Removed duplicated region for block: B:76:0x0168  */
    /* JADX WARN: Removed duplicated region for block: B:85:0x0182 A[LOOP:1: B:75:0x0166->B:85:0x0182, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:88:0x0192  */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:60:0x0130 -> B:61:0x0133). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:74:0x0165 -> B:75:0x0166). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void n0() throws com.google.android.exoplayer2.ExoPlaybackException {
        /*
            Method dump skipped, instructions count: 828
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.h1.n0():void");
    }

    public final void o(x xVar) {
        s1 s1Var = this.B;
        q1 q1Var = s1Var.j;
        if (q1Var != null && q1Var.a == xVar) {
            s1Var.m(this.U);
            z();
        }
    }

    public final void p(IOException iOException, int i) {
        ExoPlaybackException exoPlaybackException = new ExoPlaybackException(0, iOException, i);
        q1 q1Var = this.B.h;
        if (q1Var != null) {
            exoPlaybackException = exoPlaybackException.a(q1Var.f.a);
        }
        b.i.a.c.f3.q.b("ExoPlayerImplInternal", "Playback error", exoPlaybackException);
        j0(false, false);
        this.G = this.G.e(exoPlaybackException);
    }

    public final void q(boolean z2) {
        long j;
        q1 q1Var = this.B.j;
        a0.a aVar = q1Var == null ? this.G.c : q1Var.f.a;
        boolean z3 = !this.G.l.equals(aVar);
        if (z3) {
            this.G = this.G.a(aVar);
        }
        w1 w1Var = this.G;
        if (q1Var == null) {
            j = w1Var.t;
        } else {
            j = q1Var.d();
        }
        w1Var.r = j;
        this.G.f1143s = m();
        if ((z3 || z2) && q1Var != null && q1Var.d) {
            this.o.d(this.j, q1Var.m, q1Var.n.c);
        }
    }

    public final void r(o2 o2Var, boolean z2) throws ExoPlaybackException {
        long j;
        boolean z3;
        boolean z4;
        boolean z5;
        a0.a aVar;
        int i;
        int i2;
        long j2;
        Object obj;
        long j3;
        long j4;
        f fVar;
        long j5;
        int i3;
        Object obj2;
        boolean z6;
        int i4;
        int i5;
        long j6;
        boolean z7;
        boolean z8;
        long j7;
        boolean z9;
        boolean z10;
        int i6;
        int i7;
        boolean z11;
        long j8;
        Object obj3;
        boolean z12;
        Throwable th;
        boolean z13;
        boolean z14;
        w1 w1Var = this.G;
        g gVar = this.T;
        s1 s1Var = this.B;
        int i8 = this.N;
        boolean z15 = this.O;
        o2.c cVar = this.t;
        o2.b bVar = this.u;
        if (o2Var.q()) {
            a0.a aVar2 = w1.a;
            fVar = new f(w1.a, 0L, -9223372036854775807L, false, true, false);
        } else {
            a0.a aVar3 = w1Var.c;
            Object obj4 = aVar3.a;
            boolean y2 = y(w1Var, bVar);
            if (w1Var.c.a() || y2) {
                j = w1Var.d;
            } else {
                j = w1Var.t;
            }
            long j9 = j;
            if (gVar != null) {
                Object obj5 = obj4;
                Pair<Object, Long> M = M(o2Var, gVar, true, i8, z15, cVar, bVar);
                if (M == null) {
                    i6 = o2Var.a(z15);
                    j7 = j9;
                    z10 = false;
                    z9 = false;
                    z8 = true;
                } else {
                    if (gVar.c == -9223372036854775807L) {
                        i7 = o2Var.h(M.first, bVar).l;
                        j8 = j9;
                        obj3 = obj5;
                        z11 = false;
                    } else {
                        Object obj6 = M.first;
                        j8 = ((Long) M.second).longValue();
                        obj3 = obj6;
                        z11 = true;
                        i7 = -1;
                    }
                    obj5 = obj3;
                    i6 = i7;
                    z8 = false;
                    j7 = j8;
                    z9 = w1Var.f == 4;
                    z10 = z11;
                }
                z3 = z10;
                z5 = z9;
                j2 = j7;
                z4 = z8;
                aVar = aVar3;
                i = -1;
                i2 = i6;
                obj = obj5;
            } else {
                if (w1Var.f1142b.q()) {
                    i5 = o2Var.a(z15);
                    obj2 = obj4;
                } else if (o2Var.b(obj4) == -1) {
                    obj2 = obj4;
                    Object N = N(cVar, bVar, i8, z15, obj4, w1Var.f1142b, o2Var);
                    if (N == null) {
                        i4 = o2Var.a(z15);
                        z7 = true;
                    } else {
                        i4 = o2Var.h(N, bVar).l;
                        z7 = false;
                    }
                    z6 = z7;
                    aVar = aVar3;
                    i2 = i4;
                    z4 = z6;
                    obj = obj2;
                    j2 = j9;
                    i = -1;
                    z5 = false;
                    z3 = false;
                } else {
                    obj2 = obj4;
                    if (j9 == -9223372036854775807L) {
                        i5 = o2Var.h(obj2, bVar).l;
                    } else if (y2) {
                        aVar = aVar3;
                        w1Var.f1142b.h(aVar.a, bVar);
                        if (w1Var.f1142b.n(bVar.l, cVar).B == w1Var.f1142b.b(aVar.a)) {
                            Pair<Object, Long> j10 = o2Var.j(cVar, bVar, o2Var.h(obj2, bVar).l, j9 + bVar.n);
                            Object obj7 = j10.first;
                            j6 = ((Long) j10.second).longValue();
                            obj = obj7;
                        } else {
                            obj = obj2;
                            j6 = j9;
                        }
                        j2 = j6;
                        i2 = -1;
                        i = -1;
                        z5 = false;
                        z4 = false;
                        z3 = true;
                    } else {
                        aVar = aVar3;
                        i5 = -1;
                        i4 = i5;
                        z6 = false;
                        i2 = i4;
                        z4 = z6;
                        obj = obj2;
                        j2 = j9;
                        i = -1;
                        z5 = false;
                        z3 = false;
                    }
                }
                aVar = aVar3;
                i4 = i5;
                z6 = false;
                i2 = i4;
                z4 = z6;
                obj = obj2;
                j2 = j9;
                i = -1;
                z5 = false;
                z3 = false;
            }
            if (i2 != i) {
                Pair<Object, Long> j11 = o2Var.j(cVar, bVar, i2, -9223372036854775807L);
                Object obj8 = j11.first;
                j2 = ((Long) j11.second).longValue();
                obj = obj8;
                j3 = -9223372036854775807L;
            } else {
                j3 = j2;
            }
            a0.a o = s1Var.o(o2Var, obj, j2);
            boolean z16 = o.e == -1 || ((i3 = aVar.e) != -1 && o.f831b >= i3);
            boolean equals = aVar.a.equals(obj);
            boolean z17 = equals && !aVar.a() && !o.a() && z16;
            o2Var.h(obj, bVar);
            boolean z18 = equals && !y2 && j9 == j3 && ((o.a() && bVar.e(o.f831b)) || (aVar.a() && bVar.e(aVar.f831b)));
            if (z17 || z18) {
                o = aVar;
            }
            if (o.a()) {
                if (o.equals(aVar)) {
                    j5 = w1Var.t;
                } else {
                    o2Var.h(o.a, bVar);
                    j5 = o.c == bVar.d(o.f831b) ? bVar.p.o : 0L;
                }
                j4 = j5;
            } else {
                j4 = j2;
            }
            fVar = new f(o, j4, j3, z5, z4, z3);
        }
        f fVar2 = fVar;
        a0.a aVar4 = fVar2.a;
        long j12 = fVar2.c;
        boolean z19 = fVar2.d;
        long j13 = fVar2.f1009b;
        boolean z20 = !this.G.c.equals(aVar4) || j13 != this.G.t;
        int i9 = 3;
        g gVar2 = null;
        try {
            if (fVar2.e) {
                if (this.G.f != 1) {
                    f0(4);
                }
                H(false, false, false, true);
            }
            try {
                if (!z20) {
                    try {
                        z13 = false;
                        z14 = true;
                        if (!this.B.r(o2Var, this.U, k())) {
                            P(false);
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        z12 = true;
                        gVar2 = null;
                        w1 w1Var2 = this.G;
                        g gVar3 = gVar2;
                        m0(o2Var, aVar4, w1Var2.f1142b, w1Var2.c, fVar2.f ? j13 : -9223372036854775807L);
                        if (z20 || j12 != this.G.d) {
                            w1 w1Var3 = this.G;
                            Object obj9 = w1Var3.c.a;
                            o2 o2Var2 = w1Var3.f1142b;
                            if (!z20 || !z2 || o2Var2.q() || o2Var2.h(obj9, this.u).o) {
                                z12 = false;
                            }
                            long j14 = this.G.e;
                            if (o2Var.b(obj9) == -1) {
                                i9 = 4;
                            }
                            this.G = u(aVar4, j13, j12, j14, z12, i9);
                        }
                        I();
                        L(o2Var, this.G.f1142b);
                        this.G = this.G.g(o2Var);
                        if (!o2Var.q()) {
                            this.T = gVar3;
                        }
                        q(false);
                        throw th;
                    }
                } else {
                    z13 = false;
                    z14 = true;
                    if (!o2Var.q()) {
                        for (q1 q1Var = this.B.h; q1Var != null; q1Var = q1Var.l) {
                            if (q1Var.f.a.equals(aVar4)) {
                                q1Var.f = this.B.h(o2Var, q1Var.f);
                                q1Var.j();
                            }
                        }
                        j13 = R(aVar4, j13, z19);
                    }
                }
                w1 w1Var4 = this.G;
                m0(o2Var, aVar4, w1Var4.f1142b, w1Var4.c, fVar2.f ? j13 : -9223372036854775807L);
                if (z20 || j12 != this.G.d) {
                    w1 w1Var5 = this.G;
                    Object obj10 = w1Var5.c.a;
                    o2 o2Var3 = w1Var5.f1142b;
                    if (!z20 || !z2 || o2Var3.q() || o2Var3.h(obj10, this.u).o) {
                        z14 = false;
                    }
                    long j15 = this.G.e;
                    if (o2Var.b(obj10) == -1) {
                        i9 = 4;
                    }
                    this.G = u(aVar4, j13, j12, j15, z14, i9);
                }
                I();
                L(o2Var, this.G.f1142b);
                this.G = this.G.g(o2Var);
                if (!o2Var.q()) {
                    this.T = null;
                }
                q(z13);
            } catch (Throwable th3) {
                th = th3;
            }
        } catch (Throwable th4) {
            th = th4;
            z12 = true;
        }
    }

    public final void s(x xVar) throws ExoPlaybackException {
        q1 q1Var = this.B.j;
        if (q1Var != null && q1Var.a == xVar) {
            float f2 = this.f1004x.c().k;
            o2 o2Var = this.G.f1142b;
            q1Var.d = true;
            q1Var.m = q1Var.a.n();
            r i = q1Var.i(f2, o2Var);
            r1 r1Var = q1Var.f;
            long j = r1Var.f1054b;
            long j2 = r1Var.e;
            if (j2 != -9223372036854775807L && j >= j2) {
                j = Math.max(0L, j2 - 1);
            }
            long a2 = q1Var.a(i, j, false, new boolean[q1Var.i.length]);
            long j3 = q1Var.o;
            r1 r1Var2 = q1Var.f;
            q1Var.o = (r1Var2.f1054b - a2) + j3;
            q1Var.f = r1Var2.b(a2);
            this.o.d(this.j, q1Var.m, q1Var.n.c);
            if (q1Var == this.B.h) {
                J(q1Var.f.f1054b);
                g();
                w1 w1Var = this.G;
                a0.a aVar = w1Var.c;
                long j4 = q1Var.f.f1054b;
                this.G = u(aVar, j4, w1Var.d, j4, false, 5);
            }
            z();
        }
    }

    public final void t(x1 x1Var, float f2, boolean z2, boolean z3) throws ExoPlaybackException {
        int i;
        h1 h1Var = this;
        if (z2) {
            if (z3) {
                h1Var.H.a(1);
            }
            w1 w1Var = h1Var.G;
            h1Var = this;
            h1Var.G = new w1(w1Var.f1142b, w1Var.c, w1Var.d, w1Var.e, w1Var.f, w1Var.g, w1Var.h, w1Var.i, w1Var.j, w1Var.k, w1Var.l, w1Var.m, w1Var.n, x1Var, w1Var.r, w1Var.f1143s, w1Var.t, w1Var.p, w1Var.q);
        }
        float f3 = x1Var.k;
        q1 q1Var = h1Var.B.h;
        while (true) {
            i = 0;
            if (q1Var == null) {
                break;
            }
            j[] jVarArr = q1Var.n.c;
            int length = jVarArr.length;
            while (i < length) {
                j jVar = jVarArr[i];
                if (jVar != null) {
                    jVar.i(f3);
                }
                i++;
            }
            q1Var = q1Var.l;
        }
        f2[] f2VarArr = h1Var.j;
        int length2 = f2VarArr.length;
        while (i < length2) {
            f2 f2Var = f2VarArr[i];
            if (f2Var != null) {
                f2Var.n(f2, x1Var.k);
            }
            i++;
        }
    }

    @CheckResult
    public final w1 u(a0.a aVar, long j, long j2, long j3, boolean z2, int i) {
        p<Object> pVar;
        r rVar;
        o0 o0Var;
        r rVar2;
        p<Object> pVar2;
        o0 o0Var2;
        int i2 = 0;
        this.W = this.W || j != this.G.t || !aVar.equals(this.G.c);
        I();
        w1 w1Var = this.G;
        o0 o0Var3 = w1Var.i;
        r rVar3 = w1Var.j;
        List<Metadata> list = w1Var.k;
        if (this.C.j) {
            q1 q1Var = this.B.h;
            if (q1Var == null) {
                o0Var = o0.j;
            } else {
                o0Var = q1Var.m;
            }
            if (q1Var == null) {
                rVar2 = this.n;
            } else {
                rVar2 = q1Var.n;
            }
            j[] jVarArr = rVar2.c;
            b.i.a.f.e.o.f.A(4, "initialCapacity");
            Object[] objArr = new Object[4];
            int length = jVarArr.length;
            int i3 = 0;
            boolean z3 = false;
            int i4 = 0;
            while (i3 < length) {
                j jVar = jVarArr[i3];
                if (jVar != null) {
                    Metadata metadata = jVar.d(i2).u;
                    if (metadata == null) {
                        o0Var2 = o0Var;
                        Metadata metadata2 = new Metadata(new Metadata.Entry[i2]);
                        int i5 = i4 + 1;
                        if (objArr.length < i5) {
                            objArr = Arrays.copyOf(objArr, n.b.a(objArr.length, i5));
                        }
                        objArr[i4] = metadata2;
                        i4 = i5;
                    } else {
                        o0Var2 = o0Var;
                        int i6 = i4 + 1;
                        if (objArr.length < i6) {
                            objArr = Arrays.copyOf(objArr, n.b.a(objArr.length, i6));
                        }
                        objArr[i4] = metadata;
                        i4 = i6;
                        z3 = true;
                    }
                } else {
                    o0Var2 = o0Var;
                }
                i3++;
                o0Var = o0Var2;
                i2 = 0;
            }
            o0Var3 = o0Var;
            if (z3) {
                pVar2 = p.l(objArr, i4);
            } else {
                b.i.b.b.a<Object> aVar2 = p.k;
                pVar2 = h0.l;
            }
            if (q1Var != null) {
                r1 r1Var = q1Var.f;
                if (r1Var.c != j2) {
                    q1Var.f = r1Var.a(j2);
                }
            }
            pVar = pVar2;
            rVar = rVar2;
        } else if (!aVar.equals(w1Var.c)) {
            o0 o0Var4 = o0.j;
            r rVar4 = this.n;
            b.i.b.b.a<Object> aVar3 = p.k;
            pVar = h0.l;
            o0Var3 = o0Var4;
            rVar = rVar4;
        } else {
            rVar = rVar3;
            pVar = list;
        }
        if (z2) {
            d dVar = this.H;
            if (!dVar.d || dVar.e == 5) {
                dVar.a = true;
                dVar.d = true;
                dVar.e = i;
            } else {
                b.c.a.a0.d.j(i == 5);
            }
        }
        return this.G.b(aVar, j, j2, j3, m(), o0Var3, rVar, pVar);
    }

    public final boolean v() {
        q1 q1Var = this.B.j;
        if (q1Var == null) {
            return false;
        }
        return (!q1Var.d ? 0L : q1Var.a.c()) != Long.MIN_VALUE;
    }

    public final boolean x() {
        q1 q1Var = this.B.h;
        long j = q1Var.f.e;
        return q1Var.d && (j == -9223372036854775807L || this.G.t < j || !g0());
    }

    public final void z() {
        boolean z2;
        long j;
        long j2;
        if (!v()) {
            z2 = false;
        } else {
            q1 q1Var = this.B.j;
            long n = n(!q1Var.d ? 0L : q1Var.a.c());
            if (q1Var == this.B.h) {
                j2 = this.U;
                j = q1Var.o;
            } else {
                j2 = this.U - q1Var.o;
                j = q1Var.f.f1054b;
            }
            z2 = this.o.g(j2 - j, n, this.f1004x.c().k);
        }
        this.M = z2;
        if (z2) {
            q1 q1Var2 = this.B.j;
            long j3 = this.U;
            b.c.a.a0.d.D(q1Var2.g());
            q1Var2.a.g(j3 - q1Var2.o);
        }
        l0();
    }
}
