package b.i.a.c;

import androidx.annotation.Nullable;
import b.i.a.c.f3.g;
import b.i.a.c.f3.s;
import b.i.a.c.f3.z;
import java.util.Objects;
/* compiled from: DefaultMediaClock.java */
/* loaded from: classes3.dex */
public final class a1 implements s {
    public final z j;
    public final a k;
    @Nullable
    public f2 l;
    @Nullable
    public s m;
    public boolean n = true;
    public boolean o;

    /* compiled from: DefaultMediaClock.java */
    /* loaded from: classes3.dex */
    public interface a {
    }

    public a1(a aVar, g gVar) {
        this.k = aVar;
        this.j = new z(gVar);
    }

    @Override // b.i.a.c.f3.s
    public x1 c() {
        s sVar = this.m;
        if (sVar != null) {
            return sVar.c();
        }
        return this.j.n;
    }

    @Override // b.i.a.c.f3.s
    public long e() {
        if (this.n) {
            return this.j.e();
        }
        s sVar = this.m;
        Objects.requireNonNull(sVar);
        return sVar.e();
    }

    @Override // b.i.a.c.f3.s
    public void i(x1 x1Var) {
        s sVar = this.m;
        if (sVar != null) {
            sVar.i(x1Var);
            x1Var = this.m.c();
        }
        this.j.i(x1Var);
    }
}
