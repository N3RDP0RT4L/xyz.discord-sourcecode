package b.i.a.c;

import android.content.Context;
import android.net.wifi.WifiManager;
import androidx.annotation.Nullable;
/* compiled from: WifiLockManager.java */
/* loaded from: classes3.dex */
public final class r2 {
    @Nullable
    public final WifiManager a;
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public WifiManager.WifiLock f1055b;
    public boolean c;
    public boolean d;

    public r2(Context context) {
        this.a = (WifiManager) context.getApplicationContext().getSystemService("wifi");
    }

    public final void a() {
        WifiManager.WifiLock wifiLock = this.f1055b;
        if (wifiLock != null) {
            if (!this.c || !this.d) {
                wifiLock.release();
            } else {
                wifiLock.acquire();
            }
        }
    }
}
