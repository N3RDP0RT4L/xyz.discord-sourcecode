package b.i.a.c.b3.t;

import b.i.b.b.r;
import java.util.regex.Pattern;
/* compiled from: TextEmphasis.java */
/* loaded from: classes3.dex */
public final class b {
    public static final Pattern a = Pattern.compile("\\s+");

    /* renamed from: b  reason: collision with root package name */
    public static final r<String> f875b = r.l(2, "auto", "none");
    public static final r<String> c = r.r("dot", "sesame", "circle");
    public static final r<String> d = r.l(2, "filled", "open");
    public static final r<String> e = r.r("after", "before", "outside");
    public final int f;
    public final int g;
    public final int h;

    static {
        int i = r.k;
    }

    public b(int i, int i2, int i3) {
        this.f = i;
        this.g = i2;
        this.h = i3;
    }
}
