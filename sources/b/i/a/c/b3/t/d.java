package b.i.a.c.b3.t;

import android.text.SpannableStringBuilder;
import android.util.Pair;
import androidx.annotation.Nullable;
import b.i.a.c.b3.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeSet;
/* compiled from: TtmlNode.java */
/* loaded from: classes3.dex */
public final class d {
    @Nullable
    public final String a;
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public final String f879b;
    public final boolean c;
    public final long d;
    public final long e;
    @Nullable
    public final f f;
    @Nullable
    public final String[] g;
    public final String h;
    @Nullable
    public final String i;
    @Nullable
    public final d j;
    public final HashMap<String, Integer> k;
    public final HashMap<String, Integer> l;
    public List<d> m;

    public d(@Nullable String str, @Nullable String str2, long j, long j2, @Nullable f fVar, @Nullable String[] strArr, String str3, @Nullable String str4, @Nullable d dVar) {
        this.a = str;
        this.f879b = str2;
        this.i = str4;
        this.f = fVar;
        this.g = strArr;
        this.c = str2 != null;
        this.d = j;
        this.e = j2;
        Objects.requireNonNull(str3);
        this.h = str3;
        this.j = dVar;
        this.k = new HashMap<>();
        this.l = new HashMap<>();
    }

    public static d b(String str) {
        return new d(null, str.replaceAll("\r\n", "\n").replaceAll(" *\n *", "\n").replaceAll("\n", " ").replaceAll("[ \t\\x0B\f\r]+", " "), -9223372036854775807L, -9223372036854775807L, null, null, "", null, null);
    }

    public static SpannableStringBuilder f(String str, Map<String, b.C0092b> map) {
        if (!map.containsKey(str)) {
            b.C0092b bVar = new b.C0092b();
            bVar.a = new SpannableStringBuilder();
            map.put(str, bVar);
        }
        CharSequence charSequence = map.get(str).a;
        Objects.requireNonNull(charSequence);
        return (SpannableStringBuilder) charSequence;
    }

    public void a(d dVar) {
        if (this.m == null) {
            this.m = new ArrayList();
        }
        this.m.add(dVar);
    }

    public d c(int i) {
        List<d> list = this.m;
        if (list != null) {
            return list.get(i);
        }
        throw new IndexOutOfBoundsException();
    }

    public int d() {
        List<d> list = this.m;
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    public final void e(TreeSet<Long> treeSet, boolean z2) {
        boolean equals = "p".equals(this.a);
        boolean equals2 = "div".equals(this.a);
        if (z2 || equals || (equals2 && this.i != null)) {
            long j = this.d;
            if (j != -9223372036854775807L) {
                treeSet.add(Long.valueOf(j));
            }
            long j2 = this.e;
            if (j2 != -9223372036854775807L) {
                treeSet.add(Long.valueOf(j2));
            }
        }
        if (this.m != null) {
            for (int i = 0; i < this.m.size(); i++) {
                this.m.get(i).e(treeSet, z2 || equals);
            }
        }
    }

    public boolean g(long j) {
        long j2 = this.d;
        return (j2 == -9223372036854775807L && this.e == -9223372036854775807L) || (j2 <= j && this.e == -9223372036854775807L) || ((j2 == -9223372036854775807L && j < this.e) || (j2 <= j && j < this.e));
    }

    public final void h(long j, String str, List<Pair<String, String>> list) {
        if (!"".equals(this.h)) {
            str = this.h;
        }
        if (!g(j) || !"div".equals(this.a) || this.i == null) {
            for (int i = 0; i < d(); i++) {
                c(i).h(j, str, list);
            }
            return;
        }
        list.add(new Pair<>(str, this.i));
    }

    /* JADX WARN: Removed duplicated region for block: B:117:0x01eb  */
    /* JADX WARN: Removed duplicated region for block: B:119:0x01ee  */
    /* JADX WARN: Removed duplicated region for block: B:122:0x01fe  */
    /* JADX WARN: Removed duplicated region for block: B:127:0x021f  */
    /* JADX WARN: Removed duplicated region for block: B:130:0x0237  */
    /* JADX WARN: Removed duplicated region for block: B:151:0x025a A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void i(long r18, java.util.Map<java.lang.String, b.i.a.c.b3.t.f> r20, java.util.Map<java.lang.String, b.i.a.c.b3.t.e> r21, java.lang.String r22, java.util.Map<java.lang.String, b.i.a.c.b3.b.C0092b> r23) {
        /*
            Method dump skipped, instructions count: 638
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.b3.t.d.i(long, java.util.Map, java.util.Map, java.lang.String, java.util.Map):void");
    }

    public final void j(long j, boolean z2, String str, Map<String, b.C0092b> map) {
        this.k.clear();
        this.l.clear();
        if (!"metadata".equals(this.a)) {
            if (!"".equals(this.h)) {
                str = this.h;
            }
            if (this.c && z2) {
                SpannableStringBuilder f = f(str, map);
                String str2 = this.f879b;
                Objects.requireNonNull(str2);
                f.append((CharSequence) str2);
            } else if ("br".equals(this.a) && z2) {
                f(str, map).append('\n');
            } else if (g(j)) {
                for (Map.Entry<String, b.C0092b> entry : map.entrySet()) {
                    CharSequence charSequence = entry.getValue().a;
                    Objects.requireNonNull(charSequence);
                    this.k.put(entry.getKey(), Integer.valueOf(charSequence.length()));
                }
                boolean equals = "p".equals(this.a);
                for (int i = 0; i < d(); i++) {
                    c(i).j(j, z2 || equals, str, map);
                }
                if (equals) {
                    SpannableStringBuilder f2 = f(str, map);
                    int length = f2.length();
                    do {
                        length--;
                        if (length < 0) {
                            break;
                        }
                    } while (f2.charAt(length) == ' ');
                    if (length >= 0 && f2.charAt(length) != '\n') {
                        f2.append('\n');
                    }
                }
                for (Map.Entry<String, b.C0092b> entry2 : map.entrySet()) {
                    CharSequence charSequence2 = entry2.getValue().a;
                    Objects.requireNonNull(charSequence2);
                    this.l.put(entry2.getKey(), Integer.valueOf(charSequence2.length()));
                }
            }
        }
    }
}
