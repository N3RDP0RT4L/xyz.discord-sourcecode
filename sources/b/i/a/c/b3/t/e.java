package b.i.a.c.b3.t;
/* compiled from: TtmlRegion.java */
/* loaded from: classes3.dex */
public final class e {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final float f880b;
    public final float c;
    public final int d;
    public final int e;
    public final float f;
    public final float g;
    public final int h;
    public final float i;
    public final int j;

    public e(String str, float f, float f2, int i, int i2, float f3, float f4, int i3, float f5, int i4) {
        this.a = str;
        this.f880b = f;
        this.c = f2;
        this.d = i;
        this.e = i2;
        this.f = f3;
        this.g = f4;
        this.h = i3;
        this.i = f5;
        this.j = i4;
    }
}
