package b.i.a.c.b3.t;

import android.text.Layout;
import android.util.Log;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.b3.f;
import b.i.a.c.b3.g;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.q;
import com.discord.models.domain.ModelAuditLogEntry;
import com.google.android.exoplayer2.text.SubtitleDecoderException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
/* compiled from: TtmlDecoder.java */
/* loaded from: classes3.dex */
public final class c extends f {
    public static final Pattern n = Pattern.compile("^([0-9][0-9]+):([0-9][0-9]):([0-9][0-9])(?:(\\.[0-9]+)|:([0-9][0-9])(?:\\.([0-9]+))?)?$");
    public static final Pattern o = Pattern.compile("^([0-9]+(?:\\.[0-9]+)?)(h|m|s|ms|f|t)$");
    public static final Pattern p = Pattern.compile("^(([0-9]*.)?[0-9]+)(px|em|%)$");
    public static final Pattern q = Pattern.compile("^([-+]?\\d+\\.?\\d*?)%$");
    public static final Pattern r = Pattern.compile("^(\\d+\\.?\\d*?)% (\\d+\\.?\\d*?)%$");

    /* renamed from: s  reason: collision with root package name */
    public static final Pattern f876s = Pattern.compile("^(\\d+\\.?\\d*?)px (\\d+\\.?\\d*?)px$");
    public static final Pattern t = Pattern.compile("^(\\d+) (\\d+)$");
    public static final b u = new b(30.0f, 1, 1);
    public static final a v = new a(32, 15);
    public final XmlPullParserFactory w;

    /* compiled from: TtmlDecoder.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public final int a;

        public a(int i, int i2) {
            this.a = i2;
        }
    }

    /* compiled from: TtmlDecoder.java */
    /* loaded from: classes3.dex */
    public static final class b {
        public final float a;

        /* renamed from: b  reason: collision with root package name */
        public final int f877b;
        public final int c;

        public b(float f, int i, int i2) {
            this.a = f;
            this.f877b = i;
            this.c = i2;
        }
    }

    /* compiled from: TtmlDecoder.java */
    /* renamed from: b.i.a.c.b3.t.c$c  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0096c {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final int f878b;

        public C0096c(int i, int i2) {
            this.a = i;
            this.f878b = i2;
        }
    }

    public c() {
        super("TtmlDecoder");
        try {
            XmlPullParserFactory newInstance = XmlPullParserFactory.newInstance();
            this.w = newInstance;
            newInstance.setNamespaceAware(true);
        } catch (XmlPullParserException e) {
            throw new RuntimeException("Couldn't create XmlPullParserFactory instance", e);
        }
    }

    public static f k(@Nullable f fVar) {
        return fVar == null ? new f() : fVar;
    }

    public static boolean l(String str) {
        return str.equals("tt") || str.equals("head") || str.equals("body") || str.equals("div") || str.equals("p") || str.equals("span") || str.equals("br") || str.equals("style") || str.equals("styling") || str.equals("layout") || str.equals(ModelAuditLogEntry.CHANGE_KEY_REGION) || str.equals("metadata") || str.equals("image") || str.equals("data") || str.equals("information");
    }

    @Nullable
    public static Layout.Alignment m(String str) {
        String u1 = b.i.a.f.e.o.f.u1(str);
        u1.hashCode();
        char c = 65535;
        switch (u1.hashCode()) {
            case -1364013995:
                if (u1.equals("center")) {
                    c = 0;
                    break;
                }
                break;
            case 100571:
                if (u1.equals("end")) {
                    c = 1;
                    break;
                }
                break;
            case 3317767:
                if (u1.equals("left")) {
                    c = 2;
                    break;
                }
                break;
            case 108511772:
                if (u1.equals("right")) {
                    c = 3;
                    break;
                }
                break;
            case 109757538:
                if (u1.equals("start")) {
                    c = 4;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return Layout.Alignment.ALIGN_CENTER;
            case 1:
            case 3:
                return Layout.Alignment.ALIGN_OPPOSITE;
            case 2:
            case 4:
                return Layout.Alignment.ALIGN_NORMAL;
            default:
                return null;
        }
    }

    public static a n(XmlPullParser xmlPullParser, a aVar) throws SubtitleDecoderException {
        String attributeValue = xmlPullParser.getAttributeValue("http://www.w3.org/ns/ttml#parameter", "cellResolution");
        if (attributeValue == null) {
            return aVar;
        }
        Matcher matcher = t.matcher(attributeValue);
        if (!matcher.matches()) {
            Log.w("TtmlDecoder", attributeValue.length() != 0 ? "Ignoring malformed cell resolution: ".concat(attributeValue) : new String("Ignoring malformed cell resolution: "));
            return aVar;
        }
        try {
            String group = matcher.group(1);
            Objects.requireNonNull(group);
            int parseInt = Integer.parseInt(group);
            String group2 = matcher.group(2);
            Objects.requireNonNull(group2);
            int parseInt2 = Integer.parseInt(group2);
            if (parseInt != 0 && parseInt2 != 0) {
                return new a(parseInt, parseInt2);
            }
            StringBuilder sb = new StringBuilder(47);
            sb.append("Invalid cell resolution ");
            sb.append(parseInt);
            sb.append(" ");
            sb.append(parseInt2);
            throw new SubtitleDecoderException(sb.toString());
        } catch (NumberFormatException unused) {
            Log.w("TtmlDecoder", attributeValue.length() != 0 ? "Ignoring malformed cell resolution: ".concat(attributeValue) : new String("Ignoring malformed cell resolution: "));
            return aVar;
        }
    }

    public static void o(String str, f fVar) throws SubtitleDecoderException {
        Matcher matcher;
        int i = e0.a;
        String[] split = str.split("\\s+", -1);
        if (split.length == 1) {
            matcher = p.matcher(str);
        } else if (split.length == 2) {
            matcher = p.matcher(split[1]);
            Log.w("TtmlDecoder", "Multiple values in fontSize attribute. Picking the second value for vertical font size and ignoring the first.");
        } else {
            int length = split.length;
            StringBuilder sb = new StringBuilder(52);
            sb.append("Invalid number of entries for fontSize: ");
            sb.append(length);
            sb.append(".");
            throw new SubtitleDecoderException(sb.toString());
        }
        if (matcher.matches()) {
            String group = matcher.group(3);
            Objects.requireNonNull(group);
            group.hashCode();
            char c = 65535;
            switch (group.hashCode()) {
                case 37:
                    if (group.equals("%")) {
                        c = 0;
                        break;
                    }
                    break;
                case 3240:
                    if (group.equals("em")) {
                        c = 1;
                        break;
                    }
                    break;
                case 3592:
                    if (group.equals("px")) {
                        c = 2;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    fVar.j = 3;
                    break;
                case 1:
                    fVar.j = 2;
                    break;
                case 2:
                    fVar.j = 1;
                    break;
                default:
                    throw new SubtitleDecoderException(b.d.b.a.a.j(group.length() + 30, "Invalid unit for fontSize: '", group, "'."));
            }
            String group2 = matcher.group(1);
            Objects.requireNonNull(group2);
            fVar.k = Float.parseFloat(group2);
            return;
        }
        throw new SubtitleDecoderException(b.d.b.a.a.j(str.length() + 36, "Invalid expression for fontSize: '", str, "'."));
    }

    public static b p(XmlPullParser xmlPullParser) throws SubtitleDecoderException {
        String attributeValue = xmlPullParser.getAttributeValue("http://www.w3.org/ns/ttml#parameter", "frameRate");
        int parseInt = attributeValue != null ? Integer.parseInt(attributeValue) : 30;
        float f = 1.0f;
        String attributeValue2 = xmlPullParser.getAttributeValue("http://www.w3.org/ns/ttml#parameter", "frameRateMultiplier");
        if (attributeValue2 != null) {
            int i = e0.a;
            String[] split = attributeValue2.split(" ", -1);
            if (split.length == 2) {
                f = Integer.parseInt(split[0]) / Integer.parseInt(split[1]);
            } else {
                throw new SubtitleDecoderException("frameRateMultiplier doesn't have 2 parts");
            }
        }
        b bVar = u;
        int i2 = bVar.f877b;
        String attributeValue3 = xmlPullParser.getAttributeValue("http://www.w3.org/ns/ttml#parameter", "subFrameRate");
        if (attributeValue3 != null) {
            i2 = Integer.parseInt(attributeValue3);
        }
        int i3 = bVar.c;
        String attributeValue4 = xmlPullParser.getAttributeValue("http://www.w3.org/ns/ttml#parameter", "tickRate");
        if (attributeValue4 != null) {
            i3 = Integer.parseInt(attributeValue4);
        }
        return new b(parseInt * f, i2, i3);
    }

    /* JADX WARN: Removed duplicated region for block: B:118:0x026b  */
    /* JADX WARN: Removed duplicated region for block: B:71:0x01ae  */
    /* JADX WARN: Removed duplicated region for block: B:88:0x01eb A[ADDED_TO_REGION] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static java.util.Map<java.lang.String, b.i.a.c.b3.t.f> q(org.xmlpull.v1.XmlPullParser r18, java.util.Map<java.lang.String, b.i.a.c.b3.t.f> r19, b.i.a.c.b3.t.c.a r20, @androidx.annotation.Nullable b.i.a.c.b3.t.c.C0096c r21, java.util.Map<java.lang.String, b.i.a.c.b3.t.e> r22, java.util.Map<java.lang.String, java.lang.String> r23) throws java.io.IOException, org.xmlpull.v1.XmlPullParserException {
        /*
            Method dump skipped, instructions count: 684
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.b3.t.c.q(org.xmlpull.v1.XmlPullParser, java.util.Map, b.i.a.c.b3.t.c$a, b.i.a.c.b3.t.c$c, java.util.Map, java.util.Map):java.util.Map");
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    public static d r(XmlPullParser xmlPullParser, @Nullable d dVar, Map<String, e> map, b bVar) throws SubtitleDecoderException {
        long j;
        long j2;
        char c;
        XmlPullParser xmlPullParser2 = xmlPullParser;
        int attributeCount = xmlPullParser.getAttributeCount();
        f s2 = s(xmlPullParser2, null);
        String str = "";
        String str2 = null;
        long j3 = -9223372036854775807L;
        long j4 = -9223372036854775807L;
        long j5 = -9223372036854775807L;
        String[] strArr = null;
        int i = 0;
        while (i < attributeCount) {
            String attributeName = xmlPullParser2.getAttributeName(i);
            String attributeValue = xmlPullParser2.getAttributeValue(i);
            attributeName.hashCode();
            switch (attributeName.hashCode()) {
                case -934795532:
                    if (attributeName.equals(ModelAuditLogEntry.CHANGE_KEY_REGION)) {
                        c = 0;
                        break;
                    }
                    c = 65535;
                    break;
                case 99841:
                    if (attributeName.equals("dur")) {
                        c = 1;
                        break;
                    }
                    c = 65535;
                    break;
                case 100571:
                    if (attributeName.equals("end")) {
                        c = 2;
                        break;
                    }
                    c = 65535;
                    break;
                case 93616297:
                    if (attributeName.equals("begin")) {
                        c = 3;
                        break;
                    }
                    c = 65535;
                    break;
                case 109780401:
                    if (attributeName.equals("style")) {
                        c = 4;
                        break;
                    }
                    c = 65535;
                    break;
                case 1292595405:
                    if (attributeName.equals("backgroundImage")) {
                        c = 5;
                        break;
                    }
                    c = 65535;
                    break;
                default:
                    c = 65535;
                    break;
            }
            if (c != 0) {
                if (c == 1) {
                    j5 = u(attributeValue, bVar);
                } else if (c == 2) {
                    j4 = u(attributeValue, bVar);
                } else if (c == 3) {
                    j3 = u(attributeValue, bVar);
                } else if (c == 4) {
                    String[] t2 = t(attributeValue);
                    if (t2.length > 0) {
                        strArr = t2;
                    }
                } else if (c == 5 && attributeValue.startsWith("#")) {
                    str2 = attributeValue.substring(1);
                }
            } else if (map.containsKey(attributeValue)) {
                str = attributeValue;
            }
            i++;
            xmlPullParser2 = xmlPullParser;
        }
        if (dVar != null) {
            long j6 = dVar.d;
            j = -9223372036854775807L;
            if (j6 != -9223372036854775807L) {
                if (j3 != -9223372036854775807L) {
                    j3 += j6;
                }
                if (j4 != -9223372036854775807L) {
                    j4 += j6;
                }
            }
        } else {
            j = -9223372036854775807L;
        }
        if (j4 == j) {
            if (j5 != j) {
                j2 = j3 + j5;
            } else if (dVar != null) {
                long j7 = dVar.e;
                if (j7 != j) {
                    j2 = j7;
                }
            }
            return new d(xmlPullParser.getName(), null, j3, j2, s2, strArr, str, str2, dVar);
        }
        j2 = j4;
        return new d(xmlPullParser.getName(), null, j3, j2, s2, strArr, str, str2, dVar);
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Code restructure failed: missing block: B:103:0x01e6, code lost:
        if (r5.equals("auto") != false) goto L105;
     */
    /* JADX WARN: Removed duplicated region for block: B:106:0x01ed  */
    /* JADX WARN: Removed duplicated region for block: B:108:0x01f4  */
    /* JADX WARN: Removed duplicated region for block: B:126:0x0246  */
    /* JADX WARN: Removed duplicated region for block: B:127:0x0248  */
    /* JADX WARN: Removed duplicated region for block: B:130:0x025a  */
    /* JADX WARN: Removed duplicated region for block: B:140:0x0279  */
    /* JADX WARN: Removed duplicated region for block: B:144:0x0282  */
    /* JADX WARN: Removed duplicated region for block: B:147:0x0288  */
    /* JADX WARN: Removed duplicated region for block: B:89:0x01a9  */
    /* JADX WARN: Removed duplicated region for block: B:92:0x01af  */
    /* JADX WARN: Removed duplicated region for block: B:95:0x01be  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static b.i.a.c.b3.t.f s(org.xmlpull.v1.XmlPullParser r16, b.i.a.c.b3.t.f r17) {
        /*
            Method dump skipped, instructions count: 1312
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.b3.t.c.s(org.xmlpull.v1.XmlPullParser, b.i.a.c.b3.t.f):b.i.a.c.b3.t.f");
    }

    public static String[] t(String str) {
        String trim = str.trim();
        if (trim.isEmpty()) {
            return new String[0];
        }
        int i = e0.a;
        return trim.split("\\s+", -1);
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Code restructure failed: missing block: B:23:0x00ad, code lost:
        if (r13.equals("ms") == false) goto L21;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static long u(java.lang.String r13, b.i.a.c.b3.t.c.b r14) throws com.google.android.exoplayer2.text.SubtitleDecoderException {
        /*
            Method dump skipped, instructions count: 318
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.b3.t.c.u(java.lang.String, b.i.a.c.b3.t.c$b):long");
    }

    @Nullable
    public static C0096c v(XmlPullParser xmlPullParser) {
        String r0 = d.r0(xmlPullParser, "extent");
        if (r0 == null) {
            return null;
        }
        Matcher matcher = f876s.matcher(r0);
        if (!matcher.matches()) {
            Log.w("TtmlDecoder", r0.length() != 0 ? "Ignoring non-pixel tts extent: ".concat(r0) : new String("Ignoring non-pixel tts extent: "));
            return null;
        }
        try {
            String group = matcher.group(1);
            Objects.requireNonNull(group);
            int parseInt = Integer.parseInt(group);
            String group2 = matcher.group(2);
            Objects.requireNonNull(group2);
            return new C0096c(parseInt, Integer.parseInt(group2));
        } catch (NumberFormatException unused) {
            Log.w("TtmlDecoder", r0.length() != 0 ? "Ignoring malformed tts extent: ".concat(r0) : new String("Ignoring malformed tts extent: "));
            return null;
        }
    }

    @Override // b.i.a.c.b3.f
    public g j(byte[] bArr, int i, boolean z2) throws SubtitleDecoderException {
        b bVar;
        try {
            XmlPullParser newPullParser = this.w.newPullParser();
            HashMap hashMap = new HashMap();
            HashMap hashMap2 = new HashMap();
            HashMap hashMap3 = new HashMap();
            hashMap2.put("", new e("", -3.4028235E38f, -3.4028235E38f, Integer.MIN_VALUE, Integer.MIN_VALUE, -3.4028235E38f, -3.4028235E38f, Integer.MIN_VALUE, -3.4028235E38f, Integer.MIN_VALUE));
            C0096c cVar = null;
            newPullParser.setInput(new ByteArrayInputStream(bArr, 0, i), null);
            ArrayDeque arrayDeque = new ArrayDeque();
            b bVar2 = u;
            a aVar = v;
            g gVar = null;
            int i2 = 0;
            for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.getEventType()) {
                d dVar = (d) arrayDeque.peek();
                if (i2 == 0) {
                    String name = newPullParser.getName();
                    if (eventType == 2) {
                        if ("tt".equals(name)) {
                            bVar2 = p(newPullParser);
                            aVar = n(newPullParser, v);
                            cVar = v(newPullParser);
                        }
                        C0096c cVar2 = cVar;
                        b bVar3 = bVar2;
                        a aVar2 = aVar;
                        if (!l(name)) {
                            String valueOf = String.valueOf(newPullParser.getName());
                            Log.i("TtmlDecoder", valueOf.length() != 0 ? "Ignoring unsupported tag: ".concat(valueOf) : new String("Ignoring unsupported tag: "));
                            i2++;
                            bVar = bVar3;
                        } else if ("head".equals(name)) {
                            bVar = bVar3;
                            q(newPullParser, hashMap, aVar2, cVar2, hashMap2, hashMap3);
                        } else {
                            bVar = bVar3;
                            try {
                                d r2 = r(newPullParser, dVar, hashMap2, bVar);
                                arrayDeque.push(r2);
                                if (dVar != null) {
                                    dVar.a(r2);
                                }
                            } catch (SubtitleDecoderException e) {
                                q.c("TtmlDecoder", "Suppressing parser error", e);
                                i2++;
                            }
                        }
                        bVar2 = bVar;
                        cVar = cVar2;
                        aVar = aVar2;
                    } else if (eventType == 4) {
                        Objects.requireNonNull(dVar);
                        d b2 = d.b(newPullParser.getText());
                        if (dVar.m == null) {
                            dVar.m = new ArrayList();
                        }
                        dVar.m.add(b2);
                    } else if (eventType == 3) {
                        if (newPullParser.getName().equals("tt")) {
                            d dVar2 = (d) arrayDeque.peek();
                            Objects.requireNonNull(dVar2);
                            gVar = new g(dVar2, hashMap, hashMap2, hashMap3);
                        }
                        arrayDeque.pop();
                    }
                } else if (eventType == 2) {
                    i2++;
                } else if (eventType == 3) {
                    i2--;
                }
                newPullParser.next();
            }
            if (gVar != null) {
                return gVar;
            }
            throw new SubtitleDecoderException("No TTML subtitles found");
        } catch (IOException e2) {
            throw new IllegalStateException("Unexpected error when reading input.", e2);
        } catch (XmlPullParserException e3) {
            throw new SubtitleDecoderException("Unable to decode source", e3);
        }
    }
}
