package b.i.a.c.b3.r;

import android.graphics.Color;
import android.graphics.PointF;
import android.util.Log;
import androidx.annotation.ColorInt;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.q;
import b.i.a.f.e.o.f;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/* compiled from: SsaStyle.java */
/* loaded from: classes3.dex */
public final class c {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final int f872b;
    @Nullable
    @ColorInt
    public final Integer c;
    public final float d;
    public final boolean e;
    public final boolean f;
    public final boolean g;
    public final boolean h;

    /* compiled from: SsaStyle.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final int f873b;
        public final int c;
        public final int d;
        public final int e;
        public final int f;
        public final int g;
        public final int h;
        public final int i;

        public a(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
            this.a = i;
            this.f873b = i2;
            this.c = i3;
            this.d = i4;
            this.e = i5;
            this.f = i6;
            this.g = i7;
            this.h = i8;
            this.i = i9;
        }
    }

    /* compiled from: SsaStyle.java */
    /* loaded from: classes3.dex */
    public static final class b {
        public static final Pattern a = Pattern.compile("\\{([^}]*)\\}");

        /* renamed from: b  reason: collision with root package name */
        public static final Pattern f874b = Pattern.compile(e0.k("\\\\pos\\((%1$s),(%1$s)\\)", "\\s*\\d+(?:\\.\\d+)?\\s*"));
        public static final Pattern c = Pattern.compile(e0.k("\\\\move\\(%1$s,%1$s,(%1$s),(%1$s)(?:,%1$s,%1$s)?\\)", "\\s*\\d+(?:\\.\\d+)?\\s*"));
        public static final Pattern d = Pattern.compile("\\\\an(\\d+)");

        @Nullable
        public static PointF a(String str) {
            String str2;
            String str3;
            Matcher matcher = f874b.matcher(str);
            Matcher matcher2 = c.matcher(str);
            boolean find = matcher.find();
            boolean find2 = matcher2.find();
            if (find) {
                if (find2) {
                    StringBuilder sb = new StringBuilder(str.length() + 82);
                    sb.append("Override has both \\pos(x,y) and \\move(x1,y1,x2,y2); using \\pos values. override='");
                    sb.append(str);
                    sb.append("'");
                    Log.i("SsaStyle.Overrides", sb.toString());
                }
                str2 = matcher.group(1);
                str3 = matcher.group(2);
            } else if (!find2) {
                return null;
            } else {
                str2 = matcher2.group(1);
                str3 = matcher2.group(2);
            }
            Objects.requireNonNull(str2);
            float parseFloat = Float.parseFloat(str2.trim());
            Objects.requireNonNull(str3);
            return new PointF(parseFloat, Float.parseFloat(str3.trim()));
        }
    }

    public c(String str, int i, @Nullable @ColorInt Integer num, float f, boolean z2, boolean z3, boolean z4, boolean z5) {
        this.a = str;
        this.f872b = i;
        this.c = num;
        this.d = f;
        this.e = z2;
        this.f = z3;
        this.g = z4;
        this.h = z5;
    }

    public static int a(String str) {
        boolean z2;
        try {
            int parseInt = Integer.parseInt(str.trim());
            switch (parseInt) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    z2 = true;
                    break;
                default:
                    z2 = false;
                    break;
            }
            if (z2) {
                return parseInt;
            }
        } catch (NumberFormatException unused) {
        }
        String valueOf = String.valueOf(str);
        Log.w("SsaStyle", valueOf.length() != 0 ? "Ignoring unknown alignment: ".concat(valueOf) : new String("Ignoring unknown alignment: "));
        return -1;
    }

    public static boolean b(String str) {
        try {
            int parseInt = Integer.parseInt(str);
            return parseInt == 1 || parseInt == -1;
        } catch (NumberFormatException e) {
            q.c("SsaStyle", b.d.b.a.a.j(b.d.b.a.a.b(str, 33), "Failed to parse boolean value: '", str, "'"), e);
            return false;
        }
    }

    @Nullable
    @ColorInt
    public static Integer c(String str) {
        long j;
        try {
            if (str.startsWith("&H")) {
                j = Long.parseLong(str.substring(2), 16);
            } else {
                j = Long.parseLong(str);
            }
            d.j(j <= 4294967295L);
            return Integer.valueOf(Color.argb(f.H(((j >> 24) & 255) ^ 255), f.H(j & 255), f.H((j >> 8) & 255), f.H((j >> 16) & 255)));
        } catch (IllegalArgumentException e) {
            q.c("SsaStyle", b.d.b.a.a.j(b.d.b.a.a.b(str, 36), "Failed to parse color expression: '", str, "'"), e);
            return null;
        }
    }
}
