package b.i.a.c.b3.r;

import android.text.TextUtils;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import b.c.a.a0.d;
import b.i.a.f.e.o.f;
/* compiled from: SsaDialogueFormat.java */
/* loaded from: classes3.dex */
public final class b {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final int f871b;
    public final int c;
    public final int d;
    public final int e;

    public b(int i, int i2, int i3, int i4, int i5) {
        this.a = i;
        this.f871b = i2;
        this.c = i3;
        this.d = i4;
        this.e = i5;
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    @Nullable
    public static b a(String str) {
        char c;
        d.j(str.startsWith("Format:"));
        String[] split = TextUtils.split(str.substring(7), ",");
        int i = -1;
        int i2 = -1;
        int i3 = -1;
        int i4 = -1;
        for (int i5 = 0; i5 < split.length; i5++) {
            String u1 = f.u1(split[i5].trim());
            u1.hashCode();
            switch (u1.hashCode()) {
                case 100571:
                    if (u1.equals("end")) {
                        c = 0;
                        break;
                    }
                    c = 65535;
                    break;
                case 3556653:
                    if (u1.equals(NotificationCompat.MessagingStyle.Message.KEY_TEXT)) {
                        c = 1;
                        break;
                    }
                    c = 65535;
                    break;
                case 109757538:
                    if (u1.equals("start")) {
                        c = 2;
                        break;
                    }
                    c = 65535;
                    break;
                case 109780401:
                    if (u1.equals("style")) {
                        c = 3;
                        break;
                    }
                    c = 65535;
                    break;
                default:
                    c = 65535;
                    break;
            }
            if (c == 0) {
                i2 = i5;
            } else if (c == 1) {
                i4 = i5;
            } else if (c == 2) {
                i = i5;
            } else if (c == 3) {
                i3 = i5;
            }
        }
        if (i == -1 || i2 == -1 || i4 == -1) {
            return null;
        }
        return new b(i, i2, i3, i4, split.length);
    }
}
