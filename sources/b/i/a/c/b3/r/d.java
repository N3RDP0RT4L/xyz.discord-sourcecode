package b.i.a.c.b3.r;

import b.i.a.c.b3.b;
import b.i.a.c.b3.g;
import b.i.a.c.f3.e0;
import java.util.Collections;
import java.util.List;
/* compiled from: SsaSubtitle.java */
/* loaded from: classes3.dex */
public final class d implements g {
    public final List<List<b>> j;
    public final List<Long> k;

    public d(List<List<b>> list, List<Long> list2) {
        this.j = list;
        this.k = list2;
    }

    @Override // b.i.a.c.b3.g
    public int f(long j) {
        int i;
        List<Long> list = this.k;
        Long valueOf = Long.valueOf(j);
        int i2 = e0.a;
        int binarySearch = Collections.binarySearch(list, valueOf);
        if (binarySearch < 0) {
            i = ~binarySearch;
        } else {
            int size = list.size();
            do {
                binarySearch++;
                if (binarySearch >= size) {
                    break;
                }
            } while (list.get(binarySearch).compareTo(valueOf) == 0);
            i = binarySearch;
        }
        if (i < this.k.size()) {
            return i;
        }
        return -1;
    }

    @Override // b.i.a.c.b3.g
    public long g(int i) {
        boolean z2 = true;
        b.c.a.a0.d.j(i >= 0);
        if (i >= this.k.size()) {
            z2 = false;
        }
        b.c.a.a0.d.j(z2);
        return this.k.get(i).longValue();
    }

    @Override // b.i.a.c.b3.g
    public List<b> h(long j) {
        int i;
        List<Long> list = this.k;
        Long valueOf = Long.valueOf(j);
        int i2 = e0.a;
        int binarySearch = Collections.binarySearch(list, valueOf);
        if (binarySearch < 0) {
            i = -(binarySearch + 2);
        } else {
            do {
                binarySearch--;
                if (binarySearch < 0) {
                    break;
                }
            } while (list.get(binarySearch).compareTo(valueOf) == 0);
            i = binarySearch + 1;
        }
        if (i == -1) {
            return Collections.emptyList();
        }
        return this.j.get(i);
    }

    @Override // b.i.a.c.b3.g
    public int i() {
        return this.k.size();
    }
}
