package b.i.a.c.b3.r;

import android.graphics.PointF;
import android.text.Layout;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import b.c.a.a0.d;
import b.i.a.c.b3.b;
import b.i.a.c.b3.f;
import b.i.a.c.b3.g;
import b.i.a.c.b3.r.c;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.x;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/* compiled from: SsaDecoder.java */
/* loaded from: classes3.dex */
public final class a extends f {
    public static final Pattern n = Pattern.compile("(?:(\\d+):)?(\\d+):(\\d+)[:.](\\d+)");
    public final boolean o;
    @Nullable
    public final b p;
    public Map<String, c> q;
    public float r = -3.4028235E38f;

    /* renamed from: s  reason: collision with root package name */
    public float f870s = -3.4028235E38f;

    public a(@Nullable List<byte[]> list) {
        super("SsaDecoder");
        if (list == null || list.isEmpty()) {
            this.o = false;
            this.p = null;
            return;
        }
        this.o = true;
        String l = e0.l(list.get(0));
        d.j(l.startsWith("Format:"));
        b a = b.a(l);
        Objects.requireNonNull(a);
        this.p = a;
        m(new x(list.get(1)));
    }

    public static int k(long j, List<Long> list, List<List<b>> list2) {
        int i;
        int size = list.size() - 1;
        while (true) {
            if (size < 0) {
                i = 0;
                break;
            } else if (list.get(size).longValue() == j) {
                return size;
            } else {
                if (list.get(size).longValue() < j) {
                    i = size + 1;
                    break;
                }
                size--;
            }
        }
        list.add(i, Long.valueOf(j));
        list2.add(i, i == 0 ? new ArrayList() : new ArrayList(list2.get(i - 1)));
        return i;
    }

    public static float l(int i) {
        if (i == 0) {
            return 0.05f;
        }
        if (i != 1) {
            return i != 2 ? -3.4028235E38f : 0.95f;
        }
        return 0.5f;
    }

    public static long n(String str) {
        Matcher matcher = n.matcher(str.trim());
        if (!matcher.matches()) {
            return -9223372036854775807L;
        }
        String group = matcher.group(1);
        int i = e0.a;
        long parseLong = (Long.parseLong(matcher.group(2)) * 60 * 1000000) + (Long.parseLong(group) * 60 * 60 * 1000000);
        return (Long.parseLong(matcher.group(4)) * 10000) + (Long.parseLong(matcher.group(3)) * 1000000) + parseLong;
    }

    @Override // b.i.a.c.b3.f
    public g j(byte[] bArr, int i, boolean z2) {
        x xVar;
        b bVar;
        long j;
        float f;
        int i2;
        int i3;
        Layout.Alignment alignment;
        int i4;
        int i5;
        float f2;
        float f3;
        int i6;
        float f4;
        float f5;
        int i7;
        int i8;
        int i9;
        int i10;
        a aVar = this;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        x xVar2 = new x(bArr, i);
        if (!aVar.o) {
            aVar.m(xVar2);
        }
        b bVar2 = aVar.o ? aVar.p : null;
        while (true) {
            String g = xVar2.g();
            if (g == null) {
                return new d(arrayList, arrayList2);
            }
            if (g.startsWith("Format:")) {
                bVar2 = b.a(g);
            } else {
                if (g.startsWith("Dialogue:")) {
                    if (bVar2 == null) {
                        Log.w("SsaDecoder", g.length() != 0 ? "Skipping dialogue line before complete format: ".concat(g) : new String("Skipping dialogue line before complete format: "));
                    } else {
                        d.j(g.startsWith("Dialogue:"));
                        String[] split = g.substring(9).split(",", bVar2.e);
                        if (split.length != bVar2.e) {
                            Log.w("SsaDecoder", g.length() != 0 ? "Skipping dialogue line with fewer columns than format: ".concat(g) : new String("Skipping dialogue line with fewer columns than format: "));
                        } else {
                            long n2 = n(split[bVar2.a]);
                            if (n2 == -9223372036854775807L) {
                                Log.w("SsaDecoder", g.length() != 0 ? "Skipping invalid timing: ".concat(g) : new String("Skipping invalid timing: "));
                            } else {
                                long n3 = n(split[bVar2.f871b]);
                                if (n3 == -9223372036854775807L) {
                                    Log.w("SsaDecoder", g.length() != 0 ? "Skipping invalid timing: ".concat(g) : new String("Skipping invalid timing: "));
                                } else {
                                    Map<String, c> map = aVar.q;
                                    c cVar = (map == null || (i10 = bVar2.c) == -1) ? null : map.get(split[i10].trim());
                                    String str = split[bVar2.d];
                                    Matcher matcher = c.b.a.matcher(str);
                                    PointF pointF = null;
                                    int i11 = -1;
                                    while (true) {
                                        xVar = xVar2;
                                        if (matcher.find()) {
                                            String group = matcher.group(1);
                                            Objects.requireNonNull(group);
                                            try {
                                                PointF a = c.b.a(group);
                                                if (a != null) {
                                                    pointF = a;
                                                }
                                            } catch (RuntimeException unused) {
                                            }
                                            try {
                                                Matcher matcher2 = c.b.d.matcher(group);
                                                if (matcher2.find()) {
                                                    String group2 = matcher2.group(1);
                                                    Objects.requireNonNull(group2);
                                                    i9 = c.a(group2);
                                                } else {
                                                    i9 = -1;
                                                }
                                                if (i9 != -1) {
                                                    i11 = i9;
                                                }
                                            } catch (RuntimeException unused2) {
                                            }
                                            xVar2 = xVar;
                                        } else {
                                            String replace = c.b.a.matcher(str).replaceAll("").replace("\\N", "\n").replace("\\n", "\n").replace("\\h", " ");
                                            float f6 = aVar.r;
                                            float f7 = aVar.f870s;
                                            SpannableString spannableString = new SpannableString(replace);
                                            if (cVar != null) {
                                                if (cVar.c != null) {
                                                    bVar = bVar2;
                                                    j = n3;
                                                    spannableString.setSpan(new ForegroundColorSpan(cVar.c.intValue()), 0, spannableString.length(), 33);
                                                } else {
                                                    bVar = bVar2;
                                                    j = n3;
                                                }
                                                float f8 = cVar.d;
                                                if (f8 == -3.4028235E38f || f7 == -3.4028235E38f) {
                                                    f4 = -3.4028235E38f;
                                                    i6 = Integer.MIN_VALUE;
                                                } else {
                                                    f4 = f8 / f7;
                                                    i6 = 1;
                                                }
                                                boolean z3 = cVar.e;
                                                if (!z3 || !cVar.f) {
                                                    f5 = f4;
                                                    i7 = 0;
                                                    i8 = 33;
                                                    if (z3) {
                                                        spannableString.setSpan(new StyleSpan(1), 0, spannableString.length(), 33);
                                                    } else if (cVar.f) {
                                                        spannableString.setSpan(new StyleSpan(2), 0, spannableString.length(), 33);
                                                    }
                                                } else {
                                                    i7 = 0;
                                                    f5 = f4;
                                                    i8 = 33;
                                                    spannableString.setSpan(new StyleSpan(3), 0, spannableString.length(), 33);
                                                }
                                                if (cVar.g) {
                                                    spannableString.setSpan(new UnderlineSpan(), i7, spannableString.length(), i8);
                                                }
                                                if (cVar.h) {
                                                    spannableString.setSpan(new StrikethroughSpan(), i7, spannableString.length(), i8);
                                                }
                                                i3 = -1;
                                                i2 = i6;
                                                f = f5;
                                            } else {
                                                bVar = bVar2;
                                                j = n3;
                                                i3 = -1;
                                                i2 = Integer.MIN_VALUE;
                                                f = -3.4028235E38f;
                                            }
                                            if (i11 == i3) {
                                                i11 = cVar != null ? cVar.f872b : i3;
                                            }
                                            switch (i11) {
                                                case 0:
                                                default:
                                                    Log.w("SsaDecoder", b.d.b.a.a.f(30, "Unknown alignment: ", i11));
                                                case -1:
                                                    alignment = null;
                                                    break;
                                                case 1:
                                                case 4:
                                                case 7:
                                                    alignment = Layout.Alignment.ALIGN_NORMAL;
                                                    break;
                                                case 2:
                                                case 5:
                                                case 8:
                                                    alignment = Layout.Alignment.ALIGN_CENTER;
                                                    break;
                                                case 3:
                                                case 6:
                                                case 9:
                                                    alignment = Layout.Alignment.ALIGN_OPPOSITE;
                                                    break;
                                            }
                                            Layout.Alignment alignment2 = alignment;
                                            switch (i11) {
                                                case 0:
                                                default:
                                                    Log.w("SsaDecoder", b.d.b.a.a.f(30, "Unknown alignment: ", i11));
                                                case -1:
                                                    i4 = Integer.MIN_VALUE;
                                                    break;
                                                case 1:
                                                case 4:
                                                case 7:
                                                    i4 = 0;
                                                    break;
                                                case 2:
                                                case 5:
                                                case 8:
                                                    i4 = 1;
                                                    break;
                                                case 3:
                                                case 6:
                                                case 9:
                                                    i4 = 2;
                                                    break;
                                            }
                                            switch (i11) {
                                                case 0:
                                                default:
                                                    Log.w("SsaDecoder", b.d.b.a.a.f(30, "Unknown alignment: ", i11));
                                                case -1:
                                                    i5 = Integer.MIN_VALUE;
                                                    break;
                                                case 1:
                                                case 2:
                                                case 3:
                                                    i5 = 2;
                                                    break;
                                                case 4:
                                                case 5:
                                                case 6:
                                                    i5 = 1;
                                                    break;
                                                case 7:
                                                case 8:
                                                case 9:
                                                    i5 = 0;
                                                    break;
                                            }
                                            if (pointF == null || f7 == -3.4028235E38f || f6 == -3.4028235E38f) {
                                                f3 = l(i4);
                                                f2 = l(i5);
                                            } else {
                                                f3 = pointF.x / f6;
                                                f2 = pointF.y / f7;
                                            }
                                            b bVar3 = new b(spannableString, alignment2, null, null, f2, 0, i5, f3, i4, i2, f, -3.4028235E38f, -3.4028235E38f, false, ViewCompat.MEASURED_STATE_MASK, Integer.MIN_VALUE, 0.0f, null);
                                            int k = k(j, arrayList2, arrayList);
                                            for (int k2 = k(n2, arrayList2, arrayList); k2 < k; k2++) {
                                                ((List) arrayList.get(k2)).add(bVar3);
                                            }
                                            aVar = this;
                                            bVar2 = bVar;
                                            xVar2 = xVar;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                xVar = xVar2;
                bVar = bVar2;
                aVar = this;
                bVar2 = bVar;
                xVar2 = xVar;
            }
        }
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Removed duplicated region for block: B:133:0x0293  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void m(b.i.a.c.f3.x r26) {
        /*
            Method dump skipped, instructions count: 752
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.b3.r.a.m(b.i.a.c.f3.x):void");
    }
}
