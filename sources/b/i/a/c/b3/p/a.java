package b.i.a.c.b3.p;

import android.graphics.Bitmap;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import b.i.a.c.b3.b;
import b.i.a.c.b3.f;
import b.i.a.c.b3.g;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.x;
import com.google.android.exoplayer2.text.SubtitleDecoderException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;
import java.util.zip.Inflater;
/* compiled from: PgsDecoder.java */
/* loaded from: classes3.dex */
public final class a extends f {
    public final x n = new x();
    public final x o = new x();
    public final C0095a p = new C0095a();
    @Nullable
    public Inflater q;

    /* compiled from: PgsDecoder.java */
    /* renamed from: b.i.a.c.b3.p.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0095a {
        public final x a = new x();

        /* renamed from: b  reason: collision with root package name */
        public final int[] f867b = new int[256];
        public boolean c;
        public int d;
        public int e;
        public int f;
        public int g;
        public int h;
        public int i;

        public void a() {
            this.d = 0;
            this.e = 0;
            this.f = 0;
            this.g = 0;
            this.h = 0;
            this.i = 0;
            this.a.A(0);
            this.c = false;
        }
    }

    public a() {
        super("PgsDecoder");
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // b.i.a.c.b3.f
    public g j(byte[] bArr, int i, boolean z2) throws SubtitleDecoderException {
        b bVar;
        x xVar;
        x xVar2;
        int i2;
        int i3;
        int v;
        a aVar = this;
        x xVar3 = aVar.n;
        xVar3.a = bArr;
        xVar3.c = i;
        int i4 = 0;
        xVar3.f980b = 0;
        if (xVar3.a() > 0 && xVar3.c() == 120) {
            if (aVar.q == null) {
                aVar.q = new Inflater();
            }
            if (e0.x(xVar3, aVar.o, aVar.q)) {
                x xVar4 = aVar.o;
                xVar3.C(xVar4.a, xVar4.c);
            }
        }
        aVar.p.a();
        ArrayList arrayList = new ArrayList();
        while (aVar.n.a() >= 3) {
            x xVar5 = aVar.n;
            C0095a aVar2 = aVar.p;
            int i5 = xVar5.c;
            int t = xVar5.t();
            int y2 = xVar5.y();
            int i6 = xVar5.f980b + y2;
            if (i6 > i5) {
                xVar5.E(i5);
                bVar = null;
            } else {
                if (t != 128) {
                    switch (t) {
                        case 20:
                            Objects.requireNonNull(aVar2);
                            if (y2 % 5 == 2) {
                                xVar5.F(2);
                                Arrays.fill(aVar2.f867b, i4);
                                int i7 = y2 / 5;
                                for (int i8 = 0; i8 < i7; i8++) {
                                    int t2 = xVar5.t();
                                    int t3 = xVar5.t();
                                    int t4 = xVar5.t();
                                    int t5 = xVar5.t();
                                    int t6 = xVar5.t();
                                    double d = t3;
                                    xVar5 = xVar5;
                                    double d2 = t4 - 128;
                                    arrayList = arrayList;
                                    double d3 = t5 - 128;
                                    aVar2.f867b[t2] = (e0.h((int) ((1.402d * d2) + d), 0, 255) << 16) | (t6 << 24) | (e0.h((int) ((d - (0.34414d * d3)) - (d2 * 0.71414d)), 0, 255) << 8) | e0.h((int) ((d3 * 1.772d) + d), 0, 255);
                                }
                                xVar = xVar5;
                                aVar2.c = true;
                                break;
                            }
                            xVar = xVar5;
                            break;
                        case 21:
                            Objects.requireNonNull(aVar2);
                            if (y2 >= 4) {
                                xVar5.F(3);
                                int i9 = y2 - 4;
                                if ((xVar5.t() & 128) != 0) {
                                    if (i9 >= 7 && (v = xVar5.v()) >= 4) {
                                        aVar2.h = xVar5.y();
                                        aVar2.i = xVar5.y();
                                        aVar2.a.A(v - 4);
                                        i9 -= 7;
                                    }
                                }
                                x xVar6 = aVar2.a;
                                int i10 = xVar6.f980b;
                                int i11 = xVar6.c;
                                if (i10 < i11 && i9 > 0) {
                                    int min = Math.min(i9, i11 - i10);
                                    xVar5.e(aVar2.a.a, i10, min);
                                    aVar2.a.E(i10 + min);
                                }
                            }
                            xVar = xVar5;
                            break;
                        case 22:
                            Objects.requireNonNull(aVar2);
                            if (y2 >= 19) {
                                aVar2.d = xVar5.y();
                                aVar2.e = xVar5.y();
                                xVar5.F(11);
                                aVar2.f = xVar5.y();
                                aVar2.g = xVar5.y();
                            }
                            xVar = xVar5;
                            break;
                        default:
                            xVar = xVar5;
                            break;
                    }
                    bVar = null;
                } else {
                    xVar = xVar5;
                    if (aVar2.d == 0 || aVar2.e == 0 || aVar2.h == 0 || aVar2.i == 0 || (i2 = (xVar2 = aVar2.a).c) == 0 || xVar2.f980b != i2 || !aVar2.c) {
                        bVar = null;
                    } else {
                        xVar2.E(0);
                        int i12 = aVar2.h * aVar2.i;
                        int[] iArr = new int[i12];
                        int i13 = 0;
                        while (i13 < i12) {
                            int t7 = aVar2.a.t();
                            if (t7 != 0) {
                                i3 = i13 + 1;
                                iArr[i13] = aVar2.f867b[t7];
                            } else {
                                int t8 = aVar2.a.t();
                                if (t8 != 0) {
                                    i3 = ((t8 & 64) == 0 ? t8 & 63 : ((t8 & 63) << 8) | aVar2.a.t()) + i13;
                                    Arrays.fill(iArr, i13, i3, (t8 & 128) == 0 ? 0 : aVar2.f867b[aVar2.a.t()]);
                                }
                            }
                            i13 = i3;
                        }
                        Bitmap createBitmap = Bitmap.createBitmap(iArr, aVar2.h, aVar2.i, Bitmap.Config.ARGB_8888);
                        float f = aVar2.d;
                        float f2 = aVar2.e;
                        bVar = new b(null, null, null, createBitmap, aVar2.g / f2, 0, 0, aVar2.f / f, 0, Integer.MIN_VALUE, -3.4028235E38f, aVar2.h / f, aVar2.i / f2, false, ViewCompat.MEASURED_STATE_MASK, Integer.MIN_VALUE, 0.0f, null);
                    }
                    aVar2.a();
                }
                xVar.E(i6);
            }
            ArrayList arrayList2 = arrayList;
            if (bVar != null) {
                arrayList2.add(bVar);
            }
            arrayList = arrayList2;
            i4 = 0;
            aVar = this;
        }
        return new b(Collections.unmodifiableList(arrayList));
    }
}
