package b.i.a.c.b3.s;

import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import androidx.core.view.ViewCompat;
import b.i.a.c.b3.b;
import b.i.a.c.b3.f;
import b.i.a.c.b3.g;
import b.i.a.c.f3.x;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/* compiled from: SubripDecoder.java */
/* loaded from: classes3.dex */
public final class a extends f {
    public static final Pattern n = Pattern.compile("\\s*((?:(\\d+):)?(\\d+):(\\d+)(?:,(\\d+))?)\\s*-->\\s*((?:(\\d+):)?(\\d+):(\\d+)(?:,(\\d+))?)\\s*");
    public static final Pattern o = Pattern.compile("\\{\\\\.*?\\}");
    public final StringBuilder p = new StringBuilder();
    public final ArrayList<String> q = new ArrayList<>();

    public a() {
        super("SubripDecoder");
    }

    public static float k(int i) {
        if (i == 0) {
            return 0.08f;
        }
        if (i == 1) {
            return 0.5f;
        }
        if (i == 2) {
            return 0.92f;
        }
        throw new IllegalArgumentException();
    }

    public static long l(Matcher matcher, int i) {
        String group = matcher.group(i + 1);
        long parseLong = group != null ? Long.parseLong(group) * 60 * 60 * 1000 : 0L;
        String group2 = matcher.group(i + 2);
        Objects.requireNonNull(group2);
        String group3 = matcher.group(i + 3);
        Objects.requireNonNull(group3);
        long parseLong2 = (Long.parseLong(group3) * 1000) + (Long.parseLong(group2) * 60 * 1000) + parseLong;
        String group4 = matcher.group(i + 4);
        if (group4 != null) {
            parseLong2 += Long.parseLong(group4);
        }
        return parseLong2 * 1000;
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // b.i.a.c.b3.f
    public g j(byte[] bArr, int i, boolean z2) {
        x xVar;
        String g;
        int i2;
        x xVar2;
        long[] jArr;
        b bVar;
        char c;
        char c2;
        a aVar = this;
        ArrayList arrayList = new ArrayList();
        long[] jArr2 = new long[32];
        x xVar3 = new x(bArr, i);
        int i3 = 0;
        int i4 = 0;
        while (true) {
            String g2 = xVar3.g();
            if (g2 != null) {
                if (g2.length() != 0) {
                    try {
                        Integer.parseInt(g2);
                        g = xVar3.g();
                    } catch (NumberFormatException unused) {
                        xVar = xVar3;
                        Log.w("SubripDecoder", g2.length() != 0 ? "Skipping invalid index: ".concat(g2) : new String("Skipping invalid index: "));
                    }
                    if (g == null) {
                        Log.w("SubripDecoder", "Unexpected end");
                    } else {
                        Matcher matcher = n.matcher(g);
                        if (matcher.matches()) {
                            long l = l(matcher, 1);
                            if (i4 == jArr2.length) {
                                jArr2 = Arrays.copyOf(jArr2, i4 * 2);
                            }
                            int i5 = i4 + 1;
                            jArr2[i4] = l;
                            long l2 = l(matcher, 6);
                            if (i5 == jArr2.length) {
                                jArr2 = Arrays.copyOf(jArr2, i5 * 2);
                            }
                            int i6 = i5 + 1;
                            jArr2[i5] = l2;
                            aVar.p.setLength(i3);
                            aVar.q.clear();
                            for (String g3 = xVar3.g(); !TextUtils.isEmpty(g3); g3 = xVar3.g()) {
                                if (aVar.p.length() > 0) {
                                    aVar.p.append("<br>");
                                }
                                StringBuilder sb = aVar.p;
                                ArrayList<String> arrayList2 = aVar.q;
                                String trim = g3.trim();
                                StringBuilder sb2 = new StringBuilder(trim);
                                Matcher matcher2 = o.matcher(trim);
                                int i7 = 0;
                                while (matcher2.find()) {
                                    String group = matcher2.group();
                                    arrayList2.add(group);
                                    int start = matcher2.start() - i7;
                                    int length = group.length();
                                    sb2.replace(start, start + length, "");
                                    i7 += length;
                                }
                                sb.append(sb2.toString());
                            }
                            Spanned fromHtml = Html.fromHtml(aVar.p.toString());
                            String str = null;
                            int i8 = 0;
                            while (true) {
                                if (i8 < aVar.q.size()) {
                                    String str2 = aVar.q.get(i8);
                                    if (str2.matches("\\{\\\\an[1-9]\\}")) {
                                        str = str2;
                                    } else {
                                        i8++;
                                    }
                                }
                            }
                            if (str == null) {
                                bVar = new b(fromHtml, null, null, null, -3.4028235E38f, Integer.MIN_VALUE, Integer.MIN_VALUE, -3.4028235E38f, Integer.MIN_VALUE, Integer.MIN_VALUE, -3.4028235E38f, -3.4028235E38f, -3.4028235E38f, false, ViewCompat.MEASURED_STATE_MASK, Integer.MIN_VALUE, 0.0f, null);
                                jArr = jArr2;
                                xVar2 = xVar3;
                                i2 = i6;
                            } else {
                                jArr = jArr2;
                                xVar2 = xVar3;
                                switch (str.hashCode()) {
                                    case -685620710:
                                        if (str.equals("{\\an1}")) {
                                            c = 0;
                                            break;
                                        }
                                        c = 65535;
                                        break;
                                    case -685620679:
                                        if (str.equals("{\\an2}")) {
                                            c = 6;
                                            break;
                                        }
                                        c = 65535;
                                        break;
                                    case -685620648:
                                        if (str.equals("{\\an3}")) {
                                            c = 3;
                                            break;
                                        }
                                        c = 65535;
                                        break;
                                    case -685620617:
                                        if (str.equals("{\\an4}")) {
                                            c = 1;
                                            break;
                                        }
                                        c = 65535;
                                        break;
                                    case -685620586:
                                        if (str.equals("{\\an5}")) {
                                            c = 7;
                                            break;
                                        }
                                        c = 65535;
                                        break;
                                    case -685620555:
                                        if (str.equals("{\\an6}")) {
                                            c = 4;
                                            break;
                                        }
                                        c = 65535;
                                        break;
                                    case -685620524:
                                        if (str.equals("{\\an7}")) {
                                            c = 2;
                                            break;
                                        }
                                        c = 65535;
                                        break;
                                    case -685620493:
                                        if (str.equals("{\\an8}")) {
                                            c = '\b';
                                            break;
                                        }
                                        c = 65535;
                                        break;
                                    case -685620462:
                                        if (str.equals("{\\an9}")) {
                                            c = 5;
                                            break;
                                        }
                                        c = 65535;
                                        break;
                                    default:
                                        c = 65535;
                                        break;
                                }
                                i2 = i6;
                                int i9 = (c == 0 || c == 1 || c == 2) ? 0 : (c == 3 || c == 4 || c == 5) ? 2 : 1;
                                switch (str.hashCode()) {
                                    case -685620710:
                                        if (str.equals("{\\an1}")) {
                                            c2 = 0;
                                            break;
                                        }
                                        c2 = 65535;
                                        break;
                                    case -685620679:
                                        if (str.equals("{\\an2}")) {
                                            c2 = 1;
                                            break;
                                        }
                                        c2 = 65535;
                                        break;
                                    case -685620648:
                                        if (str.equals("{\\an3}")) {
                                            c2 = 2;
                                            break;
                                        }
                                        c2 = 65535;
                                        break;
                                    case -685620617:
                                        if (str.equals("{\\an4}")) {
                                            c2 = 6;
                                            break;
                                        }
                                        c2 = 65535;
                                        break;
                                    case -685620586:
                                        if (str.equals("{\\an5}")) {
                                            c2 = 7;
                                            break;
                                        }
                                        c2 = 65535;
                                        break;
                                    case -685620555:
                                        if (str.equals("{\\an6}")) {
                                            c2 = '\b';
                                            break;
                                        }
                                        c2 = 65535;
                                        break;
                                    case -685620524:
                                        if (str.equals("{\\an7}")) {
                                            c2 = 3;
                                            break;
                                        }
                                        c2 = 65535;
                                        break;
                                    case -685620493:
                                        if (str.equals("{\\an8}")) {
                                            c2 = 4;
                                            break;
                                        }
                                        c2 = 65535;
                                        break;
                                    case -685620462:
                                        if (str.equals("{\\an9}")) {
                                            c2 = 5;
                                            break;
                                        }
                                        c2 = 65535;
                                        break;
                                    default:
                                        c2 = 65535;
                                        break;
                                }
                                int i10 = (c2 == 0 || c2 == 1 || c2 == 2) ? 2 : (c2 == 3 || c2 == 4 || c2 == 5) ? 0 : 1;
                                bVar = new b(fromHtml, null, null, null, k(i10), 0, i10, k(i9), i9, Integer.MIN_VALUE, -3.4028235E38f, -3.4028235E38f, -3.4028235E38f, false, ViewCompat.MEASURED_STATE_MASK, Integer.MIN_VALUE, 0.0f, null);
                            }
                            arrayList.add(bVar);
                            arrayList.add(b.j);
                            aVar = this;
                            jArr2 = jArr;
                            xVar3 = xVar2;
                            i4 = i2;
                            i3 = 0;
                        } else {
                            xVar = xVar3;
                            Log.w("SubripDecoder", g.length() != 0 ? "Skipping invalid timing: ".concat(g) : new String("Skipping invalid timing: "));
                            aVar = this;
                            xVar3 = xVar;
                            i3 = 0;
                        }
                    }
                }
            }
        }
        return new b((b[]) arrayList.toArray(new b[0]), Arrays.copyOf(jArr2, i4));
    }
}
