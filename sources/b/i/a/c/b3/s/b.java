package b.i.a.c.b3.s;

import b.c.a.a0.d;
import b.i.a.c.b3.g;
import b.i.a.c.f3.e0;
import java.util.Collections;
import java.util.List;
/* compiled from: SubripSubtitle.java */
/* loaded from: classes3.dex */
public final class b implements g {
    public final b.i.a.c.b3.b[] j;
    public final long[] k;

    public b(b.i.a.c.b3.b[] bVarArr, long[] jArr) {
        this.j = bVarArr;
        this.k = jArr;
    }

    @Override // b.i.a.c.b3.g
    public int f(long j) {
        int b2 = e0.b(this.k, j, false, false);
        if (b2 < this.k.length) {
            return b2;
        }
        return -1;
    }

    @Override // b.i.a.c.b3.g
    public long g(int i) {
        boolean z2 = true;
        d.j(i >= 0);
        if (i >= this.k.length) {
            z2 = false;
        }
        d.j(z2);
        return this.k[i];
    }

    @Override // b.i.a.c.b3.g
    public List<b.i.a.c.b3.b> h(long j) {
        int e = e0.e(this.k, j, true, false);
        if (e != -1) {
            b.i.a.c.b3.b[] bVarArr = this.j;
            if (bVarArr[e] != b.i.a.c.b3.b.j) {
                return Collections.singletonList(bVarArr[e]);
            }
        }
        return Collections.emptyList();
    }

    @Override // b.i.a.c.b3.g
    public int i() {
        return this.k.length;
    }
}
