package b.i.a.c.b3.v;

import b.i.a.c.f3.e0;
import b.i.a.c.f3.x;
import com.google.android.exoplayer2.ParserException;
import java.util.regex.Pattern;
/* compiled from: WebvttParserUtil.java */
/* loaded from: classes3.dex */
public final class j {
    static {
        Pattern.compile("^NOTE([ \t].*)?$");
    }

    public static float a(String str) throws NumberFormatException {
        if (str.endsWith("%")) {
            return Float.parseFloat(str.substring(0, str.length() - 1)) / 100.0f;
        }
        throw new NumberFormatException("Percentages must end with %");
    }

    public static long b(String str) throws NumberFormatException {
        int i = e0.a;
        String[] split = str.split("\\.", 2);
        long j = 0;
        for (String str2 : e0.H(split[0], ":")) {
            j = (j * 60) + Long.parseLong(str2);
        }
        long j2 = j * 1000;
        if (split.length == 2) {
            j2 += Long.parseLong(split[1]);
        }
        return j2 * 1000;
    }

    public static void c(x xVar) throws ParserException {
        int i = xVar.f980b;
        String g = xVar.g();
        if (!(g != null && g.startsWith("WEBVTT"))) {
            xVar.E(i);
            String valueOf = String.valueOf(xVar.g());
            throw ParserException.a(valueOf.length() != 0 ? "Expected WEBVTT. Got ".concat(valueOf) : new String("Expected WEBVTT. Got "), null);
        }
    }
}
