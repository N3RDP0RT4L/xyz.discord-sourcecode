package b.i.a.c.b3.v;

import androidx.annotation.Nullable;
import b.i.a.c.f3.x;
import java.util.regex.Pattern;
/* compiled from: WebvttCssParser.java */
/* loaded from: classes3.dex */
public final class e {
    public static final Pattern a = Pattern.compile("\\[voice=\"([^\"]*)\"\\]");

    /* renamed from: b  reason: collision with root package name */
    public static final Pattern f884b = Pattern.compile("^((?:[0-9]*\\.)?[0-9]+)(px|em|%)$");
    public final x c = new x();
    public final StringBuilder d = new StringBuilder();

    public static String a(x xVar, StringBuilder sb) {
        boolean z2 = false;
        sb.setLength(0);
        int i = xVar.f980b;
        int i2 = xVar.c;
        while (i < i2 && !z2) {
            char c = (char) xVar.a[i];
            if ((c < 'A' || c > 'Z') && ((c < 'a' || c > 'z') && !((c >= '0' && c <= '9') || c == '#' || c == '-' || c == '.' || c == '_'))) {
                z2 = true;
            } else {
                i++;
                sb.append(c);
            }
        }
        xVar.F(i - xVar.f980b);
        return sb.toString();
    }

    @Nullable
    public static String b(x xVar, StringBuilder sb) {
        c(xVar);
        if (xVar.a() == 0) {
            return null;
        }
        String a2 = a(xVar, sb);
        if (!"".equals(a2)) {
            return a2;
        }
        StringBuilder sb2 = new StringBuilder(1);
        sb2.append((char) xVar.t());
        return sb2.toString();
    }

    /* JADX WARN: Removed duplicated region for block: B:37:0x0068 A[LOOP:1: B:4:0x0002->B:37:0x0068, LOOP_END] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static void c(b.i.a.c.f3.x r8) {
        /*
            r0 = 1
        L1:
            r1 = 1
        L2:
            int r2 = r8.a()
            if (r2 <= 0) goto L6a
            if (r1 == 0) goto L6a
            int r1 = r8.f980b
            byte[] r2 = r8.a
            r1 = r2[r1]
            char r1 = (char) r1
            r2 = 9
            r3 = 0
            if (r1 == r2) goto L28
            r2 = 10
            if (r1 == r2) goto L28
            r2 = 12
            if (r1 == r2) goto L28
            r2 = 13
            if (r1 == r2) goto L28
            r2 = 32
            if (r1 == r2) goto L28
            r1 = 0
            goto L2c
        L28:
            r8.F(r0)
            r1 = 1
        L2c:
            if (r1 != 0) goto L1
            int r1 = r8.f980b
            int r2 = r8.c
            byte[] r4 = r8.a
            int r5 = r1 + 2
            if (r5 > r2) goto L64
            int r5 = r1 + 1
            r1 = r4[r1]
            r6 = 47
            if (r1 != r6) goto L64
            int r1 = r5 + 1
            r5 = r4[r5]
            r7 = 42
            if (r5 != r7) goto L64
        L48:
            int r5 = r1 + 1
            if (r5 >= r2) goto L5c
            r1 = r4[r1]
            char r1 = (char) r1
            if (r1 != r7) goto L5a
            r1 = r4[r5]
            char r1 = (char) r1
            if (r1 != r6) goto L5a
            int r2 = r5 + 1
            r1 = r2
            goto L48
        L5a:
            r1 = r5
            goto L48
        L5c:
            int r1 = r8.f980b
            int r2 = r2 - r1
            r8.F(r2)
            r1 = 1
            goto L65
        L64:
            r1 = 0
        L65:
            if (r1 == 0) goto L68
            goto L1
        L68:
            r1 = 0
            goto L2
        L6a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.b3.v.e.c(b.i.a.c.f3.x):void");
    }
}
