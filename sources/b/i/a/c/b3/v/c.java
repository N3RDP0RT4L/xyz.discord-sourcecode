package b.i.a.c.b3.v;

import b.i.a.c.b3.b;
import b.i.a.c.b3.f;
import b.i.a.c.b3.g;
import b.i.a.c.b3.v.h;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.x;
import com.google.android.exoplayer2.text.SubtitleDecoderException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Pattern;
/* compiled from: Mp4WebvttDecoder.java */
/* loaded from: classes3.dex */
public final class c extends f {
    public final x n = new x();

    public c() {
        super("Mp4WebvttDecoder");
    }

    @Override // b.i.a.c.b3.f
    public g j(byte[] bArr, int i, boolean z2) throws SubtitleDecoderException {
        b bVar;
        x xVar = this.n;
        xVar.a = bArr;
        xVar.c = i;
        xVar.f980b = 0;
        ArrayList arrayList = new ArrayList();
        while (this.n.a() > 0) {
            if (this.n.a() >= 8) {
                int f = this.n.f();
                if (this.n.f() == 1987343459) {
                    x xVar2 = this.n;
                    int i2 = f - 8;
                    CharSequence charSequence = null;
                    b.C0092b bVar2 = null;
                    while (i2 > 0) {
                        if (i2 >= 8) {
                            int f2 = xVar2.f();
                            int f3 = xVar2.f();
                            int i3 = f2 - 8;
                            String m = e0.m(xVar2.a, xVar2.f980b, i3);
                            xVar2.F(i3);
                            i2 = (i2 - 8) - i3;
                            if (f3 == 1937011815) {
                                h.e eVar = new h.e();
                                h.e(m, eVar);
                                bVar2 = eVar.a();
                            } else if (f3 == 1885436268) {
                                charSequence = h.f(null, m.trim(), Collections.emptyList());
                            }
                        } else {
                            throw new SubtitleDecoderException("Incomplete vtt cue box header found.");
                        }
                    }
                    if (charSequence == null) {
                        charSequence = "";
                    }
                    if (bVar2 != null) {
                        bVar2.a = charSequence;
                        bVar = bVar2.a();
                    } else {
                        Pattern pattern = h.a;
                        h.e eVar2 = new h.e();
                        eVar2.c = charSequence;
                        bVar = eVar2.a().a();
                    }
                    arrayList.add(bVar);
                } else {
                    this.n.F(f - 8);
                }
            } else {
                throw new SubtitleDecoderException("Incomplete Mp4Webvtt Top Level box header found.");
            }
        }
        return new d(arrayList);
    }
}
