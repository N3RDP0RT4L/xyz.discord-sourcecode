package b.i.a.c.b3.v;

import android.graphics.Color;
import android.text.SpannableStringBuilder;
import android.text.SpannedString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.TypefaceSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import androidx.annotation.Nullable;
import androidx.constraintlayout.solver.widgets.analyzer.BasicMeasure;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.x;
import com.discord.models.domain.ModelAuditLogEntry;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/* compiled from: WebvttCueParser.java */
/* loaded from: classes3.dex */
public final class h {
    public static final Pattern a = Pattern.compile("^(\\S+)\\s+-->\\s+(\\S+)(.*)?$");

    /* renamed from: b  reason: collision with root package name */
    public static final Pattern f887b = Pattern.compile("(\\S+?):(\\S+)");
    public static final Map<String, Integer> c;
    public static final Map<String, Integer> d;

    /* compiled from: WebvttCueParser.java */
    /* loaded from: classes3.dex */
    public static class b {
        public static final /* synthetic */ int a = 0;

        /* renamed from: b  reason: collision with root package name */
        public final c f888b;
        public final int c;

        public b(c cVar, int i, a aVar) {
            this.f888b = cVar;
            this.c = i;
        }
    }

    /* compiled from: WebvttCueParser.java */
    /* loaded from: classes3.dex */
    public static final class c {
        public final String a;

        /* renamed from: b  reason: collision with root package name */
        public final int f889b;
        public final String c;
        public final Set<String> d;

        public c(String str, int i, String str2, Set<String> set) {
            this.f889b = i;
            this.a = str;
            this.c = str2;
            this.d = set;
        }
    }

    /* compiled from: WebvttCueParser.java */
    /* loaded from: classes3.dex */
    public static final class d implements Comparable<d> {
        public final int j;
        public final f k;

        public d(int i, f fVar) {
            this.j = i;
            this.k = fVar;
        }

        @Override // java.lang.Comparable
        public int compareTo(d dVar) {
            return Integer.compare(this.j, dVar.j);
        }
    }

    /* compiled from: WebvttCueParser.java */
    /* loaded from: classes3.dex */
    public static final class e {
        public CharSequence c;
        public long a = 0;

        /* renamed from: b  reason: collision with root package name */
        public long f890b = 0;
        public int d = 2;
        public float e = -3.4028235E38f;
        public int f = 1;
        public int g = 0;
        public float h = -3.4028235E38f;
        public int i = Integer.MIN_VALUE;
        public float j = 1.0f;
        public int k = Integer.MIN_VALUE;

        /* JADX WARN: Code restructure failed: missing block: B:39:0x0074, code lost:
            if (r6 == 0) goto L40;
         */
        /* JADX WARN: Removed duplicated region for block: B:38:0x0072  */
        /* JADX WARN: Removed duplicated region for block: B:39:0x0074  */
        /* JADX WARN: Removed duplicated region for block: B:43:0x0088  */
        /* JADX WARN: Removed duplicated region for block: B:51:0x00a4  */
        /* JADX WARN: Removed duplicated region for block: B:54:0x00b4  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public b.i.a.c.b3.b.C0092b a() {
            /*
                Method dump skipped, instructions count: 183
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.b3.v.h.e.a():b.i.a.c.b3.b$b");
        }
    }

    static {
        HashMap hashMap = new HashMap();
        hashMap.put("white", Integer.valueOf(Color.rgb(255, 255, 255)));
        hashMap.put("lime", Integer.valueOf(Color.rgb(0, 255, 0)));
        hashMap.put("cyan", Integer.valueOf(Color.rgb(0, 255, 255)));
        hashMap.put("red", Integer.valueOf(Color.rgb(255, 0, 0)));
        hashMap.put("yellow", Integer.valueOf(Color.rgb(255, 255, 0)));
        hashMap.put("magenta", Integer.valueOf(Color.rgb(255, 0, 255)));
        hashMap.put("blue", Integer.valueOf(Color.rgb(0, 0, 255)));
        hashMap.put("black", Integer.valueOf(Color.rgb(0, 0, 0)));
        c = Collections.unmodifiableMap(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("bg_white", Integer.valueOf(Color.rgb(255, 255, 255)));
        hashMap2.put("bg_lime", Integer.valueOf(Color.rgb(0, 255, 0)));
        hashMap2.put("bg_cyan", Integer.valueOf(Color.rgb(0, 255, 255)));
        hashMap2.put("bg_red", Integer.valueOf(Color.rgb(255, 0, 0)));
        hashMap2.put("bg_yellow", Integer.valueOf(Color.rgb(255, 255, 0)));
        hashMap2.put("bg_magenta", Integer.valueOf(Color.rgb(255, 0, 255)));
        hashMap2.put("bg_blue", Integer.valueOf(Color.rgb(0, 0, 255)));
        hashMap2.put("bg_black", Integer.valueOf(Color.rgb(0, 0, 0)));
        d = Collections.unmodifiableMap(hashMap2);
    }

    public static void a(@Nullable String str, c cVar, List<b> list, SpannableStringBuilder spannableStringBuilder, List<f> list2) {
        char c2;
        int i = cVar.f889b;
        int length = spannableStringBuilder.length();
        String str2 = cVar.a;
        str2.hashCode();
        int hashCode = str2.hashCode();
        int i2 = -1;
        if (hashCode == 0) {
            if (str2.equals("")) {
                c2 = 0;
            }
            c2 = 65535;
        } else if (hashCode == 105) {
            if (str2.equals("i")) {
                c2 = 3;
            }
            c2 = 65535;
        } else if (hashCode == 3314158) {
            if (str2.equals("lang")) {
                c2 = 6;
            }
            c2 = 65535;
        } else if (hashCode == 3511770) {
            if (str2.equals("ruby")) {
                c2 = 7;
            }
            c2 = 65535;
        } else if (hashCode == 98) {
            if (str2.equals("b")) {
                c2 = 1;
            }
            c2 = 65535;
        } else if (hashCode == 99) {
            if (str2.equals("c")) {
                c2 = 2;
            }
            c2 = 65535;
        } else if (hashCode != 117) {
            if (hashCode == 118 && str2.equals("v")) {
                c2 = 5;
            }
            c2 = 65535;
        } else {
            if (str2.equals("u")) {
                c2 = 4;
            }
            c2 = 65535;
        }
        switch (c2) {
            case 0:
            case 5:
            case 6:
                break;
            case 1:
                spannableStringBuilder.setSpan(new StyleSpan(1), i, length, 33);
                break;
            case 2:
                for (String str3 : cVar.d) {
                    Map<String, Integer> map = c;
                    if (map.containsKey(str3)) {
                        spannableStringBuilder.setSpan(new ForegroundColorSpan(map.get(str3).intValue()), i, length, 33);
                    } else {
                        Map<String, Integer> map2 = d;
                        if (map2.containsKey(str3)) {
                            spannableStringBuilder.setSpan(new BackgroundColorSpan(map2.get(str3).intValue()), i, length, 33);
                        }
                    }
                }
                break;
            case 3:
                spannableStringBuilder.setSpan(new StyleSpan(2), i, length, 33);
                break;
            case 4:
                spannableStringBuilder.setSpan(new UnderlineSpan(), i, length, 33);
                break;
            case 7:
                int c3 = c(list2, str, cVar);
                ArrayList arrayList = new ArrayList(list.size());
                arrayList.addAll(list);
                int i3 = b.a;
                Collections.sort(arrayList, b.i.a.c.b3.v.a.j);
                int i4 = cVar.f889b;
                int i5 = 0;
                int i6 = 0;
                while (i5 < arrayList.size()) {
                    if ("rt".equals(((b) arrayList.get(i5)).f888b.a)) {
                        b bVar = (b) arrayList.get(i5);
                        int c4 = c(list2, str, bVar.f888b);
                        if (c4 == i2) {
                            c4 = c3 != i2 ? c3 : 1;
                        }
                        int i7 = bVar.f888b.f889b - i6;
                        int i8 = bVar.c - i6;
                        CharSequence subSequence = spannableStringBuilder.subSequence(i7, i8);
                        spannableStringBuilder.delete(i7, i8);
                        spannableStringBuilder.setSpan(new b.i.a.c.b3.q.c(subSequence.toString(), c4), i4, i7, 33);
                        i6 = subSequence.length() + i6;
                        i4 = i7;
                    }
                    i5++;
                    i2 = -1;
                }
                break;
            default:
                return;
        }
        List<d> b2 = b(list2, str, cVar);
        int i9 = 0;
        while (true) {
            ArrayList arrayList2 = (ArrayList) b2;
            if (i9 < arrayList2.size()) {
                f fVar = ((d) arrayList2.get(i9)).k;
                if (fVar != null) {
                    if (fVar.a() != -1) {
                        b.c.a.a0.d.c(spannableStringBuilder, new StyleSpan(fVar.a()), i, length, 33);
                    }
                    if (fVar.j == 1) {
                        spannableStringBuilder.setSpan(new StrikethroughSpan(), i, length, 33);
                    }
                    if (fVar.k == 1) {
                        spannableStringBuilder.setSpan(new UnderlineSpan(), i, length, 33);
                    }
                    if (fVar.g) {
                        if (fVar.g) {
                            b.c.a.a0.d.c(spannableStringBuilder, new ForegroundColorSpan(fVar.f), i, length, 33);
                        } else {
                            throw new IllegalStateException("Font color not defined");
                        }
                    }
                    if (fVar.i) {
                        if (fVar.i) {
                            b.c.a.a0.d.c(spannableStringBuilder, new BackgroundColorSpan(fVar.h), i, length, 33);
                        } else {
                            throw new IllegalStateException("Background color not defined.");
                        }
                    }
                    if (fVar.e != null) {
                        b.c.a.a0.d.c(spannableStringBuilder, new TypefaceSpan(fVar.e), i, length, 33);
                    }
                    int i10 = fVar.n;
                    if (i10 == 1) {
                        b.c.a.a0.d.c(spannableStringBuilder, new AbsoluteSizeSpan((int) fVar.o, true), i, length, 33);
                    } else if (i10 == 2) {
                        b.c.a.a0.d.c(spannableStringBuilder, new RelativeSizeSpan(fVar.o), i, length, 33);
                    } else if (i10 == 3) {
                        b.c.a.a0.d.c(spannableStringBuilder, new RelativeSizeSpan(fVar.o / 100.0f), i, length, 33);
                    }
                    if (fVar.q) {
                        spannableStringBuilder.setSpan(new b.i.a.c.b3.q.a(), i, length, 33);
                    }
                }
                i9++;
            } else {
                return;
            }
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static List<d> b(List<f> list, @Nullable String str, c cVar) {
        int i;
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < list.size(); i2++) {
            f fVar = list.get(i2);
            String str2 = cVar.a;
            Set<String> set = cVar.d;
            String str3 = cVar.c;
            if (!fVar.a.isEmpty() || !fVar.f885b.isEmpty() || !fVar.c.isEmpty() || !fVar.d.isEmpty()) {
                int b2 = f.b(f.b(f.b(0, fVar.a, str, BasicMeasure.EXACTLY), fVar.f885b, str2, 2), fVar.d, str3, 4);
                i = (b2 == -1 || !set.containsAll(fVar.c)) ? 0 : b2 + (fVar.c.size() * 4);
            } else {
                i = TextUtils.isEmpty(str2);
            }
            if (i > 0) {
                arrayList.add(new d(i, fVar));
            }
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    public static int c(List<f> list, @Nullable String str, c cVar) {
        List<d> b2 = b(list, str, cVar);
        int i = 0;
        while (true) {
            ArrayList arrayList = (ArrayList) b2;
            if (i >= arrayList.size()) {
                return -1;
            }
            int i2 = ((d) arrayList.get(i)).k.p;
            if (i2 != -1) {
                return i2;
            }
            i++;
        }
    }

    @Nullable
    public static g d(@Nullable String str, Matcher matcher, x xVar, List<f> list) {
        e eVar = new e();
        try {
            String group = matcher.group(1);
            Objects.requireNonNull(group);
            eVar.a = j.b(group);
            String group2 = matcher.group(2);
            Objects.requireNonNull(group2);
            eVar.f890b = j.b(group2);
            String group3 = matcher.group(3);
            Objects.requireNonNull(group3);
            e(group3, eVar);
            StringBuilder sb = new StringBuilder();
            String g = xVar.g();
            while (!TextUtils.isEmpty(g)) {
                if (sb.length() > 0) {
                    sb.append("\n");
                }
                sb.append(g.trim());
                g = xVar.g();
            }
            eVar.c = f(str, sb.toString(), list);
            return new g(eVar.a().a(), eVar.a, eVar.f890b);
        } catch (NumberFormatException unused) {
            String valueOf = String.valueOf(matcher.group());
            Log.w("WebvttCueParser", valueOf.length() != 0 ? "Skipping cue with bad header: ".concat(valueOf) : new String("Skipping cue with bad header: "));
            return null;
        }
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    public static void e(String str, e eVar) {
        Matcher matcher = f887b.matcher(str);
        while (matcher.find()) {
            int i = 1;
            String group = matcher.group(1);
            Objects.requireNonNull(group);
            String group2 = matcher.group(2);
            Objects.requireNonNull(group2);
            try {
                if ("line".equals(group)) {
                    g(group2, eVar);
                } else if ("align".equals(group)) {
                    char c2 = 65535;
                    switch (group2.hashCode()) {
                        case -1364013995:
                            if (group2.equals("center")) {
                                c2 = 0;
                                break;
                            }
                            break;
                        case -1074341483:
                            if (group2.equals("middle")) {
                                c2 = 1;
                                break;
                            }
                            break;
                        case 100571:
                            if (group2.equals("end")) {
                                c2 = 2;
                                break;
                            }
                            break;
                        case 3317767:
                            if (group2.equals("left")) {
                                c2 = 3;
                                break;
                            }
                            break;
                        case 108511772:
                            if (group2.equals("right")) {
                                c2 = 4;
                                break;
                            }
                            break;
                        case 109757538:
                            if (group2.equals("start")) {
                                c2 = 5;
                                break;
                            }
                            break;
                    }
                    switch (c2) {
                        case 0:
                        case 1:
                            i = 2;
                            break;
                        case 2:
                            i = 3;
                            break;
                        case 3:
                            i = 4;
                            break;
                        case 4:
                            i = 5;
                            break;
                        case 5:
                            break;
                        default:
                            Log.w("WebvttCueParser", group2.length() != 0 ? "Invalid alignment value: ".concat(group2) : new String("Invalid alignment value: "));
                            i = 2;
                            break;
                    }
                    eVar.d = i;
                } else if (ModelAuditLogEntry.CHANGE_KEY_POSITION.equals(group)) {
                    h(group2, eVar);
                } else if ("size".equals(group)) {
                    eVar.j = j.a(group2);
                } else if ("vertical".equals(group)) {
                    if (group2.equals("lr")) {
                        i = 2;
                    } else if (!group2.equals("rl")) {
                        Log.w("WebvttCueParser", group2.length() != 0 ? "Invalid 'vertical' value: ".concat(group2) : new String("Invalid 'vertical' value: "));
                        i = Integer.MIN_VALUE;
                    }
                    eVar.k = i;
                } else {
                    StringBuilder sb = new StringBuilder(group.length() + 21 + group2.length());
                    sb.append("Unknown cue setting ");
                    sb.append(group);
                    sb.append(":");
                    sb.append(group2);
                    Log.w("WebvttCueParser", sb.toString());
                }
            } catch (NumberFormatException unused) {
                String valueOf = String.valueOf(matcher.group());
                Log.w("WebvttCueParser", valueOf.length() != 0 ? "Skipping bad cue setting: ".concat(valueOf) : new String("Skipping bad cue setting: "));
            }
        }
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    public static SpannedString f(@Nullable String str, String str2, List<f> list) {
        String str3;
        char c2;
        boolean z2;
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        ArrayDeque arrayDeque = new ArrayDeque();
        ArrayList arrayList = new ArrayList();
        int i = 0;
        int i2 = 0;
        while (true) {
            str3 = "";
            if (i2 < str2.length()) {
                char charAt = str2.charAt(i2);
                if (charAt == '&') {
                    i2++;
                    int indexOf = str2.indexOf(59, i2);
                    int indexOf2 = str2.indexOf(32, i2);
                    if (indexOf == -1) {
                        indexOf = indexOf2;
                    } else if (indexOf2 != -1) {
                        indexOf = Math.min(indexOf, indexOf2);
                    }
                    if (indexOf != -1) {
                        String substring = str2.substring(i2, indexOf);
                        substring.hashCode();
                        substring.hashCode();
                        char c3 = 65535;
                        switch (substring.hashCode()) {
                            case 3309:
                                if (substring.equals("gt")) {
                                    c3 = 0;
                                    break;
                                }
                                break;
                            case 3464:
                                if (substring.equals("lt")) {
                                    c3 = 1;
                                    break;
                                }
                                break;
                            case 96708:
                                if (substring.equals("amp")) {
                                    c3 = 2;
                                    break;
                                }
                                break;
                            case 3374865:
                                if (substring.equals("nbsp")) {
                                    c3 = 3;
                                    break;
                                }
                                break;
                        }
                        switch (c3) {
                            case 0:
                                spannableStringBuilder.append('>');
                                break;
                            case 1:
                                spannableStringBuilder.append('<');
                                break;
                            case 2:
                                spannableStringBuilder.append('&');
                                break;
                            case 3:
                                spannableStringBuilder.append(' ');
                                break;
                            default:
                                b.d.b.a.a.g0(substring.length() + 33, "ignoring unsupported entity: '&", substring, ";'", "WebvttCueParser");
                                break;
                        }
                        if (indexOf == indexOf2) {
                            spannableStringBuilder.append((CharSequence) " ");
                        }
                        i2 = indexOf + 1;
                    } else {
                        spannableStringBuilder.append(charAt);
                    }
                } else if (charAt != '<') {
                    spannableStringBuilder.append(charAt);
                    i2++;
                } else {
                    int i3 = i2 + 1;
                    if (i3 < str2.length()) {
                        boolean z3 = str2.charAt(i3) == '/';
                        int indexOf3 = str2.indexOf(62, i3);
                        i3 = indexOf3 == -1 ? str2.length() : indexOf3 + 1;
                        int i4 = i3 - 2;
                        boolean z4 = str2.charAt(i4) == '/';
                        int i5 = i2 + (z3 ? 2 : 1);
                        if (!z4) {
                            i4 = i3 - 1;
                        }
                        String substring2 = str2.substring(i5, i4);
                        if (!substring2.trim().isEmpty()) {
                            String trim = substring2.trim();
                            b.c.a.a0.d.j(!trim.isEmpty());
                            int i6 = e0.a;
                            String str4 = trim.split("[ \\.]", 2)[i];
                            str4.hashCode();
                            switch (str4.hashCode()) {
                                case 98:
                                    if (str4.equals("b")) {
                                        c2 = 0;
                                        break;
                                    }
                                    c2 = 65535;
                                    break;
                                case 99:
                                    if (str4.equals("c")) {
                                        c2 = 1;
                                        break;
                                    }
                                    c2 = 65535;
                                    break;
                                case 105:
                                    if (str4.equals("i")) {
                                        c2 = 2;
                                        break;
                                    }
                                    c2 = 65535;
                                    break;
                                case 117:
                                    if (str4.equals("u")) {
                                        c2 = 3;
                                        break;
                                    }
                                    c2 = 65535;
                                    break;
                                case 118:
                                    if (str4.equals("v")) {
                                        c2 = 4;
                                        break;
                                    }
                                    c2 = 65535;
                                    break;
                                case 3650:
                                    if (str4.equals("rt")) {
                                        c2 = 5;
                                        break;
                                    }
                                    c2 = 65535;
                                    break;
                                case 3314158:
                                    if (str4.equals("lang")) {
                                        c2 = 6;
                                        break;
                                    }
                                    c2 = 65535;
                                    break;
                                case 3511770:
                                    if (str4.equals("ruby")) {
                                        c2 = 7;
                                        break;
                                    }
                                    c2 = 65535;
                                    break;
                                default:
                                    c2 = 65535;
                                    break;
                            }
                            switch (c2) {
                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                case 4:
                                case 5:
                                case 6:
                                case 7:
                                    z2 = true;
                                    break;
                                default:
                                    z2 = false;
                                    break;
                            }
                            if (z2) {
                                if (z3) {
                                    while (!arrayDeque.isEmpty()) {
                                        c cVar = (c) arrayDeque.pop();
                                        a(str, cVar, arrayList, spannableStringBuilder, list);
                                        if (!arrayDeque.isEmpty()) {
                                            arrayList.add(new b(cVar, spannableStringBuilder.length(), null));
                                        } else {
                                            arrayList.clear();
                                        }
                                        if (cVar.a.equals(str4)) {
                                        }
                                    }
                                } else if (!z4) {
                                    int length = spannableStringBuilder.length();
                                    String trim2 = substring2.trim();
                                    b.c.a.a0.d.j(!trim2.isEmpty());
                                    int indexOf4 = trim2.indexOf(" ");
                                    if (indexOf4 != -1) {
                                        str3 = trim2.substring(indexOf4).trim();
                                        trim2 = trim2.substring(i, indexOf4);
                                    }
                                    String[] H = e0.H(trim2, "\\.");
                                    String str5 = H[i];
                                    HashSet hashSet = new HashSet();
                                    for (int i7 = 1; i7 < H.length; i7++) {
                                        hashSet.add(H[i7]);
                                    }
                                    arrayDeque.push(new c(str5, length, str3, hashSet));
                                }
                            }
                        }
                        i2 = i3;
                    }
                    i2 = i3;
                }
                i = 0;
            }
        }
        while (!arrayDeque.isEmpty()) {
            a(str, (c) arrayDeque.pop(), arrayList, spannableStringBuilder, list);
        }
        a(str, new c(str3, 0, str3, Collections.emptySet()), Collections.emptyList(), spannableStringBuilder, list);
        return SpannedString.valueOf(spannableStringBuilder);
    }

    public static void g(String str, e eVar) {
        int indexOf = str.indexOf(44);
        char c2 = 65535;
        if (indexOf != -1) {
            String substring = str.substring(indexOf + 1);
            substring.hashCode();
            int i = 2;
            switch (substring.hashCode()) {
                case -1364013995:
                    if (substring.equals("center")) {
                        c2 = 0;
                        break;
                    }
                    break;
                case -1074341483:
                    if (substring.equals("middle")) {
                        c2 = 1;
                        break;
                    }
                    break;
                case 100571:
                    if (substring.equals("end")) {
                        c2 = 2;
                        break;
                    }
                    break;
                case 109757538:
                    if (substring.equals("start")) {
                        c2 = 3;
                        break;
                    }
                    break;
            }
            switch (c2) {
                case 0:
                case 1:
                    i = 1;
                    break;
                case 2:
                    break;
                case 3:
                    i = 0;
                    break;
                default:
                    Log.w("WebvttCueParser", substring.length() != 0 ? "Invalid anchor value: ".concat(substring) : new String("Invalid anchor value: "));
                    i = Integer.MIN_VALUE;
                    break;
            }
            eVar.g = i;
            str = str.substring(0, indexOf);
        }
        if (str.endsWith("%")) {
            eVar.e = j.a(str);
            eVar.f = 0;
            return;
        }
        eVar.e = Integer.parseInt(str);
        eVar.f = 1;
    }

    public static void h(String str, e eVar) {
        int indexOf = str.indexOf(44);
        char c2 = 65535;
        if (indexOf != -1) {
            String substring = str.substring(indexOf + 1);
            substring.hashCode();
            int i = 2;
            switch (substring.hashCode()) {
                case -1842484672:
                    if (substring.equals("line-left")) {
                        c2 = 0;
                        break;
                    }
                    break;
                case -1364013995:
                    if (substring.equals("center")) {
                        c2 = 1;
                        break;
                    }
                    break;
                case -1276788989:
                    if (substring.equals("line-right")) {
                        c2 = 2;
                        break;
                    }
                    break;
                case -1074341483:
                    if (substring.equals("middle")) {
                        c2 = 3;
                        break;
                    }
                    break;
                case 100571:
                    if (substring.equals("end")) {
                        c2 = 4;
                        break;
                    }
                    break;
                case 109757538:
                    if (substring.equals("start")) {
                        c2 = 5;
                        break;
                    }
                    break;
            }
            switch (c2) {
                case 0:
                case 5:
                    i = 0;
                    break;
                case 1:
                case 3:
                    i = 1;
                    break;
                case 2:
                case 4:
                    break;
                default:
                    Log.w("WebvttCueParser", substring.length() != 0 ? "Invalid anchor value: ".concat(substring) : new String("Invalid anchor value: "));
                    i = Integer.MIN_VALUE;
                    break;
            }
            eVar.i = i;
            str = str.substring(0, indexOf);
        }
        eVar.h = j.a(str);
    }
}
