package b.i.a.c.b3.v;

import b.i.a.c.b3.v.h;
import java.util.Comparator;
/* compiled from: lambda */
/* loaded from: classes3.dex */
public final /* synthetic */ class a implements Comparator {
    public static final /* synthetic */ a j = new a();

    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        return Integer.compare(((h.b) obj).f888b.f889b, ((h.b) obj2).f888b.f889b);
    }
}
