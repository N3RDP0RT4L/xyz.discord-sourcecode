package b.i.a.c.b3;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Layout;
import androidx.core.view.ViewCompat;
import b.i.a.c.w0;
/* compiled from: lambda */
/* loaded from: classes3.dex */
public final /* synthetic */ class a implements w0.a {
    public static final /* synthetic */ a a = new a();

    @Override // b.i.a.c.w0.a
    public final w0 a(Bundle bundle) {
        int i;
        float f;
        float f2;
        int i2;
        int i3;
        boolean z2;
        boolean z3 = false;
        CharSequence charSequence = bundle.getCharSequence(b.b(0));
        CharSequence charSequence2 = charSequence != null ? charSequence : null;
        Layout.Alignment alignment = (Layout.Alignment) bundle.getSerializable(b.b(1));
        Layout.Alignment alignment2 = alignment != null ? alignment : null;
        Layout.Alignment alignment3 = (Layout.Alignment) bundle.getSerializable(b.b(2));
        Layout.Alignment alignment4 = alignment3 != null ? alignment3 : null;
        Bitmap bitmap = (Bitmap) bundle.getParcelable(b.b(3));
        Bitmap bitmap2 = bitmap != null ? bitmap : null;
        if (!bundle.containsKey(b.b(4)) || !bundle.containsKey(b.b(5))) {
            f = -3.4028235E38f;
            i = Integer.MIN_VALUE;
        } else {
            f = bundle.getFloat(b.b(4));
            i = bundle.getInt(b.b(5));
        }
        int i4 = bundle.containsKey(b.b(6)) ? bundle.getInt(b.b(6)) : Integer.MIN_VALUE;
        float f3 = bundle.containsKey(b.b(7)) ? bundle.getFloat(b.b(7)) : -3.4028235E38f;
        int i5 = bundle.containsKey(b.b(8)) ? bundle.getInt(b.b(8)) : Integer.MIN_VALUE;
        if (!bundle.containsKey(b.b(10)) || !bundle.containsKey(b.b(9))) {
            i2 = Integer.MIN_VALUE;
            f2 = -3.4028235E38f;
        } else {
            f2 = bundle.getFloat(b.b(10));
            i2 = bundle.getInt(b.b(9));
        }
        float f4 = bundle.containsKey(b.b(11)) ? bundle.getFloat(b.b(11)) : -3.4028235E38f;
        float f5 = bundle.containsKey(b.b(12)) ? bundle.getFloat(b.b(12)) : -3.4028235E38f;
        if (bundle.containsKey(b.b(13))) {
            i3 = bundle.getInt(b.b(13));
            z2 = true;
        } else {
            z2 = false;
            i3 = ViewCompat.MEASURED_STATE_MASK;
        }
        if (bundle.getBoolean(b.b(14), false)) {
            z3 = z2;
        }
        return new b(charSequence2, alignment2, alignment4, bitmap2, f, i, i4, f3, i5, i2, f2, f4, f5, z3, i3, bundle.containsKey(b.b(15)) ? bundle.getInt(b.b(15)) : Integer.MIN_VALUE, bundle.containsKey(b.b(16)) ? bundle.getFloat(b.b(16)) : 0.0f, null);
    }
}
