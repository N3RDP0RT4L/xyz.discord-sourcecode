package b.i.a.c.b3.o;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.SparseArray;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.w;
import org.objectweb.asm.Opcodes;
/* compiled from: DvbParser.java */
/* loaded from: classes3.dex */
public final class b {
    public static final byte[] a = {0, 7, 8, 15};

    /* renamed from: b  reason: collision with root package name */
    public static final byte[] f858b = {0, 119, -120, -1};
    public static final byte[] c = {0, 17, 34, 51, 68, 85, 102, 119, -120, -103, -86, -69, -52, -35, -18, -1};
    public final Paint d;
    public final Paint e;
    public final Canvas f = new Canvas();
    public final C0094b g = new C0094b(719, 575, 0, 719, 0, 575);
    public final a h = new a(0, new int[]{0, -1, ViewCompat.MEASURED_STATE_MASK, -8421505}, b(), c());
    public final h i;
    public Bitmap j;

    /* compiled from: DvbParser.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final int[] f859b;
        public final int[] c;
        public final int[] d;

        public a(int i, int[] iArr, int[] iArr2, int[] iArr3) {
            this.a = i;
            this.f859b = iArr;
            this.c = iArr2;
            this.d = iArr3;
        }
    }

    /* compiled from: DvbParser.java */
    /* renamed from: b.i.a.c.b3.o.b$b  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0094b {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final int f860b;
        public final int c;
        public final int d;
        public final int e;
        public final int f;

        public C0094b(int i, int i2, int i3, int i4, int i5, int i6) {
            this.a = i;
            this.f860b = i2;
            this.c = i3;
            this.d = i4;
            this.e = i5;
            this.f = i6;
        }
    }

    /* compiled from: DvbParser.java */
    /* loaded from: classes3.dex */
    public static final class c {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f861b;
        public final byte[] c;
        public final byte[] d;

        public c(int i, boolean z2, byte[] bArr, byte[] bArr2) {
            this.a = i;
            this.f861b = z2;
            this.c = bArr;
            this.d = bArr2;
        }
    }

    /* compiled from: DvbParser.java */
    /* loaded from: classes3.dex */
    public static final class d {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final int f862b;
        public final SparseArray<e> c;

        public d(int i, int i2, int i3, SparseArray<e> sparseArray) {
            this.a = i2;
            this.f862b = i3;
            this.c = sparseArray;
        }
    }

    /* compiled from: DvbParser.java */
    /* loaded from: classes3.dex */
    public static final class e {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final int f863b;

        public e(int i, int i2) {
            this.a = i;
            this.f863b = i2;
        }
    }

    /* compiled from: DvbParser.java */
    /* loaded from: classes3.dex */
    public static final class f {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f864b;
        public final int c;
        public final int d;
        public final int e;
        public final int f;
        public final int g;
        public final int h;
        public final int i;
        public final SparseArray<g> j;

        public f(int i, boolean z2, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, SparseArray<g> sparseArray) {
            this.a = i;
            this.f864b = z2;
            this.c = i2;
            this.d = i3;
            this.e = i5;
            this.f = i6;
            this.g = i7;
            this.h = i8;
            this.i = i9;
            this.j = sparseArray;
        }
    }

    /* compiled from: DvbParser.java */
    /* loaded from: classes3.dex */
    public static final class g {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final int f865b;

        public g(int i, int i2, int i3, int i4, int i5, int i6) {
            this.a = i3;
            this.f865b = i4;
        }
    }

    /* compiled from: DvbParser.java */
    /* loaded from: classes3.dex */
    public static final class h {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final int f866b;
        public final SparseArray<f> c = new SparseArray<>();
        public final SparseArray<a> d = new SparseArray<>();
        public final SparseArray<c> e = new SparseArray<>();
        public final SparseArray<a> f = new SparseArray<>();
        public final SparseArray<c> g = new SparseArray<>();
        @Nullable
        public C0094b h;
        @Nullable
        public d i;

        public h(int i, int i2) {
            this.a = i;
            this.f866b = i2;
        }
    }

    public b(int i, int i2) {
        Paint paint = new Paint();
        this.d = paint;
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
        paint.setPathEffect(null);
        Paint paint2 = new Paint();
        this.e = paint2;
        paint2.setStyle(Paint.Style.FILL);
        paint2.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OVER));
        paint2.setPathEffect(null);
        this.i = new h(i, i2);
    }

    public static byte[] a(int i, int i2, w wVar) {
        byte[] bArr = new byte[i];
        for (int i3 = 0; i3 < i; i3++) {
            bArr[i3] = (byte) wVar.g(i2);
        }
        return bArr;
    }

    public static int[] b() {
        int[] iArr = new int[16];
        iArr[0] = 0;
        for (int i = 1; i < 16; i++) {
            if (i < 8) {
                iArr[i] = d(255, (i & 1) != 0 ? 255 : 0, (i & 2) != 0 ? 255 : 0, (i & 4) != 0 ? 255 : 0);
            } else {
                int i2 = i & 1;
                int i3 = Opcodes.LAND;
                int i4 = i2 != 0 ? Opcodes.LAND : 0;
                int i5 = (i & 2) != 0 ? Opcodes.LAND : 0;
                if ((i & 4) == 0) {
                    i3 = 0;
                }
                iArr[i] = d(255, i4, i5, i3);
            }
        }
        return iArr;
    }

    public static int[] c() {
        int[] iArr = new int[256];
        iArr[0] = 0;
        for (int i = 0; i < 256; i++) {
            int i2 = 255;
            if (i < 8) {
                int i3 = (i & 1) != 0 ? 255 : 0;
                int i4 = (i & 2) != 0 ? 255 : 0;
                if ((i & 4) == 0) {
                    i2 = 0;
                }
                iArr[i] = d(63, i3, i4, i2);
            } else {
                int i5 = i & Opcodes.L2I;
                int i6 = Opcodes.TABLESWITCH;
                int i7 = 85;
                if (i5 == 0) {
                    int i8 = ((i & 1) != 0 ? 85 : 0) + ((i & 16) != 0 ? Opcodes.TABLESWITCH : 0);
                    int i9 = ((i & 2) != 0 ? 85 : 0) + ((i & 32) != 0 ? Opcodes.TABLESWITCH : 0);
                    if ((i & 4) == 0) {
                        i7 = 0;
                    }
                    if ((i & 64) == 0) {
                        i6 = 0;
                    }
                    iArr[i] = d(255, i8, i9, i7 + i6);
                } else if (i5 != 8) {
                    int i10 = 43;
                    if (i5 == 128) {
                        int i11 = ((i & 1) != 0 ? 43 : 0) + Opcodes.LAND + ((i & 16) != 0 ? 85 : 0);
                        int i12 = ((i & 2) != 0 ? 43 : 0) + Opcodes.LAND + ((i & 32) != 0 ? 85 : 0);
                        if ((i & 4) == 0) {
                            i10 = 0;
                        }
                        int i13 = i10 + Opcodes.LAND;
                        if ((i & 64) == 0) {
                            i7 = 0;
                        }
                        iArr[i] = d(255, i11, i12, i13 + i7);
                    } else if (i5 == 136) {
                        int i14 = ((i & 1) != 0 ? 43 : 0) + ((i & 16) != 0 ? 85 : 0);
                        int i15 = ((i & 2) != 0 ? 43 : 0) + ((i & 32) != 0 ? 85 : 0);
                        if ((i & 4) == 0) {
                            i10 = 0;
                        }
                        if ((i & 64) == 0) {
                            i7 = 0;
                        }
                        iArr[i] = d(255, i14, i15, i10 + i7);
                    }
                } else {
                    int i16 = ((i & 1) != 0 ? 85 : 0) + ((i & 16) != 0 ? Opcodes.TABLESWITCH : 0);
                    int i17 = ((i & 2) != 0 ? 85 : 0) + ((i & 32) != 0 ? Opcodes.TABLESWITCH : 0);
                    if ((i & 4) == 0) {
                        i7 = 0;
                    }
                    if ((i & 64) == 0) {
                        i6 = 0;
                    }
                    iArr[i] = d(Opcodes.LAND, i16, i17, i7 + i6);
                }
            }
        }
        return iArr;
    }

    public static int d(int i, int i2, int i3, int i4) {
        return (i << 24) | (i2 << 16) | (i3 << 8) | i4;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:107:0x01d1 A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:115:0x020b A[LOOP:3: B:85:0x0163->B:115:0x020b, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:138:0x013b A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:139:0x0205 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:64:0x0112 A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:72:0x0141 A[LOOP:2: B:40:0x00ab->B:72:0x0141, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:87:0x016a  */
    /* JADX WARN: Type inference failed for: r1v7 */
    /* JADX WARN: Type inference failed for: r3v12 */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static void e(byte[] r23, int[] r24, int r25, int r26, int r27, @androidx.annotation.Nullable android.graphics.Paint r28, android.graphics.Canvas r29) {
        /*
            Method dump skipped, instructions count: 554
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.b3.o.b.e(byte[], int[], int, int, int, android.graphics.Paint, android.graphics.Canvas):void");
    }

    public static a f(w wVar, int i) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6 = 8;
        int g2 = wVar.g(8);
        wVar.m(8);
        int i7 = i - 2;
        int i8 = 4;
        int[] iArr = {0, -1, ViewCompat.MEASURED_STATE_MASK, -8421505};
        int[] b2 = b();
        int[] c2 = c();
        while (i7 > 0) {
            int g3 = wVar.g(i6);
            int g4 = wVar.g(i6);
            int i9 = i7 - 2;
            int[] iArr2 = (g4 & 128) != 0 ? iArr : (g4 & 64) != 0 ? b2 : c2;
            if ((g4 & 1) != 0) {
                i5 = wVar.g(i6);
                i4 = wVar.g(i6);
                i3 = wVar.g(i6);
                i2 = wVar.g(i6);
                i7 = i9 - 4;
            } else {
                i5 = wVar.g(6) << 2;
                i4 = wVar.g(i8) << i8;
                i3 = wVar.g(i8) << i8;
                i2 = wVar.g(2) << 6;
                i7 = i9 - 2;
            }
            if (i5 == 0) {
                i4 = 0;
                i3 = 0;
                i2 = 255;
            }
            g2 = g2;
            double d2 = i5;
            iArr = iArr;
            double d3 = i4 - 128;
            double d4 = i3 - 128;
            iArr2[g3] = d((byte) (255 - (i2 & 255)), e0.h((int) ((1.402d * d3) + d2), 0, 255), e0.h((int) ((d2 - (0.34414d * d4)) - (d3 * 0.71414d)), 0, 255), e0.h((int) ((d4 * 1.772d) + d2), 0, 255));
            i6 = 8;
            i8 = 4;
        }
        return new a(g2, iArr, b2, c2);
    }

    public static c g(w wVar) {
        byte[] bArr;
        int g2 = wVar.g(16);
        wVar.m(4);
        int g3 = wVar.g(2);
        boolean f2 = wVar.f();
        wVar.m(1);
        byte[] bArr2 = e0.f;
        if (g3 == 1) {
            wVar.m(wVar.g(8) * 16);
        } else if (g3 == 0) {
            int g4 = wVar.g(16);
            int g5 = wVar.g(16);
            if (g4 > 0) {
                bArr2 = new byte[g4];
                wVar.i(bArr2, 0, g4);
            }
            if (g5 > 0) {
                bArr = new byte[g5];
                wVar.i(bArr, 0, g5);
                return new c(g2, f2, bArr2, bArr);
            }
        }
        bArr = bArr2;
        return new c(g2, f2, bArr2, bArr);
    }
}
