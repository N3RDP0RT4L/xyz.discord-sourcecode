package b.i.a.c.b3.o;

import b.i.a.c.b3.b;
import b.i.a.c.b3.g;
import java.util.List;
/* compiled from: DvbSubtitle.java */
/* loaded from: classes3.dex */
public final class c implements g {
    public final List<b> j;

    public c(List<b> list) {
        this.j = list;
    }

    @Override // b.i.a.c.b3.g
    public int f(long j) {
        return -1;
    }

    @Override // b.i.a.c.b3.g
    public long g(int i) {
        return 0L;
    }

    @Override // b.i.a.c.b3.g
    public List<b> h(long j) {
        return this.j;
    }

    @Override // b.i.a.c.b3.g
    public int i() {
        return 1;
    }
}
