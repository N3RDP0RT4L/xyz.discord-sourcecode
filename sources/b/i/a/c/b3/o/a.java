package b.i.a.c.b3.o;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.util.Log;
import android.util.SparseArray;
import androidx.core.view.ViewCompat;
import b.i.a.c.b3.f;
import b.i.a.c.b3.g;
import b.i.a.c.b3.o.b;
import b.i.a.c.f3.w;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
/* compiled from: DvbDecoder.java */
/* loaded from: classes3.dex */
public final class a extends f {
    public final b n;

    public a(List<byte[]> list) {
        super("DvbDecoder");
        byte[] bArr = list.get(0);
        int length = bArr.length;
        int i = 0 + 1;
        int i2 = i + 1;
        this.n = new b(((bArr[0] & 255) << 8) | (bArr[i] & 255), (bArr[i2 + 1] & 255) | ((bArr[i2] & 255) << 8));
    }

    @Override // b.i.a.c.b3.f
    public g j(byte[] bArr, int i, boolean z2) {
        c cVar;
        c cVar2;
        List list;
        int i2;
        int i3;
        c cVar3;
        SparseArray<b.g> sparseArray;
        SparseArray<b.e> sparseArray2;
        int[] iArr;
        b.f fVar;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        if (z2) {
            b.h hVar = this.n.i;
            hVar.c.clear();
            hVar.d.clear();
            hVar.e.clear();
            hVar.f.clear();
            hVar.g.clear();
            hVar.h = null;
            hVar.i = null;
        }
        b bVar = this.n;
        Objects.requireNonNull(bVar);
        w wVar = new w(bArr, i);
        while (wVar.b() >= 48 && wVar.g(8) == 15) {
            b.h hVar2 = bVar.i;
            int g = wVar.g(8);
            int i10 = 16;
            int g2 = wVar.g(16);
            int g3 = wVar.g(16);
            int d = wVar.d() + g3;
            if (g3 * 8 > wVar.b()) {
                Log.w("DvbParser", "Data field length exceeds limit");
                wVar.m(wVar.b());
            } else {
                switch (g) {
                    case 16:
                        if (g2 == hVar2.a) {
                            b.d dVar = hVar2.i;
                            int i11 = 8;
                            int g4 = wVar.g(8);
                            int g5 = wVar.g(4);
                            int g6 = wVar.g(2);
                            wVar.m(2);
                            int i12 = g3 - 2;
                            SparseArray sparseArray3 = new SparseArray();
                            while (i12 > 0) {
                                int g7 = wVar.g(i11);
                                wVar.m(i11);
                                i12 -= 6;
                                sparseArray3.put(g7, new b.e(wVar.g(16), wVar.g(16)));
                                i11 = 8;
                            }
                            b.d dVar2 = new b.d(g4, g5, g6, sparseArray3);
                            if (g6 == 0) {
                                if (!(dVar == null || dVar.a == g5)) {
                                    hVar2.i = dVar2;
                                    break;
                                }
                            } else {
                                hVar2.i = dVar2;
                                hVar2.c.clear();
                                hVar2.d.clear();
                                hVar2.e.clear();
                                break;
                            }
                        }
                        break;
                    case 17:
                        b.d dVar3 = hVar2.i;
                        if (g2 == hVar2.a && dVar3 != null) {
                            int g8 = wVar.g(8);
                            wVar.m(4);
                            boolean f = wVar.f();
                            wVar.m(3);
                            int g9 = wVar.g(16);
                            int g10 = wVar.g(16);
                            int g11 = wVar.g(3);
                            int g12 = wVar.g(3);
                            wVar.m(2);
                            int g13 = wVar.g(8);
                            int g14 = wVar.g(8);
                            int g15 = wVar.g(4);
                            int g16 = wVar.g(2);
                            wVar.m(2);
                            int i13 = g3 - 10;
                            SparseArray sparseArray4 = new SparseArray();
                            while (i13 > 0) {
                                int g17 = wVar.g(i10);
                                int g18 = wVar.g(2);
                                int g19 = wVar.g(2);
                                int g20 = wVar.g(12);
                                wVar.m(4);
                                int g21 = wVar.g(12);
                                i13 -= 6;
                                if (g18 == 1 || g18 == 2) {
                                    i13 -= 2;
                                    i5 = wVar.g(8);
                                    i4 = wVar.g(8);
                                } else {
                                    i5 = 0;
                                    i4 = 0;
                                }
                                sparseArray4.put(g17, new b.g(g18, g19, g20, g21, i5, i4));
                                i10 = 16;
                            }
                            b.f fVar2 = new b.f(g8, f, g9, g10, g11, g12, g13, g14, g15, g16, sparseArray4);
                            if (dVar3.f862b == 0 && (fVar = hVar2.c.get(g8)) != null) {
                                SparseArray<b.g> sparseArray5 = fVar.j;
                                for (int i14 = 0; i14 < sparseArray5.size(); i14++) {
                                    fVar2.j.put(sparseArray5.keyAt(i14), sparseArray5.valueAt(i14));
                                }
                            }
                            hVar2.c.put(fVar2.a, fVar2);
                            break;
                        }
                        break;
                    case 18:
                        if (g2 != hVar2.a) {
                            if (g2 == hVar2.f866b) {
                                b.a f2 = b.f(wVar, g3);
                                hVar2.f.put(f2.a, f2);
                                break;
                            }
                        } else {
                            b.a f3 = b.f(wVar, g3);
                            hVar2.d.put(f3.a, f3);
                            break;
                        }
                        break;
                    case 19:
                        if (g2 != hVar2.a) {
                            if (g2 == hVar2.f866b) {
                                b.c g22 = b.g(wVar);
                                hVar2.g.put(g22.a, g22);
                                break;
                            }
                        } else {
                            b.c g23 = b.g(wVar);
                            hVar2.e.put(g23.a, g23);
                            break;
                        }
                        break;
                    case 20:
                        if (g2 == hVar2.a) {
                            wVar.m(4);
                            boolean f4 = wVar.f();
                            wVar.m(3);
                            int g24 = wVar.g(16);
                            int g25 = wVar.g(16);
                            if (f4) {
                                int g26 = wVar.g(16);
                                i8 = wVar.g(16);
                                i7 = wVar.g(16);
                                i6 = wVar.g(16);
                                i9 = g26;
                            } else {
                                i8 = g24;
                                i6 = g25;
                                i9 = 0;
                                i7 = 0;
                            }
                            hVar2.h = new b.C0094b(g24, g25, i9, i8, i7, i6);
                            break;
                        }
                        break;
                }
                wVar.n(d - wVar.d());
            }
        }
        b.h hVar3 = bVar.i;
        b.d dVar4 = hVar3.i;
        if (dVar4 == null) {
            list = Collections.emptyList();
            cVar2 = cVar;
        } else {
            b.C0094b bVar2 = hVar3.h;
            if (bVar2 == null) {
                bVar2 = bVar.g;
            }
            Bitmap bitmap = bVar.j;
            if (!(bitmap != null && bVar2.a + 1 == bitmap.getWidth() && bVar2.f860b + 1 == bVar.j.getHeight())) {
                Bitmap createBitmap = Bitmap.createBitmap(bVar2.a + 1, bVar2.f860b + 1, Bitmap.Config.ARGB_8888);
                bVar.j = createBitmap;
                bVar.f.setBitmap(createBitmap);
            }
            ArrayList arrayList = new ArrayList();
            SparseArray<b.e> sparseArray6 = dVar4.c;
            int i15 = 0;
            while (i15 < sparseArray6.size()) {
                bVar.f.save();
                b.e valueAt = sparseArray6.valueAt(i15);
                b.f fVar3 = bVar.i.c.get(sparseArray6.keyAt(i15));
                int i16 = valueAt.a + bVar2.c;
                int i17 = valueAt.f863b + bVar2.e;
                bVar.f.clipRect(i16, i17, Math.min(fVar3.c + i16, bVar2.d), Math.min(fVar3.d + i17, bVar2.f));
                b.a aVar = bVar.i.d.get(fVar3.f);
                if (aVar == null && (aVar = bVar.i.f.get(fVar3.f)) == null) {
                    aVar = bVar.h;
                }
                SparseArray<b.g> sparseArray7 = fVar3.j;
                int i18 = 0;
                while (i18 < sparseArray7.size()) {
                    int keyAt = sparseArray7.keyAt(i18);
                    b.g valueAt2 = sparseArray7.valueAt(i18);
                    b.c cVar4 = bVar.i.e.get(keyAt);
                    if (cVar4 == null) {
                        cVar4 = bVar.i.g.get(keyAt);
                    }
                    if (cVar4 != null) {
                        Paint paint = cVar4.f861b ? null : bVar.d;
                        int i19 = fVar3.e;
                        sparseArray2 = sparseArray6;
                        int i20 = valueAt2.a + i16;
                        int i21 = valueAt2.f865b + i17;
                        sparseArray = sparseArray7;
                        Canvas canvas = bVar.f;
                        cVar3 = cVar;
                        if (i19 == 3) {
                            iArr = aVar.d;
                        } else if (i19 == 2) {
                            iArr = aVar.c;
                        } else {
                            iArr = aVar.f859b;
                        }
                        i3 = i15;
                        int[] iArr2 = iArr;
                        Paint paint2 = paint;
                        b.e(cVar4.c, iArr2, i19, i20, i21, paint2, canvas);
                        b.e(cVar4.d, iArr2, i19, i20, i21 + 1, paint2, canvas);
                    } else {
                        cVar3 = cVar;
                        sparseArray2 = sparseArray6;
                        i3 = i15;
                        sparseArray = sparseArray7;
                    }
                    i18++;
                    sparseArray6 = sparseArray2;
                    sparseArray7 = sparseArray;
                    cVar = cVar3;
                    i15 = i3;
                }
                cVar = cVar;
                sparseArray6 = sparseArray6;
                int i22 = i15;
                if (fVar3.f864b) {
                    int i23 = fVar3.e;
                    if (i23 == 3) {
                        i2 = aVar.d[fVar3.g];
                    } else if (i23 == 2) {
                        i2 = aVar.c[fVar3.h];
                    } else {
                        i2 = aVar.f859b[fVar3.i];
                    }
                    bVar.e.setColor(i2);
                    bVar.f.drawRect(i16, i17, fVar3.c + i16, fVar3.d + i17, bVar.e);
                }
                Bitmap createBitmap2 = Bitmap.createBitmap(bVar.j, i16, i17, fVar3.c, fVar3.d);
                float f5 = bVar2.a;
                float f6 = bVar2.f860b;
                arrayList.add(new b.i.a.c.b3.b(null, null, null, createBitmap2, i17 / f6, 0, 0, i16 / f5, 0, Integer.MIN_VALUE, -3.4028235E38f, fVar3.c / f5, fVar3.d / f6, false, ViewCompat.MEASURED_STATE_MASK, Integer.MIN_VALUE, 0.0f, null));
                bVar.f.drawColor(0, PorterDuff.Mode.CLEAR);
                bVar.f.restore();
                i15 = i22 + 1;
            }
            cVar2 = cVar;
            list = Collections.unmodifiableList(arrayList);
        }
        return new c(list);
    }
}
