package b.i.a.c.b3;

import android.os.Bundle;
import android.os.Parcel;
import androidx.annotation.Nullable;
import b.i.a.c.f3.f;
import b.i.b.b.h0;
import b.i.b.b.p;
import com.google.android.exoplayer2.decoder.DecoderException;
import java.nio.ByteBuffer;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Objects;
/* compiled from: ExoplayerCuesDecoder.java */
/* loaded from: classes3.dex */
public final class d implements h {
    public final c a = new c();

    /* renamed from: b  reason: collision with root package name */
    public final j f839b = new j();
    public final Deque<k> c = new ArrayDeque();
    public int d;
    public boolean e;

    /* compiled from: ExoplayerCuesDecoder.java */
    /* loaded from: classes3.dex */
    public class a extends k {
        public a() {
        }

        @Override // b.i.a.c.v2.f
        public void p() {
            d dVar = d.this;
            b.c.a.a0.d.D(dVar.c.size() < 2);
            b.c.a.a0.d.j(!dVar.c.contains(this));
            q();
            dVar.c.addFirst(this);
        }
    }

    /* compiled from: ExoplayerCuesDecoder.java */
    /* loaded from: classes3.dex */
    public static final class b implements g {
        public final long j;
        public final p<b.i.a.c.b3.b> k;

        public b(long j, p<b.i.a.c.b3.b> pVar) {
            this.j = j;
            this.k = pVar;
        }

        @Override // b.i.a.c.b3.g
        public int f(long j) {
            return this.j > j ? 0 : -1;
        }

        @Override // b.i.a.c.b3.g
        public long g(int i) {
            b.c.a.a0.d.j(i == 0);
            return this.j;
        }

        @Override // b.i.a.c.b3.g
        public List<b.i.a.c.b3.b> h(long j) {
            if (j >= this.j) {
                return this.k;
            }
            b.i.b.b.a<Object> aVar = p.k;
            return h0.l;
        }

        @Override // b.i.a.c.b3.g
        public int i() {
            return 1;
        }
    }

    public d() {
        for (int i = 0; i < 2; i++) {
            this.c.addFirst(new a());
        }
        this.d = 0;
    }

    @Override // b.i.a.c.b3.h
    public void a(long j) {
    }

    @Override // b.i.a.c.v2.d
    @Nullable
    public k b() throws DecoderException {
        b.c.a.a0.d.D(!this.e);
        if (this.d != 2 || this.c.isEmpty()) {
            return null;
        }
        k removeFirst = this.c.removeFirst();
        if (this.f839b.n()) {
            removeFirst.j(4);
        } else {
            j jVar = this.f839b;
            long j = jVar.n;
            c cVar = this.a;
            ByteBuffer byteBuffer = jVar.l;
            Objects.requireNonNull(byteBuffer);
            byte[] array = byteBuffer.array();
            Objects.requireNonNull(cVar);
            Parcel obtain = Parcel.obtain();
            obtain.unmarshall(array, 0, array.length);
            obtain.setDataPosition(0);
            Bundle readBundle = obtain.readBundle(Bundle.class.getClassLoader());
            obtain.recycle();
            ArrayList parcelableArrayList = readBundle.getParcelableArrayList("c");
            Objects.requireNonNull(parcelableArrayList);
            removeFirst.r(this.f839b.n, new b(j, f.a(b.i.a.c.b3.b.k, parcelableArrayList)), 0L);
        }
        this.f839b.p();
        this.d = 0;
        return removeFirst;
    }

    @Override // b.i.a.c.v2.d
    @Nullable
    public j c() throws DecoderException {
        b.c.a.a0.d.D(!this.e);
        if (this.d != 0) {
            return null;
        }
        this.d = 1;
        return this.f839b;
    }

    @Override // b.i.a.c.v2.d
    public void d(j jVar) throws DecoderException {
        j jVar2 = jVar;
        boolean z2 = true;
        b.c.a.a0.d.D(!this.e);
        b.c.a.a0.d.D(this.d == 1);
        if (this.f839b != jVar2) {
            z2 = false;
        }
        b.c.a.a0.d.j(z2);
        this.d = 2;
    }

    @Override // b.i.a.c.v2.d
    public void flush() {
        b.c.a.a0.d.D(!this.e);
        this.f839b.p();
        this.d = 0;
    }

    @Override // b.i.a.c.v2.d
    public void release() {
        this.e = true;
    }
}
