package b.i.a.c.b3.n;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import b.c.a.a0.d;
import b.i.a.c.b3.g;
import b.i.a.c.b3.h;
import b.i.a.c.b3.j;
import b.i.a.c.b3.k;
import b.i.a.c.f3.e0;
import b.i.a.c.v2.f;
import com.google.android.exoplayer2.decoder.DecoderException;
import com.google.android.exoplayer2.text.SubtitleDecoderException;
import java.util.ArrayDeque;
import java.util.Objects;
import java.util.PriorityQueue;
/* compiled from: CeaDecoder.java */
/* loaded from: classes3.dex */
public abstract class e implements h {
    public final ArrayDeque<b> a = new ArrayDeque<>();

    /* renamed from: b  reason: collision with root package name */
    public final ArrayDeque<k> f856b;
    public final PriorityQueue<b> c;
    @Nullable
    public b d;
    public long e;
    public long f;

    /* compiled from: CeaDecoder.java */
    /* loaded from: classes3.dex */
    public static final class b extends j implements Comparable<b> {

        /* renamed from: s  reason: collision with root package name */
        public long f857s;

        public b() {
        }

        @Override // java.lang.Comparable
        public int compareTo(b bVar) {
            b bVar2 = bVar;
            if (n() == bVar2.n()) {
                long j = this.n - bVar2.n;
                if (j == 0) {
                    j = this.f857s - bVar2.f857s;
                    if (j == 0) {
                        return 0;
                    }
                }
                if (j > 0) {
                    return 1;
                }
            } else if (n()) {
                return 1;
            }
            return -1;
        }

        public b(a aVar) {
        }
    }

    /* compiled from: CeaDecoder.java */
    /* loaded from: classes3.dex */
    public static final class c extends k {
        public f.a<c> n;

        public c(f.a<c> aVar) {
            this.n = aVar;
        }

        @Override // b.i.a.c.v2.f
        public final void p() {
            e eVar = ((b.i.a.c.b3.n.b) this.n).a;
            Objects.requireNonNull(eVar);
            q();
            eVar.f856b.add(this);
        }
    }

    public e() {
        for (int i = 0; i < 10; i++) {
            this.a.add(new b(null));
        }
        this.f856b = new ArrayDeque<>();
        for (int i2 = 0; i2 < 2; i2++) {
            this.f856b.add(new c(new b.i.a.c.b3.n.b(this)));
        }
        this.c = new PriorityQueue<>();
    }

    @Override // b.i.a.c.b3.h
    public void a(long j) {
        this.e = j;
    }

    @Override // b.i.a.c.v2.d
    @Nullable
    public j c() throws DecoderException {
        d.D(this.d == null);
        if (this.a.isEmpty()) {
            return null;
        }
        b pollFirst = this.a.pollFirst();
        this.d = pollFirst;
        return pollFirst;
    }

    @Override // b.i.a.c.v2.d
    public void d(j jVar) throws DecoderException {
        j jVar2 = jVar;
        d.j(jVar2 == this.d);
        b bVar = (b) jVar2;
        if (bVar.m()) {
            i(bVar);
        } else {
            long j = this.f;
            this.f = 1 + j;
            bVar.f857s = j;
            this.c.add(bVar);
        }
        this.d = null;
    }

    public abstract g e();

    public abstract void f(j jVar);

    @Override // b.i.a.c.v2.d
    public void flush() {
        this.f = 0L;
        this.e = 0L;
        while (!this.c.isEmpty()) {
            int i = e0.a;
            i(this.c.poll());
        }
        b bVar = this.d;
        if (bVar != null) {
            i(bVar);
            this.d = null;
        }
    }

    @Nullable
    /* renamed from: g */
    public k b() throws SubtitleDecoderException {
        if (this.f856b.isEmpty()) {
            return null;
        }
        while (!this.c.isEmpty()) {
            int i = e0.a;
            if (this.c.peek().n > this.e) {
                break;
            }
            b poll = this.c.poll();
            if (poll.n()) {
                k pollFirst = this.f856b.pollFirst();
                pollFirst.j(4);
                i(poll);
                return pollFirst;
            }
            f(poll);
            if (h()) {
                g e = e();
                k pollFirst2 = this.f856b.pollFirst();
                pollFirst2.r(poll.n, e, RecyclerView.FOREVER_NS);
                i(poll);
                return pollFirst2;
            }
            i(poll);
        }
        return null;
    }

    public abstract boolean h();

    public final void i(b bVar) {
        bVar.p();
        this.a.add(bVar);
    }

    @Override // b.i.a.c.v2.d
    public void release() {
    }
}
