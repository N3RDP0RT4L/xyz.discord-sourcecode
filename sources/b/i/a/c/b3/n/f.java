package b.i.a.c.b3.n;

import b.c.a.a0.d;
import b.i.a.c.b3.b;
import b.i.a.c.b3.g;
import java.util.Collections;
import java.util.List;
/* compiled from: CeaSubtitle.java */
/* loaded from: classes3.dex */
public final class f implements g {
    public final List<b> j;

    public f(List<b> list) {
        this.j = list;
    }

    @Override // b.i.a.c.b3.g
    public int f(long j) {
        return j < 0 ? 0 : -1;
    }

    @Override // b.i.a.c.b3.g
    public long g(int i) {
        d.j(i == 0);
        return 0L;
    }

    @Override // b.i.a.c.b3.g
    public List<b> h(long j) {
        return j >= 0 ? this.j : Collections.emptyList();
    }

    @Override // b.i.a.c.b3.g
    public int i() {
        return 1;
    }
}
