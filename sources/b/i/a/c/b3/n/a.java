package b.i.a.c.b3.n;

import b.i.a.c.b3.n.d;
import java.util.Comparator;
/* compiled from: lambda */
/* loaded from: classes3.dex */
public final /* synthetic */ class a implements Comparator {
    public static final /* synthetic */ a j = new a();

    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        return Integer.compare(((d.a) obj2).f849b, ((d.a) obj).f849b);
    }
}
