package b.i.a.c.b3.n;

import android.text.Layout;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import androidx.annotation.Nullable;
import androidx.core.view.InputDeviceCompat;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;
import b.i.a.c.b3.b;
import b.i.a.c.b3.g;
import b.i.a.c.b3.k;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.x;
import com.google.android.exoplayer2.text.SubtitleDecoderException;
import com.google.android.material.behavior.HideBottomViewOnScrollBehavior;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.objectweb.asm.Opcodes;
/* compiled from: Cea608Decoder.java */
/* loaded from: classes3.dex */
public final class c extends e {
    public static final int[] g = {11, 1, 3, 12, 14, 5, 7, 9};
    public static final int[] h = {0, 4, 8, 12, 16, 20, 24, 28};
    public static final int[] i = {-1, -16711936, -16776961, -16711681, -65536, InputDeviceCompat.SOURCE_ANY, -65281};
    public static final int[] j = {32, 33, 34, 35, 36, 37, 38, 39, 40, 41, HideBottomViewOnScrollBehavior.ENTER_ANIMATION_DURATION, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 233, 93, 237, 243, 250, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 231, 247, 209, 241, 9632};
    public static final int[] k = {Opcodes.FRETURN, Opcodes.ARETURN, Opcodes.ANEWARRAY, Opcodes.ATHROW, 8482, Opcodes.IF_ICMPGE, Opcodes.IF_ICMPGT, 9834, 224, 32, 232, 226, 234, 238, 244, 251};
    public static final int[] l = {Opcodes.INSTANCEOF, 201, 211, 218, 220, 252, 8216, Opcodes.IF_ICMPLT, 42, 39, 8212, Opcodes.RET, 8480, 8226, 8220, 8221, Opcodes.CHECKCAST, Opcodes.MONITORENTER, Opcodes.IFNONNULL, 200, 202, 203, 235, 206, 207, 239, 212, 217, 249, 219, Opcodes.LOOKUPSWITCH, Opcodes.NEW};
    public static final int[] m = {Opcodes.MONITOREXIT, 227, 205, 204, 236, 210, 242, 213, 245, 123, Opcodes.LUSHR, 92, 94, 95, 124, 126, 196, 228, 214, 246, 223, Opcodes.IF_ACMPEQ, Opcodes.IF_ICMPLE, 9474, Opcodes.MULTIANEWARRAY, 229, 216, 248, 9484, 9488, 9492, 9496};
    public static final boolean[] n = {false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false};
    public boolean A;
    public byte B;
    public byte C;
    public boolean E;
    public long F;
    public final int p;
    public final int q;
    public final int r;

    /* renamed from: s  reason: collision with root package name */
    public final long f843s;
    @Nullable
    public List<b> v;
    @Nullable
    public List<b> w;

    /* renamed from: x  reason: collision with root package name */
    public int f844x;

    /* renamed from: y  reason: collision with root package name */
    public int f845y;

    /* renamed from: z  reason: collision with root package name */
    public boolean f846z;
    public final x o = new x();
    public final ArrayList<a> t = new ArrayList<>();
    public a u = new a(0, 4);
    public int D = 0;

    /* compiled from: Cea608Decoder.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public final List<C0093a> a = new ArrayList();

        /* renamed from: b  reason: collision with root package name */
        public final List<SpannableString> f847b = new ArrayList();
        public final StringBuilder c = new StringBuilder();
        public int d;
        public int e;
        public int f;
        public int g;
        public int h;

        /* compiled from: Cea608Decoder.java */
        /* renamed from: b.i.a.c.b3.n.c$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static class C0093a {
            public final int a;

            /* renamed from: b  reason: collision with root package name */
            public final boolean f848b;
            public int c;

            public C0093a(int i, boolean z2, int i2) {
                this.a = i;
                this.f848b = z2;
                this.c = i2;
            }
        }

        public a(int i, int i2) {
            f(i);
            this.h = i2;
        }

        public void a(char c) {
            if (this.c.length() < 32) {
                this.c.append(c);
            }
        }

        public void b() {
            int length = this.c.length();
            if (length > 0) {
                this.c.delete(length - 1, length);
                for (int size = this.a.size() - 1; size >= 0; size--) {
                    C0093a aVar = this.a.get(size);
                    int i = aVar.c;
                    if (i == length) {
                        aVar.c = i - 1;
                    } else {
                        return;
                    }
                }
            }
        }

        @Nullable
        public b c(int i) {
            int i2;
            float f;
            int i3 = this.e + this.f;
            int i4 = 32 - i3;
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
            for (int i5 = 0; i5 < this.f847b.size(); i5++) {
                SpannableString spannableString = this.f847b.get(i5);
                int i6 = e0.a;
                if (spannableString.length() > i4) {
                    spannableString = spannableString.subSequence(0, i4);
                }
                spannableStringBuilder.append(spannableString);
                spannableStringBuilder.append('\n');
            }
            SpannableString d = d();
            int i7 = e0.a;
            int length = d.length();
            CharSequence charSequence = d;
            if (length > i4) {
                charSequence = d.subSequence(0, i4);
            }
            spannableStringBuilder.append(charSequence);
            if (spannableStringBuilder.length() == 0) {
                return null;
            }
            int length2 = i4 - spannableStringBuilder.length();
            int i8 = i3 - length2;
            if (i != Integer.MIN_VALUE) {
                i2 = i;
            } else if (this.g != 2 || (Math.abs(i8) >= 3 && length2 >= 0)) {
                i2 = (this.g != 2 || i8 <= 0) ? 0 : 2;
            } else {
                i2 = 1;
            }
            if (i2 != 1) {
                if (i2 == 2) {
                    i3 = 32 - length2;
                }
                f = ((i3 / 32.0f) * 0.8f) + 0.1f;
            } else {
                f = 0.5f;
            }
            int i9 = this.d;
            if (i9 > 7) {
                i9 = (i9 - 15) - 2;
            } else if (this.g == 1) {
                i9 -= this.h - 1;
            }
            return new b(spannableStringBuilder, Layout.Alignment.ALIGN_NORMAL, null, null, i9, 1, Integer.MIN_VALUE, f, i2, Integer.MIN_VALUE, -3.4028235E38f, -3.4028235E38f, -3.4028235E38f, false, ViewCompat.MEASURED_STATE_MASK, Integer.MIN_VALUE, 0.0f, null);
        }

        public final SpannableString d() {
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(this.c);
            int length = spannableStringBuilder.length();
            int i = 0;
            int i2 = -1;
            int i3 = -1;
            int i4 = 0;
            int i5 = -1;
            boolean z2 = false;
            int i6 = -1;
            while (i < this.a.size()) {
                C0093a aVar = this.a.get(i);
                boolean z3 = aVar.f848b;
                int i7 = aVar.a;
                if (i7 != 8) {
                    boolean z4 = i7 == 7;
                    if (i7 != 7) {
                        i6 = c.i[i7];
                    }
                    z2 = z4;
                }
                int i8 = aVar.c;
                i++;
                if (i8 != (i < this.a.size() ? this.a.get(i).c : length)) {
                    if (i2 != -1 && !z3) {
                        spannableStringBuilder.setSpan(new UnderlineSpan(), i2, i8, 33);
                        i2 = -1;
                    } else if (i2 == -1 && z3) {
                        i2 = i8;
                    }
                    if (i3 != -1 && !z2) {
                        spannableStringBuilder.setSpan(new StyleSpan(2), i3, i8, 33);
                        i3 = -1;
                    } else if (i3 == -1 && z2) {
                        i3 = i8;
                    }
                    if (i6 != i5) {
                        if (i5 != -1) {
                            spannableStringBuilder.setSpan(new ForegroundColorSpan(i5), i4, i8, 33);
                        }
                        i4 = i8;
                        i5 = i6;
                    }
                }
            }
            if (!(i2 == -1 || i2 == length)) {
                spannableStringBuilder.setSpan(new UnderlineSpan(), i2, length, 33);
            }
            if (!(i3 == -1 || i3 == length)) {
                spannableStringBuilder.setSpan(new StyleSpan(2), i3, length, 33);
            }
            if (!(i4 == length || i5 == -1)) {
                spannableStringBuilder.setSpan(new ForegroundColorSpan(i5), i4, length, 33);
            }
            return new SpannableString(spannableStringBuilder);
        }

        public boolean e() {
            return this.a.isEmpty() && this.f847b.isEmpty() && this.c.length() == 0;
        }

        public void f(int i) {
            this.g = i;
            this.a.clear();
            this.f847b.clear();
            this.c.setLength(0);
            this.d = 15;
            this.e = 0;
            this.f = 0;
        }
    }

    public c(String str, int i2, long j2) {
        this.f843s = j2 > 0 ? j2 * 1000 : -9223372036854775807L;
        this.p = "application/x-mp4-cea-608".equals(str) ? 2 : 3;
        if (i2 == 1) {
            this.r = 0;
            this.q = 0;
        } else if (i2 == 2) {
            this.r = 1;
            this.q = 0;
        } else if (i2 == 3) {
            this.r = 0;
            this.q = 1;
        } else if (i2 != 4) {
            Log.w("Cea608Decoder", "Invalid channel. Defaulting to CC1.");
            this.r = 0;
            this.q = 0;
        } else {
            this.r = 1;
            this.q = 1;
        }
        l(0);
        k();
        this.E = true;
        this.F = -9223372036854775807L;
    }

    @Override // b.i.a.c.b3.n.e
    public g e() {
        List<b> list = this.v;
        this.w = list;
        Objects.requireNonNull(list);
        return new f(list);
    }

    /* JADX WARN: Removed duplicated region for block: B:206:0x008f A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:216:0x0015 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:26:0x0069  */
    @Override // b.i.a.c.b3.n.e
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void f(b.i.a.c.b3.j r14) {
        /*
            Method dump skipped, instructions count: 758
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.b3.n.c.f(b.i.a.c.b3.j):void");
    }

    @Override // b.i.a.c.b3.n.e, b.i.a.c.v2.d
    public void flush() {
        super.flush();
        this.v = null;
        this.w = null;
        l(0);
        m(4);
        k();
        this.f846z = false;
        this.A = false;
        this.B = (byte) 0;
        this.C = (byte) 0;
        this.D = 0;
        this.E = true;
        this.F = -9223372036854775807L;
    }

    @Override // b.i.a.c.b3.n.e
    @Nullable
    /* renamed from: g */
    public k b() throws SubtitleDecoderException {
        k pollFirst;
        k g2 = super.b();
        if (g2 != null) {
            return g2;
        }
        long j2 = this.f843s;
        boolean z2 = false;
        if (j2 != -9223372036854775807L) {
            long j3 = this.F;
            if (j3 != -9223372036854775807L && this.e - j3 >= j2) {
                z2 = true;
            }
        }
        if (!z2 || (pollFirst = this.f856b.pollFirst()) == null) {
            return null;
        }
        List<b> emptyList = Collections.emptyList();
        this.v = emptyList;
        this.F = -9223372036854775807L;
        this.w = emptyList;
        Objects.requireNonNull(emptyList);
        pollFirst.r(this.e, new f(emptyList), RecyclerView.FOREVER_NS);
        return pollFirst;
    }

    @Override // b.i.a.c.b3.n.e
    public boolean h() {
        return this.v != this.w;
    }

    public final List<b> j() {
        int size = this.t.size();
        ArrayList arrayList = new ArrayList(size);
        int i2 = 2;
        for (int i3 = 0; i3 < size; i3++) {
            b c = this.t.get(i3).c(Integer.MIN_VALUE);
            arrayList.add(c);
            if (c != null) {
                i2 = Math.min(i2, c.t);
            }
        }
        ArrayList arrayList2 = new ArrayList(size);
        for (int i4 = 0; i4 < size; i4++) {
            b bVar = (b) arrayList.get(i4);
            if (bVar != null) {
                if (bVar.t != i2) {
                    bVar = this.t.get(i4).c(i2);
                    Objects.requireNonNull(bVar);
                }
                arrayList2.add(bVar);
            }
        }
        return arrayList2;
    }

    public final void k() {
        this.u.f(this.f844x);
        this.t.clear();
        this.t.add(this.u);
    }

    public final void l(int i2) {
        int i3 = this.f844x;
        if (i3 != i2) {
            this.f844x = i2;
            if (i2 == 3) {
                for (int i4 = 0; i4 < this.t.size(); i4++) {
                    this.t.get(i4).g = i2;
                }
                return;
            }
            k();
            if (i3 == 3 || i2 == 1 || i2 == 0) {
                this.v = Collections.emptyList();
            }
        }
    }

    public final void m(int i2) {
        this.f845y = i2;
        this.u.h = i2;
    }

    @Override // b.i.a.c.b3.n.e, b.i.a.c.v2.d
    public void release() {
    }
}
