package b.i.a.c.b3.n;

import android.text.Layout;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import b.i.a.c.b3.g;
import b.i.a.c.b3.j;
import b.i.a.c.f3.w;
import b.i.a.c.f3.x;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.objectweb.asm.Opcodes;
/* compiled from: Cea708Decoder.java */
/* loaded from: classes3.dex */
public final class d extends e {
    public final x g = new x();
    public final w h = new w();
    public int i = -1;
    public final int j;
    public final b[] k;
    public b l;
    @Nullable
    public List<b.i.a.c.b3.b> m;
    @Nullable
    public List<b.i.a.c.b3.b> n;
    @Nullable
    public c o;
    public int p;

    /* compiled from: Cea708Decoder.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public final b.i.a.c.b3.b a;

        /* renamed from: b  reason: collision with root package name */
        public final int f849b;

        public a(CharSequence charSequence, Layout.Alignment alignment, float f, int i, int i2, float f2, int i3, float f3, boolean z2, int i4, int i5) {
            int i6;
            boolean z3;
            if (z2) {
                i6 = i4;
                z3 = true;
            } else {
                z3 = false;
                i6 = ViewCompat.MEASURED_STATE_MASK;
            }
            this.a = new b.i.a.c.b3.b(charSequence, alignment, null, null, f, i, i2, f2, i3, Integer.MIN_VALUE, -3.4028235E38f, f3, -3.4028235E38f, z3, i6, Integer.MIN_VALUE, 0.0f, null);
            this.f849b = i5;
        }
    }

    /* compiled from: Cea708Decoder.java */
    /* loaded from: classes3.dex */
    public static final class b {

        /* renamed from: b  reason: collision with root package name */
        public static final int f850b;
        public static final int c;
        public static final int[] h;
        public static final int[] k;
        public int A;
        public int B;
        public int C;
        public int D;
        public int E;
        public int F;
        public int G;
        public final List<SpannableString> l = new ArrayList();
        public final SpannableStringBuilder m = new SpannableStringBuilder();
        public boolean n;
        public boolean o;
        public int p;
        public boolean q;
        public int r;

        /* renamed from: s  reason: collision with root package name */
        public int f851s;
        public int t;
        public int u;
        public boolean v;
        public int w;

        /* renamed from: x  reason: collision with root package name */
        public int f852x;

        /* renamed from: y  reason: collision with root package name */
        public int f853y;

        /* renamed from: z  reason: collision with root package name */
        public int f854z;
        public static final int a = d(2, 2, 2, 0);
        public static final int[] d = {0, 0, 0, 0, 0, 2, 0};
        public static final int[] e = {0, 0, 0, 0, 0, 0, 2};
        public static final int[] f = {3, 3, 3, 3, 3, 3, 1};
        public static final boolean[] g = {false, false, false, true, true, true, false};
        public static final int[] i = {0, 1, 2, 3, 4, 3, 4};
        public static final int[] j = {0, 0, 0, 0, 0, 3, 3};

        static {
            int d2 = d(0, 0, 0, 0);
            f850b = d2;
            int d3 = d(0, 0, 0, 3);
            c = d3;
            h = new int[]{d2, d3, d2, d2, d3, d2, d2};
            k = new int[]{d2, d2, d2, d2, d2, d3, d3};
        }

        public b() {
            f();
        }

        /* JADX WARN: Removed duplicated region for block: B:13:0x0025  */
        /* JADX WARN: Removed duplicated region for block: B:14:0x0028  */
        /* JADX WARN: Removed duplicated region for block: B:16:0x002b  */
        /* JADX WARN: Removed duplicated region for block: B:17:0x002e  */
        /* JADX WARN: Removed duplicated region for block: B:19:0x0031  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public static int d(int r4, int r5, int r6, int r7) {
            /*
                r0 = 0
                r1 = 4
                b.c.a.a0.d.t(r4, r0, r1)
                b.c.a.a0.d.t(r5, r0, r1)
                b.c.a.a0.d.t(r6, r0, r1)
                b.c.a.a0.d.t(r7, r0, r1)
                r1 = 1
                r2 = 255(0xff, float:3.57E-43)
                if (r7 == 0) goto L21
                if (r7 == r1) goto L21
                r3 = 2
                if (r7 == r3) goto L1e
                r3 = 3
                if (r7 == r3) goto L1c
                goto L21
            L1c:
                r7 = 0
                goto L23
            L1e:
                r7 = 127(0x7f, float:1.78E-43)
                goto L23
            L21:
                r7 = 255(0xff, float:3.57E-43)
            L23:
                if (r4 <= r1) goto L28
                r4 = 255(0xff, float:3.57E-43)
                goto L29
            L28:
                r4 = 0
            L29:
                if (r5 <= r1) goto L2e
                r5 = 255(0xff, float:3.57E-43)
                goto L2f
            L2e:
                r5 = 0
            L2f:
                if (r6 <= r1) goto L33
                r0 = 255(0xff, float:3.57E-43)
            L33:
                int r4 = android.graphics.Color.argb(r7, r4, r5, r0)
                return r4
            */
            throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.b3.n.d.b.d(int, int, int, int):int");
        }

        public void a(char c2) {
            if (c2 == '\n') {
                this.l.add(b());
                this.m.clear();
                if (this.A != -1) {
                    this.A = 0;
                }
                if (this.B != -1) {
                    this.B = 0;
                }
                if (this.C != -1) {
                    this.C = 0;
                }
                if (this.E != -1) {
                    this.E = 0;
                }
                while (true) {
                    if ((this.v && this.l.size() >= this.u) || this.l.size() >= 15) {
                        this.l.remove(0);
                    } else {
                        return;
                    }
                }
            } else {
                this.m.append(c2);
            }
        }

        public SpannableString b() {
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(this.m);
            int length = spannableStringBuilder.length();
            if (length > 0) {
                if (this.A != -1) {
                    spannableStringBuilder.setSpan(new StyleSpan(2), this.A, length, 33);
                }
                if (this.B != -1) {
                    spannableStringBuilder.setSpan(new UnderlineSpan(), this.B, length, 33);
                }
                if (this.C != -1) {
                    spannableStringBuilder.setSpan(new ForegroundColorSpan(this.D), this.C, length, 33);
                }
                if (this.E != -1) {
                    spannableStringBuilder.setSpan(new BackgroundColorSpan(this.F), this.E, length, 33);
                }
            }
            return new SpannableString(spannableStringBuilder);
        }

        public void c() {
            this.l.clear();
            this.m.clear();
            this.A = -1;
            this.B = -1;
            this.C = -1;
            this.E = -1;
            this.G = 0;
        }

        public boolean e() {
            return !this.n || (this.l.isEmpty() && this.m.length() == 0);
        }

        public void f() {
            c();
            this.n = false;
            this.o = false;
            this.p = 4;
            this.q = false;
            this.r = 0;
            this.f851s = 0;
            this.t = 0;
            this.u = 15;
            this.v = true;
            this.w = 0;
            this.f852x = 0;
            this.f853y = 0;
            int i2 = f850b;
            this.f854z = i2;
            this.D = a;
            this.F = i2;
        }

        public void g(boolean z2, boolean z3) {
            if (this.A != -1) {
                if (!z2) {
                    this.m.setSpan(new StyleSpan(2), this.A, this.m.length(), 33);
                    this.A = -1;
                }
            } else if (z2) {
                this.A = this.m.length();
            }
            if (this.B != -1) {
                if (!z3) {
                    this.m.setSpan(new UnderlineSpan(), this.B, this.m.length(), 33);
                    this.B = -1;
                }
            } else if (z3) {
                this.B = this.m.length();
            }
        }

        public void h(int i2, int i3) {
            if (!(this.C == -1 || this.D == i2)) {
                this.m.setSpan(new ForegroundColorSpan(this.D), this.C, this.m.length(), 33);
            }
            if (i2 != a) {
                this.C = this.m.length();
                this.D = i2;
            }
            if (!(this.E == -1 || this.F == i3)) {
                this.m.setSpan(new BackgroundColorSpan(this.F), this.E, this.m.length(), 33);
            }
            if (i3 != f850b) {
                this.E = this.m.length();
                this.F = i3;
            }
        }
    }

    /* compiled from: Cea708Decoder.java */
    /* loaded from: classes3.dex */
    public static final class c {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final int f855b;
        public final byte[] c;
        public int d = 0;

        public c(int i, int i2) {
            this.a = i;
            this.f855b = i2;
            this.c = new byte[(i2 * 2) - 1];
        }
    }

    public d(int i, @Nullable List<byte[]> list) {
        this.j = i == -1 ? 1 : i;
        if (!(list == null || (list.size() == 1 && list.get(0).length == 1 && list.get(0)[0] == 1))) {
        }
        this.k = new b[8];
        for (int i2 = 0; i2 < 8; i2++) {
            this.k[i2] = new b();
        }
        this.l = this.k[0];
    }

    @Override // b.i.a.c.b3.n.e
    public g e() {
        List<b.i.a.c.b3.b> list = this.m;
        this.n = list;
        Objects.requireNonNull(list);
        return new f(list);
    }

    @Override // b.i.a.c.b3.n.e
    public void f(j jVar) {
        ByteBuffer byteBuffer = jVar.l;
        Objects.requireNonNull(byteBuffer);
        this.g.C(byteBuffer.array(), byteBuffer.limit());
        while (this.g.a() >= 3) {
            int t = this.g.t() & 7;
            int i = t & 3;
            boolean z2 = false;
            boolean z3 = (t & 4) == 4;
            byte t2 = (byte) this.g.t();
            byte t3 = (byte) this.g.t();
            if (i == 2 || i == 3) {
                if (z3) {
                    if (i == 3) {
                        j();
                        int i2 = (t2 & 192) >> 6;
                        int i3 = this.i;
                        if (!(i3 == -1 || i2 == (i3 + 1) % 4)) {
                            l();
                            Log.w("Cea708Decoder", b.d.b.a.a.g(71, "Sequence number discontinuity. previous=", this.i, " current=", i2));
                        }
                        this.i = i2;
                        int i4 = t2 & 63;
                        if (i4 == 0) {
                            i4 = 64;
                        }
                        c cVar = new c(i2, i4);
                        this.o = cVar;
                        byte[] bArr = cVar.c;
                        int i5 = cVar.d;
                        cVar.d = i5 + 1;
                        bArr[i5] = t3;
                    } else {
                        if (i == 2) {
                            z2 = true;
                        }
                        b.c.a.a0.d.j(z2);
                        c cVar2 = this.o;
                        if (cVar2 == null) {
                            Log.e("Cea708Decoder", "Encountered DTVCC_PACKET_DATA before DTVCC_PACKET_START");
                        } else {
                            byte[] bArr2 = cVar2.c;
                            int i6 = cVar2.d;
                            int i7 = i6 + 1;
                            cVar2.d = i7;
                            bArr2[i6] = t2;
                            cVar2.d = i7 + 1;
                            bArr2[i7] = t3;
                        }
                    }
                    c cVar3 = this.o;
                    if (cVar3.d == (cVar3.f855b * 2) - 1) {
                        j();
                    }
                }
            }
        }
    }

    @Override // b.i.a.c.b3.n.e, b.i.a.c.v2.d
    public void flush() {
        super.flush();
        this.m = null;
        this.n = null;
        this.p = 0;
        this.l = this.k[0];
        l();
        this.o = null;
    }

    @Override // b.i.a.c.b3.n.e
    public boolean h() {
        return this.m != this.n;
    }

    public final void j() {
        b bVar;
        c cVar = this.o;
        if (cVar != null) {
            int i = cVar.d;
            int i2 = (cVar.f855b * 2) - 1;
            if (i != i2) {
                int i3 = cVar.a;
                StringBuilder sb = new StringBuilder(115);
                sb.append("DtvCcPacket ended prematurely; size is ");
                sb.append(i2);
                sb.append(", but current index is ");
                sb.append(i);
                sb.append(" (sequence number ");
                sb.append(i3);
                sb.append(");");
                Log.d("Cea708Decoder", sb.toString());
            }
            w wVar = this.h;
            c cVar2 = this.o;
            wVar.j(cVar2.c, cVar2.d);
            int i4 = 3;
            int g = this.h.g(3);
            int g2 = this.h.g(5);
            int i5 = 7;
            int i6 = 6;
            if (g == 7) {
                this.h.m(2);
                g = this.h.g(6);
                if (g < 7) {
                    b.d.b.a.a.d0(44, "Invalid extended service number: ", g, "Cea708Decoder");
                }
            }
            if (g2 == 0) {
                if (g != 0) {
                    StringBuilder sb2 = new StringBuilder(59);
                    sb2.append("serviceNumber is non-zero (");
                    sb2.append(g);
                    sb2.append(") when blockSize is 0");
                    Log.w("Cea708Decoder", sb2.toString());
                }
            } else if (g == this.j) {
                boolean z2 = false;
                while (this.h.b() > 0) {
                    int g3 = this.h.g(8);
                    if (g3 != 16) {
                        if (g3 > 31) {
                            if (g3 <= 127) {
                                if (g3 == 127) {
                                    this.l.a((char) 9835);
                                } else {
                                    this.l.a((char) (g3 & 255));
                                }
                            } else if (g3 <= 159) {
                                switch (g3) {
                                    case 128:
                                    case Opcodes.LOR /* 129 */:
                                    case 130:
                                    case Opcodes.LXOR /* 131 */:
                                    case Opcodes.IINC /* 132 */:
                                    case Opcodes.I2L /* 133 */:
                                    case Opcodes.I2F /* 134 */:
                                    case Opcodes.I2D /* 135 */:
                                        int i7 = g3 - 128;
                                        if (this.p != i7) {
                                            this.p = i7;
                                            this.l = this.k[i7];
                                            break;
                                        }
                                        break;
                                    case Opcodes.L2I /* 136 */:
                                        for (int i8 = 1; i8 <= 8; i8++) {
                                            if (this.h.f()) {
                                                this.k[8 - i8].c();
                                            }
                                        }
                                        break;
                                    case Opcodes.L2F /* 137 */:
                                        for (int i9 = 1; i9 <= 8; i9++) {
                                            if (this.h.f()) {
                                                this.k[8 - i9].o = true;
                                            }
                                        }
                                        break;
                                    case Opcodes.L2D /* 138 */:
                                        for (int i10 = 1; i10 <= 8; i10++) {
                                            if (this.h.f()) {
                                                this.k[8 - i10].o = false;
                                            }
                                        }
                                        break;
                                    case Opcodes.F2I /* 139 */:
                                        for (int i11 = 1; i11 <= 8; i11++) {
                                            if (this.h.f()) {
                                                this.k[8 - i11].o = !bVar.o;
                                            }
                                        }
                                        break;
                                    case Opcodes.F2L /* 140 */:
                                        for (int i12 = 1; i12 <= 8; i12++) {
                                            if (this.h.f()) {
                                                this.k[8 - i12].f();
                                            }
                                        }
                                        break;
                                    case Opcodes.F2D /* 141 */:
                                        this.h.m(8);
                                        break;
                                    case Opcodes.D2I /* 142 */:
                                        break;
                                    case Opcodes.D2L /* 143 */:
                                        l();
                                        break;
                                    case Opcodes.D2F /* 144 */:
                                        if (!this.l.n) {
                                            this.h.m(16);
                                            break;
                                        } else {
                                            this.h.g(4);
                                            this.h.g(2);
                                            this.h.g(2);
                                            boolean f = this.h.f();
                                            boolean f2 = this.h.f();
                                            this.h.g(3);
                                            this.h.g(3);
                                            this.l.g(f, f2);
                                            break;
                                        }
                                    case Opcodes.I2B /* 145 */:
                                        if (!this.l.n) {
                                            this.h.m(24);
                                            break;
                                        } else {
                                            int d = b.d(this.h.g(2), this.h.g(2), this.h.g(2), this.h.g(2));
                                            int d2 = b.d(this.h.g(2), this.h.g(2), this.h.g(2), this.h.g(2));
                                            this.h.m(2);
                                            b.d(this.h.g(2), this.h.g(2), this.h.g(2), 0);
                                            this.l.h(d, d2);
                                            break;
                                        }
                                    case Opcodes.I2C /* 146 */:
                                        if (!this.l.n) {
                                            this.h.m(16);
                                            break;
                                        } else {
                                            this.h.m(4);
                                            int g4 = this.h.g(4);
                                            this.h.m(2);
                                            this.h.g(6);
                                            b bVar2 = this.l;
                                            if (bVar2.G != g4) {
                                                bVar2.a('\n');
                                            }
                                            bVar2.G = g4;
                                            break;
                                        }
                                    case Opcodes.I2S /* 147 */:
                                    case Opcodes.LCMP /* 148 */:
                                    case Opcodes.FCMPL /* 149 */:
                                    case 150:
                                    default:
                                        Log.w("Cea708Decoder", b.d.b.a.a.f(31, "Invalid C1 command: ", g3));
                                        break;
                                    case Opcodes.DCMPL /* 151 */:
                                        if (!this.l.n) {
                                            this.h.m(32);
                                            break;
                                        } else {
                                            int d3 = b.d(this.h.g(2), this.h.g(2), this.h.g(2), this.h.g(2));
                                            this.h.g(2);
                                            b.d(this.h.g(2), this.h.g(2), this.h.g(2), 0);
                                            this.h.f();
                                            this.h.f();
                                            this.h.g(2);
                                            this.h.g(2);
                                            int g5 = this.h.g(2);
                                            this.h.m(8);
                                            b bVar3 = this.l;
                                            bVar3.f854z = d3;
                                            bVar3.w = g5;
                                            break;
                                        }
                                    case Opcodes.DCMPG /* 152 */:
                                    case 153:
                                    case 154:
                                    case 155:
                                    case 156:
                                    case 157:
                                    case 158:
                                    case Opcodes.IF_ICMPEQ /* 159 */:
                                        int i13 = g3 - 152;
                                        b bVar4 = this.k[i13];
                                        this.h.m(2);
                                        boolean f3 = this.h.f();
                                        boolean f4 = this.h.f();
                                        this.h.f();
                                        int g6 = this.h.g(i4);
                                        boolean f5 = this.h.f();
                                        int g7 = this.h.g(i5);
                                        int g8 = this.h.g(8);
                                        int g9 = this.h.g(4);
                                        int g10 = this.h.g(4);
                                        this.h.m(2);
                                        this.h.g(i6);
                                        this.h.m(2);
                                        int g11 = this.h.g(i4);
                                        int g12 = this.h.g(i4);
                                        bVar4.n = true;
                                        bVar4.o = f3;
                                        bVar4.v = f4;
                                        bVar4.p = g6;
                                        bVar4.q = f5;
                                        bVar4.r = g7;
                                        bVar4.f851s = g8;
                                        bVar4.t = g9;
                                        int i14 = g10 + 1;
                                        if (bVar4.u != i14) {
                                            bVar4.u = i14;
                                            while (true) {
                                                if ((f4 && bVar4.l.size() >= bVar4.u) || bVar4.l.size() >= 15) {
                                                    bVar4.l.remove(0);
                                                }
                                            }
                                        }
                                        if (!(g11 == 0 || bVar4.f852x == g11)) {
                                            bVar4.f852x = g11;
                                            int i15 = g11 - 1;
                                            int i16 = b.h[i15];
                                            boolean z3 = b.g[i15];
                                            int i17 = b.e[i15];
                                            int i18 = b.f[i15];
                                            int i19 = b.d[i15];
                                            bVar4.f854z = i16;
                                            bVar4.w = i19;
                                        }
                                        if (!(g12 == 0 || bVar4.f853y == g12)) {
                                            bVar4.f853y = g12;
                                            int i20 = g12 - 1;
                                            int i21 = b.j[i20];
                                            int i22 = b.i[i20];
                                            bVar4.g(false, false);
                                            bVar4.h(b.a, b.k[i20]);
                                        }
                                        if (this.p != i13) {
                                            this.p = i13;
                                            this.l = this.k[i13];
                                            break;
                                        }
                                        break;
                                }
                            } else if (g3 <= 255) {
                                this.l.a((char) (g3 & 255));
                            } else {
                                b.d.b.a.a.d0(33, "Invalid base command: ", g3, "Cea708Decoder");
                            }
                            z2 = true;
                        } else if (g3 != 0) {
                            if (g3 == i4) {
                                this.m = k();
                            } else if (g3 != 8) {
                                switch (g3) {
                                    case 12:
                                        l();
                                        continue;
                                    case 13:
                                        this.l.a('\n');
                                        continue;
                                    case 14:
                                        break;
                                    default:
                                        if (g3 < 17 || g3 > 23) {
                                            if (g3 < 24 || g3 > 31) {
                                                b.d.b.a.a.d0(31, "Invalid C0 command: ", g3, "Cea708Decoder");
                                                break;
                                            } else {
                                                b.d.b.a.a.d0(54, "Currently unsupported COMMAND_P16 Command: ", g3, "Cea708Decoder");
                                                this.h.m(16);
                                                break;
                                            }
                                        } else {
                                            b.d.b.a.a.d0(55, "Currently unsupported COMMAND_EXT1 Command: ", g3, "Cea708Decoder");
                                            this.h.m(8);
                                            continue;
                                        }
                                }
                            } else {
                                b bVar5 = this.l;
                                int length = bVar5.m.length();
                                if (length > 0) {
                                    bVar5.m.delete(length - 1, length);
                                }
                            }
                        }
                        i4 = 3;
                        i5 = 7;
                        i6 = 6;
                    } else {
                        int g13 = this.h.g(8);
                        if (g13 > 31) {
                            if (g13 <= 127) {
                                if (g13 == 32) {
                                    this.l.a(' ');
                                } else if (g13 == 33) {
                                    this.l.a((char) 160);
                                } else if (g13 == 37) {
                                    this.l.a((char) 8230);
                                } else if (g13 == 42) {
                                    this.l.a((char) 352);
                                } else if (g13 == 44) {
                                    this.l.a((char) 338);
                                } else if (g13 == 63) {
                                    this.l.a((char) 376);
                                } else if (g13 == 57) {
                                    this.l.a((char) 8482);
                                } else if (g13 == 58) {
                                    this.l.a((char) 353);
                                } else if (g13 == 60) {
                                    this.l.a((char) 339);
                                } else if (g13 != 61) {
                                    switch (g13) {
                                        case 48:
                                            this.l.a((char) 9608);
                                            break;
                                        case 49:
                                            this.l.a((char) 8216);
                                            break;
                                        case 50:
                                            this.l.a((char) 8217);
                                            break;
                                        case 51:
                                            this.l.a((char) 8220);
                                            break;
                                        case 52:
                                            this.l.a((char) 8221);
                                            break;
                                        case 53:
                                            this.l.a((char) 8226);
                                            break;
                                        default:
                                            switch (g13) {
                                                case 118:
                                                    this.l.a((char) 8539);
                                                    break;
                                                case 119:
                                                    this.l.a((char) 8540);
                                                    break;
                                                case 120:
                                                    this.l.a((char) 8541);
                                                    break;
                                                case 121:
                                                    this.l.a((char) 8542);
                                                    break;
                                                case 122:
                                                    this.l.a((char) 9474);
                                                    break;
                                                case 123:
                                                    this.l.a((char) 9488);
                                                    break;
                                                case 124:
                                                    this.l.a((char) 9492);
                                                    break;
                                                case Opcodes.LUSHR /* 125 */:
                                                    this.l.a((char) 9472);
                                                    break;
                                                case 126:
                                                    this.l.a((char) 9496);
                                                    break;
                                                case Opcodes.LAND /* 127 */:
                                                    this.l.a((char) 9484);
                                                    break;
                                                default:
                                                    b.d.b.a.a.d0(33, "Invalid G2 character: ", g13, "Cea708Decoder");
                                                    break;
                                            }
                                    }
                                } else {
                                    this.l.a((char) 8480);
                                }
                            } else if (g13 <= 159) {
                                if (g13 <= 135) {
                                    this.h.m(32);
                                } else if (g13 <= 143) {
                                    this.h.m(40);
                                } else if (g13 <= 159) {
                                    this.h.m(2);
                                    this.h.m(this.h.g(6) * 8);
                                }
                            } else if (g13 > 255) {
                                b.d.b.a.a.d0(37, "Invalid extended command: ", g13, "Cea708Decoder");
                            } else if (g13 == 160) {
                                this.l.a((char) 13252);
                            } else {
                                b.d.b.a.a.d0(33, "Invalid G3 character: ", g13, "Cea708Decoder");
                                this.l.a('_');
                            }
                            z2 = true;
                        } else if (g13 > 7) {
                            if (g13 <= 15) {
                                this.h.m(8);
                            } else if (g13 <= 23) {
                                this.h.m(16);
                            } else if (g13 <= 31) {
                                this.h.m(24);
                            }
                        }
                        i4 = 3;
                        i5 = 7;
                        i6 = 6;
                    }
                }
                if (z2) {
                    this.m = k();
                }
            }
            this.o = null;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:29:0x007d  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x0088  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x00a9  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x00ab  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x00b4  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x00b7  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x00c4  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x00c7  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.util.List<b.i.a.c.b3.b> k() {
        /*
            Method dump skipped, instructions count: 277
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.b3.n.d.k():java.util.List");
    }

    public final void l() {
        for (int i = 0; i < 8; i++) {
            this.k[i].f();
        }
    }
}
