package b.i.a.c.b3;
/* compiled from: SimpleSubtitleDecoder.java */
/* loaded from: classes3.dex */
public class e extends k {
    public final /* synthetic */ f n;

    public e(f fVar) {
        this.n = fVar;
    }

    @Override // b.i.a.c.v2.f
    public void p() {
        f fVar = this.n;
        synchronized (fVar.f1141b) {
            q();
            O[] oArr = fVar.f;
            int i = fVar.h;
            fVar.h = i + 1;
            oArr[i] = this;
            fVar.g();
        }
    }
}
