package b.i.a.c.b3;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import b.i.a.c.b3.i;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.q;
import b.i.a.c.f3.t;
import b.i.a.c.j1;
import b.i.a.c.k1;
import b.i.a.c.v0;
import com.google.android.exoplayer2.text.SubtitleDecoderException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
/* compiled from: TextRenderer.java */
/* loaded from: classes3.dex */
public final class m extends v0 implements Handler.Callback {
    public boolean A;
    public int B;
    @Nullable
    public j1 C;
    @Nullable
    public h D;
    @Nullable
    public j E;
    @Nullable
    public k F;
    @Nullable
    public k G;
    public int H;
    public long I;
    @Nullable
    public final Handler u;
    public final l v;
    public final i w;

    /* renamed from: x  reason: collision with root package name */
    public final k1 f840x;

    /* renamed from: y  reason: collision with root package name */
    public boolean f841y;

    /* renamed from: z  reason: collision with root package name */
    public boolean f842z;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public m(l lVar, @Nullable Looper looper) {
        super(3);
        Handler handler;
        i iVar = i.a;
        Objects.requireNonNull(lVar);
        this.v = lVar;
        if (looper == null) {
            handler = null;
        } else {
            int i = e0.a;
            handler = new Handler(looper, this);
        }
        this.u = handler;
        this.w = iVar;
        this.f840x = new k1();
        this.I = -9223372036854775807L;
    }

    @Override // b.i.a.c.v0
    public void B() {
        this.C = null;
        this.I = -9223372036854775807L;
        J();
        N();
        h hVar = this.D;
        Objects.requireNonNull(hVar);
        hVar.release();
        this.D = null;
        this.B = 0;
    }

    @Override // b.i.a.c.v0
    public void D(long j, boolean z2) {
        J();
        this.f841y = false;
        this.f842z = false;
        this.I = -9223372036854775807L;
        if (this.B != 0) {
            O();
            return;
        }
        N();
        h hVar = this.D;
        Objects.requireNonNull(hVar);
        hVar.flush();
    }

    @Override // b.i.a.c.v0
    public void H(j1[] j1VarArr, long j, long j2) {
        this.C = j1VarArr[0];
        if (this.D != null) {
            this.B = 1;
        } else {
            M();
        }
    }

    public final void J() {
        List<b> emptyList = Collections.emptyList();
        Handler handler = this.u;
        if (handler != null) {
            handler.obtainMessage(0, emptyList).sendToTarget();
        } else {
            this.v.e(emptyList);
        }
    }

    public final long K() {
        if (this.H == -1) {
            return RecyclerView.FOREVER_NS;
        }
        Objects.requireNonNull(this.F);
        int i = this.H;
        g gVar = this.F.l;
        Objects.requireNonNull(gVar);
        if (i >= gVar.i()) {
            return RecyclerView.FOREVER_NS;
        }
        k kVar = this.F;
        int i2 = this.H;
        g gVar2 = kVar.l;
        Objects.requireNonNull(gVar2);
        return gVar2.g(i2) + kVar.m;
    }

    public final void L(SubtitleDecoderException subtitleDecoderException) {
        String valueOf = String.valueOf(this.C);
        StringBuilder sb = new StringBuilder(valueOf.length() + 39);
        sb.append("Subtitle decoding failed. streamFormat=");
        sb.append(valueOf);
        q.b("TextRenderer", sb.toString(), subtitleDecoderException);
        J();
        O();
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Code restructure failed: missing block: B:38:0x0099, code lost:
        if (r1.equals("application/pgs") == false) goto L6;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void M() {
        /*
            Method dump skipped, instructions count: 360
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.b3.m.M():void");
    }

    public final void N() {
        this.E = null;
        this.H = -1;
        k kVar = this.F;
        if (kVar != null) {
            kVar.p();
            this.F = null;
        }
        k kVar2 = this.G;
        if (kVar2 != null) {
            kVar2.p();
            this.G = null;
        }
    }

    public final void O() {
        N();
        h hVar = this.D;
        Objects.requireNonNull(hVar);
        hVar.release();
        this.D = null;
        this.B = 0;
        M();
    }

    @Override // b.i.a.c.g2
    public int a(j1 j1Var) {
        Objects.requireNonNull((i.a) this.w);
        String str = j1Var.w;
        if (!("text/vtt".equals(str) || "text/x-ssa".equals(str) || "application/ttml+xml".equals(str) || "application/x-mp4-vtt".equals(str) || "application/x-subrip".equals(str) || "application/x-quicktime-tx3g".equals(str) || "application/cea-608".equals(str) || "application/x-mp4-cea-608".equals(str) || "application/cea-708".equals(str) || "application/dvbsubs".equals(str) || "application/pgs".equals(str) || "text/x-exoplayer-cues".equals(str))) {
            return t.i(j1Var.w) ? 1 : 0;
        }
        return (j1Var.P == 0 ? 4 : 2) | 0 | 0;
    }

    @Override // b.i.a.c.f2
    public boolean b() {
        return this.f842z;
    }

    @Override // b.i.a.c.f2
    public boolean d() {
        return true;
    }

    @Override // b.i.a.c.f2, b.i.a.c.g2
    public String getName() {
        return "TextRenderer";
    }

    @Override // android.os.Handler.Callback
    public boolean handleMessage(Message message) {
        if (message.what == 0) {
            this.v.e((List) message.obj);
            return true;
        }
        throw new IllegalStateException();
    }

    @Override // b.i.a.c.f2
    public void q(long j, long j2) {
        boolean z2;
        if (this.f1136s) {
            long j3 = this.I;
            if (j3 != -9223372036854775807L && j >= j3) {
                N();
                this.f842z = true;
            }
        }
        if (!this.f842z) {
            if (this.G == null) {
                h hVar = this.D;
                Objects.requireNonNull(hVar);
                hVar.a(j);
                try {
                    h hVar2 = this.D;
                    Objects.requireNonNull(hVar2);
                    this.G = hVar2.b();
                } catch (SubtitleDecoderException e) {
                    L(e);
                    return;
                }
            }
            if (this.n == 2) {
                if (this.F != null) {
                    long K = K();
                    z2 = false;
                    while (K <= j) {
                        this.H++;
                        K = K();
                        z2 = true;
                    }
                } else {
                    z2 = false;
                }
                k kVar = this.G;
                if (kVar != null) {
                    if (kVar.n()) {
                        if (!z2 && K() == RecyclerView.FOREVER_NS) {
                            if (this.B == 2) {
                                O();
                            } else {
                                N();
                                this.f842z = true;
                            }
                        }
                    } else if (kVar.k <= j) {
                        k kVar2 = this.F;
                        if (kVar2 != null) {
                            kVar2.p();
                        }
                        g gVar = kVar.l;
                        Objects.requireNonNull(gVar);
                        this.H = gVar.f(j - kVar.m);
                        this.F = kVar;
                        this.G = null;
                        z2 = true;
                    }
                }
                if (z2) {
                    Objects.requireNonNull(this.F);
                    k kVar3 = this.F;
                    g gVar2 = kVar3.l;
                    Objects.requireNonNull(gVar2);
                    List<b> h = gVar2.h(j - kVar3.m);
                    Handler handler = this.u;
                    if (handler != null) {
                        handler.obtainMessage(0, h).sendToTarget();
                    } else {
                        this.v.e(h);
                    }
                }
                if (this.B != 2) {
                    while (!this.f841y) {
                        try {
                            j jVar = this.E;
                            if (jVar == null) {
                                h hVar3 = this.D;
                                Objects.requireNonNull(hVar3);
                                jVar = hVar3.c();
                                if (jVar != null) {
                                    this.E = jVar;
                                } else {
                                    return;
                                }
                            }
                            if (this.B == 1) {
                                jVar.j = 4;
                                h hVar4 = this.D;
                                Objects.requireNonNull(hVar4);
                                hVar4.d(jVar);
                                this.E = null;
                                this.B = 2;
                                return;
                            }
                            int I = I(this.f840x, jVar, 0);
                            if (I == -4) {
                                if (jVar.n()) {
                                    this.f841y = true;
                                    this.A = false;
                                } else {
                                    j1 j1Var = this.f840x.f1023b;
                                    if (j1Var != null) {
                                        jVar.r = j1Var.A;
                                        jVar.s();
                                        this.A &= !jVar.o();
                                    } else {
                                        return;
                                    }
                                }
                                if (!this.A) {
                                    h hVar5 = this.D;
                                    Objects.requireNonNull(hVar5);
                                    hVar5.d(jVar);
                                    this.E = null;
                                }
                            } else if (I == -3) {
                                return;
                            }
                        } catch (SubtitleDecoderException e2) {
                            L(e2);
                            return;
                        }
                    }
                }
            }
        }
    }
}
