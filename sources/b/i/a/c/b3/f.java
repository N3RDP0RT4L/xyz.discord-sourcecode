package b.i.a.c.b3;

import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.v2.h;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.text.SubtitleDecoderException;
import java.nio.ByteBuffer;
import java.util.Objects;
/* compiled from: SimpleSubtitleDecoder.java */
/* loaded from: classes3.dex */
public abstract class f extends h<j, k, SubtitleDecoderException> implements h {
    public f(String str) {
        super(new j[2], new k[2]);
        d.D(this.g == this.e.length);
        for (DecoderInputBuffer decoderInputBuffer : this.e) {
            decoderInputBuffer.r(1024);
        }
    }

    @Override // b.i.a.c.b3.h
    public void a(long j) {
    }

    @Override // b.i.a.c.v2.h
    @Nullable
    public SubtitleDecoderException e(j jVar, k kVar, boolean z2) {
        j jVar2 = jVar;
        k kVar2 = kVar;
        try {
            ByteBuffer byteBuffer = jVar2.l;
            Objects.requireNonNull(byteBuffer);
            kVar2.r(jVar2.n, j(byteBuffer.array(), byteBuffer.limit(), z2), jVar2.r);
            kVar2.j &= Integer.MAX_VALUE;
            return null;
        } catch (SubtitleDecoderException e) {
            return e;
        }
    }

    public abstract g j(byte[] bArr, int i, boolean z2) throws SubtitleDecoderException;
}
