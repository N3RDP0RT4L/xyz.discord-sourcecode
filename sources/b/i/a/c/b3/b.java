package b.i.a.c.b3;

import android.graphics.Bitmap;
import android.text.Layout;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.TextUtils;
import androidx.annotation.ColorInt;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import b.c.a.a0.d;
import b.i.a.c.w0;
import java.util.Arrays;
import java.util.Objects;
/* compiled from: Cue.java */
/* loaded from: classes3.dex */
public final class b implements w0 {
    public static final b j = new b("", null, null, null, -3.4028235E38f, Integer.MIN_VALUE, Integer.MIN_VALUE, -3.4028235E38f, Integer.MIN_VALUE, Integer.MIN_VALUE, -3.4028235E38f, -3.4028235E38f, -3.4028235E38f, false, ViewCompat.MEASURED_STATE_MASK, Integer.MIN_VALUE, 0.0f, null);
    public static final w0.a<b> k = b.i.a.c.b3.a.a;
    public final int A;
    public final float B;
    @Nullable
    public final CharSequence l;
    @Nullable
    public final Layout.Alignment m;
    @Nullable
    public final Layout.Alignment n;
    @Nullable
    public final Bitmap o;
    public final float p;
    public final int q;
    public final int r;

    /* renamed from: s  reason: collision with root package name */
    public final float f834s;
    public final int t;
    public final float u;
    public final float v;
    public final boolean w;

    /* renamed from: x  reason: collision with root package name */
    public final int f835x;

    /* renamed from: y  reason: collision with root package name */
    public final int f836y;

    /* renamed from: z  reason: collision with root package name */
    public final float f837z;

    public b(CharSequence charSequence, Layout.Alignment alignment, Layout.Alignment alignment2, Bitmap bitmap, float f, int i, int i2, float f2, int i3, int i4, float f3, float f4, float f5, boolean z2, int i5, int i6, float f6, a aVar) {
        if (charSequence == null) {
            Objects.requireNonNull(bitmap);
        } else {
            d.j(bitmap == null);
        }
        if (charSequence instanceof Spanned) {
            this.l = SpannedString.valueOf(charSequence);
        } else if (charSequence != null) {
            this.l = charSequence.toString();
        } else {
            this.l = null;
        }
        this.m = alignment;
        this.n = alignment2;
        this.o = bitmap;
        this.p = f;
        this.q = i;
        this.r = i2;
        this.f834s = f2;
        this.t = i3;
        this.u = f4;
        this.v = f5;
        this.w = z2;
        this.f835x = i5;
        this.f836y = i4;
        this.f837z = f3;
        this.A = i6;
        this.B = f6;
    }

    public static String b(int i) {
        return Integer.toString(i, 36);
    }

    public C0092b a() {
        return new C0092b(this, null);
    }

    public boolean equals(@Nullable Object obj) {
        Bitmap bitmap;
        Bitmap bitmap2;
        if (this == obj) {
            return true;
        }
        if (obj == null || b.class != obj.getClass()) {
            return false;
        }
        b bVar = (b) obj;
        return TextUtils.equals(this.l, bVar.l) && this.m == bVar.m && this.n == bVar.n && ((bitmap = this.o) != null ? !((bitmap2 = bVar.o) == null || !bitmap.sameAs(bitmap2)) : bVar.o == null) && this.p == bVar.p && this.q == bVar.q && this.r == bVar.r && this.f834s == bVar.f834s && this.t == bVar.t && this.u == bVar.u && this.v == bVar.v && this.w == bVar.w && this.f835x == bVar.f835x && this.f836y == bVar.f836y && this.f837z == bVar.f837z && this.A == bVar.A && this.B == bVar.B;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.l, this.m, this.n, this.o, Float.valueOf(this.p), Integer.valueOf(this.q), Integer.valueOf(this.r), Float.valueOf(this.f834s), Integer.valueOf(this.t), Float.valueOf(this.u), Float.valueOf(this.v), Boolean.valueOf(this.w), Integer.valueOf(this.f835x), Integer.valueOf(this.f836y), Float.valueOf(this.f837z), Integer.valueOf(this.A), Float.valueOf(this.B)});
    }

    /* compiled from: Cue.java */
    /* renamed from: b.i.a.c.b3.b$b  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0092b {
        @Nullable
        public CharSequence a;
        @Nullable

        /* renamed from: b  reason: collision with root package name */
        public Bitmap f838b;
        @Nullable
        public Layout.Alignment c;
        @Nullable
        public Layout.Alignment d;
        public float e;
        public int f;
        public int g;
        public float h;
        public int i;
        public int j;
        public float k;
        public float l;
        public float m;
        public boolean n;
        @ColorInt
        public int o;
        public int p;
        public float q;

        public C0092b() {
            this.a = null;
            this.f838b = null;
            this.c = null;
            this.d = null;
            this.e = -3.4028235E38f;
            this.f = Integer.MIN_VALUE;
            this.g = Integer.MIN_VALUE;
            this.h = -3.4028235E38f;
            this.i = Integer.MIN_VALUE;
            this.j = Integer.MIN_VALUE;
            this.k = -3.4028235E38f;
            this.l = -3.4028235E38f;
            this.m = -3.4028235E38f;
            this.n = false;
            this.o = ViewCompat.MEASURED_STATE_MASK;
            this.p = Integer.MIN_VALUE;
        }

        public b a() {
            return new b(this.a, this.c, this.d, this.f838b, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o, this.p, this.q, null);
        }

        public C0092b(b bVar, a aVar) {
            this.a = bVar.l;
            this.f838b = bVar.o;
            this.c = bVar.m;
            this.d = bVar.n;
            this.e = bVar.p;
            this.f = bVar.q;
            this.g = bVar.r;
            this.h = bVar.f834s;
            this.i = bVar.t;
            this.j = bVar.f836y;
            this.k = bVar.f837z;
            this.l = bVar.u;
            this.m = bVar.v;
            this.n = bVar.w;
            this.o = bVar.f835x;
            this.p = bVar.A;
            this.q = bVar.B;
        }
    }
}
