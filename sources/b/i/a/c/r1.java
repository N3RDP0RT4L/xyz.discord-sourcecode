package b.i.a.c;

import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.a3.a0;
import b.i.a.c.f3.e0;
/* compiled from: MediaPeriodInfo.java */
/* loaded from: classes3.dex */
public final class r1 {
    public final a0.a a;

    /* renamed from: b  reason: collision with root package name */
    public final long f1054b;
    public final long c;
    public final long d;
    public final long e;
    public final boolean f;
    public final boolean g;
    public final boolean h;
    public final boolean i;

    public r1(a0.a aVar, long j, long j2, long j3, long j4, boolean z2, boolean z3, boolean z4, boolean z5) {
        boolean z6 = false;
        d.j(!z5 || z3);
        d.j(!z4 || z3);
        if (!z2 || (!z3 && !z4 && !z5)) {
            z6 = true;
        }
        d.j(z6);
        this.a = aVar;
        this.f1054b = j;
        this.c = j2;
        this.d = j3;
        this.e = j4;
        this.f = z2;
        this.g = z3;
        this.h = z4;
        this.i = z5;
    }

    public r1 a(long j) {
        if (j == this.c) {
            return this;
        }
        return new r1(this.a, this.f1054b, j, this.d, this.e, this.f, this.g, this.h, this.i);
    }

    public r1 b(long j) {
        if (j == this.f1054b) {
            return this;
        }
        return new r1(this.a, j, this.c, this.d, this.e, this.f, this.g, this.h, this.i);
    }

    public boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || r1.class != obj.getClass()) {
            return false;
        }
        r1 r1Var = (r1) obj;
        return this.f1054b == r1Var.f1054b && this.c == r1Var.c && this.d == r1Var.d && this.e == r1Var.e && this.f == r1Var.f && this.g == r1Var.g && this.h == r1Var.h && this.i == r1Var.i && e0.a(this.a, r1Var.a);
    }

    public int hashCode() {
        return ((((((((((((((((this.a.hashCode() + 527) * 31) + ((int) this.f1054b)) * 31) + ((int) this.c)) * 31) + ((int) this.d)) * 31) + ((int) this.e)) * 31) + (this.f ? 1 : 0)) * 31) + (this.g ? 1 : 0)) * 31) + (this.h ? 1 : 0)) * 31) + (this.i ? 1 : 0);
    }
}
