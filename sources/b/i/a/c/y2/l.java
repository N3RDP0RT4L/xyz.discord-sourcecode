package b.i.a.c.y2;

import android.media.MediaCodec;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.Surface;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import b.c.a.a0.d;
import b.i.a.c.f3.e0;
import b.i.a.c.g3.r;
import b.i.a.c.v2.c;
import b.i.a.c.y2.n;
import b.i.a.c.y2.t;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Objects;
/* compiled from: AsynchronousMediaCodecAdapter.java */
@RequiresApi(23)
/* loaded from: classes3.dex */
public final class l implements t {
    public final MediaCodec a;

    /* renamed from: b  reason: collision with root package name */
    public final o f1302b;
    public final n c;
    public final boolean d;
    public boolean e;
    public int f = 0;
    @Nullable
    public Surface g;

    /* compiled from: AsynchronousMediaCodecAdapter.java */
    /* loaded from: classes3.dex */
    public static final class b implements t.b {
        public final b.i.b.a.l<HandlerThread> a;

        /* renamed from: b  reason: collision with root package name */
        public final b.i.b.a.l<HandlerThread> f1303b;

        public b(final int i, boolean z2) {
            b.i.b.a.l<HandlerThread> aVar = new b.i.b.a.l() { // from class: b.i.a.c.y2.a
                @Override // b.i.b.a.l
                public final Object get() {
                    return new HandlerThread(l.l(i, "ExoPlayer:MediaCodecAsyncAdapter:"));
                }
            };
            b.i.b.a.l<HandlerThread> bVar = new b.i.b.a.l() { // from class: b.i.a.c.y2.b
                @Override // b.i.b.a.l
                public final Object get() {
                    return new HandlerThread(l.l(i, "ExoPlayer:MediaCodecQueueingThread:"));
                }
            };
            this.a = aVar;
            this.f1303b = bVar;
        }

        /* renamed from: b */
        public l a(t.a aVar) throws IOException {
            Exception e;
            MediaCodec mediaCodec;
            String str = aVar.a.a;
            l lVar = null;
            try {
                String valueOf = String.valueOf(str);
                d.f(valueOf.length() != 0 ? "createCodec:".concat(valueOf) : new String("createCodec:"));
                mediaCodec = MediaCodec.createByCodecName(str);
                try {
                    l lVar2 = new l(mediaCodec, this.a.get(), this.f1303b.get(), false, null);
                    try {
                        d.d0();
                        l.k(lVar2, aVar.f1310b, aVar.d, aVar.e, 0, false);
                        return lVar2;
                    } catch (Exception e2) {
                        e = e2;
                        lVar = lVar2;
                        if (lVar != null) {
                            lVar.release();
                        } else if (mediaCodec != null) {
                            mediaCodec.release();
                        }
                        throw e;
                    }
                } catch (Exception e3) {
                    e = e3;
                }
            } catch (Exception e4) {
                e = e4;
                mediaCodec = null;
            }
        }
    }

    public l(MediaCodec mediaCodec, HandlerThread handlerThread, HandlerThread handlerThread2, boolean z2, a aVar) {
        this.a = mediaCodec;
        this.f1302b = new o(handlerThread);
        this.c = new n(mediaCodec, handlerThread2);
        this.d = z2;
    }

    public static void k(l lVar, MediaFormat mediaFormat, Surface surface, MediaCrypto mediaCrypto, int i, boolean z2) {
        o oVar = lVar.f1302b;
        MediaCodec mediaCodec = lVar.a;
        d.D(oVar.c == null);
        oVar.f1306b.start();
        Handler handler = new Handler(oVar.f1306b.getLooper());
        mediaCodec.setCallback(oVar, handler);
        oVar.c = handler;
        d.f("configureCodec");
        lVar.a.configure(mediaFormat, surface, mediaCrypto, i);
        d.d0();
        if (z2) {
            lVar.g = lVar.a.createInputSurface();
        }
        n nVar = lVar.c;
        if (!nVar.h) {
            nVar.d.start();
            nVar.e = new m(nVar, nVar.d.getLooper());
            nVar.h = true;
        }
        d.f("startCodec");
        lVar.a.start();
        d.d0();
        lVar.f = 1;
    }

    public static String l(int i, String str) {
        StringBuilder sb = new StringBuilder(str);
        if (i == 1) {
            sb.append("Audio");
        } else if (i == 2) {
            sb.append("Video");
        } else {
            sb.append("Unknown(");
            sb.append(i);
            sb.append(")");
        }
        return sb.toString();
    }

    @Override // b.i.a.c.y2.t
    public boolean a() {
        return false;
    }

    @Override // b.i.a.c.y2.t
    public void b(int i, int i2, c cVar, long j, int i3) {
        n nVar = this.c;
        nVar.f();
        n.a e = n.e();
        e.a = i;
        e.f1305b = i2;
        e.c = 0;
        e.e = j;
        e.f = i3;
        MediaCodec.CryptoInfo cryptoInfo = e.d;
        cryptoInfo.numSubSamples = cVar.f;
        cryptoInfo.numBytesOfClearData = n.c(cVar.d, cryptoInfo.numBytesOfClearData);
        cryptoInfo.numBytesOfEncryptedData = n.c(cVar.e, cryptoInfo.numBytesOfEncryptedData);
        byte[] b2 = n.b(cVar.f1137b, cryptoInfo.key);
        Objects.requireNonNull(b2);
        cryptoInfo.key = b2;
        byte[] b3 = n.b(cVar.a, cryptoInfo.iv);
        Objects.requireNonNull(b3);
        cryptoInfo.iv = b3;
        cryptoInfo.mode = cVar.c;
        if (e0.a >= 24) {
            cryptoInfo.setPattern(new MediaCodec.CryptoInfo.Pattern(cVar.g, cVar.h));
        }
        nVar.e.obtainMessage(1, e).sendToTarget();
    }

    @Override // b.i.a.c.y2.t
    public void c(int i, long j) {
        this.a.releaseOutputBuffer(i, j);
    }

    @Override // b.i.a.c.y2.t
    public int d() {
        int i;
        o oVar = this.f1302b;
        synchronized (oVar.a) {
            i = -1;
            if (!oVar.b()) {
                IllegalStateException illegalStateException = oVar.m;
                if (illegalStateException == null) {
                    MediaCodec.CodecException codecException = oVar.j;
                    if (codecException == null) {
                        s sVar = oVar.d;
                        if (!(sVar.c == 0)) {
                            i = sVar.b();
                        }
                    } else {
                        oVar.j = null;
                        throw codecException;
                    }
                } else {
                    oVar.m = null;
                    throw illegalStateException;
                }
            }
        }
        return i;
    }

    @Override // b.i.a.c.y2.t
    public int e(MediaCodec.BufferInfo bufferInfo) {
        int i;
        o oVar = this.f1302b;
        synchronized (oVar.a) {
            i = -1;
            if (!oVar.b()) {
                IllegalStateException illegalStateException = oVar.m;
                if (illegalStateException == null) {
                    MediaCodec.CodecException codecException = oVar.j;
                    if (codecException == null) {
                        s sVar = oVar.e;
                        if (!(sVar.c == 0)) {
                            i = sVar.b();
                            if (i >= 0) {
                                d.H(oVar.h);
                                MediaCodec.BufferInfo remove = oVar.f.remove();
                                bufferInfo.set(remove.offset, remove.size, remove.presentationTimeUs, remove.flags);
                            } else if (i == -2) {
                                oVar.h = oVar.g.remove();
                            }
                        }
                    } else {
                        oVar.j = null;
                        throw codecException;
                    }
                } else {
                    oVar.m = null;
                    throw illegalStateException;
                }
            }
        }
        return i;
    }

    @Override // b.i.a.c.y2.t
    public void f(final t.c cVar, Handler handler) {
        m();
        this.a.setOnFrameRenderedListener(new MediaCodec.OnFrameRenderedListener() { // from class: b.i.a.c.y2.c
            @Override // android.media.MediaCodec.OnFrameRenderedListener
            public final void onFrameRendered(MediaCodec mediaCodec, long j, long j2) {
                l lVar = l.this;
                t.c cVar2 = cVar;
                Objects.requireNonNull(lVar);
                ((r.b) cVar2).b(lVar, j, j2);
            }
        }, handler);
    }

    @Override // b.i.a.c.y2.t
    public void flush() {
        this.c.d();
        this.a.flush();
        final o oVar = this.f1302b;
        final MediaCodec mediaCodec = this.a;
        Objects.requireNonNull(mediaCodec);
        final Runnable jVar = new Runnable() { // from class: b.i.a.c.y2.j
            @Override // java.lang.Runnable
            public final void run() {
                mediaCodec.start();
            }
        };
        synchronized (oVar.a) {
            oVar.k++;
            Handler handler = oVar.c;
            int i = e0.a;
            handler.post(new Runnable() { // from class: b.i.a.c.y2.d
                @Override // java.lang.Runnable
                public final void run() {
                    o oVar2 = o.this;
                    Runnable runnable = jVar;
                    synchronized (oVar2.a) {
                        if (!oVar2.l) {
                            long j = oVar2.k - 1;
                            oVar2.k = j;
                            int i2 = (j > 0L ? 1 : (j == 0L ? 0 : -1));
                            if (i2 <= 0) {
                                if (i2 < 0) {
                                    oVar2.c(new IllegalStateException());
                                } else {
                                    oVar2.a();
                                    try {
                                        runnable.run();
                                    } catch (IllegalStateException e) {
                                        oVar2.c(e);
                                    } catch (Exception e2) {
                                        oVar2.c(new IllegalStateException(e2));
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }
    }

    @Override // b.i.a.c.y2.t
    public void g(int i) {
        m();
        this.a.setVideoScalingMode(i);
    }

    @Override // b.i.a.c.y2.t
    public MediaFormat getOutputFormat() {
        MediaFormat mediaFormat;
        o oVar = this.f1302b;
        synchronized (oVar.a) {
            mediaFormat = oVar.h;
            if (mediaFormat == null) {
                throw new IllegalStateException();
            }
        }
        return mediaFormat;
    }

    @Override // b.i.a.c.y2.t
    @Nullable
    public ByteBuffer h(int i) {
        return this.a.getInputBuffer(i);
    }

    @Override // b.i.a.c.y2.t
    public void i(Surface surface) {
        m();
        this.a.setOutputSurface(surface);
    }

    @Override // b.i.a.c.y2.t
    @Nullable
    public ByteBuffer j(int i) {
        return this.a.getOutputBuffer(i);
    }

    public final void m() {
        if (this.d) {
            try {
                this.c.a();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new IllegalStateException(e);
            }
        }
    }

    @Override // b.i.a.c.y2.t
    public void queueInputBuffer(int i, int i2, int i3, long j, int i4) {
        n nVar = this.c;
        nVar.f();
        n.a e = n.e();
        e.a = i;
        e.f1305b = i2;
        e.c = i3;
        e.e = j;
        e.f = i4;
        Handler handler = nVar.e;
        int i5 = e0.a;
        handler.obtainMessage(0, e).sendToTarget();
    }

    @Override // b.i.a.c.y2.t
    public void release() {
        try {
            if (this.f == 1) {
                n nVar = this.c;
                if (nVar.h) {
                    nVar.d();
                    nVar.d.quit();
                }
                nVar.h = false;
                o oVar = this.f1302b;
                synchronized (oVar.a) {
                    oVar.l = true;
                    oVar.f1306b.quit();
                    oVar.a();
                }
            }
            this.f = 2;
        } finally {
            Surface surface = this.g;
            if (surface != null) {
                surface.release();
            }
            if (!this.e) {
                this.a.release();
                this.e = true;
            }
        }
    }

    @Override // b.i.a.c.y2.t
    public void releaseOutputBuffer(int i, boolean z2) {
        this.a.releaseOutputBuffer(i, z2);
    }

    @Override // b.i.a.c.y2.t
    public void setParameters(Bundle bundle) {
        m();
        this.a.setParameters(bundle);
    }
}
