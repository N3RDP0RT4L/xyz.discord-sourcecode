package b.i.a.c.y2;

import android.graphics.Point;
import android.media.MediaCodecInfo;
import android.util.Log;
import android.util.Pair;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.annotation.VisibleForTesting;
import b.d.b.a.a;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.t;
import b.i.a.c.j1;
import b.i.a.c.v2.g;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil;
import java.util.Objects;
/* compiled from: MediaCodecInfo.java */
/* loaded from: classes3.dex */
public final class u {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1311b;
    public final String c;
    @Nullable
    public final MediaCodecInfo.CodecCapabilities d;
    public final boolean e;
    public final boolean f;
    public final boolean g;

    @VisibleForTesting
    public u(String str, String str2, String str3, @Nullable MediaCodecInfo.CodecCapabilities codecCapabilities, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7) {
        Objects.requireNonNull(str);
        this.a = str;
        this.f1311b = str2;
        this.c = str3;
        this.d = codecCapabilities;
        this.e = z5;
        this.f = z7;
        this.g = t.j(str2);
    }

    @RequiresApi(21)
    public static Point a(MediaCodecInfo.VideoCapabilities videoCapabilities, int i, int i2) {
        int widthAlignment = videoCapabilities.getWidthAlignment();
        int heightAlignment = videoCapabilities.getHeightAlignment();
        return new Point(e0.f(i, widthAlignment) * widthAlignment, e0.f(i2, heightAlignment) * heightAlignment);
    }

    @RequiresApi(21)
    public static boolean b(MediaCodecInfo.VideoCapabilities videoCapabilities, int i, int i2, double d) {
        Point a = a(videoCapabilities, i, i2);
        int i3 = a.x;
        int i4 = a.y;
        if (d == -1.0d || d < 1.0d) {
            return videoCapabilities.isSizeSupported(i3, i4);
        }
        return videoCapabilities.areSizeAndRateSupported(i3, i4, Math.floor(d));
    }

    /* JADX WARN: Code restructure failed: missing block: B:46:0x0076, code lost:
        if ((b.i.a.c.f3.e0.a >= 21 && r15.isFeatureSupported("secure-playback")) != false) goto L48;
     */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0048  */
    /* JADX WARN: Removed duplicated region for block: B:29:0x004f  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x0065  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static b.i.a.c.y2.u i(java.lang.String r12, java.lang.String r13, java.lang.String r14, @androidx.annotation.Nullable android.media.MediaCodecInfo.CodecCapabilities r15, boolean r16, boolean r17, boolean r18, boolean r19, boolean r20) {
        /*
            r1 = r12
            r4 = r15
            b.i.a.c.y2.u r11 = new b.i.a.c.y2.u
            r0 = 1
            r2 = 0
            if (r19 != 0) goto L4a
            if (r4 == 0) goto L4a
            int r3 = b.i.a.c.f3.e0.a
            r5 = 19
            if (r3 < r5) goto L1a
            java.lang.String r5 = "adaptive-playback"
            boolean r5 = r15.isFeatureSupported(r5)
            if (r5 == 0) goto L1a
            r5 = 1
            goto L1b
        L1a:
            r5 = 0
        L1b:
            if (r5 == 0) goto L4a
            r5 = 22
            if (r3 > r5) goto L45
            java.lang.String r3 = b.i.a.c.f3.e0.d
            java.lang.String r5 = "ODROID-XU3"
            boolean r5 = r5.equals(r3)
            if (r5 != 0) goto L33
            java.lang.String r5 = "Nexus 10"
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L45
        L33:
            java.lang.String r3 = "OMX.Exynos.AVC.Decoder"
            boolean r3 = r3.equals(r12)
            if (r3 != 0) goto L43
            java.lang.String r3 = "OMX.Exynos.AVC.Decoder.secure"
            boolean r3 = r3.equals(r12)
            if (r3 == 0) goto L45
        L43:
            r3 = 1
            goto L46
        L45:
            r3 = 0
        L46:
            if (r3 != 0) goto L4a
            r8 = 1
            goto L4b
        L4a:
            r8 = 0
        L4b:
            r3 = 21
            if (r4 == 0) goto L62
            int r5 = b.i.a.c.f3.e0.a
            if (r5 < r3) goto L5d
            java.lang.String r5 = "tunneled-playback"
            boolean r5 = r15.isFeatureSupported(r5)
            if (r5 == 0) goto L5d
            r5 = 1
            goto L5e
        L5d:
            r5 = 0
        L5e:
            if (r5 == 0) goto L62
            r9 = 1
            goto L63
        L62:
            r9 = 0
        L63:
            if (r20 != 0) goto L7b
            if (r4 == 0) goto L79
            int r5 = b.i.a.c.f3.e0.a
            if (r5 < r3) goto L75
            java.lang.String r3 = "secure-playback"
            boolean r3 = r15.isFeatureSupported(r3)
            if (r3 == 0) goto L75
            r3 = 1
            goto L76
        L75:
            r3 = 0
        L76:
            if (r3 == 0) goto L79
            goto L7b
        L79:
            r10 = 0
            goto L7c
        L7b:
            r10 = 1
        L7c:
            r0 = r11
            r1 = r12
            r2 = r13
            r3 = r14
            r4 = r15
            r5 = r16
            r6 = r17
            r7 = r18
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10)
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.y2.u.i(java.lang.String, java.lang.String, java.lang.String, android.media.MediaCodecInfo$CodecCapabilities, boolean, boolean, boolean, boolean, boolean):b.i.a.c.y2.u");
    }

    public g c(j1 j1Var, j1 j1Var2) {
        boolean z2 = false;
        int i = !e0.a(j1Var.w, j1Var2.w) ? 8 : 0;
        if (this.g) {
            if (j1Var.E != j1Var2.E) {
                i |= 1024;
            }
            if (!this.e && !(j1Var.B == j1Var2.B && j1Var.C == j1Var2.C)) {
                i |= 512;
            }
            if (!e0.a(j1Var.I, j1Var2.I)) {
                i |= 2048;
            }
            String str = this.a;
            if (e0.d.startsWith("SM-T230") && "OMX.MARVELL.VIDEO.HW.CODA7542DECODER".equals(str)) {
                z2 = true;
            }
            if (z2 && !j1Var.c(j1Var2)) {
                i |= 2;
            }
            if (i == 0) {
                return new g(this.a, j1Var, j1Var2, j1Var.c(j1Var2) ? 3 : 2, 0);
            }
        } else {
            if (j1Var.J != j1Var2.J) {
                i |= 4096;
            }
            if (j1Var.K != j1Var2.K) {
                i |= 8192;
            }
            if (j1Var.L != j1Var2.L) {
                i |= 16384;
            }
            if (i == 0 && "audio/mp4a-latm".equals(this.f1311b)) {
                Pair<Integer, Integer> c = MediaCodecUtil.c(j1Var);
                Pair<Integer, Integer> c2 = MediaCodecUtil.c(j1Var2);
                if (!(c == null || c2 == null)) {
                    int intValue = ((Integer) c.first).intValue();
                    int intValue2 = ((Integer) c2.first).intValue();
                    if (intValue == 42 && intValue2 == 42) {
                        return new g(this.a, j1Var, j1Var2, 3, 0);
                    }
                }
            }
            if (!j1Var.c(j1Var2)) {
                i |= 32;
            }
            if ("audio/opus".equals(this.f1311b)) {
                i |= 2;
            }
            if (i == 0) {
                return new g(this.a, j1Var, j1Var2, 1, 0);
            }
        }
        return new g(this.a, j1Var, j1Var2, 0, i);
    }

    public MediaCodecInfo.CodecProfileLevel[] d() {
        MediaCodecInfo.CodecProfileLevel[] codecProfileLevelArr;
        MediaCodecInfo.CodecCapabilities codecCapabilities = this.d;
        return (codecCapabilities == null || (codecProfileLevelArr = codecCapabilities.profileLevels) == null) ? new MediaCodecInfo.CodecProfileLevel[0] : codecProfileLevelArr;
    }

    /* JADX WARN: Removed duplicated region for block: B:158:? A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:159:? A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:72:0x0114 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:73:0x0115  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public boolean e(b.i.a.c.j1 r13) throws com.google.android.exoplayer2.mediacodec.MediaCodecUtil.DecoderQueryException {
        /*
            Method dump skipped, instructions count: 635
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.y2.u.e(b.i.a.c.j1):boolean");
    }

    public boolean f(j1 j1Var) {
        if (this.g) {
            return this.e;
        }
        Pair<Integer, Integer> c = MediaCodecUtil.c(j1Var);
        return c != null && ((Integer) c.first).intValue() == 42;
    }

    @RequiresApi(21)
    public boolean g(int i, int i2, double d) {
        MediaCodecInfo.CodecCapabilities codecCapabilities = this.d;
        if (codecCapabilities == null) {
            h("sizeAndRate.caps");
            return false;
        }
        MediaCodecInfo.VideoCapabilities videoCapabilities = codecCapabilities.getVideoCapabilities();
        if (videoCapabilities == null) {
            h("sizeAndRate.vCaps");
            return false;
        }
        if (!b(videoCapabilities, i, i2, d)) {
            if (i < i2) {
                if ((!"OMX.MTK.VIDEO.DECODER.HEVC".equals(this.a) || !"mcv5a".equals(e0.f964b)) && b(videoCapabilities, i2, i, d)) {
                    StringBuilder sb = new StringBuilder(69);
                    sb.append("sizeAndRate.rotated, ");
                    sb.append(i);
                    sb.append("x");
                    sb.append(i2);
                    sb.append("x");
                    sb.append(d);
                    String sb2 = sb.toString();
                    String str = this.a;
                    String str2 = this.f1311b;
                    String str3 = e0.e;
                    StringBuilder Q = a.Q(a.b(str3, a.b(str2, a.b(str, a.b(sb2, 25)))), "AssumedSupport [", sb2, "] [", str);
                    a.q0(Q, ", ", str2, "] [", str3);
                    Q.append("]");
                    Log.d("MediaCodecInfo", Q.toString());
                }
            }
            StringBuilder sb3 = new StringBuilder(69);
            sb3.append("sizeAndRate.support, ");
            sb3.append(i);
            sb3.append("x");
            sb3.append(i2);
            sb3.append("x");
            sb3.append(d);
            h(sb3.toString());
            return false;
        }
        return true;
    }

    public final void h(String str) {
        String str2 = this.a;
        String str3 = this.f1311b;
        String str4 = e0.e;
        StringBuilder Q = a.Q(a.b(str4, a.b(str3, a.b(str2, a.b(str, 20)))), "NoSupport [", str, "] [", str2);
        a.q0(Q, ", ", str3, "] [", str4);
        Q.append("]");
        Log.d("MediaCodecInfo", Q.toString());
    }

    public String toString() {
        return this.a;
    }
}
