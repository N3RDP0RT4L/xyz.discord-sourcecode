package b.i.a.c.y2;

import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
/* compiled from: BatchBuffer.java */
/* loaded from: classes3.dex */
public final class p extends DecoderInputBuffer {
    public long r;

    /* renamed from: s  reason: collision with root package name */
    public int f1307s;
    public int t = 32;

    public p() {
        super(2);
    }

    @Override // com.google.android.exoplayer2.decoder.DecoderInputBuffer
    public void p() {
        super.p();
        this.f1307s = 0;
    }

    /* JADX WARN: Code restructure failed: missing block: B:15:0x0049, code lost:
        if ((r0.remaining() + r3.position()) > 3072000) goto L7;
     */
    /* JADX WARN: Removed duplicated region for block: B:17:0x004e A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:18:0x004f  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public boolean u(com.google.android.exoplayer2.decoder.DecoderInputBuffer r5) {
        /*
            r4 = this;
            boolean r0 = r5.t()
            r1 = 1
            r0 = r0 ^ r1
            b.c.a.a0.d.j(r0)
            boolean r0 = r5.l()
            r0 = r0 ^ r1
            b.c.a.a0.d.j(r0)
            boolean r0 = r5.n()
            r0 = r0 ^ r1
            b.c.a.a0.d.j(r0)
            boolean r0 = r4.v()
            r2 = 0
            if (r0 != 0) goto L22
        L20:
            r0 = 1
            goto L4c
        L22:
            int r0 = r4.f1307s
            int r3 = r4.t
            if (r0 < r3) goto L2a
        L28:
            r0 = 0
            goto L4c
        L2a:
            boolean r0 = r5.m()
            boolean r3 = r4.m()
            if (r0 == r3) goto L35
            goto L28
        L35:
            java.nio.ByteBuffer r0 = r5.l
            if (r0 == 0) goto L20
            java.nio.ByteBuffer r3 = r4.l
            if (r3 == 0) goto L20
            int r3 = r3.position()
            int r0 = r0.remaining()
            int r0 = r0 + r3
            r3 = 3072000(0x2ee000, float:4.304789E-39)
            if (r0 <= r3) goto L20
            goto L28
        L4c:
            if (r0 != 0) goto L4f
            return r2
        L4f:
            int r0 = r4.f1307s
            int r2 = r0 + 1
            r4.f1307s = r2
            if (r0 != 0) goto L63
            long r2 = r5.n
            r4.n = r2
            boolean r0 = r5.o()
            if (r0 == 0) goto L63
            r4.j = r1
        L63:
            boolean r0 = r5.m()
            if (r0 == 0) goto L6d
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            r4.j = r0
        L6d:
            java.nio.ByteBuffer r0 = r5.l
            if (r0 == 0) goto L7d
            int r2 = r0.remaining()
            r4.r(r2)
            java.nio.ByteBuffer r2 = r4.l
            r2.put(r0)
        L7d:
            long r2 = r5.n
            r4.r = r2
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.y2.p.u(com.google.android.exoplayer2.decoder.DecoderInputBuffer):boolean");
    }

    public boolean v() {
        return this.f1307s > 0;
    }
}
