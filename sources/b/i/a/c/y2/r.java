package b.i.a.c.y2;

import android.media.MediaCodec;
import android.util.Log;
import androidx.core.app.NotificationCompat;
import b.c.a.a0.d;
import b.i.a.c.f3.e0;
import b.i.a.c.y2.l;
import b.i.a.c.y2.t;
import java.io.IOException;
import java.util.Objects;
import org.webrtc.MediaStreamTrack;
/* compiled from: DefaultMediaCodecAdapterFactory.java */
/* loaded from: classes3.dex */
public final class r implements t.b {
    @Override // b.i.a.c.y2.t.b
    public t a(t.a aVar) throws IOException {
        Throwable e;
        MediaCodec createByCodecName;
        String str;
        if (e0.a >= 31) {
            int g = b.i.a.c.f3.t.g(aVar.c.w);
            int i = e0.a;
            switch (g) {
                case -2:
                    str = "none";
                    break;
                case -1:
                default:
                    if (g < 10000) {
                        str = "?";
                        break;
                    } else {
                        StringBuilder sb = new StringBuilder(20);
                        sb.append("custom (");
                        sb.append(g);
                        sb.append(")");
                        str = sb.toString();
                        break;
                    }
                case 0:
                    str = "default";
                    break;
                case 1:
                    str = MediaStreamTrack.AUDIO_TRACK_KIND;
                    break;
                case 2:
                    str = MediaStreamTrack.VIDEO_TRACK_KIND;
                    break;
                case 3:
                    str = NotificationCompat.MessagingStyle.Message.KEY_TEXT;
                    break;
                case 4:
                    str = "image";
                    break;
                case 5:
                    str = "metadata";
                    break;
                case 6:
                    str = "camera motion";
                    break;
            }
            String valueOf = String.valueOf(str);
            Log.i("DefaultMediaCodecAdapterFactory", valueOf.length() != 0 ? "Creating an asynchronous MediaCodec adapter for track type ".concat(valueOf) : new String("Creating an asynchronous MediaCodec adapter for track type "));
            return new l.b(g, false).a(aVar);
        }
        MediaCodec mediaCodec = null;
        try {
            Objects.requireNonNull(aVar.a);
            String str2 = aVar.a.a;
            String valueOf2 = String.valueOf(str2);
            d.f(valueOf2.length() != 0 ? "createCodec:".concat(valueOf2) : new String("createCodec:"));
            createByCodecName = MediaCodec.createByCodecName(str2);
            d.d0();
        } catch (IOException e2) {
            e = e2;
        } catch (RuntimeException e3) {
            e = e3;
        }
        try {
            d.f("configureCodec");
            createByCodecName.configure(aVar.f1310b, aVar.d, aVar.e, 0);
            d.d0();
            d.f("startCodec");
            createByCodecName.start();
            d.d0();
            return new w(createByCodecName, null, null);
        } catch (IOException | RuntimeException e4) {
            e = e4;
            mediaCodec = createByCodecName;
            if (mediaCodec != null) {
                mediaCodec.release();
            }
            throw e;
        }
    }
}
