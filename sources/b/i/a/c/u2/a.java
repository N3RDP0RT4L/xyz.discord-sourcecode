package b.i.a.c.u2;

import android.database.sqlite.SQLiteDatabase;
/* compiled from: DatabaseProvider.java */
/* loaded from: classes3.dex */
public interface a {
    SQLiteDatabase getReadableDatabase();

    SQLiteDatabase getWritableDatabase();
}
