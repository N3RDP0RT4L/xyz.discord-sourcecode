package b.i.a.c.t2;

import androidx.annotation.Nullable;
import com.discord.restapi.RestAPIAbortCodes;
import org.objectweb.asm.Opcodes;
/* compiled from: MpegAudioUtil.java */
/* loaded from: classes3.dex */
public final class a0 {
    public static final String[] a = {"audio/mpeg-L1", "audio/mpeg-L2", "audio/mpeg"};

    /* renamed from: b  reason: collision with root package name */
    public static final int[] f1111b = {44100, 48000, 32000};
    public static final int[] c = {32000, 64000, 96000, 128000, 160000, 192000, 224000, 256000, 288000, 320000, 352000, 384000, 416000, 448000};
    public static final int[] d = {32000, 48000, 56000, 64000, RestAPIAbortCodes.RELATIONSHIP_INCOMING_DISABLED, 96000, 112000, 128000, 144000, 160000, 176000, 192000, 224000, 256000};
    public static final int[] e = {32000, 48000, 56000, 64000, RestAPIAbortCodes.RELATIONSHIP_INCOMING_DISABLED, 96000, 112000, 128000, 160000, 192000, 224000, 256000, 320000, 384000};
    public static final int[] f = {32000, 40000, 48000, 56000, 64000, RestAPIAbortCodes.RELATIONSHIP_INCOMING_DISABLED, 96000, 112000, 128000, 160000, 192000, 224000, 256000, 320000};
    public static final int[] g = {8000, 16000, 24000, 32000, 40000, 48000, 56000, 64000, RestAPIAbortCodes.RELATIONSHIP_INCOMING_DISABLED, 96000, 112000, 128000, 144000, 160000};

    /* compiled from: MpegAudioUtil.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public int a;
        @Nullable

        /* renamed from: b  reason: collision with root package name */
        public String f1112b;
        public int c;
        public int d;
        public int e;
        public int f;
        public int g;

        public boolean a(int i) {
            int i2;
            int i3;
            int i4;
            int i5;
            if (!a0.c(i) || (i2 = (i >>> 19) & 3) == 1 || (i3 = (i >>> 17) & 3) == 0 || (i4 = (i >>> 12) & 15) == 0 || i4 == 15 || (i5 = (i >>> 10) & 3) == 3) {
                return false;
            }
            this.a = i2;
            this.f1112b = a0.a[3 - i3];
            int i6 = a0.f1111b[i5];
            this.d = i6;
            int i7 = 2;
            if (i2 == 2) {
                this.d = i6 / 2;
            } else if (i2 == 0) {
                this.d = i6 / 4;
            }
            int i8 = (i >>> 9) & 1;
            this.g = a0.b(i2, i3);
            if (i3 == 3) {
                int i9 = i2 == 3 ? a0.c[i4 - 1] : a0.d[i4 - 1];
                this.f = i9;
                this.c = (((i9 * 12) / this.d) + i8) * 4;
            } else {
                int i10 = Opcodes.D2F;
                if (i2 == 3) {
                    int i11 = i3 == 2 ? a0.e[i4 - 1] : a0.f[i4 - 1];
                    this.f = i11;
                    this.c = ((i11 * Opcodes.D2F) / this.d) + i8;
                } else {
                    int i12 = a0.g[i4 - 1];
                    this.f = i12;
                    if (i3 == 1) {
                        i10 = 72;
                    }
                    this.c = ((i10 * i12) / this.d) + i8;
                }
            }
            if (((i >> 6) & 3) == 3) {
                i7 = 1;
            }
            this.e = i7;
            return true;
        }
    }

    public static int a(int i) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        if (!c(i) || (i2 = (i >>> 19) & 3) == 1 || (i3 = (i >>> 17) & 3) == 0 || (i4 = (i >>> 12) & 15) == 0 || i4 == 15 || (i5 = (i >>> 10) & 3) == 3) {
            return -1;
        }
        int i7 = f1111b[i5];
        if (i2 == 2) {
            i7 /= 2;
        } else if (i2 == 0) {
            i7 /= 4;
        }
        int i8 = (i >>> 9) & 1;
        if (i3 == 3) {
            return ((((i2 == 3 ? c[i4 - 1] : d[i4 - 1]) * 12) / i7) + i8) * 4;
        }
        if (i2 == 3) {
            i6 = i3 == 2 ? e[i4 - 1] : f[i4 - 1];
        } else {
            i6 = g[i4 - 1];
        }
        int i9 = Opcodes.D2F;
        if (i2 == 3) {
            return ((i6 * Opcodes.D2F) / i7) + i8;
        }
        if (i3 == 1) {
            i9 = 72;
        }
        return ((i9 * i6) / i7) + i8;
    }

    public static int b(int i, int i2) {
        if (i2 == 1) {
            return i == 3 ? 1152 : 576;
        }
        if (i2 == 2) {
            return 1152;
        }
        if (i2 == 3) {
            return 384;
        }
        throw new IllegalArgumentException();
    }

    public static boolean c(int i) {
        return (i & (-2097152)) == -2097152;
    }

    public static int d(int i) {
        int i2;
        int i3;
        if (!c(i) || (i2 = (i >>> 19) & 3) == 1 || (i3 = (i >>> 17) & 3) == 0) {
            return -1;
        }
        int i4 = (i >>> 12) & 15;
        int i5 = (i >>> 10) & 3;
        if (i4 == 0 || i4 == 15 || i5 == 3) {
            return -1;
        }
        return b(i2, i3);
    }
}
