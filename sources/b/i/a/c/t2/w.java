package b.i.a.c.t2;

import androidx.annotation.Nullable;
import com.google.android.exoplayer2.audio.AudioProcessor;
import java.nio.ByteBuffer;
import java.util.Objects;
/* compiled from: ChannelMappingAudioProcessor.java */
/* loaded from: classes3.dex */
public final class w extends v {
    @Nullable
    public int[] i;
    @Nullable
    public int[] j;

    @Override // com.google.android.exoplayer2.audio.AudioProcessor
    public void c(ByteBuffer byteBuffer) {
        int[] iArr = this.j;
        Objects.requireNonNull(iArr);
        int position = byteBuffer.position();
        int limit = byteBuffer.limit();
        ByteBuffer j = j(((limit - position) / this.f1131b.e) * this.c.e);
        while (position < limit) {
            for (int i : iArr) {
                j.putShort(byteBuffer.getShort((i * 2) + position));
            }
            position += this.f1131b.e;
        }
        byteBuffer.position(limit);
        j.flip();
    }

    @Override // b.i.a.c.t2.v
    public AudioProcessor.a f(AudioProcessor.a aVar) throws AudioProcessor.UnhandledAudioFormatException {
        int[] iArr = this.i;
        if (iArr == null) {
            return AudioProcessor.a.a;
        }
        if (aVar.d == 2) {
            boolean z2 = aVar.c != iArr.length;
            int i = 0;
            while (i < iArr.length) {
                int i2 = iArr[i];
                if (i2 < aVar.c) {
                    z2 |= i2 != i;
                    i++;
                } else {
                    throw new AudioProcessor.UnhandledAudioFormatException(aVar);
                }
            }
            if (z2) {
                return new AudioProcessor.a(aVar.f2886b, iArr.length, 2);
            }
            return AudioProcessor.a.a;
        }
        throw new AudioProcessor.UnhandledAudioFormatException(aVar);
    }

    @Override // b.i.a.c.t2.v
    public void g() {
        this.j = this.i;
    }

    @Override // b.i.a.c.t2.v
    public void i() {
        this.j = null;
        this.i = null;
    }
}
