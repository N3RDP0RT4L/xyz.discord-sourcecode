package b.i.a.c.t2;

import android.util.Log;
import b.i.a.c.f3.w;
import com.google.android.exoplayer2.ParserException;
/* compiled from: AacUtil.java */
/* loaded from: classes3.dex */
public final class l {
    public static final int[] a = {96000, 88200, 64000, 48000, 44100, 32000, 24000, 22050, 16000, 12000, 11025, 8000, 7350};

    /* renamed from: b  reason: collision with root package name */
    public static final int[] f1117b = {0, 1, 2, 3, 4, 5, 6, 8, -1, -1, -1, 7, 8, -1, 8, -1};

    /* compiled from: AacUtil.java */
    /* loaded from: classes3.dex */
    public static final class b {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final int f1118b;
        public final String c;

        public b(int i, int i2, String str, a aVar) {
            this.a = i;
            this.f1118b = i2;
            this.c = str;
        }
    }

    public static int a(w wVar) throws ParserException {
        int g = wVar.g(4);
        if (g == 15) {
            return wVar.g(24);
        }
        if (g < 13) {
            return a[g];
        }
        throw ParserException.a(null, null);
    }

    public static b b(w wVar, boolean z2) throws ParserException {
        int g = wVar.g(5);
        if (g == 31) {
            g = wVar.g(6) + 32;
        }
        int a2 = a(wVar);
        int g2 = wVar.g(4);
        String f = b.d.b.a.a.f(19, "mp4a.40.", g);
        if (g == 5 || g == 29) {
            a2 = a(wVar);
            int g3 = wVar.g(5);
            if (g3 == 31) {
                g3 = wVar.g(6) + 32;
            }
            g = g3;
            if (g == 22) {
                g2 = wVar.g(4);
            }
        }
        if (z2) {
            if (!(g == 1 || g == 2 || g == 3 || g == 4 || g == 6 || g == 7 || g == 17)) {
                switch (g) {
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                        break;
                    default:
                        StringBuilder sb = new StringBuilder(42);
                        sb.append("Unsupported audio object type: ");
                        sb.append(g);
                        throw ParserException.b(sb.toString());
                }
            }
            if (wVar.f()) {
                Log.w("AacUtil", "Unexpected frameLengthFlag = 1");
            }
            if (wVar.f()) {
                wVar.m(14);
            }
            boolean f2 = wVar.f();
            if (g2 != 0) {
                if (g == 6 || g == 20) {
                    wVar.m(3);
                }
                if (f2) {
                    if (g == 22) {
                        wVar.m(16);
                    }
                    if (g == 17 || g == 19 || g == 20 || g == 23) {
                        wVar.m(3);
                    }
                    wVar.m(1);
                }
                switch (g) {
                    case 17:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                        int g4 = wVar.g(2);
                        if (g4 == 2 || g4 == 3) {
                            StringBuilder sb2 = new StringBuilder(33);
                            sb2.append("Unsupported epConfig: ");
                            sb2.append(g4);
                            throw ParserException.b(sb2.toString());
                        }
                }
            } else {
                throw new UnsupportedOperationException();
            }
        }
        int i = f1117b[g2];
        if (i != -1) {
            return new b(a2, i, f, null);
        }
        throw ParserException.a(null, null);
    }

    public static b c(byte[] bArr) throws ParserException {
        return b(new w(bArr), false);
    }
}
