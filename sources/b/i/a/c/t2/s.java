package b.i.a.c.t2;

import android.media.AudioTimestamp;
import android.media.AudioTrack;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import b.i.a.c.f3.e0;
/* compiled from: AudioTimestampPoller.java */
/* loaded from: classes3.dex */
public final class s {
    @Nullable
    public final a a;

    /* renamed from: b  reason: collision with root package name */
    public int f1123b;
    public long c;
    public long d;
    public long e;
    public long f;

    /* compiled from: AudioTimestampPoller.java */
    @RequiresApi(19)
    /* loaded from: classes3.dex */
    public static final class a {
        public final AudioTrack a;

        /* renamed from: b  reason: collision with root package name */
        public final AudioTimestamp f1124b = new AudioTimestamp();
        public long c;
        public long d;
        public long e;

        public a(AudioTrack audioTrack) {
            this.a = audioTrack;
        }
    }

    public s(AudioTrack audioTrack) {
        if (e0.a >= 19) {
            this.a = new a(audioTrack);
            a();
            return;
        }
        this.a = null;
        b(3);
    }

    public void a() {
        if (this.a != null) {
            b(0);
        }
    }

    public final void b(int i) {
        this.f1123b = i;
        if (i == 0) {
            this.e = 0L;
            this.f = -1L;
            this.c = System.nanoTime() / 1000;
            this.d = 10000L;
        } else if (i == 1) {
            this.d = 10000L;
        } else if (i == 2 || i == 3) {
            this.d = 10000000L;
        } else if (i == 4) {
            this.d = 500000L;
        } else {
            throw new IllegalStateException();
        }
    }
}
