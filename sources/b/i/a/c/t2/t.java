package b.i.a.c.t2;

import android.media.AudioTrack;
import android.os.SystemClock;
import androidx.annotation.Nullable;
import b.i.a.c.f3.e0;
import java.lang.reflect.Method;
import java.util.Objects;
/* compiled from: AudioTrackPositionTracker.java */
/* loaded from: classes3.dex */
public final class t {
    public long A;
    public long B;
    public long C;
    public boolean D;
    public long E;
    public long F;
    public final a a;

    /* renamed from: b  reason: collision with root package name */
    public final long[] f1125b;
    @Nullable
    public AudioTrack c;
    public int d;
    public int e;
    @Nullable
    public s f;
    public int g;
    public boolean h;
    public long i;
    public float j;
    public boolean k;
    public long l;
    public long m;
    @Nullable
    public Method n;
    public long o;
    public boolean p;
    public boolean q;
    public long r;

    /* renamed from: s  reason: collision with root package name */
    public long f1126s;
    public long t;
    public long u;
    public int v;
    public int w;

    /* renamed from: x  reason: collision with root package name */
    public long f1127x;

    /* renamed from: y  reason: collision with root package name */
    public long f1128y;

    /* renamed from: z  reason: collision with root package name */
    public long f1129z;

    /* compiled from: AudioTrackPositionTracker.java */
    /* loaded from: classes3.dex */
    public interface a {
        void a(long j);

        void b(int i, long j);

        void c(long j);

        void d(long j, long j2, long j3, long j4);

        void e(long j, long j2, long j3, long j4);
    }

    public t(a aVar) {
        this.a = aVar;
        if (e0.a >= 18) {
            try {
                this.n = AudioTrack.class.getMethod("getLatency", null);
            } catch (NoSuchMethodException unused) {
            }
        }
        this.f1125b = new long[10];
    }

    public final long a(long j) {
        return (j * 1000000) / this.g;
    }

    public final long b() {
        AudioTrack audioTrack = this.c;
        Objects.requireNonNull(audioTrack);
        if (this.f1127x != -9223372036854775807L) {
            return Math.min(this.A, this.f1129z + ((((SystemClock.elapsedRealtime() * 1000) - this.f1127x) * this.g) / 1000000));
        }
        int playState = audioTrack.getPlayState();
        if (playState == 1) {
            return 0L;
        }
        long playbackHeadPosition = 4294967295L & audioTrack.getPlaybackHeadPosition();
        if (this.h) {
            if (playState == 2 && playbackHeadPosition == 0) {
                this.u = this.f1126s;
            }
            playbackHeadPosition += this.u;
        }
        if (e0.a <= 29) {
            if (playbackHeadPosition == 0 && this.f1126s > 0 && playState == 3) {
                if (this.f1128y == -9223372036854775807L) {
                    this.f1128y = SystemClock.elapsedRealtime();
                }
                return this.f1126s;
            }
            this.f1128y = -9223372036854775807L;
        }
        if (this.f1126s > playbackHeadPosition) {
            this.t++;
        }
        this.f1126s = playbackHeadPosition;
        return playbackHeadPosition + (this.t << 32);
    }

    /* JADX WARN: Removed duplicated region for block: B:14:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public boolean c(long r6) {
        /*
            r5 = this;
            long r0 = r5.b()
            r2 = 0
            r3 = 1
            int r4 = (r6 > r0 ? 1 : (r6 == r0 ? 0 : -1))
            if (r4 > 0) goto L29
            boolean r6 = r5.h
            if (r6 == 0) goto L26
            android.media.AudioTrack r6 = r5.c
            java.util.Objects.requireNonNull(r6)
            int r6 = r6.getPlayState()
            r7 = 2
            if (r6 != r7) goto L26
            long r6 = r5.b()
            r0 = 0
            int r4 = (r6 > r0 ? 1 : (r6 == r0 ? 0 : -1))
            if (r4 != 0) goto L26
            r6 = 1
            goto L27
        L26:
            r6 = 0
        L27:
            if (r6 == 0) goto L2a
        L29:
            r2 = 1
        L2a:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.t2.t.c(long):boolean");
    }

    public void d() {
        this.l = 0L;
        this.w = 0;
        this.v = 0;
        this.m = 0L;
        this.C = 0L;
        this.F = 0L;
        this.k = false;
        this.c = null;
        this.f = null;
    }

    /* JADX WARN: Code restructure failed: missing block: B:12:0x0026, code lost:
        if ((b.i.a.c.f3.e0.a < 23 && (r5 == 5 || r5 == 6)) != false) goto L14;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void e(android.media.AudioTrack r3, boolean r4, int r5, int r6, int r7) {
        /*
            r2 = this;
            r2.c = r3
            r2.d = r6
            r2.e = r7
            b.i.a.c.t2.s r0 = new b.i.a.c.t2.s
            r0.<init>(r3)
            r2.f = r0
            int r3 = r3.getSampleRate()
            r2.g = r3
            r3 = 1
            r0 = 0
            if (r4 == 0) goto L29
            int r4 = b.i.a.c.f3.e0.a
            r1 = 23
            if (r4 >= r1) goto L25
            r4 = 5
            if (r5 == r4) goto L23
            r4 = 6
            if (r5 != r4) goto L25
        L23:
            r4 = 1
            goto L26
        L25:
            r4 = 0
        L26:
            if (r4 == 0) goto L29
            goto L2a
        L29:
            r3 = 0
        L2a:
            r2.h = r3
            boolean r3 = b.i.a.c.f3.e0.z(r5)
            r2.q = r3
            r4 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            if (r3 == 0) goto L40
            int r7 = r7 / r6
            long r6 = (long) r7
            long r6 = r2.a(r6)
            goto L41
        L40:
            r6 = r4
        L41:
            r2.i = r6
            r6 = 0
            r2.f1126s = r6
            r2.t = r6
            r2.u = r6
            r2.p = r0
            r2.f1127x = r4
            r2.f1128y = r4
            r2.r = r6
            r2.o = r6
            r3 = 1065353216(0x3f800000, float:1.0)
            r2.j = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.t2.t.e(android.media.AudioTrack, boolean, int, int, int):void");
    }
}
