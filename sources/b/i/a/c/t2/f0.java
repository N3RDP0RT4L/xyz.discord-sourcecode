package b.i.a.c.t2;

import b.i.a.c.f3.e0;
import com.google.android.exoplayer2.audio.AudioProcessor;
import java.nio.ByteBuffer;
/* compiled from: TrimmingAudioProcessor.java */
/* loaded from: classes3.dex */
public final class f0 extends v {
    public int i;
    public int j;
    public boolean k;
    public int l;
    public byte[] m = e0.f;
    public int n;
    public long o;

    @Override // b.i.a.c.t2.v, com.google.android.exoplayer2.audio.AudioProcessor
    public boolean b() {
        return super.b() && this.n == 0;
    }

    @Override // com.google.android.exoplayer2.audio.AudioProcessor
    public void c(ByteBuffer byteBuffer) {
        int position = byteBuffer.position();
        int limit = byteBuffer.limit();
        int i = limit - position;
        if (i != 0) {
            int min = Math.min(i, this.l);
            this.o += min / this.f1131b.e;
            this.l -= min;
            byteBuffer.position(position + min);
            if (this.l <= 0) {
                int i2 = i - min;
                int length = (this.n + i2) - this.m.length;
                ByteBuffer j = j(length);
                int h = e0.h(length, 0, this.n);
                j.put(this.m, 0, h);
                int h2 = e0.h(length - h, 0, i2);
                byteBuffer.limit(byteBuffer.position() + h2);
                j.put(byteBuffer);
                byteBuffer.limit(limit);
                int i3 = i2 - h2;
                int i4 = this.n - h;
                this.n = i4;
                byte[] bArr = this.m;
                System.arraycopy(bArr, h, bArr, 0, i4);
                byteBuffer.get(this.m, this.n, i3);
                this.n += i3;
                j.flip();
            }
        }
    }

    @Override // b.i.a.c.t2.v
    public AudioProcessor.a f(AudioProcessor.a aVar) throws AudioProcessor.UnhandledAudioFormatException {
        if (aVar.d == 2) {
            this.k = true;
            return (this.i == 0 && this.j == 0) ? AudioProcessor.a.a : aVar;
        }
        throw new AudioProcessor.UnhandledAudioFormatException(aVar);
    }

    @Override // b.i.a.c.t2.v
    public void g() {
        if (this.k) {
            this.k = false;
            int i = this.j;
            int i2 = this.f1131b.e;
            this.m = new byte[i * i2];
            this.l = this.i * i2;
        }
        this.n = 0;
    }

    @Override // b.i.a.c.t2.v, com.google.android.exoplayer2.audio.AudioProcessor
    public ByteBuffer getOutput() {
        int i;
        if (super.b() && (i = this.n) > 0) {
            j(i).put(this.m, 0, this.n).flip();
            this.n = 0;
        }
        return super.getOutput();
    }

    @Override // b.i.a.c.t2.v
    public void h() {
        int i;
        if (this.k) {
            if (this.n > 0) {
                this.o += i / this.f1131b.e;
            }
            this.n = 0;
        }
    }

    @Override // b.i.a.c.t2.v
    public void i() {
        this.m = e0.f;
    }
}
