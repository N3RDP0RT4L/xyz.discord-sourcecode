package b.i.a.c.t2;

import androidx.core.view.PointerIconCompat;
import b.i.a.c.f3.x;
/* compiled from: Ac4Util.java */
/* loaded from: classes3.dex */
public final class n {
    public static final int[] a = {2002, 2000, 1920, 1601, 1600, PointerIconCompat.TYPE_CONTEXT_MENU, 1000, 960, 800, 800, 480, 400, 400, 2048};

    /* compiled from: Ac4Util.java */
    /* loaded from: classes3.dex */
    public static final class b {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final int f1120b;
        public final int c;

        public b(int i, int i2, int i3, int i4, int i5, a aVar) {
            this.a = i3;
            this.f1120b = i4;
            this.c = i5;
        }
    }

    public static void a(int i, x xVar) {
        xVar.A(7);
        byte[] bArr = xVar.a;
        bArr[0] = -84;
        bArr[1] = 64;
        bArr[2] = -1;
        bArr[3] = -1;
        bArr[4] = (byte) ((i >> 16) & 255);
        bArr[5] = (byte) ((i >> 8) & 255);
        bArr[6] = (byte) (i & 255);
    }

    /* JADX WARN: Code restructure failed: missing block: B:41:0x0093, code lost:
        if (r10 != 11) goto L46;
     */
    /* JADX WARN: Code restructure failed: missing block: B:43:0x0098, code lost:
        if (r10 != 11) goto L46;
     */
    /* JADX WARN: Code restructure failed: missing block: B:45:0x009d, code lost:
        if (r10 != 8) goto L46;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static b.i.a.c.t2.n.b b(b.i.a.c.f3.w r10) {
        /*
            r0 = 16
            int r1 = r10.g(r0)
            int r0 = r10.g(r0)
            r2 = 4
            r3 = 65535(0xffff, float:9.1834E-41)
            if (r0 != r3) goto L18
            r0 = 24
            int r0 = r10.g(r0)
            r3 = 7
            goto L19
        L18:
            r3 = 4
        L19:
            int r0 = r0 + r3
            r3 = 44097(0xac41, float:6.1793E-41)
            if (r1 != r3) goto L21
            int r0 = r0 + 2
        L21:
            r7 = r0
            r0 = 2
            int r1 = r10.g(r0)
            r3 = 0
            r4 = 3
            if (r1 != r4) goto L3e
            r5 = 0
        L2c:
            int r6 = r10.g(r0)
            int r6 = r6 + r5
            boolean r5 = r10.f()
            if (r5 != 0) goto L39
            int r1 = r1 + r6
            goto L3e
        L39:
            int r6 = r6 + 1
            int r5 = r6 << 2
            goto L2c
        L3e:
            r5 = 10
            int r5 = r10.g(r5)
            boolean r6 = r10.f()
            if (r6 == 0) goto L53
            int r6 = r10.g(r4)
            if (r6 <= 0) goto L53
            r10.m(r0)
        L53:
            boolean r6 = r10.f()
            r8 = 48000(0xbb80, float:6.7262E-41)
            r9 = 44100(0xac44, float:6.1797E-41)
            if (r6 == 0) goto L63
            r6 = 48000(0xbb80, float:6.7262E-41)
            goto L66
        L63:
            r6 = 44100(0xac44, float:6.1797E-41)
        L66:
            int r10 = r10.g(r2)
            if (r6 != r9) goto L76
            r9 = 13
            if (r10 != r9) goto L76
            int[] r0 = b.i.a.c.t2.n.a
            r10 = r0[r10]
            r8 = r10
            goto La6
        L76:
            if (r6 != r8) goto La5
            int[] r8 = b.i.a.c.t2.n.a
            int r9 = r8.length
            if (r10 >= r9) goto La5
            r3 = r8[r10]
            int r5 = r5 % 5
            r8 = 8
            r9 = 1
            if (r5 == r9) goto L9b
            r9 = 11
            if (r5 == r0) goto L96
            if (r5 == r4) goto L9b
            if (r5 == r2) goto L8f
            goto La0
        L8f:
            if (r10 == r4) goto La2
            if (r10 == r8) goto La2
            if (r10 != r9) goto La0
            goto La2
        L96:
            if (r10 == r8) goto La2
            if (r10 != r9) goto La0
            goto La2
        L9b:
            if (r10 == r4) goto La2
            if (r10 != r8) goto La0
            goto La2
        La0:
            r8 = r3
            goto La6
        La2:
            int r3 = r3 + 1
            goto La0
        La5:
            r8 = 0
        La6:
            b.i.a.c.t2.n$b r10 = new b.i.a.c.t2.n$b
            r5 = 2
            r9 = 0
            r3 = r10
            r4 = r1
            r3.<init>(r4, r5, r6, r7, r8, r9)
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.t2.n.b(b.i.a.c.f3.w):b.i.a.c.t2.n$b");
    }
}
