package b.i.a.c.t2;

import b.c.a.a0.d;
import b.i.a.c.f3.e0;
import com.google.android.exoplayer2.audio.AudioProcessor;
import java.nio.ByteBuffer;
/* compiled from: SilenceSkippingAudioProcessor.java */
/* loaded from: classes3.dex */
public final class c0 extends v {
    public final long i = 150000;
    public final long j = 20000;
    public final short k = 1024;
    public int l;
    public boolean m;
    public byte[] n;
    public byte[] o;
    public int p;
    public int q;
    public int r;

    /* renamed from: s  reason: collision with root package name */
    public boolean f1113s;
    public long t;

    public c0() {
        d.j(true);
        byte[] bArr = e0.f;
        this.n = bArr;
        this.o = bArr;
    }

    @Override // b.i.a.c.t2.v, com.google.android.exoplayer2.audio.AudioProcessor
    public boolean a() {
        return this.m;
    }

    @Override // com.google.android.exoplayer2.audio.AudioProcessor
    public void c(ByteBuffer byteBuffer) {
        int position;
        while (byteBuffer.hasRemaining() && !this.g.hasRemaining()) {
            int i = this.p;
            if (i == 0) {
                int limit = byteBuffer.limit();
                byteBuffer.limit(Math.min(limit, byteBuffer.position() + this.n.length));
                int limit2 = byteBuffer.limit();
                while (true) {
                    limit2 -= 2;
                    if (limit2 >= byteBuffer.position()) {
                        if (Math.abs((int) byteBuffer.getShort(limit2)) > this.k) {
                            int i2 = this.l;
                            position = ((limit2 / i2) * i2) + i2;
                            break;
                        }
                    } else {
                        position = byteBuffer.position();
                        break;
                    }
                }
                if (position == byteBuffer.position()) {
                    this.p = 1;
                } else {
                    byteBuffer.limit(position);
                    int remaining = byteBuffer.remaining();
                    j(remaining).put(byteBuffer).flip();
                    if (remaining > 0) {
                        this.f1113s = true;
                    }
                }
                byteBuffer.limit(limit);
            } else if (i == 1) {
                int limit3 = byteBuffer.limit();
                int k = k(byteBuffer);
                int position2 = k - byteBuffer.position();
                byte[] bArr = this.n;
                int length = bArr.length;
                int i3 = this.q;
                int i4 = length - i3;
                if (k >= limit3 || position2 >= i4) {
                    int min = Math.min(position2, i4);
                    byteBuffer.limit(byteBuffer.position() + min);
                    byteBuffer.get(this.n, this.q, min);
                    int i5 = this.q + min;
                    this.q = i5;
                    byte[] bArr2 = this.n;
                    if (i5 == bArr2.length) {
                        if (this.f1113s) {
                            l(bArr2, this.r);
                            this.t += (this.q - (this.r * 2)) / this.l;
                        } else {
                            this.t += (i5 - this.r) / this.l;
                        }
                        m(byteBuffer, this.n, this.q);
                        this.q = 0;
                        this.p = 2;
                    }
                    byteBuffer.limit(limit3);
                } else {
                    l(bArr, i3);
                    this.q = 0;
                    this.p = 0;
                }
            } else if (i == 2) {
                int limit4 = byteBuffer.limit();
                int k2 = k(byteBuffer);
                byteBuffer.limit(k2);
                this.t += byteBuffer.remaining() / this.l;
                m(byteBuffer, this.o, this.r);
                if (k2 < limit4) {
                    l(this.o, this.r);
                    this.p = 0;
                    byteBuffer.limit(limit4);
                }
            } else {
                throw new IllegalStateException();
            }
        }
    }

    @Override // b.i.a.c.t2.v
    public AudioProcessor.a f(AudioProcessor.a aVar) throws AudioProcessor.UnhandledAudioFormatException {
        if (aVar.d == 2) {
            return this.m ? aVar : AudioProcessor.a.a;
        }
        throw new AudioProcessor.UnhandledAudioFormatException(aVar);
    }

    @Override // b.i.a.c.t2.v
    public void g() {
        if (this.m) {
            AudioProcessor.a aVar = this.f1131b;
            int i = aVar.e;
            this.l = i;
            long j = this.i;
            long j2 = aVar.f2886b;
            int i2 = ((int) ((j * j2) / 1000000)) * i;
            if (this.n.length != i2) {
                this.n = new byte[i2];
            }
            int i3 = ((int) ((this.j * j2) / 1000000)) * i;
            this.r = i3;
            if (this.o.length != i3) {
                this.o = new byte[i3];
            }
        }
        this.p = 0;
        this.t = 0L;
        this.q = 0;
        this.f1113s = false;
    }

    @Override // b.i.a.c.t2.v
    public void h() {
        int i = this.q;
        if (i > 0) {
            l(this.n, i);
        }
        if (!this.f1113s) {
            this.t += this.r / this.l;
        }
    }

    @Override // b.i.a.c.t2.v
    public void i() {
        this.m = false;
        this.r = 0;
        byte[] bArr = e0.f;
        this.n = bArr;
        this.o = bArr;
    }

    public final int k(ByteBuffer byteBuffer) {
        for (int position = byteBuffer.position(); position < byteBuffer.limit(); position += 2) {
            if (Math.abs((int) byteBuffer.getShort(position)) > this.k) {
                int i = this.l;
                return (position / i) * i;
            }
        }
        return byteBuffer.limit();
    }

    public final void l(byte[] bArr, int i) {
        j(i).put(bArr, 0, i).flip();
        if (i > 0) {
            this.f1113s = true;
        }
    }

    public final void m(ByteBuffer byteBuffer, byte[] bArr, int i) {
        int min = Math.min(byteBuffer.remaining(), this.r);
        int i2 = this.r - min;
        System.arraycopy(bArr, i - i2, this.o, 0, i2);
        byteBuffer.position(byteBuffer.limit() - min);
        byteBuffer.get(this.o, i2, min);
    }
}
