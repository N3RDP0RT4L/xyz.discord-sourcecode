package b.i.a.c.t2;

import b.c.a.a0.d;
import com.discord.utilities.rest.SendUtils;
import java.util.Arrays;
/* compiled from: Sonic.java */
/* loaded from: classes3.dex */
public final class d0 {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final int f1114b;
    public final float c;
    public final float d;
    public final float e;
    public final int f;
    public final int g;
    public final int h;
    public final short[] i;
    public short[] j;
    public int k;
    public short[] l;
    public int m;
    public short[] n;
    public int o;
    public int p;
    public int q;
    public int r;

    /* renamed from: s  reason: collision with root package name */
    public int f1115s;
    public int t;
    public int u;
    public int v;

    public d0(int i, int i2, float f, float f2, int i3) {
        this.a = i;
        this.f1114b = i2;
        this.c = f;
        this.d = f2;
        this.e = i / i3;
        this.f = i / 400;
        int i4 = i / 65;
        this.g = i4;
        int i5 = i4 * 2;
        this.h = i5;
        this.i = new short[i5];
        this.j = new short[i5 * i2];
        this.l = new short[i5 * i2];
        this.n = new short[i5 * i2];
    }

    public static void e(int i, int i2, short[] sArr, int i3, short[] sArr2, int i4, short[] sArr3, int i5) {
        for (int i6 = 0; i6 < i2; i6++) {
            int i7 = (i3 * i2) + i6;
            int i8 = (i5 * i2) + i6;
            int i9 = (i4 * i2) + i6;
            for (int i10 = 0; i10 < i; i10++) {
                sArr[i7] = (short) (((sArr3[i8] * i10) + ((i - i10) * sArr2[i9])) / i);
                i7 += i2;
                i9 += i2;
                i8 += i2;
            }
        }
    }

    public final void a(short[] sArr, int i, int i2) {
        short[] c = c(this.l, this.m, i2);
        this.l = c;
        int i3 = this.f1114b;
        System.arraycopy(sArr, i * i3, c, this.m * i3, i3 * i2);
        this.m += i2;
    }

    public final void b(short[] sArr, int i, int i2) {
        int i3 = this.h / i2;
        int i4 = this.f1114b;
        int i5 = i2 * i4;
        int i6 = i * i4;
        for (int i7 = 0; i7 < i3; i7++) {
            int i8 = 0;
            for (int i9 = 0; i9 < i5; i9++) {
                i8 += sArr[(i7 * i5) + i6 + i9];
            }
            this.i[i7] = (short) (i8 / i5);
        }
    }

    public final short[] c(short[] sArr, int i, int i2) {
        int length = sArr.length;
        int i3 = this.f1114b;
        int i4 = length / i3;
        return i + i2 <= i4 ? sArr : Arrays.copyOf(sArr, (((i4 * 3) / 2) + i2) * i3);
    }

    public final int d(short[] sArr, int i, int i2, int i3) {
        int i4 = i * this.f1114b;
        int i5 = 1;
        int i6 = 255;
        int i7 = 0;
        int i8 = 0;
        while (i2 <= i3) {
            int i9 = 0;
            for (int i10 = 0; i10 < i2; i10++) {
                i9 += Math.abs(sArr[i4 + i10] - sArr[(i4 + i2) + i10]);
            }
            if (i9 * i7 < i5 * i2) {
                i7 = i2;
                i5 = i9;
            }
            if (i9 * i6 > i8 * i2) {
                i6 = i2;
                i8 = i9;
            }
            i2++;
        }
        this.u = i5 / i7;
        this.v = i8 / i6;
        return i7;
    }

    public final void f() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8 = this.m;
        float f = this.c;
        float f2 = this.d;
        float f3 = f / f2;
        float f4 = this.e * f2;
        double d = f3;
        float f5 = 1.0f;
        int i9 = 1;
        if (d > 1.00001d || d < 0.99999d) {
            int i10 = this.k;
            if (i10 >= this.h) {
                int i11 = 0;
                while (true) {
                    int i12 = this.r;
                    if (i12 > 0) {
                        int min = Math.min(this.h, i12);
                        a(this.j, i11, min);
                        this.r -= min;
                        i11 += min;
                    } else {
                        short[] sArr = this.j;
                        int i13 = this.a;
                        int i14 = i13 > 4000 ? i13 / SendUtils.MAX_MESSAGE_CHARACTER_COUNT_PREMIUM : 1;
                        if (this.f1114b == i9 && i14 == i9) {
                            i5 = d(sArr, i11, this.f, this.g);
                        } else {
                            b(sArr, i11, i14);
                            int d2 = d(this.i, 0, this.f / i14, this.g / i14);
                            if (i14 != i9) {
                                int i15 = d2 * i14;
                                int i16 = i14 * 4;
                                int i17 = i15 - i16;
                                int i18 = i15 + i16;
                                int i19 = this.f;
                                if (i17 < i19) {
                                    i17 = i19;
                                }
                                int i20 = this.g;
                                if (i18 > i20) {
                                    i18 = i20;
                                }
                                if (this.f1114b == i9) {
                                    i5 = d(sArr, i11, i17, i18);
                                } else {
                                    b(sArr, i11, i9);
                                    i5 = d(this.i, 0, i17, i18);
                                }
                            } else {
                                i5 = d2;
                            }
                        }
                        int i21 = this.u;
                        int i22 = i21 != 0 && this.f1115s != 0 && this.v <= i21 * 3 && i21 * 2 > this.t * 3 ? this.f1115s : i5;
                        this.t = i21;
                        this.f1115s = i5;
                        if (d > 1.0d) {
                            short[] sArr2 = this.j;
                            if (f3 >= 2.0f) {
                                i7 = (int) (i22 / (f3 - f5));
                            } else {
                                this.r = (int) (((2.0f - f3) * i22) / (f3 - f5));
                                i7 = i22;
                            }
                            short[] c = c(this.l, this.m, i7);
                            this.l = c;
                            int i23 = i7;
                            e(i7, this.f1114b, c, this.m, sArr2, i11, sArr2, i11 + i22);
                            this.m += i23;
                            i11 = i22 + i23 + i11;
                        } else {
                            int i24 = i22;
                            short[] sArr3 = this.j;
                            if (f3 < 0.5f) {
                                i6 = (int) ((i24 * f3) / (f5 - f3));
                            } else {
                                this.r = (int) ((((2.0f * f3) - f5) * i24) / (f5 - f3));
                                i6 = i24;
                            }
                            int i25 = i24 + i6;
                            short[] c2 = c(this.l, this.m, i25);
                            this.l = c2;
                            int i26 = this.f1114b;
                            System.arraycopy(sArr3, i11 * i26, c2, this.m * i26, i26 * i24);
                            e(i6, this.f1114b, this.l, this.m + i24, sArr3, i11 + i24, sArr3, i11);
                            this.m += i25;
                            i11 += i6;
                        }
                    }
                    if (this.h + i11 > i10) {
                        break;
                    }
                    f5 = 1.0f;
                    i9 = 1;
                }
                int i27 = this.k - i11;
                short[] sArr4 = this.j;
                int i28 = this.f1114b;
                System.arraycopy(sArr4, i11 * i28, sArr4, 0, i28 * i27);
                this.k = i27;
            }
            f5 = 1.0f;
        } else {
            a(this.j, 0, this.k);
            this.k = 0;
        }
        if (f4 != f5 && this.m != i8) {
            int i29 = this.a;
            int i30 = (int) (i29 / f4);
            while (true) {
                if (i30 <= 16384 && i29 <= 16384) {
                    break;
                }
                i30 /= 2;
                i29 /= 2;
            }
            int i31 = this.m - i8;
            short[] c3 = c(this.n, this.o, i31);
            this.n = c3;
            short[] sArr5 = this.l;
            int i32 = this.f1114b;
            System.arraycopy(sArr5, i8 * i32, c3, this.o * i32, i32 * i31);
            this.m = i8;
            this.o += i31;
            int i33 = 0;
            while (true) {
                i = this.o;
                i2 = i - 1;
                if (i33 >= i2) {
                    break;
                }
                while (true) {
                    i3 = this.p + 1;
                    int i34 = i3 * i30;
                    i4 = this.q;
                    if (i34 <= i4 * i29) {
                        break;
                    }
                    this.l = c(this.l, this.m, 1);
                    int i35 = 0;
                    while (true) {
                        int i36 = this.f1114b;
                        if (i35 < i36) {
                            short[] sArr6 = this.n;
                            int i37 = (i33 * i36) + i35;
                            short s2 = sArr6[i37];
                            short s3 = sArr6[i37 + i36];
                            int i38 = this.p;
                            int i39 = i38 * i30;
                            int i40 = (i38 + 1) * i30;
                            int i41 = i40 - (this.q * i29);
                            int i42 = i40 - i39;
                            this.l[(this.m * i36) + i35] = (short) ((((i42 - i41) * s3) + (s2 * i41)) / i42);
                            i35++;
                        }
                    }
                    this.q++;
                    this.m++;
                }
                this.p = i3;
                if (i3 == i29) {
                    this.p = 0;
                    d.D(i4 == i30);
                    this.q = 0;
                }
                i33++;
            }
            if (i2 != 0) {
                short[] sArr7 = this.n;
                int i43 = this.f1114b;
                System.arraycopy(sArr7, i2 * i43, sArr7, 0, (i - i2) * i43);
                this.o -= i2;
            }
        }
    }
}
