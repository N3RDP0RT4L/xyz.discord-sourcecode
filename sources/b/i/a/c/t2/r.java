package b.i.a.c.t2;

import android.os.Handler;
import androidx.annotation.Nullable;
import b.i.a.c.f3.e0;
import b.i.a.c.j1;
import b.i.a.c.t2.r;
import b.i.a.c.v2.e;
import b.i.a.c.v2.g;
import java.util.Objects;
/* compiled from: AudioRendererEventListener.java */
/* loaded from: classes3.dex */
public interface r {

    /* compiled from: AudioRendererEventListener.java */
    /* loaded from: classes3.dex */
    public static final class a {
        @Nullable
        public final Handler a;
        @Nullable

        /* renamed from: b  reason: collision with root package name */
        public final r f1122b;

        public a(@Nullable Handler handler, @Nullable r rVar) {
            if (rVar != null) {
                Objects.requireNonNull(handler);
            } else {
                handler = null;
            }
            this.a = handler;
            this.f1122b = rVar;
        }

        public void a(final e eVar) {
            synchronized (eVar) {
            }
            Handler handler = this.a;
            if (handler != null) {
                handler.post(new Runnable() { // from class: b.i.a.c.t2.b
                    @Override // java.lang.Runnable
                    public final void run() {
                        r.a aVar = r.a.this;
                        e eVar2 = eVar;
                        Objects.requireNonNull(aVar);
                        synchronized (eVar2) {
                        }
                        r rVar = aVar.f1122b;
                        int i = e0.a;
                        rVar.k(eVar2);
                    }
                });
            }
        }
    }

    void B(String str);

    void C(String str, long j, long j2);

    void I(j1 j1Var, @Nullable g gVar);

    void N(Exception exc);

    void R(long j);

    void T(Exception exc);

    @Deprecated
    void U(j1 j1Var);

    void d(boolean z2);

    void e0(int i, long j, long j2);

    void k(e eVar);

    void m(e eVar);
}
