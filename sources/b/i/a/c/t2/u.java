package b.i.a.c.t2;

import androidx.annotation.Nullable;
/* compiled from: AuxEffectInfo.java */
/* loaded from: classes3.dex */
public final class u {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final float f1130b;

    public u(int i, float f) {
        this.a = i;
        this.f1130b = f;
    }

    public boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || u.class != obj.getClass()) {
            return false;
        }
        u uVar = (u) obj;
        return this.a == uVar.a && Float.compare(uVar.f1130b, this.f1130b) == 0;
    }

    public int hashCode() {
        return Float.floatToIntBits(this.f1130b) + ((527 + this.a) * 31);
    }
}
