package b.i.a.c.t2;

import android.content.Context;
import android.media.MediaFormat;
import android.os.Handler;
import androidx.annotation.Nullable;
import b.i.a.c.f2;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.q;
import b.i.a.c.f3.s;
import b.i.a.c.h2;
import b.i.a.c.j1;
import b.i.a.c.k1;
import b.i.a.c.t2.r;
import b.i.a.c.v2.e;
import b.i.a.c.v2.g;
import b.i.a.c.x1;
import b.i.a.c.y2.t;
import b.i.a.c.y2.u;
import b.i.a.c.y2.v;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.audio.AudioSink;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;
/* compiled from: MediaCodecAudioRenderer.java */
/* loaded from: classes3.dex */
public class z extends MediaCodecRenderer implements s {
    public final Context N0;
    public final r.a O0;
    public final AudioSink P0;
    public int Q0;
    public boolean R0;
    @Nullable
    public j1 S0;
    public long T0;
    public boolean U0;
    public boolean V0;
    public boolean W0;
    @Nullable
    public f2.a X0;

    /* compiled from: MediaCodecAudioRenderer.java */
    /* loaded from: classes3.dex */
    public final class b implements AudioSink.a {
        public b(a aVar) {
        }

        public void a(final Exception exc) {
            q.b("MediaCodecAudioRenderer", "Audio sink error", exc);
            final r.a aVar = z.this.O0;
            Handler handler = aVar.a;
            if (handler != null) {
                handler.post(new Runnable() { // from class: b.i.a.c.t2.d
                    @Override // java.lang.Runnable
                    public final void run() {
                        r.a aVar2 = r.a.this;
                        Exception exc2 = exc;
                        r rVar = aVar2.f1122b;
                        int i = e0.a;
                        rVar.N(exc2);
                    }
                });
            }
        }
    }

    public z(Context context, t.b bVar, v vVar, boolean z2, @Nullable Handler handler, @Nullable r rVar, AudioSink audioSink) {
        super(1, bVar, vVar, z2, 44100.0f);
        this.N0 = context.getApplicationContext();
        this.P0 = audioSink;
        this.O0 = new r.a(handler, rVar);
        audioSink.t(new b(null));
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer, b.i.a.c.v0
    public void B() {
        this.W0 = true;
        try {
            this.P0.flush();
            try {
                super.B();
            } finally {
            }
        } catch (Throwable th) {
            try {
                super.B();
                throw th;
            } finally {
            }
        }
    }

    @Override // b.i.a.c.v0
    public void C(boolean z2, boolean z3) throws ExoPlaybackException {
        final e eVar = new e();
        this.J0 = eVar;
        final r.a aVar = this.O0;
        Handler handler = aVar.a;
        if (handler != null) {
            handler.post(new Runnable() { // from class: b.i.a.c.t2.j
                @Override // java.lang.Runnable
                public final void run() {
                    r.a aVar2 = r.a.this;
                    e eVar2 = eVar;
                    r rVar = aVar2.f1122b;
                    int i = e0.a;
                    rVar.m(eVar2);
                }
            });
        }
        h2 h2Var = this.l;
        Objects.requireNonNull(h2Var);
        if (h2Var.f1011b) {
            this.P0.r();
        } else {
            this.P0.n();
        }
    }

    public final int C0(u uVar, j1 j1Var) {
        int i;
        if (!"OMX.google.raw.decoder".equals(uVar.a) || (i = e0.a) >= 24 || (i == 23 && e0.A(this.N0))) {
            return j1Var.f1014x;
        }
        return -1;
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer, b.i.a.c.v0
    public void D(long j, boolean z2) throws ExoPlaybackException {
        super.D(j, z2);
        this.P0.flush();
        this.T0 = j;
        this.U0 = true;
        this.V0 = true;
    }

    public final void D0() {
        long m = this.P0.m(b());
        if (m != Long.MIN_VALUE) {
            if (!this.V0) {
                m = Math.max(this.T0, m);
            }
            this.T0 = m;
            this.V0 = false;
        }
    }

    @Override // b.i.a.c.v0
    public void E() {
        try {
            M();
            o0();
            u0(null);
        } finally {
            if (this.W0) {
                this.W0 = false;
                this.P0.reset();
            }
        }
    }

    @Override // b.i.a.c.v0
    public void F() {
        this.P0.e();
    }

    @Override // b.i.a.c.v0
    public void G() {
        D0();
        this.P0.d();
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    public g K(u uVar, j1 j1Var, j1 j1Var2) {
        g c = uVar.c(j1Var, j1Var2);
        int i = c.e;
        if (C0(uVar, j1Var2) > this.Q0) {
            i |= 64;
        }
        int i2 = i;
        return new g(uVar.a, j1Var, j1Var2, i2 != 0 ? 0 : c.d, i2);
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    public float V(float f, j1 j1Var, j1[] j1VarArr) {
        int i = -1;
        for (j1 j1Var2 : j1VarArr) {
            int i2 = j1Var2.K;
            if (i2 != -1) {
                i = Math.max(i, i2);
            }
        }
        if (i == -1) {
            return -1.0f;
        }
        return f * i;
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    public List<u> W(v vVar, j1 j1Var, boolean z2) throws MediaCodecUtil.DecoderQueryException {
        u d;
        String str = j1Var.w;
        if (str == null) {
            return Collections.emptyList();
        }
        if (this.P0.a(j1Var) && (d = MediaCodecUtil.d("audio/raw", false, false)) != null) {
            return Collections.singletonList(d);
        }
        List<u> a2 = vVar.a(str, z2, false);
        Pattern pattern = MediaCodecUtil.a;
        ArrayList arrayList = new ArrayList(a2);
        MediaCodecUtil.j(arrayList, new b.i.a.c.y2.g(j1Var));
        if ("audio/eac3-joc".equals(str)) {
            ArrayList arrayList2 = new ArrayList(arrayList);
            arrayList2.addAll(vVar.a("audio/eac3", z2, false));
            arrayList = arrayList2;
        }
        return Collections.unmodifiableList(arrayList);
    }

    /* JADX WARN: Removed duplicated region for block: B:26:0x0092  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x00b6  */
    /* JADX WARN: Removed duplicated region for block: B:44:0x00d2  */
    /* JADX WARN: Removed duplicated region for block: B:53:0x0109  */
    /* JADX WARN: Removed duplicated region for block: B:54:0x010b  */
    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.i.a.c.y2.t.a Y(b.i.a.c.y2.u r13, b.i.a.c.j1 r14, @androidx.annotation.Nullable android.media.MediaCrypto r15, float r16) {
        /*
            Method dump skipped, instructions count: 284
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.t2.z.Y(b.i.a.c.y2.u, b.i.a.c.j1, android.media.MediaCrypto, float):b.i.a.c.y2.t$a");
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer, b.i.a.c.f2
    public boolean b() {
        return this.F0 && this.P0.b();
    }

    @Override // b.i.a.c.f3.s
    public x1 c() {
        return this.P0.c();
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer, b.i.a.c.f2
    public boolean d() {
        return this.P0.k() || super.d();
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    public void d0(final Exception exc) {
        q.b("MediaCodecAudioRenderer", "Audio codec error", exc);
        final r.a aVar = this.O0;
        Handler handler = aVar.a;
        if (handler != null) {
            handler.post(new Runnable() { // from class: b.i.a.c.t2.c
                @Override // java.lang.Runnable
                public final void run() {
                    r.a aVar2 = r.a.this;
                    Exception exc2 = exc;
                    r rVar = aVar2.f1122b;
                    int i = e0.a;
                    rVar.T(exc2);
                }
            });
        }
    }

    @Override // b.i.a.c.f3.s
    public long e() {
        if (this.n == 2) {
            D0();
        }
        return this.T0;
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    public void e0(final String str, final long j, final long j2) {
        final r.a aVar = this.O0;
        Handler handler = aVar.a;
        if (handler != null) {
            handler.post(new Runnable() { // from class: b.i.a.c.t2.f
                @Override // java.lang.Runnable
                public final void run() {
                    r.a aVar2 = r.a.this;
                    String str2 = str;
                    long j3 = j;
                    long j4 = j2;
                    r rVar = aVar2.f1122b;
                    int i = e0.a;
                    rVar.C(str2, j3, j4);
                }
            });
        }
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    public void f0(final String str) {
        final r.a aVar = this.O0;
        Handler handler = aVar.a;
        if (handler != null) {
            handler.post(new Runnable() { // from class: b.i.a.c.t2.g
                @Override // java.lang.Runnable
                public final void run() {
                    r.a aVar2 = r.a.this;
                    String str2 = str;
                    r rVar = aVar2.f1122b;
                    int i = e0.a;
                    rVar.B(str2);
                }
            });
        }
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    @Nullable
    public g g0(k1 k1Var) throws ExoPlaybackException {
        final g g02 = super.g0(k1Var);
        final r.a aVar = this.O0;
        final j1 j1Var = k1Var.f1023b;
        Handler handler = aVar.a;
        if (handler != null) {
            handler.post(new Runnable() { // from class: b.i.a.c.t2.e
                @Override // java.lang.Runnable
                public final void run() {
                    r.a aVar2 = r.a.this;
                    j1 j1Var2 = j1Var;
                    g gVar = g02;
                    r rVar = aVar2.f1122b;
                    int i = e0.a;
                    rVar.U(j1Var2);
                    aVar2.f1122b.I(j1Var2, gVar);
                }
            });
        }
        return g02;
    }

    @Override // b.i.a.c.f2, b.i.a.c.g2
    public String getName() {
        return "MediaCodecAudioRenderer";
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    public void h0(j1 j1Var, @Nullable MediaFormat mediaFormat) throws ExoPlaybackException {
        int i;
        int i2;
        j1 j1Var2 = this.S0;
        int[] iArr = null;
        if (j1Var2 != null) {
            j1Var = j1Var2;
        } else if (this.S != null) {
            if ("audio/raw".equals(j1Var.w)) {
                i = j1Var.L;
            } else if (e0.a >= 24 && mediaFormat.containsKey("pcm-encoding")) {
                i = mediaFormat.getInteger("pcm-encoding");
            } else if (mediaFormat.containsKey("v-bits-per-sample")) {
                i = e0.r(mediaFormat.getInteger("v-bits-per-sample"));
            } else {
                i = "audio/raw".equals(j1Var.w) ? j1Var.L : 2;
            }
            j1.b bVar = new j1.b();
            bVar.k = "audio/raw";
            bVar.f1021z = i;
            bVar.A = j1Var.M;
            bVar.B = j1Var.N;
            bVar.f1019x = mediaFormat.getInteger("channel-count");
            bVar.f1020y = mediaFormat.getInteger("sample-rate");
            j1 a2 = bVar.a();
            if (this.R0 && a2.J == 6 && (i2 = j1Var.J) < 6) {
                iArr = new int[i2];
                for (int i3 = 0; i3 < j1Var.J; i3++) {
                    iArr[i3] = i3;
                }
            }
            j1Var = a2;
        }
        try {
            this.P0.v(j1Var, 0, iArr);
        } catch (AudioSink.ConfigurationException e) {
            throw z(e, e.format, false, 5001);
        }
    }

    @Override // b.i.a.c.f3.s
    public void i(x1 x1Var) {
        this.P0.i(x1Var);
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    public void j0() {
        this.P0.p();
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    public void k0(DecoderInputBuffer decoderInputBuffer) {
        if (this.U0 && !decoderInputBuffer.m()) {
            if (Math.abs(decoderInputBuffer.n - this.T0) > 500000) {
                this.T0 = decoderInputBuffer.n;
            }
            this.U0 = false;
        }
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    public boolean m0(long j, long j2, @Nullable t tVar, @Nullable ByteBuffer byteBuffer, int i, int i2, int i3, long j3, boolean z2, boolean z3, j1 j1Var) throws ExoPlaybackException {
        Objects.requireNonNull(byteBuffer);
        if (this.S0 != null && (i2 & 2) != 0) {
            Objects.requireNonNull(tVar);
            tVar.releaseOutputBuffer(i, false);
            return true;
        } else if (z2) {
            if (tVar != null) {
                tVar.releaseOutputBuffer(i, false);
            }
            this.J0.f += i3;
            this.P0.p();
            return true;
        } else {
            try {
                if (!this.P0.s(byteBuffer, j3, i3)) {
                    return false;
                }
                if (tVar != null) {
                    tVar.releaseOutputBuffer(i, false);
                }
                this.J0.e += i3;
                return true;
            } catch (AudioSink.InitializationException e) {
                throw z(e, e.format, e.isRecoverable, 5001);
            } catch (AudioSink.WriteException e2) {
                throw z(e2, j1Var, e2.isRecoverable, 5002);
            }
        }
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    public void p0() throws ExoPlaybackException {
        try {
            this.P0.j();
        } catch (AudioSink.WriteException e) {
            throw z(e, e.format, e.isRecoverable, 5002);
        }
    }

    @Override // b.i.a.c.v0, b.i.a.c.b2.b
    public void r(int i, @Nullable Object obj) throws ExoPlaybackException {
        if (i == 2) {
            this.P0.q(((Float) obj).floatValue());
        } else if (i == 3) {
            this.P0.o((o) obj);
        } else if (i != 6) {
            switch (i) {
                case 9:
                    this.P0.w(((Boolean) obj).booleanValue());
                    return;
                case 10:
                    this.P0.l(((Integer) obj).intValue());
                    return;
                case 11:
                    this.X0 = (f2.a) obj;
                    return;
                default:
                    return;
            }
        } else {
            this.P0.x((u) obj);
        }
    }

    @Override // b.i.a.c.v0, b.i.a.c.f2
    @Nullable
    public s w() {
        return this;
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    public boolean x0(j1 j1Var) {
        return this.P0.a(j1Var);
    }

    @Override // com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
    public int y0(v vVar, j1 j1Var) throws MediaCodecUtil.DecoderQueryException {
        if (!b.i.a.c.f3.t.h(j1Var.w)) {
            return 0;
        }
        int i = e0.a >= 21 ? 32 : 0;
        int i2 = j1Var.P;
        boolean z2 = i2 != 0;
        boolean z3 = i2 == 0 || i2 == 2;
        if (z3 && this.P0.a(j1Var) && (!z2 || MediaCodecUtil.d("audio/raw", false, false) != null)) {
            return i | 12;
        }
        if ("audio/raw".equals(j1Var.w) && !this.P0.a(j1Var)) {
            return 1;
        }
        AudioSink audioSink = this.P0;
        int i3 = j1Var.J;
        int i4 = j1Var.K;
        j1.b bVar = new j1.b();
        bVar.k = "audio/raw";
        bVar.f1019x = i3;
        bVar.f1020y = i4;
        bVar.f1021z = 2;
        if (!audioSink.a(bVar.a())) {
            return 1;
        }
        List<u> W = W(vVar, j1Var, false);
        if (W.isEmpty()) {
            return 1;
        }
        if (!z3) {
            return 2;
        }
        u uVar = W.get(0);
        boolean e = uVar.e(j1Var);
        return ((!e || !uVar.f(j1Var)) ? 8 : 16) | (e ? 4 : 3) | i;
    }
}
