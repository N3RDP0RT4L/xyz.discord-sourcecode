package b.i.a.c;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.Pair;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.view.SurfaceView;
import android.view.TextureView;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.a3.a0;
import b.i.a.c.a3.c0;
import b.i.a.c.a3.k0;
import b.i.a.c.a3.o0;
import b.i.a.c.b2;
import b.i.a.c.c3.h;
import b.i.a.c.c3.j;
import b.i.a.c.c3.q;
import b.i.a.c.c3.r;
import b.i.a.c.e1;
import b.i.a.c.e3.f;
import b.i.a.c.f3.b0;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.g;
import b.i.a.c.f3.n;
import b.i.a.c.f3.o;
import b.i.a.c.f3.p;
import b.i.a.c.g3.y;
import b.i.a.c.h1;
import b.i.a.c.o2;
import b.i.a.c.p1;
import b.i.a.c.s2.g1;
import b.i.a.c.s2.h1;
import b.i.a.c.y1;
import b.i.b.b.h0;
import com.google.android.exoplayer2.IllegalSeekPositionException;
import com.google.android.exoplayer2.PlaybackException;
import com.google.android.exoplayer2.metadata.Metadata;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArraySet;
/* compiled from: ExoPlayerImpl.java */
/* loaded from: classes3.dex */
public final class f1 extends u0 {

    /* renamed from: b  reason: collision with root package name */
    public static final /* synthetic */ int f954b = 0;
    public int A;
    public k0 B;
    public y1.b C;
    public p1 D;
    public p1 E;
    public w1 F;
    public int G;
    public long H;
    public final r c;
    public final y1.b d;
    public final f2[] e;
    public final q f;
    public final o g;
    public final h1.e h;
    public final h1 i;
    public final p<y1.c> j;
    public final CopyOnWriteArraySet<e1.a> k;
    public final o2.b l;
    public final List<a> m;
    public final boolean n;
    public final c0 o;
    @Nullable
    public final g1 p;
    public final Looper q;
    public final f r;

    /* renamed from: s  reason: collision with root package name */
    public final long f955s;
    public final long t;
    public final g u;
    public int v;
    public boolean w;

    /* renamed from: x  reason: collision with root package name */
    public int f956x;

    /* renamed from: y  reason: collision with root package name */
    public int f957y;

    /* renamed from: z  reason: collision with root package name */
    public boolean f958z;

    /* compiled from: ExoPlayerImpl.java */
    /* loaded from: classes3.dex */
    public static final class a implements t1 {
        public final Object a;

        /* renamed from: b  reason: collision with root package name */
        public o2 f959b;

        public a(Object obj, o2 o2Var) {
            this.a = obj;
            this.f959b = o2Var;
        }

        @Override // b.i.a.c.t1
        public o2 a() {
            return this.f959b;
        }

        @Override // b.i.a.c.t1
        public Object getUid() {
            return this.a;
        }
    }

    static {
        i1.a("goog.exo.exoplayer");
    }

    @SuppressLint({"HandlerLeak"})
    public f1(f2[] f2VarArr, q qVar, c0 c0Var, n1 n1Var, f fVar, @Nullable final g1 g1Var, boolean z2, j2 j2Var, long j, long j2, m1 m1Var, long j3, boolean z3, g gVar, Looper looper, @Nullable final y1 y1Var, y1.b bVar) {
        String hexString = Integer.toHexString(System.identityHashCode(this));
        String str = e0.e;
        StringBuilder Q = b.d.b.a.a.Q(b.d.b.a.a.b(str, b.d.b.a.a.b(hexString, 30)), "Init ", hexString, " [", "ExoPlayerLib/2.16.0");
        Q.append("] [");
        Q.append(str);
        Q.append("]");
        Log.i("ExoPlayerImpl", Q.toString());
        boolean z4 = false;
        d.D(f2VarArr.length > 0);
        this.e = f2VarArr;
        Objects.requireNonNull(qVar);
        this.f = qVar;
        this.o = c0Var;
        this.r = fVar;
        this.p = g1Var;
        this.n = z2;
        this.f955s = j;
        this.t = j2;
        this.q = looper;
        this.u = gVar;
        this.v = 0;
        this.j = new p<>(new CopyOnWriteArraySet(), looper, gVar, new p.b() { // from class: b.i.a.c.u
            @Override // b.i.a.c.f3.p.b
            public final void a(Object obj, n nVar) {
                ((y1.c) obj).E(y1.this, new y1.d(nVar));
            }
        });
        this.k = new CopyOnWriteArraySet<>();
        this.m = new ArrayList();
        this.B = new k0.a(0, new Random());
        this.c = new r(new h2[f2VarArr.length], new j[f2VarArr.length], p2.j, null);
        this.l = new o2.b();
        SparseBooleanArray sparseBooleanArray = new SparseBooleanArray();
        int[] iArr = {1, 2, 3, 13, 14, 15, 16, 17, 18, 19, 20, 30};
        for (int i = 0; i < 12; i++) {
            int i2 = iArr[i];
            d.D(!false);
            sparseBooleanArray.append(i2, true);
        }
        if (qVar instanceof h) {
            d.D(!false);
            sparseBooleanArray.append(29, true);
        }
        n nVar = bVar.k;
        for (int i3 = 0; i3 < nVar.c(); i3++) {
            int b2 = nVar.b(i3);
            d.D(true);
            sparseBooleanArray.append(b2, true);
        }
        d.D(true);
        n nVar2 = new n(sparseBooleanArray, null);
        this.d = new y1.b(nVar2, null);
        SparseBooleanArray sparseBooleanArray2 = new SparseBooleanArray();
        for (int i4 = 0; i4 < nVar2.c(); i4++) {
            int b3 = nVar2.b(i4);
            d.D(true);
            sparseBooleanArray2.append(b3, true);
        }
        d.D(true);
        sparseBooleanArray2.append(4, true);
        d.D(true);
        sparseBooleanArray2.append(10, true);
        d.D(true);
        this.C = new y1.b(new n(sparseBooleanArray2, null), null);
        p1 p1Var = p1.j;
        this.D = p1Var;
        this.E = p1Var;
        this.G = -1;
        this.g = gVar.b(looper, null);
        w wVar = new w(this);
        this.h = wVar;
        this.F = w1.h(this.c);
        if (g1Var != null) {
            d.D((g1Var.p == null || g1Var.m.f1077b.isEmpty()) ? true : z4);
            g1Var.p = y1Var;
            g1Var.q = g1Var.j.b(looper, null);
            p<b.i.a.c.s2.h1> pVar = g1Var.o;
            g1Var.o = new p<>(pVar.d, looper, pVar.a, new p.b() { // from class: b.i.a.c.s2.h
                @Override // b.i.a.c.f3.p.b
                public final void a(Object obj, n nVar3) {
                    h1 h1Var = (h1) obj;
                    SparseArray<h1.a> sparseArray = g1.this.n;
                    SparseArray sparseArray2 = new SparseArray(nVar3.c());
                    for (int i5 = 0; i5 < nVar3.c(); i5++) {
                        int b4 = nVar3.b(i5);
                        h1.a aVar = sparseArray.get(b4);
                        Objects.requireNonNull(aVar);
                        sparseArray2.append(b4, aVar);
                    }
                    h1Var.K();
                }
            });
            d0(g1Var);
            fVar.f(new Handler(looper), g1Var);
        }
        this.i = new h1(f2VarArr, qVar, this.c, n1Var, fVar, this.v, this.w, g1Var, j2Var, m1Var, j3, z3, looper, gVar, wVar);
    }

    public static long j0(w1 w1Var) {
        o2.c cVar = new o2.c();
        o2.b bVar = new o2.b();
        w1Var.f1142b.h(w1Var.c.a, bVar);
        long j = w1Var.d;
        if (j == -9223372036854775807L) {
            return w1Var.f1142b.n(bVar.l, cVar).f1041z;
        }
        return bVar.n + j;
    }

    public static boolean k0(w1 w1Var) {
        return w1Var.f == 3 && w1Var.m && w1Var.n == 0;
    }

    @Override // b.i.a.c.y1
    public List A() {
        b.i.b.b.a<Object> aVar = b.i.b.b.p.k;
        return h0.l;
    }

    @Override // b.i.a.c.y1
    public int B() {
        if (f()) {
            return this.F.c.f831b;
        }
        return -1;
    }

    @Override // b.i.a.c.y1
    public int C() {
        int h02 = h0();
        if (h02 == -1) {
            return 0;
        }
        return h02;
    }

    @Override // b.i.a.c.y1
    public void E(final int i) {
        if (this.v != i) {
            this.v = i;
            ((b0.b) this.i.q.a(11, i, 0)).b();
            this.j.b(8, new p.a() { // from class: b.i.a.c.k
                @Override // b.i.a.c.f3.p.a
                public final void invoke(Object obj) {
                    ((y1.c) obj).L(i);
                }
            });
            r0();
            this.j.a();
        }
    }

    @Override // b.i.a.c.y1
    public void F(@Nullable SurfaceView surfaceView) {
    }

    @Override // b.i.a.c.y1
    public int G() {
        return this.F.n;
    }

    @Override // b.i.a.c.y1
    public p2 H() {
        return this.F.j.d;
    }

    @Override // b.i.a.c.y1
    public int I() {
        return this.v;
    }

    @Override // b.i.a.c.y1
    public long J() {
        if (f()) {
            w1 w1Var = this.F;
            a0.a aVar = w1Var.c;
            w1Var.f1142b.h(aVar.a, this.l);
            return e0.M(this.l.a(aVar.f831b, aVar.c));
        }
        o2 K = K();
        if (K.q()) {
            return -9223372036854775807L;
        }
        return K.n(C(), this.a).b();
    }

    @Override // b.i.a.c.y1
    public o2 K() {
        return this.F.f1142b;
    }

    @Override // b.i.a.c.y1
    public Looper L() {
        return this.q;
    }

    @Override // b.i.a.c.y1
    public boolean M() {
        return this.w;
    }

    @Override // b.i.a.c.y1
    public long N() {
        if (this.F.f1142b.q()) {
            return this.H;
        }
        w1 w1Var = this.F;
        if (w1Var.l.d != w1Var.c.d) {
            return w1Var.f1142b.n(C(), this.a).b();
        }
        long j = w1Var.r;
        if (this.F.l.a()) {
            w1 w1Var2 = this.F;
            o2.b h = w1Var2.f1142b.h(w1Var2.l.a, this.l);
            long c = h.c(this.F.l.f831b);
            j = c == Long.MIN_VALUE ? h.m : c;
        }
        w1 w1Var3 = this.F;
        return e0.M(m0(w1Var3.f1142b, w1Var3.l, j));
    }

    @Override // b.i.a.c.y1
    public void Q(@Nullable TextureView textureView) {
    }

    @Override // b.i.a.c.y1
    public p1 S() {
        return this.D;
    }

    @Override // b.i.a.c.y1
    public long T() {
        return e0.M(g0(this.F));
    }

    @Override // b.i.a.c.y1
    public long U() {
        return this.f955s;
    }

    @Override // b.i.a.c.y1
    public void a() {
        w1 w1Var = this.F;
        if (w1Var.f == 1) {
            w1 e = w1Var.e(null);
            w1 f = e.f(e.f1142b.q() ? 4 : 2);
            this.f956x++;
            ((b0.b) this.i.q.c(0)).b();
            s0(f, 1, 1, false, false, 5, -9223372036854775807L, -1);
        }
    }

    @Override // b.i.a.c.y1
    public x1 c() {
        return this.F.o;
    }

    public void d0(y1.c cVar) {
        p<y1.c> pVar = this.j;
        if (!pVar.g) {
            Objects.requireNonNull(cVar);
            pVar.d.add(new p.c<>(cVar));
        }
    }

    public final p1 e0() {
        o2 K = K();
        o1 o1Var = K.q() ? null : K.n(C(), this.a).p;
        if (o1Var == null) {
            return this.E;
        }
        p1.b a2 = this.E.a();
        p1 p1Var = o1Var.n;
        if (p1Var != null) {
            CharSequence charSequence = p1Var.l;
            if (charSequence != null) {
                a2.a = charSequence;
            }
            CharSequence charSequence2 = p1Var.m;
            if (charSequence2 != null) {
                a2.f1046b = charSequence2;
            }
            CharSequence charSequence3 = p1Var.n;
            if (charSequence3 != null) {
                a2.c = charSequence3;
            }
            CharSequence charSequence4 = p1Var.o;
            if (charSequence4 != null) {
                a2.d = charSequence4;
            }
            CharSequence charSequence5 = p1Var.p;
            if (charSequence5 != null) {
                a2.e = charSequence5;
            }
            CharSequence charSequence6 = p1Var.q;
            if (charSequence6 != null) {
                a2.f = charSequence6;
            }
            CharSequence charSequence7 = p1Var.r;
            if (charSequence7 != null) {
                a2.g = charSequence7;
            }
            Uri uri = p1Var.f1042s;
            if (uri != null) {
                a2.h = uri;
            }
            d2 d2Var = p1Var.t;
            if (d2Var != null) {
                a2.i = d2Var;
            }
            d2 d2Var2 = p1Var.u;
            if (d2Var2 != null) {
                a2.j = d2Var2;
            }
            byte[] bArr = p1Var.v;
            if (bArr != null) {
                Integer num = p1Var.w;
                a2.k = (byte[]) bArr.clone();
                a2.l = num;
            }
            Uri uri2 = p1Var.f1043x;
            if (uri2 != null) {
                a2.m = uri2;
            }
            Integer num2 = p1Var.f1044y;
            if (num2 != null) {
                a2.n = num2;
            }
            Integer num3 = p1Var.f1045z;
            if (num3 != null) {
                a2.o = num3;
            }
            Integer num4 = p1Var.A;
            if (num4 != null) {
                a2.p = num4;
            }
            Boolean bool = p1Var.B;
            if (bool != null) {
                a2.q = bool;
            }
            Integer num5 = p1Var.C;
            if (num5 != null) {
                a2.r = num5;
            }
            Integer num6 = p1Var.D;
            if (num6 != null) {
                a2.r = num6;
            }
            Integer num7 = p1Var.E;
            if (num7 != null) {
                a2.f1047s = num7;
            }
            Integer num8 = p1Var.F;
            if (num8 != null) {
                a2.t = num8;
            }
            Integer num9 = p1Var.G;
            if (num9 != null) {
                a2.u = num9;
            }
            Integer num10 = p1Var.H;
            if (num10 != null) {
                a2.v = num10;
            }
            Integer num11 = p1Var.I;
            if (num11 != null) {
                a2.w = num11;
            }
            CharSequence charSequence8 = p1Var.J;
            if (charSequence8 != null) {
                a2.f1048x = charSequence8;
            }
            CharSequence charSequence9 = p1Var.K;
            if (charSequence9 != null) {
                a2.f1049y = charSequence9;
            }
            CharSequence charSequence10 = p1Var.L;
            if (charSequence10 != null) {
                a2.f1050z = charSequence10;
            }
            Integer num12 = p1Var.M;
            if (num12 != null) {
                a2.A = num12;
            }
            Integer num13 = p1Var.N;
            if (num13 != null) {
                a2.B = num13;
            }
            CharSequence charSequence11 = p1Var.O;
            if (charSequence11 != null) {
                a2.C = charSequence11;
            }
            CharSequence charSequence12 = p1Var.P;
            if (charSequence12 != null) {
                a2.D = charSequence12;
            }
            Bundle bundle = p1Var.Q;
            if (bundle != null) {
                a2.E = bundle;
            }
        }
        return a2.a();
    }

    @Override // b.i.a.c.y1
    public boolean f() {
        return this.F.c.a();
    }

    public b2 f0(b2.b bVar) {
        return new b2(this.i, bVar, this.F.f1142b, C(), this.u, this.i.f1003s);
    }

    @Override // b.i.a.c.y1
    public long g() {
        return e0.M(this.F.f1143s);
    }

    public final long g0(w1 w1Var) {
        if (w1Var.f1142b.q()) {
            return e0.B(this.H);
        }
        if (w1Var.c.a()) {
            return w1Var.t;
        }
        return m0(w1Var.f1142b, w1Var.c, w1Var.t);
    }

    @Override // b.i.a.c.y1
    public void h(int i, long j) {
        o2 o2Var = this.F.f1142b;
        if (i < 0 || (!o2Var.q() && i >= o2Var.p())) {
            throw new IllegalSeekPositionException(o2Var, i, j);
        }
        int i2 = 1;
        this.f956x++;
        if (f()) {
            Log.w("ExoPlayerImpl", "seekTo ignored because an ad is playing");
            h1.d dVar = new h1.d(this.F);
            dVar.a(1);
            f1 f1Var = ((w) this.h).a;
            f1Var.g.b(new c0(f1Var, dVar));
            return;
        }
        if (this.F.f != 1) {
            i2 = 2;
        }
        int C = C();
        w1 l0 = l0(this.F.f(i2), o2Var, i0(o2Var, i, j));
        ((b0.b) this.i.q.i(3, new h1.g(o2Var, i, e0.B(j)))).b();
        s0(l0, 0, 1, true, true, 1, g0(l0), C);
    }

    public final int h0() {
        if (this.F.f1142b.q()) {
            return this.G;
        }
        w1 w1Var = this.F;
        return w1Var.f1142b.h(w1Var.c.a, this.l).l;
    }

    @Override // b.i.a.c.y1
    public y1.b i() {
        return this.C;
    }

    @Nullable
    public final Pair<Object, Long> i0(o2 o2Var, int i, long j) {
        if (o2Var.q()) {
            this.G = i;
            if (j == -9223372036854775807L) {
                j = 0;
            }
            this.H = j;
            return null;
        }
        if (i == -1 || i >= o2Var.p()) {
            i = o2Var.a(this.w);
            j = o2Var.n(i, this.a).a();
        }
        return o2Var.j(this.a, this.l, i, e0.B(j));
    }

    @Override // b.i.a.c.y1
    public boolean j() {
        return this.F.m;
    }

    @Override // b.i.a.c.y1
    public void k(final boolean z2) {
        if (this.w != z2) {
            this.w = z2;
            ((b0.b) this.i.q.a(12, z2 ? 1 : 0, 0)).b();
            this.j.b(9, new p.a() { // from class: b.i.a.c.n
                @Override // b.i.a.c.f3.p.a
                public final void invoke(Object obj) {
                    ((y1.c) obj).D(z2);
                }
            });
            r0();
            this.j.a();
        }
    }

    @Override // b.i.a.c.y1
    public long l() {
        return 3000L;
    }

    public final w1 l0(w1 w1Var, o2 o2Var, @Nullable Pair<Object, Long> pair) {
        r rVar;
        a0.a aVar;
        List<Metadata> list;
        int i;
        long j;
        d.j(o2Var.q() || pair != null);
        o2 o2Var2 = w1Var.f1142b;
        w1 g = w1Var.g(o2Var);
        if (o2Var.q()) {
            a0.a aVar2 = w1.a;
            a0.a aVar3 = w1.a;
            long B = e0.B(this.H);
            o0 o0Var = o0.j;
            r rVar2 = this.c;
            b.i.b.b.a<Object> aVar4 = b.i.b.b.p.k;
            w1 a2 = g.b(aVar3, B, B, B, 0L, o0Var, rVar2, h0.l).a(aVar3);
            a2.r = a2.t;
            return a2;
        }
        Object obj = g.c.a;
        int i2 = e0.a;
        boolean z2 = !obj.equals(pair.first);
        a0.a aVar5 = z2 ? new a0.a(pair.first) : g.c;
        long longValue = ((Long) pair.second).longValue();
        long B2 = e0.B(w());
        if (!o2Var2.q()) {
            B2 -= o2Var2.h(obj, this.l).n;
        }
        if (z2 || longValue < B2) {
            d.D(!aVar5.a());
            o0 o0Var2 = z2 ? o0.j : g.i;
            if (z2) {
                aVar = aVar5;
                rVar = this.c;
            } else {
                aVar = aVar5;
                rVar = g.j;
            }
            r rVar3 = rVar;
            if (z2) {
                b.i.b.b.a<Object> aVar6 = b.i.b.b.p.k;
                list = h0.l;
            } else {
                list = g.k;
            }
            w1 a3 = g.b(aVar, longValue, longValue, longValue, 0L, o0Var2, rVar3, list).a(aVar);
            a3.r = longValue;
            return a3;
        }
        if (i == 0) {
            int b2 = o2Var.b(g.l.a);
            if (b2 == -1 || o2Var.f(b2, this.l).l != o2Var.h(aVar5.a, this.l).l) {
                o2Var.h(aVar5.a, this.l);
                if (aVar5.a()) {
                    j = this.l.a(aVar5.f831b, aVar5.c);
                } else {
                    j = this.l.m;
                }
                g = g.b(aVar5, g.t, g.t, g.e, j - g.t, g.i, g.j, g.k).a(aVar5);
                g.r = j;
            }
        } else {
            d.D(!aVar5.a());
            long max = Math.max(0L, g.f1143s - (longValue - B2));
            long j2 = g.r;
            if (g.l.equals(g.c)) {
                j2 = longValue + max;
            }
            g = g.b(aVar5, longValue, longValue, longValue, max, g.i, g.j, g.k);
            g.r = j2;
        }
        return g;
    }

    @Override // b.i.a.c.y1
    public int m() {
        if (this.F.f1142b.q()) {
            return 0;
        }
        w1 w1Var = this.F;
        return w1Var.f1142b.b(w1Var.c.a);
    }

    public final long m0(o2 o2Var, a0.a aVar, long j) {
        o2Var.h(aVar.a, this.l);
        return j + this.l.n;
    }

    @Override // b.i.a.c.y1
    public void n(@Nullable TextureView textureView) {
    }

    public void n0() {
        String str;
        boolean z2;
        String hexString = Integer.toHexString(System.identityHashCode(this));
        String str2 = e0.e;
        HashSet<String> hashSet = i1.a;
        synchronized (i1.class) {
            str = i1.f1012b;
        }
        StringBuilder Q = b.d.b.a.a.Q(b.d.b.a.a.b(str, b.d.b.a.a.b(str2, b.d.b.a.a.b(hexString, 36))), "Release ", hexString, " [", "ExoPlayerLib/2.16.0");
        b.d.b.a.a.q0(Q, "] [", str2, "] [", str);
        Q.append("]");
        Log.i("ExoPlayerImpl", Q.toString());
        h1 h1Var = this.i;
        synchronized (h1Var) {
            if (!h1Var.I && h1Var.r.isAlive()) {
                h1Var.q.f(7);
                long j = h1Var.E;
                synchronized (h1Var) {
                    long d = h1Var.f1006z.d() + j;
                    boolean z3 = false;
                    while (!Boolean.valueOf(h1Var.I).booleanValue() && j > 0) {
                        try {
                            h1Var.f1006z.c();
                            h1Var.wait(j);
                        } catch (InterruptedException unused) {
                            z3 = true;
                        }
                        j = d - h1Var.f1006z.d();
                    }
                    if (z3) {
                        Thread.currentThread().interrupt();
                    }
                    z2 = h1Var.I;
                }
            }
            z2 = true;
        }
        if (!z2) {
            p<y1.c> pVar = this.j;
            pVar.b(10, b0.a);
            pVar.a();
        }
        this.j.c();
        this.g.j(null);
        g1 g1Var = this.p;
        if (g1Var != null) {
            this.r.d(g1Var);
        }
        w1 f = this.F.f(1);
        this.F = f;
        w1 a2 = f.a(f.c);
        this.F = a2;
        a2.r = a2.t;
        this.F.f1143s = 0L;
    }

    @Override // b.i.a.c.y1
    public y o() {
        return y.j;
    }

    public void o0(y1.c cVar) {
        p<y1.c> pVar = this.j;
        Iterator<p.c<y1.c>> it = pVar.d.iterator();
        while (it.hasNext()) {
            p.c<y1.c> next = it.next();
            if (next.a.equals(cVar)) {
                p.b<y1.c> bVar = pVar.c;
                next.d = true;
                if (next.c) {
                    bVar.a(next.a, next.f970b.b());
                }
                pVar.d.remove(next);
            }
        }
    }

    @Override // b.i.a.c.y1
    public void p(y1.e eVar) {
        o0(eVar);
    }

    public final void p0(int i, int i2) {
        for (int i3 = i2 - 1; i3 >= i; i3--) {
            this.m.remove(i3);
        }
        this.B = this.B.a(i, i2);
    }

    @Override // b.i.a.c.y1
    public int q() {
        if (f()) {
            return this.F.c.c;
        }
        return -1;
    }

    public void q0(boolean z2, int i, int i2) {
        w1 w1Var = this.F;
        if (w1Var.m != z2 || w1Var.n != i) {
            this.f956x++;
            w1 d = w1Var.d(z2, i);
            ((b0.b) this.i.q.a(1, z2 ? 1 : 0, i)).b();
            s0(d, 0, i2, false, false, 5, -9223372036854775807L, -1);
        }
    }

    @Override // b.i.a.c.y1
    public void r(@Nullable SurfaceView surfaceView) {
    }

    public final void r0() {
        y1.b bVar = this.C;
        y1.b bVar2 = this.d;
        y1.b.a aVar = new y1.b.a();
        aVar.a(bVar2);
        boolean z2 = true;
        aVar.b(4, !f());
        aVar.b(5, a0() && !f());
        aVar.b(6, X() && !f());
        aVar.b(7, !K().q() && (X() || !Z() || a0()) && !f());
        aVar.b(8, W() && !f());
        aVar.b(9, !K().q() && (W() || (Z() && Y())) && !f());
        aVar.b(10, !f());
        aVar.b(11, a0() && !f());
        if (!a0() || f()) {
            z2 = false;
        }
        aVar.b(12, z2);
        y1.b c = aVar.c();
        this.C = c;
        if (!c.equals(bVar)) {
            this.j.b(13, new p.a() { // from class: b.i.a.c.x
                @Override // b.i.a.c.f3.p.a
                public final void invoke(Object obj) {
                    ((y1.c) obj).t(f1.this.C);
                }
            });
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:73:0x020d  */
    /* JADX WARN: Removed duplicated region for block: B:74:0x023b  */
    /* JADX WARN: Removed duplicated region for block: B:77:0x0253  */
    /* JADX WARN: Removed duplicated region for block: B:78:0x0260  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void s0(final b.i.a.c.w1 r39, final int r40, final int r41, boolean r42, boolean r43, final int r44, long r45, int r47) {
        /*
            Method dump skipped, instructions count: 959
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.f1.s0(b.i.a.c.w1, int, int, boolean, boolean, int, long, int):void");
    }

    @Override // b.i.a.c.y1
    @Nullable
    public PlaybackException t() {
        return this.F.g;
    }

    @Override // b.i.a.c.y1
    public void u(boolean z2) {
        q0(z2, 0, 1);
    }

    @Override // b.i.a.c.y1
    public long v() {
        return this.t;
    }

    @Override // b.i.a.c.y1
    public long w() {
        if (!f()) {
            return T();
        }
        w1 w1Var = this.F;
        w1Var.f1142b.h(w1Var.c.a, this.l);
        w1 w1Var2 = this.F;
        if (w1Var2.d == -9223372036854775807L) {
            return w1Var2.f1142b.n(C(), this.a).a();
        }
        return e0.M(this.l.n) + e0.M(this.F.d);
    }

    @Override // b.i.a.c.y1
    public void x(y1.e eVar) {
        d0(eVar);
    }

    @Override // b.i.a.c.y1
    public int y() {
        return this.F.f;
    }
}
