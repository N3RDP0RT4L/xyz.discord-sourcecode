package b.i.a.c;

import android.os.Bundle;
import b.c.a.a0.d;
import b.d.b.a.a;
import b.i.a.c.w0;
/* compiled from: lambda */
/* loaded from: classes3.dex */
public final /* synthetic */ class o0 implements w0.a {
    public static final /* synthetic */ o0 a = new o0();

    @Override // b.i.a.c.w0.a
    public final w0 a(Bundle bundle) {
        w0 w0Var;
        boolean z2 = false;
        int i = bundle.getInt(Integer.toString(0, 36), -1);
        if (i == 0) {
            d.j(bundle.getInt(l1.a(0), -1) == 0);
            if (bundle.getBoolean(l1.a(1), false)) {
                return new l1(bundle.getBoolean(l1.a(2), false));
            }
            w0Var = new l1();
        } else if (i == 1) {
            if (bundle.getInt(Integer.toString(0, 36), -1) == 1) {
                z2 = true;
            }
            d.j(z2);
            float f = bundle.getFloat(Integer.toString(1, 36), -1.0f);
            if (f != -1.0f) {
                return new v1(f);
            }
            w0Var = new v1();
        } else if (i == 2) {
            if (bundle.getInt(l2.a(0), -1) == 2) {
                z2 = true;
            }
            d.j(z2);
            int i2 = bundle.getInt(l2.a(1), 5);
            float f2 = bundle.getFloat(l2.a(2), -1.0f);
            if (f2 != -1.0f) {
                return new l2(i2, f2);
            }
            w0Var = new l2(i2);
        } else if (i == 3) {
            d.j(bundle.getInt(n2.a(0), -1) == 3);
            if (bundle.getBoolean(n2.a(1), false)) {
                return new n2(bundle.getBoolean(n2.a(2), false));
            }
            return new n2();
        } else {
            throw new IllegalArgumentException(a.f(44, "Encountered unknown rating type: ", i));
        }
        return w0Var;
    }
}
