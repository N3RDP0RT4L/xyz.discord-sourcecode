package b.i.a.c.z2.h;

import androidx.annotation.Nullable;
import b.i.a.c.f3.w;
import b.i.a.c.z2.d;
import b.i.a.c.z2.g;
import b.i.b.a.c;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.dvbsi.AppInfoTable;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
/* compiled from: AppInfoTableDecoder.java */
/* loaded from: classes3.dex */
public final class a extends g {
    @Override // b.i.a.c.z2.g
    @Nullable
    public Metadata b(d dVar, ByteBuffer byteBuffer) {
        if (byteBuffer.get() == 116) {
            w wVar = new w(byteBuffer.array(), byteBuffer.limit());
            wVar.m(12);
            int d = (wVar.d() + wVar.g(12)) - 4;
            wVar.m(44);
            wVar.n(wVar.g(12));
            wVar.m(16);
            ArrayList arrayList = new ArrayList();
            while (wVar.d() < d) {
                wVar.m(48);
                int g = wVar.g(8);
                wVar.m(4);
                int d2 = wVar.d() + wVar.g(12);
                String str = null;
                String str2 = null;
                while (wVar.d() < d2) {
                    int g2 = wVar.g(8);
                    int g3 = wVar.g(8);
                    int d3 = wVar.d() + g3;
                    if (g2 == 2) {
                        int g4 = wVar.g(16);
                        wVar.m(8);
                        if (g4 != 3) {
                        }
                        while (wVar.d() < d3) {
                            int g5 = wVar.g(8);
                            Charset charset = c.a;
                            byte[] bArr = new byte[g5];
                            wVar.i(bArr, 0, g5);
                            str = new String(bArr, charset);
                            int g6 = wVar.g(8);
                            for (int i = 0; i < g6; i++) {
                                wVar.n(wVar.g(8));
                            }
                        }
                    } else if (g2 == 21) {
                        Charset charset2 = c.a;
                        byte[] bArr2 = new byte[g3];
                        wVar.i(bArr2, 0, g3);
                        str2 = new String(bArr2, charset2);
                    }
                    wVar.k(d3 * 8);
                }
                wVar.k(d2 * 8);
                if (!(str == null || str2 == null)) {
                    arrayList.add(new AppInfoTable(g, str2.length() != 0 ? str.concat(str2) : new String(str)));
                }
            }
            if (!arrayList.isEmpty()) {
                return new Metadata(arrayList);
            }
        }
        return null;
    }
}
