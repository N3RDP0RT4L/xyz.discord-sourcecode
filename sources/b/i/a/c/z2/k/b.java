package b.i.a.c.z2.k;

import androidx.annotation.Nullable;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.w;
import b.i.a.c.f3.x;
import b.i.a.c.z2.d;
import b.i.a.c.z2.g;
import b.i.a.f.e.o.f;
import com.adjust.sdk.Constants;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.id3.ApicFrame;
import com.google.android.exoplayer2.metadata.id3.BinaryFrame;
import com.google.android.exoplayer2.metadata.id3.ChapterFrame;
import com.google.android.exoplayer2.metadata.id3.ChapterTocFrame;
import com.google.android.exoplayer2.metadata.id3.CommentFrame;
import com.google.android.exoplayer2.metadata.id3.GeobFrame;
import com.google.android.exoplayer2.metadata.id3.Id3Frame;
import com.google.android.exoplayer2.metadata.id3.MlltFrame;
import com.google.android.exoplayer2.metadata.id3.PrivFrame;
import com.google.android.exoplayer2.metadata.id3.TextInformationFrame;
import com.google.android.exoplayer2.metadata.id3.UrlLinkFrame;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
/* compiled from: Id3Decoder.java */
/* loaded from: classes3.dex */
public final class b extends g {
    public static final /* synthetic */ int a = 0;
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public final a f1320b;

    /* compiled from: Id3Decoder.java */
    /* loaded from: classes3.dex */
    public interface a {
        boolean a(int i, int i2, int i3, int i4, int i5);
    }

    /* compiled from: Id3Decoder.java */
    /* renamed from: b.i.a.c.z2.k.b$b  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0105b {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f1321b;
        public final int c;

        public C0105b(int i, boolean z2, int i2) {
            this.a = i;
            this.f1321b = z2;
            this.c = i2;
        }
    }

    public b() {
        this.f1320b = null;
    }

    public static byte[] c(byte[] bArr, int i, int i2) {
        if (i2 <= i) {
            return e0.f;
        }
        return Arrays.copyOfRange(bArr, i, i2);
    }

    public static ApicFrame e(x xVar, int i, int i2) throws UnsupportedEncodingException {
        int i3;
        String str;
        int t = xVar.t();
        String t2 = t(t);
        int i4 = i - 1;
        byte[] bArr = new byte[i4];
        System.arraycopy(xVar.a, xVar.f980b, bArr, 0, i4);
        xVar.f980b += i4;
        if (i2 == 2) {
            String valueOf = String.valueOf(f.u1(new String(bArr, 0, 3, "ISO-8859-1")));
            String concat = valueOf.length() != 0 ? "image/".concat(valueOf) : new String("image/");
            if ("image/jpg".equals(concat)) {
                concat = "image/jpeg";
            }
            str = concat;
            i3 = 2;
        } else {
            i3 = w(bArr, 0);
            String u1 = f.u1(new String(bArr, 0, i3, "ISO-8859-1"));
            if (u1.indexOf(47) == -1) {
                str = u1.length() != 0 ? "image/".concat(u1) : new String("image/");
            } else {
                str = u1;
            }
        }
        int i5 = i3 + 2;
        int v = v(bArr, i5, t);
        return new ApicFrame(str, new String(bArr, i5, v - i5, t2), bArr[i3 + 1] & 255, c(bArr, s(t) + v, i4));
    }

    public static BinaryFrame f(x xVar, int i, String str) {
        byte[] bArr = new byte[i];
        System.arraycopy(xVar.a, xVar.f980b, bArr, 0, i);
        xVar.f980b += i;
        return new BinaryFrame(str, bArr);
    }

    public static ChapterFrame g(x xVar, int i, int i2, boolean z2, int i3, @Nullable a aVar) throws UnsupportedEncodingException {
        int i4 = xVar.f980b;
        int w = w(xVar.a, i4);
        String str = new String(xVar.a, i4, w - i4, "ISO-8859-1");
        xVar.E(w + 1);
        int f = xVar.f();
        int f2 = xVar.f();
        long u = xVar.u();
        long j = u == 4294967295L ? -1L : u;
        long u2 = xVar.u();
        long j2 = u2 == 4294967295L ? -1L : u2;
        ArrayList arrayList = new ArrayList();
        int i5 = i4 + i;
        while (xVar.f980b < i5) {
            Id3Frame j3 = j(i2, xVar, z2, i3, aVar);
            if (j3 != null) {
                arrayList.add(j3);
            }
        }
        return new ChapterFrame(str, f, f2, j, j2, (Id3Frame[]) arrayList.toArray(new Id3Frame[0]));
    }

    public static ChapterTocFrame h(x xVar, int i, int i2, boolean z2, int i3, @Nullable a aVar) throws UnsupportedEncodingException {
        int i4 = xVar.f980b;
        int w = w(xVar.a, i4);
        String str = new String(xVar.a, i4, w - i4, "ISO-8859-1");
        xVar.E(w + 1);
        int t = xVar.t();
        boolean z3 = (t & 2) != 0;
        boolean z4 = (t & 1) != 0;
        int t2 = xVar.t();
        String[] strArr = new String[t2];
        for (int i5 = 0; i5 < t2; i5++) {
            int i6 = xVar.f980b;
            int w2 = w(xVar.a, i6);
            strArr[i5] = new String(xVar.a, i6, w2 - i6, "ISO-8859-1");
            xVar.E(w2 + 1);
        }
        ArrayList arrayList = new ArrayList();
        int i7 = i4 + i;
        while (xVar.f980b < i7) {
            Id3Frame j = j(i2, xVar, z2, i3, aVar);
            if (j != null) {
                arrayList.add(j);
            }
        }
        return new ChapterTocFrame(str, z3, z4, strArr, (Id3Frame[]) arrayList.toArray(new Id3Frame[0]));
    }

    @Nullable
    public static CommentFrame i(x xVar, int i) throws UnsupportedEncodingException {
        if (i < 4) {
            return null;
        }
        int t = xVar.t();
        String t2 = t(t);
        byte[] bArr = new byte[3];
        System.arraycopy(xVar.a, xVar.f980b, bArr, 0, 3);
        xVar.f980b += 3;
        String str = new String(bArr, 0, 3);
        int i2 = i - 4;
        byte[] bArr2 = new byte[i2];
        System.arraycopy(xVar.a, xVar.f980b, bArr2, 0, i2);
        xVar.f980b += i2;
        int v = v(bArr2, 0, t);
        String str2 = new String(bArr2, 0, v, t2);
        int s2 = s(t) + v;
        return new CommentFrame(str, str2, n(bArr2, s2, v(bArr2, s2, t), t2));
    }

    /* JADX WARN: Code restructure failed: missing block: B:130:0x0188, code lost:
        if (r13 == 67) goto L131;
     */
    @androidx.annotation.Nullable
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static com.google.android.exoplayer2.metadata.id3.Id3Frame j(int r19, b.i.a.c.f3.x r20, boolean r21, int r22, @androidx.annotation.Nullable b.i.a.c.z2.k.b.a r23) {
        /*
            Method dump skipped, instructions count: 560
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.z2.k.b.j(int, b.i.a.c.f3.x, boolean, int, b.i.a.c.z2.k.b$a):com.google.android.exoplayer2.metadata.id3.Id3Frame");
    }

    public static GeobFrame k(x xVar, int i) throws UnsupportedEncodingException {
        int t = xVar.t();
        String t2 = t(t);
        int i2 = i - 1;
        byte[] bArr = new byte[i2];
        System.arraycopy(xVar.a, xVar.f980b, bArr, 0, i2);
        xVar.f980b += i2;
        int w = w(bArr, 0);
        String str = new String(bArr, 0, w, "ISO-8859-1");
        int i3 = w + 1;
        int v = v(bArr, i3, t);
        String n = n(bArr, i3, v, t2);
        int s2 = s(t) + v;
        int v2 = v(bArr, s2, t);
        return new GeobFrame(str, n, n(bArr, s2, v2, t2), c(bArr, s(t) + v2, i2));
    }

    public static MlltFrame l(x xVar, int i) {
        int y2 = xVar.y();
        int v = xVar.v();
        int v2 = xVar.v();
        int t = xVar.t();
        int t2 = xVar.t();
        w wVar = new w();
        wVar.j(xVar.a, xVar.c);
        wVar.k(xVar.f980b * 8);
        int i2 = ((i - 10) * 8) / (t + t2);
        int[] iArr = new int[i2];
        int[] iArr2 = new int[i2];
        for (int i3 = 0; i3 < i2; i3++) {
            int g = wVar.g(t);
            int g2 = wVar.g(t2);
            iArr[i3] = g;
            iArr2[i3] = g2;
        }
        return new MlltFrame(y2, v, v2, iArr, iArr2);
    }

    public static PrivFrame m(x xVar, int i) throws UnsupportedEncodingException {
        byte[] bArr = new byte[i];
        System.arraycopy(xVar.a, xVar.f980b, bArr, 0, i);
        xVar.f980b += i;
        int w = w(bArr, 0);
        return new PrivFrame(new String(bArr, 0, w, "ISO-8859-1"), c(bArr, w + 1, i));
    }

    public static String n(byte[] bArr, int i, int i2, String str) throws UnsupportedEncodingException {
        return (i2 <= i || i2 > bArr.length) ? "" : new String(bArr, i, i2 - i, str);
    }

    @Nullable
    public static TextInformationFrame o(x xVar, int i, String str) throws UnsupportedEncodingException {
        if (i < 1) {
            return null;
        }
        int t = xVar.t();
        String t2 = t(t);
        int i2 = i - 1;
        byte[] bArr = new byte[i2];
        System.arraycopy(xVar.a, xVar.f980b, bArr, 0, i2);
        xVar.f980b += i2;
        return new TextInformationFrame(str, null, new String(bArr, 0, v(bArr, 0, t), t2));
    }

    @Nullable
    public static TextInformationFrame p(x xVar, int i) throws UnsupportedEncodingException {
        if (i < 1) {
            return null;
        }
        int t = xVar.t();
        String t2 = t(t);
        int i2 = i - 1;
        byte[] bArr = new byte[i2];
        System.arraycopy(xVar.a, xVar.f980b, bArr, 0, i2);
        xVar.f980b += i2;
        int v = v(bArr, 0, t);
        String str = new String(bArr, 0, v, t2);
        int s2 = s(t) + v;
        return new TextInformationFrame("TXXX", str, n(bArr, s2, v(bArr, s2, t), t2));
    }

    public static UrlLinkFrame q(x xVar, int i, String str) throws UnsupportedEncodingException {
        byte[] bArr = new byte[i];
        System.arraycopy(xVar.a, xVar.f980b, bArr, 0, i);
        xVar.f980b += i;
        return new UrlLinkFrame(str, null, new String(bArr, 0, w(bArr, 0), "ISO-8859-1"));
    }

    @Nullable
    public static UrlLinkFrame r(x xVar, int i) throws UnsupportedEncodingException {
        if (i < 1) {
            return null;
        }
        int t = xVar.t();
        String t2 = t(t);
        int i2 = i - 1;
        byte[] bArr = new byte[i2];
        System.arraycopy(xVar.a, xVar.f980b, bArr, 0, i2);
        xVar.f980b += i2;
        int v = v(bArr, 0, t);
        String str = new String(bArr, 0, v, t2);
        int s2 = s(t) + v;
        return new UrlLinkFrame("WXXX", str, n(bArr, s2, w(bArr, s2), "ISO-8859-1"));
    }

    public static int s(int i) {
        return (i == 0 || i == 3) ? 1 : 2;
    }

    public static String t(int i) {
        return i != 1 ? i != 2 ? i != 3 ? "ISO-8859-1" : Constants.ENCODING : "UTF-16BE" : "UTF-16";
    }

    public static String u(int i, int i2, int i3, int i4, int i5) {
        return i == 2 ? String.format(Locale.US, "%c%c%c", Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4)) : String.format(Locale.US, "%c%c%c%c", Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5));
    }

    public static int v(byte[] bArr, int i, int i2) {
        int w = w(bArr, i);
        if (i2 == 0 || i2 == 3) {
            return w;
        }
        while (w < bArr.length - 1) {
            if ((w - i) % 2 == 0 && bArr[w + 1] == 0) {
                return w;
            }
            w = w(bArr, w + 1);
        }
        return bArr.length;
    }

    public static int w(byte[] bArr, int i) {
        while (i < bArr.length) {
            if (bArr[i] == 0) {
                return i;
            }
            i++;
        }
        return bArr.length;
    }

    public static int x(x xVar, int i) {
        byte[] bArr = xVar.a;
        int i2 = xVar.f980b;
        int i3 = i2;
        while (true) {
            int i4 = i3 + 1;
            if (i4 >= i2 + i) {
                return i;
            }
            if ((bArr[i3] & 255) == 255 && bArr[i4] == 0) {
                System.arraycopy(bArr, i3 + 2, bArr, i4, (i - (i3 - i2)) - 2);
                i--;
            }
            i3 = i4;
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:31:0x0074, code lost:
        if ((r10 & 1) != 0) goto L41;
     */
    /* JADX WARN: Code restructure failed: missing block: B:38:0x0082, code lost:
        if ((r10 & 128) != 0) goto L41;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static boolean y(b.i.a.c.f3.x r18, int r19, int r20, boolean r21) {
        /*
            r1 = r18
            r0 = r19
            int r2 = r1.f980b
        L6:
            int r3 = r18.a()     // Catch: java.lang.Throwable -> Lab
            r4 = 1
            r5 = r20
            if (r3 < r5) goto La7
            r3 = 3
            r6 = 0
            if (r0 < r3) goto L20
            int r7 = r18.f()     // Catch: java.lang.Throwable -> Lab
            long r8 = r18.u()     // Catch: java.lang.Throwable -> Lab
            int r10 = r18.y()     // Catch: java.lang.Throwable -> Lab
            goto L2a
        L20:
            int r7 = r18.v()     // Catch: java.lang.Throwable -> Lab
            int r8 = r18.v()     // Catch: java.lang.Throwable -> Lab
            long r8 = (long) r8
            r10 = 0
        L2a:
            r11 = 0
            if (r7 != 0) goto L38
            int r7 = (r8 > r11 ? 1 : (r8 == r11 ? 0 : -1))
            if (r7 != 0) goto L38
            if (r10 != 0) goto L38
            r1.E(r2)
            return r4
        L38:
            r7 = 4
            if (r0 != r7) goto L69
            if (r21 != 0) goto L69
            r13 = 8421504(0x808080, double:4.160776E-317)
            long r13 = r13 & r8
            int r15 = (r13 > r11 ? 1 : (r13 == r11 ? 0 : -1))
            if (r15 == 0) goto L49
            r1.E(r2)
            return r6
        L49:
            r11 = 255(0xff, double:1.26E-321)
            long r13 = r8 & r11
            r15 = 8
            long r15 = r8 >> r15
            long r15 = r15 & r11
            r17 = 7
            long r15 = r15 << r17
            long r13 = r13 | r15
            r15 = 16
            long r15 = r8 >> r15
            long r15 = r15 & r11
            r17 = 14
            long r15 = r15 << r17
            long r13 = r13 | r15
            r15 = 24
            long r8 = r8 >> r15
            long r8 = r8 & r11
            r11 = 21
            long r8 = r8 << r11
            long r8 = r8 | r13
        L69:
            if (r0 != r7) goto L77
            r3 = r10 & 64
            if (r3 == 0) goto L71
            r3 = 1
            goto L72
        L71:
            r3 = 0
        L72:
            r7 = r10 & 1
            if (r7 == 0) goto L86
            goto L87
        L77:
            if (r0 != r3) goto L85
            r3 = r10 & 32
            if (r3 == 0) goto L7f
            r3 = 1
            goto L80
        L7f:
            r3 = 0
        L80:
            r7 = r10 & 128(0x80, float:1.794E-43)
            if (r7 == 0) goto L86
            goto L87
        L85:
            r3 = 0
        L86:
            r4 = 0
        L87:
            if (r4 == 0) goto L8b
            int r3 = r3 + 4
        L8b:
            long r3 = (long) r3
            int r7 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1))
            if (r7 >= 0) goto L94
            r1.E(r2)
            return r6
        L94:
            int r3 = r18.a()     // Catch: java.lang.Throwable -> Lab
            long r3 = (long) r3
            int r7 = (r3 > r8 ? 1 : (r3 == r8 ? 0 : -1))
            if (r7 >= 0) goto La1
            r1.E(r2)
            return r6
        La1:
            int r3 = (int) r8
            r1.F(r3)     // Catch: java.lang.Throwable -> Lab
            goto L6
        La7:
            r1.E(r2)
            return r4
        Lab:
            r0 = move-exception
            r1.E(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.z2.k.b.y(b.i.a.c.f3.x, int, int, boolean):boolean");
    }

    @Override // b.i.a.c.z2.g
    @Nullable
    public Metadata b(d dVar, ByteBuffer byteBuffer) {
        return d(byteBuffer.array(), byteBuffer.limit());
    }

    /* JADX WARN: Removed duplicated region for block: B:50:0x00c1 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:51:0x00c2  */
    @androidx.annotation.Nullable
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public com.google.android.exoplayer2.metadata.Metadata d(byte[] r13, int r14) {
        /*
            Method dump skipped, instructions count: 271
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.z2.k.b.d(byte[], int):com.google.android.exoplayer2.metadata.Metadata");
    }

    public b(@Nullable a aVar) {
        this.f1320b = aVar;
    }
}
