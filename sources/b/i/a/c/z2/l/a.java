package b.i.a.c.z2.l;

import b.i.a.c.f3.d0;
import b.i.a.c.f3.w;
import b.i.a.c.f3.x;
import b.i.a.c.z2.d;
import b.i.a.c.z2.g;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.scte35.PrivateCommand;
import com.google.android.exoplayer2.metadata.scte35.SpliceInsertCommand;
import com.google.android.exoplayer2.metadata.scte35.SpliceNullCommand;
import com.google.android.exoplayer2.metadata.scte35.SpliceScheduleCommand;
import com.google.android.exoplayer2.metadata.scte35.TimeSignalCommand;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
/* compiled from: SpliceInfoDecoder.java */
/* loaded from: classes3.dex */
public final class a extends g {
    public final x a = new x();

    /* renamed from: b  reason: collision with root package name */
    public final w f1322b = new w();
    public d0 c;

    @Override // b.i.a.c.z2.g
    public Metadata b(d dVar, ByteBuffer byteBuffer) {
        int i;
        int i2;
        int i3;
        long j;
        boolean z2;
        long j2;
        ArrayList arrayList;
        boolean z3;
        boolean z4;
        int i4;
        long j3;
        long j4;
        boolean z5;
        int i5;
        int i6;
        int i7;
        long j5;
        boolean z6;
        List list;
        boolean z7;
        boolean z8;
        boolean z9;
        long j6;
        long j7;
        boolean z10;
        d0 d0Var = this.c;
        if (d0Var == null || dVar.r != d0Var.d()) {
            d0 d0Var2 = new d0(dVar.n);
            this.c = d0Var2;
            d0Var2.a(dVar.n - dVar.r);
        }
        byte[] array = byteBuffer.array();
        int limit = byteBuffer.limit();
        this.a.C(array, limit);
        this.f1322b.j(array, limit);
        this.f1322b.m(39);
        long g = (this.f1322b.g(1) << 32) | this.f1322b.g(32);
        this.f1322b.m(20);
        int g2 = this.f1322b.g(12);
        int g3 = this.f1322b.g(8);
        this.a.F(14);
        Metadata.Entry entry = null;
        if (g3 == 0) {
            entry = new SpliceNullCommand();
        } else if (g3 != 255) {
            long j8 = 128;
            if (g3 == 4) {
                x xVar = this.a;
                int t = xVar.t();
                ArrayList arrayList2 = new ArrayList(t);
                int i8 = 0;
                while (i8 < t) {
                    long u = xVar.u();
                    boolean z11 = (xVar.t() & 128) != 0;
                    ArrayList arrayList3 = new ArrayList();
                    if (!z11) {
                        int t2 = xVar.t();
                        boolean z12 = (t2 & 128) != 0;
                        boolean z13 = (t2 & 64) != 0;
                        boolean z14 = (t2 & 32) != 0;
                        long u2 = z13 ? xVar.u() : -9223372036854775807L;
                        if (!z13) {
                            int t3 = xVar.t();
                            ArrayList arrayList4 = new ArrayList(t3);
                            for (int i9 = 0; i9 < t3; i9++) {
                                t3 = t3;
                                t = t;
                                arrayList4.add(new SpliceScheduleCommand.b(xVar.t(), xVar.u(), null));
                            }
                            i4 = t;
                            arrayList3 = arrayList4;
                        } else {
                            i4 = t;
                        }
                        if (z14) {
                            long t4 = xVar.t();
                            j3 = 128;
                            z5 = (t4 & 128) != 0;
                            j4 = ((((t4 & 1) << 32) | xVar.u()) * 1000) / 90;
                        } else {
                            j3 = 128;
                            z5 = false;
                            j4 = -9223372036854775807L;
                        }
                        z2 = z5;
                        j = j4;
                        arrayList = arrayList3;
                        i3 = xVar.y();
                        z4 = z12;
                        z3 = z13;
                        j2 = u2;
                        i2 = xVar.t();
                        i = xVar.t();
                    } else {
                        i4 = t;
                        j3 = j8;
                        arrayList = arrayList3;
                        z4 = false;
                        z3 = false;
                        j2 = -9223372036854775807L;
                        z2 = false;
                        j = -9223372036854775807L;
                        i3 = 0;
                        i2 = 0;
                        i = 0;
                    }
                    arrayList2.add(new SpliceScheduleCommand.c(u, z11, z4, z3, arrayList, j2, z2, j, i3, i2, i));
                    i8++;
                    j8 = j3;
                    t = i4;
                }
                entry = new SpliceScheduleCommand(arrayList2);
            } else if (g3 == 5) {
                x xVar2 = this.a;
                d0 d0Var3 = this.c;
                long u3 = xVar2.u();
                boolean z15 = (xVar2.t() & 128) != 0;
                List emptyList = Collections.emptyList();
                if (!z15) {
                    int t5 = xVar2.t();
                    boolean z16 = (t5 & 128) != 0;
                    boolean z17 = (t5 & 64) != 0;
                    boolean z18 = (t5 & 32) != 0;
                    boolean z19 = (t5 & 16) != 0;
                    long a = (!z17 || z19) ? -9223372036854775807L : TimeSignalCommand.a(xVar2, g);
                    if (!z17) {
                        int t6 = xVar2.t();
                        ArrayList arrayList5 = new ArrayList(t6);
                        for (int i10 = 0; i10 < t6; i10++) {
                            int t7 = xVar2.t();
                            long a2 = !z19 ? TimeSignalCommand.a(xVar2, g) : -9223372036854775807L;
                            arrayList5.add(new SpliceInsertCommand.b(t7, a2, d0Var3.b(a2), null));
                        }
                        emptyList = arrayList5;
                    }
                    if (z18) {
                        long t8 = xVar2.t();
                        z10 = (t8 & 128) != 0;
                        j7 = ((((t8 & 1) << 32) | xVar2.u()) * 1000) / 90;
                    } else {
                        z10 = false;
                        j7 = -9223372036854775807L;
                    }
                    int y2 = xVar2.y();
                    int t9 = xVar2.t();
                    i7 = y2;
                    z6 = z10;
                    i5 = xVar2.t();
                    list = emptyList;
                    j5 = j7;
                    i6 = t9;
                    z9 = z16;
                    j6 = a;
                    z7 = z19;
                    z8 = z17;
                } else {
                    list = emptyList;
                    j6 = -9223372036854775807L;
                    z9 = false;
                    z8 = false;
                    z7 = false;
                    z6 = false;
                    j5 = -9223372036854775807L;
                    i7 = 0;
                    i6 = 0;
                    i5 = 0;
                }
                entry = new SpliceInsertCommand(u3, z15, z9, z8, z7, j6, d0Var3.b(j6), list, z6, j5, i7, i6, i5);
            } else if (g3 == 6) {
                x xVar3 = this.a;
                d0 d0Var4 = this.c;
                long a3 = TimeSignalCommand.a(xVar3, g);
                entry = new TimeSignalCommand(a3, d0Var4.b(a3));
            }
        } else {
            x xVar4 = this.a;
            long u4 = xVar4.u();
            int i11 = g2 - 4;
            byte[] bArr = new byte[i11];
            System.arraycopy(xVar4.a, xVar4.f980b, bArr, 0, i11);
            xVar4.f980b += i11;
            entry = new PrivateCommand(u4, bArr, g);
        }
        return entry == null ? new Metadata(new Metadata.Entry[0]) : new Metadata(entry);
    }
}
