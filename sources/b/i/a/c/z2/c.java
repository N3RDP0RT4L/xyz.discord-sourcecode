package b.i.a.c.z2;

import b.i.a.c.j1;
import b.i.a.c.z2.k.b;
/* compiled from: MetadataDecoderFactory.java */
/* loaded from: classes3.dex */
public interface c {
    public static final c a = new a();

    /* compiled from: MetadataDecoderFactory.java */
    /* loaded from: classes3.dex */
    public class a implements c {
        @Override // b.i.a.c.z2.c
        public boolean a(j1 j1Var) {
            String str = j1Var.w;
            return "application/id3".equals(str) || "application/x-emsg".equals(str) || "application/x-scte35".equals(str) || "application/x-icy".equals(str) || "application/vnd.dvb.ait".equals(str);
        }

        @Override // b.i.a.c.z2.c
        public b b(j1 j1Var) {
            String str = j1Var.w;
            if (str != null) {
                char c = 65535;
                switch (str.hashCode()) {
                    case -1354451219:
                        if (str.equals("application/vnd.dvb.ait")) {
                            c = 0;
                            break;
                        }
                        break;
                    case -1348231605:
                        if (str.equals("application/x-icy")) {
                            c = 1;
                            break;
                        }
                        break;
                    case -1248341703:
                        if (str.equals("application/id3")) {
                            c = 2;
                            break;
                        }
                        break;
                    case 1154383568:
                        if (str.equals("application/x-emsg")) {
                            c = 3;
                            break;
                        }
                        break;
                    case 1652648887:
                        if (str.equals("application/x-scte35")) {
                            c = 4;
                            break;
                        }
                        break;
                }
                switch (c) {
                    case 0:
                        return new b.i.a.c.z2.h.a();
                    case 1:
                        return new b.i.a.c.z2.j.a();
                    case 2:
                        return new b();
                    case 3:
                        return new b.i.a.c.z2.i.a();
                    case 4:
                        return new b.i.a.c.z2.l.a();
                }
            }
            String valueOf = String.valueOf(str);
            throw new IllegalArgumentException(valueOf.length() != 0 ? "Attempted to create decoder for unsupported MIME type: ".concat(valueOf) : new String("Attempted to create decoder for unsupported MIME type: "));
        }
    }

    boolean a(j1 j1Var);

    b b(j1 j1Var);
}
