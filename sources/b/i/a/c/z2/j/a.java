package b.i.a.c.z2.j;

import b.i.a.c.z2.d;
import b.i.a.c.z2.g;
import b.i.a.f.e.o.f;
import b.i.b.a.c;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.icy.IcyInfo;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.CharsetDecoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/* compiled from: IcyDecoder.java */
/* loaded from: classes3.dex */
public final class a extends g {
    public static final Pattern a = Pattern.compile("(.+?)='(.*?)';", 32);

    /* renamed from: b  reason: collision with root package name */
    public final CharsetDecoder f1319b = c.c.newDecoder();
    public final CharsetDecoder c = c.f1638b.newDecoder();

    /* JADX WARN: Finally extract failed */
    @Override // b.i.a.c.z2.g
    public Metadata b(d dVar, ByteBuffer byteBuffer) {
        String str;
        String str2 = null;
        try {
            str = this.f1319b.decode(byteBuffer).toString();
        } catch (CharacterCodingException unused) {
            try {
                str = this.c.decode(byteBuffer).toString();
                this.c.reset();
                byteBuffer.rewind();
            } catch (CharacterCodingException unused2) {
                this.c.reset();
                byteBuffer.rewind();
                str = null;
            } catch (Throwable th) {
                this.c.reset();
                byteBuffer.rewind();
                throw th;
            }
        } finally {
            this.f1319b.reset();
            byteBuffer.rewind();
        }
        byte[] bArr = new byte[byteBuffer.limit()];
        byteBuffer.get(bArr);
        if (str == null) {
            return new Metadata(new IcyInfo(bArr, null, null));
        }
        Matcher matcher = a.matcher(str);
        String str3 = null;
        for (int i = 0; matcher.find(i); i = matcher.end()) {
            String group = matcher.group(1);
            String group2 = matcher.group(2);
            if (group != null) {
                String u1 = f.u1(group);
                u1.hashCode();
                if (u1.equals("streamurl")) {
                    str3 = group2;
                } else if (u1.equals("streamtitle")) {
                    str2 = group2;
                }
            }
        }
        return new Metadata(new IcyInfo(bArr, str2, str3));
    }
}
