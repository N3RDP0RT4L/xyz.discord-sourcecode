package b.i.a.c.z2;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import androidx.annotation.Nullable;
import b.i.a.c.f3.e0;
import b.i.a.c.j1;
import b.i.a.c.k1;
import b.i.a.c.v0;
import com.google.android.exoplayer2.metadata.Metadata;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
/* compiled from: MetadataRenderer.java */
/* loaded from: classes3.dex */
public final class f extends v0 implements Handler.Callback {
    public boolean A;
    public long B;
    public long C;
    @Nullable
    public Metadata D;
    public final c u;
    public final e v;
    @Nullable
    public final Handler w;

    /* renamed from: x  reason: collision with root package name */
    public final d f1315x;
    @Nullable

    /* renamed from: y  reason: collision with root package name */
    public b f1316y;

    /* renamed from: z  reason: collision with root package name */
    public boolean f1317z;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public f(e eVar, @Nullable Looper looper) {
        super(5);
        Handler handler;
        c cVar = c.a;
        Objects.requireNonNull(eVar);
        this.v = eVar;
        if (looper == null) {
            handler = null;
        } else {
            int i = e0.a;
            handler = new Handler(looper, this);
        }
        this.w = handler;
        this.u = cVar;
        this.f1315x = new d();
        this.C = -9223372036854775807L;
    }

    @Override // b.i.a.c.v0
    public void B() {
        this.D = null;
        this.C = -9223372036854775807L;
        this.f1316y = null;
    }

    @Override // b.i.a.c.v0
    public void D(long j, boolean z2) {
        this.D = null;
        this.C = -9223372036854775807L;
        this.f1317z = false;
        this.A = false;
    }

    @Override // b.i.a.c.v0
    public void H(j1[] j1VarArr, long j, long j2) {
        this.f1316y = this.u.b(j1VarArr[0]);
    }

    public final void J(Metadata metadata, List<Metadata.Entry> list) {
        int i = 0;
        while (true) {
            Metadata.Entry[] entryArr = metadata.j;
            if (i < entryArr.length) {
                j1 y2 = entryArr[i].y();
                if (y2 == null || !this.u.a(y2)) {
                    list.add(metadata.j[i]);
                } else {
                    b b2 = this.u.b(y2);
                    byte[] o0 = metadata.j[i].o0();
                    Objects.requireNonNull(o0);
                    this.f1315x.p();
                    this.f1315x.r(o0.length);
                    ByteBuffer byteBuffer = this.f1315x.l;
                    int i2 = e0.a;
                    byteBuffer.put(o0);
                    this.f1315x.s();
                    Metadata a = b2.a(this.f1315x);
                    if (a != null) {
                        J(a, list);
                    }
                }
                i++;
            } else {
                return;
            }
        }
    }

    @Override // b.i.a.c.g2
    public int a(j1 j1Var) {
        if (!this.u.a(j1Var)) {
            return 0;
        }
        return (j1Var.P == 0 ? 4 : 2) | 0 | 0;
    }

    @Override // b.i.a.c.f2
    public boolean b() {
        return this.A;
    }

    @Override // b.i.a.c.f2
    public boolean d() {
        return true;
    }

    @Override // b.i.a.c.f2, b.i.a.c.g2
    public String getName() {
        return "MetadataRenderer";
    }

    @Override // android.os.Handler.Callback
    public boolean handleMessage(Message message) {
        if (message.what == 0) {
            this.v.b((Metadata) message.obj);
            return true;
        }
        throw new IllegalStateException();
    }

    @Override // b.i.a.c.f2
    public void q(long j, long j2) {
        boolean z2 = true;
        while (z2) {
            if (!this.f1317z && this.D == null) {
                this.f1315x.p();
                k1 A = A();
                int I = I(A, this.f1315x, 0);
                if (I == -4) {
                    if (this.f1315x.n()) {
                        this.f1317z = true;
                    } else {
                        d dVar = this.f1315x;
                        dVar.r = this.B;
                        dVar.s();
                        b bVar = this.f1316y;
                        int i = e0.a;
                        Metadata a = bVar.a(this.f1315x);
                        if (a != null) {
                            ArrayList arrayList = new ArrayList(a.j.length);
                            J(a, arrayList);
                            if (!arrayList.isEmpty()) {
                                this.D = new Metadata(arrayList);
                                this.C = this.f1315x.n;
                            }
                        }
                    }
                } else if (I == -5) {
                    j1 j1Var = A.f1023b;
                    Objects.requireNonNull(j1Var);
                    this.B = j1Var.A;
                }
            }
            Metadata metadata = this.D;
            if (metadata == null || this.C > j) {
                z2 = false;
            } else {
                Handler handler = this.w;
                if (handler != null) {
                    handler.obtainMessage(0, metadata).sendToTarget();
                } else {
                    this.v.b(metadata);
                }
                this.D = null;
                this.C = -9223372036854775807L;
                z2 = true;
            }
            if (this.f1317z && this.D == null) {
                this.A = true;
            }
        }
    }
}
