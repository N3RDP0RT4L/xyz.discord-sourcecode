package b.i.a.c;

import android.content.Context;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.os.Handler;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.d.b.a.a;
import b.i.a.c.f3.e0;
import b.i.a.c.k2;
import b.i.a.c.t0;
import b.i.a.c.t2.o;
import java.util.Objects;
import org.webrtc.MediaStreamTrack;
/* compiled from: AudioFocusManager.java */
/* loaded from: classes3.dex */
public final class t0 {
    public final AudioManager a;

    /* renamed from: b  reason: collision with root package name */
    public final a f1110b;
    @Nullable
    public b c;
    @Nullable
    public o d;
    public int f;
    public AudioFocusRequest h;
    public float g = 1.0f;
    public int e = 0;

    /* compiled from: AudioFocusManager.java */
    /* loaded from: classes3.dex */
    public class a implements AudioManager.OnAudioFocusChangeListener {
        public final Handler j;

        public a(Handler handler) {
            this.j = handler;
        }

        @Override // android.media.AudioManager.OnAudioFocusChangeListener
        public void onAudioFocusChange(final int i) {
            this.j.post(new Runnable() { // from class: b.i.a.c.c
                @Override // java.lang.Runnable
                public final void run() {
                    t0.a aVar = t0.a.this;
                    int i2 = i;
                    t0 t0Var = t0.this;
                    Objects.requireNonNull(t0Var);
                    boolean z2 = true;
                    if (i2 == -3 || i2 == -2) {
                        if (i2 != -2) {
                            o oVar = t0Var.d;
                            if (oVar == null || oVar.k != 1) {
                                z2 = false;
                            }
                            if (!z2) {
                                t0Var.d(3);
                                return;
                            }
                        }
                        t0Var.b(0);
                        t0Var.d(2);
                    } else if (i2 == -1) {
                        t0Var.b(-1);
                        t0Var.a();
                    } else if (i2 != 1) {
                        a.d0(38, "Unknown focus change type: ", i2, "AudioFocusManager");
                    } else {
                        t0Var.d(1);
                        t0Var.b(1);
                    }
                }
            });
        }
    }

    /* compiled from: AudioFocusManager.java */
    /* loaded from: classes3.dex */
    public interface b {
    }

    public t0(Context context, Handler handler, b bVar) {
        AudioManager audioManager = (AudioManager) context.getApplicationContext().getSystemService(MediaStreamTrack.AUDIO_TRACK_KIND);
        Objects.requireNonNull(audioManager);
        this.a = audioManager;
        this.c = bVar;
        this.f1110b = new a(handler);
    }

    public final void a() {
        if (this.e != 0) {
            if (e0.a >= 26) {
                AudioFocusRequest audioFocusRequest = this.h;
                if (audioFocusRequest != null) {
                    this.a.abandonAudioFocusRequest(audioFocusRequest);
                }
            } else {
                this.a.abandonAudioFocus(this.f1110b);
            }
            d(0);
        }
    }

    public final void b(int i) {
        b bVar = this.c;
        if (bVar != null) {
            k2.b bVar2 = (k2.b) bVar;
            boolean j = k2.this.j();
            k2.this.m0(j, i, k2.g0(j, i));
        }
    }

    public void c(@Nullable o oVar) {
        if (!e0.a(this.d, null)) {
            this.d = null;
            this.f = 0;
            d.m(true, "Automatic handling of audio focus is only available for USAGE_MEDIA and USAGE_GAME.");
        }
    }

    public final void d(int i) {
        if (this.e != i) {
            this.e = i;
            float f = i == 3 ? 0.2f : 1.0f;
            if (this.g != f) {
                this.g = f;
                b bVar = this.c;
                if (bVar != null) {
                    k2 k2Var = k2.this;
                    k2Var.j0(1, 2, Float.valueOf(k2Var.B * k2Var.k.g));
                }
            }
        }
    }

    public int e(boolean z2, int i) {
        int i2;
        AudioFocusRequest.Builder builder;
        int i3 = 1;
        if (i == 1 || this.f != 1) {
            a();
            return z2 ? 1 : -1;
        } else if (!z2) {
            return -1;
        } else {
            if (this.e != 1) {
                if (e0.a >= 26) {
                    AudioFocusRequest audioFocusRequest = this.h;
                    if (audioFocusRequest == null) {
                        if (audioFocusRequest == null) {
                            builder = new AudioFocusRequest.Builder(this.f);
                        } else {
                            builder = new AudioFocusRequest.Builder(this.h);
                        }
                        o oVar = this.d;
                        boolean z3 = oVar != null && oVar.k == 1;
                        Objects.requireNonNull(oVar);
                        this.h = builder.setAudioAttributes(oVar.a()).setWillPauseWhenDucked(z3).setOnAudioFocusChangeListener(this.f1110b).build();
                    }
                    i2 = this.a.requestAudioFocus(this.h);
                } else {
                    AudioManager audioManager = this.a;
                    a aVar = this.f1110b;
                    o oVar2 = this.d;
                    Objects.requireNonNull(oVar2);
                    i2 = audioManager.requestAudioFocus(aVar, e0.t(oVar2.m), this.f);
                }
                if (i2 == 1) {
                    d(1);
                } else {
                    d(0);
                    i3 = -1;
                }
            }
            return i3;
        }
    }
}
