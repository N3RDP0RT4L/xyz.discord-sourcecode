package b.i.a.c;

import android.util.Log;
import b.c.a.a0.d;
import b.i.a.c.a3.o0;
import b.i.a.c.c3.j;
import b.i.a.c.e3.o;
import b.i.a.c.f3.e0;
/* compiled from: DefaultLoadControl.java */
/* loaded from: classes3.dex */
public class z0 implements n1 {
    public final o a;

    /* renamed from: b  reason: collision with root package name */
    public final long f1314b;
    public final long c;
    public final long d;
    public final long e;
    public final int f;
    public final boolean g;
    public final long h;
    public final boolean i;
    public int j;
    public boolean k;

    public z0(o oVar, int i, int i2, int i3, int i4, int i5, boolean z2, int i6, boolean z3) {
        j(i3, 0, "bufferForPlaybackMs", "0");
        j(i4, 0, "bufferForPlaybackAfterRebufferMs", "0");
        j(i, i3, "minBufferMs", "bufferForPlaybackMs");
        j(i, i4, "minBufferMs", "bufferForPlaybackAfterRebufferMs");
        j(i2, i, "maxBufferMs", "minBufferMs");
        j(i6, 0, "backBufferDurationMs", "0");
        this.a = oVar;
        this.f1314b = e0.B(i);
        this.c = e0.B(i2);
        this.d = e0.B(i3);
        this.e = e0.B(i4);
        this.f = i5;
        this.j = i5 == -1 ? 13107200 : i5;
        this.g = z2;
        this.h = e0.B(i6);
        this.i = z3;
    }

    public static void j(int i, int i2, String str, String str2) {
        boolean z2 = i >= i2;
        StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + String.valueOf(str).length() + 21);
        sb.append(str);
        sb.append(" cannot be less than ");
        sb.append(str2);
        d.m(z2, sb.toString());
    }

    @Override // b.i.a.c.n1
    public boolean a() {
        return this.i;
    }

    @Override // b.i.a.c.n1
    public long b() {
        return this.h;
    }

    @Override // b.i.a.c.n1
    public void c() {
        k(false);
    }

    @Override // b.i.a.c.n1
    public void d(f2[] f2VarArr, o0 o0Var, j[] jVarArr) {
        int i = this.f;
        if (i == -1) {
            int i2 = 0;
            int i3 = 0;
            while (true) {
                int i4 = 13107200;
                if (i2 < f2VarArr.length) {
                    if (jVarArr[i2] != null) {
                        switch (f2VarArr[i2].x()) {
                            case -2:
                                i4 = 0;
                                break;
                            case -1:
                            default:
                                throw new IllegalArgumentException();
                            case 0:
                                i4 = 144310272;
                                break;
                            case 1:
                                break;
                            case 2:
                                i4 = 131072000;
                                break;
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                                i4 = 131072;
                                break;
                        }
                        i3 += i4;
                    }
                    i2++;
                } else {
                    i = Math.max(13107200, i3);
                }
            }
        }
        this.j = i;
        this.a.b(i);
    }

    @Override // b.i.a.c.n1
    public void e() {
        k(true);
    }

    @Override // b.i.a.c.n1
    public boolean f(long j, float f, boolean z2, long j2) {
        int i;
        int i2 = e0.a;
        if (f != 1.0f) {
            j = Math.round(j / f);
        }
        long j3 = z2 ? this.e : this.d;
        if (j2 != -9223372036854775807L) {
            j3 = Math.min(j2 / 2, j3);
        }
        if (j3 > 0 && j < j3) {
            if (!this.g) {
                o oVar = this.a;
                synchronized (oVar) {
                    i = oVar.e * oVar.f940b;
                }
                if (i >= this.j) {
                }
            }
            return false;
        }
        return true;
    }

    @Override // b.i.a.c.n1
    public boolean g(long j, long j2, float f) {
        int i;
        o oVar = this.a;
        synchronized (oVar) {
            i = oVar.e * oVar.f940b;
        }
        boolean z2 = true;
        boolean z3 = i >= this.j;
        long j3 = this.f1314b;
        if (f > 1.0f) {
            j3 = Math.min(e0.q(j3, f), this.c);
        }
        if (j2 < Math.max(j3, 500000L)) {
            if (!this.g && z3) {
                z2 = false;
            }
            this.k = z2;
            if (!z2 && j2 < 500000) {
                Log.w("DefaultLoadControl", "Target buffer size reached with less than 500ms of buffered media data.");
            }
        } else if (j2 >= this.c || z3) {
            this.k = false;
        }
        return this.k;
    }

    @Override // b.i.a.c.n1
    public o h() {
        return this.a;
    }

    @Override // b.i.a.c.n1
    public void i() {
        k(true);
    }

    public final void k(boolean z2) {
        int i = this.f;
        if (i == -1) {
            i = 13107200;
        }
        this.j = i;
        this.k = false;
        if (z2) {
            o oVar = this.a;
            synchronized (oVar) {
                if (oVar.a) {
                    oVar.b(0);
                }
            }
        }
    }
}
