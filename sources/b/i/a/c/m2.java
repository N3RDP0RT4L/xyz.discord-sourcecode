package b.i.a.c;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Handler;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.q;
import b.i.a.c.k2;
import b.i.a.c.y1;
import java.util.Iterator;
import org.webrtc.MediaStreamTrack;
/* compiled from: StreamVolumeManager.java */
/* loaded from: classes3.dex */
public final class m2 {
    public final Context a;

    /* renamed from: b  reason: collision with root package name */
    public final Handler f1029b;
    public final b c;
    public final AudioManager d;
    @Nullable
    public c e;
    public int f = 3;
    public int g;
    public boolean h;

    /* compiled from: StreamVolumeManager.java */
    /* loaded from: classes3.dex */
    public interface b {
    }

    /* compiled from: StreamVolumeManager.java */
    /* loaded from: classes3.dex */
    public final class c extends BroadcastReceiver {
        public c(a aVar) {
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            final m2 m2Var = m2.this;
            m2Var.f1029b.post(new Runnable() { // from class: b.i.a.c.p0
                @Override // java.lang.Runnable
                public final void run() {
                    m2.this.d();
                }
            });
        }
    }

    public m2(Context context, Handler handler, b bVar) {
        Context applicationContext = context.getApplicationContext();
        this.a = applicationContext;
        this.f1029b = handler;
        this.c = bVar;
        AudioManager audioManager = (AudioManager) applicationContext.getSystemService(MediaStreamTrack.AUDIO_TRACK_KIND);
        d.H(audioManager);
        this.d = audioManager;
        this.g = b(audioManager, 3);
        this.h = a(audioManager, this.f);
        c cVar = new c(null);
        try {
            applicationContext.registerReceiver(cVar, new IntentFilter("android.media.VOLUME_CHANGED_ACTION"));
            this.e = cVar;
        } catch (RuntimeException e) {
            q.c("StreamVolumeManager", "Error registering stream volume receiver", e);
        }
    }

    public static boolean a(AudioManager audioManager, int i) {
        if (e0.a >= 23) {
            return audioManager.isStreamMute(i);
        }
        return b(audioManager, i) == 0;
    }

    public static int b(AudioManager audioManager, int i) {
        try {
            return audioManager.getStreamVolume(i);
        } catch (RuntimeException e) {
            StringBuilder sb = new StringBuilder(60);
            sb.append("Could not retrieve stream volume for stream type ");
            sb.append(i);
            q.c("StreamVolumeManager", sb.toString(), e);
            return audioManager.getStreamMaxVolume(i);
        }
    }

    public void c(int i) {
        if (this.f != i) {
            this.f = i;
            d();
            k2.b bVar = (k2.b) this.c;
            c1 f02 = k2.f0(k2.this.l);
            if (!f02.equals(k2.this.H)) {
                k2 k2Var = k2.this;
                k2Var.H = f02;
                Iterator<y1.e> it = k2Var.h.iterator();
                while (it.hasNext()) {
                    it.next().z(f02);
                }
            }
        }
    }

    public final void d() {
        int b2 = b(this.d, this.f);
        boolean a2 = a(this.d, this.f);
        if (this.g != b2 || this.h != a2) {
            this.g = b2;
            this.h = a2;
            Iterator<y1.e> it = k2.this.h.iterator();
            while (it.hasNext()) {
                it.next().G(b2, a2);
            }
        }
    }
}
