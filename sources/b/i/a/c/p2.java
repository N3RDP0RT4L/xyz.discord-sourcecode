package b.i.a.c;

import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.a3.n0;
import b.i.b.b.h0;
import b.i.b.b.p;
import java.util.Arrays;
import java.util.List;
/* compiled from: TracksInfo.java */
/* loaded from: classes3.dex */
public final class p2 implements w0 {
    public static final p2 j = new p2(h0.l);
    public final p<a> k;

    /* compiled from: TracksInfo.java */
    /* loaded from: classes3.dex */
    public static final class a implements w0 {
        public final n0 j;
        public final int[] k;
        public final int l;
        public final boolean[] m;

        public a(n0 n0Var, int[] iArr, int i, boolean[] zArr) {
            int i2 = n0Var.k;
            d.j(i2 == iArr.length && i2 == zArr.length);
            this.j = n0Var;
            this.k = (int[]) iArr.clone();
            this.l = i;
            this.m = (boolean[]) zArr.clone();
        }

        public boolean equals(@Nullable Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || a.class != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            return this.l == aVar.l && this.j.equals(aVar.j) && Arrays.equals(this.k, aVar.k) && Arrays.equals(this.m, aVar.m);
        }

        public int hashCode() {
            int hashCode = Arrays.hashCode(this.k);
            return Arrays.hashCode(this.m) + ((((hashCode + (this.j.hashCode() * 31)) * 31) + this.l) * 31);
        }
    }

    static {
        b.i.b.b.a<Object> aVar = p.k;
    }

    public p2(List<a> list) {
        this.k = p.n(list);
    }

    public boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || p2.class != obj.getClass()) {
            return false;
        }
        return this.k.equals(((p2) obj).k);
    }

    public int hashCode() {
        return this.k.hashCode();
    }
}
