package b.i.a.c;

import android.util.Pair;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.a3.a0;
import b.i.a.c.a3.i0;
import b.i.a.c.a3.n;
import b.i.a.c.a3.o0;
import b.i.a.c.a3.x;
import b.i.a.c.c3.j;
import b.i.a.c.c3.q;
import b.i.a.c.c3.r;
import b.i.a.c.e3.o;
import b.i.a.c.u1;
import com.google.android.exoplayer2.ExoPlaybackException;
import java.util.Objects;
/* compiled from: MediaPeriodHolder.java */
/* loaded from: classes3.dex */
public final class q1 {
    public final x a;

    /* renamed from: b  reason: collision with root package name */
    public final Object f1052b;
    public final i0[] c;
    public boolean d;
    public boolean e;
    public r1 f;
    public boolean g;
    public final boolean[] h;
    public final g2[] i;
    public final q j;
    public final u1 k;
    @Nullable
    public q1 l;
    public o0 m = o0.j;
    public r n;
    public long o;

    public q1(g2[] g2VarArr, long j, q qVar, o oVar, u1 u1Var, r1 r1Var, r rVar) {
        this.i = g2VarArr;
        this.o = j;
        this.j = qVar;
        this.k = u1Var;
        a0.a aVar = r1Var.a;
        this.f1052b = aVar.a;
        this.f = r1Var;
        this.n = rVar;
        this.c = new i0[g2VarArr.length];
        this.h = new boolean[g2VarArr.length];
        long j2 = r1Var.f1054b;
        long j3 = r1Var.d;
        Objects.requireNonNull(u1Var);
        Pair pair = (Pair) aVar.a;
        Object obj = pair.first;
        a0.a b2 = aVar.b(pair.second);
        u1.c cVar = u1Var.c.get(obj);
        Objects.requireNonNull(cVar);
        u1Var.h.add(cVar);
        u1.b bVar = u1Var.g.get(cVar);
        if (bVar != null) {
            bVar.a.m(bVar.f1134b);
        }
        cVar.c.add(b2);
        x u = cVar.a.n(b2, oVar, j2);
        u1Var.f1133b.put(u, cVar);
        u1Var.d();
        this.a = j3 != -9223372036854775807L ? new n(u, true, 0L, j3) : u;
    }

    public long a(r rVar, long j, boolean z2, boolean[] zArr) {
        int i = 0;
        while (true) {
            boolean z3 = true;
            if (i >= rVar.a) {
                break;
            }
            boolean[] zArr2 = this.h;
            if (z2 || !rVar.a(this.n, i)) {
                z3 = false;
            }
            zArr2[i] = z3;
            i++;
        }
        i0[] i0VarArr = this.c;
        int i2 = 0;
        while (true) {
            g2[] g2VarArr = this.i;
            if (i2 >= g2VarArr.length) {
                break;
            }
            if (((v0) g2VarArr[i2]).j == -2) {
                i0VarArr[i2] = null;
            }
            i2++;
        }
        b();
        this.n = rVar;
        c();
        long m = this.a.m(rVar.c, this.h, this.c, zArr, j);
        i0[] i0VarArr2 = this.c;
        int i3 = 0;
        while (true) {
            g2[] g2VarArr2 = this.i;
            if (i3 >= g2VarArr2.length) {
                break;
            }
            if (((v0) g2VarArr2[i3]).j == -2 && this.n.b(i3)) {
                i0VarArr2[i3] = new b.i.a.c.a3.q();
            }
            i3++;
        }
        this.e = false;
        int i4 = 0;
        while (true) {
            i0[] i0VarArr3 = this.c;
            if (i4 >= i0VarArr3.length) {
                return m;
            }
            if (i0VarArr3[i4] != null) {
                d.D(rVar.b(i4));
                if (((v0) this.i[i4]).j != -2) {
                    this.e = true;
                }
            } else {
                d.D(rVar.c[i4] == null);
            }
            i4++;
        }
    }

    public final void b() {
        if (g()) {
            int i = 0;
            while (true) {
                r rVar = this.n;
                if (i < rVar.a) {
                    boolean b2 = rVar.b(i);
                    j jVar = this.n.c[i];
                    if (b2 && jVar != null) {
                        jVar.g();
                    }
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    public final void c() {
        if (g()) {
            int i = 0;
            while (true) {
                r rVar = this.n;
                if (i < rVar.a) {
                    boolean b2 = rVar.b(i);
                    j jVar = this.n.c[i];
                    if (b2 && jVar != null) {
                        jVar.e();
                    }
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    public long d() {
        if (!this.d) {
            return this.f.f1054b;
        }
        long q = this.e ? this.a.q() : Long.MIN_VALUE;
        return q == Long.MIN_VALUE ? this.f.e : q;
    }

    public long e() {
        return this.f.f1054b + this.o;
    }

    public boolean f() {
        return this.d && (!this.e || this.a.q() == Long.MIN_VALUE);
    }

    public final boolean g() {
        return this.l == null;
    }

    public void h() {
        b();
        u1 u1Var = this.k;
        x xVar = this.a;
        try {
            if (xVar instanceof n) {
                u1Var.h(((n) xVar).j);
            } else {
                u1Var.h(xVar);
            }
        } catch (RuntimeException e) {
            b.i.a.c.f3.q.b("MediaPeriodHolder", "Period release failed.", e);
        }
    }

    public r i(float f, o2 o2Var) throws ExoPlaybackException {
        j[] jVarArr;
        r b2 = this.j.b(this.i, this.m, this.f.a, o2Var);
        for (j jVar : b2.c) {
            if (jVar != null) {
                jVar.i(f);
            }
        }
        return b2;
    }

    public void j() {
        x xVar = this.a;
        if (xVar instanceof n) {
            long j = this.f.d;
            if (j == -9223372036854775807L) {
                j = Long.MIN_VALUE;
            }
            n nVar = (n) xVar;
            nVar.n = 0L;
            nVar.o = j;
        }
    }
}
