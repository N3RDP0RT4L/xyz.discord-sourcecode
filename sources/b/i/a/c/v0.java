package b.i.a.c;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import b.c.a.a0.d;
import b.i.a.c.a3.i0;
import b.i.a.c.f3.s;
import b.i.a.c.j1;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import java.io.IOException;
import java.util.Objects;
/* compiled from: BaseRenderer.java */
/* loaded from: classes3.dex */
public abstract class v0 implements f2, g2 {
    public final int j;
    @Nullable
    public h2 l;
    public int m;
    public int n;
    @Nullable
    public i0 o;
    @Nullable
    public j1[] p;
    public long q;

    /* renamed from: s  reason: collision with root package name */
    public boolean f1136s;
    public boolean t;
    public final k1 k = new k1();
    public long r = Long.MIN_VALUE;

    public v0(int i) {
        this.j = i;
    }

    public final k1 A() {
        this.k.a();
        return this.k;
    }

    public abstract void B();

    public void C(boolean z2, boolean z3) throws ExoPlaybackException {
    }

    public abstract void D(long j, boolean z2) throws ExoPlaybackException;

    public void E() {
    }

    public void F() throws ExoPlaybackException {
    }

    public void G() {
    }

    public abstract void H(j1[] j1VarArr, long j, long j2) throws ExoPlaybackException;

    public final int I(k1 k1Var, DecoderInputBuffer decoderInputBuffer, int i) {
        i0 i0Var = this.o;
        Objects.requireNonNull(i0Var);
        int a = i0Var.a(k1Var, decoderInputBuffer, i);
        if (a == -4) {
            if (decoderInputBuffer.n()) {
                this.r = Long.MIN_VALUE;
                return this.f1136s ? -4 : -3;
            }
            long j = decoderInputBuffer.n + this.q;
            decoderInputBuffer.n = j;
            this.r = Math.max(this.r, j);
        } else if (a == -5) {
            j1 j1Var = k1Var.f1023b;
            Objects.requireNonNull(j1Var);
            if (j1Var.A != RecyclerView.FOREVER_NS) {
                j1.b a2 = j1Var.a();
                a2.o = j1Var.A + this.q;
                k1Var.f1023b = a2.a();
            }
        }
        return a;
    }

    @Override // b.i.a.c.f2
    public final void f(int i) {
        this.m = i;
    }

    @Override // b.i.a.c.f2
    public final void g() {
        boolean z2 = true;
        if (this.n != 1) {
            z2 = false;
        }
        d.D(z2);
        this.k.a();
        this.n = 0;
        this.o = null;
        this.p = null;
        this.f1136s = false;
        B();
    }

    @Override // b.i.a.c.f2
    public final int getState() {
        return this.n;
    }

    @Override // b.i.a.c.f2
    @Nullable
    public final i0 h() {
        return this.o;
    }

    @Override // b.i.a.c.f2
    public final boolean j() {
        return this.r == Long.MIN_VALUE;
    }

    @Override // b.i.a.c.f2
    public final void k(j1[] j1VarArr, i0 i0Var, long j, long j2) throws ExoPlaybackException {
        d.D(!this.f1136s);
        this.o = i0Var;
        if (this.r == Long.MIN_VALUE) {
            this.r = j;
        }
        this.p = j1VarArr;
        this.q = j2;
        H(j1VarArr, j, j2);
    }

    @Override // b.i.a.c.f2
    public final void l() {
        this.f1136s = true;
    }

    @Override // b.i.a.c.f2
    public final g2 m() {
        return this;
    }

    @Override // b.i.a.c.f2
    public /* synthetic */ void n(float f, float f2) {
        e2.a(this, f, f2);
    }

    @Override // b.i.a.c.f2
    public final void o(h2 h2Var, j1[] j1VarArr, i0 i0Var, long j, boolean z2, boolean z3, long j2, long j3) throws ExoPlaybackException {
        d.D(this.n == 0);
        this.l = h2Var;
        this.n = 1;
        C(z2, z3);
        k(j1VarArr, i0Var, j2, j3);
        D(j, z2);
    }

    @Override // b.i.a.c.g2
    public int p() throws ExoPlaybackException {
        return 0;
    }

    @Override // b.i.a.c.b2.b
    public void r(int i, @Nullable Object obj) throws ExoPlaybackException {
    }

    @Override // b.i.a.c.f2
    public final void reset() {
        d.D(this.n == 0);
        this.k.a();
        E();
    }

    @Override // b.i.a.c.f2
    public final void s() throws IOException {
        i0 i0Var = this.o;
        Objects.requireNonNull(i0Var);
        i0Var.b();
    }

    @Override // b.i.a.c.f2
    public final void start() throws ExoPlaybackException {
        boolean z2 = true;
        if (this.n != 1) {
            z2 = false;
        }
        d.D(z2);
        this.n = 2;
        F();
    }

    @Override // b.i.a.c.f2
    public final void stop() {
        d.D(this.n == 2);
        this.n = 1;
        G();
    }

    @Override // b.i.a.c.f2
    public final long t() {
        return this.r;
    }

    @Override // b.i.a.c.f2
    public final void u(long j) throws ExoPlaybackException {
        this.f1136s = false;
        this.r = j;
        D(j, false);
    }

    @Override // b.i.a.c.f2
    public final boolean v() {
        return this.f1136s;
    }

    @Override // b.i.a.c.f2
    @Nullable
    public s w() {
        return null;
    }

    @Override // b.i.a.c.f2
    public final int x() {
        return this.j;
    }

    public final ExoPlaybackException y(Throwable th, @Nullable j1 j1Var, int i) {
        return z(th, j1Var, false, i);
    }

    /* JADX WARN: Removed duplicated region for block: B:16:0x0028  */
    /* JADX WARN: Removed duplicated region for block: B:17:0x002a  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final com.google.android.exoplayer2.ExoPlaybackException z(java.lang.Throwable r14, @androidx.annotation.Nullable b.i.a.c.j1 r15, boolean r16, int r17) {
        /*
            r13 = this;
            r1 = r13
            r0 = r15
            r2 = 4
            if (r0 == 0) goto L1d
            boolean r3 = r1.t
            if (r3 != 0) goto L1d
            r3 = 1
            r1.t = r3
            r3 = 0
            int r4 = r13.a(r15)     // Catch: java.lang.Throwable -> L16 com.google.android.exoplayer2.ExoPlaybackException -> L1b
            r4 = r4 & 7
            r1.t = r3
            goto L1e
        L16:
            r0 = move-exception
            r2 = r0
            r1.t = r3
            throw r2
        L1b:
            r1.t = r3
        L1d:
            r4 = 4
        L1e:
            java.lang.String r7 = r13.getName()
            int r8 = r1.m
            com.google.android.exoplayer2.ExoPlaybackException r12 = new com.google.android.exoplayer2.ExoPlaybackException
            if (r0 != 0) goto L2a
            r10 = 4
            goto L2b
        L2a:
            r10 = r4
        L2b:
            r3 = 1
            r5 = 0
            r2 = r12
            r4 = r14
            r6 = r17
            r9 = r15
            r11 = r16
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11)
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.v0.z(java.lang.Throwable, b.i.a.c.j1, boolean, int):com.google.android.exoplayer2.ExoPlaybackException");
    }
}
