package b.i.a.c;

import android.os.Handler;
import android.util.Pair;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import b.c.a.a0.d;
import b.i.a.c.a3.a0;
import b.i.a.c.a3.p0.c;
import b.i.a.c.o2;
import b.i.a.c.s2.g1;
import b.i.b.b.a;
import b.i.b.b.p;
import java.util.Objects;
/* compiled from: MediaPeriodQueue.java */
/* loaded from: classes3.dex */
public final class s1 {
    public final o2.b a = new o2.b();

    /* renamed from: b  reason: collision with root package name */
    public final o2.c f1058b = new o2.c();
    @Nullable
    public final g1 c;
    public final Handler d;
    public long e;
    public int f;
    public boolean g;
    @Nullable
    public q1 h;
    @Nullable
    public q1 i;
    @Nullable
    public q1 j;
    public int k;
    @Nullable
    public Object l;
    public long m;

    public s1(@Nullable g1 g1Var, Handler handler) {
        this.c = g1Var;
        this.d = handler;
    }

    public static a0.a p(o2 o2Var, Object obj, long j, long j2, o2.b bVar) {
        o2Var.h(obj, bVar);
        c cVar = bVar.p;
        long j3 = bVar.m;
        int i = cVar.n - 1;
        while (i >= 0) {
            boolean z2 = false;
            if (j != Long.MIN_VALUE) {
                long j4 = cVar.a(i).k;
                if (j4 != Long.MIN_VALUE ? j < j4 : !(j3 != -9223372036854775807L && j >= j3)) {
                    z2 = true;
                }
            }
            if (!z2) {
                break;
            }
            i--;
        }
        if (i < 0 || !cVar.a(i).b()) {
            i = -1;
        }
        if (i == -1) {
            return new a0.a(obj, j2, bVar.b(j));
        }
        return new a0.a(obj, i, bVar.d(i), j2);
    }

    @Nullable
    public q1 a() {
        q1 q1Var = this.h;
        if (q1Var == null) {
            return null;
        }
        if (q1Var == this.i) {
            this.i = q1Var.l;
        }
        q1Var.h();
        int i = this.k - 1;
        this.k = i;
        if (i == 0) {
            this.j = null;
            q1 q1Var2 = this.h;
            this.l = q1Var2.f1052b;
            this.m = q1Var2.f.a.d;
        }
        this.h = this.h.l;
        l();
        return this.h;
    }

    public void b() {
        if (this.k != 0) {
            q1 q1Var = this.h;
            d.H(q1Var);
            q1 q1Var2 = q1Var;
            this.l = q1Var2.f1052b;
            this.m = q1Var2.f.a.d;
            while (q1Var2 != null) {
                q1Var2.h();
                q1Var2 = q1Var2.l;
            }
            this.h = null;
            this.j = null;
            this.i = null;
            this.k = 0;
            l();
        }
    }

    @Nullable
    public final r1 c(o2 o2Var, q1 q1Var, long j) {
        long j2;
        r1 r1Var = q1Var.f;
        long j3 = (q1Var.o + r1Var.e) - j;
        long j4 = 0;
        if (r1Var.g) {
            int d = o2Var.d(o2Var.b(r1Var.a.a), this.a, this.f1058b, this.f, this.g);
            if (d == -1) {
                return null;
            }
            int i = o2Var.g(d, this.a, true).l;
            Object obj = this.a.k;
            long j5 = r1Var.a.d;
            if (o2Var.n(i, this.f1058b).B == d) {
                Pair<Object, Long> k = o2Var.k(this.f1058b, this.a, i, -9223372036854775807L, Math.max(0L, j3));
                if (k == null) {
                    return null;
                }
                obj = k.first;
                j2 = ((Long) k.second).longValue();
                q1 q1Var2 = q1Var.l;
                if (q1Var2 == null || !q1Var2.f1052b.equals(obj)) {
                    j5 = this.e;
                    this.e = 1 + j5;
                } else {
                    j5 = q1Var2.f.a.d;
                }
                j4 = -9223372036854775807L;
            } else {
                j2 = 0;
            }
            return d(o2Var, p(o2Var, obj, j2, j5, this.a), j4, j2);
        }
        a0.a aVar = r1Var.a;
        o2Var.h(aVar.a, this.a);
        if (aVar.a()) {
            int i2 = aVar.f831b;
            int i3 = this.a.p.a(i2).l;
            if (i3 == -1) {
                return null;
            }
            int a = this.a.p.a(i2).a(aVar.c);
            if (a < i3) {
                return e(o2Var, aVar.a, i2, a, r1Var.c, aVar.d);
            }
            long j6 = r1Var.c;
            if (j6 == -9223372036854775807L) {
                o2.c cVar = this.f1058b;
                o2.b bVar = this.a;
                Pair<Object, Long> k2 = o2Var.k(cVar, bVar, bVar.l, -9223372036854775807L, Math.max(0L, j3));
                if (k2 == null) {
                    return null;
                }
                j6 = ((Long) k2.second).longValue();
            }
            return f(o2Var, aVar.a, Math.max(g(o2Var, aVar.a, aVar.f831b), j6), r1Var.c, aVar.d);
        }
        int d2 = this.a.d(aVar.e);
        if (d2 != this.a.p.a(aVar.e).l) {
            return e(o2Var, aVar.a, aVar.e, d2, r1Var.e, aVar.d);
        }
        return f(o2Var, aVar.a, g(o2Var, aVar.a, aVar.e), r1Var.e, aVar.d);
    }

    @Nullable
    public final r1 d(o2 o2Var, a0.a aVar, long j, long j2) {
        o2Var.h(aVar.a, this.a);
        return aVar.a() ? e(o2Var, aVar.a, aVar.f831b, aVar.c, j, aVar.d) : f(o2Var, aVar.a, j2, j, aVar.d);
    }

    public final r1 e(o2 o2Var, Object obj, int i, int i2, long j, long j2) {
        a0.a aVar = new a0.a(obj, i, i2, j2);
        long a = o2Var.h(obj, this.a).a(i, i2);
        long j3 = i2 == this.a.p.a(i).a(-1) ? this.a.p.o : 0L;
        return new r1(aVar, (a == -9223372036854775807L || j3 < a) ? j3 : Math.max(0L, a - 1), j, -9223372036854775807L, a, this.a.p.a(i).q, false, false, false);
    }

    public final r1 f(o2 o2Var, Object obj, long j, long j2, long j3) {
        long j4 = j;
        o2Var.h(obj, this.a);
        int b2 = this.a.b(j4);
        a0.a aVar = new a0.a(obj, j3, b2);
        boolean i = i(aVar);
        boolean k = k(o2Var, aVar);
        boolean j5 = j(o2Var, aVar, i);
        boolean z2 = b2 != -1 && this.a.e(b2);
        long c = b2 != -1 ? this.a.c(b2) : -9223372036854775807L;
        long j6 = (c == -9223372036854775807L || c == Long.MIN_VALUE) ? this.a.m : c;
        if (j6 != -9223372036854775807L && j4 >= j6) {
            j4 = Math.max(0L, j6 - 1);
        }
        return new r1(aVar, j4, j2, c, j6, z2, i, k, j5);
    }

    public final long g(o2 o2Var, Object obj, int i) {
        o2Var.h(obj, this.a);
        long j = this.a.p.a(i).k;
        if (j == Long.MIN_VALUE) {
            return this.a.m;
        }
        return j + this.a.p.a(i).p;
    }

    /* JADX WARN: Removed duplicated region for block: B:20:0x0060  */
    /* JADX WARN: Removed duplicated region for block: B:21:0x006a  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.i.a.c.r1 h(b.i.a.c.o2 r19, b.i.a.c.r1 r20) {
        /*
            r18 = this;
            r0 = r18
            r1 = r19
            r2 = r20
            b.i.a.c.a3.a0$a r3 = r2.a
            boolean r12 = r0.i(r3)
            boolean r13 = r0.k(r1, r3)
            boolean r14 = r0.j(r1, r3, r12)
            b.i.a.c.a3.a0$a r4 = r2.a
            java.lang.Object r4 = r4.a
            b.i.a.c.o2$b r5 = r0.a
            r1.h(r4, r5)
            boolean r1 = r3.a()
            r4 = -1
            r5 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            if (r1 != 0) goto L35
            int r1 = r3.e
            if (r1 != r4) goto L2e
            goto L35
        L2e:
            b.i.a.c.o2$b r7 = r0.a
            long r7 = r7.c(r1)
            goto L36
        L35:
            r7 = r5
        L36:
            boolean r1 = r3.a()
            if (r1 == 0) goto L48
            b.i.a.c.o2$b r1 = r0.a
            int r5 = r3.f831b
            int r6 = r3.c
            long r5 = r1.a(r5, r6)
        L46:
            r9 = r5
            goto L5a
        L48:
            int r1 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r1 == 0) goto L55
            r5 = -9223372036854775808
            int r1 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r1 != 0) goto L53
            goto L55
        L53:
            r9 = r7
            goto L5a
        L55:
            b.i.a.c.o2$b r1 = r0.a
            long r5 = r1.m
            goto L46
        L5a:
            boolean r1 = r3.a()
            if (r1 == 0) goto L6a
            b.i.a.c.o2$b r1 = r0.a
            int r4 = r3.f831b
            boolean r1 = r1.e(r4)
            r11 = r1
            goto L7b
        L6a:
            int r1 = r3.e
            if (r1 == r4) goto L79
            b.i.a.c.o2$b r4 = r0.a
            boolean r1 = r4.e(r1)
            if (r1 == 0) goto L79
            r1 = 1
            r11 = 1
            goto L7b
        L79:
            r1 = 0
            r11 = 0
        L7b:
            b.i.a.c.r1 r15 = new b.i.a.c.r1
            long r4 = r2.f1054b
            long r1 = r2.c
            r16 = r1
            r1 = r15
            r2 = r3
            r3 = r4
            r5 = r16
            r1.<init>(r2, r3, r5, r7, r9, r11, r12, r13, r14)
            return r15
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.s1.h(b.i.a.c.o2, b.i.a.c.r1):b.i.a.c.r1");
    }

    public final boolean i(a0.a aVar) {
        return !aVar.a() && aVar.e == -1;
    }

    public final boolean j(o2 o2Var, a0.a aVar, boolean z2) {
        int b2 = o2Var.b(aVar.a);
        if (o2Var.n(o2Var.f(b2, this.a).l, this.f1058b).v) {
            return false;
        }
        return (o2Var.d(b2, this.a, this.f1058b, this.f, this.g) == -1) && z2;
    }

    public final boolean k(o2 o2Var, a0.a aVar) {
        if (!i(aVar)) {
            return false;
        }
        return o2Var.n(o2Var.h(aVar.a, this.a).l, this.f1058b).C == o2Var.b(aVar.a);
    }

    public final void l() {
        if (this.c != null) {
            a<Object> aVar = p.k;
            final p.a aVar2 = new p.a();
            for (q1 q1Var = this.h; q1Var != null; q1Var = q1Var.l) {
                aVar2.b(q1Var.f.a);
            }
            q1 q1Var2 = this.i;
            final a0.a aVar3 = q1Var2 == null ? null : q1Var2.f.a;
            this.d.post(new Runnable() { // from class: b.i.a.c.m0
                @Override // java.lang.Runnable
                public final void run() {
                    s1 s1Var = s1.this;
                    p.a aVar4 = aVar2;
                    a0.a aVar5 = aVar3;
                    g1 g1Var = s1Var.c;
                    p c = aVar4.c();
                    g1.a aVar6 = g1Var.m;
                    y1 y1Var = g1Var.p;
                    Objects.requireNonNull(y1Var);
                    Objects.requireNonNull(aVar6);
                    aVar6.f1077b = p.n(c);
                    if (!c.isEmpty()) {
                        aVar6.e = (a0.a) c.get(0);
                        Objects.requireNonNull(aVar5);
                        aVar6.f = aVar5;
                    }
                    if (aVar6.d == null) {
                        aVar6.d = g1.a.b(y1Var, aVar6.f1077b, aVar6.e, aVar6.a);
                    }
                    aVar6.d(y1Var.K());
                }
            });
        }
    }

    public void m(long j) {
        q1 q1Var = this.j;
        if (q1Var != null) {
            d.D(q1Var.g());
            if (q1Var.d) {
                q1Var.a.s(j - q1Var.o);
            }
        }
    }

    public boolean n(q1 q1Var) {
        boolean z2 = false;
        d.D(q1Var != null);
        if (q1Var.equals(this.j)) {
            return false;
        }
        this.j = q1Var;
        while (true) {
            q1Var = q1Var.l;
            if (q1Var == null) {
                break;
            }
            if (q1Var == this.i) {
                this.i = this.h;
                z2 = true;
            }
            q1Var.h();
            this.k--;
        }
        q1 q1Var2 = this.j;
        if (q1Var2.l != null) {
            q1Var2.b();
            q1Var2.l = null;
            q1Var2.c();
        }
        l();
        return z2;
    }

    public a0.a o(o2 o2Var, Object obj, long j) {
        long j2;
        int b2;
        int i = o2Var.h(obj, this.a).l;
        Object obj2 = this.l;
        if (obj2 == null || (b2 = o2Var.b(obj2)) == -1 || o2Var.f(b2, this.a).l != i) {
            q1 q1Var = this.h;
            while (true) {
                if (q1Var == null) {
                    q1 q1Var2 = this.h;
                    while (true) {
                        if (q1Var2 != null) {
                            int b3 = o2Var.b(q1Var2.f1052b);
                            if (b3 != -1 && o2Var.f(b3, this.a).l == i) {
                                j2 = q1Var2.f.a.d;
                                break;
                            }
                            q1Var2 = q1Var2.l;
                        } else {
                            j2 = this.e;
                            this.e = 1 + j2;
                            if (this.h == null) {
                                this.l = obj;
                                this.m = j2;
                            }
                        }
                    }
                } else if (q1Var.f1052b.equals(obj)) {
                    j2 = q1Var.f.a.d;
                    break;
                } else {
                    q1Var = q1Var.l;
                }
            }
        } else {
            j2 = this.m;
        }
        return p(o2Var, obj, j, j2, this.a);
    }

    public final boolean q(o2 o2Var) {
        q1 q1Var;
        q1 q1Var2 = this.h;
        if (q1Var2 == null) {
            return true;
        }
        int b2 = o2Var.b(q1Var2.f1052b);
        while (true) {
            b2 = o2Var.d(b2, this.a, this.f1058b, this.f, this.g);
            while (true) {
                q1Var = q1Var2.l;
                if (q1Var == null || q1Var2.f.g) {
                    break;
                }
                q1Var2 = q1Var;
            }
            if (b2 == -1 || q1Var == null || o2Var.b(q1Var.f1052b) != b2) {
                break;
            }
            q1Var2 = q1Var;
        }
        boolean n = n(q1Var2);
        q1Var2.f = h(o2Var, q1Var2.f);
        return !n;
    }

    public boolean r(o2 o2Var, long j, long j2) {
        r1 r1Var;
        boolean n;
        q1 q1Var = this.h;
        q1 q1Var2 = null;
        while (q1Var != null) {
            r1 r1Var2 = q1Var.f;
            if (q1Var2 == null) {
                r1Var = h(o2Var, r1Var2);
            } else {
                r1 c = c(o2Var, q1Var2, j);
                if (c == null) {
                    n = n(q1Var2);
                } else {
                    if (!(r1Var2.f1054b == c.f1054b && r1Var2.a.equals(c.a))) {
                        n = n(q1Var2);
                    } else {
                        r1Var = c;
                    }
                }
                return !n;
            }
            q1Var.f = r1Var.a(r1Var2.c);
            long j3 = r1Var2.e;
            if (!(j3 == -9223372036854775807L || j3 == r1Var.e)) {
                q1Var.j();
                long j4 = r1Var.e;
                return !n(q1Var) && !(q1Var == this.i && !q1Var.f.f && ((j2 > Long.MIN_VALUE ? 1 : (j2 == Long.MIN_VALUE ? 0 : -1)) == 0 || (j2 > ((j4 > (-9223372036854775807L) ? 1 : (j4 == (-9223372036854775807L) ? 0 : -1)) == 0 ? RecyclerView.FOREVER_NS : j4 + q1Var.o) ? 1 : (j2 == ((j4 > (-9223372036854775807L) ? 1 : (j4 == (-9223372036854775807L) ? 0 : -1)) == 0 ? RecyclerView.FOREVER_NS : j4 + q1Var.o) ? 0 : -1)) >= 0));
            }
            q1Var2 = q1Var;
            q1Var = q1Var.l;
        }
        return true;
    }
}
