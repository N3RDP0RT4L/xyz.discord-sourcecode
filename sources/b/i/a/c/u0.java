package b.i.a.c;

import b.i.a.c.o2;
/* compiled from: BasePlayer.java */
/* loaded from: classes3.dex */
public abstract class u0 implements y1 {
    public final o2.c a = new o2.c();

    @Override // b.i.a.c.y1
    public final boolean D(int i) {
        return i().k.a.get(i);
    }

    @Override // b.i.a.c.y1
    public final void O() {
        if (!K().q() && !f()) {
            if (W()) {
                int b2 = b();
                if (b2 != -1) {
                    h(b2, -9223372036854775807L);
                }
            } else if (Z() && Y()) {
                h(C(), -9223372036854775807L);
            }
        }
    }

    @Override // b.i.a.c.y1
    public final void P() {
        c0(v());
    }

    @Override // b.i.a.c.y1
    public final void R() {
        c0(-U());
    }

    public final int V() {
        o2 K = K();
        if (K.q()) {
            return -1;
        }
        int C = C();
        int I = I();
        if (I == 1) {
            I = 0;
        }
        return K.l(C, I, M());
    }

    public final boolean W() {
        return b() != -1;
    }

    public final boolean X() {
        return V() != -1;
    }

    public final boolean Y() {
        o2 K = K();
        return !K.q() && K.n(C(), this.a).v;
    }

    public final boolean Z() {
        o2 K = K();
        return !K.q() && K.n(C(), this.a).c();
    }

    public final boolean a0() {
        o2 K = K();
        return !K.q() && K.n(C(), this.a).u;
    }

    public final int b() {
        o2 K = K();
        if (K.q()) {
            return -1;
        }
        int C = C();
        int I = I();
        if (I == 1) {
            I = 0;
        }
        return K.e(C, I, M());
    }

    public final void b0(long j) {
        h(C(), j);
    }

    public final void c0(long j) {
        long T = T() + j;
        long J = J();
        if (J != -9223372036854775807L) {
            T = Math.min(T, J);
        }
        b0(Math.max(T, 0L));
    }

    @Override // b.i.a.c.y1
    public final void d() {
        u(false);
    }

    @Override // b.i.a.c.y1
    public final void e() {
        u(true);
    }

    @Override // b.i.a.c.y1
    public final void s() {
        int V;
        if (!K().q() && !f()) {
            boolean X = X();
            if (!Z() || a0()) {
                if (!X || T() > l()) {
                    b0(0L);
                    return;
                }
                int V2 = V();
                if (V2 != -1) {
                    h(V2, -9223372036854775807L);
                }
            } else if (X && (V = V()) != -1) {
                h(V, -9223372036854775807L);
            }
        }
    }

    @Override // b.i.a.c.y1
    public final boolean z() {
        return y() == 3 && j() && G() == 0;
    }
}
