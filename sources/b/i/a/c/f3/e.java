package b.i.a.c.f3;

import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
/* compiled from: AtomicFile.java */
/* loaded from: classes3.dex */
public final class e {
    public final File a;

    /* renamed from: b  reason: collision with root package name */
    public final File f963b;

    /* compiled from: AtomicFile.java */
    /* loaded from: classes3.dex */
    public static final class a extends OutputStream {
        public final FileOutputStream j;
        public boolean k = false;

        public a(File file) throws FileNotFoundException {
            this.j = new FileOutputStream(file);
        }

        @Override // java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
            if (!this.k) {
                this.k = true;
                this.j.flush();
                try {
                    this.j.getFD().sync();
                } catch (IOException e) {
                    q.c("AtomicFile", "Failed to sync file descriptor:", e);
                }
                this.j.close();
            }
        }

        @Override // java.io.OutputStream, java.io.Flushable
        public void flush() throws IOException {
            this.j.flush();
        }

        @Override // java.io.OutputStream
        public void write(int i) throws IOException {
            this.j.write(i);
        }

        @Override // java.io.OutputStream
        public void write(byte[] bArr) throws IOException {
            this.j.write(bArr);
        }

        @Override // java.io.OutputStream
        public void write(byte[] bArr, int i, int i2) throws IOException {
            this.j.write(bArr, i, i2);
        }
    }

    public e(File file) {
        this.a = file;
        this.f963b = new File(String.valueOf(file.getPath()).concat(".bak"));
    }

    public boolean a() {
        return this.a.exists() || this.f963b.exists();
    }

    public InputStream b() throws FileNotFoundException {
        if (this.f963b.exists()) {
            this.a.delete();
            this.f963b.renameTo(this.a);
        }
        return new FileInputStream(this.a);
    }

    public OutputStream c() throws IOException {
        if (this.a.exists()) {
            if (this.f963b.exists()) {
                this.a.delete();
            } else if (!this.a.renameTo(this.f963b)) {
                String valueOf = String.valueOf(this.a);
                String valueOf2 = String.valueOf(this.f963b);
                Log.w("AtomicFile", b.d.b.a.a.k(valueOf2.length() + valueOf.length() + 37, "Couldn't rename file ", valueOf, " to backup file ", valueOf2));
            }
        }
        try {
            return new a(this.a);
        } catch (FileNotFoundException e) {
            File parentFile = this.a.getParentFile();
            if (parentFile == null || !parentFile.mkdirs()) {
                String valueOf3 = String.valueOf(this.a);
                throw new IOException(b.d.b.a.a.i(valueOf3.length() + 16, "Couldn't create ", valueOf3), e);
            }
            try {
                return new a(this.a);
            } catch (FileNotFoundException e2) {
                String valueOf4 = String.valueOf(this.a);
                throw new IOException(b.d.b.a.a.i(valueOf4.length() + 16, "Couldn't create ", valueOf4), e2);
            }
        }
    }
}
