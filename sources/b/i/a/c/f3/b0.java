package b.i.a.c.f3;

import android.os.Handler;
import android.os.Message;
import androidx.annotation.GuardedBy;
import androidx.annotation.Nullable;
import b.i.a.c.f3.o;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
/* compiled from: SystemHandlerWrapper.java */
/* loaded from: classes3.dex */
public final class b0 implements o {
    @GuardedBy("messagePool")
    public static final List<b> a = new ArrayList(50);

    /* renamed from: b  reason: collision with root package name */
    public final Handler f960b;

    /* compiled from: SystemHandlerWrapper.java */
    /* loaded from: classes3.dex */
    public static final class b implements o.a {
        @Nullable
        public Message a;

        public b() {
        }

        public final void a() {
            this.a = null;
            List<b> list = b0.a;
            synchronized (list) {
                if (list.size() < 50) {
                    list.add(this);
                }
            }
        }

        public void b() {
            Message message = this.a;
            Objects.requireNonNull(message);
            message.sendToTarget();
            a();
        }

        public b(a aVar) {
        }
    }

    public b0(Handler handler) {
        this.f960b = handler;
    }

    public static b k() {
        b bVar;
        List<b> list = a;
        synchronized (list) {
            if (list.isEmpty()) {
                bVar = new b(null);
            } else {
                bVar = list.remove(list.size() - 1);
            }
        }
        return bVar;
    }

    @Override // b.i.a.c.f3.o
    public o.a a(int i, int i2, int i3) {
        b k = k();
        k.a = this.f960b.obtainMessage(i, i2, i3);
        return k;
    }

    @Override // b.i.a.c.f3.o
    public boolean b(Runnable runnable) {
        return this.f960b.post(runnable);
    }

    @Override // b.i.a.c.f3.o
    public o.a c(int i) {
        b k = k();
        k.a = this.f960b.obtainMessage(i);
        return k;
    }

    @Override // b.i.a.c.f3.o
    public boolean d(o.a aVar) {
        b bVar = (b) aVar;
        Handler handler = this.f960b;
        Message message = bVar.a;
        Objects.requireNonNull(message);
        boolean sendMessageAtFrontOfQueue = handler.sendMessageAtFrontOfQueue(message);
        bVar.a();
        return sendMessageAtFrontOfQueue;
    }

    @Override // b.i.a.c.f3.o
    public boolean e(int i) {
        return this.f960b.hasMessages(i);
    }

    @Override // b.i.a.c.f3.o
    public boolean f(int i) {
        return this.f960b.sendEmptyMessage(i);
    }

    @Override // b.i.a.c.f3.o
    public boolean g(int i, long j) {
        return this.f960b.sendEmptyMessageAtTime(i, j);
    }

    @Override // b.i.a.c.f3.o
    public void h(int i) {
        this.f960b.removeMessages(i);
    }

    @Override // b.i.a.c.f3.o
    public o.a i(int i, @Nullable Object obj) {
        b k = k();
        k.a = this.f960b.obtainMessage(i, obj);
        return k;
    }

    @Override // b.i.a.c.f3.o
    public void j(@Nullable Object obj) {
        this.f960b.removeCallbacksAndMessages(null);
    }
}
