package b.i.a.c.f3;

import android.os.Bundle;
import androidx.annotation.Nullable;
import b.i.a.c.w0;
import b.i.b.b.a;
import b.i.b.b.n;
import b.i.b.b.p;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
/* compiled from: BundleableUtil.java */
/* loaded from: classes3.dex */
public final class f {
    public static <T extends w0> p<T> a(w0.a<T> aVar, List<Bundle> list) {
        a<Object> aVar2 = p.k;
        b.i.a.f.e.o.f.A(4, "initialCapacity");
        Object[] objArr = new Object[4];
        int i = 0;
        int i2 = 0;
        while (i < list.size()) {
            Bundle bundle = list.get(i);
            Objects.requireNonNull(bundle);
            T a = aVar.a(bundle);
            Objects.requireNonNull(a);
            int i3 = i2 + 1;
            if (objArr.length < i3) {
                objArr = Arrays.copyOf(objArr, n.b.a(objArr.length, i3));
            }
            objArr[i2] = a;
            i++;
            i2 = i3;
        }
        return p.l(objArr, i2);
    }

    public static <T extends w0> List<T> b(w0.a<T> aVar, @Nullable List<Bundle> list, List<T> list2) {
        return list == null ? list2 : a(aVar, list);
    }

    @Nullable
    public static <T extends w0> T c(w0.a<T> aVar, @Nullable Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        return aVar.a(bundle);
    }
}
