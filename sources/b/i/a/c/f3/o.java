package b.i.a.c.f3;

import androidx.annotation.Nullable;
/* compiled from: HandlerWrapper.java */
/* loaded from: classes3.dex */
public interface o {

    /* compiled from: HandlerWrapper.java */
    /* loaded from: classes3.dex */
    public interface a {
    }

    a a(int i, int i2, int i3);

    boolean b(Runnable runnable);

    a c(int i);

    boolean d(a aVar);

    boolean e(int i);

    boolean f(int i);

    boolean g(int i, long j);

    void h(int i);

    a i(int i, @Nullable Object obj);

    void j(@Nullable Object obj);
}
