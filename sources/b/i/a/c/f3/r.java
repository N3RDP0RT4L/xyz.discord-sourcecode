package b.i.a.c.f3;

import b.d.b.a.a;
import java.util.Arrays;
/* compiled from: LongArray.java */
/* loaded from: classes3.dex */
public final class r {
    public int a;

    /* renamed from: b  reason: collision with root package name */
    public long[] f971b = new long[32];

    public void a(long j) {
        int i = this.a;
        long[] jArr = this.f971b;
        if (i == jArr.length) {
            this.f971b = Arrays.copyOf(jArr, i * 2);
        }
        long[] jArr2 = this.f971b;
        int i2 = this.a;
        this.a = i2 + 1;
        jArr2[i2] = j;
    }

    public long b(int i) {
        if (i >= 0 && i < this.a) {
            return this.f971b[i];
        }
        throw new IndexOutOfBoundsException(a.g(46, "Invalid index ", i, ", size is ", this.a));
    }
}
