package b.i.a.c.f3;

import android.text.TextUtils;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.constraintlayout.solver.widgets.analyzer.BasicMeasure;
import androidx.core.app.NotificationCompat;
import b.i.a.f.e.o.f;
import java.util.ArrayList;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.objectweb.asm.Opcodes;
import org.webrtc.MediaStreamTrack;
/* compiled from: MimeTypes.java */
/* loaded from: classes3.dex */
public final class t {
    public static final ArrayList<a> a = new ArrayList<>();

    /* renamed from: b  reason: collision with root package name */
    public static final Pattern f972b = Pattern.compile("^mp4a\\.([a-zA-Z0-9]{2})(?:\\.([0-9]{1,2}))?$");

    /* compiled from: MimeTypes.java */
    /* loaded from: classes3.dex */
    public static final class a {
    }

    /* compiled from: MimeTypes.java */
    @VisibleForTesting
    /* loaded from: classes3.dex */
    public static final class b {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final int f973b;

        public b(int i, int i2) {
            this.a = i;
            this.f973b = i2;
        }

        public int a() {
            int i = this.f973b;
            if (i == 2) {
                return 10;
            }
            if (i == 5) {
                return 11;
            }
            if (i == 29) {
                return 12;
            }
            if (i != 42) {
                return i != 22 ? i != 23 ? 0 : 15 : BasicMeasure.EXACTLY;
            }
            return 16;
        }
    }

    public static boolean a(@Nullable String str, @Nullable String str2) {
        b e;
        int a2;
        if (str == null) {
            return false;
        }
        char c = 65535;
        switch (str.hashCode()) {
            case -2123537834:
                if (str.equals("audio/eac3-joc")) {
                    c = 0;
                    break;
                }
                break;
            case -432837260:
                if (str.equals("audio/mpeg-L1")) {
                    c = 1;
                    break;
                }
                break;
            case -432837259:
                if (str.equals("audio/mpeg-L2")) {
                    c = 2;
                    break;
                }
                break;
            case -53558318:
                if (str.equals("audio/mp4a-latm")) {
                    c = 3;
                    break;
                }
                break;
            case 187078296:
                if (str.equals("audio/ac3")) {
                    c = 4;
                    break;
                }
                break;
            case 187094639:
                if (str.equals("audio/raw")) {
                    c = 5;
                    break;
                }
                break;
            case 1504578661:
                if (str.equals("audio/eac3")) {
                    c = 6;
                    break;
                }
                break;
            case 1504619009:
                if (str.equals("audio/flac")) {
                    c = 7;
                    break;
                }
                break;
            case 1504831518:
                if (str.equals("audio/mpeg")) {
                    c = '\b';
                    break;
                }
                break;
            case 1903231877:
                if (str.equals("audio/g711-alaw")) {
                    c = '\t';
                    break;
                }
                break;
            case 1903589369:
                if (str.equals("audio/g711-mlaw")) {
                    c = '\n';
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
            case 1:
            case 2:
            case 4:
            case 5:
            case 6:
            case 7:
            case '\b':
            case '\t':
            case '\n':
                return true;
            case 3:
                return (str2 == null || (e = e(str2)) == null || (a2 = e.a()) == 0 || a2 == 16) ? false : true;
            default:
                return false;
        }
    }

    public static int b(String str, @Nullable String str2) {
        b e;
        char c = 65535;
        switch (str.hashCode()) {
            case -2123537834:
                if (str.equals("audio/eac3-joc")) {
                    c = 0;
                    break;
                }
                break;
            case -1095064472:
                if (str.equals("audio/vnd.dts")) {
                    c = 1;
                    break;
                }
                break;
            case -53558318:
                if (str.equals("audio/mp4a-latm")) {
                    c = 2;
                    break;
                }
                break;
            case 187078296:
                if (str.equals("audio/ac3")) {
                    c = 3;
                    break;
                }
                break;
            case 187078297:
                if (str.equals("audio/ac4")) {
                    c = 4;
                    break;
                }
                break;
            case 1504578661:
                if (str.equals("audio/eac3")) {
                    c = 5;
                    break;
                }
                break;
            case 1504831518:
                if (str.equals("audio/mpeg")) {
                    c = 6;
                    break;
                }
                break;
            case 1505942594:
                if (str.equals("audio/vnd.dts.hd")) {
                    c = 7;
                    break;
                }
                break;
            case 1556697186:
                if (str.equals("audio/true-hd")) {
                    c = '\b';
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return 18;
            case 1:
                return 7;
            case 2:
                if (str2 == null || (e = e(str2)) == null) {
                    return 0;
                }
                return e.a();
            case 3:
                return 5;
            case 4:
                return 17;
            case 5:
                return 6;
            case 6:
                return 9;
            case 7:
                return 8;
            case '\b':
                return 14;
            default:
                return 0;
        }
    }

    @Nullable
    public static String c(@Nullable String str) {
        b e;
        String str2 = null;
        if (str == null) {
            return null;
        }
        String u1 = f.u1(str.trim());
        if (u1.startsWith("avc1") || u1.startsWith("avc3")) {
            return "video/avc";
        }
        if (u1.startsWith("hev1") || u1.startsWith("hvc1")) {
            return "video/hevc";
        }
        if (u1.startsWith("dvav") || u1.startsWith("dva1") || u1.startsWith("dvhe") || u1.startsWith("dvh1")) {
            return "video/dolby-vision";
        }
        if (u1.startsWith("av01")) {
            return "video/av01";
        }
        if (u1.startsWith("vp9") || u1.startsWith("vp09")) {
            return "video/x-vnd.on2.vp9";
        }
        if (u1.startsWith("vp8") || u1.startsWith("vp08")) {
            return "video/x-vnd.on2.vp8";
        }
        if (u1.startsWith("mp4a")) {
            if (u1.startsWith("mp4a.") && (e = e(u1)) != null) {
                str2 = d(e.a);
            }
            return str2 == null ? "audio/mp4a-latm" : str2;
        } else if (u1.startsWith("mha1")) {
            return "audio/mha1";
        } else {
            if (u1.startsWith("mhm1")) {
                return "audio/mhm1";
            }
            if (u1.startsWith("ac-3") || u1.startsWith("dac3")) {
                return "audio/ac3";
            }
            if (u1.startsWith("ec-3") || u1.startsWith("dec3")) {
                return "audio/eac3";
            }
            if (u1.startsWith("ec+3")) {
                return "audio/eac3-joc";
            }
            if (u1.startsWith("ac-4") || u1.startsWith("dac4")) {
                return "audio/ac4";
            }
            if (u1.startsWith("dtsc")) {
                return "audio/vnd.dts";
            }
            if (u1.startsWith("dtse")) {
                return "audio/vnd.dts.hd;profile=lbr";
            }
            if (u1.startsWith("dtsh") || u1.startsWith("dtsl")) {
                return "audio/vnd.dts.hd";
            }
            if (u1.startsWith("dtsx")) {
                return "audio/vnd.dts.uhd;profile=p2";
            }
            if (u1.startsWith("opus")) {
                return "audio/opus";
            }
            if (u1.startsWith("vorbis")) {
                return "audio/vorbis";
            }
            if (u1.startsWith("flac")) {
                return "audio/flac";
            }
            if (u1.startsWith("stpp")) {
                return "application/ttml+xml";
            }
            if (u1.startsWith("wvtt")) {
                return "text/vtt";
            }
            if (u1.contains("cea708")) {
                return "application/cea-708";
            }
            if (u1.contains("eia608") || u1.contains("cea608")) {
                return "application/cea-608";
            }
            int size = a.size();
            for (int i = 0; i < size; i++) {
                Objects.requireNonNull(a.get(i));
                if (u1.startsWith(null)) {
                    break;
                }
            }
            return null;
        }
    }

    @Nullable
    public static String d(int i) {
        if (i == 32) {
            return "video/mp4v-es";
        }
        if (i == 33) {
            return "video/avc";
        }
        if (i == 35) {
            return "video/hevc";
        }
        if (i == 64) {
            return "audio/mp4a-latm";
        }
        if (i == 163) {
            return "video/wvc1";
        }
        if (i == 177) {
            return "video/x-vnd.on2.vp9";
        }
        if (i == 165) {
            return "audio/ac3";
        }
        if (i == 166) {
            return "audio/eac3";
        }
        switch (i) {
            case 96:
            case 97:
            case 98:
            case 99:
            case 100:
            case 101:
                return "video/mpeg2";
            case 102:
            case 103:
            case 104:
                return "audio/mp4a-latm";
            case 105:
            case 107:
                return "audio/mpeg";
            case 106:
                return "video/mpeg";
            default:
                switch (i) {
                    case Opcodes.RET /* 169 */:
                    case Opcodes.IRETURN /* 172 */:
                        return "audio/vnd.dts";
                    case Opcodes.TABLESWITCH /* 170 */:
                    case Opcodes.LOOKUPSWITCH /* 171 */:
                        return "audio/vnd.dts.hd";
                    case Opcodes.LRETURN /* 173 */:
                        return "audio/opus";
                    case Opcodes.FRETURN /* 174 */:
                        return "audio/ac4";
                    default:
                        return null;
                }
        }
    }

    @Nullable
    @VisibleForTesting
    public static b e(String str) {
        Matcher matcher = f972b.matcher(str);
        if (!matcher.matches()) {
            return null;
        }
        String group = matcher.group(1);
        Objects.requireNonNull(group);
        String group2 = matcher.group(2);
        int i = 0;
        try {
            int parseInt = Integer.parseInt(group, 16);
            if (group2 != null) {
                i = Integer.parseInt(group2);
            }
            return new b(parseInt, i);
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    @Nullable
    public static String f(@Nullable String str) {
        int indexOf;
        if (str == null || (indexOf = str.indexOf(47)) == -1) {
            return null;
        }
        return str.substring(0, indexOf);
    }

    public static int g(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return -1;
        }
        if (h(str)) {
            return 1;
        }
        if (j(str)) {
            return 2;
        }
        if (i(str)) {
            return 3;
        }
        if ("image".equals(f(str))) {
            return 4;
        }
        if ("application/id3".equals(str) || "application/x-emsg".equals(str) || "application/x-scte35".equals(str)) {
            return 5;
        }
        if ("application/x-camera-motion".equals(str)) {
            return 6;
        }
        int size = a.size();
        for (int i = 0; i < size; i++) {
            Objects.requireNonNull(a.get(i));
            if (str.equals(null)) {
                return 0;
            }
        }
        return -1;
    }

    public static boolean h(@Nullable String str) {
        return MediaStreamTrack.AUDIO_TRACK_KIND.equals(f(str));
    }

    public static boolean i(@Nullable String str) {
        return NotificationCompat.MessagingStyle.Message.KEY_TEXT.equals(f(str)) || "application/cea-608".equals(str) || "application/cea-708".equals(str) || "application/x-mp4-cea-608".equals(str) || "application/x-subrip".equals(str) || "application/ttml+xml".equals(str) || "application/x-quicktime-tx3g".equals(str) || "application/x-mp4-vtt".equals(str) || "application/x-rawcc".equals(str) || "application/vobsub".equals(str) || "application/pgs".equals(str) || "application/dvbsubs".equals(str);
    }

    public static boolean j(@Nullable String str) {
        return MediaStreamTrack.VIDEO_TRACK_KIND.equals(f(str));
    }
}
