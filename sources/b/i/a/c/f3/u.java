package b.i.a.c.f3;

import b.c.a.a0.d;
import java.util.Arrays;
/* compiled from: NalUnitUtil.java */
/* loaded from: classes3.dex */
public final class u {
    public static final byte[] a = {0, 0, 0, 1};

    /* renamed from: b  reason: collision with root package name */
    public static final float[] f974b = {1.0f, 1.0f, 1.0909091f, 0.90909094f, 1.4545455f, 1.2121212f, 2.1818182f, 1.8181819f, 2.909091f, 2.4242425f, 1.6363636f, 1.3636364f, 1.939394f, 1.6161616f, 1.3333334f, 1.5f, 2.0f};
    public static final Object c = new Object();
    public static int[] d = new int[10];

    /* compiled from: NalUnitUtil.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f975b;
        public final int c;
        public final int d;
        public final int[] e;
        public final int f;
        public final int g;
        public final int h;
        public final float i;

        public a(int i, boolean z2, int i2, int i3, int[] iArr, int i4, int i5, int i6, int i7, float f) {
            this.a = i;
            this.f975b = z2;
            this.c = i2;
            this.d = i3;
            this.e = iArr;
            this.f = i4;
            this.g = i6;
            this.h = i7;
            this.i = f;
        }
    }

    /* compiled from: NalUnitUtil.java */
    /* loaded from: classes3.dex */
    public static final class b {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final int f976b;
        public final boolean c;

        public b(int i, int i2, boolean z2) {
            this.a = i;
            this.f976b = i2;
            this.c = z2;
        }
    }

    /* compiled from: NalUnitUtil.java */
    /* loaded from: classes3.dex */
    public static final class c {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final int f977b;
        public final int c;
        public final int d;
        public final int e;
        public final int f;
        public final float g;
        public final boolean h;
        public final boolean i;
        public final int j;
        public final int k;
        public final int l;
        public final boolean m;

        public c(int i, int i2, int i3, int i4, int i5, int i6, float f, boolean z2, boolean z3, int i7, int i8, int i9, boolean z4) {
            this.a = i;
            this.f977b = i2;
            this.c = i3;
            this.d = i4;
            this.e = i5;
            this.f = i6;
            this.g = f;
            this.h = z2;
            this.i = z3;
            this.j = i7;
            this.k = i8;
            this.l = i9;
            this.m = z4;
        }
    }

    public static void a(boolean[] zArr) {
        zArr[0] = false;
        zArr[1] = false;
        zArr[2] = false;
    }

    public static int b(byte[] bArr, int i, int i2, boolean[] zArr) {
        int i3 = i2 - i;
        boolean z2 = false;
        d.D(i3 >= 0);
        if (i3 == 0) {
            return i2;
        }
        if (zArr[0]) {
            zArr[0] = false;
            zArr[1] = false;
            zArr[2] = false;
            return i - 3;
        } else if (i3 > 1 && zArr[1] && bArr[i] == 1) {
            zArr[0] = false;
            zArr[1] = false;
            zArr[2] = false;
            return i - 2;
        } else if (i3 <= 2 || !zArr[2] || bArr[i] != 0 || bArr[i + 1] != 1) {
            int i4 = i2 - 1;
            int i5 = i + 2;
            while (i5 < i4) {
                if ((bArr[i5] & 254) == 0) {
                    int i6 = i5 - 2;
                    if (bArr[i6] == 0 && bArr[i5 - 1] == 0 && bArr[i5] == 1) {
                        zArr[0] = false;
                        zArr[1] = false;
                        zArr[2] = false;
                        return i6;
                    }
                    i5 -= 2;
                }
                i5 += 3;
            }
            zArr[0] = i3 <= 2 ? !(i3 != 2 ? !zArr[1] || bArr[i4] != 1 : !(zArr[2] && bArr[i2 + (-2)] == 0 && bArr[i4] == 1)) : bArr[i2 + (-3)] == 0 && bArr[i2 + (-2)] == 0 && bArr[i4] == 1;
            zArr[1] = i3 <= 1 ? !(!zArr[2] || bArr[i4] != 0) : bArr[i2 + (-2)] == 0 && bArr[i4] == 0;
            if (bArr[i4] == 0) {
                z2 = true;
            }
            zArr[2] = z2;
            return i2;
        } else {
            zArr[0] = false;
            zArr[1] = false;
            zArr[2] = false;
            return i - 1;
        }
    }

    public static a c(byte[] bArr, int i, int i2) {
        int i3 = 2;
        y yVar = new y(bArr, i + 2, i2);
        yVar.j(4);
        int e = yVar.e(3);
        yVar.i();
        int e2 = yVar.e(2);
        boolean d2 = yVar.d();
        int e3 = yVar.e(5);
        int i4 = 0;
        for (int i5 = 0; i5 < 32; i5++) {
            if (yVar.d()) {
                i4 |= 1 << i5;
            }
        }
        int i6 = 6;
        int[] iArr = new int[6];
        for (int i7 = 0; i7 < 6; i7++) {
            iArr[i7] = yVar.e(8);
        }
        int e4 = yVar.e(8);
        int i8 = 0;
        for (int i9 = 0; i9 < e; i9++) {
            if (yVar.d()) {
                i8 += 89;
            }
            if (yVar.d()) {
                i8 += 8;
            }
        }
        yVar.j(i8);
        if (e > 0) {
            yVar.j((8 - e) * 2);
        }
        int f = yVar.f();
        int f2 = yVar.f();
        if (f2 == 3) {
            yVar.i();
        }
        int f3 = yVar.f();
        int f4 = yVar.f();
        if (yVar.d()) {
            int f5 = yVar.f();
            int f6 = yVar.f();
            int f7 = yVar.f();
            int f8 = yVar.f();
            f3 -= (f5 + f6) * ((f2 == 1 || f2 == 2) ? 2 : 1);
            f4 -= (f7 + f8) * (f2 == 1 ? 2 : 1);
        }
        yVar.f();
        yVar.f();
        int f9 = yVar.f();
        for (int i10 = yVar.d() ? 0 : e; i10 <= e; i10++) {
            yVar.f();
            yVar.f();
            yVar.f();
        }
        yVar.f();
        yVar.f();
        yVar.f();
        yVar.f();
        yVar.f();
        yVar.f();
        if (yVar.d() && yVar.d()) {
            int i11 = 0;
            int i12 = 4;
            while (i11 < i12) {
                int i13 = 0;
                while (i13 < i6) {
                    if (!yVar.d()) {
                        yVar.f();
                    } else {
                        int min = Math.min(64, 1 << ((i11 << 1) + 4));
                        if (i11 > 1) {
                            yVar.g();
                        }
                        for (int i14 = 0; i14 < min; i14++) {
                            yVar.g();
                        }
                    }
                    i13 += i11 == 3 ? 3 : 1;
                    i6 = 6;
                }
                i11++;
                i12 = 4;
                i6 = 6;
                i3 = 2;
            }
        }
        yVar.j(i3);
        if (yVar.d()) {
            yVar.j(8);
            yVar.f();
            yVar.f();
            yVar.i();
        }
        int f10 = yVar.f();
        boolean z2 = false;
        int i15 = 0;
        for (int i16 = 0; i16 < f10; i16++) {
            if (i16 != 0) {
                z2 = yVar.d();
            }
            if (z2) {
                yVar.i();
                yVar.f();
                for (int i17 = 0; i17 <= i15; i17++) {
                    if (yVar.d()) {
                        yVar.i();
                    }
                }
            } else {
                int f11 = yVar.f();
                int f12 = yVar.f();
                i15 = f11 + f12;
                for (int i18 = 0; i18 < f11; i18++) {
                    yVar.f();
                    yVar.i();
                }
                for (int i19 = 0; i19 < f12; i19++) {
                    yVar.f();
                    yVar.i();
                }
            }
        }
        if (yVar.d()) {
            for (int i20 = 0; i20 < yVar.f(); i20++) {
                yVar.j(f9 + 4 + 1);
            }
        }
        yVar.j(2);
        float f13 = 1.0f;
        if (yVar.d()) {
            if (yVar.d()) {
                int e5 = yVar.e(8);
                if (e5 == 255) {
                    int e6 = yVar.e(16);
                    int e7 = yVar.e(16);
                    if (!(e6 == 0 || e7 == 0)) {
                        f13 = e6 / e7;
                    }
                } else {
                    float[] fArr = f974b;
                    if (e5 < fArr.length) {
                        f13 = fArr[e5];
                    } else {
                        b.d.b.a.a.d0(46, "Unexpected aspect_ratio_idc value: ", e5, "NalUnitUtil");
                    }
                }
            }
            if (yVar.d()) {
                yVar.i();
            }
            if (yVar.d()) {
                yVar.j(4);
                if (yVar.d()) {
                    yVar.j(24);
                }
            }
            if (yVar.d()) {
                yVar.f();
                yVar.f();
            }
            yVar.i();
            if (yVar.d()) {
                f4 *= 2;
            }
        }
        return new a(e2, d2, e3, i4, iArr, e4, f, f3, f4, f13);
    }

    public static b d(byte[] bArr, int i, int i2) {
        y yVar = new y(bArr, i + 1, i2);
        int f = yVar.f();
        int f2 = yVar.f();
        yVar.i();
        return new b(f, f2, yVar.d());
    }

    /* JADX WARN: Removed duplicated region for block: B:62:0x00f8  */
    /* JADX WARN: Removed duplicated region for block: B:65:0x0108  */
    /* JADX WARN: Removed duplicated region for block: B:77:0x013c  */
    /* JADX WARN: Removed duplicated region for block: B:84:0x0158  */
    /* JADX WARN: Removed duplicated region for block: B:88:0x016b  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static b.i.a.c.f3.u.c e(byte[] r18, int r19, int r20) {
        /*
            Method dump skipped, instructions count: 394
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.f3.u.e(byte[], int, int):b.i.a.c.f3.u$c");
    }

    public static int f(byte[] bArr, int i) {
        int i2;
        synchronized (c) {
            int i3 = 0;
            int i4 = 0;
            while (i3 < i) {
                while (true) {
                    if (i3 >= i - 2) {
                        i3 = i;
                        break;
                    }
                    if (bArr[i3] == 0 && bArr[i3 + 1] == 0 && bArr[i3 + 2] == 3) {
                        break;
                    }
                    i3++;
                }
                if (i3 < i) {
                    int[] iArr = d;
                    if (iArr.length <= i4) {
                        d = Arrays.copyOf(iArr, iArr.length * 2);
                    }
                    i4++;
                    d[i4] = i3;
                    i3 += 3;
                }
            }
            i2 = i - i4;
            int i5 = 0;
            int i6 = 0;
            for (int i7 = 0; i7 < i4; i7++) {
                int i8 = d[i7] - i6;
                System.arraycopy(bArr, i6, bArr, i5, i8);
                int i9 = i5 + i8;
                int i10 = i9 + 1;
                bArr[i9] = 0;
                i5 = i10 + 1;
                bArr[i10] = 0;
                i6 += i8 + 3;
            }
            System.arraycopy(bArr, i6, bArr, i5, i2 - i5);
        }
        return i2;
    }
}
