package b.i.a.c.f3;

import android.os.Handler;
import android.os.Looper;
import androidx.annotation.Nullable;
/* compiled from: Clock.java */
/* loaded from: classes3.dex */
public interface g {
    public static final g a = new a0();

    long a();

    o b(Looper looper, @Nullable Handler.Callback callback);

    void c();

    long d();
}
