package b.i.a.c.f3;

import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.d.b.a.a;
import b.i.b.a.c;
import java.nio.charset.Charset;
import java.util.Arrays;
/* compiled from: ParsableByteArray.java */
/* loaded from: classes3.dex */
public final class x {
    public byte[] a;

    /* renamed from: b  reason: collision with root package name */
    public int f980b;
    public int c;

    public x() {
        this.a = e0.f;
    }

    public void A(int i) {
        byte[] bArr = this.a;
        if (bArr.length < i) {
            bArr = new byte[i];
        }
        C(bArr, i);
    }

    public void B(byte[] bArr) {
        int length = bArr.length;
        this.a = bArr;
        this.c = length;
        this.f980b = 0;
    }

    public void C(byte[] bArr, int i) {
        this.a = bArr;
        this.c = i;
        this.f980b = 0;
    }

    public void D(int i) {
        d.j(i >= 0 && i <= this.a.length);
        this.c = i;
    }

    public void E(int i) {
        d.j(i >= 0 && i <= this.c);
        this.f980b = i;
    }

    public void F(int i) {
        E(this.f980b + i);
    }

    public int a() {
        return this.c - this.f980b;
    }

    public void b(int i) {
        byte[] bArr = this.a;
        if (i > bArr.length) {
            this.a = Arrays.copyOf(bArr, i);
        }
    }

    public int c() {
        return this.a[this.f980b] & 255;
    }

    public void d(w wVar, int i) {
        e(wVar.a, 0, i);
        wVar.k(0);
    }

    public void e(byte[] bArr, int i, int i2) {
        System.arraycopy(this.a, this.f980b, bArr, i, i2);
        this.f980b += i2;
    }

    public int f() {
        byte[] bArr = this.a;
        int i = this.f980b;
        int i2 = i + 1;
        this.f980b = i2;
        int i3 = i2 + 1;
        this.f980b = i3;
        int i4 = ((bArr[i] & 255) << 24) | ((bArr[i2] & 255) << 16);
        int i5 = i3 + 1;
        this.f980b = i5;
        int i6 = i4 | ((bArr[i3] & 255) << 8);
        this.f980b = i5 + 1;
        return (bArr[i5] & 255) | i6;
    }

    @Nullable
    public String g() {
        if (a() == 0) {
            return null;
        }
        int i = this.f980b;
        while (i < this.c) {
            byte b2 = this.a[i];
            int i2 = e0.a;
            if (b2 == 10 || b2 == 13) {
                break;
            }
            i++;
        }
        int i3 = this.f980b;
        if (i - i3 >= 3) {
            byte[] bArr = this.a;
            if (bArr[i3] == -17 && bArr[i3 + 1] == -69 && bArr[i3 + 2] == -65) {
                this.f980b = i3 + 3;
            }
        }
        byte[] bArr2 = this.a;
        int i4 = this.f980b;
        String m = e0.m(bArr2, i4, i - i4);
        this.f980b = i;
        int i5 = this.c;
        if (i == i5) {
            return m;
        }
        byte[] bArr3 = this.a;
        if (bArr3[i] == 13) {
            int i6 = i + 1;
            this.f980b = i6;
            if (i6 == i5) {
                return m;
            }
        }
        int i7 = this.f980b;
        if (bArr3[i7] == 10) {
            this.f980b = i7 + 1;
        }
        return m;
    }

    public int h() {
        byte[] bArr = this.a;
        int i = this.f980b;
        int i2 = i + 1;
        this.f980b = i2;
        int i3 = i2 + 1;
        this.f980b = i3;
        int i4 = (bArr[i] & 255) | ((bArr[i2] & 255) << 8);
        int i5 = i3 + 1;
        this.f980b = i5;
        int i6 = i4 | ((bArr[i3] & 255) << 16);
        this.f980b = i5 + 1;
        return ((bArr[i5] & 255) << 24) | i6;
    }

    public short i() {
        byte[] bArr = this.a;
        int i = this.f980b;
        int i2 = i + 1;
        this.f980b = i2;
        this.f980b = i2 + 1;
        return (short) (((bArr[i2] & 255) << 8) | (bArr[i] & 255));
    }

    public long j() {
        byte[] bArr = this.a;
        int i = this.f980b;
        int i2 = i + 1;
        this.f980b = i2;
        int i3 = i2 + 1;
        this.f980b = i3;
        int i4 = i3 + 1;
        this.f980b = i4;
        this.f980b = i4 + 1;
        return (bArr[i] & 255) | ((bArr[i2] & 255) << 8) | ((bArr[i3] & 255) << 16) | ((bArr[i4] & 255) << 24);
    }

    public int k() {
        int h = h();
        if (h >= 0) {
            return h;
        }
        throw new IllegalStateException(a.f(29, "Top bit not zero: ", h));
    }

    public int l() {
        byte[] bArr = this.a;
        int i = this.f980b;
        int i2 = i + 1;
        this.f980b = i2;
        this.f980b = i2 + 1;
        return ((bArr[i2] & 255) << 8) | (bArr[i] & 255);
    }

    public long m() {
        byte[] bArr = this.a;
        int i = this.f980b;
        int i2 = i + 1;
        this.f980b = i2;
        int i3 = i2 + 1;
        this.f980b = i3;
        int i4 = i3 + 1;
        this.f980b = i4;
        int i5 = i4 + 1;
        this.f980b = i5;
        int i6 = i5 + 1;
        this.f980b = i6;
        int i7 = i6 + 1;
        this.f980b = i7;
        int i8 = i7 + 1;
        this.f980b = i8;
        this.f980b = i8 + 1;
        return ((bArr[i] & 255) << 56) | ((bArr[i2] & 255) << 48) | ((bArr[i3] & 255) << 40) | ((bArr[i4] & 255) << 32) | ((bArr[i5] & 255) << 24) | ((bArr[i6] & 255) << 16) | ((bArr[i7] & 255) << 8) | (bArr[i8] & 255);
    }

    @Nullable
    public String n() {
        if (a() == 0) {
            return null;
        }
        int i = this.f980b;
        while (i < this.c && this.a[i] != 0) {
            i++;
        }
        byte[] bArr = this.a;
        int i2 = this.f980b;
        String m = e0.m(bArr, i2, i - i2);
        this.f980b = i;
        if (i < this.c) {
            this.f980b = i + 1;
        }
        return m;
    }

    public String o(int i) {
        if (i == 0) {
            return "";
        }
        int i2 = this.f980b;
        int i3 = (i2 + i) - 1;
        String m = e0.m(this.a, i2, (i3 >= this.c || this.a[i3] != 0) ? i : i - 1);
        this.f980b += i;
        return m;
    }

    public short p() {
        byte[] bArr = this.a;
        int i = this.f980b;
        int i2 = i + 1;
        this.f980b = i2;
        this.f980b = i2 + 1;
        return (short) ((bArr[i2] & 255) | ((bArr[i] & 255) << 8));
    }

    public String q(int i) {
        return r(i, c.c);
    }

    public String r(int i, Charset charset) {
        String str = new String(this.a, this.f980b, i, charset);
        this.f980b += i;
        return str;
    }

    public int s() {
        return (t() << 21) | (t() << 14) | (t() << 7) | t();
    }

    public int t() {
        byte[] bArr = this.a;
        int i = this.f980b;
        this.f980b = i + 1;
        return bArr[i] & 255;
    }

    public long u() {
        byte[] bArr = this.a;
        int i = this.f980b;
        int i2 = i + 1;
        this.f980b = i2;
        int i3 = i2 + 1;
        this.f980b = i3;
        int i4 = i3 + 1;
        this.f980b = i4;
        this.f980b = i4 + 1;
        return ((bArr[i] & 255) << 24) | ((bArr[i2] & 255) << 16) | ((bArr[i3] & 255) << 8) | (bArr[i4] & 255);
    }

    public int v() {
        byte[] bArr = this.a;
        int i = this.f980b;
        int i2 = i + 1;
        this.f980b = i2;
        int i3 = i2 + 1;
        this.f980b = i3;
        int i4 = ((bArr[i] & 255) << 16) | ((bArr[i2] & 255) << 8);
        this.f980b = i3 + 1;
        return (bArr[i3] & 255) | i4;
    }

    public int w() {
        int f = f();
        if (f >= 0) {
            return f;
        }
        throw new IllegalStateException(a.f(29, "Top bit not zero: ", f));
    }

    public long x() {
        long m = m();
        if (m >= 0) {
            return m;
        }
        StringBuilder sb = new StringBuilder(38);
        sb.append("Top bit not zero: ");
        sb.append(m);
        throw new IllegalStateException(sb.toString());
    }

    public int y() {
        byte[] bArr = this.a;
        int i = this.f980b;
        int i2 = i + 1;
        this.f980b = i2;
        this.f980b = i2 + 1;
        return (bArr[i2] & 255) | ((bArr[i] & 255) << 8);
    }

    public long z() {
        int i;
        int i2;
        byte b2;
        int i3;
        long j = this.a[this.f980b];
        int i4 = 7;
        while (true) {
            if (i4 < 0) {
                break;
            }
            if (((1 << i4) & j) != 0) {
                i4--;
            } else if (i4 < 6) {
                j &= i3 - 1;
                i2 = 7 - i4;
            } else if (i4 == 7) {
                i2 = 1;
            }
        }
        i2 = 0;
        if (i2 != 0) {
            for (i = 1; i < i2; i++) {
                if ((this.a[this.f980b + i] & 192) == 128) {
                    j = (j << 6) | (b2 & 63);
                } else {
                    StringBuilder sb = new StringBuilder(62);
                    sb.append("Invalid UTF-8 sequence continuation byte: ");
                    sb.append(j);
                    throw new NumberFormatException(sb.toString());
                }
            }
            this.f980b += i2;
            return j;
        }
        StringBuilder sb2 = new StringBuilder(55);
        sb2.append("Invalid UTF-8 sequence first byte: ");
        sb2.append(j);
        throw new NumberFormatException(sb2.toString());
    }

    public x(int i) {
        this.a = new byte[i];
        this.c = i;
    }

    public x(byte[] bArr) {
        this.a = bArr;
        this.c = bArr.length;
    }

    public x(byte[] bArr, int i) {
        this.a = bArr;
        this.c = i;
    }
}
