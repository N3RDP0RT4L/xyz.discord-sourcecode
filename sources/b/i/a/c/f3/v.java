package b.i.a.c.f3;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.TelephonyDisplayInfo;
import androidx.annotation.GuardedBy;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
/* compiled from: NetworkTypeObserver.java */
/* loaded from: classes3.dex */
public final class v {
    @Nullable
    public static v a;

    /* renamed from: b  reason: collision with root package name */
    public final Handler f978b = new Handler(Looper.getMainLooper());
    public final CopyOnWriteArrayList<WeakReference<b>> c = new CopyOnWriteArrayList<>();
    public final Object d = new Object();
    @GuardedBy("networkTypeLock")
    public int e = 0;

    /* compiled from: NetworkTypeObserver.java */
    /* loaded from: classes3.dex */
    public interface b {
        void a(int i);
    }

    /* compiled from: NetworkTypeObserver.java */
    /* loaded from: classes3.dex */
    public final class c extends BroadcastReceiver {
        public c(a aVar) {
        }

        /* JADX WARN: Code restructure failed: missing block: B:21:0x0041, code lost:
            if (b.i.a.c.f3.e0.a >= 29) goto L28;
         */
        /* JADX WARN: Removed duplicated region for block: B:33:0x006c A[Catch: RuntimeException -> 0x0079, TryCatch #0 {RuntimeException -> 0x0079, blocks: (B:31:0x0055, B:33:0x006c, B:34:0x0070, B:35:0x0075), top: B:41:0x0055 }] */
        /* JADX WARN: Removed duplicated region for block: B:34:0x0070 A[Catch: RuntimeException -> 0x0079, TryCatch #0 {RuntimeException -> 0x0079, blocks: (B:31:0x0055, B:33:0x006c, B:34:0x0070, B:35:0x0075), top: B:41:0x0055 }] */
        @Override // android.content.BroadcastReceiver
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public void onReceive(android.content.Context r9, android.content.Intent r10) {
            /*
                r8 = this;
                java.lang.String r10 = "connectivity"
                java.lang.Object r10 = r9.getSystemService(r10)
                android.net.ConnectivityManager r10 = (android.net.ConnectivityManager) r10
                r0 = 29
                r1 = 9
                r2 = 6
                r3 = 4
                r4 = 0
                r5 = 1
                r6 = 5
                if (r10 != 0) goto L14
                goto L4e
            L14:
                android.net.NetworkInfo r10 = r10.getActiveNetworkInfo()     // Catch: java.lang.SecurityException -> L4e
                if (r10 == 0) goto L4c
                boolean r7 = r10.isConnected()
                if (r7 != 0) goto L21
                goto L4c
            L21:
                int r7 = r10.getType()
                if (r7 == 0) goto L36
                if (r7 == r5) goto L44
                if (r7 == r3) goto L36
                if (r7 == r6) goto L36
                if (r7 == r2) goto L46
                if (r7 == r1) goto L34
                r1 = 8
                goto L4f
            L34:
                r1 = 7
                goto L4f
            L36:
                int r10 = r10.getSubtype()
                switch(r10) {
                    case 1: goto L4a;
                    case 2: goto L4a;
                    case 3: goto L48;
                    case 4: goto L48;
                    case 5: goto L48;
                    case 6: goto L48;
                    case 7: goto L48;
                    case 8: goto L48;
                    case 9: goto L48;
                    case 10: goto L48;
                    case 11: goto L48;
                    case 12: goto L48;
                    case 13: goto L46;
                    case 14: goto L48;
                    case 15: goto L48;
                    case 16: goto L3d;
                    case 17: goto L48;
                    case 18: goto L44;
                    case 19: goto L3d;
                    case 20: goto L3f;
                    default: goto L3d;
                }
            L3d:
                r1 = 6
                goto L4f
            L3f:
                int r10 = b.i.a.c.f3.e0.a
                if (r10 < r0) goto L4e
                goto L4f
            L44:
                r1 = 2
                goto L4f
            L46:
                r1 = 5
                goto L4f
            L48:
                r1 = 4
                goto L4f
            L4a:
                r1 = 3
                goto L4f
            L4c:
                r1 = 1
                goto L4f
            L4e:
                r1 = 0
            L4f:
                int r10 = b.i.a.c.f3.e0.a
                if (r10 < r0) goto L79
                if (r1 != r6) goto L79
                java.lang.String r0 = "phone"
                java.lang.Object r9 = r9.getSystemService(r0)     // Catch: java.lang.RuntimeException -> L79
                android.telephony.TelephonyManager r9 = (android.telephony.TelephonyManager) r9     // Catch: java.lang.RuntimeException -> L79
                java.util.Objects.requireNonNull(r9)     // Catch: java.lang.RuntimeException -> L79
                b.i.a.c.f3.v$d r0 = new b.i.a.c.f3.v$d     // Catch: java.lang.RuntimeException -> L79
                b.i.a.c.f3.v r2 = b.i.a.c.f3.v.this     // Catch: java.lang.RuntimeException -> L79
                r3 = 0
                r0.<init>(r3)     // Catch: java.lang.RuntimeException -> L79
                r2 = 31
                if (r10 >= r2) goto L70
                r9.listen(r0, r5)     // Catch: java.lang.RuntimeException -> L79
                goto L75
            L70:
                r10 = 1048576(0x100000, float:1.469368E-39)
                r9.listen(r0, r10)     // Catch: java.lang.RuntimeException -> L79
            L75:
                r9.listen(r0, r4)     // Catch: java.lang.RuntimeException -> L79
                return
            L79:
                b.i.a.c.f3.v r9 = b.i.a.c.f3.v.this
                b.i.a.c.f3.v.a(r9, r1)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.f3.v.c.onReceive(android.content.Context, android.content.Intent):void");
        }
    }

    /* compiled from: NetworkTypeObserver.java */
    /* loaded from: classes3.dex */
    public class d extends PhoneStateListener {
        public d(a aVar) {
        }

        @Override // android.telephony.PhoneStateListener
        @RequiresApi(31)
        public void onDisplayInfoChanged(TelephonyDisplayInfo telephonyDisplayInfo) {
            int overrideNetworkType = telephonyDisplayInfo.getOverrideNetworkType();
            v.a(v.this, overrideNetworkType == 3 || overrideNetworkType == 4 ? 10 : 5);
        }

        @Override // android.telephony.PhoneStateListener
        public void onServiceStateChanged(@Nullable ServiceState serviceState) {
            String serviceState2 = serviceState == null ? "" : serviceState.toString();
            v.a(v.this, serviceState2.contains("nrState=CONNECTED") || serviceState2.contains("nrState=NOT_RESTRICTED") ? 10 : 5);
        }
    }

    public v(Context context) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        context.registerReceiver(new c(null), intentFilter);
    }

    public static void a(v vVar, int i) {
        synchronized (vVar.d) {
            if (vVar.e != i) {
                vVar.e = i;
                Iterator<WeakReference<b>> it = vVar.c.iterator();
                while (it.hasNext()) {
                    WeakReference<b> next = it.next();
                    b bVar = next.get();
                    if (bVar != null) {
                        bVar.a(i);
                    } else {
                        vVar.c.remove(next);
                    }
                }
            }
        }
    }
}
