package b.i.a.c.f3;

import android.util.Pair;
import java.lang.Throwable;
/* compiled from: ErrorMessageProvider.java */
/* loaded from: classes3.dex */
public interface m<T extends Throwable> {
    Pair<Integer, String> a(T t);
}
