package b.i.a.c.f3;

import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.Nullable;
import java.net.UnknownHostException;
import org.checkerframework.dataflow.qual.Pure;
/* compiled from: Log.java */
/* loaded from: classes3.dex */
public final class q {
    @Pure
    public static String a(String str, @Nullable Throwable th) {
        String str2;
        boolean z2;
        if (th == null) {
            str2 = null;
        } else {
            Throwable th2 = th;
            while (true) {
                if (th2 == null) {
                    z2 = false;
                    break;
                } else if (th2 instanceof UnknownHostException) {
                    z2 = true;
                    break;
                } else {
                    th2 = th2.getCause();
                }
            }
            str2 = z2 ? "UnknownHostException (no network)" : Log.getStackTraceString(th).trim().replace("\t", "    ");
        }
        if (TextUtils.isEmpty(str2)) {
            return str;
        }
        String valueOf = String.valueOf(str);
        String replace = str2.replace("\n", "\n  ");
        StringBuilder sb = new StringBuilder(String.valueOf(replace).length() + valueOf.length() + 4);
        sb.append(valueOf);
        sb.append("\n  ");
        sb.append(replace);
        sb.append('\n');
        return sb.toString();
    }

    @Pure
    public static void b(String str, String str2, @Nullable Throwable th) {
        Log.e(str, a(str2, th));
    }

    @Pure
    public static void c(String str, String str2, @Nullable Throwable th) {
        Log.w(str, a(str2, th));
    }
}
