package b.i.a.c.f3;
/* compiled from: ConditionVariable.java */
/* loaded from: classes3.dex */
public class j {
    public final g a = g.a;

    /* renamed from: b  reason: collision with root package name */
    public boolean f967b;

    public synchronized boolean a() {
        boolean z2;
        z2 = this.f967b;
        this.f967b = false;
        return z2;
    }

    public synchronized boolean b() {
        if (this.f967b) {
            return false;
        }
        this.f967b = true;
        notifyAll();
        return true;
    }
}
