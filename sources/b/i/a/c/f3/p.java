package b.i.a.c.f3;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.f3.n;
import b.i.a.c.f3.p;
import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArraySet;
/* compiled from: ListenerSet.java */
/* loaded from: classes3.dex */
public final class p<T> {
    public final g a;

    /* renamed from: b  reason: collision with root package name */
    public final o f969b;
    public final b<T> c;
    public final CopyOnWriteArraySet<c<T>> d;
    public final ArrayDeque<Runnable> e = new ArrayDeque<>();
    public final ArrayDeque<Runnable> f = new ArrayDeque<>();
    public boolean g;

    /* compiled from: ListenerSet.java */
    /* loaded from: classes3.dex */
    public interface a<T> {
        void invoke(T t);
    }

    /* compiled from: ListenerSet.java */
    /* loaded from: classes3.dex */
    public interface b<T> {
        void a(T t, n nVar);
    }

    /* compiled from: ListenerSet.java */
    /* loaded from: classes3.dex */
    public static final class c<T> {
        public final T a;

        /* renamed from: b  reason: collision with root package name */
        public n.b f970b = new n.b();
        public boolean c;
        public boolean d;

        public c(T t) {
            this.a = t;
        }

        public boolean equals(@Nullable Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || c.class != obj.getClass()) {
                return false;
            }
            return this.a.equals(((c) obj).a);
        }

        public int hashCode() {
            return this.a.hashCode();
        }
    }

    public p(CopyOnWriteArraySet<c<T>> copyOnWriteArraySet, Looper looper, g gVar, b<T> bVar) {
        this.a = gVar;
        this.d = copyOnWriteArraySet;
        this.c = bVar;
        this.f969b = gVar.b(looper, new Handler.Callback() { // from class: b.i.a.c.f3.b
            @Override // android.os.Handler.Callback
            public final boolean handleMessage(Message message) {
                p pVar = p.this;
                Iterator it = pVar.d.iterator();
                while (it.hasNext()) {
                    p.c cVar = (p.c) it.next();
                    p.b<T> bVar2 = pVar.c;
                    if (!cVar.d && cVar.c) {
                        n b2 = cVar.f970b.b();
                        cVar.f970b = new n.b();
                        cVar.c = false;
                        bVar2.a(cVar.a, b2);
                    }
                    if (pVar.f969b.e(0)) {
                        return true;
                    }
                }
                return true;
            }
        });
    }

    public void a() {
        if (!this.f.isEmpty()) {
            if (!this.f969b.e(0)) {
                o oVar = this.f969b;
                oVar.d(oVar.c(0));
            }
            boolean z2 = !this.e.isEmpty();
            this.e.addAll(this.f);
            this.f.clear();
            if (!z2) {
                while (!this.e.isEmpty()) {
                    this.e.peekFirst().run();
                    this.e.removeFirst();
                }
            }
        }
    }

    public void b(final int i, final a<T> aVar) {
        final CopyOnWriteArraySet copyOnWriteArraySet = new CopyOnWriteArraySet(this.d);
        this.f.add(new Runnable() { // from class: b.i.a.c.f3.a
            @Override // java.lang.Runnable
            public final void run() {
                CopyOnWriteArraySet copyOnWriteArraySet2 = copyOnWriteArraySet;
                int i2 = i;
                p.a aVar2 = aVar;
                Iterator it = copyOnWriteArraySet2.iterator();
                while (it.hasNext()) {
                    p.c cVar = (p.c) it.next();
                    if (!cVar.d) {
                        if (i2 != -1) {
                            n.b bVar = cVar.f970b;
                            d.D(!bVar.f968b);
                            bVar.a.append(i2, true);
                        }
                        cVar.c = true;
                        aVar2.invoke(cVar.a);
                    }
                }
            }
        });
    }

    public void c() {
        Iterator<c<T>> it = this.d.iterator();
        while (it.hasNext()) {
            c<T> next = it.next();
            b<T> bVar = this.c;
            next.d = true;
            if (next.c) {
                bVar.a(next.a, next.f970b.b());
            }
        }
        this.d.clear();
        this.g = true;
    }
}
