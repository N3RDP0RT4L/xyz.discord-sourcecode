package b.i.a.c.a3;

import b.i.a.c.k1;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import java.io.IOException;
/* compiled from: SampleStream.java */
/* loaded from: classes3.dex */
public interface i0 {
    int a(k1 k1Var, DecoderInputBuffer decoderInputBuffer, int i);

    void b() throws IOException;

    int c(long j);

    boolean d();
}
