package b.i.a.c.a3;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import b.c.a.a0.d;
import b.i.a.c.a3.x;
import b.i.a.c.f3.e0;
import b.i.a.c.j1;
import b.i.a.c.j2;
import b.i.a.c.k1;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import java.io.IOException;
import java.util.Objects;
/* compiled from: ClippingMediaPeriod.java */
/* loaded from: classes3.dex */
public final class n implements x, x.a {
    public final x j;
    @Nullable
    public x.a k;
    public a[] l = new a[0];
    public long m;
    public long n;
    public long o;

    /* compiled from: ClippingMediaPeriod.java */
    /* loaded from: classes3.dex */
    public final class a implements i0 {
        public final i0 a;

        /* renamed from: b  reason: collision with root package name */
        public boolean f826b;

        public a(i0 i0Var) {
            this.a = i0Var;
        }

        @Override // b.i.a.c.a3.i0
        public int a(k1 k1Var, DecoderInputBuffer decoderInputBuffer, int i) {
            if (n.this.d()) {
                return -3;
            }
            if (this.f826b) {
                decoderInputBuffer.j = 4;
                return -4;
            }
            int a = this.a.a(k1Var, decoderInputBuffer, i);
            if (a == -5) {
                j1 j1Var = k1Var.f1023b;
                Objects.requireNonNull(j1Var);
                int i2 = j1Var.M;
                if (!(i2 == 0 && j1Var.N == 0)) {
                    n nVar = n.this;
                    int i3 = 0;
                    if (nVar.n != 0) {
                        i2 = 0;
                    }
                    if (nVar.o == Long.MIN_VALUE) {
                        i3 = j1Var.N;
                    }
                    j1.b a2 = j1Var.a();
                    a2.A = i2;
                    a2.B = i3;
                    k1Var.f1023b = a2.a();
                }
                return -5;
            }
            n nVar2 = n.this;
            long j = nVar2.o;
            if (j == Long.MIN_VALUE || ((a != -4 || decoderInputBuffer.n < j) && (a != -3 || nVar2.q() != Long.MIN_VALUE || decoderInputBuffer.m))) {
                return a;
            }
            decoderInputBuffer.p();
            decoderInputBuffer.j = 4;
            this.f826b = true;
            return -4;
        }

        @Override // b.i.a.c.a3.i0
        public void b() throws IOException {
            this.a.b();
        }

        @Override // b.i.a.c.a3.i0
        public int c(long j) {
            if (n.this.d()) {
                return -3;
            }
            return this.a.c(j);
        }

        @Override // b.i.a.c.a3.i0
        public boolean d() {
            return !n.this.d() && this.a.d();
        }
    }

    public n(x xVar, boolean z2, long j, long j2) {
        this.j = xVar;
        this.m = z2 ? j : -9223372036854775807L;
        this.n = j;
        this.o = j2;
    }

    @Override // b.i.a.c.a3.j0.a
    public void a(x xVar) {
        x.a aVar = this.k;
        Objects.requireNonNull(aVar);
        aVar.a(this);
    }

    @Override // b.i.a.c.a3.x.a
    public void b(x xVar) {
        x.a aVar = this.k;
        Objects.requireNonNull(aVar);
        aVar.b(this);
    }

    @Override // b.i.a.c.a3.x
    public long c() {
        long c = this.j.c();
        if (c != Long.MIN_VALUE) {
            long j = this.o;
            if (j == Long.MIN_VALUE || c < j) {
                return c;
            }
        }
        return Long.MIN_VALUE;
    }

    public boolean d() {
        return this.m != -9223372036854775807L;
    }

    @Override // b.i.a.c.a3.x
    public void e() throws IOException {
        this.j.e();
    }

    /* JADX WARN: Code restructure failed: missing block: B:15:0x0031, code lost:
        if (r0 > r7) goto L17;
     */
    @Override // b.i.a.c.a3.x
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public long f(long r7) {
        /*
            r6 = this;
            r0 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            r6.m = r0
            b.i.a.c.a3.n$a[] r0 = r6.l
            int r1 = r0.length
            r2 = 0
            r3 = 0
        Lc:
            if (r3 >= r1) goto L17
            r4 = r0[r3]
            if (r4 == 0) goto L14
            r4.f826b = r2
        L14:
            int r3 = r3 + 1
            goto Lc
        L17:
            b.i.a.c.a3.x r0 = r6.j
            long r0 = r0.f(r7)
            int r3 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r3 == 0) goto L33
            long r7 = r6.n
            int r3 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r3 < 0) goto L34
            long r7 = r6.o
            r3 = -9223372036854775808
            int r5 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r5 == 0) goto L33
            int r3 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r3 > 0) goto L34
        L33:
            r2 = 1
        L34:
            b.c.a.a0.d.D(r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.a3.n.f(long):long");
    }

    @Override // b.i.a.c.a3.x
    public boolean g(long j) {
        return this.j.g(j);
    }

    @Override // b.i.a.c.a3.x
    public boolean h() {
        return this.j.h();
    }

    @Override // b.i.a.c.a3.x
    public long i(long j, j2 j2Var) {
        long j2 = this.n;
        if (j == j2) {
            return j2;
        }
        long i = e0.i(j2Var.c, 0L, j - j2);
        long j3 = j2Var.d;
        long j4 = this.o;
        long i2 = e0.i(j3, 0L, j4 == Long.MIN_VALUE ? RecyclerView.FOREVER_NS : j4 - j);
        if (!(i == j2Var.c && i2 == j2Var.d)) {
            j2Var = new j2(i, i2);
        }
        return this.j.i(j, j2Var);
    }

    @Override // b.i.a.c.a3.x
    public long k() {
        if (d()) {
            long j = this.m;
            this.m = -9223372036854775807L;
            long k = k();
            return k != -9223372036854775807L ? k : j;
        }
        long k2 = this.j.k();
        if (k2 == -9223372036854775807L) {
            return -9223372036854775807L;
        }
        boolean z2 = true;
        d.D(k2 >= this.n);
        long j2 = this.o;
        if (j2 != Long.MIN_VALUE && k2 > j2) {
            z2 = false;
        }
        d.D(z2);
        return k2;
    }

    @Override // b.i.a.c.a3.x
    public void l(x.a aVar, long j) {
        this.k = aVar;
        this.j.l(this, j);
    }

    /* JADX WARN: Code restructure failed: missing block: B:34:0x0085, code lost:
        if (r1 > r5) goto L35;
     */
    /* JADX WARN: Removed duplicated region for block: B:29:0x0075  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x008f  */
    @Override // b.i.a.c.a3.x
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public long m(b.i.a.c.c3.j[] r16, boolean[] r17, b.i.a.c.a3.i0[] r18, boolean[] r19, long r20) {
        /*
            r15 = this;
            r0 = r15
            r8 = r16
            r9 = r18
            int r1 = r9.length
            b.i.a.c.a3.n$a[] r1 = new b.i.a.c.a3.n.a[r1]
            r0.l = r1
            int r1 = r9.length
            b.i.a.c.a3.i0[] r10 = new b.i.a.c.a3.i0[r1]
            r11 = 0
            r1 = 0
        Lf:
            int r2 = r9.length
            r12 = 0
            if (r1 >= r2) goto L28
            b.i.a.c.a3.n$a[] r2 = r0.l
            r3 = r9[r1]
            b.i.a.c.a3.n$a r3 = (b.i.a.c.a3.n.a) r3
            r2[r1] = r3
            r3 = r2[r1]
            if (r3 == 0) goto L23
            r2 = r2[r1]
            b.i.a.c.a3.i0 r12 = r2.a
        L23:
            r10[r1] = r12
            int r1 = r1 + 1
            goto Lf
        L28:
            b.i.a.c.a3.x r1 = r0.j
            r2 = r16
            r3 = r17
            r4 = r10
            r5 = r19
            r6 = r20
            long r1 = r1.m(r2, r3, r4, r5, r6)
            boolean r3 = r15.d()
            r4 = 1
            if (r3 == 0) goto L6a
            long r5 = r0.n
            int r3 = (r20 > r5 ? 1 : (r20 == r5 ? 0 : -1))
            if (r3 != 0) goto L6a
            r13 = 0
            int r3 = (r5 > r13 ? 1 : (r5 == r13 ? 0 : -1))
            if (r3 == 0) goto L65
            int r3 = r8.length
            r5 = 0
        L4c:
            if (r5 >= r3) goto L65
            r6 = r8[r5]
            if (r6 == 0) goto L62
            b.i.a.c.j1 r6 = r6.h()
            java.lang.String r7 = r6.w
            java.lang.String r6 = r6.t
            boolean r6 = b.i.a.c.f3.t.a(r7, r6)
            if (r6 != 0) goto L62
            r3 = 1
            goto L66
        L62:
            int r5 = r5 + 1
            goto L4c
        L65:
            r3 = 0
        L66:
            if (r3 == 0) goto L6a
            r5 = r1
            goto L6f
        L6a:
            r5 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
        L6f:
            r0.m = r5
            int r3 = (r1 > r20 ? 1 : (r1 == r20 ? 0 : -1))
            if (r3 == 0) goto L89
            long r5 = r0.n
            int r3 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r3 < 0) goto L88
            long r5 = r0.o
            r7 = -9223372036854775808
            int r3 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r3 == 0) goto L89
            int r3 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r3 > 0) goto L88
            goto L89
        L88:
            r4 = 0
        L89:
            b.c.a.a0.d.D(r4)
        L8c:
            int r3 = r9.length
            if (r11 >= r3) goto Lb8
            r3 = r10[r11]
            if (r3 != 0) goto L98
            b.i.a.c.a3.n$a[] r3 = r0.l
            r3[r11] = r12
            goto Laf
        L98:
            b.i.a.c.a3.n$a[] r3 = r0.l
            r4 = r3[r11]
            if (r4 == 0) goto La6
            r4 = r3[r11]
            b.i.a.c.a3.i0 r4 = r4.a
            r5 = r10[r11]
            if (r4 == r5) goto Laf
        La6:
            b.i.a.c.a3.n$a r4 = new b.i.a.c.a3.n$a
            r5 = r10[r11]
            r4.<init>(r5)
            r3[r11] = r4
        Laf:
            b.i.a.c.a3.n$a[] r3 = r0.l
            r3 = r3[r11]
            r9[r11] = r3
            int r11 = r11 + 1
            goto L8c
        Lb8:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.a3.n.m(b.i.a.c.c3.j[], boolean[], b.i.a.c.a3.i0[], boolean[], long):long");
    }

    @Override // b.i.a.c.a3.x
    public o0 n() {
        return this.j.n();
    }

    @Override // b.i.a.c.a3.x
    public long q() {
        long q = this.j.q();
        if (q != Long.MIN_VALUE) {
            long j = this.o;
            if (j == Long.MIN_VALUE || q < j) {
                return q;
            }
        }
        return Long.MIN_VALUE;
    }

    @Override // b.i.a.c.a3.x
    public void r(long j, boolean z2) {
        this.j.r(j, z2);
    }

    @Override // b.i.a.c.a3.x
    public void s(long j) {
        this.j.s(j);
    }
}
