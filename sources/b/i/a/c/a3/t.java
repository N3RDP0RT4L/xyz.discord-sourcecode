package b.i.a.c.a3;

import android.net.Uri;
import b.i.a.c.e3.n;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
/* compiled from: LoadEventInfo.java */
/* loaded from: classes3.dex */
public final class t {
    public static final AtomicLong a = new AtomicLong();

    /* renamed from: b  reason: collision with root package name */
    public final Map<String, List<String>> f829b;

    public t(long j, n nVar, long j2) {
        Uri uri = nVar.a;
        this.f829b = Collections.emptyMap();
    }

    public t(long j, n nVar, Uri uri, Map<String, List<String>> map, long j2, long j3, long j4) {
        this.f829b = map;
    }
}
