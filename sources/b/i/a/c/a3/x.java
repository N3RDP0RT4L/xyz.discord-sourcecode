package b.i.a.c.a3;

import b.i.a.c.a3.j0;
import b.i.a.c.c3.j;
import b.i.a.c.j2;
import java.io.IOException;
/* compiled from: MediaPeriod.java */
/* loaded from: classes3.dex */
public interface x extends j0 {

    /* compiled from: MediaPeriod.java */
    /* loaded from: classes3.dex */
    public interface a extends j0.a<x> {
        void b(x xVar);
    }

    long c();

    void e() throws IOException;

    long f(long j);

    boolean g(long j);

    boolean h();

    long i(long j, j2 j2Var);

    long k();

    void l(a aVar, long j);

    long m(j[] jVarArr, boolean[] zArr, i0[] i0VarArr, boolean[] zArr2, long j);

    o0 n();

    long q();

    void r(long j, boolean z2);

    void s(long j);
}
