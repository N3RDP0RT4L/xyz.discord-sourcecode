package b.i.a.c.a3;

import android.net.Uri;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.e3.a0;
import b.i.a.c.e3.l;
import b.i.a.c.e3.n;
import java.util.List;
import java.util.Map;
import java.util.Objects;
/* compiled from: IcyDataSource.java */
/* loaded from: classes3.dex */
public final class s implements l {
    public final l a;

    /* renamed from: b  reason: collision with root package name */
    public final int f828b;
    public final a c;
    public final byte[] d;
    public int e;

    /* compiled from: IcyDataSource.java */
    /* loaded from: classes3.dex */
    public interface a {
    }

    public s(l lVar, int i, a aVar) {
        d.j(i > 0);
        this.a = lVar;
        this.f828b = i;
        this.c = aVar;
        this.d = new byte[1];
        this.e = i;
    }

    @Override // b.i.a.c.e3.l
    public long a(n nVar) {
        throw new UnsupportedOperationException();
    }

    @Override // b.i.a.c.e3.l
    public void close() {
        throw new UnsupportedOperationException();
    }

    @Override // b.i.a.c.e3.l
    public void d(a0 a0Var) {
        Objects.requireNonNull(a0Var);
        this.a.d(a0Var);
    }

    @Override // b.i.a.c.e3.l
    public Map<String, List<String>> j() {
        return this.a.j();
    }

    @Override // b.i.a.c.e3.l
    @Nullable
    public Uri n() {
        return this.a.n();
    }

    /* JADX WARN: Removed duplicated region for block: B:25:0x0073  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x0078 A[RETURN] */
    @Override // b.i.a.c.e3.h
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public int read(byte[] r17, int r18, int r19) throws java.io.IOException {
        /*
            r16 = this;
            r0 = r16
            int r1 = r0.e
            r2 = -1
            if (r1 != 0) goto L79
            b.i.a.c.e3.l r1 = r0.a
            byte[] r3 = r0.d
            r4 = 1
            r5 = 0
            int r1 = r1.read(r3, r5, r4)
            if (r1 != r2) goto L15
        L13:
            r4 = 0
            goto L71
        L15:
            byte[] r1 = r0.d
            r1 = r1[r5]
            r1 = r1 & 255(0xff, float:3.57E-43)
            int r1 = r1 << 4
            if (r1 != 0) goto L20
            goto L71
        L20:
            byte[] r3 = new byte[r1]
            r6 = r1
            r7 = 0
        L24:
            if (r6 <= 0) goto L32
            b.i.a.c.e3.l r8 = r0.a
            int r8 = r8.read(r3, r7, r6)
            if (r8 != r2) goto L2f
            goto L13
        L2f:
            int r7 = r7 + r8
            int r6 = r6 - r8
            goto L24
        L32:
            if (r1 <= 0) goto L3c
            int r5 = r1 + (-1)
            r6 = r3[r5]
            if (r6 != 0) goto L3c
            r1 = r5
            goto L32
        L3c:
            if (r1 <= 0) goto L71
            b.i.a.c.a3.s$a r5 = r0.c
            b.i.a.c.f3.x r6 = new b.i.a.c.f3.x
            r6.<init>(r3, r1)
            b.i.a.c.a3.e0$a r5 = (b.i.a.c.a3.e0.a) r5
            boolean r1 = r5.n
            if (r1 != 0) goto L4e
            long r7 = r5.j
            goto L5c
        L4e:
            b.i.a.c.a3.e0 r1 = b.i.a.c.a3.e0.this
            java.util.Map<java.lang.String, java.lang.String> r3 = b.i.a.c.a3.e0.j
            long r7 = r1.v()
            long r9 = r5.j
            long r7 = java.lang.Math.max(r7, r9)
        L5c:
            r10 = r7
            int r13 = r6.a()
            b.i.a.c.x2.w r9 = r5.m
            java.util.Objects.requireNonNull(r9)
            r9.c(r6, r13)
            r12 = 1
            r14 = 0
            r15 = 0
            r9.d(r10, r12, r13, r14, r15)
            r5.n = r4
        L71:
            if (r4 == 0) goto L78
            int r1 = r0.f828b
            r0.e = r1
            goto L79
        L78:
            return r2
        L79:
            b.i.a.c.e3.l r1 = r0.a
            int r3 = r0.e
            r4 = r19
            int r3 = java.lang.Math.min(r3, r4)
            r4 = r17
            r5 = r18
            int r1 = r1.read(r4, r5, r3)
            if (r1 == r2) goto L92
            int r2 = r0.e
            int r2 = r2 - r1
            r0.e = r2
        L92:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.a3.s.read(byte[], int, int):int");
    }
}
