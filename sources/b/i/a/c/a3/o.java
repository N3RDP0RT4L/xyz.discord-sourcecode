package b.i.a.c.a3;

import android.os.Handler;
import androidx.annotation.CallSuper;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.a3.a0;
import b.i.a.c.a3.b0;
import b.i.a.c.a3.v;
import b.i.a.c.e3.a0;
import b.i.a.c.f3.e0;
import b.i.a.c.w2.s;
import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;
/* compiled from: CompositeMediaSource.java */
/* loaded from: classes3.dex */
public abstract class o<T> extends l {
    public final HashMap<T, b<T>> g = new HashMap<>();
    @Nullable
    public Handler h;
    @Nullable
    public a0 i;

    /* compiled from: CompositeMediaSource.java */
    /* loaded from: classes3.dex */
    public final class a implements b0, s {
        public final T j;
        public b0.a k;
        public s.a l;

        public a(T t) {
            this.k = o.this.c.g(0, null, 0L);
            this.l = o.this.d.g(0, null);
            this.j = t;
        }

        @Override // b.i.a.c.w2.s
        public void J(int i, @Nullable a0.a aVar) {
            a(i, aVar);
            this.l.b();
        }

        @Override // b.i.a.c.w2.s
        public void S(int i, @Nullable a0.a aVar) {
            a(i, aVar);
            this.l.a();
        }

        @Override // b.i.a.c.a3.b0
        public void X(int i, @Nullable a0.a aVar, t tVar, w wVar) {
            a(i, aVar);
            this.k.d(tVar, b(wVar));
        }

        public final boolean a(int i, @Nullable a0.a aVar) {
            a0.a aVar2;
            if (aVar != null) {
                o oVar = o.this;
                T t = this.j;
                v vVar = (v) oVar;
                Objects.requireNonNull(vVar);
                Void r1 = (Void) t;
                Object obj = aVar.a;
                Object obj2 = vVar.n.n;
                if (obj2 != null && obj2.equals(obj)) {
                    obj = v.a.l;
                }
                aVar2 = aVar.b(obj);
            } else {
                aVar2 = null;
            }
            Objects.requireNonNull(o.this);
            b0.a aVar3 = this.k;
            if (aVar3.a != i || !e0.a(aVar3.f803b, aVar2)) {
                this.k = o.this.c.g(i, aVar2, 0L);
            }
            s.a aVar4 = this.l;
            if (aVar4.a == i && e0.a(aVar4.f1152b, aVar2)) {
                return true;
            }
            this.l = new s.a(o.this.d.c, i, aVar2);
            return true;
        }

        public final w b(w wVar) {
            o oVar = o.this;
            long j = wVar.f;
            Objects.requireNonNull(oVar);
            o oVar2 = o.this;
            long j2 = wVar.g;
            Objects.requireNonNull(oVar2);
            return (j == wVar.f && j2 == wVar.g) ? wVar : new w(wVar.a, wVar.f830b, wVar.c, wVar.d, wVar.e, j, j2);
        }

        @Override // b.i.a.c.w2.s
        public void c0(int i, @Nullable a0.a aVar, int i2) {
            a(i, aVar);
            this.l.d(i2);
        }

        @Override // b.i.a.c.w2.s
        public void d0(int i, @Nullable a0.a aVar) {
            a(i, aVar);
            this.l.f();
        }

        @Override // b.i.a.c.a3.b0
        public void g0(int i, @Nullable a0.a aVar, t tVar, w wVar, IOException iOException, boolean z2) {
            a(i, aVar);
            this.k.e(tVar, b(wVar), iOException, z2);
        }

        @Override // b.i.a.c.w2.s
        public void i0(int i, @Nullable a0.a aVar) {
            a(i, aVar);
            this.l.c();
        }

        @Override // b.i.a.c.a3.b0
        public void o(int i, @Nullable a0.a aVar, w wVar) {
            a(i, aVar);
            this.k.b(b(wVar));
        }

        @Override // b.i.a.c.a3.b0
        public void q(int i, @Nullable a0.a aVar, t tVar, w wVar) {
            a(i, aVar);
            this.k.c(tVar, b(wVar));
        }

        @Override // b.i.a.c.w2.s
        public void u(int i, @Nullable a0.a aVar, Exception exc) {
            a(i, aVar);
            this.l.e(exc);
        }

        @Override // b.i.a.c.a3.b0
        public void x(int i, @Nullable a0.a aVar, t tVar, w wVar) {
            a(i, aVar);
            this.k.f(tVar, b(wVar));
        }
    }

    /* compiled from: CompositeMediaSource.java */
    /* loaded from: classes3.dex */
    public static final class b<T> {
        public final a0 a;

        /* renamed from: b  reason: collision with root package name */
        public final a0.b f827b;
        public final o<T>.a c;

        public b(a0 a0Var, a0.b bVar, o<T>.a aVar) {
            this.a = a0Var;
            this.f827b = bVar;
            this.c = aVar;
        }
    }

    @Override // b.i.a.c.a3.l
    @CallSuper
    public void o() {
        for (b<T> bVar : this.g.values()) {
            bVar.a.d(bVar.f827b);
        }
    }

    @Override // b.i.a.c.a3.l
    @CallSuper
    public void p() {
        for (b<T> bVar : this.g.values()) {
            bVar.a.m(bVar.f827b);
        }
    }

    public final void t(T t, a0 a0Var) {
        d.j(!this.g.containsKey(null));
        a0.b aVar = new a0.b() { // from class: b.i.a.c.a3.a
            /* JADX WARN: Removed duplicated region for block: B:22:0x0097  */
            /* JADX WARN: Removed duplicated region for block: B:23:0x00a3  */
            /* JADX WARN: Removed duplicated region for block: B:26:0x00af  */
            /* JADX WARN: Removed duplicated region for block: B:35:0x00da  */
            /* JADX WARN: Removed duplicated region for block: B:37:? A[RETURN, SYNTHETIC] */
            @Override // b.i.a.c.a3.a0.b
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public final void a(b.i.a.c.a3.a0 r11, b.i.a.c.o2 r12) {
                /*
                    Method dump skipped, instructions count: 227
                    To view this dump add '--comments-level debug' option
                */
                throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.a3.a.a(b.i.a.c.a3.a0, b.i.a.c.o2):void");
            }
        };
        a aVar2 = new a(null);
        this.g.put(null, new b<>(a0Var, aVar, aVar2));
        Handler handler = this.h;
        Objects.requireNonNull(handler);
        a0Var.b(handler, aVar2);
        Handler handler2 = this.h;
        Objects.requireNonNull(handler2);
        a0Var.f(handler2, aVar2);
        a0Var.l(aVar, this.i);
        if (!(!this.f823b.isEmpty())) {
            a0Var.d(aVar);
        }
    }
}
