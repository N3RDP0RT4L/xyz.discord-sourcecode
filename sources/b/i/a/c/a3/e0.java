package b.i.a.c.a3;

import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import b.i.a.c.a3.b0;
import b.i.a.c.a3.h0;
import b.i.a.c.a3.s;
import b.i.a.c.a3.x;
import b.i.a.c.e3.h;
import b.i.a.c.e3.l;
import b.i.a.c.e3.n;
import b.i.a.c.e3.o;
import b.i.a.c.e3.w;
import b.i.a.c.e3.y;
import b.i.a.c.j1;
import b.i.a.c.j2;
import b.i.a.c.k1;
import b.i.a.c.w2.s;
import b.i.a.c.w2.u;
import b.i.a.c.x2.h0.f;
import b.i.a.c.x2.i;
import b.i.a.c.x2.j;
import b.i.a.c.x2.t;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.drm.DrmSession;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.icy.IcyHeaders;
import com.google.android.exoplayer2.upstream.Loader;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.checkerframework.checker.nullness.qual.EnsuresNonNull;
/* compiled from: ProgressiveMediaPeriod.java */
/* loaded from: classes3.dex */
public final class e0 implements x, j, Loader.b<a>, Loader.f, h0.d {
    public static final Map<String, String> j;
    public static final j1 k;
    @Nullable
    public x.a B;
    @Nullable
    public IcyHeaders C;
    public boolean F;
    public boolean G;
    public boolean H;
    public e I;
    public t J;
    public boolean L;
    public boolean N;
    public boolean O;
    public int P;
    public long R;
    public boolean T;
    public int U;
    public boolean V;
    public boolean W;
    public final Uri l;
    public final l m;
    public final u n;
    public final w o;
    public final b0.a p;
    public final s.a q;
    public final b r;

    /* renamed from: s  reason: collision with root package name */
    public final o f805s;
    @Nullable
    public final String t;
    public final long u;
    public final d0 w;
    public final Loader v = new Loader("ProgressiveMediaPeriod");

    /* renamed from: x  reason: collision with root package name */
    public final b.i.a.c.f3.j f806x = new b.i.a.c.f3.j();

    /* renamed from: y  reason: collision with root package name */
    public final Runnable f807y = new Runnable() { // from class: b.i.a.c.a3.g
        @Override // java.lang.Runnable
        public final void run() {
            e0.this.x();
        }
    };

    /* renamed from: z  reason: collision with root package name */
    public final Runnable f808z = new Runnable() { // from class: b.i.a.c.a3.i
        @Override // java.lang.Runnable
        public final void run() {
            e0 e0Var = e0.this;
            if (!e0Var.W) {
                x.a aVar = e0Var.B;
                Objects.requireNonNull(aVar);
                aVar.a(e0Var);
            }
        }
    };
    public final Handler A = b.i.a.c.f3.e0.j();
    public d[] E = new d[0];
    public h0[] D = new h0[0];
    public long S = -9223372036854775807L;
    public long Q = -1;
    public long K = -9223372036854775807L;
    public int M = 1;

    /* compiled from: ProgressiveMediaPeriod.java */
    /* loaded from: classes3.dex */
    public final class a implements Loader.e, s.a {

        /* renamed from: b  reason: collision with root package name */
        public final Uri f809b;
        public final y c;
        public final d0 d;
        public final j e;
        public final b.i.a.c.f3.j f;
        public volatile boolean h;
        public long j;
        @Nullable
        public b.i.a.c.x2.w m;
        public boolean n;
        public final b.i.a.c.x2.s g = new b.i.a.c.x2.s();
        public boolean i = true;
        public long l = -1;
        public final long a = t.a.getAndIncrement();
        public n k = a(0);

        public a(Uri uri, l lVar, d0 d0Var, j jVar, b.i.a.c.f3.j jVar2) {
            this.f809b = uri;
            this.c = new y(lVar);
            this.d = d0Var;
            this.e = jVar;
            this.f = jVar2;
        }

        public final n a(long j) {
            Collections.emptyMap();
            Uri uri = this.f809b;
            String str = e0.this.t;
            Map<String, String> map = e0.j;
            if (uri != null) {
                return new n(uri, 0L, 1, null, map, j, -1L, str, 6, null);
            }
            throw new IllegalStateException("The uri must be set.");
        }

        public void b() throws IOException {
            h hVar;
            int i;
            int i2 = 0;
            while (i2 == 0 && !this.h) {
                try {
                    long j = this.g.a;
                    n a = a(j);
                    this.k = a;
                    long a2 = this.c.a(a);
                    this.l = a2;
                    if (a2 != -1) {
                        this.l = a2 + j;
                    }
                    e0.this.C = IcyHeaders.a(this.c.j());
                    y yVar = this.c;
                    IcyHeaders icyHeaders = e0.this.C;
                    if (icyHeaders == null || (i = icyHeaders.o) == -1) {
                        hVar = yVar;
                    } else {
                        hVar = new s(yVar, i, this);
                        b.i.a.c.x2.w B = e0.this.B(new d(0, true));
                        this.m = B;
                        ((h0) B).e(e0.k);
                    }
                    long j2 = j;
                    ((m) this.d).b(hVar, this.f809b, this.c.j(), j, this.l, this.e);
                    if (e0.this.C != null) {
                        b.i.a.c.x2.h hVar2 = ((m) this.d).f824b;
                        if (hVar2 instanceof f) {
                            ((f) hVar2).f1202s = true;
                        }
                    }
                    if (this.i) {
                        d0 d0Var = this.d;
                        long j3 = this.j;
                        b.i.a.c.x2.h hVar3 = ((m) d0Var).f824b;
                        Objects.requireNonNull(hVar3);
                        hVar3.g(j2, j3);
                        this.i = false;
                    }
                    while (true) {
                        long j4 = j2;
                        while (i2 == 0 && !this.h) {
                            try {
                                b.i.a.c.f3.j jVar = this.f;
                                synchronized (jVar) {
                                    while (!jVar.f967b) {
                                        jVar.wait();
                                    }
                                }
                                d0 d0Var2 = this.d;
                                b.i.a.c.x2.s sVar = this.g;
                                m mVar = (m) d0Var2;
                                b.i.a.c.x2.h hVar4 = mVar.f824b;
                                Objects.requireNonNull(hVar4);
                                i iVar = mVar.c;
                                Objects.requireNonNull(iVar);
                                i2 = hVar4.e(iVar, sVar);
                                j2 = ((m) this.d).a();
                                if (j2 > e0.this.u + j4) {
                                    break;
                                }
                            } catch (InterruptedException unused) {
                                throw new InterruptedIOException();
                            }
                        }
                        this.f.a();
                        e0 e0Var = e0.this;
                        e0Var.A.post(e0Var.f808z);
                    }
                    if (i2 == 1) {
                        i2 = 0;
                    } else if (((m) this.d).a() != -1) {
                        this.g.a = ((m) this.d).a();
                    }
                    y yVar2 = this.c;
                    if (yVar2 != null) {
                        try {
                            yVar2.a.close();
                        } catch (IOException unused2) {
                        }
                    }
                } catch (Throwable th) {
                    if (!(i2 == 1 || ((m) this.d).a() == -1)) {
                        this.g.a = ((m) this.d).a();
                    }
                    y yVar3 = this.c;
                    if (yVar3 != null) {
                        try {
                            yVar3.a.close();
                        } catch (IOException unused3) {
                        }
                    }
                    throw th;
                }
            }
        }
    }

    /* compiled from: ProgressiveMediaPeriod.java */
    /* loaded from: classes3.dex */
    public interface b {
    }

    /* compiled from: ProgressiveMediaPeriod.java */
    /* loaded from: classes3.dex */
    public final class c implements i0 {
        public final int a;

        public c(int i) {
            this.a = i;
        }

        @Override // b.i.a.c.a3.i0
        public int a(k1 k1Var, DecoderInputBuffer decoderInputBuffer, int i) {
            int i2;
            e0 e0Var = e0.this;
            int i3 = this.a;
            if (e0Var.D()) {
                return -3;
            }
            e0Var.y(i3);
            h0 h0Var = e0Var.D[i3];
            boolean z2 = e0Var.V;
            boolean z3 = false;
            boolean z4 = (i & 2) != 0;
            h0.b bVar = h0Var.f815b;
            synchronized (h0Var) {
                decoderInputBuffer.m = false;
                i2 = -5;
                if (!h0Var.o()) {
                    if (!z2 && !h0Var.f817x) {
                        j1 j1Var = h0Var.B;
                        if (j1Var == null || (!z4 && j1Var == h0Var.h)) {
                            i2 = -3;
                        } else {
                            h0Var.r(j1Var, k1Var);
                        }
                    }
                    decoderInputBuffer.j = 4;
                    i2 = -4;
                } else {
                    j1 j1Var2 = h0Var.c.b(h0Var.k()).a;
                    if (!z4 && j1Var2 == h0Var.h) {
                        int l = h0Var.l(h0Var.t);
                        if (!h0Var.q(l)) {
                            decoderInputBuffer.m = true;
                            i2 = -3;
                        } else {
                            decoderInputBuffer.j = h0Var.n[l];
                            long j = h0Var.o[l];
                            decoderInputBuffer.n = j;
                            if (j < h0Var.u) {
                                decoderInputBuffer.j(Integer.MIN_VALUE);
                            }
                            bVar.a = h0Var.m[l];
                            bVar.f820b = h0Var.l[l];
                            bVar.c = h0Var.p[l];
                            i2 = -4;
                        }
                    }
                    h0Var.r(j1Var2, k1Var);
                }
            }
            if (i2 == -4 && !decoderInputBuffer.n()) {
                if ((i & 1) != 0) {
                    z3 = true;
                }
                if ((i & 4) == 0) {
                    if (z3) {
                        g0 g0Var = h0Var.a;
                        g0.f(g0Var.e, decoderInputBuffer, h0Var.f815b, g0Var.c);
                    } else {
                        g0 g0Var2 = h0Var.a;
                        g0Var2.e = g0.f(g0Var2.e, decoderInputBuffer, h0Var.f815b, g0Var2.c);
                    }
                }
                if (!z3) {
                    h0Var.t++;
                }
            }
            if (i2 == -3) {
                e0Var.z(i3);
            }
            return i2;
        }

        @Override // b.i.a.c.a3.i0
        public void b() throws IOException {
            e0 e0Var = e0.this;
            h0 h0Var = e0Var.D[this.a];
            DrmSession drmSession = h0Var.i;
            if (drmSession == null || drmSession.getState() != 1) {
                e0Var.A();
                return;
            }
            DrmSession.DrmSessionException f = h0Var.i.f();
            Objects.requireNonNull(f);
            throw f;
        }

        @Override // b.i.a.c.a3.i0
        public int c(long j) {
            int i;
            e0 e0Var = e0.this;
            int i2 = this.a;
            boolean z2 = false;
            if (e0Var.D()) {
                return 0;
            }
            e0Var.y(i2);
            h0 h0Var = e0Var.D[i2];
            boolean z3 = e0Var.V;
            synchronized (h0Var) {
                int l = h0Var.l(h0Var.t);
                if (h0Var.o() && j >= h0Var.o[l]) {
                    if (j <= h0Var.w || !z3) {
                        i = h0Var.i(l, h0Var.q - h0Var.t, j, true);
                        if (i == -1) {
                            i = 0;
                        }
                    } else {
                        i = h0Var.q - h0Var.t;
                    }
                }
                i = 0;
            }
            synchronized (h0Var) {
                if (i >= 0) {
                    if (h0Var.t + i <= h0Var.q) {
                        z2 = true;
                    }
                }
                b.c.a.a0.d.j(z2);
                h0Var.t += i;
            }
            if (i == 0) {
                e0Var.z(i2);
            }
            return i;
        }

        @Override // b.i.a.c.a3.i0
        public boolean d() {
            e0 e0Var = e0.this;
            return !e0Var.D() && e0Var.D[this.a].p(e0Var.V);
        }
    }

    /* compiled from: ProgressiveMediaPeriod.java */
    /* loaded from: classes3.dex */
    public static final class d {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f811b;

        public d(int i, boolean z2) {
            this.a = i;
            this.f811b = z2;
        }

        public boolean equals(@Nullable Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || d.class != obj.getClass()) {
                return false;
            }
            d dVar = (d) obj;
            return this.a == dVar.a && this.f811b == dVar.f811b;
        }

        public int hashCode() {
            return (this.a * 31) + (this.f811b ? 1 : 0);
        }
    }

    /* compiled from: ProgressiveMediaPeriod.java */
    /* loaded from: classes3.dex */
    public static final class e {
        public final o0 a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean[] f812b;
        public final boolean[] c;
        public final boolean[] d;

        public e(o0 o0Var, boolean[] zArr) {
            this.a = o0Var;
            this.f812b = zArr;
            int i = o0Var.k;
            this.c = new boolean[i];
            this.d = new boolean[i];
        }
    }

    static {
        HashMap hashMap = new HashMap();
        hashMap.put("Icy-MetaData", "1");
        j = Collections.unmodifiableMap(hashMap);
        j1.b bVar = new j1.b();
        bVar.a = "icy";
        bVar.k = "application/x-icy";
        k = bVar.a();
    }

    public e0(Uri uri, l lVar, d0 d0Var, u uVar, s.a aVar, w wVar, b0.a aVar2, b bVar, o oVar, @Nullable String str, int i) {
        this.l = uri;
        this.m = lVar;
        this.n = uVar;
        this.q = aVar;
        this.o = wVar;
        this.p = aVar2;
        this.r = bVar;
        this.f805s = oVar;
        this.t = str;
        this.u = i;
        this.w = d0Var;
    }

    public void A() throws IOException {
        Loader loader = this.v;
        int a2 = ((b.i.a.c.e3.s) this.o).a(this.M);
        IOException iOException = loader.e;
        if (iOException == null) {
            Loader.d<? extends Loader.e> dVar = loader.d;
            if (dVar != null) {
                if (a2 == Integer.MIN_VALUE) {
                    a2 = dVar.j;
                }
                IOException iOException2 = dVar.n;
                if (iOException2 != null && dVar.o > a2) {
                    throw iOException2;
                }
                return;
            }
            return;
        }
        throw iOException;
    }

    public final b.i.a.c.x2.w B(d dVar) {
        int length = this.D.length;
        for (int i = 0; i < length; i++) {
            if (dVar.equals(this.E[i])) {
                return this.D[i];
            }
        }
        o oVar = this.f805s;
        Looper looper = this.A.getLooper();
        u uVar = this.n;
        s.a aVar = this.q;
        Objects.requireNonNull(looper);
        Objects.requireNonNull(uVar);
        Objects.requireNonNull(aVar);
        h0 h0Var = new h0(oVar, looper, uVar, aVar);
        h0Var.g = this;
        int i2 = length + 1;
        d[] dVarArr = (d[]) Arrays.copyOf(this.E, i2);
        dVarArr[length] = dVar;
        int i3 = b.i.a.c.f3.e0.a;
        this.E = dVarArr;
        h0[] h0VarArr = (h0[]) Arrays.copyOf(this.D, i2);
        h0VarArr[length] = h0Var;
        this.D = h0VarArr;
        return h0Var;
    }

    public final void C() {
        a aVar = new a(this.l, this.m, this.w, this, this.f806x);
        if (this.G) {
            b.c.a.a0.d.D(w());
            long j2 = this.K;
            if (j2 == -9223372036854775807L || this.S <= j2) {
                t tVar = this.J;
                Objects.requireNonNull(tVar);
                long j3 = tVar.h(this.S).a.c;
                long j4 = this.S;
                aVar.g.a = j3;
                aVar.j = j4;
                aVar.i = true;
                aVar.n = false;
                for (h0 h0Var : this.D) {
                    h0Var.u = this.S;
                }
                this.S = -9223372036854775807L;
            } else {
                this.V = true;
                this.S = -9223372036854775807L;
                return;
            }
        }
        this.U = u();
        Loader loader = this.v;
        int a2 = ((b.i.a.c.e3.s) this.o).a(this.M);
        Objects.requireNonNull(loader);
        Looper myLooper = Looper.myLooper();
        b.c.a.a0.d.H(myLooper);
        loader.e = null;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        new Loader.d(myLooper, aVar, this, a2, elapsedRealtime).b(0L);
        n nVar = aVar.k;
        b0.a aVar2 = this.p;
        aVar2.f(new t(aVar.a, nVar, elapsedRealtime), new w(1, -1, null, 0, null, aVar2.a(aVar.j), aVar2.a(this.K)));
    }

    public final boolean D() {
        return this.O || w();
    }

    @Override // b.i.a.c.x2.j
    public void a(final t tVar) {
        this.A.post(new Runnable() { // from class: b.i.a.c.a3.h
            @Override // java.lang.Runnable
            public final void run() {
                e0 e0Var = e0.this;
                t tVar2 = tVar;
                e0Var.J = e0Var.C == null ? tVar2 : new t.b(-9223372036854775807L, 0L);
                e0Var.K = tVar2.i();
                int i = 1;
                boolean z2 = e0Var.Q == -1 && tVar2.i() == -9223372036854775807L;
                e0Var.L = z2;
                if (z2) {
                    i = 7;
                }
                e0Var.M = i;
                ((f0) e0Var.r).u(e0Var.K, tVar2.c(), e0Var.L);
                if (!e0Var.G) {
                    e0Var.x();
                }
            }
        });
    }

    @Override // com.google.android.exoplayer2.upstream.Loader.b
    public void b(a aVar, long j2, long j3, boolean z2) {
        a aVar2 = aVar;
        y yVar = aVar2.c;
        t tVar = new t(aVar2.a, aVar2.k, yVar.c, yVar.d, j2, j3, yVar.f952b);
        Objects.requireNonNull(this.o);
        b0.a aVar3 = this.p;
        aVar3.c(tVar, new w(1, -1, null, 0, null, aVar3.a(aVar2.j), aVar3.a(this.K)));
        if (!z2) {
            if (this.Q == -1) {
                this.Q = aVar2.l;
            }
            for (h0 h0Var : this.D) {
                h0Var.s(false);
            }
            if (this.P > 0) {
                x.a aVar4 = this.B;
                Objects.requireNonNull(aVar4);
                aVar4.a(this);
            }
        }
    }

    @Override // b.i.a.c.a3.x
    public long c() {
        if (this.P == 0) {
            return Long.MIN_VALUE;
        }
        return q();
    }

    @Override // com.google.android.exoplayer2.upstream.Loader.b
    public void d(a aVar, long j2, long j3) {
        t tVar;
        a aVar2 = aVar;
        if (this.K == -9223372036854775807L && (tVar = this.J) != null) {
            boolean c2 = tVar.c();
            long v = v();
            long j4 = v == Long.MIN_VALUE ? 0L : v + 10000;
            this.K = j4;
            ((f0) this.r).u(j4, c2, this.L);
        }
        y yVar = aVar2.c;
        t tVar2 = new t(aVar2.a, aVar2.k, yVar.c, yVar.d, j2, j3, yVar.f952b);
        Objects.requireNonNull(this.o);
        b0.a aVar3 = this.p;
        aVar3.d(tVar2, new w(1, -1, null, 0, null, aVar3.a(aVar2.j), aVar3.a(this.K)));
        if (this.Q == -1) {
            this.Q = aVar2.l;
        }
        this.V = true;
        x.a aVar4 = this.B;
        Objects.requireNonNull(aVar4);
        aVar4.a(this);
    }

    @Override // b.i.a.c.a3.x
    public void e() throws IOException {
        A();
        if (this.V && !this.G) {
            throw ParserException.a("Loading finished before preparation is complete.", null);
        }
    }

    @Override // b.i.a.c.a3.x
    public long f(long j2) {
        boolean z2;
        t();
        boolean[] zArr = this.I.f812b;
        if (!this.J.c()) {
            j2 = 0;
        }
        this.O = false;
        this.R = j2;
        if (w()) {
            this.S = j2;
            return j2;
        }
        if (this.M != 7) {
            int length = this.D.length;
            for (int i = 0; i < length; i++) {
                if (!this.D[i].t(j2, false) && (zArr[i] || !this.H)) {
                    z2 = false;
                    break;
                }
            }
            z2 = true;
            if (z2) {
                return j2;
            }
        }
        this.T = false;
        this.S = j2;
        this.V = false;
        if (this.v.b()) {
            for (h0 h0Var : this.D) {
                h0Var.h();
            }
            Loader.d<? extends Loader.e> dVar = this.v.d;
            b.c.a.a0.d.H(dVar);
            dVar.a(false);
        } else {
            this.v.e = null;
            for (h0 h0Var2 : this.D) {
                h0Var2.s(false);
            }
        }
        return j2;
    }

    @Override // b.i.a.c.a3.x
    public boolean g(long j2) {
        if (!this.V) {
            if (!(this.v.e != null) && !this.T && (!this.G || this.P != 0)) {
                boolean b2 = this.f806x.b();
                if (this.v.b()) {
                    return b2;
                }
                C();
                return true;
            }
        }
        return false;
    }

    @Override // b.i.a.c.a3.x
    public boolean h() {
        boolean z2;
        if (this.v.b()) {
            b.i.a.c.f3.j jVar = this.f806x;
            synchronized (jVar) {
                z2 = jVar.f967b;
            }
            if (z2) {
                return true;
            }
        }
        return false;
    }

    @Override // b.i.a.c.a3.x
    public long i(long j2, j2 j2Var) {
        t();
        if (!this.J.c()) {
            return 0L;
        }
        t.a h = this.J.h(j2);
        long j3 = h.a.f1294b;
        long j4 = h.f1292b.f1294b;
        long j5 = j2Var.c;
        if (j5 == 0 && j2Var.d == 0) {
            return j2;
        }
        long j6 = Long.MIN_VALUE;
        int i = b.i.a.c.f3.e0.a;
        long j7 = j2 - j5;
        if (((j5 ^ j2) & (j2 ^ j7)) >= 0) {
            j6 = j7;
        }
        long j8 = j2Var.d;
        long j9 = RecyclerView.FOREVER_NS;
        long j10 = j2 + j8;
        if (((j8 ^ j10) & (j2 ^ j10)) >= 0) {
            j9 = j10;
        }
        boolean z2 = false;
        boolean z3 = j6 <= j3 && j3 <= j9;
        if (j6 <= j4 && j4 <= j9) {
            z2 = true;
        }
        if (!z3 || !z2) {
            if (z3) {
                return j3;
            }
            if (!z2) {
                return j6;
            }
        } else if (Math.abs(j3 - j2) <= Math.abs(j4 - j2)) {
            return j3;
        }
        return j4;
    }

    @Override // b.i.a.c.x2.j
    public void j() {
        this.F = true;
        this.A.post(this.f807y);
    }

    @Override // b.i.a.c.a3.x
    public long k() {
        if (!this.O) {
            return -9223372036854775807L;
        }
        if (!this.V && u() <= this.U) {
            return -9223372036854775807L;
        }
        this.O = false;
        return this.R;
    }

    @Override // b.i.a.c.a3.x
    public void l(x.a aVar, long j2) {
        this.B = aVar;
        this.f806x.b();
        C();
    }

    @Override // b.i.a.c.a3.x
    public long m(b.i.a.c.c3.j[] jVarArr, boolean[] zArr, i0[] i0VarArr, boolean[] zArr2, long j2) {
        t();
        e eVar = this.I;
        o0 o0Var = eVar.a;
        boolean[] zArr3 = eVar.c;
        int i = this.P;
        for (int i2 = 0; i2 < jVarArr.length; i2++) {
            if (i0VarArr[i2] != null && (jVarArr[i2] == null || !zArr[i2])) {
                int i3 = ((c) i0VarArr[i2]).a;
                b.c.a.a0.d.D(zArr3[i3]);
                this.P--;
                zArr3[i3] = false;
                i0VarArr[i2] = null;
            }
        }
        boolean z2 = !this.N ? j2 != 0 : i == 0;
        for (int i4 = 0; i4 < jVarArr.length; i4++) {
            if (i0VarArr[i4] == null && jVarArr[i4] != null) {
                b.i.a.c.c3.j jVar = jVarArr[i4];
                b.c.a.a0.d.D(jVar.length() == 1);
                b.c.a.a0.d.D(jVar.f(0) == 0);
                int a2 = o0Var.a(jVar.a());
                b.c.a.a0.d.D(!zArr3[a2]);
                this.P++;
                zArr3[a2] = true;
                i0VarArr[i4] = new c(a2);
                zArr2[i4] = true;
                if (!z2) {
                    h0 h0Var = this.D[a2];
                    z2 = !h0Var.t(j2, true) && h0Var.k() != 0;
                }
            }
        }
        if (this.P == 0) {
            this.T = false;
            this.O = false;
            if (this.v.b()) {
                for (h0 h0Var2 : this.D) {
                    h0Var2.h();
                }
                Loader.d<? extends Loader.e> dVar = this.v.d;
                b.c.a.a0.d.H(dVar);
                dVar.a(false);
            } else {
                for (h0 h0Var3 : this.D) {
                    h0Var3.s(false);
                }
            }
        } else if (z2) {
            j2 = f(j2);
            for (int i5 = 0; i5 < i0VarArr.length; i5++) {
                if (i0VarArr[i5] != null) {
                    zArr2[i5] = true;
                }
            }
        }
        this.N = true;
        return j2;
    }

    @Override // b.i.a.c.a3.x
    public o0 n() {
        t();
        return this.I.a;
    }

    /* JADX WARN: Removed duplicated region for block: B:27:0x007f  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x0082  */
    /* JADX WARN: Removed duplicated region for block: B:50:0x00d6  */
    /* JADX WARN: Removed duplicated region for block: B:51:0x00db  */
    /* JADX WARN: Removed duplicated region for block: B:58:0x0107  */
    @Override // com.google.android.exoplayer2.upstream.Loader.b
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public com.google.android.exoplayer2.upstream.Loader.c o(b.i.a.c.a3.e0.a r20, long r21, long r23, java.io.IOException r25, int r26) {
        /*
            Method dump skipped, instructions count: 269
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.a3.e0.o(com.google.android.exoplayer2.upstream.Loader$e, long, long, java.io.IOException, int):com.google.android.exoplayer2.upstream.Loader$c");
    }

    @Override // b.i.a.c.x2.j
    public b.i.a.c.x2.w p(int i, int i2) {
        return B(new d(i, false));
    }

    @Override // b.i.a.c.a3.x
    public long q() {
        long j2;
        boolean z2;
        long j3;
        t();
        boolean[] zArr = this.I.f812b;
        if (this.V) {
            return Long.MIN_VALUE;
        }
        if (w()) {
            return this.S;
        }
        if (this.H) {
            int length = this.D.length;
            j2 = Long.MAX_VALUE;
            for (int i = 0; i < length; i++) {
                if (zArr[i]) {
                    h0 h0Var = this.D[i];
                    synchronized (h0Var) {
                        z2 = h0Var.f817x;
                    }
                    if (!z2) {
                        h0 h0Var2 = this.D[i];
                        synchronized (h0Var2) {
                            j3 = h0Var2.w;
                        }
                        j2 = Math.min(j2, j3);
                    } else {
                        continue;
                    }
                }
            }
        } else {
            j2 = Long.MAX_VALUE;
        }
        if (j2 == RecyclerView.FOREVER_NS) {
            j2 = v();
        }
        return j2 == Long.MIN_VALUE ? this.R : j2;
    }

    @Override // b.i.a.c.a3.x
    public void r(long j2, boolean z2) {
        long j3;
        int i;
        t();
        if (!w()) {
            boolean[] zArr = this.I.c;
            int length = this.D.length;
            for (int i2 = 0; i2 < length; i2++) {
                h0 h0Var = this.D[i2];
                boolean z3 = zArr[i2];
                g0 g0Var = h0Var.a;
                synchronized (h0Var) {
                    int i3 = h0Var.q;
                    j3 = -1;
                    if (i3 != 0) {
                        long[] jArr = h0Var.o;
                        int i4 = h0Var.f816s;
                        if (j2 >= jArr[i4]) {
                            int i5 = h0Var.i(i4, (!z3 || (i = h0Var.t) == i3) ? i3 : i + 1, j2, z2);
                            if (i5 != -1) {
                                j3 = h0Var.g(i5);
                            }
                        }
                    }
                }
                g0Var.a(j3);
            }
        }
    }

    @Override // b.i.a.c.a3.x
    public void s(long j2) {
    }

    @EnsuresNonNull({"trackState", "seekMap"})
    public final void t() {
        b.c.a.a0.d.D(this.G);
        Objects.requireNonNull(this.I);
        Objects.requireNonNull(this.J);
    }

    public final int u() {
        int i = 0;
        for (h0 h0Var : this.D) {
            i += h0Var.n();
        }
        return i;
    }

    public final long v() {
        h0[] h0VarArr;
        long j2;
        long j3 = Long.MIN_VALUE;
        for (h0 h0Var : this.D) {
            synchronized (h0Var) {
                j2 = h0Var.w;
            }
            j3 = Math.max(j3, j2);
        }
        return j3;
    }

    public final boolean w() {
        return this.S != -9223372036854775807L;
    }

    public final void x() {
        Metadata metadata;
        if (!(this.W || this.G || !this.F || this.J == null)) {
            for (h0 h0Var : this.D) {
                if (h0Var.m() == null) {
                    return;
                }
            }
            this.f806x.a();
            int length = this.D.length;
            n0[] n0VarArr = new n0[length];
            boolean[] zArr = new boolean[length];
            for (int i = 0; i < length; i++) {
                j1 m = this.D[i].m();
                Objects.requireNonNull(m);
                String str = m.w;
                boolean h = b.i.a.c.f3.t.h(str);
                boolean z2 = h || b.i.a.c.f3.t.j(str);
                zArr[i] = z2;
                this.H = z2 | this.H;
                IcyHeaders icyHeaders = this.C;
                if (icyHeaders != null) {
                    if (h || this.E[i].f811b) {
                        Metadata metadata2 = m.u;
                        if (metadata2 == null) {
                            metadata = new Metadata(icyHeaders);
                        } else {
                            metadata = metadata2.a(icyHeaders);
                        }
                        j1.b a2 = m.a();
                        a2.i = metadata;
                        m = a2.a();
                    }
                    if (h && m.q == -1 && m.r == -1 && icyHeaders.j != -1) {
                        j1.b a3 = m.a();
                        a3.f = icyHeaders.j;
                        m = a3.a();
                    }
                }
                int d2 = this.n.d(m);
                j1.b a4 = m.a();
                a4.D = d2;
                n0VarArr[i] = new n0(a4.a());
            }
            this.I = new e(new o0(n0VarArr), zArr);
            this.G = true;
            x.a aVar = this.B;
            Objects.requireNonNull(aVar);
            aVar.b(this);
        }
    }

    public final void y(int i) {
        t();
        e eVar = this.I;
        boolean[] zArr = eVar.d;
        if (!zArr[i]) {
            j1 j1Var = eVar.a.l[i].l[0];
            b0.a aVar = this.p;
            aVar.b(new w(1, b.i.a.c.f3.t.g(j1Var.w), j1Var, 0, null, aVar.a(this.R), -9223372036854775807L));
            zArr[i] = true;
        }
    }

    public final void z(int i) {
        t();
        boolean[] zArr = this.I.f812b;
        if (this.T && zArr[i] && !this.D[i].p(false)) {
            this.S = 0L;
            this.T = false;
            this.O = true;
            this.R = 0L;
            this.U = 0;
            for (h0 h0Var : this.D) {
                h0Var.s(false);
            }
            x.a aVar = this.B;
            Objects.requireNonNull(aVar);
            aVar.a(this);
        }
    }
}
