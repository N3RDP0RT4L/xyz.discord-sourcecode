package b.i.a.c.a3;

import java.util.Arrays;
import java.util.Random;
/* compiled from: ShuffleOrder.java */
/* loaded from: classes3.dex */
public interface k0 {
    k0 a(int i, int i2);

    int b();

    int c();

    int d(int i);

    int e(int i);

    k0 f(int i, int i2);

    int g();

    k0 h();

    /* compiled from: ShuffleOrder.java */
    /* loaded from: classes3.dex */
    public static class a implements k0 {
        public final Random a;

        /* renamed from: b  reason: collision with root package name */
        public final int[] f822b;
        public final int[] c;

        public a(int[] iArr, Random random) {
            this.f822b = iArr;
            this.a = random;
            this.c = new int[iArr.length];
            for (int i = 0; i < iArr.length; i++) {
                this.c[iArr[i]] = i;
            }
        }

        @Override // b.i.a.c.a3.k0
        public k0 a(int i, int i2) {
            int i3 = i2 - i;
            int[] iArr = new int[this.f822b.length - i3];
            int i4 = 0;
            int i5 = 0;
            while (true) {
                int[] iArr2 = this.f822b;
                if (i4 >= iArr2.length) {
                    return new a(iArr, new Random(this.a.nextLong()));
                }
                if (iArr2[i4] < i || iArr2[i4] >= i2) {
                    iArr[i4 - i5] = iArr2[i4] >= i ? iArr2[i4] - i3 : iArr2[i4];
                } else {
                    i5++;
                }
                i4++;
            }
        }

        @Override // b.i.a.c.a3.k0
        public int b() {
            return this.f822b.length;
        }

        @Override // b.i.a.c.a3.k0
        public int c() {
            int[] iArr = this.f822b;
            if (iArr.length > 0) {
                return iArr[0];
            }
            return -1;
        }

        @Override // b.i.a.c.a3.k0
        public int d(int i) {
            int i2 = this.c[i] - 1;
            if (i2 >= 0) {
                return this.f822b[i2];
            }
            return -1;
        }

        @Override // b.i.a.c.a3.k0
        public int e(int i) {
            int i2 = this.c[i] + 1;
            int[] iArr = this.f822b;
            if (i2 < iArr.length) {
                return iArr[i2];
            }
            return -1;
        }

        @Override // b.i.a.c.a3.k0
        public k0 f(int i, int i2) {
            int[] iArr = new int[i2];
            int[] iArr2 = new int[i2];
            int i3 = 0;
            int i4 = 0;
            while (i4 < i2) {
                iArr[i4] = this.a.nextInt(this.f822b.length + 1);
                int i5 = i4 + 1;
                int nextInt = this.a.nextInt(i5);
                iArr2[i4] = iArr2[nextInt];
                iArr2[nextInt] = i4 + i;
                i4 = i5;
            }
            Arrays.sort(iArr);
            int[] iArr3 = new int[this.f822b.length + i2];
            int i6 = 0;
            int i7 = 0;
            while (true) {
                int[] iArr4 = this.f822b;
                if (i3 >= iArr4.length + i2) {
                    return new a(iArr3, new Random(this.a.nextLong()));
                }
                if (i6 >= i2 || i7 != iArr[i6]) {
                    i7++;
                    iArr3[i3] = iArr4[i7];
                    if (iArr3[i3] >= i) {
                        iArr3[i3] = iArr3[i3] + i2;
                    }
                } else {
                    i6++;
                    iArr3[i3] = iArr2[i6];
                }
                i3++;
            }
        }

        @Override // b.i.a.c.a3.k0
        public int g() {
            int[] iArr = this.f822b;
            if (iArr.length > 0) {
                return iArr[iArr.length - 1];
            }
            return -1;
        }

        @Override // b.i.a.c.a3.k0
        public k0 h() {
            return new a(0, new Random(this.a.nextLong()));
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public a(int r6, java.util.Random r7) {
            /*
                r5 = this;
                int[] r0 = new int[r6]
                r1 = 0
            L3:
                if (r1 >= r6) goto L13
                int r2 = r1 + 1
                int r3 = r7.nextInt(r2)
                r4 = r0[r3]
                r0[r1] = r4
                r0[r3] = r1
                r1 = r2
                goto L3
            L13:
                r5.<init>(r0, r7)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.i.a.c.a3.k0.a.<init>(int, java.util.Random):void");
        }
    }
}
