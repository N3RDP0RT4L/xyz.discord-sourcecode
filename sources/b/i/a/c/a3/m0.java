package b.i.a.c.a3;

import android.util.SparseArray;
import b.c.a.a0.d;
import b.i.a.c.f3.k;
/* compiled from: SpannedData.java */
/* loaded from: classes3.dex */
public final class m0<V> {
    public final k<V> c;

    /* renamed from: b  reason: collision with root package name */
    public final SparseArray<V> f825b = new SparseArray<>();
    public int a = -1;

    public m0(k<V> kVar) {
        this.c = kVar;
    }

    public void a(int i, V v) {
        boolean z2 = false;
        if (this.a == -1) {
            d.D(this.f825b.size() == 0);
            this.a = 0;
        }
        if (this.f825b.size() > 0) {
            SparseArray<V> sparseArray = this.f825b;
            int keyAt = sparseArray.keyAt(sparseArray.size() - 1);
            if (i >= keyAt) {
                z2 = true;
            }
            d.j(z2);
            if (keyAt == i) {
                SparseArray<V> sparseArray2 = this.f825b;
                this.c.accept(sparseArray2.valueAt(sparseArray2.size() - 1));
            }
        }
        this.f825b.append(i, v);
    }

    public V b(int i) {
        if (this.a == -1) {
            this.a = 0;
        }
        while (true) {
            int i2 = this.a;
            if (i2 <= 0 || i >= this.f825b.keyAt(i2)) {
                break;
            }
            this.a--;
        }
        while (this.a < this.f825b.size() - 1 && i >= this.f825b.keyAt(this.a + 1)) {
            this.a++;
        }
        return this.f825b.valueAt(this.a);
    }

    public V c() {
        SparseArray<V> sparseArray = this.f825b;
        return sparseArray.valueAt(sparseArray.size() - 1);
    }
}
