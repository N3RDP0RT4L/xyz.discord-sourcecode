package b.i.a.c.a3;

import android.media.MediaCodec;
import androidx.annotation.Nullable;
import b.i.a.c.a3.h0;
import b.i.a.c.e3.e;
import b.i.a.c.e3.o;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.x;
import b.i.a.c.v2.c;
import b.i.a.c.x2.w;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Objects;
/* compiled from: SampleDataQueue.java */
/* loaded from: classes3.dex */
public class g0 {
    public final o a;

    /* renamed from: b  reason: collision with root package name */
    public final int f813b;
    public final x c = new x(32);
    public a d;
    public a e;
    public a f;
    public long g;

    /* compiled from: SampleDataQueue.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public final long a;

        /* renamed from: b  reason: collision with root package name */
        public final long f814b;
        public boolean c;
        @Nullable
        public e d;
        @Nullable
        public a e;

        public a(long j, int i) {
            this.a = j;
            this.f814b = j + i;
        }

        public int a(long j) {
            return ((int) (j - this.a)) + this.d.f935b;
        }
    }

    public g0(o oVar) {
        this.a = oVar;
        int i = oVar.f940b;
        this.f813b = i;
        a aVar = new a(0L, i);
        this.d = aVar;
        this.e = aVar;
        this.f = aVar;
    }

    public static a d(a aVar, long j, ByteBuffer byteBuffer, int i) {
        while (j >= aVar.f814b) {
            aVar = aVar.e;
        }
        while (i > 0) {
            int min = Math.min(i, (int) (aVar.f814b - j));
            byteBuffer.put(aVar.d.a, aVar.a(j), min);
            i -= min;
            j += min;
            if (j == aVar.f814b) {
                aVar = aVar.e;
            }
        }
        return aVar;
    }

    public static a e(a aVar, long j, byte[] bArr, int i) {
        while (j >= aVar.f814b) {
            aVar = aVar.e;
        }
        int i2 = i;
        while (i2 > 0) {
            int min = Math.min(i2, (int) (aVar.f814b - j));
            System.arraycopy(aVar.d.a, aVar.a(j), bArr, i - i2, min);
            i2 -= min;
            j += min;
            if (j == aVar.f814b) {
                aVar = aVar.e;
            }
        }
        return aVar;
    }

    public static a f(a aVar, DecoderInputBuffer decoderInputBuffer, h0.b bVar, x xVar) {
        if (decoderInputBuffer.t()) {
            long j = bVar.f820b;
            int i = 1;
            xVar.A(1);
            a e = e(aVar, j, xVar.a, 1);
            long j2 = j + 1;
            byte b2 = xVar.a[0];
            boolean z2 = (b2 & 128) != 0;
            int i2 = b2 & Byte.MAX_VALUE;
            c cVar = decoderInputBuffer.k;
            byte[] bArr = cVar.a;
            if (bArr == null) {
                cVar.a = new byte[16];
            } else {
                Arrays.fill(bArr, (byte) 0);
            }
            aVar = e(e, j2, cVar.a, i2);
            long j3 = j2 + i2;
            if (z2) {
                xVar.A(2);
                aVar = e(aVar, j3, xVar.a, 2);
                j3 += 2;
                i = xVar.y();
            }
            int[] iArr = cVar.d;
            if (iArr == null || iArr.length < i) {
                iArr = new int[i];
            }
            int[] iArr2 = cVar.e;
            if (iArr2 == null || iArr2.length < i) {
                iArr2 = new int[i];
            }
            if (z2) {
                int i3 = i * 6;
                xVar.A(i3);
                aVar = e(aVar, j3, xVar.a, i3);
                j3 += i3;
                xVar.E(0);
                for (int i4 = 0; i4 < i; i4++) {
                    iArr[i4] = xVar.y();
                    iArr2[i4] = xVar.w();
                }
            } else {
                iArr[0] = 0;
                iArr2[0] = bVar.a - ((int) (j3 - bVar.f820b));
            }
            w.a aVar2 = bVar.c;
            int i5 = e0.a;
            byte[] bArr2 = aVar2.f1295b;
            byte[] bArr3 = cVar.a;
            int i6 = aVar2.a;
            int i7 = aVar2.c;
            int i8 = aVar2.d;
            cVar.f = i;
            cVar.d = iArr;
            cVar.e = iArr2;
            cVar.f1137b = bArr2;
            cVar.a = bArr3;
            cVar.c = i6;
            cVar.g = i7;
            cVar.h = i8;
            MediaCodec.CryptoInfo cryptoInfo = cVar.i;
            cryptoInfo.numSubSamples = i;
            cryptoInfo.numBytesOfClearData = iArr;
            cryptoInfo.numBytesOfEncryptedData = iArr2;
            cryptoInfo.key = bArr2;
            cryptoInfo.iv = bArr3;
            cryptoInfo.mode = i6;
            if (e0.a >= 24) {
                c.b bVar2 = cVar.j;
                Objects.requireNonNull(bVar2);
                bVar2.f1138b.set(i7, i8);
                bVar2.a.setPattern(bVar2.f1138b);
            }
            long j4 = bVar.f820b;
            int i9 = (int) (j3 - j4);
            bVar.f820b = j4 + i9;
            bVar.a -= i9;
        }
        if (decoderInputBuffer.l()) {
            xVar.A(4);
            a e2 = e(aVar, bVar.f820b, xVar.a, 4);
            int w = xVar.w();
            bVar.f820b += 4;
            bVar.a -= 4;
            decoderInputBuffer.r(w);
            a d = d(e2, bVar.f820b, decoderInputBuffer.l, w);
            bVar.f820b += w;
            int i10 = bVar.a - w;
            bVar.a = i10;
            ByteBuffer byteBuffer = decoderInputBuffer.o;
            if (byteBuffer == null || byteBuffer.capacity() < i10) {
                decoderInputBuffer.o = ByteBuffer.allocate(i10);
            } else {
                decoderInputBuffer.o.clear();
            }
            return d(d, bVar.f820b, decoderInputBuffer.o, bVar.a);
        }
        decoderInputBuffer.r(bVar.a);
        return d(aVar, bVar.f820b, decoderInputBuffer.l, bVar.a);
    }

    public void a(long j) {
        a aVar;
        if (j != -1) {
            while (true) {
                aVar = this.d;
                if (j < aVar.f814b) {
                    break;
                }
                o oVar = this.a;
                e eVar = aVar.d;
                synchronized (oVar) {
                    e[] eVarArr = oVar.c;
                    eVarArr[0] = eVar;
                    oVar.a(eVarArr);
                }
                a aVar2 = this.d;
                aVar2.d = null;
                a aVar3 = aVar2.e;
                aVar2.e = null;
                this.d = aVar3;
            }
            if (this.e.a < aVar.a) {
                this.e = aVar;
            }
        }
    }

    public final void b(int i) {
        long j = this.g + i;
        this.g = j;
        a aVar = this.f;
        if (j == aVar.f814b) {
            this.f = aVar.e;
        }
    }

    public final int c(int i) {
        e eVar;
        a aVar = this.f;
        if (!aVar.c) {
            o oVar = this.a;
            synchronized (oVar) {
                oVar.e++;
                int i2 = oVar.f;
                if (i2 > 0) {
                    e[] eVarArr = oVar.g;
                    int i3 = i2 - 1;
                    oVar.f = i3;
                    eVar = eVarArr[i3];
                    Objects.requireNonNull(eVar);
                    oVar.g[oVar.f] = null;
                } else {
                    eVar = new e(new byte[oVar.f940b], 0);
                }
            }
            a aVar2 = new a(this.f.f814b, this.f813b);
            aVar.d = eVar;
            aVar.e = aVar2;
            aVar.c = true;
        }
        return Math.min(i, (int) (this.f.f814b - this.g));
    }
}
