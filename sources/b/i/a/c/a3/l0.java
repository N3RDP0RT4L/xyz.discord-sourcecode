package b.i.a.c.a3;

import android.net.Uri;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.a3.p0.c;
import b.i.a.c.o1;
import b.i.a.c.o2;
import b.i.a.c.p1;
import b.i.b.b.h0;
import b.i.b.b.p;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
/* compiled from: SinglePeriodTimeline.java */
/* loaded from: classes3.dex */
public final class l0 extends o2 {
    public static final Object k = new Object();
    public final long l;
    public final long m;
    public final boolean n;
    @Nullable
    public final o1 o;
    @Nullable
    public final o1.g p;

    static {
        o1.d.a aVar = new o1.d.a();
        o1.f.a aVar2 = new o1.f.a(null);
        List emptyList = Collections.emptyList();
        p<Object> pVar = h0.l;
        o1.g.a aVar3 = new o1.g.a();
        Uri uri = Uri.EMPTY;
        d.D(aVar2.f1033b == null || aVar2.a != null);
        if (uri != null) {
            new o1.i(uri, null, aVar2.a != null ? new o1.f(aVar2, null) : null, null, emptyList, null, pVar, null, null);
        }
        aVar.a();
        new o1.g(aVar3, null);
        p1 p1Var = p1.j;
    }

    public l0(long j, boolean z2, boolean z3, boolean z4, @Nullable Object obj, o1 o1Var) {
        o1.g gVar = z4 ? o1Var.m : null;
        this.l = j;
        this.m = j;
        this.n = z2;
        Objects.requireNonNull(o1Var);
        this.o = o1Var;
        this.p = gVar;
    }

    @Override // b.i.a.c.o2
    public int b(Object obj) {
        return k.equals(obj) ? 0 : -1;
    }

    @Override // b.i.a.c.o2
    public o2.b g(int i, o2.b bVar, boolean z2) {
        d.t(i, 0, 1);
        Object obj = z2 ? k : null;
        long j = this.l;
        Objects.requireNonNull(bVar);
        bVar.f(null, obj, 0, j, 0L, c.j, false);
        return bVar;
    }

    @Override // b.i.a.c.o2
    public int i() {
        return 1;
    }

    @Override // b.i.a.c.o2
    public Object m(int i) {
        d.t(i, 0, 1);
        return k;
    }

    @Override // b.i.a.c.o2
    public o2.c o(int i, o2.c cVar, long j) {
        d.t(i, 0, 1);
        cVar.e(o2.c.j, this.o, null, -9223372036854775807L, -9223372036854775807L, -9223372036854775807L, this.n, false, this.p, 0L, this.m, 0, 0, 0L);
        return cVar;
    }

    @Override // b.i.a.c.o2
    public int p() {
        return 1;
    }
}
