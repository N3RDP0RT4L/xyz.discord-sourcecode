package b.i.a.c.a3;

import android.os.Looper;
import androidx.annotation.CallSuper;
import androidx.annotation.GuardedBy;
import androidx.annotation.Nullable;
import androidx.constraintlayout.solver.widgets.analyzer.BasicMeasure;
import b.i.a.c.a3.g0;
import b.i.a.c.e3.e;
import b.i.a.c.e3.h;
import b.i.a.c.e3.o;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.t;
import b.i.a.c.f3.x;
import b.i.a.c.j1;
import b.i.a.c.k1;
import b.i.a.c.w2.m;
import b.i.a.c.w2.s;
import b.i.a.c.w2.u;
import b.i.a.c.x2.v;
import b.i.a.c.x2.w;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.drm.DrmSession;
import java.io.EOFException;
import java.io.IOException;
import java.util.Objects;
/* compiled from: SampleQueue.java */
/* loaded from: classes3.dex */
public class h0 implements w {
    @Nullable
    public j1 A;
    @Nullable
    public j1 B;
    public boolean C;
    public boolean D;
    public final g0 a;
    @Nullable
    public final u d;
    @Nullable
    public final s.a e;
    @Nullable
    public final Looper f;
    @Nullable
    public d g;
    @Nullable
    public j1 h;
    @Nullable
    public DrmSession i;
    public int q;
    public int r;

    /* renamed from: s  reason: collision with root package name */
    public int f816s;
    public int t;

    /* renamed from: x  reason: collision with root package name */
    public boolean f817x;

    /* renamed from: b  reason: collision with root package name */
    public final b f815b = new b();
    public int j = 1000;
    public int[] k = new int[1000];
    public long[] l = new long[1000];
    public long[] o = new long[1000];
    public int[] n = new int[1000];
    public int[] m = new int[1000];
    public w.a[] p = new w.a[1000];
    public final m0<c> c = new m0<>(k.a);
    public long u = Long.MIN_VALUE;
    public long v = Long.MIN_VALUE;
    public long w = Long.MIN_VALUE;

    /* renamed from: z  reason: collision with root package name */
    public boolean f819z = true;

    /* renamed from: y  reason: collision with root package name */
    public boolean f818y = true;

    /* compiled from: SampleQueue.java */
    /* loaded from: classes3.dex */
    public static final class b {
        public int a;

        /* renamed from: b  reason: collision with root package name */
        public long f820b;
        @Nullable
        public w.a c;
    }

    /* compiled from: SampleQueue.java */
    /* loaded from: classes3.dex */
    public static final class c {
        public final j1 a;

        /* renamed from: b  reason: collision with root package name */
        public final u.b f821b;

        public c(j1 j1Var, u.b bVar, a aVar) {
            this.a = j1Var;
            this.f821b = bVar;
        }
    }

    /* compiled from: SampleQueue.java */
    /* loaded from: classes3.dex */
    public interface d {
    }

    public h0(o oVar, @Nullable Looper looper, @Nullable u uVar, @Nullable s.a aVar) {
        this.f = looper;
        this.d = uVar;
        this.e = aVar;
        this.a = new g0(oVar);
    }

    @Override // b.i.a.c.x2.w
    public final int a(h hVar, int i, boolean z2, int i2) throws IOException {
        g0 g0Var = this.a;
        int c2 = g0Var.c(i);
        g0.a aVar = g0Var.f;
        int read = hVar.read(aVar.d.a, aVar.a(g0Var.g), c2);
        if (read != -1) {
            g0Var.b(read);
            return read;
        } else if (z2) {
            return -1;
        } else {
            throw new EOFException();
        }
    }

    @Override // b.i.a.c.x2.w
    public /* synthetic */ int b(h hVar, int i, boolean z2) {
        return v.a(this, hVar, i, z2);
    }

    @Override // b.i.a.c.x2.w
    public /* synthetic */ void c(x xVar, int i) {
        v.b(this, xVar, i);
    }

    @Override // b.i.a.c.x2.w
    public void d(long j, int i, int i2, int i3, @Nullable w.a aVar) {
        u.b bVar;
        int i4 = i & 1;
        boolean z2 = i4 != 0;
        if (this.f818y) {
            if (z2) {
                this.f818y = false;
            } else {
                return;
            }
        }
        long j2 = j + 0;
        if (this.C) {
            if (j2 >= this.u) {
                if (i4 == 0) {
                    if (!this.D) {
                        String valueOf = String.valueOf(this.B);
                        b.d.b.a.a.f0(valueOf.length() + 50, "Overriding unexpected non-sync sample for format: ", valueOf, "SampleQueue");
                        this.D = true;
                    }
                    i |= 1;
                }
            } else {
                return;
            }
        }
        long j3 = (this.a.g - i2) - i3;
        synchronized (this) {
            int i5 = this.q;
            if (i5 > 0) {
                int l = l(i5 - 1);
                b.c.a.a0.d.j(this.l[l] + ((long) this.m[l]) <= j3);
            }
            this.f817x = (536870912 & i) != 0;
            this.w = Math.max(this.w, j2);
            int l2 = l(this.q);
            this.o[l2] = j2;
            this.l[l2] = j3;
            this.m[l2] = i2;
            this.n[l2] = i;
            this.p[l2] = aVar;
            this.k[l2] = 0;
            if ((this.c.f825b.size() == 0) || !this.c.c().a.equals(this.B)) {
                u uVar = this.d;
                if (uVar != null) {
                    Looper looper = this.f;
                    Objects.requireNonNull(looper);
                    bVar = uVar.b(looper, this.e, this.B);
                } else {
                    bVar = m.f1149b;
                }
                m0<c> m0Var = this.c;
                int n = n();
                j1 j1Var = this.B;
                Objects.requireNonNull(j1Var);
                m0Var.a(n, new c(j1Var, bVar, null));
            }
            int i6 = this.q + 1;
            this.q = i6;
            int i7 = this.j;
            if (i6 == i7) {
                int i8 = i7 + 1000;
                int[] iArr = new int[i8];
                long[] jArr = new long[i8];
                long[] jArr2 = new long[i8];
                int[] iArr2 = new int[i8];
                int[] iArr3 = new int[i8];
                w.a[] aVarArr = new w.a[i8];
                int i9 = this.f816s;
                int i10 = i7 - i9;
                System.arraycopy(this.l, i9, jArr, 0, i10);
                System.arraycopy(this.o, this.f816s, jArr2, 0, i10);
                System.arraycopy(this.n, this.f816s, iArr2, 0, i10);
                System.arraycopy(this.m, this.f816s, iArr3, 0, i10);
                System.arraycopy(this.p, this.f816s, aVarArr, 0, i10);
                System.arraycopy(this.k, this.f816s, iArr, 0, i10);
                int i11 = this.f816s;
                System.arraycopy(this.l, 0, jArr, i10, i11);
                System.arraycopy(this.o, 0, jArr2, i10, i11);
                System.arraycopy(this.n, 0, iArr2, i10, i11);
                System.arraycopy(this.m, 0, iArr3, i10, i11);
                System.arraycopy(this.p, 0, aVarArr, i10, i11);
                System.arraycopy(this.k, 0, iArr, i10, i11);
                this.l = jArr;
                this.o = jArr2;
                this.n = iArr2;
                this.m = iArr3;
                this.p = aVarArr;
                this.k = iArr;
                this.f816s = 0;
                this.j = i8;
            }
        }
    }

    @Override // b.i.a.c.x2.w
    public final void e(j1 j1Var) {
        boolean z2;
        this.A = j1Var;
        synchronized (this) {
            z2 = false;
            this.f819z = false;
            if (!e0.a(j1Var, this.B)) {
                if ((this.c.f825b.size() == 0) || !this.c.c().a.equals(j1Var)) {
                    this.B = j1Var;
                } else {
                    this.B = this.c.c().a;
                }
                j1 j1Var2 = this.B;
                this.C = t.a(j1Var2.w, j1Var2.t);
                this.D = false;
                z2 = true;
            }
        }
        d dVar = this.g;
        if (dVar != null && z2) {
            e0 e0Var = (e0) dVar;
            e0Var.A.post(e0Var.f807y);
        }
    }

    @Override // b.i.a.c.x2.w
    public final void f(x xVar, int i, int i2) {
        g0 g0Var = this.a;
        Objects.requireNonNull(g0Var);
        while (i > 0) {
            int c2 = g0Var.c(i);
            g0.a aVar = g0Var.f;
            xVar.e(aVar.d.a, aVar.a(g0Var.g), c2);
            i -= c2;
            g0Var.b(c2);
        }
    }

    @GuardedBy("this")
    public final long g(int i) {
        int i2;
        this.v = Math.max(this.v, j(i));
        this.q -= i;
        int i3 = this.r + i;
        this.r = i3;
        int i4 = this.f816s + i;
        this.f816s = i4;
        int i5 = this.j;
        if (i4 >= i5) {
            this.f816s = i4 - i5;
        }
        int i6 = this.t - i;
        this.t = i6;
        int i7 = 0;
        if (i6 < 0) {
            this.t = 0;
        }
        m0<c> m0Var = this.c;
        while (i7 < m0Var.f825b.size() - 1) {
            int i8 = i7 + 1;
            if (i3 < m0Var.f825b.keyAt(i8)) {
                break;
            }
            m0Var.c.accept(m0Var.f825b.valueAt(i7));
            m0Var.f825b.removeAt(i7);
            int i9 = m0Var.a;
            if (i9 > 0) {
                m0Var.a = i9 - 1;
            }
            i7 = i8;
        }
        if (this.q != 0) {
            return this.l[this.f816s];
        }
        int i10 = this.f816s;
        if (i10 == 0) {
            i10 = this.j;
        }
        return this.l[i10 - 1] + this.m[i2];
    }

    public final void h() {
        long g;
        g0 g0Var = this.a;
        synchronized (this) {
            int i = this.q;
            g = i == 0 ? -1L : g(i);
        }
        g0Var.a(g);
    }

    public final int i(int i, int i2, long j, boolean z2) {
        int i3 = -1;
        for (int i4 = 0; i4 < i2; i4++) {
            long[] jArr = this.o;
            if (jArr[i] > j) {
                return i3;
            }
            if (!z2 || (this.n[i] & 1) != 0) {
                if (jArr[i] == j) {
                    return i4;
                }
                i3 = i4;
            }
            i++;
            if (i == this.j) {
                i = 0;
            }
        }
        return i3;
    }

    public final long j(int i) {
        long j = Long.MIN_VALUE;
        if (i == 0) {
            return Long.MIN_VALUE;
        }
        int l = l(i - 1);
        for (int i2 = 0; i2 < i; i2++) {
            j = Math.max(j, this.o[l]);
            if ((this.n[l] & 1) != 0) {
                break;
            }
            l--;
            if (l == -1) {
                l = this.j - 1;
            }
        }
        return j;
    }

    public final int k() {
        return this.r + this.t;
    }

    public final int l(int i) {
        int i2 = this.f816s + i;
        int i3 = this.j;
        return i2 < i3 ? i2 : i2 - i3;
    }

    @Nullable
    public final synchronized j1 m() {
        return this.f819z ? null : this.B;
    }

    public final int n() {
        return this.r + this.q;
    }

    public final boolean o() {
        return this.t != this.q;
    }

    @CallSuper
    public synchronized boolean p(boolean z2) {
        j1 j1Var;
        boolean z3 = true;
        if (!o()) {
            if (!z2 && !this.f817x && ((j1Var = this.B) == null || j1Var == this.h)) {
                z3 = false;
            }
            return z3;
        } else if (this.c.b(k()).a != this.h) {
            return true;
        } else {
            return q(l(this.t));
        }
    }

    public final boolean q(int i) {
        DrmSession drmSession = this.i;
        return drmSession == null || drmSession.getState() == 4 || ((this.n[i] & BasicMeasure.EXACTLY) == 0 && this.i.d());
    }

    public final void r(j1 j1Var, k1 k1Var) {
        j1 j1Var2;
        j1 j1Var3 = this.h;
        boolean z2 = j1Var3 == null;
        DrmInitData drmInitData = z2 ? null : j1Var3.f1016z;
        this.h = j1Var;
        DrmInitData drmInitData2 = j1Var.f1016z;
        u uVar = this.d;
        if (uVar != null) {
            int d2 = uVar.d(j1Var);
            j1.b a2 = j1Var.a();
            a2.D = d2;
            j1Var2 = a2.a();
        } else {
            j1Var2 = j1Var;
        }
        k1Var.f1023b = j1Var2;
        k1Var.a = this.i;
        if (this.d != null) {
            if (z2 || !e0.a(drmInitData, drmInitData2)) {
                DrmSession drmSession = this.i;
                u uVar2 = this.d;
                Looper looper = this.f;
                Objects.requireNonNull(looper);
                DrmSession c2 = uVar2.c(looper, this.e, j1Var);
                this.i = c2;
                k1Var.a = c2;
                if (drmSession != null) {
                    drmSession.b(this.e);
                }
            }
        }
    }

    @CallSuper
    public void s(boolean z2) {
        g0 g0Var = this.a;
        g0.a aVar = g0Var.d;
        if (aVar.c) {
            g0.a aVar2 = g0Var.f;
            int i = (((int) (aVar2.a - aVar.a)) / g0Var.f813b) + (aVar2.c ? 1 : 0);
            e[] eVarArr = new e[i];
            int i2 = 0;
            while (i2 < i) {
                eVarArr[i2] = aVar.d;
                aVar.d = null;
                g0.a aVar3 = aVar.e;
                aVar.e = null;
                i2++;
                aVar = aVar3;
            }
            g0Var.a.a(eVarArr);
        }
        g0.a aVar4 = new g0.a(0L, g0Var.f813b);
        g0Var.d = aVar4;
        g0Var.e = aVar4;
        g0Var.f = aVar4;
        g0Var.g = 0L;
        g0Var.a.c();
        this.q = 0;
        this.r = 0;
        this.f816s = 0;
        this.t = 0;
        this.f818y = true;
        this.u = Long.MIN_VALUE;
        this.v = Long.MIN_VALUE;
        this.w = Long.MIN_VALUE;
        this.f817x = false;
        m0<c> m0Var = this.c;
        for (int i3 = 0; i3 < m0Var.f825b.size(); i3++) {
            m0Var.c.accept(m0Var.f825b.valueAt(i3));
        }
        m0Var.a = -1;
        m0Var.f825b.clear();
        if (z2) {
            this.A = null;
            this.B = null;
            this.f819z = true;
        }
    }

    public final synchronized boolean t(long j, boolean z2) {
        synchronized (this) {
            this.t = 0;
            g0 g0Var = this.a;
            g0Var.e = g0Var.d;
        }
        int l = l(0);
        if (o() && j >= this.o[l] && (j <= this.w || z2)) {
            int i = i(l, this.q - this.t, j, true);
            if (i == -1) {
                return false;
            }
            this.u = j;
            this.t += i;
            return true;
        }
        return false;
    }
}
