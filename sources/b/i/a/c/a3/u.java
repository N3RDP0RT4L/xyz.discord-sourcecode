package b.i.a.c.a3;

import androidx.annotation.Nullable;
import b.i.a.c.a3.a0;
import b.i.a.c.a3.x;
import b.i.a.c.c3.j;
import b.i.a.c.e3.o;
import b.i.a.c.f3.e0;
import b.i.a.c.j2;
import java.io.IOException;
import java.util.Objects;
/* compiled from: MaskingMediaPeriod.java */
/* loaded from: classes3.dex */
public final class u implements x, x.a {
    public final a0.a j;
    public final long k;
    public final o l;
    public a0 m;
    public x n;
    @Nullable
    public x.a o;
    public long p = -9223372036854775807L;

    public u(a0.a aVar, o oVar, long j) {
        this.j = aVar;
        this.l = oVar;
        this.k = j;
    }

    @Override // b.i.a.c.a3.j0.a
    public void a(x xVar) {
        x.a aVar = this.o;
        int i = e0.a;
        aVar.a(this);
    }

    @Override // b.i.a.c.a3.x.a
    public void b(x xVar) {
        x.a aVar = this.o;
        int i = e0.a;
        aVar.b(this);
    }

    @Override // b.i.a.c.a3.x
    public long c() {
        x xVar = this.n;
        int i = e0.a;
        return xVar.c();
    }

    public void d(a0.a aVar) {
        long j = this.k;
        long j2 = this.p;
        if (j2 != -9223372036854775807L) {
            j = j2;
        }
        a0 a0Var = this.m;
        Objects.requireNonNull(a0Var);
        x n = a0Var.n(aVar, this.l, j);
        this.n = n;
        if (this.o != null) {
            n.l(this, j);
        }
    }

    @Override // b.i.a.c.a3.x
    public void e() throws IOException {
        try {
            x xVar = this.n;
            if (xVar != null) {
                xVar.e();
                return;
            }
            a0 a0Var = this.m;
            if (a0Var != null) {
                a0Var.h();
            }
        } catch (IOException e) {
            throw e;
        }
    }

    @Override // b.i.a.c.a3.x
    public long f(long j) {
        x xVar = this.n;
        int i = e0.a;
        return xVar.f(j);
    }

    @Override // b.i.a.c.a3.x
    public boolean g(long j) {
        x xVar = this.n;
        return xVar != null && xVar.g(j);
    }

    @Override // b.i.a.c.a3.x
    public boolean h() {
        x xVar = this.n;
        return xVar != null && xVar.h();
    }

    @Override // b.i.a.c.a3.x
    public long i(long j, j2 j2Var) {
        x xVar = this.n;
        int i = e0.a;
        return xVar.i(j, j2Var);
    }

    @Override // b.i.a.c.a3.x
    public long k() {
        x xVar = this.n;
        int i = e0.a;
        return xVar.k();
    }

    @Override // b.i.a.c.a3.x
    public void l(x.a aVar, long j) {
        this.o = aVar;
        x xVar = this.n;
        if (xVar != null) {
            long j2 = this.k;
            long j3 = this.p;
            if (j3 != -9223372036854775807L) {
                j2 = j3;
            }
            xVar.l(this, j2);
        }
    }

    @Override // b.i.a.c.a3.x
    public long m(j[] jVarArr, boolean[] zArr, i0[] i0VarArr, boolean[] zArr2, long j) {
        long j2;
        long j3 = this.p;
        if (j3 == -9223372036854775807L || j != this.k) {
            j2 = j;
        } else {
            this.p = -9223372036854775807L;
            j2 = j3;
        }
        x xVar = this.n;
        int i = e0.a;
        return xVar.m(jVarArr, zArr, i0VarArr, zArr2, j2);
    }

    @Override // b.i.a.c.a3.x
    public o0 n() {
        x xVar = this.n;
        int i = e0.a;
        return xVar.n();
    }

    @Override // b.i.a.c.a3.x
    public long q() {
        x xVar = this.n;
        int i = e0.a;
        return xVar.q();
    }

    @Override // b.i.a.c.a3.x
    public void r(long j, boolean z2) {
        x xVar = this.n;
        int i = e0.a;
        xVar.r(j, z2);
    }

    @Override // b.i.a.c.a3.x
    public void s(long j) {
        x xVar = this.n;
        int i = e0.a;
        xVar.s(j);
    }
}
