package b.i.a.c.a3;

import android.os.Handler;
import androidx.annotation.CheckResult;
import androidx.annotation.Nullable;
import b.i.a.c.a3.a0;
import b.i.a.c.a3.b0;
import b.i.a.c.f3.e0;
import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
/* compiled from: MediaSourceEventListener.java */
/* loaded from: classes3.dex */
public interface b0 {
    void X(int i, @Nullable a0.a aVar, t tVar, w wVar);

    void g0(int i, @Nullable a0.a aVar, t tVar, w wVar, IOException iOException, boolean z2);

    void o(int i, @Nullable a0.a aVar, w wVar);

    void q(int i, @Nullable a0.a aVar, t tVar, w wVar);

    void x(int i, @Nullable a0.a aVar, t tVar, w wVar);

    /* compiled from: MediaSourceEventListener.java */
    /* loaded from: classes3.dex */
    public static class a {
        public final int a;
        @Nullable

        /* renamed from: b  reason: collision with root package name */
        public final a0.a f803b;
        public final CopyOnWriteArrayList<C0091a> c;
        public final long d;

        /* compiled from: MediaSourceEventListener.java */
        /* renamed from: b.i.a.c.a3.b0$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0091a {
            public Handler a;

            /* renamed from: b  reason: collision with root package name */
            public b0 f804b;

            public C0091a(Handler handler, b0 b0Var) {
                this.a = handler;
                this.f804b = b0Var;
            }
        }

        public a() {
            this.c = new CopyOnWriteArrayList<>();
            this.a = 0;
            this.f803b = null;
            this.d = 0L;
        }

        public final long a(long j) {
            long M = e0.M(j);
            if (M == -9223372036854775807L) {
                return -9223372036854775807L;
            }
            return this.d + M;
        }

        public void b(final w wVar) {
            Iterator<C0091a> it = this.c.iterator();
            while (it.hasNext()) {
                C0091a next = it.next();
                final b0 b0Var = next.f804b;
                e0.E(next.a, new Runnable() { // from class: b.i.a.c.a3.d
                    @Override // java.lang.Runnable
                    public final void run() {
                        b0.a aVar = b0.a.this;
                        b0Var.o(aVar.a, aVar.f803b, wVar);
                    }
                });
            }
        }

        public void c(final t tVar, final w wVar) {
            Iterator<C0091a> it = this.c.iterator();
            while (it.hasNext()) {
                C0091a next = it.next();
                final b0 b0Var = next.f804b;
                e0.E(next.a, new Runnable() { // from class: b.i.a.c.a3.f
                    @Override // java.lang.Runnable
                    public final void run() {
                        b0.a aVar = b0.a.this;
                        b0Var.q(aVar.a, aVar.f803b, tVar, wVar);
                    }
                });
            }
        }

        public void d(final t tVar, final w wVar) {
            Iterator<C0091a> it = this.c.iterator();
            while (it.hasNext()) {
                C0091a next = it.next();
                final b0 b0Var = next.f804b;
                e0.E(next.a, new Runnable() { // from class: b.i.a.c.a3.c
                    @Override // java.lang.Runnable
                    public final void run() {
                        b0.a aVar = b0.a.this;
                        b0Var.X(aVar.a, aVar.f803b, tVar, wVar);
                    }
                });
            }
        }

        public void e(final t tVar, final w wVar, final IOException iOException, final boolean z2) {
            Iterator<C0091a> it = this.c.iterator();
            while (it.hasNext()) {
                C0091a next = it.next();
                final b0 b0Var = next.f804b;
                e0.E(next.a, new Runnable() { // from class: b.i.a.c.a3.b
                    @Override // java.lang.Runnable
                    public final void run() {
                        b0.a aVar = b0.a.this;
                        b0Var.g0(aVar.a, aVar.f803b, tVar, wVar, iOException, z2);
                    }
                });
            }
        }

        public void f(final t tVar, final w wVar) {
            Iterator<C0091a> it = this.c.iterator();
            while (it.hasNext()) {
                C0091a next = it.next();
                final b0 b0Var = next.f804b;
                e0.E(next.a, new Runnable() { // from class: b.i.a.c.a3.e
                    @Override // java.lang.Runnable
                    public final void run() {
                        b0.a aVar = b0.a.this;
                        b0Var.x(aVar.a, aVar.f803b, tVar, wVar);
                    }
                });
            }
        }

        @CheckResult
        public a g(int i, @Nullable a0.a aVar, long j) {
            return new a(this.c, i, aVar, j);
        }

        public a(CopyOnWriteArrayList<C0091a> copyOnWriteArrayList, int i, @Nullable a0.a aVar, long j) {
            this.c = copyOnWriteArrayList;
            this.a = i;
            this.f803b = aVar;
            this.d = j;
        }
    }
}
