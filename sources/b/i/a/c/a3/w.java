package b.i.a.c.a3;

import androidx.annotation.Nullable;
import b.i.a.c.j1;
/* compiled from: MediaLoadData.java */
/* loaded from: classes3.dex */
public final class w {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final int f830b;
    @Nullable
    public final j1 c;
    public final int d;
    @Nullable
    public final Object e;
    public final long f;
    public final long g;

    public w(int i, int i2, @Nullable j1 j1Var, int i3, @Nullable Object obj, long j, long j2) {
        this.a = i;
        this.f830b = i2;
        this.c = j1Var;
        this.d = i3;
        this.e = obj;
        this.f = j;
        this.g = j2;
    }
}
