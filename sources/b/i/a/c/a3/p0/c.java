package b.i.a.c.a3.p0;

import android.net.Uri;
import androidx.annotation.IntRange;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.f3.e0;
import b.i.a.c.w0;
import java.util.Arrays;
/* compiled from: AdPlaybackState.java */
/* loaded from: classes3.dex */
public final class c implements w0 {
    public static final a k;
    @Nullable
    public final Object m = null;
    public final int n;
    public final long o;
    public final long p;
    public final int q;
    public final a[] r;
    public static final c j = new c(null, new a[0], 0, -9223372036854775807L, 0);
    public static final w0.a<c> l = b.a;

    /* compiled from: AdPlaybackState.java */
    /* loaded from: classes3.dex */
    public static final class a implements w0 {
        public static final /* synthetic */ int j = 0;
        public final long k;
        public final int l;
        public final Uri[] m;
        public final int[] n;
        public final long[] o;
        public final long p;
        public final boolean q;

        public a(long j2, int i, int[] iArr, Uri[] uriArr, long[] jArr, long j3, boolean z2) {
            d.j(iArr.length == uriArr.length);
            this.k = j2;
            this.l = i;
            this.n = iArr;
            this.m = uriArr;
            this.o = jArr;
            this.p = j3;
            this.q = z2;
        }

        public static String c(int i) {
            return Integer.toString(i, 36);
        }

        public int a(@IntRange(from = -1) int i) {
            int i2 = i + 1;
            while (true) {
                int[] iArr = this.n;
                if (i2 >= iArr.length || this.q || iArr[i2] == 0 || iArr[i2] == 1) {
                    break;
                }
                i2++;
            }
            return i2;
        }

        public boolean b() {
            if (this.l == -1) {
                return true;
            }
            for (int i = 0; i < this.l; i++) {
                int[] iArr = this.n;
                if (iArr[i] == 0 || iArr[i] == 1) {
                    return true;
                }
            }
            return false;
        }

        public boolean equals(@Nullable Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || a.class != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            return this.k == aVar.k && this.l == aVar.l && Arrays.equals(this.m, aVar.m) && Arrays.equals(this.n, aVar.n) && Arrays.equals(this.o, aVar.o) && this.p == aVar.p && this.q == aVar.q;
        }

        public int hashCode() {
            long j2 = this.k;
            int hashCode = Arrays.hashCode(this.n);
            int hashCode2 = Arrays.hashCode(this.o);
            long j3 = this.p;
            return ((((hashCode2 + ((hashCode + (((((this.l * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + Arrays.hashCode(this.m)) * 31)) * 31)) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + (this.q ? 1 : 0);
        }
    }

    static {
        a aVar = new a(0L, -1, new int[0], new Uri[0], new long[0], 0L, false);
        int[] iArr = aVar.n;
        int length = iArr.length;
        int max = Math.max(0, length);
        int[] copyOf = Arrays.copyOf(iArr, max);
        Arrays.fill(copyOf, length, max, 0);
        long[] jArr = aVar.o;
        int length2 = jArr.length;
        int max2 = Math.max(0, length2);
        long[] copyOf2 = Arrays.copyOf(jArr, max2);
        Arrays.fill(copyOf2, length2, max2, -9223372036854775807L);
        k = new a(aVar.k, 0, copyOf, (Uri[]) Arrays.copyOf(aVar.m, 0), copyOf2, aVar.p, aVar.q);
    }

    public c(@Nullable Object obj, a[] aVarArr, long j2, long j3, int i) {
        this.o = j2;
        this.p = j3;
        this.n = aVarArr.length + i;
        this.r = aVarArr;
        this.q = i;
    }

    public static String b(int i) {
        return Integer.toString(i, 36);
    }

    public a a(@IntRange(from = 0) int i) {
        int i2 = this.q;
        if (i < i2) {
            return k;
        }
        return this.r[i - i2];
    }

    public boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || c.class != obj.getClass()) {
            return false;
        }
        c cVar = (c) obj;
        return e0.a(this.m, cVar.m) && this.n == cVar.n && this.o == cVar.o && this.p == cVar.p && this.q == cVar.q && Arrays.equals(this.r, cVar.r);
    }

    public int hashCode() {
        int i = this.n * 31;
        Object obj = this.m;
        return ((((((((i + (obj == null ? 0 : obj.hashCode())) * 31) + ((int) this.o)) * 31) + ((int) this.p)) * 31) + this.q) * 31) + Arrays.hashCode(this.r);
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("AdPlaybackState(adsId=");
        R.append(this.m);
        R.append(", adResumePositionUs=");
        R.append(this.o);
        R.append(", adGroups=[");
        for (int i = 0; i < this.r.length; i++) {
            R.append("adGroup(timeUs=");
            R.append(this.r[i].k);
            R.append(", ads=[");
            for (int i2 = 0; i2 < this.r[i].n.length; i2++) {
                R.append("ad(state=");
                int i3 = this.r[i].n[i2];
                if (i3 == 0) {
                    R.append('_');
                } else if (i3 == 1) {
                    R.append('R');
                } else if (i3 == 2) {
                    R.append('S');
                } else if (i3 == 3) {
                    R.append('P');
                } else if (i3 != 4) {
                    R.append('?');
                } else {
                    R.append('!');
                }
                R.append(", durationUs=");
                R.append(this.r[i].o[i2]);
                R.append(')');
                if (i2 < this.r[i].n.length - 1) {
                    R.append(", ");
                }
            }
            R.append("])");
            if (i < this.r.length - 1) {
                R.append(", ");
            }
        }
        R.append("])");
        return R.toString();
    }
}
