package b.i.a.c.s2;

import android.util.SparseArray;
import androidx.annotation.Nullable;
import androidx.core.view.InputDeviceCompat;
import androidx.core.view.PointerIconCompat;
import androidx.media.AudioAttributesCompat;
import b.i.a.c.a2;
import b.i.a.c.a3.a0;
import b.i.a.c.a3.b0;
import b.i.a.c.a3.o0;
import b.i.a.c.a3.t;
import b.i.a.c.c1;
import b.i.a.c.c3.n;
import b.i.a.c.e3.f;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.g;
import b.i.a.c.f3.o;
import b.i.a.c.f3.p;
import b.i.a.c.g3.w;
import b.i.a.c.g3.x;
import b.i.a.c.g3.y;
import b.i.a.c.j1;
import b.i.a.c.o1;
import b.i.a.c.o2;
import b.i.a.c.p1;
import b.i.a.c.p2;
import b.i.a.c.s2.h1;
import b.i.a.c.t2.r;
import b.i.a.c.v2.e;
import b.i.a.c.w2.s;
import b.i.a.c.x1;
import b.i.a.c.y1;
import b.i.a.c.z1;
import b.i.b.b.h0;
import b.i.b.b.i0;
import b.i.b.b.q;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackException;
import com.google.android.exoplayer2.metadata.Metadata;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArraySet;
import org.checkerframework.checker.nullness.qual.RequiresNonNull;
/* compiled from: AnalyticsCollector.java */
/* loaded from: classes3.dex */
public class g1 implements y1.e, r, x, b0, f.a, s {
    public final g j;
    public final o2.b k;
    public final a m;
    public p<h1> o;
    public y1 p;
    public o q;
    public boolean r;
    public final o2.c l = new o2.c();
    public final SparseArray<h1.a> n = new SparseArray<>();

    /* compiled from: AnalyticsCollector.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public final o2.b a;

        /* renamed from: b  reason: collision with root package name */
        public b.i.b.b.p<a0.a> f1077b = h0.l;
        public q<a0.a, o2> c = i0.m;
        @Nullable
        public a0.a d;
        public a0.a e;
        public a0.a f;

        public a(o2.b bVar) {
            this.a = bVar;
            b.i.b.b.a<Object> aVar = b.i.b.b.p.k;
        }

        @Nullable
        public static a0.a b(y1 y1Var, b.i.b.b.p<a0.a> pVar, @Nullable a0.a aVar, o2.b bVar) {
            o2 K = y1Var.K();
            int m = y1Var.m();
            Object m2 = K.q() ? null : K.m(m);
            int b2 = (y1Var.f() || K.q()) ? -1 : K.f(m, bVar).b(e0.B(y1Var.T()) - bVar.n);
            for (int i = 0; i < pVar.size(); i++) {
                a0.a aVar2 = pVar.get(i);
                if (c(aVar2, m2, y1Var.f(), y1Var.B(), y1Var.q(), b2)) {
                    return aVar2;
                }
            }
            if (pVar.isEmpty() && aVar != null) {
                if (c(aVar, m2, y1Var.f(), y1Var.B(), y1Var.q(), b2)) {
                    return aVar;
                }
            }
            return null;
        }

        public static boolean c(a0.a aVar, @Nullable Object obj, boolean z2, int i, int i2, int i3) {
            if (!aVar.a.equals(obj)) {
                return false;
            }
            return (z2 && aVar.f831b == i && aVar.c == i2) || (!z2 && aVar.f831b == -1 && aVar.e == i3);
        }

        public final void a(q.a<a0.a, o2> aVar, @Nullable a0.a aVar2, o2 o2Var) {
            if (aVar2 != null) {
                if (o2Var.b(aVar2.a) != -1) {
                    aVar.c(aVar2, o2Var);
                    return;
                }
                o2 o2Var2 = this.c.get(aVar2);
                if (o2Var2 != null) {
                    aVar.c(aVar2, o2Var2);
                }
            }
        }

        public final void d(o2 o2Var) {
            q.a<a0.a, o2> aVar = new q.a<>(4);
            if (this.f1077b.isEmpty()) {
                a(aVar, this.e, o2Var);
                if (!b.i.a.f.e.o.f.V(this.f, this.e)) {
                    a(aVar, this.f, o2Var);
                }
                if (!b.i.a.f.e.o.f.V(this.d, this.e) && !b.i.a.f.e.o.f.V(this.d, this.f)) {
                    a(aVar, this.d, o2Var);
                }
            } else {
                for (int i = 0; i < this.f1077b.size(); i++) {
                    a(aVar, this.f1077b.get(i), o2Var);
                }
                if (!this.f1077b.contains(this.d)) {
                    a(aVar, this.d, o2Var);
                }
            }
            this.c = aVar.a();
        }
    }

    public g1(g gVar) {
        this.j = gVar;
        this.o = new p<>(new CopyOnWriteArraySet(), e0.o(), gVar, o0.a);
        o2.b bVar = new o2.b();
        this.k = bVar;
        this.m = new a(bVar);
    }

    @Override // b.i.a.c.y1.c
    public void A(final p1 p1Var) {
        final h1.a k0 = k0();
        p.a<h1> r0Var = new p.a() { // from class: b.i.a.c.s2.r0
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).o0();
            }
        };
        this.n.put(14, k0);
        p<h1> pVar = this.o;
        pVar.b(14, r0Var);
        pVar.a();
    }

    @Override // b.i.a.c.t2.r
    public final void B(final String str) {
        final h1.a p0 = p0();
        p.a<h1> tVar = new p.a() { // from class: b.i.a.c.s2.t
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).W();
            }
        };
        this.n.put(PointerIconCompat.TYPE_ALL_SCROLL, p0);
        p<h1> pVar = this.o;
        pVar.b(PointerIconCompat.TYPE_ALL_SCROLL, tVar);
        pVar.a();
    }

    @Override // b.i.a.c.t2.r
    public final void C(final String str, final long j, final long j2) {
        final h1.a p0 = p0();
        p.a<h1> l0Var = new p.a() { // from class: b.i.a.c.s2.l0
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                h1 h1Var = (h1) obj;
                h1Var.k0();
                h1Var.U();
                h1Var.T();
            }
        };
        this.n.put(PointerIconCompat.TYPE_VERTICAL_TEXT, p0);
        p<h1> pVar = this.o;
        pVar.b(PointerIconCompat.TYPE_VERTICAL_TEXT, l0Var);
        pVar.a();
    }

    @Override // b.i.a.c.y1.c
    public final void D(final boolean z2) {
        final h1.a k0 = k0();
        p.a<h1> g0Var = new p.a() { // from class: b.i.a.c.s2.g0
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).o();
            }
        };
        this.n.put(9, k0);
        p<h1> pVar = this.o;
        pVar.b(9, g0Var);
        pVar.a();
    }

    @Override // b.i.a.c.y1.c
    public /* synthetic */ void E(y1 y1Var, y1.d dVar) {
        a2.e(this, y1Var, dVar);
    }

    @Override // b.i.a.c.g3.x
    public final void F(final int i, final long j) {
        final h1.a o0 = o0();
        p.a<h1> zVar = new p.a() { // from class: b.i.a.c.s2.z
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).f();
            }
        };
        this.n.put(AudioAttributesCompat.FLAG_ALL, o0);
        p<h1> pVar = this.o;
        pVar.b(AudioAttributesCompat.FLAG_ALL, zVar);
        pVar.a();
    }

    @Override // b.i.a.c.y1.e
    public /* synthetic */ void G(int i, boolean z2) {
        a2.d(this, i, z2);
    }

    @Override // b.i.a.c.y1.c
    public final void H(final boolean z2, final int i) {
        final h1.a k0 = k0();
        p.a<h1> j0Var = new p.a() { // from class: b.i.a.c.s2.j0
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).G();
            }
        };
        this.n.put(-1, k0);
        p<h1> pVar = this.o;
        pVar.b(-1, j0Var);
        pVar.a();
    }

    @Override // b.i.a.c.t2.r
    public final void I(final j1 j1Var, @Nullable final b.i.a.c.v2.g gVar) {
        final h1.a p0 = p0();
        p.a<h1> i0Var = new p.a() { // from class: b.i.a.c.s2.i0
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                h1 h1Var = (h1) obj;
                h1Var.d0();
                h1Var.v();
                h1Var.x();
            }
        };
        this.n.put(PointerIconCompat.TYPE_ALIAS, p0);
        p<h1> pVar = this.o;
        pVar.b(PointerIconCompat.TYPE_ALIAS, i0Var);
        pVar.a();
    }

    @Override // b.i.a.c.w2.s
    public final void J(int i, @Nullable a0.a aVar) {
        final h1.a n0 = n0(i, aVar);
        p.a<h1> t0Var = new p.a() { // from class: b.i.a.c.s2.t0
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).Y();
            }
        };
        this.n.put(1034, n0);
        p<h1> pVar = this.o;
        pVar.b(1034, t0Var);
        pVar.a();
    }

    @Override // b.i.a.c.g3.x
    public final void K(final Object obj, final long j) {
        final h1.a p0 = p0();
        p.a<h1> f1Var = new p.a() { // from class: b.i.a.c.s2.f1
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj2) {
                ((h1) obj2).c();
            }
        };
        this.n.put(1027, p0);
        p<h1> pVar = this.o;
        pVar.b(1027, f1Var);
        pVar.a();
    }

    @Override // b.i.a.c.y1.c
    public final void L(final int i) {
        final h1.a k0 = k0();
        p.a<h1> b0Var = new p.a() { // from class: b.i.a.c.s2.b0
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).b();
            }
        };
        this.n.put(8, k0);
        p<h1> pVar = this.o;
        pVar.b(8, b0Var);
        pVar.a();
    }

    @Override // b.i.a.c.y1.c
    public final void M(@Nullable final o1 o1Var, final int i) {
        final h1.a k0 = k0();
        p.a<h1> qVar = new p.a() { // from class: b.i.a.c.s2.q
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).t();
            }
        };
        this.n.put(1, k0);
        p<h1> pVar = this.o;
        pVar.b(1, qVar);
        pVar.a();
    }

    @Override // b.i.a.c.t2.r
    public final void N(final Exception exc) {
        final h1.a p0 = p0();
        p.a<h1> e0Var = new p.a() { // from class: b.i.a.c.s2.e0
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).w();
            }
        };
        this.n.put(PointerIconCompat.TYPE_ZOOM_IN, p0);
        p<h1> pVar = this.o;
        pVar.b(PointerIconCompat.TYPE_ZOOM_IN, e0Var);
        pVar.a();
    }

    @Override // b.i.a.c.g3.x
    public /* synthetic */ void O(j1 j1Var) {
        w.a(this, j1Var);
    }

    @Override // b.i.a.c.g3.x
    public final void P(final e eVar) {
        final h1.a p0 = p0();
        p.a<h1> d0Var = new p.a() { // from class: b.i.a.c.s2.d0
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                h1 h1Var = (h1) obj;
                h1Var.Z();
                h1Var.F();
            }
        };
        this.n.put(PointerIconCompat.TYPE_GRAB, p0);
        p<h1> pVar = this.o;
        pVar.b(PointerIconCompat.TYPE_GRAB, d0Var);
        pVar.a();
    }

    @Override // b.i.a.c.g3.x
    public final void Q(final j1 j1Var, @Nullable final b.i.a.c.v2.g gVar) {
        final h1.a p0 = p0();
        p.a<h1> y0Var = new p.a() { // from class: b.i.a.c.s2.y0
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                h1 h1Var = (h1) obj;
                h1Var.h0();
                h1Var.B();
                h1Var.x();
            }
        };
        this.n.put(1022, p0);
        p<h1> pVar = this.o;
        pVar.b(1022, y0Var);
        pVar.a();
    }

    @Override // b.i.a.c.t2.r
    public final void R(final long j) {
        final h1.a p0 = p0();
        p.a<h1> n0Var = new p.a() { // from class: b.i.a.c.s2.n0
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).j();
            }
        };
        this.n.put(PointerIconCompat.TYPE_COPY, p0);
        p<h1> pVar = this.o;
        pVar.b(PointerIconCompat.TYPE_COPY, n0Var);
        pVar.a();
    }

    @Override // b.i.a.c.w2.s
    public final void S(int i, @Nullable a0.a aVar) {
        final h1.a n0 = n0(i, aVar);
        p.a<h1> e1Var = new p.a() { // from class: b.i.a.c.s2.e1
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).A();
            }
        };
        this.n.put(1031, n0);
        p<h1> pVar = this.o;
        pVar.b(1031, e1Var);
        pVar.a();
    }

    @Override // b.i.a.c.t2.r
    public final void T(final Exception exc) {
        final h1.a p0 = p0();
        p.a<h1> kVar = new p.a() { // from class: b.i.a.c.s2.k
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).V();
            }
        };
        this.n.put(1037, p0);
        p<h1> pVar = this.o;
        pVar.b(1037, kVar);
        pVar.a();
    }

    @Override // b.i.a.c.t2.r
    public /* synthetic */ void U(j1 j1Var) {
        b.i.a.c.t2.q.a(this, j1Var);
    }

    @Override // b.i.a.c.g3.x
    public final void V(final Exception exc) {
        final h1.a p0 = p0();
        p.a<h1> q0Var = new p.a() { // from class: b.i.a.c.s2.q0
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).O();
            }
        };
        this.n.put(1038, p0);
        p<h1> pVar = this.o;
        pVar.b(1038, q0Var);
        pVar.a();
    }

    @Override // b.i.a.c.y1.c
    public final void W(final boolean z2, final int i) {
        final h1.a k0 = k0();
        p.a<h1> d1Var = new p.a() { // from class: b.i.a.c.s2.d1
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).e();
            }
        };
        this.n.put(5, k0);
        p<h1> pVar = this.o;
        pVar.b(5, d1Var);
        pVar.a();
    }

    @Override // b.i.a.c.a3.b0
    public final void X(int i, @Nullable a0.a aVar, final t tVar, final b.i.a.c.a3.w wVar) {
        final h1.a n0 = n0(i, aVar);
        p.a<h1> yVar = new p.a() { // from class: b.i.a.c.s2.y
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).p();
            }
        };
        this.n.put(PointerIconCompat.TYPE_CONTEXT_MENU, n0);
        p<h1> pVar = this.o;
        pVar.b(PointerIconCompat.TYPE_CONTEXT_MENU, yVar);
        pVar.a();
    }

    @Override // b.i.a.c.y1.c
    public final void Y(final o0 o0Var, final n nVar) {
        final h1.a k0 = k0();
        p.a<h1> iVar = new p.a() { // from class: b.i.a.c.s2.i
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).m0();
            }
        };
        this.n.put(2, k0);
        p<h1> pVar = this.o;
        pVar.b(2, iVar);
        pVar.a();
    }

    @Override // b.i.a.c.g3.x
    public final void Z(final e eVar) {
        final h1.a o0 = o0();
        p.a<h1> jVar = new p.a() { // from class: b.i.a.c.s2.j
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                h1 h1Var = (h1) obj;
                h1Var.m();
                h1Var.g();
            }
        };
        this.n.put(InputDeviceCompat.SOURCE_GAMEPAD, o0);
        p<h1> pVar = this.o;
        pVar.b(InputDeviceCompat.SOURCE_GAMEPAD, jVar);
        pVar.a();
    }

    @Override // b.i.a.c.y1.c
    public final void a() {
        final h1.a k0 = k0();
        p.a<h1> uVar = new p.a() { // from class: b.i.a.c.s2.u
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).a();
            }
        };
        this.n.put(-1, k0);
        p<h1> pVar = this.o;
        pVar.b(-1, uVar);
        pVar.a();
    }

    @Override // b.i.a.c.y1.e
    public void a0(final int i, final int i2) {
        final h1.a p0 = p0();
        p.a<h1> nVar = new p.a() { // from class: b.i.a.c.s2.n
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).l();
            }
        };
        this.n.put(1029, p0);
        p<h1> pVar = this.o;
        pVar.b(1029, nVar);
        pVar.a();
    }

    @Override // b.i.a.c.y1.e
    public final void b(final Metadata metadata) {
        final h1.a k0 = k0();
        p.a<h1> vVar = new p.a() { // from class: b.i.a.c.s2.v
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).f0();
            }
        };
        this.n.put(PointerIconCompat.TYPE_CROSSHAIR, k0);
        p<h1> pVar = this.o;
        pVar.b(PointerIconCompat.TYPE_CROSSHAIR, vVar);
        pVar.a();
    }

    @Override // b.i.a.c.y1.c
    public final void b0(final x1 x1Var) {
        final h1.a k0 = k0();
        p.a<h1> mVar = new p.a() { // from class: b.i.a.c.s2.m
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).I();
            }
        };
        this.n.put(12, k0);
        p<h1> pVar = this.o;
        pVar.b(12, mVar);
        pVar.a();
    }

    @Override // b.i.a.c.y1.e
    public /* synthetic */ void c() {
        a2.r(this);
    }

    @Override // b.i.a.c.w2.s
    public final void c0(int i, @Nullable a0.a aVar, final int i2) {
        final h1.a n0 = n0(i, aVar);
        p.a<h1> p0Var = new p.a() { // from class: b.i.a.c.s2.p0
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                h1 h1Var = (h1) obj;
                h1Var.p0();
                h1Var.X();
            }
        };
        this.n.put(1030, n0);
        p<h1> pVar = this.o;
        pVar.b(1030, p0Var);
        pVar.a();
    }

    @Override // b.i.a.c.y1.e
    public final void d(final boolean z2) {
        final h1.a p0 = p0();
        p.a<h1> u0Var = new p.a() { // from class: b.i.a.c.s2.u0
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).q();
            }
        };
        this.n.put(PointerIconCompat.TYPE_TOP_LEFT_DIAGONAL_DOUBLE_ARROW, p0);
        p<h1> pVar = this.o;
        pVar.b(PointerIconCompat.TYPE_TOP_LEFT_DIAGONAL_DOUBLE_ARROW, u0Var);
        pVar.a();
    }

    @Override // b.i.a.c.w2.s
    public final void d0(int i, @Nullable a0.a aVar) {
        final h1.a n0 = n0(i, aVar);
        p.a<h1> bVar = new p.a() { // from class: b.i.a.c.s2.b
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).g0();
            }
        };
        this.n.put(1035, n0);
        p<h1> pVar = this.o;
        pVar.b(1035, bVar);
        pVar.a();
    }

    @Override // b.i.a.c.y1.e
    public /* synthetic */ void e(List list) {
        a2.b(this, list);
    }

    @Override // b.i.a.c.t2.r
    public final void e0(final int i, final long j, final long j2) {
        final h1.a p0 = p0();
        p.a<h1> c0Var = new p.a() { // from class: b.i.a.c.s2.c0
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).C();
            }
        };
        this.n.put(PointerIconCompat.TYPE_NO_DROP, p0);
        p<h1> pVar = this.o;
        pVar.b(PointerIconCompat.TYPE_NO_DROP, c0Var);
        pVar.a();
    }

    @Override // b.i.a.c.y1.e
    public final void f(final y yVar) {
        final h1.a p0 = p0();
        p.a<h1> m0Var = new p.a() { // from class: b.i.a.c.s2.m0
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                y yVar2 = yVar;
                h1 h1Var = (h1) obj;
                h1Var.n0();
                int i = yVar2.k;
                h1Var.r();
            }
        };
        this.n.put(1028, p0);
        p<h1> pVar = this.o;
        pVar.b(1028, m0Var);
        pVar.a();
    }

    @Override // b.i.a.c.y1.c
    public /* synthetic */ void f0(PlaybackException playbackException) {
        a2.p(this, playbackException);
    }

    @Override // b.i.a.c.y1.c
    public final void g(final y1.f fVar, final y1.f fVar2, final int i) {
        if (i == 1) {
            this.r = false;
        }
        a aVar = this.m;
        y1 y1Var = this.p;
        Objects.requireNonNull(y1Var);
        aVar.d = a.b(y1Var, aVar.f1077b, aVar.e, aVar.a);
        final h1.a k0 = k0();
        p.a<h1> rVar = new p.a() { // from class: b.i.a.c.s2.r
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                h1 h1Var = (h1) obj;
                h1Var.S();
                h1Var.y();
            }
        };
        this.n.put(11, k0);
        p<h1> pVar = this.o;
        pVar.b(11, rVar);
        pVar.a();
    }

    @Override // b.i.a.c.a3.b0
    public final void g0(int i, @Nullable a0.a aVar, final t tVar, final b.i.a.c.a3.w wVar, final IOException iOException, final boolean z2) {
        final h1.a n0 = n0(i, aVar);
        p.a<h1> f0Var = new p.a() { // from class: b.i.a.c.s2.f0
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).u();
            }
        };
        this.n.put(PointerIconCompat.TYPE_HELP, n0);
        p<h1> pVar = this.o;
        pVar.b(PointerIconCompat.TYPE_HELP, f0Var);
        pVar.a();
    }

    @Override // b.i.a.c.y1.c
    public final void h(final int i) {
        final h1.a k0 = k0();
        p.a<h1> b1Var = new p.a() { // from class: b.i.a.c.s2.b1
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).e0();
            }
        };
        this.n.put(6, k0);
        p<h1> pVar = this.o;
        pVar.b(6, b1Var);
        pVar.a();
    }

    @Override // b.i.a.c.g3.x
    public final void h0(final long j, final int i) {
        final h1.a o0 = o0();
        p.a<h1> c1Var = new p.a() { // from class: b.i.a.c.s2.c1
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).a0();
            }
        };
        this.n.put(1026, o0);
        p<h1> pVar = this.o;
        pVar.b(1026, c1Var);
        pVar.a();
    }

    @Override // b.i.a.c.y1.c
    public /* synthetic */ void i(boolean z2) {
        z1.d(this, z2);
    }

    @Override // b.i.a.c.w2.s
    public final void i0(int i, @Nullable a0.a aVar) {
        final h1.a n0 = n0(i, aVar);
        p.a<h1> v0Var = new p.a() { // from class: b.i.a.c.s2.v0
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).s();
            }
        };
        this.n.put(1033, n0);
        p<h1> pVar = this.o;
        pVar.b(1033, v0Var);
        pVar.a();
    }

    @Override // b.i.a.c.y1.c
    public /* synthetic */ void j(int i) {
        z1.l(this, i);
    }

    @Override // b.i.a.c.y1.c
    public void j0(final boolean z2) {
        final h1.a k0 = k0();
        p.a<h1> z0Var = new p.a() { // from class: b.i.a.c.s2.z0
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).D();
            }
        };
        this.n.put(7, k0);
        p<h1> pVar = this.o;
        pVar.b(7, z0Var);
        pVar.a();
    }

    @Override // b.i.a.c.t2.r
    public final void k(final e eVar) {
        final h1.a o0 = o0();
        p.a<h1> w0Var = new p.a() { // from class: b.i.a.c.s2.w0
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                h1 h1Var = (h1) obj;
                h1Var.d();
                h1Var.g();
            }
        };
        this.n.put(PointerIconCompat.TYPE_HORIZONTAL_DOUBLE_ARROW, o0);
        p<h1> pVar = this.o;
        pVar.b(PointerIconCompat.TYPE_HORIZONTAL_DOUBLE_ARROW, w0Var);
        pVar.a();
    }

    public final h1.a k0() {
        return m0(this.m.d);
    }

    @Override // b.i.a.c.g3.x
    public final void l(final String str) {
        final h1.a p0 = p0();
        p.a<h1> fVar = new p.a() { // from class: b.i.a.c.s2.f
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).N();
            }
        };
        this.n.put(1024, p0);
        p<h1> pVar = this.o;
        pVar.b(1024, fVar);
        pVar.a();
    }

    @RequiresNonNull({"player"})
    public final h1.a l0(o2 o2Var, int i, @Nullable a0.a aVar) {
        long j;
        a0.a aVar2 = o2Var.q() ? null : aVar;
        long d = this.j.d();
        boolean z2 = false;
        boolean z3 = o2Var.equals(this.p.K()) && i == this.p.C();
        long j2 = 0;
        if (aVar2 != null && aVar2.a()) {
            if (z3 && this.p.B() == aVar2.f831b && this.p.q() == aVar2.c) {
                z2 = true;
            }
            if (z2) {
                j2 = this.p.T();
            }
        } else if (z3) {
            j = this.p.w();
            return new h1.a(d, o2Var, i, aVar2, j, this.p.K(), this.p.C(), this.m.d, this.p.T(), this.p.g());
        } else if (!o2Var.q()) {
            j2 = o2Var.o(i, this.l, 0L).a();
        }
        j = j2;
        return new h1.a(d, o2Var, i, aVar2, j, this.p.K(), this.p.C(), this.m.d, this.p.T(), this.p.g());
    }

    @Override // b.i.a.c.t2.r
    public final void m(final e eVar) {
        final h1.a p0 = p0();
        p.a<h1> lVar = new p.a() { // from class: b.i.a.c.s2.l
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                h1 h1Var = (h1) obj;
                h1Var.h();
                h1Var.F();
            }
        };
        this.n.put(PointerIconCompat.TYPE_TEXT, p0);
        p<h1> pVar = this.o;
        pVar.b(PointerIconCompat.TYPE_TEXT, lVar);
        pVar.a();
    }

    public final h1.a m0(@Nullable a0.a aVar) {
        Objects.requireNonNull(this.p);
        o2 o2Var = aVar == null ? null : this.m.c.get(aVar);
        if (aVar != null && o2Var != null) {
            return l0(o2Var, o2Var.h(aVar.a, this.k).l, aVar);
        }
        int C = this.p.C();
        o2 K = this.p.K();
        if (!(C < K.p())) {
            K = o2.j;
        }
        return l0(K, C, null);
    }

    @Override // b.i.a.c.g3.x
    public final void n(final String str, final long j, final long j2) {
        final h1.a p0 = p0();
        p.a<h1> dVar = new p.a() { // from class: b.i.a.c.s2.d
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                h1 h1Var = (h1) obj;
                h1Var.i0();
                h1Var.M();
                h1Var.T();
            }
        };
        this.n.put(PointerIconCompat.TYPE_GRABBING, p0);
        p<h1> pVar = this.o;
        pVar.b(PointerIconCompat.TYPE_GRABBING, dVar);
        pVar.a();
    }

    public final h1.a n0(int i, @Nullable a0.a aVar) {
        Objects.requireNonNull(this.p);
        boolean z2 = false;
        if (aVar != null) {
            if (this.m.c.get(aVar) != null) {
                z2 = true;
            }
            if (z2) {
                return m0(aVar);
            }
            return l0(o2.j, i, aVar);
        }
        o2 K = this.p.K();
        if (i < K.p()) {
            z2 = true;
        }
        if (!z2) {
            K = o2.j;
        }
        return l0(K, i, null);
    }

    @Override // b.i.a.c.a3.b0
    public final void o(int i, @Nullable a0.a aVar, final b.i.a.c.a3.w wVar) {
        final h1.a n0 = n0(i, aVar);
        p.a<h1> gVar = new p.a() { // from class: b.i.a.c.s2.g
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).H();
            }
        };
        this.n.put(PointerIconCompat.TYPE_WAIT, n0);
        p<h1> pVar = this.o;
        pVar.b(PointerIconCompat.TYPE_WAIT, gVar);
        pVar.a();
    }

    public final h1.a o0() {
        return m0(this.m.e);
    }

    @Override // b.i.a.c.y1.c
    public void p(final p2 p2Var) {
        final h1.a k0 = k0();
        p.a<h1> wVar = new p.a() { // from class: b.i.a.c.s2.w
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).i();
            }
        };
        this.n.put(2, k0);
        p<h1> pVar = this.o;
        pVar.b(2, wVar);
        pVar.a();
    }

    public final h1.a p0() {
        return m0(this.m.f);
    }

    @Override // b.i.a.c.a3.b0
    public final void q(int i, @Nullable a0.a aVar, final t tVar, final b.i.a.c.a3.w wVar) {
        final h1.a n0 = n0(i, aVar);
        p.a<h1> h0Var = new p.a() { // from class: b.i.a.c.s2.h0
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).k();
            }
        };
        this.n.put(PointerIconCompat.TYPE_HAND, n0);
        p<h1> pVar = this.o;
        pVar.b(PointerIconCompat.TYPE_HAND, h0Var);
        pVar.a();
    }

    @Override // b.i.a.c.y1.c
    public final void r(final boolean z2) {
        final h1.a k0 = k0();
        p.a<h1> eVar = new p.a() { // from class: b.i.a.c.s2.e
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                h1 h1Var = (h1) obj;
                h1Var.L();
                h1Var.Q();
            }
        };
        this.n.put(3, k0);
        p<h1> pVar = this.o;
        pVar.b(3, eVar);
        pVar.a();
    }

    @Override // b.i.a.c.y1.c
    public final void s(final PlaybackException playbackException) {
        b.i.a.c.a3.y yVar;
        final h1.a m0 = (!(playbackException instanceof ExoPlaybackException) || (yVar = ((ExoPlaybackException) playbackException).mediaPeriodId) == null) ? null : m0(new a0.a(yVar));
        if (m0 == null) {
            m0 = k0();
        }
        p.a<h1> aVar = new p.a() { // from class: b.i.a.c.s2.a
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).J();
            }
        };
        this.n.put(10, m0);
        p<h1> pVar = this.o;
        pVar.b(10, aVar);
        pVar.a();
    }

    @Override // b.i.a.c.y1.c
    public void t(final y1.b bVar) {
        final h1.a k0 = k0();
        p.a<h1> a0Var = new p.a() { // from class: b.i.a.c.s2.a0
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).P();
            }
        };
        this.n.put(13, k0);
        p<h1> pVar = this.o;
        pVar.b(13, a0Var);
        pVar.a();
    }

    @Override // b.i.a.c.w2.s
    public final void u(int i, @Nullable a0.a aVar, final Exception exc) {
        final h1.a n0 = n0(i, aVar);
        p.a<h1> cVar = new p.a() { // from class: b.i.a.c.s2.c
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).n();
            }
        };
        this.n.put(1032, n0);
        p<h1> pVar = this.o;
        pVar.b(1032, cVar);
        pVar.a();
    }

    @Override // b.i.a.c.y1.c
    public final void v(o2 o2Var, final int i) {
        a aVar = this.m;
        y1 y1Var = this.p;
        Objects.requireNonNull(y1Var);
        aVar.d = a.b(y1Var, aVar.f1077b, aVar.e, aVar.a);
        aVar.d(y1Var.K());
        final h1.a k0 = k0();
        p.a<h1> xVar = new p.a() { // from class: b.i.a.c.s2.x
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).z();
            }
        };
        this.n.put(0, k0);
        p<h1> pVar = this.o;
        pVar.b(0, xVar);
        pVar.a();
    }

    @Override // b.i.a.c.y1.e
    public final void w(final float f) {
        final h1.a p0 = p0();
        p.a<h1> sVar = new p.a() { // from class: b.i.a.c.s2.s
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).b0();
            }
        };
        this.n.put(PointerIconCompat.TYPE_ZOOM_OUT, p0);
        p<h1> pVar = this.o;
        pVar.b(PointerIconCompat.TYPE_ZOOM_OUT, sVar);
        pVar.a();
    }

    @Override // b.i.a.c.a3.b0
    public final void x(int i, @Nullable a0.a aVar, final t tVar, final b.i.a.c.a3.w wVar) {
        final h1.a n0 = n0(i, aVar);
        p.a<h1> k0Var = new p.a() { // from class: b.i.a.c.s2.k0
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).R();
            }
        };
        this.n.put(1000, n0);
        p<h1> pVar = this.o;
        pVar.b(1000, k0Var);
        pVar.a();
    }

    @Override // b.i.a.c.y1.c
    public final void y(final int i) {
        final h1.a k0 = k0();
        p.a<h1> a1Var = new p.a() { // from class: b.i.a.c.s2.a1
            @Override // b.i.a.c.f3.p.a
            public final void invoke(Object obj) {
                ((h1) obj).c0();
            }
        };
        this.n.put(4, k0);
        p<h1> pVar = this.o;
        pVar.b(4, a1Var);
        pVar.a();
    }

    @Override // b.i.a.c.y1.e
    public /* synthetic */ void z(c1 c1Var) {
        a2.c(this, c1Var);
    }
}
