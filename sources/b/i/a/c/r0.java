package b.i.a.c;

import android.util.Pair;
import b.i.a.c.a3.k0;
import b.i.a.c.f3.e0;
import b.i.a.c.o2;
import java.util.Objects;
/* compiled from: AbstractConcatenatedTimeline.java */
/* loaded from: classes3.dex */
public abstract class r0 extends o2 {
    public final int k;
    public final k0 l;
    public final boolean m;

    public r0(boolean z2, k0 k0Var) {
        this.m = z2;
        this.l = k0Var;
        this.k = k0Var.b();
    }

    @Override // b.i.a.c.o2
    public int a(boolean z2) {
        if (this.k == 0) {
            return -1;
        }
        int i = 0;
        if (this.m) {
            z2 = false;
        }
        if (z2) {
            i = this.l.c();
        }
        do {
            c2 c2Var = (c2) this;
            if (c2Var.r[i].q()) {
                i = r(i, z2);
            } else {
                return c2Var.r[i].a(z2) + c2Var.q[i];
            }
        } while (i != -1);
        return -1;
    }

    @Override // b.i.a.c.o2
    public final int b(Object obj) {
        int b2;
        if (!(obj instanceof Pair)) {
            return -1;
        }
        Pair pair = (Pair) obj;
        Object obj2 = pair.first;
        Object obj3 = pair.second;
        c2 c2Var = (c2) this;
        Integer num = c2Var.t.get(obj2);
        int intValue = num == null ? -1 : num.intValue();
        if (intValue == -1 || (b2 = c2Var.r[intValue].b(obj3)) == -1) {
            return -1;
        }
        return c2Var.p[intValue] + b2;
    }

    @Override // b.i.a.c.o2
    public int c(boolean z2) {
        int i = this.k;
        if (i == 0) {
            return -1;
        }
        if (this.m) {
            z2 = false;
        }
        int g = z2 ? this.l.g() : i - 1;
        do {
            c2 c2Var = (c2) this;
            if (c2Var.r[g].q()) {
                g = s(g, z2);
            } else {
                return c2Var.r[g].c(z2) + c2Var.q[g];
            }
        } while (g != -1);
        return -1;
    }

    @Override // b.i.a.c.o2
    public int e(int i, int i2, boolean z2) {
        int i3 = 0;
        if (this.m) {
            if (i2 == 1) {
                i2 = 2;
            }
            z2 = false;
        }
        c2 c2Var = (c2) this;
        int d = e0.d(c2Var.q, i + 1, false, false);
        int i4 = c2Var.q[d];
        o2 o2Var = c2Var.r[d];
        int i5 = i - i4;
        if (i2 != 2) {
            i3 = i2;
        }
        int e = o2Var.e(i5, i3, z2);
        if (e != -1) {
            return i4 + e;
        }
        int r = r(d, z2);
        while (r != -1 && c2Var.r[r].q()) {
            r = r(r, z2);
        }
        if (r != -1) {
            return c2Var.r[r].a(z2) + c2Var.q[r];
        } else if (i2 == 2) {
            return a(z2);
        } else {
            return -1;
        }
    }

    @Override // b.i.a.c.o2
    public final o2.b g(int i, o2.b bVar, boolean z2) {
        c2 c2Var = (c2) this;
        int d = e0.d(c2Var.p, i + 1, false, false);
        int i2 = c2Var.q[d];
        c2Var.r[d].g(i - c2Var.p[d], bVar, z2);
        bVar.l += i2;
        if (z2) {
            Object obj = c2Var.f891s[d];
            Object obj2 = bVar.k;
            Objects.requireNonNull(obj2);
            bVar.k = Pair.create(obj, obj2);
        }
        return bVar;
    }

    @Override // b.i.a.c.o2
    public final o2.b h(Object obj, o2.b bVar) {
        Pair pair = (Pair) obj;
        Object obj2 = pair.first;
        Object obj3 = pair.second;
        c2 c2Var = (c2) this;
        Integer num = c2Var.t.get(obj2);
        int intValue = num == null ? -1 : num.intValue();
        int i = c2Var.q[intValue];
        c2Var.r[intValue].h(obj3, bVar);
        bVar.l += i;
        bVar.k = obj;
        return bVar;
    }

    @Override // b.i.a.c.o2
    public int l(int i, int i2, boolean z2) {
        int i3 = 0;
        if (this.m) {
            if (i2 == 1) {
                i2 = 2;
            }
            z2 = false;
        }
        c2 c2Var = (c2) this;
        int d = e0.d(c2Var.q, i + 1, false, false);
        int i4 = c2Var.q[d];
        o2 o2Var = c2Var.r[d];
        int i5 = i - i4;
        if (i2 != 2) {
            i3 = i2;
        }
        int l = o2Var.l(i5, i3, z2);
        if (l != -1) {
            return i4 + l;
        }
        int s2 = s(d, z2);
        while (s2 != -1 && c2Var.r[s2].q()) {
            s2 = s(s2, z2);
        }
        if (s2 != -1) {
            return c2Var.r[s2].c(z2) + c2Var.q[s2];
        } else if (i2 == 2) {
            return c(z2);
        } else {
            return -1;
        }
    }

    @Override // b.i.a.c.o2
    public final Object m(int i) {
        c2 c2Var = (c2) this;
        int d = e0.d(c2Var.p, i + 1, false, false);
        return Pair.create(c2Var.f891s[d], c2Var.r[d].m(i - c2Var.p[d]));
    }

    @Override // b.i.a.c.o2
    public final o2.c o(int i, o2.c cVar, long j) {
        c2 c2Var = (c2) this;
        int d = e0.d(c2Var.q, i + 1, false, false);
        int i2 = c2Var.q[d];
        int i3 = c2Var.p[d];
        c2Var.r[d].o(i - i2, cVar, j);
        Object obj = c2Var.f891s[d];
        if (!o2.c.j.equals(cVar.n)) {
            obj = Pair.create(obj, cVar.n);
        }
        cVar.n = obj;
        cVar.B += i3;
        cVar.C += i3;
        return cVar;
    }

    public final int r(int i, boolean z2) {
        if (z2) {
            return this.l.e(i);
        }
        if (i < this.k - 1) {
            return i + 1;
        }
        return -1;
    }

    public final int s(int i, boolean z2) {
        if (z2) {
            return this.l.d(i);
        }
        if (i > 0) {
            return i - 1;
        }
        return -1;
    }
}
