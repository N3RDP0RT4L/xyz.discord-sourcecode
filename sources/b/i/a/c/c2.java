package b.i.a.c;

import b.i.a.c.a3.k0;
import java.util.Collection;
import java.util.HashMap;
/* compiled from: PlaylistTimeline.java */
/* loaded from: classes3.dex */
public final class c2 extends r0 {
    public final int n;
    public final int o;
    public final int[] p;
    public final int[] q;
    public final o2[] r;

    /* renamed from: s  reason: collision with root package name */
    public final Object[] f891s;
    public final HashMap<Object, Integer> t = new HashMap<>();

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public c2(Collection<? extends t1> collection, k0 k0Var) {
        super(false, k0Var);
        int i = 0;
        int size = collection.size();
        this.p = new int[size];
        this.q = new int[size];
        this.r = new o2[size];
        this.f891s = new Object[size];
        int i2 = 0;
        int i3 = 0;
        for (t1 t1Var : collection) {
            this.r[i3] = t1Var.a();
            this.q[i3] = i;
            this.p[i3] = i2;
            i += this.r[i3].p();
            i2 += this.r[i3].i();
            this.f891s[i3] = t1Var.getUid();
            HashMap<Object, Integer> hashMap = this.t;
            Object obj = this.f891s[i3];
            i3++;
            hashMap.put(obj, Integer.valueOf(i3));
        }
        this.n = i;
        this.o = i2;
    }

    @Override // b.i.a.c.o2
    public int i() {
        return this.o;
    }

    @Override // b.i.a.c.o2
    public int p() {
        return this.n;
    }
}
