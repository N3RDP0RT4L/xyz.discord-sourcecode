package b.i.a.c;

import android.net.Uri;
import android.util.Pair;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.a3.p0.c;
import b.i.a.c.f3.e0;
import b.i.a.c.o1;
import b.i.a.c.w0;
import b.i.b.b.h0;
import b.i.b.b.p;
import com.google.errorprone.annotations.InlineMe;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
/* compiled from: Timeline.java */
/* loaded from: classes3.dex */
public abstract class o2 implements w0 {
    public static final o2 j = new a();

    /* compiled from: Timeline.java */
    /* loaded from: classes3.dex */
    public class a extends o2 {
        @Override // b.i.a.c.o2
        public int b(Object obj) {
            return -1;
        }

        @Override // b.i.a.c.o2
        public b g(int i, b bVar, boolean z2) {
            throw new IndexOutOfBoundsException();
        }

        @Override // b.i.a.c.o2
        public int i() {
            return 0;
        }

        @Override // b.i.a.c.o2
        public Object m(int i) {
            throw new IndexOutOfBoundsException();
        }

        @Override // b.i.a.c.o2
        public c o(int i, c cVar, long j) {
            throw new IndexOutOfBoundsException();
        }

        @Override // b.i.a.c.o2
        public int p() {
            return 0;
        }
    }

    /* compiled from: Timeline.java */
    /* loaded from: classes3.dex */
    public static final class b implements w0 {
        @Nullable
        public Object j;
        @Nullable
        public Object k;
        public int l;
        public long m;
        public long n;
        public boolean o;
        public b.i.a.c.a3.p0.c p = b.i.a.c.a3.p0.c.j;

        public long a(int i, int i2) {
            c.a a = this.p.a(i);
            if (a.l != -1) {
                return a.o[i2];
            }
            return -9223372036854775807L;
        }

        public int b(long j) {
            b.i.a.c.a3.p0.c cVar = this.p;
            long j2 = this.m;
            Objects.requireNonNull(cVar);
            if (j == Long.MIN_VALUE) {
                return -1;
            }
            if (j2 != -9223372036854775807L && j >= j2) {
                return -1;
            }
            int i = cVar.q;
            while (i < cVar.n) {
                if (cVar.a(i).k == Long.MIN_VALUE || cVar.a(i).k > j) {
                    c.a a = cVar.a(i);
                    if (a.l == -1 || a.a(-1) < a.l) {
                        break;
                    }
                }
                i++;
            }
            if (i < cVar.n) {
                return i;
            }
            return -1;
        }

        public long c(int i) {
            return this.p.a(i).k;
        }

        public int d(int i) {
            return this.p.a(i).a(-1);
        }

        public boolean e(int i) {
            return this.p.a(i).q;
        }

        public boolean equals(@Nullable Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || !b.class.equals(obj.getClass())) {
                return false;
            }
            b bVar = (b) obj;
            return e0.a(this.j, bVar.j) && e0.a(this.k, bVar.k) && this.l == bVar.l && this.m == bVar.m && this.n == bVar.n && this.o == bVar.o && e0.a(this.p, bVar.p);
        }

        public b f(@Nullable Object obj, @Nullable Object obj2, int i, long j, long j2, b.i.a.c.a3.p0.c cVar, boolean z2) {
            this.j = obj;
            this.k = obj2;
            this.l = i;
            this.m = j;
            this.n = j2;
            this.p = cVar;
            this.o = z2;
            return this;
        }

        public int hashCode() {
            Object obj = this.j;
            int i = 0;
            int hashCode = (217 + (obj == null ? 0 : obj.hashCode())) * 31;
            Object obj2 = this.k;
            if (obj2 != null) {
                i = obj2.hashCode();
            }
            long j = this.m;
            long j2 = this.n;
            return this.p.hashCode() + ((((((((((hashCode + i) * 31) + this.l) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + (this.o ? 1 : 0)) * 31);
        }
    }

    /* compiled from: Timeline.java */
    /* loaded from: classes3.dex */
    public static final class c implements w0 {
        public static final Object j = new Object();
        public static final Object k = new Object();
        public static final o1 l;
        public static final w0.a<c> m;
        public long A;
        public int B;
        public int C;
        public long D;
        @Nullable
        @Deprecated
        public Object o;
        @Nullable
        public Object q;
        public long r;

        /* renamed from: s  reason: collision with root package name */
        public long f1038s;
        public long t;
        public boolean u;
        public boolean v;
        @Deprecated
        public boolean w;
        @Nullable

        /* renamed from: x  reason: collision with root package name */
        public o1.g f1039x;

        /* renamed from: y  reason: collision with root package name */
        public boolean f1040y;

        /* renamed from: z  reason: collision with root package name */
        public long f1041z;
        public Object n = j;
        public o1 p = l;

        static {
            o1.i iVar;
            o1.d.a aVar = new o1.d.a();
            o1.f.a aVar2 = new o1.f.a(null);
            List emptyList = Collections.emptyList();
            p<Object> pVar = h0.l;
            o1.g.a aVar3 = new o1.g.a();
            Uri uri = Uri.EMPTY;
            d.D(aVar2.f1033b == null || aVar2.a != null);
            if (uri != null) {
                iVar = new o1.i(uri, null, aVar2.a != null ? new o1.f(aVar2, null) : null, null, emptyList, null, pVar, null, null);
            } else {
                iVar = null;
            }
            l = new o1("com.google.android.exoplayer2.Timeline", aVar.a(), iVar, new o1.g(aVar3, null), p1.j, null);
            m = q0.a;
        }

        public static String d(int i) {
            return Integer.toString(i, 36);
        }

        public long a() {
            return e0.M(this.f1041z);
        }

        public long b() {
            return e0.M(this.A);
        }

        public boolean c() {
            d.D(this.w == (this.f1039x != null));
            return this.f1039x != null;
        }

        public c e(Object obj, @Nullable o1 o1Var, @Nullable Object obj2, long j2, long j3, long j4, boolean z2, boolean z3, @Nullable o1.g gVar, long j5, long j6, int i, int i2, long j7) {
            o1.h hVar;
            this.n = obj;
            this.p = o1Var != null ? o1Var : l;
            this.o = (o1Var == null || (hVar = o1Var.l) == null) ? null : hVar.g;
            this.q = obj2;
            this.r = j2;
            this.f1038s = j3;
            this.t = j4;
            this.u = z2;
            this.v = z3;
            this.w = gVar != null;
            this.f1039x = gVar;
            this.f1041z = j5;
            this.A = j6;
            this.B = i;
            this.C = i2;
            this.D = j7;
            this.f1040y = false;
            return this;
        }

        public boolean equals(@Nullable Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || !c.class.equals(obj.getClass())) {
                return false;
            }
            c cVar = (c) obj;
            return e0.a(this.n, cVar.n) && e0.a(this.p, cVar.p) && e0.a(this.q, cVar.q) && e0.a(this.f1039x, cVar.f1039x) && this.r == cVar.r && this.f1038s == cVar.f1038s && this.t == cVar.t && this.u == cVar.u && this.v == cVar.v && this.f1040y == cVar.f1040y && this.f1041z == cVar.f1041z && this.A == cVar.A && this.B == cVar.B && this.C == cVar.C && this.D == cVar.D;
        }

        public int hashCode() {
            int hashCode = (this.p.hashCode() + ((this.n.hashCode() + 217) * 31)) * 31;
            Object obj = this.q;
            int i = 0;
            int hashCode2 = (hashCode + (obj == null ? 0 : obj.hashCode())) * 31;
            o1.g gVar = this.f1039x;
            if (gVar != null) {
                i = gVar.hashCode();
            }
            long j2 = this.r;
            long j3 = this.f1038s;
            long j4 = this.t;
            long j5 = this.f1041z;
            long j6 = this.A;
            long j7 = this.D;
            return ((((((((((((((((((((((hashCode2 + i) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + ((int) (j4 ^ (j4 >>> 32)))) * 31) + (this.u ? 1 : 0)) * 31) + (this.v ? 1 : 0)) * 31) + (this.f1040y ? 1 : 0)) * 31) + ((int) (j5 ^ (j5 >>> 32)))) * 31) + ((int) (j6 ^ (j6 >>> 32)))) * 31) + this.B) * 31) + this.C) * 31) + ((int) (j7 ^ (j7 >>> 32)));
        }
    }

    public int a(boolean z2) {
        return q() ? -1 : 0;
    }

    public abstract int b(Object obj);

    public int c(boolean z2) {
        if (q()) {
            return -1;
        }
        return p() - 1;
    }

    public final int d(int i, b bVar, c cVar, int i2, boolean z2) {
        int i3 = g(i, bVar, false).l;
        if (n(i3, cVar).C != i) {
            return i + 1;
        }
        int e = e(i3, i2, z2);
        if (e == -1) {
            return -1;
        }
        return n(e, cVar).B;
    }

    public int e(int i, int i2, boolean z2) {
        if (i2 != 0) {
            if (i2 == 1) {
                return i;
            }
            if (i2 != 2) {
                throw new IllegalStateException();
            } else if (i == c(z2)) {
                return a(z2);
            } else {
                return i + 1;
            }
        } else if (i == c(z2)) {
            return -1;
        } else {
            return i + 1;
        }
    }

    public boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof o2)) {
            return false;
        }
        o2 o2Var = (o2) obj;
        if (!(o2Var.p() == p() && o2Var.i() == i())) {
            return false;
        }
        c cVar = new c();
        b bVar = new b();
        c cVar2 = new c();
        b bVar2 = new b();
        for (int i = 0; i < p(); i++) {
            if (!n(i, cVar).equals(o2Var.n(i, cVar2))) {
                return false;
            }
        }
        for (int i2 = 0; i2 < i(); i2++) {
            if (!g(i2, bVar, true).equals(o2Var.g(i2, bVar2, true))) {
                return false;
            }
        }
        return true;
    }

    public final b f(int i, b bVar) {
        return g(i, bVar, false);
    }

    public abstract b g(int i, b bVar, boolean z2);

    public b h(Object obj, b bVar) {
        return g(b(obj), bVar, true);
    }

    public int hashCode() {
        c cVar = new c();
        b bVar = new b();
        int p = p() + 217;
        for (int i = 0; i < p(); i++) {
            p = (p * 31) + n(i, cVar).hashCode();
        }
        int i2 = i() + (p * 31);
        for (int i3 = 0; i3 < i(); i3++) {
            i2 = (i2 * 31) + g(i3, bVar, true).hashCode();
        }
        return i2;
    }

    public abstract int i();

    @InlineMe(replacement = "this.getPeriodPositionUs(window, period, windowIndex, windowPositionUs)")
    @Deprecated
    public final Pair<Object, Long> j(c cVar, b bVar, int i, long j2) {
        Pair<Object, Long> k = k(cVar, bVar, i, j2, 0L);
        Objects.requireNonNull(k);
        return k;
    }

    @Nullable
    @InlineMe(replacement = "this.getPeriodPositionUs(window, period, windowIndex, windowPositionUs, defaultPositionProjectionUs)")
    @Deprecated
    public final Pair<Object, Long> k(c cVar, b bVar, int i, long j2, long j3) {
        d.t(i, 0, p());
        o(i, cVar, j3);
        if (j2 == -9223372036854775807L) {
            j2 = cVar.f1041z;
            if (j2 == -9223372036854775807L) {
                return null;
            }
        }
        int i2 = cVar.B;
        f(i2, bVar);
        while (i2 < cVar.C && bVar.n != j2) {
            int i3 = i2 + 1;
            if (f(i3, bVar).n > j2) {
                break;
            }
            i2 = i3;
        }
        g(i2, bVar, true);
        long j4 = j2 - bVar.n;
        long j5 = bVar.m;
        if (j5 != -9223372036854775807L) {
            j4 = Math.min(j4, j5 - 1);
        }
        long max = Math.max(0L, j4);
        Object obj = bVar.k;
        Objects.requireNonNull(obj);
        return Pair.create(obj, Long.valueOf(max));
    }

    public int l(int i, int i2, boolean z2) {
        if (i2 != 0) {
            if (i2 == 1) {
                return i;
            }
            if (i2 != 2) {
                throw new IllegalStateException();
            } else if (i == a(z2)) {
                return c(z2);
            } else {
                return i - 1;
            }
        } else if (i == a(z2)) {
            return -1;
        } else {
            return i - 1;
        }
    }

    public abstract Object m(int i);

    public final c n(int i, c cVar) {
        return o(i, cVar, 0L);
    }

    public abstract c o(int i, c cVar, long j2);

    public abstract int p();

    public final boolean q() {
        return p() == 0;
    }
}
