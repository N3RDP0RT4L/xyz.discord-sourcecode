package b.i.a.c;

import android.os.Handler;
import android.util.Pair;
import androidx.annotation.Nullable;
import b.i.a.c.a3.a0;
import b.i.a.c.a3.b0;
import b.i.a.c.a3.k0;
import b.i.a.c.a3.t;
import b.i.a.c.a3.u;
import b.i.a.c.a3.v;
import b.i.a.c.a3.w;
import b.i.a.c.a3.x;
import b.i.a.c.e3.a0;
import b.i.a.c.f3.e0;
import b.i.a.c.s2.g1;
import b.i.a.c.w2.s;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
/* compiled from: MediaSourceList.java */
/* loaded from: classes3.dex */
public final class u1 {
    public final d d;
    public final b0.a e;
    public final s.a f;
    public boolean j;
    @Nullable
    public a0 k;
    public k0 i = new k0.a(0, new Random());

    /* renamed from: b  reason: collision with root package name */
    public final IdentityHashMap<x, c> f1133b = new IdentityHashMap<>();
    public final Map<Object, c> c = new HashMap();
    public final List<c> a = new ArrayList();
    public final HashMap<c, b> g = new HashMap<>();
    public final Set<c> h = new HashSet();

    /* compiled from: MediaSourceList.java */
    /* loaded from: classes3.dex */
    public final class a implements b0, s {
        public final c j;
        public b0.a k;
        public s.a l;

        public a(c cVar) {
            this.k = u1.this.e;
            this.l = u1.this.f;
            this.j = cVar;
        }

        @Override // b.i.a.c.w2.s
        public void J(int i, @Nullable a0.a aVar) {
            if (a(i, aVar)) {
                this.l.b();
            }
        }

        @Override // b.i.a.c.w2.s
        public void S(int i, @Nullable a0.a aVar) {
            if (a(i, aVar)) {
                this.l.a();
            }
        }

        @Override // b.i.a.c.a3.b0
        public void X(int i, @Nullable a0.a aVar, t tVar, w wVar) {
            if (a(i, aVar)) {
                this.k.d(tVar, wVar);
            }
        }

        public final boolean a(int i, @Nullable a0.a aVar) {
            a0.a aVar2 = null;
            if (aVar != null) {
                c cVar = this.j;
                int i2 = 0;
                while (true) {
                    if (i2 >= cVar.c.size()) {
                        break;
                    } else if (cVar.c.get(i2).d == aVar.d) {
                        aVar2 = aVar.b(Pair.create(cVar.f1135b, aVar.a));
                        break;
                    } else {
                        i2++;
                    }
                }
                if (aVar2 == null) {
                    return false;
                }
            }
            int i3 = i + this.j.d;
            b0.a aVar3 = this.k;
            if (aVar3.a != i3 || !e0.a(aVar3.f803b, aVar2)) {
                this.k = u1.this.e.g(i3, aVar2, 0L);
            }
            s.a aVar4 = this.l;
            if (aVar4.a == i3 && e0.a(aVar4.f1152b, aVar2)) {
                return true;
            }
            this.l = u1.this.f.g(i3, aVar2);
            return true;
        }

        @Override // b.i.a.c.w2.s
        public void c0(int i, @Nullable a0.a aVar, int i2) {
            if (a(i, aVar)) {
                this.l.d(i2);
            }
        }

        @Override // b.i.a.c.w2.s
        public void d0(int i, @Nullable a0.a aVar) {
            if (a(i, aVar)) {
                this.l.f();
            }
        }

        @Override // b.i.a.c.a3.b0
        public void g0(int i, @Nullable a0.a aVar, t tVar, w wVar, IOException iOException, boolean z2) {
            if (a(i, aVar)) {
                this.k.e(tVar, wVar, iOException, z2);
            }
        }

        @Override // b.i.a.c.w2.s
        public void i0(int i, @Nullable a0.a aVar) {
            if (a(i, aVar)) {
                this.l.c();
            }
        }

        @Override // b.i.a.c.a3.b0
        public void o(int i, @Nullable a0.a aVar, w wVar) {
            if (a(i, aVar)) {
                this.k.b(wVar);
            }
        }

        @Override // b.i.a.c.a3.b0
        public void q(int i, @Nullable a0.a aVar, t tVar, w wVar) {
            if (a(i, aVar)) {
                this.k.c(tVar, wVar);
            }
        }

        @Override // b.i.a.c.w2.s
        public void u(int i, @Nullable a0.a aVar, Exception exc) {
            if (a(i, aVar)) {
                this.l.e(exc);
            }
        }

        @Override // b.i.a.c.a3.b0
        public void x(int i, @Nullable a0.a aVar, t tVar, w wVar) {
            if (a(i, aVar)) {
                this.k.f(tVar, wVar);
            }
        }
    }

    /* compiled from: MediaSourceList.java */
    /* loaded from: classes3.dex */
    public static final class b {
        public final b.i.a.c.a3.a0 a;

        /* renamed from: b  reason: collision with root package name */
        public final a0.b f1134b;
        public final a c;

        public b(b.i.a.c.a3.a0 a0Var, a0.b bVar, a aVar) {
            this.a = a0Var;
            this.f1134b = bVar;
            this.c = aVar;
        }
    }

    /* compiled from: MediaSourceList.java */
    /* loaded from: classes3.dex */
    public static final class c implements t1 {
        public final v a;
        public int d;
        public boolean e;
        public final List<a0.a> c = new ArrayList();

        /* renamed from: b  reason: collision with root package name */
        public final Object f1135b = new Object();

        public c(b.i.a.c.a3.a0 a0Var, boolean z2) {
            this.a = new v(a0Var, z2);
        }

        @Override // b.i.a.c.t1
        public o2 a() {
            return this.a.n;
        }

        @Override // b.i.a.c.t1
        public Object getUid() {
            return this.f1135b;
        }
    }

    /* compiled from: MediaSourceList.java */
    /* loaded from: classes3.dex */
    public interface d {
    }

    public u1(d dVar, @Nullable g1 g1Var, Handler handler) {
        this.d = dVar;
        b0.a aVar = new b0.a();
        this.e = aVar;
        s.a aVar2 = new s.a();
        this.f = aVar2;
        if (g1Var != null) {
            aVar.c.add(new b0.a.C0091a(handler, g1Var));
            aVar2.c.add(new s.a.C0100a(handler, g1Var));
        }
    }

    public o2 a(int i, List<c> list, k0 k0Var) {
        if (!list.isEmpty()) {
            this.i = k0Var;
            for (int i2 = i; i2 < list.size() + i; i2++) {
                c cVar = list.get(i2 - i);
                if (i2 > 0) {
                    c cVar2 = this.a.get(i2 - 1);
                    v.a aVar = cVar2.a.n;
                    cVar.d = aVar.p() + cVar2.d;
                    cVar.e = false;
                    cVar.c.clear();
                } else {
                    cVar.d = 0;
                    cVar.e = false;
                    cVar.c.clear();
                }
                b(i2, cVar.a.n.p());
                this.a.add(i2, cVar);
                this.c.put(cVar.f1135b, cVar);
                if (this.j) {
                    g(cVar);
                    if (this.f1133b.isEmpty()) {
                        this.h.add(cVar);
                    } else {
                        b bVar = this.g.get(cVar);
                        if (bVar != null) {
                            bVar.a.d(bVar.f1134b);
                        }
                    }
                }
            }
        }
        return c();
    }

    public final void b(int i, int i2) {
        while (i < this.a.size()) {
            this.a.get(i).d += i2;
            i++;
        }
    }

    public o2 c() {
        if (this.a.isEmpty()) {
            return o2.j;
        }
        int i = 0;
        for (int i2 = 0; i2 < this.a.size(); i2++) {
            c cVar = this.a.get(i2);
            cVar.d = i;
            i += cVar.a.n.p();
        }
        return new c2(this.a, this.i);
    }

    public final void d() {
        Iterator<c> it = this.h.iterator();
        while (it.hasNext()) {
            c next = it.next();
            if (next.c.isEmpty()) {
                b bVar = this.g.get(next);
                if (bVar != null) {
                    bVar.a.d(bVar.f1134b);
                }
                it.remove();
            }
        }
    }

    public int e() {
        return this.a.size();
    }

    public final void f(c cVar) {
        if (cVar.e && cVar.c.isEmpty()) {
            b remove = this.g.remove(cVar);
            Objects.requireNonNull(remove);
            remove.a.a(remove.f1134b);
            remove.a.c(remove.c);
            remove.a.g(remove.c);
            this.h.remove(cVar);
        }
    }

    public final void g(c cVar) {
        v vVar = cVar.a;
        a0.b n0Var = new a0.b() { // from class: b.i.a.c.n0
            @Override // b.i.a.c.a3.a0.b
            public final void a(b.i.a.c.a3.a0 a0Var, o2 o2Var) {
                ((h1) u1.this.d).q.f(22);
            }
        };
        a aVar = new a(cVar);
        this.g.put(cVar, new b(vVar, n0Var, aVar));
        Handler handler = new Handler(e0.o(), null);
        Objects.requireNonNull(vVar);
        b0.a aVar2 = vVar.c;
        Objects.requireNonNull(aVar2);
        aVar2.c.add(new b0.a.C0091a(handler, aVar));
        Handler handler2 = new Handler(e0.o(), null);
        s.a aVar3 = vVar.d;
        Objects.requireNonNull(aVar3);
        aVar3.c.add(new s.a.C0100a(handler2, aVar));
        vVar.l(n0Var, this.k);
    }

    public void h(x xVar) {
        c remove = this.f1133b.remove(xVar);
        Objects.requireNonNull(remove);
        remove.a.j(xVar);
        remove.c.remove(((u) xVar).j);
        if (!this.f1133b.isEmpty()) {
            d();
        }
        f(remove);
    }

    public final void i(int i, int i2) {
        for (int i3 = i2 - 1; i3 >= i; i3--) {
            c remove = this.a.remove(i3);
            this.c.remove(remove.f1135b);
            b(i3, -remove.a.n.p());
            remove.e = true;
            if (this.j) {
                f(remove);
            }
        }
    }
}
