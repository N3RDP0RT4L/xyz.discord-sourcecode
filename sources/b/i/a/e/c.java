package b.i.a.e;

import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.solver.widgets.analyzer.BasicMeasure;
import androidx.core.view.MarginLayoutParamsCompat;
import com.google.android.flexbox.FlexItem;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
/* compiled from: FlexboxHelper.java */
/* loaded from: classes3.dex */
public class c {
    public final b.i.a.e.a a;

    /* renamed from: b  reason: collision with root package name */
    public boolean[] f1324b;
    @Nullable
    public int[] c;
    @Nullable
    public long[] d;
    @Nullable
    public long[] e;

    /* compiled from: FlexboxHelper.java */
    /* loaded from: classes3.dex */
    public static class b {
        public List<b.i.a.e.b> a;

        /* renamed from: b  reason: collision with root package name */
        public int f1325b;

        public void a() {
            this.a = null;
            this.f1325b = 0;
        }
    }

    /* compiled from: FlexboxHelper.java */
    /* renamed from: b.i.a.e.c$c  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static class C0108c implements Comparable<C0108c> {
        public int j;
        public int k;

        public C0108c() {
        }

        @Override // java.lang.Comparable
        public int compareTo(@NonNull C0108c cVar) {
            C0108c cVar2 = cVar;
            int i = this.k;
            int i2 = cVar2.k;
            return i != i2 ? i - i2 : this.j - cVar2.j;
        }

        @NonNull
        public String toString() {
            StringBuilder R = b.d.b.a.a.R("Order{order=");
            R.append(this.k);
            R.append(", index=");
            return b.d.b.a.a.z(R, this.j, '}');
        }

        public C0108c(a aVar) {
        }
    }

    public c(b.i.a.e.a aVar) {
        this.a = aVar;
    }

    public void A(int i) {
        View c;
        if (i < this.a.getFlexItemCount()) {
            int flexDirection = this.a.getFlexDirection();
            if (this.a.getAlignItems() == 4) {
                int[] iArr = this.c;
                List<b.i.a.e.b> flexLinesInternal = this.a.getFlexLinesInternal();
                int size = flexLinesInternal.size();
                for (int i2 = iArr != null ? iArr[i] : 0; i2 < size; i2++) {
                    b.i.a.e.b bVar = flexLinesInternal.get(i2);
                    int i3 = bVar.h;
                    for (int i4 = 0; i4 < i3; i4++) {
                        int i5 = bVar.o + i4;
                        if (!(i4 >= this.a.getFlexItemCount() || (c = this.a.c(i5)) == null || c.getVisibility() == 8)) {
                            FlexItem flexItem = (FlexItem) c.getLayoutParams();
                            if (flexItem.u() == -1 || flexItem.u() == 4) {
                                if (flexDirection == 0 || flexDirection == 1) {
                                    z(c, bVar.g, i5);
                                } else if (flexDirection == 2 || flexDirection == 3) {
                                    y(c, bVar.g, i5);
                                } else {
                                    throw new IllegalArgumentException(b.d.b.a.a.p("Invalid flex direction: ", flexDirection));
                                }
                            }
                        }
                    }
                }
                return;
            }
            for (b.i.a.e.b bVar2 : this.a.getFlexLinesInternal()) {
                for (Integer num : bVar2.n) {
                    View c2 = this.a.c(num.intValue());
                    if (flexDirection == 0 || flexDirection == 1) {
                        z(c2, bVar2.g, num.intValue());
                    } else if (flexDirection == 2 || flexDirection == 3) {
                        y(c2, bVar2.g, num.intValue());
                    } else {
                        throw new IllegalArgumentException(b.d.b.a.a.p("Invalid flex direction: ", flexDirection));
                    }
                }
            }
        }
    }

    public final void B(int i, int i2, int i3, View view) {
        long[] jArr = this.d;
        if (jArr != null) {
            jArr[i] = (i2 & 4294967295L) | (i3 << 32);
        }
        long[] jArr2 = this.e;
        if (jArr2 != null) {
            jArr2[i] = (view.getMeasuredWidth() & 4294967295L) | (view.getMeasuredHeight() << 32);
        }
    }

    public final void a(List<b.i.a.e.b> list, b.i.a.e.b bVar, int i, int i2) {
        bVar.m = i2;
        this.a.b(bVar);
        bVar.p = i;
        list.add(bVar);
    }

    /* JADX WARN: Code restructure failed: missing block: B:86:0x022c, code lost:
        if (r2 < (r8 + r11)) goto L87;
     */
    /* JADX WARN: Removed duplicated region for block: B:105:0x02b9  */
    /* JADX WARN: Removed duplicated region for block: B:108:0x02d1  */
    /* JADX WARN: Removed duplicated region for block: B:109:0x02d4  */
    /* JADX WARN: Removed duplicated region for block: B:112:0x02e4  */
    /* JADX WARN: Removed duplicated region for block: B:113:0x02e7  */
    /* JADX WARN: Removed duplicated region for block: B:116:0x02f1  */
    /* JADX WARN: Removed duplicated region for block: B:119:0x02fb  */
    /* JADX WARN: Removed duplicated region for block: B:120:0x0300  */
    /* JADX WARN: Removed duplicated region for block: B:123:0x032f  */
    /* JADX WARN: Removed duplicated region for block: B:124:0x0334  */
    /* JADX WARN: Removed duplicated region for block: B:127:0x035b  */
    /* JADX WARN: Removed duplicated region for block: B:133:0x0394  */
    /* JADX WARN: Removed duplicated region for block: B:136:0x039f  */
    /* JADX WARN: Removed duplicated region for block: B:145:0x03c5 A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:90:0x0233  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void b(b.i.a.e.c.b r24, int r25, int r26, int r27, int r28, int r29, @androidx.annotation.Nullable java.util.List<b.i.a.e.b> r30) {
        /*
            Method dump skipped, instructions count: 999
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.e.c.b(b.i.a.e.c$b, int, int, int, int, int, java.util.List):void");
    }

    /* JADX WARN: Removed duplicated region for block: B:12:0x002d  */
    /* JADX WARN: Removed duplicated region for block: B:13:0x0032  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x0040  */
    /* JADX WARN: Removed duplicated region for block: B:20:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void c(android.view.View r7, int r8) {
        /*
            r6 = this;
            android.view.ViewGroup$LayoutParams r0 = r7.getLayoutParams()
            com.google.android.flexbox.FlexItem r0 = (com.google.android.flexbox.FlexItem) r0
            int r1 = r7.getMeasuredWidth()
            int r2 = r7.getMeasuredHeight()
            int r3 = r0.D()
            r4 = 1
            if (r1 >= r3) goto L1a
            int r1 = r0.D()
            goto L24
        L1a:
            int r3 = r0.t0()
            if (r1 <= r3) goto L26
            int r1 = r0.t0()
        L24:
            r3 = 1
            goto L27
        L26:
            r3 = 0
        L27:
            int r5 = r0.f0()
            if (r2 >= r5) goto L32
            int r2 = r0.f0()
            goto L3e
        L32:
            int r5 = r0.m0()
            if (r2 <= r5) goto L3d
            int r2 = r0.m0()
            goto L3e
        L3d:
            r4 = r3
        L3e:
            if (r4 == 0) goto L55
            r0 = 1073741824(0x40000000, float:2.0)
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r1, r0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r0)
            r7.measure(r1, r0)
            r6.B(r8, r1, r0, r7)
            b.i.a.e.a r0 = r6.a
            r0.e(r8, r7)
        L55:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.e.c.c(android.view.View, int):void");
    }

    public void d(List<b.i.a.e.b> list, int i) {
        int i2 = this.c[i];
        if (i2 == -1) {
            i2 = 0;
        }
        for (int size = list.size() - 1; size >= i2; size--) {
            list.remove(size);
        }
        int[] iArr = this.c;
        int length = iArr.length - 1;
        if (i > length) {
            Arrays.fill(iArr, -1);
        } else {
            Arrays.fill(iArr, i, length, -1);
        }
        long[] jArr = this.d;
        int length2 = jArr.length - 1;
        if (i > length2) {
            Arrays.fill(jArr, 0L);
        } else {
            Arrays.fill(jArr, i, length2, 0L);
        }
    }

    public final List<b.i.a.e.b> e(List<b.i.a.e.b> list, int i, int i2) {
        ArrayList arrayList = new ArrayList();
        b.i.a.e.b bVar = new b.i.a.e.b();
        bVar.g = (i - i2) / 2;
        int size = list.size();
        for (int i3 = 0; i3 < size; i3++) {
            if (i3 == 0) {
                arrayList.add(bVar);
            }
            arrayList.add(list.get(i3));
            if (i3 == list.size() - 1) {
                arrayList.add(bVar);
            }
        }
        return arrayList;
    }

    @NonNull
    public final List<C0108c> f(int i) {
        ArrayList arrayList = new ArrayList(i);
        for (int i2 = 0; i2 < i; i2++) {
            C0108c cVar = new C0108c(null);
            cVar.k = ((FlexItem) this.a.f(i2).getLayoutParams()).getOrder();
            cVar.j = i2;
            arrayList.add(cVar);
        }
        return arrayList;
    }

    public void g(int i, int i2, int i3) {
        int i4;
        int i5;
        int flexDirection = this.a.getFlexDirection();
        if (flexDirection == 0 || flexDirection == 1) {
            int mode = View.MeasureSpec.getMode(i2);
            int size = View.MeasureSpec.getSize(i2);
            i4 = mode;
            i5 = size;
        } else if (flexDirection == 2 || flexDirection == 3) {
            i4 = View.MeasureSpec.getMode(i);
            i5 = View.MeasureSpec.getSize(i);
        } else {
            throw new IllegalArgumentException(b.d.b.a.a.p("Invalid flex direction: ", flexDirection));
        }
        List<b.i.a.e.b> flexLinesInternal = this.a.getFlexLinesInternal();
        if (i4 == 1073741824) {
            int sumOfCrossSize = this.a.getSumOfCrossSize() + i3;
            int i6 = 0;
            if (flexLinesInternal.size() == 1) {
                flexLinesInternal.get(0).g = i5 - i3;
            } else if (flexLinesInternal.size() >= 2) {
                int alignContent = this.a.getAlignContent();
                if (alignContent == 1) {
                    int i7 = i5 - sumOfCrossSize;
                    b.i.a.e.b bVar = new b.i.a.e.b();
                    bVar.g = i7;
                    flexLinesInternal.add(0, bVar);
                } else if (alignContent == 2) {
                    this.a.setFlexLines(e(flexLinesInternal, i5, sumOfCrossSize));
                } else if (alignContent != 3) {
                    if (alignContent != 4) {
                        if (alignContent == 5 && sumOfCrossSize < i5) {
                            float size2 = (i5 - sumOfCrossSize) / flexLinesInternal.size();
                            int size3 = flexLinesInternal.size();
                            float f = 0.0f;
                            while (i6 < size3) {
                                b.i.a.e.b bVar2 = flexLinesInternal.get(i6);
                                float f2 = bVar2.g + size2;
                                if (i6 == flexLinesInternal.size() - 1) {
                                    f2 += f;
                                    f = 0.0f;
                                }
                                int round = Math.round(f2);
                                float f3 = (f2 - round) + f;
                                if (f3 > 1.0f) {
                                    round++;
                                    f3 -= 1.0f;
                                } else if (f3 < -1.0f) {
                                    round--;
                                    f3 += 1.0f;
                                }
                                f = f3;
                                bVar2.g = round;
                                i6++;
                            }
                        }
                    } else if (sumOfCrossSize >= i5) {
                        this.a.setFlexLines(e(flexLinesInternal, i5, sumOfCrossSize));
                    } else {
                        int size4 = (i5 - sumOfCrossSize) / (flexLinesInternal.size() * 2);
                        ArrayList arrayList = new ArrayList();
                        b.i.a.e.b bVar3 = new b.i.a.e.b();
                        bVar3.g = size4;
                        for (b.i.a.e.b bVar4 : flexLinesInternal) {
                            arrayList.add(bVar3);
                            arrayList.add(bVar4);
                            arrayList.add(bVar3);
                        }
                        this.a.setFlexLines(arrayList);
                    }
                } else if (sumOfCrossSize < i5) {
                    float size5 = (i5 - sumOfCrossSize) / (flexLinesInternal.size() - 1);
                    ArrayList arrayList2 = new ArrayList();
                    int size6 = flexLinesInternal.size();
                    float f4 = 0.0f;
                    while (i6 < size6) {
                        arrayList2.add(flexLinesInternal.get(i6));
                        if (i6 != flexLinesInternal.size() - 1) {
                            b.i.a.e.b bVar5 = new b.i.a.e.b();
                            if (i6 == flexLinesInternal.size() - 2) {
                                bVar5.g = Math.round(f4 + size5);
                                f4 = 0.0f;
                            } else {
                                bVar5.g = Math.round(size5);
                            }
                            int i8 = bVar5.g;
                            float f5 = (size5 - i8) + f4;
                            if (f5 > 1.0f) {
                                bVar5.g = i8 + 1;
                                f5 -= 1.0f;
                            } else if (f5 < -1.0f) {
                                bVar5.g = i8 - 1;
                                f5 += 1.0f;
                            }
                            arrayList2.add(bVar5);
                            f4 = f5;
                        }
                        i6++;
                    }
                    this.a.setFlexLines(arrayList2);
                }
            }
        }
    }

    public void h(int i, int i2, int i3) {
        int i4;
        int i5;
        int i6;
        int flexItemCount = this.a.getFlexItemCount();
        boolean[] zArr = this.f1324b;
        int i7 = 0;
        if (zArr == null) {
            if (flexItemCount < 10) {
                flexItemCount = 10;
            }
            this.f1324b = new boolean[flexItemCount];
        } else if (zArr.length < flexItemCount) {
            int length = zArr.length * 2;
            if (length >= flexItemCount) {
                flexItemCount = length;
            }
            this.f1324b = new boolean[flexItemCount];
        } else {
            Arrays.fill(zArr, false);
        }
        if (i3 < this.a.getFlexItemCount()) {
            int flexDirection = this.a.getFlexDirection();
            int flexDirection2 = this.a.getFlexDirection();
            if (flexDirection2 == 0 || flexDirection2 == 1) {
                int mode = View.MeasureSpec.getMode(i);
                i5 = View.MeasureSpec.getSize(i);
                int largestMainSize = this.a.getLargestMainSize();
                if (mode != 1073741824 && largestMainSize <= i5) {
                    i5 = largestMainSize;
                }
                i6 = this.a.getPaddingLeft();
                i4 = this.a.getPaddingRight();
            } else if (flexDirection2 == 2 || flexDirection2 == 3) {
                int mode2 = View.MeasureSpec.getMode(i2);
                i5 = View.MeasureSpec.getSize(i2);
                if (mode2 != 1073741824) {
                    i5 = this.a.getLargestMainSize();
                }
                i6 = this.a.getPaddingTop();
                i4 = this.a.getPaddingBottom();
            } else {
                throw new IllegalArgumentException(b.d.b.a.a.p("Invalid flex direction: ", flexDirection));
            }
            int i8 = i4 + i6;
            int[] iArr = this.c;
            if (iArr != null) {
                i7 = iArr[i3];
            }
            List<b.i.a.e.b> flexLinesInternal = this.a.getFlexLinesInternal();
            int size = flexLinesInternal.size();
            while (i7 < size) {
                b.i.a.e.b bVar = flexLinesInternal.get(i7);
                int i9 = bVar.e;
                if (i9 < i5 && bVar.q) {
                    l(i, i2, bVar, i5, i8, false);
                } else if (i9 > i5 && bVar.r) {
                    w(i, i2, bVar, i5, i8, false);
                }
                i7++;
            }
        }
    }

    public void i(int i) {
        int[] iArr = this.c;
        if (iArr == null) {
            if (i < 10) {
                i = 10;
            }
            this.c = new int[i];
        } else if (iArr.length < i) {
            int length = iArr.length * 2;
            if (length >= i) {
                i = length;
            }
            this.c = Arrays.copyOf(iArr, i);
        }
    }

    public void j(int i) {
        long[] jArr = this.d;
        if (jArr == null) {
            if (i < 10) {
                i = 10;
            }
            this.d = new long[i];
        } else if (jArr.length < i) {
            int length = jArr.length * 2;
            if (length >= i) {
                i = length;
            }
            this.d = Arrays.copyOf(jArr, i);
        }
    }

    public void k(int i) {
        long[] jArr = this.e;
        if (jArr == null) {
            if (i < 10) {
                i = 10;
            }
            this.e = new long[i];
        } else if (jArr.length < i) {
            int length = jArr.length * 2;
            if (length >= i) {
                i = length;
            }
            this.e = Arrays.copyOf(jArr, i);
        }
    }

    public final void l(int i, int i2, b.i.a.e.b bVar, int i3, int i4, boolean z2) {
        int i5;
        int i6;
        int i7;
        double d;
        int i8;
        double d2;
        float f = bVar.j;
        float f2 = 0.0f;
        if (f > 0.0f && i3 >= (i5 = bVar.e)) {
            float f3 = (i3 - i5) / f;
            bVar.e = i4 + bVar.f;
            if (!z2) {
                bVar.g = Integer.MIN_VALUE;
            }
            int i9 = 0;
            boolean z3 = false;
            int i10 = 0;
            float f4 = 0.0f;
            while (i9 < bVar.h) {
                int i11 = bVar.o + i9;
                View c = this.a.c(i11);
                if (c == null || c.getVisibility() == 8) {
                    i6 = i5;
                } else {
                    FlexItem flexItem = (FlexItem) c.getLayoutParams();
                    int flexDirection = this.a.getFlexDirection();
                    if (flexDirection == 0 || flexDirection == 1) {
                        i6 = i5;
                        int measuredWidth = c.getMeasuredWidth();
                        long[] jArr = this.e;
                        if (jArr != null) {
                            measuredWidth = (int) jArr[i11];
                        }
                        int measuredHeight = c.getMeasuredHeight();
                        long[] jArr2 = this.e;
                        if (jArr2 != null) {
                            measuredHeight = m(jArr2[i11]);
                        }
                        if (!this.f1324b[i11] && flexItem.S() > 0.0f) {
                            float S = (flexItem.S() * f3) + measuredWidth;
                            if (i9 == bVar.h - 1) {
                                S += f4;
                                f4 = 0.0f;
                            }
                            int round = Math.round(S);
                            if (round > flexItem.t0()) {
                                round = flexItem.t0();
                                this.f1324b[i11] = true;
                                bVar.j -= flexItem.S();
                                z3 = true;
                            } else {
                                float f5 = (S - round) + f4;
                                double d3 = f5;
                                if (d3 > 1.0d) {
                                    round++;
                                    d = d3 - 1.0d;
                                } else {
                                    if (d3 < -1.0d) {
                                        round--;
                                        d = d3 + 1.0d;
                                    }
                                    f4 = f5;
                                }
                                f5 = (float) d;
                                f4 = f5;
                            }
                            int n = n(i2, flexItem, bVar.m);
                            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(round, BasicMeasure.EXACTLY);
                            c.measure(makeMeasureSpec, n);
                            int measuredWidth2 = c.getMeasuredWidth();
                            int measuredHeight2 = c.getMeasuredHeight();
                            B(i11, makeMeasureSpec, n, c);
                            this.a.e(i11, c);
                            measuredWidth = measuredWidth2;
                            measuredHeight = measuredHeight2;
                        }
                        int max = Math.max(i10, this.a.j(c) + flexItem.I() + flexItem.N() + measuredHeight);
                        bVar.e = flexItem.d0() + flexItem.J() + measuredWidth + bVar.e;
                        i7 = max;
                    } else {
                        int measuredHeight3 = c.getMeasuredHeight();
                        long[] jArr3 = this.e;
                        if (jArr3 != null) {
                            measuredHeight3 = m(jArr3[i11]);
                        }
                        int measuredWidth3 = c.getMeasuredWidth();
                        long[] jArr4 = this.e;
                        if (jArr4 != null) {
                            measuredWidth3 = (int) jArr4[i11];
                        }
                        if (this.f1324b[i11] || flexItem.S() <= f2) {
                            i8 = i5;
                        } else {
                            float S2 = (flexItem.S() * f3) + measuredHeight3;
                            if (i9 == bVar.h - 1) {
                                S2 += f4;
                                f4 = 0.0f;
                            }
                            int round2 = Math.round(S2);
                            if (round2 > flexItem.m0()) {
                                round2 = flexItem.m0();
                                this.f1324b[i11] = true;
                                bVar.j -= flexItem.S();
                                i8 = i5;
                                z3 = true;
                            } else {
                                float f6 = (S2 - round2) + f4;
                                i8 = i5;
                                double d4 = f6;
                                if (d4 > 1.0d) {
                                    round2++;
                                    d2 = d4 - 1.0d;
                                } else if (d4 < -1.0d) {
                                    round2--;
                                    d2 = d4 + 1.0d;
                                } else {
                                    f4 = f6;
                                }
                                f4 = (float) d2;
                            }
                            int o = o(i, flexItem, bVar.m);
                            int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(round2, BasicMeasure.EXACTLY);
                            c.measure(o, makeMeasureSpec2);
                            measuredWidth3 = c.getMeasuredWidth();
                            int measuredHeight4 = c.getMeasuredHeight();
                            B(i11, o, makeMeasureSpec2, c);
                            this.a.e(i11, c);
                            measuredHeight3 = measuredHeight4;
                        }
                        i7 = Math.max(i10, this.a.j(c) + flexItem.d0() + flexItem.J() + measuredWidth3);
                        bVar.e = flexItem.I() + flexItem.N() + measuredHeight3 + bVar.e;
                        i6 = i8;
                    }
                    bVar.g = Math.max(bVar.g, i7);
                    i10 = i7;
                }
                i9++;
                i5 = i6;
                f2 = 0.0f;
            }
            int i12 = i5;
            if (z3 && i12 != bVar.e) {
                l(i, i2, bVar, i3, i4, true);
            }
        }
    }

    public int m(long j) {
        return (int) (j >> 32);
    }

    public final int n(int i, FlexItem flexItem, int i2) {
        b.i.a.e.a aVar = this.a;
        int h = aVar.h(i, flexItem.I() + flexItem.N() + this.a.getPaddingBottom() + aVar.getPaddingTop() + i2, flexItem.getHeight());
        int size = View.MeasureSpec.getSize(h);
        if (size > flexItem.m0()) {
            return View.MeasureSpec.makeMeasureSpec(flexItem.m0(), View.MeasureSpec.getMode(h));
        }
        return size < flexItem.f0() ? View.MeasureSpec.makeMeasureSpec(flexItem.f0(), View.MeasureSpec.getMode(h)) : h;
    }

    public final int o(int i, FlexItem flexItem, int i2) {
        b.i.a.e.a aVar = this.a;
        int d = aVar.d(i, flexItem.d0() + flexItem.J() + this.a.getPaddingRight() + aVar.getPaddingLeft() + i2, flexItem.getWidth());
        int size = View.MeasureSpec.getSize(d);
        if (size > flexItem.t0()) {
            return View.MeasureSpec.makeMeasureSpec(flexItem.t0(), View.MeasureSpec.getMode(d));
        }
        return size < flexItem.D() ? View.MeasureSpec.makeMeasureSpec(flexItem.D(), View.MeasureSpec.getMode(d)) : d;
    }

    public final int p(FlexItem flexItem, boolean z2) {
        if (z2) {
            return flexItem.I();
        }
        return flexItem.d0();
    }

    public final int q(FlexItem flexItem, boolean z2) {
        if (z2) {
            return flexItem.d0();
        }
        return flexItem.I();
    }

    public final int r(FlexItem flexItem, boolean z2) {
        if (z2) {
            return flexItem.N();
        }
        return flexItem.J();
    }

    public final int s(FlexItem flexItem, boolean z2) {
        if (z2) {
            return flexItem.J();
        }
        return flexItem.N();
    }

    public final boolean t(int i, int i2, b.i.a.e.b bVar) {
        return i == i2 - 1 && bVar.a() != 0;
    }

    public void u(View view, b.i.a.e.b bVar, int i, int i2, int i3, int i4) {
        FlexItem flexItem = (FlexItem) view.getLayoutParams();
        int alignItems = this.a.getAlignItems();
        if (flexItem.u() != -1) {
            alignItems = flexItem.u();
        }
        int i5 = bVar.g;
        if (alignItems != 0) {
            if (alignItems != 1) {
                if (alignItems == 2) {
                    int N = ((flexItem.N() + (i5 - view.getMeasuredHeight())) - flexItem.I()) / 2;
                    if (this.a.getFlexWrap() != 2) {
                        int i6 = i2 + N;
                        view.layout(i, i6, i3, view.getMeasuredHeight() + i6);
                        return;
                    }
                    int i7 = i2 - N;
                    view.layout(i, i7, i3, view.getMeasuredHeight() + i7);
                    return;
                } else if (alignItems != 3) {
                    if (alignItems != 4) {
                        return;
                    }
                } else if (this.a.getFlexWrap() != 2) {
                    int max = Math.max(bVar.l - view.getBaseline(), flexItem.N());
                    view.layout(i, i2 + max, i3, i4 + max);
                    return;
                } else {
                    int max2 = Math.max(view.getBaseline() + (bVar.l - view.getMeasuredHeight()), flexItem.I());
                    view.layout(i, i2 - max2, i3, i4 - max2);
                    return;
                }
            } else if (this.a.getFlexWrap() != 2) {
                int i8 = i2 + i5;
                view.layout(i, (i8 - view.getMeasuredHeight()) - flexItem.I(), i3, i8 - flexItem.I());
                return;
            } else {
                view.layout(i, flexItem.N() + view.getMeasuredHeight() + (i2 - i5), i3, flexItem.N() + view.getMeasuredHeight() + (i4 - i5));
                return;
            }
        }
        if (this.a.getFlexWrap() != 2) {
            view.layout(i, flexItem.N() + i2, i3, flexItem.N() + i4);
        } else {
            view.layout(i, i2 - flexItem.I(), i3, i4 - flexItem.I());
        }
    }

    public void v(View view, b.i.a.e.b bVar, boolean z2, int i, int i2, int i3, int i4) {
        FlexItem flexItem = (FlexItem) view.getLayoutParams();
        int alignItems = this.a.getAlignItems();
        if (flexItem.u() != -1) {
            alignItems = flexItem.u();
        }
        int i5 = bVar.g;
        if (alignItems != 0) {
            if (alignItems != 1) {
                if (alignItems == 2) {
                    ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
                    int marginStart = ((MarginLayoutParamsCompat.getMarginStart(marginLayoutParams) + (i5 - view.getMeasuredWidth())) - MarginLayoutParamsCompat.getMarginEnd(marginLayoutParams)) / 2;
                    if (!z2) {
                        view.layout(i + marginStart, i2, i3 + marginStart, i4);
                        return;
                    } else {
                        view.layout(i - marginStart, i2, i3 - marginStart, i4);
                        return;
                    }
                } else if (!(alignItems == 3 || alignItems == 4)) {
                    return;
                }
            } else if (!z2) {
                view.layout(((i + i5) - view.getMeasuredWidth()) - flexItem.d0(), i2, ((i3 + i5) - view.getMeasuredWidth()) - flexItem.d0(), i4);
                return;
            } else {
                int J = flexItem.J();
                view.layout(flexItem.J() + view.getMeasuredWidth() + (i - i5), i2, J + view.getMeasuredWidth() + (i3 - i5), i4);
                return;
            }
        }
        if (!z2) {
            view.layout(flexItem.J() + i, i2, flexItem.J() + i3, i4);
        } else {
            view.layout(i - flexItem.d0(), i2, i3 - flexItem.d0(), i4);
        }
    }

    public final void w(int i, int i2, b.i.a.e.b bVar, int i3, int i4, boolean z2) {
        int i5;
        int i6;
        int i7;
        int i8;
        int i9 = bVar.e;
        float f = bVar.k;
        float f2 = 0.0f;
        if (f > 0.0f && i3 <= i9) {
            float f3 = (i9 - i3) / f;
            bVar.e = i4 + bVar.f;
            if (!z2) {
                bVar.g = Integer.MIN_VALUE;
            }
            int i10 = 0;
            boolean z3 = false;
            int i11 = 0;
            float f4 = 0.0f;
            while (i10 < bVar.h) {
                int i12 = bVar.o + i10;
                View c = this.a.c(i12);
                if (c == null || c.getVisibility() == 8) {
                    i6 = i9;
                    i5 = i10;
                } else {
                    FlexItem flexItem = (FlexItem) c.getLayoutParams();
                    int flexDirection = this.a.getFlexDirection();
                    if (flexDirection == 0 || flexDirection == 1) {
                        i6 = i9;
                        int i13 = i10;
                        int measuredWidth = c.getMeasuredWidth();
                        long[] jArr = this.e;
                        if (jArr != null) {
                            measuredWidth = (int) jArr[i12];
                        }
                        int measuredHeight = c.getMeasuredHeight();
                        long[] jArr2 = this.e;
                        if (jArr2 != null) {
                            measuredHeight = m(jArr2[i12]);
                        }
                        if (this.f1324b[i12] || flexItem.x() <= 0.0f) {
                            i5 = i13;
                        } else {
                            float x2 = measuredWidth - (flexItem.x() * f3);
                            i5 = i13;
                            if (i5 == bVar.h - 1) {
                                x2 += f4;
                                f4 = 0.0f;
                            }
                            int round = Math.round(x2);
                            if (round < flexItem.D()) {
                                i8 = flexItem.D();
                                this.f1324b[i12] = true;
                                bVar.k -= flexItem.x();
                                z3 = true;
                            } else {
                                float f5 = (x2 - round) + f4;
                                double d = f5;
                                if (d > 1.0d) {
                                    round++;
                                    f5 -= 1.0f;
                                } else if (d < -1.0d) {
                                    round--;
                                    f5 += 1.0f;
                                }
                                f4 = f5;
                                i8 = round;
                            }
                            int n = n(i2, flexItem, bVar.m);
                            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i8, BasicMeasure.EXACTLY);
                            c.measure(makeMeasureSpec, n);
                            int measuredWidth2 = c.getMeasuredWidth();
                            int measuredHeight2 = c.getMeasuredHeight();
                            B(i12, makeMeasureSpec, n, c);
                            this.a.e(i12, c);
                            measuredWidth = measuredWidth2;
                            measuredHeight = measuredHeight2;
                        }
                        int max = Math.max(i11, this.a.j(c) + flexItem.I() + flexItem.N() + measuredHeight);
                        bVar.e = flexItem.d0() + flexItem.J() + measuredWidth + bVar.e;
                        i7 = max;
                    } else {
                        int measuredHeight3 = c.getMeasuredHeight();
                        long[] jArr3 = this.e;
                        if (jArr3 != null) {
                            measuredHeight3 = m(jArr3[i12]);
                        }
                        int measuredWidth3 = c.getMeasuredWidth();
                        long[] jArr4 = this.e;
                        if (jArr4 != null) {
                            measuredWidth3 = (int) jArr4[i12];
                        }
                        if (this.f1324b[i12] || flexItem.x() <= f2) {
                            i6 = i9;
                            i5 = i10;
                        } else {
                            float x3 = measuredHeight3 - (flexItem.x() * f3);
                            if (i10 == bVar.h - 1) {
                                x3 += f4;
                                f4 = 0.0f;
                            }
                            int round2 = Math.round(x3);
                            if (round2 < flexItem.f0()) {
                                int f02 = flexItem.f0();
                                this.f1324b[i12] = true;
                                bVar.k -= flexItem.x();
                                i5 = i10;
                                round2 = f02;
                                z3 = true;
                                i6 = i9;
                            } else {
                                float f6 = (x3 - round2) + f4;
                                i6 = i9;
                                i5 = i10;
                                double d2 = f6;
                                if (d2 > 1.0d) {
                                    round2++;
                                    f6 -= 1.0f;
                                } else if (d2 < -1.0d) {
                                    round2--;
                                    f6 += 1.0f;
                                }
                                f4 = f6;
                            }
                            int o = o(i, flexItem, bVar.m);
                            int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(round2, BasicMeasure.EXACTLY);
                            c.measure(o, makeMeasureSpec2);
                            measuredWidth3 = c.getMeasuredWidth();
                            int measuredHeight4 = c.getMeasuredHeight();
                            B(i12, o, makeMeasureSpec2, c);
                            this.a.e(i12, c);
                            measuredHeight3 = measuredHeight4;
                        }
                        i7 = Math.max(i11, this.a.j(c) + flexItem.d0() + flexItem.J() + measuredWidth3);
                        bVar.e = flexItem.I() + flexItem.N() + measuredHeight3 + bVar.e;
                    }
                    bVar.g = Math.max(bVar.g, i7);
                    i11 = i7;
                }
                i10 = i5 + 1;
                i9 = i6;
                f2 = 0.0f;
            }
            int i14 = i9;
            if (z3 && i14 != bVar.e) {
                w(i, i2, bVar, i3, i4, true);
            }
        }
    }

    public final int[] x(int i, List<C0108c> list, SparseIntArray sparseIntArray) {
        Collections.sort(list);
        sparseIntArray.clear();
        int[] iArr = new int[i];
        int i2 = 0;
        for (C0108c cVar : list) {
            int i3 = cVar.j;
            iArr[i2] = i3;
            sparseIntArray.append(i3, cVar.k);
            i2++;
        }
        return iArr;
    }

    public final void y(View view, int i, int i2) {
        int i3;
        FlexItem flexItem = (FlexItem) view.getLayoutParams();
        int min = Math.min(Math.max(((i - flexItem.J()) - flexItem.d0()) - this.a.j(view), flexItem.D()), flexItem.t0());
        long[] jArr = this.e;
        if (jArr != null) {
            i3 = m(jArr[i2]);
        } else {
            i3 = view.getMeasuredHeight();
        }
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i3, BasicMeasure.EXACTLY);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(min, BasicMeasure.EXACTLY);
        view.measure(makeMeasureSpec2, makeMeasureSpec);
        B(i2, makeMeasureSpec2, makeMeasureSpec, view);
        this.a.e(i2, view);
    }

    public final void z(View view, int i, int i2) {
        int i3;
        FlexItem flexItem = (FlexItem) view.getLayoutParams();
        int min = Math.min(Math.max(((i - flexItem.N()) - flexItem.I()) - this.a.j(view), flexItem.f0()), flexItem.m0());
        long[] jArr = this.e;
        if (jArr != null) {
            i3 = (int) jArr[i2];
        } else {
            i3 = view.getMeasuredWidth();
        }
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i3, BasicMeasure.EXACTLY);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(min, BasicMeasure.EXACTLY);
        view.measure(makeMeasureSpec, makeMeasureSpec2);
        B(i2, makeMeasureSpec, makeMeasureSpec2, view);
        this.a.e(i2, view);
    }
}
