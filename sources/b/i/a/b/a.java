package b.i.a.b;

import androidx.annotation.Nullable;
import java.util.Objects;
/* compiled from: AutoValue_Event.java */
/* loaded from: classes.dex */
public final class a<T> extends c<T> {
    public final T a;

    /* renamed from: b  reason: collision with root package name */
    public final d f738b;

    public a(@Nullable Integer num, T t, d dVar) {
        Objects.requireNonNull(t, "Null payload");
        this.a = t;
        Objects.requireNonNull(dVar, "Null priority");
        this.f738b = dVar;
    }

    @Override // b.i.a.b.c
    @Nullable
    public Integer a() {
        return null;
    }

    @Override // b.i.a.b.c
    public T b() {
        return this.a;
    }

    @Override // b.i.a.b.c
    public d c() {
        return this.f738b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof c)) {
            return false;
        }
        c cVar = (c) obj;
        return cVar.a() == null && this.a.equals(cVar.b()) && this.f738b.equals(cVar.c());
    }

    public int hashCode() {
        return this.f738b.hashCode() ^ (((-721379959) ^ this.a.hashCode()) * 1000003);
    }

    public String toString() {
        return "Event{code=" + ((Object) null) + ", payload=" + this.a + ", priority=" + this.f738b + "}";
    }
}
