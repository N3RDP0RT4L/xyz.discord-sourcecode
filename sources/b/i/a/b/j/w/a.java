package b.i.a.b.j.w;

import android.util.SparseArray;
import androidx.annotation.NonNull;
import b.i.a.b.d;
import java.util.EnumMap;
/* compiled from: PriorityMapping.java */
/* loaded from: classes3.dex */
public final class a {
    public static SparseArray<d> a = new SparseArray<>();

    /* renamed from: b  reason: collision with root package name */
    public static EnumMap<d, Integer> f800b;

    static {
        EnumMap<d, Integer> enumMap = new EnumMap<>(d.class);
        f800b = enumMap;
        enumMap.put((EnumMap<d, Integer>) d.DEFAULT, (d) 0);
        f800b.put((EnumMap<d, Integer>) d.VERY_LOW, (d) 1);
        f800b.put((EnumMap<d, Integer>) d.HIGHEST, (d) 2);
        for (d dVar : f800b.keySet()) {
            a.append(f800b.get(dVar).intValue(), dVar);
        }
    }

    public static int a(@NonNull d dVar) {
        Integer num = f800b.get(dVar);
        if (num != null) {
            return num.intValue();
        }
        throw new IllegalStateException("PriorityMapping is missing known Priority value " + dVar);
    }

    @NonNull
    public static d b(int i) {
        d dVar = a.get(i);
        if (dVar != null) {
            return dVar;
        }
        throw new IllegalArgumentException(b.d.b.a.a.p("Unknown Priority for value ", i));
    }
}
