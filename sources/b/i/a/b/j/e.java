package b.i.a.b.j;

import androidx.annotation.NonNull;
import b.d.b.a.a;
import b.i.a.b.b;
import java.util.Arrays;
import java.util.Objects;
/* compiled from: EncodedPayload.java */
/* loaded from: classes3.dex */
public final class e {
    public final b a;

    /* renamed from: b  reason: collision with root package name */
    public final byte[] f762b;

    public e(@NonNull b bVar, @NonNull byte[] bArr) {
        Objects.requireNonNull(bVar, "encoding is null");
        Objects.requireNonNull(bArr, "bytes is null");
        this.a = bVar;
        this.f762b = bArr;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof e)) {
            return false;
        }
        e eVar = (e) obj;
        if (!this.a.equals(eVar.a)) {
            return false;
        }
        return Arrays.equals(this.f762b, eVar.f762b);
    }

    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ Arrays.hashCode(this.f762b);
    }

    public String toString() {
        StringBuilder R = a.R("EncodedPayload{encoding=");
        R.append(this.a);
        R.append(", bytes=[...]}");
        return R.toString();
    }
}
