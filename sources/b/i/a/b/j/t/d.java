package b.i.a.b.j.t;

import b.i.a.b.j.q.e;
import b.i.a.b.j.t.h.r;
import b.i.a.b.j.t.i.c;
import c0.a.a;
import java.util.concurrent.Executor;
/* compiled from: DefaultScheduler_Factory.java */
/* loaded from: classes3.dex */
public final class d implements a {
    public final a<Executor> a;

    /* renamed from: b  reason: collision with root package name */
    public final a<e> f778b;
    public final a<r> c;
    public final a<c> d;
    public final a<b.i.a.b.j.u.a> e;

    public d(a<Executor> aVar, a<e> aVar2, a<r> aVar3, a<c> aVar4, a<b.i.a.b.j.u.a> aVar5) {
        this.a = aVar;
        this.f778b = aVar2;
        this.c = aVar3;
        this.d = aVar4;
        this.e = aVar5;
    }

    @Override // c0.a.a
    public Object get() {
        return new c(this.a.get(), this.f778b.get(), this.c.get(), this.d.get(), this.e.get());
    }
}
