package b.i.a.b.j.t.i;

import android.database.Cursor;
import b.i.a.b.b;
import b.i.a.b.j.t.i.t;
import java.util.ArrayList;
/* compiled from: SQLiteEventStore.java */
/* loaded from: classes3.dex */
public final /* synthetic */ class l implements t.b {
    public static final l a = new l();

    @Override // b.i.a.b.j.t.i.t.b
    public Object apply(Object obj) {
        Cursor cursor = (Cursor) obj;
        b bVar = t.j;
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (cursor.moveToNext()) {
            byte[] blob = cursor.getBlob(0);
            arrayList.add(blob);
            i += blob.length;
        }
        byte[] bArr = new byte[i];
        int i2 = 0;
        for (int i3 = 0; i3 < arrayList.size(); i3++) {
            byte[] bArr2 = (byte[]) arrayList.get(i3);
            System.arraycopy(bArr2, 0, bArr, i2, bArr2.length);
            i2 += bArr2.length;
        }
        return bArr;
    }
}
