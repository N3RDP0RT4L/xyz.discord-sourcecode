package b.i.a.b.j.t.i;
/* compiled from: AutoValue_EventStoreConfig.java */
/* loaded from: classes3.dex */
public final class a extends d {

    /* renamed from: b  reason: collision with root package name */
    public final long f791b;
    public final int c;
    public final int d;
    public final long e;
    public final int f;

    public a(long j, int i, int i2, long j2, int i3, C0089a aVar) {
        this.f791b = j;
        this.c = i;
        this.d = i2;
        this.e = j2;
        this.f = i3;
    }

    @Override // b.i.a.b.j.t.i.d
    public int a() {
        return this.d;
    }

    @Override // b.i.a.b.j.t.i.d
    public long b() {
        return this.e;
    }

    @Override // b.i.a.b.j.t.i.d
    public int c() {
        return this.c;
    }

    @Override // b.i.a.b.j.t.i.d
    public int d() {
        return this.f;
    }

    @Override // b.i.a.b.j.t.i.d
    public long e() {
        return this.f791b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof d)) {
            return false;
        }
        d dVar = (d) obj;
        return this.f791b == dVar.e() && this.c == dVar.c() && this.d == dVar.a() && this.e == dVar.b() && this.f == dVar.d();
    }

    public int hashCode() {
        long j = this.f791b;
        long j2 = this.e;
        return this.f ^ ((((((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ this.c) * 1000003) ^ this.d) * 1000003) ^ ((int) ((j2 >>> 32) ^ j2))) * 1000003);
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("EventStoreConfig{maxStorageSizeInBytes=");
        R.append(this.f791b);
        R.append(", loadBatchSize=");
        R.append(this.c);
        R.append(", criticalSectionEnterTimeoutMs=");
        R.append(this.d);
        R.append(", eventCleanUpAge=");
        R.append(this.e);
        R.append(", maxBlobByteSizePerRow=");
        return b.d.b.a.a.A(R, this.f, "}");
    }
}
