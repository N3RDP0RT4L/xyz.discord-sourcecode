package b.i.a.b.j.t.i;

import c0.a.a;
/* compiled from: SQLiteEventStore_Factory.java */
/* loaded from: classes3.dex */
public final class u implements a {
    public final a<b.i.a.b.j.v.a> a;

    /* renamed from: b  reason: collision with root package name */
    public final a<b.i.a.b.j.v.a> f799b;
    public final a<d> c;
    public final a<z> d;

    public u(a<b.i.a.b.j.v.a> aVar, a<b.i.a.b.j.v.a> aVar2, a<d> aVar3, a<z> aVar4) {
        this.a = aVar;
        this.f799b = aVar2;
        this.c = aVar3;
        this.d = aVar4;
    }

    @Override // c0.a.a
    public Object get() {
        return new t(this.a.get(), this.f799b.get(), this.c.get(), this.d.get());
    }
}
