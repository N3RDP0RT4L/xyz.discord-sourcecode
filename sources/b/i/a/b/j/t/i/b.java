package b.i.a.b.j.t.i;

import b.d.b.a.a;
import b.i.a.b.j.f;
import b.i.a.b.j.i;
import java.util.Objects;
/* compiled from: AutoValue_PersistedEvent.java */
/* loaded from: classes3.dex */
public final class b extends h {
    public final long a;

    /* renamed from: b  reason: collision with root package name */
    public final i f793b;
    public final f c;

    public b(long j, i iVar, f fVar) {
        this.a = j;
        Objects.requireNonNull(iVar, "Null transportContext");
        this.f793b = iVar;
        Objects.requireNonNull(fVar, "Null event");
        this.c = fVar;
    }

    @Override // b.i.a.b.j.t.i.h
    public f a() {
        return this.c;
    }

    @Override // b.i.a.b.j.t.i.h
    public long b() {
        return this.a;
    }

    @Override // b.i.a.b.j.t.i.h
    public i c() {
        return this.f793b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof h)) {
            return false;
        }
        h hVar = (h) obj;
        return this.a == hVar.b() && this.f793b.equals(hVar.c()) && this.c.equals(hVar.a());
    }

    public int hashCode() {
        long j = this.a;
        return this.c.hashCode() ^ ((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ this.f793b.hashCode()) * 1000003);
    }

    public String toString() {
        StringBuilder R = a.R("PersistedEvent{id=");
        R.append(this.a);
        R.append(", transportContext=");
        R.append(this.f793b);
        R.append(", event=");
        R.append(this.c);
        R.append("}");
        return R.toString();
    }
}
