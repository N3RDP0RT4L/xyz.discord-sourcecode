package b.i.a.b.j.t.i;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.os.SystemClock;
import android.util.Base64;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.WorkerThread;
import androidx.core.app.NotificationCompat;
import b.i.a.b.b;
import b.i.a.b.j.a;
import b.i.a.b.j.e;
import b.i.a.b.j.f;
import b.i.a.b.j.i;
import b.i.a.b.j.t.i.t;
import b.i.a.b.j.u.a;
import com.discord.models.domain.ModelAuditLogEntry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
/* compiled from: SQLiteEventStore.java */
@WorkerThread
/* loaded from: classes3.dex */
public class t implements b.i.a.b.j.t.i.c, b.i.a.b.j.u.a {
    public static final b.i.a.b.b j = new b.i.a.b.b("proto");
    public final z k;
    public final b.i.a.b.j.v.a l;
    public final b.i.a.b.j.v.a m;
    public final b.i.a.b.j.t.i.d n;

    /* compiled from: SQLiteEventStore.java */
    /* loaded from: classes3.dex */
    public interface b<T, U> {
        U apply(T t);
    }

    /* compiled from: SQLiteEventStore.java */
    /* loaded from: classes3.dex */
    public static class c {
        public final String a;

        /* renamed from: b  reason: collision with root package name */
        public final String f798b;

        public c(String str, String str2, a aVar) {
            this.a = str;
            this.f798b = str2;
        }
    }

    /* compiled from: SQLiteEventStore.java */
    /* loaded from: classes3.dex */
    public interface d<T> {
        T a();
    }

    public t(b.i.a.b.j.v.a aVar, b.i.a.b.j.v.a aVar2, b.i.a.b.j.t.i.d dVar, z zVar) {
        this.k = zVar;
        this.l = aVar;
        this.m = aVar2;
        this.n = dVar;
    }

    public static String f(Iterable<h> iterable) {
        StringBuilder sb = new StringBuilder("(");
        Iterator<h> it = iterable.iterator();
        while (it.hasNext()) {
            sb.append(it.next().b());
            if (it.hasNext()) {
                sb.append(',');
            }
        }
        sb.append(')');
        return sb.toString();
    }

    public static <T> T n(Cursor cursor, b<Cursor, T> bVar) {
        try {
            return bVar.apply(cursor);
        } finally {
            cursor.close();
        }
    }

    @Override // b.i.a.b.j.u.a
    public <T> T a(a.AbstractC0090a<T> aVar) {
        final SQLiteDatabase b2 = b();
        e(new d(b2) { // from class: b.i.a.b.j.t.i.m
            public final SQLiteDatabase a;

            {
                this.a = b2;
            }

            @Override // b.i.a.b.j.t.i.t.d
            public Object a() {
                SQLiteDatabase sQLiteDatabase = this.a;
                b bVar = t.j;
                sQLiteDatabase.beginTransaction();
                return null;
            }
        }, n.a);
        try {
            T execute = aVar.execute();
            b2.setTransactionSuccessful();
            return execute;
        } finally {
            b2.endTransaction();
        }
    }

    @Override // b.i.a.b.j.t.i.c
    @Nullable
    public h a0(final i iVar, final f fVar) {
        b.c.a.a0.d.Y("SQLiteEventStore", "Storing event with priority=%s, name=%s for destination %s", iVar.d(), fVar.g(), iVar.b());
        long longValue = ((Long) d(new b(this, iVar, fVar) { // from class: b.i.a.b.j.t.i.s
            public final t a;

            /* renamed from: b  reason: collision with root package name */
            public final i f797b;
            public final f c;

            {
                this.a = this;
                this.f797b = iVar;
                this.c = fVar;
            }

            @Override // b.i.a.b.j.t.i.t.b
            public Object apply(Object obj) {
                long j2;
                t tVar = this.a;
                i iVar2 = this.f797b;
                f fVar2 = this.c;
                SQLiteDatabase sQLiteDatabase = (SQLiteDatabase) obj;
                b bVar = t.j;
                if (tVar.b().compileStatement("PRAGMA page_size").simpleQueryForLong() * tVar.b().compileStatement("PRAGMA page_count").simpleQueryForLong() >= tVar.n.e()) {
                    return -1L;
                }
                Long c2 = tVar.c(sQLiteDatabase, iVar2);
                if (c2 != null) {
                    j2 = c2.longValue();
                } else {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("backend_name", iVar2.b());
                    contentValues.put("priority", Integer.valueOf(b.i.a.b.j.w.a.a(iVar2.d())));
                    contentValues.put("next_request_ms", (Integer) 0);
                    if (iVar2.c() != null) {
                        contentValues.put(NotificationCompat.MessagingStyle.Message.KEY_EXTRAS_BUNDLE, Base64.encodeToString(iVar2.c(), 0));
                    }
                    j2 = sQLiteDatabase.insert("transport_contexts", null, contentValues);
                }
                int d2 = tVar.n.d();
                byte[] bArr = fVar2.d().f762b;
                boolean z2 = bArr.length <= d2;
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("context_id", Long.valueOf(j2));
                contentValues2.put("transport_name", fVar2.g());
                contentValues2.put("timestamp_ms", Long.valueOf(fVar2.e()));
                contentValues2.put("uptime_ms", Long.valueOf(fVar2.h()));
                contentValues2.put("payload_encoding", fVar2.d().a.a);
                contentValues2.put(ModelAuditLogEntry.CHANGE_KEY_CODE, fVar2.c());
                contentValues2.put("num_attempts", (Integer) 0);
                contentValues2.put("inline", Boolean.valueOf(z2));
                contentValues2.put("payload", z2 ? bArr : new byte[0]);
                long insert = sQLiteDatabase.insert("events", null, contentValues2);
                if (!z2) {
                    int ceil = (int) Math.ceil(bArr.length / d2);
                    for (int i = 1; i <= ceil; i++) {
                        byte[] copyOfRange = Arrays.copyOfRange(bArr, (i - 1) * d2, Math.min(i * d2, bArr.length));
                        ContentValues contentValues3 = new ContentValues();
                        contentValues3.put("event_id", Long.valueOf(insert));
                        contentValues3.put("sequence_num", Integer.valueOf(i));
                        contentValues3.put("bytes", copyOfRange);
                        sQLiteDatabase.insert("event_payloads", null, contentValues3);
                    }
                }
                for (Map.Entry entry : Collections.unmodifiableMap(fVar2.b()).entrySet()) {
                    ContentValues contentValues4 = new ContentValues();
                    contentValues4.put("event_id", Long.valueOf(insert));
                    contentValues4.put(ModelAuditLogEntry.CHANGE_KEY_NAME, (String) entry.getKey());
                    contentValues4.put("value", (String) entry.getValue());
                    sQLiteDatabase.insert("event_metadata", null, contentValues4);
                }
                return Long.valueOf(insert);
            }
        })).longValue();
        if (longValue < 1) {
            return null;
        }
        return new b.i.a.b.j.t.i.b(longValue, iVar, fVar);
    }

    @VisibleForTesting
    public SQLiteDatabase b() {
        final z zVar = this.k;
        zVar.getClass();
        return (SQLiteDatabase) e(new d(zVar) { // from class: b.i.a.b.j.t.i.o
            public final z a;

            {
                this.a = zVar;
            }

            @Override // b.i.a.b.j.t.i.t.d
            public Object a() {
                return this.a.getWritableDatabase();
            }
        }, r.a);
    }

    @Nullable
    public final Long c(SQLiteDatabase sQLiteDatabase, i iVar) {
        StringBuilder sb = new StringBuilder("backend_name = ? and priority = ?");
        ArrayList arrayList = new ArrayList(Arrays.asList(iVar.b(), String.valueOf(b.i.a.b.j.w.a.a(iVar.d()))));
        if (iVar.c() != null) {
            sb.append(" and extras = ?");
            arrayList.add(Base64.encodeToString(iVar.c(), 0));
        }
        Cursor query = sQLiteDatabase.query("transport_contexts", new String[]{"_id"}, sb.toString(), (String[]) arrayList.toArray(new String[0]), null, null, null);
        try {
            return !query.moveToNext() ? null : Long.valueOf(query.getLong(0));
        } finally {
            query.close();
        }
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.k.close();
    }

    public final <T> T d(b<SQLiteDatabase, T> bVar) {
        SQLiteDatabase b2 = b();
        b2.beginTransaction();
        try {
            T apply = bVar.apply(b2);
            b2.setTransactionSuccessful();
            return apply;
        } finally {
            b2.endTransaction();
        }
    }

    public final <T> T e(d<T> dVar, b<Throwable, T> bVar) {
        long a2 = this.m.a();
        while (true) {
            try {
                return dVar.a();
            } catch (SQLiteDatabaseLockedException e) {
                if (this.m.a() >= this.n.a() + a2) {
                    return bVar.apply(e);
                }
                SystemClock.sleep(50L);
            }
        }
    }

    /* JADX WARN: Finally extract failed */
    @Override // b.i.a.b.j.t.i.c
    public long h0(i iVar) {
        Long l;
        Cursor rawQuery = b().rawQuery("SELECT next_request_ms FROM transport_contexts WHERE backend_name = ? and priority = ?", new String[]{iVar.b(), String.valueOf(b.i.a.b.j.w.a.a(iVar.d()))});
        try {
            if (rawQuery.moveToNext()) {
                l = Long.valueOf(rawQuery.getLong(0));
            } else {
                l = 0L;
            }
            rawQuery.close();
            return l.longValue();
        } catch (Throwable th) {
            rawQuery.close();
            throw th;
        }
    }

    /* JADX WARN: Finally extract failed */
    @Override // b.i.a.b.j.t.i.c
    public int l() {
        long a2 = this.l.a() - this.n.b();
        SQLiteDatabase b2 = b();
        b2.beginTransaction();
        try {
            Integer valueOf = Integer.valueOf(b2.delete("events", "timestamp_ms < ?", new String[]{String.valueOf(a2)}));
            b2.setTransactionSuccessful();
            b2.endTransaction();
            return valueOf.intValue();
        } catch (Throwable th) {
            b2.endTransaction();
            throw th;
        }
    }

    /* JADX WARN: Finally extract failed */
    @Override // b.i.a.b.j.t.i.c
    public boolean l0(i iVar) {
        Boolean bool;
        SQLiteDatabase b2 = b();
        b2.beginTransaction();
        try {
            Long c2 = c(b2, iVar);
            if (c2 == null) {
                bool = Boolean.FALSE;
            } else {
                bool = (Boolean) n(b().rawQuery("SELECT 1 FROM events WHERE context_id = ? LIMIT 1", new String[]{c2.toString()}), q.a);
            }
            b2.setTransactionSuccessful();
            b2.endTransaction();
            return bool.booleanValue();
        } catch (Throwable th) {
            b2.endTransaction();
            throw th;
        }
    }

    @Override // b.i.a.b.j.t.i.c
    public void m(Iterable<h> iterable) {
        if (iterable.iterator().hasNext()) {
            StringBuilder R = b.d.b.a.a.R("DELETE FROM events WHERE _id in ");
            R.append(f(iterable));
            b().compileStatement(R.toString()).execute();
        }
    }

    @Override // b.i.a.b.j.t.i.c
    public void n0(Iterable<h> iterable) {
        if (iterable.iterator().hasNext()) {
            StringBuilder R = b.d.b.a.a.R("UPDATE events SET num_attempts = num_attempts + 1 WHERE _id in ");
            R.append(f(iterable));
            String sb = R.toString();
            SQLiteDatabase b2 = b();
            b2.beginTransaction();
            try {
                b2.compileStatement(sb).execute();
                b2.compileStatement("DELETE FROM events WHERE num_attempts >= 16").execute();
                b2.setTransactionSuccessful();
            } finally {
                b2.endTransaction();
            }
        }
    }

    @Override // b.i.a.b.j.t.i.c
    public Iterable<h> r(final i iVar) {
        return (Iterable) d(new b(this, iVar) { // from class: b.i.a.b.j.t.i.j
            public final t a;

            /* renamed from: b  reason: collision with root package name */
            public final i f795b;

            {
                this.a = this;
                this.f795b = iVar;
            }

            /* JADX WARN: Finally extract failed */
            @Override // b.i.a.b.j.t.i.t.b
            public Object apply(Object obj) {
                final t tVar = this.a;
                final i iVar2 = this.f795b;
                SQLiteDatabase sQLiteDatabase = (SQLiteDatabase) obj;
                b bVar = t.j;
                Objects.requireNonNull(tVar);
                final ArrayList arrayList = new ArrayList();
                Long c2 = tVar.c(sQLiteDatabase, iVar2);
                if (c2 != null) {
                    t.n(sQLiteDatabase.query("events", new String[]{"_id", "transport_name", "timestamp_ms", "uptime_ms", "payload_encoding", "payload", ModelAuditLogEntry.CHANGE_KEY_CODE, "inline"}, "context_id = ?", new String[]{c2.toString()}, null, null, null, String.valueOf(tVar.n.c())), new t.b(tVar, arrayList, iVar2) { // from class: b.i.a.b.j.t.i.k
                        public final t a;

                        /* renamed from: b  reason: collision with root package name */
                        public final List f796b;
                        public final i c;

                        {
                            this.a = tVar;
                            this.f796b = arrayList;
                            this.c = iVar2;
                        }

                        @Override // b.i.a.b.j.t.i.t.b
                        public Object apply(Object obj2) {
                            b bVar2;
                            b bVar3;
                            t tVar2 = this.a;
                            List list = this.f796b;
                            i iVar3 = this.c;
                            Cursor cursor = (Cursor) obj2;
                            b bVar4 = t.j;
                            while (cursor.moveToNext()) {
                                long j2 = cursor.getLong(0);
                                boolean z2 = cursor.getInt(7) != 0;
                                a.b bVar5 = new a.b();
                                bVar5.f = new HashMap();
                                bVar5.f(cursor.getString(1));
                                bVar5.e(cursor.getLong(2));
                                bVar5.g(cursor.getLong(3));
                                if (z2) {
                                    String string = cursor.getString(4);
                                    if (string == null) {
                                        bVar3 = t.j;
                                    } else {
                                        bVar3 = new b(string);
                                    }
                                    bVar5.d(new e(bVar3, cursor.getBlob(5)));
                                } else {
                                    String string2 = cursor.getString(4);
                                    if (string2 == null) {
                                        bVar2 = t.j;
                                    } else {
                                        bVar2 = new b(string2);
                                    }
                                    bVar5.d(new e(bVar2, (byte[]) t.n(tVar2.b().query("event_payloads", new String[]{"bytes"}, "event_id = ?", new String[]{String.valueOf(j2)}, null, null, "sequence_num"), l.a)));
                                }
                                if (!cursor.isNull(6)) {
                                    bVar5.f758b = Integer.valueOf(cursor.getInt(6));
                                }
                                list.add(new b(j2, iVar3, bVar5.b()));
                            }
                            return null;
                        }
                    });
                }
                HashMap hashMap = new HashMap();
                StringBuilder sb = new StringBuilder("event_id IN (");
                for (int i = 0; i < arrayList.size(); i++) {
                    sb.append(((h) arrayList.get(i)).b());
                    if (i < arrayList.size() - 1) {
                        sb.append(',');
                    }
                }
                sb.append(')');
                Cursor query = sQLiteDatabase.query("event_metadata", new String[]{"event_id", ModelAuditLogEntry.CHANGE_KEY_NAME, "value"}, sb.toString(), null, null, null, null);
                while (query.moveToNext()) {
                    try {
                        long j2 = query.getLong(0);
                        Set set = (Set) hashMap.get(Long.valueOf(j2));
                        if (set == null) {
                            set = new HashSet();
                            hashMap.put(Long.valueOf(j2), set);
                        }
                        set.add(new t.c(query.getString(1), query.getString(2), null));
                    } catch (Throwable th) {
                        query.close();
                        throw th;
                    }
                }
                query.close();
                ListIterator listIterator = arrayList.listIterator();
                while (listIterator.hasNext()) {
                    h hVar = (h) listIterator.next();
                    if (hashMap.containsKey(Long.valueOf(hVar.b()))) {
                        f.a i2 = hVar.a().i();
                        for (t.c cVar : (Set) hashMap.get(Long.valueOf(hVar.b()))) {
                            i2.a(cVar.a, cVar.f798b);
                        }
                        listIterator.set(new b(hVar.b(), hVar.c(), i2.b()));
                    }
                }
                return arrayList;
            }
        });
    }

    @Override // b.i.a.b.j.t.i.c
    public void v(final i iVar, final long j2) {
        d(new b(j2, iVar) { // from class: b.i.a.b.j.t.i.i
            public final long a;

            /* renamed from: b  reason: collision with root package name */
            public final b.i.a.b.j.i f794b;

            {
                this.a = j2;
                this.f794b = iVar;
            }

            @Override // b.i.a.b.j.t.i.t.b
            public Object apply(Object obj) {
                long j3 = this.a;
                b.i.a.b.j.i iVar2 = this.f794b;
                SQLiteDatabase sQLiteDatabase = (SQLiteDatabase) obj;
                b bVar = t.j;
                ContentValues contentValues = new ContentValues();
                contentValues.put("next_request_ms", Long.valueOf(j3));
                if (sQLiteDatabase.update("transport_contexts", contentValues, "backend_name = ? and priority = ?", new String[]{iVar2.b(), String.valueOf(b.i.a.b.j.w.a.a(iVar2.d()))}) < 1) {
                    contentValues.put("backend_name", iVar2.b());
                    contentValues.put("priority", Integer.valueOf(b.i.a.b.j.w.a.a(iVar2.d())));
                    sQLiteDatabase.insert("transport_contexts", null, contentValues);
                }
                return null;
            }
        });
    }

    @Override // b.i.a.b.j.t.i.c
    public Iterable<i> z() {
        SQLiteDatabase b2 = b();
        b2.beginTransaction();
        try {
            List list = (List) n(b2.rawQuery("SELECT distinct t._id, t.backend_name, t.priority, t.extras FROM transport_contexts AS t, events AS e WHERE e.context_id = t._id", new String[0]), p.a);
            b2.setTransactionSuccessful();
            return list;
        } finally {
            b2.endTransaction();
        }
    }
}
