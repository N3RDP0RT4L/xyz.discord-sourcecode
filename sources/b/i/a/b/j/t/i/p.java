package b.i.a.b.j.t.i;

import android.database.Cursor;
import android.util.Base64;
import b.i.a.b.b;
import b.i.a.b.j.b;
import b.i.a.b.j.i;
import b.i.a.b.j.t.i.t;
import b.i.a.b.j.w.a;
import java.util.ArrayList;
/* compiled from: SQLiteEventStore.java */
/* loaded from: classes3.dex */
public final /* synthetic */ class p implements t.b {
    public static final p a = new p();

    @Override // b.i.a.b.j.t.i.t.b
    public Object apply(Object obj) {
        Cursor cursor = (Cursor) obj;
        b bVar = t.j;
        ArrayList arrayList = new ArrayList();
        while (cursor.moveToNext()) {
            i.a a2 = i.a();
            a2.b(cursor.getString(1));
            a2.c(a.b(cursor.getInt(2)));
            String string = cursor.getString(3);
            b.C0086b bVar2 = (b.C0086b) a2;
            bVar2.f760b = string == null ? null : Base64.decode(string, 0);
            arrayList.add(bVar2.a());
        }
        return arrayList;
    }
}
