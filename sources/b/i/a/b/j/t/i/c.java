package b.i.a.b.j.t.i;

import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import b.i.a.b.j.f;
import b.i.a.b.j.i;
import java.io.Closeable;
/* compiled from: EventStore.java */
@WorkerThread
/* loaded from: classes3.dex */
public interface c extends Closeable {
    @Nullable
    h a0(i iVar, f fVar);

    long h0(i iVar);

    int l();

    boolean l0(i iVar);

    void m(Iterable<h> iterable);

    void n0(Iterable<h> iterable);

    Iterable<h> r(i iVar);

    void v(i iVar, long j);

    Iterable<i> z();
}
