package b.i.a.b.j.t.i;

import andhook.lib.xposed.callbacks.XCallback;
import b.d.b.a.a;
import com.google.auto.value.AutoValue;
/* compiled from: EventStoreConfig.java */
@AutoValue
/* loaded from: classes3.dex */
public abstract class d {
    public static final d a;

    static {
        Long l = 10485760L;
        Integer num = 200;
        Integer valueOf = Integer.valueOf((int) XCallback.PRIORITY_HIGHEST);
        Long l2 = 604800000L;
        Integer num2 = 81920;
        String str = l == null ? " maxStorageSizeInBytes" : "";
        if (num == null) {
            str = a.v(str, " loadBatchSize");
        }
        if (valueOf == null) {
            str = a.v(str, " criticalSectionEnterTimeoutMs");
        }
        if (l2 == null) {
            str = a.v(str, " eventCleanUpAge");
        }
        if (num2 == null) {
            str = a.v(str, " maxBlobByteSizePerRow");
        }
        if (str.isEmpty()) {
            a = new a(l.longValue(), num.intValue(), valueOf.intValue(), l2.longValue(), num2.intValue(), null);
            return;
        }
        throw new IllegalStateException(a.v("Missing required properties:", str));
    }

    public abstract int a();

    public abstract long b();

    public abstract int c();

    public abstract int d();

    public abstract long e();
}
