package b.i.a.b.j.t.i;

import android.content.Context;
import c0.a.a;
/* compiled from: SchemaManager_Factory.java */
/* loaded from: classes3.dex */
public final class a0 implements a {
    public final a<Context> a;

    /* renamed from: b  reason: collision with root package name */
    public final a<String> f792b;
    public final a<Integer> c;

    public a0(a<Context> aVar, a<String> aVar2, a<Integer> aVar3) {
        this.a = aVar;
        this.f792b = aVar2;
        this.c = aVar3;
    }

    @Override // c0.a.a
    public Object get() {
        return new z(this.a.get(), this.f792b.get(), this.c.get().intValue());
    }
}
