package b.i.a.b.j.t;

import b.i.a.b.h;
import b.i.a.b.j.f;
import b.i.a.b.j.i;
import b.i.a.b.j.n;
import b.i.a.b.j.q.e;
import b.i.a.b.j.q.m;
import b.i.a.b.j.t.h.r;
import b.i.a.b.j.u.a;
import java.util.concurrent.Executor;
import java.util.logging.Logger;
/* compiled from: DefaultScheduler.java */
/* loaded from: classes3.dex */
public class c implements e {
    public static final Logger a = Logger.getLogger(n.class.getName());

    /* renamed from: b  reason: collision with root package name */
    public final r f777b;
    public final Executor c;
    public final e d;
    public final b.i.a.b.j.t.i.c e;
    public final a f;

    public c(Executor executor, e eVar, r rVar, b.i.a.b.j.t.i.c cVar, a aVar) {
        this.c = executor;
        this.d = eVar;
        this.f777b = rVar;
        this.e = cVar;
        this.f = aVar;
    }

    @Override // b.i.a.b.j.t.e
    public void a(final i iVar, final f fVar, final h hVar) {
        this.c.execute(new Runnable(this, iVar, hVar, fVar) { // from class: b.i.a.b.j.t.a
            public final c j;
            public final i k;
            public final h l;
            public final f m;

            {
                this.j = this;
                this.k = iVar;
                this.l = hVar;
                this.m = fVar;
            }

            @Override // java.lang.Runnable
            public void run() {
                final c cVar = this.j;
                final i iVar2 = this.k;
                h hVar2 = this.l;
                f fVar2 = this.m;
                Logger logger = c.a;
                try {
                    m mVar = cVar.d.get(iVar2.b());
                    if (mVar == null) {
                        String format = String.format("Transport backend '%s' is not registered", iVar2.b());
                        c.a.warning(format);
                        hVar2.a(new IllegalArgumentException(format));
                    } else {
                        final f b2 = mVar.b(fVar2);
                        cVar.f.a(new a.AbstractC0090a(cVar, iVar2, b2) { // from class: b.i.a.b.j.t.b
                            public final c a;

                            /* renamed from: b  reason: collision with root package name */
                            public final i f776b;
                            public final f c;

                            {
                                this.a = cVar;
                                this.f776b = iVar2;
                                this.c = b2;
                            }

                            @Override // b.i.a.b.j.u.a.AbstractC0090a
                            public Object execute() {
                                c cVar2 = this.a;
                                i iVar3 = this.f776b;
                                cVar2.e.a0(iVar3, this.c);
                                cVar2.f777b.a(iVar3, 1);
                                return null;
                            }
                        });
                        hVar2.a(null);
                    }
                } catch (Exception e) {
                    Logger logger2 = c.a;
                    StringBuilder R = b.d.b.a.a.R("Error scheduling event ");
                    R.append(e.getMessage());
                    logger2.warning(R.toString());
                    hVar2.a(e);
                }
            }
        });
    }
}
