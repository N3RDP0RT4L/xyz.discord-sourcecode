package b.i.a.b.j.t;

import b.i.a.b.d;
import b.i.a.b.j.t.h.b;
import b.i.a.b.j.t.h.c;
import b.i.a.b.j.t.h.f;
import c0.a.a;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
/* compiled from: SchedulingConfigModule_ConfigFactory.java */
/* loaded from: classes3.dex */
public final class f implements a {
    public final a<b.i.a.b.j.v.a> a;

    public f(a<b.i.a.b.j.v.a> aVar) {
        this.a = aVar;
    }

    @Override // c0.a.a
    public Object get() {
        b.i.a.b.j.v.a aVar = this.a.get();
        HashMap hashMap = new HashMap();
        d dVar = d.DEFAULT;
        f.a.AbstractC0088a a = f.a.a();
        a.b(30000L);
        a.c(86400000L);
        hashMap.put(dVar, a.a());
        d dVar2 = d.HIGHEST;
        f.a.AbstractC0088a a2 = f.a.a();
        a2.b(1000L);
        a2.c(86400000L);
        hashMap.put(dVar2, a2.a());
        d dVar3 = d.VERY_LOW;
        f.a.AbstractC0088a a3 = f.a.a();
        a3.b(86400000L);
        a3.c(86400000L);
        Set<f.b> unmodifiableSet = Collections.unmodifiableSet(new HashSet(Arrays.asList(f.b.NETWORK_UNMETERED, f.b.DEVICE_IDLE)));
        c.b bVar = (c.b) a3;
        Objects.requireNonNull(unmodifiableSet, "Null flags");
        bVar.c = unmodifiableSet;
        hashMap.put(dVar3, bVar.a());
        Objects.requireNonNull(aVar, "missing required property: clock");
        int size = hashMap.keySet().size();
        d.values();
        if (size >= 3) {
            new HashMap();
            return new b(aVar, hashMap);
        }
        throw new IllegalStateException("Not all priorities have been configured");
    }
}
