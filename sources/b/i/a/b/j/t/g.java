package b.i.a.b.j.t;

import android.content.Context;
import b.i.a.b.j.t.h.d;
import b.i.a.b.j.t.h.f;
import b.i.a.b.j.t.i.c;
import c0.a.a;
/* compiled from: SchedulingModule_WorkSchedulerFactory.java */
/* loaded from: classes3.dex */
public final class g implements a {
    public final a<Context> a;

    /* renamed from: b  reason: collision with root package name */
    public final a<c> f779b;
    public final a<f> c;
    public final a<b.i.a.b.j.v.a> d;

    public g(a<Context> aVar, a<c> aVar2, a<f> aVar3, a<b.i.a.b.j.v.a> aVar4) {
        this.a = aVar;
        this.f779b = aVar2;
        this.c = aVar3;
        this.d = aVar4;
    }

    @Override // c0.a.a
    public Object get() {
        this.d.get();
        return new d(this.a.get(), this.f779b.get(), this.c.get());
    }
}
