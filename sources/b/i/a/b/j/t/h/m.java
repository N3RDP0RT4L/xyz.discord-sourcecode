package b.i.a.b.j.t.h;

import android.content.Context;
import b.i.a.b.j.q.e;
import b.i.a.b.j.t.i.c;
import c0.a.a;
import java.util.concurrent.Executor;
/* compiled from: Uploader_Factory.java */
/* loaded from: classes3.dex */
public final class m implements a {
    public final a<Context> a;

    /* renamed from: b  reason: collision with root package name */
    public final a<e> f788b;
    public final a<c> c;
    public final a<r> d;
    public final a<Executor> e;
    public final a<b.i.a.b.j.u.a> f;
    public final a<b.i.a.b.j.v.a> g;

    public m(a<Context> aVar, a<e> aVar2, a<c> aVar3, a<r> aVar4, a<Executor> aVar5, a<b.i.a.b.j.u.a> aVar6, a<b.i.a.b.j.v.a> aVar7) {
        this.a = aVar;
        this.f788b = aVar2;
        this.c = aVar3;
        this.d = aVar4;
        this.e = aVar5;
        this.f = aVar6;
        this.g = aVar7;
    }

    @Override // c0.a.a
    public Object get() {
        return new l(this.a.get(), this.f788b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get());
    }
}
