package b.i.a.b.j.t.h;

import b.i.a.b.d;
import b.i.a.b.j.t.h.c;
import com.google.auto.value.AutoValue;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
/* compiled from: SchedulerConfig.java */
@AutoValue
/* loaded from: classes3.dex */
public abstract class f {

    /* compiled from: SchedulerConfig.java */
    @AutoValue
    /* loaded from: classes3.dex */
    public static abstract class a {

        /* compiled from: SchedulerConfig.java */
        @AutoValue.Builder
        /* renamed from: b.i.a.b.j.t.h.f$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static abstract class AbstractC0088a {
            public abstract a a();

            public abstract AbstractC0088a b(long j);

            public abstract AbstractC0088a c(long j);
        }

        public static AbstractC0088a a() {
            c.b bVar = new c.b();
            Set<b> emptySet = Collections.emptySet();
            Objects.requireNonNull(emptySet, "Null flags");
            bVar.c = emptySet;
            return bVar;
        }

        public abstract long b();

        public abstract Set<b> c();

        public abstract long d();
    }

    /* compiled from: SchedulerConfig.java */
    /* loaded from: classes3.dex */
    public enum b {
        NETWORK_UNMETERED,
        DEVICE_IDLE,
        DEVICE_CHARGING
    }

    public abstract b.i.a.b.j.v.a a();

    public long b(d dVar, long j, int i) {
        int i2;
        long a2 = j - a().a();
        a aVar = c().get(dVar);
        long b2 = aVar.b();
        return Math.min(Math.max((long) (Math.pow(3.0d, i - 1) * b2 * Math.max(1.0d, Math.log(10000.0d) / Math.log((b2 > 1 ? b2 : 2L) * i2))), a2), aVar.d());
    }

    public abstract Map<d, a> c();
}
