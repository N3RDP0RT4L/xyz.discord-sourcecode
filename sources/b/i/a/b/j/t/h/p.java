package b.i.a.b.j.t.h;

import b.i.a.b.j.t.i.c;
import b.i.a.b.j.u.a;
import java.util.concurrent.Executor;
/* compiled from: WorkInitializer.java */
/* loaded from: classes3.dex */
public class p {
    public final Executor a;

    /* renamed from: b  reason: collision with root package name */
    public final c f789b;
    public final r c;
    public final a d;

    public p(Executor executor, c cVar, r rVar, a aVar) {
        this.a = executor;
        this.f789b = cVar;
        this.c = rVar;
        this.d = aVar;
    }
}
