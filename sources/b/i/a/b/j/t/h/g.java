package b.i.a.b.j.t.h;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import b.i.a.b.j.i;
import b.i.a.b.j.t.i.c;
import b.i.a.b.j.u.a;
import com.google.android.datatransport.runtime.synchronization.SynchronizationException;
/* compiled from: Uploader.java */
/* loaded from: classes3.dex */
public final /* synthetic */ class g implements Runnable {
    public final l j;
    public final i k;
    public final int l;
    public final Runnable m;

    public g(l lVar, i iVar, int i, Runnable runnable) {
        this.j = lVar;
        this.k = iVar;
        this.l = i;
        this.m = runnable;
    }

    @Override // java.lang.Runnable
    public void run() {
        final l lVar = this.j;
        final i iVar = this.k;
        final int i = this.l;
        Runnable runnable = this.m;
        try {
            try {
                a aVar = lVar.f;
                final c cVar = lVar.c;
                cVar.getClass();
                aVar.a(new a.AbstractC0090a(cVar) { // from class: b.i.a.b.j.t.h.j
                    public final c a;

                    {
                        this.a = cVar;
                    }

                    @Override // b.i.a.b.j.u.a.AbstractC0090a
                    public Object execute() {
                        return Integer.valueOf(this.a.l());
                    }
                });
                NetworkInfo activeNetworkInfo = ((ConnectivityManager) lVar.a.getSystemService("connectivity")).getActiveNetworkInfo();
                if (!(activeNetworkInfo != null && activeNetworkInfo.isConnected())) {
                    lVar.f.a(new a.AbstractC0090a(lVar, iVar, i) { // from class: b.i.a.b.j.t.h.k
                        public final l a;

                        /* renamed from: b  reason: collision with root package name */
                        public final i f786b;
                        public final int c;

                        {
                            this.a = lVar;
                            this.f786b = iVar;
                            this.c = i;
                        }

                        @Override // b.i.a.b.j.u.a.AbstractC0090a
                        public Object execute() {
                            l lVar2 = this.a;
                            lVar2.d.a(this.f786b, this.c + 1);
                            return null;
                        }
                    });
                } else {
                    lVar.a(iVar, i);
                }
            } catch (SynchronizationException unused) {
                lVar.d.a(iVar, i + 1);
            }
        } finally {
            runnable.run();
        }
    }
}
