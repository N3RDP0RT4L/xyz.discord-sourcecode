package b.i.a.b.j.t.h;

import android.content.Context;
import b.c.a.a0.d;
import b.i.a.b.j.i;
import b.i.a.b.j.q.e;
import b.i.a.b.j.q.g;
import b.i.a.b.j.q.m;
import b.i.a.b.j.t.i.c;
import b.i.a.b.j.t.i.h;
import b.i.a.b.j.u.a;
import java.util.ArrayList;
import java.util.concurrent.Executor;
/* compiled from: Uploader.java */
/* loaded from: classes3.dex */
public class l {
    public final Context a;

    /* renamed from: b  reason: collision with root package name */
    public final e f787b;
    public final c c;
    public final r d;
    public final Executor e;
    public final a f;
    public final b.i.a.b.j.v.a g;

    public l(Context context, e eVar, c cVar, r rVar, Executor executor, a aVar, b.i.a.b.j.v.a aVar2) {
        this.a = context;
        this.f787b = eVar;
        this.c = cVar;
        this.d = rVar;
        this.e = executor;
        this.f = aVar;
        this.g = aVar2;
    }

    public void a(final i iVar, final int i) {
        g a;
        m mVar = this.f787b.get(iVar.b());
        final Iterable<h> iterable = (Iterable) this.f.a(new a.AbstractC0090a(this, iVar) { // from class: b.i.a.b.j.t.h.h
            public final l a;

            /* renamed from: b  reason: collision with root package name */
            public final i f784b;

            {
                this.a = this;
                this.f784b = iVar;
            }

            @Override // b.i.a.b.j.u.a.AbstractC0090a
            public Object execute() {
                l lVar = this.a;
                return lVar.c.r(this.f784b);
            }
        });
        if (iterable.iterator().hasNext()) {
            if (mVar == null) {
                d.X("Uploader", "Unknown backend for %s, deleting event batch for it...", iVar);
                a = g.a();
            } else {
                ArrayList arrayList = new ArrayList();
                for (h hVar : iterable) {
                    arrayList.add(hVar.a());
                }
                byte[] c = iVar.c();
                if (1 != 0) {
                    a = mVar.a(new b.i.a.b.j.q.a(arrayList, c, null));
                } else {
                    throw new IllegalStateException(b.d.b.a.a.v("Missing required properties:", ""));
                }
            }
            final g gVar = a;
            this.f.a(new a.AbstractC0090a(this, gVar, iterable, iVar, i) { // from class: b.i.a.b.j.t.h.i
                public final l a;

                /* renamed from: b  reason: collision with root package name */
                public final g f785b;
                public final Iterable c;
                public final b.i.a.b.j.i d;
                public final int e;

                {
                    this.a = this;
                    this.f785b = gVar;
                    this.c = iterable;
                    this.d = iVar;
                    this.e = i;
                }

                @Override // b.i.a.b.j.u.a.AbstractC0090a
                public Object execute() {
                    l lVar = this.a;
                    g gVar2 = this.f785b;
                    Iterable<h> iterable2 = this.c;
                    b.i.a.b.j.i iVar2 = this.d;
                    int i2 = this.e;
                    if (gVar2.c() == g.a.TRANSIENT_ERROR) {
                        lVar.c.n0(iterable2);
                        lVar.d.a(iVar2, i2 + 1);
                        return null;
                    }
                    lVar.c.m(iterable2);
                    if (gVar2.c() == g.a.OK) {
                        lVar.c.v(iVar2, gVar2.b() + lVar.g.a());
                    }
                    if (!lVar.c.l0(iVar2)) {
                        return null;
                    }
                    lVar.d.a(iVar2, 1);
                    return null;
                }
            });
        }
    }
}
