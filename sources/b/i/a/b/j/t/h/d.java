package b.i.a.b.j.t.h;

import android.content.Context;
import androidx.annotation.RequiresApi;
import b.i.a.b.j.t.i.c;
/* compiled from: JobInfoScheduler.java */
@RequiresApi(api = 21)
/* loaded from: classes3.dex */
public class d implements r {
    public final Context a;

    /* renamed from: b  reason: collision with root package name */
    public final c f783b;
    public final f c;

    public d(Context context, c cVar, f fVar) {
        this.a = context;
        this.f783b = cVar;
        this.c = fVar;
    }

    /* JADX WARN: Code restructure failed: missing block: B:12:0x0092, code lost:
        r7 = false;
     */
    @Override // b.i.a.b.j.t.h.r
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void a(b.i.a.b.j.i r18, int r19) {
        /*
            Method dump skipped, instructions count: 344
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.b.j.t.h.d.a(b.i.a.b.j.i, int):void");
    }
}
