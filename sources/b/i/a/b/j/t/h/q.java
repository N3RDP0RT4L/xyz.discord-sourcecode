package b.i.a.b.j.t.h;

import b.i.a.b.j.t.i.c;
import c0.a.a;
import java.util.concurrent.Executor;
/* compiled from: WorkInitializer_Factory.java */
/* loaded from: classes3.dex */
public final class q implements a {
    public final a<Executor> a;

    /* renamed from: b  reason: collision with root package name */
    public final a<c> f790b;
    public final a<r> c;
    public final a<b.i.a.b.j.u.a> d;

    public q(a<Executor> aVar, a<c> aVar2, a<r> aVar3, a<b.i.a.b.j.u.a> aVar4) {
        this.a = aVar;
        this.f790b = aVar2;
        this.c = aVar3;
        this.d = aVar4;
    }

    @Override // c0.a.a
    public Object get() {
        return new p(this.a.get(), this.f790b.get(), this.c.get(), this.d.get());
    }
}
