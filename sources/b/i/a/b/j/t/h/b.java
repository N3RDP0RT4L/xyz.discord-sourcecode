package b.i.a.b.j.t.h;

import b.i.a.b.d;
import b.i.a.b.j.t.h.f;
import b.i.a.b.j.v.a;
import java.util.Map;
import java.util.Objects;
/* compiled from: AutoValue_SchedulerConfig.java */
/* loaded from: classes3.dex */
public final class b extends f {
    public final a a;

    /* renamed from: b  reason: collision with root package name */
    public final Map<d, f.a> f780b;

    public b(a aVar, Map<d, f.a> map) {
        Objects.requireNonNull(aVar, "Null clock");
        this.a = aVar;
        Objects.requireNonNull(map, "Null values");
        this.f780b = map;
    }

    @Override // b.i.a.b.j.t.h.f
    public a a() {
        return this.a;
    }

    @Override // b.i.a.b.j.t.h.f
    public Map<d, f.a> c() {
        return this.f780b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        return this.a.equals(fVar.a()) && this.f780b.equals(fVar.c());
    }

    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ this.f780b.hashCode();
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("SchedulerConfig{clock=");
        R.append(this.a);
        R.append(", values=");
        return b.d.b.a.a.L(R, this.f780b, "}");
    }
}
