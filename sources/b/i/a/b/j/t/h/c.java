package b.i.a.b.j.t.h;

import b.i.a.b.j.t.h.f;
import java.util.Set;
/* compiled from: AutoValue_SchedulerConfig_ConfigValue.java */
/* loaded from: classes3.dex */
public final class c extends f.a {
    public final long a;

    /* renamed from: b  reason: collision with root package name */
    public final long f781b;
    public final Set<f.b> c;

    /* compiled from: AutoValue_SchedulerConfig_ConfigValue.java */
    /* loaded from: classes3.dex */
    public static final class b extends f.a.AbstractC0088a {
        public Long a;

        /* renamed from: b  reason: collision with root package name */
        public Long f782b;
        public Set<f.b> c;

        @Override // b.i.a.b.j.t.h.f.a.AbstractC0088a
        public f.a a() {
            String str = this.a == null ? " delta" : "";
            if (this.f782b == null) {
                str = b.d.b.a.a.v(str, " maxAllowedDelay");
            }
            if (this.c == null) {
                str = b.d.b.a.a.v(str, " flags");
            }
            if (str.isEmpty()) {
                return new c(this.a.longValue(), this.f782b.longValue(), this.c, null);
            }
            throw new IllegalStateException(b.d.b.a.a.v("Missing required properties:", str));
        }

        @Override // b.i.a.b.j.t.h.f.a.AbstractC0088a
        public f.a.AbstractC0088a b(long j) {
            this.a = Long.valueOf(j);
            return this;
        }

        @Override // b.i.a.b.j.t.h.f.a.AbstractC0088a
        public f.a.AbstractC0088a c(long j) {
            this.f782b = Long.valueOf(j);
            return this;
        }
    }

    public c(long j, long j2, Set set, a aVar) {
        this.a = j;
        this.f781b = j2;
        this.c = set;
    }

    @Override // b.i.a.b.j.t.h.f.a
    public long b() {
        return this.a;
    }

    @Override // b.i.a.b.j.t.h.f.a
    public Set<f.b> c() {
        return this.c;
    }

    @Override // b.i.a.b.j.t.h.f.a
    public long d() {
        return this.f781b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof f.a)) {
            return false;
        }
        f.a aVar = (f.a) obj;
        return this.a == aVar.b() && this.f781b == aVar.d() && this.c.equals(aVar.c());
    }

    public int hashCode() {
        long j = this.a;
        long j2 = this.f781b;
        return this.c.hashCode() ^ ((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ ((int) ((j2 >>> 32) ^ j2))) * 1000003);
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("ConfigValue{delta=");
        R.append(this.a);
        R.append(", maxAllowedDelay=");
        R.append(this.f781b);
        R.append(", flags=");
        R.append(this.c);
        R.append("}");
        return R.toString();
    }
}
