package b.i.a.b.j.q;

import b.d.b.a.a;
import b.i.a.b.j.q.g;
import java.util.Objects;
/* compiled from: AutoValue_BackendResponse.java */
/* loaded from: classes3.dex */
public final class b extends g {
    public final g.a a;

    /* renamed from: b  reason: collision with root package name */
    public final long f768b;

    public b(g.a aVar, long j) {
        Objects.requireNonNull(aVar, "Null status");
        this.a = aVar;
        this.f768b = j;
    }

    @Override // b.i.a.b.j.q.g
    public long b() {
        return this.f768b;
    }

    @Override // b.i.a.b.j.q.g
    public g.a c() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof g)) {
            return false;
        }
        g gVar = (g) obj;
        return this.a.equals(gVar.c()) && this.f768b == gVar.b();
    }

    public int hashCode() {
        long j = this.f768b;
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ ((int) (j ^ (j >>> 32)));
    }

    public String toString() {
        StringBuilder R = a.R("BackendResponse{status=");
        R.append(this.a);
        R.append(", nextRequestWaitMillis=");
        return a.B(R, this.f768b, "}");
    }
}
