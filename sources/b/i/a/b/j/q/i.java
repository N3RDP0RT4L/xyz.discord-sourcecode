package b.i.a.b.j.q;

import android.content.Context;
import b.i.a.b.j.v.a;
/* compiled from: CreationContextFactory.java */
/* loaded from: classes3.dex */
public class i {
    public final Context a;

    /* renamed from: b  reason: collision with root package name */
    public final a f770b;
    public final a c;

    public i(Context context, a aVar, a aVar2) {
        this.a = context;
        this.f770b = aVar;
        this.c = aVar2;
    }
}
