package b.i.a.b.j.q;

import com.google.auto.value.AutoValue;
/* compiled from: BackendResponse.java */
@AutoValue
/* loaded from: classes3.dex */
public abstract class g {

    /* compiled from: BackendResponse.java */
    /* loaded from: classes3.dex */
    public enum a {
        OK,
        TRANSIENT_ERROR,
        FATAL_ERROR
    }

    public static g a() {
        return new b(a.FATAL_ERROR, -1L);
    }

    public abstract long b();

    public abstract a c();
}
