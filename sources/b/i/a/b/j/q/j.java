package b.i.a.b.j.q;

import android.content.Context;
import c0.a.a;
/* compiled from: CreationContextFactory_Factory.java */
/* loaded from: classes3.dex */
public final class j implements a {
    public final a<Context> a;

    /* renamed from: b  reason: collision with root package name */
    public final a<b.i.a.b.j.v.a> f771b;
    public final a<b.i.a.b.j.v.a> c;

    public j(a<Context> aVar, a<b.i.a.b.j.v.a> aVar2, a<b.i.a.b.j.v.a> aVar3) {
        this.a = aVar;
        this.f771b = aVar2;
        this.c = aVar3;
    }

    @Override // c0.a.a
    public Object get() {
        return new i(this.a.get(), this.f771b.get(), this.c.get());
    }
}
