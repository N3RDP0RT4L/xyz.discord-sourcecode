package b.i.a.b.j.q;

import android.content.Context;
import androidx.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;
/* compiled from: MetadataBackendRegistry.java */
/* loaded from: classes3.dex */
public class k implements e {
    public final a a;

    /* renamed from: b  reason: collision with root package name */
    public final i f772b;
    public final Map<String, m> c = new HashMap();

    /* compiled from: MetadataBackendRegistry.java */
    /* loaded from: classes3.dex */
    public static class a {
        public final Context a;

        /* renamed from: b  reason: collision with root package name */
        public Map<String, String> f773b = null;

        public a(Context context) {
            this.a = context;
        }

        /* JADX WARN: Removed duplicated region for block: B:15:0x003a  */
        /* JADX WARN: Removed duplicated region for block: B:16:0x0044  */
        @androidx.annotation.Nullable
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public b.i.a.b.j.q.d a(java.lang.String r15) {
            /*
                Method dump skipped, instructions count: 254
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: b.i.a.b.j.q.k.a.a(java.lang.String):b.i.a.b.j.q.d");
        }
    }

    public k(Context context, i iVar) {
        a aVar = new a(context);
        this.a = aVar;
        this.f772b = iVar;
    }

    @Override // b.i.a.b.j.q.e
    @Nullable
    public synchronized m get(String str) {
        if (this.c.containsKey(str)) {
            return this.c.get(str);
        }
        d a2 = this.a.a(str);
        if (a2 == null) {
            return null;
        }
        i iVar = this.f772b;
        m create = a2.create(new c(iVar.a, iVar.f770b, iVar.c, str));
        this.c.put(str, create);
        return create;
    }
}
