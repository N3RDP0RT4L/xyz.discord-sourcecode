package b.i.a.b.j.q;

import android.content.Context;
import androidx.annotation.NonNull;
import b.i.a.b.j.v.a;
import java.util.Objects;
/* compiled from: AutoValue_CreationContext.java */
/* loaded from: classes3.dex */
public final class c extends h {
    public final Context a;

    /* renamed from: b  reason: collision with root package name */
    public final a f769b;
    public final a c;
    public final String d;

    public c(Context context, a aVar, a aVar2, String str) {
        Objects.requireNonNull(context, "Null applicationContext");
        this.a = context;
        Objects.requireNonNull(aVar, "Null wallClock");
        this.f769b = aVar;
        Objects.requireNonNull(aVar2, "Null monotonicClock");
        this.c = aVar2;
        Objects.requireNonNull(str, "Null backendName");
        this.d = str;
    }

    @Override // b.i.a.b.j.q.h
    public Context a() {
        return this.a;
    }

    @Override // b.i.a.b.j.q.h
    @NonNull
    public String b() {
        return this.d;
    }

    @Override // b.i.a.b.j.q.h
    public a c() {
        return this.c;
    }

    @Override // b.i.a.b.j.q.h
    public a d() {
        return this.f769b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof h)) {
            return false;
        }
        h hVar = (h) obj;
        return this.a.equals(hVar.a()) && this.f769b.equals(hVar.d()) && this.c.equals(hVar.c()) && this.d.equals(hVar.b());
    }

    public int hashCode() {
        return ((((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.f769b.hashCode()) * 1000003) ^ this.c.hashCode()) * 1000003) ^ this.d.hashCode();
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("CreationContext{applicationContext=");
        R.append(this.a);
        R.append(", wallClock=");
        R.append(this.f769b);
        R.append(", monotonicClock=");
        R.append(this.c);
        R.append(", backendName=");
        return b.d.b.a.a.H(R, this.d, "}");
    }
}
