package b.i.a.b.j.q;

import androidx.annotation.Nullable;
import com.google.auto.value.AutoValue;
/* compiled from: BackendRequest.java */
@AutoValue
/* loaded from: classes3.dex */
public abstract class f {
    public abstract Iterable<b.i.a.b.j.f> a();

    @Nullable
    public abstract byte[] b();
}
