package b.i.a.b.j.q;

import android.content.Context;
import c0.a.a;
/* compiled from: MetadataBackendRegistry_Factory.java */
/* loaded from: classes3.dex */
public final class l implements a {
    public final a<Context> a;

    /* renamed from: b  reason: collision with root package name */
    public final a<i> f774b;

    public l(a<Context> aVar, a<i> aVar2) {
        this.a = aVar;
        this.f774b = aVar2;
    }

    @Override // c0.a.a
    public Object get() {
        return new k(this.a.get(), this.f774b.get());
    }
}
