package b.i.a.b.j.q;

import android.content.Context;
import androidx.annotation.NonNull;
import b.i.a.b.j.v.a;
import com.google.auto.value.AutoValue;
/* compiled from: CreationContext.java */
@AutoValue
/* loaded from: classes3.dex */
public abstract class h {
    public abstract Context a();

    @NonNull
    public abstract String b();

    public abstract a c();

    public abstract a d();
}
