package b.i.a.b.j.q;

import androidx.annotation.Nullable;
import b.i.a.b.j.f;
import java.util.Arrays;
/* compiled from: AutoValue_BackendRequest.java */
/* loaded from: classes3.dex */
public final class a extends f {
    public final Iterable<f> a;

    /* renamed from: b  reason: collision with root package name */
    public final byte[] f767b;

    public a(Iterable iterable, byte[] bArr, C0087a aVar) {
        this.a = iterable;
        this.f767b = bArr;
    }

    @Override // b.i.a.b.j.q.f
    public Iterable<f> a() {
        return this.a;
    }

    @Override // b.i.a.b.j.q.f
    @Nullable
    public byte[] b() {
        return this.f767b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        if (this.a.equals(fVar.a())) {
            if (Arrays.equals(this.f767b, fVar instanceof a ? ((a) fVar).f767b : fVar.b())) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ Arrays.hashCode(this.f767b);
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("BackendRequest{events=");
        R.append(this.a);
        R.append(", extras=");
        R.append(Arrays.toString(this.f767b));
        R.append("}");
        return R.toString();
    }
}
