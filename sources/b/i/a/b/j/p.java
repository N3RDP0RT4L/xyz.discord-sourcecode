package b.i.a.b.j;

import b.i.a.b.j.t.e;
import b.i.a.b.j.t.h.l;
import c0.a.a;
/* compiled from: TransportRuntime_Factory.java */
/* loaded from: classes3.dex */
public final class p implements a {
    public final a<b.i.a.b.j.v.a> a;

    /* renamed from: b  reason: collision with root package name */
    public final a<b.i.a.b.j.v.a> f766b;
    public final a<e> c;
    public final a<l> d;
    public final a<b.i.a.b.j.t.h.p> e;

    public p(a<b.i.a.b.j.v.a> aVar, a<b.i.a.b.j.v.a> aVar2, a<e> aVar3, a<l> aVar4, a<b.i.a.b.j.t.h.p> aVar5) {
        this.a = aVar;
        this.f766b = aVar2;
        this.c = aVar3;
        this.d = aVar4;
        this.e = aVar5;
    }

    @Override // c0.a.a
    public Object get() {
        return new n(this.a.get(), this.f766b.get(), this.c.get(), this.d.get(), this.e.get());
    }
}
