package b.i.a.b.j;

import b.i.a.b.b;
import b.i.a.b.c;
import b.i.a.b.d;
import b.i.a.b.e;
import b.i.a.b.f;
import b.i.a.b.h;
import b.i.a.b.j.a;
import b.i.a.b.j.b;
import b.i.a.b.j.i;
import java.util.HashMap;
import java.util.Objects;
/* compiled from: TransportImpl.java */
/* loaded from: classes3.dex */
public final class l<T> implements f<T> {
    public final i a;

    /* renamed from: b  reason: collision with root package name */
    public final String f764b;
    public final b c;
    public final e<T, byte[]> d;
    public final m e;

    public l(i iVar, String str, b bVar, e<T, byte[]> eVar, m mVar) {
        this.a = iVar;
        this.f764b = str;
        this.c = bVar;
        this.d = eVar;
        this.e = mVar;
    }

    @Override // b.i.a.b.f
    public void a(c<T> cVar) {
        b(cVar, k.a);
    }

    @Override // b.i.a.b.f
    public void b(c<T> cVar, h hVar) {
        m mVar = this.e;
        i iVar = this.a;
        Objects.requireNonNull(iVar, "Null transportContext");
        Objects.requireNonNull(cVar, "Null event");
        String str = this.f764b;
        Objects.requireNonNull(str, "Null transportName");
        e<T, byte[]> eVar = this.d;
        Objects.requireNonNull(eVar, "Null transformer");
        b bVar = this.c;
        Objects.requireNonNull(bVar, "Null encoding");
        n nVar = (n) mVar;
        b.i.a.b.j.t.e eVar2 = nVar.d;
        d c = cVar.c();
        i.a a = i.a();
        a.b(iVar.b());
        a.c(c);
        b.C0086b bVar2 = (b.C0086b) a;
        bVar2.f760b = iVar.c();
        i a2 = bVar2.a();
        a.b bVar3 = new a.b();
        bVar3.f = new HashMap();
        bVar3.e(nVar.f765b.a());
        bVar3.g(nVar.c.a());
        bVar3.f(str);
        bVar3.d(new e(bVar, eVar.apply(cVar.b())));
        bVar3.f758b = cVar.a();
        eVar2.a(a2, bVar3.b(), hVar);
    }
}
