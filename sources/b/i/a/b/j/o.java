package b.i.a.b.j;

import java.io.Closeable;
import java.io.IOException;
/* compiled from: TransportRuntimeComponent.java */
/* loaded from: classes3.dex */
public abstract class o implements Closeable {
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        ((c) this).o.get().close();
    }
}
