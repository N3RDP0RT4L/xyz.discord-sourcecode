package b.i.a.b.j;

import android.content.Context;
import b.i.a.b.b;
import b.i.a.b.g;
import b.i.a.b.j.b;
import b.i.a.b.j.i;
import b.i.a.b.j.t.e;
import b.i.a.b.j.t.h.l;
import b.i.a.b.j.t.h.p;
import b.i.a.b.j.u.a;
import b.i.a.b.j.v.a;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
/* compiled from: TransportRuntime.java */
/* loaded from: classes3.dex */
public class n implements m {
    public static volatile o a;

    /* renamed from: b  reason: collision with root package name */
    public final a f765b;
    public final a c;
    public final e d;
    public final l e;

    public n(a aVar, a aVar2, e eVar, l lVar, final p pVar) {
        this.f765b = aVar;
        this.c = aVar2;
        this.d = eVar;
        this.e = lVar;
        pVar.a.execute(new Runnable(pVar) { // from class: b.i.a.b.j.t.h.n
            public final p j;

            {
                this.j = pVar;
            }

            @Override // java.lang.Runnable
            public void run() {
                final p pVar2 = this.j;
                pVar2.d.a(new a.AbstractC0090a(pVar2) { // from class: b.i.a.b.j.t.h.o
                    public final p a;

                    {
                        this.a = pVar2;
                    }

                    @Override // b.i.a.b.j.u.a.AbstractC0090a
                    public Object execute() {
                        p pVar3 = this.a;
                        for (i iVar : pVar3.f789b.z()) {
                            pVar3.c.a(iVar, 1);
                        }
                        return null;
                    }
                });
            }
        });
    }

    public static n a() {
        o oVar = a;
        if (oVar != null) {
            return ((c) oVar).u.get();
        }
        throw new IllegalStateException("Not initialized!");
    }

    public static void b(Context context) {
        if (a == null) {
            synchronized (n.class) {
                if (a == null) {
                    Objects.requireNonNull(context);
                    a = new c(context, null);
                }
            }
        }
    }

    public g c(d dVar) {
        Set set;
        if (dVar instanceof d) {
            Objects.requireNonNull((b.i.a.b.i.a) dVar);
            set = Collections.unmodifiableSet(b.i.a.b.i.a.d);
        } else {
            set = Collections.singleton(new b("proto"));
        }
        i.a a2 = i.a();
        Objects.requireNonNull(dVar);
        a2.b("cct");
        b.C0086b bVar = (b.C0086b) a2;
        bVar.f760b = ((b.i.a.b.i.a) dVar).b();
        return new j(set, bVar.a(), this);
    }
}
