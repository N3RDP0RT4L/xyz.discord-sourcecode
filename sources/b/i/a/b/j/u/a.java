package b.i.a.b.j.u;

import androidx.annotation.WorkerThread;
/* compiled from: SynchronizationGuard.java */
@WorkerThread
/* loaded from: classes3.dex */
public interface a {

    /* compiled from: SynchronizationGuard.java */
    /* renamed from: b.i.a.b.j.u.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public interface AbstractC0090a<T> {
        T execute();
    }

    <T> T a(AbstractC0090a<T> aVar);
}
