package b.i.a.b.j.r.a;

import c0.a.a;
/* compiled from: InstanceFactory.java */
/* loaded from: classes3.dex */
public final class b<T> implements a {
    public final T a;

    public b(T t) {
        this.a = t;
    }

    @Override // c0.a.a
    public T get() {
        return this.a;
    }
}
