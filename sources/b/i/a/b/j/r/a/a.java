package b.i.a.b.j.r.a;
/* compiled from: DoubleCheck.java */
/* loaded from: classes3.dex */
public final class a<T> implements c0.a.a<T> {
    public static final Object a = new Object();

    /* renamed from: b  reason: collision with root package name */
    public volatile c0.a.a<T> f775b;
    public volatile Object c = a;

    public a(c0.a.a<T> aVar) {
        this.f775b = aVar;
    }

    public static Object a(Object obj, Object obj2) {
        if (!(obj != a) || obj == obj2) {
            return obj2;
        }
        throw new IllegalStateException("Scoped provider was invoked recursively returning different results: " + obj + " & " + obj2 + ". This is likely due to a circular dependency.");
    }

    @Override // c0.a.a
    public T get() {
        T t = (T) this.c;
        Object obj = a;
        if (t == obj) {
            synchronized (this) {
                t = this.c;
                if (t == obj) {
                    t = this.f775b.get();
                    a(this.c, t);
                    this.c = t;
                    this.f775b = null;
                }
            }
        }
        return t;
    }
}
