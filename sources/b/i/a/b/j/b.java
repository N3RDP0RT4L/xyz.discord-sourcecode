package b.i.a.b.j;

import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;
import b.i.a.b.d;
import b.i.a.b.j.i;
import java.util.Arrays;
import java.util.Objects;
/* compiled from: AutoValue_TransportContext.java */
/* loaded from: classes3.dex */
public final class b extends i {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final byte[] f759b;
    public final d c;

    /* compiled from: AutoValue_TransportContext.java */
    /* renamed from: b.i.a.b.j.b$b  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0086b extends i.a {
        public String a;

        /* renamed from: b  reason: collision with root package name */
        public byte[] f760b;
        public d c;

        @Override // b.i.a.b.j.i.a
        public i a() {
            String str = this.a == null ? " backendName" : "";
            if (this.c == null) {
                str = b.d.b.a.a.v(str, " priority");
            }
            if (str.isEmpty()) {
                return new b(this.a, this.f760b, this.c, null);
            }
            throw new IllegalStateException(b.d.b.a.a.v("Missing required properties:", str));
        }

        @Override // b.i.a.b.j.i.a
        public i.a b(String str) {
            Objects.requireNonNull(str, "Null backendName");
            this.a = str;
            return this;
        }

        @Override // b.i.a.b.j.i.a
        public i.a c(d dVar) {
            Objects.requireNonNull(dVar, "Null priority");
            this.c = dVar;
            return this;
        }
    }

    public b(String str, byte[] bArr, d dVar, a aVar) {
        this.a = str;
        this.f759b = bArr;
        this.c = dVar;
    }

    @Override // b.i.a.b.j.i
    public String b() {
        return this.a;
    }

    @Override // b.i.a.b.j.i
    @Nullable
    public byte[] c() {
        return this.f759b;
    }

    @Override // b.i.a.b.j.i
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public d d() {
        return this.c;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof i)) {
            return false;
        }
        i iVar = (i) obj;
        if (this.a.equals(iVar.b())) {
            if (Arrays.equals(this.f759b, iVar instanceof b ? ((b) iVar).f759b : iVar.c()) && this.c.equals(iVar.d())) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        return ((((this.a.hashCode() ^ 1000003) * 1000003) ^ Arrays.hashCode(this.f759b)) * 1000003) ^ this.c.hashCode();
    }
}
