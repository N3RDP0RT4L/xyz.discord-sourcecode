package b.i.a.b.j;

import b.i.a.b.b;
import b.i.a.b.e;
import b.i.a.b.f;
import b.i.a.b.g;
import java.util.Set;
/* compiled from: TransportFactoryImpl.java */
/* loaded from: classes3.dex */
public final class j implements g {
    public final Set<b> a;

    /* renamed from: b  reason: collision with root package name */
    public final i f763b;
    public final m c;

    public j(Set<b> set, i iVar, m mVar) {
        this.a = set;
        this.f763b = iVar;
        this.c = mVar;
    }

    @Override // b.i.a.b.g
    public <T> f<T> a(String str, Class<T> cls, b bVar, e<T, byte[]> eVar) {
        if (this.a.contains(bVar)) {
            return new l(this.f763b, str, bVar, eVar, this.c);
        }
        throw new IllegalArgumentException(String.format("%s is not supported byt this factory. Supported encodings are: %s.", bVar, this.a));
    }
}
