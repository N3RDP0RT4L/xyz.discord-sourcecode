package b.i.a.b.j;

import androidx.annotation.Nullable;
import b.i.a.b.j.f;
import java.util.Map;
import java.util.Objects;
/* compiled from: AutoValue_EventInternal.java */
/* loaded from: classes3.dex */
public final class a extends f {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final Integer f757b;
    public final e c;
    public final long d;
    public final long e;
    public final Map<String, String> f;

    /* compiled from: AutoValue_EventInternal.java */
    /* loaded from: classes3.dex */
    public static final class b extends f.a {
        public String a;

        /* renamed from: b  reason: collision with root package name */
        public Integer f758b;
        public e c;
        public Long d;
        public Long e;
        public Map<String, String> f;

        @Override // b.i.a.b.j.f.a
        public f b() {
            String str = this.a == null ? " transportName" : "";
            if (this.c == null) {
                str = b.d.b.a.a.v(str, " encodedPayload");
            }
            if (this.d == null) {
                str = b.d.b.a.a.v(str, " eventMillis");
            }
            if (this.e == null) {
                str = b.d.b.a.a.v(str, " uptimeMillis");
            }
            if (this.f == null) {
                str = b.d.b.a.a.v(str, " autoMetadata");
            }
            if (str.isEmpty()) {
                return new a(this.a, this.f758b, this.c, this.d.longValue(), this.e.longValue(), this.f, null);
            }
            throw new IllegalStateException(b.d.b.a.a.v("Missing required properties:", str));
        }

        @Override // b.i.a.b.j.f.a
        public Map<String, String> c() {
            Map<String, String> map = this.f;
            if (map != null) {
                return map;
            }
            throw new IllegalStateException("Property \"autoMetadata\" has not been set");
        }

        public f.a d(e eVar) {
            Objects.requireNonNull(eVar, "Null encodedPayload");
            this.c = eVar;
            return this;
        }

        public f.a e(long j) {
            this.d = Long.valueOf(j);
            return this;
        }

        public f.a f(String str) {
            Objects.requireNonNull(str, "Null transportName");
            this.a = str;
            return this;
        }

        public f.a g(long j) {
            this.e = Long.valueOf(j);
            return this;
        }
    }

    public a(String str, Integer num, e eVar, long j, long j2, Map map, C0085a aVar) {
        this.a = str;
        this.f757b = num;
        this.c = eVar;
        this.d = j;
        this.e = j2;
        this.f = map;
    }

    @Override // b.i.a.b.j.f
    public Map<String, String> b() {
        return this.f;
    }

    @Override // b.i.a.b.j.f
    @Nullable
    public Integer c() {
        return this.f757b;
    }

    @Override // b.i.a.b.j.f
    public e d() {
        return this.c;
    }

    @Override // b.i.a.b.j.f
    public long e() {
        return this.d;
    }

    public boolean equals(Object obj) {
        Integer num;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        return this.a.equals(fVar.g()) && ((num = this.f757b) != null ? num.equals(fVar.c()) : fVar.c() == null) && this.c.equals(fVar.d()) && this.d == fVar.e() && this.e == fVar.h() && this.f.equals(fVar.b());
    }

    @Override // b.i.a.b.j.f
    public String g() {
        return this.a;
    }

    @Override // b.i.a.b.j.f
    public long h() {
        return this.e;
    }

    public int hashCode() {
        int hashCode = (this.a.hashCode() ^ 1000003) * 1000003;
        Integer num = this.f757b;
        int hashCode2 = num == null ? 0 : num.hashCode();
        long j = this.d;
        long j2 = this.e;
        return ((((((((hashCode ^ hashCode2) * 1000003) ^ this.c.hashCode()) * 1000003) ^ ((int) (j ^ (j >>> 32)))) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)))) * 1000003) ^ this.f.hashCode();
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("EventInternal{transportName=");
        R.append(this.a);
        R.append(", code=");
        R.append(this.f757b);
        R.append(", encodedPayload=");
        R.append(this.c);
        R.append(", eventMillis=");
        R.append(this.d);
        R.append(", uptimeMillis=");
        R.append(this.e);
        R.append(", autoMetadata=");
        return b.d.b.a.a.L(R, this.f, "}");
    }
}
