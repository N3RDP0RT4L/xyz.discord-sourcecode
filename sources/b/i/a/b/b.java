package b.i.a.b;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.d.b.a.a;
import java.util.Objects;
/* compiled from: Encoding.java */
/* loaded from: classes.dex */
public final class b {
    public final String a;

    public b(@NonNull String str) {
        Objects.requireNonNull(str, "name is null");
        this.a = str;
    }

    public boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof b)) {
            return false;
        }
        return this.a.equals(((b) obj).a);
    }

    public int hashCode() {
        return this.a.hashCode() ^ 1000003;
    }

    @NonNull
    public String toString() {
        return a.H(a.R("Encoding{name=\""), this.a, "\"}");
    }
}
