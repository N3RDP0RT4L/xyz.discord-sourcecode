package b.i.a.b;
/* compiled from: Transformer.java */
/* loaded from: classes.dex */
public interface e<T, U> {
    U apply(T t);
}
