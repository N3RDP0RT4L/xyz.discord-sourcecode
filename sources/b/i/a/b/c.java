package b.i.a.b;

import androidx.annotation.Nullable;
import com.google.auto.value.AutoValue;
/* compiled from: Event.java */
@AutoValue
/* loaded from: classes.dex */
public abstract class c<T> {
    @Nullable
    public abstract Integer a();

    public abstract T b();

    public abstract d c();
}
