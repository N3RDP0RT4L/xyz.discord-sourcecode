package b.i.a.b.i.e;

import androidx.annotation.Nullable;
/* compiled from: AutoValue_AndroidClientInfo.java */
/* loaded from: classes3.dex */
public final class c extends b.i.a.b.i.e.a {
    public final Integer a;

    /* renamed from: b  reason: collision with root package name */
    public final String f743b;
    public final String c;
    public final String d;
    public final String e;
    public final String f;
    public final String g;
    public final String h;
    public final String i;
    public final String j;
    public final String k;
    public final String l;

    public c(Integer num, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, a aVar) {
        this.a = num;
        this.f743b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.g = str6;
        this.h = str7;
        this.i = str8;
        this.j = str9;
        this.k = str10;
        this.l = str11;
    }

    @Override // b.i.a.b.i.e.a
    @Nullable
    public String a() {
        return this.l;
    }

    @Override // b.i.a.b.i.e.a
    @Nullable
    public String b() {
        return this.j;
    }

    @Override // b.i.a.b.i.e.a
    @Nullable
    public String c() {
        return this.d;
    }

    @Override // b.i.a.b.i.e.a
    @Nullable
    public String d() {
        return this.h;
    }

    @Override // b.i.a.b.i.e.a
    @Nullable
    public String e() {
        return this.c;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof b.i.a.b.i.e.a)) {
            return false;
        }
        b.i.a.b.i.e.a aVar = (b.i.a.b.i.e.a) obj;
        Integer num = this.a;
        if (num != null ? num.equals(aVar.l()) : aVar.l() == null) {
            String str = this.f743b;
            if (str != null ? str.equals(aVar.i()) : aVar.i() == null) {
                String str2 = this.c;
                if (str2 != null ? str2.equals(aVar.e()) : aVar.e() == null) {
                    String str3 = this.d;
                    if (str3 != null ? str3.equals(aVar.c()) : aVar.c() == null) {
                        String str4 = this.e;
                        if (str4 != null ? str4.equals(aVar.k()) : aVar.k() == null) {
                            String str5 = this.f;
                            if (str5 != null ? str5.equals(aVar.j()) : aVar.j() == null) {
                                String str6 = this.g;
                                if (str6 != null ? str6.equals(aVar.g()) : aVar.g() == null) {
                                    String str7 = this.h;
                                    if (str7 != null ? str7.equals(aVar.d()) : aVar.d() == null) {
                                        String str8 = this.i;
                                        if (str8 != null ? str8.equals(aVar.f()) : aVar.f() == null) {
                                            String str9 = this.j;
                                            if (str9 != null ? str9.equals(aVar.b()) : aVar.b() == null) {
                                                String str10 = this.k;
                                                if (str10 != null ? str10.equals(aVar.h()) : aVar.h() == null) {
                                                    String str11 = this.l;
                                                    if (str11 == null) {
                                                        if (aVar.a() == null) {
                                                            return true;
                                                        }
                                                    } else if (str11.equals(aVar.a())) {
                                                        return true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    @Override // b.i.a.b.i.e.a
    @Nullable
    public String f() {
        return this.i;
    }

    @Override // b.i.a.b.i.e.a
    @Nullable
    public String g() {
        return this.g;
    }

    @Override // b.i.a.b.i.e.a
    @Nullable
    public String h() {
        return this.k;
    }

    public int hashCode() {
        Integer num = this.a;
        int i = 0;
        int hashCode = ((num == null ? 0 : num.hashCode()) ^ 1000003) * 1000003;
        String str = this.f743b;
        int hashCode2 = (hashCode ^ (str == null ? 0 : str.hashCode())) * 1000003;
        String str2 = this.c;
        int hashCode3 = (hashCode2 ^ (str2 == null ? 0 : str2.hashCode())) * 1000003;
        String str3 = this.d;
        int hashCode4 = (hashCode3 ^ (str3 == null ? 0 : str3.hashCode())) * 1000003;
        String str4 = this.e;
        int hashCode5 = (hashCode4 ^ (str4 == null ? 0 : str4.hashCode())) * 1000003;
        String str5 = this.f;
        int hashCode6 = (hashCode5 ^ (str5 == null ? 0 : str5.hashCode())) * 1000003;
        String str6 = this.g;
        int hashCode7 = (hashCode6 ^ (str6 == null ? 0 : str6.hashCode())) * 1000003;
        String str7 = this.h;
        int hashCode8 = (hashCode7 ^ (str7 == null ? 0 : str7.hashCode())) * 1000003;
        String str8 = this.i;
        int hashCode9 = (hashCode8 ^ (str8 == null ? 0 : str8.hashCode())) * 1000003;
        String str9 = this.j;
        int hashCode10 = (hashCode9 ^ (str9 == null ? 0 : str9.hashCode())) * 1000003;
        String str10 = this.k;
        int hashCode11 = (hashCode10 ^ (str10 == null ? 0 : str10.hashCode())) * 1000003;
        String str11 = this.l;
        if (str11 != null) {
            i = str11.hashCode();
        }
        return hashCode11 ^ i;
    }

    @Override // b.i.a.b.i.e.a
    @Nullable
    public String i() {
        return this.f743b;
    }

    @Override // b.i.a.b.i.e.a
    @Nullable
    public String j() {
        return this.f;
    }

    @Override // b.i.a.b.i.e.a
    @Nullable
    public String k() {
        return this.e;
    }

    @Override // b.i.a.b.i.e.a
    @Nullable
    public Integer l() {
        return this.a;
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("AndroidClientInfo{sdkVersion=");
        R.append(this.a);
        R.append(", model=");
        R.append(this.f743b);
        R.append(", hardware=");
        R.append(this.c);
        R.append(", device=");
        R.append(this.d);
        R.append(", product=");
        R.append(this.e);
        R.append(", osBuild=");
        R.append(this.f);
        R.append(", manufacturer=");
        R.append(this.g);
        R.append(", fingerprint=");
        R.append(this.h);
        R.append(", locale=");
        R.append(this.i);
        R.append(", country=");
        R.append(this.j);
        R.append(", mccMnc=");
        R.append(this.k);
        R.append(", applicationBuild=");
        return b.d.b.a.a.H(R, this.l, "}");
    }
}
