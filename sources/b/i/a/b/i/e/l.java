package b.i.a.b.i.e;

import androidx.annotation.Nullable;
import com.google.auto.value.AutoValue;
/* compiled from: LogEvent.java */
@AutoValue
/* loaded from: classes3.dex */
public abstract class l {

    /* compiled from: LogEvent.java */
    @AutoValue.Builder
    /* loaded from: classes3.dex */
    public static abstract class a {
    }

    @Nullable
    public abstract Integer a();

    public abstract long b();

    public abstract long c();

    @Nullable
    public abstract o d();

    @Nullable
    public abstract byte[] e();

    @Nullable
    public abstract String f();

    public abstract long g();
}
