package b.i.a.b.i.e;

import androidx.annotation.Nullable;
import com.google.auto.value.AutoValue;
import java.util.List;
/* compiled from: LogRequest.java */
@AutoValue
/* loaded from: classes3.dex */
public abstract class m {
    @Nullable
    public abstract k a();

    @Nullable
    public abstract List<l> b();

    @Nullable
    public abstract Integer c();

    @Nullable
    public abstract String d();

    @Nullable
    public abstract p e();

    public abstract long f();

    public abstract long g();
}
