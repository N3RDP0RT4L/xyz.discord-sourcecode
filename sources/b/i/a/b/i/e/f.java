package b.i.a.b.i.e;

import androidx.annotation.Nullable;
import b.i.a.b.i.e.l;
import java.util.Arrays;
/* compiled from: AutoValue_LogEvent.java */
/* loaded from: classes3.dex */
public final class f extends l {
    public final long a;

    /* renamed from: b  reason: collision with root package name */
    public final Integer f745b;
    public final long c;
    public final byte[] d;
    public final String e;
    public final long f;
    public final o g;

    /* compiled from: AutoValue_LogEvent.java */
    /* loaded from: classes3.dex */
    public static final class b extends l.a {
        public Long a;

        /* renamed from: b  reason: collision with root package name */
        public Integer f746b;
        public Long c;
        public byte[] d;
        public String e;
        public Long f;
        public o g;
    }

    public f(long j, Integer num, long j2, byte[] bArr, String str, long j3, o oVar, a aVar) {
        this.a = j;
        this.f745b = num;
        this.c = j2;
        this.d = bArr;
        this.e = str;
        this.f = j3;
        this.g = oVar;
    }

    @Override // b.i.a.b.i.e.l
    @Nullable
    public Integer a() {
        return this.f745b;
    }

    @Override // b.i.a.b.i.e.l
    public long b() {
        return this.a;
    }

    @Override // b.i.a.b.i.e.l
    public long c() {
        return this.c;
    }

    @Override // b.i.a.b.i.e.l
    @Nullable
    public o d() {
        return this.g;
    }

    @Override // b.i.a.b.i.e.l
    @Nullable
    public byte[] e() {
        return this.d;
    }

    public boolean equals(Object obj) {
        Integer num;
        String str;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof l)) {
            return false;
        }
        l lVar = (l) obj;
        if (this.a == lVar.b() && ((num = this.f745b) != null ? num.equals(lVar.a()) : lVar.a() == null) && this.c == lVar.c()) {
            if (Arrays.equals(this.d, lVar instanceof f ? ((f) lVar).d : lVar.e()) && ((str = this.e) != null ? str.equals(lVar.f()) : lVar.f() == null) && this.f == lVar.g()) {
                o oVar = this.g;
                if (oVar == null) {
                    if (lVar.d() == null) {
                        return true;
                    }
                } else if (oVar.equals(lVar.d())) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override // b.i.a.b.i.e.l
    @Nullable
    public String f() {
        return this.e;
    }

    @Override // b.i.a.b.i.e.l
    public long g() {
        return this.f;
    }

    public int hashCode() {
        long j = this.a;
        int i = (((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003;
        Integer num = this.f745b;
        int i2 = 0;
        int hashCode = num == null ? 0 : num.hashCode();
        long j2 = this.c;
        int hashCode2 = (((((i ^ hashCode) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)))) * 1000003) ^ Arrays.hashCode(this.d)) * 1000003;
        String str = this.e;
        int hashCode3 = str == null ? 0 : str.hashCode();
        long j3 = this.f;
        int i3 = (((hashCode2 ^ hashCode3) * 1000003) ^ ((int) ((j3 >>> 32) ^ j3))) * 1000003;
        o oVar = this.g;
        if (oVar != null) {
            i2 = oVar.hashCode();
        }
        return i3 ^ i2;
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("LogEvent{eventTimeMs=");
        R.append(this.a);
        R.append(", eventCode=");
        R.append(this.f745b);
        R.append(", eventUptimeMs=");
        R.append(this.c);
        R.append(", sourceExtension=");
        R.append(Arrays.toString(this.d));
        R.append(", sourceExtensionJsonProto3=");
        R.append(this.e);
        R.append(", timezoneOffsetSeconds=");
        R.append(this.f);
        R.append(", networkConnectionInfo=");
        R.append(this.g);
        R.append("}");
        return R.toString();
    }
}
