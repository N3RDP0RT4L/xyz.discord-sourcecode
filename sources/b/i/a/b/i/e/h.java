package b.i.a.b.i.e;

import b.d.b.a.a;
/* compiled from: AutoValue_LogResponse.java */
/* loaded from: classes3.dex */
public final class h extends n {
    public final long a;

    public h(long j) {
        this.a = j;
    }

    @Override // b.i.a.b.i.e.n
    public long b() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return (obj instanceof n) && this.a == ((n) obj).b();
    }

    public int hashCode() {
        long j = this.a;
        return 1000003 ^ ((int) (j ^ (j >>> 32)));
    }

    public String toString() {
        return a.B(a.R("LogResponse{nextRequestWaitMillis="), this.a, "}");
    }
}
