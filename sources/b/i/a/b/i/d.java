package b.i.a.b.i;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import androidx.annotation.Nullable;
import b.i.a.b.i.e.c;
import b.i.a.b.i.e.f;
import b.i.a.b.i.e.i;
import b.i.a.b.i.e.j;
import b.i.a.b.i.e.k;
import b.i.a.b.i.e.o;
import b.i.a.b.i.e.p;
import b.i.a.b.j.f;
import b.i.a.b.j.q.f;
import b.i.a.b.j.q.g;
import b.i.a.b.j.q.m;
import b.i.c.p.h.e;
import com.adjust.sdk.Constants;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
/* compiled from: CctTransportBackend.java */
/* loaded from: classes.dex */
public final class d implements m {
    public final b.i.c.p.a a;

    /* renamed from: b  reason: collision with root package name */
    public final ConnectivityManager f740b;
    public final Context c;
    public final b.i.a.b.j.v.a e;
    public final b.i.a.b.j.v.a f;
    public final URL d = c(b.i.a.b.i.a.a);
    public final int g = 40000;

    /* compiled from: CctTransportBackend.java */
    /* loaded from: classes.dex */
    public static final class a {
        public final URL a;

        /* renamed from: b  reason: collision with root package name */
        public final j f741b;
        @Nullable
        public final String c;

        public a(URL url, j jVar, @Nullable String str) {
            this.a = url;
            this.f741b = jVar;
            this.c = str;
        }
    }

    /* compiled from: CctTransportBackend.java */
    /* loaded from: classes.dex */
    public static final class b {
        public final int a;
        @Nullable

        /* renamed from: b  reason: collision with root package name */
        public final URL f742b;
        public final long c;

        public b(int i, @Nullable URL url, long j) {
            this.a = i;
            this.f742b = url;
            this.c = j;
        }
    }

    public d(Context context, b.i.a.b.j.v.a aVar, b.i.a.b.j.v.a aVar2) {
        e eVar = new e();
        ((b.i.a.b.i.e.b) b.i.a.b.i.e.b.a).a(eVar);
        eVar.e = true;
        this.a = new b.i.c.p.h.d(eVar);
        this.c = context;
        this.f740b = (ConnectivityManager) context.getSystemService("connectivity");
        this.e = aVar2;
        this.f = aVar;
    }

    public static URL c(String str) {
        try {
            return new URL(str);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException(b.d.b.a.a.v("Invalid url: ", str), e);
        }
    }

    @Override // b.i.a.b.j.q.m
    public g a(f fVar) {
        g.a aVar;
        IOException e;
        String str;
        Integer num;
        g.a aVar2;
        f.b bVar;
        g.a aVar3 = g.a.TRANSIENT_ERROR;
        HashMap hashMap = new HashMap();
        b.i.a.b.j.q.a aVar4 = (b.i.a.b.j.q.a) fVar;
        for (b.i.a.b.j.f fVar2 : aVar4.a) {
            String g = fVar2.g();
            if (!hashMap.containsKey(g)) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(fVar2);
                hashMap.put(g, arrayList);
            } else {
                ((List) hashMap.get(g)).add(fVar2);
            }
        }
        ArrayList arrayList2 = new ArrayList();
        Iterator it = hashMap.entrySet().iterator();
        while (true) {
            String str2 = null;
            if (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                b.i.a.b.j.f fVar3 = (b.i.a.b.j.f) ((List) entry.getValue()).get(0);
                p pVar = p.DEFAULT;
                Long valueOf = Long.valueOf(this.f.a());
                Long valueOf2 = Long.valueOf(this.e.a());
                b.i.a.b.i.e.e eVar = new b.i.a.b.i.e.e(k.a.ANDROID_FIREBASE, new c(Integer.valueOf(fVar3.f("sdk-version")), fVar3.a("model"), fVar3.a("hardware"), fVar3.a("device"), fVar3.a("product"), fVar3.a("os-uild"), fVar3.a("manufacturer"), fVar3.a("fingerprint"), fVar3.a("locale"), fVar3.a("country"), fVar3.a("mcc_mnc"), fVar3.a("application_build"), null), null);
                try {
                    str = null;
                    num = Integer.valueOf(Integer.parseInt((String) entry.getKey()));
                } catch (NumberFormatException unused) {
                    str = (String) entry.getKey();
                    num = null;
                }
                ArrayList arrayList3 = new ArrayList();
                Iterator it2 = ((List) entry.getValue()).iterator();
                while (it2.hasNext()) {
                    b.i.a.b.j.f fVar4 = (b.i.a.b.j.f) it2.next();
                    b.i.a.b.j.e d = fVar4.d();
                    it = it;
                    b.i.a.b.b bVar2 = d.a;
                    it2 = it2;
                    if (bVar2.equals(new b.i.a.b.b("proto"))) {
                        byte[] bArr = d.f762b;
                        bVar = new f.b();
                        bVar.d = bArr;
                    } else if (bVar2.equals(new b.i.a.b.b("json"))) {
                        String str3 = new String(d.f762b, Charset.forName(Constants.ENCODING));
                        bVar = new f.b();
                        bVar.e = str3;
                    } else {
                        aVar2 = aVar3;
                        Log.w(b.c.a.a0.d.D0("CctTransportBackend"), String.format("Received event of unsupported encoding %s. Skipping...", bVar2));
                        aVar3 = aVar2;
                    }
                    bVar.a = Long.valueOf(fVar4.e());
                    bVar.c = Long.valueOf(fVar4.h());
                    String str4 = fVar4.b().get("tz-offset");
                    bVar.f = Long.valueOf(str4 == null ? 0L : Long.valueOf(str4).longValue());
                    aVar2 = aVar3;
                    bVar.g = new i(o.b.C.get(fVar4.f("net-type")), o.a.E.get(fVar4.f("mobile-subtype")), null);
                    if (fVar4.c() != null) {
                        bVar.f746b = fVar4.c();
                    }
                    String str5 = bVar.a == null ? " eventTimeMs" : "";
                    if (bVar.c == null) {
                        str5 = b.d.b.a.a.v(str5, " eventUptimeMs");
                    }
                    if (bVar.f == null) {
                        str5 = b.d.b.a.a.v(str5, " timezoneOffsetSeconds");
                    }
                    if (str5.isEmpty()) {
                        arrayList3.add(new b.i.a.b.i.e.f(bVar.a.longValue(), bVar.f746b, bVar.c.longValue(), bVar.d, bVar.e, bVar.f.longValue(), bVar.g, null));
                        aVar3 = aVar2;
                    } else {
                        throw new IllegalStateException(b.d.b.a.a.v("Missing required properties:", str5));
                    }
                }
                it = it;
                aVar3 = aVar3;
                String str6 = valueOf == null ? " requestTimeMs" : "";
                if (valueOf2 == null) {
                    str6 = b.d.b.a.a.v(str6, " requestUptimeMs");
                }
                if (str6.isEmpty()) {
                    arrayList2.add(new b.i.a.b.i.e.g(valueOf.longValue(), valueOf2.longValue(), eVar, num, str, arrayList3, pVar, null));
                } else {
                    throw new IllegalStateException(b.d.b.a.a.v("Missing required properties:", str6));
                }
            } else {
                g.a aVar5 = aVar3;
                b.i.a.b.i.e.d dVar = new b.i.a.b.i.e.d(arrayList2);
                URL url = this.d;
                if (aVar4.f767b != null) {
                    try {
                        b.i.a.b.i.a a2 = b.i.a.b.i.a.a(((b.i.a.b.j.q.a) fVar).f767b);
                        String str7 = a2.g;
                        if (str7 != null) {
                            str2 = str7;
                        }
                        String str8 = a2.f;
                        if (str8 != null) {
                            url = c(str8);
                        }
                    } catch (IllegalArgumentException unused2) {
                        return g.a();
                    }
                }
                try {
                    b bVar3 = (b) b.c.a.a0.d.V1(5, new a(url, dVar, str2), new b.i.a.b.i.b(this), c.a);
                    int i = bVar3.a;
                    if (i == 200) {
                        return new b.i.a.b.j.q.b(g.a.OK, bVar3.c);
                    }
                    if (i < 500 && i != 404) {
                        return g.a();
                    }
                    aVar = aVar5;
                    try {
                        return new b.i.a.b.j.q.b(aVar, -1L);
                    } catch (IOException e2) {
                        e = e2;
                        b.c.a.a0.d.c0("CctTransportBackend", "Could not make request to the backend", e);
                        return new b.i.a.b.j.q.b(aVar, -1L);
                    }
                } catch (IOException e3) {
                    e = e3;
                    aVar = aVar5;
                }
            }
        }
    }

    @Override // b.i.a.b.j.q.m
    public b.i.a.b.j.f b(b.i.a.b.j.f fVar) {
        int i;
        int i2;
        NetworkInfo activeNetworkInfo = this.f740b.getActiveNetworkInfo();
        f.a i3 = fVar.i();
        i3.c().put("sdk-version", String.valueOf(Build.VERSION.SDK_INT));
        i3.c().put("model", Build.MODEL);
        i3.c().put("hardware", Build.HARDWARE);
        i3.c().put("device", Build.DEVICE);
        i3.c().put("product", Build.PRODUCT);
        i3.c().put("os-uild", Build.ID);
        i3.c().put("manufacturer", Build.MANUFACTURER);
        i3.c().put("fingerprint", Build.FINGERPRINT);
        Calendar.getInstance();
        i3.c().put("tz-offset", String.valueOf(TimeZone.getDefault().getOffset(Calendar.getInstance().getTimeInMillis()) / 1000));
        if (activeNetworkInfo == null) {
            i = o.b.NONE.f();
        } else {
            i = activeNetworkInfo.getType();
        }
        i3.c().put("net-type", String.valueOf(i));
        int i4 = -1;
        if (activeNetworkInfo == null) {
            i2 = o.a.UNKNOWN_MOBILE_SUBTYPE.f();
        } else {
            i2 = activeNetworkInfo.getSubtype();
            if (i2 == -1) {
                i2 = o.a.COMBINED.f();
            } else if (o.a.E.get(i2) == null) {
                i2 = 0;
            }
        }
        i3.c().put("mobile-subtype", String.valueOf(i2));
        i3.c().put("country", Locale.getDefault().getCountry());
        i3.c().put("locale", Locale.getDefault().getLanguage());
        i3.c().put("mcc_mnc", ((TelephonyManager) this.c.getSystemService("phone")).getSimOperator());
        Context context = this.c;
        try {
            i4 = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            b.c.a.a0.d.c0("CctTransportBackend", "Unable to find version code for package", e);
        }
        i3.c().put("application_build", Integer.toString(i4));
        return i3.b();
    }
}
