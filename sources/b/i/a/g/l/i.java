package b.i.a.g.l;

import androidx.transition.Transition;
/* compiled from: TransitionListenerAdapter.java */
/* loaded from: classes3.dex */
public abstract class i implements Transition.TransitionListener {
    @Override // androidx.transition.Transition.TransitionListener
    public void onTransitionCancel(Transition transition) {
    }

    @Override // androidx.transition.Transition.TransitionListener
    public void onTransitionPause(Transition transition) {
    }

    @Override // androidx.transition.Transition.TransitionListener
    public void onTransitionResume(Transition transition) {
    }
}
