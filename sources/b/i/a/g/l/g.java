package b.i.a.g.l;

import android.graphics.Path;
import com.google.android.material.shape.ShapeAppearanceModel;
import com.google.android.material.shape.ShapeAppearancePathProvider;
/* compiled from: MaskEvaluator.java */
/* loaded from: classes3.dex */
public class g {
    public final Path a = new Path();

    /* renamed from: b  reason: collision with root package name */
    public final Path f1631b = new Path();
    public final Path c = new Path();
    public final ShapeAppearancePathProvider d = ShapeAppearancePathProvider.getInstance();
    public ShapeAppearanceModel e;
}
