package b.i.a.g.l;
/* compiled from: FadeModeResult.java */
/* loaded from: classes3.dex */
public class c {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final int f1628b;
    public final boolean c;

    public c(int i, int i2, boolean z2) {
        this.a = i;
        this.f1628b = i2;
        this.c = z2;
    }

    public static c a(int i, int i2) {
        return new c(i, i2, false);
    }
}
