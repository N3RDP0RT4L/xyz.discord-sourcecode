package b.i.a.g.l.l;

import android.transition.Transition;
import androidx.annotation.RequiresApi;
/* compiled from: TransitionListenerAdapter.java */
@RequiresApi(21)
/* loaded from: classes3.dex */
public abstract class i implements Transition.TransitionListener {
    @Override // android.transition.Transition.TransitionListener
    public void onTransitionCancel(Transition transition) {
    }

    @Override // android.transition.Transition.TransitionListener
    public void onTransitionEnd(Transition transition) {
    }

    @Override // android.transition.Transition.TransitionListener
    public void onTransitionPause(Transition transition) {
    }

    @Override // android.transition.Transition.TransitionListener
    public void onTransitionResume(Transition transition) {
    }

    @Override // android.transition.Transition.TransitionListener
    public void onTransitionStart(Transition transition) {
    }
}
