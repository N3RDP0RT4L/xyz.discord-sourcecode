package b.i.a.g.l.l;

import androidx.annotation.RequiresApi;
/* compiled from: FitModeResult.java */
@RequiresApi(21)
/* loaded from: classes3.dex */
public class f {
    public final float a;

    /* renamed from: b  reason: collision with root package name */
    public final float f1635b;
    public final float c;
    public final float d;
    public final float e;
    public final float f;

    public f(float f, float f2, float f3, float f4, float f5, float f6) {
        this.a = f;
        this.f1635b = f2;
        this.c = f3;
        this.d = f4;
        this.e = f5;
        this.f = f6;
    }
}
