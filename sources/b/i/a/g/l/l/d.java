package b.i.a.g.l.l;

import android.graphics.RectF;
import androidx.annotation.RequiresApi;
/* compiled from: FitModeEvaluator.java */
@RequiresApi(21)
/* loaded from: classes3.dex */
public interface d {
    f a(float f, float f2, float f3, float f4, float f5, float f6, float f7);

    boolean b(f fVar);

    void c(RectF rectF, float f, f fVar);
}
