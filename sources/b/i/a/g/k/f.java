package b.i.a.g.k;

import android.view.accessibility.AccessibilityManager;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.exifinterface.media.ExifInterface;
import com.google.android.material.R;
import com.google.android.material.chip.Chip;
import com.google.android.material.timepicker.ClockHandView;
import com.google.android.material.timepicker.TimeModel;
import com.google.android.material.timepicker.TimePickerView;
import java.util.Locale;
import java.util.Objects;
/* compiled from: TimePickerClockPresenter.java */
/* loaded from: classes3.dex */
public class f implements ClockHandView.OnRotateListener, TimePickerView.d, TimePickerView.c, ClockHandView.OnActionUpListener, g {
    public static final String[] j = {"12", "1", ExifInterface.GPS_MEASUREMENT_2D, ExifInterface.GPS_MEASUREMENT_3D, "4", "5", "6", "7", "8", "9", "10", "11"};
    public static final String[] k = {"00", ExifInterface.GPS_MEASUREMENT_2D, "4", "6", "8", "10", "12", "14", "16", "18", "20", "22"};
    public static final String[] l = {"00", "5", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55"};
    public TimePickerView m;
    public TimeModel n;
    public float o;
    public float p;
    public boolean q = false;

    public f(TimePickerView timePickerView, TimeModel timeModel) {
        this.m = timePickerView;
        this.n = timeModel;
        if (timeModel.l == 0) {
            timePickerView.n.setVisibility(0);
        }
        this.m.l.q.add(this);
        TimePickerView timePickerView2 = this.m;
        timePickerView2.q = this;
        timePickerView2.p = this;
        timePickerView2.l.f3048y = this;
        g(j, "%d");
        g(k, "%d");
        g(l, "%02d");
        a();
    }

    @Override // b.i.a.g.k.g
    public void a() {
        this.p = d() * this.n.b();
        TimeModel timeModel = this.n;
        this.o = timeModel.n * 6;
        e(timeModel.o, false);
        f();
    }

    @Override // com.google.android.material.timepicker.TimePickerView.d
    public void b(int i) {
        e(i, true);
    }

    @Override // b.i.a.g.k.g
    public void c() {
        this.m.setVisibility(8);
    }

    public final int d() {
        return this.n.l == 1 ? 15 : 30;
    }

    public void e(int i, boolean z2) {
        String[] strArr;
        boolean z3 = false;
        boolean z4 = i == 12;
        TimePickerView timePickerView = this.m;
        timePickerView.l.l = z4;
        TimeModel timeModel = this.n;
        timeModel.o = i;
        if (z4) {
            strArr = l;
        } else {
            strArr = timeModel.l == 1 ? k : j;
        }
        timePickerView.m.c(strArr, z4 ? R.string.material_minute_suffix : R.string.material_hour_suffix);
        this.m.l.b(z4 ? this.o : this.p, z2);
        TimePickerView timePickerView2 = this.m;
        timePickerView2.j.setChecked(i == 12);
        Chip chip = timePickerView2.k;
        if (i == 10) {
            z3 = true;
        }
        chip.setChecked(z3);
        ViewCompat.setAccessibilityDelegate(this.m.k, new a(this.m.getContext(), R.string.material_hour_selection));
        ViewCompat.setAccessibilityDelegate(this.m.j, new a(this.m.getContext(), R.string.material_minute_selection));
    }

    public final void f() {
        TimePickerView timePickerView = this.m;
        TimeModel timeModel = this.n;
        int i = timeModel.p;
        int b2 = timeModel.b();
        int i2 = this.n.n;
        timePickerView.n.check(i == 1 ? R.id.material_clock_period_pm_button : R.id.material_clock_period_am_button);
        Locale locale = timePickerView.getResources().getConfiguration().locale;
        String format = String.format(locale, "%02d", Integer.valueOf(i2));
        String format2 = String.format(locale, "%02d", Integer.valueOf(b2));
        timePickerView.j.setText(format);
        timePickerView.k.setText(format2);
    }

    public final void g(String[] strArr, String str) {
        for (int i = 0; i < strArr.length; i++) {
            strArr[i] = TimeModel.a(this.m.getResources(), strArr[i], str);
        }
    }

    @Override // com.google.android.material.timepicker.ClockHandView.OnActionUpListener
    public void onActionUp(float f, boolean z2) {
        this.q = true;
        TimeModel timeModel = this.n;
        int i = timeModel.n;
        int i2 = timeModel.m;
        if (timeModel.o == 10) {
            TimePickerView timePickerView = this.m;
            timePickerView.l.b(this.p, false);
            if (!((AccessibilityManager) ContextCompat.getSystemService(this.m.getContext(), AccessibilityManager.class)).isTouchExplorationEnabled()) {
                e(12, true);
            }
        } else {
            int round = Math.round(f);
            if (!z2) {
                TimeModel timeModel2 = this.n;
                Objects.requireNonNull(timeModel2);
                timeModel2.n = (((round + 15) / 30) * 5) % 60;
                this.o = this.n.n * 6;
            }
            TimePickerView timePickerView2 = this.m;
            timePickerView2.l.b(this.o, z2);
        }
        this.q = false;
        f();
        TimeModel timeModel3 = this.n;
        if (timeModel3.n != i || timeModel3.m != i2) {
            this.m.performHapticFeedback(4);
        }
    }

    @Override // com.google.android.material.timepicker.ClockHandView.OnRotateListener
    public void onRotate(float f, boolean z2) {
        if (!this.q) {
            TimeModel timeModel = this.n;
            int i = timeModel.m;
            int i2 = timeModel.n;
            int round = Math.round(f);
            TimeModel timeModel2 = this.n;
            if (timeModel2.o == 12) {
                int i3 = ((round + 3) / 6) % 60;
                timeModel2.n = i3;
                this.o = (float) Math.floor(i3 * 6);
            } else {
                this.n.c((round + (d() / 2)) / d());
                this.p = d() * this.n.b();
            }
            if (!z2) {
                f();
                TimeModel timeModel3 = this.n;
                if (timeModel3.n != i2 || timeModel3.m != i) {
                    this.m.performHapticFeedback(4);
                }
            }
        }
    }

    @Override // b.i.a.g.k.g
    public void show() {
        this.m.setVisibility(0);
    }
}
