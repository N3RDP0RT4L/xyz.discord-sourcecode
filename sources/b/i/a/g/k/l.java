package b.i.a.g.k;

import android.view.GestureDetector;
import android.view.MotionEvent;
import com.google.android.material.timepicker.ChipTextInputComboView;
import com.google.android.material.timepicker.MaterialTimePicker;
import com.google.android.material.timepicker.TimePickerView;
/* compiled from: TimePickerView.java */
/* loaded from: classes3.dex */
public class l extends GestureDetector.SimpleOnGestureListener {
    public final /* synthetic */ TimePickerView j;

    public l(TimePickerView timePickerView) {
        this.j = timePickerView;
    }

    @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnDoubleTapListener
    public boolean onDoubleTap(MotionEvent motionEvent) {
        boolean onDoubleTap = super.onDoubleTap(motionEvent);
        TimePickerView.b bVar = this.j.r;
        if (bVar != null) {
            MaterialTimePicker.a aVar = (MaterialTimePicker.a) bVar;
            boolean z2 = true;
            MaterialTimePicker.access$402(MaterialTimePicker.this, 1);
            MaterialTimePicker materialTimePicker = MaterialTimePicker.this;
            MaterialTimePicker.access$600(materialTimePicker, MaterialTimePicker.access$500(materialTimePicker));
            i access$700 = MaterialTimePicker.access$700(MaterialTimePicker.this);
            access$700.n.setChecked(access$700.k.o == 12);
            ChipTextInputComboView chipTextInputComboView = access$700.o;
            if (access$700.k.o != 10) {
                z2 = false;
            }
            chipTextInputComboView.setChecked(z2);
        }
        return onDoubleTap;
    }
}
