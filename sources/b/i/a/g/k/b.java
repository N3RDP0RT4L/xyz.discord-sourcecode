package b.i.a.g.k;

import android.view.ViewTreeObserver;
import com.google.android.material.timepicker.ClockFaceView;
import com.google.android.material.timepicker.ClockHandView;
/* compiled from: ClockFaceView.java */
/* loaded from: classes3.dex */
public class b implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ ClockFaceView j;

    public b(ClockFaceView clockFaceView) {
        this.j = clockFaceView;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        if (!this.j.isShown()) {
            return true;
        }
        this.j.getViewTreeObserver().removeOnPreDrawListener(this);
        ClockFaceView clockFaceView = this.j;
        int height = ((this.j.getHeight() / 2) - clockFaceView.m.r) - clockFaceView.t;
        if (height != clockFaceView.k) {
            clockFaceView.k = height;
            clockFaceView.a();
            ClockHandView clockHandView = clockFaceView.m;
            clockHandView.A = clockFaceView.k;
            clockHandView.invalidate();
        }
        return true;
    }
}
