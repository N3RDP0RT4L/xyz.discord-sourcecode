package b.i.a.g.d;

import android.widget.BaseAdapter;
import android.widget.TextView;
import androidx.annotation.Nullable;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.DateSelector;
import com.google.android.material.datepicker.MaterialCalendarGridView;
import com.google.android.material.datepicker.Month;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
/* compiled from: MonthAdapter.java */
/* loaded from: classes3.dex */
public class g extends BaseAdapter {
    public static final int j = l.i().getMaximum(4);
    public final Month k;
    public final DateSelector<?> l;
    public Collection<Long> m;
    public b n;
    public final CalendarConstraints o;

    public g(Month month, DateSelector<?> dateSelector, CalendarConstraints calendarConstraints) {
        this.k = month;
        this.l = dateSelector;
        this.o = calendarConstraints;
        this.m = dateSelector.getSelectedDays();
    }

    public int a(int i) {
        return b() + (i - 1);
    }

    public int b() {
        return this.k.j();
    }

    @Nullable
    /* renamed from: c */
    public Long getItem(int i) {
        if (i < this.k.j() || i > d()) {
            return null;
        }
        Month month = this.k;
        return Long.valueOf(month.k((i - month.j()) + 1));
    }

    public int d() {
        return (this.k.j() + this.k.n) - 1;
    }

    public final void e(@Nullable TextView textView, long j2) {
        a aVar;
        if (textView != null) {
            boolean z2 = false;
            if (this.o.getDateValidator().isValid(j2)) {
                textView.setEnabled(true);
                Iterator<Long> it = this.l.getSelectedDays().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    if (l.a(j2) == l.a(it.next().longValue())) {
                        z2 = true;
                        break;
                    }
                }
                if (z2) {
                    aVar = this.n.f1605b;
                } else if (l.h().getTimeInMillis() == j2) {
                    aVar = this.n.c;
                } else {
                    aVar = this.n.a;
                }
            } else {
                textView.setEnabled(false);
                aVar = this.n.g;
            }
            aVar.b(textView);
        }
    }

    public final void f(MaterialCalendarGridView materialCalendarGridView, long j2) {
        if (Month.h(j2).equals(this.k)) {
            Calendar d = l.d(this.k.j);
            d.setTimeInMillis(j2);
            e((TextView) materialCalendarGridView.getChildAt(materialCalendarGridView.getAdapter2().a(d.get(5)) - materialCalendarGridView.getFirstVisiblePosition()), j2);
        }
    }

    @Override // android.widget.Adapter
    public int getCount() {
        return b() + this.k.n;
    }

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        return i / this.k.m;
    }

    /* JADX WARN: Removed duplicated region for block: B:28:0x00e3  */
    @Override // android.widget.Adapter
    @androidx.annotation.NonNull
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public android.view.View getView(int r7, @androidx.annotation.Nullable android.view.View r8, @androidx.annotation.NonNull android.view.ViewGroup r9) {
        /*
            r6 = this;
            android.content.Context r0 = r9.getContext()
            b.i.a.g.d.b r1 = r6.n
            if (r1 != 0) goto Lf
            b.i.a.g.d.b r1 = new b.i.a.g.d.b
            r1.<init>(r0)
            r6.n = r1
        Lf:
            r0 = r8
            android.widget.TextView r0 = (android.widget.TextView) r0
            r1 = 0
            if (r8 != 0) goto L26
            android.content.Context r8 = r9.getContext()
            android.view.LayoutInflater r8 = android.view.LayoutInflater.from(r8)
            int r0 = com.google.android.material.R.layout.mtrl_calendar_day
            android.view.View r8 = r8.inflate(r0, r9, r1)
            r0 = r8
            android.widget.TextView r0 = (android.widget.TextView) r0
        L26:
            int r8 = r6.b()
            int r8 = r7 - r8
            if (r8 < 0) goto Ld4
            com.google.android.material.datepicker.Month r9 = r6.k
            int r2 = r9.n
            if (r8 < r2) goto L36
            goto Ld4
        L36:
            r2 = 1
            int r8 = r8 + r2
            r0.setTag(r9)
            android.content.res.Resources r9 = r0.getResources()
            android.content.res.Configuration r9 = r9.getConfiguration()
            java.util.Locale r9 = r9.locale
            java.lang.Object[] r3 = new java.lang.Object[r2]
            java.lang.Integer r4 = java.lang.Integer.valueOf(r8)
            r3[r1] = r4
            java.lang.String r4 = "%d"
            java.lang.String r9 = java.lang.String.format(r9, r4, r3)
            r0.setText(r9)
            com.google.android.material.datepicker.Month r9 = r6.k
            long r8 = r9.k(r8)
            com.google.android.material.datepicker.Month r3 = r6.k
            int r3 = r3.l
            com.google.android.material.datepicker.Month r4 = com.google.android.material.datepicker.Month.i()
            int r4 = r4.l
            r5 = 24
            if (r3 != r4) goto L9c
            java.util.Locale r3 = java.util.Locale.getDefault()
            int r4 = android.os.Build.VERSION.SDK_INT
            if (r4 < r5) goto L82
            java.lang.String r4 = "MMMEd"
            android.icu.text.DateFormat r3 = b.i.a.g.d.l.c(r4, r3)
            java.util.Date r4 = new java.util.Date
            r4.<init>(r8)
            java.lang.String r8 = r3.format(r4)
            goto L98
        L82:
            java.util.concurrent.atomic.AtomicReference<b.i.a.g.d.k> r4 = b.i.a.g.d.l.a
            java.text.DateFormat r3 = java.text.DateFormat.getDateInstance(r1, r3)
            java.util.TimeZone r4 = b.i.a.g.d.l.g()
            r3.setTimeZone(r4)
            java.util.Date r4 = new java.util.Date
            r4.<init>(r8)
            java.lang.String r8 = r3.format(r4)
        L98:
            r0.setContentDescription(r8)
            goto Lcd
        L9c:
            java.util.Locale r3 = java.util.Locale.getDefault()
            int r4 = android.os.Build.VERSION.SDK_INT
            if (r4 < r5) goto Lb4
            java.lang.String r4 = "yMMMEd"
            android.icu.text.DateFormat r3 = b.i.a.g.d.l.c(r4, r3)
            java.util.Date r4 = new java.util.Date
            r4.<init>(r8)
            java.lang.String r8 = r3.format(r4)
            goto Lca
        Lb4:
            java.util.concurrent.atomic.AtomicReference<b.i.a.g.d.k> r4 = b.i.a.g.d.l.a
            java.text.DateFormat r3 = java.text.DateFormat.getDateInstance(r1, r3)
            java.util.TimeZone r4 = b.i.a.g.d.l.g()
            r3.setTimeZone(r4)
            java.util.Date r4 = new java.util.Date
            r4.<init>(r8)
            java.lang.String r8 = r3.format(r4)
        Lca:
            r0.setContentDescription(r8)
        Lcd:
            r0.setVisibility(r1)
            r0.setEnabled(r2)
            goto Ldc
        Ld4:
            r8 = 8
            r0.setVisibility(r8)
            r0.setEnabled(r1)
        Ldc:
            java.lang.Long r7 = r6.getItem(r7)
            if (r7 != 0) goto Le3
            goto Lea
        Le3:
            long r7 = r7.longValue()
            r6.e(r0, r7)
        Lea:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.g.d.g.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
    }

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public boolean hasStableIds() {
        return true;
    }
}
