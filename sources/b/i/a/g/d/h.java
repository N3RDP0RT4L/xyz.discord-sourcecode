package b.i.a.g.d;

import android.view.View;
import android.widget.AdapterView;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.MaterialCalendar;
import com.google.android.material.datepicker.MaterialCalendarGridView;
import com.google.android.material.datepicker.MonthsPagerAdapter;
import com.google.android.material.datepicker.OnSelectionChangedListener;
import java.util.Iterator;
/* compiled from: MonthsPagerAdapter.java */
/* loaded from: classes3.dex */
public class h implements AdapterView.OnItemClickListener {
    public final /* synthetic */ MaterialCalendarGridView j;
    public final /* synthetic */ MonthsPagerAdapter k;

    public h(MonthsPagerAdapter monthsPagerAdapter, MaterialCalendarGridView materialCalendarGridView) {
        this.k = monthsPagerAdapter;
        this.j = materialCalendarGridView;
    }

    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        CalendarConstraints calendarConstraints;
        RecyclerView recyclerView;
        RecyclerView recyclerView2;
        g a = this.j.getAdapter2();
        if (i >= a.b() && i <= a.d()) {
            MaterialCalendar.l lVar = this.k.d;
            long longValue = this.j.getAdapter2().getItem(i).longValue();
            MaterialCalendar.d dVar = (MaterialCalendar.d) lVar;
            calendarConstraints = MaterialCalendar.this.calendarConstraints;
            if (calendarConstraints.getDateValidator().isValid(longValue)) {
                MaterialCalendar.this.dateSelector.select(longValue);
                Iterator it = MaterialCalendar.this.onSelectionChangedListeners.iterator();
                while (it.hasNext()) {
                    ((OnSelectionChangedListener) it.next()).onSelectionChanged(MaterialCalendar.this.dateSelector.getSelection());
                }
                MaterialCalendar.this.recyclerView.getAdapter().notifyDataSetChanged();
                recyclerView = MaterialCalendar.this.yearSelector;
                if (recyclerView != null) {
                    recyclerView2 = MaterialCalendar.this.yearSelector;
                    recyclerView2.getAdapter().notifyDataSetChanged();
                }
            }
        }
    }
}
