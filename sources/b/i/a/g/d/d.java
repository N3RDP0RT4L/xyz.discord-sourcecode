package b.i.a.g.d;

import b.i.a.f.e.o.f;
/* compiled from: DateFormatTextWatcher.java */
/* loaded from: classes3.dex */
public class d implements Runnable {
    public final /* synthetic */ long j;
    public final /* synthetic */ c k;

    public d(c cVar, long j) {
        this.k = cVar;
        this.j = j;
    }

    @Override // java.lang.Runnable
    public void run() {
        c cVar = this.k;
        cVar.j.setError(String.format(cVar.m, f.g0(this.j)));
        this.k.a();
    }
}
