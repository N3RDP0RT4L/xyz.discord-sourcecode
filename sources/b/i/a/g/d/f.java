package b.i.a.g.d;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.core.view.AccessibilityDelegateCompat;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import com.google.android.material.datepicker.MaterialCalendarGridView;
/* compiled from: MaterialCalendarGridView.java */
/* loaded from: classes3.dex */
public class f extends AccessibilityDelegateCompat {
    public f(MaterialCalendarGridView materialCalendarGridView) {
    }

    @Override // androidx.core.view.AccessibilityDelegateCompat
    public void onInitializeAccessibilityNodeInfo(View view, @NonNull AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        super.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat);
        accessibilityNodeInfoCompat.setCollectionInfo(null);
    }
}
