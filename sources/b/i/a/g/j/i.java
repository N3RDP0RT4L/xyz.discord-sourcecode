package b.i.a.g.j;

import android.animation.ValueAnimator;
import androidx.annotation.NonNull;
/* compiled from: DropdownMenuEndIconDelegate.java */
/* loaded from: classes3.dex */
public class i implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ h a;

    public i(h hVar) {
        this.a = hVar;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(@NonNull ValueAnimator valueAnimator) {
        this.a.c.setAlpha(((Float) valueAnimator.getAnimatedValue()).floatValue());
    }
}
