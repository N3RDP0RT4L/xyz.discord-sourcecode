package b.i.a.g.j;

import android.animation.AnimatorSet;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import com.google.android.material.R;
import com.google.android.material.animation.AnimationUtils;
import com.google.android.material.textfield.TextInputLayout;
/* compiled from: ClearTextEndIconDelegate.java */
/* loaded from: classes3.dex */
public class a extends m {
    public final TextWatcher d = new C0124a();
    public final View.OnFocusChangeListener e = new b();
    public final TextInputLayout.OnEditTextAttachedListener f = new c();
    public final TextInputLayout.OnEndIconChangedListener g = new d();
    public AnimatorSet h;
    public ValueAnimator i;

    /* compiled from: ClearTextEndIconDelegate.java */
    /* renamed from: b.i.a.g.j.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public class C0124a implements TextWatcher {
        public C0124a() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(@NonNull Editable editable) {
            if (a.this.a.getSuffixText() == null) {
                a.this.d(editable.length() > 0);
            }
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    /* compiled from: ClearTextEndIconDelegate.java */
    /* loaded from: classes3.dex */
    public class b implements View.OnFocusChangeListener {
        public b() {
        }

        @Override // android.view.View.OnFocusChangeListener
        public void onFocusChange(View view, boolean z2) {
            boolean z3 = true;
            boolean z4 = !TextUtils.isEmpty(((EditText) view).getText());
            a aVar = a.this;
            if (!z4 || !z2) {
                z3 = false;
            }
            aVar.d(z3);
        }
    }

    /* compiled from: ClearTextEndIconDelegate.java */
    /* loaded from: classes3.dex */
    public class c implements TextInputLayout.OnEditTextAttachedListener {
        public c() {
        }

        /* JADX WARN: Code restructure failed: missing block: B:8:0x0019, code lost:
            if ((r0.getText().length() > 0) != false) goto L10;
         */
        @Override // com.google.android.material.textfield.TextInputLayout.OnEditTextAttachedListener
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public void onEditTextAttached(@androidx.annotation.NonNull com.google.android.material.textfield.TextInputLayout r5) {
            /*
                r4 = this;
                android.widget.EditText r0 = r5.getEditText()
                boolean r1 = r0.hasFocus()
                r2 = 1
                r3 = 0
                if (r1 == 0) goto L1c
                android.text.Editable r1 = r0.getText()
                int r1 = r1.length()
                if (r1 <= 0) goto L18
                r1 = 1
                goto L19
            L18:
                r1 = 0
            L19:
                if (r1 == 0) goto L1c
                goto L1d
            L1c:
                r2 = 0
            L1d:
                r5.setEndIconVisible(r2)
                r5.setEndIconCheckable(r3)
                b.i.a.g.j.a r5 = b.i.a.g.j.a.this
                android.view.View$OnFocusChangeListener r5 = r5.e
                r0.setOnFocusChangeListener(r5)
                b.i.a.g.j.a r5 = b.i.a.g.j.a.this
                android.text.TextWatcher r5 = r5.d
                r0.removeTextChangedListener(r5)
                b.i.a.g.j.a r5 = b.i.a.g.j.a.this
                android.text.TextWatcher r5 = r5.d
                r0.addTextChangedListener(r5)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.i.a.g.j.a.c.onEditTextAttached(com.google.android.material.textfield.TextInputLayout):void");
        }
    }

    /* compiled from: ClearTextEndIconDelegate.java */
    /* loaded from: classes3.dex */
    public class d implements TextInputLayout.OnEndIconChangedListener {

        /* compiled from: ClearTextEndIconDelegate.java */
        /* renamed from: b.i.a.g.j.a$d$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public class RunnableC0125a implements Runnable {
            public final /* synthetic */ EditText j;

            public RunnableC0125a(EditText editText) {
                this.j = editText;
            }

            @Override // java.lang.Runnable
            public void run() {
                this.j.removeTextChangedListener(a.this.d);
            }
        }

        public d() {
        }

        @Override // com.google.android.material.textfield.TextInputLayout.OnEndIconChangedListener
        public void onEndIconChanged(@NonNull TextInputLayout textInputLayout, int i) {
            EditText editText = textInputLayout.getEditText();
            if (editText != null && i == 2) {
                editText.post(new RunnableC0125a(editText));
                if (editText.getOnFocusChangeListener() == a.this.e) {
                    editText.setOnFocusChangeListener(null);
                }
            }
        }
    }

    /* compiled from: ClearTextEndIconDelegate.java */
    /* loaded from: classes3.dex */
    public class e implements View.OnClickListener {
        public e() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            Editable text = a.this.a.getEditText().getText();
            if (text != null) {
                text.clear();
            }
            a.this.a.refreshEndIconDrawableState();
        }
    }

    public a(@NonNull TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    @Override // b.i.a.g.j.m
    public void a() {
        this.a.setEndIconDrawable(AppCompatResources.getDrawable(this.f1622b, R.drawable.mtrl_ic_cancel));
        TextInputLayout textInputLayout = this.a;
        textInputLayout.setEndIconContentDescription(textInputLayout.getResources().getText(R.string.clear_text_end_icon_content_description));
        this.a.setEndIconOnClickListener(new e());
        this.a.addOnEditTextAttachedListener(this.f);
        this.a.addOnEndIconChangedListener(this.g);
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.8f, 1.0f);
        ofFloat.setInterpolator(AnimationUtils.LINEAR_OUT_SLOW_IN_INTERPOLATOR);
        ofFloat.setDuration(150L);
        ofFloat.addUpdateListener(new b.i.a.g.j.e(this));
        ValueAnimator ofFloat2 = ValueAnimator.ofFloat(0.0f, 1.0f);
        TimeInterpolator timeInterpolator = AnimationUtils.LINEAR_INTERPOLATOR;
        ofFloat2.setInterpolator(timeInterpolator);
        ofFloat2.setDuration(100L);
        ofFloat2.addUpdateListener(new b.i.a.g.j.d(this));
        AnimatorSet animatorSet = new AnimatorSet();
        this.h = animatorSet;
        animatorSet.playTogether(ofFloat, ofFloat2);
        this.h.addListener(new b.i.a.g.j.b(this));
        ValueAnimator ofFloat3 = ValueAnimator.ofFloat(1.0f, 0.0f);
        ofFloat3.setInterpolator(timeInterpolator);
        ofFloat3.setDuration(100L);
        ofFloat3.addUpdateListener(new b.i.a.g.j.d(this));
        this.i = ofFloat3;
        ofFloat3.addListener(new b.i.a.g.j.c(this));
    }

    @Override // b.i.a.g.j.m
    public void c(boolean z2) {
        if (this.a.getSuffixText() != null) {
            d(z2);
        }
    }

    public final void d(boolean z2) {
        boolean z3 = this.a.isEndIconVisible() == z2;
        if (z2 && !this.h.isRunning()) {
            this.i.cancel();
            this.h.start();
            if (z3) {
                this.h.end();
            }
        } else if (!z2) {
            this.h.cancel();
            this.i.start();
            if (z3) {
                this.i.end();
            }
        }
    }
}
