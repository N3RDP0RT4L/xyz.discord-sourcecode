package b.i.a.g.j;

import android.animation.ValueAnimator;
import androidx.annotation.NonNull;
/* compiled from: ClearTextEndIconDelegate.java */
/* loaded from: classes3.dex */
public class d implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ a a;

    public d(a aVar) {
        this.a = aVar;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(@NonNull ValueAnimator valueAnimator) {
        this.a.c.setAlpha(((Float) valueAnimator.getAnimatedValue()).floatValue());
    }
}
