package b.i.a.g.j;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.ColorInt;
import androidx.annotation.DimenRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import com.google.android.material.R;
import com.google.android.material.animation.AnimationUtils;
import com.google.android.material.animation.AnimatorSetCompat;
import com.google.android.material.resources.MaterialResources;
import com.google.android.material.textfield.TextInputLayout;
import java.util.ArrayList;
import java.util.List;
/* compiled from: IndicatorViewController.java */
/* loaded from: classes3.dex */
public final class n {
    public final Context a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextInputLayout f1623b;
    public LinearLayout c;
    public int d;
    public FrameLayout e;
    @Nullable
    public Animator f;
    public final float g;
    public int h;
    public int i;
    @Nullable
    public CharSequence j;
    public boolean k;
    @Nullable
    public TextView l;
    @Nullable
    public CharSequence m;
    public int n;
    @Nullable
    public ColorStateList o;
    public CharSequence p;
    public boolean q;
    @Nullable
    public TextView r;

    /* renamed from: s  reason: collision with root package name */
    public int f1624s;
    @Nullable
    public ColorStateList t;
    public Typeface u;

    /* compiled from: IndicatorViewController.java */
    /* loaded from: classes3.dex */
    public class a extends AnimatorListenerAdapter {
        public final /* synthetic */ int a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ TextView f1625b;
        public final /* synthetic */ int c;
        public final /* synthetic */ TextView d;

        public a(int i, TextView textView, int i2, TextView textView2) {
            this.a = i;
            this.f1625b = textView;
            this.c = i2;
            this.d = textView2;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            TextView textView;
            n nVar = n.this;
            nVar.h = this.a;
            nVar.f = null;
            TextView textView2 = this.f1625b;
            if (textView2 != null) {
                textView2.setVisibility(4);
                if (this.c == 1 && (textView = n.this.l) != null) {
                    textView.setText((CharSequence) null);
                }
            }
            TextView textView3 = this.d;
            if (textView3 != null) {
                textView3.setTranslationY(0.0f);
                this.d.setAlpha(1.0f);
            }
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            TextView textView = this.d;
            if (textView != null) {
                textView.setVisibility(0);
            }
        }
    }

    public n(@NonNull TextInputLayout textInputLayout) {
        Context context = textInputLayout.getContext();
        this.a = context;
        this.f1623b = textInputLayout;
        this.g = context.getResources().getDimensionPixelSize(R.dimen.design_textinput_caption_translate_y);
    }

    public void a(TextView textView, int i) {
        if (this.c == null && this.e == null) {
            LinearLayout linearLayout = new LinearLayout(this.a);
            this.c = linearLayout;
            linearLayout.setOrientation(0);
            this.f1623b.addView(this.c, -1, -2);
            this.e = new FrameLayout(this.a);
            this.c.addView(this.e, new LinearLayout.LayoutParams(0, -2, 1.0f));
            if (this.f1623b.getEditText() != null) {
                b();
            }
        }
        if (i == 0 || i == 1) {
            this.e.setVisibility(0);
            this.e.addView(textView);
        } else {
            this.c.addView(textView, new LinearLayout.LayoutParams(-2, -2));
        }
        this.c.setVisibility(0);
        this.d++;
    }

    public void b() {
        if ((this.c == null || this.f1623b.getEditText() == null) ? false : true) {
            EditText editText = this.f1623b.getEditText();
            boolean isFontScaleAtLeast1_3 = MaterialResources.isFontScaleAtLeast1_3(this.a);
            LinearLayout linearLayout = this.c;
            int i = R.dimen.material_helper_text_font_1_3_padding_horizontal;
            ViewCompat.setPaddingRelative(linearLayout, h(isFontScaleAtLeast1_3, i, ViewCompat.getPaddingStart(editText)), h(isFontScaleAtLeast1_3, R.dimen.material_helper_text_font_1_3_padding_top, this.a.getResources().getDimensionPixelSize(R.dimen.material_helper_text_default_padding_top)), h(isFontScaleAtLeast1_3, i, ViewCompat.getPaddingEnd(editText)), 0);
        }
    }

    public void c() {
        Animator animator = this.f;
        if (animator != null) {
            animator.cancel();
        }
    }

    public final void d(@NonNull List<Animator> list, boolean z2, @Nullable TextView textView, int i, int i2, int i3) {
        if (textView != null && z2) {
            if (i == i3 || i == i2) {
                ObjectAnimator ofFloat = ObjectAnimator.ofFloat(textView, View.ALPHA, i3 == i ? 1.0f : 0.0f);
                ofFloat.setDuration(167L);
                ofFloat.setInterpolator(AnimationUtils.LINEAR_INTERPOLATOR);
                list.add(ofFloat);
                if (i3 == i) {
                    ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(textView, View.TRANSLATION_Y, -this.g, 0.0f);
                    ofFloat2.setDuration(217L);
                    ofFloat2.setInterpolator(AnimationUtils.LINEAR_OUT_SLOW_IN_INTERPOLATOR);
                    list.add(ofFloat2);
                }
            }
        }
    }

    public boolean e() {
        return this.i == 1 && this.l != null && !TextUtils.isEmpty(this.j);
    }

    @Nullable
    public final TextView f(int i) {
        if (i == 1) {
            return this.l;
        }
        if (i != 2) {
            return null;
        }
        return this.r;
    }

    @ColorInt
    public int g() {
        TextView textView = this.l;
        if (textView != null) {
            return textView.getCurrentTextColor();
        }
        return -1;
    }

    public final int h(boolean z2, @DimenRes int i, int i2) {
        return z2 ? this.a.getResources().getDimensionPixelSize(i) : i2;
    }

    public void i() {
        this.j = null;
        c();
        if (this.h == 1) {
            if (!this.q || TextUtils.isEmpty(this.p)) {
                this.i = 0;
            } else {
                this.i = 2;
            }
        }
        l(this.h, this.i, k(this.l, null));
    }

    public void j(TextView textView, int i) {
        FrameLayout frameLayout;
        LinearLayout linearLayout = this.c;
        if (linearLayout != null) {
            if (!(i == 0 || i == 1) || (frameLayout = this.e) == null) {
                linearLayout.removeView(textView);
            } else {
                frameLayout.removeView(textView);
            }
            int i2 = this.d - 1;
            this.d = i2;
            LinearLayout linearLayout2 = this.c;
            if (i2 == 0) {
                linearLayout2.setVisibility(8);
            }
        }
    }

    public final boolean k(@Nullable TextView textView, @Nullable CharSequence charSequence) {
        return ViewCompat.isLaidOut(this.f1623b) && this.f1623b.isEnabled() && (this.i != this.h || textView == null || !TextUtils.equals(textView.getText(), charSequence));
    }

    public final void l(int i, int i2, boolean z2) {
        TextView f;
        TextView f2;
        if (i != i2) {
            if (z2) {
                AnimatorSet animatorSet = new AnimatorSet();
                this.f = animatorSet;
                ArrayList arrayList = new ArrayList();
                d(arrayList, this.q, this.r, 2, i, i2);
                d(arrayList, this.k, this.l, 1, i, i2);
                AnimatorSetCompat.playTogether(animatorSet, arrayList);
                animatorSet.addListener(new a(i2, f(i), i, f(i2)));
                animatorSet.start();
            } else if (i != i2) {
                if (!(i2 == 0 || (f2 = f(i2)) == null)) {
                    f2.setVisibility(0);
                    f2.setAlpha(1.0f);
                }
                if (!(i == 0 || (f = f(i)) == null)) {
                    f.setVisibility(4);
                    if (i == 1) {
                        f.setText((CharSequence) null);
                    }
                }
                this.h = i2;
            }
            this.f1623b.updateEditTextBackground();
            this.f1623b.updateLabelState(z2);
            this.f1623b.updateTextInputBoxState();
        }
    }
}
