package b.i.a.g.j;

import androidx.annotation.NonNull;
import com.google.android.material.textfield.TextInputLayout;
/* compiled from: CustomEndIconDelegate.java */
/* loaded from: classes3.dex */
public class f extends m {
    public f(@NonNull TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    @Override // b.i.a.g.j.m
    public void a() {
        this.a.setEndIconOnClickListener(null);
        this.a.setEndIconOnLongClickListener(null);
    }
}
