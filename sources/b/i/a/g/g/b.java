package b.i.a.g.g;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
/* compiled from: CircularIndeterminateAnimatorDelegate.java */
/* loaded from: classes3.dex */
public class b extends AnimatorListenerAdapter {
    public final /* synthetic */ d a;

    public b(d dVar) {
        this.a = dVar;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationRepeat(Animator animator) {
        super.onAnimationRepeat(animator);
        d dVar = this.a;
        dVar.m = (dVar.m + 4) % dVar.l.indicatorColors.length;
    }
}
