package b.i.a.g.g;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
/* compiled from: CircularIndeterminateAnimatorDelegate.java */
/* loaded from: classes3.dex */
public class c extends AnimatorListenerAdapter {
    public final /* synthetic */ d a;

    public c(d dVar) {
        this.a = dVar;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        super.onAnimationEnd(animator);
        this.a.a();
        d dVar = this.a;
        dVar.p.onAnimationEnd(dVar.a);
    }
}
