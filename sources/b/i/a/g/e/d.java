package b.i.a.g.e;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import b.i.a.g.e.f;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
/* compiled from: FloatingActionButtonImpl.java */
/* loaded from: classes3.dex */
public class d extends AnimatorListenerAdapter {
    public boolean a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ boolean f1609b;
    public final /* synthetic */ f.AbstractC0121f c;
    public final /* synthetic */ f d;

    public d(f fVar, boolean z2, f.AbstractC0121f fVar2) {
        this.d = fVar;
        this.f1609b = z2;
        this.c = fVar2;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        this.a = true;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        f fVar = this.d;
        fVar.B = 0;
        fVar.v = null;
        if (!this.a) {
            FloatingActionButton floatingActionButton = fVar.F;
            boolean z2 = this.f1609b;
            floatingActionButton.internalSetVisibility(z2 ? 8 : 4, z2);
            f.AbstractC0121f fVar2 = this.c;
            if (fVar2 != null) {
                FloatingActionButton.a aVar = (FloatingActionButton.a) fVar2;
                aVar.a.onHidden(FloatingActionButton.this);
            }
        }
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        this.d.F.internalSetVisibility(0, this.f1609b);
        f fVar = this.d;
        fVar.B = 1;
        fVar.v = animator;
        this.a = false;
    }
}
