package b.i.a.g.e;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import b.i.a.g.e.f;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
/* compiled from: FloatingActionButtonImpl.java */
/* loaded from: classes3.dex */
public class e extends AnimatorListenerAdapter {
    public final /* synthetic */ boolean a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ f.AbstractC0121f f1610b;
    public final /* synthetic */ f c;

    public e(f fVar, boolean z2, f.AbstractC0121f fVar2) {
        this.c = fVar;
        this.a = z2;
        this.f1610b = fVar2;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        f fVar = this.c;
        fVar.B = 0;
        fVar.v = null;
        f.AbstractC0121f fVar2 = this.f1610b;
        if (fVar2 != null) {
            FloatingActionButton.a aVar = (FloatingActionButton.a) fVar2;
            aVar.a.onShown(FloatingActionButton.this);
        }
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        this.c.F.internalSetVisibility(0, this.a);
        f fVar = this.c;
        fVar.B = 2;
        fVar.v = animator;
    }
}
