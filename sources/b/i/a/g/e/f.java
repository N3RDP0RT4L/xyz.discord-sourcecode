package b.i.a.g.e;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.res.ColorStateList;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Build;
import android.view.View;
import android.view.ViewTreeObserver;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Preconditions;
import androidx.core.view.ViewCompat;
import com.google.android.material.animation.AnimationUtils;
import com.google.android.material.animation.AnimatorSetCompat;
import com.google.android.material.animation.ImageMatrixProperty;
import com.google.android.material.animation.MatrixEvaluator;
import com.google.android.material.animation.MotionSpec;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.internal.StateListAnimator;
import com.google.android.material.shadow.ShadowViewDelegate;
import com.google.android.material.shape.MaterialShapeDrawable;
import com.google.android.material.shape.ShapeAppearanceModel;
import com.google.android.material.shape.Shapeable;
import java.util.ArrayList;
import java.util.Iterator;
/* compiled from: FloatingActionButtonImpl.java */
/* loaded from: classes3.dex */
public class f {
    public static final TimeInterpolator a = AnimationUtils.FAST_OUT_LINEAR_IN_INTERPOLATOR;

    /* renamed from: b  reason: collision with root package name */
    public static final int[] f1611b = {16842919, 16842910};
    public static final int[] c = {16843623, 16842908, 16842910};
    public static final int[] d = {16842908, 16842910};
    public static final int[] e = {16843623, 16842910};
    public static final int[] f = {16842910};
    public static final int[] g = new int[0];
    public int A;
    public ArrayList<Animator.AnimatorListener> C;
    public ArrayList<Animator.AnimatorListener> D;
    public ArrayList<e> E;
    public final FloatingActionButton F;
    public final ShadowViewDelegate G;
    @Nullable
    public ViewTreeObserver.OnPreDrawListener L;
    @Nullable
    public ShapeAppearanceModel h;
    @Nullable
    public MaterialShapeDrawable i;
    @Nullable
    public Drawable j;
    @Nullable
    public b.i.a.g.e.c k;
    @Nullable
    public Drawable l;
    public boolean m;
    public float o;
    public float p;
    public float q;
    public int r;
    @NonNull

    /* renamed from: s  reason: collision with root package name */
    public final StateListAnimator f1612s;
    @Nullable
    public MotionSpec t;
    @Nullable
    public MotionSpec u;
    @Nullable
    public Animator v;
    @Nullable
    public MotionSpec w;
    @Nullable

    /* renamed from: x  reason: collision with root package name */
    public MotionSpec f1613x;

    /* renamed from: y  reason: collision with root package name */
    public float f1614y;
    public boolean n = true;

    /* renamed from: z  reason: collision with root package name */
    public float f1615z = 1.0f;
    public int B = 0;
    public final Rect H = new Rect();
    public final RectF I = new RectF();
    public final RectF J = new RectF();
    public final Matrix K = new Matrix();

    /* compiled from: FloatingActionButtonImpl.java */
    /* loaded from: classes3.dex */
    public class b extends h {
        public b(f fVar) {
            super(null);
        }

        @Override // b.i.a.g.e.f.h
        public float a() {
            return 0.0f;
        }
    }

    /* compiled from: FloatingActionButtonImpl.java */
    /* loaded from: classes3.dex */
    public class c extends h {
        public c() {
            super(null);
        }

        @Override // b.i.a.g.e.f.h
        public float a() {
            f fVar = f.this;
            return fVar.o + fVar.p;
        }
    }

    /* compiled from: FloatingActionButtonImpl.java */
    /* loaded from: classes3.dex */
    public class d extends h {
        public d() {
            super(null);
        }

        @Override // b.i.a.g.e.f.h
        public float a() {
            f fVar = f.this;
            return fVar.o + fVar.q;
        }
    }

    /* compiled from: FloatingActionButtonImpl.java */
    /* loaded from: classes3.dex */
    public interface e {
        void a();

        void b();
    }

    /* compiled from: FloatingActionButtonImpl.java */
    /* renamed from: b.i.a.g.e.f$f  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public interface AbstractC0121f {
    }

    /* compiled from: FloatingActionButtonImpl.java */
    /* loaded from: classes3.dex */
    public class g extends h {
        public g() {
            super(null);
        }

        @Override // b.i.a.g.e.f.h
        public float a() {
            return f.this.o;
        }
    }

    /* compiled from: FloatingActionButtonImpl.java */
    /* loaded from: classes3.dex */
    public abstract class h extends AnimatorListenerAdapter implements ValueAnimator.AnimatorUpdateListener {
        public boolean a;

        /* renamed from: b  reason: collision with root package name */
        public float f1616b;
        public float c;

        public h(b.i.a.g.e.d dVar) {
        }

        public abstract float a();

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            f.this.w((int) this.c);
            this.a = false;
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(@NonNull ValueAnimator valueAnimator) {
            if (!this.a) {
                MaterialShapeDrawable materialShapeDrawable = f.this.i;
                this.f1616b = materialShapeDrawable == null ? 0.0f : materialShapeDrawable.getElevation();
                this.c = a();
                this.a = true;
            }
            f fVar = f.this;
            float f = this.f1616b;
            fVar.w((int) ((valueAnimator.getAnimatedFraction() * (this.c - f)) + f));
        }
    }

    public f(FloatingActionButton floatingActionButton, ShadowViewDelegate shadowViewDelegate) {
        this.F = floatingActionButton;
        this.G = shadowViewDelegate;
        StateListAnimator stateListAnimator = new StateListAnimator();
        this.f1612s = stateListAnimator;
        stateListAnimator.addState(f1611b, c(new d()));
        stateListAnimator.addState(c, c(new c()));
        stateListAnimator.addState(d, c(new c()));
        stateListAnimator.addState(e, c(new c()));
        stateListAnimator.addState(f, c(new g()));
        stateListAnimator.addState(g, c(new b(this)));
        this.f1614y = floatingActionButton.getRotation();
    }

    public final void a(float f2, @NonNull Matrix matrix) {
        matrix.reset();
        Drawable drawable = this.F.getDrawable();
        if (drawable != null && this.A != 0) {
            RectF rectF = this.I;
            RectF rectF2 = this.J;
            rectF.set(0.0f, 0.0f, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
            int i = this.A;
            rectF2.set(0.0f, 0.0f, i, i);
            matrix.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.CENTER);
            int i2 = this.A;
            matrix.postScale(f2, f2, i2 / 2.0f, i2 / 2.0f);
        }
    }

    @NonNull
    public final AnimatorSet b(@NonNull MotionSpec motionSpec, float f2, float f3, float f4) {
        ArrayList arrayList = new ArrayList();
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.F, View.ALPHA, f2);
        motionSpec.getTiming("opacity").apply(ofFloat);
        arrayList.add(ofFloat);
        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this.F, View.SCALE_X, f3);
        motionSpec.getTiming("scale").apply(ofFloat2);
        int i = Build.VERSION.SDK_INT;
        if (i == 26) {
            ofFloat2.setEvaluator(new b.i.a.g.e.g(this));
        }
        arrayList.add(ofFloat2);
        ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(this.F, View.SCALE_Y, f3);
        motionSpec.getTiming("scale").apply(ofFloat3);
        if (i == 26) {
            ofFloat3.setEvaluator(new b.i.a.g.e.g(this));
        }
        arrayList.add(ofFloat3);
        a(f4, this.K);
        ObjectAnimator ofObject = ObjectAnimator.ofObject(this.F, new ImageMatrixProperty(), new a(), new Matrix(this.K));
        motionSpec.getTiming("iconScale").apply(ofObject);
        arrayList.add(ofObject);
        AnimatorSet animatorSet = new AnimatorSet();
        AnimatorSetCompat.playTogether(animatorSet, arrayList);
        return animatorSet;
    }

    @NonNull
    public final ValueAnimator c(@NonNull h hVar) {
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setInterpolator(a);
        valueAnimator.setDuration(100L);
        valueAnimator.addListener(hVar);
        valueAnimator.addUpdateListener(hVar);
        valueAnimator.setFloatValues(0.0f, 1.0f);
        return valueAnimator;
    }

    public float d() {
        throw null;
    }

    public void e(@NonNull Rect rect) {
        int sizeDimension = this.m ? (this.r - this.F.getSizeDimension()) / 2 : 0;
        float d2 = this.n ? d() + this.q : 0.0f;
        int max = Math.max(sizeDimension, (int) Math.ceil(d2));
        int max2 = Math.max(sizeDimension, (int) Math.ceil(d2 * 1.5f));
        rect.set(max, max2, max, max2);
    }

    public void f(ColorStateList colorStateList, @Nullable PorterDuff.Mode mode, ColorStateList colorStateList2, int i) {
        throw null;
    }

    public boolean g() {
        return this.F.getVisibility() == 0 ? this.B == 1 : this.B != 2;
    }

    public boolean h() {
        return this.F.getVisibility() != 0 ? this.B == 2 : this.B != 1;
    }

    public void i() {
        throw null;
    }

    public void j() {
        throw null;
    }

    public void k(int[] iArr) {
        throw null;
    }

    public void l(float f2, float f3, float f4) {
        throw null;
    }

    public void m() {
        ArrayList<e> arrayList = this.E;
        if (arrayList != null) {
            Iterator<e> it = arrayList.iterator();
            while (it.hasNext()) {
                it.next().b();
            }
        }
    }

    public void n() {
        ArrayList<e> arrayList = this.E;
        if (arrayList != null) {
            Iterator<e> it = arrayList.iterator();
            while (it.hasNext()) {
                it.next().a();
            }
        }
    }

    public final void o(float f2) {
        this.f1615z = f2;
        Matrix matrix = this.K;
        a(f2, matrix);
        this.F.setImageMatrix(matrix);
    }

    public void p(@Nullable ColorStateList colorStateList) {
        throw null;
    }

    public final void q(@NonNull ShapeAppearanceModel shapeAppearanceModel) {
        this.h = shapeAppearanceModel;
        MaterialShapeDrawable materialShapeDrawable = this.i;
        if (materialShapeDrawable != null) {
            materialShapeDrawable.setShapeAppearanceModel(shapeAppearanceModel);
        }
        Drawable drawable = this.j;
        if (drawable instanceof Shapeable) {
            ((Shapeable) drawable).setShapeAppearanceModel(shapeAppearanceModel);
        }
        b.i.a.g.e.c cVar = this.k;
        if (cVar != null) {
            cVar.o = shapeAppearanceModel;
            cVar.invalidateSelf();
        }
    }

    public boolean r() {
        throw null;
    }

    public final boolean s() {
        return ViewCompat.isLaidOut(this.F) && !this.F.isInEditMode();
    }

    public final boolean t() {
        return !this.m || this.F.getSizeDimension() >= this.r;
    }

    public void u() {
        throw null;
    }

    public final void v() {
        Rect rect = this.H;
        e(rect);
        Preconditions.checkNotNull(this.l, "Didn't initialize content background");
        if (r()) {
            this.G.setBackgroundDrawable(new InsetDrawable(this.l, rect.left, rect.top, rect.right, rect.bottom));
        } else {
            this.G.setBackgroundDrawable(this.l);
        }
        this.G.setShadowPadding(rect.left, rect.top, rect.right, rect.bottom);
    }

    public void w(float f2) {
        MaterialShapeDrawable materialShapeDrawable = this.i;
        if (materialShapeDrawable != null) {
            materialShapeDrawable.setElevation(f2);
        }
    }

    /* compiled from: FloatingActionButtonImpl.java */
    /* loaded from: classes3.dex */
    public class a extends MatrixEvaluator {
        public a() {
        }

        @Override // com.google.android.material.animation.MatrixEvaluator, android.animation.TypeEvaluator
        public Matrix evaluate(float f, @NonNull Matrix matrix, @NonNull Matrix matrix2) {
            f.this.f1615z = f;
            return super.evaluate(f, matrix, matrix2);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // com.google.android.material.animation.MatrixEvaluator
        public Matrix evaluate(float f, @NonNull Matrix matrix, @NonNull Matrix matrix2) {
            f.this.f1615z = f;
            return super.evaluate(f, matrix, matrix2);
        }
    }
}
