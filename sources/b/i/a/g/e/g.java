package b.i.a.g.e;

import android.animation.FloatEvaluator;
import android.animation.TypeEvaluator;
/* compiled from: FloatingActionButtonImpl.java */
/* loaded from: classes3.dex */
public class g implements TypeEvaluator<Float> {
    public FloatEvaluator a = new FloatEvaluator();

    public g(f fVar) {
    }

    @Override // android.animation.TypeEvaluator
    public Float evaluate(float f, Float f2, Float f3) {
        float floatValue = this.a.evaluate(f, (Number) f2, (Number) f3).floatValue();
        if (floatValue < 0.1f) {
            floatValue = 0.0f;
        }
        return Float.valueOf(floatValue);
    }
}
