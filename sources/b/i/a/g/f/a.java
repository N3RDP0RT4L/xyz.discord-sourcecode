package b.i.a.g.f;

import android.os.Build;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextDirectionHeuristic;
import android.text.TextDirectionHeuristics;
import android.text.TextPaint;
import android.text.TextUtils;
import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;
import androidx.core.util.Preconditions;
import java.lang.reflect.Constructor;
/* compiled from: StaticLayoutBuilderCompat.java */
@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
/* loaded from: classes3.dex */
public final class a {
    public static boolean a;
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public static Constructor<StaticLayout> f1617b;
    @Nullable
    public static Object c;
    public CharSequence d;
    public final TextPaint e;
    public final int f;
    public int g;
    public boolean k;
    public Layout.Alignment h = Layout.Alignment.ALIGN_NORMAL;
    public int i = Integer.MAX_VALUE;
    public boolean j = true;
    @Nullable
    public TextUtils.TruncateAt l = null;

    /* compiled from: StaticLayoutBuilderCompat.java */
    /* renamed from: b.i.a.g.f.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static class C0122a extends Exception {
        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public C0122a(java.lang.Throwable r3) {
            /*
                r2 = this;
                java.lang.String r0 = "Error thrown initializing StaticLayout "
                java.lang.StringBuilder r0 = b.d.b.a.a.R(r0)
                java.lang.String r1 = r3.getMessage()
                r0.append(r1)
                java.lang.String r0 = r0.toString()
                r2.<init>(r0, r3)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.i.a.g.f.a.C0122a.<init>(java.lang.Throwable):void");
        }
    }

    public a(CharSequence charSequence, TextPaint textPaint, int i) {
        this.d = charSequence;
        this.e = textPaint;
        this.f = i;
        this.g = charSequence.length();
    }

    public StaticLayout a() throws C0122a {
        if (this.d == null) {
            this.d = "";
        }
        int max = Math.max(0, this.f);
        CharSequence charSequence = this.d;
        if (this.i == 1) {
            charSequence = TextUtils.ellipsize(charSequence, this.e, max, this.l);
        }
        int min = Math.min(charSequence.length(), this.g);
        this.g = min;
        int i = Build.VERSION.SDK_INT;
        if (i >= 23) {
            if (this.k) {
                this.h = Layout.Alignment.ALIGN_OPPOSITE;
            }
            StaticLayout.Builder obtain = StaticLayout.Builder.obtain(charSequence, 0, min, this.e, max);
            obtain.setAlignment(this.h);
            obtain.setIncludePad(this.j);
            obtain.setTextDirection(this.k ? TextDirectionHeuristics.RTL : TextDirectionHeuristics.LTR);
            TextUtils.TruncateAt truncateAt = this.l;
            if (truncateAt != null) {
                obtain.setEllipsize(truncateAt);
            }
            obtain.setMaxLines(this.i);
            return obtain.build();
        }
        if (!a) {
            try {
                c = this.k && i >= 23 ? TextDirectionHeuristics.RTL : TextDirectionHeuristics.LTR;
                Class cls = Integer.TYPE;
                Class cls2 = Float.TYPE;
                Constructor<StaticLayout> declaredConstructor = StaticLayout.class.getDeclaredConstructor(CharSequence.class, cls, cls, TextPaint.class, cls, Layout.Alignment.class, TextDirectionHeuristic.class, cls2, cls2, Boolean.TYPE, TextUtils.TruncateAt.class, cls, cls);
                f1617b = declaredConstructor;
                declaredConstructor.setAccessible(true);
                a = true;
            } catch (Exception e) {
                throw new C0122a(e);
            }
        }
        try {
            return (StaticLayout) ((Constructor) Preconditions.checkNotNull(f1617b)).newInstance(charSequence, 0, Integer.valueOf(this.g), this.e, Integer.valueOf(max), this.h, Preconditions.checkNotNull(c), Float.valueOf(1.0f), Float.valueOf(0.0f), Boolean.valueOf(this.j), null, Integer.valueOf(max), Integer.valueOf(this.i));
        } catch (Exception e2) {
            throw new C0122a(e2);
        }
    }
}
