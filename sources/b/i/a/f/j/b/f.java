package b.i.a.f.j.b;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.internal.nearby.zzgs;
import com.google.android.gms.nearby.messages.Message;
/* loaded from: classes3.dex */
public final class f implements Parcelable.Creator<Message> {
    @Override // android.os.Parcelable.Creator
    public final Message createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        byte[] bArr = null;
        long j = 0;
        String str = null;
        String str2 = null;
        zzgs[] zzgsVarArr = null;
        int i = 0;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                bArr = d.N(parcel, readInt);
            } else if (c == 2) {
                str2 = d.R(parcel, readInt);
            } else if (c == 3) {
                str = d.R(parcel, readInt);
            } else if (c == 4) {
                zzgsVarArr = (zzgs[]) d.U(parcel, readInt, zzgs.CREATOR);
            } else if (c == 5) {
                j = d.H1(parcel, readInt);
            } else if (c != 1000) {
                d.d2(parcel, readInt);
            } else {
                i = d.G1(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        return new Message(i, bArr, str, str2, zzgsVarArr, j);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Message[] newArray(int i) {
        return new Message[i];
    }
}
