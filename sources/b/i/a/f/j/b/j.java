package b.i.a.f.j.b;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.nearby.messages.Strategy;
/* loaded from: classes3.dex */
public final class j implements Parcelable.Creator<Strategy> {
    @Override // android.os.Parcelable.Creator
    public final Strategy createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        boolean z2 = false;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c != 1000) {
                switch (c) {
                    case 1:
                        i2 = d.G1(parcel, readInt);
                        continue;
                    case 2:
                        i3 = d.G1(parcel, readInt);
                        continue;
                    case 3:
                        i4 = d.G1(parcel, readInt);
                        continue;
                    case 4:
                        z2 = d.E1(parcel, readInt);
                        continue;
                    case 5:
                        i5 = d.G1(parcel, readInt);
                        continue;
                    case 6:
                        i6 = d.G1(parcel, readInt);
                        continue;
                    case 7:
                        i7 = d.G1(parcel, readInt);
                        continue;
                    default:
                        d.d2(parcel, readInt);
                        continue;
                }
            } else {
                i = d.G1(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        return new Strategy(i, i2, i3, i4, z2, i5, i6, i7);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Strategy[] newArray(int i) {
        return new Strategy[i];
    }
}
