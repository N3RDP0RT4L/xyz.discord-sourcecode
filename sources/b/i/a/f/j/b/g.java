package b.i.a.f.j.b;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.internal.nearby.zzgp;
import com.google.android.gms.internal.nearby.zzgu;
import com.google.android.gms.nearby.messages.MessageFilter;
import com.google.android.gms.nearby.messages.internal.zzad;
import java.util.ArrayList;
/* loaded from: classes3.dex */
public final class g implements Parcelable.Creator<MessageFilter> {
    @Override // android.os.Parcelable.Creator
    public final MessageFilter createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        ArrayList arrayList = null;
        ArrayList arrayList2 = null;
        ArrayList arrayList3 = null;
        int i = 0;
        boolean z2 = false;
        int i2 = 0;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                arrayList = d.V(parcel, readInt, zzad.CREATOR);
            } else if (c == 2) {
                arrayList2 = d.V(parcel, readInt, zzgu.CREATOR);
            } else if (c == 3) {
                z2 = d.E1(parcel, readInt);
            } else if (c == 4) {
                arrayList3 = d.V(parcel, readInt, zzgp.CREATOR);
            } else if (c == 5) {
                i2 = d.G1(parcel, readInt);
            } else if (c != 1000) {
                d.d2(parcel, readInt);
            } else {
                i = d.G1(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        return new MessageFilter(i, arrayList, arrayList2, z2, arrayList3, i2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ MessageFilter[] newArray(int i) {
        return new MessageFilter[i];
    }
}
