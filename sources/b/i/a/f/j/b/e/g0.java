package b.i.a.f.j.b.e;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.nearby.messages.internal.ClientAppContext;
import com.google.android.gms.nearby.messages.internal.zzcb;
/* loaded from: classes3.dex */
public final class g0 implements Parcelable.Creator<zzcb> {
    @Override // android.os.Parcelable.Creator
    public final zzcb createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        IBinder iBinder = null;
        IBinder iBinder2 = null;
        String str = null;
        ClientAppContext clientAppContext = null;
        int i = 0;
        boolean z2 = false;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    i = d.G1(parcel, readInt);
                    break;
                case 2:
                    iBinder = d.F1(parcel, readInt);
                    break;
                case 3:
                    iBinder2 = d.F1(parcel, readInt);
                    break;
                case 4:
                    z2 = d.E1(parcel, readInt);
                    break;
                case 5:
                    str = d.R(parcel, readInt);
                    break;
                case 6:
                    clientAppContext = (ClientAppContext) d.Q(parcel, readInt, ClientAppContext.CREATOR);
                    break;
                default:
                    d.d2(parcel, readInt);
                    break;
            }
        }
        d.f0(parcel, m2);
        return new zzcb(i, iBinder, iBinder2, z2, str, clientAppContext);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzcb[] newArray(int i) {
        return new zzcb[i];
    }
}
