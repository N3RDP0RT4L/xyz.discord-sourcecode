package b.i.a.f.j.b.e;

import b.i.a.f.e.h.j.e;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;
/* loaded from: classes3.dex */
public final class t implements e<Status> {
    public final /* synthetic */ TaskCompletionSource a;

    public t(TaskCompletionSource taskCompletionSource) {
        this.a = taskCompletionSource;
    }

    @Override // b.i.a.f.e.h.j.e
    public final void a(Status status) {
        TaskCompletionSource taskCompletionSource = this.a;
        taskCompletionSource.a.t(new ApiException(status));
    }

    @Override // b.i.a.f.e.h.j.e
    public final void b(Object obj) {
        this.a.a.s(null);
    }
}
