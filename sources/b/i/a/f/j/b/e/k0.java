package b.i.a.f.j.b.e;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.internal.nearby.zzgs;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.internal.Update;
import com.google.android.gms.nearby.messages.internal.zza;
import com.google.android.gms.nearby.messages.internal.zze;
/* loaded from: classes3.dex */
public final class k0 implements Parcelable.Creator<Update> {
    @Override // android.os.Parcelable.Creator
    public final Update createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        Message message = null;
        zze zzeVar = null;
        zza zzaVar = null;
        zzgs zzgsVar = null;
        byte[] bArr = null;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    i = d.G1(parcel, readInt);
                    break;
                case 2:
                    i2 = d.G1(parcel, readInt);
                    break;
                case 3:
                    message = (Message) d.Q(parcel, readInt, Message.CREATOR);
                    break;
                case 4:
                    zzeVar = (zze) d.Q(parcel, readInt, zze.CREATOR);
                    break;
                case 5:
                    zzaVar = (zza) d.Q(parcel, readInt, zza.CREATOR);
                    break;
                case 6:
                    zzgsVar = (zzgs) d.Q(parcel, readInt, zzgs.CREATOR);
                    break;
                case 7:
                    bArr = d.N(parcel, readInt);
                    break;
                default:
                    d.d2(parcel, readInt);
                    break;
            }
        }
        d.f0(parcel, m2);
        return new Update(i, i2, message, zzeVar, zzaVar, zzgsVar, bArr);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Update[] newArray(int i) {
        return new Update[i];
    }
}
