package b.i.a.f.j.b.e;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.nearby.messages.internal.zze;
import com.google.android.material.shadow.ShadowDrawableWrapper;
/* loaded from: classes3.dex */
public final class m0 implements Parcelable.Creator<zze> {
    @Override // android.os.Parcelable.Creator
    public final zze createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        int i = 0;
        double d = ShadowDrawableWrapper.COS_45;
        int i2 = 0;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                i = d.G1(parcel, readInt);
            } else if (c == 2) {
                i2 = d.G1(parcel, readInt);
            } else if (c != 3) {
                d.d2(parcel, readInt);
            } else {
                d.x2(parcel, readInt, 8);
                d = parcel.readDouble();
            }
        }
        d.f0(parcel, m2);
        return new zze(i, i2, d);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zze[] newArray(int i) {
        return new zze[i];
    }
}
