package b.i.a.f.j.b.e;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.nearby.messages.internal.ClientAppContext;
/* loaded from: classes3.dex */
public final class l0 implements Parcelable.Creator<ClientAppContext> {
    @Override // android.os.Parcelable.Creator
    public final ClientAppContext createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        int i = 0;
        boolean z2 = false;
        int i2 = 0;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    i = d.G1(parcel, readInt);
                    break;
                case 2:
                    str = d.R(parcel, readInt);
                    break;
                case 3:
                    str2 = d.R(parcel, readInt);
                    break;
                case 4:
                    z2 = d.E1(parcel, readInt);
                    break;
                case 5:
                    i2 = d.G1(parcel, readInt);
                    break;
                case 6:
                    str3 = d.R(parcel, readInt);
                    break;
                default:
                    d.d2(parcel, readInt);
                    break;
            }
        }
        d.f0(parcel, m2);
        return new ClientAppContext(i, str, str2, z2, i2, str3);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ClientAppContext[] newArray(int i) {
        return new ClientAppContext[i];
    }
}
