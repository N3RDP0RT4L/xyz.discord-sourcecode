package b.i.a.f.j.b.e;

import android.os.Parcel;
import android.os.RemoteException;
import b.i.a.f.h.m.b;
import b.i.a.f.h.m.c;
import b.i.a.f.h.m.h;
import b.i.a.f.h.m.i;
import com.google.android.gms.nearby.messages.internal.Update;
import com.google.android.gms.nearby.messages.internal.zzaf;
/* loaded from: classes3.dex */
public abstract class p0 extends b implements o0 {
    public p0() {
        super("com.google.android.gms.nearby.messages.internal.IMessageListener");
    }

    @Override // b.i.a.f.h.m.b
    public final boolean c(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1 || i == 2) {
            zzaf zzafVar = (zzaf) c.a(parcel, zzaf.CREATOR);
        } else if (i != 4) {
            return false;
        } else {
            ((h) this).a.a(new i(parcel.createTypedArrayList(Update.CREATOR)));
        }
        return true;
    }
}
