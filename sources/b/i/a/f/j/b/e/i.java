package b.i.a.f.j.b.e;

import android.app.Activity;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.f.e.h.a;
import b.i.a.f.e.h.b;
import b.i.a.f.e.h.j.c0;
import b.i.a.f.e.h.j.d0;
import b.i.a.f.e.h.j.g;
import b.i.a.f.e.h.j.k;
import b.i.a.f.e.h.j.n0;
import b.i.a.f.e.k.c;
import b.i.a.f.h.m.h;
import b.i.a.f.h.m.j;
import b.i.a.f.h.m.o;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.MessageListener;
import com.google.android.gms.nearby.messages.MessagesClient;
import com.google.android.gms.nearby.messages.PublishOptions;
import com.google.android.gms.nearby.messages.SubscribeOptions;
import com.google.android.gms.nearby.messages.internal.SubscribeRequest;
import com.google.android.gms.nearby.messages.internal.zzaf;
import com.google.android.gms.nearby.messages.internal.zzbz;
import com.google.android.gms.nearby.messages.internal.zzce;
import com.google.android.gms.nearby.messages.internal.zzcg;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.lang.ref.WeakReference;
import java.util.Objects;
/* loaded from: classes3.dex */
public final class i extends MessagesClient {
    public static final a.g<f> j;
    public static final a.AbstractC0111a<f, b.i.a.f.j.b.a> k;
    public static final a<b.i.a.f.j.b.a> l;
    public final int m = 1;

    static {
        a.g<f> gVar = new a.g<>();
        j = gVar;
        q qVar = new q();
        k = qVar;
        l = new a<>("Nearby.MESSAGES_API", qVar, gVar);
    }

    public i(Activity activity, @Nullable b.i.a.f.j.b.a aVar) {
        super(activity, l, aVar, b.a.a);
        activity.getApplication().registerActivityLifecycleCallbacks(new z(activity, this, null));
    }

    public static k j(i iVar, TaskCompletionSource taskCompletionSource) {
        Objects.requireNonNull(iVar);
        t tVar = new t(taskCompletionSource);
        String name = Status.class.getName();
        Looper looper = iVar.e;
        d.z(tVar, "Listener must not be null");
        d.z(looper, "Looper must not be null");
        d.z(name, "Listener type must not be null");
        return new k(looper, tVar, name);
    }

    @Override // b.i.a.f.e.h.b
    public final c.a a() {
        return super.a();
    }

    @Override // com.google.android.gms.nearby.messages.MessagesClient
    public final Task<Void> f(final Message message, final PublishOptions publishOptions) {
        k m = m(message);
        final r rVar = new r(this, m(publishOptions.f2989b), m);
        return k(m, new a0(this, message, rVar, publishOptions) { // from class: b.i.a.f.j.b.e.j
            public final i a;

            /* renamed from: b  reason: collision with root package name */
            public final Message f1580b;
            public final b0 c;
            public final PublishOptions d;

            {
                this.a = this;
                this.f1580b = message;
                this.c = rVar;
                this.d = publishOptions;
            }

            @Override // b.i.a.f.j.b.e.a0
            public final void a(f fVar, k kVar) {
                i iVar = this.a;
                Message message2 = this.f1580b;
                b0 b0Var = this.c;
                PublishOptions publishOptions2 = this.d;
                Objects.requireNonNull(iVar);
                zzaf zzafVar = new zzaf(1, message2);
                int i = iVar.m;
                Objects.requireNonNull(fVar);
                ((u0) fVar.w()).n(new zzbz(2, zzafVar, publishOptions2.a, new b.i.a.f.h.m.j(kVar), null, null, false, b0Var, false, null, i));
            }
        }, new a0(message) { // from class: b.i.a.f.j.b.e.k
            public final Message a;

            {
                this.a = message;
            }

            @Override // b.i.a.f.j.b.e.a0
            public final void a(f fVar, b.i.a.f.e.h.j.k kVar) {
                zzaf zzafVar = new zzaf(1, this.a);
                Objects.requireNonNull(fVar);
                ((u0) fVar.w()).P(new zzce(1, zzafVar, new j(kVar), null, null, false, null));
            }
        });
    }

    @Override // com.google.android.gms.nearby.messages.MessagesClient
    public final Task<Void> g(MessageListener messageListener, final SubscribeOptions subscribeOptions) {
        d.o(subscribeOptions.a.f2990s == 0, "Strategy.setBackgroundScanMode() is only supported by background subscribe (the version which takes a PendingIntent).");
        final k m = m(messageListener);
        final s sVar = new s(this, m(subscribeOptions.c), m);
        return k(m, new a0(this, m, sVar, subscribeOptions) { // from class: b.i.a.f.j.b.e.l
            public final i a;

            /* renamed from: b  reason: collision with root package name */
            public final k f1581b;
            public final d0 c;
            public final SubscribeOptions d;

            {
                this.a = this;
                this.f1581b = m;
                this.c = sVar;
                this.d = subscribeOptions;
            }

            @Override // b.i.a.f.j.b.e.a0
            public final void a(f fVar, k kVar) {
                i iVar = this.a;
                k kVar2 = this.f1581b;
                d0 d0Var = this.c;
                SubscribeOptions subscribeOptions2 = this.d;
                int i = iVar.m;
                if (!fVar.A.a(kVar2.c)) {
                    o<k.a, IBinder> oVar = fVar.A;
                    oVar.a.put(kVar2.c, new WeakReference<>(new h(kVar2)));
                }
                ((u0) fVar.w()).C(new SubscribeRequest(3, fVar.A.b(kVar2.c), subscribeOptions2.a, new j(kVar), subscribeOptions2.f2991b, null, 0, null, null, null, false, d0Var, false, null, subscribeOptions2.d, 0, i));
            }
        }, new a0(m) { // from class: b.i.a.f.j.b.e.m
            public final k a;

            {
                this.a = m;
            }

            @Override // b.i.a.f.j.b.e.a0
            public final void a(f fVar, k kVar) {
                k kVar2 = this.a;
                Objects.requireNonNull(fVar);
                j jVar = new j(kVar);
                if (!fVar.A.a(kVar2.c)) {
                    jVar.g(new Status(0, null));
                    return;
                }
                ((u0) fVar.w()).j0(new zzcg(1, fVar.A.b(kVar2.c), jVar, null, 0, null, null, false, null));
                o<k.a, IBinder> oVar = fVar.A;
                oVar.a.remove(kVar2.c);
            }
        });
    }

    @Override // com.google.android.gms.nearby.messages.MessagesClient
    public final Task<Void> h(Message message) {
        return l(message);
    }

    @Override // com.google.android.gms.nearby.messages.MessagesClient
    public final Task<Void> i(MessageListener messageListener) {
        return l(messageListener);
    }

    public final <T> Task<Void> k(k<T> kVar, a0 a0Var, a0 a0Var2) {
        v vVar = new v(this, kVar, a0Var);
        k.a<T> aVar = kVar.c;
        x xVar = new x(this, aVar, a0Var2);
        d.z(kVar.c, "Listener has already been released.");
        d.z(aVar, "Listener has already been released.");
        d.o(d.h0(kVar.c, aVar), "Listener registration and unregistration methods must be constructed with the same ListenerHolder.");
        g gVar = this.i;
        Runnable runnable = b.i.a.f.e.h.k.j;
        Objects.requireNonNull(gVar);
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        n0 n0Var = new n0(new d0(vVar, xVar, runnable), taskCompletionSource);
        Handler handler = gVar.f1355x;
        handler.sendMessage(handler.obtainMessage(8, new c0(n0Var, gVar.f1354s.get(), this)));
        return taskCompletionSource.a;
    }

    public final <T> Task<Void> l(T t) {
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        String name = t.getClass().getName();
        d.z(t, "Listener must not be null");
        d.z(name, "Listener type must not be null");
        d.v(name, "Listener type must not be empty");
        b(new k.a<>(t, name)).b(new u(taskCompletionSource));
        return taskCompletionSource.a;
    }

    public final <T> k<T> m(T t) {
        if (t == null) {
            return null;
        }
        String name = t.getClass().getName();
        Looper looper = this.e;
        d.z(t, "Listener must not be null");
        d.z(looper, "Looper must not be null");
        d.z(name, "Listener type must not be null");
        return new k<>(looper, t, name);
    }
}
