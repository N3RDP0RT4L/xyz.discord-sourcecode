package b.i.a.f.j.b.e;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.internal.zzaf;
/* loaded from: classes3.dex */
public final class e implements Parcelable.Creator<zzaf> {
    @Override // android.os.Parcelable.Creator
    public final zzaf createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        int i = 0;
        Message message = null;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                message = (Message) d.Q(parcel, readInt, Message.CREATOR);
            } else if (c != 1000) {
                d.d2(parcel, readInt);
            } else {
                i = d.G1(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        return new zzaf(i, message);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzaf[] newArray(int i) {
        return new zzaf[i];
    }
}
