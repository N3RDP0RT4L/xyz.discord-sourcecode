package b.i.a.f.j.b.e;

import android.os.Parcel;
import android.os.RemoteException;
import b.i.a.f.h.m.b;
import b.i.a.f.h.m.c;
import b.i.a.f.h.m.m;
import b.i.a.f.h.m.n;
/* loaded from: classes3.dex */
public abstract class a1 extends b implements z0 {
    public a1() {
        super("com.google.android.gms.nearby.messages.internal.IStatusCallback");
    }

    @Override // b.i.a.f.h.m.b
    public final boolean c(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        boolean z2 = false;
        if (i != 1) {
            return false;
        }
        int i3 = c.a;
        if (parcel.readInt() != 0) {
            z2 = true;
        }
        ((m) this).a.a(new n(z2));
        return true;
    }
}
