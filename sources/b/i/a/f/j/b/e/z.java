package b.i.a.f.j.b.e;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;
import b.i.a.f.e.h.j.k;
/* loaded from: classes3.dex */
public final class z implements Application.ActivityLifecycleCallbacks {
    public final Activity j;
    public final i k;

    public z(Activity activity, i iVar, q qVar) {
        this.j = activity;
        this.k = iVar;
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivityCreated(Activity activity, Bundle bundle) {
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivityDestroyed(Activity activity) {
        if (activity == this.j) {
            Log.v("NearbyMessages", String.format("Unregistering ClientLifecycleSafetyNet's ActivityLifecycleCallbacks for %s", activity.getPackageName()));
            activity.getApplication().unregisterActivityLifecycleCallbacks(this);
        }
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivityPaused(Activity activity) {
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivityResumed(Activity activity) {
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivityStarted(Activity activity) {
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivityStopped(Activity activity) {
        if (activity == this.j) {
            i iVar = this.k;
            iVar.c(new y(iVar, new a0(1) { // from class: b.i.a.f.j.b.e.p
                @Override // b.i.a.f.j.b.e.a0
                public final void a(f fVar, k kVar) {
                    fVar.G(1);
                }
            }));
        }
    }
}
