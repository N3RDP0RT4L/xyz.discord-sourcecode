package b.i.a.f.j.b.e;

import android.os.Parcel;
import android.os.RemoteException;
import b.i.a.f.h.m.b;
import b.i.a.f.h.m.c;
import b.i.a.f.h.m.j;
import com.google.android.gms.common.api.Status;
/* loaded from: classes3.dex */
public abstract class s0 extends b implements r0 {
    public s0() {
        super("com.google.android.gms.nearby.messages.internal.INearbyMessagesCallback");
    }

    @Override // b.i.a.f.h.m.b
    public final boolean c(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        ((j) this).g((Status) c.a(parcel, Status.CREATOR));
        return true;
    }
}
