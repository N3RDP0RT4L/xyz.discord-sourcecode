package b.i.a.f.j.b.e;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.nearby.messages.internal.ClientAppContext;
import com.google.android.gms.nearby.messages.internal.zzcg;
/* loaded from: classes3.dex */
public final class j0 implements Parcelable.Creator<zzcg> {
    @Override // android.os.Parcelable.Creator
    public final zzcg createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        IBinder iBinder = null;
        IBinder iBinder2 = null;
        PendingIntent pendingIntent = null;
        String str = null;
        String str2 = null;
        ClientAppContext clientAppContext = null;
        int i = 0;
        int i2 = 0;
        boolean z2 = false;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    i = d.G1(parcel, readInt);
                    break;
                case 2:
                    iBinder = d.F1(parcel, readInt);
                    break;
                case 3:
                    iBinder2 = d.F1(parcel, readInt);
                    break;
                case 4:
                    pendingIntent = (PendingIntent) d.Q(parcel, readInt, PendingIntent.CREATOR);
                    break;
                case 5:
                    i2 = d.G1(parcel, readInt);
                    break;
                case 6:
                    str = d.R(parcel, readInt);
                    break;
                case 7:
                    str2 = d.R(parcel, readInt);
                    break;
                case '\b':
                    z2 = d.E1(parcel, readInt);
                    break;
                case '\t':
                    clientAppContext = (ClientAppContext) d.Q(parcel, readInt, ClientAppContext.CREATOR);
                    break;
                default:
                    d.d2(parcel, readInt);
                    break;
            }
        }
        d.f0(parcel, m2);
        return new zzcg(i, iBinder, iBinder2, pendingIntent, i2, str, str2, z2, clientAppContext);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzcg[] newArray(int i) {
        return new zzcg[i];
    }
}
