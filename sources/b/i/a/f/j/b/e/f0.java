package b.i.a.f.j.b.e;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.nearby.messages.Strategy;
import com.google.android.gms.nearby.messages.internal.ClientAppContext;
import com.google.android.gms.nearby.messages.internal.zzaf;
import com.google.android.gms.nearby.messages.internal.zzbz;
/* loaded from: classes3.dex */
public final class f0 implements Parcelable.Creator<zzbz> {
    @Override // android.os.Parcelable.Creator
    public final zzbz createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        zzaf zzafVar = null;
        Strategy strategy = null;
        IBinder iBinder = null;
        String str = null;
        String str2 = null;
        IBinder iBinder2 = null;
        ClientAppContext clientAppContext = null;
        int i = 0;
        boolean z2 = false;
        boolean z3 = false;
        int i2 = 0;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    i = d.G1(parcel, readInt);
                    break;
                case 2:
                    zzafVar = (zzaf) d.Q(parcel, readInt, zzaf.CREATOR);
                    break;
                case 3:
                    strategy = (Strategy) d.Q(parcel, readInt, Strategy.CREATOR);
                    break;
                case 4:
                    iBinder = d.F1(parcel, readInt);
                    break;
                case 5:
                    str = d.R(parcel, readInt);
                    break;
                case 6:
                    str2 = d.R(parcel, readInt);
                    break;
                case 7:
                    z2 = d.E1(parcel, readInt);
                    break;
                case '\b':
                    iBinder2 = d.F1(parcel, readInt);
                    break;
                case '\t':
                    z3 = d.E1(parcel, readInt);
                    break;
                case '\n':
                    clientAppContext = (ClientAppContext) d.Q(parcel, readInt, ClientAppContext.CREATOR);
                    break;
                case 11:
                    i2 = d.G1(parcel, readInt);
                    break;
                default:
                    d.d2(parcel, readInt);
                    break;
            }
        }
        d.f0(parcel, m2);
        return new zzbz(i, zzafVar, strategy, iBinder, str, str2, z2, iBinder2, z3, clientAppContext, i2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzbz[] newArray(int i) {
        return new zzbz[i];
    }
}
