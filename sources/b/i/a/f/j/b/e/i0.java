package b.i.a.f.j.b.e;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.nearby.messages.internal.ClientAppContext;
import com.google.android.gms.nearby.messages.internal.zzaf;
import com.google.android.gms.nearby.messages.internal.zzce;
/* loaded from: classes3.dex */
public final class i0 implements Parcelable.Creator<zzce> {
    @Override // android.os.Parcelable.Creator
    public final zzce createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        zzaf zzafVar = null;
        IBinder iBinder = null;
        String str = null;
        String str2 = null;
        ClientAppContext clientAppContext = null;
        int i = 0;
        boolean z2 = false;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    i = d.G1(parcel, readInt);
                    break;
                case 2:
                    zzafVar = (zzaf) d.Q(parcel, readInt, zzaf.CREATOR);
                    break;
                case 3:
                    iBinder = d.F1(parcel, readInt);
                    break;
                case 4:
                    str = d.R(parcel, readInt);
                    break;
                case 5:
                    str2 = d.R(parcel, readInt);
                    break;
                case 6:
                    z2 = d.E1(parcel, readInt);
                    break;
                case 7:
                    clientAppContext = (ClientAppContext) d.Q(parcel, readInt, ClientAppContext.CREATOR);
                    break;
                default:
                    d.d2(parcel, readInt);
                    break;
            }
        }
        d.f0(parcel, m2);
        return new zzce(i, zzafVar, iBinder, str, str2, z2, clientAppContext);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzce[] newArray(int i) {
        return new zzce[i];
    }
}
