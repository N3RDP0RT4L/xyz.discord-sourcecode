package b.i.a.f.j.b.e;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.nearby.messages.internal.ClientAppContext;
import com.google.android.gms.nearby.messages.internal.zzj;
/* loaded from: classes3.dex */
public final class n0 implements Parcelable.Creator<zzj> {
    @Override // android.os.Parcelable.Creator
    public final zzj createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        int i = 0;
        ClientAppContext clientAppContext = null;
        int i2 = 0;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                i = d.G1(parcel, readInt);
            } else if (c == 2) {
                clientAppContext = (ClientAppContext) d.Q(parcel, readInt, ClientAppContext.CREATOR);
            } else if (c != 3) {
                d.d2(parcel, readInt);
            } else {
                i2 = d.G1(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        return new zzj(i, clientAppContext, i2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzj[] newArray(int i) {
        return new zzj[i];
    }
}
