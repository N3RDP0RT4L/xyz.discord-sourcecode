package b.i.a.f.j.b.e;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.nearby.messages.internal.zzad;
/* loaded from: classes3.dex */
public final class d implements Parcelable.Creator<zzad> {
    @Override // android.os.Parcelable.Creator
    public final zzad createFromParcel(Parcel parcel) {
        int m2 = b.c.a.a0.d.m2(parcel);
        String str = null;
        String str2 = null;
        int i = 0;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                str = b.c.a.a0.d.R(parcel, readInt);
            } else if (c == 2) {
                str2 = b.c.a.a0.d.R(parcel, readInt);
            } else if (c != 1000) {
                b.c.a.a0.d.d2(parcel, readInt);
            } else {
                i = b.c.a.a0.d.G1(parcel, readInt);
            }
        }
        b.c.a.a0.d.f0(parcel, m2);
        return new zzad(i, str, str2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzad[] newArray(int i) {
        return new zzad[i];
    }
}
