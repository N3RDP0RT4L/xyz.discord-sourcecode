package b.i.a.f.j.b.e;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.nearby.messages.internal.SubscribeRequest;
import com.google.android.gms.nearby.messages.internal.zzbz;
import com.google.android.gms.nearby.messages.internal.zzcb;
import com.google.android.gms.nearby.messages.internal.zzce;
import com.google.android.gms.nearby.messages.internal.zzcg;
import com.google.android.gms.nearby.messages.internal.zzj;
/* loaded from: classes3.dex */
public interface u0 extends IInterface {
    void C(SubscribeRequest subscribeRequest) throws RemoteException;

    void P(zzce zzceVar) throws RemoteException;

    void i0(zzj zzjVar) throws RemoteException;

    void j0(zzcg zzcgVar) throws RemoteException;

    void n(zzbz zzbzVar) throws RemoteException;

    void s(zzcb zzcbVar) throws RemoteException;
}
