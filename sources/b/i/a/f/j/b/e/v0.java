package b.i.a.f.j.b.e;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import b.i.a.f.h.m.a;
import b.i.a.f.h.m.c;
import com.google.android.gms.nearby.messages.internal.SubscribeRequest;
import com.google.android.gms.nearby.messages.internal.zzbz;
import com.google.android.gms.nearby.messages.internal.zzcb;
import com.google.android.gms.nearby.messages.internal.zzce;
import com.google.android.gms.nearby.messages.internal.zzcg;
import com.google.android.gms.nearby.messages.internal.zzj;
/* loaded from: classes3.dex */
public final class v0 extends a implements u0 {
    public v0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.nearby.messages.internal.INearbyMessagesService");
    }

    @Override // b.i.a.f.j.b.e.u0
    public final void C(SubscribeRequest subscribeRequest) throws RemoteException {
        Parcel c = c();
        c.b(c, subscribeRequest);
        g(3, c);
    }

    @Override // b.i.a.f.j.b.e.u0
    public final void P(zzce zzceVar) throws RemoteException {
        Parcel c = c();
        c.b(c, zzceVar);
        g(2, c);
    }

    @Override // b.i.a.f.j.b.e.u0
    public final void i0(zzj zzjVar) throws RemoteException {
        Parcel c = c();
        c.b(c, zzjVar);
        g(9, c);
    }

    @Override // b.i.a.f.j.b.e.u0
    public final void j0(zzcg zzcgVar) throws RemoteException {
        Parcel c = c();
        c.b(c, zzcgVar);
        g(4, c);
    }

    @Override // b.i.a.f.j.b.e.u0
    public final void n(zzbz zzbzVar) throws RemoteException {
        Parcel c = c();
        c.b(c, zzbzVar);
        g(1, c);
    }

    @Override // b.i.a.f.j.b.e.u0
    public final void s(zzcb zzcbVar) throws RemoteException {
        Parcel c = c();
        c.b(c, zzcbVar);
        g(8, c);
    }
}
