package b.i.a.f.j.b.e;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.nearby.messages.MessageFilter;
import com.google.android.gms.nearby.messages.Strategy;
import com.google.android.gms.nearby.messages.internal.ClientAppContext;
import com.google.android.gms.nearby.messages.internal.SubscribeRequest;
/* loaded from: classes3.dex */
public final class h0 implements Parcelable.Creator<SubscribeRequest> {
    @Override // android.os.Parcelable.Creator
    public final SubscribeRequest createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        IBinder iBinder = null;
        Strategy strategy = null;
        IBinder iBinder2 = null;
        MessageFilter messageFilter = null;
        PendingIntent pendingIntent = null;
        String str = null;
        String str2 = null;
        byte[] bArr = null;
        IBinder iBinder3 = null;
        ClientAppContext clientAppContext = null;
        int i = 0;
        int i2 = 0;
        boolean z2 = false;
        boolean z3 = false;
        boolean z4 = false;
        int i3 = 0;
        int i4 = 0;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    i = d.G1(parcel, readInt);
                    break;
                case 2:
                    iBinder = d.F1(parcel, readInt);
                    break;
                case 3:
                    strategy = (Strategy) d.Q(parcel, readInt, Strategy.CREATOR);
                    break;
                case 4:
                    iBinder2 = d.F1(parcel, readInt);
                    break;
                case 5:
                    messageFilter = (MessageFilter) d.Q(parcel, readInt, MessageFilter.CREATOR);
                    break;
                case 6:
                    pendingIntent = (PendingIntent) d.Q(parcel, readInt, PendingIntent.CREATOR);
                    break;
                case 7:
                    i2 = d.G1(parcel, readInt);
                    break;
                case '\b':
                    str = d.R(parcel, readInt);
                    break;
                case '\t':
                    str2 = d.R(parcel, readInt);
                    break;
                case '\n':
                    bArr = d.N(parcel, readInt);
                    break;
                case 11:
                    z2 = d.E1(parcel, readInt);
                    break;
                case '\f':
                    iBinder3 = d.F1(parcel, readInt);
                    break;
                case '\r':
                    z3 = d.E1(parcel, readInt);
                    break;
                case 14:
                    clientAppContext = (ClientAppContext) d.Q(parcel, readInt, ClientAppContext.CREATOR);
                    break;
                case 15:
                    z4 = d.E1(parcel, readInt);
                    break;
                case 16:
                    i3 = d.G1(parcel, readInt);
                    break;
                case 17:
                    i4 = d.G1(parcel, readInt);
                    break;
                default:
                    d.d2(parcel, readInt);
                    break;
            }
        }
        d.f0(parcel, m2);
        return new SubscribeRequest(i, iBinder, strategy, iBinder2, messageFilter, pendingIntent, i2, str, str2, bArr, z2, iBinder3, z3, clientAppContext, z4, i3, i4);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ SubscribeRequest[] newArray(int i) {
        return new SubscribeRequest[i];
    }
}
