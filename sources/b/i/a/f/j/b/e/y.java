package b.i.a.f.j.b.e;

import android.os.RemoteException;
import b.i.a.f.e.h.j.p;
import com.google.android.gms.tasks.TaskCompletionSource;
/* loaded from: classes3.dex */
public final class y extends p<f, Void> {
    public final /* synthetic */ a0 c;
    public final /* synthetic */ i d;

    public y(i iVar, a0 a0Var) {
        this.d = iVar;
        this.c = a0Var;
    }

    @Override // b.i.a.f.e.h.j.p
    public final /* synthetic */ void c(f fVar, TaskCompletionSource<Void> taskCompletionSource) throws RemoteException {
        this.c.a(fVar, i.j(this.d, taskCompletionSource));
    }
}
