package b.i.a.f.h.m;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.internal.nearby.zzgs;
/* loaded from: classes3.dex */
public final class f implements Parcelable.Creator<zzgs> {
    @Override // android.os.Parcelable.Creator
    public final zzgs createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        String str = null;
        String str2 = null;
        int i = 0;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 3) {
                str = d.R(parcel, readInt);
            } else if (c == 6) {
                str2 = d.R(parcel, readInt);
            } else if (c != 1000) {
                d.d2(parcel, readInt);
            } else {
                i = d.G1(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        return new zzgs(i, str, str2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzgs[] newArray(int i) {
        return new zzgs[i];
    }
}
