package b.i.a.f.h.m;

import android.os.Parcel;
import android.os.ParcelUuid;
import android.os.Parcelable;
import com.google.android.gms.internal.nearby.zzgp;
/* loaded from: classes3.dex */
public final class d implements Parcelable.Creator<zzgp> {
    @Override // android.os.Parcelable.Creator
    public final zzgp createFromParcel(Parcel parcel) {
        int m2 = b.c.a.a0.d.m2(parcel);
        ParcelUuid parcelUuid = null;
        ParcelUuid parcelUuid2 = null;
        ParcelUuid parcelUuid3 = null;
        byte[] bArr = null;
        byte[] bArr2 = null;
        byte[] bArr3 = null;
        byte[] bArr4 = null;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c != 1) {
                switch (c) {
                    case 4:
                        parcelUuid = (ParcelUuid) b.c.a.a0.d.Q(parcel, readInt, ParcelUuid.CREATOR);
                        continue;
                    case 5:
                        parcelUuid2 = (ParcelUuid) b.c.a.a0.d.Q(parcel, readInt, ParcelUuid.CREATOR);
                        continue;
                    case 6:
                        parcelUuid3 = (ParcelUuid) b.c.a.a0.d.Q(parcel, readInt, ParcelUuid.CREATOR);
                        continue;
                    case 7:
                        bArr = b.c.a.a0.d.N(parcel, readInt);
                        continue;
                    case '\b':
                        bArr2 = b.c.a.a0.d.N(parcel, readInt);
                        continue;
                    case '\t':
                        i2 = b.c.a.a0.d.G1(parcel, readInt);
                        continue;
                    case '\n':
                        bArr3 = b.c.a.a0.d.N(parcel, readInt);
                        continue;
                    case 11:
                        bArr4 = b.c.a.a0.d.N(parcel, readInt);
                        continue;
                    default:
                        b.c.a.a0.d.d2(parcel, readInt);
                        continue;
                }
            } else {
                i = b.c.a.a0.d.G1(parcel, readInt);
            }
        }
        b.c.a.a0.d.f0(parcel, m2);
        return new zzgp(i, parcelUuid, parcelUuid2, parcelUuid3, bArr, bArr2, i2, bArr3, bArr4);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzgp[] newArray(int i) {
        return new zzgp[i];
    }
}
