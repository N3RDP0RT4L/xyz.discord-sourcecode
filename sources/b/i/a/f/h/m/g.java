package b.i.a.f.h.m;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.internal.nearby.zzgu;
/* loaded from: classes3.dex */
public final class g implements Parcelable.Creator<zzgu> {
    @Override // android.os.Parcelable.Creator
    public final zzgu createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        int i = 0;
        byte[] bArr = null;
        int i2 = 0;
        boolean z2 = false;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                i2 = d.G1(parcel, readInt);
            } else if (c == 2) {
                bArr = d.N(parcel, readInt);
            } else if (c == 3) {
                z2 = d.E1(parcel, readInt);
            } else if (c != 1000) {
                d.d2(parcel, readInt);
            } else {
                i = d.G1(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        return new zzgu(i, i2, bArr, z2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzgu[] newArray(int i) {
        return new zzgu[i];
    }
}
