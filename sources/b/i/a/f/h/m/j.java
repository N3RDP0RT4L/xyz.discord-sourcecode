package b.i.a.f.h.m;

import android.os.RemoteException;
import android.util.Log;
import b.i.a.f.e.h.j.e;
import b.i.a.f.e.h.j.k;
import b.i.a.f.j.b.e.s0;
import com.google.android.gms.common.api.Status;
/* loaded from: classes3.dex */
public final class j extends s0 {
    public final k<e<Status>> a;

    /* renamed from: b  reason: collision with root package name */
    public boolean f1505b = false;

    public j(k<e<Status>> kVar) {
        this.a = kVar;
    }

    public final synchronized void g(Status status) throws RemoteException {
        if (!this.f1505b) {
            this.a.a(new k(status));
            this.f1505b = true;
            return;
        }
        String valueOf = String.valueOf(status);
        StringBuilder sb = new StringBuilder(valueOf.length() + 28);
        sb.append("Received multiple statuses: ");
        sb.append(valueOf);
        Log.wtf("NearbyMessagesCallbackWrapper", sb.toString(), new Exception());
    }
}
