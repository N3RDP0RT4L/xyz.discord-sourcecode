package b.i.a.f.h.m;

import b.i.a.f.e.h.j.e;
import com.google.android.gms.common.api.Status;
/* loaded from: classes3.dex */
public final class k extends l<e<Status>> {
    public final /* synthetic */ Status a;

    public k(Status status) {
        this.a = status;
    }

    @Override // b.i.a.f.e.h.j.k.b
    public final /* synthetic */ void a(Object obj) {
        e eVar = (e) obj;
        if (this.a.w0()) {
            eVar.b(this.a);
        } else {
            eVar.a(this.a);
        }
    }
}
