package b.i.a.f.h.b;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
/* compiled from: com.google.android.gms:play-services-auth-api-phone@@17.4.0 */
/* loaded from: classes3.dex */
public final class d implements e, IInterface {
    public final IBinder a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1401b = "com.google.android.gms.auth.api.phone.internal.ISmsRetrieverApiService";

    public d(IBinder iBinder) {
        this.a = iBinder;
    }

    @Override // b.i.a.f.h.b.e
    public final void Y(g gVar) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.f1401b);
        int i = c.a;
        obtain.writeStrongBinder((a) gVar);
        Parcel obtain2 = Parcel.obtain();
        try {
            this.a.transact(1, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this.a;
    }
}
