package b.i.a.f.h.i;

import java.lang.ref.Reference;
import java.util.List;
import java.util.Objects;
import java.util.Vector;
/* compiled from: com.google.firebase:firebase-messaging@@21.0.0 */
/* loaded from: classes3.dex */
public final class h extends g {
    public final f a = new f();

    @Override // b.i.a.f.h.i.g
    public final void a(Throwable th, Throwable th2) {
        List<Throwable> putIfAbsent;
        if (th2 != th) {
            Objects.requireNonNull(th2, "The suppressed exception cannot be null.");
            f fVar = this.a;
            for (Reference<? extends Throwable> poll = fVar.f1404b.poll(); poll != null; poll = fVar.f1404b.poll()) {
                fVar.a.remove(poll);
            }
            List<Throwable> list = fVar.a.get(new i(th, null));
            if (list == null && (putIfAbsent = fVar.a.putIfAbsent(new i(th, fVar.f1404b), (list = new Vector<>(2)))) != null) {
                list = putIfAbsent;
            }
            list.add(th2);
            return;
        }
        throw new IllegalArgumentException("Self suppression is not allowed.", th2);
    }
}
