package b.i.a.f.h.i;

import java.io.PrintStream;
import org.objectweb.asm.Opcodes;
/* compiled from: com.google.firebase:firebase-messaging@@21.0.0 */
/* loaded from: classes3.dex */
public final class e {
    public static final g a;

    /* compiled from: com.google.firebase:firebase-messaging@@21.0.0 */
    /* loaded from: classes3.dex */
    public static final class a extends g {
        @Override // b.i.a.f.h.i.g
        public final void a(Throwable th, Throwable th2) {
        }
    }

    static {
        g gVar;
        Integer num = null;
        try {
            try {
                num = (Integer) Class.forName("android.os.Build$VERSION").getField("SDK_INT").get(null);
            } catch (Exception e) {
                System.err.println("Failed to retrieve value from android.os.Build$VERSION.SDK_INT due to the following exception.");
                e.printStackTrace(System.err);
            }
            if (num != null && num.intValue() >= 19) {
                gVar = new j();
            } else if (!Boolean.getBoolean("com.google.devtools.build.android.desugar.runtime.twr_disable_mimic")) {
                gVar = new h();
            } else {
                gVar = new a();
            }
        } catch (Throwable th) {
            PrintStream printStream = System.err;
            String name = a.class.getName();
            StringBuilder sb = new StringBuilder(name.length() + Opcodes.I2L);
            sb.append("An error has occurred when initializing the try-with-resources desuguring strategy. The default strategy ");
            sb.append(name);
            sb.append("will be used. The error is: ");
            printStream.println(sb.toString());
            th.printStackTrace(System.err);
            gVar = new a();
        }
        a = gVar;
        if (num != null) {
            num.intValue();
        }
    }
}
