package b.i.a.f.h.e;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import b.i.a.f.e.h.j.g0;
import b.i.a.f.e.h.j.i0;
import b.i.a.f.l.b.f;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.signin.internal.zab;
import com.google.android.gms.signin.internal.zag;
import com.google.android.gms.signin.internal.zam;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public class a extends Binder implements IInterface {
    public a(String str) {
        attachInterface(this, str);
    }

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this;
    }

    @Override // android.os.Binder
    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        boolean z2;
        if (i > 16777215) {
            z2 = super.onTransact(i, parcel, parcel2, i2);
        } else {
            parcel.enforceInterface(getInterfaceDescriptor());
            z2 = false;
        }
        if (z2) {
            return true;
        }
        f fVar = (f) this;
        switch (i) {
            case 3:
                ConnectionResult connectionResult = (ConnectionResult) b.a(parcel, ConnectionResult.CREATOR);
                zab zabVar = (zab) b.a(parcel, zab.CREATOR);
                break;
            case 4:
                Status status = (Status) b.a(parcel, Status.CREATOR);
                break;
            case 5:
            default:
                return false;
            case 6:
                Status status2 = (Status) b.a(parcel, Status.CREATOR);
                break;
            case 7:
                Status status3 = (Status) b.a(parcel, Status.CREATOR);
                GoogleSignInAccount googleSignInAccount = (GoogleSignInAccount) b.a(parcel, GoogleSignInAccount.CREATOR);
                break;
            case 8:
                g0 g0Var = (g0) fVar;
                g0Var.c.post(new i0(g0Var, (zam) b.a(parcel, zam.CREATOR)));
                break;
            case 9:
                zag zagVar = (zag) b.a(parcel, zag.CREATOR);
                break;
        }
        parcel2.writeNoException();
        return true;
    }
}
