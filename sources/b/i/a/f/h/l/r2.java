package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class r2 extends l2<String> {
    public r2(q2 q2Var, String str, String str2) {
        super(q2Var, str, str2, true, null);
    }

    @Override // b.i.a.f.h.l.l2
    public final /* synthetic */ String a(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        return null;
    }
}
