package b.i.a.f.h.l;

import b.i.a.f.h.l.n0;
/* compiled from: com.google.android.gms:play-services-measurement@@18.0.0 */
/* loaded from: classes3.dex */
public final class r0 implements a5 {
    public static final a5 a = new r0();

    @Override // b.i.a.f.h.l.a5
    public final boolean f(int i) {
        return n0.a.f(i) != null;
    }
}
