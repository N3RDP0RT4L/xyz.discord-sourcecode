package b.i.a.f.h.l;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.internal.measurement.zzae;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class h implements Parcelable.Creator<zzae> {
    @Override // android.os.Parcelable.Creator
    public final zzae createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        long j = 0;
        String str = null;
        long j2 = 0;
        String str2 = null;
        String str3 = null;
        Bundle bundle = null;
        boolean z2 = false;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    j = d.H1(parcel, readInt);
                    break;
                case 2:
                    j2 = d.H1(parcel, readInt);
                    break;
                case 3:
                    z2 = d.E1(parcel, readInt);
                    break;
                case 4:
                    str = d.R(parcel, readInt);
                    break;
                case 5:
                    str2 = d.R(parcel, readInt);
                    break;
                case 6:
                    str3 = d.R(parcel, readInt);
                    break;
                case 7:
                    bundle = d.M(parcel, readInt);
                    break;
                default:
                    d.d2(parcel, readInt);
                    break;
            }
        }
        d.f0(parcel, m2);
        return new zzae(j, j2, z2, str, str2, str3, bundle);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzae[] newArray(int i) {
        return new zzae[i];
    }
}
