package b.i.a.f.h.l;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class t6 extends r6<FieldDescriptorType, Object> {
    public t6(int i) {
        super(i, null);
    }

    @Override // b.i.a.f.h.l.r6
    public final void c() {
        if (!this.n) {
            for (int i = 0; i < e(); i++) {
                Map.Entry<FieldDescriptorType, Object> d = d(i);
                if (((p4) d.getKey()).d()) {
                    d.setValue(Collections.unmodifiableList((List) d.getValue()));
                }
            }
            Iterator it = g().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                if (((p4) entry.getKey()).d()) {
                    entry.setValue(Collections.unmodifiableList((List) entry.getValue()));
                }
            }
        }
        super.c();
    }
}
