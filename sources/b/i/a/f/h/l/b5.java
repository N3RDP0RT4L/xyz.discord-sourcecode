package b.i.a.f.h.l;

import java.util.List;
import java.util.RandomAccess;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public interface b5<E> extends List<E>, RandomAccess {
    boolean a();

    b5<E> f(int i);

    void g0();
}
