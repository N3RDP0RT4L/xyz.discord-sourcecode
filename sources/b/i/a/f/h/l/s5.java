package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class s5 implements z5 {
    @Override // b.i.a.f.h.l.z5
    public final boolean a(Class<?> cls) {
        return false;
    }

    @Override // b.i.a.f.h.l.z5
    public final a6 b(Class<?> cls) {
        throw new IllegalStateException("This should never be called.");
    }
}
