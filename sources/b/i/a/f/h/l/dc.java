package b.i.a.f.h.l;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import b.i.a.f.f.a;
import com.google.android.gms.internal.measurement.zzae;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public abstract class dc extends s0 implements ec {
    public dc() {
        super("com.google.android.gms.measurement.api.internal.IAppMeasurementDynamiteService");
    }

    public static ec asInterface(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.api.internal.IAppMeasurementDynamiteService");
        if (queryLocalInterface instanceof ec) {
            return (ec) queryLocalInterface;
        }
        return new gc(iBinder);
    }

    @Override // b.i.a.f.h.l.s0
    public final boolean c(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        fc fcVar;
        boolean z2 = false;
        fc fcVar2 = null;
        fc fcVar3 = null;
        fc fcVar4 = null;
        c cVar = null;
        c cVar2 = null;
        c cVar3 = null;
        fc fcVar5 = null;
        fc fcVar6 = null;
        fc fcVar7 = null;
        fc fcVar8 = null;
        fc fcVar9 = null;
        fc fcVar10 = null;
        d dVar = null;
        fc fcVar11 = null;
        fc fcVar12 = null;
        fc fcVar13 = null;
        fc fcVar14 = null;
        fc fcVar15 = null;
        switch (i) {
            case 1:
                initialize(a.AbstractBinderC0116a.g(parcel.readStrongBinder()), (zzae) v.a(parcel, zzae.CREATOR), parcel.readLong());
                break;
            case 2:
                logEvent(parcel.readString(), parcel.readString(), (Bundle) v.a(parcel, Bundle.CREATOR), parcel.readInt() != 0, parcel.readInt() != 0, parcel.readLong());
                break;
            case 3:
                String readString = parcel.readString();
                String readString2 = parcel.readString();
                Bundle bundle = (Bundle) v.a(parcel, Bundle.CREATOR);
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder != null) {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface instanceof fc) {
                        fcVar = (fc) queryLocalInterface;
                    } else {
                        fcVar = new hc(readStrongBinder);
                    }
                    fcVar2 = fcVar;
                }
                logEventAndBundle(readString, readString2, bundle, fcVar2, parcel.readLong());
                break;
            case 4:
                String readString3 = parcel.readString();
                String readString4 = parcel.readString();
                a g = a.AbstractBinderC0116a.g(parcel.readStrongBinder());
                ClassLoader classLoader = v.a;
                setUserProperty(readString3, readString4, g, parcel.readInt() != 0, parcel.readLong());
                break;
            case 5:
                String readString5 = parcel.readString();
                String readString6 = parcel.readString();
                ClassLoader classLoader2 = v.a;
                if (parcel.readInt() != 0) {
                    z2 = true;
                }
                IBinder readStrongBinder2 = parcel.readStrongBinder();
                if (readStrongBinder2 != null) {
                    IInterface queryLocalInterface2 = readStrongBinder2.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface2 instanceof fc) {
                        fcVar15 = (fc) queryLocalInterface2;
                    } else {
                        fcVar15 = new hc(readStrongBinder2);
                    }
                }
                getUserProperties(readString5, readString6, z2, fcVar15);
                break;
            case 6:
                String readString7 = parcel.readString();
                IBinder readStrongBinder3 = parcel.readStrongBinder();
                if (readStrongBinder3 != null) {
                    IInterface queryLocalInterface3 = readStrongBinder3.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface3 instanceof fc) {
                        fcVar14 = (fc) queryLocalInterface3;
                    } else {
                        fcVar14 = new hc(readStrongBinder3);
                    }
                }
                getMaxUserProperties(readString7, fcVar14);
                break;
            case 7:
                setUserId(parcel.readString(), parcel.readLong());
                break;
            case 8:
                setConditionalUserProperty((Bundle) v.a(parcel, Bundle.CREATOR), parcel.readLong());
                break;
            case 9:
                clearConditionalUserProperty(parcel.readString(), parcel.readString(), (Bundle) v.a(parcel, Bundle.CREATOR));
                break;
            case 10:
                String readString8 = parcel.readString();
                String readString9 = parcel.readString();
                IBinder readStrongBinder4 = parcel.readStrongBinder();
                if (readStrongBinder4 != null) {
                    IInterface queryLocalInterface4 = readStrongBinder4.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface4 instanceof fc) {
                        fcVar13 = (fc) queryLocalInterface4;
                    } else {
                        fcVar13 = new hc(readStrongBinder4);
                    }
                }
                getConditionalUserProperties(readString8, readString9, fcVar13);
                break;
            case 11:
                ClassLoader classLoader3 = v.a;
                if (parcel.readInt() != 0) {
                    z2 = true;
                }
                setMeasurementEnabled(z2, parcel.readLong());
                break;
            case 12:
                resetAnalyticsData(parcel.readLong());
                break;
            case 13:
                setMinimumSessionDuration(parcel.readLong());
                break;
            case 14:
                setSessionTimeoutDuration(parcel.readLong());
                break;
            case 15:
                setCurrentScreen(a.AbstractBinderC0116a.g(parcel.readStrongBinder()), parcel.readString(), parcel.readString(), parcel.readLong());
                break;
            case 16:
                IBinder readStrongBinder5 = parcel.readStrongBinder();
                if (readStrongBinder5 != null) {
                    IInterface queryLocalInterface5 = readStrongBinder5.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface5 instanceof fc) {
                        fcVar12 = (fc) queryLocalInterface5;
                    } else {
                        fcVar12 = new hc(readStrongBinder5);
                    }
                }
                getCurrentScreenName(fcVar12);
                break;
            case 17:
                IBinder readStrongBinder6 = parcel.readStrongBinder();
                if (readStrongBinder6 != null) {
                    IInterface queryLocalInterface6 = readStrongBinder6.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface6 instanceof fc) {
                        fcVar11 = (fc) queryLocalInterface6;
                    } else {
                        fcVar11 = new hc(readStrongBinder6);
                    }
                }
                getCurrentScreenClass(fcVar11);
                break;
            case 18:
                IBinder readStrongBinder7 = parcel.readStrongBinder();
                if (readStrongBinder7 != null) {
                    IInterface queryLocalInterface7 = readStrongBinder7.queryLocalInterface("com.google.android.gms.measurement.api.internal.IStringProvider");
                    if (queryLocalInterface7 instanceof d) {
                        dVar = (d) queryLocalInterface7;
                    } else {
                        dVar = new f(readStrongBinder7);
                    }
                }
                setInstanceIdProvider(dVar);
                break;
            case 19:
                IBinder readStrongBinder8 = parcel.readStrongBinder();
                if (readStrongBinder8 != null) {
                    IInterface queryLocalInterface8 = readStrongBinder8.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface8 instanceof fc) {
                        fcVar10 = (fc) queryLocalInterface8;
                    } else {
                        fcVar10 = new hc(readStrongBinder8);
                    }
                }
                getCachedAppInstanceId(fcVar10);
                break;
            case 20:
                IBinder readStrongBinder9 = parcel.readStrongBinder();
                if (readStrongBinder9 != null) {
                    IInterface queryLocalInterface9 = readStrongBinder9.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface9 instanceof fc) {
                        fcVar9 = (fc) queryLocalInterface9;
                    } else {
                        fcVar9 = new hc(readStrongBinder9);
                    }
                }
                getAppInstanceId(fcVar9);
                break;
            case 21:
                IBinder readStrongBinder10 = parcel.readStrongBinder();
                if (readStrongBinder10 != null) {
                    IInterface queryLocalInterface10 = readStrongBinder10.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface10 instanceof fc) {
                        fcVar8 = (fc) queryLocalInterface10;
                    } else {
                        fcVar8 = new hc(readStrongBinder10);
                    }
                }
                getGmpAppId(fcVar8);
                break;
            case 22:
                IBinder readStrongBinder11 = parcel.readStrongBinder();
                if (readStrongBinder11 != null) {
                    IInterface queryLocalInterface11 = readStrongBinder11.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface11 instanceof fc) {
                        fcVar7 = (fc) queryLocalInterface11;
                    } else {
                        fcVar7 = new hc(readStrongBinder11);
                    }
                }
                generateEventId(fcVar7);
                break;
            case 23:
                beginAdUnitExposure(parcel.readString(), parcel.readLong());
                break;
            case 24:
                endAdUnitExposure(parcel.readString(), parcel.readLong());
                break;
            case 25:
                onActivityStarted(a.AbstractBinderC0116a.g(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 26:
                onActivityStopped(a.AbstractBinderC0116a.g(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 27:
                onActivityCreated(a.AbstractBinderC0116a.g(parcel.readStrongBinder()), (Bundle) v.a(parcel, Bundle.CREATOR), parcel.readLong());
                break;
            case 28:
                onActivityDestroyed(a.AbstractBinderC0116a.g(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 29:
                onActivityPaused(a.AbstractBinderC0116a.g(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 30:
                onActivityResumed(a.AbstractBinderC0116a.g(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 31:
                a g2 = a.AbstractBinderC0116a.g(parcel.readStrongBinder());
                IBinder readStrongBinder12 = parcel.readStrongBinder();
                if (readStrongBinder12 != null) {
                    IInterface queryLocalInterface12 = readStrongBinder12.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface12 instanceof fc) {
                        fcVar6 = (fc) queryLocalInterface12;
                    } else {
                        fcVar6 = new hc(readStrongBinder12);
                    }
                }
                onActivitySaveInstanceState(g2, fcVar6, parcel.readLong());
                break;
            case 32:
                Bundle bundle2 = (Bundle) v.a(parcel, Bundle.CREATOR);
                IBinder readStrongBinder13 = parcel.readStrongBinder();
                if (readStrongBinder13 != null) {
                    IInterface queryLocalInterface13 = readStrongBinder13.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface13 instanceof fc) {
                        fcVar5 = (fc) queryLocalInterface13;
                    } else {
                        fcVar5 = new hc(readStrongBinder13);
                    }
                }
                performAction(bundle2, fcVar5, parcel.readLong());
                break;
            case 33:
                logHealthData(parcel.readInt(), parcel.readString(), a.AbstractBinderC0116a.g(parcel.readStrongBinder()), a.AbstractBinderC0116a.g(parcel.readStrongBinder()), a.AbstractBinderC0116a.g(parcel.readStrongBinder()));
                break;
            case 34:
                IBinder readStrongBinder14 = parcel.readStrongBinder();
                if (readStrongBinder14 != null) {
                    IInterface queryLocalInterface14 = readStrongBinder14.queryLocalInterface("com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
                    if (queryLocalInterface14 instanceof c) {
                        cVar3 = (c) queryLocalInterface14;
                    } else {
                        cVar3 = new e(readStrongBinder14);
                    }
                }
                setEventInterceptor(cVar3);
                break;
            case 35:
                IBinder readStrongBinder15 = parcel.readStrongBinder();
                if (readStrongBinder15 != null) {
                    IInterface queryLocalInterface15 = readStrongBinder15.queryLocalInterface("com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
                    if (queryLocalInterface15 instanceof c) {
                        cVar2 = (c) queryLocalInterface15;
                    } else {
                        cVar2 = new e(readStrongBinder15);
                    }
                }
                registerOnMeasurementEventListener(cVar2);
                break;
            case 36:
                IBinder readStrongBinder16 = parcel.readStrongBinder();
                if (readStrongBinder16 != null) {
                    IInterface queryLocalInterface16 = readStrongBinder16.queryLocalInterface("com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
                    if (queryLocalInterface16 instanceof c) {
                        cVar = (c) queryLocalInterface16;
                    } else {
                        cVar = new e(readStrongBinder16);
                    }
                }
                unregisterOnMeasurementEventListener(cVar);
                break;
            case 37:
                initForTests(parcel.readHashMap(v.a));
                break;
            case 38:
                IBinder readStrongBinder17 = parcel.readStrongBinder();
                if (readStrongBinder17 != null) {
                    IInterface queryLocalInterface17 = readStrongBinder17.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface17 instanceof fc) {
                        fcVar4 = (fc) queryLocalInterface17;
                    } else {
                        fcVar4 = new hc(readStrongBinder17);
                    }
                }
                getTestFlag(fcVar4, parcel.readInt());
                break;
            case 39:
                ClassLoader classLoader4 = v.a;
                if (parcel.readInt() != 0) {
                    z2 = true;
                }
                setDataCollectionEnabled(z2);
                break;
            case 40:
                IBinder readStrongBinder18 = parcel.readStrongBinder();
                if (readStrongBinder18 != null) {
                    IInterface queryLocalInterface18 = readStrongBinder18.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface18 instanceof fc) {
                        fcVar3 = (fc) queryLocalInterface18;
                    } else {
                        fcVar3 = new hc(readStrongBinder18);
                    }
                }
                isDataCollectionEnabled(fcVar3);
                break;
            case 41:
            default:
                return false;
            case 42:
                setDefaultEventParameters((Bundle) v.a(parcel, Bundle.CREATOR));
                break;
            case 43:
                clearMeasurementEnabled(parcel.readLong());
                break;
            case 44:
                setConsent((Bundle) v.a(parcel, Bundle.CREATOR), parcel.readLong());
                break;
            case 45:
                setConsentThirdParty((Bundle) v.a(parcel, Bundle.CREATOR), parcel.readLong());
                break;
        }
        parcel2.writeNoException();
        return true;
    }
}
