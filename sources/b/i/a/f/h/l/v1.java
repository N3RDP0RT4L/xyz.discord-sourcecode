package b.i.a.f.h.l;

import android.content.Context;
import b.d.b.a.a;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class v1 extends t2 {
    public final Context a;

    /* renamed from: b  reason: collision with root package name */
    public final z2<x2<h2>> f1494b;

    public v1(Context context, z2<x2<h2>> z2Var) {
        this.a = context;
        this.f1494b = z2Var;
    }

    @Override // b.i.a.f.h.l.t2
    public final Context a() {
        return this.a;
    }

    @Override // b.i.a.f.h.l.t2
    public final z2<x2<h2>> b() {
        return this.f1494b;
    }

    public final boolean equals(Object obj) {
        z2<x2<h2>> z2Var;
        if (obj == this) {
            return true;
        }
        if (obj instanceof t2) {
            t2 t2Var = (t2) obj;
            if (this.a.equals(t2Var.a()) && ((z2Var = this.f1494b) != null ? z2Var.equals(t2Var.b()) : t2Var.b() == null)) {
                return true;
            }
        }
        return false;
    }

    public final int hashCode() {
        int hashCode = (this.a.hashCode() ^ 1000003) * 1000003;
        z2<x2<h2>> z2Var = this.f1494b;
        return hashCode ^ (z2Var == null ? 0 : z2Var.hashCode());
    }

    public final String toString() {
        String valueOf = String.valueOf(this.a);
        String valueOf2 = String.valueOf(this.f1494b);
        StringBuilder Q = a.Q(valueOf2.length() + valueOf.length() + 46, "FlagsContext{context=", valueOf, ", hermeticFileOverrides=", valueOf2);
        Q.append("}");
        return Q.toString();
    }
}
