package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public abstract class m5 {
    public static final m5 a = new o5(null);

    /* renamed from: b  reason: collision with root package name */
    public static final m5 f1455b = new n5(null);

    public m5(l5 l5Var) {
    }

    public abstract <L> void a(Object obj, Object obj2, long j);

    public abstract void b(Object obj, long j);
}
