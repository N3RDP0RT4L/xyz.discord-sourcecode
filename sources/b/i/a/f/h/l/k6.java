package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class k6 {
    public static final i6 a;

    /* renamed from: b  reason: collision with root package name */
    public static final i6 f1450b;

    static {
        i6 i6Var;
        try {
            i6Var = (i6) Class.forName("com.google.protobuf.NewInstanceSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            i6Var = null;
        }
        a = i6Var;
        f1450b = new h6();
    }
}
