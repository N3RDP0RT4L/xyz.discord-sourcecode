package b.i.a.f.h.l;

import java.nio.charset.Charset;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class n6 {
    public static final n6 a = new n6();
    public final ConcurrentMap<Class<?>, q6<?>> c = new ConcurrentHashMap();

    /* renamed from: b  reason: collision with root package name */
    public final p6 f1458b = new p5();

    public final <T> q6<T> a(Class<T> cls) {
        q6<T> q6Var;
        g6 g6Var;
        Class<?> cls2;
        Charset charset = w4.a;
        Objects.requireNonNull(cls, "messageType");
        q6<T> q6Var2 = (q6<T>) this.c.get(cls);
        if (q6Var2 != null) {
            return q6Var2;
        }
        p5 p5Var = (p5) this.f1458b;
        Objects.requireNonNull(p5Var);
        Class<?> cls3 = s6.a;
        if (u4.class.isAssignableFrom(cls) || (cls2 = s6.a) == null || cls2.isAssignableFrom(cls)) {
            a6 b2 = p5Var.f1474b.b(cls);
            if (b2.b()) {
                if (u4.class.isAssignableFrom(cls)) {
                    d7<?, ?> d7Var = s6.d;
                    j4<?> j4Var = k4.a;
                    g6Var = new g6(d7Var, k4.a, b2.c());
                } else {
                    d7<?, ?> d7Var2 = s6.f1491b;
                    j4<?> j4Var2 = k4.f1449b;
                    if (j4Var2 != null) {
                        g6Var = new g6(d7Var2, j4Var2, b2.c());
                    } else {
                        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
                    }
                }
                q6Var = g6Var;
            } else {
                boolean z2 = false;
                if (u4.class.isAssignableFrom(cls)) {
                    if (b2.a() == 1) {
                        z2 = true;
                    }
                    if (z2) {
                        i6 i6Var = k6.f1450b;
                        m5 m5Var = m5.f1455b;
                        d7<?, ?> d7Var3 = s6.d;
                        j4<?> j4Var3 = k4.a;
                        q6Var = e6.n(b2, i6Var, m5Var, d7Var3, k4.a, x5.f1498b);
                    } else {
                        q6Var = e6.n(b2, k6.f1450b, m5.f1455b, s6.d, null, x5.f1498b);
                    }
                } else {
                    if (b2.a() == 1) {
                        z2 = true;
                    }
                    if (z2) {
                        i6 i6Var2 = k6.a;
                        m5 m5Var2 = m5.a;
                        d7<?, ?> d7Var4 = s6.f1491b;
                        j4<?> j4Var4 = k4.f1449b;
                        if (j4Var4 != null) {
                            q6Var = e6.n(b2, i6Var2, m5Var2, d7Var4, j4Var4, x5.a);
                        } else {
                            throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
                        }
                    } else {
                        q6Var = e6.n(b2, k6.a, m5.a, s6.c, null, x5.a);
                    }
                }
            }
            q6<T> q6Var3 = (q6<T>) this.c.putIfAbsent(cls, q6Var);
            return q6Var3 != null ? q6Var3 : q6Var;
        }
        throw new IllegalArgumentException("Message classes must extend GeneratedMessage or GeneratedMessageLite");
    }

    public final <T> q6<T> b(T t) {
        return a(t.getClass());
    }
}
