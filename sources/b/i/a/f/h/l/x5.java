package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class x5 {
    public static final v5 a;

    /* renamed from: b  reason: collision with root package name */
    public static final v5 f1498b;

    static {
        v5 v5Var;
        try {
            v5Var = (v5) Class.forName("com.google.protobuf.MapFieldSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            v5Var = null;
        }
        a = v5Var;
        f1498b = new y5();
    }
}
