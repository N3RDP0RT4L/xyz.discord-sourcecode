package b.i.a.f.h.l;

import b.d.b.a.a;
import java.util.Objects;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class c3<T> implements z2<T> {
    public volatile z2<T> j;
    public volatile boolean k;
    @NullableDecl
    public T l;

    public c3(z2<T> z2Var) {
        Objects.requireNonNull(z2Var);
        this.j = z2Var;
    }

    @Override // b.i.a.f.h.l.z2
    public final T a() {
        if (!this.k) {
            synchronized (this) {
                if (!this.k) {
                    T a = this.j.a();
                    this.l = a;
                    this.k = true;
                    this.j = null;
                    return a;
                }
            }
        }
        return this.l;
    }

    public final String toString() {
        Object obj = this.j;
        if (obj == null) {
            String valueOf = String.valueOf(this.l);
            obj = a.j(valueOf.length() + 25, "<supplier that returned ", valueOf, ">");
        }
        String valueOf2 = String.valueOf(obj);
        return a.j(valueOf2.length() + 19, "Suppliers.memoize(", valueOf2, ")");
    }
}
