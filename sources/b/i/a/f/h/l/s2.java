package b.i.a.f.h.l;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;
import androidx.annotation.GuardedBy;
import androidx.collection.ArrayMap;
import java.util.Map;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class s2 implements b2 {
    @GuardedBy("SharedPreferencesLoader.class")
    public static final Map<String, s2> a = new ArrayMap();

    /* renamed from: b  reason: collision with root package name */
    public final SharedPreferences f1488b;
    public final SharedPreferences.OnSharedPreferenceChangeListener c;
    public final Object d;
    public volatile Map<String, ?> e;

    public static s2 a(Context context) {
        s2 s2Var;
        if (!w1.a()) {
            synchronized (s2.class) {
                s2Var = a.get(null);
                if (s2Var == null) {
                    StrictMode.allowThreadDiskReads();
                    throw null;
                }
            }
            return s2Var;
        }
        throw null;
    }

    public static synchronized void b() {
        synchronized (s2.class) {
            for (s2 s2Var : a.values()) {
                s2Var.f1488b.unregisterOnSharedPreferenceChangeListener(s2Var.c);
            }
            a.clear();
        }
    }

    @Override // b.i.a.f.h.l.b2
    public final Object g(String str) {
        Map<String, ?> map = this.e;
        if (map == null) {
            synchronized (this.d) {
                map = this.e;
                if (map == null) {
                    StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
                    Map<String, ?> all = this.f1488b.getAll();
                    this.e = all;
                    StrictMode.setThreadPolicy(allowThreadDiskReads);
                    map = all;
                }
            }
        }
        if (map != null) {
            return map.get(str);
        }
        return null;
    }
}
