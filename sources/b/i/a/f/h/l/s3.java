package b.i.a.f.h.l;

import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class s3 {
    public int a;

    /* renamed from: b  reason: collision with root package name */
    public long f1489b;
    public Object c;
    public final h4 d;

    public s3(h4 h4Var) {
        Objects.requireNonNull(h4Var);
        this.d = h4Var;
    }
}
