package b.i.a.f.h.l;

import java.util.Arrays;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class w3 implements x3 {
    public w3(v3 v3Var) {
    }

    @Override // b.i.a.f.h.l.x3
    public final byte[] a(byte[] bArr, int i, int i2) {
        return Arrays.copyOfRange(bArr, i, i2 + i);
    }
}
