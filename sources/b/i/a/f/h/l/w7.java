package b.i.a.f.h.l;

import com.google.android.material.shadow.ShadowDrawableWrapper;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public enum w7 {
    INT(0),
    LONG(0L),
    FLOAT(Float.valueOf(0.0f)),
    DOUBLE(Double.valueOf((double) ShadowDrawableWrapper.COS_45)),
    BOOLEAN(Boolean.FALSE),
    STRING(""),
    BYTE_STRING(t3.j),
    ENUM(null),
    MESSAGE(null);
    
    private final Object zzj;

    w7(Object obj) {
        this.zzj = obj;
    }
}
