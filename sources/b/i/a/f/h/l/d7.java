package b.i.a.f.h.l;

import java.io.IOException;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public abstract class d7<T, B> {
    public abstract B a();

    public abstract void b(B b2, int i, long j);

    public abstract void c(T t, v7 v7Var) throws IOException;

    public abstract void d(Object obj, T t);

    public abstract T e(Object obj);

    public abstract void f(T t, v7 v7Var) throws IOException;

    public abstract T g(T t, T t2);

    public abstract void h(Object obj);

    public abstract int i(T t);

    public abstract int j(T t);
}
