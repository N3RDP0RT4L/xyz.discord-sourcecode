package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class n5 extends m5 {
    public n5(l5 l5Var) {
        super(null);
    }

    public static <E> b5<E> c(Object obj, long j) {
        return (b5) j7.r(obj, j);
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v2, types: [java.util.List] */
    @Override // b.i.a.f.h.l.m5
    public final <E> void a(Object obj, Object obj2, long j) {
        b5<E> c = c(obj, j);
        b5<E> c2 = c(obj2, j);
        int size = c.size();
        int size2 = c2.size();
        b5<E> b5Var = c;
        b5Var = c;
        if (size > 0 && size2 > 0) {
            boolean a = c.a();
            b5<E> b5Var2 = c;
            if (!a) {
                b5Var2 = c.f(size2 + size);
            }
            b5Var2.addAll(c2);
            b5Var = b5Var2;
        }
        if (size > 0) {
            c2 = b5Var;
        }
        j7.f(obj, j, c2);
    }

    @Override // b.i.a.f.h.l.m5
    public final void b(Object obj, long j) {
        c(obj, j).g0();
    }
}
