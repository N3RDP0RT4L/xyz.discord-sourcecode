package b.i.a.f.h.l;

import b.i.a.f.h.l.u4;
/* compiled from: com.google.android.gms:play-services-measurement@@18.0.0 */
/* loaded from: classes3.dex */
public final class p1 extends u4<p1, a> implements d6 {
    private static final p1 zzf;
    private static volatile j6<p1> zzg;
    private int zzc;
    private b5<q1> zzd = m6.k;
    private n1 zze;

    /* compiled from: com.google.android.gms:play-services-measurement@@18.0.0 */
    /* loaded from: classes3.dex */
    public static final class a extends u4.b<p1, a> implements d6 {
        public a(r1 r1Var) {
            super(p1.zzf);
        }
    }

    static {
        p1 p1Var = new p1();
        zzf = p1Var;
        u4.r(p1.class, p1Var);
    }

    @Override // b.i.a.f.h.l.u4
    public final Object p(int i, Object obj, Object obj2) {
        switch (r1.a[i - 1]) {
            case 1:
                return new p1();
            case 2:
                return new a(null);
            case 3:
                return new o6(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001\u001b\u0002ဉ\u0000", new Object[]{"zzc", "zzd", q1.class, "zze"});
            case 4:
                return zzf;
            case 5:
                j6<p1> j6Var = zzg;
                if (j6Var == null) {
                    synchronized (p1.class) {
                        j6Var = zzg;
                        if (j6Var == null) {
                            j6Var = new u4.a<>(zzf);
                            zzg = j6Var;
                        }
                    }
                }
                return j6Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
