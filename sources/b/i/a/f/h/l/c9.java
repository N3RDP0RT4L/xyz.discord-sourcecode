package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class c9 implements d9 {
    public static final l2<Boolean> a = new q2(i2.a("com.google.android.gms.measurement")).c("measurement.client.firebase_feature_rollout.v1.enable", true);

    @Override // b.i.a.f.h.l.d9
    public final boolean a() {
        return true;
    }

    @Override // b.i.a.f.h.l.d9
    public final boolean b() {
        return a.d().booleanValue();
    }
}
