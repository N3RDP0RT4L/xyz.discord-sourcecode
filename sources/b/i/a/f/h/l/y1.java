package b.i.a.f.h.l;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.StrictMode;
import android.util.Log;
import androidx.annotation.GuardedBy;
import androidx.collection.ArrayMap;
import b.i.a.f.e.o.f;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class y1 implements b2 {
    @GuardedBy("ConfigurationContentLoader.class")
    public static final Map<Uri, y1> a = new ArrayMap();

    /* renamed from: b  reason: collision with root package name */
    public static final String[] f1499b = {"key", "value"};
    public final ContentResolver c;
    public final Uri d;
    public final ContentObserver e;
    public volatile Map<String, String> g;
    public final Object f = new Object();
    @GuardedBy("this")
    public final List<z1> h = new ArrayList();

    public y1(ContentResolver contentResolver, Uri uri) {
        a2 a2Var = new a2(this);
        this.e = a2Var;
        Objects.requireNonNull(contentResolver);
        Objects.requireNonNull(uri);
        this.c = contentResolver;
        this.d = uri;
        contentResolver.registerContentObserver(uri, false, a2Var);
    }

    public static y1 a(ContentResolver contentResolver, Uri uri) {
        y1 y1Var;
        synchronized (y1.class) {
            Map<Uri, y1> map = a;
            y1Var = map.get(uri);
            if (y1Var == null) {
                try {
                    y1 y1Var2 = new y1(contentResolver, uri);
                    try {
                        map.put(uri, y1Var2);
                    } catch (SecurityException unused) {
                    }
                    y1Var = y1Var2;
                } catch (SecurityException unused2) {
                }
            }
        }
        return y1Var;
    }

    public static synchronized void c() {
        synchronized (y1.class) {
            for (y1 y1Var : a.values()) {
                y1Var.c.unregisterContentObserver(y1Var.e);
            }
            a.clear();
        }
    }

    public final Map<String, String> b() {
        Map<String, String> map;
        Map<String, String> map2 = this.g;
        if (map2 == null) {
            synchronized (this.f) {
                map2 = this.g;
                if (map2 == null) {
                    StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
                    try {
                        map = (Map) f.T1(new d2(this) { // from class: b.i.a.f.h.l.x1
                            public final y1 a;

                            {
                                this.a = this;
                            }

                            /* JADX WARN: Finally extract failed */
                            @Override // b.i.a.f.h.l.d2
                            public final Object a() {
                                Map map3;
                                y1 y1Var = this.a;
                                Cursor query = y1Var.c.query(y1Var.d, y1.f1499b, null, null, null);
                                if (query == null) {
                                    return Collections.emptyMap();
                                }
                                try {
                                    int count = query.getCount();
                                    if (count == 0) {
                                        Map emptyMap = Collections.emptyMap();
                                        query.close();
                                        return emptyMap;
                                    }
                                    if (count <= 256) {
                                        map3 = new ArrayMap(count);
                                    } else {
                                        map3 = new HashMap(count, 1.0f);
                                    }
                                    while (query.moveToNext()) {
                                        map3.put(query.getString(0), query.getString(1));
                                    }
                                    query.close();
                                    return map3;
                                } catch (Throwable th) {
                                    query.close();
                                    throw th;
                                }
                            }
                        });
                        StrictMode.setThreadPolicy(allowThreadDiskReads);
                    } catch (SQLiteException | IllegalStateException | SecurityException unused) {
                        Log.e("ConfigurationContentLoader", "PhenotypeFlag unable to load ContentProvider, using default values");
                        StrictMode.setThreadPolicy(allowThreadDiskReads);
                        map = null;
                    }
                    this.g = map;
                    map2 = map;
                }
            }
        }
        return map2 != null ? map2 : Collections.emptyMap();
    }

    @Override // b.i.a.f.h.l.b2
    public final /* synthetic */ Object g(String str) {
        return b().get(str);
    }
}
