package b.i.a.f.h.l;

import android.content.Context;
import android.os.Bundle;
import b.i.a.f.h.l.g;
/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@18.0.0 */
/* loaded from: classes3.dex */
public final class j extends g.a {
    public final /* synthetic */ String n;
    public final /* synthetic */ String o;
    public final /* synthetic */ Context p;
    public final /* synthetic */ Bundle q;
    public final /* synthetic */ g r;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public j(g gVar, String str, String str2, Context context, Bundle bundle) {
        super(true);
        this.r = gVar;
        this.n = str;
        this.o = str2;
        this.p = context;
        this.q = bundle;
    }

    /* JADX WARN: Can't wrap try/catch for region: R(13:45|3|(1:5)(1:6)|7|(8:11|13|14|43|(1:16)(1:17)|18|21|(2:23|24)(4:25|(3:27|(1:29)(1:30)|31)(3:(1:33)|(1:35)(1:36)|37)|38|39))|12|13|14|43|(0)(0)|18|21|(0)(0)) */
    /* JADX WARN: Code restructure failed: missing block: B:19:0x004e, code lost:
        r5 = move-exception;
     */
    /* JADX WARN: Code restructure failed: missing block: B:20:0x004f, code lost:
        r4.b(r5, true, false);
     */
    /* JADX WARN: Removed duplicated region for block: B:16:0x003a A[Catch: LoadingException -> 0x004e, Exception -> 0x00a4, TRY_ENTER, TryCatch #0 {LoadingException -> 0x004e, blocks: (B:16:0x003a, B:17:0x003d, B:18:0x003f), top: B:43:0x0038, outer: #1 }] */
    /* JADX WARN: Removed duplicated region for block: B:17:0x003d A[Catch: LoadingException -> 0x004e, Exception -> 0x00a4, TryCatch #0 {LoadingException -> 0x004e, blocks: (B:16:0x003a, B:17:0x003d, B:18:0x003f), top: B:43:0x0038, outer: #1 }] */
    /* JADX WARN: Removed duplicated region for block: B:23:0x005a A[Catch: Exception -> 0x00a4, TryCatch #1 {Exception -> 0x00a4, blocks: (B:3:0x0002, B:5:0x000d, B:7:0x001c, B:13:0x002f, B:16:0x003a, B:17:0x003d, B:18:0x003f, B:20:0x004f, B:21:0x0052, B:23:0x005a, B:25:0x0064, B:27:0x0072, B:38:0x0087), top: B:45:0x0002, inners: #0 }] */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0064 A[Catch: Exception -> 0x00a4, TryCatch #1 {Exception -> 0x00a4, blocks: (B:3:0x0002, B:5:0x000d, B:7:0x001c, B:13:0x002f, B:16:0x003a, B:17:0x003d, B:18:0x003f, B:20:0x004f, B:21:0x0052, B:23:0x005a, B:25:0x0064, B:27:0x0072, B:38:0x0087), top: B:45:0x0002, inners: #0 }] */
    @Override // b.i.a.f.h.l.g.a
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void a() {
        /*
            r14 = this;
            r0 = 0
            r1 = 1
            java.lang.String r2 = r14.n     // Catch: java.lang.Exception -> La4
            java.lang.String r3 = r14.o     // Catch: java.lang.Exception -> La4
            boolean r2 = b.i.a.f.h.l.g.e(r2, r3)     // Catch: java.lang.Exception -> La4
            r3 = 0
            if (r2 == 0) goto L19
            java.lang.String r2 = r14.o     // Catch: java.lang.Exception -> La4
            java.lang.String r4 = r14.n     // Catch: java.lang.Exception -> La4
            b.i.a.f.h.l.g r5 = r14.r     // Catch: java.lang.Exception -> La4
            java.lang.String r5 = r5.c     // Catch: java.lang.Exception -> La4
            r11 = r2
            r10 = r4
            r9 = r5
            goto L1c
        L19:
            r9 = r3
            r10 = r9
            r11 = r10
        L1c:
            android.content.Context r2 = r14.p     // Catch: java.lang.Exception -> La4
            b.i.a.f.h.l.g.d(r2)     // Catch: java.lang.Exception -> La4
            java.lang.Boolean r2 = b.i.a.f.h.l.g.f1438b     // Catch: java.lang.Exception -> La4
            boolean r2 = r2.booleanValue()     // Catch: java.lang.Exception -> La4
            if (r2 != 0) goto L2e
            if (r10 == 0) goto L2c
            goto L2e
        L2c:
            r2 = 0
            goto L2f
        L2e:
            r2 = 1
        L2f:
            b.i.a.f.h.l.g r4 = r14.r     // Catch: java.lang.Exception -> La4
            android.content.Context r5 = r14.p     // Catch: java.lang.Exception -> La4
            java.util.Objects.requireNonNull(r4)     // Catch: java.lang.Exception -> La4
            java.lang.String r6 = "com.google.android.gms.measurement.dynamite"
            if (r2 == 0) goto L3d
            com.google.android.gms.dynamite.DynamiteModule$a r7 = com.google.android.gms.dynamite.DynamiteModule.c     // Catch: com.google.android.gms.dynamite.DynamiteModule.LoadingException -> L4e java.lang.Exception -> La4
            goto L3f
        L3d:
            com.google.android.gms.dynamite.DynamiteModule$a r7 = com.google.android.gms.dynamite.DynamiteModule.a     // Catch: com.google.android.gms.dynamite.DynamiteModule.LoadingException -> L4e java.lang.Exception -> La4
        L3f:
            com.google.android.gms.dynamite.DynamiteModule r5 = com.google.android.gms.dynamite.DynamiteModule.c(r5, r7, r6)     // Catch: com.google.android.gms.dynamite.DynamiteModule.LoadingException -> L4e java.lang.Exception -> La4
            java.lang.String r7 = "com.google.android.gms.measurement.internal.AppMeasurementDynamiteService"
            android.os.IBinder r5 = r5.b(r7)     // Catch: com.google.android.gms.dynamite.DynamiteModule.LoadingException -> L4e java.lang.Exception -> La4
            b.i.a.f.h.l.ec r3 = b.i.a.f.h.l.dc.asInterface(r5)     // Catch: com.google.android.gms.dynamite.DynamiteModule.LoadingException -> L4e java.lang.Exception -> La4
            goto L52
        L4e:
            r5 = move-exception
            r4.b(r5, r1, r0)     // Catch: java.lang.Exception -> La4
        L52:
            r4.j = r3     // Catch: java.lang.Exception -> La4
            b.i.a.f.h.l.g r3 = r14.r     // Catch: java.lang.Exception -> La4
            b.i.a.f.h.l.ec r3 = r3.j     // Catch: java.lang.Exception -> La4
            if (r3 != 0) goto L64
            b.i.a.f.h.l.g r2 = r14.r     // Catch: java.lang.Exception -> La4
            java.lang.String r2 = r2.c     // Catch: java.lang.Exception -> La4
            java.lang.String r3 = "Failed to connect to measurement client."
            android.util.Log.w(r2, r3)     // Catch: java.lang.Exception -> La4
            return
        L64:
            android.content.Context r3 = r14.p     // Catch: java.lang.Exception -> La4
            int r3 = com.google.android.gms.dynamite.DynamiteModule.a(r3, r6)     // Catch: java.lang.Exception -> La4
            android.content.Context r4 = r14.p     // Catch: java.lang.Exception -> La4
            int r4 = com.google.android.gms.dynamite.DynamiteModule.d(r4, r6, r0)     // Catch: java.lang.Exception -> La4
            if (r2 == 0) goto L7d
            int r2 = java.lang.Math.max(r3, r4)     // Catch: java.lang.Exception -> La4
            if (r4 >= r3) goto L7a
            r3 = 1
            goto L7b
        L7a:
            r3 = 0
        L7b:
            r8 = r3
            goto L87
        L7d:
            if (r3 <= 0) goto L80
            r4 = r3
        L80:
            if (r3 <= 0) goto L84
            r2 = 1
            goto L85
        L84:
            r2 = 0
        L85:
            r8 = r2
            r2 = r4
        L87:
            com.google.android.gms.internal.measurement.zzae r13 = new com.google.android.gms.internal.measurement.zzae     // Catch: java.lang.Exception -> La4
            r4 = 33025(0x8101, double:1.63165E-319)
            long r6 = (long) r2     // Catch: java.lang.Exception -> La4
            android.os.Bundle r12 = r14.q     // Catch: java.lang.Exception -> La4
            r3 = r13
            r3.<init>(r4, r6, r8, r9, r10, r11, r12)     // Catch: java.lang.Exception -> La4
            b.i.a.f.h.l.g r2 = r14.r     // Catch: java.lang.Exception -> La4
            b.i.a.f.h.l.ec r2 = r2.j     // Catch: java.lang.Exception -> La4
            android.content.Context r3 = r14.p     // Catch: java.lang.Exception -> La4
            b.i.a.f.f.b r4 = new b.i.a.f.f.b     // Catch: java.lang.Exception -> La4
            r4.<init>(r3)     // Catch: java.lang.Exception -> La4
            long r5 = r14.j     // Catch: java.lang.Exception -> La4
            r2.initialize(r4, r13, r5)     // Catch: java.lang.Exception -> La4
            return
        La4:
            r2 = move-exception
            b.i.a.f.h.l.g r3 = r14.r
            r3.b(r2, r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.h.l.j.a():void");
    }
}
