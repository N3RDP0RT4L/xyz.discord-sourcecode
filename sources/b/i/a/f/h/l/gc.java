package b.i.a.f.h.l;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import b.i.a.f.f.a;
import com.google.android.gms.internal.measurement.zzae;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class gc extends a implements ec {
    public gc(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.api.internal.IAppMeasurementDynamiteService");
    }

    @Override // b.i.a.f.h.l.ec
    public final void beginAdUnitExposure(String str, long j) throws RemoteException {
        Parcel c = c();
        c.writeString(str);
        c.writeLong(j);
        i(23, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void clearConditionalUserProperty(String str, String str2, Bundle bundle) throws RemoteException {
        Parcel c = c();
        c.writeString(str);
        c.writeString(str2);
        v.c(c, bundle);
        i(9, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void endAdUnitExposure(String str, long j) throws RemoteException {
        Parcel c = c();
        c.writeString(str);
        c.writeLong(j);
        i(24, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void generateEventId(fc fcVar) throws RemoteException {
        Parcel c = c();
        v.b(c, fcVar);
        i(22, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void getCachedAppInstanceId(fc fcVar) throws RemoteException {
        Parcel c = c();
        v.b(c, fcVar);
        i(19, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void getConditionalUserProperties(String str, String str2, fc fcVar) throws RemoteException {
        Parcel c = c();
        c.writeString(str);
        c.writeString(str2);
        v.b(c, fcVar);
        i(10, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void getCurrentScreenClass(fc fcVar) throws RemoteException {
        Parcel c = c();
        v.b(c, fcVar);
        i(17, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void getCurrentScreenName(fc fcVar) throws RemoteException {
        Parcel c = c();
        v.b(c, fcVar);
        i(16, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void getGmpAppId(fc fcVar) throws RemoteException {
        Parcel c = c();
        v.b(c, fcVar);
        i(21, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void getMaxUserProperties(String str, fc fcVar) throws RemoteException {
        Parcel c = c();
        c.writeString(str);
        v.b(c, fcVar);
        i(6, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void getUserProperties(String str, String str2, boolean z2, fc fcVar) throws RemoteException {
        Parcel c = c();
        c.writeString(str);
        c.writeString(str2);
        ClassLoader classLoader = v.a;
        c.writeInt(z2 ? 1 : 0);
        v.b(c, fcVar);
        i(5, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void initialize(a aVar, zzae zzaeVar, long j) throws RemoteException {
        Parcel c = c();
        v.b(c, aVar);
        v.c(c, zzaeVar);
        c.writeLong(j);
        i(1, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void logEvent(String str, String str2, Bundle bundle, boolean z2, boolean z3, long j) throws RemoteException {
        Parcel c = c();
        c.writeString(str);
        c.writeString(str2);
        v.c(c, bundle);
        c.writeInt(z2 ? 1 : 0);
        c.writeInt(z3 ? 1 : 0);
        c.writeLong(j);
        i(2, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void logHealthData(int i, String str, a aVar, a aVar2, a aVar3) throws RemoteException {
        Parcel c = c();
        c.writeInt(i);
        c.writeString(str);
        v.b(c, aVar);
        v.b(c, aVar2);
        v.b(c, aVar3);
        i(33, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void onActivityCreated(a aVar, Bundle bundle, long j) throws RemoteException {
        Parcel c = c();
        v.b(c, aVar);
        v.c(c, bundle);
        c.writeLong(j);
        i(27, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void onActivityDestroyed(a aVar, long j) throws RemoteException {
        Parcel c = c();
        v.b(c, aVar);
        c.writeLong(j);
        i(28, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void onActivityPaused(a aVar, long j) throws RemoteException {
        Parcel c = c();
        v.b(c, aVar);
        c.writeLong(j);
        i(29, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void onActivityResumed(a aVar, long j) throws RemoteException {
        Parcel c = c();
        v.b(c, aVar);
        c.writeLong(j);
        i(30, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void onActivitySaveInstanceState(a aVar, fc fcVar, long j) throws RemoteException {
        Parcel c = c();
        v.b(c, aVar);
        v.b(c, fcVar);
        c.writeLong(j);
        i(31, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void onActivityStarted(a aVar, long j) throws RemoteException {
        Parcel c = c();
        v.b(c, aVar);
        c.writeLong(j);
        i(25, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void onActivityStopped(a aVar, long j) throws RemoteException {
        Parcel c = c();
        v.b(c, aVar);
        c.writeLong(j);
        i(26, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void registerOnMeasurementEventListener(c cVar) throws RemoteException {
        Parcel c = c();
        v.b(c, cVar);
        i(35, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void setConditionalUserProperty(Bundle bundle, long j) throws RemoteException {
        Parcel c = c();
        v.c(c, bundle);
        c.writeLong(j);
        i(8, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void setCurrentScreen(a aVar, String str, String str2, long j) throws RemoteException {
        Parcel c = c();
        v.b(c, aVar);
        c.writeString(str);
        c.writeString(str2);
        c.writeLong(j);
        i(15, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void setDataCollectionEnabled(boolean z2) throws RemoteException {
        Parcel c = c();
        ClassLoader classLoader = v.a;
        c.writeInt(z2 ? 1 : 0);
        i(39, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void setUserId(String str, long j) throws RemoteException {
        Parcel c = c();
        c.writeString(str);
        c.writeLong(j);
        i(7, c);
    }

    @Override // b.i.a.f.h.l.ec
    public final void setUserProperty(String str, String str2, a aVar, boolean z2, long j) throws RemoteException {
        Parcel c = c();
        c.writeString(str);
        c.writeString(str2);
        v.b(c, aVar);
        c.writeInt(z2 ? 1 : 0);
        c.writeLong(j);
        i(4, c);
    }
}
