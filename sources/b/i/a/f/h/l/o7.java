package b.i.a.f.h.l;

import b.d.b.a.a;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class o7 extends IllegalArgumentException {
    public o7(int i, int i2) {
        super(a.g(54, "Unpaired surrogate at index ", i, " of ", i2));
    }
}
