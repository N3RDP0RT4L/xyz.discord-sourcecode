package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class z9 implements w9 {
    public static final l2<Boolean> a;

    /* renamed from: b  reason: collision with root package name */
    public static final l2<Boolean> f1502b;

    static {
        q2 q2Var = new q2(i2.a("com.google.android.gms.measurement"));
        a = q2Var.c("measurement.collection.efficient_engagement_reporting_enabled_2", true);
        f1502b = q2Var.c("measurement.collection.redundant_engagement_removal_enabled", false);
        q2Var.a("measurement.id.collection.redundant_engagement_removal_enabled", 0L);
    }

    @Override // b.i.a.f.h.l.w9
    public final boolean a() {
        return a.d().booleanValue();
    }

    @Override // b.i.a.f.h.l.w9
    public final boolean b() {
        return f1502b.d().booleanValue();
    }
}
