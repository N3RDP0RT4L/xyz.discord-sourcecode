package b.i.a.f.h.l;

import android.os.Bundle;
import java.util.concurrent.atomic.AtomicReference;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class cc extends ic {
    public final AtomicReference<Bundle> a = new AtomicReference<>();

    /* renamed from: b  reason: collision with root package name */
    public boolean f1435b;

    /* JADX WARN: Code restructure failed: missing block: B:3:0x0002, code lost:
        r4 = r4.get("r");
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static <T> T g(android.os.Bundle r4, java.lang.Class<T> r5) {
        /*
            if (r4 == 0) goto L37
            java.lang.String r0 = "r"
            java.lang.Object r4 = r4.get(r0)
            if (r4 == 0) goto L37
            java.lang.Object r4 = r5.cast(r4)     // Catch: java.lang.ClassCastException -> Lf
            return r4
        Lf:
            r0 = move-exception
            java.lang.String r5 = r5.getCanonicalName()
            java.lang.Class r4 = r4.getClass()
            java.lang.String r4 = r4.getCanonicalName()
            java.lang.String r1 = "Unexpected object type. Expected, Received"
            java.lang.String r2 = ": %s, %s"
            java.lang.String r1 = r1.concat(r2)
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            r2[r3] = r5
            r5 = 1
            r2[r5] = r4
            java.lang.String r4 = java.lang.String.format(r1, r2)
            java.lang.String r5 = "AM"
            android.util.Log.w(r5, r4, r0)
            throw r0
        L37:
            r4 = 0
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.h.l.cc.g(android.os.Bundle, java.lang.Class):java.lang.Object");
    }

    @Override // b.i.a.f.h.l.fc
    public final void f(Bundle bundle) {
        synchronized (this.a) {
            this.a.set(bundle);
            this.f1435b = true;
            this.a.notify();
        }
    }

    public final String i(long j) {
        return (String) g(t0(j), String.class);
    }

    public final Bundle t0(long j) {
        Bundle bundle;
        synchronized (this.a) {
            if (!this.f1435b) {
                try {
                    this.a.wait(j);
                } catch (InterruptedException unused) {
                    return null;
                }
            }
            bundle = this.a.get();
        }
        return bundle;
    }
}
