package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class n9 implements k9 {
    public static final l2<Boolean> a = new q2(i2.a("com.google.android.gms.measurement")).c("measurement.service.database_return_empty_collection", true);

    @Override // b.i.a.f.h.l.k9
    public final boolean a() {
        return true;
    }

    @Override // b.i.a.f.h.l.k9
    public final boolean b() {
        return a.d().booleanValue();
    }
}
