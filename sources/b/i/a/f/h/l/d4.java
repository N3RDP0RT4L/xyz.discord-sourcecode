package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class d4 {
    public static long a(long j) {
        return (-(j & 1)) ^ (j >>> 1);
    }

    public static int b(int i) {
        return (-(i & 1)) ^ (i >>> 1);
    }
}
