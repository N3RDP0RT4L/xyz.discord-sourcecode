package b.i.a.f.h.l;

import java.lang.reflect.Field;
import java.nio.Buffer;
import java.security.AccessController;
import sun.misc.Unsafe;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class j7 {
    public static final Unsafe a;

    /* renamed from: b  reason: collision with root package name */
    public static final Class<?> f1448b = q3.a;
    public static final boolean c;
    public static final boolean d;
    public static final c e;
    public static final boolean f;
    public static final boolean g;
    public static final long h;
    public static final boolean i;

    /* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
    /* loaded from: classes3.dex */
    public static final class a extends c {
        public a(Unsafe unsafe) {
            super(unsafe);
        }

        @Override // b.i.a.f.h.l.j7.c
        public final byte a(Object obj, long j) {
            if (j7.i) {
                return j7.t(obj, j);
            }
            return j7.u(obj, j);
        }

        @Override // b.i.a.f.h.l.j7.c
        public final void b(Object obj, long j, byte b2) {
            if (j7.i) {
                j7.l(obj, j, b2);
            } else {
                j7.o(obj, j, b2);
            }
        }

        @Override // b.i.a.f.h.l.j7.c
        public final void c(Object obj, long j, double d) {
            f(obj, j, Double.doubleToLongBits(d));
        }

        @Override // b.i.a.f.h.l.j7.c
        public final void d(Object obj, long j, float f) {
            e(obj, j, Float.floatToIntBits(f));
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // b.i.a.f.h.l.j7.c
        public final void g(Object obj, long j, boolean z2) {
            if (j7.i) {
                j7.l(obj, j, z2 ? (byte) 1 : (byte) 0);
            } else {
                j7.o(obj, j, z2 ? (byte) 1 : (byte) 0);
            }
        }

        @Override // b.i.a.f.h.l.j7.c
        public final boolean h(Object obj, long j) {
            return j7.i ? j7.t(obj, j) != 0 : j7.u(obj, j) != 0;
        }

        @Override // b.i.a.f.h.l.j7.c
        public final float i(Object obj, long j) {
            return Float.intBitsToFloat(k(obj, j));
        }

        @Override // b.i.a.f.h.l.j7.c
        public final double j(Object obj, long j) {
            return Double.longBitsToDouble(l(obj, j));
        }
    }

    /* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
    /* loaded from: classes3.dex */
    public static final class b extends c {
        public b(Unsafe unsafe) {
            super(unsafe);
        }

        @Override // b.i.a.f.h.l.j7.c
        public final byte a(Object obj, long j) {
            if (j7.i) {
                return j7.t(obj, j);
            }
            return j7.u(obj, j);
        }

        @Override // b.i.a.f.h.l.j7.c
        public final void b(Object obj, long j, byte b2) {
            if (j7.i) {
                j7.l(obj, j, b2);
            } else {
                j7.o(obj, j, b2);
            }
        }

        @Override // b.i.a.f.h.l.j7.c
        public final void c(Object obj, long j, double d) {
            f(obj, j, Double.doubleToLongBits(d));
        }

        @Override // b.i.a.f.h.l.j7.c
        public final void d(Object obj, long j, float f) {
            e(obj, j, Float.floatToIntBits(f));
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // b.i.a.f.h.l.j7.c
        public final void g(Object obj, long j, boolean z2) {
            if (j7.i) {
                j7.l(obj, j, z2 ? (byte) 1 : (byte) 0);
            } else {
                j7.o(obj, j, z2 ? (byte) 1 : (byte) 0);
            }
        }

        @Override // b.i.a.f.h.l.j7.c
        public final boolean h(Object obj, long j) {
            return j7.i ? j7.t(obj, j) != 0 : j7.u(obj, j) != 0;
        }

        @Override // b.i.a.f.h.l.j7.c
        public final float i(Object obj, long j) {
            return Float.intBitsToFloat(k(obj, j));
        }

        @Override // b.i.a.f.h.l.j7.c
        public final double j(Object obj, long j) {
            return Double.longBitsToDouble(l(obj, j));
        }
    }

    /* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
    /* loaded from: classes3.dex */
    public static abstract class c {
        public Unsafe a;

        public c(Unsafe unsafe) {
            this.a = unsafe;
        }

        public abstract byte a(Object obj, long j);

        public abstract void b(Object obj, long j, byte b2);

        public abstract void c(Object obj, long j, double d);

        public abstract void d(Object obj, long j, float f);

        public final void e(Object obj, long j, int i) {
            this.a.putInt(obj, j, i);
        }

        public final void f(Object obj, long j, long j2) {
            this.a.putLong(obj, j, j2);
        }

        public abstract void g(Object obj, long j, boolean z2);

        public abstract boolean h(Object obj, long j);

        public abstract float i(Object obj, long j);

        public abstract double j(Object obj, long j);

        public final int k(Object obj, long j) {
            return this.a.getInt(obj, j);
        }

        public final long l(Object obj, long j) {
            return this.a.getLong(obj, j);
        }
    }

    /* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
    /* loaded from: classes3.dex */
    public static final class d extends c {
        public d(Unsafe unsafe) {
            super(unsafe);
        }

        @Override // b.i.a.f.h.l.j7.c
        public final byte a(Object obj, long j) {
            return this.a.getByte(obj, j);
        }

        @Override // b.i.a.f.h.l.j7.c
        public final void b(Object obj, long j, byte b2) {
            this.a.putByte(obj, j, b2);
        }

        @Override // b.i.a.f.h.l.j7.c
        public final void c(Object obj, long j, double d) {
            this.a.putDouble(obj, j, d);
        }

        @Override // b.i.a.f.h.l.j7.c
        public final void d(Object obj, long j, float f) {
            this.a.putFloat(obj, j, f);
        }

        @Override // b.i.a.f.h.l.j7.c
        public final void g(Object obj, long j, boolean z2) {
            this.a.putBoolean(obj, j, z2);
        }

        @Override // b.i.a.f.h.l.j7.c
        public final boolean h(Object obj, long j) {
            return this.a.getBoolean(obj, j);
        }

        @Override // b.i.a.f.h.l.j7.c
        public final float i(Object obj, long j) {
            return this.a.getFloat(obj, j);
        }

        @Override // b.i.a.f.h.l.j7.c
        public final double j(Object obj, long j) {
            return this.a.getDouble(obj, j);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:31:0x0120  */
    /* JADX WARN: Removed duplicated region for block: B:57:0x026f  */
    /* JADX WARN: Removed duplicated region for block: B:62:0x0282  */
    /* JADX WARN: Removed duplicated region for block: B:67:0x0122 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    static {
        /*
            Method dump skipped, instructions count: 646
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.h.l.j7.<clinit>():void");
    }

    public static byte a(byte[] bArr, long j) {
        return e.a(bArr, h + j);
    }

    public static int b(Object obj, long j) {
        return e.k(obj, j);
    }

    public static <T> T c(Class<T> cls) {
        try {
            return (T) a.allocateInstance(cls);
        } catch (InstantiationException e2) {
            throw new IllegalStateException(e2);
        }
    }

    public static void d(Object obj, long j, double d2) {
        e.c(obj, j, d2);
    }

    public static void e(Object obj, long j, long j2) {
        e.f(obj, j, j2);
    }

    public static void f(Object obj, long j, Object obj2) {
        e.a.putObject(obj, j, obj2);
    }

    public static void g(byte[] bArr, long j, byte b2) {
        e.b(bArr, h + j, b2);
    }

    public static int h(Class<?> cls) {
        if (g) {
            return e.a.arrayBaseOffset(cls);
        }
        return -1;
    }

    public static long i(Object obj, long j) {
        return e.l(obj, j);
    }

    public static int j(Class<?> cls) {
        if (g) {
            return e.a.arrayIndexScale(cls);
        }
        return -1;
    }

    public static Unsafe k() {
        try {
            return (Unsafe) AccessController.doPrivileged(new i7());
        } catch (Throwable unused) {
            return null;
        }
    }

    public static void l(Object obj, long j, byte b2) {
        long j2 = (-4) & j;
        int b3 = b(obj, j2);
        int i2 = ((~((int) j)) & 3) << 3;
        c cVar = e;
        cVar.e(obj, j2, ((255 & b2) << i2) | (b3 & (~(255 << i2))));
    }

    public static boolean m(Object obj, long j) {
        return e.h(obj, j);
    }

    public static float n(Object obj, long j) {
        return e.i(obj, j);
    }

    public static void o(Object obj, long j, byte b2) {
        long j2 = (-4) & j;
        int i2 = (((int) j) & 3) << 3;
        c cVar = e;
        cVar.e(obj, j2, ((255 & b2) << i2) | (b(obj, j2) & (~(255 << i2))));
    }

    public static boolean p(Class<?> cls) {
        if (!q3.a()) {
            return false;
        }
        try {
            Class<?> cls2 = f1448b;
            Class<?> cls3 = Boolean.TYPE;
            cls2.getMethod("peekLong", cls, cls3);
            cls2.getMethod("pokeLong", cls, Long.TYPE, cls3);
            Class<?> cls4 = Integer.TYPE;
            cls2.getMethod("pokeInt", cls, cls4, cls3);
            cls2.getMethod("peekInt", cls, cls3);
            cls2.getMethod("pokeByte", cls, Byte.TYPE);
            cls2.getMethod("peekByte", cls);
            cls2.getMethod("pokeByteArray", cls, byte[].class, cls4, cls4);
            cls2.getMethod("peekByteArray", cls, byte[].class, cls4, cls4);
            return true;
        } catch (Throwable unused) {
            return false;
        }
    }

    public static double q(Object obj, long j) {
        return e.j(obj, j);
    }

    public static Object r(Object obj, long j) {
        return e.a.getObject(obj, j);
    }

    public static Field s() {
        Field field;
        Field field2;
        if (q3.a()) {
            try {
                field2 = Buffer.class.getDeclaredField("effectiveDirectAddress");
            } catch (Throwable unused) {
                field2 = null;
            }
            if (field2 != null) {
                return field2;
            }
        }
        try {
            field = Buffer.class.getDeclaredField("address");
        } catch (Throwable unused2) {
            field = null;
        }
        if (field == null || field.getType() != Long.TYPE) {
            return null;
        }
        return field;
    }

    public static byte t(Object obj, long j) {
        return (byte) (b(obj, (-4) & j) >>> ((int) (((~j) & 3) << 3)));
    }

    public static byte u(Object obj, long j) {
        return (byte) (b(obj, (-4) & j) >>> ((int) ((j & 3) << 3)));
    }
}
