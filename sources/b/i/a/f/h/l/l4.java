package b.i.a.f.h.l;

import b.i.a.f.h.l.h4;
import b.i.a.f.h.l.u4;
import java.io.IOException;
import java.util.Map;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class l4 extends j4<u4.c> {
    @Override // b.i.a.f.h.l.j4
    public final int a(Map.Entry<?, ?> entry) {
        u4.c cVar = (u4.c) entry.getKey();
        throw new NoSuchMethodError();
    }

    @Override // b.i.a.f.h.l.j4
    public final n4<u4.c> b(Object obj) {
        return ((u4.d) obj).zzc;
    }

    @Override // b.i.a.f.h.l.j4
    public final Object c(h4 h4Var, c6 c6Var, int i) {
        return h4Var.d.get(new h4.a(c6Var, i));
    }

    @Override // b.i.a.f.h.l.j4
    public final void d(v7 v7Var, Map.Entry<?, ?> entry) throws IOException {
        u4.c cVar = (u4.c) entry.getKey();
        throw new NoSuchMethodError();
    }

    @Override // b.i.a.f.h.l.j4
    public final boolean e(c6 c6Var) {
        return c6Var instanceof u4.d;
    }

    @Override // b.i.a.f.h.l.j4
    public final n4<u4.c> f(Object obj) {
        return ((u4.d) obj).u();
    }

    @Override // b.i.a.f.h.l.j4
    public final void g(Object obj) {
        ((u4.d) obj).zzc.h();
    }
}
