package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class o6 implements a6 {
    public final c6 a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1473b;
    public final Object[] c;
    public final int d;

    public o6(c6 c6Var, String str, Object[] objArr) {
        this.a = c6Var;
        this.f1473b = str;
        this.c = objArr;
        char charAt = str.charAt(0);
        if (charAt < 55296) {
            this.d = charAt;
            return;
        }
        int i = charAt & 8191;
        int i2 = 13;
        int i3 = 1;
        while (true) {
            i3++;
            char charAt2 = str.charAt(i3);
            if (charAt2 >= 55296) {
                i |= (charAt2 & 8191) << i2;
                i2 += 13;
            } else {
                this.d = i | (charAt2 << i2);
                return;
            }
        }
    }

    @Override // b.i.a.f.h.l.a6
    public final int a() {
        return (this.d & 1) == 1 ? 1 : 2;
    }

    @Override // b.i.a.f.h.l.a6
    public final boolean b() {
        return (this.d & 2) == 2;
    }

    @Override // b.i.a.f.h.l.a6
    public final c6 c() {
        return this.a;
    }
}
