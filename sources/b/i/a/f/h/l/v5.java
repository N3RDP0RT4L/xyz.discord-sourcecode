package b.i.a.f.h.l;

import java.util.Map;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public interface v5 {
    Map<?, ?> b(Object obj);

    Object c(Object obj);

    boolean d(Object obj);

    Map<?, ?> e(Object obj);

    Object f(Object obj, Object obj2);

    t5<?, ?> g(Object obj);

    Object h(Object obj);

    int i(int i, Object obj, Object obj2);
}
