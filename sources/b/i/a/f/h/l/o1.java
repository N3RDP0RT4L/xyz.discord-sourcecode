package b.i.a.f.h.l;

import b.i.a.f.h.l.u4;
/* compiled from: com.google.android.gms:play-services-measurement@@18.0.0 */
/* loaded from: classes3.dex */
public final class o1 extends u4<o1, a> implements d6 {
    private static final o1 zzf;
    private static volatile j6<o1> zzg;
    private int zzc;
    private String zzd = "";
    private b5<q1> zze = m6.k;

    /* compiled from: com.google.android.gms:play-services-measurement@@18.0.0 */
    /* loaded from: classes3.dex */
    public static final class a extends u4.b<o1, a> implements d6 {
        public a(r1 r1Var) {
            super(o1.zzf);
        }
    }

    static {
        o1 o1Var = new o1();
        zzf = o1Var;
        u4.r(o1.class, o1Var);
    }

    @Override // b.i.a.f.h.l.u4
    public final Object p(int i, Object obj, Object obj2) {
        switch (r1.a[i - 1]) {
            case 1:
                return new o1();
            case 2:
                return new a(null);
            case 3:
                return new o6(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001ဈ\u0000\u0002\u001b", new Object[]{"zzc", "zzd", "zze", q1.class});
            case 4:
                return zzf;
            case 5:
                j6<o1> j6Var = zzg;
                if (j6Var == null) {
                    synchronized (o1.class) {
                        j6Var = zzg;
                        if (j6Var == null) {
                            j6Var = new u4.a<>(zzf);
                            zzg = j6Var;
                        }
                    }
                }
                return j6Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
