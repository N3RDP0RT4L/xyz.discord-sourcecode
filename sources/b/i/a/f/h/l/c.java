package b.i.a.f.h.l;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public interface c extends IInterface {
    void Z(String str, String str2, Bundle bundle, long j) throws RemoteException;

    int a() throws RemoteException;
}
