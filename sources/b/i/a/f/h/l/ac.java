package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class ac implements yb {
    public static final l2<Boolean> a = new q2(i2.a("com.google.android.gms.measurement")).c("measurement.upload.file_lock_state_check", false);

    @Override // b.i.a.f.h.l.yb
    public final boolean a() {
        return a.d().booleanValue();
    }
}
