package b.i.a.f.h.l;

import b.d.b.a.a;
import java.io.Serializable;
import java.util.Objects;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class a3<T> implements z2<T>, Serializable {
    public volatile transient boolean j;
    @NullableDecl
    public transient T k;
    private final z2<T> zza;

    public a3(z2<T> z2Var) {
        Objects.requireNonNull(z2Var);
        this.zza = z2Var;
    }

    @Override // b.i.a.f.h.l.z2
    public final T a() {
        if (!this.j) {
            synchronized (this) {
                if (!this.j) {
                    T a = this.zza.a();
                    this.k = a;
                    this.j = true;
                    return a;
                }
            }
        }
        return this.k;
    }

    public final String toString() {
        Object obj;
        if (this.j) {
            String valueOf = String.valueOf(this.k);
            obj = a.j(valueOf.length() + 25, "<supplier that returned ", valueOf, ">");
        } else {
            obj = this.zza;
        }
        String valueOf2 = String.valueOf(obj);
        return a.j(valueOf2.length() + 19, "Suppliers.memoize(", valueOf2, ")");
    }
}
