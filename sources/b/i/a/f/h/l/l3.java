package b.i.a.f.h.l;

import b.d.b.a.a;
import b.i.a.f.h.l.l3;
import b.i.a.f.h.l.n3;
import com.google.android.gms.internal.measurement.zzhi;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public abstract class l3<MessageType extends l3<MessageType, BuilderType>, BuilderType extends n3<MessageType, BuilderType>> implements c6 {
    public int zza = 0;

    public static <T> void c(Iterable<T> iterable, List<? super T> list) {
        Charset charset = w4.a;
        Objects.requireNonNull(iterable);
        if (iterable instanceof j5) {
            List<?> b2 = ((j5) iterable).b();
            j5 j5Var = (j5) list;
            int size = list.size();
            for (Object obj : b2) {
                if (obj == null) {
                    StringBuilder sb = new StringBuilder(37);
                    sb.append("Element at index ");
                    sb.append(j5Var.size() - size);
                    sb.append(" is null.");
                    String sb2 = sb.toString();
                    int size2 = j5Var.size();
                    while (true) {
                        size2--;
                        if (size2 < size) {
                            break;
                        }
                        j5Var.remove(size2);
                    }
                    throw new NullPointerException(sb2);
                } else if (obj instanceof t3) {
                    j5Var.t((t3) obj);
                } else {
                    j5Var.add((String) obj);
                }
            }
        } else if (iterable instanceof l6) {
            list.addAll((Collection) iterable);
        } else {
            if ((list instanceof ArrayList) && (iterable instanceof Collection)) {
                ((ArrayList) list).ensureCapacity(((Collection) iterable).size() + list.size());
            }
            int size3 = list.size();
            for (T t : iterable) {
                if (t == null) {
                    StringBuilder sb3 = new StringBuilder(37);
                    sb3.append("Element at index ");
                    sb3.append(list.size() - size3);
                    sb3.append(" is null.");
                    String sb4 = sb3.toString();
                    int size4 = list.size();
                    while (true) {
                        size4--;
                        if (size4 < size3) {
                            break;
                        }
                        list.remove(size4);
                    }
                    throw new NullPointerException(sb4);
                }
                list.add(t);
            }
        }
    }

    @Override // b.i.a.f.h.l.c6
    public final t3 b() {
        try {
            u4 u4Var = (u4) this;
            int g = u4Var.g();
            t3 t3Var = t3.j;
            byte[] bArr = new byte[g];
            Logger logger = zzhi.a;
            zzhi.a aVar = new zzhi.a(bArr, g);
            u4Var.i(aVar);
            if (aVar.a() == 0) {
                return new z3(bArr);
            }
            throw new IllegalStateException("Did not write as much data as expected.");
        } catch (IOException e) {
            String name = getClass().getName();
            StringBuilder Q = a.Q(name.length() + 62 + 10, "Serializing ", name, " to a ", "ByteString");
            Q.append(" threw an IOException (should never happen).");
            throw new RuntimeException(Q.toString(), e);
        }
    }

    public final byte[] d() {
        try {
            u4 u4Var = (u4) this;
            int g = u4Var.g();
            byte[] bArr = new byte[g];
            Logger logger = zzhi.a;
            zzhi.a aVar = new zzhi.a(bArr, g);
            u4Var.i(aVar);
            if (aVar.a() == 0) {
                return bArr;
            }
            throw new IllegalStateException("Did not write as much data as expected.");
        } catch (IOException e) {
            String name = getClass().getName();
            StringBuilder Q = a.Q(name.length() + 62 + 10, "Serializing ", name, " to a ", "byte array");
            Q.append(" threw an IOException (should never happen).");
            throw new RuntimeException(Q.toString(), e);
        }
    }

    public int e() {
        throw new UnsupportedOperationException();
    }

    public void l(int i) {
        throw new UnsupportedOperationException();
    }
}
