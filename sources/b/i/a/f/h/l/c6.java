package b.i.a.f.h.l;

import com.google.android.gms.internal.measurement.zzhi;
import java.io.IOException;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public interface c6 extends d6 {
    t3 b();

    b6 f();

    int g();

    b6 h();

    void i(zzhi zzhiVar) throws IOException;
}
