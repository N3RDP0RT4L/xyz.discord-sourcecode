package b.i.a.f.h.l;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.Log;
import android.util.Pair;
import androidx.annotation.GuardedBy;
import b.i.a.f.i.b.z5;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@18.0.0 */
/* loaded from: classes3.dex */
public class g {
    public static volatile g a;

    /* renamed from: b  reason: collision with root package name */
    public static Boolean f1438b;
    public final String c;
    public final b.i.a.f.e.o.b d;
    public final ExecutorService e;
    public final b.i.a.f.i.a.a f;
    @GuardedBy("listenerList")
    public final List<Pair<z5, c>> g;
    public int h;
    public boolean i;
    public volatile ec j;

    /* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@18.0.0 */
    /* loaded from: classes3.dex */
    public abstract class a implements Runnable {
        public final long j = System.currentTimeMillis();
        public final long k = SystemClock.elapsedRealtime();
        public final boolean l;

        public a(boolean z2) {
            Objects.requireNonNull((b.i.a.f.e.o.c) g.this.d);
            Objects.requireNonNull((b.i.a.f.e.o.c) g.this.d);
            this.l = z2;
        }

        public abstract void a() throws RemoteException;

        public void b() {
        }

        @Override // java.lang.Runnable
        public void run() {
            if (g.this.i) {
                b();
                return;
            }
            try {
                a();
            } catch (Exception e) {
                g.this.b(e, false, this.l);
                b();
            }
        }
    }

    /* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@18.0.0 */
    /* loaded from: classes3.dex */
    public class b implements Application.ActivityLifecycleCallbacks {
        public b() {
        }

        @Override // android.app.Application.ActivityLifecycleCallbacks
        public final void onActivityCreated(Activity activity, Bundle bundle) {
            g gVar = g.this;
            gVar.e.execute(new e0(this, bundle, activity));
        }

        @Override // android.app.Application.ActivityLifecycleCallbacks
        public final void onActivityDestroyed(Activity activity) {
            g gVar = g.this;
            gVar.e.execute(new j0(this, activity));
        }

        @Override // android.app.Application.ActivityLifecycleCallbacks
        public final void onActivityPaused(Activity activity) {
            g gVar = g.this;
            gVar.e.execute(new f0(this, activity));
        }

        @Override // android.app.Application.ActivityLifecycleCallbacks
        public final void onActivityResumed(Activity activity) {
            g gVar = g.this;
            gVar.e.execute(new g0(this, activity));
        }

        @Override // android.app.Application.ActivityLifecycleCallbacks
        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
            cc ccVar = new cc();
            g gVar = g.this;
            gVar.e.execute(new h0(this, activity, ccVar));
            Bundle t0 = ccVar.t0(50L);
            if (t0 != null) {
                bundle.putAll(t0);
            }
        }

        @Override // android.app.Application.ActivityLifecycleCallbacks
        public final void onActivityStarted(Activity activity) {
            g gVar = g.this;
            gVar.e.execute(new d0(this, activity));
        }

        @Override // android.app.Application.ActivityLifecycleCallbacks
        public final void onActivityStopped(Activity activity) {
            g gVar = g.this;
            gVar.e.execute(new i0(this, activity));
        }
    }

    /* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@18.0.0 */
    /* loaded from: classes3.dex */
    public static class c extends b.i.a.f.h.l.b {
        public final z5 a;

        public c(z5 z5Var) {
            this.a = z5Var;
        }

        @Override // b.i.a.f.h.l.c
        public final void Z(String str, String str2, Bundle bundle, long j) {
            this.a.a(str, str2, bundle, j);
        }

        @Override // b.i.a.f.h.l.c
        public final int a() {
            return System.identityHashCode(this.a);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:21:0x0063  */
    /* JADX WARN: Removed duplicated region for block: B:23:0x006d  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x0053 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public g(android.content.Context r9, java.lang.String r10, java.lang.String r11, java.lang.String r12, android.os.Bundle r13) {
        /*
            r8 = this;
            r8.<init>()
            if (r10 == 0) goto Lf
            boolean r0 = e(r11, r12)
            if (r0 != 0) goto Lc
            goto Lf
        Lc:
            r8.c = r10
            goto L13
        Lf:
            java.lang.String r10 = "FA"
            r8.c = r10
        L13:
            b.i.a.f.e.o.c r10 = b.i.a.f.e.o.c.a
            r8.d = r10
            b.i.a.f.h.l.o r7 = new b.i.a.f.h.l.o
            r7.<init>()
            java.util.concurrent.ThreadPoolExecutor r10 = new java.util.concurrent.ThreadPoolExecutor
            java.util.concurrent.TimeUnit r5 = java.util.concurrent.TimeUnit.SECONDS
            java.util.concurrent.LinkedBlockingQueue r6 = new java.util.concurrent.LinkedBlockingQueue
            r6.<init>()
            r1 = 1
            r2 = 1
            r3 = 60
            r0 = r10
            r0.<init>(r1, r2, r3, r5, r6, r7)
            r0 = 1
            r10.allowCoreThreadTimeOut(r0)
            java.util.concurrent.ExecutorService r10 = java.util.concurrent.Executors.unconfigurableExecutorService(r10)
            r8.e = r10
            b.i.a.f.i.a.a r10 = new b.i.a.f.i.a.a
            r10.<init>(r8)
            r8.f = r10
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            r8.g = r10
            r10 = 0
            java.lang.String r1 = "google_app_id"
            java.lang.String r1 = b.i.a.f.e.o.f.V1(r9, r1)     // Catch: java.lang.IllegalStateException -> L50
            if (r1 == 0) goto L50
            r1 = 1
            goto L51
        L50:
            r1 = 0
        L51:
            if (r1 == 0) goto L60
            java.lang.String r1 = "com.google.firebase.analytics.FirebaseAnalytics"
            java.lang.Class.forName(r1)     // Catch: java.lang.ClassNotFoundException -> L5a
            r1 = 1
            goto L5b
        L5a:
            r1 = 0
        L5b:
            if (r1 == 0) goto L5e
            goto L60
        L5e:
            r1 = 0
            goto L61
        L60:
            r1 = 1
        L61:
            if (r1 != 0) goto L6d
            r8.i = r0
            java.lang.String r9 = r8.c
            java.lang.String r10 = "Disabling data collection. Found google_app_id in strings.xml but Google Analytics for Firebase is missing. Remove this value or add Google Analytics for Firebase to resume data collection."
            android.util.Log.w(r9, r10)
            return
        L6d:
            boolean r1 = e(r11, r12)
            if (r1 != 0) goto L93
            if (r11 == 0) goto L7f
            if (r12 == 0) goto L7f
            java.lang.String r10 = r8.c
            java.lang.String r0 = "Deferring to Google Analytics for Firebase for event data collection. https://goo.gl/J1sWQy"
            android.util.Log.v(r10, r0)
            goto L93
        L7f:
            if (r11 != 0) goto L83
            r1 = 1
            goto L84
        L83:
            r1 = 0
        L84:
            if (r12 != 0) goto L87
            goto L88
        L87:
            r0 = 0
        L88:
            r10 = r1 ^ r0
            if (r10 == 0) goto L93
            java.lang.String r10 = r8.c
            java.lang.String r0 = "Specified origin or custom app id is null. Both parameters will be ignored."
            android.util.Log.w(r10, r0)
        L93:
            b.i.a.f.h.l.j r10 = new b.i.a.f.h.l.j
            r1 = r10
            r2 = r8
            r3 = r11
            r4 = r12
            r5 = r9
            r6 = r13
            r1.<init>(r2, r3, r4, r5, r6)
            java.util.concurrent.ExecutorService r11 = r8.e
            r11.execute(r10)
            android.content.Context r9 = r9.getApplicationContext()
            android.app.Application r9 = (android.app.Application) r9
            if (r9 != 0) goto Lb3
            java.lang.String r9 = r8.c
            java.lang.String r10 = "Unable to register lifecycle notifications. Application null."
            android.util.Log.w(r9, r10)
            return
        Lb3:
            b.i.a.f.h.l.g$b r10 = new b.i.a.f.h.l.g$b
            r10.<init>()
            r9.registerActivityLifecycleCallbacks(r10)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.h.l.g.<init>(android.content.Context, java.lang.String, java.lang.String, java.lang.String, android.os.Bundle):void");
    }

    public static g a(Context context, String str, String str2, String str3, Bundle bundle) {
        Objects.requireNonNull(context, "null reference");
        if (a == null) {
            synchronized (g.class) {
                if (a == null) {
                    a = new g(context, null, null, null, bundle);
                }
            }
        }
        return a;
    }

    /* JADX WARN: Removed duplicated region for block: B:17:0x002c A[Catch: all -> 0x0052, Exception -> 0x0054, TRY_ENTER, TRY_LEAVE, TryCatch #2 {Exception -> 0x0054, blocks: (B:4:0x0003, B:7:0x0009, B:9:0x000f, B:11:0x001f, B:14:0x0024, B:17:0x002c, B:19:0x0032), top: B:30:0x0003, outer: #1 }] */
    /* JADX WARN: Removed duplicated region for block: B:19:0x0032 A[Catch: all -> 0x0052, Exception -> 0x0054, TRY_ENTER, TRY_LEAVE, TryCatch #2 {Exception -> 0x0054, blocks: (B:4:0x0003, B:7:0x0009, B:9:0x000f, B:11:0x001f, B:14:0x0024, B:17:0x002c, B:19:0x0032), top: B:30:0x0003, outer: #1 }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static void d(android.content.Context r6) {
        /*
            java.lang.Class<b.i.a.f.h.l.g> r0 = b.i.a.f.h.l.g.class
            monitor-enter(r0)
            java.lang.Boolean r1 = b.i.a.f.h.l.g.f1438b     // Catch: java.lang.Throwable -> L52 java.lang.Exception -> L54
            if (r1 == 0) goto L9
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L52
            goto L61
        L9:
            java.lang.String r1 = "app_measurement_internal_disable_startup_flags"
            b.c.a.a0.d.w(r1)     // Catch: java.lang.Throwable -> L52 java.lang.Exception -> L54
            r2 = 0
            b.i.a.f.e.p.a r3 = b.i.a.f.e.p.b.a(r6)     // Catch: android.content.pm.PackageManager.NameNotFoundException -> L29 java.lang.Throwable -> L52 java.lang.Exception -> L54
            java.lang.String r4 = r6.getPackageName()     // Catch: android.content.pm.PackageManager.NameNotFoundException -> L29 java.lang.Throwable -> L52 java.lang.Exception -> L54
            r5 = 128(0x80, float:1.794E-43)
            android.content.pm.ApplicationInfo r3 = r3.a(r4, r5)     // Catch: android.content.pm.PackageManager.NameNotFoundException -> L29 java.lang.Throwable -> L52 java.lang.Exception -> L54
            if (r3 == 0) goto L29
            android.os.Bundle r3 = r3.metaData     // Catch: android.content.pm.PackageManager.NameNotFoundException -> L29 java.lang.Throwable -> L52 java.lang.Exception -> L54
            if (r3 != 0) goto L24
            goto L29
        L24:
            boolean r1 = r3.getBoolean(r1)     // Catch: android.content.pm.PackageManager.NameNotFoundException -> L29 java.lang.Throwable -> L52 java.lang.Exception -> L54
            goto L2a
        L29:
            r1 = 0
        L2a:
            if (r1 == 0) goto L32
            java.lang.Boolean r6 = java.lang.Boolean.TRUE     // Catch: java.lang.Throwable -> L52 java.lang.Exception -> L54
            b.i.a.f.h.l.g.f1438b = r6     // Catch: java.lang.Throwable -> L52 java.lang.Exception -> L54
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L52
            goto L61
        L32:
            java.lang.String r1 = "com.google.android.gms.measurement.prefs"
            android.content.SharedPreferences r6 = r6.getSharedPreferences(r1, r2)     // Catch: java.lang.Throwable -> L52 java.lang.Exception -> L54
            java.lang.String r1 = "allow_remote_dynamite"
            r2 = 1
            boolean r1 = r6.getBoolean(r1, r2)     // Catch: java.lang.Throwable -> L52 java.lang.Exception -> L54
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch: java.lang.Throwable -> L52 java.lang.Exception -> L54
            b.i.a.f.h.l.g.f1438b = r1     // Catch: java.lang.Throwable -> L52 java.lang.Exception -> L54
            android.content.SharedPreferences$Editor r6 = r6.edit()     // Catch: java.lang.Throwable -> L52 java.lang.Exception -> L54
            java.lang.String r1 = "allow_remote_dynamite"
            r6.remove(r1)     // Catch: java.lang.Throwable -> L52 java.lang.Exception -> L54
            r6.apply()     // Catch: java.lang.Throwable -> L52 java.lang.Exception -> L54
            goto L60
        L52:
            r6 = move-exception
            goto L62
        L54:
            r6 = move-exception
            java.lang.String r1 = "FA"
            java.lang.String r2 = "Exception reading flag from SharedPreferences."
            android.util.Log.e(r1, r2, r6)     // Catch: java.lang.Throwable -> L52
            java.lang.Boolean r6 = java.lang.Boolean.TRUE     // Catch: java.lang.Throwable -> L52
            b.i.a.f.h.l.g.f1438b = r6     // Catch: java.lang.Throwable -> L52
        L60:
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L52
        L61:
            return
        L62:
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L52
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.h.l.g.d(android.content.Context):void");
    }

    public static boolean e(String str, String str2) {
        boolean z2;
        if (!(str2 == null || str == null)) {
            try {
                Class.forName("com.google.firebase.analytics.FirebaseAnalytics");
                z2 = true;
            } catch (ClassNotFoundException unused) {
                z2 = false;
            }
            if (!z2) {
                return true;
            }
        }
        return false;
    }

    public final void b(Exception exc, boolean z2, boolean z3) {
        this.i |= z2;
        if (z2) {
            Log.w(this.c, "Data collection startup failed. No data will be collected.", exc);
            return;
        }
        if (z3) {
            this.e.execute(new y(this, "Error with data collection. Data lost.", exc));
        }
        Log.w(this.c, "Error with data collection. Data lost.", exc);
    }

    public final void c(String str, String str2, Bundle bundle, boolean z2, boolean z3, Long l) {
        this.e.execute(new c0(this, null, str, str2, bundle, z2, z3));
    }
}
