package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class wb implements xb {
    public static final l2<Boolean> a = new q2(i2.a("com.google.android.gms.measurement")).c("measurement.collection.service.update_with_analytics_fix", false);

    @Override // b.i.a.f.h.l.xb
    public final boolean a() {
        return a.d().booleanValue();
    }
}
