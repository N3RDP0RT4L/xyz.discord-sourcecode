package b.i.a.f.h.l;

import b.i.a.f.h.l.p4;
import java.io.IOException;
import java.util.Map;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public abstract class j4<T extends p4<T>> {
    public abstract int a(Map.Entry<?, ?> entry);

    public abstract n4<T> b(Object obj);

    public abstract Object c(h4 h4Var, c6 c6Var, int i);

    public abstract void d(v7 v7Var, Map.Entry<?, ?> entry) throws IOException;

    public abstract boolean e(c6 c6Var);

    public abstract n4<T> f(Object obj);

    public abstract void g(Object obj);
}
