package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class qb implements rb {
    public static final l2<Boolean> a = new q2(i2.a("com.google.android.gms.measurement")).c("measurement.integration.disable_firebase_instance_id", false);

    @Override // b.i.a.f.h.l.rb
    public final boolean a() {
        return true;
    }

    @Override // b.i.a.f.h.l.rb
    public final boolean b() {
        return a.d().booleanValue();
    }
}
