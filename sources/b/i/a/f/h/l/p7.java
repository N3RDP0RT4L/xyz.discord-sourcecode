package b.i.a.f.h.l;
/* JADX WARN: Init of enum A can be incorrect */
/* JADX WARN: Init of enum l can be incorrect */
/* JADX WARN: Init of enum m can be incorrect */
/* JADX WARN: Init of enum n can be incorrect */
/* JADX WARN: Init of enum o can be incorrect */
/* JADX WARN: Init of enum p can be incorrect */
/* JADX WARN: Init of enum r can be incorrect */
/* JADX WARN: Init of enum s can be incorrect */
/* JADX WARN: Init of enum t can be incorrect */
/* JADX WARN: Init of enum u can be incorrect */
/* JADX WARN: Init of enum v can be incorrect */
/* JADX WARN: Init of enum x can be incorrect */
/* JADX WARN: Init of enum y can be incorrect */
/* JADX WARN: Init of enum z can be incorrect */
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public enum p7 {
    DOUBLE(w7.DOUBLE, 1),
    FLOAT(w7.FLOAT, 5),
    INT64(r5, 0),
    UINT64(r5, 0),
    INT32(r11, 0),
    FIXED64(r5, 1),
    FIXED32(r11, 5),
    BOOL(w7.BOOLEAN, 0),
    STRING { // from class: b.i.a.f.h.l.s7
    },
    GROUP { // from class: b.i.a.f.h.l.r7
    },
    MESSAGE { // from class: b.i.a.f.h.l.u7
    },
    BYTES { // from class: b.i.a.f.h.l.t7
    },
    UINT32(r11, 0),
    ENUM(w7.ENUM, 0),
    SFIXED32(r11, 5),
    SFIXED64(r5, 1),
    SINT32(r11, 0),
    SINT64(r5, 0);
    
    private final w7 zzs;
    private final int zzt;

    static {
        w7 w7Var = w7.LONG;
        w7 w7Var2 = w7.INT;
        final w7 w7Var3 = w7.STRING;
        final w7 w7Var4 = w7.MESSAGE;
        final w7 w7Var5 = w7.BYTE_STRING;
    }

    p7(w7 w7Var, int i) {
        this.zzs = w7Var;
        this.zzt = i;
    }

    public final w7 f() {
        return this.zzs;
    }

    p7(w7 w7Var, int i, q7 q7Var) {
        this.zzs = w7Var;
        this.zzt = i;
    }
}
