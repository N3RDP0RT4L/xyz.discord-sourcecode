package b.i.a.f.h.l;

import b.d.b.a.a;
import com.google.android.gms.internal.measurement.zzhi;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public class z3 extends b4 {
    public final byte[] zzb;

    public z3(byte[] bArr) {
        Objects.requireNonNull(bArr);
        this.zzb = bArr;
    }

    @Override // b.i.a.f.h.l.t3
    public byte c(int i) {
        return this.zzb[i];
    }

    @Override // b.i.a.f.h.l.t3
    public int d() {
        return this.zzb.length;
    }

    @Override // b.i.a.f.h.l.t3
    public final int e(int i, int i2, int i3) {
        byte[] bArr = this.zzb;
        int o = o();
        Charset charset = w4.a;
        for (int i4 = o; i4 < o + i3; i4++) {
            i = (i * 31) + bArr[i4];
        }
        return i;
    }

    @Override // b.i.a.f.h.l.t3
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof t3) || d() != ((t3) obj).d()) {
            return false;
        }
        if (d() == 0) {
            return true;
        }
        if (!(obj instanceof z3)) {
            return obj.equals(this);
        }
        z3 z3Var = (z3) obj;
        int n = n();
        int n2 = z3Var.n();
        if (n != 0 && n2 != 0 && n != n2) {
            return false;
        }
        int d = d();
        if (d > z3Var.d()) {
            int d2 = d();
            StringBuilder sb = new StringBuilder(40);
            sb.append("Length too large: ");
            sb.append(d);
            sb.append(d2);
            throw new IllegalArgumentException(sb.toString());
        } else if (d <= z3Var.d()) {
            byte[] bArr = this.zzb;
            byte[] bArr2 = z3Var.zzb;
            int o = o() + d;
            int o2 = o();
            int o3 = z3Var.o();
            while (o2 < o) {
                if (bArr[o2] != bArr2[o3]) {
                    return false;
                }
                o2++;
                o3++;
            }
            return true;
        } else {
            throw new IllegalArgumentException(a.g(59, "Ran off end of other: 0, ", d, ", ", z3Var.d()));
        }
    }

    @Override // b.i.a.f.h.l.t3
    public final t3 g(int i, int i2) {
        int l = t3.l(0, i2, d());
        if (l == 0) {
            return t3.j;
        }
        return new y3(this.zzb, o(), l);
    }

    @Override // b.i.a.f.h.l.t3
    public final String i(Charset charset) {
        return new String(this.zzb, o(), d(), charset);
    }

    @Override // b.i.a.f.h.l.t3
    public final void j(u3 u3Var) throws IOException {
        ((zzhi.a) u3Var).Z(this.zzb, o(), d());
    }

    @Override // b.i.a.f.h.l.t3
    public byte k(int i) {
        return this.zzb[i];
    }

    @Override // b.i.a.f.h.l.t3
    public final boolean m() {
        int o = o();
        return k7.b(this.zzb, o, d() + o);
    }

    public int o() {
        return 0;
    }
}
