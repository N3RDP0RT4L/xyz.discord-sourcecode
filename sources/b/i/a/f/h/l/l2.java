package b.i.a.f.h.l;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public abstract class l2<T> {

    /* renamed from: b  reason: collision with root package name */
    public static volatile t2 f1452b;
    public final q2 e;
    public final String f;
    public final T g;
    public volatile int h = -1;
    public volatile T i;
    public final boolean j;
    public static final Object a = new Object();
    public static u2 c = new u2(n2.a);
    public static final AtomicInteger d = new AtomicInteger();

    static {
        new AtomicReference();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public l2(q2 q2Var, String str, Object obj, boolean z2, m2 m2Var) {
        if (q2Var.a != null) {
            this.e = q2Var;
            this.f = str;
            this.g = obj;
            this.j = z2;
            return;
        }
        throw new IllegalArgumentException("Must pass a valid SharedPreferences file name or ContentProvider URI");
    }

    public abstract T a(Object obj);

    public final String b(String str) {
        if (str != null && str.isEmpty()) {
            return this.f;
        }
        String valueOf = String.valueOf(str);
        String valueOf2 = String.valueOf(this.f);
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    public final String c() {
        Objects.requireNonNull(this.e);
        return b("");
    }

    /* JADX WARN: Removed duplicated region for block: B:42:0x00c1  */
    /* JADX WARN: Removed duplicated region for block: B:43:0x00c2 A[Catch: all -> 0x0146, TryCatch #0 {, blocks: (B:8:0x0021, B:10:0x0025, B:14:0x002d, B:16:0x0044, B:22:0x0055, B:24:0x005b, B:26:0x0069, B:28:0x007c, B:30:0x0086, B:32:0x0090, B:33:0x0095, B:35:0x009e, B:37:0x00b0, B:38:0x00b5, B:39:0x00bb, B:43:0x00c2, B:45:0x00db, B:49:0x00e4, B:50:0x00e6, B:52:0x00f6, B:54:0x010c, B:57:0x011b, B:59:0x0125, B:60:0x012a, B:61:0x0130, B:63:0x0139, B:64:0x013c, B:65:0x0140, B:66:0x0144), top: B:73:0x0021 }] */
    /* JADX WARN: Removed duplicated region for block: B:52:0x00f6 A[Catch: all -> 0x0146, TryCatch #0 {, blocks: (B:8:0x0021, B:10:0x0025, B:14:0x002d, B:16:0x0044, B:22:0x0055, B:24:0x005b, B:26:0x0069, B:28:0x007c, B:30:0x0086, B:32:0x0090, B:33:0x0095, B:35:0x009e, B:37:0x00b0, B:38:0x00b5, B:39:0x00bb, B:43:0x00c2, B:45:0x00db, B:49:0x00e4, B:50:0x00e6, B:52:0x00f6, B:54:0x010c, B:57:0x011b, B:59:0x0125, B:60:0x012a, B:61:0x0130, B:63:0x0139, B:64:0x013c, B:65:0x0140, B:66:0x0144), top: B:73:0x0021 }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final T d() {
        /*
            Method dump skipped, instructions count: 332
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.h.l.l2.d():java.lang.Object");
    }
}
