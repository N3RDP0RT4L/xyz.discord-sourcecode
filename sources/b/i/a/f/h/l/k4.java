package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class k4 {
    public static final j4<?> a = new l4();

    /* renamed from: b  reason: collision with root package name */
    public static final j4<?> f1449b;

    static {
        j4<?> j4Var;
        try {
            j4Var = (j4) Class.forName("com.google.protobuf.ExtensionSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            j4Var = null;
        }
        f1449b = j4Var;
    }
}
