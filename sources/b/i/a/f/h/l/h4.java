package b.i.a.f.h.l;

import b.i.a.f.h.l.u4;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public class h4 {
    public static volatile h4 a;

    /* renamed from: b  reason: collision with root package name */
    public static volatile h4 f1442b;
    public static final h4 c = new h4(true);
    public final Map<a, u4.f<?, ?>> d;

    /* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
    /* loaded from: classes3.dex */
    public static final class a {
        public final Object a;

        /* renamed from: b  reason: collision with root package name */
        public final int f1443b;

        public a(Object obj, int i) {
            this.a = obj;
            this.f1443b = i;
        }

        public final boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return this.a == aVar.a && this.f1443b == aVar.f1443b;
        }

        public final int hashCode() {
            return (System.identityHashCode(this.a) * 65535) + this.f1443b;
        }
    }

    public h4() {
        this.d = new HashMap();
    }

    public static h4 a() {
        h4 h4Var = a;
        if (h4Var == null) {
            synchronized (h4.class) {
                h4Var = a;
                if (h4Var == null) {
                    h4Var = c;
                    a = h4Var;
                }
            }
        }
        return h4Var;
    }

    public h4(boolean z2) {
        this.d = Collections.emptyMap();
    }
}
