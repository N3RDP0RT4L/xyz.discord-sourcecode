package b.i.a.f.h.l;

import b.i.a.f.h.l.p4;
import b.i.a.f.h.l.u4;
import com.google.android.gms.internal.measurement.zzhi;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class n4<T extends p4<T>> {
    public static final n4 a = new n4(true);

    /* renamed from: b  reason: collision with root package name */
    public final r6<T, Object> f1457b = new t6(16);
    public boolean c;
    public boolean d;

    public n4(boolean z2) {
        int i = r6.j;
        h();
        h();
    }

    public static int a(p4<?> p4Var, Object obj) {
        p7 b2 = p4Var.b();
        int a2 = p4Var.a();
        if (!p4Var.d()) {
            return b(b2, a2, obj);
        }
        int i = 0;
        if (p4Var.e()) {
            for (Object obj2 : (List) obj) {
                i += g(b2, obj2);
            }
            return zzhi.D(a2) + i + zzhi.L(i);
        }
        for (Object obj3 : (List) obj) {
            i += b(b2, a2, obj3);
        }
        return i;
    }

    public static int b(p7 p7Var, int i, Object obj) {
        int D = zzhi.D(i);
        if (p7Var == p7.GROUP) {
            c6 c6Var = (c6) obj;
            if (c6Var instanceof m3) {
                m3 m3Var = (m3) c6Var;
            }
            D <<= 1;
        }
        return g(p7Var, obj) + D;
    }

    public static Object d(Object obj) {
        if (obj instanceof f6) {
            return ((f6) obj).a();
        }
        if (!(obj instanceof byte[])) {
            return obj;
        }
        byte[] bArr = (byte[]) obj;
        byte[] bArr2 = new byte[bArr.length];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        return bArr2;
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Code restructure failed: missing block: B:12:0x0027, code lost:
        if ((r3 instanceof b.i.a.f.h.l.y4) == false) goto L4;
     */
    /* JADX WARN: Code restructure failed: missing block: B:16:0x0030, code lost:
        if ((r3 instanceof byte[]) == false) goto L4;
     */
    /* JADX WARN: Code restructure failed: missing block: B:8:0x001e, code lost:
        if ((r3 instanceof b.i.a.f.h.l.d5) == false) goto L4;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static void e(b.i.a.f.h.l.p7 r2, java.lang.Object r3) {
        /*
            java.nio.charset.Charset r0 = b.i.a.f.h.l.w4.a
            java.util.Objects.requireNonNull(r3)
            int[] r0 = b.i.a.f.h.l.m4.a
            b.i.a.f.h.l.w7 r2 = r2.f()
            int r2 = r2.ordinal()
            r2 = r0[r2]
            r0 = 1
            r1 = 0
            switch(r2) {
                case 1: goto L42;
                case 2: goto L3f;
                case 3: goto L3c;
                case 4: goto L39;
                case 5: goto L36;
                case 6: goto L33;
                case 7: goto L2a;
                case 8: goto L21;
                case 9: goto L18;
                default: goto L16;
            }
        L16:
            r0 = 0
            goto L44
        L18:
            boolean r2 = r3 instanceof b.i.a.f.h.l.c6
            if (r2 != 0) goto L44
            boolean r2 = r3 instanceof b.i.a.f.h.l.d5
            if (r2 == 0) goto L16
            goto L44
        L21:
            boolean r2 = r3 instanceof java.lang.Integer
            if (r2 != 0) goto L44
            boolean r2 = r3 instanceof b.i.a.f.h.l.y4
            if (r2 == 0) goto L16
            goto L44
        L2a:
            boolean r2 = r3 instanceof b.i.a.f.h.l.t3
            if (r2 != 0) goto L44
            boolean r2 = r3 instanceof byte[]
            if (r2 == 0) goto L16
            goto L44
        L33:
            boolean r0 = r3 instanceof java.lang.String
            goto L44
        L36:
            boolean r0 = r3 instanceof java.lang.Boolean
            goto L44
        L39:
            boolean r0 = r3 instanceof java.lang.Double
            goto L44
        L3c:
            boolean r0 = r3 instanceof java.lang.Float
            goto L44
        L3f:
            boolean r0 = r3 instanceof java.lang.Long
            goto L44
        L42:
            boolean r0 = r3 instanceof java.lang.Integer
        L44:
            if (r0 == 0) goto L47
            return
        L47:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r3 = "Wrong object type used with protocol message reflection."
            r2.<init>(r3)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.h.l.n4.e(b.i.a.f.h.l.p7, java.lang.Object):void");
    }

    public static <T extends p4<T>> boolean f(Map.Entry<T, Object> entry) {
        T key = entry.getKey();
        if (key.c() == w7.MESSAGE) {
            if (key.d()) {
                for (c6 c6Var : (List) entry.getValue()) {
                    if (!c6Var.j()) {
                        return false;
                    }
                }
            } else {
                Object value = entry.getValue();
                if (value instanceof c6) {
                    if (!((c6) value).j()) {
                        return false;
                    }
                } else if (value instanceof d5) {
                    return true;
                } else {
                    throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
                }
            }
        }
        return true;
    }

    public static int g(p7 p7Var, Object obj) {
        int g;
        int L;
        switch (m4.f1454b[p7Var.ordinal()]) {
            case 1:
                ((Double) obj).doubleValue();
                Logger logger = zzhi.a;
                return 8;
            case 2:
                ((Float) obj).floatValue();
                Logger logger2 = zzhi.a;
                return 4;
            case 3:
                return zzhi.F(((Long) obj).longValue());
            case 4:
                return zzhi.F(((Long) obj).longValue());
            case 5:
                return zzhi.H(((Integer) obj).intValue());
            case 6:
                ((Long) obj).longValue();
                Logger logger3 = zzhi.a;
                return 8;
            case 7:
                ((Integer) obj).intValue();
                Logger logger4 = zzhi.a;
                return 4;
            case 8:
                ((Boolean) obj).booleanValue();
                Logger logger5 = zzhi.a;
                return 1;
            case 9:
                Logger logger6 = zzhi.a;
                return ((c6) obj).g();
            case 10:
                if (!(obj instanceof d5)) {
                    Logger logger7 = zzhi.a;
                    g = ((c6) obj).g();
                    L = zzhi.L(g);
                    break;
                } else {
                    Logger logger8 = zzhi.a;
                    g = ((d5) obj).a();
                    L = zzhi.L(g);
                    break;
                }
            case 11:
                if (obj instanceof t3) {
                    return zzhi.p((t3) obj);
                }
                return zzhi.q((String) obj);
            case 12:
                if (!(obj instanceof t3)) {
                    Logger logger9 = zzhi.a;
                    g = ((byte[]) obj).length;
                    L = zzhi.L(g);
                    break;
                } else {
                    return zzhi.p((t3) obj);
                }
            case 13:
                return zzhi.L(((Integer) obj).intValue());
            case 14:
                ((Integer) obj).intValue();
                Logger logger10 = zzhi.a;
                return 4;
            case 15:
                ((Long) obj).longValue();
                Logger logger11 = zzhi.a;
                return 8;
            case 16:
                return zzhi.O(((Integer) obj).intValue());
            case 17:
                return zzhi.K(((Long) obj).longValue());
            case 18:
                if (obj instanceof y4) {
                    return zzhi.H(((y4) obj).a());
                }
                return zzhi.H(((Integer) obj).intValue());
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
        return L + g;
    }

    public static int k(Map.Entry<T, Object> entry) {
        int M;
        int L;
        int g;
        int L2;
        T key = entry.getKey();
        Object value = entry.getValue();
        if (key.c() != w7.MESSAGE || key.d() || key.e()) {
            return a(key, value);
        }
        if (value instanceof d5) {
            M = zzhi.M(2, entry.getKey().a()) + (zzhi.L(8) << 1);
            L = zzhi.L(24);
            g = ((d5) value).a();
            L2 = zzhi.L(g);
        } else {
            M = zzhi.M(2, entry.getKey().a()) + (zzhi.L(8) << 1);
            L = zzhi.L(24);
            g = ((c6) value).g();
            L2 = zzhi.L(g);
        }
        return L2 + g + L + M;
    }

    public final Object c(T t) {
        Object obj = this.f1457b.get(t);
        if (!(obj instanceof d5)) {
            return obj;
        }
        d5 d5Var = (d5) obj;
        int i = d5.c;
        throw new NoSuchMethodError();
    }

    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        n4 n4Var = new n4();
        for (int i = 0; i < this.f1457b.e(); i++) {
            Map.Entry<T, Object> d = this.f1457b.d(i);
            n4Var.i(d.getKey(), d.getValue());
        }
        for (Map.Entry<T, Object> entry : this.f1457b.g()) {
            n4Var.i(entry.getKey(), entry.getValue());
        }
        n4Var.d = this.d;
        return n4Var;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof n4)) {
            return false;
        }
        return this.f1457b.equals(((n4) obj).f1457b);
    }

    public final void h() {
        if (!this.c) {
            this.f1457b.c();
            this.c = true;
        }
    }

    public final int hashCode() {
        return this.f1457b.hashCode();
    }

    public final void i(T t, Object obj) {
        if (!t.d()) {
            e(t.b(), obj);
        } else if (obj instanceof List) {
            ArrayList arrayList = new ArrayList();
            arrayList.addAll((List) obj);
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                Object obj2 = arrayList.get(i);
                i++;
                e(t.b(), obj2);
            }
            obj = arrayList;
        } else {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        }
        if (obj instanceof d5) {
            this.d = true;
        }
        this.f1457b.put(t, obj);
    }

    public final void j(Map.Entry<T, Object> entry) {
        c6 c6Var;
        T key = entry.getKey();
        Object value = entry.getValue();
        if (value instanceof d5) {
            d5 d5Var = (d5) value;
            int i = d5.c;
            throw new NoSuchMethodError();
        } else if (key.d()) {
            Object c = c(key);
            if (c == null) {
                c = new ArrayList();
            }
            for (Object obj : (List) value) {
                ((List) c).add(d(obj));
            }
            this.f1457b.put(key, c);
        } else if (key.c() == w7.MESSAGE) {
            Object c2 = c(key);
            if (c2 == null) {
                this.f1457b.put(key, d(value));
                return;
            }
            if (c2 instanceof f6) {
                c6Var = key.c0((f6) c2, (f6) value);
            } else {
                c6Var = ((u4.b) key.X(((c6) c2).f(), (c6) value)).p();
            }
            this.f1457b.put(key, c6Var);
        } else {
            this.f1457b.put(key, d(value));
        }
    }

    public final Iterator<Map.Entry<T, Object>> l() {
        if (this.d) {
            return new i5(this.f1457b.entrySet().iterator());
        }
        return this.f1457b.entrySet().iterator();
    }

    public final boolean m() {
        for (int i = 0; i < this.f1457b.e(); i++) {
            if (!f(this.f1457b.d(i))) {
                return false;
            }
        }
        for (Map.Entry<T, Object> entry : this.f1457b.g()) {
            if (!f(entry)) {
                return false;
            }
        }
        return true;
    }

    public n4() {
        int i = r6.j;
    }
}
