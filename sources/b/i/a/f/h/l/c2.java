package b.i.a.f.h.l;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import androidx.annotation.GuardedBy;
import androidx.core.content.PermissionChecker;
import b.i.a.f.e.o.f;
import java.util.HashMap;
import java.util.TreeMap;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class c2 implements b2 {
    @GuardedBy("GservicesLoader.class")
    public static c2 a;

    /* renamed from: b  reason: collision with root package name */
    public final Context f1433b;
    public final ContentObserver c;

    public c2(Context context) {
        this.f1433b = context;
        f2 f2Var = new f2();
        this.c = f2Var;
        context.getContentResolver().registerContentObserver(u1.a, true, f2Var);
    }

    public static c2 a(Context context) {
        c2 c2Var;
        synchronized (c2.class) {
            if (a == null) {
                a = PermissionChecker.checkSelfPermission(context, "com.google.android.providers.gsf.permission.READ_GSERVICES") == 0 ? new c2(context) : new c2();
            }
            c2Var = a;
        }
        return c2Var;
    }

    @Override // b.i.a.f.h.l.b2
    public final Object g(final String str) {
        if (this.f1433b == null) {
            return null;
        }
        try {
            return (String) f.T1(new d2(this, str) { // from class: b.i.a.f.h.l.g2
                public final c2 a;

                /* renamed from: b  reason: collision with root package name */
                public final String f1439b;

                {
                    this.a = this;
                    this.f1439b = str;
                }

                @Override // b.i.a.f.h.l.d2
                public final Object a() {
                    String str2;
                    Cursor query;
                    c2 c2Var = this.a;
                    String str3 = this.f1439b;
                    ContentResolver contentResolver = c2Var.f1433b.getContentResolver();
                    Uri uri = u1.a;
                    synchronized (u1.class) {
                        if (u1.f == null) {
                            u1.e.set(false);
                            u1.f = new HashMap<>();
                            u1.k = new Object();
                            u1.l = false;
                            contentResolver.registerContentObserver(u1.a, true, new t1());
                        } else if (u1.e.getAndSet(false)) {
                            u1.f.clear();
                            u1.g.clear();
                            u1.h.clear();
                            u1.i.clear();
                            u1.j.clear();
                            u1.k = new Object();
                            u1.l = false;
                        }
                        Object obj = u1.k;
                        str2 = null;
                        if (u1.f.containsKey(str3)) {
                            String str4 = u1.f.get(str3);
                            if (str4 != null) {
                                str2 = str4;
                            }
                        } else {
                            String[] strArr = u1.m;
                            int length = strArr.length;
                            int i = 0;
                            while (true) {
                                if (i >= length) {
                                    query = contentResolver.query(u1.a, null, null, new String[]{str3}, null);
                                    if (query != null) {
                                        try {
                                            if (!query.moveToFirst()) {
                                                u1.a(obj, str3, null);
                                            } else {
                                                String string = query.getString(1);
                                                if (string != null && string.equals(null)) {
                                                    string = null;
                                                }
                                                u1.a(obj, str3, string);
                                                if (string != null) {
                                                    str2 = string;
                                                }
                                            }
                                        } finally {
                                            query.close();
                                        }
                                    } else if (query != null) {
                                    }
                                } else if (!str3.startsWith(strArr[i])) {
                                    i++;
                                } else if (!u1.l || u1.f.isEmpty()) {
                                    String[] strArr2 = u1.m;
                                    HashMap<String, String> hashMap = u1.f;
                                    query = contentResolver.query(u1.f1492b, null, null, strArr2, null);
                                    TreeMap treeMap = new TreeMap();
                                    if (query != null) {
                                        while (query.moveToNext()) {
                                            treeMap.put(query.getString(0), query.getString(1));
                                        }
                                    }
                                    hashMap.putAll(treeMap);
                                    u1.l = true;
                                    if (u1.f.containsKey(str3)) {
                                        String str5 = u1.f.get(str3);
                                        if (str5 != null) {
                                            str2 = str5;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    return str2;
                }
            });
        } catch (IllegalStateException | SecurityException e) {
            String valueOf = String.valueOf(str);
            Log.e("GservicesLoader", valueOf.length() != 0 ? "Unable to read GServices for: ".concat(valueOf) : new String("Unable to read GServices for: "), e);
            return null;
        }
    }

    public c2() {
        this.f1433b = null;
        this.c = null;
    }
}
