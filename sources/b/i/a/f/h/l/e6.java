package b.i.a.f.h.l;

import b.d.b.a.a;
import b.i.a.f.e.o.f;
import com.google.android.gms.internal.measurement.zzhi;
import com.google.android.gms.internal.measurement.zzij;
import com.google.android.material.shadow.ShadowDrawableWrapper;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import sun.misc.Unsafe;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class e6<T> implements q6<T> {
    public static final int[] a = new int[0];

    /* renamed from: b  reason: collision with root package name */
    public static final Unsafe f1437b = j7.k();
    public final int[] c;
    public final Object[] d;
    public final int e;
    public final int f;
    public final c6 g;
    public final boolean h;
    public final boolean i;
    public final int[] j;
    public final int k;
    public final int l;
    public final i6 m;
    public final m5 n;
    public final d7<?, ?> o;
    public final j4<?> p;
    public final v5 q;

    public e6(int[] iArr, Object[] objArr, int i, int i2, c6 c6Var, boolean z2, int[] iArr2, int i3, int i4, i6 i6Var, m5 m5Var, d7 d7Var, j4 j4Var, v5 v5Var) {
        this.c = iArr;
        this.d = objArr;
        this.e = i;
        this.f = i2;
        boolean z3 = c6Var instanceof u4;
        this.i = z2;
        this.h = j4Var != null && j4Var.e(c6Var);
        this.j = iArr2;
        this.k = i3;
        this.l = i4;
        this.m = i6Var;
        this.n = m5Var;
        this.o = d7Var;
        this.p = j4Var;
        this.g = c6Var;
        this.q = v5Var;
    }

    public static <T> float F(T t, long j) {
        return ((Float) j7.r(t, j)).floatValue();
    }

    public static <T> int J(T t, long j) {
        return ((Integer) j7.r(t, j)).intValue();
    }

    public static <T> long L(T t, long j) {
        return ((Long) j7.r(t, j)).longValue();
    }

    public static c7 M(Object obj) {
        u4 u4Var = (u4) obj;
        c7 c7Var = u4Var.zzb;
        if (c7Var != c7.a) {
            return c7Var;
        }
        c7 c = c7.c();
        u4Var.zzb = c;
        return c;
    }

    public static <T> boolean N(T t, long j) {
        return ((Boolean) j7.r(t, j)).booleanValue();
    }

    public static e6 n(a6 a6Var, i6 i6Var, m5 m5Var, d7 d7Var, j4 j4Var, v5 v5Var) {
        int i;
        int i2;
        int i3;
        int[] iArr;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        o6 o6Var;
        int i12;
        String str;
        Object[] objArr;
        boolean z2;
        int i13;
        int i14;
        int i15;
        Field field;
        int i16;
        char charAt;
        int i17;
        int i18;
        Field field2;
        Field field3;
        int i19;
        char charAt2;
        int i20;
        char charAt3;
        int i21;
        char charAt4;
        int i22;
        char charAt5;
        int i23;
        char charAt6;
        int i24;
        char charAt7;
        int i25;
        char charAt8;
        int i26;
        char charAt9;
        int i27;
        char charAt10;
        int i28;
        char charAt11;
        int i29;
        char charAt12;
        int i30;
        char charAt13;
        if (a6Var instanceof o6) {
            o6 o6Var2 = (o6) a6Var;
            int i31 = 0;
            boolean z3 = ((o6Var2.d & 1) == 1 ? (char) 1 : (char) 2) == 2;
            String str2 = o6Var2.f1473b;
            int length = str2.length();
            if (str2.charAt(0) >= 55296) {
                int i32 = 1;
                while (true) {
                    i = i32 + 1;
                    if (str2.charAt(i32) < 55296) {
                        break;
                    }
                    i32 = i;
                }
            } else {
                i = 1;
            }
            int i33 = i + 1;
            int charAt14 = str2.charAt(i);
            if (charAt14 >= 55296) {
                int i34 = charAt14 & 8191;
                int i35 = 13;
                while (true) {
                    i30 = i33 + 1;
                    charAt13 = str2.charAt(i33);
                    if (charAt13 < 55296) {
                        break;
                    }
                    i34 |= (charAt13 & 8191) << i35;
                    i35 += 13;
                    i33 = i30;
                }
                charAt14 = i34 | (charAt13 << i35);
                i33 = i30;
            }
            if (charAt14 == 0) {
                iArr = a;
                i7 = 0;
                i6 = 0;
                i5 = 0;
                i4 = 0;
                i3 = 0;
                i2 = 0;
            } else {
                int i36 = i33 + 1;
                int charAt15 = str2.charAt(i33);
                if (charAt15 >= 55296) {
                    int i37 = charAt15 & 8191;
                    int i38 = 13;
                    while (true) {
                        i29 = i36 + 1;
                        charAt12 = str2.charAt(i36);
                        if (charAt12 < 55296) {
                            break;
                        }
                        i37 |= (charAt12 & 8191) << i38;
                        i38 += 13;
                        i36 = i29;
                    }
                    charAt15 = i37 | (charAt12 << i38);
                    i36 = i29;
                }
                int i39 = i36 + 1;
                int charAt16 = str2.charAt(i36);
                if (charAt16 >= 55296) {
                    int i40 = charAt16 & 8191;
                    int i41 = 13;
                    while (true) {
                        i28 = i39 + 1;
                        charAt11 = str2.charAt(i39);
                        if (charAt11 < 55296) {
                            break;
                        }
                        i40 |= (charAt11 & 8191) << i41;
                        i41 += 13;
                        i39 = i28;
                    }
                    charAt16 = i40 | (charAt11 << i41);
                    i39 = i28;
                }
                int i42 = i39 + 1;
                i6 = str2.charAt(i39);
                if (i6 >= 55296) {
                    int i43 = i6 & 8191;
                    int i44 = 13;
                    while (true) {
                        i27 = i42 + 1;
                        charAt10 = str2.charAt(i42);
                        if (charAt10 < 55296) {
                            break;
                        }
                        i43 |= (charAt10 & 8191) << i44;
                        i44 += 13;
                        i42 = i27;
                    }
                    i6 = i43 | (charAt10 << i44);
                    i42 = i27;
                }
                int i45 = i42 + 1;
                i5 = str2.charAt(i42);
                if (i5 >= 55296) {
                    int i46 = i5 & 8191;
                    int i47 = 13;
                    while (true) {
                        i26 = i45 + 1;
                        charAt9 = str2.charAt(i45);
                        if (charAt9 < 55296) {
                            break;
                        }
                        i46 |= (charAt9 & 8191) << i47;
                        i47 += 13;
                        i45 = i26;
                    }
                    i5 = i46 | (charAt9 << i47);
                    i45 = i26;
                }
                int i48 = i45 + 1;
                i4 = str2.charAt(i45);
                if (i4 >= 55296) {
                    int i49 = i4 & 8191;
                    int i50 = 13;
                    while (true) {
                        i25 = i48 + 1;
                        charAt8 = str2.charAt(i48);
                        if (charAt8 < 55296) {
                            break;
                        }
                        i49 |= (charAt8 & 8191) << i50;
                        i50 += 13;
                        i48 = i25;
                    }
                    i4 = i49 | (charAt8 << i50);
                    i48 = i25;
                }
                int i51 = i48 + 1;
                int charAt17 = str2.charAt(i48);
                if (charAt17 >= 55296) {
                    int i52 = charAt17 & 8191;
                    int i53 = 13;
                    while (true) {
                        i24 = i51 + 1;
                        charAt7 = str2.charAt(i51);
                        if (charAt7 < 55296) {
                            break;
                        }
                        i52 |= (charAt7 & 8191) << i53;
                        i53 += 13;
                        i51 = i24;
                    }
                    charAt17 = i52 | (charAt7 << i53);
                    i51 = i24;
                }
                int i54 = i51 + 1;
                int charAt18 = str2.charAt(i51);
                if (charAt18 >= 55296) {
                    int i55 = charAt18 & 8191;
                    int i56 = 13;
                    while (true) {
                        i23 = i54 + 1;
                        charAt6 = str2.charAt(i54);
                        if (charAt6 < 55296) {
                            break;
                        }
                        i55 |= (charAt6 & 8191) << i56;
                        i56 += 13;
                        i54 = i23;
                    }
                    charAt18 = i55 | (charAt6 << i56);
                    i54 = i23;
                }
                int i57 = i54 + 1;
                i2 = str2.charAt(i54);
                if (i2 >= 55296) {
                    int i58 = i2 & 8191;
                    int i59 = i57;
                    int i60 = 13;
                    while (true) {
                        i22 = i59 + 1;
                        charAt5 = str2.charAt(i59);
                        if (charAt5 < 55296) {
                            break;
                        }
                        i58 |= (charAt5 & 8191) << i60;
                        i60 += 13;
                        i59 = i22;
                    }
                    i2 = i58 | (charAt5 << i60);
                    i57 = i22;
                }
                i3 = (charAt15 << 1) + charAt16;
                i7 = charAt17;
                iArr = new int[i2 + charAt17 + charAt18];
                i31 = charAt15;
                i33 = i57;
            }
            Unsafe unsafe = f1437b;
            Object[] objArr2 = o6Var2.c;
            Class<?> cls = o6Var2.a.getClass();
            int i61 = i33;
            int[] iArr2 = new int[i4 * 3];
            Object[] objArr3 = new Object[i4 << 1];
            int i62 = i2 + i7;
            int i63 = i2;
            int i64 = i62;
            int i65 = 0;
            int i66 = 0;
            while (i61 < length) {
                int i67 = i61 + 1;
                int charAt19 = str2.charAt(i61);
                length = length;
                if (charAt19 >= 55296) {
                    int i68 = charAt19 & 8191;
                    int i69 = i67;
                    int i70 = 13;
                    while (true) {
                        i21 = i69 + 1;
                        charAt4 = str2.charAt(i69);
                        i8 = i2;
                        if (charAt4 < 55296) {
                            break;
                        }
                        i68 |= (charAt4 & 8191) << i70;
                        i70 += 13;
                        i69 = i21;
                        i2 = i8;
                    }
                    charAt19 = i68 | (charAt4 << i70);
                    i9 = i21;
                } else {
                    i8 = i2;
                    i9 = i67;
                }
                int i71 = i9 + 1;
                int charAt20 = str2.charAt(i9);
                if (charAt20 >= 55296) {
                    int i72 = charAt20 & 8191;
                    int i73 = i71;
                    int i74 = 13;
                    while (true) {
                        i20 = i73 + 1;
                        charAt3 = str2.charAt(i73);
                        i10 = i5;
                        if (charAt3 < 55296) {
                            break;
                        }
                        i72 |= (charAt3 & 8191) << i74;
                        i74 += 13;
                        i73 = i20;
                        i5 = i10;
                    }
                    charAt20 = i72 | (charAt3 << i74);
                    i11 = i20;
                } else {
                    i10 = i5;
                    i11 = i71;
                }
                int i75 = charAt20 & 255;
                i6 = i6;
                if ((charAt20 & 1024) != 0) {
                    i65++;
                    iArr[i65] = i66;
                }
                if (i75 >= 51) {
                    int i76 = i11 + 1;
                    int charAt21 = str2.charAt(i11);
                    char c = 55296;
                    if (charAt21 >= 55296) {
                        int i77 = charAt21 & 8191;
                        int i78 = 13;
                        int i79 = i76;
                        while (true) {
                            i19 = i79 + 1;
                            charAt2 = str2.charAt(i79);
                            if (charAt2 < c) {
                                break;
                            }
                            i77 |= (charAt2 & 8191) << i78;
                            i78 += 13;
                            i79 = i19;
                            c = 55296;
                        }
                        charAt21 = i77 | (charAt2 << i78);
                        i18 = i19;
                    } else {
                        i18 = i76;
                    }
                    i11 = i18;
                    int i80 = i75 - 51;
                    o6Var = o6Var2;
                    if (i80 == 9 || i80 == 17) {
                        i3++;
                        objArr3[((i66 / 3) << 1) + 1] = objArr2[i3];
                    } else if (i80 == 12 && !z3) {
                        i3++;
                        objArr3[((i66 / 3) << 1) + 1] = objArr2[i3];
                    }
                    int i81 = charAt21 << 1;
                    Object obj = objArr2[i81];
                    if (obj instanceof Field) {
                        field2 = (Field) obj;
                    } else {
                        field2 = p(cls, (String) obj);
                        objArr2[i81] = field2;
                    }
                    i12 = i3;
                    i14 = (int) unsafe.objectFieldOffset(field2);
                    int i82 = i81 + 1;
                    Object obj2 = objArr2[i82];
                    if (obj2 instanceof Field) {
                        field3 = (Field) obj2;
                    } else {
                        field3 = p(cls, (String) obj2);
                        objArr2[i82] = field3;
                    }
                    i15 = (int) unsafe.objectFieldOffset(field3);
                    str = str2;
                    z2 = z3;
                    objArr = objArr3;
                    i13 = 0;
                } else {
                    o6Var = o6Var2;
                    int i83 = i3 + 1;
                    Field p = p(cls, (String) objArr2[i3]);
                    if (i75 == 9 || i75 == 17) {
                        objArr3[((i66 / 3) << 1) + 1] = p.getType();
                    } else {
                        if (i75 == 27 || i75 == 49) {
                            i17 = i83 + 1;
                            objArr3[((i66 / 3) << 1) + 1] = objArr2[i83];
                        } else {
                            if (i75 == 12 || i75 == 30 || i75 == 44) {
                                if (!z3) {
                                    i17 = i83 + 1;
                                    objArr3[((i66 / 3) << 1) + 1] = objArr2[i83];
                                }
                            } else if (i75 == 50) {
                                int i84 = i63 + 1;
                                iArr[i63] = i66;
                                int i85 = (i66 / 3) << 1;
                                i17 = i83 + 1;
                                objArr3[i85] = objArr2[i83];
                                if ((charAt20 & 2048) != 0) {
                                    i83 = i17 + 1;
                                    objArr3[i85 + 1] = objArr2[i17];
                                    objArr = objArr3;
                                    i63 = i84;
                                    z2 = z3;
                                } else {
                                    i63 = i84;
                                }
                            }
                            i14 = (int) unsafe.objectFieldOffset(p);
                            if ((charAt20 & 4096) == 4096 || i75 > 17) {
                                i12 = i83;
                                str = str2;
                                i15 = 1048575;
                                i13 = 0;
                            } else {
                                int i86 = i11 + 1;
                                int charAt22 = str2.charAt(i11);
                                if (charAt22 >= 55296) {
                                    int i87 = charAt22 & 8191;
                                    int i88 = 13;
                                    while (true) {
                                        i16 = i86 + 1;
                                        charAt = str2.charAt(i86);
                                        if (charAt < 55296) {
                                            break;
                                        }
                                        i87 |= (charAt & 8191) << i88;
                                        i88 += 13;
                                        i86 = i16;
                                    }
                                    charAt22 = i87 | (charAt << i88);
                                    i86 = i16;
                                }
                                int i89 = (charAt22 / 32) + (i31 << 1);
                                Object obj3 = objArr2[i89];
                                i12 = i83;
                                if (obj3 instanceof Field) {
                                    field = (Field) obj3;
                                } else {
                                    field = p(cls, (String) obj3);
                                    objArr2[i89] = field;
                                }
                                str = str2;
                                i15 = (int) unsafe.objectFieldOffset(field);
                                i13 = charAt22 % 32;
                                i11 = i86;
                            }
                            if (i75 >= 18 && i75 <= 49) {
                                i64++;
                                iArr[i64] = i14;
                            }
                        }
                        i83 = i17;
                    }
                    z2 = z3;
                    objArr = objArr3;
                    i14 = (int) unsafe.objectFieldOffset(p);
                    if ((charAt20 & 4096) == 4096) {
                    }
                    i12 = i83;
                    str = str2;
                    i15 = 1048575;
                    i13 = 0;
                    if (i75 >= 18) {
                        i64++;
                        iArr[i64] = i14;
                    }
                }
                int i90 = i66 + 1;
                iArr2[i66] = charAt19;
                int i91 = i90 + 1;
                iArr2[i90] = ((charAt20 & 512) != 0 ? 536870912 : 0) | ((charAt20 & 256) != 0 ? 268435456 : 0) | (i75 << 20) | i14;
                i66 = i91 + 1;
                iArr2[i91] = (i13 << 20) | i15;
                z3 = z2;
                i61 = i11;
                i2 = i8;
                i5 = i10;
                objArr3 = objArr;
                str2 = str;
                i3 = i12;
                o6Var2 = o6Var;
            }
            return new e6(iArr2, objArr3, i6, i5, o6Var2.a, z3, iArr, i2, i62, i6Var, m5Var, d7Var, j4Var, v5Var);
        }
        z6 z6Var = (z6) a6Var;
        throw new NoSuchMethodError();
    }

    public static Field p(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            String name = cls.getName();
            String arrays = Arrays.toString(declaredFields);
            throw new RuntimeException(a.H(a.Q(a.b(arrays, name.length() + a.b(str, 40)), "Field ", str, " for ", name), " not found. Known fields are ", arrays));
        }
    }

    public static List<?> q(Object obj, long j) {
        return (List) j7.r(obj, j);
    }

    public static void r(int i, Object obj, v7 v7Var) throws IOException {
        if (obj instanceof String) {
            ((g4) v7Var).a.k(i, (String) obj);
            return;
        }
        ((g4) v7Var).a.h(i, (t3) obj);
    }

    public static <UT, UB> void s(d7<UT, UB> d7Var, T t, v7 v7Var) throws IOException {
        d7Var.c(d7Var.e(t), v7Var);
    }

    public static <T> double y(T t, long j) {
        return ((Double) j7.r(t, j)).doubleValue();
    }

    public final Object A(int i) {
        return this.d[(i / 3) << 1];
    }

    public final void B(T t, int i) {
        int i2 = this.c[i + 2];
        long j = 1048575 & i2;
        if (j != 1048575) {
            j7.e.e(t, j, (1 << (i2 >>> 20)) | j7.b(t, j));
        }
    }

    public final void C(T t, int i, int i2) {
        j7.e.e(t, this.c[i2 + 2] & 1048575, i);
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0031  */
    /* JADX WARN: Removed duplicated region for block: B:170:0x0527  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void D(T r18, b.i.a.f.h.l.v7 r19) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 1488
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.h.l.e6.D(java.lang.Object, b.i.a.f.h.l.v7):void");
    }

    public final void E(T t, T t2, int i) {
        int[] iArr = this.c;
        int i2 = iArr[i + 1];
        int i3 = iArr[i];
        long j = i2 & 1048575;
        if (w(t2, i3, i)) {
            Object obj = null;
            if (w(t, i3, i)) {
                obj = j7.r(t, j);
            }
            Object r = j7.r(t2, j);
            if (obj != null && r != null) {
                j7.f(t, j, w4.c(obj, r));
                C(t, i3, i);
            } else if (r != null) {
                j7.f(t, j, r);
                C(t, i3, i);
            }
        }
    }

    public final a5 G(int i) {
        return (a5) this.d[((i / 3) << 1) + 1];
    }

    public final boolean H(T t, T t2, int i) {
        return v(t, i) == v(t2, i);
    }

    public final int I(int i) {
        return this.c[i + 1];
    }

    public final int K(int i) {
        return this.c[i + 2];
    }

    @Override // b.i.a.f.h.l.q6
    public final T a() {
        return (T) this.m.b(this.g);
    }

    @Override // b.i.a.f.h.l.q6
    public final int b(T t) {
        int i;
        int i2;
        int length = this.c.length;
        int i3 = 0;
        for (int i4 = 0; i4 < length; i4 += 3) {
            int I = I(i4);
            int i5 = this.c[i4];
            long j = 1048575 & I;
            int i6 = 37;
            switch ((I & 267386880) >>> 20) {
                case 0:
                    i2 = i3 * 53;
                    i = w4.a(Double.doubleToLongBits(j7.q(t, j)));
                    i3 = i + i2;
                    break;
                case 1:
                    i2 = i3 * 53;
                    i = Float.floatToIntBits(j7.n(t, j));
                    i3 = i + i2;
                    break;
                case 2:
                    i2 = i3 * 53;
                    i = w4.a(j7.i(t, j));
                    i3 = i + i2;
                    break;
                case 3:
                    i2 = i3 * 53;
                    i = w4.a(j7.i(t, j));
                    i3 = i + i2;
                    break;
                case 4:
                    i2 = i3 * 53;
                    i = j7.b(t, j);
                    i3 = i + i2;
                    break;
                case 5:
                    i2 = i3 * 53;
                    i = w4.a(j7.i(t, j));
                    i3 = i + i2;
                    break;
                case 6:
                    i2 = i3 * 53;
                    i = j7.b(t, j);
                    i3 = i + i2;
                    break;
                case 7:
                    i2 = i3 * 53;
                    i = w4.b(j7.m(t, j));
                    i3 = i + i2;
                    break;
                case 8:
                    i2 = i3 * 53;
                    i = ((String) j7.r(t, j)).hashCode();
                    i3 = i + i2;
                    break;
                case 9:
                    Object r = j7.r(t, j);
                    if (r != null) {
                        i6 = r.hashCode();
                    }
                    i3 = (i3 * 53) + i6;
                    break;
                case 10:
                    i2 = i3 * 53;
                    i = j7.r(t, j).hashCode();
                    i3 = i + i2;
                    break;
                case 11:
                    i2 = i3 * 53;
                    i = j7.b(t, j);
                    i3 = i + i2;
                    break;
                case 12:
                    i2 = i3 * 53;
                    i = j7.b(t, j);
                    i3 = i + i2;
                    break;
                case 13:
                    i2 = i3 * 53;
                    i = j7.b(t, j);
                    i3 = i + i2;
                    break;
                case 14:
                    i2 = i3 * 53;
                    i = w4.a(j7.i(t, j));
                    i3 = i + i2;
                    break;
                case 15:
                    i2 = i3 * 53;
                    i = j7.b(t, j);
                    i3 = i + i2;
                    break;
                case 16:
                    i2 = i3 * 53;
                    i = w4.a(j7.i(t, j));
                    i3 = i + i2;
                    break;
                case 17:
                    Object r2 = j7.r(t, j);
                    if (r2 != null) {
                        i6 = r2.hashCode();
                    }
                    i3 = (i3 * 53) + i6;
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    i2 = i3 * 53;
                    i = j7.r(t, j).hashCode();
                    i3 = i + i2;
                    break;
                case 50:
                    i2 = i3 * 53;
                    i = j7.r(t, j).hashCode();
                    i3 = i + i2;
                    break;
                case 51:
                    if (w(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = w4.a(Double.doubleToLongBits(y(t, j)));
                        i3 = i + i2;
                        break;
                    } else {
                        break;
                    }
                case 52:
                    if (w(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = Float.floatToIntBits(F(t, j));
                        i3 = i + i2;
                        break;
                    } else {
                        break;
                    }
                case 53:
                    if (w(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = w4.a(L(t, j));
                        i3 = i + i2;
                        break;
                    } else {
                        break;
                    }
                case 54:
                    if (w(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = w4.a(L(t, j));
                        i3 = i + i2;
                        break;
                    } else {
                        break;
                    }
                case 55:
                    if (w(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = J(t, j);
                        i3 = i + i2;
                        break;
                    } else {
                        break;
                    }
                case 56:
                    if (w(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = w4.a(L(t, j));
                        i3 = i + i2;
                        break;
                    } else {
                        break;
                    }
                case 57:
                    if (w(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = J(t, j);
                        i3 = i + i2;
                        break;
                    } else {
                        break;
                    }
                case 58:
                    if (w(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = w4.b(N(t, j));
                        i3 = i + i2;
                        break;
                    } else {
                        break;
                    }
                case 59:
                    if (w(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = ((String) j7.r(t, j)).hashCode();
                        i3 = i + i2;
                        break;
                    } else {
                        break;
                    }
                case 60:
                    if (w(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = j7.r(t, j).hashCode();
                        i3 = i + i2;
                        break;
                    } else {
                        break;
                    }
                case 61:
                    if (w(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = j7.r(t, j).hashCode();
                        i3 = i + i2;
                        break;
                    } else {
                        break;
                    }
                case 62:
                    if (w(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = J(t, j);
                        i3 = i + i2;
                        break;
                    } else {
                        break;
                    }
                case 63:
                    if (w(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = J(t, j);
                        i3 = i + i2;
                        break;
                    } else {
                        break;
                    }
                case 64:
                    if (w(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = J(t, j);
                        i3 = i + i2;
                        break;
                    } else {
                        break;
                    }
                case 65:
                    if (w(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = w4.a(L(t, j));
                        i3 = i + i2;
                        break;
                    } else {
                        break;
                    }
                case 66:
                    if (w(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = J(t, j);
                        i3 = i + i2;
                        break;
                    } else {
                        break;
                    }
                case 67:
                    if (w(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = w4.a(L(t, j));
                        i3 = i + i2;
                        break;
                    } else {
                        break;
                    }
                case 68:
                    if (w(t, i5, i4)) {
                        i2 = i3 * 53;
                        i = j7.r(t, j).hashCode();
                        i3 = i + i2;
                        break;
                    } else {
                        break;
                    }
            }
        }
        int hashCode = this.o.e(t).hashCode() + (i3 * 53);
        return this.h ? (hashCode * 53) + this.p.b(t).hashCode() : hashCode;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // b.i.a.f.h.l.q6
    public final boolean c(T t) {
        int i;
        int i2;
        int i3 = 1048575;
        int i4 = 0;
        int i5 = 0;
        while (true) {
            boolean z2 = true;
            if (i5 >= this.k) {
                return !this.h || this.p.b(t).m();
            }
            int i6 = this.j[i5];
            int i7 = this.c[i6];
            int I = I(i6);
            int i8 = this.c[i6 + 2];
            int i9 = i8 & 1048575;
            int i10 = 1 << (i8 >>> 20);
            if (i9 != i3) {
                if (i9 != 1048575) {
                    i4 = f1437b.getInt(t, i9);
                }
                i = i4;
                i2 = i9;
            } else {
                i2 = i3;
                i = i4;
            }
            if (((268435456 & I) != 0) && !x(t, i6, i2, i, i10)) {
                return false;
            }
            int i11 = (267386880 & I) >>> 20;
            if (i11 != 9 && i11 != 17) {
                if (i11 != 27) {
                    if (i11 == 60 || i11 == 68) {
                        if (w(t, i7, i6) && !o(i6).c(j7.r(t, I & 1048575))) {
                            return false;
                        }
                    } else if (i11 != 49) {
                        if (i11 == 50 && !this.q.e(j7.r(t, I & 1048575)).isEmpty()) {
                            Objects.requireNonNull(this.q.g(A(i6)));
                            throw null;
                        }
                    }
                }
                List list = (List) j7.r(t, I & 1048575);
                if (!list.isEmpty()) {
                    q6 o = o(i6);
                    int i12 = 0;
                    while (true) {
                        if (i12 >= list.size()) {
                            break;
                        } else if (!o.c(list.get(i12))) {
                            z2 = false;
                            break;
                        } else {
                            i12++;
                        }
                    }
                }
                if (!z2) {
                    return false;
                }
            } else if (x(t, i6, i2, i, i10) && !o(i6).c(j7.r(t, I & 1048575))) {
                return false;
            }
            i5++;
            i3 = i2;
            i4 = i;
        }
    }

    @Override // b.i.a.f.h.l.q6
    public final void d(T t) {
        int i;
        int i2 = this.k;
        while (true) {
            i = this.l;
            if (i2 >= i) {
                break;
            }
            long I = I(this.j[i2]) & 1048575;
            Object r = j7.r(t, I);
            if (r != null) {
                j7.f(t, I, this.q.c(r));
            }
            i2++;
        }
        int length = this.j.length;
        while (i < length) {
            this.n.b(t, this.j[i]);
            i++;
        }
        this.o.h(t);
        if (this.h) {
            this.p.g(t);
        }
    }

    @Override // b.i.a.f.h.l.q6
    public final int e(T t) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        if (this.i) {
            Unsafe unsafe = f1437b;
            int i10 = 0;
            for (int i11 = 0; i11 < this.c.length; i11 += 3) {
                int I = I(i11);
                int i12 = (I & 267386880) >>> 20;
                int i13 = this.c[i11];
                long j = I & 1048575;
                if (i12 >= o4.DOUBLE_LIST_PACKED.a() && i12 <= o4.SINT64_LIST_PACKED.a()) {
                    int i14 = this.c[i11 + 2];
                }
                switch (i12) {
                    case 0:
                        if (v(t, i11)) {
                            i6 = zzhi.u(i13);
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 1:
                        if (v(t, i11)) {
                            i6 = zzhi.n(i13);
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 2:
                        if (v(t, i11)) {
                            i6 = zzhi.B(i13, j7.i(t, j));
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 3:
                        if (v(t, i11)) {
                            i6 = zzhi.E(i13, j7.i(t, j));
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 4:
                        if (v(t, i11)) {
                            i6 = zzhi.I(i13, j7.b(t, j));
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 5:
                        if (v(t, i11)) {
                            i6 = zzhi.N(i13);
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 6:
                        if (v(t, i11)) {
                            i6 = zzhi.S(i13);
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 7:
                        if (v(t, i11)) {
                            i6 = zzhi.v(i13);
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 8:
                        if (v(t, i11)) {
                            Object r = j7.r(t, j);
                            if (r instanceof t3) {
                                i6 = zzhi.w(i13, (t3) r);
                            } else {
                                i6 = zzhi.o(i13, (String) r);
                            }
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 9:
                        if (v(t, i11)) {
                            i6 = s6.a(i13, j7.r(t, j), o(i11));
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 10:
                        if (v(t, i11)) {
                            i6 = zzhi.w(i13, (t3) j7.r(t, j));
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 11:
                        if (v(t, i11)) {
                            i6 = zzhi.M(i13, j7.b(t, j));
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 12:
                        if (v(t, i11)) {
                            i6 = zzhi.U(i13, j7.b(t, j));
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 13:
                        if (v(t, i11)) {
                            i6 = zzhi.T(i13);
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 14:
                        if (v(t, i11)) {
                            i6 = zzhi.Q(i13);
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 15:
                        if (v(t, i11)) {
                            i6 = zzhi.P(i13, j7.b(t, j));
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 16:
                        if (v(t, i11)) {
                            i6 = zzhi.J(i13, j7.i(t, j));
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 17:
                        if (v(t, i11)) {
                            i6 = zzhi.x(i13, (c6) j7.r(t, j), o(i11));
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 18:
                        i6 = s6.K(i13, q(t, j));
                        i10 += i6;
                        break;
                    case 19:
                        i6 = s6.H(i13, q(t, j));
                        i10 += i6;
                        break;
                    case 20:
                        i6 = s6.k(i13, q(t, j));
                        i10 += i6;
                        break;
                    case 21:
                        i6 = s6.r(i13, q(t, j));
                        i10 += i6;
                        break;
                    case 22:
                        i6 = s6.y(i13, q(t, j));
                        i10 += i6;
                        break;
                    case 23:
                        i6 = s6.K(i13, q(t, j));
                        i10 += i6;
                        break;
                    case 24:
                        i6 = s6.H(i13, q(t, j));
                        i10 += i6;
                        break;
                    case 25:
                        List<?> q = q(t, j);
                        Class<?> cls = s6.a;
                        int size = q.size();
                        i6 = size == 0 ? 0 : size * zzhi.v(i13);
                        i10 += i6;
                        break;
                    case 26:
                        i6 = s6.b(i13, q(t, j));
                        i10 += i6;
                        break;
                    case 27:
                        i6 = s6.c(i13, q(t, j), o(i11));
                        i10 += i6;
                        break;
                    case 28:
                        i6 = s6.l(i13, q(t, j));
                        i10 += i6;
                        break;
                    case 29:
                        i6 = s6.B(i13, q(t, j));
                        i10 += i6;
                        break;
                    case 30:
                        i6 = s6.v(i13, q(t, j));
                        i10 += i6;
                        break;
                    case 31:
                        i6 = s6.H(i13, q(t, j));
                        i10 += i6;
                        break;
                    case 32:
                        i6 = s6.K(i13, q(t, j));
                        i10 += i6;
                        break;
                    case 33:
                        i6 = s6.E(i13, q(t, j));
                        i10 += i6;
                        break;
                    case 34:
                        i6 = s6.s(i13, q(t, j));
                        i10 += i6;
                        break;
                    case 35:
                        i9 = s6.L((List) unsafe.getObject(t, j));
                        if (i9 > 0) {
                            i8 = zzhi.D(i13);
                            i7 = zzhi.L(i9);
                            i10 = i7 + i8 + i9 + i10;
                            break;
                        } else {
                            break;
                        }
                    case 36:
                        i9 = s6.I((List) unsafe.getObject(t, j));
                        if (i9 > 0) {
                            i8 = zzhi.D(i13);
                            i7 = zzhi.L(i9);
                            i10 = i7 + i8 + i9 + i10;
                            break;
                        } else {
                            break;
                        }
                    case 37:
                        i9 = s6.d((List) unsafe.getObject(t, j));
                        if (i9 > 0) {
                            i8 = zzhi.D(i13);
                            i7 = zzhi.L(i9);
                            i10 = i7 + i8 + i9 + i10;
                            break;
                        } else {
                            break;
                        }
                    case 38:
                        i9 = s6.n((List) unsafe.getObject(t, j));
                        if (i9 > 0) {
                            i8 = zzhi.D(i13);
                            i7 = zzhi.L(i9);
                            i10 = i7 + i8 + i9 + i10;
                            break;
                        } else {
                            break;
                        }
                    case 39:
                        i9 = s6.z((List) unsafe.getObject(t, j));
                        if (i9 > 0) {
                            i8 = zzhi.D(i13);
                            i7 = zzhi.L(i9);
                            i10 = i7 + i8 + i9 + i10;
                            break;
                        } else {
                            break;
                        }
                    case 40:
                        i9 = s6.L((List) unsafe.getObject(t, j));
                        if (i9 > 0) {
                            i8 = zzhi.D(i13);
                            i7 = zzhi.L(i9);
                            i10 = i7 + i8 + i9 + i10;
                            break;
                        } else {
                            break;
                        }
                    case 41:
                        i9 = s6.I((List) unsafe.getObject(t, j));
                        if (i9 > 0) {
                            i8 = zzhi.D(i13);
                            i7 = zzhi.L(i9);
                            i10 = i7 + i8 + i9 + i10;
                            break;
                        } else {
                            break;
                        }
                    case 42:
                        Class<?> cls2 = s6.a;
                        i9 = ((List) unsafe.getObject(t, j)).size();
                        if (i9 > 0) {
                            i8 = zzhi.D(i13);
                            i7 = zzhi.L(i9);
                            i10 = i7 + i8 + i9 + i10;
                            break;
                        } else {
                            break;
                        }
                    case 43:
                        i9 = s6.C((List) unsafe.getObject(t, j));
                        if (i9 > 0) {
                            i8 = zzhi.D(i13);
                            i7 = zzhi.L(i9);
                            i10 = i7 + i8 + i9 + i10;
                            break;
                        } else {
                            break;
                        }
                    case 44:
                        i9 = s6.w((List) unsafe.getObject(t, j));
                        if (i9 > 0) {
                            i8 = zzhi.D(i13);
                            i7 = zzhi.L(i9);
                            i10 = i7 + i8 + i9 + i10;
                            break;
                        } else {
                            break;
                        }
                    case 45:
                        i9 = s6.I((List) unsafe.getObject(t, j));
                        if (i9 > 0) {
                            i8 = zzhi.D(i13);
                            i7 = zzhi.L(i9);
                            i10 = i7 + i8 + i9 + i10;
                            break;
                        } else {
                            break;
                        }
                    case 46:
                        i9 = s6.L((List) unsafe.getObject(t, j));
                        if (i9 > 0) {
                            i8 = zzhi.D(i13);
                            i7 = zzhi.L(i9);
                            i10 = i7 + i8 + i9 + i10;
                            break;
                        } else {
                            break;
                        }
                    case 47:
                        i9 = s6.F((List) unsafe.getObject(t, j));
                        if (i9 > 0) {
                            i8 = zzhi.D(i13);
                            i7 = zzhi.L(i9);
                            i10 = i7 + i8 + i9 + i10;
                            break;
                        } else {
                            break;
                        }
                    case 48:
                        i9 = s6.t((List) unsafe.getObject(t, j));
                        if (i9 > 0) {
                            i8 = zzhi.D(i13);
                            i7 = zzhi.L(i9);
                            i10 = i7 + i8 + i9 + i10;
                            break;
                        } else {
                            break;
                        }
                    case 49:
                        i6 = s6.m(i13, q(t, j), o(i11));
                        i10 += i6;
                        break;
                    case 50:
                        i6 = this.q.i(i13, j7.r(t, j), A(i11));
                        i10 += i6;
                        break;
                    case 51:
                        if (w(t, i13, i11)) {
                            i6 = zzhi.u(i13);
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 52:
                        if (w(t, i13, i11)) {
                            i6 = zzhi.n(i13);
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 53:
                        if (w(t, i13, i11)) {
                            i6 = zzhi.B(i13, L(t, j));
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 54:
                        if (w(t, i13, i11)) {
                            i6 = zzhi.E(i13, L(t, j));
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 55:
                        if (w(t, i13, i11)) {
                            i6 = zzhi.I(i13, J(t, j));
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 56:
                        if (w(t, i13, i11)) {
                            i6 = zzhi.N(i13);
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 57:
                        if (w(t, i13, i11)) {
                            i6 = zzhi.S(i13);
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 58:
                        if (w(t, i13, i11)) {
                            i6 = zzhi.v(i13);
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 59:
                        if (w(t, i13, i11)) {
                            Object r2 = j7.r(t, j);
                            if (r2 instanceof t3) {
                                i6 = zzhi.w(i13, (t3) r2);
                            } else {
                                i6 = zzhi.o(i13, (String) r2);
                            }
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 60:
                        if (w(t, i13, i11)) {
                            i6 = s6.a(i13, j7.r(t, j), o(i11));
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 61:
                        if (w(t, i13, i11)) {
                            i6 = zzhi.w(i13, (t3) j7.r(t, j));
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 62:
                        if (w(t, i13, i11)) {
                            i6 = zzhi.M(i13, J(t, j));
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 63:
                        if (w(t, i13, i11)) {
                            i6 = zzhi.U(i13, J(t, j));
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 64:
                        if (w(t, i13, i11)) {
                            i6 = zzhi.T(i13);
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 65:
                        if (w(t, i13, i11)) {
                            i6 = zzhi.Q(i13);
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 66:
                        if (w(t, i13, i11)) {
                            i6 = zzhi.P(i13, J(t, j));
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 67:
                        if (w(t, i13, i11)) {
                            i6 = zzhi.J(i13, L(t, j));
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                    case 68:
                        if (w(t, i13, i11)) {
                            i6 = zzhi.x(i13, (c6) j7.r(t, j), o(i11));
                            i10 += i6;
                            break;
                        } else {
                            break;
                        }
                }
            }
            d7<?, ?> d7Var = this.o;
            return d7Var.j(d7Var.e(t)) + i10;
        }
        Unsafe unsafe2 = f1437b;
        int i15 = 0;
        int i16 = 1048575;
        int i17 = 0;
        for (int i18 = 0; i18 < this.c.length; i18 += 3) {
            int I2 = I(i18);
            int[] iArr = this.c;
            int i19 = iArr[i18];
            int i20 = (I2 & 267386880) >>> 20;
            if (i20 <= 17) {
                int i21 = iArr[i18 + 2];
                int i22 = i21 & 1048575;
                i = 1 << (i21 >>> 20);
                if (i22 != i16) {
                    i17 = unsafe2.getInt(t, i22);
                    i16 = i22;
                }
            } else {
                i = 0;
            }
            long j2 = I2 & 1048575;
            switch (i20) {
                case 0:
                    if ((i17 & i) != 0) {
                        i2 = zzhi.u(i19);
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 1:
                    if ((i17 & i) != 0) {
                        i2 = zzhi.n(i19);
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 2:
                    if ((i17 & i) != 0) {
                        i2 = zzhi.B(i19, unsafe2.getLong(t, j2));
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 3:
                    if ((i17 & i) != 0) {
                        i2 = zzhi.E(i19, unsafe2.getLong(t, j2));
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 4:
                    if ((i17 & i) != 0) {
                        i2 = zzhi.I(i19, unsafe2.getInt(t, j2));
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 5:
                    if ((i17 & i) != 0) {
                        i2 = zzhi.N(i19);
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 6:
                    if ((i17 & i) != 0) {
                        i2 = zzhi.S(i19);
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 7:
                    if ((i17 & i) != 0) {
                        i2 = zzhi.v(i19);
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 8:
                    if ((i17 & i) != 0) {
                        Object object = unsafe2.getObject(t, j2);
                        if (object instanceof t3) {
                            i2 = zzhi.w(i19, (t3) object);
                        } else {
                            i2 = zzhi.o(i19, (String) object);
                        }
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 9:
                    if ((i17 & i) != 0) {
                        i2 = s6.a(i19, unsafe2.getObject(t, j2), o(i18));
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 10:
                    if ((i17 & i) != 0) {
                        i2 = zzhi.w(i19, (t3) unsafe2.getObject(t, j2));
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 11:
                    if ((i17 & i) != 0) {
                        i2 = zzhi.M(i19, unsafe2.getInt(t, j2));
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 12:
                    if ((i17 & i) != 0) {
                        i2 = zzhi.U(i19, unsafe2.getInt(t, j2));
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 13:
                    if ((i17 & i) != 0) {
                        i2 = zzhi.T(i19);
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 14:
                    if ((i17 & i) != 0) {
                        i2 = zzhi.Q(i19);
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 15:
                    if ((i17 & i) != 0) {
                        i2 = zzhi.P(i19, unsafe2.getInt(t, j2));
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 16:
                    if ((i17 & i) != 0) {
                        i2 = zzhi.J(i19, unsafe2.getLong(t, j2));
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 17:
                    if ((i17 & i) != 0) {
                        i2 = zzhi.x(i19, (c6) unsafe2.getObject(t, j2), o(i18));
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 18:
                    i2 = s6.K(i19, (List) unsafe2.getObject(t, j2));
                    i15 += i2;
                    break;
                case 19:
                    i2 = s6.H(i19, (List) unsafe2.getObject(t, j2));
                    i15 += i2;
                    break;
                case 20:
                    i2 = s6.k(i19, (List) unsafe2.getObject(t, j2));
                    i15 += i2;
                    break;
                case 21:
                    i2 = s6.r(i19, (List) unsafe2.getObject(t, j2));
                    i15 += i2;
                    break;
                case 22:
                    i2 = s6.y(i19, (List) unsafe2.getObject(t, j2));
                    i15 += i2;
                    break;
                case 23:
                    i2 = s6.K(i19, (List) unsafe2.getObject(t, j2));
                    i15 += i2;
                    break;
                case 24:
                    i2 = s6.H(i19, (List) unsafe2.getObject(t, j2));
                    i15 += i2;
                    break;
                case 25:
                    Class<?> cls3 = s6.a;
                    int size2 = ((List) unsafe2.getObject(t, j2)).size();
                    i2 = size2 == 0 ? 0 : size2 * zzhi.v(i19);
                    i15 += i2;
                    break;
                case 26:
                    i2 = s6.b(i19, (List) unsafe2.getObject(t, j2));
                    i15 += i2;
                    break;
                case 27:
                    i2 = s6.c(i19, (List) unsafe2.getObject(t, j2), o(i18));
                    i15 += i2;
                    break;
                case 28:
                    i2 = s6.l(i19, (List) unsafe2.getObject(t, j2));
                    i15 += i2;
                    break;
                case 29:
                    i2 = s6.B(i19, (List) unsafe2.getObject(t, j2));
                    i15 += i2;
                    break;
                case 30:
                    i2 = s6.v(i19, (List) unsafe2.getObject(t, j2));
                    i15 += i2;
                    break;
                case 31:
                    i2 = s6.H(i19, (List) unsafe2.getObject(t, j2));
                    i15 += i2;
                    break;
                case 32:
                    i2 = s6.K(i19, (List) unsafe2.getObject(t, j2));
                    i15 += i2;
                    break;
                case 33:
                    i2 = s6.E(i19, (List) unsafe2.getObject(t, j2));
                    i15 += i2;
                    break;
                case 34:
                    i2 = s6.s(i19, (List) unsafe2.getObject(t, j2));
                    i15 += i2;
                    break;
                case 35:
                    i5 = s6.L((List) unsafe2.getObject(t, j2));
                    if (i5 > 0) {
                        i4 = zzhi.D(i19);
                        i3 = zzhi.L(i5);
                        i15 = i3 + i4 + i5 + i15;
                        break;
                    } else {
                        break;
                    }
                case 36:
                    i5 = s6.I((List) unsafe2.getObject(t, j2));
                    if (i5 > 0) {
                        i4 = zzhi.D(i19);
                        i3 = zzhi.L(i5);
                        i15 = i3 + i4 + i5 + i15;
                        break;
                    } else {
                        break;
                    }
                case 37:
                    i5 = s6.d((List) unsafe2.getObject(t, j2));
                    if (i5 > 0) {
                        i4 = zzhi.D(i19);
                        i3 = zzhi.L(i5);
                        i15 = i3 + i4 + i5 + i15;
                        break;
                    } else {
                        break;
                    }
                case 38:
                    i5 = s6.n((List) unsafe2.getObject(t, j2));
                    if (i5 > 0) {
                        i4 = zzhi.D(i19);
                        i3 = zzhi.L(i5);
                        i15 = i3 + i4 + i5 + i15;
                        break;
                    } else {
                        break;
                    }
                case 39:
                    i5 = s6.z((List) unsafe2.getObject(t, j2));
                    if (i5 > 0) {
                        i4 = zzhi.D(i19);
                        i3 = zzhi.L(i5);
                        i15 = i3 + i4 + i5 + i15;
                        break;
                    } else {
                        break;
                    }
                case 40:
                    i5 = s6.L((List) unsafe2.getObject(t, j2));
                    if (i5 > 0) {
                        i4 = zzhi.D(i19);
                        i3 = zzhi.L(i5);
                        i15 = i3 + i4 + i5 + i15;
                        break;
                    } else {
                        break;
                    }
                case 41:
                    i5 = s6.I((List) unsafe2.getObject(t, j2));
                    if (i5 > 0) {
                        i4 = zzhi.D(i19);
                        i3 = zzhi.L(i5);
                        i15 = i3 + i4 + i5 + i15;
                        break;
                    } else {
                        break;
                    }
                case 42:
                    Class<?> cls4 = s6.a;
                    i5 = ((List) unsafe2.getObject(t, j2)).size();
                    if (i5 > 0) {
                        i4 = zzhi.D(i19);
                        i3 = zzhi.L(i5);
                        i15 = i3 + i4 + i5 + i15;
                        break;
                    } else {
                        break;
                    }
                case 43:
                    i5 = s6.C((List) unsafe2.getObject(t, j2));
                    if (i5 > 0) {
                        i4 = zzhi.D(i19);
                        i3 = zzhi.L(i5);
                        i15 = i3 + i4 + i5 + i15;
                        break;
                    } else {
                        break;
                    }
                case 44:
                    i5 = s6.w((List) unsafe2.getObject(t, j2));
                    if (i5 > 0) {
                        i4 = zzhi.D(i19);
                        i3 = zzhi.L(i5);
                        i15 = i3 + i4 + i5 + i15;
                        break;
                    } else {
                        break;
                    }
                case 45:
                    i5 = s6.I((List) unsafe2.getObject(t, j2));
                    if (i5 > 0) {
                        i4 = zzhi.D(i19);
                        i3 = zzhi.L(i5);
                        i15 = i3 + i4 + i5 + i15;
                        break;
                    } else {
                        break;
                    }
                case 46:
                    i5 = s6.L((List) unsafe2.getObject(t, j2));
                    if (i5 > 0) {
                        i4 = zzhi.D(i19);
                        i3 = zzhi.L(i5);
                        i15 = i3 + i4 + i5 + i15;
                        break;
                    } else {
                        break;
                    }
                case 47:
                    i5 = s6.F((List) unsafe2.getObject(t, j2));
                    if (i5 > 0) {
                        i4 = zzhi.D(i19);
                        i3 = zzhi.L(i5);
                        i15 = i3 + i4 + i5 + i15;
                        break;
                    } else {
                        break;
                    }
                case 48:
                    i5 = s6.t((List) unsafe2.getObject(t, j2));
                    if (i5 > 0) {
                        i4 = zzhi.D(i19);
                        i3 = zzhi.L(i5);
                        i15 = i3 + i4 + i5 + i15;
                        break;
                    } else {
                        break;
                    }
                case 49:
                    i2 = s6.m(i19, (List) unsafe2.getObject(t, j2), o(i18));
                    i15 += i2;
                    break;
                case 50:
                    i2 = this.q.i(i19, unsafe2.getObject(t, j2), A(i18));
                    i15 += i2;
                    break;
                case 51:
                    if (w(t, i19, i18)) {
                        i2 = zzhi.u(i19);
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 52:
                    if (w(t, i19, i18)) {
                        i2 = zzhi.n(i19);
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 53:
                    if (w(t, i19, i18)) {
                        i2 = zzhi.B(i19, L(t, j2));
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 54:
                    if (w(t, i19, i18)) {
                        i2 = zzhi.E(i19, L(t, j2));
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 55:
                    if (w(t, i19, i18)) {
                        i2 = zzhi.I(i19, J(t, j2));
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 56:
                    if (w(t, i19, i18)) {
                        i2 = zzhi.N(i19);
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 57:
                    if (w(t, i19, i18)) {
                        i2 = zzhi.S(i19);
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 58:
                    if (w(t, i19, i18)) {
                        i2 = zzhi.v(i19);
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 59:
                    if (w(t, i19, i18)) {
                        Object object2 = unsafe2.getObject(t, j2);
                        if (object2 instanceof t3) {
                            i2 = zzhi.w(i19, (t3) object2);
                        } else {
                            i2 = zzhi.o(i19, (String) object2);
                        }
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 60:
                    if (w(t, i19, i18)) {
                        i2 = s6.a(i19, unsafe2.getObject(t, j2), o(i18));
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 61:
                    if (w(t, i19, i18)) {
                        i2 = zzhi.w(i19, (t3) unsafe2.getObject(t, j2));
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 62:
                    if (w(t, i19, i18)) {
                        i2 = zzhi.M(i19, J(t, j2));
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 63:
                    if (w(t, i19, i18)) {
                        i2 = zzhi.U(i19, J(t, j2));
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 64:
                    if (w(t, i19, i18)) {
                        i2 = zzhi.T(i19);
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 65:
                    if (w(t, i19, i18)) {
                        i2 = zzhi.Q(i19);
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 66:
                    if (w(t, i19, i18)) {
                        i2 = zzhi.P(i19, J(t, j2));
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 67:
                    if (w(t, i19, i18)) {
                        i2 = zzhi.J(i19, L(t, j2));
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
                case 68:
                    if (w(t, i19, i18)) {
                        i2 = zzhi.x(i19, (c6) unsafe2.getObject(t, j2), o(i18));
                        i15 += i2;
                        break;
                    } else {
                        break;
                    }
            }
        }
        d7<?, ?> d7Var2 = this.o;
        int j3 = d7Var2.j(d7Var2.e(t)) + i15;
        if (!this.h) {
            return j3;
        }
        n4<?> b2 = this.p.b(t);
        int i23 = 0;
        for (int i24 = 0; i24 < b2.f1457b.e(); i24++) {
            Map.Entry<?, Object> d = b2.f1457b.d(i24);
            i23 += n4.a((p4) d.getKey(), d.getValue());
        }
        for (Map.Entry<?, Object> entry : b2.f1457b.g()) {
            i23 += n4.a((p4) entry.getKey(), entry.getValue());
        }
        return j3 + i23;
    }

    /* JADX WARN: Code restructure failed: missing block: B:10:0x0038, code lost:
        if (b.i.a.f.h.l.s6.j(b.i.a.f.h.l.j7.r(r10, r6), b.i.a.f.h.l.j7.r(r11, r6)) != false) goto L86;
     */
    /* JADX WARN: Code restructure failed: missing block: B:16:0x006a, code lost:
        if (b.i.a.f.h.l.s6.j(b.i.a.f.h.l.j7.r(r10, r6), b.i.a.f.h.l.j7.r(r11, r6)) != false) goto L86;
     */
    /* JADX WARN: Code restructure failed: missing block: B:20:0x007e, code lost:
        if (b.i.a.f.h.l.j7.i(r10, r6) == b.i.a.f.h.l.j7.i(r11, r6)) goto L86;
     */
    /* JADX WARN: Code restructure failed: missing block: B:24:0x0090, code lost:
        if (b.i.a.f.h.l.j7.b(r10, r6) == b.i.a.f.h.l.j7.b(r11, r6)) goto L86;
     */
    /* JADX WARN: Code restructure failed: missing block: B:28:0x00a4, code lost:
        if (b.i.a.f.h.l.j7.i(r10, r6) == b.i.a.f.h.l.j7.i(r11, r6)) goto L86;
     */
    /* JADX WARN: Code restructure failed: missing block: B:32:0x00b6, code lost:
        if (b.i.a.f.h.l.j7.b(r10, r6) == b.i.a.f.h.l.j7.b(r11, r6)) goto L86;
     */
    /* JADX WARN: Code restructure failed: missing block: B:36:0x00c8, code lost:
        if (b.i.a.f.h.l.j7.b(r10, r6) == b.i.a.f.h.l.j7.b(r11, r6)) goto L86;
     */
    /* JADX WARN: Code restructure failed: missing block: B:40:0x00da, code lost:
        if (b.i.a.f.h.l.j7.b(r10, r6) == b.i.a.f.h.l.j7.b(r11, r6)) goto L86;
     */
    /* JADX WARN: Code restructure failed: missing block: B:44:0x00f0, code lost:
        if (b.i.a.f.h.l.s6.j(b.i.a.f.h.l.j7.r(r10, r6), b.i.a.f.h.l.j7.r(r11, r6)) != false) goto L86;
     */
    /* JADX WARN: Code restructure failed: missing block: B:48:0x0106, code lost:
        if (b.i.a.f.h.l.s6.j(b.i.a.f.h.l.j7.r(r10, r6), b.i.a.f.h.l.j7.r(r11, r6)) != false) goto L86;
     */
    /* JADX WARN: Code restructure failed: missing block: B:52:0x011c, code lost:
        if (b.i.a.f.h.l.s6.j(b.i.a.f.h.l.j7.r(r10, r6), b.i.a.f.h.l.j7.r(r11, r6)) != false) goto L86;
     */
    /* JADX WARN: Code restructure failed: missing block: B:56:0x012e, code lost:
        if (b.i.a.f.h.l.j7.m(r10, r6) == b.i.a.f.h.l.j7.m(r11, r6)) goto L86;
     */
    /* JADX WARN: Code restructure failed: missing block: B:60:0x0140, code lost:
        if (b.i.a.f.h.l.j7.b(r10, r6) == b.i.a.f.h.l.j7.b(r11, r6)) goto L86;
     */
    /* JADX WARN: Code restructure failed: missing block: B:64:0x0154, code lost:
        if (b.i.a.f.h.l.j7.i(r10, r6) == b.i.a.f.h.l.j7.i(r11, r6)) goto L86;
     */
    /* JADX WARN: Code restructure failed: missing block: B:68:0x0165, code lost:
        if (b.i.a.f.h.l.j7.b(r10, r6) == b.i.a.f.h.l.j7.b(r11, r6)) goto L86;
     */
    /* JADX WARN: Code restructure failed: missing block: B:72:0x0178, code lost:
        if (b.i.a.f.h.l.j7.i(r10, r6) == b.i.a.f.h.l.j7.i(r11, r6)) goto L86;
     */
    /* JADX WARN: Code restructure failed: missing block: B:76:0x018b, code lost:
        if (b.i.a.f.h.l.j7.i(r10, r6) == b.i.a.f.h.l.j7.i(r11, r6)) goto L86;
     */
    /* JADX WARN: Code restructure failed: missing block: B:80:0x01a4, code lost:
        if (java.lang.Float.floatToIntBits(b.i.a.f.h.l.j7.n(r10, r6)) == java.lang.Float.floatToIntBits(b.i.a.f.h.l.j7.n(r11, r6))) goto L86;
     */
    /* JADX WARN: Code restructure failed: missing block: B:84:0x01bf, code lost:
        if (java.lang.Double.doubleToLongBits(b.i.a.f.h.l.j7.q(r10, r6)) == java.lang.Double.doubleToLongBits(b.i.a.f.h.l.j7.q(r11, r6))) goto L86;
     */
    @Override // b.i.a.f.h.l.q6
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final boolean f(T r10, T r11) {
        /*
            Method dump skipped, instructions count: 640
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.h.l.e6.f(java.lang.Object, java.lang.Object):boolean");
    }

    /* JADX WARN: Removed duplicated region for block: B:12:0x002e  */
    /* JADX WARN: Removed duplicated region for block: B:165:0x059e  */
    @Override // b.i.a.f.h.l.q6
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void g(T r13, b.i.a.f.h.l.v7 r14) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 1610
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.h.l.e6.g(java.lang.Object, b.i.a.f.h.l.v7):void");
    }

    /* JADX WARN: Code restructure failed: missing block: B:118:0x0340, code lost:
        if (r0 == r14) goto L128;
     */
    /* JADX WARN: Code restructure failed: missing block: B:127:0x0387, code lost:
        if (r0 == r15) goto L128;
     */
    /* JADX WARN: Code restructure failed: missing block: B:130:0x03a1, code lost:
        r2 = r18;
        r10 = r19;
        r8 = r24;
     */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v16, types: [int] */
    @Override // b.i.a.f.h.l.q6
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void h(T r29, byte[] r30, int r31, int r32, b.i.a.f.h.l.s3 r33) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 1046
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.h.l.e6.h(java.lang.Object, byte[], int, int, b.i.a.f.h.l.s3):void");
    }

    @Override // b.i.a.f.h.l.q6
    public final void i(T t, T t2) {
        Objects.requireNonNull(t2);
        for (int i = 0; i < this.c.length; i += 3) {
            int I = I(i);
            long j = 1048575 & I;
            int i2 = this.c[i];
            switch ((I & 267386880) >>> 20) {
                case 0:
                    if (v(t2, i)) {
                        j7.d(t, j, j7.q(t2, j));
                        B(t, i);
                        break;
                    } else {
                        break;
                    }
                case 1:
                    if (v(t2, i)) {
                        j7.e.d(t, j, j7.n(t2, j));
                        B(t, i);
                        break;
                    } else {
                        break;
                    }
                case 2:
                    if (v(t2, i)) {
                        j7.e(t, j, j7.i(t2, j));
                        B(t, i);
                        break;
                    } else {
                        break;
                    }
                case 3:
                    if (v(t2, i)) {
                        j7.e(t, j, j7.i(t2, j));
                        B(t, i);
                        break;
                    } else {
                        break;
                    }
                case 4:
                    if (v(t2, i)) {
                        j7.e.e(t, j, j7.b(t2, j));
                        B(t, i);
                        break;
                    } else {
                        break;
                    }
                case 5:
                    if (v(t2, i)) {
                        j7.e(t, j, j7.i(t2, j));
                        B(t, i);
                        break;
                    } else {
                        break;
                    }
                case 6:
                    if (v(t2, i)) {
                        j7.e.e(t, j, j7.b(t2, j));
                        B(t, i);
                        break;
                    } else {
                        break;
                    }
                case 7:
                    if (v(t2, i)) {
                        j7.e.g(t, j, j7.m(t2, j));
                        B(t, i);
                        break;
                    } else {
                        break;
                    }
                case 8:
                    if (v(t2, i)) {
                        j7.f(t, j, j7.r(t2, j));
                        B(t, i);
                        break;
                    } else {
                        break;
                    }
                case 9:
                    u(t, t2, i);
                    break;
                case 10:
                    if (v(t2, i)) {
                        j7.f(t, j, j7.r(t2, j));
                        B(t, i);
                        break;
                    } else {
                        break;
                    }
                case 11:
                    if (v(t2, i)) {
                        j7.e.e(t, j, j7.b(t2, j));
                        B(t, i);
                        break;
                    } else {
                        break;
                    }
                case 12:
                    if (v(t2, i)) {
                        j7.e.e(t, j, j7.b(t2, j));
                        B(t, i);
                        break;
                    } else {
                        break;
                    }
                case 13:
                    if (v(t2, i)) {
                        j7.e.e(t, j, j7.b(t2, j));
                        B(t, i);
                        break;
                    } else {
                        break;
                    }
                case 14:
                    if (v(t2, i)) {
                        j7.e(t, j, j7.i(t2, j));
                        B(t, i);
                        break;
                    } else {
                        break;
                    }
                case 15:
                    if (v(t2, i)) {
                        j7.e.e(t, j, j7.b(t2, j));
                        B(t, i);
                        break;
                    } else {
                        break;
                    }
                case 16:
                    if (v(t2, i)) {
                        j7.e(t, j, j7.i(t2, j));
                        B(t, i);
                        break;
                    } else {
                        break;
                    }
                case 17:
                    u(t, t2, i);
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    this.n.a(t, t2, j);
                    break;
                case 50:
                    v5 v5Var = this.q;
                    Class<?> cls = s6.a;
                    j7.f(t, j, v5Var.f(j7.r(t, j), j7.r(t2, j)));
                    break;
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 57:
                case 58:
                case 59:
                    if (w(t2, i2, i)) {
                        j7.f(t, j, j7.r(t2, j));
                        C(t, i2, i);
                        break;
                    } else {
                        break;
                    }
                case 60:
                    E(t, t2, i);
                    break;
                case 61:
                case 62:
                case 63:
                case 64:
                case 65:
                case 66:
                case 67:
                    if (w(t2, i2, i)) {
                        j7.f(t, j, j7.r(t2, j));
                        C(t, i2, i);
                        break;
                    } else {
                        break;
                    }
                case 68:
                    E(t, t2, i);
                    break;
            }
        }
        d7<?, ?> d7Var = this.o;
        Class<?> cls2 = s6.a;
        d7Var.d(t, d7Var.g(d7Var.e(t), d7Var.e(t2)));
        if (this.h) {
            s6.i(this.p, t, t2);
        }
    }

    public final int j(T t, byte[] bArr, int i, int i2, int i3, int i4, int i5, int i6, int i7, long j, int i8, s3 s3Var) throws IOException {
        int i9;
        Unsafe unsafe = f1437b;
        long j2 = this.c[i8 + 2] & 1048575;
        boolean z2 = true;
        switch (i7) {
            case 51:
                if (i5 == 1) {
                    unsafe.putObject(t, j, Double.valueOf(Double.longBitsToDouble(f.o2(bArr, i))));
                    i9 = i + 8;
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 52:
                if (i5 == 5) {
                    unsafe.putObject(t, j, Float.valueOf(Float.intBitsToFloat(f.N1(bArr, i))));
                    i9 = i + 4;
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 53:
            case 54:
                if (i5 == 0) {
                    i9 = f.n2(bArr, i, s3Var);
                    unsafe.putObject(t, j, Long.valueOf(s3Var.f1489b));
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 55:
            case 62:
                if (i5 == 0) {
                    i9 = f.O1(bArr, i, s3Var);
                    unsafe.putObject(t, j, Integer.valueOf(s3Var.a));
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 56:
            case 65:
                if (i5 == 1) {
                    unsafe.putObject(t, j, Long.valueOf(f.o2(bArr, i)));
                    i9 = i + 8;
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 57:
            case 64:
                if (i5 == 5) {
                    unsafe.putObject(t, j, Integer.valueOf(f.N1(bArr, i)));
                    i9 = i + 4;
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 58:
                if (i5 == 0) {
                    i9 = f.n2(bArr, i, s3Var);
                    if (s3Var.f1489b == 0) {
                        z2 = false;
                    }
                    unsafe.putObject(t, j, Boolean.valueOf(z2));
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 59:
                if (i5 == 2) {
                    int O1 = f.O1(bArr, i, s3Var);
                    int i10 = s3Var.a;
                    if (i10 == 0) {
                        unsafe.putObject(t, j, "");
                    } else if ((i6 & 536870912) == 0 || k7.b(bArr, O1, O1 + i10)) {
                        unsafe.putObject(t, j, new String(bArr, O1, i10, w4.a));
                        O1 += i10;
                    } else {
                        throw zzij.e();
                    }
                    unsafe.putInt(t, j2, i4);
                    return O1;
                }
                return i;
            case 60:
                if (i5 == 2) {
                    int M1 = f.M1(o(i8), bArr, i, i2, s3Var);
                    Object object = unsafe.getInt(t, j2) == i4 ? unsafe.getObject(t, j) : null;
                    if (object == null) {
                        unsafe.putObject(t, j, s3Var.c);
                    } else {
                        unsafe.putObject(t, j, w4.c(object, s3Var.c));
                    }
                    unsafe.putInt(t, j2, i4);
                    return M1;
                }
                return i;
            case 61:
                if (i5 == 2) {
                    i9 = f.v2(bArr, i, s3Var);
                    unsafe.putObject(t, j, s3Var.c);
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 63:
                if (i5 == 0) {
                    int O12 = f.O1(bArr, i, s3Var);
                    int i11 = s3Var.a;
                    a5 a5Var = (a5) this.d[((i8 / 3) << 1) + 1];
                    if (a5Var == null || a5Var.f(i11)) {
                        unsafe.putObject(t, j, Integer.valueOf(i11));
                        i9 = O12;
                        unsafe.putInt(t, j2, i4);
                        return i9;
                    }
                    M(t).a(i3, Long.valueOf(i11));
                    return O12;
                }
                return i;
            case 66:
                if (i5 == 0) {
                    i9 = f.O1(bArr, i, s3Var);
                    unsafe.putObject(t, j, Integer.valueOf(d4.b(s3Var.a)));
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 67:
                if (i5 == 0) {
                    i9 = f.n2(bArr, i, s3Var);
                    unsafe.putObject(t, j, Long.valueOf(d4.a(s3Var.f1489b)));
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 68:
                if (i5 == 3) {
                    i9 = f.L1(o(i8), bArr, i, i2, (i3 & (-8)) | 4, s3Var);
                    Object object2 = unsafe.getInt(t, j2) == i4 ? unsafe.getObject(t, j) : null;
                    if (object2 == null) {
                        unsafe.putObject(t, j, s3Var.c);
                    } else {
                        unsafe.putObject(t, j, w4.c(object2, s3Var.c));
                    }
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            default:
                return i;
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:118:0x0256  */
    /* JADX WARN: Removed duplicated region for block: B:138:0x02a4  */
    /* JADX WARN: Removed duplicated region for block: B:89:0x01dc  */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:116:0x0253 -> B:117:0x0254). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:136:0x02a1 -> B:137:0x02a2). Please submit an issue!!! */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:87:0x01d9 -> B:88:0x01da). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final int k(T r18, byte[] r19, int r20, int r21, int r22, int r23, int r24, int r25, long r26, int r28, long r29, b.i.a.f.h.l.s3 r31) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 1262
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.h.l.e6.k(java.lang.Object, byte[], int, int, int, int, int, int, long, int, long, b.i.a.f.h.l.s3):int");
    }

    public final <K, V> int l(T t, byte[] bArr, int i, int i2, int i3, long j, s3 s3Var) throws IOException {
        Unsafe unsafe = f1437b;
        Object obj = this.d[(i3 / 3) << 1];
        Object object = unsafe.getObject(t, j);
        if (this.q.d(object)) {
            Object h = this.q.h(obj);
            this.q.f(h, object);
            unsafe.putObject(t, j, h);
            object = h;
        }
        t5<?, ?> g = this.q.g(obj);
        this.q.b(object);
        int O1 = f.O1(bArr, i, s3Var);
        int i4 = s3Var.a;
        if (i4 < 0 || i4 > i2 - O1) {
            throw zzij.a();
        }
        Objects.requireNonNull(g);
        throw null;
    }

    /* JADX WARN: Code restructure failed: missing block: B:167:0x04fe, code lost:
        if (r5 == 1048575) goto L169;
     */
    /* JADX WARN: Code restructure failed: missing block: B:168:0x0500, code lost:
        r28.putInt(r11, r5, r6);
     */
    /* JADX WARN: Code restructure failed: missing block: B:169:0x0506, code lost:
        r3 = r7.k;
     */
    /* JADX WARN: Code restructure failed: missing block: B:171:0x050a, code lost:
        if (r3 >= r7.l) goto L241;
     */
    /* JADX WARN: Code restructure failed: missing block: B:172:0x050c, code lost:
        r4 = r7.j[r3];
        r5 = r7.o;
        r8 = r7.c[r4];
        r6 = b.i.a.f.h.l.j7.r(r11, r6[r4 + 1] & 1048575);
     */
    /* JADX WARN: Code restructure failed: missing block: B:173:0x0520, code lost:
        if (r6 != null) goto L174;
     */
    /* JADX WARN: Code restructure failed: missing block: B:174:0x0523, code lost:
        r4 = (r4 / 3) << 1;
        r8 = (b.i.a.f.h.l.a5) r7.d[r4 + 1];
     */
    /* JADX WARN: Code restructure failed: missing block: B:175:0x052f, code lost:
        if (r8 != null) goto L176;
     */
    /* JADX WARN: Code restructure failed: missing block: B:176:0x0532, code lost:
        r6 = r7.q.b(r6);
        r7.q.g(r7.d[r4]);
        r4 = r6.entrySet().iterator();
     */
    /* JADX WARN: Code restructure failed: missing block: B:178:0x054d, code lost:
        if (r4.hasNext() == false) goto L242;
     */
    /* JADX WARN: Code restructure failed: missing block: B:179:0x054f, code lost:
        r6 = r4.next();
     */
    /* JADX WARN: Code restructure failed: missing block: B:180:0x0563, code lost:
        if (r8.f(((java.lang.Integer) r6.getValue()).intValue()) == false) goto L240;
     */
    /* JADX WARN: Code restructure failed: missing block: B:181:0x0566, code lost:
        r5.a();
        r6.getKey();
        r6.getValue();
        java.util.Objects.requireNonNull(null);
     */
    /* JADX WARN: Code restructure failed: missing block: B:182:0x0572, code lost:
        throw null;
     */
    /* JADX WARN: Code restructure failed: missing block: B:183:0x0573, code lost:
        r3 = r3 + 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:184:0x0576, code lost:
        if (r1 != 0) goto L189;
     */
    /* JADX WARN: Code restructure failed: missing block: B:186:0x057a, code lost:
        if (r0 != r34) goto L187;
     */
    /* JADX WARN: Code restructure failed: missing block: B:188:0x0581, code lost:
        throw com.google.android.gms.internal.measurement.zzij.d();
     */
    /* JADX WARN: Code restructure failed: missing block: B:190:0x0584, code lost:
        if (r0 > r34) goto L193;
     */
    /* JADX WARN: Code restructure failed: missing block: B:191:0x0586, code lost:
        if (r10 != r1) goto L193;
     */
    /* JADX WARN: Code restructure failed: missing block: B:192:0x0588, code lost:
        return r0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:194:0x058d, code lost:
        throw com.google.android.gms.internal.measurement.zzij.d();
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final int m(T r31, byte[] r32, int r33, int r34, int r35, b.i.a.f.h.l.s3 r36) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 1462
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.h.l.e6.m(java.lang.Object, byte[], int, int, int, b.i.a.f.h.l.s3):int");
    }

    public final q6 o(int i) {
        int i2 = (i / 3) << 1;
        Object[] objArr = this.d;
        q6 q6Var = (q6) objArr[i2];
        if (q6Var != null) {
            return q6Var;
        }
        q6<T> a2 = n6.a.a((Class) objArr[i2 + 1]);
        this.d[i2] = a2;
        return a2;
    }

    public final <K, V> void t(v7 v7Var, int i, Object obj, int i2) throws IOException {
        if (obj != null) {
            this.q.g(this.d[(i2 / 3) << 1]);
            Map<?, ?> e = this.q.e(obj);
            g4 g4Var = (g4) v7Var;
            Objects.requireNonNull(g4Var);
            Iterator<Map.Entry<?, ?>> it = e.entrySet().iterator();
            if (it.hasNext()) {
                Map.Entry<?, ?> next = it.next();
                g4Var.a.f(i, 2);
                next.getKey();
                next.getValue();
                Objects.requireNonNull(null);
                throw null;
            }
        }
    }

    public final void u(T t, T t2, int i) {
        long j = this.c[i + 1] & 1048575;
        if (v(t2, i)) {
            Object r = j7.r(t, j);
            Object r2 = j7.r(t2, j);
            if (r != null && r2 != null) {
                j7.f(t, j, w4.c(r, r2));
                B(t, i);
            } else if (r2 != null) {
                j7.f(t, j, r2);
                B(t, i);
            }
        }
    }

    public final boolean v(T t, int i) {
        int[] iArr = this.c;
        int i2 = iArr[i + 2];
        long j = i2 & 1048575;
        if (j == 1048575) {
            int i3 = iArr[i + 1];
            long j2 = i3 & 1048575;
            switch ((i3 & 267386880) >>> 20) {
                case 0:
                    return j7.q(t, j2) != ShadowDrawableWrapper.COS_45;
                case 1:
                    return j7.n(t, j2) != 0.0f;
                case 2:
                    return j7.i(t, j2) != 0;
                case 3:
                    return j7.i(t, j2) != 0;
                case 4:
                    return j7.b(t, j2) != 0;
                case 5:
                    return j7.i(t, j2) != 0;
                case 6:
                    return j7.b(t, j2) != 0;
                case 7:
                    return j7.m(t, j2);
                case 8:
                    Object r = j7.r(t, j2);
                    if (r instanceof String) {
                        return !((String) r).isEmpty();
                    }
                    if (r instanceof t3) {
                        return !t3.j.equals(r);
                    }
                    throw new IllegalArgumentException();
                case 9:
                    return j7.r(t, j2) != null;
                case 10:
                    return !t3.j.equals(j7.r(t, j2));
                case 11:
                    return j7.b(t, j2) != 0;
                case 12:
                    return j7.b(t, j2) != 0;
                case 13:
                    return j7.b(t, j2) != 0;
                case 14:
                    return j7.i(t, j2) != 0;
                case 15:
                    return j7.b(t, j2) != 0;
                case 16:
                    return j7.i(t, j2) != 0;
                case 17:
                    return j7.r(t, j2) != null;
                default:
                    throw new IllegalArgumentException();
            }
        } else {
            return (j7.b(t, j) & (1 << (i2 >>> 20))) != 0;
        }
    }

    public final boolean w(T t, int i, int i2) {
        return j7.b(t, (long) (this.c[i2 + 2] & 1048575)) == i;
    }

    public final boolean x(T t, int i, int i2, int i3, int i4) {
        if (i2 == 1048575) {
            return v(t, i);
        }
        return (i3 & i4) != 0;
    }

    public final int z(int i, int i2) {
        int length = (this.c.length / 3) - 1;
        while (i2 <= length) {
            int i3 = (length + i2) >>> 1;
            int i4 = i3 * 3;
            int i5 = this.c[i4];
            if (i == i5) {
                return i4;
            }
            if (i < i5) {
                length = i3 - 1;
            } else {
                i2 = i3 + 1;
            }
        }
        return -1;
    }
}
