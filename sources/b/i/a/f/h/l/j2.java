package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class j2 {
    public static volatile x2<Boolean> a = w2.j;

    /* renamed from: b  reason: collision with root package name */
    public static final Object f1447b = new Object();

    /* JADX WARN: Removed duplicated region for block: B:27:0x0085 A[Catch: all -> 0x00b3, TRY_LEAVE, TryCatch #1 {, blocks: (B:12:0x0046, B:14:0x004e, B:15:0x005a, B:17:0x005c, B:20:0x006b, B:22:0x0077, B:27:0x0085, B:28:0x0089, B:29:0x008f, B:35:0x009b, B:36:0x00a5), top: B:45:0x0046 }] */
    /* JADX WARN: Removed duplicated region for block: B:34:0x009a  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static boolean a(android.content.Context r4, android.net.Uri r5) {
        /*
            java.lang.String r5 = r5.getAuthority()
            java.lang.String r0 = "com.google.android.gms.phenotype"
            boolean r0 = r0.equals(r5)
            r1 = 0
            if (r0 != 0) goto L2e
            java.lang.String r4 = "PhenotypeClientHelper"
            java.lang.String r0 = java.lang.String.valueOf(r5)
            int r0 = r0.length()
            int r0 = r0 + 91
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r0)
            r2.append(r5)
            java.lang.String r5 = " is an unsupported authority. Only com.google.android.gms.phenotype authority is supported."
            r2.append(r5)
            java.lang.String r5 = r2.toString()
            android.util.Log.e(r4, r5)
            return r1
        L2e:
            b.i.a.f.h.l.x2<java.lang.Boolean> r5 = b.i.a.f.h.l.j2.a
            boolean r5 = r5.b()
            if (r5 == 0) goto L43
            b.i.a.f.h.l.x2<java.lang.Boolean> r4 = b.i.a.f.h.l.j2.a
            java.lang.Object r4 = r4.c()
            java.lang.Boolean r4 = (java.lang.Boolean) r4
            boolean r4 = r4.booleanValue()
            return r4
        L43:
            java.lang.Object r5 = b.i.a.f.h.l.j2.f1447b
            monitor-enter(r5)
            b.i.a.f.h.l.x2<java.lang.Boolean> r0 = b.i.a.f.h.l.j2.a     // Catch: java.lang.Throwable -> Lb3
            boolean r0 = r0.b()     // Catch: java.lang.Throwable -> Lb3
            if (r0 == 0) goto L5c
            b.i.a.f.h.l.x2<java.lang.Boolean> r4 = b.i.a.f.h.l.j2.a     // Catch: java.lang.Throwable -> Lb3
            java.lang.Object r4 = r4.c()     // Catch: java.lang.Throwable -> Lb3
            java.lang.Boolean r4 = (java.lang.Boolean) r4     // Catch: java.lang.Throwable -> Lb3
            boolean r4 = r4.booleanValue()     // Catch: java.lang.Throwable -> Lb3
            monitor-exit(r5)     // Catch: java.lang.Throwable -> Lb3
            return r4
        L5c:
            java.lang.String r0 = "com.google.android.gms"
            java.lang.String r2 = r4.getPackageName()     // Catch: java.lang.Throwable -> Lb3
            boolean r0 = r0.equals(r2)     // Catch: java.lang.Throwable -> Lb3
            r2 = 1
            if (r0 == 0) goto L6b
        L69:
            r0 = 1
            goto L83
        L6b:
            android.content.pm.PackageManager r0 = r4.getPackageManager()     // Catch: java.lang.Throwable -> Lb3
            java.lang.String r3 = "com.google.android.gms.phenotype"
            android.content.pm.ProviderInfo r0 = r0.resolveContentProvider(r3, r1)     // Catch: java.lang.Throwable -> Lb3
            if (r0 == 0) goto L82
            java.lang.String r3 = "com.google.android.gms"
            java.lang.String r0 = r0.packageName     // Catch: java.lang.Throwable -> Lb3
            boolean r0 = r3.equals(r0)     // Catch: java.lang.Throwable -> Lb3
            if (r0 == 0) goto L82
            goto L69
        L82:
            r0 = 0
        L83:
            if (r0 == 0) goto L9b
            android.content.pm.PackageManager r4 = r4.getPackageManager()     // Catch: java.lang.Throwable -> Lb3
            java.lang.String r0 = "com.google.android.gms"
            android.content.pm.ApplicationInfo r4 = r4.getApplicationInfo(r0, r1)     // Catch: android.content.pm.PackageManager.NameNotFoundException -> L97 java.lang.Throwable -> Lb3
            int r4 = r4.flags     // Catch: java.lang.Throwable -> Lb3
            r4 = r4 & 129(0x81, float:1.81E-43)
            if (r4 == 0) goto L97
            r4 = 1
            goto L98
        L97:
            r4 = 0
        L98:
            if (r4 == 0) goto L9b
            r1 = 1
        L9b:
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r1)     // Catch: java.lang.Throwable -> Lb3
            b.i.a.f.h.l.x2 r4 = b.i.a.f.h.l.x2.a(r4)     // Catch: java.lang.Throwable -> Lb3
            b.i.a.f.h.l.j2.a = r4     // Catch: java.lang.Throwable -> Lb3
            monitor-exit(r5)     // Catch: java.lang.Throwable -> Lb3
            b.i.a.f.h.l.x2<java.lang.Boolean> r4 = b.i.a.f.h.l.j2.a
            java.lang.Object r4 = r4.c()
            java.lang.Boolean r4 = (java.lang.Boolean) r4
            boolean r4 = r4.booleanValue()
            return r4
        Lb3:
            r4 = move-exception
            monitor-exit(r5)     // Catch: java.lang.Throwable -> Lb3
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.h.l.j2.a(android.content.Context, android.net.Uri):boolean");
    }
}
