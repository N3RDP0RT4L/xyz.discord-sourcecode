package b.i.a.f.h.l;

import android.os.Bundle;
import android.os.Parcel;
import android.os.RemoteException;
import b.i.a.f.h.l.g;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public abstract class b extends s0 implements c {
    public b() {
        super("com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
    }

    @Override // b.i.a.f.h.l.s0
    public final boolean c(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            ((g.c) this).Z(parcel.readString(), parcel.readString(), (Bundle) v.a(parcel, Bundle.CREATOR), parcel.readLong());
            parcel2.writeNoException();
        } else if (i != 2) {
            return false;
        } else {
            int a = ((g.c) this).a();
            parcel2.writeNoException();
            parcel2.writeInt(a);
        }
        return true;
    }
}
