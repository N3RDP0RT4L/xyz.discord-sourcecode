package b.i.a.f.h.l;

import com.google.android.gms.internal.measurement.zzhi;
import com.google.android.gms.internal.measurement.zzii;
import com.google.android.gms.internal.measurement.zzij;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class c7 {
    public static final c7 a = new c7(0, new int[0], new Object[0], false);

    /* renamed from: b  reason: collision with root package name */
    public int f1434b;
    public int[] c;
    public Object[] d;
    public int e;
    public boolean f;

    public c7() {
        this(0, new int[8], new Object[8], true);
    }

    public static void b(int i, Object obj, v7 v7Var) throws IOException {
        int i2 = i >>> 3;
        int i3 = i & 7;
        if (i3 == 0) {
            ((g4) v7Var).a.g(i2, ((Long) obj).longValue());
        } else if (i3 == 1) {
            ((g4) v7Var).a.z(i2, ((Long) obj).longValue());
        } else if (i3 == 2) {
            ((g4) v7Var).a.h(i2, (t3) obj);
        } else if (i3 == 3) {
            g4 g4Var = (g4) v7Var;
            g4Var.a.f(i2, 3);
            ((c7) obj).d(v7Var);
            g4Var.a.f(i2, 4);
        } else if (i3 == 5) {
            ((g4) v7Var).a.G(i2, ((Integer) obj).intValue());
        } else {
            int i4 = zzij.j;
            throw new RuntimeException(new zzii("Protocol message tag had invalid wire type."));
        }
    }

    public static c7 c() {
        return new c7(0, new int[8], new Object[8], true);
    }

    public final void a(int i, Object obj) {
        if (this.f) {
            int i2 = this.f1434b;
            int[] iArr = this.c;
            if (i2 == iArr.length) {
                int i3 = i2 + (i2 < 4 ? 8 : i2 >> 1);
                this.c = Arrays.copyOf(iArr, i3);
                this.d = Arrays.copyOf(this.d, i3);
            }
            int[] iArr2 = this.c;
            int i4 = this.f1434b;
            iArr2[i4] = i;
            this.d[i4] = obj;
            this.f1434b = i4 + 1;
            return;
        }
        throw new UnsupportedOperationException();
    }

    public final void d(v7 v7Var) throws IOException {
        if (this.f1434b != 0) {
            Objects.requireNonNull(v7Var);
            for (int i = 0; i < this.f1434b; i++) {
                b(this.c[i], this.d[i], v7Var);
            }
        }
    }

    public final int e() {
        int i;
        int i2 = this.e;
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < this.f1434b; i4++) {
            int i5 = this.c[i4];
            int i6 = i5 >>> 3;
            int i7 = i5 & 7;
            if (i7 == 0) {
                i = zzhi.E(i6, ((Long) this.d[i4]).longValue());
            } else if (i7 == 1) {
                ((Long) this.d[i4]).longValue();
                i = zzhi.N(i6);
            } else if (i7 == 2) {
                i = zzhi.w(i6, (t3) this.d[i4]);
            } else if (i7 == 3) {
                i3 = ((c7) this.d[i4]).e() + (zzhi.D(i6) << 1) + i3;
            } else if (i7 == 5) {
                ((Integer) this.d[i4]).intValue();
                i = zzhi.S(i6);
            } else {
                int i8 = zzij.j;
                throw new IllegalStateException(new zzii("Protocol message tag had invalid wire type."));
            }
            i3 = i + i3;
        }
        this.e = i3;
        return i3;
    }

    public final boolean equals(Object obj) {
        boolean z2;
        boolean z3;
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof c7)) {
            return false;
        }
        c7 c7Var = (c7) obj;
        int i = this.f1434b;
        if (i == c7Var.f1434b) {
            int[] iArr = this.c;
            int[] iArr2 = c7Var.c;
            int i2 = 0;
            while (true) {
                if (i2 >= i) {
                    z2 = true;
                    break;
                } else if (iArr[i2] != iArr2[i2]) {
                    z2 = false;
                    break;
                } else {
                    i2++;
                }
            }
            if (z2) {
                Object[] objArr = this.d;
                Object[] objArr2 = c7Var.d;
                int i3 = this.f1434b;
                int i4 = 0;
                while (true) {
                    if (i4 >= i3) {
                        z3 = true;
                        break;
                    } else if (!objArr[i4].equals(objArr2[i4])) {
                        z3 = false;
                        break;
                    } else {
                        i4++;
                    }
                }
                if (z3) {
                    return true;
                }
            }
        }
        return false;
    }

    public final int hashCode() {
        int i = this.f1434b;
        int i2 = (i + 527) * 31;
        int[] iArr = this.c;
        int i3 = 17;
        int i4 = 17;
        for (int i5 = 0; i5 < i; i5++) {
            i4 = (i4 * 31) + iArr[i5];
        }
        int i6 = (i2 + i4) * 31;
        Object[] objArr = this.d;
        int i7 = this.f1434b;
        for (int i8 = 0; i8 < i7; i8++) {
            i3 = (i3 * 31) + objArr[i8].hashCode();
        }
        return i6 + i3;
    }

    public c7(int i, int[] iArr, Object[] objArr, boolean z2) {
        this.e = -1;
        this.f1434b = i;
        this.c = iArr;
        this.d = objArr;
        this.f = z2;
    }
}
