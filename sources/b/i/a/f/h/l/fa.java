package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class fa implements ca {
    public static final l2<Boolean> a = new q2(i2.a("com.google.android.gms.measurement")).c("measurement.ga.ga_app_id", false);

    @Override // b.i.a.f.h.l.ca
    public final boolean a() {
        return true;
    }

    @Override // b.i.a.f.h.l.ca
    public final boolean b() {
        return a.d().booleanValue();
    }
}
