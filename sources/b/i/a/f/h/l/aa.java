package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class aa implements ba {
    public static final l2<Boolean> a = new q2(i2.a("com.google.android.gms.measurement")).c("measurement.collection.firebase_global_collection_flag_enabled", true);

    @Override // b.i.a.f.h.l.ba
    public final boolean a() {
        return a.d().booleanValue();
    }
}
