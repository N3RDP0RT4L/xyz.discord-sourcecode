package b.i.a.f.h.l;

import com.google.android.gms.internal.measurement.zzhi;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Logger;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class s6 {
    public static final Class<?> a;

    /* renamed from: b  reason: collision with root package name */
    public static final d7<?, ?> f1491b;
    public static final d7<?, ?> c;
    public static final d7<?, ?> d;

    static {
        Class<?> cls;
        try {
            cls = Class.forName("com.google.protobuf.GeneratedMessage");
        } catch (Throwable unused) {
            cls = null;
        }
        a = cls;
        f1491b = e(false);
        c = e(true);
        d = new f7();
    }

    public static void A(int i, List<Long> list, v7 v7Var, boolean z2) throws IOException {
        if (!(list == null || list.isEmpty())) {
            g4 g4Var = (g4) v7Var;
            Objects.requireNonNull(g4Var);
            int i2 = 0;
            if (z2) {
                g4Var.a.f(i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    i3 += zzhi.K(list.get(i4).longValue());
                }
                g4Var.a.r(i3);
                while (i2 < list.size()) {
                    g4Var.a.m(zzhi.R(list.get(i2).longValue()));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                g4Var.a.g(i, zzhi.R(list.get(i2).longValue()));
                i2++;
            }
        }
    }

    public static int B(int i, List list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return (zzhi.D(i) * size) + C(list);
    }

    public static int C(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof x4) {
            x4 x4Var = (x4) list;
            i = 0;
            while (i2 < size) {
                i += zzhi.L(x4Var.e(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzhi.L(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    public static void D(int i, List<Long> list, v7 v7Var, boolean z2) throws IOException {
        if (!(list == null || list.isEmpty())) {
            g4 g4Var = (g4) v7Var;
            Objects.requireNonNull(g4Var);
            int i2 = 0;
            if (z2) {
                g4Var.a.f(i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    list.get(i4).longValue();
                    Logger logger = zzhi.a;
                    i3 += 8;
                }
                g4Var.a.r(i3);
                while (i2 < list.size()) {
                    g4Var.a.A(list.get(i2).longValue());
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                g4Var.a.z(i, list.get(i2).longValue());
                i2++;
            }
        }
    }

    public static int E(int i, List list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return (zzhi.D(i) * size) + F(list);
    }

    public static int F(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof x4) {
            x4 x4Var = (x4) list;
            i = 0;
            while (i2 < size) {
                i += zzhi.O(x4Var.e(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzhi.O(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    public static void G(int i, List<Long> list, v7 v7Var, boolean z2) throws IOException {
        if (!(list == null || list.isEmpty())) {
            g4 g4Var = (g4) v7Var;
            Objects.requireNonNull(g4Var);
            int i2 = 0;
            if (z2) {
                g4Var.a.f(i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    list.get(i4).longValue();
                    Logger logger = zzhi.a;
                    i3 += 8;
                }
                g4Var.a.r(i3);
                while (i2 < list.size()) {
                    g4Var.a.A(list.get(i2).longValue());
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                g4Var.a.z(i, list.get(i2).longValue());
                i2++;
            }
        }
    }

    public static int H(int i, List list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzhi.S(i) * size;
    }

    public static int I(List<?> list) {
        return list.size() << 2;
    }

    public static void J(int i, List<Integer> list, v7 v7Var, boolean z2) throws IOException {
        if (!(list == null || list.isEmpty())) {
            g4 g4Var = (g4) v7Var;
            Objects.requireNonNull(g4Var);
            int i2 = 0;
            if (z2) {
                g4Var.a.f(i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    i3 += zzhi.H(list.get(i4).intValue());
                }
                g4Var.a.r(i3);
                while (i2 < list.size()) {
                    g4Var.a.e(list.get(i2).intValue());
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                g4Var.a.s(i, list.get(i2).intValue());
                i2++;
            }
        }
    }

    public static int K(int i, List list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzhi.N(i) * size;
    }

    public static int L(List<?> list) {
        return list.size() << 3;
    }

    public static void M(int i, List<Integer> list, v7 v7Var, boolean z2) throws IOException {
        if (!(list == null || list.isEmpty())) {
            g4 g4Var = (g4) v7Var;
            Objects.requireNonNull(g4Var);
            int i2 = 0;
            if (z2) {
                g4Var.a.f(i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    i3 += zzhi.L(list.get(i4).intValue());
                }
                g4Var.a.r(i3);
                while (i2 < list.size()) {
                    g4Var.a.r(list.get(i2).intValue());
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                g4Var.a.y(i, list.get(i2).intValue());
                i2++;
            }
        }
    }

    public static void N(int i, List<Integer> list, v7 v7Var, boolean z2) throws IOException {
        if (!(list == null || list.isEmpty())) {
            g4 g4Var = (g4) v7Var;
            Objects.requireNonNull(g4Var);
            int i2 = 0;
            if (z2) {
                g4Var.a.f(i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    i3 += zzhi.O(list.get(i4).intValue());
                }
                g4Var.a.r(i3);
                while (i2 < list.size()) {
                    g4Var.a.r(zzhi.V(list.get(i2).intValue()));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                g4Var.a.y(i, zzhi.V(list.get(i2).intValue()));
                i2++;
            }
        }
    }

    public static void O(int i, List<Integer> list, v7 v7Var, boolean z2) throws IOException {
        if (!(list == null || list.isEmpty())) {
            g4 g4Var = (g4) v7Var;
            Objects.requireNonNull(g4Var);
            int i2 = 0;
            if (z2) {
                g4Var.a.f(i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    list.get(i4).intValue();
                    Logger logger = zzhi.a;
                    i3 += 4;
                }
                g4Var.a.r(i3);
                while (i2 < list.size()) {
                    g4Var.a.C(list.get(i2).intValue());
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                g4Var.a.G(i, list.get(i2).intValue());
                i2++;
            }
        }
    }

    public static void P(int i, List<Integer> list, v7 v7Var, boolean z2) throws IOException {
        if (!(list == null || list.isEmpty())) {
            g4 g4Var = (g4) v7Var;
            Objects.requireNonNull(g4Var);
            int i2 = 0;
            if (z2) {
                g4Var.a.f(i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    list.get(i4).intValue();
                    Logger logger = zzhi.a;
                    i3 += 4;
                }
                g4Var.a.r(i3);
                while (i2 < list.size()) {
                    g4Var.a.C(list.get(i2).intValue());
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                g4Var.a.G(i, list.get(i2).intValue());
                i2++;
            }
        }
    }

    public static void Q(int i, List<Integer> list, v7 v7Var, boolean z2) throws IOException {
        if (!(list == null || list.isEmpty())) {
            g4 g4Var = (g4) v7Var;
            Objects.requireNonNull(g4Var);
            int i2 = 0;
            if (z2) {
                g4Var.a.f(i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    i3 += zzhi.H(list.get(i4).intValue());
                }
                g4Var.a.r(i3);
                while (i2 < list.size()) {
                    g4Var.a.e(list.get(i2).intValue());
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                g4Var.a.s(i, list.get(i2).intValue());
                i2++;
            }
        }
    }

    public static void R(int i, List<Boolean> list, v7 v7Var, boolean z2) throws IOException {
        if (!(list == null || list.isEmpty())) {
            g4 g4Var = (g4) v7Var;
            Objects.requireNonNull(g4Var);
            int i2 = 0;
            if (z2) {
                g4Var.a.f(i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    list.get(i4).booleanValue();
                    Logger logger = zzhi.a;
                    i3++;
                }
                g4Var.a.r(i3);
                while (i2 < list.size()) {
                    g4Var.a.d(list.get(i2).booleanValue() ? (byte) 1 : (byte) 0);
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                g4Var.a.l(i, list.get(i2).booleanValue());
                i2++;
            }
        }
    }

    public static int a(int i, Object obj, q6 q6Var) {
        if (obj instanceof h5) {
            int L = zzhi.L(i << 3);
            int a2 = ((h5) obj).a();
            return zzhi.L(a2) + a2 + L;
        }
        return zzhi.c((c6) obj, q6Var) + zzhi.L(i << 3);
    }

    public static int b(int i, List<?> list) {
        int i2;
        int i3;
        int size = list.size();
        int i4 = 0;
        if (size == 0) {
            return 0;
        }
        int D = zzhi.D(i) * size;
        if (list instanceof j5) {
            j5 j5Var = (j5) list;
            while (i4 < size) {
                Object A = j5Var.A(i4);
                if (A instanceof t3) {
                    i3 = zzhi.p((t3) A);
                } else {
                    i3 = zzhi.q((String) A);
                }
                D = i3 + D;
                i4++;
            }
        } else {
            while (i4 < size) {
                Object obj = list.get(i4);
                if (obj instanceof t3) {
                    i2 = zzhi.p((t3) obj);
                } else {
                    i2 = zzhi.q((String) obj);
                }
                D = i2 + D;
                i4++;
            }
        }
        return D;
    }

    public static int c(int i, List<?> list, q6 q6Var) {
        int i2;
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int D = zzhi.D(i) * size;
        for (int i3 = 0; i3 < size; i3++) {
            Object obj = list.get(i3);
            if (obj instanceof h5) {
                i2 = zzhi.b((h5) obj);
            } else {
                i2 = zzhi.c((c6) obj, q6Var);
            }
            D += i2;
        }
        return D;
    }

    public static int d(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof q5) {
            q5 q5Var = (q5) list;
            i = 0;
            while (i2 < size) {
                i += zzhi.F(q5Var.e(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzhi.F(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    public static d7<?, ?> e(boolean z2) {
        Class<?> cls;
        try {
            cls = Class.forName("com.google.protobuf.UnknownFieldSetSchema");
        } catch (Throwable unused) {
            cls = null;
        }
        if (cls == null) {
            return null;
        }
        try {
            return (d7) cls.getConstructor(Boolean.TYPE).newInstance(Boolean.valueOf(z2));
        } catch (Throwable unused2) {
            return null;
        }
    }

    public static void f(int i, List<String> list, v7 v7Var) throws IOException {
        if (list != null && !list.isEmpty()) {
            g4 g4Var = (g4) v7Var;
            Objects.requireNonNull(g4Var);
            int i2 = 0;
            if (list instanceof j5) {
                j5 j5Var = (j5) list;
                while (i2 < list.size()) {
                    Object A = j5Var.A(i2);
                    if (A instanceof String) {
                        g4Var.a.k(i, (String) A);
                    } else {
                        g4Var.a.h(i, (t3) A);
                    }
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                g4Var.a.k(i, list.get(i2));
                i2++;
            }
        }
    }

    public static void g(int i, List<?> list, v7 v7Var, q6 q6Var) throws IOException {
        if (!(list == null || list.isEmpty())) {
            g4 g4Var = (g4) v7Var;
            Objects.requireNonNull(g4Var);
            for (int i2 = 0; i2 < list.size(); i2++) {
                g4Var.a.j(i, (c6) list.get(i2), q6Var);
            }
        }
    }

    public static void h(int i, List<Double> list, v7 v7Var, boolean z2) throws IOException {
        if (!(list == null || list.isEmpty())) {
            g4 g4Var = (g4) v7Var;
            Objects.requireNonNull(g4Var);
            int i2 = 0;
            if (z2) {
                g4Var.a.f(i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    list.get(i4).doubleValue();
                    Logger logger = zzhi.a;
                    i3 += 8;
                }
                g4Var.a.r(i3);
                while (i2 < list.size()) {
                    zzhi zzhiVar = g4Var.a;
                    double doubleValue = list.get(i2).doubleValue();
                    Objects.requireNonNull(zzhiVar);
                    zzhiVar.A(Double.doubleToRawLongBits(doubleValue));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                zzhi zzhiVar2 = g4Var.a;
                double doubleValue2 = list.get(i2).doubleValue();
                Objects.requireNonNull(zzhiVar2);
                zzhiVar2.z(i, Double.doubleToRawLongBits(doubleValue2));
                i2++;
            }
        }
    }

    public static <T, FT extends p4<FT>> void i(j4<FT> j4Var, T t, T t2) {
        n4<FT> b2 = j4Var.b(t2);
        if (!b2.f1457b.isEmpty()) {
            n4<FT> f = j4Var.f(t);
            Objects.requireNonNull(f);
            for (int i = 0; i < b2.f1457b.e(); i++) {
                f.j(b2.f1457b.d(i));
            }
            for (Map.Entry<FT, Object> entry : b2.f1457b.g()) {
                f.j(entry);
            }
        }
    }

    public static boolean j(Object obj, Object obj2) {
        if (obj != obj2) {
            return obj != null && obj.equals(obj2);
        }
        return true;
    }

    public static int k(int i, List list) {
        if (list.size() == 0) {
            return 0;
        }
        return (zzhi.D(i) * list.size()) + d(list);
    }

    public static int l(int i, List<t3> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int D = zzhi.D(i) * size;
        for (int i2 = 0; i2 < list.size(); i2++) {
            D += zzhi.p(list.get(i2));
        }
        return D;
    }

    public static int m(int i, List<c6> list, q6 q6Var) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < size; i3++) {
            i2 += zzhi.x(i, list.get(i3), q6Var);
        }
        return i2;
    }

    public static int n(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof q5) {
            q5 q5Var = (q5) list;
            i = 0;
            while (i2 < size) {
                i += zzhi.F(q5Var.e(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzhi.F(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    public static void o(int i, List<t3> list, v7 v7Var) throws IOException {
        if (!(list == null || list.isEmpty())) {
            g4 g4Var = (g4) v7Var;
            Objects.requireNonNull(g4Var);
            for (int i2 = 0; i2 < list.size(); i2++) {
                g4Var.a.h(i, list.get(i2));
            }
        }
    }

    public static void p(int i, List<?> list, v7 v7Var, q6 q6Var) throws IOException {
        if (!(list == null || list.isEmpty())) {
            g4 g4Var = (g4) v7Var;
            Objects.requireNonNull(g4Var);
            for (int i2 = 0; i2 < list.size(); i2++) {
                g4Var.e(i, list.get(i2), q6Var);
            }
        }
    }

    public static void q(int i, List<Float> list, v7 v7Var, boolean z2) throws IOException {
        if (!(list == null || list.isEmpty())) {
            g4 g4Var = (g4) v7Var;
            Objects.requireNonNull(g4Var);
            int i2 = 0;
            if (z2) {
                g4Var.a.f(i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    list.get(i4).floatValue();
                    Logger logger = zzhi.a;
                    i3 += 4;
                }
                g4Var.a.r(i3);
                while (i2 < list.size()) {
                    zzhi zzhiVar = g4Var.a;
                    float floatValue = list.get(i2).floatValue();
                    Objects.requireNonNull(zzhiVar);
                    zzhiVar.C(Float.floatToRawIntBits(floatValue));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                zzhi zzhiVar2 = g4Var.a;
                float floatValue2 = list.get(i2).floatValue();
                Objects.requireNonNull(zzhiVar2);
                zzhiVar2.G(i, Float.floatToRawIntBits(floatValue2));
                i2++;
            }
        }
    }

    public static int r(int i, List list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return (zzhi.D(i) * size) + n(list);
    }

    public static int s(int i, List list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return (zzhi.D(i) * size) + t(list);
    }

    public static int t(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof q5) {
            q5 q5Var = (q5) list;
            i = 0;
            while (i2 < size) {
                i += zzhi.K(q5Var.e(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzhi.K(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    public static void u(int i, List<Long> list, v7 v7Var, boolean z2) throws IOException {
        if (!(list == null || list.isEmpty())) {
            g4 g4Var = (g4) v7Var;
            Objects.requireNonNull(g4Var);
            int i2 = 0;
            if (z2) {
                g4Var.a.f(i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    i3 += zzhi.F(list.get(i4).longValue());
                }
                g4Var.a.r(i3);
                while (i2 < list.size()) {
                    g4Var.a.m(list.get(i2).longValue());
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                g4Var.a.g(i, list.get(i2).longValue());
                i2++;
            }
        }
    }

    public static int v(int i, List list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return (zzhi.D(i) * size) + w(list);
    }

    public static int w(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof x4) {
            x4 x4Var = (x4) list;
            i = 0;
            while (i2 < size) {
                i += zzhi.H(x4Var.e(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzhi.H(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    public static void x(int i, List<Long> list, v7 v7Var, boolean z2) throws IOException {
        if (!(list == null || list.isEmpty())) {
            g4 g4Var = (g4) v7Var;
            Objects.requireNonNull(g4Var);
            int i2 = 0;
            if (z2) {
                g4Var.a.f(i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    i3 += zzhi.F(list.get(i4).longValue());
                }
                g4Var.a.r(i3);
                while (i2 < list.size()) {
                    g4Var.a.m(list.get(i2).longValue());
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                g4Var.a.g(i, list.get(i2).longValue());
                i2++;
            }
        }
    }

    public static int y(int i, List list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return (zzhi.D(i) * size) + z(list);
    }

    public static int z(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof x4) {
            x4 x4Var = (x4) list;
            i = 0;
            while (i2 < size) {
                i += zzhi.H(x4Var.e(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzhi.H(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }
}
