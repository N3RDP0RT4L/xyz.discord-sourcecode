package b.i.a.f.h.l;

import java.io.IOException;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public interface q6<T> {
    T a();

    int b(T t);

    boolean c(T t);

    void d(T t);

    int e(T t);

    boolean f(T t, T t2);

    void g(T t, v7 v7Var) throws IOException;

    void h(T t, byte[] bArr, int i, int i2, s3 s3Var) throws IOException;

    void i(T t, T t2);
}
