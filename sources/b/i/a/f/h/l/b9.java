package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class b9 implements y8 {
    public static final l2<Boolean> a = new q2(i2.a("com.google.android.gms.measurement")).c("measurement.upload.file_truncate_fix", false);

    @Override // b.i.a.f.h.l.y8
    public final boolean a() {
        return a.d().booleanValue();
    }
}
