package b.i.a.f.h.l;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class d3 extends a implements e2 {
    public d3(IBinder iBinder) {
        super(iBinder, "com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
    }

    @Override // b.i.a.f.h.l.e2
    public final Bundle f(Bundle bundle) throws RemoteException {
        Parcel c = c();
        v.c(c, bundle);
        Parcel g = g(1, c);
        Bundle bundle2 = (Bundle) v.a(g, Bundle.CREATOR);
        g.recycle();
        return bundle2;
    }
}
