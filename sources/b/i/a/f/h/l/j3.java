package b.i.a.f.h.l;

import java.lang.ref.Reference;
import java.util.List;
import java.util.Objects;
import java.util.Vector;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class j3 extends f3 {
    public final i3 a = new i3();

    @Override // b.i.a.f.h.l.f3
    public final void a(Throwable th, Throwable th2) {
        List<Throwable> putIfAbsent;
        if (th2 != th) {
            Objects.requireNonNull(th2, "The suppressed exception cannot be null.");
            i3 i3Var = this.a;
            for (Reference<? extends Throwable> poll = i3Var.f1446b.poll(); poll != null; poll = i3Var.f1446b.poll()) {
                i3Var.a.remove(poll);
            }
            List<Throwable> list = i3Var.a.get(new h3(th, null));
            if (list == null && (putIfAbsent = i3Var.a.putIfAbsent(new h3(th, i3Var.f1446b), (list = new Vector<>(2)))) != null) {
                list = putIfAbsent;
            }
            list.add(th2);
            return;
        }
        throw new IllegalArgumentException("Self suppression is not allowed.", th2);
    }
}
