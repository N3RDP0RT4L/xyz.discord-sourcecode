package b.i.a.f.h.l;

import b.d.b.a.a;
import java.util.Map;
/* JADX WARN: Incorrect field signature: TK; */
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class y6 implements Comparable<y6>, Map.Entry<K, V> {
    public final Comparable j;
    public V k;
    public final /* synthetic */ r6 l;

    /* JADX WARN: Type inference failed for: r3v1, types: [V, java.lang.Object] */
    public y6(r6 r6Var, Map.Entry<K, V> entry) {
        ?? value = entry.getValue();
        this.l = r6Var;
        this.j = (Comparable) entry.getKey();
        this.k = value;
    }

    @Override // java.lang.Comparable
    public final /* synthetic */ int compareTo(y6 y6Var) {
        return this.j.compareTo(y6Var.j);
    }

    @Override // java.util.Map.Entry
    public final boolean equals(Object obj) {
        boolean z2;
        boolean z3;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        Comparable comparable = this.j;
        Object key = entry.getKey();
        if (comparable == null) {
            z2 = key == null;
        } else {
            z2 = comparable.equals(key);
        }
        if (z2) {
            V v = this.k;
            Object value = entry.getValue();
            if (v == 0) {
                z3 = value == null;
            } else {
                z3 = v.equals(value);
            }
            if (z3) {
                return true;
            }
        }
        return false;
    }

    @Override // java.util.Map.Entry
    public final /* synthetic */ Object getKey() {
        return this.j;
    }

    @Override // java.util.Map.Entry
    public final V getValue() {
        return this.k;
    }

    @Override // java.util.Map.Entry
    public final int hashCode() {
        Comparable comparable = this.j;
        int i = 0;
        int hashCode = comparable == null ? 0 : comparable.hashCode();
        V v = this.k;
        if (v != 0) {
            i = v.hashCode();
        }
        return hashCode ^ i;
    }

    @Override // java.util.Map.Entry
    public final V setValue(V v) {
        r6 r6Var = this.l;
        int i = r6.j;
        r6Var.h();
        V v2 = this.k;
        this.k = v;
        return v2;
    }

    public final String toString() {
        String valueOf = String.valueOf(this.j);
        String valueOf2 = String.valueOf(this.k);
        return a.j(valueOf2.length() + valueOf.length() + 1, valueOf, "=", valueOf2);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public y6(r6 r6Var, K k, V v) {
        this.l = r6Var;
        this.j = k;
        this.k = v;
    }
}
