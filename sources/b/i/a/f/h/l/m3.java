package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public abstract class m3 implements f6 {
    @Override // b.i.a.f.h.l.f6
    public final f6 a() {
        throw new UnsupportedOperationException("clone() should be implemented by subclasses.");
    }

    public Object clone() throws CloneNotSupportedException {
        throw new UnsupportedOperationException("clone() should be implemented by subclasses.");
    }
}
