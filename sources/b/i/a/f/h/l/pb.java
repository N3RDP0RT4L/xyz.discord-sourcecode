package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class pb implements mb {
    public static final l2<Boolean> a = new q2(i2.a("com.google.android.gms.measurement")).c("measurement.collection.synthetic_data_mitigation", false);

    @Override // b.i.a.f.h.l.mb
    public final boolean a() {
        return a.d().booleanValue();
    }
}
