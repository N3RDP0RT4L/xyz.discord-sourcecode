package b.i.a.f.h.l;

import com.google.android.gms.internal.measurement.zzij;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public abstract class m7 {
    public abstract int a(int i, byte[] bArr, int i2, int i3);

    public abstract int b(CharSequence charSequence, byte[] bArr, int i, int i2);

    public abstract String c(byte[] bArr, int i, int i2) throws zzij;
}
