package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class db implements ab {
    public static final l2<Boolean> a = new q2(i2.a("com.google.android.gms.measurement")).c("measurement.collection.log_event_and_bundle_v2", true);

    @Override // b.i.a.f.h.l.ab
    public final boolean a() {
        return a.d().booleanValue();
    }
}
