package b.i.a.f.h.l;

import java.lang.Comparable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public class r6<K extends Comparable<K>, V> extends AbstractMap<K, V> {
    public static final /* synthetic */ int j = 0;
    public final int k;
    public boolean n;
    public volatile a7 o;
    public List<y6> l = Collections.emptyList();
    public Map<K, V> m = Collections.emptyMap();
    public Map<K, V> p = Collections.emptyMap();

    public r6(int i, t6 t6Var) {
        this.k = i;
    }

    public final int a(K k) {
        int size = this.l.size() - 1;
        if (size >= 0) {
            int compareTo = k.compareTo(this.l.get(size).j);
            if (compareTo > 0) {
                return -(size + 2);
            }
            if (compareTo == 0) {
                return size;
            }
        }
        int i = 0;
        while (i <= size) {
            int i2 = (i + size) / 2;
            int compareTo2 = k.compareTo(this.l.get(i2).j);
            if (compareTo2 < 0) {
                size = i2 - 1;
            } else if (compareTo2 <= 0) {
                return i2;
            } else {
                i = i2 + 1;
            }
        }
        return -(i + 1);
    }

    /* renamed from: b */
    public final V put(K k, V v) {
        h();
        int a = a(k);
        if (a >= 0) {
            y6 y6Var = this.l.get(a);
            y6Var.l.h();
            V v2 = y6Var.k;
            y6Var.k = v;
            return v2;
        }
        h();
        if (this.l.isEmpty() && !(this.l instanceof ArrayList)) {
            this.l = new ArrayList(this.k);
        }
        int i = -(a + 1);
        if (i >= this.k) {
            return i().put(k, v);
        }
        int size = this.l.size();
        int i2 = this.k;
        if (size == i2) {
            y6 remove = this.l.remove(i2 - 1);
            i().put((K) remove.j, remove.k);
        }
        this.l.add(i, new y6(this, k, v));
        return null;
    }

    public void c() {
        Map<K, V> map;
        Map<K, V> map2;
        if (!this.n) {
            if (this.m.isEmpty()) {
                map = Collections.emptyMap();
            } else {
                map = Collections.unmodifiableMap(this.m);
            }
            this.m = map;
            if (this.p.isEmpty()) {
                map2 = Collections.emptyMap();
            } else {
                map2 = Collections.unmodifiableMap(this.p);
            }
            this.p = map2;
            this.n = true;
        }
    }

    @Override // java.util.AbstractMap, java.util.Map
    public void clear() {
        h();
        if (!this.l.isEmpty()) {
            this.l.clear();
        }
        if (!this.m.isEmpty()) {
            this.m.clear();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public boolean containsKey(Object obj) {
        Comparable comparable = (Comparable) obj;
        return a(comparable) >= 0 || this.m.containsKey(comparable);
    }

    public final Map.Entry<K, V> d(int i) {
        return this.l.get(i);
    }

    public final int e() {
        return this.l.size();
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Set<Map.Entry<K, V>> entrySet() {
        if (this.o == null) {
            this.o = new a7(this, null);
        }
        return this.o;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof r6)) {
            return super.equals(obj);
        }
        r6 r6Var = (r6) obj;
        int size = size();
        if (size != r6Var.size()) {
            return false;
        }
        int e = e();
        if (e != r6Var.e()) {
            return entrySet().equals(r6Var.entrySet());
        }
        for (int i = 0; i < e; i++) {
            if (!d(i).equals(r6Var.d(i))) {
                return false;
            }
        }
        if (e != size) {
            return this.m.equals(r6Var.m);
        }
        return true;
    }

    public final V f(int i) {
        h();
        V v = this.l.remove(i).k;
        if (!this.m.isEmpty()) {
            Iterator<Map.Entry<K, V>> it = i().entrySet().iterator();
            this.l.add(new y6(this, it.next()));
            it.remove();
        }
        return v;
    }

    public final Iterable<Map.Entry<K, V>> g() {
        if (this.m.isEmpty()) {
            return (Iterable<Map.Entry<K, V>>) u6.f1493b;
        }
        return this.m.entrySet();
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public V get(Object obj) {
        Comparable comparable = (Comparable) obj;
        int a = a(comparable);
        if (a >= 0) {
            return this.l.get(a).k;
        }
        return this.m.get(comparable);
    }

    public final void h() {
        if (this.n) {
            throw new UnsupportedOperationException();
        }
    }

    @Override // java.util.AbstractMap, java.util.Map
    public int hashCode() {
        int e = e();
        int i = 0;
        for (int i2 = 0; i2 < e; i2++) {
            i += this.l.get(i2).hashCode();
        }
        return this.m.size() > 0 ? i + this.m.hashCode() : i;
    }

    public final SortedMap<K, V> i() {
        h();
        if (this.m.isEmpty() && !(this.m instanceof TreeMap)) {
            TreeMap treeMap = new TreeMap();
            this.m = treeMap;
            this.p = treeMap.descendingMap();
        }
        return (SortedMap) this.m;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public V remove(Object obj) {
        h();
        Comparable comparable = (Comparable) obj;
        int a = a(comparable);
        if (a >= 0) {
            return (V) f(a);
        }
        if (this.m.isEmpty()) {
            return null;
        }
        return this.m.remove(comparable);
    }

    @Override // java.util.AbstractMap, java.util.Map
    public int size() {
        return this.m.size() + this.l.size();
    }
}
