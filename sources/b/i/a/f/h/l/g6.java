package b.i.a.f.h.l;

import b.i.a.f.h.l.u4;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class g6<T> implements q6<T> {
    public final c6 a;

    /* renamed from: b  reason: collision with root package name */
    public final d7<?, ?> f1440b;
    public final boolean c;
    public final j4<?> d;

    public g6(d7<?, ?> d7Var, j4<?> j4Var, c6 c6Var) {
        this.f1440b = d7Var;
        this.c = j4Var.e(c6Var);
        this.d = j4Var;
        this.a = c6Var;
    }

    @Override // b.i.a.f.h.l.q6
    public final T a() {
        return (T) ((u4.b) this.a.h()).o();
    }

    @Override // b.i.a.f.h.l.q6
    public final int b(T t) {
        int hashCode = this.f1440b.e(t).hashCode();
        return this.c ? (hashCode * 53) + this.d.b(t).hashCode() : hashCode;
    }

    @Override // b.i.a.f.h.l.q6
    public final boolean c(T t) {
        return this.d.b(t).m();
    }

    @Override // b.i.a.f.h.l.q6
    public final void d(T t) {
        this.f1440b.h(t);
        this.d.g(t);
    }

    @Override // b.i.a.f.h.l.q6
    public final int e(T t) {
        d7<?, ?> d7Var = this.f1440b;
        int i = d7Var.i(d7Var.e(t)) + 0;
        if (!this.c) {
            return i;
        }
        n4<?> b2 = this.d.b(t);
        int i2 = 0;
        for (int i3 = 0; i3 < b2.f1457b.e(); i3++) {
            i2 += n4.k(b2.f1457b.d(i3));
        }
        for (Map.Entry<?, Object> entry : b2.f1457b.g()) {
            i2 += n4.k(entry);
        }
        return i + i2;
    }

    @Override // b.i.a.f.h.l.q6
    public final boolean f(T t, T t2) {
        if (!this.f1440b.e(t).equals(this.f1440b.e(t2))) {
            return false;
        }
        if (this.c) {
            return this.d.b(t).equals(this.d.b(t2));
        }
        return true;
    }

    @Override // b.i.a.f.h.l.q6
    public final void g(T t, v7 v7Var) throws IOException {
        Iterator<Map.Entry<?, Object>> l = this.d.b(t).l();
        while (l.hasNext()) {
            Map.Entry<?, Object> next = l.next();
            p4 p4Var = (p4) next.getKey();
            if (p4Var.c() != w7.MESSAGE || p4Var.d() || p4Var.e()) {
                throw new IllegalStateException("Found invalid MessageSet item.");
            } else if (next instanceof f5) {
                ((g4) v7Var).c(p4Var.a(), ((f5) next).j.getValue().c());
            } else {
                ((g4) v7Var).c(p4Var.a(), next.getValue());
            }
        }
        d7<?, ?> d7Var = this.f1440b;
        d7Var.f(d7Var.e(t), v7Var);
    }

    /* JADX WARN: Removed duplicated region for block: B:32:0x0090  */
    /* JADX WARN: Removed duplicated region for block: B:58:0x0095 A[EDGE_INSN: B:58:0x0095->B:33:0x0095 ?: BREAK  , SYNTHETIC] */
    @Override // b.i.a.f.h.l.q6
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void h(T r10, byte[] r11, int r12, int r13, b.i.a.f.h.l.s3 r14) throws java.io.IOException {
        /*
            r9 = this;
            r0 = r10
            b.i.a.f.h.l.u4 r0 = (b.i.a.f.h.l.u4) r0
            b.i.a.f.h.l.c7 r1 = r0.zzb
            b.i.a.f.h.l.c7 r2 = b.i.a.f.h.l.c7.a
            if (r1 != r2) goto Lf
            b.i.a.f.h.l.c7 r1 = b.i.a.f.h.l.c7.c()
            r0.zzb = r1
        Lf:
            b.i.a.f.h.l.u4$d r10 = (b.i.a.f.h.l.u4.d) r10
            r10.u()
            r10 = 0
            r0 = r10
        L16:
            if (r12 >= r13) goto La0
            int r4 = b.i.a.f.e.o.f.O1(r11, r12, r14)
            int r2 = r14.a
            r12 = 11
            r3 = 2
            if (r2 == r12) goto L4e
            r12 = r2 & 7
            if (r12 != r3) goto L49
            b.i.a.f.h.l.j4<?> r12 = r9.d
            b.i.a.f.h.l.h4 r0 = r14.d
            b.i.a.f.h.l.c6 r3 = r9.a
            int r5 = r2 >>> 3
            java.lang.Object r12 = r12.c(r0, r3, r5)
            r0 = r12
            b.i.a.f.h.l.u4$f r0 = (b.i.a.f.h.l.u4.f) r0
            if (r0 != 0) goto L41
            r3 = r11
            r5 = r13
            r6 = r1
            r7 = r14
            int r12 = b.i.a.f.e.o.f.I1(r2, r3, r4, r5, r6, r7)
            goto L16
        L41:
            b.i.a.f.h.l.n6 r10 = b.i.a.f.h.l.n6.a
            java.lang.NoSuchMethodError r10 = new java.lang.NoSuchMethodError
            r10.<init>()
            throw r10
        L49:
            int r12 = b.i.a.f.e.o.f.G1(r2, r11, r4, r13, r14)
            goto L16
        L4e:
            r12 = 0
            r2 = r10
        L50:
            if (r4 >= r13) goto L95
            int r4 = b.i.a.f.e.o.f.O1(r11, r4, r14)
            int r5 = r14.a
            int r6 = r5 >>> 3
            r7 = r5 & 7
            if (r6 == r3) goto L77
            r8 = 3
            if (r6 == r8) goto L62
            goto L8c
        L62:
            if (r0 != 0) goto L6f
            if (r7 != r3) goto L8c
            int r4 = b.i.a.f.e.o.f.v2(r11, r4, r14)
            java.lang.Object r2 = r14.c
            b.i.a.f.h.l.t3 r2 = (b.i.a.f.h.l.t3) r2
            goto L50
        L6f:
            b.i.a.f.h.l.n6 r10 = b.i.a.f.h.l.n6.a
            java.lang.NoSuchMethodError r10 = new java.lang.NoSuchMethodError
            r10.<init>()
            throw r10
        L77:
            if (r7 != 0) goto L8c
            int r4 = b.i.a.f.e.o.f.O1(r11, r4, r14)
            int r12 = r14.a
            b.i.a.f.h.l.j4<?> r0 = r9.d
            b.i.a.f.h.l.h4 r5 = r14.d
            b.i.a.f.h.l.c6 r6 = r9.a
            java.lang.Object r0 = r0.c(r5, r6, r12)
            b.i.a.f.h.l.u4$f r0 = (b.i.a.f.h.l.u4.f) r0
            goto L50
        L8c:
            r6 = 12
            if (r5 == r6) goto L95
            int r4 = b.i.a.f.e.o.f.G1(r5, r11, r4, r13, r14)
            goto L50
        L95:
            if (r2 == 0) goto L9d
            int r12 = r12 << 3
            r12 = r12 | r3
            r1.a(r12, r2)
        L9d:
            r12 = r4
            goto L16
        La0:
            if (r12 != r13) goto La3
            return
        La3:
            com.google.android.gms.internal.measurement.zzij r10 = com.google.android.gms.internal.measurement.zzij.d()
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.h.l.g6.h(java.lang.Object, byte[], int, int, b.i.a.f.h.l.s3):void");
    }

    @Override // b.i.a.f.h.l.q6
    public final void i(T t, T t2) {
        d7<?, ?> d7Var = this.f1440b;
        Class<?> cls = s6.a;
        d7Var.d(t, d7Var.g(d7Var.e(t), d7Var.e(t2)));
        if (this.c) {
            s6.i(this.d, t, t2);
        }
    }
}
