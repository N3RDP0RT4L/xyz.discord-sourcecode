package b.i.a.f.h.l;

import b.d.b.a.a;
import b.i.a.f.e.o.f;
import java.io.Serializable;
import java.util.Arrays;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class b3<T> implements z2<T>, Serializable {
    @NullableDecl
    private final T zza;

    public b3(@NullableDecl T t) {
        this.zza = t;
    }

    @Override // b.i.a.f.h.l.z2
    public final T a() {
        return this.zza;
    }

    public final boolean equals(@NullableDecl Object obj) {
        if (obj instanceof b3) {
            return f.j2(this.zza, ((b3) obj).zza);
        }
        return false;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.zza});
    }

    public final String toString() {
        String valueOf = String.valueOf(this.zza);
        return a.j(valueOf.length() + 22, "Suppliers.ofInstance(", valueOf, ")");
    }
}
