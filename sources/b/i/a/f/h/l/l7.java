package b.i.a.f.h.l;

import b.i.a.f.e.o.f;
import com.google.android.gms.internal.measurement.zzij;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class l7 extends m7 {
    @Override // b.i.a.f.h.l.m7
    public final int a(int i, byte[] bArr, int i2, int i3) {
        while (i2 < i3 && bArr[i2] >= 0) {
            i2++;
        }
        if (i2 >= i3) {
            return 0;
        }
        while (i2 < i3) {
            int i4 = i2 + 1;
            byte b2 = bArr[i2];
            if (b2 < 0) {
                if (b2 < -32) {
                    if (i4 >= i3) {
                        return b2;
                    }
                    if (b2 >= -62) {
                        i2 = i4 + 1;
                        if (bArr[i4] > -65) {
                        }
                    }
                    return -1;
                } else if (b2 < -16) {
                    if (i4 >= i3 - 1) {
                        return k7.d(bArr, i4, i3);
                    }
                    int i5 = i4 + 1;
                    byte b3 = bArr[i4];
                    if (b3 <= -65 && ((b2 != -32 || b3 >= -96) && (b2 != -19 || b3 < -96))) {
                        i2 = i5 + 1;
                        if (bArr[i5] > -65) {
                        }
                    }
                    return -1;
                } else if (i4 >= i3 - 2) {
                    return k7.d(bArr, i4, i3);
                } else {
                    int i6 = i4 + 1;
                    byte b4 = bArr[i4];
                    if (b4 <= -65) {
                        if ((((b4 + 112) + (b2 << 28)) >> 30) == 0) {
                            int i7 = i6 + 1;
                            if (bArr[i6] <= -65) {
                                i4 = i7 + 1;
                                if (bArr[i7] > -65) {
                                }
                            }
                        }
                    }
                    return -1;
                }
            }
            i2 = i4;
        }
        return 0;
    }

    /* JADX WARN: Code restructure failed: missing block: B:12:0x001d, code lost:
        return r10 + r0;
     */
    @Override // b.i.a.f.h.l.m7
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final int b(java.lang.CharSequence r8, byte[] r9, int r10, int r11) {
        /*
            Method dump skipped, instructions count: 256
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.h.l.l7.b(java.lang.CharSequence, byte[], int, int):int");
    }

    @Override // b.i.a.f.h.l.m7
    public final String c(byte[] bArr, int i, int i2) throws zzij {
        if ((i | i2 | ((bArr.length - i) - i2)) >= 0) {
            int i3 = i + i2;
            char[] cArr = new char[i2];
            int i4 = 0;
            while (i < i3) {
                byte b2 = bArr[i];
                if (!f.i2(b2)) {
                    break;
                }
                i++;
                i4++;
                cArr[i4] = (char) b2;
            }
            int i5 = i4;
            while (i < i3) {
                int i6 = i + 1;
                byte b3 = bArr[i];
                if (f.i2(b3)) {
                    int i7 = i5 + 1;
                    cArr[i5] = (char) b3;
                    i = i6;
                    while (true) {
                        i5 = i7;
                        if (i < i3) {
                            byte b4 = bArr[i];
                            if (f.i2(b4)) {
                                i++;
                                i7 = i5 + 1;
                                cArr[i5] = (char) b4;
                            }
                        }
                    }
                } else {
                    if (!(b3 < -32)) {
                        if (b3 < -16) {
                            if (i6 < i3 - 1) {
                                int i8 = i6 + 1;
                                i = i8 + 1;
                                i5++;
                                f.b2(b3, bArr[i6], bArr[i8], cArr, i5);
                            } else {
                                throw zzij.e();
                            }
                        } else if (i6 < i3 - 2) {
                            int i9 = i6 + 1;
                            byte b5 = bArr[i6];
                            int i10 = i9 + 1;
                            i = i10 + 1;
                            f.a2(b3, b5, bArr[i9], bArr[i10], cArr, i5);
                            i5 = i5 + 1 + 1;
                        } else {
                            throw zzij.e();
                        }
                    } else if (i6 < i3) {
                        i = i6 + 1;
                        i5++;
                        f.c2(b3, bArr[i6], cArr, i5);
                    } else {
                        throw zzij.e();
                    }
                }
            }
            return new String(cArr, 0, i5);
        }
        throw new ArrayIndexOutOfBoundsException(String.format("buffer length=%d, index=%d, size=%d", Integer.valueOf(bArr.length), Integer.valueOf(i), Integer.valueOf(i2)));
    }
}
