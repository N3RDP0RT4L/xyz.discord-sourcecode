package b.i.a.f.h.l;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final class o5 extends m5 {
    public static final Class<?> c = Collections.unmodifiableList(Collections.emptyList()).getClass();

    public o5(l5 l5Var) {
        super(null);
    }

    @Override // b.i.a.f.h.l.m5
    public final <E> void a(Object obj, Object obj2, long j) {
        ArrayList arrayList;
        List list = (List) j7.r(obj2, j);
        int size = list.size();
        List list2 = (List) j7.r(obj, j);
        if (list2.isEmpty()) {
            if (list2 instanceof j5) {
                list2 = new k5(size);
            } else if (!(list2 instanceof l6) || !(list2 instanceof b5)) {
                list2 = new ArrayList(size);
            } else {
                list2 = ((b5) list2).f(size);
            }
            j7.f(obj, j, list2);
        } else {
            if (c.isAssignableFrom(list2.getClass())) {
                ArrayList arrayList2 = new ArrayList(list2.size() + size);
                arrayList2.addAll(list2);
                j7.f(obj, j, arrayList2);
                arrayList = arrayList2;
            } else if (list2 instanceof e7) {
                k5 k5Var = new k5(list2.size() + size);
                k5Var.addAll(k5Var.size(), (e7) list2);
                j7.f(obj, j, k5Var);
                arrayList = k5Var;
            } else if ((list2 instanceof l6) && (list2 instanceof b5)) {
                b5 b5Var = (b5) list2;
                if (!b5Var.a()) {
                    list2 = b5Var.f(list2.size() + size);
                    j7.f(obj, j, list2);
                }
            }
            list2 = arrayList;
        }
        int size2 = list2.size();
        int size3 = list.size();
        if (size2 > 0 && size3 > 0) {
            list2.addAll(list);
        }
        if (size2 > 0) {
            list = list2;
        }
        j7.f(obj, j, list);
    }

    @Override // b.i.a.f.h.l.m5
    public final void b(Object obj, long j) {
        Object obj2;
        List list = (List) j7.r(obj, j);
        if (list instanceof j5) {
            obj2 = ((j5) list).s();
        } else if (!c.isAssignableFrom(list.getClass())) {
            if (!(list instanceof l6) || !(list instanceof b5)) {
                obj2 = Collections.unmodifiableList(list);
            } else {
                b5 b5Var = (b5) list;
                if (b5Var.a()) {
                    b5Var.g0();
                    return;
                }
                return;
            }
        } else {
            return;
        }
        j7.f(obj, j, obj2);
    }
}
