package b.i.a.f.h.l;

import b.i.a.f.h.l.p4;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public interface p4<T extends p4<T>> extends Comparable<T> {
    b6 X(b6 b6Var, c6 c6Var);

    int a();

    p7 b();

    w7 c();

    f6 c0(f6 f6Var, f6 f6Var2);

    boolean d();

    boolean e();
}
