package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class eb implements fb {
    public static final l2<Boolean> a = new q2(i2.a("com.google.android.gms.measurement")).c("measurement.scheduler.task_thread.cleanup_on_exit", false);

    @Override // b.i.a.f.h.l.fb
    public final boolean a() {
        return a.d().booleanValue();
    }
}
