package b.i.a.f.h.l;

import android.net.Uri;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class q2 {
    public final Uri a;

    public q2(Uri uri) {
        this.a = uri;
    }

    public final l2<Long> a(String str, long j) {
        Object obj = l2.a;
        return new m2(this, str, Long.valueOf(j));
    }

    public final l2<String> b(String str, String str2) {
        Object obj = l2.a;
        return new r2(this, str, str2);
    }

    public final l2<Boolean> c(String str, boolean z2) {
        Object obj = l2.a;
        return new p2(this, str, Boolean.valueOf(z2));
    }
}
