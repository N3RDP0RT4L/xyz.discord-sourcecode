package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class u9 implements v9 {
    public static final l2<Boolean> a = new q2(i2.a("com.google.android.gms.measurement")).c("measurement.client.freeride_engagement_fix", true);

    @Override // b.i.a.f.h.l.v9
    public final boolean a() {
        return true;
    }

    @Override // b.i.a.f.h.l.v9
    public final boolean b() {
        return a.d().booleanValue();
    }
}
