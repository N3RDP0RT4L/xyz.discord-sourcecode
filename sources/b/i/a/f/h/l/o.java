package b.i.a.f.h.l;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@18.0.0 */
/* loaded from: classes3.dex */
public final class o implements ThreadFactory {
    public ThreadFactory j = Executors.defaultThreadFactory();

    @Override // java.util.concurrent.ThreadFactory
    public final Thread newThread(Runnable runnable) {
        Thread newThread = this.j.newThread(runnable);
        newThread.setName("ScionFrontendApi");
        return newThread;
    }
}
