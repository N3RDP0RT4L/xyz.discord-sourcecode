package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class xa implements ua {
    public static final l2<Boolean> a = new q2(i2.a("com.google.android.gms.measurement")).c("measurement.validation.internal_limits_internal_event_params", false);

    @Override // b.i.a.f.h.l.ua
    public final boolean a() {
        return a.d().booleanValue();
    }
}
