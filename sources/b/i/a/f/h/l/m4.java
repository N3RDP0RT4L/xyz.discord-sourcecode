package b.i.a.f.h.l;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public final /* synthetic */ class m4 {
    public static final /* synthetic */ int[] a;

    /* renamed from: b  reason: collision with root package name */
    public static final /* synthetic */ int[] f1454b;

    static {
        p7.values();
        int[] iArr = new int[18];
        f1454b = iArr;
        try {
            iArr[p7.DOUBLE.ordinal()] = 1;
        } catch (NoSuchFieldError unused) {
        }
        try {
            f1454b[p7.FLOAT.ordinal()] = 2;
        } catch (NoSuchFieldError unused2) {
        }
        try {
            f1454b[p7.INT64.ordinal()] = 3;
        } catch (NoSuchFieldError unused3) {
        }
        try {
            f1454b[p7.UINT64.ordinal()] = 4;
        } catch (NoSuchFieldError unused4) {
        }
        try {
            f1454b[p7.INT32.ordinal()] = 5;
        } catch (NoSuchFieldError unused5) {
        }
        try {
            f1454b[p7.FIXED64.ordinal()] = 6;
        } catch (NoSuchFieldError unused6) {
        }
        try {
            f1454b[p7.FIXED32.ordinal()] = 7;
        } catch (NoSuchFieldError unused7) {
        }
        try {
            f1454b[p7.BOOL.ordinal()] = 8;
        } catch (NoSuchFieldError unused8) {
        }
        try {
            f1454b[p7.GROUP.ordinal()] = 9;
        } catch (NoSuchFieldError unused9) {
        }
        try {
            f1454b[p7.MESSAGE.ordinal()] = 10;
        } catch (NoSuchFieldError unused10) {
        }
        try {
            f1454b[p7.STRING.ordinal()] = 11;
        } catch (NoSuchFieldError unused11) {
        }
        try {
            f1454b[p7.BYTES.ordinal()] = 12;
        } catch (NoSuchFieldError unused12) {
        }
        try {
            f1454b[p7.UINT32.ordinal()] = 13;
        } catch (NoSuchFieldError unused13) {
        }
        try {
            f1454b[p7.SFIXED32.ordinal()] = 14;
        } catch (NoSuchFieldError unused14) {
        }
        try {
            f1454b[p7.SFIXED64.ordinal()] = 15;
        } catch (NoSuchFieldError unused15) {
        }
        try {
            f1454b[p7.SINT32.ordinal()] = 16;
        } catch (NoSuchFieldError unused16) {
        }
        try {
            f1454b[p7.SINT64.ordinal()] = 17;
        } catch (NoSuchFieldError unused17) {
        }
        try {
            f1454b[p7.ENUM.ordinal()] = 18;
        } catch (NoSuchFieldError unused18) {
        }
        w7.values();
        int[] iArr2 = new int[9];
        a = iArr2;
        try {
            w7 w7Var = w7.INT;
            iArr2[0] = 1;
        } catch (NoSuchFieldError unused19) {
        }
        try {
            int[] iArr3 = a;
            w7 w7Var2 = w7.LONG;
            iArr3[1] = 2;
        } catch (NoSuchFieldError unused20) {
        }
        try {
            int[] iArr4 = a;
            w7 w7Var3 = w7.FLOAT;
            iArr4[2] = 3;
        } catch (NoSuchFieldError unused21) {
        }
        try {
            int[] iArr5 = a;
            w7 w7Var4 = w7.DOUBLE;
            iArr5[3] = 4;
        } catch (NoSuchFieldError unused22) {
        }
        try {
            int[] iArr6 = a;
            w7 w7Var5 = w7.BOOLEAN;
            iArr6[4] = 5;
        } catch (NoSuchFieldError unused23) {
        }
        try {
            int[] iArr7 = a;
            w7 w7Var6 = w7.STRING;
            iArr7[5] = 6;
        } catch (NoSuchFieldError unused24) {
        }
        try {
            a[w7.BYTE_STRING.ordinal()] = 7;
        } catch (NoSuchFieldError unused25) {
        }
        try {
            a[w7.ENUM.ordinal()] = 8;
        } catch (NoSuchFieldError unused26) {
        }
        try {
            a[w7.MESSAGE.ordinal()] = 9;
        } catch (NoSuchFieldError unused27) {
        }
    }
}
