package b.i.a.f.h.l;
/* JADX WARN: Init of enum A can be incorrect */
/* JADX WARN: Init of enum B can be incorrect */
/* JADX WARN: Init of enum C can be incorrect */
/* JADX WARN: Init of enum D can be incorrect */
/* JADX WARN: Init of enum E can be incorrect */
/* JADX WARN: Init of enum F can be incorrect */
/* JADX WARN: Init of enum G can be incorrect */
/* JADX WARN: Init of enum H can be incorrect */
/* JADX WARN: Init of enum I can be incorrect */
/* JADX WARN: Init of enum J can be incorrect */
/* JADX WARN: Init of enum K can be incorrect */
/* JADX WARN: Init of enum L can be incorrect */
/* JADX WARN: Init of enum M can be incorrect */
/* JADX WARN: Init of enum N can be incorrect */
/* JADX WARN: Init of enum O can be incorrect */
/* JADX WARN: Init of enum P can be incorrect */
/* JADX WARN: Init of enum Q can be incorrect */
/* JADX WARN: Init of enum R can be incorrect */
/* JADX WARN: Init of enum S can be incorrect */
/* JADX WARN: Init of enum T can be incorrect */
/* JADX WARN: Init of enum U can be incorrect */
/* JADX WARN: Init of enum V can be incorrect */
/* JADX WARN: Init of enum W can be incorrect */
/* JADX WARN: Init of enum X can be incorrect */
/* JADX WARN: Init of enum Y can be incorrect */
/* JADX WARN: Init of enum Z can be incorrect */
/* JADX WARN: Init of enum a0 can be incorrect */
/* JADX WARN: Init of enum b0 can be incorrect */
/* JADX WARN: Init of enum c0 can be incorrect */
/* JADX WARN: Init of enum d0 can be incorrect */
/* JADX WARN: Init of enum e0 can be incorrect */
/* JADX WARN: Init of enum f0 can be incorrect */
/* JADX WARN: Init of enum g0 can be incorrect */
/* JADX WARN: Init of enum j can be incorrect */
/* JADX WARN: Init of enum k can be incorrect */
/* JADX WARN: Init of enum l can be incorrect */
/* JADX WARN: Init of enum m can be incorrect */
/* JADX WARN: Init of enum n can be incorrect */
/* JADX WARN: Init of enum o can be incorrect */
/* JADX WARN: Init of enum p can be incorrect */
/* JADX WARN: Init of enum q can be incorrect */
/* JADX WARN: Init of enum r can be incorrect */
/* JADX WARN: Init of enum s can be incorrect */
/* JADX WARN: Init of enum t can be incorrect */
/* JADX WARN: Init of enum u can be incorrect */
/* JADX WARN: Init of enum v can be incorrect */
/* JADX WARN: Init of enum w can be incorrect */
/* JADX WARN: Init of enum x can be incorrect */
/* JADX WARN: Init of enum y can be incorrect */
/* JADX WARN: Init of enum z can be incorrect */
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public enum o4 {
    DOUBLE(0, r7, r8),
    FLOAT(1, r7, r9),
    INT64(2, r7, r10),
    UINT64(3, r7, r10),
    INT32(4, r7, r11),
    FIXED64(5, r7, r10),
    FIXED32(6, r7, r11),
    BOOL(7, r7, r12),
    STRING(8, r7, r13),
    MESSAGE(9, r7, r14),
    BYTES(10, r7, r15),
    UINT32(11, r7, r11),
    ENUM(12, r7, r16),
    SFIXED32(13, r7, r11),
    SFIXED64(14, r7, r10),
    SINT32(15, r7, r11),
    SINT64(16, r7, r10),
    GROUP(17, r7, r14),
    DOUBLE_LIST(18, r7, r8),
    FLOAT_LIST(19, r7, r9),
    INT64_LIST(20, r7, r10),
    UINT64_LIST(21, r7, r10),
    INT32_LIST(22, r7, r11),
    FIXED64_LIST(23, r7, r10),
    FIXED32_LIST(24, r7, r11),
    BOOL_LIST(25, r7, r12),
    STRING_LIST(26, r7, r13),
    MESSAGE_LIST(27, r7, r14),
    BYTES_LIST(28, r7, r15),
    UINT32_LIST(29, r7, r11),
    ENUM_LIST(30, r7, r16),
    SFIXED32_LIST(31, r7, r11),
    SFIXED64_LIST(32, r7, r10),
    SINT32_LIST(33, r7, r11),
    SINT64_LIST(34, r7, r10),
    DOUBLE_LIST_PACKED(35, r13, r8),
    FLOAT_LIST_PACKED(36, r13, r9),
    INT64_LIST_PACKED(37, r13, r10),
    UINT64_LIST_PACKED(38, r13, r10),
    INT32_LIST_PACKED(39, r13, r11),
    FIXED64_LIST_PACKED(40, r13, r10),
    FIXED32_LIST_PACKED(41, r13, r11),
    BOOL_LIST_PACKED(42, r13, r12),
    UINT32_LIST_PACKED(43, r13, r11),
    ENUM_LIST_PACKED(44, r13, r16),
    SFIXED32_LIST_PACKED(45, r13, r11),
    SFIXED64_LIST_PACKED(46, r13, r8),
    SINT32_LIST_PACKED(47, r13, e5.INT),
    SINT64_LIST_PACKED(48, r13, r8),
    GROUP_LIST(49, r7, r14),
    MAP(50, q4.MAP, e5.VOID);
    

    /* renamed from: i0  reason: collision with root package name */
    public static final o4[] f1467i0 = new o4[51];
    private final e5 zzaz;
    private final int zzba;
    private final q4 zzbb;
    private final Class<?> zzbc;
    private final boolean zzbd;

    static {
        q4 q4Var = q4.SCALAR;
        e5 e5Var = e5.DOUBLE;
        e5 e5Var2 = e5.FLOAT;
        e5 e5Var3 = e5.LONG;
        e5 e5Var4 = e5.INT;
        e5 e5Var5 = e5.BOOLEAN;
        e5 e5Var6 = e5.STRING;
        e5 e5Var7 = e5.MESSAGE;
        e5 e5Var8 = e5.BYTE_STRING;
        e5 e5Var9 = e5.ENUM;
        q4 q4Var2 = q4.VECTOR;
        q4 q4Var3 = q4.PACKED_VECTOR;
        e5 e5Var10 = e5.LONG;
        o4[] values = values();
        for (int i = 0; i < 51; i++) {
            o4 o4Var = values[i];
            f1467i0[o4Var.zzba] = o4Var;
        }
    }

    o4(int i, q4 q4Var, e5 e5Var) {
        int i2;
        this.zzba = i;
        this.zzbb = q4Var;
        this.zzaz = e5Var;
        int i3 = r4.a[q4Var.ordinal()];
        boolean z2 = true;
        if (i3 == 1) {
            this.zzbc = e5Var.f();
        } else if (i3 != 2) {
            this.zzbc = null;
        } else {
            this.zzbc = e5Var.f();
        }
        this.zzbd = (q4Var != q4.SCALAR || (i2 = r4.f1486b[e5Var.ordinal()]) == 1 || i2 == 2 || i2 == 3) ? false : z2;
    }

    public final int a() {
        return this.zzba;
    }
}
