package b.i.a.f.h.n;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
/* compiled from: com.android.billingclient:billing@@4.0.0 */
/* loaded from: classes3.dex */
public final class b extends e implements d {
    public b(IBinder iBinder) {
        super(iBinder);
    }

    @Override // b.i.a.f.h.n.d
    public final Bundle E(int i, String str, String str2, String str3) throws RemoteException {
        Parcel c = c();
        c.writeInt(3);
        c.writeString(str);
        c.writeString(str2);
        c.writeString(str3);
        Parcel g = g(4, c);
        Bundle bundle = (Bundle) g.a(g, Bundle.CREATOR);
        g.recycle();
        return bundle;
    }

    @Override // b.i.a.f.h.n.d
    public final Bundle F(int i, String str, String str2, String str3, String str4) throws RemoteException {
        Parcel c = c();
        c.writeInt(3);
        c.writeString(str);
        c.writeString(str2);
        c.writeString(str3);
        c.writeString(null);
        Parcel g = g(3, c);
        Bundle bundle = (Bundle) g.a(g, Bundle.CREATOR);
        g.recycle();
        return bundle;
    }

    @Override // b.i.a.f.h.n.d
    public final int N(int i, String str, String str2) throws RemoteException {
        Parcel c = c();
        c.writeInt(i);
        c.writeString(str);
        c.writeString(str2);
        Parcel g = g(1, c);
        int readInt = g.readInt();
        g.recycle();
        return readInt;
    }

    @Override // b.i.a.f.h.n.d
    public final Bundle c0(int i, String str, String str2, String str3, String str4, Bundle bundle) throws RemoteException {
        Parcel c = c();
        c.writeInt(i);
        c.writeString(str);
        c.writeString(str2);
        c.writeString(str3);
        c.writeString(null);
        int i2 = g.a;
        c.writeInt(1);
        bundle.writeToParcel(c, 0);
        Parcel g = g(8, c);
        Bundle bundle2 = (Bundle) g.a(g, Bundle.CREATOR);
        g.recycle();
        return bundle2;
    }

    @Override // b.i.a.f.h.n.d
    public final Bundle e0(int i, String str, String str2, Bundle bundle) throws RemoteException {
        Parcel c = c();
        c.writeInt(3);
        c.writeString(str);
        c.writeString(str2);
        int i2 = g.a;
        c.writeInt(1);
        bundle.writeToParcel(c, 0);
        Parcel g = g(2, c);
        Bundle bundle2 = (Bundle) g.a(g, Bundle.CREATOR);
        g.recycle();
        return bundle2;
    }

    @Override // b.i.a.f.h.n.d
    public final Bundle o(int i, String str, String str2, Bundle bundle, Bundle bundle2) throws RemoteException {
        Parcel c = c();
        c.writeInt(10);
        c.writeString(str);
        c.writeString(str2);
        int i2 = g.a;
        c.writeInt(1);
        bundle.writeToParcel(c, 0);
        c.writeInt(1);
        bundle2.writeToParcel(c, 0);
        Parcel g = g(901, c);
        Bundle bundle3 = (Bundle) g.a(g, Bundle.CREATOR);
        g.recycle();
        return bundle3;
    }

    @Override // b.i.a.f.h.n.d
    public final Bundle o0(int i, String str, String str2, String str3, Bundle bundle) throws RemoteException {
        Parcel c = c();
        c.writeInt(9);
        c.writeString(str);
        c.writeString(str2);
        c.writeString(str3);
        int i2 = g.a;
        c.writeInt(1);
        bundle.writeToParcel(c, 0);
        Parcel g = g(11, c);
        Bundle bundle2 = (Bundle) g.a(g, Bundle.CREATOR);
        g.recycle();
        return bundle2;
    }

    @Override // b.i.a.f.h.n.d
    public final int q(int i, String str, String str2) throws RemoteException {
        Parcel c = c();
        c.writeInt(3);
        c.writeString(str);
        c.writeString(str2);
        Parcel g = g(5, c);
        int readInt = g.readInt();
        g.recycle();
        return readInt;
    }

    @Override // b.i.a.f.h.n.d
    public final Bundle r(int i, String str, String str2, Bundle bundle) throws RemoteException {
        Parcel c = c();
        c.writeInt(9);
        c.writeString(str);
        c.writeString(str2);
        int i2 = g.a;
        c.writeInt(1);
        bundle.writeToParcel(c, 0);
        Parcel g = g(12, c);
        Bundle bundle2 = (Bundle) g.a(g, Bundle.CREATOR);
        g.recycle();
        return bundle2;
    }
}
