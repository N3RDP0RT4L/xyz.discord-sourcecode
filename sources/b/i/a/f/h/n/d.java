package b.i.a.f.h.n;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
/* compiled from: com.android.billingclient:billing@@4.0.0 */
/* loaded from: classes3.dex */
public interface d extends IInterface {
    Bundle E(int i, String str, String str2, String str3) throws RemoteException;

    Bundle F(int i, String str, String str2, String str3, String str4) throws RemoteException;

    int N(int i, String str, String str2) throws RemoteException;

    Bundle c0(int i, String str, String str2, String str3, String str4, Bundle bundle) throws RemoteException;

    Bundle e0(int i, String str, String str2, Bundle bundle) throws RemoteException;

    Bundle o(int i, String str, String str2, Bundle bundle, Bundle bundle2) throws RemoteException;

    Bundle o0(int i, String str, String str2, String str3, Bundle bundle) throws RemoteException;

    int q(int i, String str, String str2) throws RemoteException;

    Bundle r(int i, String str, String str2, Bundle bundle) throws RemoteException;
}
