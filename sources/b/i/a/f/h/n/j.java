package b.i.a.f.h.n;

import b.i.a.f.e.o.f;
/* compiled from: com.android.billingclient:billing@@4.0.0 */
/* loaded from: classes3.dex */
public final class j extends k {
    public final transient int l;
    public final transient int m;
    public final /* synthetic */ k zzc;

    public j(k kVar, int i, int i2) {
        this.zzc = kVar;
        this.l = i;
        this.m = i2;
    }

    @Override // b.i.a.f.h.n.h
    public final int d() {
        return this.zzc.e() + this.l + this.m;
    }

    @Override // b.i.a.f.h.n.h
    public final int e() {
        return this.zzc.e() + this.l;
    }

    @Override // b.i.a.f.h.n.h
    public final Object[] g() {
        return this.zzc.g();
    }

    @Override // java.util.List
    public final Object get(int i) {
        f.F1(i, this.m, "index");
        return this.zzc.get(i + this.l);
    }

    @Override // b.i.a.f.h.n.k
    /* renamed from: h */
    public final k subList(int i, int i2) {
        f.s2(i, i2, this.m);
        k kVar = this.zzc;
        int i3 = this.l;
        return kVar.subList(i + i3, i2 + i3);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final int size() {
        return this.m;
    }
}
