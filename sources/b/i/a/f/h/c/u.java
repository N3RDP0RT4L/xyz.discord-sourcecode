package b.i.a.f.h.c;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.internal.p001authapi.zzz;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public final class u implements Parcelable.Creator<zzz> {
    @Override // android.os.Parcelable.Creator
    public final zzz createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        Credential credential = null;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            if (((char) readInt) != 1) {
                d.d2(parcel, readInt);
            } else {
                credential = (Credential) d.Q(parcel, readInt, Credential.CREATOR);
            }
        }
        d.f0(parcel, m2);
        return new zzz(credential);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzz[] newArray(int i) {
        return new zzz[i];
    }
}
