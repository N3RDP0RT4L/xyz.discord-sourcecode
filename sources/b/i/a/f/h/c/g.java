package b.i.a.f.h.c;

import android.content.Context;
import android.os.RemoteException;
import b.i.a.f.c.a.d.b;
import b.i.a.f.e.h.c;
import b.i.a.f.e.h.h;
import com.google.android.gms.auth.api.credentials.CredentialRequest;
import com.google.android.gms.common.api.Status;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public final class g extends m<b> {
    public final /* synthetic */ CredentialRequest l;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public g(c cVar, CredentialRequest credentialRequest) {
        super(cVar);
        this.l = credentialRequest;
    }

    @Override // com.google.android.gms.common.api.internal.BasePendingResult
    public final h d(Status status) {
        return new e(status, null);
    }

    @Override // b.i.a.f.h.c.m
    public final void m(Context context, t tVar) throws RemoteException {
        tVar.t(new j(this), this.l);
    }
}
