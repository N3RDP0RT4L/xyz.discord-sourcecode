package b.i.a.f.h.c;

import b.i.a.f.e.h.j.e;
import com.google.android.gms.common.api.Status;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public final class n extends f {
    public e<Status> a;

    public n(e<Status> eVar) {
        this.a = eVar;
    }

    @Override // b.i.a.f.h.c.r
    public final void l(Status status) {
        this.a.b(status);
    }
}
