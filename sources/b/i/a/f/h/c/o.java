package b.i.a.f.h.c;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.util.Base64;
import b.i.a.f.c.a.a;
import b.i.a.f.e.h.c;
import b.i.a.f.e.k.c;
import b.i.a.f.e.k.d;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public final class o extends d<t> {
    public final a.C0109a A;

    public o(Context context, Looper looper, c cVar, a.C0109a aVar, c.a aVar2, c.b bVar) {
        super(context, looper, 68, cVar, aVar2, bVar);
        a.C0109a.C0110a aVar3 = new a.C0109a.C0110a(aVar == null ? a.C0109a.j : aVar);
        byte[] bArr = new byte[16];
        a.a.nextBytes(bArr);
        aVar3.c = Base64.encodeToString(bArr, 11);
        this.A = new a.C0109a(aVar3);
    }

    @Override // b.i.a.f.e.k.b, b.i.a.f.e.h.a.f
    public final int l() {
        return 12800000;
    }

    @Override // b.i.a.f.e.k.b
    public final /* synthetic */ IInterface r(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.auth.api.credentials.internal.ICredentialsService");
        if (queryLocalInterface instanceof t) {
            return (t) queryLocalInterface;
        }
        return new s(iBinder);
    }

    @Override // b.i.a.f.e.k.b
    public final Bundle u() {
        a.C0109a aVar = this.A;
        Objects.requireNonNull(aVar);
        Bundle bundle = new Bundle();
        bundle.putString("consumer_package", aVar.k);
        bundle.putBoolean("force_save_dialog", aVar.l);
        bundle.putString("log_session_id", aVar.m);
        return bundle;
    }

    @Override // b.i.a.f.e.k.b
    public final String x() {
        return "com.google.android.gms.auth.api.credentials.internal.ICredentialsService";
    }

    @Override // b.i.a.f.e.k.b
    public final String y() {
        return "com.google.android.gms.auth.api.credentials.service.START";
    }
}
