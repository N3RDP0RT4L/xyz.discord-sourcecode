package b.i.a.f.h.c;

import android.content.Context;
import android.os.RemoteException;
import b.i.a.f.e.h.c;
import b.i.a.f.e.h.h;
import com.google.android.gms.common.api.Status;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public final class k extends m<Status> {
    public k(c cVar) {
        super(cVar);
    }

    @Override // com.google.android.gms.common.api.internal.BasePendingResult
    public final /* synthetic */ h d(Status status) {
        return status;
    }

    @Override // b.i.a.f.h.c.m
    public final void m(Context context, t tVar) throws RemoteException {
        tVar.M(new n(this));
    }
}
