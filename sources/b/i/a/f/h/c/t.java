package b.i.a.f.h.c;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.auth.api.credentials.CredentialRequest;
import com.google.android.gms.internal.p001authapi.zzt;
import com.google.android.gms.internal.p001authapi.zzz;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public interface t extends IInterface {
    void L(r rVar, zzt zztVar) throws RemoteException;

    void M(r rVar) throws RemoteException;

    void t(r rVar, CredentialRequest credentialRequest) throws RemoteException;

    void x(r rVar, zzz zzzVar) throws RemoteException;
}
