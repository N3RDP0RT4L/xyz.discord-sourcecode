package b.i.a.f.h.c;

import android.content.Context;
import android.os.DeadObjectException;
import android.os.RemoteException;
import b.i.a.f.c.a.a;
import b.i.a.f.e.h.c;
import b.i.a.f.e.h.h;
import b.i.a.f.e.h.j.d;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public abstract class m<R extends h> extends d<R, o> {
    public m(c cVar) {
        super(a.e, cVar);
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.android.gms.common.api.internal.BasePendingResult, b.i.a.f.e.h.j.e
    public /* bridge */ /* synthetic */ void b(Object obj) {
        b((h) obj);
    }

    @Override // b.i.a.f.e.h.j.d
    public void k(o oVar) throws RemoteException {
        o oVar2 = oVar;
        m(oVar2.d, (t) oVar2.w());
    }

    public abstract void m(Context context, t tVar) throws DeadObjectException, RemoteException;
}
