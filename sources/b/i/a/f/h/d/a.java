package b.i.a.f.h.d;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.text.TextUtils;
import b.i.a.f.c.a.b;
import b.i.a.f.e.h.c;
import b.i.a.f.e.k.c;
import b.i.a.f.e.k.d;
/* loaded from: classes3.dex */
public final class a extends d<b> {
    public final Bundle A;

    public a(Context context, Looper looper, c cVar, b.i.a.f.c.a.c cVar2, c.a aVar, c.b bVar) {
        super(context, looper, 16, cVar, aVar, bVar);
        if (cVar2 == null) {
            this.A = new Bundle();
            return;
        }
        throw new NoSuchMethodError();
    }

    @Override // b.i.a.f.e.k.b, b.i.a.f.e.h.a.f
    public final int l() {
        return 12451000;
    }

    @Override // b.i.a.f.e.k.b, b.i.a.f.e.h.a.f
    public final boolean o() {
        b.i.a.f.e.k.c cVar = this.f1378x;
        Account account = cVar.a;
        if (TextUtils.isEmpty(account != null ? account.name : null)) {
            return false;
        }
        if (cVar.d.get(b.c) == null) {
            return !cVar.f1376b.isEmpty();
        }
        throw null;
    }

    @Override // b.i.a.f.e.k.b
    public final /* synthetic */ IInterface r(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.auth.api.internal.IAuthService");
        if (queryLocalInterface instanceof b) {
            return (b) queryLocalInterface;
        }
        return new c(iBinder);
    }

    @Override // b.i.a.f.e.k.b
    public final Bundle u() {
        return this.A;
    }

    @Override // b.i.a.f.e.k.b
    public final String x() {
        return "com.google.android.gms.auth.api.internal.IAuthService";
    }

    @Override // b.i.a.f.e.k.b
    public final String y() {
        return "com.google.android.gms.auth.service.START";
    }
}
