package b.i.a.f.h.d;

import android.os.IBinder;
import android.os.IInterface;
/* loaded from: classes3.dex */
public final class c implements b, IInterface {
    public final IBinder a;

    public c(IBinder iBinder) {
        this.a = iBinder;
    }

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this.a;
    }
}
