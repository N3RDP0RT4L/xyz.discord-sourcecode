package b.i.a.f.h.j;

import android.os.SystemClock;
import b.i.a.f.e.o.b;
import b.i.a.f.e.o.c;
import java.util.Objects;
/* loaded from: classes3.dex */
public final class y0 {
    public final b a;

    /* renamed from: b  reason: collision with root package name */
    public long f1425b;

    public y0(b bVar) {
        Objects.requireNonNull(bVar, "null reference");
        this.a = bVar;
    }

    public final void a() {
        Objects.requireNonNull((c) this.a);
        this.f1425b = SystemClock.elapsedRealtime();
    }

    public final boolean b(long j) {
        if (this.f1425b == 0) {
            return true;
        }
        Objects.requireNonNull((c) this.a);
        return SystemClock.elapsedRealtime() - this.f1425b > j;
    }
}
