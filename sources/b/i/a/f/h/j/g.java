package b.i.a.f.h.j;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.SystemClock;
import b.c.a.a0.d;
import b.d.b.a.a;
import b.i.a.f.b.f;
import b.i.a.f.e.o.b;
import b.i.a.f.e.o.c;
import java.util.List;
import java.util.Objects;
import org.objectweb.asm.Opcodes;
@SuppressLint({"StaticFieldLeak"})
/* loaded from: classes3.dex */
public class g {
    public static volatile g a;

    /* renamed from: b  reason: collision with root package name */
    public final Context f1413b;
    public final Context c;
    public final b d;
    public final z e = new z(this);
    public final m0 f;
    public final f g;
    public final a h;
    public final c0 i;
    public final a1 j;
    public final p0 k;

    public g(i iVar) {
        Context context = iVar.a;
        d.z(context, "Application context can't be null");
        Context context2 = iVar.f1415b;
        Objects.requireNonNull(context2, "null reference");
        this.f1413b = context;
        this.c = context2;
        c cVar = c.a;
        this.d = cVar;
        m0 m0Var = new m0(this);
        m0Var.J();
        this.f = m0Var;
        m0 c = c();
        String str = f.a;
        c.a(4, a.j(a.b(str, Opcodes.I2F), "Google Analytics ", str, " is starting up. To enable debug logging on a device run:\n  adb shell setprop log.tag.GAv4 DEBUG\n  adb logcat -s GAv4"), null, null, null);
        p0 p0Var = new p0(this);
        p0Var.J();
        this.k = p0Var;
        a1 a1Var = new a1(this);
        a1Var.J();
        this.j = a1Var;
        a aVar = new a(this, iVar);
        Objects.requireNonNull(cVar, "null reference");
        q qVar = new q(this);
        if (f.a == null) {
            synchronized (f.class) {
                if (f.a == null) {
                    f.a = new f(context);
                }
            }
        }
        f fVar = f.a;
        fVar.e = new h(this);
        this.g = fVar;
        b.i.a.f.b.b bVar = new b.i.a.f.b.b(this);
        qVar.J();
        c0 c0Var = new c0(this);
        c0Var.J();
        this.i = c0Var;
        aVar.J();
        this.h = aVar;
        g gVar = bVar.a;
        a(gVar.j);
        a1 a1Var2 = gVar.j;
        a1Var2.N();
        a1Var2.N();
        if (a1Var2.p) {
            a1Var2.N();
        }
        a1Var2.N();
        r rVar = aVar.l;
        rVar.N();
        d.G(!rVar.l, "Analytics backend already started");
        rVar.l = true;
        rVar.q().a(new u(rVar));
    }

    public static void a(e eVar) {
        d.z(eVar, "Analytics service not created/initialized");
        d.o(eVar.I(), "Analytics service not initialized");
    }

    public static g b(Context context) {
        Objects.requireNonNull(context, "null reference");
        if (a == null) {
            synchronized (g.class) {
                if (a == null) {
                    long elapsedRealtime = SystemClock.elapsedRealtime();
                    g gVar = new g(new i(context));
                    a = gVar;
                    synchronized (b.i.a.f.b.b.class) {
                        List<Runnable> list = b.i.a.f.b.b.f1326b;
                        if (list != null) {
                            for (Runnable runnable : list) {
                                runnable.run();
                            }
                            b.i.a.f.b.b.f1326b = null;
                        }
                    }
                    long elapsedRealtime2 = SystemClock.elapsedRealtime() - elapsedRealtime;
                    long longValue = e0.B.a.longValue();
                    if (elapsedRealtime2 > longValue) {
                        gVar.c().f("Slow initialization (ms)", Long.valueOf(elapsedRealtime2), Long.valueOf(longValue));
                    }
                }
            }
        }
        return a;
    }

    public final m0 c() {
        a(this.f);
        return this.f;
    }

    public final f d() {
        Objects.requireNonNull(this.g, "null reference");
        return this.g;
    }

    public final a e() {
        a(this.h);
        return this.h;
    }
}
