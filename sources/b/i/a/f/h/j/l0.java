package b.i.a.f.h.j;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;
import java.util.Map;
/* loaded from: classes3.dex */
public final class l0 implements k0, IInterface {
    public final IBinder a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1418b = "com.google.android.gms.analytics.internal.IAnalyticsService";

    public l0(IBinder iBinder) {
        this.a = iBinder;
    }

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this.a;
    }

    @Override // b.i.a.f.h.j.k0
    public final void g0(Map map, long j, String str, List<?> list) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.f1418b);
        obtain.writeMap(map);
        obtain.writeLong(j);
        obtain.writeString(str);
        obtain.writeTypedList(list);
        Parcel obtain2 = Parcel.obtain();
        try {
            this.a.transact(1, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }
}
