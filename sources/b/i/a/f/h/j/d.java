package b.i.a.f.h.j;

import android.text.TextUtils;
import b.i.a.f.b.f;
import java.util.Objects;
/* loaded from: classes3.dex */
public class d {
    public final g j;

    public d(g gVar) {
        Objects.requireNonNull(gVar, "null reference");
        this.j = gVar;
    }

    public static String c(Object obj) {
        if (obj == null) {
            return "";
        }
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof Boolean) {
            return obj == Boolean.TRUE ? "true" : "false";
        }
        if (obj instanceof Throwable) {
            return ((Throwable) obj).toString();
        }
        return obj.toString();
    }

    public static String e(String str, Object obj, Object obj2, Object obj3) {
        String str2 = "";
        if (str == null) {
            str = str2;
        }
        String c = c(obj);
        String c2 = c(obj2);
        String c3 = c(obj3);
        StringBuilder sb = new StringBuilder();
        if (!TextUtils.isEmpty(str)) {
            sb.append(str);
            str2 = ": ";
        }
        String str3 = ", ";
        if (!TextUtils.isEmpty(c)) {
            sb.append(str2);
            sb.append(c);
            str2 = str3;
        }
        if (!TextUtils.isEmpty(c2)) {
            sb.append(str2);
            sb.append(c2);
        } else {
            str3 = str2;
        }
        if (!TextUtils.isEmpty(c3)) {
            sb.append(str3);
            sb.append(c3);
        }
        return sb.toString();
    }

    public final void A(String str, Object obj) {
        a(6, str, obj, null, null);
    }

    public final void C(String str) {
        a(2, str, null, null, null);
    }

    public final void D(String str) {
        a(5, str, null, null, null);
    }

    public final void H(String str) {
        a(6, str, null, null, null);
    }

    /* JADX WARN: Code restructure failed: missing block: B:33:0x00a7, code lost:
        r1.o.a(r6);
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void a(int r6, java.lang.String r7, java.lang.Object r8, java.lang.Object r9, java.lang.Object r10) {
        /*
            r5 = this;
            b.i.a.f.h.j.g r0 = r5.j
            r1 = 0
            if (r0 == 0) goto L8
            b.i.a.f.h.j.m0 r0 = r0.f
            goto L9
        L8:
            r0 = r1
        L9:
            if (r0 == 0) goto Lb2
            b.i.a.f.h.j.f0<java.lang.String> r2 = b.i.a.f.h.j.e0.f1407b
            V r2 = r2.a
            java.lang.String r2 = (java.lang.String) r2
            boolean r3 = android.util.Log.isLoggable(r2, r6)
            if (r3 == 0) goto L1e
            java.lang.String r3 = e(r7, r8, r9, r10)
            android.util.Log.println(r6, r2, r3)
        L1e:
            r2 = 5
            if (r6 < r2) goto Lb1
            monitor-enter(r0)
            java.lang.String r2 = "null reference"
            java.util.Objects.requireNonNull(r7, r2)     // Catch: java.lang.Throwable -> Lae
            r2 = 0
            if (r6 >= 0) goto L2b
            r6 = 0
        L2b:
            r3 = 9
            if (r6 < r3) goto L31
            r6 = 8
        L31:
            b.i.a.f.h.j.g r3 = r0.j     // Catch: java.lang.Throwable -> Lae
            b.i.a.f.h.j.z r3 = r3.e     // Catch: java.lang.Throwable -> Lae
            boolean r3 = r3.a()     // Catch: java.lang.Throwable -> Lae
            if (r3 == 0) goto L3e
            r3 = 67
            goto L40
        L3e:
            r3 = 99
        L40:
            java.lang.String r4 = "01VDIWEA?"
            char r6 = r4.charAt(r6)     // Catch: java.lang.Throwable -> Lae
            java.lang.String r4 = b.i.a.f.h.j.f.a     // Catch: java.lang.Throwable -> Lae
            java.lang.String r8 = b.i.a.f.h.j.m0.R(r8)     // Catch: java.lang.Throwable -> Lae
            java.lang.String r9 = b.i.a.f.h.j.m0.R(r9)     // Catch: java.lang.Throwable -> Lae
            java.lang.String r10 = b.i.a.f.h.j.m0.R(r10)     // Catch: java.lang.Throwable -> Lae
            java.lang.String r7 = e(r7, r8, r9, r10)     // Catch: java.lang.Throwable -> Lae
            java.lang.String r8 = java.lang.String.valueOf(r4)     // Catch: java.lang.Throwable -> Lae
            int r8 = r8.length()     // Catch: java.lang.Throwable -> Lae
            int r8 = r8 + 4
            java.lang.String r9 = java.lang.String.valueOf(r7)     // Catch: java.lang.Throwable -> Lae
            int r9 = r9.length()     // Catch: java.lang.Throwable -> Lae
            int r8 = r8 + r9
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch: java.lang.Throwable -> Lae
            r9.<init>(r8)     // Catch: java.lang.Throwable -> Lae
            java.lang.String r8 = "3"
            r9.append(r8)     // Catch: java.lang.Throwable -> Lae
            r9.append(r6)     // Catch: java.lang.Throwable -> Lae
            r9.append(r3)     // Catch: java.lang.Throwable -> Lae
            r9.append(r4)     // Catch: java.lang.Throwable -> Lae
            java.lang.String r6 = ":"
            r9.append(r6)     // Catch: java.lang.Throwable -> Lae
            r9.append(r7)     // Catch: java.lang.Throwable -> Lae
            java.lang.String r6 = r9.toString()     // Catch: java.lang.Throwable -> Lae
            int r7 = r6.length()     // Catch: java.lang.Throwable -> Lae
            r8 = 1024(0x400, float:1.435E-42)
            if (r7 <= r8) goto L96
            java.lang.String r6 = r6.substring(r2, r8)     // Catch: java.lang.Throwable -> Lae
        L96:
            b.i.a.f.h.j.g r7 = r0.j     // Catch: java.lang.Throwable -> Lae
            b.i.a.f.h.j.p0 r8 = r7.k     // Catch: java.lang.Throwable -> Lae
            if (r8 == 0) goto La5
            boolean r8 = r8.I()     // Catch: java.lang.Throwable -> Lae
            if (r8 != 0) goto La3
            goto La5
        La3:
            b.i.a.f.h.j.p0 r1 = r7.k     // Catch: java.lang.Throwable -> Lae
        La5:
            if (r1 == 0) goto Lac
            b.i.a.f.h.j.r0 r7 = r1.o     // Catch: java.lang.Throwable -> Lae
            r7.a(r6)     // Catch: java.lang.Throwable -> Lae
        Lac:
            monitor-exit(r0)
            goto Lb1
        Lae:
            r6 = move-exception
            monitor-exit(r0)
            throw r6
        Lb1:
            return
        Lb2:
            b.i.a.f.h.j.f0<java.lang.String> r0 = b.i.a.f.h.j.e0.f1407b
            V r0 = r0.a
            java.lang.String r0 = (java.lang.String) r0
            boolean r1 = android.util.Log.isLoggable(r0, r6)
            if (r1 == 0) goto Lc5
            java.lang.String r7 = e(r7, r8, r9, r10)
            android.util.Log.println(r6, r0, r7)
        Lc5:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.h.j.d.a(int, java.lang.String, java.lang.Object, java.lang.Object, java.lang.Object):void");
    }

    public final void b(String str, Object obj) {
        a(2, str, obj, null, null);
    }

    public final void d(String str, Object obj) {
        a(3, str, obj, null, null);
    }

    public final void f(String str, Object obj, Object obj2) {
        a(5, str, obj, obj2, null);
    }

    public final m0 n() {
        return this.j.c();
    }

    public final f q() {
        return this.j.d();
    }

    public final a s() {
        return this.j.e();
    }

    public final a1 t() {
        g gVar = this.j;
        g.a(gVar.j);
        return gVar.j;
    }

    public final p0 u() {
        g gVar = this.j;
        g.a(gVar.k);
        return gVar.k;
    }

    public final void x(String str, Object obj) {
        a(5, str, obj, null, null);
    }

    public final void y(String str, Object obj, Object obj2) {
        a(6, str, obj, obj2, null);
    }
}
