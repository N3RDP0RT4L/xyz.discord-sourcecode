package b.i.a.f.h.j;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import androidx.core.app.NotificationCompat;
/* loaded from: classes3.dex */
public final class c0 extends e {
    public boolean l;
    public boolean m;
    public final AlarmManager n = (AlarmManager) this.j.f1413b.getSystemService(NotificationCompat.CATEGORY_ALARM);
    public Integer o;

    public c0(g gVar) {
        super(gVar);
    }

    @Override // b.i.a.f.h.j.e
    public final void L() {
        try {
            O();
            if (z.b() > 0) {
                Context context = this.j.f1413b;
                ActivityInfo receiverInfo = context.getPackageManager().getReceiverInfo(new ComponentName(context, "com.google.android.gms.analytics.AnalyticsReceiver"), 0);
                if (receiverInfo != null && receiverInfo.enabled) {
                    C("Receiver registered for local dispatch.");
                    this.l = true;
                }
            }
        } catch (PackageManager.NameNotFoundException unused) {
        }
    }

    public final void O() {
        this.m = false;
        this.n.cancel(S());
        if (Build.VERSION.SDK_INT >= 24) {
            int R = R();
            b("Cancelling job. JobID", Integer.valueOf(R));
            ((JobScheduler) this.j.f1413b.getSystemService("jobscheduler")).cancel(R);
        }
    }

    public final int R() {
        if (this.o == null) {
            String valueOf = String.valueOf(this.j.f1413b.getPackageName());
            this.o = Integer.valueOf((valueOf.length() != 0 ? "analytics".concat(valueOf) : new String("analytics")).hashCode());
        }
        return this.o.intValue();
    }

    public final PendingIntent S() {
        Context context = this.j.f1413b;
        return PendingIntent.getBroadcast(context, 0, new Intent("com.google.android.gms.analytics.ANALYTICS_DISPATCH").setComponent(new ComponentName(context, "com.google.android.gms.analytics.AnalyticsReceiver")), 0);
    }
}
