package b.i.a.f.h.j;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.util.Log;
import b.i.a.f.b.f;
/* loaded from: classes3.dex */
public final class q extends e {
    public final f1 l = new f1();

    public q(g gVar) {
        super(gVar);
    }

    @Override // b.i.a.f.h.j.e
    public final void L() {
        f q = q();
        if (q.d == null) {
            synchronized (q) {
                if (q.d == null) {
                    f1 f1Var = new f1();
                    PackageManager packageManager = q.f1327b.getPackageManager();
                    String packageName = q.f1327b.getPackageName();
                    f1Var.c = packageName;
                    f1Var.d = packageManager.getInstallerPackageName(packageName);
                    String str = null;
                    try {
                        PackageInfo packageInfo = packageManager.getPackageInfo(q.f1327b.getPackageName(), 0);
                        if (packageInfo != null) {
                            CharSequence applicationLabel = packageManager.getApplicationLabel(packageInfo.applicationInfo);
                            if (!TextUtils.isEmpty(applicationLabel)) {
                                packageName = applicationLabel.toString();
                            }
                            str = packageInfo.versionName;
                        }
                    } catch (PackageManager.NameNotFoundException unused) {
                        String valueOf = String.valueOf(packageName);
                        Log.e("GAv4", valueOf.length() != 0 ? "Error retrieving package info: appName set to ".concat(valueOf) : new String("Error retrieving package info: appName set to "));
                    }
                    f1Var.a = packageName;
                    f1Var.f1412b = str;
                    q.d = f1Var;
                }
            }
        }
        f1 f1Var2 = q.d;
        f1 f1Var3 = this.l;
        if (!TextUtils.isEmpty(f1Var2.a)) {
            f1Var3.a = f1Var2.a;
        }
        if (!TextUtils.isEmpty(f1Var2.f1412b)) {
            f1Var3.f1412b = f1Var2.f1412b;
        }
        if (!TextUtils.isEmpty(f1Var2.c)) {
            f1Var3.c = f1Var2.c;
        }
        if (!TextUtils.isEmpty(f1Var2.d)) {
            f1Var3.d = f1Var2.d;
        }
        a1 t = t();
        t.N();
        String str2 = t.m;
        if (str2 != null) {
            this.l.a = str2;
        }
        t.N();
        String str3 = t.l;
        if (str3 != null) {
            this.l.f1412b = str3;
        }
    }
}
