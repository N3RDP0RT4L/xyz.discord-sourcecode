package b.i.a.f.h.j;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteException;
import android.os.Build;
import android.os.PersistableBundle;
import android.os.SystemClock;
import b.c.a.a0.d;
import b.i.a.f.b.f;
import b.i.a.f.e.n.a;
import b.i.a.f.e.o.c;
import b.i.a.f.e.p.b;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import org.objectweb.asm.Opcodes;
/* loaded from: classes3.dex */
public final class r extends e {
    public boolean l;
    public final o m;
    public final o0 n;
    public final n0 o;
    public final j p;
    public final a0 r;

    /* renamed from: s  reason: collision with root package name */
    public final a0 f1420s;
    public long u;
    public boolean v;
    public long q = Long.MIN_VALUE;
    public final y0 t = new y0(this.j.d);

    public r(g gVar, i iVar) {
        super(gVar);
        this.o = new n0(gVar);
        this.m = new o(gVar);
        this.n = new o0(gVar);
        this.p = new j(gVar);
        this.r = new s(this, gVar);
        this.f1420s = new t(this, gVar);
    }

    @Override // b.i.a.f.h.j.e
    public final void L() {
        this.m.J();
        this.n.J();
        this.p.J();
    }

    public final void O() {
        f.b();
        f.b();
        N();
        if (!e0.a.a.booleanValue()) {
            D("Service client disabled. Can't dispatch local hits to device AnalyticsService");
        }
        if (!this.p.R()) {
            C("Service not connected");
        } else if (!this.m.R()) {
            C("Dispatching local hits to device AnalyticsService");
            while (true) {
                try {
                    ArrayList arrayList = (ArrayList) this.m.T(z.c());
                    if (arrayList.isEmpty()) {
                        U();
                        return;
                    }
                    while (true) {
                        if (!arrayList.isEmpty()) {
                            j0 j0Var = (j0) arrayList.get(0);
                            if (!this.p.S(j0Var)) {
                                U();
                                return;
                            }
                            arrayList.remove(j0Var);
                            try {
                                this.m.W(j0Var.c);
                            } catch (SQLiteException e) {
                                A("Failed to remove hit that was send for delivery", e);
                                W();
                                return;
                            }
                        }
                    }
                } catch (SQLiteException e2) {
                    A("Failed to read hits from store", e2);
                    W();
                    return;
                }
            }
        }
    }

    public final void R(d0 d0Var) {
        long j;
        long j2 = this.u;
        f.b();
        N();
        long O = u().O();
        if (O != 0) {
            Objects.requireNonNull((c) this.j.d);
            j = Math.abs(System.currentTimeMillis() - O);
        } else {
            j = -1;
        }
        d("Dispatching local hits. Elapsed time since last dispatch (ms)", Long.valueOf(j));
        S();
        try {
            T();
            u().R();
            U();
            if (d0Var != null) {
                d0Var.a(null);
            }
            if (this.u != j2) {
                Context context = this.o.f1419b.f1413b;
                Intent intent = new Intent("com.google.analytics.RADIO_POWERED");
                intent.addCategory(context.getPackageName());
                intent.putExtra(n0.a, true);
                context.sendOrderedBroadcast(intent, null);
            }
        } catch (Exception e) {
            A("Local dispatch failed", e);
            u().R();
            U();
            if (d0Var != null) {
                d0Var.a(e);
            }
        }
    }

    public final void S() {
        k0 k0Var;
        if (!this.v && e0.a.a.booleanValue() && !this.p.R()) {
            if (this.t.b(e0.f1411z.a.longValue())) {
                this.t.a();
                C("Connecting to service");
                j jVar = this.p;
                Objects.requireNonNull(jVar);
                f.b();
                jVar.N();
                boolean z2 = true;
                if (jVar.m == null) {
                    l lVar = jVar.l;
                    Objects.requireNonNull(lVar);
                    f.b();
                    Intent intent = new Intent("com.google.android.gms.analytics.service.START");
                    intent.setComponent(new ComponentName("com.google.android.gms", "com.google.android.gms.analytics.service.AnalyticsService"));
                    Context context = lVar.l.j.f1413b;
                    intent.putExtra("app_package_name", context.getPackageName());
                    a b2 = a.b();
                    synchronized (lVar) {
                        k0Var = null;
                        lVar.j = null;
                        lVar.k = true;
                        boolean a = b2.a(context, intent, lVar.l.l, Opcodes.LOR);
                        lVar.l.b("Bind to service requested", Boolean.valueOf(a));
                        if (!a) {
                            lVar.k = false;
                        } else {
                            try {
                                lVar.wait(e0.f1410y.a.longValue());
                            } catch (InterruptedException unused) {
                                lVar.l.D("Wait for service connect was interrupted");
                            }
                            lVar.k = false;
                            k0 k0Var2 = lVar.j;
                            lVar.j = null;
                            if (k0Var2 == null) {
                                lVar.l.H("Successfully bound to service but never got onServiceConnected callback");
                            }
                            k0Var = k0Var2;
                        }
                    }
                    if (k0Var != null) {
                        jVar.m = k0Var;
                        jVar.T();
                    } else {
                        z2 = false;
                    }
                }
                if (z2) {
                    C("Connected to service");
                    this.t.f1425b = 0L;
                    O();
                }
            }
        }
    }

    public final boolean T() {
        f.b();
        N();
        C("Dispatching a batch of local hits");
        boolean z2 = !this.p.R();
        boolean z3 = !this.n.W();
        if (!z2 || !z3) {
            long max = Math.max(z.c(), e0.h.a.intValue());
            ArrayList arrayList = new ArrayList();
            long j = 0;
            while (true) {
                try {
                    o oVar = this.m;
                    oVar.N();
                    oVar.O().beginTransaction();
                    arrayList.clear();
                    try {
                        List<j0> T = this.m.T(max);
                        ArrayList arrayList2 = (ArrayList) T;
                        if (arrayList2.isEmpty()) {
                            C("Store is empty, nothing to dispatch");
                            W();
                            try {
                                this.m.setTransactionSuccessful();
                                this.m.endTransaction();
                                return false;
                            } catch (SQLiteException e) {
                                A("Failed to commit local dispatch transaction", e);
                                W();
                                return false;
                            }
                        } else {
                            b("Hits loaded from store. count", Integer.valueOf(arrayList2.size()));
                            Iterator it = arrayList2.iterator();
                            while (it.hasNext()) {
                                if (((j0) it.next()).c == j) {
                                    y("Database contains successfully uploaded hit", Long.valueOf(j), Integer.valueOf(arrayList2.size()));
                                    W();
                                    try {
                                        this.m.setTransactionSuccessful();
                                        this.m.endTransaction();
                                        return false;
                                    } catch (SQLiteException e2) {
                                        A("Failed to commit local dispatch transaction", e2);
                                        W();
                                        return false;
                                    }
                                }
                            }
                            if (this.p.R()) {
                                C("Service connected, sending hits to the service");
                                while (!arrayList2.isEmpty()) {
                                    j0 j0Var = (j0) arrayList2.get(0);
                                    if (!this.p.S(j0Var)) {
                                        break;
                                    }
                                    j = Math.max(j, j0Var.c);
                                    arrayList2.remove(j0Var);
                                    d("Hit sent do device AnalyticsService for delivery", j0Var);
                                    try {
                                        this.m.W(j0Var.c);
                                        arrayList.add(Long.valueOf(j0Var.c));
                                    } catch (SQLiteException e3) {
                                        A("Failed to remove hit that was send for delivery", e3);
                                        W();
                                        try {
                                            this.m.setTransactionSuccessful();
                                            this.m.endTransaction();
                                            return false;
                                        } catch (SQLiteException e4) {
                                            A("Failed to commit local dispatch transaction", e4);
                                            W();
                                            return false;
                                        }
                                    }
                                }
                            }
                            if (this.n.W()) {
                                List<Long> V = this.n.V(T);
                                for (Long l : V) {
                                    j = Math.max(j, l.longValue());
                                }
                                try {
                                    this.m.S(V);
                                    arrayList.addAll(V);
                                } catch (SQLiteException e5) {
                                    A("Failed to remove successfully uploaded hits", e5);
                                    W();
                                    try {
                                        this.m.setTransactionSuccessful();
                                        this.m.endTransaction();
                                        return false;
                                    } catch (SQLiteException e6) {
                                        A("Failed to commit local dispatch transaction", e6);
                                        W();
                                        return false;
                                    }
                                }
                            }
                            if (arrayList.isEmpty()) {
                                try {
                                    this.m.setTransactionSuccessful();
                                    this.m.endTransaction();
                                    return false;
                                } catch (SQLiteException e7) {
                                    A("Failed to commit local dispatch transaction", e7);
                                    W();
                                    return false;
                                }
                            } else {
                                try {
                                    this.m.setTransactionSuccessful();
                                    this.m.endTransaction();
                                } catch (SQLiteException e8) {
                                    A("Failed to commit local dispatch transaction", e8);
                                    W();
                                    return false;
                                }
                            }
                        }
                    } catch (SQLiteException e9) {
                        x("Failed to read hits from persisted store", e9);
                        W();
                        try {
                            this.m.setTransactionSuccessful();
                            this.m.endTransaction();
                            return false;
                        } catch (SQLiteException e10) {
                            A("Failed to commit local dispatch transaction", e10);
                            W();
                            return false;
                        }
                    }
                } catch (Throwable th) {
                    this.m.setTransactionSuccessful();
                    this.m.endTransaction();
                    throw th;
                }
                try {
                    this.m.setTransactionSuccessful();
                    this.m.endTransaction();
                    throw th;
                } catch (SQLiteException e11) {
                    A("Failed to commit local dispatch transaction", e11);
                    W();
                    return false;
                }
            }
        } else {
            C("No network or service available. Will retry later");
            return false;
        }
    }

    public final void U() {
        long j;
        long j2;
        f.b();
        N();
        boolean z2 = true;
        long j3 = 0;
        if (!(!this.v && X() > 0)) {
            this.o.a();
            W();
        } else if (this.m.R()) {
            this.o.a();
            W();
        } else {
            if (!e0.w.a.booleanValue()) {
                n0 n0Var = this.o;
                n0Var.f1419b.c();
                n0Var.f1419b.e();
                if (!n0Var.c) {
                    Context context = n0Var.f1419b.f1413b;
                    context.registerReceiver(n0Var, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
                    IntentFilter intentFilter = new IntentFilter("com.google.analytics.RADIO_POWERED");
                    intentFilter.addCategory(context.getPackageName());
                    context.registerReceiver(n0Var, intentFilter);
                    n0Var.d = n0Var.b();
                    n0Var.f1419b.c().b("Registering connectivity change receiver. Network connected", Boolean.valueOf(n0Var.d));
                    n0Var.c = true;
                }
                n0 n0Var2 = this.o;
                if (!n0Var2.c) {
                    n0Var2.f1419b.c().D("Connectivity unknown. Receiver not registered");
                }
                z2 = n0Var2.d;
            }
            if (z2) {
                V();
                long X = X();
                long O = u().O();
                if (O != 0) {
                    Objects.requireNonNull((c) this.j.d);
                    j = X - Math.abs(System.currentTimeMillis() - O);
                    if (j <= 0) {
                        j = Math.min(e0.d.a.longValue(), X);
                    }
                } else {
                    j = Math.min(e0.d.a.longValue(), X);
                }
                b("Dispatch scheduled (ms)", Long.valueOf(j));
                if (this.r.d()) {
                    a0 a0Var = this.r;
                    if (a0Var.d == 0) {
                        j2 = 0;
                    } else {
                        Objects.requireNonNull((c) a0Var.f1405b.d);
                        j2 = Math.abs(System.currentTimeMillis() - a0Var.d);
                    }
                    long max = Math.max(1L, j + j2);
                    a0 a0Var2 = this.r;
                    if (a0Var2.d()) {
                        if (max < 0) {
                            a0Var2.a();
                            return;
                        }
                        Objects.requireNonNull((c) a0Var2.f1405b.d);
                        long abs = max - Math.abs(System.currentTimeMillis() - a0Var2.d);
                        if (abs >= 0) {
                            j3 = abs;
                        }
                        a0Var2.b().removeCallbacks(a0Var2.c);
                        if (!a0Var2.b().postDelayed(a0Var2.c, j3)) {
                            a0Var2.f1405b.c().A("Failed to adjust delayed post. time", Long.valueOf(j3));
                            return;
                        }
                        return;
                    }
                    return;
                }
                this.r.e(j);
                return;
            }
            W();
            V();
        }
    }

    public final void V() {
        long j;
        g gVar = this.j;
        g.a(gVar.i);
        c0 c0Var = gVar.i;
        if (c0Var.l && !c0Var.m) {
            f.b();
            N();
            try {
                j = this.m.V();
            } catch (SQLiteException e) {
                A("Failed to get min/max hit times from local store", e);
                j = 0;
            }
            if (j != 0) {
                Objects.requireNonNull((c) this.j.d);
                if (Math.abs(System.currentTimeMillis() - j) <= e0.f.a.longValue()) {
                    b("Dispatch alarm scheduled (ms)", Long.valueOf(z.b()));
                    c0Var.N();
                    d.G(c0Var.l, "Receiver not registered");
                    long b2 = z.b();
                    if (b2 > 0) {
                        c0Var.O();
                        Objects.requireNonNull((c) c0Var.j.d);
                        long elapsedRealtime = SystemClock.elapsedRealtime() + b2;
                        c0Var.m = true;
                        e0.C.a.booleanValue();
                        if (Build.VERSION.SDK_INT >= 24) {
                            c0Var.C("Scheduling upload with JobScheduler");
                            Context context = c0Var.j.f1413b;
                            ComponentName componentName = new ComponentName(context, "com.google.android.gms.analytics.AnalyticsJobService");
                            int R = c0Var.R();
                            PersistableBundle persistableBundle = new PersistableBundle();
                            persistableBundle.putString("action", "com.google.android.gms.analytics.ANALYTICS_DISPATCH");
                            JobInfo build = new JobInfo.Builder(R, componentName).setMinimumLatency(b2).setOverrideDeadline(b2 << 1).setExtras(persistableBundle).build();
                            c0Var.b("Scheduling job. JobID", Integer.valueOf(R));
                            Method method = b1.a;
                            JobScheduler jobScheduler = (JobScheduler) context.getSystemService("jobscheduler");
                            if (b1.a != null) {
                                Objects.requireNonNull((c1) b1.c);
                            }
                            jobScheduler.schedule(build);
                            return;
                        }
                        c0Var.C("Scheduling upload with AlarmManager");
                        c0Var.n.setInexactRepeating(2, elapsedRealtime, b2, c0Var.S());
                    }
                }
            }
        }
    }

    public final void W() {
        if (this.r.d()) {
            C("All hits dispatched or no network/service. Going to power save mode");
        }
        this.r.a();
        g gVar = this.j;
        g.a(gVar.i);
        c0 c0Var = gVar.i;
        if (c0Var.m) {
            c0Var.O();
        }
    }

    public final long X() {
        long j = this.q;
        if (j != Long.MIN_VALUE) {
            return j;
        }
        long longValue = e0.c.a.longValue();
        a1 t = t();
        t.N();
        if (!t.n) {
            return longValue;
        }
        a1 t2 = t();
        t2.N();
        return t2.o * 1000;
    }

    public final boolean b0(String str) {
        return b.a(this.j.f1413b).a.checkCallingOrSelfPermission(str) == 0;
    }
}
