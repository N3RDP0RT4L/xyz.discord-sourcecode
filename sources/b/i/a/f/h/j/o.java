package b.i.a.f.h.j;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import b.c.a.a0.d;
import b.i.a.f.b.f;
import b.i.a.f.e.o.c;
import b.i.a.f.e.o.e;
import com.adjust.sdk.Constants;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemGuildWelcomeKt;
import java.io.Closeable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
/* loaded from: classes3.dex */
public final class o extends e implements Closeable {
    public static final String l = String.format("CREATE TABLE IF NOT EXISTS %s ( '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '%s' INTEGER NOT NULL, '%s' TEXT NOT NULL, '%s' TEXT NOT NULL, '%s' INTEGER);", "hits2", "hit_id", "hit_time", "hit_url", "hit_string", "hit_app_id");
    public static final String m = String.format("SELECT MAX(%s) FROM %s WHERE 1;", "hit_time", "hits2");
    public final p n;
    public final y0 o = new y0(this.j.d);
    public final y0 p = new y0(this.j.d);

    public o(g gVar) {
        super(gVar);
        this.n = new p(this, gVar.f1413b, "google_analytics_v4.db");
    }

    @Override // b.i.a.f.h.j.e
    public final void L() {
    }

    public final SQLiteDatabase O() {
        try {
            return this.n.getWritableDatabase();
        } catch (SQLiteException e) {
            x("Error opening database", e);
            throw e;
        }
    }

    public final boolean R() {
        f.b();
        N();
        Cursor cursor = null;
        try {
            try {
                Cursor rawQuery = O().rawQuery("SELECT COUNT(*) FROM hits2", null);
                if (rawQuery.moveToFirst()) {
                    long j = rawQuery.getLong(0);
                    rawQuery.close();
                    return j == 0;
                }
                throw new SQLiteException("Database returned empty set");
            } catch (SQLiteException e) {
                y("Database error", "SELECT COUNT(*) FROM hits2", e);
                throw e;
            }
        } catch (Throwable th) {
            if (0 != 0) {
                cursor.close();
            }
            throw th;
        }
    }

    public final void S(List<Long> list) {
        f.b();
        N();
        if (!list.isEmpty()) {
            StringBuilder sb = new StringBuilder("hit_id");
            sb.append(" in (");
            for (int i = 0; i < list.size(); i++) {
                Long l2 = list.get(i);
                if (l2 == null || l2.longValue() == 0) {
                    throw new SQLiteException("Invalid hit id");
                }
                if (i > 0) {
                    sb.append(",");
                }
                sb.append(l2);
            }
            sb.append(")");
            String sb2 = sb.toString();
            try {
                SQLiteDatabase O = O();
                b("Deleting dispatched hits. count", Integer.valueOf(list.size()));
                int delete = O.delete("hits2", sb2, null);
                if (delete != list.size()) {
                    a(5, "Deleted fewer hits then expected", Integer.valueOf(list.size()), Integer.valueOf(delete), sb2);
                }
            } catch (SQLiteException e) {
                A("Error deleting hits", e);
                throw e;
            }
        }
    }

    public final List<j0> T(long j) {
        boolean z2;
        d.l(j >= 0);
        f.b();
        N();
        Cursor cursor = null;
        try {
            try {
                cursor = O().query("hits2", new String[]{"hit_id", "hit_time", "hit_string", "hit_url", "hit_app_id"}, null, null, null, null, String.format("%s ASC", "hit_id"), Long.toString(j));
                ArrayList arrayList = new ArrayList();
                if (cursor.moveToFirst()) {
                    do {
                        long j2 = cursor.getLong(0);
                        long j3 = cursor.getLong(1);
                        String string = cursor.getString(2);
                        String string2 = cursor.getString(3);
                        int i = cursor.getInt(4);
                        Map<String, String> X = X(string);
                        if (!TextUtils.isEmpty(string2) && string2.startsWith("http:")) {
                            z2 = false;
                            arrayList.add(new j0(this, X, j3, z2, j2, i));
                        }
                        z2 = true;
                        arrayList.add(new j0(this, X, j3, z2, j2, i));
                    } while (cursor.moveToNext());
                    cursor.close();
                    return arrayList;
                }
                cursor.close();
                return arrayList;
            } catch (SQLiteException e) {
                A("Error loading hits from the database", e);
                throw e;
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    public final int U() {
        f.b();
        N();
        if (!this.o.b(86400000L)) {
            return 0;
        }
        this.o.a();
        C("Deleting stale hits (if any)");
        SQLiteDatabase O = O();
        Objects.requireNonNull((c) this.j.d);
        int delete = O.delete("hits2", "hit_time < ?", new String[]{Long.toString(System.currentTimeMillis() - WidgetChatListAdapterItemGuildWelcomeKt.OLD_GUILD_AGE_THRESHOLD)});
        b("Deleted stale hits, count", Integer.valueOf(delete));
        return delete;
    }

    public final long V() {
        f.b();
        N();
        String str = m;
        Cursor cursor = null;
        try {
            try {
                cursor = O().rawQuery(str, null);
                if (cursor.moveToFirst()) {
                    long j = cursor.getLong(0);
                    cursor.close();
                    return j;
                }
                cursor.close();
                return 0L;
            } catch (SQLiteException e) {
                y("Database error", str, e);
                throw e;
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    public final void W(long j) {
        f.b();
        N();
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(Long.valueOf(j));
        b("Deleting hit, id", Long.valueOf(j));
        S(arrayList);
    }

    public final Map<String, String> X(String str) {
        if (TextUtils.isEmpty(str)) {
            return new HashMap(0);
        }
        try {
            if (!str.startsWith("?")) {
                str = str.length() != 0 ? "?".concat(str) : new String("?");
            }
            return e.a(new URI(str), Constants.ENCODING);
        } catch (URISyntaxException e) {
            A("Error parsing hit parameters", e);
            return new HashMap(0);
        }
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public final void close() {
        try {
            this.n.close();
        } catch (SQLiteException e) {
            A("Sql error closing database", e);
        } catch (IllegalStateException e2) {
            A("Error closing database", e2);
        }
    }

    public final void endTransaction() {
        N();
        O().endTransaction();
    }

    public final void setTransactionSuccessful() {
        N();
        O().setTransactionSuccessful();
    }
}
