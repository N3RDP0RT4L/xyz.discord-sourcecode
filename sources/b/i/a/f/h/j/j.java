package b.i.a.f.h.j;

import android.os.RemoteException;
import b.i.a.f.b.f;
import b.i.a.f.e.n.a;
import java.util.Collections;
import java.util.Objects;
/* loaded from: classes3.dex */
public final class j extends e {
    public final l l = new l(this);
    public k0 m;
    public final a0 n;
    public final y0 o;

    public j(g gVar) {
        super(gVar);
        this.o = new y0(gVar.d);
        this.n = new k(this, gVar);
    }

    @Override // b.i.a.f.h.j.e
    public final void L() {
    }

    public final void O() {
        f.b();
        N();
        try {
            a.b().c(this.j.f1413b, this.l);
        } catch (IllegalArgumentException | IllegalStateException unused) {
        }
        if (this.m != null) {
            this.m = null;
            a s2 = s();
            s2.N();
            f.b();
            r rVar = s2.l;
            f.b();
            rVar.N();
            rVar.C("Service disconnected");
        }
    }

    public final boolean R() {
        f.b();
        N();
        return this.m != null;
    }

    public final boolean S(j0 j0Var) {
        String str;
        Objects.requireNonNull(j0Var, "null reference");
        f.b();
        N();
        k0 k0Var = this.m;
        if (k0Var == null) {
            return false;
        }
        if (j0Var.f) {
            str = z.d();
        } else {
            str = z.e();
        }
        try {
            k0Var.g0(j0Var.a, j0Var.d, str, Collections.emptyList());
            T();
            return true;
        } catch (RemoteException unused) {
            C("Failed to send hits to AnalyticsService");
            return false;
        }
    }

    public final void T() {
        this.o.a();
        this.n.e(e0.f1409x.a.longValue());
    }
}
