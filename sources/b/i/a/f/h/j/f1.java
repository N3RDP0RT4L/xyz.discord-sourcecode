package b.i.a.f.h.j;

import b.i.a.f.b.e;
import java.util.HashMap;
/* loaded from: classes3.dex */
public final class f1 extends e<f1> {
    public String a;

    /* renamed from: b  reason: collision with root package name */
    public String f1412b;
    public String c;
    public String d;

    public final String toString() {
        HashMap hashMap = new HashMap();
        hashMap.put("appName", this.a);
        hashMap.put("appVersion", this.f1412b);
        hashMap.put("appId", this.c);
        hashMap.put("appInstallerId", this.d);
        return e.a(hashMap, 0);
    }
}
