package b.i.a.f.h.j;

import android.content.Context;
import b.c.a.a0.d;
/* loaded from: classes3.dex */
public final class i {
    public final Context a;

    /* renamed from: b  reason: collision with root package name */
    public final Context f1415b;

    public i(Context context) {
        Context applicationContext = context.getApplicationContext();
        d.z(applicationContext, "Application context can't be null");
        this.a = applicationContext;
        this.f1415b = applicationContext;
    }
}
