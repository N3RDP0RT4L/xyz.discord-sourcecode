package b.i.a.f.h.j;

import b.i.a.f.b.f;
import java.util.Objects;
/* loaded from: classes3.dex */
public final class m implements Runnable {
    public final /* synthetic */ k0 j;
    public final /* synthetic */ l k;

    public m(l lVar, k0 k0Var) {
        this.k = lVar;
        this.j = k0Var;
    }

    @Override // java.lang.Runnable
    public final void run() {
        if (!this.k.l.R()) {
            this.k.l.a(3, "Connected to service after a timeout", null, null, null);
            j jVar = this.k.l;
            k0 k0Var = this.j;
            Objects.requireNonNull(jVar);
            f.b();
            jVar.m = k0Var;
            jVar.T();
            a s2 = jVar.s();
            Objects.requireNonNull(s2);
            f.b();
            s2.l.O();
        }
    }
}
