package b.i.a.f.h.j;

import android.content.SharedPreferences;
import androidx.recyclerview.widget.RecyclerView;
import b.c.a.a0.d;
import b.i.a.f.e.o.c;
import java.util.Objects;
import java.util.UUID;
/* loaded from: classes3.dex */
public final class r0 {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ p0 f1421b;

    public r0(p0 p0Var, String str, long j, q0 q0Var) {
        this.f1421b = p0Var;
        d.w(str);
        d.l(j > 0);
        this.a = str;
    }

    public final void a(String str) {
        if (this.f1421b.l.getLong(String.valueOf(this.a).concat(":start"), 0L) == 0) {
            Objects.requireNonNull((c) this.f1421b.j.d);
            long currentTimeMillis = System.currentTimeMillis();
            SharedPreferences.Editor edit = this.f1421b.l.edit();
            edit.remove(b());
            edit.remove(c());
            edit.putLong(String.valueOf(this.a).concat(":start"), currentTimeMillis);
            edit.commit();
        }
        if (str == null) {
            str = "";
        }
        synchronized (this) {
            long j = this.f1421b.l.getLong(b(), 0L);
            if (j <= 0) {
                SharedPreferences.Editor edit2 = this.f1421b.l.edit();
                edit2.putString(c(), str);
                edit2.putLong(b(), 1L);
                edit2.apply();
                return;
            }
            long j2 = j + 1;
            boolean z2 = (UUID.randomUUID().getLeastSignificantBits() & RecyclerView.FOREVER_NS) < RecyclerView.FOREVER_NS / j2;
            SharedPreferences.Editor edit3 = this.f1421b.l.edit();
            if (z2) {
                edit3.putString(c(), str);
            }
            edit3.putLong(b(), j2);
            edit3.apply();
        }
    }

    public final String b() {
        return String.valueOf(this.a).concat(":count");
    }

    public final String c() {
        return String.valueOf(this.a).concat(":value");
    }
}
