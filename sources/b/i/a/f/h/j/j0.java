package b.i.a.f.h.j;

import android.text.TextUtils;
import b.d.b.a.a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
/* loaded from: classes3.dex */
public final class j0 {
    public final Map<String, String> a;
    public final long c;
    public final long d;
    public final int e;
    public final boolean f;

    /* renamed from: b  reason: collision with root package name */
    public final List<?> f1417b = Collections.emptyList();
    public final String g = null;

    public j0(d dVar, Map<String, String> map, long j, boolean z2, long j2, int i) {
        String a;
        String a2;
        Objects.requireNonNull(map, "null reference");
        this.d = j;
        this.f = z2;
        this.c = j2;
        this.e = i;
        TextUtils.isEmpty(null);
        HashMap hashMap = new HashMap();
        Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
        while (true) {
            boolean z3 = false;
            if (!it.hasNext()) {
                break;
            }
            Map.Entry<String, String> next = it.next();
            String key = next.getKey();
            if ((key != null ? key.toString().startsWith("&") : z3) && (a2 = a(dVar, next.getKey())) != null) {
                hashMap.put(a2, b(dVar, next.getValue()));
            }
        }
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String key2 = entry.getKey();
            if (!(key2 == null ? false : key2.toString().startsWith("&")) && (a = a(dVar, entry.getKey())) != null) {
                hashMap.put(a, b(dVar, entry.getValue()));
            }
        }
        if (!TextUtils.isEmpty(this.g)) {
            String str = this.g;
            if (str != null && !hashMap.containsKey("_v")) {
                hashMap.put("_v", str);
            }
            if (this.g.equals("ma4.0.0") || this.g.equals("ma4.0.1")) {
                hashMap.remove("adid");
            }
        }
        this.a = Collections.unmodifiableMap(hashMap);
    }

    public static String a(d dVar, Object obj) {
        if (obj == null) {
            return null;
        }
        String obj2 = obj.toString();
        if (obj2.startsWith("&")) {
            obj2 = obj2.substring(1);
        }
        int length = obj2.length();
        if (length > 256) {
            obj2 = obj2.substring(0, 256);
            dVar.f("Hit param name is too long and will be trimmed", Integer.valueOf(length), obj2);
        }
        if (TextUtils.isEmpty(obj2)) {
            return null;
        }
        return obj2;
    }

    public static String b(d dVar, Object obj) {
        String obj2 = obj == null ? "" : obj.toString();
        int length = obj2.length();
        if (length <= 8192) {
            return obj2;
        }
        String substring = obj2.substring(0, 8192);
        dVar.f("Hit param value is too long and will be trimmed", Integer.valueOf(length), substring);
        return substring;
    }

    public final String toString() {
        StringBuilder R = a.R("ht=");
        R.append(this.d);
        if (this.c != 0) {
            R.append(", dbId=");
            R.append(this.c);
        }
        if (this.e != 0) {
            R.append(", appUID=");
            R.append(this.e);
        }
        ArrayList arrayList = new ArrayList(this.a.keySet());
        Collections.sort(arrayList);
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            Object obj = arrayList.get(i);
            i++;
            String str = (String) obj;
            R.append(", ");
            R.append(str);
            R.append("=");
            R.append(this.a.get(str));
        }
        return R.toString();
    }
}
