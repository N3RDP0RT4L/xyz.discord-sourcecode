package b.i.a.f.h.j;

import b.i.a.f.b.f;
import b.i.a.f.e.o.c;
import java.util.Objects;
/* loaded from: classes3.dex */
public final class a extends e {
    public final r l;

    public a(g gVar, i iVar) {
        super(gVar);
        this.l = new r(gVar, iVar);
    }

    @Override // b.i.a.f.h.j.e
    public final void L() {
        this.l.J();
    }

    public final void O() {
        f.b();
        r rVar = this.l;
        Objects.requireNonNull(rVar);
        f.b();
        Objects.requireNonNull((c) rVar.j.d);
        rVar.u = System.currentTimeMillis();
    }
}
