package b.i.a.f.h.j;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import java.util.Objects;
/* loaded from: classes3.dex */
public class n0 extends BroadcastReceiver {
    public static final String a = n0.class.getName();

    /* renamed from: b  reason: collision with root package name */
    public final g f1419b;
    public boolean c;
    public boolean d;

    public n0(g gVar) {
        Objects.requireNonNull(gVar, "null reference");
        this.f1419b = gVar;
    }

    public final void a() {
        if (this.c) {
            this.f1419b.c().C("Unregistering connectivity change receiver");
            this.c = false;
            this.d = false;
            try {
                this.f1419b.f1413b.unregisterReceiver(this);
            } catch (IllegalArgumentException e) {
                this.f1419b.c().A("Failed to unregister the network broadcast receiver", e);
            }
        }
    }

    public final boolean b() {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.f1419b.f1413b.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                if (activeNetworkInfo.isConnected()) {
                    return true;
                }
            }
        } catch (SecurityException unused) {
        }
        return false;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        this.f1419b.c();
        this.f1419b.e();
        String action = intent.getAction();
        this.f1419b.c().b("NetworkBroadcastReceiver received action", action);
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
            boolean b2 = b();
            if (this.d != b2) {
                this.d = b2;
                a e = this.f1419b.e();
                e.b("Network connectivity status changed", Boolean.valueOf(b2));
                e.q().a(new b(e, b2));
            }
        } else if (!"com.google.analytics.RADIO_POWERED".equals(action)) {
            this.f1419b.c().x("NetworkBroadcastReceiver received unknown action", action);
        } else if (!intent.hasExtra(a)) {
            a e2 = this.f1419b.e();
            e2.C("Radio powered up");
            e2.N();
            Context context2 = e2.j.f1413b;
            if (!s0.a(context2) || !t0.c(context2)) {
                e2.N();
                e2.q().a(new c(e2, null));
                return;
            }
            Intent intent2 = new Intent("com.google.android.gms.analytics.ANALYTICS_DISPATCH");
            intent2.setComponent(new ComponentName(context2, "com.google.android.gms.analytics.AnalyticsService"));
            context2.startService(intent2);
        }
    }
}
