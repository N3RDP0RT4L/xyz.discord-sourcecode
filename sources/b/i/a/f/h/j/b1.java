package b.i.a.f.h.j;

import android.annotation.TargetApi;
import androidx.annotation.Nullable;
import java.lang.reflect.Method;
@TargetApi(24)
/* loaded from: classes3.dex */
public final class b1 {
    @Nullable
    public static final Method a;
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public static final Method f1406b;
    public static volatile d1 c;

    /* JADX WARN: Removed duplicated region for block: B:19:0x003b A[EXC_TOP_SPLITTER, SYNTHETIC] */
    static {
        /*
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            int r1 = android.os.Build.VERSION.SDK_INT
            java.lang.String r2 = "JobSchedulerCompat"
            r3 = 6
            r4 = 0
            r5 = 24
            if (r1 < r5) goto L34
            java.lang.Class<android.app.job.JobScheduler> r1 = android.app.job.JobScheduler.class
            java.lang.String r6 = "scheduleAsPackage"
            r7 = 4
            java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch: java.lang.NoSuchMethodException -> L28
            r8 = 0
            java.lang.Class<android.app.job.JobInfo> r9 = android.app.job.JobInfo.class
            r7[r8] = r9     // Catch: java.lang.NoSuchMethodException -> L28
            r8 = 1
            r7[r8] = r0     // Catch: java.lang.NoSuchMethodException -> L28
            r8 = 2
            java.lang.Class r9 = java.lang.Integer.TYPE     // Catch: java.lang.NoSuchMethodException -> L28
            r7[r8] = r9     // Catch: java.lang.NoSuchMethodException -> L28
            r8 = 3
            r7[r8] = r0     // Catch: java.lang.NoSuchMethodException -> L28
            java.lang.reflect.Method r0 = r1.getDeclaredMethod(r6, r7)     // Catch: java.lang.NoSuchMethodException -> L28
            goto L35
        L28:
            boolean r0 = android.util.Log.isLoggable(r2, r3)
            if (r0 == 0) goto L34
            java.lang.String r0 = "No scheduleAsPackage method available, falling back to schedule"
            android.util.Log.e(r2, r0)
        L34:
            r0 = r4
        L35:
            b.i.a.f.h.j.b1.a = r0
            int r0 = android.os.Build.VERSION.SDK_INT
            if (r0 < r5) goto L50
            java.lang.Class<android.os.UserHandle> r0 = android.os.UserHandle.class
            java.lang.String r1 = "myUserId"
            java.lang.reflect.Method r4 = r0.getDeclaredMethod(r1, r4)     // Catch: java.lang.NoSuchMethodException -> L44
            goto L50
        L44:
            boolean r0 = android.util.Log.isLoggable(r2, r3)
            if (r0 == 0) goto L50
            java.lang.String r0 = "No myUserId method available"
            android.util.Log.e(r2, r0)
        L50:
            b.i.a.f.h.j.b1.f1406b = r4
            b.i.a.f.h.j.d1 r0 = b.i.a.f.h.j.c1.a
            b.i.a.f.h.j.b1.c = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.h.j.b1.<clinit>():void");
    }
}
