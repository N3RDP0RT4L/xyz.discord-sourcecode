package b.i.a.f.h.j;

import b.i.a.f.e.i.e;
import com.discord.stores.StoreGuildScheduledEvents;
import java.util.Objects;
/* loaded from: classes3.dex */
public final class e0 {
    public static f0<Boolean> a = f0.d("analytics.service_client_enabled", true, true);

    /* renamed from: b  reason: collision with root package name */
    public static f0<String> f1407b = f0.c("analytics.log_tag", "GAv4", "GAv4-SVC");
    public static f0<Long> c = f0.b("analytics.local_dispatch_millis", StoreGuildScheduledEvents.FETCH_GUILD_EVENTS_THRESHOLD, 120000);
    public static f0<Long> d = f0.b("analytics.initial_local_dispatch_millis", 5000, 5000);
    public static f0<Long> e = f0.b("analytics.dispatch_alarm_millis", 7200000, 7200000);
    public static f0<Long> f = f0.b("analytics.max_dispatch_alarm_millis", 32400000, 32400000);
    public static f0<Integer> g = f0.a("analytics.max_hits_per_dispatch", 20, 20);
    public static f0<Integer> h = f0.a("analytics.max_hits_per_batch", 20, 20);
    public static f0<String> i = f0.c("analytics.insecure_host", "http://www.google-analytics.com", "http://www.google-analytics.com");
    public static f0<String> j = f0.c("analytics.secure_host", "https://ssl.google-analytics.com", "https://ssl.google-analytics.com");
    public static f0<String> k = f0.c("analytics.simple_endpoint", "/collect", "/collect");
    public static f0<String> l = f0.c("analytics.batching_endpoint", "/batch", "/batch");
    public static f0<Integer> m = f0.a("analytics.max_get_length", 2036, 2036);
    public static f0<String> n = f0.c("analytics.batching_strategy.k", "BATCH_BY_COUNT", "BATCH_BY_COUNT");
    public static f0<String> o = f0.c("analytics.compression_strategy.k", "GZIP", "GZIP");
    public static f0<Integer> p = f0.a("analytics.max_hit_length.k", 8192, 8192);
    public static f0<Integer> q = f0.a("analytics.max_post_length.k", 8192, 8192);
    public static f0<Integer> r = f0.a("analytics.max_batch_post_length", 8192, 8192);

    /* renamed from: s  reason: collision with root package name */
    public static f0<String> f1408s = f0.c("analytics.fallback_responses.k", "404,502", "404,502");
    public static f0<Integer> t = f0.a("analytics.batch_retry_interval.seconds.k", 3600, 3600);
    public static f0<Integer> u = f0.a("analytics.http_connection.connect_timeout_millis", 60000, 60000);
    public static f0<Integer> v = f0.a("analytics.http_connection.read_timeout_millis", 61000, 61000);
    public static f0<Boolean> w = f0.d("analytics.test.disable_receiver", false, false);

    /* renamed from: x  reason: collision with root package name */
    public static f0<Long> f1409x = f0.b("analytics.service_client.idle_disconnect_millis", 10000, 10000);

    /* renamed from: y  reason: collision with root package name */
    public static f0<Long> f1410y = f0.b("analytics.service_client.connect_timeout_millis", 5000, 5000);

    /* renamed from: z  reason: collision with root package name */
    public static f0<Long> f1411z = f0.b("analytics.service_client.reconnect_throttle_millis", StoreGuildScheduledEvents.FETCH_GUILD_EVENTS_THRESHOLD, StoreGuildScheduledEvents.FETCH_GUILD_EVENTS_THRESHOLD);
    public static f0<Long> A = f0.b("analytics.monitoring.sample_period_millis", 86400000, 86400000);
    public static f0<Long> B = f0.b("analytics.initialization_warning_threshold", 5000, 5000);
    public static f0<Boolean> C = f0.d("analytics.gcm_task_service", false, false);

    static {
        f0.d("analytics.service_enabled", false, false);
        f0.b("analytics.max_tokens", 60L, 60L);
        Objects.requireNonNull(new e("analytics.tokens_per_sec", Float.valueOf(0.5f)), "null reference");
        f0.a("analytics.max_stored_hits", 2000, 20000);
        f0.a("analytics.max_stored_hits_per_app", 2000, 2000);
        f0.a("analytics.max_stored_properties_per_app", 100, 100);
        f0.b("analytics.min_local_dispatch_millis", 120000L, 120000L);
        f0.b("analytics.max_local_dispatch_millis", 7200000L, 7200000L);
        f0.a("analytics.max_hits_per_request.k", 20, 20);
        f0.b("analytics.service_monitor_interval", 86400000L, 86400000L);
        f0.b("analytics.campaigns.time_limit", 86400000L, 86400000L);
        f0.c("analytics.first_party_experiment_id", "", "");
        f0.a("analytics.first_party_experiment_variant", 0, 0);
        f0.b("analytics.service_client.second_connect_delay_millis", 5000L, 5000L);
        f0.b("analytics.service_client.unexpected_reconnect_millis", 60000L, 60000L);
    }
}
