package b.i.a.f.h.j;

import android.content.pm.ApplicationInfo;
import android.text.TextUtils;
import b.i.a.f.e.o.g;
import java.util.HashSet;
import java.util.Set;
/* loaded from: classes3.dex */
public final class z {
    public final g a;

    /* renamed from: b  reason: collision with root package name */
    public volatile Boolean f1426b;
    public String c;
    public Set<Integer> d;

    public z(g gVar) {
        this.a = gVar;
    }

    public static long b() {
        return e0.e.a.longValue();
    }

    public static int c() {
        return e0.g.a.intValue();
    }

    public static String d() {
        return e0.j.a;
    }

    public static String e() {
        return e0.i.a;
    }

    public static String f() {
        return e0.k.a;
    }

    public final boolean a() {
        if (this.f1426b == null) {
            synchronized (this) {
                if (this.f1426b == null) {
                    ApplicationInfo applicationInfo = this.a.f1413b.getApplicationInfo();
                    String a = g.a();
                    if (applicationInfo != null) {
                        String str = applicationInfo.processName;
                        this.f1426b = Boolean.valueOf(str != null && str.equals(a));
                    }
                    if ((this.f1426b == null || !this.f1426b.booleanValue()) && "com.google.android.gms.analytics".equals(a)) {
                        this.f1426b = Boolean.TRUE;
                    }
                    if (this.f1426b == null) {
                        this.f1426b = Boolean.TRUE;
                        this.a.c().H("My process not in the list of running processes");
                    }
                }
            }
        }
        return this.f1426b.booleanValue();
    }

    public final Set<Integer> g() {
        String str;
        String str2 = e0.f1408s.a;
        if (this.d == null || (str = this.c) == null || !str.equals(str2)) {
            String[] split = TextUtils.split(str2, ",");
            HashSet hashSet = new HashSet();
            for (String str3 : split) {
                try {
                    hashSet.add(Integer.valueOf(Integer.parseInt(str3)));
                } catch (NumberFormatException unused) {
                }
            }
            this.c = str2;
            this.d = hashSet;
        }
        return this.d;
    }
}
