package b.i.a.f.h.j;
/* loaded from: classes3.dex */
public final class w0 implements d0 {
    public final /* synthetic */ Runnable a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ t0 f1424b;

    public w0(t0 t0Var, Runnable runnable) {
        this.f1424b = t0Var;
        this.a = runnable;
    }

    @Override // b.i.a.f.h.j.d0
    public final void a(Throwable th) {
        this.f1424b.f1423b.post(this.a);
    }
}
