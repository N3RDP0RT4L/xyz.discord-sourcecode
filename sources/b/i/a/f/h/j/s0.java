package b.i.a.f.h.j;

import android.content.Context;
import b.i.a.f.m.a;
import java.util.Objects;
/* loaded from: classes3.dex */
public final class s0 {
    public static Object a = new Object();

    /* renamed from: b  reason: collision with root package name */
    public static a f1422b;
    public static Boolean c;

    public static boolean a(Context context) {
        Objects.requireNonNull(context, "null reference");
        Boolean bool = c;
        if (bool != null) {
            return bool.booleanValue();
        }
        boolean a2 = z0.a(context, "com.google.android.gms.analytics.AnalyticsReceiver", false);
        c = Boolean.valueOf(a2);
        return a2;
    }
}
