package b.i.a.f.h.j;

import android.content.SharedPreferences;
import b.i.a.f.b.f;
import b.i.a.f.e.o.c;
import java.util.Objects;
/* loaded from: classes3.dex */
public final class p0 extends e {
    public SharedPreferences l;
    public long m;
    public long n = -1;
    public final r0 o = new r0(this, "monitoring", e0.A.a.longValue(), null);

    public p0(g gVar) {
        super(gVar);
    }

    @Override // b.i.a.f.h.j.e
    public final void L() {
        this.l = this.j.f1413b.getSharedPreferences("com.google.android.gms.analytics.prefs", 0);
    }

    public final long O() {
        f.b();
        N();
        if (this.n == -1) {
            this.n = this.l.getLong("last_dispatch", 0L);
        }
        return this.n;
    }

    public final void R() {
        f.b();
        N();
        Objects.requireNonNull((c) this.j.d);
        long currentTimeMillis = System.currentTimeMillis();
        SharedPreferences.Editor edit = this.l.edit();
        edit.putLong("last_dispatch", currentTimeMillis);
        edit.apply();
        this.n = currentTimeMillis;
    }
}
