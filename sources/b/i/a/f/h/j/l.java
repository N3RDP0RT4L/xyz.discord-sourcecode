package b.i.a.f.h.j;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import b.c.a.a0.d;
import b.i.a.f.e.n.a;
/* loaded from: classes3.dex */
public final class l implements ServiceConnection {
    public volatile k0 j;
    public volatile boolean k;
    public final /* synthetic */ j l;

    public l(j jVar) {
        this.l = jVar;
    }

    @Override // android.content.ServiceConnection
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        k0 k0Var;
        d.u("AnalyticsServiceConnection.onServiceConnected");
        synchronized (this) {
            if (iBinder == null) {
                this.l.H("Service connected with null binder");
                notifyAll();
                return;
            }
            k0 k0Var2 = null;
            try {
                String interfaceDescriptor = iBinder.getInterfaceDescriptor();
                if ("com.google.android.gms.analytics.internal.IAnalyticsService".equals(interfaceDescriptor)) {
                    IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.analytics.internal.IAnalyticsService");
                    if (queryLocalInterface instanceof k0) {
                        k0Var = (k0) queryLocalInterface;
                    } else {
                        k0Var = new l0(iBinder);
                    }
                    k0Var2 = k0Var;
                    this.l.C("Bound to IAnalyticsService interface");
                } else {
                    this.l.A("Got binder with a wrong descriptor", interfaceDescriptor);
                }
            } catch (RemoteException unused) {
                this.l.H("Service connect failed to get IAnalyticsService");
            }
            if (k0Var2 == null) {
                try {
                    a b2 = a.b();
                    j jVar = this.l;
                    b2.c(jVar.j.f1413b, jVar.l);
                } catch (IllegalArgumentException unused2) {
                }
            } else if (!this.k) {
                this.l.D("onServiceConnected received after the timeout limit");
                this.l.q().a(new m(this, k0Var2));
            } else {
                this.j = k0Var2;
            }
            notifyAll();
        }
    }

    @Override // android.content.ServiceConnection
    public final void onServiceDisconnected(ComponentName componentName) {
        d.u("AnalyticsServiceConnection.onServiceDisconnected");
        this.l.q().a(new n(this, componentName));
    }
}
