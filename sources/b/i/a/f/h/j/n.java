package b.i.a.f.h.j;

import android.content.ComponentName;
import b.i.a.f.b.f;
import java.util.Objects;
/* loaded from: classes3.dex */
public final class n implements Runnable {
    public final /* synthetic */ ComponentName j;
    public final /* synthetic */ l k;

    public n(l lVar, ComponentName componentName) {
        this.k = lVar;
        this.j = componentName;
    }

    @Override // java.lang.Runnable
    public final void run() {
        j jVar = this.k.l;
        ComponentName componentName = this.j;
        Objects.requireNonNull(jVar);
        f.b();
        if (jVar.m != null) {
            jVar.m = null;
            jVar.b("Disconnected from device AnalyticsService", componentName);
            a s2 = jVar.s();
            s2.N();
            f.b();
            r rVar = s2.l;
            f.b();
            rVar.N();
            rVar.C("Service disconnected");
        }
    }
}
