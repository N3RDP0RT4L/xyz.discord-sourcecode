package b.i.a.f.h.j;
/* loaded from: classes3.dex */
public enum w {
    NONE,
    BATCH_BY_SESSION,
    BATCH_BY_TIME,
    BATCH_BY_BRUTE_FORCE,
    BATCH_BY_COUNT,
    BATCH_BY_SIZE
}
