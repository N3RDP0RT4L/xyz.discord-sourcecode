package b.i.a.f.h.j;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.os.Handler;
import androidx.annotation.RequiresPermission;
import b.i.a.f.h.j.x0;
import b.i.a.f.m.a;
import java.util.Objects;
/* loaded from: classes3.dex */
public final class t0<T extends Context & x0> {
    public static Boolean a;

    /* renamed from: b  reason: collision with root package name */
    public final Handler f1423b = new e1();
    public final T c;

    public t0(T t) {
        this.c = t;
    }

    public static boolean c(Context context) {
        Objects.requireNonNull(context, "null reference");
        Boolean bool = a;
        if (bool != null) {
            return bool.booleanValue();
        }
        boolean z2 = false;
        try {
            ServiceInfo serviceInfo = context.getPackageManager().getServiceInfo(new ComponentName(context, "com.google.android.gms.analytics.AnalyticsService"), 0);
            if (serviceInfo != null) {
                if (serviceInfo.enabled) {
                    z2 = true;
                }
            }
        } catch (PackageManager.NameNotFoundException unused) {
        }
        a = Boolean.valueOf(z2);
        return z2;
    }

    @RequiresPermission(allOf = {"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"})
    public final int a(Intent intent, final int i) {
        try {
            synchronized (s0.a) {
                a aVar = s0.f1422b;
                if (aVar != null && aVar.c.isHeld()) {
                    aVar.b();
                }
            }
        } catch (SecurityException unused) {
        }
        final m0 c = g.b(this.c).c();
        if (intent == null) {
            c.D("AnalyticsService started with null intent");
            return 2;
        }
        String action = intent.getAction();
        c.a(2, "Local AnalyticsService called. startId, action", Integer.valueOf(i), action, null);
        if ("com.google.android.gms.analytics.ANALYTICS_DISPATCH".equals(action)) {
            b(new Runnable(this, i, c) { // from class: b.i.a.f.h.j.u0
                public final t0 j;
                public final int k;
                public final m0 l;

                {
                    this.j = this;
                    this.k = i;
                    this.l = c;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    t0 t0Var = this.j;
                    int i2 = this.k;
                    m0 m0Var = this.l;
                    if (t0Var.c.b(i2)) {
                        m0Var.C("Local AnalyticsService processed last dispatch request");
                    }
                }
            });
        }
        return 2;
    }

    public final void b(Runnable runnable) {
        a e = g.b(this.c).e();
        w0 w0Var = new w0(this, runnable);
        e.N();
        e.q().a(new c(e, w0Var));
    }
}
