package b.i.a.f.h.j;

import android.content.Context;
import android.content.SharedPreferences;
import b.i.a.f.b.a;
import b.i.a.f.b.f;
import b.i.a.f.e.o.c;
import java.util.Objects;
/* loaded from: classes3.dex */
public final class u implements Runnable {
    public final /* synthetic */ r j;

    public u(r rVar) {
        this.j = rVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        boolean z2;
        r rVar = this.j;
        rVar.N();
        f.b();
        Context context = rVar.j.f1413b;
        if (!s0.a(context)) {
            rVar.D("AnalyticsReceiver is not registered or is disabled. Register the receiver for reliable dispatching on non-Google Play devices. See http://goo.gl/8Rd3yj for instructions.");
        } else if (!t0.c(context)) {
            rVar.H("AnalyticsService is not registered or is disabled. Analytics service at risk of not starting. See http://goo.gl/8Rd3yj for instructions.");
        }
        Boolean bool = a.a;
        Boolean bool2 = a.a;
        if (bool2 != null) {
            z2 = bool2.booleanValue();
        } else {
            z2 = z0.a(context, "com.google.android.gms.analytics.CampaignTrackingReceiver", true);
            a.a = Boolean.valueOf(z2);
        }
        if (!z2) {
            rVar.D("CampaignTrackingReceiver is not registered, not exported or is disabled. Installation campaign tracking is not possible. See http://goo.gl/8Rd3yj for instructions.");
        }
        p0 u = rVar.u();
        Objects.requireNonNull(u);
        f.b();
        u.N();
        if (u.m == 0) {
            long j = u.l.getLong("first_run", 0L);
            if (j != 0) {
                u.m = j;
            } else {
                Objects.requireNonNull((c) u.j.d);
                long currentTimeMillis = System.currentTimeMillis();
                SharedPreferences.Editor edit = u.l.edit();
                edit.putLong("first_run", currentTimeMillis);
                if (!edit.commit()) {
                    u.D("Failed to commit first run time");
                }
                u.m = currentTimeMillis;
            }
        }
        if (!rVar.b0("android.permission.ACCESS_NETWORK_STATE")) {
            rVar.H("Missing required android.permission.ACCESS_NETWORK_STATE. Google Analytics disabled. See http://goo.gl/8Rd3yj for instructions");
            rVar.N();
            f.b();
            rVar.v = true;
            rVar.p.O();
            rVar.U();
        }
        if (!rVar.b0("android.permission.INTERNET")) {
            rVar.H("Missing required android.permission.INTERNET. Google Analytics disabled. See http://goo.gl/8Rd3yj for instructions");
            rVar.N();
            f.b();
            rVar.v = true;
            rVar.p.O();
            rVar.U();
        }
        if (t0.c(rVar.j.f1413b)) {
            rVar.C("AnalyticsService registered in the app manifest and enabled");
        } else {
            rVar.D("AnalyticsService not registered in the app manifest. Hits might not be delivered reliably. See http://goo.gl/8Rd3yj for instructions.");
        }
        if (!rVar.v && !rVar.m.R()) {
            rVar.S();
        }
        rVar.U();
    }
}
