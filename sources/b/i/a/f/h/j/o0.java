package b.i.a.f.h.j;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.TextUtils;
import b.c.a.a0.d;
import b.i.a.f.b.f;
import b.i.a.f.e.o.c;
import com.adjust.sdk.Constants;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
/* loaded from: classes3.dex */
public final class o0 extends e {
    public static final byte[] l = "\n".getBytes();
    public final String m;
    public final y0 n;

    public o0(g gVar) {
        super(gVar);
        String str;
        String str2 = f.a;
        String str3 = Build.VERSION.RELEASE;
        Locale locale = Locale.getDefault();
        if (locale != null) {
            String language = locale.getLanguage();
            if (!TextUtils.isEmpty(language)) {
                StringBuilder sb = new StringBuilder();
                sb.append(language.toLowerCase(locale));
                if (!TextUtils.isEmpty(locale.getCountry())) {
                    sb.append("-");
                    sb.append(locale.getCountry().toLowerCase(locale));
                }
                str = sb.toString();
                this.m = String.format("%s/%s (Linux; U; Android %s; %s; %s Build/%s)", "GoogleAnalytics", str2, str3, str, Build.MODEL, Build.ID);
                this.n = new y0(gVar.d);
            }
        }
        str = null;
        this.m = String.format("%s/%s (Linux; U; Android %s; %s; %s Build/%s)", "GoogleAnalytics", str2, str3, str, Build.MODEL, Build.ID);
        this.n = new y0(gVar.d);
    }

    public static void S(StringBuilder sb, String str, String str2) throws UnsupportedEncodingException {
        if (sb.length() != 0) {
            sb.append('&');
        }
        sb.append(URLEncoder.encode(str, Constants.ENCODING));
        sb.append('=');
        sb.append(URLEncoder.encode(str2, Constants.ENCODING));
    }

    @Override // b.i.a.f.h.j.e
    public final void L() {
        b("Network initialized. User agent", this.m);
    }

    /* JADX WARN: Removed duplicated region for block: B:37:0x009f  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x0095 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final int O(java.net.URL r10, byte[] r11) {
        /*
            r9 = this;
            java.lang.String r0 = "Error closing http post connection output stream"
            java.lang.String r1 = "null reference"
            java.util.Objects.requireNonNull(r11, r1)
            int r1 = r11.length
            java.lang.Integer r5 = java.lang.Integer.valueOf(r1)
            r3 = 3
            java.lang.String r4 = "POST bytes, url"
            r7 = 0
            r2 = r9
            r6 = r10
            r2.a(r3, r4, r5, r6, r7)
            b.i.a.f.h.j.f0<java.lang.String> r1 = b.i.a.f.h.j.e0.f1407b
            V r1 = r1.a
            java.lang.String r1 = (java.lang.String) r1
            r2 = 2
            boolean r1 = android.util.Log.isLoggable(r1, r2)
            if (r1 == 0) goto L2c
            java.lang.String r1 = new java.lang.String
            r1.<init>(r11)
            java.lang.String r2 = "Post payload\n"
            r9.b(r2, r1)
        L2c:
            r1 = 0
            b.i.a.f.h.j.g r2 = r9.j     // Catch: java.lang.Throwable -> L73 java.io.IOException -> L76
            android.content.Context r2 = r2.f1413b     // Catch: java.lang.Throwable -> L73 java.io.IOException -> L76
            r2.getPackageName()     // Catch: java.lang.Throwable -> L73 java.io.IOException -> L76
            java.net.HttpURLConnection r10 = r9.U(r10)     // Catch: java.lang.Throwable -> L73 java.io.IOException -> L76
            r2 = 1
            r10.setDoOutput(r2)     // Catch: java.io.IOException -> L71 java.lang.Throwable -> L8f
            int r2 = r11.length     // Catch: java.io.IOException -> L71 java.lang.Throwable -> L8f
            r10.setFixedLengthStreamingMode(r2)     // Catch: java.io.IOException -> L71 java.lang.Throwable -> L8f
            r10.connect()     // Catch: java.io.IOException -> L71 java.lang.Throwable -> L8f
            java.io.OutputStream r1 = r10.getOutputStream()     // Catch: java.io.IOException -> L71 java.lang.Throwable -> L8f
            r1.write(r11)     // Catch: java.io.IOException -> L71 java.lang.Throwable -> L8f
            r9.T(r10)     // Catch: java.io.IOException -> L71 java.lang.Throwable -> L8f
            int r11 = r10.getResponseCode()     // Catch: java.io.IOException -> L71 java.lang.Throwable -> L8f
            r2 = 200(0xc8, float:2.8E-43)
            if (r11 != r2) goto L5c
            b.i.a.f.h.j.a r2 = r9.s()     // Catch: java.io.IOException -> L71 java.lang.Throwable -> L8f
            r2.O()     // Catch: java.io.IOException -> L71 java.lang.Throwable -> L8f
        L5c:
            java.lang.String r2 = "POST status"
            java.lang.Integer r3 = java.lang.Integer.valueOf(r11)     // Catch: java.io.IOException -> L71 java.lang.Throwable -> L8f
            r9.d(r2, r3)     // Catch: java.io.IOException -> L71 java.lang.Throwable -> L8f
            r1.close()     // Catch: java.io.IOException -> L69
            goto L6d
        L69:
            r1 = move-exception
            r9.A(r0, r1)
        L6d:
            r10.disconnect()
            return r11
        L71:
            r11 = move-exception
            goto L79
        L73:
            r10 = move-exception
            r11 = r1
            goto L93
        L76:
            r10 = move-exception
            r11 = r10
            r10 = r1
        L79:
            java.lang.String r2 = "Network POST connection error"
            r9.x(r2, r11)     // Catch: java.lang.Throwable -> L8f
            if (r1 == 0) goto L88
            r1.close()     // Catch: java.io.IOException -> L84
            goto L88
        L84:
            r11 = move-exception
            r9.A(r0, r11)
        L88:
            if (r10 == 0) goto L8d
            r10.disconnect()
        L8d:
            r10 = 0
            return r10
        L8f:
            r11 = move-exception
            r8 = r11
            r11 = r10
            r10 = r8
        L93:
            if (r1 == 0) goto L9d
            r1.close()     // Catch: java.io.IOException -> L99
            goto L9d
        L99:
            r1 = move-exception
            r9.A(r0, r1)
        L9d:
            if (r11 == 0) goto La2
            r11.disconnect()
        La2:
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.h.j.o0.O(java.net.URL, byte[]):int");
    }

    public final String R(j0 j0Var, boolean z2) {
        long j;
        String str;
        StringBuilder sb = new StringBuilder();
        try {
            for (Map.Entry<String, String> entry : j0Var.a.entrySet()) {
                String key = entry.getKey();
                if (!"ht".equals(key) && !"qt".equals(key) && !"AppUID".equals(key) && !"z".equals(key) && !"_gmsv".equals(key)) {
                    S(sb, key, entry.getValue());
                }
            }
            S(sb, "ht", String.valueOf(j0Var.d));
            Objects.requireNonNull((c) this.j.d);
            S(sb, "qt", String.valueOf(System.currentTimeMillis() - j0Var.d));
            if (z2) {
                d.w("_s");
                d.o(true, "Short param name required");
                String str2 = j0Var.a.get("_s");
                if (str2 == null) {
                    str2 = "0";
                }
                try {
                    j = Long.parseLong(str2);
                } catch (NumberFormatException unused) {
                    j = 0;
                }
                if (j != 0) {
                    str = String.valueOf(j);
                } else {
                    str = String.valueOf(j0Var.c);
                }
                S(sb, "z", str);
            }
            return sb.toString();
        } catch (UnsupportedEncodingException e) {
            A("Failed to encode name or value", e);
            return null;
        }
    }

    public final void T(HttpURLConnection httpURLConnection) throws IOException {
        InputStream inputStream;
        Throwable th;
        try {
            inputStream = httpURLConnection.getInputStream();
            try {
                do {
                } while (inputStream.read(new byte[1024]) > 0);
                try {
                    inputStream.close();
                } catch (IOException e) {
                    A("Error closing http connection input stream", e);
                }
            } catch (Throwable th2) {
                th = th2;
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e2) {
                        A("Error closing http connection input stream", e2);
                    }
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            inputStream = null;
        }
    }

    public final HttpURLConnection U(URL url) throws IOException {
        URLConnection openConnection = url.openConnection();
        if (openConnection instanceof HttpURLConnection) {
            HttpURLConnection httpURLConnection = (HttpURLConnection) openConnection;
            httpURLConnection.setDefaultUseCaches(false);
            httpURLConnection.setConnectTimeout(e0.u.a.intValue());
            httpURLConnection.setReadTimeout(e0.v.a.intValue());
            httpURLConnection.setInstanceFollowRedirects(false);
            httpURLConnection.setRequestProperty("User-Agent", this.m);
            httpURLConnection.setDoInput(true);
            return httpURLConnection;
        }
        throw new IOException("Failed to obtain http connection");
    }

    /* JADX WARN: Code restructure failed: missing block: B:164:0x038e, code lost:
        if (r0 == 200) goto L193;
     */
    /* JADX WARN: Code restructure failed: missing block: B:192:0x0422, code lost:
        if (O(r5, r4) == 200) goto L193;
     */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:112:0x026e  */
    /* JADX WARN: Removed duplicated region for block: B:119:0x0281  */
    /* JADX WARN: Removed duplicated region for block: B:123:0x028f  */
    /* JADX WARN: Removed duplicated region for block: B:125:0x0299  */
    /* JADX WARN: Removed duplicated region for block: B:130:0x02c3  */
    /* JADX WARN: Removed duplicated region for block: B:190:0x0418  */
    /* JADX WARN: Removed duplicated region for block: B:191:0x041e  */
    /* JADX WARN: Removed duplicated region for block: B:196:0x0429  */
    /* JADX WARN: Removed duplicated region for block: B:199:0x0276 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:210:0x0263 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:223:0x014f A[EDGE_INSN: B:223:0x014f->B:62:0x014f ?: BREAK  , SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:225:0x043c A[EDGE_INSN: B:225:0x043c->B:198:0x043c ?: BREAK  , SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:35:0x0099  */
    /* JADX WARN: Removed duplicated region for block: B:61:0x0144 A[LOOP:0: B:36:0x00c6->B:61:0x0144, LOOP_END] */
    /* JADX WARN: Type inference failed for: r16v0, types: [b.i.a.f.h.j.d, b.i.a.f.h.j.e, b.i.a.f.h.j.o0] */
    /* JADX WARN: Type inference failed for: r1v12 */
    /* JADX WARN: Type inference failed for: r1v13 */
    /* JADX WARN: Type inference failed for: r1v14, types: [java.net.HttpURLConnection] */
    /* JADX WARN: Type inference failed for: r1v15 */
    /* JADX WARN: Type inference failed for: r1v16 */
    /* JADX WARN: Type inference failed for: r1v19 */
    /* JADX WARN: Type inference failed for: r1v33, types: [java.net.HttpURLConnection] */
    /* JADX WARN: Type inference failed for: r1v7, types: [java.io.ByteArrayOutputStream] */
    /* JADX WARN: Type inference failed for: r2v12 */
    /* JADX WARN: Type inference failed for: r2v13 */
    /* JADX WARN: Type inference failed for: r2v16 */
    /* JADX WARN: Type inference failed for: r2v22 */
    /* JADX WARN: Type inference failed for: r2v27 */
    /* JADX WARN: Type inference failed for: r2v30, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r2v62 */
    /* JADX WARN: Type inference failed for: r2v63 */
    /* JADX WARN: Type inference failed for: r2v64 */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.util.List<java.lang.Long> V(java.util.List<b.i.a.f.h.j.j0> r17) {
        /*
            Method dump skipped, instructions count: 1085
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.h.j.o0.V(java.util.List):java.util.List");
    }

    public final boolean W() {
        f.b();
        N();
        NetworkInfo networkInfo = null;
        try {
            networkInfo = ((ConnectivityManager) this.j.f1413b.getSystemService("connectivity")).getActiveNetworkInfo();
        } catch (SecurityException unused) {
        }
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        C("No network connectivity");
        return false;
    }
}
