package b.i.a.f.h.o;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import b.i.a.f.h.o.i;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.safetynet.SafeBrowsingData;
import com.google.android.gms.safetynet.zza;
import com.google.android.gms.safetynet.zzd;
import com.google.android.gms.safetynet.zzf;
import com.google.android.gms.safetynet.zzh;
/* loaded from: classes3.dex */
public class a extends Binder implements IInterface {
    public a(String str) {
        attachInterface(this, str);
    }

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this;
    }

    @Override // android.os.Binder
    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        boolean z2;
        if (i > 16777215) {
            z2 = super.onTransact(i, parcel, parcel2, i2);
        } else {
            parcel.enforceInterface(getInterfaceDescriptor());
            z2 = false;
        }
        if (z2) {
            return true;
        }
        f fVar = (f) this;
        if (i == 1) {
            Status status = (Status) b.a(parcel, Status.CREATOR);
            zza zzaVar = (zza) b.a(parcel, zza.CREATOR);
            throw new UnsupportedOperationException();
        } else if (i == 2) {
            parcel.readString();
            throw new UnsupportedOperationException();
        } else if (i == 3) {
            Status status2 = (Status) b.a(parcel, Status.CREATOR);
            SafeBrowsingData safeBrowsingData = (SafeBrowsingData) b.a(parcel, SafeBrowsingData.CREATOR);
            throw new UnsupportedOperationException();
        } else if (i == 4) {
            Status status3 = (Status) b.a(parcel, Status.CREATOR);
            parcel.readInt();
            throw new UnsupportedOperationException();
        } else if (i == 6) {
            ((k) fVar).a.b(new i.b((Status) b.a(parcel, Status.CREATOR), (zzf) b.a(parcel, zzf.CREATOR)));
            return true;
        } else if (i == 8) {
            Status status4 = (Status) b.a(parcel, Status.CREATOR);
            zzd zzdVar = (zzd) b.a(parcel, zzd.CREATOR);
            throw new UnsupportedOperationException();
        } else if (i == 15) {
            Status status5 = (Status) b.a(parcel, Status.CREATOR);
            zzh zzhVar = (zzh) b.a(parcel, zzh.CREATOR);
            throw new UnsupportedOperationException();
        } else if (i == 10) {
            Status status6 = (Status) b.a(parcel, Status.CREATOR);
            parcel.readInt();
            throw new UnsupportedOperationException();
        } else if (i != 11) {
            return false;
        } else {
            Status status7 = (Status) b.a(parcel, Status.CREATOR);
            throw new UnsupportedOperationException();
        }
    }
}
