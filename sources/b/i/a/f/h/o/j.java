package b.i.a.f.h.o;

import android.os.RemoteException;
import b.i.a.f.e.h.c;
import b.i.a.f.h.o.i;
/* loaded from: classes3.dex */
public final class j extends i.a {
    public final /* synthetic */ String m;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public j(c cVar, String str) {
        super(cVar);
        this.m = str;
    }

    @Override // b.i.a.f.e.h.j.d
    public final /* synthetic */ void k(l lVar) throws RemoteException {
        ((g) lVar.w()).n0(this.l, this.m);
    }
}
