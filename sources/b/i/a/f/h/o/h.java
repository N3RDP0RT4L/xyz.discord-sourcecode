package b.i.a.f.h.o;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
/* loaded from: classes3.dex */
public final class h implements g, IInterface {
    public final IBinder a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1507b = "com.google.android.gms.safetynet.internal.ISafetyNetService";

    public h(IBinder iBinder) {
        this.a = iBinder;
    }

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this.a;
    }

    @Override // b.i.a.f.h.o.g
    public final void n0(e eVar, String str) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.f1507b);
        int i = b.a;
        obtain.writeStrongBinder(eVar == null ? null : (a) eVar);
        obtain.writeString(str);
        Parcel obtain2 = Parcel.obtain();
        try {
            this.a.transact(6, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }
}
