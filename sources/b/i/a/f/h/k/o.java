package b.i.a.f.h.k;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.internal.icing.zzk;
import com.google.android.gms.internal.icing.zzt;
/* compiled from: com.google.firebase:firebase-appindexing@@19.1.0 */
/* loaded from: classes3.dex */
public final class o implements Parcelable.Creator<zzk> {
    @Override // android.os.Parcelable.Creator
    public final zzk createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        String str = null;
        zzt zztVar = null;
        byte[] bArr = null;
        int i = -1;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                str = d.R(parcel, readInt);
            } else if (c == 3) {
                zztVar = (zzt) d.Q(parcel, readInt, zzt.CREATOR);
            } else if (c == 4) {
                i = d.G1(parcel, readInt);
            } else if (c != 5) {
                d.d2(parcel, readInt);
            } else {
                bArr = d.N(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        return new zzk(str, zztVar, i, bArr);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzk[] newArray(int i) {
        return new zzk[i];
    }
}
