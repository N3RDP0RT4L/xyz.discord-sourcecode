package b.i.a.f.h.k;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.firebase.appindexing.internal.zza;
/* compiled from: com.google.firebase:firebase-appindexing@@19.1.0 */
/* loaded from: classes3.dex */
public final class d implements b, IInterface {
    public final IBinder a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1427b = "com.google.android.gms.appdatasearch.internal.ILightweightAppDataSearch";

    public d(IBinder iBinder) {
        this.a = iBinder;
    }

    @Override // b.i.a.f.h.k.b
    public final void S(c cVar, zza[] zzaVarArr) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.f1427b);
        int i = i.a;
        obtain.writeStrongBinder((a) cVar);
        obtain.writeTypedArray(zzaVarArr, 0);
        Parcel obtain2 = Parcel.obtain();
        try {
            this.a.transact(7, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this.a;
    }
}
