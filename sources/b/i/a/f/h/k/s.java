package b.i.a.f.h.k;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.internal.icing.zzm;
import com.google.android.gms.internal.icing.zzt;
import com.google.android.gms.internal.icing.zzu;
/* compiled from: com.google.firebase:firebase-appindexing@@19.1.0 */
/* loaded from: classes3.dex */
public final class s implements Parcelable.Creator<zzt> {
    @Override // android.os.Parcelable.Creator
    public final zzt createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        zzm[] zzmVarArr = null;
        String str4 = null;
        zzu zzuVar = null;
        boolean z2 = false;
        int i = 1;
        boolean z3 = false;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 11) {
                str4 = d.R(parcel, readInt);
            } else if (c != '\f') {
                switch (c) {
                    case 1:
                        str = d.R(parcel, readInt);
                        continue;
                    case 2:
                        str2 = d.R(parcel, readInt);
                        continue;
                    case 3:
                        z2 = d.E1(parcel, readInt);
                        continue;
                    case 4:
                        i = d.G1(parcel, readInt);
                        continue;
                    case 5:
                        z3 = d.E1(parcel, readInt);
                        continue;
                    case 6:
                        str3 = d.R(parcel, readInt);
                        continue;
                    case 7:
                        zzmVarArr = (zzm[]) d.U(parcel, readInt, zzm.CREATOR);
                        continue;
                    default:
                        d.d2(parcel, readInt);
                        continue;
                }
            } else {
                zzuVar = (zzu) d.Q(parcel, readInt, zzu.CREATOR);
            }
        }
        d.f0(parcel, m2);
        return new zzt(str, str2, z2, i, z3, str3, zzmVarArr, str4, zzuVar);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzt[] newArray(int i) {
        return new zzt[i];
    }
}
