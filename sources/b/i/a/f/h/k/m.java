package b.i.a.f.h.k;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.internal.icing.zzh;
import com.google.android.gms.internal.icing.zzk;
/* compiled from: com.google.firebase:firebase-appindexing@@19.1.0 */
/* loaded from: classes3.dex */
public final class m implements Parcelable.Creator<zzh> {
    @Override // android.os.Parcelable.Creator
    public final zzh createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        zzk[] zzkVarArr = null;
        String str = null;
        Account account = null;
        boolean z2 = false;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                zzkVarArr = (zzk[]) d.U(parcel, readInt, zzk.CREATOR);
            } else if (c == 2) {
                str = d.R(parcel, readInt);
            } else if (c == 3) {
                z2 = d.E1(parcel, readInt);
            } else if (c != 4) {
                d.d2(parcel, readInt);
            } else {
                account = (Account) d.Q(parcel, readInt, Account.CREATOR);
            }
        }
        d.f0(parcel, m2);
        return new zzh(zzkVarArr, str, z2, account);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzh[] newArray(int i) {
        return new zzh[i];
    }
}
