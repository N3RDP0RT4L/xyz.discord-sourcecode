package b.i.a.f.h.k;

import b.i.a.f.e.h.a;
/* compiled from: com.google.firebase:firebase-appindexing@@19.1.0 */
/* loaded from: classes3.dex */
public final class k {
    public static final a.g<g> a;

    /* renamed from: b  reason: collision with root package name */
    public static final a.AbstractC0111a<g, a.d.c> f1428b;
    public static final a<a.d.c> c;

    static {
        a.g<g> gVar = new a.g<>();
        a = gVar;
        j jVar = new j();
        f1428b = jVar;
        c = new a<>("AppDataSearch.LIGHTWEIGHT_API", jVar, gVar);
    }
}
