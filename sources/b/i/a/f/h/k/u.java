package b.i.a.f.h.k;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.internal.icing.zzh;
import com.google.android.gms.internal.icing.zzi;
import com.google.android.gms.internal.icing.zzw;
/* compiled from: com.google.firebase:firebase-appindexing@@19.1.0 */
/* loaded from: classes3.dex */
public final class u implements Parcelable.Creator<zzw> {
    @Override // android.os.Parcelable.Creator
    public final zzw createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        zzi zziVar = null;
        long j = 0;
        String str = null;
        zzh zzhVar = null;
        String str2 = null;
        int i = 0;
        boolean z2 = false;
        int i2 = -1;
        int i3 = 0;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    zziVar = (zzi) d.Q(parcel, readInt, zzi.CREATOR);
                    break;
                case 2:
                    j = d.H1(parcel, readInt);
                    break;
                case 3:
                    i = d.G1(parcel, readInt);
                    break;
                case 4:
                    str = d.R(parcel, readInt);
                    break;
                case 5:
                    zzhVar = (zzh) d.Q(parcel, readInt, zzh.CREATOR);
                    break;
                case 6:
                    z2 = d.E1(parcel, readInt);
                    break;
                case 7:
                    i2 = d.G1(parcel, readInt);
                    break;
                case '\b':
                    i3 = d.G1(parcel, readInt);
                    break;
                case '\t':
                    str2 = d.R(parcel, readInt);
                    break;
                default:
                    d.d2(parcel, readInt);
                    break;
            }
        }
        d.f0(parcel, m2);
        return new zzw(zziVar, j, i, str, zzhVar, z2, i2, i3, str2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzw[] newArray(int i) {
        return new zzw[i];
    }
}
