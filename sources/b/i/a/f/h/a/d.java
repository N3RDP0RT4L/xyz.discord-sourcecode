package b.i.a.f.h.a;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
/* loaded from: classes3.dex */
public final class d implements b, IInterface {
    public final IBinder a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1399b = "com.google.android.gms.ads.identifier.internal.IAdvertisingIdService";

    public d(IBinder iBinder) {
        this.a = iBinder;
    }

    @Override // b.i.a.f.h.a.b
    public final boolean D(boolean z2) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.f1399b);
        int i = a.a;
        boolean z3 = true;
        obtain.writeInt(1);
        Parcel c = c(2, obtain);
        if (c.readInt() == 0) {
            z3 = false;
        }
        c.recycle();
        return z3;
    }

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this.a;
    }

    public final Parcel c(int i, Parcel parcel) {
        parcel = Parcel.obtain();
        try {
            this.a.transact(i, parcel, parcel, 0);
            parcel.readException();
            return parcel;
        } catch (RuntimeException e) {
            throw e;
        } finally {
            parcel.recycle();
        }
    }

    @Override // b.i.a.f.h.a.b
    public final String getId() throws RemoteException {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.f1399b);
        Parcel c = c(1, obtain);
        String readString = c.readString();
        c.recycle();
        return readString;
    }
}
