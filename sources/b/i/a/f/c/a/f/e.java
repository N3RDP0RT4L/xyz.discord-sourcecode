package b.i.a.f.c.a.f;

import com.google.android.gms.common.api.Scope;
import java.util.Comparator;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public final class e implements Comparator<Scope> {
    @Override // java.util.Comparator
    public final int compare(Scope scope, Scope scope2) {
        return scope.k.compareTo(scope2.k);
    }
}
