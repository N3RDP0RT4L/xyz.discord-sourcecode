package b.i.a.f.c.a.f.b;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public final class l extends c {
    public final /* synthetic */ i a;

    public l(i iVar) {
        this.a = iVar;
    }

    @Override // b.i.a.f.c.a.f.b.c, b.i.a.f.c.a.f.b.n
    public final void v(Status status) throws RemoteException {
        this.a.b(status);
    }
}
