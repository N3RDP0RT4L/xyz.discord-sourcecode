package b.i.a.f.c.a.f.b;

import android.os.Parcel;
import android.os.RemoteException;
import b.i.a.f.h.c.b;
import b.i.a.f.h.c.d;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public abstract class p extends b implements n {
    public p() {
        super("com.google.android.gms.auth.api.signin.internal.ISignInCallbacks");
    }

    @Override // b.i.a.f.h.c.b
    public final boolean c(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 101:
                GoogleSignInAccount googleSignInAccount = (GoogleSignInAccount) d.a(parcel, GoogleSignInAccount.CREATOR);
                Status status = (Status) d.a(parcel, Status.CREATOR);
                throw new UnsupportedOperationException();
            case 102:
                p((Status) d.a(parcel, Status.CREATOR));
                break;
            case 103:
                v((Status) d.a(parcel, Status.CREATOR));
                break;
            default:
                return false;
        }
        parcel2.writeNoException();
        return true;
    }
}
