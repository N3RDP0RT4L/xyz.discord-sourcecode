package b.i.a.f.c.a.f.b;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public interface n extends IInterface {
    void p(Status status) throws RemoteException;

    void v(Status status) throws RemoteException;
}
