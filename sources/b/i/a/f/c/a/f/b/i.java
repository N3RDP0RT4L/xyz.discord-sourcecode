package b.i.a.f.c.a.f.b;

import android.os.RemoteException;
import b.i.a.f.e.h.c;
import b.i.a.f.e.h.h;
import com.google.android.gms.common.api.Status;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public final class i extends k<Status> {
    public i(c cVar) {
        super(cVar);
    }

    @Override // com.google.android.gms.common.api.internal.BasePendingResult
    public final /* synthetic */ h d(Status status) {
        return status;
    }

    @Override // b.i.a.f.e.h.j.d
    public final void k(f fVar) throws RemoteException {
        f fVar2 = fVar;
        ((o) fVar2.w()).a0(new l(this), fVar2.A);
    }
}
