package b.i.a.f.c.a.f.b;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public final class j extends c {
    public final /* synthetic */ h a;

    public j(h hVar) {
        this.a = hVar;
    }

    @Override // b.i.a.f.c.a.f.b.c, b.i.a.f.c.a.f.b.n
    public final void p(Status status) throws RemoteException {
        this.a.b(status);
    }
}
