package b.i.a.f.c.a.f.b;

import android.content.Context;
import android.util.Log;
import androidx.loader.content.AsyncTaskLoader;
import b.i.a.f.e.h.c;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public final class d extends AsyncTaskLoader<Void> {
    public Semaphore a = new Semaphore(0);

    /* renamed from: b  reason: collision with root package name */
    public Set<c> f1332b;

    public d(Context context, Set<c> set) {
        super(context);
        this.f1332b = set;
    }

    @Override // androidx.loader.content.AsyncTaskLoader
    public final Void loadInBackground() {
        Iterator<c> it = this.f1332b.iterator();
        if (!it.hasNext()) {
            try {
                this.a.tryAcquire(0, 5L, TimeUnit.SECONDS);
                return null;
            } catch (InterruptedException e) {
                Log.i("GACSignInLoader", "Unexpected InterruptedException", e);
                Thread.currentThread().interrupt();
                return null;
            }
        } else {
            Objects.requireNonNull(it.next());
            throw new UnsupportedOperationException();
        }
    }

    @Override // androidx.loader.content.Loader
    public final void onStartLoading() {
        this.a.drainPermits();
        forceLoad();
    }
}
