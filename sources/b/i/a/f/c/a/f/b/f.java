package b.i.a.f.c.a.f.b;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.util.Base64;
import androidx.annotation.Nullable;
import b.i.a.f.e.h.c;
import b.i.a.f.e.k.c;
import b.i.a.f.e.k.d;
import b.i.a.f.h.c.a;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;
import java.util.Arrays;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public final class f extends d<o> {
    public final GoogleSignInOptions A;

    public f(Context context, Looper looper, c cVar, @Nullable GoogleSignInOptions googleSignInOptions, c.a aVar, c.b bVar) {
        super(context, looper, 91, cVar, aVar, bVar);
        GoogleSignInOptions.a aVar2;
        if (googleSignInOptions != null) {
            aVar2 = new GoogleSignInOptions.a(googleSignInOptions);
        } else {
            aVar2 = new GoogleSignInOptions.a();
        }
        byte[] bArr = new byte[16];
        a.a.nextBytes(bArr);
        aVar2.i = Base64.encodeToString(bArr, 11);
        if (!cVar.c.isEmpty()) {
            for (Scope scope : cVar.c) {
                aVar2.a.add(scope);
                aVar2.a.addAll(Arrays.asList(new Scope[0]));
            }
        }
        this.A = aVar2.a();
    }

    @Override // b.i.a.f.e.k.b, b.i.a.f.e.h.a.f
    public final int l() {
        return 12451000;
    }

    @Override // b.i.a.f.e.k.b
    public final /* synthetic */ IInterface r(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.auth.api.signin.internal.ISignInService");
        if (queryLocalInterface instanceof o) {
            return (o) queryLocalInterface;
        }
        return new r(iBinder);
    }

    @Override // b.i.a.f.e.k.b
    public final String x() {
        return "com.google.android.gms.auth.api.signin.internal.ISignInService";
    }

    @Override // b.i.a.f.e.k.b
    public final String y() {
        return "com.google.android.gms.auth.api.signin.service.START";
    }
}
