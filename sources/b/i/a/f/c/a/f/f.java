package b.i.a.f.c.a.f;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.internal.GoogleSignInOptionsExtensionParcelable;
import com.google.android.gms.common.api.Scope;
import java.util.ArrayList;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public final class f implements Parcelable.Creator<GoogleSignInOptions> {
    @Override // android.os.Parcelable.Creator
    public final GoogleSignInOptions createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        ArrayList arrayList = null;
        ArrayList arrayList2 = null;
        Account account = null;
        String str = null;
        String str2 = null;
        String str3 = null;
        int i = 0;
        boolean z2 = false;
        boolean z3 = false;
        boolean z4 = false;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    i = d.G1(parcel, readInt);
                    break;
                case 2:
                    arrayList2 = d.V(parcel, readInt, Scope.CREATOR);
                    break;
                case 3:
                    account = (Account) d.Q(parcel, readInt, Account.CREATOR);
                    break;
                case 4:
                    z2 = d.E1(parcel, readInt);
                    break;
                case 5:
                    z3 = d.E1(parcel, readInt);
                    break;
                case 6:
                    z4 = d.E1(parcel, readInt);
                    break;
                case 7:
                    str = d.R(parcel, readInt);
                    break;
                case '\b':
                    str2 = d.R(parcel, readInt);
                    break;
                case '\t':
                    arrayList = d.V(parcel, readInt, GoogleSignInOptionsExtensionParcelable.CREATOR);
                    break;
                case '\n':
                    str3 = d.R(parcel, readInt);
                    break;
                default:
                    d.d2(parcel, readInt);
                    break;
            }
        }
        d.f0(parcel, m2);
        return new GoogleSignInOptions(i, arrayList2, account, z2, z3, z4, str, str2, GoogleSignInOptions.y0(arrayList), str3);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ GoogleSignInOptions[] newArray(int i) {
        return new GoogleSignInOptions[i];
    }
}
