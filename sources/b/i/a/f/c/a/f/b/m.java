package b.i.a.f.c.a.f.b;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public final class m {
    @Nullable
    public static m a;

    /* renamed from: b  reason: collision with root package name */
    public a f1333b;

    public m(Context context) {
        a a2 = a.a(context);
        this.f1333b = a2;
        a2.b();
        this.f1333b.c();
    }

    public static synchronized m b(@NonNull Context context) {
        m mVar;
        synchronized (m.class) {
            Context applicationContext = context.getApplicationContext();
            synchronized (m.class) {
                mVar = a;
                if (mVar == null) {
                    mVar = new m(applicationContext);
                    a = mVar;
                }
            }
            return mVar;
        }
        return mVar;
    }

    public final synchronized void a() {
        a aVar = this.f1333b;
        aVar.c.lock();
        aVar.d.edit().clear().apply();
        aVar.c.unlock();
    }
}
