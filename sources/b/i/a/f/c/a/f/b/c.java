package b.i.a.f.c.a.f.b;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public class c extends p {
    @Override // b.i.a.f.c.a.f.b.n
    public void p(Status status) throws RemoteException {
        throw new UnsupportedOperationException();
    }

    @Override // b.i.a.f.c.a.f.b.n
    public void v(Status status) throws RemoteException {
        throw new UnsupportedOperationException();
    }
}
