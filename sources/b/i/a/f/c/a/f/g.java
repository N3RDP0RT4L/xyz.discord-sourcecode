package b.i.a.f.c.a.f;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.SignInAccount;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public final class g implements Parcelable.Creator<SignInAccount> {
    @Override // android.os.Parcelable.Creator
    public final SignInAccount createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        String str = "";
        GoogleSignInAccount googleSignInAccount = null;
        String str2 = str;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 4) {
                str = d.R(parcel, readInt);
            } else if (c == 7) {
                googleSignInAccount = (GoogleSignInAccount) d.Q(parcel, readInt, GoogleSignInAccount.CREATOR);
            } else if (c != '\b') {
                d.d2(parcel, readInt);
            } else {
                str2 = d.R(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        return new SignInAccount(str, googleSignInAccount, str2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ SignInAccount[] newArray(int i) {
        return new SignInAccount[i];
    }
}
