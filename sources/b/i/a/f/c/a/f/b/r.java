package b.i.a.f.c.a.f.b;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import b.i.a.f.h.c.c;
import b.i.a.f.h.c.d;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public final class r extends c implements o {
    public r(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.auth.api.signin.internal.ISignInService");
    }

    @Override // b.i.a.f.c.a.f.b.o
    public final void a0(n nVar, GoogleSignInOptions googleSignInOptions) throws RemoteException {
        Parcel c = c();
        d.b(c, nVar);
        d.c(c, googleSignInOptions);
        g(103, c);
    }

    @Override // b.i.a.f.c.a.f.b.o
    public final void k0(n nVar, GoogleSignInOptions googleSignInOptions) throws RemoteException {
        Parcel c = c();
        d.b(c, nVar);
        d.c(c, googleSignInOptions);
        g(102, c);
    }
}
