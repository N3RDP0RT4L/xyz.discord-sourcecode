package b.i.a.f.c.a.f.b;

import android.content.Context;
import android.os.Binder;
import android.os.IInterface;
import android.os.Parcel;
import b.c.a.a0.d;
import b.i.a.f.c.a.f.a;
import b.i.a.f.e.h.c;
import b.i.a.f.e.h.e;
import b.i.a.f.e.h.j.o;
import b.i.a.f.e.k.k;
import b.i.a.f.e.o.f;
import b.i.a.f.h.c.b;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public final class q extends b implements IInterface {
    public final Context a;

    public q(Context context) {
        super("com.google.android.gms.auth.api.signin.internal.IRevocationService");
        this.a = context;
    }

    @Override // b.i.a.f.h.c.b
    public final boolean c(int i, Parcel parcel, Parcel parcel2, int i2) {
        BasePendingResult basePendingResult;
        BasePendingResult basePendingResult2;
        if (i == 1) {
            g();
            a a = a.a(this.a);
            GoogleSignInAccount b2 = a.b();
            GoogleSignInOptions googleSignInOptions = GoogleSignInOptions.n;
            if (b2 != null) {
                googleSignInOptions = a.c();
            }
            Context context = this.a;
            Objects.requireNonNull(googleSignInOptions, "null reference");
            a aVar = new a(context, googleSignInOptions);
            if (b2 != null) {
                c cVar = aVar.g;
                Context context2 = aVar.a;
                boolean z2 = aVar.f() == 3;
                g.a.a("Revoking access", new Object[0]);
                String g = a.a(context2).g("refreshToken");
                g.a(context2);
                if (z2) {
                    b.i.a.f.e.l.a aVar2 = e.j;
                    if (g == null) {
                        Status status = new Status(4, null);
                        d.z(status, "Result must not be null");
                        d.o(!status.w0(), "Status code must not be SUCCESS");
                        basePendingResult2 = new e(null, status);
                        basePendingResult2.b(status);
                    } else {
                        e eVar = new e(g);
                        new Thread(eVar).start();
                        basePendingResult2 = eVar.l;
                    }
                } else {
                    basePendingResult2 = cVar.b(new i(cVar));
                }
                k.a(basePendingResult2);
                return true;
            }
            c cVar2 = aVar.g;
            Context context3 = aVar.a;
            boolean z3 = aVar.f() == 3;
            g.a.a("Signing out", new Object[0]);
            g.a(context3);
            if (z3) {
                Status status2 = Status.j;
                d.z(status2, "Result must not be null");
                basePendingResult = new o(cVar2);
                basePendingResult.b(status2);
            } else {
                basePendingResult = cVar2.b(new h(cVar2));
            }
            k.a(basePendingResult);
            return true;
        } else if (i != 2) {
            return false;
        } else {
            g();
            m.b(this.a).a();
            return true;
        }
    }

    public final void g() {
        if (!f.D0(this.a, Binder.getCallingUid())) {
            int callingUid = Binder.getCallingUid();
            StringBuilder sb = new StringBuilder(52);
            sb.append("Calling UID ");
            sb.append(callingUid);
            sb.append(" is not Google Play services.");
            throw new SecurityException(sb.toString());
        }
    }
}
