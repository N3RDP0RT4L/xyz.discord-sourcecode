package b.i.a.f.c.a.f;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Scope;
import java.util.ArrayList;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public final class d implements Parcelable.Creator<GoogleSignInAccount> {
    @Override // android.os.Parcelable.Creator
    public final GoogleSignInAccount createFromParcel(Parcel parcel) {
        int m2 = b.c.a.a0.d.m2(parcel);
        String str = null;
        long j = 0;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        Uri uri = null;
        String str5 = null;
        String str6 = null;
        ArrayList arrayList = null;
        String str7 = null;
        String str8 = null;
        int i = 0;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    i = b.c.a.a0.d.G1(parcel, readInt);
                    break;
                case 2:
                    str = b.c.a.a0.d.R(parcel, readInt);
                    break;
                case 3:
                    str2 = b.c.a.a0.d.R(parcel, readInt);
                    break;
                case 4:
                    str3 = b.c.a.a0.d.R(parcel, readInt);
                    break;
                case 5:
                    str4 = b.c.a.a0.d.R(parcel, readInt);
                    break;
                case 6:
                    uri = (Uri) b.c.a.a0.d.Q(parcel, readInt, Uri.CREATOR);
                    break;
                case 7:
                    str5 = b.c.a.a0.d.R(parcel, readInt);
                    break;
                case '\b':
                    j = b.c.a.a0.d.H1(parcel, readInt);
                    break;
                case '\t':
                    str6 = b.c.a.a0.d.R(parcel, readInt);
                    break;
                case '\n':
                    arrayList = b.c.a.a0.d.V(parcel, readInt, Scope.CREATOR);
                    break;
                case 11:
                    str7 = b.c.a.a0.d.R(parcel, readInt);
                    break;
                case '\f':
                    str8 = b.c.a.a0.d.R(parcel, readInt);
                    break;
                default:
                    b.c.a.a0.d.d2(parcel, readInt);
                    break;
            }
        }
        b.c.a.a0.d.f0(parcel, m2);
        return new GoogleSignInAccount(i, str, str2, str3, str4, uri, str5, j, str6, arrayList, str7, str8);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ GoogleSignInAccount[] newArray(int i) {
        return new GoogleSignInAccount[i];
    }
}
