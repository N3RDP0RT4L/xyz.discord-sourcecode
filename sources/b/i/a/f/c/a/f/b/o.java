package b.i.a.f.c.a.f.b;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public interface o extends IInterface {
    void a0(n nVar, GoogleSignInOptions googleSignInOptions) throws RemoteException;

    void k0(n nVar, GoogleSignInOptions googleSignInOptions) throws RemoteException;
}
