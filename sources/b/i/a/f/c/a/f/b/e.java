package b.i.a.f.c.a.f.b;

import androidx.browser.trusted.sharing.ShareTarget;
import b.c.a.a0.d;
import b.i.a.f.e.h.j.o;
import b.i.a.f.e.l.a;
import com.google.android.gms.common.api.Status;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public final class e implements Runnable {
    public static final a j = new a("RevokeAccessOperation", new String[0]);
    public final String k;
    public final o l = new o(null);

    public e(String str) {
        d.w(str);
        this.k = str;
    }

    @Override // java.lang.Runnable
    public final void run() {
        Status status = Status.l;
        try {
            String valueOf = String.valueOf(this.k);
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(valueOf.length() != 0 ? "https://accounts.google.com/o/oauth2/revoke?token=".concat(valueOf) : new String("https://accounts.google.com/o/oauth2/revoke?token=")).openConnection();
            httpURLConnection.setRequestProperty("Content-Type", ShareTarget.ENCODING_TYPE_URL_ENCODED);
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode == 200) {
                status = Status.j;
            } else {
                j.b("Unable to revoke access!", new Object[0]);
            }
            a aVar = j;
            StringBuilder sb = new StringBuilder(26);
            sb.append("Response Code: ");
            sb.append(responseCode);
            aVar.a(sb.toString(), new Object[0]);
        } catch (IOException e) {
            a aVar2 = j;
            String valueOf2 = String.valueOf(e.toString());
            aVar2.b(valueOf2.length() != 0 ? "IOException when revoking access: ".concat(valueOf2) : new String("IOException when revoking access: "), new Object[0]);
        } catch (Exception e2) {
            a aVar3 = j;
            String valueOf3 = String.valueOf(e2.toString());
            aVar3.b(valueOf3.length() != 0 ? "Exception when revoking access: ".concat(valueOf3) : new String("Exception when revoking access: "), new Object[0]);
        }
        this.l.b(status);
    }
}
