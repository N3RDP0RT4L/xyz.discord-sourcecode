package b.i.a.f.c.a.f.b;

import android.content.Context;
import android.os.Handler;
import b.i.a.f.e.h.c;
import b.i.a.f.e.l.a;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public final class g {
    public static a a = new a("GoogleSignInCommon", new String[0]);

    public static void a(Context context) {
        m.b(context).a();
        Set<c> set = c.a;
        synchronized (set) {
        }
        Iterator<c> it = set.iterator();
        if (!it.hasNext()) {
            synchronized (b.i.a.f.e.h.j.g.l) {
                b.i.a.f.e.h.j.g gVar = b.i.a.f.e.h.j.g.m;
                if (gVar != null) {
                    gVar.f1354s.incrementAndGet();
                    Handler handler = gVar.f1355x;
                    handler.sendMessageAtFrontOfQueue(handler.obtainMessage(10));
                }
            }
            return;
        }
        Objects.requireNonNull(it.next());
        throw new UnsupportedOperationException();
    }
}
