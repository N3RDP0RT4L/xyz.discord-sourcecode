package b.i.a.f.c.a.e;

import android.content.Context;
import androidx.annotation.NonNull;
import b.i.a.f.e.h.a;
import b.i.a.f.e.h.b;
import b.i.a.f.h.b.i;
/* compiled from: com.google.android.gms:play-services-auth-api-phone@@17.4.0 */
/* loaded from: classes3.dex */
public abstract class a extends b<a.d.c> {
    public static final a.g<i> j;
    public static final a.AbstractC0111a<i, a.d.c> k;
    public static final b.i.a.f.e.h.a<a.d.c> l;

    static {
        a.g<i> gVar = new a.g<>();
        j = gVar;
        b bVar = new b();
        k = bVar;
        l = new b.i.a.f.e.h.a<>("SmsRetriever.API", bVar, gVar);
    }

    public a(@NonNull Context context) {
        super(context, l, (a.d) null, b.a.a);
    }
}
