package b.i.a.f.c.a.d;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.auth.api.credentials.CredentialPickerConfig;
import com.google.android.gms.auth.api.credentials.CredentialRequest;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public final class g implements Parcelable.Creator<CredentialRequest> {
    @Override // android.os.Parcelable.Creator
    public final CredentialRequest createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        String[] strArr = null;
        CredentialPickerConfig credentialPickerConfig = null;
        CredentialPickerConfig credentialPickerConfig2 = null;
        String str = null;
        String str2 = null;
        int i = 0;
        boolean z2 = false;
        boolean z3 = false;
        boolean z4 = false;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c != 1000) {
                switch (c) {
                    case 1:
                        z2 = d.E1(parcel, readInt);
                        continue;
                    case 2:
                        strArr = d.S(parcel, readInt);
                        continue;
                    case 3:
                        credentialPickerConfig = (CredentialPickerConfig) d.Q(parcel, readInt, CredentialPickerConfig.CREATOR);
                        continue;
                    case 4:
                        credentialPickerConfig2 = (CredentialPickerConfig) d.Q(parcel, readInt, CredentialPickerConfig.CREATOR);
                        continue;
                    case 5:
                        z3 = d.E1(parcel, readInt);
                        continue;
                    case 6:
                        str = d.R(parcel, readInt);
                        continue;
                    case 7:
                        str2 = d.R(parcel, readInt);
                        continue;
                    case '\b':
                        z4 = d.E1(parcel, readInt);
                        continue;
                    default:
                        d.d2(parcel, readInt);
                        continue;
                }
            } else {
                i = d.G1(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        return new CredentialRequest(i, z2, strArr, credentialPickerConfig, credentialPickerConfig2, z3, str, str2, z4);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ CredentialRequest[] newArray(int i) {
        return new CredentialRequest[i];
    }
}
