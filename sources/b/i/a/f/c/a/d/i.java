package b.i.a.f.c.a.d;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.auth.api.credentials.IdToken;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public final class i implements Parcelable.Creator<IdToken> {
    @Override // android.os.Parcelable.Creator
    public final IdToken createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        String str = null;
        String str2 = null;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                str = d.R(parcel, readInt);
            } else if (c != 2) {
                d.d2(parcel, readInt);
            } else {
                str2 = d.R(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        return new IdToken(str, str2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ IdToken[] newArray(int i) {
        return new IdToken[i];
    }
}
