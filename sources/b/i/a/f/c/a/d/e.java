package b.i.a.f.c.a.d;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.IdToken;
import java.util.ArrayList;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public final class e implements Parcelable.Creator<Credential> {
    @Override // android.os.Parcelable.Creator
    public final Credential createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        String str = null;
        String str2 = null;
        Uri uri = null;
        ArrayList arrayList = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    str = d.R(parcel, readInt);
                    break;
                case 2:
                    str2 = d.R(parcel, readInt);
                    break;
                case 3:
                    uri = (Uri) d.Q(parcel, readInt, Uri.CREATOR);
                    break;
                case 4:
                    arrayList = d.V(parcel, readInt, IdToken.CREATOR);
                    break;
                case 5:
                    str3 = d.R(parcel, readInt);
                    break;
                case 6:
                    str4 = d.R(parcel, readInt);
                    break;
                case 7:
                case '\b':
                default:
                    d.d2(parcel, readInt);
                    break;
                case '\t':
                    str5 = d.R(parcel, readInt);
                    break;
                case '\n':
                    str6 = d.R(parcel, readInt);
                    break;
            }
        }
        d.f0(parcel, m2);
        return new Credential(str, str2, uri, arrayList, str3, str4, str5, str6);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Credential[] newArray(int i) {
        return new Credential[i];
    }
}
