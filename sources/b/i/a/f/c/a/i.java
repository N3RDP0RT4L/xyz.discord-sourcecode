package b.i.a.f.c.a;

import android.content.Context;
import android.os.Looper;
import androidx.annotation.Nullable;
import b.i.a.f.c.a.f.b.f;
import b.i.a.f.e.h.a;
import b.i.a.f.e.h.c;
import b.i.a.f.e.k.c;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public final class i extends a.AbstractC0111a<f, GoogleSignInOptions> {
    @Override // b.i.a.f.e.h.a.AbstractC0111a
    public final /* synthetic */ f a(Context context, Looper looper, c cVar, @Nullable GoogleSignInOptions googleSignInOptions, c.a aVar, c.b bVar) {
        return new f(context, looper, cVar, googleSignInOptions, aVar, bVar);
    }
}
