package b.i.a.f.c.a;

import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.f.c.a.d.c;
import b.i.a.f.c.a.f.b.f;
import b.i.a.f.e.h.a;
import b.i.a.f.h.c.h;
import b.i.a.f.h.c.o;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import java.util.Arrays;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public final class a {
    public static final a.g<o> a;

    /* renamed from: b  reason: collision with root package name */
    public static final a.g<f> f1328b;
    public static final a.AbstractC0111a<o, C0109a> c;
    public static final a.AbstractC0111a<f, GoogleSignInOptions> d;
    public static final b.i.a.f.e.h.a<C0109a> e;
    public static final b.i.a.f.e.h.a<GoogleSignInOptions> f;
    public static final c g = new h();

    /* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
    @Deprecated
    /* renamed from: b.i.a.f.c.a.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static class C0109a implements a.d {
        public static final C0109a j = new C0109a(new C0110a());
        public final String k;
        public final boolean l;
        @Nullable
        public final String m;

        public C0109a(C0110a aVar) {
            this.k = aVar.a;
            this.l = aVar.f1329b.booleanValue();
            this.m = aVar.c;
        }

        public boolean equals(@Nullable Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof C0109a)) {
                return false;
            }
            C0109a aVar = (C0109a) obj;
            return d.h0(this.k, aVar.k) && this.l == aVar.l && d.h0(this.m, aVar.m);
        }

        public int hashCode() {
            return Arrays.hashCode(new Object[]{this.k, Boolean.valueOf(this.l), this.m});
        }

        /* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
        @Deprecated
        /* renamed from: b.i.a.f.c.a.a$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static class C0110a {
            public String a;

            /* renamed from: b  reason: collision with root package name */
            public Boolean f1329b;
            @Nullable
            public String c;

            public C0110a() {
                this.f1329b = Boolean.FALSE;
            }

            public C0110a(C0109a aVar) {
                this.f1329b = Boolean.FALSE;
                this.a = aVar.k;
                this.f1329b = Boolean.valueOf(aVar.l);
                this.c = aVar.m;
            }
        }
    }

    static {
        a.g<o> gVar = new a.g<>();
        a = gVar;
        a.g<f> gVar2 = new a.g<>();
        f1328b = gVar2;
        h hVar = new h();
        c = hVar;
        i iVar = new i();
        d = iVar;
        b.i.a.f.e.h.a<c> aVar = b.c;
        e = new b.i.a.f.e.h.a<>("Auth.CREDENTIALS_API", hVar, gVar);
        f = new b.i.a.f.e.h.a<>("Auth.GOOGLE_SIGN_IN_API", iVar, gVar2);
        b.i.a.f.h.d.d dVar = b.d;
    }
}
