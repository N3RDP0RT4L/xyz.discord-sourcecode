package b.i.a.f.m;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.PowerManager;
import android.os.WorkSource;
import android.util.Log;
import androidx.annotation.NonNull;
import b.c.a.a0.d;
import b.i.a.f.e.m.a;
import b.i.a.f.e.o.h;
import b.i.a.f.e.o.i;
import b.i.a.f.e.p.b;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicInteger;
/* loaded from: classes3.dex */
public class a {
    public static ScheduledExecutorService a;

    /* renamed from: b  reason: collision with root package name */
    public final Object f1587b;
    public final PowerManager.WakeLock c;
    public WorkSource d;
    public final int e;
    public final String f;
    public final Context g;
    public boolean h;
    public final Map<String, Integer[]> i;
    public int j;
    public AtomicInteger k;

    public a(@NonNull Context context, int i, @NonNull String str) {
        a.AbstractC0114a aVar;
        WorkSource workSource = null;
        String packageName = context == null ? null : context.getPackageName();
        this.f1587b = this;
        this.h = true;
        this.i = new HashMap();
        Collections.synchronizedSet(new HashSet());
        this.k = new AtomicInteger(0);
        d.z(context, "WakeLock: context must not be null");
        d.v(str, "WakeLock: wakeLockName must not be empty");
        this.e = i;
        this.g = context.getApplicationContext();
        if (!"com.google.android.gms".equals(context.getPackageName())) {
            this.f = str.length() != 0 ? "*gcore*:".concat(str) : new String("*gcore*:");
        } else {
            this.f = str;
        }
        this.c = ((PowerManager) context.getSystemService("power")).newWakeLock(i, str);
        if (i.a(context)) {
            packageName = h.a(packageName) ? context.getPackageName() : packageName;
            if (!(context.getPackageManager() == null || packageName == null)) {
                try {
                    ApplicationInfo applicationInfo = b.a(context).a.getPackageManager().getApplicationInfo(packageName, 0);
                    if (applicationInfo == null) {
                        Log.e("WorkSourceUtil", packageName.length() != 0 ? "Could not get applicationInfo from package: ".concat(packageName) : new String("Could not get applicationInfo from package: "));
                    } else {
                        int i2 = applicationInfo.uid;
                        workSource = new WorkSource();
                        Method method = i.f1395b;
                        if (method != null) {
                            try {
                                method.invoke(workSource, Integer.valueOf(i2), packageName);
                            } catch (Exception e) {
                                Log.wtf("WorkSourceUtil", "Unable to assign blame through WorkSource", e);
                            }
                        } else {
                            Method method2 = i.a;
                            if (method2 != null) {
                                try {
                                    method2.invoke(workSource, Integer.valueOf(i2));
                                } catch (Exception e2) {
                                    Log.wtf("WorkSourceUtil", "Unable to assign blame through WorkSource", e2);
                                }
                            }
                        }
                    }
                } catch (PackageManager.NameNotFoundException unused) {
                    Log.e("WorkSourceUtil", packageName.length() != 0 ? "Could not find package: ".concat(packageName) : new String("Could not find package: "));
                }
            }
            this.d = workSource;
            if (workSource != null && i.a(this.g)) {
                WorkSource workSource2 = this.d;
                if (workSource2 != null) {
                    workSource2.add(workSource);
                } else {
                    this.d = workSource;
                }
                try {
                    this.c.setWorkSource(this.d);
                } catch (ArrayIndexOutOfBoundsException | IllegalArgumentException e3) {
                    Log.wtf("WakeLock", e3.toString());
                }
            }
        }
        if (a == null) {
            synchronized (b.i.a.f.e.m.a.class) {
                if (b.i.a.f.e.m.a.a == null) {
                    b.i.a.f.e.m.a.a = new b.i.a.f.e.m.b();
                }
                aVar = b.i.a.f.e.m.a.a;
            }
            Objects.requireNonNull((b.i.a.f.e.m.b) aVar);
            a = Executors.unconfigurableScheduledExecutorService(Executors.newScheduledThreadPool(1));
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:20:0x0057, code lost:
        if (r3 == false) goto L21;
     */
    /* JADX WARN: Code restructure failed: missing block: B:24:0x005f, code lost:
        if (r7.j == 0) goto L25;
     */
    /* JADX WARN: Code restructure failed: missing block: B:25:0x0061, code lost:
        b.c.a.a0.d.v0(r7.c, null);
        c();
        r7.j++;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void a(long r8) {
        /*
            r7 = this;
            java.util.concurrent.atomic.AtomicInteger r0 = r7.k
            r0.incrementAndGet()
            boolean r0 = r7.h
            r1 = 0
            if (r0 == 0) goto Ld
            android.text.TextUtils.isEmpty(r1)
        Ld:
            java.lang.Object r0 = r7.f1587b
            monitor-enter(r0)
            java.util.Map<java.lang.String, java.lang.Integer[]> r2 = r7.i     // Catch: java.lang.Throwable -> L87
            boolean r2 = r2.isEmpty()     // Catch: java.lang.Throwable -> L87
            r3 = 0
            if (r2 == 0) goto L1d
            int r2 = r7.j     // Catch: java.lang.Throwable -> L87
            if (r2 <= 0) goto L2c
        L1d:
            android.os.PowerManager$WakeLock r2 = r7.c     // Catch: java.lang.Throwable -> L87
            boolean r2 = r2.isHeld()     // Catch: java.lang.Throwable -> L87
            if (r2 != 0) goto L2c
            java.util.Map<java.lang.String, java.lang.Integer[]> r2 = r7.i     // Catch: java.lang.Throwable -> L87
            r2.clear()     // Catch: java.lang.Throwable -> L87
            r7.j = r3     // Catch: java.lang.Throwable -> L87
        L2c:
            boolean r2 = r7.h     // Catch: java.lang.Throwable -> L87
            r4 = 1
            if (r2 == 0) goto L59
            java.util.Map<java.lang.String, java.lang.Integer[]> r2 = r7.i     // Catch: java.lang.Throwable -> L87
            java.lang.Object r2 = r2.get(r1)     // Catch: java.lang.Throwable -> L87
            java.lang.Integer[] r2 = (java.lang.Integer[]) r2     // Catch: java.lang.Throwable -> L87
            if (r2 != 0) goto L4a
            java.util.Map<java.lang.String, java.lang.Integer[]> r2 = r7.i     // Catch: java.lang.Throwable -> L87
            java.lang.Integer[] r5 = new java.lang.Integer[r4]     // Catch: java.lang.Throwable -> L87
            java.lang.Integer r6 = java.lang.Integer.valueOf(r4)     // Catch: java.lang.Throwable -> L87
            r5[r3] = r6     // Catch: java.lang.Throwable -> L87
            r2.put(r1, r5)     // Catch: java.lang.Throwable -> L87
            r3 = 1
            goto L57
        L4a:
            r5 = r2[r3]     // Catch: java.lang.Throwable -> L87
            int r5 = r5.intValue()     // Catch: java.lang.Throwable -> L87
            int r5 = r5 + r4
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch: java.lang.Throwable -> L87
            r2[r3] = r5     // Catch: java.lang.Throwable -> L87
        L57:
            if (r3 != 0) goto L61
        L59:
            boolean r2 = r7.h     // Catch: java.lang.Throwable -> L87
            if (r2 != 0) goto L6e
            int r2 = r7.j     // Catch: java.lang.Throwable -> L87
            if (r2 != 0) goto L6e
        L61:
            android.os.PowerManager$WakeLock r2 = r7.c     // Catch: java.lang.Throwable -> L87
            b.c.a.a0.d.v0(r2, r1)     // Catch: java.lang.Throwable -> L87
            r7.c()     // Catch: java.lang.Throwable -> L87
            int r1 = r7.j     // Catch: java.lang.Throwable -> L87
            int r1 = r1 + r4
            r7.j = r1     // Catch: java.lang.Throwable -> L87
        L6e:
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L87
            android.os.PowerManager$WakeLock r0 = r7.c
            r0.acquire()
            r0 = 0
            int r2 = (r8 > r0 ? 1 : (r8 == r0 ? 0 : -1))
            if (r2 <= 0) goto L86
            java.util.concurrent.ScheduledExecutorService r0 = b.i.a.f.m.a.a
            b.i.a.f.m.b r1 = new b.i.a.f.m.b
            r1.<init>(r7)
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.MILLISECONDS
            r0.schedule(r1, r8, r2)
        L86:
            return
        L87:
            r8 = move-exception
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L87
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.m.a.a(long):void");
    }

    /* JADX WARN: Code restructure failed: missing block: B:19:0x0051, code lost:
        if (r4 == false) goto L20;
     */
    /* JADX WARN: Code restructure failed: missing block: B:23:0x0059, code lost:
        if (r6.j == 1) goto L24;
     */
    /* JADX WARN: Code restructure failed: missing block: B:24:0x005b, code lost:
        b.c.a.a0.d.v0(r6.c, null);
        c();
        r6.j--;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void b() {
        /*
            r6 = this;
            java.util.concurrent.atomic.AtomicInteger r0 = r6.k
            int r0 = r0.decrementAndGet()
            if (r0 >= 0) goto L19
            java.lang.String r0 = "WakeLock"
            java.lang.String r1 = r6.f
            java.lang.String r1 = java.lang.String.valueOf(r1)
            java.lang.String r2 = " release without a matched acquire!"
            java.lang.String r1 = r1.concat(r2)
            android.util.Log.e(r0, r1)
        L19:
            boolean r0 = r6.h
            r1 = 0
            if (r0 == 0) goto L21
            android.text.TextUtils.isEmpty(r1)
        L21:
            java.lang.Object r0 = r6.f1587b
            monitor-enter(r0)
            boolean r2 = r6.h     // Catch: java.lang.Throwable -> L6d
            r3 = 1
            if (r2 == 0) goto L53
            java.util.Map<java.lang.String, java.lang.Integer[]> r2 = r6.i     // Catch: java.lang.Throwable -> L6d
            java.lang.Object r2 = r2.get(r1)     // Catch: java.lang.Throwable -> L6d
            java.lang.Integer[] r2 = (java.lang.Integer[]) r2     // Catch: java.lang.Throwable -> L6d
            r4 = 0
            if (r2 != 0) goto L35
            goto L51
        L35:
            r5 = r2[r4]     // Catch: java.lang.Throwable -> L6d
            int r5 = r5.intValue()     // Catch: java.lang.Throwable -> L6d
            if (r5 != r3) goto L44
            java.util.Map<java.lang.String, java.lang.Integer[]> r2 = r6.i     // Catch: java.lang.Throwable -> L6d
            r2.remove(r1)     // Catch: java.lang.Throwable -> L6d
            r4 = 1
            goto L51
        L44:
            r5 = r2[r4]     // Catch: java.lang.Throwable -> L6d
            int r5 = r5.intValue()     // Catch: java.lang.Throwable -> L6d
            int r5 = r5 - r3
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch: java.lang.Throwable -> L6d
            r2[r4] = r5     // Catch: java.lang.Throwable -> L6d
        L51:
            if (r4 != 0) goto L5b
        L53:
            boolean r2 = r6.h     // Catch: java.lang.Throwable -> L6d
            if (r2 != 0) goto L68
            int r2 = r6.j     // Catch: java.lang.Throwable -> L6d
            if (r2 != r3) goto L68
        L5b:
            android.os.PowerManager$WakeLock r2 = r6.c     // Catch: java.lang.Throwable -> L6d
            b.c.a.a0.d.v0(r2, r1)     // Catch: java.lang.Throwable -> L6d
            r6.c()     // Catch: java.lang.Throwable -> L6d
            int r1 = r6.j     // Catch: java.lang.Throwable -> L6d
            int r1 = r1 - r3
            r6.j = r1     // Catch: java.lang.Throwable -> L6d
        L68:
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L6d
            r6.d()
            return
        L6d:
            r1 = move-exception
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L6d
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.m.a.b():void");
    }

    /* JADX WARN: Removed duplicated region for block: B:11:0x002e  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.util.List<java.lang.String> c() {
        /*
            r12 = this;
            android.os.WorkSource r0 = r12.d
            java.lang.reflect.Method r1 = b.i.a.f.e.o.i.a
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.lang.String r2 = "null reference"
            java.lang.String r3 = "Unable to assign blame through WorkSource"
            java.lang.String r4 = "WorkSourceUtil"
            r5 = 0
            if (r0 != 0) goto L13
            goto L2b
        L13:
            java.lang.reflect.Method r6 = b.i.a.f.e.o.i.c
            if (r6 == 0) goto L2b
            java.lang.Object[] r7 = new java.lang.Object[r5]     // Catch: java.lang.Exception -> L27
            java.lang.Object r6 = r6.invoke(r0, r7)     // Catch: java.lang.Exception -> L27
            java.util.Objects.requireNonNull(r6, r2)     // Catch: java.lang.Exception -> L27
            java.lang.Integer r6 = (java.lang.Integer) r6     // Catch: java.lang.Exception -> L27
            int r6 = r6.intValue()     // Catch: java.lang.Exception -> L27
            goto L2c
        L27:
            r6 = move-exception
            android.util.Log.wtf(r4, r3, r6)
        L2b:
            r6 = 0
        L2c:
            if (r6 == 0) goto L5a
            r7 = 0
        L2f:
            if (r7 >= r6) goto L5a
            java.lang.reflect.Method r8 = b.i.a.f.e.o.i.d
            r9 = 0
            if (r8 == 0) goto L4b
            r10 = 1
            java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch: java.lang.Exception -> L47
            java.lang.Integer r11 = java.lang.Integer.valueOf(r7)     // Catch: java.lang.Exception -> L47
            r10[r5] = r11     // Catch: java.lang.Exception -> L47
            java.lang.Object r8 = r8.invoke(r0, r10)     // Catch: java.lang.Exception -> L47
            java.lang.String r8 = (java.lang.String) r8     // Catch: java.lang.Exception -> L47
            r9 = r8
            goto L4b
        L47:
            r8 = move-exception
            android.util.Log.wtf(r4, r3, r8)
        L4b:
            boolean r8 = b.i.a.f.e.o.h.a(r9)
            if (r8 != 0) goto L57
            java.util.Objects.requireNonNull(r9, r2)
            r1.add(r9)
        L57:
            int r7 = r7 + 1
            goto L2f
        L5a:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.m.a.c():java.util.List");
    }

    public final void d() {
        if (this.c.isHeld()) {
            try {
                this.c.release();
            } catch (RuntimeException e) {
                if (e.getClass().equals(RuntimeException.class)) {
                    Log.e("WakeLock", String.valueOf(this.f).concat(" was already released!"), e);
                } else {
                    throw e;
                }
            }
            this.c.isHeld();
        }
    }
}
