package b.i.a.f.l.b;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.common.internal.zas;
import com.google.android.gms.signin.internal.zak;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public final class i implements Parcelable.Creator<zak> {
    @Override // android.os.Parcelable.Creator
    public final zak createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        int i = 0;
        zas zasVar = null;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                i = d.G1(parcel, readInt);
            } else if (c != 2) {
                d.d2(parcel, readInt);
            } else {
                zasVar = (zas) d.Q(parcel, readInt, zas.CREATOR);
            }
        }
        d.f0(parcel, m2);
        return new zak(i, zasVar);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zak[] newArray(int i) {
        return new zak[i];
    }
}
