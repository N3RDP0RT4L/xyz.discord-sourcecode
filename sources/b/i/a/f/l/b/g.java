package b.i.a.f.l.b;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import b.i.a.f.h.e.a;
import b.i.a.f.h.e.b;
import com.google.android.gms.signin.internal.zak;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public final class g implements e, IInterface {
    public final IBinder a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1585b = "com.google.android.gms.signin.internal.ISignInService";

    public g(IBinder iBinder) {
        this.a = iBinder;
    }

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this.a;
    }

    @Override // b.i.a.f.l.b.e
    public final void f0(zak zakVar, c cVar) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.f1585b);
        int i = b.a;
        obtain.writeInt(1);
        zakVar.writeToParcel(obtain, 0);
        obtain.writeStrongBinder((a) cVar);
        Parcel obtain2 = Parcel.obtain();
        try {
            this.a.transact(12, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }
}
