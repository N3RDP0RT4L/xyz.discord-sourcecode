package b.i.a.f.l.b;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.zau;
import com.google.android.gms.signin.internal.zam;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public final class j implements Parcelable.Creator<zam> {
    @Override // android.os.Parcelable.Creator
    public final zam createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        ConnectionResult connectionResult = null;
        zau zauVar = null;
        int i = 0;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                i = d.G1(parcel, readInt);
            } else if (c == 2) {
                connectionResult = (ConnectionResult) d.Q(parcel, readInt, ConnectionResult.CREATOR);
            } else if (c != 3) {
                d.d2(parcel, readInt);
            } else {
                zauVar = (zau) d.Q(parcel, readInt, zau.CREATOR);
            }
        }
        d.f0(parcel, m2);
        return new zam(i, connectionResult, zauVar);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zam[] newArray(int i) {
        return new zam[i];
    }
}
