package b.i.a.f.l.b;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.signin.internal.zab;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public final class b implements Parcelable.Creator<zab> {
    @Override // android.os.Parcelable.Creator
    public final zab createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        int i = 0;
        Intent intent = null;
        int i2 = 0;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                i = d.G1(parcel, readInt);
            } else if (c == 2) {
                i2 = d.G1(parcel, readInt);
            } else if (c != 3) {
                d.d2(parcel, readInt);
            } else {
                intent = (Intent) d.Q(parcel, readInt, Intent.CREATOR);
            }
        }
        d.f0(parcel, m2);
        return new zab(i, i2, intent);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zab[] newArray(int i) {
        return new zab[i];
    }
}
