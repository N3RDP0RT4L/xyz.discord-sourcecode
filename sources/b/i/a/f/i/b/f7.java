package b.i.a.f.i.b;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.annotation.WorkerThread;
import b.c.a.a0.d;
import com.adjust.sdk.Constants;
import com.google.android.material.shadow.ShadowDrawableWrapper;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
@WorkerThread
/* loaded from: classes3.dex */
public final class f7 implements Runnable {
    public final URL j;
    public final x4 k;
    public final String l;
    public final /* synthetic */ d7 m;

    public f7(d7 d7Var, String str, URL url, x4 x4Var) {
        this.m = d7Var;
        d.w(str);
        this.j = url;
        this.k = x4Var;
        this.l = str;
    }

    public final void a(final int i, final Exception exc, final byte[] bArr, final Map<String, List<String>> map) {
        this.m.f().v(new Runnable(this, i, exc, bArr, map) { // from class: b.i.a.f.i.b.e7
            public final f7 j;
            public final int k;
            public final Exception l;
            public final byte[] m;
            public final Map n;

            {
                this.j = this;
                this.k = i;
                this.l = exc;
                this.m = bArr;
                this.n = map;
            }

            @Override // java.lang.Runnable
            public final void run() {
                List<ResolveInfo> queryIntentActivities;
                f7 f7Var = this.j;
                int i2 = this.k;
                Exception exc2 = this.l;
                byte[] bArr2 = this.m;
                u4 u4Var = f7Var.k.a;
                boolean z2 = true;
                if (!((i2 == 200 || i2 == 204 || i2 == 304) && exc2 == null)) {
                    u4Var.g().i.c("Network Request for Deferred Deep Link failed. response, exception", Integer.valueOf(i2), exc2);
                    return;
                }
                u4Var.o().f1522y.a(true);
                if (bArr2.length == 0) {
                    u4Var.g().m.a("Deferred Deep Link response empty.");
                    return;
                }
                try {
                    JSONObject jSONObject = new JSONObject(new String(bArr2));
                    String optString = jSONObject.optString(Constants.DEEPLINK, "");
                    String optString2 = jSONObject.optString("gclid", "");
                    double optDouble = jSONObject.optDouble("timestamp", ShadowDrawableWrapper.COS_45);
                    if (TextUtils.isEmpty(optString)) {
                        u4Var.g().m.a("Deferred Deep Link is empty.");
                        return;
                    }
                    t9 t = u4Var.t();
                    if (TextUtils.isEmpty(optString) || (queryIntentActivities = t.a.f1566b.getPackageManager().queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse(optString)), 0)) == null || queryIntentActivities.isEmpty()) {
                        z2 = false;
                    }
                    if (!z2) {
                        u4Var.g().i.c("Deferred Deep Link validation failed. gclid, deep link", optString2, optString);
                        return;
                    }
                    Bundle bundle = new Bundle();
                    bundle.putString("gclid", optString2);
                    bundle.putString("_cis", "ddp");
                    u4Var.q.H("auto", "_cmp", bundle);
                    t9 t2 = u4Var.t();
                    if (!TextUtils.isEmpty(optString) && t2.Y(optString, optDouble)) {
                        t2.a.f1566b.sendBroadcast(new Intent("android.google.analytics.action.DEEPLINK_ACTION"));
                    }
                } catch (JSONException e) {
                    u4Var.g().f.b("Failed to parse the Deferred Deep Link response. exception", e);
                }
            }
        });
    }

    @Override // java.lang.Runnable
    public final void run() {
        IOException e;
        Map<String, List<String>> map;
        HttpURLConnection httpURLConnection;
        Throwable th;
        this.m.a();
        int i = 0;
        try {
            httpURLConnection = this.m.s(this.j);
            try {
                i = httpURLConnection.getResponseCode();
                map = httpURLConnection.getHeaderFields();
            } catch (IOException e2) {
                e = e2;
                map = null;
            } catch (Throwable th2) {
                th = th2;
                map = null;
            }
            try {
                byte[] t = d7.t(httpURLConnection);
                httpURLConnection.disconnect();
                a(i, null, t, map);
            } catch (IOException e3) {
                e = e3;
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
                a(i, e, null, map);
            } catch (Throwable th3) {
                th = th3;
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
                a(i, null, null, map);
                throw th;
            }
        } catch (IOException e4) {
            e = e4;
            httpURLConnection = null;
            map = null;
        } catch (Throwable th4) {
            th = th4;
            httpURLConnection = null;
            map = null;
        }
    }
}
