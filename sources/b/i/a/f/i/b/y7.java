package b.i.a.f.i.b;

import b.i.a.f.e.c;
import b.i.a.f.h.l.fc;
import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService;
import com.google.android.gms.measurement.internal.zzaq;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-measurement-sdk@@18.0.0 */
/* loaded from: classes3.dex */
public final class y7 implements Runnable {
    public final /* synthetic */ fc j;
    public final /* synthetic */ zzaq k;
    public final /* synthetic */ String l;
    public final /* synthetic */ AppMeasurementDynamiteService m;

    public y7(AppMeasurementDynamiteService appMeasurementDynamiteService, fc fcVar, zzaq zzaqVar, String str) {
        this.m = appMeasurementDynamiteService;
        this.j = fcVar;
        this.k = zzaqVar;
        this.l = str;
    }

    @Override // java.lang.Runnable
    public final void run() {
        q7 x2 = this.m.a.x();
        fc fcVar = this.j;
        zzaq zzaqVar = this.k;
        String str = this.l;
        x2.b();
        x2.t();
        t9 e = x2.e();
        Objects.requireNonNull(e);
        if (c.f1342b.b(e.a.f1566b, 12451000) != 0) {
            x2.g().i.a("Not bundling data. Service unavailable or out of date");
            x2.e().P(fcVar, new byte[0]);
            return;
        }
        x2.z(new c8(x2, zzaqVar, str, fcVar));
    }
}
