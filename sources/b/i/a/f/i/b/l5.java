package b.i.a.f.i.b;

import com.google.android.gms.measurement.internal.zzaq;
import java.util.concurrent.Callable;
/* compiled from: com.google.android.gms:play-services-measurement@@18.0.0 */
/* loaded from: classes3.dex */
public final class l5 implements Callable<byte[]> {
    public final /* synthetic */ zzaq j;
    public final /* synthetic */ String k;
    public final /* synthetic */ z4 l;

    public l5(z4 z4Var, zzaq zzaqVar, String str) {
        this.l = z4Var;
        this.j = zzaqVar;
        this.k = str;
    }

    @Override // java.util.concurrent.Callable
    public final byte[] call() throws Exception {
        this.l.a.R();
        k9 k9Var = this.l.a;
        k9.C(k9Var.i);
        g7 g7Var = k9Var.i;
        g7Var.b();
        g7Var.a.l();
        throw null;
    }
}
