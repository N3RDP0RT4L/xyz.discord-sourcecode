package b.i.a.f.i.b;

import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.annotation.WorkerThread;
import b.i.a.f.e.o.c;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class y9 {
    public final u4 a;

    public y9(u4 u4Var) {
        this.a = u4Var;
    }

    @WorkerThread
    public final void a(String str, Bundle bundle) {
        String str2;
        this.a.f().b();
        if (!this.a.d()) {
            if (bundle.isEmpty()) {
                str2 = null;
            } else {
                if (str == null || str.isEmpty()) {
                    str = "auto";
                }
                Uri.Builder builder = new Uri.Builder();
                builder.path(str);
                for (String str3 : bundle.keySet()) {
                    builder.appendQueryParameter(str3, bundle.getString(str3));
                }
                str2 = builder.build().toString();
            }
            if (!TextUtils.isEmpty(str2)) {
                this.a.o().B.b(str2);
                h4 h4Var = this.a.o().C;
                Objects.requireNonNull((c) this.a.o);
                h4Var.b(System.currentTimeMillis());
            }
        }
    }

    public final boolean b() {
        if (!c()) {
            return false;
        }
        Objects.requireNonNull((c) this.a.o);
        return System.currentTimeMillis() - this.a.o().C.a() > this.a.h.n(null, p.E0);
    }

    public final boolean c() {
        return this.a.o().C.a() > 0;
    }
}
