package b.i.a.f.i.b;

import b.i.a.f.h.l.l0;
/* compiled from: com.google.android.gms:play-services-measurement@@18.0.0 */
/* loaded from: classes3.dex */
public final class fa extends ea {
    public l0 g;
    public final /* synthetic */ ba h;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public fa(ba baVar, String str, int i, l0 l0Var) {
        super(str, i);
        this.h = baVar;
        this.g = l0Var;
    }

    @Override // b.i.a.f.i.b.ea
    public final int a() {
        return this.g.y();
    }

    @Override // b.i.a.f.i.b.ea
    public final boolean g() {
        return false;
    }

    @Override // b.i.a.f.i.b.ea
    public final boolean h() {
        return this.g.C();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:148:0x042a  */
    /* JADX WARN: Removed duplicated region for block: B:151:0x0432 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:152:0x0433  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final boolean i(java.lang.Long r17, java.lang.Long r18, b.i.a.f.h.l.a1 r19, long r20, b.i.a.f.i.b.l r22, boolean r23) {
        /*
            Method dump skipped, instructions count: 1184
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.fa.i(java.lang.Long, java.lang.Long, b.i.a.f.h.l.a1, long, b.i.a.f.i.b.l, boolean):boolean");
    }
}
