package b.i.a.f.i.b;

import android.os.Bundle;
import android.os.Parcel;
import android.os.RemoteException;
import b.c.a.a0.d;
import b.i.a.f.h.l.s0;
import b.i.a.f.h.l.v;
import com.google.android.gms.measurement.internal.zzaq;
import com.google.android.gms.measurement.internal.zzku;
import com.google.android.gms.measurement.internal.zzn;
import com.google.android.gms.measurement.internal.zzz;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public abstract class l3 extends s0 implements i3 {
    public l3() {
        super("com.google.android.gms.measurement.internal.IMeasurementService");
    }

    @Override // b.i.a.f.h.l.s0
    public final boolean c(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        ArrayList arrayList;
        boolean z2 = false;
        switch (i) {
            case 1:
                ((z4) this).l0((zzaq) v.a(parcel, zzaq.CREATOR), (zzn) v.a(parcel, zzn.CREATOR));
                parcel2.writeNoException();
                break;
            case 2:
                zzku zzkuVar = (zzku) v.a(parcel, zzku.CREATOR);
                zzn zznVar = (zzn) v.a(parcel, zzn.CREATOR);
                z4 z4Var = (z4) this;
                Objects.requireNonNull(zzkuVar, "null reference");
                z4Var.u0(zznVar);
                z4Var.i(new o5(z4Var, zzkuVar, zznVar));
                parcel2.writeNoException();
                break;
            case 3:
            case 8:
            default:
                return false;
            case 4:
                zzn zznVar2 = (zzn) v.a(parcel, zzn.CREATOR);
                z4 z4Var2 = (z4) this;
                z4Var2.u0(zznVar2);
                z4Var2.i(new q5(z4Var2, zznVar2));
                parcel2.writeNoException();
                break;
            case 5:
                zzaq zzaqVar = (zzaq) v.a(parcel, zzaq.CREATOR);
                String readString = parcel.readString();
                parcel.readString();
                z4 z4Var3 = (z4) this;
                Objects.requireNonNull(zzaqVar, "null reference");
                d.w(readString);
                z4Var3.t0(readString, true);
                z4Var3.i(new m5(z4Var3, zzaqVar, readString));
                parcel2.writeNoException();
                break;
            case 6:
                zzn zznVar3 = (zzn) v.a(parcel, zzn.CREATOR);
                z4 z4Var4 = (z4) this;
                z4Var4.u0(zznVar3);
                z4Var4.i(new c5(z4Var4, zznVar3));
                parcel2.writeNoException();
                break;
            case 7:
                zzn zznVar4 = (zzn) v.a(parcel, zzn.CREATOR);
                if (parcel.readInt() != 0) {
                    z2 = true;
                }
                z4 z4Var5 = (z4) this;
                z4Var5.u0(zznVar4);
                try {
                    List<u9> list = (List) ((FutureTask) z4Var5.a.f().t(new n5(z4Var5, zznVar4))).get();
                    arrayList = new ArrayList(list.size());
                    for (u9 u9Var : list) {
                        if (z2 || !t9.r0(u9Var.c)) {
                            arrayList.add(new zzku(u9Var));
                        }
                    }
                } catch (InterruptedException | ExecutionException e) {
                    z4Var5.a.g().f.c("Failed to get user properties. appId", q3.s(zznVar4.j), e);
                    arrayList = null;
                }
                parcel2.writeNoException();
                parcel2.writeTypedList(arrayList);
                break;
            case 9:
                byte[] j = ((z4) this).j((zzaq) v.a(parcel, zzaq.CREATOR), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeByteArray(j);
                break;
            case 10:
                ((z4) this).G(parcel.readLong(), parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                break;
            case 11:
                String A = ((z4) this).A((zzn) v.a(parcel, zzn.CREATOR));
                parcel2.writeNoException();
                parcel2.writeString(A);
                break;
            case 12:
                ((z4) this).q0((zzz) v.a(parcel, zzz.CREATOR), (zzn) v.a(parcel, zzn.CREATOR));
                parcel2.writeNoException();
                break;
            case 13:
                ((z4) this).g((zzz) v.a(parcel, zzz.CREATOR));
                parcel2.writeNoException();
                break;
            case 14:
                String readString2 = parcel.readString();
                String readString3 = parcel.readString();
                ClassLoader classLoader = v.a;
                if (parcel.readInt() != 0) {
                    z2 = true;
                }
                List<zzku> R = ((z4) this).R(readString2, readString3, z2, (zzn) v.a(parcel, zzn.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(R);
                break;
            case 15:
                String readString4 = parcel.readString();
                String readString5 = parcel.readString();
                String readString6 = parcel.readString();
                ClassLoader classLoader2 = v.a;
                if (parcel.readInt() != 0) {
                    z2 = true;
                }
                List<zzku> u = ((z4) this).u(readString4, readString5, readString6, z2);
                parcel2.writeNoException();
                parcel2.writeTypedList(u);
                break;
            case 16:
                List<zzz> K = ((z4) this).K(parcel.readString(), parcel.readString(), (zzn) v.a(parcel, zzn.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(K);
                break;
            case 17:
                List<zzz> J = ((z4) this).J(parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeTypedList(J);
                break;
            case 18:
                zzn zznVar5 = (zzn) v.a(parcel, zzn.CREATOR);
                z4 z4Var6 = (z4) this;
                z4Var6.t0(zznVar5.j, false);
                z4Var6.i(new h5(z4Var6, zznVar5));
                parcel2.writeNoException();
                break;
            case 19:
                ((z4) this).m0((Bundle) v.a(parcel, Bundle.CREATOR), (zzn) v.a(parcel, zzn.CREATOR));
                parcel2.writeNoException();
                break;
            case 20:
                ((z4) this).m((zzn) v.a(parcel, zzn.CREATOR));
                parcel2.writeNoException();
                break;
        }
        return true;
    }
}
