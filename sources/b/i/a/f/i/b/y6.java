package b.i.a.f.i.b;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import androidx.annotation.MainThread;
import b.i.a.f.e.o.c;
import com.adjust.sdk.Constants;
import com.discord.models.domain.ModelAuditLogEntry;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
@TargetApi(14)
@MainThread
/* loaded from: classes3.dex */
public final class y6 implements Application.ActivityLifecycleCallbacks {
    public final /* synthetic */ c6 j;

    public y6(c6 c6Var, d6 d6Var) {
        this.j = c6Var;
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivityCreated(Activity activity, Bundle bundle) {
        try {
            this.j.g().n.a("onActivityCreated");
            Intent intent = activity.getIntent();
            if (intent != null) {
                Uri data = intent.getData();
                if (data != null && data.isHierarchical()) {
                    this.j.e();
                    this.j.f().v(new c7(this, bundle == null, data, t9.V(intent) ? "gs" : "auto", data.getQueryParameter(Constants.REFERRER)));
                }
            }
        } catch (Exception e) {
            this.j.g().f.b("Throwable caught in onActivityCreated", e);
        } finally {
            this.j.q().y(activity, bundle);
        }
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivityDestroyed(Activity activity) {
        h7 q = this.j.q();
        synchronized (q.l) {
            if (activity == q.g) {
                q.g = null;
            }
        }
        if (q.a.h.z().booleanValue()) {
            q.f.remove(activity);
        }
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    @MainThread
    public final void onActivityPaused(Activity activity) {
        h7 q = this.j.q();
        if (q.a.h.o(p.v0)) {
            synchronized (q.l) {
                q.k = false;
                q.h = true;
            }
        }
        Objects.requireNonNull((c) q.a.o);
        long elapsedRealtime = SystemClock.elapsedRealtime();
        if (!q.a.h.o(p.u0) || q.a.h.z().booleanValue()) {
            i7 E = q.E(activity);
            q.d = q.c;
            q.c = null;
            q.f().v(new o7(q, E, elapsedRealtime));
        } else {
            q.c = null;
            q.f().v(new l7(q, elapsedRealtime));
        }
        w8 s2 = this.j.s();
        Objects.requireNonNull((c) s2.a.o);
        s2.f().v(new y8(s2, SystemClock.elapsedRealtime()));
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    @MainThread
    public final void onActivityResumed(Activity activity) {
        w8 s2 = this.j.s();
        Objects.requireNonNull((c) s2.a.o);
        s2.f().v(new v8(s2, SystemClock.elapsedRealtime()));
        h7 q = this.j.q();
        if (q.a.h.o(p.v0)) {
            synchronized (q.l) {
                q.k = true;
                if (activity != q.g) {
                    synchronized (q.l) {
                        q.g = activity;
                        q.h = false;
                    }
                    if (q.a.h.o(p.u0) && q.a.h.z().booleanValue()) {
                        q.i = null;
                        q.f().v(new n7(q));
                    }
                }
            }
        }
        if (!q.a.h.o(p.u0) || q.a.h.z().booleanValue()) {
            q.z(activity, q.E(activity), false);
            a m = q.m();
            Objects.requireNonNull((c) m.a.o);
            m.f().v(new a3(m, SystemClock.elapsedRealtime()));
            return;
        }
        q.c = q.i;
        q.f().v(new m7(q));
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        i7 i7Var;
        h7 q = this.j.q();
        if (q.a.h.z().booleanValue() && bundle != null && (i7Var = q.f.get(activity)) != null) {
            Bundle bundle2 = new Bundle();
            bundle2.putLong(ModelAuditLogEntry.CHANGE_KEY_ID, i7Var.c);
            bundle2.putString(ModelAuditLogEntry.CHANGE_KEY_NAME, i7Var.a);
            bundle2.putString("referrer_name", i7Var.f1533b);
            bundle.putBundle("com.google.app_measurement.screen_service", bundle2);
        }
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivityStarted(Activity activity) {
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivityStopped(Activity activity) {
    }
}
