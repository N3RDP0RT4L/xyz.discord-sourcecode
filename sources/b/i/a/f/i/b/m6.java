package b.i.a.f.i.b;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class m6 implements Runnable {
    public final /* synthetic */ AtomicReference j;
    public final /* synthetic */ c6 k;

    public m6(c6 c6Var, AtomicReference atomicReference) {
        this.k = c6Var;
        this.j = atomicReference;
    }

    @Override // java.lang.Runnable
    public final void run() {
        String str;
        synchronized (this.j) {
            AtomicReference atomicReference = this.j;
            c6 c6Var = this.k;
            c cVar = c6Var.a.h;
            n3 o = c6Var.o();
            o.t();
            String str2 = o.c;
            Objects.requireNonNull(cVar);
            j3<String> j3Var = p.L;
            if (str2 == null) {
                str = j3Var.a(null);
            } else {
                str = j3Var.a(cVar.c.h(str2, j3Var.f1535b));
            }
            atomicReference.set(str);
            this.j.notify();
        }
    }
}
