package b.i.a.f.i.b;

import android.content.SharedPreferences;
import android.util.Pair;
import androidx.annotation.WorkerThread;
import com.discord.stores.StoreGuildScheduledEvents;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class d4 extends r5 {
    public static final Pair<String, Long> c = new Pair<>("", 0L);
    public SharedPreferences d;
    public k4 e;
    public String n;
    public boolean o;
    public long p;
    public boolean w;
    public final h4 f = new h4(this, "last_upload", 0);
    public final h4 g = new h4(this, "last_upload_attempt", 0);
    public final h4 h = new h4(this, "backoff", 0);
    public final h4 i = new h4(this, "last_delete_stale", 0);
    public final h4 q = new h4(this, "time_before_start", 10000);
    public final h4 r = new h4(this, "session_timeout", StoreGuildScheduledEvents.FETCH_GUILD_EVENTS_THRESHOLD);

    /* renamed from: s  reason: collision with root package name */
    public final f4 f1520s = new f4(this, "start_new_session", true);
    public final h4 v = new h4(this, "last_pause_time", 0);
    public final j4 t = new j4(this, "non_personalized_ads");
    public final f4 u = new f4(this, "allow_remote_dynamite", false);
    public final h4 j = new h4(this, "midnight_offset", 0);
    public final h4 k = new h4(this, "first_open_time", 0);
    public final h4 l = new h4(this, "app_install_time", 0);
    public final j4 m = new j4(this, "app_instance_id");

    /* renamed from: x  reason: collision with root package name */
    public f4 f1521x = new f4(this, "app_backgrounded", false);

    /* renamed from: y  reason: collision with root package name */
    public f4 f1522y = new f4(this, "deep_link_retrieval_complete", false);

    /* renamed from: z  reason: collision with root package name */
    public h4 f1523z = new h4(this, "deep_link_retrieval_attempts", 0);
    public final j4 A = new j4(this, "firebase_feature_rollouts");
    public final j4 B = new j4(this, "deferred_attribution_cache");
    public final h4 C = new h4(this, "deferred_attribution_cache_timestamp", 0);
    public final i4 D = new i4(this, "default_event_parameters");

    public d4(u4 u4Var) {
        super(u4Var);
    }

    @Override // b.i.a.f.i.b.r5
    @WorkerThread
    public final void m() {
        SharedPreferences sharedPreferences = this.a.f1566b.getSharedPreferences("com.google.android.gms.measurement.prefs", 0);
        this.d = sharedPreferences;
        boolean z2 = sharedPreferences.getBoolean("has_been_opened", false);
        this.w = z2;
        if (!z2) {
            SharedPreferences.Editor edit = this.d.edit();
            edit.putBoolean("has_been_opened", true);
            edit.apply();
        }
        this.e = new k4(this, "health_monitor", Math.max(0L, p.c.a(null).longValue()), null);
    }

    @Override // b.i.a.f.i.b.r5
    public final boolean r() {
        return true;
    }

    @WorkerThread
    public final void s(Boolean bool) {
        b();
        SharedPreferences.Editor edit = w().edit();
        if (bool != null) {
            edit.putBoolean("measurement_enabled", bool.booleanValue());
        } else {
            edit.remove("measurement_enabled");
        }
        edit.apply();
    }

    @WorkerThread
    public final boolean t(int i) {
        return d.e(i, w().getInt("consent_source", 100));
    }

    public final boolean u(long j) {
        return j - this.r.a() > this.v.a();
    }

    @WorkerThread
    public final void v(boolean z2) {
        b();
        g().n.b("App measurement setting deferred collection", Boolean.valueOf(z2));
        SharedPreferences.Editor edit = w().edit();
        edit.putBoolean("deferred_analytics_collection", z2);
        edit.apply();
    }

    @WorkerThread
    public final SharedPreferences w() {
        b();
        o();
        return this.d;
    }

    @WorkerThread
    public final Boolean x() {
        b();
        if (w().contains("measurement_enabled")) {
            return Boolean.valueOf(w().getBoolean("measurement_enabled", true));
        }
        return null;
    }

    @WorkerThread
    public final d y() {
        b();
        return d.b(w().getString("consent_settings", "G1"));
    }
}
