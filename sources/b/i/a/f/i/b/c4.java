package b.i.a.f.i.b;

import androidx.annotation.WorkerThread;
import b.c.a.a0.d;
import java.net.URL;
import java.util.Map;
/* compiled from: com.google.android.gms:play-services-measurement@@18.0.0 */
@WorkerThread
/* loaded from: classes3.dex */
public final class c4 implements Runnable {
    public final URL j;
    public final byte[] k;
    public final z3 l;
    public final String m;
    public final Map<String, String> n;
    public final /* synthetic */ x3 o;

    public c4(x3 x3Var, String str, URL url, byte[] bArr, Map<String, String> map, z3 z3Var) {
        this.o = x3Var;
        d.w(str);
        this.j = url;
        this.k = bArr;
        this.l = z3Var;
        this.m = str;
        this.n = map;
    }

    /* JADX WARN: Removed duplicated region for block: B:45:0x00e1  */
    /* JADX WARN: Removed duplicated region for block: B:57:0x011a  */
    /* JADX WARN: Removed duplicated region for block: B:60:0x0102 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:62:0x00c9 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    @Override // java.lang.Runnable
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void run() {
        /*
            Method dump skipped, instructions count: 307
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.c4.run():void");
    }
}
