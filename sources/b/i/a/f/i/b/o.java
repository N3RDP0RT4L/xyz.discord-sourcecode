package b.i.a.f.i.b;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.measurement.internal.zzap;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class o implements Parcelable.Creator<zzap> {
    @Override // android.os.Parcelable.Creator
    public final zzap createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        Bundle bundle = null;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            if (((char) readInt) != 2) {
                d.d2(parcel, readInt);
            } else {
                bundle = d.M(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        return new zzap(bundle);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzap[] newArray(int i) {
        return new zzap[i];
    }
}
