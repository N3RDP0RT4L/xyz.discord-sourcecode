package b.i.a.f.i.b;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class z7 extends i {
    public final /* synthetic */ q7 e;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public z7(q7 q7Var, t5 t5Var) {
        super(t5Var);
        this.e = q7Var;
    }

    @Override // b.i.a.f.i.b.i
    public final void a() {
        this.e.g().i.a("Tasks have been queued for a long time");
    }
}
