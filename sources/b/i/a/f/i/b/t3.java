package b.i.a.f.i.b;

import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import androidx.exifinterface.media.ExifInterface;
import androidx.recyclerview.widget.RecyclerView;
import b.i.a.f.e.o.g;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class t3 implements Runnable {
    public final /* synthetic */ int j;
    public final /* synthetic */ String k;
    public final /* synthetic */ Object l;
    public final /* synthetic */ Object m;
    public final /* synthetic */ Object n;
    public final /* synthetic */ q3 o;

    public t3(q3 q3Var, int i, String str, Object obj, Object obj2, Object obj3) {
        this.o = q3Var;
        this.j = i;
        this.k = str;
        this.l = obj;
        this.m = obj2;
        this.n = obj3;
    }

    @Override // java.lang.Runnable
    public final void run() {
        d4 o = this.o.a.o();
        if (!o.n()) {
            this.o.v(6, "Persisted config not initialized. Not logging error/warn");
            return;
        }
        q3 q3Var = this.o;
        boolean z2 = true;
        if (q3Var.c == 0) {
            c cVar = q3Var.a.h;
            if (cVar.d == null) {
                synchronized (cVar) {
                    if (cVar.d == null) {
                        ApplicationInfo applicationInfo = cVar.a.f1566b.getApplicationInfo();
                        String a = g.a();
                        if (applicationInfo != null) {
                            String str = applicationInfo.processName;
                            cVar.d = Boolean.valueOf(str != null && str.equals(a));
                        }
                        if (cVar.d == null) {
                            cVar.d = Boolean.TRUE;
                            cVar.g().f.a("My process not in the list of running processes");
                        }
                    }
                }
            }
            if (cVar.d.booleanValue()) {
                this.o.c = 'C';
            } else {
                this.o.c = 'c';
            }
        }
        q3 q3Var2 = this.o;
        if (q3Var2.d < 0) {
            q3Var2.d = 33025L;
        }
        char charAt = "01VDIWEA?".charAt(this.j);
        q3 q3Var3 = this.o;
        char c = q3Var3.c;
        long j = q3Var3.d;
        String u = q3.u(true, this.k, this.l, this.m, this.n);
        StringBuilder sb = new StringBuilder(String.valueOf(u).length() + 24);
        sb.append(ExifInterface.GPS_MEASUREMENT_2D);
        sb.append(charAt);
        sb.append(c);
        sb.append(j);
        sb.append(":");
        sb.append(u);
        String sb2 = sb.toString();
        if (sb2.length() > 1024) {
            sb2 = this.k.substring(0, 1024);
        }
        k4 k4Var = o.e;
        k4Var.e.b();
        if (k4Var.e.w().getLong(k4Var.a, 0L) == 0) {
            k4Var.a();
        }
        if (sb2 == null) {
            sb2 = "";
        }
        long j2 = k4Var.e.w().getLong(k4Var.f1537b, 0L);
        if (j2 <= 0) {
            SharedPreferences.Editor edit = k4Var.e.w().edit();
            edit.putString(k4Var.c, sb2);
            edit.putLong(k4Var.f1537b, 1L);
            edit.apply();
            return;
        }
        long j3 = j2 + 1;
        if ((k4Var.e.e().v0().nextLong() & RecyclerView.FOREVER_NS) >= RecyclerView.FOREVER_NS / j3) {
            z2 = false;
        }
        SharedPreferences.Editor edit2 = k4Var.e.w().edit();
        if (z2) {
            edit2.putString(k4Var.c, sb2);
        }
        edit2.putLong(k4Var.f1537b, j3);
        edit2.apply();
    }
}
