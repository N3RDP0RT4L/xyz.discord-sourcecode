package b.i.a.f.i.b;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import b.i.a.f.h.l.a;
import b.i.a.f.h.l.v;
import com.google.android.gms.measurement.internal.zzaq;
import com.google.android.gms.measurement.internal.zzku;
import com.google.android.gms.measurement.internal.zzn;
import com.google.android.gms.measurement.internal.zzz;
import java.util.ArrayList;
import java.util.List;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class k3 extends a implements i3 {
    public k3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.internal.IMeasurementService");
    }

    @Override // b.i.a.f.i.b.i3
    public final String A(zzn zznVar) throws RemoteException {
        Parcel c = c();
        v.c(c, zznVar);
        Parcel g = g(11, c);
        String readString = g.readString();
        g.recycle();
        return readString;
    }

    @Override // b.i.a.f.i.b.i3
    public final void G(long j, String str, String str2, String str3) throws RemoteException {
        Parcel c = c();
        c.writeLong(j);
        c.writeString(str);
        c.writeString(str2);
        c.writeString(str3);
        i(10, c);
    }

    @Override // b.i.a.f.i.b.i3
    public final void I(zzn zznVar) throws RemoteException {
        Parcel c = c();
        v.c(c, zznVar);
        i(18, c);
    }

    @Override // b.i.a.f.i.b.i3
    public final List<zzz> J(String str, String str2, String str3) throws RemoteException {
        Parcel c = c();
        c.writeString(str);
        c.writeString(str2);
        c.writeString(str3);
        Parcel g = g(17, c);
        ArrayList createTypedArrayList = g.createTypedArrayList(zzz.CREATOR);
        g.recycle();
        return createTypedArrayList;
    }

    @Override // b.i.a.f.i.b.i3
    public final List<zzz> K(String str, String str2, zzn zznVar) throws RemoteException {
        Parcel c = c();
        c.writeString(str);
        c.writeString(str2);
        v.c(c, zznVar);
        Parcel g = g(16, c);
        ArrayList createTypedArrayList = g.createTypedArrayList(zzz.CREATOR);
        g.recycle();
        return createTypedArrayList;
    }

    @Override // b.i.a.f.i.b.i3
    public final List<zzku> R(String str, String str2, boolean z2, zzn zznVar) throws RemoteException {
        Parcel c = c();
        c.writeString(str);
        c.writeString(str2);
        ClassLoader classLoader = v.a;
        c.writeInt(z2 ? 1 : 0);
        v.c(c, zznVar);
        Parcel g = g(14, c);
        ArrayList createTypedArrayList = g.createTypedArrayList(zzku.CREATOR);
        g.recycle();
        return createTypedArrayList;
    }

    @Override // b.i.a.f.i.b.i3
    public final void T(zzn zznVar) throws RemoteException {
        Parcel c = c();
        v.c(c, zznVar);
        i(4, c);
    }

    @Override // b.i.a.f.i.b.i3
    public final void d0(zzn zznVar) throws RemoteException {
        Parcel c = c();
        v.c(c, zznVar);
        i(6, c);
    }

    @Override // b.i.a.f.i.b.i3
    public final byte[] j(zzaq zzaqVar, String str) throws RemoteException {
        Parcel c = c();
        v.c(c, zzaqVar);
        c.writeString(str);
        Parcel g = g(9, c);
        byte[] createByteArray = g.createByteArray();
        g.recycle();
        return createByteArray;
    }

    @Override // b.i.a.f.i.b.i3
    public final void l0(zzaq zzaqVar, zzn zznVar) throws RemoteException {
        Parcel c = c();
        v.c(c, zzaqVar);
        v.c(c, zznVar);
        i(1, c);
    }

    @Override // b.i.a.f.i.b.i3
    public final void m(zzn zznVar) throws RemoteException {
        Parcel c = c();
        v.c(c, zznVar);
        i(20, c);
    }

    @Override // b.i.a.f.i.b.i3
    public final void m0(Bundle bundle, zzn zznVar) throws RemoteException {
        Parcel c = c();
        v.c(c, bundle);
        v.c(c, zznVar);
        i(19, c);
    }

    @Override // b.i.a.f.i.b.i3
    public final void p0(zzku zzkuVar, zzn zznVar) throws RemoteException {
        Parcel c = c();
        v.c(c, zzkuVar);
        v.c(c, zznVar);
        i(2, c);
    }

    @Override // b.i.a.f.i.b.i3
    public final void q0(zzz zzzVar, zzn zznVar) throws RemoteException {
        Parcel c = c();
        v.c(c, zzzVar);
        v.c(c, zznVar);
        i(12, c);
    }

    @Override // b.i.a.f.i.b.i3
    public final List<zzku> u(String str, String str2, String str3, boolean z2) throws RemoteException {
        Parcel c = c();
        c.writeString(str);
        c.writeString(str2);
        c.writeString(str3);
        ClassLoader classLoader = v.a;
        c.writeInt(z2 ? 1 : 0);
        Parcel g = g(15, c);
        ArrayList createTypedArrayList = g.createTypedArrayList(zzku.CREATOR);
        g.recycle();
        return createTypedArrayList;
    }
}
