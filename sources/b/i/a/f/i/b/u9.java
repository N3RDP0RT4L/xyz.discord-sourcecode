package b.i.a.f.i.b;

import b.c.a.a0.d;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class u9 {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1572b;
    public final String c;
    public final long d;
    public final Object e;

    public u9(String str, String str2, String str3, long j, Object obj) {
        d.w(str);
        d.w(str3);
        Objects.requireNonNull(obj, "null reference");
        this.a = str;
        this.f1572b = str2;
        this.c = str3;
        this.d = j;
        this.e = obj;
    }
}
