package b.i.a.f.i.b;

import android.app.Application;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.WorkerThread;
import b.d.b.a.a;
import b.i.a.f.e.h.j.h;
import b.i.a.f.e.o.b;
import b.i.a.f.e.o.c;
import b.i.a.f.e.o.f;
import b.i.a.f.h.l.c2;
import b.i.a.f.h.l.l2;
import b.i.a.f.h.l.s2;
import b.i.a.f.h.l.t2;
import b.i.a.f.h.l.t8;
import b.i.a.f.h.l.v1;
import b.i.a.f.h.l.y1;
import b.i.a.f.h.l.z2;
import com.google.android.gms.internal.measurement.zzae;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public class u4 implements t5 {
    public static volatile u4 a;
    public long A;
    public volatile Boolean B;
    public Boolean C;
    public Boolean D;
    public int F;
    public final long H;

    /* renamed from: b  reason: collision with root package name */
    public final Context f1566b;
    public final String c;
    public final String d;
    public final String e;
    public final boolean f;
    public final ga g;
    public final c h;
    public final d4 i;
    public final q3 j;
    public final r4 k;
    public final w8 l;
    public final t9 m;
    public final o3 n;
    public final b o;
    public final h7 p;
    public final c6 q;
    public final a r;

    /* renamed from: s  reason: collision with root package name */
    public final d7 f1567s;
    public m3 t;
    public q7 u;
    public j v;
    public n3 w;

    /* renamed from: x  reason: collision with root package name */
    public m4 f1568x;

    /* renamed from: z  reason: collision with root package name */
    public Boolean f1570z;

    /* renamed from: y  reason: collision with root package name */
    public boolean f1569y = false;
    public AtomicInteger G = new AtomicInteger(0);
    public volatile boolean E = true;

    public u4(y5 y5Var) {
        long j;
        Context context;
        Bundle bundle;
        boolean z2 = false;
        Context context2 = y5Var.a;
        ga gaVar = new ga();
        this.g = gaVar;
        f.c = gaVar;
        this.f1566b = context2;
        this.c = y5Var.f1576b;
        this.d = y5Var.c;
        this.e = y5Var.d;
        this.f = y5Var.h;
        this.B = y5Var.e;
        zzae zzaeVar = y5Var.g;
        if (!(zzaeVar == null || (bundle = zzaeVar.p) == null)) {
            Object obj = bundle.get("measurementEnabled");
            if (obj instanceof Boolean) {
                this.C = (Boolean) obj;
            }
            Object obj2 = zzaeVar.p.get("measurementDeactivated");
            if (obj2 instanceof Boolean) {
                this.D = (Boolean) obj2;
            }
        }
        synchronized (l2.a) {
            t2 t2Var = l2.f1452b;
            final Context applicationContext = context2.getApplicationContext();
            applicationContext = applicationContext == null ? context2 : applicationContext;
            if (t2Var == null || t2Var.a() != applicationContext) {
                y1.c();
                s2.b();
                synchronized (c2.class) {
                    c2 c2Var = c2.a;
                    if (!(c2Var == null || (context = c2Var.f1433b) == null || c2Var.c == null)) {
                        context.getContentResolver().unregisterContentObserver(c2.a.c);
                    }
                    c2.a = null;
                }
                l2.f1452b = new v1(applicationContext, f.Q1(new z2(applicationContext) { // from class: b.i.a.f.h.l.k2
                    public final Context j;

                    {
                        this.j = applicationContext;
                    }

                    @Override // b.i.a.f.h.l.z2
                    public final Object a() {
                        x2 x2Var;
                        Context context3 = this.j;
                        String str = Build.TYPE;
                        String str2 = Build.TAGS;
                        if (!((str.equals("eng") || str.equals("userdebug")) && (str2.contains("dev-keys") || str2.contains("test-keys")))) {
                            return w2.j;
                        }
                        if (w1.a() && !context3.isDeviceProtectedStorage()) {
                            context3 = context3.createDeviceProtectedStorageContext();
                        }
                        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
                        try {
                            StrictMode.allowThreadDiskWrites();
                            File file = new File(context3.getDir("phenotype_hermetic", 0), "overrides.txt");
                            if (file.exists()) {
                                x2Var = new y2(file);
                            } else {
                                x2Var = w2.j;
                            }
                        } catch (RuntimeException e) {
                            Log.e("HermeticFileOverrides", "no data dir", e);
                            x2Var = w2.j;
                        } finally {
                            StrictMode.setThreadPolicy(allowThreadDiskReads);
                        }
                        if (!x2Var.b()) {
                            return w2.j;
                        }
                        File file2 = (File) x2Var.c();
                        try {
                            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file2)));
                            HashMap hashMap = new HashMap();
                            while (true) {
                                String readLine = bufferedReader.readLine();
                                if (readLine != null) {
                                    String[] split = readLine.split(" ", 3);
                                    if (split.length != 3) {
                                        Log.e("HermeticFileOverrides", readLine.length() != 0 ? "Invalid: ".concat(readLine) : new String("Invalid: "));
                                    } else {
                                        String str3 = split[0];
                                        String decode = Uri.decode(split[1]);
                                        String decode2 = Uri.decode(split[2]);
                                        if (!hashMap.containsKey(str3)) {
                                            hashMap.put(str3, new HashMap());
                                        }
                                        ((Map) hashMap.get(str3)).put(decode, decode2);
                                    }
                                } else {
                                    String valueOf = String.valueOf(file2);
                                    StringBuilder sb = new StringBuilder(valueOf.length() + 7);
                                    sb.append("Parsed ");
                                    sb.append(valueOf);
                                    Log.i("HermeticFileOverrides", sb.toString());
                                    h2 h2Var = new h2(hashMap);
                                    bufferedReader.close();
                                    return new y2(h2Var);
                                }
                            }
                        } catch (IOException e2) {
                            throw new RuntimeException(e2);
                        }
                    }
                }));
                l2.d.incrementAndGet();
            }
        }
        this.o = c.a;
        Long l = y5Var.i;
        if (l != null) {
            j = l.longValue();
        } else {
            j = System.currentTimeMillis();
        }
        this.H = j;
        this.h = new c(this);
        d4 d4Var = new d4(this);
        d4Var.p();
        this.i = d4Var;
        q3 q3Var = new q3(this);
        q3Var.p();
        this.j = q3Var;
        t9 t9Var = new t9(this);
        t9Var.p();
        this.m = t9Var;
        o3 o3Var = new o3(this);
        o3Var.p();
        this.n = o3Var;
        this.r = new a(this);
        h7 h7Var = new h7(this);
        h7Var.u();
        this.p = h7Var;
        c6 c6Var = new c6(this);
        c6Var.u();
        this.q = c6Var;
        w8 w8Var = new w8(this);
        w8Var.u();
        this.l = w8Var;
        d7 d7Var = new d7(this);
        d7Var.p();
        this.f1567s = d7Var;
        r4 r4Var = new r4(this);
        r4Var.p();
        this.k = r4Var;
        zzae zzaeVar2 = y5Var.g;
        if (!(zzaeVar2 == null || zzaeVar2.k == 0)) {
            z2 = true;
        }
        boolean z3 = !z2;
        if (context2.getApplicationContext() instanceof Application) {
            c6 s2 = s();
            if (s2.a.f1566b.getApplicationContext() instanceof Application) {
                Application application = (Application) s2.a.f1566b.getApplicationContext();
                if (s2.c == null) {
                    s2.c = new y6(s2, null);
                }
                if (z3) {
                    application.unregisterActivityLifecycleCallbacks(s2.c);
                    application.registerActivityLifecycleCallbacks(s2.c);
                    s2.g().n.a("Registered activity lifecycle callback");
                }
            }
        } else {
            g().i.a("Application context is not an Application");
        }
        r4Var.v(new w4(this, y5Var));
    }

    public static u4 b(Context context, zzae zzaeVar, Long l) {
        Bundle bundle;
        if (zzaeVar != null && (zzaeVar.n == null || zzaeVar.o == null)) {
            zzaeVar = new zzae(zzaeVar.j, zzaeVar.k, zzaeVar.l, zzaeVar.m, null, null, zzaeVar.p);
        }
        Objects.requireNonNull(context, "null reference");
        Objects.requireNonNull(context.getApplicationContext(), "null reference");
        if (a == null) {
            synchronized (u4.class) {
                if (a == null) {
                    a = new u4(new y5(context, zzaeVar, l));
                }
            }
        } else if (!(zzaeVar == null || (bundle = zzaeVar.p) == null || !bundle.containsKey("dataCollectionDefaultEnabled"))) {
            a.B = Boolean.valueOf(zzaeVar.p.getBoolean("dataCollectionDefaultEnabled"));
        }
        return a;
    }

    public static void c(s5 s5Var) {
        if (s5Var == null) {
            throw new IllegalStateException("Component not created");
        }
    }

    public static void p(a5 a5Var) {
        if (a5Var == null) {
            throw new IllegalStateException("Component not created");
        } else if (!a5Var.f1514b) {
            String valueOf = String.valueOf(a5Var.getClass());
            throw new IllegalStateException(a.i(valueOf.length() + 27, "Component not initialized: ", valueOf));
        }
    }

    public static void q(r5 r5Var) {
        if (r5Var == null) {
            throw new IllegalStateException("Component not created");
        } else if (!r5Var.n()) {
            String valueOf = String.valueOf(r5Var.getClass());
            throw new IllegalStateException(a.i(valueOf.length() + 27, "Component not initialized: ", valueOf));
        }
    }

    public final a A() {
        a aVar = this.r;
        if (aVar != null) {
            return aVar;
        }
        throw new IllegalStateException("Component not created");
    }

    @WorkerThread
    public final boolean B() {
        return this.B != null && this.B.booleanValue();
    }

    public final c a() {
        return this.h;
    }

    @WorkerThread
    public final boolean d() {
        return e() == 0;
    }

    @WorkerThread
    public final int e() {
        f().b();
        if (this.h.x()) {
            return 1;
        }
        Boolean bool = this.D;
        if (bool != null && bool.booleanValue()) {
            return 2;
        }
        if (t8.b() && this.h.o(p.H0) && !h()) {
            return 8;
        }
        Boolean x2 = o().x();
        if (x2 != null) {
            return x2.booleanValue() ? 0 : 3;
        }
        Boolean w = this.h.w("firebase_analytics_collection_enabled");
        if (w != null) {
            return w.booleanValue() ? 0 : 4;
        }
        Boolean bool2 = this.C;
        if (bool2 != null) {
            return bool2.booleanValue() ? 0 : 5;
        }
        if (h.a("isMeasurementExplicitlyDisabled").e) {
            return 6;
        }
        return (!this.h.o(p.S) || this.B == null || this.B.booleanValue()) ? 0 : 7;
    }

    @Override // b.i.a.f.i.b.t5
    public final r4 f() {
        q(this.k);
        return this.k;
    }

    @Override // b.i.a.f.i.b.t5
    public final q3 g() {
        q(this.j);
        return this.j;
    }

    @WorkerThread
    public final boolean h() {
        f().b();
        return this.E;
    }

    @Override // b.i.a.f.i.b.t5
    public final b i() {
        return this.o;
    }

    @Override // b.i.a.f.i.b.t5
    public final Context j() {
        return this.f1566b;
    }

    @Override // b.i.a.f.i.b.t5
    public final ga k() {
        return this.g;
    }

    public final void l() {
        throw new IllegalStateException("Unexpected call on client side");
    }

    /* JADX WARN: Code restructure failed: missing block: B:11:0x0033, code lost:
        if (java.lang.Math.abs(android.os.SystemClock.elapsedRealtime() - r6.A) > 1000) goto L12;
     */
    /* JADX WARN: Code restructure failed: missing block: B:31:0x00c1, code lost:
        if (android.text.TextUtils.isEmpty(r0.l) == false) goto L32;
     */
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final boolean m() {
        /*
            r6 = this;
            boolean r0 = r6.f1569y
            if (r0 == 0) goto Ld1
            b.i.a.f.i.b.r4 r0 = r6.f()
            r0.b()
            java.lang.Boolean r0 = r6.f1570z
            if (r0 == 0) goto L35
            long r1 = r6.A
            r3 = 0
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L35
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto Lca
            b.i.a.f.e.o.b r0 = r6.o
            b.i.a.f.e.o.c r0 = (b.i.a.f.e.o.c) r0
            java.util.Objects.requireNonNull(r0)
            long r0 = android.os.SystemClock.elapsedRealtime()
            long r2 = r6.A
            long r0 = r0 - r2
            long r0 = java.lang.Math.abs(r0)
            r2 = 1000(0x3e8, double:4.94E-321)
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 <= 0) goto Lca
        L35:
            b.i.a.f.e.o.b r0 = r6.o
            b.i.a.f.e.o.c r0 = (b.i.a.f.e.o.c) r0
            java.util.Objects.requireNonNull(r0)
            long r0 = android.os.SystemClock.elapsedRealtime()
            r6.A = r0
            b.i.a.f.i.b.t9 r0 = r6.t()
            java.lang.String r1 = "android.permission.INTERNET"
            boolean r0 = r0.p0(r1)
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L82
            b.i.a.f.i.b.t9 r0 = r6.t()
            java.lang.String r3 = "android.permission.ACCESS_NETWORK_STATE"
            boolean r0 = r0.p0(r3)
            if (r0 == 0) goto L82
            android.content.Context r0 = r6.f1566b
            b.i.a.f.e.p.a r0 = b.i.a.f.e.p.b.a(r0)
            boolean r0 = r0.c()
            if (r0 != 0) goto L80
            b.i.a.f.i.b.c r0 = r6.h
            boolean r0 = r0.C()
            if (r0 != 0) goto L80
            android.content.Context r0 = r6.f1566b
            boolean r0 = b.i.a.f.i.b.n4.a(r0)
            if (r0 == 0) goto L82
            android.content.Context r0 = r6.f1566b
            boolean r0 = b.i.a.f.i.b.t9.U(r0)
            if (r0 == 0) goto L82
        L80:
            r0 = 1
            goto L83
        L82:
            r0 = 0
        L83:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r6.f1570z = r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto Lca
            b.i.a.f.i.b.t9 r0 = r6.t()
            b.i.a.f.i.b.n3 r3 = r6.z()
            r3.t()
            java.lang.String r3 = r3.k
            b.i.a.f.i.b.n3 r4 = r6.z()
            r4.t()
            java.lang.String r4 = r4.l
            b.i.a.f.i.b.n3 r5 = r6.z()
            r5.t()
            java.lang.String r5 = r5.m
            boolean r0 = r0.c0(r3, r4, r5)
            if (r0 != 0) goto Lc3
            b.i.a.f.i.b.n3 r0 = r6.z()
            r0.t()
            java.lang.String r0 = r0.l
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto Lc4
        Lc3:
            r1 = 1
        Lc4:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r1)
            r6.f1570z = r0
        Lca:
            java.lang.Boolean r0 = r6.f1570z
            boolean r0 = r0.booleanValue()
            return r0
        Ld1:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "AppMeasurement is not initialized"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.u4.m():boolean");
    }

    public final d7 n() {
        q(this.f1567s);
        return this.f1567s;
    }

    public final d4 o() {
        c(this.i);
        return this.i;
    }

    public final w8 r() {
        p(this.l);
        return this.l;
    }

    public final c6 s() {
        p(this.q);
        return this.q;
    }

    public final t9 t() {
        c(this.m);
        return this.m;
    }

    public final o3 u() {
        c(this.n);
        return this.n;
    }

    public final boolean v() {
        return TextUtils.isEmpty(this.c);
    }

    public final h7 w() {
        p(this.p);
        return this.p;
    }

    public final q7 x() {
        p(this.u);
        return this.u;
    }

    public final j y() {
        q(this.v);
        return this.v;
    }

    public final n3 z() {
        p(this.w);
        return this.w;
    }
}
