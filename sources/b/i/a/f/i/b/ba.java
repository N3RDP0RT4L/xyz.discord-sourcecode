package b.i.a.f.i.b;

import java.util.Map;
import java.util.Set;
/* compiled from: com.google.android.gms:play-services-measurement@@18.0.0 */
/* loaded from: classes3.dex */
public final class ba extends i9 {
    public String d;
    public Set<Integer> e;
    public Map<Integer, da> f;
    public Long g;
    public Long h;

    public ba(k9 k9Var) {
        super(k9Var);
    }

    @Override // b.i.a.f.i.b.i9
    public final boolean p() {
        return false;
    }

    public final da s(int i) {
        if (this.f.containsKey(Integer.valueOf(i))) {
            return this.f.get(Integer.valueOf(i));
        }
        da daVar = new da(this, this.d, null);
        this.f.put(Integer.valueOf(i), daVar);
        return daVar;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: Unreachable block: B:135:0x0318
        	at jadx.core.dex.visitors.blocks.BlockProcessor.checkForUnreachableBlocks(BlockProcessor.java:92)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:52)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:44)
        */
    @androidx.annotation.WorkerThread
    public final java.util.List<b.i.a.f.h.l.y0> t(java.lang.String r63, java.util.List<b.i.a.f.h.l.a1> r64, java.util.List<b.i.a.f.h.l.i1> r65, java.lang.Long r66, java.lang.Long r67) {
        /*
            Method dump skipped, instructions count: 2532
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.ba.t(java.lang.String, java.util.List, java.util.List, java.lang.Long, java.lang.Long):java.util.List");
    }

    public final boolean u(int i, int i2) {
        if (this.f.get(Integer.valueOf(i)) == null) {
            return false;
        }
        return this.f.get(Integer.valueOf(i)).d.get(i2);
    }
}
