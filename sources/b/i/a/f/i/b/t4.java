package b.i.a.f.i.b;

import java.lang.Thread;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class t4 implements Thread.UncaughtExceptionHandler {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ r4 f1564b;

    public t4(r4 r4Var, String str) {
        this.f1564b = r4Var;
        this.a = str;
    }

    @Override // java.lang.Thread.UncaughtExceptionHandler
    public final synchronized void uncaughtException(Thread thread, Throwable th) {
        this.f1564b.g().f.b(this.a, th);
    }
}
