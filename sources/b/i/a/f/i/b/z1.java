package b.i.a.f.i.b;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public class z1 extends s5 implements t5 {
    public z1(u4 u4Var) {
        super(u4Var);
    }

    @Override // b.i.a.f.i.b.s5
    public void a() {
        this.a.f().a();
    }

    @Override // b.i.a.f.i.b.s5
    public void b() {
        this.a.f().b();
    }

    public a m() {
        return this.a.A();
    }

    public c6 n() {
        return this.a.s();
    }

    public n3 o() {
        return this.a.z();
    }

    public q7 p() {
        return this.a.x();
    }

    public h7 q() {
        return this.a.w();
    }

    public m3 r() {
        u4 u4Var = this.a;
        u4.p(u4Var.t);
        return u4Var.t;
    }

    public w8 s() {
        return this.a.r();
    }
}
