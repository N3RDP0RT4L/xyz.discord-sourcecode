package b.i.a.f.i.b;

import android.os.SystemClock;
import android.util.Pair;
import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;
import b.i.a.f.e.o.c;
import b.i.a.f.h.l.t8;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Locale;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-measurement@@18.0.0 */
/* loaded from: classes3.dex */
public final class q8 extends i9 {
    public String d;
    public boolean e;
    public long f;

    public q8(k9 k9Var) {
        super(k9Var);
    }

    @Override // b.i.a.f.i.b.i9
    public final boolean p() {
        return false;
    }

    @NonNull
    @WorkerThread
    public final Pair<String, Boolean> s(String str, d dVar) {
        if (!t8.b() || !this.a.h.o(p.J0) || dVar.j()) {
            return u(str);
        }
        return new Pair<>("", Boolean.FALSE);
    }

    @WorkerThread
    @Deprecated
    public final String t(String str) {
        b();
        String str2 = (String) u(str).first;
        MessageDigest x0 = t9.x0();
        if (x0 == null) {
            return null;
        }
        return String.format(Locale.US, "%032X", new BigInteger(1, x0.digest(str2.getBytes())));
    }

    @NonNull
    @WorkerThread
    @Deprecated
    public final Pair<String, Boolean> u(String str) {
        b();
        Objects.requireNonNull((c) this.a.o);
        long elapsedRealtime = SystemClock.elapsedRealtime();
        if (this.d != null && elapsedRealtime < this.f) {
            return new Pair<>(this.d, Boolean.valueOf(this.e));
        }
        c cVar = this.a.h;
        Objects.requireNonNull(cVar);
        this.f = elapsedRealtime + cVar.n(str, p.f1548b);
        try {
            AdvertisingIdClient.Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(this.a.f1566b);
            if (advertisingIdInfo != null) {
                this.d = advertisingIdInfo.getId();
                this.e = advertisingIdInfo.isLimitAdTrackingEnabled();
            }
            if (this.d == null) {
                this.d = "";
            }
        } catch (Exception e) {
            g().m.b("Unable to get advertising id", e);
            this.d = "";
        }
        return new Pair<>(this.d, Boolean.valueOf(this.e));
    }
}
