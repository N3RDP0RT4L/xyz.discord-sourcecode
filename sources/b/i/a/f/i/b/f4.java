package b.i.a.f.i.b;

import android.content.SharedPreferences;
import androidx.annotation.WorkerThread;
import b.c.a.a0.d;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class f4 {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f1529b;
    public boolean c;
    public boolean d;
    public final /* synthetic */ d4 e;

    public f4(d4 d4Var, String str, boolean z2) {
        this.e = d4Var;
        d.w(str);
        this.a = str;
        this.f1529b = z2;
    }

    @WorkerThread
    public final void a(boolean z2) {
        SharedPreferences.Editor edit = this.e.w().edit();
        edit.putBoolean(this.a, z2);
        edit.apply();
        this.d = z2;
    }

    @WorkerThread
    public final boolean b() {
        if (!this.c) {
            this.c = true;
            this.d = this.e.w().getBoolean(this.a, this.f1529b);
        }
        return this.d;
    }
}
