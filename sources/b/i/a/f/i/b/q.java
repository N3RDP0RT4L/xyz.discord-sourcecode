package b.i.a.f.i.b;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.measurement.internal.zzap;
import com.google.android.gms.measurement.internal.zzaq;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class q implements Parcelable.Creator<zzaq> {
    @Override // android.os.Parcelable.Creator
    public final zzaq createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        String str = null;
        long j = 0;
        zzap zzapVar = null;
        String str2 = null;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 2) {
                str = d.R(parcel, readInt);
            } else if (c == 3) {
                zzapVar = (zzap) d.Q(parcel, readInt, zzap.CREATOR);
            } else if (c == 4) {
                str2 = d.R(parcel, readInt);
            } else if (c != 5) {
                d.d2(parcel, readInt);
            } else {
                j = d.H1(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        return new zzaq(str, zzapVar, str2, j);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzaq[] newArray(int i) {
        return new zzaq[i];
    }
}
