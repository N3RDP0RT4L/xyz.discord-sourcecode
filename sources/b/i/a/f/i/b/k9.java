package b.i.a.f.i.b;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;
import b.c.a.a0.d;
import b.i.a.f.e.o.b;
import b.i.a.f.e.o.c;
import b.i.a.f.h.l.a1;
import b.i.a.f.h.l.c1;
import b.i.a.f.h.l.da;
import b.i.a.f.h.l.e1;
import b.i.a.f.h.l.ea;
import b.i.a.f.h.l.i1;
import b.i.a.f.h.l.t8;
import b.i.a.f.h.l.u4;
import b.i.a.f.h.l.x7;
import com.google.android.gms.measurement.internal.zzap;
import com.google.android.gms.measurement.internal.zzaq;
import com.google.android.gms.measurement.internal.zzku;
import com.google.android.gms.measurement.internal.zzn;
import com.google.android.gms.measurement.internal.zzz;
import java.math.BigInteger;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-measurement@@18.0.0 */
/* loaded from: classes3.dex */
public class k9 implements t5 {
    public static volatile k9 a;

    /* renamed from: b  reason: collision with root package name */
    public p4 f1538b;
    public x3 c;
    public g d;
    public b4 e;
    public h9 f;
    public ba g;
    public final q9 h;
    public g7 i;
    public q8 j;
    public final u4 k;
    public boolean m;
    public long n;
    public List<Runnable> o;
    public int p;
    public int q;
    public boolean r;

    /* renamed from: s  reason: collision with root package name */
    public boolean f1539s;
    public boolean t;
    public FileLock u;
    public FileChannel v;
    public List<Long> w;

    /* renamed from: x  reason: collision with root package name */
    public List<Long> f1540x;
    public boolean l = false;
    public final v9 A = new n9(this);

    /* renamed from: y  reason: collision with root package name */
    public long f1541y = -1;

    /* renamed from: z  reason: collision with root package name */
    public final Map<String, d> f1542z = new HashMap();

    /* compiled from: com.google.android.gms:play-services-measurement@@18.0.0 */
    /* loaded from: classes3.dex */
    public class a {
        public e1 a;

        /* renamed from: b  reason: collision with root package name */
        public List<Long> f1543b;
        public List<a1> c;
        public long d;

        public a(k9 k9Var, j9 j9Var) {
        }

        public final void a(e1 e1Var) {
            this.a = e1Var;
        }

        public final boolean b(long j, a1 a1Var) {
            if (this.c == null) {
                this.c = new ArrayList();
            }
            if (this.f1543b == null) {
                this.f1543b = new ArrayList();
            }
            if (this.c.size() > 0 && ((this.c.get(0).H() / 1000) / 60) / 60 != ((a1Var.H() / 1000) / 60) / 60) {
                return false;
            }
            long g = this.d + a1Var.g();
            if (g >= Math.max(0, p.i.a(null).intValue())) {
                return false;
            }
            this.d = g;
            this.c.add(a1Var);
            this.f1543b.add(Long.valueOf(j));
            return this.c.size() < Math.max(1, p.j.a(null).intValue());
        }
    }

    public k9(r9 r9Var) {
        u4 b2 = u4.b(r9Var.a, null, null);
        this.k = b2;
        q9 q9Var = new q9(this);
        q9Var.o();
        this.h = q9Var;
        x3 x3Var = new x3(this);
        x3Var.o();
        this.c = x3Var;
        p4 p4Var = new p4(this);
        p4Var.o();
        this.f1538b = p4Var;
        b2.f().v(new j9(this, r9Var));
    }

    public static void C(i9 i9Var) {
        if (i9Var == null) {
            throw new IllegalStateException("Upload Component not created");
        } else if (!i9Var.c) {
            String valueOf = String.valueOf(i9Var.getClass());
            throw new IllegalStateException(b.d.b.a.a.i(valueOf.length() + 27, "Component not initialized: ", valueOf));
        }
    }

    public static k9 b(Context context) {
        Objects.requireNonNull(context, "null reference");
        Objects.requireNonNull(context.getApplicationContext(), "null reference");
        if (a == null) {
            synchronized (k9.class) {
                if (a == null) {
                    a = new k9(new r9(context));
                }
            }
        }
        return a;
    }

    public static void d(a1.a aVar, int i, String str) {
        List<c1> v = aVar.v();
        for (int i2 = 0; i2 < v.size(); i2++) {
            if ("_err".equals(v.get(i2).B())) {
                return;
            }
        }
        c1.a Q = c1.Q();
        Q.s("_err");
        Q.r(Long.valueOf(i).longValue());
        c1 c1Var = (c1) ((u4) Q.p());
        c1.a Q2 = c1.Q();
        Q2.s("_ev");
        Q2.t(str);
        c1 c1Var2 = (c1) ((u4) Q2.p());
        if (aVar.l) {
            aVar.n();
            aVar.l = false;
        }
        a1.A((a1) aVar.k, c1Var);
        if (aVar.l) {
            aVar.n();
            aVar.l = false;
        }
        a1.A((a1) aVar.k, c1Var2);
    }

    public static void e(a1.a aVar, @NonNull String str) {
        List<c1> v = aVar.v();
        for (int i = 0; i < v.size(); i++) {
            if (str.equals(v.get(i).B())) {
                aVar.x(i);
                return;
            }
        }
    }

    public final void A(a1.a aVar, a1.a aVar2) {
        d.l("_e".equals(aVar.y()));
        N();
        c1 w = q9.w((a1) ((u4) aVar.p()), "_et");
        if (w.I() && w.J() > 0) {
            long J = w.J();
            N();
            c1 w2 = q9.w((a1) ((u4) aVar2.p()), "_et");
            if (w2 != null && w2.J() > 0) {
                J += w2.J();
            }
            N();
            q9.E(aVar2, "_et", Long.valueOf(J));
            N();
            q9.E(aVar, "_fr", 1L);
        }
    }

    @WorkerThread
    public final void B(zzaq zzaqVar, zzn zznVar) {
        if (ea.b() && this.k.h.o(p.A0)) {
            u3 b2 = u3.b(zzaqVar);
            this.k.t().H(b2.d, K().h0(zznVar.j));
            this.k.t().Q(b2, this.k.h.m(zznVar.j));
            zzaqVar = b2.a();
        }
        if (this.k.h.o(p.f1552e0) && "_cmp".equals(zzaqVar.j) && "referrer API v2".equals(zzaqVar.k.j.getString("_cis"))) {
            String string = zzaqVar.k.j.getString("gclid");
            if (!TextUtils.isEmpty(string)) {
                o(new zzku("_lgclid", zzaqVar.m, string, "auto"), zznVar);
            }
        }
        l(zzaqVar, zznVar);
    }

    @WorkerThread
    public final void D(zzku zzkuVar, zzn zznVar) {
        U();
        P();
        if (L(zznVar)) {
            if (!zznVar.q) {
                G(zznVar);
            } else if (!"_npa".equals(zzkuVar.k) || zznVar.B == null) {
                this.k.g().m.b("Removing user property", this.k.u().y(zzkuVar.k));
                K().b0();
                try {
                    G(zznVar);
                    K().V(zznVar.j, zzkuVar.k);
                    K().s();
                    this.k.g().m.b("User property removed", this.k.u().y(zzkuVar.k));
                } finally {
                    K().e0();
                }
            } else {
                this.k.g().m.a("Falling back to manifest metadata value for ad personalization");
                Objects.requireNonNull((c) this.k.o);
                o(new zzku("_npa", System.currentTimeMillis(), Long.valueOf(zznVar.B.booleanValue() ? 1L : 0L), "auto"), zznVar);
            }
        }
    }

    /* JADX WARN: Can't wrap try/catch for region: R(7:158|97|(2:101|(8:103|(3:105|(2:107|(1:109))(1:110)|111)(1:112)|113|(1:115)(1:116)|117|164|119|(4:123|(1:125)|126|(1:128))))|118|164|119|(0)) */
    /* JADX WARN: Code restructure failed: missing block: B:120:0x039d, code lost:
        r0 = move-exception;
     */
    /* JADX WARN: Code restructure failed: missing block: B:121:0x039e, code lost:
        r22.k.g().f.c("Application info is null, first open report might be inaccurate. appId", b.i.a.f.i.b.q3.s(r23.j), r0);
        r14 = r18;
     */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:123:0x03b5 A[Catch: all -> 0x04b4, TryCatch #2 {all -> 0x04b4, blocks: (B:24:0x00ad, B:26:0x00bb, B:30:0x00c9, B:32:0x00cd, B:36:0x00dc, B:38:0x00f6, B:40:0x0100, B:43:0x0109, B:44:0x0119, B:46:0x0125, B:48:0x013c, B:49:0x0162, B:51:0x01b4, B:53:0x01c5, B:56:0x01d8, B:58:0x01e3, B:62:0x01f0, B:64:0x01fb, B:66:0x0201, B:70:0x0210, B:72:0x0213, B:74:0x0238, B:76:0x023d, B:79:0x024d, B:82:0x025e, B:85:0x0273, B:87:0x02cb, B:89:0x02d7, B:91:0x02db, B:92:0x02de, B:94:0x02fd, B:97:0x0317, B:99:0x0328, B:101:0x033f, B:103:0x0347, B:105:0x034f, B:109:0x0361, B:110:0x0367, B:113:0x036f, B:117:0x037a, B:119:0x038d, B:121:0x039e, B:123:0x03b5, B:125:0x03bb, B:126:0x03c0, B:128:0x03c6, B:131:0x03d1, B:132:0x03d4, B:135:0x03f0, B:137:0x0423, B:139:0x042b, B:141:0x042f, B:142:0x0432, B:143:0x0446, B:145:0x0454, B:147:0x046c, B:148:0x0473, B:149:0x0488, B:151:0x048c, B:152:0x04a5), top: B:162:0x00ad, inners: #0, #1, #3 }] */
    /* JADX WARN: Removed duplicated region for block: B:149:0x0488 A[Catch: all -> 0x04b4, TryCatch #2 {all -> 0x04b4, blocks: (B:24:0x00ad, B:26:0x00bb, B:30:0x00c9, B:32:0x00cd, B:36:0x00dc, B:38:0x00f6, B:40:0x0100, B:43:0x0109, B:44:0x0119, B:46:0x0125, B:48:0x013c, B:49:0x0162, B:51:0x01b4, B:53:0x01c5, B:56:0x01d8, B:58:0x01e3, B:62:0x01f0, B:64:0x01fb, B:66:0x0201, B:70:0x0210, B:72:0x0213, B:74:0x0238, B:76:0x023d, B:79:0x024d, B:82:0x025e, B:85:0x0273, B:87:0x02cb, B:89:0x02d7, B:91:0x02db, B:92:0x02de, B:94:0x02fd, B:97:0x0317, B:99:0x0328, B:101:0x033f, B:103:0x0347, B:105:0x034f, B:109:0x0361, B:110:0x0367, B:113:0x036f, B:117:0x037a, B:119:0x038d, B:121:0x039e, B:123:0x03b5, B:125:0x03bb, B:126:0x03c0, B:128:0x03c6, B:131:0x03d1, B:132:0x03d4, B:135:0x03f0, B:137:0x0423, B:139:0x042b, B:141:0x042f, B:142:0x0432, B:143:0x0446, B:145:0x0454, B:147:0x046c, B:148:0x0473, B:149:0x0488, B:151:0x048c, B:152:0x04a5), top: B:162:0x00ad, inners: #0, #1, #3 }] */
    /* JADX WARN: Removed duplicated region for block: B:46:0x0125 A[Catch: all -> 0x04b4, TryCatch #2 {all -> 0x04b4, blocks: (B:24:0x00ad, B:26:0x00bb, B:30:0x00c9, B:32:0x00cd, B:36:0x00dc, B:38:0x00f6, B:40:0x0100, B:43:0x0109, B:44:0x0119, B:46:0x0125, B:48:0x013c, B:49:0x0162, B:51:0x01b4, B:53:0x01c5, B:56:0x01d8, B:58:0x01e3, B:62:0x01f0, B:64:0x01fb, B:66:0x0201, B:70:0x0210, B:72:0x0213, B:74:0x0238, B:76:0x023d, B:79:0x024d, B:82:0x025e, B:85:0x0273, B:87:0x02cb, B:89:0x02d7, B:91:0x02db, B:92:0x02de, B:94:0x02fd, B:97:0x0317, B:99:0x0328, B:101:0x033f, B:103:0x0347, B:105:0x034f, B:109:0x0361, B:110:0x0367, B:113:0x036f, B:117:0x037a, B:119:0x038d, B:121:0x039e, B:123:0x03b5, B:125:0x03bb, B:126:0x03c0, B:128:0x03c6, B:131:0x03d1, B:132:0x03d4, B:135:0x03f0, B:137:0x0423, B:139:0x042b, B:141:0x042f, B:142:0x0432, B:143:0x0446, B:145:0x0454, B:147:0x046c, B:148:0x0473, B:149:0x0488, B:151:0x048c, B:152:0x04a5), top: B:162:0x00ad, inners: #0, #1, #3 }] */
    /* JADX WARN: Removed duplicated region for block: B:56:0x01d8 A[Catch: all -> 0x04b4, TryCatch #2 {all -> 0x04b4, blocks: (B:24:0x00ad, B:26:0x00bb, B:30:0x00c9, B:32:0x00cd, B:36:0x00dc, B:38:0x00f6, B:40:0x0100, B:43:0x0109, B:44:0x0119, B:46:0x0125, B:48:0x013c, B:49:0x0162, B:51:0x01b4, B:53:0x01c5, B:56:0x01d8, B:58:0x01e3, B:62:0x01f0, B:64:0x01fb, B:66:0x0201, B:70:0x0210, B:72:0x0213, B:74:0x0238, B:76:0x023d, B:79:0x024d, B:82:0x025e, B:85:0x0273, B:87:0x02cb, B:89:0x02d7, B:91:0x02db, B:92:0x02de, B:94:0x02fd, B:97:0x0317, B:99:0x0328, B:101:0x033f, B:103:0x0347, B:105:0x034f, B:109:0x0361, B:110:0x0367, B:113:0x036f, B:117:0x037a, B:119:0x038d, B:121:0x039e, B:123:0x03b5, B:125:0x03bb, B:126:0x03c0, B:128:0x03c6, B:131:0x03d1, B:132:0x03d4, B:135:0x03f0, B:137:0x0423, B:139:0x042b, B:141:0x042f, B:142:0x0432, B:143:0x0446, B:145:0x0454, B:147:0x046c, B:148:0x0473, B:149:0x0488, B:151:0x048c, B:152:0x04a5), top: B:162:0x00ad, inners: #0, #1, #3 }] */
    /* JADX WARN: Removed duplicated region for block: B:76:0x023d A[Catch: all -> 0x04b4, TryCatch #2 {all -> 0x04b4, blocks: (B:24:0x00ad, B:26:0x00bb, B:30:0x00c9, B:32:0x00cd, B:36:0x00dc, B:38:0x00f6, B:40:0x0100, B:43:0x0109, B:44:0x0119, B:46:0x0125, B:48:0x013c, B:49:0x0162, B:51:0x01b4, B:53:0x01c5, B:56:0x01d8, B:58:0x01e3, B:62:0x01f0, B:64:0x01fb, B:66:0x0201, B:70:0x0210, B:72:0x0213, B:74:0x0238, B:76:0x023d, B:79:0x024d, B:82:0x025e, B:85:0x0273, B:87:0x02cb, B:89:0x02d7, B:91:0x02db, B:92:0x02de, B:94:0x02fd, B:97:0x0317, B:99:0x0328, B:101:0x033f, B:103:0x0347, B:105:0x034f, B:109:0x0361, B:110:0x0367, B:113:0x036f, B:117:0x037a, B:119:0x038d, B:121:0x039e, B:123:0x03b5, B:125:0x03bb, B:126:0x03c0, B:128:0x03c6, B:131:0x03d1, B:132:0x03d4, B:135:0x03f0, B:137:0x0423, B:139:0x042b, B:141:0x042f, B:142:0x0432, B:143:0x0446, B:145:0x0454, B:147:0x046c, B:148:0x0473, B:149:0x0488, B:151:0x048c, B:152:0x04a5), top: B:162:0x00ad, inners: #0, #1, #3 }] */
    /* JADX WARN: Removed duplicated region for block: B:77:0x024a  */
    /* JADX WARN: Removed duplicated region for block: B:82:0x025e A[Catch: all -> 0x04b4, TRY_LEAVE, TryCatch #2 {all -> 0x04b4, blocks: (B:24:0x00ad, B:26:0x00bb, B:30:0x00c9, B:32:0x00cd, B:36:0x00dc, B:38:0x00f6, B:40:0x0100, B:43:0x0109, B:44:0x0119, B:46:0x0125, B:48:0x013c, B:49:0x0162, B:51:0x01b4, B:53:0x01c5, B:56:0x01d8, B:58:0x01e3, B:62:0x01f0, B:64:0x01fb, B:66:0x0201, B:70:0x0210, B:72:0x0213, B:74:0x0238, B:76:0x023d, B:79:0x024d, B:82:0x025e, B:85:0x0273, B:87:0x02cb, B:89:0x02d7, B:91:0x02db, B:92:0x02de, B:94:0x02fd, B:97:0x0317, B:99:0x0328, B:101:0x033f, B:103:0x0347, B:105:0x034f, B:109:0x0361, B:110:0x0367, B:113:0x036f, B:117:0x037a, B:119:0x038d, B:121:0x039e, B:123:0x03b5, B:125:0x03bb, B:126:0x03c0, B:128:0x03c6, B:131:0x03d1, B:132:0x03d4, B:135:0x03f0, B:137:0x0423, B:139:0x042b, B:141:0x042f, B:142:0x0432, B:143:0x0446, B:145:0x0454, B:147:0x046c, B:148:0x0473, B:149:0x0488, B:151:0x048c, B:152:0x04a5), top: B:162:0x00ad, inners: #0, #1, #3 }] */
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void E(com.google.android.gms.measurement.internal.zzn r23) {
        /*
            Method dump skipped, instructions count: 1213
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.k9.E(com.google.android.gms.measurement.internal.zzn):void");
    }

    @WorkerThread
    public final void F(zzz zzzVar, zzn zznVar) {
        Objects.requireNonNull(zzzVar, "null reference");
        d.w(zzzVar.j);
        Objects.requireNonNull(zzzVar.l, "null reference");
        d.w(zzzVar.l.k);
        U();
        P();
        if (L(zznVar)) {
            if (!zznVar.q) {
                G(zznVar);
                return;
            }
            K().b0();
            try {
                G(zznVar);
                zzz Z = K().Z(zzzVar.j, zzzVar.l.k);
                if (Z != null) {
                    this.k.g().m.c("Removing conditional user property", zzzVar.j, this.k.u().y(zzzVar.l.k));
                    K().a0(zzzVar.j, zzzVar.l.k);
                    if (Z.n) {
                        K().V(zzzVar.j, zzzVar.l.k);
                    }
                    zzaq zzaqVar = zzzVar.t;
                    if (zzaqVar != null) {
                        Bundle bundle = null;
                        zzap zzapVar = zzaqVar.k;
                        if (zzapVar != null) {
                            bundle = zzapVar.x0();
                        }
                        Bundle bundle2 = bundle;
                        t9 t = this.k.t();
                        String str = zzzVar.j;
                        zzaq zzaqVar2 = zzzVar.t;
                        I(t.B(str, zzaqVar2.j, bundle2, Z.k, zzaqVar2.m, true, x7.b() && this.k.h.o(p.M0)), zznVar);
                    }
                } else {
                    this.k.g().i.c("Conditional user property doesn't exist", q3.s(zzzVar.j), this.k.u().y(zzzVar.l.k));
                }
                K().s();
            } finally {
                K().e0();
            }
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:129:0x02bf  */
    /* JADX WARN: Removed duplicated region for block: B:132:0x02d1  */
    /* JADX WARN: Removed duplicated region for block: B:157:0x0351  */
    /* JADX WARN: Removed duplicated region for block: B:165:0x0375  */
    /* JADX WARN: Removed duplicated region for block: B:168:0x0383  */
    /* JADX WARN: Removed duplicated region for block: B:171:0x0391  */
    /* JADX WARN: Removed duplicated region for block: B:181:0x03ce  */
    /* JADX WARN: Removed duplicated region for block: B:184:0x03dc  */
    /* JADX WARN: Removed duplicated region for block: B:187:0x03ea  */
    /* JADX WARN: Removed duplicated region for block: B:195:0x0407  */
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final b.i.a.f.i.b.a4 G(com.google.android.gms.measurement.internal.zzn r11) {
        /*
            Method dump skipped, instructions count: 1039
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.k9.G(com.google.android.gms.measurement.internal.zzn):b.i.a.f.i.b.a4");
    }

    public final p4 H() {
        C(this.f1538b);
        return this.f1538b;
    }

    /* JADX WARN: Can't wrap try/catch for region: R(13:73|(1:75)(1:76)|77|(2:79|(1:81)(6:82|83|90|(1:92)|94|(0)))|84|303|85|89|83|90|(0)|94|(0)) */
    /* JADX WARN: Code restructure failed: missing block: B:289:0x0942, code lost:
        if (r7.e < r26.k.a().s(r4.a)) goto L290;
     */
    /* JADX WARN: Code restructure failed: missing block: B:290:0x0944, code lost:
        r5 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:86:0x0280, code lost:
        r0 = move-exception;
     */
    /* JADX WARN: Code restructure failed: missing block: B:88:0x0282, code lost:
        r7.g().z().c("Error pruning currencies. appId", b.i.a.f.i.b.q3.s(r15), r0);
     */
    /* JADX WARN: Removed duplicated region for block: B:102:0x0352 A[Catch: all -> 0x099c, TryCatch #2 {all -> 0x099c, blocks: (B:37:0x013e, B:40:0x014d, B:42:0x0157, B:47:0x0163, B:54:0x0175, B:57:0x0181, B:59:0x0198, B:64:0x01b1, B:67:0x01bf, B:68:0x01dc, B:69:0x01e6, B:71:0x01ec, B:73:0x01fa, B:75:0x0202, B:76:0x0207, B:77:0x020c, B:79:0x0217, B:82:0x021e, B:84:0x0248, B:85:0x0267, B:88:0x0282, B:89:0x0293, B:90:0x02b0, B:92:0x02ba, B:96:0x02f3, B:100:0x0305, B:102:0x0352, B:104:0x0357, B:105:0x0370, B:109:0x0381, B:111:0x0396, B:113:0x039b, B:114:0x03b4, B:118:0x03d9, B:122:0x03fe, B:123:0x0417, B:126:0x0426, B:129:0x0449, B:130:0x0465, B:132:0x046f, B:134:0x047b, B:136:0x0481, B:137:0x048c, B:139:0x0498, B:140:0x04af, B:142:0x04d7, B:145:0x04f0, B:148:0x0536, B:149:0x0540, B:150:0x054e, B:152:0x0585, B:153:0x058a, B:155:0x0592, B:156:0x0597, B:158:0x059f, B:159:0x05a4, B:161:0x05ad, B:162:0x05b1, B:164:0x05be, B:165:0x05c3, B:167:0x05c9, B:169:0x05d7, B:170:0x05ee, B:172:0x05f4, B:174:0x0604, B:176:0x060e, B:178:0x0616, B:179:0x061b, B:181:0x0625, B:183:0x062f, B:185:0x0637, B:186:0x063d, B:188:0x0647, B:190:0x064f, B:191:0x0654, B:193:0x065c, B:194:0x065f, B:196:0x066e, B:197:0x0671, B:199:0x0687, B:201:0x0695, B:203:0x069b, B:205:0x06ad, B:207:0x06b1, B:209:0x06bc, B:210:0x06c7, B:212:0x06d9, B:214:0x06dd, B:216:0x06e3, B:218:0x06f3, B:220:0x0705, B:221:0x071f, B:223:0x0725, B:224:0x073c, B:225:0x073f, B:227:0x0784, B:228:0x0789, B:230:0x0791, B:232:0x0797, B:234:0x07a5, B:235:0x07a9, B:236:0x07ac, B:239:0x07b4, B:240:0x07b7, B:241:0x07b8, B:243:0x07c4, B:245:0x07d3, B:247:0x07e1, B:248:0x07e9, B:249:0x07f0, B:251:0x0800, B:253:0x080e, B:255:0x0814, B:256:0x081f, B:258:0x0854, B:259:0x0859, B:260:0x0865, B:262:0x086b, B:264:0x0879, B:266:0x087f, B:268:0x0889, B:269:0x0890, B:271:0x089a, B:272:0x08a1, B:273:0x08ac, B:275:0x08b2, B:277:0x08e1, B:278:0x08f1, B:280:0x08f9, B:281:0x08fd, B:283:0x0906, B:286:0x0913, B:288:0x0931, B:292:0x0947, B:294:0x094d, B:296:0x0951, B:297:0x0969), top: B:307:0x013e, inners: #0, #1 }] */
    /* JADX WARN: Removed duplicated region for block: B:108:0x037f  */
    /* JADX WARN: Removed duplicated region for block: B:54:0x0175 A[Catch: all -> 0x099c, TRY_LEAVE, TryCatch #2 {all -> 0x099c, blocks: (B:37:0x013e, B:40:0x014d, B:42:0x0157, B:47:0x0163, B:54:0x0175, B:57:0x0181, B:59:0x0198, B:64:0x01b1, B:67:0x01bf, B:68:0x01dc, B:69:0x01e6, B:71:0x01ec, B:73:0x01fa, B:75:0x0202, B:76:0x0207, B:77:0x020c, B:79:0x0217, B:82:0x021e, B:84:0x0248, B:85:0x0267, B:88:0x0282, B:89:0x0293, B:90:0x02b0, B:92:0x02ba, B:96:0x02f3, B:100:0x0305, B:102:0x0352, B:104:0x0357, B:105:0x0370, B:109:0x0381, B:111:0x0396, B:113:0x039b, B:114:0x03b4, B:118:0x03d9, B:122:0x03fe, B:123:0x0417, B:126:0x0426, B:129:0x0449, B:130:0x0465, B:132:0x046f, B:134:0x047b, B:136:0x0481, B:137:0x048c, B:139:0x0498, B:140:0x04af, B:142:0x04d7, B:145:0x04f0, B:148:0x0536, B:149:0x0540, B:150:0x054e, B:152:0x0585, B:153:0x058a, B:155:0x0592, B:156:0x0597, B:158:0x059f, B:159:0x05a4, B:161:0x05ad, B:162:0x05b1, B:164:0x05be, B:165:0x05c3, B:167:0x05c9, B:169:0x05d7, B:170:0x05ee, B:172:0x05f4, B:174:0x0604, B:176:0x060e, B:178:0x0616, B:179:0x061b, B:181:0x0625, B:183:0x062f, B:185:0x0637, B:186:0x063d, B:188:0x0647, B:190:0x064f, B:191:0x0654, B:193:0x065c, B:194:0x065f, B:196:0x066e, B:197:0x0671, B:199:0x0687, B:201:0x0695, B:203:0x069b, B:205:0x06ad, B:207:0x06b1, B:209:0x06bc, B:210:0x06c7, B:212:0x06d9, B:214:0x06dd, B:216:0x06e3, B:218:0x06f3, B:220:0x0705, B:221:0x071f, B:223:0x0725, B:224:0x073c, B:225:0x073f, B:227:0x0784, B:228:0x0789, B:230:0x0791, B:232:0x0797, B:234:0x07a5, B:235:0x07a9, B:236:0x07ac, B:239:0x07b4, B:240:0x07b7, B:241:0x07b8, B:243:0x07c4, B:245:0x07d3, B:247:0x07e1, B:248:0x07e9, B:249:0x07f0, B:251:0x0800, B:253:0x080e, B:255:0x0814, B:256:0x081f, B:258:0x0854, B:259:0x0859, B:260:0x0865, B:262:0x086b, B:264:0x0879, B:266:0x087f, B:268:0x0889, B:269:0x0890, B:271:0x089a, B:272:0x08a1, B:273:0x08ac, B:275:0x08b2, B:277:0x08e1, B:278:0x08f1, B:280:0x08f9, B:281:0x08fd, B:283:0x0906, B:286:0x0913, B:288:0x0931, B:292:0x0947, B:294:0x094d, B:296:0x0951, B:297:0x0969), top: B:307:0x013e, inners: #0, #1 }] */
    /* JADX WARN: Removed duplicated region for block: B:92:0x02ba A[Catch: all -> 0x099c, TryCatch #2 {all -> 0x099c, blocks: (B:37:0x013e, B:40:0x014d, B:42:0x0157, B:47:0x0163, B:54:0x0175, B:57:0x0181, B:59:0x0198, B:64:0x01b1, B:67:0x01bf, B:68:0x01dc, B:69:0x01e6, B:71:0x01ec, B:73:0x01fa, B:75:0x0202, B:76:0x0207, B:77:0x020c, B:79:0x0217, B:82:0x021e, B:84:0x0248, B:85:0x0267, B:88:0x0282, B:89:0x0293, B:90:0x02b0, B:92:0x02ba, B:96:0x02f3, B:100:0x0305, B:102:0x0352, B:104:0x0357, B:105:0x0370, B:109:0x0381, B:111:0x0396, B:113:0x039b, B:114:0x03b4, B:118:0x03d9, B:122:0x03fe, B:123:0x0417, B:126:0x0426, B:129:0x0449, B:130:0x0465, B:132:0x046f, B:134:0x047b, B:136:0x0481, B:137:0x048c, B:139:0x0498, B:140:0x04af, B:142:0x04d7, B:145:0x04f0, B:148:0x0536, B:149:0x0540, B:150:0x054e, B:152:0x0585, B:153:0x058a, B:155:0x0592, B:156:0x0597, B:158:0x059f, B:159:0x05a4, B:161:0x05ad, B:162:0x05b1, B:164:0x05be, B:165:0x05c3, B:167:0x05c9, B:169:0x05d7, B:170:0x05ee, B:172:0x05f4, B:174:0x0604, B:176:0x060e, B:178:0x0616, B:179:0x061b, B:181:0x0625, B:183:0x062f, B:185:0x0637, B:186:0x063d, B:188:0x0647, B:190:0x064f, B:191:0x0654, B:193:0x065c, B:194:0x065f, B:196:0x066e, B:197:0x0671, B:199:0x0687, B:201:0x0695, B:203:0x069b, B:205:0x06ad, B:207:0x06b1, B:209:0x06bc, B:210:0x06c7, B:212:0x06d9, B:214:0x06dd, B:216:0x06e3, B:218:0x06f3, B:220:0x0705, B:221:0x071f, B:223:0x0725, B:224:0x073c, B:225:0x073f, B:227:0x0784, B:228:0x0789, B:230:0x0791, B:232:0x0797, B:234:0x07a5, B:235:0x07a9, B:236:0x07ac, B:239:0x07b4, B:240:0x07b7, B:241:0x07b8, B:243:0x07c4, B:245:0x07d3, B:247:0x07e1, B:248:0x07e9, B:249:0x07f0, B:251:0x0800, B:253:0x080e, B:255:0x0814, B:256:0x081f, B:258:0x0854, B:259:0x0859, B:260:0x0865, B:262:0x086b, B:264:0x0879, B:266:0x087f, B:268:0x0889, B:269:0x0890, B:271:0x089a, B:272:0x08a1, B:273:0x08ac, B:275:0x08b2, B:277:0x08e1, B:278:0x08f1, B:280:0x08f9, B:281:0x08fd, B:283:0x0906, B:286:0x0913, B:288:0x0931, B:292:0x0947, B:294:0x094d, B:296:0x0951, B:297:0x0969), top: B:307:0x013e, inners: #0, #1 }] */
    /* JADX WARN: Removed duplicated region for block: B:96:0x02f3 A[Catch: all -> 0x099c, TRY_LEAVE, TryCatch #2 {all -> 0x099c, blocks: (B:37:0x013e, B:40:0x014d, B:42:0x0157, B:47:0x0163, B:54:0x0175, B:57:0x0181, B:59:0x0198, B:64:0x01b1, B:67:0x01bf, B:68:0x01dc, B:69:0x01e6, B:71:0x01ec, B:73:0x01fa, B:75:0x0202, B:76:0x0207, B:77:0x020c, B:79:0x0217, B:82:0x021e, B:84:0x0248, B:85:0x0267, B:88:0x0282, B:89:0x0293, B:90:0x02b0, B:92:0x02ba, B:96:0x02f3, B:100:0x0305, B:102:0x0352, B:104:0x0357, B:105:0x0370, B:109:0x0381, B:111:0x0396, B:113:0x039b, B:114:0x03b4, B:118:0x03d9, B:122:0x03fe, B:123:0x0417, B:126:0x0426, B:129:0x0449, B:130:0x0465, B:132:0x046f, B:134:0x047b, B:136:0x0481, B:137:0x048c, B:139:0x0498, B:140:0x04af, B:142:0x04d7, B:145:0x04f0, B:148:0x0536, B:149:0x0540, B:150:0x054e, B:152:0x0585, B:153:0x058a, B:155:0x0592, B:156:0x0597, B:158:0x059f, B:159:0x05a4, B:161:0x05ad, B:162:0x05b1, B:164:0x05be, B:165:0x05c3, B:167:0x05c9, B:169:0x05d7, B:170:0x05ee, B:172:0x05f4, B:174:0x0604, B:176:0x060e, B:178:0x0616, B:179:0x061b, B:181:0x0625, B:183:0x062f, B:185:0x0637, B:186:0x063d, B:188:0x0647, B:190:0x064f, B:191:0x0654, B:193:0x065c, B:194:0x065f, B:196:0x066e, B:197:0x0671, B:199:0x0687, B:201:0x0695, B:203:0x069b, B:205:0x06ad, B:207:0x06b1, B:209:0x06bc, B:210:0x06c7, B:212:0x06d9, B:214:0x06dd, B:216:0x06e3, B:218:0x06f3, B:220:0x0705, B:221:0x071f, B:223:0x0725, B:224:0x073c, B:225:0x073f, B:227:0x0784, B:228:0x0789, B:230:0x0791, B:232:0x0797, B:234:0x07a5, B:235:0x07a9, B:236:0x07ac, B:239:0x07b4, B:240:0x07b7, B:241:0x07b8, B:243:0x07c4, B:245:0x07d3, B:247:0x07e1, B:248:0x07e9, B:249:0x07f0, B:251:0x0800, B:253:0x080e, B:255:0x0814, B:256:0x081f, B:258:0x0854, B:259:0x0859, B:260:0x0865, B:262:0x086b, B:264:0x0879, B:266:0x087f, B:268:0x0889, B:269:0x0890, B:271:0x089a, B:272:0x08a1, B:273:0x08ac, B:275:0x08b2, B:277:0x08e1, B:278:0x08f1, B:280:0x08f9, B:281:0x08fd, B:283:0x0906, B:286:0x0913, B:288:0x0931, B:292:0x0947, B:294:0x094d, B:296:0x0951, B:297:0x0969), top: B:307:0x013e, inners: #0, #1 }] */
    /* JADX WARN: Removed duplicated region for block: B:99:0x0302  */
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void I(com.google.android.gms.measurement.internal.zzaq r27, com.google.android.gms.measurement.internal.zzn r28) {
        /*
            Method dump skipped, instructions count: 2470
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.k9.I(com.google.android.gms.measurement.internal.zzaq, com.google.android.gms.measurement.internal.zzn):void");
    }

    public final x3 J() {
        C(this.c);
        return this.c;
    }

    public final g K() {
        C(this.d);
        return this.d;
    }

    public final boolean L(zzn zznVar) {
        return (!da.b() || !this.k.h.u(zznVar.j, p.f1557j0)) ? !TextUtils.isEmpty(zznVar.k) || !TextUtils.isEmpty(zznVar.A) : !TextUtils.isEmpty(zznVar.k) || !TextUtils.isEmpty(zznVar.E) || !TextUtils.isEmpty(zznVar.A);
    }

    public final ba M() {
        C(this.g);
        return this.g;
    }

    public final q9 N() {
        C(this.h);
        return this.h;
    }

    public final o3 O() {
        return this.k.u();
    }

    public final void P() {
        if (!this.l) {
            throw new IllegalStateException("UploadController is not initialized");
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:140:0x031d A[Catch: all -> 0x0407, TryCatch #1 {all -> 0x0407, blocks: (B:3:0x000c, B:5:0x0016, B:8:0x0029, B:10:0x002f, B:13:0x0042, B:15:0x004a, B:18:0x0053, B:23:0x005f, B:26:0x0072, B:28:0x007c, B:31:0x0092, B:33:0x00bb, B:35:0x00c1, B:36:0x00c4, B:38:0x00d4, B:39:0x00eb, B:41:0x00fb, B:43:0x0101, B:44:0x010b, B:46:0x0131, B:48:0x0137, B:50:0x0143, B:52:0x014d, B:53:0x0151, B:55:0x0157, B:57:0x016b, B:61:0x0174, B:63:0x017a, B:65:0x018e, B:67:0x0198, B:68:0x019d, B:69:0x01a0, B:71:0x01c5, B:73:0x01cb, B:75:0x01d7, B:79:0x01e4, B:81:0x01ea, B:83:0x01f6, B:88:0x0204, B:90:0x020a, B:92:0x0216, B:99:0x0227, B:101:0x024f, B:102:0x0254, B:104:0x025f, B:105:0x0264, B:107:0x026f, B:108:0x0274, B:110:0x027d, B:112:0x0281, B:113:0x0286, B:114:0x028d, B:116:0x0293, B:119:0x02a1, B:121:0x02a5, B:122:0x02aa, B:124:0x02b5, B:125:0x02ba, B:127:0x02c3, B:129:0x02c7, B:130:0x02cc, B:131:0x02d3, B:133:0x02df, B:135:0x02f7, B:136:0x02fc, B:137:0x0303, B:138:0x030e, B:140:0x031d, B:142:0x032f, B:143:0x0347, B:147:0x0355, B:149:0x035c, B:150:0x036a, B:151:0x0373, B:153:0x0382, B:154:0x038e, B:155:0x03c4, B:156:0x03d6, B:158:0x03f4, B:160:0x03fe), top: B:168:0x000c, inners: #0 }] */
    /* JADX WARN: Removed duplicated region for block: B:141:0x032e  */
    /* JADX WARN: Removed duplicated region for block: B:145:0x0352  */
    /* JADX WARN: Removed duplicated region for block: B:146:0x0354  */
    /* JADX WARN: Removed duplicated region for block: B:149:0x035c A[Catch: MalformedURLException -> 0x03c4, all -> 0x0407, TryCatch #0 {MalformedURLException -> 0x03c4, blocks: (B:143:0x0347, B:147:0x0355, B:149:0x035c, B:150:0x036a, B:151:0x0373, B:153:0x0382, B:154:0x038e), top: B:166:0x0347, outer: #1 }] */
    /* JADX WARN: Removed duplicated region for block: B:150:0x036a A[Catch: MalformedURLException -> 0x03c4, all -> 0x0407, TryCatch #0 {MalformedURLException -> 0x03c4, blocks: (B:143:0x0347, B:147:0x0355, B:149:0x035c, B:150:0x036a, B:151:0x0373, B:153:0x0382, B:154:0x038e), top: B:166:0x0347, outer: #1 }] */
    /* JADX WARN: Removed duplicated region for block: B:153:0x0382 A[Catch: MalformedURLException -> 0x03c4, all -> 0x0407, TryCatch #0 {MalformedURLException -> 0x03c4, blocks: (B:143:0x0347, B:147:0x0355, B:149:0x035c, B:150:0x036a, B:151:0x0373, B:153:0x0382, B:154:0x038e), top: B:166:0x0347, outer: #1 }] */
    /* JADX WARN: Removed duplicated region for block: B:99:0x0227 A[Catch: all -> 0x0407, TryCatch #1 {all -> 0x0407, blocks: (B:3:0x000c, B:5:0x0016, B:8:0x0029, B:10:0x002f, B:13:0x0042, B:15:0x004a, B:18:0x0053, B:23:0x005f, B:26:0x0072, B:28:0x007c, B:31:0x0092, B:33:0x00bb, B:35:0x00c1, B:36:0x00c4, B:38:0x00d4, B:39:0x00eb, B:41:0x00fb, B:43:0x0101, B:44:0x010b, B:46:0x0131, B:48:0x0137, B:50:0x0143, B:52:0x014d, B:53:0x0151, B:55:0x0157, B:57:0x016b, B:61:0x0174, B:63:0x017a, B:65:0x018e, B:67:0x0198, B:68:0x019d, B:69:0x01a0, B:71:0x01c5, B:73:0x01cb, B:75:0x01d7, B:79:0x01e4, B:81:0x01ea, B:83:0x01f6, B:88:0x0204, B:90:0x020a, B:92:0x0216, B:99:0x0227, B:101:0x024f, B:102:0x0254, B:104:0x025f, B:105:0x0264, B:107:0x026f, B:108:0x0274, B:110:0x027d, B:112:0x0281, B:113:0x0286, B:114:0x028d, B:116:0x0293, B:119:0x02a1, B:121:0x02a5, B:122:0x02aa, B:124:0x02b5, B:125:0x02ba, B:127:0x02c3, B:129:0x02c7, B:130:0x02cc, B:131:0x02d3, B:133:0x02df, B:135:0x02f7, B:136:0x02fc, B:137:0x0303, B:138:0x030e, B:140:0x031d, B:142:0x032f, B:143:0x0347, B:147:0x0355, B:149:0x035c, B:150:0x036a, B:151:0x0373, B:153:0x0382, B:154:0x038e, B:155:0x03c4, B:156:0x03d6, B:158:0x03f4, B:160:0x03fe), top: B:168:0x000c, inners: #0 }] */
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void Q() {
        /*
            Method dump skipped, instructions count: 1038
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.k9.Q():void");
    }

    /* JADX WARN: Removed duplicated region for block: B:25:0x00a3  */
    /* JADX WARN: Removed duplicated region for block: B:42:0x010b  */
    /* JADX WARN: Removed duplicated region for block: B:43:0x0122  */
    /* JADX WARN: Removed duplicated region for block: B:57:0x018b  */
    /* JADX WARN: Removed duplicated region for block: B:58:0x01a1  */
    /* JADX WARN: Removed duplicated region for block: B:65:? A[RETURN, SYNTHETIC] */
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void R() {
        /*
            Method dump skipped, instructions count: 439
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.k9.R():void");
    }

    public final b4 S() {
        b4 b4Var = this.e;
        if (b4Var != null) {
            return b4Var;
        }
        throw new IllegalStateException("Network broadcast receiver not created");
    }

    public final h9 T() {
        C(this.f);
        return this.f;
    }

    @WorkerThread
    public final void U() {
        this.k.f().b();
    }

    public final long V() {
        Objects.requireNonNull((c) this.k.o);
        long currentTimeMillis = System.currentTimeMillis();
        d4 o = this.k.o();
        o.o();
        o.b();
        long a2 = o.j.a();
        if (a2 == 0) {
            a2 = 1 + o.e().v0().nextInt(86400000);
            o.j.b(a2);
        }
        return ((((currentTimeMillis + a2) / 1000) / 60) / 60) / 24;
    }

    @WorkerThread
    @Deprecated
    public final String W() {
        byte[] bArr = new byte[16];
        this.k.t().v0().nextBytes(bArr);
        return String.format(Locale.US, "%032x", new BigInteger(1, bArr));
    }

    @WorkerThread
    public final d a(String str) {
        String str2;
        d dVar = d.a;
        if (!t8.b() || !this.k.h.o(p.J0)) {
            return dVar;
        }
        U();
        P();
        d dVar2 = this.f1542z.get(str);
        if (dVar2 != null) {
            return dVar2;
        }
        g K = K();
        Objects.requireNonNull(K);
        Objects.requireNonNull(str, "null reference");
        K.b();
        K.n();
        Cursor cursor = null;
        try {
            try {
                cursor = K.t().rawQuery("select consent_state from consent_settings where app_id=? limit 1;", new String[]{str});
                if (cursor.moveToFirst()) {
                    str2 = cursor.getString(0);
                    cursor.close();
                } else {
                    cursor.close();
                    str2 = "G1";
                }
                d b2 = d.b(str2);
                s(str, b2);
                return b2;
            } catch (SQLiteException e) {
                K.g().f.c("Database error", "select consent_state from consent_settings where app_id=? limit 1;", e);
                throw e;
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @WorkerThread
    public final String c(d dVar) {
        if (!t8.b() || !this.k.h.o(p.J0) || dVar.k()) {
            return W();
        }
        return null;
    }

    @Override // b.i.a.f.i.b.t5
    public final r4 f() {
        return this.k.f();
    }

    @Override // b.i.a.f.i.b.t5
    public final q3 g() {
        return this.k.g();
    }

    public final void h(e1.a aVar, long j, boolean z2) {
        u9 u9Var;
        boolean z3;
        String str = z2 ? "_se" : "_lte";
        u9 Y = K().Y(aVar.k0(), str);
        if (Y == null || Y.e == null) {
            String k0 = aVar.k0();
            Objects.requireNonNull((c) this.k.o);
            u9Var = new u9(k0, "auto", str, System.currentTimeMillis(), Long.valueOf(j));
        } else {
            String k02 = aVar.k0();
            Objects.requireNonNull((c) this.k.o);
            u9Var = new u9(k02, "auto", str, System.currentTimeMillis(), Long.valueOf(((Long) Y.e).longValue() + j));
        }
        i1.a L = i1.L();
        L.r(str);
        Objects.requireNonNull((c) this.k.o);
        L.q(System.currentTimeMillis());
        L.s(((Long) u9Var.e).longValue());
        i1 i1Var = (i1) ((u4) L.p());
        int s2 = q9.s(aVar, str);
        if (s2 >= 0) {
            if (aVar.l) {
                aVar.n();
                aVar.l = false;
            }
            e1.x((e1) aVar.k, s2, i1Var);
            z3 = true;
        } else {
            z3 = false;
        }
        if (!z3) {
            if (aVar.l) {
                aVar.n();
                aVar.l = false;
            }
            e1.A((e1) aVar.k, i1Var);
        }
        if (j > 0) {
            K().M(u9Var);
            this.k.g().n.c("Updated engagement user property. scope, value", z2 ? "session-scoped" : "lifetime", u9Var.e);
        }
    }

    @Override // b.i.a.f.i.b.t5
    public final b i() {
        return this.k.o;
    }

    @Override // b.i.a.f.i.b.t5
    public final Context j() {
        return this.k.f1566b;
    }

    @Override // b.i.a.f.i.b.t5
    public final ga k() {
        return this.k.g;
    }

    @WorkerThread
    public final void l(zzaq zzaqVar, zzn zznVar) {
        List<zzz> list;
        List<zzz> list2;
        List<zzz> list3;
        zzaq zzaqVar2 = zzaqVar;
        Objects.requireNonNull(zznVar, "null reference");
        d.w(zznVar.j);
        U();
        P();
        String str = zznVar.j;
        long j = zzaqVar2.m;
        N();
        if (q9.O(zzaqVar, zznVar)) {
            if (!zznVar.q) {
                G(zznVar);
                return;
            }
            List<String> list4 = zznVar.D;
            if (list4 != null) {
                if (list4.contains(zzaqVar2.j)) {
                    Bundle x0 = zzaqVar2.k.x0();
                    x0.putLong("ga_safelisted", 1L);
                    zzaqVar2 = new zzaq(zzaqVar2.j, new zzap(x0), zzaqVar2.l, zzaqVar2.m);
                } else {
                    this.k.g().m.d("Dropping non-safelisted event. appId, event name, origin", str, zzaqVar2.j, zzaqVar2.l);
                    return;
                }
            }
            K().b0();
            try {
                g K = K();
                d.w(str);
                K.b();
                K.n();
                int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
                if (i < 0) {
                    K.g().i.c("Invalid time querying timed out conditional properties", q3.s(str), Long.valueOf(j));
                    list = Collections.emptyList();
                } else {
                    list = K.F("active=0 and app_id=? and abs(? - creation_timestamp) > trigger_timeout", new String[]{str, String.valueOf(j)});
                }
                for (zzz zzzVar : list) {
                    if (zzzVar != null) {
                        this.k.g().n.d("User property timed out", zzzVar.j, this.k.u().y(zzzVar.l.k), zzzVar.l.w0());
                        zzaq zzaqVar3 = zzzVar.p;
                        if (zzaqVar3 != null) {
                            I(new zzaq(zzaqVar3, j), zznVar);
                        }
                        K().a0(str, zzzVar.l.k);
                    }
                }
                g K2 = K();
                d.w(str);
                K2.b();
                K2.n();
                if (i < 0) {
                    K2.g().i.c("Invalid time querying expired conditional properties", q3.s(str), Long.valueOf(j));
                    list2 = Collections.emptyList();
                } else {
                    list2 = K2.F("active<>0 and app_id=? and abs(? - triggered_timestamp) > time_to_live", new String[]{str, String.valueOf(j)});
                }
                ArrayList arrayList = new ArrayList(list2.size());
                for (zzz zzzVar2 : list2) {
                    if (zzzVar2 != null) {
                        this.k.g().n.d("User property expired", zzzVar2.j, this.k.u().y(zzzVar2.l.k), zzzVar2.l.w0());
                        K().V(str, zzzVar2.l.k);
                        zzaq zzaqVar4 = zzzVar2.t;
                        if (zzaqVar4 != null) {
                            arrayList.add(zzaqVar4);
                        }
                        K().a0(str, zzzVar2.l.k);
                    }
                }
                int size = arrayList.size();
                int i2 = 0;
                while (i2 < size) {
                    Object obj = arrayList.get(i2);
                    i2++;
                    I(new zzaq((zzaq) obj, j), zznVar);
                }
                g K3 = K();
                String str2 = zzaqVar2.j;
                d.w(str);
                d.w(str2);
                K3.b();
                K3.n();
                if (i < 0) {
                    K3.g().i.d("Invalid time querying triggered conditional properties", q3.s(str), K3.d().u(str2), Long.valueOf(j));
                    list3 = Collections.emptyList();
                } else {
                    list3 = K3.F("active=0 and app_id=? and trigger_event_name=? and abs(? - creation_timestamp) <= trigger_timeout", new String[]{str, str2, String.valueOf(j)});
                }
                ArrayList arrayList2 = new ArrayList(list3.size());
                for (zzz zzzVar3 : list3) {
                    if (zzzVar3 != null) {
                        zzku zzkuVar = zzzVar3.l;
                        u9 u9Var = new u9(zzzVar3.j, zzzVar3.k, zzkuVar.k, j, zzkuVar.w0());
                        if (K().M(u9Var)) {
                            this.k.g().n.d("User property triggered", zzzVar3.j, this.k.u().y(u9Var.c), u9Var.e);
                        } else {
                            this.k.g().f.d("Too many active user properties, ignoring", q3.s(zzzVar3.j), this.k.u().y(u9Var.c), u9Var.e);
                        }
                        zzaq zzaqVar5 = zzzVar3.r;
                        if (zzaqVar5 != null) {
                            arrayList2.add(zzaqVar5);
                        }
                        zzzVar3.l = new zzku(u9Var);
                        zzzVar3.n = true;
                        K().N(zzzVar3);
                    }
                }
                I(zzaqVar2, zznVar);
                int size2 = arrayList2.size();
                int i3 = 0;
                while (i3 < size2) {
                    Object obj2 = arrayList2.get(i3);
                    i3++;
                    I(new zzaq((zzaq) obj2, j), zznVar);
                }
                K().s();
            } finally {
                K().e0();
            }
        }
    }

    @WorkerThread
    public final void m(zzaq zzaqVar, String str) {
        boolean z2;
        String str2;
        a4 T = K().T(str);
        if (T == null || TextUtils.isEmpty(T.M())) {
            this.k.g().m.b("No app data available; dropping event", str);
            return;
        }
        Boolean z3 = z(T);
        if (z3 == null) {
            if (!"_ui".equals(zzaqVar.j)) {
                this.k.g().i.b("Could not find package. appId", q3.s(str));
            }
        } else if (!z3.booleanValue()) {
            this.k.g().f.b("App version does not match; dropping event. appId", q3.s(str));
            return;
        }
        String v = T.v();
        String M = T.M();
        long N = T.N();
        String O = T.O();
        long P = T.P();
        long Q = T.Q();
        boolean T2 = T.T();
        String H = T.H();
        long g = T.g();
        boolean h = T.h();
        boolean i = T.i();
        String y2 = T.y();
        Boolean j = T.j();
        long S = T.S();
        List<String> k = T.k();
        if (da.b()) {
            z2 = T2;
            if (this.k.h.u(T.o(), p.f1557j0)) {
                str2 = T.B();
                B(zzaqVar, new zzn(str, v, M, N, O, P, Q, (String) null, z2, false, H, g, 0L, 0, h, i, false, y2, j, S, k, str2, (t8.b() || !this.k.h.o(p.J0)) ? "" : a(str).d()));
            }
        } else {
            z2 = T2;
        }
        str2 = null;
        B(zzaqVar, new zzn(str, v, M, N, O, P, Q, (String) null, z2, false, H, g, 0L, 0, h, i, false, y2, j, S, k, str2, (t8.b() || !this.k.h.o(p.J0)) ? "" : a(str).d()));
    }

    /* JADX WARN: Code restructure failed: missing block: B:27:0x009b, code lost:
        if (android.text.TextUtils.isEmpty(r2) != false) goto L28;
     */
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void n(b.i.a.f.i.b.a4 r12) {
        /*
            Method dump skipped, instructions count: 376
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.k9.n(b.i.a.f.i.b.a4):void");
    }

    @WorkerThread
    public final void o(zzku zzkuVar, zzn zznVar) {
        U();
        P();
        if (L(zznVar)) {
            if (!zznVar.q) {
                G(zznVar);
                return;
            }
            int h02 = this.k.t().h0(zzkuVar.k);
            if (h02 != 0) {
                this.k.t();
                String E = t9.E(zzkuVar.k, 24, true);
                String str = zzkuVar.k;
                this.k.t().S(this.A, zznVar.j, h02, "_ev", E, str != null ? str.length() : 0);
                return;
            }
            int i02 = this.k.t().i0(zzkuVar.k, zzkuVar.w0());
            if (i02 != 0) {
                this.k.t();
                String E2 = t9.E(zzkuVar.k, 24, true);
                Object w0 = zzkuVar.w0();
                this.k.t().S(this.A, zznVar.j, i02, "_ev", E2, (w0 == null || (!(w0 instanceof String) && !(w0 instanceof CharSequence))) ? 0 : String.valueOf(w0).length());
                return;
            }
            Object n0 = this.k.t().n0(zzkuVar.k, zzkuVar.w0());
            if (n0 != null) {
                if ("_sid".equals(zzkuVar.k)) {
                    long j = zzkuVar.l;
                    String str2 = zzkuVar.o;
                    long j2 = 0;
                    u9 Y = K().Y(zznVar.j, "_sno");
                    if (Y != null) {
                        Object obj = Y.e;
                        if (obj instanceof Long) {
                            j2 = ((Long) obj).longValue();
                            o(new zzku("_sno", j, Long.valueOf(j2 + 1), str2), zznVar);
                        }
                    }
                    if (Y != null) {
                        this.k.g().i.b("Retrieved last session number from database does not contain a valid (long) value", Y.e);
                    }
                    l z2 = K().z(zznVar.j, "_s");
                    if (z2 != null) {
                        j2 = z2.c;
                        this.k.g().n.b("Backfill the session number. Last used session number", Long.valueOf(j2));
                    }
                    o(new zzku("_sno", j, Long.valueOf(j2 + 1), str2), zznVar);
                }
                u9 u9Var = new u9(zznVar.j, zzkuVar.o, zzkuVar.k, zzkuVar.l, n0);
                this.k.g().n.c("Setting user property", this.k.u().y(u9Var.c), n0);
                K().b0();
                try {
                    G(zznVar);
                    boolean M = K().M(u9Var);
                    K().s();
                    if (!M) {
                        this.k.g().f.c("Too many unique user properties are set. Ignoring user property", this.k.u().y(u9Var.c), u9Var.e);
                        this.k.t().S(this.A, zznVar.j, 9, null, null, 0);
                    }
                } finally {
                    K().e0();
                }
            }
        }
    }

    @WorkerThread
    public final void p(zzn zznVar) {
        if (this.w != null) {
            ArrayList arrayList = new ArrayList();
            this.f1540x = arrayList;
            arrayList.addAll(this.w);
        }
        g K = K();
        String str = zznVar.j;
        d.w(str);
        K.b();
        K.n();
        try {
            SQLiteDatabase t = K.t();
            String[] strArr = {str};
            int delete = t.delete("apps", "app_id=?", strArr) + 0 + t.delete("events", "app_id=?", strArr) + t.delete("user_attributes", "app_id=?", strArr) + t.delete("conditional_properties", "app_id=?", strArr) + t.delete("raw_events", "app_id=?", strArr) + t.delete("raw_events_metadata", "app_id=?", strArr) + t.delete("queue", "app_id=?", strArr) + t.delete("audience_filter_values", "app_id=?", strArr) + t.delete("main_event_params", "app_id=?", strArr) + t.delete("default_event_params", "app_id=?", strArr);
            if (delete > 0) {
                K.g().n.c("Reset analytics data. app, records", str, Integer.valueOf(delete));
            }
        } catch (SQLiteException e) {
            K.g().f.c("Error resetting analytics data. appId, error", q3.s(str), e);
        }
        if (zznVar.q) {
            E(zznVar);
        }
    }

    @WorkerThread
    public final void q(zzz zzzVar, zzn zznVar) {
        zzaq zzaqVar;
        boolean z2;
        Objects.requireNonNull(zzzVar, "null reference");
        d.w(zzzVar.j);
        Objects.requireNonNull(zzzVar.k, "null reference");
        Objects.requireNonNull(zzzVar.l, "null reference");
        d.w(zzzVar.l.k);
        U();
        P();
        if (L(zznVar)) {
            if (!zznVar.q) {
                G(zznVar);
                return;
            }
            zzz zzzVar2 = new zzz(zzzVar);
            boolean z3 = false;
            zzzVar2.n = false;
            K().b0();
            try {
                zzz Z = K().Z(zzzVar2.j, zzzVar2.l.k);
                if (Z != null && !Z.k.equals(zzzVar2.k)) {
                    this.k.g().i.d("Updating a conditional user property with different origin. name, origin, origin (from DB)", this.k.u().y(zzzVar2.l.k), zzzVar2.k, Z.k);
                }
                if (Z != null && (z2 = Z.n)) {
                    zzzVar2.k = Z.k;
                    zzzVar2.m = Z.m;
                    zzzVar2.q = Z.q;
                    zzzVar2.o = Z.o;
                    zzzVar2.r = Z.r;
                    zzzVar2.n = z2;
                    zzku zzkuVar = zzzVar2.l;
                    zzzVar2.l = new zzku(zzkuVar.k, Z.l.l, zzkuVar.w0(), Z.l.o);
                } else if (TextUtils.isEmpty(zzzVar2.o)) {
                    zzku zzkuVar2 = zzzVar2.l;
                    zzzVar2.l = new zzku(zzkuVar2.k, zzzVar2.m, zzkuVar2.w0(), zzzVar2.l.o);
                    zzzVar2.n = true;
                    z3 = true;
                }
                if (zzzVar2.n) {
                    zzku zzkuVar3 = zzzVar2.l;
                    u9 u9Var = new u9(zzzVar2.j, zzzVar2.k, zzkuVar3.k, zzkuVar3.l, zzkuVar3.w0());
                    if (K().M(u9Var)) {
                        this.k.g().m.d("User property updated immediately", zzzVar2.j, this.k.u().y(u9Var.c), u9Var.e);
                    } else {
                        this.k.g().f.d("(2)Too many active user properties, ignoring", q3.s(zzzVar2.j), this.k.u().y(u9Var.c), u9Var.e);
                    }
                    if (z3 && (zzaqVar = zzzVar2.r) != null) {
                        I(new zzaq(zzaqVar, zzzVar2.m), zznVar);
                    }
                }
                if (K().N(zzzVar2)) {
                    this.k.g().m.d("Conditional property added", zzzVar2.j, this.k.u().y(zzzVar2.l.k), zzzVar2.l.w0());
                } else {
                    this.k.g().f.d("Too many conditional properties, ignoring", q3.s(zzzVar2.j), this.k.u().y(zzzVar2.l.k), zzzVar2.l.w0());
                }
                K().s();
            } finally {
                K().e0();
            }
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:27:0x00b6, code lost:
        r8 = r7.k.o().h;
        java.util.Objects.requireNonNull((b.i.a.f.e.o.c) r7.k.o);
        r8.b(java.lang.System.currentTimeMillis());
     */
    /* JADX WARN: Code restructure failed: missing block: B:30:0x00d5, code lost:
        r10 = r12.get("Last-Modified");
     */
    /* JADX WARN: Removed duplicated region for block: B:46:0x0126 A[Catch: all -> 0x0175, TryCatch #1 {all -> 0x017e, blocks: (B:4:0x000c, B:5:0x000e, B:55:0x0168, B:6:0x0027, B:15:0x0043, B:20:0x005e, B:27:0x00b6, B:28:0x00ce, B:30:0x00d5, B:33:0x00e1, B:35:0x00e7, B:40:0x00f4, B:41:0x00fc, B:43:0x0106, B:44:0x010d, B:46:0x0126, B:47:0x0134, B:48:0x014a, B:50:0x0154, B:52:0x015a, B:53:0x015e, B:54:0x0161), top: B:61:0x000c }] */
    /* JADX WARN: Removed duplicated region for block: B:47:0x0134 A[Catch: all -> 0x0175, TryCatch #1 {all -> 0x017e, blocks: (B:4:0x000c, B:5:0x000e, B:55:0x0168, B:6:0x0027, B:15:0x0043, B:20:0x005e, B:27:0x00b6, B:28:0x00ce, B:30:0x00d5, B:33:0x00e1, B:35:0x00e7, B:40:0x00f4, B:41:0x00fc, B:43:0x0106, B:44:0x010d, B:46:0x0126, B:47:0x0134, B:48:0x014a, B:50:0x0154, B:52:0x015a, B:53:0x015e, B:54:0x0161), top: B:61:0x000c }] */
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void r(java.lang.String r8, int r9, java.lang.Throwable r10, byte[] r11, java.util.Map<java.lang.String, java.util.List<java.lang.String>> r12) {
        /*
            Method dump skipped, instructions count: 389
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.k9.r(java.lang.String, int, java.lang.Throwable, byte[], java.util.Map):void");
    }

    @WorkerThread
    public final void s(String str, d dVar) {
        if (t8.b() && this.k.h.o(p.J0)) {
            U();
            P();
            this.f1542z.put(str, dVar);
            g K = K();
            if (t8.b() && K.a.h.o(p.J0)) {
                Objects.requireNonNull(str, "null reference");
                K.b();
                K.n();
                ContentValues contentValues = new ContentValues();
                contentValues.put("app_id", str);
                contentValues.put("consent_state", dVar.d());
                try {
                    if (K.t().insertWithOnConflict("consent_settings", null, contentValues, 5) == -1) {
                        K.g().f.b("Failed to insert/update consent setting (got -1). appId", q3.s(str));
                    }
                } catch (SQLiteException e) {
                    K.g().f.c("Error storing consent setting. appId, error", q3.s(str), e);
                }
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: Unreachable block: B:72:0x018e
        	at jadx.core.dex.visitors.blocks.BlockProcessor.checkForUnreachableBlocks(BlockProcessor.java:92)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:52)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:44)
        */
    @androidx.annotation.WorkerThread
    public final boolean t(long r44) {
        /*
            Method dump skipped, instructions count: 4262
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.k9.t(long):boolean");
    }

    public final boolean u(a1.a aVar, a1.a aVar2) {
        d.l("_e".equals(aVar.y()));
        N();
        c1 w = q9.w((a1) ((u4) aVar.p()), "_sc");
        String str = null;
        String G = w == null ? null : w.G();
        N();
        c1 w2 = q9.w((a1) ((u4) aVar2.p()), "_pc");
        if (w2 != null) {
            str = w2.G();
        }
        if (str == null || !str.equals(G)) {
            return false;
        }
        A(aVar, aVar2);
        return true;
    }

    public final boolean v() {
        U();
        P();
        return ((K().S("select count(1) > 0 from raw_events", null) > 0L ? 1 : (K().S("select count(1) > 0 from raw_events", null) == 0L ? 0 : -1)) != 0) || !TextUtils.isEmpty(K().u());
    }

    /* JADX WARN: Removed duplicated region for block: B:102:0x0399  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x00a8  */
    /* JADX WARN: Removed duplicated region for block: B:32:0x00e4  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x012c  */
    /* JADX WARN: Removed duplicated region for block: B:56:0x01a4  */
    /* JADX WARN: Removed duplicated region for block: B:58:0x01c2  */
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void w() {
        /*
            Method dump skipped, instructions count: 1021
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.k9.w():void");
    }

    @WorkerThread
    public final void x() {
        U();
        if (this.r || this.f1539s || this.t) {
            this.k.g().n.d("Not stopping services. fetch, network, upload", Boolean.valueOf(this.r), Boolean.valueOf(this.f1539s), Boolean.valueOf(this.t));
            return;
        }
        this.k.g().n.a("Stopping uploading service(s)");
        List<Runnable> list = this.o;
        if (list != null) {
            for (Runnable runnable : list) {
                runnable.run();
            }
            this.o.clear();
        }
    }

    @WorkerThread
    public final zzn y(String str) {
        a4 T = K().T(str);
        if (T == null || TextUtils.isEmpty(T.M())) {
            this.k.g().m.b("No app data available; dropping", str);
            return null;
        }
        Boolean z2 = z(T);
        if (z2 == null || z2.booleanValue()) {
            return new zzn(str, T.v(), T.M(), T.N(), T.O(), T.P(), T.Q(), (String) null, T.T(), false, T.H(), T.g(), 0L, 0, T.h(), T.i(), false, T.y(), T.j(), T.S(), T.k(), (!da.b() || !this.k.h.u(str, p.f1557j0)) ? null : T.B(), (!t8.b() || !this.k.h.o(p.J0)) ? "" : a(str).d());
        }
        this.k.g().f.b("App version does not match; dropping. appId", q3.s(str));
        return null;
    }

    @WorkerThread
    public final Boolean z(a4 a4Var) {
        try {
            if (a4Var.N() != -2147483648L) {
                if (a4Var.N() == b.i.a.f.e.p.b.a(this.k.f1566b).b(a4Var.o(), 0).versionCode) {
                    return Boolean.TRUE;
                }
            } else {
                String str = b.i.a.f.e.p.b.a(this.k.f1566b).b(a4Var.o(), 0).versionName;
                if (a4Var.M() != null && a4Var.M().equals(str)) {
                    return Boolean.TRUE;
                }
            }
            return Boolean.FALSE;
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }
}
