package b.i.a.f.i.b;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import androidx.annotation.WorkerThread;
import androidx.collection.ArrayMap;
import b.c.a.a0.d;
import b.i.a.f.e.o.f;
import b.i.a.f.h.l.k0;
import b.i.a.f.h.l.l0;
import b.i.a.f.h.l.m0;
import b.i.a.f.h.l.o0;
import b.i.a.f.h.l.t0;
import b.i.a.f.h.l.u0;
import b.i.a.f.h.l.u4;
import b.i.a.f.h.l.v0;
import b.i.a.f.h.l.x7;
import com.google.android.gms.internal.measurement.zzij;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-measurement@@18.0.0 */
/* loaded from: classes3.dex */
public final class p4 extends i9 implements e {
    public final Map<String, Map<String, String>> d = new ArrayMap();
    public final Map<String, Map<String, Boolean>> e = new ArrayMap();
    public final Map<String, Map<String, Boolean>> f = new ArrayMap();
    public final Map<String, u0> g = new ArrayMap();
    public final Map<String, String> i = new ArrayMap();
    public final Map<String, Map<String, Integer>> h = new ArrayMap();

    public p4(k9 k9Var) {
        super(k9Var);
    }

    public static Map<String, String> u(u0 u0Var) {
        ArrayMap arrayMap = new ArrayMap();
        for (v0 v0Var : u0Var.B()) {
            arrayMap.put(v0Var.u(), v0Var.v());
        }
        return arrayMap;
    }

    @WorkerThread
    public final boolean A(String str) {
        b();
        u0 s2 = s(str);
        if (s2 == null) {
            return false;
        }
        return s2.E();
    }

    @WorkerThread
    public final long B(String str) {
        String h = h(str, "measurement.account.time_zone_offset_minutes");
        if (TextUtils.isEmpty(h)) {
            return 0L;
        }
        try {
            return Long.parseLong(h);
        } catch (NumberFormatException e) {
            g().i.c("Unable to parse timezone offset. appId", q3.s(str), e);
            return 0L;
        }
    }

    public final boolean C(String str) {
        return "1".equals(h(str, "measurement.upload.blacklist_internal"));
    }

    public final boolean D(String str) {
        return "1".equals(h(str, "measurement.upload.blacklist_public"));
    }

    /* JADX WARN: Not initialized variable reg: 2, insn: 0x00d3: MOVE  (r1 I:??[OBJECT, ARRAY]) = (r2 I:??[OBJECT, ARRAY]), block:B:26:0x00d3 */
    /* JADX WARN: Removed duplicated region for block: B:22:0x0082  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x00a1  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x00d6  */
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void E(java.lang.String r12) {
        /*
            r11 = this;
            r11.n()
            r11.b()
            b.c.a.a0.d.w(r12)
            java.util.Map<java.lang.String, b.i.a.f.h.l.u0> r0 = r11.g
            java.lang.Object r0 = r0.get(r12)
            if (r0 != 0) goto Lda
            b.i.a.f.i.b.g r0 = r11.q()
            java.util.Objects.requireNonNull(r0)
            b.c.a.a0.d.w(r12)
            r0.b()
            r0.n()
            r1 = 0
            android.database.sqlite.SQLiteDatabase r2 = r0.t()     // Catch: java.lang.Throwable -> L66 android.database.sqlite.SQLiteException -> L68
            java.lang.String r3 = "apps"
            java.lang.String r4 = "remote_config"
            java.lang.String[] r4 = new java.lang.String[]{r4}     // Catch: java.lang.Throwable -> L66 android.database.sqlite.SQLiteException -> L68
            java.lang.String r5 = "app_id=?"
            r6 = 1
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch: java.lang.Throwable -> L66 android.database.sqlite.SQLiteException -> L68
            r10 = 0
            r6[r10] = r12     // Catch: java.lang.Throwable -> L66 android.database.sqlite.SQLiteException -> L68
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r2 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch: java.lang.Throwable -> L66 android.database.sqlite.SQLiteException -> L68
            boolean r3 = r2.moveToFirst()     // Catch: android.database.sqlite.SQLiteException -> L64 java.lang.Throwable -> Ld2
            if (r3 != 0) goto L47
            r2.close()
            goto L7f
        L47:
            byte[] r3 = r2.getBlob(r10)     // Catch: android.database.sqlite.SQLiteException -> L64 java.lang.Throwable -> Ld2
            boolean r4 = r2.moveToNext()     // Catch: android.database.sqlite.SQLiteException -> L64 java.lang.Throwable -> Ld2
            if (r4 == 0) goto L60
            b.i.a.f.i.b.q3 r4 = r0.g()     // Catch: android.database.sqlite.SQLiteException -> L64 java.lang.Throwable -> Ld2
            b.i.a.f.i.b.s3 r4 = r4.f     // Catch: android.database.sqlite.SQLiteException -> L64 java.lang.Throwable -> Ld2
            java.lang.String r5 = "Got multiple records for app config, expected one. appId"
            java.lang.Object r6 = b.i.a.f.i.b.q3.s(r12)     // Catch: android.database.sqlite.SQLiteException -> L64 java.lang.Throwable -> Ld2
            r4.b(r5, r6)     // Catch: android.database.sqlite.SQLiteException -> L64 java.lang.Throwable -> Ld2
        L60:
            r2.close()
            goto L80
        L64:
            r3 = move-exception
            goto L6b
        L66:
            r12 = move-exception
            goto Ld4
        L68:
            r2 = move-exception
            r3 = r2
            r2 = r1
        L6b:
            b.i.a.f.i.b.q3 r0 = r0.g()     // Catch: java.lang.Throwable -> Ld2
            b.i.a.f.i.b.s3 r0 = r0.f     // Catch: java.lang.Throwable -> Ld2
            java.lang.String r4 = "Error querying remote config. appId"
            java.lang.Object r5 = b.i.a.f.i.b.q3.s(r12)     // Catch: java.lang.Throwable -> Ld2
            r0.c(r4, r5, r3)     // Catch: java.lang.Throwable -> Ld2
            if (r2 == 0) goto L7f
            r2.close()
        L7f:
            r3 = r1
        L80:
            if (r3 != 0) goto La1
            java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.String>> r0 = r11.d
            r0.put(r12, r1)
            java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Boolean>> r0 = r11.e
            r0.put(r12, r1)
            java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Boolean>> r0 = r11.f
            r0.put(r12, r1)
            java.util.Map<java.lang.String, b.i.a.f.h.l.u0> r0 = r11.g
            r0.put(r12, r1)
            java.util.Map<java.lang.String, java.lang.String> r0 = r11.i
            r0.put(r12, r1)
            java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.Integer>> r0 = r11.h
            r0.put(r12, r1)
            return
        La1:
            b.i.a.f.h.l.u0 r0 = r11.t(r12, r3)
            b.i.a.f.h.l.u4$b r0 = r0.t()
            b.i.a.f.h.l.u0$a r0 = (b.i.a.f.h.l.u0.a) r0
            r11.v(r12, r0)
            java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.String>> r2 = r11.d
            b.i.a.f.h.l.c6 r3 = r0.p()
            b.i.a.f.h.l.u4 r3 = (b.i.a.f.h.l.u4) r3
            b.i.a.f.h.l.u0 r3 = (b.i.a.f.h.l.u0) r3
            java.util.Map r3 = u(r3)
            r2.put(r12, r3)
            java.util.Map<java.lang.String, b.i.a.f.h.l.u0> r2 = r11.g
            b.i.a.f.h.l.c6 r0 = r0.p()
            b.i.a.f.h.l.u4 r0 = (b.i.a.f.h.l.u4) r0
            b.i.a.f.h.l.u0 r0 = (b.i.a.f.h.l.u0) r0
            r2.put(r12, r0)
            java.util.Map<java.lang.String, java.lang.String> r0 = r11.i
            r0.put(r12, r1)
            goto Lda
        Ld2:
            r12 = move-exception
            r1 = r2
        Ld4:
            if (r1 == 0) goto Ld9
            r1.close()
        Ld9:
            throw r12
        Lda:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.p4.E(java.lang.String):void");
    }

    @Override // b.i.a.f.i.b.e
    @WorkerThread
    public final String h(String str, String str2) {
        b();
        E(str);
        Map<String, String> map = this.d.get(str);
        if (map != null) {
            return map.get(str2);
        }
        return null;
    }

    @Override // b.i.a.f.i.b.i9
    public final boolean p() {
        return false;
    }

    @WorkerThread
    public final u0 s(String str) {
        n();
        b();
        d.w(str);
        E(str);
        return this.g.get(str);
    }

    @WorkerThread
    public final u0 t(String str, byte[] bArr) {
        if (bArr == null) {
            return u0.G();
        }
        try {
            u0 u0Var = (u0) ((u4) ((u0.a) q9.x(u0.F(), bArr)).p());
            s3 s3Var = g().n;
            String str2 = null;
            Long valueOf = u0Var.x() ? Long.valueOf(u0Var.y()) : null;
            if (u0Var.z()) {
                str2 = u0Var.A();
            }
            s3Var.c("Parsed config. version, gmp_app_id", valueOf, str2);
            return u0Var;
        } catch (zzij e) {
            g().i.c("Unable to merge remote config. appId", q3.s(str), e);
            return u0.G();
        } catch (RuntimeException e2) {
            g().i.c("Unable to merge remote config. appId", q3.s(str), e2);
            return u0.G();
        }
    }

    public final void v(String str, u0.a aVar) {
        ArrayMap arrayMap = new ArrayMap();
        ArrayMap arrayMap2 = new ArrayMap();
        ArrayMap arrayMap3 = new ArrayMap();
        for (int i = 0; i < ((u0) aVar.k).C(); i++) {
            t0.a t = ((u0) aVar.k).u(i).t();
            if (TextUtils.isEmpty(t.q())) {
                g().i.a("EventConfig contained null event name");
            } else {
                String q = t.q();
                String Y1 = f.Y1(t.q(), v5.a, v5.c);
                if (!TextUtils.isEmpty(Y1)) {
                    if (t.l) {
                        t.n();
                        t.l = false;
                    }
                    t0.v((t0) t.k, Y1);
                    if (aVar.l) {
                        aVar.n();
                        aVar.l = false;
                    }
                    u0.w((u0) aVar.k, i, (t0) ((u4) t.p()));
                }
                if (!x7.b() || !this.a.h.o(p.N0)) {
                    arrayMap.put(t.q(), Boolean.valueOf(((t0) t.k).w()));
                } else {
                    arrayMap.put(q, Boolean.valueOf(((t0) t.k).w()));
                }
                arrayMap2.put(t.q(), Boolean.valueOf(((t0) t.k).x()));
                if (((t0) t.k).y()) {
                    if (t.r() < 2 || t.r() > 65535) {
                        g().i.c("Invalid sampling rate. Event name, sample rate", t.q(), Integer.valueOf(t.r()));
                    } else {
                        arrayMap3.put(t.q(), Integer.valueOf(t.r()));
                    }
                }
            }
        }
        this.e.put(str, arrayMap);
        this.f.put(str, arrayMap2);
        this.h.put(str, arrayMap3);
    }

    @WorkerThread
    public final boolean w(String str, byte[] bArr, String str2) {
        byte[] bArr2;
        boolean z2;
        String str3;
        boolean z3;
        boolean z4;
        n();
        b();
        d.w(str);
        u0.a t = t(str, bArr).t();
        v(str, t);
        this.g.put(str, (u0) ((u4) t.p()));
        this.i.put(str, str2);
        this.d.put(str, u((u0) ((u4) t.p())));
        g q = q();
        ArrayList arrayList = new ArrayList(Collections.unmodifiableList(((u0) t.k).D()));
        Objects.requireNonNull(q);
        String str4 = "app_id=? and audience_id=?";
        String str5 = "null reference";
        for (int i = 0; i < arrayList.size(); i++) {
            k0.a t2 = ((k0) arrayList.get(i)).t();
            if (((k0) t2.k).D() != 0) {
                for (int i2 = 0; i2 < ((k0) t2.k).D(); i2++) {
                    l0.a t3 = ((k0) t2.k).z(i2).t();
                    l0.a aVar = (l0.a) ((u4.b) t3.clone());
                    t = t;
                    String Y1 = f.Y1(((l0) t3.k).z(), v5.a, v5.c);
                    if (Y1 != null) {
                        if (aVar.l) {
                            aVar.n();
                            aVar.l = false;
                        }
                        l0.w((l0) aVar.k, Y1);
                        z4 = true;
                    } else {
                        z4 = false;
                    }
                    for (int i3 = 0; i3 < ((l0) t3.k).B(); i3++) {
                        m0 u = ((l0) t3.k).u(i3);
                        t3 = t3;
                        str4 = str4;
                        str5 = str5;
                        String Y12 = f.Y1(u.C(), u5.a, u5.f1571b);
                        if (Y12 != null) {
                            m0.a t4 = u.t();
                            if (t4.l) {
                                t4.n();
                                t4.l = false;
                            }
                            m0.u((m0) t4.k, Y12);
                            m0 m0Var = (m0) ((u4) t4.p());
                            if (aVar.l) {
                                aVar.n();
                                aVar.l = false;
                            }
                            l0.v((l0) aVar.k, i3, m0Var);
                            z4 = true;
                        }
                    }
                    str4 = str4;
                    str5 = str5;
                    if (z4) {
                        if (t2.l) {
                            t2.n();
                            t2.l = false;
                        }
                        k0.v((k0) t2.k, i2, (l0) ((u4) aVar.p()));
                        arrayList.set(i, (k0) ((u4) t2.p()));
                    }
                }
            }
            t = t;
            str4 = str4;
            str5 = str5;
            if (((k0) t2.k).B() != 0) {
                for (int i4 = 0; i4 < ((k0) t2.k).B(); i4++) {
                    o0 u2 = ((k0) t2.k).u(i4);
                    String Y13 = f.Y1(u2.x(), x5.a, x5.f1574b);
                    if (Y13 != null) {
                        o0.a t5 = u2.t();
                        if (t5.l) {
                            t5.n();
                            z3 = false;
                            t5.l = false;
                        } else {
                            z3 = false;
                        }
                        o0.u((o0) t5.k, Y13);
                        if (t2.l) {
                            t2.n();
                            t2.l = z3;
                        }
                        k0.w((k0) t2.k, i4, (o0) ((u4) t5.p()));
                        arrayList.set(i, (k0) ((u4) t2.p()));
                    }
                }
            }
        }
        u0.a aVar2 = t;
        String str6 = str4;
        String str7 = str5;
        q.n();
        q.b();
        d.w(str);
        SQLiteDatabase t6 = q.t();
        t6.beginTransaction();
        try {
            q.n();
            q.b();
            d.w(str);
            SQLiteDatabase t7 = q.t();
            t7.delete("property_filters", "app_id=?", new String[]{str});
            t7.delete("event_filters", "app_id=?", new String[]{str});
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                k0 k0Var = (k0) it.next();
                q.n();
                q.b();
                d.w(str);
                String str8 = str7;
                Objects.requireNonNull(k0Var, str8);
                if (!k0Var.x()) {
                    q.g().i.b("Audience with no ID. appId", q3.s(str));
                } else {
                    int y2 = k0Var.y();
                    Iterator<l0> it2 = k0Var.C().iterator();
                    while (true) {
                        if (it2.hasNext()) {
                            if (!it2.next().x()) {
                                q.g().i.c("Event filter with no ID. Audience definition ignored. appId, audienceId", q3.s(str), Integer.valueOf(y2));
                                break;
                            }
                        } else {
                            for (o0 o0Var : k0Var.A()) {
                                if (!o0Var.v()) {
                                    q.g().i.c("Property filter with no ID. Audience definition ignored. appId, audienceId", q3.s(str), Integer.valueOf(y2));
                                }
                            }
                            Iterator<l0> it3 = k0Var.C().iterator();
                            while (true) {
                                if (it3.hasNext()) {
                                    if (!q.O(str, y2, it3.next())) {
                                        z2 = false;
                                        break;
                                    }
                                } else {
                                    z2 = true;
                                    break;
                                }
                            }
                            if (z2) {
                                Iterator<o0> it4 = k0Var.A().iterator();
                                while (true) {
                                    if (it4.hasNext()) {
                                        if (!q.P(str, y2, it4.next())) {
                                            z2 = false;
                                            break;
                                        }
                                    } else {
                                        break;
                                    }
                                }
                            }
                            if (!z2) {
                                q.n();
                                q.b();
                                d.w(str);
                                SQLiteDatabase t8 = q.t();
                                str3 = str6;
                                t8.delete("property_filters", str3, new String[]{str, String.valueOf(y2)});
                                t8.delete("event_filters", str3, new String[]{str, String.valueOf(y2)});
                            } else {
                                str3 = str6;
                            }
                            str7 = str8;
                            str6 = str3;
                        }
                    }
                }
                str7 = str8;
            }
            ArrayList arrayList2 = new ArrayList();
            Iterator it5 = arrayList.iterator();
            while (it5.hasNext()) {
                k0 k0Var2 = (k0) it5.next();
                arrayList2.add(k0Var2.x() ? Integer.valueOf(k0Var2.y()) : null);
            }
            q.W(str, arrayList2);
            t6.setTransactionSuccessful();
            try {
                if (aVar2.l) {
                    aVar2.n();
                    aVar2.l = false;
                }
                u0.v((u0) aVar2.k);
                bArr2 = ((u0) ((u4) aVar2.p())).d();
            } catch (RuntimeException e) {
                g().i.c("Unable to serialize reduced-size config. Storing full config instead. appId", q3.s(str), e);
                bArr2 = bArr;
            }
            g q2 = q();
            d.w(str);
            q2.b();
            q2.n();
            ContentValues contentValues = new ContentValues();
            contentValues.put("remote_config", bArr2);
            try {
                if (q2.t().update("apps", contentValues, "app_id = ?", new String[]{str}) == 0) {
                    q2.g().f.b("Failed to update remote config (got 0). appId", q3.s(str));
                }
            } catch (SQLiteException e2) {
                q2.g().f.c("Error storing remote config. appId", q3.s(str), e2);
            }
            this.g.put(str, (u0) ((u4) aVar2.p()));
            return true;
        } finally {
            t6.endTransaction();
        }
    }

    @WorkerThread
    public final boolean x(String str, String str2) {
        Boolean bool;
        b();
        E(str);
        if ("1".equals(h(str, "measurement.upload.blacklist_internal")) && t9.r0(str2)) {
            return true;
        }
        if ("1".equals(h(str, "measurement.upload.blacklist_public")) && t9.X(str2)) {
            return true;
        }
        Map<String, Boolean> map = this.e.get(str);
        if (map == null || (bool = map.get(str2)) == null) {
            return false;
        }
        return bool.booleanValue();
    }

    @WorkerThread
    public final boolean y(String str, String str2) {
        Boolean bool;
        b();
        E(str);
        if ("ecommerce_purchase".equals(str2) || "purchase".equals(str2) || "refund".equals(str2)) {
            return true;
        }
        Map<String, Boolean> map = this.f.get(str);
        if (map == null || (bool = map.get(str2)) == null) {
            return false;
        }
        return bool.booleanValue();
    }

    @WorkerThread
    public final int z(String str, String str2) {
        Integer num;
        b();
        E(str);
        Map<String, Integer> map = this.h.get(str);
        if (map == null || (num = map.get(str2)) == null) {
            return 1;
        }
        return num.intValue();
    }
}
