package b.i.a.f.i.b;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import b.d.b.a.a;
import b.i.a.f.e.o.c;
import b.i.a.f.h.l.a1;
import b.i.a.f.h.l.b6;
import b.i.a.f.h.l.c1;
import b.i.a.f.h.l.d1;
import b.i.a.f.h.l.e1;
import b.i.a.f.h.l.g1;
import b.i.a.f.h.l.h1;
import b.i.a.f.h.l.h4;
import b.i.a.f.h.l.i1;
import b.i.a.f.h.l.i2;
import b.i.a.f.h.l.m0;
import b.i.a.f.h.l.n0;
import b.i.a.f.h.l.n3;
import b.i.a.f.h.l.o0;
import b.i.a.f.h.l.p0;
import b.i.a.f.h.l.q5;
import b.i.a.f.h.l.s4;
import b.i.a.f.h.l.u4;
import b.i.a.f.h.l.y0;
import b.i.a.f.h.l.y1;
import b.i.a.f.h.l.z0;
import com.discord.models.domain.ModelAuditLogEntry;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader$ParseException;
import com.google.android.gms.internal.measurement.zzij;
import com.google.android.gms.measurement.internal.zzap;
import com.google.android.gms.measurement.internal.zzaq;
import com.google.android.gms.measurement.internal.zzn;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
/* compiled from: com.google.android.gms:play-services-measurement@@18.0.0 */
/* loaded from: classes3.dex */
public final class q9 extends i9 {
    public q9(k9 k9Var) {
        super(k9Var);
    }

    public static String A(boolean z2, boolean z3, boolean z4) {
        StringBuilder sb = new StringBuilder();
        if (z2) {
            sb.append("Dynamic ");
        }
        if (z3) {
            sb.append("Sequence ");
        }
        if (z4) {
            sb.append("Session-Scoped ");
        }
        return sb.toString();
    }

    public static List<Long> B(BitSet bitSet) {
        int length = (bitSet.length() + 63) / 64;
        ArrayList arrayList = new ArrayList(length);
        for (int i = 0; i < length; i++) {
            long j = 0;
            for (int i2 = 0; i2 < 64; i2++) {
                int i3 = (i << 6) + i2;
                if (i3 < bitSet.length()) {
                    if (bitSet.get(i3)) {
                        j |= 1 << i2;
                    }
                }
            }
            arrayList.add(Long.valueOf(j));
        }
        return arrayList;
    }

    public static List<c1> D(Bundle[] bundleArr) {
        ArrayList arrayList = new ArrayList();
        for (Bundle bundle : bundleArr) {
            if (bundle != null) {
                c1.a Q = c1.Q();
                for (String str : bundle.keySet()) {
                    c1.a Q2 = c1.Q();
                    Q2.s(str);
                    Object obj = bundle.get(str);
                    if (obj instanceof Long) {
                        Q2.r(((Long) obj).longValue());
                    } else if (obj instanceof String) {
                        Q2.t((String) obj);
                    } else if (obj instanceof Double) {
                        Q2.q(((Double) obj).doubleValue());
                    }
                    if (Q.l) {
                        Q.n();
                        Q.l = false;
                    }
                    c1.x((c1) Q.k, (c1) ((u4) Q2.p()));
                }
                if (((c1) Q.k).P() > 0) {
                    arrayList.add((c1) ((u4) Q.p()));
                }
            }
        }
        return arrayList;
    }

    public static void E(a1.a aVar, String str, Object obj) {
        List<c1> v = aVar.v();
        int i = 0;
        while (true) {
            if (i >= v.size()) {
                i = -1;
                break;
            } else if (str.equals(v.get(i).B())) {
                break;
            } else {
                i++;
            }
        }
        c1.a Q = c1.Q();
        Q.s(str);
        if (obj instanceof Long) {
            Q.r(((Long) obj).longValue());
        } else if (obj instanceof String) {
            Q.t((String) obj);
        } else if (obj instanceof Double) {
            Q.q(((Double) obj).doubleValue());
        } else if (obj instanceof Bundle[]) {
            List<c1> D = D((Bundle[]) obj);
            if (Q.l) {
                Q.n();
                Q.l = false;
            }
            c1.y((c1) Q.k, D);
        }
        if (i >= 0) {
            if (aVar.l) {
                aVar.n();
                aVar.l = false;
            }
            a1.y((a1) aVar.k, i, (c1) ((u4) Q.p()));
            return;
        }
        aVar.s(Q);
    }

    public static void H(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            sb.append("  ");
        }
    }

    public static void J(StringBuilder sb, int i, String str, n0 n0Var) {
        if (n0Var != null) {
            H(sb, i);
            sb.append(str);
            sb.append(" {\n");
            if (n0Var.u()) {
                L(sb, i, "comparison_type", n0Var.v().name());
            }
            if (n0Var.w()) {
                L(sb, i, "match_as_float", Boolean.valueOf(n0Var.x()));
            }
            if (n0Var.y()) {
                L(sb, i, "comparison_value", n0Var.z());
            }
            if (n0Var.A()) {
                L(sb, i, "min_comparison_value", n0Var.B());
            }
            if (n0Var.C()) {
                L(sb, i, "max_comparison_value", n0Var.D());
            }
            H(sb, i);
            sb.append("}\n");
        }
    }

    public static void K(StringBuilder sb, int i, String str, g1 g1Var) {
        if (g1Var != null) {
            H(sb, 3);
            sb.append(str);
            sb.append(" {\n");
            if (g1Var.G() != 0) {
                H(sb, 4);
                sb.append("results: ");
                int i2 = 0;
                for (Long l : g1Var.E()) {
                    i2++;
                    if (i2 != 0) {
                        sb.append(", ");
                    }
                    sb.append(l);
                }
                sb.append('\n');
            }
            if (g1Var.z() != 0) {
                H(sb, 4);
                sb.append("status: ");
                int i3 = 0;
                for (Long l2 : g1Var.v()) {
                    i3++;
                    if (i3 != 0) {
                        sb.append(", ");
                    }
                    sb.append(l2);
                }
                sb.append('\n');
            }
            if (g1Var.J() != 0) {
                H(sb, 4);
                sb.append("dynamic_filter_timestamps: {");
                int i4 = 0;
                for (z0 z0Var : g1Var.I()) {
                    i4++;
                    if (i4 != 0) {
                        sb.append(", ");
                    }
                    sb.append(z0Var.w() ? Integer.valueOf(z0Var.x()) : null);
                    sb.append(":");
                    sb.append(z0Var.y() ? Long.valueOf(z0Var.z()) : null);
                }
                sb.append("}\n");
            }
            if (g1Var.L() != 0) {
                H(sb, 4);
                sb.append("sequence_filter_timestamps: {");
                int i5 = 0;
                for (h1 h1Var : g1Var.K()) {
                    i5++;
                    if (i5 != 0) {
                        sb.append(", ");
                    }
                    sb.append(h1Var.x() ? Integer.valueOf(h1Var.y()) : null);
                    sb.append(": [");
                    int i6 = 0;
                    for (Long l3 : h1Var.z()) {
                        long longValue = l3.longValue();
                        i6++;
                        if (i6 != 0) {
                            sb.append(", ");
                        }
                        sb.append(longValue);
                    }
                    sb.append("]");
                }
                sb.append("}\n");
            }
            H(sb, 3);
            sb.append("}\n");
        }
    }

    public static void L(StringBuilder sb, int i, String str, Object obj) {
        if (obj != null) {
            H(sb, i + 1);
            sb.append(str);
            sb.append(": ");
            sb.append(obj);
            sb.append('\n');
        }
    }

    @WorkerThread
    public static boolean O(zzaq zzaqVar, zzn zznVar) {
        Objects.requireNonNull(zzaqVar, "null reference");
        return !TextUtils.isEmpty(zznVar.k) || !TextUtils.isEmpty(zznVar.A);
    }

    public static boolean P(String str) {
        return str != null && str.matches("([+-])?([0-9]+\\.?[0-9]*|[0-9]*\\.?[0-9]+)") && str.length() <= 310;
    }

    public static boolean Q(List<Long> list, int i) {
        if (i >= (((q5) list).m << 6)) {
            return false;
        }
        return ((1 << (i % 64)) & ((Long) ((q5) list).get(i / 64)).longValue()) != 0;
    }

    public static Object R(a1 a1Var, String str) {
        c1 w = w(a1Var, str);
        if (w == null) {
            return null;
        }
        if (w.F()) {
            return w.G();
        }
        if (w.I()) {
            return Long.valueOf(w.J());
        }
        if (w.M()) {
            return Double.valueOf(w.N());
        }
        if (w.P() <= 0) {
            return null;
        }
        List<c1> O = w.O();
        ArrayList arrayList = new ArrayList();
        for (c1 c1Var : O) {
            if (c1Var != null) {
                Bundle bundle = new Bundle();
                for (c1 c1Var2 : c1Var.O()) {
                    if (c1Var2.F()) {
                        bundle.putString(c1Var2.B(), c1Var2.G());
                    } else if (c1Var2.I()) {
                        bundle.putLong(c1Var2.B(), c1Var2.J());
                    } else if (c1Var2.M()) {
                        bundle.putDouble(c1Var2.B(), c1Var2.N());
                    }
                }
                if (!bundle.isEmpty()) {
                    arrayList.add(bundle);
                }
            }
        }
        return (Bundle[]) arrayList.toArray(new Bundle[arrayList.size()]);
    }

    public static int s(e1.a aVar, String str) {
        for (int i = 0; i < ((e1) aVar.k).P0(); i++) {
            if (str.equals(((e1) aVar.k).h0(i).D())) {
                return i;
            }
        }
        return -1;
    }

    public static c1 w(a1 a1Var, String str) {
        for (c1 c1Var : a1Var.v()) {
            if (c1Var.B().equals(str)) {
                return c1Var;
            }
        }
        return null;
    }

    public static <Builder extends b6> Builder x(Builder builder, byte[] bArr) throws zzij {
        h4 h4Var = h4.f1442b;
        if (h4Var == null) {
            synchronized (h4.class) {
                h4Var = h4.f1442b;
                if (h4Var == null) {
                    h4Var = s4.b(h4.class);
                    h4.f1442b = h4Var;
                }
            }
        }
        if (h4Var != null) {
            n3 n3Var = (n3) builder;
            Objects.requireNonNull(n3Var);
            u4.b bVar = (u4.b) n3Var;
            bVar.m(bArr, bArr.length, h4Var);
            return bVar;
        }
        n3 n3Var2 = (n3) builder;
        Objects.requireNonNull(n3Var2);
        u4.b bVar2 = (u4.b) n3Var2;
        bVar2.m(bArr, bArr.length, h4.a());
        return bVar2;
    }

    public final List<Long> C(List<Long> list, List<Integer> list2) {
        int i;
        ArrayList arrayList = new ArrayList(list);
        for (Integer num : list2) {
            if (num.intValue() < 0) {
                g().i.b("Ignoring negative bit index to be cleared", num);
            } else {
                int intValue = num.intValue() / 64;
                if (intValue >= arrayList.size()) {
                    g().i.c("Ignoring bit index greater than bitSet size", num, Integer.valueOf(arrayList.size()));
                } else {
                    arrayList.set(intValue, Long.valueOf(((Long) arrayList.get(intValue)).longValue() & (~(1 << (num.intValue() % 64)))));
                }
            }
        }
        int size = arrayList.size();
        int size2 = arrayList.size() - 1;
        while (true) {
            size = size2;
            i = size;
            if (size < 0 || ((Long) arrayList.get(size)).longValue() != 0) {
                break;
            }
            size2 = size - 1;
        }
        return arrayList.subList(0, i);
    }

    public final void F(c1.a aVar, Object obj) {
        Objects.requireNonNull(obj, "null reference");
        if (aVar.l) {
            aVar.n();
            aVar.l = false;
        }
        c1.u((c1) aVar.k);
        if (aVar.l) {
            aVar.n();
            aVar.l = false;
        }
        c1.C((c1) aVar.k);
        if (aVar.l) {
            aVar.n();
            aVar.l = false;
        }
        c1.E((c1) aVar.k);
        if (aVar.l) {
            aVar.n();
            aVar.l = false;
        }
        c1.H((c1) aVar.k);
        if (obj instanceof String) {
            aVar.t((String) obj);
        } else if (obj instanceof Long) {
            aVar.r(((Long) obj).longValue());
        } else if (obj instanceof Double) {
            aVar.q(((Double) obj).doubleValue());
        } else if (obj instanceof Bundle[]) {
            List<c1> D = D((Bundle[]) obj);
            if (aVar.l) {
                aVar.n();
                aVar.l = false;
            }
            c1.y((c1) aVar.k, D);
        } else {
            g().f.b("Ignoring invalid (type) event param value", obj);
        }
    }

    public final void G(i1.a aVar, Object obj) {
        Objects.requireNonNull(obj, "null reference");
        if (aVar.l) {
            aVar.n();
            aVar.l = false;
        }
        i1.u((i1) aVar.k);
        if (aVar.l) {
            aVar.n();
            aVar.l = false;
        }
        i1.A((i1) aVar.k);
        if (aVar.l) {
            aVar.n();
            aVar.l = false;
        }
        i1.E((i1) aVar.k);
        if (obj instanceof String) {
            String str = (String) obj;
            if (aVar.l) {
                aVar.n();
                aVar.l = false;
            }
            i1.C((i1) aVar.k, str);
        } else if (obj instanceof Long) {
            aVar.s(((Long) obj).longValue());
        } else if (obj instanceof Double) {
            double doubleValue = ((Double) obj).doubleValue();
            if (aVar.l) {
                aVar.n();
                aVar.l = false;
            }
            i1.v((i1) aVar.k, doubleValue);
        } else {
            g().f.b("Ignoring invalid (type) user attribute value", obj);
        }
    }

    public final void I(StringBuilder sb, int i, m0 m0Var) {
        if (m0Var != null) {
            H(sb, i);
            sb.append("filter {\n");
            if (m0Var.z()) {
                L(sb, i, "complement", Boolean.valueOf(m0Var.A()));
            }
            if (m0Var.B()) {
                L(sb, i, "param_name", d().x(m0Var.C()));
            }
            if (m0Var.v()) {
                int i2 = i + 1;
                p0 w = m0Var.w();
                if (w != null) {
                    H(sb, i2);
                    sb.append("string_filter");
                    sb.append(" {\n");
                    if (w.u()) {
                        L(sb, i2, "match_type", w.v().name());
                    }
                    if (w.w()) {
                        L(sb, i2, "expression", w.x());
                    }
                    if (w.y()) {
                        L(sb, i2, "case_sensitive", Boolean.valueOf(w.z()));
                    }
                    if (w.B() > 0) {
                        H(sb, i2 + 1);
                        sb.append("expression_list {\n");
                        for (String str : w.A()) {
                            H(sb, i2 + 2);
                            sb.append(str);
                            sb.append("\n");
                        }
                        sb.append("}\n");
                    }
                    H(sb, i2);
                    sb.append("}\n");
                }
            }
            if (m0Var.x()) {
                J(sb, i + 1, "number_filter", m0Var.y());
            }
            H(sb, i);
            sb.append("}\n");
        }
    }

    public final void M(StringBuilder sb, int i, List<c1> list) {
        if (list != null) {
            int i2 = i + 1;
            for (c1 c1Var : list) {
                if (c1Var != null) {
                    H(sb, i2);
                    sb.append("param {\n");
                    Double d = null;
                    L(sb, i2, ModelAuditLogEntry.CHANGE_KEY_NAME, c1Var.A() ? d().x(c1Var.B()) : null);
                    L(sb, i2, "string_value", c1Var.F() ? c1Var.G() : null);
                    L(sb, i2, "int_value", c1Var.I() ? Long.valueOf(c1Var.J()) : null);
                    if (c1Var.M()) {
                        d = Double.valueOf(c1Var.N());
                    }
                    L(sb, i2, "double_value", d);
                    if (c1Var.P() > 0) {
                        M(sb, i2, c1Var.O());
                    }
                    H(sb, i2);
                    sb.append("}\n");
                }
            }
        }
    }

    public final boolean N(long j, long j2) {
        if (j == 0 || j2 <= 0) {
            return true;
        }
        Objects.requireNonNull((c) this.a.o);
        return Math.abs(System.currentTimeMillis() - j) > j2;
    }

    public final byte[] S(byte[] bArr) throws IOException {
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
            GZIPInputStream gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr2 = new byte[1024];
            while (true) {
                int read = gZIPInputStream.read(bArr2);
                if (read > 0) {
                    byteArrayOutputStream.write(bArr2, 0, read);
                } else {
                    gZIPInputStream.close();
                    byteArrayInputStream.close();
                    return byteArrayOutputStream.toByteArray();
                }
            }
        } catch (IOException e) {
            g().f.b("Failed to ungzip content", e);
            throw e;
        }
    }

    public final byte[] T(byte[] bArr) throws IOException {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gZIPOutputStream.write(bArr);
            gZIPOutputStream.close();
            byteArrayOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            g().f.b("Failed to gzip content", e);
            throw e;
        }
    }

    @Nullable
    public final List<Integer> U() {
        Context context = this.f1534b.k.f1566b;
        List<j3<?>> list = p.a;
        y1 a = y1.a(context.getContentResolver(), i2.a("com.google.android.gms.measurement"));
        Map<String, String> emptyMap = a == null ? Collections.emptyMap() : a.b();
        if (emptyMap == null || emptyMap.size() == 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        int intValue = p.P.a(null).intValue();
        for (Map.Entry<String, String> entry : emptyMap.entrySet()) {
            if (entry.getKey().startsWith("measurement.id.")) {
                try {
                    int parseInt = Integer.parseInt(entry.getValue());
                    if (parseInt != 0) {
                        arrayList.add(Integer.valueOf(parseInt));
                        if (arrayList.size() >= intValue) {
                            g().i.b("Too many experiment IDs. Number of IDs", Integer.valueOf(arrayList.size()));
                            break;
                        }
                        continue;
                    } else {
                        continue;
                    }
                } catch (NumberFormatException e) {
                    g().i.b("Experiment ID NumberFormatException", e);
                }
            }
        }
        if (arrayList.size() == 0) {
            return null;
        }
        return arrayList;
    }

    @Override // b.i.a.f.i.b.i9
    public final boolean p() {
        return false;
    }

    @WorkerThread
    public final long t(byte[] bArr) {
        e().b();
        MessageDigest x0 = t9.x0();
        if (x0 != null) {
            return t9.w(x0.digest(bArr));
        }
        g().f.a("Failed to get MD5");
        return 0L;
    }

    public final <T extends Parcelable> T u(byte[] bArr, Parcelable.Creator<T> creator) {
        if (bArr == null) {
            return null;
        }
        Parcel obtain = Parcel.obtain();
        try {
            obtain.unmarshall(bArr, 0, bArr.length);
            obtain.setDataPosition(0);
            return creator.createFromParcel(obtain);
        } catch (SafeParcelReader$ParseException unused) {
            g().f.a("Failed to load parcelable from buffer");
            return null;
        } finally {
            obtain.recycle();
        }
    }

    public final a1 v(m mVar) {
        a1.a M = a1.M();
        long j = mVar.e;
        if (M.l) {
            M.n();
            M.l = false;
        }
        a1.E((a1) M.k, j);
        zzap zzapVar = mVar.f;
        Objects.requireNonNull(zzapVar);
        for (String str : zzapVar.j.keySet()) {
            c1.a Q = c1.Q();
            Q.s(str);
            F(Q, mVar.f.w0(str));
            M.s(Q);
        }
        return (a1) ((u4) M.p());
    }

    public final String y(o0 o0Var) {
        StringBuilder R = a.R("\nproperty_filter {\n");
        if (o0Var.v()) {
            L(R, 0, "filter_id", Integer.valueOf(o0Var.w()));
        }
        L(R, 0, "property_name", d().y(o0Var.x()));
        String A = A(o0Var.z(), o0Var.A(), o0Var.C());
        if (!A.isEmpty()) {
            L(R, 0, "filter_type", A);
        }
        I(R, 1, o0Var.y());
        R.append("}\n");
        return R.toString();
    }

    public final String z(d1 d1Var) {
        StringBuilder R = a.R("\nbatch {\n");
        for (e1 e1Var : d1Var.v()) {
            if (e1Var != null) {
                H(R, 1);
                R.append("bundle {\n");
                if (e1Var.E()) {
                    L(R, 1, "protocol_version", Integer.valueOf(e1Var.g0()));
                }
                L(R, 1, "platform", e1Var.y1());
                if (e1Var.I1()) {
                    L(R, 1, "gmp_version", Long.valueOf(e1Var.F()));
                }
                if (e1Var.H()) {
                    L(R, 1, "uploading_gmp_version", Long.valueOf(e1Var.I()));
                }
                if (e1Var.p0()) {
                    L(R, 1, "dynamite_version", Long.valueOf(e1Var.q0()));
                }
                if (e1Var.a0()) {
                    L(R, 1, "config_version", Long.valueOf(e1Var.b0()));
                }
                L(R, 1, "gmp_app_id", e1Var.S());
                L(R, 1, "admob_app_id", e1Var.o0());
                L(R, 1, "app_id", e1Var.G1());
                L(R, 1, "app_version", e1Var.H1());
                if (e1Var.X()) {
                    L(R, 1, "app_version_major", Integer.valueOf(e1Var.Y()));
                }
                L(R, 1, "firebase_instance_id", e1Var.W());
                if (e1Var.N()) {
                    L(R, 1, "dev_cert_hash", Long.valueOf(e1Var.O()));
                }
                L(R, 1, "app_store", e1Var.F1());
                if (e1Var.Y0()) {
                    L(R, 1, "upload_timestamp_millis", Long.valueOf(e1Var.Z0()));
                }
                if (e1Var.g1()) {
                    L(R, 1, "start_timestamp_millis", Long.valueOf(e1Var.h1()));
                }
                if (e1Var.n1()) {
                    L(R, 1, "end_timestamp_millis", Long.valueOf(e1Var.o1()));
                }
                if (e1Var.s1()) {
                    L(R, 1, "previous_bundle_start_timestamp_millis", Long.valueOf(e1Var.t1()));
                }
                if (e1Var.v1()) {
                    L(R, 1, "previous_bundle_end_timestamp_millis", Long.valueOf(e1Var.w1()));
                }
                L(R, 1, "app_instance_id", e1Var.M());
                L(R, 1, "resettable_device_id", e1Var.J());
                L(R, 1, "device_id", e1Var.Z());
                L(R, 1, "ds_id", e1Var.e0());
                if (e1Var.K()) {
                    L(R, 1, "limited_ad_tracking", Boolean.valueOf(e1Var.L()));
                }
                L(R, 1, "os_version", e1Var.A1());
                L(R, 1, "device_model", e1Var.B1());
                L(R, 1, "user_default_language", e1Var.C1());
                if (e1Var.D1()) {
                    L(R, 1, "time_zone_offset_minutes", Integer.valueOf(e1Var.E1()));
                }
                if (e1Var.P()) {
                    L(R, 1, "bundle_sequential_index", Integer.valueOf(e1Var.Q()));
                }
                if (e1Var.T()) {
                    L(R, 1, "service_upload", Boolean.valueOf(e1Var.U()));
                }
                L(R, 1, "health_monitor", e1Var.R());
                if (!this.a.h.o(p.y0) && e1Var.c0() && e1Var.d0() != 0) {
                    L(R, 1, "android_id", Long.valueOf(e1Var.d0()));
                }
                if (e1Var.f0()) {
                    L(R, 1, "retry_counter", Integer.valueOf(e1Var.n0()));
                }
                if (e1Var.s0()) {
                    L(R, 1, "consent_signals", e1Var.t0());
                }
                List<i1> K0 = e1Var.K0();
                if (K0 != null) {
                    for (i1 i1Var : K0) {
                        if (i1Var != null) {
                            H(R, 2);
                            R.append("user_property {\n");
                            Double d = null;
                            L(R, 2, "set_timestamp_millis", i1Var.y() ? Long.valueOf(i1Var.z()) : null);
                            L(R, 2, ModelAuditLogEntry.CHANGE_KEY_NAME, d().y(i1Var.D()));
                            L(R, 2, "string_value", i1Var.G());
                            L(R, 2, "int_value", i1Var.H() ? Long.valueOf(i1Var.I()) : null);
                            if (i1Var.J()) {
                                d = Double.valueOf(i1Var.K());
                            }
                            L(R, 2, "double_value", d);
                            H(R, 2);
                            R.append("}\n");
                        }
                    }
                }
                List<y0> V = e1Var.V();
                if (V != null) {
                    for (y0 y0Var : V) {
                        if (y0Var != null) {
                            H(R, 2);
                            R.append("audience_membership {\n");
                            if (y0Var.x()) {
                                L(R, 2, "audience_id", Integer.valueOf(y0Var.y()));
                            }
                            if (y0Var.D()) {
                                L(R, 2, "new_audience", Boolean.valueOf(y0Var.E()));
                            }
                            K(R, 2, "current_data", y0Var.A());
                            if (y0Var.B()) {
                                K(R, 2, "previous_data", y0Var.C());
                            }
                            H(R, 2);
                            R.append("}\n");
                        }
                    }
                }
                List<a1> y0 = e1Var.y0();
                if (y0 != null) {
                    for (a1 a1Var : y0) {
                        if (a1Var != null) {
                            H(R, 2);
                            R.append("event {\n");
                            L(R, 2, ModelAuditLogEntry.CHANGE_KEY_NAME, d().u(a1Var.F()));
                            if (a1Var.G()) {
                                L(R, 2, "timestamp_millis", Long.valueOf(a1Var.H()));
                            }
                            if (a1Var.I()) {
                                L(R, 2, "previous_timestamp_millis", Long.valueOf(a1Var.J()));
                            }
                            if (a1Var.K()) {
                                L(R, 2, "count", Integer.valueOf(a1Var.L()));
                            }
                            if (a1Var.D() != 0) {
                                M(R, 2, a1Var.v());
                            }
                            H(R, 2);
                            R.append("}\n");
                        }
                    }
                }
                H(R, 1);
                R.append("}\n");
            }
        }
        R.append("}\n");
        return R.toString();
    }
}
