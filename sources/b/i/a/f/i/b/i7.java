package b.i.a.f.i.b;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class i7 {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1533b;
    public final long c;
    public boolean d;
    public final boolean e;
    public final long f;

    public i7(String str, String str2, long j) {
        this(str, str2, j, false, 0L);
    }

    public i7(String str, String str2, long j, boolean z2, long j2) {
        this.a = str;
        this.f1533b = str2;
        this.c = j;
        this.d = false;
        this.e = z2;
        this.f = j2;
    }
}
