package b.i.a.f.i.b;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import b.c.a.a0.d;
import b.d.b.a.a;
import b.i.a.f.e.e;
import b.i.a.f.e.o.c;
import b.i.a.f.e.o.f;
import b.i.a.f.e.p.b;
import b.i.a.f.h.l.da;
import b.i.a.f.h.l.fc;
import b.i.a.f.h.l.g8;
import b.i.a.f.h.l.h8;
import com.adjust.sdk.Constants;
import com.discord.models.domain.ModelAuditLogEntry;
import com.google.android.gms.measurement.internal.zzap;
import com.google.android.gms.measurement.internal.zzaq;
import com.google.android.gms.measurement.internal.zzku;
import com.google.android.gms.measurement.internal.zzz;
import java.io.ByteArrayInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import javax.security.auth.x500.X500Principal;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class t9 extends r5 {
    public static final String[] c = {"firebase_", "google_", "ga_"};
    public static final String[] d = {"_err"};
    public SecureRandom e;
    public int g;
    public Integer h = null;
    public final AtomicLong f = new AtomicLong(0);

    public t9(u4 u4Var) {
        super(u4Var);
    }

    public static Bundle A(List<zzku> list) {
        Bundle bundle = new Bundle();
        if (list == null) {
            return bundle;
        }
        for (zzku zzkuVar : list) {
            String str = zzkuVar.n;
            if (str != null) {
                bundle.putString(zzkuVar.k, str);
            } else {
                Long l = zzkuVar.m;
                if (l != null) {
                    bundle.putLong(zzkuVar.k, l.longValue());
                } else {
                    Double d2 = zzkuVar.p;
                    if (d2 != null) {
                        bundle.putDouble(zzkuVar.k, d2.doubleValue());
                    }
                }
            }
        }
        return bundle;
    }

    public static String E(String str, int i, boolean z2) {
        if (str == null) {
            return null;
        }
        if (str.codePointCount(0, str.length()) <= i) {
            return str;
        }
        if (z2) {
            return String.valueOf(str.substring(0, str.offsetByCodePoints(0, i))).concat("...");
        }
        return null;
    }

    public static void F(Bundle bundle, int i, String str, Object obj) {
        if (l0(bundle, i)) {
            bundle.putString("_ev", E(str, 40, true));
            if (obj == null) {
                return;
            }
            if ((obj instanceof String) || (obj instanceof CharSequence)) {
                bundle.putLong("_el", String.valueOf(obj).length());
            }
        }
    }

    public static boolean U(Context context) {
        Objects.requireNonNull(context, "null reference");
        if (Build.VERSION.SDK_INT >= 24) {
            return k0(context, "com.google.android.gms.measurement.AppMeasurementJobService");
        }
        return k0(context, "com.google.android.gms.measurement.AppMeasurementService");
    }

    public static boolean V(Intent intent) {
        String stringExtra = intent.getStringExtra("android.intent.extra.REFERRER_NAME");
        return "android-app://com.google.android.googlequicksearchbox/https/www.google.com".equals(stringExtra) || "https://www.google.com".equals(stringExtra) || "android-app://com.google.appcrawler".equals(stringExtra);
    }

    public static boolean W(Object obj) {
        return (obj instanceof Parcelable[]) || (obj instanceof ArrayList) || (obj instanceof Bundle);
    }

    public static boolean X(String str) {
        d.w(str);
        return str.charAt(0) != '_' || str.equals("_ep");
    }

    public static boolean d0(String str, String str2, String str3, String str4) {
        boolean isEmpty = TextUtils.isEmpty(str);
        boolean isEmpty2 = TextUtils.isEmpty(str2);
        if (!isEmpty && !isEmpty2) {
            return !str.equals(str2);
        }
        if (isEmpty && isEmpty2) {
            return (TextUtils.isEmpty(str3) || TextUtils.isEmpty(str4)) ? !TextUtils.isEmpty(str4) : !str3.equals(str4);
        }
        if (isEmpty || !isEmpty2) {
            return TextUtils.isEmpty(str3) || !str3.equals(str4);
        }
        if (TextUtils.isEmpty(str4)) {
            return false;
        }
        return TextUtils.isEmpty(str3) || !str3.equals(str4);
    }

    public static boolean e0(String str, String[] strArr) {
        for (String str2 : strArr) {
            if (q0(str, str2)) {
                return true;
            }
        }
        return false;
    }

    public static byte[] g0(Parcelable parcelable) {
        if (parcelable == null) {
            return null;
        }
        Parcel obtain = Parcel.obtain();
        try {
            parcelable.writeToParcel(obtain, 0);
            return obtain.marshall();
        } finally {
            obtain.recycle();
        }
    }

    public static ArrayList<Bundle> j0(List<zzz> list) {
        if (list == null) {
            return new ArrayList<>(0);
        }
        ArrayList<Bundle> arrayList = new ArrayList<>(list.size());
        for (zzz zzzVar : list) {
            Bundle bundle = new Bundle();
            bundle.putString("app_id", zzzVar.j);
            bundle.putString("origin", zzzVar.k);
            bundle.putLong("creation_timestamp", zzzVar.m);
            bundle.putString(ModelAuditLogEntry.CHANGE_KEY_NAME, zzzVar.l.k);
            f.d2(bundle, zzzVar.l.w0());
            bundle.putBoolean("active", zzzVar.n);
            String str = zzzVar.o;
            if (str != null) {
                bundle.putString("trigger_event_name", str);
            }
            zzaq zzaqVar = zzzVar.p;
            if (zzaqVar != null) {
                bundle.putString("timed_out_event_name", zzaqVar.j);
                zzap zzapVar = zzzVar.p.k;
                if (zzapVar != null) {
                    bundle.putBundle("timed_out_event_params", zzapVar.x0());
                }
            }
            bundle.putLong("trigger_timeout", zzzVar.q);
            zzaq zzaqVar2 = zzzVar.r;
            if (zzaqVar2 != null) {
                bundle.putString("triggered_event_name", zzaqVar2.j);
                zzap zzapVar2 = zzzVar.r.k;
                if (zzapVar2 != null) {
                    bundle.putBundle("triggered_event_params", zzapVar2.x0());
                }
            }
            bundle.putLong("triggered_timestamp", zzzVar.l.l);
            bundle.putLong("time_to_live", zzzVar.f2988s);
            zzaq zzaqVar3 = zzzVar.t;
            if (zzaqVar3 != null) {
                bundle.putString("expired_event_name", zzaqVar3.j);
                zzap zzapVar3 = zzzVar.t.k;
                if (zzapVar3 != null) {
                    bundle.putBundle("expired_event_params", zzapVar3.x0());
                }
            }
            arrayList.add(bundle);
        }
        return arrayList;
    }

    public static boolean k0(Context context, String str) {
        ServiceInfo serviceInfo;
        try {
            PackageManager packageManager = context.getPackageManager();
            if (!(packageManager == null || (serviceInfo = packageManager.getServiceInfo(new ComponentName(context, str), 0)) == null)) {
                if (serviceInfo.enabled) {
                    return true;
                }
            }
        } catch (PackageManager.NameNotFoundException unused) {
        }
        return false;
    }

    public static boolean l0(Bundle bundle, int i) {
        if (bundle == null || bundle.getLong("_err") != 0) {
            return false;
        }
        bundle.putLong("_err", i);
        return true;
    }

    public static boolean q0(String str, String str2) {
        if (str == null && str2 == null) {
            return true;
        }
        if (str == null) {
            return false;
        }
        return str.equals(str2);
    }

    public static boolean r0(String str) {
        return !TextUtils.isEmpty(str) && str.startsWith("_");
    }

    public static long u(long j, long j2) {
        return ((j2 * 60000) + j) / 86400000;
    }

    public static long v(zzap zzapVar) {
        long j = 0;
        if (zzapVar == null) {
            return 0L;
        }
        for (String str : zzapVar.j.keySet()) {
            Object w0 = zzapVar.w0(str);
            if (w0 instanceof Parcelable[]) {
                j += ((Parcelable[]) w0).length;
            }
        }
        return j;
    }

    public static long w(byte[] bArr) {
        Objects.requireNonNull(bArr, "null reference");
        int i = 0;
        d.F(bArr.length > 0);
        long j = 0;
        for (int length = bArr.length - 1; length >= 0 && length >= bArr.length - 8; length--) {
            j += (bArr[length] & 255) << i;
            i += 8;
        }
        return j;
    }

    public static MessageDigest x0() {
        MessageDigest messageDigest;
        for (int i = 0; i < 2; i++) {
            try {
                messageDigest = MessageDigest.getInstance(Constants.MD5);
            } catch (NoSuchAlgorithmException unused) {
            }
            if (messageDigest != null) {
                return messageDigest;
            }
        }
        return null;
    }

    public final zzaq B(String str, String str2, Bundle bundle, String str3, long j, boolean z2, boolean z3) {
        if (TextUtils.isEmpty(str2)) {
            return null;
        }
        if (t(str2, z3) == 0) {
            Bundle bundle2 = bundle != null ? new Bundle(bundle) : new Bundle();
            bundle2.putString("_o", str3);
            Bundle z4 = z(str, str2, bundle2, Collections.singletonList("_o"), false);
            if (z2) {
                z4 = y(z4);
            }
            return new zzaq(str2, new zzap(z4), str3, j);
        }
        g().f.b("Invalid conditional property event name", d().y(str2));
        throw new IllegalArgumentException();
    }

    public final Object C(int i, Object obj, boolean z2, boolean z3) {
        Parcelable[] parcelableArr;
        if (obj == null) {
            return null;
        }
        if ((obj instanceof Long) || (obj instanceof Double)) {
            return obj;
        }
        if (obj instanceof Integer) {
            return Long.valueOf(((Integer) obj).intValue());
        }
        if (obj instanceof Byte) {
            return Long.valueOf(((Byte) obj).byteValue());
        }
        if (obj instanceof Short) {
            return Long.valueOf(((Short) obj).shortValue());
        }
        if (obj instanceof Boolean) {
            return Long.valueOf(((Boolean) obj).booleanValue() ? 1L : 0L);
        } else if (obj instanceof Float) {
            return Double.valueOf(((Float) obj).doubleValue());
        } else {
            if ((obj instanceof String) || (obj instanceof Character) || (obj instanceof CharSequence)) {
                return E(String.valueOf(obj), i, z2);
            }
            if (!z3 || (!(obj instanceof Bundle[]) && !(obj instanceof Parcelable[]))) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            for (Parcelable parcelable : (Parcelable[]) obj) {
                if (parcelable instanceof Bundle) {
                    Bundle y2 = y((Bundle) parcelable);
                    if (!y2.isEmpty()) {
                        arrayList.add(y2);
                    }
                }
            }
            return arrayList.toArray(new Bundle[arrayList.size()]);
        }
    }

    public final Object D(String str, Object obj) {
        int i = 256;
        if ("_ev".equals(str)) {
            return C(256, obj, true, true);
        }
        if (!r0(str)) {
            i = 100;
        }
        return C(i, obj, false, true);
    }

    @WorkerThread
    public final void G(Bundle bundle, long j) {
        long j2 = bundle.getLong("_et");
        if (j2 != 0) {
            g().i.b("Params already contained engagement", Long.valueOf(j2));
        }
        bundle.putLong("_et", j + j2);
    }

    public final void H(Bundle bundle, Bundle bundle2) {
        if (bundle2 != null) {
            for (String str : bundle2.keySet()) {
                if (!bundle.containsKey(str)) {
                    e().I(bundle, str, bundle2.get(str));
                }
            }
        }
    }

    public final void I(Bundle bundle, String str, Object obj) {
        if (bundle != null) {
            if (obj instanceof Long) {
                bundle.putLong(str, ((Long) obj).longValue());
            } else if (obj instanceof String) {
                bundle.putString(str, String.valueOf(obj));
            } else if (obj instanceof Double) {
                bundle.putDouble(str, ((Double) obj).doubleValue());
            } else if (obj instanceof Bundle[]) {
                bundle.putParcelableArray(str, (Bundle[]) obj);
            } else if (str != null) {
                g().k.c("Not putting event parameter. Invalid value type. name, type", d().x(str), obj != null ? obj.getClass().getSimpleName() : null);
            }
        }
    }

    public final void J(fc fcVar, int i) {
        Bundle bundle = new Bundle();
        bundle.putInt("r", i);
        try {
            fcVar.f(bundle);
        } catch (RemoteException e) {
            this.a.g().i.b("Error returning int value to wrapper", e);
        }
    }

    public final void K(fc fcVar, long j) {
        try {
            fcVar.f(a.I("r", j));
        } catch (RemoteException e) {
            this.a.g().i.b("Error returning long value to wrapper", e);
        }
    }

    public final void L(fc fcVar, Bundle bundle) {
        try {
            fcVar.f(bundle);
        } catch (RemoteException e) {
            this.a.g().i.b("Error returning bundle value to wrapper", e);
        }
    }

    public final void M(fc fcVar, String str) {
        Bundle bundle = new Bundle();
        bundle.putString("r", str);
        try {
            fcVar.f(bundle);
        } catch (RemoteException e) {
            this.a.g().i.b("Error returning string value to wrapper", e);
        }
    }

    public final void N(fc fcVar, ArrayList<Bundle> arrayList) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("r", arrayList);
        try {
            fcVar.f(bundle);
        } catch (RemoteException e) {
            this.a.g().i.b("Error returning bundle list to wrapper", e);
        }
    }

    public final void O(fc fcVar, boolean z2) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("r", z2);
        try {
            fcVar.f(bundle);
        } catch (RemoteException e) {
            this.a.g().i.b("Error returning boolean value to wrapper", e);
        }
    }

    public final void P(fc fcVar, byte[] bArr) {
        Bundle bundle = new Bundle();
        bundle.putByteArray("r", bArr);
        try {
            fcVar.f(bundle);
        } catch (RemoteException e) {
            this.a.g().i.b("Error returning byte array to wrapper", e);
        }
    }

    public final void Q(u3 u3Var, int i) {
        Iterator it = new TreeSet(u3Var.d.keySet()).iterator();
        int i2 = 0;
        while (it.hasNext()) {
            String str = (String) it.next();
            if (X(str) && (i2 = i2 + 1) > i) {
                StringBuilder sb = new StringBuilder(48);
                sb.append("Event can't contain more than ");
                sb.append(i);
                sb.append(" params");
                g().h.c(sb.toString(), d().u(u3Var.a), d().s(u3Var.d));
                l0(u3Var.d, 5);
                u3Var.d.remove(str);
            }
        }
    }

    public final void R(v9 v9Var, int i, String str, String str2, int i2) {
        S(v9Var, null, i, str, str2, i2);
    }

    public final void S(v9 v9Var, String str, int i, String str2, String str3, int i2) {
        Bundle bundle = new Bundle();
        l0(bundle, i);
        if (!TextUtils.isEmpty(str2) && !TextUtils.isEmpty(str3)) {
            bundle.putString(str2, str3);
        }
        if (i == 6 || i == 7 || i == 2) {
            bundle.putLong("_el", i2);
        }
        if (!((g8) h8.j.a()).a() || !this.a.h.o(p.O0)) {
            this.a.s().H("auto", "_err", bundle);
        } else {
            v9Var.a(str, bundle);
        }
    }

    public final void T(String str, String str2, String str3, Bundle bundle, @Nullable List<String> list, boolean z2) {
        int i;
        String str4;
        int i2;
        if (bundle != null) {
            Iterator it = new TreeSet(bundle.keySet()).iterator();
            int i3 = 0;
            while (it.hasNext()) {
                String str5 = (String) it.next();
                if (list == null || !list.contains(str5)) {
                    i = z2 ? u0(str5) : 0;
                    if (i == 0) {
                        i = w0(str5);
                    }
                } else {
                    i = 0;
                }
                if (i != 0) {
                    F(bundle, i, str5, i == 3 ? str5 : null);
                    bundle.remove(str5);
                } else {
                    if (W(bundle.get(str5))) {
                        g().k.d("Nested Bundle parameters are not allowed; discarded. event name, param name, child param name", str2, str3, str5);
                        i2 = 22;
                        str4 = str5;
                    } else {
                        str4 = str5;
                        i2 = s(str, str2, str5, bundle.get(str5), bundle, list, z2, false);
                    }
                    if (i2 != 0 && !"_ev".equals(str4)) {
                        F(bundle, i2, str4, bundle.get(str4));
                        bundle.remove(str4);
                    } else if (X(str4) && !e0(str4, u5.d) && (i3 = i3 + 1) > 0) {
                        g().h.c("Item cannot contain custom parameters", d().u(str2), d().s(bundle));
                        l0(bundle, 23);
                        bundle.remove(str4);
                    }
                }
            }
        }
    }

    @SuppressLint({"ApplySharedPref"})
    public final boolean Y(String str, double d2) {
        try {
            SharedPreferences.Editor edit = this.a.f1566b.getSharedPreferences("google.analytics.deferred.deeplink.prefs", 0).edit();
            edit.putString(Constants.DEEPLINK, str);
            edit.putLong("timestamp", Double.doubleToRawLongBits(d2));
            return edit.commit();
        } catch (Exception e) {
            g().f.b("Failed to persist Deferred Deep Link. exception", e);
            return false;
        }
    }

    public final boolean Z(String str, int i, String str2) {
        if (str2 == null) {
            g().h.b("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.codePointCount(0, str2.length()) <= i) {
            return true;
        } else {
            g().h.d("Name is too long. Type, maximum supported length, name", str, Integer.valueOf(i), str2);
            return false;
        }
    }

    public final boolean a0(String str, String str2) {
        if (str2 == null) {
            g().h.b("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.length() == 0) {
            g().h.b("Name is required and can't be empty. Type", str);
            return false;
        } else {
            int codePointAt = str2.codePointAt(0);
            if (!Character.isLetter(codePointAt)) {
                g().h.c("Name must start with a letter. Type, name", str, str2);
                return false;
            }
            int length = str2.length();
            int charCount = Character.charCount(codePointAt);
            while (charCount < length) {
                int codePointAt2 = str2.codePointAt(charCount);
                if (codePointAt2 == 95 || Character.isLetterOrDigit(codePointAt2)) {
                    charCount += Character.charCount(codePointAt2);
                } else {
                    g().h.c("Name must consist of letters, digits or _ (underscores). Type, name", str, str2);
                    return false;
                }
            }
            return true;
        }
    }

    public final boolean b0(String str, String str2, int i, Object obj) {
        if (obj != null && !(obj instanceof Long) && !(obj instanceof Float) && !(obj instanceof Integer) && !(obj instanceof Byte) && !(obj instanceof Short) && !(obj instanceof Boolean) && !(obj instanceof Double)) {
            if (!(obj instanceof String) && !(obj instanceof Character) && !(obj instanceof CharSequence)) {
                return false;
            }
            String valueOf = String.valueOf(obj);
            if (valueOf.codePointCount(0, valueOf.length()) > i) {
                g().k.d("Value is too long; discarded. Value kind, name, value length", str, str2, Integer.valueOf(valueOf.length()));
                return false;
            }
        }
        return true;
    }

    public final boolean c0(String str, String str2, String str3) {
        if (!TextUtils.isEmpty(str)) {
            Objects.requireNonNull(str, "null reference");
            if (str.matches("^(1:\\d+:android:[a-f0-9]+|ca-app-pub-.*)$")) {
                return true;
            }
            if (this.a.v()) {
                g().h.b("Invalid google_app_id. Firebase Analytics disabled. See https://goo.gl/NAOOOI. provided id", q3.s(str));
            }
            return false;
        } else if (da.b() && this.a.h.o(p.f1557j0) && !TextUtils.isEmpty(str3)) {
            return true;
        } else {
            if (!TextUtils.isEmpty(str2)) {
                Objects.requireNonNull(str2, "null reference");
                if (str2.matches("^(1:\\d+:android:[a-f0-9]+|ca-app-pub-.*)$")) {
                    return true;
                }
                g().h.b("Invalid admob_app_id. Analytics disabled.", q3.s(str2));
                return false;
            }
            if (this.a.v()) {
                g().h.a("Missing google_app_id. Firebase Analytics disabled. See https://goo.gl/NAOOOI");
            }
            return false;
        }
    }

    public final boolean f0(String str, String[] strArr, String[] strArr2, String str2) {
        boolean z2;
        if (str2 == null) {
            g().h.b("Name is required and can't be null. Type", str);
            return false;
        }
        String[] strArr3 = c;
        int length = strArr3.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z2 = false;
                break;
            } else if (str2.startsWith(strArr3[i])) {
                z2 = true;
                break;
            } else {
                i++;
            }
        }
        if (z2) {
            g().h.c("Name starts with reserved prefix. Type, name", str, str2);
            return false;
        } else if (strArr == null || !e0(str2, strArr) || (strArr2 != null && e0(str2, strArr2))) {
            return true;
        } else {
            g().h.c("Name is reserved. Type, name", str, str2);
            return false;
        }
    }

    public final int h0(String str) {
        if (!m0("user property", str)) {
            return 6;
        }
        if (!f0("user property", x5.a, null, str)) {
            return 15;
        }
        return !Z("user property", 24, str) ? 6 : 0;
    }

    public final int i0(String str, Object obj) {
        boolean z2;
        if ("_ldl".equals(str)) {
            z2 = b0("user property referrer", str, z0(str), obj);
        } else {
            z2 = b0("user property", str, z0(str), obj);
        }
        return z2 ? 0 : 7;
    }

    @Override // b.i.a.f.i.b.r5
    @WorkerThread
    public final void m() {
        b();
        SecureRandom secureRandom = new SecureRandom();
        long nextLong = secureRandom.nextLong();
        if (nextLong == 0) {
            nextLong = secureRandom.nextLong();
            if (nextLong == 0) {
                g().i.a("Utils falling back to Random for random id");
            }
        }
        this.f.set(nextLong);
    }

    public final boolean m0(String str, String str2) {
        if (str2 == null) {
            g().h.b("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.length() == 0) {
            g().h.b("Name is required and can't be empty. Type", str);
            return false;
        } else {
            int codePointAt = str2.codePointAt(0);
            if (Character.isLetter(codePointAt) || codePointAt == 95) {
                int length = str2.length();
                int charCount = Character.charCount(codePointAt);
                while (charCount < length) {
                    int codePointAt2 = str2.codePointAt(charCount);
                    if (codePointAt2 == 95 || Character.isLetterOrDigit(codePointAt2)) {
                        charCount += Character.charCount(codePointAt2);
                    } else {
                        g().h.c("Name must consist of letters, digits or _ (underscores). Type, name", str, str2);
                        return false;
                    }
                }
                return true;
            }
            g().h.c("Name must start with a letter or _ (underscore). Type, name", str, str2);
            return false;
        }
    }

    public final Object n0(String str, Object obj) {
        if ("_ldl".equals(str)) {
            return C(z0(str), obj, true, false);
        }
        return C(z0(str), obj, false, false);
    }

    public final boolean o0(Context context, String str) {
        Signature[] signatureArr;
        X500Principal x500Principal = new X500Principal("CN=Android Debug,O=Android,C=US");
        try {
            PackageInfo packageInfo = b.a(context).a.getPackageManager().getPackageInfo(str, 64);
            if (packageInfo == null || (signatureArr = packageInfo.signatures) == null || signatureArr.length <= 0) {
                return true;
            }
            return ((X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(signatureArr[0].toByteArray()))).getSubjectX500Principal().equals(x500Principal);
        } catch (PackageManager.NameNotFoundException e) {
            g().f.b("Package name not found", e);
            return true;
        } catch (CertificateException e2) {
            g().f.b("Error obtaining certificate", e2);
            return true;
        }
    }

    @WorkerThread
    public final boolean p0(String str) {
        b();
        if (b.a(this.a.f1566b).a.checkCallingOrSelfPermission(str) == 0) {
            return true;
        }
        g().m.b("Permission not granted", str);
        return false;
    }

    @Override // b.i.a.f.i.b.r5
    public final boolean r() {
        return true;
    }

    /* JADX WARN: Removed duplicated region for block: B:26:0x0076  */
    /* JADX WARN: Removed duplicated region for block: B:48:0x00cf A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:49:0x00d0  */
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final int s(java.lang.String r17, java.lang.String r18, java.lang.String r19, java.lang.Object r20, android.os.Bundle r21, @androidx.annotation.Nullable java.util.List<java.lang.String> r22, boolean r23, boolean r24) {
        /*
            Method dump skipped, instructions count: 354
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.t9.s(java.lang.String, java.lang.String, java.lang.String, java.lang.Object, android.os.Bundle, java.util.List, boolean, boolean):int");
    }

    public final boolean s0(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        return this.a.h.h("debug.firebase.analytics.app", "").equals(str);
    }

    public final int t(String str, boolean z2) {
        if (!m0("event", str)) {
            return 2;
        }
        if (z2) {
            if (!f0("event", v5.a, v5.f1573b, str)) {
                return 13;
            }
        } else if (!f0("event", v5.a, null, str)) {
            return 13;
        }
        return !Z("event", 40, str) ? 2 : 0;
    }

    public final long t0() {
        long andIncrement;
        long j;
        if (this.f.get() == 0) {
            synchronized (this.f) {
                long nanoTime = System.nanoTime();
                Objects.requireNonNull((c) this.a.o);
                long nextLong = new Random(nanoTime ^ System.currentTimeMillis()).nextLong();
                int i = this.g + 1;
                this.g = i;
                j = nextLong + i;
            }
            return j;
        }
        synchronized (this.f) {
            this.f.compareAndSet(-1L, 1L);
            andIncrement = this.f.getAndIncrement();
        }
        return andIncrement;
    }

    public final int u0(String str) {
        if (!a0("event param", str)) {
            return 3;
        }
        if (!f0("event param", null, null, str)) {
            return 14;
        }
        return !Z("event param", 40, str) ? 3 : 0;
    }

    @WorkerThread
    public final SecureRandom v0() {
        b();
        if (this.e == null) {
            this.e = new SecureRandom();
        }
        return this.e;
    }

    public final int w0(String str) {
        if (!m0("event param", str)) {
            return 3;
        }
        if (!f0("event param", null, null, str)) {
            return 14;
        }
        return !Z("event param", 40, str) ? 3 : 0;
    }

    public final Bundle x(@NonNull Uri uri) {
        String str;
        String str2;
        String str3;
        String str4;
        if (uri == null) {
            return null;
        }
        try {
            if (uri.isHierarchical()) {
                str4 = uri.getQueryParameter("utm_campaign");
                str3 = uri.getQueryParameter("utm_source");
                str2 = uri.getQueryParameter("utm_medium");
                str = uri.getQueryParameter("gclid");
            } else {
                str4 = null;
                str3 = null;
                str2 = null;
                str = null;
            }
            if (TextUtils.isEmpty(str4) && TextUtils.isEmpty(str3) && TextUtils.isEmpty(str2) && TextUtils.isEmpty(str)) {
                return null;
            }
            Bundle bundle = new Bundle();
            if (!TextUtils.isEmpty(str4)) {
                bundle.putString("campaign", str4);
            }
            if (!TextUtils.isEmpty(str3)) {
                bundle.putString("source", str3);
            }
            if (!TextUtils.isEmpty(str2)) {
                bundle.putString(Constants.MEDIUM, str2);
            }
            if (!TextUtils.isEmpty(str)) {
                bundle.putString("gclid", str);
            }
            String queryParameter = uri.getQueryParameter("utm_term");
            if (!TextUtils.isEmpty(queryParameter)) {
                bundle.putString("term", queryParameter);
            }
            String queryParameter2 = uri.getQueryParameter("utm_content");
            if (!TextUtils.isEmpty(queryParameter2)) {
                bundle.putString("content", queryParameter2);
            }
            String queryParameter3 = uri.getQueryParameter("aclid");
            if (!TextUtils.isEmpty(queryParameter3)) {
                bundle.putString("aclid", queryParameter3);
            }
            String queryParameter4 = uri.getQueryParameter("cp1");
            if (!TextUtils.isEmpty(queryParameter4)) {
                bundle.putString("cp1", queryParameter4);
            }
            String queryParameter5 = uri.getQueryParameter("anid");
            if (!TextUtils.isEmpty(queryParameter5)) {
                bundle.putString("anid", queryParameter5);
            }
            return bundle;
        } catch (UnsupportedOperationException e) {
            g().i.b("Install referrer url isn't a hierarchical URI", e);
            return null;
        }
    }

    public final Bundle y(Bundle bundle) {
        Bundle bundle2 = new Bundle();
        if (bundle != null) {
            for (String str : bundle.keySet()) {
                Object D = D(str, bundle.get(str));
                if (D == null) {
                    g().k.b("Param value can't be null", d().x(str));
                } else {
                    I(bundle2, str, D);
                }
            }
        }
        return bundle2;
    }

    public final int y0() {
        if (this.h == null) {
            b.i.a.f.e.c cVar = b.i.a.f.e.c.f1342b;
            Context context = this.a.f1566b;
            Objects.requireNonNull(cVar);
            AtomicBoolean atomicBoolean = e.a;
            int i = 0;
            try {
                i = context.getPackageManager().getPackageInfo("com.google.android.gms", 0).versionCode;
            } catch (PackageManager.NameNotFoundException unused) {
                Log.w("GooglePlayServicesUtil", "Google Play services is missing.");
            }
            this.h = Integer.valueOf(i / 1000);
        }
        return this.h.intValue();
    }

    public final Bundle z(String str, String str2, Bundle bundle, @Nullable List list, boolean z2) {
        Set<String> set;
        int i;
        int i2;
        t9 t9Var = this;
        boolean e02 = e0(str2, v5.d);
        if (bundle == null) {
            return null;
        }
        Bundle bundle2 = new Bundle(bundle);
        int t = t9Var.a.h.t();
        if (t9Var.a.h.u(str, p.Z)) {
            set = new TreeSet<>(bundle.keySet());
        } else {
            set = bundle.keySet();
        }
        int i3 = 0;
        for (String str3 : set) {
            if (list == null || !list.contains(str3)) {
                i = z2 ? t9Var.u0(str3) : 0;
                if (i == 0) {
                    i = t9Var.w0(str3);
                }
            } else {
                i = 0;
            }
            if (i != 0) {
                F(bundle2, i, str3, i == 3 ? str3 : null);
                bundle2.remove(str3);
                i2 = t;
            } else {
                i2 = t;
                int s2 = s(str, str2, str3, bundle.get(str3), bundle2, list, z2, e02);
                if (s2 == 17) {
                    F(bundle2, s2, str3, Boolean.FALSE);
                } else if (s2 != 0 && !"_ev".equals(str3)) {
                    F(bundle2, s2, s2 == 21 ? str2 : str3, bundle.get(str3));
                    bundle2.remove(str3);
                }
                if (X(str3)) {
                    int i4 = i3 + 1;
                    if (i4 > i2) {
                        StringBuilder sb = new StringBuilder(48);
                        sb.append("Event can't contain more than ");
                        sb.append(i2);
                        sb.append(" params");
                        g().h.c(sb.toString(), d().u(str2), d().s(bundle));
                        l0(bundle2, 5);
                        bundle2.remove(str3);
                        i3 = i4;
                        t = i2;
                        t9Var = this;
                    } else {
                        i3 = i4;
                    }
                }
            }
            t = i2;
            t9Var = this;
        }
        return bundle2;
    }

    public final int z0(String str) {
        if ("_ldl".equals(str)) {
            return 2048;
        }
        if ("_id".equals(str)) {
            return 256;
        }
        return (!this.a.h.o(p.f1555h0) || !"_lgclid".equals(str)) ? 36 : 100;
    }
}
