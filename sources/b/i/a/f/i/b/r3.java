package b.i.a.f.i.b;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import androidx.annotation.NonNull;
import b.i.a.f.e.c;
import b.i.a.f.e.k.b;
import b.i.a.f.e.k.e;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class r3 extends b<i3> {
    public r3(Context context, Looper looper, b.a aVar, b.AbstractC0113b bVar) {
        super(context, looper, e.a(context), c.f1342b, 93, aVar, bVar, null);
    }

    @Override // b.i.a.f.e.k.b, b.i.a.f.e.h.a.f
    public final int l() {
        return 12451000;
    }

    @Override // b.i.a.f.e.k.b
    public final /* synthetic */ i3 r(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.internal.IMeasurementService");
        if (queryLocalInterface instanceof i3) {
            return (i3) queryLocalInterface;
        }
        return new k3(iBinder);
    }

    @Override // b.i.a.f.e.k.b
    @NonNull
    public final String x() {
        return "com.google.android.gms.measurement.internal.IMeasurementService";
    }

    @Override // b.i.a.f.e.k.b
    @NonNull
    public final String y() {
        return "com.google.android.gms.measurement.START";
    }
}
