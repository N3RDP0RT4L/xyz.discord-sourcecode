package b.i.a.f.i.b;

import android.content.Context;
import b.i.a.f.e.o.b;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public class s5 implements t5 {
    public final u4 a;

    public s5(u4 u4Var) {
        Objects.requireNonNull(u4Var, "null reference");
        this.a = u4Var;
    }

    public void a() {
        this.a.f().a();
    }

    public void b() {
        this.a.f().b();
    }

    public j c() {
        return this.a.y();
    }

    public o3 d() {
        return this.a.u();
    }

    public t9 e() {
        return this.a.t();
    }

    @Override // b.i.a.f.i.b.t5
    public r4 f() {
        return this.a.f();
    }

    @Override // b.i.a.f.i.b.t5
    public q3 g() {
        return this.a.g();
    }

    @Override // b.i.a.f.i.b.t5
    public b i() {
        return this.a.o;
    }

    @Override // b.i.a.f.i.b.t5
    public Context j() {
        return this.a.f1566b;
    }

    @Override // b.i.a.f.i.b.t5
    public ga k() {
        return this.a.g;
    }

    public d4 l() {
        return this.a.o();
    }
}
