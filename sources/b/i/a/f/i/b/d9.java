package b.i.a.f.i.b;

import android.os.Bundle;
import android.os.SystemClock;
import androidx.annotation.WorkerThread;
import b.i.a.f.e.o.c;
import b.i.a.f.h.l.r9;
import b.i.a.f.h.l.s9;
import b.i.a.f.h.l.v9;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class d9 {
    public long a;

    /* renamed from: b  reason: collision with root package name */
    public long f1524b;
    public final i c;
    public final /* synthetic */ w8 d;

    public d9(w8 w8Var) {
        this.d = w8Var;
        this.c = new c9(this, w8Var.a);
        Objects.requireNonNull((c) w8Var.a.o);
        long elapsedRealtime = SystemClock.elapsedRealtime();
        this.a = elapsedRealtime;
        this.f1524b = elapsedRealtime;
    }

    @WorkerThread
    public final boolean a(boolean z2, boolean z3, long j) {
        this.d.b();
        this.d.t();
        if (!r9.b() || !this.d.a.h.o(p.q0) || this.d.a.d()) {
            h4 h4Var = this.d.l().v;
            Objects.requireNonNull((c) this.d.a.o);
            h4Var.b(System.currentTimeMillis());
        }
        long j2 = j - this.a;
        if (z2 || j2 >= 1000) {
            if (this.d.a.h.o(p.T) && !z3) {
                if (!((v9) s9.j.a()).a() || !this.d.a.h.o(p.V)) {
                    j2 = b();
                } else {
                    j2 = j - this.f1524b;
                    this.f1524b = j;
                }
            }
            this.d.g().n.b("Recording user engagement, ms", Long.valueOf(j2));
            Bundle bundle = new Bundle();
            bundle.putLong("_et", j2);
            h7.A(this.d.q().w(!this.d.a.h.z().booleanValue()), bundle, true);
            if (this.d.a.h.o(p.T) && !this.d.a.h.o(p.U) && z3) {
                bundle.putLong("_fr", 1L);
            }
            if (!this.d.a.h.o(p.U) || !z3) {
                this.d.n().H("auto", "_e", bundle);
            }
            this.a = j;
            this.c.c();
            this.c.b(3600000L);
            return true;
        }
        this.d.g().n.b("Screen exposed for less than 1000 ms. Event not sent. time", Long.valueOf(j2));
        return false;
    }

    @WorkerThread
    public final long b() {
        Objects.requireNonNull((c) this.d.a.o);
        long elapsedRealtime = SystemClock.elapsedRealtime();
        long j = elapsedRealtime - this.f1524b;
        this.f1524b = elapsedRealtime;
        return j;
    }
}
