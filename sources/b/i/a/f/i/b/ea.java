package b.i.a.f.i.b;

import b.i.a.f.h.l.n0;
import b.i.a.f.h.l.p0;
import com.google.android.material.shadow.ShadowDrawableWrapper;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
/* compiled from: com.google.android.gms:play-services-measurement@@18.0.0 */
/* loaded from: classes3.dex */
public abstract class ea {
    public String a;

    /* renamed from: b  reason: collision with root package name */
    public int f1527b;
    public Boolean c;
    public Boolean d;
    public Long e;
    public Long f;

    public ea(String str, int i) {
        this.a = str;
        this.f1527b = i;
    }

    public static Boolean b(long j, n0 n0Var) {
        try {
            return f(new BigDecimal(j), n0Var, ShadowDrawableWrapper.COS_45);
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    public static Boolean c(Boolean bool, boolean z2) {
        if (bool == null) {
            return null;
        }
        return Boolean.valueOf(bool.booleanValue() != z2);
    }

    public static Boolean d(String str, n0 n0Var) {
        if (!q9.P(str)) {
            return null;
        }
        try {
            return f(new BigDecimal(str), n0Var, ShadowDrawableWrapper.COS_45);
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    public static Boolean e(String str, p0 p0Var, q3 q3Var) {
        String str2;
        List<String> list;
        p0.b bVar = p0.b.REGEXP;
        p0.b bVar2 = p0.b.IN_LIST;
        Objects.requireNonNull(p0Var, "null reference");
        if (str == null || !p0Var.u() || p0Var.v() == p0.b.UNKNOWN_MATCH_TYPE) {
            return null;
        }
        if (p0Var.v() == bVar2) {
            if (p0Var.B() == 0) {
                return null;
            }
        } else if (!p0Var.w()) {
            return null;
        }
        p0.b v = p0Var.v();
        boolean z2 = p0Var.z();
        if (z2 || v == bVar || v == bVar2) {
            str2 = p0Var.x();
        } else {
            str2 = p0Var.x().toUpperCase(Locale.ENGLISH);
        }
        if (p0Var.B() == 0) {
            list = null;
        } else {
            list = p0Var.A();
            if (!z2) {
                ArrayList arrayList = new ArrayList(list.size());
                for (String str3 : list) {
                    arrayList.add(str3.toUpperCase(Locale.ENGLISH));
                }
                list = Collections.unmodifiableList(arrayList);
            }
        }
        String str4 = v == bVar ? str2 : null;
        if (v == bVar2) {
            if (list == null || list.size() == 0) {
                return null;
            }
        } else if (str2 == null) {
            return null;
        }
        if (!z2 && v != bVar) {
            str = str.toUpperCase(Locale.ENGLISH);
        }
        switch (aa.a[v.ordinal()]) {
            case 1:
                try {
                    return Boolean.valueOf(Pattern.compile(str4, z2 ? 0 : 66).matcher(str).matches());
                } catch (PatternSyntaxException unused) {
                    if (q3Var == null) {
                        return null;
                    }
                    q3Var.i.b("Invalid regular expression in REGEXP audience filter. expression", str4);
                    return null;
                }
            case 2:
                return Boolean.valueOf(str.startsWith(str2));
            case 3:
                return Boolean.valueOf(str.endsWith(str2));
            case 4:
                return Boolean.valueOf(str.contains(str2));
            case 5:
                return Boolean.valueOf(str.equals(str2));
            case 6:
                return Boolean.valueOf(list.contains(str));
            default:
                return null;
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:33:0x0082, code lost:
        if (r3 != null) goto L34;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static java.lang.Boolean f(java.math.BigDecimal r9, b.i.a.f.h.l.n0 r10, double r11) {
        /*
            Method dump skipped, instructions count: 270
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.ea.f(java.math.BigDecimal, b.i.a.f.h.l.n0, double):java.lang.Boolean");
    }

    public abstract int a();

    public abstract boolean g();

    public abstract boolean h();
}
