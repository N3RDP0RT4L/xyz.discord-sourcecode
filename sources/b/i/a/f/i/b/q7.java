package b.i.a.f.i.b;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Looper;
import android.os.SystemClock;
import androidx.annotation.WorkerThread;
import b.i.a.f.e.n.a;
import b.i.a.f.e.o.c;
import com.google.android.gms.measurement.internal.zzz;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import org.objectweb.asm.Opcodes;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class q7 extends a5 {
    public i3 d;
    public volatile Boolean e;
    public final i f;
    public final e9 g;
    public final i i;
    public final List<Runnable> h = new ArrayList();
    public final k8 c = new k8(this);

    public q7(u4 u4Var) {
        super(u4Var);
        this.g = new e9(u4Var.o);
        this.f = new p7(this, u4Var);
        this.i = new z7(this, u4Var);
    }

    public static void x(q7 q7Var, ComponentName componentName) {
        q7Var.b();
        if (q7Var.d != null) {
            q7Var.d = null;
            q7Var.g().n.b("Disconnected from device MeasurementService", componentName);
            q7Var.b();
            q7Var.C();
        }
    }

    @WorkerThread
    public final void A(AtomicReference<String> atomicReference) {
        b();
        t();
        z(new v7(this, atomicReference, I(false)));
    }

    @WorkerThread
    public final boolean B() {
        b();
        t();
        return this.d != null;
    }

    @WorkerThread
    public final void C() {
        b();
        t();
        if (!B()) {
            if (G()) {
                k8 k8Var = this.c;
                k8Var.l.b();
                Context context = k8Var.l.a.f1566b;
                synchronized (k8Var) {
                    if (k8Var.j) {
                        k8Var.l.g().n.a("Connection attempt already in progress");
                    } else if (k8Var.k == null || (!k8Var.k.e() && !k8Var.k.j())) {
                        k8Var.k = new r3(context, Looper.getMainLooper(), k8Var, k8Var);
                        k8Var.l.g().n.a("Connecting to remote service");
                        k8Var.j = true;
                        k8Var.k.q();
                    } else {
                        k8Var.l.g().n.a("Already awaiting connection attempt");
                    }
                }
            } else if (!this.a.h.C()) {
                List<ResolveInfo> queryIntentServices = this.a.f1566b.getPackageManager().queryIntentServices(new Intent().setClassName(this.a.f1566b, "com.google.android.gms.measurement.AppMeasurementService"), 65536);
                if (queryIntentServices != null && queryIntentServices.size() > 0) {
                    Intent intent = new Intent("com.google.android.gms.measurement.START");
                    intent.setComponent(new ComponentName(this.a.f1566b, "com.google.android.gms.measurement.AppMeasurementService"));
                    k8 k8Var2 = this.c;
                    k8Var2.l.b();
                    Context context2 = k8Var2.l.a.f1566b;
                    a b2 = a.b();
                    synchronized (k8Var2) {
                        if (k8Var2.j) {
                            k8Var2.l.g().n.a("Connection attempt already in progress");
                            return;
                        }
                        k8Var2.l.g().n.a("Using local app measurement service");
                        k8Var2.j = true;
                        b2.a(context2, intent, k8Var2.l.c, Opcodes.LOR);
                        return;
                    }
                }
                g().f.a("Unable to use remote or local measurement implementation. Please register the AppMeasurementService service in the app manifest");
            }
        }
    }

    @WorkerThread
    public final void D() {
        b();
        t();
        k8 k8Var = this.c;
        if (k8Var.k != null && (k8Var.k.j() || k8Var.k.e())) {
            k8Var.k.h();
        }
        k8Var.k = null;
        try {
            a.b().c(this.a.f1566b, this.c);
        } catch (IllegalArgumentException | IllegalStateException unused) {
        }
        this.d = null;
    }

    @WorkerThread
    public final boolean E() {
        b();
        t();
        if (!this.a.h.o(p.J0)) {
            return false;
        }
        return !G() || e().y0() >= p.K0.a(null).intValue();
    }

    @WorkerThread
    public final void F() {
        b();
        e9 e9Var = this.g;
        Objects.requireNonNull((c) e9Var.a);
        e9Var.f1526b = SystemClock.elapsedRealtime();
        this.f.b(p.J.a(null).longValue());
    }

    /* JADX WARN: Removed duplicated region for block: B:40:0x00ea  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x0103  */
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final boolean G() {
        /*
            Method dump skipped, instructions count: 293
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.q7.G():boolean");
    }

    @WorkerThread
    public final void H() {
        b();
        g().n.b("Processing queued up service tasks", Integer.valueOf(this.h.size()));
        for (Runnable runnable : this.h) {
            try {
                runnable.run();
            } catch (Exception e) {
                g().f.b("Task exception while flushing queue", e);
            }
        }
        this.h.clear();
        this.i.c();
    }

    /* JADX WARN: Removed duplicated region for block: B:47:0x017a  */
    /* JADX WARN: Removed duplicated region for block: B:48:0x0181  */
    /* JADX WARN: Removed duplicated region for block: B:51:0x01ac  */
    /* JADX WARN: Removed duplicated region for block: B:57:0x01e1  */
    /* JADX WARN: Removed duplicated region for block: B:58:0x01e4  */
    /* JADX WARN: Removed duplicated region for block: B:61:0x01fa  */
    /* JADX WARN: Removed duplicated region for block: B:64:0x0212  */
    /* JADX WARN: Removed duplicated region for block: B:68:0x021e  */
    @androidx.annotation.Nullable
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final com.google.android.gms.measurement.internal.zzn I(boolean r36) {
        /*
            Method dump skipped, instructions count: 608
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.q7.I(boolean):com.google.android.gms.measurement.internal.zzn");
    }

    @Override // b.i.a.f.i.b.a5
    public final boolean v() {
        return false;
    }

    /* JADX WARN: Finally extract failed */
    /* JADX WARN: Removed duplicated region for block: B:104:0x01df  */
    /* JADX WARN: Removed duplicated region for block: B:106:0x01e4  */
    /* JADX WARN: Removed duplicated region for block: B:113:0x01f4  */
    /* JADX WARN: Removed duplicated region for block: B:115:0x01f9  */
    /* JADX WARN: Removed duplicated region for block: B:121:0x0210  */
    /* JADX WARN: Removed duplicated region for block: B:123:0x0215  */
    /* JADX WARN: Removed duplicated region for block: B:126:0x0222  */
    /* JADX WARN: Removed duplicated region for block: B:128:0x0227  */
    /* JADX WARN: Removed duplicated region for block: B:132:0x023b  */
    /* JADX WARN: Removed duplicated region for block: B:133:0x0244  */
    /* JADX WARN: Removed duplicated region for block: B:136:0x0249 A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:140:0x0255  */
    /* JADX WARN: Removed duplicated region for block: B:185:0x0218 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:187:0x0218 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:189:0x0218 A[SYNTHETIC] */
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void w(b.i.a.f.i.b.i3 r28, com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable r29, com.google.android.gms.measurement.internal.zzn r30) {
        /*
            Method dump skipped, instructions count: 696
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.q7.w(b.i.a.f.i.b.i3, com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable, com.google.android.gms.measurement.internal.zzn):void");
    }

    @WorkerThread
    public final void y(zzz zzzVar) {
        boolean z2;
        b();
        t();
        m3 r = r();
        r.e();
        byte[] g02 = t9.g0(zzzVar);
        if (g02.length > 131072) {
            r.g().g.a("Conditional user property too long for local database. Sending directly to service");
            z2 = false;
        } else {
            z2 = r.x(2, g02);
        }
        z(new g8(this, z2, new zzz(zzzVar), I(true), zzzVar));
    }

    @WorkerThread
    public final void z(Runnable runnable) throws IllegalStateException {
        b();
        if (B()) {
            runnable.run();
        } else if (this.h.size() >= 1000) {
            g().f.a("Discarding data. Max runnable queue size reached");
        } else {
            this.h.add(runnable);
            this.i.b(60000L);
            C();
        }
    }
}
