package b.i.a.f.i.b;

import android.app.Activity;
import android.app.Application;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Pair;
import androidx.annotation.GuardedBy;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import b.c.a.a0.d;
import b.i.a.f.e.o.c;
import b.i.a.f.e.o.f;
import b.i.a.f.h.l.g9;
import b.i.a.f.h.l.j9;
import b.i.a.f.h.l.r9;
import b.i.a.f.h.l.t8;
import com.discord.models.domain.ModelAuditLogEntry;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.measurement.internal.zzn;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class c6 extends a5 {
    public y6 c;
    public w5 d;
    public boolean f;
    public final y9 n;
    public final Set<z5> e = new CopyOnWriteArraySet();
    public final Object h = new Object();
    public boolean o = true;
    public final v9 p = new q6(this);
    public final AtomicReference<String> g = new AtomicReference<>();
    @GuardedBy("consentLock")
    public d i = new d(null, null);
    @GuardedBy("consentLock")
    public int j = 100;
    public long l = -1;
    public int m = 100;
    public final AtomicLong k = new AtomicLong(0);

    public c6(u4 u4Var) {
        super(u4Var);
        this.n = new y9(u4Var);
    }

    /* JADX WARN: Removed duplicated region for block: B:16:0x0062  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x00af  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static void C(b.i.a.f.i.b.c6 r4, b.i.a.f.i.b.d r5, int r6, long r7, boolean r9, boolean r10) {
        /*
            r4.b()
            r4.t()
            long r0 = r4.l
            int r2 = (r7 > r0 ? 1 : (r7 == r0 ? 0 : -1))
            if (r2 > 0) goto L21
            int r0 = r4.m
            boolean r0 = b.i.a.f.i.b.d.e(r0, r6)
            if (r0 == 0) goto L21
            b.i.a.f.i.b.q3 r4 = r4.g()
            b.i.a.f.i.b.s3 r4 = r4.l
            java.lang.String r6 = "Dropped out-of-date consent setting, proposed settings"
            r4.b(r6, r5)
            goto Lbe
        L21:
            b.i.a.f.i.b.d4 r0 = r4.l()
            java.util.Objects.requireNonNull(r0)
            boolean r1 = b.i.a.f.h.l.t8.b()
            r2 = 0
            if (r1 == 0) goto L5f
            b.i.a.f.i.b.u4 r1 = r0.a
            b.i.a.f.i.b.c r1 = r1.h
            b.i.a.f.i.b.j3<java.lang.Boolean> r3 = b.i.a.f.i.b.p.H0
            boolean r1 = r1.o(r3)
            if (r1 == 0) goto L5f
            r0.b()
            boolean r1 = r0.t(r6)
            if (r1 == 0) goto L5f
            android.content.SharedPreferences r0 = r0.w()
            android.content.SharedPreferences$Editor r0 = r0.edit()
            java.lang.String r5 = r5.d()
            java.lang.String r1 = "consent_settings"
            r0.putString(r1, r5)
            java.lang.String r5 = "consent_source"
            r0.putInt(r5, r6)
            r0.apply()
            r5 = 1
            goto L60
        L5f:
            r5 = 0
        L60:
            if (r5 == 0) goto Laf
            r4.l = r7
            r4.m = r6
            b.i.a.f.i.b.q7 r5 = r4.p()
            java.util.Objects.requireNonNull(r5)
            boolean r6 = b.i.a.f.h.l.t8.b()
            if (r6 == 0) goto La0
            b.i.a.f.i.b.u4 r6 = r5.a
            b.i.a.f.i.b.c r6 = r6.h
            b.i.a.f.i.b.j3<java.lang.Boolean> r7 = b.i.a.f.i.b.p.H0
            boolean r6 = r6.o(r7)
            if (r6 == 0) goto La0
            r5.b()
            r5.t()
            if (r9 == 0) goto L8e
            b.i.a.f.i.b.m3 r6 = r5.r()
            r6.y()
        L8e:
            boolean r6 = r5.E()
            if (r6 == 0) goto La0
            com.google.android.gms.measurement.internal.zzn r6 = r5.I(r2)
            b.i.a.f.i.b.e8 r7 = new b.i.a.f.i.b.e8
            r7.<init>(r5, r6)
            r5.z(r7)
        La0:
            if (r10 == 0) goto Lbe
            b.i.a.f.i.b.q7 r4 = r4.p()
            java.util.concurrent.atomic.AtomicReference r5 = new java.util.concurrent.atomic.AtomicReference
            r5.<init>()
            r4.A(r5)
            goto Lbe
        Laf:
            b.i.a.f.i.b.q3 r4 = r4.g()
            b.i.a.f.i.b.s3 r4 = r4.l
            java.lang.Integer r5 = java.lang.Integer.valueOf(r6)
            java.lang.String r6 = "Lower precedence consent source ignored, proposed source"
            r4.b(r6, r5)
        Lbe:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.c6.C(b.i.a.f.i.b.c6, b.i.a.f.i.b.d, int, long, boolean, boolean):void");
    }

    public final void A(d dVar, int i, long j) {
        boolean z2;
        boolean z3;
        d dVar2;
        boolean z4;
        if (t8.b() && this.a.h.o(p.H0)) {
            t();
            if ((!this.a.h.o(p.I0) || i != 20) && dVar.f1519b == null && dVar.c == null) {
                g().k.a("Discarding empty consent settings");
                return;
            }
            synchronized (this.h) {
                z2 = false;
                if (d.e(i, this.j)) {
                    z4 = dVar.f(this.i);
                    if (dVar.k() && !this.i.k()) {
                        z2 = true;
                    }
                    d dVar3 = this.i;
                    Boolean bool = dVar.f1519b;
                    if (bool == null) {
                        bool = dVar3.f1519b;
                    }
                    Boolean bool2 = dVar.c;
                    if (bool2 == null) {
                        bool2 = dVar3.c;
                    }
                    d dVar4 = new d(bool, bool2);
                    this.i = dVar4;
                    this.j = i;
                    z3 = z2;
                    dVar2 = dVar4;
                    z2 = true;
                } else {
                    dVar2 = dVar;
                    z3 = false;
                    z4 = false;
                }
            }
            if (!z2) {
                g().l.b("Ignoring lower-priority consent settings, proposed settings", dVar2);
                return;
            }
            long andIncrement = this.k.getAndIncrement();
            if (z4) {
                this.g.set(null);
                f().w(new x6(this, dVar2, j, i, andIncrement, z3));
            } else if (!this.a.h.o(p.I0) || !(i == 40 || i == 20)) {
                f().v(new z6(this, dVar2, i, andIncrement, z3));
            } else {
                f().w(new w6(this, dVar2, i, andIncrement, z3));
            }
        }
    }

    @WorkerThread
    public final void B(w5 w5Var) {
        w5 w5Var2;
        b();
        t();
        if (!(w5Var == null || w5Var == (w5Var2 = this.d))) {
            d.G(w5Var2 == null, "EventInterceptor already set.");
        }
        this.d = w5Var;
    }

    @WorkerThread
    public final void D(@Nullable Boolean bool, boolean z2) {
        b();
        t();
        g().m.b("Setting app measurement enabled (FE)", bool);
        l().s(bool);
        if (t8.b() && this.a.h.o(p.H0) && z2) {
            d4 l = l();
            Objects.requireNonNull(l);
            if (t8.b() && l.a.h.o(p.H0)) {
                l.b();
                SharedPreferences.Editor edit = l.w().edit();
                if (bool != null) {
                    edit.putBoolean("measurement_enabled_from_api", bool.booleanValue());
                } else {
                    edit.remove("measurement_enabled_from_api");
                }
                edit.apply();
            }
        }
        if (!t8.b() || !this.a.h.o(p.H0) || this.a.h() || !bool.booleanValue()) {
            P();
        }
    }

    @WorkerThread
    public final void E(String str, String str2, long j, Bundle bundle) {
        b();
        F(str, str2, j, bundle, true, this.d == null || t9.r0(str2), false, null);
    }

    /* JADX WARN: Code restructure failed: missing block: B:61:0x014d, code lost:
        r5 = 13;
     */
    /* JADX WARN: Removed duplicated region for block: B:64:0x0157  */
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void F(java.lang.String r28, java.lang.String r29, long r30, android.os.Bundle r32, boolean r33, boolean r34, boolean r35, java.lang.String r36) {
        /*
            Method dump skipped, instructions count: 1365
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.c6.F(java.lang.String, java.lang.String, long, android.os.Bundle, boolean, boolean, boolean, java.lang.String):void");
    }

    public final void G(String str, String str2, long j, Object obj) {
        f().v(new j6(this, str, str2, obj, j));
    }

    public final void H(String str, String str2, Bundle bundle) {
        Objects.requireNonNull((c) this.a.o);
        I(str, str2, bundle, true, true, System.currentTimeMillis());
    }

    public final void I(String str, String str2, Bundle bundle, boolean z2, boolean z3, long j) {
        String str3;
        String str4 = str == null ? "app" : str;
        Bundle bundle2 = bundle == null ? new Bundle() : bundle;
        if (this.a.h.o(p.v0) && t9.q0(str2, "screen_view")) {
            h7 q = q();
            if (!q.a.h.o(p.v0)) {
                q.g().k.a("Manual screen reporting is disabled.");
                return;
            }
            synchronized (q.l) {
                if (!q.k) {
                    q.g().k.a("Cannot log screen view event when the app is in the background.");
                    return;
                }
                String string = bundle2.getString("screen_name");
                if (string == null || (string.length() > 0 && string.length() <= 100)) {
                    String string2 = bundle2.getString("screen_class");
                    if (string2 == null || (string2.length() > 0 && string2.length() <= 100)) {
                        if (string2 == null) {
                            Activity activity = q.g;
                            str3 = activity != null ? h7.x(activity.getClass().getCanonicalName()) : "Activity";
                        } else {
                            str3 = string2;
                        }
                        if (q.h && q.c != null) {
                            q.h = false;
                            boolean q0 = t9.q0(q.c.f1533b, str3);
                            boolean q02 = t9.q0(q.c.a, string);
                            if (q0 && q02) {
                                q.g().k.a("Ignoring call to log screen view event with duplicate parameters.");
                                return;
                            }
                        }
                        q.g().n.c("Logging screen view with name, class", string == null ? "null" : string, str3 == null ? "null" : str3);
                        i7 i7Var = q.c == null ? q.d : q.c;
                        i7 i7Var2 = new i7(string, str3, q.e().t0(), true, j);
                        q.c = i7Var2;
                        q.d = i7Var;
                        q.i = i7Var2;
                        Objects.requireNonNull((c) q.a.o);
                        q.f().v(new k7(q, bundle2, i7Var2, i7Var, SystemClock.elapsedRealtime()));
                        return;
                    }
                    q.g().k.b("Invalid screen class length for screen view. Length", Integer.valueOf(string2.length()));
                    return;
                }
                q.g().k.b("Invalid screen name length for screen view. Length", Integer.valueOf(string.length()));
                return;
            }
        }
        Q(str4, str2, j, bundle2, z3, !z3 || this.d == null || t9.r0(str2), !z2, null);
    }

    public final void J(String str, String str2, Object obj) {
        Objects.requireNonNull((c) this.a.o);
        L(str, str2, obj, true, System.currentTimeMillis());
    }

    /* JADX WARN: Removed duplicated region for block: B:22:0x006a  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x0076  */
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void K(java.lang.String r9, java.lang.String r10, java.lang.Object r11, long r12) {
        /*
            r8 = this;
            b.c.a.a0.d.w(r9)
            b.c.a.a0.d.w(r10)
            r8.b()
            r8.t()
            java.lang.String r0 = "allow_personalized_ads"
            boolean r0 = r0.equals(r10)
            java.lang.String r1 = "_npa"
            if (r0 == 0) goto L60
            boolean r0 = r11 instanceof java.lang.String
            if (r0 == 0) goto L50
            r0 = r11
            java.lang.String r0 = (java.lang.String) r0
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L50
            java.util.Locale r10 = java.util.Locale.ENGLISH
            java.lang.String r10 = r0.toLowerCase(r10)
            java.lang.String r11 = "false"
            boolean r10 = r11.equals(r10)
            r2 = 1
            if (r10 == 0) goto L35
            r4 = r2
            goto L37
        L35:
            r4 = 0
        L37:
            java.lang.Long r10 = java.lang.Long.valueOf(r4)
            b.i.a.f.i.b.d4 r0 = r8.l()
            b.i.a.f.i.b.j4 r0 = r0.t
            long r4 = r10.longValue()
            int r6 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r6 != 0) goto L4b
            java.lang.String r11 = "true"
        L4b:
            r0.b(r11)
            r6 = r10
            goto L5e
        L50:
            if (r11 != 0) goto L60
            b.i.a.f.i.b.d4 r10 = r8.l()
            b.i.a.f.i.b.j4 r10 = r10.t
            java.lang.String r0 = "unset"
            r10.b(r0)
            r6 = r11
        L5e:
            r3 = r1
            goto L62
        L60:
            r3 = r10
            r6 = r11
        L62:
            b.i.a.f.i.b.u4 r10 = r8.a
            boolean r10 = r10.d()
            if (r10 != 0) goto L76
            b.i.a.f.i.b.q3 r9 = r8.g()
            b.i.a.f.i.b.s3 r9 = r9.n
            java.lang.String r10 = "User property not set since app measurement is disabled"
            r9.a(r10)
            return
        L76:
            b.i.a.f.i.b.u4 r10 = r8.a
            boolean r10 = r10.m()
            if (r10 != 0) goto L7f
            return
        L7f:
            com.google.android.gms.measurement.internal.zzku r10 = new com.google.android.gms.measurement.internal.zzku
            r2 = r10
            r4 = r12
            r7 = r9
            r2.<init>(r3, r4, r6, r7)
            b.i.a.f.i.b.q7 r9 = r8.p()
            r9.b()
            r9.t()
            b.i.a.f.i.b.m3 r11 = r9.r()
            java.util.Objects.requireNonNull(r11)
            android.os.Parcel r12 = android.os.Parcel.obtain()
            r13 = 0
            r10.writeToParcel(r12, r13)
            byte[] r0 = r12.marshall()
            r12.recycle()
            int r12 = r0.length
            r1 = 131072(0x20000, float:1.83671E-40)
            r2 = 1
            if (r12 <= r1) goto Lb9
            b.i.a.f.i.b.q3 r11 = r11.g()
            b.i.a.f.i.b.s3 r11 = r11.g
            java.lang.String r12 = "User property too long for local database. Sending directly to service"
            r11.a(r12)
            goto Lbd
        Lb9:
            boolean r13 = r11.x(r2, r0)
        Lbd:
            com.google.android.gms.measurement.internal.zzn r11 = r9.I(r2)
            b.i.a.f.i.b.r7 r12 = new b.i.a.f.i.b.r7
            r12.<init>(r9, r13, r10, r11)
            r9.z(r12)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.c6.K(java.lang.String, java.lang.String, java.lang.Object, long):void");
    }

    public final void L(String str, String str2, Object obj, boolean z2, long j) {
        int i;
        if (str == null) {
            str = "app";
        }
        String str3 = str;
        if (z2) {
            i = e().h0(str2);
        } else {
            t9 e = e();
            if (e.a0("user property", str2)) {
                if (!e.f0("user property", x5.a, null, str2)) {
                    i = 15;
                } else if (e.Z("user property", 24, str2)) {
                    i = 0;
                }
            }
            i = 6;
        }
        if (i != 0) {
            e();
            this.a.t().R(this.p, i, "_ev", t9.E(str2, 24, true), str2 != null ? str2.length() : 0);
        } else if (obj != null) {
            int i02 = e().i0(str2, obj);
            if (i02 != 0) {
                e();
                this.a.t().R(this.p, i02, "_ev", t9.E(str2, 24, true), ((obj instanceof String) || (obj instanceof CharSequence)) ? String.valueOf(obj).length() : 0);
                return;
            }
            Object n0 = e().n0(str2, obj);
            if (n0 != null) {
                G(str3, str2, j, n0);
            }
        } else {
            G(str3, str2, j, null);
        }
    }

    public final void M() {
        if (this.a.f1566b.getApplicationContext() instanceof Application) {
            ((Application) this.a.f1566b.getApplicationContext()).unregisterActivityLifecycleCallbacks(this.c);
        }
    }

    @WorkerThread
    public final void N() {
        b();
        t();
        if (this.a.m()) {
            if (this.a.h.o(p.f1551d0)) {
                Boolean w = this.a.h.w("google_analytics_deferred_deep_link_enabled");
                if (w != null && w.booleanValue()) {
                    g().m.a("Deferred Deep Link feature enabled.");
                    f().v(new Runnable(this) { // from class: b.i.a.f.i.b.e6
                        public final c6 j;

                        {
                            this.j = this;
                        }

                        @Override // java.lang.Runnable
                        public final void run() {
                            Pair pair;
                            NetworkInfo networkInfo;
                            URL url;
                            c6 c6Var = this.j;
                            c6Var.b();
                            if (c6Var.l().f1522y.b()) {
                                c6Var.g().m.a("Deferred Deep Link already retrieved. Not fetching again.");
                                return;
                            }
                            long a = c6Var.l().f1523z.a();
                            c6Var.l().f1523z.b(a + 1);
                            if (a >= 5) {
                                c6Var.g().i.a("Permanently failed to retrieve Deferred Deep Link. Reached maximum retries.");
                                c6Var.l().f1522y.a(true);
                                return;
                            }
                            u4 u4Var = c6Var.a;
                            u4Var.f().b();
                            u4.q(u4Var.n());
                            n3 z2 = u4Var.z();
                            z2.t();
                            String str = z2.c;
                            d4 o = u4Var.o();
                            o.b();
                            Objects.requireNonNull((c) o.a.o);
                            long elapsedRealtime = SystemClock.elapsedRealtime();
                            if (o.n == null || elapsedRealtime >= o.p) {
                                c cVar = o.a.h;
                                Objects.requireNonNull(cVar);
                                o.p = cVar.n(str, p.f1548b) + elapsedRealtime;
                                try {
                                    AdvertisingIdClient.Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(o.a.f1566b);
                                    if (advertisingIdInfo != null) {
                                        o.n = advertisingIdInfo.getId();
                                        o.o = advertisingIdInfo.isLimitAdTrackingEnabled();
                                    }
                                    if (o.n == null) {
                                        o.n = "";
                                    }
                                } catch (Exception e) {
                                    o.g().m.b("Unable to get advertising id", e);
                                    o.n = "";
                                }
                                pair = new Pair(o.n, Boolean.valueOf(o.o));
                            } else {
                                pair = new Pair(o.n, Boolean.valueOf(o.o));
                            }
                            if (!u4Var.h.y().booleanValue() || ((Boolean) pair.second).booleanValue() || TextUtils.isEmpty((CharSequence) pair.first)) {
                                u4Var.g().m.a("ADID unavailable to retrieve Deferred Deep Link. Skipping");
                                return;
                            }
                            d7 n = u4Var.n();
                            n.o();
                            try {
                                networkInfo = ((ConnectivityManager) n.a.f1566b.getSystemService("connectivity")).getActiveNetworkInfo();
                            } catch (SecurityException unused) {
                                networkInfo = null;
                            }
                            if (!(networkInfo != null && networkInfo.isConnected())) {
                                u4Var.g().i.a("Network is not available for Deferred Deep Link request. Skipping");
                                return;
                            }
                            t9 t = u4Var.t();
                            u4Var.z();
                            String str2 = (String) pair.first;
                            long a2 = u4Var.o().f1523z.a() - 1;
                            Objects.requireNonNull(t);
                            try {
                                d.w(str2);
                                d.w(str);
                                String format = String.format("https://www.googleadservices.com/pagead/conversion/app/deeplink?id_type=adid&sdk_version=%s&rdid=%s&bundleid=%s&retry=%s", String.format("v%s.%s", 33025L, Integer.valueOf(t.y0())), str2, str, Long.valueOf(a2));
                                if (str.equals(t.a.h.h("debug.deferred.deeplink", ""))) {
                                    format = format.concat("&ddl_test=1");
                                }
                                url = new URL(format);
                            } catch (IllegalArgumentException | MalformedURLException e2) {
                                t.g().f.b("Failed to create BOW URL for Deferred Deep Link. exception", e2.getMessage());
                                url = null;
                            }
                            d7 n2 = u4Var.n();
                            x4 x4Var = new x4(u4Var);
                            n2.b();
                            n2.o();
                            Objects.requireNonNull(url, "null reference");
                            n2.f().x(new f7(n2, str, url, x4Var));
                        }
                    });
                }
            }
            q7 p = p();
            p.b();
            p.t();
            zzn I = p.I(true);
            p.r().x(3, new byte[0]);
            p.z(new x7(p, I));
            this.o = false;
            d4 l = l();
            l.b();
            String string = l.w().getString("previous_os_version", null);
            l.c().o();
            String str = Build.VERSION.RELEASE;
            if (!TextUtils.isEmpty(str) && !str.equals(string)) {
                SharedPreferences.Editor edit = l.w().edit();
                edit.putString("previous_os_version", str);
                edit.apply();
            }
            if (!TextUtils.isEmpty(string)) {
                c().o();
                if (!string.equals(str)) {
                    Bundle bundle = new Bundle();
                    bundle.putString("_po", string);
                    H("auto", "_ou", bundle);
                }
            }
        }
    }

    @Nullable
    public final String O() {
        u4 u4Var = this.a;
        String str = u4Var.c;
        if (str != null) {
            return str;
        }
        try {
            return f.V1(u4Var.f1566b, "google_app_id");
        } catch (IllegalStateException e) {
            this.a.g().f.b("getGoogleAppId failed with exception", e);
            return null;
        }
    }

    @WorkerThread
    public final void P() {
        b();
        String a = l().t.a();
        if (a != null) {
            if ("unset".equals(a)) {
                Objects.requireNonNull((c) this.a.o);
                K("app", "_npa", null, System.currentTimeMillis());
            } else {
                Long valueOf = Long.valueOf("true".equals(a) ? 1L : 0L);
                Objects.requireNonNull((c) this.a.o);
                K("app", "_npa", valueOf, System.currentTimeMillis());
            }
        }
        boolean z2 = true;
        if (!this.a.d() || !this.o) {
            g().m.a("Updating Scion state (FE)");
            q7 p = p();
            p.b();
            p.t();
            p.z(new b8(p, p.I(true)));
            return;
        }
        g().m.a("Recording app launch after enabling measurement for the first time (FE)");
        N();
        if (r9.b() && this.a.h.o(p.q0)) {
            s().d.a();
        }
        if (((j9) g9.j.a()).a() && this.a.h.o(p.t0)) {
            if (this.a.f1568x.a.o().l.a() <= 0) {
                z2 = false;
            }
            if (!z2) {
                m4 m4Var = this.a.f1568x;
                m4Var.a(m4Var.a.f1566b.getPackageName());
            }
        }
        if (this.a.h.o(p.D0)) {
            f().v(new f6(this));
        }
    }

    public final void Q(String str, String str2, long j, Bundle bundle, boolean z2, boolean z3, boolean z4, String str3) {
        Bundle bundle2 = new Bundle(bundle);
        for (String str4 : bundle2.keySet()) {
            Object obj = bundle2.get(str4);
            if (obj instanceof Bundle) {
                bundle2.putBundle(str4, new Bundle((Bundle) obj));
            } else {
                int i = 0;
                if (obj instanceof Parcelable[]) {
                    Parcelable[] parcelableArr = (Parcelable[]) obj;
                    while (i < parcelableArr.length) {
                        if (parcelableArr[i] instanceof Bundle) {
                            parcelableArr[i] = new Bundle((Bundle) parcelableArr[i]);
                        }
                        i++;
                    }
                } else if (obj instanceof List) {
                    List list = (List) obj;
                    while (i < list.size()) {
                        Object obj2 = list.get(i);
                        if (obj2 instanceof Bundle) {
                            list.set(i, new Bundle((Bundle) obj2));
                        }
                        i++;
                    }
                }
            }
        }
        f().v(new k6(this, str, str2, j, bundle2, z2, z3, z4, null));
    }

    public final void R(String str, String str2, Bundle bundle) {
        Objects.requireNonNull((c) this.a.o);
        long currentTimeMillis = System.currentTimeMillis();
        d.w(str);
        Bundle bundle2 = new Bundle();
        bundle2.putString(ModelAuditLogEntry.CHANGE_KEY_NAME, str);
        bundle2.putLong("creation_timestamp", currentTimeMillis);
        if (str2 != null) {
            bundle2.putString("expired_event_name", str2);
            bundle2.putBundle("expired_event_params", bundle);
        }
        f().v(new p6(this, bundle2));
    }

    @Override // b.i.a.f.i.b.a5
    public final boolean v() {
        return false;
    }

    public final void w(long j, boolean z2) {
        b();
        t();
        g().m.a("Resetting analytics data (FE)");
        w8 s2 = s();
        s2.b();
        d9 d9Var = s2.e;
        d9Var.c.c();
        d9Var.a = 0L;
        d9Var.f1524b = 0L;
        boolean d = this.a.d();
        d4 l = l();
        l.k.b(j);
        if (!TextUtils.isEmpty(l.l().A.a())) {
            l.A.b(null);
        }
        if (r9.b() && l.a.h.o(p.q0)) {
            l.v.b(0L);
        }
        if (!l.a.h.x()) {
            l.v(!d);
        }
        l.B.b(null);
        l.C.b(0L);
        l.D.b(null);
        if (z2) {
            q7 p = p();
            p.b();
            p.t();
            zzn I = p.I(false);
            p.r().y();
            p.z(new t7(p, I));
        }
        if (r9.b() && this.a.h.o(p.q0)) {
            s().d.a();
        }
        this.o = !d;
    }

    public final void x(Bundle bundle, int i, long j) {
        if (t8.b() && this.a.h.o(p.H0)) {
            t();
            String string = bundle.getString("ad_storage");
            if ((string == null || d.i(string) != null) && ((string = bundle.getString("analytics_storage")) == null || d.i(string) != null)) {
                string = null;
            }
            if (string != null) {
                g().k.b("Ignoring invalid consent setting", string);
                g().k.a("Valid consent values are 'granted', 'denied'");
            }
            A(d.g(bundle), i, j);
        }
    }

    public final void y(Bundle bundle, long j) {
        Objects.requireNonNull(bundle, "null reference");
        Bundle bundle2 = new Bundle(bundle);
        if (!TextUtils.isEmpty(bundle2.getString("app_id"))) {
            g().i.a("Package name should be null when calling setConditionalUserProperty");
        }
        bundle2.remove("app_id");
        f.S1(bundle2, "app_id", String.class, null);
        f.S1(bundle2, "origin", String.class, null);
        f.S1(bundle2, ModelAuditLogEntry.CHANGE_KEY_NAME, String.class, null);
        f.S1(bundle2, "value", Object.class, null);
        f.S1(bundle2, "trigger_event_name", String.class, null);
        f.S1(bundle2, "trigger_timeout", Long.class, 0L);
        f.S1(bundle2, "timed_out_event_name", String.class, null);
        f.S1(bundle2, "timed_out_event_params", Bundle.class, null);
        f.S1(bundle2, "triggered_event_name", String.class, null);
        f.S1(bundle2, "triggered_event_params", Bundle.class, null);
        f.S1(bundle2, "time_to_live", Long.class, 0L);
        f.S1(bundle2, "expired_event_name", String.class, null);
        f.S1(bundle2, "expired_event_params", Bundle.class, null);
        d.w(bundle2.getString(ModelAuditLogEntry.CHANGE_KEY_NAME));
        d.w(bundle2.getString("origin"));
        Objects.requireNonNull(bundle2.get("value"), "null reference");
        bundle2.putLong("creation_timestamp", j);
        String string = bundle2.getString(ModelAuditLogEntry.CHANGE_KEY_NAME);
        Object obj = bundle2.get("value");
        if (e().h0(string) != 0) {
            g().f.b("Invalid conditional user property name", d().y(string));
        } else if (e().i0(string, obj) != 0) {
            g().f.c("Invalid conditional user property value", d().y(string), obj);
        } else {
            Object n0 = e().n0(string, obj);
            if (n0 == null) {
                g().f.c("Unable to normalize conditional user property value", d().y(string), obj);
                return;
            }
            f.d2(bundle2, n0);
            long j2 = bundle2.getLong("trigger_timeout");
            if (TextUtils.isEmpty(bundle2.getString("trigger_event_name")) || (j2 <= 15552000000L && j2 >= 1)) {
                long j3 = bundle2.getLong("time_to_live");
                if (j3 > 15552000000L || j3 < 1) {
                    g().f.c("Invalid conditional user property time to live", d().y(string), Long.valueOf(j3));
                } else {
                    f().v(new n6(this, bundle2));
                }
            } else {
                g().f.c("Invalid conditional user property timeout", d().y(string), Long.valueOf(j2));
            }
        }
    }

    @WorkerThread
    public final void z(d dVar) {
        Boolean bool;
        b();
        boolean z2 = (dVar.k() && dVar.j()) || p().E();
        if (z2 != this.a.h()) {
            u4 u4Var = this.a;
            u4Var.f().b();
            u4Var.E = z2;
            d4 l = l();
            Objects.requireNonNull(l);
            if (t8.b() && l.a.h.o(p.H0)) {
                l.b();
                if (l.w().contains("measurement_enabled_from_api")) {
                    bool = Boolean.valueOf(l.w().getBoolean("measurement_enabled_from_api", true));
                    if (z2 || bool == null || bool.booleanValue()) {
                        D(Boolean.valueOf(z2), false);
                    }
                    return;
                }
            }
            bool = null;
            if (z2) {
            }
            D(Boolean.valueOf(z2), false);
        }
    }
}
