package b.i.a.f.i.b;
/* compiled from: com.google.android.gms:play-services-measurement@@18.0.0 */
/* loaded from: classes3.dex */
public final class m9 implements z3 {
    public final /* synthetic */ String a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ k9 f1546b;

    public m9(k9 k9Var, String str) {
        this.f1546b = k9Var;
        this.a = str;
    }

    /* JADX WARN: Code restructure failed: missing block: B:52:0x0157, code lost:
        r10 = r9.k.o().h;
        java.util.Objects.requireNonNull((b.i.a.f.e.o.c) r9.k.o);
        r10.b(java.lang.System.currentTimeMillis());
     */
    /* JADX WARN: Finally extract failed */
    @Override // b.i.a.f.i.b.z3
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void a(java.lang.String r9, int r10, java.lang.Throwable r11, byte[] r12, java.util.Map<java.lang.String, java.util.List<java.lang.String>> r13) {
        /*
            Method dump skipped, instructions count: 390
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.m9.a(java.lang.String, int, java.lang.Throwable, byte[], java.util.Map):void");
    }
}
