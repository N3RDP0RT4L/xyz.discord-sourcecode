package b.i.a.f.i.b;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class o7 implements Runnable {
    public final /* synthetic */ i7 j;
    public final /* synthetic */ long k;
    public final /* synthetic */ h7 l;

    public o7(h7 h7Var, i7 i7Var, long j) {
        this.l = h7Var;
        this.j = i7Var;
        this.k = j;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.l.C(this.j, false, this.k);
        h7 h7Var = this.l;
        h7Var.e = null;
        q7 p = h7Var.p();
        p.b();
        p.t();
        p.z(new w7(p, null));
    }
}
