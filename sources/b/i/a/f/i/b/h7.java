package b.i.a.f.i.b;

import android.app.Activity;
import android.os.Bundle;
import android.os.SystemClock;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;
import b.i.a.f.e.o.c;
import com.discord.models.domain.ModelAuditLogEntry;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class h7 extends a5 {
    public volatile i7 c;
    public i7 d;
    public i7 e;
    public Activity g;
    public volatile boolean h;
    public volatile i7 i;
    public i7 j;
    public boolean k;
    public String m;
    public final Object l = new Object();
    public final Map<Activity, i7> f = new ConcurrentHashMap();

    public h7(u4 u4Var) {
        super(u4Var);
    }

    public static void A(i7 i7Var, Bundle bundle, boolean z2) {
        if (i7Var != null && (!bundle.containsKey("_sc") || z2)) {
            String str = i7Var.a;
            if (str != null) {
                bundle.putString("_sn", str);
            } else {
                bundle.remove("_sn");
            }
            String str2 = i7Var.f1533b;
            if (str2 != null) {
                bundle.putString("_sc", str2);
            } else {
                bundle.remove("_sc");
            }
            bundle.putLong("_si", i7Var.c);
        } else if (i7Var == null && z2) {
            bundle.remove("_sn");
            bundle.remove("_sc");
            bundle.remove("_si");
        }
    }

    public static String x(String str) {
        String[] split = str.split("\\.");
        String str2 = split.length > 0 ? split[split.length - 1] : "";
        return str2.length() > 100 ? str2.substring(0, 100) : str2;
    }

    /* JADX WARN: Removed duplicated region for block: B:62:0x010d  */
    /* JADX WARN: Removed duplicated region for block: B:69:0x0131  */
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void B(b.i.a.f.i.b.i7 r9, b.i.a.f.i.b.i7 r10, long r11, boolean r13, android.os.Bundle r14) {
        /*
            Method dump skipped, instructions count: 369
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.h7.B(b.i.a.f.i.b.i7, b.i.a.f.i.b.i7, long, boolean, android.os.Bundle):void");
    }

    @WorkerThread
    public final void C(i7 i7Var, boolean z2, long j) {
        a m = m();
        Objects.requireNonNull((c) this.a.o);
        m.t(SystemClock.elapsedRealtime());
        if (s().w(i7Var != null && i7Var.d, z2, j) && i7Var != null) {
            i7Var.d = false;
        }
    }

    @WorkerThread
    public final void D(String str, i7 i7Var) {
        b();
        synchronized (this) {
            String str2 = this.m;
            if (str2 == null || str2.equals(str)) {
                this.m = str;
            }
        }
    }

    @MainThread
    public final i7 E(@NonNull Activity activity) {
        Objects.requireNonNull(activity, "null reference");
        i7 i7Var = this.f.get(activity);
        if (i7Var == null) {
            i7 i7Var2 = new i7(null, x(activity.getClass().getCanonicalName()), e().t0());
            this.f.put(activity, i7Var2);
            i7Var = i7Var2;
        }
        return (this.a.h.o(p.v0) && this.i != null) ? this.i : i7Var;
    }

    @Override // b.i.a.f.i.b.a5
    public final boolean v() {
        return false;
    }

    @WorkerThread
    public final i7 w(boolean z2) {
        t();
        b();
        if (!this.a.h.o(p.v0) || !z2) {
            return this.e;
        }
        i7 i7Var = this.e;
        return i7Var != null ? i7Var : this.j;
    }

    @MainThread
    public final void y(Activity activity, Bundle bundle) {
        Bundle bundle2;
        if (this.a.h.z().booleanValue() && bundle != null && (bundle2 = bundle.getBundle("com.google.app_measurement.screen_service")) != null) {
            this.f.put(activity, new i7(bundle2.getString(ModelAuditLogEntry.CHANGE_KEY_NAME), bundle2.getString("referrer_name"), bundle2.getLong(ModelAuditLogEntry.CHANGE_KEY_ID)));
        }
    }

    @MainThread
    public final void z(Activity activity, i7 i7Var, boolean z2) {
        i7 i7Var2;
        i7 i7Var3 = this.c == null ? this.d : this.c;
        if (i7Var.f1533b == null) {
            i7Var2 = new i7(i7Var.a, activity != null ? x(activity.getClass().getCanonicalName()) : null, i7Var.c, i7Var.e, i7Var.f);
        } else {
            i7Var2 = i7Var;
        }
        this.d = this.c;
        this.c = i7Var2;
        Objects.requireNonNull((c) this.a.o);
        f().v(new j7(this, i7Var2, i7Var3, SystemClock.elapsedRealtime(), z2));
    }
}
