package b.i.a.f.i.b;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.IInterface;
import androidx.annotation.MainThread;
import b.i.a.f.h.l.d3;
import b.i.a.f.h.l.e2;
import b.i.a.f.h.l.e3;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class l4 implements ServiceConnection {
    public final String j;
    public final /* synthetic */ m4 k;

    public l4(m4 m4Var, String str) {
        this.k = m4Var;
        this.j = str;
    }

    @Override // android.content.ServiceConnection
    @MainThread
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        e2 e2Var;
        if (iBinder == null) {
            this.k.a.g().i.a("Install Referrer connection returned with null binder");
            return;
        }
        try {
            int i = e3.a;
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
            if (queryLocalInterface instanceof e2) {
                e2Var = (e2) queryLocalInterface;
            } else {
                e2Var = new d3(iBinder);
            }
            if (e2Var == null) {
                this.k.a.g().i.a("Install Referrer Service implementation was not found");
                return;
            }
            this.k.a.g().n.a("Install Referrer Service connected");
            this.k.a.f().v(new o4(this, e2Var, this));
        } catch (Exception e) {
            this.k.a.g().i.b("Exception occurred while calling Install Referrer API", e);
        }
    }

    @Override // android.content.ServiceConnection
    @MainThread
    public final void onServiceDisconnected(ComponentName componentName) {
        this.k.a.g().n.a("Install Referrer Service disconnected");
    }
}
