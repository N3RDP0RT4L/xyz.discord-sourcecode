package b.i.a.f.i.b;

import b.i.a.f.h.l.f9;
import b.i.a.f.h.l.i1;
import b.i.a.f.h.l.m0;
import b.i.a.f.h.l.o0;
import java.math.BigDecimal;
/* compiled from: com.google.android.gms:play-services-measurement@@18.0.0 */
/* loaded from: classes3.dex */
public final class ha extends ea {
    public o0 g;
    public final /* synthetic */ ba h;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ha(ba baVar, String str, int i, o0 o0Var) {
        super(str, i);
        this.h = baVar;
        this.g = o0Var;
    }

    @Override // b.i.a.f.i.b.ea
    public final int a() {
        return this.g.w();
    }

    @Override // b.i.a.f.i.b.ea
    public final boolean g() {
        return true;
    }

    @Override // b.i.a.f.i.b.ea
    public final boolean h() {
        return false;
    }

    public final boolean i(Long l, Long l2, i1 i1Var, boolean z2) {
        boolean z3 = f9.b() && this.h.a.h.u(this.a, p.f1547a0);
        boolean z4 = this.g.z();
        boolean A = this.g.A();
        boolean C = this.g.C();
        boolean z5 = z4 || A || C;
        Boolean bool = null;
        Boolean bool2 = null;
        Integer num = null;
        bool = null;
        bool = null;
        bool = null;
        bool = null;
        if (!z2 || z5) {
            m0 y2 = this.g.y();
            boolean A2 = y2.A();
            if (i1Var.H()) {
                if (!y2.x()) {
                    this.h.g().i.b("No number filter for long property. property", this.h.d().y(i1Var.D()));
                } else {
                    bool = ea.c(ea.b(i1Var.I(), y2.y()), A2);
                }
            } else if (i1Var.J()) {
                if (!y2.x()) {
                    this.h.g().i.b("No number filter for double property. property", this.h.d().y(i1Var.D()));
                } else {
                    double K = i1Var.K();
                    try {
                        bool2 = ea.f(new BigDecimal(K), y2.y(), Math.ulp(K));
                    } catch (NumberFormatException unused) {
                    }
                    bool = ea.c(bool2, A2);
                }
            } else if (!i1Var.F()) {
                this.h.g().i.b("User property has no value, property", this.h.d().y(i1Var.D()));
            } else if (y2.v()) {
                bool = ea.c(ea.e(i1Var.G(), y2.w(), this.h.g()), A2);
            } else if (!y2.x()) {
                this.h.g().i.b("No string or number filter defined. property", this.h.d().y(i1Var.D()));
            } else if (q9.P(i1Var.G())) {
                bool = ea.c(ea.d(i1Var.G(), y2.y()), A2);
            } else {
                this.h.g().i.c("Invalid user property value for Numeric number filter. property, value", this.h.d().y(i1Var.D()), i1Var.G());
            }
            this.h.g().n.b("Property filter result", bool == null ? "null" : bool);
            if (bool == null) {
                return false;
            }
            this.c = Boolean.TRUE;
            if (C && !bool.booleanValue()) {
                return true;
            }
            if (!z2 || this.g.z()) {
                this.d = bool;
            }
            if (bool.booleanValue() && z5 && i1Var.y()) {
                long z6 = i1Var.z();
                if (l != null) {
                    z6 = l.longValue();
                }
                if (z3 && this.g.z() && !this.g.A() && l2 != null) {
                    z6 = l2.longValue();
                }
                if (this.g.A()) {
                    this.f = Long.valueOf(z6);
                } else {
                    this.e = Long.valueOf(z6);
                }
            }
            return true;
        }
        s3 s3Var = this.h.g().n;
        Integer valueOf = Integer.valueOf(this.f1527b);
        if (this.g.v()) {
            num = Integer.valueOf(this.g.w());
        }
        s3Var.c("Property filter already evaluated true and it is not associated with an enhanced audience. audience ID, filter ID", valueOf, num);
        return true;
    }
}
