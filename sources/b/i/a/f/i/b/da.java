package b.i.a.f.i.b;

import androidx.annotation.NonNull;
import androidx.collection.ArrayMap;
import b.i.a.f.h.l.f9;
import b.i.a.f.h.l.g1;
import b.i.a.f.h.l.h1;
import b.i.a.f.h.l.u4;
import b.i.a.f.h.l.y0;
import b.i.a.f.h.l.z0;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;
import java.util.Map;
/* compiled from: com.google.android.gms:play-services-measurement@@18.0.0 */
/* loaded from: classes3.dex */
public final class da {
    public String a;

    /* renamed from: b  reason: collision with root package name */
    public boolean f1525b;
    public g1 c;
    public BitSet d;
    public BitSet e;
    public Map<Integer, Long> f;
    public Map<Integer, List<Long>> g;
    public final /* synthetic */ ba h;

    public da(ba baVar, String str, aa aaVar) {
        this.h = baVar;
        this.a = str;
        this.f1525b = true;
        this.d = new BitSet();
        this.e = new BitSet();
        this.f = new ArrayMap();
        this.g = new ArrayMap();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v13, types: [java.lang.Iterable] */
    /* JADX WARN: Type inference failed for: r1v17, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r1v18, types: [java.util.List] */
    @NonNull
    public final y0 a(int i) {
        ArrayList arrayList;
        ?? r1;
        y0.a F = y0.F();
        if (F.l) {
            F.n();
            F.l = false;
        }
        y0.u((y0) F.k, i);
        boolean z2 = this.f1525b;
        if (F.l) {
            F.n();
            F.l = false;
        }
        y0.w((y0) F.k, z2);
        g1 g1Var = this.c;
        if (g1Var != null) {
            if (F.l) {
                F.n();
                F.l = false;
            }
            y0.z((y0) F.k, g1Var);
        }
        g1.a M = g1.M();
        List<Long> B = q9.B(this.d);
        if (M.l) {
            M.n();
            M.l = false;
        }
        g1.D((g1) M.k, B);
        List<Long> B2 = q9.B(this.e);
        if (M.l) {
            M.n();
            M.l = false;
        }
        g1.y((g1) M.k, B2);
        if (this.f == null) {
            arrayList = null;
        } else {
            arrayList = new ArrayList(this.f.size());
            for (Integer num : this.f.keySet()) {
                int intValue = num.intValue();
                z0.a A = z0.A();
                if (A.l) {
                    A.n();
                    A.l = false;
                }
                z0.u((z0) A.k, intValue);
                long longValue = this.f.get(Integer.valueOf(intValue)).longValue();
                if (A.l) {
                    A.n();
                    A.l = false;
                }
                z0.v((z0) A.k, longValue);
                arrayList.add((z0) ((u4) A.p()));
            }
        }
        if (M.l) {
            M.n();
            M.l = false;
        }
        g1.F((g1) M.k, arrayList);
        if (this.g == null) {
            r1 = Collections.emptyList();
        } else {
            r1 = new ArrayList(this.g.size());
            for (Integer num2 : this.g.keySet()) {
                h1.a B3 = h1.B();
                int intValue2 = num2.intValue();
                if (B3.l) {
                    B3.n();
                    B3.l = false;
                }
                h1.v((h1) B3.k, intValue2);
                List<Long> list = this.g.get(num2);
                if (list != null) {
                    Collections.sort(list);
                    if (B3.l) {
                        B3.n();
                        B3.l = false;
                    }
                    h1.w((h1) B3.k, list);
                }
                r1.add((h1) ((u4) B3.p()));
            }
        }
        if (M.l) {
            M.n();
            M.l = false;
        }
        g1.H((g1) M.k, r1);
        if (F.l) {
            F.n();
            F.l = false;
        }
        y0.v((y0) F.k, (g1) ((u4) M.p()));
        return (y0) ((u4) F.p());
    }

    public final void b(@NonNull ea eaVar) {
        int a = eaVar.a();
        Boolean bool = eaVar.c;
        if (bool != null) {
            this.e.set(a, bool.booleanValue());
        }
        Boolean bool2 = eaVar.d;
        if (bool2 != null) {
            this.d.set(a, bool2.booleanValue());
        }
        if (eaVar.e != null) {
            Long l = this.f.get(Integer.valueOf(a));
            long longValue = eaVar.e.longValue() / 1000;
            if (l == null || longValue > l.longValue()) {
                this.f.put(Integer.valueOf(a), Long.valueOf(longValue));
            }
        }
        if (eaVar.f != null) {
            List<Long> list = this.g.get(Integer.valueOf(a));
            if (list == null) {
                list = new ArrayList<>();
                this.g.put(Integer.valueOf(a), list);
            }
            if (eaVar.g()) {
                list.clear();
            }
            if (f9.b() && this.h.a.h.u(this.a, p.f1550c0) && eaVar.h()) {
                list.clear();
            }
            if (!f9.b() || !this.h.a.h.u(this.a, p.f1550c0)) {
                list.add(Long.valueOf(eaVar.f.longValue() / 1000));
                return;
            }
            long longValue2 = eaVar.f.longValue() / 1000;
            if (!list.contains(Long.valueOf(longValue2))) {
                list.add(Long.valueOf(longValue2));
            }
        }
    }

    public da(ba baVar, String str, g1 g1Var, BitSet bitSet, BitSet bitSet2, Map map, Map map2, aa aaVar) {
        this.h = baVar;
        this.a = str;
        this.d = bitSet;
        this.e = bitSet2;
        this.f = map;
        this.g = new ArrayMap();
        for (Integer num : map2.keySet()) {
            ArrayList arrayList = new ArrayList();
            arrayList.add((Long) map2.get(num));
            this.g.put(num, arrayList);
        }
        this.f1525b = false;
        this.c = g1Var;
    }
}
