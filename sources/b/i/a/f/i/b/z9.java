package b.i.a.f.i.b;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.measurement.internal.zzn;
import java.util.ArrayList;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class z9 implements Parcelable.Creator<zzn> {
    @Override // android.os.Parcelable.Creator
    public final zzn createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        long j = 0;
        long j2 = -2147483648L;
        String str = "";
        long j3 = 0;
        long j4 = 0;
        long j5 = 0;
        long j6 = 0;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        String str8 = null;
        Boolean bool = null;
        ArrayList<String> arrayList = null;
        String str9 = null;
        boolean z2 = true;
        boolean z3 = false;
        int i = 0;
        boolean z4 = true;
        boolean z5 = true;
        boolean z6 = false;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 2:
                    str2 = d.R(parcel, readInt);
                    break;
                case 3:
                    str3 = d.R(parcel, readInt);
                    break;
                case 4:
                    str4 = d.R(parcel, readInt);
                    break;
                case 5:
                    str5 = d.R(parcel, readInt);
                    break;
                case 6:
                    j = d.H1(parcel, readInt);
                    break;
                case 7:
                    j3 = d.H1(parcel, readInt);
                    break;
                case '\b':
                    str6 = d.R(parcel, readInt);
                    break;
                case '\t':
                    z2 = d.E1(parcel, readInt);
                    break;
                case '\n':
                    z3 = d.E1(parcel, readInt);
                    break;
                case 11:
                    j2 = d.H1(parcel, readInt);
                    break;
                case '\f':
                    str7 = d.R(parcel, readInt);
                    break;
                case '\r':
                    j4 = d.H1(parcel, readInt);
                    break;
                case 14:
                    j5 = d.H1(parcel, readInt);
                    break;
                case 15:
                    i = d.G1(parcel, readInt);
                    break;
                case 16:
                    z4 = d.E1(parcel, readInt);
                    break;
                case 17:
                    z5 = d.E1(parcel, readInt);
                    break;
                case 18:
                    z6 = d.E1(parcel, readInt);
                    break;
                case 19:
                    str8 = d.R(parcel, readInt);
                    break;
                case 20:
                default:
                    d.d2(parcel, readInt);
                    break;
                case 21:
                    int M1 = d.M1(parcel, readInt);
                    if (M1 != 0) {
                        d.z2(parcel, readInt, M1, 4);
                        bool = Boolean.valueOf(parcel.readInt() != 0);
                        break;
                    } else {
                        bool = null;
                        break;
                    }
                case 22:
                    j6 = d.H1(parcel, readInt);
                    break;
                case 23:
                    arrayList = d.T(parcel, readInt);
                    break;
                case 24:
                    str9 = d.R(parcel, readInt);
                    break;
                case 25:
                    str = d.R(parcel, readInt);
                    break;
            }
        }
        d.f0(parcel, m2);
        return new zzn(str2, str3, str4, str5, j, j3, str6, z2, z3, j2, str7, j4, j5, i, z4, z5, z6, str8, bool, j6, arrayList, str9, str);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzn[] newArray(int i) {
        return new zzn[i];
    }
}
