package b.i.a.f.i.b;

import android.os.Bundle;
import androidx.annotation.WorkerThread;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public interface z5 {
    @WorkerThread
    void a(String str, String str2, Bundle bundle, long j);
}
