package b.i.a.f.i.b;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.Size;
import androidx.annotation.WorkerThread;
import b.c.a.a0.d;
import b.i.a.f.e.p.b;
import b.i.a.f.h.l.o8;
import b.i.a.f.h.l.oa;
import b.i.a.f.h.l.pa;
import java.lang.reflect.InvocationTargetException;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class c extends s5 {

    /* renamed from: b  reason: collision with root package name */
    public Boolean f1517b;
    @NonNull
    public e c = b.a;
    public Boolean d;

    public c(u4 u4Var) {
        super(u4Var);
    }

    public static long B() {
        return p.D.a(null).longValue();
    }

    public final boolean A(String str) {
        return "1".equals(this.c.h(str, "measurement.event_sampling_enabled"));
    }

    @WorkerThread
    public final boolean C() {
        if (this.f1517b == null) {
            Boolean w = w("app_measurement_lite");
            this.f1517b = w;
            if (w == null) {
                this.f1517b = Boolean.FALSE;
            }
        }
        return this.f1517b.booleanValue() || !this.a.f;
    }

    @Nullable
    public final Bundle D() {
        try {
            if (this.a.f1566b.getPackageManager() == null) {
                g().f.a("Failed to load metadata: PackageManager is null");
                return null;
            }
            ApplicationInfo a = b.a(this.a.f1566b).a(this.a.f1566b.getPackageName(), 128);
            if (a != null) {
                return a.metaData;
            }
            g().f.a("Failed to load metadata: ApplicationInfo is null");
            return null;
        } catch (PackageManager.NameNotFoundException e) {
            g().f.b("Failed to load metadata: Package name not found", e);
            return null;
        }
    }

    public final String h(String str, String str2) {
        try {
            return (String) Class.forName("android.os.SystemProperties").getMethod("get", String.class, String.class).invoke(null, str, str2);
        } catch (ClassNotFoundException e) {
            g().f.b("Could not find SystemProperties class", e);
            return str2;
        } catch (IllegalAccessException e2) {
            g().f.b("Could not access SystemProperties.get()", e2);
            return str2;
        } catch (NoSuchMethodException e3) {
            g().f.b("Could not find SystemProperties.get() method", e3);
            return str2;
        } catch (InvocationTargetException e4) {
            g().f.b("SystemProperties.get() threw an exception", e4);
            return str2;
        }
    }

    public final int m(@Size(min = 1) String str) {
        return Math.max(Math.min(q(str, p.I), 100), 25);
    }

    @WorkerThread
    public final long n(String str, @NonNull j3<Long> j3Var) {
        if (str == null) {
            return j3Var.a(null).longValue();
        }
        String h = this.c.h(str, j3Var.f1535b);
        if (TextUtils.isEmpty(h)) {
            return j3Var.a(null).longValue();
        }
        try {
            return j3Var.a(Long.valueOf(Long.parseLong(h))).longValue();
        } catch (NumberFormatException unused) {
            return j3Var.a(null).longValue();
        }
    }

    public final boolean o(j3<Boolean> j3Var) {
        return u(null, j3Var);
    }

    public final int p(@Size(min = 1) String str) {
        if (!o8.b() || !u(null, p.w0)) {
            return 500;
        }
        return Math.max(Math.min(q(str, p.H), 2000), 500);
    }

    @WorkerThread
    public final int q(String str, @NonNull j3<Integer> j3Var) {
        if (str == null) {
            return j3Var.a(null).intValue();
        }
        String h = this.c.h(str, j3Var.f1535b);
        if (TextUtils.isEmpty(h)) {
            return j3Var.a(null).intValue();
        }
        try {
            return j3Var.a(Integer.valueOf(Integer.parseInt(h))).intValue();
        } catch (NumberFormatException unused) {
            return j3Var.a(null).intValue();
        }
    }

    @WorkerThread
    public final double r(String str, @NonNull j3<Double> j3Var) {
        if (str == null) {
            return j3Var.a(null).doubleValue();
        }
        String h = this.c.h(str, j3Var.f1535b);
        if (TextUtils.isEmpty(h)) {
            return j3Var.a(null).doubleValue();
        }
        try {
            return j3Var.a(Double.valueOf(Double.parseDouble(h))).doubleValue();
        } catch (NumberFormatException unused) {
            return j3Var.a(null).doubleValue();
        }
    }

    @WorkerThread
    public final int s(@Size(min = 1) String str) {
        return q(str, p.o);
    }

    public final int t() {
        if (!o8.b() || !this.a.h.u(null, p.x0)) {
            return 25;
        }
        t9 e = e();
        Boolean bool = e.a.x().e;
        return e.y0() >= 201500 || (bool != null && !bool.booleanValue()) ? 100 : 25;
    }

    @WorkerThread
    public final boolean u(String str, @NonNull j3<Boolean> j3Var) {
        if (str == null) {
            return j3Var.a(null).booleanValue();
        }
        String h = this.c.h(str, j3Var.f1535b);
        if (TextUtils.isEmpty(h)) {
            return j3Var.a(null).booleanValue();
        }
        return j3Var.a(Boolean.valueOf(Boolean.parseBoolean(h))).booleanValue();
    }

    public final boolean v(String str, j3<Boolean> j3Var) {
        return u(str, j3Var);
    }

    @Nullable
    public final Boolean w(@Size(min = 1) String str) {
        d.w(str);
        Bundle D = D();
        if (D == null) {
            g().f.a("Failed to load metadata: Metadata bundle is null");
            return null;
        } else if (!D.containsKey(str)) {
            return null;
        } else {
            return Boolean.valueOf(D.getBoolean(str));
        }
    }

    public final boolean x() {
        Boolean w = w("firebase_analytics_collection_deactivated");
        return w != null && w.booleanValue();
    }

    public final Boolean y() {
        Boolean w = w("google_analytics_adid_collection_enabled");
        return Boolean.valueOf(w == null || w.booleanValue());
    }

    public final Boolean z() {
        if (!((oa) pa.j.a()).a() || !o(p.u0)) {
            return Boolean.TRUE;
        }
        Boolean w = w("google_analytics_automatic_screen_reporting_enabled");
        return Boolean.valueOf(w == null || w.booleanValue());
    }
}
