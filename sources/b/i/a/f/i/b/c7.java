package b.i.a.f.i.b;

import android.net.Uri;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class c7 implements Runnable {
    public final /* synthetic */ boolean j;
    public final /* synthetic */ Uri k;
    public final /* synthetic */ String l;
    public final /* synthetic */ String m;
    public final /* synthetic */ y6 n;

    public c7(y6 y6Var, boolean z2, Uri uri, String str, String str2) {
        this.n = y6Var;
        this.j = z2;
        this.k = uri;
        this.l = str;
        this.m = str2;
    }

    /* JADX WARN: Code restructure failed: missing block: B:85:0x01b7, code lost:
        r2.j.g().m.a("Activity created with data 'referrer' without required params");
     */
    /* JADX WARN: Code restructure failed: missing block: B:95:?, code lost:
        return;
     */
    /* JADX WARN: Removed duplicated region for block: B:32:0x00a0 A[Catch: Exception -> 0x01cf, TRY_ENTER, TryCatch #0 {Exception -> 0x01cf, blocks: (B:3:0x0011, B:6:0x002b, B:8:0x0039, B:10:0x0047, B:13:0x0054, B:15:0x005a, B:17:0x0060, B:19:0x0066, B:21:0x006c, B:23:0x0077, B:25:0x007f, B:26:0x0084, B:27:0x008a, B:29:0x0094, B:32:0x00a0, B:34:0x00ac, B:36:0x00bf, B:39:0x00c7, B:41:0x00cd, B:42:0x00e0, B:44:0x00f3, B:46:0x00fc, B:49:0x010c, B:52:0x011c, B:55:0x0124, B:57:0x012a, B:58:0x0135, B:61:0x013d, B:65:0x015c, B:67:0x016f, B:68:0x0177, B:69:0x0184, B:70:0x018b, B:72:0x0191, B:74:0x0197, B:76:0x019d, B:78:0x01a3, B:80:0x01ab, B:85:0x01b7, B:86:0x01c3, B:88:0x01c9), top: B:92:0x0011 }] */
    /* JADX WARN: Removed duplicated region for block: B:45:0x00fb  */
    /* JADX WARN: Removed duplicated region for block: B:60:0x013b  */
    /* JADX WARN: Removed duplicated region for block: B:61:0x013d A[Catch: Exception -> 0x01cf, TRY_LEAVE, TryCatch #0 {Exception -> 0x01cf, blocks: (B:3:0x0011, B:6:0x002b, B:8:0x0039, B:10:0x0047, B:13:0x0054, B:15:0x005a, B:17:0x0060, B:19:0x0066, B:21:0x006c, B:23:0x0077, B:25:0x007f, B:26:0x0084, B:27:0x008a, B:29:0x0094, B:32:0x00a0, B:34:0x00ac, B:36:0x00bf, B:39:0x00c7, B:41:0x00cd, B:42:0x00e0, B:44:0x00f3, B:46:0x00fc, B:49:0x010c, B:52:0x011c, B:55:0x0124, B:57:0x012a, B:58:0x0135, B:61:0x013d, B:65:0x015c, B:67:0x016f, B:68:0x0177, B:69:0x0184, B:70:0x018b, B:72:0x0191, B:74:0x0197, B:76:0x019d, B:78:0x01a3, B:80:0x01ab, B:85:0x01b7, B:86:0x01c3, B:88:0x01c9), top: B:92:0x0011 }] */
    @Override // java.lang.Runnable
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void run() {
        /*
            Method dump skipped, instructions count: 478
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.c7.run():void");
    }
}
