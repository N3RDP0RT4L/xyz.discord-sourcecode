package b.i.a.f.i.b;

import android.os.Looper;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class ga {
    public static boolean a() {
        return Looper.myLooper() == Looper.getMainLooper();
    }
}
