package b.i.a.f.i.b;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class s3 {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f1563b;
    public final boolean c;
    public final /* synthetic */ q3 d;

    public s3(q3 q3Var, int i, boolean z2, boolean z3) {
        this.d = q3Var;
        this.a = i;
        this.f1563b = z2;
        this.c = z3;
    }

    public final void a(String str) {
        this.d.w(this.a, this.f1563b, this.c, str, null, null, null);
    }

    public final void b(String str, Object obj) {
        this.d.w(this.a, this.f1563b, this.c, str, obj, null, null);
    }

    public final void c(String str, Object obj, Object obj2) {
        this.d.w(this.a, this.f1563b, this.c, str, obj, obj2, null);
    }

    public final void d(String str, Object obj, Object obj2, Object obj3) {
        this.d.w(this.a, this.f1563b, this.c, str, obj, obj2, obj3);
    }
}
