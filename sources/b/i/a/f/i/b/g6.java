package b.i.a.f.i.b;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class g6 implements Runnable {
    public final /* synthetic */ boolean j;
    public final /* synthetic */ c6 k;

    public g6(c6 c6Var, boolean z2) {
        this.k = c6Var;
        this.j = z2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        boolean d = this.k.a.d();
        boolean B = this.k.a.B();
        this.k.a.B = Boolean.valueOf(this.j);
        if (B == this.j) {
            this.k.a.g().n.b("Default data collection state already set to", Boolean.valueOf(this.j));
        }
        if (this.k.a.d() == d || this.k.a.d() != this.k.a.B()) {
            this.k.a.g().k.c("Default data collection is different than actual status", Boolean.valueOf(this.j), Boolean.valueOf(d));
        }
        this.k.P();
    }
}
