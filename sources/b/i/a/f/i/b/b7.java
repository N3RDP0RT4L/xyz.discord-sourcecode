package b.i.a.f.i.b;

import android.os.Bundle;
import java.util.List;
import java.util.Map;
/* compiled from: com.google.android.gms:play-services-measurement-base@@18.0.0 */
/* loaded from: classes3.dex */
public interface b7 {
    String a();

    String b();

    String c();

    String d();

    long e();

    void f(Bundle bundle);

    void g(String str);

    List<Bundle> h(String str, String str2);

    int i(String str);

    void j(String str);

    Map<String, Object> k(String str, String str2, boolean z2);

    void l(String str, String str2, Bundle bundle);

    void m(String str, String str2, Bundle bundle);
}
