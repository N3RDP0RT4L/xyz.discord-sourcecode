package b.i.a.f.i.b;

import android.os.Process;
import androidx.annotation.GuardedBy;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class v4 extends Thread {
    public final BlockingQueue<s4<?>> k;
    public final /* synthetic */ r4 m;
    @GuardedBy("threadLifeCycleLock")
    public boolean l = false;
    public final Object j = new Object();

    public v4(r4 r4Var, String str, BlockingQueue<s4<?>> blockingQueue) {
        this.m = r4Var;
        Objects.requireNonNull(blockingQueue, "null reference");
        this.k = blockingQueue;
        setName(str);
    }

    public final void a(InterruptedException interruptedException) {
        this.m.g().i.b(String.valueOf(getName()).concat(" was interrupted"), interruptedException);
    }

    public final void b() {
        synchronized (this.m.j) {
            if (!this.l) {
                this.m.k.release();
                this.m.j.notifyAll();
                r4 r4Var = this.m;
                if (this == r4Var.d) {
                    r4Var.d = null;
                } else if (this == r4Var.e) {
                    r4Var.e = null;
                } else {
                    r4Var.g().f.a("Current scheduler thread is neither worker nor network");
                }
                this.l = true;
            }
        }
    }

    @Override // java.lang.Thread, java.lang.Runnable
    public final void run() {
        boolean z2 = false;
        while (!z2) {
            try {
                this.m.k.acquire();
                z2 = true;
            } catch (InterruptedException e) {
                a(e);
            }
        }
        try {
            int i = Process.getThreadPriority(Process.myTid());
            while (true) {
                s4<?> poll = this.k.poll();
                if (poll == null) {
                    synchronized (this.j) {
                        if (this.k.peek() == null) {
                            Objects.requireNonNull(this.m);
                            try {
                                this.j.wait(30000L);
                            } catch (InterruptedException e2) {
                                a(e2);
                            }
                        }
                    }
                    synchronized (this.m.j) {
                        if (this.k.peek() == null) {
                            break;
                        }
                    }
                } else {
                    if (!poll.k) {
                        i = 10;
                    }
                    Process.setThreadPriority(i);
                    poll.run();
                }
            }
            if (this.m.a.h.o(p.r0)) {
                b();
            }
        } finally {
            b();
        }
    }
}
