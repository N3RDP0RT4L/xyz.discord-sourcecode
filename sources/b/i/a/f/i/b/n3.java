package b.i.a.f.i.b;

import java.util.List;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class n3 extends a5 {
    public String c;
    public String d;
    public int e;
    public String f;
    public long g;
    public long h;
    public List<String> i;
    public int j;
    public String k;
    public String l;
    public String m;

    public n3(u4 u4Var, long j) {
        super(u4Var);
        this.h = j;
    }

    @Override // b.i.a.f.i.b.a5
    public final boolean v() {
        return true;
    }

    /* JADX WARN: Can't wrap try/catch for region: R(33:2|(1:4)(6:150|5|(1:8)(2:9|(1:11))|152|12|(4:14|(1:16)|17|18))|20|9a|27|(1:32)(1:31)|33|(1:38)(1:37)|39|(1:(1:42)(1:43))|(3:45|46|(19:59|61|(1:63)|64|146|65|(1:72)(1:69)|73|(1:75)(1:76)|77|(2:98|(3:100|(1:102)(1:103)|104))(6:81|(1:83)(1:84)|85|(1:87)(1:88)|89|(3:93|(1:95)(1:96)|97))|(3:106|(1:108)(1:109)|110)|112|(1:114)(2:115|(5:118|(3:148|120|(1:122)(1:123))|(3:127|(1:129)(3:131|(3:134|(1:157)|132)|156)|130)|(1:137)|(2:139|140)(2:141|142)))|117|(0)|(0)|(0)|(0)(0)))|60|61|(0)|64|146|65|(1:67)|72|73|(0)(0)|77|(1:79)|98|(0)|(0)|112|(0)(0)|117|(0)|(0)|(0)|(0)(0)) */
    /* JADX WARN: Code restructure failed: missing block: B:111:0x0282, code lost:
        g().f.c("Fetching Google App Id failed with exception. appId", b.i.a.f.i.b.q3.s(r3), r0);
     */
    /* JADX WARN: Code restructure failed: missing block: B:70:0x01c3, code lost:
        r0 = move-exception;
     */
    /* JADX WARN: Removed duplicated region for block: B:100:0x0240 A[Catch: IllegalStateException -> 0x01c3, TryCatch #0 {IllegalStateException -> 0x01c3, blocks: (B:65:0x019e, B:67:0x01ac, B:69:0x01b8, B:72:0x01c6, B:73:0x01ce, B:77:0x01d8, B:79:0x01e0, B:81:0x01ec, B:84:0x020b, B:85:0x020f, B:89:0x0219, B:91:0x0221, B:93:0x0227, B:96:0x0233, B:97:0x0237, B:98:0x023a, B:100:0x0240, B:103:0x025f, B:104:0x0263, B:106:0x0267, B:108:0x0279, B:109:0x027c, B:110:0x027e), top: B:146:0x019e }] */
    /* JADX WARN: Removed duplicated region for block: B:106:0x0267 A[Catch: IllegalStateException -> 0x01c3, TryCatch #0 {IllegalStateException -> 0x01c3, blocks: (B:65:0x019e, B:67:0x01ac, B:69:0x01b8, B:72:0x01c6, B:73:0x01ce, B:77:0x01d8, B:79:0x01e0, B:81:0x01ec, B:84:0x020b, B:85:0x020f, B:89:0x0219, B:91:0x0221, B:93:0x0227, B:96:0x0233, B:97:0x0237, B:98:0x023a, B:100:0x0240, B:103:0x025f, B:104:0x0263, B:106:0x0267, B:108:0x0279, B:109:0x027c, B:110:0x027e), top: B:146:0x019e }] */
    /* JADX WARN: Removed duplicated region for block: B:114:0x02a5  */
    /* JADX WARN: Removed duplicated region for block: B:115:0x02b1  */
    /* JADX WARN: Removed duplicated region for block: B:127:0x02ea  */
    /* JADX WARN: Removed duplicated region for block: B:137:0x031c  */
    /* JADX WARN: Removed duplicated region for block: B:139:0x0320  */
    /* JADX WARN: Removed duplicated region for block: B:141:0x032b  */
    /* JADX WARN: Removed duplicated region for block: B:148:0x02c4 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:63:0x0197  */
    /* JADX WARN: Removed duplicated region for block: B:75:0x01d4  */
    /* JADX WARN: Removed duplicated region for block: B:76:0x01d7  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void w() {
        /*
            Method dump skipped, instructions count: 840
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.n3.w():void");
    }
}
