package b.i.a.f.i.b;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.measurement.internal.zzku;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class s9 implements Parcelable.Creator<zzku> {
    @Override // android.os.Parcelable.Creator
    public final zzku createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        long j = 0;
        String str = null;
        Long l = null;
        Float f = null;
        String str2 = null;
        String str3 = null;
        Double d = null;
        int i = 0;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    i = d.G1(parcel, readInt);
                    break;
                case 2:
                    str = d.R(parcel, readInt);
                    break;
                case 3:
                    j = d.H1(parcel, readInt);
                    break;
                case 4:
                    int M1 = d.M1(parcel, readInt);
                    if (M1 != 0) {
                        d.z2(parcel, readInt, M1, 8);
                        l = Long.valueOf(parcel.readLong());
                        break;
                    } else {
                        l = null;
                        break;
                    }
                case 5:
                    int M12 = d.M1(parcel, readInt);
                    if (M12 != 0) {
                        d.z2(parcel, readInt, M12, 4);
                        f = Float.valueOf(parcel.readFloat());
                        break;
                    } else {
                        f = null;
                        break;
                    }
                case 6:
                    str2 = d.R(parcel, readInt);
                    break;
                case 7:
                    str3 = d.R(parcel, readInt);
                    break;
                case '\b':
                    int M13 = d.M1(parcel, readInt);
                    if (M13 != 0) {
                        d.z2(parcel, readInt, M13, 8);
                        d = Double.valueOf(parcel.readDouble());
                        break;
                    } else {
                        d = null;
                        break;
                    }
                default:
                    d.d2(parcel, readInt);
                    break;
            }
        }
        d.f0(parcel, m2);
        return new zzku(i, str, j, l, f, str2, str3, d);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzku[] newArray(int i) {
        return new zzku[i];
    }
}
