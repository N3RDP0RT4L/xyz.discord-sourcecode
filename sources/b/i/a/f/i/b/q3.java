package b.i.a.f.i.b;

import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import androidx.annotation.GuardedBy;
import b.d.b.a.a;
import b.i.a.f.e.o.c;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class q3 extends r5 {
    @GuardedBy("this")
    public String e;
    public char c = 0;
    public long d = -1;
    public final s3 f = new s3(this, 6, false, false);
    public final s3 g = new s3(this, 6, true, false);
    public final s3 h = new s3(this, 6, false, true);
    public final s3 i = new s3(this, 5, false, false);
    public final s3 j = new s3(this, 5, true, false);
    public final s3 k = new s3(this, 5, false, true);
    public final s3 l = new s3(this, 4, false, false);
    public final s3 m = new s3(this, 3, false, false);
    public final s3 n = new s3(this, 2, false, false);

    public q3(u4 u4Var) {
        super(u4Var);
    }

    public static Object s(String str) {
        if (str == null) {
            return null;
        }
        return new v3(str);
    }

    public static String t(boolean z2, Object obj) {
        String className;
        String str = "";
        if (obj == null) {
            return str;
        }
        if (obj instanceof Integer) {
            obj = Long.valueOf(((Integer) obj).intValue());
        }
        int i = 0;
        if (obj instanceof Long) {
            if (!z2) {
                return String.valueOf(obj);
            }
            Long l = (Long) obj;
            if (Math.abs(l.longValue()) < 100) {
                return String.valueOf(obj);
            }
            if (String.valueOf(obj).charAt(0) == '-') {
                str = "-";
            }
            String valueOf = String.valueOf(Math.abs(l.longValue()));
            long round = Math.round(Math.pow(10.0d, valueOf.length() - 1));
            long round2 = Math.round(Math.pow(10.0d, valueOf.length()) - 1.0d);
            StringBuilder P = a.P(str.length() + str.length() + 43, str, round, "...");
            P.append(str);
            P.append(round2);
            return P.toString();
        } else if (obj instanceof Boolean) {
            return String.valueOf(obj);
        } else {
            if (obj instanceof Throwable) {
                Throwable th = (Throwable) obj;
                StringBuilder sb = new StringBuilder(z2 ? th.getClass().getName() : th.toString());
                String y2 = y(u4.class.getCanonicalName());
                StackTraceElement[] stackTrace = th.getStackTrace();
                int length = stackTrace.length;
                while (true) {
                    if (i >= length) {
                        break;
                    }
                    StackTraceElement stackTraceElement = stackTrace[i];
                    if (!stackTraceElement.isNativeMethod() && (className = stackTraceElement.getClassName()) != null && y(className).equals(y2)) {
                        sb.append(": ");
                        sb.append(stackTraceElement);
                        break;
                    }
                    i++;
                }
                return sb.toString();
            } else if (obj instanceof v3) {
                return ((v3) obj).a;
            } else {
                return z2 ? "-" : String.valueOf(obj);
            }
        }
    }

    public static String u(boolean z2, String str, Object obj, Object obj2, Object obj3) {
        String str2 = "";
        if (str == null) {
            str = str2;
        }
        String t = t(z2, obj);
        String t2 = t(z2, obj2);
        String t3 = t(z2, obj3);
        StringBuilder sb = new StringBuilder();
        if (!TextUtils.isEmpty(str)) {
            sb.append(str);
            str2 = ": ";
        }
        String str3 = ", ";
        if (!TextUtils.isEmpty(t)) {
            sb.append(str2);
            sb.append(t);
            str2 = str3;
        }
        if (!TextUtils.isEmpty(t2)) {
            sb.append(str2);
            sb.append(t2);
        } else {
            str3 = str2;
        }
        if (!TextUtils.isEmpty(t3)) {
            sb.append(str3);
            sb.append(t3);
        }
        return sb.toString();
    }

    public static String y(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        int lastIndexOf = str.lastIndexOf(46);
        return lastIndexOf == -1 ? str : str.substring(0, lastIndexOf);
    }

    public final s3 A() {
        return this.i;
    }

    public final s3 B() {
        return this.k;
    }

    public final s3 C() {
        return this.m;
    }

    public final s3 D() {
        return this.n;
    }

    public final String E() {
        long j;
        Pair<String, Long> pair;
        k4 k4Var = l().e;
        k4Var.e.b();
        k4Var.e.b();
        long j2 = k4Var.e.w().getLong(k4Var.a, 0L);
        if (j2 == 0) {
            k4Var.a();
            j = 0;
        } else {
            Objects.requireNonNull((c) k4Var.e.a.o);
            j = Math.abs(j2 - System.currentTimeMillis());
        }
        long j3 = k4Var.d;
        if (j >= j3) {
            if (j > (j3 << 1)) {
                k4Var.a();
            } else {
                String string = k4Var.e.w().getString(k4Var.c, null);
                long j4 = k4Var.e.w().getLong(k4Var.f1537b, 0L);
                k4Var.a();
                if (string == null || j4 <= 0) {
                    pair = d4.c;
                } else {
                    pair = new Pair<>(string, Long.valueOf(j4));
                }
                if (pair != null || pair == d4.c) {
                    return null;
                }
                String valueOf = String.valueOf(pair.second);
                String str = (String) pair.first;
                return a.j(a.b(str, valueOf.length() + 1), valueOf, ":", str);
            }
        }
        pair = null;
        if (pair != null) {
        }
        return null;
    }

    public final String F() {
        String str;
        synchronized (this) {
            if (this.e == null) {
                String str2 = this.a.e;
                if (str2 == null) {
                    str2 = "FA";
                }
                this.e = str2;
            }
            str = this.e;
        }
        return str;
    }

    @Override // b.i.a.f.i.b.r5
    public final boolean r() {
        return false;
    }

    public final void v(int i, String str) {
        Log.println(i, F(), str);
    }

    public final void w(int i, boolean z2, boolean z3, String str, Object obj, Object obj2, Object obj3) {
        if (!z2 && Log.isLoggable(F(), i)) {
            Log.println(i, F(), u(false, str, obj, obj2, obj3));
        }
        if (!z3 && i >= 5) {
            Objects.requireNonNull(str, "null reference");
            r4 r4Var = this.a.k;
            if (r4Var == null) {
                v(6, "Scheduler not set. Not logging error/warn");
            } else if (!r4Var.n()) {
                v(6, "Scheduler not initialized. Not logging error/warn");
            } else {
                if (i < 0) {
                    i = 0;
                }
                r4Var.v(new t3(this, i >= 9 ? 8 : i, str, obj, obj2, obj3));
            }
        }
    }

    public final boolean x(int i) {
        return Log.isLoggable(F(), i);
    }

    public final s3 z() {
        return this.f;
    }
}
