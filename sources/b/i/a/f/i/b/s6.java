package b.i.a.f.i.b;

import java.util.concurrent.atomic.AtomicReference;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class s6 implements Runnable {
    public final /* synthetic */ AtomicReference j;
    public final /* synthetic */ c6 k;

    public s6(c6 c6Var, AtomicReference atomicReference) {
        this.k = c6Var;
        this.j = atomicReference;
    }

    @Override // java.lang.Runnable
    public final void run() {
        synchronized (this.j) {
            AtomicReference atomicReference = this.j;
            c6 c6Var = this.k;
            c cVar = c6Var.a.h;
            n3 o = c6Var.o();
            o.t();
            atomicReference.set(Integer.valueOf(cVar.q(o.c, p.N)));
            this.j.notify();
        }
    }
}
