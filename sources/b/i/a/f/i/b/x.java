package b.i.a.f.i.b;

import b.c.a.a0.d;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class x implements Runnable {
    public final /* synthetic */ String j;
    public final /* synthetic */ long k;
    public final /* synthetic */ a l;

    public x(a aVar, String str, long j) {
        this.l = aVar;
        this.j = str;
        this.k = j;
    }

    @Override // java.lang.Runnable
    public final void run() {
        a aVar = this.l;
        String str = this.j;
        long j = this.k;
        aVar.b();
        d.w(str);
        Integer num = aVar.c.get(str);
        if (num != null) {
            i7 w = aVar.q().w(false);
            int intValue = num.intValue() - 1;
            if (intValue == 0) {
                aVar.c.remove(str);
                Long l = aVar.f1508b.get(str);
                if (l == null) {
                    aVar.g().f.a("First ad unit exposure time was never set");
                } else {
                    aVar.f1508b.remove(str);
                    aVar.w(str, j - l.longValue(), w);
                }
                if (aVar.c.isEmpty()) {
                    long j2 = aVar.d;
                    if (j2 == 0) {
                        aVar.g().f.a("First ad exposure time was never set");
                        return;
                    }
                    aVar.u(j - j2, w);
                    aVar.d = 0L;
                    return;
                }
                return;
            }
            aVar.c.put(str, Integer.valueOf(intValue));
            return;
        }
        aVar.g().f.b("Call to endAdUnitExposure for unknown ad unit id", str);
    }
}
