package b.i.a.f.i.b;

import android.os.Bundle;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.d.b.a.a;
import com.google.android.gms.measurement.internal.zzap;
import com.google.android.gms.measurement.internal.zzaq;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class o3 extends r5 {
    public static final AtomicReference<String[]> c = new AtomicReference<>();
    public static final AtomicReference<String[]> d = new AtomicReference<>();
    public static final AtomicReference<String[]> e = new AtomicReference<>();

    public o3(u4 u4Var) {
        super(u4Var);
    }

    @Nullable
    public static String v(String str, String[] strArr, String[] strArr2, AtomicReference<String[]> atomicReference) {
        String str2;
        Objects.requireNonNull(atomicReference, "null reference");
        d.l(strArr.length == strArr2.length);
        for (int i = 0; i < strArr.length; i++) {
            if (t9.q0(str, strArr[i])) {
                synchronized (atomicReference) {
                    String[] strArr3 = atomicReference.get();
                    if (strArr3 == null) {
                        strArr3 = new String[strArr2.length];
                        atomicReference.set(strArr3);
                    }
                    if (strArr3[i] == null) {
                        strArr3[i] = strArr2[i] + "(" + strArr[i] + ")";
                    }
                    str2 = strArr3[i];
                }
                return str2;
            }
        }
        return str;
    }

    @Override // b.i.a.f.i.b.r5
    public final boolean r() {
        return false;
    }

    @Nullable
    public final String s(Bundle bundle) {
        String str;
        if (bundle == null) {
            return null;
        }
        if (!z()) {
            return bundle.toString();
        }
        StringBuilder R = a.R("Bundle[{");
        for (String str2 : bundle.keySet()) {
            if (R.length() != 8) {
                R.append(", ");
            }
            R.append(x(str2));
            R.append("=");
            Object obj = bundle.get(str2);
            if (obj instanceof Bundle) {
                str = w(new Object[]{obj});
            } else if (obj instanceof Object[]) {
                str = w((Object[]) obj);
            } else if (obj instanceof ArrayList) {
                str = w(((ArrayList) obj).toArray());
            } else {
                str = String.valueOf(obj);
            }
            R.append(str);
        }
        R.append("}]");
        return R.toString();
    }

    @Nullable
    public final String t(zzaq zzaqVar) {
        String str;
        if (!z()) {
            return zzaqVar.toString();
        }
        StringBuilder R = a.R("origin=");
        R.append(zzaqVar.l);
        R.append(",name=");
        R.append(u(zzaqVar.j));
        R.append(",params=");
        zzap zzapVar = zzaqVar.k;
        if (zzapVar == null) {
            str = null;
        } else if (!z()) {
            str = zzapVar.toString();
        } else {
            str = s(zzapVar.x0());
        }
        R.append(str);
        return R.toString();
    }

    @Nullable
    public final String u(String str) {
        if (str == null) {
            return null;
        }
        return !z() ? str : v(str, v5.c, v5.a, c);
    }

    @Nullable
    public final String w(Object[] objArr) {
        String str;
        if (objArr == null) {
            return "[]";
        }
        StringBuilder R = a.R("[");
        for (Object obj : objArr) {
            if (obj instanceof Bundle) {
                str = s((Bundle) obj);
            } else {
                str = String.valueOf(obj);
            }
            if (str != null) {
                if (R.length() != 1) {
                    R.append(", ");
                }
                R.append(str);
            }
        }
        R.append("]");
        return R.toString();
    }

    @Nullable
    public final String x(String str) {
        if (str == null) {
            return null;
        }
        return !z() ? str : v(str, u5.f1571b, u5.a, d);
    }

    @Nullable
    public final String y(String str) {
        if (str == null) {
            return null;
        }
        if (!z()) {
            return str;
        }
        if (!str.startsWith("_exp_")) {
            return v(str, x5.f1574b, x5.a, e);
        }
        return "experiment_id(" + str + ")";
    }

    public final boolean z() {
        return this.a.v() && this.a.g().x(3);
    }
}
