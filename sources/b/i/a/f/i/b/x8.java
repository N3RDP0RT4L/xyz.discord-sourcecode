package b.i.a.f.i.b;

import androidx.annotation.WorkerThread;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class x8 {
    public b9 a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ w8 f1575b;

    public x8(w8 w8Var) {
        this.f1575b = w8Var;
    }

    @WorkerThread
    public final void a() {
        this.f1575b.b();
        b9 b9Var = this.a;
        if (b9Var != null) {
            this.f1575b.c.removeCallbacks(b9Var);
        }
        if (this.f1575b.a.h.o(p.v0)) {
            this.f1575b.l().f1521x.a(false);
        }
    }
}
