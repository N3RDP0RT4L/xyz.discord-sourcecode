package b.i.a.f.i.b;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public abstract class r5 extends s5 {

    /* renamed from: b  reason: collision with root package name */
    public boolean f1562b;

    public r5(u4 u4Var) {
        super(u4Var);
        this.a.F++;
    }

    public void m() {
    }

    public final boolean n() {
        return this.f1562b;
    }

    public final void o() {
        if (!n()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    public final void p() {
        if (this.f1562b) {
            throw new IllegalStateException("Can't initialize twice");
        } else if (!r()) {
            this.a.G.incrementAndGet();
            this.f1562b = true;
        }
    }

    public final void q() {
        if (!this.f1562b) {
            m();
            this.a.G.incrementAndGet();
            this.f1562b = true;
            return;
        }
        throw new IllegalStateException("Can't initialize twice");
    }

    public abstract boolean r();
}
