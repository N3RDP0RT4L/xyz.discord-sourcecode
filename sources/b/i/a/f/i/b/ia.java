package b.i.a.f.i.b;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.measurement.internal.zzaq;
import com.google.android.gms.measurement.internal.zzku;
import com.google.android.gms.measurement.internal.zzz;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class ia implements Parcelable.Creator<zzz> {
    @Override // android.os.Parcelable.Creator
    public final zzz createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        long j = 0;
        String str = null;
        long j2 = 0;
        long j3 = 0;
        String str2 = null;
        zzku zzkuVar = null;
        String str3 = null;
        zzaq zzaqVar = null;
        zzaq zzaqVar2 = null;
        zzaq zzaqVar3 = null;
        boolean z2 = false;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 2:
                    str = d.R(parcel, readInt);
                    break;
                case 3:
                    str2 = d.R(parcel, readInt);
                    break;
                case 4:
                    zzkuVar = (zzku) d.Q(parcel, readInt, zzku.CREATOR);
                    break;
                case 5:
                    j = d.H1(parcel, readInt);
                    break;
                case 6:
                    z2 = d.E1(parcel, readInt);
                    break;
                case 7:
                    str3 = d.R(parcel, readInt);
                    break;
                case '\b':
                    zzaqVar = (zzaq) d.Q(parcel, readInt, zzaq.CREATOR);
                    break;
                case '\t':
                    j2 = d.H1(parcel, readInt);
                    break;
                case '\n':
                    zzaqVar2 = (zzaq) d.Q(parcel, readInt, zzaq.CREATOR);
                    break;
                case 11:
                    j3 = d.H1(parcel, readInt);
                    break;
                case '\f':
                    zzaqVar3 = (zzaq) d.Q(parcel, readInt, zzaq.CREATOR);
                    break;
                default:
                    d.d2(parcel, readInt);
                    break;
            }
        }
        d.f0(parcel, m2);
        return new zzz(str, str2, zzkuVar, j, z2, str3, zzaqVar, j2, zzaqVar2, j3, zzaqVar3);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzz[] newArray(int i) {
        return new zzz[i];
    }
}
