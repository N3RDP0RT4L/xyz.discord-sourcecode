package b.i.a.f.i.b;

import android.os.Bundle;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class d {
    public static final d a = new d(null, null);

    /* renamed from: b  reason: collision with root package name */
    public final Boolean f1519b;
    public final Boolean c;

    public d(Boolean bool, Boolean bool2) {
        this.f1519b = bool;
        this.c = bool2;
    }

    public static int a(Boolean bool) {
        if (bool == null) {
            return 0;
        }
        return bool.booleanValue() ? 1 : 2;
    }

    /* JADX WARN: Removed duplicated region for block: B:14:0x0026  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static b.i.a.f.i.b.d b(java.lang.String r7) {
        /*
            r0 = 0
            if (r7 == 0) goto L38
            int r1 = r7.length()
            r2 = 49
            r3 = 48
            r4 = 3
            if (r1 < r4) goto L1e
            r1 = 2
            char r1 = r7.charAt(r1)
            if (r1 == r3) goto L1b
            if (r1 == r2) goto L18
            goto L1e
        L18:
            java.lang.Boolean r1 = java.lang.Boolean.TRUE
            goto L1f
        L1b:
            java.lang.Boolean r1 = java.lang.Boolean.FALSE
            goto L1f
        L1e:
            r1 = r0
        L1f:
            int r5 = r7.length()
            r6 = 4
            if (r5 < r6) goto L35
            char r7 = r7.charAt(r4)
            if (r7 == r3) goto L32
            if (r7 == r2) goto L2f
            goto L35
        L2f:
            java.lang.Boolean r7 = java.lang.Boolean.TRUE
            goto L34
        L32:
            java.lang.Boolean r7 = java.lang.Boolean.FALSE
        L34:
            r0 = r7
        L35:
            r7 = r0
            r0 = r1
            goto L39
        L38:
            r7 = r0
        L39:
            b.i.a.f.i.b.d r1 = new b.i.a.f.i.b.d
            r1.<init>(r0, r7)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.d.b(java.lang.String):b.i.a.f.i.b.d");
    }

    public static Boolean c(Boolean bool, Boolean bool2) {
        if (bool == null) {
            return bool2;
        }
        if (bool2 == null) {
            return bool;
        }
        return Boolean.valueOf(bool.booleanValue() && bool2.booleanValue());
    }

    public static boolean e(int i, int i2) {
        return i <= i2;
    }

    public static d g(Bundle bundle) {
        if (bundle == null) {
            return a;
        }
        return new d(i(bundle.getString("ad_storage")), i(bundle.getString("analytics_storage")));
    }

    public static Boolean i(String str) {
        if (str == null) {
            return null;
        }
        if (str.equals("granted")) {
            return Boolean.TRUE;
        }
        if (str.equals("denied")) {
            return Boolean.FALSE;
        }
        return null;
    }

    public final String d() {
        char c;
        StringBuilder sb = new StringBuilder("G1");
        Boolean bool = this.f1519b;
        char c2 = '0';
        if (bool == null) {
            c = '-';
        } else {
            c = bool.booleanValue() ? '1' : '0';
        }
        sb.append(c);
        Boolean bool2 = this.c;
        if (bool2 == null) {
            c2 = '-';
        } else if (bool2.booleanValue()) {
            c2 = '1';
        }
        sb.append(c2);
        return sb.toString();
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof d)) {
            return false;
        }
        d dVar = (d) obj;
        return a(this.f1519b) == a(dVar.f1519b) && a(this.c) == a(dVar.c);
    }

    public final boolean f(d dVar) {
        Boolean bool = this.f1519b;
        Boolean bool2 = Boolean.FALSE;
        if (bool != bool2 || dVar.f1519b == bool2) {
            return this.c == bool2 && dVar.c != bool2;
        }
        return true;
    }

    public final d h(d dVar) {
        return new d(c(this.f1519b, dVar.f1519b), c(this.c, dVar.c));
    }

    public final int hashCode() {
        return a(this.c) + ((a(this.f1519b) + 527) * 31);
    }

    public final boolean j() {
        Boolean bool = this.f1519b;
        return bool == null || bool.booleanValue();
    }

    public final boolean k() {
        Boolean bool = this.c;
        return bool == null || bool.booleanValue();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("ConsentSettings: ");
        sb.append("adStorage=");
        Boolean bool = this.f1519b;
        String str = "granted";
        if (bool == null) {
            sb.append("uninitialized");
        } else {
            sb.append(bool.booleanValue() ? str : "denied");
        }
        sb.append(", analyticsStorage=");
        Boolean bool2 = this.c;
        if (bool2 == null) {
            sb.append("uninitialized");
        } else {
            if (!bool2.booleanValue()) {
                str = "denied";
            }
            sb.append(str);
        }
        return sb.toString();
    }
}
