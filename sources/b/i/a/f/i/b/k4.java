package b.i.a.f.i.b;

import android.content.SharedPreferences;
import androidx.annotation.WorkerThread;
import b.c.a.a0.d;
import b.i.a.f.e.o.c;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class k4 {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1537b;
    public final String c;
    public final long d;
    public final /* synthetic */ d4 e;

    public k4(d4 d4Var, String str, long j, g4 g4Var) {
        this.e = d4Var;
        d.w(str);
        d.l(j > 0);
        this.a = str.concat(":start");
        this.f1537b = str.concat(":count");
        this.c = str.concat(":value");
        this.d = j;
    }

    @WorkerThread
    public final void a() {
        this.e.b();
        Objects.requireNonNull((c) this.e.a.o);
        long currentTimeMillis = System.currentTimeMillis();
        SharedPreferences.Editor edit = this.e.w().edit();
        edit.remove(this.f1537b);
        edit.remove(this.c);
        edit.putLong(this.a, currentTimeMillis);
        edit.apply();
    }
}
