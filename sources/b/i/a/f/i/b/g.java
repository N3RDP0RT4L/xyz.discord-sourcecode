package b.i.a.f.i.b;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Pair;
import androidx.annotation.WorkerThread;
import b.c.a.a0.d;
import b.d.b.a.a;
import b.i.a.f.e.o.c;
import b.i.a.f.h.l.a1;
import b.i.a.f.h.l.da;
import b.i.a.f.h.l.e1;
import b.i.a.f.h.l.l0;
import b.i.a.f.h.l.o0;
import b.i.a.f.h.l.o8;
import b.i.a.f.h.l.u4;
import com.discord.models.domain.ModelAuditLogEntry;
import com.google.android.gms.measurement.internal.zzz;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.objectweb.asm.Opcodes;
/* compiled from: com.google.android.gms:play-services-measurement@@18.0.0 */
/* loaded from: classes3.dex */
public final class g extends i9 {
    public static final String[] d = {"last_bundled_timestamp", "ALTER TABLE events ADD COLUMN last_bundled_timestamp INTEGER;", "last_bundled_day", "ALTER TABLE events ADD COLUMN last_bundled_day INTEGER;", "last_sampled_complex_event_id", "ALTER TABLE events ADD COLUMN last_sampled_complex_event_id INTEGER;", "last_sampling_rate", "ALTER TABLE events ADD COLUMN last_sampling_rate INTEGER;", "last_exempt_from_sampling", "ALTER TABLE events ADD COLUMN last_exempt_from_sampling INTEGER;", "current_session_count", "ALTER TABLE events ADD COLUMN current_session_count INTEGER;"};
    public static final String[] e = {"origin", "ALTER TABLE user_attributes ADD COLUMN origin TEXT;"};
    public static final String[] f = {"app_version", "ALTER TABLE apps ADD COLUMN app_version TEXT;", "app_store", "ALTER TABLE apps ADD COLUMN app_store TEXT;", "gmp_version", "ALTER TABLE apps ADD COLUMN gmp_version INTEGER;", "dev_cert_hash", "ALTER TABLE apps ADD COLUMN dev_cert_hash INTEGER;", "measurement_enabled", "ALTER TABLE apps ADD COLUMN measurement_enabled INTEGER;", "last_bundle_start_timestamp", "ALTER TABLE apps ADD COLUMN last_bundle_start_timestamp INTEGER;", "day", "ALTER TABLE apps ADD COLUMN day INTEGER;", "daily_public_events_count", "ALTER TABLE apps ADD COLUMN daily_public_events_count INTEGER;", "daily_events_count", "ALTER TABLE apps ADD COLUMN daily_events_count INTEGER;", "daily_conversions_count", "ALTER TABLE apps ADD COLUMN daily_conversions_count INTEGER;", "remote_config", "ALTER TABLE apps ADD COLUMN remote_config BLOB;", "config_fetched_time", "ALTER TABLE apps ADD COLUMN config_fetched_time INTEGER;", "failed_config_fetch_time", "ALTER TABLE apps ADD COLUMN failed_config_fetch_time INTEGER;", "app_version_int", "ALTER TABLE apps ADD COLUMN app_version_int INTEGER;", "firebase_instance_id", "ALTER TABLE apps ADD COLUMN firebase_instance_id TEXT;", "daily_error_events_count", "ALTER TABLE apps ADD COLUMN daily_error_events_count INTEGER;", "daily_realtime_events_count", "ALTER TABLE apps ADD COLUMN daily_realtime_events_count INTEGER;", "health_monitor_sample", "ALTER TABLE apps ADD COLUMN health_monitor_sample TEXT;", "android_id", "ALTER TABLE apps ADD COLUMN android_id INTEGER;", "adid_reporting_enabled", "ALTER TABLE apps ADD COLUMN adid_reporting_enabled INTEGER;", "ssaid_reporting_enabled", "ALTER TABLE apps ADD COLUMN ssaid_reporting_enabled INTEGER;", "admob_app_id", "ALTER TABLE apps ADD COLUMN admob_app_id TEXT;", "linked_admob_app_id", "ALTER TABLE apps ADD COLUMN linked_admob_app_id TEXT;", "dynamite_version", "ALTER TABLE apps ADD COLUMN dynamite_version INTEGER;", "safelisted_events", "ALTER TABLE apps ADD COLUMN safelisted_events TEXT;", "ga_app_id", "ALTER TABLE apps ADD COLUMN ga_app_id TEXT;"};
    public static final String[] g = {"realtime", "ALTER TABLE raw_events ADD COLUMN realtime INTEGER;"};
    public static final String[] h = {"has_realtime", "ALTER TABLE queue ADD COLUMN has_realtime INTEGER;", "retry_count", "ALTER TABLE queue ADD COLUMN retry_count INTEGER;"};
    public static final String[] i = {"session_scoped", "ALTER TABLE event_filters ADD COLUMN session_scoped BOOLEAN;"};
    public static final String[] j = {"session_scoped", "ALTER TABLE property_filters ADD COLUMN session_scoped BOOLEAN;"};
    public static final String[] k = {"previous_install_count", "ALTER TABLE app2 ADD COLUMN previous_install_count INTEGER;"};
    public final e9 m = new e9(this.a.o);
    public final h l = new h(this, this.a.f1566b, "google_app_measurement.db");

    public g(k9 k9Var) {
        super(k9Var);
    }

    @WorkerThread
    public static void G(ContentValues contentValues, String str, Object obj) {
        d.w(str);
        Objects.requireNonNull(obj, "null reference");
        if (obj instanceof String) {
            contentValues.put(str, (String) obj);
        } else if (obj instanceof Long) {
            contentValues.put(str, (Long) obj);
        } else if (obj instanceof Double) {
            contentValues.put(str, (Double) obj);
        } else {
            throw new IllegalArgumentException("Invalid value type");
        }
    }

    @WorkerThread
    public final Object A(Cursor cursor, int i2) {
        int type = cursor.getType(i2);
        if (type == 0) {
            g().f.a("Loaded invalid null value from database");
            return null;
        } else if (type == 1) {
            return Long.valueOf(cursor.getLong(i2));
        } else {
            if (type == 2) {
                return Double.valueOf(cursor.getDouble(i2));
            }
            if (type == 3) {
                return cursor.getString(i2);
            }
            if (type != 4) {
                g().f.b("Loaded invalid unknown value type, ignoring it", Integer.valueOf(type));
                return null;
            }
            g().f.a("Loaded invalid blob type value, ignoring it");
            return null;
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:23:0x0053  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.lang.String B(long r5) {
        /*
            r4 = this;
            r4.b()
            r4.n()
            r0 = 0
            android.database.sqlite.SQLiteDatabase r1 = r4.t()     // Catch: java.lang.Throwable -> L3a android.database.sqlite.SQLiteException -> L3c
            java.lang.String r2 = "select app_id from apps where app_id in (select distinct app_id from raw_events) and config_fetched_time < ? order by failed_config_fetch_time limit 1;"
            r3 = 1
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch: java.lang.Throwable -> L3a android.database.sqlite.SQLiteException -> L3c
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch: java.lang.Throwable -> L3a android.database.sqlite.SQLiteException -> L3c
            r6 = 0
            r3[r6] = r5     // Catch: java.lang.Throwable -> L3a android.database.sqlite.SQLiteException -> L3c
            android.database.Cursor r5 = r1.rawQuery(r2, r3)     // Catch: java.lang.Throwable -> L3a android.database.sqlite.SQLiteException -> L3c
            boolean r1 = r5.moveToFirst()     // Catch: android.database.sqlite.SQLiteException -> L38 java.lang.Throwable -> L4f
            if (r1 != 0) goto L30
            b.i.a.f.i.b.q3 r6 = r4.g()     // Catch: android.database.sqlite.SQLiteException -> L38 java.lang.Throwable -> L4f
            b.i.a.f.i.b.s3 r6 = r6.n     // Catch: android.database.sqlite.SQLiteException -> L38 java.lang.Throwable -> L4f
            java.lang.String r1 = "No expired configs for apps with pending events"
            r6.a(r1)     // Catch: android.database.sqlite.SQLiteException -> L38 java.lang.Throwable -> L4f
            r5.close()
            return r0
        L30:
            java.lang.String r6 = r5.getString(r6)     // Catch: android.database.sqlite.SQLiteException -> L38 java.lang.Throwable -> L4f
            r5.close()
            return r6
        L38:
            r6 = move-exception
            goto L3e
        L3a:
            r6 = move-exception
            goto L51
        L3c:
            r6 = move-exception
            r5 = r0
        L3e:
            b.i.a.f.i.b.q3 r1 = r4.g()     // Catch: java.lang.Throwable -> L4f
            b.i.a.f.i.b.s3 r1 = r1.f     // Catch: java.lang.Throwable -> L4f
            java.lang.String r2 = "Error selecting expired configs"
            r1.b(r2, r6)     // Catch: java.lang.Throwable -> L4f
            if (r5 == 0) goto L4e
            r5.close()
        L4e:
            return r0
        L4f:
            r6 = move-exception
            r0 = r5
        L51:
            if (r0 == 0) goto L56
            r0.close()
        L56:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.g.B(long):java.lang.String");
    }

    /* JADX WARN: Not initialized variable reg: 2, insn: 0x00b2: MOVE  (r1 I:??[OBJECT, ARRAY]) = (r2 I:??[OBJECT, ARRAY]), block:B:35:0x00b2 */
    /* JADX WARN: Removed duplicated region for block: B:37:0x00b5  */
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.util.List<b.i.a.f.i.b.u9> C(java.lang.String r14) {
        /*
            r13 = this;
            b.c.a.a0.d.w(r14)
            r13.b()
            r13.n()
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r1 = 0
            android.database.sqlite.SQLiteDatabase r2 = r13.t()     // Catch: java.lang.Throwable -> L7c android.database.sqlite.SQLiteException -> L7e
            java.lang.String r3 = "user_attributes"
            java.lang.String r4 = "name"
            java.lang.String r5 = "origin"
            java.lang.String r6 = "set_timestamp"
            java.lang.String r7 = "value"
            java.lang.String[] r4 = new java.lang.String[]{r4, r5, r6, r7}     // Catch: java.lang.Throwable -> L7c android.database.sqlite.SQLiteException -> L7e
            java.lang.String r5 = "app_id=?"
            r11 = 1
            java.lang.String[] r6 = new java.lang.String[r11]     // Catch: java.lang.Throwable -> L7c android.database.sqlite.SQLiteException -> L7e
            r12 = 0
            r6[r12] = r14     // Catch: java.lang.Throwable -> L7c android.database.sqlite.SQLiteException -> L7e
            r7 = 0
            r8 = 0
            java.lang.String r9 = "rowid"
            java.lang.String r10 = "1000"
            android.database.Cursor r2 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch: java.lang.Throwable -> L7c android.database.sqlite.SQLiteException -> L7e
            boolean r3 = r2.moveToFirst()     // Catch: android.database.sqlite.SQLiteException -> L7a java.lang.Throwable -> Lb1
            if (r3 != 0) goto L3d
            r2.close()
            return r0
        L3d:
            java.lang.String r7 = r2.getString(r12)     // Catch: android.database.sqlite.SQLiteException -> L7a java.lang.Throwable -> Lb1
            java.lang.String r3 = r2.getString(r11)     // Catch: android.database.sqlite.SQLiteException -> L7a java.lang.Throwable -> Lb1
            if (r3 != 0) goto L49
            java.lang.String r3 = ""
        L49:
            r6 = r3
            r3 = 2
            long r8 = r2.getLong(r3)     // Catch: android.database.sqlite.SQLiteException -> L7a java.lang.Throwable -> Lb1
            r3 = 3
            java.lang.Object r10 = r13.A(r2, r3)     // Catch: android.database.sqlite.SQLiteException -> L7a java.lang.Throwable -> Lb1
            if (r10 != 0) goto L66
            b.i.a.f.i.b.q3 r3 = r13.g()     // Catch: android.database.sqlite.SQLiteException -> L7a java.lang.Throwable -> Lb1
            b.i.a.f.i.b.s3 r3 = r3.f     // Catch: android.database.sqlite.SQLiteException -> L7a java.lang.Throwable -> Lb1
            java.lang.String r4 = "Read invalid user property value, ignoring it. appId"
            java.lang.Object r5 = b.i.a.f.i.b.q3.s(r14)     // Catch: android.database.sqlite.SQLiteException -> L7a java.lang.Throwable -> Lb1
            r3.b(r4, r5)     // Catch: android.database.sqlite.SQLiteException -> L7a java.lang.Throwable -> Lb1
            goto L70
        L66:
            b.i.a.f.i.b.u9 r3 = new b.i.a.f.i.b.u9     // Catch: android.database.sqlite.SQLiteException -> L7a java.lang.Throwable -> Lb1
            r4 = r3
            r5 = r14
            r4.<init>(r5, r6, r7, r8, r10)     // Catch: android.database.sqlite.SQLiteException -> L7a java.lang.Throwable -> Lb1
            r0.add(r3)     // Catch: android.database.sqlite.SQLiteException -> L7a java.lang.Throwable -> Lb1
        L70:
            boolean r3 = r2.moveToNext()     // Catch: android.database.sqlite.SQLiteException -> L7a java.lang.Throwable -> Lb1
            if (r3 != 0) goto L3d
            r2.close()
            return r0
        L7a:
            r0 = move-exception
            goto L80
        L7c:
            r14 = move-exception
            goto Lb3
        L7e:
            r0 = move-exception
            r2 = r1
        L80:
            b.i.a.f.i.b.q3 r3 = r13.g()     // Catch: java.lang.Throwable -> Lb1
            b.i.a.f.i.b.s3 r3 = r3.f     // Catch: java.lang.Throwable -> Lb1
            java.lang.String r4 = "Error querying user properties. appId"
            java.lang.Object r5 = b.i.a.f.i.b.q3.s(r14)     // Catch: java.lang.Throwable -> Lb1
            r3.c(r4, r5, r0)     // Catch: java.lang.Throwable -> Lb1
            boolean r0 = b.i.a.f.h.l.l9.b()     // Catch: java.lang.Throwable -> Lb1
            if (r0 == 0) goto Lab
            b.i.a.f.i.b.u4 r0 = r13.a     // Catch: java.lang.Throwable -> Lb1
            b.i.a.f.i.b.c r0 = r0.h     // Catch: java.lang.Throwable -> Lb1
            b.i.a.f.i.b.j3<java.lang.Boolean> r3 = b.i.a.f.i.b.p.F0     // Catch: java.lang.Throwable -> Lb1
            boolean r14 = r0.u(r14, r3)     // Catch: java.lang.Throwable -> Lb1
            if (r14 == 0) goto Lab
            java.util.List r14 = java.util.Collections.emptyList()     // Catch: java.lang.Throwable -> Lb1
            if (r2 == 0) goto Laa
            r2.close()
        Laa:
            return r14
        Lab:
            if (r2 == 0) goto Lb0
            r2.close()
        Lb0:
            return r1
        Lb1:
            r14 = move-exception
            r1 = r2
        Lb3:
            if (r1 == 0) goto Lb8
            r1.close()
        Lb8:
            throw r14
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.g.C(java.lang.String):java.util.List");
    }

    @WorkerThread
    public final List<Pair<e1, Long>> D(String str, int i2, int i3) {
        byte[] S;
        b();
        n();
        d.l(i2 > 0);
        d.l(i3 > 0);
        d.w(str);
        Cursor cursor = null;
        try {
            try {
                Cursor query = t().query("queue", new String[]{"rowid", "data", "retry_count"}, "app_id=?", new String[]{str}, null, null, "rowid", String.valueOf(i2));
                if (!query.moveToFirst()) {
                    List<Pair<e1, Long>> emptyList = Collections.emptyList();
                    query.close();
                    return emptyList;
                }
                ArrayList arrayList = new ArrayList();
                int i4 = 0;
                do {
                    long j2 = query.getLong(0);
                    try {
                        S = m().S(query.getBlob(1));
                    } catch (IOException e2) {
                        g().f.c("Failed to unzip queued bundle. appId", q3.s(str), e2);
                    }
                    if (!arrayList.isEmpty() && S.length + i4 > i3) {
                        break;
                    }
                    try {
                        e1.a aVar = (e1.a) q9.x(e1.u0(), S);
                        if (!query.isNull(2)) {
                            int i5 = query.getInt(2);
                            if (aVar.l) {
                                aVar.n();
                                aVar.l = false;
                            }
                            e1.V0((e1) aVar.k, i5);
                        }
                        i4 += S.length;
                        arrayList.add(Pair.create((e1) ((u4) aVar.p()), Long.valueOf(j2)));
                    } catch (IOException e3) {
                        g().f.c("Failed to merge queued bundle. appId", q3.s(str), e3);
                    }
                    if (!query.moveToNext()) {
                        break;
                    }
                } while (i4 <= i3);
                query.close();
                return arrayList;
            } catch (SQLiteException e4) {
                g().f.c("Error querying bundles. appId", q3.s(str), e4);
                List<Pair<e1, Long>> emptyList2 = Collections.emptyList();
                if (0 != 0) {
                    cursor.close();
                }
                return emptyList2;
            }
        } catch (Throwable th) {
            if (0 != 0) {
                cursor.close();
            }
            throw th;
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:18:0x0090, code lost:
        g().f.b("Read more than the max allowed user properties, ignoring excess", 1000);
     */
    /* JADX WARN: Removed duplicated region for block: B:45:0x011b  */
    /* JADX WARN: Removed duplicated region for block: B:50:0x0123  */
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.util.List<b.i.a.f.i.b.u9> E(java.lang.String r22, java.lang.String r23, java.lang.String r24) {
        /*
            Method dump skipped, instructions count: 295
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.g.E(java.lang.String, java.lang.String, java.lang.String):java.util.List");
    }

    /* JADX WARN: Code restructure failed: missing block: B:9:0x0050, code lost:
        g().f.b("Read more than the max allowed conditional properties, ignoring extra", 1000);
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.util.List<com.google.android.gms.measurement.internal.zzz> F(java.lang.String r27, java.lang.String[] r28) {
        /*
            Method dump skipped, instructions count: 286
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.g.F(java.lang.String, java.lang.String[]):java.util.List");
    }

    @WorkerThread
    public final void H(l lVar) {
        Objects.requireNonNull(lVar, "null reference");
        b();
        n();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", lVar.a);
        contentValues.put(ModelAuditLogEntry.CHANGE_KEY_NAME, lVar.f1544b);
        contentValues.put("lifetime_count", Long.valueOf(lVar.c));
        contentValues.put("current_bundle_count", Long.valueOf(lVar.d));
        contentValues.put("last_fire_timestamp", Long.valueOf(lVar.f));
        contentValues.put("last_bundled_timestamp", Long.valueOf(lVar.g));
        contentValues.put("last_bundled_day", lVar.h);
        contentValues.put("last_sampled_complex_event_id", lVar.i);
        contentValues.put("last_sampling_rate", lVar.j);
        contentValues.put("current_session_count", Long.valueOf(lVar.e));
        Boolean bool = lVar.k;
        contentValues.put("last_exempt_from_sampling", (bool == null || !bool.booleanValue()) ? null : 1L);
        try {
            if (t().insertWithOnConflict("events", null, contentValues, 5) == -1) {
                g().f.b("Failed to insert/update event aggregates (got -1). appId", q3.s(lVar.a));
            }
        } catch (SQLiteException e2) {
            g().f.c("Error storing event aggregates. appId", q3.s(lVar.a), e2);
        }
    }

    @WorkerThread
    public final void I(a4 a4Var) {
        b();
        n();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", a4Var.o());
        contentValues.put("app_instance_id", a4Var.s());
        contentValues.put("gmp_app_id", a4Var.v());
        contentValues.put("resettable_device_id_hash", a4Var.E());
        contentValues.put("last_bundle_index", Long.valueOf(a4Var.U()));
        contentValues.put("last_bundle_start_timestamp", Long.valueOf(a4Var.K()));
        contentValues.put("last_bundle_end_timestamp", Long.valueOf(a4Var.L()));
        contentValues.put("app_version", a4Var.M());
        contentValues.put("app_store", a4Var.O());
        contentValues.put("gmp_version", Long.valueOf(a4Var.P()));
        contentValues.put("dev_cert_hash", Long.valueOf(a4Var.Q()));
        contentValues.put("measurement_enabled", Boolean.valueOf(a4Var.T()));
        a4Var.a.f().b();
        contentValues.put("day", Long.valueOf(a4Var.f1511x));
        a4Var.a.f().b();
        contentValues.put("daily_public_events_count", Long.valueOf(a4Var.f1512y));
        a4Var.a.f().b();
        contentValues.put("daily_events_count", Long.valueOf(a4Var.f1513z));
        a4Var.a.f().b();
        contentValues.put("daily_conversions_count", Long.valueOf(a4Var.A));
        a4Var.a.f().b();
        contentValues.put("config_fetched_time", Long.valueOf(a4Var.F));
        a4Var.a.f().b();
        contentValues.put("failed_config_fetch_time", Long.valueOf(a4Var.G));
        contentValues.put("app_version_int", Long.valueOf(a4Var.N()));
        contentValues.put("firebase_instance_id", a4Var.H());
        a4Var.a.f().b();
        contentValues.put("daily_error_events_count", Long.valueOf(a4Var.B));
        a4Var.a.f().b();
        contentValues.put("daily_realtime_events_count", Long.valueOf(a4Var.C));
        a4Var.a.f().b();
        contentValues.put("health_monitor_sample", a4Var.D);
        contentValues.put("android_id", Long.valueOf(a4Var.g()));
        contentValues.put("adid_reporting_enabled", Boolean.valueOf(a4Var.h()));
        contentValues.put("ssaid_reporting_enabled", Boolean.valueOf(a4Var.i()));
        contentValues.put("admob_app_id", a4Var.y());
        contentValues.put("dynamite_version", Long.valueOf(a4Var.S()));
        if (a4Var.k() != null) {
            if (a4Var.k().size() == 0) {
                g().i.b("Safelisted events should not be an empty list. appId", a4Var.o());
            } else {
                contentValues.put("safelisted_events", TextUtils.join(",", a4Var.k()));
            }
        }
        if (da.b() && this.a.h.u(a4Var.o(), p.f1557j0)) {
            contentValues.put("ga_app_id", a4Var.B());
        }
        try {
            SQLiteDatabase t = t();
            if (t.update("apps", contentValues, "app_id = ?", new String[]{a4Var.o()}) == 0 && t.insertWithOnConflict("apps", null, contentValues, 5) == -1) {
                g().f.b("Failed to insert/update app (got -1). appId", q3.s(a4Var.o()));
            }
        } catch (SQLiteException e2) {
            g().f.c("Error storing app. appId", q3.s(a4Var.o()), e2);
        }
    }

    @WorkerThread
    public final void J(List<Long> list) {
        b();
        n();
        Objects.requireNonNull(list, "null reference");
        if (list.size() == 0) {
            throw new IllegalArgumentException("Given Integer is zero");
        } else if (R()) {
            String join = TextUtils.join(",", list);
            String j2 = a.j(a.b(join, 2), "(", join, ")");
            if (S(a.j(a.b(j2, 80), "SELECT COUNT(1) FROM queue WHERE rowid IN ", j2, " AND retry_count =  2147483647 LIMIT 1"), null) > 0) {
                g().i.a("The number of upload retries exceeds the limit. Will remain unchanged.");
            }
            try {
                SQLiteDatabase t = t();
                StringBuilder sb = new StringBuilder(String.valueOf(j2).length() + Opcodes.LAND);
                sb.append("UPDATE queue SET retry_count = IFNULL(retry_count, 0) + 1 WHERE rowid IN ");
                sb.append(j2);
                sb.append(" AND (retry_count IS NULL OR retry_count < 2147483647)");
                t.execSQL(sb.toString());
            } catch (SQLiteException e2) {
                g().f.b("Error incrementing retry count. error", e2);
            }
        }
    }

    @WorkerThread
    public final boolean K(e1 e1Var, boolean z2) {
        b();
        n();
        d.w(e1Var.G1());
        d.F(e1Var.n1());
        i0();
        Objects.requireNonNull((c) this.a.o);
        long currentTimeMillis = System.currentTimeMillis();
        if (e1Var.o1() < currentTimeMillis - c.B() || e1Var.o1() > c.B() + currentTimeMillis) {
            g().i.d("Storing bundle outside of the max uploading time span. appId, now, timestamp", q3.s(e1Var.G1()), Long.valueOf(currentTimeMillis), Long.valueOf(e1Var.o1()));
        }
        try {
            byte[] T = m().T(e1Var.d());
            g().n.b("Saving bundle, size", Integer.valueOf(T.length));
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", e1Var.G1());
            contentValues.put("bundle_end_timestamp", Long.valueOf(e1Var.o1()));
            contentValues.put("data", T);
            contentValues.put("has_realtime", Integer.valueOf(z2 ? 1 : 0));
            if (e1Var.f0()) {
                contentValues.put("retry_count", Integer.valueOf(e1Var.n0()));
            }
            try {
                if (t().insert("queue", null, contentValues) != -1) {
                    return true;
                }
                g().f.b("Failed to insert bundle (got -1). appId", q3.s(e1Var.G1()));
                return false;
            } catch (SQLiteException e2) {
                g().f.c("Error storing bundle. appId", q3.s(e1Var.G1()), e2);
                return false;
            }
        } catch (IOException e3) {
            g().f.c("Data loss. Failed to serialize bundle. appId", q3.s(e1Var.G1()), e3);
            return false;
        }
    }

    public final boolean L(m mVar, long j2, boolean z2) {
        b();
        n();
        d.w(mVar.a);
        byte[] d2 = m().v(mVar).d();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", mVar.a);
        contentValues.put(ModelAuditLogEntry.CHANGE_KEY_NAME, mVar.f1545b);
        contentValues.put("timestamp", Long.valueOf(mVar.d));
        contentValues.put("metadata_fingerprint", Long.valueOf(j2));
        contentValues.put("data", d2);
        contentValues.put("realtime", Integer.valueOf(z2 ? 1 : 0));
        try {
            if (t().insert("raw_events", null, contentValues) != -1) {
                return true;
            }
            g().f.b("Failed to insert raw event (got -1). appId", q3.s(mVar.a));
            return false;
        } catch (SQLiteException e2) {
            g().f.c("Error storing raw event. appId", q3.s(mVar.a), e2);
            return false;
        }
    }

    @WorkerThread
    public final boolean M(u9 u9Var) {
        b();
        n();
        if (Y(u9Var.a, u9Var.c) == null) {
            if (t9.X(u9Var.c)) {
                long S = S("select count(1) from user_attributes where app_id=? and name not like '!_%' escape '!'", new String[]{u9Var.a});
                c cVar = this.a.h;
                String str = u9Var.a;
                Objects.requireNonNull(cVar);
                int i2 = 25;
                if (o8.b() && cVar.u(null, p.w0)) {
                    i2 = Math.max(Math.min(cVar.q(str, p.G), 100), 25);
                }
                if (S >= i2) {
                    return false;
                }
            } else if (!"_npa".equals(u9Var.c) && S("select count(1) from user_attributes where app_id=? and origin=? AND name like '!_%' escape '!'", new String[]{u9Var.a, u9Var.f1572b}) >= 25) {
                return false;
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", u9Var.a);
        contentValues.put("origin", u9Var.f1572b);
        contentValues.put(ModelAuditLogEntry.CHANGE_KEY_NAME, u9Var.c);
        contentValues.put("set_timestamp", Long.valueOf(u9Var.d));
        G(contentValues, "value", u9Var.e);
        try {
            if (t().insertWithOnConflict("user_attributes", null, contentValues, 5) == -1) {
                g().f.b("Failed to insert/update user property (got -1). appId", q3.s(u9Var.a));
            }
        } catch (SQLiteException e2) {
            g().f.c("Error storing user property. appId", q3.s(u9Var.a), e2);
        }
        return true;
    }

    @WorkerThread
    public final boolean N(zzz zzzVar) {
        b();
        n();
        if (Y(zzzVar.j, zzzVar.l.k) == null && S("SELECT COUNT(1) FROM conditional_properties WHERE app_id=?", new String[]{zzzVar.j}) >= 1000) {
            return false;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", zzzVar.j);
        contentValues.put("origin", zzzVar.k);
        contentValues.put(ModelAuditLogEntry.CHANGE_KEY_NAME, zzzVar.l.k);
        G(contentValues, "value", zzzVar.l.w0());
        contentValues.put("active", Boolean.valueOf(zzzVar.n));
        contentValues.put("trigger_event_name", zzzVar.o);
        contentValues.put("trigger_timeout", Long.valueOf(zzzVar.q));
        e();
        contentValues.put("timed_out_event", t9.g0(zzzVar.p));
        contentValues.put("creation_timestamp", Long.valueOf(zzzVar.m));
        e();
        contentValues.put("triggered_event", t9.g0(zzzVar.r));
        contentValues.put("triggered_timestamp", Long.valueOf(zzzVar.l.l));
        contentValues.put("time_to_live", Long.valueOf(zzzVar.f2988s));
        e();
        contentValues.put("expired_event", t9.g0(zzzVar.t));
        try {
            if (t().insertWithOnConflict("conditional_properties", null, contentValues, 5) == -1) {
                g().f.b("Failed to insert/update conditional user property (got -1)", q3.s(zzzVar.j));
            }
        } catch (SQLiteException e2) {
            g().f.c("Error storing conditional user property", q3.s(zzzVar.j), e2);
        }
        return true;
    }

    @WorkerThread
    public final boolean O(String str, int i2, l0 l0Var) {
        n();
        b();
        d.w(str);
        Objects.requireNonNull(l0Var, "null reference");
        Integer num = null;
        if (TextUtils.isEmpty(l0Var.z())) {
            s3 s3Var = g().i;
            Object s2 = q3.s(str);
            Integer valueOf = Integer.valueOf(i2);
            if (l0Var.x()) {
                num = Integer.valueOf(l0Var.y());
            }
            s3Var.d("Event filter had no event name. Audience definition ignored. appId, audienceId, filterId", s2, valueOf, String.valueOf(num));
            return false;
        }
        byte[] d2 = l0Var.d();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", str);
        contentValues.put("audience_id", Integer.valueOf(i2));
        contentValues.put("filter_id", l0Var.x() ? Integer.valueOf(l0Var.y()) : null);
        contentValues.put("event_name", l0Var.z());
        contentValues.put("session_scoped", l0Var.G() ? Boolean.valueOf(l0Var.H()) : null);
        contentValues.put("data", d2);
        try {
            if (t().insertWithOnConflict("event_filters", null, contentValues, 5) != -1) {
                return true;
            }
            g().f.b("Failed to insert event filter (got -1). appId", q3.s(str));
            return true;
        } catch (SQLiteException e2) {
            g().f.c("Error storing event filter. appId", q3.s(str), e2);
            return false;
        }
    }

    @WorkerThread
    public final boolean P(String str, int i2, o0 o0Var) {
        n();
        b();
        d.w(str);
        Objects.requireNonNull(o0Var, "null reference");
        Integer num = null;
        if (TextUtils.isEmpty(o0Var.x())) {
            s3 s3Var = g().i;
            Object s2 = q3.s(str);
            Integer valueOf = Integer.valueOf(i2);
            if (o0Var.v()) {
                num = Integer.valueOf(o0Var.w());
            }
            s3Var.d("Property filter had no property name. Audience definition ignored. appId, audienceId, filterId", s2, valueOf, String.valueOf(num));
            return false;
        }
        byte[] d2 = o0Var.d();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", str);
        contentValues.put("audience_id", Integer.valueOf(i2));
        contentValues.put("filter_id", o0Var.v() ? Integer.valueOf(o0Var.w()) : null);
        contentValues.put("property_name", o0Var.x());
        contentValues.put("session_scoped", o0Var.B() ? Boolean.valueOf(o0Var.C()) : null);
        contentValues.put("data", d2);
        try {
            if (t().insertWithOnConflict("property_filters", null, contentValues, 5) != -1) {
                return true;
            }
            g().f.b("Failed to insert property filter (got -1). appId", q3.s(str));
            return false;
        } catch (SQLiteException e2) {
            g().f.c("Error storing property filter. appId", q3.s(str), e2);
            return false;
        }
    }

    public final boolean Q(String str, Long l, long j2, a1 a1Var) {
        b();
        n();
        Objects.requireNonNull(a1Var, "null reference");
        d.w(str);
        Objects.requireNonNull(l, "null reference");
        byte[] d2 = a1Var.d();
        g().n.c("Saving complex main event, appId, data size", d().u(str), Integer.valueOf(d2.length));
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", str);
        contentValues.put("event_id", l);
        contentValues.put("children_to_process", Long.valueOf(j2));
        contentValues.put("main_event", d2);
        try {
            if (t().insertWithOnConflict("main_event_params", null, contentValues, 5) != -1) {
                return true;
            }
            g().f.b("Failed to insert complex main event (got -1). appId", q3.s(str));
            return false;
        } catch (SQLiteException e2) {
            g().f.c("Error storing complex main event. appId", q3.s(str), e2);
            return false;
        }
    }

    public final boolean R() {
        return this.a.f1566b.getDatabasePath("google_app_measurement.db").exists();
    }

    @WorkerThread
    public final long S(String str, String[] strArr) {
        Cursor cursor = null;
        try {
            try {
                Cursor rawQuery = t().rawQuery(str, strArr);
                if (rawQuery.moveToFirst()) {
                    long j2 = rawQuery.getLong(0);
                    rawQuery.close();
                    return j2;
                }
                throw new SQLiteException("Database returned empty set");
            } catch (SQLiteException e2) {
                g().f.c("Database error", str, e2);
                throw e2;
            }
        } catch (Throwable th) {
            if (0 != 0) {
                cursor.close();
            }
            throw th;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:17:0x00f0  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x00f2  */
    /* JADX WARN: Removed duplicated region for block: B:21:0x010f  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x0111  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x012e  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x0130  */
    /* JADX WARN: Removed duplicated region for block: B:29:0x014d  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x014f  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x016f  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x0173 A[Catch: SQLiteException -> 0x0284, all -> 0x029f, TryCatch #1 {SQLiteException -> 0x0284, blocks: (B:4:0x0061, B:8:0x006b, B:10:0x00cc, B:15:0x00d6, B:19:0x00f3, B:23:0x0112, B:27:0x0131, B:31:0x0150, B:34:0x0173, B:35:0x0178, B:39:0x019e, B:43:0x01bd, B:45:0x01d9, B:48:0x01e3, B:49:0x01e7, B:50:0x01ea, B:52:0x01f2, B:57:0x01fc, B:59:0x0207, B:63:0x020f, B:66:0x0224, B:67:0x0228, B:69:0x0233, B:70:0x0245, B:72:0x024b, B:74:0x0257, B:75:0x0260, B:77:0x0271), top: B:93:0x0061 }] */
    /* JADX WARN: Removed duplicated region for block: B:37:0x019b  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x019d  */
    /* JADX WARN: Removed duplicated region for block: B:41:0x01ba  */
    /* JADX WARN: Removed duplicated region for block: B:42:0x01bc  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x01d9 A[Catch: SQLiteException -> 0x0284, all -> 0x029f, TryCatch #1 {SQLiteException -> 0x0284, blocks: (B:4:0x0061, B:8:0x006b, B:10:0x00cc, B:15:0x00d6, B:19:0x00f3, B:23:0x0112, B:27:0x0131, B:31:0x0150, B:34:0x0173, B:35:0x0178, B:39:0x019e, B:43:0x01bd, B:45:0x01d9, B:48:0x01e3, B:49:0x01e7, B:50:0x01ea, B:52:0x01f2, B:57:0x01fc, B:59:0x0207, B:63:0x020f, B:66:0x0224, B:67:0x0228, B:69:0x0233, B:70:0x0245, B:72:0x024b, B:74:0x0257, B:75:0x0260, B:77:0x0271), top: B:93:0x0061 }] */
    /* JADX WARN: Removed duplicated region for block: B:59:0x0207 A[Catch: SQLiteException -> 0x0284, all -> 0x029f, TryCatch #1 {SQLiteException -> 0x0284, blocks: (B:4:0x0061, B:8:0x006b, B:10:0x00cc, B:15:0x00d6, B:19:0x00f3, B:23:0x0112, B:27:0x0131, B:31:0x0150, B:34:0x0173, B:35:0x0178, B:39:0x019e, B:43:0x01bd, B:45:0x01d9, B:48:0x01e3, B:49:0x01e7, B:50:0x01ea, B:52:0x01f2, B:57:0x01fc, B:59:0x0207, B:63:0x020f, B:66:0x0224, B:67:0x0228, B:69:0x0233, B:70:0x0245, B:72:0x024b, B:74:0x0257, B:75:0x0260, B:77:0x0271), top: B:93:0x0061 }] */
    /* JADX WARN: Removed duplicated region for block: B:65:0x0223  */
    /* JADX WARN: Removed duplicated region for block: B:66:0x0224 A[Catch: SQLiteException -> 0x0284, all -> 0x029f, TryCatch #1 {SQLiteException -> 0x0284, blocks: (B:4:0x0061, B:8:0x006b, B:10:0x00cc, B:15:0x00d6, B:19:0x00f3, B:23:0x0112, B:27:0x0131, B:31:0x0150, B:34:0x0173, B:35:0x0178, B:39:0x019e, B:43:0x01bd, B:45:0x01d9, B:48:0x01e3, B:49:0x01e7, B:50:0x01ea, B:52:0x01f2, B:57:0x01fc, B:59:0x0207, B:63:0x020f, B:66:0x0224, B:67:0x0228, B:69:0x0233, B:70:0x0245, B:72:0x024b, B:74:0x0257, B:75:0x0260, B:77:0x0271), top: B:93:0x0061 }] */
    /* JADX WARN: Removed duplicated region for block: B:69:0x0233 A[Catch: SQLiteException -> 0x0284, all -> 0x029f, TryCatch #1 {SQLiteException -> 0x0284, blocks: (B:4:0x0061, B:8:0x006b, B:10:0x00cc, B:15:0x00d6, B:19:0x00f3, B:23:0x0112, B:27:0x0131, B:31:0x0150, B:34:0x0173, B:35:0x0178, B:39:0x019e, B:43:0x01bd, B:45:0x01d9, B:48:0x01e3, B:49:0x01e7, B:50:0x01ea, B:52:0x01f2, B:57:0x01fc, B:59:0x0207, B:63:0x020f, B:66:0x0224, B:67:0x0228, B:69:0x0233, B:70:0x0245, B:72:0x024b, B:74:0x0257, B:75:0x0260, B:77:0x0271), top: B:93:0x0061 }] */
    /* JADX WARN: Removed duplicated region for block: B:72:0x024b A[Catch: SQLiteException -> 0x0284, all -> 0x029f, TryCatch #1 {SQLiteException -> 0x0284, blocks: (B:4:0x0061, B:8:0x006b, B:10:0x00cc, B:15:0x00d6, B:19:0x00f3, B:23:0x0112, B:27:0x0131, B:31:0x0150, B:34:0x0173, B:35:0x0178, B:39:0x019e, B:43:0x01bd, B:45:0x01d9, B:48:0x01e3, B:49:0x01e7, B:50:0x01ea, B:52:0x01f2, B:57:0x01fc, B:59:0x0207, B:63:0x020f, B:66:0x0224, B:67:0x0228, B:69:0x0233, B:70:0x0245, B:72:0x024b, B:74:0x0257, B:75:0x0260, B:77:0x0271), top: B:93:0x0061 }] */
    /* JADX WARN: Removed duplicated region for block: B:77:0x0271 A[Catch: SQLiteException -> 0x0284, all -> 0x029f, TRY_LEAVE, TryCatch #1 {SQLiteException -> 0x0284, blocks: (B:4:0x0061, B:8:0x006b, B:10:0x00cc, B:15:0x00d6, B:19:0x00f3, B:23:0x0112, B:27:0x0131, B:31:0x0150, B:34:0x0173, B:35:0x0178, B:39:0x019e, B:43:0x01bd, B:45:0x01d9, B:48:0x01e3, B:49:0x01e7, B:50:0x01ea, B:52:0x01f2, B:57:0x01fc, B:59:0x0207, B:63:0x020f, B:66:0x0224, B:67:0x0228, B:69:0x0233, B:70:0x0245, B:72:0x024b, B:74:0x0257, B:75:0x0260, B:77:0x0271), top: B:93:0x0061 }] */
    /* JADX WARN: Removed duplicated region for block: B:91:0x02a3  */
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final b.i.a.f.i.b.a4 T(java.lang.String r36) {
        /*
            Method dump skipped, instructions count: 679
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.g.T(java.lang.String):b.i.a.f.i.b.a4");
    }

    @WorkerThread
    public final List<zzz> U(String str, String str2, String str3) {
        d.w(str);
        b();
        n();
        ArrayList arrayList = new ArrayList(3);
        arrayList.add(str);
        StringBuilder sb = new StringBuilder("app_id=?");
        if (!TextUtils.isEmpty(str2)) {
            arrayList.add(str2);
            sb.append(" and origin=?");
        }
        if (!TextUtils.isEmpty(str3)) {
            arrayList.add(String.valueOf(str3).concat("*"));
            sb.append(" and name glob ?");
        }
        return F(sb.toString(), (String[]) arrayList.toArray(new String[arrayList.size()]));
    }

    @WorkerThread
    public final void V(String str, String str2) {
        d.w(str);
        d.w(str2);
        b();
        n();
        try {
            t().delete("user_attributes", "app_id=? and name=?", new String[]{str, str2});
        } catch (SQLiteException e2) {
            g().f.d("Error deleting user property. appId", q3.s(str), d().y(str2), e2);
        }
    }

    public final boolean W(String str, List<Integer> list) {
        d.w(str);
        n();
        b();
        SQLiteDatabase t = t();
        try {
            long S = S("select count(1) from audience_filter_values where app_id=?", new String[]{str});
            int max = Math.max(0, Math.min(2000, this.a.h.q(str, p.F)));
            if (S <= max) {
                return false;
            }
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < list.size(); i2++) {
                Integer num = list.get(i2);
                if (num == null) {
                    return false;
                }
                arrayList.add(Integer.toString(num.intValue()));
            }
            String join = TextUtils.join(",", arrayList);
            String j2 = a.j(a.b(join, 2), "(", join, ")");
            return t.delete("audience_filter_values", a.j(a.b(j2, Opcodes.F2L), "audience_id in (select audience_id from audience_filter_values where app_id=? and audience_id not in ", j2, " order by rowid desc limit -1 offset ?)"), new String[]{str, Integer.toString(max)}) > 0;
        } catch (SQLiteException e2) {
            g().f.c("Database error querying filters. appId", q3.s(str), e2);
            return false;
        }
    }

    public final long X(String str) {
        d.w(str);
        b();
        n();
        try {
            return t().delete("raw_events", "rowid in (select rowid from raw_events where app_id=? order by rowid desc limit -1 offset ?)", new String[]{str, String.valueOf(Math.max(0, Math.min(1000000, this.a.h.q(str, p.p))))});
        } catch (SQLiteException e2) {
            g().f.c("Error deleting over the limit events. appId", q3.s(str), e2);
            return 0L;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:31:0x00a1  */
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final b.i.a.f.i.b.u9 Y(java.lang.String r19, java.lang.String r20) {
        /*
            r18 = this;
            r8 = r20
            b.c.a.a0.d.w(r19)
            b.c.a.a0.d.w(r20)
            r18.b()
            r18.n()
            r9 = 0
            android.database.sqlite.SQLiteDatabase r10 = r18.t()     // Catch: java.lang.Throwable -> L78 android.database.sqlite.SQLiteException -> L7c
            java.lang.String r11 = "user_attributes"
            java.lang.String r0 = "set_timestamp"
            java.lang.String r1 = "value"
            java.lang.String r2 = "origin"
            java.lang.String[] r12 = new java.lang.String[]{r0, r1, r2}     // Catch: java.lang.Throwable -> L78 android.database.sqlite.SQLiteException -> L7c
            java.lang.String r13 = "app_id=? and name=?"
            r0 = 2
            java.lang.String[] r14 = new java.lang.String[r0]     // Catch: java.lang.Throwable -> L78 android.database.sqlite.SQLiteException -> L7c
            r1 = 0
            r14[r1] = r19     // Catch: java.lang.Throwable -> L78 android.database.sqlite.SQLiteException -> L7c
            r2 = 1
            r14[r2] = r8     // Catch: java.lang.Throwable -> L78 android.database.sqlite.SQLiteException -> L7c
            r15 = 0
            r16 = 0
            r17 = 0
            android.database.Cursor r10 = r10.query(r11, r12, r13, r14, r15, r16, r17)     // Catch: java.lang.Throwable -> L78 android.database.sqlite.SQLiteException -> L7c
            boolean r3 = r10.moveToFirst()     // Catch: java.lang.Throwable -> L70 android.database.sqlite.SQLiteException -> L74
            if (r3 != 0) goto L3d
            r10.close()
            return r9
        L3d:
            long r5 = r10.getLong(r1)     // Catch: java.lang.Throwable -> L70 android.database.sqlite.SQLiteException -> L74
            r11 = r18
            java.lang.Object r7 = r11.A(r10, r2)     // Catch: android.database.sqlite.SQLiteException -> L6e java.lang.Throwable -> L9d
            java.lang.String r3 = r10.getString(r0)     // Catch: android.database.sqlite.SQLiteException -> L6e java.lang.Throwable -> L9d
            b.i.a.f.i.b.u9 r0 = new b.i.a.f.i.b.u9     // Catch: android.database.sqlite.SQLiteException -> L6e java.lang.Throwable -> L9d
            r1 = r0
            r2 = r19
            r4 = r20
            r1.<init>(r2, r3, r4, r5, r7)     // Catch: android.database.sqlite.SQLiteException -> L6e java.lang.Throwable -> L9d
            boolean r1 = r10.moveToNext()     // Catch: android.database.sqlite.SQLiteException -> L6e java.lang.Throwable -> L9d
            if (r1 == 0) goto L6a
            b.i.a.f.i.b.q3 r1 = r18.g()     // Catch: android.database.sqlite.SQLiteException -> L6e java.lang.Throwable -> L9d
            b.i.a.f.i.b.s3 r1 = r1.f     // Catch: android.database.sqlite.SQLiteException -> L6e java.lang.Throwable -> L9d
            java.lang.String r2 = "Got multiple records for user property, expected one. appId"
            java.lang.Object r3 = b.i.a.f.i.b.q3.s(r19)     // Catch: android.database.sqlite.SQLiteException -> L6e java.lang.Throwable -> L9d
            r1.b(r2, r3)     // Catch: android.database.sqlite.SQLiteException -> L6e java.lang.Throwable -> L9d
        L6a:
            r10.close()
            return r0
        L6e:
            r0 = move-exception
            goto L80
        L70:
            r0 = move-exception
            r11 = r18
            goto L9e
        L74:
            r0 = move-exception
            r11 = r18
            goto L80
        L78:
            r0 = move-exception
            r11 = r18
            goto L9f
        L7c:
            r0 = move-exception
            r11 = r18
            r10 = r9
        L80:
            b.i.a.f.i.b.q3 r1 = r18.g()     // Catch: java.lang.Throwable -> L9d
            b.i.a.f.i.b.s3 r1 = r1.f     // Catch: java.lang.Throwable -> L9d
            java.lang.String r2 = "Error querying user property. appId"
            java.lang.Object r3 = b.i.a.f.i.b.q3.s(r19)     // Catch: java.lang.Throwable -> L9d
            b.i.a.f.i.b.o3 r4 = r18.d()     // Catch: java.lang.Throwable -> L9d
            java.lang.String r4 = r4.y(r8)     // Catch: java.lang.Throwable -> L9d
            r1.d(r2, r3, r4, r0)     // Catch: java.lang.Throwable -> L9d
            if (r10 == 0) goto L9c
            r10.close()
        L9c:
            return r9
        L9d:
            r0 = move-exception
        L9e:
            r9 = r10
        L9f:
            if (r9 == 0) goto La4
            r9.close()
        La4:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.g.Y(java.lang.String, java.lang.String):b.i.a.f.i.b.u9");
    }

    /* JADX WARN: Removed duplicated region for block: B:35:0x0119  */
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final com.google.android.gms.measurement.internal.zzz Z(java.lang.String r30, java.lang.String r31) {
        /*
            Method dump skipped, instructions count: 285
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.g.Z(java.lang.String, java.lang.String):com.google.android.gms.measurement.internal.zzz");
    }

    @WorkerThread
    public final int a0(String str, String str2) {
        d.w(str);
        d.w(str2);
        b();
        n();
        try {
            return t().delete("conditional_properties", "app_id=? and name=?", new String[]{str, str2});
        } catch (SQLiteException e2) {
            g().f.d("Error deleting conditional property", q3.s(str), d().y(str2), e2);
            return 0;
        }
    }

    @WorkerThread
    public final void b0() {
        n();
        t().beginTransaction();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:39:0x00cb  */
    /* JADX WARN: Type inference failed for: r9v0 */
    /* JADX WARN: Type inference failed for: r9v1, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r9v2 */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.util.Map<java.lang.Integer, java.util.List<b.i.a.f.h.l.l0>> c0(java.lang.String r13, java.lang.String r14) {
        /*
            r12 = this;
            r12.n()
            r12.b()
            b.c.a.a0.d.w(r13)
            b.c.a.a0.d.w(r14)
            androidx.collection.ArrayMap r0 = new androidx.collection.ArrayMap
            r0.<init>()
            android.database.sqlite.SQLiteDatabase r1 = r12.t()
            r9 = 0
            java.lang.String r2 = "event_filters"
            java.lang.String r3 = "audience_id"
            java.lang.String r4 = "data"
            java.lang.String[] r3 = new java.lang.String[]{r3, r4}     // Catch: java.lang.Throwable -> L92 android.database.sqlite.SQLiteException -> L94
            java.lang.String r4 = "app_id=? AND event_name=?"
            r5 = 2
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch: java.lang.Throwable -> L92 android.database.sqlite.SQLiteException -> L94
            r10 = 0
            r5[r10] = r13     // Catch: java.lang.Throwable -> L92 android.database.sqlite.SQLiteException -> L94
            r11 = 1
            r5[r11] = r14     // Catch: java.lang.Throwable -> L92 android.database.sqlite.SQLiteException -> L94
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r14 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch: java.lang.Throwable -> L92 android.database.sqlite.SQLiteException -> L94
            boolean r1 = r14.moveToFirst()     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            if (r1 != 0) goto L40
            java.util.Map r13 = java.util.Collections.emptyMap()     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            r14.close()
            return r13
        L40:
            byte[] r1 = r14.getBlob(r11)     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            b.i.a.f.h.l.l0$a r2 = b.i.a.f.h.l.l0.I()     // Catch: java.io.IOException -> L76 android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            b.i.a.f.h.l.b6 r1 = b.i.a.f.i.b.q9.x(r2, r1)     // Catch: java.io.IOException -> L76 android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            b.i.a.f.h.l.l0$a r1 = (b.i.a.f.h.l.l0.a) r1     // Catch: java.io.IOException -> L76 android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            b.i.a.f.h.l.c6 r1 = r1.p()     // Catch: java.io.IOException -> L76 android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            b.i.a.f.h.l.u4 r1 = (b.i.a.f.h.l.u4) r1     // Catch: java.io.IOException -> L76 android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            b.i.a.f.h.l.l0 r1 = (b.i.a.f.h.l.l0) r1     // Catch: java.io.IOException -> L76 android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            int r2 = r14.getInt(r10)     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            java.lang.Object r3 = r0.get(r3)     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            java.util.List r3 = (java.util.List) r3     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            if (r3 != 0) goto L72
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            r3.<init>()     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            r0.put(r2, r3)     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
        L72:
            r3.add(r1)     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            goto L86
        L76:
            r1 = move-exception
            b.i.a.f.i.b.q3 r2 = r12.g()     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            b.i.a.f.i.b.s3 r2 = r2.f     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            java.lang.String r3 = "Failed to merge filter. appId"
            java.lang.Object r4 = b.i.a.f.i.b.q3.s(r13)     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            r2.c(r3, r4, r1)     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
        L86:
            boolean r1 = r14.moveToNext()     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            if (r1 != 0) goto L40
            r14.close()
            return r0
        L90:
            r0 = move-exception
            goto L96
        L92:
            r13 = move-exception
            goto Lc9
        L94:
            r0 = move-exception
            r14 = r9
        L96:
            b.i.a.f.i.b.q3 r1 = r12.g()     // Catch: java.lang.Throwable -> Lc7
            b.i.a.f.i.b.s3 r1 = r1.f     // Catch: java.lang.Throwable -> Lc7
            java.lang.String r2 = "Database error querying filters. appId"
            java.lang.Object r3 = b.i.a.f.i.b.q3.s(r13)     // Catch: java.lang.Throwable -> Lc7
            r1.c(r2, r3, r0)     // Catch: java.lang.Throwable -> Lc7
            boolean r0 = b.i.a.f.h.l.l9.b()     // Catch: java.lang.Throwable -> Lc7
            if (r0 == 0) goto Lc1
            b.i.a.f.i.b.u4 r0 = r12.a     // Catch: java.lang.Throwable -> Lc7
            b.i.a.f.i.b.c r0 = r0.h     // Catch: java.lang.Throwable -> Lc7
            b.i.a.f.i.b.j3<java.lang.Boolean> r1 = b.i.a.f.i.b.p.F0     // Catch: java.lang.Throwable -> Lc7
            boolean r13 = r0.u(r13, r1)     // Catch: java.lang.Throwable -> Lc7
            if (r13 == 0) goto Lc1
            java.util.Map r13 = java.util.Collections.emptyMap()     // Catch: java.lang.Throwable -> Lc7
            if (r14 == 0) goto Lc0
            r14.close()
        Lc0:
            return r13
        Lc1:
            if (r14 == 0) goto Lc6
            r14.close()
        Lc6:
            return r9
        Lc7:
            r13 = move-exception
            r9 = r14
        Lc9:
            if (r9 == 0) goto Lce
            r9.close()
        Lce:
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.g.c0(java.lang.String, java.lang.String):java.util.Map");
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:39:0x00cb  */
    /* JADX WARN: Type inference failed for: r9v0 */
    /* JADX WARN: Type inference failed for: r9v1, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r9v2 */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.util.Map<java.lang.Integer, java.util.List<b.i.a.f.h.l.o0>> d0(java.lang.String r13, java.lang.String r14) {
        /*
            r12 = this;
            r12.n()
            r12.b()
            b.c.a.a0.d.w(r13)
            b.c.a.a0.d.w(r14)
            androidx.collection.ArrayMap r0 = new androidx.collection.ArrayMap
            r0.<init>()
            android.database.sqlite.SQLiteDatabase r1 = r12.t()
            r9 = 0
            java.lang.String r2 = "property_filters"
            java.lang.String r3 = "audience_id"
            java.lang.String r4 = "data"
            java.lang.String[] r3 = new java.lang.String[]{r3, r4}     // Catch: java.lang.Throwable -> L92 android.database.sqlite.SQLiteException -> L94
            java.lang.String r4 = "app_id=? AND property_name=?"
            r5 = 2
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch: java.lang.Throwable -> L92 android.database.sqlite.SQLiteException -> L94
            r10 = 0
            r5[r10] = r13     // Catch: java.lang.Throwable -> L92 android.database.sqlite.SQLiteException -> L94
            r11 = 1
            r5[r11] = r14     // Catch: java.lang.Throwable -> L92 android.database.sqlite.SQLiteException -> L94
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r14 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch: java.lang.Throwable -> L92 android.database.sqlite.SQLiteException -> L94
            boolean r1 = r14.moveToFirst()     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            if (r1 != 0) goto L40
            java.util.Map r13 = java.util.Collections.emptyMap()     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            r14.close()
            return r13
        L40:
            byte[] r1 = r14.getBlob(r11)     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            b.i.a.f.h.l.o0$a r2 = b.i.a.f.h.l.o0.D()     // Catch: java.io.IOException -> L76 android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            b.i.a.f.h.l.b6 r1 = b.i.a.f.i.b.q9.x(r2, r1)     // Catch: java.io.IOException -> L76 android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            b.i.a.f.h.l.o0$a r1 = (b.i.a.f.h.l.o0.a) r1     // Catch: java.io.IOException -> L76 android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            b.i.a.f.h.l.c6 r1 = r1.p()     // Catch: java.io.IOException -> L76 android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            b.i.a.f.h.l.u4 r1 = (b.i.a.f.h.l.u4) r1     // Catch: java.io.IOException -> L76 android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            b.i.a.f.h.l.o0 r1 = (b.i.a.f.h.l.o0) r1     // Catch: java.io.IOException -> L76 android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            int r2 = r14.getInt(r10)     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            java.lang.Object r3 = r0.get(r3)     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            java.util.List r3 = (java.util.List) r3     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            if (r3 != 0) goto L72
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            r3.<init>()     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            r0.put(r2, r3)     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
        L72:
            r3.add(r1)     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            goto L86
        L76:
            r1 = move-exception
            b.i.a.f.i.b.q3 r2 = r12.g()     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            b.i.a.f.i.b.s3 r2 = r2.f     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            java.lang.String r3 = "Failed to merge filter"
            java.lang.Object r4 = b.i.a.f.i.b.q3.s(r13)     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            r2.c(r3, r4, r1)     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
        L86:
            boolean r1 = r14.moveToNext()     // Catch: android.database.sqlite.SQLiteException -> L90 java.lang.Throwable -> Lc7
            if (r1 != 0) goto L40
            r14.close()
            return r0
        L90:
            r0 = move-exception
            goto L96
        L92:
            r13 = move-exception
            goto Lc9
        L94:
            r0 = move-exception
            r14 = r9
        L96:
            b.i.a.f.i.b.q3 r1 = r12.g()     // Catch: java.lang.Throwable -> Lc7
            b.i.a.f.i.b.s3 r1 = r1.f     // Catch: java.lang.Throwable -> Lc7
            java.lang.String r2 = "Database error querying filters. appId"
            java.lang.Object r3 = b.i.a.f.i.b.q3.s(r13)     // Catch: java.lang.Throwable -> Lc7
            r1.c(r2, r3, r0)     // Catch: java.lang.Throwable -> Lc7
            boolean r0 = b.i.a.f.h.l.l9.b()     // Catch: java.lang.Throwable -> Lc7
            if (r0 == 0) goto Lc1
            b.i.a.f.i.b.u4 r0 = r12.a     // Catch: java.lang.Throwable -> Lc7
            b.i.a.f.i.b.c r0 = r0.h     // Catch: java.lang.Throwable -> Lc7
            b.i.a.f.i.b.j3<java.lang.Boolean> r1 = b.i.a.f.i.b.p.F0     // Catch: java.lang.Throwable -> Lc7
            boolean r13 = r0.u(r13, r1)     // Catch: java.lang.Throwable -> Lc7
            if (r13 == 0) goto Lc1
            java.util.Map r13 = java.util.Collections.emptyMap()     // Catch: java.lang.Throwable -> Lc7
            if (r14 == 0) goto Lc0
            r14.close()
        Lc0:
            return r13
        Lc1:
            if (r14 == 0) goto Lc6
            r14.close()
        Lc6:
            return r9
        Lc7:
            r13 = move-exception
            r9 = r14
        Lc9:
            if (r9 == 0) goto Lce
            r9.close()
        Lce:
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.g.d0(java.lang.String, java.lang.String):java.util.Map");
    }

    @WorkerThread
    public final void e0() {
        n();
        t().endTransaction();
    }

    public final long f0(String str) {
        d.w(str);
        return w("select count(1) from events where app_id=? and name not like '!_%' escape '!'", new String[]{str}, 0L);
    }

    @WorkerThread
    public final long g0(String str, String str2) {
        Throwable th;
        SQLiteException e2;
        long w;
        d.w(str);
        d.w(str2);
        b();
        n();
        SQLiteDatabase t = t();
        t.beginTransaction();
        long j2 = 0;
        try {
            StringBuilder sb = new StringBuilder(str2.length() + 32);
            sb.append("select ");
            sb.append(str2);
            sb.append(" from app2 where app_id=?");
            try {
                try {
                    w = w(sb.toString(), new String[]{str}, -1L);
                    if (w == -1) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("app_id", str);
                        contentValues.put("first_open_count", (Integer) 0);
                        contentValues.put("previous_install_count", (Integer) 0);
                        if (t.insertWithOnConflict("app2", null, contentValues, 5) == -1) {
                            g().f.c("Failed to insert column (got -1). appId", q3.s(str), str2);
                            t.endTransaction();
                            return -1L;
                        }
                        w = 0;
                    }
                } catch (SQLiteException e3) {
                    e2 = e3;
                }
                try {
                    ContentValues contentValues2 = new ContentValues();
                    contentValues2.put("app_id", str);
                    contentValues2.put(str2, Long.valueOf(1 + w));
                    if (t.update("app2", contentValues2, "app_id = ?", new String[]{str}) == 0) {
                        g().f.c("Failed to update column (got 0). appId", q3.s(str), str2);
                        t.endTransaction();
                        return -1L;
                    }
                    t.setTransactionSuccessful();
                    t.endTransaction();
                    return w;
                } catch (SQLiteException e4) {
                    e2 = e4;
                    j2 = w;
                    g().f.d("Error inserting column. appId", q3.s(str), str2, e2);
                    t.endTransaction();
                    return j2;
                }
            } catch (Throwable th2) {
                th = th2;
                t.endTransaction();
                throw th;
            }
        } catch (SQLiteException e5) {
            e2 = e5;
        } catch (Throwable th3) {
            th = th3;
        }
    }

    /* JADX WARN: Not initialized variable reg: 1, insn: 0x00ca: MOVE  (r0 I:??[OBJECT, ARRAY]) = (r1 I:??[OBJECT, ARRAY]), block:B:41:0x00ca */
    /* JADX WARN: Removed duplicated region for block: B:43:0x00cd  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final android.os.Bundle h0(java.lang.String r8) {
        /*
            r7 = this;
            r7.b()
            r7.n()
            r0 = 0
            android.database.sqlite.SQLiteDatabase r1 = r7.t()     // Catch: java.lang.Throwable -> Lb4 android.database.sqlite.SQLiteException -> Lb6
            java.lang.String r2 = "select parameters from default_event_params where app_id=?"
            r3 = 1
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch: java.lang.Throwable -> Lb4 android.database.sqlite.SQLiteException -> Lb6
            r4 = 0
            r3[r4] = r8     // Catch: java.lang.Throwable -> Lb4 android.database.sqlite.SQLiteException -> Lb6
            android.database.Cursor r1 = r1.rawQuery(r2, r3)     // Catch: java.lang.Throwable -> Lb4 android.database.sqlite.SQLiteException -> Lb6
            boolean r2 = r1.moveToFirst()     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            if (r2 != 0) goto L2c
            b.i.a.f.i.b.q3 r8 = r7.g()     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            b.i.a.f.i.b.s3 r8 = r8.n     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            java.lang.String r2 = "Default event parameters not found"
            r8.a(r2)     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            r1.close()
            return r0
        L2c:
            byte[] r2 = r1.getBlob(r4)     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            b.i.a.f.h.l.a1$a r3 = b.i.a.f.h.l.a1.M()     // Catch: java.io.IOException -> L9e android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            b.i.a.f.h.l.b6 r2 = b.i.a.f.i.b.q9.x(r3, r2)     // Catch: java.io.IOException -> L9e android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            b.i.a.f.h.l.a1$a r2 = (b.i.a.f.h.l.a1.a) r2     // Catch: java.io.IOException -> L9e android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            b.i.a.f.h.l.c6 r2 = r2.p()     // Catch: java.io.IOException -> L9e android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            b.i.a.f.h.l.u4 r2 = (b.i.a.f.h.l.u4) r2     // Catch: java.io.IOException -> L9e android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            b.i.a.f.h.l.a1 r2 = (b.i.a.f.h.l.a1) r2     // Catch: java.io.IOException -> L9e android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            r7.m()     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            java.util.List r8 = r2.v()     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            android.os.Bundle r2 = new android.os.Bundle     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            r2.<init>()     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            java.util.Iterator r8 = r8.iterator()     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
        L52:
            boolean r3 = r8.hasNext()     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            if (r3 == 0) goto L9a
            java.lang.Object r3 = r8.next()     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            b.i.a.f.h.l.c1 r3 = (b.i.a.f.h.l.c1) r3     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            java.lang.String r4 = r3.B()     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            boolean r5 = r3.M()     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            if (r5 == 0) goto L70
            double r5 = r3.N()     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            r2.putDouble(r4, r5)     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            goto L52
        L70:
            boolean r5 = r3.K()     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            if (r5 == 0) goto L7e
            float r3 = r3.L()     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            r2.putFloat(r4, r3)     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            goto L52
        L7e:
            boolean r5 = r3.F()     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            if (r5 == 0) goto L8c
            java.lang.String r3 = r3.G()     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            r2.putString(r4, r3)     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            goto L52
        L8c:
            boolean r5 = r3.I()     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            if (r5 == 0) goto L52
            long r5 = r3.J()     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            r2.putLong(r4, r5)     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            goto L52
        L9a:
            r1.close()
            return r2
        L9e:
            r2 = move-exception
            b.i.a.f.i.b.q3 r3 = r7.g()     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            b.i.a.f.i.b.s3 r3 = r3.f     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            java.lang.String r4 = "Failed to retrieve default event parameters. appId"
            java.lang.Object r8 = b.i.a.f.i.b.q3.s(r8)     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            r3.c(r4, r8, r2)     // Catch: android.database.sqlite.SQLiteException -> Lb2 java.lang.Throwable -> Lc9
            r1.close()
            return r0
        Lb2:
            r8 = move-exception
            goto Lb8
        Lb4:
            r8 = move-exception
            goto Lcb
        Lb6:
            r8 = move-exception
            r1 = r0
        Lb8:
            b.i.a.f.i.b.q3 r2 = r7.g()     // Catch: java.lang.Throwable -> Lc9
            b.i.a.f.i.b.s3 r2 = r2.f     // Catch: java.lang.Throwable -> Lc9
            java.lang.String r3 = "Error selecting default event parameters"
            r2.b(r3, r8)     // Catch: java.lang.Throwable -> Lc9
            if (r1 == 0) goto Lc8
            r1.close()
        Lc8:
            return r0
        Lc9:
            r8 = move-exception
            r0 = r1
        Lcb:
            if (r0 == 0) goto Ld0
            r0.close()
        Ld0:
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.g.h0(java.lang.String):android.os.Bundle");
    }

    @WorkerThread
    public final void i0() {
        b();
        n();
        if (R()) {
            long a = l().i.a();
            Objects.requireNonNull((c) this.a.o);
            long elapsedRealtime = SystemClock.elapsedRealtime();
            if (Math.abs(elapsedRealtime - a) > p.f1560y.a(null).longValue()) {
                l().i.b(elapsedRealtime);
                b();
                n();
                if (R()) {
                    SQLiteDatabase t = t();
                    Objects.requireNonNull((c) this.a.o);
                    int delete = t.delete("queue", "abs(bundle_end_timestamp - ?) > cast(? as integer)", new String[]{String.valueOf(System.currentTimeMillis()), String.valueOf(c.B())});
                    if (delete > 0) {
                        g().n.b("Deleted stale rows. rowsDeleted", Integer.valueOf(delete));
                    }
                }
            }
        }
    }

    public final long j0() {
        Cursor cursor = null;
        try {
            try {
                cursor = t().rawQuery("select rowid from raw_events order by rowid desc limit 1;", null);
                if (!cursor.moveToFirst()) {
                    cursor.close();
                    return -1L;
                }
                long j2 = cursor.getLong(0);
                cursor.close();
                return j2;
            } catch (SQLiteException e2) {
                g().f.b("Error querying raw events", e2);
                if (cursor != null) {
                    cursor.close();
                }
                return -1L;
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @Override // b.i.a.f.i.b.i9
    public final boolean p() {
        return false;
    }

    @WorkerThread
    public final void s() {
        n();
        t().setTransactionSuccessful();
    }

    @WorkerThread
    public final SQLiteDatabase t() {
        b();
        try {
            return this.l.getWritableDatabase();
        } catch (SQLiteException e2) {
            g().i.b("Error opening database", e2);
            throw e2;
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:22:0x003b  */
    /* JADX WARN: Type inference failed for: r0v0, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARN: Type inference failed for: r0v1, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r0v5 */
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.lang.String u() {
        /*
            r6 = this;
            android.database.sqlite.SQLiteDatabase r0 = r6.t()
            r1 = 0
            java.lang.String r2 = "select app_id from queue order by has_realtime desc, rowid asc limit 1;"
            android.database.Cursor r0 = r0.rawQuery(r2, r1)     // Catch: java.lang.Throwable -> L20 android.database.sqlite.SQLiteException -> L25
            boolean r2 = r0.moveToFirst()     // Catch: android.database.sqlite.SQLiteException -> L1e java.lang.Throwable -> L38
            if (r2 == 0) goto L1a
            r2 = 0
            java.lang.String r1 = r0.getString(r2)     // Catch: android.database.sqlite.SQLiteException -> L1e java.lang.Throwable -> L38
            r0.close()
            return r1
        L1a:
            r0.close()
            return r1
        L1e:
            r2 = move-exception
            goto L27
        L20:
            r0 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L39
        L25:
            r2 = move-exception
            r0 = r1
        L27:
            b.i.a.f.i.b.q3 r3 = r6.g()     // Catch: java.lang.Throwable -> L38
            b.i.a.f.i.b.s3 r3 = r3.f     // Catch: java.lang.Throwable -> L38
            java.lang.String r4 = "Database error getting next bundle app id"
            r3.b(r4, r2)     // Catch: java.lang.Throwable -> L38
            if (r0 == 0) goto L37
            r0.close()
        L37:
            return r1
        L38:
            r1 = move-exception
        L39:
            if (r0 == 0) goto L3e
            r0.close()
        L3e:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.g.u():java.lang.String");
    }

    public final long v(e1 e1Var) throws IOException {
        b();
        n();
        d.w(e1Var.G1());
        byte[] d2 = e1Var.d();
        long t = m().t(d2);
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", e1Var.G1());
        contentValues.put("metadata_fingerprint", Long.valueOf(t));
        contentValues.put("metadata", d2);
        try {
            t().insertWithOnConflict("raw_events_metadata", null, contentValues, 4);
            return t;
        } catch (SQLiteException e2) {
            g().f.c("Error storing raw event metadata. appId", q3.s(e1Var.G1()), e2);
            throw e2;
        }
    }

    @WorkerThread
    public final long w(String str, String[] strArr, long j2) {
        Cursor cursor = null;
        try {
            try {
                cursor = t().rawQuery(str, strArr);
                if (cursor.moveToFirst()) {
                    long j3 = cursor.getLong(0);
                    cursor.close();
                    return j3;
                }
                cursor.close();
                return j2;
            } catch (SQLiteException e2) {
                g().f.c("Database error", str, e2);
                throw e2;
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @WorkerThread
    public final f x(long j2, String str, long j3, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
        d.w(str);
        b();
        n();
        String[] strArr = {str};
        f fVar = new f();
        Cursor cursor = null;
        try {
            try {
                SQLiteDatabase t = t();
                Cursor query = t.query("apps", new String[]{"day", "daily_events_count", "daily_public_events_count", "daily_conversions_count", "daily_error_events_count", "daily_realtime_events_count"}, "app_id=?", new String[]{str}, null, null, null);
                if (!query.moveToFirst()) {
                    g().i.b("Not updating daily counts, app is not known. appId", q3.s(str));
                    query.close();
                    return fVar;
                }
                if (query.getLong(0) == j2) {
                    fVar.f1528b = query.getLong(1);
                    fVar.a = query.getLong(2);
                    fVar.c = query.getLong(3);
                    fVar.d = query.getLong(4);
                    fVar.e = query.getLong(5);
                }
                if (z2) {
                    fVar.f1528b += j3;
                }
                if (z3) {
                    fVar.a += j3;
                }
                if (z4) {
                    fVar.c += j3;
                }
                if (z5) {
                    fVar.d += j3;
                }
                if (z6) {
                    fVar.e += j3;
                }
                ContentValues contentValues = new ContentValues();
                contentValues.put("day", Long.valueOf(j2));
                contentValues.put("daily_public_events_count", Long.valueOf(fVar.a));
                contentValues.put("daily_events_count", Long.valueOf(fVar.f1528b));
                contentValues.put("daily_conversions_count", Long.valueOf(fVar.c));
                contentValues.put("daily_error_events_count", Long.valueOf(fVar.d));
                contentValues.put("daily_realtime_events_count", Long.valueOf(fVar.e));
                t.update("apps", contentValues, "app_id=?", strArr);
                query.close();
                return fVar;
            } catch (SQLiteException e2) {
                g().f.c("Error updating daily counts. appId", q3.s(str), e2);
                if (0 != 0) {
                    cursor.close();
                }
                return fVar;
            }
        } catch (Throwable th) {
            if (0 != 0) {
                cursor.close();
            }
            throw th;
        }
    }

    @WorkerThread
    public final f y(long j2, String str, boolean z2, boolean z3) {
        return x(j2, str, 1L, false, false, z2, false, z3);
    }

    /* JADX WARN: Not initialized variable reg: 14, insn: 0x0143: MOVE  (r18 I:??[OBJECT, ARRAY]) = (r14 I:??[OBJECT, ARRAY]), block:B:61:0x0143 */
    /* JADX WARN: Removed duplicated region for block: B:58:0x013e  */
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final b.i.a.f.i.b.l z(java.lang.String r26, java.lang.String r27) {
        /*
            Method dump skipped, instructions count: 331
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.i.b.g.z(java.lang.String, java.lang.String):b.i.a.f.i.b.l");
    }
}
