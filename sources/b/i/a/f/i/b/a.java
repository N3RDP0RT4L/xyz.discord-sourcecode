package b.i.a.f.i.b;

import android.os.Bundle;
import androidx.annotation.WorkerThread;
import androidx.collection.ArrayMap;
import java.util.Map;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class a extends z1 {
    public long d;
    public final Map<String, Integer> c = new ArrayMap();

    /* renamed from: b  reason: collision with root package name */
    public final Map<String, Long> f1508b = new ArrayMap();

    public a(u4 u4Var) {
        super(u4Var);
    }

    @WorkerThread
    public final void t(long j) {
        i7 w = q().w(false);
        for (String str : this.f1508b.keySet()) {
            w(str, j - this.f1508b.get(str).longValue(), w);
        }
        if (!this.f1508b.isEmpty()) {
            u(j - this.d, w);
        }
        x(j);
    }

    @WorkerThread
    public final void u(long j, i7 i7Var) {
        if (i7Var == null) {
            g().n.a("Not logging ad exposure. No active activity");
        } else if (j < 1000) {
            g().n.b("Not logging ad exposure. Less than 1000 ms. exposure", Long.valueOf(j));
        } else {
            Bundle bundle = new Bundle();
            bundle.putLong("_xt", j);
            h7.A(i7Var, bundle, true);
            n().H("am", "_xa", bundle);
        }
    }

    public final void v(String str, long j) {
        if (str == null || str.length() == 0) {
            g().f.a("Ad unit id must be a non-empty string");
        } else {
            f().v(new y0(this, str, j));
        }
    }

    @WorkerThread
    public final void w(String str, long j, i7 i7Var) {
        if (i7Var == null) {
            g().n.a("Not logging ad unit exposure. No active activity");
        } else if (j < 1000) {
            g().n.b("Not logging ad unit exposure. Less than 1000 ms. exposure", Long.valueOf(j));
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("_ai", str);
            bundle.putLong("_xt", j);
            h7.A(i7Var, bundle, true);
            n().H("am", "_xu", bundle);
        }
    }

    @WorkerThread
    public final void x(long j) {
        for (String str : this.f1508b.keySet()) {
            this.f1508b.put(str, Long.valueOf(j));
        }
        if (!this.f1508b.isEmpty()) {
            this.d = j;
        }
    }

    public final void y(String str, long j) {
        if (str == null || str.length() == 0) {
            g().f.a("Ad unit id must be a non-empty string");
        } else {
            f().v(new x(this, str, j));
        }
    }
}
