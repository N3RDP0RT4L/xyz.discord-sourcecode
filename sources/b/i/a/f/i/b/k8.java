package b.i.a.f.i.b;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.f.e.k.b;
import b.i.a.f.e.n.a;
import com.google.android.gms.common.ConnectionResult;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class k8 implements ServiceConnection, b.a, b.AbstractC0113b {
    public volatile boolean j;
    public volatile r3 k;
    public final /* synthetic */ q7 l;

    public k8(q7 q7Var) {
        this.l = q7Var;
    }

    @Override // b.i.a.f.e.k.b.a
    @MainThread
    public final void c(int i) {
        d.u("MeasurementServiceConnection.onConnectionSuspended");
        this.l.g().m.a("Service connection suspended");
        this.l.f().v(new o8(this));
    }

    @Override // b.i.a.f.e.k.b.AbstractC0113b
    @MainThread
    public final void g(@NonNull ConnectionResult connectionResult) {
        d.u("MeasurementServiceConnection.onConnectionFailed");
        u4 u4Var = this.l.a;
        q3 q3Var = u4Var.j;
        q3 q3Var2 = (q3Var == null || !q3Var.n()) ? null : u4Var.j;
        if (q3Var2 != null) {
            q3Var2.i.b("Service connection failed", connectionResult);
        }
        synchronized (this) {
            this.j = false;
            this.k = null;
        }
        this.l.f().v(new n8(this));
    }

    @Override // b.i.a.f.e.k.b.a
    @MainThread
    public final void i(@Nullable Bundle bundle) {
        d.u("MeasurementServiceConnection.onConnected");
        synchronized (this) {
            try {
                this.l.f().v(new l8(this, this.k.w()));
            } catch (DeadObjectException | IllegalStateException unused) {
                this.k = null;
                this.j = false;
            }
        }
    }

    @Override // android.content.ServiceConnection
    @MainThread
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        i3 i3Var;
        d.u("MeasurementServiceConnection.onServiceConnected");
        synchronized (this) {
            if (iBinder == null) {
                this.j = false;
                this.l.g().f.a("Service connected with null binder");
                return;
            }
            i3 i3Var2 = null;
            try {
                String interfaceDescriptor = iBinder.getInterfaceDescriptor();
                if ("com.google.android.gms.measurement.internal.IMeasurementService".equals(interfaceDescriptor)) {
                    IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.internal.IMeasurementService");
                    if (queryLocalInterface instanceof i3) {
                        i3Var = (i3) queryLocalInterface;
                    } else {
                        i3Var = new k3(iBinder);
                    }
                    i3Var2 = i3Var;
                    this.l.g().n.a("Bound to IMeasurementService interface");
                } else {
                    this.l.g().f.b("Got binder with a wrong descriptor", interfaceDescriptor);
                }
            } catch (RemoteException unused) {
                this.l.g().f.a("Service connect failed to get IMeasurementService");
            }
            if (i3Var2 == null) {
                this.j = false;
                try {
                    a b2 = a.b();
                    q7 q7Var = this.l;
                    b2.c(q7Var.a.f1566b, q7Var.c);
                } catch (IllegalArgumentException unused2) {
                }
            } else {
                this.l.f().v(new j8(this, i3Var2));
            }
        }
    }

    @Override // android.content.ServiceConnection
    @MainThread
    public final void onServiceDisconnected(ComponentName componentName) {
        d.u("MeasurementServiceConnection.onServiceDisconnected");
        this.l.g().m.a("Service disconnected");
        this.l.f().v(new m8(this, componentName));
    }
}
