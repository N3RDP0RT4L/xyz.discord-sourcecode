package b.i.a.f.i.b;

import b.i.a.f.h.l.fc;
import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService;
/* compiled from: com.google.android.gms:play-services-measurement-sdk@@18.0.0 */
/* loaded from: classes3.dex */
public final class a6 implements Runnable {
    public final /* synthetic */ fc j;
    public final /* synthetic */ AppMeasurementDynamiteService k;

    public a6(AppMeasurementDynamiteService appMeasurementDynamiteService, fc fcVar) {
        this.k = appMeasurementDynamiteService;
        this.j = fcVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        q7 x2 = this.k.a.x();
        fc fcVar = this.j;
        x2.b();
        x2.t();
        x2.z(new u7(x2, x2.I(false), fcVar));
    }
}
