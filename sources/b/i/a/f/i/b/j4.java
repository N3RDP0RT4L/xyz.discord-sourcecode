package b.i.a.f.i.b;

import android.content.SharedPreferences;
import androidx.annotation.WorkerThread;
import b.c.a.a0.d;
/* compiled from: com.google.android.gms:play-services-measurement-impl@@18.0.0 */
/* loaded from: classes3.dex */
public final class j4 {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public boolean f1536b;
    public String c;
    public final /* synthetic */ d4 d;

    public j4(d4 d4Var, String str) {
        this.d = d4Var;
        d.w(str);
        this.a = str;
    }

    @WorkerThread
    public final String a() {
        if (!this.f1536b) {
            this.f1536b = true;
            this.c = this.d.w().getString(this.a, null);
        }
        return this.c;
    }

    @WorkerThread
    public final void b(String str) {
        SharedPreferences.Editor edit = this.d.w().edit();
        edit.putString(this.a, str);
        edit.apply();
        this.c = str;
    }
}
