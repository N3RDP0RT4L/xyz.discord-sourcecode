package b.i.a.f.i.b;

import java.util.List;
import java.util.Map;
/* compiled from: com.google.android.gms:play-services-measurement@@18.0.0 */
/* loaded from: classes3.dex */
public final class l9 implements z3 {
    public final /* synthetic */ k9 a;

    public l9(k9 k9Var) {
        this.a = k9Var;
    }

    @Override // b.i.a.f.i.b.z3
    public final void a(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        this.a.r(str, i, th, bArr, map);
    }
}
