package b.i.a.f.i.b;

import androidx.annotation.WorkerThread;
import java.util.List;
import java.util.Map;
/* compiled from: com.google.android.gms:play-services-measurement@@18.0.0 */
@WorkerThread
/* loaded from: classes3.dex */
public interface z3 {
    void a(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map);
}
