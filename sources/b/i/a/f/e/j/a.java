package b.i.a.f.e.j;

import android.database.CursorWindow;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.common.data.DataHolder;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public final class a implements Parcelable.Creator<DataHolder> {
    @Override // android.os.Parcelable.Creator
    public final DataHolder createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        String[] strArr = null;
        int i = 0;
        CursorWindow[] cursorWindowArr = null;
        Bundle bundle = null;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                strArr = d.S(parcel, readInt);
            } else if (c == 2) {
                cursorWindowArr = (CursorWindow[]) d.U(parcel, readInt, CursorWindow.CREATOR);
            } else if (c == 3) {
                i3 = d.G1(parcel, readInt);
            } else if (c == 4) {
                bundle = d.M(parcel, readInt);
            } else if (c != 1000) {
                d.d2(parcel, readInt);
            } else {
                i2 = d.G1(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        DataHolder dataHolder = new DataHolder(i2, strArr, cursorWindowArr, i3, bundle);
        dataHolder.l = new Bundle();
        int i4 = 0;
        while (true) {
            String[] strArr2 = dataHolder.k;
            if (i4 >= strArr2.length) {
                break;
            }
            dataHolder.l.putInt(strArr2[i4], i4);
            i4++;
        }
        dataHolder.p = new int[dataHolder.m.length];
        int i5 = 0;
        while (true) {
            CursorWindow[] cursorWindowArr2 = dataHolder.m;
            if (i >= cursorWindowArr2.length) {
                return dataHolder;
            }
            dataHolder.p[i] = i5;
            i5 += dataHolder.m[i].getNumRows() - (i5 - cursorWindowArr2[i].getStartPosition());
            i++;
        }
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ DataHolder[] newArray(int i) {
        return new DataHolder[i];
    }
}
