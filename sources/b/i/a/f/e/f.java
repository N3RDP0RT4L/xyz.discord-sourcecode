package b.i.a.f.e;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.RemoteException;
import android.os.StrictMode;
import android.util.Log;
import androidx.annotation.RecentlyNonNull;
import b.i.a.f.f.b;
import com.google.android.gms.common.zzn;
import com.google.android.gms.common.zzq;
import com.google.android.gms.dynamite.DynamiteModule;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public class f {
    public static f a;

    /* renamed from: b  reason: collision with root package name */
    public final Context f1344b;
    public volatile String c;

    public f(@RecentlyNonNull Context context) {
        this.f1344b = context.getApplicationContext();
    }

    @RecentlyNonNull
    public static f a(@RecentlyNonNull Context context) {
        Objects.requireNonNull(context, "null reference");
        synchronized (f.class) {
            if (a == null) {
                z zVar = b0.a;
                synchronized (b0.class) {
                    if (b0.e == null) {
                        b0.e = context.getApplicationContext();
                    } else {
                        Log.w("GoogleCertificates", "GoogleCertificates has been initialized already");
                    }
                }
                a = new f(context);
            }
        }
        return a;
    }

    public static final x c(PackageInfo packageInfo, x... xVarArr) {
        Signature[] signatureArr = packageInfo.signatures;
        if (signatureArr == null) {
            return null;
        }
        if (signatureArr.length != 1) {
            Log.w("GoogleSignatureVerifier", "Package has more than one signature.");
            return null;
        }
        y yVar = new y(packageInfo.signatures[0].toByteArray());
        for (int i = 0; i < xVarArr.length; i++) {
            if (xVarArr[i].equals(yVar)) {
                return xVarArr[i];
            }
        }
        return null;
    }

    public static final boolean d(@RecentlyNonNull PackageInfo packageInfo, boolean z2) {
        if (!(packageInfo == null || packageInfo.signatures == null)) {
            if ((z2 ? c(packageInfo, a0.a) : c(packageInfo, a0.a[0])) != null) {
                return true;
            }
        }
        return false;
    }

    public boolean b(int i) {
        g0 g0Var;
        int length;
        boolean z2;
        g0 b2;
        ApplicationInfo applicationInfo;
        String[] packagesForUid = this.f1344b.getPackageManager().getPackagesForUid(i);
        if (packagesForUid != null && (length = packagesForUid.length) != 0) {
            g0Var = null;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    Objects.requireNonNull(g0Var, "null reference");
                    break;
                }
                String str = packagesForUid[i2];
                if (str == null) {
                    g0Var = g0.b("null pkg");
                } else if (!str.equals(this.c)) {
                    z zVar = b0.a;
                    StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
                    try {
                        try {
                            b0.a();
                            z2 = b0.c.h();
                            StrictMode.setThreadPolicy(allowThreadDiskReads);
                        } finally {
                        }
                    } catch (RemoteException | DynamiteModule.LoadingException e) {
                        Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e);
                        StrictMode.setThreadPolicy(allowThreadDiskReads);
                        z2 = false;
                    }
                    if (z2) {
                        boolean a2 = e.a(this.f1344b);
                        allowThreadDiskReads = StrictMode.allowThreadDiskReads();
                        try {
                            Objects.requireNonNull(b0.e, "null reference");
                            try {
                                b0.a();
                                try {
                                    zzq H = b0.c.H(new zzn(str, a2, false, new b(b0.e), false));
                                    if (H.j) {
                                        b2 = g0.a;
                                    } else {
                                        String str2 = H.k;
                                        str2 = "error checking package certificate";
                                        if (str2 == null) {
                                        }
                                        b2 = b.i.a.f.e.o.f.k2(H.l) == 4 ? g0.c(str2, new PackageManager.NameNotFoundException()) : g0.b(str2);
                                    }
                                } catch (RemoteException e2) {
                                    Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e2);
                                    b2 = g0.c("module call", e2);
                                }
                            } catch (DynamiteModule.LoadingException e3) {
                                Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e3);
                                String valueOf = String.valueOf(e3.getMessage());
                                b2 = g0.c(valueOf.length() != 0 ? "module init: ".concat(valueOf) : new String("module init: "), e3);
                            }
                        } finally {
                        }
                    } else {
                        try {
                            PackageInfo packageInfo = this.f1344b.getPackageManager().getPackageInfo(str, 64);
                            boolean a3 = e.a(this.f1344b);
                            if (packageInfo == null) {
                                b2 = g0.b("null pkg");
                            } else {
                                Signature[] signatureArr = packageInfo.signatures;
                                if (signatureArr == null || signatureArr.length != 1) {
                                    b2 = g0.b("single cert required");
                                } else {
                                    y yVar = new y(packageInfo.signatures[0].toByteArray());
                                    String str3 = packageInfo.packageName;
                                    allowThreadDiskReads = StrictMode.allowThreadDiskReads();
                                    try {
                                        g0 b3 = b0.b(str3, yVar, a3, false);
                                        StrictMode.setThreadPolicy(allowThreadDiskReads);
                                        if (!(!b3.f1345b || (applicationInfo = packageInfo.applicationInfo) == null || (applicationInfo.flags & 2) == 0)) {
                                            allowThreadDiskReads = StrictMode.allowThreadDiskReads();
                                            try {
                                                g0 b4 = b0.b(str3, yVar, false, true);
                                                StrictMode.setThreadPolicy(allowThreadDiskReads);
                                                if (b4.f1345b) {
                                                    b2 = g0.b("debuggable release cert app rejected");
                                                }
                                            } finally {
                                            }
                                        }
                                        b2 = b3;
                                    } finally {
                                    }
                                }
                            }
                        } catch (PackageManager.NameNotFoundException e4) {
                            g0Var = g0.c(str.length() != 0 ? "no pkg ".concat(str) : new String("no pkg "), e4);
                        }
                    }
                    if (b2.f1345b) {
                        this.c = str;
                    }
                    g0Var = b2;
                } else {
                    g0Var = g0.a;
                }
                if (g0Var.f1345b) {
                    break;
                }
                i2++;
            }
        } else {
            g0Var = g0.b("no pkgs");
        }
        if (!g0Var.f1345b && Log.isLoggable("GoogleCertificatesRslt", 3)) {
            if (g0Var.d != null) {
                Log.d("GoogleCertificatesRslt", g0Var.a(), g0Var.d);
            } else {
                Log.d("GoogleCertificatesRslt", g0Var.a());
            }
        }
        return g0Var.f1345b;
    }
}
