package b.i.a.f.e;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import android.util.Log;
import b.i.a.f.e.k.c0;
import b.i.a.f.e.k.d0;
import b.i.a.f.e.k.e0;
import b.i.a.f.e.o.a;
import b.i.a.f.e.o.d;
import b.i.a.f.f.b;
import com.adjust.sdk.Constants;
import com.google.android.gms.common.zzs;
import com.google.android.gms.dynamite.DynamiteModule;
import java.security.MessageDigest;
import java.util.Objects;
import java.util.concurrent.Callable;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public final class b0 {
    public static volatile e0 c;
    public static Context e;
    public static final z a = new v(x.i("0\u0082\u0004C0\u0082\u0003+ \u0003\u0002\u0001\u0002\u0002\t\u0000Âà\u0087FdJ0\u008d0"));

    /* renamed from: b  reason: collision with root package name */
    public static final z f1341b = new w(x.i("0\u0082\u0004¨0\u0082\u0003\u0090 \u0003\u0002\u0001\u0002\u0002\t\u0000Õ\u0085¸l}ÓNõ0"));
    public static final Object d = new Object();

    static {
        new t(x.i("0\u0082\u0005È0\u0082\u0003° \u0003\u0002\u0001\u0002\u0002\u0014\u0010\u008ae\bsù/\u008eQí"));
        new u(x.i("0\u0082\u0006\u00040\u0082\u0003ì \u0003\u0002\u0001\u0002\u0002\u0014\u0003£²\u00ad×árÊkì"));
    }

    public static void a() throws DynamiteModule.LoadingException {
        e0 e0Var;
        if (c == null) {
            Objects.requireNonNull(e, "null reference");
            synchronized (d) {
                try {
                    if (c == null) {
                        IBinder b2 = DynamiteModule.c(e, DynamiteModule.f2978b, "com.google.android.gms.googlecertificates").b("com.google.android.gms.common.GoogleCertificatesImpl");
                        int i = d0.a;
                        if (b2 == null) {
                            e0Var = null;
                        } else {
                            IInterface queryLocalInterface = b2.queryLocalInterface("com.google.android.gms.common.internal.IGoogleCertificatesApi");
                            if (queryLocalInterface instanceof e0) {
                                e0Var = (e0) queryLocalInterface;
                            } else {
                                e0Var = new c0(b2);
                            }
                        }
                        c = e0Var;
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
    }

    public static g0 b(final String str, final x xVar, final boolean z2, boolean z3) {
        try {
            a();
            Objects.requireNonNull(e, "null reference");
            try {
                return c.V(new zzs(str, xVar, z2, z3), new b(e.getPackageManager())) ? g0.a : new f0(new Callable(z2, str, xVar) { // from class: b.i.a.f.e.s
                    public final boolean j;
                    public final String k;
                    public final x l;

                    {
                        this.j = z2;
                        this.k = str;
                        this.l = xVar;
                    }

                    @Override // java.util.concurrent.Callable
                    public final Object call() {
                        boolean z4 = this.j;
                        String str2 = this.k;
                        x xVar2 = this.l;
                        Object[] objArr = new Object[5];
                        objArr[0] = true != (!z4 && b0.b(str2, xVar2, true, false).f1345b) ? "not allowed" : "debug cert rejected";
                        objArr[1] = str2;
                        MessageDigest a2 = a.a(Constants.SHA1);
                        Objects.requireNonNull(a2, "null reference");
                        byte[] digest = a2.digest(xVar2.g());
                        int length = digest.length;
                        char[] cArr = new char[length + length];
                        int i = 0;
                        for (byte b2 : digest) {
                            int i2 = b2 & 255;
                            int i3 = i + 1;
                            char[] cArr2 = d.f1392b;
                            cArr[i] = cArr2[i2 >>> 4];
                            i = i3 + 1;
                            cArr[i3] = cArr2[i2 & 15];
                        }
                        objArr[2] = new String(cArr);
                        objArr[3] = Boolean.valueOf(z4);
                        objArr[4] = "12451000.false";
                        return String.format("%s: pkg=%s, sha1=%s, atk=%s, ver=%s", objArr);
                    }
                });
            } catch (RemoteException e2) {
                Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e2);
                return g0.c("module call", e2);
            }
        } catch (DynamiteModule.LoadingException e3) {
            Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e3);
            String valueOf = String.valueOf(e3.getMessage());
            return g0.c(valueOf.length() != 0 ? "module init: ".concat(valueOf) : new String("module init: "), e3);
        }
    }
}
