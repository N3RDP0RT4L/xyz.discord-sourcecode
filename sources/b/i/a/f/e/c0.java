package b.i.a.f.e;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.common.zzn;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public final class c0 implements Parcelable.Creator<zzn> {
    @Override // android.os.Parcelable.Creator
    public final zzn createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        String str = null;
        IBinder iBinder = null;
        boolean z2 = false;
        boolean z3 = false;
        boolean z4 = false;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                str = d.R(parcel, readInt);
            } else if (c == 2) {
                z2 = d.E1(parcel, readInt);
            } else if (c == 3) {
                z3 = d.E1(parcel, readInt);
            } else if (c == 4) {
                iBinder = d.F1(parcel, readInt);
            } else if (c != 5) {
                d.d2(parcel, readInt);
            } else {
                z4 = d.E1(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        return new zzn(str, z2, z3, iBinder, z4);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ zzn[] newArray(int i) {
        return new zzn[i];
    }
}
