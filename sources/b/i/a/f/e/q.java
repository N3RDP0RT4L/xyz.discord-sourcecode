package b.i.a.f.e;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.common.ConnectionResult;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public final class q implements Parcelable.Creator<ConnectionResult> {
    @Override // android.os.Parcelable.Creator
    public final ConnectionResult createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        int i = 0;
        PendingIntent pendingIntent = null;
        String str = null;
        int i2 = 0;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                i = d.G1(parcel, readInt);
            } else if (c == 2) {
                i2 = d.G1(parcel, readInt);
            } else if (c == 3) {
                pendingIntent = (PendingIntent) d.Q(parcel, readInt, PendingIntent.CREATOR);
            } else if (c != 4) {
                d.d2(parcel, readInt);
            } else {
                str = d.R(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        return new ConnectionResult(i, i2, pendingIntent, str);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ ConnectionResult[] newArray(int i) {
        return new ConnectionResult[i];
    }
}
