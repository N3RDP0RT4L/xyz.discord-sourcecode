package b.i.a.f.e.k;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.zau;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public final class x implements Parcelable.Creator<zau> {
    @Override // android.os.Parcelable.Creator
    public final zau createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        IBinder iBinder = null;
        ConnectionResult connectionResult = null;
        int i = 0;
        boolean z2 = false;
        boolean z3 = false;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                i = d.G1(parcel, readInt);
            } else if (c == 2) {
                iBinder = d.F1(parcel, readInt);
            } else if (c == 3) {
                connectionResult = (ConnectionResult) d.Q(parcel, readInt, ConnectionResult.CREATOR);
            } else if (c == 4) {
                z2 = d.E1(parcel, readInt);
            } else if (c != 5) {
                d.d2(parcel, readInt);
            } else {
                z3 = d.E1(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        return new zau(i, iBinder, connectionResult, z2, z3);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zau[] newArray(int i) {
        return new zau[i];
    }
}
