package b.i.a.f.e.k;

import android.accounts.Account;
import android.os.IInterface;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.a.f.e.h.a;
import com.google.android.gms.common.api.Scope;
import java.util.Collections;
import java.util.Set;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public abstract class d<T extends IInterface> extends b<T> implements a.f {

    /* renamed from: x  reason: collision with root package name */
    public final c f1378x;

    /* renamed from: y  reason: collision with root package name */
    public final Set<Scope> f1379y;
    @Nullable

    /* renamed from: z  reason: collision with root package name */
    public final Account f1380z;

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public d(android.content.Context r10, android.os.Looper r11, int r12, b.i.a.f.e.k.c r13, b.i.a.f.e.h.j.f r14, b.i.a.f.e.h.j.l r15) {
        /*
            r9 = this;
            b.i.a.f.e.k.e r3 = b.i.a.f.e.k.e.a(r10)
            java.lang.Object r0 = com.google.android.gms.common.GoogleApiAvailability.c
            com.google.android.gms.common.GoogleApiAvailability r4 = com.google.android.gms.common.GoogleApiAvailability.d
            java.lang.String r0 = "null reference"
            java.util.Objects.requireNonNull(r14, r0)
            java.util.Objects.requireNonNull(r15, r0)
            b.i.a.f.e.k.q r6 = new b.i.a.f.e.k.q
            r6.<init>(r14)
            b.i.a.f.e.k.p r7 = new b.i.a.f.e.k.p
            r7.<init>(r15)
            java.lang.String r8 = r13.f
            r0 = r9
            r1 = r10
            r2 = r11
            r5 = r12
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            r9.f1378x = r13
            android.accounts.Account r10 = r13.a
            r9.f1380z = r10
            java.util.Set<com.google.android.gms.common.api.Scope> r10 = r13.c
            java.util.Iterator r11 = r10.iterator()
        L2f:
            boolean r12 = r11.hasNext()
            if (r12 == 0) goto L4a
            java.lang.Object r12 = r11.next()
            com.google.android.gms.common.api.Scope r12 = (com.google.android.gms.common.api.Scope) r12
            boolean r12 = r10.contains(r12)
            if (r12 == 0) goto L42
            goto L2f
        L42:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r11 = "Expanding scopes is not permitted, use implied scopes instead"
            r10.<init>(r11)
            throw r10
        L4a:
            r9.f1379y = r10
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.e.k.d.<init>(android.content.Context, android.os.Looper, int, b.i.a.f.e.k.c, b.i.a.f.e.h.j.f, b.i.a.f.e.h.j.l):void");
    }

    @Override // b.i.a.f.e.h.a.f
    @NonNull
    public Set<Scope> a() {
        return o() ? this.f1379y : Collections.emptySet();
    }

    @Override // b.i.a.f.e.k.b
    @Nullable
    public final Account s() {
        return this.f1380z;
    }

    @Override // b.i.a.f.e.k.b
    public final Set<Scope> v() {
        return this.f1379y;
    }
}
