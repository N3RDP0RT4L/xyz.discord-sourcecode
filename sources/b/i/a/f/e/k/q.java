package b.i.a.f.e.k;

import android.os.Bundle;
import androidx.annotation.Nullable;
import b.i.a.f.e.h.j.f;
import b.i.a.f.e.k.b;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public final class q implements b.a {
    public final /* synthetic */ f j;

    public q(f fVar) {
        this.j = fVar;
    }

    @Override // b.i.a.f.e.k.b.a
    public final void c(int i) {
        this.j.c(i);
    }

    @Override // b.i.a.f.e.k.b.a
    public final void i(@Nullable Bundle bundle) {
        this.j.i(null);
    }
}
