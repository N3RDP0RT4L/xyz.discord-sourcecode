package b.i.a.f.e.k;

import android.os.IInterface;
import android.os.RemoteException;
import b.i.a.f.f.a;
import com.google.android.gms.common.zzn;
import com.google.android.gms.common.zzq;
import com.google.android.gms.common.zzs;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public interface e0 extends IInterface {
    zzq H(zzn zznVar) throws RemoteException;

    boolean V(zzs zzsVar, a aVar) throws RemoteException;

    boolean h() throws RemoteException;
}
