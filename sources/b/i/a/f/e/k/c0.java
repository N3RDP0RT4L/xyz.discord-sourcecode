package b.i.a.f.e.k;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import b.i.a.f.h.g.a;
import b.i.a.f.h.g.c;
import com.google.android.gms.common.zzn;
import com.google.android.gms.common.zzq;
import com.google.android.gms.common.zzs;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public final class c0 extends a implements e0 {
    public c0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.IGoogleCertificatesApi");
    }

    @Override // b.i.a.f.e.k.e0
    public final zzq H(zzn zznVar) throws RemoteException {
        Parcel g = g();
        int i = c.a;
        g.writeInt(1);
        zznVar.writeToParcel(g, 0);
        Parcel c = c(6, g);
        zzq zzqVar = (zzq) c.a(c, zzq.CREATOR);
        c.recycle();
        return zzqVar;
    }

    @Override // b.i.a.f.e.k.e0
    public final boolean V(zzs zzsVar, b.i.a.f.f.a aVar) throws RemoteException {
        Parcel g = g();
        int i = c.a;
        boolean z2 = true;
        g.writeInt(1);
        zzsVar.writeToParcel(g, 0);
        c.b(g, aVar);
        Parcel c = c(5, g);
        if (c.readInt() == 0) {
            z2 = false;
        }
        c.recycle();
        return z2;
    }

    @Override // b.i.a.f.e.k.e0
    public final boolean h() throws RemoteException {
        Parcel c = c(7, g());
        int i = c.a;
        boolean z2 = c.readInt() != 0;
        c.recycle();
        return z2;
    }
}
