package b.i.a.f.e.k;

import android.content.Intent;
import b.i.a.f.e.h.j.j;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public final class o extends n {
    public final /* synthetic */ Intent j;
    public final /* synthetic */ j k;

    public o(Intent intent, j jVar, int i) {
        this.j = intent;
        this.k = jVar;
    }

    @Override // b.i.a.f.e.k.n
    public final void a() {
        Intent intent = this.j;
        if (intent != null) {
            this.k.startActivityForResult(intent, 2);
        }
    }
}
