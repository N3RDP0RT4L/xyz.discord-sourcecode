package b.i.a.f.e.k;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.GetServiceRequest;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public final class p0 implements Parcelable.Creator<GetServiceRequest> {
    public static void a(GetServiceRequest getServiceRequest, Parcel parcel, int i) {
        int y2 = d.y2(parcel, 20293);
        int i2 = getServiceRequest.j;
        parcel.writeInt(262145);
        parcel.writeInt(i2);
        int i3 = getServiceRequest.k;
        parcel.writeInt(262146);
        parcel.writeInt(i3);
        int i4 = getServiceRequest.l;
        parcel.writeInt(262147);
        parcel.writeInt(i4);
        d.t2(parcel, 4, getServiceRequest.m, false);
        d.r2(parcel, 5, getServiceRequest.n, false);
        d.v2(parcel, 6, getServiceRequest.o, i, false);
        d.p2(parcel, 7, getServiceRequest.p, false);
        d.s2(parcel, 8, getServiceRequest.q, i, false);
        d.v2(parcel, 10, getServiceRequest.r, i, false);
        d.v2(parcel, 11, getServiceRequest.f2977s, i, false);
        boolean z2 = getServiceRequest.t;
        parcel.writeInt(262156);
        parcel.writeInt(z2 ? 1 : 0);
        int i5 = getServiceRequest.u;
        parcel.writeInt(262157);
        parcel.writeInt(i5);
        boolean z3 = getServiceRequest.v;
        parcel.writeInt(262158);
        parcel.writeInt(z3 ? 1 : 0);
        d.t2(parcel, 15, getServiceRequest.w, false);
        d.A2(parcel, y2);
    }

    @Override // android.os.Parcelable.Creator
    public final GetServiceRequest createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        String str = null;
        IBinder iBinder = null;
        Scope[] scopeArr = null;
        Bundle bundle = null;
        Account account = null;
        Feature[] featureArr = null;
        Feature[] featureArr2 = null;
        String str2 = null;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        boolean z2 = false;
        int i4 = 0;
        boolean z3 = false;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    i = d.G1(parcel, readInt);
                    break;
                case 2:
                    i2 = d.G1(parcel, readInt);
                    break;
                case 3:
                    i3 = d.G1(parcel, readInt);
                    break;
                case 4:
                    str = d.R(parcel, readInt);
                    break;
                case 5:
                    iBinder = d.F1(parcel, readInt);
                    break;
                case 6:
                    scopeArr = (Scope[]) d.U(parcel, readInt, Scope.CREATOR);
                    break;
                case 7:
                    bundle = d.M(parcel, readInt);
                    break;
                case '\b':
                    account = (Account) d.Q(parcel, readInt, Account.CREATOR);
                    break;
                case '\t':
                default:
                    d.d2(parcel, readInt);
                    break;
                case '\n':
                    featureArr = (Feature[]) d.U(parcel, readInt, Feature.CREATOR);
                    break;
                case 11:
                    featureArr2 = (Feature[]) d.U(parcel, readInt, Feature.CREATOR);
                    break;
                case '\f':
                    z2 = d.E1(parcel, readInt);
                    break;
                case '\r':
                    i4 = d.G1(parcel, readInt);
                    break;
                case 14:
                    z3 = d.E1(parcel, readInt);
                    break;
                case 15:
                    str2 = d.R(parcel, readInt);
                    break;
            }
        }
        d.f0(parcel, m2);
        return new GetServiceRequest(i, i2, i3, str, iBinder, scopeArr, bundle, account, featureArr, featureArr2, z2, i4, z3, str2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ GetServiceRequest[] newArray(int i) {
        return new GetServiceRequest[i];
    }
}
