package b.i.a.f.e.k;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import androidx.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public final class r0 implements ServiceConnection {
    public final Map<ServiceConnection, ServiceConnection> j = new HashMap();
    public int k = 2;
    public boolean l;
    @Nullable
    public IBinder m;
    public final q0 n;
    public ComponentName o;
    public final /* synthetic */ t0 p;

    public r0(t0 t0Var, q0 q0Var) {
        this.p = t0Var;
        this.n = q0Var;
    }

    /* JADX WARN: Removed duplicated region for block: B:24:0x00a3  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x00b8  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void a(java.lang.String r10) {
        /*
            r9 = this;
            r0 = 3
            r9.k = r0
            b.i.a.f.e.k.t0 r0 = r9.p
            b.i.a.f.e.n.a r1 = r0.f
            android.content.Context r2 = r0.d
            b.i.a.f.e.k.q0 r0 = r9.n
            java.lang.String r3 = "ConnectionStatusConfig"
            java.lang.String r4 = r0.f1387b
            r5 = 0
            if (r4 == 0) goto L8a
            boolean r4 = r0.e
            if (r4 == 0) goto L78
            android.os.Bundle r4 = new android.os.Bundle
            r4.<init>()
            java.lang.String r6 = r0.f1387b
            java.lang.String r7 = "serviceActionBundleKey"
            r4.putString(r7, r6)
            android.content.ContentResolver r6 = r2.getContentResolver()     // Catch: java.lang.IllegalArgumentException -> L2f
            android.net.Uri r7 = b.i.a.f.e.k.q0.a     // Catch: java.lang.IllegalArgumentException -> L2f
            java.lang.String r8 = "serviceIntentCall"
            android.os.Bundle r4 = r6.call(r7, r8, r5, r4)     // Catch: java.lang.IllegalArgumentException -> L2f
            goto L4f
        L2f:
            r4 = move-exception
            java.lang.String r4 = java.lang.String.valueOf(r4)
            int r6 = r4.length()
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            int r6 = r6 + 34
            r7.<init>(r6)
            java.lang.String r6 = "Dynamic intent resolution failed: "
            r7.append(r6)
            r7.append(r4)
            java.lang.String r4 = r7.toString()
            android.util.Log.w(r3, r4)
            r4 = r5
        L4f:
            if (r4 != 0) goto L52
            goto L5b
        L52:
            java.lang.String r5 = "serviceResponseIntentKey"
            android.os.Parcelable r4 = r4.getParcelable(r5)
            r5 = r4
            android.content.Intent r5 = (android.content.Intent) r5
        L5b:
            if (r5 != 0) goto L78
            java.lang.String r4 = r0.f1387b
            java.lang.String r4 = java.lang.String.valueOf(r4)
            java.lang.String r6 = "Dynamic lookup for intent failed for action: "
            int r7 = r4.length()
            if (r7 == 0) goto L70
            java.lang.String r4 = r6.concat(r4)
            goto L75
        L70:
            java.lang.String r4 = new java.lang.String
            r4.<init>(r6)
        L75:
            android.util.Log.w(r3, r4)
        L78:
            if (r5 == 0) goto L7b
            goto L93
        L7b:
            android.content.Intent r3 = new android.content.Intent
            java.lang.String r4 = r0.f1387b
            r3.<init>(r4)
            java.lang.String r0 = r0.c
            android.content.Intent r0 = r3.setPackage(r0)
            r4 = r0
            goto L94
        L8a:
            android.content.Intent r0 = new android.content.Intent
            r0.<init>()
            android.content.Intent r5 = r0.setComponent(r5)
        L93:
            r4 = r5
        L94:
            b.i.a.f.e.k.q0 r0 = r9.n
            int r6 = r0.d
            r7 = 1
            r3 = r10
            r5 = r9
            boolean r10 = r1.d(r2, r3, r4, r5, r6, r7)
            r9.l = r10
            if (r10 == 0) goto Lb8
            b.i.a.f.e.k.t0 r10 = r9.p
            android.os.Handler r10 = r10.e
            b.i.a.f.e.k.q0 r0 = r9.n
            r1 = 1
            android.os.Message r10 = r10.obtainMessage(r1, r0)
            b.i.a.f.e.k.t0 r0 = r9.p
            android.os.Handler r1 = r0.e
            long r2 = r0.h
            r1.sendMessageDelayed(r10, r2)
            return
        Lb8:
            r10 = 2
            r9.k = r10
            b.i.a.f.e.k.t0 r10 = r9.p     // Catch: java.lang.IllegalArgumentException -> Lc4
            b.i.a.f.e.n.a r0 = r10.f     // Catch: java.lang.IllegalArgumentException -> Lc4
            android.content.Context r10 = r10.d     // Catch: java.lang.IllegalArgumentException -> Lc4
            r0.c(r10, r9)     // Catch: java.lang.IllegalArgumentException -> Lc4
        Lc4:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.e.k.r0.a(java.lang.String):void");
    }

    @Override // android.content.ServiceConnection
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        synchronized (this.p.c) {
            this.p.e.removeMessages(1, this.n);
            this.m = iBinder;
            this.o = componentName;
            for (ServiceConnection serviceConnection : this.j.values()) {
                serviceConnection.onServiceConnected(componentName, iBinder);
            }
            this.k = 1;
        }
    }

    @Override // android.content.ServiceConnection
    public final void onServiceDisconnected(ComponentName componentName) {
        synchronized (this.p.c) {
            this.p.e.removeMessages(1, this.n);
            this.m = null;
            this.o = componentName;
            for (ServiceConnection serviceConnection : this.j.values()) {
                serviceConnection.onServiceDisconnected(componentName);
            }
            this.k = 2;
        }
    }
}
