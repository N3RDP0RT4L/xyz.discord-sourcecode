package b.i.a.f.e.k;

import android.os.Parcel;
import android.os.RemoteException;
import b.i.a.f.e.x;
import b.i.a.f.f.a;
import b.i.a.f.h.g.b;
import b.i.a.f.h.g.c;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public abstract class y0 extends b implements z0 {
    public static final /* synthetic */ int a = 0;

    public y0() {
        super("com.google.android.gms.common.internal.ICertData");
    }

    @Override // b.i.a.f.h.g.b
    public final boolean c(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            a d = ((x) this).d();
            parcel2.writeNoException();
            c.b(parcel2, d);
        } else if (i != 2) {
            return false;
        } else {
            int i3 = ((x) this).f1397b;
            parcel2.writeNoException();
            parcel2.writeInt(i3);
        }
        return true;
    }
}
