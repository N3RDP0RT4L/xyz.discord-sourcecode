package b.i.a.f.e.k;

import androidx.annotation.NonNull;
import b.i.a.f.e.h.j.l;
import b.i.a.f.e.k.b;
import com.google.android.gms.common.ConnectionResult;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public final class p implements b.AbstractC0113b {
    public final /* synthetic */ l j;

    public p(l lVar) {
        this.j = lVar;
    }

    @Override // b.i.a.f.e.k.b.AbstractC0113b
    public final void g(@NonNull ConnectionResult connectionResult) {
        this.j.g(connectionResult);
    }
}
