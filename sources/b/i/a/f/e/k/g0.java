package b.i.a.f.e.k;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.common.internal.RootTelemetryConfiguration;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public final class g0 implements Parcelable.Creator<RootTelemetryConfiguration> {
    @Override // android.os.Parcelable.Creator
    public final RootTelemetryConfiguration createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        int i = 0;
        boolean z2 = false;
        boolean z3 = false;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                i = d.G1(parcel, readInt);
            } else if (c == 2) {
                z2 = d.E1(parcel, readInt);
            } else if (c == 3) {
                z3 = d.E1(parcel, readInt);
            } else if (c == 4) {
                i2 = d.G1(parcel, readInt);
            } else if (c != 5) {
                d.d2(parcel, readInt);
            } else {
                i3 = d.G1(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        return new RootTelemetryConfiguration(i, z2, z3, i2, i3);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ RootTelemetryConfiguration[] newArray(int i) {
        return new RootTelemetryConfiguration[i];
    }
}
