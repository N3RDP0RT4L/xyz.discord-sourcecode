package b.i.a.f.e.k;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.common.internal.ConnectionTelemetryConfiguration;
import com.google.android.gms.common.internal.RootTelemetryConfiguration;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public final class o0 implements Parcelable.Creator<ConnectionTelemetryConfiguration> {
    @Override // android.os.Parcelable.Creator
    public final ConnectionTelemetryConfiguration createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        RootTelemetryConfiguration rootTelemetryConfiguration = null;
        int[] iArr = null;
        int[] iArr2 = null;
        boolean z2 = false;
        boolean z3 = false;
        int i = 0;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    rootTelemetryConfiguration = (RootTelemetryConfiguration) d.Q(parcel, readInt, RootTelemetryConfiguration.CREATOR);
                    break;
                case 2:
                    z2 = d.E1(parcel, readInt);
                    break;
                case 3:
                    z3 = d.E1(parcel, readInt);
                    break;
                case 4:
                    iArr = d.O(parcel, readInt);
                    break;
                case 5:
                    i = d.G1(parcel, readInt);
                    break;
                case 6:
                    iArr2 = d.O(parcel, readInt);
                    break;
                default:
                    d.d2(parcel, readInt);
                    break;
            }
        }
        d.f0(parcel, m2);
        return new ConnectionTelemetryConfiguration(rootTelemetryConfiguration, z2, z3, iArr, i, iArr2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ ConnectionTelemetryConfiguration[] newArray(int i) {
        return new ConnectionTelemetryConfiguration[i];
    }
}
