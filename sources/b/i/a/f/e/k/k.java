package b.i.a.f.e.k;

import b.i.a.f.e.h.d;
import b.i.a.f.e.h.h;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public class k {
    public static final b a = new t();

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    /* loaded from: classes3.dex */
    public interface a<R extends h, T> {
        T a(R r);
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    /* loaded from: classes3.dex */
    public interface b {
    }

    public static <R extends h> Task<Void> a(d<R> dVar) {
        u uVar = new u();
        b bVar = a;
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        dVar.c(new s(dVar, taskCompletionSource, uVar, bVar));
        return taskCompletionSource.a;
    }
}
