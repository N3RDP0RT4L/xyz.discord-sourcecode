package b.i.a.f.e.k;

import android.accounts.Account;
import android.view.View;
import androidx.collection.ArraySet;
import com.google.android.gms.common.api.Scope;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public final class c {
    public final Account a;

    /* renamed from: b  reason: collision with root package name */
    public final Set<Scope> f1376b;
    public final Set<Scope> c;
    public final Map<b.i.a.f.e.h.a<?>, b> d;
    public final String e;
    public final String f;
    public final b.i.a.f.l.a g;
    public Integer h;

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    /* loaded from: classes3.dex */
    public static final class a {
        public Account a;

        /* renamed from: b  reason: collision with root package name */
        public ArraySet<Scope> f1377b;
        public String c;
        public String d;

        public final c a() {
            return new c(this.a, this.f1377b, null, 0, null, this.c, this.d, b.i.a.f.l.a.j);
        }
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    /* loaded from: classes3.dex */
    public static final class b {
    }

    public c(Account account, Set set, Map map, int i, View view, String str, String str2, b.i.a.f.l.a aVar) {
        this.a = account;
        Set<Scope> emptySet = set == null ? Collections.emptySet() : Collections.unmodifiableSet(set);
        this.f1376b = emptySet;
        Map<b.i.a.f.e.h.a<?>, b> emptyMap = Collections.emptyMap();
        this.d = emptyMap;
        this.e = str;
        this.f = str2;
        this.g = aVar;
        HashSet hashSet = new HashSet(emptySet);
        for (b bVar : emptyMap.values()) {
            Objects.requireNonNull(bVar);
            hashSet.addAll(null);
        }
        this.c = Collections.unmodifiableSet(hashSet);
    }
}
