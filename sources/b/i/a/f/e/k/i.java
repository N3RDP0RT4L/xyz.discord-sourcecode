package b.i.a.f.e.k;

import android.os.IInterface;
import android.os.RemoteException;
import androidx.annotation.Nullable;
import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.internal.GetServiceRequest;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public interface i extends IInterface {
    void w(@RecentlyNonNull h hVar, @Nullable GetServiceRequest getServiceRequest) throws RemoteException;
}
