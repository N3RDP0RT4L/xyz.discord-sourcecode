package b.i.a.f.e.k;

import android.content.Context;
import android.content.ServiceConnection;
import androidx.annotation.RecentlyNonNull;
import b.c.a.a0.d;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public abstract class e {
    public static final Object a = new Object();

    /* renamed from: b  reason: collision with root package name */
    public static e f1381b;

    @RecentlyNonNull
    public static e a(@RecentlyNonNull Context context) {
        synchronized (a) {
            if (f1381b == null) {
                f1381b = new t0(context.getApplicationContext());
            }
        }
        return f1381b;
    }

    public final void b(@RecentlyNonNull String str, @RecentlyNonNull String str2, int i, @RecentlyNonNull ServiceConnection serviceConnection, @RecentlyNonNull String str3, boolean z2) {
        q0 q0Var = new q0(str, str2, i, z2);
        t0 t0Var = (t0) this;
        d.z(serviceConnection, "ServiceConnection must not be null");
        synchronized (t0Var.c) {
            r0 r0Var = t0Var.c.get(q0Var);
            if (r0Var == null) {
                String valueOf = String.valueOf(q0Var);
                StringBuilder sb = new StringBuilder(valueOf.length() + 50);
                sb.append("Nonexistent connection status for service config: ");
                sb.append(valueOf);
                throw new IllegalStateException(sb.toString());
            } else if (r0Var.j.containsKey(serviceConnection)) {
                r0Var.j.remove(serviceConnection);
                if (r0Var.j.isEmpty()) {
                    t0Var.e.sendMessageDelayed(t0Var.e.obtainMessage(0, q0Var), t0Var.g);
                }
            } else {
                String valueOf2 = String.valueOf(q0Var);
                StringBuilder sb2 = new StringBuilder(valueOf2.length() + 76);
                sb2.append("Trying to unbind a GmsServiceConnection  that was not bound before.  config=");
                sb2.append(valueOf2);
                throw new IllegalStateException(sb2.toString());
            }
        }
    }

    public abstract boolean c(q0 q0Var, ServiceConnection serviceConnection, String str);
}
