package b.i.a.f.e.k;

import android.accounts.Account;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import androidx.annotation.RecentlyNonNull;
import b.i.a.f.h.g.b;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public interface g extends IInterface {

    /* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
    /* loaded from: classes3.dex */
    public static abstract class a extends b implements g {
        @RecentlyNonNull
        public static g g(@RecentlyNonNull IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IAccountAccessor");
            if (queryLocalInterface instanceof g) {
                return (g) queryLocalInterface;
            }
            return new w0(iBinder);
        }
    }

    @RecentlyNonNull
    Account b() throws RemoteException;
}
