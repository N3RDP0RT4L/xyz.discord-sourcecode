package b.i.a.f.e.k;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.Log;
import b.c.a.a0.d;
import b.i.a.f.h.g.b;
import b.i.a.f.h.g.c;
import com.google.android.gms.common.internal.zzi;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public abstract class a0 extends b implements h {
    public a0() {
        super("com.google.android.gms.common.internal.IGmsCallbacks");
    }

    @Override // b.i.a.f.h.g.b
    public final boolean c(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            ((j0) this).g(parcel.readInt(), parcel.readStrongBinder(), (Bundle) c.a(parcel, Bundle.CREATOR));
        } else if (i == 2) {
            parcel.readInt();
            Bundle bundle = (Bundle) c.a(parcel, Bundle.CREATOR);
            Log.wtf("GmsClient", "received deprecated onAccountValidationComplete callback, ignoring", new Exception());
        } else if (i != 3) {
            return false;
        } else {
            int readInt = parcel.readInt();
            IBinder readStrongBinder = parcel.readStrongBinder();
            zzi zziVar = (zzi) c.a(parcel, zzi.CREATOR);
            j0 j0Var = (j0) this;
            b bVar = j0Var.a;
            d.z(bVar, "onPostInitCompleteWithConnectionInfo can be called only once per call togetRemoteService");
            Objects.requireNonNull(zziVar, "null reference");
            bVar.v = zziVar;
            j0Var.g(readInt, readStrongBinder, zziVar.j);
        }
        parcel2.writeNoException();
        return true;
    }
}
