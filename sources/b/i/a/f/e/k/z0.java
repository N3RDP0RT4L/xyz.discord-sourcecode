package b.i.a.f.e.k;

import android.os.IInterface;
import android.os.RemoteException;
import b.i.a.f.f.a;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public interface z0 extends IInterface {
    a d() throws RemoteException;

    int e() throws RemoteException;
}
