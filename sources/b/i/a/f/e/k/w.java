package b.i.a.f.e.k;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.internal.zas;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public final class w implements Parcelable.Creator<zas> {
    @Override // android.os.Parcelable.Creator
    public final zas createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        int i = 0;
        Account account = null;
        GoogleSignInAccount googleSignInAccount = null;
        int i2 = 0;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                i = d.G1(parcel, readInt);
            } else if (c == 2) {
                account = (Account) d.Q(parcel, readInt, Account.CREATOR);
            } else if (c == 3) {
                i2 = d.G1(parcel, readInt);
            } else if (c != 4) {
                d.d2(parcel, readInt);
            } else {
                googleSignInAccount = (GoogleSignInAccount) d.Q(parcel, readInt, GoogleSignInAccount.CREATOR);
            }
        }
        d.f0(parcel, m2);
        return new zas(i, account, i2, googleSignInAccount);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zas[] newArray(int i) {
        return new zas[i];
    }
}
