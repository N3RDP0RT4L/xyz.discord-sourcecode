package b.i.a.f.e.k;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import b.i.a.f.f.a;
import b.i.a.f.h.g.a;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public final class x0 extends a implements z0 {
    public x0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.ICertData");
    }

    @Override // b.i.a.f.e.k.z0
    public final b.i.a.f.f.a d() throws RemoteException {
        Parcel c = c(1, g());
        b.i.a.f.f.a g = a.AbstractBinderC0116a.g(c.readStrongBinder());
        c.recycle();
        return g;
    }

    @Override // b.i.a.f.e.k.z0
    public final int e() throws RemoteException {
        Parcel c = c(2, g());
        int readInt = c.readInt();
        c.recycle();
        return readInt;
    }
}
