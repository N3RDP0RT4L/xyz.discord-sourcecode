package b.i.a.f.e.k;

import android.content.Context;
import android.util.SparseIntArray;
import androidx.annotation.NonNull;
import b.i.a.f.e.c;
import b.i.a.f.e.h.a;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public final class r {
    public final SparseIntArray a = new SparseIntArray();

    /* renamed from: b  reason: collision with root package name */
    public c f1388b;

    public r(@NonNull c cVar) {
        Objects.requireNonNull(cVar, "null reference");
        this.f1388b = cVar;
    }

    public final int a(@NonNull Context context, @NonNull a.f fVar) {
        Objects.requireNonNull(context, "null reference");
        Objects.requireNonNull(fVar, "null reference");
        int i = 0;
        if (!fVar.k()) {
            return 0;
        }
        int l = fVar.l();
        int i2 = this.a.get(l, -1);
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        while (true) {
            if (i3 >= this.a.size()) {
                i = i2;
                break;
            }
            int keyAt = this.a.keyAt(i3);
            if (keyAt > l && this.a.get(keyAt) == 0) {
                break;
            }
            i3++;
        }
        if (i == -1) {
            i = this.f1388b.b(context, l);
        }
        this.a.put(l, i);
        return i;
    }
}
