package b.i.a.f.e;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.common.zzs;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public final class e0 implements Parcelable.Creator<zzs> {
    @Override // android.os.Parcelable.Creator
    public final zzs createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        boolean z2 = false;
        String str = null;
        IBinder iBinder = null;
        boolean z3 = false;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                str = d.R(parcel, readInt);
            } else if (c == 2) {
                iBinder = d.F1(parcel, readInt);
            } else if (c == 3) {
                z2 = d.E1(parcel, readInt);
            } else if (c != 4) {
                d.d2(parcel, readInt);
            } else {
                z3 = d.E1(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        return new zzs(str, iBinder, z2, z3);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ zzs[] newArray(int i) {
        return new zzs[i];
    }
}
