package b.i.a.f.e;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.common.Feature;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public final class r implements Parcelable.Creator<Feature> {
    @Override // android.os.Parcelable.Creator
    public final Feature createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        String str = null;
        int i = 0;
        long j = -1;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                str = d.R(parcel, readInt);
            } else if (c == 2) {
                i = d.G1(parcel, readInt);
            } else if (c != 3) {
                d.d2(parcel, readInt);
            } else {
                j = d.H1(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        return new Feature(str, i, j);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Feature[] newArray(int i) {
        return new Feature[i];
    }
}
