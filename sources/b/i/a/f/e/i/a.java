package b.i.a.f.e.i;

import androidx.annotation.RecentlyNonNull;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public abstract class a<T> {
    @RecentlyNonNull
    public final T a;

    public a(@RecentlyNonNull String str, @RecentlyNonNull T t) {
        this.a = t;
    }
}
