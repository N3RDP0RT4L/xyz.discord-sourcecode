package b.i.a.f.e.l;

import android.util.Log;
import androidx.annotation.Nullable;
import androidx.annotation.RecentlyNonNull;
import b.i.a.f.e.k.f;
import java.util.Locale;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public class a {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1390b;
    public final int c;

    public a(@RecentlyNonNull String str, @RecentlyNonNull String... strArr) {
        String str2;
        if (strArr.length == 0) {
            str2 = "";
        } else {
            StringBuilder O = b.d.b.a.a.O('[');
            for (String str3 : strArr) {
                if (O.length() > 1) {
                    O.append(",");
                }
                O.append(str3);
            }
            O.append("] ");
            str2 = O.toString();
        }
        this.f1390b = str2;
        this.a = str;
        new f(str);
        int i = 2;
        while (i <= 7 && !Log.isLoggable(this.a, i)) {
            i++;
        }
        this.c = i;
    }

    public void a(@RecentlyNonNull String str, @Nullable Object... objArr) {
        if (this.c <= 3) {
            String str2 = this.a;
            if (objArr.length > 0) {
                str = String.format(Locale.US, str, objArr);
            }
            Log.d(str2, this.f1390b.concat(str));
        }
    }

    public void b(@RecentlyNonNull String str, @Nullable Object... objArr) {
        String str2 = this.a;
        if (objArr.length > 0) {
            str = String.format(Locale.US, str, objArr);
        }
        Log.e(str2, this.f1390b.concat(str));
    }
}
