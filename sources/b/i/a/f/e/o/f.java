package b.i.a.f.e.o;

import android.annotation.TargetApi;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.Camera;
import android.opengl.EGL14;
import android.opengl.EGLConfig;
import android.opengl.EGLContext;
import android.opengl.EGLDisplay;
import android.opengl.EGLSurface;
import android.opengl.GLES20;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;
import androidx.annotation.DimenRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.WorkerThread;
import androidx.media.AudioAttributesCompat;
import androidx.recyclerview.widget.RecyclerView;
import b.i.a.f.h.l.a3;
import b.i.a.f.h.l.b5;
import b.i.a.f.h.l.b7;
import b.i.a.f.h.l.c3;
import b.i.a.f.h.l.c7;
import b.i.a.f.h.l.d2;
import b.i.a.f.h.l.e6;
import b.i.a.f.h.l.k7;
import b.i.a.f.h.l.q6;
import b.i.a.f.h.l.s3;
import b.i.a.f.h.l.t3;
import b.i.a.f.h.l.u4;
import b.i.a.f.h.l.w4;
import b.i.a.f.h.l.x4;
import b.i.a.f.h.l.z2;
import b.i.a.f.h.l.z3;
import b.i.a.f.i.b.ga;
import b.i.a.f.i.b.q3;
import b.i.a.f.n.c0;
import b.i.a.f.n.d0;
import b.i.b.b.h;
import b.i.b.b.n0;
import b.i.c.l.d;
import b.i.c.l.e;
import b.i.c.l.o;
import b.i.c.m.d.k.s0;
import b.i.c.m.d.k.t0;
import b.o.a.x.i;
import b.o.a.x.j;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.google.android.gms.common.R;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.measurement.zzij;
import com.google.android.gms.tasks.Task;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.firebase.appindexing.FirebaseAppIndexingException;
import com.google.firebase.appindexing.FirebaseAppIndexingInvalidArgumentException;
import com.google.firebase.appindexing.FirebaseAppIndexingTooManyArgumentsException;
import com.google.firebase.appindexing.zza;
import com.google.firebase.appindexing.zzb;
import com.google.firebase.appindexing.zzc;
import com.google.firebase.appindexing.zzd;
import com.google.firebase.appindexing.zze;
import com.google.firebase.appindexing.zzf;
import com.google.firebase.appindexing.zzg;
import com.google.firebase.appindexing.zzh;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.internal.bind.TypeAdapters;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.MalformedJsonException;
import com.lytefast.flexinput.viewmodel.FlexInputViewModel;
import d0.g0.t;
import d0.k;
import d0.l;
import d0.w.d;
import d0.z.d.e0;
import d0.z.d.m;
import f0.e0.f.d;
import g0.n;
import g0.p;
import g0.u;
import g0.v;
import g0.x;
import java.io.EOFException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.IDN;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPOutputStream;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineExceptionHandler;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.Job;
import kotlinx.coroutines.internal.UndeliveredElementException;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import org.objectweb.asm.Opcodes;
import rx.exceptions.CompositeException;
import rx.exceptions.OnCompletedFailedException;
import rx.exceptions.OnErrorFailedException;
import rx.exceptions.OnErrorNotImplementedException;
import rx.exceptions.OnErrorThrowable;
import s.a.a.r;
import s.a.b0;
import s.a.b2.f;
import s.a.b2.q;
import s.a.b2.s;
import s.a.f0;
import s.a.f1;
import s.a.g0;
import s.a.h0;
import s.a.i0;
import s.a.i1;
import s.a.j0;
import s.a.j1;
import s.a.k1;
import s.a.m0;
import s.a.s1;
import s.a.t1;
import s.a.w;
import s.a.y1;
import s.a.z;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public final class f {
    public static Context a;
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public static Boolean f1393b;
    public static ga c;

    /* compiled from: SizeSelectors.java */
    /* loaded from: classes3.dex */
    public class a implements j {
        public final /* synthetic */ int a;

        public a(int i) {
            this.a = i;
        }

        @Override // b.o.a.x.j
        public boolean a(@NonNull b.o.a.x.b bVar) {
            return bVar.j <= this.a;
        }
    }

    /* compiled from: SizeSelectors.java */
    /* loaded from: classes3.dex */
    public class b implements j {
        public final /* synthetic */ int a;

        public b(int i) {
            this.a = i;
        }

        @Override // b.o.a.x.j
        public boolean a(@NonNull b.o.a.x.b bVar) {
            return bVar.j >= this.a;
        }
    }

    /* compiled from: SizeSelectors.java */
    /* loaded from: classes3.dex */
    public class c implements j {
        public final /* synthetic */ int a;

        public c(int i) {
            this.a = i;
        }

        @Override // b.o.a.x.j
        public boolean a(@NonNull b.o.a.x.b bVar) {
            return bVar.k <= this.a;
        }
    }

    /* compiled from: SizeSelectors.java */
    /* loaded from: classes3.dex */
    public class d implements j {
        public final /* synthetic */ int a;

        public d(int i) {
            this.a = i;
        }

        @Override // b.o.a.x.j
        public boolean a(@NonNull b.o.a.x.b bVar) {
            return bVar.k >= this.a;
        }
    }

    /* compiled from: Await.kt */
    @d0.w.i.a.e(c = "kotlinx.coroutines.AwaitKt", f = "Await.kt", l = {26}, m = "awaitAll")
    /* loaded from: classes3.dex */
    public static final class e extends d0.w.i.a.d {
        public Object L$0;
        public int label;
        public /* synthetic */ Object result;

        public e(Continuation continuation) {
            super(continuation);
        }

        @Override // d0.w.i.a.a
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return f.l(null, this);
        }
    }

    /* compiled from: Channels.kt */
    @d0.w.i.a.e(c = "kotlinx.coroutines.flow.FlowKt__ChannelsKt", f = "Channels.kt", l = {50, 61}, m = "emitAllImpl$FlowKt__ChannelsKt")
    /* renamed from: b.i.a.f.e.o.f$f  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0115f extends d0.w.i.a.d {
        public Object L$0;
        public Object L$1;
        public Object L$2;
        public Object L$3;
        public boolean Z$0;
        public int label;
        public /* synthetic */ Object result;

        public C0115f(Continuation continuation) {
            super(continuation);
        }

        @Override // d0.w.i.a.a
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return f.U(null, null, false, this);
        }
    }

    /* compiled from: KotlinExtensions.kt */
    /* loaded from: classes3.dex */
    public static final class g implements Runnable {
        public final /* synthetic */ Continuation j;
        public final /* synthetic */ Exception k;

        public g(Continuation continuation, Exception exc) {
            this.j = continuation;
            this.k = exc;
        }

        @Override // java.lang.Runnable
        public final void run() {
            Continuation intercepted = d0.w.h.b.intercepted(this.j);
            Exception exc = this.k;
            k.a aVar = k.j;
            intercepted.resumeWith(k.m73constructorimpl(l.createFailure(exc)));
        }
    }

    /* compiled from: KotlinExtensions.kt */
    @d0.w.i.a.e(c = "retrofit2.KotlinExtensions", f = "KotlinExtensions.kt", l = {113}, m = "suspendAndThrow")
    /* loaded from: classes3.dex */
    public static final class h extends d0.w.i.a.d {
        public Object L$0;
        public int label;
        public /* synthetic */ Object result;

        public h(Continuation continuation) {
            super(continuation);
        }

        @Override // d0.w.i.a.a
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return f.i1(null, this);
        }
    }

    @CanIgnoreReturnValue
    public static int A(int i, String str) {
        if (i >= 0) {
            return i;
        }
        throw new IllegalArgumentException(str + " cannot be negative but was: " + i);
    }

    public static boolean A0() {
        return Build.VERSION.SDK_INT >= 26;
    }

    public static boolean A1(long j) {
        int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
        if (i >= 0) {
            return i != 0;
        }
        throw new IllegalArgumentException(b.d.b.a.a.s("n >= 0 required but it was ", j));
    }

    public static final void B(long j, long j2, long j3) {
        if ((j2 | j3) < 0 || j2 > j || j - j2 < j3) {
            throw new ArrayIndexOutOfBoundsException("size=" + j + " offset=" + j2 + " byteCount=" + j3);
        }
    }

    public static final boolean B0(int i) {
        return i == 1 || i == 2;
    }

    @NonNull
    public static Task<Void> B1(@Nullable Collection<? extends Task<?>> collection) {
        if (collection == null || collection.isEmpty()) {
            return Z(null);
        }
        for (Task<?> task : collection) {
            Objects.requireNonNull(task, "null tasks are not accepted");
        }
        c0 c0Var = new c0();
        b.i.a.f.n.j jVar = new b.i.a.f.n.j(collection.size(), c0Var);
        for (Task<?> task2 : collection) {
            p2(task2, jVar);
        }
        return c0Var;
    }

    @CanIgnoreReturnValue
    public static int C(int i, int i2) {
        if (i >= 0 && i <= i2) {
            return i;
        }
        throw new IndexOutOfBoundsException(m(i, i2, "index"));
    }

    public static boolean C0(Camera camera) {
        List<String> supportedFlashModes;
        if (camera != null) {
            Camera.Parameters parameters = camera.getParameters();
            if (parameters.getFlashMode() != null && (supportedFlashModes = parameters.getSupportedFlashModes()) != null && !supportedFlashModes.isEmpty() && (supportedFlashModes.size() != 1 || !supportedFlashModes.get(0).equals("off"))) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARN: Finally extract failed */
    public static final <T> Object C1(CoroutineContext coroutineContext, Function2<? super CoroutineScope, ? super Continuation<? super T>, ? extends Object> function2, Continuation<? super T> continuation) {
        Object obj;
        boolean z2;
        CoroutineContext context = continuation.getContext();
        CoroutineContext plus = context.plus(coroutineContext);
        Job job = (Job) plus.get(Job.h);
        if (job == null || job.a()) {
            if (plus == context) {
                r rVar = new r(plus, continuation);
                obj = g1(rVar, rVar, function2);
            } else {
                int i = d0.w.d.e;
                d.b bVar = d.b.a;
                if (m.areEqual((d0.w.d) plus.get(bVar), (d0.w.d) context.get(bVar))) {
                    y1 y1Var = new y1(plus, continuation);
                    Object b2 = s.a.a.a.b(plus, null);
                    try {
                        Object g1 = g1(y1Var, y1Var, function2);
                        s.a.a.a.a(plus, b2);
                        obj = g1;
                    } catch (Throwable th) {
                        s.a.a.a.a(plus, b2);
                        throw th;
                    }
                } else {
                    i0 i0Var = new i0(plus, continuation);
                    i0Var.f0();
                    f1(function2, i0Var, i0Var, null, 4);
                    while (true) {
                        int i2 = i0Var._decision;
                        z2 = false;
                        if (i2 == 0) {
                            if (i0.n.compareAndSet(i0Var, 0, 1)) {
                                z2 = true;
                                break;
                            }
                        } else if (i2 != 2) {
                            throw new IllegalStateException("Already suspended".toString());
                        }
                    }
                    if (z2) {
                        obj = d0.w.h.c.getCOROUTINE_SUSPENDED();
                    } else {
                        obj = i1.a(i0Var.M());
                        if (obj instanceof w) {
                            throw ((w) obj).f3819b;
                        }
                    }
                }
            }
            if (obj == d0.w.h.c.getCOROUTINE_SUSPENDED()) {
                d0.w.i.a.g.probeCoroutineSuspended(continuation);
            }
            return obj;
        }
        throw job.q();
    }

    public static void D(int i, int i2, int i3) {
        String str;
        if (i < 0 || i2 < i || i2 > i3) {
            if (i < 0 || i > i3) {
                str = m(i, i3, "start index");
            } else {
                str = (i2 < 0 || i2 > i3) ? m(i2, i3, "end index") : I0("end index (%s) must not be less than start index (%s)", Integer.valueOf(i2), Integer.valueOf(i));
            }
            throw new IndexOutOfBoundsException(str);
        }
    }

    public static boolean D0(@RecentlyNonNull Context context, int i) {
        if (!z1(context, i, "com.google.android.gms")) {
            return false;
        }
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo("com.google.android.gms", 64);
            b.i.a.f.e.f a2 = b.i.a.f.e.f.a(context);
            Objects.requireNonNull(a2);
            if (packageInfo == null) {
                return false;
            }
            if (!b.i.a.f.e.f.d(packageInfo, false)) {
                if (!b.i.a.f.e.f.d(packageInfo, true)) {
                    return false;
                }
                if (!b.i.a.f.e.e.a(a2.f1344b)) {
                    Log.w("GoogleSignatureVerifier", "Test-keys aren't accepted on this build.");
                    return false;
                }
            }
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
            if (Log.isLoggable("UidVerifier", 3)) {
                Log.d("UidVerifier", "Package manager can't find google play services package, defaulting to false");
            }
            return false;
        }
    }

    @NonNull
    public static b.o.a.x.c D1(@NonNull j jVar) {
        return new b.o.a.x.k(jVar, null);
    }

    public static void E(boolean z2) {
        if (!z2) {
            throw new IllegalStateException("no calls to next() since the last call to remove()");
        }
    }

    public static synchronized boolean E0(@RecentlyNonNull Context context) {
        Boolean bool;
        synchronized (f.class) {
            Context applicationContext = context.getApplicationContext();
            Context context2 = a;
            if (!(context2 == null || (bool = f1393b) == null || context2 != applicationContext)) {
                return bool.booleanValue();
            }
            f1393b = null;
            if (A0()) {
                f1393b = Boolean.valueOf(applicationContext.getPackageManager().isInstantApp());
            } else {
                try {
                    context.getClassLoader().loadClass("com.google.android.instantapps.supervisor.InstantAppsRuntime");
                    f1393b = Boolean.TRUE;
                } catch (ClassNotFoundException unused) {
                    f1393b = Boolean.FALSE;
                }
            }
            a = applicationContext;
            return f1393b.booleanValue();
        }
    }

    public static <T> Class<T> E1(Class<T> cls) {
        return cls == Integer.TYPE ? Integer.class : cls == Float.TYPE ? Float.class : cls == Byte.TYPE ? Byte.class : cls == Double.TYPE ? Double.class : cls == Long.TYPE ? Long.class : cls == Character.TYPE ? Character.class : cls == Boolean.TYPE ? Boolean.class : cls == Short.TYPE ? Short.class : cls == Void.TYPE ? Void.class : cls;
    }

    public static void F(boolean z2, @NullableDecl String str, int i) {
        if (!z2) {
            throw new IllegalStateException(I0(str, Integer.valueOf(i)));
        }
    }

    public static final boolean F0(g0.e eVar) {
        m.checkParameterIsNotNull(eVar, "$this$isProbablyUtf8");
        try {
            g0.e eVar2 = new g0.e();
            eVar.n(eVar2, 0L, d0.d0.f.coerceAtMost(eVar.k, 64L));
            for (int i = 0; i < 16; i++) {
                if (eVar2.w()) {
                    return true;
                }
                int I = eVar2.I();
                if (Character.isISOControl(I) && !Character.isWhitespace(I)) {
                    return false;
                }
            }
            return true;
        } catch (EOFException unused) {
            return false;
        }
    }

    public static int F1(int i, int i2, @NullableDecl String str) {
        String str2;
        if (i >= 0 && i < i2) {
            return i;
        }
        if (i < 0) {
            str2 = l2("%s (%s) must not be negative", "index", Integer.valueOf(i));
        } else if (i2 < 0) {
            throw new IllegalArgumentException(b.d.b.a.a.f(26, "negative size: ", i2));
        } else {
            str2 = l2("%s (%s) must be less than size (%s)", "index", Integer.valueOf(i), Integer.valueOf(i2));
        }
        throw new IndexOutOfBoundsException(str2);
    }

    public static void G(boolean z2, @NullableDecl String str, @NullableDecl Object obj) {
        if (!z2) {
            throw new IllegalStateException(I0(str, obj));
        }
    }

    public static final Job G0(CoroutineScope coroutineScope, CoroutineContext coroutineContext, CoroutineStart coroutineStart, Function2<? super CoroutineScope, ? super Continuation<? super Unit>, ? extends Object> function2) {
        s.a.b bVar;
        CoroutineContext a2 = z.a(coroutineScope, coroutineContext);
        Objects.requireNonNull(coroutineStart);
        if (coroutineStart == CoroutineStart.LAZY) {
            bVar = new k1(a2, function2);
        } else {
            bVar = new s1(a2, true);
        }
        bVar.j0(coroutineStart, bVar, function2);
        return bVar;
    }

    public static int G1(int i, byte[] bArr, int i2, int i3, s3 s3Var) throws zzij {
        if ((i >>> 3) != 0) {
            int i4 = i & 7;
            if (i4 == 0) {
                return n2(bArr, i2, s3Var);
            }
            if (i4 == 1) {
                return i2 + 8;
            }
            if (i4 == 2) {
                return O1(bArr, i2, s3Var) + s3Var.a;
            }
            if (i4 == 3) {
                int i5 = (i & (-8)) | 4;
                int i6 = 0;
                while (i2 < i3) {
                    i2 = O1(bArr, i2, s3Var);
                    i6 = s3Var.a;
                    if (i6 == i5) {
                        break;
                    }
                    i2 = G1(i6, bArr, i2, i3, s3Var);
                }
                if (i2 <= i3 && i6 == i5) {
                    return i2;
                }
                throw zzij.d();
            } else if (i4 == 5) {
                return i2 + 4;
            } else {
                throw zzij.c();
            }
        } else {
            throw zzij.c();
        }
    }

    public static int H(long j) {
        int i = (int) j;
        if (((long) i) == j) {
            return i;
        }
        throw new IllegalArgumentException(I0("Out of range: %s", Long.valueOf(j)));
    }

    public static /* synthetic */ Job H0(CoroutineScope coroutineScope, CoroutineContext coroutineContext, CoroutineStart coroutineStart, Function2 function2, int i, Object obj) {
        if ((i & 1) != 0) {
            coroutineContext = d0.w.f.j;
        }
        return G0(coroutineScope, coroutineContext, (i & 2) != 0 ? CoroutineStart.DEFAULT : null, function2);
    }

    public static int H1(int i, byte[] bArr, int i2, int i3, b5<?> b5Var, s3 s3Var) {
        x4 x4Var = (x4) b5Var;
        int O1 = O1(bArr, i2, s3Var);
        x4Var.g(s3Var.a);
        while (O1 < i3) {
            int O12 = O1(bArr, O1, s3Var);
            if (i != s3Var.a) {
                break;
            }
            O1 = O1(bArr, O12, s3Var);
            x4Var.g(s3Var.a);
        }
        return O1;
    }

    public static /* synthetic */ boolean I(s sVar, Throwable th, int i, Object obj) {
        int i2 = i & 1;
        return sVar.j(null);
    }

    public static String I0(@NullableDecl String str, @NullableDecl Object... objArr) {
        int indexOf;
        String str2;
        String valueOf = String.valueOf(str);
        int i = 0;
        for (int i2 = 0; i2 < objArr.length; i2++) {
            Object obj = objArr[i2];
            try {
                str2 = String.valueOf(obj);
            } catch (Exception e2) {
                String str3 = obj.getClass().getName() + MentionUtilsKt.MENTIONS_CHAR + Integer.toHexString(System.identityHashCode(obj));
                Logger.getLogger("com.google.common.base.Strings").log(Level.WARNING, "Exception during lenientFormat for " + str3, (Throwable) e2);
                str2 = "<" + str3 + " threw " + e2.getClass().getName() + ">";
            }
            objArr[i2] = str2;
        }
        StringBuilder sb = new StringBuilder((objArr.length * 16) + valueOf.length());
        int i3 = 0;
        while (i < objArr.length && (indexOf = valueOf.indexOf("%s", i3)) != -1) {
            sb.append((CharSequence) valueOf, i3, indexOf);
            i++;
            sb.append(objArr[i]);
            i3 = indexOf + 2;
        }
        sb.append((CharSequence) valueOf, i3, valueOf.length());
        if (i < objArr.length) {
            sb.append(" [");
            int i4 = i + 1;
            sb.append(objArr[i]);
            while (i4 < objArr.length) {
                sb.append(", ");
                i4++;
                sb.append(objArr[i4]);
            }
            sb.append(']');
        }
        return sb.toString();
    }

    public static int I1(int i, byte[] bArr, int i2, int i3, c7 c7Var, s3 s3Var) throws zzij {
        if ((i >>> 3) != 0) {
            int i4 = i & 7;
            if (i4 == 0) {
                int n2 = n2(bArr, i2, s3Var);
                c7Var.a(i, Long.valueOf(s3Var.f1489b));
                return n2;
            } else if (i4 == 1) {
                c7Var.a(i, Long.valueOf(o2(bArr, i2)));
                return i2 + 8;
            } else if (i4 == 2) {
                int O1 = O1(bArr, i2, s3Var);
                int i5 = s3Var.a;
                if (i5 < 0) {
                    throw zzij.b();
                } else if (i5 <= bArr.length - O1) {
                    if (i5 == 0) {
                        c7Var.a(i, t3.j);
                    } else {
                        c7Var.a(i, t3.h(bArr, O1, i5));
                    }
                    return O1 + i5;
                } else {
                    throw zzij.a();
                }
            } else if (i4 == 3) {
                c7 c2 = c7.c();
                int i6 = (i & (-8)) | 4;
                int i7 = 0;
                while (true) {
                    if (i2 >= i3) {
                        break;
                    }
                    int O12 = O1(bArr, i2, s3Var);
                    int i8 = s3Var.a;
                    if (i8 == i6) {
                        i7 = i8;
                        i2 = O12;
                        break;
                    }
                    i7 = i8;
                    i2 = I1(i8, bArr, O12, i3, c2, s3Var);
                }
                if (i2 > i3 || i7 != i6) {
                    throw zzij.d();
                }
                c7Var.a(i, c2);
                return i2;
            } else if (i4 == 5) {
                c7Var.a(i, Integer.valueOf(N1(bArr, i2)));
                return i2 + 4;
            } else {
                throw zzij.c();
            }
        } else {
            throw zzij.c();
        }
    }

    public static int J(int i, int i2) {
        int i3 = i - i2;
        if (i3 > i2) {
            i3 = i2;
            i2 = i3;
        }
        int i4 = 1;
        int i5 = 1;
        while (i > i2) {
            i4 *= i;
            if (i5 <= i3) {
                i4 /= i5;
                i5++;
            }
            i--;
        }
        while (i5 <= i3) {
            i4 /= i5;
            i5++;
        }
        return i4;
    }

    public static final float[] J0(float[] fArr) {
        m.checkNotNullParameter(fArr, "matrix");
        return (float[]) fArr.clone();
    }

    public static int J1(int i, byte[] bArr, int i2, s3 s3Var) {
        int i3 = i & Opcodes.LAND;
        int i4 = i2 + 1;
        byte b2 = bArr[i2];
        if (b2 >= 0) {
            s3Var.a = i3 | (b2 << 7);
            return i4;
        }
        int i5 = i3 | ((b2 & Byte.MAX_VALUE) << 7);
        int i6 = i4 + 1;
        byte b3 = bArr[i4];
        if (b3 >= 0) {
            s3Var.a = i5 | (b3 << 14);
            return i6;
        }
        int i7 = i5 | ((b3 & Byte.MAX_VALUE) << 14);
        int i8 = i6 + 1;
        byte b4 = bArr[i6];
        if (b4 >= 0) {
            s3Var.a = i7 | (b4 << 21);
            return i8;
        }
        int i9 = i7 | ((b4 & Byte.MAX_VALUE) << 21);
        int i10 = i8 + 1;
        byte b5 = bArr[i8];
        if (b5 >= 0) {
            s3Var.a = i9 | (b5 << 28);
            return i10;
        }
        int i11 = i9 | ((b5 & Byte.MAX_VALUE) << 28);
        while (true) {
            int i12 = i10 + 1;
            if (bArr[i10] >= 0) {
                s3Var.a = i11;
                return i12;
            }
            i10 = i12;
        }
    }

    public static final byte[] K(String str) {
        int i;
        int i2;
        char charAt;
        m.checkParameterIsNotNull(str, "$this$commonAsUtf8ToByteArray");
        byte[] bArr = new byte[str.length() * 4];
        int length = str.length();
        int i3 = 0;
        while (i3 < length) {
            char charAt2 = str.charAt(i3);
            if (charAt2 >= 128) {
                int length2 = str.length();
                int i4 = i3;
                while (i3 < length2) {
                    char charAt3 = str.charAt(i3);
                    if (charAt3 < 128) {
                        int i5 = i4 + 1;
                        bArr[i4] = (byte) charAt3;
                        i3++;
                        while (i3 < length2 && str.charAt(i3) < 128) {
                            i3++;
                            i5++;
                            bArr[i5] = (byte) str.charAt(i3);
                        }
                        i4 = i5;
                    } else {
                        if (charAt3 < 2048) {
                            int i6 = i4 + 1;
                            bArr[i4] = (byte) ((charAt3 >> 6) | Opcodes.CHECKCAST);
                            i = i6 + 1;
                            bArr[i6] = (byte) ((charAt3 & '?') | 128);
                        } else if (55296 > charAt3 || 57343 < charAt3) {
                            int i7 = i4 + 1;
                            bArr[i4] = (byte) ((charAt3 >> '\f') | 224);
                            int i8 = i7 + 1;
                            bArr[i7] = (byte) (((charAt3 >> 6) & 63) | 128);
                            i = i8 + 1;
                            bArr[i8] = (byte) ((charAt3 & '?') | 128);
                        } else if (charAt3 > 56319 || length2 <= (i2 = i3 + 1) || 56320 > (charAt = str.charAt(i2)) || 57343 < charAt) {
                            i = i4 + 1;
                            bArr[i4] = 63;
                        } else {
                            int charAt4 = (str.charAt(i2) + (charAt3 << '\n')) - 56613888;
                            int i9 = i4 + 1;
                            bArr[i4] = (byte) ((charAt4 >> 18) | 240);
                            int i10 = i9 + 1;
                            bArr[i9] = (byte) (((charAt4 >> 12) & 63) | 128);
                            int i11 = i10 + 1;
                            bArr[i10] = (byte) (((charAt4 >> 6) & 63) | 128);
                            i = i11 + 1;
                            bArr[i11] = (byte) ((charAt4 & 63) | 128);
                            i3 += 2;
                            i4 = i;
                        }
                        i3++;
                        i4 = i;
                    }
                }
                byte[] copyOf = Arrays.copyOf(bArr, i4);
                m.checkExpressionValueIsNotNull(copyOf, "java.util.Arrays.copyOf(this, newSize)");
                return copyOf;
            }
            bArr[i3] = (byte) charAt2;
            i3++;
        }
        byte[] copyOf2 = Arrays.copyOf(bArr, str.length());
        m.checkExpressionValueIsNotNull(copyOf2, "java.util.Arrays.copyOf(this, newSize)");
        return copyOf2;
    }

    @NonNull
    public static b.o.a.x.c K0(int i) {
        return D1(new c(i));
    }

    public static int K1(q6<?> q6Var, int i, byte[] bArr, int i2, int i3, b5<?> b5Var, s3 s3Var) throws IOException {
        int M1 = M1(q6Var, bArr, i2, i3, s3Var);
        b5Var.add(s3Var.c);
        while (M1 < i3) {
            int O1 = O1(bArr, M1, s3Var);
            if (i != s3Var.a) {
                break;
            }
            M1 = M1(q6Var, bArr, O1, i3, s3Var);
            b5Var.add(s3Var.c);
        }
        return M1;
    }

    @NonNull
    public static Rect L(@NonNull b.o.a.x.b bVar, @NonNull b.o.a.x.a aVar) {
        int i;
        int i2 = bVar.j;
        int i3 = bVar.k;
        int i4 = 0;
        if (Math.abs(aVar.i() - b.o.a.x.a.f(bVar.j, bVar.k).i()) <= 5.0E-4f) {
            return new Rect(0, 0, i2, i3);
        }
        if (b.o.a.x.a.f(i2, i3).i() > aVar.i()) {
            int round = Math.round(aVar.i() * i3);
            i2 = round;
            i4 = Math.round((i2 - round) / 2.0f);
            i = 0;
        } else {
            int round2 = Math.round(i2 / aVar.i());
            i = Math.round((i3 - round2) / 2.0f);
            i3 = round2;
        }
        return new Rect(i4, i, i2 + i4, i3 + i);
    }

    @NonNull
    public static b.o.a.x.c L0(int i) {
        return D1(new a(i));
    }

    public static int L1(q6 q6Var, byte[] bArr, int i, int i2, int i3, s3 s3Var) throws IOException {
        e6 e6Var = (e6) q6Var;
        Object b2 = e6Var.m.b(e6Var.g);
        int m = e6Var.m(b2, bArr, i, i2, i3, s3Var);
        e6Var.d(b2);
        s3Var.c = b2;
        return m;
    }

    public static final <R> Object M(Function2<? super CoroutineScope, ? super Continuation<? super R>, ? extends Object> function2, Continuation<? super R> continuation) {
        r rVar = new r(continuation.getContext(), continuation);
        Object g1 = g1(rVar, rVar, function2);
        if (g1 == d0.w.h.c.getCOROUTINE_SUSPENDED()) {
            d0.w.i.a.g.probeCoroutineSuspended(continuation);
        }
        return g1;
    }

    @NonNull
    public static b.o.a.x.c M0(int i) {
        return D1(new d(i));
    }

    public static int M1(q6 q6Var, byte[] bArr, int i, int i2, s3 s3Var) throws IOException {
        int i3 = i + 1;
        int i4 = bArr[i];
        if (i4 < 0) {
            i3 = J1(i4, bArr, i3, s3Var);
            i4 = s3Var.a;
        }
        int i5 = i3;
        if (i4 < 0 || i4 > i2 - i5) {
            throw zzij.a();
        }
        Object a2 = q6Var.a();
        int i6 = i4 + i5;
        q6Var.h(a2, bArr, i5, i6, s3Var);
        q6Var.d(a2);
        s3Var.c = a2;
        return i6;
    }

    public static b.i.c.l.d<?> N(String str, String str2) {
        final b.i.c.x.a aVar = new b.i.c.x.a(str, str2);
        d.b a2 = b.i.c.l.d.a(b.i.c.x.e.class);
        a2.d = 1;
        a2.c(new b.i.c.l.f(aVar) { // from class: b.i.c.l.c
            public final Object a;

            {
                this.a = aVar;
            }

            @Override // b.i.c.l.f
            public Object a(e eVar) {
                return this.a;
            }
        });
        return a2.b();
    }

    @NonNull
    public static b.o.a.x.c N0(int i) {
        return D1(new b(i));
    }

    public static int N1(byte[] bArr, int i) {
        return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
    }

    /* JADX WARN: Code restructure failed: missing block: B:63:0x00ea, code lost:
        return null;
     */
    /* JADX WARN: Removed duplicated region for block: B:55:0x00be  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final java.net.InetAddress O(java.lang.String r20, int r21, int r22) {
        /*
            Method dump skipped, instructions count: 259
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.e.o.f.O(java.lang.String, int, int):java.net.InetAddress");
    }

    public static /* synthetic */ void O0(FlexInputViewModel flexInputViewModel, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = true;
        }
        flexInputViewModel.onContentDialogDismissed(z2);
    }

    public static int O1(byte[] bArr, int i, s3 s3Var) {
        int i2 = i + 1;
        byte b2 = bArr[i];
        if (b2 < 0) {
            return J1(b2, bArr, i2, s3Var);
        }
        s3Var.a = b2;
        return i2;
    }

    public static final Object P(long j, Continuation<? super Unit> continuation) {
        if (j <= 0) {
            return Unit.a;
        }
        s.a.l lVar = new s.a.l(d0.w.h.b.intercepted(continuation), 1);
        lVar.A();
        if (j < RecyclerView.FOREVER_NS) {
            i0(lVar.o).c(j, lVar);
        }
        Object u = lVar.u();
        if (u == d0.w.h.c.getCOROUTINE_SUSPENDED()) {
            d0.w.i.a.g.probeCoroutineSuspended(continuation);
        }
        return u;
    }

    public static /* synthetic */ void P0(FlexInputViewModel flexInputViewModel, String str, Boolean bool, int i, Object obj) {
        int i2 = i & 2;
        flexInputViewModel.onInputTextChanged(str, null);
    }

    public static int P1(byte[] bArr, int i, b5<?> b5Var, s3 s3Var) throws IOException {
        x4 x4Var = (x4) b5Var;
        int O1 = O1(bArr, i, s3Var);
        int i2 = s3Var.a + O1;
        while (O1 < i2) {
            O1 = O1(bArr, O1, s3Var);
            x4Var.g(s3Var.a);
        }
        if (O1 == i2) {
            return O1;
        }
        throw zzij.a();
    }

    public static float Q(float f, float f2, float f3, float f4) {
        float f5 = f - f3;
        float f6 = f2 - f4;
        return (float) Math.sqrt((f6 * f6) + (f5 * f5));
    }

    @NonNull
    public static b.o.a.x.c Q0(b.o.a.x.c... cVarArr) {
        return new b.o.a.x.l(cVarArr, null);
    }

    public static <T> z2<T> Q1(z2<T> z2Var) {
        if ((z2Var instanceof c3) || (z2Var instanceof a3)) {
            return z2Var;
        }
        if (z2Var instanceof Serializable) {
            return new a3(z2Var);
        }
        return new c3(z2Var);
    }

    public static float R(int i, int i2, int i3, int i4) {
        int i5 = i - i3;
        int i6 = i2 - i4;
        return (float) Math.sqrt((i6 * i6) + (i5 * i5));
    }

    public static int R0(int i) {
        if (i >= 200 && i <= 299) {
            return 0;
        }
        if (i >= 300 && i <= 399) {
            return 1;
        }
        if (i >= 400 && i <= 499) {
            return 0;
        }
        if (i >= 500) {
        }
        return 1;
    }

    public static FirebaseAppIndexingException R1(@NonNull Status status, String str) {
        String str2 = status.q;
        if (str2 != null && !str2.isEmpty()) {
            str = str2;
        }
        int i = status.p;
        if (i == 17510) {
            return new FirebaseAppIndexingInvalidArgumentException(str);
        }
        if (i == 17511) {
            return new FirebaseAppIndexingTooManyArgumentsException(str);
        }
        if (i == 17602) {
            return new zzh(str);
        }
        switch (i) {
            case 17513:
                return new zzb(str);
            case 17514:
                return new zza(str);
            case 17515:
                return new zzg(str);
            case 17516:
                return new zze(str);
            case 17517:
                return new zzf(str);
            case 17518:
                return new zzd(str);
            case 17519:
                return new zzc(str);
            default:
                return new FirebaseAppIndexingException(str);
        }
    }

    public static int S(Context context, float f) {
        float applyDimension = TypedValue.applyDimension(1, f, context.getResources().getDisplayMetrics());
        int i = (int) (applyDimension + 0.5d);
        if (i != 0 || applyDimension <= 0.0f) {
            return i;
        }
        return 1;
    }

    public static JsonElement S0(JsonReader jsonReader) throws JsonParseException {
        EOFException e2;
        boolean z2;
        try {
            try {
                jsonReader.N();
                z2 = false;
            } catch (EOFException e3) {
                e2 = e3;
                z2 = true;
            }
            try {
                return TypeAdapters.X.read(jsonReader);
            } catch (EOFException e4) {
                e2 = e4;
                if (z2) {
                    return b.i.d.j.a;
                }
                throw new JsonSyntaxException(e2);
            }
        } catch (MalformedJsonException e5) {
            throw new JsonSyntaxException(e5);
        } catch (IOException e6) {
            throw new JsonIOException(e6);
        } catch (NumberFormatException e7) {
            throw new JsonSyntaxException(e7);
        }
    }

    public static <T> T S1(@NonNull Bundle bundle, String str, Class<T> cls, T t) {
        T t2 = (T) bundle.get(str);
        if (t2 == null) {
            return t;
        }
        if (cls.isAssignableFrom(t2.getClass())) {
            return t2;
        }
        throw new IllegalStateException(String.format("Invalid conditional user property field type. '%s' expected [%s] but was [%s]", str, cls.getCanonicalName(), t2.getClass().getCanonicalName()));
    }

    public static float T(float f, float f2, float f3, float f4) {
        float f5 = f / (f4 / 2.0f);
        float f6 = f3 / 2.0f;
        if (f5 < 1.0f) {
            return (f6 * f5 * f5 * f5) + f2;
        }
        float f7 = f5 - 2.0f;
        return (((f7 * f7 * f7) + 2.0f) * f6) + f2;
    }

    public static final Object T0(Object obj, E e2) {
        if (obj == null) {
            return e2;
        }
        if (obj instanceof ArrayList) {
            ((ArrayList) obj).add(e2);
            return obj;
        }
        ArrayList arrayList = new ArrayList(4);
        arrayList.add(obj);
        arrayList.add(e2);
        return arrayList;
    }

    public static <V> V T1(d2<V> d2Var) {
        try {
            return d2Var.a();
        } catch (SecurityException unused) {
            long clearCallingIdentity = Binder.clearCallingIdentity();
            try {
                return d2Var.a();
            } finally {
                Binder.restoreCallingIdentity(clearCallingIdentity);
            }
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x0060  */
    /* JADX WARN: Removed duplicated region for block: B:23:0x0076 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:24:0x0077  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x0082 A[Catch: all -> 0x005d, TryCatch #2 {all -> 0x005d, blocks: (B:13:0x0036, B:18:0x0059, B:25:0x007e, B:27:0x0082, B:29:0x0086, B:35:0x0094, B:36:0x0095, B:37:0x00a0, B:38:0x00a1, B:40:0x00a5, B:43:0x00b8, B:44:0x00c3), top: B:56:0x0022 }] */
    /* JADX WARN: Removed duplicated region for block: B:38:0x00a1 A[Catch: all -> 0x005d, TryCatch #2 {all -> 0x005d, blocks: (B:13:0x0036, B:18:0x0059, B:25:0x007e, B:27:0x0082, B:29:0x0086, B:35:0x0094, B:36:0x0095, B:37:0x00a0, B:38:0x00a1, B:40:0x00a5, B:43:0x00b8, B:44:0x00c3), top: B:56:0x0022 }] */
    /* JADX WARN: Type inference failed for: r10v0, types: [boolean] */
    /* JADX WARN: Type inference failed for: r10v1, types: [s.a.b2.o] */
    /* JADX WARN: Type inference failed for: r10v3 */
    /* JADX WARN: Type inference failed for: r9v1 */
    /* JADX WARN: Type inference failed for: r9v3 */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:41:0x00b5 -> B:14:0x0039). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final <T> java.lang.Object U(s.a.c2.e<? super T> r8, s.a.b2.o<? extends T> r9, boolean r10, kotlin.coroutines.Continuation<? super kotlin.Unit> r11) {
        /*
            Method dump skipped, instructions count: 208
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.e.o.f.U(s.a.c2.e, s.a.b2.o, boolean, kotlin.coroutines.Continuation):java.lang.Object");
    }

    public static long U0(AtomicLong atomicLong, long j) {
        long j2;
        long j3;
        do {
            j2 = atomicLong.get();
            if (j2 == RecyclerView.FOREVER_NS) {
                return RecyclerView.FOREVER_NS;
            }
            j3 = j2 - j;
            if (j3 < 0) {
                throw new IllegalStateException(b.d.b.a.a.s("More produced than requested: ", j3));
            }
        } while (!atomicLong.compareAndSet(j2, j3));
        return j3;
    }

    public static <TResult> TResult U1(@NonNull Task<TResult> task) throws ExecutionException {
        if (task.p()) {
            return task.l();
        }
        if (task.n()) {
            throw new CancellationException("Task is already canceled");
        }
        throw new ExecutionException(task.k());
    }

    public static boolean V(@NullableDecl Object obj, @NullableDecl Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public static RuntimeException V0(Throwable th) {
        if (th instanceof RuntimeException) {
            throw ((RuntimeException) th);
        } else if (th instanceof Error) {
            throw ((Error) th);
        } else {
            throw new RuntimeException(th);
        }
    }

    public static String V1(Context context, String str) {
        Resources resources;
        int identifier;
        try {
            Objects.requireNonNull(context, "null reference");
            resources = context.getResources();
            identifier = resources.getIdentifier(str, "string", resources.getResourcePackageName(R.a.common_google_play_services_unknown_issue));
        } catch (Resources.NotFoundException unused) {
        }
        if (identifier == 0) {
            return null;
        }
        return resources.getString(identifier);
    }

    public static <T> T W(@NullableDecl T t, @NullableDecl T t2) {
        if (t != null) {
            return t;
        }
        Objects.requireNonNull(t2, "Both parameters are null");
        return t2;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Failed to find switch 'out' block
        	at jadx.core.dex.visitors.regions.RegionMaker.processSwitch(RegionMaker.java:817)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:160)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:94)
        	at jadx.core.dex.visitors.regions.RegionMaker.processSwitch(RegionMaker.java:856)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:160)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:94)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:730)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:155)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:94)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:52)
        */
    public static void W0(int r2, h0.a.a.c r3) {
        /*
            int r0 = r2 >>> 24
            if (r0 == 0) goto L25
            r1 = 1
            if (r0 == r1) goto L25
            switch(r0) {
                case 16: goto L1b;
                case 17: goto L1b;
                case 18: goto L1b;
                case 19: goto L13;
                case 20: goto L13;
                case 21: goto L13;
                case 22: goto L25;
                case 23: goto L1b;
                default: goto La;
            }
        La:
            switch(r0) {
                case 66: goto L1b;
                case 67: goto L1b;
                case 68: goto L1b;
                case 69: goto L1b;
                case 70: goto L1b;
                case 71: goto L17;
                case 72: goto L17;
                case 73: goto L17;
                case 74: goto L17;
                case 75: goto L17;
                default: goto Ld;
            }
        Ld:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            r2.<init>()
            throw r2
        L13:
            r3.g(r0)
            goto L2a
        L17:
            r3.i(r2)
            goto L2a
        L1b:
            r1 = 16776960(0xffff00, float:2.3509528E-38)
            r2 = r2 & r1
            int r2 = r2 >> 8
            r3.e(r0, r2)
            goto L2a
        L25:
            int r2 = r2 >>> 16
            r3.j(r2)
        L2a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.e.o.f.W0(int, h0.a.a.c):void");
    }

    public static String W1(t3 t3Var) {
        b7 b7Var = new b7(t3Var);
        StringBuilder sb = new StringBuilder(t3Var.d());
        for (int i = 0; i < b7Var.a.d(); i++) {
            byte c2 = b7Var.a.c(i);
            if (c2 == 34) {
                sb.append("\\\"");
            } else if (c2 == 39) {
                sb.append("\\'");
            } else if (c2 != 92) {
                switch (c2) {
                    case 7:
                        sb.append("\\a");
                        continue;
                    case 8:
                        sb.append("\\b");
                        continue;
                    case 9:
                        sb.append("\\t");
                        continue;
                    case 10:
                        sb.append("\\n");
                        continue;
                    case 11:
                        sb.append("\\v");
                        continue;
                    case 12:
                        sb.append("\\f");
                        continue;
                    case 13:
                        sb.append("\\r");
                        continue;
                    default:
                        if (c2 < 32 || c2 > 126) {
                            sb.append('\\');
                            sb.append((char) (((c2 >>> 6) & 3) + 48));
                            sb.append((char) (((c2 >>> 3) & 7) + 48));
                            sb.append((char) ((c2 & 7) + 48));
                            break;
                        } else {
                            sb.append((char) c2);
                            continue;
                        }
                }
            } else {
                sb.append("\\\\");
            }
        }
        return sb.toString();
    }

    public static final FloatBuffer X(int i) {
        ByteBuffer order = ByteBuffer.allocateDirect(i * 4 * 1).order(ByteOrder.nativeOrder());
        order.limit(order.capacity());
        m.checkNotNullExpressionValue(order, "allocateDirect(size * Egloo.SIZE_OF_BYTE)\n        .order(ByteOrder.nativeOrder())\n        .also { it.limit(it.capacity()) }");
        FloatBuffer asFloatBuffer = order.asFloatBuffer();
        m.checkNotNullExpressionValue(asFloatBuffer, "byteBuffer(size * Egloo.SIZE_OF_FLOAT).asFloatBuffer()");
        return asFloatBuffer;
    }

    public static final <T> Object X0(Object obj, Continuation<? super T> continuation) {
        if (obj instanceof w) {
            k.a aVar = k.j;
            return k.m73constructorimpl(l.createFailure(((w) obj).f3819b));
        }
        k.a aVar2 = k.j;
        return k.m73constructorimpl(obj);
    }

    public static final String X1(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (Character.isUpperCase(charAt)) {
                sb.append("_");
            }
            sb.append(Character.toLowerCase(charAt));
        }
        return sb.toString();
    }

    @NonNull
    public static <TResult> Task<TResult> Y(@RecentlyNonNull Exception exc) {
        c0 c0Var = new c0();
        c0Var.t(exc);
        return c0Var;
    }

    public static final <T> void Y0(j0<? super T> j0Var, Continuation<? super T> continuation, boolean z2) {
        Object obj;
        Object m = j0Var.m();
        Throwable e2 = j0Var.e(m);
        if (e2 != null) {
            k.a aVar = k.j;
            obj = l.createFailure(e2);
        } else {
            k.a aVar2 = k.j;
            obj = j0Var.j(m);
        }
        Object obj2 = k.m73constructorimpl(obj);
        if (z2) {
            Objects.requireNonNull(continuation, "null cannot be cast to non-null type kotlinx.coroutines.internal.DispatchedContinuation<T>");
            s.a.a.g gVar = (s.a.a.g) continuation;
            CoroutineContext context = gVar.getContext();
            Object b2 = s.a.a.a.b(context, gVar.p);
            try {
                gVar.r.resumeWith(obj2);
            } finally {
                s.a.a.a.a(context, b2);
            }
        } else {
            continuation.resumeWith(obj2);
        }
    }

    @Nullable
    public static String Y1(String str, String[] strArr, String[] strArr2) {
        boolean z2;
        int min = Math.min(strArr.length, strArr2.length);
        for (int i = 0; i < min; i++) {
            String str2 = strArr[i];
            if (str == null && str2 == null) {
                z2 = true;
            } else {
                z2 = str == null ? false : str.equals(str2);
            }
            if (z2) {
                return strArr2[i];
            }
        }
        return null;
    }

    @NonNull
    public static <TResult> Task<TResult> Z(@RecentlyNonNull TResult tresult) {
        c0 c0Var = new c0();
        c0Var.s(tresult);
        return c0Var;
    }

    public static int Z0(float f) {
        return (int) (f + (f < 0.0f ? -0.5f : 0.5f));
    }

    @WorkerThread
    public static Set<String> Z1(SQLiteDatabase sQLiteDatabase, String str) {
        HashSet hashSet = new HashSet();
        Cursor rawQuery = sQLiteDatabase.rawQuery(b.d.b.a.a.j(b.d.b.a.a.b(str, 22), "SELECT * FROM ", str, " LIMIT 0"), null);
        try {
            Collections.addAll(hashSet, rawQuery.getColumnNames());
            return hashSet;
        } finally {
            rawQuery.close();
        }
    }

    public static final CancellationException a(String str, Throwable th) {
        CancellationException cancellationException = new CancellationException(str);
        cancellationException.initCause(th);
        return cancellationException;
    }

    public static final String a0(long j) {
        String str;
        if (j <= -999500000) {
            str = ((j - 500000000) / 1000000000) + " s ";
        } else if (j <= -999500) {
            str = ((j - 500000) / 1000000) + " ms";
        } else if (j <= 0) {
            str = ((j - 500) / 1000) + " µs";
        } else if (j < 999500) {
            str = ((j + 500) / 1000) + " µs";
        } else if (j < 999500000) {
            str = ((j + 500000) / 1000000) + " ms";
        } else {
            str = ((j + 500000000) / 1000000000) + " s ";
        }
        String format = String.format("%6s", Arrays.copyOf(new Object[]{str}, 1));
        m.checkExpressionValueIsNotNull(format, "java.lang.String.format(format, *args)");
        return format;
    }

    public static final int a1(u uVar, int i) {
        int i2;
        m.checkParameterIsNotNull(uVar, "$this$segment");
        int[] iArr = uVar.o;
        int i3 = i + 1;
        int length = uVar.n.length;
        m.checkParameterIsNotNull(iArr, "$this$binarySearch");
        int i4 = length - 1;
        int i5 = 0;
        while (true) {
            if (i5 <= i4) {
                i2 = (i5 + i4) >>> 1;
                int i6 = iArr[i2];
                if (i6 >= i3) {
                    if (i6 <= i3) {
                        break;
                    }
                    i4 = i2 - 1;
                } else {
                    i5 = i2 + 1;
                }
            } else {
                i2 = (-i5) - 1;
                break;
            }
        }
        return i2 >= 0 ? i2 : ~i2;
    }

    public static void a2(byte b2, byte b3, byte b4, byte b5, char[] cArr, int i) throws zzij {
        if (!w2(b3)) {
            if ((((b3 + 112) + (b2 << 28)) >> 30) == 0 && !w2(b4) && !w2(b5)) {
                int i2 = ((b2 & 7) << 18) | ((b3 & 63) << 12) | ((b4 & 63) << 6) | (b5 & 63);
                cArr[i] = (char) ((i2 >>> 10) + 55232);
                cArr[i + 1] = (char) ((i2 & AudioAttributesCompat.FLAG_ALL) + 56320);
                return;
            }
        }
        throw zzij.e();
    }

    public static s.a.b2.f b(int i, s.a.b2.e eVar, Function1 function1, int i2) {
        s.a.b2.e eVar2 = s.a.b2.e.SUSPEND;
        boolean z2 = false;
        if ((i2 & 1) != 0) {
            i = 0;
        }
        if ((i2 & 2) != 0) {
            eVar = eVar2;
        }
        int i3 = i2 & 4;
        int i4 = 1;
        if (i == -2) {
            if (eVar == eVar2) {
                Objects.requireNonNull(s.a.b2.f.i);
                i4 = f.a.a;
            }
            return new s.a.b2.d(i4, eVar, null);
        } else if (i == -1) {
            if (eVar == eVar2) {
                z2 = true;
            }
            if (z2) {
                return new s.a.b2.j(null);
            }
            throw new IllegalArgumentException("CONFLATED capacity cannot be used with non-default onBufferOverflow".toString());
        } else if (i != 0) {
            if (i == Integer.MAX_VALUE) {
                return new s.a.b2.k(null);
            }
            if (i == 1 && eVar == s.a.b2.e.DROP_OLDEST) {
                return new s.a.b2.j(null);
            }
            return new s.a.b2.d(i, eVar, null);
        } else if (eVar == eVar2) {
            return new q(null);
        } else {
            return new s.a.b2.d(1, eVar, null);
        }
    }

    public static b.i.c.l.d<?> b0(final String str, final b.i.c.x.g<Context> gVar) {
        d.b a2 = b.i.c.l.d.a(b.i.c.x.e.class);
        a2.d = 1;
        a2.a(new o(Context.class, 1, 0));
        a2.c(new b.i.c.l.f(str, gVar) { // from class: b.i.c.x.f
            public final String a;

            /* renamed from: b  reason: collision with root package name */
            public final g f1801b;

            {
                this.a = str;
                this.f1801b = gVar;
            }

            @Override // b.i.c.l.f
            public Object a(e eVar) {
                return new a(this.a, this.f1801b.a((Context) eVar.a(Context.class)));
            }
        });
        return a2.b();
    }

    public static final v b1(Socket socket) throws IOException {
        m.checkParameterIsNotNull(socket, "$this$sink");
        g0.w wVar = new g0.w(socket);
        OutputStream outputStream = socket.getOutputStream();
        m.checkExpressionValueIsNotNull(outputStream, "getOutputStream()");
        p pVar = new p(outputStream, wVar);
        m.checkParameterIsNotNull(pVar, "sink");
        return new g0.c(wVar, pVar);
    }

    public static void b2(byte b2, byte b3, byte b4, char[] cArr, int i) throws zzij {
        if (w2(b3) || ((b2 == -32 && b3 < -96) || ((b2 == -19 && b3 >= -96) || w2(b4)))) {
            throw zzij.e();
        }
        cArr[i] = (char) (((b2 & 15) << 12) | ((b3 & 63) << 6) | (b4 & 63));
    }

    public static final CoroutineScope c(CoroutineContext coroutineContext) {
        if (coroutineContext.get(Job.h) == null) {
            coroutineContext = coroutineContext.plus(new f1(null));
        }
        return new s.a.a.f(coroutineContext);
    }

    public static long c0(AtomicLong atomicLong, long j) {
        long j2;
        do {
            j2 = atomicLong.get();
        } while (!atomicLong.compareAndSet(j2, f(j2, j)));
        return j2;
    }

    public static int c1(int i) {
        return (int) (Integer.rotateLeft((int) (i * (-862048943)), 15) * 461845907);
    }

    public static void c2(byte b2, byte b3, char[] cArr, int i) throws zzij {
        if (b2 < -62 || w2(b3)) {
            throw zzij.e();
        }
        cArr[i] = (char) (((b2 & 31) << 6) | (b3 & 63));
    }

    public static s.a.u d(Job job, int i) {
        int i2 = i & 1;
        return new t1(null);
    }

    public static float[] d0(RectF rectF) {
        return new float[]{rectF.centerX(), rectF.centerY()};
    }

    public static final x d1(Socket socket) throws IOException {
        m.checkParameterIsNotNull(socket, "$this$source");
        g0.w wVar = new g0.w(socket);
        InputStream inputStream = socket.getInputStream();
        m.checkExpressionValueIsNotNull(inputStream, "getInputStream()");
        n nVar = new n(inputStream, wVar);
        m.checkParameterIsNotNull(nVar, "source");
        return new g0.d(wVar, nVar);
    }

    public static void d2(@NonNull Bundle bundle, @NonNull Object obj) {
        if (obj instanceof Double) {
            bundle.putDouble("value", ((Double) obj).doubleValue());
        } else if (obj instanceof Long) {
            bundle.putLong("value", ((Long) obj).longValue());
        } else {
            bundle.putString("value", obj.toString());
        }
    }

    public static final void e(f0.e0.f.a aVar, f0.e0.f.c cVar, String str) {
        d.b bVar = f0.e0.f.d.c;
        Logger logger = f0.e0.f.d.f3583b;
        StringBuilder sb = new StringBuilder();
        sb.append(cVar.f);
        sb.append(' ');
        String format = String.format("%-22s", Arrays.copyOf(new Object[]{str}, 1));
        m.checkExpressionValueIsNotNull(format, "java.lang.String.format(format, *args)");
        sb.append(format);
        sb.append(": ");
        sb.append(aVar.c);
        logger.fine(sb.toString());
    }

    public static final String e0(Object obj) {
        return obj.getClass().getSimpleName();
    }

    public static final void e1(Continuation<? super Unit> continuation, Continuation<?> continuation2) {
        try {
            Continuation intercepted = d0.w.h.b.intercepted(continuation);
            k.a aVar = k.j;
            s.a.a.h.b(intercepted, k.m73constructorimpl(Unit.a), null, 2);
        } catch (Throwable th) {
            k.a aVar2 = k.j;
            ((s.a.b) continuation2).resumeWith(k.m73constructorimpl(l.createFailure(th)));
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:110:0x024a, code lost:
        r6 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:83:0x01e5, code lost:
        if (((java.lang.Boolean) r4).booleanValue() == false) goto L110;
     */
    /* JADX WARN: Code restructure failed: missing block: B:87:0x01f3, code lost:
        if (((java.lang.Integer) r4).intValue() == 0) goto L110;
     */
    /* JADX WARN: Code restructure failed: missing block: B:91:0x0204, code lost:
        if (((java.lang.Float) r4).floatValue() == 0.0f) goto L110;
     */
    /* JADX WARN: Code restructure failed: missing block: B:95:0x0216, code lost:
        if (((java.lang.Double) r4).doubleValue() == com.google.android.material.shadow.ShadowDrawableWrapper.COS_45) goto L110;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static void e2(b.i.a.f.h.l.c6 r13, java.lang.StringBuilder r14, int r15) {
        /*
            Method dump skipped, instructions count: 685
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.e.o.f.e2(b.i.a.f.h.l.c6, java.lang.StringBuilder, int):void");
    }

    public static long f(long j, long j2) {
        long j3 = j + j2;
        return j3 < 0 ? RecyclerView.FOREVER_NS : j3;
    }

    public static float[] f0(RectF rectF) {
        float f = rectF.left;
        float f2 = rectF.top;
        float f3 = rectF.right;
        float f4 = rectF.bottom;
        return new float[]{f, f2, f3, f2, f3, f4, f, f4};
    }

    public static void f1(Function2 function2, Object obj, Continuation continuation, Function1 function1, int i) {
        int i2 = i & 4;
        try {
            Continuation intercepted = d0.w.h.b.intercepted(d0.w.h.b.createCoroutineUnintercepted(function2, obj, continuation));
            k.a aVar = k.j;
            s.a.a.h.a(intercepted, k.m73constructorimpl(Unit.a), null);
        } catch (Throwable th) {
            k.a aVar2 = k.j;
            continuation.resumeWith(k.m73constructorimpl(l.createFailure(th)));
        }
    }

    public static void f2(q3 q3Var, SQLiteDatabase sQLiteDatabase) {
        if (q3Var != null) {
            File file = new File(sQLiteDatabase.getPath());
            if (!file.setReadable(false, false)) {
                q3Var.i.a("Failed to turn off database read permission");
            }
            if (!file.setWritable(false, false)) {
                q3Var.i.a("Failed to turn off database write permission");
            }
            if (!file.setReadable(true, true)) {
                q3Var.i.a("Failed to turn on database read permission for owner");
            }
            if (!file.setWritable(true, true)) {
                q3Var.i.a("Failed to turn on database write permission for owner");
                return;
            }
            return;
        }
        throw new IllegalArgumentException("Monitor must not be null");
    }

    @NonNull
    public static b.o.a.x.c g(b.o.a.x.c... cVarArr) {
        return new i(cVarArr, null);
    }

    public static String g0(long j) {
        return h0(j, null);
    }

    public static final <T, R> Object g1(r<? super T> rVar, R r, Function2<? super R, ? super Continuation<? super T>, ? extends Object> function2) {
        Object obj;
        Object R;
        rVar.f0();
        try {
        } catch (Throwable th) {
            obj = new w(th, false, 2);
        }
        if (function2 != null) {
            obj = ((Function2) e0.beforeCheckcastToFunctionOfArity(function2, 2)).invoke(r, rVar);
            if (!(obj == d0.w.h.c.getCOROUTINE_SUSPENDED() || (R = rVar.R(obj)) == i1.f3813b)) {
                if (!(R instanceof w)) {
                    return i1.a(R);
                }
                throw ((w) R).f3819b;
            }
            return d0.w.h.c.getCOROUTINE_SUSPENDED();
        }
        throw new NullPointerException("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
    }

    @WorkerThread
    public static void g2(q3 q3Var, SQLiteDatabase sQLiteDatabase, String str, String str2, String str3, String[] strArr) throws SQLiteException {
        boolean z2;
        String[] split;
        if (q3Var != null) {
            Cursor cursor = null;
            try {
                try {
                    cursor = sQLiteDatabase.query("SQLITE_MASTER", new String[]{ModelAuditLogEntry.CHANGE_KEY_NAME}, "name=?", new String[]{str}, null, null, null);
                    z2 = cursor.moveToFirst();
                    cursor.close();
                } catch (Throwable th) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            } catch (SQLiteException e2) {
                q3Var.i.c("Error querying for table", str, e2);
                if (cursor != null) {
                    cursor.close();
                }
                z2 = false;
            }
            if (!z2) {
                sQLiteDatabase.execSQL(str2);
            }
            try {
                Set<String> Z1 = Z1(sQLiteDatabase, str);
                for (String str4 : str3.split(",")) {
                    if (!((HashSet) Z1).remove(str4)) {
                        StringBuilder sb = new StringBuilder(str.length() + 35 + String.valueOf(str4).length());
                        sb.append("Table ");
                        sb.append(str);
                        sb.append(" is missing required column: ");
                        sb.append(str4);
                        throw new SQLiteException(sb.toString());
                    }
                }
                if (strArr != null) {
                    for (int i = 0; i < strArr.length; i += 2) {
                        if (!((HashSet) Z1).remove(strArr[i])) {
                            sQLiteDatabase.execSQL(strArr[i + 1]);
                        }
                    }
                }
                if (!((HashSet) Z1).isEmpty()) {
                    q3Var.i.c("Table has extra columns. table, columns", str, TextUtils.join(", ", Z1));
                }
            } catch (SQLiteException e3) {
                q3Var.f.b("Failed to verify columns on table that was just created", str);
                throw e3;
            }
        } else {
            throw new IllegalArgumentException("Monitor must not be null");
        }
    }

    public static final boolean h(byte[] bArr, int i, byte[] bArr2, int i2, int i3) {
        m.checkParameterIsNotNull(bArr, "a");
        m.checkParameterIsNotNull(bArr2, "b");
        for (int i4 = 0; i4 < i3; i4++) {
            if (bArr[i4 + i] != bArr2[i4 + i2]) {
                return false;
            }
        }
        return true;
    }

    public static String h0(long j, @Nullable SimpleDateFormat simpleDateFormat) {
        Calendar h2 = b.i.a.g.d.l.h();
        Calendar i = b.i.a.g.d.l.i();
        i.setTimeInMillis(j);
        if (simpleDateFormat != null) {
            return simpleDateFormat.format(new Date(j));
        }
        if (h2.get(1) == i.get(1)) {
            return n0(j, Locale.getDefault());
        }
        return s0(j, Locale.getDefault());
    }

    public static int h1(int[] iArr) {
        int i = 0;
        for (int i2 : iArr) {
            i += i2;
        }
        return i;
    }

    public static final void h2(StringBuilder sb, int i, String str, Object obj) {
        if (obj instanceof List) {
            for (Object obj2 : (List) obj) {
                h2(sb, i, str, obj2);
            }
        } else if (obj instanceof Map) {
            for (Map.Entry entry : ((Map) obj).entrySet()) {
                h2(sb, i, str, entry);
            }
        } else {
            sb.append('\n');
            int i2 = 0;
            for (int i3 = 0; i3 < i; i3++) {
                sb.append(' ');
            }
            sb.append(str);
            if (obj instanceof String) {
                sb.append(": \"");
                t3 t3Var = t3.j;
                sb.append(W1(new z3(((String) obj).getBytes(w4.a))));
                sb.append('\"');
            } else if (obj instanceof t3) {
                sb.append(": \"");
                sb.append(W1((t3) obj));
                sb.append('\"');
            } else if (obj instanceof u4) {
                sb.append(" {");
                e2((u4) obj, sb, i + 2);
                sb.append("\n");
                while (i2 < i) {
                    sb.append(' ');
                    i2++;
                }
                sb.append("}");
            } else if (obj instanceof Map.Entry) {
                sb.append(" {");
                Map.Entry entry2 = (Map.Entry) obj;
                int i4 = i + 2;
                h2(sb, i4, "key", entry2.getKey());
                h2(sb, i4, "value", entry2.getValue());
                sb.append("\n");
                while (i2 < i) {
                    sb.append(' ');
                    i2++;
                }
                sb.append("}");
            } else {
                sb.append(": ");
                sb.append(obj.toString());
            }
        }
    }

    public static f0 i(CoroutineScope coroutineScope, CoroutineContext coroutineContext, CoroutineStart coroutineStart, Function2 function2, int i, Object obj) {
        g0 g0Var;
        CoroutineStart coroutineStart2 = null;
        d0.w.f fVar = (i & 1) != 0 ? d0.w.f.j : null;
        if ((i & 2) != 0) {
            coroutineStart2 = CoroutineStart.DEFAULT;
        }
        CoroutineContext a2 = z.a(coroutineScope, fVar);
        Objects.requireNonNull(coroutineStart2);
        if (coroutineStart2 == CoroutineStart.LAZY) {
            g0Var = new j1(a2, function2);
        } else {
            g0Var = new g0(a2, true);
        }
        g0Var.j0(coroutineStart2, g0Var, function2);
        return g0Var;
    }

    public static final h0 i0(CoroutineContext coroutineContext) {
        int i = d0.w.d.e;
        CoroutineContext.Element element = coroutineContext.get(d.b.a);
        if (!(element instanceof h0)) {
            element = null;
        }
        h0 h0Var = (h0) element;
        return h0Var != null ? h0Var : s.a.e0.a;
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0035  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final java.lang.Object i1(java.lang.Exception r4, kotlin.coroutines.Continuation<?> r5) {
        /*
            boolean r0 = r5 instanceof b.i.a.f.e.o.f.h
            if (r0 == 0) goto L13
            r0 = r5
            b.i.a.f.e.o.f$h r0 = (b.i.a.f.e.o.f.h) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            b.i.a.f.e.o.f$h r0 = new b.i.a.f.e.o.f$h
            r0.<init>(r5)
        L18:
            java.lang.Object r5 = r0.result
            java.lang.Object r1 = d0.w.h.c.getCOROUTINE_SUSPENDED()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L35
            if (r2 != r3) goto L2d
            java.lang.Object r4 = r0.L$0
            java.lang.Exception r4 = (java.lang.Exception) r4
            d0.l.throwOnFailure(r5)
            goto L5a
        L2d:
            java.lang.IllegalStateException r4 = new java.lang.IllegalStateException
            java.lang.String r5 = "call to 'resume' before 'invoke' with coroutine"
            r4.<init>(r5)
            throw r4
        L35:
            d0.l.throwOnFailure(r5)
            r0.L$0 = r4
            r0.label = r3
            kotlinx.coroutines.CoroutineDispatcher r5 = s.a.k0.a
            kotlin.coroutines.CoroutineContext r2 = r0.getContext()
            b.i.a.f.e.o.f$g r3 = new b.i.a.f.e.o.f$g
            r3.<init>(r0, r4)
            r5.dispatch(r2, r3)
            java.lang.Object r4 = d0.w.h.c.getCOROUTINE_SUSPENDED()
            java.lang.Object r5 = d0.w.h.c.getCOROUTINE_SUSPENDED()
            if (r4 != r5) goto L57
            d0.w.i.a.g.probeCoroutineSuspended(r0)
        L57:
            if (r4 != r1) goto L5a
            return r1
        L5a:
            kotlin.Unit r4 = kotlin.Unit.a
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.e.o.f.i1(java.lang.Exception, kotlin.coroutines.Continuation):java.lang.Object");
    }

    public static boolean i2(byte b2) {
        return b2 >= 0;
    }

    @RecentlyNonNull
    public static <TResult> TResult j(@RecentlyNonNull Task<TResult> task) throws ExecutionException, InterruptedException {
        b.c.a.a0.d.x("Must not be called on the main application thread");
        b.c.a.a0.d.z(task, "Task must not be null");
        if (task.o()) {
            return (TResult) U1(task);
        }
        b.i.a.f.n.h hVar = new b.i.a.f.n.h(null);
        p2(task, hVar);
        hVar.a.await();
        return (TResult) U1(task);
    }

    public static final int j0(b.p.a.b bVar, @DimenRes int i) {
        m.checkNotNullParameter(bVar, "$this$getDimenPixelSize");
        return bVar.getResources().getDimensionPixelSize(i);
    }

    public static final long j1(String str, long j, long j2, long j3) {
        String k1 = k1(str);
        if (k1 == null) {
            return j;
        }
        Long longOrNull = d0.g0.s.toLongOrNull(k1);
        if (longOrNull != null) {
            long longValue = longOrNull.longValue();
            if (j2 <= longValue && j3 >= longValue) {
                return longValue;
            }
            throw new IllegalStateException(("System property '" + str + "' should be in range " + j2 + ".." + j3 + ", but is '" + longValue + '\'').toString());
        }
        throw new IllegalStateException(("System property '" + str + "' has unrecognized value '" + k1 + '\'').toString());
    }

    public static boolean j2(@NullableDecl Object obj, @NullableDecl Object obj2) {
        if (obj != obj2) {
            return obj != null && obj.equals(obj2);
        }
        return true;
    }

    @RecentlyNonNull
    public static <TResult> TResult k(@RecentlyNonNull Task<TResult> task, long j, @RecentlyNonNull TimeUnit timeUnit) throws ExecutionException, InterruptedException, TimeoutException {
        b.c.a.a0.d.x("Must not be called on the main application thread");
        b.c.a.a0.d.z(task, "Task must not be null");
        b.c.a.a0.d.z(timeUnit, "TimeUnit must not be null");
        if (task.o()) {
            return (TResult) U1(task);
        }
        b.i.a.f.n.h hVar = new b.i.a.f.n.h(null);
        p2(task, hVar);
        if (hVar.a.await(j, timeUnit)) {
            return (TResult) U1(task);
        }
        throw new TimeoutException("Timed out waiting for Task");
    }

    @NullableDecl
    public static <T> T k0(Iterable<? extends T> iterable, @NullableDecl T t) {
        n0 n0Var = new n0((h.a) iterable);
        return n0Var.hasNext() ? (T) n0Var.next() : t;
    }

    public static final String k1(String str) {
        int i = s.a.a.u.a;
        try {
            return System.getProperty(str);
        } catch (SecurityException unused) {
            return null;
        }
    }

    public static int k2(int i) {
        int[] iArr = {1, 2, 3, 4, 5};
        for (int i2 = 0; i2 < 5; i2++) {
            int i3 = iArr[i2];
            int i4 = i3 - 1;
            if (i3 == 0) {
                throw null;
            } else if (i4 == i) {
                return i3;
            }
        }
        return 1;
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0036  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final <T> java.lang.Object l(s.a.f0<? extends T>[] r8, kotlin.coroutines.Continuation<? super java.util.List<? extends T>> r9) {
        /*
            boolean r0 = r9 instanceof b.i.a.f.e.o.f.e
            if (r0 == 0) goto L13
            r0 = r9
            b.i.a.f.e.o.f$e r0 = (b.i.a.f.e.o.f.e) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            b.i.a.f.e.o.f$e r0 = new b.i.a.f.e.o.f$e
            r0.<init>(r9)
        L18:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = d0.w.h.c.getCOROUTINE_SUSPENDED()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L36
            if (r2 != r3) goto L2e
            java.lang.Object r8 = r0.L$0
            s.a.f0[] r8 = (s.a.f0[]) r8
            d0.l.throwOnFailure(r9)
            goto Lac
        L2e:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r9)
            throw r8
        L36:
            d0.l.throwOnFailure(r9)
            int r9 = r8.length
            r2 = 0
            if (r9 != 0) goto L3f
            r9 = 1
            goto L40
        L3f:
            r9 = 0
        L40:
            if (r9 == 0) goto L47
            java.util.List r8 = d0.t.n.emptyList()
            goto Laf
        L47:
            s.a.d r9 = new s.a.d
            r9.<init>(r8)
            r0.L$0 = r8
            r0.label = r3
            s.a.l r4 = new s.a.l
            kotlin.coroutines.Continuation r5 = d0.w.h.b.intercepted(r0)
            r4.<init>(r5, r3)
            r4.A()
            int r8 = r8.length
            s.a.d$a[] r3 = new s.a.d.a[r8]
            r5 = 0
        L60:
            if (r5 >= r8) goto L81
            java.lang.Integer r6 = d0.w.i.a.b.boxInt(r5)
            int r6 = r6.intValue()
            s.a.f0<T>[] r7 = r9.f3809b
            r6 = r7[r6]
            r6.start()
            s.a.d$a r7 = new s.a.d$a
            r7.<init>(r4, r6)
            s.a.m0 r6 = r6.u(r7)
            r7.n = r6
            r3[r5] = r7
            int r5 = r5 + 1
            goto L60
        L81:
            s.a.d$b r5 = new s.a.d$b
            r5.<init>(r9, r3)
        L86:
            if (r2 >= r8) goto L8f
            r9 = r3[r2]
            r9._disposer = r5
            int r2 = r2 + 1
            goto L86
        L8f:
            boolean r8 = r4.v()
            if (r8 == 0) goto L99
            r5.b()
            goto L9c
        L99:
            r4.f(r5)
        L9c:
            java.lang.Object r9 = r4.u()
            java.lang.Object r8 = d0.w.h.c.getCOROUTINE_SUSPENDED()
            if (r9 != r8) goto La9
            d0.w.i.a.g.probeCoroutineSuspended(r0)
        La9:
            if (r9 != r1) goto Lac
            return r1
        Lac:
            r8 = r9
            java.util.List r8 = (java.util.List) r8
        Laf:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.e.o.f.l(s.a.f0[], kotlin.coroutines.Continuation):java.lang.Object");
    }

    public static final String l0(Object obj) {
        return Integer.toHexString(System.identityHashCode(obj));
    }

    public static int l1(String str, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 4) != 0) {
            i2 = 1;
        }
        if ((i4 & 8) != 0) {
            i3 = Integer.MAX_VALUE;
        }
        return (int) j1(str, i, i2, i3);
    }

    public static String l2(String str, Object... objArr) {
        int length;
        int length2;
        int indexOf;
        String sb;
        int i = 0;
        int i2 = 0;
        while (true) {
            length = objArr.length;
            if (i2 >= length) {
                break;
            }
            Object obj = objArr[i2];
            if (obj == null) {
                sb = "null";
            } else {
                try {
                    sb = obj.toString();
                } catch (Exception e2) {
                    String name = obj.getClass().getName();
                    String hexString = Integer.toHexString(System.identityHashCode(obj));
                    StringBuilder sb2 = new StringBuilder(name.length() + 1 + String.valueOf(hexString).length());
                    sb2.append(name);
                    sb2.append(MentionUtilsKt.MENTIONS_CHAR);
                    sb2.append(hexString);
                    String sb3 = sb2.toString();
                    Logger logger = Logger.getLogger("com.google.common.base.Strings");
                    Level level = Level.WARNING;
                    String valueOf = String.valueOf(sb3);
                    logger.logp(level, "com.google.common.base.Strings", "lenientToString", valueOf.length() != 0 ? "Exception during lenientFormat for ".concat(valueOf) : new String("Exception during lenientFormat for "), (Throwable) e2);
                    String name2 = e2.getClass().getName();
                    StringBuilder sb4 = new StringBuilder(String.valueOf(sb3).length() + 9 + name2.length());
                    b.d.b.a.a.q0(sb4, "<", sb3, " threw ", name2);
                    sb4.append(">");
                    sb = sb4.toString();
                }
            }
            objArr[i2] = sb;
            i2++;
        }
        StringBuilder sb5 = new StringBuilder((length * 16) + str.length());
        int i3 = 0;
        while (true) {
            length2 = objArr.length;
            if (i >= length2 || (indexOf = str.indexOf("%s", i3)) == -1) {
                break;
            }
            sb5.append((CharSequence) str, i3, indexOf);
            i++;
            sb5.append(objArr[i]);
            i3 = indexOf + 2;
        }
        sb5.append((CharSequence) str, i3, str.length());
        if (i < length2) {
            sb5.append(" [");
            int i4 = i + 1;
            sb5.append(objArr[i]);
            while (i4 < objArr.length) {
                sb5.append(", ");
                i4++;
                sb5.append(objArr[i4]);
            }
            sb5.append(']');
        }
        return sb5.toString();
    }

    public static String m(int i, int i2, @NullableDecl String str) {
        if (i < 0) {
            return I0("%s (%s) must not be negative", str, Integer.valueOf(i));
        }
        if (i2 >= 0) {
            return I0("%s (%s) must not be greater than size (%s)", str, Integer.valueOf(i), Integer.valueOf(i2));
        }
        throw new IllegalArgumentException(b.d.b.a.a.p("negative size: ", i2));
    }

    @TargetApi(17)
    public static int m0() {
        EGLDisplay eglGetDisplay = EGL14.eglGetDisplay(0);
        int[] iArr = new int[2];
        EGL14.eglInitialize(eglGetDisplay, iArr, 0, iArr, 1);
        EGLConfig[] eGLConfigArr = new EGLConfig[1];
        int[] iArr2 = new int[1];
        EGL14.eglChooseConfig(eglGetDisplay, new int[]{12351, 12430, 12329, 0, 12352, 4, 12339, 1, 12344}, 0, eGLConfigArr, 0, 1, iArr2, 0);
        if (iArr2[0] == 0) {
            return 0;
        }
        EGLConfig eGLConfig = eGLConfigArr[0];
        EGLSurface eglCreatePbufferSurface = EGL14.eglCreatePbufferSurface(eglGetDisplay, eGLConfig, new int[]{12375, 64, 12374, 64, 12344}, 0);
        EGLContext eglCreateContext = EGL14.eglCreateContext(eglGetDisplay, eGLConfig, EGL14.EGL_NO_CONTEXT, new int[]{12440, 2, 12344}, 0);
        EGL14.eglMakeCurrent(eglGetDisplay, eglCreatePbufferSurface, eglCreatePbufferSurface, eglCreateContext);
        int[] iArr3 = new int[1];
        GLES20.glGetIntegerv(3379, iArr3, 0);
        EGLSurface eGLSurface = EGL14.EGL_NO_SURFACE;
        EGL14.eglMakeCurrent(eglGetDisplay, eGLSurface, eGLSurface, EGL14.EGL_NO_CONTEXT);
        EGL14.eglDestroySurface(eglGetDisplay, eglCreatePbufferSurface);
        EGL14.eglDestroyContext(eglGetDisplay, eglCreateContext);
        EGL14.eglTerminate(eglGetDisplay);
        return iArr3[0];
    }

    public static /* synthetic */ long m1(String str, long j, long j2, long j3, int i, Object obj) {
        if ((i & 4) != 0) {
            j2 = 1;
        }
        long j4 = j2;
        if ((i & 8) != 0) {
            j3 = RecyclerView.FOREVER_NS;
        }
        return j1(str, j, j4, j3);
    }

    public static boolean m2(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public static ExecutorService n(String str) {
        ExecutorService unconfigurableExecutorService = Executors.unconfigurableExecutorService(new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue(), new s0(str, new AtomicLong(1L)), new ThreadPoolExecutor.DiscardPolicy()));
        Runtime.getRuntime().addShutdownHook(new Thread(new t0(str, unconfigurableExecutorService, 2L, TimeUnit.SECONDS), b.d.b.a.a.v("Crashlytics Shutdown Hook for ", str)));
        return unconfigurableExecutorService;
    }

    public static String n0(long j, Locale locale) {
        if (Build.VERSION.SDK_INT >= 24) {
            return b.i.a.g.d.l.c("MMMd", locale).format(new Date(j));
        }
        AtomicReference<b.i.a.g.d.k> atomicReference = b.i.a.g.d.l.a;
        DateFormat dateInstance = DateFormat.getDateInstance(2, locale);
        dateInstance.setTimeZone(b.i.a.g.d.l.g());
        SimpleDateFormat simpleDateFormat = (SimpleDateFormat) dateInstance;
        String pattern = simpleDateFormat.toPattern();
        int b2 = b.i.a.g.d.l.b(pattern, "yY", 1, 0);
        if (b2 < pattern.length()) {
            String str = "EMd";
            int b3 = b.i.a.g.d.l.b(pattern, str, 1, b2);
            if (b3 < pattern.length()) {
                str = "EMd,";
            }
            pattern = pattern.replace(pattern.substring(b.i.a.g.d.l.b(pattern, str, -1, b2) + 1, b3), " ").trim();
        }
        simpleDateFormat.applyPattern(pattern);
        return simpleDateFormat.format(new Date(j));
    }

    public static void n1(List<? extends Throwable> list) {
        if (list != null && !list.isEmpty()) {
            if (list.size() == 1) {
                Throwable th = list.get(0);
                if (th instanceof RuntimeException) {
                    throw ((RuntimeException) th);
                } else if (th instanceof Error) {
                    throw ((Error) th);
                } else {
                    throw new RuntimeException(th);
                }
            } else {
                throw new CompositeException(list);
            }
        }
    }

    public static int n2(byte[] bArr, int i, s3 s3Var) {
        byte b2;
        int i2 = i + 1;
        long j = bArr[i];
        if (j >= 0) {
            s3Var.f1489b = j;
            return i2;
        }
        int i3 = i2 + 1;
        byte b3 = bArr[i2];
        long j2 = (j & 127) | ((b3 & Byte.MAX_VALUE) << 7);
        int i4 = 7;
        while (b3 < 0) {
            i3++;
            i4 += 7;
            j2 |= (b2 & Byte.MAX_VALUE) << i4;
            b3 = bArr[i3];
        }
        s3Var.f1489b = j2;
        return i3;
    }

    @NonNull
    @Deprecated
    public static <TResult> Task<TResult> o(@RecentlyNonNull Executor executor, @RecentlyNonNull Callable<TResult> callable) {
        b.c.a.a0.d.z(executor, "Executor must not be null");
        b.c.a.a0.d.z(callable, "Callback must not be null");
        c0 c0Var = new c0();
        executor.execute(new d0(c0Var, callable));
        return c0Var;
    }

    /* JADX WARN: Code restructure failed: missing block: B:15:0x0029, code lost:
        if (r2 == null) goto L26;
     */
    /* JADX WARN: Code restructure failed: missing block: B:16:0x002b, code lost:
        r0 = r2._state;
        r5 = false;
     */
    /* JADX WARN: Code restructure failed: missing block: B:17:0x0030, code lost:
        if ((r0 instanceof s.a.v) == false) goto L21;
     */
    /* JADX WARN: Code restructure failed: missing block: B:19:0x0036, code lost:
        if (((s.a.v) r0).d == null) goto L21;
     */
    /* JADX WARN: Code restructure failed: missing block: B:20:0x0038, code lost:
        r2.q();
     */
    /* JADX WARN: Code restructure failed: missing block: B:21:0x003c, code lost:
        r2._decision = 0;
        r2._state = s.a.c.j;
        r5 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:22:0x0043, code lost:
        if (r5 == false) goto L24;
     */
    /* JADX WARN: Code restructure failed: missing block: B:23:0x0045, code lost:
        r3 = r2;
     */
    /* JADX WARN: Code restructure failed: missing block: B:24:0x0046, code lost:
        if (r3 == null) goto L26;
     */
    /* JADX WARN: Code restructure failed: missing block: B:25:0x0048, code lost:
        return r3;
     */
    /* JADX WARN: Code restructure failed: missing block: B:27:0x004e, code lost:
        return new s.a.l<>(r6, 2);
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final <T> s.a.l<T> o0(kotlin.coroutines.Continuation<? super T> r6) {
        /*
            boolean r0 = r6 instanceof s.a.a.g
            r1 = 2
            if (r0 != 0) goto Lb
            s.a.l r0 = new s.a.l
            r0.<init>(r6, r1)
            return r0
        Lb:
            r0 = r6
            s.a.a.g r0 = (s.a.a.g) r0
        Le:
            java.lang.Object r2 = r0._reusableCancellableContinuation
            r3 = 0
            if (r2 != 0) goto L19
            s.a.a.t r2 = s.a.a.h.f3799b
            r0._reusableCancellableContinuation = r2
            r2 = r3
            goto L29
        L19:
            boolean r4 = r2 instanceof s.a.l
            if (r4 == 0) goto L4f
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r4 = s.a.a.g.m
            s.a.a.t r5 = s.a.a.h.f3799b
            boolean r4 = r4.compareAndSet(r0, r2, r5)
            if (r4 == 0) goto Le
            s.a.l r2 = (s.a.l) r2
        L29:
            if (r2 == 0) goto L49
            java.lang.Object r0 = r2._state
            boolean r4 = r0 instanceof s.a.v
            r5 = 0
            if (r4 == 0) goto L3c
            s.a.v r0 = (s.a.v) r0
            java.lang.Object r0 = r0.d
            if (r0 == 0) goto L3c
            r2.q()
            goto L43
        L3c:
            r2._decision = r5
            s.a.c r0 = s.a.c.j
            r2._state = r0
            r5 = 1
        L43:
            if (r5 == 0) goto L46
            r3 = r2
        L46:
            if (r3 == 0) goto L49
            return r3
        L49:
            s.a.l r0 = new s.a.l
            r0.<init>(r6, r1)
            return r0
        L4f:
            java.lang.String r6 = "Inconsistent state "
            java.lang.String r6 = b.d.b.a.a.u(r6, r2)
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r6 = r6.toString()
            r0.<init>(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.e.o.f.o0(kotlin.coroutines.Continuation):s.a.l");
    }

    public static void o1(Throwable th) {
        if (th instanceof OnErrorNotImplementedException) {
            throw ((OnErrorNotImplementedException) th);
        } else if (th instanceof OnErrorFailedException) {
            throw ((OnErrorFailedException) th);
        } else if (th instanceof OnCompletedFailedException) {
            throw ((OnCompletedFailedException) th);
        } else if (th instanceof VirtualMachineError) {
            throw ((VirtualMachineError) th);
        } else if (th instanceof ThreadDeath) {
            throw ((ThreadDeath) th);
        } else if (th instanceof LinkageError) {
            throw ((LinkageError) th);
        }
    }

    public static long o2(byte[] bArr, int i) {
        return ((bArr[i + 7] & 255) << 56) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16) | ((bArr[i + 3] & 255) << 24) | ((bArr[i + 4] & 255) << 32) | ((bArr[i + 5] & 255) << 40) | ((bArr[i + 6] & 255) << 48);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static final <E> UndeliveredElementException p(Function1<? super E, Unit> function1, E e2, UndeliveredElementException undeliveredElementException) {
        try {
            function1.invoke(e2);
        } catch (Throwable th) {
            if (undeliveredElementException == null || undeliveredElementException.getCause() == th) {
                return new UndeliveredElementException(b.d.b.a.a.u("Exception in undelivered element handler for ", e2), th);
            }
            d0.b.addSuppressed(undeliveredElementException, th);
        }
        return undeliveredElementException;
    }

    public static int p0(int[] iArr, int i, boolean z2) {
        int[] iArr2 = iArr;
        int i2 = 0;
        for (int i3 : iArr2) {
            i2 += i3;
        }
        int length = iArr2.length;
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        while (true) {
            int i7 = length - 1;
            if (i4 >= i7) {
                return i5;
            }
            int i8 = 1 << i4;
            i6 |= i8;
            int i9 = 1;
            while (i9 < iArr2[i4]) {
                int i10 = i2 - i9;
                int i11 = length - i4;
                int i12 = i11 - 2;
                int J = J(i10 - 1, i12);
                if (z2 && i6 == 0) {
                    int i13 = i11 - 1;
                    if (i10 - i13 >= i13) {
                        J -= J(i10 - i11, i12);
                    }
                }
                if (i11 - 1 > 1) {
                    int i14 = 0;
                    for (int i15 = i10 - i12; i15 > i; i15--) {
                        i14 += J((i10 - i15) - 1, i11 - 3);
                    }
                    J -= (i7 - i4) * i14;
                } else if (i10 > i) {
                    J--;
                }
                i5 += J;
                i9++;
                i6 &= ~i8;
                iArr2 = iArr;
            }
            i2 -= i9;
            i4++;
            iArr2 = iArr;
        }
    }

    public static void p1(Throwable th, j0.g<?> gVar, Object obj) {
        o1(th);
        gVar.onError(OnErrorThrowable.a(th, obj));
    }

    public static <T> void p2(Task<T> task, b.i.a.f.n.i<? super T> iVar) {
        Executor executor = b.i.a.f.n.g.f1589b;
        task.g(executor, iVar);
        task.e(executor, iVar);
        task.a(executor, iVar);
    }

    public static /* synthetic */ UndeliveredElementException q(Function1 function1, Object obj, UndeliveredElementException undeliveredElementException, int i) {
        int i2 = i & 2;
        return p(function1, obj, null);
    }

    public static int q0(Context context) {
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        if (defaultDisplay.getWidth() == defaultDisplay.getHeight()) {
            return 3;
        }
        return defaultDisplay.getWidth() < defaultDisplay.getHeight() ? 1 : 2;
    }

    public static int[] q1(Collection<? extends Number> collection) {
        if (collection instanceof b.i.b.c.a) {
            b.i.b.c.a aVar = (b.i.b.c.a) collection;
            return Arrays.copyOfRange(aVar.array, aVar.start, aVar.end);
        }
        Object[] array = collection.toArray();
        int length = array.length;
        int[] iArr = new int[length];
        for (int i = 0; i < length; i++) {
            Object obj = array[i];
            Objects.requireNonNull(obj);
            iArr[i] = ((Number) obj).intValue();
        }
        return iArr;
    }

    public static void q2(boolean z2, @NullableDecl Object obj) {
        if (!z2) {
            throw new IllegalStateException(String.valueOf(obj));
        }
    }

    public static final void r(CoroutineScope coroutineScope, CancellationException cancellationException) {
        Job job = (Job) coroutineScope.getCoroutineContext().get(Job.h);
        if (job != null) {
            job.b(cancellationException);
            return;
        }
        throw new IllegalStateException(("Scope cannot be cancelled because it does not have a job: " + coroutineScope).toString());
    }

    public static DateFormat r0(int i, int i2) {
        String str;
        String str2;
        StringBuilder sb = new StringBuilder();
        if (i == 0) {
            str = "EEEE, MMMM d, yyyy";
        } else if (i == 1) {
            str = "MMMM d, yyyy";
        } else if (i == 2) {
            str = "MMM d, yyyy";
        } else if (i == 3) {
            str = "M/d/yy";
        } else {
            throw new IllegalArgumentException(b.d.b.a.a.p("Unknown DateFormat style: ", i));
        }
        sb.append(str);
        sb.append(" ");
        if (i2 == 0 || i2 == 1) {
            str2 = "h:mm:ss a z";
        } else if (i2 == 2) {
            str2 = "h:mm:ss a";
        } else if (i2 == 3) {
            str2 = "h:mm a";
        } else {
            throw new IllegalArgumentException(b.d.b.a.a.p("Unknown DateFormat style: ", i2));
        }
        sb.append(str2);
        return new SimpleDateFormat(sb.toString(), Locale.US);
    }

    public static final String r1(String str) {
        InetAddress inetAddress;
        m.checkParameterIsNotNull(str, "$this$toCanonicalHost");
        int i = 0;
        int i2 = -1;
        if (d0.g0.w.contains$default((CharSequence) str, (CharSequence) ":", false, 2, (Object) null)) {
            if (!t.startsWith$default(str, "[", false, 2, null) || !t.endsWith$default(str, "]", false, 2, null)) {
                inetAddress = O(str, 0, str.length());
            } else {
                inetAddress = O(str, 1, str.length() - 1);
            }
            if (inetAddress == null) {
                return null;
            }
            byte[] address = inetAddress.getAddress();
            if (address.length == 16) {
                m.checkExpressionValueIsNotNull(address, "address");
                int i3 = 0;
                int i4 = 0;
                while (i3 < address.length) {
                    int i5 = i3;
                    while (i5 < 16 && address[i5] == 0 && address[i5 + 1] == 0) {
                        i5 += 2;
                    }
                    int i6 = i5 - i3;
                    if (i6 > i4 && i6 >= 4) {
                        i2 = i3;
                        i4 = i6;
                    }
                    i3 = i5 + 2;
                }
                g0.e eVar = new g0.e();
                while (i < address.length) {
                    if (i == i2) {
                        eVar.T(58);
                        i += i4;
                        if (i == 16) {
                            eVar.T(58);
                        }
                    } else {
                        if (i > 0) {
                            eVar.T(58);
                        }
                        byte b2 = address[i];
                        byte[] bArr = f0.e0.c.a;
                        eVar.Q(((b2 & 255) << 8) | (address[i + 1] & 255));
                        i += 2;
                    }
                }
                return eVar.D();
            } else if (address.length == 4) {
                return inetAddress.getHostAddress();
            } else {
                throw new AssertionError("Invalid IPv6 address: '" + str + '\'');
            }
        } else {
            try {
                String ascii = IDN.toASCII(str);
                m.checkExpressionValueIsNotNull(ascii, "IDN.toASCII(host)");
                Locale locale = Locale.US;
                m.checkExpressionValueIsNotNull(locale, "Locale.US");
                if (ascii != null) {
                    String lowerCase = ascii.toLowerCase(locale);
                    m.checkExpressionValueIsNotNull(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                    if (lowerCase.length() == 0) {
                        return null;
                    }
                    int length = lowerCase.length();
                    for (int i7 = 0; i7 < length; i7++) {
                        char charAt = lowerCase.charAt(i7);
                        if (charAt > 31 && charAt < 127 && d0.g0.w.indexOf$default((CharSequence) " #%/:?@[\\]", charAt, 0, false, 6, (Object) null) == -1) {
                        }
                        i = 1;
                        break;
                    }
                    if (i != 0) {
                        return null;
                    }
                    return lowerCase;
                }
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            } catch (IllegalArgumentException unused) {
                return null;
            }
        }
    }

    public static int r2(byte[] bArr, int i, s3 s3Var) throws zzij {
        int O1 = O1(bArr, i, s3Var);
        int i2 = s3Var.a;
        if (i2 < 0) {
            throw zzij.b();
        } else if (i2 == 0) {
            s3Var.c = "";
            return O1;
        } else {
            s3Var.c = new String(bArr, O1, i2, w4.a);
            return O1 + i2;
        }
    }

    public static void s(CoroutineContext coroutineContext, CancellationException cancellationException, int i, Object obj) {
        int i2 = i & 1;
        Job job = (Job) coroutineContext.get(Job.h);
        if (job != null) {
            job.b(null);
        }
    }

    public static String s0(long j, Locale locale) {
        if (Build.VERSION.SDK_INT >= 24) {
            return b.i.a.g.d.l.c("yMMMd", locale).format(new Date(j));
        }
        AtomicReference<b.i.a.g.d.k> atomicReference = b.i.a.g.d.l.a;
        DateFormat dateInstance = DateFormat.getDateInstance(2, locale);
        dateInstance.setTimeZone(b.i.a.g.d.l.g());
        return dateInstance.format(new Date(j));
    }

    public static final String s1(Continuation<?> continuation) {
        Object obj;
        if (continuation instanceof s.a.a.g) {
            return continuation.toString();
        }
        try {
            k.a aVar = k.j;
            obj = k.m73constructorimpl(continuation + MentionUtilsKt.MENTIONS_CHAR + l0(continuation));
        } catch (Throwable th) {
            k.a aVar2 = k.j;
            obj = k.m73constructorimpl(l.createFailure(th));
        }
        Throwable th2 = k.m75exceptionOrNullimpl(obj);
        String str = obj;
        if (th2 != null) {
            str = continuation.getClass().getName() + MentionUtilsKt.MENTIONS_CHAR + l0(continuation);
        }
        return (String) str;
    }

    public static void s2(int i, int i2, int i3) {
        String str;
        if (i < 0 || i2 < i || i2 > i3) {
            if (i < 0 || i > i3) {
                str = u2(i, i3, "start index");
            } else {
                str = (i2 < 0 || i2 > i3) ? u2(i2, i3, "end index") : l2("end index (%s) must not be less than start index (%s)", Integer.valueOf(i2), Integer.valueOf(i));
            }
            throw new IndexOutOfBoundsException(str);
        }
    }

    public static /* synthetic */ void t(Job job, CancellationException cancellationException, int i, Object obj) {
        int i2 = i & 1;
        job.b(null);
    }

    public static void t0(@Nullable InputStream inputStream, @NonNull File file) throws IOException {
        Throwable th;
        byte[] bArr = new byte[8192];
        GZIPOutputStream gZIPOutputStream = null;
        try {
            GZIPOutputStream gZIPOutputStream2 = new GZIPOutputStream(new FileOutputStream(file));
            while (true) {
                try {
                    int read = inputStream.read(bArr);
                    if (read > 0) {
                        gZIPOutputStream2.write(bArr, 0, read);
                    } else {
                        gZIPOutputStream2.finish();
                        b.i.c.m.d.k.h.d(gZIPOutputStream2);
                        return;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    gZIPOutputStream = gZIPOutputStream2;
                    b.i.c.m.d.k.h.d(gZIPOutputStream);
                    throw th;
                }
            }
        } catch (Throwable th3) {
            th = th3;
        }
    }

    public static final String t1(byte b2) {
        char[] cArr = g0.z.b.a;
        return new String(new char[]{cArr[(b2 >> 4) & 15], cArr[b2 & 15]});
    }

    public static int t2(byte[] bArr, int i, s3 s3Var) throws zzij {
        int O1 = O1(bArr, i, s3Var);
        int i2 = s3Var.a;
        if (i2 < 0) {
            throw zzij.b();
        } else if (i2 == 0) {
            s3Var.c = "";
            return O1;
        } else {
            s3Var.c = k7.a.c(bArr, O1, i2);
            return O1 + i2;
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static final void u(s.a.b2.o<?> oVar, Throwable th) {
        CancellationException cancellationException = null;
        if (th != null) {
            if (th instanceof CancellationException) {
                cancellationException = th;
            }
            cancellationException = cancellationException;
            if (cancellationException == null) {
                cancellationException = a("Channel was consumed, consumer had failed", th);
            }
        }
        oVar.b(cancellationException);
    }

    public static final void u0(CoroutineContext coroutineContext, Throwable th) {
        try {
            int i = CoroutineExceptionHandler.g;
            CoroutineExceptionHandler coroutineExceptionHandler = (CoroutineExceptionHandler) coroutineContext.get(CoroutineExceptionHandler.a.a);
            if (coroutineExceptionHandler != null) {
                coroutineExceptionHandler.handleException(coroutineContext, th);
            } else {
                b0.a(coroutineContext, th);
            }
        } catch (Throwable th2) {
            if (th != th2) {
                RuntimeException runtimeException = new RuntimeException("Exception while trying to handle coroutine exception", th2);
                d0.b.addSuppressed(runtimeException, th);
                th = runtimeException;
            }
            b0.a(coroutineContext, th);
        }
    }

    public static String u1(String str) {
        int length = str.length();
        int i = 0;
        while (i < length) {
            char charAt = str.charAt(i);
            if (charAt >= 'A' && charAt <= 'Z') {
                char[] charArray = str.toCharArray();
                while (i < length) {
                    char c2 = charArray[i];
                    if (c2 >= 'A' && c2 <= 'Z') {
                        charArray[i] = (char) (c2 ^ ' ');
                    }
                    i++;
                }
                return String.valueOf(charArray);
            }
            i++;
        }
        return str;
    }

    public static String u2(int i, int i2, @NullableDecl String str) {
        if (i < 0) {
            return l2("%s (%s) must not be negative", str, Integer.valueOf(i));
        }
        if (i2 >= 0) {
            return l2("%s (%s) must not be greater than size (%s)", str, Integer.valueOf(i), Integer.valueOf(i2));
        }
        throw new IllegalArgumentException(b.d.b.a.a.f(26, "negative size: ", i2));
    }

    public static void v(boolean z2) {
        if (!z2) {
            throw new IllegalArgumentException();
        }
    }

    public static int v0(long j) {
        return (int) (j ^ (j >>> 32));
    }

    public static final <T> Object v1(Object obj, Function1<? super Throwable, Unit> function1) {
        Throwable th = k.m75exceptionOrNullimpl(obj);
        if (th == null) {
            return function1 != null ? new s.a.x(obj, function1) : obj;
        }
        return new w(th, false, 2);
    }

    public static int v2(byte[] bArr, int i, s3 s3Var) throws zzij {
        int O1 = O1(bArr, i, s3Var);
        int i2 = s3Var.a;
        if (i2 < 0) {
            throw zzij.b();
        } else if (i2 > bArr.length - O1) {
            throw zzij.a();
        } else if (i2 == 0) {
            s3Var.c = t3.j;
            return O1;
        } else {
            s3Var.c = t3.h(bArr, O1, i2);
            return O1 + i2;
        }
    }

    public static void w(boolean z2) {
        if (!z2) {
            throw new IllegalArgumentException();
        }
    }

    public static /* synthetic */ m0 w0(Job job, boolean z2, boolean z3, Function1 function1, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = false;
        }
        if ((i & 2) != 0) {
            z3 = true;
        }
        return job.n(z2, z3, function1);
    }

    public static String w1(String str) {
        int length = str.length();
        int i = 0;
        while (i < length) {
            char charAt = str.charAt(i);
            if (charAt >= 'a' && charAt <= 'z') {
                char[] charArray = str.toCharArray();
                while (i < length) {
                    char c2 = charArray[i];
                    if (c2 >= 'a' && c2 <= 'z') {
                        charArray[i] = (char) (c2 ^ ' ');
                    }
                    i++;
                }
                return String.valueOf(charArray);
            }
            i++;
        }
        return str;
    }

    public static boolean w2(byte b2) {
        return b2 > -65;
    }

    @CanIgnoreReturnValue
    public static int x(int i, int i2) {
        String str;
        if (i >= 0 && i < i2) {
            return i;
        }
        if (i < 0) {
            str = I0("%s (%s) must not be negative", "index", Integer.valueOf(i));
        } else if (i2 >= 0) {
            str = I0("%s (%s) must be less than size (%s)", "index", Integer.valueOf(i), Integer.valueOf(i2));
        } else {
            throw new IllegalArgumentException(b.d.b.a.a.p("negative size: ", i2));
        }
        throw new IndexOutOfBoundsException(str);
    }

    public static m0 x0(long j, Runnable runnable, CoroutineContext coroutineContext) {
        return s.a.e0.a.x(j, runnable, coroutineContext);
    }

    @NonNull
    public static b.o.a.n.o.e x1(@NonNull b.o.a.n.o.e... eVarArr) {
        return new b.o.a.n.o.j(Arrays.asList(eVarArr));
    }

    public static int x2(String str) {
        if (Log.isLoggable("FirebaseAppIndex", 3) ? true : Log.isLoggable("FirebaseAppIndex", 3)) {
            return Log.d("FirebaseAppIndex", str);
        }
        return 0;
    }

    @CanIgnoreReturnValue
    public static Object y(Object obj, int i) {
        if (obj != null) {
            return obj;
        }
        throw new NullPointerException(b.d.b.a.a.p("at index ", i));
    }

    public static final boolean y0(CoroutineScope coroutineScope) {
        Job job = (Job) coroutineScope.getCoroutineContext().get(Job.h);
        if (job != null) {
            return job.a();
        }
        return true;
    }

    public static RectF y1(float[] fArr) {
        RectF rectF = new RectF(Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY, Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY);
        for (int i = 1; i < fArr.length; i += 2) {
            float round = Math.round(fArr[i - 1] * 10.0f) / 10.0f;
            float round2 = Math.round(fArr[i] * 10.0f) / 10.0f;
            float f = rectF.left;
            if (round < f) {
                f = round;
            }
            rectF.left = f;
            float f2 = rectF.top;
            if (round2 < f2) {
                f2 = round2;
            }
            rectF.top = f2;
            float f3 = rectF.right;
            if (round <= f3) {
                round = f3;
            }
            rectF.right = round;
            float f4 = rectF.bottom;
            if (round2 <= f4) {
                round2 = f4;
            }
            rectF.bottom = round2;
        }
        rectF.sort();
        return rectF;
    }

    public static void z(Object obj, Object obj2) {
        if (obj == null) {
            throw new NullPointerException(b.d.b.a.a.u("null key in entry: null=", obj2));
        } else if (obj2 == null) {
            throw new NullPointerException("null value in entry: " + obj + "=null");
        }
    }

    public static final boolean z0(AssertionError assertionError) {
        m.checkParameterIsNotNull(assertionError, "$this$isAndroidGetsocknameError");
        if (assertionError.getCause() == null) {
            return false;
        }
        String message = assertionError.getMessage();
        return message != null ? d0.g0.w.contains$default((CharSequence) message, (CharSequence) "getsockname failed", false, 2, (Object) null) : false;
    }

    @TargetApi(19)
    public static boolean z1(@RecentlyNonNull Context context, int i, @RecentlyNonNull String str) {
        b.i.a.f.e.p.a a2 = b.i.a.f.e.p.b.a(context);
        Objects.requireNonNull(a2);
        try {
            AppOpsManager appOpsManager = (AppOpsManager) a2.a.getSystemService("appops");
            if (appOpsManager != null) {
                appOpsManager.checkPackage(i, str);
                return true;
            }
            throw new NullPointerException("context.getSystemService(Context.APP_OPS_SERVICE) is null");
        } catch (SecurityException unused) {
            return false;
        }
    }
}
