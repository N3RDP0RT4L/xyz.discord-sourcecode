package b.i.a.f.e.o;

import android.os.Process;
import android.os.StrictMode;
import androidx.annotation.RecentlyNullable;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public class g {
    public static String a;

    /* renamed from: b  reason: collision with root package name */
    public static int f1394b;

    @RecentlyNullable
    public static String a() {
        BufferedReader bufferedReader;
        Throwable th;
        if (a == null) {
            if (f1394b == 0) {
                f1394b = Process.myPid();
            }
            int i = f1394b;
            String str = null;
            str = null;
            str = null;
            BufferedReader bufferedReader2 = null;
            str = null;
            try {
                if (i > 0) {
                    try {
                        StringBuilder sb = new StringBuilder(25);
                        sb.append("/proc/");
                        sb.append(i);
                        sb.append("/cmdline");
                        String sb2 = sb.toString();
                        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
                        bufferedReader = new BufferedReader(new FileReader(sb2));
                        StrictMode.setThreadPolicy(allowThreadDiskReads);
                    } catch (IOException unused) {
                        bufferedReader = null;
                    } catch (Throwable th2) {
                        th = th2;
                    }
                    try {
                        String readLine = bufferedReader.readLine();
                        Objects.requireNonNull(readLine, "null reference");
                        str = readLine.trim();
                        bufferedReader.close();
                    } catch (IOException unused2) {
                        if (bufferedReader != null) {
                            bufferedReader.close();
                        }
                        a = str;
                        return a;
                    } catch (Throwable th3) {
                        th = th3;
                        bufferedReader2 = bufferedReader;
                        if (bufferedReader2 != null) {
                            try {
                                bufferedReader2.close();
                            } catch (IOException unused3) {
                            }
                        }
                        throw th;
                    }
                }
            } catch (IOException unused4) {
            }
            a = str;
        }
        return a;
    }
}
