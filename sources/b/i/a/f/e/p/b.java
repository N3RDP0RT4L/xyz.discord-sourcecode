package b.i.a.f.e.p;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.annotation.RecentlyNonNull;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public class b {
    public static b a = new b();
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public a f1396b = null;

    @RecentlyNonNull
    public static a a(@RecentlyNonNull Context context) {
        a aVar;
        b bVar = a;
        synchronized (bVar) {
            if (bVar.f1396b == null) {
                if (context.getApplicationContext() != null) {
                    context = context.getApplicationContext();
                }
                bVar.f1396b = new a(context);
            }
            aVar = bVar.f1396b;
        }
        return aVar;
    }
}
