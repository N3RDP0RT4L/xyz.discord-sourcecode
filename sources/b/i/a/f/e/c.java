package b.i.a.f.e;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.text.TextUtils;
import androidx.annotation.Nullable;
import androidx.annotation.RecentlyNullable;
import b.c.a.a0.d;
import b.d.b.a.a;
import b.i.a.f.e.k.u0;
import b.i.a.f.e.p.b;
import com.discord.models.domain.ModelAuditLogEntry;
import java.util.concurrent.atomic.AtomicBoolean;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public class c {
    public static final int a = 12451000;

    /* renamed from: b  reason: collision with root package name */
    public static final c f1342b = new c();

    static {
        AtomicBoolean atomicBoolean = e.a;
    }

    @RecentlyNullable
    public Intent a(@Nullable Context context, int i, @Nullable String str) {
        if (i == 1 || i == 2) {
            if (context == null || !d.a1(context)) {
                StringBuilder R = a.R("gcore_");
                R.append(a);
                R.append("-");
                if (!TextUtils.isEmpty(str)) {
                    R.append(str);
                }
                R.append("-");
                if (context != null) {
                    R.append(context.getPackageName());
                }
                R.append("-");
                if (context != null) {
                    try {
                        R.append(b.a(context).b(context.getPackageName(), 0).versionCode);
                    } catch (PackageManager.NameNotFoundException unused) {
                    }
                }
                String sb = R.toString();
                Uri uri = u0.a;
                Intent intent = new Intent("android.intent.action.VIEW");
                Uri.Builder appendQueryParameter = Uri.parse("market://details").buildUpon().appendQueryParameter(ModelAuditLogEntry.CHANGE_KEY_ID, "com.google.android.gms");
                if (!TextUtils.isEmpty(sb)) {
                    appendQueryParameter.appendQueryParameter("pcampaignid", sb);
                }
                intent.setData(appendQueryParameter.build());
                intent.setPackage("com.android.vending");
                intent.addFlags(524288);
                return intent;
            }
            Uri uri2 = u0.a;
            Intent intent2 = new Intent("com.google.android.clockwork.home.UPDATE_ANDROID_WEAR_ACTION");
            intent2.setPackage("com.google.android.wearable.app");
            return intent2;
        } else if (i != 3) {
            return null;
        } else {
            Uri uri3 = u0.a;
            Uri fromParts = Uri.fromParts("package", "com.google.android.gms", null);
            Intent intent3 = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
            intent3.setData(fromParts);
            return intent3;
        }
    }

    /* JADX WARN: Can't wrap try/catch for region: R(19:2|(2:88|3)|5|(2:9|(2:11|(2:13|14))(2:15|16))|17|(4:19|(3:21|(1:26)(1:25)|27)|28|(13:30|(1:33)(1:34)|35|(2:84|37)(1:39)|40|86|41|42|(1:44)(2:(2:46|(1:48))|(7:55|(1:57)(1:58)|(1:60)|(1:62)(4:63|(2:82|65)|68|(1:70)(1:71))|74|(1:76)(1:(1:78))|(1:80)(1:81))(1:53))|54|74|(0)(0)|(0)(0)))|31|(0)(0)|35|(0)(0)|40|86|41|42|(0)(0)|54|74|(0)(0)|(0)(0)) */
    /* JADX WARN: Code restructure failed: missing block: B:72:0x015f, code lost:
        android.util.Log.w("GooglePlayServicesUtil", java.lang.String.valueOf(r5).concat(" requires Google Play services, but they are missing."));
     */
    /* JADX WARN: Removed duplicated region for block: B:33:0x007c  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x007e  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x00a3  */
    /* JADX WARN: Removed duplicated region for block: B:44:0x00b3  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x00c1  */
    /* JADX WARN: Removed duplicated region for block: B:76:0x0171  */
    /* JADX WARN: Removed duplicated region for block: B:77:0x0173  */
    /* JADX WARN: Removed duplicated region for block: B:80:0x017b A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:81:0x017c A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:84:0x008c A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public int b(@androidx.annotation.RecentlyNonNull android.content.Context r11, int r12) {
        /*
            Method dump skipped, instructions count: 381
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.e.c.b(android.content.Context, int):int");
    }
}
