package b.i.a.f.e;

import java.lang.ref.WeakReference;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public abstract class z extends x {
    public static final WeakReference<byte[]> c = new WeakReference<>(null);
    public WeakReference<byte[]> d = c;

    public z(byte[] bArr) {
        super(bArr);
    }

    @Override // b.i.a.f.e.x
    public final byte[] g() {
        byte[] bArr;
        synchronized (this) {
            bArr = this.d.get();
            if (bArr == null) {
                bArr = t0();
                this.d = new WeakReference<>(bArr);
            }
        }
        return bArr;
    }

    public abstract byte[] t0();
}
