package b.i.a.f.e.h;

import android.os.Looper;
import androidx.annotation.NonNull;
import b.i.a.f.e.h.a;
import b.i.a.f.e.h.j.d;
import b.i.a.f.e.h.j.f;
import b.i.a.f.e.h.j.l;
import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
@Deprecated
/* loaded from: classes3.dex */
public abstract class c {
    public static final Set<c> a = Collections.newSetFromMap(new WeakHashMap());

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    @Deprecated
    /* loaded from: classes3.dex */
    public interface a extends f {
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    @Deprecated
    /* loaded from: classes3.dex */
    public interface b extends l {
    }

    public <A extends a.b, R extends h, T extends d<R, A>> T a(@NonNull T t) {
        throw new UnsupportedOperationException();
    }

    public <A extends a.b, T extends d<? extends h, A>> T b(@NonNull T t) {
        throw new UnsupportedOperationException();
    }

    public Looper c() {
        throw new UnsupportedOperationException();
    }
}
