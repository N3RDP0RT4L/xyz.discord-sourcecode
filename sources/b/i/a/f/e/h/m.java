package b.i.a.f.e.h;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Status;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public final class m implements Parcelable.Creator<Status> {
    @Override // android.os.Parcelable.Creator
    public final Status createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        String str = null;
        PendingIntent pendingIntent = null;
        ConnectionResult connectionResult = null;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                i2 = d.G1(parcel, readInt);
            } else if (c == 2) {
                str = d.R(parcel, readInt);
            } else if (c == 3) {
                pendingIntent = (PendingIntent) d.Q(parcel, readInt, PendingIntent.CREATOR);
            } else if (c == 4) {
                connectionResult = (ConnectionResult) d.Q(parcel, readInt, ConnectionResult.CREATOR);
            } else if (c != 1000) {
                d.d2(parcel, readInt);
            } else {
                i = d.G1(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        return new Status(i, i2, str, pendingIntent, connectionResult);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Status[] newArray(int i) {
        return new Status[i];
    }
}
