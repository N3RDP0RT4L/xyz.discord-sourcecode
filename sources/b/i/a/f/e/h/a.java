package b.i.a.f.e.h;

import android.accounts.Account;
import android.content.Context;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.a.f.e.h.a.d;
import b.i.a.f.e.h.c;
import b.i.a.f.e.h.j.l;
import b.i.a.f.e.k.b;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.Scope;
import java.util.Set;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public final class a<O extends d> {
    @Nullable
    public final AbstractC0111a<?, O> a;
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public final g<?> f1346b;
    public final String c;

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    /* renamed from: b.i.a.f.e.h.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static abstract class AbstractC0111a<T extends f, O> extends e<T, O> {
        @Deprecated
        public T a(Context context, Looper looper, b.i.a.f.e.k.c cVar, O o, c.a aVar, c.b bVar) {
            return b(context, looper, cVar, o, aVar, bVar);
        }

        public T b(Context context, Looper looper, b.i.a.f.e.k.c cVar, O o, b.i.a.f.e.h.j.f fVar, l lVar) {
            throw new UnsupportedOperationException("buildClient must be implemented");
        }
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    /* loaded from: classes3.dex */
    public interface b {
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    /* loaded from: classes3.dex */
    public static class c<C extends b> {
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    /* loaded from: classes3.dex */
    public interface d {

        /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
        /* renamed from: b.i.a.f.e.h.a$d$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public interface AbstractC0112a extends d {
            Account T();
        }

        /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
        /* loaded from: classes3.dex */
        public interface b extends d {
            GoogleSignInAccount L();
        }

        /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
        /* loaded from: classes3.dex */
        public static final class c implements d {
        }
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    /* loaded from: classes3.dex */
    public static abstract class e<T extends b, O> {
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    /* loaded from: classes3.dex */
    public interface f extends b {
        @NonNull
        Set<Scope> a();

        void b(@Nullable b.i.a.f.e.k.g gVar, @Nullable Set<Scope> set);

        void c(String str);

        boolean e();

        String f();

        void g(b.c cVar);

        void h();

        void i(b.e eVar);

        boolean j();

        boolean k();

        int l();

        Feature[] m();

        @Nullable
        String n();

        boolean o();
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    /* loaded from: classes3.dex */
    public static final class g<C extends f> extends c<C> {
    }

    /* JADX WARN: Multi-variable type inference failed */
    public <C extends f> a(String str, AbstractC0111a<C, O> aVar, g<C> gVar) {
        b.c.a.a0.d.z(aVar, "Cannot construct an Api with a null ClientBuilder");
        b.c.a.a0.d.z(gVar, "Cannot construct an Api with a null ClientKey");
        this.c = str;
        this.a = aVar;
        this.f1346b = gVar;
    }
}
