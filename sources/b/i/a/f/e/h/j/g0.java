package b.i.a.f.e.h.j;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import b.i.a.f.e.h.a;
import b.i.a.f.e.h.c;
import b.i.a.f.e.h.j.g;
import b.i.a.f.l.b.d;
import b.i.a.f.l.f;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Scope;
import java.util.Set;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public final class g0 extends d implements c.a, c.b {
    public static a.AbstractC0111a<? extends f, b.i.a.f.l.a> a = b.i.a.f.l.c.c;

    /* renamed from: b  reason: collision with root package name */
    public final Context f1360b;
    public final Handler c;
    public final a.AbstractC0111a<? extends f, b.i.a.f.l.a> d;
    public Set<Scope> e;
    public b.i.a.f.e.k.c f;
    public f g;
    public h0 h;

    @WorkerThread
    public g0(Context context, Handler handler, @NonNull b.i.a.f.e.k.c cVar) {
        a.AbstractC0111a<? extends f, b.i.a.f.l.a> aVar = a;
        this.f1360b = context;
        this.c = handler;
        b.c.a.a0.d.z(cVar, "ClientSettings must not be null");
        this.f = cVar;
        this.e = cVar.f1376b;
        this.d = aVar;
    }

    @Override // b.i.a.f.e.h.j.f
    @WorkerThread
    public final void c(int i) {
        this.g.h();
    }

    @Override // b.i.a.f.e.h.j.l
    @WorkerThread
    public final void g(@NonNull ConnectionResult connectionResult) {
        ((g.b) this.h).b(connectionResult);
    }

    @Override // b.i.a.f.e.h.j.f
    @WorkerThread
    public final void i(@Nullable Bundle bundle) {
        this.g.d(this);
    }
}
