package b.i.a.f.e.h.j;

import com.google.android.gms.common.ConnectionResult;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public final class s0 {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final ConnectionResult f1368b;

    public s0(ConnectionResult connectionResult, int i) {
        Objects.requireNonNull(connectionResult, "null reference");
        this.f1368b = connectionResult;
        this.a = i;
    }
}
