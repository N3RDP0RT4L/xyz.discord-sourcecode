package b.i.a.f.e.h.j;

import androidx.annotation.NonNull;
import b.i.a.f.n.c;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public final class y0 implements c<TResult> {
    public final /* synthetic */ TaskCompletionSource a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ x0 f1372b;

    public y0(x0 x0Var, TaskCompletionSource taskCompletionSource) {
        this.f1372b = x0Var;
        this.a = taskCompletionSource;
    }

    @Override // b.i.a.f.n.c
    public final void onComplete(@NonNull Task<TResult> task) {
        this.f1372b.f1370b.remove(this.a);
    }
}
