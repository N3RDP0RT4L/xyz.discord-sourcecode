package b.i.a.f.e.h.j;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import androidx.collection.ArrayMap;
import androidx.collection.ArraySet;
import b.c.a.a0.d;
import b.i.a.f.e.h.a;
import b.i.a.f.e.h.c;
import b.i.a.f.e.h.j.k;
import b.i.a.f.e.k.b;
import b.i.a.f.e.k.j;
import b.i.a.f.e.k.r;
import b.i.a.f.e.k.y;
import b.i.a.f.j.b.e.i;
import b.i.a.f.j.b.e.v;
import b.i.a.f.l.f;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiActivity;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.UnsupportedApiCallException;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import org.checkerframework.checker.initialization.qual.NotOnlyInitialized;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public class g implements Handler.Callback {
    public static final Status j = new Status(4, "Sign-out occurred while this API call was in progress.");
    public static final Status k = new Status(4, "The user must be signed in to make this API call.");
    public static final Object l = new Object();
    @Nullable
    public static g m;
    public final Context o;
    public final GoogleApiAvailability p;
    public final r q;
    @NotOnlyInitialized

    /* renamed from: x  reason: collision with root package name */
    public final Handler f1355x;

    /* renamed from: y  reason: collision with root package name */
    public volatile boolean f1356y;
    public long n = 10000;
    public final AtomicInteger r = new AtomicInteger(1);

    /* renamed from: s  reason: collision with root package name */
    public final AtomicInteger f1354s = new AtomicInteger(0);
    public final Map<b.i.a.f.e.h.j.b<?>, a<?>> t = new ConcurrentHashMap(5, 0.75f, 1);
    @Nullable
    public a1 u = null;
    public final Set<b.i.a.f.e.h.j.b<?>> v = new ArraySet();
    public final Set<b.i.a.f.e.h.j.b<?>> w = new ArraySet();

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    /* loaded from: classes3.dex */
    public class a<O extends a.d> implements c.a, c.b {
        @NotOnlyInitialized

        /* renamed from: b  reason: collision with root package name */
        public final a.f f1357b;
        public final a.b c;
        public final b.i.a.f.e.h.j.b<O> d;
        public final x0 e;
        public final int h;
        @Nullable
        public final g0 i;
        public boolean j;
        public final Queue<s> a = new LinkedList();
        public final Set<r0> f = new HashSet();
        public final Map<k.a<?>, d0> g = new HashMap();
        public final List<c> k = new ArrayList();
        @Nullable
        public ConnectionResult l = null;

        /* JADX WARN: Type inference failed for: r1v4, types: [b.i.a.f.e.h.a$b, b.i.a.f.e.h.a$f] */
        @WorkerThread
        public a(b.i.a.f.e.h.b<O> bVar) {
            Looper looper = g.this.f1355x.getLooper();
            b.i.a.f.e.k.c a = bVar.a().a();
            b.i.a.f.e.h.a<O> aVar = bVar.f1347b;
            d.G(aVar.a != null, "This API was constructed with a SimpleClientBuilder. Use getSimpleClientBuilder");
            a.AbstractC0111a<?, O> aVar2 = aVar.a;
            Objects.requireNonNull(aVar2, "null reference");
            ?? a2 = aVar2.a(bVar.a, looper, a, bVar.c, this, this);
            this.f1357b = a2;
            if (!(a2 instanceof y)) {
                this.c = a2;
                this.d = bVar.d;
                this.e = new x0();
                this.h = bVar.f;
                if (a2.o()) {
                    this.i = new g0(g.this.o, g.this.f1355x, bVar.a().a());
                } else {
                    this.i = null;
                }
            } else {
                y yVar = (y) a2;
                throw new NoSuchMethodError();
            }
        }

        @Nullable
        @WorkerThread
        public final Feature a(@Nullable Feature[] featureArr) {
            if (!(featureArr == null || featureArr.length == 0)) {
                Feature[] m = this.f1357b.m();
                if (m == null) {
                    m = new Feature[0];
                }
                ArrayMap arrayMap = new ArrayMap(m.length);
                for (Feature feature : m) {
                    arrayMap.put(feature.j, Long.valueOf(feature.w0()));
                }
                for (Feature feature2 : featureArr) {
                    Long l = (Long) arrayMap.get(feature2.j);
                    if (l == null || l.longValue() < feature2.w0()) {
                        return feature2;
                    }
                }
            }
            return null;
        }

        @WorkerThread
        public final void b() {
            d.s(g.this.f1355x);
            Status status = g.j;
            f(status);
            x0 x0Var = this.e;
            Objects.requireNonNull(x0Var);
            x0Var.a(false, status);
            for (k.a aVar : (k.a[]) this.g.keySet().toArray(new k.a[0])) {
                j(new p0(aVar, new TaskCompletionSource()));
            }
            n(new ConnectionResult(4));
            if (this.f1357b.j()) {
                this.f1357b.i(new x(this));
            }
        }

        @Override // b.i.a.f.e.h.j.f
        public final void c(int i) {
            if (Looper.myLooper() == g.this.f1355x.getLooper()) {
                d(i);
            } else {
                g.this.f1355x.post(new v(this, i));
            }
        }

        @WorkerThread
        public final void d(int i) {
            q();
            this.j = true;
            x0 x0Var = this.e;
            String n = this.f1357b.n();
            Objects.requireNonNull(x0Var);
            StringBuilder sb = new StringBuilder("The connection to Google Play services was lost");
            if (i == 1) {
                sb.append(" due to service disconnection.");
            } else if (i == 3) {
                sb.append(" due to dead object exception.");
            }
            if (n != null) {
                sb.append(" Last reason for disconnect: ");
                sb.append(n);
            }
            x0Var.a(true, new Status(20, sb.toString()));
            Handler handler = g.this.f1355x;
            Message obtain = Message.obtain(handler, 9, this.d);
            Objects.requireNonNull(g.this);
            handler.sendMessageDelayed(obtain, 5000L);
            Handler handler2 = g.this.f1355x;
            Message obtain2 = Message.obtain(handler2, 11, this.d);
            Objects.requireNonNull(g.this);
            handler2.sendMessageDelayed(obtain2, 120000L);
            g.this.q.a.clear();
            for (d0 d0Var : this.g.values()) {
                d0Var.c.run();
            }
        }

        @WorkerThread
        public final void e(@NonNull ConnectionResult connectionResult, @Nullable Exception exc) {
            f fVar;
            d.s(g.this.f1355x);
            g0 g0Var = this.i;
            if (!(g0Var == null || (fVar = g0Var.g) == null)) {
                fVar.h();
            }
            q();
            g.this.q.a.clear();
            n(connectionResult);
            if (connectionResult.l == 4) {
                f(g.k);
            } else if (this.a.isEmpty()) {
                this.l = connectionResult;
            } else if (exc != null) {
                d.s(g.this.f1355x);
                h(null, exc, false);
            } else if (!g.this.f1356y) {
                Status p = p(connectionResult);
                d.s(g.this.f1355x);
                h(p, null, false);
            } else {
                h(p(connectionResult), null, true);
                if (!this.a.isEmpty() && !l(connectionResult) && !g.this.c(connectionResult, this.h)) {
                    if (connectionResult.l == 18) {
                        this.j = true;
                    }
                    if (this.j) {
                        Handler handler = g.this.f1355x;
                        Message obtain = Message.obtain(handler, 9, this.d);
                        Objects.requireNonNull(g.this);
                        handler.sendMessageDelayed(obtain, 5000L);
                        return;
                    }
                    Status p2 = p(connectionResult);
                    d.s(g.this.f1355x);
                    h(p2, null, false);
                }
            }
        }

        @WorkerThread
        public final void f(Status status) {
            d.s(g.this.f1355x);
            h(status, null, false);
        }

        @Override // b.i.a.f.e.h.j.l
        @WorkerThread
        public final void g(@NonNull ConnectionResult connectionResult) {
            e(connectionResult, null);
        }

        @WorkerThread
        public final void h(@Nullable Status status, @Nullable Exception exc, boolean z2) {
            d.s(g.this.f1355x);
            boolean z3 = true;
            boolean z4 = status == null;
            if (exc != null) {
                z3 = false;
            }
            if (z4 != z3) {
                Iterator<s> it = this.a.iterator();
                while (it.hasNext()) {
                    s next = it.next();
                    if (!z2 || next.a == 2) {
                        if (status != null) {
                            next.b(status);
                        } else {
                            next.e(exc);
                        }
                        it.remove();
                    }
                }
                return;
            }
            throw new IllegalArgumentException("Status XOR exception should be null");
        }

        @Override // b.i.a.f.e.h.j.f
        public final void i(@Nullable Bundle bundle) {
            if (Looper.myLooper() == g.this.f1355x.getLooper()) {
                t();
            } else {
                g.this.f1355x.post(new u(this));
            }
        }

        @WorkerThread
        public final void j(s sVar) {
            d.s(g.this.f1355x);
            if (!this.f1357b.j()) {
                this.a.add(sVar);
                ConnectionResult connectionResult = this.l;
                if (connectionResult == null || !connectionResult.w0()) {
                    r();
                } else {
                    e(this.l, null);
                }
            } else if (m(sVar)) {
                w();
            } else {
                this.a.add(sVar);
            }
        }

        @WorkerThread
        public final boolean k(boolean z2) {
            d.s(g.this.f1355x);
            if (!this.f1357b.j() || this.g.size() != 0) {
                return false;
            }
            x0 x0Var = this.e;
            if (!x0Var.a.isEmpty() || !x0Var.f1370b.isEmpty()) {
                if (z2) {
                    w();
                }
                return false;
            }
            this.f1357b.c("Timing out service connection.");
            return true;
        }

        @WorkerThread
        public final boolean l(@NonNull ConnectionResult connectionResult) {
            synchronized (g.l) {
                g gVar = g.this;
                if (gVar.u == null || !gVar.v.contains(this.d)) {
                    return false;
                }
                g.this.u.n(connectionResult, this.h);
                return true;
            }
        }

        @WorkerThread
        public final boolean m(s sVar) {
            if (!(sVar instanceof m0)) {
                o(sVar);
                return true;
            }
            m0 m0Var = (m0) sVar;
            Feature a = a(m0Var.f(this));
            if (a == null) {
                o(sVar);
                return true;
            }
            String name = this.c.getClass().getName();
            String str = a.j;
            long w0 = a.w0();
            StringBuilder Q = b.d.b.a.a.Q(b.d.b.a.a.b(str, name.length() + 77), name, " could not execute call because it requires feature (", str, ", ");
            Q.append(w0);
            Q.append(").");
            Log.w("GoogleApiManager", Q.toString());
            if (!g.this.f1356y || !m0Var.g(this)) {
                m0Var.e(new UnsupportedApiCallException(a));
                return true;
            }
            c cVar = new c(this.d, a, null);
            int indexOf = this.k.indexOf(cVar);
            if (indexOf >= 0) {
                c cVar2 = this.k.get(indexOf);
                g.this.f1355x.removeMessages(15, cVar2);
                Handler handler = g.this.f1355x;
                Message obtain = Message.obtain(handler, 15, cVar2);
                Objects.requireNonNull(g.this);
                handler.sendMessageDelayed(obtain, 5000L);
                return false;
            }
            this.k.add(cVar);
            Handler handler2 = g.this.f1355x;
            Message obtain2 = Message.obtain(handler2, 15, cVar);
            Objects.requireNonNull(g.this);
            handler2.sendMessageDelayed(obtain2, 5000L);
            Handler handler3 = g.this.f1355x;
            Message obtain3 = Message.obtain(handler3, 16, cVar);
            Objects.requireNonNull(g.this);
            handler3.sendMessageDelayed(obtain3, 120000L);
            ConnectionResult connectionResult = new ConnectionResult(2, null);
            if (l(connectionResult)) {
                return false;
            }
            g.this.c(connectionResult, this.h);
            return false;
        }

        @WorkerThread
        public final void n(ConnectionResult connectionResult) {
            Iterator<r0> it = this.f.iterator();
            if (it.hasNext()) {
                r0 next = it.next();
                if (d.h0(connectionResult, ConnectionResult.j)) {
                    this.f1357b.f();
                }
                Objects.requireNonNull(next);
                throw null;
            }
            this.f.clear();
        }

        @WorkerThread
        public final void o(s sVar) {
            sVar.d(this.e, s());
            try {
                sVar.c(this);
            } catch (DeadObjectException unused) {
                c(1);
                this.f1357b.c("DeadObjectException thrown while running ApiCallRunner.");
            } catch (Throwable th) {
                throw new IllegalStateException(String.format("Error in GoogleApi implementation for client %s.", this.c.getClass().getName()), th);
            }
        }

        public final Status p(ConnectionResult connectionResult) {
            String str = this.d.f1350b.c;
            String valueOf = String.valueOf(connectionResult);
            return new Status(17, b.d.b.a.a.k(valueOf.length() + b.d.b.a.a.b(str, 63), "API: ", str, " is not available on this device. Connection failed with: ", valueOf));
        }

        @WorkerThread
        public final void q() {
            d.s(g.this.f1355x);
            this.l = null;
        }

        @WorkerThread
        public final void r() {
            d.s(g.this.f1355x);
            if (!this.f1357b.j() && !this.f1357b.e()) {
                try {
                    g gVar = g.this;
                    int a = gVar.q.a(gVar.o, this.f1357b);
                    if (a != 0) {
                        ConnectionResult connectionResult = new ConnectionResult(a, null);
                        String name = this.c.getClass().getName();
                        String valueOf = String.valueOf(connectionResult);
                        StringBuilder sb = new StringBuilder(name.length() + 35 + valueOf.length());
                        sb.append("The service for ");
                        sb.append(name);
                        sb.append(" is not available: ");
                        sb.append(valueOf);
                        Log.w("GoogleApiManager", sb.toString());
                        e(connectionResult, null);
                        return;
                    }
                    g gVar2 = g.this;
                    a.f fVar = this.f1357b;
                    b bVar = new b(fVar, this.d);
                    if (fVar.o()) {
                        g0 g0Var = this.i;
                        Objects.requireNonNull(g0Var, "null reference");
                        f fVar2 = g0Var.g;
                        if (fVar2 != null) {
                            fVar2.h();
                        }
                        g0Var.f.h = Integer.valueOf(System.identityHashCode(g0Var));
                        a.AbstractC0111a<? extends f, b.i.a.f.l.a> aVar = g0Var.d;
                        Context context = g0Var.f1360b;
                        Looper looper = g0Var.c.getLooper();
                        b.i.a.f.e.k.c cVar = g0Var.f;
                        g0Var.g = aVar.a(context, looper, cVar, cVar.g, g0Var, g0Var);
                        g0Var.h = bVar;
                        Set<Scope> set = g0Var.e;
                        if (set == null || set.isEmpty()) {
                            g0Var.c.post(new f0(g0Var));
                        } else {
                            g0Var.g.p();
                        }
                    }
                    try {
                        this.f1357b.g(bVar);
                    } catch (SecurityException e) {
                        e(new ConnectionResult(10), e);
                    }
                } catch (IllegalStateException e2) {
                    e(new ConnectionResult(10), e2);
                }
            }
        }

        public final boolean s() {
            return this.f1357b.o();
        }

        @WorkerThread
        public final void t() {
            q();
            n(ConnectionResult.j);
            v();
            Iterator<d0> it = this.g.values().iterator();
            while (it.hasNext()) {
                d0 next = it.next();
                Objects.requireNonNull(next.a);
                if (a(null) != null) {
                    it.remove();
                } else {
                    try {
                        m<a.b, ?> mVar = next.a;
                        a.b bVar = this.c;
                        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
                        v vVar = (v) mVar;
                        Objects.requireNonNull(vVar);
                        vVar.f1582b.a((b.i.a.f.j.b.e.f) bVar, i.j(vVar.c, taskCompletionSource));
                    } catch (DeadObjectException unused) {
                        c(3);
                        this.f1357b.c("DeadObjectException thrown while calling register listener method.");
                    } catch (RemoteException unused2) {
                        it.remove();
                    }
                }
            }
            u();
            w();
        }

        @WorkerThread
        public final void u() {
            ArrayList arrayList = new ArrayList(this.a);
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                Object obj = arrayList.get(i);
                i++;
                s sVar = (s) obj;
                if (!this.f1357b.j()) {
                    return;
                }
                if (m(sVar)) {
                    this.a.remove(sVar);
                }
            }
        }

        @WorkerThread
        public final void v() {
            if (this.j) {
                g.this.f1355x.removeMessages(11, this.d);
                g.this.f1355x.removeMessages(9, this.d);
                this.j = false;
            }
        }

        public final void w() {
            g.this.f1355x.removeMessages(12, this.d);
            Handler handler = g.this.f1355x;
            handler.sendMessageDelayed(handler.obtainMessage(12, this.d), g.this.n);
        }
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    /* loaded from: classes3.dex */
    public class b implements h0, b.c {
        public final a.f a;

        /* renamed from: b  reason: collision with root package name */
        public final b.i.a.f.e.h.j.b<?> f1358b;
        @Nullable
        public b.i.a.f.e.k.g c = null;
        @Nullable
        public Set<Scope> d = null;
        public boolean e = false;

        public b(a.f fVar, b.i.a.f.e.h.j.b<?> bVar) {
            this.a = fVar;
            this.f1358b = bVar;
        }

        @Override // b.i.a.f.e.k.b.c
        public final void a(@NonNull ConnectionResult connectionResult) {
            g.this.f1355x.post(new z(this, connectionResult));
        }

        @WorkerThread
        public final void b(ConnectionResult connectionResult) {
            a<?> aVar = g.this.t.get(this.f1358b);
            if (aVar != null) {
                d.s(g.this.f1355x);
                a.f fVar = aVar.f1357b;
                String name = aVar.c.getClass().getName();
                String valueOf = String.valueOf(connectionResult);
                fVar.c(b.d.b.a.a.k(valueOf.length() + name.length() + 25, "onSignInFailed for ", name, " with ", valueOf));
                aVar.e(connectionResult, null);
            }
        }
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    /* loaded from: classes3.dex */
    public static class c {
        public final b.i.a.f.e.h.j.b<?> a;

        /* renamed from: b  reason: collision with root package name */
        public final Feature f1359b;

        public c(b.i.a.f.e.h.j.b bVar, Feature feature, t tVar) {
            this.a = bVar;
            this.f1359b = feature;
        }

        public final boolean equals(@Nullable Object obj) {
            if (obj != null && (obj instanceof c)) {
                c cVar = (c) obj;
                if (d.h0(this.a, cVar.a) && d.h0(this.f1359b, cVar.f1359b)) {
                    return true;
                }
            }
            return false;
        }

        public final int hashCode() {
            return Arrays.hashCode(new Object[]{this.a, this.f1359b});
        }

        public final String toString() {
            j jVar = new j(this);
            jVar.a("key", this.a);
            jVar.a("feature", this.f1359b);
            return jVar.toString();
        }
    }

    public g(Context context, Looper looper, GoogleApiAvailability googleApiAvailability) {
        boolean z2 = true;
        this.f1356y = true;
        this.o = context;
        b.i.a.f.h.e.c cVar = new b.i.a.f.h.e.c(looper, this);
        this.f1355x = cVar;
        this.p = googleApiAvailability;
        this.q = new r(googleApiAvailability);
        PackageManager packageManager = context.getPackageManager();
        if (d.f == null) {
            d.f = Boolean.valueOf((!b.i.a.f.e.o.f.A0() || !packageManager.hasSystemFeature("android.hardware.type.automotive")) ? false : z2);
        }
        if (d.f.booleanValue()) {
            this.f1356y = false;
        }
        cVar.sendMessage(cVar.obtainMessage(6));
    }

    public static g a(Context context) {
        g gVar;
        synchronized (l) {
            if (m == null) {
                HandlerThread handlerThread = new HandlerThread("GoogleApiHandler", 9);
                handlerThread.start();
                Looper looper = handlerThread.getLooper();
                Context applicationContext = context.getApplicationContext();
                Object obj = GoogleApiAvailability.c;
                m = new g(applicationContext, looper, GoogleApiAvailability.d);
            }
            gVar = m;
        }
        return gVar;
    }

    public final void b(@NonNull a1 a1Var) {
        synchronized (l) {
            if (this.u != a1Var) {
                this.u = a1Var;
                this.v.clear();
            }
            this.v.addAll(a1Var.o);
        }
    }

    public final boolean c(ConnectionResult connectionResult, int i) {
        PendingIntent pendingIntent;
        GoogleApiAvailability googleApiAvailability = this.p;
        Context context = this.o;
        Objects.requireNonNull(googleApiAvailability);
        if (connectionResult.w0()) {
            pendingIntent = connectionResult.m;
        } else {
            Intent a2 = googleApiAvailability.a(context, connectionResult.l, null);
            pendingIntent = a2 == null ? null : PendingIntent.getActivity(context, 0, a2, 134217728);
        }
        if (pendingIntent == null) {
            return false;
        }
        int i2 = connectionResult.l;
        int i3 = GoogleApiActivity.j;
        Intent intent = new Intent(context, GoogleApiActivity.class);
        intent.putExtra("pending_intent", pendingIntent);
        intent.putExtra("failing_client_id", i);
        intent.putExtra("notify_manager", true);
        googleApiAvailability.i(context, i2, PendingIntent.getActivity(context, 0, intent, 134217728));
        return true;
    }

    @WorkerThread
    public final a<?> d(b.i.a.f.e.h.b<?> bVar) {
        b.i.a.f.e.h.j.b<?> bVar2 = bVar.d;
        a<?> aVar = this.t.get(bVar2);
        if (aVar == null) {
            aVar = new a<>(bVar);
            this.t.put(bVar2, aVar);
        }
        if (aVar.s()) {
            this.w.add(bVar2);
        }
        aVar.r();
        return aVar;
    }

    /* JADX WARN: Code restructure failed: missing block: B:24:0x0086, code lost:
        r6 = false;
     */
    @Override // android.os.Handler.Callback
    @androidx.annotation.WorkerThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public boolean handleMessage(android.os.Message r11) {
        /*
            Method dump skipped, instructions count: 844
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.i.a.f.e.h.j.g.handleMessage(android.os.Message):boolean");
    }
}
