package b.i.a.f.e.h.j;

import android.os.DeadObjectException;
import android.os.RemoteException;
import androidx.annotation.NonNull;
import b.i.a.f.e.h.a;
import b.i.a.f.e.h.a.b;
import b.i.a.f.e.h.c;
import b.i.a.f.e.h.h;
import b.i.a.f.e.k.y;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public abstract class d<R extends h, A extends a.b> extends BasePendingResult<R> implements e<R> {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public d(@NonNull a<?> aVar, @NonNull c cVar) {
        super(cVar);
        b.c.a.a0.d.z(cVar, "GoogleApiClient must not be null");
        b.c.a.a0.d.z(aVar, "Api must not be null");
        if (aVar.f1346b == null) {
            throw new IllegalStateException("This API was constructed with null client keys. This should not be possible.");
        }
    }

    @Override // b.i.a.f.e.h.j.e
    public final void a(@NonNull Status status) {
        b.c.a.a0.d.o(!status.w0(), "Failed result must not be success");
        b(d(status));
    }

    public abstract void k(@NonNull A a) throws RemoteException;

    public final void l(@NonNull A a) throws DeadObjectException {
        if (!(a instanceof y)) {
            try {
                k(a);
            } catch (DeadObjectException e) {
                a(new Status(8, e.getLocalizedMessage(), null));
                throw e;
            } catch (RemoteException e2) {
                a(new Status(8, e2.getLocalizedMessage(), null));
            }
        } else {
            y yVar = (y) a;
            throw new NoSuchMethodError();
        }
    }
}
