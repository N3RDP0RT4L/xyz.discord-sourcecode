package b.i.a.f.e.h.j;

import android.os.Looper;
import android.os.Message;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public final class k<L> {
    public final c a;
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public volatile L f1362b;
    @Nullable
    public volatile a<L> c;

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    /* loaded from: classes3.dex */
    public static final class a<L> {
        public final L a;

        /* renamed from: b  reason: collision with root package name */
        public final String f1363b;

        public a(L l, String str) {
            this.a = l;
            this.f1363b = str;
        }

        public final boolean equals(@Nullable Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return this.a == aVar.a && this.f1363b.equals(aVar.f1363b);
        }

        public final int hashCode() {
            return this.f1363b.hashCode() + (System.identityHashCode(this.a) * 31);
        }
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    /* loaded from: classes3.dex */
    public interface b<L> {
        void a(L l);

        void b();
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    /* loaded from: classes3.dex */
    public final class c extends b.i.a.f.h.e.c {
        public c(Looper looper) {
            super(looper);
        }

        @Override // android.os.Handler
        public final void handleMessage(Message message) {
            boolean z2 = true;
            if (message.what != 1) {
                z2 = false;
            }
            d.l(z2);
            b bVar = (b) message.obj;
            L l = k.this.f1362b;
            if (l == null) {
                bVar.b();
                return;
            }
            try {
                bVar.a(l);
            } catch (RuntimeException e) {
                bVar.b();
                throw e;
            }
        }
    }

    public k(@NonNull Looper looper, @NonNull L l, @NonNull String str) {
        this.a = new c(looper);
        d.z(l, "Listener must not be null");
        this.f1362b = l;
        d.w(str);
        this.c = new a<>(l, str);
    }

    public final void a(b<? super L> bVar) {
        d.z(bVar, "Notifier must not be null");
        this.a.sendMessage(this.a.obtainMessage(1, bVar));
    }
}
