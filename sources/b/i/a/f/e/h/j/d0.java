package b.i.a.f.e.h.j;

import androidx.annotation.NonNull;
import b.i.a.f.e.h.a;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public final class d0 {
    public final m<a.b, ?> a;

    /* renamed from: b  reason: collision with root package name */
    public final q<a.b, ?> f1352b;
    public final Runnable c;

    public d0(@NonNull m<a.b, ?> mVar, @NonNull q<a.b, ?> qVar, @NonNull Runnable runnable) {
        this.a = mVar;
        this.f1352b = qVar;
        this.c = runnable;
    }
}
