package b.i.a.f.e.h.j;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.widget.ProgressBar;
import androidx.annotation.MainThread;
import b.i.a.f.e.e;
import b.i.a.f.e.k.l;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiActivity;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public final class v0 implements Runnable {
    public final s0 j;
    public final /* synthetic */ t0 k;

    public v0(t0 t0Var, s0 s0Var) {
        this.k = t0Var;
        this.j = s0Var;
    }

    @Override // java.lang.Runnable
    @MainThread
    public final void run() {
        if (this.k.k) {
            ConnectionResult connectionResult = this.j.f1368b;
            if (connectionResult.w0()) {
                t0 t0Var = this.k;
                j jVar = t0Var.j;
                Activity b2 = t0Var.b();
                PendingIntent pendingIntent = connectionResult.m;
                Objects.requireNonNull(pendingIntent, "null reference");
                int i = this.j.a;
                int i2 = GoogleApiActivity.j;
                Intent intent = new Intent(b2, GoogleApiActivity.class);
                intent.putExtra("pending_intent", pendingIntent);
                intent.putExtra("failing_client_id", i);
                intent.putExtra("notify_manager", false);
                jVar.startActivityForResult(intent, 1);
            } else if (this.k.n.d(connectionResult.l)) {
                t0 t0Var2 = this.k;
                GoogleApiAvailability googleApiAvailability = t0Var2.n;
                Activity b3 = t0Var2.b();
                t0 t0Var3 = this.k;
                googleApiAvailability.j(b3, t0Var3.j, connectionResult.l, t0Var3);
            } else if (connectionResult.l == 18) {
                Activity b4 = this.k.b();
                t0 t0Var4 = this.k;
                ProgressBar progressBar = new ProgressBar(b4, null, 16842874);
                progressBar.setIndeterminate(true);
                progressBar.setVisibility(0);
                AlertDialog.Builder builder = new AlertDialog.Builder(b4);
                builder.setView(progressBar);
                builder.setMessage(l.e(b4, 18));
                builder.setPositiveButton("", (DialogInterface.OnClickListener) null);
                AlertDialog create = builder.create();
                GoogleApiAvailability.h(b4, create, "GooglePlayServicesUpdatingDialog", t0Var4);
                t0 t0Var5 = this.k;
                GoogleApiAvailability googleApiAvailability2 = t0Var5.n;
                Context applicationContext = t0Var5.b().getApplicationContext();
                u0 u0Var = new u0(this, create);
                Objects.requireNonNull(googleApiAvailability2);
                IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
                intentFilter.addDataScheme("package");
                a0 a0Var = new a0(u0Var);
                applicationContext.registerReceiver(a0Var, intentFilter);
                a0Var.a = applicationContext;
                if (!e.b(applicationContext, "com.google.android.gms")) {
                    this.k.m();
                    if (create.isShowing()) {
                        create.dismiss();
                    }
                    a0Var.a();
                }
            } else {
                this.k.l(connectionResult, this.j.a);
            }
        }
    }
}
