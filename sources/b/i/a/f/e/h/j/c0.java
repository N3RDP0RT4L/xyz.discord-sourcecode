package b.i.a.f.e.h.j;

import b.i.a.f.e.h.b;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public final class c0 {
    public final s a;

    /* renamed from: b  reason: collision with root package name */
    public final int f1351b;
    public final b<?> c;

    public c0(s sVar, int i, b<?> bVar) {
        this.a = sVar;
        this.f1351b = i;
        this.c = bVar;
    }
}
