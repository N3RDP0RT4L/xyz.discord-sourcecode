package b.i.a.f.e.h.j;

import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.api.Status;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public interface n {
    @RecentlyNonNull
    Exception a(@RecentlyNonNull Status status);
}
