package b.i.a.f.e.h.j;

import android.os.RemoteException;
import androidx.annotation.Nullable;
import b.i.a.f.e.h.a;
import b.i.a.f.e.h.a.b;
import b.i.a.f.h.b.j;
import com.google.android.gms.common.Feature;
import com.google.android.gms.tasks.TaskCompletionSource;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public abstract class p<A extends a.b, ResultT> {
    @Nullable
    public final Feature[] a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f1365b;

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    /* loaded from: classes3.dex */
    public static class a<A extends a.b, ResultT> {
        public j<A, TaskCompletionSource<ResultT>> a;

        /* renamed from: b  reason: collision with root package name */
        public Feature[] f1366b;

        public a(j0 j0Var) {
        }
    }

    @Deprecated
    public p() {
        this.a = null;
        this.f1365b = false;
    }

    public abstract void c(A a2, TaskCompletionSource<ResultT> taskCompletionSource) throws RemoteException;

    public p(Feature[] featureArr, boolean z2, j0 j0Var) {
        this.a = featureArr;
        this.f1365b = z2;
    }
}
