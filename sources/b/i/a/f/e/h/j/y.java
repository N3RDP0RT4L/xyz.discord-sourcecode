package b.i.a.f.e.h.j;

import android.os.Looper;
import androidx.annotation.NonNull;
import b.i.a.f.e.h.a;
import b.i.a.f.e.h.a.d;
import b.i.a.f.e.h.b;
import b.i.a.f.e.h.h;
import org.checkerframework.checker.initialization.qual.NotOnlyInitialized;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public final class y<O extends a.d> extends r {
    @NotOnlyInitialized

    /* renamed from: b  reason: collision with root package name */
    public final b<O> f1371b;

    public y(b<O> bVar) {
        super("Method is not supported by connectionless client. APIs supporting connectionless client must not call this method.");
        this.f1371b = bVar;
    }

    @Override // b.i.a.f.e.h.c
    public final <A extends a.b, R extends h, T extends d<R, A>> T a(@NonNull T t) {
        this.f1371b.d(0, t);
        return t;
    }

    @Override // b.i.a.f.e.h.c
    public final <A extends a.b, T extends d<? extends h, A>> T b(@NonNull T t) {
        this.f1371b.d(1, t);
        return t;
    }

    @Override // b.i.a.f.e.h.c
    public final Looper c() {
        return this.f1371b.e;
    }
}
