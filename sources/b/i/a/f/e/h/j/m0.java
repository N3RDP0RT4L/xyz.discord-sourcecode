package b.i.a.f.e.h.j;

import androidx.annotation.Nullable;
import b.i.a.f.e.h.j.g;
import com.google.android.gms.common.Feature;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public abstract class m0 extends s {
    public m0(int i) {
        super(i);
    }

    @Nullable
    public abstract Feature[] f(g.a<?> aVar);

    public abstract boolean g(g.a<?> aVar);
}
