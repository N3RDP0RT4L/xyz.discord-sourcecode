package b.i.a.f.e.h;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.common.api.Scope;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public final class l implements Parcelable.Creator<Scope> {
    @Override // android.os.Parcelable.Creator
    public final Scope createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        int i = 0;
        String str = null;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                i = d.G1(parcel, readInt);
            } else if (c != 2) {
                d.d2(parcel, readInt);
            } else {
                str = d.R(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        return new Scope(i, str);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Scope[] newArray(int i) {
        return new Scope[i];
    }
}
