package b.i.a.f.e.h;

import androidx.annotation.RecentlyNonNull;
import b.i.a.f.e.h.h;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public interface i<R extends h> {
    void a(@RecentlyNonNull R r);
}
