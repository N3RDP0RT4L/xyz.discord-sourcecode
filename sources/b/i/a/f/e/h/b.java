package b.i.a.f.e.h;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.collection.ArraySet;
import b.i.a.f.e.h.a;
import b.i.a.f.e.h.a.d;
import b.i.a.f.e.h.j.a1;
import b.i.a.f.e.h.j.c0;
import b.i.a.f.e.h.j.g;
import b.i.a.f.e.h.j.k;
import b.i.a.f.e.h.j.n;
import b.i.a.f.e.h.j.o0;
import b.i.a.f.e.h.j.p;
import b.i.a.f.e.h.j.p0;
import b.i.a.f.e.h.j.q0;
import b.i.a.f.e.h.j.y;
import b.i.a.f.e.k.c;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.GoogleApiActivity;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.internal.BasePendingResult;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.Objects;
import java.util.Set;
import org.checkerframework.checker.initialization.qual.NotOnlyInitialized;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public class b<O extends a.d> {
    public final Context a;

    /* renamed from: b  reason: collision with root package name */
    public final b.i.a.f.e.h.a<O> f1347b;
    public final O c;
    public final b.i.a.f.e.h.j.b<O> d;
    public final Looper e;
    public final int f;
    @NotOnlyInitialized
    public final c g;
    public final n h;
    public final g i;

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    /* loaded from: classes3.dex */
    public static class a {
        public static final a a = new a(new b.i.a.f.e.h.j.a(), null, Looper.getMainLooper());

        /* renamed from: b  reason: collision with root package name */
        public final n f1348b;
        public final Looper c;

        public a(n nVar, Account account, Looper looper) {
            this.f1348b = nVar;
            this.c = looper;
        }
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    @Deprecated
    public b(@NonNull Context context, b.i.a.f.e.h.a<O> aVar, O o, n nVar) {
        this(context, aVar, o, new a(nVar, null, Looper.getMainLooper()));
        b.c.a.a0.d.z(nVar, "StatusExceptionMapper must not be null.");
    }

    @Nullable
    public static String e(Object obj) {
        if (!(Build.VERSION.SDK_INT >= 30)) {
            return null;
        }
        try {
            return (String) Context.class.getMethod("getAttributionTag", new Class[0]).invoke(obj, new Object[0]);
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException unused) {
            return null;
        }
    }

    public c.a a() {
        Set<Scope> set;
        GoogleSignInAccount L;
        GoogleSignInAccount L2;
        c.a aVar = new c.a();
        O o = this.c;
        Account account = null;
        if (!(o instanceof a.d.b) || (L2 = ((a.d.b) o).L()) == null) {
            O o2 = this.c;
            if (o2 instanceof a.d.AbstractC0112a) {
                account = ((a.d.AbstractC0112a) o2).T();
            }
        } else if (L2.m != null) {
            account = new Account(L2.m, "com.google");
        }
        aVar.a = account;
        O o3 = this.c;
        if (!(o3 instanceof a.d.b) || (L = ((a.d.b) o3).L()) == null) {
            set = Collections.emptySet();
        } else {
            set = L.w0();
        }
        if (aVar.f1377b == null) {
            aVar.f1377b = new ArraySet<>();
        }
        aVar.f1377b.addAll(set);
        aVar.d = this.a.getClass().getName();
        aVar.c = this.a.getPackageName();
        return aVar;
    }

    public Task<Boolean> b(@NonNull k.a<?> aVar) {
        b.c.a.a0.d.z(aVar, "Listener key cannot be null.");
        g gVar = this.i;
        Objects.requireNonNull(gVar);
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        p0 p0Var = new p0(aVar, taskCompletionSource);
        Handler handler = gVar.f1355x;
        handler.sendMessage(handler.obtainMessage(13, new c0(p0Var, gVar.f1354s.get(), this)));
        return taskCompletionSource.a;
    }

    public <TResult, A extends a.b> Task<TResult> c(p<A, TResult> pVar) {
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        g gVar = this.i;
        n nVar = this.h;
        Objects.requireNonNull(gVar);
        q0 q0Var = new q0(1, pVar, taskCompletionSource, nVar);
        Handler handler = gVar.f1355x;
        handler.sendMessage(handler.obtainMessage(4, new c0(q0Var, gVar.f1354s.get(), this)));
        return taskCompletionSource.a;
    }

    public final <A extends a.b, T extends b.i.a.f.e.h.j.d<? extends h, A>> T d(int i, @NonNull T t) {
        t.k = t.k || BasePendingResult.a.get().booleanValue();
        g gVar = this.i;
        Objects.requireNonNull(gVar);
        o0 o0Var = new o0(i, t);
        Handler handler = gVar.f1355x;
        handler.sendMessage(handler.obtainMessage(4, new c0(o0Var, gVar.f1354s.get(), this)));
        return t;
    }

    @MainThread
    public b(@NonNull Activity activity, b.i.a.f.e.h.a<O> aVar, O o, a aVar2) {
        b.c.a.a0.d.z(activity, "Null activity is not permitted.");
        b.c.a.a0.d.z(aVar, "Api must not be null.");
        b.c.a.a0.d.z(aVar2, "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        Context applicationContext = activity.getApplicationContext();
        this.a = applicationContext;
        e(activity);
        this.f1347b = aVar;
        this.c = o;
        this.e = aVar2.c;
        b.i.a.f.e.h.j.b<O> bVar = new b.i.a.f.e.h.j.b<>(aVar, o);
        this.d = bVar;
        this.g = new y(this);
        g a2 = g.a(applicationContext);
        this.i = a2;
        this.f = a2.r.getAndIncrement();
        this.h = aVar2.f1348b;
        if (!(activity instanceof GoogleApiActivity)) {
            try {
                a1.o(activity, a2, bVar);
            } catch (IllegalStateException | ConcurrentModificationException unused) {
            }
        }
        Handler handler = this.i.f1355x;
        handler.sendMessage(handler.obtainMessage(7, this));
    }

    public b(@NonNull Context context, b.i.a.f.e.h.a<O> aVar, O o, a aVar2) {
        b.c.a.a0.d.z(context, "Null context is not permitted.");
        b.c.a.a0.d.z(aVar, "Api must not be null.");
        b.c.a.a0.d.z(aVar2, "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        Context applicationContext = context.getApplicationContext();
        this.a = applicationContext;
        e(context);
        this.f1347b = aVar;
        this.c = o;
        this.e = aVar2.c;
        this.d = new b.i.a.f.e.h.j.b<>(aVar, o);
        this.g = new y(this);
        g a2 = g.a(applicationContext);
        this.i = a2;
        this.f = a2.r.getAndIncrement();
        this.h = aVar2.f1348b;
        Handler handler = a2.f1355x;
        handler.sendMessage(handler.obtainMessage(7, this));
    }
}
