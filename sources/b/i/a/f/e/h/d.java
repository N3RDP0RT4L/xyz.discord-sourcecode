package b.i.a.f.e.h;

import androidx.annotation.NonNull;
import b.i.a.f.e.h.h;
import com.google.android.gms.common.api.Status;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public abstract class d<R extends h> {

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    /* loaded from: classes3.dex */
    public interface a {
        void a(Status status);
    }

    public abstract void c(@NonNull a aVar);
}
