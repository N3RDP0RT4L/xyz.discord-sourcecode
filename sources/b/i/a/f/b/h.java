package b.i.a.f.b;

import android.util.Log;
import b.i.a.f.b.f;
import java.lang.Thread;
import java.util.concurrent.FutureTask;
/* loaded from: classes3.dex */
public final class h extends FutureTask<T> {
    public final /* synthetic */ f.a j;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public h(f.a aVar, Runnable runnable, Object obj) {
        super(runnable, obj);
        this.j = aVar;
    }

    @Override // java.util.concurrent.FutureTask
    public final void setException(Throwable th) {
        Thread.UncaughtExceptionHandler uncaughtExceptionHandler = f.this.e;
        if (uncaughtExceptionHandler != null) {
            uncaughtExceptionHandler.uncaughtException(Thread.currentThread(), th);
        } else if (Log.isLoggable("GAv4", 6)) {
            String valueOf = String.valueOf(th);
            StringBuilder sb = new StringBuilder(valueOf.length() + 37);
            sb.append("MeasurementExecutor: job failed with ");
            sb.append(valueOf);
            Log.e("GAv4", sb.toString());
        }
        super.setException(th);
    }
}
