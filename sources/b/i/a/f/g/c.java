package b.i.a.f.g;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public final class c implements i {
    @Override // b.i.a.f.g.i
    public final int a(Context context, String str, boolean z2) throws DynamiteModule.LoadingException {
        return DynamiteModule.d(context, str, z2);
    }

    @Override // b.i.a.f.g.i
    public final int b(Context context, String str) {
        return DynamiteModule.a(context, str);
    }
}
