package b.i.a.f.g;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import b.i.a.f.f.a;
import b.i.a.f.h.g.a;
import b.i.a.f.h.g.c;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public final class l extends a implements IInterface {
    public l(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoaderV2");
    }

    public final b.i.a.f.f.a i(b.i.a.f.f.a aVar, String str, int i, b.i.a.f.f.a aVar2) throws RemoteException {
        Parcel g = g();
        c.b(g, aVar);
        g.writeString(str);
        g.writeInt(i);
        c.b(g, aVar2);
        Parcel c = c(2, g);
        b.i.a.f.f.a g2 = a.AbstractBinderC0116a.g(c.readStrongBinder());
        c.recycle();
        return g2;
    }

    public final b.i.a.f.f.a t0(b.i.a.f.f.a aVar, String str, int i, b.i.a.f.f.a aVar2) throws RemoteException {
        Parcel g = g();
        c.b(g, aVar);
        g.writeString(str);
        g.writeInt(i);
        c.b(g, aVar2);
        Parcel c = c(3, g);
        b.i.a.f.f.a g2 = a.AbstractBinderC0116a.g(c.readStrongBinder());
        c.recycle();
        return g2;
    }
}
