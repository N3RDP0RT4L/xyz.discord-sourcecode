package b.i.a.f.g;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public final class d implements DynamiteModule.a {
    @Override // com.google.android.gms.dynamite.DynamiteModule.a
    public final j a(Context context, String str, i iVar) throws DynamiteModule.LoadingException {
        j jVar = new j();
        int b2 = iVar.b(context, str);
        jVar.a = b2;
        if (b2 != 0) {
            jVar.c = -1;
        } else {
            int a = iVar.a(context, str, true);
            jVar.f1398b = a;
            if (a != 0) {
                jVar.c = 1;
            }
        }
        return jVar;
    }
}
