package b.i.a.f.g;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public final class e implements DynamiteModule.a {
    @Override // com.google.android.gms.dynamite.DynamiteModule.a
    public final j a(Context context, String str, i iVar) throws DynamiteModule.LoadingException {
        int i;
        j jVar = new j();
        int b2 = iVar.b(context, str);
        jVar.a = b2;
        int i2 = 0;
        if (b2 != 0) {
            i = iVar.a(context, str, false);
            jVar.f1398b = i;
        } else {
            i = iVar.a(context, str, true);
            jVar.f1398b = i;
        }
        int i3 = jVar.a;
        if (i3 != 0) {
            i2 = i3;
        } else if (i == 0) {
            jVar.c = 0;
            return jVar;
        }
        if (i2 >= i) {
            jVar.c = -1;
        } else {
            jVar.c = 1;
        }
        return jVar;
    }
}
