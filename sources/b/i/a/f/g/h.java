package b.i.a.f.g;

import android.content.Context;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public final class h implements i {
    public final int a;

    public h(int i) {
        this.a = i;
    }

    @Override // b.i.a.f.g.i
    public final int a(Context context, String str, boolean z2) {
        return 0;
    }

    @Override // b.i.a.f.g.i
    public final int b(Context context, String str) {
        return this.a;
    }
}
