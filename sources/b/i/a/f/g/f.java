package b.i.a.f.g;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public final class f implements DynamiteModule.a {
    @Override // com.google.android.gms.dynamite.DynamiteModule.a
    public final j a(Context context, String str, i iVar) throws DynamiteModule.LoadingException {
        j jVar = new j();
        jVar.a = iVar.b(context, str);
        int a = iVar.a(context, str, true);
        jVar.f1398b = a;
        int i = jVar.a;
        if (i == 0) {
            if (a == 0) {
                jVar.c = 0;
                return jVar;
            }
            i = 0;
        }
        if (a >= i) {
            jVar.c = 1;
        } else {
            jVar.c = -1;
        }
        return jVar;
    }
}
