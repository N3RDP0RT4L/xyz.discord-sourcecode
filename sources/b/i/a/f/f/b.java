package b.i.a.f.f;

import android.os.IBinder;
import androidx.annotation.RecentlyNonNull;
import b.i.a.f.f.a;
import java.lang.reflect.Field;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public final class b<T> extends a.AbstractBinderC0116a {
    public final T a;

    public b(T t) {
        this.a = t;
    }

    @RecentlyNonNull
    public static <T> T i(@RecentlyNonNull a aVar) {
        if (aVar instanceof b) {
            return ((b) aVar).a;
        }
        IBinder asBinder = aVar.asBinder();
        Field[] declaredFields = asBinder.getClass().getDeclaredFields();
        Field field = null;
        int i = 0;
        for (Field field2 : declaredFields) {
            if (!field2.isSynthetic()) {
                i++;
                field = field2;
            }
        }
        if (i == 1) {
            Objects.requireNonNull(field, "null reference");
            if (!field.isAccessible()) {
                field.setAccessible(true);
                try {
                    return (T) field.get(asBinder);
                } catch (IllegalAccessException e) {
                    throw new IllegalArgumentException("Could not access the field in remoteBinder.", e);
                } catch (NullPointerException e2) {
                    throw new IllegalArgumentException("Binder object is null.", e2);
                }
            } else {
                throw new IllegalArgumentException("IObjectWrapper declared field not private!");
            }
        } else {
            throw new IllegalArgumentException(b.d.b.a.a.f(64, "Unexpected number of IObjectWrapper declared fields: ", declaredFields.length));
        }
    }
}
