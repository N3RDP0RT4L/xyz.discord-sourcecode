package b.i.a.f.d;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.cloudmessaging.zza;
import com.google.android.gms.cloudmessaging.zzp;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
/* compiled from: com.google.android.gms:play-services-cloud-messaging@@16.0.0 */
/* loaded from: classes3.dex */
public final /* synthetic */ class k implements Runnable {
    public final g j;

    public k(g gVar) {
        this.j = gVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        final g gVar = this.j;
        while (true) {
            synchronized (gVar) {
                if (gVar.j == 2) {
                    if (gVar.m.isEmpty()) {
                        gVar.c();
                        return;
                    }
                    final q<?> poll = gVar.m.poll();
                    gVar.n.put(poll.a, poll);
                    gVar.o.c.schedule(new Runnable(gVar, poll) { // from class: b.i.a.f.d.m
                        public final g j;
                        public final q k;

                        {
                            this.j = gVar;
                            this.k = poll;
                        }

                        @Override // java.lang.Runnable
                        public final void run() {
                            g gVar2 = this.j;
                            int i = this.k.a;
                            synchronized (gVar2) {
                                q<?> qVar = gVar2.n.get(i);
                                if (qVar != null) {
                                    StringBuilder sb = new StringBuilder(31);
                                    sb.append("Timing out request: ");
                                    sb.append(i);
                                    Log.w("MessengerIpcClient", sb.toString());
                                    gVar2.n.remove(i);
                                    qVar.b(new zzp(3, "Timed out waiting for response"));
                                    gVar2.c();
                                }
                            }
                        }
                    }, 30L, TimeUnit.SECONDS);
                    if (Log.isLoggable("MessengerIpcClient", 3)) {
                        String valueOf = String.valueOf(poll);
                        StringBuilder sb = new StringBuilder(valueOf.length() + 8);
                        sb.append("Sending ");
                        sb.append(valueOf);
                        Log.d("MessengerIpcClient", sb.toString());
                    }
                    Context context = gVar.o.f1335b;
                    Messenger messenger = gVar.k;
                    Message obtain = Message.obtain();
                    obtain.what = poll.c;
                    obtain.arg1 = poll.a;
                    obtain.replyTo = messenger;
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("oneWay", poll.d());
                    bundle.putString("pkg", context.getPackageName());
                    bundle.putBundle("data", poll.d);
                    obtain.setData(bundle);
                    try {
                        p pVar = gVar.l;
                        Messenger messenger2 = pVar.a;
                        if (messenger2 == null) {
                            zza zzaVar = pVar.f1336b;
                            if (zzaVar == null) {
                                throw new IllegalStateException("Both messengers are null");
                                break;
                            }
                            Messenger messenger3 = zzaVar.j;
                            Objects.requireNonNull(messenger3);
                            messenger3.send(obtain);
                        } else {
                            messenger2.send(obtain);
                        }
                    } catch (RemoteException e) {
                        gVar.a(2, e.getMessage());
                    }
                } else {
                    return;
                }
            }
        }
    }
}
