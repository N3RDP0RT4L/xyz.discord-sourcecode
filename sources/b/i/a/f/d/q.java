package b.i.a.f.d;

import android.os.Bundle;
import android.util.Log;
import b.d.b.a.a;
import com.google.android.gms.cloudmessaging.zzp;
import com.google.android.gms.tasks.TaskCompletionSource;
/* compiled from: com.google.android.gms:play-services-cloud-messaging@@16.0.0 */
/* loaded from: classes3.dex */
public abstract class q<T> {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final TaskCompletionSource<T> f1337b = new TaskCompletionSource<>();
    public final int c;
    public final Bundle d;

    public q(int i, int i2, Bundle bundle) {
        this.a = i;
        this.c = i2;
        this.d = bundle;
    }

    public abstract void a(Bundle bundle);

    public final void b(zzp zzpVar) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(this);
            String valueOf2 = String.valueOf(zzpVar);
            Log.d("MessengerIpcClient", a.k(valueOf2.length() + valueOf.length() + 14, "Failing ", valueOf, " with ", valueOf2));
        }
        this.f1337b.a.t(zzpVar);
    }

    public final void c(T t) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(this);
            String valueOf2 = String.valueOf(t);
            Log.d("MessengerIpcClient", a.k(valueOf2.length() + valueOf.length() + 16, "Finishing ", valueOf, " with ", valueOf2));
        }
        this.f1337b.a.s(t);
    }

    public abstract boolean d();

    public String toString() {
        int i = this.c;
        int i2 = this.a;
        boolean d = d();
        StringBuilder sb = new StringBuilder(55);
        sb.append("Request { what=");
        sb.append(i);
        sb.append(" id=");
        sb.append(i2);
        sb.append(" oneWay=");
        sb.append(d);
        sb.append("}");
        return sb.toString();
    }
}
