package b.i.a.f.d;

import android.os.Bundle;
import android.util.Log;
import b.i.a.f.n.a;
import com.google.android.gms.tasks.Task;
import java.io.IOException;
/* compiled from: com.google.android.gms:play-services-cloud-messaging@@16.0.0 */
/* loaded from: classes3.dex */
public final /* synthetic */ class t implements a {
    public static final a a = new t();

    @Override // b.i.a.f.n.a
    public final Object a(Task task) {
        int i = b.a;
        if (task.p()) {
            return (Bundle) task.l();
        }
        if (Log.isLoggable("Rpc", 3)) {
            String valueOf = String.valueOf(task.k());
            StringBuilder sb = new StringBuilder(valueOf.length() + 22);
            sb.append("Error making request: ");
            sb.append(valueOf);
            Log.d("Rpc", sb.toString());
        }
        throw new IOException("SERVICE_NOT_AVAILABLE", task.k());
    }
}
