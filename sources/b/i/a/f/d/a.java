package b.i.a.f.d;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;
import b.i.a.f.e.o.f;
import com.google.android.gms.cloudmessaging.CloudMessage;
import com.google.android.gms.tasks.Task;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
/* compiled from: com.google.android.gms:play-services-cloud-messaging@@16.0.0 */
/* loaded from: classes3.dex */
public abstract class a extends BroadcastReceiver {
    public final ExecutorService a;

    public a() {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 1, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue(), new b.i.a.f.e.o.j.a("firebase-iid-executor"));
        threadPoolExecutor.allowCoreThreadTimeOut(true);
        this.a = Executors.unconfigurableExecutorService(threadPoolExecutor);
    }

    @WorkerThread
    public final int a(@NonNull Context context, @NonNull Intent intent) {
        PendingIntent pendingIntent = (PendingIntent) intent.getParcelableExtra("pending_intent");
        if (pendingIntent != null) {
            try {
                pendingIntent.send();
            } catch (PendingIntent.CanceledException unused) {
                Log.e("CloudMessagingReceiver", "Notification pending intent canceled");
            }
        }
        Bundle extras = intent.getExtras();
        if (extras != null) {
            extras.remove("pending_intent");
        } else {
            extras = new Bundle();
        }
        if ("com.google.firebase.messaging.NOTIFICATION_OPEN".equals(intent.getAction())) {
            try {
                f.j(new b.i.c.s.f(context).b(new Intent("com.google.firebase.messaging.NOTIFICATION_OPEN").putExtras(extras)));
                return -1;
            } catch (InterruptedException | ExecutionException e) {
                Log.e("FirebaseInstanceId", "Failed to send notification open event to service.", e);
                return -1;
            }
        } else if ("com.google.firebase.messaging.NOTIFICATION_DISMISS".equals(intent.getAction())) {
            try {
                f.j(new b.i.c.s.f(context).b(new Intent("com.google.firebase.messaging.NOTIFICATION_DISMISS").putExtras(extras)));
                return -1;
            } catch (InterruptedException | ExecutionException e2) {
                Log.e("FirebaseInstanceId", "Failed to send notification dismissed event to service.", e2);
                return -1;
            }
        } else {
            Log.e("CloudMessagingReceiver", "Unknown notification action");
            return 500;
        }
    }

    @WorkerThread
    public final int b(@NonNull Context context, @NonNull Intent intent) {
        Task task;
        int i;
        int i2 = 500;
        if (intent.getExtras() == null) {
            return 500;
        }
        String stringExtra = intent.getStringExtra("google.message_id");
        if (TextUtils.isEmpty(stringExtra)) {
            task = f.Z(null);
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("google.message_id", stringExtra);
            f a = f.a(context);
            synchronized (a) {
                i = a.e;
                a.e = i + 1;
            }
            task = a.b(new o(i, bundle));
        }
        try {
            i2 = ((Integer) f.j(new b.i.c.s.f(context).b(new CloudMessage(intent).j))).intValue();
        } catch (InterruptedException | ExecutionException e) {
            Log.e("FirebaseInstanceId", "Failed to send message to service.", e);
        }
        try {
            f.k(task, TimeUnit.SECONDS.toMillis(1L), TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e2) {
            String valueOf = String.valueOf(e2);
            b.d.b.a.a.f0(valueOf.length() + 20, "Message ack failed: ", valueOf, "CloudMessagingReceiver");
        }
        return i2;
    }

    @Override // android.content.BroadcastReceiver
    public final void onReceive(@NonNull final Context context, @NonNull final Intent intent) {
        if (intent != null) {
            final boolean isOrderedBroadcast = isOrderedBroadcast();
            final BroadcastReceiver.PendingResult goAsync = goAsync();
            this.a.execute(new Runnable(this, intent, context, isOrderedBroadcast, goAsync) { // from class: b.i.a.f.d.e
                public final a j;
                public final Intent k;
                public final Context l;
                public final boolean m;
                public final BroadcastReceiver.PendingResult n;

                {
                    this.j = this;
                    this.k = intent;
                    this.l = context;
                    this.m = isOrderedBroadcast;
                    this.n = goAsync;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    int i;
                    a aVar = this.j;
                    Intent intent2 = this.k;
                    Context context2 = this.l;
                    boolean z2 = this.m;
                    BroadcastReceiver.PendingResult pendingResult = this.n;
                    Objects.requireNonNull(aVar);
                    try {
                        Parcelable parcelableExtra = intent2.getParcelableExtra("wrapped_intent");
                        Intent intent3 = parcelableExtra instanceof Intent ? (Intent) parcelableExtra : null;
                        if (intent3 != null) {
                            i = aVar.a(context2, intent3);
                        } else {
                            i = aVar.b(context2, intent2);
                        }
                        if (z2) {
                            pendingResult.setResultCode(i);
                        }
                    } finally {
                        pendingResult.finish();
                    }
                }
            });
        }
    }
}
