package b.i.a.f.d;

import android.os.Bundle;
import b.i.a.f.n.f;
import com.google.android.gms.tasks.Task;
/* compiled from: com.google.android.gms:play-services-cloud-messaging@@16.0.0 */
/* loaded from: classes3.dex */
public final /* synthetic */ class w implements f {
    public static final f a = new w();

    @Override // b.i.a.f.n.f
    public final Task a(Object obj) {
        Bundle bundle = (Bundle) obj;
        int i = b.a;
        if (bundle != null && bundle.containsKey("google.messenger")) {
            return b.i.a.f.e.o.f.Z(null);
        }
        return b.i.a.f.e.o.f.Z(bundle);
    }
}
