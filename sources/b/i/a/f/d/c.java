package b.i.a.f.d;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.cloudmessaging.CloudMessage;
/* compiled from: com.google.android.gms:play-services-cloud-messaging@@16.0.0 */
/* loaded from: classes3.dex */
public final class c implements Parcelable.Creator<CloudMessage> {
    @Override // android.os.Parcelable.Creator
    public final CloudMessage createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        Intent intent = null;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            if (((char) readInt) != 1) {
                d.d2(parcel, readInt);
            } else {
                intent = (Intent) d.Q(parcel, readInt, Intent.CREATOR);
            }
        }
        d.f0(parcel, m2);
        return new CloudMessage(intent);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ CloudMessage[] newArray(int i) {
        return new CloudMessage[i];
    }
}
