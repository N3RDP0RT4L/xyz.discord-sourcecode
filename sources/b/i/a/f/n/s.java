package b.i.a.f.n;

import com.google.android.gms.tasks.Task;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-tasks@@17.2.1 */
/* loaded from: classes3.dex */
public final class s implements Runnable {
    public final /* synthetic */ Task j;
    public final /* synthetic */ t k;

    public s(t tVar, Task task) {
        this.k = tVar;
        this.j = task;
    }

    @Override // java.lang.Runnable
    public final void run() {
        synchronized (this.k.f1595b) {
            d dVar = this.k.c;
            if (dVar != null) {
                Exception k = this.j.k();
                Objects.requireNonNull(k, "null reference");
                dVar.onFailure(k);
            }
        }
    }
}
