package b.i.a.f.n;

import androidx.annotation.NonNull;
import com.google.android.gms.tasks.Task;
import java.util.ArrayDeque;
import java.util.Queue;
/* compiled from: com.google.android.gms:play-services-tasks@@17.2.1 */
/* loaded from: classes3.dex */
public final class z<TResult> {
    public final Object a = new Object();

    /* renamed from: b  reason: collision with root package name */
    public Queue<y<TResult>> f1598b;
    public boolean c;

    public final void a(@NonNull y<TResult> yVar) {
        synchronized (this.a) {
            if (this.f1598b == null) {
                this.f1598b = new ArrayDeque();
            }
            this.f1598b.add(yVar);
        }
    }

    public final void b(@NonNull Task<TResult> task) {
        y<TResult> poll;
        synchronized (this.a) {
            if (this.f1598b != null && !this.c) {
                this.c = true;
                while (true) {
                    synchronized (this.a) {
                        poll = this.f1598b.poll();
                        if (poll == null) {
                            this.c = false;
                            return;
                        }
                    }
                    poll.a(task);
                }
            }
        }
    }
}
