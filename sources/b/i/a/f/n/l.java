package b.i.a.f.n;

import androidx.annotation.NonNull;
import com.google.android.gms.tasks.Task;
import java.util.concurrent.Executor;
/* compiled from: com.google.android.gms:play-services-tasks@@17.2.1 */
/* loaded from: classes3.dex */
public final class l<TResult, TContinuationResult> implements y<TResult> {
    public final Executor a;

    /* renamed from: b  reason: collision with root package name */
    public final a<TResult, TContinuationResult> f1591b;
    public final c0<TContinuationResult> c;

    public l(@NonNull Executor executor, @NonNull a<TResult, TContinuationResult> aVar, @NonNull c0<TContinuationResult> c0Var) {
        this.a = executor;
        this.f1591b = aVar;
        this.c = c0Var;
    }

    @Override // b.i.a.f.n.y
    public final void a(@NonNull Task<TResult> task) {
        this.a.execute(new k(this, task));
    }
}
