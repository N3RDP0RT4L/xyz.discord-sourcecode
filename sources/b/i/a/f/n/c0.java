package b.i.a.f.n;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.d.b.a.a;
import com.google.android.gms.tasks.DuplicateTaskCompletionException;
import com.google.android.gms.tasks.RuntimeExecutionException;
import com.google.android.gms.tasks.Task;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;
/* compiled from: com.google.android.gms:play-services-tasks@@17.2.1 */
/* loaded from: classes3.dex */
public final class c0<TResult> extends Task<TResult> {
    public final Object a = new Object();

    /* renamed from: b  reason: collision with root package name */
    public final z<TResult> f1588b = new z<>();
    public boolean c;
    public volatile boolean d;
    @Nullable
    public TResult e;
    public Exception f;

    @Override // com.google.android.gms.tasks.Task
    @NonNull
    public final Task<TResult> a(@NonNull Executor executor, @NonNull b bVar) {
        this.f1588b.a(new p(executor, bVar));
        w();
        return this;
    }

    @Override // com.google.android.gms.tasks.Task
    @NonNull
    public final Task<TResult> b(@NonNull c<TResult> cVar) {
        this.f1588b.a(new r(g.a, cVar));
        w();
        return this;
    }

    @Override // com.google.android.gms.tasks.Task
    @NonNull
    public final Task<TResult> c(@NonNull Executor executor, @NonNull c<TResult> cVar) {
        this.f1588b.a(new r(executor, cVar));
        w();
        return this;
    }

    @Override // com.google.android.gms.tasks.Task
    @NonNull
    public final Task<TResult> d(@NonNull d dVar) {
        e(g.a, dVar);
        return this;
    }

    @Override // com.google.android.gms.tasks.Task
    @NonNull
    public final Task<TResult> e(@NonNull Executor executor, @NonNull d dVar) {
        this.f1588b.a(new t(executor, dVar));
        w();
        return this;
    }

    @Override // com.google.android.gms.tasks.Task
    @NonNull
    public final Task<TResult> f(@NonNull e<? super TResult> eVar) {
        g(g.a, eVar);
        return this;
    }

    @Override // com.google.android.gms.tasks.Task
    @NonNull
    public final Task<TResult> g(@NonNull Executor executor, @NonNull e<? super TResult> eVar) {
        this.f1588b.a(new v(executor, eVar));
        w();
        return this;
    }

    @Override // com.google.android.gms.tasks.Task
    @NonNull
    public final <TContinuationResult> Task<TContinuationResult> h(@NonNull a<TResult, TContinuationResult> aVar) {
        return i(g.a, aVar);
    }

    @Override // com.google.android.gms.tasks.Task
    @NonNull
    public final <TContinuationResult> Task<TContinuationResult> i(@NonNull Executor executor, @NonNull a<TResult, TContinuationResult> aVar) {
        c0 c0Var = new c0();
        this.f1588b.a(new l(executor, aVar, c0Var));
        w();
        return c0Var;
    }

    @Override // com.google.android.gms.tasks.Task
    @NonNull
    public final <TContinuationResult> Task<TContinuationResult> j(@NonNull Executor executor, @NonNull a<TResult, Task<TContinuationResult>> aVar) {
        c0 c0Var = new c0();
        this.f1588b.a(new n(executor, aVar, c0Var));
        w();
        return c0Var;
    }

    @Override // com.google.android.gms.tasks.Task
    @Nullable
    public final Exception k() {
        Exception exc;
        synchronized (this.a) {
            exc = this.f;
        }
        return exc;
    }

    @Override // com.google.android.gms.tasks.Task
    public final TResult l() {
        TResult tresult;
        synchronized (this.a) {
            d.G(this.c, "Task is not yet complete");
            if (!this.d) {
                Exception exc = this.f;
                if (exc == null) {
                    tresult = this.e;
                } else {
                    throw new RuntimeExecutionException(exc);
                }
            } else {
                throw new CancellationException("Task is already canceled.");
            }
        }
        return tresult;
    }

    @Override // com.google.android.gms.tasks.Task
    public final <X extends Throwable> TResult m(@NonNull Class<X> cls) throws Throwable {
        TResult tresult;
        synchronized (this.a) {
            d.G(this.c, "Task is not yet complete");
            if (this.d) {
                throw new CancellationException("Task is already canceled.");
            } else if (!cls.isInstance(this.f)) {
                Exception exc = this.f;
                if (exc == null) {
                    tresult = this.e;
                } else {
                    throw new RuntimeExecutionException(exc);
                }
            } else {
                throw cls.cast(this.f);
            }
        }
        return tresult;
    }

    @Override // com.google.android.gms.tasks.Task
    public final boolean n() {
        return this.d;
    }

    @Override // com.google.android.gms.tasks.Task
    public final boolean o() {
        boolean z2;
        synchronized (this.a) {
            z2 = this.c;
        }
        return z2;
    }

    @Override // com.google.android.gms.tasks.Task
    public final boolean p() {
        boolean z2;
        synchronized (this.a) {
            z2 = false;
            if (this.c && !this.d && this.f == null) {
                z2 = true;
            }
        }
        return z2;
    }

    @Override // com.google.android.gms.tasks.Task
    @NonNull
    public final <TContinuationResult> Task<TContinuationResult> q(@NonNull f<TResult, TContinuationResult> fVar) {
        Executor executor = g.a;
        c0 c0Var = new c0();
        this.f1588b.a(new x(executor, fVar, c0Var));
        w();
        return c0Var;
    }

    @Override // com.google.android.gms.tasks.Task
    @NonNull
    public final <TContinuationResult> Task<TContinuationResult> r(Executor executor, f<TResult, TContinuationResult> fVar) {
        c0 c0Var = new c0();
        this.f1588b.a(new x(executor, fVar, c0Var));
        w();
        return c0Var;
    }

    public final void s(@Nullable TResult tresult) {
        synchronized (this.a) {
            v();
            this.c = true;
            this.e = tresult;
        }
        this.f1588b.b(this);
    }

    public final void t(@NonNull Exception exc) {
        d.z(exc, "Exception must not be null");
        synchronized (this.a) {
            v();
            this.c = true;
            this.f = exc;
        }
        this.f1588b.b(this);
    }

    public final boolean u() {
        synchronized (this.a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.d = true;
            this.f1588b.b(this);
            return true;
        }
    }

    public final void v() {
        String str;
        if (this.c) {
            int i = DuplicateTaskCompletionException.j;
            if (o()) {
                Exception k = k();
                if (k != null) {
                    str = "failure";
                } else if (p()) {
                    String valueOf = String.valueOf(l());
                    str = a.H(new StringBuilder(valueOf.length() + 7), "result ", valueOf);
                } else {
                    str = n() ? "cancellation" : "unknown issue";
                }
                String valueOf2 = String.valueOf(str);
                throw new DuplicateTaskCompletionException(valueOf2.length() != 0 ? "Complete with: ".concat(valueOf2) : new String("Complete with: "), k);
            }
            throw new IllegalStateException("DuplicateTaskCompletionException can only be created from completed Task.");
        }
    }

    public final void w() {
        synchronized (this.a) {
            if (this.c) {
                this.f1588b.b(this);
            }
        }
    }
}
