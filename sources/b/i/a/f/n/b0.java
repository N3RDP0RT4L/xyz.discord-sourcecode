package b.i.a.f.n;

import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import b.i.a.f.h.p.a;
import java.util.concurrent.Executor;
/* compiled from: com.google.android.gms:play-services-tasks@@17.2.1 */
/* loaded from: classes3.dex */
public final class b0 implements Executor {
    public final Handler j = new a(Looper.getMainLooper());

    @Override // java.util.concurrent.Executor
    public final void execute(@NonNull Runnable runnable) {
        this.j.post(runnable);
    }
}
