package b.i.a.f.n;

import com.google.android.gms.tasks.RuntimeExecutionException;
import com.google.android.gms.tasks.Task;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;
/* compiled from: com.google.android.gms:play-services-tasks@@17.2.1 */
/* loaded from: classes3.dex */
public final class w implements Runnable {
    public final /* synthetic */ Task j;
    public final /* synthetic */ x k;

    public w(x xVar, Task task) {
        this.k = xVar;
        this.j = task;
    }

    @Override // java.lang.Runnable
    public final void run() {
        try {
            Task a = this.k.f1597b.a(this.j.l());
            if (a == null) {
                x xVar = this.k;
                xVar.c.t(new NullPointerException("Continuation returned null"));
                return;
            }
            Executor executor = g.f1589b;
            a.g(executor, this.k);
            a.e(executor, this.k);
            a.a(executor, this.k);
        } catch (RuntimeExecutionException e) {
            if (e.getCause() instanceof Exception) {
                this.k.c.t((Exception) e.getCause());
                return;
            }
            this.k.c.t(e);
        } catch (CancellationException unused) {
            this.k.c.u();
        } catch (Exception e2) {
            this.k.c.t(e2);
        }
    }
}
