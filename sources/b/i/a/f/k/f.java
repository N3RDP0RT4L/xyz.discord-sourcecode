package b.i.a.f.k;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.safetynet.zzh;
/* loaded from: classes3.dex */
public final class f implements Parcelable.Creator<zzh> {
    @Override // android.os.Parcelable.Creator
    public final zzh createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        int i = 0;
        boolean z2 = false;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 2) {
                i = d.G1(parcel, readInt);
            } else if (c != 3) {
                d.d2(parcel, readInt);
            } else {
                z2 = d.E1(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        return new zzh(i, z2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzh[] newArray(int i) {
        return new zzh[i];
    }
}
