package b.i.a.f.k;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.safetynet.SafeBrowsingData;
/* loaded from: classes3.dex */
public final class g implements Parcelable.Creator<SafeBrowsingData> {
    public static void a(SafeBrowsingData safeBrowsingData, Parcel parcel, int i) {
        int y2 = d.y2(parcel, 20293);
        d.t2(parcel, 2, safeBrowsingData.j, false);
        d.s2(parcel, 3, safeBrowsingData.k, i, false);
        d.s2(parcel, 4, safeBrowsingData.l, i, false);
        long j = safeBrowsingData.m;
        parcel.writeInt(524293);
        parcel.writeLong(j);
        d.q2(parcel, 6, safeBrowsingData.n, false);
        d.A2(parcel, y2);
    }

    @Override // android.os.Parcelable.Creator
    public final SafeBrowsingData createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        String str = null;
        long j = 0;
        DataHolder dataHolder = null;
        ParcelFileDescriptor parcelFileDescriptor = null;
        byte[] bArr = null;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 2) {
                str = d.R(parcel, readInt);
            } else if (c == 3) {
                dataHolder = (DataHolder) d.Q(parcel, readInt, DataHolder.CREATOR);
            } else if (c == 4) {
                parcelFileDescriptor = (ParcelFileDescriptor) d.Q(parcel, readInt, ParcelFileDescriptor.CREATOR);
            } else if (c == 5) {
                j = d.H1(parcel, readInt);
            } else if (c != 6) {
                d.d2(parcel, readInt);
            } else {
                bArr = d.N(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        return new SafeBrowsingData(str, dataHolder, parcelFileDescriptor, j, bArr);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ SafeBrowsingData[] newArray(int i) {
        return new SafeBrowsingData[i];
    }
}
