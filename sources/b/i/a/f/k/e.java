package b.i.a.f.k;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import com.google.android.gms.safetynet.zzf;
/* loaded from: classes3.dex */
public final class e implements Parcelable.Creator<zzf> {
    @Override // android.os.Parcelable.Creator
    public final zzf createFromParcel(Parcel parcel) {
        int m2 = d.m2(parcel);
        String str = null;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            if (((char) readInt) != 2) {
                d.d2(parcel, readInt);
            } else {
                str = d.R(parcel, readInt);
            }
        }
        d.f0(parcel, m2);
        return new zzf(str);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzf[] newArray(int i) {
        return new zzf[i];
    }
}
