package b.i.a.f.k;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.safetynet.HarmfulAppsData;
import com.google.android.gms.safetynet.zzd;
/* loaded from: classes3.dex */
public final class d implements Parcelable.Creator<zzd> {
    @Override // android.os.Parcelable.Creator
    public final zzd createFromParcel(Parcel parcel) {
        int m2 = b.c.a.a0.d.m2(parcel);
        long j = 0;
        HarmfulAppsData[] harmfulAppsDataArr = null;
        int i = 0;
        boolean z2 = false;
        while (parcel.dataPosition() < m2) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 2) {
                j = b.c.a.a0.d.H1(parcel, readInt);
            } else if (c == 3) {
                harmfulAppsDataArr = (HarmfulAppsData[]) b.c.a.a0.d.U(parcel, readInt, HarmfulAppsData.CREATOR);
            } else if (c == 4) {
                i = b.c.a.a0.d.G1(parcel, readInt);
            } else if (c != 5) {
                b.c.a.a0.d.d2(parcel, readInt);
            } else {
                z2 = b.c.a.a0.d.E1(parcel, readInt);
            }
        }
        b.c.a.a0.d.f0(parcel, m2);
        return new zzd(j, harmfulAppsDataArr, i, z2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzd[] newArray(int i) {
        return new zzd[i];
    }
}
