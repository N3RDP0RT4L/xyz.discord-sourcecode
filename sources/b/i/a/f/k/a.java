package b.i.a.f.k;

import b.i.a.f.e.h.a;
import b.i.a.f.h.o.i;
import b.i.a.f.h.o.l;
import com.google.android.gms.safetynet.SafetyNetApi;
/* loaded from: classes3.dex */
public final class a {
    public static final a.g<l> a;

    /* renamed from: b  reason: collision with root package name */
    public static final a.AbstractC0111a<l, a.d.c> f1584b;
    @Deprecated
    public static final b.i.a.f.e.h.a<a.d.c> c;
    @Deprecated
    public static final SafetyNetApi d = new i();

    static {
        a.g<l> gVar = new a.g<>();
        a = gVar;
        h hVar = new h();
        f1584b = hVar;
        c = new b.i.a.f.e.h.a<>("SafetyNet.API", hVar, gVar);
    }
}
