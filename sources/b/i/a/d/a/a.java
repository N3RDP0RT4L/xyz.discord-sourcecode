package b.i.a.d.a;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import b.i.a.a.b;
import b.i.a.a.c;
/* compiled from: IGetInstallReferrerService.java */
/* loaded from: classes3.dex */
public interface a extends IInterface {

    /* compiled from: IGetInstallReferrerService.java */
    /* renamed from: b.i.a.d.a.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static abstract class AbstractBinderC0106a extends b implements a {
        public static final /* synthetic */ int a = 0;

        /* compiled from: IGetInstallReferrerService.java */
        /* renamed from: b.i.a.d.a.a$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static class C0107a extends b.i.a.a.a implements a {
            public C0107a(IBinder iBinder) {
                super(iBinder);
            }

            @Override // b.i.a.d.a.a
            public final Bundle B(Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                obtain.writeInterfaceToken("com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
                int i = c.a;
                obtain.writeInt(1);
                bundle.writeToParcel(obtain, 0);
                obtain = Parcel.obtain();
                try {
                    this.a.transact(1, obtain, obtain, 0);
                    obtain.readException();
                    obtain.recycle();
                    return (Bundle) (obtain.readInt() == 0 ? null : (Parcelable) Bundle.CREATOR.createFromParcel(obtain));
                } catch (RuntimeException e) {
                    throw e;
                } finally {
                    obtain.recycle();
                }
            }
        }
    }

    Bundle B(Bundle bundle) throws RemoteException;
}
