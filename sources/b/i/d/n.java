package b.i.d;
/* JADX WARN: Failed to restore enum class, 'enum' modifier removed */
/* compiled from: LongSerializationPolicy.java */
/* loaded from: classes3.dex */
public abstract class n extends Enum<n> {
    public static final n j;
    public static final n k;
    public static final /* synthetic */ n[] l;

    /* JADX WARN: Failed to restore enum class, 'enum' modifier removed */
    /* compiled from: LongSerializationPolicy.java */
    /* loaded from: classes3.dex */
    public final class a extends n {
        public a(String str, int i) {
            super(str, i, null);
        }
    }

    static {
        a aVar = new a("DEFAULT", 0);
        j = aVar;
        n bVar = new n("STRING", 1) { // from class: b.i.d.n.b
        };
        k = bVar;
        l = new n[]{aVar, bVar};
    }

    public n(String str, int i, a aVar) {
    }

    public static n valueOf(String str) {
        return (n) Enum.valueOf(n.class, str);
    }

    public static n[] values() {
        return (n[]) l.clone();
    }
}
