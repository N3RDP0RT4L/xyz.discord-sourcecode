package b.i.d.q;
/* compiled from: UnsafeAllocator.java */
/* loaded from: classes3.dex */
public class v extends w {
    @Override // b.i.d.q.w
    public <T> T b(Class<T> cls) {
        throw new UnsupportedOperationException("Cannot allocate " + cls);
    }
}
