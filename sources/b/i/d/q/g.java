package b.i.d.q;

import b.i.d.f;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Constructor;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;
/* compiled from: ConstructorConstructor.java */
/* loaded from: classes3.dex */
public final class g {
    public final Map<Type, f<?>> a;

    /* renamed from: b  reason: collision with root package name */
    public final b.i.d.q.y.b f1804b = b.i.d.q.y.b.a;

    /* compiled from: ConstructorConstructor.java */
    /* loaded from: classes3.dex */
    public class a implements r<T> {
        public final /* synthetic */ f a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ Type f1805b;

        public a(g gVar, f fVar, Type type) {
            this.a = fVar;
            this.f1805b = type;
        }

        /* JADX WARN: Type inference failed for: r0v1, types: [T, java.lang.Object] */
        @Override // b.i.d.q.r
        public T a() {
            return this.a.a(this.f1805b);
        }
    }

    /* compiled from: ConstructorConstructor.java */
    /* loaded from: classes3.dex */
    public class b implements r<T> {
        public final /* synthetic */ f a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ Type f1806b;

        public b(g gVar, f fVar, Type type) {
            this.a = fVar;
            this.f1806b = type;
        }

        /* JADX WARN: Type inference failed for: r0v1, types: [T, java.lang.Object] */
        @Override // b.i.d.q.r
        public T a() {
            return this.a.a(this.f1806b);
        }
    }

    public g(Map<Type, f<?>> map) {
        this.a = map;
    }

    public <T> r<T> a(TypeToken<T> typeToken) {
        h hVar;
        Type type = typeToken.getType();
        Class<? super T> rawType = typeToken.getRawType();
        f<?> fVar = this.a.get(type);
        if (fVar != null) {
            return new a(this, fVar, type);
        }
        f<?> fVar2 = this.a.get(rawType);
        if (fVar2 != null) {
            return new b(this, fVar2, type);
        }
        r<T> rVar = null;
        try {
            Constructor<? super T> declaredConstructor = rawType.getDeclaredConstructor(new Class[0]);
            if (!declaredConstructor.isAccessible()) {
                this.f1804b.a(declaredConstructor);
            }
            hVar = new h(this, declaredConstructor);
        } catch (NoSuchMethodException unused) {
            hVar = null;
        }
        if (hVar != null) {
            return hVar;
        }
        if (Collection.class.isAssignableFrom(rawType)) {
            if (SortedSet.class.isAssignableFrom(rawType)) {
                rVar = new i(this);
            } else if (EnumSet.class.isAssignableFrom(rawType)) {
                rVar = new j(this, type);
            } else if (Set.class.isAssignableFrom(rawType)) {
                rVar = new k(this);
            } else if (Queue.class.isAssignableFrom(rawType)) {
                rVar = new l(this);
            } else {
                rVar = new m(this);
            }
        } else if (Map.class.isAssignableFrom(rawType)) {
            if (ConcurrentNavigableMap.class.isAssignableFrom(rawType)) {
                rVar = new n(this);
            } else if (ConcurrentMap.class.isAssignableFrom(rawType)) {
                rVar = new b.i.d.q.b(this);
            } else if (SortedMap.class.isAssignableFrom(rawType)) {
                rVar = new c(this);
            } else if (!(type instanceof ParameterizedType) || String.class.isAssignableFrom(TypeToken.get(((ParameterizedType) type).getActualTypeArguments()[0]).getRawType())) {
                rVar = new e(this);
            } else {
                rVar = new d(this);
            }
        }
        return rVar != null ? rVar : new f(this, rawType, type);
    }

    public String toString() {
        return this.a.toString();
    }
}
