package b.i.d.q.y;

import com.google.gson.JsonIOException;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
/* compiled from: UnsafeReflectionAccessor.java */
/* loaded from: classes3.dex */
public final class c extends b {

    /* renamed from: b  reason: collision with root package name */
    public static Class f1812b;
    public final Object c;
    public final Field d;

    public c() {
        Object obj;
        Field field = null;
        try {
            Class<?> cls = Class.forName("sun.misc.Unsafe");
            f1812b = cls;
            Field declaredField = cls.getDeclaredField("theUnsafe");
            declaredField.setAccessible(true);
            obj = declaredField.get(null);
        } catch (Exception unused) {
            obj = null;
        }
        this.c = obj;
        try {
            field = AccessibleObject.class.getDeclaredField("override");
        } catch (NoSuchFieldException unused2) {
        }
        this.d = field;
    }

    @Override // b.i.d.q.y.b
    public void a(AccessibleObject accessibleObject) {
        boolean z2 = false;
        if (!(this.c == null || this.d == null)) {
            try {
                f1812b.getMethod("putBoolean", Object.class, Long.TYPE, Boolean.TYPE).invoke(this.c, accessibleObject, Long.valueOf(((Long) f1812b.getMethod("objectFieldOffset", Field.class).invoke(this.c, this.d)).longValue()), Boolean.TRUE);
                z2 = true;
            } catch (Exception unused) {
            }
        }
        if (!z2) {
            try {
                accessibleObject.setAccessible(true);
            } catch (SecurityException e) {
                throw new JsonIOException("Gson couldn't modify fields for " + accessibleObject + "\nand sun.misc.Unsafe not found.\nEither write a custom type adapter, or make fields accessible, or include sun.misc.Unsafe.", e);
            }
        }
    }
}
