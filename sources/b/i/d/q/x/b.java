package b.i.d.q.x;

import b.i.d.g;
import b.i.d.j;
import b.i.d.k;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
/* compiled from: JsonTreeWriter.java */
/* loaded from: classes3.dex */
public final class b extends JsonWriter {
    public static final Writer u = new a();
    public static final k v = new k("closed");

    /* renamed from: x  reason: collision with root package name */
    public String f1810x;
    public final List<JsonElement> w = new ArrayList();

    /* renamed from: y  reason: collision with root package name */
    public JsonElement f1811y = j.a;

    /* compiled from: JsonTreeWriter.java */
    /* loaded from: classes3.dex */
    public class a extends Writer {
        @Override // java.io.Writer, java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
            throw new AssertionError();
        }

        @Override // java.io.Writer, java.io.Flushable
        public void flush() throws IOException {
            throw new AssertionError();
        }

        @Override // java.io.Writer
        public void write(char[] cArr, int i, int i2) {
            throw new AssertionError();
        }
    }

    public b() {
        super(u);
    }

    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter A(long j) throws IOException {
        O(new k(Long.valueOf(j)));
        return this;
    }

    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter C(Boolean bool) throws IOException {
        if (bool == null) {
            O(j.a);
            return this;
        }
        O(new k(bool));
        return this;
    }

    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter D(Number number) throws IOException {
        if (number == null) {
            O(j.a);
            return this;
        }
        if (!this.q) {
            double doubleValue = number.doubleValue();
            if (Double.isNaN(doubleValue) || Double.isInfinite(doubleValue)) {
                throw new IllegalArgumentException("JSON forbids NaN and infinities: " + number);
            }
        }
        O(new k(number));
        return this;
    }

    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter H(String str) throws IOException {
        if (str == null) {
            O(j.a);
            return this;
        }
        O(new k(str));
        return this;
    }

    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter I(boolean z2) throws IOException {
        O(new k(Boolean.valueOf(z2)));
        return this;
    }

    public JsonElement L() {
        if (this.w.isEmpty()) {
            return this.f1811y;
        }
        StringBuilder R = b.d.b.a.a.R("Expected one JSON element but was ");
        R.append(this.w);
        throw new IllegalStateException(R.toString());
    }

    public final JsonElement N() {
        List<JsonElement> list = this.w;
        return list.get(list.size() - 1);
    }

    public final void O(JsonElement jsonElement) {
        if (this.f1810x != null) {
            if (!(jsonElement instanceof j) || this.t) {
                ((JsonObject) N()).a.put(this.f1810x, jsonElement);
            }
            this.f1810x = null;
        } else if (this.w.isEmpty()) {
            this.f1811y = jsonElement;
        } else {
            JsonElement N = N();
            if (N instanceof g) {
                ((g) N).j.add(jsonElement);
                return;
            }
            throw new IllegalStateException();
        }
    }

    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter b() throws IOException {
        g gVar = new g();
        O(gVar);
        this.w.add(gVar);
        return this;
    }

    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter c() throws IOException {
        JsonObject jsonObject = new JsonObject();
        O(jsonObject);
        this.w.add(jsonObject);
        return this;
    }

    @Override // com.google.gson.stream.JsonWriter, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        if (this.w.isEmpty()) {
            this.w.add(v);
            return;
        }
        throw new IOException("Incomplete document");
    }

    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter e() throws IOException {
        if (this.w.isEmpty() || this.f1810x != null) {
            throw new IllegalStateException();
        } else if (N() instanceof g) {
            List<JsonElement> list = this.w;
            list.remove(list.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter f() throws IOException {
        if (this.w.isEmpty() || this.f1810x != null) {
            throw new IllegalStateException();
        } else if (N() instanceof JsonObject) {
            List<JsonElement> list = this.w;
            list.remove(list.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    @Override // com.google.gson.stream.JsonWriter, java.io.Flushable
    public void flush() throws IOException {
    }

    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter n(String str) throws IOException {
        if (this.w.isEmpty() || this.f1810x != null) {
            throw new IllegalStateException();
        } else if (N() instanceof JsonObject) {
            this.f1810x = str;
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter s() throws IOException {
        O(j.a);
        return this;
    }
}
