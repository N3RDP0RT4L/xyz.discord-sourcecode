package b.i.d.q.x;

import andhook.lib.xposed.ClassUtils;
import b.i.d.g;
import b.i.d.j;
import b.i.d.k;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
/* compiled from: JsonTreeReader.java */
/* loaded from: classes3.dex */
public final class a extends JsonReader {
    public Object[] B = new Object[32];
    public int C = 0;
    public String[] D = new String[32];
    public int[] E = new int[32];

    /* renamed from: z  reason: collision with root package name */
    public static final Reader f1809z = new C0156a();
    public static final Object A = new Object();

    /* compiled from: JsonTreeReader.java */
    /* renamed from: b.i.d.q.x.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public class C0156a extends Reader {
        @Override // java.io.Reader, java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
            throw new AssertionError();
        }

        @Override // java.io.Reader
        public int read(char[] cArr, int i, int i2) throws IOException {
            throw new AssertionError();
        }
    }

    public a(JsonElement jsonElement) {
        super(f1809z);
        c0(jsonElement);
    }

    private String t() {
        StringBuilder R = b.d.b.a.a.R(" at path ");
        R.append(getPath());
        return R.toString();
    }

    @Override // com.google.gson.stream.JsonReader
    public long A() throws IOException {
        JsonToken N = N();
        JsonToken jsonToken = JsonToken.NUMBER;
        if (N == jsonToken || N == JsonToken.STRING) {
            k kVar = (k) X();
            long longValue = kVar.a instanceof Number ? kVar.i().longValue() : Long.parseLong(kVar.g());
            b0();
            int i = this.C;
            if (i > 0) {
                int[] iArr = this.E;
                int i2 = i - 1;
                iArr[i2] = iArr[i2] + 1;
            }
            return longValue;
        }
        throw new IllegalStateException("Expected " + jsonToken + " but was " + N + t());
    }

    @Override // com.google.gson.stream.JsonReader
    public String C() throws IOException {
        W(JsonToken.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) X()).next();
        String str = (String) entry.getKey();
        this.D[this.C - 1] = str;
        c0(entry.getValue());
        return str;
    }

    @Override // com.google.gson.stream.JsonReader
    public void H() throws IOException {
        W(JsonToken.NULL);
        b0();
        int i = this.C;
        if (i > 0) {
            int[] iArr = this.E;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
    }

    @Override // com.google.gson.stream.JsonReader
    public String J() throws IOException {
        JsonToken N = N();
        JsonToken jsonToken = JsonToken.STRING;
        if (N == jsonToken || N == JsonToken.NUMBER) {
            String g = ((k) b0()).g();
            int i = this.C;
            if (i > 0) {
                int[] iArr = this.E;
                int i2 = i - 1;
                iArr[i2] = iArr[i2] + 1;
            }
            return g;
        }
        throw new IllegalStateException("Expected " + jsonToken + " but was " + N + t());
    }

    @Override // com.google.gson.stream.JsonReader
    public JsonToken N() throws IOException {
        if (this.C == 0) {
            return JsonToken.END_DOCUMENT;
        }
        Object X = X();
        if (X instanceof Iterator) {
            boolean z2 = this.B[this.C - 2] instanceof JsonObject;
            Iterator it = (Iterator) X;
            if (!it.hasNext()) {
                return z2 ? JsonToken.END_OBJECT : JsonToken.END_ARRAY;
            }
            if (z2) {
                return JsonToken.NAME;
            }
            c0(it.next());
            return N();
        } else if (X instanceof JsonObject) {
            return JsonToken.BEGIN_OBJECT;
        } else {
            if (X instanceof g) {
                return JsonToken.BEGIN_ARRAY;
            }
            if (X instanceof k) {
                Object obj = ((k) X).a;
                if (obj instanceof String) {
                    return JsonToken.STRING;
                }
                if (obj instanceof Boolean) {
                    return JsonToken.BOOLEAN;
                }
                if (obj instanceof Number) {
                    return JsonToken.NUMBER;
                }
                throw new AssertionError();
            } else if (X instanceof j) {
                return JsonToken.NULL;
            } else {
                if (X == A) {
                    throw new IllegalStateException("JsonReader is closed");
                }
                throw new AssertionError();
            }
        }
    }

    @Override // com.google.gson.stream.JsonReader
    public void U() throws IOException {
        if (N() == JsonToken.NAME) {
            C();
            this.D[this.C - 2] = "null";
        } else {
            b0();
            int i = this.C;
            if (i > 0) {
                this.D[i - 1] = "null";
            }
        }
        int i2 = this.C;
        if (i2 > 0) {
            int[] iArr = this.E;
            int i3 = i2 - 1;
            iArr[i3] = iArr[i3] + 1;
        }
    }

    public final void W(JsonToken jsonToken) throws IOException {
        if (N() != jsonToken) {
            throw new IllegalStateException("Expected " + jsonToken + " but was " + N() + t());
        }
    }

    public final Object X() {
        return this.B[this.C - 1];
    }

    @Override // com.google.gson.stream.JsonReader
    public void a() throws IOException {
        W(JsonToken.BEGIN_ARRAY);
        c0(((g) X()).iterator());
        this.E[this.C - 1] = 0;
    }

    @Override // com.google.gson.stream.JsonReader
    public void b() throws IOException {
        W(JsonToken.BEGIN_OBJECT);
        c0(new LinkedTreeMap.b.a((LinkedTreeMap.b) ((JsonObject) X()).j()));
    }

    public final Object b0() {
        Object[] objArr = this.B;
        int i = this.C - 1;
        this.C = i;
        Object obj = objArr[i];
        objArr[i] = null;
        return obj;
    }

    public final void c0(Object obj) {
        int i = this.C;
        Object[] objArr = this.B;
        if (i == objArr.length) {
            int i2 = i * 2;
            this.B = Arrays.copyOf(objArr, i2);
            this.E = Arrays.copyOf(this.E, i2);
            this.D = (String[]) Arrays.copyOf(this.D, i2);
        }
        Object[] objArr2 = this.B;
        int i3 = this.C;
        this.C = i3 + 1;
        objArr2[i3] = obj;
    }

    @Override // com.google.gson.stream.JsonReader, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.B = new Object[]{A};
        this.C = 1;
    }

    @Override // com.google.gson.stream.JsonReader
    public void e() throws IOException {
        W(JsonToken.END_ARRAY);
        b0();
        b0();
        int i = this.C;
        if (i > 0) {
            int[] iArr = this.E;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
    }

    @Override // com.google.gson.stream.JsonReader
    public void f() throws IOException {
        W(JsonToken.END_OBJECT);
        b0();
        b0();
        int i = this.C;
        if (i > 0) {
            int[] iArr = this.E;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
    }

    @Override // com.google.gson.stream.JsonReader
    public String getPath() {
        StringBuilder O = b.d.b.a.a.O(ClassUtils.INNER_CLASS_SEPARATOR_CHAR);
        int i = 0;
        while (i < this.C) {
            Object[] objArr = this.B;
            if (objArr[i] instanceof g) {
                i++;
                if (objArr[i] instanceof Iterator) {
                    O.append('[');
                    O.append(this.E[i]);
                    O.append(']');
                }
            } else if (objArr[i] instanceof JsonObject) {
                i++;
                if (objArr[i] instanceof Iterator) {
                    O.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
                    String[] strArr = this.D;
                    if (strArr[i] != null) {
                        O.append(strArr[i]);
                    }
                }
            }
            i++;
        }
        return O.toString();
    }

    @Override // com.google.gson.stream.JsonReader
    public boolean q() throws IOException {
        JsonToken N = N();
        return (N == JsonToken.END_OBJECT || N == JsonToken.END_ARRAY) ? false : true;
    }

    @Override // com.google.gson.stream.JsonReader
    public String toString() {
        return a.class.getSimpleName();
    }

    @Override // com.google.gson.stream.JsonReader
    public boolean u() throws IOException {
        W(JsonToken.BOOLEAN);
        boolean h = ((k) b0()).h();
        int i = this.C;
        if (i > 0) {
            int[] iArr = this.E;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
        return h;
    }

    @Override // com.google.gson.stream.JsonReader
    public double x() throws IOException {
        JsonToken N = N();
        JsonToken jsonToken = JsonToken.NUMBER;
        if (N == jsonToken || N == JsonToken.STRING) {
            k kVar = (k) X();
            double doubleValue = kVar.a instanceof Number ? kVar.i().doubleValue() : Double.parseDouble(kVar.g());
            if (this.l || (!Double.isNaN(doubleValue) && !Double.isInfinite(doubleValue))) {
                b0();
                int i = this.C;
                if (i > 0) {
                    int[] iArr = this.E;
                    int i2 = i - 1;
                    iArr[i2] = iArr[i2] + 1;
                }
                return doubleValue;
            }
            throw new NumberFormatException("JSON forbids NaN and infinities: " + doubleValue);
        }
        throw new IllegalStateException("Expected " + jsonToken + " but was " + N + t());
    }

    @Override // com.google.gson.stream.JsonReader
    public int y() throws IOException {
        JsonToken N = N();
        JsonToken jsonToken = JsonToken.NUMBER;
        if (N == jsonToken || N == JsonToken.STRING) {
            int c = ((k) X()).c();
            b0();
            int i = this.C;
            if (i > 0) {
                int[] iArr = this.E;
                int i2 = i - 1;
                iArr[i2] = iArr[i2] + 1;
            }
            return c;
        }
        throw new IllegalStateException("Expected " + jsonToken + " but was " + N + t());
    }
}
