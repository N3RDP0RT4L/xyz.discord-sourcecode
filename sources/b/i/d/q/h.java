package b.i.d.q;

import b.d.b.a.a;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
/* compiled from: ConstructorConstructor.java */
/* loaded from: classes3.dex */
public class h implements r<T> {
    public final /* synthetic */ Constructor a;

    public h(g gVar, Constructor constructor) {
        this.a = constructor;
    }

    /* JADX WARN: Type inference failed for: r0v6, types: [T, java.lang.Object] */
    @Override // b.i.d.q.r
    public T a() {
        try {
            return this.a.newInstance(null);
        } catch (IllegalAccessException e) {
            throw new AssertionError(e);
        } catch (InstantiationException e2) {
            StringBuilder R = a.R("Failed to invoke ");
            R.append(this.a);
            R.append(" with no args");
            throw new RuntimeException(R.toString(), e2);
        } catch (InvocationTargetException e3) {
            StringBuilder R2 = a.R("Failed to invoke ");
            R2.append(this.a);
            R2.append(" with no args");
            throw new RuntimeException(R2.toString(), e3.getTargetException());
        }
    }
}
