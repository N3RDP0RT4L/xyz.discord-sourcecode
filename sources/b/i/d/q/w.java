package b.i.d.q;

import b.d.b.a.a;
import java.lang.reflect.Modifier;
/* compiled from: UnsafeAllocator.java */
/* loaded from: classes3.dex */
public abstract class w {
    public static void a(Class<?> cls) {
        int modifiers = cls.getModifiers();
        if (Modifier.isInterface(modifiers)) {
            StringBuilder R = a.R("Interface can't be instantiated! Interface name: ");
            R.append(cls.getName());
            throw new UnsupportedOperationException(R.toString());
        } else if (Modifier.isAbstract(modifiers)) {
            StringBuilder R2 = a.R("Abstract class can't be instantiated! Class name: ");
            R2.append(cls.getName());
            throw new UnsupportedOperationException(R2.toString());
        }
    }

    public abstract <T> T b(Class<T> cls) throws Exception;
}
