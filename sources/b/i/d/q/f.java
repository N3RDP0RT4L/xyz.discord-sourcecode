package b.i.d.q;

import b.d.b.a.a;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
/* compiled from: ConstructorConstructor.java */
/* loaded from: classes3.dex */
public class f implements r<T> {
    public final w a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ Class f1803b;
    public final /* synthetic */ Type c;

    public f(g gVar, Class cls, Type type) {
        w wVar;
        this.f1803b = cls;
        this.c = type;
        try {
            Class<?> cls2 = Class.forName("sun.misc.Unsafe");
            Field declaredField = cls2.getDeclaredField("theUnsafe");
            declaredField.setAccessible(true);
            wVar = new s(cls2.getMethod("allocateInstance", Class.class), declaredField.get(null));
        } catch (Exception unused) {
            try {
                try {
                    Method declaredMethod = ObjectStreamClass.class.getDeclaredMethod("getConstructorId", Class.class);
                    declaredMethod.setAccessible(true);
                    int intValue = ((Integer) declaredMethod.invoke(null, Object.class)).intValue();
                    Method declaredMethod2 = ObjectStreamClass.class.getDeclaredMethod("newInstance", Class.class, Integer.TYPE);
                    declaredMethod2.setAccessible(true);
                    wVar = new t(declaredMethod2, intValue);
                } catch (Exception unused2) {
                    Method declaredMethod3 = ObjectInputStream.class.getDeclaredMethod("newInstance", Class.class, Class.class);
                    declaredMethod3.setAccessible(true);
                    wVar = new u(declaredMethod3);
                }
            } catch (Exception unused3) {
                wVar = new v();
            }
        }
        this.a = wVar;
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [T, java.lang.Object] */
    @Override // b.i.d.q.r
    public T a() {
        try {
            return this.a.b(this.f1803b);
        } catch (Exception e) {
            StringBuilder R = a.R("Unable to invoke no-args constructor for ");
            R.append(this.c);
            R.append(". Registering an InstanceCreator with Gson for this type may fix this problem.");
            throw new RuntimeException(R.toString(), e);
        }
    }
}
