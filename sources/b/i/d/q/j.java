package b.i.d.q;

import b.d.b.a.a;
import com.google.gson.JsonIOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.EnumSet;
/* compiled from: ConstructorConstructor.java */
/* loaded from: classes3.dex */
public class j implements r<T> {
    public final /* synthetic */ Type a;

    public j(g gVar, Type type) {
        this.a = type;
    }

    /* JADX WARN: Type inference failed for: r0v7, types: [T, java.util.EnumSet] */
    @Override // b.i.d.q.r
    public T a() {
        Type type = this.a;
        if (type instanceof ParameterizedType) {
            Type type2 = ((ParameterizedType) type).getActualTypeArguments()[0];
            if (type2 instanceof Class) {
                return EnumSet.noneOf((Class) type2);
            }
            StringBuilder R = a.R("Invalid EnumSet type: ");
            R.append(this.a.toString());
            throw new JsonIOException(R.toString());
        }
        StringBuilder R2 = a.R("Invalid EnumSet type: ");
        R2.append(this.a.toString());
        throw new JsonIOException(R2.toString());
    }
}
