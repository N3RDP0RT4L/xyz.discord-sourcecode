package b.i.d;

import com.google.gson.JsonElement;
/* compiled from: JsonNull.java */
/* loaded from: classes3.dex */
public final class j extends JsonElement {
    public static final j a = new j();

    public boolean equals(Object obj) {
        return this == obj || (obj instanceof j);
    }

    public int hashCode() {
        return j.class.hashCode();
    }
}
