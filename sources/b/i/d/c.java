package b.i.d;

import java.lang.reflect.Field;
import java.util.Locale;
/* JADX WARN: Failed to restore enum class, 'enum' modifier removed */
/* compiled from: FieldNamingPolicy.java */
/* loaded from: classes3.dex */
public abstract class c extends Enum<c> implements b.i.d.d {
    public static final c j;
    public static final c k;
    public static final c l;
    public static final c m;
    public static final c n;
    public static final c o;
    public static final /* synthetic */ c[] p;

    /* JADX WARN: Failed to restore enum class, 'enum' modifier removed */
    /* compiled from: FieldNamingPolicy.java */
    /* loaded from: classes3.dex */
    public final class a extends c {
        public a(String str, int i) {
            super(str, i, null);
        }

        @Override // b.i.d.d
        public String f(Field field) {
            return field.getName();
        }
    }

    static {
        a aVar = new a("IDENTITY", 0);
        j = aVar;
        c bVar = new c("UPPER_CAMEL_CASE", 1) { // from class: b.i.d.c.b
            @Override // b.i.d.d
            public String f(Field field) {
                return c.h(field.getName());
            }
        };
        k = bVar;
        c cVar = new c("UPPER_CAMEL_CASE_WITH_SPACES", 2) { // from class: b.i.d.c.c
            @Override // b.i.d.d
            public String f(Field field) {
                return c.h(c.g(field.getName(), " "));
            }
        };
        l = cVar;
        c dVar = new c("LOWER_CASE_WITH_UNDERSCORES", 3) { // from class: b.i.d.c.d
            @Override // b.i.d.d
            public String f(Field field) {
                return c.g(field.getName(), "_").toLowerCase(Locale.ENGLISH);
            }
        };
        m = dVar;
        c eVar = new c("LOWER_CASE_WITH_DASHES", 4) { // from class: b.i.d.c.e
            @Override // b.i.d.d
            public String f(Field field) {
                return c.g(field.getName(), "-").toLowerCase(Locale.ENGLISH);
            }
        };
        n = eVar;
        c fVar = new c("LOWER_CASE_WITH_DOTS", 5) { // from class: b.i.d.c.f
            @Override // b.i.d.d
            public String f(Field field) {
                return c.g(field.getName(), ".").toLowerCase(Locale.ENGLISH);
            }
        };
        o = fVar;
        p = new c[]{aVar, bVar, cVar, dVar, eVar, fVar};
    }

    public c(String str, int i, a aVar) {
    }

    public static String g(String str, String str2) {
        StringBuilder sb = new StringBuilder();
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (Character.isUpperCase(charAt) && sb.length() != 0) {
                sb.append(str2);
            }
            sb.append(charAt);
        }
        return sb.toString();
    }

    public static String h(String str) {
        int length = str.length() - 1;
        int i = 0;
        while (!Character.isLetter(str.charAt(i)) && i < length) {
            i++;
        }
        char charAt = str.charAt(i);
        if (Character.isUpperCase(charAt)) {
            return str;
        }
        char upperCase = Character.toUpperCase(charAt);
        if (i == 0) {
            StringBuilder O = b.d.b.a.a.O(upperCase);
            O.append(str.substring(1));
            return O.toString();
        }
        return str.substring(0, i) + upperCase + str.substring(i + 1);
    }

    public static c valueOf(String str) {
        return (c) Enum.valueOf(c.class, str);
    }

    public static c[] values() {
        return (c[]) p.clone();
    }
}
