package b.i.d;

import com.google.gson.JsonElement;
import java.lang.reflect.Type;
/* compiled from: JsonSerializer.java */
/* loaded from: classes3.dex */
public interface m<T> {
    JsonElement serialize(T t, Type type, l lVar);
}
