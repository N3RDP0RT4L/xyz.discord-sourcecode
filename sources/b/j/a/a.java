package b.j.a;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import b.j.a.f.d;
import com.hcaptcha.sdk.HCaptchaTokenResponse;
/* loaded from: classes3.dex */
public class a extends d<HCaptchaTokenResponse> {
    public final FragmentManager e;

    public a(@NonNull Activity activity) {
        this.e = ((FragmentActivity) activity).getSupportFragmentManager();
    }
}
