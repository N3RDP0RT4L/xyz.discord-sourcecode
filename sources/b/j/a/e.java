package b.j.a;

import android.webkit.JavascriptInterface;
import b.g.a.b.p.i;
import b.g.a.b.t.k;
import b.g.a.c.i0.d;
import b.g.a.c.r;
import b.j.a.f.a;
import b.j.a.f.b;
import b.j.a.f.c;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.hcaptcha.sdk.HCaptchaConfig;
import com.hcaptcha.sdk.HCaptchaError;
import com.hcaptcha.sdk.HCaptchaException;
import com.hcaptcha.sdk.HCaptchaTokenResponse;
import java.io.IOException;
import java.io.Serializable;
import java.util.Objects;
/* loaded from: classes3.dex */
public class e implements Serializable {
    private final HCaptchaConfig hCaptchaConfig;
    private final a onFailureListener;
    private final b onLoadedListener;
    private final c<HCaptchaTokenResponse> onSuccessListener;

    public e(HCaptchaConfig hCaptchaConfig, b bVar, c<HCaptchaTokenResponse> cVar, a aVar) {
        this.hCaptchaConfig = hCaptchaConfig;
        this.onLoadedListener = bVar;
        this.onSuccessListener = cVar;
        this.onFailureListener = aVar;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof e)) {
            return false;
        }
        e eVar = (e) obj;
        Objects.requireNonNull(eVar);
        HCaptchaConfig hCaptchaConfig = this.hCaptchaConfig;
        HCaptchaConfig hCaptchaConfig2 = eVar.hCaptchaConfig;
        if (hCaptchaConfig != null ? !hCaptchaConfig.equals(hCaptchaConfig2) : hCaptchaConfig2 != null) {
            return false;
        }
        b bVar = this.onLoadedListener;
        b bVar2 = eVar.onLoadedListener;
        if (bVar != null ? !bVar.equals(bVar2) : bVar2 != null) {
            return false;
        }
        c<HCaptchaTokenResponse> cVar = this.onSuccessListener;
        c<HCaptchaTokenResponse> cVar2 = eVar.onSuccessListener;
        if (cVar != null ? !cVar.equals(cVar2) : cVar2 != null) {
            return false;
        }
        a aVar = this.onFailureListener;
        a aVar2 = eVar.onFailureListener;
        return aVar != null ? aVar.equals(aVar2) : aVar2 == null;
    }

    @JavascriptInterface
    public String getConfig() {
        char[] cArr;
        r rVar = new r(null, null, null);
        HCaptchaConfig hCaptchaConfig = this.hCaptchaConfig;
        i iVar = new i(rVar._jsonFactory.a());
        try {
            rVar.b(rVar.c(iVar), hCaptchaConfig);
            String c = iVar.j.c();
            k kVar = iVar.j;
            kVar.c = -1;
            kVar.h = 0;
            kVar.j = null;
            if (kVar.e) {
                kVar.e = false;
                kVar.d.clear();
                kVar.f = 0;
                kVar.h = 0;
            }
            b.g.a.b.t.a aVar = kVar.f674b;
            if (!(aVar == null || (cArr = kVar.g) == null)) {
                kVar.g = null;
                aVar.d.set(2, cArr);
            }
            return c;
        } catch (JsonProcessingException e) {
            throw e;
        } catch (IOException e2) {
            throw new JsonMappingException(null, String.format("Unexpected IOException (of type %s): %s", e2.getClass().getName(), d.h(e2)));
        }
    }

    public int hashCode() {
        HCaptchaConfig hCaptchaConfig = this.hCaptchaConfig;
        int i = 43;
        int hashCode = hCaptchaConfig == null ? 43 : hCaptchaConfig.hashCode();
        b bVar = this.onLoadedListener;
        int hashCode2 = ((hashCode + 59) * 59) + (bVar == null ? 43 : bVar.hashCode());
        c<HCaptchaTokenResponse> cVar = this.onSuccessListener;
        int hashCode3 = (hashCode2 * 59) + (cVar == null ? 43 : cVar.hashCode());
        a aVar = this.onFailureListener;
        int i2 = hashCode3 * 59;
        if (aVar != null) {
            i = aVar.hashCode();
        }
        return i2 + i;
    }

    @JavascriptInterface
    public void onError(int i) {
        this.onFailureListener.onFailure(new HCaptchaException(HCaptchaError.fromId(i)));
    }

    @JavascriptInterface
    public void onLoaded() {
        c cVar = (c) this.onLoadedListener;
        cVar.k.post(new b(cVar));
    }

    @JavascriptInterface
    public void onPass(String str) {
        this.onSuccessListener.onSuccess(new HCaptchaTokenResponse(str));
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("HCaptchaJSInterface(hCaptchaConfig=");
        R.append(this.hCaptchaConfig);
        R.append(", onLoadedListener=");
        R.append(this.onLoadedListener);
        R.append(", onSuccessListener=");
        R.append(this.onSuccessListener);
        R.append(", onFailureListener=");
        R.append(this.onFailureListener);
        R.append(")");
        return R.toString();
    }
}
