package b.n.a.a;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
/* compiled from: ISACallback.java */
/* loaded from: classes3.dex */
public interface a extends IInterface {

    /* compiled from: ISACallback.java */
    /* renamed from: b.n.a.a.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static abstract class AbstractBinderC0163a extends Binder implements a {
        public AbstractBinderC0163a() {
            attachInterface(this, "com.msc.sa.aidl.ISACallback");
        }

        @Override // android.os.IInterface
        public IBinder asBinder() {
            return this;
        }

        /* JADX WARN: Code restructure failed: missing block: B:49:0x00e7, code lost:
            if (r12 != false) goto L50;
         */
        @Override // android.os.Binder
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public boolean onTransact(int r9, android.os.Parcel r10, android.os.Parcel r11, int r12) throws android.os.RemoteException {
            /*
                Method dump skipped, instructions count: 436
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: b.n.a.a.a.AbstractBinderC0163a.onTransact(int, android.os.Parcel, android.os.Parcel, int):boolean");
        }
    }
}
