package b.n.a.a;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
/* compiled from: ISAService.java */
/* loaded from: classes3.dex */
public interface b extends IInterface {

    /* compiled from: ISAService.java */
    /* loaded from: classes3.dex */
    public static abstract class a extends Binder implements b {
        public static final /* synthetic */ int a = 0;

        /* compiled from: ISAService.java */
        /* renamed from: b.n.a.a.b$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static class C0164a implements b {
            public IBinder a;

            public C0164a(IBinder iBinder) {
                this.a = iBinder;
            }

            @Override // b.n.a.a.b
            public String Q(String str, String str2, String str3, b.n.a.a.a aVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.msc.sa.aidl.ISAService");
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeString(str3);
                    obtain.writeStrongBinder(aVar != null ? aVar.asBinder() : null);
                    if (!this.a.transact(1, obtain, obtain2, 0)) {
                        int i = a.a;
                    }
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @Override // android.os.IInterface
            public IBinder asBinder() {
                return this.a;
            }

            @Override // b.n.a.a.b
            public boolean h0(int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.msc.sa.aidl.ISAService");
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    boolean z2 = true;
                    obtain.writeInt(1);
                    bundle.writeToParcel(obtain, 0);
                    if (!this.a.transact(6, obtain, obtain2, 0)) {
                        int i2 = a.a;
                    }
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        z2 = false;
                    }
                    return z2;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }

    String Q(String str, String str2, String str3, b.n.a.a.a aVar) throws RemoteException;

    boolean h0(int i, String str, Bundle bundle) throws RemoteException;
}
