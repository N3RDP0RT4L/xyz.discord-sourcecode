package b.o.b.f;

import android.opengl.GLES20;
import b.o.b.a.d;
import b.o.b.c.f;
import d0.p;
import d0.q;
import d0.z.d.m;
/* compiled from: GlTexture.kt */
/* loaded from: classes3.dex */
public final class b {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final int f1964b;
    public final Integer c;
    public final Integer d;
    public final Integer e;
    public final Integer f;
    public final int g;

    public b(int i, int i2, Integer num) {
        int i3;
        this.a = i;
        this.f1964b = i2;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        if (num == null) {
            int[] iArr = q.m82constructorimpl(1);
            int i4 = q.m85getSizeimpl(iArr);
            int[] iArr2 = new int[i4];
            for (int i5 = 0; i5 < i4; i5++) {
                iArr2[i5] = q.m84getpVg5ArA(iArr, i5);
            }
            GLES20.glGenTextures(1, iArr2, 0);
            q.m86setVXSXFK8(iArr, 0, p.m81constructorimpl(iArr2[0]));
            d.b("glGenTextures");
            i3 = q.m84getpVg5ArA(iArr, 0);
        } else {
            i3 = num.intValue();
        }
        this.g = i3;
        if (num == null) {
            a aVar = new a(this, null);
            m.checkNotNullParameter(this, "<this>");
            m.checkNotNullParameter(aVar, "block");
            a();
            aVar.invoke();
            b();
        }
    }

    public void a() {
        GLES20.glActiveTexture(p.m81constructorimpl(this.a));
        GLES20.glBindTexture(p.m81constructorimpl(this.f1964b), p.m81constructorimpl(this.g));
        d.b("bind");
    }

    public void b() {
        GLES20.glBindTexture(p.m81constructorimpl(this.f1964b), p.m81constructorimpl(0));
        GLES20.glActiveTexture(f.c);
        d.b("unbind");
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public b(int i, int i2, Integer num, int i3) {
        this((i3 & 1) != 0 ? f.c : i, (i3 & 2) != 0 ? f.d : i2, null);
        int i4 = i3 & 4;
    }
}
