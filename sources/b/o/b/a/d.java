package b.o.b.a;

import android.opengl.EGL14;
import android.opengl.GLES20;
import android.opengl.GLU;
import android.opengl.Matrix;
import android.util.Log;
import b.d.b.a.a;
import b.o.b.c.f;
import d0.p;
import d0.z.d.m;
/* compiled from: Egloo.kt */
/* loaded from: classes3.dex */
public final class d {
    public static final float[] a;

    static {
        float[] fArr = new float[16];
        m.checkNotNullParameter(fArr, "<this>");
        m.checkNotNullParameter(fArr, "matrix");
        Matrix.setIdentityM(fArr, 0);
        a = fArr;
    }

    public static final void a(String str) {
        m.checkNotNullParameter(str, "opName");
        int eglGetError = EGL14.eglGetError();
        if (eglGetError != b.o.b.c.d.d) {
            StringBuilder W = a.W("Error during ", str, ": EGL error 0x");
            String hexString = Integer.toHexString(eglGetError);
            m.checkNotNullExpressionValue(hexString, "toHexString(value)");
            W.append(hexString);
            String sb = W.toString();
            Log.e("Egloo", sb);
            throw new RuntimeException(sb);
        }
    }

    public static final void b(String str) {
        m.checkNotNullParameter(str, "opName");
        int i = p.m81constructorimpl(GLES20.glGetError());
        int i2 = f.a;
        if (i != 0) {
            StringBuilder W = a.W("Error during ", str, ": glError 0x");
            String hexString = Integer.toHexString(i);
            m.checkNotNullExpressionValue(hexString, "toHexString(value)");
            W.append(hexString);
            W.append(": ");
            String gluErrorString = GLU.gluErrorString(i);
            m.checkNotNullExpressionValue(gluErrorString, "gluErrorString(value)");
            W.append(gluErrorString);
            String sb = W.toString();
            Log.e("Egloo", sb);
            throw new RuntimeException(sb);
        }
    }
}
