package b.o.b.a;

import android.opengl.EGL14;
import android.opengl.EGLConfig;
import android.util.Log;
import b.o.b.c.a;
import b.o.b.c.c;
import b.o.b.c.d;
import d0.t.c0;
import d0.t.k;
import d0.z.d.m;
import java.util.Iterator;
import org.webrtc.EglBase;
/* compiled from: EglNativeConfigChooser.kt */
/* loaded from: classes3.dex */
public class b {
    public final a a(c cVar, int i, boolean z2) {
        int i2;
        m.checkNotNullParameter(cVar, "display");
        if (i >= 3) {
            i2 = d.j | d.k;
        } else {
            i2 = d.j;
        }
        int[] iArr = new int[15];
        iArr[0] = d.l;
        iArr[1] = 8;
        iArr[2] = d.m;
        iArr[3] = 8;
        iArr[4] = d.n;
        iArr[5] = 8;
        iArr[6] = d.o;
        iArr[7] = 8;
        iArr[8] = d.p;
        iArr[9] = d.q | d.r;
        iArr[10] = d.f1958s;
        iArr[11] = i2;
        iArr[12] = z2 ? EglBase.EGL_RECORDABLE_ANDROID : d.e;
        iArr[13] = z2 ? 1 : 0;
        iArr[14] = d.e;
        a[] aVarArr = new a[1];
        EGLConfig[] eGLConfigArr = new EGLConfig[1];
        boolean eglChooseConfig = EGL14.eglChooseConfig(cVar.a, iArr, 0, eGLConfigArr, 0, 1, new int[1], 0);
        if (eglChooseConfig) {
            Iterator<Integer> it = k.getIndices(aVarArr).iterator();
            while (it.hasNext()) {
                int nextInt = ((c0) it).nextInt();
                EGLConfig eGLConfig = eGLConfigArr[nextInt];
                aVarArr[nextInt] = eGLConfig == null ? null : new a(eGLConfig);
            }
        }
        if (eglChooseConfig) {
            return aVarArr[0];
        }
        Log.w("EglConfigChooser", "Unable to find RGB8888 / " + i + " EGLConfig");
        return null;
    }
}
