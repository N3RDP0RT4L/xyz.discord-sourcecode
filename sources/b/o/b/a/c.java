package b.o.b.a;

import android.opengl.EGL14;
import android.opengl.EGLDisplay;
import b.o.b.c.a;
import b.o.b.c.b;
import b.o.b.c.d;
import b.o.b.c.e;
import d0.z.d.m;
/* compiled from: EglNativeCore.kt */
/* loaded from: classes3.dex */
public class c {
    public b.o.b.c.c a;

    /* renamed from: b  reason: collision with root package name */
    public b f1955b;
    public a c;

    public c(b bVar, int i) {
        a a;
        m.checkNotNullParameter(bVar, "sharedContext");
        b.o.b.c.c cVar = d.f1957b;
        this.a = cVar;
        this.f1955b = d.a;
        EGLDisplay eglGetDisplay = EGL14.eglGetDisplay(0);
        b.o.b.c.c cVar2 = new b.o.b.c.c(eglGetDisplay);
        this.a = cVar2;
        if (cVar2 == cVar) {
            throw new RuntimeException("unable to get EGL14 display");
        } else if (EGL14.eglInitialize(eglGetDisplay, new int[1], 0, new int[1], 0)) {
            b bVar2 = new b();
            boolean z2 = (i & 1) != 0;
            if (((i & 2) != 0) && (a = bVar2.a(this.a, 3, z2)) != null) {
                b bVar3 = new b(EGL14.eglCreateContext(this.a.a, a.a, bVar.a, new int[]{d.i, 3, d.e}, 0));
                try {
                    d.a("eglCreateContext (3)");
                    this.c = a;
                    this.f1955b = bVar3;
                } catch (Exception unused) {
                }
            }
            if (this.f1955b == d.a) {
                a a2 = bVar2.a(this.a, 2, z2);
                if (a2 != null) {
                    b bVar4 = new b(EGL14.eglCreateContext(this.a.a, a2.a, bVar.a, new int[]{d.i, 2, d.e}, 0));
                    d.a("eglCreateContext (2)");
                    this.c = a2;
                    this.f1955b = bVar4;
                    return;
                }
                throw new RuntimeException("Unable to find a suitable EGLConfig");
            }
        } else {
            throw new RuntimeException("unable to initialize EGL14");
        }
    }

    public final int a(e eVar, int i) {
        m.checkNotNullParameter(eVar, "eglSurface");
        int[] iArr = new int[1];
        EGL14.eglQuerySurface(this.a.a, eVar.a, i, iArr, 0);
        return iArr[0];
    }
}
