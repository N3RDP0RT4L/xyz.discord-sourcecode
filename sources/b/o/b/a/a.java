package b.o.b.a;

import android.opengl.EGL14;
import android.opengl.EGLContext;
import android.opengl.EGLDisplay;
import android.opengl.EGLSurface;
import b.o.b.c.b;
import b.o.b.c.c;
import b.o.b.c.d;
import b.o.b.c.e;
/* compiled from: EglCore.kt */
/* loaded from: classes3.dex */
public final class a extends c {
    public a(EGLContext eGLContext, int i) {
        super(new b(eGLContext), i);
    }

    public void b() {
        c cVar = this.a;
        c cVar2 = d.f1957b;
        if (cVar != cVar2) {
            e eVar = d.c;
            b bVar = d.a;
            EGLDisplay eGLDisplay = cVar.a;
            EGLSurface eGLSurface = eVar.a;
            EGL14.eglMakeCurrent(eGLDisplay, eGLSurface, eGLSurface, bVar.a);
            EGL14.eglDestroyContext(this.a.a, this.f1955b.a);
            EGL14.eglReleaseThread();
            EGL14.eglTerminate(this.a.a);
        }
        this.a = cVar2;
        this.f1955b = d.a;
        this.c = null;
    }

    public final void finalize() {
        b();
    }
}
