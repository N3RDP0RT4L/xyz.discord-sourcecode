package b.o.b.b;

import android.opengl.GLES20;
import b.i.a.f.e.o.f;
import b.o.b.a.d;
import java.nio.FloatBuffer;
/* compiled from: GlRect.kt */
/* loaded from: classes3.dex */
public class c extends a {
    @Deprecated
    public static final float[] c = {-1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f};
    public FloatBuffer d;

    public c() {
        float[] fArr = c;
        FloatBuffer X = f.X(fArr.length);
        X.put(fArr);
        X.clear();
        this.d = X;
    }

    @Override // b.o.b.b.b
    public void a() {
        d.b("glDrawArrays start");
        GLES20.glDrawArrays(b.o.b.c.f.f1959b, 0, this.d.limit() / this.f1956b);
        d.b("glDrawArrays end");
    }

    @Override // b.o.b.b.b
    public FloatBuffer b() {
        return this.d;
    }
}
