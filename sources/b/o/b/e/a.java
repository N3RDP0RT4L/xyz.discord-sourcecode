package b.o.b.e;

import android.graphics.Bitmap;
import android.opengl.EGL14;
import android.opengl.GLES20;
import b.o.b.c.b;
import b.o.b.c.d;
import b.o.b.c.e;
import d0.z.d.m;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Objects;
/* compiled from: EglSurface.kt */
/* loaded from: classes3.dex */
public abstract class a {
    public b.o.b.a.a a;

    /* renamed from: b  reason: collision with root package name */
    public e f1963b;
    public int c = -1;
    public int d = -1;

    public a(b.o.b.a.a aVar, e eVar) {
        m.checkNotNullParameter(aVar, "eglCore");
        m.checkNotNullParameter(eVar, "eglSurface");
        m.checkNotNullParameter(aVar, "eglCore");
        m.checkNotNullParameter(eVar, "eglSurface");
        this.a = aVar;
        this.f1963b = eVar;
    }

    public final void a(OutputStream outputStream, Bitmap.CompressFormat compressFormat) {
        m.checkNotNullParameter(outputStream, "stream");
        m.checkNotNullParameter(compressFormat, "format");
        b.o.b.a.a aVar = this.a;
        e eVar = this.f1963b;
        Objects.requireNonNull(aVar);
        m.checkNotNullParameter(eVar, "eglSurface");
        if (m.areEqual(aVar.f1955b, new b(EGL14.eglGetCurrentContext())) && m.areEqual(eVar, new e(EGL14.eglGetCurrentSurface(d.h)))) {
            int i = this.c;
            if (i < 0) {
                i = this.a.a(this.f1963b, d.f);
            }
            int i2 = this.d;
            if (i2 < 0) {
                i2 = this.a.a(this.f1963b, d.g);
            }
            int i3 = i2;
            ByteBuffer allocateDirect = ByteBuffer.allocateDirect(i * i3 * 4);
            allocateDirect.order(ByteOrder.LITTLE_ENDIAN);
            GLES20.glReadPixels(0, 0, i, i3, 6408, 5121, allocateDirect);
            b.o.b.a.d.b("glReadPixels");
            allocateDirect.rewind();
            Bitmap createBitmap = Bitmap.createBitmap(i, i3, Bitmap.Config.ARGB_8888);
            createBitmap.copyPixelsFromBuffer(allocateDirect);
            createBitmap.compress(compressFormat, 90, outputStream);
            createBitmap.recycle();
            return;
        }
        throw new RuntimeException("Expected EGL context/surface is not current");
    }
}
