package b.o.b.e;
/* compiled from: EglWindowSurface.kt */
/* loaded from: classes3.dex */
public class b extends a {
    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b(b.o.b.a.a r7, android.graphics.SurfaceTexture r8) {
        /*
            r6 = this;
            java.lang.String r0 = "eglCore"
            d0.z.d.m.checkNotNullParameter(r7, r0)
            java.lang.String r1 = "surfaceTexture"
            d0.z.d.m.checkNotNullParameter(r8, r1)
            java.lang.String r1 = "surface"
            d0.z.d.m.checkNotNullParameter(r8, r1)
            r1 = 1
            int[] r1 = new int[r1]
            int r2 = b.o.b.c.d.e
            r3 = 0
            r1[r3] = r2
            b.o.b.c.c r2 = r7.a
            b.o.b.c.a r4 = r7.c
            d0.z.d.m.checkNotNull(r4)
            b.o.b.c.e r5 = new b.o.b.c.e
            android.opengl.EGLDisplay r2 = r2.a
            android.opengl.EGLConfig r4 = r4.a
            android.opengl.EGLSurface r8 = android.opengl.EGL14.eglCreateWindowSurface(r2, r4, r8, r1, r3)
            r5.<init>(r8)
            java.lang.String r8 = "eglCreateWindowSurface"
            b.o.b.a.d.a(r8)
            b.o.b.c.e r8 = b.o.b.c.d.c
            if (r5 == r8) goto L40
            d0.z.d.m.checkNotNullParameter(r7, r0)
            java.lang.String r8 = "eglSurface"
            d0.z.d.m.checkNotNullParameter(r5, r8)
            r6.<init>(r7, r5)
            return
        L40:
            java.lang.RuntimeException r7 = new java.lang.RuntimeException
            java.lang.String r8 = "surface was null"
            r7.<init>(r8)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: b.o.b.e.b.<init>(b.o.b.a.a, android.graphics.SurfaceTexture):void");
    }
}
