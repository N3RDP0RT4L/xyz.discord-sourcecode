package b.o.b.d;

import android.opengl.GLES20;
import b.o.b.a.d;
import b.o.b.c.f;
import d0.p;
import d0.z.d.m;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: GlShader.kt */
/* loaded from: classes3.dex */
public final class c {
    public static final a a = new a(null);

    /* renamed from: b  reason: collision with root package name */
    public final int f1962b;

    /* compiled from: GlShader.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    public c(int i, String str) {
        m.checkNotNullParameter(str, "source");
        int i2 = p.m81constructorimpl(GLES20.glCreateShader(p.m81constructorimpl(i)));
        d.b(m.stringPlus("glCreateShader type=", Integer.valueOf(i)));
        GLES20.glShaderSource(i2, str);
        GLES20.glCompileShader(i2);
        int[] iArr = new int[1];
        GLES20.glGetShaderiv(i2, f.l, iArr, 0);
        if (iArr[0] != 0) {
            this.f1962b = i2;
            return;
        }
        StringBuilder S = b.d.b.a.a.S("Could not compile shader ", i, ": '");
        S.append((Object) GLES20.glGetShaderInfoLog(i2));
        S.append("' source: ");
        S.append(str);
        String sb = S.toString();
        GLES20.glDeleteShader(i2);
        throw new RuntimeException(sb);
    }
}
