package b.o.b.d;

import android.graphics.RectF;
import b.i.a.f.e.o.f;
import b.o.b.b.a;
import b.o.b.d.b;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.nio.FloatBuffer;
/* compiled from: GlTextureProgram.kt */
/* loaded from: classes3.dex */
public class d extends a {
    public final b f;
    public final b h;
    public final b i;
    public final b j;
    public a m;
    public float[] e = f.J0(b.o.b.a.d.a);
    public FloatBuffer g = f.X(8);
    public final RectF k = new RectF();
    public int l = -1;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public d(int i, String str, String str2, String str3, String str4) {
        super(i, false, new c[0]);
        m.checkNotNullParameter(str, "vertexPositionName");
        m.checkNotNullParameter(str2, "vertexMvpMatrixName");
        b.a aVar = b.a.ATTRIB;
        b.a aVar2 = b.a.UNIFORM;
        m.checkNotNullParameter(str, "vertexPositionName");
        m.checkNotNullParameter(str2, "vertexMvpMatrixName");
        m.checkNotNullParameter(str4, ModelAuditLogEntry.CHANGE_KEY_NAME);
        int i2 = this.a;
        m.checkNotNullParameter(str4, ModelAuditLogEntry.CHANGE_KEY_NAME);
        this.f = new b(i2, aVar2, str4, null);
        m.checkNotNullParameter(str3, ModelAuditLogEntry.CHANGE_KEY_NAME);
        int i3 = this.a;
        m.checkNotNullParameter(str3, ModelAuditLogEntry.CHANGE_KEY_NAME);
        this.h = new b(i3, aVar, str3, null);
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        int i4 = this.a;
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        this.i = new b(i4, aVar, str, null);
        m.checkNotNullParameter(str2, ModelAuditLogEntry.CHANGE_KEY_NAME);
        int i5 = this.a;
        m.checkNotNullParameter(str2, ModelAuditLogEntry.CHANGE_KEY_NAME);
        this.j = new b(i5, aVar2, str2, null);
    }
}
