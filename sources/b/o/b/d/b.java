package b.o.b.d;

import android.opengl.GLES20;
import android.util.Log;
import b.o.b.a.d;
import d0.p;
import d0.z.d.m;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: GlProgramLocation.kt */
/* loaded from: classes3.dex */
public final class b {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final int f1961b;
    public final int c;

    /* compiled from: GlProgramLocation.kt */
    /* loaded from: classes3.dex */
    public enum a {
        ATTRIB,
        UNIFORM
    }

    public b(int i, a aVar, String str, DefaultConstructorMarker defaultConstructorMarker) {
        int i2;
        this.a = str;
        int ordinal = aVar.ordinal();
        if (ordinal == 0) {
            i2 = GLES20.glGetAttribLocation(p.m81constructorimpl(i), str);
        } else if (ordinal == 1) {
            i2 = GLES20.glGetUniformLocation(p.m81constructorimpl(i), str);
        } else {
            throw new NoWhenBranchMatchedException();
        }
        this.f1961b = i2;
        float[] fArr = d.a;
        m.checkNotNullParameter(str, "label");
        if (i2 >= 0) {
            this.c = p.m81constructorimpl(i2);
            return;
        }
        String str2 = "Unable to locate " + str + " in program";
        Log.e("Egloo", str2);
        throw new RuntimeException(str2);
    }
}
