package b.o.b.c;

import android.opengl.EGLSurface;
import b.d.b.a.a;
import d0.z.d.m;
/* compiled from: egl.kt */
/* loaded from: classes3.dex */
public final class e {
    public final EGLSurface a;

    public e(EGLSurface eGLSurface) {
        this.a = eGLSurface;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof e) && m.areEqual(this.a, ((e) obj).a);
    }

    public int hashCode() {
        EGLSurface eGLSurface = this.a;
        if (eGLSurface == null) {
            return 0;
        }
        return eGLSurface.hashCode();
    }

    public String toString() {
        StringBuilder R = a.R("EglSurface(native=");
        R.append(this.a);
        R.append(')');
        return R.toString();
    }
}
