package b.o.b.c;

import android.opengl.EGLDisplay;
import b.d.b.a.a;
import d0.z.d.m;
/* compiled from: egl.kt */
/* loaded from: classes3.dex */
public final class c {
    public final EGLDisplay a;

    public c(EGLDisplay eGLDisplay) {
        this.a = eGLDisplay;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof c) && m.areEqual(this.a, ((c) obj).a);
    }

    public int hashCode() {
        EGLDisplay eGLDisplay = this.a;
        if (eGLDisplay == null) {
            return 0;
        }
        return eGLDisplay.hashCode();
    }

    public String toString() {
        StringBuilder R = a.R("EglDisplay(native=");
        R.append(this.a);
        R.append(')');
        return R.toString();
    }
}
