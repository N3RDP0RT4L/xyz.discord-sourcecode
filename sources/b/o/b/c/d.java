package b.o.b.c;

import android.opengl.EGL14;
/* compiled from: egl.kt */
/* loaded from: classes3.dex */
public final class d {
    public static final b a = new b(EGL14.EGL_NO_CONTEXT);

    /* renamed from: b  reason: collision with root package name */
    public static final c f1957b = new c(EGL14.EGL_NO_DISPLAY);
    public static final e c = new e(EGL14.EGL_NO_SURFACE);
    public static final int d = 12288;
    public static final int e = 12344;
    public static final int f = 12375;
    public static final int g = 12374;
    public static final int h = 12377;
    public static final int i = 12440;
    public static final int j = 4;
    public static final int k = 64;
    public static final int l = 12324;
    public static final int m = 12323;
    public static final int n = 12322;
    public static final int o = 12321;
    public static final int p = 12339;
    public static final int q = 4;
    public static final int r = 1;

    /* renamed from: s  reason: collision with root package name */
    public static final int f1958s = 12352;
}
