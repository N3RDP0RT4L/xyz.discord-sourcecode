package b.o.b.c;

import android.opengl.EGLContext;
import b.d.b.a.a;
import d0.z.d.m;
/* compiled from: egl.kt */
/* loaded from: classes3.dex */
public final class b {
    public final EGLContext a;

    public b(EGLContext eGLContext) {
        this.a = eGLContext;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof b) && m.areEqual(this.a, ((b) obj).a);
    }

    public int hashCode() {
        EGLContext eGLContext = this.a;
        if (eGLContext == null) {
            return 0;
        }
        return eGLContext.hashCode();
    }

    public String toString() {
        StringBuilder R = a.R("EglContext(native=");
        R.append(this.a);
        R.append(')');
        return R.toString();
    }
}
