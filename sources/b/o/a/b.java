package b.o.a;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
/* compiled from: CameraLogger.java */
/* loaded from: classes3.dex */
public final class b {
    public static int a = 3;

    /* renamed from: b  reason: collision with root package name */
    public static Set<AbstractC0165b> f1906b;
    @VisibleForTesting
    public static AbstractC0165b c;
    @NonNull
    public String d;

    /* compiled from: CameraLogger.java */
    /* loaded from: classes3.dex */
    public class a implements AbstractC0165b {
        @Override // b.o.a.b.AbstractC0165b
        public void a(int i, @NonNull String str, @NonNull String str2, @Nullable Throwable th) {
            if (i == 0) {
                Log.v(str, str2, th);
            } else if (i == 1) {
                Log.i(str, str2, th);
            } else if (i == 2) {
                Log.w(str, str2, th);
            } else if (i == 3) {
                Log.e(str, str2, th);
            }
        }
    }

    /* compiled from: CameraLogger.java */
    /* renamed from: b.o.a.b$b  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public interface AbstractC0165b {
        void a(int i, @NonNull String str, @NonNull String str2, @Nullable Throwable th);
    }

    static {
        CopyOnWriteArraySet copyOnWriteArraySet = new CopyOnWriteArraySet();
        f1906b = copyOnWriteArraySet;
        a aVar = new a();
        c = aVar;
        copyOnWriteArraySet.add(aVar);
    }

    public b(@NonNull String str) {
        this.d = str;
    }

    @Nullable
    public final String a(int i, @NonNull Object... objArr) {
        Throwable th = null;
        if (!(a <= i && f1906b.size() > 0)) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (Object obj : objArr) {
            if (obj instanceof Throwable) {
                th = (Throwable) obj;
            }
            sb.append(String.valueOf(obj));
            sb.append(" ");
        }
        String trim = sb.toString().trim();
        for (AbstractC0165b bVar : f1906b) {
            bVar.a(i, this.d, trim, th);
        }
        return trim;
    }
}
