package b.o.a.w;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.o.a.n.g;
import b.o.a.n.i;
import b.o.a.n.v.e;
import b.o.a.n.v.f;
import b.o.a.n.v.h;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.Objects;
/* compiled from: CameraPreview.java */
/* loaded from: classes3.dex */
public abstract class a<T extends View, Output> {
    public static final b.o.a.b a = new b.o.a.b(a.class.getSimpleName());

    /* renamed from: b  reason: collision with root package name */
    public c f1953b;
    public T c;
    public boolean d;
    public int e;
    public int f;
    public int g;
    public int h;
    public int i;

    /* compiled from: CameraPreview.java */
    /* renamed from: b.o.a.w.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public class RunnableC0179a implements Runnable {
        public final /* synthetic */ TaskCompletionSource j;

        public RunnableC0179a(TaskCompletionSource taskCompletionSource) {
            this.j = taskCompletionSource;
        }

        @Override // java.lang.Runnable
        public void run() {
            View k = a.this.k();
            ViewParent parent = k.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(k);
            }
            this.j.a.s(null);
        }
    }

    /* compiled from: CameraPreview.java */
    /* loaded from: classes3.dex */
    public interface b {
        void a();
    }

    /* compiled from: CameraPreview.java */
    /* loaded from: classes3.dex */
    public interface c {
    }

    public a(@NonNull Context context, @NonNull ViewGroup viewGroup) {
        this.c = n(context, viewGroup);
    }

    public void e(@Nullable b bVar) {
    }

    public final void f(int i, int i2) {
        a.a(1, "dispatchOnSurfaceAvailable:", "w=", Integer.valueOf(i), "h=", Integer.valueOf(i2));
        this.e = i;
        this.f = i2;
        if (i > 0 && i2 > 0) {
            e(null);
        }
        c cVar = this.f1953b;
        if (cVar != null) {
            ((i) cVar).V();
        }
    }

    public final void g() {
        this.e = 0;
        this.f = 0;
        c cVar = this.f1953b;
        if (cVar != null) {
            i iVar = (i) cVar;
            i.j.a(1, "onSurfaceDestroyed");
            iVar.N0(false);
            iVar.M0(false);
        }
    }

    public final void h(int i, int i2) {
        a.a(1, "dispatchOnSurfaceSizeChanged:", "w=", Integer.valueOf(i), "h=", Integer.valueOf(i2));
        if (i != this.e || i2 != this.f) {
            this.e = i;
            this.f = i2;
            if (i > 0 && i2 > 0) {
                e(null);
            }
            c cVar = this.f1953b;
            if (cVar != null) {
                g gVar = (g) cVar;
                Objects.requireNonNull(gVar);
                i.j.a(1, "onSurfaceChanged:", "Size is", gVar.U0(b.o.a.n.t.b.VIEW));
                f fVar = gVar.n;
                fVar.b("surface changed", true, new h(fVar, e.BIND, new b.o.a.n.h(gVar)));
            }
        }
    }

    @NonNull
    public abstract Output i();

    @NonNull
    public abstract Class<Output> j();

    @NonNull
    public abstract View k();

    @NonNull
    public final b.o.a.x.b l() {
        return new b.o.a.x.b(this.e, this.f);
    }

    public final boolean m() {
        return this.e > 0 && this.f > 0;
    }

    @NonNull
    public abstract T n(@NonNull Context context, @NonNull ViewGroup viewGroup);

    @CallSuper
    public void o() {
        if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
            View k = k();
            ViewParent parent = k.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(k);
                return;
            }
            return;
        }
        Handler handler = new Handler(Looper.getMainLooper());
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        handler.post(new RunnableC0179a(taskCompletionSource));
        try {
            b.i.a.f.e.o.f.j(taskCompletionSource.a);
        } catch (Exception unused) {
        }
    }

    public void p() {
    }

    public void q() {
    }

    public void r(int i) {
        this.i = i;
    }

    public void s(int i, int i2) {
        a.a(1, "setStreamSize:", "desiredW=", Integer.valueOf(i), "desiredH=", Integer.valueOf(i2));
        this.g = i;
        this.h = i2;
        if (i > 0 && i2 > 0) {
            e(null);
        }
    }

    public void t(@Nullable c cVar) {
        c cVar2;
        c cVar3;
        if (m() && (cVar3 = this.f1953b) != null) {
            i iVar = (i) cVar3;
            i.j.a(1, "onSurfaceDestroyed");
            iVar.N0(false);
            iVar.M0(false);
        }
        this.f1953b = cVar;
        if (m() && (cVar2 = this.f1953b) != null) {
            ((i) cVar2).V();
        }
    }

    public boolean u() {
        return this instanceof d;
    }
}
