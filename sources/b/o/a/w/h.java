package b.o.a.w;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import b.o.a.b;
import com.otaliastudios.cameraview.R;
/* compiled from: SurfaceCameraPreview.java */
/* loaded from: classes3.dex */
public class h extends a<SurfaceView, SurfaceHolder> {
    public static final b j = new b(h.class.getSimpleName());
    public boolean k;
    public View l;

    public h(@NonNull Context context, @NonNull ViewGroup viewGroup) {
        super(context, viewGroup);
    }

    @Override // b.o.a.w.a
    @NonNull
    public SurfaceHolder i() {
        return ((SurfaceView) this.c).getHolder();
    }

    @Override // b.o.a.w.a
    @NonNull
    public Class<SurfaceHolder> j() {
        return SurfaceHolder.class;
    }

    @Override // b.o.a.w.a
    @NonNull
    public View k() {
        return this.l;
    }

    @Override // b.o.a.w.a
    @NonNull
    public SurfaceView n(@NonNull Context context, @NonNull ViewGroup viewGroup) {
        View inflate = LayoutInflater.from(context).inflate(R.b.cameraview_surface_view, viewGroup, false);
        viewGroup.addView(inflate, 0);
        SurfaceView surfaceView = (SurfaceView) inflate.findViewById(R.a.surface_view);
        SurfaceHolder holder = surfaceView.getHolder();
        holder.setType(3);
        holder.addCallback(new g(this));
        this.l = inflate;
        return surfaceView;
    }
}
