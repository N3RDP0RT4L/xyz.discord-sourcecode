package b.o.a.w;

import android.graphics.SurfaceTexture;
import androidx.annotation.NonNull;
import b.o.a.o.b;
/* compiled from: RendererFrameCallback.java */
/* loaded from: classes3.dex */
public interface f {
    void a(@NonNull SurfaceTexture surfaceTexture, int i, float f, float f2);

    void b(int i);

    void c(@NonNull b bVar);
}
