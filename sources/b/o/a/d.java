package b.o.a;

import android.os.Handler;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
/* compiled from: CameraUtils.java */
/* loaded from: classes3.dex */
public class d implements Runnable {
    public final /* synthetic */ byte[] j;
    public final /* synthetic */ File k;
    public final /* synthetic */ Handler l;
    public final /* synthetic */ k m;

    /* compiled from: CameraUtils.java */
    /* loaded from: classes3.dex */
    public class a implements Runnable {
        public final /* synthetic */ File j;

        public a(File file) {
            this.j = file;
        }

        @Override // java.lang.Runnable
        public void run() {
            d.this.m.a(this.j);
        }
    }

    public d(byte[] bArr, File file, Handler handler, k kVar) {
        this.j = bArr;
        this.k = file;
        this.l = handler;
        this.m = kVar;
    }

    @Override // java.lang.Runnable
    public void run() {
        byte[] bArr = this.j;
        File file = this.k;
        boolean exists = file.exists();
        file = null;
        if (!exists || file.delete()) {
            try {
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));
                bufferedOutputStream.write(bArr);
                bufferedOutputStream.flush();
                bufferedOutputStream.close();
            } catch (IOException e) {
                e.a.a(3, "writeToFile:", "could not write file.", e);
            }
            this.l.post(new a(file));
        }
        this.l.post(new a(file));
    }
}
