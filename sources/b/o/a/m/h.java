package b.o.a.m;

import androidx.annotation.NonNull;
/* compiled from: Hdr.java */
/* loaded from: classes3.dex */
public enum h implements c {
    OFF(0),
    ON(1);
    
    private int value;

    h(int i) {
        this.value = i;
    }

    @NonNull
    public static h f(int i) {
        h[] values = values();
        for (int i2 = 0; i2 < 2; i2++) {
            h hVar = values[i2];
            if (hVar.value == i) {
                return hVar;
            }
        }
        return OFF;
    }

    public int g() {
        return this.value;
    }
}
