package b.o.a.m;

import androidx.annotation.NonNull;
/* compiled from: WhiteBalance.java */
/* loaded from: classes3.dex */
public enum m implements c {
    AUTO(0),
    INCANDESCENT(1),
    FLUORESCENT(2),
    DAYLIGHT(3),
    CLOUDY(4);
    
    private int value;

    m(int i) {
        this.value = i;
    }

    @NonNull
    public static m f(int i) {
        m[] values = values();
        for (int i2 = 0; i2 < 5; i2++) {
            m mVar = values[i2];
            if (mVar.value == i) {
                return mVar;
            }
        }
        return AUTO;
    }

    public int g() {
        return this.value;
    }
}
