package b.o.a.t;

import android.graphics.PointF;
import android.graphics.RectF;
import androidx.annotation.NonNull;
/* compiled from: MeteringTransform.java */
/* loaded from: classes3.dex */
public interface c<T> {
    @NonNull
    T a(@NonNull RectF rectF, int i);

    @NonNull
    PointF b(@NonNull PointF pointF);
}
