package b.o.a.q;

import androidx.annotation.NonNull;
/* JADX WARN: Init of enum j can be incorrect */
/* JADX WARN: Init of enum k can be incorrect */
/* JADX WARN: Init of enum l can be incorrect */
/* JADX WARN: Init of enum m can be incorrect */
/* JADX WARN: Init of enum n can be incorrect */
/* compiled from: Gesture.java */
/* loaded from: classes3.dex */
public enum a {
    PINCH(r1),
    TAP(r4),
    LONG_TAP(r4),
    SCROLL_HORIZONTAL(r1),
    SCROLL_VERTICAL(r1);
    
    private d type;

    static {
        d dVar = d.CONTINUOUS;
        d dVar2 = d.ONE_SHOT;
    }

    a(@NonNull d dVar) {
        this.type = dVar;
    }

    public boolean f(@NonNull b bVar) {
        return bVar == b.NONE || bVar.g() == this.type;
    }
}
