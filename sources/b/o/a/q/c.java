package b.o.a.q;

import android.graphics.PointF;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
/* compiled from: GestureFinder.java */
/* loaded from: classes3.dex */
public abstract class c {
    public boolean a;
    @VisibleForTesting

    /* renamed from: b  reason: collision with root package name */
    public b.o.a.q.a f1943b;
    public PointF[] c;

    /* compiled from: GestureFinder.java */
    /* loaded from: classes3.dex */
    public interface a {
    }

    public c(@NonNull a aVar, int i) {
        this.c = new PointF[i];
        for (int i2 = 0; i2 < i; i2++) {
            this.c[i2] = new PointF(0.0f, 0.0f);
        }
    }

    public final float a(float f, float f2, float f3) {
        float b2 = b(f, f2, f3);
        if (b2 < f2) {
            b2 = f2;
        }
        if (b2 > f3) {
            b2 = f3;
        }
        float f4 = ((f3 - f2) / 50.0f) / 2.0f;
        return (b2 < f - f4 || b2 > f4 + f) ? b2 : f;
    }

    public abstract float b(float f, float f2, float f3);
}
