package b.o.a.q;

import android.view.GestureDetector;
import android.view.MotionEvent;
import androidx.annotation.NonNull;
import b.o.a.b;
import b.o.a.q.c;
import com.otaliastudios.cameraview.CameraView;
/* compiled from: ScrollGestureFinder.java */
/* loaded from: classes3.dex */
public class f extends c {
    public static final b d = new b(f.class.getSimpleName());
    public GestureDetector e;
    public boolean f;
    public float g;

    /* compiled from: ScrollGestureFinder.java */
    /* loaded from: classes3.dex */
    public class a extends GestureDetector.SimpleOnGestureListener {
        public final /* synthetic */ c.a j;

        public a(c.a aVar) {
            this.j = aVar;
        }

        /* JADX WARN: Removed duplicated region for block: B:22:0x00a1  */
        /* JADX WARN: Removed duplicated region for block: B:23:0x00ae  */
        /* JADX WARN: Removed duplicated region for block: B:26:0x00c1  */
        /* JADX WARN: Removed duplicated region for block: B:27:0x00c5  */
        @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public boolean onScroll(android.view.MotionEvent r8, android.view.MotionEvent r9, float r10, float r11) {
            /*
                r7 = this;
                b.o.a.q.a r0 = b.o.a.q.a.SCROLL_HORIZONTAL
                b.o.a.b r1 = b.o.a.q.f.d
                r2 = 3
                java.lang.Object[] r2 = new java.lang.Object[r2]
                r3 = 0
                java.lang.String r4 = "onScroll:"
                r2[r3] = r4
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                r4.<init>()
                java.lang.String r5 = "distanceX="
                r4.append(r5)
                r4.append(r10)
                java.lang.String r4 = r4.toString()
                r5 = 1
                r2[r5] = r4
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                r4.<init>()
                java.lang.String r6 = "distanceY="
                r4.append(r6)
                r4.append(r11)
                java.lang.String r4 = r4.toString()
                r6 = 2
                r2[r6] = r4
                r1.a(r5, r2)
                if (r8 == 0) goto Lcc
                if (r9 != 0) goto L3d
                goto Lcc
            L3d:
                float r1 = r8.getX()
                b.o.a.q.f r2 = b.o.a.q.f.this
                android.graphics.PointF[] r2 = r2.c
                r2 = r2[r3]
                float r2 = r2.x
                int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
                if (r1 != 0) goto L64
                float r1 = r8.getY()
                b.o.a.q.f r2 = b.o.a.q.f.this
                android.graphics.PointF[] r4 = r2.c
                r4 = r4[r3]
                float r4 = r4.y
                int r1 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
                if (r1 == 0) goto L5e
                goto L64
            L5e:
                b.o.a.q.a r8 = r2.f1943b
                if (r8 != r0) goto L8c
                r3 = 1
                goto L8c
            L64:
                float r1 = java.lang.Math.abs(r10)
                float r2 = java.lang.Math.abs(r11)
                int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
                if (r1 < 0) goto L72
                r1 = 1
                goto L73
            L72:
                r1 = 0
            L73:
                b.o.a.q.f r2 = b.o.a.q.f.this
                if (r1 == 0) goto L78
                goto L7a
            L78:
                b.o.a.q.a r0 = b.o.a.q.a.SCROLL_VERTICAL
            L7a:
                r2.f1943b = r0
                android.graphics.PointF[] r0 = r2.c
                r0 = r0[r3]
                float r2 = r8.getX()
                float r8 = r8.getY()
                r0.set(r2, r8)
                r3 = r1
            L8c:
                b.o.a.q.f r8 = b.o.a.q.f.this
                android.graphics.PointF[] r8 = r8.c
                r8 = r8[r5]
                float r0 = r9.getX()
                float r9 = r9.getY()
                r8.set(r0, r9)
                b.o.a.q.f r8 = b.o.a.q.f.this
                if (r3 == 0) goto Lae
                b.o.a.q.c$a r9 = r7.j
                com.otaliastudios.cameraview.CameraView$b r9 = (com.otaliastudios.cameraview.CameraView.b) r9
                com.otaliastudios.cameraview.CameraView r9 = com.otaliastudios.cameraview.CameraView.this
                int r9 = r9.getWidth()
                float r9 = (float) r9
                float r10 = r10 / r9
                goto Lbb
            Lae:
                b.o.a.q.c$a r9 = r7.j
                com.otaliastudios.cameraview.CameraView$b r9 = (com.otaliastudios.cameraview.CameraView.b) r9
                com.otaliastudios.cameraview.CameraView r9 = com.otaliastudios.cameraview.CameraView.this
                int r9 = r9.getHeight()
                float r9 = (float) r9
                float r10 = r11 / r9
            Lbb:
                r8.g = r10
                b.o.a.q.f r8 = b.o.a.q.f.this
                if (r3 == 0) goto Lc5
                float r9 = r8.g
                float r9 = -r9
                goto Lc7
            Lc5:
                float r9 = r8.g
            Lc7:
                r8.g = r9
                r8.f = r5
                return r5
            Lcc:
                return r3
            */
            throw new UnsupportedOperationException("Method not decompiled: b.o.a.q.f.a.onScroll(android.view.MotionEvent, android.view.MotionEvent, float, float):boolean");
        }
    }

    public f(@NonNull c.a aVar) {
        super(aVar, 2);
        CameraView.b bVar = (CameraView.b) aVar;
        GestureDetector gestureDetector = new GestureDetector(bVar.g(), new a(bVar));
        this.e = gestureDetector;
        gestureDetector.setIsLongpressEnabled(false);
    }

    @Override // b.o.a.q.c
    public float b(float f, float f2, float f3) {
        return ((f3 - f2) * this.g * 2.0f) + f;
    }

    public boolean c(@NonNull MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.f = false;
        }
        this.e.onTouchEvent(motionEvent);
        if (this.f) {
            d.a(1, "Notifying a gesture of type", this.f1943b.name());
        }
        return this.f;
    }
}
