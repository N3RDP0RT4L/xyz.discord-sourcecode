package b.o.a.q;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
/* JADX WARN: Init of enum j can be incorrect */
/* JADX WARN: Init of enum k can be incorrect */
/* JADX WARN: Init of enum l can be incorrect */
/* JADX WARN: Init of enum m can be incorrect */
/* JADX WARN: Init of enum n can be incorrect */
/* JADX WARN: Init of enum o can be incorrect */
/* JADX WARN: Init of enum p can be incorrect */
/* JADX WARN: Init of enum q can be incorrect */
/* compiled from: GestureAction.java */
/* loaded from: classes3.dex */
public enum b {
    NONE(0, r1),
    AUTO_FOCUS(1, r1),
    TAKE_PICTURE(2, r1),
    TAKE_PICTURE_SNAPSHOT(3, r1),
    ZOOM(4, r8),
    EXPOSURE_CORRECTION(5, r8),
    FILTER_CONTROL_1(6, r8),
    FILTER_CONTROL_2(7, r8);
    
    private d type;
    private int value;

    static {
        d dVar = d.ONE_SHOT;
        d dVar2 = d.CONTINUOUS;
    }

    b(int i, @NonNull d dVar) {
        this.value = i;
        this.type = dVar;
    }

    @Nullable
    public static b f(int i) {
        b[] values = values();
        for (int i2 = 0; i2 < 8; i2++) {
            b bVar = values[i2];
            if (bVar.value == i) {
                return bVar;
            }
        }
        return null;
    }

    @NonNull
    public d g() {
        return this.type;
    }

    public int h() {
        return this.value;
    }
}
