package b.o.a.v;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.o.a.b;
import b.o.a.l;
import b.o.a.v.d;
/* compiled from: SnapshotPictureRecorder.java */
/* loaded from: classes3.dex */
public abstract class i extends d {
    public static final b m = new b(i.class.getSimpleName());

    public i(@NonNull l.a aVar, @Nullable d.a aVar2) {
        super(aVar, aVar2);
    }
}
