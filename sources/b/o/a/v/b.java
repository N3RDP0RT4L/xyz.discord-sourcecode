package b.o.a.v;

import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.DngCreator;
import android.hardware.camera2.TotalCaptureResult;
import android.location.Location;
import android.media.Image;
import android.media.ImageReader;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.exifinterface.media.ExifInterface;
import b.o.a.l;
import b.o.a.m.j;
import b.o.a.n.d;
import b.o.a.n.o.c;
import b.o.a.n.o.e;
import b.o.a.r.g;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
/* compiled from: Full2PictureRecorder.java */
@RequiresApi(21)
/* loaded from: classes3.dex */
public class b extends c implements ImageReader.OnImageAvailableListener {
    public final c n;
    public final b.o.a.n.o.a o = new a();
    public final ImageReader p;
    public final CaptureRequest.Builder q;
    public DngCreator r;

    /* compiled from: Full2PictureRecorder.java */
    /* loaded from: classes3.dex */
    public class a extends e {
        public a() {
        }

        @Override // b.o.a.n.o.e, b.o.a.n.o.a
        public void b(@NonNull c cVar, @NonNull CaptureRequest captureRequest, @NonNull TotalCaptureResult totalCaptureResult) {
            int i;
            b bVar = b.this;
            if (bVar.j.g == j.DNG) {
                bVar.r = new DngCreator(((d) cVar).f1915g0, totalCaptureResult);
                b bVar2 = b.this;
                DngCreator dngCreator = bVar2.r;
                int i2 = bVar2.j.c;
                int i3 = (i2 + 360) % 360;
                if (i3 == 0) {
                    i = 1;
                } else if (i3 == 90) {
                    i = 6;
                } else if (i3 == 180) {
                    i = 3;
                } else if (i3 == 270) {
                    i = 8;
                } else {
                    throw new IllegalArgumentException(b.d.b.a.a.p("Invalid orientation: ", i2));
                }
                dngCreator.setOrientation(i);
                b bVar3 = b.this;
                Location location = bVar3.j.f1908b;
                if (location != null) {
                    bVar3.r.setLocation(location);
                }
            }
        }

        @Override // b.o.a.n.o.e, b.o.a.n.o.a
        public void c(@NonNull c cVar, @NonNull CaptureRequest captureRequest) {
            if (this.d) {
                j(cVar);
                this.d = false;
            }
            if (captureRequest.getTag() == 2) {
                c.m.a(1, "onCaptureStarted:", "Dispatching picture shutter.");
                b.this.a(false);
                l(Integer.MAX_VALUE);
            }
        }

        @Override // b.o.a.n.o.e
        public void j(@NonNull c cVar) {
            this.c = cVar;
            b bVar = b.this;
            bVar.q.addTarget(bVar.p.getSurface());
            b bVar2 = b.this;
            l.a aVar = bVar2.j;
            if (aVar.g == j.JPEG) {
                bVar2.q.set(CaptureRequest.JPEG_ORIENTATION, Integer.valueOf(aVar.c));
            }
            b.this.q.setTag(2);
            try {
                ((d) cVar).e1(this, b.this.q);
            } catch (CameraAccessException e) {
                b bVar3 = b.this;
                bVar3.j = null;
                bVar3.l = e;
                bVar3.b();
                l(Integer.MAX_VALUE);
            }
        }
    }

    public b(@NonNull l.a aVar, @NonNull d dVar, @NonNull CaptureRequest.Builder builder, @NonNull ImageReader imageReader) {
        super(aVar, dVar);
        this.n = dVar;
        this.q = builder;
        this.p = imageReader;
        g b2 = g.b("FallbackCameraThread");
        g.c = b2;
        imageReader.setOnImageAvailableListener(this, b2.f);
    }

    @Override // b.o.a.v.d
    public void c() {
        this.o.e(this.n);
    }

    public final void d(@NonNull Image image) {
        int i = 0;
        ByteBuffer buffer = image.getPlanes()[0].getBuffer();
        byte[] bArr = new byte[buffer.remaining()];
        buffer.get(bArr);
        l.a aVar = this.j;
        aVar.f = bArr;
        aVar.c = 0;
        try {
            int attributeInt = new ExifInterface(new ByteArrayInputStream(this.j.f)).getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
            l.a aVar2 = this.j;
            switch (attributeInt) {
                case 3:
                case 4:
                    i = 180;
                    break;
                case 5:
                case 6:
                    i = 90;
                    break;
                case 7:
                case 8:
                    i = 270;
                    break;
            }
            aVar2.c = i;
        } catch (IOException unused) {
        }
    }

    public final void e(@NonNull Image image) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(byteArrayOutputStream);
        try {
            this.r.writeImage(bufferedOutputStream, image);
            bufferedOutputStream.flush();
            this.j.f = byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            this.r.close();
            try {
                bufferedOutputStream.close();
            } catch (IOException unused) {
            }
            throw new RuntimeException(e);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:26:0x0069  */
    /* JADX WARN: Type inference failed for: r2v1 */
    /* JADX WARN: Type inference failed for: r2v2, types: [android.media.Image] */
    /* JADX WARN: Type inference failed for: r2v3 */
    @Override // android.media.ImageReader.OnImageAvailableListener
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void onImageAvailable(android.media.ImageReader r6) {
        /*
            r5 = this;
            b.o.a.b r0 = b.o.a.v.c.m
            r1 = 1
            java.lang.Object[] r2 = new java.lang.Object[r1]
            java.lang.String r3 = "onImageAvailable started."
            r4 = 0
            r2[r4] = r3
            r0.a(r1, r2)
            r2 = 0
            android.media.Image r6 = r6.acquireNextImage()     // Catch: java.lang.Throwable -> L54 java.lang.Exception -> L56
            b.o.a.l$a r3 = r5.j     // Catch: java.lang.Exception -> L52 java.lang.Throwable -> L65
            b.o.a.m.j r3 = r3.g     // Catch: java.lang.Exception -> L52 java.lang.Throwable -> L65
            int r3 = r3.ordinal()     // Catch: java.lang.Exception -> L52 java.lang.Throwable -> L65
            if (r3 == 0) goto L3d
            if (r3 != r1) goto L22
            r5.e(r6)     // Catch: java.lang.Exception -> L52 java.lang.Throwable -> L65
            goto L40
        L22:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch: java.lang.Exception -> L52 java.lang.Throwable -> L65
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: java.lang.Exception -> L52 java.lang.Throwable -> L65
            r1.<init>()     // Catch: java.lang.Exception -> L52 java.lang.Throwable -> L65
            java.lang.String r3 = "Unknown format: "
            r1.append(r3)     // Catch: java.lang.Exception -> L52 java.lang.Throwable -> L65
            b.o.a.l$a r3 = r5.j     // Catch: java.lang.Exception -> L52 java.lang.Throwable -> L65
            b.o.a.m.j r3 = r3.g     // Catch: java.lang.Exception -> L52 java.lang.Throwable -> L65
            r1.append(r3)     // Catch: java.lang.Exception -> L52 java.lang.Throwable -> L65
            java.lang.String r1 = r1.toString()     // Catch: java.lang.Exception -> L52 java.lang.Throwable -> L65
            r0.<init>(r1)     // Catch: java.lang.Exception -> L52 java.lang.Throwable -> L65
            throw r0     // Catch: java.lang.Exception -> L52 java.lang.Throwable -> L65
        L3d:
            r5.d(r6)     // Catch: java.lang.Exception -> L52 java.lang.Throwable -> L65
        L40:
            if (r6 == 0) goto L45
            r6.close()
        L45:
            java.lang.Object[] r6 = new java.lang.Object[r1]
            java.lang.String r2 = "onImageAvailable ended."
            r6[r4] = r2
            r0.a(r1, r6)
            r5.b()
            return
        L52:
            r0 = move-exception
            goto L58
        L54:
            r0 = move-exception
            goto L67
        L56:
            r0 = move-exception
            r6 = r2
        L58:
            r5.j = r2     // Catch: java.lang.Throwable -> L65
            r5.l = r0     // Catch: java.lang.Throwable -> L65
            r5.b()     // Catch: java.lang.Throwable -> L65
            if (r6 == 0) goto L64
            r6.close()
        L64:
            return
        L65:
            r0 = move-exception
            r2 = r6
        L67:
            if (r2 == 0) goto L6c
            r2.close()
        L6c:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.o.a.v.b.onImageAvailable(android.media.ImageReader):void");
    }
}
