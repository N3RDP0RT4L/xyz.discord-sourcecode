package b.o.a.x;

import androidx.annotation.NonNull;
import b.i.a.f.e.o.f;
import java.util.ArrayList;
import java.util.List;
/* compiled from: SizeSelectors.java */
/* loaded from: classes3.dex */
public class l implements c {
    public c[] a;

    public l(c[] cVarArr, f.a aVar) {
        this.a = cVarArr;
    }

    @Override // b.o.a.x.c
    @NonNull
    public List<b> a(@NonNull List<b> list) {
        List<b> list2 = null;
        for (c cVar : this.a) {
            list2 = cVar.a(list);
            if (!list2.isEmpty()) {
                break;
            }
        }
        return list2 == null ? new ArrayList() : list2;
    }
}
