package b.o.a.x;

import androidx.annotation.NonNull;
/* compiled from: SizeSelectors.java */
/* loaded from: classes3.dex */
public class h implements j {
    public final /* synthetic */ int a;

    public h(int i) {
        this.a = i;
    }

    @Override // b.o.a.x.j
    public boolean a(@NonNull b bVar) {
        return bVar.k * bVar.j >= this.a;
    }
}
