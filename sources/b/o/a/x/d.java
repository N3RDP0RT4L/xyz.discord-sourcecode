package b.o.a.x;

import androidx.annotation.NonNull;
/* compiled from: SizeSelectors.java */
/* loaded from: classes3.dex */
public class d implements j {
    public final /* synthetic */ float a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ float f1954b;

    public d(float f, float f2) {
        this.a = f;
        this.f1954b = f2;
    }

    @Override // b.o.a.x.j
    public boolean a(@NonNull b bVar) {
        float i = a.f(bVar.j, bVar.k).i();
        float f = this.a;
        float f2 = this.f1954b;
        return i >= f - f2 && i <= f + f2;
    }
}
