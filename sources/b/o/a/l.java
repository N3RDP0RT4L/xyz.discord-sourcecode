package b.o.a;

import android.location.Location;
import androidx.annotation.NonNull;
import b.o.a.m.e;
import b.o.a.m.j;
import b.o.a.x.b;
/* compiled from: PictureResult.java */
/* loaded from: classes3.dex */
public class l {
    public final byte[] a;

    /* compiled from: PictureResult.java */
    /* loaded from: classes3.dex */
    public static class a {
        public boolean a;

        /* renamed from: b  reason: collision with root package name */
        public Location f1908b;
        public int c;
        public b d;
        public e e;
        public byte[] f;
        public j g;
    }

    public l(@NonNull a aVar) {
        boolean z2 = aVar.a;
        this.a = aVar.f;
    }
}
