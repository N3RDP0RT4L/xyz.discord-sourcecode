package b.o.a.o;

import androidx.annotation.NonNull;
/* compiled from: Filter.java */
/* loaded from: classes3.dex */
public interface b {
    @NonNull
    b a();

    @NonNull
    String c();

    void e(long j, @NonNull float[] fArr);

    void f();

    @NonNull
    String h();

    void j(int i);

    void k(int i, int i2);
}
