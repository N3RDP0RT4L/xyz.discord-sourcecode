package b.o.a.r;

import android.content.Context;
import android.hardware.display.DisplayManager;
import android.os.Handler;
import android.os.Looper;
import android.view.OrientationEventListener;
import android.view.WindowManager;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.otaliastudios.cameraview.CameraView;
/* compiled from: OrientationHelper.java */
/* loaded from: classes3.dex */
public class f {

    /* renamed from: b  reason: collision with root package name */
    public final Context f1947b;
    public final c c;
    @VisibleForTesting
    public final OrientationEventListener d;
    public boolean h;
    public final Handler a = new Handler(Looper.getMainLooper());
    public int e = -1;
    public int g = -1;
    @VisibleForTesting
    public final DisplayManager.DisplayListener f = new b();

    /* compiled from: OrientationHelper.java */
    /* loaded from: classes3.dex */
    public class a extends OrientationEventListener {
        public a(Context context, int i) {
            super(context, i);
        }

        /* JADX WARN: Code restructure failed: missing block: B:5:0x0008, code lost:
            if (r6 != (-1)) goto L22;
         */
        @Override // android.view.OrientationEventListener
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public void onOrientationChanged(int r6) {
            /*
                r5 = this;
                r0 = 0
                r1 = -1
                if (r6 != r1) goto Lb
                b.o.a.r.f r6 = b.o.a.r.f.this
                int r6 = r6.e
                if (r6 == r1) goto L2d
                goto L2e
            Lb:
                r1 = 315(0x13b, float:4.41E-43)
                if (r6 >= r1) goto L2d
                r2 = 45
                if (r6 >= r2) goto L14
                goto L2d
            L14:
                r3 = 135(0x87, float:1.89E-43)
                if (r6 < r2) goto L1d
                if (r6 >= r3) goto L1d
                r6 = 90
                goto L2e
            L1d:
                r2 = 225(0xe1, float:3.15E-43)
                if (r6 < r3) goto L26
                if (r6 >= r2) goto L26
                r6 = 180(0xb4, float:2.52E-43)
                goto L2e
            L26:
                if (r6 < r2) goto L2d
                if (r6 >= r1) goto L2d
                r6 = 270(0x10e, float:3.78E-43)
                goto L2e
            L2d:
                r6 = 0
            L2e:
                b.o.a.r.f r1 = b.o.a.r.f.this
                int r2 = r1.e
                if (r6 == r2) goto L87
                r1.e = r6
                b.o.a.r.f$c r1 = r1.c
                com.otaliastudios.cameraview.CameraView$b r1 = (com.otaliastudios.cameraview.CameraView.b) r1
                b.o.a.b r2 = r1.f3149b
                r3 = 2
                java.lang.Object[] r3 = new java.lang.Object[r3]
                java.lang.String r4 = "onDeviceOrientationChanged"
                r3[r0] = r4
                java.lang.Integer r0 = java.lang.Integer.valueOf(r6)
                r4 = 1
                r3[r4] = r0
                r2.a(r4, r3)
                com.otaliastudios.cameraview.CameraView r0 = com.otaliastudios.cameraview.CameraView.this
                b.o.a.r.f r2 = r0.f3147y
                int r2 = r2.g
                boolean r3 = r0.m
                if (r3 != 0) goto L6a
                int r3 = 360 - r2
                int r3 = r3 % 360
                b.o.a.n.i r0 = r0.f3148z
                b.o.a.n.t.a r0 = r0.e()
                r0.e(r3)
                r0.e = r3
                r0.d()
                goto L78
            L6a:
                b.o.a.n.i r0 = r0.f3148z
                b.o.a.n.t.a r0 = r0.e()
                r0.e(r6)
                r0.e = r6
                r0.d()
            L78:
                int r6 = r6 + r2
                int r6 = r6 % 360
                com.otaliastudios.cameraview.CameraView r0 = com.otaliastudios.cameraview.CameraView.this
                android.os.Handler r0 = r0.u
                b.o.a.j r2 = new b.o.a.j
                r2.<init>(r1, r6)
                r0.post(r2)
            L87:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.o.a.r.f.a.onOrientationChanged(int):void");
        }
    }

    /* compiled from: OrientationHelper.java */
    /* loaded from: classes3.dex */
    public class b implements DisplayManager.DisplayListener {
        public b() {
        }

        @Override // android.hardware.display.DisplayManager.DisplayListener
        public void onDisplayAdded(int i) {
        }

        @Override // android.hardware.display.DisplayManager.DisplayListener
        public void onDisplayChanged(int i) {
            f fVar = f.this;
            int i2 = fVar.g;
            int a = fVar.a();
            if (a != i2) {
                f fVar2 = f.this;
                fVar2.g = a;
                CameraView.b bVar = (CameraView.b) fVar2.c;
                if (CameraView.this.d()) {
                    bVar.f3149b.a(2, "onDisplayOffsetChanged", "restarting the camera.");
                    CameraView.this.close();
                    CameraView.this.open();
                }
            }
        }

        @Override // android.hardware.display.DisplayManager.DisplayListener
        public void onDisplayRemoved(int i) {
        }
    }

    /* compiled from: OrientationHelper.java */
    /* loaded from: classes3.dex */
    public interface c {
    }

    public f(@NonNull Context context, @NonNull c cVar) {
        this.f1947b = context;
        this.c = cVar;
        this.d = new a(context.getApplicationContext(), 3);
    }

    public final int a() {
        int rotation = ((WindowManager) this.f1947b.getSystemService("window")).getDefaultDisplay().getRotation();
        if (rotation == 1) {
            return 90;
        }
        if (rotation != 2) {
            return rotation != 3 ? 0 : 270;
        }
        return 180;
    }
}
