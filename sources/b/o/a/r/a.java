package b.o.a.r;

import android.annotation.SuppressLint;
import android.media.CamcorderProfile;
import androidx.annotation.NonNull;
import b.o.a.b;
import com.discord.utilities.voice.ScreenShareManager;
import com.discord.widgets.settings.profile.WidgetEditUserOrGuildMemberProfile;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import org.objectweb.asm.Opcodes;
/* compiled from: CamcorderProfiles.java */
/* loaded from: classes3.dex */
public class a {
    public static final b a = new b(a.class.getSimpleName());
    @SuppressLint({"UseSparseArrays"})

    /* renamed from: b  reason: collision with root package name */
    public static Map<b.o.a.x.b, Integer> f1944b;

    /* compiled from: CamcorderProfiles.java */
    /* renamed from: b.o.a.r.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public class C0175a implements Comparator<b.o.a.x.b> {
        public final /* synthetic */ long j;

        public C0175a(long j) {
            this.j = j;
        }

        @Override // java.util.Comparator
        public int compare(b.o.a.x.b bVar, b.o.a.x.b bVar2) {
            b.o.a.x.b bVar3 = bVar;
            b.o.a.x.b bVar4 = bVar2;
            int i = (Math.abs((bVar3.j * bVar3.k) - this.j) > Math.abs((bVar4.j * bVar4.k) - this.j) ? 1 : (Math.abs((bVar3.j * bVar3.k) - this.j) == Math.abs((bVar4.j * bVar4.k) - this.j) ? 0 : -1));
            if (i < 0) {
                return -1;
            }
            return i == 0 ? 0 : 1;
        }
    }

    static {
        HashMap hashMap = new HashMap();
        f1944b = hashMap;
        hashMap.put(new b.o.a.x.b(Opcodes.ARETURN, Opcodes.D2F), 2);
        f1944b.put(new b.o.a.x.b(320, 240), 7);
        f1944b.put(new b.o.a.x.b(352, ScreenShareManager.THUMBNAIL_HEIGHT_PX), 3);
        f1944b.put(new b.o.a.x.b(720, 480), 4);
        f1944b.put(new b.o.a.x.b(1280, 720), 5);
        f1944b.put(new b.o.a.x.b(1920, WidgetEditUserOrGuildMemberProfile.MAX_BANNER_IMAGE_SIZE), 6);
        f1944b.put(new b.o.a.x.b(3840, 2160), 8);
    }

    @NonNull
    public static CamcorderProfile a(int i, @NonNull b.o.a.x.b bVar) {
        long j = bVar.j * bVar.k;
        ArrayList arrayList = new ArrayList(f1944b.keySet());
        Collections.sort(arrayList, new C0175a(j));
        while (arrayList.size() > 0) {
            int intValue = f1944b.get((b.o.a.x.b) arrayList.remove(0)).intValue();
            if (CamcorderProfile.hasProfile(i, intValue)) {
                return CamcorderProfile.get(i, intValue);
            }
        }
        return CamcorderProfile.get(i, 0);
    }

    @NonNull
    public static CamcorderProfile b(@NonNull String str, @NonNull b.o.a.x.b bVar) {
        try {
            return a(Integer.parseInt(str), bVar);
        } catch (NumberFormatException unused) {
            a.a(2, "NumberFormatException for Camera2 id:", str);
            return CamcorderProfile.get(0);
        }
    }
}
