package b.o.a.r;

import android.opengl.GLES20;
import androidx.annotation.NonNull;
import b.o.b.a.d;
import b.o.b.c.f;
import b.o.b.f.b;
import d0.p;
import d0.z.d.m;
/* compiled from: GlTextureDrawer.java */
/* loaded from: classes3.dex */
public class c {
    public final b a;

    /* renamed from: b  reason: collision with root package name */
    public float[] f1946b = (float[]) d.a.clone();
    @NonNull
    public b.o.a.o.b c = new b.o.a.o.c();
    public b.o.a.o.b d = null;
    public int e = -1;

    public c(@NonNull b bVar) {
        this.a = bVar;
    }

    public void a(long j) {
        if (this.d != null) {
            b();
            this.c = this.d;
            this.d = null;
        }
        if (this.e == -1) {
            String c = this.c.c();
            String h = this.c.h();
            m.checkNotNullParameter(c, "vertexShaderSource");
            m.checkNotNullParameter(h, "fragmentShaderSource");
            b.o.b.d.c[] cVarArr = {new b.o.b.d.c(f.n, c), new b.o.b.d.c(f.o, h)};
            m.checkNotNullParameter(cVarArr, "shaders");
            int i = p.m81constructorimpl(GLES20.glCreateProgram());
            d.b("glCreateProgram");
            if (i != 0) {
                for (int i2 = 0; i2 < 2; i2++) {
                    GLES20.glAttachShader(i, p.m81constructorimpl(cVarArr[i2].f1962b));
                    d.b("glAttachShader");
                }
                GLES20.glLinkProgram(i);
                int[] iArr = new int[1];
                GLES20.glGetProgramiv(i, f.m, iArr, 0);
                int i3 = iArr[0];
                int i4 = f.a;
                if (i3 == 1) {
                    this.e = i;
                    this.c.j(i);
                    d.b("program creation");
                } else {
                    String stringPlus = m.stringPlus("Could not link program: ", GLES20.glGetProgramInfoLog(i));
                    GLES20.glDeleteProgram(i);
                    throw new RuntimeException(stringPlus);
                }
            } else {
                throw new RuntimeException("Could not create program");
            }
        }
        GLES20.glUseProgram(this.e);
        d.b("glUseProgram(handle)");
        this.a.a();
        this.c.e(j, this.f1946b);
        this.a.b();
        GLES20.glUseProgram(0);
        d.b("glUseProgram(0)");
    }

    public void b() {
        if (this.e != -1) {
            this.c.f();
            GLES20.glDeleteProgram(this.e);
            this.e = -1;
        }
    }
}
