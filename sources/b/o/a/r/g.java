package b.o.a.r;

import android.os.Handler;
import android.os.HandlerThread;
import androidx.annotation.NonNull;
import java.lang.ref.WeakReference;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
/* compiled from: WorkerHandler.java */
/* loaded from: classes3.dex */
public class g {
    public static final b.o.a.b a = new b.o.a.b(g.class.getSimpleName());

    /* renamed from: b  reason: collision with root package name */
    public static final ConcurrentHashMap<String, WeakReference<g>> f1948b = new ConcurrentHashMap<>(4);
    public static g c;
    public String d;
    public HandlerThread e;
    public Handler f;
    public Executor g = new b();

    /* compiled from: WorkerHandler.java */
    /* loaded from: classes3.dex */
    public class a extends HandlerThread {
        public a(g gVar, String str) {
            super(str);
        }

        @Override // java.lang.Thread
        @NonNull
        public String toString() {
            return super.toString() + "[" + getThreadId() + "]";
        }
    }

    /* compiled from: WorkerHandler.java */
    /* loaded from: classes3.dex */
    public class b implements Executor {
        public b() {
        }

        @Override // java.util.concurrent.Executor
        public void execute(@NonNull Runnable runnable) {
            g.this.c(runnable);
        }
    }

    /* compiled from: WorkerHandler.java */
    /* loaded from: classes3.dex */
    public class c implements Runnable {
        public final /* synthetic */ CountDownLatch j;

        public c(g gVar, CountDownLatch countDownLatch) {
            this.j = countDownLatch;
        }

        @Override // java.lang.Runnable
        public void run() {
            this.j.countDown();
        }
    }

    public g(@NonNull String str) {
        this.d = str;
        a aVar = new a(this, str);
        this.e = aVar;
        aVar.setDaemon(true);
        this.e.start();
        this.f = new Handler(this.e.getLooper());
        CountDownLatch countDownLatch = new CountDownLatch(1);
        this.f.post(new c(this, countDownLatch));
        try {
            countDownLatch.await();
        } catch (InterruptedException unused) {
        }
    }

    public static void a(@NonNull Runnable runnable) {
        g b2 = b("FallbackCameraThread");
        c = b2;
        b2.f.post(runnable);
    }

    @NonNull
    public static g b(@NonNull String str) {
        ConcurrentHashMap<String, WeakReference<g>> concurrentHashMap = f1948b;
        if (concurrentHashMap.containsKey(str)) {
            g gVar = concurrentHashMap.get(str).get();
            if (gVar == null) {
                a.a(2, "get:", "Thread reference died. Removing.", str);
                concurrentHashMap.remove(str);
            } else if (!gVar.e.isAlive() || gVar.e.isInterrupted()) {
                HandlerThread handlerThread = gVar.e;
                if (handlerThread.isAlive()) {
                    handlerThread.interrupt();
                    handlerThread.quit();
                }
                concurrentHashMap.remove(gVar.d);
                a.a(2, "get:", "Thread reference found, but not alive or interrupted.", "Removing.", str);
                concurrentHashMap.remove(str);
            } else {
                a.a(2, "get:", "Reusing cached worker handler.", str);
                return gVar;
            }
        }
        a.a(1, "get:", "Creating new handler.", str);
        g gVar2 = new g(str);
        concurrentHashMap.put(str, new WeakReference<>(gVar2));
        return gVar2;
    }

    public void c(@NonNull Runnable runnable) {
        if (Thread.currentThread() == this.e) {
            runnable.run();
        } else {
            this.f.post(runnable);
        }
    }
}
