package b.o.a;

import androidx.annotation.NonNull;
import androidx.annotation.UiThread;
import com.otaliastudios.cameraview.CameraException;
/* compiled from: CameraListener.java */
/* loaded from: classes3.dex */
public abstract class a {
    @UiThread
    public void a() {
    }

    @UiThread
    public void b(@NonNull CameraException cameraException) {
    }

    @UiThread
    public void c(@NonNull c cVar) {
    }

    @UiThread
    public void d(@NonNull l lVar) {
    }
}
