package b.o.a;

import androidx.annotation.NonNull;
import b.o.a.m.d;
import b.o.a.m.e;
import b.o.a.m.f;
import b.o.a.m.g;
import b.o.a.m.h;
import b.o.a.m.i;
import b.o.a.m.j;
import b.o.a.m.k;
import b.o.a.m.l;
import b.o.a.m.m;
import b.o.a.x.a;
import b.o.a.x.b;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
/* compiled from: CameraOptions.java */
/* loaded from: classes3.dex */
public abstract class c {
    public Set<m> a = new HashSet(5);

    /* renamed from: b  reason: collision with root package name */
    public Set<e> f1907b = new HashSet(2);
    public Set<f> c = new HashSet(4);
    public Set<h> d = new HashSet(2);
    public Set<b> e = new HashSet(15);
    public Set<b> f = new HashSet(5);
    public Set<a> g = new HashSet(4);
    public Set<a> h = new HashSet(3);
    public Set<j> i = new HashSet(2);
    public Set<Integer> j = new HashSet(2);
    public boolean k;
    public boolean l;
    public float m;
    public float n;
    public boolean o;
    public float p;
    public float q;

    @NonNull
    public final Collection<e> a() {
        return Collections.unmodifiableSet(this.f1907b);
    }

    @NonNull
    public final Collection<f> b() {
        return Collections.unmodifiableSet(this.c);
    }

    public final boolean c(@NonNull b.o.a.m.c cVar) {
        Collection collection;
        Class<?> cls = cVar.getClass();
        if (cls.equals(b.o.a.m.a.class)) {
            collection = Arrays.asList(b.o.a.m.a.values());
        } else if (cls.equals(e.class)) {
            collection = a();
        } else if (cls.equals(f.class)) {
            collection = b();
        } else if (cls.equals(g.class)) {
            collection = Arrays.asList(g.values());
        } else if (cls.equals(h.class)) {
            collection = Collections.unmodifiableSet(this.d);
        } else if (cls.equals(i.class)) {
            collection = Arrays.asList(i.values());
        } else if (cls.equals(l.class)) {
            collection = Arrays.asList(l.values());
        } else if (cls.equals(b.o.a.m.b.class)) {
            collection = Arrays.asList(b.o.a.m.b.values());
        } else if (cls.equals(m.class)) {
            collection = Collections.unmodifiableSet(this.a);
        } else if (cls.equals(d.class)) {
            collection = Arrays.asList(d.values());
        } else if (cls.equals(k.class)) {
            collection = Arrays.asList(k.values());
        } else if (cls.equals(j.class)) {
            collection = Collections.unmodifiableSet(this.i);
        } else {
            collection = Collections.emptyList();
        }
        return collection.contains(cVar);
    }
}
