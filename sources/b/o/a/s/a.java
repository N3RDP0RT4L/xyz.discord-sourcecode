package b.o.a.s;

import android.content.Context;
import android.graphics.PointF;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
/* compiled from: AutoFocusMarker.java */
/* loaded from: classes3.dex */
public interface a {
    void a(@NonNull b bVar, @NonNull PointF pointF);

    @Nullable
    View b(@NonNull Context context, @NonNull ViewGroup viewGroup);

    void c(@NonNull b bVar, boolean z2, @NonNull PointF pointF);
}
