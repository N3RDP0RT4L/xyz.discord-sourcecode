package b.o.a.n;

import b.o.a.x.b;
/* compiled from: CameraBaseEngine.java */
/* loaded from: classes3.dex */
public class h implements Runnable {
    public final /* synthetic */ g j;

    public h(g gVar) {
        this.j = gVar;
    }

    @Override // java.lang.Runnable
    public void run() {
        b R0 = this.j.R0();
        if (R0.equals(this.j.f1927s)) {
            i.j.a(1, "onSurfaceChanged:", "The computed preview size is identical. No op.");
            return;
        }
        i.j.a(1, "onSurfaceChanged:", "Computed a new preview size. Calling onPreviewStreamSizeChanged().");
        g gVar = this.j;
        gVar.f1927s = R0;
        gVar.X0();
    }
}
