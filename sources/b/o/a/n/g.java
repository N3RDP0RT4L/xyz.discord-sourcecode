package b.o.a.n;

import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import b.o.a.l;
import b.o.a.m.e;
import b.o.a.m.f;
import b.o.a.m.h;
import b.o.a.m.i;
import b.o.a.m.j;
import b.o.a.m.l;
import b.o.a.m.m;
import b.o.a.n.i;
import com.otaliastudios.cameraview.CameraException;
import com.otaliastudios.cameraview.CameraView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
/* compiled from: CameraBaseEngine.java */
/* loaded from: classes3.dex */
public abstract class g extends i {
    public h A;
    public j B;
    public Location C;
    public float D;
    public float E;
    public boolean F;
    public boolean G;
    public boolean H;
    public float I;
    public boolean J;
    public b.o.a.p.c K;
    public final b.o.a.n.t.a L = new b.o.a.n.t.a();
    @Nullable
    public b.o.a.x.c M;
    public b.o.a.x.c N;
    public b.o.a.x.c O;
    public e P;
    public i Q;
    public b.o.a.m.a R;
    public long S;
    public int T;
    public int U;
    public int V;
    public long W;
    public int X;
    public int Y;
    public int Z;

    /* renamed from: a0  reason: collision with root package name */
    public int f1924a0;

    /* renamed from: b0  reason: collision with root package name */
    public int f1925b0;

    /* renamed from: c0  reason: collision with root package name */
    public b.o.a.u.a f1926c0;
    public b.o.a.w.a o;
    public b.o.a.c p;
    public b.o.a.v.d q;
    public b.o.a.x.b r;

    /* renamed from: s  reason: collision with root package name */
    public b.o.a.x.b f1927s;
    public b.o.a.x.b t;
    public int u;
    public boolean v;
    public f w;

    /* renamed from: x  reason: collision with root package name */
    public m f1928x;

    /* renamed from: y  reason: collision with root package name */
    public l f1929y;

    /* renamed from: z  reason: collision with root package name */
    public b.o.a.m.b f1930z;

    /* compiled from: CameraBaseEngine.java */
    /* loaded from: classes3.dex */
    public class a implements Runnable {
        public final /* synthetic */ e j;
        public final /* synthetic */ e k;

        public a(e eVar, e eVar2) {
            this.j = eVar;
            this.k = eVar2;
        }

        @Override // java.lang.Runnable
        public void run() {
            if (g.this.c(this.j)) {
                g.this.X();
                return;
            }
            g.this.P = this.k;
        }
    }

    /* compiled from: CameraBaseEngine.java */
    /* loaded from: classes3.dex */
    public class b implements Runnable {
        public b() {
        }

        @Override // java.lang.Runnable
        public void run() {
            g.this.X();
        }
    }

    /* compiled from: CameraBaseEngine.java */
    /* loaded from: classes3.dex */
    public class c implements Runnable {
        public final /* synthetic */ l.a j;
        public final /* synthetic */ boolean k;

        public c(l.a aVar, boolean z2) {
            this.j = aVar;
            this.k = z2;
        }

        @Override // java.lang.Runnable
        public void run() {
            i.j.a(1, "takePicture:", "running. isTakingPicture:", Boolean.valueOf(g.this.W0()));
            if (!g.this.W0()) {
                g gVar = g.this;
                if (gVar.Q != i.VIDEO) {
                    l.a aVar = this.j;
                    aVar.a = false;
                    aVar.f1908b = gVar.C;
                    aVar.e = gVar.P;
                    aVar.g = gVar.B;
                    gVar.Y0(aVar, this.k);
                    return;
                }
                throw new IllegalStateException("Can't take hq pictures while in VIDEO mode");
            }
        }
    }

    /* compiled from: CameraBaseEngine.java */
    /* loaded from: classes3.dex */
    public class d implements Runnable {
        public final /* synthetic */ l.a j;
        public final /* synthetic */ boolean k;

        public d(l.a aVar, boolean z2) {
            this.j = aVar;
            this.k = z2;
        }

        @Override // java.lang.Runnable
        public void run() {
            i.j.a(1, "takePictureSnapshot:", "running. isTakingPicture:", Boolean.valueOf(g.this.W0()));
            if (!g.this.W0()) {
                l.a aVar = this.j;
                g gVar = g.this;
                aVar.f1908b = gVar.C;
                aVar.a = true;
                aVar.e = gVar.P;
                aVar.g = j.JPEG;
                g.this.Z0(this.j, b.o.a.x.a.g(gVar.U0(b.o.a.n.t.b.OUTPUT)), this.k);
            }
        }
    }

    public g(@NonNull i.g gVar) {
        super(gVar);
        b.i.a.f.e.o.f.Z(null);
        b.i.a.f.e.o.f.Z(null);
        b.i.a.f.e.o.f.Z(null);
        b.i.a.f.e.o.f.Z(null);
        b.i.a.f.e.o.f.Z(null);
        b.i.a.f.e.o.f.Z(null);
        b.i.a.f.e.o.f.Z(null);
        b.i.a.f.e.o.f.Z(null);
    }

    @Override // b.o.a.n.i
    public final float A() {
        return this.I;
    }

    @Override // b.o.a.n.i
    public final void A0(int i) {
        this.U = i;
    }

    @Override // b.o.a.n.i
    public final boolean B() {
        return this.J;
    }

    @Override // b.o.a.n.i
    public final void B0(@NonNull b.o.a.m.l lVar) {
        this.f1929y = lVar;
    }

    @Override // b.o.a.n.i
    @Nullable
    public final b.o.a.x.b C(@NonNull b.o.a.n.t.b bVar) {
        b.o.a.x.b bVar2 = this.f1927s;
        if (bVar2 == null) {
            return null;
        }
        return this.L.b(b.o.a.n.t.b.SENSOR, bVar) ? bVar2.f() : bVar2;
    }

    @Override // b.o.a.n.i
    public final void C0(int i) {
        this.T = i;
    }

    @Override // b.o.a.n.i
    public final int D() {
        return this.Y;
    }

    @Override // b.o.a.n.i
    public final void D0(long j) {
        this.S = j;
    }

    @Override // b.o.a.n.i
    public final int E() {
        return this.X;
    }

    @Override // b.o.a.n.i
    public final void E0(@NonNull b.o.a.x.c cVar) {
        this.O = cVar;
    }

    @Override // b.o.a.n.i
    @Nullable
    public final b.o.a.x.b F(@NonNull b.o.a.n.t.b bVar) {
        b.o.a.x.b C = C(bVar);
        if (C == null) {
            return null;
        }
        boolean b2 = this.L.b(bVar, b.o.a.n.t.b.VIEW);
        int i = b2 ? this.Y : this.X;
        int i2 = b2 ? this.X : this.Y;
        if (i <= 0) {
            i = Integer.MAX_VALUE;
        }
        if (i2 <= 0) {
            i2 = Integer.MAX_VALUE;
        }
        HashMap<String, b.o.a.x.a> hashMap = b.o.a.x.a.j;
        float i3 = b.o.a.x.a.f(C.j, C.k).i();
        if (b.o.a.x.a.f(i, i2).i() >= i3) {
            int min = Math.min(C.k, i2);
            return new b.o.a.x.b((int) Math.floor(min * i3), min);
        }
        int min2 = Math.min(C.j, i);
        return new b.o.a.x.b(min2, (int) Math.floor(min2 / i3));
    }

    @Override // b.o.a.n.i
    public final int G() {
        return this.U;
    }

    @Override // b.o.a.n.i
    @NonNull
    public final b.o.a.m.l H() {
        return this.f1929y;
    }

    @Override // b.o.a.n.i
    public final int I() {
        return this.T;
    }

    @Override // b.o.a.n.i
    public final long J() {
        return this.S;
    }

    @Override // b.o.a.n.i
    @Nullable
    public final b.o.a.x.b K(@NonNull b.o.a.n.t.b bVar) {
        b.o.a.x.b bVar2 = this.r;
        if (bVar2 == null || this.Q == b.o.a.m.i.PICTURE) {
            return null;
        }
        return this.L.b(b.o.a.n.t.b.SENSOR, bVar) ? bVar2.f() : bVar2;
    }

    @Override // b.o.a.n.i
    @NonNull
    public final b.o.a.x.c L() {
        return this.O;
    }

    @Override // b.o.a.n.i
    @NonNull
    public final m M() {
        return this.f1928x;
    }

    @Override // b.o.a.n.i
    public final float N() {
        return this.D;
    }

    @Override // b.o.a.n.i
    public void O0(@NonNull l.a aVar) {
        boolean z2 = this.G;
        b.o.a.n.v.f fVar = this.n;
        fVar.b("take picture", true, new b.o.a.n.v.h(fVar, b.o.a.n.v.e.BIND, new c(aVar, z2)));
    }

    @Override // b.o.a.n.i
    public void P0(@NonNull l.a aVar) {
        boolean z2 = this.H;
        b.o.a.n.v.f fVar = this.n;
        fVar.b("take picture snapshot", true, new b.o.a.n.v.h(fVar, b.o.a.n.v.e.BIND, new d(aVar, z2)));
    }

    @NonNull
    public final b.o.a.x.b Q0(@NonNull b.o.a.m.i iVar) {
        Set set;
        b.o.a.x.c cVar;
        boolean b2 = this.L.b(b.o.a.n.t.b.SENSOR, b.o.a.n.t.b.VIEW);
        if (iVar == b.o.a.m.i.PICTURE) {
            cVar = this.N;
            set = Collections.unmodifiableSet(this.p.e);
        } else {
            cVar = this.O;
            set = Collections.unmodifiableSet(this.p.f);
        }
        b.o.a.x.c Q0 = b.i.a.f.e.o.f.Q0(cVar, new b.o.a.x.e());
        ArrayList arrayList = new ArrayList(set);
        b.o.a.x.b bVar = ((b.o.a.x.l) Q0).a(arrayList).get(0);
        if (arrayList.contains(bVar)) {
            i.j.a(1, "computeCaptureSize:", "result:", bVar, "flip:", Boolean.valueOf(b2), "mode:", iVar);
            return b2 ? bVar.f() : bVar;
        }
        throw new RuntimeException("SizeSelectors must not return Sizes other than those in the input list.");
    }

    @NonNull
    public final b.o.a.x.b R0() {
        b.o.a.n.t.b bVar = b.o.a.n.t.b.VIEW;
        List<b.o.a.x.b> T0 = T0();
        boolean b2 = this.L.b(b.o.a.n.t.b.SENSOR, bVar);
        ArrayList arrayList = new ArrayList(T0.size());
        for (b.o.a.x.b bVar2 : T0) {
            if (b2) {
                bVar2 = bVar2.f();
            }
            arrayList.add(bVar2);
        }
        b.o.a.x.b U0 = U0(bVar);
        if (U0 != null) {
            b.o.a.x.b bVar3 = this.r;
            b.o.a.x.a f = b.o.a.x.a.f(bVar3.j, bVar3.k);
            if (b2) {
                f = b.o.a.x.a.f(f.l, f.k);
            }
            b.o.a.b bVar4 = i.j;
            bVar4.a(1, "computePreviewStreamSize:", "targetRatio:", f, "targetMinSize:", U0);
            b.o.a.x.c g = b.i.a.f.e.o.f.g(b.i.a.f.e.o.f.D1(new b.o.a.x.d(f.i(), 0.0f)), new b.o.a.x.e());
            b.o.a.x.c g2 = b.i.a.f.e.o.f.g(b.i.a.f.e.o.f.M0(U0.k), b.i.a.f.e.o.f.N0(U0.j), new b.o.a.x.f());
            b.o.a.x.c Q0 = b.i.a.f.e.o.f.Q0(b.i.a.f.e.o.f.g(g, g2), g2, g, new b.o.a.x.e());
            b.o.a.x.c cVar = this.M;
            if (cVar != null) {
                Q0 = b.i.a.f.e.o.f.Q0(cVar, Q0);
            }
            b.o.a.x.b bVar5 = ((b.o.a.x.l) Q0).a(arrayList).get(0);
            if (arrayList.contains(bVar5)) {
                if (b2) {
                    bVar5 = bVar5.f();
                }
                bVar4.a(1, "computePreviewStreamSize:", "result:", bVar5, "flip:", Boolean.valueOf(b2));
                return bVar5;
            }
            throw new RuntimeException("SizeSelectors must not return Sizes other than those in the input list.");
        }
        throw new IllegalStateException("targetMinSize should not be null here.");
    }

    @NonNull
    public b.o.a.p.c S0() {
        if (this.K == null) {
            this.K = V0(this.f1925b0);
        }
        return this.K;
    }

    @NonNull
    public abstract List<b.o.a.x.b> T0();

    @Nullable
    public final b.o.a.x.b U0(@NonNull b.o.a.n.t.b bVar) {
        b.o.a.w.a aVar = this.o;
        if (aVar == null) {
            return null;
        }
        return this.L.b(b.o.a.n.t.b.VIEW, bVar) ? aVar.l().f() : aVar.l();
    }

    @NonNull
    public abstract b.o.a.p.c V0(int i);

    public final boolean W0() {
        return this.q != null;
    }

    public abstract void X0();

    public abstract void Y0(@NonNull l.a aVar, boolean z2);

    @Override // b.o.a.n.i
    public final void Z(@NonNull b.o.a.m.a aVar) {
        if (this.R != aVar) {
            this.R = aVar;
        }
    }

    public abstract void Z0(@NonNull l.a aVar, @NonNull b.o.a.x.a aVar2, boolean z2);

    public void a(@Nullable l.a aVar, @Nullable Exception exc) {
        this.q = null;
        if (aVar != null) {
            CameraView.b bVar = (CameraView.b) this.m;
            bVar.f3149b.a(1, "dispatchOnPictureTaken", aVar);
            CameraView.this.u.post(new b.o.a.i(bVar, aVar));
            return;
        }
        i.j.a(3, "onPictureResult", "result is null: something went wrong.", exc);
        ((CameraView.b) this.m).a(new CameraException(exc, 4));
    }

    @Override // b.o.a.n.i
    public final void a0(int i) {
        this.V = i;
    }

    public final boolean a1() {
        long j = this.W;
        return j > 0 && j != RecyclerView.FOREVER_NS;
    }

    @Override // b.o.a.n.i
    public final void b0(@NonNull b.o.a.m.b bVar) {
        this.f1930z = bVar;
    }

    @Override // b.o.a.n.i
    public final void c0(long j) {
        this.W = j;
    }

    @Override // b.o.a.n.i
    @NonNull
    public final b.o.a.n.t.a e() {
        return this.L;
    }

    @Override // b.o.a.n.i
    public final void e0(@NonNull e eVar) {
        e eVar2 = this.P;
        if (eVar != eVar2) {
            this.P = eVar;
            b.o.a.n.v.f fVar = this.n;
            fVar.b("facing", true, new b.o.a.n.v.h(fVar, b.o.a.n.v.e.ENGINE, new a(eVar, eVar2)));
        }
    }

    @Override // b.o.a.n.i
    @NonNull
    public final b.o.a.m.a f() {
        return this.R;
    }

    @Override // b.o.a.n.i
    public final int g() {
        return this.V;
    }

    @Override // b.o.a.n.i
    @NonNull
    public final b.o.a.m.b h() {
        return this.f1930z;
    }

    @Override // b.o.a.n.i
    public final void h0(int i) {
        this.f1924a0 = i;
    }

    @Override // b.o.a.n.i
    public final long i() {
        return this.W;
    }

    @Override // b.o.a.n.i
    public final void i0(int i) {
        this.Z = i;
    }

    @Override // b.o.a.n.i
    @Nullable
    public final b.o.a.c j() {
        return this.p;
    }

    @Override // b.o.a.n.i
    public final void j0(int i) {
        this.f1925b0 = i;
    }

    @Override // b.o.a.n.i
    public final float k() {
        return this.E;
    }

    @Override // b.o.a.n.i
    @NonNull
    public final e l() {
        return this.P;
    }

    @Override // b.o.a.n.i
    @NonNull
    public final f m() {
        return this.w;
    }

    @Override // b.o.a.n.i
    public final int n() {
        return this.u;
    }

    @Override // b.o.a.n.i
    public final void n0(@NonNull b.o.a.m.i iVar) {
        if (iVar != this.Q) {
            this.Q = iVar;
            b.o.a.n.v.f fVar = this.n;
            fVar.b("mode", true, new b.o.a.n.v.h(fVar, b.o.a.n.v.e.ENGINE, new b()));
        }
    }

    @Override // b.o.a.n.i
    public final int o() {
        return this.f1924a0;
    }

    @Override // b.o.a.n.i
    public final void o0(@Nullable b.o.a.u.a aVar) {
        this.f1926c0 = aVar;
    }

    @Override // b.o.a.n.i
    public final int p() {
        return this.Z;
    }

    @Override // b.o.a.n.i
    public final int q() {
        return this.f1925b0;
    }

    @Override // b.o.a.n.i
    public final void q0(boolean z2) {
        this.G = z2;
    }

    @Override // b.o.a.n.i
    @NonNull
    public final h r() {
        return this.A;
    }

    @Override // b.o.a.n.i
    public final void r0(@NonNull b.o.a.x.c cVar) {
        this.N = cVar;
    }

    @Override // b.o.a.n.i
    @Nullable
    public final Location s() {
        return this.C;
    }

    @Override // b.o.a.n.i
    public final void s0(boolean z2) {
        this.H = z2;
    }

    @Override // b.o.a.n.i
    @NonNull
    public final b.o.a.m.i t() {
        return this.Q;
    }

    @Override // b.o.a.n.i
    @NonNull
    public final j u() {
        return this.B;
    }

    @Override // b.o.a.n.i
    public final void u0(@NonNull b.o.a.w.a aVar) {
        b.o.a.w.a aVar2 = this.o;
        if (aVar2 != null) {
            aVar2.t(null);
        }
        this.o = aVar;
        aVar.t(this);
    }

    @Override // b.o.a.n.i
    public final boolean v() {
        return this.G;
    }

    @Override // b.o.a.n.i
    @Nullable
    public final b.o.a.x.b w(@NonNull b.o.a.n.t.b bVar) {
        b.o.a.x.b bVar2 = this.r;
        if (bVar2 == null || this.Q == b.o.a.m.i.VIDEO) {
            return null;
        }
        return this.L.b(b.o.a.n.t.b.SENSOR, bVar) ? bVar2.f() : bVar2;
    }

    @Override // b.o.a.n.i
    public final void w0(boolean z2) {
        this.J = z2;
    }

    @Override // b.o.a.n.i
    @NonNull
    public final b.o.a.x.c x() {
        return this.N;
    }

    @Override // b.o.a.n.i
    public final void x0(@Nullable b.o.a.x.c cVar) {
        this.M = cVar;
    }

    @Override // b.o.a.n.i
    public final boolean y() {
        return this.H;
    }

    @Override // b.o.a.n.i
    public final void y0(int i) {
        this.Y = i;
    }

    @Override // b.o.a.n.i
    @NonNull
    public final b.o.a.w.a z() {
        return this.o;
    }

    @Override // b.o.a.n.i
    public final void z0(int i) {
        this.X = i;
    }
}
