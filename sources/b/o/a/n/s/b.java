package b.o.a.n.s;

import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.MeteringRectangle;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import b.o.a.n.t.a;
import b.o.a.t.c;
/* compiled from: Camera2MeteringTransform.java */
@RequiresApi(21)
/* loaded from: classes3.dex */
public class b implements c<MeteringRectangle> {
    public static final b.o.a.b a = new b.o.a.b(b.class.getSimpleName());

    /* renamed from: b  reason: collision with root package name */
    public final a f1935b;
    public final b.o.a.x.b c;
    public final b.o.a.x.b d;
    public final boolean e;
    public final CameraCharacteristics f;
    public final CaptureRequest.Builder g;

    public b(@NonNull a aVar, @NonNull b.o.a.x.b bVar, @NonNull b.o.a.x.b bVar2, boolean z2, @NonNull CameraCharacteristics cameraCharacteristics, @NonNull CaptureRequest.Builder builder) {
        this.f1935b = aVar;
        this.c = bVar;
        this.d = bVar2;
        this.e = z2;
        this.f = cameraCharacteristics;
        this.g = builder;
    }

    @Override // b.o.a.t.c
    @NonNull
    public MeteringRectangle a(@NonNull RectF rectF, int i) {
        Rect rect = new Rect();
        rectF.round(rect);
        return new MeteringRectangle(rect, i);
    }

    @Override // b.o.a.t.c
    @NonNull
    public PointF b(@NonNull PointF pointF) {
        int i;
        int i2;
        PointF pointF2 = new PointF(pointF.x, pointF.y);
        b.o.a.x.b bVar = this.c;
        b.o.a.x.b bVar2 = this.d;
        int i3 = bVar.j;
        int i4 = bVar.k;
        b.o.a.x.a g = b.o.a.x.a.g(bVar2);
        b.o.a.x.a f = b.o.a.x.a.f(bVar.j, bVar.k);
        if (this.e) {
            if (g.i() > f.i()) {
                float i5 = g.i() / f.i();
                float f2 = pointF2.x;
                float f3 = bVar.j;
                pointF2.x = (((i5 - 1.0f) * f3) / 2.0f) + f2;
                i3 = Math.round(f3 * i5);
            } else {
                float i6 = f.i() / g.i();
                float f4 = pointF2.y;
                float f5 = bVar.k;
                pointF2.y = (((i6 - 1.0f) * f5) / 2.0f) + f4;
                i4 = Math.round(f5 * i6);
            }
        }
        b.o.a.x.b bVar3 = this.d;
        pointF2.x = (bVar3.j / i3) * pointF2.x;
        pointF2.y = (bVar3.k / i4) * pointF2.y;
        int c = this.f1935b.c(b.o.a.n.t.b.SENSOR, b.o.a.n.t.b.VIEW, 1);
        boolean z2 = c % 180 != 0;
        float f6 = pointF2.x;
        float f7 = pointF2.y;
        if (c == 0) {
            pointF2.x = f6;
            pointF2.y = f7;
        } else if (c == 90) {
            pointF2.x = f7;
            pointF2.y = bVar3.j - f6;
        } else if (c == 180) {
            pointF2.x = bVar3.j - f6;
            pointF2.y = bVar3.k - f7;
        } else if (c == 270) {
            pointF2.x = bVar3.k - f7;
            pointF2.y = f6;
        } else {
            throw new IllegalStateException(b.d.b.a.a.p("Unexpected angle ", c));
        }
        if (z2) {
            bVar3 = bVar3.f();
        }
        Rect rect = (Rect) this.g.get(CaptureRequest.SCALER_CROP_REGION);
        if (rect == null) {
            i = bVar3.j;
        } else {
            i = rect.width();
        }
        if (rect == null) {
            i2 = bVar3.k;
        } else {
            i2 = rect.height();
        }
        pointF2.x = ((i - bVar3.j) / 2.0f) + pointF2.x;
        pointF2.y = ((i2 - bVar3.k) / 2.0f) + pointF2.y;
        Rect rect2 = (Rect) this.g.get(CaptureRequest.SCALER_CROP_REGION);
        pointF2.x += rect2 == null ? 0.0f : rect2.left;
        pointF2.y += rect2 == null ? 0.0f : rect2.top;
        Rect rect3 = (Rect) this.f.get(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE);
        if (rect3 == null) {
            rect3 = new Rect(0, 0, i, i2);
        }
        int width = rect3.width();
        int height = rect3.height();
        b.o.a.b bVar4 = a;
        bVar4.a(1, "input:", pointF, "output (before clipping):", pointF2);
        if (pointF2.x < 0.0f) {
            pointF2.x = 0.0f;
        }
        if (pointF2.y < 0.0f) {
            pointF2.y = 0.0f;
        }
        float f8 = width;
        if (pointF2.x > f8) {
            pointF2.x = f8;
        }
        float f9 = height;
        if (pointF2.y > f9) {
            pointF2.y = f9;
        }
        bVar4.a(1, "input:", pointF, "output (after clipping):", pointF2);
        return pointF2;
    }
}
