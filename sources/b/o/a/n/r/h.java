package b.o.a.n.r;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import b.i.a.f.e.o.f;
import b.o.a.n.o.d;
import b.o.a.n.o.e;
/* compiled from: MeterResetAction.java */
@RequiresApi(21)
/* loaded from: classes3.dex */
public class h extends d {
    public final e e = f.x1(new d(), new f(), new j());

    @Override // b.o.a.n.o.d
    @NonNull
    public e m() {
        return this.e;
    }
}
