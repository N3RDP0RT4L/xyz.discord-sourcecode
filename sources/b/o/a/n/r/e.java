package b.o.a.n.r;

import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.MeteringRectangle;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import b.o.a.b;
import b.o.a.n.d;
import b.o.a.n.o.c;
import java.util.List;
/* compiled from: FocusMeter.java */
@RequiresApi(21)
/* loaded from: classes3.dex */
public class e extends a {
    public static final b i = new b(e.class.getSimpleName());

    public e(@NonNull List<MeteringRectangle> list, boolean z2) {
        super(list, z2);
    }

    @Override // b.o.a.n.o.e, b.o.a.n.o.a
    public void b(@NonNull c cVar, @NonNull CaptureRequest captureRequest, @NonNull TotalCaptureResult totalCaptureResult) {
        Integer num = (Integer) totalCaptureResult.get(CaptureResult.CONTROL_AF_STATE);
        i.a(1, "onCaptureCompleted:", "afState:", num);
        if (num != null) {
            int intValue = num.intValue();
            if (intValue == 4) {
                this.g = true;
                l(Integer.MAX_VALUE);
            } else if (intValue == 5) {
                this.g = false;
                l(Integer.MAX_VALUE);
            }
        }
    }

    @Override // b.o.a.n.o.e
    public void i(@NonNull c cVar) {
        ((d) cVar).f1917i0.set(CaptureRequest.CONTROL_AF_TRIGGER, null);
    }

    @Override // b.o.a.n.r.a
    public boolean m(@NonNull c cVar) {
        Integer num = (Integer) ((d) cVar).f1917i0.get(CaptureRequest.CONTROL_AF_MODE);
        boolean z2 = num != null && (num.intValue() == 1 || num.intValue() == 4 || num.intValue() == 3 || num.intValue() == 2);
        i.a(1, "checkIsSupported:", Boolean.valueOf(z2));
        return z2;
    }

    @Override // b.o.a.n.r.a
    public boolean n(@NonNull c cVar) {
        TotalCaptureResult totalCaptureResult = ((d) cVar).f1918j0;
        if (totalCaptureResult != null) {
            Integer num = (Integer) totalCaptureResult.get(CaptureResult.CONTROL_AF_STATE);
            boolean z2 = num != null && (num.intValue() == 4 || num.intValue() == 2);
            i.a(1, "checkShouldSkip:", Boolean.valueOf(z2));
            return z2;
        }
        i.a(1, "checkShouldSkip: false - lastResult is null.");
        return false;
    }

    @Override // b.o.a.n.r.a
    public void o(@NonNull c cVar, @NonNull List<MeteringRectangle> list) {
        i.a(1, "onStarted:", "with areas:", list);
        ((d) cVar).f1917i0.set(CaptureRequest.CONTROL_AF_TRIGGER, 1);
        int intValue = ((Integer) k(CameraCharacteristics.CONTROL_MAX_REGIONS_AF, 0)).intValue();
        if (!list.isEmpty() && intValue > 0) {
            ((d) cVar).f1917i0.set(CaptureRequest.CONTROL_AF_REGIONS, (MeteringRectangle[]) list.subList(0, Math.min(intValue, list.size())).toArray(new MeteringRectangle[0]));
        }
        ((d) cVar).k1();
    }
}
