package b.o.a.n.r;

import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.MeteringRectangle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import b.o.a.b;
import b.o.a.n.d;
import b.o.a.n.o.c;
/* compiled from: FocusReset.java */
@RequiresApi(21)
/* loaded from: classes3.dex */
public class f extends b {
    public static final b f = new b(f.class.getSimpleName());

    public f() {
        super(true);
    }

    @Override // b.o.a.n.r.b
    public void m(@NonNull c cVar, @Nullable MeteringRectangle meteringRectangle) {
        boolean z2;
        int intValue = ((Integer) k(CameraCharacteristics.CONTROL_MAX_REGIONS_AF, 0)).intValue();
        boolean z3 = true;
        if (meteringRectangle == null || intValue <= 0) {
            z2 = false;
        } else {
            ((d) cVar).f1917i0.set(CaptureRequest.CONTROL_AF_REGIONS, new MeteringRectangle[]{meteringRectangle});
            z2 = true;
        }
        TotalCaptureResult totalCaptureResult = ((d) cVar).f1918j0;
        Integer num = totalCaptureResult == null ? null : (Integer) totalCaptureResult.get(CaptureResult.CONTROL_AF_TRIGGER);
        f.a(2, "onStarted:", "last focus trigger is", num);
        if (num == null || num.intValue() != 1) {
            z3 = z2;
        } else {
            ((d) cVar).f1917i0.set(CaptureRequest.CONTROL_AF_TRIGGER, 2);
        }
        if (z3) {
            ((d) cVar).k1();
        }
        l(Integer.MAX_VALUE);
    }
}
