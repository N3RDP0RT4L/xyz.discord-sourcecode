package b.o.a.n.r;

import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.MeteringRectangle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import b.o.a.b;
import b.o.a.n.d;
import b.o.a.n.o.c;
/* compiled from: WhiteBalanceReset.java */
@RequiresApi(21)
/* loaded from: classes3.dex */
public class j extends b {
    public static final b f = new b(j.class.getSimpleName());

    public j() {
        super(true);
    }

    @Override // b.o.a.n.r.b
    public void m(@NonNull c cVar, @Nullable MeteringRectangle meteringRectangle) {
        f.a(2, "onStarted:", "with area:", meteringRectangle);
        int intValue = ((Integer) k(CameraCharacteristics.CONTROL_MAX_REGIONS_AWB, 0)).intValue();
        if (meteringRectangle != null && intValue > 0) {
            ((d) cVar).f1917i0.set(CaptureRequest.CONTROL_AWB_REGIONS, new MeteringRectangle[]{meteringRectangle});
            ((d) cVar).k1();
        }
        l(Integer.MAX_VALUE);
    }
}
