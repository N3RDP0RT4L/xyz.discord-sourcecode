package b.o.a.n.r;

import android.graphics.Rect;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.params.MeteringRectangle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import b.o.a.n.o.c;
import b.o.a.n.o.e;
/* compiled from: BaseReset.java */
@RequiresApi(21)
/* loaded from: classes3.dex */
public abstract class b extends e {
    public boolean e;

    public b(boolean z2) {
        this.e = z2;
    }

    @Override // b.o.a.n.o.e
    public final void j(@NonNull c cVar) {
        this.c = cVar;
        MeteringRectangle meteringRectangle = null;
        if (this.e) {
            meteringRectangle = new MeteringRectangle((Rect) k(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE, new Rect()), 0);
        }
        m(cVar, meteringRectangle);
    }

    public abstract void m(@NonNull c cVar, @Nullable MeteringRectangle meteringRectangle);
}
