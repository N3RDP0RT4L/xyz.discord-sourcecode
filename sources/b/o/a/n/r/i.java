package b.o.a.n.r;

import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.MeteringRectangle;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import b.o.a.b;
import b.o.a.n.d;
import b.o.a.n.o.c;
import java.util.List;
/* compiled from: WhiteBalanceMeter.java */
@RequiresApi(21)
/* loaded from: classes3.dex */
public class i extends a {
    public static final b i = new b(i.class.getSimpleName());

    public i(@NonNull List<MeteringRectangle> list, boolean z2) {
        super(list, z2);
    }

    @Override // b.o.a.n.o.e, b.o.a.n.o.a
    public void b(@NonNull c cVar, @NonNull CaptureRequest captureRequest, @NonNull TotalCaptureResult totalCaptureResult) {
        Integer num = (Integer) totalCaptureResult.get(CaptureResult.CONTROL_AWB_STATE);
        i.a(1, "onCaptureCompleted:", "awbState:", num);
        if (num != null) {
            int intValue = num.intValue();
            if (intValue == 2) {
                this.g = true;
                l(Integer.MAX_VALUE);
            } else if (intValue == 3) {
                this.g = false;
                l(Integer.MAX_VALUE);
            }
        }
    }

    @Override // b.o.a.n.r.a
    public boolean m(@NonNull c cVar) {
        boolean z2 = ((Integer) k(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL, -1)).intValue() != 2;
        Integer num = (Integer) ((d) cVar).f1917i0.get(CaptureRequest.CONTROL_AWB_MODE);
        boolean z3 = z2 && num != null && num.intValue() == 1;
        i.a(1, "checkIsSupported:", Boolean.valueOf(z3));
        return z3;
    }

    @Override // b.o.a.n.r.a
    public boolean n(@NonNull c cVar) {
        TotalCaptureResult totalCaptureResult = ((d) cVar).f1918j0;
        if (totalCaptureResult != null) {
            Integer num = (Integer) totalCaptureResult.get(CaptureResult.CONTROL_AWB_STATE);
            boolean z2 = num != null && num.intValue() == 2;
            i.a(1, "checkShouldSkip:", Boolean.valueOf(z2));
            return z2;
        }
        i.a(1, "checkShouldSkip: false - lastResult is null.");
        return false;
    }

    @Override // b.o.a.n.r.a
    public void o(@NonNull c cVar, @NonNull List<MeteringRectangle> list) {
        i.a(1, "onStarted:", "with areas:", list);
        int intValue = ((Integer) k(CameraCharacteristics.CONTROL_MAX_REGIONS_AWB, 0)).intValue();
        if (!list.isEmpty() && intValue > 0) {
            ((d) cVar).f1917i0.set(CaptureRequest.CONTROL_AWB_REGIONS, (MeteringRectangle[]) list.subList(0, Math.min(intValue, list.size())).toArray(new MeteringRectangle[0]));
            ((d) cVar).k1();
        }
    }
}
