package b.o.a.n.r;

import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.MeteringRectangle;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import b.o.a.b;
import b.o.a.n.d;
import java.util.List;
/* compiled from: ExposureMeter.java */
@RequiresApi(21)
/* loaded from: classes3.dex */
public class c extends a {
    public static final b i = new b(c.class.getSimpleName());
    public boolean j = false;
    public boolean k = false;

    public c(@NonNull List<MeteringRectangle> list, boolean z2) {
        super(list, z2);
    }

    @Override // b.o.a.n.o.e, b.o.a.n.o.a
    public void b(@NonNull b.o.a.n.o.c cVar, @NonNull CaptureRequest captureRequest, @NonNull TotalCaptureResult totalCaptureResult) {
        Integer num = (Integer) totalCaptureResult.get(CaptureResult.CONTROL_AE_STATE);
        Integer num2 = (Integer) totalCaptureResult.get(CaptureResult.CONTROL_AE_PRECAPTURE_TRIGGER);
        i.a(1, "onCaptureCompleted:", "aeState:", num, "aeTriggerState:", num2);
        if (num != null) {
            if (this.f1931b == 0) {
                int intValue = num.intValue();
                if (intValue != 2) {
                    if (intValue == 3) {
                        this.g = false;
                        l(Integer.MAX_VALUE);
                    } else if (intValue != 4) {
                        if (intValue == 5) {
                            l(1);
                        }
                    }
                }
                if (num2 != null && num2.intValue() == 1) {
                    this.g = true;
                    l(Integer.MAX_VALUE);
                }
            }
            if (this.f1931b == 1) {
                int intValue2 = num.intValue();
                if (intValue2 != 2) {
                    if (intValue2 == 3) {
                        this.g = false;
                        l(Integer.MAX_VALUE);
                        return;
                    } else if (intValue2 != 4) {
                        return;
                    }
                }
                this.g = true;
                l(Integer.MAX_VALUE);
            }
        }
    }

    @Override // b.o.a.n.o.e
    public void i(@NonNull b.o.a.n.o.c cVar) {
        ((d) cVar).f1917i0.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER, null);
    }

    @Override // b.o.a.n.r.a
    public boolean m(@NonNull b.o.a.n.o.c cVar) {
        boolean z2 = ((Integer) k(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL, -1)).intValue() == 2;
        Integer num = (Integer) ((d) cVar).f1917i0.get(CaptureRequest.CONTROL_AE_MODE);
        boolean z3 = num != null && (num.intValue() == 1 || num.intValue() == 3 || num.intValue() == 2 || num.intValue() == 4 || num.intValue() == 5);
        this.k = !z2;
        boolean z4 = ((Integer) k(CameraCharacteristics.CONTROL_MAX_REGIONS_AE, 0)).intValue() > 0;
        this.j = z4;
        boolean z5 = z3 && (this.k || z4);
        i.a(1, "checkIsSupported:", Boolean.valueOf(z5), "trigger:", Boolean.valueOf(this.k), "areas:", Boolean.valueOf(this.j));
        return z5;
    }

    @Override // b.o.a.n.r.a
    public boolean n(@NonNull b.o.a.n.o.c cVar) {
        TotalCaptureResult totalCaptureResult = ((d) cVar).f1918j0;
        if (totalCaptureResult != null) {
            Integer num = (Integer) totalCaptureResult.get(CaptureResult.CONTROL_AE_STATE);
            boolean z2 = num != null && num.intValue() == 2;
            i.a(1, "checkShouldSkip:", Boolean.valueOf(z2));
            return z2;
        }
        i.a(1, "checkShouldSkip: false - lastResult is null.");
        return false;
    }

    @Override // b.o.a.n.r.a
    public void o(@NonNull b.o.a.n.o.c cVar, @NonNull List<MeteringRectangle> list) {
        i.a(1, "onStarted:", "with areas:", list);
        if (this.j && !list.isEmpty()) {
            ((d) cVar).f1917i0.set(CaptureRequest.CONTROL_AE_REGIONS, (MeteringRectangle[]) list.subList(0, Math.min(((Integer) k(CameraCharacteristics.CONTROL_MAX_REGIONS_AE, 0)).intValue(), list.size())).toArray(new MeteringRectangle[0]));
        }
        if (this.k) {
            ((d) cVar).f1917i0.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER, 1);
        }
        ((d) cVar).k1();
        if (this.k) {
            l(0);
        } else {
            l(1);
        }
    }
}
