package b.o.a.n.r;

import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.MeteringRectangle;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import b.o.a.b;
import b.o.a.n.o.c;
/* compiled from: ExposureReset.java */
@RequiresApi(21)
/* loaded from: classes3.dex */
public class d extends b {
    public static final b f = new b(d.class.getSimpleName());

    public d() {
        super(true);
    }

    @Override // b.o.a.n.o.e, b.o.a.n.o.a
    public void b(@NonNull c cVar, @NonNull CaptureRequest captureRequest, @NonNull TotalCaptureResult totalCaptureResult) {
        if (this.f1931b == 0) {
            ((b.o.a.n.d) cVar).f1917i0.set(CaptureRequest.CONTROL_AE_LOCK, Boolean.FALSE);
            ((b.o.a.n.d) cVar).k1();
            l(Integer.MAX_VALUE);
        }
    }

    @Override // b.o.a.n.r.b
    public void m(@NonNull c cVar, @Nullable MeteringRectangle meteringRectangle) {
        int intValue = ((Integer) k(CameraCharacteristics.CONTROL_MAX_REGIONS_AE, 0)).intValue();
        if (meteringRectangle != null && intValue > 0) {
            ((b.o.a.n.d) cVar).f1917i0.set(CaptureRequest.CONTROL_AE_REGIONS, new MeteringRectangle[]{meteringRectangle});
        }
        TotalCaptureResult totalCaptureResult = ((b.o.a.n.d) cVar).f1918j0;
        Integer num = totalCaptureResult == null ? null : (Integer) totalCaptureResult.get(CaptureResult.CONTROL_AE_PRECAPTURE_TRIGGER);
        b bVar = f;
        int i = 2;
        bVar.a(1, "onStarted:", "last precapture trigger is", num);
        if (num != null && num.intValue() == 1) {
            bVar.a(1, "onStarted:", "canceling precapture.");
            if (Build.VERSION.SDK_INT < 23) {
                i = 0;
            }
            ((b.o.a.n.d) cVar).f1917i0.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER, Integer.valueOf(i));
        }
        b.o.a.n.d dVar = (b.o.a.n.d) cVar;
        dVar.f1917i0.set(CaptureRequest.CONTROL_AE_LOCK, Boolean.TRUE);
        dVar.k1();
        l(0);
    }
}
