package b.o.a.n.r;

import android.hardware.camera2.params.MeteringRectangle;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import b.o.a.b;
import b.o.a.n.o.c;
import b.o.a.n.o.e;
import java.util.List;
/* compiled from: BaseMeter.java */
@RequiresApi(21)
/* loaded from: classes3.dex */
public abstract class a extends e {
    public static final b e = new b(a.class.getSimpleName());
    public final List<MeteringRectangle> f;
    public boolean g;
    public boolean h;

    public a(@NonNull List<MeteringRectangle> list, boolean z2) {
        this.f = list;
        this.h = z2;
    }

    @Override // b.o.a.n.o.e
    public final void j(@NonNull c cVar) {
        this.c = cVar;
        boolean z2 = this.h && n(cVar);
        if (!m(cVar) || z2) {
            e.a(1, "onStart:", "not supported or skipped. Dispatching COMPLETED state.");
            this.g = true;
            l(Integer.MAX_VALUE);
            return;
        }
        e.a(1, "onStart:", "supported and not skipped. Dispatching onStarted.");
        o(cVar, this.f);
    }

    public abstract boolean m(@NonNull c cVar);

    public abstract boolean n(@NonNull c cVar);

    public abstract void o(@NonNull c cVar, @NonNull List<MeteringRectangle> list);
}
