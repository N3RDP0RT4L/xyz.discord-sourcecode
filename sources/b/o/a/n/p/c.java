package b.o.a.n.p;

import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import b.o.a.b;
import b.o.a.n.d;
/* compiled from: FocusLock.java */
@RequiresApi(21)
/* loaded from: classes3.dex */
public class c extends a {
    public static final b e = new b(c.class.getSimpleName());

    @Override // b.o.a.n.o.e, b.o.a.n.o.a
    public void b(@NonNull b.o.a.n.o.c cVar, @NonNull CaptureRequest captureRequest, @NonNull TotalCaptureResult totalCaptureResult) {
        Integer num = (Integer) totalCaptureResult.get(CaptureResult.CONTROL_AF_STATE);
        Integer num2 = (Integer) totalCaptureResult.get(CaptureResult.CONTROL_AF_MODE);
        e.a(1, "onCapture:", "afState:", num, "afMode:", num2);
        if (num != null && num2 != null && num2.intValue() == 1) {
            int intValue = num.intValue();
            if (intValue == 0 || intValue == 2 || intValue == 4 || intValue == 5 || intValue == 6) {
                l(Integer.MAX_VALUE);
            }
        }
    }

    @Override // b.o.a.n.p.a
    public boolean m(@NonNull b.o.a.n.o.c cVar) {
        for (int i : (int[]) k(CameraCharacteristics.CONTROL_AF_AVAILABLE_MODES, new int[0])) {
            if (i == 1) {
                return true;
            }
        }
        return false;
    }

    @Override // b.o.a.n.p.a
    public boolean n(@NonNull b.o.a.n.o.c cVar) {
        TotalCaptureResult totalCaptureResult = ((d) cVar).f1918j0;
        if (totalCaptureResult != null) {
            Integer num = (Integer) totalCaptureResult.get(CaptureResult.CONTROL_AF_STATE);
            boolean z2 = num != null && (num.intValue() == 4 || num.intValue() == 5 || num.intValue() == 0 || num.intValue() == 2 || num.intValue() == 6);
            Integer num2 = (Integer) totalCaptureResult.get(CaptureResult.CONTROL_AF_MODE);
            boolean z3 = z2 && (num2 != null && num2.intValue() == 1);
            e.a(1, "checkShouldSkip:", Boolean.valueOf(z3));
            return z3;
        }
        e.a(1, "checkShouldSkip: false - lastResult is null.");
        return false;
    }

    @Override // b.o.a.n.p.a
    public void o(@NonNull b.o.a.n.o.c cVar) {
        ((d) cVar).f1917i0.set(CaptureRequest.CONTROL_AF_MODE, 1);
        d dVar = (d) cVar;
        dVar.f1917i0.set(CaptureRequest.CONTROL_AF_TRIGGER, 2);
        dVar.k1();
    }
}
