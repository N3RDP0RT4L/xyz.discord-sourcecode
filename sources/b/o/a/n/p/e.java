package b.o.a.n.p;

import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import b.o.a.b;
import b.o.a.n.d;
import b.o.a.n.o.c;
/* compiled from: WhiteBalanceLock.java */
@RequiresApi(21)
/* loaded from: classes3.dex */
public class e extends a {
    public static final b e = new b(e.class.getSimpleName());

    @Override // b.o.a.n.o.e, b.o.a.n.o.a
    public void b(@NonNull c cVar, @NonNull CaptureRequest captureRequest, @NonNull TotalCaptureResult totalCaptureResult) {
        Integer num = (Integer) totalCaptureResult.get(CaptureResult.CONTROL_AWB_STATE);
        e.a(1, "processCapture:", "awbState:", num);
        if (num != null && num.intValue() == 3) {
            l(Integer.MAX_VALUE);
        }
    }

    @Override // b.o.a.n.p.a
    public boolean m(@NonNull c cVar) {
        boolean z2 = ((Integer) k(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL, -1)).intValue() != 2;
        Integer num = (Integer) ((d) cVar).f1917i0.get(CaptureRequest.CONTROL_AWB_MODE);
        boolean z3 = z2 && num != null && num.intValue() == 1;
        e.a(1, "checkIsSupported:", Boolean.valueOf(z3));
        return z3;
    }

    @Override // b.o.a.n.p.a
    public boolean n(@NonNull c cVar) {
        TotalCaptureResult totalCaptureResult = ((d) cVar).f1918j0;
        if (totalCaptureResult != null) {
            Integer num = (Integer) totalCaptureResult.get(CaptureResult.CONTROL_AWB_STATE);
            boolean z2 = num != null && num.intValue() == 3;
            e.a(1, "checkShouldSkip:", Boolean.valueOf(z2));
            return z2;
        }
        e.a(1, "checkShouldSkip: false - lastResult is null.");
        return false;
    }

    @Override // b.o.a.n.p.a
    public void o(@NonNull c cVar) {
        ((d) cVar).f1917i0.set(CaptureRequest.CONTROL_AWB_LOCK, Boolean.TRUE);
        ((d) cVar).k1();
    }
}
