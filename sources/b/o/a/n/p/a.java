package b.o.a.n.p;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import b.o.a.n.o.c;
import b.o.a.n.o.e;
/* compiled from: BaseLock.java */
@RequiresApi(21)
/* loaded from: classes3.dex */
public abstract class a extends e {
    @Override // b.o.a.n.o.e
    public final void j(@NonNull c cVar) {
        this.c = cVar;
        boolean n = n(cVar);
        if (!m(cVar) || n) {
            l(Integer.MAX_VALUE);
        } else {
            o(cVar);
        }
    }

    public abstract boolean m(@NonNull c cVar);

    public abstract boolean n(@NonNull c cVar);

    public abstract void o(@NonNull c cVar);
}
