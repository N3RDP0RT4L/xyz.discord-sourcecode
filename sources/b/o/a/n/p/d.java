package b.o.a.n.p;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import b.i.a.f.e.o.f;
import b.o.a.n.o.e;
/* compiled from: LockAction.java */
@RequiresApi(21)
/* loaded from: classes3.dex */
public class d extends b.o.a.n.o.d {
    public final e e = f.x1(new b(), new c(), new e());

    @Override // b.o.a.n.o.d
    @NonNull
    public e m() {
        return this.e;
    }
}
