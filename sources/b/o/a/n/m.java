package b.o.a.n;

import b.i.a.f.n.e;
import b.o.a.g;
import com.otaliastudios.cameraview.CameraView;
/* compiled from: CameraEngine.java */
/* loaded from: classes3.dex */
public class m implements e<Void> {
    public final /* synthetic */ i a;

    public m(i iVar) {
        this.a = iVar;
    }

    @Override // b.i.a.f.n.e
    public void onSuccess(Void r6) {
        CameraView.b bVar = (CameraView.b) this.a.m;
        bVar.f3149b.a(1, "dispatchOnCameraClosed");
        CameraView.this.u.post(new g(bVar));
    }
}
