package b.o.a.n.q;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import b.o.a.m.c;
import b.o.a.m.e;
import b.o.a.m.h;
import b.o.a.m.m;
import java.util.HashMap;
import java.util.Map;
/* compiled from: Camera2Mapper.java */
@RequiresApi(21)
/* loaded from: classes3.dex */
public class b {
    public static b a;

    /* renamed from: b  reason: collision with root package name */
    public static final Map<e, Integer> f1933b;
    public static final Map<m, Integer> c;
    public static final Map<h, Integer> d;

    static {
        HashMap hashMap = new HashMap();
        f1933b = hashMap;
        HashMap hashMap2 = new HashMap();
        c = hashMap2;
        HashMap hashMap3 = new HashMap();
        d = hashMap3;
        hashMap.put(e.BACK, 1);
        hashMap.put(e.FRONT, 0);
        hashMap2.put(m.AUTO, 1);
        hashMap2.put(m.CLOUDY, 6);
        hashMap2.put(m.DAYLIGHT, 5);
        hashMap2.put(m.FLUORESCENT, 3);
        hashMap2.put(m.INCANDESCENT, 2);
        hashMap3.put(h.OFF, 0);
        hashMap3.put(h.ON, 18);
    }

    @Nullable
    public final <C extends c, T> C a(@NonNull Map<C, T> map, @NonNull T t) {
        for (C c2 : map.keySet()) {
            if (t.equals(map.get(c2))) {
                return c2;
            }
        }
        return null;
    }
}
