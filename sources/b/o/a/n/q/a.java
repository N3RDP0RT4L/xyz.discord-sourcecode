package b.o.a.n.q;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.o.a.m.c;
import b.o.a.m.e;
import b.o.a.m.f;
import b.o.a.m.h;
import b.o.a.m.m;
import java.util.HashMap;
import java.util.Map;
/* compiled from: Camera1Mapper.java */
/* loaded from: classes3.dex */
public class a {
    public static a a;

    /* renamed from: b  reason: collision with root package name */
    public static final Map<f, String> f1932b;
    public static final Map<m, String> c;
    public static final Map<e, Integer> d;
    public static final Map<h, String> e;

    static {
        HashMap hashMap = new HashMap();
        f1932b = hashMap;
        HashMap hashMap2 = new HashMap();
        c = hashMap2;
        HashMap hashMap3 = new HashMap();
        d = hashMap3;
        HashMap hashMap4 = new HashMap();
        e = hashMap4;
        hashMap.put(f.OFF, "off");
        hashMap.put(f.ON, "on");
        hashMap.put(f.AUTO, "auto");
        hashMap.put(f.TORCH, "torch");
        hashMap3.put(e.BACK, 0);
        hashMap3.put(e.FRONT, 1);
        hashMap2.put(m.AUTO, "auto");
        hashMap2.put(m.INCANDESCENT, "incandescent");
        hashMap2.put(m.FLUORESCENT, "fluorescent");
        hashMap2.put(m.DAYLIGHT, "daylight");
        hashMap2.put(m.CLOUDY, "cloudy-daylight");
        hashMap4.put(h.OFF, "auto");
        hashMap4.put(h.ON, "hdr");
    }

    @NonNull
    public static a a() {
        if (a == null) {
            a = new a();
        }
        return a;
    }

    @Nullable
    public final <C extends c, T> C b(@NonNull Map<C, T> map, @NonNull T t) {
        for (C c2 : map.keySet()) {
            if (t.equals(map.get(c2))) {
                return c2;
            }
        }
        return null;
    }
}
