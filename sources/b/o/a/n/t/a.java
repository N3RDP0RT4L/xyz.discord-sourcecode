package b.o.a.n.t;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import b.o.a.b;
import b.o.a.m.e;
/* compiled from: Angles.java */
/* loaded from: classes3.dex */
public class a {
    public static final b a = new b(a.class.getSimpleName());

    /* renamed from: b  reason: collision with root package name */
    public e f1936b;
    @VisibleForTesting
    public int c = 0;
    @VisibleForTesting
    public int d = 0;
    @VisibleForTesting
    public int e = 0;

    public final int a(@NonNull b bVar, @NonNull b bVar2) {
        if (bVar == bVar2) {
            return 0;
        }
        b bVar3 = b.BASE;
        if (bVar2 == bVar3) {
            return ((360 - a(bVar2, bVar)) + 360) % 360;
        }
        if (bVar != bVar3) {
            return ((a(bVar3, bVar2) - a(bVar3, bVar)) + 360) % 360;
        }
        int ordinal = bVar2.ordinal();
        if (ordinal == 1) {
            return ((360 - this.c) + 360) % 360;
        }
        if (ordinal == 2) {
            return ((360 - this.d) + 360) % 360;
        }
        if (ordinal == 3) {
            return (this.e + 360) % 360;
        }
        throw new RuntimeException("Unknown reference: " + bVar2);
    }

    public boolean b(@NonNull b bVar, @NonNull b bVar2) {
        return c(bVar, bVar2, 1) % 180 != 0;
    }

    public int c(@NonNull b bVar, @NonNull b bVar2, @NonNull int i) {
        int a2 = a(bVar, bVar2);
        return (i == 2 && this.f1936b == e.FRONT) ? ((360 - a2) + 360) % 360 : a2;
    }

    public final void d() {
        a.a(1, "Angles changed:", "sensorOffset:", Integer.valueOf(this.c), "displayOffset:", Integer.valueOf(this.d), "deviceOrientation:", Integer.valueOf(this.e));
    }

    public final void e(int i) {
        if (i != 0 && i != 90 && i != 180 && i != 270) {
            throw new IllegalStateException(b.d.b.a.a.p("This value is not sanitized: ", i));
        }
    }

    public void f(@NonNull e eVar, int i) {
        e(i);
        this.f1936b = eVar;
        this.c = i;
        if (eVar == e.FRONT) {
            this.c = ((360 - i) + 360) % 360;
        }
        d();
    }
}
