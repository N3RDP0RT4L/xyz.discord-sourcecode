package b.o.a.n;

import android.util.Range;
import java.util.Comparator;
/* compiled from: Camera2Engine.java */
/* loaded from: classes3.dex */
public class e implements Comparator<Range<Integer>> {
    public final /* synthetic */ boolean j;

    public e(d dVar, boolean z2) {
        this.j = z2;
    }

    @Override // java.util.Comparator
    public int compare(Range<Integer> range, Range<Integer> range2) {
        Range<Integer> range3 = range;
        Range<Integer> range4 = range2;
        if (this.j) {
            return (range3.getUpper().intValue() - range3.getLower().intValue()) - (range4.getUpper().intValue() - range4.getLower().intValue());
        }
        return (range4.getUpper().intValue() - range4.getLower().intValue()) - (range3.getUpper().intValue() - range3.getLower().intValue());
    }
}
