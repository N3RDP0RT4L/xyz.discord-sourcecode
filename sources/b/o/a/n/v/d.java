package b.o.a.n.v;

import b.i.a.f.n.c;
import com.google.android.gms.tasks.Task;
/* compiled from: CameraOrchestrator.java */
/* loaded from: classes3.dex */
public class d implements Runnable {
    public final /* synthetic */ c j;
    public final /* synthetic */ Task k;

    public d(c cVar, Task task) {
        this.j = cVar;
        this.k = task;
    }

    @Override // java.lang.Runnable
    public void run() {
        this.j.onComplete(this.k);
    }
}
