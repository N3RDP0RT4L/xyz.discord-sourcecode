package b.o.a.n.u;

import android.hardware.Camera;
import android.media.CamcorderProfile;
import androidx.annotation.NonNull;
import b.o.a.c;
import b.o.a.m.e;
import b.o.a.m.f;
import b.o.a.m.h;
import b.o.a.m.j;
import b.o.a.m.m;
import b.o.a.r.a;
import b.o.a.x.b;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
/* compiled from: Camera1Options.java */
/* loaded from: classes3.dex */
public class a extends c {
    public a(@NonNull Camera.Parameters parameters, int i, boolean z2) {
        CamcorderProfile camcorderProfile;
        int i2;
        int i3;
        b.o.a.n.q.a a = b.o.a.n.q.a.a();
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i4 = 0; i4 < numberOfCameras; i4++) {
            Camera.getCameraInfo(i4, cameraInfo);
            e eVar = (e) a.b(b.o.a.n.q.a.d, Integer.valueOf(cameraInfo.facing));
            if (eVar != null) {
                this.f1907b.add(eVar);
            }
        }
        List<String> supportedWhiteBalance = parameters.getSupportedWhiteBalance();
        if (supportedWhiteBalance != null) {
            for (String str : supportedWhiteBalance) {
                m mVar = (m) a.b(b.o.a.n.q.a.c, str);
                if (mVar != null) {
                    this.a.add(mVar);
                }
            }
        }
        this.c.add(f.OFF);
        List<String> supportedFlashModes = parameters.getSupportedFlashModes();
        if (supportedFlashModes != null) {
            for (String str2 : supportedFlashModes) {
                f fVar = (f) a.b(b.o.a.n.q.a.f1932b, str2);
                if (fVar != null) {
                    this.c.add(fVar);
                }
            }
        }
        this.d.add(h.OFF);
        List<String> supportedSceneModes = parameters.getSupportedSceneModes();
        if (supportedSceneModes != null) {
            for (String str3 : supportedSceneModes) {
                h hVar = (h) a.b(b.o.a.n.q.a.e, str3);
                if (hVar != null) {
                    this.d.add(hVar);
                }
            }
        }
        this.k = parameters.isZoomSupported();
        this.o = parameters.getSupportedFocusModes().contains("auto");
        float exposureCompensationStep = parameters.getExposureCompensationStep();
        this.m = parameters.getMinExposureCompensation() * exposureCompensationStep;
        this.n = parameters.getMaxExposureCompensation() * exposureCompensationStep;
        this.l = (parameters.getMinExposureCompensation() == 0 && parameters.getMaxExposureCompensation() == 0) ? false : true;
        for (Camera.Size size : parameters.getSupportedPictureSizes()) {
            int i5 = z2 ? size.height : size.width;
            int i6 = z2 ? size.width : size.height;
            this.e.add(new b(i5, i6));
            this.g.add(b.o.a.x.a.f(i5, i6));
        }
        ArrayList arrayList = new ArrayList(b.o.a.r.a.f1944b.keySet());
        Collections.sort(arrayList, new a.C0175a(Integer.MAX_VALUE * Integer.MAX_VALUE));
        while (true) {
            if (arrayList.size() <= 0) {
                camcorderProfile = CamcorderProfile.get(i, 0);
                break;
            }
            int intValue = b.o.a.r.a.f1944b.get((b) arrayList.remove(0)).intValue();
            if (CamcorderProfile.hasProfile(i, intValue)) {
                camcorderProfile = CamcorderProfile.get(i, intValue);
                break;
            }
        }
        int i7 = camcorderProfile.videoFrameWidth;
        int i8 = camcorderProfile.videoFrameHeight;
        List<Camera.Size> supportedVideoSizes = parameters.getSupportedVideoSizes();
        if (supportedVideoSizes != null) {
            for (Camera.Size size2 : supportedVideoSizes) {
                int i9 = size2.width;
                if (i9 <= i7 && (i3 = size2.height) <= i8) {
                    int i10 = z2 ? i3 : i9;
                    i9 = !z2 ? i3 : i9;
                    this.f.add(new b(i10, i9));
                    this.h.add(b.o.a.x.a.f(i10, i9));
                }
            }
        } else {
            for (Camera.Size size3 : parameters.getSupportedPreviewSizes()) {
                int i11 = size3.width;
                if (i11 <= i7 && (i2 = size3.height) <= i8) {
                    int i12 = z2 ? i2 : i11;
                    i11 = !z2 ? i2 : i11;
                    this.f.add(new b(i12, i11));
                    this.h.add(b.o.a.x.a.f(i12, i11));
                }
            }
        }
        this.p = Float.MAX_VALUE;
        this.q = -3.4028235E38f;
        for (int[] iArr : parameters.getSupportedPreviewFpsRange()) {
            this.p = Math.min(this.p, iArr[0] / 1000.0f);
            this.q = Math.max(this.q, iArr[1] / 1000.0f);
        }
        this.i.add(j.JPEG);
        this.j.add(17);
    }
}
