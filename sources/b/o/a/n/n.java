package b.o.a.n;

import com.google.android.gms.tasks.Task;
import java.util.concurrent.Callable;
/* compiled from: CameraEngine.java */
/* loaded from: classes3.dex */
public class n implements Callable<Task<Void>> {
    public final /* synthetic */ i j;

    public n(i iVar) {
        this.j = iVar;
    }

    @Override // java.util.concurrent.Callable
    public Task<Void> call() throws Exception {
        return this.j.T();
    }
}
