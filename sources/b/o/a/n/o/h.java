package b.o.a.n.o;

import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import java.util.List;
/* compiled from: SequenceAction.java */
@RequiresApi(21)
/* loaded from: classes3.dex */
public class h extends e {
    public final List<e> e;
    public int f = -1;

    /* compiled from: SequenceAction.java */
    /* loaded from: classes3.dex */
    public class a implements b {
        public a() {
        }

        @Override // b.o.a.n.o.b
        public void a(@NonNull b.o.a.n.o.a aVar, int i) {
            if (i == Integer.MAX_VALUE) {
                ((e) aVar).a.remove(this);
                h.this.m();
            }
        }
    }

    public h(@NonNull List<e> list) {
        this.e = list;
        m();
    }

    @Override // b.o.a.n.o.e, b.o.a.n.o.a
    public void b(@NonNull c cVar, @NonNull CaptureRequest captureRequest, @NonNull TotalCaptureResult totalCaptureResult) {
        int i = this.f;
        if (i >= 0) {
            this.e.get(i).b(cVar, captureRequest, totalCaptureResult);
        }
    }

    @Override // b.o.a.n.o.e, b.o.a.n.o.a
    public void c(@NonNull c cVar, @NonNull CaptureRequest captureRequest) {
        if (this.d) {
            j(cVar);
            this.d = false;
        }
        int i = this.f;
        if (i >= 0) {
            this.e.get(i).c(cVar, captureRequest);
        }
    }

    @Override // b.o.a.n.o.e, b.o.a.n.o.a
    public void d(@NonNull c cVar, @NonNull CaptureRequest captureRequest, @NonNull CaptureResult captureResult) {
        int i = this.f;
        if (i >= 0) {
            this.e.get(i).d(cVar, captureRequest, captureResult);
        }
    }

    @Override // b.o.a.n.o.e
    public void h(@NonNull c cVar) {
        int i = this.f;
        if (i >= 0) {
            this.e.get(i).h(cVar);
        }
    }

    @Override // b.o.a.n.o.e
    public void j(@NonNull c cVar) {
        this.c = cVar;
        int i = this.f;
        if (i >= 0) {
            this.e.get(i).j(cVar);
        }
    }

    public final void m() {
        int i = this.f;
        boolean z2 = false;
        boolean z3 = i == -1;
        if (i == this.e.size() - 1) {
            z2 = true;
        }
        if (z2) {
            l(Integer.MAX_VALUE);
            return;
        }
        int i2 = this.f + 1;
        this.f = i2;
        this.e.get(i2).f(new a());
        if (!z3) {
            this.e.get(this.f).j(this.c);
        }
    }
}
