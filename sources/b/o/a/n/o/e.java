package b.o.a.n.o;

import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import b.o.a.n.d;
import java.util.ArrayList;
import java.util.List;
/* compiled from: BaseAction.java */
@RequiresApi(21)
/* loaded from: classes3.dex */
public abstract class e implements a {
    public final List<b> a = new ArrayList();

    /* renamed from: b  reason: collision with root package name */
    public int f1931b;
    public c c;
    public boolean d;

    @Override // b.o.a.n.o.a
    public final void a(@NonNull c cVar) {
        ((d) cVar).p0.remove(this);
        if (!g()) {
            h(cVar);
            l(Integer.MAX_VALUE);
        }
        this.d = false;
    }

    @Override // b.o.a.n.o.a
    public void b(@NonNull c cVar, @NonNull CaptureRequest captureRequest, @NonNull TotalCaptureResult totalCaptureResult) {
    }

    @Override // b.o.a.n.o.a
    @CallSuper
    public void c(@NonNull c cVar, @NonNull CaptureRequest captureRequest) {
        if (this.d) {
            j(cVar);
            this.d = false;
        }
    }

    @Override // b.o.a.n.o.a
    public void d(@NonNull c cVar, @NonNull CaptureRequest captureRequest, @NonNull CaptureResult captureResult) {
    }

    @Override // b.o.a.n.o.a
    public final void e(@NonNull c cVar) {
        this.c = cVar;
        d dVar = (d) cVar;
        if (!dVar.p0.contains(this)) {
            dVar.p0.add(this);
        }
        if (((d) cVar).f1918j0 != null) {
            j(cVar);
        } else {
            this.d = true;
        }
    }

    public void f(@NonNull b bVar) {
        if (!this.a.contains(bVar)) {
            this.a.add(bVar);
            bVar.a(this, this.f1931b);
        }
    }

    public boolean g() {
        return this.f1931b == Integer.MAX_VALUE;
    }

    public void h(@NonNull c cVar) {
    }

    public void i(@NonNull c cVar) {
    }

    @CallSuper
    public void j(@NonNull c cVar) {
        this.c = cVar;
    }

    @NonNull
    public <T> T k(@NonNull CameraCharacteristics.Key<T> key, @NonNull T t) {
        T t2 = (T) ((d) this.c).f1915g0.get(key);
        return t2 == null ? t : t2;
    }

    public final void l(int i) {
        if (i != this.f1931b) {
            this.f1931b = i;
            for (b bVar : this.a) {
                bVar.a(this, this.f1931b);
            }
            if (this.f1931b == Integer.MAX_VALUE) {
                ((d) this.c).p0.remove(this);
                i(this.c);
            }
        }
    }
}
