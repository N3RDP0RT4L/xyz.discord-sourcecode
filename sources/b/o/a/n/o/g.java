package b.o.a.n.o;

import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import b.o.a.b;
import b.o.a.n.i;
/* compiled from: LogAction.java */
@RequiresApi(21)
/* loaded from: classes3.dex */
public class g extends e {
    public static final b e = new b(i.class.getSimpleName());
    public String f;

    @Override // b.o.a.n.o.e, b.o.a.n.o.a
    public void b(@NonNull c cVar, @NonNull CaptureRequest captureRequest, @NonNull TotalCaptureResult totalCaptureResult) {
        String str = "aeMode: " + ((Integer) totalCaptureResult.get(CaptureResult.CONTROL_AE_MODE)) + " aeLock: " + ((Boolean) totalCaptureResult.get(CaptureResult.CONTROL_AE_LOCK)) + " aeState: " + ((Integer) totalCaptureResult.get(CaptureResult.CONTROL_AE_STATE)) + " aeTriggerState: " + ((Integer) totalCaptureResult.get(CaptureResult.CONTROL_AE_PRECAPTURE_TRIGGER)) + " afState: " + ((Integer) totalCaptureResult.get(CaptureResult.CONTROL_AF_STATE)) + " afTriggerState: " + ((Integer) totalCaptureResult.get(CaptureResult.CONTROL_AF_TRIGGER));
        if (!str.equals(this.f)) {
            this.f = str;
            e.a(1, str);
        }
    }

    @Override // b.o.a.n.o.e
    public void i(@NonNull c cVar) {
        l(0);
        e(cVar);
    }
}
