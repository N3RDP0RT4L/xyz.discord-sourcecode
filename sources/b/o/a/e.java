package b.o.a;

import android.hardware.Camera;
import androidx.annotation.NonNull;
import b.o.a.n.q.a;
import java.util.Objects;
/* compiled from: CameraUtils.java */
/* loaded from: classes3.dex */
public class e {
    public static final b a = new b(e.class.getSimpleName());

    public static boolean a(@NonNull b.o.a.m.e eVar) {
        Objects.requireNonNull(a.a());
        int intValue = a.d.get(eVar).intValue();
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.getCameraInfo(i, cameraInfo);
            if (cameraInfo.facing == intValue) {
                return true;
            }
        }
        return false;
    }
}
