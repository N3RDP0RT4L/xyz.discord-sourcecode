package b.o.a.u;

import android.graphics.SurfaceTexture;
import android.view.Surface;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import b.o.a.r.c;
import b.o.a.r.e;
/* compiled from: OverlayDrawer.java */
/* loaded from: classes3.dex */
public class b {
    public static final b.o.a.b a = new b.o.a.b(b.class.getSimpleName());

    /* renamed from: b  reason: collision with root package name */
    public a f1949b;
    public SurfaceTexture c;
    public Surface d;
    public final Object g = new Object();
    @VisibleForTesting
    public c e = new c(new b.o.b.f.b(33984, 36197, null, 4));
    public e f = new e(this.e.a.g);

    public b(@NonNull a aVar, @NonNull b.o.a.x.b bVar) {
        this.f1949b = aVar;
        SurfaceTexture surfaceTexture = new SurfaceTexture(this.e.a.g);
        this.c = surfaceTexture;
        surfaceTexture.setDefaultBufferSize(bVar.j, bVar.k);
        this.d = new Surface(this.c);
    }
}
