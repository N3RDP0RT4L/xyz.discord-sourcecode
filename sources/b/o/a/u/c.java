package b.o.a.u;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import b.o.a.b;
import b.o.a.u.a;
import com.otaliastudios.cameraview.R;
/* compiled from: OverlayLayout.java */
@SuppressLint({"CustomViewStyleable"})
/* loaded from: classes3.dex */
public class c extends FrameLayout implements b.o.a.u.a {
    public static final String j;
    public static final b k;
    @VisibleForTesting
    public a.EnumC0176a l = a.EnumC0176a.PREVIEW;
    public boolean m;

    /* compiled from: OverlayLayout.java */
    /* loaded from: classes3.dex */
    public static class a extends FrameLayout.LayoutParams {
        public boolean a;

        /* renamed from: b  reason: collision with root package name */
        public boolean f1950b;
        public boolean c;

        public a(@NonNull Context context, @NonNull AttributeSet attributeSet) {
            super(context, attributeSet);
            this.a = false;
            this.f1950b = false;
            this.c = false;
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.c.CameraView_Layout);
            try {
                this.a = obtainStyledAttributes.getBoolean(R.c.CameraView_Layout_layout_drawOnPreview, false);
                this.f1950b = obtainStyledAttributes.getBoolean(R.c.CameraView_Layout_layout_drawOnPictureSnapshot, false);
                this.c = obtainStyledAttributes.getBoolean(R.c.CameraView_Layout_layout_drawOnVideoSnapshot, false);
            } finally {
                obtainStyledAttributes.recycle();
            }
        }

        @VisibleForTesting
        public boolean a(@NonNull a.EnumC0176a aVar) {
            return (aVar == a.EnumC0176a.PREVIEW && this.a) || (aVar == a.EnumC0176a.VIDEO_SNAPSHOT && this.c) || (aVar == a.EnumC0176a.PICTURE_SNAPSHOT && this.f1950b);
        }

        @NonNull
        public String toString() {
            StringBuilder sb = new StringBuilder();
            b.d.b.a.a.i0(a.class, sb, "[drawOnPreview:");
            sb.append(this.a);
            sb.append(",drawOnPictureSnapshot:");
            sb.append(this.f1950b);
            sb.append(",drawOnVideoSnapshot:");
            return b.d.b.a.a.M(sb, this.c, "]");
        }
    }

    static {
        String simpleName = c.class.getSimpleName();
        j = simpleName;
        k = new b(simpleName);
    }

    public c(@NonNull Context context) {
        super(context);
        setWillNotDraw(false);
    }

    public void a(@NonNull a.EnumC0176a aVar, @NonNull Canvas canvas) {
        synchronized (this) {
            this.l = aVar;
            int ordinal = aVar.ordinal();
            if (ordinal == 0) {
                super.draw(canvas);
            } else if (ordinal == 1 || ordinal == 2) {
                canvas.save();
                float width = canvas.getWidth() / getWidth();
                float height = canvas.getHeight() / getHeight();
                b bVar = k;
                bVar.a(0, "draw", "target:", aVar, "canvas:", canvas.getWidth() + "x" + canvas.getHeight(), "view:", getWidth() + "x" + getHeight(), "widthScale:", Float.valueOf(width), "heightScale:", Float.valueOf(height), "hardwareCanvasMode:", Boolean.valueOf(this.m));
                canvas.scale(width, height);
                dispatchDraw(canvas);
                canvas.restore();
            }
        }
    }

    public boolean b(@NonNull a.EnumC0176a aVar) {
        for (int i = 0; i < getChildCount(); i++) {
            if (((a) getChildAt(i).getLayoutParams()).a(aVar)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: c */
    public a generateLayoutParams(AttributeSet attributeSet) {
        return new a(getContext(), attributeSet);
    }

    @Override // android.view.View
    @SuppressLint({"MissingSuperCall"})
    public void draw(Canvas canvas) {
        k.a(1, "normal draw called.");
        a.EnumC0176a aVar = a.EnumC0176a.PREVIEW;
        if (b(aVar)) {
            a(aVar, canvas);
        }
    }

    @Override // android.view.ViewGroup
    public boolean drawChild(Canvas canvas, View view, long j2) {
        a aVar = (a) view.getLayoutParams();
        if (aVar.a(this.l)) {
            k.a(0, "Performing drawing for view:", view.getClass().getSimpleName(), "target:", this.l, "params:", aVar);
            return super.drawChild(canvas, view, j2);
        }
        k.a(0, "Skipping drawing for view:", view.getClass().getSimpleName(), "target:", this.l, "params:", aVar);
        return false;
    }

    public boolean getHardwareCanvasEnabled() {
        return this.m;
    }

    public void setHardwareCanvasEnabled(boolean z2) {
        this.m = z2;
    }
}
