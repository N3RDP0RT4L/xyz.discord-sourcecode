package b.a.p;

import andhook.lib.xposed.callbacks.XCallback;
import android.content.Context;
import b.c.a.a0.d;
import b.i.a.c.e1;
import b.i.a.c.e3.o;
import b.i.a.c.e3.q;
import b.i.a.c.k2;
import b.i.a.c.z0;
import b.i.b.a.l;
import com.discord.app.AppLog;
import com.discord.player.AppMediaPlayer;
import com.discord.utilities.guilds.GuildConstantsKt;
import d0.z.d.m;
import j0.p.a;
import rx.Scheduler;
/* compiled from: AppMediaPlayerFactory.kt */
/* loaded from: classes.dex */
public final class i {
    public static final AppMediaPlayer a(Context context) {
        m.checkNotNullParameter(context, "context");
        AppLog appLog = AppLog.g;
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(appLog, "logger");
        j jVar = new j(new q.a(context), 104857600);
        k kVar = new k();
        d.D(true);
        z0.j(GuildConstantsKt.MAX_GUILD_MEMBERS_NOTIFY_ALL_MESSAGES, 0, "bufferForPlaybackMs", "0");
        z0.j(5000, 0, "bufferForPlaybackAfterRebufferMs", "0");
        z0.j(XCallback.PRIORITY_HIGHEST, GuildConstantsKt.MAX_GUILD_MEMBERS_NOTIFY_ALL_MESSAGES, "minBufferMs", "bufferForPlaybackMs");
        z0.j(XCallback.PRIORITY_HIGHEST, 5000, "minBufferMs", "bufferForPlaybackAfterRebufferMs");
        z0.j(30000, XCallback.PRIORITY_HIGHEST, "maxBufferMs", "minBufferMs");
        d.D(true);
        final z0 z0Var = new z0(new o(true, 65536), XCallback.PRIORITY_HIGHEST, 30000, GuildConstantsKt.MAX_GUILD_MEMBERS_NOTIFY_ALL_MESSAGES, 5000, -1, false, 0, false);
        m.checkNotNullExpressionValue(z0Var, "DefaultLoadControl.Build…     )\n          .build()");
        e1.b bVar = new e1.b(context);
        d.D(!bVar.f920s);
        bVar.f = new l() { // from class: b.i.a.c.d
            @Override // b.i.b.a.l
            public final Object get() {
                return n1.this;
            }
        };
        d.D(!bVar.f920s);
        bVar.f920s = true;
        k2 k2Var = new k2(bVar);
        m.checkNotNullExpressionValue(k2Var, "ExoPlayer\n          .Bui…ntrol)\n          .build()");
        k2Var.x(kVar);
        Scheduler a = a.a();
        m.checkNotNullExpressionValue(a, "Schedulers.computation()");
        return new AppMediaPlayer(k2Var, kVar, jVar, a, appLog);
    }
}
