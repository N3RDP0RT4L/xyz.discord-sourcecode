package b.a.p;

import b.a.p.k;
import b.i.a.c.k2;
import com.discord.player.AppMediaPlayer;
import j0.j.b.a;
import java.util.concurrent.TimeUnit;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import rx.subjects.PublishSubject;
/* compiled from: AppMediaPlayer.kt */
/* loaded from: classes.dex */
public final class e<T> implements Action1<k.c> {
    public final /* synthetic */ AppMediaPlayer j;

    public e(AppMediaPlayer appMediaPlayer) {
        this.j = appMediaPlayer;
    }

    @Override // rx.functions.Action1
    public void call(k.c cVar) {
        int i = cVar.a;
        if (i == 2) {
            PublishSubject<AppMediaPlayer.Event> publishSubject = this.j.a;
            publishSubject.k.onNext(AppMediaPlayer.Event.b.a);
        } else if (i == 3) {
            PublishSubject<AppMediaPlayer.Event> publishSubject2 = this.j.a;
            publishSubject2.k.onNext(AppMediaPlayer.Event.a.a);
            AppMediaPlayer appMediaPlayer = this.j;
            Subscription subscription = appMediaPlayer.f2731b;
            if (subscription != null) {
                subscription.unsubscribe();
            }
            appMediaPlayer.f2731b = Observable.E(500L, 500L, TimeUnit.MILLISECONDS, appMediaPlayer.i).K().I(a.a()).W(new g(appMediaPlayer), new h(appMediaPlayer));
        } else if (i == 4) {
            PublishSubject<AppMediaPlayer.Event> publishSubject3 = this.j.a;
            publishSubject3.k.onNext(AppMediaPlayer.Event.e.a);
            Subscription subscription2 = this.j.f2731b;
            if (subscription2 != null) {
                subscription2.unsubscribe();
            }
            AppMediaPlayer appMediaPlayer2 = this.j;
            PublishSubject<AppMediaPlayer.Event> publishSubject4 = appMediaPlayer2.a;
            publishSubject4.k.onNext(new AppMediaPlayer.Event.c(((k2) appMediaPlayer2.f).T()));
        }
    }
}
