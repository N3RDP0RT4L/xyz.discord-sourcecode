package b.a.k.g;

import b.d.b.a.a;
/* compiled from: ParseState.kt */
/* loaded from: classes.dex */
public final class c {
    public final boolean a;

    /* renamed from: b  reason: collision with root package name */
    public int f242b;

    public c() {
        this(false, 0, 3);
    }

    public c(boolean z2, int i) {
        this.a = z2;
        this.f242b = i;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof c)) {
            return false;
        }
        c cVar = (c) obj;
        return this.a == cVar.a && this.f242b == cVar.f242b;
    }

    public int hashCode() {
        boolean z2 = this.a;
        if (z2) {
            z2 = true;
        }
        int i = z2 ? 1 : 0;
        int i2 = z2 ? 1 : 0;
        return (i * 31) + this.f242b;
    }

    public String toString() {
        StringBuilder R = a.R("ParseState(isEscaped=");
        R.append(this.a);
        R.append(", argumentIndex=");
        return a.A(R, this.f242b, ")");
    }

    public c(boolean z2, int i, int i2) {
        z2 = (i2 & 1) != 0 ? false : z2;
        i = (i2 & 2) != 0 ? 0 : i;
        this.a = z2;
        this.f242b = i;
    }
}
