package b.a.k;

import android.content.Context;
import android.content.res.Resources;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import com.discord.i18n.RenderContext;
import com.discord.simpleast.core.parser.Parser;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.text.MatchResult;
import kotlin.text.Regex;
/* compiled from: FormatUtils.kt */
/* loaded from: classes.dex */
public final class b {
    public static final Regex a = new Regex("\\{(\\S+?)\\}");

    /* compiled from: FormatUtils.kt */
    /* loaded from: classes.dex */
    public static final class a extends o implements Function1<RenderContext, Unit> {
        public static final a j = new a();

        public a() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(RenderContext renderContext) {
            m.checkNotNullParameter(renderContext, "$receiver");
            return Unit.a;
        }
    }

    /* compiled from: FormatUtils.kt */
    /* renamed from: b.a.k.b$b  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0034b extends o implements Function1<RenderContext, Unit> {
        public static final C0034b j = new C0034b();

        public C0034b() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(RenderContext renderContext) {
            m.checkNotNullParameter(renderContext, "$receiver");
            return Unit.a;
        }
    }

    /* compiled from: FormatUtils.kt */
    /* loaded from: classes.dex */
    public static final class c extends o implements Function1<RenderContext, Unit> {
        public static final c j = new c();

        public c() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(RenderContext renderContext) {
            m.checkNotNullParameter(renderContext, "$receiver");
            return Unit.a;
        }
    }

    /* compiled from: FormatUtils.kt */
    /* loaded from: classes.dex */
    public static final class d extends o implements Function1<RenderContext, Unit> {
        public static final d j = new d();

        public d() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(RenderContext renderContext) {
            m.checkNotNullParameter(renderContext, "$receiver");
            return Unit.a;
        }
    }

    /* compiled from: FormatUtils.kt */
    /* loaded from: classes.dex */
    public static final class e extends o implements Function1<RenderContext, Unit> {
        public static final e j = new e();

        public e() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(RenderContext renderContext) {
            m.checkNotNullParameter(renderContext, "$receiver");
            return Unit.a;
        }
    }

    /* compiled from: FormatUtils.kt */
    /* loaded from: classes.dex */
    public static final class f extends o implements Function1<MatchResult, CharSequence> {
        public final /* synthetic */ Map $namedArgs;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public f(Map map) {
            super(1);
            this.$namedArgs = map;
        }

        @Override // kotlin.jvm.functions.Function1
        public CharSequence invoke(MatchResult matchResult) {
            MatchResult matchResult2 = matchResult;
            m.checkNotNullParameter(matchResult2, "matchResult");
            String str = (String) this.$namedArgs.get(matchResult2.getGroupValues().get(1));
            return str != null ? str : matchResult2.getValue();
        }
    }

    /* compiled from: FormatUtils.kt */
    /* loaded from: classes.dex */
    public static final class g extends o implements Function1<RenderContext, Unit> {
        public static final g j = new g();

        public g() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(RenderContext renderContext) {
            m.checkNotNullParameter(renderContext, "$receiver");
            return Unit.a;
        }
    }

    /* compiled from: FormatUtils.kt */
    /* loaded from: classes.dex */
    public static final class h extends o implements Function1<RenderContext, Unit> {
        public static final h j = new h();

        public h() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(RenderContext renderContext) {
            m.checkNotNullParameter(renderContext, "$receiver");
            return Unit.a;
        }
    }

    public static final void a(TextView textView, CharSequence charSequence) {
        m.checkNotNullParameter(textView, "$this$bindText");
        textView.setText(charSequence);
        int i = 0;
        if (!(!(charSequence == null || charSequence.length() == 0))) {
            i = 8;
        }
        textView.setVisibility(i);
    }

    public static final CharSequence b(Context context, @StringRes int i, Object[] objArr, Function1<? super RenderContext, Unit> function1) {
        m.checkNotNullParameter(context, "$this$i18nFormat");
        m.checkNotNullParameter(objArr, "formatArgs");
        m.checkNotNullParameter(function1, "initializer");
        Resources resources = context.getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        return c(resources, i, Arrays.copyOf(objArr, objArr.length), function1);
    }

    public static final CharSequence c(Resources resources, @StringRes int i, Object[] objArr, Function1<? super RenderContext, Unit> function1) {
        m.checkNotNullParameter(resources, "$this$i18nFormat");
        m.checkNotNullParameter(objArr, "formatArgs");
        m.checkNotNullParameter(function1, "initializer");
        String string = resources.getString(i);
        m.checkNotNullExpressionValue(string, "getString(stringResId)");
        return g(string, Arrays.copyOf(objArr, objArr.length), function1);
    }

    public static final CharSequence d(View view, @StringRes int i, Object[] objArr, Function1<? super RenderContext, Unit> function1) {
        m.checkNotNullParameter(view, "$this$i18nFormat");
        m.checkNotNullParameter(objArr, "formatArgs");
        m.checkNotNullParameter(function1, "initializer");
        Context context = view.getContext();
        m.checkNotNullExpressionValue(context, "context");
        return b(context, i, Arrays.copyOf(objArr, objArr.length), function1);
    }

    public static final CharSequence e(Fragment fragment, @StringRes int i, Object[] objArr, Function1<? super RenderContext, Unit> function1) {
        m.checkNotNullParameter(fragment, "$this$i18nFormat");
        m.checkNotNullParameter(objArr, "formatArgs");
        m.checkNotNullParameter(function1, "initializer");
        Context requireContext = fragment.requireContext();
        m.checkNotNullExpressionValue(requireContext, "requireContext()");
        return b(requireContext, i, Arrays.copyOf(objArr, objArr.length), function1);
    }

    public static final CharSequence f(CharSequence charSequence, Object[] objArr, RenderContext renderContext) {
        m.checkNotNullParameter(charSequence, "$this$i18nFormat");
        m.checkNotNullParameter(objArr, "formatArgs");
        m.checkNotNullParameter(renderContext, "renderContext");
        Map<String, String> map = renderContext.a;
        List<? extends Object> list = renderContext.c;
        boolean z2 = true;
        if (!map.isEmpty()) {
            charSequence = a.replace(charSequence, new f(map));
        } else {
            if (list != null && !list.isEmpty()) {
                z2 = false;
            }
            if (!z2) {
                String replace = a.replace(charSequence, "%s");
                Object[] copyOf = Arrays.copyOf(objArr, objArr.length);
                charSequence = b.d.b.a.a.N(copyOf, copyOf.length, replace, "java.lang.String.format(this, *args)");
            }
        }
        b.a.k.a aVar = b.a.k.a.d;
        b.a.k.g.b bVar = b.a.k.a.a;
        if (bVar == null) {
            m.throwUninitializedPropertyAccessException("formattingParserProvider");
        }
        b.a.k.g.a a2 = bVar.a();
        if (renderContext.f) {
            String obj = charSequence.toString();
            Locale locale = Locale.ROOT;
            m.checkNotNullExpressionValue(locale, "Locale.ROOT");
            Objects.requireNonNull(obj, "null cannot be cast to non-null type java.lang.String");
            charSequence = obj.toUpperCase(locale);
            m.checkNotNullExpressionValue(charSequence, "(this as java.lang.String).toUpperCase(locale)");
        }
        String str = charSequence;
        Objects.requireNonNull(a2);
        m.checkNotNullParameter(str, "input");
        m.checkNotNullParameter(renderContext, "renderContext");
        List parse$default = Parser.parse$default(a2, str, new b.a.k.g.c(false, 0, 3), null, 4, null);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        b.a.t.b.b.h.a(spannableStringBuilder, parse$default, renderContext);
        return spannableStringBuilder;
    }

    public static final CharSequence g(CharSequence charSequence, Object[] objArr, Function1<? super RenderContext, Unit> function1) {
        m.checkNotNullParameter(charSequence, "$this$i18nFormat");
        m.checkNotNullParameter(objArr, "formatArgs");
        m.checkNotNullParameter(function1, "initializer");
        return f(charSequence, objArr, b.c.a.a0.d.R1(function1, Arrays.copyOf(objArr, objArr.length)));
    }

    public static final void m(TextView textView, @StringRes int i, Object[] objArr, Function1<? super RenderContext, Unit> function1) {
        m.checkNotNullParameter(textView, "$this$i18nSetText");
        m.checkNotNullParameter(objArr, "formatArgs");
        m.checkNotNullParameter(function1, "initializer");
        RenderContext R1 = b.c.a.a0.d.R1(function1, Arrays.copyOf(objArr, objArr.length));
        textView.setMovementMethod(R1.g ? LinkMovementMethod.getInstance() : null);
        String string = textView.getContext().getString(i);
        m.checkNotNullExpressionValue(string, "context.getString(stringResId)");
        textView.setText(f(string, objArr, R1));
    }

    public static void o(TextView textView, CharSequence charSequence, Object[] objArr, Function1 function1, int i) {
        int i2 = i & 4;
        MovementMethod movementMethod = null;
        h hVar = i2 != 0 ? h.j : null;
        m.checkNotNullParameter(textView, "$this$i18nSetText");
        m.checkNotNullParameter(objArr, "formatArgs");
        m.checkNotNullParameter(hVar, "initializer");
        if (charSequence == null) {
            textView.setText((CharSequence) null);
            textView.setMovementMethod(null);
            return;
        }
        RenderContext R1 = b.c.a.a0.d.R1(hVar, Arrays.copyOf(objArr, objArr.length));
        if (R1.g) {
            movementMethod = LinkMovementMethod.getInstance();
        }
        textView.setMovementMethod(movementMethod);
        textView.setText(f(charSequence, objArr, R1));
    }
}
