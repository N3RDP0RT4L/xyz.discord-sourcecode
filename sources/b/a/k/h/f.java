package b.a.k.h;

import b.a.k.f.c;
import com.discord.i18n.RenderContext;
import com.discord.simpleast.core.parser.ParseSpec;
import com.discord.simpleast.core.parser.Parser;
import com.discord.simpleast.core.parser.Rule;
import d0.z.d.m;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/* compiled from: HookRule.kt */
/* loaded from: classes.dex */
public final class f extends Rule<RenderContext, c, S> {
    public f(Pattern pattern) {
        super(pattern);
    }

    @Override // com.discord.simpleast.core.parser.Rule
    public ParseSpec<RenderContext, S> parse(Matcher matcher, Parser<RenderContext, ? super c, S> parser, S s2) {
        m.checkNotNullParameter(matcher, "matcher");
        m.checkNotNullParameter(parser, "parser");
        String group = matcher.group(2);
        m.checkNotNull(group);
        if (matcher.start(1) == matcher.end(1)) {
            c cVar = new c(group);
            m.checkNotNullParameter(cVar, "node");
            return new ParseSpec<>(cVar, s2);
        }
        c cVar2 = new c(group);
        int start = matcher.start(1);
        int end = matcher.end(1);
        m.checkNotNullParameter(cVar2, "node");
        return new ParseSpec<>(cVar2, s2, start, end);
    }
}
