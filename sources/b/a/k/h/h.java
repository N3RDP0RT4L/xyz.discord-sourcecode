package b.a.k.h;

import android.text.style.StyleSpan;
import b.a.k.g.c;
import com.discord.simpleast.core.node.Node;
import com.discord.simpleast.core.node.StyleNode;
import com.discord.simpleast.core.parser.ParseSpec;
import com.discord.simpleast.core.parser.Parser;
import com.discord.simpleast.core.parser.Rule;
import d0.z.d.m;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/* compiled from: ItalicsRule.kt */
/* loaded from: classes.dex */
public final class h extends Rule<T, Node<T>, c> {
    public h(Pattern pattern) {
        super(pattern);
    }

    @Override // com.discord.simpleast.core.parser.Rule
    public ParseSpec parse(Matcher matcher, Parser parser, c cVar) {
        Node node;
        int i;
        int i2;
        c cVar2 = cVar;
        m.checkNotNullParameter(matcher, "matcher");
        m.checkNotNullParameter(parser, "parser");
        m.checkNotNullParameter(cVar2, "state");
        boolean z2 = false;
        if (cVar2.a) {
            node = new Node.a(new Node[0]);
        } else {
            node = new StyleNode(d0.t.m.listOf(new StyleSpan(2)));
        }
        String group = matcher.group(2);
        if (group != null) {
            if (group.length() > 0) {
                z2 = true;
            }
            if (z2) {
                i2 = matcher.start(2);
                i = matcher.end(2);
                m.checkNotNullParameter(node, "node");
                return new ParseSpec(node, cVar2, i2, i);
            }
        }
        i2 = matcher.start(1);
        i = matcher.end(1);
        m.checkNotNullParameter(node, "node");
        return new ParseSpec(node, cVar2, i2, i);
    }
}
