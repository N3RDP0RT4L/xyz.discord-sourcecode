package b.a.k.h;

import b.a.k.g.c;
import com.discord.simpleast.core.node.Node;
import com.discord.simpleast.core.parser.ParseSpec;
import com.discord.simpleast.core.parser.Parser;
import com.discord.simpleast.core.parser.Rule;
import d0.z.d.m;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/* compiled from: BangEscapeRule.kt */
/* loaded from: classes.dex */
public final class a extends Rule<T, Node<T>, c> {
    public a(Pattern pattern) {
        super(pattern);
    }

    @Override // com.discord.simpleast.core.parser.Rule
    public ParseSpec parse(Matcher matcher, Parser parser, c cVar) {
        c cVar2 = cVar;
        m.checkNotNullParameter(matcher, "matcher");
        m.checkNotNullParameter(parser, "parser");
        m.checkNotNullParameter(cVar2, "state");
        c cVar3 = new c(true, cVar2.f242b);
        Node.a aVar = new Node.a(new Node[0]);
        int start = matcher.start(1);
        int end = matcher.end(1);
        m.checkNotNullParameter(aVar, "node");
        return new ParseSpec(aVar, cVar3, start, end);
    }
}
