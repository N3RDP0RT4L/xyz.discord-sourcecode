package b.a.k.h;

import b.a.k.f.d;
import b.a.k.g.c;
import com.discord.i18n.RenderContext;
import com.discord.simpleast.core.node.Node;
import com.discord.simpleast.core.parser.ParseSpec;
import com.discord.simpleast.core.parser.Parser;
import com.discord.simpleast.core.parser.Rule;
import d0.z.d.m;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/* compiled from: StrikethroughRule.kt */
/* loaded from: classes.dex */
public final class i extends Rule<RenderContext, Node<RenderContext>, c> {
    public i(Pattern pattern) {
        super(pattern);
    }

    @Override // com.discord.simpleast.core.parser.Rule
    public ParseSpec<RenderContext, c> parse(Matcher matcher, Parser<RenderContext, ? super Node<RenderContext>, c> parser, c cVar) {
        Node node;
        c cVar2 = cVar;
        m.checkNotNullParameter(matcher, "matcher");
        m.checkNotNullParameter(parser, "parser");
        m.checkNotNullParameter(cVar2, "state");
        if (cVar2.a) {
            node = new Node.a(new Node[0]);
        } else {
            node = new d();
        }
        int start = matcher.start(1);
        int end = matcher.end(1);
        m.checkNotNullParameter(node, "node");
        return new ParseSpec<>(node, cVar2, start, end);
    }
}
