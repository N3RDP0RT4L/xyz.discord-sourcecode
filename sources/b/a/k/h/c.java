package b.a.k.h;

import b.a.k.f.a;
import com.discord.i18n.RenderContext;
import com.discord.simpleast.core.node.Node;
import com.discord.simpleast.core.parser.ParseSpec;
import com.discord.simpleast.core.parser.Parser;
import com.discord.simpleast.core.parser.Rule;
import d0.z.d.m;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/* compiled from: BoldRule.kt */
/* loaded from: classes.dex */
public final class c extends Rule<RenderContext, Node<RenderContext>, b.a.k.g.c> {
    public c(Pattern pattern) {
        super(pattern);
    }

    @Override // com.discord.simpleast.core.parser.Rule
    public ParseSpec<RenderContext, b.a.k.g.c> parse(Matcher matcher, Parser<RenderContext, ? super Node<RenderContext>, b.a.k.g.c> parser, b.a.k.g.c cVar) {
        Node node;
        b.a.k.g.c cVar2 = cVar;
        m.checkNotNullParameter(matcher, "matcher");
        m.checkNotNullParameter(parser, "parser");
        m.checkNotNullParameter(cVar2, "state");
        if (cVar2.a) {
            node = new Node.a(new Node[0]);
        } else {
            node = new a();
        }
        int start = matcher.start(1);
        int end = matcher.end(1);
        m.checkNotNullParameter(node, "node");
        return new ParseSpec<>(node, cVar2, start, end);
    }
}
