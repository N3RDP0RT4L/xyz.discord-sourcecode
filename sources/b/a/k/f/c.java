package b.a.k.f;

import android.text.SpannableStringBuilder;
import android.webkit.URLUtil;
import b.a.k.a;
import com.discord.i18n.Hook;
import com.discord.i18n.RenderContext;
import com.discord.simpleast.core.node.Node;
import d0.z.d.m;
import java.util.List;
/* compiled from: HookNode.kt */
/* loaded from: classes.dex */
public final class c extends Node.a<RenderContext> {
    public final String a;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public c(String str) {
        super(new Node[0]);
        m.checkNotNullParameter(str, "key");
        this.a = str;
    }

    @Override // com.discord.simpleast.core.node.Node.a, com.discord.simpleast.core.node.Node
    public void render(SpannableStringBuilder spannableStringBuilder, Object obj) {
        List<Object> list;
        RenderContext renderContext = (RenderContext) obj;
        m.checkNotNullParameter(spannableStringBuilder, "builder");
        m.checkNotNullParameter(renderContext, "renderContext");
        Hook hook = renderContext.f2683b.get(this.a);
        int length = spannableStringBuilder.length();
        super.render(spannableStringBuilder, renderContext);
        Hook.a aVar = null;
        CharSequence charSequence = hook != null ? hook.f2681b : null;
        if (charSequence != null) {
            spannableStringBuilder.replace(length, spannableStringBuilder.length(), charSequence);
        }
        if (!(hook == null || (list = hook.a) == null)) {
            for (Object obj2 : list) {
                spannableStringBuilder.setSpan(obj2, length, spannableStringBuilder.length(), 33);
            }
        }
        Hook.a aVar2 = hook != null ? hook.c : null;
        if (aVar2 != null) {
            aVar = aVar2;
        } else if (URLUtil.isValidUrl(this.a)) {
            a aVar3 = a.d;
            aVar = new Hook.a(a.f241b.invoke(), a.c);
        }
        if (aVar != null) {
            Integer num = aVar.a;
            if (num == null) {
                a aVar4 = a.d;
                num = a.f241b.invoke();
            }
            spannableStringBuilder.setSpan(new b.a.k.e.a(new b(this, aVar), num), length, spannableStringBuilder.length(), 33);
            renderContext.g = true;
        }
    }
}
