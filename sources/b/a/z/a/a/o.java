package b.a.z.a.a;

import android.text.TextUtils;
import com.discord.widgets.channels.permissions.WidgetChannelSettingsPermissionsAddMember;
import j0.k.b;
/* compiled from: lambda */
/* loaded from: classes2.dex */
public final /* synthetic */ class o implements b {
    public static final /* synthetic */ o j = new o();

    @Override // j0.k.b
    public final Object call(Object obj) {
        int i = WidgetChannelSettingsPermissionsAddMember.j;
        return Boolean.valueOf(!TextUtils.isEmpty((String) obj));
    }
}
