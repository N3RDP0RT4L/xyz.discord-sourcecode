package b.a.t.a;

import b.a.t.a.o;
import com.discord.simpleast.core.node.Node;
import com.discord.simpleast.core.parser.ParseSpec;
import com.discord.simpleast.core.parser.Parser;
import com.discord.simpleast.core.parser.Rule;
import d0.z.d.m;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/* compiled from: Kotlin.kt */
/* loaded from: classes.dex */
public final class p extends Rule<RC, Node<RC>, S> {
    public final /* synthetic */ f a;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public p(o.b.a aVar, f fVar, Pattern pattern) {
        super(pattern);
        this.a = fVar;
    }

    @Override // com.discord.simpleast.core.parser.Rule
    public ParseSpec<RC, S> parse(Matcher matcher, Parser<RC, ? super Node<RC>, S> parser, S s2) {
        m.checkNotNullParameter(matcher, "matcher");
        m.checkNotNullParameter(parser, "parser");
        String group = matcher.group(1);
        String group2 = matcher.group(2);
        String group3 = matcher.group(3);
        String group4 = matcher.group(4);
        m.checkNotNull(group);
        m.checkNotNull(group3);
        m.checkNotNull(group4);
        o.b bVar = new o.b(group, group2, group3, group4, this.a);
        m.checkNotNullParameter(bVar, "node");
        return new ParseSpec<>(bVar, s2);
    }
}
