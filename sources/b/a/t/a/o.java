package b.a.t.a;

import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.simpleast.core.node.Node;
import com.discord.simpleast.core.node.StyleNode;
import d0.g0.i;
import d0.z.d.m;
import java.util.regex.Pattern;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.text.Regex;
/* compiled from: Kotlin.kt */
/* loaded from: classes.dex */
public final class o {
    public static final String[] a = {"public|private|internal|inline|lateinit|abstract|open|reified", "import|package", "class|interface|data|enum|sealed|object|typealias", "fun|override|this|super|where|constructor|init|param|delegate", "const|val|var|get|final|vararg|it", "return|break|continue|suspend", "for|while|do|if|else|when|try|catch|finally|throw", "in|out|is|as|typeof", "shr|ushr|shl|ushl", "true|false|null"};

    /* renamed from: b  reason: collision with root package name */
    public static final String[] f294b = {"true|false|Boolean|String|Char", "Int|UInt|Long|ULong|Float|Double|Byte|UByte|Short|UShort", "Self|Set|Map|MutableMap|List|MutableList|Array|Runnable|Unit", "arrayOf|listOf|mapOf|setOf|let|also|apply|run"};
    public static final Pattern c = Pattern.compile("^(?:(?://.*?(?=\\n|$))|(/\\*.*?\\*/))", 32);
    public static final Pattern d = Pattern.compile("^@(\\w+)");
    public static final Pattern e = Pattern.compile("^\"[\\s\\S]*?(?<!\\\\)\"(?=\\W|\\s|$)");
    public static final o f = null;

    /* compiled from: Kotlin.kt */
    /* loaded from: classes.dex */
    public static final class a<RC> extends Node.a<RC> {

        /* renamed from: b  reason: collision with root package name */
        public static final C0051a f295b = new C0051a(null);
        public static final Pattern a = Pattern.compile("^(val|var)(\\s+\\w+)", 32);

        /* compiled from: Kotlin.kt */
        /* renamed from: b.a.t.a.o$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C0051a {
            public C0051a(DefaultConstructorMarker defaultConstructorMarker) {
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(String str, String str2, f<RC> fVar) {
            super(new StyleNode.b(str, fVar.d), new StyleNode.b(str2, fVar.e));
            m.checkNotNullParameter(str, "definition");
            m.checkNotNullParameter(str2, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkNotNullParameter(fVar, "codeStyleProviders");
        }
    }

    /* compiled from: Kotlin.kt */
    /* loaded from: classes.dex */
    public static final class b<RC> extends Node.a<RC> {

        /* renamed from: b  reason: collision with root package name */
        public static final a f296b = new a(null);
        public static final Pattern a = new Regex("^(fun)( *<.*>)?( \\w+)( *\\(.*?\\))", i.DOT_MATCHES_ALL).toPattern();

        /* compiled from: Kotlin.kt */
        /* loaded from: classes.dex */
        public static final class a {
            public a(DefaultConstructorMarker defaultConstructorMarker) {
            }
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public b(java.lang.String r4, java.lang.String r5, java.lang.String r6, java.lang.String r7, b.a.t.a.f<RC> r8) {
            /*
                r3 = this;
                java.lang.String r0 = "pre"
                d0.z.d.m.checkNotNullParameter(r4, r0)
                java.lang.String r0 = "signature"
                d0.z.d.m.checkNotNullParameter(r6, r0)
                java.lang.String r0 = "params"
                d0.z.d.m.checkNotNullParameter(r7, r0)
                java.lang.String r0 = "codeStyleProviders"
                d0.z.d.m.checkNotNullParameter(r8, r0)
                r0 = 4
                com.discord.simpleast.core.node.Node[] r0 = new com.discord.simpleast.core.node.Node[r0]
                com.discord.simpleast.core.node.StyleNode$b r1 = new com.discord.simpleast.core.node.StyleNode$b
                com.discord.simpleast.core.node.StyleNode$a<R> r2 = r8.d
                r1.<init>(r4, r2)
                r4 = 0
                r0[r4] = r1
                r4 = 1
                if (r5 == 0) goto L2f
                com.discord.simpleast.core.node.StyleNode$b r1 = new com.discord.simpleast.core.node.StyleNode$b
                com.discord.simpleast.core.node.StyleNode$a<R> r2 = r8.g
                r1.<init>(r5, r2)
                goto L30
            L2f:
                r1 = 0
            L30:
                r0[r4] = r1
                r4 = 2
                com.discord.simpleast.core.node.StyleNode$b r5 = new com.discord.simpleast.core.node.StyleNode$b
                com.discord.simpleast.core.node.StyleNode$a<R> r1 = r8.e
                r5.<init>(r6, r1)
                r0[r4] = r5
                r4 = 3
                com.discord.simpleast.core.node.StyleNode$b r5 = new com.discord.simpleast.core.node.StyleNode$b
                com.discord.simpleast.core.node.StyleNode$a<R> r6 = r8.h
                r5.<init>(r7, r6)
                r0[r4] = r5
                r3.<init>(r0)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.a.t.a.o.b.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, b.a.t.a.f):void");
        }
    }
}
