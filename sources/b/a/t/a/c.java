package b.a.t.a;

import android.text.SpannableStringBuilder;
import com.discord.simpleast.code.CodeNode;
import com.discord.simpleast.core.node.Node;
import com.discord.simpleast.core.node.StyleNode;
import com.discord.simpleast.core.parser.ParseSpec;
import com.discord.simpleast.core.parser.Parser;
import com.discord.simpleast.core.parser.Rule;
import d0.z.d.m;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/* compiled from: CodeRules.kt */
/* loaded from: classes.dex */
public final class c extends Rule<R, Node<R>, S> {
    public final /* synthetic */ StyleNode.a a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ StyleNode.a f284b;

    /* compiled from: CodeRules.kt */
    /* loaded from: classes.dex */
    public static final class a extends Node.a<R> {
        public a(CodeNode codeNode, Node[] nodeArr) {
            super(nodeArr);
        }

        @Override // com.discord.simpleast.core.node.Node.a, com.discord.simpleast.core.node.Node
        public void render(SpannableStringBuilder spannableStringBuilder, R r) {
            m.checkNotNullParameter(spannableStringBuilder, "builder");
            int length = spannableStringBuilder.length();
            super.render(spannableStringBuilder, r);
            Iterator<?> it = c.this.f284b.get(r).iterator();
            while (it.hasNext()) {
                spannableStringBuilder.setSpan(it.next(), length, spannableStringBuilder.length(), 33);
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public c(e eVar, StyleNode.a aVar, StyleNode.a aVar2, Pattern pattern) {
        super(pattern);
        this.a = aVar;
        this.f284b = aVar2;
    }

    @Override // com.discord.simpleast.core.parser.Rule
    public ParseSpec<R, S> parse(Matcher matcher, Parser<R, ? super Node<R>, S> parser, S s2) {
        m.checkNotNullParameter(matcher, "matcher");
        m.checkNotNullParameter(parser, "parser");
        String group = matcher.group(2);
        if (group == null) {
            group = "";
        }
        if (group.length() == 0) {
            String group2 = matcher.group();
            m.checkNotNullExpressionValue(group2, "matcher.group()");
            b.a.t.b.a.a aVar = new b.a.t.b.a.a(group2);
            m.checkNotNullParameter(aVar, "node");
            return new ParseSpec<>(aVar, s2);
        }
        CodeNode codeNode = new CodeNode(new CodeNode.a.b(group), null, this.a);
        a aVar2 = new a(codeNode, new Node[]{codeNode});
        m.checkNotNullParameter(aVar2, "node");
        return new ParseSpec<>(aVar2, s2);
    }
}
