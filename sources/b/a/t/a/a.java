package b.a.t.a;

import com.discord.simpleast.code.CodeNode;
import com.discord.simpleast.core.node.Node;
import com.discord.simpleast.core.node.StyleNode;
import com.discord.simpleast.core.parser.ParseSpec;
import com.discord.simpleast.core.parser.Parser;
import com.discord.simpleast.core.parser.Rule;
import d0.g0.w;
import d0.z.d.m;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kotlin.jvm.functions.Function3;
/* compiled from: CodeRules.kt */
/* loaded from: classes.dex */
public final class a extends Rule<R, Node<R>, S> {
    public final /* synthetic */ Map a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ StyleNode.a f283b;
    public final /* synthetic */ Function3 c;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public a(e eVar, Map map, StyleNode.a aVar, Function3 function3, Pattern pattern) {
        super(pattern);
        this.a = map;
        this.f283b = aVar;
        this.c = function3;
    }

    @Override // com.discord.simpleast.core.parser.Rule
    public ParseSpec<R, S> parse(Matcher matcher, Parser<R, ? super Node<R>, S> parser, S s2) {
        CodeNode.a aVar;
        m.checkNotNullParameter(matcher, "matcher");
        m.checkNotNullParameter(parser, "parser");
        String group = matcher.group(1);
        String group2 = matcher.group(3);
        if (group2 == null) {
            group2 = "";
        }
        String group3 = matcher.group(2);
        List list = null;
        boolean z2 = false;
        if (group3 != null) {
            z2 = w.contains$default((CharSequence) group3, '\n', false, 2, (Object) null);
        }
        if (group != null) {
            list = (List) this.a.get(group);
        }
        if (list != null) {
            List parse = parser.parse(group2, s2, list);
            Objects.requireNonNull(parse, "null cannot be cast to non-null type kotlin.collections.List<com.discord.simpleast.core.node.Node<R>>");
            aVar = new CodeNode.a.C0191a(group2, parse);
        } else {
            aVar = new CodeNode.a.b(group2);
        }
        Node node = (Node) this.c.invoke(new CodeNode(aVar, group, this.f283b), Boolean.valueOf(z2), s2);
        m.checkNotNullParameter(node, "node");
        return new ParseSpec<>(node, s2);
    }
}
