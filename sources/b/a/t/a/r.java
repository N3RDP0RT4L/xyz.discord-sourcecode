package b.a.t.a;

import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.simpleast.core.node.Node;
import com.discord.simpleast.core.node.StyleNode;
import d0.g0.i;
import d0.z.d.m;
import java.util.regex.Pattern;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.text.Regex;
/* compiled from: TypeScript.kt */
/* loaded from: classes.dex */
public final class r {
    public static final String[] a = {"import|from|export|default|package", "class|enum", "function|super|extends|implements|arguments", "var|let|const|static|get|set|new", "return|break|continue|yield|void", "if|else|for|while|do|switch|async|await|case|try|catch|finally|delete|throw|NaN|Infinity", "of|in|instanceof|typeof", "debugger|with", "true|false|null|undefined", "type|as|interface|public|private|protected|module|declare|namespace", "abstract|keyof|readonly|is|asserts|infer|override|intrinsic"};

    /* renamed from: b  reason: collision with root package name */
    public static final String[] f297b = {"String|Boolean|RegExp|Number|Date|Math|JSON|Symbol|BigInt|Atomics|DataView", "Function|Promise|Generator|GeneratorFunction|AsyncFunction|AsyncGenerator|AsyncGeneratorFunction", "Array|Object|Map|Set|WeakMap|WeakSet|Int8Array|Int16Array|Int32Array|Uint8Array|Uint16Array", "Uint32Array|Uint8ClampedArray|Float32Array|Float64Array|BigInt64Array|BigUint64Array|Buffer", "ArrayBuffer|SharedArrayBuffer", "Reflect|Proxy|Intl|WebAssembly", "console|process|require|isNaN|parseInt|parseFloat|encodeURI|decodeURI|encodeURIComponent", "decodeURIComponent|this|global|globalThis|eval|isFinite|module", "setTimeout|setInterval|clearTimeout|clearInterval|setImmediate|clearImmediate", "queueMicrotask|document|window", "Error|SyntaxError|TypeError|RangeError|ReferenceError|EvalError|InternalError|URIError", "AggregateError|escape|unescape|URL|URLSearchParams|TextEncoder|TextDecoder", "AbortController|AbortSignal|EventTarget|Event|MessageChannel", "MessagePort|MessageEvent|FinalizationRegistry|WeakRef", "regeneratorRuntime|performance", "Iterable|Iterator|IterableIterator", "Partial|Required|Readonly|Record|Pick|Omit|Exclude|Extract", "NonNullable|Parameters|ConstructorParameters|ReturnType", "InstanceType|ThisParameterType|OmitThisParameter", "ThisType|Uppercase|Lowercase|Capitalize|Uncapitalize"};
    public static final String[] c = {"string|number|boolean|object|symbol|any|unknown|bigint|never"};
    public static final Pattern d = Pattern.compile("^/.+(?<!\\\\)/[dgimsuy]*");
    public static final Pattern e = Pattern.compile("^(?:(?://.*?(?=\\n|$))|(/\\*.*?\\*/))", 32);
    public static final Pattern f = Pattern.compile("^('.*?(?<!\\\\)'|\".*?(?<!\\\\)\"|`[\\s\\S]*?(?<!\\\\)`)(?=\\W|\\s|$)");
    public static final r g = null;

    /* compiled from: TypeScript.kt */
    /* loaded from: classes.dex */
    public static final class a<RC> extends Node.a<RC> {

        /* renamed from: b  reason: collision with root package name */
        public static final C0052a f298b = new C0052a(null);
        public static final Pattern a = Pattern.compile("^(@)(\\s*[a-zA-Z_$][a-zA-Z0-9_$]*)(<.*>)?", 32);

        /* compiled from: TypeScript.kt */
        /* renamed from: b.a.t.a.r$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C0052a {
            public C0052a(DefaultConstructorMarker defaultConstructorMarker) {
            }
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public a(java.lang.String r4, java.lang.String r5, java.lang.String r6, b.a.t.a.f<RC> r7) {
            /*
                r3 = this;
                java.lang.String r0 = "prefix"
                d0.z.d.m.checkNotNullParameter(r4, r0)
                java.lang.String r0 = "decorator"
                d0.z.d.m.checkNotNullParameter(r5, r0)
                java.lang.String r0 = "codeStyleProviders"
                d0.z.d.m.checkNotNullParameter(r7, r0)
                r0 = 3
                com.discord.simpleast.core.node.Node[] r0 = new com.discord.simpleast.core.node.Node[r0]
                com.discord.simpleast.core.node.StyleNode$b r1 = new com.discord.simpleast.core.node.StyleNode$b
                com.discord.simpleast.core.node.StyleNode$a<R> r2 = r7.d
                r1.<init>(r4, r2)
                r4 = 0
                r0[r4] = r1
                r4 = 1
                com.discord.simpleast.core.node.StyleNode$b r1 = new com.discord.simpleast.core.node.StyleNode$b
                com.discord.simpleast.core.node.StyleNode$a<R> r2 = r7.g
                r1.<init>(r5, r2)
                r0[r4] = r1
                r4 = 2
                if (r6 == 0) goto L32
                com.discord.simpleast.core.node.StyleNode$b r5 = new com.discord.simpleast.core.node.StyleNode$b
                com.discord.simpleast.core.node.StyleNode$a<R> r7 = r7.g
                r5.<init>(r6, r7)
                goto L33
            L32:
                r5 = 0
            L33:
                r0[r4] = r5
                r3.<init>(r0)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.a.t.a.r.a.<init>(java.lang.String, java.lang.String, java.lang.String, b.a.t.a.f):void");
        }
    }

    /* compiled from: TypeScript.kt */
    /* loaded from: classes.dex */
    public static final class b<RC> extends Node.a<RC> {

        /* renamed from: b  reason: collision with root package name */
        public static final a f299b = new a(null);
        public static final Pattern a = Pattern.compile("^(var|let|const)(\\s+[a-zA-Z_$][a-zA-Z0-9_$]*)");

        /* compiled from: TypeScript.kt */
        /* loaded from: classes.dex */
        public static final class a {
            public a(DefaultConstructorMarker defaultConstructorMarker) {
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(String str, String str2, f<RC> fVar) {
            super(new StyleNode.b(str, fVar.d), new StyleNode.b(str2, fVar.e));
            m.checkNotNullParameter(str, "definition");
            m.checkNotNullParameter(str2, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkNotNullParameter(fVar, "codeStyleProviders");
        }
    }

    /* compiled from: TypeScript.kt */
    /* loaded from: classes.dex */
    public static final class c<RC> extends Node.a<RC> {

        /* renamed from: b  reason: collision with root package name */
        public static final a f300b = new a(null);
        public static final Pattern a = new Regex("^((?:function\\*?|static|get|set|async)\\s)(\\s*[a-zA-Z_$][a-zA-Z0-9_$]*)?(\\s*<.*>)?", i.DOT_MATCHES_ALL).toPattern();

        /* compiled from: TypeScript.kt */
        /* loaded from: classes.dex */
        public static final class a {
            public a(DefaultConstructorMarker defaultConstructorMarker) {
            }
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public c(java.lang.String r5, java.lang.String r6, java.lang.String r7, b.a.t.a.f<RC> r8) {
            /*
                r4 = this;
                java.lang.String r0 = "pre"
                d0.z.d.m.checkNotNullParameter(r5, r0)
                java.lang.String r0 = "codeStyleProviders"
                d0.z.d.m.checkNotNullParameter(r8, r0)
                r0 = 3
                com.discord.simpleast.core.node.Node[] r0 = new com.discord.simpleast.core.node.Node[r0]
                com.discord.simpleast.core.node.StyleNode$b r1 = new com.discord.simpleast.core.node.StyleNode$b
                com.discord.simpleast.core.node.StyleNode$a<R> r2 = r8.d
                r1.<init>(r5, r2)
                r5 = 0
                r0[r5] = r1
                r5 = 1
                r1 = 0
                if (r6 == 0) goto L24
                com.discord.simpleast.core.node.StyleNode$b r2 = new com.discord.simpleast.core.node.StyleNode$b
                com.discord.simpleast.core.node.StyleNode$a<R> r3 = r8.e
                r2.<init>(r6, r3)
                goto L25
            L24:
                r2 = r1
            L25:
                r0[r5] = r2
                r5 = 2
                if (r7 == 0) goto L31
                com.discord.simpleast.core.node.StyleNode$b r1 = new com.discord.simpleast.core.node.StyleNode$b
                com.discord.simpleast.core.node.StyleNode$a<R> r6 = r8.g
                r1.<init>(r7, r6)
            L31:
                r0[r5] = r1
                r4.<init>(r0)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.a.t.a.r.c.<init>(java.lang.String, java.lang.String, java.lang.String, b.a.t.a.f):void");
        }
    }

    /* compiled from: TypeScript.kt */
    /* loaded from: classes.dex */
    public static final class d<RC> extends Node.a<RC> {

        /* renamed from: b  reason: collision with root package name */
        public static final a f301b = new a(null);
        public static final Pattern a = Pattern.compile("^([{\\[(,;](?:\\s*-)?)(\\s*(?:public|private|protected|readonly))?(\\s*[a-zA-Z0-9_$]+)((?:\\s*\\?)?\\s*:)");

        /* compiled from: TypeScript.kt */
        /* loaded from: classes.dex */
        public static final class a {
            public a(DefaultConstructorMarker defaultConstructorMarker) {
            }
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public d(java.lang.String r4, java.lang.String r5, java.lang.String r6, java.lang.String r7, b.a.t.a.f<RC> r8) {
            /*
                r3 = this;
                java.lang.String r0 = "prefix"
                d0.z.d.m.checkNotNullParameter(r4, r0)
                java.lang.String r0 = "property"
                d0.z.d.m.checkNotNullParameter(r6, r0)
                java.lang.String r0 = "suffix"
                d0.z.d.m.checkNotNullParameter(r7, r0)
                java.lang.String r0 = "codeStyleProviders"
                d0.z.d.m.checkNotNullParameter(r8, r0)
                r0 = 4
                com.discord.simpleast.core.node.Node[] r0 = new com.discord.simpleast.core.node.Node[r0]
                com.discord.simpleast.core.node.StyleNode$b r1 = new com.discord.simpleast.core.node.StyleNode$b
                com.discord.simpleast.core.node.StyleNode$a<R> r2 = r8.a
                r1.<init>(r4, r2)
                r4 = 0
                r0[r4] = r1
                r4 = 1
                if (r5 == 0) goto L2f
                com.discord.simpleast.core.node.StyleNode$b r1 = new com.discord.simpleast.core.node.StyleNode$b
                com.discord.simpleast.core.node.StyleNode$a<R> r2 = r8.d
                r1.<init>(r5, r2)
                goto L30
            L2f:
                r1 = 0
            L30:
                r0[r4] = r1
                r4 = 2
                com.discord.simpleast.core.node.StyleNode$b r5 = new com.discord.simpleast.core.node.StyleNode$b
                com.discord.simpleast.core.node.StyleNode$a<R> r1 = r8.e
                r5.<init>(r6, r1)
                r0[r4] = r5
                r4 = 3
                com.discord.simpleast.core.node.StyleNode$b r5 = new com.discord.simpleast.core.node.StyleNode$b
                com.discord.simpleast.core.node.StyleNode$a<R> r6 = r8.a
                r5.<init>(r7, r6)
                r0[r4] = r5
                r3.<init>(r0)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.a.t.a.r.d.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, b.a.t.a.f):void");
        }
    }
}
