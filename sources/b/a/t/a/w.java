package b.a.t.a;

import b.a.t.a.v;
import com.discord.simpleast.core.node.Node;
import com.discord.simpleast.core.parser.ParseSpec;
import com.discord.simpleast.core.parser.Parser;
import com.discord.simpleast.core.parser.Rule;
import d0.z.d.m;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/* compiled from: Xml.kt */
/* loaded from: classes.dex */
public final class w extends Rule<RC, Node<RC>, S> {
    public final /* synthetic */ f a;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public w(v vVar, f fVar, Pattern pattern) {
        super(pattern);
        this.a = fVar;
    }

    @Override // com.discord.simpleast.core.parser.Rule
    public ParseSpec<RC, S> parse(Matcher matcher, Parser<RC, ? super Node<RC>, S> parser, S s2) {
        m.checkNotNullParameter(matcher, "matcher");
        m.checkNotNullParameter(parser, "parser");
        String group = matcher.group(1);
        m.checkNotNull(group);
        String group2 = matcher.group(3);
        if (matcher.group(2) != null) {
            v.a aVar = new v.a(group, group2, this.a);
            int start = matcher.start(2);
            int end = matcher.end(2);
            m.checkNotNullParameter(aVar, "node");
            return new ParseSpec<>(aVar, s2, start, end);
        }
        v.a aVar2 = new v.a(group, group2, this.a);
        m.checkNotNullParameter(aVar2, "node");
        return new ParseSpec<>(aVar2, s2);
    }
}
