package b.a.t.a;

import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.simpleast.core.node.Node;
import com.discord.simpleast.core.node.StyleNode;
import d0.g0.i;
import d0.z.d.m;
import java.util.regex.Pattern;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.text.Regex;
/* compiled from: JavaScript.kt */
/* loaded from: classes.dex */
public final class k {
    public static final String[] a = {"import|from|export|default|package", "class|enum", "function|super|extends|implements|arguments", "var|let|const|static|get|set|new", "return|break|continue|yield|void", "if|else|for|while|do|switch|async|await|case|try|catch|finally|delete|throw|NaN|Infinity", "of|in|instanceof|typeof", "debugger|with", "true|false|null|undefined"};

    /* renamed from: b  reason: collision with root package name */
    public static final String[] f290b = {"String|Boolean|RegExp|Number|Date|Math|JSON|Symbol|BigInt|Atomics|DataView", "Function|Promise|Generator|GeneratorFunction|AsyncFunction|AsyncGenerator|AsyncGeneratorFunction", "Array|Object|Map|Set|WeakMap|WeakSet|Int8Array|Int16Array|Int32Array|Uint8Array|Uint16Array", "Uint32Array|Uint8ClampedArray|Float32Array|Float64Array|BigInt64Array|BigUint64Array|Buffer", "ArrayBuffer|SharedArrayBuffer", "Reflect|Proxy|Intl|WebAssembly", "console|process|require|isNaN|parseInt|parseFloat|encodeURI|decodeURI|encodeURIComponent", "decodeURIComponent|this|global|globalThis|eval|isFinite|module", "setTimeout|setInterval|clearTimeout|clearInterval|setImmediate|clearImmediate", "queueMicrotask|document|window", "Error|SyntaxError|TypeError|RangeError|ReferenceError|EvalError|InternalError|URIError", "AggregateError|escape|unescape|URL|URLSearchParams|TextEncoder|TextDecoder", "AbortController|AbortSignal|EventTarget|Event|MessageChannel", "MessagePort|MessageEvent|FinalizationRegistry|WeakRef", "regeneratorRuntime|performance"};
    public static final Pattern c = Pattern.compile("^/.+(?<!\\\\)/[dgimsuy]*");
    public static final Pattern d = Pattern.compile("^<.*(?<!\\\\)>");
    public static final Pattern e = Pattern.compile("^(?:(?://.*?(?=\\n|$))|(/\\*.*?\\*/))", 32);
    public static final Pattern f = Pattern.compile("^('.*?(?<!\\\\)'|\".*?(?<!\\\\)\"|`[\\s\\S]*?(?<!\\\\)`)(?=\\W|\\s|$)");
    public static final k g = null;

    /* compiled from: JavaScript.kt */
    /* loaded from: classes.dex */
    public static final class a<RC> extends Node.a<RC> {

        /* renamed from: b  reason: collision with root package name */
        public static final C0050a f291b = new C0050a(null);
        public static final Pattern a = Pattern.compile("^(var|let|const)(\\s+[a-zA-Z_$][a-zA-Z0-9_$]*)");

        /* compiled from: JavaScript.kt */
        /* renamed from: b.a.t.a.k$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C0050a {
            public C0050a(DefaultConstructorMarker defaultConstructorMarker) {
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(String str, String str2, f<RC> fVar) {
            super(new StyleNode.b(str, fVar.d), new StyleNode.b(str2, fVar.e));
            m.checkNotNullParameter(str, "definition");
            m.checkNotNullParameter(str2, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkNotNullParameter(fVar, "codeStyleProviders");
        }
    }

    /* compiled from: JavaScript.kt */
    /* loaded from: classes.dex */
    public static final class b<RC> extends Node.a<RC> {

        /* renamed from: b  reason: collision with root package name */
        public static final a f292b = new a(null);
        public static final Pattern a = new Regex("^(function\\*?|static|get|set|async)(\\s+[a-zA-Z_$][a-zA-Z0-9_$]*)?(\\s*\\(.*?\\))", i.DOT_MATCHES_ALL).toPattern();

        /* compiled from: JavaScript.kt */
        /* loaded from: classes.dex */
        public static final class a {
            public a(DefaultConstructorMarker defaultConstructorMarker) {
            }
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public b(java.lang.String r4, java.lang.String r5, java.lang.String r6, b.a.t.a.f<RC> r7) {
            /*
                r3 = this;
                java.lang.String r0 = "pre"
                d0.z.d.m.checkNotNullParameter(r4, r0)
                java.lang.String r0 = "params"
                d0.z.d.m.checkNotNullParameter(r6, r0)
                java.lang.String r0 = "codeStyleProviders"
                d0.z.d.m.checkNotNullParameter(r7, r0)
                r0 = 3
                com.discord.simpleast.core.node.Node[] r0 = new com.discord.simpleast.core.node.Node[r0]
                com.discord.simpleast.core.node.StyleNode$b r1 = new com.discord.simpleast.core.node.StyleNode$b
                com.discord.simpleast.core.node.StyleNode$a<R> r2 = r7.d
                r1.<init>(r4, r2)
                r4 = 0
                r0[r4] = r1
                r4 = 1
                if (r5 == 0) goto L29
                com.discord.simpleast.core.node.StyleNode$b r1 = new com.discord.simpleast.core.node.StyleNode$b
                com.discord.simpleast.core.node.StyleNode$a<R> r2 = r7.e
                r1.<init>(r5, r2)
                goto L2a
            L29:
                r1 = 0
            L2a:
                r0[r4] = r1
                r4 = 2
                com.discord.simpleast.core.node.StyleNode$b r5 = new com.discord.simpleast.core.node.StyleNode$b
                com.discord.simpleast.core.node.StyleNode$a<R> r7 = r7.h
                r5.<init>(r6, r7)
                r0[r4] = r5
                r3.<init>(r0)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.a.t.a.k.b.<init>(java.lang.String, java.lang.String, java.lang.String, b.a.t.a.f):void");
        }
    }

    /* compiled from: JavaScript.kt */
    /* loaded from: classes.dex */
    public static final class c<RC> extends Node.a<RC> {

        /* renamed from: b  reason: collision with root package name */
        public static final a f293b = new a(null);
        public static final Pattern a = Pattern.compile("^([\\{\\[\\,])(\\s*[a-zA-Z0-9_$]+)(\\s*:)");

        /* compiled from: JavaScript.kt */
        /* loaded from: classes.dex */
        public static final class a {
            public a(DefaultConstructorMarker defaultConstructorMarker) {
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(String str, String str2, String str3, f<RC> fVar) {
            super(new StyleNode.b(str, fVar.a), new StyleNode.b(str2, fVar.e), new StyleNode.b(str3, fVar.a));
            m.checkNotNullParameter(str, "prefix");
            m.checkNotNullParameter(str2, "property");
            m.checkNotNullParameter(str3, "suffix");
            m.checkNotNullParameter(fVar, "codeStyleProviders");
        }
    }
}
