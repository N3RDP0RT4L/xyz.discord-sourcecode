package b.a.t.a;

import com.discord.simpleast.core.node.Node;
import java.util.regex.Pattern;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: Crystal.kt */
/* loaded from: classes.dex */
public final class i {
    public static final String[] a = {"true|false|nil", "module|require|include|extend|lib", "abstract|private|protected", "annotation|class|finalize|new|initialize|allocate|self|super", "union|typeof|forall|is_a?|nil?|as?|as|responds_to?|alias|type", "property|getter|setter|struct|of", "previous_def|method|fun|enum|macro", "rescue|raise|begin|end|ensure", "if|else|elsif|then|unless|until", "for|in|of|do|when|select|with", "while|break|next|yield|case", "print|puts|return"};

    /* renamed from: b  reason: collision with root package name */
    public static final String[] f288b = {"Nil|Bool|true|false|Void|NoReturn", "Number|BigDecimal|BigRational|BigFloat|BigInt", "Int|Int8|Int16|Int32|Int64|UInt8|UInt16|UInt32|UInt64|Float|Float32|Float64", "Char|String|Symbol|Regex", "StaticArray|Array|Set|Hash|Range|Tuple|NamedTuple|Union|BitArray", "Proc|Command|Enum|Class", "Reference|Value|Struct|Object|Pointer", "Exception|ArgumentError|KeyError|TypeCastError|IndexError|RuntimeError|NilAssertionError|InvalidBigDecimalException|NotImplementedError|OverflowError", "pointerof|sizeof|instance_sizeof|offsetof|uninitialized"};
    public static final Pattern c = Pattern.compile("^(#.*)");
    public static final Pattern d = Pattern.compile("^@\\[(\\w+)(?:\\(.+\\))?]");
    public static final Pattern e = Pattern.compile("^\"[\\s\\S]*?(?<!\\\\)\"(?=\\W|\\s|$)");
    public static final Pattern f = Pattern.compile("^/.*?/[imx]?");
    public static final Pattern g = Pattern.compile("^(:\"?(?:[+-/%&^|]|\\*\\*?|\\w+|(?:<(?=[<=\\s])[<=]?(?:(?<==)>)?|>(?=[>=\\s])[>=]?(?:(?<==)>)?)|\\[][?=]?|(?:!(?=[=~\\s])[=~]?|=?(?:~|==?)))(?:(?<!\\\\)\"(?=\\s|$))?)");
    public static final i h = null;

    /* compiled from: Crystal.kt */
    /* loaded from: classes.dex */
    public static final class a<RC> extends Node.a<RC> {

        /* renamed from: b  reason: collision with root package name */
        public static final C0049a f289b = new C0049a(null);
        public static final Pattern a = Pattern.compile("^(def)( +\\w+)( *\\( *(?:@\\w+ +: +\\w*)?\\w+(?: +[:=] +.*)? *\\))?(?!.+)");

        /* compiled from: Crystal.kt */
        /* renamed from: b.a.t.a.i$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C0049a {
            public C0049a(DefaultConstructorMarker defaultConstructorMarker) {
            }
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public a(java.lang.String r4, java.lang.String r5, java.lang.String r6, b.a.t.a.f<RC> r7) {
            /*
                r3 = this;
                java.lang.String r0 = "pre"
                d0.z.d.m.checkNotNullParameter(r4, r0)
                java.lang.String r0 = "signature"
                d0.z.d.m.checkNotNullParameter(r5, r0)
                java.lang.String r0 = "codeStyleProviders"
                d0.z.d.m.checkNotNullParameter(r7, r0)
                r0 = 3
                com.discord.simpleast.core.node.Node[] r0 = new com.discord.simpleast.core.node.Node[r0]
                com.discord.simpleast.core.node.StyleNode$b r1 = new com.discord.simpleast.core.node.StyleNode$b
                com.discord.simpleast.core.node.StyleNode$a<R> r2 = r7.d
                r1.<init>(r4, r2)
                r4 = 0
                r0[r4] = r1
                r4 = 1
                com.discord.simpleast.core.node.StyleNode$b r1 = new com.discord.simpleast.core.node.StyleNode$b
                com.discord.simpleast.core.node.StyleNode$a<R> r2 = r7.e
                r1.<init>(r5, r2)
                r0[r4] = r1
                r4 = 2
                if (r6 == 0) goto L33
                com.discord.simpleast.core.node.StyleNode$b r5 = new com.discord.simpleast.core.node.StyleNode$b
                com.discord.simpleast.core.node.StyleNode$a<R> r7 = r7.h
                r5.<init>(r6, r7)
                goto L34
            L33:
                r5 = 0
            L34:
                r0[r4] = r5
                r3.<init>(r0)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.a.t.a.i.a.<init>(java.lang.String, java.lang.String, java.lang.String, b.a.t.a.f):void");
        }
    }
}
