package b.a.t.a;

import b.a.t.b.a.a;
import com.discord.simpleast.core.node.Node;
import com.discord.simpleast.core.node.StyleNode;
import com.discord.simpleast.core.parser.ParseSpec;
import com.discord.simpleast.core.parser.Parser;
import com.discord.simpleast.core.parser.Rule;
import d0.z.d.m;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/* compiled from: CodeRules.kt */
/* loaded from: classes.dex */
public final class d extends Rule<R, Node<R>, S> {
    public final /* synthetic */ int a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ StyleNode.a f285b;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public d(Pattern pattern, int i, StyleNode.a aVar, Pattern pattern2) {
        super(pattern2);
        this.a = i;
        this.f285b = aVar;
    }

    @Override // com.discord.simpleast.core.parser.Rule
    public ParseSpec<R, S> parse(Matcher matcher, Parser<R, ? super Node<R>, S> parser, S s2) {
        m.checkNotNullParameter(matcher, "matcher");
        m.checkNotNullParameter(parser, "parser");
        String group = matcher.group(this.a);
        if (group == null) {
            group = "";
        }
        StyleNode.a aVar = this.f285b;
        Node bVar = aVar != null ? new StyleNode.b(group, aVar) : new a(group);
        m.checkNotNullParameter(bVar, "node");
        return new ParseSpec<>(bVar, s2);
    }
}
