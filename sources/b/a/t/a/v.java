package b.a.t.a;

import android.text.SpannableStringBuilder;
import com.discord.simpleast.core.node.Node;
import com.discord.simpleast.core.node.StyleNode;
import d0.o;
import d0.z.d.k;
import d0.z.d.m;
import java.util.Iterator;
import java.util.Objects;
import java.util.regex.Pattern;
import kotlin.Pair;
import kotlin.jvm.functions.Function1;
/* compiled from: Xml.kt */
/* loaded from: classes.dex */
public final class v {
    public static final Pattern a;

    /* renamed from: b  reason: collision with root package name */
    public static final Pattern f302b;
    public static final v c = new v();

    /* compiled from: Xml.kt */
    /* loaded from: classes.dex */
    public static final class a<RC> extends Node.a<RC> {
        public final String a;

        /* renamed from: b  reason: collision with root package name */
        public final String f303b;
        public final f<RC> c;

        /* compiled from: Xml.kt */
        /* renamed from: b.a.t.a.v$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final /* synthetic */ class C0053a extends k implements Function1<RC, Iterable<?>> {
            public C0053a(StyleNode.a aVar) {
                super(1, aVar, StyleNode.a.class, "get", "get(Ljava/lang/Object;)Ljava/lang/Iterable;", 0);
            }

            @Override // kotlin.jvm.functions.Function1
            public Iterable<?> invoke(Object obj) {
                return ((StyleNode.a) this.receiver).get(obj);
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(String str, String str2, f<RC> fVar) {
            super(new Node[0]);
            m.checkNotNullParameter(str, "opening");
            m.checkNotNullParameter(fVar, "codeStyleProviders");
            this.a = str;
            this.f303b = str2;
            this.c = fVar;
        }

        @Override // com.discord.simpleast.core.node.Node.a, com.discord.simpleast.core.node.Node
        public void render(SpannableStringBuilder spannableStringBuilder, RC rc) {
            Pair pair;
            m.checkNotNullParameter(spannableStringBuilder, "builder");
            String str = this.a;
            int length = str.length();
            boolean z2 = false;
            int i = 0;
            while (true) {
                if (i >= length) {
                    i = -1;
                    break;
                }
                char charAt = str.charAt(i);
                if (d0.g0.a.isWhitespace(charAt) || charAt == '/') {
                    break;
                }
                i++;
            }
            if (i != -1) {
                String str2 = this.a;
                Objects.requireNonNull(str2, "null cannot be cast to non-null type java.lang.String");
                String substring = str2.substring(0, i);
                m.checkNotNullExpressionValue(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                String str3 = this.a;
                Objects.requireNonNull(str3, "null cannot be cast to non-null type java.lang.String");
                String substring2 = str3.substring(i);
                m.checkNotNullExpressionValue(substring2, "(this as java.lang.String).substring(startIndex)");
                pair = o.to(substring, substring2);
            } else {
                pair = o.to(this.a, "");
            }
            String str4 = (String) pair.component2();
            C0053a aVar = new C0053a(this.c.g);
            int length2 = spannableStringBuilder.length();
            spannableStringBuilder.append((CharSequence) ('<' + ((String) pair.component1())));
            Iterator<?> it = aVar.invoke(rc).iterator();
            while (it.hasNext()) {
                spannableStringBuilder.setSpan(it.next(), length2, spannableStringBuilder.length(), 33);
            }
            int length3 = spannableStringBuilder.length();
            spannableStringBuilder.append((CharSequence) (str4 + '>'));
            Iterator<?> it2 = this.c.h.get(rc).iterator();
            while (it2.hasNext()) {
                spannableStringBuilder.setSpan(it2.next(), length3, spannableStringBuilder.length() - 1, 33);
            }
            Iterator<?> it3 = aVar.invoke(rc).iterator();
            while (it3.hasNext()) {
                spannableStringBuilder.setSpan(it3.next(), spannableStringBuilder.length() - 1, spannableStringBuilder.length(), 33);
            }
            super.render(spannableStringBuilder, rc);
            String str5 = this.f303b;
            if (str5 == null || str5.length() == 0) {
                z2 = true;
            }
            if (!z2) {
                int length4 = spannableStringBuilder.length();
                StringBuilder R = b.d.b.a.a.R("</");
                R.append(this.f303b);
                R.append('>');
                spannableStringBuilder.append((CharSequence) R.toString());
                Iterator<?> it4 = aVar.invoke(rc).iterator();
                while (it4.hasNext()) {
                    spannableStringBuilder.setSpan(it4.next(), length4 + 1, spannableStringBuilder.length(), 33);
                }
            }
        }
    }

    static {
        Pattern compile = Pattern.compile("^<!--[\\s\\S]*?-->", 32);
        m.checkNotNullExpressionValue(compile, "Pattern.compile(\"\"\"^<!--…*?-->\"\"\", Pattern.DOTALL)");
        a = compile;
        Pattern compile2 = Pattern.compile("^<([\\s\\S]+?)(?:>(.*?)<\\/([\\s\\S]+?))?>", 32);
        m.checkNotNullExpressionValue(compile2, "Pattern.compile(\n      \"…?))?>\"\"\", Pattern.DOTALL)");
        f302b = compile2;
    }
}
