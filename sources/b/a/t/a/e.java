package b.a.t.a;

import b.d.b.a.a;
import com.discord.simpleast.core.node.Node;
import com.discord.simpleast.core.node.StyleNode;
import com.discord.simpleast.core.parser.Rule;
import d0.t.k;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;
import kotlin.jvm.functions.Function1;
/* compiled from: CodeRules.kt */
/* loaded from: classes.dex */
public final class e {
    public static final Pattern a;

    /* renamed from: b  reason: collision with root package name */
    public static final Pattern f286b;
    public static final Pattern c;
    public static final Pattern d;
    public static final Pattern e;
    public static final e f = new e();

    static {
        Pattern compile = Pattern.compile("^```(?:([\\w+\\-.]+?)?(\\s*\\n))?([^\\n].*?)\\n*```", 32);
        m.checkNotNullExpressionValue(compile, "Pattern.compile(\"\"\"^```(…n*```\"\"\", Pattern.DOTALL)");
        a = compile;
        Pattern compile2 = Pattern.compile("^(``?)([^`]*)\\1", 32);
        m.checkNotNullExpressionValue(compile2, "Pattern.compile(\"\"\"^(``?…]*)\\1\"\"\", Pattern.DOTALL)");
        f286b = compile2;
        Pattern compile3 = Pattern.compile("^(?:\\n\\s*)+");
        m.checkNotNullExpressionValue(compile3, "Pattern.compile(\"\"\"^(?:\\n\\s*)+\"\"\")");
        c = compile3;
        Pattern compile4 = Pattern.compile("^[\\s\\S]+?(?=\\b|[^0-9A-Za-z\\s\\u00c0-\\uffff]|\\n| {2,}\\n|\\w+:\\S|$)");
        m.checkNotNullExpressionValue(compile4, "Pattern.compile(\"\"\"^[\\s\\…|\\n| {2,}\\n|\\w+:\\S|$)\"\"\")");
        d = compile4;
        Pattern compile5 = Pattern.compile("^\\b\\d+?\\b");
        m.checkNotNullExpressionValue(compile5, "Pattern.compile(\"\"\"^\\b\\d+?\\b\"\"\")");
        e = compile5;
    }

    public static Rule e(e eVar, Pattern pattern, int i, StyleNode.a aVar, int i2) {
        if ((i2 & 1) != 0) {
            i = 0;
        }
        if ((i2 & 2) != 0) {
            aVar = null;
        }
        m.checkNotNullParameter(pattern, "$this$toMatchGroupRule");
        return new d(pattern, i, aVar, pattern);
    }

    public final <R, S> List<Rule<R, Node<R>, S>> a(f<R> fVar, List<? extends Rule<R, Node<R>, S>> list, String[] strArr, String[] strArr2, String[] strArr3, String[] strArr4) {
        String[] strArr5 = (String[]) Arrays.copyOf(strArr, strArr.length);
        m.checkNotNullParameter(fVar, "codeStyleProviders");
        m.checkNotNullParameter(strArr5, "identifiers");
        StringBuilder R = a.R("^\\b(");
        R.append(k.joinToString$default(strArr5, "|", (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, (Function1) null, 62, (Object) null));
        R.append(")(\\s+\\w+)");
        Pattern compile = Pattern.compile(R.toString());
        m.checkNotNullExpressionValue(compile, "Pattern.compile(\"\"\"^\\b($…String(\"|\")})(\\s+\\w+)\"\"\")");
        Pattern d2 = d((String[]) Arrays.copyOf(strArr2, strArr2.length));
        m.checkNotNullExpressionValue(d2, "createWordPattern(*builtIns)");
        Pattern d3 = d((String[]) Arrays.copyOf(strArr3, strArr3.length));
        m.checkNotNullExpressionValue(d3, "createWordPattern(*keywords)");
        Pattern d4 = d((String[]) Arrays.copyOf(strArr4, strArr4.length));
        m.checkNotNullExpressionValue(d4, "createWordPattern(*types)");
        return u.plus((Collection) list, (Iterable) n.listOf((Object[]) new Rule[]{new b(fVar, strArr5, compile), e(this, d2, 0, fVar.g, 1), e(this, d3, 0, fVar.d, 1), e(this, d4, 0, fVar.f, 1), e(this, e, 0, fVar.c, 1), e(this, c, 0, null, 3), e(this, d, 0, null, 3)}));
    }

    public final Pattern c(String str) {
        return Pattern.compile("^(?:" + str + ".*?(?=\\n|$))");
    }

    public final Pattern d(String... strArr) {
        m.checkNotNullParameter(strArr, "words");
        StringBuilder R = a.R("^\\b(?:");
        R.append(k.joinToString$default(strArr, "|", (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, (Function1) null, 62, (Object) null));
        R.append(")\\b");
        return Pattern.compile(R.toString());
    }
}
