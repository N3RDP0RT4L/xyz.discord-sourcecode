package b.a.t.a;

import b.d.b.a.a;
import com.discord.simpleast.core.node.StyleNode;
import d0.z.d.m;
/* compiled from: CodeStyleProviders.kt */
/* loaded from: classes.dex */
public final class f<R> {
    public final StyleNode.a<R> a;

    /* renamed from: b  reason: collision with root package name */
    public final StyleNode.a<R> f287b;
    public final StyleNode.a<R> c;
    public final StyleNode.a<R> d;
    public final StyleNode.a<R> e;
    public final StyleNode.a<R> f;
    public final StyleNode.a<R> g;
    public final StyleNode.a<R> h;

    public f(StyleNode.a<R> aVar, StyleNode.a<R> aVar2, StyleNode.a<R> aVar3, StyleNode.a<R> aVar4, StyleNode.a<R> aVar5, StyleNode.a<R> aVar6, StyleNode.a<R> aVar7, StyleNode.a<R> aVar8) {
        m.checkNotNullParameter(aVar, "defaultStyleProvider");
        m.checkNotNullParameter(aVar2, "commentStyleProvider");
        m.checkNotNullParameter(aVar3, "literalStyleProvider");
        m.checkNotNullParameter(aVar4, "keywordStyleProvider");
        m.checkNotNullParameter(aVar5, "identifierStyleProvider");
        m.checkNotNullParameter(aVar6, "typesStyleProvider");
        m.checkNotNullParameter(aVar7, "genericsStyleProvider");
        m.checkNotNullParameter(aVar8, "paramsStyleProvider");
        this.a = aVar;
        this.f287b = aVar2;
        this.c = aVar3;
        this.d = aVar4;
        this.e = aVar5;
        this.f = aVar6;
        this.g = aVar7;
        this.h = aVar8;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        return m.areEqual(this.a, fVar.a) && m.areEqual(this.f287b, fVar.f287b) && m.areEqual(this.c, fVar.c) && m.areEqual(this.d, fVar.d) && m.areEqual(this.e, fVar.e) && m.areEqual(this.f, fVar.f) && m.areEqual(this.g, fVar.g) && m.areEqual(this.h, fVar.h);
    }

    public int hashCode() {
        StyleNode.a<R> aVar = this.a;
        int i = 0;
        int hashCode = (aVar != null ? aVar.hashCode() : 0) * 31;
        StyleNode.a<R> aVar2 = this.f287b;
        int hashCode2 = (hashCode + (aVar2 != null ? aVar2.hashCode() : 0)) * 31;
        StyleNode.a<R> aVar3 = this.c;
        int hashCode3 = (hashCode2 + (aVar3 != null ? aVar3.hashCode() : 0)) * 31;
        StyleNode.a<R> aVar4 = this.d;
        int hashCode4 = (hashCode3 + (aVar4 != null ? aVar4.hashCode() : 0)) * 31;
        StyleNode.a<R> aVar5 = this.e;
        int hashCode5 = (hashCode4 + (aVar5 != null ? aVar5.hashCode() : 0)) * 31;
        StyleNode.a<R> aVar6 = this.f;
        int hashCode6 = (hashCode5 + (aVar6 != null ? aVar6.hashCode() : 0)) * 31;
        StyleNode.a<R> aVar7 = this.g;
        int hashCode7 = (hashCode6 + (aVar7 != null ? aVar7.hashCode() : 0)) * 31;
        StyleNode.a<R> aVar8 = this.h;
        if (aVar8 != null) {
            i = aVar8.hashCode();
        }
        return hashCode7 + i;
    }

    public String toString() {
        StringBuilder R = a.R("CodeStyleProviders(defaultStyleProvider=");
        R.append(this.a);
        R.append(", commentStyleProvider=");
        R.append(this.f287b);
        R.append(", literalStyleProvider=");
        R.append(this.c);
        R.append(", keywordStyleProvider=");
        R.append(this.d);
        R.append(", identifierStyleProvider=");
        R.append(this.e);
        R.append(", typesStyleProvider=");
        R.append(this.f);
        R.append(", genericsStyleProvider=");
        R.append(this.g);
        R.append(", paramsStyleProvider=");
        R.append(this.h);
        R.append(")");
        return R.toString();
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public f() {
        /*
            r9 = this;
            b.a.t.a.g r8 = b.a.t.a.g.a
            r0 = r9
            r1 = r8
            r2 = r8
            r3 = r8
            r4 = r8
            r5 = r8
            r6 = r8
            r7 = r8
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.t.a.f.<init>():void");
    }
}
