package b.a.t.c;

import android.text.style.CharacterStyle;
import b.a.t.b.b.e;
import com.discord.simpleast.core.node.Node;
import com.discord.simpleast.core.node.StyleNode;
import com.discord.simpleast.core.parser.ParseSpec;
import com.discord.simpleast.core.parser.Parser;
import com.discord.simpleast.core.parser.Rule;
import d0.g0.w;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kotlin.jvm.functions.Function1;
import kotlin.text.Regex;
/* compiled from: MarkdownRules.kt */
/* loaded from: classes.dex */
public final class a {
    public static final Pattern a;

    /* renamed from: b  reason: collision with root package name */
    public static final Pattern f305b;
    public static final Pattern c;
    public static final a e = new a();
    public static final Pattern d = new Regex("^\\s*(?:(?:(.+)(?: +\\{([\\w ]*)\\}))|(.*))[ \\t]*\\n *([=\\-]){3,}[ \\t]*(?=\\n|$)").toPattern();

    /* compiled from: MarkdownRules.kt */
    /* loaded from: classes.dex */
    public static class c<R, S> extends Rule.BlockRule<R, Node<R>, S> {
        private final Function1<Integer, CharacterStyle> styleSpanProvider;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        /* JADX WARN: Multi-variable type inference failed */
        public c(Pattern pattern, Function1<? super Integer, ? extends CharacterStyle> function1) {
            super(pattern);
            m.checkNotNullParameter(pattern, "pattern");
            m.checkNotNullParameter(function1, "styleSpanProvider");
            this.styleSpanProvider = function1;
        }

        public StyleNode<R, CharacterStyle> createHeaderStyleNode(String str) {
            m.checkNotNullParameter(str, "headerStyleGroup");
            return new StyleNode<>(d0.t.m.listOf(this.styleSpanProvider.invoke(Integer.valueOf(str.length()))));
        }

        public final Function1<Integer, CharacterStyle> getStyleSpanProvider() {
            return this.styleSpanProvider;
        }

        @Override // com.discord.simpleast.core.parser.Rule
        public ParseSpec<R, S> parse(Matcher matcher, Parser<R, ? super Node<R>, S> parser, S s2) {
            m.checkNotNullParameter(matcher, "matcher");
            m.checkNotNullParameter(parser, "parser");
            String group = matcher.group(1);
            m.checkNotNullExpressionValue(group, "matcher.group(1)");
            StyleNode<R, CharacterStyle> createHeaderStyleNode = createHeaderStyleNode(group);
            int start = matcher.start(2);
            int end = matcher.end(2);
            m.checkNotNullParameter(createHeaderStyleNode, "node");
            return new ParseSpec<>(createHeaderStyleNode, s2, start, end);
        }

        /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
        public c(Function1<? super Integer, ? extends CharacterStyle> function1) {
            this(a.f305b, function1);
            m.checkNotNullParameter(function1, "styleSpanProvider");
            a aVar = a.e;
        }
    }

    static {
        Pattern compile = Pattern.compile("^\\*[ \\t](.*)(?=\\n|$)", 0);
        m.checkNotNullExpressionValue(compile, "java.util.regex.Pattern.compile(this, flags)");
        a = compile;
        Pattern compile2 = Pattern.compile("^\\s*(#+)[ \\t](.*) *(?=\\n|$)", 0);
        m.checkNotNullExpressionValue(compile2, "java.util.regex.Pattern.compile(this, flags)");
        f305b = compile2;
        Pattern compile3 = Pattern.compile("^\\s*(.+)\\n *(=|-){3,} *(?=\\n|$)", 0);
        m.checkNotNullExpressionValue(compile3, "java.util.regex.Pattern.compile(this, flags)");
        c = compile3;
    }

    /* compiled from: MarkdownRules.kt */
    /* renamed from: b.a.t.c.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static class C0054a<RC, T, S> extends b<RC, S> {
        private final Function1<String, T> classSpanProvider;
        private final List<Rule<RC, Node<RC>, S>> innerRules;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        /* JADX WARN: Multi-variable type inference failed */
        public C0054a(Function1<? super Integer, ? extends CharacterStyle> function1, Function1<? super String, ? extends T> function12, List<? extends Rule<RC, Node<RC>, S>> list) {
            super(a.d, function1);
            m.checkNotNullParameter(function1, "styleSpanProvider");
            m.checkNotNullParameter(function12, "classSpanProvider");
            m.checkNotNullParameter(list, "innerRules");
            a aVar = a.e;
            this.classSpanProvider = function12;
            this.innerRules = list;
        }

        public final Function1<String, T> getClassSpanProvider() {
            return this.classSpanProvider;
        }

        public final List<Rule<RC, Node<RC>, S>> getInnerRules() {
            return this.innerRules;
        }

        @Override // b.a.t.c.a.b, b.a.t.c.a.c, com.discord.simpleast.core.parser.Rule
        public ParseSpec<RC, S> parse(Matcher matcher, Parser<RC, ? super Node<RC>, S> parser, S s2) {
            List list;
            String obj;
            m.checkNotNullParameter(matcher, "matcher");
            m.checkNotNullParameter(parser, "parser");
            String group = matcher.group(4);
            m.checkNotNullExpressionValue(group, "matcher.group(4)");
            StyleNode<RC, CharacterStyle> createHeaderStyleNode = createHeaderStyleNode(group);
            String group2 = matcher.group(1);
            if (group2 == null) {
                group2 = matcher.group(3);
            }
            m.checkNotNullExpressionValue(group2, "headerBody");
            for (T t : parser.parse(group2, s2, this.innerRules)) {
                Objects.requireNonNull(t, "null cannot be cast to non-null type com.discord.simpleast.core.node.Node<RC>");
                createHeaderStyleNode.addChild((Node) t);
            }
            String group3 = matcher.group(2);
            List<String> split$default = (group3 == null || (obj = w.trim(group3).toString()) == null) ? null : w.split$default((CharSequence) obj, new char[]{' '}, false, 0, 6, (Object) null);
            if (split$default != null) {
                list = new ArrayList();
                for (String str : split$default) {
                    T invoke = this.classSpanProvider.invoke(str);
                    if (invoke != null) {
                        list.add(invoke);
                    }
                }
            } else {
                list = n.emptyList();
            }
            if (!list.isEmpty()) {
                StyleNode<RC, CharacterStyle> styleNode = new StyleNode<>(list);
                styleNode.addChild(createHeaderStyleNode);
                createHeaderStyleNode = styleNode;
            }
            m.checkNotNullParameter(createHeaderStyleNode, "node");
            return new ParseSpec<>(createHeaderStyleNode, s2);
        }

        /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
        public C0054a(Function1<? super Integer, ? extends CharacterStyle> function1, Function1<? super String, ? extends T> function12) {
            this(function1, function12, u.plus((Collection<? extends Rule>) e.b(false, false, 2), e.h.d()));
            m.checkNotNullParameter(function1, "styleSpanProvider");
            m.checkNotNullParameter(function12, "classSpanProvider");
        }
    }

    /* compiled from: MarkdownRules.kt */
    /* loaded from: classes.dex */
    public static class b<R, S> extends c<R, S> {
        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public b(java.util.regex.Pattern r1, kotlin.jvm.functions.Function1 r2, int r3, kotlin.jvm.internal.DefaultConstructorMarker r4) {
            /*
                r0 = this;
                r3 = r3 & 1
                if (r3 == 0) goto L8
                b.a.t.c.a r1 = b.a.t.c.a.e
                java.util.regex.Pattern r1 = b.a.t.c.a.c
            L8:
                r0.<init>(r1, r2)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.a.t.c.a.b.<init>(java.util.regex.Pattern, kotlin.jvm.functions.Function1, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
        }

        @Override // b.a.t.c.a.c
        public StyleNode<R, CharacterStyle> createHeaderStyleNode(String str) {
            m.checkNotNullParameter(str, "headerStyleGroup");
            return new StyleNode<>(d0.t.m.listOf(getStyleSpanProvider().invoke(Integer.valueOf((str.hashCode() == 61 && str.equals("=")) ? 1 : 2))));
        }

        @Override // b.a.t.c.a.c, com.discord.simpleast.core.parser.Rule
        public ParseSpec<R, S> parse(Matcher matcher, Parser<R, ? super Node<R>, S> parser, S s2) {
            throw null;
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(Pattern pattern, Function1<? super Integer, ? extends CharacterStyle> function1) {
            super(pattern, function1);
            m.checkNotNullParameter(pattern, "pattern");
            m.checkNotNullParameter(function1, "styleSpanProvider");
        }
    }
}
