package b.a.t.b.b;

import android.text.style.StyleSpan;
import com.discord.simpleast.core.node.Node;
import com.discord.simpleast.core.node.StyleNode;
import com.discord.simpleast.core.parser.ParseSpec;
import com.discord.simpleast.core.parser.Parser;
import com.discord.simpleast.core.parser.Rule;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/* compiled from: SimpleMarkdownRules.kt */
/* loaded from: classes.dex */
public final class c extends Rule<R, Node<R>, S> {
    public c(e eVar, Pattern pattern) {
        super(pattern);
    }

    @Override // com.discord.simpleast.core.parser.Rule
    public ParseSpec<R, S> parse(Matcher matcher, Parser<R, ? super Node<R>, S> parser, S s2) {
        int i;
        int i2;
        m.checkNotNullParameter(matcher, "matcher");
        m.checkNotNullParameter(parser, "parser");
        String group = matcher.group(2);
        if (group != null) {
            if (group.length() > 0) {
                i2 = matcher.start(2);
                i = matcher.end(2);
                ArrayList arrayList = new ArrayList(1);
                arrayList.add(new StyleSpan(2));
                StyleNode styleNode = new StyleNode(arrayList);
                m.checkNotNullParameter(styleNode, "node");
                return new ParseSpec<>(styleNode, s2, i2, i);
            }
        }
        i2 = matcher.start(1);
        i = matcher.end(1);
        ArrayList arrayList2 = new ArrayList(1);
        arrayList2.add(new StyleSpan(2));
        StyleNode styleNode2 = new StyleNode(arrayList2);
        m.checkNotNullParameter(styleNode2, "node");
        return new ParseSpec<>(styleNode2, s2, i2, i);
    }
}
