package b.a.s;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.net.Uri;
import android.provider.MediaStore;
import androidx.core.app.NotificationCompat;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.time.Clock;
import com.discord.utilities.time.ClockFactory;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.g0.t;
import d0.z.d.m;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ScreenshotContentObserver.kt */
/* loaded from: classes.dex */
public final class b extends ContentObserver {
    public static final a a = new a(null);

    /* renamed from: b  reason: collision with root package name */
    public String f281b;
    public final Logger c;
    public final ContentResolver d;
    public final Clock e;
    public final Function2<Uri, String, Unit> f;

    /* compiled from: ScreenshotContentObserver.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: ScreenshotContentObserver.kt */
    /* renamed from: b.a.s.b$b  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0048b {
        public final String a;

        /* renamed from: b  reason: collision with root package name */
        public final long f282b;
        public final String c;
        public final String d;
        public final Uri e;
        public final long f;

        public C0048b(long j, String str, String str2, Uri uri, long j2) {
            m.checkNotNullParameter(str, "fileName");
            m.checkNotNullParameter(str2, "relativePath");
            m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
            this.f282b = j;
            this.c = str;
            this.d = str2;
            this.e = uri;
            this.f = j2;
            this.a = str2 + MentionUtilsKt.SLASH_CHAR + str;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof C0048b)) {
                return false;
            }
            C0048b bVar = (C0048b) obj;
            return this.f282b == bVar.f282b && m.areEqual(this.c, bVar.c) && m.areEqual(this.d, bVar.d) && m.areEqual(this.e, bVar.e) && this.f == bVar.f;
        }

        public int hashCode() {
            int a = a0.a.a.b.a(this.f282b) * 31;
            String str = this.c;
            int i = 0;
            int hashCode = (a + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.d;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            Uri uri = this.e;
            if (uri != null) {
                i = uri.hashCode();
            }
            return a0.a.a.b.a(this.f) + ((hashCode2 + i) * 31);
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("ScreenshotData(id=");
            R.append(this.f282b);
            R.append(", fileName=");
            R.append(this.c);
            R.append(", relativePath=");
            R.append(this.d);
            R.append(", uri=");
            R.append(this.e);
            R.append(", dateAdded=");
            return b.d.b.a.a.B(R, this.f, ")");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public b(Logger logger, ContentResolver contentResolver, Clock clock, Function2 function2, int i) {
        super(null);
        Clock clock2 = (i & 4) != 0 ? ClockFactory.get() : null;
        m.checkNotNullParameter(logger, "logger");
        m.checkNotNullParameter(contentResolver, "contentResolver");
        m.checkNotNullParameter(clock2, "clock");
        m.checkNotNullParameter(function2, "onScreenshot");
        this.c = logger;
        this.d = contentResolver;
        this.e = clock2;
        this.f = function2;
    }

    /* JADX WARN: Removed duplicated region for block: B:32:0x00c2  */
    /* JADX WARN: Removed duplicated region for block: B:50:? A[RETURN, SYNTHETIC] */
    /* JADX WARN: Type inference failed for: r7v0, types: [java.lang.Throwable, java.lang.Object, java.lang.String] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void a(android.net.Uri r26) {
        /*
            Method dump skipped, instructions count: 286
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.s.b.a(android.net.Uri):void");
    }

    @Override // android.database.ContentObserver
    public void onChange(boolean z2, Uri uri) {
        super.onChange(z2, uri);
        if (uri != null) {
            String uri2 = uri.toString();
            m.checkNotNullExpressionValue(uri2, "uri.toString()");
            String uri3 = MediaStore.Images.Media.EXTERNAL_CONTENT_URI.toString();
            m.checkNotNullExpressionValue(uri3, "MediaStore.Images.Media.…AL_CONTENT_URI.toString()");
            if (t.startsWith$default(uri2, uri3, false, 2, null)) {
                try {
                    a(uri);
                } catch (Exception e) {
                    Logger.e$default(this.c, "Error processing screenshot", e, null, 4, null);
                }
            }
        }
    }
}
