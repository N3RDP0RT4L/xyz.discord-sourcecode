package b.a.j;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import androidx.annotation.MainThread;
import com.discord.floating_view_manager.FloatingViewGravity;
import com.discord.utilities.logging.Logger;
import d0.z.d.m;
import java.lang.ref.WeakReference;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: FloatingViewManager.kt */
/* loaded from: classes.dex */
public final class a {
    public Function1<? super Integer, Unit> a;

    /* renamed from: b  reason: collision with root package name */
    public final Map<Integer, C0032a> f239b = new LinkedHashMap();
    public final Logger c;

    /* compiled from: FloatingViewManager.kt */
    /* renamed from: b.a.j.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0032a {
        public final View a;

        /* renamed from: b  reason: collision with root package name */
        public final ViewGroup f240b;
        public final ViewTreeObserver.OnPreDrawListener c;

        public C0032a(View view, ViewGroup viewGroup, ViewTreeObserver.OnPreDrawListener onPreDrawListener) {
            m.checkNotNullParameter(view, "floatingView");
            m.checkNotNullParameter(viewGroup, "ancestorViewGroup");
            m.checkNotNullParameter(onPreDrawListener, "ancestorPreDrawListener");
            this.a = view;
            this.f240b = viewGroup;
            this.c = onPreDrawListener;
        }
    }

    /* compiled from: FloatingViewManager.kt */
    /* loaded from: classes.dex */
    public static final class b {
        public static WeakReference<a> a;
    }

    public a(Logger logger) {
        m.checkNotNullParameter(logger, "logger");
        this.c = logger;
    }

    public static final void a(a aVar, View view, View view2, FloatingViewGravity floatingViewGravity, int i, int i2) {
        int i3;
        Objects.requireNonNull(aVar);
        int[] iArr = new int[2];
        view2.getLocationInWindow(iArr);
        int i4 = iArr[0];
        int i5 = iArr[1];
        int width = (((view2.getWidth() / 2) + i4) - (view.getWidth() / 2)) + i;
        int ordinal = floatingViewGravity.ordinal();
        if (ordinal == 0) {
            i3 = i5 - view.getHeight();
        } else if (ordinal == 1) {
            i3 = ((view2.getHeight() / 2) + i5) - (view.getHeight() / 2);
        } else if (ordinal == 2) {
            i3 = view2.getHeight() + i5;
        } else {
            throw new NoWhenBranchMatchedException();
        }
        view.setX(width);
        view.setY(i3 + i2);
    }

    @MainThread
    public final void b(int i) {
        C0032a aVar = this.f239b.get(Integer.valueOf(i));
        if (aVar != null) {
            ViewGroup viewGroup = aVar.f240b;
            View view = aVar.a;
            ViewTreeObserver.OnPreDrawListener onPreDrawListener = aVar.c;
            this.f239b.remove(Integer.valueOf(i));
            viewGroup.getViewTreeObserver().removeOnPreDrawListener(onPreDrawListener);
            viewGroup.removeView(view);
            Function1<? super Integer, Unit> function1 = this.a;
            if (function1 != null) {
                function1.invoke(Integer.valueOf(i));
            }
        }
    }
}
