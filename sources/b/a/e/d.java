package b.a.e;

import d0.z.d.m;
import rx.Observable;
import rx.Subscription;
import rx.subjects.BehaviorSubject;
import rx.subjects.Subject;
/* compiled from: Backgrounded.kt */
/* loaded from: classes.dex */
public final class d {
    public static boolean a = true;

    /* renamed from: b  reason: collision with root package name */
    public static final Subject<Boolean, Boolean> f65b;
    public static Subscription c;
    public static final d d = new d();

    static {
        BehaviorSubject l0 = BehaviorSubject.l0(true);
        m.checkNotNullExpressionValue(l0, "BehaviorSubject.create(isBackgrounded)");
        f65b = l0;
    }

    public final Observable<Boolean> a() {
        Observable<Boolean> q = f65b.q();
        m.checkNotNullExpressionValue(q, "emitter.distinctUntilChanged()");
        return q;
    }
}
