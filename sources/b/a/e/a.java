package b.a.e;

import d0.z.d.k;
import java.util.Objects;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.Subscription;
/* compiled from: Backgrounded.kt */
/* loaded from: classes.dex */
public final /* synthetic */ class a extends k implements Function1<Boolean, Unit> {
    public a(d dVar) {
        super(1, dVar, d.class, "emit", "emit(Z)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public Unit invoke(Boolean bool) {
        boolean booleanValue = bool.booleanValue();
        boolean z2 = d.a;
        Objects.requireNonNull((d) this.receiver);
        d.a = booleanValue;
        Subscription subscription = d.c;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        d.f65b.onNext(Boolean.valueOf(booleanValue));
        return Unit.a;
    }
}
