package b.a.r;

import android.net.Uri;
import d0.w.i.a.e;
import d0.w.i.a.k;
import d0.z.d.m;
import f0.x;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineScope;
/* compiled from: SamsungConnect.kt */
@e(c = "com.discord.samsung.SamsungConnect$postSamsungAuthorizeCallback$2", f = "SamsungConnect.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class b extends k implements Function2<CoroutineScope, Continuation<? super Uri>, Object> {
    public final /* synthetic */ x $okHttpClient;
    public final /* synthetic */ String $samsungAuthCode;
    public final /* synthetic */ String $state;
    public final /* synthetic */ String $url;
    public int label;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public b(String str, String str2, String str3, x xVar, Continuation continuation) {
        super(2, continuation);
        this.$state = str;
        this.$samsungAuthCode = str2;
        this.$url = str3;
        this.$okHttpClient = xVar;
    }

    @Override // d0.w.i.a.a
    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        m.checkNotNullParameter(continuation, "completion");
        return new b(this.$state, this.$samsungAuthCode, this.$url, this.$okHttpClient, continuation);
    }

    @Override // kotlin.jvm.functions.Function2
    public final Object invoke(CoroutineScope coroutineScope, Continuation<? super Uri> continuation) {
        return ((b) create(coroutineScope, continuation)).invokeSuspend(Unit.a);
    }

    /* JADX WARN: Code restructure failed: missing block: B:27:0x00f2, code lost:
        if (r2 == false) goto L29;
     */
    @Override // d0.w.i.a.a
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.lang.Object invokeSuspend(java.lang.Object r13) {
        /*
            Method dump skipped, instructions count: 296
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.r.b.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
