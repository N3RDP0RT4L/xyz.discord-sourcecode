package b.a.r;

import android.net.Uri;
import d0.w.i.a.e;
import d0.w.i.a.k;
import d0.z.d.m;
import f0.x;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineScope;
/* compiled from: SamsungConnect.kt */
@e(c = "com.discord.samsung.SamsungConnect$getSamsungAuthorizeCallback$2", f = "SamsungConnect.kt", l = {}, m = "invokeSuspend")
/* loaded from: classes.dex */
public final class a extends k implements Function2<CoroutineScope, Continuation<? super Uri>, Object> {
    public final /* synthetic */ x $okHttpClient;
    public final /* synthetic */ String $url;
    public int label;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public a(String str, x xVar, Continuation continuation) {
        super(2, continuation);
        this.$url = str;
        this.$okHttpClient = xVar;
    }

    @Override // d0.w.i.a.a
    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        m.checkNotNullParameter(continuation, "completion");
        return new a(this.$url, this.$okHttpClient, continuation);
    }

    @Override // kotlin.jvm.functions.Function2
    public final Object invoke(CoroutineScope coroutineScope, Continuation<? super Uri> continuation) {
        Continuation<? super Uri> continuation2 = continuation;
        m.checkNotNullParameter(continuation2, "completion");
        return new a(this.$url, this.$okHttpClient, continuation2).invokeSuspend(Unit.a);
    }

    /* JADX WARN: Code restructure failed: missing block: B:23:0x0061, code lost:
        if (r4 == false) goto L25;
     */
    @Override // d0.w.i.a.a
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.lang.Object invokeSuspend(java.lang.Object r7) {
        /*
            r6 = this;
            d0.w.h.c.getCOROUTINE_SUSPENDED()
            int r0 = r6.label
            if (r0 != 0) goto L83
            d0.l.throwOnFailure(r7)
            okhttp3.Request$a r7 = new okhttp3.Request$a
            r7.<init>()
            java.lang.String r0 = "GET"
            r1 = 0
            r7.c(r0, r1)
            java.lang.String r0 = r6.$url
            r7.f(r0)
            okhttp3.Request r7 = r7.a()
            f0.x r0 = r6.$okHttpClient
            f0.e r7 = r0.b(r7)
            f0.e0.g.e r7 = (f0.e0.g.e) r7
            okhttp3.Response r7 = r7.execute()
            r0 = 2
            java.lang.String r2 = "Location"
            java.lang.String r2 = okhttp3.Response.a(r7, r2, r1, r0)
            if (r2 == 0) goto L38
            android.net.Uri r2 = android.net.Uri.parse(r2)
            goto L39
        L38:
            r2 = r1
        L39:
            java.lang.String r3 = "error"
            r4 = 0
            if (r2 == 0) goto L49
            java.lang.String r5 = r2.getQuery()
            if (r5 == 0) goto L49
            boolean r0 = d0.g0.w.contains$default(r5, r3, r4, r0, r1)
            goto L4a
        L49:
            r0 = 0
        L4a:
            if (r0 != 0) goto L64
            boolean r0 = r7.b()
            if (r0 != 0) goto L63
            int r7 = r7.m
            r0 = 307(0x133, float:4.3E-43)
            if (r7 == r0) goto L60
            r0 = 308(0x134, float:4.32E-43)
            if (r7 == r0) goto L60
            switch(r7) {
                case 300: goto L60;
                case 301: goto L60;
                case 302: goto L60;
                case 303: goto L60;
                default: goto L5f;
            }
        L5f:
            goto L61
        L60:
            r4 = 1
        L61:
            if (r4 == 0) goto L64
        L63:
            return r2
        L64:
            com.discord.samsung.SamsungConnect$SamsungCallbackException r7 = new com.discord.samsung.SamsungConnect$SamsungCallbackException
            if (r2 == 0) goto L6f
            java.lang.String r0 = r2.getQueryParameter(r3)
            if (r0 == 0) goto L6f
            goto L72
        L6f:
            java.lang.String r0 = "unknown"
        L72:
            java.lang.String r3 = "location?.getQueryParameter(\"error\") ?: \"unknown\""
            d0.z.d.m.checkNotNullExpressionValue(r0, r3)
            if (r2 == 0) goto L7f
            java.lang.String r1 = "error_description"
            java.lang.String r1 = r2.getQueryParameter(r1)
        L7f:
            r7.<init>(r0, r1)
            throw r7
        L83:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r0)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.r.a.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
