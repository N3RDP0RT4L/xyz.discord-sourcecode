package b.a.r;

import android.content.Intent;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import com.discord.samsung.SamsungConnectActivity;
import d0.z.d.m;
import kotlin.jvm.functions.Function1;
/* compiled from: SamsungConnectActivity.kt */
/* loaded from: classes.dex */
public final class c<O> implements ActivityResultCallback<ActivityResult> {
    public final /* synthetic */ Function1 a;

    public c(Function1 function1) {
        this.a = function1;
    }

    @Override // androidx.activity.result.ActivityResultCallback
    public void onActivityResult(ActivityResult activityResult) {
        ActivityResult activityResult2 = activityResult;
        m.checkNotNullExpressionValue(activityResult2, "activityResult");
        int i = 0;
        if (activityResult2.getResultCode() == 500) {
            Function1 function1 = this.a;
            Intent data = activityResult2.getData();
            if (data != null) {
                i = data.getIntExtra("com.discord.samsung.intent.extra.ATTEMPT_COUNT", 0);
            }
            function1.invoke(new SamsungConnectActivity.Result.Failure(true, i));
            return;
        }
        Intent data2 = activityResult2.getData();
        if (data2 == null) {
            this.a.invoke(new SamsungConnectActivity.Result.Failure(false, 0, 2));
            return;
        }
        Function1 function12 = this.a;
        String stringExtra = data2.getStringExtra("com.discord.samsung.intent.extra.AUTH_CODE");
        String str = "";
        if (stringExtra == null) {
            stringExtra = str;
        }
        String stringExtra2 = data2.getStringExtra("com.discord.samsung.intent.extra.SERVER_URL");
        if (stringExtra2 != null) {
            str = stringExtra2;
        }
        function12.invoke(new SamsungConnectActivity.Result.Success(stringExtra, str));
    }
}
