package b.a.i;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.textfield.TextInputLayout;
/* compiled from: ViewServerSettingsIconNameBinding.java */
/* loaded from: classes.dex */
public final class f3 implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f110b;
    @NonNull
    public final TextInputLayout c;
    @NonNull
    public final SimpleDraweeView d;
    @NonNull
    public final TextView e;
    @NonNull
    public final TextView f;

    public f3(@NonNull ConstraintLayout constraintLayout, @NonNull TextView textView, @NonNull TextInputLayout textInputLayout, @NonNull SimpleDraweeView simpleDraweeView, @NonNull ConstraintLayout constraintLayout2, @NonNull TextView textView2, @NonNull TextView textView3) {
        this.a = constraintLayout;
        this.f110b = textView;
        this.c = textInputLayout;
        this.d = simpleDraweeView;
        this.e = textView2;
        this.f = textView3;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
