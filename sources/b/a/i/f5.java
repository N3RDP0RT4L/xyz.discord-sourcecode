package b.a.i;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
/* compiled from: WidgetHomePanelCenterContentUnreadBinding.java */
/* loaded from: classes.dex */
public final class f5 implements ViewBinding {
    @NonNull
    public final TextView a;

    public f5(@NonNull TextView textView, @NonNull TextView textView2) {
        this.a = textView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
