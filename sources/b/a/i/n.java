package b.a.i;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
/* compiled from: ExpressionPickerCategorySelectionOverlineViewBinding.java */
/* loaded from: classes.dex */
public final class n implements ViewBinding {
    @NonNull
    public final View a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final View f160b;

    public n(@NonNull View view, @NonNull View view2) {
        this.a = view;
        this.f160b = view2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
