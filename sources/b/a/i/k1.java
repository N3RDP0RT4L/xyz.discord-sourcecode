package b.a.i;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.views.UsernameView;
import com.facebook.drawee.span.SimpleDraweeSpanTextView;
import com.facebook.drawee.view.SimpleDraweeView;
/* compiled from: SettingsMemberViewBinding.java */
/* loaded from: classes.dex */
public final class k1 implements ViewBinding {
    @NonNull
    public final View a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final SimpleDraweeView f142b;
    @NonNull
    public final SimpleDraweeSpanTextView c;
    @NonNull
    public final UsernameView d;

    public k1(@NonNull View view, @NonNull SimpleDraweeView simpleDraweeView, @NonNull SimpleDraweeSpanTextView simpleDraweeSpanTextView, @NonNull UsernameView usernameView) {
        this.a = view;
        this.f142b = simpleDraweeView;
        this.c = simpleDraweeSpanTextView;
        this.d = usernameView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
