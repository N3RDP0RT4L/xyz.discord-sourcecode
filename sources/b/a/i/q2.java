package b.a.i;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
/* compiled from: ViewPhoneOrEmailInputBinding.java */
/* loaded from: classes.dex */
public final class q2 implements ViewBinding {
    @NonNull
    public final View a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextInputLayout f181b;
    @NonNull
    public final TextInputEditText c;
    @NonNull
    public final TextInputLayout d;

    public q2(@NonNull View view, @NonNull TextInputLayout textInputLayout, @NonNull TextInputEditText textInputEditText, @NonNull TextInputLayout textInputLayout2) {
        this.a = view;
        this.f181b = textInputLayout;
        this.c = textInputEditText;
        this.d = textInputLayout2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
