package b.a.i;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.views.StatusView;
/* compiled from: ViewToolbarTitleBinding.java */
/* loaded from: classes.dex */
public final class x3 implements ViewBinding {
    @NonNull
    public final View a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ImageView f224b;
    @NonNull
    public final StatusView c;
    @NonNull
    public final TextView d;
    @NonNull
    public final TextView e;
    @NonNull
    public final ImageView f;

    public x3(@NonNull View view, @NonNull ImageView imageView, @NonNull StatusView statusView, @NonNull TextView textView, @NonNull TextView textView2, @NonNull ImageView imageView2) {
        this.a = view;
        this.f224b = imageView;
        this.c = statusView;
        this.d = textView;
        this.e = textView2;
        this.f = imageView2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
