package b.a.i;

import android.view.View;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
/* compiled from: SparkleViewBinding.java */
/* loaded from: classes.dex */
public final class o1 implements ViewBinding {
    @NonNull
    public final View a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ImageView f166b;

    public o1(@NonNull View view, @NonNull ImageView imageView) {
        this.a = view;
        this.f166b = imageView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
