package b.a.i;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
/* compiled from: ViewSimpleSpinnerDropdownItemBinding.java */
/* loaded from: classes.dex */
public final class q3 implements ViewBinding {
    @NonNull
    public final TextView a;

    public q3(@NonNull TextView textView) {
        this.a = textView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
