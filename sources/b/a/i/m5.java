package b.a.i;

import android.view.View;
import android.widget.RadioButton;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
/* compiled from: WidgetMemberVerificationMultipleChoiceRadioItemBinding.java */
/* loaded from: classes.dex */
public final class m5 implements ViewBinding {
    @NonNull
    public final RadioButton a;

    public m5(@NonNull RadioButton radioButton) {
        this.a = radioButton;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
