package b.a.i;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.views.LoadingButton;
/* compiled from: LayoutContactSyncSuggestionsBinding.java */
/* loaded from: classes.dex */
public final class k0 implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final RecyclerView f141b;
    @NonNull
    public final LoadingButton c;

    public k0(@NonNull ConstraintLayout constraintLayout, @NonNull RecyclerView recyclerView, @NonNull LoadingButton loadingButton, @NonNull LinkifiedTextView linkifiedTextView, @NonNull TextView textView) {
        this.a = constraintLayout;
        this.f141b = recyclerView;
        this.c = loadingButton;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
