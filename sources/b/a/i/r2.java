package b.a.i;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.views.CutoutView;
import com.facebook.drawee.view.SimpleDraweeView;
/* compiled from: ViewPileItemBinding.java */
/* loaded from: classes.dex */
public final class r2 implements ViewBinding {
    @NonNull
    public final CutoutView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final SimpleDraweeView f186b;
    @NonNull
    public final SimpleDraweeView c;
    @NonNull
    public final TextView d;

    public r2(@NonNull CutoutView cutoutView, @NonNull SimpleDraweeView simpleDraweeView, @NonNull SimpleDraweeView simpleDraweeView2, @NonNull TextView textView) {
        this.a = cutoutView;
        this.f186b = simpleDraweeView;
        this.c = simpleDraweeView2;
        this.d = textView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
