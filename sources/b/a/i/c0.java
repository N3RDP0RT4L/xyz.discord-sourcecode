package b.a.i;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.viewbinding.ViewBinding;
/* compiled from: InviteSettingsRadioButtonBinding.java */
/* loaded from: classes.dex */
public final class c0 implements ViewBinding {
    @NonNull
    public final AppCompatRadioButton a;

    public c0(@NonNull AppCompatRadioButton appCompatRadioButton) {
        this.a = appCompatRadioButton;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
