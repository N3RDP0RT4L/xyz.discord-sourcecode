package b.a.i;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.views.typing.TypingDot;
/* compiled from: TypingDotsViewBinding.java */
/* loaded from: classes.dex */
public final class r1 implements ViewBinding {
    @NonNull
    public final View a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TypingDot f185b;
    @NonNull
    public final TypingDot c;
    @NonNull
    public final TypingDot d;

    public r1(@NonNull View view, @NonNull TypingDot typingDot, @NonNull TypingDot typingDot2, @NonNull TypingDot typingDot3) {
        this.a = view;
        this.f185b = typingDot;
        this.c = typingDot2;
        this.d = typingDot3;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
