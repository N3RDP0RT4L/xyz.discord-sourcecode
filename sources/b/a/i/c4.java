package b.a.i;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.facebook.drawee.span.SimpleDraweeSpanTextView;
/* compiled from: ViewUsernameBinding.java */
/* loaded from: classes.dex */
public final class c4 implements ViewBinding {
    @NonNull
    public final View a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f92b;
    @NonNull
    public final SimpleDraweeSpanTextView c;

    public c4(@NonNull View view, @NonNull TextView textView, @NonNull SimpleDraweeSpanTextView simpleDraweeSpanTextView) {
        this.a = view;
        this.f92b = textView;
        this.c = simpleDraweeSpanTextView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
