package b.a.i;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
/* compiled from: ViewChatActionItemBinding.java */
/* loaded from: classes.dex */
public final class z1 implements ViewBinding {
    @NonNull
    public final View a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ImageView f235b;
    @NonNull
    public final TextView c;

    public z1(@NonNull View view, @NonNull ImageView imageView, @NonNull TextView textView) {
        this.a = view;
        this.f235b = imageView;
        this.c = textView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
