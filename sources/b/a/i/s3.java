package b.a.i;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.views.VoiceUserView;
import com.discord.views.calls.SpeakerPulseView;
/* compiled from: ViewStageChannelSpeakerBinding.java */
/* loaded from: classes.dex */
public final class s3 implements ViewBinding {
    @NonNull
    public final View a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final VoiceUserView f194b;
    @NonNull
    public final ImageView c;
    @NonNull
    public final ImageView d;
    @NonNull
    public final ImageView e;
    @NonNull
    public final ImageView f;
    @NonNull
    public final TextView g;
    @NonNull
    public final SpeakerPulseView h;

    public s3(@NonNull View view, @NonNull VoiceUserView voiceUserView, @NonNull ImageView imageView, @NonNull ImageView imageView2, @NonNull ImageView imageView3, @NonNull ImageView imageView4, @NonNull TextView textView, @NonNull SpeakerPulseView speakerPulseView) {
        this.a = view;
        this.f194b = voiceUserView;
        this.c = imageView;
        this.d = imageView2;
        this.e = imageView3;
        this.f = imageView4;
        this.g = textView;
        this.h = speakerPulseView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
