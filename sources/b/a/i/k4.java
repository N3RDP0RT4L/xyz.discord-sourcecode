package b.a.i;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
/* compiled from: WidgetCallFullscreenAudioShareWarningBinding.java */
/* loaded from: classes.dex */
public final class k4 implements ViewBinding {
    @NonNull
    public final View a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final View f145b;

    public k4(@NonNull View view, @NonNull View view2) {
        this.a = view;
        this.f145b = view2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
