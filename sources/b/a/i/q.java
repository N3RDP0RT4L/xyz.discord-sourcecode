package b.a.i;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.button.MaterialButton;
/* compiled from: GuildBoostActivatedDialogBinding.java */
/* loaded from: classes.dex */
public final class q implements ViewBinding {
    @NonNull
    public final LinearLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f178b;
    @NonNull
    public final TextView c;
    @NonNull
    public final LottieAnimationView d;
    @NonNull
    public final MaterialButton e;

    public q(@NonNull LinearLayout linearLayout, @NonNull TextView textView, @NonNull TextView textView2, @NonNull LottieAnimationView lottieAnimationView, @NonNull MaterialButton materialButton) {
        this.a = linearLayout;
        this.f178b = textView;
        this.c = textView2;
        this.d = lottieAnimationView;
        this.e = materialButton;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
