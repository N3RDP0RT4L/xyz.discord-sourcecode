package b.a.i;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.views.stages.SpeakersRecyclerView;
/* compiled from: ViewStageCardSpeakersBinding.java */
/* loaded from: classes.dex */
public final class r3 implements ViewBinding {
    @NonNull
    public final View a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final SpeakersRecyclerView f187b;

    public r3(@NonNull View view, @NonNull SpeakersRecyclerView speakersRecyclerView) {
        this.a = view;
        this.f187b = speakersRecyclerView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
