package b.a.i;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.discord.views.LoadingButton;
import com.discord.views.steps.StepsProgressIndicatorView;
import com.google.android.material.button.MaterialButton;
/* compiled from: ViewStepsBinding.java */
/* loaded from: classes.dex */
public final class u3 implements ViewBinding {
    @NonNull
    public final RelativeLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final LinearLayout f206b;
    @NonNull
    public final TextView c;
    @NonNull
    public final ImageView d;
    @NonNull
    public final LoadingButton e;
    @NonNull
    public final MaterialButton f;
    @NonNull
    public final StepsProgressIndicatorView g;
    @NonNull
    public final TextView h;
    @NonNull
    public final ViewPager2 i;

    public u3(@NonNull RelativeLayout relativeLayout, @NonNull LinearLayout linearLayout, @NonNull TextView textView, @NonNull ImageView imageView, @NonNull LoadingButton loadingButton, @NonNull MaterialButton materialButton, @NonNull StepsProgressIndicatorView stepsProgressIndicatorView, @NonNull TextView textView2, @NonNull ViewPager2 viewPager2) {
        this.a = relativeLayout;
        this.f206b = linearLayout;
        this.c = textView;
        this.d = imageView;
        this.e = loadingButton;
        this.f = materialButton;
        this.g = stepsProgressIndicatorView;
        this.h = textView2;
        this.i = viewPager2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
