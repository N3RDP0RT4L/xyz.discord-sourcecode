package b.a.i;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.button.MaterialButton;
/* compiled from: ImageUploadDialogBinding.java */
/* loaded from: classes.dex */
public final class z implements ViewBinding {
    @NonNull
    public final LinearLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final MaterialButton f233b;
    @NonNull
    public final TextView c;
    @NonNull
    public final SimpleDraweeView d;
    @NonNull
    public final MaterialButton e;

    public z(@NonNull LinearLayout linearLayout, @NonNull MaterialButton materialButton, @NonNull TextView textView, @NonNull SimpleDraweeView simpleDraweeView, @NonNull MaterialButton materialButton2) {
        this.a = linearLayout;
        this.f233b = materialButton;
        this.c = textView;
        this.d = simpleDraweeView;
        this.e = materialButton2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
