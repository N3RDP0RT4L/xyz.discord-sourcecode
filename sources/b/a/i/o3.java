package b.a.i;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.switchmaterial.SwitchMaterial;
/* compiled from: ViewSettingSwitchBinding.java */
/* loaded from: classes.dex */
public final class o3 implements ViewBinding {
    @NonNull
    public final View a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final SwitchMaterial f168b;
    @NonNull
    public final ConstraintLayout c;

    public o3(@NonNull View view, @NonNull SwitchMaterial switchMaterial, @NonNull ConstraintLayout constraintLayout) {
        this.a = view;
        this.f168b = switchMaterial;
        this.c = constraintLayout;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
