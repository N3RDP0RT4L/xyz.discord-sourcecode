package b.a.i;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.chip.Chip;
/* compiled from: ViewGuildBoostConfirmationBinding.java */
/* loaded from: classes.dex */
public final class h2 implements ViewBinding {
    @NonNull
    public final View a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final SimpleDraweeView f122b;
    @NonNull
    public final TextView c;
    @NonNull
    public final Chip d;
    @NonNull
    public final Chip e;
    @NonNull
    public final ImageView f;

    public h2(@NonNull View view, @NonNull SimpleDraweeView simpleDraweeView, @NonNull TextView textView, @NonNull Chip chip, @NonNull Chip chip2, @NonNull ImageView imageView) {
        this.a = view;
        this.f122b = simpleDraweeView;
        this.c = textView;
        this.d = chip;
        this.e = chip2;
        this.f = imageView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
