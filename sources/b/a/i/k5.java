package b.a.i;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.viewbinding.ViewBinding;
/* compiled from: WidgetHubEmailFlowWaitlistBinding.java */
/* loaded from: classes.dex */
public final class k5 implements ViewBinding {
    @NonNull
    public final NestedScrollView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f146b;

    public k5(@NonNull NestedScrollView nestedScrollView, @NonNull TextView textView) {
        this.a = nestedScrollView;
        this.f146b = textView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
