package b.a.i;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
/* compiled from: ViewPremiumGuildProgressBinding.java */
/* loaded from: classes.dex */
public final class t2 implements ViewBinding {
    @NonNull
    public final FrameLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ImageView f200b;
    @NonNull
    public final TextView c;
    @NonNull
    public final ProgressBar d;

    public t2(@NonNull FrameLayout frameLayout, @NonNull ImageView imageView, @NonNull TextView textView, @NonNull ProgressBar progressBar) {
        this.a = frameLayout;
        this.f200b = imageView;
        this.c = textView;
        this.d = progressBar;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
