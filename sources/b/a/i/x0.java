package b.a.i;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.views.VoiceUserView;
/* compiled from: OverlayMenuVoiceMembersItemBinding.java */
/* loaded from: classes.dex */
public final class x0 implements ViewBinding {
    @NonNull
    public final VoiceUserView a;

    public x0(@NonNull VoiceUserView voiceUserView) {
        this.a = voiceUserView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
