package b.a.i;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.textview.MaterialTextView;
/* compiled from: BottomSheetSimpleSelectorItemBinding.java */
/* loaded from: classes.dex */
public final class d implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final MaterialTextView f94b;
    @NonNull
    public final SimpleDraweeView c;
    @NonNull
    public final MaterialTextView d;

    public d(@NonNull ConstraintLayout constraintLayout, @NonNull MaterialTextView materialTextView, @NonNull SimpleDraweeView simpleDraweeView, @NonNull MaterialTextView materialTextView2) {
        this.a = constraintLayout;
        this.f94b = materialTextView;
        this.c = simpleDraweeView;
        this.d = materialTextView2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
