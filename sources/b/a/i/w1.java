package b.a.i;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.button.MaterialButton;
/* compiled from: ViewActiveSubscriptionBinding.java */
/* loaded from: classes.dex */
public final class w1 implements ViewBinding {
    @NonNull
    public final FrameLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f217b;
    @NonNull
    public final ImageView c;
    @NonNull
    public final ImageView d;
    @NonNull
    public final ImageView e;
    @NonNull
    public final TextView f;
    @NonNull
    public final MaterialButton g;
    @NonNull
    public final ProgressBar h;
    @NonNull
    public final MaterialButton i;

    public w1(@NonNull FrameLayout frameLayout, @NonNull TextView textView, @NonNull ImageView imageView, @NonNull ImageView imageView2, @NonNull ImageView imageView3, @NonNull TextView textView2, @NonNull MaterialButton materialButton, @NonNull ProgressBar progressBar, @NonNull MaterialButton materialButton2) {
        this.a = frameLayout;
        this.f217b = textView;
        this.c = imageView;
        this.d = imageView2;
        this.e = imageView3;
        this.f = textView2;
        this.g = materialButton;
        this.h = progressBar;
        this.i = materialButton2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
