package b.a.i;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.utilities.view.text.SimpleDraweeSpanTextView;
/* compiled from: ReactionViewBinding.java */
/* loaded from: classes.dex */
public final class g1 implements ViewBinding {
    @NonNull
    public final LinearLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f114b;
    @NonNull
    public final TextView c;
    @NonNull
    public final TextSwitcher d;
    @NonNull
    public final SimpleDraweeSpanTextView e;

    public g1(@NonNull LinearLayout linearLayout, @NonNull TextView textView, @NonNull TextView textView2, @NonNull TextSwitcher textSwitcher, @NonNull SimpleDraweeSpanTextView simpleDraweeSpanTextView) {
        this.a = linearLayout;
        this.f114b = textView;
        this.c = textView2;
        this.d = textSwitcher;
        this.e = simpleDraweeSpanTextView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
