package b.a.i;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.discord.views.StreamPreviewView;
import com.facebook.drawee.view.SimpleDraweeView;
/* compiled from: WidgetStreamRichPresenceBinding.java */
/* loaded from: classes.dex */
public final class y5 implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final LinearLayout f232b;
    @NonNull
    public final TextView c;
    @NonNull
    public final TextView d;
    @NonNull
    public final SimpleDraweeView e;
    @NonNull
    public final TextView f;
    @NonNull
    public final TextView g;
    @NonNull
    public final TextView h;
    @NonNull
    public final StreamPreviewView i;

    public y5(@NonNull ConstraintLayout constraintLayout, @NonNull LinearLayout linearLayout, @NonNull TextView textView, @NonNull TextView textView2, @NonNull SimpleDraweeView simpleDraweeView, @NonNull TextView textView3, @NonNull TextView textView4, @NonNull TextView textView5, @NonNull StreamPreviewView streamPreviewView) {
        this.a = constraintLayout;
        this.f232b = linearLayout;
        this.c = textView;
        this.d = textView2;
        this.e = simpleDraweeView;
        this.f = textView3;
        this.g = textView4;
        this.h = textView5;
        this.i = streamPreviewView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
