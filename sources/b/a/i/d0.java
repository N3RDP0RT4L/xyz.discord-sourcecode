package b.a.i;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.viewbinding.ViewBinding;
import com.discord.views.premium.AccountCreditView;
/* compiled from: LayoutAccountCreditsListBinding.java */
/* loaded from: classes.dex */
public final class d0 implements ViewBinding {
    @NonNull
    public final CardView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final AccountCreditView f95b;
    @NonNull
    public final AccountCreditView c;
    @NonNull
    public final View d;

    public d0(@NonNull CardView cardView, @NonNull AccountCreditView accountCreditView, @NonNull AccountCreditView accountCreditView2, @NonNull View view) {
        this.a = cardView;
        this.f95b = accountCreditView;
        this.c = accountCreditView2;
        this.d = view;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
