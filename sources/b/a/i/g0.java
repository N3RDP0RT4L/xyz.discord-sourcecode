package b.a.i;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.google.android.material.button.MaterialButton;
/* compiled from: LayoutAgeVerifyUnderageBinding.java */
/* loaded from: classes.dex */
public final class g0 implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final MaterialButton f113b;
    @NonNull
    public final LinkifiedTextView c;
    @NonNull
    public final TextView d;

    public g0(@NonNull ConstraintLayout constraintLayout, @NonNull MaterialButton materialButton, @NonNull LinkifiedTextView linkifiedTextView, @NonNull TextView textView, @NonNull TextView textView2) {
        this.a = constraintLayout;
        this.f113b = materialButton;
        this.c = linkifiedTextView;
        this.d = textView2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
