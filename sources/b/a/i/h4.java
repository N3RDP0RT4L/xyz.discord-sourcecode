package b.a.i;

import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
/* compiled from: VolumeSliderViewBinding.java */
/* loaded from: classes.dex */
public final class h4 implements ViewBinding {
    @NonNull
    public final View a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ImageView f124b;
    @NonNull
    public final ImageView c;
    @NonNull
    public final SeekBar d;

    public h4(@NonNull View view, @NonNull ImageView imageView, @NonNull ImageView imageView2, @NonNull SeekBar seekBar) {
        this.a = view;
        this.f124b = imageView;
        this.c = imageView2;
        this.d = seekBar;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
