package b.a.i;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.textfield.TextInputLayout;
import xyz.discord.R;
/* compiled from: OverlayVoiceChannelSelectorBinding.java */
/* loaded from: classes.dex */
public final class z0 implements ViewBinding {
    @NonNull
    public final CardView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ImageView f234b;
    @NonNull
    public final CardView c;
    @NonNull
    public final TextView d;
    @NonNull
    public final TextInputLayout e;
    @NonNull
    public final RecyclerView f;

    public z0(@NonNull CardView cardView, @NonNull ImageView imageView, @NonNull CardView cardView2, @NonNull TextView textView, @NonNull TextInputLayout textInputLayout, @NonNull RecyclerView recyclerView, @NonNull TextView textView2) {
        this.a = cardView;
        this.f234b = imageView;
        this.c = cardView2;
        this.d = textView;
        this.e = textInputLayout;
        this.f = recyclerView;
    }

    @NonNull
    public static z0 a(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, boolean z2) {
        View inflate = layoutInflater.inflate(R.layout.overlay_voice_channel_selector, viewGroup, false);
        if (z2) {
            viewGroup.addView(inflate);
        }
        int i = R.id.close;
        ImageView imageView = (ImageView) inflate.findViewById(R.id.close);
        if (imageView != null) {
            CardView cardView = (CardView) inflate;
            i = R.id.empty_results;
            TextView textView = (TextView) inflate.findViewById(R.id.empty_results);
            if (textView != null) {
                i = R.id.overlay_channel_search;
                TextInputLayout textInputLayout = (TextInputLayout) inflate.findViewById(R.id.overlay_channel_search);
                if (textInputLayout != null) {
                    i = R.id.results_rv;
                    RecyclerView recyclerView = (RecyclerView) inflate.findViewById(R.id.results_rv);
                    if (recyclerView != null) {
                        i = R.id.test;
                        TextView textView2 = (TextView) inflate.findViewById(R.id.test);
                        if (textView2 != null) {
                            return new z0(cardView, imageView, cardView, textView, textInputLayout, recyclerView, textView2);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
