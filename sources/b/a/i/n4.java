package b.a.i;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.button.MaterialButton;
import xyz.discord.R;
/* compiled from: WidgetChannelSettingsDeleteBinding.java */
/* loaded from: classes.dex */
public final class n4 implements ViewBinding {
    @NonNull
    public final LinearLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f164b;
    @NonNull
    public final MaterialButton c;
    @NonNull
    public final MaterialButton d;
    @NonNull
    public final TextView e;

    public n4(@NonNull LinearLayout linearLayout, @NonNull TextView textView, @NonNull MaterialButton materialButton, @NonNull MaterialButton materialButton2, @NonNull TextView textView2) {
        this.a = linearLayout;
        this.f164b = textView;
        this.c = materialButton;
        this.d = materialButton2;
        this.e = textView2;
    }

    @NonNull
    public static n4 a(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, boolean z2) {
        View inflate = layoutInflater.inflate(R.layout.widget_channel_settings_delete, (ViewGroup) null, false);
        if (!z2) {
            int i = R.id.channel_settings_delete_body;
            TextView textView = (TextView) inflate.findViewById(R.id.channel_settings_delete_body);
            if (textView != null) {
                i = R.id.channel_settings_delete_cancel;
                MaterialButton materialButton = (MaterialButton) inflate.findViewById(R.id.channel_settings_delete_cancel);
                if (materialButton != null) {
                    i = R.id.channel_settings_delete_confirm;
                    MaterialButton materialButton2 = (MaterialButton) inflate.findViewById(R.id.channel_settings_delete_confirm);
                    if (materialButton2 != null) {
                        i = R.id.channel_settings_delete_title;
                        TextView textView2 = (TextView) inflate.findViewById(R.id.channel_settings_delete_title);
                        if (textView2 != null) {
                            return new n4((LinearLayout) inflate, textView, materialButton, materialButton2, textView2);
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
        }
        throw null;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
