package b.a.i;

import android.view.View;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.switchmaterial.SwitchMaterial;
/* compiled from: WidgetRemoteAuthPendingLoginBinding.java */
/* loaded from: classes.dex */
public final class r5 implements ViewBinding {
    @NonNull
    public final LinearLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final MaterialButton f189b;
    @NonNull
    public final MaterialButton c;
    @NonNull
    public final SwitchMaterial d;

    public r5(@NonNull LinearLayout linearLayout, @NonNull MaterialButton materialButton, @NonNull MaterialButton materialButton2, @NonNull SwitchMaterial switchMaterial) {
        this.a = linearLayout;
        this.f189b = materialButton;
        this.c = materialButton2;
        this.d = switchMaterial;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
