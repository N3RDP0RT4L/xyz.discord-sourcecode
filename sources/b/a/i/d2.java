package b.a.i;

import android.view.View;
import android.widget.Space;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
/* compiled from: ViewCodeVerificationSpaceBinding.java */
/* loaded from: classes.dex */
public final class d2 implements ViewBinding {
    @NonNull
    public final Space a;

    public d2(@NonNull Space space) {
        this.a = space;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
