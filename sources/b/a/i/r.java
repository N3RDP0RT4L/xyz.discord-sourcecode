package b.a.i;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.app.AppViewFlipper;
import com.discord.views.LoadingButton;
import com.google.android.material.button.MaterialButton;
/* compiled from: GuildBoostCancelDialogBinding.java */
/* loaded from: classes.dex */
public final class r implements ViewBinding {
    @NonNull
    public final LinearLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f184b;
    @NonNull
    public final LoadingButton c;
    @NonNull
    public final TextView d;
    @NonNull
    public final AppViewFlipper e;
    @NonNull
    public final TextView f;
    @NonNull
    public final MaterialButton g;

    public r(@NonNull LinearLayout linearLayout, @NonNull ProgressBar progressBar, @NonNull TextView textView, @NonNull LoadingButton loadingButton, @NonNull TextView textView2, @NonNull AppViewFlipper appViewFlipper, @NonNull TextView textView3, @NonNull MaterialButton materialButton, @NonNull LinearLayout linearLayout2) {
        this.a = linearLayout;
        this.f184b = textView;
        this.c = loadingButton;
        this.d = textView2;
        this.e = appViewFlipper;
        this.f = textView3;
        this.g = materialButton;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
