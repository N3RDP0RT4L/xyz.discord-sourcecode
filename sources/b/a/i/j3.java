package b.a.i;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
/* compiled from: ViewServerSettingsUploadBannerBinding.java */
/* loaded from: classes.dex */
public final class j3 implements ViewBinding {
    @NonNull
    public final LinearLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final i3 f137b;
    @NonNull
    public final FrameLayout c;
    @NonNull
    public final TextView d;
    @NonNull
    public final SimpleDraweeView e;
    @NonNull
    public final FloatingActionButton f;
    @NonNull
    public final LinkifiedTextView g;
    @NonNull
    public final TextView h;
    @NonNull
    public final TextView i;

    public j3(@NonNull LinearLayout linearLayout, @NonNull i3 i3Var, @NonNull FrameLayout frameLayout, @NonNull TextView textView, @NonNull SimpleDraweeView simpleDraweeView, @NonNull FloatingActionButton floatingActionButton, @NonNull LinkifiedTextView linkifiedTextView, @NonNull TextView textView2, @NonNull TextView textView3) {
        this.a = linearLayout;
        this.f137b = i3Var;
        this.c = frameLayout;
        this.d = textView;
        this.e = simpleDraweeView;
        this.f = floatingActionButton;
        this.g = linkifiedTextView;
        this.h = textView2;
        this.i = textView3;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
