package b.a.i;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.views.calls.VideoCallParticipantView;
/* compiled from: VideoCallGridItemBinding.java */
/* loaded from: classes.dex */
public final class u1 implements ViewBinding {
    @NonNull
    public final VideoCallParticipantView a;

    public u1(@NonNull VideoCallParticipantView videoCallParticipantView) {
        this.a = videoCallParticipantView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
