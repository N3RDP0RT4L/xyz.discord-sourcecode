package b.a.i;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.views.LoadingButton;
import com.google.android.material.button.MaterialButton;
/* compiled from: LayoutAgeVerifyConfirmBinding.java */
/* loaded from: classes.dex */
public final class f0 implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final MaterialButton f108b;
    @NonNull
    public final LoadingButton c;
    @NonNull
    public final LinkifiedTextView d;
    @NonNull
    public final TextView e;

    public f0(@NonNull ConstraintLayout constraintLayout, @NonNull MaterialButton materialButton, @NonNull LoadingButton loadingButton, @NonNull LinkifiedTextView linkifiedTextView, @NonNull TextView textView) {
        this.a = constraintLayout;
        this.f108b = materialButton;
        this.c = loadingButton;
        this.d = linkifiedTextView;
        this.e = textView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
