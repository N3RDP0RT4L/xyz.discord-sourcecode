package b.a.i;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.viewbinding.ViewBinding;
import com.discord.utilities.view.text.LinkifiedTextView;
/* compiled from: WidgetHubEmailFlowConfirmationBinding.java */
/* loaded from: classes.dex */
public final class j5 implements ViewBinding {
    @NonNull
    public final NestedScrollView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final LinkifiedTextView f139b;
    @NonNull
    public final TextView c;
    @NonNull
    public final LinkifiedTextView d;

    public j5(@NonNull NestedScrollView nestedScrollView, @NonNull LinkifiedTextView linkifiedTextView, @NonNull TextView textView, @NonNull LinkifiedTextView linkifiedTextView2) {
        this.a = nestedScrollView;
        this.f139b = linkifiedTextView;
        this.c = textView;
        this.d = linkifiedTextView2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
