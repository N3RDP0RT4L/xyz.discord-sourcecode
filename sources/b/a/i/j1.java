package b.a.i;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
/* compiled from: ServerSettingsMembersRoleSpinnerItemOpenBinding.java */
/* loaded from: classes.dex */
public final class j1 implements ViewBinding {
    @NonNull
    public final TextView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f135b;

    public j1(@NonNull TextView textView, @NonNull TextView textView2) {
        this.a = textView;
        this.f135b = textView2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
