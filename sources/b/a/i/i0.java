package b.a.i;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.discord.views.LoadingButton;
/* compiled from: LayoutContactSyncLandingBinding.java */
/* loaded from: classes.dex */
public final class i0 implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f127b;
    @NonNull
    public final LoadingButton c;
    @NonNull
    public final View d;
    @NonNull
    public final m0 e;

    public i0(@NonNull ConstraintLayout constraintLayout, @NonNull LinearLayout linearLayout, @NonNull TextView textView, @NonNull LoadingButton loadingButton, @NonNull View view, @NonNull TextView textView2, @NonNull TextView textView3, @NonNull m0 m0Var) {
        this.a = constraintLayout;
        this.f127b = textView;
        this.c = loadingButton;
        this.d = view;
        this.e = m0Var;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
