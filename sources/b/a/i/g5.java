package b.a.i;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentContainerView;
import androidx.viewbinding.ViewBinding;
/* compiled from: WidgetHomePanelLeftBinding.java */
/* loaded from: classes.dex */
public final class g5 implements ViewBinding {
    @NonNull
    public final RelativeLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f118b;
    @NonNull
    public final FragmentContainerView c;

    public g5(@NonNull RelativeLayout relativeLayout, @NonNull TextView textView, @NonNull FragmentContainerView fragmentContainerView, @NonNull FragmentContainerView fragmentContainerView2, @NonNull FrameLayout frameLayout) {
        this.a = relativeLayout;
        this.f118b = textView;
        this.c = fragmentContainerView2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
