package b.a.i;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.viewbinding.ViewBinding;
import com.discord.widgets.roles.RoleIconView;
/* compiled from: GuildRoleChipBinding.java */
/* loaded from: classes.dex */
public final class w implements ViewBinding {
    @NonNull
    public final CardView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ImageView f215b;
    @NonNull
    public final RoleIconView c;
    @NonNull
    public final TextView d;

    public w(@NonNull CardView cardView, @NonNull ImageView imageView, @NonNull RoleIconView roleIconView, @NonNull TextView textView) {
        this.a = cardView;
        this.f215b = imageView;
        this.c = roleIconView;
        this.d = textView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
