package b.a.i;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.viewbinding.ViewBinding;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.facebook.drawee.view.SimpleDraweeView;
/* compiled from: WidgetChatListAdapterItemSingleLineMessagePreviewBinding.java */
/* loaded from: classes.dex */
public final class u4 implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ImageView f207b;
    @NonNull
    public final LinkifiedTextView c;
    @NonNull
    public final SimpleDraweeView d;
    @NonNull
    public final TextView e;

    public u4(@NonNull ConstraintLayout constraintLayout, @NonNull ImageView imageView, @NonNull LinkifiedTextView linkifiedTextView, @NonNull SimpleDraweeView simpleDraweeView, @NonNull TextView textView, @NonNull Guideline guideline, @NonNull Guideline guideline2, @NonNull Guideline guideline3) {
        this.a = constraintLayout;
        this.f207b = imageView;
        this.c = linkifiedTextView;
        this.d = simpleDraweeView;
        this.e = textView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
