package b.a.i;

import android.view.View;
import android.view.ViewStub;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentContainerView;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.appbar.AppBarLayout;
/* compiled from: WidgetHomePanelCenterChatBinding.java */
/* loaded from: classes.dex */
public final class e5 implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ViewStub f106b;
    @NonNull
    public final f5 c;
    @NonNull
    public final FragmentContainerView d;
    @NonNull
    public final FragmentContainerView e;

    public e5(@NonNull ConstraintLayout constraintLayout, @NonNull AppBarLayout appBarLayout, @NonNull ViewStub viewStub, @NonNull ViewStub viewStub2, @NonNull f5 f5Var, @NonNull View view, @NonNull FragmentContainerView fragmentContainerView, @NonNull FragmentContainerView fragmentContainerView2, @NonNull FragmentContainerView fragmentContainerView3, @NonNull FragmentContainerView fragmentContainerView4, @NonNull ConstraintLayout constraintLayout2, @NonNull FragmentContainerView fragmentContainerView5, @NonNull FragmentContainerView fragmentContainerView6) {
        this.a = constraintLayout;
        this.f106b = viewStub2;
        this.c = f5Var;
        this.d = fragmentContainerView;
        this.e = fragmentContainerView4;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
