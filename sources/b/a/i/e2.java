package b.a.i;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
/* compiled from: ViewCodeVerificationTextBinding.java */
/* loaded from: classes.dex */
public final class e2 implements ViewBinding {
    @NonNull
    public final TextView a;

    public e2(@NonNull TextView textView) {
        this.a = textView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
