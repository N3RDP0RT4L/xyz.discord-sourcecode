package b.a.i;

import android.view.View;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.views.typing.TypingDots;
import com.discord.widgets.botuikit.views.select.SelectComponentView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.flexbox.FlexboxLayout;
import com.google.android.material.textview.MaterialTextView;
import xyz.discord.R;
/* compiled from: WidgetChatListBotUiSelectComponentBinding.java */
/* loaded from: classes.dex */
public final class w4 implements ViewBinding {
    @NonNull
    public final SelectComponentView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ImageView f220b;
    @NonNull
    public final TypingDots c;
    @NonNull
    public final SimpleDraweeView d;
    @NonNull
    public final MaterialTextView e;
    @NonNull
    public final FlexboxLayout f;

    public w4(@NonNull SelectComponentView selectComponentView, @NonNull ImageView imageView, @NonNull TypingDots typingDots, @NonNull SimpleDraweeView simpleDraweeView, @NonNull MaterialTextView materialTextView, @NonNull FlexboxLayout flexboxLayout) {
        this.a = selectComponentView;
        this.f220b = imageView;
        this.c = typingDots;
        this.d = simpleDraweeView;
        this.e = materialTextView;
        this.f = flexboxLayout;
    }

    @NonNull
    public static w4 a(@NonNull View view) {
        int i = R.id.select_component_chevron;
        ImageView imageView = (ImageView) view.findViewById(R.id.select_component_chevron);
        if (imageView != null) {
            i = R.id.select_component_loading;
            TypingDots typingDots = (TypingDots) view.findViewById(R.id.select_component_loading);
            if (typingDots != null) {
                i = R.id.select_component_selection_icon;
                SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.select_component_selection_icon);
                if (simpleDraweeView != null) {
                    i = R.id.select_component_selection_text;
                    MaterialTextView materialTextView = (MaterialTextView) view.findViewById(R.id.select_component_selection_text);
                    if (materialTextView != null) {
                        i = R.id.select_component_selections_root;
                        FlexboxLayout flexboxLayout = (FlexboxLayout) view.findViewById(R.id.select_component_selections_root);
                        if (flexboxLayout != null) {
                            return new w4((SelectComponentView) view, imageView, typingDots, simpleDraweeView, materialTextView, flexboxLayout);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
