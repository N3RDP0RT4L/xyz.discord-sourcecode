package b.a.i;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.viewbinding.ViewBinding;
/* compiled from: ViewSettingsPremiumGuildNoGuildsBinding.java */
/* loaded from: classes.dex */
public final class p3 implements ViewBinding {
    @NonNull
    public final LinearLayoutCompat a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ImageView f175b;
    @NonNull
    public final TextView c;
    @NonNull
    public final TextView d;

    public p3(@NonNull LinearLayoutCompat linearLayoutCompat, @NonNull ImageView imageView, @NonNull TextView textView, @NonNull TextView textView2) {
        this.a = linearLayoutCompat;
        this.f175b = imageView;
        this.c = textView;
        this.d = textView2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
