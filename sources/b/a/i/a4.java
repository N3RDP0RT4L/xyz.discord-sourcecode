package b.a.i;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import xyz.discord.R;
/* compiled from: ViewUserStatusPresenceBinding.java */
/* loaded from: classes.dex */
public final class a4 implements ViewBinding {
    @NonNull
    public final LinearLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ImageView f77b;
    @NonNull
    public final TextView c;
    @NonNull
    public final TextView d;

    public a4(@NonNull LinearLayout linearLayout, @NonNull ImageView imageView, @NonNull TextView textView, @NonNull TextView textView2, @NonNull LinearLayout linearLayout2) {
        this.a = linearLayout;
        this.f77b = imageView;
        this.c = textView;
        this.d = textView2;
    }

    @NonNull
    public static a4 a(@NonNull View view) {
        int i = R.id.user_status_presence_dot;
        ImageView imageView = (ImageView) view.findViewById(R.id.user_status_presence_dot);
        if (imageView != null) {
            i = R.id.user_status_presence_subtitle;
            TextView textView = (TextView) view.findViewById(R.id.user_status_presence_subtitle);
            if (textView != null) {
                i = R.id.user_status_presence_title;
                TextView textView2 = (TextView) view.findViewById(R.id.user_status_presence_title);
                if (textView2 != null) {
                    LinearLayout linearLayout = (LinearLayout) view;
                    return new a4(linearLayout, imageView, textView, textView2, linearLayout);
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
