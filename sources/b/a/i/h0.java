package b.a.i;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.discord.views.phone.PhoneOrEmailInputView;
import com.google.android.material.button.MaterialButton;
/* compiled from: LayoutContactSyncAddPhoneBinding.java */
/* loaded from: classes.dex */
public final class h0 implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final PhoneOrEmailInputView f120b;
    @NonNull
    public final MaterialButton c;

    public h0(@NonNull ConstraintLayout constraintLayout, @NonNull PhoneOrEmailInputView phoneOrEmailInputView, @NonNull MaterialButton materialButton, @NonNull TextView textView, @NonNull TextView textView2) {
        this.a = constraintLayout;
        this.f120b = phoneOrEmailInputView;
        this.c = materialButton;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
