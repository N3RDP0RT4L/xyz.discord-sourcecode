package b.a.i;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentContainerView;
import androidx.viewbinding.ViewBinding;
import com.discord.utilities.view.rounded.RoundedRelativeLayout;
/* compiled from: WidgetHomePanelCenterBinding.java */
/* loaded from: classes.dex */
public final class d5 implements ViewBinding {
    @NonNull
    public final RoundedRelativeLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final e5 f100b;
    @NonNull
    public final FragmentContainerView c;

    public d5(@NonNull RoundedRelativeLayout roundedRelativeLayout, @NonNull e5 e5Var, @NonNull FragmentContainerView fragmentContainerView) {
        this.a = roundedRelativeLayout;
        this.f100b = e5Var;
        this.c = fragmentContainerView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
