package b.a.i;

import android.view.View;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.views.FailedUploadView;
/* compiled from: ViewChatUploadListBinding.java */
/* loaded from: classes.dex */
public final class b2 implements ViewBinding {
    @NonNull
    public final LinearLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final FailedUploadView f83b;
    @NonNull
    public final FailedUploadView c;
    @NonNull
    public final FailedUploadView d;

    public b2(@NonNull LinearLayout linearLayout, @NonNull FailedUploadView failedUploadView, @NonNull FailedUploadView failedUploadView2, @NonNull FailedUploadView failedUploadView3) {
        this.a = linearLayout;
        this.f83b = failedUploadView;
        this.c = failedUploadView2;
        this.d = failedUploadView3;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
