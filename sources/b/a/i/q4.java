package b.a.i;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.button.MaterialButton;
/* compiled from: WidgetChatInputMemberVerificationGuardBinding.java */
/* loaded from: classes.dex */
public final class q4 implements ViewBinding {
    @NonNull
    public final RelativeLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final CardView f182b;
    @NonNull
    public final ImageView c;
    @NonNull
    public final MaterialButton d;
    @NonNull
    public final ImageView e;
    @NonNull
    public final TextView f;

    public q4(@NonNull RelativeLayout relativeLayout, @NonNull CardView cardView, @NonNull ImageView imageView, @NonNull MaterialButton materialButton, @NonNull ImageView imageView2, @NonNull TextView textView) {
        this.a = relativeLayout;
        this.f182b = cardView;
        this.c = imageView;
        this.d = materialButton;
        this.e = imageView2;
        this.f = textView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
