package b.a.i;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
/* compiled from: WidgetHomePanelLoadingBinding.java */
/* loaded from: classes.dex */
public final class h5 implements ViewBinding {
    @NonNull
    public final FrameLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ImageView f125b;

    public h5(@NonNull FrameLayout frameLayout, @NonNull ImageView imageView) {
        this.a = frameLayout;
        this.f125b = imageView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
