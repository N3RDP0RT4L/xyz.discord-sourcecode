package b.a.i;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.views.CheckedSetting;
import xyz.discord.R;
/* compiled from: LayoutContactSyncToggleInfoBinding.java */
/* loaded from: classes.dex */
public final class m0 implements ViewBinding {
    @NonNull
    public final LinearLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final LinkifiedTextView f155b;
    @NonNull
    public final CheckedSetting c;

    public m0(@NonNull LinearLayout linearLayout, @NonNull TextView textView, @NonNull LinkifiedTextView linkifiedTextView, @NonNull CheckedSetting checkedSetting) {
        this.a = linearLayout;
        this.f155b = linkifiedTextView;
        this.c = checkedSetting;
    }

    @NonNull
    public static m0 a(@NonNull View view) {
        int i = R.id.contact_sync_discovery_info_1;
        TextView textView = (TextView) view.findViewById(R.id.contact_sync_discovery_info_1);
        if (textView != null) {
            i = R.id.contact_sync_discovery_info_2;
            LinkifiedTextView linkifiedTextView = (LinkifiedTextView) view.findViewById(R.id.contact_sync_discovery_info_2);
            if (linkifiedTextView != null) {
                i = R.id.contact_sync_discovery_toggle;
                CheckedSetting checkedSetting = (CheckedSetting) view.findViewById(R.id.contact_sync_discovery_toggle);
                if (checkedSetting != null) {
                    return new m0((LinearLayout) view, textView, linkifiedTextView, checkedSetting);
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
