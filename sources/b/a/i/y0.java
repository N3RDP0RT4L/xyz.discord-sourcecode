package b.a.i;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.facebook.drawee.view.SimpleDraweeView;
/* compiled from: OverlayVoiceBubbleBinding.java */
/* loaded from: classes.dex */
public final class y0 implements ViewBinding {
    @NonNull
    public final SimpleDraweeView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final SimpleDraweeView f227b;

    public y0(@NonNull SimpleDraweeView simpleDraweeView, @NonNull SimpleDraweeView simpleDraweeView2) {
        this.a = simpleDraweeView;
        this.f227b = simpleDraweeView2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
