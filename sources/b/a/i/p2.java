package b.a.i;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.viewbinding.ViewBinding;
/* compiled from: ViewOverlayMenuContentBinding.java */
/* loaded from: classes.dex */
public final class p2 implements ViewBinding {
    @NonNull
    public final CardView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f174b;
    @NonNull
    public final TextView c;
    @NonNull
    public final TextView d;
    @NonNull
    public final ImageView e;
    @NonNull
    public final TextView f;
    @NonNull
    public final TextView g;

    public p2(@NonNull CardView cardView, @NonNull TextView textView, @NonNull TextView textView2, @NonNull TextView textView3, @NonNull ImageView imageView, @NonNull TextView textView4, @NonNull TextView textView5) {
        this.a = cardView;
        this.f174b = textView;
        this.c = textView2;
        this.d = textView3;
        this.e = imageView;
        this.f = textView4;
        this.g = textView5;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
