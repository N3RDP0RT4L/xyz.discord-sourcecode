package b.a.i;

import android.view.View;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.rlottie.RLottieImageView;
import com.facebook.drawee.view.SimpleDraweeView;
/* compiled from: StickerViewBinding.java */
/* loaded from: classes.dex */
public final class p1 implements ViewBinding {
    @NonNull
    public final View a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final SimpleDraweeView f173b;
    @NonNull
    public final RLottieImageView c;
    @NonNull
    public final ImageView d;

    public p1(@NonNull View view, @NonNull SimpleDraweeView simpleDraweeView, @NonNull RLottieImageView rLottieImageView, @NonNull ImageView imageView) {
        this.a = view;
        this.f173b = simpleDraweeView;
        this.c = rLottieImageView;
        this.d = imageView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
