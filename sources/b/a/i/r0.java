package b.a.i;

import android.view.View;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
/* compiled from: LayoutVoiceBottomSheetEmptyBinding.java */
/* loaded from: classes.dex */
public final class r0 implements ViewBinding {
    @NonNull
    public final LinearLayout a;

    public r0(@NonNull LinearLayout linearLayout) {
        this.a = linearLayout;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
