package b.a.i;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.discord.widgets.botuikit.views.ActionRowComponentView;
import com.google.android.flexbox.FlexboxLayout;
import xyz.discord.R;
/* compiled from: WidgetChatListBotUiActionRowComponentBinding.java */
/* loaded from: classes.dex */
public final class v4 implements ViewBinding {
    @NonNull
    public final ActionRowComponentView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final FlexboxLayout f213b;
    @NonNull
    public final i2 c;

    public v4(@NonNull ActionRowComponentView actionRowComponentView, @NonNull FlexboxLayout flexboxLayout, @NonNull i2 i2Var) {
        this.a = actionRowComponentView;
        this.f213b = flexboxLayout;
        this.c = i2Var;
    }

    @NonNull
    public static v4 a(@NonNull View view) {
        int i = R.id.action_row_component_view_group;
        FlexboxLayout flexboxLayout = (FlexboxLayout) view.findViewById(R.id.action_row_component_view_group);
        if (flexboxLayout != null) {
            i = R.id.action_row_component_view_group_error_row;
            View findViewById = view.findViewById(R.id.action_row_component_view_group_error_row);
            if (findViewById != null) {
                int i2 = R.id.view_interaction_failed_label_icon;
                ImageView imageView = (ImageView) findViewById.findViewById(R.id.view_interaction_failed_label_icon);
                if (imageView != null) {
                    i2 = R.id.view_interaction_failed_label_message;
                    TextView textView = (TextView) findViewById.findViewById(R.id.view_interaction_failed_label_message);
                    if (textView != null) {
                        return new v4((ActionRowComponentView) view, flexboxLayout, new i2((ConstraintLayout) findViewById, imageView, textView));
                    }
                }
                throw new NullPointerException("Missing required view with ID: ".concat(findViewById.getResources().getResourceName(i2)));
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
