package b.a.i;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.views.GuildView;
import xyz.discord.R;
/* compiled from: ViewMobileReportsChannelPreviewBinding.java */
/* loaded from: classes.dex */
public final class l2 implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final GuildView f150b;
    @NonNull
    public final TextView c;
    @NonNull
    public final LinkifiedTextView d;

    public l2(@NonNull ConstraintLayout constraintLayout, @NonNull GuildView guildView, @NonNull TextView textView, @NonNull LinkifiedTextView linkifiedTextView) {
        this.a = constraintLayout;
        this.f150b = guildView;
        this.c = textView;
        this.d = linkifiedTextView;
    }

    @NonNull
    public static l2 a(@NonNull View view) {
        int i = R.id.image;
        GuildView guildView = (GuildView) view.findViewById(R.id.image);
        if (guildView != null) {
            i = R.id.kicker;
            TextView textView = (TextView) view.findViewById(R.id.kicker);
            if (textView != null) {
                i = R.id.text;
                LinkifiedTextView linkifiedTextView = (LinkifiedTextView) view.findViewById(R.id.text);
                if (linkifiedTextView != null) {
                    return new l2((ConstraintLayout) view, guildView, textView, linkifiedTextView);
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
