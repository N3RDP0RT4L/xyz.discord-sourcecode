package b.a.i;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.viewbinding.ViewBinding;
/* compiled from: WidgetSettingsPremiumPriceChangeNoticeBinding.java */
/* loaded from: classes.dex */
public final class v5 implements ViewBinding {
    @NonNull
    public final CardView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final CardView f214b;
    @NonNull
    public final TextView c;

    public v5(@NonNull CardView cardView, @NonNull CardView cardView2, @NonNull TextView textView) {
        this.a = cardView;
        this.f214b = cardView2;
        this.c = textView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
