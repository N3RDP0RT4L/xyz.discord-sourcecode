package b.a.i;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
/* compiled from: DirectoryChannelTabBinding.java */
/* loaded from: classes.dex */
public final class m implements ViewBinding {
    @NonNull
    public final TextView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f154b;

    public m(@NonNull TextView textView, @NonNull TextView textView2) {
        this.a = textView;
        this.f154b = textView2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
