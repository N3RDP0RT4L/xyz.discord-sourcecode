package b.a.i;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
/* compiled from: ServerSettingsMembersRoleSpinnerItemBinding.java */
/* loaded from: classes.dex */
public final class i1 implements ViewBinding {
    @NonNull
    public final TextView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f128b;

    public i1(@NonNull TextView textView, @NonNull TextView textView2) {
        this.a = textView;
        this.f128b = textView2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
