package b.a.i;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.views.CutoutView;
import com.facebook.drawee.view.SimpleDraweeView;
import xyz.discord.R;
/* compiled from: ViewUserSummaryItemBinding.java */
/* loaded from: classes.dex */
public final class b4 implements ViewBinding {
    @NonNull
    public final CutoutView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final SimpleDraweeView f85b;

    public b4(@NonNull CutoutView cutoutView, @NonNull SimpleDraweeView simpleDraweeView) {
        this.a = cutoutView;
        this.f85b = simpleDraweeView;
    }

    @NonNull
    public static b4 a(@NonNull View view) {
        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.avatar);
        if (simpleDraweeView != null) {
            return new b4((CutoutView) view, simpleDraweeView);
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(R.id.avatar)));
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
