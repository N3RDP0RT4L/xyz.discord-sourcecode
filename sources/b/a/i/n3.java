package b.a.i;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.utilities.view.text.LinkifiedTextView;
/* compiled from: ViewSettingSharedBinding.java */
/* loaded from: classes.dex */
public final class n3 implements ViewBinding {
    @NonNull
    public final View a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ImageView f163b;
    @NonNull
    public final TextView c;
    @NonNull
    public final LinkifiedTextView d;
    @NonNull
    public final TextView e;

    public n3(@NonNull View view, @NonNull ImageView imageView, @NonNull TextView textView, @NonNull LinkifiedTextView linkifiedTextView, @NonNull TextView textView2) {
        this.a = view;
        this.f163b = imageView;
        this.c = textView;
        this.d = linkifiedTextView;
        this.e = textView2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
