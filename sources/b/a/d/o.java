package b.a.d;

import android.content.Context;
import androidx.core.app.NotificationCompat;
import com.discord.app.AppFragment;
import com.discord.utilities.error.Error;
import d0.t.h0;
import d0.z.d.m;
import j0.l.e.k;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import rx.Observable;
import rx.functions.Action1;
/* compiled from: AppTransformers.kt */
/* loaded from: classes.dex */
public final class o {
    public static final o a = new o();

    /* compiled from: AppTransformers.kt */
    /* loaded from: classes.dex */
    public static final class a extends d0.z.d.o implements Function1<T, T> {
        public static final a j = new a();

        public a() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public final T invoke(T t) {
            m.checkNotNull(t);
            return t;
        }
    }

    /* compiled from: AppTransformers.kt */
    /* loaded from: classes.dex */
    public static final class b<T, R> implements Observable.c<Map<K, ? extends V>, Map<K, ? extends V1>> {
        public static final b j = new b();

        @Override // j0.k.b
        public Object call(Object obj) {
            Observable observable = (Observable) obj;
            return new k(h0.emptyMap());
        }
    }

    /* compiled from: AppTransformers.kt */
    /* loaded from: classes.dex */
    public static final class c<T, R> implements Observable.c<Map<K, ? extends V>, Map<K, ? extends V1>> {
        public final /* synthetic */ Collection j;
        public final /* synthetic */ Function1 k;

        public c(Collection collection, Function1 function1) {
            this.j = collection;
            this.k = function1;
        }

        @Override // j0.k.b
        public Object call(Object obj) {
            return ((Observable) obj).Y(new s(this)).q();
        }
    }

    /* compiled from: AppTransformers.kt */
    /* loaded from: classes.dex */
    public static final class d<T, R> implements Observable.c<T, T> {
        public final /* synthetic */ Function1 j;
        public final /* synthetic */ Object k;
        public final /* synthetic */ long l;
        public final /* synthetic */ TimeUnit m;

        public d(Function1 function1, Object obj, long j, TimeUnit timeUnit) {
            this.j = function1;
            this.k = obj;
            this.l = j;
            this.m = timeUnit;
        }

        @Override // j0.k.b
        public Object call(Object obj) {
            return ((Observable) obj).Y(new t(this));
        }
    }

    /* compiled from: AppTransformers.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class e extends d0.z.d.k implements Function1<T, Unit> {
        public e(Action1 action1) {
            super(1, action1, Action1.class, NotificationCompat.CATEGORY_CALL, "call(Ljava/lang/Object;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(Object obj) {
            ((Action1) this.receiver).call(obj);
            return Unit.a;
        }
    }

    /* compiled from: AppTransformers.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class f extends d0.z.d.k implements Function1<T, Unit> {
        public f(Action1 action1) {
            super(1, action1, Action1.class, NotificationCompat.CATEGORY_CALL, "call(Ljava/lang/Object;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(Object obj) {
            ((Action1) this.receiver).call(obj);
            return Unit.a;
        }
    }

    public static final <K, T> Observable.c<Map<K, T>, Map<K, T>> a(Collection<? extends K> collection) {
        m.checkNotNullParameter(collection, "filterKeys");
        return b(collection, a.j);
    }

    public static final <K, V, V1> Observable.c<Map<K, V>, Map<K, V1>> b(Collection<? extends K> collection, Function1<? super V, ? extends V1> function1) {
        m.checkNotNullParameter(function1, "valueMapper");
        if (collection == null || collection.isEmpty()) {
            return b.j;
        }
        return new c(collection, function1);
    }

    public static final <T> Observable.c<T, T> c(Function1<? super T, Boolean> function1, T t, long j, TimeUnit timeUnit) {
        m.checkNotNullParameter(function1, "predicate");
        m.checkNotNullParameter(timeUnit, "timeUnit");
        return new d(function1, t, j, timeUnit);
    }

    public static /* synthetic */ Observable.c d(Function1 function1, Object obj, long j, TimeUnit timeUnit, int i) {
        if ((i & 4) != 0) {
            j = 5000;
        }
        return c(function1, obj, j, (i & 8) != 0 ? TimeUnit.MILLISECONDS : null);
    }

    public static final <T> Observable.c<T, T> e(Action1<? super T> action1, Class<?> cls) {
        m.checkNotNullParameter(action1, "onNext");
        m.checkNotNullParameter(cls, "errorClass");
        return new w(null, cls, null, action1, null);
    }

    public static Observable.c f(o oVar, Function1 function1, String str, Function1 function12, Function1 function13, Context context, Function0 function0, int i) {
        Function1 function14 = (i & 4) != 0 ? null : function12;
        int i2 = i & 8;
        Context context2 = (i & 16) != 0 ? null : context;
        int i3 = i & 32;
        m.checkNotNullParameter(function1, "onNext");
        m.checkNotNullParameter(str, "errorTag");
        return new y(context2, str, null, function1, function14, null);
    }

    public static final <T> Observable.c<T, T> h(Action1<? super T> action1, Context context, Action1<Error> action12) {
        m.checkNotNullParameter(action1, "onNext");
        e eVar = new e(action1);
        c0 c0Var = true & true ? null : action12 != null ? new c0(action12) : null;
        boolean z2 = true & true;
        Context context2 = true & true ? null : context;
        boolean z3 = true & true;
        m.checkNotNullParameter(eVar, "onNext");
        m.checkNotNullParameter("restClient", "errorTag");
        return new y(context2, "restClient", null, eVar, c0Var, null);
    }

    public static final <T> Observable.c<T, T> i(Action1<? super T> action1, AppFragment appFragment) {
        m.checkNotNullParameter(action1, "onNext");
        m.checkNotNullParameter(appFragment, "fragment");
        return a.g(appFragment.getContext(), new f(action1), null);
    }

    public static /* synthetic */ Observable.c j(Action1 action1, Context context, Action1 action12, int i) {
        int i2 = i & 4;
        return h(action1, context, null);
    }

    public final <T> Observable.c<T, T> g(Context context, Function1<? super T, Unit> function1, Action1<Error> action1) {
        m.checkNotNullParameter(function1, "onNext");
        return f(this, function1, "restClient", action1 != null ? new c0(action1) : null, null, context, null, 40);
    }
}
