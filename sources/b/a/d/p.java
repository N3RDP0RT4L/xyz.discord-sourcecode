package b.a.d;

import j0.k.b;
import java.util.Map;
/* compiled from: AppTransformers.kt */
/* loaded from: classes.dex */
public final class p<T, R> implements b<K, Boolean> {
    public final /* synthetic */ Map j;

    public p(Map map) {
        this.j = map;
    }

    @Override // j0.k.b
    public Boolean call(Object obj) {
        return Boolean.valueOf(this.j.containsKey(obj));
    }
}
