package b.a.d;

import com.discord.rtcconnection.RtcConnection;
import j0.k.b;
/* compiled from: DiscordConnectService.kt */
/* loaded from: classes.dex */
public final class j0<T, R> implements b<RtcConnection.StateChange, Boolean> {
    public static final j0 j = new j0();

    @Override // j0.k.b
    public Boolean call(RtcConnection.StateChange stateChange) {
        RtcConnection.State state = stateChange.a;
        return Boolean.valueOf(!(state instanceof RtcConnection.State.d) && !(state instanceof RtcConnection.State.h));
    }
}
