package b.a.d;

import androidx.lifecycle.ViewModelProvider;
import d0.z.d.o;
import kotlin.jvm.functions.Function0;
/* compiled from: AppViewModelDelegates.kt */
/* loaded from: classes.dex */
public final class e0 extends o implements Function0<ViewModelProvider.Factory> {
    public final /* synthetic */ Function0 $viewModelProducer;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public e0(Function0 function0) {
        super(0);
        this.$viewModelProducer = function0;
    }

    @Override // kotlin.jvm.functions.Function0
    public ViewModelProvider.Factory invoke() {
        return new d0(this);
    }
}
