package b.a.d;

import com.discord.app.AppLog;
import com.discord.utilities.logging.Logger;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
/* compiled from: AppLog.kt */
/* loaded from: classes.dex */
public final class g extends o implements Function3<Integer, String, Exception, Unit> {
    public final /* synthetic */ String $tag;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public g(String str) {
        super(3);
        this.$tag = str;
    }

    @Override // kotlin.jvm.functions.Function3
    public Unit invoke(Integer num, String str, Exception exc) {
        int intValue = num.intValue();
        String str2 = str;
        Exception exc2 = exc;
        m.checkNotNullParameter(str2, "message");
        if (intValue == 4) {
            AppLog appLog = AppLog.g;
            appLog.i(this.$tag + ' ' + str2, exc2);
        } else if (intValue == 5) {
            AppLog appLog2 = AppLog.g;
            appLog2.w(this.$tag + ' ' + str2, exc2);
        } else if (intValue == 6 || intValue == 7) {
            AppLog appLog3 = AppLog.g;
            Logger.e$default(appLog3, this.$tag + ' ' + str2, exc2, null, 4, null);
        }
        return Unit.a;
    }
}
