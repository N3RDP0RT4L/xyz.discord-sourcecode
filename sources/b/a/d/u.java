package b.a.d;

import androidx.core.app.NotificationCompat;
import d0.z.d.k;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.functions.Action1;
/* compiled from: AppTransformers.kt */
/* loaded from: classes.dex */
public final /* synthetic */ class u extends k implements Function1<T, Unit> {
    public u(Action1 action1) {
        super(1, action1, Action1.class, NotificationCompat.CATEGORY_CALL, "call(Ljava/lang/Object;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public Unit invoke(Object obj) {
        ((Action1) this.receiver).call(obj);
        return Unit.a;
    }
}
