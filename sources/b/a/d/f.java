package b.a.d;

import android.annotation.SuppressLint;
import b.d.b.a.a;
import com.discord.stores.StoreStream;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.z.d.m;
import java.util.Objects;
/* compiled from: AppHelpDesk.kt */
/* loaded from: classes.dex */
public final class f {
    public static final f a = new f();

    public static final String c() {
        StringBuilder V = a.V("https://support.discord.com", "/hc/");
        String locale = StoreStream.Companion.getUserSettingsSystem().getLocale();
        Objects.requireNonNull(locale, "null cannot be cast to non-null type java.lang.String");
        String lowerCase = locale.toLowerCase();
        m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase()");
        V.append(lowerCase);
        V.append("/requests/new");
        return V.toString();
    }

    public final String a(long j, String str) {
        if (str != null) {
            StringBuilder V = a.V("https://support.discord.com", "/hc/");
            V.append(b());
            V.append("/articles/");
            V.append(j);
            V.append(MentionUtilsKt.CHANNELS_CHAR);
            V.append(str);
            return V.toString();
        }
        StringBuilder V2 = a.V("https://support.discord.com", "/hc/");
        V2.append(b());
        V2.append("/articles/");
        V2.append(j);
        return V2.toString();
    }

    @SuppressLint({"DefaultLocale"})
    public final String b() {
        String locale = StoreStream.Companion.getUserSettingsSystem().getLocale();
        Objects.requireNonNull(locale, "null cannot be cast to non-null type java.lang.String");
        String lowerCase = locale.toLowerCase();
        m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase()");
        return lowerCase;
    }
}
