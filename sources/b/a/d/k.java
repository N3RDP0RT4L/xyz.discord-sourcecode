package b.a.d;

import j0.k.b;
/* compiled from: AppState.kt */
/* loaded from: classes.dex */
public final class k<T, R> implements b<Integer, Boolean> {
    public static final k j = new k();

    @Override // j0.k.b
    public Boolean call(Integer num) {
        return Boolean.valueOf(num.intValue() > 0);
    }
}
