package b.a.d;

import b.a.k.b;
import com.discord.app.AppFragment;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: AppFragment.kt */
/* loaded from: classes.dex */
public final class e extends o implements Function1<Exception, Unit> {
    public final /* synthetic */ AppFragment.d this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public e(AppFragment.d dVar) {
        super(1);
        this.this$0 = dVar;
    }

    @Override // kotlin.jvm.functions.Function1
    public Unit invoke(Exception exc) {
        CharSequence e;
        Exception exc2 = exc;
        m.checkNotNullParameter(exc2, "e");
        AppFragment appFragment = AppFragment.this;
        e = b.e(appFragment, R.string.unable_to_open_media_chooser, new Object[]{exc2.getMessage()}, (r4 & 4) != 0 ? b.a.j : null);
        m.j(appFragment, e, 0, 4);
        return Unit.a;
    }
}
