package b.a.d.l0;

import andhook.lib.xposed.ClassUtils;
import android.net.Uri;
import com.discord.BuildConfig;
import d0.g0.i;
import d0.g0.t;
import d0.z.d.m;
import kotlin.text.Regex;
/* compiled from: RoutingPatterns.kt */
/* loaded from: classes.dex */
public final class a {
    public static final Regex A;
    public static final Regex B;
    public static final Regex C = new Regex("^/ra/([\\w-]+)$");
    public static final Regex D;
    public static final Regex E;
    public static final a F;
    public static final String a;

    /* renamed from: b  reason: collision with root package name */
    public static final String f60b;
    public static final String c;
    public static final String d;
    public static final String e;
    public static final String f;
    public static final String g;
    public static final String h;
    public static final String i;
    public static final String j;
    public static final String k;
    public static final Regex l;
    public static final Regex m;
    public static final Regex n;
    public static final Regex o;
    public static final Regex p;
    public static final Regex q;
    public static final Regex r;

    /* renamed from: s  reason: collision with root package name */
    public static final Regex f61s;
    public static final Regex t;
    public static final Regex u;
    public static final Regex v;
    public static final Regex w;

    /* renamed from: x  reason: collision with root package name */
    public static final Regex f62x;

    /* renamed from: y  reason: collision with root package name */
    public static final Regex f63y;

    /* renamed from: z  reason: collision with root package name */
    public static final Regex f64z;

    static {
        a aVar = new a();
        F = aVar;
        String b2 = aVar.b(BuildConfig.HOST);
        a = b2;
        String b3 = aVar.b(BuildConfig.HOST_ALTERNATE);
        f60b = b3;
        String b4 = aVar.b(BuildConfig.HOST_GIFT);
        c = b4;
        String b5 = aVar.b(BuildConfig.HOST_INVITE);
        d = b5;
        String b6 = aVar.b(BuildConfig.HOST_GUILD_TEMPLATE);
        e = b6;
        String replace$default = t.replace$default(b2, ".", "\\.", false, 4, (Object) null);
        f = replace$default;
        String replace$default2 = t.replace$default(b3, ".", "\\.", false, 4, (Object) null);
        g = replace$default2;
        String str = "(?:" + replace$default + '|' + replace$default2 + ')';
        h = str;
        String replace$default3 = t.replace$default(b4, ".", "\\.", false, 4, (Object) null);
        i = replace$default3;
        String replace$default4 = t.replace$default(b5, ".", "\\.", false, 4, (Object) null);
        j = replace$default4;
        String replace$default5 = t.replace$default(b6, ".", "\\.", false, 4, (Object) null);
        k = replace$default5;
        i iVar = i.IGNORE_CASE;
        l = new Regex("^(?:ptb|canary|www)." + str + ClassUtils.INNER_CLASS_SEPARATOR_CHAR, iVar);
        m = new Regex("^/(?:(invite|gift|template)/)?([\\w-]+)/?$", iVar);
        n = new Regex("^/(?:invite/)?([\\w-]+)/?$", iVar);
        StringBuilder sb = new StringBuilder();
        sb.append("(?:https?://(?:(?:" + str + "/invite)|");
        sb.append("(?:" + replace$default4 + "))|");
        sb.append("(?:^" + replace$default4 + "))/([\\w-]+)/?");
        sb.append("(?:(?:\\S*[\\?&]event=)([\\w-]+))?");
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "StringBuilder()\n        …\"\")\n          .toString()");
        o = new Regex(sb2, iVar);
        p = new Regex("^/(?:template/)?([\\w-]+)/?$", iVar);
        StringBuilder sb3 = new StringBuilder();
        sb3.append("(?:https?://(?:(?:" + str + "/template)|");
        sb3.append("(?:" + replace$default5 + "))|");
        sb3.append("(?:" + replace$default5 + "))/([\\w-]+)/?");
        String sb4 = sb3.toString();
        m.checkNotNullExpressionValue(sb4, "StringBuilder()\n        …?\")\n          .toString()");
        q = new Regex(sb4, iVar);
        StringBuilder sb5 = new StringBuilder();
        sb5.append("(?:https?://)?(?:(?:" + str + "/gifts)|");
        sb5.append("(?:" + replace$default3 + "))/([\\w-]+)/?");
        String sb6 = sb5.toString();
        m.checkNotNullExpressionValue(sb6, "StringBuilder()\n        …?\")\n          .toString()");
        r = new Regex(sb6, iVar);
        f61s = new Regex("^/connect(?:/(\\d+))?/?$", iVar);
        t = new Regex("^/channels/((?:@me)|(?:\\d+))(?:/(\\d+))?(?:/(\\d+))?/?$", iVar);
        u = new Regex("^/channels/@me/-1?/?$", iVar);
        v = new Regex("^/lurk/(\\d+)(?:/(\\d+))?/?$", iVar);
        w = new Regex("^/channels/@me/user/(\\d+)/?$", iVar);
        f62x = new Regex("^/users/(\\d+)/?$", iVar);
        f63y = new Regex("^/settings(/\\w+)*/?$", iVar);
        f64z = new Regex("^/feature(/\\w+)*/?$", iVar);
        A = new Regex("^/query(/\\w+)*/?$", iVar);
        B = new Regex("^/(?:oauth2|connect)/authorize/?$", iVar);
        D = new Regex("^/guild/((?:@me)|(?:\\d+))/premiumguild/?$", iVar);
        E = new Regex("^/events/(\\d+)/(\\d+)/?$", iVar);
    }

    public final boolean a(String str) {
        if (str == null) {
            return false;
        }
        if (!m.areEqual(str, a) && !m.areEqual(str, f60b) && !m.areEqual(str, c) && !m.areEqual(str, e) && !m.areEqual(str, d)) {
            return l.matches(str);
        }
        return true;
    }

    public final String b(String str) {
        Uri parse = Uri.parse(str);
        m.checkNotNullExpressionValue(parse, "Uri.parse(this)");
        String host = parse.getHost();
        return host != null ? host : "";
    }
}
