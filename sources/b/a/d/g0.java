package b.a.d;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import d0.z.d.m;
/* compiled from: AppViewModelDelegates.kt */
/* loaded from: classes.dex */
public final class g0 implements ViewModelProvider.Factory {
    public final /* synthetic */ h0 a;

    public g0(h0 h0Var) {
        this.a = h0Var;
    }

    @Override // androidx.lifecycle.ViewModelProvider.Factory
    public <T extends ViewModel> T create(Class<T> cls) {
        m.checkNotNullParameter(cls, "modelClass");
        return (T) this.a.$viewModelProducer.invoke();
    }
}
