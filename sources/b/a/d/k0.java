package b.a.d;

import com.discord.rtcconnection.RtcConnection;
import com.discord.stores.StoreStream;
import j0.k.b;
import rx.Observable;
/* compiled from: DiscordConnectService.kt */
/* loaded from: classes.dex */
public final class k0<T, R> implements b<Boolean, Observable<? extends RtcConnection.StateChange>> {
    public static final k0 j = new k0();

    @Override // j0.k.b
    public Observable<? extends RtcConnection.StateChange> call(Boolean bool) {
        return StoreStream.Companion.getRtcConnection().getConnectionState().x(j0.j);
    }
}
