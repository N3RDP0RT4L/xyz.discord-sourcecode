package b.a.d;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import d0.z.d.m;
/* compiled from: AppViewModelDelegates.kt */
/* loaded from: classes.dex */
public final class d0 implements ViewModelProvider.Factory {
    public final /* synthetic */ e0 a;

    public d0(e0 e0Var) {
        this.a = e0Var;
    }

    @Override // androidx.lifecycle.ViewModelProvider.Factory
    public <T extends ViewModel> T create(Class<T> cls) {
        m.checkNotNullParameter(cls, "modelClass");
        return (T) this.a.$viewModelProducer.invoke();
    }
}
