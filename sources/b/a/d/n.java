package b.a.d;

import com.discord.utilities.rx.ObservableExtensionsKt;
import d0.z.d.m;
import rx.Observable;
/* compiled from: AppTransformers.kt */
/* loaded from: classes.dex */
public final class n<T, R> implements Observable.c<T, T> {
    public static final n j = new n();

    @Override // j0.k.b
    public Object call(Object obj) {
        Observable observable = (Observable) obj;
        m.checkNotNullExpressionValue(observable, "observable");
        return ObservableExtensionsKt.computationBuffered(observable).q();
    }
}
