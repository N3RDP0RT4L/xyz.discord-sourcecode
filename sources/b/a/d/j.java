package b.a.d;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.activity.result.ActivityResultLauncher;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import b.c.a.a0.d;
import com.discord.app.AppActivity;
import com.discord.app.AppComponent;
import com.discord.app.AppFragment;
import com.discord.app.AppLog;
import com.discord.stores.StoreNavigation;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreTabsNavigation;
import com.discord.widgets.auth.WidgetAgeVerify;
import com.discord.widgets.auth.WidgetAuthAgeGated;
import com.discord.widgets.auth.WidgetAuthBirthday;
import com.discord.widgets.auth.WidgetAuthCaptcha;
import com.discord.widgets.auth.WidgetAuthLanding;
import com.discord.widgets.auth.WidgetAuthLogin;
import com.discord.widgets.auth.WidgetAuthMfa;
import com.discord.widgets.auth.WidgetAuthPhoneVerify;
import com.discord.widgets.auth.WidgetAuthRegisterAccountInformation;
import com.discord.widgets.auth.WidgetAuthRegisterIdentity;
import com.discord.widgets.auth.WidgetAuthResetPassword;
import com.discord.widgets.auth.WidgetAuthUndeleteAccount;
import com.discord.widgets.auth.WidgetOauth2Authorize;
import com.discord.widgets.auth.WidgetOauth2AuthorizeSamsung;
import com.discord.widgets.channels.permissions.WidgetChannelSettingsEditPermissions;
import com.discord.widgets.channels.permissions.WidgetChannelSettingsPermissionsOverview;
import com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventSettings;
import com.discord.widgets.nux.WidgetNuxChannelPrompt;
import com.discord.widgets.servers.WidgetServerNotifications;
import com.discord.widgets.servers.WidgetServerSettingsBans;
import com.discord.widgets.servers.WidgetServerSettingsChannels;
import com.discord.widgets.servers.WidgetServerSettingsEditMember;
import com.discord.widgets.servers.WidgetServerSettingsEditRole;
import com.discord.widgets.servers.WidgetServerSettingsEmojis;
import com.discord.widgets.servers.WidgetServerSettingsEmojisEdit;
import com.discord.widgets.servers.WidgetServerSettingsIntegrations;
import com.discord.widgets.servers.WidgetServerSettingsModeration;
import com.discord.widgets.servers.WidgetServerSettingsOverview;
import com.discord.widgets.servers.WidgetServerSettingsRoles;
import com.discord.widgets.servers.WidgetServerSettingsSecurity;
import com.discord.widgets.servers.WidgetServerSettingsVanityUrl;
import com.discord.widgets.servers.community.WidgetServerSettingsCommunityOverview;
import com.discord.widgets.servers.community.WidgetServerSettingsEnableCommunitySteps;
import com.discord.widgets.servers.settings.invites.WidgetServerSettingsInstantInvites;
import com.discord.widgets.servers.settings.members.WidgetServerSettingsMembers;
import com.discord.widgets.settings.WidgetSettingsAppearance;
import com.discord.widgets.settings.WidgetSettingsAuthorizedApps;
import com.discord.widgets.settings.WidgetSettingsBehavior;
import com.discord.widgets.settings.WidgetSettingsLanguage;
import com.discord.widgets.settings.WidgetSettingsMedia;
import com.discord.widgets.settings.WidgetSettingsNotifications;
import com.discord.widgets.settings.WidgetSettingsPrivacy;
import com.discord.widgets.settings.WidgetSettingsVoice;
import com.discord.widgets.settings.account.WidgetSettingsAccount;
import com.discord.widgets.settings.account.WidgetSettingsAccountBackupCodes;
import com.discord.widgets.settings.account.WidgetSettingsAccountChangePassword;
import com.discord.widgets.settings.account.WidgetSettingsAccountEmailEdit;
import com.discord.widgets.settings.account.WidgetSettingsAccountEmailEditConfirm;
import com.discord.widgets.settings.account.WidgetSettingsAccountUsernameEdit;
import com.discord.widgets.settings.account.WidgetSettingsBlockedUsers;
import com.discord.widgets.settings.account.mfa.WidgetEnableMFASteps;
import com.discord.widgets.settings.connections.WidgetSettingsUserConnections;
import com.discord.widgets.settings.developer.WidgetSettingsDeveloper;
import com.discord.widgets.settings.premium.WidgetChoosePlan;
import com.discord.widgets.settings.premium.WidgetSettingsPremium;
import com.discord.widgets.tabs.WidgetTabsHost;
import com.discord.widgets.user.WidgetUserPasswordVerify;
import com.discord.widgets.user.account.WidgetUserAccountVerify;
import com.discord.widgets.user.email.WidgetUserEmailUpdate;
import com.discord.widgets.user.email.WidgetUserEmailVerify;
import com.discord.widgets.user.phone.WidgetUserPhoneManage;
import com.discord.widgets.user.phone.WidgetUserPhoneVerify;
import d0.e0.c;
import d0.t.m;
import d0.t.n;
import d0.z.d.a0;
import java.util.List;
/* compiled from: AppScreen.kt */
/* loaded from: classes.dex */
public final class j {
    public static final j g = new j();
    public static final List<c<? extends AppFragment>> a = n.listOf((Object[]) new c[]{a0.getOrCreateKotlinClass(WidgetAuthLanding.class), a0.getOrCreateKotlinClass(WidgetAuthLogin.class), a0.getOrCreateKotlinClass(WidgetAuthRegisterIdentity.class), a0.getOrCreateKotlinClass(WidgetAuthRegisterAccountInformation.class), a0.getOrCreateKotlinClass(WidgetAuthUndeleteAccount.class), a0.getOrCreateKotlinClass(WidgetAuthCaptcha.class), a0.getOrCreateKotlinClass(WidgetAuthMfa.class), a0.getOrCreateKotlinClass(WidgetAuthBirthday.class), a0.getOrCreateKotlinClass(WidgetAuthAgeGated.class), a0.getOrCreateKotlinClass(WidgetAuthPhoneVerify.class), a0.getOrCreateKotlinClass(WidgetAuthResetPassword.class)});

    /* renamed from: b  reason: collision with root package name */
    public static final List<c<WidgetAgeVerify>> f58b = m.listOf(a0.getOrCreateKotlinClass(WidgetAgeVerify.class));
    public static final List<c<? extends WidgetOauth2Authorize>> c = n.listOf((Object[]) new c[]{a0.getOrCreateKotlinClass(WidgetOauth2Authorize.class), a0.getOrCreateKotlinClass(WidgetOauth2AuthorizeSamsung.class)});
    public static final List<c<? extends AppFragment>> d = n.listOf((Object[]) new c[]{a0.getOrCreateKotlinClass(WidgetSettingsAccount.class), a0.getOrCreateKotlinClass(WidgetSettingsAccountBackupCodes.class), a0.getOrCreateKotlinClass(WidgetSettingsAccountChangePassword.class), a0.getOrCreateKotlinClass(WidgetSettingsAccountUsernameEdit.class), a0.getOrCreateKotlinClass(WidgetSettingsAccountEmailEdit.class), a0.getOrCreateKotlinClass(WidgetSettingsAccountEmailEditConfirm.class), a0.getOrCreateKotlinClass(WidgetUserPasswordVerify.class), a0.getOrCreateKotlinClass(WidgetEnableMFASteps.class), a0.getOrCreateKotlinClass(WidgetSettingsAppearance.class), a0.getOrCreateKotlinClass(WidgetSettingsBehavior.class), a0.getOrCreateKotlinClass(WidgetSettingsLanguage.class), a0.getOrCreateKotlinClass(WidgetSettingsMedia.class), a0.getOrCreateKotlinClass(WidgetSettingsPremium.class), a0.getOrCreateKotlinClass(WidgetSettingsNotifications.class), a0.getOrCreateKotlinClass(WidgetSettingsUserConnections.class), a0.getOrCreateKotlinClass(WidgetSettingsVoice.class), a0.getOrCreateKotlinClass(WidgetSettingsPrivacy.class), a0.getOrCreateKotlinClass(WidgetSettingsAuthorizedApps.class), a0.getOrCreateKotlinClass(WidgetServerNotifications.class), a0.getOrCreateKotlinClass(WidgetServerSettingsOverview.class), a0.getOrCreateKotlinClass(WidgetServerSettingsChannels.class), a0.getOrCreateKotlinClass(WidgetServerSettingsEditMember.class), a0.getOrCreateKotlinClass(WidgetServerSettingsEditRole.class), a0.getOrCreateKotlinClass(WidgetServerSettingsIntegrations.class), a0.getOrCreateKotlinClass(WidgetServerSettingsModeration.class), a0.getOrCreateKotlinClass(WidgetServerSettingsVanityUrl.class), a0.getOrCreateKotlinClass(WidgetServerSettingsSecurity.class), a0.getOrCreateKotlinClass(WidgetServerSettingsMembers.class), a0.getOrCreateKotlinClass(WidgetServerSettingsEmojis.class), a0.getOrCreateKotlinClass(WidgetServerSettingsEmojisEdit.class), a0.getOrCreateKotlinClass(WidgetServerSettingsRoles.class), a0.getOrCreateKotlinClass(WidgetServerSettingsInstantInvites.class), a0.getOrCreateKotlinClass(WidgetServerSettingsBans.class), a0.getOrCreateKotlinClass(WidgetChannelSettingsEditPermissions.class), a0.getOrCreateKotlinClass(WidgetChannelSettingsPermissionsOverview.class), a0.getOrCreateKotlinClass(WidgetAuthRegisterIdentity.class), a0.getOrCreateKotlinClass(WidgetAuthRegisterAccountInformation.class), a0.getOrCreateKotlinClass(WidgetAuthBirthday.class), a0.getOrCreateKotlinClass(WidgetAuthAgeGated.class), a0.getOrCreateKotlinClass(WidgetAuthLogin.class), a0.getOrCreateKotlinClass(WidgetAuthPhoneVerify.class), a0.getOrCreateKotlinClass(WidgetAuthResetPassword.class), a0.getOrCreateKotlinClass(WidgetSettingsDeveloper.class), a0.getOrCreateKotlinClass(WidgetSettingsBlockedUsers.class), a0.getOrCreateKotlinClass(WidgetNuxChannelPrompt.class), a0.getOrCreateKotlinClass(WidgetChoosePlan.class), a0.getOrCreateKotlinClass(WidgetServerSettingsCommunityOverview.class), a0.getOrCreateKotlinClass(WidgetServerSettingsEnableCommunitySteps.class), a0.getOrCreateKotlinClass(WidgetGuildScheduledEventSettings.class)});
    public static final List<c<? extends AppFragment>> e = n.listOf((Object[]) new c[]{a0.getOrCreateKotlinClass(WidgetUserAccountVerify.class), a0.getOrCreateKotlinClass(WidgetUserEmailVerify.class), a0.getOrCreateKotlinClass(WidgetUserEmailUpdate.class), a0.getOrCreateKotlinClass(WidgetUserPhoneManage.class), a0.getOrCreateKotlinClass(WidgetUserPhoneVerify.class), a0.getOrCreateKotlinClass(WidgetUserPasswordVerify.class)});
    public static final List<c<WidgetTabsHost>> f = m.listOf(a0.getOrCreateKotlinClass(WidgetTabsHost.class));

    public static final void b(Context context, boolean z2, Intent intent) {
        Class cls;
        StoreNavigation.PanelAction panelAction;
        d0.z.d.m.checkNotNullParameter(context, "context");
        if (z2) {
            if (intent != null ? intent.getBooleanExtra("com.discord.intent.extra.EXTRA_OPEN_PANEL", false) : false) {
                panelAction = StoreNavigation.PanelAction.OPEN;
            } else {
                panelAction = StoreNavigation.PanelAction.CLOSE;
            }
            StoreTabsNavigation.selectHomeTab$default(StoreStream.Companion.getTabsNavigation(), panelAction, false, 2, null);
            cls = WidgetTabsHost.class;
        } else {
            cls = WidgetAuthLanding.class;
        }
        d(context, cls, intent);
    }

    public static /* synthetic */ void c(Context context, boolean z2, Intent intent, int i) {
        if ((i & 2) != 0) {
            z2 = true;
        }
        if ((i & 4) != 0) {
            intent = null;
        }
        b(context, z2, intent);
    }

    public static final void d(Context context, Class<? extends AppComponent> cls, Parcelable parcelable) {
        Intent intent;
        d0.z.d.m.checkNotNullParameter(context, "context");
        d0.z.d.m.checkNotNullParameter(cls, "screen");
        Intent intent2 = null;
        Intent intent3 = (Intent) (!(parcelable instanceof Intent) ? null : parcelable);
        if (intent3 != null) {
            intent2 = intent3;
        } else if (parcelable != null) {
            intent2 = d.g2(parcelable);
        }
        AppLog appLog = AppLog.g;
        String simpleName = context.getClass().getSimpleName();
        d0.z.d.m.checkNotNullExpressionValue(simpleName, "javaClass.simpleName");
        String simpleName2 = cls.getSimpleName();
        d0.z.d.m.checkNotNullExpressionValue(simpleName2, "screen.simpleName");
        appLog.f(simpleName, simpleName2);
        if (intent2 != null) {
            intent = new Intent(intent2);
        } else {
            intent = new Intent();
        }
        Intent putExtra = intent.setClass(context, AppActivity.class).putExtra("com.discord.intent.extra.EXTRA_SCREEN", cls);
        d0.z.d.m.checkNotNullExpressionValue(putExtra, "if (extras != null) {\n  …nts.EXTRA_SCREEN, screen)");
        context.startActivity(putExtra);
    }

    public static /* synthetic */ void e(Context context, Class cls, Parcelable parcelable, int i) {
        int i2 = i & 4;
        d(context, cls, null);
    }

    public static void g(j jVar, FragmentManager fragmentManager, Context context, Class cls, int i, boolean z2, String str, Parcelable parcelable, int i2) {
        if ((i2 & 8) != 0) {
            i = 16908290;
        }
        if ((i2 & 16) != 0) {
            z2 = false;
        }
        int i3 = i2 & 32;
        if ((i2 & 64) != 0) {
            parcelable = null;
        }
        d0.z.d.m.checkNotNullParameter(context, "context");
        d0.z.d.m.checkNotNullParameter(cls, "screen");
        if (fragmentManager != null) {
            FragmentTransaction beginTransaction = fragmentManager.beginTransaction();
            d0.z.d.m.checkNotNullExpressionValue(beginTransaction, "fragmentManager.beginTransaction()");
            Fragment instantiate = Fragment.instantiate(context, cls.getName());
            if (parcelable != null) {
                d0.z.d.m.checkNotNullExpressionValue(instantiate, "fragment");
                Bundle bundle = (Bundle) (!(parcelable instanceof Bundle) ? null : parcelable);
                if (bundle == null) {
                    bundle = d.e2(parcelable);
                }
                instantiate.setArguments(bundle);
            }
            d0.z.d.m.checkNotNullExpressionValue(instantiate, "Fragment.instantiate(con….toBundle()\n      }\n    }");
            beginTransaction.replace(i, instantiate, cls.getName());
            if (z2) {
                beginTransaction.addToBackStack(null);
            }
            beginTransaction.commit();
        }
    }

    public final Intent a(Context context, Class<? extends AppComponent> cls, Intent intent) {
        Intent intent2;
        AppLog appLog = AppLog.g;
        String simpleName = context.getClass().getSimpleName();
        d0.z.d.m.checkNotNullExpressionValue(simpleName, "javaClass.simpleName");
        String simpleName2 = cls.getSimpleName();
        d0.z.d.m.checkNotNullExpressionValue(simpleName2, "screen.simpleName");
        appLog.f(simpleName, simpleName2);
        if (intent != null) {
            intent2 = new Intent(intent);
        } else {
            intent2 = new Intent();
        }
        Intent putExtra = intent2.setClass(context, AppActivity.class).putExtra("com.discord.intent.extra.EXTRA_SCREEN", cls);
        d0.z.d.m.checkNotNullExpressionValue(putExtra, "if (extras != null) {\n  …nts.EXTRA_SCREEN, screen)");
        return putExtra;
    }

    public final void f(Context context, ActivityResultLauncher<Intent> activityResultLauncher, Class<? extends AppComponent> cls, Parcelable parcelable) {
        d0.z.d.m.checkNotNullParameter(context, "context");
        d0.z.d.m.checkNotNullParameter(activityResultLauncher, "launcher");
        d0.z.d.m.checkNotNullParameter(cls, "screen");
        Intent intent = null;
        Intent intent2 = (Intent) (!(parcelable instanceof Intent) ? null : parcelable);
        if (intent2 != null) {
            intent = intent2;
        } else if (parcelable != null) {
            intent = d.g2(parcelable);
        }
        activityResultLauncher.launch(a(context, cls, intent));
    }
}
