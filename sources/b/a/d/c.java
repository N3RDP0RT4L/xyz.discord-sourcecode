package b.a.d;

import android.util.TypedValue;
import com.discord.app.AppActivity;
import d0.z.d.o;
import kotlin.jvm.functions.Function2;
/* compiled from: AppActivity.kt */
/* loaded from: classes.dex */
public final class c extends o implements Function2<Integer, Boolean, TypedValue> {
    public final /* synthetic */ AppActivity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public c(AppActivity appActivity) {
        super(2);
        this.this$0 = appActivity;
    }

    @Override // kotlin.jvm.functions.Function2
    public TypedValue invoke(Integer num, Boolean bool) {
        int intValue = num.intValue();
        boolean booleanValue = bool.booleanValue();
        TypedValue typedValue = new TypedValue();
        this.this$0.getTheme().resolveAttribute(intValue, typedValue, booleanValue);
        return typedValue;
    }
}
