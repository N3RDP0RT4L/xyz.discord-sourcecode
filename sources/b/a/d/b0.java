package b.a.d;

import d0.z.d.m;
import d0.z.d.o;
import j0.l.e.k;
import kotlin.jvm.functions.Function1;
import rx.Observable;
/* compiled from: AppTransformers.kt */
/* loaded from: classes.dex */
public final class b0 extends o implements Function1<T, Observable<R>> {
    public final /* synthetic */ Object $switchedValue;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public b0(Object obj) {
        super(1);
        this.$switchedValue = obj;
    }

    @Override // kotlin.jvm.functions.Function1
    public Object invoke(Object obj) {
        k kVar = new k(this.$switchedValue);
        m.checkNotNullExpressionValue(kVar, "Observable.just(switchedValue)");
        return kVar;
    }
}
