package b.a.d;

import com.discord.app.AppTransitionActivity;
import d0.z.d.m;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeMap;
import kotlin.jvm.functions.Function0;
/* compiled from: AppEventHandlerActivity.kt */
/* loaded from: classes.dex */
public abstract class d extends AppTransitionActivity {
    public final TreeMap<Integer, HashMap<String, Function0<Boolean>>> l = new TreeMap<>();

    @Override // com.discord.app.AppTransitionActivity, androidx.activity.ComponentActivity, android.app.Activity
    public void onBackPressed() {
        boolean z2;
        Collection<HashMap<String, Function0<Boolean>>> values = this.l.values();
        m.checkNotNullExpressionValue(values, "backPressHandlers\n        .values");
        Iterator<T> it = values.iterator();
        loop0: while (true) {
            if (!it.hasNext()) {
                z2 = false;
                break;
            }
            Collection<Function0> values2 = ((HashMap) it.next()).values();
            m.checkNotNullExpressionValue(values2, "handlers\n              .values");
            for (Function0 function0 : values2) {
                if (((Boolean) function0.invoke()).booleanValue()) {
                    z2 = true;
                    break loop0;
                }
            }
        }
        if (!z2 && !isFinishing()) {
            try {
                super.onBackPressed();
            } catch (Exception e) {
                if (!(e instanceof IllegalArgumentException) && !(e instanceof IllegalStateException)) {
                    throw e;
                }
            }
        }
    }
}
