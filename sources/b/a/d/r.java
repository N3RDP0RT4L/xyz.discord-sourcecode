package b.a.d;

import j0.k.b;
import java.util.Map;
/* compiled from: AppTransformers.kt */
/* loaded from: classes.dex */
public final class r<T, R> implements b<K, V1> {
    public final /* synthetic */ s j;
    public final /* synthetic */ Map k;

    public r(s sVar, Map map) {
        this.j = sVar;
        this.k = map;
    }

    /* JADX WARN: Type inference failed for: r3v2, types: [java.lang.Object, V1] */
    @Override // j0.k.b
    public final V1 call(K k) {
        return this.j.j.k.invoke(this.k.get(k));
    }
}
