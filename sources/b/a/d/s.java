package b.a.d;

import b.a.d.o;
import j0.k.b;
import java.util.Map;
import rx.Observable;
/* compiled from: AppTransformers.kt */
/* loaded from: classes.dex */
public final class s<T, R> implements b<Map<K, ? extends V>, Observable<? extends Map<K, V1>>> {
    public final /* synthetic */ o.c j;

    public s(o.c cVar) {
        this.j = cVar;
    }

    @Override // j0.k.b
    public Object call(Object obj) {
        Map map = (Map) obj;
        return Observable.A(this.j.j).x(new p(map)).g0(q.j, new r(this, map));
    }
}
