package b.a.d;

import b.a.d.o;
import j0.k.b;
import j0.l.e.k;
import rx.Observable;
/* compiled from: AppTransformers.kt */
/* loaded from: classes.dex */
public final class t<T, R> implements b<T, Observable<? extends T>> {
    public final /* synthetic */ o.d j;

    public t(o.d dVar) {
        this.j = dVar;
    }

    @Override // j0.k.b
    public Object call(Object obj) {
        if (((Boolean) this.j.j.invoke(obj)).booleanValue()) {
            return new k(obj);
        }
        k kVar = new k(this.j.k);
        o.d dVar = this.j;
        return kVar.p(dVar.l, dVar.m);
    }
}
