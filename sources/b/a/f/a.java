package b.a.f;

import android.view.View;
import com.discord.chips_view.ChipsView;
import com.discord.chips_view.ChipsView.a;
import d0.z.d.m;
import java.util.Objects;
/* compiled from: Chip.kt */
/* loaded from: classes.dex */
public final class a<K, T extends ChipsView.a> implements View.OnClickListener {
    public b.a.f.h.a j;
    public boolean k;
    public String l;
    public String m;
    public final K n;
    public final T o;
    public final C0030a p;
    public final ChipsView<K, T> q;

    /* compiled from: Chip.kt */
    /* renamed from: b.a.f.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0030a {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final int f66b;
        public final int c;
        public final int d;
        public final int e;
        public final int f;

        public C0030a(int i, float f, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
            this.a = i;
            this.f66b = i2;
            this.c = i3;
            this.d = i4;
            this.e = i5;
            this.f = i8;
        }
    }

    public a(String str, String str2, K k, T t, C0030a aVar, ChipsView<K, T> chipsView) {
        m.checkNotNullParameter(aVar, "params");
        m.checkNotNullParameter(chipsView, "container");
        this.l = str;
        this.m = str2;
        this.n = k;
        this.o = t;
        this.p = aVar;
        this.q = chipsView;
        String str3 = null;
        if (str == null) {
            this.l = t != null ? t.getDisplayString() : null;
        }
        String str4 = this.l;
        if ((str4 != null ? str4.length() : 0) > 30) {
            String str5 = this.l;
            if (str5 != null) {
                str3 = str5.substring(0, 30);
                m.checkNotNullExpressionValue(str3, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            }
            this.l = m.stringPlus(str3, "...");
        }
    }

    public boolean equals(Object obj) {
        if (obj instanceof a) {
            return m.areEqual(this.o, ((a) obj).o);
        }
        return false;
    }

    public int hashCode() {
        String str = this.l;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.m;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        K k = this.n;
        int hashCode3 = (hashCode2 + (k != null ? k.hashCode() : 0)) * 31;
        T t = this.o;
        int hashCode4 = t != null ? t.hashCode() : 0;
        int hashCode5 = (this.q.hashCode() + ((this.p.hashCode() + ((hashCode3 + hashCode4) * 31)) * 31)) * 31;
        b.a.f.h.a aVar = this.j;
        if (aVar != null) {
            i = aVar.hashCode();
        }
        return c.a(this.k) + ((hashCode5 + i) * 31);
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        m.checkNotNullParameter(view, "v");
        this.q.A.clearFocus();
        ChipsView<K, T> chipsView = this.q;
        Objects.requireNonNull(chipsView);
        m.checkNotNullParameter(this, "chip");
        chipsView.f(this);
        chipsView.e(this);
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("{[Data: ");
        R.append(this.o);
        R.append(']');
        R.append("[Label: ");
        R.append(this.l);
        R.append(']');
        R.append("[ImageDescription: ");
        R.append(this.m);
        R.append(']');
        R.append('}');
        return R.toString();
    }
}
