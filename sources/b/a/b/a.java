package b.a.b;

import b.i.d.e;
import com.discord.api.activity.ActivityPlatform;
import com.discord.api.activity.ActivityPlatformTypeAdapter;
import com.discord.api.activity.ActivityType;
import com.discord.api.activity.ActivityTypeTypeAdapter;
import com.discord.api.application.ApplicationType;
import com.discord.api.application.ApplicationTypeAdapter;
import com.discord.api.auth.OAuthScope;
import com.discord.api.auth.OAuthScopeTypeAdapter;
import com.discord.api.botuikit.ButtonStyle;
import com.discord.api.botuikit.ButtonStyleTypeAdapter;
import com.discord.api.botuikit.ComponentType;
import com.discord.api.botuikit.gson.ComponentRuntimeTypeAdapter;
import com.discord.api.botuikit.gson.ComponentTypeTypeAdapter;
import com.discord.api.commands.ApplicationCommandPermissionType;
import com.discord.api.commands.ApplicationCommandPermissionTypeTypeAdapter;
import com.discord.api.commands.ApplicationCommandType;
import com.discord.api.commands.CommandTypeAdapter;
import com.discord.api.friendsuggestions.AllowedInSuggestionsType;
import com.discord.api.friendsuggestions.AllowedInSuggestionsTypeAdapter;
import com.discord.api.friendsuggestions.FriendSuggestionReasonType;
import com.discord.api.friendsuggestions.FriendSuggestionReasonTypeAdapter;
import com.discord.api.guild.GuildExplicitContentFilter;
import com.discord.api.guild.GuildExplicitContentFilterTypeAdapter;
import com.discord.api.guild.GuildHubType;
import com.discord.api.guild.GuildHubTypeTypeAdapter;
import com.discord.api.guild.GuildMaxVideoChannelUsers;
import com.discord.api.guild.GuildMaxVideoChannelUsersTypeAdapter;
import com.discord.api.guild.GuildVerificationLevel;
import com.discord.api.guild.GuildVerificationLevelTypeAdapter;
import com.discord.api.guildjoinrequest.ApplicationStatus;
import com.discord.api.guildjoinrequest.ApplicationStatusTypeAdapter;
import com.discord.api.guildrolesubscription.GuildRoleSubscriptionBenefitType;
import com.discord.api.guildrolesubscription.GuildRoleSubscriptionBenefitTypeAdapter;
import com.discord.api.guildrolesubscription.PayoutGroupStatus;
import com.discord.api.guildrolesubscription.PayoutGroupStatusTypeAdapter;
import com.discord.api.guildrolesubscription.PayoutGroupType;
import com.discord.api.guildrolesubscription.PayoutGroupTypeAdapter;
import com.discord.api.guildrolesubscription.PayoutStatus;
import com.discord.api.guildrolesubscription.PayoutStatusAdapter;
import com.discord.api.guildscheduledevent.GuildScheduledEventEntityType;
import com.discord.api.guildscheduledevent.GuildScheduledEventEntityTypeTypeAdapter;
import com.discord.api.guildscheduledevent.GuildScheduledEventStatus;
import com.discord.api.guildscheduledevent.GuildScheduledEventStatusTypeAdapter;
import com.discord.api.localizedstring.LocalizedString;
import com.discord.api.localizedstring.LocalizedStringTypeAdapter;
import com.discord.api.message.activity.MessageActivityType;
import com.discord.api.message.activity.MessageActivityTypeTypeAdapter;
import com.discord.api.message.embed.EmbedType;
import com.discord.api.message.embed.EmbedTypeTypeAdapter;
import com.discord.api.premium.PremiumTier;
import com.discord.api.premium.PremiumTierTypeAdapter;
import com.discord.api.premium.PriceTierType;
import com.discord.api.premium.PriceTierTypeAdapter;
import com.discord.api.report.ReportNodeBottomButton;
import com.discord.api.report.ReportNodeBottomButtonTypeAdapter;
import com.discord.api.report.ReportNodeChild;
import com.discord.api.report.ReportNodeChildTypeAdapter;
import com.discord.api.report.ReportNodeElementData;
import com.discord.api.report.ReportNodeElementDataTypeAdapter;
import com.discord.api.science.AnalyticsSchemaTypeAdapter;
import com.discord.api.science.Science;
import com.discord.api.stageinstance.StageInstancePrivacyLevel;
import com.discord.api.stageinstance.StageInstancePrivacyLevelTypeAdapter;
import com.discord.api.sticker.StickerFormatType;
import com.discord.api.sticker.StickerFormatTypeTypeAdapter;
import com.discord.api.sticker.StickerType;
import com.discord.api.sticker.StickerTypeTypeAdapter;
import com.discord.api.user.NsfwAllowance;
import com.discord.api.user.NsfwAllowanceTypeAdapter;
import com.discord.api.user.Phone;
import com.discord.api.user.PhoneTypeAdapter;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.api.utcdatetime.UtcDateTimeTypeAdapter;
import com.discord.nullserializable.NullSerializableTypeAdapterFactory;
import d0.o;
import d0.t.h0;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import java.util.Map;
import kotlin.jvm.functions.Function0;
/* compiled from: TypeAdapterRegistrar.kt */
/* loaded from: classes.dex */
public final class a {
    public static final Map<Class<? extends Object>, Function0<Object>> a = h0.mapOf(o.to(ActivityPlatform.class, b.j), o.to(ActivityType.class, C0028a.D), o.to(ApplicationStatus.class, C0028a.M), o.to(UtcDateTime.class, C0028a.N), o.to(NsfwAllowance.class, C0028a.O), o.to(PremiumTier.class, C0028a.P), o.to(ActivityType.class, C0028a.Q), o.to(Phone.class, C0028a.R), o.to(ApplicationCommandType.class, C0028a.S), o.to(OAuthScope.class, C0028a.j), o.to(GuildHubType.class, C0028a.k), o.to(GuildVerificationLevel.class, C0028a.l), o.to(GuildExplicitContentFilter.class, C0028a.m), o.to(GuildMaxVideoChannelUsers.class, C0028a.n), o.to(ComponentType.class, C0028a.o), o.to(ButtonStyle.class, C0028a.p), o.to(ReportNodeChild.class, C0028a.q), o.to(ReportNodeElementData.class, C0028a.r), o.to(ReportNodeBottomButton.class, C0028a.f52s), o.to(ApplicationCommandPermissionType.class, C0028a.t), o.to(StageInstancePrivacyLevel.class, C0028a.u), o.to(FriendSuggestionReasonType.class, C0028a.v), o.to(AllowedInSuggestionsType.class, C0028a.w), o.to(StickerFormatType.class, C0028a.f53x), o.to(LocalizedString.class, C0028a.f54y), o.to(MessageActivityType.class, C0028a.f55z), o.to(EmbedType.class, C0028a.A), o.to(StickerType.class, C0028a.B), o.to(GuildScheduledEventStatus.class, C0028a.C), o.to(GuildScheduledEventEntityType.class, C0028a.E), o.to(Science.Event.SchemaObject.class, C0028a.F), o.to(GuildRoleSubscriptionBenefitType.class, C0028a.G), o.to(PriceTierType.class, C0028a.H), o.to(ApplicationType.class, C0028a.I), o.to(PayoutStatus.class, C0028a.J), o.to(PayoutGroupType.class, C0028a.K), o.to(PayoutGroupStatus.class, C0028a.L));

    /* renamed from: b  reason: collision with root package name */
    public static final List<b.i.d.o> f51b = n.listOf((Object[]) new b.i.d.o[]{ComponentRuntimeTypeAdapter.INSTANCE.a(), new NullSerializableTypeAdapterFactory()});

    /* compiled from: kotlin-style lambda group */
    /* renamed from: b.a.b.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0028a extends d0.z.d.o implements Function0<Object> {
        public final /* synthetic */ int T;
        public static final C0028a j = new C0028a(0);
        public static final C0028a k = new C0028a(1);
        public static final C0028a l = new C0028a(2);
        public static final C0028a m = new C0028a(3);
        public static final C0028a n = new C0028a(4);
        public static final C0028a o = new C0028a(5);
        public static final C0028a p = new C0028a(6);
        public static final C0028a q = new C0028a(7);
        public static final C0028a r = new C0028a(8);

        /* renamed from: s  reason: collision with root package name */
        public static final C0028a f52s = new C0028a(9);
        public static final C0028a t = new C0028a(10);
        public static final C0028a u = new C0028a(11);
        public static final C0028a v = new C0028a(12);
        public static final C0028a w = new C0028a(13);

        /* renamed from: x  reason: collision with root package name */
        public static final C0028a f53x = new C0028a(14);

        /* renamed from: y  reason: collision with root package name */
        public static final C0028a f54y = new C0028a(15);

        /* renamed from: z  reason: collision with root package name */
        public static final C0028a f55z = new C0028a(16);
        public static final C0028a A = new C0028a(17);
        public static final C0028a B = new C0028a(18);
        public static final C0028a C = new C0028a(19);
        public static final C0028a D = new C0028a(20);
        public static final C0028a E = new C0028a(21);
        public static final C0028a F = new C0028a(22);
        public static final C0028a G = new C0028a(23);
        public static final C0028a H = new C0028a(24);
        public static final C0028a I = new C0028a(25);
        public static final C0028a J = new C0028a(26);
        public static final C0028a K = new C0028a(27);
        public static final C0028a L = new C0028a(28);
        public static final C0028a M = new C0028a(29);
        public static final C0028a N = new C0028a(30);
        public static final C0028a O = new C0028a(31);
        public static final C0028a P = new C0028a(32);
        public static final C0028a Q = new C0028a(33);
        public static final C0028a R = new C0028a(34);
        public static final C0028a S = new C0028a(35);

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public C0028a(int i) {
            super(0);
            this.T = i;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Object invoke() {
            switch (this.T) {
                case 0:
                    return new OAuthScopeTypeAdapter();
                case 1:
                    return new GuildHubTypeTypeAdapter();
                case 2:
                    return new GuildVerificationLevelTypeAdapter();
                case 3:
                    return new GuildExplicitContentFilterTypeAdapter();
                case 4:
                    return new GuildMaxVideoChannelUsersTypeAdapter();
                case 5:
                    return new ComponentTypeTypeAdapter();
                case 6:
                    return new ButtonStyleTypeAdapter();
                case 7:
                    return new ReportNodeChildTypeAdapter();
                case 8:
                    return new ReportNodeElementDataTypeAdapter();
                case 9:
                    return new ReportNodeBottomButtonTypeAdapter();
                case 10:
                    return new ApplicationCommandPermissionTypeTypeAdapter();
                case 11:
                    return new StageInstancePrivacyLevelTypeAdapter();
                case 12:
                    return new FriendSuggestionReasonTypeAdapter();
                case 13:
                    return new AllowedInSuggestionsTypeAdapter();
                case 14:
                    return new StickerFormatTypeTypeAdapter();
                case 15:
                    return new LocalizedStringTypeAdapter();
                case 16:
                    return new MessageActivityTypeTypeAdapter();
                case 17:
                    return new EmbedTypeTypeAdapter();
                case 18:
                    return new StickerTypeTypeAdapter();
                case 19:
                    return new GuildScheduledEventStatusTypeAdapter();
                case 20:
                    return new ActivityTypeTypeAdapter();
                case 21:
                    return new GuildScheduledEventEntityTypeTypeAdapter();
                case 22:
                    return new AnalyticsSchemaTypeAdapter();
                case 23:
                    return new GuildRoleSubscriptionBenefitTypeAdapter();
                case 24:
                    return new PriceTierTypeAdapter();
                case 25:
                    return new ApplicationTypeAdapter();
                case 26:
                    return new PayoutStatusAdapter();
                case 27:
                    return new PayoutGroupTypeAdapter();
                case 28:
                    return new PayoutGroupStatusTypeAdapter();
                case 29:
                    return new ApplicationStatusTypeAdapter();
                case 30:
                    return new UtcDateTimeTypeAdapter();
                case 31:
                    return new NsfwAllowanceTypeAdapter();
                case 32:
                    return new PremiumTierTypeAdapter();
                case 33:
                    return new ActivityTypeTypeAdapter();
                case 34:
                    return new PhoneTypeAdapter();
                case 35:
                    return new CommandTypeAdapter();
                default:
                    throw null;
            }
        }
    }

    /* compiled from: TypeAdapterRegistrar.kt */
    /* loaded from: classes.dex */
    public static final class b extends d0.z.d.o implements Function0<ActivityPlatformTypeAdapter> {
        public static final b j = new b();

        public b() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public ActivityPlatformTypeAdapter invoke() {
            return new ActivityPlatformTypeAdapter();
        }
    }

    public static final e a(e eVar) {
        m.checkNotNullParameter(eVar, "$this$registerDiscordApiTypeAdapters");
        for (Map.Entry<Class<? extends Object>, Function0<Object>> entry : a.entrySet()) {
            eVar.b(entry.getKey(), entry.getValue().invoke());
        }
        m.checkNotNullParameter(eVar, "$this$registerDiscordApiTypeAdapterFactories");
        for (b.i.d.o oVar : f51b) {
            eVar.e.add(oVar);
        }
        return eVar;
    }
}
