package b.a.g;

import d0.z.d.m;
/* compiled from: ColorUtils.kt */
/* loaded from: classes.dex */
public final class c {
    public static final void a(int i, int i2, int i3, float[] fArr) {
        float f;
        float f2;
        m.checkNotNullParameter(fArr, "hsl");
        float f3 = i / 255.0f;
        float f4 = i2 / 255.0f;
        float f5 = i3 / 255.0f;
        float max = Math.max(f3, Math.max(f4, f5));
        float min = Math.min(f3, Math.min(f4, f5));
        float f6 = max - min;
        float f7 = (max + min) / 2.0f;
        if (max == min) {
            f2 = 0.0f;
            f = 0.0f;
        } else {
            f2 = max == f3 ? ((f4 - f5) / f6) % 6.0f : max == f4 ? ((f5 - f3) / f6) + 2.0f : ((f3 - f4) / f6) + 4.0f;
            f = f6 / (1.0f - Math.abs((2.0f * f7) - 1.0f));
        }
        fArr[0] = (f2 * 60.0f) % 360.0f;
        fArr[1] = f;
        fArr[2] = f7;
    }
}
