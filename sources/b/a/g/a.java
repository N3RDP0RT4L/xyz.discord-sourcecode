package b.a.g;

import android.graphics.Color;
import android.util.SparseIntArray;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ColorCutQuantizer.kt */
/* loaded from: classes.dex */
public final class a {
    public static final b a = new b(null);

    /* renamed from: b  reason: collision with root package name */
    public final float[] f68b = new float[3];
    public final int[] c;
    public final SparseIntArray d;
    public final List<d> e;

    /* compiled from: ColorCutQuantizer.kt */
    /* renamed from: b.a.g.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0031a<T> implements Comparator<c> {
        public static final C0031a j = new C0031a();

        @Override // java.util.Comparator
        public int compare(c cVar, c cVar2) {
            return cVar2.b() - cVar.b();
        }
    }

    /* compiled from: ColorCutQuantizer.kt */
    /* loaded from: classes.dex */
    public static final class b {
        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: ColorCutQuantizer.kt */
    /* loaded from: classes.dex */
    public final class c {
        public int a;

        /* renamed from: b  reason: collision with root package name */
        public int f69b;
        public int c;
        public int d;
        public int e;
        public int f;
        public final int g;
        public int h;

        public c(int i, int i2) {
            this.g = i;
            this.h = i2;
            a();
        }

        public final void a() {
            this.e = 255;
            this.c = 255;
            this.a = 255;
            this.f = 0;
            this.d = 0;
            this.f69b = 0;
            int i = this.g;
            int i2 = this.h;
            if (i <= i2) {
                while (true) {
                    int i3 = a.this.c[i];
                    int red = Color.red(i3);
                    int green = Color.green(i3);
                    int blue = Color.blue(i3);
                    if (red > this.f69b) {
                        this.f69b = red;
                    }
                    if (red < this.a) {
                        this.a = red;
                    }
                    if (green > this.d) {
                        this.d = green;
                    }
                    if (green < this.c) {
                        this.c = green;
                    }
                    if (blue > this.f) {
                        this.f = blue;
                    }
                    if (blue < this.e) {
                        this.e = blue;
                    }
                    if (i != i2) {
                        i++;
                    } else {
                        return;
                    }
                }
            }
        }

        public final int b() {
            return ((this.f - this.e) + 1) * ((this.d - this.c) + 1) * ((this.f69b - this.a) + 1);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:125:0x025b  */
    /* JADX WARN: Removed duplicated region for block: B:137:0x00a7 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:152:0x0268 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:155:0x01bb A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:37:0x009b  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x00a0  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public a(b.a.g.b r18, int r19, kotlin.jvm.internal.DefaultConstructorMarker r20) {
        /*
            Method dump skipped, instructions count: 637
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.g.a.<init>(b.a.g.b, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public static final void a(a aVar, int i, int i2, int i3) {
        Objects.requireNonNull(aVar);
        if (i == -2) {
            while (i2 <= i3) {
                int[] iArr = aVar.c;
                int i4 = iArr[i2];
                iArr[i2] = Color.rgb((i4 >> 8) & 255, (i4 >> 16) & 255, i4 & 255);
                i2++;
            }
        } else if (i == -1) {
            while (i2 <= i3) {
                int[] iArr2 = aVar.c;
                int i5 = iArr2[i2];
                iArr2[i2] = Color.rgb(i5 & 255, (i5 >> 8) & 255, (i5 >> 16) & 255);
                i2++;
            }
        }
    }
}
