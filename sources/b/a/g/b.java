package b.a.g;

import d0.z.d.m;
import java.util.Arrays;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ColorHistogram.kt */
/* loaded from: classes.dex */
public final class b {
    public static final a a = new a(null);

    /* renamed from: b  reason: collision with root package name */
    public final int[] f70b;
    public final int[] c;
    public final int d;

    /* compiled from: ColorHistogram.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    public b(int[] iArr) {
        int i;
        m.checkNotNullParameter(iArr, "pixels");
        Arrays.sort(iArr);
        int i2 = 0;
        if (iArr.length < 2) {
            i = iArr.length;
        } else {
            int i3 = iArr[0];
            int length = iArr.length;
            int i4 = i3;
            i = 1;
            for (int i5 = 1; i5 < length; i5++) {
                if (iArr[i5] != i4) {
                    i4 = iArr[i5];
                    i++;
                }
            }
        }
        this.d = i;
        int[] iArr2 = new int[i];
        this.f70b = iArr2;
        int[] iArr3 = new int[i];
        this.c = iArr3;
        if (!(iArr.length == 0)) {
            int i6 = iArr[0];
            iArr2[0] = i6;
            iArr3[0] = 1;
            if (iArr.length != 1) {
                int length2 = iArr.length;
                for (int i7 = 1; i7 < length2; i7++) {
                    if (iArr[i7] == i6) {
                        int[] iArr4 = this.c;
                        iArr4[i2] = iArr4[i2] + 1;
                    } else {
                        i6 = iArr[i7];
                        i2++;
                        this.f70b[i2] = i6;
                        this.c[i2] = 1;
                    }
                }
            }
        }
    }
}
