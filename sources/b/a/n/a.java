package b.a.n;

import android.view.View;
import com.discord.overlay.OverlayManager;
/* compiled from: OverlayManager.kt */
/* loaded from: classes.dex */
public final class a implements View.OnTouchListener {
    public final /* synthetic */ OverlayManager j;

    public a(OverlayManager overlayManager) {
        this.j = overlayManager;
    }

    /* JADX WARN: Removed duplicated region for block: B:13:0x0030  */
    /* JADX WARN: Removed duplicated region for block: B:21:0x00aa  */
    @Override // android.view.View.OnTouchListener
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final boolean onTouch(android.view.View r12, android.view.MotionEvent r13) {
        /*
            r11 = this;
            java.lang.String r0 = "event"
            d0.z.d.m.checkNotNullExpressionValue(r13, r0)
            int r0 = r13.getAction()
            r1 = 1
            r2 = 0
            r3 = 2
            if (r0 == r1) goto L12
            if (r0 == r3) goto L12
            goto Laf
        L12:
            com.discord.overlay.OverlayManager r0 = r11.j
            b.a.n.h.a r0 = r0.n
            if (r0 == 0) goto L2c
            float r4 = r13.getRawX()
            int r4 = (int) r4
            float r5 = r13.getRawY()
            int r5 = (int) r5
            android.graphics.Rect r0 = r0.n
            boolean r0 = r0.contains(r4, r5)
            if (r0 != r1) goto L2c
            r0 = 1
            goto L2d
        L2c:
            r0 = 0
        L2d:
            r4 = 0
            if (r0 == 0) goto Laa
            java.lang.String r0 = "null cannot be cast to non-null type com.discord.overlay.views.OverlayBubbleWrap"
            java.util.Objects.requireNonNull(r12, r0)
            r5 = r12
            com.discord.overlay.views.OverlayBubbleWrap r5 = (com.discord.overlay.views.OverlayBubbleWrap) r5
            int r12 = r13.getAction()
            if (r12 != r1) goto La4
            com.discord.overlay.OverlayManager r12 = r11.j
            java.util.Objects.requireNonNull(r12)
            android.graphics.Point r13 = r5.w
            if (r13 == 0) goto L71
            android.graphics.Point r0 = r5.v
            int r2 = r13.x
            r0.x = r2
            int r13 = r13.y
            r0.y = r13
            java.lang.Class r13 = r5.getClass()
            java.lang.String r13 = r13.getSimpleName()
            java.lang.String r0 = "Moved to anchor ["
            java.lang.StringBuilder r0 = b.d.b.a.a.R(r0)
            android.graphics.Point r2 = r5.v
            r0.append(r2)
            r2 = 93
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r13, r0)
        L71:
            r12.c(r4)
            android.content.Context r13 = r5.getContext()
            int r0 = com.discord.overlay.R.a.fade_out
            android.animation.Animator r13 = android.animation.AnimatorInflater.loadAnimator(r13, r0)
            r13.setTarget(r5)
            b.a.n.e r0 = new b.a.n.e
            r0.<init>(r12, r5)
            r13.addListener(r0)
            r13.start()
            float r12 = r5.getX()
            int r6 = (int) r12
            float r12 = r5.getY()
            int r12 = (int) r12
            int r13 = r5.getHeight()
            int r13 = r13 / r3
            int r7 = r13 + r12
            r8 = 0
            r9 = 4
            r10 = 0
            com.discord.overlay.views.OverlayBubbleWrap.c(r5, r6, r7, r8, r9, r10)
            return r1
        La4:
            com.discord.overlay.OverlayManager r12 = r11.j
            r12.c(r5)
            goto Laf
        Laa:
            com.discord.overlay.OverlayManager r12 = r11.j
            r12.c(r4)
        Laf:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.n.a.onTouch(android.view.View, android.view.MotionEvent):boolean");
    }
}
