package b.a.n;

import android.animation.Animator;
import com.discord.overlay.OverlayManager;
import com.discord.overlay.views.OverlayBubbleWrap;
import d0.z.d.m;
/* compiled from: Animator.kt */
/* loaded from: classes.dex */
public final class e implements Animator.AnimatorListener {
    public final /* synthetic */ OverlayManager a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ OverlayBubbleWrap f245b;

    public e(OverlayManager overlayManager, OverlayBubbleWrap overlayBubbleWrap) {
        this.a = overlayManager;
        this.f245b = overlayBubbleWrap;
    }

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        m.checkNotNullParameter(animator, "animator");
    }

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        m.checkNotNullParameter(animator, "animator");
        if (this.a.n != null) {
            OverlayBubbleWrap overlayBubbleWrap = this.f245b;
            m.checkNotNullParameter(overlayBubbleWrap, "bubble");
            m.checkNotNullParameter(overlayBubbleWrap, "bubble");
        }
        f fVar = this.a.o;
        if (fVar != null) {
            fVar.b(this.f245b);
        }
    }

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationRepeat(Animator animator) {
        m.checkNotNullParameter(animator, "animator");
    }

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        m.checkNotNullParameter(animator, "animator");
    }
}
