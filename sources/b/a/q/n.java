package b.a.q;

import b.d.b.a.a;
/* compiled from: MediaSinkWantsLadder.kt */
/* loaded from: classes.dex */
public final class n {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final int f270b;
    public final double c;
    public final int d;
    public final int e;

    public n(int i, int i2, double d, int i3, int i4) {
        this.a = i;
        this.f270b = i2;
        this.c = d;
        this.d = i3;
        this.e = i4;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof n)) {
            return false;
        }
        n nVar = (n) obj;
        return this.a == nVar.a && this.f270b == nVar.f270b && Double.compare(this.c, nVar.c) == 0 && this.d == nVar.d && this.e == nVar.e;
    }

    public int hashCode() {
        return ((((Double.doubleToLongBits(this.c) + (((this.a * 31) + this.f270b) * 31)) * 31) + this.d) * 31) + this.e;
    }

    public String toString() {
        StringBuilder R = a.R("ResolutionBudget(width=");
        R.append(this.a);
        R.append(", height=");
        R.append(this.f270b);
        R.append(", budgetPortion=");
        R.append(this.c);
        R.append(", mutedFramerate=");
        R.append(this.d);
        R.append(", framerate=");
        return a.A(R, this.e, ")");
    }
}
