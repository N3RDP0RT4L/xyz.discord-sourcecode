package b.a.q.k0;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import androidx.annotation.MainThread;
import b.a.q.l0.a;
import b.c.a.a0.d;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.rtcconnection.enums.AudioManagerBroadcastAction;
import com.discord.rtcconnection.enums.ScoAudioState;
import d0.z.d.m;
import java.util.Objects;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.webrtc.MediaStreamTrack;
import org.webrtc.ThreadUtils;
/* compiled from: AudioManagerBroadcastReceiver.kt */
/* loaded from: classes.dex */
public final class a extends BroadcastReceiver {
    public static final C0039a a = new C0039a(null);

    /* renamed from: b  reason: collision with root package name */
    public final Context f257b;
    public final h c;

    /* compiled from: AudioManagerBroadcastReceiver.kt */
    /* renamed from: b.a.q.k0.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0039a {
        public C0039a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    public a(Context context, h hVar) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(hVar, "listener");
        this.f257b = context;
        this.c = hVar;
    }

    @Override // android.content.BroadcastReceiver
    @MainThread
    public void onReceive(Context context, Intent intent) {
        b.a.q.l0.a aVar;
        m.checkNotNullParameter(context, "context");
        ThreadUtils.checkIsOnMainThread();
        if (intent != null) {
            Objects.requireNonNull(AudioManagerBroadcastAction.Companion);
            m.checkNotNullParameter(intent, "intent");
            String action = intent.getAction();
            AudioManagerBroadcastAction audioManagerBroadcastAction = null;
            boolean z2 = false;
            if (action != null) {
                m.checkNotNullParameter(action, "action");
                AudioManagerBroadcastAction[] values = AudioManagerBroadcastAction.values();
                int i = 0;
                while (true) {
                    if (i >= 5) {
                        break;
                    }
                    AudioManagerBroadcastAction audioManagerBroadcastAction2 = values[i];
                    if (m.areEqual(audioManagerBroadcastAction2.getAction(), action)) {
                        audioManagerBroadcastAction = audioManagerBroadcastAction2;
                        break;
                    }
                    i++;
                }
            }
            if (audioManagerBroadcastAction == null) {
                StringBuilder R = b.d.b.a.a.R("unable to parse AudioManagerBroadcastAction for action: ");
                R.append(intent.getAction());
                d.f1("AudioManagerBroadcastReceiver", R.toString());
                return;
            }
            d.e1("AudioManagerBroadcastReceiver", "onReceive: action = " + audioManagerBroadcastAction);
            try {
                h hVar = this.c;
                int ordinal = audioManagerBroadcastAction.ordinal();
                if (ordinal == 0) {
                    hVar.c(context);
                } else if (ordinal == 1) {
                    m.checkNotNullParameter(intent, "intent");
                    if (m.areEqual(intent.getAction(), "android.intent.action.HEADSET_PLUG")) {
                        if (!(intent.getIntExtra("state", 0) == 1)) {
                            aVar = a.b.a;
                        } else {
                            String stringExtra = intent.getStringExtra(ModelAuditLogEntry.CHANGE_KEY_NAME);
                            if (stringExtra == null) {
                                stringExtra = "unknown";
                            }
                            m.checkNotNullExpressionValue(stringExtra, "intent.getStringExtra(\"name\") ?: \"unknown\"");
                            if (intent.getIntExtra("microphone", 0) == 1) {
                                z2 = true;
                            }
                            aVar = new a.C0040a(stringExtra, z2);
                        }
                        hVar.e(context, aVar);
                        return;
                    }
                    throw new IllegalArgumentException("Failed requirement.".toString());
                } else if (ordinal == 2) {
                    hVar.b(context, ScoAudioState.Companion.b(intent));
                } else if (ordinal == 3) {
                    m.checkNotNullParameter(context, "$this$isMicrophoneMute");
                    m.checkNotNullParameter(context, "$this$getAudioManager");
                    Object systemService = context.getSystemService(MediaStreamTrack.AUDIO_TRACK_KIND);
                    Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.media.AudioManager");
                    hVar.a(context, ((AudioManager) systemService).isMicrophoneMute());
                } else if (ordinal == 4) {
                    m.checkNotNullParameter(context, "$this$isSpeakerphoneOn");
                    m.checkNotNullParameter(context, "$this$getAudioManager");
                    Object systemService2 = context.getSystemService(MediaStreamTrack.AUDIO_TRACK_KIND);
                    Objects.requireNonNull(systemService2, "null cannot be cast to non-null type android.media.AudioManager");
                    hVar.d(context, ((AudioManager) systemService2).isSpeakerphoneOn());
                }
            } catch (Throwable th) {
                d.c1("AudioManagerBroadcastReceiver", "error handling " + audioManagerBroadcastAction, th);
            }
        }
    }
}
