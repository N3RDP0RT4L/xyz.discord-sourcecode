package b.a.q.k0;

import android.media.AudioManager;
import b.c.a.a0.d;
import com.discord.rtcconnection.audio.DiscordAudioManager;
/* compiled from: DiscordAudioManager.kt */
/* loaded from: classes.dex */
public final class e implements AudioManager.OnAudioFocusChangeListener {
    public final /* synthetic */ DiscordAudioManager j;

    public e(DiscordAudioManager discordAudioManager) {
        this.j = discordAudioManager;
    }

    @Override // android.media.AudioManager.OnAudioFocusChangeListener
    public final void onAudioFocusChange(int i) {
        d.b1("DiscordAudioManager", "[AudioFocus] new focus: " + i);
        if (i == -3 || i == -2) {
            this.j.i(false);
        } else if (i == 1) {
            this.j.i(true);
            this.j.l();
        }
    }
}
