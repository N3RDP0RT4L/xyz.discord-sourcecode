package b.a.q.k0;

import android.content.Context;
import androidx.annotation.MainThread;
import b.a.q.l0.a;
import com.discord.rtcconnection.enums.ScoAudioState;
/* compiled from: OnAudioManagerBroadcastListener.kt */
@MainThread
/* loaded from: classes.dex */
public interface h {
    void a(Context context, boolean z2);

    void b(Context context, ScoAudioState.b bVar);

    void c(Context context);

    void d(Context context, boolean z2);

    void e(Context context, a aVar);
}
