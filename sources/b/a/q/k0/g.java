package b.a.q.k0;

import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: EchoCancellation.kt */
/* loaded from: classes.dex */
public final class g {
    public volatile boolean d;
    public final boolean e;
    public final boolean f;
    public final boolean g;
    public static final a c = new a(null);
    public static final g a = new g(true, false, false);

    /* renamed from: b  reason: collision with root package name */
    public static final g f260b = new g(true, false, false);

    /* compiled from: EchoCancellation.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    public g(boolean z2, boolean z3, boolean z4) {
        this.e = z2;
        this.f = z3;
        this.g = z4;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof g)) {
            return false;
        }
        g gVar = (g) obj;
        return this.e == gVar.e && this.f == gVar.f && this.g == gVar.g;
    }

    public int hashCode() {
        boolean z2 = this.e;
        int i = 1;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int i4 = i2 * 31;
        boolean z3 = this.f;
        if (z3) {
            z3 = true;
        }
        int i5 = z3 ? 1 : 0;
        int i6 = z3 ? 1 : 0;
        int i7 = (i4 + i5) * 31;
        boolean z4 = this.g;
        if (!z4) {
            i = z4 ? 1 : 0;
        }
        return i7 + i;
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("EchoCancellation(shouldEarlyEnableHwAec=");
        R.append(this.e);
        R.append(", alwaysEnableAec=");
        R.append(this.f);
        R.append(", disableSwAecWhenHwIsEnabled=");
        return b.d.b.a.a.M(R, this.g, ")");
    }
}
