package b.a.q.k0;

import android.content.Context;
import android.os.Process;
import b.c.a.a0.d;
import d0.z.d.m;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: AudioPermissions.kt */
/* loaded from: classes.dex */
public final class b {
    public static final a a = new a(null);

    /* renamed from: b  reason: collision with root package name */
    public final boolean f258b;
    public final boolean c;
    public final boolean d;

    /* compiled from: AudioPermissions.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    public b(Context context) {
        m.checkNotNullParameter(context, "context");
        boolean z2 = true;
        boolean z3 = context.checkPermission("android.permission.MODIFY_AUDIO_SETTINGS", Process.myPid(), Process.myUid()) == 0;
        boolean z4 = context.checkPermission("android.permission.RECORD_AUDIO", Process.myPid(), Process.myUid()) == 0;
        z2 = context.checkPermission("android.permission.BLUETOOTH", Process.myPid(), Process.myUid()) != 0 ? false : z2;
        this.f258b = z3;
        this.c = z4;
        this.d = z2;
        if (!z3) {
            d.f1("AudioPermissions", "MODIFY_AUDIO_SETTINGS permission is missing");
        }
        if (!z4) {
            d.f1("AudioPermissions", "RECORD_AUDIO permission is missing");
        }
        if (!z2) {
            d.f1("AudioPermissions", "BLUETOOTH permission is missing");
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        return this.f258b == bVar.f258b && this.c == bVar.c && this.d == bVar.d;
    }

    public int hashCode() {
        boolean z2 = this.f258b;
        int i = 1;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int i4 = i2 * 31;
        boolean z3 = this.c;
        if (z3) {
            z3 = true;
        }
        int i5 = z3 ? 1 : 0;
        int i6 = z3 ? 1 : 0;
        int i7 = (i4 + i5) * 31;
        boolean z4 = this.d;
        if (!z4) {
            i = z4 ? 1 : 0;
        }
        return i7 + i;
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("AudioPermissions(hasModifyAudioSettingsPermission=");
        R.append(this.f258b);
        R.append(", hasRecordAudioPermission=");
        R.append(this.c);
        R.append(", hasBluetoothPermission=");
        return b.d.b.a.a.M(R, this.d, ")");
    }
}
