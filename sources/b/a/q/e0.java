package b.a.q;

import andhook.lib.xposed.ClassUtils;
import b.d.b.a.a;
import com.discord.rtcconnection.RtcConnection;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: RtcConnection.kt */
/* loaded from: classes.dex */
public final class e0 extends o implements Function1<Exception, Unit> {
    public final /* synthetic */ RtcConnection this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public e0(RtcConnection rtcConnection) {
        super(1);
        this.this$0 = rtcConnection;
    }

    @Override // kotlin.jvm.functions.Function1
    public Unit invoke(Exception exc) {
        Exception exc2 = exc;
        m.checkNotNullParameter(exc2, "it");
        RtcConnection rtcConnection = this.this$0;
        StringBuilder R = a.R("Error occurred while connecting to RTC server: ");
        R.append(exc2.getMessage());
        R.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
        RtcConnection.j(rtcConnection, true, R.toString(), null, false, 12);
        return Unit.a;
    }
}
