package b.a.q;

import com.discord.rtcconnection.EncodeQuality;
import com.google.android.material.shadow.ShadowDrawableWrapper;
import d0.t.n0;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: MediaSinkWantsLadder.kt */
/* loaded from: classes.dex */
public final class e {
    public static final b a = new b(null);

    /* renamed from: b  reason: collision with root package name */
    public final int f253b;
    public final Map<EncodeQuality, n> c;
    public final List<c> d;
    public final j0 e;

    /* compiled from: MediaSinkWantsLadder.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final int f254b;
        public final double c;
        public final int d;

        public a(int i, int i2, double d, int i3) {
            this.a = i;
            this.f254b = i2;
            this.c = d;
            this.d = i3;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return this.a == aVar.a && this.f254b == aVar.f254b && Double.compare(this.c, aVar.c) == 0 && this.d == aVar.d;
        }

        public int hashCode() {
            return ((Double.doubleToLongBits(this.c) + (((this.a * 31) + this.f254b) * 31)) * 31) + this.d;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("CandidateResolution(width=");
            R.append(this.a);
            R.append(", height=");
            R.append(this.f254b);
            R.append(", budgetPortion=");
            R.append(this.c);
            R.append(", pixelCount=");
            return b.d.b.a.a.A(R, this.d, ")");
        }
    }

    /* compiled from: MediaSinkWantsLadder.kt */
    /* loaded from: classes.dex */
    public static final class b {
        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: MediaSinkWantsLadder.kt */
    /* loaded from: classes.dex */
    public static final class c {
        public final n a;

        /* renamed from: b  reason: collision with root package name */
        public final int f255b;
        public final EncodeQuality c;

        public c(n nVar, int i, EncodeQuality encodeQuality) {
            m.checkNotNullParameter(nVar, "budget");
            m.checkNotNullParameter(encodeQuality, "wantValue");
            this.a = nVar;
            this.f255b = i;
            this.c = encodeQuality;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof c)) {
                return false;
            }
            c cVar = (c) obj;
            return m.areEqual(this.a, cVar.a) && this.f255b == cVar.f255b && m.areEqual(this.c, cVar.c);
        }

        public int hashCode() {
            n nVar = this.a;
            int i = 0;
            int hashCode = (((nVar != null ? nVar.hashCode() : 0) * 31) + this.f255b) * 31;
            EncodeQuality encodeQuality = this.c;
            if (encodeQuality != null) {
                i = encodeQuality.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("OrderedLadder(budget=");
            R.append(this.a);
            R.append(", pixelCount=");
            R.append(this.f255b);
            R.append(", wantValue=");
            R.append(this.c);
            R.append(")");
            return R.toString();
        }
    }

    public e(j0 j0Var, int i) {
        n nVar;
        Object obj;
        int i2;
        EncodeQuality encodeQuality;
        boolean z2;
        j0 j0Var2 = (i & 1) != 0 ? f.a : null;
        m.checkNotNullParameter(j0Var2, "options");
        this.e = j0Var2;
        b.a.q.b bVar = j0Var2.a;
        int i3 = bVar.a * bVar.f252b;
        this.f253b = i3;
        Set of = n0.setOf((Object[]) new Double[]{Double.valueOf((double) ShadowDrawableWrapper.COS_45), Double.valueOf(4.0d), Double.valueOf(8.0d), Double.valueOf(10.0d)});
        ArrayList arrayList = new ArrayList();
        for (int i4 = 1; i4 < 4096; i4++) {
            double d = i4;
            double d2 = (d * 16.0d) / 9.0d;
            if (of.contains(Double.valueOf(d2 % 16.0d)) && of.contains(Double.valueOf(d % 16.0d))) {
                double d3 = d * d2;
                arrayList.add(new a((int) d2, i4, d3 / i3, (int) d3));
            }
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        EncodeQuality encodeQuality2 = EncodeQuality.Hundred;
        int i5 = 1;
        int i6 = 0;
        while (i5 <= 25) {
            ListIterator listIterator = arrayList.listIterator(arrayList.size());
            while (true) {
                if (!listIterator.hasPrevious()) {
                    obj = null;
                    break;
                }
                obj = listIterator.previous();
                if (((a) obj).d * i5 <= i3) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    break;
                }
            }
            a aVar = (a) obj;
            aVar = aVar == null ? (a) u.first((List<? extends Object>) arrayList) : aVar;
            if (i6 != aVar.a) {
                i2 = i3;
                linkedHashMap.put(encodeQuality2, new n(aVar.a, aVar.f254b, aVar.c, encodeQuality2.compareTo(EncodeQuality.Twenty) <= 0 ? 12 : 20, 30));
                EncodeQuality[] values = EncodeQuality.values();
                int i7 = 10;
                while (true) {
                    if (i7 < 0) {
                        encodeQuality = null;
                        break;
                    }
                    encodeQuality = values[i7];
                    if (encodeQuality.getValue() < encodeQuality2.getValue()) {
                        break;
                    }
                    i7--;
                }
                encodeQuality2 = encodeQuality == null ? EncodeQuality.Zero : encodeQuality;
                i6 = aVar.a;
            } else {
                i2 = i3;
            }
            i5++;
            i3 = i2;
        }
        this.c = linkedHashMap;
        EncodeQuality[] values2 = EncodeQuality.values();
        ArrayList arrayList2 = new ArrayList(11);
        for (int i8 = 0; i8 < 11; i8++) {
            EncodeQuality encodeQuality3 = values2[i8];
            arrayList2.add((encodeQuality3 == EncodeQuality.Zero || (nVar = (n) linkedHashMap.get(encodeQuality3)) == null) ? null : new c(nVar, nVar.f270b * nVar.a, encodeQuality3));
        }
        this.d = u.filterNotNull(arrayList2);
    }
}
