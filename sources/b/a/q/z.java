package b.a.q;

import com.discord.rtcconnection.EncodeQuality;
import com.discord.rtcconnection.RtcConnection;
import d0.o;
import java.util.Map;
import kotlin.Pair;
import rx.functions.Func2;
/* compiled from: RtcConnection.kt */
/* loaded from: classes.dex */
public final class z<T1, T2, R> implements Func2<Map<String, ? extends EncodeQuality>, RtcConnection.StateChange, Pair<? extends Map<String, ? extends EncodeQuality>, ? extends RtcConnection.State>> {
    public static final z j = new z();

    @Override // rx.functions.Func2
    public Pair<? extends Map<String, ? extends EncodeQuality>, ? extends RtcConnection.State> call(Map<String, ? extends EncodeQuality> map, RtcConnection.StateChange stateChange) {
        return o.to(map, stateChange.a);
    }
}
