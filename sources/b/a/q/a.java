package b.a.q;

import d0.z.d.m;
/* compiled from: MediaSinkWantsLadder.kt */
/* loaded from: classes.dex */
public final class a {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final int f251b;
    public final Integer c;

    public a(int i, int i2, Integer num) {
        this.a = i;
        this.f251b = i2;
        this.c = num;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof a)) {
            return false;
        }
        a aVar = (a) obj;
        return this.a == aVar.a && this.f251b == aVar.f251b && m.areEqual(this.c, aVar.c);
    }

    public int hashCode() {
        int i = ((this.a * 31) + this.f251b) * 31;
        Integer num = this.c;
        return i + (num != null ? num.hashCode() : 0);
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("Bitrate(min=");
        R.append(this.a);
        R.append(", max=");
        R.append(this.f251b);
        R.append(", target=");
        return b.d.b.a.a.E(R, this.c, ")");
    }
}
