package b.a.q;

import com.discord.rtcconnection.RtcConnection;
import d0.z.d.o;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLSocketFactory;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: RtcConnection.kt */
/* loaded from: classes.dex */
public final class w extends o implements Function0<Unit> {
    public final /* synthetic */ String $endpoint;
    public final /* synthetic */ SSLSocketFactory $sslSocketFactory;
    public final /* synthetic */ String $token;
    public final /* synthetic */ RtcConnection this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public w(RtcConnection rtcConnection, String str, String str2, SSLSocketFactory sSLSocketFactory) {
        super(0);
        this.this$0 = rtcConnection;
        this.$endpoint = str;
        this.$token = str2;
        this.$sslSocketFactory = sSLSocketFactory;
    }

    @Override // kotlin.jvm.functions.Function0
    public Unit invoke() {
        RtcConnection rtcConnection = this.this$0;
        rtcConnection.C = Long.valueOf(rtcConnection.W.currentTimeMillis());
        RtcConnection rtcConnection2 = this.this$0;
        rtcConnection2.E++;
        rtcConnection2.F = rtcConnection2.U.a().c0(2L, TimeUnit.SECONDS).W(new t(this), new v(this));
        return Unit.a;
    }
}
