package b.a.q;

import b.a.q.n0.a;
import com.discord.rtcconnection.RtcConnection;
import d0.g0.t;
import d0.t.g0;
import d0.z.d.m;
import d0.z.d.o;
import java.net.URI;
import javax.net.ssl.SSLSocketFactory;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: RtcConnection.kt */
/* loaded from: classes.dex */
public final class s extends o implements Function0<Unit> {
    public final /* synthetic */ t this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public s(t tVar) {
        super(0);
        this.this$0 = tVar;
    }

    @Override // kotlin.jvm.functions.Function0
    public Unit invoke() {
        w wVar = this.this$0.j;
        RtcConnection rtcConnection = wVar.this$0;
        String str = wVar.$endpoint;
        String str2 = wVar.$token;
        SSLSocketFactory sSLSocketFactory = wVar.$sslSocketFactory;
        rtcConnection.q.succeed();
        String str3 = sSLSocketFactory != null ? "wss" : "ws";
        if (str == null || str.length() == 0) {
            a aVar = rtcConnection.w;
            if (aVar != null) {
                aVar.c();
            }
            rtcConnection.u(RtcConnection.State.b.a);
        } else {
            String replace$default = t.replace$default(str, ".gg", ".media", false, 4, (Object) null);
            String w = b.d.b.a.a.w(str3, "://", replace$default);
            String replace$default2 = t.replace$default(w, ":80", ":443", false, 4, (Object) null);
            a aVar2 = rtcConnection.w;
            if (aVar2 != null) {
                aVar2.c();
            }
            if (rtcConnection.u) {
                RtcConnection.j(rtcConnection, false, "Connect called on destroyed instance.", null, false, 4);
            } else if (str2 == null) {
                RtcConnection.j(rtcConnection, false, "Connect called with no token.", null, false, 12);
            } else {
                rtcConnection.r("connecting via endpoint: " + replace$default + " token: " + str2);
                try {
                    URI uri = new URI(w);
                    rtcConnection.f2751y = uri.getHost();
                    rtcConnection.f2752z = Integer.valueOf(uri.getPort());
                } catch (Exception e) {
                    rtcConnection.V.e(rtcConnection.l, "Failed to parse RTC endpoint", e, g0.mapOf(d0.o.to("endpoint", replace$default)));
                }
                a aVar3 = new a(replace$default2, str2, sSLSocketFactory, rtcConnection.V, rtcConnection.U.c(), rtcConnection.W, rtcConnection.l);
                h0 h0Var = rtcConnection.M;
                m.checkNotNullParameter(h0Var, "listener");
                aVar3.q.add(h0Var);
                aVar3.d();
                rtcConnection.w = aVar3;
            }
        }
        return Unit.a;
    }
}
