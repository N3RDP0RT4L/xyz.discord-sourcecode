package b.a.q;

import andhook.lib.xposed.ClassUtils;
import b.a.q.m0.a;
import b.a.q.o0.b;
import b.a.q.o0.c;
import b.a.q.o0.d;
import b.a.q.o0.e;
import co.discord.media_engine.MediaType;
import co.discord.media_engine.StreamParameters;
import co.discord.media_engine.VoiceQuality;
import com.discord.rtcconnection.KrispOveruseDetector;
import com.discord.rtcconnection.MediaSinkWantsManager;
import com.discord.rtcconnection.RtcConnection;
import com.discord.rtcconnection.mediaengine.MediaEngineConnection;
import com.discord.rtcconnection.socket.io.Payloads;
import d0.t.h0;
import d0.t.j;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import kotlin.NoWhenBranchMatchedException;
import org.webrtc.MediaStreamTrack;
import rx.Observable;
import rx.Subscription;
/* compiled from: RtcConnection.kt */
/* loaded from: classes.dex */
public final class b0 implements MediaEngineConnection.d {
    public final /* synthetic */ RtcConnection a;

    public b0(RtcConnection rtcConnection) {
        this.a = rtcConnection;
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
    public void onConnected(MediaEngineConnection mediaEngineConnection, MediaEngineConnection.TransportInfo transportInfo, List<a> list) {
        m.checkNotNullParameter(mediaEngineConnection, "connection");
        m.checkNotNullParameter(transportInfo, "transportInfo");
        m.checkNotNullParameter(list, "supportedVideoCodecs");
        RtcConnection rtcConnection = this.a;
        rtcConnection.q.succeed();
        rtcConnection.r = transportInfo;
        MediaEngineConnection mediaEngineConnection2 = rtcConnection.f2750x;
        if (mediaEngineConnection2 != null) {
            d dVar = new d(1000L, rtcConnection.V, mediaEngineConnection2, new VoiceQuality(), rtcConnection.B, new KrispOveruseDetector(mediaEngineConnection2), 0, 64);
            dVar.a.clear();
            Subscription subscription = dVar.f275b;
            if (subscription != null && !subscription.isUnsubscribed()) {
                dVar.a();
            }
            dVar.f275b = Observable.D(0L, dVar.c, TimeUnit.MILLISECONDS).W(new b(dVar), new c(dVar));
            rtcConnection.o = dVar;
            rtcConnection.p = false;
        }
        if (transportInfo.c.ordinal() != 0) {
            StringBuilder R = b.d.b.a.a.R("Unsupported protocol: ");
            R.append(transportInfo.c);
            R.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
            RtcConnection.j(rtcConnection, true, R.toString(), null, false, 12);
        } else {
            rtcConnection.V.recordBreadcrumb("Sending UDP info to RTC server.", rtcConnection.l);
            b.a.q.n0.a aVar = rtcConnection.w;
            if (aVar == null) {
                RtcConnection.o(rtcConnection, "onEngineConnectionConnected() socket was null.", null, null, 6);
                return;
            }
            String str = transportInfo.a;
            int i = transportInfo.f2770b;
            m.checkNotNullParameter("udp", "protocol");
            m.checkNotNullParameter(str, "address");
            m.checkNotNullParameter("xsalsa20_poly1305", "mode");
            m.checkNotNullParameter(list, "codecs");
            aVar.H.a();
            Payloads.Protocol.ProtocolInfo protocolInfo = new Payloads.Protocol.ProtocolInfo(str, i, "xsalsa20_poly1305");
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
            for (a aVar2 : list) {
                arrayList.add(new Payloads.Protocol.CodecInfo(aVar2.a, aVar2.f262b, aVar2.c, aVar2.d, aVar2.e));
            }
            aVar.n(1, new Payloads.Protocol("udp", protocolInfo, arrayList));
        }
        for (RtcConnection.c cVar : rtcConnection.n) {
            cVar.onMediaEngineConnectionConnected(rtcConnection);
        }
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
    public void onConnectionStateChange(MediaEngineConnection mediaEngineConnection, MediaEngineConnection.ConnectionState connectionState) {
        RtcConnection.State state;
        m.checkNotNullParameter(mediaEngineConnection, "connection");
        m.checkNotNullParameter(connectionState, "connectionState");
        RtcConnection rtcConnection = this.a;
        RtcConnection.StateChange stateChange = rtcConnection.f2749s;
        rtcConnection.r("Connection state change: " + connectionState);
        int ordinal = connectionState.ordinal();
        if (ordinal == 0) {
            state = RtcConnection.State.h.a;
        } else if (ordinal == 1) {
            state = RtcConnection.State.g.a;
        } else if (ordinal == 2) {
            state = RtcConnection.State.f.a;
        } else if (ordinal == 3) {
            state = RtcConnection.State.e.a;
        } else {
            throw new NoWhenBranchMatchedException();
        }
        rtcConnection.u(state);
        if (stateChange.a == RtcConnection.State.g.a && rtcConnection.f2749s.a == RtcConnection.State.h.a) {
            rtcConnection.q();
        }
        if (rtcConnection.f2749s.a == RtcConnection.State.f.a) {
            rtcConnection.D = Long.valueOf(rtcConnection.W.currentTimeMillis());
            rtcConnection.J = true;
            Map<String, Object> mutableMapOf = h0.mutableMapOf(d0.o.to("connect_count", Integer.valueOf(rtcConnection.E)));
            Long l = rtcConnection.C;
            Long l2 = rtcConnection.D;
            Long valueOf = (l2 == null || l == null) ? null : Long.valueOf(l2.longValue() - l.longValue());
            if (valueOf != null) {
                mutableMapOf.put("connect_time", Long.valueOf(valueOf.longValue()));
            }
            rtcConnection.b(mutableMapOf);
            rtcConnection.p(RtcConnection.AnalyticsEvent.VOICE_CONNECTION_SUCCESS, mutableMapOf);
            MediaSinkWantsManager mediaSinkWantsManager = rtcConnection.H;
            if (mediaSinkWantsManager != null) {
                mediaSinkWantsManager.b(new i(mediaSinkWantsManager, rtcConnection.f2750x));
            }
        }
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
    public void onDestroy(MediaEngineConnection mediaEngineConnection) {
        m.checkNotNullParameter(mediaEngineConnection, "connection");
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
    public void onError(MediaEngineConnection mediaEngineConnection, MediaEngineConnection.FailedConnectionException failedConnectionException) {
        m.checkNotNullParameter(mediaEngineConnection, "connection");
        m.checkNotNullParameter(failedConnectionException, "exception");
        RtcConnection rtcConnection = this.a;
        Objects.requireNonNull(rtcConnection);
        String str = "connection error: " + failedConnectionException.a();
        int ordinal = failedConnectionException.a().ordinal();
        if (ordinal == 0 || ordinal == 1 || ordinal == 2) {
            StringBuilder V = b.d.b.a.a.V(str, " -- ");
            V.append(failedConnectionException.getMessage());
            rtcConnection.r(V.toString());
        } else {
            RtcConnection.AnalyticsEvent analyticsEvent = RtcConnection.AnalyticsEvent.VOICE_CONNECTION_FAILURE;
            Map<String, Object> mutableMapOf = h0.mutableMapOf(d0.o.to("connect_count", Integer.valueOf(rtcConnection.E)));
            rtcConnection.b(mutableMapOf);
            rtcConnection.p(analyticsEvent, mutableMapOf);
        }
        RtcConnection.j(rtcConnection, true, str, failedConnectionException, false, 8);
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
    public void onKrispStatus(MediaEngineConnection mediaEngineConnection, KrispOveruseDetector.Status status) {
        m.checkNotNullParameter(mediaEngineConnection, "connection");
        m.checkNotNullParameter(status, "status");
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
    public void onLocalMute(long j, boolean z2) {
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
    public void onLocalVideoOffScreen(long j, boolean z2) {
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
    public void onSpeaking(long j, int i, boolean z2) {
        b.a.q.n0.a aVar;
        RtcConnection rtcConnection = this.a;
        if (j == rtcConnection.T && (aVar = rtcConnection.w) != null) {
            aVar.H.a();
            aVar.n(5, new Payloads.Speaking(i, Integer.valueOf(z2 ? 1 : 0), 0, null, 8, null));
        }
        for (RtcConnection.c cVar : rtcConnection.n) {
            cVar.onSpeaking(j, z2);
        }
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
    public void onTargetBitrate(int i) {
        e eVar = this.a.B;
        synchronized (eVar) {
            eVar.l.k = i;
        }
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
    public void onTargetFrameRate(int i) {
        e eVar = this.a.B;
        synchronized (eVar) {
            eVar.l.l = i;
        }
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
    public void onVideo(long j, Integer num, int i, int i2, int i3, StreamParameters[] streamParametersArr) {
        m.checkNotNullParameter(streamParametersArr, "streams");
        RtcConnection rtcConnection = this.a;
        List<StreamParameters> asList = j.asList(streamParametersArr);
        if (j == rtcConnection.T) {
            b.a.q.n0.a aVar = rtcConnection.w;
            if (aVar == null) {
                RtcConnection.o(rtcConnection, "sendVideo() socket was null.", null, null, 6);
            } else {
                rtcConnection.L = i2;
                if (i2 != 0) {
                    rtcConnection.p = true;
                }
                ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(asList, 10));
                for (StreamParameters streamParameters : asList) {
                    String str = streamParameters.getType() == MediaType.Audio ? MediaStreamTrack.AUDIO_TRACK_KIND : MediaStreamTrack.VIDEO_TRACK_KIND;
                    String rid = streamParameters.getRid();
                    Integer valueOf = Integer.valueOf(streamParameters.getSsrc());
                    Integer valueOf2 = Integer.valueOf(streamParameters.getRtxSsrc());
                    Boolean valueOf3 = Boolean.valueOf(streamParameters.getActive());
                    Integer valueOf4 = Integer.valueOf(streamParameters.getMaxBitrate());
                    Integer valueOf5 = Integer.valueOf(streamParameters.getQuality());
                    j0 j0Var = f.a;
                    Integer valueOf6 = Integer.valueOf(j0Var.f256b.c);
                    Payloads.ResolutionType resolutionType = Payloads.ResolutionType.Fixed;
                    b bVar = j0Var.f256b;
                    arrayList.add(new Payloads.Stream(str, rid, valueOf6, valueOf5, valueOf, valueOf2, new Payloads.Stream.MaxResolution(resolutionType, bVar.a, bVar.f252b), valueOf3, valueOf4));
                }
                m.checkNotNullParameter(arrayList, "streams");
                aVar.H.a();
                aVar.n(12, new Payloads.Video(i, i2, i3, null, arrayList));
            }
            MediaSinkWantsManager mediaSinkWantsManager = rtcConnection.H;
            if (mediaSinkWantsManager != null) {
                mediaSinkWantsManager.b(new k(mediaSinkWantsManager, i2 != 0));
            }
        }
        for (RtcConnection.c cVar : rtcConnection.n) {
            cVar.onVideoStream(j, num, i, i2, i3);
        }
    }
}
