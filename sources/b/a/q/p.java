package b.a.q;

import com.discord.rtcconnection.RtcConnection;
import d0.z.d.m;
import rx.functions.Action1;
/* compiled from: RtcConnection.kt */
/* loaded from: classes.dex */
public final class p<T> implements Action1<Boolean> {
    public final /* synthetic */ RtcConnection j;

    public p(RtcConnection rtcConnection) {
        this.j = rtcConnection;
    }

    @Override // rx.functions.Action1
    public void call(Boolean bool) {
        Boolean bool2 = bool;
        this.j.s(new o(this, bool2));
        RtcConnection rtcConnection = this.j;
        m.checkNotNullExpressionValue(bool2, "isConnected");
        rtcConnection.G = bool2.booleanValue() ? null : Long.valueOf(this.j.W.currentTimeMillis());
    }
}
