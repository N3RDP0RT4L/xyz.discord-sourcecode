package b.a.q;

import b.d.b.a.a;
import d0.z.d.m;
/* compiled from: MediaSinkWantsLadder.kt */
/* loaded from: classes.dex */
public final class j0 {
    public final b a;

    /* renamed from: b  reason: collision with root package name */
    public final b f256b;
    public final a c;
    public final int d;
    public final a e;

    public j0(b bVar, b bVar2, a aVar, int i, a aVar2) {
        m.checkNotNullParameter(bVar, "videoBudget");
        m.checkNotNullParameter(bVar2, "videoCapture");
        m.checkNotNullParameter(aVar, "videoBitrate");
        m.checkNotNullParameter(aVar2, "desktopBitrate");
        this.a = bVar;
        this.f256b = bVar2;
        this.c = aVar;
        this.d = i;
        this.e = aVar2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof j0)) {
            return false;
        }
        j0 j0Var = (j0) obj;
        return m.areEqual(this.a, j0Var.a) && m.areEqual(this.f256b, j0Var.f256b) && m.areEqual(this.c, j0Var.c) && this.d == j0Var.d && m.areEqual(this.e, j0Var.e);
    }

    public int hashCode() {
        b bVar = this.a;
        int i = 0;
        int hashCode = (bVar != null ? bVar.hashCode() : 0) * 31;
        b bVar2 = this.f256b;
        int hashCode2 = (hashCode + (bVar2 != null ? bVar2.hashCode() : 0)) * 31;
        a aVar = this.c;
        int hashCode3 = (((hashCode2 + (aVar != null ? aVar.hashCode() : 0)) * 31) + this.d) * 31;
        a aVar2 = this.e;
        if (aVar2 != null) {
            i = aVar2.hashCode();
        }
        return hashCode3 + i;
    }

    public String toString() {
        StringBuilder R = a.R("VideoQualityManagerOptions(videoBudget=");
        R.append(this.a);
        R.append(", videoCapture=");
        R.append(this.f256b);
        R.append(", videoBitrate=");
        R.append(this.c);
        R.append(", videoBitrateFloor=");
        R.append(this.d);
        R.append(", desktopBitrate=");
        R.append(this.e);
        R.append(")");
        return R.toString();
    }
}
