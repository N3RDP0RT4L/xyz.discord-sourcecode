package b.a.q;

import b.a.q.n0.a;
import com.discord.rtcconnection.EncodeQuality;
import com.discord.rtcconnection.RtcConnection;
import d0.t.g0;
import d0.z.d.k;
import d0.z.d.m;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: RtcConnection.kt */
/* loaded from: classes.dex */
public final /* synthetic */ class a0 extends k implements Function1<Pair<? extends Map<String, ? extends EncodeQuality>, ? extends RtcConnection.State>, Unit> {
    public a0(RtcConnection rtcConnection) {
        super(1, rtcConnection, RtcConnection.class, "onLocalMediaSinkWants", "onLocalMediaSinkWants(Lkotlin/Pair;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public Unit invoke(Pair<? extends Map<String, ? extends EncodeQuality>, ? extends RtcConnection.State> pair) {
        Pair<? extends Map<String, ? extends EncodeQuality>, ? extends RtcConnection.State> pair2 = pair;
        m.checkNotNullParameter(pair2, "p1");
        RtcConnection rtcConnection = (RtcConnection) this.receiver;
        if (rtcConnection.f2746b0) {
            Map<String, ? extends EncodeQuality> component1 = pair2.component1();
            if (m.areEqual(pair2.component2(), RtcConnection.State.f.a)) {
                rtcConnection.V.i(rtcConnection.l, "local MediaSinkWants: " + component1, null);
                a aVar = rtcConnection.w;
                if (aVar == null) {
                    RtcConnection.o(rtcConnection, "onLocalMediaSinkWants() socket was null.", null, null, 6);
                } else {
                    LinkedHashMap linkedHashMap = new LinkedHashMap(g0.mapCapacity(component1.size()));
                    Iterator<T> it = component1.entrySet().iterator();
                    while (it.hasNext()) {
                        Map.Entry entry = (Map.Entry) it.next();
                        linkedHashMap.put(entry.getKey(), Integer.valueOf(((EncodeQuality) entry.getValue()).getValue()));
                    }
                    m.checkNotNullParameter(linkedHashMap, "wants");
                    aVar.H.a();
                    if (aVar.r >= 5) {
                        aVar.n(15, linkedHashMap);
                    }
                }
            }
        }
        return Unit.a;
    }
}
