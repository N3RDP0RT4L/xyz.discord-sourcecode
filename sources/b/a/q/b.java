package b.a.q;

import b.d.b.a.a;
import d0.z.d.m;
/* compiled from: MediaSinkWantsLadder.kt */
/* loaded from: classes.dex */
public final class b {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final int f252b;
    public final int c;
    public final Integer d;

    public b(int i, int i2, int i3, Integer num) {
        this.a = i;
        this.f252b = i2;
        this.c = i3;
        this.d = num;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        return this.a == bVar.a && this.f252b == bVar.f252b && this.c == bVar.c && m.areEqual(this.d, bVar.d);
    }

    public int hashCode() {
        int i = ((((this.a * 31) + this.f252b) * 31) + this.c) * 31;
        Integer num = this.d;
        return i + (num != null ? num.hashCode() : 0);
    }

    public String toString() {
        StringBuilder R = a.R("IVideoQuality(width=");
        R.append(this.a);
        R.append(", height=");
        R.append(this.f252b);
        R.append(", framerate=");
        R.append(this.c);
        R.append(", pixelCount=");
        return a.E(R, this.d, ")");
    }
}
