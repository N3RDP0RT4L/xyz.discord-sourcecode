package b.a.q;

import b.a.q.e;
import b.a.q.n0.a;
import b.a.q.o0.d;
import b.a.q.o0.e;
import co.discord.media_engine.MediaType;
import co.discord.media_engine.StreamParameters;
import com.discord.rtcconnection.EncodeQuality;
import com.discord.rtcconnection.MediaSinkWantsManager;
import com.discord.rtcconnection.RtcConnection;
import com.discord.rtcconnection.VideoMetadata;
import com.discord.rtcconnection.mediaengine.MediaEngine;
import com.discord.rtcconnection.mediaengine.MediaEngineConnection;
import com.discord.rtcconnection.socket.io.Payloads;
import com.discord.utilities.logging.Logger;
import d0.t.n;
import d0.t.o;
import d0.t.r;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import kotlin.NoWhenBranchMatchedException;
import org.webrtc.MediaStreamTrack;
/* compiled from: RtcConnection.kt */
/* loaded from: classes.dex */
public final class h0 implements a.d {
    public final /* synthetic */ RtcConnection a;

    public h0(RtcConnection rtcConnection) {
        this.a = rtcConnection;
    }

    @Override // b.a.q.n0.a.d
    public void a(String str, String str2) {
        m.checkNotNullParameter(str, "audioCodec");
        m.checkNotNullParameter(str2, "videoCodec");
        MediaEngineConnection mediaEngineConnection = this.a.f2750x;
        if (mediaEngineConnection != null) {
            mediaEngineConnection.r(str, str2);
        }
    }

    @Override // b.a.q.n0.a.d
    public void b(boolean z2, Integer num, String str) {
        RtcConnection rtcConnection = this.a;
        Objects.requireNonNull(rtcConnection);
        rtcConnection.r("Disconnected from RTC server. wasFatal: " + z2 + " -- code: " + num + " -- reason: " + str);
        MediaEngineConnection mediaEngineConnection = rtcConnection.f2750x;
        if (mediaEngineConnection != null) {
            mediaEngineConnection.destroy();
        }
        MediaEngineConnection mediaEngineConnection2 = rtcConnection.f2750x;
        if (mediaEngineConnection2 != null) {
            mediaEngineConnection2.o(rtcConnection.N);
        }
        Long l = rtcConnection.G;
        long currentTimeMillis = l != null ? rtcConnection.W.currentTimeMillis() - l.longValue() : 0L;
        boolean z3 = true;
        boolean z4 = currentTimeMillis > 30000;
        if ((num != null && num.intValue() == 1000) || z4) {
            z3 = false;
        }
        if (!(rtcConnection.f2749s.a instanceof RtcConnection.State.d)) {
            rtcConnection.n(z3, str);
            d dVar = rtcConnection.o;
            if (dVar != null) {
                dVar.a();
            }
            rtcConnection.o = null;
            rtcConnection.p = false;
        }
        rtcConnection.A = 0;
        rtcConnection.D = null;
        MediaSinkWantsManager mediaSinkWantsManager = rtcConnection.H;
        if (mediaSinkWantsManager != null) {
            mediaSinkWantsManager.b(new g(mediaSinkWantsManager));
        }
        rtcConnection.u(new RtcConnection.State.d(z3));
        if (z3) {
            long fail = rtcConnection.q.fail(new d0(rtcConnection));
            Logger.w$default(rtcConnection.V, rtcConnection.l, "Disconnect was not clean! Reason: " + str + ", code: " + num + ". Reconnecting in " + (fail / 1000) + " seconds.", null, 4, null);
        }
    }

    @Override // b.a.q.n0.a.d
    public void c(String str, List<Integer> list) {
        m.checkNotNullParameter(str, "mode");
        m.checkNotNullParameter(list, "secretKey");
        MediaEngineConnection mediaEngineConnection = this.a.f2750x;
        if (mediaEngineConnection != null) {
            mediaEngineConnection.t(str, u.toIntArray(list));
        }
    }

    @Override // b.a.q.n0.a.d
    public void d(String str) {
        m.checkNotNullParameter(str, "mediaSessionId");
        RtcConnection rtcConnection = this.a;
        rtcConnection.K = str;
        for (RtcConnection.c cVar : rtcConnection.n) {
            cVar.onMediaSessionIdReceived();
            rtcConnection.p(RtcConnection.AnalyticsEvent.MEDIA_SESSION_JOINED, new LinkedHashMap());
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v12, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r3v13, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r3v8, types: [java.util.List] */
    @Override // b.a.q.n0.a.d
    public void e(long j, int i, int i2, List<Payloads.Stream> list) {
        VideoMetadata videoMetadata;
        Object obj;
        boolean z2;
        ?? r3;
        Payloads.Stream copy;
        RtcConnection rtcConnection = this.a;
        e eVar = rtcConnection.B;
        long j2 = i2;
        synchronized (eVar) {
            if (j2 != 0) {
                eVar.n.put(Long.valueOf(j2), Long.valueOf(eVar.q.currentTimeMillis()));
                Logger.i$default(eVar.p, "VideoQuality: handleVideoStreamUpdate(userId: " + j + ", videoSsrc: " + j2 + ')', null, 2, null);
            }
        }
        if (j != rtcConnection.T) {
            boolean z3 = false;
            if (rtcConnection.H != null) {
                if (list != null) {
                    r3 = new ArrayList(o.collectionSizeOrDefault(list, 10));
                    for (Payloads.Stream stream : list) {
                        copy = stream.copy((r20 & 1) != 0 ? stream.type : MediaStreamTrack.VIDEO_TRACK_KIND, (r20 & 2) != 0 ? stream.rid : null, (r20 & 4) != 0 ? stream.maxFrameRate : null, (r20 & 8) != 0 ? stream.quality : null, (r20 & 16) != 0 ? stream.ssrc : null, (r20 & 32) != 0 ? stream.rtxSsrc : null, (r20 & 64) != 0 ? stream.maxResolution : null, (r20 & 128) != 0 ? stream.active : Boolean.valueOf(i2 > 0), (r20 & 256) != 0 ? stream.maxBitrate : null);
                        r3.add(copy);
                    }
                } else {
                    r3 = n.emptyList();
                }
                boolean isEmpty = r3.isEmpty();
                List list2 = r3;
                if (isEmpty) {
                    Integer valueOf = Integer.valueOf(i2);
                    Integer valueOf2 = Integer.valueOf(i2 + 1);
                    if (i2 > 0) {
                        z3 = true;
                    }
                    list2 = d0.t.m.listOf(new Payloads.Stream(MediaStreamTrack.VIDEO_TRACK_KIND, "100", null, 100, valueOf, valueOf2, null, Boolean.valueOf(z3), null));
                }
                rtcConnection.H.c(j, Long.valueOf(i));
                MediaSinkWantsManager mediaSinkWantsManager = rtcConnection.H;
                Objects.requireNonNull(mediaSinkWantsManager);
                m.checkNotNullParameter(list2, "ssrcs");
                mediaSinkWantsManager.b(new l(mediaSinkWantsManager, list2, j));
                return;
            }
            if (list != null) {
                Iterator it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj = null;
                        break;
                    }
                    obj = it.next();
                    Payloads.Stream stream2 = (Payloads.Stream) obj;
                    Integer ssrc = stream2.getSsrc();
                    if (ssrc == null || ssrc.intValue() != i2 || stream2.getMaxResolution() == null) {
                        z2 = false;
                        continue;
                    } else {
                        z2 = true;
                        continue;
                    }
                    if (z2) {
                        break;
                    }
                }
                Payloads.Stream stream3 = (Payloads.Stream) obj;
                if (stream3 != null) {
                    Payloads.Stream.MaxResolution maxResolution = stream3.getMaxResolution();
                    m.checkNotNull(maxResolution);
                    videoMetadata = new VideoMetadata(j, maxResolution.getWidth(), stream3.getMaxResolution().getHeight(), stream3.getMaxFrameRate(), stream3.getMaxResolution().getType());
                    rtcConnection.d(j, i, j2, videoMetadata);
                }
            }
            videoMetadata = null;
            rtcConnection.d(j, i, j2, videoMetadata);
        }
    }

    @Override // b.a.q.n0.a.d
    public void f(Map<String, Integer> map) {
        boolean z2;
        Integer num;
        m.checkNotNullParameter(map, "wants");
        RtcConnection rtcConnection = this.a;
        if (rtcConnection.H != null) {
            long j = rtcConnection.L;
            int intValue = (j == 0 || (num = map.get(String.valueOf(j))) == null) ? 0 : num.intValue();
            Integer num2 = map.get("any");
            int intValue2 = num2 != null ? num2.intValue() : 0;
            if (intValue <= 0) {
                intValue = intValue2 > 0 ? intValue2 : 100;
            }
            e.c cVar = null;
            rtcConnection.V.i(rtcConnection.l, "remote MediaSinkWants: " + map + ", decided on encode quality " + intValue, null);
            EncodeQuality[] values = EncodeQuality.values();
            for (int i = 10; i >= 0; i--) {
                EncodeQuality encodeQuality = values[i];
                if (encodeQuality.getValue() <= intValue) {
                    e eVar = rtcConnection.H.j;
                    j0 j0Var = eVar.e;
                    m.checkNotNullParameter(encodeQuality, "wantValue");
                    List<e.c> list = eVar.d;
                    ListIterator<e.c> listIterator = list.listIterator(list.size());
                    while (true) {
                        if (!listIterator.hasPrevious()) {
                            break;
                        }
                        e.c previous = listIterator.previous();
                        if (encodeQuality.compareTo(previous.c) >= 0) {
                            z2 = true;
                            continue;
                        } else {
                            z2 = false;
                            continue;
                        }
                        if (z2) {
                            cVar = previous;
                            break;
                        }
                    }
                    e.c cVar2 = cVar;
                    if (cVar2 == null) {
                        cVar2 = (e.c) u.first((List<? extends Object>) eVar.d);
                    }
                    n nVar = cVar2.a;
                    int max = Math.max((int) (j0Var.c.a * nVar.c), j0Var.d);
                    int max2 = Math.max((int) (j0Var.c.f251b * nVar.c), j0Var.d);
                    MediaEngineConnection mediaEngineConnection = rtcConnection.f2750x;
                    if (mediaEngineConnection != null) {
                        mediaEngineConnection.m(new MediaEngineConnection.b(max, max2, nVar.a, nVar.f270b, nVar.d, nVar.e));
                        return;
                    }
                    return;
                }
            }
            throw new NoSuchElementException("Array contains no element matching the predicate.");
        }
    }

    @Override // b.a.q.n0.a.d
    public void g(long j) {
        RtcConnection.Quality quality;
        RtcConnection rtcConnection = this.a;
        rtcConnection.v.add(Long.valueOf(j));
        if (rtcConnection.v.size() > 5) {
            r.removeFirst(rtcConnection.v);
        }
        if (j > 500) {
            rtcConnection.A++;
        }
        double d = j;
        Objects.requireNonNull(RtcConnection.Quality.Companion);
        if (Double.isNaN(d)) {
            quality = RtcConnection.Quality.UNKNOWN;
        } else if (d < 250) {
            quality = RtcConnection.Quality.FINE;
        } else if (d < 500) {
            quality = RtcConnection.Quality.AVERAGE;
        } else {
            quality = RtcConnection.Quality.BAD;
        }
        for (RtcConnection.c cVar : rtcConnection.n) {
            cVar.onQualityUpdate(quality);
        }
    }

    @Override // b.a.q.n0.a.d
    public void h() {
    }

    @Override // b.a.q.n0.a.d
    public void i() {
        RtcConnection rtcConnection = this.a;
        rtcConnection.q.cancel();
        rtcConnection.V.recordBreadcrumb("Connected to RTC server.", rtcConnection.l);
        a aVar = rtcConnection.w;
        if (aVar == null) {
            RtcConnection.o(rtcConnection, "onSocketConnect() socket was null.", null, null, 6);
            return;
        }
        List listOf = d0.t.m.listOf(new Payloads.Stream(MediaStreamTrack.VIDEO_TRACK_KIND, "100", null, 100, null, null, null, null, null));
        String str = rtcConnection.S;
        long j = rtcConnection.T;
        String str2 = rtcConnection.Q;
        boolean z2 = rtcConnection.R;
        m.checkNotNullParameter(str, "serverId");
        m.checkNotNullParameter(str2, "sessionId");
        m.checkNotNullParameter(listOf, "streams");
        aVar.H.a();
        aVar.t = str;
        aVar.u = str2;
        aVar.B = a.c.IDENTIFYING;
        aVar.n(0, new Payloads.Identify(str, j, str2, aVar.E, z2, listOf));
        rtcConnection.u(RtcConnection.State.a.a);
    }

    @Override // b.a.q.n0.a.d
    public void j(long j) {
        Map<String, Object> c;
        RtcConnection rtcConnection = this.a;
        if ((rtcConnection.X instanceof RtcConnection.d.a) && (c = rtcConnection.B.c(String.valueOf(j))) != null) {
            rtcConnection.l(j, c);
        }
        MediaSinkWantsManager mediaSinkWantsManager = rtcConnection.H;
        if (mediaSinkWantsManager != null) {
            mediaSinkWantsManager.c(j, null);
        }
        MediaSinkWantsManager mediaSinkWantsManager2 = rtcConnection.H;
        if (mediaSinkWantsManager2 != null) {
            List emptyList = n.emptyList();
            m.checkNotNullParameter(emptyList, "ssrcs");
            mediaSinkWantsManager2.b(new l(mediaSinkWantsManager2, emptyList, j));
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r6v5, types: [java.util.List] */
    @Override // b.a.q.n0.a.d
    public void k(int i, int i2, String str, List<Payloads.Stream> list) {
        MediaEngineConnection.Type type;
        m.checkNotNullParameter(str, "ip");
        m.checkNotNullParameter(list, "streams");
        RtcConnection rtcConnection = this.a;
        Objects.requireNonNull(rtcConnection);
        rtcConnection.r("Discovered dedicated UDP server on port " + i);
        rtcConnection.u(RtcConnection.State.g.a);
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
        Iterator it = list.iterator();
        while (it.hasNext()) {
            Payloads.Stream stream = (Payloads.Stream) it.next();
            MediaType mediaType = MediaStreamTrack.VIDEO_TRACK_KIND.equals(stream.getType()) ? MediaType.Video : MediaType.Audio;
            String rid = stream.getRid();
            if (rid == null) {
                rid = "";
            }
            String str2 = rid;
            Integer ssrc = stream.getSsrc();
            int intValue = ssrc != null ? ssrc.intValue() : 0;
            Integer rtxSsrc = stream.getRtxSsrc();
            int intValue2 = rtxSsrc != null ? rtxSsrc.intValue() : 0;
            Boolean active = stream.getActive();
            boolean booleanValue = active != null ? active.booleanValue() : false;
            Integer maxBitrate = stream.getMaxBitrate();
            int intValue3 = maxBitrate != null ? maxBitrate.intValue() : 0;
            Integer quality = stream.getQuality();
            it = it;
            arrayList.add(new StreamParameters(mediaType, str2, intValue, intValue2, booleanValue, intValue3, quality != null ? quality.intValue() : 100, 0));
        }
        boolean isEmpty = arrayList.isEmpty();
        ArrayList arrayList2 = arrayList;
        if (isEmpty) {
            arrayList2 = d0.t.m.listOf(new StreamParameters(MediaType.Video, "100", i2 + 1, i2 + 2, false, 0, 100, 0));
        }
        MediaEngine mediaEngine = rtcConnection.U;
        long j = rtcConnection.T;
        MediaEngine.a aVar = new MediaEngine.a(i2, str, i, arrayList2);
        RtcConnection.d dVar = rtcConnection.X;
        if (m.areEqual(dVar, RtcConnection.d.a.a)) {
            type = MediaEngineConnection.Type.DEFAULT;
        } else if (dVar instanceof RtcConnection.d.b) {
            type = MediaEngineConnection.Type.STREAM;
        } else {
            throw new NoWhenBranchMatchedException();
        }
        MediaEngineConnection g = mediaEngine.g(j, aVar, type, new e0(rtcConnection));
        if (g == null) {
            RtcConnection.o(rtcConnection, "onSocketHello(): connect() return null.", null, null, 6);
            return;
        }
        g.l(rtcConnection.N);
        rtcConnection.f2750x = g;
    }

    @Override // b.a.q.n0.a.d
    public void onConnecting() {
        RtcConnection rtcConnection = this.a;
        rtcConnection.q.cancel();
        StringBuilder sb = new StringBuilder();
        sb.append("Connecting to RTC server ");
        a aVar = rtcConnection.w;
        sb.append(aVar != null ? aVar.D : null);
        rtcConnection.r(sb.toString());
        rtcConnection.u(RtcConnection.State.c.a);
    }

    @Override // b.a.q.n0.a.d
    public void onSpeaking(long j, int i, boolean z2) {
        RtcConnection rtcConnection = this.a;
        if (j != rtcConnection.T) {
            MediaEngineConnection mediaEngineConnection = rtcConnection.f2750x;
            if (mediaEngineConnection != null) {
                mediaEngineConnection.s(j, i, null, rtcConnection.g(j), rtcConnection.h(j));
            }
            MediaSinkWantsManager mediaSinkWantsManager = rtcConnection.H;
            if (mediaSinkWantsManager != null) {
                mediaSinkWantsManager.c(j, Long.valueOf(i));
            }
            for (RtcConnection.c cVar : rtcConnection.n) {
                cVar.onUserCreated(rtcConnection, j);
            }
        }
    }
}
