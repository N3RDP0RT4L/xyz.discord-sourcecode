package b.a.q.m0.c;

import android.content.Context;
import android.os.Build;
import androidx.annotation.AnyThread;
import b.a.q.e0;
import co.discord.media_engine.RtcRegion;
import co.discord.media_engine.VideoInputDeviceDescription;
import com.discord.rtcconnection.mediaengine.MediaEngine;
import com.discord.rtcconnection.mediaengine.MediaEngineConnection;
import com.discord.utilities.logging.Logger;
import com.hammerandchisel.libdiscord.Discord;
import d0.t.n0;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Future;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import org.webrtc.Logging;
import org.webrtc.voiceengine.WebRtcAudioManager;
import org.webrtc.voiceengine.WebRtcAudioUtils;
import rx.Emitter;
import rx.Observable;
import rx.functions.Action1;
/* compiled from: MediaEngineLegacy.kt */
/* loaded from: classes.dex */
public final class k implements MediaEngine {
    public static final Set<String> a = n0.setOf((Object[]) new String[]{"Pixel", "Pixel XL", "Pixel 3a XL", "Pixel 4", "Pixel 4 XL", "Pixel 5"});

    /* renamed from: b  reason: collision with root package name */
    public static final Set<String> f267b = n0.setOf((Object[]) new String[]{"Pixel 3a", "Redmi Note 8 Pro", "Redmi Note 8 pro"});
    public final u c;
    public final List<MediaEngineConnection> d;
    public boolean e;
    public Discord f;
    public List<b.a.q.m0.a> g;
    public MediaEngine.OpenSLUsageMode h;
    public MediaEngine.EchoCancellationInfo i;
    public final Context j;
    public final MediaEngine.c k;
    public final b.a.q.c l;
    public final MediaEngine.OpenSLESConfig m;
    public final Logger n;
    public final b.a.q.k0.g o;
    public final MediaEngine.b p;
    public final Set<String> q;
    public final Set<String> r;

    /* compiled from: MediaEngineLegacy.kt */
    /* loaded from: classes.dex */
    public static final class a extends o implements Function0<Unit> {
        public a() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public Unit invoke() {
            k kVar = k.this;
            b.a.q.m0.c.j jVar = b.a.q.m0.c.j.j;
            Set<String> set = k.a;
            kVar.n(jVar);
            return Unit.a;
        }
    }

    /* compiled from: MediaEngineLegacy.kt */
    /* loaded from: classes.dex */
    public static final class b extends o implements Function1<MediaEngine.c, Unit> {
        public final /* synthetic */ b.a.q.m0.c.e $connection;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(b.a.q.m0.c.e eVar) {
            super(1);
            this.$connection = eVar;
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(MediaEngine.c cVar) {
            MediaEngine.c cVar2 = cVar;
            m.checkNotNullParameter(cVar2, "it");
            cVar2.onNewConnection(this.$connection);
            return Unit.a;
        }
    }

    /* compiled from: MediaEngineLegacy.kt */
    /* loaded from: classes.dex */
    public static final class c extends MediaEngineConnection.a {

        /* compiled from: MediaEngineLegacy.kt */
        /* loaded from: classes.dex */
        public static final class a extends o implements Function0<Unit> {
            public a() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            public Unit invoke() {
                k kVar = k.this;
                l lVar = l.j;
                Set<String> set = k.a;
                kVar.n(lVar);
                return Unit.a;
            }
        }

        /* compiled from: MediaEngineLegacy.kt */
        /* loaded from: classes.dex */
        public static final class b extends o implements Function0<Unit> {
            public final /* synthetic */ MediaEngineConnection $connection;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public b(MediaEngineConnection mediaEngineConnection) {
                super(0);
                this.$connection = mediaEngineConnection;
            }

            @Override // kotlin.jvm.functions.Function0
            public Unit invoke() {
                k.m(k.this, this.$connection);
                return Unit.a;
            }
        }

        /* compiled from: MediaEngineLegacy.kt */
        /* renamed from: b.a.q.m0.c.k$c$c  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C0043c extends o implements Function0<Unit> {
            public final /* synthetic */ MediaEngineConnection $connection;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C0043c(MediaEngineConnection mediaEngineConnection) {
                super(0);
                this.$connection = mediaEngineConnection;
            }

            @Override // kotlin.jvm.functions.Function0
            public Unit invoke() {
                k.m(k.this, this.$connection);
                return Unit.a;
            }
        }

        /* compiled from: MediaEngineLegacy.kt */
        /* loaded from: classes.dex */
        public static final class d extends o implements Function0<Unit> {
            public final /* synthetic */ MediaEngineConnection $connection;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public d(MediaEngineConnection mediaEngineConnection) {
                super(0);
                this.$connection = mediaEngineConnection;
            }

            @Override // kotlin.jvm.functions.Function0
            public Unit invoke() {
                k.m(k.this, this.$connection);
                return Unit.a;
            }
        }

        public c() {
        }

        @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.a, com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
        public void onConnected(MediaEngineConnection mediaEngineConnection, MediaEngineConnection.TransportInfo transportInfo, List<b.a.q.m0.a> list) {
            m.checkNotNullParameter(mediaEngineConnection, "connection");
            m.checkNotNullParameter(transportInfo, "transportInfo");
            m.checkNotNullParameter(list, "supportedVideoCodecs");
            k.this.o(new a());
        }

        @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.a, com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
        public void onConnectionStateChange(MediaEngineConnection mediaEngineConnection, MediaEngineConnection.ConnectionState connectionState) {
            m.checkNotNullParameter(mediaEngineConnection, "connection");
            m.checkNotNullParameter(connectionState, "connectionState");
            if (connectionState == MediaEngineConnection.ConnectionState.DISCONNECTED) {
                k.this.o(new b(mediaEngineConnection));
            }
        }

        @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.a, com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
        public void onDestroy(MediaEngineConnection mediaEngineConnection) {
            m.checkNotNullParameter(mediaEngineConnection, "connection");
            k.this.o(new C0043c(mediaEngineConnection));
        }

        @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.a, com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
        public void onError(MediaEngineConnection mediaEngineConnection, MediaEngineConnection.FailedConnectionException failedConnectionException) {
            m.checkNotNullParameter(mediaEngineConnection, "connection");
            m.checkNotNullParameter(failedConnectionException, "exception");
            k.this.o(new d(mediaEngineConnection));
        }
    }

    /* compiled from: MediaEngineLegacy.kt */
    /* loaded from: classes.dex */
    public static final class d<T> implements Action1<Emitter<MediaEngine.AudioInfo>> {
        public d() {
        }

        @Override // rx.functions.Action1
        public void call(Emitter<MediaEngine.AudioInfo> emitter) {
            Emitter<MediaEngine.AudioInfo> emitter2 = emitter;
            Discord discord = k.this.f;
            if (discord != null) {
                discord.getAudioSubsystem(new o(emitter2));
            }
        }
    }

    /* compiled from: MediaEngineLegacy.kt */
    /* loaded from: classes.dex */
    public static final class e<T> implements Action1<List<? extends b.a.q.m0.a>> {
        public e() {
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // rx.functions.Action1
        public void call(List<? extends b.a.q.m0.a> list) {
            List<? extends b.a.q.m0.a> list2 = list;
            k kVar = k.this;
            m.checkNotNullExpressionValue(list2, "codecs");
            kVar.g = list2;
        }
    }

    /* compiled from: MediaEngineLegacy.kt */
    /* loaded from: classes.dex */
    public static final class f<T, R> implements j0.k.b<List<? extends b.a.q.m0.a>, Unit> {
        public static final f j = new f();

        @Override // j0.k.b
        public Unit call(List<? extends b.a.q.m0.a> list) {
            return Unit.a;
        }
    }

    /* compiled from: MediaEngineLegacy.kt */
    /* loaded from: classes.dex */
    public static final class g implements Discord.LocalVoiceLevelChangedCallback {
        public final /* synthetic */ Function1 a;

        public g(Function1 function1) {
            this.a = function1;
        }

        @Override // com.hammerandchisel.libdiscord.Discord.LocalVoiceLevelChangedCallback
        public final void onLocalVoiceLevelChanged(float f, int i) {
            boolean z2 = true;
            if ((i & 1) == 0) {
                z2 = false;
            }
            this.a.invoke(new MediaEngine.LocalVoiceStatus(f, z2));
        }
    }

    /* compiled from: MediaEngineLegacy.kt */
    /* loaded from: classes.dex */
    public static final class h implements Discord.BuiltinAECCallback {
        public final /* synthetic */ b.a.q.k0.g a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ k f268b;

        /* compiled from: MediaEngineLegacy.kt */
        /* loaded from: classes.dex */
        public static final class a extends o implements Function0<Unit> {
            public final /* synthetic */ boolean $available;
            public final /* synthetic */ boolean $enabled;
            public final /* synthetic */ boolean $requestEnabled;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public a(boolean z2, boolean z3, boolean z4) {
                super(0);
                this.$enabled = z2;
                this.$requestEnabled = z3;
                this.$available = z4;
            }

            @Override // kotlin.jvm.functions.Function0
            public Unit invoke() {
                h.this.a.d = this.$enabled;
                k kVar = h.this.f268b;
                boolean z2 = this.$requestEnabled;
                boolean z3 = this.$available;
                Objects.requireNonNull(kVar.o);
                kVar.i = new MediaEngine.EchoCancellationInfo(z2, z3, WebRtcAudioUtils.isAcousticEchoCancelerSupported(), this.$enabled, false, false, false, false, false, 496);
                return Unit.a;
            }
        }

        public h(b.a.q.k0.g gVar, Discord discord, k kVar) {
            this.a = gVar;
            this.f268b = kVar;
        }

        @Override // com.hammerandchisel.libdiscord.Discord.BuiltinAECCallback
        public final void onConfigureBuiltinAEC(boolean z2, boolean z3, boolean z4) {
            this.f268b.o(new a(z4, z2, z3));
        }
    }

    /* compiled from: MediaEngineLegacy.kt */
    /* loaded from: classes.dex */
    public static final class i implements Logging.ExternalReporter {
        public i() {
        }

        @Override // org.webrtc.Logging.ExternalReporter
        public final void e(String str, String str2, Throwable th) {
            Logger logger = k.this.n;
            m.checkNotNullExpressionValue(str, "tag");
            m.checkNotNullExpressionValue(str2, "message");
            Logger.e$default(logger, str, str2, th, null, 8, null);
        }
    }

    /* compiled from: MediaEngineLegacy.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class j extends d0.z.d.k implements Function1<MediaEngine.c, Unit> {
        public static final j j = new j();

        public j() {
            super(1, MediaEngine.c.class, "onNativeEngineInitialized", "onNativeEngineInitialized()V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(MediaEngine.c cVar) {
            MediaEngine.c cVar2 = cVar;
            m.checkNotNullParameter(cVar2, "p1");
            cVar2.onNativeEngineInitialized();
            return Unit.a;
        }
    }

    /* compiled from: MediaEngineLegacy.kt */
    /* renamed from: b.a.q.m0.c.k$k  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0044k extends o implements Function0<Unit> {
        public final /* synthetic */ MediaEngine.VoiceConfig $voiceConfig;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public C0044k(MediaEngine.VoiceConfig voiceConfig) {
            super(0);
            this.$voiceConfig = voiceConfig;
        }

        @Override // kotlin.jvm.functions.Function0
        public Unit invoke() {
            boolean z2;
            StringBuilder R = b.d.b.a.a.R("updateVoiceConfig: ");
            R.append(this.$voiceConfig);
            b.c.a.a0.d.b1("MediaEngineLegacy", R.toString());
            MediaEngine.VoiceConfig voiceConfig = this.$voiceConfig;
            k kVar = k.this;
            float f = voiceConfig.a;
            Discord discord = kVar.f;
            if (discord != null) {
                discord.setSpeakerVolume(Math.min(300.0f, Math.max(0.0f, f)) / 100.0f);
            }
            k kVar2 = k.this;
            boolean z3 = voiceConfig.f2768b;
            kVar2.l.a();
            b.a.q.k0.g gVar = kVar2.o;
            if (gVar.g && gVar.d) {
                z2 = false;
            } else {
                b.a.q.k0.g gVar2 = kVar2.o;
                z2 = gVar2.f && !gVar2.d ? true : z3;
            }
            if (z2 != z3) {
                b.c.a.a0.d.b1("MediaEngineLegacy", "ignoring call to setEchoCancellation(" + z3 + "), config=" + kVar2.o);
            }
            Discord discord2 = kVar2.f;
            if (discord2 != null) {
                discord2.setEchoCancellation(z2, false, new m(kVar2));
            }
            k kVar3 = k.this;
            boolean z4 = voiceConfig.c;
            Discord discord3 = kVar3.f;
            if (discord3 != null) {
                discord3.setNoiseSuppression(z4);
            }
            k kVar4 = k.this;
            boolean z5 = voiceConfig.d;
            kVar4.c.a = z5;
            Discord discord4 = kVar4.f;
            if (discord4 != null) {
                discord4.setNoiseCancellation(z5);
            }
            k kVar5 = k.this;
            boolean z6 = voiceConfig.e;
            Discord discord5 = kVar5.f;
            if (discord5 != null) {
                discord5.setAutomaticGainControl(z6);
            }
            for (MediaEngineConnection mediaEngineConnection : k.this.getConnections()) {
                mediaEngineConnection.k(voiceConfig.f, voiceConfig.g);
                mediaEngineConnection.v(voiceConfig.h);
                mediaEngineConnection.c(voiceConfig.i || mediaEngineConnection.getType() == MediaEngineConnection.Type.STREAM);
            }
            return Unit.a;
        }
    }

    public k(Context context, MediaEngine.c cVar, b.a.q.c cVar2, MediaEngine.OpenSLESConfig openSLESConfig, Logger logger, b.a.q.k0.g gVar, MediaEngine.b bVar, Set set, Set set2, int i2) {
        Set<String> set3 = null;
        Set<String> set4 = (i2 & 128) != 0 ? a : null;
        set3 = (i2 & 256) != 0 ? f267b : set3;
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(cVar, "listener");
        m.checkNotNullParameter(cVar2, "mediaEngineThreadExecutor");
        m.checkNotNullParameter(openSLESConfig, "openSLESConfig");
        m.checkNotNullParameter(logger, "logger");
        m.checkNotNullParameter(gVar, "echoCancellation");
        m.checkNotNullParameter(bVar, "echoCancellationCallback");
        m.checkNotNullParameter(set4, "defaultOpenSLAllowList");
        m.checkNotNullParameter(set3, "defaultOpenSLExcludeList");
        this.j = context;
        this.k = cVar;
        this.l = cVar2;
        this.m = openSLESConfig;
        this.n = logger;
        this.o = gVar;
        this.p = bVar;
        this.q = set4;
        this.r = set3;
        this.c = new u();
        this.d = new ArrayList();
        this.e = true;
        this.h = MediaEngine.OpenSLUsageMode.ALLOW_LIST;
    }

    public static final void m(k kVar, MediaEngineConnection mediaEngineConnection) {
        synchronized (kVar) {
            Discord discord = kVar.f;
            if (discord != null) {
                discord.setLocalVoiceLevelChangedCallback(null);
            }
            kVar.d.remove(mediaEngineConnection);
        }
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngine
    public Observable<Unit> a() {
        if (this.g != null) {
            j0.l.e.k kVar = new j0.l.e.k(Unit.a);
            m.checkNotNullExpressionValue(kVar, "Observable.just(Unit)");
            return kVar;
        }
        p();
        if (this.f == null) {
            Observable<Unit> w = Observable.w(new IllegalStateException("Failed to initialize native media engine"));
            m.checkNotNullExpressionValue(w, "Observable.error(Illegal…ze native media engine\"))");
            return w;
        }
        Observable n = Observable.n(new q(this), Emitter.BackpressureMode.NONE);
        m.checkNotNullExpressionValue(n, "Observable.create({ emit…er.BackpressureMode.NONE)");
        Observable<Unit> F = n.t(new e()).F(f.j);
        m.checkNotNullExpressionValue(F, "getSupportedVideoCodecs(…s }\n        .map { Unit }");
        return F;
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngine
    public void b(RtcRegion[] rtcRegionArr, Function1<? super String[], Unit> function1) {
        m.checkNotNullParameter(rtcRegionArr, "regionsWithIps");
        m.checkNotNullParameter(function1, "callback");
        p();
        Discord discord = this.f;
        if (discord != null) {
            discord.getRankedRtcRegions(rtcRegionArr, new r(function1));
        }
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngine
    public b.a.q.c c() {
        return this.l;
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngine
    @AnyThread
    public void d(MediaEngine.VoiceConfig voiceConfig) {
        m.checkNotNullParameter(voiceConfig, "voiceConfig");
        o(new C0044k(voiceConfig));
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngine
    public Observable<MediaEngine.AudioInfo> e() {
        Observable<MediaEngine.AudioInfo> n = Observable.n(new d(), Emitter.BackpressureMode.LATEST);
        m.checkNotNullExpressionValue(n, "Observable.create({ emit….BackpressureMode.LATEST)");
        return n;
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngine
    public void f(int i2) {
        Discord discord = this.f;
        if (discord != null) {
            discord.setVideoInputDevice(i2);
        }
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngine
    public synchronized MediaEngineConnection g(long j2, MediaEngine.a aVar, MediaEngineConnection.Type type, Function1<? super Exception, Unit> function1) {
        m.checkNotNullParameter(aVar, "options");
        m.checkNotNullParameter(type, "type");
        m.checkNotNullParameter(function1, "onFailure");
        if (!(this.g != null)) {
            ((e0) function1).invoke(new IllegalStateException("connect() called on unprepared media engine."));
            return null;
        }
        Discord discord = this.f;
        if (discord == null) {
            Logger.e$default(this.n, "MediaEngineLegacy", "connect() called without voiceEngineLegacy.", null, null, 12, null);
            return null;
        }
        o(new a());
        Logger logger = this.n;
        Logger.i$default(logger, "MediaEngineLegacy", "Connecting with options: " + aVar, null, 4, null);
        c cVar = new c();
        b.a.q.c cVar2 = this.l;
        Logger logger2 = this.n;
        u uVar = this.c;
        List<b.a.q.m0.a> list = this.g;
        if (list == null) {
            m.throwUninitializedPropertyAccessException("supportedVideoCodecs");
        }
        b.a.q.m0.c.e eVar = new b.a.q.m0.c.e(cVar2, logger2, uVar, discord, list, type, j2, aVar, d0.t.m.listOf(cVar));
        this.d.add(eVar);
        n(new b(eVar));
        return eVar;
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngine
    public synchronized List<MediaEngineConnection> getConnections() {
        return u.toList(this.d);
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngine
    public void h(MediaEngine.OpenSLUsageMode openSLUsageMode) {
        m.checkNotNullParameter(openSLUsageMode, "openSLUsageMode");
        if (this.f != null) {
            Logger.e$default(this.n, "MediaEngineLegacy", "setting openSLUsageMode too late", null, null, 12, null);
        }
        this.h = openSLUsageMode;
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngine
    public Discord i() {
        return this.f;
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngine
    public void j(Function1<? super VideoInputDeviceDescription[], Unit> function1) {
        m.checkNotNullParameter(function1, "devicesCallback");
        p();
        Discord discord = this.f;
        if (discord != null) {
            discord.getVideoInputDevices(new s(function1));
        }
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngine
    public void k(boolean z2) {
        this.e = z2;
        Discord discord = this.f;
        if (discord != null) {
            discord.setAudioInputEnabled(z2);
        }
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngine
    public void l(Function1<? super MediaEngine.LocalVoiceStatus, Unit> function1) {
        if (function1 != null) {
            p();
            Discord discord = this.f;
            if (discord != null) {
                discord.setLocalVoiceLevelChangedCallback(new g(function1));
                return;
            }
            return;
        }
        Discord discord2 = this.f;
        if (discord2 != null) {
            discord2.setLocalVoiceLevelChangedCallback(null);
        }
    }

    public final void n(Function1<? super MediaEngine.c, Unit> function1) {
        try {
            function1.invoke(this.k);
        } catch (Exception e2) {
            Logger.e$default(this.n, "MediaEngineLegacy", "Error in listener", e2, null, 8, null);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v0, types: [b.a.q.m0.c.t] */
    public final Future<?> o(Function0<Unit> function0) {
        b.a.q.c cVar = this.l;
        if (function0 != null) {
            function0 = new t(function0);
        }
        return cVar.submit((Runnable) function0);
    }

    public final void p() {
        if (this.f == null) {
            Logger logger = this.n;
            StringBuilder R = b.d.b.a.a.R("initializing voice engine. OpenSL ES: ");
            R.append(this.m);
            R.append(", OpenSL usage mode: ");
            R.append(this.h);
            Logger.i$default(logger, "MediaEngineLegacy", R.toString(), null, 4, null);
            int ordinal = this.m.ordinal();
            boolean z2 = false;
            if (ordinal == 0) {
                if (this.h == MediaEngine.OpenSLUsageMode.ALLOW_LIST) {
                    z2 = this.q.contains(Build.MODEL);
                } else if (!this.r.contains(Build.MODEL)) {
                    z2 = true;
                }
                Logger logger2 = this.n;
                StringBuilder R2 = b.d.b.a.a.R("OpenSL ES default. mode: ");
                R2.append(this.h);
                R2.append(", enableOpenSL: ");
                R2.append(z2);
                R2.append(", model: '");
                Logger.i$default(logger2, "MediaEngineLegacy", b.d.b.a.a.G(R2, Build.MODEL, '\''), null, 4, null);
                WebRtcAudioManager.setBlacklistDeviceForOpenSLESUsage(!z2);
            } else if (ordinal == 1) {
                WebRtcAudioManager.setBlacklistDeviceForOpenSLESUsage(false);
            } else if (ordinal == 2) {
                WebRtcAudioManager.setBlacklistDeviceForOpenSLESUsage(true);
            }
            Logging.externalReporter = new i();
            try {
                Discord discord = new Discord(this.j, 2);
                b.a.q.k0.g gVar = this.o;
                if (gVar.e) {
                    discord.enableBuiltInAEC(true, new h(gVar, discord, this));
                }
                discord.setAudioInputEnabled(this.e);
                this.f = discord;
            } catch (ExceptionInInitializerError e2) {
                Logger.e$default(this.n, "MediaEngineLegacy", "Unable to initialize voice engine.", e2, null, 8, null);
            } catch (UnsatisfiedLinkError e3) {
                Logger.e$default(this.n, "MediaEngineLegacy", "Unable to initialize voice engine.", e3, null, 8, null);
            } catch (Throwable th) {
                Logger.e$default(this.n, "MediaEngineLegacy", "Unable to initialize voice engine, new error discovered", th, null, 8, null);
            }
            if (this.f != null) {
                n(j.j);
            }
        }
    }
}
