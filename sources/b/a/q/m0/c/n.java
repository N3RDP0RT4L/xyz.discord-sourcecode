package b.a.q.m0.c;

import d0.u.a;
import d0.z.d.m;
import java.util.Comparator;
/* compiled from: Comparisons.kt */
/* loaded from: classes.dex */
public final class n<T> implements Comparator<T> {
    @Override // java.util.Comparator
    public final int compare(T t, T t2) {
        return a.compareValues(Integer.valueOf(!m.areEqual((String) t, "H264") ? 1 : 0), Integer.valueOf(!m.areEqual((String) t2, "H264") ? 1 : 0));
    }
}
