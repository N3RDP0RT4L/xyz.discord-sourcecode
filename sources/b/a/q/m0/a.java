package b.a.q.m0;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
/* compiled from: Codec.kt */
/* loaded from: classes.dex */
public final class a {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final int f262b;
    public final String c;
    public final int d;
    public final Integer e;

    public a(String str, int i, String str2, int i2, Integer num) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(str2, "type");
        this.a = str;
        this.f262b = i;
        this.c = str2;
        this.d = i2;
        this.e = num;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof a)) {
            return false;
        }
        a aVar = (a) obj;
        return m.areEqual(this.a, aVar.a) && this.f262b == aVar.f262b && m.areEqual(this.c, aVar.c) && this.d == aVar.d && m.areEqual(this.e, aVar.e);
    }

    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (((str != null ? str.hashCode() : 0) * 31) + this.f262b) * 31;
        String str2 = this.c;
        int hashCode2 = (((hashCode + (str2 != null ? str2.hashCode() : 0)) * 31) + this.d) * 31;
        Integer num = this.e;
        if (num != null) {
            i = num.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("Codec(name=");
        R.append(this.a);
        R.append(", priority=");
        R.append(this.f262b);
        R.append(", type=");
        R.append(this.c);
        R.append(", payloadType=");
        R.append(this.d);
        R.append(", rtxPayloadType=");
        return b.d.b.a.a.E(R, this.e, ")");
    }
}
