package b.a.q.l0;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: WiredHeadsetState.kt */
/* loaded from: classes.dex */
public abstract class a {

    /* compiled from: WiredHeadsetState.kt */
    /* renamed from: b.a.q.l0.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0040a extends a {
        public final String a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f261b;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public C0040a(String str, boolean z2) {
            super(false, null);
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            this.a = str;
            this.f261b = z2;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof C0040a)) {
                return false;
            }
            C0040a aVar = (C0040a) obj;
            return m.areEqual(this.a, aVar.a) && this.f261b == aVar.f261b;
        }

        public int hashCode() {
            String str = this.a;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            boolean z2 = this.f261b;
            if (z2) {
                z2 = true;
            }
            int i = z2 ? 1 : 0;
            int i2 = z2 ? 1 : 0;
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("PluggedIn(name=");
            R.append(this.a);
            R.append(", hasMic=");
            return b.d.b.a.a.M(R, this.f261b, ")");
        }
    }

    /* compiled from: WiredHeadsetState.kt */
    /* loaded from: classes.dex */
    public static final class b extends a {
        public static final b a = new b();

        public b() {
            super(false, null);
        }

        public String toString() {
            return "WiredHeadsetState.Unplugged";
        }
    }

    public a(boolean z2, DefaultConstructorMarker defaultConstructorMarker) {
    }
}
