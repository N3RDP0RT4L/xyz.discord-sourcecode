package b.a.q.o0;

import b.a.q.o0.e;
import co.discord.media_engine.InboundRtpVideo;
import co.discord.media_engine.OutboundRtpAudio;
import co.discord.media_engine.OutboundRtpVideo;
import co.discord.media_engine.ReceiverReport;
import co.discord.media_engine.Stats;
import co.discord.media_engine.Transport;
import com.discord.rtcconnection.KrispOveruseDetector;
import com.discord.rtcconnection.mediaengine.MediaEngineConnection;
import com.discord.utilities.logging.Logger;
import d0.z.d.k;
import d0.z.d.m;
import f0.e0.c;
import java.util.Map;
import java.util.Objects;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: RtcStatsCollector.kt */
/* loaded from: classes.dex */
public final /* synthetic */ class a extends k implements Function1<Stats, Unit> {
    public a(d dVar) {
        super(1, dVar, d.class, "onStatsReceived", "onStatsReceived(Lco/discord/media_engine/Stats;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public Unit invoke(Stats stats) {
        long j;
        OutboundRtpVideo outboundRtpVideo;
        ReceiverReport[] receiverReports;
        Stats stats2 = stats;
        m.checkNotNullParameter(stats2, "p1");
        d dVar = (d) this.receiver;
        dVar.a.add(stats2);
        if (dVar.a.size() > dVar.i) {
            dVar.a.removeFirst();
        }
        dVar.f.update(stats2);
        e eVar = dVar.g;
        synchronized (eVar) {
            m.checkNotNullParameter(stats2, "stats");
            long currentTimeMillis = eVar.q.currentTimeMillis();
            Transport transport = stats2.getTransport();
            eVar.g(null, Integer.valueOf((transport == null || (receiverReports = transport.getReceiverReports()) == null) ? 0 : receiverReports.length), currentTimeMillis);
            if (!eVar.k.a() && (outboundRtpVideo = stats2.getOutboundRtpVideo()) != null) {
                eVar.a(eVar.l, new e.g(outboundRtpVideo.getResolution().getHeight(), currentTimeMillis, new e.a(outboundRtpVideo.getFramesEncoded(), outboundRtpVideo.getFramesSent(), outboundRtpVideo.getPacketsSent(), outboundRtpVideo.getPacketsLost(), 0L, outboundRtpVideo.getBytesSent(), outboundRtpVideo.getNackCount(), outboundRtpVideo.getPliCount(), outboundRtpVideo.getQpSum(), 0L, 0L, 0L, 0L, 0L)));
                if (eVar.l.d == null && outboundRtpVideo.getFramesEncoded() > 0) {
                    eVar.l.d = Long.valueOf(currentTimeMillis - eVar.g);
                    Logger.i$default(eVar.p, "VideoQuality: outboundStats.timeToFirstFrame: " + eVar.l.d, null, 2, null);
                }
                eVar.b(outboundRtpVideo.getBitrateTarget());
            }
            if (!eVar.i.a()) {
                for (Map.Entry<String, InboundRtpVideo> entry : stats2.getInboundRtpVideo().entrySet()) {
                    String key = entry.getKey();
                    InboundRtpVideo value = entry.getValue();
                    Map<String, e.C0047e> map = eVar.m;
                    e.C0047e eVar2 = map.get(key);
                    if (eVar2 == null) {
                        eVar2 = new e.C0047e();
                        map.put(key, eVar2);
                    }
                    e.C0047e eVar3 = eVar2;
                    eVar.a(eVar3, eVar.f(value, currentTimeMillis));
                    if (eVar3.d == null && value.getFramesDecoded() > 0) {
                        byte[] bArr = c.a;
                        m.checkParameterIsNotNull(key, "$this$toLongOrDefault");
                        try {
                            j = Long.parseLong(key);
                        } catch (NumberFormatException unused) {
                            j = 0;
                        }
                        Long l = eVar.n.get(Long.valueOf(value.getSsrc()));
                        if (l != null) {
                            eVar3.d = Long.valueOf(currentTimeMillis - l.longValue());
                            Logger.i$default(eVar.p, "VideoQuality: inbound.timeToFirstFrame: " + eVar3.d + " (userId: " + j + ", ssrc: " + value.getSsrc() + ')', null, 2, null);
                        } else {
                            Logger.e$default(eVar.p, "VideoQuality: inbound.timeToFirstFrame: Unable to locate start time. (userId: " + j + ", ssrc: " + value.getSsrc() + ')', null, null, 6, null);
                        }
                    }
                }
            }
        }
        KrispOveruseDetector krispOveruseDetector = dVar.h;
        Objects.requireNonNull(krispOveruseDetector);
        m.checkNotNullParameter(stats2, "stats");
        if (krispOveruseDetector.d.getType() == MediaEngineConnection.Type.DEFAULT && krispOveruseDetector.d.b()) {
            OutboundRtpAudio outboundRtpAudio = stats2.getOutboundRtpAudio();
            if (outboundRtpAudio != null && outboundRtpAudio.getNoiseCancellerIsEnabled()) {
                OutboundRtpAudio outboundRtpAudio2 = krispOveruseDetector.f2742b;
                if (outboundRtpAudio2 != null) {
                    Pair<Boolean, Long> a = krispOveruseDetector.a(outboundRtpAudio2, stats2.getOutboundRtpAudio(), 8.0d);
                    boolean booleanValue = a.component1().booleanValue();
                    long longValue = a.component2().longValue();
                    if (booleanValue) {
                        krispOveruseDetector.d.f(KrispOveruseDetector.Status.CPU_OVERUSE);
                    } else if (longValue == 0) {
                        int i = krispOveruseDetector.c + 1;
                        krispOveruseDetector.c = i;
                        if (i > 2) {
                            krispOveruseDetector.d.f(KrispOveruseDetector.Status.FAILED);
                        }
                    } else {
                        krispOveruseDetector.c = 0;
                    }
                }
                krispOveruseDetector.f2742b = stats2.getOutboundRtpAudio();
            }
            OutboundRtpAudio outboundRtpAudio3 = stats2.getOutboundRtpAudio();
            if (outboundRtpAudio3 != null && outboundRtpAudio3.getVoiceActivityDetectorIsEnabled()) {
                OutboundRtpAudio outboundRtpAudio4 = krispOveruseDetector.a;
                if (outboundRtpAudio4 != null && krispOveruseDetector.a(outboundRtpAudio4, stats2.getOutboundRtpAudio(), 4.0d).component1().booleanValue()) {
                    krispOveruseDetector.d.f(KrispOveruseDetector.Status.VAD_CPU_OVERUSE);
                }
                krispOveruseDetector.a = stats2.getOutboundRtpAudio();
            }
        }
        return Unit.a;
    }
}
