package b.a.q.o0;

import androidx.annotation.AnyThread;
import co.discord.media_engine.InboundRtpVideo;
import com.discord.utilities.collections.Histogram;
import com.discord.utilities.collections.ListenerCollection;
import com.discord.utilities.collections.ListenerCollectionSubject;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.system.DeviceResourceUsageMonitor;
import com.discord.utilities.time.Clock;
import com.discord.utilities.time.TimeSpan;
import d0.t.h0;
import d0.t.n;
import d0.t.r;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: VideoQuality.kt */
/* loaded from: classes.dex */
public final class e {
    public final ListenerCollectionSubject<?> e;
    public final ListenerCollection<?> f;
    public final long g;
    public Long h;
    public final d i;
    public final d j;
    public final d k;
    public final f l = new f();
    public final Map<String, C0047e> m = new LinkedHashMap();
    public final Map<Long, Long> n = new LinkedHashMap();
    public final DeviceResourceUsageMonitor o;
    public final Logger p;
    public final Clock q;
    public static final b d = new b(null);
    public static final List<Integer> a = n.listOf((Object[]) new Integer[]{0, 500000, 1000000, 1500000, 2000000, 3000000, 4000000, 5000000, 6000000, 7000000, 8000000});

    /* renamed from: b  reason: collision with root package name */
    public static final List<Integer> f276b = n.listOf((Object[]) new Integer[]{0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60});
    public static final List<Integer> c = n.listOf((Object[]) new Integer[]{720, 480, 360});

    /* compiled from: VideoQuality.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public long a;

        /* renamed from: b  reason: collision with root package name */
        public long f277b;
        public long c;
        public long d;
        public long e;
        public long f;
        public long g;
        public long h;
        public long i;
        public long j;
        public long k;
        public long l;
        public long m;
        public long n;

        public a() {
            this(0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 16383);
        }

        public a(long j, long j2, long j3, long j4, long j5, long j6, long j7, long j8, long j9, long j10, long j11, long j12, long j13, long j14) {
            this.a = j;
            this.f277b = j2;
            this.c = j3;
            this.d = j4;
            this.e = j5;
            this.f = j6;
            this.g = j7;
            this.h = j8;
            this.i = j9;
            this.j = j10;
            this.k = j11;
            this.l = j12;
            this.m = j13;
            this.n = j14;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return this.a == aVar.a && this.f277b == aVar.f277b && this.c == aVar.c && this.d == aVar.d && this.e == aVar.e && this.f == aVar.f && this.g == aVar.g && this.h == aVar.h && this.i == aVar.i && this.j == aVar.j && this.k == aVar.k && this.l == aVar.l && this.m == aVar.m && this.n == aVar.n;
        }

        public int hashCode() {
            int a = a0.a.a.b.a(this.f277b);
            int a2 = a0.a.a.b.a(this.c);
            int a3 = a0.a.a.b.a(this.d);
            int a4 = a0.a.a.b.a(this.e);
            int a5 = a0.a.a.b.a(this.f);
            int a6 = a0.a.a.b.a(this.g);
            int a7 = a0.a.a.b.a(this.h);
            int a8 = a0.a.a.b.a(this.i);
            int a9 = a0.a.a.b.a(this.j);
            int a10 = a0.a.a.b.a(this.k);
            int a11 = a0.a.a.b.a(this.l);
            int a12 = a0.a.a.b.a(this.m);
            return a0.a.a.b.a(this.n) + ((a12 + ((a11 + ((a10 + ((a9 + ((a8 + ((a7 + ((a6 + ((a5 + ((a4 + ((a3 + ((a2 + ((a + (a0.a.a.b.a(this.a) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31);
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("AggregatedProperties(framesCodec=");
            R.append(this.a);
            R.append(", framesNetwork=");
            R.append(this.f277b);
            R.append(", packets=");
            R.append(this.c);
            R.append(", packetsLost=");
            R.append(this.d);
            R.append(", framesDropped=");
            R.append(this.e);
            R.append(", bytes=");
            R.append(this.f);
            R.append(", nackCount=");
            R.append(this.g);
            R.append(", pliCount=");
            R.append(this.h);
            R.append(", qpSum=");
            R.append(this.i);
            R.append(", freezeCount=");
            R.append(this.j);
            R.append(", pauseCount=");
            R.append(this.k);
            R.append(", totalFreezesDuration=");
            R.append(this.l);
            R.append(", totalPausesDuration=");
            R.append(this.m);
            R.append(", totalFramesDuration=");
            return b.d.b.a.a.B(R, this.n, ")");
        }

        public /* synthetic */ a(long j, long j2, long j3, long j4, long j5, long j6, long j7, long j8, long j9, long j10, long j11, long j12, long j13, long j14, int i) {
            this((i & 1) != 0 ? 0L : j, (i & 2) != 0 ? 0L : j2, (i & 4) != 0 ? 0L : j3, (i & 8) != 0 ? 0L : j4, (i & 16) != 0 ? 0L : j5, (i & 32) != 0 ? 0L : j6, (i & 64) != 0 ? 0L : j7, (i & 128) != 0 ? 0L : j8, (i & 256) != 0 ? 0L : j9, (i & 512) != 0 ? 0L : j10, (i & 1024) != 0 ? 0L : j11, (i & 2048) != 0 ? 0L : j12, (i & 4096) != 0 ? 0L : j13, (i & 8192) == 0 ? j14 : 0L);
        }
    }

    /* compiled from: VideoQuality.kt */
    /* loaded from: classes.dex */
    public static final class b {
        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public static final long a(b bVar, Float f) {
            if (f != null) {
                return d0.a0.a.roundToLong(f.floatValue());
            }
            return 0L;
        }
    }

    /* compiled from: VideoQuality.kt */
    /* loaded from: classes.dex */
    public static abstract class c {
    }

    /* compiled from: VideoQuality.kt */
    /* loaded from: classes.dex */
    public static final class d {
        public Long a;

        /* renamed from: b  reason: collision with root package name */
        public long f278b;

        public d(boolean z2, long j) {
            this.a = z2 ? Long.valueOf(j) : null;
        }

        public final boolean a() {
            return this.a != null;
        }

        public final void b(boolean z2, long j) {
            Long l = this.a;
            if (l == null) {
                if (z2) {
                    this.a = Long.valueOf(j);
                }
            } else if (l != null) {
                long longValue = l.longValue();
                if (!z2) {
                    this.f278b = (j - longValue) + this.f278b;
                    this.a = null;
                }
            }
        }

        public final long c(long j) {
            Long l = this.a;
            if (l == null) {
                return this.f278b;
            }
            return (this.f278b + j) - l.longValue();
        }
    }

    /* compiled from: VideoQuality.kt */
    /* renamed from: b.a.q.o0.e$e  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static class C0047e {
        public Long d;
        public long f;
        public float i;
        public List<g> a = new ArrayList();

        /* renamed from: b  reason: collision with root package name */
        public final Histogram f279b = new Histogram(5, 0, 2, null);
        public final Histogram c = new Histogram(25600, 0, 2, null);
        public a e = new a(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 16383);
        public Map<Integer, Float> g = new LinkedHashMap();
        public Map<Integer, Float> h = new LinkedHashMap();
        public Map<Integer, Float> j = new LinkedHashMap();

        public C0047e() {
            for (Number number : e.a) {
                this.g.put(Integer.valueOf(number.intValue()), Float.valueOf(0.0f));
            }
            for (Number number2 : e.f276b) {
                this.h.put(Integer.valueOf(number2.intValue()), Float.valueOf(0.0f));
            }
            for (Number number3 : e.c) {
                this.j.put(Integer.valueOf(number3.intValue()), Float.valueOf(0.0f));
            }
        }
    }

    /* compiled from: VideoQuality.kt */
    /* loaded from: classes.dex */
    public static final class f extends C0047e {
        public int k = 2500000;
        public int l = 30;
        public long m;
        public long n;
        public long o;
    }

    /* compiled from: VideoQuality.kt */
    /* loaded from: classes.dex */
    public static final class g {
        public final long a;

        /* renamed from: b  reason: collision with root package name */
        public final long f280b;
        public final a c;

        public g(long j, long j2, a aVar) {
            m.checkNotNullParameter(aVar, "aggregatedProperties");
            this.a = j;
            this.f280b = j2;
            this.c = aVar;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof g)) {
                return false;
            }
            g gVar = (g) obj;
            return this.a == gVar.a && this.f280b == gVar.f280b && m.areEqual(this.c, gVar.c);
        }

        public int hashCode() {
            int a = (a0.a.a.b.a(this.f280b) + (a0.a.a.b.a(this.a) * 31)) * 31;
            a aVar = this.c;
            return a + (aVar != null ? aVar.hashCode() : 0);
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("RawVideoStats(resolution=");
            R.append(this.a);
            R.append(", timestamp=");
            R.append(this.f280b);
            R.append(", aggregatedProperties=");
            R.append(this.c);
            R.append(")");
            return R.toString();
        }

        public g() {
            this(0L, 0L, new a(0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 16383));
        }
    }

    /* compiled from: VideoQuality.kt */
    /* loaded from: classes.dex */
    public static final class h extends o implements Function1<DeviceResourceUsageMonitor.ResourceUsage, Unit> {
        public h() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(DeviceResourceUsageMonitor.ResourceUsage resourceUsage) {
            DeviceResourceUsageMonitor.ResourceUsage resourceUsage2 = resourceUsage;
            m.checkNotNullParameter(resourceUsage2, "it");
            e eVar = e.this;
            List<Integer> list = e.a;
            synchronized (eVar) {
                for (Map.Entry<String, C0047e> entry : eVar.m.entrySet()) {
                    C0047e value = entry.getValue();
                    value.f279b.addSample(resourceUsage2.getCpuUsagePercent());
                    value.c.addSample(resourceUsage2.getMemoryRssBytes());
                }
                eVar.l.f279b.addSample(resourceUsage2.getCpuUsagePercent());
                eVar.l.c.addSample(resourceUsage2.getMemoryRssBytes());
            }
            return Unit.a;
        }
    }

    public e(Logger logger, Clock clock) {
        m.checkNotNullParameter(logger, "logger");
        m.checkNotNullParameter(clock, "clock");
        this.p = logger;
        this.q = clock;
        ListenerCollectionSubject<?> listenerCollectionSubject = new ListenerCollectionSubject<>();
        this.e = listenerCollectionSubject;
        this.f = listenerCollectionSubject;
        long currentTimeMillis = clock.currentTimeMillis();
        this.g = currentTimeMillis;
        this.i = new d(false, currentTimeMillis);
        this.j = new d(false, currentTimeMillis);
        this.k = new d(false, currentTimeMillis);
        DeviceResourceUsageMonitor deviceResourceUsageMonitor = new DeviceResourceUsageMonitor(new TimeSpan(1L, TimeUnit.SECONDS), clock, new h());
        this.o = deviceResourceUsageMonitor;
        deviceResourceUsageMonitor.start();
    }

    public final void a(C0047e eVar, g gVar) {
        eVar.a.add(gVar);
        if (eVar.a.size() >= 2) {
            List<g> list = eVar.a;
            g gVar2 = list.get(list.size() - 1);
            List<g> list2 = eVar.a;
            g gVar3 = list2.get(list2.size() - 2);
            b.a.q.o0.f fVar = b.a.q.o0.f.j;
            eVar.f = (gVar2.f280b - gVar3.f280b) + eVar.f;
            a aVar = eVar.e;
            a aVar2 = gVar2.c;
            a aVar3 = gVar3.c;
            aVar.a = fVar.a(aVar2.a, aVar3.a) + aVar.a;
            aVar.f277b = fVar.a(aVar2.f277b, aVar3.f277b) + aVar.f277b;
            aVar.c = fVar.a(aVar2.c, aVar3.c) + aVar.c;
            aVar.d = fVar.a(aVar2.d, aVar3.d) + aVar.d;
            aVar.e = fVar.a(aVar2.e, aVar3.e) + aVar.e;
            aVar.f = fVar.a(aVar2.f, aVar3.f) + aVar.f;
            aVar.g = fVar.a(aVar2.g, aVar3.g) + aVar.g;
            aVar.h = fVar.a(aVar2.h, aVar3.h) + aVar.h;
            aVar.i = fVar.a(aVar2.i, aVar3.i) + aVar.i;
            aVar.j = fVar.a(aVar2.j, aVar3.j) + aVar.j;
            aVar.k = fVar.a(aVar2.j, aVar3.j) + aVar.k;
            aVar.l = fVar.a(aVar2.l, aVar3.l) + aVar.l;
            aVar.m = fVar.a(aVar2.m, aVar3.m) + aVar.m;
            aVar.n = fVar.a(aVar2.n, aVar3.n) + aVar.n;
            a aVar4 = gVar2.c;
            long j = aVar4.f;
            long j2 = aVar4.a;
            long j3 = gVar2.f280b;
            long j4 = gVar2.a;
            float f2 = ((float) (j3 - gVar3.f280b)) / 1000.0f;
            eVar.i = (((float) j4) * f2) + eVar.i;
            if (eVar.a.size() >= 6) {
                List<g> list3 = eVar.a;
                g gVar4 = list3.get(list3.size() - 3);
                a aVar5 = gVar4.c;
                long j5 = aVar5.f;
                long j6 = aVar5.a;
                long j7 = gVar4.f280b;
                Iterator<T> it = c.iterator();
                while (true) {
                    float f3 = 0.0f;
                    if (!it.hasNext()) {
                        break;
                    }
                    j2 = j2;
                    int intValue = ((Number) it.next()).intValue();
                    j6 = j6;
                    if (j4 <= intValue) {
                        Map<Integer, Float> map = eVar.j;
                        Integer valueOf = Integer.valueOf(intValue);
                        Float f4 = eVar.j.get(Integer.valueOf(intValue));
                        if (f4 != null) {
                            f3 = f4.floatValue();
                        }
                        map.put(valueOf, Float.valueOf(f3 + f2));
                    }
                }
                float f5 = ((float) (j3 - j7)) / 1000.0f;
                float f6 = ((float) ((j - j5) * 8)) / f5;
                float f7 = ((float) (j2 - j6)) / f5;
                for (Number number : a) {
                    int intValue2 = number.intValue();
                    if (f6 <= intValue2) {
                        Map<Integer, Float> map2 = eVar.g;
                        Integer valueOf2 = Integer.valueOf(intValue2);
                        Float f8 = eVar.g.get(Integer.valueOf(intValue2));
                        map2.put(valueOf2, Float.valueOf((f8 != null ? f8.floatValue() : 0.0f) + f2));
                    }
                }
                for (Number number2 : f276b) {
                    int intValue3 = number2.intValue();
                    if (f7 <= intValue3) {
                        Map<Integer, Float> map3 = eVar.h;
                        Integer valueOf3 = Integer.valueOf(intValue3);
                        Float f9 = eVar.h.get(Integer.valueOf(intValue3));
                        map3.put(valueOf3, Float.valueOf((f9 != null ? f9.floatValue() : 0.0f) + f2));
                    }
                }
                r.removeFirst(eVar.a);
            }
        }
    }

    public final void b(int i) {
        if (this.l.a.size() >= 2) {
            List<g> list = this.l.a;
            long j = list.get(list.size() - 1).f280b;
            List<g> list2 = this.l.a;
            float f2 = ((float) (j - list2.get(list2.size() - 2).f280b)) / 1000.0f;
            f fVar = this.l;
            long j2 = fVar.m;
            Float valueOf = Float.valueOf(fVar.l * f2);
            long j3 = 0;
            fVar.m = j2 + (valueOf != null ? d0.a0.a.roundToLong(valueOf.floatValue()) : 0L);
            f fVar2 = this.l;
            long j4 = fVar2.n;
            Float valueOf2 = Float.valueOf((i / 8.0f) * f2);
            fVar2.n = j4 + (valueOf2 != null ? d0.a0.a.roundToLong(valueOf2.floatValue()) : 0L);
            f fVar3 = this.l;
            long j5 = fVar3.o;
            Float valueOf3 = Float.valueOf((fVar3.k / 8.0f) * f2);
            if (valueOf3 != null) {
                j3 = d0.a0.a.roundToLong(valueOf3.floatValue());
            }
            fVar3.o = j5 + j3;
        }
    }

    @AnyThread
    public final synchronized Map<String, Object> c(String str) {
        C0047e eVar;
        m.checkNotNullParameter(str, "userId");
        eVar = this.m.get(str);
        return eVar != null ? e(eVar) : null;
    }

    @AnyThread
    public final synchronized Map<String, Object> d() {
        f fVar;
        float f2;
        int i;
        Float valueOf;
        Float valueOf2;
        Float valueOf3;
        fVar = this.l;
        f2 = ((float) fVar.f) / 1000.0f;
        i = (f2 > 0 ? 1 : (f2 == 0 ? 0 : -1));
        return h0.plus(e(this.l), h0.mapOf(d0.o.to("target_bitrate_max", Long.valueOf((i <= 0 || (valueOf3 = Float.valueOf(((float) (fVar.o * ((long) 8))) / f2)) == null) ? 0L : d0.a0.a.roundToLong(valueOf3.floatValue()))), d0.o.to("target_bitrate_network", Long.valueOf((i <= 0 || (valueOf2 = Float.valueOf(((float) (this.l.n * ((long) 8))) / f2)) == null) ? 0L : d0.a0.a.roundToLong(valueOf2.floatValue()))), d0.o.to("target_fps", Long.valueOf((i <= 0 || (valueOf = Float.valueOf(((float) this.l.m) / f2)) == null) ? 0L : d0.a0.a.roundToLong(valueOf.floatValue()))), d0.o.to("duration_encoder_nvidia_cuda", 0L), d0.o.to("duration_encoder_nvidia_direct3d", 0L), d0.o.to("duration_encoder_openh264", 0L), d0.o.to("duration_encoder_videotoolbox", 0L), d0.o.to("duration_encoder_amd_direct3d", 0L), d0.o.to("duration_encoder_intel", 0L), d0.o.to("duration_encoder_intel_direct3d", 0L), d0.o.to("duration_encoder_unknown", 0L)));
    }

    public final Map<String, Object> e(C0047e eVar) {
        Float valueOf;
        long currentTimeMillis = this.q.currentTimeMillis();
        Long l = this.h;
        float longValue = ((float) (l != null ? l.longValue() - this.g : currentTimeMillis - this.g)) / 1000.0f;
        float f2 = ((float) eVar.f) / 1000.0f;
        int i = (f2 > 0 ? 1 : (f2 == 0 ? 0 : -1));
        long roundToLong = (i <= 0 || (valueOf = Float.valueOf(eVar.i / f2)) == null) ? 0L : d0.a0.a.roundToLong(valueOf.floatValue());
        Histogram.Report report = eVar.f279b.getReport();
        Histogram.Report report2 = eVar.c.getReport();
        b bVar = d;
        long j = 1024;
        Map mapOf = h0.mapOf(d0.o.to("duration", Double.valueOf(Math.floor(longValue))), b.d.b.a.a.Z(bVar, eVar.g.get(8000000), "duration_stream_under_8mbps"), b.d.b.a.a.Z(bVar, eVar.g.get(7000000), "duration_stream_under_7mbps"), b.d.b.a.a.Z(bVar, eVar.g.get(6000000), "duration_stream_under_6mbps"), b.d.b.a.a.Z(bVar, eVar.g.get(5000000), "duration_stream_under_5mbps"), b.d.b.a.a.Z(bVar, eVar.g.get(4000000), "duration_stream_under_4mbps"), b.d.b.a.a.Z(bVar, eVar.g.get(3000000), "duration_stream_under_3mbps"), b.d.b.a.a.Z(bVar, eVar.g.get(2000000), "duration_stream_under_2mbps"), b.d.b.a.a.Z(bVar, eVar.g.get(1500000), "duration_stream_under_1_5mbps"), b.d.b.a.a.Z(bVar, eVar.g.get(1000000), "duration_stream_under_1mbps"), b.d.b.a.a.Z(bVar, eVar.g.get(500000), "duration_stream_under_0_5mbps"), b.d.b.a.a.Z(bVar, eVar.g.get(0), "duration_stream_at_0mbps"), b.d.b.a.a.Z(bVar, eVar.h.get(60), "duration_fps_under_60"), b.d.b.a.a.Z(bVar, eVar.h.get(55), "duration_fps_under_55"), b.d.b.a.a.Z(bVar, eVar.h.get(50), "duration_fps_under_50"), b.d.b.a.a.Z(bVar, eVar.h.get(45), "duration_fps_under_45"), b.d.b.a.a.Z(bVar, eVar.h.get(40), "duration_fps_under_40"), b.d.b.a.a.Z(bVar, eVar.h.get(35), "duration_fps_under_35"), b.d.b.a.a.Z(bVar, eVar.h.get(30), "duration_fps_under_30"), b.d.b.a.a.Z(bVar, eVar.h.get(25), "duration_fps_under_25"), b.d.b.a.a.Z(bVar, eVar.h.get(20), "duration_fps_under_20"), b.d.b.a.a.Z(bVar, eVar.h.get(15), "duration_fps_under_15"), b.d.b.a.a.Z(bVar, eVar.h.get(10), "duration_fps_under_10"), b.d.b.a.a.Z(bVar, eVar.h.get(5), "duration_fps_under_5"), b.d.b.a.a.Z(bVar, eVar.h.get(0), "duration_fps_at_0"), d0.o.to("avg_resolution", Long.valueOf(roundToLong)), b.d.b.a.a.Z(bVar, eVar.j.get(720), "duration_resolution_under_720"), b.d.b.a.a.Z(bVar, eVar.j.get(480), "duration_resolution_under_480"), b.d.b.a.a.Z(bVar, eVar.j.get(360), "duration_resolution_under_360"), d0.o.to("num_pauses", 0), d0.o.to("duration_paused", Long.valueOf(b.a(bVar, Float.valueOf(((float) this.i.c(currentTimeMillis)) / 1000.0f)))), d0.o.to("duration_zero_receivers", Long.valueOf(b.a(bVar, Float.valueOf(((float) this.j.c(currentTimeMillis)) / 1000.0f)))), d0.o.to("duration_video_stopped", Long.valueOf(b.a(bVar, Float.valueOf(((float) this.k.c(currentTimeMillis)) / 1000.0f)))), d0.o.to("client_performance_cpu_percentile25", Long.valueOf(report.getPercentile25())), d0.o.to("client_performance_cpu_percentile50", Long.valueOf(report.getPercentile50())), d0.o.to("client_performance_cpu_percentile75", Long.valueOf(report.getPercentile75())), d0.o.to("client_performance_cpu_percentile90", Long.valueOf(report.getPercentile90())), d0.o.to("client_performance_cpu_percentile95", Long.valueOf(report.getPercentile95())), d0.o.to("client_performance_memory_percentile25", Long.valueOf(report2.getPercentile25() / j)), d0.o.to("client_performance_memory_percentile50", Long.valueOf(report2.getPercentile50() / j)), d0.o.to("client_performance_memory_percentile75", Long.valueOf(report2.getPercentile75() / j)), d0.o.to("client_performance_memory_percentile90", Long.valueOf(report2.getPercentile90() / j)), d0.o.to("client_performance_memory_percentile95", Long.valueOf(report2.getPercentile95() / j)), d0.o.to("client_performance_memory_min", Long.valueOf(report2.getMin() / j)), d0.o.to("client_performance_memory_max", Long.valueOf(report2.getMax() / j)));
        a aVar = eVar.e;
        long a2 = i > 0 ? b.a(bVar, Float.valueOf(((float) (aVar.f * 8)) / f2)) : 0L;
        long a3 = i > 0 ? b.a(bVar, Float.valueOf(((float) aVar.a) / f2)) : 0L;
        Pair[] pairArr = new Pair[16];
        pairArr[0] = d0.o.to("avg_bitrate", Long.valueOf(a2));
        pairArr[1] = d0.o.to("avg_fps", Long.valueOf(a3));
        pairArr[2] = d0.o.to("num_bytes", Long.valueOf(aVar.f));
        pairArr[3] = d0.o.to("num_packets_lost", Long.valueOf(aVar.d));
        pairArr[4] = d0.o.to("num_packets", Long.valueOf(aVar.c));
        pairArr[5] = d0.o.to("num_frames", Long.valueOf(aVar.f277b));
        Long l2 = eVar.d;
        pairArr[6] = d0.o.to("time_to_first_frame_ms", Long.valueOf(l2 != null ? l2.longValue() : 0L));
        pairArr[7] = d0.o.to("num_frames_dropped", Long.valueOf(aVar.e));
        pairArr[8] = d0.o.to("num_nacks", Long.valueOf(aVar.g));
        pairArr[9] = d0.o.to("num_plis", Long.valueOf(aVar.h));
        pairArr[10] = d0.o.to("qp_sum", Long.valueOf(aVar.i));
        pairArr[11] = d0.o.to("receiver_freeze_count", Long.valueOf(aVar.j));
        pairArr[12] = d0.o.to("receiver_pause_count", Long.valueOf(aVar.k));
        pairArr[13] = d0.o.to("receiver_total_freezes_duration", Long.valueOf(aVar.l));
        pairArr[14] = d0.o.to("receiver_total_pauses_duration", Long.valueOf(aVar.m));
        pairArr[15] = d0.o.to("receiver_total_frames_duration", Long.valueOf(aVar.n));
        return h0.plus(mapOf, h0.mapOf(pairArr));
    }

    public final g f(InboundRtpVideo inboundRtpVideo, long j) {
        return new g(inboundRtpVideo.getResolution().getHeight(), j, new a(inboundRtpVideo.getFramesDecoded(), inboundRtpVideo.getFramesReceived(), inboundRtpVideo.getPacketsReceived(), inboundRtpVideo.getPacketsLost(), inboundRtpVideo.getFramesDropped(), inboundRtpVideo.getBytesReceived(), inboundRtpVideo.getNackCount(), inboundRtpVideo.getPliCount(), inboundRtpVideo.getQpSum(), inboundRtpVideo.getFreezeCount(), inboundRtpVideo.getPauseCount(), inboundRtpVideo.getTotalFreezesDuration(), inboundRtpVideo.getTotalPausesDuration(), inboundRtpVideo.getTotalFramesDuration()));
    }

    public final void g(Boolean bool, Integer num, long j) {
        boolean z2 = false;
        if (num != null) {
            this.j.b(num.intValue() == 0, j);
        }
        if (this.i.a() || this.j.a()) {
            z2 = true;
        }
        if (z2 != this.k.a()) {
            this.k.b(z2, j);
            this.l.a.clear();
        }
    }
}
