package b.a.q;

import b.d.b.a.a;
import com.discord.rtcconnection.RtcConnection;
import rx.functions.Action1;
/* compiled from: RtcConnection.kt */
/* loaded from: classes.dex */
public final class q<T> implements Action1<Throwable> {
    public final /* synthetic */ RtcConnection j;

    public q(RtcConnection rtcConnection) {
        this.j = rtcConnection;
    }

    @Override // rx.functions.Action1
    public void call(Throwable th) {
        RtcConnection rtcConnection = this.j;
        StringBuilder R = a.R("failed to handle connectivity change in ");
        R.append(this.j.l);
        RtcConnection.o(rtcConnection, R.toString(), th, null, 4);
    }
}
