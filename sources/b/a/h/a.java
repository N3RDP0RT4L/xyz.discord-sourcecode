package b.a.h;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import androidx.browser.customtabs.CustomTabsService;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
/* compiled from: CustomTabsPackages.kt */
/* loaded from: classes.dex */
public final class a {
    /* JADX WARN: Multi-variable type inference failed */
    public static final String a(Context context) {
        Object obj;
        Object obj2;
        Object obj3;
        m.checkNotNullParameter(context, "$this$getCustomTabsDefaultPackage");
        PackageManager packageManager = context.getPackageManager();
        String str = null;
        Intent data = new Intent().setAction("android.intent.action.VIEW").addCategory("android.intent.category.BROWSABLE").setData(Uri.fromParts("http", "", null));
        m.checkNotNullExpressionValue(data, "Intent()\n          .setA…mParts(\"http\", \"\", null))");
        List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(data, 0);
        m.checkNotNullExpressionValue(queryIntentActivities, "packageManager.queryInte…ltViewIntentHandler(), 0)");
        ArrayList arrayList = new ArrayList();
        for (ResolveInfo resolveInfo : queryIntentActivities) {
            Intent intent = new Intent().setAction(CustomTabsService.ACTION_CUSTOM_TABS_CONNECTION).setPackage(resolveInfo.activityInfo.packageName);
            m.checkNotNullExpressionValue(intent, "Intent()\n            .se…activityInfo.packageName)");
            String str2 = context.getPackageManager().resolveService(intent, 0) != null ? resolveInfo.activityInfo.packageName : null;
            if (str2 != null) {
                arrayList.add(str2);
            }
        }
        Iterator it = arrayList.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (m.areEqual((String) obj, "com.android.chrome")) {
                break;
            }
        }
        String str3 = (String) obj;
        if (str3 == null) {
            Iterator it2 = arrayList.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    obj3 = null;
                    break;
                }
                obj3 = it2.next();
                if (m.areEqual((String) obj3, "com.chrome.beta")) {
                    break;
                }
            }
            str3 = (String) obj3;
        }
        if (str3 == null) {
            Iterator it3 = arrayList.iterator();
            while (true) {
                if (!it3.hasNext()) {
                    obj2 = null;
                    break;
                }
                obj2 = it3.next();
                if (m.areEqual((String) obj2, "com.chrome.dev")) {
                    break;
                }
            }
            str3 = (String) obj2;
        }
        if (str3 == null) {
            Iterator it4 = arrayList.iterator();
            while (true) {
                if (!it4.hasNext()) {
                    break;
                }
                Object next = it4.next();
                if (m.areEqual((String) next, "com.google.android.apps.chrome")) {
                    str = next;
                    break;
                }
            }
            str3 = str;
        }
        return str3 != null ? str3 : (String) u.firstOrNull((List<? extends Object>) arrayList);
    }
}
