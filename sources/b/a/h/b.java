package b.a.h;

import android.content.ComponentName;
import android.net.Uri;
import android.os.Bundle;
import androidx.browser.customtabs.CustomTabsCallback;
import androidx.browser.customtabs.CustomTabsClient;
import androidx.browser.customtabs.CustomTabsService;
import androidx.browser.customtabs.CustomTabsServiceConnection;
import androidx.browser.customtabs.CustomTabsSession;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
/* compiled from: CustomTabsService.kt */
/* loaded from: classes.dex */
public final class b extends CustomTabsServiceConnection {
    public final List<Uri> j;

    /* JADX WARN: Multi-variable type inference failed */
    public b(List<? extends Uri> list) {
        m.checkNotNullParameter(list, "uris");
        this.j = list;
    }

    @Override // androidx.browser.customtabs.CustomTabsServiceConnection
    public void onCustomTabsServiceConnected(ComponentName componentName, CustomTabsClient customTabsClient) {
        m.checkNotNullParameter(componentName, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(customTabsClient, "client");
        customTabsClient.warmup(0L);
        CustomTabsSession newSession = customTabsClient.newSession(new CustomTabsCallback());
        if (newSession != null) {
            Uri uri = (Uri) u.first((List<? extends Object>) this.j);
            List<Uri> drop = u.drop(this.j, 1);
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(drop, 10));
            for (Uri uri2 : drop) {
                Bundle bundle = new Bundle();
                bundle.putParcelable(CustomTabsService.KEY_URL, uri2);
                arrayList.add(bundle);
            }
            newSession.mayLaunchUrl(uri, Bundle.EMPTY, arrayList);
        }
    }

    @Override // android.content.ServiceConnection
    public void onServiceDisconnected(ComponentName componentName) {
    }
}
