package b.a.a;

import b.a.a.w;
import com.discord.models.user.User;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetUrgentMessageDialogViewModel.kt */
/* loaded from: classes.dex */
public final class u extends o implements Function1<w.c, Unit> {
    public final /* synthetic */ w this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public u(w wVar) {
        super(1);
        this.this$0 = wVar;
    }

    @Override // kotlin.jvm.functions.Function1
    public Unit invoke(w.c cVar) {
        w.c cVar2 = cVar;
        m.checkNotNullParameter(cVar2, "storeState");
        w wVar = this.this$0;
        Objects.requireNonNull(wVar);
        User user = cVar2.a;
        w.d viewState = wVar.getViewState();
        wVar.updateViewState(new w.d(Integer.valueOf(user.getFlags()), viewState == null ? false : viewState.f49b));
        return Unit.a;
    }
}
