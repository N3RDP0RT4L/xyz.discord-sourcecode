package b.a.a;

import b.a.a.w;
import com.discord.api.user.User;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.subjects.PublishSubject;
/* compiled from: WidgetUrgentMessageDialogViewModel.kt */
/* loaded from: classes.dex */
public final class x extends o implements Function1<User, Unit> {
    public final /* synthetic */ w this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public x(w wVar) {
        super(1);
        this.this$0 = wVar;
    }

    @Override // kotlin.jvm.functions.Function1
    public Unit invoke(User user) {
        m.checkNotNullParameter(user, "it");
        PublishSubject<w.b> publishSubject = this.this$0.k;
        publishSubject.k.onNext(w.b.a.a);
        return Unit.a;
    }
}
