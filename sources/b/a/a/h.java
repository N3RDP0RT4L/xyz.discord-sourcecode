package b.a.a;

import andhook.lib.HookHelper;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.a.p;
import b.a.d.f;
import b.a.d.f0;
import b.a.d.h0;
import b.a.k.b;
import com.discord.app.AppDialog;
import com.discord.app.AppViewModel;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.google.android.material.button.MaterialButton;
import d0.z.d.a0;
import d0.z.d.k;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetAccessibilityDetectionDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 \u00152\u00020\u0001:\u0001\u0016B\u0007¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006R\u001d\u0010\f\u001a\u00020\u00078B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR\u001d\u0010\u0012\u001a\u00020\r8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011¨\u0006\u0017"}, d2 = {"Lb/a/a/h;", "Lcom/discord/app/AppDialog;", "Landroid/view/View;", "view", "", "onViewBound", "(Landroid/view/View;)V", "Lb/a/a/p;", "m", "Lkotlin/Lazy;", "getViewModel", "()Lb/a/a/p;", "viewModel", "Lb/a/i/a;", "l", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "h", "()Lb/a/i/a;", "binding", HookHelper.constructorName, "()V", "k", "b", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class h extends AppDialog {
    public static final /* synthetic */ KProperty[] j = {b.d.b.a.a.b0(h.class, "binding", "getBinding()Lcom/discord/databinding/AllowAccessibilityDetectionDialogBinding;", 0)};
    public static final b k = new b(null);
    public final FragmentViewBindingDelegate l = FragmentViewBindingDelegateKt.viewBinding$default(this, c.j, null, 2, null);
    public final Lazy m;

    /* compiled from: java-style lambda group */
    /* loaded from: classes2.dex */
    public static final class a implements View.OnClickListener {
        public final /* synthetic */ int j;
        public final /* synthetic */ Object k;

        public a(int i, Object obj) {
            this.j = i;
            this.k = obj;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            int i = this.j;
            if (i == 0) {
                h.g((h) this.k, false);
            } else if (i == 1) {
                h.g((h) this.k, true);
            } else {
                throw null;
            }
        }
    }

    /* compiled from: WidgetAccessibilityDetectionDialog.kt */
    /* loaded from: classes.dex */
    public static final class b {
        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: WidgetAccessibilityDetectionDialog.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class c extends k implements Function1<View, b.a.i.a> {
        public static final c j = new c();

        public c() {
            super(1, b.a.i.a.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/AllowAccessibilityDetectionDialogBinding;", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public b.a.i.a invoke(View view) {
            View view2 = view;
            m.checkNotNullParameter(view2, "p1");
            int i = R.id.accessibility_detection_body_text;
            TextView textView = (TextView) view2.findViewById(R.id.accessibility_detection_body_text);
            if (textView != null) {
                i = R.id.accessibility_detection_disable;
                MaterialButton materialButton = (MaterialButton) view2.findViewById(R.id.accessibility_detection_disable);
                if (materialButton != null) {
                    i = R.id.accessibility_detection_enable;
                    MaterialButton materialButton2 = (MaterialButton) view2.findViewById(R.id.accessibility_detection_enable);
                    if (materialButton2 != null) {
                        i = R.id.view_dialog_confirmation_header;
                        TextView textView2 = (TextView) view2.findViewById(R.id.view_dialog_confirmation_header);
                        if (textView2 != null) {
                            return new b.a.i.a((LinearLayout) view2, textView, materialButton, materialButton2, textView2);
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
        }
    }

    /* compiled from: WidgetAccessibilityDetectionDialog.kt */
    /* loaded from: classes.dex */
    public static final class d extends o implements Function0<AppViewModel<p.a>> {
        public static final d j = new d();

        public d() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public AppViewModel<p.a> invoke() {
            return new p(null, null, 3);
        }
    }

    public h() {
        super(R.layout.allow_accessibility_detection_dialog);
        d dVar = d.j;
        f0 f0Var = new f0(this);
        this.m = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(p.class), new k(0, f0Var), new h0(dVar));
    }

    public static final void g(h hVar, boolean z2) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(((p) hVar.m.getValue()).j.setIsAccessibilityDetectionAllowed(z2), false, 1, null), p.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, q.j);
        hVar.dismiss();
    }

    public final b.a.i.a h() {
        return (b.a.i.a) this.l.getValue((Fragment) this, j[0]);
    }

    @Override // com.discord.app.AppDialog
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        h().c.setOnClickListener(new a(0, this));
        h().d.setOnClickListener(new a(1, this));
        TextView textView = h().f72b;
        m.checkNotNullExpressionValue(textView, "binding.accessibilityDetectionBodyText");
        b.a.k.b.m(textView, R.string.accessibility_detection_modal_body, new Object[]{f.a.a(360035966492L, null)}, (r4 & 4) != 0 ? b.g.j : null);
    }
}
