package b.a.a.a;

import androidx.annotation.StringRes;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelSubscription;
import com.discord.stores.StoreGuildBoost;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreSubscriptions;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func2;
/* compiled from: GuildBoostCancelViewModel.kt */
/* loaded from: classes.dex */
public final class f extends AppViewModel<c> {
    public static final a j = new a(null);
    public final long k;
    public final StoreGuildBoost l;
    public final StoreSubscriptions m;
    public final RestAPI n;

    /* compiled from: GuildBoostCancelViewModel.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: GuildBoostCancelViewModel.kt */
    /* loaded from: classes.dex */
    public static final class b {
        public final StoreGuildBoost.State a;

        /* renamed from: b  reason: collision with root package name */
        public final StoreSubscriptions.SubscriptionsState f41b;

        public b(StoreGuildBoost.State state, StoreSubscriptions.SubscriptionsState subscriptionsState) {
            m.checkNotNullParameter(state, "guildBoostState");
            m.checkNotNullParameter(subscriptionsState, "subscriptionState");
            this.a = state;
            this.f41b = subscriptionsState;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return m.areEqual(this.a, bVar.a) && m.areEqual(this.f41b, bVar.f41b);
        }

        public int hashCode() {
            StoreGuildBoost.State state = this.a;
            int i = 0;
            int hashCode = (state != null ? state.hashCode() : 0) * 31;
            StoreSubscriptions.SubscriptionsState subscriptionsState = this.f41b;
            if (subscriptionsState != null) {
                i = subscriptionsState.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("StoreState(guildBoostState=");
            R.append(this.a);
            R.append(", subscriptionState=");
            R.append(this.f41b);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: GuildBoostCancelViewModel.kt */
    /* loaded from: classes.dex */
    public static abstract class c {
        public final boolean a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f42b;
        public final boolean c;
        public final boolean d;
        public final boolean e;

        /* compiled from: GuildBoostCancelViewModel.kt */
        /* loaded from: classes.dex */
        public static final class a extends c {
            public final ModelSubscription f;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public a(ModelSubscription modelSubscription) {
                super(false, true, false, true, false, null);
                m.checkNotNullParameter(modelSubscription, Traits.Payment.Type.SUBSCRIPTION);
                this.f = modelSubscription;
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof a) && m.areEqual(this.f, ((a) obj).f);
                }
                return true;
            }

            public int hashCode() {
                ModelSubscription modelSubscription = this.f;
                if (modelSubscription != null) {
                    return modelSubscription.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = b.d.b.a.a.R("CancelInProgress(subscription=");
                R.append(this.f);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: GuildBoostCancelViewModel.kt */
        /* loaded from: classes.dex */
        public static final class b extends c {
            public final ModelSubscription f;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public b(ModelSubscription modelSubscription) {
                super(false, false, false, false, true, null);
                m.checkNotNullParameter(modelSubscription, Traits.Payment.Type.SUBSCRIPTION);
                this.f = modelSubscription;
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof b) && m.areEqual(this.f, ((b) obj).f);
                }
                return true;
            }

            public int hashCode() {
                ModelSubscription modelSubscription = this.f;
                if (modelSubscription != null) {
                    return modelSubscription.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = b.d.b.a.a.R("Cancelled(subscription=");
                R.append(this.f);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: GuildBoostCancelViewModel.kt */
        /* renamed from: b.a.a.a.f$c$c  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C0010c extends c {
            public final Integer f;

            public C0010c() {
                this(null, 1);
            }

            /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
            public /* synthetic */ C0010c(Integer num, int i) {
                this(null);
                int i2 = i & 1;
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof C0010c) && m.areEqual(this.f, ((C0010c) obj).f);
                }
                return true;
            }

            public int hashCode() {
                Integer num = this.f;
                if (num != null) {
                    return num.hashCode();
                }
                return 0;
            }

            public String toString() {
                return b.d.b.a.a.E(b.d.b.a.a.R("Dismiss(dismissStringId="), this.f, ")");
            }

            public C0010c(@StringRes Integer num) {
                super(false, false, false, false, true, null);
                this.f = num;
            }
        }

        /* compiled from: GuildBoostCancelViewModel.kt */
        /* loaded from: classes.dex */
        public static final class d extends c {
            public final ModelSubscription f;

            public d(ModelSubscription modelSubscription) {
                super(true, true, true, false, true, null);
                this.f = modelSubscription;
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof d) && m.areEqual(this.f, ((d) obj).f);
                }
                return true;
            }

            public int hashCode() {
                ModelSubscription modelSubscription = this.f;
                if (modelSubscription != null) {
                    return modelSubscription.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = b.d.b.a.a.R("FailureCancelling(subscription=");
                R.append(this.f);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: GuildBoostCancelViewModel.kt */
        /* loaded from: classes.dex */
        public static final class e extends c {
            public final ModelSubscription f;
            public final boolean g;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public e(ModelSubscription modelSubscription, boolean z2) {
                super(false, true, true, false, true, null);
                m.checkNotNullParameter(modelSubscription, Traits.Payment.Type.SUBSCRIPTION);
                this.f = modelSubscription;
                this.g = z2;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof e)) {
                    return false;
                }
                e eVar = (e) obj;
                return m.areEqual(this.f, eVar.f) && this.g == eVar.g;
            }

            public int hashCode() {
                ModelSubscription modelSubscription = this.f;
                int hashCode = (modelSubscription != null ? modelSubscription.hashCode() : 0) * 31;
                boolean z2 = this.g;
                if (z2) {
                    z2 = true;
                }
                int i = z2 ? 1 : 0;
                int i2 = z2 ? 1 : 0;
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = b.d.b.a.a.R("Loaded(subscription=");
                R.append(this.f);
                R.append(", isFromInventory=");
                return b.d.b.a.a.M(R, this.g, ")");
            }
        }

        /* compiled from: GuildBoostCancelViewModel.kt */
        /* renamed from: b.a.a.a.f$c$f  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C0011f extends c {
            public static final C0011f f = new C0011f();

            public C0011f() {
                super(false, true, false, false, true, null);
            }
        }

        public c(boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, DefaultConstructorMarker defaultConstructorMarker) {
            this.a = z2;
            this.f42b = z3;
            this.c = z4;
            this.d = z5;
            this.e = z6;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public f(long j2, StoreGuildBoost storeGuildBoost, StoreSubscriptions storeSubscriptions, RestAPI restAPI, Observable observable, int i) {
        super(c.C0011f.f);
        Observable observable2;
        StoreGuildBoost guildBoosts = (i & 2) != 0 ? StoreStream.Companion.getGuildBoosts() : null;
        StoreSubscriptions subscriptions = (i & 4) != 0 ? StoreStream.Companion.getSubscriptions() : null;
        RestAPI api = (i & 8) != 0 ? RestAPI.Companion.getApi() : null;
        if ((i & 16) != 0) {
            Observable observeGuildBoostState$default = StoreGuildBoost.observeGuildBoostState$default(guildBoosts, null, 1, null);
            Observable<StoreSubscriptions.SubscriptionsState> observeSubscriptions = subscriptions.observeSubscriptions();
            e eVar = e.j;
            observable2 = Observable.j(observeGuildBoostState$default, observeSubscriptions, (Func2) (eVar != null ? new h(eVar) : eVar));
            m.checkNotNullExpressionValue(observable2, "Observable.combineLatest…   ::StoreState\n        )");
        } else {
            observable2 = null;
        }
        m.checkNotNullParameter(guildBoosts, "storeGuildBoost");
        m.checkNotNullParameter(subscriptions, "storeSubscriptions");
        m.checkNotNullParameter(api, "api");
        m.checkNotNullParameter(observable2, "storeObservable");
        this.k = j2;
        this.l = guildBoosts;
        this.m = subscriptions;
        this.n = api;
        guildBoosts.fetchUserGuildBoostState();
        subscriptions.fetchSubscriptions();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable2), this, null, 2, null), f.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new d(this));
    }
}
