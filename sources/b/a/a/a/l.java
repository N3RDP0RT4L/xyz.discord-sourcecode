package b.a.a.a;

import b.a.a.a.k;
import com.discord.utilities.premium.GuildBoostUtils;
import d0.z.d.o;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: GuildBoostUncancelViewModel.kt */
/* loaded from: classes.dex */
public final class l extends o implements Function1<GuildBoostUtils.ModifyGuildBoostSlotResult, Unit> {
    public final /* synthetic */ k.c.b $loadedViewState;
    public final /* synthetic */ k this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public l(k kVar, k.c.b bVar) {
        super(1);
        this.this$0 = kVar;
        this.$loadedViewState = bVar;
    }

    /* JADX WARN: Code restructure failed: missing block: B:7:0x0012, code lost:
        if (r6 != 2) goto L10;
     */
    @Override // kotlin.jvm.functions.Function1
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public kotlin.Unit invoke(com.discord.utilities.premium.GuildBoostUtils.ModifyGuildBoostSlotResult r6) {
        /*
            r5 = this;
            com.discord.utilities.premium.GuildBoostUtils$ModifyGuildBoostSlotResult r6 = (com.discord.utilities.premium.GuildBoostUtils.ModifyGuildBoostSlotResult) r6
            java.lang.String r0 = "result"
            d0.z.d.m.checkNotNullParameter(r6, r0)
            int r6 = r6.ordinal()
            if (r6 == 0) goto L24
            r0 = 1
            if (r6 == r0) goto L15
            r0 = 2
            if (r6 == r0) goto L24
            goto L2b
        L15:
            b.a.a.a.k r6 = r5.this$0
            b.a.a.a.k$c$b r1 = r5.$loadedViewState
            r2 = 0
            r3 = 0
            r4 = 3
            b.a.a.a.k$c$b r0 = b.a.a.a.k.c.b.a(r1, r2, r3, r0, r4)
            r6.updateViewState(r0)
            goto L2b
        L24:
            b.a.a.a.k r6 = r5.this$0
            b.a.a.a.k$c$d r0 = b.a.a.a.k.c.d.a
            r6.updateViewState(r0)
        L2b:
            kotlin.Unit r6 = kotlin.Unit.a
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.a.a.l.invoke(java.lang.Object):java.lang.Object");
    }
}
