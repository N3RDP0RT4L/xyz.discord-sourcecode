package b.a.a.a;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.a.i.q;
import b.a.k.b;
import com.airbnb.lottie.LottieAnimationView;
import com.discord.app.AppDialog;
import com.discord.utilities.animations.LottieAnimationUtilsKt;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.ranges.IntRange;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: GuildBoostActivatedDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \u00142\u00020\u0001:\u0001\u0015B\u0007¢\u0006\u0004\b\u0013\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0007\u0010\bR\u001e\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\t8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\n\u0010\u000bR\u001d\u0010\u0012\u001a\u00020\r8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011¨\u0006\u0016"}, d2 = {"Lb/a/a/a/c;", "Lcom/discord/app/AppDialog;", "", "onViewBoundOrOnResume", "()V", "Landroid/content/DialogInterface;", "dialog", "onDismiss", "(Landroid/content/DialogInterface;)V", "Lkotlin/Function0;", "n", "Lkotlin/jvm/functions/Function0;", "dismissListener", "Lb/a/i/q;", "m", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "g", "()Lb/a/i/q;", "binding", HookHelper.constructorName, "l", "a", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class c extends AppDialog {
    public final FragmentViewBindingDelegate m = FragmentViewBindingDelegateKt.viewBinding$default(this, b.j, null, 2, null);
    public Function0<Unit> n;
    public static final /* synthetic */ KProperty[] j = {b.d.b.a.a.b0(c.class, "binding", "getBinding()Lcom/discord/databinding/GuildBoostActivatedDialogBinding;", 0)};
    public static final a l = new a(null);
    public static final IntRange k = new IntRange(540, 825);

    /* compiled from: GuildBoostActivatedDialog.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final void a(FragmentManager fragmentManager, Context context, String str, int i, boolean z2, Function0<Unit> function0) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(function0, "dismissListener");
            CharSequence b2 = z2 ? b.a.k.b.b(context, R.string.guild_subscription_purchase_modal_transferred_description_mobile1, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null) : b.a.k.b.b(context, R.string.guild_subscription_purchase_modal_activated_description_mobile1, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            CharSequence b3 = str == null ? b.a.k.b.b(context, R.string.guild_subscription_purchase_modal_activated_description_no_application, new Object[]{StringResourceUtilsKt.getI18nPluralString(context, R.plurals.guild_subscription_purchase_modal_activated_description_no_application_guildSubscriptionQuantity, i, Integer.valueOf(i))}, (r4 & 4) != 0 ? b.C0034b.j : null) : z2 ? b.a.k.b.b(context, R.string.guild_subscription_purchase_modal_transferred_description_mobile2, new Object[]{str, StringResourceUtilsKt.getI18nPluralString(context, R.plurals.guild_subscription_purchase_modal_transferred_description_mobile2_guildSubscriptionQuantity, i, Integer.valueOf(i))}, (r4 & 4) != 0 ? b.C0034b.j : null) : b.a.k.b.b(context, R.string.guild_subscription_purchase_modal_activated_description_mobile2, new Object[]{str, StringResourceUtilsKt.getI18nPluralString(context, R.plurals.guild_subscription_purchase_modal_activated_description_guildSubscriptionQuantity, i, Integer.valueOf(i))}, (r4 & 4) != 0 ? b.C0034b.j : null);
            c cVar = new c();
            cVar.n = function0;
            Bundle bundle = new Bundle();
            bundle.putCharSequence("extra_body1_text", b2);
            bundle.putCharSequence("extra_body2_text", b3);
            cVar.setArguments(bundle);
            cVar.show(fragmentManager, c.class.getName());
        }
    }

    /* compiled from: GuildBoostActivatedDialog.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class b extends k implements Function1<View, q> {
        public static final b j = new b();

        public b() {
            super(1, q.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/GuildBoostActivatedDialogBinding;", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public q invoke(View view) {
            View view2 = view;
            m.checkNotNullParameter(view2, "p1");
            int i = R.id.guild_boost_activated_body_1;
            TextView textView = (TextView) view2.findViewById(R.id.guild_boost_activated_body_1);
            if (textView != null) {
                i = R.id.guild_boost_activated_body_2;
                TextView textView2 = (TextView) view2.findViewById(R.id.guild_boost_activated_body_2);
                if (textView2 != null) {
                    i = R.id.guild_boost_activated_lottie;
                    LottieAnimationView lottieAnimationView = (LottieAnimationView) view2.findViewById(R.id.guild_boost_activated_lottie);
                    if (lottieAnimationView != null) {
                        i = R.id.guild_boost_activated_ok;
                        MaterialButton materialButton = (MaterialButton) view2.findViewById(R.id.guild_boost_activated_ok);
                        if (materialButton != null) {
                            return new q((LinearLayout) view2, textView, textView2, lottieAnimationView, materialButton);
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
        }
    }

    /* compiled from: GuildBoostActivatedDialog.kt */
    /* renamed from: b.a.a.a.c$c  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class View$OnClickListenerC0009c implements View.OnClickListener {
        public View$OnClickListenerC0009c() {
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            c.this.dismiss();
        }
    }

    public c() {
        super(R.layout.guild_boost_activated_dialog);
    }

    public final q g() {
        return (q) this.m.getValue((Fragment) this, j[0]);
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        m.checkNotNullParameter(dialogInterface, "dialog");
        Function0<Unit> function0 = this.n;
        if (function0 != null) {
            function0.invoke();
        }
        super.onDismiss(dialogInterface);
    }

    @Override // com.discord.app.AppDialog
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        TextView textView = g().f178b;
        m.checkNotNullExpressionValue(textView, "binding.guildBoostActivatedBody1");
        Bundle arguments = getArguments();
        CharSequence charSequence = null;
        textView.setText(arguments != null ? arguments.getCharSequence("extra_body1_text") : null);
        TextView textView2 = g().c;
        m.checkNotNullExpressionValue(textView2, "binding.guildBoostActivatedBody2");
        Bundle arguments2 = getArguments();
        if (arguments2 != null) {
            charSequence = arguments2.getCharSequence("extra_body2_text");
        }
        textView2.setText(charSequence);
        g().e.setOnClickListener(new View$OnClickListenerC0009c());
        LottieAnimationView lottieAnimationView = g().d;
        m.checkNotNullExpressionValue(lottieAnimationView, "binding.guildBoostActivatedLottie");
        LottieAnimationUtilsKt.loopFrom$default(lottieAnimationView, 180, k, false, 4, null);
    }
}
