package b.a.a.a;

import b.a.a.a.f;
import com.discord.models.domain.ModelAppliedGuildBoost;
import com.discord.models.domain.ModelGuildBoostSlot;
import com.discord.models.domain.ModelSubscription;
import com.discord.stores.StoreGuildBoost;
import com.discord.stores.StoreSubscriptions;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import java.util.Objects;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: GuildBoostCancelViewModel.kt */
/* loaded from: classes.dex */
public final class d extends o implements Function1<f.b, Unit> {
    public final /* synthetic */ f this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public d(f fVar) {
        super(1);
        this.this$0 = fVar;
    }

    @Override // kotlin.jvm.functions.Function1
    public Unit invoke(f.b bVar) {
        Object obj;
        ModelAppliedGuildBoost premiumGuildSubscription;
        f.b bVar2 = bVar;
        m.checkNotNullParameter(bVar2, "storeState");
        f fVar = this.this$0;
        Objects.requireNonNull(fVar);
        StoreGuildBoost.State state = bVar2.a;
        StoreSubscriptions.SubscriptionsState subscriptionsState = bVar2.f41b;
        if (!(fVar.getViewState() instanceof f.c.b) && !(fVar.getViewState() instanceof f.c.C0010c)) {
            if ((state instanceof StoreGuildBoost.State.Loading) || (subscriptionsState instanceof StoreSubscriptions.SubscriptionsState.Loading)) {
                obj = f.c.C0011f.f;
            } else if ((state instanceof StoreGuildBoost.State.Failure) || (subscriptionsState instanceof StoreSubscriptions.SubscriptionsState.Failure)) {
                obj = new f.c.C0010c(Integer.valueOf((int) R.string.premium_guild_subscription_cancel_error_mobile));
            } else if (!(state instanceof StoreGuildBoost.State.Loaded) || !(subscriptionsState instanceof StoreSubscriptions.SubscriptionsState.Loaded)) {
                obj = new f.c.C0010c(Integer.valueOf((int) R.string.premium_guild_subscription_cancel_error_mobile));
            } else {
                ModelSubscription modelSubscription = (ModelSubscription) u.firstOrNull((List<? extends Object>) ((StoreSubscriptions.SubscriptionsState.Loaded) subscriptionsState).getSubscriptions());
                if (modelSubscription == null) {
                    obj = new f.c.C0010c(Integer.valueOf((int) R.string.premium_guild_subscription_cancel_error_mobile));
                } else if (fVar.getViewState() instanceof f.c.b) {
                    obj = new f.c.b(modelSubscription);
                } else {
                    ModelGuildBoostSlot modelGuildBoostSlot = ((StoreGuildBoost.State.Loaded) state).getBoostSlotMap().get(Long.valueOf(fVar.k));
                    obj = new f.c.e(modelSubscription, ((modelGuildBoostSlot == null || (premiumGuildSubscription = modelGuildBoostSlot.getPremiumGuildSubscription()) == null) ? null : Long.valueOf(premiumGuildSubscription.getGuildId())) != null);
                }
            }
            fVar.updateViewState(obj);
        }
        return Unit.a;
    }
}
