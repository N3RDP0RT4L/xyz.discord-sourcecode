package b.a.a.a;

import andhook.lib.HookHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.a.a.k;
import b.a.d.f0;
import b.a.d.h0;
import b.a.i.s;
import com.discord.app.AppDialog;
import com.discord.app.AppViewFlipper;
import com.discord.app.AppViewModel;
import com.discord.utilities.premium.GuildBoostUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.LoadingButton;
import com.google.android.material.button.MaterialButton;
import d0.g;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: GuildBoostUncancelDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \u001c2\u00020\u0001:\u0001\u001dB\u0007¢\u0006\u0004\b\u001b\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\bR!\u0010\u000f\u001a\u00060\tj\u0002`\n8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000eR\u001d\u0010\u0014\u001a\u00020\u00108B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0011\u0010\f\u001a\u0004\b\u0012\u0010\u0013R\u001d\u0010\u001a\u001a\u00020\u00158B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019¨\u0006\u001e"}, d2 = {"Lb/a/a/a/a;", "Lcom/discord/app/AppDialog;", "Landroid/view/View;", "view", "", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "", "Lcom/discord/primitives/GuildBoostSlotId;", "n", "Lkotlin/Lazy;", "getSlotId", "()J", "slotId", "Lb/a/a/a/k;", "m", "getViewModel", "()Lb/a/a/a/k;", "viewModel", "Lb/a/i/s;", "l", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "g", "()Lb/a/i/s;", "binding", HookHelper.constructorName, "k", "b", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class a extends AppDialog {
    public static final /* synthetic */ KProperty[] j = {b.d.b.a.a.b0(a.class, "binding", "getBinding()Lcom/discord/databinding/GuildBoostUncancelDialogBinding;", 0)};
    public static final b k = new b(null);
    public final Lazy m;
    public final FragmentViewBindingDelegate l = FragmentViewBindingDelegateKt.viewBinding$default(this, c.j, null, 2, null);
    public final Lazy n = g.lazy(new e());

    /* compiled from: java-style lambda group */
    /* renamed from: b.a.a.a.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class View$OnClickListenerC0007a implements View.OnClickListener {
        public final /* synthetic */ int j;
        public final /* synthetic */ Object k;

        public View$OnClickListenerC0007a(int i, Object obj) {
            this.j = i;
            this.k = obj;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            int i = this.j;
            if (i == 0) {
                k kVar = (k) ((a) this.k).m.getValue();
                k.c viewState = kVar.getViewState();
                if (!m.areEqual(viewState, k.c.C0012c.a)) {
                    if ((viewState instanceof k.c.b) || (viewState instanceof k.c.a)) {
                        kVar.updateViewState(new k.c.a(null, 1));
                    }
                }
            } else if (i == 1) {
                k kVar2 = (k) ((a) this.k).m.getValue();
                k.c viewState2 = kVar2.getViewState();
                if (!m.areEqual(viewState2, k.c.C0012c.a)) {
                    if (viewState2 instanceof k.c.b) {
                        k.c viewState3 = kVar2.getViewState();
                        Objects.requireNonNull(viewState3, "null cannot be cast to non-null type com.discord.dialogs.guildboost.GuildBoostUncancelViewModel.ViewState.Loaded");
                        if (!((k.c.b) viewState3).f43b) {
                            k.c viewState4 = kVar2.getViewState();
                            if (!(viewState4 instanceof k.c.b)) {
                                viewState4 = null;
                            }
                            k.c.b bVar = (k.c.b) viewState4;
                            if (bVar != null) {
                                kVar2.updateViewState(k.c.b.a(bVar, null, true, false, 5));
                                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(GuildBoostUtils.INSTANCE.uncancelGuildBoostSlot(kVar2.n, kVar2.k, bVar.a, kVar2.m), kVar2, null, 2, null), k.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new l(kVar2, bVar));
                            }
                        }
                    } else if ((viewState2 instanceof k.c.d) || (viewState2 instanceof k.c.a)) {
                        kVar2.updateViewState(new k.c.a(null, 1));
                    }
                }
            } else {
                throw null;
            }
        }
    }

    /* compiled from: GuildBoostUncancelDialog.kt */
    /* loaded from: classes.dex */
    public static final class b {
        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: GuildBoostUncancelDialog.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class c extends d0.z.d.k implements Function1<View, s> {
        public static final c j = new c();

        public c() {
            super(1, s.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/GuildBoostUncancelDialogBinding;", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public s invoke(View view) {
            View view2 = view;
            m.checkNotNullParameter(view2, "p1");
            int i = R.id.guild_boost_uncancel_body;
            TextView textView = (TextView) view2.findViewById(R.id.guild_boost_uncancel_body);
            if (textView != null) {
                i = R.id.guild_boost_uncancel_confirm;
                LoadingButton loadingButton = (LoadingButton) view2.findViewById(R.id.guild_boost_uncancel_confirm);
                if (loadingButton != null) {
                    i = R.id.guild_boost_uncancel_error;
                    TextView textView2 = (TextView) view2.findViewById(R.id.guild_boost_uncancel_error);
                    if (textView2 != null) {
                        i = R.id.guild_boost_uncancel_flipper;
                        AppViewFlipper appViewFlipper = (AppViewFlipper) view2.findViewById(R.id.guild_boost_uncancel_flipper);
                        if (appViewFlipper != null) {
                            i = R.id.guild_boost_uncancel_header;
                            TextView textView3 = (TextView) view2.findViewById(R.id.guild_boost_uncancel_header);
                            if (textView3 != null) {
                                i = R.id.guild_boost_uncancel_image;
                                ImageView imageView = (ImageView) view2.findViewById(R.id.guild_boost_uncancel_image);
                                if (imageView != null) {
                                    i = R.id.guild_boost_uncancel_nevermind;
                                    MaterialButton materialButton = (MaterialButton) view2.findViewById(R.id.guild_boost_uncancel_nevermind);
                                    if (materialButton != null) {
                                        i = R.id.notice_header_container;
                                        LinearLayout linearLayout = (LinearLayout) view2.findViewById(R.id.notice_header_container);
                                        if (linearLayout != null) {
                                            return new s((LinearLayout) view2, textView, loadingButton, textView2, appViewFlipper, textView3, imageView, materialButton, linearLayout);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
        }
    }

    /* compiled from: GuildBoostUncancelDialog.kt */
    /* loaded from: classes.dex */
    public static final class d extends o implements Function1<k.c, Unit> {
        public d() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(k.c cVar) {
            k.c cVar2 = cVar;
            a aVar = a.this;
            m.checkNotNullExpressionValue(cVar2, "it");
            KProperty[] kPropertyArr = a.j;
            AppViewFlipper appViewFlipper = aVar.g().e;
            m.checkNotNullExpressionValue(appViewFlipper, "binding.guildBoostUncancelFlipper");
            boolean z2 = true;
            appViewFlipper.setDisplayedChild(!(cVar2 instanceof k.c.C0012c) ? 1 : 0);
            TextView textView = aVar.g().d;
            m.checkNotNullExpressionValue(textView, "binding.guildBoostUncancelError");
            boolean z3 = cVar2 instanceof k.c.b;
            int i = 8;
            textView.setVisibility(z3 && ((k.c.b) cVar2).c ? 0 : 8);
            MaterialButton materialButton = aVar.g().h;
            m.checkNotNullExpressionValue(materialButton, "binding.guildBoostUncancelNevermind");
            if (z3) {
                i = 0;
            }
            materialButton.setVisibility(i);
            MaterialButton materialButton2 = aVar.g().h;
            m.checkNotNullExpressionValue(materialButton2, "binding.guildBoostUncancelNevermind");
            ViewExtensions.setEnabledAndAlpha$default(materialButton2, z3 && !((k.c.b) cVar2).f43b, 0.0f, 2, null);
            aVar.g().c.setIsLoading(z3 && ((k.c.b) cVar2).f43b);
            k.c.C0012c cVar3 = k.c.C0012c.a;
            if (!(!m.areEqual(cVar2, cVar3)) || !z3 || ((k.c.b) cVar2).f43b) {
                z2 = false;
            }
            aVar.setCancelable(z2);
            if (!m.areEqual(cVar2, cVar3)) {
                if (z3) {
                    TextView textView2 = aVar.g().f;
                    m.checkNotNullExpressionValue(textView2, "binding.guildBoostUncancelHeader");
                    textView2.setText(aVar.getString(R.string.premium_guild_subscription_inventory_uncancel_title_mobile));
                    TextView textView3 = aVar.g().f190b;
                    m.checkNotNullExpressionValue(textView3, "binding.guildBoostUncancelBody");
                    textView3.setText(aVar.getString(R.string.premium_guild_subscription_inventory_uncancel_description));
                } else if (m.areEqual(cVar2, k.c.d.a)) {
                    TextView textView4 = aVar.g().f;
                    m.checkNotNullExpressionValue(textView4, "binding.guildBoostUncancelHeader");
                    textView4.setText(aVar.getString(R.string.premium_guild_subscription_inventory_uncancel_confirm_title));
                    ImageView imageView = aVar.g().g;
                    m.checkNotNullExpressionValue(imageView, "binding.guildBoostUncancelImage");
                    imageView.setVisibility(0);
                    TextView textView5 = aVar.g().f190b;
                    m.checkNotNullExpressionValue(textView5, "binding.guildBoostUncancelBody");
                    textView5.setText(aVar.getString(R.string.premium_guild_subscription_inventory_uncancel_confirm_description));
                    aVar.g().c.setText(aVar.getString(R.string.okay));
                } else if (cVar2 instanceof k.c.a) {
                    Integer num = ((k.c.a) cVar2).a;
                    if (num != null) {
                        b.a.d.m.i(aVar, num.intValue(), 0, 4);
                    }
                    aVar.dismiss();
                }
            }
            return Unit.a;
        }
    }

    /* compiled from: GuildBoostUncancelDialog.kt */
    /* loaded from: classes.dex */
    public static final class e extends o implements Function0<Long> {
        public e() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public Long invoke() {
            Bundle arguments = a.this.getArguments();
            Long valueOf = arguments != null ? Long.valueOf(arguments.getLong("extra_slot_id")) : null;
            Objects.requireNonNull(valueOf, "null cannot be cast to non-null type com.discord.primitives.GuildBoostSlotId /* = kotlin.Long */");
            return Long.valueOf(valueOf.longValue());
        }
    }

    /* compiled from: GuildBoostUncancelDialog.kt */
    /* loaded from: classes.dex */
    public static final class f extends o implements Function0<AppViewModel<k.c>> {
        public f() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public AppViewModel<k.c> invoke() {
            return new k(((Number) a.this.n.getValue()).longValue(), null, null, null, null, 30);
        }
    }

    public a() {
        super(R.layout.guild_boost_uncancel_dialog);
        f fVar = new f();
        f0 f0Var = new f0(this);
        this.m = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(k.class), new k(3, f0Var), new h0(fVar));
    }

    public final s g() {
        return (s) this.l.getValue((Fragment) this, j[0]);
    }

    @Override // com.discord.app.AppDialog
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        g().h.setOnClickListener(new View$OnClickListenerC0007a(0, this));
        g().c.setOnClickListener(new View$OnClickListenerC0007a(1, this));
    }

    @Override // com.discord.app.AppDialog
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable<k.c> q = ((k) this.m.getValue()).observeViewState().q();
        m.checkNotNullExpressionValue(q, "viewModel\n        .obser…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(q, this, null, 2, null), a.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new d());
    }
}
