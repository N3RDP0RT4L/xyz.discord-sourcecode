package b.a.a.a;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.a.a.f;
import b.a.d.f0;
import b.a.d.h0;
import b.a.i.r;
import b.a.k.b;
import com.discord.app.AppDialog;
import com.discord.app.AppViewFlipper;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelSubscription;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.premium.GuildBoostUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.LoadingButton;
import com.google.android.material.button.MaterialButton;
import d0.g;
import d0.z.d.a0;
import d0.z.d.k;
import d0.z.d.m;
import d0.z.d.o;
import java.text.DateFormat;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: GuildBoostCancelDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \u001c2\u00020\u0001:\u0001\u001dB\u0007¢\u0006\u0004\b\u001b\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\bR\u001d\u0010\u000e\u001a\u00020\t8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\f\u0010\rR\u001d\u0010\u0014\u001a\u00020\u000f8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R!\u0010\u001a\u001a\u00060\u0015j\u0002`\u00168B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0017\u0010\u000b\u001a\u0004\b\u0018\u0010\u0019¨\u0006\u001e"}, d2 = {"Lb/a/a/a/b;", "Lcom/discord/app/AppDialog;", "Landroid/view/View;", "view", "", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Lb/a/a/a/f;", "m", "Lkotlin/Lazy;", "getViewModel", "()Lb/a/a/a/f;", "viewModel", "Lb/a/i/r;", "l", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "g", "()Lb/a/i/r;", "binding", "", "Lcom/discord/primitives/GuildBoostSlotId;", "n", "getSlotId", "()J", "slotId", HookHelper.constructorName, "k", "b", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class b extends AppDialog {
    public static final /* synthetic */ KProperty[] j = {b.d.b.a.a.b0(b.class, "binding", "getBinding()Lcom/discord/databinding/GuildBoostCancelDialogBinding;", 0)};
    public static final C0008b k = new C0008b(null);
    public final Lazy m;
    public final FragmentViewBindingDelegate l = FragmentViewBindingDelegateKt.viewBinding$default(this, c.j, null, 2, null);
    public final Lazy n = g.lazy(new e());

    /* compiled from: java-style lambda group */
    /* loaded from: classes2.dex */
    public static final class a implements View.OnClickListener {
        public final /* synthetic */ int j;
        public final /* synthetic */ Object k;

        public a(int i, Object obj) {
            this.j = i;
            this.k = obj;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            ModelSubscription modelSubscription;
            int i = this.j;
            if (i == 0) {
                b.a.a.a.f fVar = (b.a.a.a.f) ((b) this.k).m.getValue();
                f.c viewState = fVar.getViewState();
                if ((viewState instanceof f.c.C0011f) || (viewState instanceof f.c.a)) {
                    return;
                }
                if ((viewState instanceof f.c.e) || (viewState instanceof f.c.d) || (viewState instanceof f.c.b)) {
                    fVar.updateViewState(new f.c.C0010c(null, 1));
                }
            } else if (i == 1) {
                b.a.a.a.f fVar2 = (b.a.a.a.f) ((b) this.k).m.getValue();
                f.c viewState2 = fVar2.getViewState();
                if ((viewState2 instanceof f.c.C0011f) || (viewState2 instanceof f.c.a)) {
                    return;
                }
                if ((viewState2 instanceof f.c.e) || (viewState2 instanceof f.c.d)) {
                    f.c viewState3 = fVar2.getViewState();
                    if (viewState3 instanceof f.c.e) {
                        modelSubscription = ((f.c.e) viewState3).f;
                    } else {
                        modelSubscription = viewState3 instanceof f.c.d ? ((f.c.d) viewState3).f : null;
                    }
                    if (modelSubscription != null) {
                        fVar2.updateViewState(new f.c.a(modelSubscription));
                        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(GuildBoostUtils.INSTANCE.cancelGuildBoostSlot(fVar2.n, fVar2.k, modelSubscription, fVar2.l), fVar2, null, 2, null), b.a.a.a.f.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new g(fVar2, modelSubscription));
                    }
                } else if (viewState2 instanceof f.c.b) {
                    fVar2.updateViewState(new f.c.C0010c(null, 1));
                }
            } else {
                throw null;
            }
        }
    }

    /* compiled from: GuildBoostCancelDialog.kt */
    /* renamed from: b.a.a.a.b$b  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0008b {
        public C0008b(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: GuildBoostCancelDialog.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class c extends k implements Function1<View, r> {
        public static final c j = new c();

        public c() {
            super(1, r.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/GuildBoostCancelDialogBinding;", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public r invoke(View view) {
            View view2 = view;
            m.checkNotNullParameter(view2, "p1");
            int i = R.id.active_subscription_progress;
            ProgressBar progressBar = (ProgressBar) view2.findViewById(R.id.active_subscription_progress);
            if (progressBar != null) {
                i = R.id.guild_boost_cancel_body;
                TextView textView = (TextView) view2.findViewById(R.id.guild_boost_cancel_body);
                if (textView != null) {
                    i = R.id.guild_boost_cancel_confirm;
                    LoadingButton loadingButton = (LoadingButton) view2.findViewById(R.id.guild_boost_cancel_confirm);
                    if (loadingButton != null) {
                        i = R.id.guild_boost_cancel_error;
                        TextView textView2 = (TextView) view2.findViewById(R.id.guild_boost_cancel_error);
                        if (textView2 != null) {
                            i = R.id.guild_boost_cancel_flipper;
                            AppViewFlipper appViewFlipper = (AppViewFlipper) view2.findViewById(R.id.guild_boost_cancel_flipper);
                            if (appViewFlipper != null) {
                                i = R.id.guild_boost_cancel_header;
                                TextView textView3 = (TextView) view2.findViewById(R.id.guild_boost_cancel_header);
                                if (textView3 != null) {
                                    i = R.id.guild_boost_cancel_nevermind;
                                    MaterialButton materialButton = (MaterialButton) view2.findViewById(R.id.guild_boost_cancel_nevermind);
                                    if (materialButton != null) {
                                        i = R.id.notice_header_container;
                                        LinearLayout linearLayout = (LinearLayout) view2.findViewById(R.id.notice_header_container);
                                        if (linearLayout != null) {
                                            return new r((LinearLayout) view2, progressBar, textView, loadingButton, textView2, appViewFlipper, textView3, materialButton, linearLayout);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
        }
    }

    /* compiled from: GuildBoostCancelDialog.kt */
    /* loaded from: classes.dex */
    public static final class d extends o implements Function1<f.c, Unit> {
        public d() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(f.c cVar) {
            f.c cVar2 = cVar;
            b bVar = b.this;
            m.checkNotNullExpressionValue(cVar2, "it");
            KProperty[] kPropertyArr = b.j;
            TextView textView = bVar.g().d;
            m.checkNotNullExpressionValue(textView, "binding.guildBoostCancelError");
            int i = 8;
            textView.setVisibility(cVar2.a ? 0 : 8);
            MaterialButton materialButton = bVar.g().g;
            m.checkNotNullExpressionValue(materialButton, "binding.guildBoostCancelNevermind");
            if (cVar2.f42b) {
                i = 0;
            }
            materialButton.setVisibility(i);
            MaterialButton materialButton2 = bVar.g().g;
            m.checkNotNullExpressionValue(materialButton2, "binding.guildBoostCancelNevermind");
            ViewExtensions.setEnabledAndAlpha$default(materialButton2, cVar2.c, 0.0f, 2, null);
            bVar.setCancelable(cVar2.e);
            bVar.g().c.setIsLoading(cVar2.d);
            AppViewFlipper appViewFlipper = bVar.g().e;
            m.checkNotNullExpressionValue(appViewFlipper, "binding.guildBoostCancelFlipper");
            appViewFlipper.setDisplayedChild(!(cVar2 instanceof f.c.C0011f) ? 1 : 0);
            if (cVar2 instanceof f.c.e) {
                TimeUtils timeUtils = TimeUtils.INSTANCE;
                f.c.e eVar = (f.c.e) cVar2;
                String currentPeriodEnd = eVar.f.getCurrentPeriodEnd();
                Context requireContext = bVar.requireContext();
                m.checkNotNullExpressionValue(requireContext, "requireContext()");
                String renderUtcDate$default = TimeUtils.renderUtcDate$default(timeUtils, currentPeriodEnd, requireContext, (String) null, (DateFormat) null, 0, 28, (Object) null);
                if (eVar.g) {
                    TextView textView2 = bVar.g().f184b;
                    m.checkNotNullExpressionValue(textView2, "binding.guildBoostCancelBody");
                    b.a.k.b.m(textView2, R.string.premium_guild_subscription_cancel_body_inventory, new Object[]{renderUtcDate$default}, (r4 & 4) != 0 ? b.g.j : null);
                } else {
                    TextView textView3 = bVar.g().f184b;
                    m.checkNotNullExpressionValue(textView3, "binding.guildBoostCancelBody");
                    b.a.k.b.m(textView3, R.string.premium_guild_subscription_cancel_body_guild, new Object[]{renderUtcDate$default}, (r4 & 4) != 0 ? b.g.j : null);
                }
                bVar.g().c.setBackgroundColor(ColorCompat.getColor(bVar, (int) R.color.status_red_500));
            } else if (cVar2 instanceof f.c.b) {
                TextView textView4 = bVar.g().f;
                m.checkNotNullExpressionValue(textView4, "binding.guildBoostCancelHeader");
                textView4.setText(bVar.getString(R.string.premium_guild_subscription_cancel_title_pending_cancellation));
                TextView textView5 = bVar.g().f184b;
                m.checkNotNullExpressionValue(textView5, "binding.guildBoostCancelBody");
                TimeUtils timeUtils2 = TimeUtils.INSTANCE;
                String currentPeriodEnd2 = ((f.c.b) cVar2).f.getCurrentPeriodEnd();
                Context requireContext2 = bVar.requireContext();
                m.checkNotNullExpressionValue(requireContext2, "requireContext()");
                b.a.k.b.m(textView5, R.string.premium_guild_subscription_confirm_body, new Object[]{TimeUtils.renderUtcDate$default(timeUtils2, currentPeriodEnd2, requireContext2, (String) null, (DateFormat) null, 0, 28, (Object) null)}, (r4 & 4) != 0 ? b.g.j : null);
                bVar.g().c.setText(bVar.getString(R.string.okay));
                bVar.g().c.setBackgroundColor(ColorCompat.getThemedColor(bVar, (int) R.attr.color_brand_500));
            } else if (cVar2 instanceof f.c.C0010c) {
                Integer num = ((f.c.C0010c) cVar2).f;
                if (num != null) {
                    b.a.d.m.g(bVar.requireContext(), num.intValue(), 0, null, 12);
                }
                bVar.dismiss();
            }
            return Unit.a;
        }
    }

    /* compiled from: GuildBoostCancelDialog.kt */
    /* loaded from: classes.dex */
    public static final class e extends o implements Function0<Long> {
        public e() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public Long invoke() {
            Bundle arguments = b.this.getArguments();
            Long valueOf = arguments != null ? Long.valueOf(arguments.getLong("extra_slot_id")) : null;
            Objects.requireNonNull(valueOf, "null cannot be cast to non-null type com.discord.primitives.GuildBoostSlotId /* = kotlin.Long */");
            return Long.valueOf(valueOf.longValue());
        }
    }

    /* compiled from: GuildBoostCancelDialog.kt */
    /* loaded from: classes.dex */
    public static final class f extends o implements Function0<AppViewModel<f.c>> {
        public f() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public AppViewModel<f.c> invoke() {
            return new b.a.a.a.f(((Number) b.this.n.getValue()).longValue(), null, null, null, null, 30);
        }
    }

    public b() {
        super(R.layout.guild_boost_cancel_dialog);
        f fVar = new f();
        f0 f0Var = new f0(this);
        this.m = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(b.a.a.a.f.class), new k(2, f0Var), new h0(fVar));
    }

    public final r g() {
        return (r) this.l.getValue((Fragment) this, j[0]);
    }

    @Override // com.discord.app.AppDialog
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        g().g.setOnClickListener(new a(0, this));
        g().c.setOnClickListener(new a(1, this));
    }

    @Override // com.discord.app.AppDialog
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable<f.c> q = ((b.a.a.a.f) this.m.getValue()).observeViewState().q();
        m.checkNotNullExpressionValue(q, "viewModel\n        .obser…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(q, this, null, 2, null), b.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new d());
    }
}
