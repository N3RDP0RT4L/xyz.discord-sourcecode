package b.a.a.a;

import b.a.a.a.k;
import com.discord.models.domain.ModelSubscription;
import com.discord.stores.StoreSubscriptions;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: GuildBoostUncancelViewModel.kt */
/* loaded from: classes.dex */
public final class i extends o implements Function1<k.b, Unit> {
    public final /* synthetic */ k this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public i(k kVar) {
        super(1);
        this.this$0 = kVar;
    }

    @Override // kotlin.jvm.functions.Function1
    public Unit invoke(k.b bVar) {
        Object obj;
        k.b bVar2 = bVar;
        m.checkNotNullParameter(bVar2, "storeState");
        k kVar = this.this$0;
        if (!(kVar.getViewState() instanceof k.c.d) && !(kVar.getViewState() instanceof k.c.a)) {
            StoreSubscriptions.SubscriptionsState subscriptionsState = bVar2.a;
            if (subscriptionsState instanceof StoreSubscriptions.SubscriptionsState.Loading) {
                obj = k.c.C0012c.a;
            } else if (subscriptionsState instanceof StoreSubscriptions.SubscriptionsState.Failure) {
                obj = new k.c.a(Integer.valueOf((int) R.string.premium_guild_subscription_cancel_error_mobile));
            } else if (subscriptionsState instanceof StoreSubscriptions.SubscriptionsState.Loaded) {
                ModelSubscription modelSubscription = (ModelSubscription) u.firstOrNull((List<? extends Object>) ((StoreSubscriptions.SubscriptionsState.Loaded) subscriptionsState).getSubscriptions());
                if (modelSubscription == null) {
                    obj = new k.c.a(Integer.valueOf((int) R.string.premium_guild_subscription_cancel_error_mobile));
                } else {
                    obj = new k.c.b(modelSubscription, false, false);
                }
            } else {
                obj = new k.c.a(null, 1);
            }
            kVar.updateViewState(obj);
        }
        return Unit.a;
    }
}
