package b.a.a.a;

import com.discord.models.domain.ModelSubscription;
import com.discord.utilities.premium.GuildBoostUtils;
import d0.z.d.o;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: GuildBoostCancelViewModel.kt */
/* loaded from: classes.dex */
public final class g extends o implements Function1<GuildBoostUtils.ModifyGuildBoostSlotResult, Unit> {
    public final /* synthetic */ ModelSubscription $subscription;
    public final /* synthetic */ f this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public g(f fVar, ModelSubscription modelSubscription) {
        super(1);
        this.this$0 = fVar;
        this.$subscription = modelSubscription;
    }

    /* JADX WARN: Code restructure failed: missing block: B:7:0x0012, code lost:
        if (r3 != 2) goto L10;
     */
    @Override // kotlin.jvm.functions.Function1
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public kotlin.Unit invoke(com.discord.utilities.premium.GuildBoostUtils.ModifyGuildBoostSlotResult r3) {
        /*
            r2 = this;
            com.discord.utilities.premium.GuildBoostUtils$ModifyGuildBoostSlotResult r3 = (com.discord.utilities.premium.GuildBoostUtils.ModifyGuildBoostSlotResult) r3
            java.lang.String r0 = "result"
            d0.z.d.m.checkNotNullParameter(r3, r0)
            int r3 = r3.ordinal()
            if (r3 == 0) goto L22
            r0 = 1
            if (r3 == r0) goto L15
            r0 = 2
            if (r3 == r0) goto L22
            goto L2e
        L15:
            b.a.a.a.f r3 = r2.this$0
            b.a.a.a.f$c$d r0 = new b.a.a.a.f$c$d
            com.discord.models.domain.ModelSubscription r1 = r2.$subscription
            r0.<init>(r1)
            r3.updateViewState(r0)
            goto L2e
        L22:
            b.a.a.a.f r3 = r2.this$0
            b.a.a.a.f$c$b r0 = new b.a.a.a.f$c$b
            com.discord.models.domain.ModelSubscription r1 = r2.$subscription
            r0.<init>(r1)
            r3.updateViewState(r0)
        L2e:
            kotlin.Unit r3 = kotlin.Unit.a
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.a.a.g.invoke(java.lang.Object):java.lang.Object");
    }
}
