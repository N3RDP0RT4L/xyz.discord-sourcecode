package b.a.a;

import android.content.Context;
import android.view.View;
import b.a.a.w;
import com.discord.restapi.RestAPIParams;
import com.discord.utilities.channel.ChannelSelector;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.user.UserUtils;
import d0.z.d.m;
import java.util.Objects;
import kotlin.reflect.KProperty;
/* compiled from: WidgetUrgentMessageDialog.kt */
/* loaded from: classes.dex */
public final class t implements View.OnClickListener {
    public final /* synthetic */ s j;

    public t(s sVar) {
        this.j = sVar;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        Integer num;
        s sVar = this.j;
        KProperty[] kPropertyArr = s.j;
        w h = sVar.h();
        m.checkNotNullExpressionValue(view, "button");
        Context context = view.getContext();
        m.checkNotNullExpressionValue(context, "button.context");
        Objects.requireNonNull(h);
        m.checkNotNullParameter(context, "context");
        ChannelSelector.Companion.getInstance().findAndSetDirectMessage(context, UserUtils.SYSTEM_USER_ID);
        w.d viewState = h.getViewState();
        if (viewState != null && (num = viewState.a) != null) {
            RestAPIParams.UserInfo userInfo = new RestAPIParams.UserInfo(null, null, null, null, null, null, null, null, null, Integer.valueOf(num.intValue() & (-8193)), null, 1535, null);
            h.updateViewState(new w.d(viewState.a, true));
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(h.l.patchUser(userInfo), false, 1, null), h, null, 2, null), w.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new y(h), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new x(h));
        }
    }
}
