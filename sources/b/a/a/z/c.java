package b.a.a.z;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import b.a.i.i4;
import b.a.k.b;
import com.discord.api.application.Application;
import com.discord.api.premium.SubscriptionPlan;
import com.discord.app.AppDialog;
import com.discord.app.AppViewFlipper;
import com.discord.models.domain.ModelGift;
import com.discord.models.domain.ModelSku;
import com.discord.models.domain.ModelStoreListing;
import com.discord.rlottie.RLottieImageView;
import com.discord.stores.StoreGifting;
import com.discord.stores.StoreNotices;
import com.discord.stores.StoreStream;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.error.Error;
import com.discord.utilities.gifting.GiftStyle;
import com.discord.utilities.gifting.GiftStyleKt;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.premium.PremiumUtils;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.utilities.views.ViewCoroutineScopeKt;
import com.discord.widgets.home.WidgetHome;
import com.discord.widgets.settings.premium.WidgetSettingsGifting;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.button.MaterialButton;
import d0.l;
import d0.t.n;
import d0.z.d.a0;
import d0.z.d.k;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import kotlinx.coroutines.CoroutineScope;
import rx.Observable;
import rx.functions.Func2;
import xyz.discord.R;
/* compiled from: WidgetGiftAcceptDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \u00192\u00020\u0001:\u0001\u001aB\u0007¢\u0006\u0004\b\u0018\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u001f\u0010\r\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u0010\u0010\u0011R\u001d\u0010\u0017\u001a\u00020\u00128B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016¨\u0006\u001b"}, d2 = {"Lb/a/a/z/c;", "Lcom/discord/app/AppDialog;", "Landroid/view/View;", "view", "", "onViewBound", "(Landroid/view/View;)V", "onDestroy", "()V", "Lcom/discord/models/domain/ModelGift;", "gift", "Lb/a/a/z/c$a$a;", "uiState", "g", "(Lcom/discord/models/domain/ModelGift;Lb/a/a/z/c$a$a;)V", "", "i", "(Lcom/discord/models/domain/ModelGift;)Ljava/lang/CharSequence;", "Lb/a/i/i4;", "l", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "h", "()Lb/a/i/i4;", "binding", HookHelper.constructorName, "k", "a", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class c extends AppDialog {
    public static final /* synthetic */ KProperty[] j = {b.d.b.a.a.b0(c.class, "binding", "getBinding()Lcom/discord/databinding/WidgetAcceptGiftDialogBinding;", 0)};
    public static final a k = new a(null);
    public final FragmentViewBindingDelegate l = FragmentViewBindingDelegateKt.viewBinding$default(this, b.j, null, 2, null);

    /* compiled from: WidgetGiftAcceptDialog.kt */
    /* loaded from: classes.dex */
    public static final class a {

        /* compiled from: WidgetGiftAcceptDialog.kt */
        /* renamed from: b.a.a.z.c$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C0026a {
            public final StoreGifting.GiftState a;

            /* renamed from: b  reason: collision with root package name */
            public final boolean f50b;

            public C0026a(StoreGifting.GiftState giftState, boolean z2) {
                m.checkNotNullParameter(giftState, "giftState");
                this.a = giftState;
                this.f50b = z2;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof C0026a)) {
                    return false;
                }
                C0026a aVar = (C0026a) obj;
                return m.areEqual(this.a, aVar.a) && this.f50b == aVar.f50b;
            }

            public int hashCode() {
                StoreGifting.GiftState giftState = this.a;
                int hashCode = (giftState != null ? giftState.hashCode() : 0) * 31;
                boolean z2 = this.f50b;
                if (z2) {
                    z2 = true;
                }
                int i = z2 ? 1 : 0;
                int i2 = z2 ? 1 : 0;
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = b.d.b.a.a.R("GiftUiState(giftState=");
                R.append(this.a);
                R.append(", reduceMotion=");
                return b.d.b.a.a.M(R, this.f50b, ")");
            }
        }

        /* compiled from: WidgetGiftAcceptDialog.kt */
        /* loaded from: classes.dex */
        public static final class b extends o implements Function1<FragmentActivity, Boolean> {
            public final /* synthetic */ long $channelId;
            public final /* synthetic */ String $giftCode;
            public final /* synthetic */ String $source;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public b(String str, String str2, long j) {
                super(1);
                this.$giftCode = str;
                this.$source = str2;
                this.$channelId = j;
            }

            @Override // kotlin.jvm.functions.Function1
            public Boolean invoke(FragmentActivity fragmentActivity) {
                FragmentActivity fragmentActivity2 = fragmentActivity;
                m.checkNotNullParameter(fragmentActivity2, "appActivity");
                StoreStream.Companion.getAnalytics().trackOpenGiftAcceptModal(this.$giftCode, this.$source, this.$channelId);
                c cVar = new c();
                Bundle bundle = new Bundle();
                bundle.putString("ARG_GIFT_CODE", this.$giftCode);
                cVar.setArguments(bundle);
                FragmentManager supportFragmentManager = fragmentActivity2.getSupportFragmentManager();
                m.checkNotNullExpressionValue(supportFragmentManager, "appActivity.supportFragmentManager");
                cVar.show(supportFragmentManager, a0.getOrCreateKotlinClass(c.class).toString());
                return Boolean.TRUE;
            }
        }

        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final void a(String str, String str2, long j) {
            m.checkNotNullParameter(str, "giftCode");
            m.checkNotNullParameter(str2, "source");
            m.checkNotNullParameter(str, "giftCode");
            StoreStream.Companion.getNotices().requestToShow(new StoreNotices.Notice("gift:" + str, null, 0L, 0, false, n.listOf((Object[]) new d0.e0.c[]{a0.getOrCreateKotlinClass(WidgetSettingsGifting.class), a0.getOrCreateKotlinClass(WidgetHome.class)}), 0L, false, 0L, new b(str, str2, j), 150, null));
        }
    }

    /* compiled from: WidgetGiftAcceptDialog.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class b extends k implements Function1<View, i4> {
        public static final b j = new b();

        public b() {
            super(1, i4.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetAcceptGiftDialogBinding;", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public i4 invoke(View view) {
            View view2 = view;
            m.checkNotNullParameter(view2, "p1");
            int i = R.id.accept_gift_body_container;
            LinearLayout linearLayout = (LinearLayout) view2.findViewById(R.id.accept_gift_body_container);
            if (linearLayout != null) {
                i = R.id.accept_gift_body_image;
                SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view2.findViewById(R.id.accept_gift_body_image);
                if (simpleDraweeView != null) {
                    i = R.id.accept_gift_body_lottie;
                    RLottieImageView rLottieImageView = (RLottieImageView) view2.findViewById(R.id.accept_gift_body_lottie);
                    if (rLottieImageView != null) {
                        i = R.id.accept_gift_body_text;
                        TextView textView = (TextView) view2.findViewById(R.id.accept_gift_body_text);
                        if (textView != null) {
                            i = R.id.accept_gift_confirm;
                            MaterialButton materialButton = (MaterialButton) view2.findViewById(R.id.accept_gift_confirm);
                            if (materialButton != null) {
                                i = R.id.accept_gift_disclaimer_container;
                                LinearLayout linearLayout2 = (LinearLayout) view2.findViewById(R.id.accept_gift_disclaimer_container);
                                if (linearLayout2 != null) {
                                    i = R.id.accept_gift_disclaimer_text;
                                    LinkifiedTextView linkifiedTextView = (LinkifiedTextView) view2.findViewById(R.id.accept_gift_disclaimer_text);
                                    if (linkifiedTextView != null) {
                                        i = R.id.accept_gift_flipper;
                                        AppViewFlipper appViewFlipper = (AppViewFlipper) view2.findViewById(R.id.accept_gift_flipper);
                                        if (appViewFlipper != null) {
                                            i = R.id.accept_gift_header;
                                            TextView textView2 = (TextView) view2.findViewById(R.id.accept_gift_header);
                                            if (textView2 != null) {
                                                i = R.id.accept_gift_progress;
                                                ProgressBar progressBar = (ProgressBar) view2.findViewById(R.id.accept_gift_progress);
                                                if (progressBar != null) {
                                                    return new i4((LinearLayout) view2, linearLayout, simpleDraweeView, rLottieImageView, textView, materialButton, linearLayout2, linkifiedTextView, appViewFlipper, textView2, progressBar);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
        }
    }

    /* compiled from: WidgetGiftAcceptDialog.kt */
    @d0.w.i.a.e(c = "com.discord.dialogs.gifting.WidgetGiftAcceptDialog$configureUI$1$1", f = "WidgetGiftAcceptDialog.kt", l = {117}, m = "invokeSuspend")
    /* renamed from: b.a.a.z.c$c  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0027c extends d0.w.i.a.k implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
        public final /* synthetic */ RLottieImageView $this_apply;
        public int label;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public C0027c(RLottieImageView rLottieImageView, Continuation continuation) {
            super(2, continuation);
            this.$this_apply = rLottieImageView;
        }

        @Override // d0.w.i.a.a
        public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
            m.checkNotNullParameter(continuation, "completion");
            return new C0027c(this.$this_apply, continuation);
        }

        @Override // kotlin.jvm.functions.Function2
        public final Object invoke(CoroutineScope coroutineScope, Continuation<? super Unit> continuation) {
            Continuation<? super Unit> continuation2 = continuation;
            m.checkNotNullParameter(continuation2, "completion");
            return new C0027c(this.$this_apply, continuation2).invokeSuspend(Unit.a);
        }

        @Override // d0.w.i.a.a
        public final Object invokeSuspend(Object obj) {
            Object coroutine_suspended = d0.w.h.c.getCOROUTINE_SUSPENDED();
            int i = this.label;
            if (i == 0) {
                l.throwOnFailure(obj);
                this.label = 1;
                if (b.i.a.f.e.o.f.P(200L, this) == coroutine_suspended) {
                    return coroutine_suspended;
                }
            } else if (i == 1) {
                l.throwOnFailure(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.$this_apply.b();
            return Unit.a;
        }
    }

    /* compiled from: WidgetGiftAcceptDialog.kt */
    /* loaded from: classes.dex */
    public static final class d<T1, T2, R> implements Func2<StoreGifting.GiftState, Boolean, a.C0026a> {
        public static final d j = new d();

        @Override // rx.functions.Func2
        public a.C0026a call(StoreGifting.GiftState giftState, Boolean bool) {
            StoreGifting.GiftState giftState2 = giftState;
            Boolean bool2 = bool;
            m.checkNotNullExpressionValue(giftState2, "giftState");
            m.checkNotNullExpressionValue(bool2, "reduceMotion");
            return new a.C0026a(giftState2, bool2.booleanValue());
        }
    }

    /* compiled from: WidgetGiftAcceptDialog.kt */
    /* loaded from: classes.dex */
    public static final class e extends o implements Function1<a.C0026a, Unit> {
        public e() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(a.C0026a aVar) {
            CharSequence e;
            ModelSku sku;
            ModelSku sku2;
            String str;
            ModelSku sku3;
            a.C0026a aVar2 = aVar;
            StoreGifting.GiftState giftState = aVar2.a;
            int i = 0;
            if (giftState instanceof StoreGifting.GiftState.Loading) {
                c cVar = c.this;
                KProperty[] kPropertyArr = c.j;
                AppViewFlipper appViewFlipper = cVar.h().h;
                m.checkNotNullExpressionValue(appViewFlipper, "binding.acceptGiftFlipper");
                appViewFlipper.setDisplayedChild(0);
            } else {
                CharSequence charSequence = null;
                int i2 = 8;
                if (giftState instanceof StoreGifting.GiftState.Resolved) {
                    StoreGifting.GiftState.Resolved resolved = (StoreGifting.GiftState.Resolved) giftState;
                    if (resolved.getGift().getRedeemed()) {
                        c cVar2 = c.this;
                        m.checkNotNullExpressionValue(aVar2, "giftUiState");
                        KProperty[] kPropertyArr2 = c.j;
                        Objects.requireNonNull(cVar2);
                        ModelGift gift = resolved.getGift();
                        cVar2.g(gift, aVar2);
                        ModelStoreListing storeListing = gift.getStoreListing();
                        if (storeListing == null || (sku3 = storeListing.getSku()) == null || (str = sku3.getName()) == null) {
                            str = "";
                        }
                        CharSequence i3 = cVar2.i(gift);
                        LinearLayout linearLayout = b.d.b.a.a.p0(b.d.b.a.a.p0(cVar2.h().i, "binding.acceptGiftHeader", gift.isAnyNitroGift() ? b.a.k.b.e(cVar2, R.string.gift_confirmation_header_success_nitro, new Object[]{str}, (r4 & 4) != 0 ? b.a.j : null) : b.a.k.b.e(cVar2, R.string.gift_confirmation_header_success, new Object[0], (r4 & 4) != 0 ? b.a.j : null), cVar2).d, "binding.acceptGiftBodyText", gift.isNitroClassicGift() ? b.a.k.b.e(cVar2, R.string.gift_confirmation_body_success_nitro_classic_mobile, new Object[]{i3}, (r4 & 4) != 0 ? b.a.j : null) : gift.isNitroGift() ? b.a.k.b.e(cVar2, R.string.gift_confirmation_body_success_nitro_mobile, new Object[]{i3}, (r4 & 4) != 0 ? b.a.j : null) : b.a.k.b.e(cVar2, R.string.gift_confirmation_body_success_mobile, new Object[]{str}, (r4 & 4) != 0 ? b.a.j : null), cVar2).f;
                        m.checkNotNullExpressionValue(linearLayout, "binding.acceptGiftDisclaimerContainer");
                        linearLayout.setVisibility(8);
                        MaterialButton materialButton = cVar2.h().e;
                        m.checkNotNullExpressionValue(materialButton, "binding.acceptGiftConfirm");
                        Context context = cVar2.getContext();
                        if (context != null) {
                            charSequence = context.getText(R.string.gift_confirmation_button_success_mobile);
                        }
                        materialButton.setText(charSequence);
                        cVar2.h().e.setOnClickListener(new b.a.a.z.d(cVar2));
                    } else if (resolved.getGift().getMaxUses() == resolved.getGift().getUses()) {
                        c cVar3 = c.this;
                        m.checkNotNullExpressionValue(aVar2, "giftUiState");
                        KProperty[] kPropertyArr3 = c.j;
                        Objects.requireNonNull(cVar3);
                        cVar3.g(resolved.getGift(), aVar2);
                        TextView textView = cVar3.h().i;
                        m.checkNotNullExpressionValue(textView, "binding.acceptGiftHeader");
                        b.a.k.b.m(textView, R.string.gift_confirmation_header_fail, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
                        TextView textView2 = cVar3.h().d;
                        m.checkNotNullExpressionValue(textView2, "binding.acceptGiftBodyText");
                        b.a.k.b.m(textView2, R.string.gift_confirmation_body_claimed, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
                        LinearLayout linearLayout2 = cVar3.h().f;
                        m.checkNotNullExpressionValue(linearLayout2, "binding.acceptGiftDisclaimerContainer");
                        linearLayout2.setVisibility(8);
                        MaterialButton materialButton2 = cVar3.h().e;
                        m.checkNotNullExpressionValue(materialButton2, "binding.acceptGiftConfirm");
                        b.a.k.b.m(materialButton2, R.string.gift_confirmation_button_fail, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
                        cVar3.h().e.setOnClickListener(new b.a.a.z.e(cVar3));
                    } else {
                        c cVar4 = c.this;
                        m.checkNotNullExpressionValue(aVar2, "giftUiState");
                        KProperty[] kPropertyArr4 = c.j;
                        Objects.requireNonNull(cVar4);
                        ModelGift gift2 = ((StoreGifting.HasGift) giftState).getGift();
                        cVar4.g(gift2, aVar2);
                        ModelStoreListing storeListing2 = gift2.getStoreListing();
                        String name = (storeListing2 == null || (sku2 = storeListing2.getSku()) == null) ? null : sku2.getName();
                        LinearLayout linearLayout3 = b.d.b.a.a.p0(b.d.b.a.a.p0(cVar4.h().i, "binding.acceptGiftHeader", gift2.isAnyNitroGift() ? b.a.k.b.e(cVar4, R.string.gift_confirmation_header_confirm_nitro, new Object[]{name}, (r4 & 4) != 0 ? b.a.j : null) : b.a.k.b.e(cVar4, R.string.gift_confirmation_header_confirm, new Object[0], (r4 & 4) != 0 ? b.a.j : null), cVar4).d, "binding.acceptGiftBodyText", gift2.isAnyNitroGift() ? b.a.k.b.e(cVar4, R.string.gift_confirmation_body_confirm_nitro, new Object[]{name, cVar4.i(gift2)}, (r4 & 4) != 0 ? b.a.j : null) : b.a.k.b.e(cVar4, R.string.gift_confirmation_body_confirm, new Object[]{name}, (r4 & 4) != 0 ? b.a.j : null), cVar4).f;
                        m.checkNotNullExpressionValue(linearLayout3, "binding.acceptGiftDisclaimerContainer");
                        if (gift2.isAnyNitroGift()) {
                            i2 = 0;
                        }
                        linearLayout3.setVisibility(i2);
                        MaterialButton materialButton3 = cVar4.h().e;
                        m.checkNotNullExpressionValue(materialButton3, "binding.acceptGiftConfirm");
                        b.a.k.b.m(materialButton3, R.string.gift_confirmation_button_confirm_mobile, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
                        cVar4.h().e.setOnClickListener(new b.a.a.z.f(gift2));
                    }
                } else if (giftState instanceof StoreGifting.GiftState.Redeeming) {
                    c cVar5 = c.this;
                    m.checkNotNullExpressionValue(aVar2, "giftUiState");
                    KProperty[] kPropertyArr5 = c.j;
                    Objects.requireNonNull(cVar5);
                    ModelGift gift3 = ((StoreGifting.GiftState.Redeeming) giftState).getGift();
                    cVar5.g(gift3, aVar2);
                    ModelStoreListing storeListing3 = gift3.getStoreListing();
                    String name2 = (storeListing3 == null || (sku = storeListing3.getSku()) == null) ? null : sku.getName();
                    MaterialButton materialButton4 = cVar5.h().e;
                    m.checkNotNullExpressionValue(materialButton4, "binding.acceptGiftConfirm");
                    materialButton4.setVisibility(8);
                    ProgressBar progressBar = cVar5.h().j;
                    m.checkNotNullExpressionValue(progressBar, "binding.acceptGiftProgress");
                    progressBar.setVisibility(0);
                    LinearLayout linearLayout4 = b.d.b.a.a.p0(b.d.b.a.a.p0(cVar5.h().i, "binding.acceptGiftHeader", gift3.isAnyNitroGift() ? b.a.k.b.e(cVar5, R.string.gift_confirmation_header_confirm_nitro, new Object[]{name2}, (r4 & 4) != 0 ? b.a.j : null) : b.a.k.b.e(cVar5, R.string.gift_confirmation_header_confirm, new Object[0], (r4 & 4) != 0 ? b.a.j : null), cVar5).d, "binding.acceptGiftBodyText", gift3.isAnyNitroGift() ? b.a.k.b.e(cVar5, R.string.gift_confirmation_body_confirm_nitro, new Object[]{name2, cVar5.i(gift3)}, (r4 & 4) != 0 ? b.a.j : null) : b.a.k.b.e(cVar5, R.string.gift_confirmation_body_confirm, new Object[]{name2}, (r4 & 4) != 0 ? b.a.j : null), cVar5).f;
                    m.checkNotNullExpressionValue(linearLayout4, "binding.acceptGiftDisclaimerContainer");
                    if (!gift3.isAnyNitroGift()) {
                        i = 8;
                    }
                    linearLayout4.setVisibility(i);
                } else if (giftState instanceof StoreGifting.GiftState.RedeemedFailed) {
                    c cVar6 = c.this;
                    StoreGifting.GiftState.RedeemedFailed redeemedFailed = (StoreGifting.GiftState.RedeemedFailed) giftState;
                    m.checkNotNullExpressionValue(aVar2, "giftUiState");
                    KProperty[] kPropertyArr6 = c.j;
                    Objects.requireNonNull(cVar6);
                    cVar6.g(redeemedFailed.getGift(), aVar2);
                    TextView textView3 = cVar6.h().i;
                    m.checkNotNullExpressionValue(textView3, "binding.acceptGiftHeader");
                    e = b.a.k.b.e(cVar6, R.string.gift_confirmation_header_fail, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
                    textView3.setText(e);
                    TextView textView4 = cVar6.h().d;
                    m.checkNotNullExpressionValue(textView4, "binding.acceptGiftBodyText");
                    b.a.k.b.m(textView4, R.string.gift_confirmation_body_unknown_error, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
                    if (redeemedFailed.getCanRetry()) {
                        TextView textView5 = cVar6.h().d;
                        m.checkNotNullExpressionValue(textView5, "binding.acceptGiftBodyText");
                        b.a.k.b.m(textView5, R.string.gift_confirmation_body_unknown_error, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
                        MaterialButton materialButton5 = cVar6.h().e;
                        m.checkNotNullExpressionValue(materialButton5, "binding.acceptGiftConfirm");
                        Context context2 = cVar6.getContext();
                        if (context2 != null) {
                            charSequence = context2.getText(R.string.retry);
                        }
                        materialButton5.setText(charSequence);
                        cVar6.h().e.setOnClickListener(new g(0, redeemedFailed));
                    } else {
                        Integer errorCode = redeemedFailed.getErrorCode();
                        if (errorCode != null && errorCode.intValue() == 50054) {
                            cVar6.h().d.setText(R.string.gift_confirmation_body_self_gift_no_payment);
                        } else if (errorCode != null && errorCode.intValue() == 100024) {
                            cVar6.h().d.setText(R.string.gift_confirmation_body_error_nitro_upgrade_downgrade);
                        } else if (errorCode != null && errorCode.intValue() == 100022) {
                            cVar6.h().d.setText(R.string.gift_confirmation_body_error_subscription_managed);
                        } else if (errorCode != null && errorCode.intValue() == 100025) {
                            cVar6.h().d.setText(R.string.gift_confirmation_body_error_invoice_open);
                        }
                        MaterialButton materialButton6 = cVar6.h().e;
                        m.checkNotNullExpressionValue(materialButton6, "binding.acceptGiftConfirm");
                        Context context3 = cVar6.getContext();
                        if (context3 != null) {
                            charSequence = context3.getText(R.string.gift_confirmation_button_fail);
                        }
                        materialButton6.setText(charSequence);
                        cVar6.h().e.setOnClickListener(new g(1, cVar6));
                    }
                } else if ((giftState instanceof StoreGifting.GiftState.Invalid) || (giftState instanceof StoreGifting.GiftState.LoadFailed) || (giftState instanceof StoreGifting.GiftState.Revoking)) {
                    c.this.dismiss();
                }
            }
            return Unit.a;
        }
    }

    /* compiled from: WidgetGiftAcceptDialog.kt */
    /* loaded from: classes.dex */
    public static final class f extends o implements Function1<Error, Unit> {
        public f() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(Error error) {
            m.checkNotNullParameter(error, "it");
            c.this.dismiss();
            return Unit.a;
        }
    }

    public c() {
        super(R.layout.widget_accept_gift_dialog);
    }

    public final void g(ModelGift modelGift, a.C0026a aVar) {
        ModelSku sku;
        Application application;
        String f2;
        ModelSku sku2;
        AppViewFlipper appViewFlipper = h().h;
        m.checkNotNullExpressionValue(appViewFlipper, "binding.acceptGiftFlipper");
        appViewFlipper.setDisplayedChild(1);
        String str = null;
        if (modelGift.isAnyNitroGift()) {
            GiftStyle customStyle = GiftStyleKt.getCustomStyle(modelGift);
            if (customStyle != null) {
                RLottieImageView rLottieImageView = h().c;
                m.checkNotNullExpressionValue(rLottieImageView, "binding.acceptGiftBodyLottie");
                rLottieImageView.setVisibility(0);
                SimpleDraweeView simpleDraweeView = h().f131b;
                m.checkNotNullExpressionValue(simpleDraweeView, "binding.acceptGiftBodyImage");
                simpleDraweeView.setVisibility(8);
                RLottieImageView rLottieImageView2 = h().c;
                if (aVar.f50b) {
                    rLottieImageView2.setImageResource(customStyle.getStaticRes());
                } else {
                    int animRes = customStyle.getAnimRes();
                    RLottieImageView rLottieImageView3 = h().c;
                    m.checkNotNullExpressionValue(rLottieImageView3, "binding.acceptGiftBodyLottie");
                    int dpToPixels = DimenUtils.dpToPixels(rLottieImageView3.getLayoutParams().width);
                    RLottieImageView rLottieImageView4 = h().c;
                    m.checkNotNullExpressionValue(rLottieImageView4, "binding.acceptGiftBodyLottie");
                    rLottieImageView2.c(animRes, dpToPixels, DimenUtils.dpToPixels(rLottieImageView4.getLayoutParams().height));
                    CoroutineScope coroutineScope = ViewCoroutineScopeKt.getCoroutineScope(rLottieImageView2);
                    if (coroutineScope != null) {
                        b.i.a.f.e.o.f.H0(coroutineScope, null, null, new C0027c(rLottieImageView2, null), 3, null);
                    }
                }
                m.checkNotNullExpressionValue(rLottieImageView2, "binding.acceptGiftBodyLo…  }\n          }\n        }");
            } else {
                MGImages mGImages = MGImages.INSTANCE;
                SimpleDraweeView simpleDraweeView2 = h().f131b;
                m.checkNotNullExpressionValue(simpleDraweeView2, "binding.acceptGiftBodyImage");
                MGImages.setImage$default(mGImages, simpleDraweeView2, PremiumUtils.INSTANCE.getNitroGiftIcon(modelGift), (MGImages.ChangeDetector) null, 4, (Object) null);
            }
        } else {
            ModelStoreListing storeListing = modelGift.getStoreListing();
            if (!(storeListing == null || (sku = storeListing.getSku()) == null || (application = sku.getApplication()) == null || (f2 = application.f()) == null)) {
                ModelStoreListing storeListing2 = modelGift.getStoreListing();
                str = IconUtils.getApplicationIcon$default((storeListing2 == null || (sku2 = storeListing2.getSku()) == null) ? 0L : sku2.getApplicationId(), f2, 0, 4, (Object) null);
            }
            h().f131b.setImageURI(str);
        }
        MaterialButton materialButton = h().e;
        m.checkNotNullExpressionValue(materialButton, "binding.acceptGiftConfirm");
        materialButton.setVisibility(0);
        ProgressBar progressBar = h().j;
        m.checkNotNullExpressionValue(progressBar, "binding.acceptGiftProgress");
        progressBar.setVisibility(8);
    }

    public final i4 h() {
        return (i4) this.l.getValue((Fragment) this, j[0]);
    }

    public final CharSequence i(ModelGift modelGift) {
        int i;
        CharSequence e2;
        CharSequence e3;
        SubscriptionPlan subscriptionPlan = modelGift.getSubscriptionPlan();
        if (subscriptionPlan == null) {
            return "";
        }
        int ordinal = subscriptionPlan.b().ordinal();
        if (ordinal == 0) {
            i = R.plurals.gift_confirmation_nitro_time_frame_months_time;
        } else if (ordinal == 1) {
            i = R.plurals.gift_confirmation_nitro_time_frame_years_time;
        } else {
            throw new NoWhenBranchMatchedException();
        }
        Context requireContext = requireContext();
        m.checkNotNullExpressionValue(requireContext, "requireContext()");
        CharSequence i18nPluralString = StringResourceUtilsKt.getI18nPluralString(requireContext, i, subscriptionPlan.c(), Integer.valueOf(subscriptionPlan.c()));
        int ordinal2 = subscriptionPlan.b().ordinal();
        if (ordinal2 == 0) {
            e2 = b.a.k.b.e(this, R.string.gift_confirmation_nitro_time_frame_months, new Object[]{i18nPluralString}, (r4 & 4) != 0 ? b.a.j : null);
            return e2;
        } else if (ordinal2 == 1) {
            e3 = b.a.k.b.e(this, R.string.gift_confirmation_nitro_time_frame_years, new Object[]{i18nPluralString}, (r4 & 4) != 0 ? b.a.j : null);
            return e3;
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        String string;
        Bundle arguments = getArguments();
        if (!(arguments == null || (string = arguments.getString("ARG_GIFT_CODE")) == null)) {
            a aVar = k;
            m.checkNotNullExpressionValue(string, "it");
            Objects.requireNonNull(aVar);
            m.checkNotNullParameter(string, "giftCode");
            String str = "gift:" + string;
            if (str != null) {
                StoreStream.Companion.getNotices().markDialogSeen(str);
            }
        }
        super.onDestroy();
    }

    @Override // com.discord.app.AppDialog
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        Bundle arguments = getArguments();
        String string = arguments != null ? arguments.getString("ARG_GIFT_CODE") : null;
        if (string == null) {
            dismiss();
            return;
        }
        LinkifiedTextView linkifiedTextView = h().g;
        m.checkNotNullExpressionValue(linkifiedTextView, "binding.acceptGiftDisclaimerText");
        b.a.k.b.m(linkifiedTextView, R.string.gift_confirmation_body_confirm_nitro_disclaimer, new Object[]{b.a.d.f.a.a(360055386693L, null)}, (r4 & 4) != 0 ? b.g.j : null);
        StoreStream.Companion companion = StoreStream.Companion;
        Observable j2 = Observable.j(companion.getGifting().requestGift(string), companion.getAccessibility().observeReducedMotionEnabled(), d.j);
        m.checkNotNullExpressionValue(j2, "Observable.combineLatest…iftState, reduceMotion) }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(j2, this, null, 2, null), c.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new f(), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new e());
    }
}
