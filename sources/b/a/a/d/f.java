package b.a.a.d;

import androidx.annotation.StringRes;
import com.discord.app.AppViewModel;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserRelationships;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.PublishSubject;
import xyz.discord.R;
/* compiled from: UserActionsDialogViewModel.kt */
/* loaded from: classes.dex */
public final class f extends AppViewModel<d> {
    public static final a j = new a(null);
    public final PublishSubject<b> k;
    public final long l;
    public final RestAPI m;

    /* compiled from: UserActionsDialogViewModel.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: UserActionsDialogViewModel.kt */
    /* loaded from: classes.dex */
    public static abstract class b {

        /* compiled from: UserActionsDialogViewModel.kt */
        /* loaded from: classes.dex */
        public static final class a extends b {
            public final int a;

            public a(int i) {
                super(null);
                this.a = i;
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof a) && this.a == ((a) obj).a;
                }
                return true;
            }

            public int hashCode() {
                return this.a;
            }

            public String toString() {
                return b.d.b.a.a.A(b.d.b.a.a.R("Failure(failureMessageStringRes="), this.a, ")");
            }
        }

        /* compiled from: UserActionsDialogViewModel.kt */
        /* renamed from: b.a.a.d.f$b$b  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C0018b extends b {
            public final int a;

            public C0018b(int i) {
                super(null);
                this.a = i;
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof C0018b) && this.a == ((C0018b) obj).a;
                }
                return true;
            }

            public int hashCode() {
                return this.a;
            }

            public String toString() {
                return b.d.b.a.a.A(b.d.b.a.a.R("Success(successMessageStringRes="), this.a, ")");
            }
        }

        public b() {
        }

        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: UserActionsDialogViewModel.kt */
    /* loaded from: classes.dex */
    public static final class c {
        public final User a;

        /* renamed from: b  reason: collision with root package name */
        public final Integer f45b;
        public final GuildMember c;

        public c(User user, Integer num, GuildMember guildMember) {
            this.a = user;
            this.f45b = num;
            this.c = guildMember;
        }
    }

    /* compiled from: UserActionsDialogViewModel.kt */
    /* loaded from: classes.dex */
    public static abstract class d {

        /* compiled from: UserActionsDialogViewModel.kt */
        /* loaded from: classes.dex */
        public static final class a extends d {
            public final User a;

            /* renamed from: b  reason: collision with root package name */
            public final String f46b;
            public final boolean c;
            public final boolean d;
            public final boolean e;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public a(User user, String str, boolean z2, boolean z3, boolean z4) {
                super(null);
                m.checkNotNullParameter(user, "user");
                this.a = user;
                this.f46b = str;
                this.c = z2;
                this.d = z3;
                this.e = z4;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof a)) {
                    return false;
                }
                a aVar = (a) obj;
                return m.areEqual(this.a, aVar.a) && m.areEqual(this.f46b, aVar.f46b) && this.c == aVar.c && this.d == aVar.d && this.e == aVar.e;
            }

            public int hashCode() {
                User user = this.a;
                int i = 0;
                int hashCode = (user != null ? user.hashCode() : 0) * 31;
                String str = this.f46b;
                if (str != null) {
                    i = str.hashCode();
                }
                int i2 = (hashCode + i) * 31;
                boolean z2 = this.c;
                int i3 = 1;
                if (z2) {
                    z2 = true;
                }
                int i4 = z2 ? 1 : 0;
                int i5 = z2 ? 1 : 0;
                int i6 = (i2 + i4) * 31;
                boolean z3 = this.d;
                if (z3) {
                    z3 = true;
                }
                int i7 = z3 ? 1 : 0;
                int i8 = z3 ? 1 : 0;
                int i9 = (i6 + i7) * 31;
                boolean z4 = this.e;
                if (!z4) {
                    i3 = z4 ? 1 : 0;
                }
                return i9 + i3;
            }

            public String toString() {
                StringBuilder R = b.d.b.a.a.R("Loaded(user=");
                R.append(this.a);
                R.append(", userNickname=");
                R.append(this.f46b);
                R.append(", showBlockItem=");
                R.append(this.c);
                R.append(", showUnblockItem=");
                R.append(this.d);
                R.append(", showRemoveFriendItem=");
                return b.d.b.a.a.M(R, this.e, ")");
            }
        }

        /* compiled from: UserActionsDialogViewModel.kt */
        /* loaded from: classes.dex */
        public static final class b extends d {
            public static final b a = new b();

            public b() {
                super(null);
            }
        }

        public d(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: UserActionsDialogViewModel.kt */
    /* loaded from: classes.dex */
    public static final class e extends o implements Function1<Void, Unit> {
        public final /* synthetic */ int $successMessageStringRes;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public e(int i) {
            super(1);
            this.$successMessageStringRes = i;
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(Void r3) {
            f fVar = f.this;
            int i = this.$successMessageStringRes;
            PublishSubject<b> publishSubject = fVar.k;
            publishSubject.k.onNext(new b.C0018b(i));
            return Unit.a;
        }
    }

    /* compiled from: UserActionsDialogViewModel.kt */
    /* renamed from: b.a.a.d.f$f  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0019f extends o implements Function1<Error, Unit> {
        public C0019f() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(Error error) {
            m.checkNotNullParameter(error, "it");
            PublishSubject<b> publishSubject = f.this.k;
            publishSubject.k.onNext(new b.a(R.string.default_failure_to_perform_action_message));
            return Unit.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public f(long j2, long j3, RestAPI restAPI, Observable observable, int i) {
        super(d.b.a);
        Observable observable2;
        RestAPI api = (i & 4) != 0 ? RestAPI.Companion.getApi() : null;
        if ((i & 8) != 0) {
            StoreStream.Companion companion = StoreStream.Companion;
            StoreUser users = companion.getUsers();
            StoreUserRelationships userRelationships = companion.getUserRelationships();
            StoreGuilds guilds = companion.getGuilds();
            StoreChannels channels = companion.getChannels();
            Observable<User> observeUser = users.observeUser(j2);
            Observable<Integer> observe = userRelationships.observe(j2);
            Observable q = channels.observeChannel(j3).Y(new b.a.a.d.d(guilds, j2)).q();
            m.checkNotNullExpressionValue(q, "storeChannels\n          …  .distinctUntilChanged()");
            observable2 = Observable.i(observeUser, observe, q, b.a.a.d.e.a).q();
            m.checkNotNullExpressionValue(observable2, "Observable.combineLatest…  .distinctUntilChanged()");
        } else {
            observable2 = null;
        }
        m.checkNotNullParameter(api, "restAPI");
        m.checkNotNullParameter(observable2, "storeObservable");
        this.l = j2;
        this.m = api;
        this.k = PublishSubject.k0();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable2), this, null, 2, null), f.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new b.a.a.d.b(this));
    }

    public final void removeRelationship(@StringRes int i) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.m.removeRelationship("User Profile", this.l), false, 1, null), this, null, 2, null), f.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new C0019f(), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new e(i));
    }
}
