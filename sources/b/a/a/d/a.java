package b.a.a.d;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.a.d.f;
import b.a.d.f0;
import b.a.d.h0;
import b.a.i.s1;
import com.discord.app.AppDialog;
import com.discord.app.AppViewModel;
import com.discord.models.user.User;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.user.UserNameFormatterKt;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.a0;
import d0.z.d.k;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.subjects.PublishSubject;
import xyz.discord.R;
/* compiled from: UserActionsDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \u001a2\u00020\u0001:\u0001\u001bB\u0007¢\u0006\u0004\b\u0019\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\bR\u001d\u0010\u000e\u001a\u00020\t8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\f\u0010\rR\u001d\u0010\u0014\u001a\u00020\u000f8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0018\u001a\u00020\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017¨\u0006\u001c"}, d2 = {"Lb/a/a/d/a;", "Lcom/discord/app/AppDialog;", "Landroid/view/View;", "view", "", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Lb/a/i/s1;", "l", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "g", "()Lb/a/i/s1;", "binding", "Lb/a/a/d/f;", "m", "Lkotlin/Lazy;", "h", "()Lb/a/a/d/f;", "viewModel", "Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;", "n", "Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;", "imagesChangeDetector", HookHelper.constructorName, "k", "b", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class a extends AppDialog {
    public static final /* synthetic */ KProperty[] j = {b.d.b.a.a.b0(a.class, "binding", "getBinding()Lcom/discord/databinding/UserActionsDialogBinding;", 0)};
    public static final b k = new b(null);
    public final Lazy m;
    public final FragmentViewBindingDelegate l = FragmentViewBindingDelegateKt.viewBinding$default(this, c.j, null, 2, null);
    public final MGImages.DistinctChangeDetector n = new MGImages.DistinctChangeDetector();

    /* compiled from: java-style lambda group */
    /* renamed from: b.a.a.d.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class View$OnClickListenerC0017a implements View.OnClickListener {
        public final /* synthetic */ int j;
        public final /* synthetic */ Object k;

        public View$OnClickListenerC0017a(int i, Object obj) {
            this.j = i;
            this.k = obj;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            Observable addRelationship;
            int i = this.j;
            if (i == 0) {
                KProperty[] kPropertyArr = a.j;
                b.a.a.d.f h = ((a) this.k).h();
                addRelationship = h.m.addRelationship("User Profile", h.l, (r16 & 4) != 0 ? null : 2, (r16 & 8) != 0 ? null : null, (r16 & 16) != 0 ? null : null);
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(addRelationship, false, 1, null), h, null, 2, null), b.a.a.d.f.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new b.a.a.d.h(h), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new b.a.a.d.g(h));
            } else if (i == 1) {
                KProperty[] kPropertyArr2 = a.j;
                ((a) this.k).h().removeRelationship(R.string.user_has_been_unblocked);
            } else if (i == 2) {
                KProperty[] kPropertyArr3 = a.j;
                ((a) this.k).h().removeRelationship(R.string.friend_has_been_deleted);
            } else {
                throw null;
            }
        }
    }

    /* compiled from: UserActionsDialog.kt */
    /* loaded from: classes.dex */
    public static final class b {
        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: UserActionsDialog.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class c extends k implements Function1<View, s1> {
        public static final c j = new c();

        public c() {
            super(1, s1.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/UserActionsDialogBinding;", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public s1 invoke(View view) {
            View view2 = view;
            m.checkNotNullParameter(view2, "p1");
            int i = R.id.user_actions_dialog_avatar;
            SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view2.findViewById(R.id.user_actions_dialog_avatar);
            if (simpleDraweeView != null) {
                i = R.id.user_actions_dialog_block_item;
                TextView textView = (TextView) view2.findViewById(R.id.user_actions_dialog_block_item);
                if (textView != null) {
                    i = R.id.user_actions_dialog_remove_friend_item;
                    TextView textView2 = (TextView) view2.findViewById(R.id.user_actions_dialog_remove_friend_item);
                    if (textView2 != null) {
                        i = R.id.user_actions_dialog_unblock_item;
                        TextView textView3 = (TextView) view2.findViewById(R.id.user_actions_dialog_unblock_item);
                        if (textView3 != null) {
                            i = R.id.user_actions_dialog_user_name;
                            TextView textView4 = (TextView) view2.findViewById(R.id.user_actions_dialog_user_name);
                            if (textView4 != null) {
                                return new s1((LinearLayout) view2, simpleDraweeView, textView, textView2, textView3, textView4);
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
        }
    }

    /* compiled from: ObservableExtensions.kt */
    /* loaded from: classes.dex */
    public static final class d<T, R> implements j0.k.b<Object, Boolean> {
        public static final d j = new d();

        @Override // j0.k.b
        public Boolean call(Object obj) {
            return Boolean.valueOf(obj instanceof f.d.a);
        }
    }

    /* compiled from: ObservableExtensions.kt */
    /* loaded from: classes.dex */
    public static final class e<T, R> implements j0.k.b<Object, T> {
        public static final e j = new e();

        @Override // j0.k.b
        public final T call(Object obj) {
            Objects.requireNonNull(obj, "null cannot be cast to non-null type com.discord.dialogs.useractions.UserActionsDialogViewModel.ViewState.Loaded");
            return (T) ((f.d.a) obj);
        }
    }

    /* compiled from: UserActionsDialog.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class f extends k implements Function1<f.d.a, Unit> {
        public f(a aVar) {
            super(1, aVar, a.class, "updateView", "updateView(Lcom/discord/dialogs/useractions/UserActionsDialogViewModel$ViewState$Loaded;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(f.d.a aVar) {
            f.d.a aVar2 = aVar;
            m.checkNotNullParameter(aVar2, "p1");
            a aVar3 = (a) this.receiver;
            KProperty[] kPropertyArr = a.j;
            SimpleDraweeView simpleDraweeView = aVar3.g().f192b;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.userActionsDialogAvatar");
            IconUtils.setIcon$default(simpleDraweeView, IconUtils.getForUser$default(aVar2.a, true, null, 4, null), 0, (Function1) null, aVar3.n, 12, (Object) null);
            TextView textView = aVar3.g().f;
            m.checkNotNullExpressionValue(textView, "binding.userActionsDialogUserName");
            User user = aVar2.a;
            String str = aVar2.f46b;
            Context requireContext = aVar3.requireContext();
            m.checkNotNullExpressionValue(requireContext, "requireContext()");
            textView.setText(UserNameFormatterKt.getSpannableForUserNameWithDiscrim(user, str, requireContext, R.attr.colorHeaderPrimary, R.attr.font_primary_semibold, R.integer.uikit_textsize_large_sp, R.attr.colorTextMuted, R.attr.font_primary_normal, R.integer.uikit_textsize_large_sp));
            TextView textView2 = aVar3.g().c;
            m.checkNotNullExpressionValue(textView2, "binding.userActionsDialogBlockItem");
            int i = 8;
            textView2.setVisibility(aVar2.c ? 0 : 8);
            TextView textView3 = aVar3.g().e;
            m.checkNotNullExpressionValue(textView3, "binding.userActionsDialogUnblockItem");
            textView3.setVisibility(aVar2.d ? 0 : 8);
            TextView textView4 = aVar3.g().d;
            m.checkNotNullExpressionValue(textView4, "binding.userActionsDialogRemoveFriendItem");
            if (aVar2.e) {
                i = 0;
            }
            textView4.setVisibility(i);
            return Unit.a;
        }
    }

    /* compiled from: UserActionsDialog.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class g extends k implements Function1<f.b, Unit> {
        public g(a aVar) {
            super(1, aVar, a.class, "handleEvent", "handleEvent(Lcom/discord/dialogs/useractions/UserActionsDialogViewModel$Event;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(f.b bVar) {
            f.b bVar2 = bVar;
            m.checkNotNullParameter(bVar2, "p1");
            a aVar = (a) this.receiver;
            KProperty[] kPropertyArr = a.j;
            Objects.requireNonNull(aVar);
            if (bVar2 instanceof f.b.C0018b) {
                b.a.d.m.i(aVar, ((f.b.C0018b) bVar2).a, 0, 4);
                aVar.dismiss();
            } else if (bVar2 instanceof f.b.a) {
                b.a.d.m.i(aVar, ((f.b.a) bVar2).a, 0, 4);
                aVar.dismiss();
            }
            return Unit.a;
        }
    }

    /* compiled from: UserActionsDialog.kt */
    /* loaded from: classes.dex */
    public static final class h extends o implements Function0<AppViewModel<f.d>> {
        public h() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public AppViewModel<f.d> invoke() {
            a aVar = a.this;
            KProperty[] kPropertyArr = a.j;
            return new b.a.a.d.f(aVar.getArgumentsOrDefault().getLong("com.discord.intent.extra.EXTRA_USER_ID", 0L), a.this.getArgumentsOrDefault().getLong("com.discord.intent.extra.EXTRA_CHANNEL_ID", 0L), null, null, 12);
        }
    }

    public a() {
        super(R.layout.user_actions_dialog);
        h hVar = new h();
        f0 f0Var = new f0(this);
        this.m = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(b.a.a.d.f.class), new k(4, f0Var), new h0(hVar));
    }

    public final s1 g() {
        return (s1) this.l.getValue((Fragment) this, j[0]);
    }

    public final b.a.a.d.f h() {
        return (b.a.a.d.f) this.m.getValue();
    }

    @Override // com.discord.app.AppDialog
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        g().c.setOnClickListener(new View$OnClickListenerC0017a(0, this));
        g().e.setOnClickListener(new View$OnClickListenerC0017a(1, this));
        g().d.setOnClickListener(new View$OnClickListenerC0017a(2, this));
    }

    @Override // com.discord.app.AppDialog
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable<R> F = h().observeViewState().x(d.j).F(e.j);
        m.checkNotNullExpressionValue(F, "filter { it is T }.map { it as T }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(F, this, null, 2, null), a.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new f(this));
        PublishSubject<f.b> publishSubject = h().k;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(publishSubject, this, null, 2, null), a.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new g(this));
    }
}
