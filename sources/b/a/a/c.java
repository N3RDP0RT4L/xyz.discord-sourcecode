package b.a.a;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.a.i.a0;
import b.a.k.b;
import com.discord.app.AppDialog;
import com.discord.dialogs.ImageUploadDialog;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.rest.FileUploadAlertType;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.settings.premium.WidgetSettingsPremium;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.switchmaterial.SwitchMaterial;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: ImageUploadFailedDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 \u00132\u00020\u0001:\u0001\u0014B\u0007¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006R\u001e\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\b\u0010\tR\u001d\u0010\u0010\u001a\u00020\u000b8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000f¨\u0006\u0015"}, d2 = {"Lb/a/a/c;", "Lcom/discord/app/AppDialog;", "Landroid/view/View;", "view", "", "onViewBound", "(Landroid/view/View;)V", "Lkotlin/Function0;", "m", "Lkotlin/jvm/functions/Function0;", "onResendCompressed", "Lb/a/i/a0;", "l", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "g", "()Lb/a/i/a0;", "binding", HookHelper.constructorName, "()V", "k", "b", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class c extends AppDialog {
    public static final /* synthetic */ KProperty[] j = {b.d.b.a.a.b0(c.class, "binding", "getBinding()Lcom/discord/databinding/ImageUploadFailedDialogBinding;", 0)};
    public static final b k = new b(null);
    public final FragmentViewBindingDelegate l = FragmentViewBindingDelegateKt.viewBinding$default(this, C0016c.j, null, 2, null);
    public Function0<Unit> m;

    /* compiled from: java-style lambda group */
    /* loaded from: classes.dex */
    public static final class a implements View.OnClickListener {
        public final /* synthetic */ int j;
        public final /* synthetic */ Object k;

        public a(int i, Object obj) {
            this.j = i;
            this.k = obj;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            int i = this.j;
            if (i == 0) {
                Function0<Unit> function0 = ((c) this.k).m;
                if (function0 != null) {
                    function0.invoke();
                }
                ((c) this.k).dismiss();
            } else if (i == 1) {
                ((c) this.k).dismiss();
            } else if (i == 2) {
                ((c) this.k).dismiss();
            } else if (i == 3) {
                AnalyticsTracker.INSTANCE.premiumSettingsOpened(new Traits.Location(null, "File Upload Popout (w/ Compression)", null, null, null, 29, null));
                WidgetSettingsPremium.Companion companion = WidgetSettingsPremium.Companion;
                m.checkNotNullExpressionValue(view, "it");
                Context context = view.getContext();
                m.checkNotNullExpressionValue(context, "it.context");
                WidgetSettingsPremium.Companion.launch$default(companion, context, null, null, 6, null);
                ((c) this.k).dismiss();
            } else if (i == 4) {
                KProperty[] kPropertyArr = c.j;
                ((c) this.k).g().c.toggle();
            } else {
                throw null;
            }
        }
    }

    /* compiled from: ImageUploadFailedDialog.kt */
    /* loaded from: classes.dex */
    public static final class b {
        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final void a(FragmentManager fragmentManager, boolean z2, int i, float f, float f2, Function0<Unit> function0, int i2, boolean z3, boolean z4, boolean z5) {
            FileUploadAlertType fileUploadAlertType;
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            if (z2) {
                fileUploadAlertType = FileUploadAlertType.OVER_MAX_SIZE;
            } else {
                fileUploadAlertType = FileUploadAlertType.NITRO_UPSELL;
            }
            FileUploadAlertType fileUploadAlertType2 = fileUploadAlertType;
            float f3 = 1048576;
            StoreStream.Companion.getAnalytics().trackFileUploadAlertViewed(fileUploadAlertType2, i2, (int) (f * f3), (int) (f3 * f2), z3, z4, z2);
            c cVar = new c();
            cVar.m = function0;
            Bundle bundle = new Bundle();
            bundle.putBoolean("PARAM_IS_USER_PREMIUM", z2);
            bundle.putBoolean("PARAM_CAN_COMPRESS", !z4 && !z5);
            bundle.putInt("PARAM_MAX_FILE_SIZE_MB", i);
            cVar.setArguments(bundle);
            cVar.show(fragmentManager, ImageUploadDialog.class.getName());
        }
    }

    /* compiled from: ImageUploadFailedDialog.kt */
    /* renamed from: b.a.a.c$c  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class C0016c extends k implements Function1<View, a0> {
        public static final C0016c j = new C0016c();

        public C0016c() {
            super(1, a0.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/ImageUploadFailedDialogBinding;", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public a0 invoke(View view) {
            View view2 = view;
            m.checkNotNullParameter(view2, "p1");
            int i = R.id.chat_input_upload_too_large_center_file;
            ImageView imageView = (ImageView) view2.findViewById(R.id.chat_input_upload_too_large_center_file);
            if (imageView != null) {
                i = R.id.image_upload_failed_cancel;
                MaterialButton materialButton = (MaterialButton) view2.findViewById(R.id.image_upload_failed_cancel);
                if (materialButton != null) {
                    i = R.id.image_upload_failed_compression_settings_checkbox;
                    SwitchMaterial switchMaterial = (SwitchMaterial) view2.findViewById(R.id.image_upload_failed_compression_settings_checkbox);
                    if (switchMaterial != null) {
                        i = R.id.image_upload_failed_compression_settings_container;
                        RelativeLayout relativeLayout = (RelativeLayout) view2.findViewById(R.id.image_upload_failed_compression_settings_container);
                        if (relativeLayout != null) {
                            i = R.id.image_upload_failed_divider;
                            View findViewById = view2.findViewById(R.id.image_upload_failed_divider);
                            if (findViewById != null) {
                                i = R.id.image_upload_failed_nitro;
                                MaterialButton materialButton2 = (MaterialButton) view2.findViewById(R.id.image_upload_failed_nitro);
                                if (materialButton2 != null) {
                                    i = R.id.image_upload_failed_nitro_wrapper;
                                    RelativeLayout relativeLayout2 = (RelativeLayout) view2.findViewById(R.id.image_upload_failed_nitro_wrapper);
                                    if (relativeLayout2 != null) {
                                        i = R.id.image_upload_failed_okay;
                                        MaterialButton materialButton3 = (MaterialButton) view2.findViewById(R.id.image_upload_failed_okay);
                                        if (materialButton3 != null) {
                                            i = R.id.image_upload_failed_send_compress;
                                            MaterialButton materialButton4 = (MaterialButton) view2.findViewById(R.id.image_upload_failed_send_compress);
                                            if (materialButton4 != null) {
                                                i = R.id.image_upload_failed_text;
                                                TextView textView = (TextView) view2.findViewById(R.id.image_upload_failed_text);
                                                if (textView != null) {
                                                    i = R.id.setting_label;
                                                    TextView textView2 = (TextView) view2.findViewById(R.id.setting_label);
                                                    if (textView2 != null) {
                                                        i = R.id.setting_subtext;
                                                        TextView textView3 = (TextView) view2.findViewById(R.id.setting_subtext);
                                                        if (textView3 != null) {
                                                            return new a0((RelativeLayout) view2, imageView, materialButton, switchMaterial, relativeLayout, findViewById, materialButton2, relativeLayout2, materialButton3, materialButton4, textView, textView2, textView3);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
        }
    }

    /* compiled from: ImageUploadFailedDialog.kt */
    /* loaded from: classes.dex */
    public static final class d implements CompoundButton.OnCheckedChangeListener {
        public static final d a = new d();

        @Override // android.widget.CompoundButton.OnCheckedChangeListener
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z2) {
            StoreStream.Companion.getUserSettings().setIsAutoImageCompressionEnabled(z2);
        }
    }

    public c() {
        super(R.layout.image_upload_failed_dialog);
    }

    public final a0 g() {
        return (a0) this.l.getValue((Fragment) this, j[0]);
    }

    @Override // com.discord.app.AppDialog
    public void onViewBound(View view) {
        CharSequence charSequence;
        CharSequence charSequence2;
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        int i = getArgumentsOrDefault().getInt("PARAM_MAX_FILE_SIZE_MB");
        boolean z2 = getArgumentsOrDefault().getBoolean("PARAM_IS_USER_PREMIUM");
        boolean z3 = this.m != null && getArgumentsOrDefault().getBoolean("PARAM_CAN_COMPRESS");
        int i2 = 8;
        int i3 = i != 8 ? i != 50 ? i != 100 ? 0 : R.string.file_upload_limit_premium_tier_2 : R.string.file_upload_limit_premium_tier_1 : R.string.file_upload_limit_standard;
        MaterialButton materialButton = g().i;
        m.checkNotNullExpressionValue(materialButton, "binding.imageUploadFailedSendCompress");
        materialButton.setVisibility(z3 ? 0 : 8);
        g().i.setOnClickListener(new a(0, this));
        boolean z4 = z2 && !z3;
        View view2 = g().e;
        m.checkNotNullExpressionValue(view2, "binding.imageUploadFailedDivider");
        boolean z5 = !z4;
        view2.setVisibility(z5 ? 0 : 8);
        MaterialButton materialButton2 = g().h;
        m.checkNotNullExpressionValue(materialButton2, "binding.imageUploadFailedOkay");
        materialButton2.setVisibility(z4 ? 0 : 8);
        g().h.setOnClickListener(new a(1, this));
        MaterialButton materialButton3 = g().f73b;
        m.checkNotNullExpressionValue(materialButton3, "binding.imageUploadFailedCancel");
        materialButton3.setVisibility(z5 ? 0 : 8);
        g().f73b.setOnClickListener(new a(2, this));
        RelativeLayout relativeLayout = g().g;
        m.checkNotNullExpressionValue(relativeLayout, "binding.imageUploadFailedNitroWrapper");
        relativeLayout.setVisibility(z2 ^ true ? 0 : 8);
        g().f.setOnClickListener(new a(3, this));
        RelativeLayout relativeLayout2 = g().d;
        m.checkNotNullExpressionValue(relativeLayout2, "binding.imageUploadFaile…pressionSettingsContainer");
        if (z3) {
            i2 = 0;
        }
        relativeLayout2.setVisibility(i2);
        g().d.setOnClickListener(new a(4, this));
        SwitchMaterial switchMaterial = g().c;
        m.checkNotNullExpressionValue(switchMaterial, "binding.imageUploadFaile…mpressionSettingsCheckbox");
        switchMaterial.setChecked(StoreStream.Companion.getUserSettings().getIsAutoImageCompressionEnabled());
        g().c.setOnCheckedChangeListener(d.a);
        TextView textView = g().j;
        m.checkNotNullExpressionValue(textView, "binding.imageUploadFailedText");
        CharSequence charSequence3 = null;
        if (z2 || !z3) {
            Context context = getContext();
            if (context != null) {
                Object[] objArr = new Object[1];
                Context context2 = getContext();
                if (context2 != null) {
                    charSequence = b.a.k.b.b(context2, i3, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                } else {
                    charSequence = null;
                }
                objArr[0] = charSequence;
                charSequence3 = b.a.k.b.b(context, R.string.upload_area_too_large_help, objArr, (r4 & 4) != 0 ? b.C0034b.j : null);
            }
        } else {
            Context context3 = getContext();
            if (context3 != null) {
                Object[] objArr2 = new Object[1];
                Context context4 = getContext();
                if (context4 != null) {
                    charSequence2 = b.a.k.b.b(context4, i3, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                } else {
                    charSequence2 = null;
                }
                objArr2[0] = charSequence2;
                charSequence3 = b.a.k.b.b(context3, R.string.upload_area_too_large_help_mobile, objArr2, (r4 & 4) != 0 ? b.C0034b.j : null);
            }
        }
        textView.setText(charSequence3);
    }
}
