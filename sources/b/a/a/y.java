package b.a.a;

import b.a.a.w;
import com.discord.utilities.error.Error;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.subjects.PublishSubject;
/* compiled from: WidgetUrgentMessageDialogViewModel.kt */
/* loaded from: classes.dex */
public final class y extends o implements Function1<Error, Unit> {
    public final /* synthetic */ w this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public y(w wVar) {
        super(1);
        this.this$0 = wVar;
    }

    @Override // kotlin.jvm.functions.Function1
    public Unit invoke(Error error) {
        m.checkNotNullParameter(error, "it");
        PublishSubject<w.b> publishSubject = this.this$0.k;
        publishSubject.k.onNext(w.b.C0024b.a);
        return Unit.a;
    }
}
