package b.a.a;

import andhook.lib.HookHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.discord.app.AppDialog;
import com.discord.utilities.birthday.BirthdayHelper;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.google.android.material.button.MaterialButton;
import d0.z.d.m;
import java.util.Calendar;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: DatePickerDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\n\n\u0002\u0010\u000b\n\u0002\b\u0007\u0018\u0000 \u001d2\u00020\u0001:\u0001\u001eB\u0007¢\u0006\u0004\b\u001c\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0004R\u001d\u0010\u000b\u001a\u00020\u00068B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\t\u0010\nR0\u0010\u0014\u001a\u0010\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0002\u0018\u00010\f8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0017\u001a\u00020\r8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016R\u0016\u0010\u001b\u001a\u00020\u00188\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0019\u0010\u001a¨\u0006\u001f"}, d2 = {"Lb/a/a/k;", "Lcom/discord/app/AppDialog;", "", "onViewBoundOrOnResume", "()V", "onPause", "Lb/a/i/k;", "o", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "g", "()Lb/a/i/k;", "binding", "Lkotlin/Function1;", "", "l", "Lkotlin/jvm/functions/Function1;", "getOnDatePicked", "()Lkotlin/jvm/functions/Function1;", "setOnDatePicked", "(Lkotlin/jvm/functions/Function1;)V", "onDatePicked", "m", "J", "selectedDate", "", "n", "Z", "datePicked", HookHelper.constructorName, "k", "a", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class k extends AppDialog {
    public static final /* synthetic */ KProperty[] j = {b.d.b.a.a.b0(k.class, "binding", "getBinding()Lcom/discord/databinding/DialogDatePickerBinding;", 0)};
    public static final a k = new a(null);
    public Function1<? super Long, Unit> l;
    public long m;
    public boolean n;
    public final FragmentViewBindingDelegate o = FragmentViewBindingDelegateKt.viewBinding$default(this, b.j, null, 2, null);

    /* compiled from: DatePickerDialog.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final k a(FragmentManager fragmentManager, CharSequence charSequence, long j, long j2) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(charSequence, "label");
            k kVar = new k();
            Bundle bundle = new Bundle();
            bundle.putCharSequence("label", charSequence);
            bundle.putLong("initial_date", j);
            bundle.putLong("max_date", j2);
            kVar.setArguments(bundle);
            kVar.show(fragmentManager, k.class.getName());
            return kVar;
        }
    }

    /* compiled from: DatePickerDialog.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class b extends d0.z.d.k implements Function1<View, b.a.i.k> {
        public static final b j = new b();

        public b() {
            super(1, b.a.i.k.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/DialogDatePickerBinding;", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public b.a.i.k invoke(View view) {
            View view2 = view;
            m.checkNotNullParameter(view2, "p1");
            int i = R.id.dialog_date_picker_done;
            MaterialButton materialButton = (MaterialButton) view2.findViewById(R.id.dialog_date_picker_done);
            if (materialButton != null) {
                i = R.id.dialog_date_picker_input;
                DatePicker datePicker = (DatePicker) view2.findViewById(R.id.dialog_date_picker_input);
                if (datePicker != null) {
                    i = R.id.dialog_date_picker_title;
                    TextView textView = (TextView) view2.findViewById(R.id.dialog_date_picker_title);
                    if (textView != null) {
                        return new b.a.i.k((LinearLayout) view2, materialButton, datePicker, textView);
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
        }
    }

    /* compiled from: DatePickerDialog.kt */
    /* loaded from: classes.dex */
    public static final class c implements DatePicker.OnDateChangedListener {
        public c() {
        }

        @Override // android.widget.DatePicker.OnDateChangedListener
        public final void onDateChanged(DatePicker datePicker, int i, int i2, int i3) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(i, i2, i3);
            k kVar = k.this;
            m.checkNotNullExpressionValue(calendar, "cal");
            kVar.m = calendar.getTimeInMillis();
            k kVar2 = k.this;
            kVar2.n = true;
            Function1<? super Long, Unit> function1 = kVar2.l;
            if (function1 != null) {
                function1.invoke(Long.valueOf(kVar2.m));
            }
        }
    }

    /* compiled from: DatePickerDialog.kt */
    /* loaded from: classes.dex */
    public static final class d implements View.OnClickListener {
        public d() {
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            Function1<? super Long, Unit> function1;
            k kVar = k.this;
            if (kVar.n && (function1 = kVar.l) != null) {
                function1.invoke(Long.valueOf(kVar.m));
            }
            k.this.dismiss();
        }
    }

    public k() {
        super(R.layout.dialog_date_picker);
    }

    public final b.a.i.k g() {
        return (b.a.i.k) this.o.getValue((Fragment) this, j[0]);
    }

    @Override // com.discord.app.AppDialog, androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        g().c.clearFocus();
    }

    @Override // com.discord.app.AppDialog
    public void onViewBoundOrOnResume() {
        CharSequence charSequence;
        super.onViewBoundOrOnResume();
        TextView textView = g().d;
        m.checkNotNullExpressionValue(textView, "binding.dialogDatePickerTitle");
        Bundle arguments = getArguments();
        if (arguments == null || (charSequence = arguments.getCharSequence("label")) == null) {
            charSequence = "";
        }
        textView.setText(charSequence);
        DatePicker datePicker = g().c;
        m.checkNotNullExpressionValue(datePicker, "binding.dialogDatePickerInput");
        Bundle arguments2 = getArguments();
        long j2 = 0;
        datePicker.setMaxDate(arguments2 != null ? arguments2.getLong("max_date") : 0L);
        DatePicker datePicker2 = g().c;
        m.checkNotNullExpressionValue(datePicker2, "binding.dialogDatePickerInput");
        datePicker2.setMinDate(BirthdayHelper.INSTANCE.subtractYearsFromToday(150));
        Bundle arguments3 = getArguments();
        if (arguments3 != null) {
            j2 = arguments3.getLong("initial_date");
        }
        this.m = j2;
        if (this.l == null) {
            dismiss();
        }
        Calendar calendar = Calendar.getInstance();
        m.checkNotNullExpressionValue(calendar, "calendar");
        calendar.setTimeInMillis(this.m);
        g().c.init(calendar.get(1), calendar.get(2), calendar.get(5), new c());
        g().f140b.setOnClickListener(new d());
    }
}
