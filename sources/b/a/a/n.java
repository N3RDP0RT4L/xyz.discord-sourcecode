package b.a.a;

import andhook.lib.HookHelper;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.i.l;
import com.discord.app.AppDialog;
import com.discord.databinding.DialogSimpleSelectorItemBinding;
import com.discord.utilities.view.recycler.MaxHeightRecyclerView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.utilities.views.SimpleRecyclerAdapter;
import com.google.android.material.button.MaterialButton;
import d0.t.j;
import d0.z.d.a0;
import d0.z.d.k;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: SelectorDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 \u00192\u00020\u0001:\u0002\u001a\u001bB\u0007¢\u0006\u0004\b\u0018\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\bR0\u0010\u0011\u001a\u0010\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\t8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001d\u0010\u0017\u001a\u00020\u00128B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016¨\u0006\u001c"}, d2 = {"Lb/a/a/n;", "Lcom/discord/app/AppDialog;", "Landroid/view/View;", "view", "", "onViewBound", "(Landroid/view/View;)V", "onPause", "()V", "Lkotlin/Function1;", "", "l", "Lkotlin/jvm/functions/Function1;", "getOnSelectedListener", "()Lkotlin/jvm/functions/Function1;", "setOnSelectedListener", "(Lkotlin/jvm/functions/Function1;)V", "onSelectedListener", "Lb/a/i/l;", "m", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "g", "()Lb/a/i/l;", "binding", HookHelper.constructorName, "k", "a", "b", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class n extends AppDialog {
    public static final /* synthetic */ KProperty[] j = {b.d.b.a.a.b0(n.class, "binding", "getBinding()Lcom/discord/databinding/DialogSimpleSelectorBinding;", 0)};
    public static final a k = new a(null);
    public Function1<? super Integer, Unit> l;
    public final FragmentViewBindingDelegate m = FragmentViewBindingDelegateKt.viewBinding$default(this, c.j, null, 2, null);

    /* compiled from: SelectorDialog.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final n a(FragmentManager fragmentManager, CharSequence charSequence, CharSequence[] charSequenceArr, Function1<? super Integer, Unit> function1) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(charSequence, "title");
            m.checkNotNullParameter(charSequenceArr, "options");
            m.checkNotNullParameter(function1, "onSelectedListener");
            n nVar = new n();
            Bundle bundle = new Bundle();
            bundle.putCharSequence("INTENT_DIALOG_TITLE", charSequence);
            bundle.putCharSequenceArray("INTENT_DIALOG_OPTIONS", charSequenceArr);
            nVar.setArguments(bundle);
            nVar.l = function1;
            nVar.show(fragmentManager, a0.getOrCreateKotlinClass(n.class).toString());
            return nVar;
        }
    }

    /* compiled from: SelectorDialog.kt */
    /* loaded from: classes.dex */
    public final class b extends SimpleRecyclerAdapter.ViewHolder<CharSequence> {
        public final DialogSimpleSelectorItemBinding a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ n f48b;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public b(b.a.a.n r2, com.discord.databinding.DialogSimpleSelectorItemBinding r3) {
            /*
                r1 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r3, r0)
                r1.f48b = r2
                android.widget.TextView r2 = r3.a
                java.lang.String r0 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r2, r0)
                r1.<init>(r2)
                r1.a = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.a.a.n.b.<init>(b.a.a.n, com.discord.databinding.DialogSimpleSelectorItemBinding):void");
        }

        @Override // com.discord.utilities.views.SimpleRecyclerAdapter.ViewHolder
        public void bind(CharSequence charSequence) {
            CharSequence charSequence2 = charSequence;
            m.checkNotNullParameter(charSequence2, "data");
            TextView textView = this.a.a;
            m.checkNotNullExpressionValue(textView, "binding.root");
            textView.setText(charSequence2);
            this.a.a.setOnClickListener(new o(this));
        }
    }

    /* compiled from: SelectorDialog.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class c extends k implements Function1<View, l> {
        public static final c j = new c();

        public c() {
            super(1, l.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/DialogSimpleSelectorBinding;", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public l invoke(View view) {
            View view2 = view;
            m.checkNotNullParameter(view2, "p1");
            int i = R.id.dialog_cancel;
            MaterialButton materialButton = (MaterialButton) view2.findViewById(R.id.dialog_cancel);
            if (materialButton != null) {
                i = R.id.dialog_selections;
                MaxHeightRecyclerView maxHeightRecyclerView = (MaxHeightRecyclerView) view2.findViewById(R.id.dialog_selections);
                if (maxHeightRecyclerView != null) {
                    i = R.id.dialog_title;
                    TextView textView = (TextView) view2.findViewById(R.id.dialog_title);
                    if (textView != null) {
                        return new l((LinearLayout) view2, materialButton, maxHeightRecyclerView, textView);
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
        }
    }

    /* compiled from: SelectorDialog.kt */
    /* loaded from: classes.dex */
    public static final class d implements View.OnClickListener {
        public d() {
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            n.this.dismiss();
        }
    }

    /* compiled from: SelectorDialog.kt */
    /* loaded from: classes.dex */
    public static final class e extends o implements Function2<LayoutInflater, ViewGroup, b> {
        public e() {
            super(2);
        }

        @Override // kotlin.jvm.functions.Function2
        public b invoke(LayoutInflater layoutInflater, ViewGroup viewGroup) {
            LayoutInflater layoutInflater2 = layoutInflater;
            ViewGroup viewGroup2 = viewGroup;
            m.checkNotNullParameter(layoutInflater2, "inflater");
            m.checkNotNullParameter(viewGroup2, "parent");
            DialogSimpleSelectorItemBinding a = DialogSimpleSelectorItemBinding.a(layoutInflater2, viewGroup2, false);
            m.checkNotNullExpressionValue(a, "DialogSimpleSelectorItem…(inflater, parent, false)");
            return new b(n.this, a);
        }
    }

    public n() {
        super(R.layout.dialog_simple_selector);
    }

    public final l g() {
        return (l) this.m.getValue((Fragment) this, j[0]);
    }

    @Override // com.discord.app.AppDialog, androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        dismiss();
    }

    @Override // com.discord.app.AppDialog
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        TextView textView = g().d;
        m.checkNotNullExpressionValue(textView, "binding.dialogTitle");
        textView.setText(getArgumentsOrDefault().getCharSequence("INTENT_DIALOG_TITLE", null));
        g().f147b.setOnClickListener(new d());
        CharSequence[] charSequenceArray = getArgumentsOrDefault().getCharSequenceArray("INTENT_DIALOG_OPTIONS");
        if (charSequenceArray != null) {
            MaxHeightRecyclerView maxHeightRecyclerView = g().c;
            m.checkNotNullExpressionValue(maxHeightRecyclerView, "binding.dialogSelections");
            maxHeightRecyclerView.setAdapter(new SimpleRecyclerAdapter(j.asList(charSequenceArray), new e()));
            MaxHeightRecyclerView maxHeightRecyclerView2 = g().c;
            RecyclerView.Adapter adapter = maxHeightRecyclerView2.getAdapter();
            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }
            maxHeightRecyclerView2.setHasFixedSize(false);
            maxHeightRecyclerView2.setNestedScrollingEnabled(false);
            SimpleRecyclerAdapter.Companion companion = SimpleRecyclerAdapter.Companion;
            MaxHeightRecyclerView maxHeightRecyclerView3 = g().c;
            m.checkNotNullExpressionValue(maxHeightRecyclerView3, "binding.dialogSelections");
            companion.addThemedDivider(maxHeightRecyclerView3);
        }
    }
}
