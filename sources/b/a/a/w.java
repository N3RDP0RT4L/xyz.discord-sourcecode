package b.a.a;

import com.discord.app.AppViewModel;
import com.discord.models.user.User;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.PublishSubject;
/* compiled from: WidgetUrgentMessageDialogViewModel.kt */
/* loaded from: classes.dex */
public final class w extends AppViewModel<d> {
    public static final a j = new a(null);
    public final PublishSubject<b> k;
    public final RestAPI l;

    /* compiled from: WidgetUrgentMessageDialogViewModel.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: WidgetUrgentMessageDialogViewModel.kt */
    /* loaded from: classes.dex */
    public static abstract class b {

        /* compiled from: WidgetUrgentMessageDialogViewModel.kt */
        /* loaded from: classes.dex */
        public static final class a extends b {
            public static final a a = new a();

            public a() {
                super(null);
            }
        }

        /* compiled from: WidgetUrgentMessageDialogViewModel.kt */
        /* renamed from: b.a.a.w$b$b  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C0024b extends b {
            public static final C0024b a = new C0024b();

            public C0024b() {
                super(null);
            }
        }

        public b() {
        }

        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: WidgetUrgentMessageDialogViewModel.kt */
    /* loaded from: classes.dex */
    public static final class c {
        public final User a;

        public c(User user) {
            m.checkNotNullParameter(user, "user");
            this.a = user;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof c) && m.areEqual(this.a, ((c) obj).a);
            }
            return true;
        }

        public int hashCode() {
            User user = this.a;
            if (user != null) {
                return user.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("StoreState(user=");
            R.append(this.a);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: WidgetUrgentMessageDialogViewModel.kt */
    /* loaded from: classes.dex */
    public static final class d {
        public final Integer a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f49b;

        public d(Integer num, boolean z2) {
            this.a = num;
            this.f49b = z2;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof d)) {
                return false;
            }
            d dVar = (d) obj;
            return m.areEqual(this.a, dVar.a) && this.f49b == dVar.f49b;
        }

        public int hashCode() {
            Integer num = this.a;
            int hashCode = (num != null ? num.hashCode() : 0) * 31;
            boolean z2 = this.f49b;
            if (z2) {
                z2 = true;
            }
            int i = z2 ? 1 : 0;
            int i2 = z2 ? 1 : 0;
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("ViewState(userFlags=");
            R.append(this.a);
            R.append(", isBusy=");
            return b.d.b.a.a.M(R, this.f49b, ")");
        }
    }

    public w() {
        this(null, null, null, 7);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public w(RestAPI restAPI, StoreUser storeUser, Observable observable, int i) {
        super(null, 1, null);
        Observable observable2;
        RestAPI api = (i & 1) != 0 ? RestAPI.Companion.getApi() : null;
        StoreUser users = (i & 2) != 0 ? StoreStream.Companion.getUsers() : null;
        if ((i & 4) != 0) {
            observable2 = StoreUser.observeMe$default(users, false, 1, null).F(v.j);
            m.checkNotNullExpressionValue(observable2, "storeUser.observeMe().ma…oreState(user = meUser) }");
        } else {
            observable2 = null;
        }
        m.checkNotNullParameter(api, "restAPI");
        m.checkNotNullParameter(users, "storeUser");
        m.checkNotNullParameter(observable2, "storeObservable");
        this.l = api;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable2), this, null, 2, null), w.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new u(this));
        this.k = PublishSubject.k0();
    }
}
