package b.a.a;

import android.view.View;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: FriendRequestDialogs.kt */
/* loaded from: classes.dex */
public final class l extends o implements Function1<View, Unit> {
    public final /* synthetic */ Function1 $onConfirm;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public l(Function1 function1) {
        super(1);
        this.$onConfirm = function1;
    }

    @Override // kotlin.jvm.functions.Function1
    public Unit invoke(View view) {
        m.checkNotNullParameter(view, "<anonymous parameter 0>");
        this.$onConfirm.invoke(Integer.valueOf((int) R.string.friend_request_cancelled));
        return Unit.a;
    }
}
