package b.a.a.b;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.a.i.b1;
import com.discord.app.AppDialog;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: PremiumActivatedDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \u00142\u00020\u0001:\u0001\u0015B\u0007¢\u0006\u0004\b\u0013\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004R*\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u00058\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0006\u0010\u0007\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u001d\u0010\u0012\u001a\u00020\r8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011¨\u0006\u0016"}, d2 = {"Lb/a/a/b/f;", "Lcom/discord/app/AppDialog;", "", "onResume", "()V", "Lkotlin/Function0;", "l", "Lkotlin/jvm/functions/Function0;", "getOnDismiss", "()Lkotlin/jvm/functions/Function0;", "setOnDismiss", "(Lkotlin/jvm/functions/Function0;)V", "onDismiss", "Lb/a/i/b1;", "m", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "g", "()Lb/a/i/b1;", "binding", HookHelper.constructorName, "k", "a", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class f extends AppDialog {
    public static final /* synthetic */ KProperty[] j = {b.d.b.a.a.b0(f.class, "binding", "getBinding()Lcom/discord/databinding/PremiumActivatedDialogBinding;", 0)};
    public static final a k = new a(null);
    public Function0<Unit> l;
    public final FragmentViewBindingDelegate m = FragmentViewBindingDelegateKt.viewBinding$default(this, b.j, null, 2, null);

    /* compiled from: PremiumActivatedDialog.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final void a(FragmentManager fragmentManager, Function0<Unit> function0, boolean z2) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(function0, "onDismiss");
            f fVar = new f();
            fVar.l = function0;
            Bundle bundle = new Bundle();
            bundle.putBoolean("ARG_IS_TIER_1", z2);
            fVar.setArguments(bundle);
            fVar.show(fragmentManager, f.class.getSimpleName());
        }
    }

    /* compiled from: PremiumActivatedDialog.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class b extends k implements Function1<View, b1> {
        public static final b j = new b();

        public b() {
            super(1, b1.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/PremiumActivatedDialogBinding;", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public b1 invoke(View view) {
            View view2 = view;
            m.checkNotNullParameter(view2, "p1");
            int i = R.id.premium_activated_confirm;
            MaterialButton materialButton = (MaterialButton) view2.findViewById(R.id.premium_activated_confirm);
            if (materialButton != null) {
                i = R.id.premium_activated_description;
                TextView textView = (TextView) view2.findViewById(R.id.premium_activated_description);
                if (textView != null) {
                    i = R.id.premium_activated_header_background;
                    ImageView imageView = (ImageView) view2.findViewById(R.id.premium_activated_header_background);
                    if (imageView != null) {
                        i = R.id.premium_activated_logo;
                        ImageView imageView2 = (ImageView) view2.findViewById(R.id.premium_activated_logo);
                        if (imageView2 != null) {
                            i = R.id.premium_activated_title;
                            ImageView imageView3 = (ImageView) view2.findViewById(R.id.premium_activated_title);
                            if (imageView3 != null) {
                                i = R.id.premium_activated_wumpus;
                                ImageView imageView4 = (ImageView) view2.findViewById(R.id.premium_activated_wumpus);
                                if (imageView4 != null) {
                                    return new b1((LinearLayout) view2, materialButton, textView, imageView, imageView2, imageView3, imageView4);
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
        }
    }

    /* compiled from: PremiumActivatedDialog.kt */
    /* loaded from: classes.dex */
    public static final class c implements View.OnClickListener {
        public c() {
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            Function0<Unit> function0 = f.this.l;
            if (function0 != null) {
                function0.invoke();
            }
            f.this.dismiss();
        }
    }

    public f() {
        super(R.layout.premium_activated_dialog);
    }

    public final b1 g() {
        return (b1) this.m.getValue((Fragment) this, j[0]);
    }

    @Override // com.discord.app.AppDialog, androidx.fragment.app.Fragment
    public void onResume() {
        int i;
        super.onResume();
        if (this.l == null) {
            dismiss();
        }
        boolean z2 = getArgumentsOrDefault().getBoolean("ARG_IS_TIER_1");
        int i2 = z2 ? R.drawable.bg_premium_classic_subscription_header : R.drawable.bg_premium_subscription_header;
        int i3 = z2 ? R.drawable.img_logo_nitro_classic : R.drawable.img_logo_discord_nitro;
        int i4 = z2 ? R.drawable.img_wumpus_riding_wheel : R.drawable.img_wumpus_jetpack;
        if (z2) {
            Context requireContext = requireContext();
            m.checkNotNullExpressionValue(requireContext, "requireContext()");
            i = DrawableCompat.getThemedDrawableRes$default(requireContext, (int) R.attr.img_premium_activated_tier_1, 0, 2, (Object) null);
        } else {
            Context requireContext2 = requireContext();
            m.checkNotNullExpressionValue(requireContext2, "requireContext()");
            i = DrawableCompat.getThemedDrawableRes$default(requireContext2, (int) R.attr.img_premium_activated_tier_2, 0, 2, (Object) null);
        }
        int i5 = z2 ? R.string.billing_switch_plan_confirm_tier_1 : R.string.billing_switch_plan_confirm_tier_2;
        g().d.setBackgroundResource(i2);
        g().e.setImageDrawable(ContextCompat.getDrawable(requireContext(), i3));
        g().g.setImageDrawable(ContextCompat.getDrawable(requireContext(), i4));
        ImageView imageView = g().g;
        m.checkNotNullExpressionValue(imageView, "binding.premiumActivatedWumpus");
        imageView.setScaleX(z2 ? 1.0f : -1.0f);
        g().f.setImageDrawable(ContextCompat.getDrawable(requireContext(), i));
        TextView textView = g().c;
        m.checkNotNullExpressionValue(textView, "binding.premiumActivatedDescription");
        textView.setText(getString(i5));
        requireDialog().setCanceledOnTouchOutside(true);
        g().f82b.setOnClickListener(new c());
    }
}
