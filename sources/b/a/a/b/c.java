package b.a.a.b;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.DrawableRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;
import b.a.i.p5;
import b.a.i.u0;
import b.a.k.b;
import com.discord.app.AppDialog;
import com.discord.app.AppFragment;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.view.recycler.ViewPager2ExtensionsKt;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.settings.premium.WidgetSettingsPremium;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.tabs.TabLayout;
import d0.t.n;
import d0.t.u;
import d0.z.d.k;
import d0.z.d.m;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: MultiValuePropPremiumUpsellDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000f\u0018\u0000 \u001d2\u00020\u0001:\u0004\u001e\u001f !B\u0007¢\u0006\u0004\b\u001b\u0010\u001cJ\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\u000e\u001a\u00020\u000b8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\f\u0010\rR\u0016\u0010\u0012\u001a\u00020\u000f8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011R\u001d\u0010\u0018\u001a\u00020\u00138B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017R\u0016\u0010\u001a\u001a\u00020\u000f8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u0011¨\u0006\""}, d2 = {"Lb/a/a/b/c;", "Lcom/discord/app/AppDialog;", "Landroid/os/Bundle;", "savedInstanceState", "", "onCreate", "(Landroid/os/Bundle;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "Lb/a/a/b/c$e;", "m", "Lb/a/a/b/c$e;", "pagerAdapter", "", "j", "()Z", "showOtherPages", "Lb/a/i/u0;", "l", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "h", "()Lb/a/i/u0;", "binding", "i", "showLearnMore", HookHelper.constructorName, "()V", "k", "b", "c", "d", "e", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class c extends AppDialog {
    public static final /* synthetic */ KProperty[] j = {b.d.b.a.a.b0(c.class, "binding", "getBinding()Lcom/discord/databinding/MultiValuePropPremiumUpsellDialogBinding;", 0)};
    public static final b k = new b(null);
    public final FragmentViewBindingDelegate l = FragmentViewBindingDelegateKt.viewBinding$default(this, f.j, null, 2, null);
    public e m;

    /* compiled from: java-style lambda group */
    /* loaded from: classes.dex */
    public static final class a implements View.OnClickListener {
        public final /* synthetic */ int j;
        public final /* synthetic */ Object k;

        public a(int i, Object obj) {
            this.j = i;
            this.k = obj;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            int i = this.j;
            if (i == 0) {
                ((c) this.k).dismiss();
            } else if (i == 1) {
                c.g((c) this.k);
            } else if (i == 2) {
                c.g((c) this.k);
            } else {
                throw null;
            }
        }
    }

    /* compiled from: MultiValuePropPremiumUpsellDialog.kt */
    /* loaded from: classes.dex */
    public static final class b {
        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public static void a(b bVar, FragmentManager fragmentManager, int i, String str, String str2, String str3, String str4, String str5, String str6, boolean z2, boolean z3, int i2) {
            if ((i2 & 4) != 0) {
                str = null;
            }
            if ((i2 & 8) != 0) {
                str2 = null;
            }
            if ((i2 & 16) != 0) {
                str3 = null;
            }
            if ((i2 & 32) != 0) {
                str4 = null;
            }
            if ((i2 & 64) != 0) {
                str5 = null;
            }
            if ((i2 & 128) != 0) {
                str6 = null;
            }
            if ((i2 & 256) != 0) {
                z2 = true;
            }
            if ((i2 & 512) != 0) {
                z3 = true;
            }
            Objects.requireNonNull(bVar);
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            c cVar = new c();
            Bundle bundle = new Bundle();
            bundle.putInt("extra_page_number", i);
            if (str != null) {
                bundle.putString("extra_header_string", str);
            }
            if (str2 != null) {
                bundle.putString("extra_body_text", str2);
            }
            bundle.putString("extra_page_name", str3);
            bundle.putString("extra_section_name", str4);
            bundle.putString("extra_object_name", str5);
            bundle.putString("extra_object_type", str6);
            bundle.putBoolean("extra_show_other_pages", z2);
            bundle.putBoolean("extra_show_learn_more", z3);
            cVar.setArguments(bundle);
            cVar.show(fragmentManager, c.class.getName());
        }
    }

    /* compiled from: MultiValuePropPremiumUpsellDialog.kt */
    /* renamed from: b.a.a.b.c$c  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0015c {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public CharSequence f44b;
        public CharSequence c;

        public C0015c(@DrawableRes int i, CharSequence charSequence, CharSequence charSequence2) {
            m.checkNotNullParameter(charSequence, "headerText");
            m.checkNotNullParameter(charSequence2, "bodyText");
            this.a = i;
            this.f44b = charSequence;
            this.c = charSequence2;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof C0015c)) {
                return false;
            }
            C0015c cVar = (C0015c) obj;
            return this.a == cVar.a && m.areEqual(this.f44b, cVar.f44b) && m.areEqual(this.c, cVar.c);
        }

        public int hashCode() {
            int i = this.a * 31;
            CharSequence charSequence = this.f44b;
            int i2 = 0;
            int hashCode = (i + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
            CharSequence charSequence2 = this.c;
            if (charSequence2 != null) {
                i2 = charSequence2.hashCode();
            }
            return hashCode + i2;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("UpsellData(imageResId=");
            R.append(this.a);
            R.append(", headerText=");
            R.append(this.f44b);
            R.append(", bodyText=");
            return b.d.b.a.a.D(R, this.c, ")");
        }
    }

    /* compiled from: MultiValuePropPremiumUpsellDialog.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \f2\u00020\u0001:\u0001\rB\u0007¢\u0006\u0004\b\u000b\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004R\u001d\u0010\n\u001a\u00020\u00058B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0006\u0010\u0007\u001a\u0004\b\b\u0010\t¨\u0006\u000e"}, d2 = {"b/a/a/b/c$d", "Lcom/discord/app/AppFragment;", "", "onViewBoundOrOnResume", "()V", "Lb/a/i/p5;", "l", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "g", "()Lb/a/i/p5;", "binding", HookHelper.constructorName, "k", "a", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class d extends AppFragment {
        public static final /* synthetic */ KProperty[] j = {b.d.b.a.a.b0(d.class, "binding", "getBinding()Lcom/discord/databinding/WidgetPremiumUpsellBinding;", 0)};
        public static final a k = new a(null);
        public final FragmentViewBindingDelegate l = FragmentViewBindingDelegateKt.viewBinding$default(this, b.j, null, 2, null);

        /* compiled from: MultiValuePropPremiumUpsellDialog.kt */
        /* loaded from: classes.dex */
        public static final class a {
            public a(DefaultConstructorMarker defaultConstructorMarker) {
            }
        }

        /* compiled from: MultiValuePropPremiumUpsellDialog.kt */
        /* loaded from: classes.dex */
        public static final /* synthetic */ class b extends k implements Function1<View, p5> {
            public static final b j = new b();

            public b() {
                super(1, p5.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetPremiumUpsellBinding;", 0);
            }

            @Override // kotlin.jvm.functions.Function1
            public p5 invoke(View view) {
                View view2 = view;
                m.checkNotNullParameter(view2, "p1");
                int i = R.id.premium_upsell_body;
                TextView textView = (TextView) view2.findViewById(R.id.premium_upsell_body);
                if (textView != null) {
                    i = R.id.premium_upsell_header;
                    TextView textView2 = (TextView) view2.findViewById(R.id.premium_upsell_header);
                    if (textView2 != null) {
                        i = R.id.premium_upsell_img;
                        ImageView imageView = (ImageView) view2.findViewById(R.id.premium_upsell_img);
                        if (imageView != null) {
                            return new p5((LinearLayout) view2, textView, textView2, imageView);
                        }
                    }
                }
                throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
            }
        }

        public d() {
            super(R.layout.widget_premium_upsell);
        }

        public final p5 g() {
            return (p5) this.l.getValue((Fragment) this, j[0]);
        }

        @Override // com.discord.app.AppFragment
        public void onViewBoundOrOnResume() {
            CharSequence charSequence;
            String string;
            super.onViewBoundOrOnResume();
            ImageView imageView = g().d;
            Bundle arguments = getArguments();
            imageView.setImageResource(arguments != null ? arguments.getInt("extra_image_id") : 0);
            TextView textView = g().c;
            m.checkNotNullExpressionValue(textView, "binding.premiumUpsellHeader");
            Bundle arguments2 = getArguments();
            textView.setText(arguments2 != null ? arguments2.getString("extra_header_string") : null);
            TextView textView2 = g().f177b;
            m.checkNotNullExpressionValue(textView2, "binding.premiumUpsellBody");
            Bundle arguments3 = getArguments();
            if (arguments3 == null || (string = arguments3.getString("extra_body_text")) == null) {
                charSequence = "";
            } else {
                charSequence = b.a.k.b.g(string, new Object[0], (r3 & 2) != 0 ? b.e.j : null);
            }
            textView2.setText(charSequence);
        }
    }

    /* compiled from: MultiValuePropPremiumUpsellDialog.kt */
    /* loaded from: classes.dex */
    public static final class e extends FragmentStateAdapter {
        public final List<C0015c> a;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public e(Fragment fragment, List<C0015c> list) {
            super(fragment);
            m.checkNotNullParameter(fragment, "fragment");
            m.checkNotNullParameter(list, "pages");
            this.a = list;
        }

        @Override // androidx.viewpager2.adapter.FragmentStateAdapter
        public Fragment createFragment(int i) {
            C0015c cVar = this.a.get(i);
            d.a aVar = d.k;
            int i2 = cVar.a;
            CharSequence charSequence = cVar.f44b;
            CharSequence charSequence2 = cVar.c;
            Objects.requireNonNull(aVar);
            m.checkNotNullParameter(charSequence, "headerText");
            m.checkNotNullParameter(charSequence2, "bodyText");
            Bundle bundle = new Bundle();
            bundle.putInt("extra_image_id", i2);
            bundle.putString("extra_header_string", charSequence.toString());
            bundle.putString("extra_body_text", charSequence2.toString());
            d dVar = new d();
            dVar.setArguments(bundle);
            return dVar;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public int getItemCount() {
            return this.a.size();
        }
    }

    /* compiled from: MultiValuePropPremiumUpsellDialog.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class f extends k implements Function1<View, u0> {
        public static final f j = new f();

        public f() {
            super(1, u0.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/MultiValuePropPremiumUpsellDialogBinding;", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public u0 invoke(View view) {
            View view2 = view;
            m.checkNotNullParameter(view2, "p1");
            int i = R.id.premium_upsell_button_container;
            LinearLayout linearLayout = (LinearLayout) view2.findViewById(R.id.premium_upsell_button_container);
            if (linearLayout != null) {
                i = R.id.premium_upsell_close;
                MaterialButton materialButton = (MaterialButton) view2.findViewById(R.id.premium_upsell_close);
                if (materialButton != null) {
                    i = R.id.premium_upsell_divider;
                    View findViewById = view2.findViewById(R.id.premium_upsell_divider);
                    if (findViewById != null) {
                        i = R.id.premium_upsell_dots;
                        TabLayout tabLayout = (TabLayout) view2.findViewById(R.id.premium_upsell_dots);
                        if (tabLayout != null) {
                            i = R.id.premium_upsell_get_premium;
                            MaterialButton materialButton2 = (MaterialButton) view2.findViewById(R.id.premium_upsell_get_premium);
                            if (materialButton2 != null) {
                                i = R.id.premium_upsell_learn_more;
                                MaterialButton materialButton3 = (MaterialButton) view2.findViewById(R.id.premium_upsell_learn_more);
                                if (materialButton3 != null) {
                                    i = R.id.premium_upsell_viewpager;
                                    ViewPager2 viewPager2 = (ViewPager2) view2.findViewById(R.id.premium_upsell_viewpager);
                                    if (viewPager2 != null) {
                                        return new u0((RelativeLayout) view2, linearLayout, materialButton, findViewById, tabLayout, materialButton2, materialButton3, viewPager2);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
        }
    }

    public c() {
        super(R.layout.multi_value_prop_premium_upsell_dialog);
    }

    public static final void g(c cVar) {
        Bundle arguments = cVar.getArguments();
        String string = arguments != null ? arguments.getString("extra_page_name") : null;
        Bundle arguments2 = cVar.getArguments();
        String string2 = arguments2 != null ? arguments2.getString("extra_section_name") : null;
        Bundle arguments3 = cVar.getArguments();
        String string3 = arguments3 != null ? arguments3.getString("extra_object_name") : null;
        Bundle arguments4 = cVar.getArguments();
        AnalyticsTracker.INSTANCE.premiumSettingsOpened(new Traits.Location(string, string2, string3, arguments4 != null ? arguments4.getString("extra_object_type") : null, null, 16, null));
        WidgetSettingsPremium.Companion companion = WidgetSettingsPremium.Companion;
        Context requireContext = cVar.requireContext();
        m.checkNotNullExpressionValue(requireContext, "requireContext()");
        WidgetSettingsPremium.Companion.launch$default(companion, requireContext, null, null, 6, null);
        cVar.dismiss();
    }

    public final u0 h() {
        return (u0) this.l.getValue((Fragment) this, j[0]);
    }

    public final boolean i() {
        Bundle arguments = getArguments();
        return arguments != null && arguments.getBoolean("extra_show_learn_more");
    }

    public final boolean j() {
        Bundle arguments = getArguments();
        return arguments != null && arguments.getBoolean("extra_show_other_pages");
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        CharSequence e2;
        CharSequence e3;
        CharSequence e4;
        CharSequence e5;
        String string;
        String str;
        super.onCreate(bundle);
        AppDialog.hideKeyboard$default(this, null, 1, null);
        String string2 = getString(R.string.premium_upsell_tag_passive_mobile);
        m.checkNotNullExpressionValue(string2, "getString(R.string.premi…psell_tag_passive_mobile)");
        String string3 = getString(R.string.premium_upsell_tag_description_mobile);
        m.checkNotNullExpressionValue(string3, "getString(R.string.premi…l_tag_description_mobile)");
        String string4 = getString(R.string.premium_upsell_emoji_passive_mobile);
        m.checkNotNullExpressionValue(string4, "getString(R.string.premi…ell_emoji_passive_mobile)");
        String string5 = getString(R.string.premium_upsell_emoji_description_mobile);
        m.checkNotNullExpressionValue(string5, "getString(R.string.premi…emoji_description_mobile)");
        String string6 = getString(R.string.premium_upsell_animated_emojis_passive_mobile);
        m.checkNotNullExpressionValue(string6, "getString(R.string.premi…ed_emojis_passive_mobile)");
        String string7 = getString(R.string.premium_upsell_animated_emojis_description_mobile);
        m.checkNotNullExpressionValue(string7, "getString(R.string.premi…mojis_description_mobile)");
        e2 = b.a.k.b.e(this, R.string.premium_upsell_upload_passive_mobile, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        e3 = b.a.k.b.e(this, R.string.file_upload_limit_standard, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        e4 = b.a.k.b.e(this, R.string.file_upload_limit_premium_tier_2, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        e5 = b.a.k.b.e(this, R.string.premium_upsell_upload_description_mobile, new Object[]{e3, e4}, (r4 & 4) != 0 ? b.a.j : null);
        String string8 = getString(R.string.premium_upsell_animated_avatar_passive_mobile);
        m.checkNotNullExpressionValue(string8, "getString(R.string.premi…ed_avatar_passive_mobile)");
        String string9 = getString(R.string.premium_upsell_animated_avatar_description_mobile);
        m.checkNotNullExpressionValue(string9, "getString(R.string.premi…vatar_description_mobile)");
        String string10 = getString(R.string.premium_upsell_badge_passive_mobile);
        m.checkNotNullExpressionValue(string10, "getString(R.string.premi…ell_badge_passive_mobile)");
        String string11 = getString(R.string.premium_upsell_badge_description_mobile);
        m.checkNotNullExpressionValue(string11, "getString(R.string.premi…badge_description_mobile)");
        List listOf = n.listOf((Object[]) new C0015c[]{new C0015c(R.drawable.img_tag_upsell, string2, string3), new C0015c(R.drawable.img_global_emoji_upsell, string4, string5), new C0015c(R.drawable.img_animated_emoji_upsell, string6, string7), new C0015c(R.drawable.img_upload_upsell, e2, e5), new C0015c(R.drawable.img_avatar_upsell, string8, string9), new C0015c(R.drawable.img_badge_upsell, string10, string11)});
        Bundle arguments = getArguments();
        int i = arguments != null ? arguments.getInt("extra_page_number") : 0;
        Bundle arguments2 = getArguments();
        String str2 = "";
        if (arguments2 != null && arguments2.containsKey("extra_header_string")) {
            C0015c cVar = (C0015c) listOf.get(i);
            Bundle arguments3 = getArguments();
            if (arguments3 == null || (str = arguments3.getString("extra_header_string")) == null) {
                str = str2;
            }
            Objects.requireNonNull(cVar);
            m.checkNotNullParameter(str, "<set-?>");
            cVar.f44b = str;
        }
        Bundle arguments4 = getArguments();
        if (arguments4 != null && arguments4.containsKey("extra_body_text")) {
            C0015c cVar2 = (C0015c) listOf.get(i);
            Bundle arguments5 = getArguments();
            if (!(arguments5 == null || (string = arguments5.getString("extra_body_text")) == null)) {
                str2 = string;
            }
            Objects.requireNonNull(cVar2);
            m.checkNotNullParameter(str2, "<set-?>");
            cVar2.c = str2;
        }
        C0015c cVar3 = (C0015c) listOf.get(i);
        List mutableListOf = n.mutableListOf(cVar3);
        if (j()) {
            mutableListOf.addAll(u.minus(listOf, cVar3));
        }
        this.m = new e(this, mutableListOf);
    }

    @Override // com.discord.app.AppDialog
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        ViewPager2 viewPager2 = h().f;
        m.checkNotNullExpressionValue(viewPager2, "binding.premiumUpsellViewpager");
        e eVar = this.m;
        if (eVar == null) {
            m.throwUninitializedPropertyAccessException("pagerAdapter");
        }
        viewPager2.setAdapter(eVar);
        TabLayout tabLayout = h().c;
        m.checkNotNullExpressionValue(tabLayout, "binding.premiumUpsellDots");
        int i = 8;
        tabLayout.setVisibility(j() ? 0 : 8);
        TabLayout tabLayout2 = h().c;
        m.checkNotNullExpressionValue(tabLayout2, "binding.premiumUpsellDots");
        ViewPager2 viewPager22 = h().f;
        m.checkNotNullExpressionValue(viewPager22, "binding.premiumUpsellViewpager");
        ViewPager2ExtensionsKt.setUpWithViewPager2$default(tabLayout2, viewPager22, null, 2, null);
        h().f204b.setOnClickListener(new a(0, this));
        MaterialButton materialButton = h().e;
        m.checkNotNullExpressionValue(materialButton, "binding.premiumUpsellLearnMore");
        materialButton.setVisibility(i() ? 0 : 8);
        h().e.setOnClickListener(new a(1, this));
        MaterialButton materialButton2 = h().d;
        m.checkNotNullExpressionValue(materialButton2, "binding.premiumUpsellGetPremium");
        if (!i()) {
            i = 0;
        }
        materialButton2.setVisibility(i);
        h().d.setOnClickListener(new a(2, this));
    }
}
