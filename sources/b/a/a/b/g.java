package b.a.a.b;

import andhook.lib.HookHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import b.a.i.c1;
import b.a.k.b;
import com.discord.app.AppDialog;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: PremiumAndGuildBoostActivatedDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u0000 \u00142\u00020\u0001:\u0001\u0015B\u0007¢\u0006\u0004\b\u0013\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004R\u001d\u0010\n\u001a\u00020\u00058B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0006\u0010\u0007\u001a\u0004\b\b\u0010\tR*\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u000b8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011¨\u0006\u0016"}, d2 = {"Lb/a/a/b/g;", "Lcom/discord/app/AppDialog;", "", "onViewBoundOrOnResume", "()V", "Lb/a/i/c1;", "m", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lb/a/i/c1;", "binding", "Lkotlin/Function0;", "l", "Lkotlin/jvm/functions/Function0;", "getOnDismiss", "()Lkotlin/jvm/functions/Function0;", "setOnDismiss", "(Lkotlin/jvm/functions/Function0;)V", "onDismiss", HookHelper.constructorName, "k", "a", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class g extends AppDialog {
    public static final /* synthetic */ KProperty[] j = {b.d.b.a.a.b0(g.class, "binding", "getBinding()Lcom/discord/databinding/PremiumAndGuildBoostActivatedDialogBinding;", 0)};
    public static final a k = new a(null);
    public Function0<Unit> l;
    public final FragmentViewBindingDelegate m = FragmentViewBindingDelegateKt.viewBinding$default(this, b.j, null, 2, null);

    /* compiled from: PremiumAndGuildBoostActivatedDialog.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: PremiumAndGuildBoostActivatedDialog.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class b extends k implements Function1<View, c1> {
        public static final b j = new b();

        public b() {
            super(1, c1.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/PremiumAndGuildBoostActivatedDialogBinding;", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public c1 invoke(View view) {
            View view2 = view;
            m.checkNotNullParameter(view2, "p1");
            int i = R.id.premium_activated_confirm;
            MaterialButton materialButton = (MaterialButton) view2.findViewById(R.id.premium_activated_confirm);
            if (materialButton != null) {
                i = R.id.premium_and_guild_boost_activated_text;
                TextView textView = (TextView) view2.findViewById(R.id.premium_and_guild_boost_activated_text);
                if (textView != null) {
                    return new c1((LinearLayout) view2, materialButton, textView);
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
        }
    }

    /* compiled from: PremiumAndGuildBoostActivatedDialog.kt */
    /* loaded from: classes.dex */
    public static final class c implements View.OnClickListener {
        public c() {
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            Function0<Unit> function0 = g.this.l;
            if (function0 != null) {
                function0.invoke();
            }
            g.this.dismiss();
        }
    }

    public g() {
        super(R.layout.premium_and_guild_boost_activated_dialog);
    }

    @Override // com.discord.app.AppDialog
    public void onViewBoundOrOnResume() {
        String str;
        super.onViewBoundOrOnResume();
        if (this.l == null) {
            dismiss();
        }
        Bundle arguments = getArguments();
        if (arguments == null || (str = arguments.getString("extra_plan_text")) == null) {
            str = "";
        }
        m.checkNotNullExpressionValue(str, "arguments?.getString(EXTRA_PLAN_NAME) ?: \"\"");
        FragmentViewBindingDelegate fragmentViewBindingDelegate = this.m;
        KProperty<?>[] kPropertyArr = j;
        TextView textView = ((c1) fragmentViewBindingDelegate.getValue((Fragment) this, kPropertyArr[0])).c;
        m.checkNotNullExpressionValue(textView, "binding.premiumAndGuildBoostActivatedText");
        b.a.k.b.m(textView, R.string.billing_premium_and_premium_guild_plan_activated, new Object[]{str}, (r4 & 4) != 0 ? b.g.j : null);
        requireDialog().setCanceledOnTouchOutside(true);
        ((c1) this.m.getValue((Fragment) this, kPropertyArr[0])).f89b.setOnClickListener(new c());
    }
}
