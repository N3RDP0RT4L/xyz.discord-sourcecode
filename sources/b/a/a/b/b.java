package b.a.a.b;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import b.a.i.u;
import com.discord.app.AppDialog;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.servers.guildboost.WidgetGuildBoost;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: GuildBoostUpsellDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 \u000f2\u00020\u0001:\u0001\u0010B\u0007¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006R\u001d\u0010\f\u001a\u00020\u00078B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000b¨\u0006\u0011"}, d2 = {"Lb/a/a/b/b;", "Lcom/discord/app/AppDialog;", "Landroid/view/View;", "view", "", "onViewBound", "(Landroid/view/View;)V", "Lb/a/i/u;", "l", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "g", "()Lb/a/i/u;", "binding", HookHelper.constructorName, "()V", "k", "b", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class b extends AppDialog {
    public static final /* synthetic */ KProperty[] j = {b.d.b.a.a.b0(b.class, "binding", "getBinding()Lcom/discord/databinding/GuildBoostUpsellDialogBinding;", 0)};
    public static final C0014b k = new C0014b(null);
    public final FragmentViewBindingDelegate l = FragmentViewBindingDelegateKt.viewBinding$default(this, c.j, null, 2, null);

    /* compiled from: java-style lambda group */
    /* loaded from: classes.dex */
    public static final class a implements View.OnClickListener {
        public final /* synthetic */ int j;
        public final /* synthetic */ Object k;

        public a(int i, Object obj) {
            this.j = i;
            this.k = obj;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            int i = this.j;
            if (i == 0) {
                ((b) this.k).dismiss();
            } else if (i == 1) {
                ((b) this.k).dismiss();
            } else {
                throw null;
            }
        }
    }

    /* compiled from: GuildBoostUpsellDialog.kt */
    /* renamed from: b.a.a.b.b$b  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class C0014b {
        public C0014b(DefaultConstructorMarker defaultConstructorMarker) {
        }

        /* JADX WARN: Removed duplicated region for block: B:19:0x0041  */
        /* JADX WARN: Removed duplicated region for block: B:21:0x004c  */
        /* JADX WARN: Removed duplicated region for block: B:28:0x0076  */
        /* JADX WARN: Removed duplicated region for block: B:29:0x007c  */
        /* JADX WARN: Removed duplicated region for block: B:31:0x007f  */
        /* JADX WARN: Removed duplicated region for block: B:32:0x0085  */
        /* JADX WARN: Removed duplicated region for block: B:34:0x0088  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public final void a(androidx.fragment.app.FragmentManager r13, long r14, java.lang.Long r16, java.lang.Integer r17, com.discord.utilities.analytics.Traits.Location r18) {
            /*
                r12 = this;
                r0 = r13
                java.lang.String r1 = "fragmentManager"
                d0.z.d.m.checkNotNullParameter(r13, r1)
                b.a.a.b.b r1 = new b.a.a.b.b
                r1.<init>()
                r2 = 0
                if (r17 != 0) goto Lf
                goto L1a
            Lf:
                int r3 = r17.intValue()
                r4 = 1
                if (r3 != r4) goto L1a
                java.lang.String r3 = "tier_1"
                goto L35
            L1a:
                if (r17 != 0) goto L1d
                goto L28
            L1d:
                int r3 = r17.intValue()
                r4 = 2
                if (r3 != r4) goto L28
                java.lang.String r3 = "tier_2"
                goto L35
            L28:
                if (r17 != 0) goto L2b
                goto L37
            L2b:
                int r3 = r17.intValue()
                r4 = 3
                if (r3 != r4) goto L37
                java.lang.String r3 = "tier_3"
            L35:
                r8 = r3
                goto L38
            L37:
                r8 = r2
            L38:
                java.lang.String r3 = "extra_guild_id"
                r4 = r14
                android.os.Bundle r3 = b.d.b.a.a.I(r3, r14)
                if (r16 == 0) goto L4a
                long r4 = r16.longValue()
                java.lang.String r6 = "extra_channel_id"
                r3.putLong(r6, r4)
            L4a:
                if (r8 == 0) goto L51
                java.lang.String r4 = "extra_object_type"
                r3.putString(r4, r8)
            L51:
                if (r18 == 0) goto L5e
                java.lang.String r4 = r18.getPage()
                if (r4 == 0) goto L5e
                java.lang.String r5 = "extra_page"
                r3.putString(r5, r4)
            L5e:
                r1.setArguments(r3)
                java.lang.Class<b.a.a.b.b> r3 = b.a.a.b.b.class
                java.lang.String r3 = r3.getSimpleName()
                r1.show(r13, r3)
                com.discord.utilities.analytics.AnalyticsTracker r0 = com.discord.utilities.analytics.AnalyticsTracker.INSTANCE
                java.lang.String r1 = "Premium Guild Upsell Modal - Tier "
                java.lang.String r1 = b.d.b.a.a.v(r1, r8)
                com.discord.utilities.analytics.Traits$Location r3 = new com.discord.utilities.analytics.Traits$Location
                if (r18 == 0) goto L7c
                java.lang.String r4 = r18.getPage()
                r5 = r4
                goto L7d
            L7c:
                r5 = r2
            L7d:
                if (r18 == 0) goto L85
                java.lang.String r4 = r18.get_object()
                r7 = r4
                goto L86
            L85:
                r7 = r2
            L86:
                if (r18 == 0) goto L8c
                java.lang.String r2 = r18.getSection()
            L8c:
                r6 = r2
                r9 = 0
                r10 = 16
                r11 = 0
                r4 = r3
                r4.<init>(r5, r6, r7, r8, r9, r10, r11)
                r0.openModal(r1, r3)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.a.a.b.b.C0014b.a(androidx.fragment.app.FragmentManager, long, java.lang.Long, java.lang.Integer, com.discord.utilities.analytics.Traits$Location):void");
        }
    }

    /* compiled from: GuildBoostUpsellDialog.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class c extends k implements Function1<View, u> {
        public static final c j = new c();

        public c() {
            super(1, u.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/GuildBoostUpsellDialogBinding;", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public u invoke(View view) {
            View view2 = view;
            m.checkNotNullParameter(view2, "p1");
            int i = R.id.guild_boost_upsell_close;
            ImageView imageView = (ImageView) view2.findViewById(R.id.guild_boost_upsell_close);
            if (imageView != null) {
                i = R.id.guild_boost_upsell_close_button;
                TextView textView = (TextView) view2.findViewById(R.id.guild_boost_upsell_close_button);
                if (textView != null) {
                    i = R.id.guild_boost_upsell_subscribe_button;
                    MaterialButton materialButton = (MaterialButton) view2.findViewById(R.id.guild_boost_upsell_subscribe_button);
                    if (materialButton != null) {
                        return new u((RelativeLayout) view2, imageView, textView, materialButton);
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
        }
    }

    /* compiled from: GuildBoostUpsellDialog.kt */
    /* loaded from: classes.dex */
    public static final class d implements View.OnClickListener {
        public final /* synthetic */ long k;
        public final /* synthetic */ Long l;
        public final /* synthetic */ String m;
        public final /* synthetic */ String n;

        public d(long j, Long l, String str, String str2) {
            this.k = j;
            this.l = l;
            this.m = str;
            this.n = str2;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            AnalyticsTracker.INSTANCE.guildBoostPromotionOpened(this.k, new Traits.Location(this.m, Traits.Location.Section.PREMIUM_GUILD_UPSELL_MODAL, Traits.Location.Obj.BUTTON_CTA, this.n, null, 16, null), this.l);
            WidgetGuildBoost.Companion companion = WidgetGuildBoost.Companion;
            Context requireContext = b.this.requireContext();
            m.checkNotNullExpressionValue(requireContext, "requireContext()");
            companion.create(requireContext, this.k);
            b.this.dismiss();
        }
    }

    public b() {
        super(R.layout.guild_boost_upsell_dialog);
    }

    public final u g() {
        return (u) this.l.getValue((Fragment) this, j[0]);
    }

    @Override // com.discord.app.AppDialog
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        long j2 = getArgumentsOrDefault().getLong("extra_guild_id", -1L);
        Bundle arguments = getArguments();
        Long valueOf = arguments != null ? Long.valueOf(arguments.getLong("extra_channel_id")) : null;
        Bundle arguments2 = getArguments();
        String string = arguments2 != null ? arguments2.getString("extra_object_type") : null;
        Bundle arguments3 = getArguments();
        g().d.setOnClickListener(new d(j2, valueOf, arguments3 != null ? arguments3.getString("extra_page") : null, string));
        g().f203b.setOnClickListener(new a(0, this));
        g().c.setOnClickListener(new a(1, this));
    }
}
