package b.a.a.b;

import andhook.lib.HookHelper;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import b.a.i.t;
import b.a.k.b;
import com.discord.app.AppDialog;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: UpgradeGuildBoostDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u000b\u0018\u0000 )2\u00020\u0001:\u0001*B\u0007¢\u0006\u0004\b(\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0004R*\u0010\r\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u00068\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0007\u0010\b\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR$\u0010\u0015\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R*\u0010\u0019\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u00068\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0016\u0010\b\u001a\u0004\b\u0017\u0010\n\"\u0004\b\u0018\u0010\fR\u001d\u0010\u001f\u001a\u00020\u001a8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\"\u0010'\u001a\u00020 8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010$\"\u0004\b%\u0010&¨\u0006+"}, d2 = {"Lb/a/a/b/a;", "Lcom/discord/app/AppDialog;", "", "onStart", "()V", "onViewBoundOrOnResume", "Lkotlin/Function0;", "l", "Lkotlin/jvm/functions/Function0;", "getOnContinueClickListener", "()Lkotlin/jvm/functions/Function0;", "setOnContinueClickListener", "(Lkotlin/jvm/functions/Function0;)V", "onContinueClickListener", "", "n", "Ljava/lang/String;", "getUpgradePrice", "()Ljava/lang/String;", "setUpgradePrice", "(Ljava/lang/String;)V", "upgradePrice", "m", "getOnUpgradeClickListener", "setOnUpgradeClickListener", "onUpgradeClickListener", "Lb/a/i/t;", "p", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "g", "()Lb/a/i/t;", "binding", "", "o", "Z", "getHasBoostPlan", "()Z", "setHasBoostPlan", "(Z)V", "hasBoostPlan", HookHelper.constructorName, "k", "b", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class a extends AppDialog {
    public static final /* synthetic */ KProperty[] j = {b.d.b.a.a.b0(a.class, "binding", "getBinding()Lcom/discord/databinding/GuildBoostUpgradeDialogBinding;", 0)};
    public static final b k = new b(null);
    public Function0<Unit> l;
    public Function0<Unit> m;
    public String n;
    public boolean o;
    public final FragmentViewBindingDelegate p = FragmentViewBindingDelegateKt.viewBinding$default(this, c.j, null, 2, null);

    /* compiled from: java-style lambda group */
    /* renamed from: b.a.a.b.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class View$OnClickListenerC0013a implements View.OnClickListener {
        public final /* synthetic */ int j;
        public final /* synthetic */ Object k;

        public View$OnClickListenerC0013a(int i, Object obj) {
            this.j = i;
            this.k = obj;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            int i = this.j;
            if (i == 0) {
                Function0<Unit> function0 = ((a) this.k).l;
                if (function0 != null) {
                    function0.invoke();
                }
                ((a) this.k).dismiss();
            } else if (i == 1) {
                Function0<Unit> function02 = ((a) this.k).m;
                if (function02 != null) {
                    function02.invoke();
                }
                ((a) this.k).dismiss();
            } else if (i == 2) {
                ((a) this.k).dismiss();
            } else {
                throw null;
            }
        }
    }

    /* compiled from: UpgradeGuildBoostDialog.kt */
    /* loaded from: classes.dex */
    public static final class b {
        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: UpgradeGuildBoostDialog.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class c extends k implements Function1<View, t> {
        public static final c j = new c();

        public c() {
            super(1, t.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/GuildBoostUpgradeDialogBinding;", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public t invoke(View view) {
            View view2 = view;
            m.checkNotNullParameter(view2, "p1");
            int i = R.id.continue_button;
            TextView textView = (TextView) view2.findViewById(R.id.continue_button);
            if (textView != null) {
                i = R.id.premium_upsell_close;
                ImageView imageView = (ImageView) view2.findViewById(R.id.premium_upsell_close);
                if (imageView != null) {
                    i = R.id.premium_upsell_description;
                    TextView textView2 = (TextView) view2.findViewById(R.id.premium_upsell_description);
                    if (textView2 != null) {
                        i = R.id.premium_upsell_server_perk;
                        TextView textView3 = (TextView) view2.findViewById(R.id.premium_upsell_server_perk);
                        if (textView3 != null) {
                            i = R.id.upgrade_button;
                            MaterialButton materialButton = (MaterialButton) view2.findViewById(R.id.upgrade_button);
                            if (materialButton != null) {
                                return new t((RelativeLayout) view2, textView, imageView, textView2, textView3, materialButton);
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
        }
    }

    public a() {
        super(R.layout.guild_boost_upgrade_dialog);
    }

    public final t g() {
        return (t) this.p.getValue((Fragment) this, j[0]);
    }

    @Override // com.discord.app.AppDialog, androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        Dialog requireDialog = requireDialog();
        m.checkNotNullExpressionValue(requireDialog, "requireDialog()");
        Window window = requireDialog.getWindow();
        if (window != null) {
            window.setLayout(-1, -2);
        }
    }

    @Override // com.discord.app.AppDialog
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        if (this.l == null || this.m == null) {
            dismiss();
        }
        requireDialog().setCanceledOnTouchOutside(true);
        TextView textView = g().d;
        m.checkNotNullExpressionValue(textView, "binding.premiumUpsellDescription");
        b.a.k.b.m(textView, R.string.premium_upsell_feature_pretext, new Object[]{this.n}, (r4 & 4) != 0 ? b.g.j : null);
        TextView textView2 = g().e;
        m.checkNotNullExpressionValue(textView2, "binding.premiumUpsellServerPerk");
        Context requireContext = requireContext();
        m.checkNotNullExpressionValue(requireContext, "requireContext()");
        b.a.k.b.m(textView2, R.string.premium_upsell_feature_free_guild_subscription, new Object[]{StringResourceUtilsKt.getI18nPluralString(requireContext, R.plurals.premium_upsell_feature_free_guild_subscription_numFreeGuildSubscriptions, 2, 2)}, (r4 & 4) != 0 ? b.g.j : null);
        TextView textView3 = g().f197b;
        m.checkNotNullExpressionValue(textView3, "binding.continueButton");
        textView3.setVisibility(this.o ^ true ? 0 : 8);
        g().f197b.setOnClickListener(new View$OnClickListenerC0013a(0, this));
        g().f.setOnClickListener(new View$OnClickListenerC0013a(1, this));
        g().c.setOnClickListener(new View$OnClickListenerC0013a(2, this));
    }
}
