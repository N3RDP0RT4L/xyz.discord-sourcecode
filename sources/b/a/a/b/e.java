package b.a.a.b;

import andhook.lib.HookHelper;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import b.a.i.d1;
import b.a.k.b;
import com.discord.app.AppDialog;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import java.text.NumberFormat;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: UpgradePremiumYearlyDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u0000 )2\u00020\u0001:\u0001*B\u0007¢\u0006\u0004\b(\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0004R$\u0010\r\u001a\u0004\u0018\u00010\u00068\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0007\u0010\b\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\"\u0010\u0015\u001a\u00020\u000e8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R*\u0010\u001d\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u00168\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u001d\u0010#\u001a\u00020\u001e8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"R*\u0010'\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u00168\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b$\u0010\u0018\u001a\u0004\b%\u0010\u001a\"\u0004\b&\u0010\u001c¨\u0006+"}, d2 = {"Lb/a/a/b/e;", "Lcom/discord/app/AppDialog;", "", "onStart", "()V", "onViewBoundOrOnResume", "", "n", "Ljava/lang/String;", "getUpgradePrice", "()Ljava/lang/String;", "setUpgradePrice", "(Ljava/lang/String;)V", "upgradePrice", "", "o", "Z", "getHasMonthlyPlan", "()Z", "setHasMonthlyPlan", "(Z)V", "hasMonthlyPlan", "Lkotlin/Function0;", "l", "Lkotlin/jvm/functions/Function0;", "getOnMonthlyClickListener", "()Lkotlin/jvm/functions/Function0;", "setOnMonthlyClickListener", "(Lkotlin/jvm/functions/Function0;)V", "onMonthlyClickListener", "Lb/a/i/d1;", "p", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "g", "()Lb/a/i/d1;", "binding", "m", "getOnYearlyClickListener", "setOnYearlyClickListener", "onYearlyClickListener", HookHelper.constructorName, "k", "b", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class e extends AppDialog {
    public static final /* synthetic */ KProperty[] j = {b.d.b.a.a.b0(e.class, "binding", "getBinding()Lcom/discord/databinding/PremiumUpgradeYearlyDialogBinding;", 0)};
    public static final b k = new b(null);
    public Function0<Unit> l;
    public Function0<Unit> m;
    public String n;
    public boolean o;
    public final FragmentViewBindingDelegate p = FragmentViewBindingDelegateKt.viewBinding$default(this, c.j, null, 2, null);

    /* compiled from: java-style lambda group */
    /* loaded from: classes2.dex */
    public static final class a implements View.OnClickListener {
        public final /* synthetic */ int j;
        public final /* synthetic */ Object k;

        public a(int i, Object obj) {
            this.j = i;
            this.k = obj;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            int i = this.j;
            if (i == 0) {
                Function0<Unit> function0 = ((e) this.k).l;
                if (function0 != null) {
                    function0.invoke();
                }
                ((e) this.k).dismiss();
            } else if (i == 1) {
                Function0<Unit> function02 = ((e) this.k).m;
                if (function02 != null) {
                    function02.invoke();
                }
                ((e) this.k).dismiss();
            } else {
                throw null;
            }
        }
    }

    /* compiled from: UpgradePremiumYearlyDialog.kt */
    /* loaded from: classes.dex */
    public static final class b {
        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: UpgradePremiumYearlyDialog.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class c extends k implements Function1<View, d1> {
        public static final c j = new c();

        public c() {
            super(1, d1.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/PremiumUpgradeYearlyDialogBinding;", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public d1 invoke(View view) {
            View view2 = view;
            m.checkNotNullParameter(view2, "p1");
            int i = R.id.premium_upsell_header;
            ImageView imageView = (ImageView) view2.findViewById(R.id.premium_upsell_header);
            if (imageView != null) {
                i = R.id.premium_upsell_yearly_description;
                TextView textView = (TextView) view2.findViewById(R.id.premium_upsell_yearly_description);
                if (textView != null) {
                    i = R.id.premium_upsell_yearly_title;
                    TextView textView2 = (TextView) view2.findViewById(R.id.premium_upsell_yearly_title);
                    if (textView2 != null) {
                        i = R.id.purchase_premium_monthly;
                        TextView textView3 = (TextView) view2.findViewById(R.id.purchase_premium_monthly);
                        if (textView3 != null) {
                            i = R.id.purchase_premium_yearly;
                            MaterialButton materialButton = (MaterialButton) view2.findViewById(R.id.purchase_premium_yearly);
                            if (materialButton != null) {
                                return new d1((LinearLayout) view2, imageView, textView, textView2, textView3, materialButton);
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
        }
    }

    public e() {
        super(R.layout.premium_upgrade_yearly_dialog);
    }

    public final d1 g() {
        return (d1) this.p.getValue((Fragment) this, j[0]);
    }

    @Override // com.discord.app.AppDialog, androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        Dialog requireDialog = requireDialog();
        m.checkNotNullExpressionValue(requireDialog, "requireDialog()");
        Window window = requireDialog.getWindow();
        if (window != null) {
            window.setLayout(-1, -2);
        }
    }

    @Override // com.discord.app.AppDialog
    public void onViewBoundOrOnResume() {
        CharSequence e;
        super.onViewBoundOrOnResume();
        if (this.l == null || this.m == null) {
            dismiss();
        }
        requireDialog().setCanceledOnTouchOutside(true);
        boolean z2 = getArgumentsOrDefault().getBoolean("ARG_IS_TIER_1");
        e = b.a.k.b.e(this, z2 ? R.string.premium_tier_1 : R.string.premium_tier_2, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        TextView textView = g().d;
        m.checkNotNullExpressionValue(textView, "binding.premiumUpsellYearlyTitle");
        b.a.k.b.m(textView, R.string.premium_upsell_yearly_title, new Object[]{NumberFormat.getPercentInstance().format(Float.valueOf(0.16f)), e}, (r4 & 4) != 0 ? b.g.j : null);
        TextView textView2 = g().c;
        m.checkNotNullExpressionValue(textView2, "binding.premiumUpsellYearlyDescription");
        b.a.k.b.m(textView2, R.string.premium_upsell_yearly_description, new Object[]{NumberFormat.getPercentInstance().format(Float.valueOf(0.16f))}, (r4 & 4) != 0 ? b.g.j : null);
        MaterialButton materialButton = g().f;
        m.checkNotNullExpressionValue(materialButton, "binding.purchasePremiumYearly");
        b.a.k.b.m(materialButton, R.string.premium_upsell_yearly_cta, new Object[]{this.n}, (r4 & 4) != 0 ? b.g.j : null);
        g().f96b.setImageResource(z2 ? R.drawable.img_premium_guild_subscription_purchase_header_tier_1 : R.drawable.img_premium_guild_subscription_purchase_header_tier_2);
        TextView textView3 = g().e;
        m.checkNotNullExpressionValue(textView3, "binding.purchasePremiumMonthly");
        textView3.setVisibility(this.o ^ true ? 0 : 8);
        g().e.setOnClickListener(new a(0, this));
        g().f.setOnClickListener(new a(1, this));
    }
}
