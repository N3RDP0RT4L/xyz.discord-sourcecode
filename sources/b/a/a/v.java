package b.a.a;

import b.a.a.w;
import com.discord.models.user.MeUser;
import d0.z.d.m;
import j0.k.b;
/* compiled from: WidgetUrgentMessageDialogViewModel.kt */
/* loaded from: classes.dex */
public final class v<T, R> implements b<MeUser, w.c> {
    public static final v j = new v();

    @Override // j0.k.b
    public w.c call(MeUser meUser) {
        MeUser meUser2 = meUser;
        m.checkNotNullExpressionValue(meUser2, "meUser");
        return new w.c(meUser2);
    }
}
