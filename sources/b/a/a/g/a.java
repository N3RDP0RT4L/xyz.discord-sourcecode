package b.a.a.g;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import b.a.i.l5;
import b.a.k.b;
import com.discord.app.AppDialog;
import com.discord.stores.StoreNotices;
import com.discord.stores.StoreStream;
import com.discord.utilities.string.StringUtilsKt;
import com.discord.utilities.uri.UriHandler;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetMaskedLinksDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \u00102\u00020\u0001:\u0001\u0011B\u0007¢\u0006\u0004\b\u000f\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\bR\u001d\u0010\u000e\u001a\u00020\t8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\f\u0010\r¨\u0006\u0012"}, d2 = {"Lb/a/a/g/a;", "Lcom/discord/app/AppDialog;", "Landroid/view/View;", "view", "", "onViewBound", "(Landroid/view/View;)V", "onDestroy", "()V", "Lb/a/i/l5;", "l", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "g", "()Lb/a/i/l5;", "binding", HookHelper.constructorName, "k", "b", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class a extends AppDialog {
    public static final /* synthetic */ KProperty[] j = {b.d.b.a.a.b0(a.class, "binding", "getBinding()Lcom/discord/databinding/WidgetMaskedLinksDialogBinding;", 0)};
    public static final b k = new b(null);
    public final FragmentViewBindingDelegate l = FragmentViewBindingDelegateKt.viewBinding$default(this, c.j, null, 2, null);

    /* compiled from: java-style lambda group */
    /* renamed from: b.a.a.g.a$a  reason: collision with other inner class name */
    /* loaded from: classes2.dex */
    public static final class View$OnClickListenerC0022a implements View.OnClickListener {
        public final /* synthetic */ int j;
        public final /* synthetic */ Object k;
        public final /* synthetic */ Object l;

        public View$OnClickListenerC0022a(int i, Object obj, Object obj2) {
            this.j = i;
            this.k = obj;
            this.l = obj2;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            int i = this.j;
            if (i == 0) {
                a aVar = (a) this.k;
                m.checkNotNullExpressionValue(view, "v");
                Context context = view.getContext();
                m.checkNotNullExpressionValue(context, "v.context");
                String str = (String) this.l;
                KProperty[] kPropertyArr = a.j;
                Objects.requireNonNull(aVar);
                UriHandler.handle$default(UriHandler.INSTANCE, context, str, null, 4, null);
                aVar.dismiss();
            } else if (i == 1) {
                a aVar2 = (a) this.k;
                m.checkNotNullExpressionValue(view, "v");
                Context context2 = view.getContext();
                m.checkNotNullExpressionValue(context2, "v.context");
                String str2 = (String) this.l;
                KProperty[] kPropertyArr2 = a.j;
                Objects.requireNonNull(aVar2);
                StoreStream.Companion.getMaskedLinks().trustDomain(str2);
                UriHandler.handle$default(UriHandler.INSTANCE, context2, str2, null, 4, null);
                aVar2.dismiss();
            } else {
                throw null;
            }
        }
    }

    /* compiled from: WidgetMaskedLinksDialog.kt */
    /* loaded from: classes.dex */
    public static final class b {
        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: WidgetMaskedLinksDialog.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class c extends k implements Function1<View, l5> {
        public static final c j = new c();

        public c() {
            super(1, l5.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetMaskedLinksDialogBinding;", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public l5 invoke(View view) {
            View view2 = view;
            m.checkNotNullParameter(view2, "p1");
            int i = R.id.masked_links_body_text;
            TextView textView = (TextView) view2.findViewById(R.id.masked_links_body_text);
            if (textView != null) {
                i = R.id.masked_links_cancel;
                MaterialButton materialButton = (MaterialButton) view2.findViewById(R.id.masked_links_cancel);
                if (materialButton != null) {
                    i = R.id.masked_links_confirm;
                    MaterialButton materialButton2 = (MaterialButton) view2.findViewById(R.id.masked_links_confirm);
                    if (materialButton2 != null) {
                        i = R.id.masked_links_trust_domain;
                        TextView textView2 = (TextView) view2.findViewById(R.id.masked_links_trust_domain);
                        if (textView2 != null) {
                            i = R.id.view_dialog_confirmation_header;
                            TextView textView3 = (TextView) view2.findViewById(R.id.view_dialog_confirmation_header);
                            if (textView3 != null) {
                                return new l5((LinearLayout) view2, textView, materialButton, materialButton2, textView2, textView3);
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
        }
    }

    /* compiled from: WidgetMaskedLinksDialog.kt */
    /* loaded from: classes.dex */
    public static final class d implements View.OnClickListener {
        public d() {
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            a.this.dismiss();
        }
    }

    public a() {
        super(R.layout.widget_masked_links_dialog);
    }

    public final l5 g() {
        return (l5) this.l.getValue((Fragment) this, j[0]);
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        StoreNotices notices = StoreStream.Companion.getNotices();
        Objects.requireNonNull(k);
        StoreNotices.markSeen$default(notices, "WIDGET_SPOOPY_LINKS_DIALOG", 0L, 2, null);
    }

    @Override // com.discord.app.AppDialog
    public void onViewBound(View view) {
        String str;
        Object obj;
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        Bundle arguments = getArguments();
        if (arguments == null || (str = arguments.getString("WIDGET_SPOOPY_LINKS_DIALOG_URL")) == null) {
            str = "";
        }
        m.checkNotNullExpressionValue(str, "arguments?.getString(DIALOG_URL) ?: \"\"");
        try {
            obj = StringUtilsKt.toPunyCodeASCIIUrl(str);
        } catch (Exception unused) {
            dismiss();
            obj = Unit.a;
        }
        TextView textView = g().f153b;
        m.checkNotNullExpressionValue(textView, "binding.maskedLinksBodyText");
        b.a.k.b.m(textView, R.string.masked_link_body, new Object[]{obj}, (r4 & 4) != 0 ? b.g.j : null);
        g().c.setOnClickListener(new d());
        g().d.setOnClickListener(new View$OnClickListenerC0022a(0, this, str));
        g().e.setOnClickListener(new View$OnClickListenerC0022a(1, this, str));
    }
}
