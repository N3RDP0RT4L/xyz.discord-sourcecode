package b.a.a;

import andhook.lib.HookHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.a.i.e;
import b.a.k.b;
import com.discord.app.AppDialog;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: CameraCapacityDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \f2\u00020\u0001:\u0001\rB\u0007¢\u0006\u0004\b\u000b\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004R\u001d\u0010\n\u001a\u00020\u00058B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0006\u0010\u0007\u001a\u0004\b\b\u0010\t¨\u0006\u000e"}, d2 = {"Lb/a/a/j;", "Lcom/discord/app/AppDialog;", "", "onResume", "()V", "Lb/a/i/e;", "l", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lb/a/i/e;", "binding", HookHelper.constructorName, "k", "a", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class j extends AppDialog {
    public static final /* synthetic */ KProperty[] j = {b.d.b.a.a.b0(j.class, "binding", "getBinding()Lcom/discord/databinding/CameraCapacityDialogBinding;", 0)};
    public static final a k = new a(null);
    public final FragmentViewBindingDelegate l = FragmentViewBindingDelegateKt.viewBinding$default(this, b.j, null, 2, null);

    /* compiled from: CameraCapacityDialog.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final void a(FragmentManager fragmentManager, int i) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            Bundle bundle = new Bundle();
            bundle.putInt("ARG_MAX_VIDEO_CHANNEL_USERS", i);
            j jVar = new j();
            jVar.setArguments(bundle);
            jVar.show(fragmentManager, j.class.getSimpleName());
        }
    }

    /* compiled from: CameraCapacityDialog.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class b extends k implements Function1<View, e> {
        public static final b j = new b();

        public b() {
            super(1, e.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/CameraCapacityDialogBinding;", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public e invoke(View view) {
            View view2 = view;
            m.checkNotNullParameter(view2, "p1");
            int i = R.id.camera_capacity_body;
            TextView textView = (TextView) view2.findViewById(R.id.camera_capacity_body);
            if (textView != null) {
                i = R.id.camera_capacity_confirm;
                MaterialButton materialButton = (MaterialButton) view2.findViewById(R.id.camera_capacity_confirm);
                if (materialButton != null) {
                    i = R.id.notice_header_container;
                    LinearLayout linearLayout = (LinearLayout) view2.findViewById(R.id.notice_header_container);
                    if (linearLayout != null) {
                        return new e((LinearLayout) view2, textView, materialButton, linearLayout);
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
        }
    }

    /* compiled from: CameraCapacityDialog.kt */
    /* loaded from: classes.dex */
    public static final class c implements View.OnClickListener {
        public c() {
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            j.this.dismiss();
        }
    }

    public j() {
        super(R.layout.camera_capacity_dialog);
    }

    @Override // com.discord.app.AppDialog, androidx.fragment.app.Fragment
    public void onResume() {
        CharSequence e;
        super.onResume();
        requireDialog().setCanceledOnTouchOutside(true);
        FragmentViewBindingDelegate fragmentViewBindingDelegate = this.l;
        KProperty<?>[] kPropertyArr = j;
        ((e) fragmentViewBindingDelegate.getValue((Fragment) this, kPropertyArr[0])).c.setOnClickListener(new c());
        Bundle arguments = getArguments();
        Integer valueOf = arguments != null ? Integer.valueOf(arguments.getInt("ARG_MAX_VIDEO_CHANNEL_USERS")) : null;
        TextView textView = ((e) this.l.getValue((Fragment) this, kPropertyArr[0])).f101b;
        m.checkNotNullExpressionValue(textView, "binding.cameraCapacityBody");
        e = b.a.k.b.e(this, R.string.video_capacity_modal_body, new Object[]{String.valueOf(valueOf)}, (r4 & 4) != 0 ? b.a.j : null);
        textView.setText(e);
    }
}
