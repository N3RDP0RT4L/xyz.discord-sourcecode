package b.a.a;

import andhook.lib.HookHelper;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.a.w;
import b.a.d.f0;
import b.a.d.h0;
import b.a.i.z5;
import com.discord.app.AppDialog;
import com.discord.app.AppViewModel;
import com.discord.stores.StoreStream;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.LoadingButton;
import d0.z.d.a0;
import d0.z.d.k;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.subjects.PublishSubject;
import xyz.discord.R;
/* compiled from: WidgetUrgentMessageDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \u00162\u00020\u0001:\u0001\u0017B\u0007¢\u0006\u0004\b\u0015\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\bR\u001d\u0010\u000e\u001a\u00020\t8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\f\u0010\rR\u001d\u0010\u0014\u001a\u00020\u000f8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013¨\u0006\u0018"}, d2 = {"Lb/a/a/s;", "Lcom/discord/app/AppDialog;", "Landroid/view/View;", "view", "", "onViewBound", "(Landroid/view/View;)V", "onDestroy", "()V", "Lb/a/a/w;", "m", "Lkotlin/Lazy;", "h", "()Lb/a/a/w;", "viewModel", "Lb/a/i/z5;", "l", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "g", "()Lb/a/i/z5;", "binding", HookHelper.constructorName, "k", "a", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class s extends AppDialog {
    public static final /* synthetic */ KProperty[] j = {b.d.b.a.a.b0(s.class, "binding", "getBinding()Lcom/discord/databinding/WidgetUrgentMessageDialogBinding;", 0)};
    public static final a k = new a(null);
    public final FragmentViewBindingDelegate l = FragmentViewBindingDelegateKt.viewBinding$default(this, b.j, null, 2, null);
    public final Lazy m;

    /* compiled from: WidgetUrgentMessageDialog.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: WidgetUrgentMessageDialog.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class b extends k implements Function1<View, z5> {
        public static final b j = new b();

        public b() {
            super(1, z5.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetUrgentMessageDialogBinding;", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public z5 invoke(View view) {
            View view2 = view;
            m.checkNotNullParameter(view2, "p1");
            int i = R.id.view_dialog_confirmation_confirm;
            LoadingButton loadingButton = (LoadingButton) view2.findViewById(R.id.view_dialog_confirmation_confirm);
            if (loadingButton != null) {
                i = R.id.view_dialog_confirmation_header;
                TextView textView = (TextView) view2.findViewById(R.id.view_dialog_confirmation_header);
                if (textView != null) {
                    i = R.id.view_dialog_confirmation_text;
                    TextView textView2 = (TextView) view2.findViewById(R.id.view_dialog_confirmation_text);
                    if (textView2 != null) {
                        return new z5((LinearLayout) view2, loadingButton, textView, textView2);
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
        }
    }

    /* compiled from: WidgetUrgentMessageDialog.kt */
    /* loaded from: classes.dex */
    public static final class c extends o implements Function1<w.d, Unit> {
        public c() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(w.d dVar) {
            w.d dVar2 = dVar;
            m.checkNotNullParameter(dVar2, "viewState");
            s sVar = s.this;
            KProperty[] kPropertyArr = s.j;
            TextView textView = sVar.g().c;
            m.checkNotNullExpressionValue(textView, "binding.viewDialogConfirmationHeader");
            textView.setText(sVar.getString(R.string.system_dm_urgent_message_modal_header));
            TextView textView2 = sVar.g().d;
            m.checkNotNullExpressionValue(textView2, "binding.viewDialogConfirmationText");
            textView2.setText(sVar.getString(R.string.system_dm_urgent_message_modal_body));
            sVar.g().f238b.setText(sVar.getString(R.string.okay));
            sVar.g().f238b.setIsLoading(dVar2.f49b);
            sVar.g().f238b.setOnClickListener(new t(sVar));
            return Unit.a;
        }
    }

    /* compiled from: WidgetUrgentMessageDialog.kt */
    /* loaded from: classes.dex */
    public static final class d extends o implements Function1<w.b, Unit> {
        public d() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(w.b bVar) {
            w.b bVar2 = bVar;
            m.checkNotNullParameter(bVar2, "event");
            s sVar = s.this;
            KProperty[] kPropertyArr = s.j;
            Objects.requireNonNull(sVar);
            if (m.areEqual(bVar2, w.b.a.a)) {
                sVar.dismiss();
            } else if (m.areEqual(bVar2, w.b.C0024b.a)) {
                b.a.d.m.g(sVar.getContext(), R.string.internal_server_error, 0, null, 12);
            } else {
                throw new NoWhenBranchMatchedException();
            }
            return Unit.a;
        }
    }

    /* compiled from: WidgetUrgentMessageDialog.kt */
    /* loaded from: classes.dex */
    public static final class e extends o implements Function0<AppViewModel<w.d>> {
        public static final e j = new e();

        public e() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public AppViewModel<w.d> invoke() {
            return new w(null, null, null, 7);
        }
    }

    public s() {
        super(R.layout.widget_urgent_message_dialog);
        e eVar = e.j;
        f0 f0Var = new f0(this);
        this.m = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(w.class), new k(1, f0Var), new h0(eVar));
    }

    public final z5 g() {
        return (z5) this.l.getValue((Fragment) this, j[0]);
    }

    public final w h() {
        return (w) this.m.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        StoreStream.Companion.getNotices().markDialogSeen("URGENT_MESSAGE_DIALOG");
        super.onDestroy();
    }

    @Override // com.discord.app.AppDialog
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setCancelable(false);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(h().observeViewState(), this, null, 2, null), s.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new c());
        PublishSubject<w.b> publishSubject = h().k;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(publishSubject, this, null, 2, null), s.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new d());
    }
}
