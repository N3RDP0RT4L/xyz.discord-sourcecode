package b.a.a.f.a;

import andhook.lib.HookHelper;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.a.f.a.d;
import b.a.d.f0;
import b.a.d.h0;
import com.discord.app.AppDialog;
import com.discord.app.AppViewModel;
import com.discord.rtcconnection.audio.DiscordAudioManager;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.color.ColorCompatKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.google.android.material.radiobutton.MaterialRadioButton;
import d0.z.d.a0;
import d0.z.d.k;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: AudioOutputSelectionDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \u00162\u00020\u0001:\u0001\u0017B\u0007¢\u0006\u0004\b\u0015\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\bR\u001d\u0010\u000e\u001a\u00020\t8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\f\u0010\rR\u001d\u0010\u0014\u001a\u00020\u000f8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013¨\u0006\u0018"}, d2 = {"Lb/a/a/f/a/a;", "Lcom/discord/app/AppDialog;", "Landroid/view/View;", "view", "", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Lb/a/a/f/a/d;", "m", "Lkotlin/Lazy;", "getViewModel", "()Lb/a/a/f/a/d;", "viewModel", "Lb/a/i/b;", "l", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "i", "()Lb/a/i/b;", "binding", HookHelper.constructorName, "k", "b", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class a extends AppDialog {
    public static final /* synthetic */ KProperty[] j = {b.d.b.a.a.b0(a.class, "binding", "getBinding()Lcom/discord/databinding/AudioOutputSelectionDialogBinding;", 0)};
    public static final b k = new b(null);
    public final FragmentViewBindingDelegate l = FragmentViewBindingDelegateKt.viewBinding$default(this, c.j, null, 2, null);
    public final Lazy m;

    /* compiled from: java-style lambda group */
    /* renamed from: b.a.a.f.a.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static final class View$OnClickListenerC0020a implements View.OnClickListener {
        public final /* synthetic */ int j;
        public final /* synthetic */ Object k;

        public View$OnClickListenerC0020a(int i, Object obj) {
            this.j = i;
            this.k = obj;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            int i = this.j;
            if (i == 0) {
                a.h((a) this.k).j.selectOutputDevice(DiscordAudioManager.DeviceTypes.BLUETOOTH_HEADSET);
                a.g((a) this.k);
            } else if (i == 1) {
                a.h((a) this.k).j.selectOutputDevice(DiscordAudioManager.DeviceTypes.SPEAKERPHONE);
                a.g((a) this.k);
            } else if (i == 2) {
                a.h((a) this.k).j.selectOutputDevice(DiscordAudioManager.DeviceTypes.WIRED_HEADSET);
                a.g((a) this.k);
            } else if (i == 3) {
                a.h((a) this.k).j.selectOutputDevice(DiscordAudioManager.DeviceTypes.EARPIECE);
                a.g((a) this.k);
            } else {
                throw null;
            }
        }
    }

    /* compiled from: AudioOutputSelectionDialog.kt */
    /* loaded from: classes.dex */
    public static final class b {
        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: AudioOutputSelectionDialog.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class c extends k implements Function1<View, b.a.i.b> {
        public static final c j = new c();

        public c() {
            super(1, b.a.i.b.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/AudioOutputSelectionDialogBinding;", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public b.a.i.b invoke(View view) {
            View view2 = view;
            m.checkNotNullParameter(view2, "p1");
            int i = R.id.audio_output_selection_bluetooth_radio;
            MaterialRadioButton materialRadioButton = (MaterialRadioButton) view2.findViewById(R.id.audio_output_selection_bluetooth_radio);
            if (materialRadioButton != null) {
                i = R.id.audio_output_selection_dialog_header;
                TextView textView = (TextView) view2.findViewById(R.id.audio_output_selection_dialog_header);
                if (textView != null) {
                    i = R.id.audio_output_selection_dialog_radio_group;
                    RadioGroup radioGroup = (RadioGroup) view2.findViewById(R.id.audio_output_selection_dialog_radio_group);
                    if (radioGroup != null) {
                        i = R.id.audio_output_selection_earpiece_radio;
                        MaterialRadioButton materialRadioButton2 = (MaterialRadioButton) view2.findViewById(R.id.audio_output_selection_earpiece_radio);
                        if (materialRadioButton2 != null) {
                            i = R.id.audio_output_selection_speaker_radio;
                            MaterialRadioButton materialRadioButton3 = (MaterialRadioButton) view2.findViewById(R.id.audio_output_selection_speaker_radio);
                            if (materialRadioButton3 != null) {
                                i = R.id.audio_output_selection_wired_radio;
                                MaterialRadioButton materialRadioButton4 = (MaterialRadioButton) view2.findViewById(R.id.audio_output_selection_wired_radio);
                                if (materialRadioButton4 != null) {
                                    return new b.a.i.b((LinearLayout) view2, materialRadioButton, textView, radioGroup, materialRadioButton2, materialRadioButton3, materialRadioButton4);
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
        }
    }

    /* compiled from: AudioOutputSelectionDialog.kt */
    /* loaded from: classes.dex */
    public static final class d extends o implements Function1<d.a, Unit> {
        public d() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(d.a aVar) {
            int i;
            d.a aVar2 = aVar;
            m.checkNotNullParameter(aVar2, "viewState");
            a aVar3 = a.this;
            Objects.requireNonNull(aVar3);
            m.checkNotNullParameter(aVar2, "viewState");
            if (!(aVar2 instanceof d.a.b) && (aVar2 instanceof d.a.C0021a)) {
                d.a.C0021a aVar4 = (d.a.C0021a) aVar2;
                int ordinal = aVar4.a.ordinal();
                if (ordinal == 2) {
                    MaterialRadioButton materialRadioButton = aVar3.i().e;
                    m.checkNotNullExpressionValue(materialRadioButton, "binding.audioOutputSelectionSpeakerRadio");
                    i = materialRadioButton.getId();
                } else if (ordinal == 3) {
                    MaterialRadioButton materialRadioButton2 = aVar3.i().f;
                    m.checkNotNullExpressionValue(materialRadioButton2, "binding.audioOutputSelectionWiredRadio");
                    i = materialRadioButton2.getId();
                } else if (ordinal == 4) {
                    MaterialRadioButton materialRadioButton3 = aVar3.i().d;
                    m.checkNotNullExpressionValue(materialRadioButton3, "binding.audioOutputSelectionEarpieceRadio");
                    i = materialRadioButton3.getId();
                } else if (ordinal == 5) {
                    MaterialRadioButton materialRadioButton4 = aVar3.i().f80b;
                    m.checkNotNullExpressionValue(materialRadioButton4, "binding.audioOutputSelectionBluetoothRadio");
                    i = materialRadioButton4.getId();
                }
                aVar3.i().c.check(i);
                MaterialRadioButton materialRadioButton5 = aVar3.i().f80b;
                m.checkNotNullExpressionValue(materialRadioButton5, "binding.audioOutputSelectionBluetoothRadio");
                int i2 = 0;
                materialRadioButton5.setVisibility(aVar4.f47b ? 0 : 8);
                MaterialRadioButton materialRadioButton6 = aVar3.i().f80b;
                m.checkNotNullExpressionValue(materialRadioButton6, "binding.audioOutputSelectionBluetoothRadio");
                String str = aVar4.c;
                if (str == null) {
                    str = aVar3.getString(R.string.audio_devices_bluetooth);
                }
                materialRadioButton6.setText(str);
                MaterialRadioButton materialRadioButton7 = aVar3.i().e;
                m.checkNotNullExpressionValue(materialRadioButton7, "binding.audioOutputSelectionSpeakerRadio");
                materialRadioButton7.setVisibility(aVar4.d ? 0 : 8);
                MaterialRadioButton materialRadioButton8 = aVar3.i().f;
                m.checkNotNullExpressionValue(materialRadioButton8, "binding.audioOutputSelectionWiredRadio");
                materialRadioButton8.setVisibility(aVar4.e ? 0 : 8);
                MaterialRadioButton materialRadioButton9 = aVar3.i().d;
                m.checkNotNullExpressionValue(materialRadioButton9, "binding.audioOutputSelectionEarpieceRadio");
                if (!aVar4.f) {
                    i2 = 8;
                }
                materialRadioButton9.setVisibility(i2);
            }
            return Unit.a;
        }
    }

    /* compiled from: AudioOutputSelectionDialog.kt */
    /* loaded from: classes.dex */
    public static final class e extends o implements Function0<AppViewModel<d.a>> {
        public static final e j = new e();

        public e() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public AppViewModel<d.a> invoke() {
            return new b.a.a.f.a.d(null, null, 3);
        }
    }

    public a() {
        super(R.layout.audio_output_selection_dialog);
        e eVar = e.j;
        f0 f0Var = new f0(this);
        this.m = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(b.a.a.f.a.d.class), new k(5, f0Var), new h0(eVar));
    }

    public static final void g(a aVar) {
        Objects.requireNonNull(aVar);
        Observable<Long> d02 = Observable.d0(300L, TimeUnit.MILLISECONDS);
        m.checkNotNullExpressionValue(d02, "Observable\n        .time…S, TimeUnit.MILLISECONDS)");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(d02, aVar, null, 2, null), a.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new b.a.a.f.a.b(aVar));
    }

    public static final b.a.a.f.a.d h(a aVar) {
        return (b.a.a.f.a.d) aVar.m.getValue();
    }

    public final b.a.i.b i() {
        return (b.a.i.b) this.l.getValue((Fragment) this, j[0]);
    }

    @Override // com.discord.app.AppDialog
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        int themedColor = ColorCompat.getThemedColor(view, (int) R.attr.colorInteractiveNormal);
        MaterialRadioButton materialRadioButton = i().f80b;
        m.checkNotNullExpressionValue(materialRadioButton, "binding.audioOutputSelectionBluetoothRadio");
        ColorCompatKt.setDrawableColor(materialRadioButton, themedColor);
        MaterialRadioButton materialRadioButton2 = i().e;
        m.checkNotNullExpressionValue(materialRadioButton2, "binding.audioOutputSelectionSpeakerRadio");
        ColorCompatKt.setDrawableColor(materialRadioButton2, themedColor);
        MaterialRadioButton materialRadioButton3 = i().f;
        m.checkNotNullExpressionValue(materialRadioButton3, "binding.audioOutputSelectionWiredRadio");
        ColorCompatKt.setDrawableColor(materialRadioButton3, themedColor);
        MaterialRadioButton materialRadioButton4 = i().d;
        m.checkNotNullExpressionValue(materialRadioButton4, "binding.audioOutputSelectionEarpieceRadio");
        ColorCompatKt.setDrawableColor(materialRadioButton4, themedColor);
        i().f80b.setOnClickListener(new View$OnClickListenerC0020a(0, this));
        i().e.setOnClickListener(new View$OnClickListenerC0020a(1, this));
        i().f.setOnClickListener(new View$OnClickListenerC0020a(2, this));
        i().d.setOnClickListener(new View$OnClickListenerC0020a(3, this));
    }

    @Override // com.discord.app.AppDialog
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(((b.a.a.f.a.d) this.m.getValue()).observeViewState(), this, null, 2, null), a.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new d());
    }
}
