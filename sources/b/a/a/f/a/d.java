package b.a.a.f.a;

import com.discord.app.AppViewModel;
import com.discord.rtcconnection.audio.DiscordAudioManager;
import com.discord.stores.StoreAudioManagerV2;
import com.discord.stores.StoreStream;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: AudioOutputSelectionDialogViewModel.kt */
/* loaded from: classes.dex */
public final class d extends AppViewModel<a> {
    public final StoreAudioManagerV2 j;

    /* compiled from: AudioOutputSelectionDialogViewModel.kt */
    /* loaded from: classes.dex */
    public static abstract class a {

        /* compiled from: AudioOutputSelectionDialogViewModel.kt */
        /* renamed from: b.a.a.f.a.d$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C0021a extends a {
            public final DiscordAudioManager.DeviceTypes a;

            /* renamed from: b  reason: collision with root package name */
            public final boolean f47b;
            public final String c;
            public final boolean d;
            public final boolean e;
            public final boolean f;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C0021a(DiscordAudioManager.DeviceTypes deviceTypes, boolean z2, String str, boolean z3, boolean z4, boolean z5) {
                super(null);
                m.checkNotNullParameter(deviceTypes, "selectedAudioOutput");
                this.a = deviceTypes;
                this.f47b = z2;
                this.c = str;
                this.d = z3;
                this.e = z4;
                this.f = z5;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof C0021a)) {
                    return false;
                }
                C0021a aVar = (C0021a) obj;
                return m.areEqual(this.a, aVar.a) && this.f47b == aVar.f47b && m.areEqual(this.c, aVar.c) && this.d == aVar.d && this.e == aVar.e && this.f == aVar.f;
            }

            public int hashCode() {
                DiscordAudioManager.DeviceTypes deviceTypes = this.a;
                int i = 0;
                int hashCode = (deviceTypes != null ? deviceTypes.hashCode() : 0) * 31;
                boolean z2 = this.f47b;
                int i2 = 1;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                int i5 = (hashCode + i3) * 31;
                String str = this.c;
                if (str != null) {
                    i = str.hashCode();
                }
                int i6 = (i5 + i) * 31;
                boolean z3 = this.d;
                if (z3) {
                    z3 = true;
                }
                int i7 = z3 ? 1 : 0;
                int i8 = z3 ? 1 : 0;
                int i9 = (i6 + i7) * 31;
                boolean z4 = this.e;
                if (z4) {
                    z4 = true;
                }
                int i10 = z4 ? 1 : 0;
                int i11 = z4 ? 1 : 0;
                int i12 = (i9 + i10) * 31;
                boolean z5 = this.f;
                if (!z5) {
                    i2 = z5 ? 1 : 0;
                }
                return i12 + i2;
            }

            public String toString() {
                StringBuilder R = b.d.b.a.a.R("Loaded(selectedAudioOutput=");
                R.append(this.a);
                R.append(", showBluetoothItem=");
                R.append(this.f47b);
                R.append(", bluetoothDeviceName=");
                R.append(this.c);
                R.append(", showSpeakerItem=");
                R.append(this.d);
                R.append(", showWiredItem=");
                R.append(this.e);
                R.append(", showEarpieceItem=");
                return b.d.b.a.a.M(R, this.f, ")");
            }
        }

        /* compiled from: AudioOutputSelectionDialogViewModel.kt */
        /* loaded from: classes.dex */
        public static final class b extends a {
            public static final b a = new b();

            public b() {
                super(null);
            }
        }

        public a() {
        }

        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    public d() {
        this(null, null, 3);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public d(StoreAudioManagerV2 storeAudioManagerV2, Observable observable, int i) {
        super(a.b.a);
        StoreAudioManagerV2 audioManagerV2 = (i & 1) != 0 ? StoreStream.Companion.getAudioManagerV2() : null;
        Observable<StoreAudioManagerV2.State> observeAudioManagerState = (i & 2) != 0 ? audioManagerV2.observeAudioManagerState() : null;
        m.checkNotNullParameter(audioManagerV2, "storeAudioManager");
        m.checkNotNullParameter(observeAudioManagerState, "storeStateObservable");
        this.j = audioManagerV2;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observeAudioManagerState), this, null, 2, null), d.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new c(this));
    }
}
