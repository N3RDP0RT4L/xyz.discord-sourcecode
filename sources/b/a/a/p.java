package b.a.a;

import com.discord.app.AppViewModel;
import com.discord.stores.StoreNotices;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUserSettings;
import d0.z.d.m;
import java.util.Objects;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: WidgetAccessibilityDetectionDialogViewModel.kt */
/* loaded from: classes.dex */
public final class p extends AppViewModel<a> {
    public final StoreUserSettings j;
    public final StoreNotices k;

    /* compiled from: WidgetAccessibilityDetectionDialogViewModel.kt */
    /* loaded from: classes.dex */
    public static abstract class a {

        /* compiled from: WidgetAccessibilityDetectionDialogViewModel.kt */
        /* renamed from: b.a.a.p$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C0023a extends a {
            public static final C0023a a = new C0023a();

            public C0023a() {
                super(null);
            }
        }

        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    public p() {
        this(null, null, 3);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public p(StoreUserSettings storeUserSettings, StoreNotices storeNotices, int i) {
        super(a.C0023a.a);
        StoreNotices storeNotices2 = null;
        StoreUserSettings userSettings = (i & 1) != 0 ? StoreStream.Companion.getUserSettings() : null;
        storeNotices2 = (i & 2) != 0 ? StoreStream.Companion.getNotices() : storeNotices2;
        m.checkNotNullParameter(userSettings, "storeUserSettings");
        m.checkNotNullParameter(storeNotices2, "storeNotices");
        this.j = userSettings;
        this.k = storeNotices2;
    }

    @Override // com.discord.app.AppViewModel, androidx.lifecycle.ViewModel
    public void onCleared() {
        StoreNotices storeNotices = this.k;
        Objects.requireNonNull(h.k);
        StoreNotices.markSeen$default(storeNotices, "ALLOW_ACCESSIBILITY_DETECTION_DIALOG", 0L, 2, null);
    }
}
