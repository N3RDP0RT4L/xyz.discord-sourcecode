package b.a.a;

import andhook.lib.HookHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.a.i.m1;
import com.discord.app.AppDialog;
import com.discord.dialogs.SimpleConfirmationDialogArgs;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.google.android.material.button.MaterialButton;
import d0.g;
import d0.z.d.a0;
import d0.z.d.k;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: SimpleConfirmationDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \u001a2\u00020\u0001:\u0001\u001bB\u0007¢\u0006\u0004\b\u0019\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004R\u001d\u0010\n\u001a\u00020\u00058F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u0006\u0010\u0007\u001a\u0004\b\b\u0010\tR$\u0010\u0012\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001d\u0010\u0018\u001a\u00020\u00138B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017¨\u0006\u001c"}, d2 = {"Lb/a/a/e;", "Lcom/discord/app/AppDialog;", "", "onViewBoundOrOnResume", "()V", "Lcom/discord/dialogs/SimpleConfirmationDialogArgs;", "l", "Lkotlin/Lazy;", "g", "()Lcom/discord/dialogs/SimpleConfirmationDialogArgs;", "args", "Landroid/view/View$OnClickListener;", "m", "Landroid/view/View$OnClickListener;", "getPositiveClickListener", "()Landroid/view/View$OnClickListener;", "setPositiveClickListener", "(Landroid/view/View$OnClickListener;)V", "positiveClickListener", "Lb/a/i/m1;", "n", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "h", "()Lb/a/i/m1;", "binding", HookHelper.constructorName, "k", "c", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class e extends AppDialog {
    public static final /* synthetic */ KProperty[] j = {b.d.b.a.a.b0(e.class, "binding", "getBinding()Lcom/discord/databinding/SimpleConfirmationDialogBinding;", 0)};
    public static final c k = new c(null);
    public View.OnClickListener m;
    public final Lazy l = g.lazy(new b(this, "intent_args_key"));
    public final FragmentViewBindingDelegate n = FragmentViewBindingDelegateKt.viewBinding$default(this, d.j, null, 2, null);

    /* compiled from: java-style lambda group */
    /* loaded from: classes.dex */
    public static final class a implements View.OnClickListener {
        public final /* synthetic */ int j;
        public final /* synthetic */ Object k;

        public a(int i, Object obj) {
            this.j = i;
            this.k = obj;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            int i = this.j;
            if (i == 0) {
                View.OnClickListener onClickListener = ((e) this.k).m;
                if (onClickListener != null) {
                    onClickListener.onClick(view);
                }
                ((e) this.k).dismiss();
            } else if (i == 1) {
                ((e) this.k).dismiss();
            } else {
                throw null;
            }
        }
    }

    /* compiled from: ArgUtils.kt */
    /* loaded from: classes.dex */
    public static final class b extends o implements Function0<SimpleConfirmationDialogArgs> {
        public final /* synthetic */ String $argsKey;
        public final /* synthetic */ AppDialog $this_args;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(AppDialog appDialog, String str) {
            super(0);
            this.$this_args = appDialog;
            this.$argsKey = str;
        }

        @Override // kotlin.jvm.functions.Function0
        public SimpleConfirmationDialogArgs invoke() {
            Bundle arguments = this.$this_args.getArguments();
            SimpleConfirmationDialogArgs simpleConfirmationDialogArgs = null;
            Object obj = arguments != null ? arguments.get(this.$argsKey) : null;
            if (obj instanceof SimpleConfirmationDialogArgs) {
                simpleConfirmationDialogArgs = obj;
            }
            SimpleConfirmationDialogArgs simpleConfirmationDialogArgs2 = simpleConfirmationDialogArgs;
            if (simpleConfirmationDialogArgs2 != null) {
                return simpleConfirmationDialogArgs2;
            }
            StringBuilder R = b.d.b.a.a.R("Missing args for class type ");
            b.d.b.a.a.j0(SimpleConfirmationDialogArgs.class, R, " + key ");
            throw new IllegalStateException(b.d.b.a.a.G(R, this.$argsKey, '!'));
        }
    }

    /* compiled from: SimpleConfirmationDialog.kt */
    /* loaded from: classes.dex */
    public static final class c {
        public c(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final e a(FragmentManager fragmentManager, SimpleConfirmationDialogArgs simpleConfirmationDialogArgs, View.OnClickListener onClickListener) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(simpleConfirmationDialogArgs, "args");
            m.checkNotNullParameter(onClickListener, "positiveClickListener");
            e eVar = new e();
            eVar.setArguments(b.c.a.a0.d.e2(simpleConfirmationDialogArgs));
            eVar.m = onClickListener;
            eVar.show(fragmentManager, a0.getOrCreateKotlinClass(e.class).toString());
            return eVar;
        }
    }

    /* compiled from: SimpleConfirmationDialog.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class d extends k implements Function1<View, m1> {
        public static final d j = new d();

        public d() {
            super(1, m1.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/SimpleConfirmationDialogBinding;", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public m1 invoke(View view) {
            View view2 = view;
            m.checkNotNullParameter(view2, "p1");
            int i = R.id.notice_header_container;
            LinearLayout linearLayout = (LinearLayout) view2.findViewById(R.id.notice_header_container);
            if (linearLayout != null) {
                i = R.id.simple_confirmation_dialog_description;
                TextView textView = (TextView) view2.findViewById(R.id.simple_confirmation_dialog_description);
                if (textView != null) {
                    i = R.id.simple_confirmation_dialog_negative;
                    MaterialButton materialButton = (MaterialButton) view2.findViewById(R.id.simple_confirmation_dialog_negative);
                    if (materialButton != null) {
                        i = R.id.simple_confirmation_dialog_positive;
                        MaterialButton materialButton2 = (MaterialButton) view2.findViewById(R.id.simple_confirmation_dialog_positive);
                        if (materialButton2 != null) {
                            i = R.id.simple_confirmation_dialog_title;
                            TextView textView2 = (TextView) view2.findViewById(R.id.simple_confirmation_dialog_title);
                            if (textView2 != null) {
                                return new m1((LinearLayout) view2, linearLayout, textView, materialButton, materialButton2, textView2);
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
        }
    }

    public e() {
        super(R.layout.simple_confirmation_dialog);
    }

    public final SimpleConfirmationDialogArgs g() {
        return (SimpleConfirmationDialogArgs) this.l.getValue();
    }

    public final m1 h() {
        return (m1) this.n.getValue((Fragment) this, j[0]);
    }

    @Override // com.discord.app.AppDialog
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        requireDialog().setCanceledOnTouchOutside(true);
        h().d.setOnClickListener(new a(0, this));
        h().d.setText(g().l);
        h().c.setOnClickListener(new a(1, this));
        MaterialButton materialButton = h().c;
        m.checkNotNullExpressionValue(materialButton, "binding.simpleConfirmationDialogNegative");
        materialButton.setText(g().m);
        TextView textView = h().e;
        m.checkNotNullExpressionValue(textView, "binding.simpleConfirmationDialogTitle");
        b.a.k.b.a(textView, g().j);
        TextView textView2 = h().f156b;
        m.checkNotNullExpressionValue(textView2, "binding.simpleConfirmationDialogDescription");
        b.a.k.b.a(textView2, g().k);
    }
}
