package b.a.a;

import andhook.lib.HookHelper;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import b.a.i.c5;
import com.discord.api.auth.mfa.EnableMfaResponse;
import com.discord.app.AppDialog;
import com.discord.restapi.RestAPIParams;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.LoadingButton;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.k;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetEnableTwoFactorPasswordDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\b\u0018\u0000 \u00142\u00020\u0001:\u0001\u0015B\u0007¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006R\u001d\u0010\f\u001a\u00020\u00078B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR\"\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u00040\r8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010¨\u0006\u0016"}, d2 = {"Lb/a/a/r;", "Lcom/discord/app/AppDialog;", "Landroid/view/View;", "view", "", "onViewBound", "(Landroid/view/View;)V", "Lb/a/i/c5;", "l", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "g", "()Lb/a/i/c5;", "binding", "Lkotlin/Function1;", "", "m", "Lkotlin/jvm/functions/Function1;", "onValidPasswordEntered", HookHelper.constructorName, "()V", "k", "a", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class r extends AppDialog {
    public static final /* synthetic */ KProperty[] j = {b.d.b.a.a.b0(r.class, "binding", "getBinding()Lcom/discord/databinding/WidgetEnableTwoFactorPasswordDialogBinding;", 0)};
    public static final a k = new a(null);
    public final FragmentViewBindingDelegate l = FragmentViewBindingDelegateKt.viewBinding$default(this, b.j, null, 2, null);
    public Function1<? super String, Unit> m = c.j;

    /* compiled from: WidgetEnableTwoFactorPasswordDialog.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: WidgetEnableTwoFactorPasswordDialog.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class b extends k implements Function1<View, c5> {
        public static final b j = new b();

        public b() {
            super(1, c5.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetEnableTwoFactorPasswordDialogBinding;", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public c5 invoke(View view) {
            View view2 = view;
            m.checkNotNullParameter(view2, "p1");
            int i = R.id.enable_two_factor_password_body_container;
            LinearLayout linearLayout = (LinearLayout) view2.findViewById(R.id.enable_two_factor_password_body_container);
            if (linearLayout != null) {
                i = R.id.enable_two_factor_password_body_text;
                TextView textView = (TextView) view2.findViewById(R.id.enable_two_factor_password_body_text);
                if (textView != null) {
                    i = R.id.enable_two_factor_password_cancel;
                    MaterialButton materialButton = (MaterialButton) view2.findViewById(R.id.enable_two_factor_password_cancel);
                    if (materialButton != null) {
                        i = R.id.enable_two_factor_password_header;
                        TextView textView2 = (TextView) view2.findViewById(R.id.enable_two_factor_password_header);
                        if (textView2 != null) {
                            i = R.id.enable_two_factor_password_header_container;
                            LinearLayout linearLayout2 = (LinearLayout) view2.findViewById(R.id.enable_two_factor_password_header_container);
                            if (linearLayout2 != null) {
                                i = R.id.enable_two_factor_password_ok;
                                LoadingButton loadingButton = (LoadingButton) view2.findViewById(R.id.enable_two_factor_password_ok);
                                if (loadingButton != null) {
                                    i = R.id.enable_two_factor_password_view_input;
                                    TextInputLayout textInputLayout = (TextInputLayout) view2.findViewById(R.id.enable_two_factor_password_view_input);
                                    if (textInputLayout != null) {
                                        return new c5((LinearLayout) view2, linearLayout, textView, materialButton, textView2, linearLayout2, loadingButton, textInputLayout);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
        }
    }

    /* compiled from: WidgetEnableTwoFactorPasswordDialog.kt */
    /* loaded from: classes.dex */
    public static final class c extends o implements Function1<String, Unit> {
        public static final c j = new c();

        public c() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(String str) {
            m.checkNotNullParameter(str, "it");
            return Unit.a;
        }
    }

    /* compiled from: WidgetEnableTwoFactorPasswordDialog.kt */
    /* loaded from: classes.dex */
    public static final class d implements View.OnClickListener {
        public final /* synthetic */ View k;

        /* compiled from: WidgetEnableTwoFactorPasswordDialog.kt */
        /* loaded from: classes.dex */
        public static final class a extends o implements Function1<EnableMfaResponse, Unit> {
            public static final a j = new a();

            public a() {
                super(1);
            }

            @Override // kotlin.jvm.functions.Function1
            public Unit invoke(EnableMfaResponse enableMfaResponse) {
                m.checkNotNullParameter(enableMfaResponse, "it");
                return Unit.a;
            }
        }

        /* compiled from: WidgetEnableTwoFactorPasswordDialog.kt */
        /* loaded from: classes.dex */
        public static final class b extends o implements Function1<Error, Unit> {
            public b() {
                super(1);
            }

            @Override // kotlin.jvm.functions.Function1
            public Unit invoke(Error error) {
                Error error2 = error;
                m.checkNotNullParameter(error2, "error");
                r rVar = r.this;
                KProperty[] kPropertyArr = r.j;
                rVar.g().c.setIsLoading(false);
                Error.Response response = error2.getResponse();
                m.checkNotNullExpressionValue(response, "error.response");
                if (response.getCode() == 60005) {
                    error2.setShowErrorToasts(false);
                    r.this.dismiss();
                    r rVar2 = r.this;
                    Function1<? super String, Unit> function1 = rVar2.m;
                    TextInputLayout textInputLayout = rVar2.g().d;
                    m.checkNotNullExpressionValue(textInputLayout, "binding.enableTwoFactorPasswordViewInput");
                    function1.invoke(ViewExtensions.getTextOrEmpty(textInputLayout));
                }
                return Unit.a;
            }
        }

        public d(View view) {
            this.k = view;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            r rVar = r.this;
            KProperty[] kPropertyArr = r.j;
            TextInputLayout textInputLayout = rVar.g().d;
            m.checkNotNullExpressionValue(textInputLayout, "binding.enableTwoFactorPasswordViewInput");
            String textOrEmpty = ViewExtensions.getTextOrEmpty(textInputLayout);
            r.this.g().c.setIsLoading(true);
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().enableMFA(new RestAPIParams.EnableMFA("random code", "random secret", textOrEmpty)), false, 1, null), r.this, null, 2, null), r.this.getClass(), (r18 & 2) != 0 ? null : this.k.getContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new b(), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, a.j);
        }
    }

    /* compiled from: WidgetEnableTwoFactorPasswordDialog.kt */
    /* loaded from: classes.dex */
    public static final class e implements View.OnClickListener {
        public e() {
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            FragmentActivity activity = r.this.e();
            if (activity != null) {
                activity.onBackPressed();
            }
        }
    }

    public r() {
        super(R.layout.widget_enable_two_factor_password_dialog);
    }

    public final c5 g() {
        return (c5) this.l.getValue((Fragment) this, j[0]);
    }

    @Override // com.discord.app.AppDialog
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setCancelable(false);
        g().c.setIsLoading(false);
        g().c.setOnClickListener(new d(view));
        g().f93b.setOnClickListener(new e());
    }
}
