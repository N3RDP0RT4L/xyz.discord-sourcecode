package b.a.a;

import com.discord.models.domain.ModelUserSettings;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetAccessibilityDetectionDialogViewModel.kt */
/* loaded from: classes.dex */
public final class q extends o implements Function1<ModelUserSettings, Unit> {
    public static final q j = new q();

    public q() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public Unit invoke(ModelUserSettings modelUserSettings) {
        m.checkNotNullParameter(modelUserSettings, "it");
        return Unit.a;
    }
}
