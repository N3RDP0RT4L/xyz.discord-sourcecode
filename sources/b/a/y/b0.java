package b.a.y;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;
import b.a.i.g1;
import com.discord.api.message.reaction.MessageReaction;
import com.discord.utilities.textprocessing.node.EmojiNode;
import com.discord.utilities.view.text.SimpleDraweeSpanTextView;
import d0.z.d.m;
import xyz.discord.R;
/* compiled from: ReactionView.kt */
/* loaded from: classes2.dex */
public final class b0 extends LinearLayout {
    public int j;
    public Long k;
    public MessageReaction l;
    public final g1 m;

    public b0(Context context) {
        super(context);
        View inflate = LayoutInflater.from(getContext()).inflate(R.layout.reaction_view, (ViewGroup) this, false);
        addView(inflate);
        int i = R.id.counter_text_1;
        TextView textView = (TextView) inflate.findViewById(R.id.counter_text_1);
        if (textView != null) {
            i = R.id.counter_text_2;
            TextView textView2 = (TextView) inflate.findViewById(R.id.counter_text_2);
            if (textView2 != null) {
                i = R.id.counter_text_switcher;
                TextSwitcher textSwitcher = (TextSwitcher) inflate.findViewById(R.id.counter_text_switcher);
                if (textSwitcher != null) {
                    i = R.id.emoji_text_view;
                    SimpleDraweeSpanTextView simpleDraweeSpanTextView = (SimpleDraweeSpanTextView) inflate.findViewById(R.id.emoji_text_view);
                    if (simpleDraweeSpanTextView != null) {
                        g1 g1Var = new g1((LinearLayout) inflate, textView, textView2, textSwitcher, simpleDraweeSpanTextView);
                        m.checkNotNullExpressionValue(g1Var, "ReactionViewBinding.infl…rom(context), this, true)");
                        this.m = g1Var;
                        return;
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    private final void setIsMe(boolean z2) {
        setActivated(z2);
        setSelected(z2);
        TextView textView = this.m.f114b;
        m.checkNotNullExpressionValue(textView, "binding.counterText1");
        textView.setActivated(z2);
        TextView textView2 = this.m.c;
        m.checkNotNullExpressionValue(textView2, "binding.counterText2");
        textView2.setActivated(z2);
    }

    public final void a(MessageReaction messageReaction, long j, boolean z2) {
        m.checkNotNullParameter(messageReaction, "reaction");
        Long l = this.k;
        boolean z3 = l == null || l == null || l.longValue() != j;
        MessageReaction messageReaction2 = this.l;
        int a = messageReaction.a();
        g1 g1Var = this.m;
        int i = this.j;
        if (a != i) {
            if (z3) {
                g1Var.d.setCurrentText(String.valueOf(a));
            } else {
                if (a > i) {
                    TextSwitcher textSwitcher = g1Var.d;
                    textSwitcher.setInAnimation(textSwitcher.getContext(), R.anim.anim_slide_in_up);
                    textSwitcher.setOutAnimation(textSwitcher.getContext(), R.anim.anim_slide_out_up);
                } else {
                    TextSwitcher textSwitcher2 = g1Var.d;
                    textSwitcher2.setInAnimation(textSwitcher2.getContext(), R.anim.anim_slide_in_down);
                    textSwitcher2.setOutAnimation(textSwitcher2.getContext(), R.anim.anim_slide_out_down);
                }
                g1Var.d.setText(String.valueOf(a));
            }
            this.j = a;
        }
        if (messageReaction2 == null || (!m.areEqual(messageReaction.b(), messageReaction2.b()))) {
            EmojiNode.Companion companion = EmojiNode.Companion;
            SimpleDraweeSpanTextView simpleDraweeSpanTextView = this.m.e;
            m.checkNotNullExpressionValue(simpleDraweeSpanTextView, "binding.emojiTextView");
            EmojiNode.Companion.renderEmoji$default(companion, simpleDraweeSpanTextView, messageReaction.b(), z2, 0, 4, null);
        }
        setIsMe(messageReaction.c());
        this.l = messageReaction;
        this.k = Long.valueOf(j);
    }

    public final MessageReaction getReaction() {
        return this.l;
    }
}
