package b.a.y;

import android.view.KeyEvent;
import android.view.View;
import com.discord.views.CodeVerificationView;
import d0.g0.y;
import d0.z.d.m;
import java.util.Objects;
/* compiled from: CodeVerificationView.kt */
/* loaded from: classes2.dex */
public final class d implements View.OnKeyListener {
    public final /* synthetic */ CodeVerificationView j;

    public d(CodeVerificationView codeVerificationView) {
        this.j = codeVerificationView;
    }

    @Override // android.view.View.OnKeyListener
    public final boolean onKey(View view, int i, KeyEvent keyEvent) {
        m.checkNotNullExpressionValue(keyEvent, "event");
        if (keyEvent.getAction() == 0) {
            CodeVerificationView codeVerificationView = this.j;
            CodeVerificationView.a aVar = CodeVerificationView.j;
            Objects.requireNonNull(codeVerificationView);
            if (keyEvent.getKeyCode() == 67) {
                if (codeVerificationView.q.length() > 0) {
                    codeVerificationView.q = y.dropLast(codeVerificationView.q, 1);
                    codeVerificationView.d();
                }
            } else if (keyEvent.getKeyCode() == 7) {
                codeVerificationView.c('0');
            } else {
                int unicodeChar = keyEvent.getUnicodeChar();
                if (unicodeChar != 0) {
                    codeVerificationView.c((char) unicodeChar);
                }
            }
        }
        return true;
    }
}
