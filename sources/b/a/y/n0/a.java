package b.a.y.n0;

import android.view.View;
import com.discord.utilities.billing.GooglePlayInAppSku;
import com.discord.views.premium.GiftSelectView;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: GiftSelectView.kt */
/* loaded from: classes2.dex */
public final class a implements View.OnClickListener {
    public final /* synthetic */ GiftSelectView j;

    public a(GiftSelectView giftSelectView) {
        this.j = giftSelectView;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        Function1<? super GooglePlayInAppSku, Unit> function1;
        GiftSelectView giftSelectView = this.j;
        GooglePlayInAppSku googlePlayInAppSku = giftSelectView.l;
        if (!(googlePlayInAppSku == null || (function1 = giftSelectView.k) == null)) {
            function1.invoke(googlePlayInAppSku);
        }
        giftSelectView.j.c.setOnClickListener(null);
    }
}
