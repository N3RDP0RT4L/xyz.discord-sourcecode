package b.a.y;

import android.graphics.Bitmap;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: VoiceUserView.kt */
/* loaded from: classes2.dex */
public final class h0 extends o implements Function2<Bitmap, String, Unit> {
    public static final h0 j = new h0();

    public h0() {
        super(2);
    }

    @Override // kotlin.jvm.functions.Function2
    public Unit invoke(Bitmap bitmap, String str) {
        m.checkNotNullParameter(bitmap, "<anonymous parameter 0>");
        return Unit.a;
    }
}
