package b.a.y;

import android.animation.Animator;
import android.content.Context;
import android.util.Log;
import android.view.View;
import androidx.core.view.ViewCompat;
import com.discord.app.AppComponent;
import com.discord.overlay.views.OverlayDialog;
import d0.z.d.m;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;
/* compiled from: OverlayAppDialog.kt */
/* loaded from: classes2.dex */
public abstract class l extends OverlayDialog implements AppComponent {

    /* renamed from: x  reason: collision with root package name */
    public final Subject<Void, Void> f309x;

    /* renamed from: y  reason: collision with root package name */
    public Function1<? super OverlayDialog, Unit> f310y = m.j;

    /* compiled from: Animator.kt */
    /* loaded from: classes2.dex */
    public static final class a implements Animator.AnimatorListener {
        public a() {
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationCancel(Animator animator) {
            m.checkNotNullParameter(animator, "animator");
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            m.checkNotNullParameter(animator, "animator");
            l.this.getOnDialogClosed().invoke(l.this);
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationRepeat(Animator animator) {
            m.checkNotNullParameter(animator, "animator");
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            m.checkNotNullParameter(animator, "animator");
        }
    }

    /* compiled from: OverlayAppDialog.kt */
    /* loaded from: classes2.dex */
    public static final class b implements View.OnAttachStateChangeListener {
        public final /* synthetic */ Animator j;

        public b(Animator animator) {
            this.j = animator;
        }

        @Override // android.view.View.OnAttachStateChangeListener
        public void onViewAttachedToWindow(View view) {
        }

        @Override // android.view.View.OnAttachStateChangeListener
        public void onViewDetachedFromWindow(View view) {
            this.j.end();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public l(Context context) {
        super(context);
        m.checkNotNullParameter(context, "context");
        PublishSubject k0 = PublishSubject.k0();
        m.checkNotNullExpressionValue(k0, "PublishSubject.create()");
        this.f309x = k0;
        setOnClickListener(new k(this));
    }

    public final void g() {
        getUnsubscribeSignal().onNext(null);
        Log.i(getClass().getSimpleName(), "closing");
        if (!ViewCompat.isAttachedToWindow(this)) {
            this.f310y.invoke(this);
            return;
        }
        Animator closingAnimator = getClosingAnimator();
        closingAnimator.addListener(new a());
        closingAnimator.start();
        addOnAttachStateChangeListener(new b(closingAnimator));
    }

    public abstract Animator getClosingAnimator();

    public final Function1<OverlayDialog, Unit> getOnDialogClosed() {
        return this.f310y;
    }

    @Override // com.discord.app.AppComponent
    public Subject<Void, Void> getUnsubscribeSignal() {
        return this.f309x;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        getUnsubscribeSignal().onNext(null);
        super.onDetachedFromWindow();
    }

    public final void setOnDialogClosed(Function1<? super OverlayDialog, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.f310y = function1;
    }
}
