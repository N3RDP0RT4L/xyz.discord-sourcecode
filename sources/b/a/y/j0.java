package b.a.y;

import android.graphics.Bitmap;
import b.f.j.q.a;
import com.discord.views.VoiceUserView;
import d0.z.d.m;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: VoiceUserView.kt */
/* loaded from: classes2.dex */
public final class j0 extends a {
    public final /* synthetic */ VoiceUserView.c a;

    public j0(VoiceUserView.c cVar) {
        this.a = cVar;
    }

    @Override // b.f.j.q.a
    public void process(Bitmap bitmap) {
        if (bitmap != null) {
            Function2<? super Bitmap, ? super String, Unit> function2 = VoiceUserView.this.p;
            Bitmap copy = bitmap.copy(Bitmap.Config.ARGB_8888, false);
            m.checkNotNullExpressionValue(copy, "bitmap.copy(Bitmap.Config.ARGB_8888, false)");
            function2.invoke(copy, this.a.$newAvatarUrl);
        }
    }
}
