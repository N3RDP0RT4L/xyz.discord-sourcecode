package b.a.y;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.content.Context;
import android.content.res.Configuration;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import b.a.i.z0;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.app.AppComponent;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.text.TextWatcher;
import com.discord.widgets.user.search.WidgetGlobalSearchAdapter;
import com.discord.widgets.user.search.WidgetGlobalSearchModel;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.List;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import xyz.discord.R;
/* compiled from: OverlayVoiceSelectorBubbleDialog.kt */
/* loaded from: classes2.dex */
public final class z extends l implements AppComponent {
    public final WidgetGlobalSearchAdapter A;
    public final BehaviorSubject<String> B = BehaviorSubject.l0("");
    public String C;

    /* renamed from: z  reason: collision with root package name */
    public z0 f314z;

    /* compiled from: OverlayVoiceSelectorBubbleDialog.kt */
    /* loaded from: classes2.dex */
    public static final class a implements View.OnClickListener {
        public a() {
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            z.this.g();
        }
    }

    /* compiled from: OverlayVoiceSelectorBubbleDialog.kt */
    /* loaded from: classes2.dex */
    public static final class b extends o implements Function1<Editable, Unit> {
        public b() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(Editable editable) {
            Editable editable2 = editable;
            m.checkNotNullParameter(editable2, "editable");
            z.this.B.onNext(editable2.toString());
            return Unit.a;
        }
    }

    /* compiled from: OverlayVoiceSelectorBubbleDialog.kt */
    /* loaded from: classes2.dex */
    public static final class c<T, R> implements j0.k.b<String, String> {
        public static final c j = new c();

        @Override // j0.k.b
        public String call(String str) {
            return '!' + str;
        }
    }

    /* compiled from: OverlayVoiceSelectorBubbleDialog.kt */
    /* loaded from: classes2.dex */
    public static final class d extends o implements Function1<WidgetGlobalSearchModel, Unit> {
        public d() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(WidgetGlobalSearchModel widgetGlobalSearchModel) {
            WidgetGlobalSearchModel widgetGlobalSearchModel2 = widgetGlobalSearchModel;
            m.checkNotNullParameter(widgetGlobalSearchModel2, "it");
            z zVar = z.this;
            TextView textView = zVar.f314z.d;
            m.checkNotNullExpressionValue(textView, "binding.emptyResults");
            int i = 8;
            textView.setVisibility(widgetGlobalSearchModel2.getData().isEmpty() ? 0 : 8);
            RecyclerView recyclerView = zVar.f314z.f;
            m.checkNotNullExpressionValue(recyclerView, "binding.resultsRv");
            if (!widgetGlobalSearchModel2.getData().isEmpty()) {
                i = 0;
            }
            recyclerView.setVisibility(i);
            WidgetGlobalSearchAdapter widgetGlobalSearchAdapter = zVar.A;
            widgetGlobalSearchAdapter.setOnUpdated(new x(zVar, widgetGlobalSearchModel2));
            List<WidgetGlobalSearchModel.ItemDataPayload> data = widgetGlobalSearchModel2.getData();
            ArrayList arrayList = new ArrayList();
            for (Object obj : data) {
                Channel channel = ((WidgetGlobalSearchModel.ItemDataPayload) obj).getChannel();
                if (channel != null && ChannelUtils.E(channel)) {
                    arrayList.add(obj);
                }
            }
            widgetGlobalSearchAdapter.setData(arrayList);
            widgetGlobalSearchAdapter.setOnSelectedListener(new y(zVar, widgetGlobalSearchModel2));
            return Unit.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public z(Context context) {
        super(context);
        m.checkNotNullParameter(context, "context");
        getWindowLayoutParams().flags &= -9;
        z0 a2 = z0.a(LayoutInflater.from(getContext()), this, true);
        m.checkNotNullExpressionValue(a2, "OverlayVoiceChannelSelec…rom(context), this, true)");
        this.f314z = a2;
        RecyclerView recyclerView = this.f314z.f;
        m.checkNotNullExpressionValue(recyclerView, "binding.resultsRv");
        this.A = new WidgetGlobalSearchAdapter(recyclerView);
    }

    @Override // b.a.y.l
    public Animator getClosingAnimator() {
        Animator loadAnimator = AnimatorInflater.loadAnimator(getContext(), R.animator.overlay_slide_down_fade_out);
        loadAnimator.setTarget(this.f314z.c);
        m.checkNotNullExpressionValue(loadAnimator, "AnimatorInflater.loadAni…binding.dialogCard)\n    }");
        return loadAnimator;
    }

    public final void h() {
        this.f314z.f234b.setOnClickListener(new a());
        TextInputLayout textInputLayout = this.f314z.e;
        m.checkNotNullExpressionValue(textInputLayout, "binding.overlayChannelSearch");
        ViewExtensions.setText(textInputLayout, this.C);
        TextInputLayout textInputLayout2 = this.f314z.e;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.overlayChannelSearch");
        EditText editText = textInputLayout2.getEditText();
        if (editText != null) {
            editText.addTextChangedListener(new TextWatcher(null, null, new b(), 3, null));
        }
        WidgetGlobalSearchAdapter widgetGlobalSearchAdapter = this.A;
        RecyclerView recyclerView = this.f314z.f;
        m.checkNotNullExpressionValue(recyclerView, "binding.resultsRv");
        widgetGlobalSearchAdapter.setRecycler(recyclerView);
        MGRecyclerAdapter.Companion.configure(this.A);
        WidgetGlobalSearchModel.Companion companion = WidgetGlobalSearchModel.Companion;
        Observable<String> F = this.B.F(c.j);
        m.checkNotNullExpressionValue(F, "filterPublisher.map { \"!$it\" }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(companion.getForNav(F), this, this.A), z.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new d());
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        h();
        Animator loadAnimator = AnimatorInflater.loadAnimator(getContext(), R.animator.overlay_slide_up_fade_in);
        loadAnimator.setTarget(this.f314z.c);
        loadAnimator.start();
    }

    @Override // com.discord.overlay.views.OverlayDialog, com.discord.overlay.views.OverlayBubbleWrap, android.view.View
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        getUnsubscribeSignal().onNext(null);
        removeAllViewsInLayout();
        z0 a2 = z0.a(LayoutInflater.from(getContext()), this, true);
        m.checkNotNullExpressionValue(a2, "OverlayVoiceChannelSelec…rom(context), this, true)");
        this.f314z = a2;
        h();
    }
}
