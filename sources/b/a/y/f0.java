package b.a.y;

import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import b.a.i.d;
import b.a.k.b;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.views.SimpleRecyclerAdapter;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.textview.MaterialTextView;
import d0.z.d.m;
import xyz.discord.R;
/* compiled from: SelectorBottomSheet.kt */
/* loaded from: classes2.dex */
public final class f0 extends SimpleRecyclerAdapter.ViewHolder<d0> {
    public final i a;

    /* renamed from: b  reason: collision with root package name */
    public final DialogInterface f307b;
    public final d c;

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public f0(b.a.y.i r3, android.content.DialogInterface r4, b.a.i.d r5) {
        /*
            r2 = this;
            java.lang.String r0 = "onSelectedListener"
            d0.z.d.m.checkNotNullParameter(r3, r0)
            java.lang.String r0 = "dialogInterface"
            d0.z.d.m.checkNotNullParameter(r4, r0)
            java.lang.String r0 = "itemBinding"
            d0.z.d.m.checkNotNullParameter(r5, r0)
            androidx.constraintlayout.widget.ConstraintLayout r0 = r5.a
            java.lang.String r1 = "itemBinding.root"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            r2.<init>(r0)
            r2.a = r3
            r2.f307b = r4
            r2.c = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.y.f0.<init>(b.a.y.i, android.content.DialogInterface, b.a.i.d):void");
    }

    @Override // com.discord.utilities.views.SimpleRecyclerAdapter.ViewHolder
    public void bind(d0 d0Var) {
        int i;
        d0 d0Var2 = d0Var;
        m.checkNotNullParameter(d0Var2, "data");
        MaterialTextView materialTextView = this.c.d;
        m.checkNotNullExpressionValue(materialTextView, "itemBinding.selectComponentSheetItemTitle");
        b.a(materialTextView, d0Var2.e());
        MaterialTextView materialTextView2 = this.c.f94b;
        m.checkNotNullExpressionValue(materialTextView2, "itemBinding.selectComponentSheetItemDescription");
        ViewExtensions.setTextAndVisibilityBy(materialTextView2, d0Var2.a());
        SimpleDraweeView simpleDraweeView = this.c.c;
        m.checkNotNullExpressionValue(simpleDraweeView, "itemBinding.selectComponentSheetItemIcon");
        int i2 = 0;
        if (!((d0Var2.d() == null && d0Var2.b() == null) ? false : true)) {
            i2 = 8;
        }
        simpleDraweeView.setVisibility(i2);
        String d = d0Var2.d();
        if (d != null) {
            SimpleDraweeView simpleDraweeView2 = this.c.c;
            m.checkNotNullExpressionValue(simpleDraweeView2, "itemBinding.selectComponentSheetItemIcon");
            MGImages.setImage$default(simpleDraweeView2, d, R.dimen.emoji_size, R.dimen.emoji_size, true, null, null, 96, null);
        } else {
            Integer b2 = d0Var2.b();
            if (b2 != null) {
                this.c.c.setImageResource(b2.intValue());
            }
        }
        Integer c = d0Var2.c();
        if (c != null) {
            this.c.c.setColorFilter(c.intValue());
        }
        MaterialTextView materialTextView3 = this.c.d;
        materialTextView3.setText(d0Var2.e());
        Integer g = d0Var2.g();
        if (g != null) {
            i = g.intValue();
        } else {
            m.checkNotNullExpressionValue(materialTextView3, "this");
            i = ColorCompat.getThemedColor(materialTextView3, (int) R.attr.colorHeaderPrimary);
        }
        materialTextView3.setTextColor(i);
        Integer f = d0Var2.f();
        Drawable drawable = null;
        if (f != null) {
            f.intValue();
            Drawable drawable2 = ContextCompat.getDrawable(materialTextView3.getContext(), d0Var2.f().intValue());
            if (drawable2 != null) {
                MaterialTextView materialTextView4 = this.c.d;
                m.checkNotNullExpressionValue(materialTextView4, "itemBinding.selectComponentSheetItemTitle");
                DrawableCompat.setTint(drawable2, ColorCompat.getThemedColor(materialTextView4, (int) R.attr.colorHeaderPrimary));
                drawable = drawable2;
                com.discord.utilities.drawable.DrawableCompat.setCompoundDrawablesCompat$default(materialTextView3, (Drawable) null, (Drawable) null, drawable, (Drawable) null, 11, (Object) null);
                this.c.a.setOnClickListener(new e0(this));
            }
        }
        com.discord.utilities.drawable.DrawableCompat.setCompoundDrawablesCompat$default(materialTextView3, (Drawable) null, (Drawable) null, drawable, (Drawable) null, 11, (Object) null);
        this.c.a.setOnClickListener(new e0(this));
    }
}
