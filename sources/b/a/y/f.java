package b.a.y;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.utilities.color.ColorCompat;
import d0.z.d.m;
import java.util.Objects;
import xyz.discord.R;
/* compiled from: DividerDecoration.kt */
/* loaded from: classes2.dex */
public final class f extends DividerItemDecoration {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final int f306b;
    public final int c;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public f(Context context, int i, int i2, int i3, int i4, int i5) {
        super(context, i);
        i2 = (i5 & 4) != 0 ? ColorCompat.getThemedColor(context, (int) R.attr.colorPrimaryDivider) : i2;
        i3 = (i5 & 8) != 0 ? 0 : i3;
        i4 = (i5 & 16) != 0 ? 0 : i4;
        m.checkNotNullParameter(context, "context");
        this.a = i2;
        this.f306b = i3;
        this.c = i4;
        Drawable drawable = ContextCompat.getDrawable(context, R.drawable.drawable_divider);
        if (drawable != null) {
            Drawable wrap = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(wrap, i2);
            setDrawable(wrap);
        }
    }

    @Override // androidx.recyclerview.widget.DividerItemDecoration, androidx.recyclerview.widget.RecyclerView.ItemDecoration
    public void onDraw(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        m.checkNotNullParameter(canvas, "c");
        m.checkNotNullParameter(recyclerView, "parent");
        m.checkNotNullParameter(state, "state");
        int paddingLeft = recyclerView.getPaddingLeft() + this.f306b;
        int width = (recyclerView.getWidth() - recyclerView.getPaddingRight()) - this.c;
        Drawable drawable = getDrawable();
        int childCount = recyclerView.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = recyclerView.getChildAt(i);
            m.checkNotNullExpressionValue(childAt, "parent.getChildAt(i)");
            int bottom = childAt.getBottom();
            ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
            Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView.LayoutParams");
            int i2 = bottom + ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) layoutParams)).bottomMargin;
            int intrinsicHeight = (drawable != null ? drawable.getIntrinsicHeight() : 0) + i2;
            if (drawable != null) {
                drawable.setBounds(paddingLeft, i2, width, intrinsicHeight);
            }
            if (drawable != null) {
                drawable.draw(canvas);
            }
        }
    }
}
