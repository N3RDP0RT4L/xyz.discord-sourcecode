package b.a.y;

import com.discord.api.channel.Channel;
import com.discord.models.guild.Guild;
import com.discord.rtcconnection.RtcConnection;
import com.discord.stores.StoreStream;
import com.discord.views.OverlayMenuView;
import com.discord.widgets.voice.model.CallModel;
import j0.k.b;
import j0.l.e.k;
import rx.Observable;
import rx.functions.Func4;
/* compiled from: OverlayMenuView.kt */
/* loaded from: classes2.dex */
public final class r<T, R> implements b<Channel, Observable<? extends OverlayMenuView.a>> {
    public static final r j = new r();

    @Override // j0.k.b
    public Observable<? extends OverlayMenuView.a> call(Channel channel) {
        Channel channel2 = channel;
        if (channel2 == null) {
            return new k(null);
        }
        StoreStream.Companion companion = StoreStream.Companion;
        Observable<Long> observePermissionsForChannel = companion.getPermissions().observePermissionsForChannel(channel2.h());
        Observable<Guild> observeGuild = companion.getGuilds().observeGuild(channel2.f());
        Observable<CallModel> observable = CallModel.Companion.get(channel2.h());
        Observable<RtcConnection.Quality> quality = companion.getRtcConnection().getQuality();
        q qVar = q.j;
        Object obj = qVar;
        if (qVar != null) {
            obj = new u(qVar);
        }
        return Observable.h(observePermissionsForChannel, observeGuild, observable, quality, (Func4) obj);
    }
}
