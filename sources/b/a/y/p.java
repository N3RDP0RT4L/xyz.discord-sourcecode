package b.a.y;

import com.discord.api.channel.Channel;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreVoiceParticipants;
import d0.t.n;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.List;
import rx.Observable;
/* compiled from: OverlayMenuBubbleDialog.kt */
/* loaded from: classes2.dex */
public final class p<T, R> implements b<Channel, Observable<? extends List<? extends StoreVoiceParticipants.VoiceUser>>> {
    public final /* synthetic */ Long j;

    public p(Long l) {
        this.j = l;
    }

    @Override // j0.k.b
    public Observable<? extends List<? extends StoreVoiceParticipants.VoiceUser>> call(Channel channel) {
        Channel channel2 = channel;
        if (channel2 == null) {
            return new k(n.emptyList());
        }
        StoreVoiceParticipants voiceParticipants = StoreStream.Companion.getVoiceParticipants();
        Long l = this.j;
        m.checkNotNullExpressionValue(l, "channelId");
        return (Observable<R>) voiceParticipants.get(l.longValue()).F(new o(channel2)).q();
    }
}
