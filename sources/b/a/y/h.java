package b.a.y;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import b.a.i.v0;
import com.discord.views.OAuthPermissionViews;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import kotlin.jvm.functions.Function2;
import xyz.discord.R;
/* compiled from: OAuthPermissionViews.kt */
/* loaded from: classes2.dex */
public final class h extends o implements Function2<LayoutInflater, ViewGroup, OAuthPermissionViews.a> {
    public static final h j = new h();

    public h() {
        super(2);
    }

    @Override // kotlin.jvm.functions.Function2
    public OAuthPermissionViews.a invoke(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        LayoutInflater layoutInflater2 = layoutInflater;
        ViewGroup viewGroup2 = viewGroup;
        m.checkNotNullParameter(layoutInflater2, "inflater");
        m.checkNotNullParameter(viewGroup2, "parent");
        View inflate = layoutInflater2.inflate(R.layout.oauth_token_permission_list_item, viewGroup2, false);
        Objects.requireNonNull(inflate, "rootView");
        v0 v0Var = new v0((TextView) inflate);
        m.checkNotNullExpressionValue(v0Var, "OauthTokenPermissionList…(inflater, parent, false)");
        return new OAuthPermissionViews.a(v0Var);
    }
}
