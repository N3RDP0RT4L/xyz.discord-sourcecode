package b.a.y.p0;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import b.a.i.a5;
import b.a.k.b;
import b.a.y.p0.d;
import b.d.b.a.a;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.utilities.resources.StringResourceUtilsKt;
import d0.z.d.m;
import xyz.discord.R;
/* compiled from: SpeakersViewHolder.kt */
/* loaded from: classes2.dex */
public final class b extends MGRecyclerViewHolder<c, d> {
    public final a5 a;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public b(c cVar) {
        super((int) R.layout.widget_discovery_other_speakers_count, cVar);
        m.checkNotNullParameter(cVar, "adapter");
        View view = this.itemView;
        TextView textView = (TextView) view.findViewById(R.id.other_speakers_count_text);
        if (textView != null) {
            a5 a5Var = new a5((LinearLayout) view, textView);
            m.checkNotNullExpressionValue(a5Var, "WidgetDiscoveryOtherSpea…untBinding.bind(itemView)");
            this.a = a5Var;
            return;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(R.id.other_speakers_count_text)));
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerViewHolder
    public void onConfigure(int i, d dVar) {
        CharSequence d;
        d dVar2 = dVar;
        m.checkNotNullParameter(dVar2, "data");
        super.onConfigure(i, dVar2);
        int i2 = ((d.b) dVar2).l;
        TextView textView = this.a.f78b;
        m.checkNotNullExpressionValue(textView, "binding.otherSpeakersCountText");
        View view = this.itemView;
        m.checkNotNullExpressionValue(view, "itemView");
        d = b.a.k.b.d(view, R.string.discovery_speaker_count, new Object[]{StringResourceUtilsKt.getI18nPluralString(a.x(this.itemView, "itemView", "itemView.context"), R.plurals.discovery_speaker_count_speakersCount, i2, Integer.valueOf(i2))}, (r4 & 4) != 0 ? b.c.j : null);
        textView.setText(d);
    }
}
