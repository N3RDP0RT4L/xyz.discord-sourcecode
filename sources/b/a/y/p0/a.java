package b.a.y.p0;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import b.a.i.b5;
import b.a.y.p0.d;
import com.discord.utilities.extensions.SimpleDraweeViewExtensionsKt;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import xyz.discord.R;
/* compiled from: SpeakersViewHolder.kt */
/* loaded from: classes2.dex */
public final class a extends MGRecyclerViewHolder<c, d> {
    public final b5 a;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public a(c cVar) {
        super((int) R.layout.widget_discovery_stage_card_speaker, cVar);
        m.checkNotNullParameter(cVar, "adapter");
        View view = this.itemView;
        int i = R.id.discovery_stage_card_speaker_avatar;
        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.discovery_stage_card_speaker_avatar);
        if (simpleDraweeView != null) {
            i = R.id.discovery_stage_card_speaker_name;
            TextView textView = (TextView) view.findViewById(R.id.discovery_stage_card_speaker_name);
            if (textView != null) {
                b5 b5Var = new b5((LinearLayout) view, simpleDraweeView, textView);
                m.checkNotNullExpressionValue(b5Var, "WidgetDiscoveryStageCard…kerBinding.bind(itemView)");
                this.a = b5Var;
                return;
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerViewHolder
    public void onConfigure(int i, d dVar) {
        d dVar2 = dVar;
        m.checkNotNullParameter(dVar2, "data");
        super.onConfigure(i, dVar2);
        d.a aVar = (d.a) dVar2;
        SimpleDraweeView simpleDraweeView = this.a.f86b;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.discoveryStageCardSpeakerAvatar");
        SimpleDraweeViewExtensionsKt.setAvatar$default(simpleDraweeView, aVar.l, false, R.dimen.avatar_size_medium, aVar.m, 2, null);
        TextView textView = this.a.c;
        m.checkNotNullExpressionValue(textView, "binding.discoveryStageCardSpeakerName");
        textView.setText(aVar.n);
    }
}
