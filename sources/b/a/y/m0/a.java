package b.a.y.m0;

import android.view.View;
import android.widget.AdapterView;
import kotlin.jvm.functions.Function1;
/* compiled from: ExperimentOverrideView.kt */
/* loaded from: classes2.dex */
public final class a implements AdapterView.OnItemSelectedListener {
    public final /* synthetic */ Function1 j;

    public a(Function1 function1) {
        this.j = function1;
    }

    @Override // android.widget.AdapterView.OnItemSelectedListener
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
        if (j != -1) {
            this.j.invoke(Integer.valueOf((int) j));
        }
    }

    @Override // android.widget.AdapterView.OnItemSelectedListener
    public void onNothingSelected(AdapterView<?> adapterView) {
    }
}
