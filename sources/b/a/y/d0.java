package b.a.y;

import b.d.b.a.a;
import d0.z.d.m;
import java.io.Serializable;
import org.objectweb.asm.Opcodes;
/* compiled from: SelectorBottomSheet.kt */
/* loaded from: classes2.dex */
public final class d0 implements Serializable {
    private final CharSequence description;
    private final Integer iconRes;
    private final Integer iconTint;
    private final String iconUri;
    private final String title;
    private final Integer titleEndIcon;
    private final Integer titleTextColor;

    public d0() {
        this(null, null, null, null, null, null, null, Opcodes.LAND);
    }

    public d0(String str, CharSequence charSequence, Integer num, String str2, Integer num2, Integer num3, Integer num4, int i) {
        str = (i & 1) != 0 ? null : str;
        charSequence = (i & 2) != 0 ? null : charSequence;
        num = (i & 4) != 0 ? null : num;
        str2 = (i & 8) != 0 ? null : str2;
        int i2 = i & 16;
        num3 = (i & 32) != 0 ? null : num3;
        int i3 = i & 64;
        this.title = str;
        this.description = charSequence;
        this.iconRes = num;
        this.iconUri = str2;
        this.iconTint = null;
        this.titleTextColor = num3;
        this.titleEndIcon = null;
    }

    public final CharSequence a() {
        return this.description;
    }

    public final Integer b() {
        return this.iconRes;
    }

    public final Integer c() {
        return this.iconTint;
    }

    public final String d() {
        return this.iconUri;
    }

    public final String e() {
        return this.title;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof d0)) {
            return false;
        }
        d0 d0Var = (d0) obj;
        return m.areEqual(this.title, d0Var.title) && m.areEqual(this.description, d0Var.description) && m.areEqual(this.iconRes, d0Var.iconRes) && m.areEqual(this.iconUri, d0Var.iconUri) && m.areEqual(this.iconTint, d0Var.iconTint) && m.areEqual(this.titleTextColor, d0Var.titleTextColor) && m.areEqual(this.titleEndIcon, d0Var.titleEndIcon);
    }

    public final Integer f() {
        return this.titleEndIcon;
    }

    public final Integer g() {
        return this.titleTextColor;
    }

    public int hashCode() {
        String str = this.title;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        CharSequence charSequence = this.description;
        int hashCode2 = (hashCode + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Integer num = this.iconRes;
        int hashCode3 = (hashCode2 + (num != null ? num.hashCode() : 0)) * 31;
        String str2 = this.iconUri;
        int hashCode4 = (hashCode3 + (str2 != null ? str2.hashCode() : 0)) * 31;
        Integer num2 = this.iconTint;
        int hashCode5 = (hashCode4 + (num2 != null ? num2.hashCode() : 0)) * 31;
        Integer num3 = this.titleTextColor;
        int hashCode6 = (hashCode5 + (num3 != null ? num3.hashCode() : 0)) * 31;
        Integer num4 = this.titleEndIcon;
        if (num4 != null) {
            i = num4.hashCode();
        }
        return hashCode6 + i;
    }

    public String toString() {
        StringBuilder R = a.R("SimpleBottomSheetItem(title=");
        R.append(this.title);
        R.append(", description=");
        R.append(this.description);
        R.append(", iconRes=");
        R.append(this.iconRes);
        R.append(", iconUri=");
        R.append(this.iconUri);
        R.append(", iconTint=");
        R.append(this.iconTint);
        R.append(", titleTextColor=");
        R.append(this.titleTextColor);
        R.append(", titleEndIcon=");
        return a.E(R, this.titleEndIcon, ")");
    }
}
