package b.a.y;

import com.discord.overlay.views.OverlayDialog;
import d0.z.d.o;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: OverlayAppDialog.kt */
/* loaded from: classes2.dex */
public final class m extends o implements Function1<OverlayDialog, Unit> {
    public static final m j = new m();

    public m() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public Unit invoke(OverlayDialog overlayDialog) {
        d0.z.d.m.checkNotNullParameter(overlayDialog, "it");
        return Unit.a;
    }
}
