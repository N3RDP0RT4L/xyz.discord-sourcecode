package b.a.y.q0;

import d0.z.d.o;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StepsView.kt */
/* loaded from: classes2.dex */
public final class a extends o implements Function1<Integer, Unit> {
    public static final a j = new a();

    public a() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public Unit invoke(Integer num) {
        num.intValue();
        return Unit.a;
    }
}
