package b.a.y;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import b.a.i.x0;
import com.discord.views.OverlayMenuBubbleDialog;
import com.discord.views.VoiceUserView;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import kotlin.jvm.functions.Function2;
import xyz.discord.R;
/* compiled from: OverlayMenuBubbleDialog.kt */
/* loaded from: classes2.dex */
public final class n extends o implements Function2<LayoutInflater, ViewGroup, OverlayMenuBubbleDialog.a> {
    public static final n j = new n();

    public n() {
        super(2);
    }

    @Override // kotlin.jvm.functions.Function2
    public OverlayMenuBubbleDialog.a invoke(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        LayoutInflater layoutInflater2 = layoutInflater;
        ViewGroup viewGroup2 = viewGroup;
        m.checkNotNullParameter(layoutInflater2, "layoutInflater");
        m.checkNotNullParameter(viewGroup2, "parent");
        View inflate = layoutInflater2.inflate(R.layout.overlay_menu_voice_members_item, viewGroup2, false);
        Objects.requireNonNull(inflate, "rootView");
        x0 x0Var = new x0((VoiceUserView) inflate);
        m.checkNotNullExpressionValue(x0Var, "OverlayMenuVoiceMembersI…tInflater, parent, false)");
        return new OverlayMenuBubbleDialog.a(x0Var);
    }
}
