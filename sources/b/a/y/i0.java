package b.a.y;

import com.discord.views.VoiceUserView;
import d0.z.d.o;
import kotlin.jvm.functions.Function0;
/* compiled from: VoiceUserView.kt */
/* loaded from: classes2.dex */
public final class i0 extends o implements Function0<Boolean> {
    public final /* synthetic */ VoiceUserView this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public i0(VoiceUserView voiceUserView) {
        super(0);
        this.this$0 = voiceUserView;
    }

    @Override // kotlin.jvm.functions.Function0
    public Boolean invoke() {
        VoiceUserView voiceUserView = this.this$0;
        return Boolean.valueOf(voiceUserView.m == VoiceUserView.a.RINGING && voiceUserView.f2808s);
    }
}
