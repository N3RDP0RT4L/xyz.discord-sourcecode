package b.a.y.r0;

import android.content.Context;
import android.widget.ImageView;
import com.discord.api.sticker.BaseSticker;
import com.discord.rlottie.RLottieDrawable;
import com.discord.rlottie.RLottieImageView;
import com.discord.utilities.file.DownloadUtils;
import com.discord.utilities.stickers.StickerUtils;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.views.sticker.StickerView;
import d0.z.d.m;
import d0.z.d.o;
import java.io.File;
import java.util.Objects;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StickerView.kt */
/* loaded from: classes2.dex */
public final class h extends o implements Function1<Pair<? extends DownloadUtils.DownloadState, ? extends Integer>, Unit> {
    public final /* synthetic */ Integer $localAnimationSettings;
    public final /* synthetic */ BaseSticker $sticker;
    public final /* synthetic */ StickerView this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public h(StickerView stickerView, Integer num, BaseSticker baseSticker) {
        super(1);
        this.this$0 = stickerView;
        this.$localAnimationSettings = num;
        this.$sticker = baseSticker;
    }

    @Override // kotlin.jvm.functions.Function1
    public Unit invoke(Pair<? extends DownloadUtils.DownloadState, ? extends Integer> pair) {
        Pair<? extends DownloadUtils.DownloadState, ? extends Integer> pair2 = pair;
        DownloadUtils.DownloadState component1 = pair2.component1();
        Integer component2 = pair2.component2();
        if (component1 instanceof DownloadUtils.DownloadState.Completed) {
            Integer num = this.$localAnimationSettings;
            if (num != null) {
                component2 = num;
            }
            RLottieDrawable.PlaybackMode playbackMode = (component2 != null && component2.intValue() == 0) ? RLottieDrawable.PlaybackMode.LOOP : RLottieDrawable.PlaybackMode.FREEZE;
            ImageView imageView = this.this$0.j.d;
            m.checkNotNullExpressionValue(imageView, "binding.stickerViewPlaceholder");
            imageView.setVisibility(8);
            RLottieImageView rLottieImageView = this.this$0.j.c;
            m.checkNotNullExpressionValue(rLottieImageView, "binding.stickerViewLottie");
            rLottieImageView.setContentDescription(this.this$0.c(this.$sticker));
            StickerView stickerView = this.this$0;
            RLottieImageView rLottieImageView2 = stickerView.j.c;
            Context context = stickerView.getContext();
            m.checkNotNullExpressionValue(context, "context");
            File file = ((DownloadUtils.DownloadState.Completed) component1).getFile();
            StickerUtils stickerUtils = StickerUtils.INSTANCE;
            int default_sticker_size_px = stickerUtils.getDEFAULT_STICKER_SIZE_PX();
            int default_sticker_size_px2 = stickerUtils.getDEFAULT_STICKER_SIZE_PX();
            Objects.requireNonNull(rLottieImageView2);
            m.checkParameterIsNotNull(context, "context");
            m.checkParameterIsNotNull(file, "file");
            m.checkParameterIsNotNull(playbackMode, "playbackMode");
            RLottieDrawable rLottieDrawable = new RLottieDrawable(file, default_sticker_size_px, default_sticker_size_px2, false, true, rLottieImageView2.a(context).getRefreshRate(), (int[]) null, 64);
            rLottieImageView2.j = rLottieDrawable;
            rLottieDrawable.f(playbackMode);
            RLottieDrawable rLottieDrawable2 = rLottieImageView2.j;
            if (rLottieDrawable2 != null) {
                rLottieDrawable2.e(true);
            }
            rLottieImageView2.setImageDrawable(rLottieImageView2.j);
            if (component2 != null && component2.intValue() == 0) {
                this.this$0.j.c.b();
            } else if (component2 != null && component2.intValue() == 1) {
                RLottieImageView rLottieImageView3 = this.this$0.j.c;
                m.checkNotNullExpressionValue(rLottieImageView3, "binding.stickerViewLottie");
                ViewExtensions.setOnLongClickListenerConsumeClick(rLottieImageView3, new g(this));
            } else if (component2 != null && component2.intValue() == 2) {
                this.this$0.j.c.setOnLongClickListener(null);
            }
        }
        return Unit.a;
    }
}
