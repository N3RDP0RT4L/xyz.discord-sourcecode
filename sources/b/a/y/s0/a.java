package b.a.y.s0;

import com.discord.views.typing.TypingDots;
import d0.z.d.o;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: TypingDots.kt */
/* loaded from: classes2.dex */
public final class a extends o implements Function0<Unit> {
    public final /* synthetic */ TypingDots this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public a(TypingDots typingDots) {
        super(0);
        this.this$0 = typingDots;
    }

    @Override // kotlin.jvm.functions.Function0
    public Unit invoke() {
        if (this.this$0.isAttachedToWindow()) {
            this.this$0.a(true);
        }
        return Unit.a;
    }
}
