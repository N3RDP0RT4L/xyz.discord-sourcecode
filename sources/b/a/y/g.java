package b.a.y;

import android.view.View;
import kotlin.jvm.functions.Function0;
/* compiled from: JoinVoiceChannelButton.kt */
/* loaded from: classes2.dex */
public final class g implements View.OnClickListener {
    public final /* synthetic */ Function0 j;

    public g(Function0 function0) {
        this.j = function0;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.j.invoke();
    }
}
