package b.a.y;

import com.discord.api.channel.Channel;
import com.discord.stores.StoreVoiceParticipants;
import j0.k.b;
import java.util.List;
import java.util.Map;
/* compiled from: OverlayMenuBubbleDialog.kt */
/* loaded from: classes2.dex */
public final class o<T, R> implements b<Map<Long, ? extends StoreVoiceParticipants.VoiceUser>, List<? extends StoreVoiceParticipants.VoiceUser>> {
    public final /* synthetic */ Channel j;

    public o(Channel channel) {
        this.j = channel;
    }

    /* JADX WARN: Code restructure failed: missing block: B:14:0x0038, code lost:
        if ((r2 != null ? b.c.a.a0.d.y0(r2) : null) == com.discord.api.voice.state.StageRequestToSpeakState.ON_STAGE) goto L15;
     */
    /* JADX WARN: Removed duplicated region for block: B:21:0x003f A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:23:0x000f A[SYNTHETIC] */
    @Override // j0.k.b
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public java.util.List<? extends com.discord.stores.StoreVoiceParticipants.VoiceUser> call(java.util.Map<java.lang.Long, ? extends com.discord.stores.StoreVoiceParticipants.VoiceUser> r5) {
        /*
            r4 = this;
            java.util.Map r5 = (java.util.Map) r5
            java.util.Collection r5 = r5.values()
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.util.Iterator r5 = r5.iterator()
        Lf:
            boolean r1 = r5.hasNext()
            if (r1 == 0) goto L43
            java.lang.Object r1 = r5.next()
            r2 = r1
            com.discord.stores.StoreVoiceParticipants$VoiceUser r2 = (com.discord.stores.StoreVoiceParticipants.VoiceUser) r2
            boolean r3 = r2.isConnected()
            if (r3 == 0) goto L3c
            com.discord.api.channel.Channel r3 = r4.j
            boolean r3 = com.discord.api.channel.ChannelUtils.z(r3)
            if (r3 == 0) goto L3a
            com.discord.api.voice.state.VoiceState r2 = r2.getVoiceState()
            if (r2 == 0) goto L35
            com.discord.api.voice.state.StageRequestToSpeakState r2 = b.c.a.a0.d.y0(r2)
            goto L36
        L35:
            r2 = 0
        L36:
            com.discord.api.voice.state.StageRequestToSpeakState r3 = com.discord.api.voice.state.StageRequestToSpeakState.ON_STAGE
            if (r2 != r3) goto L3c
        L3a:
            r2 = 1
            goto L3d
        L3c:
            r2 = 0
        L3d:
            if (r2 == 0) goto Lf
            r0.add(r1)
            goto Lf
        L43:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.y.o.call(java.lang.Object):java.lang.Object");
    }
}
