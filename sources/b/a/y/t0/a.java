package b.a.y.t0;

import android.graphics.Bitmap;
import com.discord.views.user.UserAvatarPresenceView;
import d0.z.d.m;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: UserAvatarPresenceView.kt */
/* loaded from: classes2.dex */
public final class a extends b.f.j.q.a {
    public final /* synthetic */ UserAvatarPresenceView.c a;

    public a(UserAvatarPresenceView.c cVar) {
        this.a = cVar;
    }

    @Override // b.f.j.q.a
    public void process(Bitmap bitmap) {
        if (bitmap != null) {
            Function2<? super Bitmap, ? super String, Unit> function2 = UserAvatarPresenceView.this.m;
            Bitmap copy = bitmap.copy(Bitmap.Config.ARGB_8888, false);
            m.checkNotNullExpressionValue(copy, "bitmap.copy(Bitmap.Config.ARGB_8888, false)");
            function2.invoke(copy, this.a.$iconUrl);
        }
    }
}
