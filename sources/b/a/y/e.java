package b.a.y;

import d0.z.d.m;
import d0.z.d.o;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: CodeVerificationView.kt */
/* loaded from: classes2.dex */
public final class e extends o implements Function1<String, Unit> {
    public static final e j = new e();

    public e() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public Unit invoke(String str) {
        m.checkNotNullParameter(str, "it");
        return Unit.a;
    }
}
