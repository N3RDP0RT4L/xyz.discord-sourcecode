package b.a.y;

import android.content.Context;
import android.content.res.Configuration;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import b.a.i.y0;
import com.discord.app.AppComponent;
import com.discord.overlay.views.OverlayBubbleWrap;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreVoiceParticipants;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import d0.z.d.o;
import j0.l.e.k;
import java.util.Objects;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.Observable;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;
import xyz.discord.R;
/* compiled from: OverlayVoiceBubble.kt */
/* loaded from: classes2.dex */
public final class w extends OverlayBubbleWrap implements AppComponent {

    /* renamed from: x  reason: collision with root package name */
    public static final /* synthetic */ int f311x = 0;

    /* renamed from: y  reason: collision with root package name */
    public final y0 f312y;

    /* renamed from: z  reason: collision with root package name */
    public final Subject<Void, Void> f313z;

    /* compiled from: OverlayVoiceBubble.kt */
    /* loaded from: classes2.dex */
    public static final class a<T, R> implements j0.k.b<Long, Observable<? extends StoreVoiceParticipants.VoiceUser>> {
        public static final a j = new a();

        @Override // j0.k.b
        public Observable<? extends StoreVoiceParticipants.VoiceUser> call(Long l) {
            Long l2 = l;
            if (l2 != null && l2.longValue() == 0) {
                return new k(null);
            }
            StoreVoiceParticipants voiceParticipants = StoreStream.Companion.getVoiceParticipants();
            m.checkNotNullExpressionValue(l2, "channelId");
            return (Observable<R>) voiceParticipants.get(l2.longValue()).F(v.j).q();
        }
    }

    /* compiled from: OverlayVoiceBubble.kt */
    /* loaded from: classes2.dex */
    public static final class b extends o implements Function1<StoreVoiceParticipants.VoiceUser, Unit> {
        public b() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(StoreVoiceParticipants.VoiceUser voiceUser) {
            StoreVoiceParticipants.VoiceUser voiceUser2 = voiceUser;
            w wVar = w.this;
            int i = w.f311x;
            if (voiceUser2 == null) {
                wVar.h();
            } else {
                IconUtils.setIcon$default(wVar.getImageView$app_productionGoogleRelease(), voiceUser2.getUser(), 0, null, null, voiceUser2.getGuildMember(), 28, null);
                wVar.g();
            }
            return Unit.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public w(Context context) {
        super(context);
        m.checkNotNullParameter(context, "context");
        View inflate = LayoutInflater.from(getContext()).inflate(R.layout.overlay_voice_bubble, (ViewGroup) this, false);
        addView(inflate);
        Objects.requireNonNull(inflate, "rootView");
        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) inflate;
        y0 y0Var = new y0(simpleDraweeView, simpleDraweeView);
        m.checkNotNullExpressionValue(y0Var, "OverlayVoiceBubbleBindin…rom(context), this, true)");
        this.f312y = y0Var;
        PublishSubject k0 = PublishSubject.k0();
        m.checkNotNullExpressionValue(k0, "PublishSubject.create()");
        this.f313z = k0;
    }

    @Override // com.discord.overlay.views.OverlayBubbleWrap
    public void a(boolean z2) {
        h();
        super.a(z2);
    }

    @Override // com.discord.overlay.views.OverlayBubbleWrap
    public void b(View view) {
        m.checkNotNullParameter(view, "targetView");
        super.b(view);
        g();
    }

    public final void g() {
        getImageView$app_productionGoogleRelease().animate().setDuration(100L).alpha(1.0f).start();
    }

    public final SimpleDraweeView getImageView$app_productionGoogleRelease() {
        SimpleDraweeView simpleDraweeView = this.f312y.f227b;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.overlayBubbleIv");
        return simpleDraweeView;
    }

    @Override // com.discord.app.AppComponent
    public Subject<Void, Void> getUnsubscribeSignal() {
        return this.f313z;
    }

    public final void h() {
        getImageView$app_productionGoogleRelease().animate().setDuration(200L).alpha(0.5f).start();
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Observable<R> Y = StoreStream.Companion.getVoiceChannelSelected().observeSelectedVoiceChannelId().Y(a.j);
        m.checkNotNullExpressionValue(Y, "StoreStream\n        .get…d()\n          }\n        }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(Y), this, null, 2, null), w.class, (r18 & 2) != 0 ? null : getContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new b());
    }

    @Override // com.discord.overlay.views.OverlayBubbleWrap, android.view.View
    public void onConfigurationChanged(Configuration configuration) {
        int i = getWindowLayoutParams().x;
        Context context = getContext();
        m.checkNotNullExpressionValue(context, "context");
        int i2 = i > e(context).centerX() ? Integer.MAX_VALUE : Integer.MIN_VALUE;
        super.onConfigurationChanged(configuration);
        OverlayBubbleWrap.c(this, i2, getWindowLayoutParams().y, null, 4, null);
        getSpringAnimationX().skipToEnd();
        getSpringAnimationY().skipToEnd();
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        getUnsubscribeSignal().onNext(null);
        super.onDetachedFromWindow();
    }
}
