package b.a.y;

import android.view.View;
/* compiled from: SelectorBottomSheet.kt */
/* loaded from: classes2.dex */
public final class e0 implements View.OnClickListener {
    public final /* synthetic */ f0 j;

    public e0(f0 f0Var) {
        this.j = f0Var;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        f0 f0Var = this.j;
        f0Var.a.c(f0Var.getAdapterPosition());
        this.j.f307b.dismiss();
    }
}
