package b.a.y.k0;

import j0.k.b;
/* compiled from: VideoCallParticipantView.kt */
/* loaded from: classes2.dex */
public final class l<T, R> implements b<Integer, Boolean> {
    public static final l j = new l();

    @Override // j0.k.b
    public Boolean call(Integer num) {
        return Boolean.valueOf(num != null);
    }
}
