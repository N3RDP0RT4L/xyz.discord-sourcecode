package b.a.y.k0;

import android.view.View;
import com.discord.views.calls.VideoCallParticipantView;
/* compiled from: VideoCallParticipantView.kt */
/* loaded from: classes2.dex */
public final class n implements View.OnClickListener {
    public final /* synthetic */ VideoCallParticipantView j;
    public final /* synthetic */ VideoCallParticipantView.ParticipantData k;

    public n(VideoCallParticipantView videoCallParticipantView, VideoCallParticipantView.ParticipantData participantData) {
        this.j = videoCallParticipantView;
        this.k = participantData;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        if (this.k.f2812b.getApplicationStream() != null) {
            this.j.n.invoke(this.k.f2812b.getApplicationStream().getEncodedStreamKey());
        }
    }
}
