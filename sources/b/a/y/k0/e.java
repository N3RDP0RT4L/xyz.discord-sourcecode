package b.a.y.k0;

import org.webrtc.RendererCommon;
import rx.subjects.BehaviorSubject;
/* compiled from: RxRendererEvents.kt */
/* loaded from: classes2.dex */
public final class e implements RendererCommon.RendererEvents {
    public final BehaviorSubject<a> j = BehaviorSubject.k0();

    /* compiled from: RxRendererEvents.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final int f308b;
        public final int c;

        public a(int i, int i2, int i3) {
            this.a = i;
            this.f308b = i2;
            this.c = i3;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return this.a == aVar.a && this.f308b == aVar.f308b && this.c == aVar.c;
        }

        public int hashCode() {
            return (((this.a * 31) + this.f308b) * 31) + this.c;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("Resolution(width=");
            R.append(this.a);
            R.append(", height=");
            R.append(this.f308b);
            R.append(", rotation=");
            return b.d.b.a.a.A(R, this.c, ")");
        }
    }

    @Override // org.webrtc.RendererCommon.RendererEvents
    public void onFirstFrameRendered() {
    }

    @Override // org.webrtc.RendererCommon.RendererEvents
    public void onFrameResolutionChanged(int i, int i2, int i3) {
        this.j.onNext(new a(i, i2, i3));
    }
}
