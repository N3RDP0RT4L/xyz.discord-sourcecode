package b.a.y.k0;

import com.discord.views.calls.StageCallSpeakerView;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.Subscription;
/* compiled from: StageCallSpeakerView.kt */
/* loaded from: classes2.dex */
public final class h extends o implements Function1<Subscription, Unit> {
    public final /* synthetic */ StageCallSpeakerView this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public h(StageCallSpeakerView stageCallSpeakerView) {
        super(1);
        this.this$0 = stageCallSpeakerView;
    }

    @Override // kotlin.jvm.functions.Function1
    public Unit invoke(Subscription subscription) {
        Subscription subscription2 = subscription;
        m.checkNotNullParameter(subscription2, "it");
        this.this$0.k = subscription2;
        return Unit.a;
    }
}
