package b.a.y.k0;

import d0.z.d.m;
import d0.z.d.o;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: VideoCallParticipantView.kt */
/* loaded from: classes2.dex */
public final class j extends o implements Function1<String, Unit> {
    public static final j j = new j();

    public j() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public Unit invoke(String str) {
        m.checkNotNullParameter(str, "it");
        return Unit.a;
    }
}
