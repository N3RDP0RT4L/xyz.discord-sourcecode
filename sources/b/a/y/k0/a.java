package b.a.y.k0;

import android.graphics.Point;
import d0.z.d.m;
import j0.k.b;
import java.util.concurrent.TimeUnit;
import kotlin.Unit;
import rx.Observable;
/* compiled from: AppVideoStreamRenderer.kt */
/* loaded from: classes2.dex */
public final class a<T, R> implements b<Unit, Observable<? extends Point>> {
    public final /* synthetic */ e j;

    public a(e eVar) {
        this.j = eVar;
    }

    @Override // j0.k.b
    public Observable<? extends Point> call(Unit unit) {
        Observable observable = (Observable<R>) this.j.j.O(50L, TimeUnit.MILLISECONDS).F(f.j).K();
        m.checkNotNullExpressionValue(observable, "frameResolutionSubject\n …  .onBackpressureLatest()");
        return observable;
    }
}
