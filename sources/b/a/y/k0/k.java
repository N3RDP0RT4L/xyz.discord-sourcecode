package b.a.y.k0;

import android.graphics.Bitmap;
import com.discord.utilities.colors.RepresentativeColorsKt;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: VideoCallParticipantView.kt */
/* loaded from: classes2.dex */
public final class k extends o implements Function2<Bitmap, String, Unit> {
    public final /* synthetic */ String $colorId;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public k(String str) {
        super(2);
        this.$colorId = str;
    }

    @Override // kotlin.jvm.functions.Function2
    public Unit invoke(Bitmap bitmap, String str) {
        Bitmap bitmap2 = bitmap;
        m.checkNotNullParameter(bitmap2, "bitmap");
        RepresentativeColorsKt.getUserRepresentativeColors().handleBitmap(this.$colorId, bitmap2, str);
        return Unit.a;
    }
}
