package b.a.y.k0;

import com.discord.views.calls.AppVideoStreamRenderer;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.Subscription;
/* compiled from: AppVideoStreamRenderer.kt */
/* loaded from: classes2.dex */
public final class c extends o implements Function1<Subscription, Unit> {
    public final /* synthetic */ AppVideoStreamRenderer this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public c(AppVideoStreamRenderer appVideoStreamRenderer) {
        super(1);
        this.this$0 = appVideoStreamRenderer;
    }

    @Override // kotlin.jvm.functions.Function1
    public Unit invoke(Subscription subscription) {
        Subscription subscription2 = subscription;
        m.checkNotNullParameter(subscription2, "it");
        this.this$0.l = subscription2;
        return Unit.a;
    }
}
