package b.a.y.k0;

import android.graphics.Point;
import b.a.y.k0.e;
import j0.k.b;
/* compiled from: RxRendererEvents.kt */
/* loaded from: classes2.dex */
public final class f<T, R> implements b<e.a, Point> {
    public static final f j = new f();

    @Override // j0.k.b
    public Point call(e.a aVar) {
        e.a aVar2 = aVar;
        if (aVar2 == null) {
            return null;
        }
        int i = aVar2.c;
        if (i == -180 || i == 0 || i == 180) {
            return new Point(aVar2.a, aVar2.f308b);
        }
        return new Point(aVar2.f308b, aVar2.a);
    }
}
