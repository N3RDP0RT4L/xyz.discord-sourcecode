package b.a.y;

import andhook.lib.HookHelper;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.os.BundleKt;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.i.d;
import com.discord.app.AppBottomSheet;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.view.recycler.MaxHeightRecyclerView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.utilities.views.SimpleRecyclerAdapter;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.textview.MaterialTextView;
import d0.z.d.a0;
import d0.z.d.k;
import d0.z.d.m;
import d0.z.d.o;
import java.io.Serializable;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: SelectorBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 %2\u00020\u00012\u00020\u0002:\u0001&B\u0007¢\u0006\u0004\b$\u0010\u000eJ\u000f\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0004\u0010\u0005J!\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0007\u001a\u00020\u00062\b\u0010\t\u001a\u0004\u0018\u00010\bH\u0016¢\u0006\u0004\b\u000b\u0010\fJ\u000f\u0010\r\u001a\u00020\nH\u0016¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0011\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u000fH\u0016¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0014\u001a\u00020\n2\u0006\u0010\u0013\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0014\u0010\u0015R0\u0010\u001d\u001a\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\n\u0018\u00010\u00168\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u001d\u0010#\u001a\u00020\u001e8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"¨\u0006'"}, d2 = {"Lb/a/y/c0;", "Lb/a/y/i;", "Lcom/discord/app/AppBottomSheet;", "", "getContentViewResId", "()I", "Landroid/view/View;", "view", "Landroid/os/Bundle;", "savedInstanceState", "", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "onPause", "()V", "Landroid/content/DialogInterface;", "dialog", "onDismiss", "(Landroid/content/DialogInterface;)V", ModelAuditLogEntry.CHANGE_KEY_POSITION, "c", "(I)V", "Lkotlin/Function1;", "l", "Lkotlin/jvm/functions/Function1;", "getOnSelectedListener", "()Lkotlin/jvm/functions/Function1;", "setOnSelectedListener", "(Lkotlin/jvm/functions/Function1;)V", "onSelectedListener", "Lb/a/i/c;", "m", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "g", "()Lb/a/i/c;", "binding", HookHelper.constructorName, "k", "a", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class c0 extends AppBottomSheet implements i {
    public static final /* synthetic */ KProperty[] j = {b.d.b.a.a.b0(c0.class, "binding", "getBinding()Lcom/discord/databinding/BottomSheetSimpleSelectorBinding;", 0)};
    public static final a k = new a(null);
    public Function1<? super Integer, Unit> l;
    public final FragmentViewBindingDelegate m = FragmentViewBindingDelegateKt.viewBinding$default(this, b.j, null, 2, null);

    /* compiled from: SelectorBottomSheet.kt */
    /* loaded from: classes2.dex */
    public static final class a {

        /* compiled from: SelectorBottomSheet.kt */
        /* renamed from: b.a.y.c0$a$a */
        /* loaded from: classes2.dex */
        public static final class C0056a extends o implements Function1<Integer, Unit> {
            public static final C0056a j = new C0056a();

            public C0056a() {
                super(1);
            }

            @Override // kotlin.jvm.functions.Function1
            public Unit invoke(Integer num) {
                num.intValue();
                return Unit.a;
            }
        }

        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public static /* synthetic */ c0 b(a aVar, FragmentManager fragmentManager, String str, List list, boolean z2, Function1 function1, int i) {
            boolean z3 = (i & 8) != 0 ? true : z2;
            C0056a aVar2 = function1;
            if ((i & 16) != 0) {
                aVar2 = C0056a.j;
            }
            return aVar.a(fragmentManager, str, list, z3, aVar2);
        }

        public final c0 a(FragmentManager fragmentManager, String str, List<d0> list, boolean z2, Function1<? super Integer, Unit> function1) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(str, "title");
            m.checkNotNullParameter(list, "options");
            m.checkNotNullParameter(function1, "onSelectedListener");
            c0 c0Var = new c0();
            c0Var.setArguments(BundleKt.bundleOf(d0.o.to("simple_bottom_sheet_title", str), d0.o.to("simple_bottom_sheet_options", list), d0.o.to("simple_bottom_sheet_show_dividers", Boolean.valueOf(z2))));
            c0Var.l = function1;
            c0Var.show(fragmentManager, a0.getOrCreateKotlinClass(c0.class).toString());
            return c0Var;
        }
    }

    /* compiled from: SelectorBottomSheet.kt */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class b extends k implements Function1<View, b.a.i.c> {
        public static final b j = new b();

        public b() {
            super(1, b.a.i.c.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/BottomSheetSimpleSelectorBinding;", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public b.a.i.c invoke(View view) {
            View view2 = view;
            m.checkNotNullParameter(view2, "p1");
            int i = R.id.widget_simple_bottom_sheet_selector_header;
            ConstraintLayout constraintLayout = (ConstraintLayout) view2.findViewById(R.id.widget_simple_bottom_sheet_selector_header);
            if (constraintLayout != null) {
                i = R.id.widget_simple_bottom_sheet_selector_placeholder;
                TextView textView = (TextView) view2.findViewById(R.id.widget_simple_bottom_sheet_selector_placeholder);
                if (textView != null) {
                    i = R.id.widget_simple_bottom_sheet_selector_recycler;
                    MaxHeightRecyclerView maxHeightRecyclerView = (MaxHeightRecyclerView) view2.findViewById(R.id.widget_simple_bottom_sheet_selector_recycler);
                    if (maxHeightRecyclerView != null) {
                        return new b.a.i.c((ConstraintLayout) view2, constraintLayout, textView, maxHeightRecyclerView);
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
        }
    }

    /* compiled from: SelectorBottomSheet.kt */
    /* loaded from: classes2.dex */
    public static final class c extends o implements Function2<LayoutInflater, ViewGroup, f0> {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c() {
            super(2);
            c0.this = r1;
        }

        @Override // kotlin.jvm.functions.Function2
        public f0 invoke(LayoutInflater layoutInflater, ViewGroup viewGroup) {
            LayoutInflater layoutInflater2 = layoutInflater;
            ViewGroup viewGroup2 = viewGroup;
            m.checkNotNullParameter(layoutInflater2, "inflater");
            m.checkNotNullParameter(viewGroup2, "parent");
            View inflate = layoutInflater2.inflate(R.layout.bottom_sheet_simple_selector_item, viewGroup2, false);
            int i = R.id.select_component_sheet_item_description;
            MaterialTextView materialTextView = (MaterialTextView) inflate.findViewById(R.id.select_component_sheet_item_description);
            if (materialTextView != null) {
                i = R.id.select_component_sheet_item_icon;
                SimpleDraweeView simpleDraweeView = (SimpleDraweeView) inflate.findViewById(R.id.select_component_sheet_item_icon);
                if (simpleDraweeView != null) {
                    i = R.id.select_component_sheet_item_title;
                    MaterialTextView materialTextView2 = (MaterialTextView) inflate.findViewById(R.id.select_component_sheet_item_title);
                    if (materialTextView2 != null) {
                        d dVar = new d((ConstraintLayout) inflate, materialTextView, simpleDraweeView, materialTextView2);
                        m.checkNotNullExpressionValue(dVar, "BottomSheetSimpleSelecto…(inflater, parent, false)");
                        c0 c0Var = c0.this;
                        Dialog requireDialog = c0Var.requireDialog();
                        m.checkNotNullExpressionValue(requireDialog, "requireDialog()");
                        return new f0(c0Var, requireDialog, dVar);
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
        }
    }

    public c0() {
        super(false, 1, null);
    }

    @Override // b.a.y.i
    public void c(int i) {
        Function1<? super Integer, Unit> function1 = this.l;
        if (function1 != null) {
            function1.invoke(Integer.valueOf(i));
        }
    }

    public final b.a.i.c g() {
        return (b.a.i.c) this.m.getValue((Fragment) this, j[0]);
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.bottom_sheet_simple_selector;
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        m.checkNotNullParameter(dialogInterface, "dialog");
        super.onDismiss(dialogInterface);
        this.l = null;
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        dismiss();
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        m.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        setBottomSheetCollapsedStateDisabled();
        String string = getArgumentsOrDefault().getString("simple_bottom_sheet_title");
        Serializable serializable = getArgumentsOrDefault().getSerializable("simple_bottom_sheet_options");
        if (!(serializable instanceof List)) {
            serializable = null;
        }
        List list = (List) serializable;
        boolean z2 = getArgumentsOrDefault().getBoolean("simple_bottom_sheet_show_dividers");
        TextView textView = g().c;
        m.checkNotNullExpressionValue(textView, "binding.widgetSimpleBottomSheetSelectorPlaceholder");
        textView.setText(string);
        ConstraintLayout constraintLayout = g().f88b;
        m.checkNotNullExpressionValue(constraintLayout, "binding.widgetSimpleBottomSheetSelectorHeader");
        int i = 0;
        if (!(!(string == null || string.length() == 0))) {
            i = 8;
        }
        constraintLayout.setVisibility(i);
        if (list != null) {
            MaxHeightRecyclerView maxHeightRecyclerView = g().d;
            m.checkNotNullExpressionValue(maxHeightRecyclerView, "binding.widgetSimpleBottomSheetSelectorRecycler");
            maxHeightRecyclerView.setAdapter(new SimpleRecyclerAdapter(list, new c()));
            MaxHeightRecyclerView maxHeightRecyclerView2 = g().d;
            RecyclerView.Adapter adapter = maxHeightRecyclerView2.getAdapter();
            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }
            if (z2) {
                SimpleRecyclerAdapter.Companion.addThemedDivider(maxHeightRecyclerView2);
            }
        }
    }
}
