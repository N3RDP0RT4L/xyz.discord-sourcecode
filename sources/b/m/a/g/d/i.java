package b.m.a.g.d;

import b.m.a.e;
import b.m.a.g.d.e;
import com.lyft.kronos.internal.ntp.NTPSyncException;
import d0.z.d.m;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
/* compiled from: SntpService.kt */
/* loaded from: classes3.dex */
public final class i implements h {
    public final AtomicReference<a> a = new AtomicReference<>(a.INIT);

    /* renamed from: b  reason: collision with root package name */
    public final AtomicLong f1905b = new AtomicLong(0);
    public final ExecutorService c = Executors.newSingleThreadExecutor(b.j);
    public final e d;
    public final b.m.a.b e;
    public final f f;
    public final e g;
    public final List<String> h;
    public final long i;
    public final long j;
    public final long k;

    /* compiled from: SntpService.kt */
    /* loaded from: classes3.dex */
    public enum a {
        INIT,
        IDLE,
        SYNCING,
        STOPPED
    }

    /* compiled from: SntpService.kt */
    /* loaded from: classes3.dex */
    public static final class b implements ThreadFactory {
        public static final b j = new b();

        @Override // java.util.concurrent.ThreadFactory
        public final Thread newThread(Runnable runnable) {
            return new Thread(runnable, "kronos-android");
        }
    }

    /* compiled from: SntpService.kt */
    /* loaded from: classes3.dex */
    public static final class c implements Runnable {
        public c() {
        }

        /* JADX WARN: Finally extract failed */
        @Override // java.lang.Runnable
        public final void run() {
            boolean z2;
            i iVar = i.this;
            iVar.c();
            Iterator<String> it = iVar.h.iterator();
            do {
                z2 = false;
                if (it.hasNext()) {
                    String next = it.next();
                    a aVar = a.IDLE;
                    AtomicReference<a> atomicReference = iVar.a;
                    a aVar2 = a.SYNCING;
                    if (atomicReference.getAndSet(aVar2) != aVar2) {
                        long b2 = iVar.e.b();
                        e eVar = iVar.g;
                        if (eVar != null) {
                            eVar.a(next);
                        }
                        try {
                            e.b d = iVar.d.d(next, Long.valueOf(iVar.i));
                            m.checkExpressionValueIsNotNull(d, "response");
                            if (d.a() < 0) {
                                throw new NTPSyncException("Invalid time " + d.a() + " received from " + next);
                                break;
                            }
                            iVar.f.a(d);
                            long j = d.c;
                            long b3 = iVar.e.b() - b2;
                            b.m.a.e eVar2 = iVar.g;
                            if (eVar2 != null) {
                                eVar2.b(j, b3);
                            }
                            iVar.a.set(aVar);
                            iVar.f1905b.set(iVar.e.b());
                            z2 = true;
                            continue;
                        } finally {
                            try {
                                continue;
                            } catch (Throwable th) {
                            }
                        }
                    }
                } else {
                    return;
                }
            } while (!z2);
        }
    }

    public i(e eVar, b.m.a.b bVar, f fVar, b.m.a.e eVar2, List<String> list, long j, long j2, long j3) {
        m.checkParameterIsNotNull(eVar, "sntpClient");
        m.checkParameterIsNotNull(bVar, "deviceClock");
        m.checkParameterIsNotNull(fVar, "responseCache");
        m.checkParameterIsNotNull(list, "ntpHosts");
        this.d = eVar;
        this.e = bVar;
        this.f = fVar;
        this.g = eVar2;
        this.h = list;
        this.i = j;
        this.j = j2;
        this.k = j3;
    }

    /* JADX WARN: Code restructure failed: missing block: B:9:0x0039, code lost:
        if ((java.lang.Math.abs((r0.a - r0.f1903b) - (r0.d.a() - r0.d.b())) < 1000) == false) goto L11;
     */
    @Override // b.m.a.g.d.h
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.m.a.d a() {
        /*
            r10 = this;
            r10.c()
            b.m.a.g.d.f r0 = r10.f
            b.m.a.g.d.e$b r0 = r0.get()
            java.util.concurrent.atomic.AtomicReference<b.m.a.g.d.i$a> r1 = r10.a
            b.m.a.g.d.i$a r2 = b.m.a.g.d.i.a.INIT
            b.m.a.g.d.i$a r3 = b.m.a.g.d.i.a.IDLE
            boolean r1 = r1.compareAndSet(r2, r3)
            r2 = 1
            r3 = 0
            if (r1 == 0) goto L3c
            if (r0 == 0) goto L3c
            long r4 = r0.a
            long r6 = r0.f1903b
            long r4 = r4 - r6
            b.m.a.b r1 = r0.d
            long r6 = r1.a()
            b.m.a.b r1 = r0.d
            long r8 = r1.b()
            long r6 = r6 - r8
            long r4 = r4 - r6
            long r4 = java.lang.Math.abs(r4)
            r6 = 1000(0x3e8, double:4.94E-321)
            int r1 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r1 >= 0) goto L38
            r1 = 1
            goto L39
        L38:
            r1 = 0
        L39:
            if (r1 != 0) goto L3c
            goto L3d
        L3c:
            r2 = 0
        L3d:
            r1 = 0
            if (r2 == 0) goto L46
            b.m.a.g.d.f r0 = r10.f
            r0.clear()
            r0 = r1
        L46:
            if (r0 != 0) goto L5f
            b.m.a.b r0 = r10.e
            long r2 = r0.b()
            java.util.concurrent.atomic.AtomicLong r0 = r10.f1905b
            long r4 = r0.get()
            long r2 = r2 - r4
            long r4 = r10.j
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 < 0) goto L5e
            r10.b()
        L5e:
            return r1
        L5f:
            b.m.a.b r1 = r0.d
            long r1 = r1.b()
            long r3 = r0.f1903b
            long r1 = r1 - r3
            long r3 = r10.k
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 < 0) goto L84
            b.m.a.b r3 = r10.e
            long r3 = r3.b()
            java.util.concurrent.atomic.AtomicLong r5 = r10.f1905b
            long r5 = r5.get()
            long r3 = r3 - r5
            long r5 = r10.j
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 < 0) goto L84
            r10.b()
        L84:
            b.m.a.d r3 = new b.m.a.d
            long r4 = r0.a()
            java.lang.Long r0 = java.lang.Long.valueOf(r1)
            r3.<init>(r4, r0)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: b.m.a.g.d.i.a():b.m.a.d");
    }

    @Override // b.m.a.g.d.h
    public void b() {
        c();
        if (this.a.get() != a.SYNCING) {
            this.c.submit(new c());
        }
    }

    public final void c() {
        if (this.a.get() == a.STOPPED) {
            throw new IllegalStateException("Service already shutdown");
        }
    }
}
