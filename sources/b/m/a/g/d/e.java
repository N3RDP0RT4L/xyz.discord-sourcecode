package b.m.a.g.d;

import com.discord.api.permission.Permission;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Arrays;
import org.objectweb.asm.Opcodes;
/* compiled from: SntpClient.java */
/* loaded from: classes3.dex */
public class e {
    public final b.m.a.b a;

    /* renamed from: b  reason: collision with root package name */
    public final c f1902b;
    public final b.m.a.g.d.a c;

    /* compiled from: SntpClient.java */
    /* loaded from: classes3.dex */
    public static class a extends IOException {
        public a(String str) {
            super(str);
        }
    }

    /* compiled from: SntpClient.java */
    /* loaded from: classes3.dex */
    public static final class b {
        public final long a;

        /* renamed from: b  reason: collision with root package name */
        public final long f1903b;
        public final long c;
        public final b.m.a.b d;

        public b(long j, long j2, long j3, b.m.a.b bVar) {
            this.a = j;
            this.f1903b = j2;
            this.c = j3;
            this.d = bVar;
        }

        public long a() {
            return this.a + this.c + (this.d.b() - this.f1903b);
        }
    }

    public e(b.m.a.b bVar, c cVar, b.m.a.g.d.a aVar) {
        this.a = bVar;
        this.f1902b = cVar;
        this.c = aVar;
    }

    public static void a(byte b2, byte b3, int i, long j) throws a {
        if (b2 == 3) {
            throw new a("unsynchronized server");
        } else if (b3 != 4 && b3 != 5) {
            throw new a(b.d.b.a.a.p("untrusted mode: ", b3));
        } else if (i == 0 || i > 15) {
            throw new a(b.d.b.a.a.p("untrusted stratum: ", i));
        } else if (j == 0) {
            throw new a("zero transmitTime");
        }
    }

    public static long b(byte[] bArr, int i) {
        int i2 = bArr[i];
        int i3 = bArr[i + 1];
        int i4 = bArr[i + 2];
        int i5 = bArr[i + 3];
        if ((i2 & 128) == 128) {
            i2 = (i2 & Opcodes.LAND) + 128;
        }
        if ((i3 & 128) == 128) {
            i3 = (i3 & Opcodes.LAND) + 128;
        }
        if ((i4 & 128) == 128) {
            i4 = (i4 & Opcodes.LAND) + 128;
        }
        if ((i5 & 128) == 128) {
            i5 = (i5 & Opcodes.LAND) + 128;
        }
        return (i2 << 24) + (i3 << 16) + (i4 << 8) + i5;
    }

    public static long c(byte[] bArr, int i) {
        long b2 = b(bArr, i);
        return ((b(bArr, i + 4) * 1000) / Permission.REQUEST_TO_SPEAK) + ((b2 - 2208988800L) * 1000);
    }

    public b d(String str, Long l) throws IOException {
        DatagramSocket datagramSocket = null;
        try {
            InetAddress a2 = this.f1902b.a(str);
            datagramSocket = this.c.c();
            datagramSocket.setSoTimeout(l.intValue());
            byte[] bArr = new byte[48];
            DatagramPacket b2 = this.c.b(bArr, a2, 123);
            bArr[0] = 27;
            long a3 = this.a.a();
            long b3 = this.a.b();
            long j = a3 / 1000;
            Long.signum(j);
            long j2 = a3 - (j * 1000);
            long j3 = j + 2208988800L;
            bArr[40] = (byte) (j3 >> 24);
            bArr[41] = (byte) (j3 >> 16);
            bArr[42] = (byte) (j3 >> 8);
            bArr[43] = (byte) (j3 >> 0);
            long j4 = (j2 * Permission.REQUEST_TO_SPEAK) / 1000;
            bArr[44] = (byte) (j4 >> 24);
            bArr[45] = (byte) (j4 >> 16);
            bArr[46] = (byte) (j4 >> 8);
            bArr[47] = (byte) (Math.random() * 255.0d);
            datagramSocket.send(b2);
            byte[] copyOf = Arrays.copyOf(bArr, 48);
            datagramSocket.receive(this.c.a(copyOf));
            long b4 = this.a.b();
            long j5 = (b4 - b3) + a3;
            long c = c(copyOf, 24);
            long c2 = c(copyOf, 32);
            long c3 = c(copyOf, 40);
            a((byte) ((copyOf[0] >> 6) & 3), (byte) (copyOf[0] & 7), copyOf[1] & 255, c3);
            b bVar = new b(j5, b4, ((c3 - j5) + (c2 - c)) / 2, this.a);
            datagramSocket.close();
            return bVar;
        } catch (Throwable th) {
            if (datagramSocket != null) {
                datagramSocket.close();
            }
            throw th;
        }
    }
}
