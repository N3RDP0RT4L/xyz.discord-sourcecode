package b.m.a.g.d;

import b.m.a.b;
import b.m.a.f;
import b.m.a.g.d.e;
import d0.z.d.m;
/* compiled from: SntpResponseCache.kt */
/* loaded from: classes3.dex */
public final class g implements f {
    public final f a;

    /* renamed from: b  reason: collision with root package name */
    public final b f1904b;

    public g(f fVar, b bVar) {
        m.checkParameterIsNotNull(fVar, "syncResponseCache");
        m.checkParameterIsNotNull(bVar, "deviceClock");
        this.a = fVar;
        this.f1904b = bVar;
    }

    @Override // b.m.a.g.d.f
    public void a(e.b bVar) {
        m.checkParameterIsNotNull(bVar, "response");
        this.a.f(bVar.a);
        this.a.a(bVar.f1903b);
        this.a.b(bVar.c);
    }

    @Override // b.m.a.g.d.f
    public void clear() {
        this.a.clear();
    }

    @Override // b.m.a.g.d.f
    public e.b get() {
        long e = this.a.e();
        long c = this.a.c();
        long d = this.a.d();
        if (c == 0) {
            return null;
        }
        return new e.b(e, c, d, this.f1904b);
    }
}
