package b.m.a;

import b.d.b.a.a;
import d0.z.d.m;
/* compiled from: Clock.kt */
/* loaded from: classes3.dex */
public final class d {
    public final long a;

    /* renamed from: b  reason: collision with root package name */
    public final Long f1900b;

    public d(long j, Long l) {
        this.a = j;
        this.f1900b = l;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof d) {
                d dVar = (d) obj;
                if (!(this.a == dVar.a) || !m.areEqual(this.f1900b, dVar.f1900b)) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        long j = this.a;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        Long l = this.f1900b;
        return i + (l != null ? l.hashCode() : 0);
    }

    public String toString() {
        StringBuilder R = a.R("KronosTime(posixTimeMs=");
        R.append(this.a);
        R.append(", timeSinceLastNtpSyncMs=");
        return a.F(R, this.f1900b, ")");
    }
}
