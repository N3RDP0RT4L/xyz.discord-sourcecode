package b.g.a.b.t;

import b.g.a.b.t.d;
import java.io.IOException;
/* compiled from: DefaultIndenter.java */
/* loaded from: classes2.dex */
public class c extends d.c {
    public static final String j;
    public static final c k;
    private static final long serialVersionUID = 1;
    private final int charsPerLevel;
    private final String eol;
    private final char[] indents;

    static {
        String str;
        try {
            str = System.getProperty("line.separator");
        } catch (Throwable unused) {
            str = "\n";
        }
        j = str;
        k = new c("  ", str);
    }

    public c() {
        this("  ", j);
    }

    @Override // b.g.a.b.t.d.b
    public void a(b.g.a.b.d dVar, int i) throws IOException {
        dVar.T(this.eol);
        if (i > 0) {
            int i2 = i * this.charsPerLevel;
            while (true) {
                char[] cArr = this.indents;
                if (i2 > cArr.length) {
                    dVar.U(cArr, 0, cArr.length);
                    i2 -= this.indents.length;
                } else {
                    dVar.U(cArr, 0, i2);
                    return;
                }
            }
        }
    }

    public c(String str, String str2) {
        this.charsPerLevel = str.length();
        this.indents = new char[str.length() * 16];
        int i = 0;
        for (int i2 = 0; i2 < 16; i2++) {
            str.getChars(0, str.length(), this.indents, i);
            i += str.length();
        }
        this.eol = str2;
    }
}
