package b.g.a.b.t;

import b.g.a.b.j;
import b.g.a.b.k;
import java.io.IOException;
import java.io.Serializable;
/* compiled from: DefaultPrettyPrinter.java */
/* loaded from: classes2.dex */
public class d implements j, e<d>, Serializable {
    public static final b.g.a.b.p.j j = new b.g.a.b.p.j(" ");
    private static final long serialVersionUID = 1;
    public b _arrayIndenter;
    public String _objectFieldValueSeparatorWithSpaces;
    public b _objectIndenter;
    public final k _rootSeparator;
    public j _separators;
    public boolean _spacesInObjectEntries;
    public transient int k;

    /* compiled from: DefaultPrettyPrinter.java */
    /* loaded from: classes2.dex */
    public static class a extends c {
        public static final a j = new a();

        @Override // b.g.a.b.t.d.b
        public void a(b.g.a.b.d dVar, int i) throws IOException {
            dVar.R(' ');
        }

        @Override // b.g.a.b.t.d.c, b.g.a.b.t.d.b
        public boolean isInline() {
            return true;
        }
    }

    /* compiled from: DefaultPrettyPrinter.java */
    /* loaded from: classes2.dex */
    public interface b {
        void a(b.g.a.b.d dVar, int i) throws IOException;

        boolean isInline();
    }

    /* compiled from: DefaultPrettyPrinter.java */
    /* loaded from: classes2.dex */
    public static class c implements b, Serializable {
        @Override // b.g.a.b.t.d.b
        public boolean isInline() {
            return !(this instanceof b.g.a.b.t.c);
        }
    }

    public d() {
        b.g.a.b.p.j jVar = j;
        this._arrayIndenter = a.j;
        this._objectIndenter = b.g.a.b.t.c.k;
        this._spacesInObjectEntries = true;
        this._rootSeparator = jVar;
        j jVar2 = j.f659b;
        this._separators = jVar2;
        StringBuilder R = b.d.b.a.a.R(" ");
        R.append(jVar2.c());
        R.append(" ");
        this._objectFieldValueSeparatorWithSpaces = R.toString();
    }

    @Override // b.g.a.b.j
    public void a(b.g.a.b.d dVar) throws IOException {
        dVar.R('{');
        if (!this._objectIndenter.isInline()) {
            this.k++;
        }
    }

    @Override // b.g.a.b.j
    public void b(b.g.a.b.d dVar) throws IOException {
        k kVar = this._rootSeparator;
        if (kVar != null) {
            dVar.S(kVar);
        }
    }

    @Override // b.g.a.b.j
    public void c(b.g.a.b.d dVar) throws IOException {
        dVar.R(this._separators.a());
        this._arrayIndenter.a(dVar, this.k);
    }

    @Override // b.g.a.b.j
    public void d(b.g.a.b.d dVar) throws IOException {
        this._objectIndenter.a(dVar, this.k);
    }

    @Override // b.g.a.b.t.e
    public d e() {
        if (d.class == d.class) {
            return new d(this);
        }
        throw new IllegalStateException(b.d.b.a.a.n(d.class, b.d.b.a.a.R("Failed `createInstance()`: "), " does not override method; it has to"));
    }

    @Override // b.g.a.b.j
    public void f(b.g.a.b.d dVar, int i) throws IOException {
        if (!this._objectIndenter.isInline()) {
            this.k--;
        }
        if (i > 0) {
            this._objectIndenter.a(dVar, this.k);
        } else {
            dVar.R(' ');
        }
        dVar.R('}');
    }

    @Override // b.g.a.b.j
    public void g(b.g.a.b.d dVar) throws IOException {
        if (!this._arrayIndenter.isInline()) {
            this.k++;
        }
        dVar.R('[');
    }

    @Override // b.g.a.b.j
    public void h(b.g.a.b.d dVar) throws IOException {
        this._arrayIndenter.a(dVar, this.k);
    }

    @Override // b.g.a.b.j
    public void i(b.g.a.b.d dVar) throws IOException {
        dVar.R(this._separators.b());
        this._objectIndenter.a(dVar, this.k);
    }

    @Override // b.g.a.b.j
    public void j(b.g.a.b.d dVar, int i) throws IOException {
        if (!this._arrayIndenter.isInline()) {
            this.k--;
        }
        if (i > 0) {
            this._arrayIndenter.a(dVar, this.k);
        } else {
            dVar.R(' ');
        }
        dVar.R(']');
    }

    @Override // b.g.a.b.j
    public void k(b.g.a.b.d dVar) throws IOException {
        if (this._spacesInObjectEntries) {
            dVar.T(this._objectFieldValueSeparatorWithSpaces);
        } else {
            dVar.R(this._separators.c());
        }
    }

    public d(d dVar) {
        k kVar = dVar._rootSeparator;
        this._arrayIndenter = a.j;
        this._objectIndenter = b.g.a.b.t.c.k;
        this._spacesInObjectEntries = true;
        this._arrayIndenter = dVar._arrayIndenter;
        this._objectIndenter = dVar._objectIndenter;
        this._spacesInObjectEntries = dVar._spacesInObjectEntries;
        this.k = dVar.k;
        this._separators = dVar._separators;
        this._objectFieldValueSeparatorWithSpaces = dVar._objectFieldValueSeparatorWithSpaces;
        this._rootSeparator = kVar;
    }
}
