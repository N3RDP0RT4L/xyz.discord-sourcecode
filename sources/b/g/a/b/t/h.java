package b.g.a.b.t;

import b.g.a.b.m;
import b.g.a.b.t.g;
/* compiled from: JacksonFeatureSet.java */
/* loaded from: classes2.dex */
public final class h<F extends g> {
    public int a;

    public h(int i) {
        this.a = i;
    }

    public static <F extends g> h<F> a(F[] fArr) {
        if (fArr.length <= 31) {
            int i = 0;
            for (F f : fArr) {
                if (f.f()) {
                    i |= f.g();
                }
            }
            return new h<>(i);
        }
        throw new IllegalArgumentException(String.format("Can not use type `%s` with JacksonFeatureSet: too many entries (%d > 31)", fArr[0].getClass().getName(), Integer.valueOf(fArr.length)));
    }

    public h<F> b(F f) {
        int g = ((m) f).g() | this.a;
        return g == this.a ? this : new h<>(g);
    }
}
