package b.g.a.b.t;

import java.util.concurrent.ConcurrentHashMap;
/* compiled from: InternCache.java */
/* loaded from: classes2.dex */
public final class f extends ConcurrentHashMap<String, String> {
    public static final f j = new f();
    private static final long serialVersionUID = 1;
    private final Object lock = new Object();

    public f() {
        super(180, 0.8f, 4);
    }

    public String a(String str) {
        String str2 = get(str);
        if (str2 != null) {
            return str2;
        }
        if (size() >= 180) {
            synchronized (this.lock) {
                if (size() >= 180) {
                    clear();
                }
            }
        }
        String intern = str.intern();
        put(intern, intern);
        return intern;
    }
}
