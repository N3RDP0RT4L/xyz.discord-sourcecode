package b.g.a.b.t;

import b.g.a.b.t.l;
import java.lang.ref.SoftReference;
/* compiled from: BufferRecyclers.java */
/* loaded from: classes2.dex */
public class b {
    public static final l a;

    /* renamed from: b  reason: collision with root package name */
    public static final ThreadLocal<SoftReference<a>> f673b;

    static {
        boolean z2;
        try {
            z2 = "true".equals(System.getProperty("com.fasterxml.jackson.core.util.BufferRecyclers.trackReusableBuffers"));
        } catch (SecurityException unused) {
            z2 = false;
        }
        a = z2 ? l.a.a : null;
        f673b = new ThreadLocal<>();
    }
}
