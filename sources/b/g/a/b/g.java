package b.g.a.b;

import b.g.a.b.p.a;
import com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel;
/* compiled from: JsonStreamContext.java */
/* loaded from: classes2.dex */
public abstract class g {
    public int a;

    /* renamed from: b  reason: collision with root package name */
    public int f657b;

    public g() {
    }

    public abstract String a();

    public final boolean b() {
        return this.a == 1;
    }

    public final boolean c() {
        return this.a == 2;
    }

    public abstract void d(Object obj);

    public String e() {
        int i = this.a;
        return i != 0 ? i != 1 ? i != 2 ? "?" : "Object" : "Array" : "root";
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(64);
        int i = this.a;
        if (i != 0) {
            int i2 = 0;
            if (i != 1) {
                sb.append('{');
                String a = a();
                if (a != null) {
                    sb.append('\"');
                    int[] iArr = a.f;
                    int length = iArr.length;
                    int length2 = a.length();
                    while (i2 < length2) {
                        char charAt = a.charAt(i2);
                        if (charAt >= length || iArr[charAt] == 0) {
                            sb.append(charAt);
                        } else {
                            sb.append('\\');
                            int i3 = iArr[charAt];
                            if (i3 < 0) {
                                sb.append('u');
                                sb.append('0');
                                sb.append('0');
                                char[] cArr = a.a;
                                sb.append(cArr[charAt >> 4]);
                                sb.append(cArr[charAt & 15]);
                            } else {
                                sb.append((char) i3);
                            }
                        }
                        i2++;
                    }
                    sb.append('\"');
                } else {
                    sb.append('?');
                }
                sb.append('}');
            } else {
                sb.append('[');
                int i4 = this.f657b;
                if (i4 >= 0) {
                    i2 = i4;
                }
                sb.append(i2);
                sb.append(']');
            }
        } else {
            sb.append(AutocompleteViewModel.COMMAND_DISCOVER_TOKEN);
        }
        return sb.toString();
    }

    public g(int i, int i2) {
        this.a = i;
        this.f657b = i2;
    }
}
