package b.g.a.b;

import java.io.IOException;
/* compiled from: PrettyPrinter.java */
/* loaded from: classes2.dex */
public interface j {

    /* renamed from: b  reason: collision with root package name */
    public static final b.g.a.b.t.j f659b = new b.g.a.b.t.j();

    static {
        new b.g.a.b.p.j(" ");
    }

    void a(d dVar) throws IOException;

    void b(d dVar) throws IOException;

    void c(d dVar) throws IOException;

    void d(d dVar) throws IOException;

    void f(d dVar, int i) throws IOException;

    void g(d dVar) throws IOException;

    void h(d dVar) throws IOException;

    void i(d dVar) throws IOException;

    void j(d dVar, int i) throws IOException;

    void k(d dVar) throws IOException;
}
