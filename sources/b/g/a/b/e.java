package b.g.a.b;

import java.io.Serializable;
/* compiled from: JsonLocation.java */
/* loaded from: classes2.dex */
public class e implements Serializable {
    public static final e j = new e(null, -1, -1, -1, -1);
    private static final long serialVersionUID = 1;
    public final int _columnNr;
    public final int _lineNr;
    public final long _totalBytes;
    public final long _totalChars;
    public final transient Object k = null;

    public e(Object obj, long j2, long j3, int i, int i2) {
        this._totalBytes = j2;
        this._totalChars = j3;
        this._lineNr = i;
        this._columnNr = i2;
    }

    public final int a(StringBuilder sb, String str) {
        sb.append('\"');
        sb.append(str);
        sb.append('\"');
        return str.length();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || !(obj instanceof e)) {
            return false;
        }
        e eVar = (e) obj;
        Object obj2 = this.k;
        if (obj2 == null) {
            if (eVar.k != null) {
                return false;
            }
        } else if (!obj2.equals(eVar.k)) {
            return false;
        }
        return this._lineNr == eVar._lineNr && this._columnNr == eVar._columnNr && this._totalChars == eVar._totalChars && this._totalBytes == eVar._totalBytes;
    }

    public int hashCode() {
        Object obj = this.k;
        return ((((obj == null ? 1 : obj.hashCode()) ^ this._lineNr) + this._columnNr) ^ ((int) this._totalChars)) + ((int) this._totalBytes);
    }

    /* JADX WARN: Removed duplicated region for block: B:29:0x00bb  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public java.lang.String toString() {
        /*
            r9 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r1 = 80
            r0.<init>(r1)
            java.lang.String r1 = "[Source: "
            r0.append(r1)
            java.lang.Object r1 = r9.k
            r2 = 93
            if (r1 != 0) goto L19
            java.lang.String r1 = "UNKNOWN"
            r0.append(r1)
            goto Lc9
        L19:
            boolean r3 = r1 instanceof java.lang.Class
            if (r3 == 0) goto L21
            r3 = r1
            java.lang.Class r3 = (java.lang.Class) r3
            goto L25
        L21:
            java.lang.Class r3 = r1.getClass()
        L25:
            java.lang.String r4 = r3.getName()
            java.lang.String r5 = "java."
            boolean r5 = r4.startsWith(r5)
            if (r5 == 0) goto L36
            java.lang.String r4 = r3.getSimpleName()
            goto L43
        L36:
            boolean r3 = r1 instanceof byte[]
            if (r3 == 0) goto L3d
            java.lang.String r4 = "byte[]"
            goto L43
        L3d:
            boolean r3 = r1 instanceof char[]
            if (r3 == 0) goto L43
            java.lang.String r4 = "char[]"
        L43:
            r3 = 40
            r0.append(r3)
            r0.append(r4)
            r3 = 41
            r0.append(r3)
            boolean r3 = r1 instanceof java.lang.CharSequence
            r4 = 500(0x1f4, float:7.0E-43)
            r5 = 0
            java.lang.String r6 = " chars"
            if (r3 == 0) goto L70
            java.lang.CharSequence r1 = (java.lang.CharSequence) r1
            int r3 = r1.length()
            int r4 = java.lang.Math.min(r3, r4)
            java.lang.CharSequence r1 = r1.subSequence(r5, r4)
            java.lang.String r1 = r1.toString()
            int r1 = r9.a(r0, r1)
            goto L8f
        L70:
            boolean r3 = r1 instanceof char[]
            r7 = 34
            if (r3 == 0) goto L92
            char[] r1 = (char[]) r1
            int r3 = r1.length
            java.lang.String r8 = new java.lang.String
            int r4 = java.lang.Math.min(r3, r4)
            r8.<init>(r1, r5, r4)
            r0.append(r7)
            r0.append(r8)
            r0.append(r7)
            int r1 = r8.length()
        L8f:
            int r5 = r3 - r1
            goto Lb9
        L92:
            boolean r3 = r1 instanceof byte[]
            if (r3 == 0) goto Lb9
            byte[] r1 = (byte[]) r1
            int r3 = r1.length
            int r3 = java.lang.Math.min(r3, r4)
            java.lang.String r4 = new java.lang.String
            java.lang.String r6 = "UTF-8"
            java.nio.charset.Charset r6 = java.nio.charset.Charset.forName(r6)
            r4.<init>(r1, r5, r3, r6)
            r0.append(r7)
            r0.append(r4)
            r0.append(r7)
            r4.length()
            int r1 = r1.length
            int r5 = r1 - r3
            java.lang.String r6 = " bytes"
        Lb9:
            if (r5 <= 0) goto Lc9
            java.lang.String r1 = "[truncated "
            r0.append(r1)
            r0.append(r5)
            r0.append(r6)
            r0.append(r2)
        Lc9:
            java.lang.String r1 = "; line: "
            r0.append(r1)
            int r1 = r9._lineNr
            r0.append(r1)
            java.lang.String r1 = ", column: "
            r0.append(r1)
            int r1 = r9._columnNr
            java.lang.String r0 = b.d.b.a.a.z(r0, r1, r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.b.e.toString():java.lang.String");
    }
}
