package b.g.a.b.q;

import b.g.a.b.g;
/* compiled from: JsonWriteContext.java */
/* loaded from: classes2.dex */
public class c extends g {
    public final c c;
    public a d;
    public c e;
    public String f;
    public Object g;
    public boolean h;

    public c(int i, c cVar, a aVar) {
        this.a = i;
        this.c = cVar;
        this.d = aVar;
        this.f657b = -1;
    }

    @Override // b.g.a.b.g
    public final String a() {
        return this.f;
    }

    @Override // b.g.a.b.g
    public void d(Object obj) {
        this.g = obj;
    }

    public c f() {
        c cVar = this.e;
        if (cVar == null) {
            a aVar = this.d;
            c cVar2 = new c(1, this, aVar == null ? null : aVar.a());
            this.e = cVar2;
            return cVar2;
        }
        cVar.j(1);
        return cVar;
    }

    public c g(Object obj) {
        c cVar = this.e;
        if (cVar == null) {
            a aVar = this.d;
            c cVar2 = new c(1, this, aVar == null ? null : aVar.a(), obj);
            this.e = cVar2;
            return cVar2;
        }
        cVar.k(1, obj);
        return cVar;
    }

    public c h() {
        c cVar = this.e;
        if (cVar == null) {
            a aVar = this.d;
            c cVar2 = new c(2, this, aVar == null ? null : aVar.a());
            this.e = cVar2;
            return cVar2;
        }
        cVar.j(2);
        return cVar;
    }

    public c i(Object obj) {
        c cVar = this.e;
        if (cVar == null) {
            a aVar = this.d;
            c cVar2 = new c(2, this, aVar == null ? null : aVar.a(), obj);
            this.e = cVar2;
            return cVar2;
        }
        cVar.k(2, obj);
        return cVar;
    }

    public c j(int i) {
        this.a = i;
        this.f657b = -1;
        this.f = null;
        this.h = false;
        this.g = null;
        a aVar = this.d;
        if (aVar != null) {
            aVar.f666b = null;
            aVar.c = null;
            aVar.d = null;
        }
        return this;
    }

    public c k(int i, Object obj) {
        this.a = i;
        this.f657b = -1;
        this.f = null;
        this.h = false;
        this.g = obj;
        a aVar = this.d;
        if (aVar != null) {
            aVar.f666b = null;
            aVar.c = null;
            aVar.d = null;
        }
        return this;
    }

    /* JADX WARN: Removed duplicated region for block: B:25:0x0053  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public int l(java.lang.String r5) throws com.fasterxml.jackson.core.JsonProcessingException {
        /*
            r4 = this;
            int r0 = r4.a
            r1 = 2
            if (r0 != r1) goto L71
            boolean r0 = r4.h
            if (r0 == 0) goto La
            goto L71
        La:
            r0 = 1
            r4.h = r0
            r4.f = r5
            b.g.a.b.q.a r1 = r4.d
            if (r1 == 0) goto L6b
            java.lang.String r2 = r1.f666b
            if (r2 != 0) goto L1a
            r1.f666b = r5
            goto L27
        L1a:
            boolean r2 = r5.equals(r2)
            if (r2 == 0) goto L21
            goto L2f
        L21:
            java.lang.String r2 = r1.c
            if (r2 != 0) goto L29
            r1.c = r5
        L27:
            r2 = 0
            goto L51
        L29:
            boolean r2 = r5.equals(r2)
            if (r2 == 0) goto L31
        L2f:
            r2 = 1
            goto L51
        L31:
            java.util.HashSet<java.lang.String> r2 = r1.d
            if (r2 != 0) goto L4a
            java.util.HashSet r2 = new java.util.HashSet
            r3 = 16
            r2.<init>(r3)
            r1.d = r2
            java.lang.String r3 = r1.f666b
            r2.add(r3)
            java.util.HashSet<java.lang.String> r2 = r1.d
            java.lang.String r3 = r1.c
            r2.add(r3)
        L4a:
            java.util.HashSet<java.lang.String> r2 = r1.d
            boolean r2 = r2.add(r5)
            r2 = r2 ^ r0
        L51:
            if (r2 == 0) goto L6b
            java.lang.Object r0 = r1.a
            com.fasterxml.jackson.core.JsonGenerationException r1 = new com.fasterxml.jackson.core.JsonGenerationException
            java.lang.String r2 = "Duplicate field '"
            java.lang.String r3 = "'"
            java.lang.String r5 = b.d.b.a.a.w(r2, r5, r3)
            boolean r2 = r0 instanceof b.g.a.b.d
            if (r2 == 0) goto L66
            b.g.a.b.d r0 = (b.g.a.b.d) r0
            goto L67
        L66:
            r0 = 0
        L67:
            r1.<init>(r5, r0)
            throw r1
        L6b:
            int r5 = r4.f657b
            if (r5 >= 0) goto L70
            r0 = 0
        L70:
            return r0
        L71:
            r5 = 4
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.b.q.c.l(java.lang.String):int");
    }

    public int m() {
        int i = this.a;
        if (i == 2) {
            if (!this.h) {
                return 5;
            }
            this.h = false;
            this.f657b++;
            return 2;
        } else if (i == 1) {
            int i2 = this.f657b;
            this.f657b = i2 + 1;
            return i2 < 0 ? 0 : 1;
        } else {
            int i3 = this.f657b + 1;
            this.f657b = i3;
            return i3 == 0 ? 0 : 3;
        }
    }

    public c(int i, c cVar, a aVar, Object obj) {
        this.a = i;
        this.c = cVar;
        this.d = aVar;
        this.f657b = -1;
        this.g = obj;
    }
}
