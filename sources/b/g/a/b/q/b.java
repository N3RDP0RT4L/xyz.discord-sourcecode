package b.g.a.b.q;

import b.g.a.b.d;
import b.g.a.b.i;
import b.g.a.b.k;
import b.g.a.b.o.a;
import b.g.a.b.p.c;
import b.g.a.b.t.d;
import com.fasterxml.jackson.core.JsonGenerationException;
import java.io.IOException;
import org.objectweb.asm.Opcodes;
/* compiled from: JsonGeneratorImpl.java */
/* loaded from: classes2.dex */
public abstract class b extends a {
    public static final int[] r = b.g.a.b.p.a.f;

    /* renamed from: s  reason: collision with root package name */
    public final c f667s;
    public int u;
    public boolean w;
    public int[] t = r;
    public k v = d.j;

    public b(c cVar, int i, i iVar) {
        super(i, iVar);
        this.f667s = cVar;
        if (d.a.ESCAPE_NON_ASCII.g(i)) {
            this.u = Opcodes.LAND;
        }
        this.w = !d.a.QUOTE_FIELD_NAMES.g(i);
    }

    @Override // b.g.a.b.d
    public b.g.a.b.d b(d.a aVar) {
        int h = aVar.h();
        this.o &= ~h;
        if ((h & a.m) != 0) {
            if (aVar == d.a.WRITE_NUMBERS_AS_STRINGS) {
                this.p = false;
            } else if (aVar == d.a.ESCAPE_NON_ASCII) {
                y0(0);
            } else if (aVar == d.a.STRICT_DUPLICATE_DETECTION) {
                c cVar = this.q;
                cVar.d = null;
                this.q = cVar;
            }
        }
        if (aVar == d.a.QUOTE_FIELD_NAMES) {
            this.w = true;
        }
        return this;
    }

    @Override // b.g.a.b.o.a
    public void r0(int i, int i2) {
        if ((a.m & i2) != 0) {
            this.p = d.a.WRITE_NUMBERS_AS_STRINGS.g(i);
            d.a aVar = d.a.ESCAPE_NON_ASCII;
            if (aVar.g(i2)) {
                if (aVar.g(i)) {
                    y0(Opcodes.LAND);
                } else {
                    y0(0);
                }
            }
            d.a aVar2 = d.a.STRICT_DUPLICATE_DETECTION;
            if (aVar2.g(i2)) {
                if (aVar2.g(i)) {
                    c cVar = this.q;
                    if (cVar.d == null) {
                        cVar.d = new a(this);
                        this.q = cVar;
                    }
                } else {
                    c cVar2 = this.q;
                    cVar2.d = null;
                    this.q = cVar2;
                }
            }
        }
        this.w = !d.a.QUOTE_FIELD_NAMES.g(i);
    }

    public void x0(String str) throws IOException {
        throw new JsonGenerationException(String.format("Can not %s, expecting field name (context: %s)", str, this.q.e()), this);
    }

    public b.g.a.b.d y0(int i) {
        if (i < 0) {
            i = 0;
        }
        this.u = i;
        return this;
    }
}
