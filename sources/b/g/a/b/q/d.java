package b.g.a.b.q;

import b.g.a.b.d;
import b.g.a.b.i;
import b.g.a.b.j;
import b.g.a.b.k;
import b.g.a.b.p.a;
import b.g.a.b.p.c;
import b.g.a.b.p.g;
import b.g.a.b.t.m;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.fasterxml.jackson.core.JsonGenerationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Objects;
/* compiled from: WriterBasedJsonGenerator.java */
/* loaded from: classes2.dex */
public class d extends b {

    /* renamed from: x  reason: collision with root package name */
    public static final char[] f668x = (char[]) a.a.clone();
    public char[] A;
    public int B;
    public int C;
    public int D;
    public char[] E;

    /* renamed from: y  reason: collision with root package name */
    public final Writer f669y;

    /* renamed from: z  reason: collision with root package name */
    public char f670z;

    public d(c cVar, int i, i iVar, Writer writer, char c) {
        super(cVar, i, iVar);
        int[] iArr;
        this.f669y = writer;
        if (cVar.e == null) {
            char[] a = cVar.c.a(1, 0);
            cVar.e = a;
            this.A = a;
            this.D = a.length;
            this.f670z = c;
            if (c != '\"') {
                if (c == '\"') {
                    iArr = a.f;
                } else {
                    a.C0079a aVar = a.C0079a.a;
                    int[] iArr2 = aVar.f661b[c];
                    if (iArr2 == null) {
                        iArr2 = Arrays.copyOf(a.f, 128);
                        if (iArr2[c] == 0) {
                            iArr2[c] = -1;
                        }
                        aVar.f661b[c] = iArr2;
                    }
                    iArr = iArr2;
                }
                this.t = iArr;
                return;
            }
            return;
        }
        throw new IllegalStateException("Trying to call same allocXxx() method second time");
    }

    @Override // b.g.a.b.d
    public void A() throws IOException {
        t0("write a null");
        H0();
    }

    public final void A0(char c, int i) throws IOException, JsonGenerationException {
        int i2;
        if (i >= 0) {
            if (this.C + 2 > this.D) {
                B0();
            }
            char[] cArr = this.A;
            int i3 = this.C;
            int i4 = i3 + 1;
            this.C = i4;
            cArr[i3] = '\\';
            this.C = i4 + 1;
            cArr[i4] = (char) i;
        } else if (i != -2) {
            if (this.C + 5 >= this.D) {
                B0();
            }
            int i5 = this.C;
            char[] cArr2 = this.A;
            int i6 = i5 + 1;
            cArr2[i5] = '\\';
            int i7 = i6 + 1;
            cArr2[i6] = 'u';
            if (c > 255) {
                int i8 = 255 & (c >> '\b');
                int i9 = i7 + 1;
                char[] cArr3 = f668x;
                cArr2[i7] = cArr3[i8 >> 4];
                i2 = i9 + 1;
                cArr2[i9] = cArr3[i8 & 15];
                c = (char) (c & 255);
            } else {
                int i10 = i7 + 1;
                cArr2[i7] = '0';
                i2 = i10 + 1;
                cArr2[i10] = '0';
            }
            int i11 = i2 + 1;
            char[] cArr4 = f668x;
            cArr2[i2] = cArr4[c >> 4];
            cArr2[i11] = cArr4[c & 15];
            this.C = i11 + 1;
        } else {
            Objects.requireNonNull(null);
            throw null;
        }
    }

    public void B0() throws IOException {
        int i = this.C;
        int i2 = this.B;
        int i3 = i - i2;
        if (i3 > 0) {
            this.B = 0;
            this.C = 0;
            this.f669y.write(this.A, i2, i3);
        }
    }

    @Override // b.g.a.b.d
    public void C(double d) throws IOException {
        if (!this.p) {
            String str = g.a;
            if (!(Double.isNaN(d) || Double.isInfinite(d)) || !d(d.a.QUOTE_NON_NUMERIC_NUMBERS)) {
                t0("write a number");
                T(String.valueOf(d));
                return;
            }
        }
        j0(String.valueOf(d));
    }

    public final int C0(char[] cArr, int i, int i2, char c, int i3) throws IOException, JsonGenerationException {
        int i4;
        if (i3 >= 0) {
            if (i <= 1 || i >= i2) {
                char[] cArr2 = this.E;
                if (cArr2 == null) {
                    cArr2 = z0();
                }
                cArr2[1] = (char) i3;
                this.f669y.write(cArr2, 0, 2);
                return i;
            }
            int i5 = i - 2;
            cArr[i5] = '\\';
            cArr[i5 + 1] = (char) i3;
            return i5;
        } else if (i3 == -2) {
            Objects.requireNonNull(null);
            throw null;
        } else if (i <= 5 || i >= i2) {
            char[] cArr3 = this.E;
            if (cArr3 == null) {
                cArr3 = z0();
            }
            this.B = this.C;
            if (c > 255) {
                int i6 = (c >> '\b') & 255;
                int i7 = c & 255;
                char[] cArr4 = f668x;
                cArr3[10] = cArr4[i6 >> 4];
                cArr3[11] = cArr4[i6 & 15];
                cArr3[12] = cArr4[i7 >> 4];
                cArr3[13] = cArr4[i7 & 15];
                this.f669y.write(cArr3, 8, 6);
                return i;
            }
            char[] cArr5 = f668x;
            cArr3[6] = cArr5[c >> 4];
            cArr3[7] = cArr5[c & 15];
            this.f669y.write(cArr3, 2, 6);
            return i;
        } else {
            int i8 = i - 6;
            int i9 = i8 + 1;
            cArr[i8] = '\\';
            int i10 = i9 + 1;
            cArr[i9] = 'u';
            if (c > 255) {
                int i11 = (c >> '\b') & 255;
                int i12 = i10 + 1;
                char[] cArr6 = f668x;
                cArr[i10] = cArr6[i11 >> 4];
                i4 = i12 + 1;
                cArr[i12] = cArr6[i11 & 15];
                c = (char) (c & 255);
            } else {
                int i13 = i10 + 1;
                cArr[i10] = '0';
                i4 = i13 + 1;
                cArr[i13] = '0';
            }
            int i14 = i4 + 1;
            char[] cArr7 = f668x;
            cArr[i4] = cArr7[c >> 4];
            cArr[i14] = cArr7[c & 15];
            return i14 - 5;
        }
    }

    @Override // b.g.a.b.d
    public void D(float f) throws IOException {
        if (!this.p) {
            String str = g.a;
            if (!(Float.isNaN(f) || Float.isInfinite(f)) || !d(d.a.QUOTE_NON_NUMERIC_NUMBERS)) {
                t0("write a number");
                T(String.valueOf(f));
                return;
            }
        }
        j0(String.valueOf(f));
    }

    public final void D0(char c, int i) throws IOException, JsonGenerationException {
        int i2;
        if (i >= 0) {
            int i3 = this.C;
            if (i3 >= 2) {
                int i4 = i3 - 2;
                this.B = i4;
                char[] cArr = this.A;
                cArr[i4] = '\\';
                cArr[i4 + 1] = (char) i;
                return;
            }
            char[] cArr2 = this.E;
            if (cArr2 == null) {
                cArr2 = z0();
            }
            this.B = this.C;
            cArr2[1] = (char) i;
            this.f669y.write(cArr2, 0, 2);
        } else if (i != -2) {
            int i5 = this.C;
            if (i5 >= 6) {
                char[] cArr3 = this.A;
                int i6 = i5 - 6;
                this.B = i6;
                cArr3[i6] = '\\';
                int i7 = i6 + 1;
                cArr3[i7] = 'u';
                if (c > 255) {
                    int i8 = (c >> '\b') & 255;
                    int i9 = i7 + 1;
                    char[] cArr4 = f668x;
                    cArr3[i9] = cArr4[i8 >> 4];
                    i2 = i9 + 1;
                    cArr3[i2] = cArr4[i8 & 15];
                    c = (char) (c & 255);
                } else {
                    int i10 = i7 + 1;
                    cArr3[i10] = '0';
                    i2 = i10 + 1;
                    cArr3[i2] = '0';
                }
                int i11 = i2 + 1;
                char[] cArr5 = f668x;
                cArr3[i11] = cArr5[c >> 4];
                cArr3[i11 + 1] = cArr5[c & 15];
                return;
            }
            char[] cArr6 = this.E;
            if (cArr6 == null) {
                cArr6 = z0();
            }
            this.B = this.C;
            if (c > 255) {
                int i12 = (c >> '\b') & 255;
                int i13 = c & 255;
                char[] cArr7 = f668x;
                cArr6[10] = cArr7[i12 >> 4];
                cArr6[11] = cArr7[i12 & 15];
                cArr6[12] = cArr7[i13 >> 4];
                cArr6[13] = cArr7[i13 & 15];
                this.f669y.write(cArr6, 8, 6);
                return;
            }
            char[] cArr8 = f668x;
            cArr6[6] = cArr8[c >> 4];
            cArr6[7] = cArr8[c & 15];
            this.f669y.write(cArr6, 2, 6);
        } else {
            Objects.requireNonNull(null);
            throw null;
        }
    }

    public final int E0(InputStream inputStream, byte[] bArr, int i, int i2, int i3) throws IOException {
        int i4 = 0;
        while (i < i2) {
            i4++;
            i++;
            bArr[i4] = bArr[i];
        }
        int min = Math.min(i3, bArr.length);
        do {
            int i5 = min - i4;
            if (i5 == 0) {
                break;
            }
            int read = inputStream.read(bArr, i4, i5);
            if (read < 0) {
                return i4;
            }
            i4 += read;
        } while (i4 < 3);
        return i4;
    }

    public final int F0(b.g.a.b.a aVar, InputStream inputStream, byte[] bArr) throws IOException, JsonGenerationException {
        int i = this.D - 6;
        int i2 = 2;
        int d = aVar.d() >> 2;
        int i3 = -3;
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        while (true) {
            if (i4 > i3) {
                i5 = E0(inputStream, bArr, i4, i5, bArr.length);
                if (i5 < 3) {
                    break;
                }
                i3 = i5 - 3;
                i4 = 0;
            }
            if (this.C > i) {
                B0();
            }
            int i7 = i4 + 1;
            int i8 = i7 + 1;
            i4 = i8 + 1;
            i6 += 3;
            int b2 = aVar.b((((bArr[i7] & 255) | (bArr[i4] << 8)) << 8) | (bArr[i8] & 255), this.A, this.C);
            this.C = b2;
            d--;
            if (d <= 0) {
                char[] cArr = this.A;
                int i9 = b2 + 1;
                this.C = i9;
                cArr[b2] = '\\';
                this.C = i9 + 1;
                cArr[i9] = 'n';
                d = aVar.d() >> 2;
            }
        }
        if (i5 <= 0) {
            return i6;
        }
        if (this.C > i) {
            B0();
        }
        int i10 = bArr[0] << 16;
        if (1 < i5) {
            i10 |= (bArr[1] & 255) << 8;
        } else {
            i2 = 1;
        }
        int i11 = i6 + i2;
        this.C = aVar.c(i10, i2, this.A, this.C);
        return i11;
    }

    public final int G0(b.g.a.b.a aVar, InputStream inputStream, byte[] bArr, int i) throws IOException, JsonGenerationException {
        int E0;
        int i2 = this.D - 6;
        int i3 = 2;
        int d = aVar.d() >> 2;
        int i4 = -3;
        int i5 = 0;
        int i6 = 0;
        while (true) {
            if (i <= 2) {
                break;
            }
            if (i5 > i4) {
                i6 = E0(inputStream, bArr, i5, i6, i);
                if (i6 < 3) {
                    i5 = 0;
                    break;
                }
                i4 = i6 - 3;
                i5 = 0;
            }
            if (this.C > i2) {
                B0();
            }
            int i7 = i5 + 1;
            int i8 = i7 + 1;
            i5 = i8 + 1;
            i -= 3;
            int b2 = aVar.b((((bArr[i7] & 255) | (bArr[i5] << 8)) << 8) | (bArr[i8] & 255), this.A, this.C);
            this.C = b2;
            d--;
            if (d <= 0) {
                char[] cArr = this.A;
                int i9 = b2 + 1;
                this.C = i9;
                cArr[b2] = '\\';
                this.C = i9 + 1;
                cArr[i9] = 'n';
                d = aVar.d() >> 2;
            }
        }
        if (i <= 0 || (E0 = E0(inputStream, bArr, i5, i6, i)) <= 0) {
            return i;
        }
        if (this.C > i2) {
            B0();
        }
        int i10 = bArr[0] << 16;
        if (1 < E0) {
            i10 |= (bArr[1] & 255) << 8;
        } else {
            i3 = 1;
        }
        this.C = aVar.c(i10, i3, this.A, this.C);
        return i - i3;
    }

    @Override // b.g.a.b.d
    public void H(int i) throws IOException {
        t0("write a number");
        if (this.p) {
            if (this.C + 13 >= this.D) {
                B0();
            }
            char[] cArr = this.A;
            int i2 = this.C;
            int i3 = i2 + 1;
            this.C = i3;
            cArr[i2] = this.f670z;
            int d = g.d(i, cArr, i3);
            this.C = d;
            char[] cArr2 = this.A;
            this.C = d + 1;
            cArr2[d] = this.f670z;
            return;
        }
        if (this.C + 11 >= this.D) {
            B0();
        }
        this.C = g.d(i, this.A, this.C);
    }

    public final void H0() throws IOException {
        if (this.C + 4 >= this.D) {
            B0();
        }
        int i = this.C;
        char[] cArr = this.A;
        cArr[i] = 'n';
        int i2 = i + 1;
        cArr[i2] = 'u';
        int i3 = i2 + 1;
        cArr[i3] = 'l';
        int i4 = i3 + 1;
        cArr[i4] = 'l';
        this.C = i4 + 1;
    }

    @Override // b.g.a.b.d
    public void I(long j) throws IOException {
        t0("write a number");
        if (this.p) {
            if (this.C + 23 >= this.D) {
                B0();
            }
            char[] cArr = this.A;
            int i = this.C;
            int i2 = i + 1;
            this.C = i2;
            cArr[i] = this.f670z;
            int e = g.e(j, cArr, i2);
            this.C = e;
            char[] cArr2 = this.A;
            this.C = e + 1;
            cArr2[e] = this.f670z;
            return;
        }
        if (this.C + 21 >= this.D) {
            B0();
        }
        this.C = g.e(j, this.A, this.C);
    }

    public final void I0(String str) throws IOException {
        if (this.C >= this.D) {
            B0();
        }
        char[] cArr = this.A;
        int i = this.C;
        this.C = i + 1;
        cArr[i] = this.f670z;
        T(str);
        if (this.C >= this.D) {
            B0();
        }
        char[] cArr2 = this.A;
        int i2 = this.C;
        this.C = i2 + 1;
        cArr2[i2] = this.f670z;
    }

    @Override // b.g.a.b.d
    public void J(String str) throws IOException {
        t0("write a number");
        if (str == null) {
            H0();
        } else if (this.p) {
            I0(str);
        } else {
            T(str);
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:69:0x010a, code lost:
        r5 = r18.B;
        r4 = r4 - r5;
     */
    /* JADX WARN: Code restructure failed: missing block: B:70:0x010d, code lost:
        if (r4 <= 0) goto L99;
     */
    /* JADX WARN: Code restructure failed: missing block: B:71:0x010f, code lost:
        r18.f669y.write(r3, r5, r4);
     */
    /* JADX WARN: Code restructure failed: missing block: B:72:0x0114, code lost:
        r3 = r18.A;
        r4 = r18.C;
        r18.C = r4 + 1;
        r3 = r3[r4];
        D0(r3, r0[r3]);
     */
    /* JADX WARN: Removed duplicated region for block: B:91:0x012a A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void J0(java.lang.String r19) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 299
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.b.q.d.J0(java.lang.String):void");
    }

    @Override // b.g.a.b.d
    public void L(BigDecimal bigDecimal) throws IOException {
        t0("write a number");
        if (bigDecimal == null) {
            H0();
        } else if (this.p) {
            I0(o0(bigDecimal));
        } else {
            T(o0(bigDecimal));
        }
    }

    @Override // b.g.a.b.d
    public void N(BigInteger bigInteger) throws IOException {
        t0("write a number");
        if (bigInteger == null) {
            H0();
        } else if (this.p) {
            I0(bigInteger.toString());
        } else {
            T(bigInteger.toString());
        }
    }

    @Override // b.g.a.b.d
    public void O(short s2) throws IOException {
        t0("write a number");
        if (this.p) {
            if (this.C + 8 >= this.D) {
                B0();
            }
            char[] cArr = this.A;
            int i = this.C;
            int i2 = i + 1;
            this.C = i2;
            cArr[i] = this.f670z;
            int d = g.d(s2, cArr, i2);
            this.C = d;
            char[] cArr2 = this.A;
            this.C = d + 1;
            cArr2[d] = this.f670z;
            return;
        }
        if (this.C + 6 >= this.D) {
            B0();
        }
        this.C = g.d(s2, this.A, this.C);
    }

    @Override // b.g.a.b.d
    public void R(char c) throws IOException {
        if (this.C >= this.D) {
            B0();
        }
        char[] cArr = this.A;
        int i = this.C;
        this.C = i + 1;
        cArr[i] = c;
    }

    @Override // b.g.a.b.d
    public void S(k kVar) throws IOException {
        int b2 = kVar.b(this.A, this.C);
        if (b2 < 0) {
            T(kVar.getValue());
        } else {
            this.C += b2;
        }
    }

    @Override // b.g.a.b.d
    public void T(String str) throws IOException {
        int length = str.length();
        int i = this.D - this.C;
        if (i == 0) {
            B0();
            i = this.D - this.C;
        }
        if (i >= length) {
            str.getChars(0, length, this.A, this.C);
            this.C += length;
            return;
        }
        int i2 = this.D;
        int i3 = this.C;
        int i4 = i2 - i3;
        str.getChars(0, i4, this.A, i3);
        this.C += i4;
        B0();
        int length2 = str.length() - i4;
        while (true) {
            int i5 = this.D;
            if (length2 > i5) {
                int i6 = i4 + i5;
                str.getChars(i4, i6, this.A, 0);
                this.B = 0;
                this.C = i5;
                B0();
                length2 -= i5;
                i4 = i6;
            } else {
                str.getChars(i4, i4 + length2, this.A, 0);
                this.B = 0;
                this.C = length2;
                return;
            }
        }
    }

    @Override // b.g.a.b.d
    public void U(char[] cArr, int i, int i2) throws IOException {
        if (i2 < 32) {
            if (i2 > this.D - this.C) {
                B0();
            }
            System.arraycopy(cArr, i, this.A, this.C, i2);
            this.C += i2;
            return;
        }
        B0();
        this.f669y.write(cArr, i, i2);
    }

    @Override // b.g.a.b.d
    public void W() throws IOException {
        t0("start an array");
        this.q = this.q.f();
        j jVar = this.l;
        if (jVar != null) {
            jVar.g(this);
            return;
        }
        if (this.C >= this.D) {
            B0();
        }
        char[] cArr = this.A;
        int i = this.C;
        this.C = i + 1;
        cArr[i] = '[';
    }

    @Override // b.g.a.b.d
    public void X(Object obj) throws IOException {
        t0("start an array");
        this.q = this.q.g(obj);
        j jVar = this.l;
        if (jVar != null) {
            jVar.g(this);
            return;
        }
        if (this.C >= this.D) {
            B0();
        }
        char[] cArr = this.A;
        int i = this.C;
        this.C = i + 1;
        cArr[i] = '[';
    }

    @Override // b.g.a.b.d
    public void b0(Object obj, int i) throws IOException {
        t0("start an array");
        this.q = this.q.g(obj);
        j jVar = this.l;
        if (jVar != null) {
            jVar.g(this);
            return;
        }
        if (this.C >= this.D) {
            B0();
        }
        char[] cArr = this.A;
        int i2 = this.C;
        this.C = i2 + 1;
        cArr[i2] = '[';
    }

    @Override // b.g.a.b.d
    public void c0() throws IOException {
        t0("start an object");
        this.q = this.q.h();
        j jVar = this.l;
        if (jVar != null) {
            jVar.a(this);
            return;
        }
        if (this.C >= this.D) {
            B0();
        }
        char[] cArr = this.A;
        int i = this.C;
        this.C = i + 1;
        cArr[i] = '{';
    }

    @Override // b.g.a.b.d, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        if (this.A != null && d(d.a.AUTO_CLOSE_JSON_CONTENT)) {
            while (true) {
                c cVar = this.q;
                if (!cVar.b()) {
                    if (!cVar.c()) {
                        break;
                    }
                    u();
                } else {
                    t();
                }
            }
        }
        B0();
        this.B = 0;
        this.C = 0;
        if (this.f669y != null) {
            if (this.f667s.f662b || d(d.a.AUTO_CLOSE_TARGET)) {
                this.f669y.close();
            } else if (d(d.a.FLUSH_PASSED_TO_STREAM)) {
                this.f669y.flush();
            }
        }
        char[] cArr = this.A;
        if (cArr != null) {
            this.A = null;
            c cVar2 = this.f667s;
            Objects.requireNonNull(cVar2);
            char[] cArr2 = cVar2.e;
            if (cArr == cArr2 || cArr.length >= cArr2.length) {
                cVar2.e = null;
                cVar2.c.d.set(1, cArr);
                return;
            }
            throw new IllegalArgumentException("Trying to release buffer smaller than original");
        }
    }

    @Override // b.g.a.b.d
    public void d0(Object obj) throws IOException {
        t0("start an object");
        this.q = this.q.i(obj);
        j jVar = this.l;
        if (jVar != null) {
            jVar.a(this);
            return;
        }
        if (this.C >= this.D) {
            B0();
        }
        char[] cArr = this.A;
        int i = this.C;
        this.C = i + 1;
        cArr[i] = '{';
    }

    /* JADX WARN: Finally extract failed */
    @Override // b.g.a.b.d
    public int f(b.g.a.b.a aVar, InputStream inputStream, int i) throws IOException, JsonGenerationException {
        t0("write a binary value");
        if (this.C >= this.D) {
            B0();
        }
        char[] cArr = this.A;
        int i2 = this.C;
        this.C = i2 + 1;
        cArr[i2] = this.f670z;
        c cVar = this.f667s;
        if (cVar.d == null) {
            b.g.a.b.t.a aVar2 = cVar.c;
            Objects.requireNonNull(aVar2);
            int i3 = b.g.a.b.t.a.a[3];
            if (i3 <= 0) {
                i3 = 0;
            }
            byte[] andSet = aVar2.c.getAndSet(3, null);
            if (andSet == null || andSet.length < i3) {
                andSet = new byte[i3];
            }
            cVar.d = andSet;
            try {
                if (i < 0) {
                    i = F0(aVar, inputStream, andSet);
                } else {
                    int G0 = G0(aVar, inputStream, andSet, i);
                    if (G0 > 0) {
                        throw new JsonGenerationException("Too few bytes available: missing " + G0 + " bytes (out of " + i + ")", this);
                    }
                }
                this.f667s.a(andSet);
                if (this.C >= this.D) {
                    B0();
                }
                char[] cArr2 = this.A;
                int i4 = this.C;
                this.C = i4 + 1;
                cArr2[i4] = this.f670z;
                return i;
            } catch (Throwable th) {
                this.f667s.a(andSet);
                throw th;
            }
        } else {
            throw new IllegalStateException("Trying to call same allocXxx() method second time");
        }
    }

    @Override // java.io.Flushable
    public void flush() throws IOException {
        B0();
        if (this.f669y != null && d(d.a.FLUSH_PASSED_TO_STREAM)) {
            this.f669y.flush();
        }
    }

    @Override // b.g.a.b.d
    public void g0(k kVar) throws IOException {
        t0("write a string");
        if (this.C >= this.D) {
            B0();
        }
        char[] cArr = this.A;
        int i = this.C;
        int i2 = i + 1;
        this.C = i2;
        cArr[i] = this.f670z;
        int c = kVar.c(cArr, i2);
        if (c < 0) {
            char[] a = kVar.a();
            int length = a.length;
            if (length < 32) {
                if (length > this.D - this.C) {
                    B0();
                }
                System.arraycopy(a, 0, this.A, this.C, length);
                this.C += length;
            } else {
                B0();
                this.f669y.write(a, 0, length);
            }
            if (this.C >= this.D) {
                B0();
            }
            char[] cArr2 = this.A;
            int i3 = this.C;
            this.C = i3 + 1;
            cArr2[i3] = this.f670z;
            return;
        }
        int i4 = this.C + c;
        this.C = i4;
        if (i4 >= this.D) {
            B0();
        }
        char[] cArr3 = this.A;
        int i5 = this.C;
        this.C = i5 + 1;
        cArr3[i5] = this.f670z;
    }

    @Override // b.g.a.b.d
    public void j0(String str) throws IOException {
        t0("write a string");
        if (str == null) {
            H0();
            return;
        }
        if (this.C >= this.D) {
            B0();
        }
        char[] cArr = this.A;
        int i = this.C;
        this.C = i + 1;
        cArr[i] = this.f670z;
        J0(str);
        if (this.C >= this.D) {
            B0();
        }
        char[] cArr2 = this.A;
        int i2 = this.C;
        this.C = i2 + 1;
        cArr2[i2] = this.f670z;
    }

    /* JADX WARN: Removed duplicated region for block: B:56:0x003f A[EDGE_INSN: B:56:0x003f->B:18:0x003f ?: BREAK  , SYNTHETIC] */
    @Override // b.g.a.b.d
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void m0(char[] r11, int r12, int r13) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 201
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.b.q.d.m0(char[], int, int):void");
    }

    @Override // b.g.a.b.d
    public void n(b.g.a.b.a aVar, byte[] bArr, int i, int i2) throws IOException, JsonGenerationException {
        t0("write a binary value");
        if (this.C >= this.D) {
            B0();
        }
        char[] cArr = this.A;
        int i3 = this.C;
        this.C = i3 + 1;
        cArr[i3] = this.f670z;
        int i4 = i2 + i;
        int i5 = i4 - 3;
        int i6 = this.D - 6;
        int d = aVar.d() >> 2;
        while (i <= i5) {
            if (this.C > i6) {
                B0();
            }
            int i7 = i + 1;
            int i8 = i7 + 1;
            int i9 = ((bArr[i] << 8) | (bArr[i7] & 255)) << 8;
            i = i8 + 1;
            int b2 = aVar.b(i9 | (bArr[i8] & 255), this.A, this.C);
            this.C = b2;
            d--;
            if (d <= 0) {
                char[] cArr2 = this.A;
                int i10 = b2 + 1;
                this.C = i10;
                cArr2[b2] = '\\';
                this.C = i10 + 1;
                cArr2[i10] = 'n';
                d = aVar.d() >> 2;
            }
        }
        int i11 = i4 - i;
        if (i11 > 0) {
            if (this.C > i6) {
                B0();
            }
            int i12 = i + 1;
            int i13 = bArr[i] << 16;
            if (i11 == 2) {
                i13 |= (bArr[i12] & 255) << 8;
            }
            this.C = aVar.c(i13, i11, this.A, this.C);
        }
        if (this.C >= this.D) {
            B0();
        }
        char[] cArr3 = this.A;
        int i14 = this.C;
        this.C = i14 + 1;
        cArr3[i14] = this.f670z;
    }

    @Override // b.g.a.b.d
    public void s(boolean z2) throws IOException {
        int i;
        t0("write a boolean value");
        if (this.C + 5 >= this.D) {
            B0();
        }
        int i2 = this.C;
        char[] cArr = this.A;
        if (z2) {
            cArr[i2] = 't';
            int i3 = i2 + 1;
            cArr[i3] = 'r';
            int i4 = i3 + 1;
            cArr[i4] = 'u';
            i = i4 + 1;
            cArr[i] = 'e';
        } else {
            cArr[i2] = 'f';
            int i5 = i2 + 1;
            cArr[i5] = 'a';
            int i6 = i5 + 1;
            cArr[i6] = 'l';
            int i7 = i6 + 1;
            cArr[i7] = 's';
            i = i7 + 1;
            cArr[i] = 'e';
        }
        this.C = i + 1;
    }

    @Override // b.g.a.b.d
    public void t() throws IOException {
        if (this.q.b()) {
            j jVar = this.l;
            if (jVar != null) {
                jVar.j(this, this.q.f657b + 1);
            } else {
                if (this.C >= this.D) {
                    B0();
                }
                char[] cArr = this.A;
                int i = this.C;
                this.C = i + 1;
                cArr[i] = ']';
            }
            c cVar = this.q;
            cVar.g = null;
            this.q = cVar.c;
            return;
        }
        StringBuilder R = b.d.b.a.a.R("Current context not Array but ");
        R.append(this.q.e());
        throw new JsonGenerationException(R.toString(), this);
    }

    @Override // b.g.a.b.o.a
    public final void t0(String str) throws IOException {
        char c;
        int m = this.q.m();
        j jVar = this.l;
        if (jVar == null) {
            if (m == 1) {
                c = ',';
            } else if (m == 2) {
                c = MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR;
            } else if (m == 3) {
                k kVar = this.v;
                if (kVar != null) {
                    T(kVar.getValue());
                    return;
                }
                return;
            } else if (m == 5) {
                x0(str);
                throw null;
            } else {
                return;
            }
            if (this.C >= this.D) {
                B0();
            }
            char[] cArr = this.A;
            int i = this.C;
            this.C = i + 1;
            cArr[i] = c;
        } else if (m != 0) {
            if (m == 1) {
                jVar.c(this);
            } else if (m == 2) {
                jVar.k(this);
            } else if (m == 3) {
                jVar.b(this);
            } else if (m != 5) {
                int i2 = m.a;
                throw new RuntimeException("Internal error: this code path should never get executed");
            } else {
                x0(str);
                throw null;
            }
        } else if (this.q.b()) {
            this.l.h(this);
        } else if (this.q.c()) {
            this.l.d(this);
        }
    }

    @Override // b.g.a.b.d
    public void u() throws IOException {
        if (this.q.c()) {
            j jVar = this.l;
            if (jVar != null) {
                jVar.f(this, this.q.f657b + 1);
            } else {
                if (this.C >= this.D) {
                    B0();
                }
                char[] cArr = this.A;
                int i = this.C;
                this.C = i + 1;
                cArr[i] = '}';
            }
            c cVar = this.q;
            cVar.g = null;
            this.q = cVar.c;
            return;
        }
        StringBuilder R = b.d.b.a.a.R("Current context not Object but ");
        R.append(this.q.e());
        throw new JsonGenerationException(R.toString(), this);
    }

    @Override // b.g.a.b.d
    public void x(k kVar) throws IOException {
        int l = this.q.l(kVar.getValue());
        if (l != 4) {
            boolean z2 = l == 1;
            j jVar = this.l;
            if (jVar != null) {
                if (z2) {
                    jVar.i(this);
                } else {
                    jVar.d(this);
                }
                char[] a = kVar.a();
                if (this.w) {
                    U(a, 0, a.length);
                    return;
                }
                if (this.C >= this.D) {
                    B0();
                }
                char[] cArr = this.A;
                int i = this.C;
                this.C = i + 1;
                cArr[i] = this.f670z;
                U(a, 0, a.length);
                if (this.C >= this.D) {
                    B0();
                }
                char[] cArr2 = this.A;
                int i2 = this.C;
                this.C = i2 + 1;
                cArr2[i2] = this.f670z;
                return;
            }
            if (this.C + 1 >= this.D) {
                B0();
            }
            if (z2) {
                char[] cArr3 = this.A;
                int i3 = this.C;
                this.C = i3 + 1;
                cArr3[i3] = ',';
            }
            if (this.w) {
                char[] a2 = kVar.a();
                U(a2, 0, a2.length);
                return;
            }
            char[] cArr4 = this.A;
            int i4 = this.C;
            int i5 = i4 + 1;
            this.C = i5;
            cArr4[i4] = this.f670z;
            int c = kVar.c(cArr4, i5);
            if (c < 0) {
                char[] a3 = kVar.a();
                U(a3, 0, a3.length);
                if (this.C >= this.D) {
                    B0();
                }
                char[] cArr5 = this.A;
                int i6 = this.C;
                this.C = i6 + 1;
                cArr5[i6] = this.f670z;
                return;
            }
            int i7 = this.C + c;
            this.C = i7;
            if (i7 >= this.D) {
                B0();
            }
            char[] cArr6 = this.A;
            int i8 = this.C;
            this.C = i8 + 1;
            cArr6[i8] = this.f670z;
            return;
        }
        throw new JsonGenerationException("Can not write a field name, expecting a value", this);
    }

    @Override // b.g.a.b.d
    public void y(String str) throws IOException {
        int l = this.q.l(str);
        if (l != 4) {
            boolean z2 = l == 1;
            j jVar = this.l;
            if (jVar != null) {
                if (z2) {
                    jVar.i(this);
                } else {
                    jVar.d(this);
                }
                if (this.w) {
                    J0(str);
                    return;
                }
                if (this.C >= this.D) {
                    B0();
                }
                char[] cArr = this.A;
                int i = this.C;
                this.C = i + 1;
                cArr[i] = this.f670z;
                J0(str);
                if (this.C >= this.D) {
                    B0();
                }
                char[] cArr2 = this.A;
                int i2 = this.C;
                this.C = i2 + 1;
                cArr2[i2] = this.f670z;
                return;
            }
            if (this.C + 1 >= this.D) {
                B0();
            }
            if (z2) {
                char[] cArr3 = this.A;
                int i3 = this.C;
                this.C = i3 + 1;
                cArr3[i3] = ',';
            }
            if (this.w) {
                J0(str);
                return;
            }
            char[] cArr4 = this.A;
            int i4 = this.C;
            this.C = i4 + 1;
            cArr4[i4] = this.f670z;
            J0(str);
            if (this.C >= this.D) {
                B0();
            }
            char[] cArr5 = this.A;
            int i5 = this.C;
            this.C = i5 + 1;
            cArr5[i5] = this.f670z;
            return;
        }
        throw new JsonGenerationException("Can not write a field name, expecting a value", this);
    }

    public final char[] z0() {
        char[] cArr = {'\\', 0, '\\', 'u', '0', '0', 0, 0, '\\', 'u'};
        this.E = cArr;
        return cArr;
    }
}
