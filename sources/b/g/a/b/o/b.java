package b.g.a.b.o;

import androidx.recyclerview.widget.RecyclerView;
import b.g.a.b.f;
import java.math.BigDecimal;
import java.math.BigInteger;
/* compiled from: ParserMinimalBase.java */
/* loaded from: classes2.dex */
public abstract class b extends f {
    public static final BigInteger j;
    public static final BigInteger k;
    public static final BigInteger l;
    public static final BigInteger m;

    static {
        BigInteger valueOf = BigInteger.valueOf(-2147483648L);
        j = valueOf;
        BigInteger valueOf2 = BigInteger.valueOf(2147483647L);
        k = valueOf2;
        BigInteger valueOf3 = BigInteger.valueOf(Long.MIN_VALUE);
        l = valueOf3;
        BigInteger valueOf4 = BigInteger.valueOf(RecyclerView.FOREVER_NS);
        m = valueOf4;
        new BigDecimal(valueOf3);
        new BigDecimal(valueOf4);
        new BigDecimal(valueOf);
        new BigDecimal(valueOf2);
    }

    public b(int i) {
        super(i);
    }
}
