package b.g.a.b;

import b.g.a.b.d;
import b.g.a.b.f;
import b.g.a.b.p.b;
import b.g.a.b.p.h;
import b.g.a.b.t.g;
import b.g.a.b.t.l;
import java.io.Serializable;
import java.lang.ref.SoftReference;
/* compiled from: JsonFactory.java */
/* loaded from: classes2.dex */
public class c extends n implements Serializable {
    public static final int j = a.h();
    public static final int k = f.a.f();
    public static final int l = d.a.f();
    public static final k m = b.g.a.b.t.d.j;
    private static final long serialVersionUID = 2;
    public b _characterEscapes;
    public int _factoryFeatures;
    public int _generatorFeatures;
    public b.g.a.b.p.d _inputDecorator;
    public int _maximumNonEscapedChar;
    public i _objectCodec;
    public h _outputDecorator;
    public int _parserFeatures;
    public final char _quoteChar;
    public k _rootValueSeparator;

    /* compiled from: JsonFactory.java */
    /* loaded from: classes2.dex */
    public enum a implements g {
        INTERN_FIELD_NAMES(true),
        CANONICALIZE_FIELD_NAMES(true),
        FAIL_ON_SYMBOL_HASH_OVERFLOW(true),
        USE_THREAD_LOCAL_FOR_BUFFER_RECYCLING(true);
        
        private final boolean _defaultState;

        a(boolean z2) {
            this._defaultState = z2;
        }

        public static int h() {
            a[] values = values();
            int i = 0;
            for (int i2 = 0; i2 < 4; i2++) {
                a aVar = values[i2];
                if (aVar._defaultState) {
                    i |= aVar.g();
                }
            }
            return i;
        }

        @Override // b.g.a.b.t.g
        public boolean f() {
            return this._defaultState;
        }

        @Override // b.g.a.b.t.g
        public int g() {
            return 1 << ordinal();
        }
    }

    public c() {
        this(null);
    }

    public b.g.a.b.t.a a() {
        SoftReference<b.g.a.b.t.a> softReference;
        if (!((a.USE_THREAD_LOCAL_FOR_BUFFER_RECYCLING.g() & this._factoryFeatures) != 0)) {
            return new b.g.a.b.t.a();
        }
        SoftReference<b.g.a.b.t.a> softReference2 = b.g.a.b.t.b.f673b.get();
        b.g.a.b.t.a aVar = softReference2 == null ? null : softReference2.get();
        if (aVar == null) {
            aVar = new b.g.a.b.t.a();
            l lVar = b.g.a.b.t.b.a;
            if (lVar != null) {
                softReference = new SoftReference<>(aVar, lVar.f675b);
                lVar.a.put(softReference, Boolean.TRUE);
                while (true) {
                    SoftReference softReference3 = (SoftReference) lVar.f675b.poll();
                    if (softReference3 == null) {
                        break;
                    }
                    lVar.a.remove(softReference3);
                }
            } else {
                softReference = new SoftReference<>(aVar);
            }
            b.g.a.b.t.b.f673b.set(softReference);
        }
        return aVar;
    }

    public i b() {
        return this._objectCodec;
    }

    public Object readResolve() {
        return new c(this, this._objectCodec);
    }

    public c(i iVar) {
        b.g.a.b.r.b.a();
        b.g.a.b.r.a.a();
        this._factoryFeatures = j;
        this._parserFeatures = k;
        this._generatorFeatures = l;
        this._rootValueSeparator = m;
        this._objectCodec = iVar;
        this._quoteChar = '\"';
    }

    public c(c cVar, i iVar) {
        b.g.a.b.r.b.a();
        b.g.a.b.r.a.a();
        this._factoryFeatures = j;
        this._parserFeatures = k;
        this._generatorFeatures = l;
        this._rootValueSeparator = m;
        this._objectCodec = iVar;
        this._factoryFeatures = cVar._factoryFeatures;
        this._parserFeatures = cVar._parserFeatures;
        this._generatorFeatures = cVar._generatorFeatures;
        this._rootValueSeparator = cVar._rootValueSeparator;
        this._maximumNonEscapedChar = cVar._maximumNonEscapedChar;
        this._quoteChar = cVar._quoteChar;
    }
}
