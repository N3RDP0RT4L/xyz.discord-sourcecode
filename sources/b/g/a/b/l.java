package b.g.a.b;

import b.g.a.b.t.g;
/* compiled from: StreamReadCapability.java */
/* loaded from: classes2.dex */
public enum l implements g {
    DUPLICATE_PROPERTIES(false),
    SCALARS_AS_OBJECTS(false),
    UNTYPED_SCALARS(false);
    
    private final boolean _defaultState;
    private final int _mask = 1 << ordinal();

    l(boolean z2) {
        this._defaultState = z2;
    }

    @Override // b.g.a.b.t.g
    public boolean f() {
        return this._defaultState;
    }

    @Override // b.g.a.b.t.g
    public int g() {
        return this._mask;
    }
}
