package b.g.a.b.p;

import androidx.recyclerview.widget.RecyclerView;
/* compiled from: NumberInput.java */
/* loaded from: classes2.dex */
public final class f {
    public static final String a = String.valueOf(Long.MIN_VALUE).substring(1);

    /* renamed from: b  reason: collision with root package name */
    public static final String f664b = String.valueOf((long) RecyclerView.FOREVER_NS);

    public static long a(String str) {
        int i;
        if (str.length() > 9) {
            return Long.parseLong(str);
        }
        boolean z2 = false;
        char charAt = str.charAt(0);
        int length = str.length();
        int i2 = 1;
        if (charAt == '-') {
            z2 = true;
        }
        if (!z2) {
            if (length > 9) {
                i = Integer.parseInt(str);
                return i;
            }
            if (charAt <= '9') {
            }
            i = Integer.parseInt(str);
            return i;
        } else if (length == 1 || length > 10) {
            i = Integer.parseInt(str);
            return i;
        } else {
            charAt = str.charAt(1);
            i2 = 2;
            if (charAt <= '9' || charAt < '0') {
                i = Integer.parseInt(str);
            } else {
                int i3 = charAt - '0';
                if (i2 < length) {
                    int i4 = i2 + 1;
                    char charAt2 = str.charAt(i2);
                    if (charAt2 > '9' || charAt2 < '0') {
                        i = Integer.parseInt(str);
                    } else {
                        i3 = (i3 * 10) + (charAt2 - '0');
                        if (i4 < length) {
                            int i5 = i4 + 1;
                            char charAt3 = str.charAt(i4);
                            if (charAt3 > '9' || charAt3 < '0') {
                                i = Integer.parseInt(str);
                            } else {
                                i3 = (i3 * 10) + (charAt3 - '0');
                                if (i5 < length) {
                                    while (true) {
                                        int i6 = i5 + 1;
                                        char charAt4 = str.charAt(i5);
                                        if (charAt4 > '9' || charAt4 < '0') {
                                            break;
                                        }
                                        i3 = (i3 * 10) + (charAt4 - '0');
                                        if (i6 >= length) {
                                            break;
                                        }
                                        i5 = i6;
                                    }
                                    i = Integer.parseInt(str);
                                }
                            }
                        }
                    }
                }
                i = z2 ? -i3 : i3;
            }
            return i;
        }
    }
}
