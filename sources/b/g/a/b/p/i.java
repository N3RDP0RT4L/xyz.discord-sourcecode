package b.g.a.b.p;

import b.g.a.b.t.a;
import b.g.a.b.t.k;
import java.io.IOException;
import java.io.Writer;
/* compiled from: SegmentedStringWriter.java */
/* loaded from: classes2.dex */
public final class i extends Writer {
    public final k j;

    public i(a aVar) {
        this.j = new k(aVar);
    }

    @Override // java.io.Writer, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
    }

    @Override // java.io.Writer, java.io.Flushable
    public void flush() {
    }

    @Override // java.io.Writer
    public void write(char[] cArr) {
        this.j.b(cArr, 0, cArr.length);
    }

    @Override // java.io.Writer
    public void write(char[] cArr, int i, int i2) {
        this.j.b(cArr, i, i2);
    }

    @Override // java.io.Writer, java.lang.Appendable
    public Writer append(char c) {
        write(c);
        return this;
    }

    @Override // java.io.Writer
    public void write(int i) {
        k kVar = this.j;
        char c = (char) i;
        if (kVar.c >= 0) {
            kVar.f(16);
        }
        kVar.i = null;
        kVar.j = null;
        char[] cArr = kVar.g;
        if (kVar.h >= cArr.length) {
            kVar.d();
            cArr = kVar.g;
        }
        int i2 = kVar.h;
        kVar.h = i2 + 1;
        cArr[i2] = c;
    }

    @Override // java.io.Writer, java.lang.Appendable
    /* renamed from: append  reason: collision with other method in class */
    public Appendable mo0append(char c) throws IOException {
        write(c);
        return this;
    }

    @Override // java.io.Writer, java.lang.Appendable
    public Writer append(CharSequence charSequence) {
        String charSequence2 = charSequence.toString();
        this.j.a(charSequence2, 0, charSequence2.length());
        return this;
    }

    @Override // java.io.Writer, java.lang.Appendable
    public Writer append(CharSequence charSequence, int i, int i2) {
        String charSequence2 = charSequence.subSequence(i, i2).toString();
        this.j.a(charSequence2, 0, charSequence2.length());
        return this;
    }

    @Override // java.io.Writer
    public void write(String str) {
        this.j.a(str, 0, str.length());
    }

    @Override // java.io.Writer
    public void write(String str, int i, int i2) {
        this.j.a(str, i, i2);
    }
}
