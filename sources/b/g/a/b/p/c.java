package b.g.a.b.p;

import b.g.a.b.t.a;
/* compiled from: IOContext.java */
/* loaded from: classes2.dex */
public class c {
    public final Object a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f662b;
    public final a c;
    public byte[] d;
    public char[] e;

    public c(a aVar, Object obj, boolean z2) {
        this.c = aVar;
        this.a = obj;
        this.f662b = z2;
    }

    public void a(byte[] bArr) {
        byte[] bArr2 = this.d;
        if (bArr == bArr2 || bArr.length >= bArr2.length) {
            this.d = null;
            this.c.c.set(3, bArr);
            return;
        }
        throw new IllegalArgumentException("Trying to release buffer smaller than original");
    }
}
