package b.g.a.b.p;

import b.g.a.b.k;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
/* compiled from: SerializedString.java */
/* loaded from: classes2.dex */
public class j implements k, Serializable {
    public static final e j = e.f663b;
    private static final long serialVersionUID = 1;
    public char[] _quotedChars;
    public byte[] _quotedUTF8Ref;
    public byte[] _unquotedUTF8Ref;
    public final String _value;
    public transient String k;

    public j(String str) {
        if (str != null) {
            this._value = str;
            return;
        }
        throw new IllegalStateException("Null String illegal for SerializedString");
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException {
        this.k = objectInputStream.readUTF();
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeUTF(this._value);
    }

    @Override // b.g.a.b.k
    public final char[] a() {
        char[] cArr = this._quotedChars;
        if (cArr != null) {
            return cArr;
        }
        char[] a = j.a(this._value);
        this._quotedChars = a;
        return a;
    }

    @Override // b.g.a.b.k
    public int b(char[] cArr, int i) {
        String str = this._value;
        int length = str.length();
        if (i + length > cArr.length) {
            return -1;
        }
        str.getChars(0, length, cArr, i);
        return length;
    }

    @Override // b.g.a.b.k
    public int c(char[] cArr, int i) {
        char[] cArr2 = this._quotedChars;
        if (cArr2 == null) {
            cArr2 = j.a(this._value);
            this._quotedChars = cArr2;
        }
        int length = cArr2.length;
        if (i + length > cArr.length) {
            return -1;
        }
        System.arraycopy(cArr2, 0, cArr, i, length);
        return length;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != j.class) {
            return false;
        }
        return this._value.equals(((j) obj)._value);
    }

    @Override // b.g.a.b.k
    public final String getValue() {
        return this._value;
    }

    public final int hashCode() {
        return this._value.hashCode();
    }

    public Object readResolve() {
        return new j(this.k);
    }

    public final String toString() {
        return this._value;
    }
}
