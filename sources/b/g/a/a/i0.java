package b.g.a.a;

import java.io.Serializable;
/* compiled from: ObjectIdGenerator.java */
/* loaded from: classes2.dex */
public abstract class i0<T> implements Serializable {
    public abstract boolean a(i0<?> i0Var);

    public abstract i0<T> b(Class<?> cls);

    public abstract T c(Object obj);

    public abstract Class<?> d();

    public abstract i0<T> e(Object obj);
}
