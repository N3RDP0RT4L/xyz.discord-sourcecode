package b.g.a.a;

import java.io.Serializable;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Set;
/* compiled from: JsonIncludeProperties.java */
@Target({ElementType.ANNOTATION_TYPE, ElementType.TYPE, ElementType.METHOD, ElementType.CONSTRUCTOR, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
/* loaded from: classes2.dex */
public @interface q {

    /* compiled from: JsonIncludeProperties.java */
    /* loaded from: classes2.dex */
    public static class a implements Serializable {
        public static final a j = new a(null);
        private static final long serialVersionUID = 1;
        public final Set<String> _included;

        public a(Set<String> set) {
            this._included = set;
        }

        public boolean equals(Object obj) {
            boolean z2;
            if (obj == this) {
                return true;
            }
            if (obj != null && obj.getClass() == a.class) {
                Set<String> set = this._included;
                Set<String> set2 = ((a) obj)._included;
                if (set == null) {
                    z2 = set2 == null;
                } else {
                    z2 = set.equals(set2);
                }
                if (z2) {
                    return true;
                }
            }
            return false;
        }

        public int hashCode() {
            Set<String> set = this._included;
            if (set == null) {
                return 0;
            }
            return set.size();
        }

        public String toString() {
            return String.format("JsonIncludeProperties.Value(included=%s)", this._included);
        }
    }

    String[] value() default {};
}
