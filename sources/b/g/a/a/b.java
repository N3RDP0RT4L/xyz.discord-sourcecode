package b.g.a.a;

import java.io.Serializable;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/* compiled from: JacksonInject.java */
@Target({ElementType.ANNOTATION_TYPE, ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
/* loaded from: classes2.dex */
public @interface b {

    /* compiled from: JacksonInject.java */
    /* loaded from: classes2.dex */
    public static class a implements Serializable {
        public static final a j = new a(null, null);
        private static final long serialVersionUID = 1;
        public final Object _id;
        public final Boolean _useInput;

        public a(Object obj, Boolean bool) {
            this._id = obj;
            this._useInput = bool;
        }

        public static a a(Object obj, Boolean bool) {
            if ("".equals(obj)) {
                obj = null;
            }
            if (obj == null && bool == null) {
                return j;
            }
            return new a(obj, bool);
        }

        public boolean equals(Object obj) {
            boolean z2;
            if (obj == this) {
                return true;
            }
            if (obj != null && obj.getClass() == a.class) {
                a aVar = (a) obj;
                Boolean bool = this._useInput;
                Boolean bool2 = aVar._useInput;
                if (bool == null) {
                    z2 = bool2 == null;
                } else {
                    z2 = bool.equals(bool2);
                }
                if (z2) {
                    Object obj2 = this._id;
                    if (obj2 == null) {
                        return aVar._id == null;
                    }
                    return obj2.equals(aVar._id);
                }
            }
            return false;
        }

        public int hashCode() {
            Object obj = this._id;
            int i = 1;
            if (obj != null) {
                i = 1 + obj.hashCode();
            }
            Boolean bool = this._useInput;
            return bool != null ? i + bool.hashCode() : i;
        }

        public String toString() {
            return String.format("JacksonInject.Value(id=%s,useInput=%s)", this._id, this._useInput);
        }
    }

    m0 useInput() default m0.DEFAULT;

    String value() default "";
}
