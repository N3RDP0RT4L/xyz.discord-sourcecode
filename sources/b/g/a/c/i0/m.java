package b.g.a.c.i0;

import b.d.b.a.a;
/* compiled from: NameTransformer.java */
/* loaded from: classes2.dex */
public final class m extends n {
    public final /* synthetic */ String k;

    public m(String str) {
        this.k = str;
    }

    @Override // b.g.a.c.i0.n
    public String a(String str) {
        StringBuilder R = a.R(str);
        R.append(this.k);
        return R.toString();
    }

    public String toString() {
        return a.H(a.R("[SuffixTransformer('"), this.k, "')]");
    }
}
