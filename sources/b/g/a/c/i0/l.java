package b.g.a.c.i0;

import b.d.b.a.a;
/* compiled from: NameTransformer.java */
/* loaded from: classes2.dex */
public final class l extends n {
    public final /* synthetic */ String k;

    public l(String str) {
        this.k = str;
    }

    @Override // b.g.a.c.i0.n
    public String a(String str) {
        return a.H(new StringBuilder(), this.k, str);
    }

    public String toString() {
        return a.H(a.R("[PrefixTransformer('"), this.k, "')]");
    }
}
