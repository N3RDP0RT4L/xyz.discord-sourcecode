package b.g.a.c.i0;

import b.g.a.a.p;
import b.g.a.c.b;
import b.g.a.c.c0.g;
import b.g.a.c.c0.i;
import b.g.a.c.c0.j;
import b.g.a.c.c0.m;
import b.g.a.c.c0.s;
import b.g.a.c.t;
import b.g.a.c.z.l;
import java.util.Objects;
/* compiled from: SimpleBeanPropertyDefinition.java */
/* loaded from: classes2.dex */
public class q extends s {
    public final b k;
    public final i l;
    public final b.g.a.c.s m;
    public final t n;
    public final p.b o;

    public q(b bVar, i iVar, t tVar, b.g.a.c.s sVar, p.b bVar2) {
        this.k = bVar;
        this.l = iVar;
        this.n = tVar;
        this.m = sVar == null ? b.g.a.c.s.k : sVar;
        this.o = bVar2;
    }

    public static q w(l<?> lVar, i iVar, t tVar, b.g.a.c.s sVar, p.a aVar) {
        p.b bVar;
        p.a aVar2;
        if (aVar == null || aVar == (aVar2 = p.a.USE_DEFAULTS)) {
            bVar = s.j;
        } else if (aVar != aVar2) {
            bVar = new p.b(aVar, null, null, null);
        } else {
            bVar = p.b.j;
        }
        return new q(lVar.e(), iVar, tVar, sVar, bVar);
    }

    @Override // b.g.a.c.c0.s
    public p.b g() {
        return this.o;
    }

    @Override // b.g.a.c.c0.s
    public m k() {
        i iVar = this.l;
        if (iVar instanceof m) {
            return (m) iVar;
        }
        return null;
    }

    @Override // b.g.a.c.c0.s
    public g l() {
        i iVar = this.l;
        if (iVar instanceof g) {
            return (g) iVar;
        }
        return null;
    }

    @Override // b.g.a.c.c0.s
    public t m() {
        return this.n;
    }

    @Override // b.g.a.c.c0.s
    public j n() {
        i iVar = this.l;
        if (!(iVar instanceof j) || ((j) iVar).o() != 0) {
            return null;
        }
        return (j) this.l;
    }

    @Override // b.g.a.c.c0.s
    public b.g.a.c.s o() {
        return this.m;
    }

    @Override // b.g.a.c.c0.s
    public String p() {
        return this.n._simpleName;
    }

    @Override // b.g.a.c.c0.s
    public i q() {
        return this.l;
    }

    @Override // b.g.a.c.c0.s
    public Class<?> r() {
        i iVar = this.l;
        if (iVar == null) {
            return Object.class;
        }
        return iVar.d();
    }

    @Override // b.g.a.c.c0.s
    public j s() {
        i iVar = this.l;
        if (!(iVar instanceof j) || ((j) iVar).o() != 1) {
            return null;
        }
        return (j) this.l;
    }

    @Override // b.g.a.c.c0.s
    public t t() {
        b bVar = this.k;
        if (!(bVar == null || this.l == null)) {
            Objects.requireNonNull(bVar);
        }
        return null;
    }

    @Override // b.g.a.c.c0.s
    public boolean u() {
        return false;
    }
}
