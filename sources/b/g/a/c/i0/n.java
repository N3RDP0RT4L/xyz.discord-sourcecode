package b.g.a.c.i0;

import java.io.Serializable;
/* compiled from: NameTransformer.java */
/* loaded from: classes2.dex */
public abstract class n {
    public static final n j = new b();

    /* compiled from: NameTransformer.java */
    /* loaded from: classes2.dex */
    public static class a extends n implements Serializable {
        private static final long serialVersionUID = 1;
        public final n _t1;
        public final n _t2;

        public a(n nVar, n nVar2) {
            this._t1 = nVar;
            this._t2 = nVar2;
        }

        @Override // b.g.a.c.i0.n
        public String a(String str) {
            return this._t1.a(this._t2.a(str));
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("[ChainedTransformer(");
            R.append(this._t1);
            R.append(", ");
            R.append(this._t2);
            R.append(")]");
            return R.toString();
        }
    }

    /* compiled from: NameTransformer.java */
    /* loaded from: classes2.dex */
    public static final class b extends n implements Serializable {
        private static final long serialVersionUID = 1;

        @Override // b.g.a.c.i0.n
        public String a(String str) {
            return str;
        }
    }

    public abstract String a(String str);
}
