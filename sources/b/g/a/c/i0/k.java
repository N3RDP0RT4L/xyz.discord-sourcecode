package b.g.a.c.i0;

import b.d.b.a.a;
/* compiled from: NameTransformer.java */
/* loaded from: classes2.dex */
public final class k extends n {
    public final /* synthetic */ String k;
    public final /* synthetic */ String l;

    public k(String str, String str2) {
        this.k = str;
        this.l = str2;
    }

    @Override // b.g.a.c.i0.n
    public String a(String str) {
        return this.k + str + this.l;
    }

    public String toString() {
        StringBuilder R = a.R("[PreAndSuffixTransformer('");
        R.append(this.k);
        R.append("','");
        return a.H(R, this.l, "')]");
    }
}
