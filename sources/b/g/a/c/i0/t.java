package b.g.a.c.i0;

import b.g.a.b.e;
import b.g.a.b.g;
/* compiled from: TokenBufferReadContext.java */
/* loaded from: classes2.dex */
public class t extends g {
    public final g c = null;
    public final e d = e.j;
    public String e;
    public Object f;

    public t() {
        super(0, -1);
    }

    @Override // b.g.a.b.g
    public String a() {
        return this.e;
    }

    @Override // b.g.a.b.g
    public void d(Object obj) {
        this.f = obj;
    }
}
