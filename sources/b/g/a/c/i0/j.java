package b.g.a.c.i0;
/* compiled from: LookupCache.java */
/* loaded from: classes2.dex */
public interface j<K, V> {
    V get(Object obj);

    V putIfAbsent(K k, V v);
}
