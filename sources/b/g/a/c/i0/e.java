package b.g.a.c.i0;

import b.g.a.c.h0.n;
import b.g.a.c.j;
/* compiled from: Converter.java */
/* loaded from: classes2.dex */
public interface e<IN, OUT> {

    /* compiled from: Converter.java */
    /* loaded from: classes2.dex */
    public static abstract class a implements e<Object, Object> {
    }

    j a(n nVar);

    OUT convert(IN in);
}
