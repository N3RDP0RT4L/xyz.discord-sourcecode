package b.g.a.c.i0;

import b.d.b.a.a;
import b.g.a.c.j;
/* compiled from: TypeKey.java */
/* loaded from: classes2.dex */
public class u {
    public int a;

    /* renamed from: b  reason: collision with root package name */
    public Class<?> f726b;
    public j c;
    public boolean d;

    public u() {
    }

    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != u.class) {
            return false;
        }
        u uVar = (u) obj;
        if (uVar.d != this.d) {
            return false;
        }
        Class<?> cls = this.f726b;
        if (cls != null) {
            return uVar.f726b == cls;
        }
        return this.c.equals(uVar.c);
    }

    public final int hashCode() {
        return this.a;
    }

    public final String toString() {
        if (this.f726b != null) {
            StringBuilder R = a.R("{class: ");
            a.i0(this.f726b, R, ", typed? ");
            return a.M(R, this.d, "}");
        }
        StringBuilder R2 = a.R("{type: ");
        R2.append(this.c);
        R2.append(", typed? ");
        return a.M(R2, this.d, "}");
    }

    public u(Class<?> cls, boolean z2) {
        int i;
        this.f726b = cls;
        this.c = null;
        this.d = z2;
        if (z2) {
            i = cls.getName().hashCode() + 1;
        } else {
            i = cls.getName().hashCode();
        }
        this.a = i;
    }

    public u(j jVar, boolean z2) {
        int i;
        this.c = jVar;
        this.f726b = null;
        this.d = z2;
        if (z2) {
            i = jVar._hash - 2;
        } else {
            i = jVar._hash - 1;
        }
        this.a = i;
    }
}
