package b.g.a.c.i0;

import java.util.Iterator;
import java.util.NoSuchElementException;
/* compiled from: ArrayIterator.java */
/* loaded from: classes2.dex */
public class b<T> implements Iterator<T>, Iterable<T> {
    public final T[] j;
    public int k = 0;

    public b(T[] tArr) {
        this.j = tArr;
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return this.k < this.j.length;
    }

    @Override // java.lang.Iterable
    public Iterator<T> iterator() {
        return this;
    }

    @Override // java.util.Iterator
    public T next() {
        int i = this.k;
        T[] tArr = this.j;
        if (i < tArr.length) {
            this.k = i + 1;
            return tArr[i];
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Iterator
    public void remove() {
        throw new UnsupportedOperationException();
    }
}
