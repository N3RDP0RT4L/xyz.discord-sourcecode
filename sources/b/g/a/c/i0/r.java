package b.g.a.c.i0;

import andhook.lib.xposed.ClassUtils;
import androidx.core.app.NotificationManagerCompat;
import com.discord.utilities.time.TimeUtils;
import com.discord.widgets.chat.input.MentionUtilsKt;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/* compiled from: StdDateFormat.java */
/* loaded from: classes2.dex */
public class r extends DateFormat {
    public static final Pattern j = Pattern.compile("\\d\\d\\d\\d[-]\\d\\d[-]\\d\\d");
    public static final Pattern k;
    public static final String[] l;
    public static final TimeZone m;
    public static final Locale n;
    public static final DateFormat o;
    public static final r p;
    public static final Calendar q;
    public Boolean _lenient;
    public final Locale _locale;
    private boolean _tzSerializedWithColon;
    public transient TimeZone r;

    /* renamed from: s  reason: collision with root package name */
    public transient Calendar f725s;
    public transient DateFormat t;

    static {
        try {
            k = Pattern.compile("\\d\\d\\d\\d[-]\\d\\d[-]\\d\\d[T]\\d\\d[:]\\d\\d(?:[:]\\d\\d)?(\\.\\d+)?(Z|[+-]\\d\\d(?:[:]?\\d\\d)?)?");
            l = new String[]{"yyyy-MM-dd'T'HH:mm:ss.SSSX", "yyyy-MM-dd'T'HH:mm:ss.SSS", "EEE, dd MMM yyyy HH:mm:ss zzz", TimeUtils.UTCFormat.SHORT};
            TimeZone timeZone = TimeZone.getTimeZone("UTC");
            m = timeZone;
            Locale locale = Locale.US;
            n = locale;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", locale);
            o = simpleDateFormat;
            simpleDateFormat.setTimeZone(timeZone);
            p = new r();
            q = new GregorianCalendar(timeZone, locale);
        } catch (Throwable th) {
            throw new RuntimeException(th);
        }
    }

    public r() {
        this._tzSerializedWithColon = true;
        this._locale = n;
    }

    public static int f(String str, int i) {
        return (str.charAt(i + 1) - '0') + ((str.charAt(i) - '0') * 10);
    }

    public static int g(String str, int i) {
        int charAt = ((str.charAt(i + 1) - '0') * 100) + ((str.charAt(i) - '0') * 1000);
        return (str.charAt(i + 3) - '0') + ((str.charAt(i + 2) - '0') * 10) + charAt;
    }

    public static void j(StringBuffer stringBuffer, int i) {
        int i2 = i / 10;
        if (i2 == 0) {
            stringBuffer.append('0');
        } else {
            stringBuffer.append((char) (i2 + 48));
            i -= i2 * 10;
        }
        stringBuffer.append((char) (i + 48));
    }

    public static void k(StringBuffer stringBuffer, int i) {
        int i2 = i / 100;
        if (i2 == 0) {
            stringBuffer.append('0');
            stringBuffer.append('0');
        } else {
            if (i2 > 99) {
                stringBuffer.append(i2);
            } else {
                j(stringBuffer, i2);
            }
            i -= i2 * 100;
        }
        j(stringBuffer, i);
    }

    public Calendar b(TimeZone timeZone) {
        Calendar calendar = this.f725s;
        if (calendar == null) {
            calendar = (Calendar) q.clone();
            this.f725s = calendar;
        }
        if (!calendar.getTimeZone().equals(timeZone)) {
            calendar.setTimeZone(timeZone);
        }
        calendar.setLenient(isLenient());
        return calendar;
    }

    @Override // java.text.DateFormat, java.text.Format
    public Object clone() {
        return new r(this.r, this._locale, this._lenient, this._tzSerializedWithColon);
    }

    @Override // java.text.DateFormat
    public boolean equals(Object obj) {
        return obj == this;
    }

    @Override // java.text.DateFormat
    public StringBuffer format(Date date, StringBuffer stringBuffer, FieldPosition fieldPosition) {
        TimeZone timeZone = this.r;
        if (timeZone == null) {
            timeZone = m;
        }
        Calendar b2 = b(timeZone);
        b2.setTime(date);
        int i = b2.get(1);
        char c = '+';
        if (b2.get(0) != 0) {
            if (i > 9999) {
                stringBuffer.append('+');
            }
            k(stringBuffer, i);
        } else if (i == 1) {
            stringBuffer.append("+0000");
        } else {
            stringBuffer.append('-');
            k(stringBuffer, i - 1);
        }
        stringBuffer.append('-');
        j(stringBuffer, b2.get(2) + 1);
        stringBuffer.append('-');
        j(stringBuffer, b2.get(5));
        stringBuffer.append('T');
        j(stringBuffer, b2.get(11));
        stringBuffer.append(MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR);
        j(stringBuffer, b2.get(12));
        stringBuffer.append(MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR);
        j(stringBuffer, b2.get(13));
        stringBuffer.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
        int i2 = b2.get(14);
        int i3 = i2 / 100;
        if (i3 == 0) {
            stringBuffer.append('0');
        } else {
            stringBuffer.append((char) (i3 + 48));
            i2 -= i3 * 100;
        }
        j(stringBuffer, i2);
        int offset = timeZone.getOffset(b2.getTimeInMillis());
        if (offset != 0) {
            int i4 = offset / 60000;
            int abs = Math.abs(i4 / 60);
            int abs2 = Math.abs(i4 % 60);
            if (offset < 0) {
                c = '-';
            }
            stringBuffer.append(c);
            j(stringBuffer, abs);
            if (this._tzSerializedWithColon) {
                stringBuffer.append(MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR);
            }
            j(stringBuffer, abs2);
        } else if (this._tzSerializedWithColon) {
            stringBuffer.append("+00:00");
        } else {
            stringBuffer.append("+0000");
        }
        return stringBuffer;
    }

    @Override // java.text.DateFormat
    public TimeZone getTimeZone() {
        return this.r;
    }

    public Date h(String str) throws IllegalArgumentException, ParseException {
        char c;
        String str2;
        int length = str.length();
        TimeZone timeZone = m;
        if (!(this.r == null || 'Z' == str.charAt(length - 1))) {
            timeZone = this.r;
        }
        Calendar b2 = b(timeZone);
        b2.clear();
        int i = 0;
        if (length > 10) {
            Matcher matcher = k.matcher(str);
            if (matcher.matches()) {
                int start = matcher.start(2);
                int end = matcher.end(2);
                int i2 = end - start;
                if (i2 > 1) {
                    int f = f(str, start + 1) * 3600;
                    if (i2 >= 5) {
                        f += f(str, end - 2) * 60;
                    }
                    b2.set(15, str.charAt(start) == '-' ? f * NotificationManagerCompat.IMPORTANCE_UNSPECIFIED : f * 1000);
                    b2.set(16, 0);
                }
                b2.set(g(str, 0), f(str, 5) - 1, f(str, 8), f(str, 11), f(str, 14), (length <= 16 || str.charAt(16) != ':') ? 0 : f(str, 17));
                int start2 = matcher.start(1) + 1;
                int end2 = matcher.end(1);
                if (start2 >= end2) {
                    b2.set(14, 0);
                } else {
                    int i3 = end2 - start2;
                    if (i3 != 0) {
                        if (i3 != 1) {
                            if (i3 != 2) {
                                if (i3 == 3 || i3 <= 9) {
                                    i = 0 + (str.charAt(start2 + 2) - '0');
                                } else {
                                    throw new ParseException(String.format("Cannot parse date \"%s\": invalid fractional seconds '%s'; can use at most 9 digits", str, matcher.group(1).substring(1)), start2);
                                }
                            }
                            i += (str.charAt(start2 + 1) - '0') * 10;
                        }
                        i += (str.charAt(start2) - '0') * 100;
                    }
                    b2.set(14, i);
                }
                return b2.getTime();
            }
            c = 1;
            str2 = "yyyy-MM-dd'T'HH:mm:ss.SSSX";
        } else if (j.matcher(str).matches()) {
            b2.set(g(str, 0), f(str, 5) - 1, f(str, 8), 0, 0, 0);
            b2.set(14, 0);
            return b2.getTime();
        } else {
            str2 = TimeUtils.UTCFormat.SHORT;
            c = 1;
        }
        Object[] objArr = new Object[3];
        objArr[0] = str;
        objArr[c] = str2;
        objArr[2] = this._lenient;
        throw new ParseException(String.format("Cannot parse date \"%s\": while it seems to fit format '%s', parsing fails (leniency? %s)", objArr), 0);
    }

    @Override // java.text.DateFormat
    public int hashCode() {
        return System.identityHashCode(this);
    }

    /* JADX WARN: Code restructure failed: missing block: B:39:0x0096, code lost:
        if (r5 < 0) goto L40;
     */
    /* JADX WARN: Code restructure failed: missing block: B:43:0x009f, code lost:
        if (r0 == false) goto L49;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public java.util.Date i(java.lang.String r8, java.text.ParsePosition r9) throws java.text.ParseException {
        /*
            Method dump skipped, instructions count: 255
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.c.i0.r.i(java.lang.String, java.text.ParsePosition):java.util.Date");
    }

    @Override // java.text.DateFormat
    public boolean isLenient() {
        Boolean bool = this._lenient;
        return bool == null || bool.booleanValue();
    }

    public r l(Locale locale) {
        return locale.equals(this._locale) ? this : new r(this.r, locale, this._lenient, this._tzSerializedWithColon);
    }

    public r m(TimeZone timeZone) {
        if (timeZone == null) {
            timeZone = m;
        }
        TimeZone timeZone2 = this.r;
        return (timeZone == timeZone2 || timeZone.equals(timeZone2)) ? this : new r(timeZone, this._locale, this._lenient, this._tzSerializedWithColon);
    }

    @Override // java.text.DateFormat
    public Date parse(String str) throws ParseException {
        String[] strArr;
        String trim = str.trim();
        ParsePosition parsePosition = new ParsePosition(0);
        Date i = i(trim, parsePosition);
        if (i != null) {
            return i;
        }
        StringBuilder sb = new StringBuilder();
        for (String str2 : l) {
            if (sb.length() > 0) {
                sb.append("\", \"");
            } else {
                sb.append('\"');
            }
            sb.append(str2);
        }
        sb.append('\"');
        throw new ParseException(String.format("Cannot parse date \"%s\": not compatible with any of standard forms (%s)", trim, sb.toString()), parsePosition.getErrorIndex());
    }

    @Override // java.text.DateFormat
    public void setLenient(boolean z2) {
        Boolean valueOf = Boolean.valueOf(z2);
        Boolean bool = this._lenient;
        if (!(valueOf == bool || (valueOf != null && valueOf.equals(bool)))) {
            this._lenient = valueOf;
            this.t = null;
        }
    }

    @Override // java.text.DateFormat
    public void setTimeZone(TimeZone timeZone) {
        if (!timeZone.equals(this.r)) {
            this.t = null;
            this.r = timeZone;
        }
    }

    public String toString() {
        return String.format("DateFormat %s: (timezone: %s, locale: %s, lenient: %s)", r.class.getName(), this.r, this._locale, this._lenient);
    }

    public r(TimeZone timeZone, Locale locale, Boolean bool, boolean z2) {
        this._tzSerializedWithColon = true;
        this.r = timeZone;
        this._locale = locale;
        this._lenient = bool;
        this._tzSerializedWithColon = z2;
    }

    @Override // java.text.DateFormat
    public Date parse(String str, ParsePosition parsePosition) {
        try {
            return i(str, parsePosition);
        } catch (ParseException unused) {
            return null;
        }
    }
}
