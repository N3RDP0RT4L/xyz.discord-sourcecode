package b.g.a.c.i0;

import b.d.b.a.a;
import b.g.a.b.k;
import b.g.a.b.p.j;
import b.g.a.c.z.l;
import java.io.Serializable;
import java.lang.annotation.Annotation;
/* compiled from: EnumValues.java */
/* loaded from: classes2.dex */
public final class f implements Serializable {
    private static final long serialVersionUID = 1;
    private final Class<Enum<?>> _enumClass;
    private final k[] _textual;
    private final Enum<?>[] _values;

    public f(Class<Enum<?>> cls, k[] kVarArr) {
        this._enumClass = cls;
        this._values = cls.getEnumConstants();
        this._textual = kVarArr;
    }

    public static f a(l<?> lVar, Class<Enum<?>> cls) {
        Annotation[] annotationArr = d.a;
        Class<? super Enum<?>> superclass = cls.getSuperclass() != Enum.class ? cls.getSuperclass() : cls;
        Enum<?>[] enumArr = (Enum[]) superclass.getEnumConstants();
        if (enumArr != null) {
            String[] f = lVar.e().f(superclass, enumArr, new String[enumArr.length]);
            k[] kVarArr = new k[enumArr.length];
            int length = enumArr.length;
            for (int i = 0; i < length; i++) {
                Enum<?> r4 = enumArr[i];
                String str = f[i];
                if (str == null) {
                    str = r4.name();
                }
                kVarArr[r4.ordinal()] = new j(str);
            }
            return new f(cls, kVarArr);
        }
        StringBuilder R = a.R("Cannot determine enum constants for Class ");
        R.append(cls.getName());
        throw new IllegalArgumentException(R.toString());
    }

    public Class<Enum<?>> b() {
        return this._enumClass;
    }

    public k c(Enum<?> r2) {
        return this._textual[r2.ordinal()];
    }
}
