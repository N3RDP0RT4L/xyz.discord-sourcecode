package b.g.a.c;

import b.d.b.a.a;
import b.g.a.b.k;
import b.g.a.b.t.f;
import b.g.a.c.i0.d;
import java.io.Serializable;
import java.lang.annotation.Annotation;
/* compiled from: PropertyName.java */
/* loaded from: classes2.dex */
public class t implements Serializable {
    public static final t j = new t("", null);
    public static final t k = new t(new String(""), null);
    private static final long serialVersionUID = 1;
    public k _encodedSimple;
    public final String _namespace;
    public final String _simpleName;

    public t(String str) {
        Annotation[] annotationArr = d.a;
        this._simpleName = str;
        this._namespace = null;
    }

    public static t a(String str) {
        if (str == null || str.isEmpty()) {
            return j;
        }
        return new t(f.j.a(str), null);
    }

    public static t b(String str, String str2) {
        if (str == null) {
            str = "";
        }
        if (str2 != null || !str.isEmpty()) {
            return new t(f.j.a(str), str2);
        }
        return j;
    }

    public boolean c() {
        return !this._simpleName.isEmpty();
    }

    public boolean d() {
        return this._namespace == null && this._simpleName.isEmpty();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != t.class) {
            return false;
        }
        t tVar = (t) obj;
        String str = this._simpleName;
        if (str == null) {
            if (tVar._simpleName != null) {
                return false;
            }
        } else if (!str.equals(tVar._simpleName)) {
            return false;
        }
        String str2 = this._namespace;
        if (str2 == null) {
            return tVar._namespace == null;
        }
        return str2.equals(tVar._namespace);
    }

    public int hashCode() {
        String str = this._namespace;
        if (str == null) {
            return this._simpleName.hashCode();
        }
        return str.hashCode() ^ this._simpleName.hashCode();
    }

    public Object readResolve() {
        String str;
        return (this._namespace != null || ((str = this._simpleName) != null && !"".equals(str))) ? this : j;
    }

    public String toString() {
        if (this._namespace == null) {
            return this._simpleName;
        }
        StringBuilder R = a.R("{");
        R.append(this._namespace);
        R.append("}");
        R.append(this._simpleName);
        return R.toString();
    }

    public t(String str, String str2) {
        Annotation[] annotationArr = d.a;
        this._simpleName = str == null ? "" : str;
        this._namespace = str2;
    }
}
