package b.g.a.c;

import b.g.a.b.b;
import b.g.a.b.c;
import b.g.a.b.d;
import b.g.a.b.i;
import b.g.a.b.k;
import b.g.a.b.t.e;
import b.g.a.c.a0.d;
import b.g.a.c.c0.d0;
import b.g.a.c.c0.w;
import b.g.a.c.c0.x;
import b.g.a.c.g0.f;
import b.g.a.c.g0.j;
import b.g.a.c.g0.q;
import b.g.a.c.h0.n;
import b.g.a.c.i0.p;
import b.g.a.c.z.a;
import b.g.a.c.z.d;
import b.g.a.c.z.g;
import java.io.Closeable;
import java.io.IOException;
import java.io.Serializable;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
/* compiled from: ObjectMapper.java */
/* loaded from: classes2.dex */
public class r extends i implements Serializable {
    public static final b j;
    public static final a k;
    private static final long serialVersionUID = 2;
    public final d _coercionConfigs;
    public final g _configOverrides;
    public f _deserializationConfig;
    public b.g.a.c.a0.d _deserializationContext;
    public i _injectableValues;
    public final c _jsonFactory;
    public d0 _mixIns;
    public Set<Object> _registeredModuleTypes;
    public final ConcurrentHashMap<j, k<Object>> _rootDeserializers;
    public v _serializationConfig;
    public q _serializerFactory;
    public j _serializerProvider;
    public b.g.a.c.e0.d _subtypeResolver;
    public n _typeFactory;

    static {
        x xVar = new x();
        j = xVar;
        k = new a(null, xVar, null, n.k, null, b.g.a.c.i0.r.p, Locale.getDefault(), null, b.f652b, b.g.a.c.e0.h.g.j, new w.b());
    }

    public r() {
        this(null, null, null);
    }

    public final void a(String str, Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException(String.format("argument \"%s\" is null", str));
        }
    }

    public final void b(b.g.a.b.d dVar, Object obj) throws IOException {
        Exception e;
        v vVar = this._serializationConfig;
        if (!vVar.v(w.CLOSE_CLOSEABLE) || !(obj instanceof Closeable)) {
            try {
                this._serializerProvider.K(vVar, this._serializerFactory).L(dVar, obj);
                dVar.close();
            } catch (Exception e2) {
                Annotation[] annotationArr = b.g.a.c.i0.d.a;
                dVar.b(d.a.AUTO_CLOSE_JSON_CONTENT);
                try {
                    dVar.close();
                } catch (Exception e3) {
                    e2.addSuppressed(e3);
                }
                if (!(e2 instanceof IOException)) {
                    b.g.a.c.i0.d.x(e2);
                    throw new RuntimeException(e2);
                }
                throw ((IOException) e2);
            }
        } else {
            Closeable closeable = (Closeable) obj;
            try {
                this._serializerProvider.K(vVar, this._serializerFactory).L(dVar, obj);
            } catch (Exception e4) {
                e = e4;
            }
            try {
                closeable.close();
                dVar.close();
            } catch (Exception e5) {
                e = e5;
                closeable = null;
                b.g.a.c.i0.d.f(dVar, closeable, e);
                throw null;
            }
        }
    }

    public b.g.a.b.d c(Writer writer) throws IOException {
        a("w", writer);
        c cVar = this._jsonFactory;
        b.g.a.b.p.c cVar2 = new b.g.a.b.p.c(cVar.a(), writer, false);
        if (cVar._outputDecorator == null) {
            b.g.a.b.q.d dVar = new b.g.a.b.q.d(cVar2, cVar._generatorFeatures, cVar._objectCodec, writer, cVar._quoteChar);
            int i = cVar._maximumNonEscapedChar;
            if (i > 0) {
                dVar.y0(i);
            }
            if (cVar._characterEscapes == null) {
                k kVar = cVar._rootValueSeparator;
                if (kVar != c.m) {
                    dVar.v = kVar;
                }
                v vVar = this._serializationConfig;
                Objects.requireNonNull(vVar);
                if (w.INDENT_OUTPUT.h(vVar._serFeatures) && dVar.l == null) {
                    b.g.a.b.j jVar = vVar._defaultPrettyPrinter;
                    if (jVar instanceof e) {
                        jVar = (b.g.a.b.j) ((e) jVar).e();
                    }
                    if (jVar != null) {
                        dVar.l = jVar;
                    }
                }
                boolean h = w.WRITE_BIGDECIMAL_AS_PLAIN.h(vVar._serFeatures);
                int i2 = vVar._generatorFeaturesToChange;
                if (i2 != 0 || h) {
                    int i3 = vVar._generatorFeatures;
                    if (h) {
                        int h2 = d.a.WRITE_BIGDECIMAL_AS_PLAIN.h();
                        i3 |= h2;
                        i2 |= h2;
                    }
                    dVar.w0(i3, i2);
                }
                return dVar;
            }
            throw null;
        }
        throw null;
    }

    public r(c cVar, j jVar, b.g.a.c.a0.d dVar) {
        this._rootDeserializers = new ConcurrentHashMap<>(64, 0.6f, 2);
        if (cVar == null) {
            this._jsonFactory = new q(this);
        } else {
            this._jsonFactory = cVar;
            if (cVar.b() == null) {
                cVar._objectCodec = this;
            }
        }
        this._subtypeResolver = new b.g.a.c.e0.h.i();
        p pVar = new p();
        this._typeFactory = n.k;
        d0 d0Var = new d0(null);
        this._mixIns = d0Var;
        a aVar = k;
        b.g.a.c.c0.r rVar = new b.g.a.c.c0.r();
        a aVar2 = aVar._classIntrospector == rVar ? aVar : new a(rVar, aVar._annotationIntrospector, aVar._propertyNamingStrategy, aVar._typeFactory, aVar._typeResolverBuilder, aVar._dateFormat, aVar._locale, aVar._timeZone, aVar._defaultBase64, aVar._typeValidator, aVar._accessorNaming);
        g gVar = new g();
        this._configOverrides = gVar;
        b.g.a.c.z.d dVar2 = new b.g.a.c.z.d();
        this._coercionConfigs = dVar2;
        a aVar3 = aVar2;
        this._serializationConfig = new v(aVar3, this._subtypeResolver, d0Var, pVar, gVar);
        this._deserializationConfig = new f(aVar3, this._subtypeResolver, d0Var, pVar, gVar, dVar2);
        Objects.requireNonNull(this._jsonFactory);
        v vVar = this._serializationConfig;
        p pVar2 = p.SORT_PROPERTIES_ALPHABETICALLY;
        if (vVar.q(pVar2)) {
            this._serializationConfig = this._serializationConfig.t(pVar2);
            this._deserializationConfig = this._deserializationConfig.t(pVar2);
        }
        this._serializerProvider = new j.a();
        this._deserializationContext = new d.a(b.g.a.c.a0.b.j);
        this._serializerFactory = f.l;
    }
}
