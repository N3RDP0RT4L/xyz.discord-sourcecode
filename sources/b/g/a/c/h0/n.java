package b.g.a.c.h0;

import b.d.b.a.a;
import b.g.a.c.i0.d;
import b.g.a.c.i0.h;
import b.g.a.c.j;
import b.g.a.c.l;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.TreeMap;
import java.util.TreeSet;
/* compiled from: TypeFactory.java */
/* loaded from: classes2.dex */
public class n implements Serializable {

    /* renamed from: s  reason: collision with root package name */
    public static final Class<?> f718s;
    private static final long serialVersionUID = 1;
    public static final Class<?> t;
    public static final Class<?> u;
    public static final k v;
    public static final k w;

    /* renamed from: x  reason: collision with root package name */
    public static final k f719x;
    public static final j[] j = new j[0];
    public static final n k = new n();
    public static final m l = m.l;
    public static final Class<?> m = String.class;
    public static final Class<?> n = Object.class;
    public static final Class<?> o = Comparable.class;
    public static final Class<?> p = Class.class;
    public static final Class<?> q = Enum.class;
    public static final Class<?> r = l.class;

    /* renamed from: y  reason: collision with root package name */
    public static final k f720y = new k(String.class);

    /* renamed from: z  reason: collision with root package name */
    public static final k f721z = new k(Object.class);
    public static final k A = new k(Comparable.class);
    public static final k B = new k(Enum.class);
    public static final k C = new k(Class.class);
    public static final k D = new k(l.class);
    public final b.g.a.c.i0.j<Object, j> _typeCache = new h(16, 200);
    public final p _parser = new p(this);
    public final o[] _modifiers = null;
    public final ClassLoader _classLoader = null;

    static {
        Class<?> cls = Boolean.TYPE;
        f718s = cls;
        Class<?> cls2 = Integer.TYPE;
        t = cls2;
        Class<?> cls3 = Long.TYPE;
        u = cls3;
        v = new k(cls);
        w = new k(cls2);
        f719x = new k(cls3);
    }

    public static j k() {
        Objects.requireNonNull(k);
        return f721z;
    }

    public j a(Class<?> cls) {
        if (cls.isPrimitive()) {
            if (cls == f718s) {
                return v;
            }
            if (cls == t) {
                return w;
            }
            if (cls == u) {
                return f719x;
            }
            return null;
        } else if (cls == m) {
            return f720y;
        } else {
            if (cls == n) {
                return f721z;
            }
            if (cls == r) {
                return D;
            }
            return null;
        }
    }

    public j b(c cVar, Type type, m mVar) {
        j jVar;
        Type[] bounds;
        m mVar2;
        if (type instanceof Class) {
            jVar = c(cVar, (Class) type, l);
        } else if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            Class<?> cls = (Class) parameterizedType.getRawType();
            if (cls == q) {
                jVar = B;
            } else if (cls == o) {
                jVar = A;
            } else if (cls == p) {
                jVar = C;
            } else {
                Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                int length = actualTypeArguments == null ? 0 : actualTypeArguments.length;
                if (length == 0) {
                    mVar2 = l;
                } else {
                    j[] jVarArr = new j[length];
                    for (int i = 0; i < length; i++) {
                        jVarArr[i] = b(cVar, actualTypeArguments[i], mVar);
                    }
                    mVar2 = m.d(cls, jVarArr);
                }
                jVar = c(cVar, cls, mVar2);
            }
        } else if (type instanceof j) {
            return (j) type;
        } else {
            if (type instanceof GenericArrayType) {
                jVar = a.L(b(cVar, ((GenericArrayType) type).getGenericComponentType(), mVar), mVar);
            } else if (type instanceof TypeVariable) {
                TypeVariable typeVariable = (TypeVariable) type;
                String name = typeVariable.getName();
                if (mVar != null) {
                    j e = mVar.e(name);
                    if (e != null) {
                        jVar = e;
                    } else if (mVar.h(name)) {
                        jVar = f721z;
                    } else {
                        m l2 = mVar.l(name);
                        synchronized (typeVariable) {
                            bounds = typeVariable.getBounds();
                        }
                        jVar = b(cVar, bounds[0], l2);
                    }
                } else {
                    throw new IllegalArgumentException(a.w("Null `bindings` passed (type variable \"", name, "\")"));
                }
            } else if (type instanceof WildcardType) {
                jVar = b(cVar, ((WildcardType) type).getUpperBounds()[0], mVar);
            } else {
                StringBuilder R = a.R("Unrecognized Type: ");
                R.append(type == null ? "[null]" : type.toString());
                throw new IllegalArgumentException(R.toString());
            }
        }
        if (this._modifiers != null) {
            jVar.j();
            o[] oVarArr = this._modifiers;
            if (oVarArr.length > 0) {
                o oVar = oVarArr[0];
                throw null;
            }
        }
        return jVar;
    }

    /* JADX WARN: Removed duplicated region for block: B:98:0x01ac  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.g.a.c.j c(b.g.a.c.h0.c r20, java.lang.Class<?> r21, b.g.a.c.h0.m r22) {
        /*
            Method dump skipped, instructions count: 526
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.c.h0.n.c(b.g.a.c.h0.c, java.lang.Class, b.g.a.c.h0.m):b.g.a.c.j");
    }

    public j d(Class<?> cls, m mVar, j jVar, j[] jVarArr) {
        return new k(cls, mVar, jVar, jVarArr, null, null, false);
    }

    public j[] e(c cVar, Class<?> cls, m mVar) {
        Annotation[] annotationArr = d.a;
        Type[] genericInterfaces = cls.getGenericInterfaces();
        if (genericInterfaces == null || genericInterfaces.length == 0) {
            return j;
        }
        int length = genericInterfaces.length;
        j[] jVarArr = new j[length];
        for (int i = 0; i < length; i++) {
            jVarArr[i] = b(cVar, genericInterfaces[i], mVar);
        }
        return jVarArr;
    }

    public final boolean f(j jVar, j jVar2) {
        if (jVar2 instanceof h) {
            ((h) jVar2)._actualType = jVar;
            return true;
        } else if (jVar._class != jVar2._class) {
            return false;
        } else {
            List<j> g = jVar.j().g();
            List<j> g2 = jVar2.j().g();
            int size = g.size();
            for (int i = 0; i < size; i++) {
                if (!f(g.get(i), g2.get(i))) {
                    return false;
                }
            }
            return true;
        }
    }

    public j g(j jVar, Class<?> cls) {
        Class<?> cls2 = jVar._class;
        if (cls2 == cls) {
            return jVar;
        }
        j i = jVar.i(cls);
        if (i != null) {
            return i;
        }
        if (!cls.isAssignableFrom(cls2)) {
            throw new IllegalArgumentException(String.format("Class %s not a super-type of %s", cls.getName(), jVar));
        }
        throw new IllegalArgumentException(String.format("Internal error: class %s not included as super-type for %s", cls.getName(), jVar));
    }

    public j h(j jVar, Class<?> cls, boolean z2) throws IllegalArgumentException {
        j jVar2;
        String str;
        Class<?> cls2 = jVar._class;
        if (cls2 == cls) {
            return jVar;
        }
        if (cls2 == Object.class) {
            jVar2 = c(null, cls, l);
        } else if (cls2.isAssignableFrom(cls)) {
            if (jVar.v()) {
                if (jVar.z()) {
                    if (cls == HashMap.class || cls == LinkedHashMap.class || cls == EnumMap.class || cls == TreeMap.class) {
                        jVar2 = c(null, cls, m.c(cls, jVar.o(), jVar.k()));
                    }
                } else if (jVar.u()) {
                    if (cls == ArrayList.class || cls == LinkedList.class || cls == HashSet.class || cls == TreeSet.class) {
                        jVar2 = c(null, cls, m.b(cls, jVar.k()));
                    } else if (cls2 == EnumSet.class) {
                        return jVar;
                    }
                }
            }
            if (jVar.j().i()) {
                jVar2 = c(null, cls, l);
            } else {
                int length = cls.getTypeParameters().length;
                if (length == 0) {
                    jVar2 = c(null, cls, l);
                } else {
                    h[] hVarArr = new h[length];
                    for (int i = 0; i < length; i++) {
                        hVarArr[i] = new h(i);
                    }
                    j i2 = c(null, cls, m.d(cls, hVarArr)).i(jVar._class);
                    if (i2 != null) {
                        List<j> g = jVar.j().g();
                        List<j> g2 = i2.j().g();
                        int size = g2.size();
                        int size2 = g.size();
                        int i3 = 0;
                        while (i3 < size2) {
                            j jVar3 = g.get(i3);
                            j k2 = i3 < size ? g2.get(i3) : k();
                            if (!f(jVar3, k2)) {
                                if (!(jVar3._class == Object.class)) {
                                    if (i3 == 0 && jVar.z()) {
                                        if (k2._class == Object.class) {
                                            continue;
                                        }
                                    }
                                    if (jVar3._class.isInterface()) {
                                        Class<?> cls3 = k2._class;
                                        Class<?> cls4 = jVar3._class;
                                        if (cls4 == cls3 || cls4.isAssignableFrom(cls3)) {
                                        }
                                    }
                                    str = String.format("Type parameter #%d/%d differs; can not specialize %s with %s", Integer.valueOf(i3 + 1), Integer.valueOf(size2), jVar3.e(), k2.e());
                                    break;
                                }
                                continue;
                            }
                            i3++;
                        }
                        str = null;
                        if (str == null || z2) {
                            j[] jVarArr = new j[length];
                            for (int i4 = 0; i4 < length; i4++) {
                                j jVar4 = hVarArr[i4]._actualType;
                                if (jVar4 == null) {
                                    jVar4 = k();
                                }
                                jVarArr[i4] = jVar4;
                            }
                            jVar2 = c(null, cls, m.d(cls, jVarArr));
                        } else {
                            StringBuilder R = a.R("Failed to specialize base type ");
                            R.append(jVar.e());
                            R.append(" as ");
                            R.append(cls.getName());
                            R.append(", problem: ");
                            R.append(str);
                            throw new IllegalArgumentException(R.toString());
                        }
                    } else {
                        throw new IllegalArgumentException(String.format("Internal error: unable to locate supertype (%s) from resolved subtype %s", jVar._class.getName(), cls.getName()));
                    }
                }
            }
        } else {
            throw new IllegalArgumentException(String.format("Class %s not subtype of %s", d.u(cls), d.n(jVar)));
        }
        return jVar2.F(jVar);
    }

    public j[] i(j jVar, Class<?> cls) {
        j i = jVar.i(cls);
        if (i == null) {
            return j;
        }
        return i.j().k();
    }

    @Deprecated
    public j j(Class<?> cls) {
        j a;
        m mVar = l;
        return (!mVar.i() || (a = a(cls)) == null) ? d(cls, mVar, null, null) : a;
    }
}
