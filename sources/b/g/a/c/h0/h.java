package b.g.a.c.h0;

import andhook.lib.xposed.ClassUtils;
import b.d.b.a.a;
import b.g.a.c.j;
/* compiled from: PlaceholderForType.java */
/* loaded from: classes2.dex */
public class h extends l {
    private static final long serialVersionUID = 1;
    public j _actualType;
    public final int _ordinal;

    public h(int i) {
        super(Object.class, m.l, n.k(), null, 1, null, null, false);
        this._ordinal = i;
    }

    @Override // b.g.a.c.j
    public j C(Class<?> cls, m mVar, j jVar, j[] jVarArr) {
        L();
        throw null;
    }

    @Override // b.g.a.c.j
    public j D(j jVar) {
        L();
        throw null;
    }

    @Override // b.g.a.c.j
    public j E(Object obj) {
        L();
        throw null;
    }

    @Override // b.g.a.c.j
    public j G() {
        L();
        throw null;
    }

    @Override // b.g.a.c.j
    public j H(Object obj) {
        L();
        throw null;
    }

    @Override // b.g.a.c.j
    public j I(Object obj) {
        L();
        throw null;
    }

    @Override // b.g.a.c.h0.l
    public String K() {
        return toString();
    }

    public final <T> T L() {
        StringBuilder R = a.R("Operation should not be attempted on ");
        R.append(h.class.getName());
        throw new UnsupportedOperationException(R.toString());
    }

    @Override // b.g.a.c.j
    public boolean equals(Object obj) {
        return obj == this;
    }

    @Override // b.g.a.c.j
    public StringBuilder l(StringBuilder sb) {
        sb.append(ClassUtils.INNER_CLASS_SEPARATOR_CHAR);
        sb.append(this._ordinal + 1);
        return sb;
    }

    @Override // b.g.a.c.j
    public StringBuilder m(StringBuilder sb) {
        l(sb);
        return sb;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        l(sb);
        return sb.toString();
    }

    @Override // b.g.a.c.j
    public boolean v() {
        return false;
    }
}
