package b.g.a.c.h0;

import b.g.a.c.i0.d;
import b.g.a.c.j;
import java.io.Serializable;
import java.lang.reflect.TypeVariable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
/* compiled from: TypeBindings.java */
/* loaded from: classes2.dex */
public class m implements Serializable {
    public static final String[] j;
    public static final j[] k;
    public static final m l;
    private static final long serialVersionUID = 1;
    private final int _hashCode;
    private final String[] _names;
    private final j[] _types;
    private final String[] _unboundVariables;

    /* compiled from: TypeBindings.java */
    /* loaded from: classes2.dex */
    public static final class a {
        public final Class<?> a;

        /* renamed from: b  reason: collision with root package name */
        public final j[] f716b;
        public final int c;

        public a(Class<?> cls, j[] jVarArr, int i) {
            this.a = cls;
            this.f716b = jVarArr;
            this.c = i;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj == null || obj.getClass() != a.class) {
                return false;
            }
            a aVar = (a) obj;
            if (this.c == aVar.c && this.a == aVar.a) {
                j[] jVarArr = aVar.f716b;
                int length = this.f716b.length;
                if (length == jVarArr.length) {
                    for (int i = 0; i < length; i++) {
                        if (!this.f716b[i].equals(jVarArr[i])) {
                            return false;
                        }
                    }
                    return true;
                }
            }
            return false;
        }

        public int hashCode() {
            return this.c;
        }

        public String toString() {
            return b.d.b.a.a.n(this.a, new StringBuilder(), "<>");
        }
    }

    /* compiled from: TypeBindings.java */
    /* loaded from: classes2.dex */
    public static class b {
        public static final TypeVariable<?>[] a = AbstractList.class.getTypeParameters();

        /* renamed from: b  reason: collision with root package name */
        public static final TypeVariable<?>[] f717b = Collection.class.getTypeParameters();
        public static final TypeVariable<?>[] c = Iterable.class.getTypeParameters();
        public static final TypeVariable<?>[] d = List.class.getTypeParameters();
        public static final TypeVariable<?>[] e = ArrayList.class.getTypeParameters();
        public static final TypeVariable<?>[] f = Map.class.getTypeParameters();
        public static final TypeVariable<?>[] g = HashMap.class.getTypeParameters();
        public static final TypeVariable<?>[] h = LinkedHashMap.class.getTypeParameters();
    }

    static {
        String[] strArr = new String[0];
        j = strArr;
        j[] jVarArr = new j[0];
        k = jVarArr;
        l = new m(strArr, jVarArr, null);
    }

    public m(String[] strArr, j[] jVarArr, String[] strArr2) {
        strArr = strArr == null ? j : strArr;
        this._names = strArr;
        jVarArr = jVarArr == null ? k : jVarArr;
        this._types = jVarArr;
        if (strArr.length == jVarArr.length) {
            int length = jVarArr.length;
            int i = 1;
            for (int i2 = 0; i2 < length; i2++) {
                i += this._types[i2]._hash;
            }
            this._unboundVariables = strArr2;
            this._hashCode = i;
            return;
        }
        StringBuilder R = b.d.b.a.a.R("Mismatching names (");
        R.append(strArr.length);
        R.append("), types (");
        throw new IllegalArgumentException(b.d.b.a.a.A(R, jVarArr.length, ")"));
    }

    public static m b(Class<?> cls, j jVar) {
        TypeVariable[] typeVariableArr;
        TypeVariable<?>[] typeVariableArr2 = b.a;
        if (cls == Collection.class) {
            typeVariableArr = b.f717b;
        } else if (cls == List.class) {
            typeVariableArr = b.d;
        } else if (cls == ArrayList.class) {
            typeVariableArr = b.e;
        } else if (cls == AbstractList.class) {
            typeVariableArr = b.a;
        } else if (cls == Iterable.class) {
            typeVariableArr = b.c;
        } else {
            typeVariableArr = cls.getTypeParameters();
        }
        int length = typeVariableArr == null ? 0 : typeVariableArr.length;
        if (length == 1) {
            return new m(new String[]{typeVariableArr[0].getName()}, new j[]{jVar}, null);
        }
        StringBuilder R = b.d.b.a.a.R("Cannot create TypeBindings for class ");
        R.append(cls.getName());
        R.append(" with 1 type parameter: class expects ");
        R.append(length);
        throw new IllegalArgumentException(R.toString());
    }

    public static m c(Class<?> cls, j jVar, j jVar2) {
        TypeVariable[] typeVariableArr;
        TypeVariable<?>[] typeVariableArr2 = b.a;
        if (cls == Map.class) {
            typeVariableArr = b.f;
        } else if (cls == HashMap.class) {
            typeVariableArr = b.g;
        } else if (cls == LinkedHashMap.class) {
            typeVariableArr = b.h;
        } else {
            typeVariableArr = cls.getTypeParameters();
        }
        int length = typeVariableArr == null ? 0 : typeVariableArr.length;
        if (length == 2) {
            return new m(new String[]{typeVariableArr[0].getName(), typeVariableArr[1].getName()}, new j[]{jVar, jVar2}, null);
        }
        StringBuilder R = b.d.b.a.a.R("Cannot create TypeBindings for class ");
        R.append(cls.getName());
        R.append(" with 2 type parameters: class expects ");
        R.append(length);
        throw new IllegalArgumentException(R.toString());
    }

    public static m d(Class<?> cls, j[] jVarArr) {
        String[] strArr;
        int length = jVarArr.length;
        if (length == 1) {
            return b(cls, jVarArr[0]);
        }
        if (length == 2) {
            return c(cls, jVarArr[0], jVarArr[1]);
        }
        TypeVariable<Class<?>>[] typeParameters = cls.getTypeParameters();
        if (typeParameters == null || typeParameters.length == 0) {
            strArr = j;
        } else {
            int length2 = typeParameters.length;
            strArr = new String[length2];
            for (int i = 0; i < length2; i++) {
                strArr[i] = typeParameters[i].getName();
            }
        }
        if (strArr.length == jVarArr.length) {
            return new m(strArr, jVarArr, null);
        }
        StringBuilder R = b.d.b.a.a.R("Cannot create TypeBindings for class ");
        b.d.b.a.a.i0(cls, R, " with ");
        R.append(jVarArr.length);
        R.append(" type parameter");
        R.append(jVarArr.length == 1 ? "" : "s");
        R.append(": class expects ");
        R.append(strArr.length);
        throw new IllegalArgumentException(R.toString());
    }

    public Object a(Class<?> cls) {
        return new a(cls, this._types, this._hashCode);
    }

    public j e(String str) {
        j jVar;
        int length = this._names.length;
        for (int i = 0; i < length; i++) {
            if (str.equals(this._names[i])) {
                j jVar2 = this._types[i];
                return (!(jVar2 instanceof j) || (jVar = ((j) jVar2)._referencedType) == null) ? jVar2 : jVar;
            }
        }
        return null;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!d.o(obj, m.class)) {
            return false;
        }
        int length = this._types.length;
        j[] jVarArr = ((m) obj)._types;
        if (length != jVarArr.length) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            if (!jVarArr[i].equals(this._types[i])) {
                return false;
            }
        }
        return true;
    }

    public j f(int i) {
        if (i < 0) {
            return null;
        }
        j[] jVarArr = this._types;
        if (i >= jVarArr.length) {
            return null;
        }
        return jVarArr[i];
    }

    public List<j> g() {
        j[] jVarArr = this._types;
        if (jVarArr.length == 0) {
            return Collections.emptyList();
        }
        return Arrays.asList(jVarArr);
    }

    public boolean h(String str) {
        String[] strArr = this._unboundVariables;
        if (strArr == null) {
            return false;
        }
        int length = strArr.length;
        do {
            length--;
            if (length < 0) {
                return false;
            }
        } while (!str.equals(this._unboundVariables[length]));
        return true;
    }

    public int hashCode() {
        return this._hashCode;
    }

    public boolean i() {
        return this._types.length == 0;
    }

    public int j() {
        return this._types.length;
    }

    public j[] k() {
        return this._types;
    }

    public m l(String str) {
        String[] strArr = this._unboundVariables;
        int length = strArr == null ? 0 : strArr.length;
        String[] strArr2 = length == 0 ? new String[1] : (String[]) Arrays.copyOf(strArr, length + 1);
        strArr2[length] = str;
        return new m(this._names, this._types, strArr2);
    }

    public Object readResolve() {
        String[] strArr = this._names;
        return (strArr == null || strArr.length == 0) ? l : this;
    }

    public String toString() {
        if (this._types.length == 0) {
            return "<>";
        }
        StringBuilder O = b.d.b.a.a.O('<');
        int length = this._types.length;
        for (int i = 0; i < length; i++) {
            if (i > 0) {
                O.append(',');
            }
            j jVar = this._types[i];
            StringBuilder sb = new StringBuilder(40);
            jVar.m(sb);
            O.append(sb.toString());
        }
        O.append('>');
        return O.toString();
    }
}
