package b.g.a.c.h0;

import b.g.a.c.j;
/* compiled from: MapLikeType.java */
/* loaded from: classes2.dex */
public class f extends l {
    private static final long serialVersionUID = 1;
    public final j _keyType;
    public final j _valueType;

    public f(Class<?> cls, m mVar, j jVar, j[] jVarArr, j jVar2, j jVar3, Object obj, Object obj2, boolean z2) {
        super(cls, mVar, jVar, jVarArr, jVar2._hash ^ jVar3._hash, obj, obj2, z2);
        this._keyType = jVar2;
        this._valueType = jVar3;
    }

    @Override // b.g.a.c.j
    public j C(Class<?> cls, m mVar, j jVar, j[] jVarArr) {
        return new f(cls, mVar, jVar, jVarArr, this._keyType, this._valueType, this._valueHandler, this._typeHandler, this._asStatic);
    }

    @Override // b.g.a.c.j
    public j D(j jVar) {
        return this._valueType == jVar ? this : new f(this._class, this._bindings, this._superClass, this._superInterfaces, this._keyType, jVar, this._valueHandler, this._typeHandler, this._asStatic);
    }

    @Override // b.g.a.c.j
    public j F(j jVar) {
        j F;
        j F2;
        j F3 = super.F(jVar);
        j o = jVar.o();
        if (!(!(F3 instanceof f) || o == null || (F2 = this._keyType.F(o)) == this._keyType)) {
            F3 = ((f) F3).M(F2);
        }
        j k = jVar.k();
        return (k == null || (F = this._valueType.F(k)) == this._valueType) ? F3 : F3.D(F);
    }

    @Override // b.g.a.c.h0.l
    public String K() {
        StringBuilder sb = new StringBuilder();
        sb.append(this._class.getName());
        if (this._keyType != null) {
            sb.append('<');
            sb.append(this._keyType.e());
            sb.append(',');
            sb.append(this._valueType.e());
            sb.append('>');
        }
        return sb.toString();
    }

    /* renamed from: L */
    public f E(Object obj) {
        return new f(this._class, this._bindings, this._superClass, this._superInterfaces, this._keyType, this._valueType.H(obj), this._valueHandler, this._typeHandler, this._asStatic);
    }

    public f M(j jVar) {
        return jVar == this._keyType ? this : new f(this._class, this._bindings, this._superClass, this._superInterfaces, jVar, this._valueType, this._valueHandler, this._typeHandler, this._asStatic);
    }

    /* renamed from: N */
    public f G() {
        return this._asStatic ? this : new f(this._class, this._bindings, this._superClass, this._superInterfaces, this._keyType, this._valueType.G(), this._valueHandler, this._typeHandler, true);
    }

    /* renamed from: O */
    public f H(Object obj) {
        return new f(this._class, this._bindings, this._superClass, this._superInterfaces, this._keyType, this._valueType, this._valueHandler, obj, this._asStatic);
    }

    /* renamed from: P */
    public f I(Object obj) {
        return new f(this._class, this._bindings, this._superClass, this._superInterfaces, this._keyType, this._valueType, obj, this._typeHandler, this._asStatic);
    }

    @Override // b.g.a.c.j
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        f fVar = (f) obj;
        return this._class == fVar._class && this._keyType.equals(fVar._keyType) && this._valueType.equals(fVar._valueType);
    }

    @Override // b.g.a.c.j
    public j k() {
        return this._valueType;
    }

    @Override // b.g.a.c.j
    public StringBuilder l(StringBuilder sb) {
        l.J(this._class, sb, true);
        return sb;
    }

    @Override // b.g.a.c.j
    public StringBuilder m(StringBuilder sb) {
        l.J(this._class, sb, false);
        sb.append('<');
        this._keyType.m(sb);
        this._valueType.m(sb);
        sb.append(">;");
        return sb;
    }

    @Override // b.g.a.c.j
    public j o() {
        return this._keyType;
    }

    @Override // b.g.a.c.j
    public boolean s() {
        return super.s() || this._valueType.s() || this._keyType.s();
    }

    public String toString() {
        return String.format("[map-like type; class %s, %s -> %s]", this._class.getName(), this._keyType, this._valueType);
    }

    @Override // b.g.a.c.j
    public boolean v() {
        return true;
    }

    @Override // b.g.a.c.j
    public boolean z() {
        return true;
    }
}
