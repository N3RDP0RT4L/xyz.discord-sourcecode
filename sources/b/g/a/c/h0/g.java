package b.g.a.c.h0;

import b.d.b.a.a;
import b.g.a.c.j;
/* compiled from: MapType.java */
/* loaded from: classes2.dex */
public final class g extends f {
    private static final long serialVersionUID = 1;

    public g(Class<?> cls, m mVar, j jVar, j[] jVarArr, j jVar2, j jVar3, Object obj, Object obj2, boolean z2) {
        super(cls, mVar, jVar, jVarArr, jVar2, jVar3, obj, obj2, z2);
    }

    public static g Q(Class<?> cls, m mVar, j jVar, j[] jVarArr, j jVar2, j jVar3) {
        return new g(cls, mVar, jVar, jVarArr, jVar2, jVar3, null, null, false);
    }

    @Override // b.g.a.c.h0.f, b.g.a.c.j
    public j C(Class<?> cls, m mVar, j jVar, j[] jVarArr) {
        return new g(cls, mVar, jVar, jVarArr, this._keyType, this._valueType, this._valueHandler, this._typeHandler, this._asStatic);
    }

    @Override // b.g.a.c.h0.f, b.g.a.c.j
    public j D(j jVar) {
        return this._valueType == jVar ? this : new g(this._class, this._bindings, this._superClass, this._superInterfaces, this._keyType, jVar, this._valueHandler, this._typeHandler, this._asStatic);
    }

    @Override // b.g.a.c.h0.f
    public f M(j jVar) {
        return jVar == this._keyType ? this : new g(this._class, this._bindings, this._superClass, this._superInterfaces, jVar, this._valueType, this._valueHandler, this._typeHandler, this._asStatic);
    }

    /* renamed from: R */
    public g L(Object obj) {
        return new g(this._class, this._bindings, this._superClass, this._superInterfaces, this._keyType, this._valueType.H(obj), this._valueHandler, this._typeHandler, this._asStatic);
    }

    /* renamed from: S */
    public g N() {
        return this._asStatic ? this : new g(this._class, this._bindings, this._superClass, this._superInterfaces, this._keyType.G(), this._valueType.G(), this._valueHandler, this._typeHandler, true);
    }

    /* renamed from: T */
    public g O(Object obj) {
        return new g(this._class, this._bindings, this._superClass, this._superInterfaces, this._keyType, this._valueType, this._valueHandler, obj, this._asStatic);
    }

    /* renamed from: U */
    public g P(Object obj) {
        return new g(this._class, this._bindings, this._superClass, this._superInterfaces, this._keyType, this._valueType, obj, this._typeHandler, this._asStatic);
    }

    @Override // b.g.a.c.h0.f
    public String toString() {
        StringBuilder R = a.R("[map type; class ");
        a.i0(this._class, R, ", ");
        R.append(this._keyType);
        R.append(" -> ");
        R.append(this._valueType);
        R.append("]");
        return R.toString();
    }
}
