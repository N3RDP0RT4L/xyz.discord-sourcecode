package b.g.a.c.h0;

import b.d.b.a.a;
import b.g.a.c.j;
/* compiled from: CollectionType.java */
/* loaded from: classes2.dex */
public final class e extends d {
    private static final long serialVersionUID = 1;

    public e(Class<?> cls, m mVar, j jVar, j[] jVarArr, j jVar2, Object obj, Object obj2, boolean z2) {
        super(cls, mVar, jVar, jVarArr, jVar2, obj, obj2, z2);
    }

    @Override // b.g.a.c.h0.d, b.g.a.c.j
    public j C(Class<?> cls, m mVar, j jVar, j[] jVarArr) {
        return new e(cls, mVar, jVar, jVarArr, this._elementType, this._valueHandler, this._typeHandler, this._asStatic);
    }

    @Override // b.g.a.c.h0.d, b.g.a.c.j
    public j D(j jVar) {
        return this._elementType == jVar ? this : new e(this._class, this._bindings, this._superClass, this._superInterfaces, jVar, this._valueHandler, this._typeHandler, this._asStatic);
    }

    /* renamed from: P */
    public e L(Object obj) {
        return new e(this._class, this._bindings, this._superClass, this._superInterfaces, this._elementType.H(obj), this._valueHandler, this._typeHandler, this._asStatic);
    }

    /* renamed from: Q */
    public e M() {
        return this._asStatic ? this : new e(this._class, this._bindings, this._superClass, this._superInterfaces, this._elementType.G(), this._valueHandler, this._typeHandler, true);
    }

    /* renamed from: R */
    public e N(Object obj) {
        return new e(this._class, this._bindings, this._superClass, this._superInterfaces, this._elementType, this._valueHandler, obj, this._asStatic);
    }

    /* renamed from: S */
    public e O(Object obj) {
        return new e(this._class, this._bindings, this._superClass, this._superInterfaces, this._elementType, obj, this._typeHandler, this._asStatic);
    }

    @Override // b.g.a.c.h0.d
    public String toString() {
        StringBuilder R = a.R("[collection type; class ");
        a.i0(this._class, R, ", contains ");
        R.append(this._elementType);
        R.append("]");
        return R.toString();
    }
}
