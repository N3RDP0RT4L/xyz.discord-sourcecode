package b.g.a.c.h0;

import b.d.b.a.a;
import b.g.a.b.d;
import b.g.a.b.h;
import b.g.a.b.s.b;
import b.g.a.c.e0.g;
import b.g.a.c.j;
import b.g.a.c.m;
import b.g.a.c.x;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
/* compiled from: TypeBase.java */
/* loaded from: classes2.dex */
public abstract class l extends j implements m {
    public static final m j = m.l;
    private static final long serialVersionUID = 1;
    public final m _bindings;
    public final j _superClass;
    public final j[] _superInterfaces;

    public l(Class<?> cls, m mVar, j jVar, j[] jVarArr, int i, Object obj, Object obj2, boolean z2) {
        super(cls, i, obj, obj2, z2);
        this._bindings = mVar == null ? j : mVar;
        this._superClass = jVar;
        this._superInterfaces = jVarArr;
    }

    public static StringBuilder J(Class<?> cls, StringBuilder sb, boolean z2) {
        if (!cls.isPrimitive()) {
            sb.append('L');
            String name = cls.getName();
            int length = name.length();
            for (int i = 0; i < length; i++) {
                char charAt = name.charAt(i);
                if (charAt == '.') {
                    charAt = MentionUtilsKt.SLASH_CHAR;
                }
                sb.append(charAt);
            }
            if (z2) {
                sb.append(';');
            }
        } else if (cls == Boolean.TYPE) {
            sb.append('Z');
        } else if (cls == Byte.TYPE) {
            sb.append('B');
        } else if (cls == Short.TYPE) {
            sb.append('S');
        } else if (cls == Character.TYPE) {
            sb.append('C');
        } else if (cls == Integer.TYPE) {
            sb.append('I');
        } else if (cls == Long.TYPE) {
            sb.append('J');
        } else if (cls == Float.TYPE) {
            sb.append('F');
        } else if (cls == Double.TYPE) {
            sb.append('D');
        } else if (cls == Void.TYPE) {
            sb.append('V');
        } else {
            StringBuilder R = a.R("Unrecognized primitive type: ");
            R.append(cls.getName());
            throw new IllegalStateException(R.toString());
        }
        return sb;
    }

    public String K() {
        return this._class.getName();
    }

    @Override // b.g.a.c.m
    public void c(d dVar, x xVar) throws IOException, JsonProcessingException {
        dVar.j0(K());
    }

    @Override // b.g.a.c.m
    public void d(d dVar, x xVar, g gVar) throws IOException {
        b bVar = new b(this, h.VALUE_STRING);
        gVar.e(dVar, bVar);
        dVar.j0(K());
        gVar.f(dVar, bVar);
    }

    @Override // b.g.a.b.s.a
    public String e() {
        return K();
    }

    @Override // b.g.a.c.j
    public j f(int i) {
        return this._bindings.f(i);
    }

    @Override // b.g.a.c.j
    public int g() {
        return this._bindings.j();
    }

    @Override // b.g.a.c.j
    public final j i(Class<?> cls) {
        j i;
        j[] jVarArr;
        if (cls == this._class) {
            return this;
        }
        if (cls.isInterface() && (jVarArr = this._superInterfaces) != null) {
            int length = jVarArr.length;
            for (int i2 = 0; i2 < length; i2++) {
                j i3 = this._superInterfaces[i2].i(cls);
                if (i3 != null) {
                    return i3;
                }
            }
        }
        j jVar = this._superClass;
        if (jVar == null || (i = jVar.i(cls)) == null) {
            return null;
        }
        return i;
    }

    @Override // b.g.a.c.j
    public m j() {
        return this._bindings;
    }

    @Override // b.g.a.c.j
    public List<j> n() {
        j[] jVarArr = this._superInterfaces;
        if (jVarArr == null) {
            return Collections.emptyList();
        }
        int length = jVarArr.length;
        if (length == 0) {
            return Collections.emptyList();
        }
        if (length != 1) {
            return Arrays.asList(jVarArr);
        }
        return Collections.singletonList(jVarArr[0]);
    }

    @Override // b.g.a.c.j
    public j q() {
        return this._superClass;
    }
}
