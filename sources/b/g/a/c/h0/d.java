package b.g.a.c.h0;

import b.d.b.a.a;
import b.g.a.c.j;
/* compiled from: CollectionLikeType.java */
/* loaded from: classes2.dex */
public class d extends l {
    private static final long serialVersionUID = 1;
    public final j _elementType;

    public d(Class<?> cls, m mVar, j jVar, j[] jVarArr, j jVar2, Object obj, Object obj2, boolean z2) {
        super(cls, mVar, jVar, jVarArr, jVar2._hash, obj, obj2, z2);
        this._elementType = jVar2;
    }

    @Override // b.g.a.c.j
    public j C(Class<?> cls, m mVar, j jVar, j[] jVarArr) {
        return new d(cls, mVar, jVar, jVarArr, this._elementType, this._valueHandler, this._typeHandler, this._asStatic);
    }

    @Override // b.g.a.c.j
    public j D(j jVar) {
        return this._elementType == jVar ? this : new d(this._class, this._bindings, this._superClass, this._superInterfaces, jVar, this._valueHandler, this._typeHandler, this._asStatic);
    }

    @Override // b.g.a.c.j
    public j F(j jVar) {
        j F;
        j F2 = super.F(jVar);
        j k = jVar.k();
        return (k == null || (F = this._elementType.F(k)) == this._elementType) ? F2 : F2.D(F);
    }

    @Override // b.g.a.c.h0.l
    public String K() {
        StringBuilder sb = new StringBuilder();
        sb.append(this._class.getName());
        if (this._elementType != null) {
            sb.append('<');
            sb.append(this._elementType.e());
            sb.append('>');
        }
        return sb.toString();
    }

    /* renamed from: L */
    public d E(Object obj) {
        return new d(this._class, this._bindings, this._superClass, this._superInterfaces, this._elementType.H(obj), this._valueHandler, this._typeHandler, this._asStatic);
    }

    /* renamed from: M */
    public d G() {
        return this._asStatic ? this : new d(this._class, this._bindings, this._superClass, this._superInterfaces, this._elementType.G(), this._valueHandler, this._typeHandler, true);
    }

    /* renamed from: N */
    public d H(Object obj) {
        return new d(this._class, this._bindings, this._superClass, this._superInterfaces, this._elementType, this._valueHandler, obj, this._asStatic);
    }

    /* renamed from: O */
    public d I(Object obj) {
        return new d(this._class, this._bindings, this._superClass, this._superInterfaces, this._elementType, obj, this._typeHandler, this._asStatic);
    }

    @Override // b.g.a.c.j
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        d dVar = (d) obj;
        return this._class == dVar._class && this._elementType.equals(dVar._elementType);
    }

    @Override // b.g.a.c.j
    public j k() {
        return this._elementType;
    }

    @Override // b.g.a.c.j
    public StringBuilder l(StringBuilder sb) {
        l.J(this._class, sb, true);
        return sb;
    }

    @Override // b.g.a.c.j
    public StringBuilder m(StringBuilder sb) {
        l.J(this._class, sb, false);
        sb.append('<');
        this._elementType.m(sb);
        sb.append(">;");
        return sb;
    }

    @Override // b.g.a.c.j
    public boolean s() {
        return super.s() || this._elementType.s();
    }

    public String toString() {
        StringBuilder R = a.R("[collection-like type; class ");
        a.i0(this._class, R, ", contains ");
        R.append(this._elementType);
        R.append("]");
        return R.toString();
    }

    @Override // b.g.a.c.j
    public boolean u() {
        return true;
    }

    @Override // b.g.a.c.j
    public boolean v() {
        return true;
    }
}
