package b.g.a.c.h0;

import b.d.b.a.a;
import java.util.ArrayList;
/* compiled from: ClassStack.java */
/* loaded from: classes2.dex */
public final class c {
    public final c a;

    /* renamed from: b  reason: collision with root package name */
    public final Class<?> f715b;
    public ArrayList<j> c;

    public c(c cVar, Class<?> cls) {
        this.a = cVar;
        this.f715b = cls;
    }

    public String toString() {
        StringBuilder R = a.R("[ClassStack (self-refs: ");
        ArrayList<j> arrayList = this.c;
        R.append(arrayList == null ? "0" : String.valueOf(arrayList.size()));
        R.append(')');
        for (c cVar = this; cVar != null; cVar = cVar.a) {
            R.append(' ');
            R.append(cVar.f715b.getName());
        }
        R.append(']');
        return R.toString();
    }

    public c(Class<?> cls) {
        this.a = null;
        this.f715b = cls;
    }
}
