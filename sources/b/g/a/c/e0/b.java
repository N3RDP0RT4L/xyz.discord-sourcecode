package b.g.a.c.e0;

import b.d.b.a.a;
import java.io.Serializable;
import java.util.Objects;
/* compiled from: NamedType.java */
/* loaded from: classes2.dex */
public final class b implements Serializable {
    private static final long serialVersionUID = 1;
    public final Class<?> _class;
    public final int _hashCode;
    public String _name;

    public b(Class<?> cls, String str) {
        this._class = cls;
        this._hashCode = cls.getName().hashCode() + (str == null ? 0 : str.hashCode());
        this._name = (str == null || str.isEmpty()) ? null : str;
    }

    public boolean a() {
        return this._name != null;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != b.class) {
            return false;
        }
        b bVar = (b) obj;
        return this._class == bVar._class && Objects.equals(this._name, bVar._name);
    }

    public int hashCode() {
        return this._hashCode;
    }

    public String toString() {
        StringBuilder R = a.R("[NamedType, class ");
        a.i0(this._class, R, ", name: ");
        return a.H(R, this._name == null ? "null" : a.H(a.R("'"), this._name, "'"), "]");
    }
}
