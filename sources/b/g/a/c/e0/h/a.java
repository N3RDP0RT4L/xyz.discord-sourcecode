package b.g.a.c.e0.h;

import b.g.a.a.c0;
import b.g.a.c.d;
import b.g.a.c.e0.e;
/* compiled from: AsArrayTypeSerializer.java */
/* loaded from: classes2.dex */
public class a extends m {
    public a(e eVar, d dVar) {
        super(eVar, dVar);
    }

    @Override // b.g.a.c.e0.g
    public c0.a c() {
        return c0.a.WRAPPER_ARRAY;
    }

    /* renamed from: g */
    public a a(d dVar) {
        return this.f695b == dVar ? this : new a(this.a, dVar);
    }
}
