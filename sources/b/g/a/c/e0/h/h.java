package b.g.a.c.e0.h;

import b.g.a.c.e0.c;
import b.g.a.c.h0.n;
import b.g.a.c.j;
/* compiled from: MinimalClassNameIdResolver.java */
/* loaded from: classes2.dex */
public class h extends f {
    public final String c;

    public h(j jVar, n nVar, c cVar) {
        super(jVar, nVar, cVar);
        String name = jVar._class.getName();
        int lastIndexOf = name.lastIndexOf(46);
        if (lastIndexOf < 0) {
            this.c = ".";
            return;
        }
        this.c = name.substring(0, lastIndexOf + 1);
        name.substring(0, lastIndexOf);
    }

    @Override // b.g.a.c.e0.h.f, b.g.a.c.e0.e
    public String a(Object obj) {
        String name = obj.getClass().getName();
        return name.startsWith(this.c) ? name.substring(this.c.length() - 1) : name;
    }
}
