package b.g.a.c.e0.h;

import b.g.a.a.c0;
import b.g.a.c.d;
import b.g.a.c.e0.e;
/* compiled from: AsExistingPropertyTypeSerializer.java */
/* loaded from: classes2.dex */
public class b extends d {
    public b(e eVar, d dVar, String str) {
        super(eVar, dVar, str);
    }

    @Override // b.g.a.c.e0.h.d, b.g.a.c.e0.h.a, b.g.a.c.e0.g
    public c0.a c() {
        return c0.a.EXISTING_PROPERTY;
    }

    /* renamed from: i */
    public b h(d dVar) {
        return this.f695b == dVar ? this : new b(this.a, dVar, this.c);
    }
}
