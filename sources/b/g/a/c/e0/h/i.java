package b.g.a.c.e0.h;

import b.g.a.c.c0.c;
import b.g.a.c.e0.b;
import b.g.a.c.e0.d;
import b.g.a.c.j;
import b.g.a.c.z.l;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
/* compiled from: StdSubtypeResolver.java */
/* loaded from: classes2.dex */
public class i extends d implements Serializable {
    private static final long serialVersionUID = 1;
    public LinkedHashSet<b> _registeredSubtypes;

    @Override // b.g.a.c.e0.d
    public Collection<b> a(l<?> lVar, b.g.a.c.c0.i iVar, j jVar) {
        Class<?> cls;
        List<b> N;
        b.g.a.c.b e = lVar.e();
        if (jVar == null) {
            cls = iVar.d();
        } else {
            cls = jVar._class;
        }
        HashMap<b, b> hashMap = new HashMap<>();
        LinkedHashSet<b> linkedHashSet = this._registeredSubtypes;
        if (linkedHashSet != null) {
            Iterator<b> it = linkedHashSet.iterator();
            while (it.hasNext()) {
                b next = it.next();
                if (cls.isAssignableFrom(next._class)) {
                    b(b.g.a.c.c0.d.h(lVar, next._class), next, lVar, e, hashMap);
                }
            }
        }
        if (!(iVar == null || (N = e.N(iVar)) == null)) {
            for (b bVar : N) {
                b(b.g.a.c.c0.d.h(lVar, bVar._class), bVar, lVar, e, hashMap);
            }
        }
        b(b.g.a.c.c0.d.h(lVar, cls), new b(cls, null), lVar, e, hashMap);
        return new ArrayList(hashMap.values());
    }

    public void b(c cVar, b bVar, l<?> lVar, b.g.a.c.b bVar2, HashMap<b, b> hashMap) {
        String O;
        if (!bVar.a() && (O = bVar2.O(cVar)) != null) {
            bVar = new b(bVar._class, O);
        }
        b bVar3 = new b(bVar._class, null);
        if (!hashMap.containsKey(bVar3)) {
            hashMap.put(bVar3, bVar);
            List<b> N = bVar2.N(cVar);
            if (!(N == null || N.isEmpty())) {
                for (b bVar4 : N) {
                    b(b.g.a.c.c0.d.h(lVar, bVar4._class), bVar4, lVar, bVar2, hashMap);
                }
            }
        } else if (bVar.a() && !hashMap.get(bVar3).a()) {
            hashMap.put(bVar3, bVar);
        }
    }
}
