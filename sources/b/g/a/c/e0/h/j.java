package b.g.a.c.e0.h;

import b.g.a.a.c0;
import b.g.a.c.e0.a;
import b.g.a.c.e0.b;
import b.g.a.c.e0.c;
import b.g.a.c.e0.e;
import b.g.a.c.e0.f;
import b.g.a.c.e0.g;
import b.g.a.c.p;
import b.g.a.c.v;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
/* compiled from: StdTypeResolverBuilder.java */
/* loaded from: classes2.dex */
public class j implements f<j> {
    public e _customIdResolver;
    public c0.b _idType;
    public c0.a _includeAs;
    public String _typeProperty;

    @Override // b.g.a.c.e0.f
    public j a(boolean z2) {
        return this;
    }

    @Override // b.g.a.c.e0.f
    public /* bridge */ /* synthetic */ j b(c0.b bVar, e eVar) {
        g(bVar, eVar);
        return this;
    }

    @Override // b.g.a.c.e0.f
    public j c(String str) {
        if (str == null || str.isEmpty()) {
            str = this._idType.f();
        }
        this._typeProperty = str;
        return this;
    }

    @Override // b.g.a.c.e0.f
    public j d(Class cls) {
        return this;
    }

    @Override // b.g.a.c.e0.f
    public g e(v vVar, b.g.a.c.j jVar, Collection<b> collection) {
        String str;
        if (this._idType == c0.b.NONE || jVar._class.isPrimitive()) {
            return null;
        }
        c cVar = vVar._base._typeValidator;
        if (cVar == g.j && vVar.q(p.BLOCK_UNSAFE_POLYMORPHIC_BASE_TYPES)) {
            cVar = new a();
        }
        e eVar = this._customIdResolver;
        if (eVar == null) {
            c0.b bVar = this._idType;
            if (bVar != null) {
                int ordinal = bVar.ordinal();
                if (ordinal != 0) {
                    if (ordinal != 1) {
                        if (ordinal == 2) {
                            eVar = new h(jVar, vVar._base._typeFactory, cVar);
                        } else if (ordinal == 3) {
                            ConcurrentHashMap concurrentHashMap = new ConcurrentHashMap();
                            vVar.q(p.ACCEPT_CASE_INSENSITIVE_VALUES);
                            if (collection != null) {
                                for (b bVar2 : collection) {
                                    Class<?> cls = bVar2._class;
                                    if (bVar2.a()) {
                                        str = bVar2._name;
                                    } else {
                                        str = l.d(cls);
                                    }
                                    concurrentHashMap.put(cls.getName(), str);
                                }
                            }
                            eVar = new l(vVar, jVar, concurrentHashMap, null);
                        } else if (ordinal != 4) {
                            StringBuilder R = b.d.b.a.a.R("Do not know how to construct standard type id resolver for idType: ");
                            R.append(this._idType);
                            throw new IllegalStateException(R.toString());
                        }
                    }
                    eVar = new f(jVar, vVar._base._typeFactory, cVar);
                } else {
                    eVar = null;
                }
            } else {
                throw new IllegalStateException("Cannot build, 'init()' not yet called");
            }
        }
        if (this._idType == c0.b.DEDUCTION) {
            return new b(eVar, null, this._typeProperty);
        }
        int ordinal2 = this._includeAs.ordinal();
        if (ordinal2 == 0) {
            return new d(eVar, null, this._typeProperty);
        }
        if (ordinal2 == 1) {
            return new e(eVar, null);
        }
        if (ordinal2 == 2) {
            return new a(eVar, null);
        }
        if (ordinal2 == 3) {
            return new c(eVar, null, this._typeProperty);
        }
        if (ordinal2 == 4) {
            return new b(eVar, null, this._typeProperty);
        }
        StringBuilder R2 = b.d.b.a.a.R("Do not know how to construct standard type serializer for inclusion type: ");
        R2.append(this._includeAs);
        throw new IllegalStateException(R2.toString());
    }

    @Override // b.g.a.c.e0.f
    public j f(c0.a aVar) {
        if (aVar != null) {
            this._includeAs = aVar;
            return this;
        }
        throw new IllegalArgumentException("includeAs cannot be null");
    }

    public j g(c0.b bVar, e eVar) {
        if (bVar != null) {
            this._idType = bVar;
            this._customIdResolver = eVar;
            this._typeProperty = bVar.f();
            return this;
        }
        throw new IllegalArgumentException("idType cannot be null");
    }
}
