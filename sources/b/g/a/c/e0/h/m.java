package b.g.a.c.e0.h;

import b.g.a.b.h;
import b.g.a.b.s.b;
import b.g.a.c.d;
import b.g.a.c.e0.e;
import b.g.a.c.e0.g;
import java.io.IOException;
import java.util.Objects;
/* compiled from: TypeSerializerBase.java */
/* loaded from: classes2.dex */
public abstract class m extends g {
    public final e a;

    /* renamed from: b  reason: collision with root package name */
    public final d f695b;

    public m(e eVar, d dVar) {
        this.a = eVar;
        this.f695b = dVar;
    }

    @Override // b.g.a.c.e0.g
    public String b() {
        return null;
    }

    @Override // b.g.a.c.e0.g
    public b e(b.g.a.b.d dVar, b bVar) throws IOException {
        String str;
        if (bVar.c == null) {
            Object obj = bVar.a;
            Class<?> cls = bVar.f671b;
            if (cls == null) {
                str = this.a.a(obj);
            } else {
                str = this.a.c(obj, cls);
            }
            bVar.c = str;
        }
        Objects.requireNonNull(dVar);
        Object obj2 = bVar.c;
        h hVar = bVar.f;
        String valueOf = obj2 instanceof String ? (String) obj2 : String.valueOf(obj2);
        bVar.g = true;
        int i = bVar.e;
        h hVar2 = h.START_OBJECT;
        if (hVar != hVar2) {
            b.c.a.y.b.j(i);
            if (i == 3 || i == 4) {
                bVar.e = 1;
                i = 1;
            }
        }
        int h = b.c.a.y.b.h(i);
        if (h == 1) {
            dVar.c0();
            dVar.y(valueOf);
        } else if (h == 2) {
            dVar.d0(bVar.a);
            dVar.y(bVar.d);
            dVar.j0(valueOf);
            return bVar;
        } else if (!(h == 3 || h == 4)) {
            dVar.W();
            dVar.j0(valueOf);
        }
        if (hVar == hVar2) {
            dVar.d0(bVar.a);
        } else if (hVar == h.START_ARRAY) {
            dVar.W();
        }
        return bVar;
    }

    @Override // b.g.a.c.e0.g
    public b f(b.g.a.b.d dVar, b bVar) throws IOException {
        Objects.requireNonNull(dVar);
        h hVar = bVar.f;
        if (hVar == h.START_OBJECT) {
            dVar.u();
        } else if (hVar == h.START_ARRAY) {
            dVar.t();
        }
        if (bVar.g) {
            int h = b.c.a.y.b.h(bVar.e);
            if (h == 0) {
                dVar.t();
            } else if (!(h == 2 || h == 3)) {
                if (h != 4) {
                    dVar.u();
                } else {
                    Object obj = bVar.c;
                    String valueOf = obj instanceof String ? (String) obj : String.valueOf(obj);
                    dVar.y(bVar.d);
                    dVar.j0(valueOf);
                }
            }
        }
        return bVar;
    }
}
