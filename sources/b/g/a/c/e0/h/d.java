package b.g.a.c.e0.h;

import b.g.a.a.c0;
import b.g.a.c.e0.e;
/* compiled from: AsPropertyTypeSerializer.java */
/* loaded from: classes2.dex */
public class d extends a {
    public final String c;

    public d(e eVar, b.g.a.c.d dVar, String str) {
        super(eVar, dVar);
        this.c = str;
    }

    @Override // b.g.a.c.e0.h.m, b.g.a.c.e0.g
    public String b() {
        return this.c;
    }

    @Override // b.g.a.c.e0.h.a, b.g.a.c.e0.g
    public c0.a c() {
        return c0.a.PROPERTY;
    }

    /* renamed from: h */
    public d g(b.g.a.c.d dVar) {
        return this.f695b == dVar ? this : new d(this.a, dVar, this.c);
    }
}
