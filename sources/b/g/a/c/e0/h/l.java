package b.g.a.c.e0.h;

import b.g.a.c.c0.q;
import b.g.a.c.h0.n;
import b.g.a.c.j;
import b.g.a.c.p;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
/* compiled from: TypeNameIdResolver.java */
/* loaded from: classes2.dex */
public class l extends k {
    public final b.g.a.c.z.l<?> c;
    public final ConcurrentHashMap<String, String> d;
    public final Map<String, j> e;

    public l(b.g.a.c.z.l<?> lVar, j jVar, ConcurrentHashMap<String, String> concurrentHashMap, HashMap<String, j> hashMap) {
        super(jVar, lVar._base._typeFactory);
        this.c = lVar;
        this.d = concurrentHashMap;
        this.e = hashMap;
        lVar.q(p.ACCEPT_CASE_INSENSITIVE_VALUES);
    }

    public static String d(Class<?> cls) {
        String name = cls.getName();
        int lastIndexOf = name.lastIndexOf(46);
        return lastIndexOf < 0 ? name : name.substring(lastIndexOf + 1);
    }

    @Override // b.g.a.c.e0.e
    public String a(Object obj) {
        return e(obj.getClass());
    }

    @Override // b.g.a.c.e0.e
    public String c(Object obj, Class<?> cls) {
        if (obj == null) {
            return e(cls);
        }
        return e(obj.getClass());
    }

    public String e(Class<?> cls) {
        if (cls == null) {
            return null;
        }
        String name = cls.getName();
        String str = this.d.get(name);
        if (str == null) {
            Class<?> cls2 = this.a.b(null, cls, n.l)._class;
            if (this.c.p()) {
                str = this.c.e().O(((q) this.c.o(cls2)).f);
            }
            if (str == null) {
                str = d(cls2);
            }
            this.d.put(name, str);
        }
        return str;
    }

    public String toString() {
        return String.format("[%s; id-to-type=%s]", l.class.getName(), this.e);
    }
}
