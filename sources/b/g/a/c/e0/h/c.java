package b.g.a.c.e0.h;

import b.g.a.a.c0;
import b.g.a.c.d;
import b.g.a.c.e0.e;
import b.g.a.c.e0.g;
/* compiled from: AsExternalTypeSerializer.java */
/* loaded from: classes2.dex */
public class c extends m {
    public final String c;

    public c(e eVar, d dVar, String str) {
        super(eVar, dVar);
        this.c = str;
    }

    @Override // b.g.a.c.e0.g
    public g a(d dVar) {
        return this.f695b == dVar ? this : new c(this.a, dVar, this.c);
    }

    @Override // b.g.a.c.e0.h.m, b.g.a.c.e0.g
    public String b() {
        return this.c;
    }

    @Override // b.g.a.c.e0.g
    public c0.a c() {
        return c0.a.EXTERNAL_PROPERTY;
    }
}
