package b.g.a.c.e0.h;

import b.g.a.a.c0;
import b.g.a.c.d;
import b.g.a.c.e0.g;
/* compiled from: AsWrapperTypeSerializer.java */
/* loaded from: classes2.dex */
public class e extends m {
    public e(b.g.a.c.e0.e eVar, d dVar) {
        super(eVar, dVar);
    }

    @Override // b.g.a.c.e0.g
    public g a(d dVar) {
        return this.f695b == dVar ? this : new e(this.a, dVar);
    }

    @Override // b.g.a.c.e0.g
    public c0.a c() {
        return c0.a.WRAPPER_OBJECT;
    }
}
