package b.g.a.c.e0.h;

import b.d.b.a.a;
import b.g.a.c.e0.c;
import b.g.a.c.h0.e;
import b.g.a.c.h0.g;
import b.g.a.c.h0.m;
import b.g.a.c.h0.n;
import b.g.a.c.i0.d;
import b.g.a.c.j;
import java.lang.reflect.Field;
import java.lang.reflect.TypeVariable;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
/* compiled from: ClassNameIdResolver.java */
/* loaded from: classes2.dex */
public class f extends k {
    public f(j jVar, n nVar, c cVar) {
        super(jVar, nVar);
    }

    @Override // b.g.a.c.e0.e
    public String a(Object obj) {
        return d(obj, obj.getClass(), this.a);
    }

    @Override // b.g.a.c.e0.e
    public String c(Object obj, Class<?> cls) {
        return d(obj, cls, this.a);
    }

    public String d(Object obj, Class<?> cls, n nVar) {
        Class<?> cls2;
        j jVar;
        j jVar2;
        m mVar;
        Class<?> cls3;
        m mVar2;
        Class<? super Object> superclass = (!d.q(cls) || cls.isEnum()) ? cls : cls.getSuperclass();
        String name = superclass.getName();
        if (!name.startsWith("java.util.")) {
            return (name.indexOf(36) < 0 || d.m(superclass) == null || d.m(this.f694b._class) != null) ? name : this.f694b._class.getName();
        }
        if (obj instanceof EnumSet) {
            EnumSet enumSet = (EnumSet) obj;
            if (!enumSet.isEmpty()) {
                cls3 = ((Enum) enumSet.iterator().next()).getDeclaringClass();
            } else {
                Field field = d.b.a.f724b;
                if (field != null) {
                    try {
                        cls3 = (Class) field.get(enumSet);
                    } catch (Exception e) {
                        throw new IllegalArgumentException(e);
                    }
                } else {
                    throw new IllegalStateException("Cannot figure out type for EnumSet (odd JDK platform?)");
                }
            }
            j c = nVar.c(null, cls3, n.l);
            String[] strArr = m.j;
            TypeVariable[] typeParameters = EnumSet.class.getTypeParameters();
            int length = typeParameters == null ? 0 : typeParameters.length;
            if (length == 0) {
                mVar2 = m.l;
            } else if (length == 1) {
                mVar2 = new m(new String[]{typeParameters[0].getName()}, new j[]{c}, null);
            } else {
                StringBuilder R = a.R("Cannot create TypeBindings for class ");
                R.append(EnumSet.class.getName());
                R.append(" with 1 type parameter: class expects ");
                R.append(length);
                throw new IllegalArgumentException(R.toString());
            }
            e eVar = (e) nVar.c(null, EnumSet.class, mVar2);
            if (mVar2.i()) {
                j k = eVar.i(Collection.class).k();
                if (!k.equals(c)) {
                    throw new IllegalArgumentException(String.format("Non-generic Collection class %s did not resolve to something with element type %s but %s ", d.u(EnumSet.class), c, k));
                }
            }
            return eVar.K();
        } else if (!(obj instanceof EnumMap)) {
            return name;
        } else {
            EnumMap enumMap = (EnumMap) obj;
            if (!enumMap.isEmpty()) {
                cls2 = ((Enum) enumMap.keySet().iterator().next()).getDeclaringClass();
            } else {
                Field field2 = d.b.a.c;
                if (field2 != null) {
                    try {
                        cls2 = (Class) field2.get(enumMap);
                    } catch (Exception e2) {
                        throw new IllegalArgumentException(e2);
                    }
                } else {
                    throw new IllegalStateException("Cannot figure out type for EnumMap (odd JDK platform?)");
                }
            }
            Objects.requireNonNull(nVar);
            if (EnumMap.class == Properties.class) {
                jVar2 = n.f720y;
                jVar = jVar2;
            } else {
                m mVar3 = n.l;
                jVar2 = nVar.c(null, cls2, mVar3);
                jVar = nVar.c(null, Object.class, mVar3);
            }
            j[] jVarArr = {jVar2, jVar};
            String[] strArr2 = m.j;
            TypeVariable[] typeParameters2 = EnumMap.class.getTypeParameters();
            if (typeParameters2 == null || typeParameters2.length == 0) {
                mVar = m.l;
            } else {
                int length2 = typeParameters2.length;
                String[] strArr3 = new String[length2];
                for (int i = 0; i < length2; i++) {
                    strArr3[i] = typeParameters2[i].getName();
                }
                if (length2 == 2) {
                    mVar = new m(strArr3, jVarArr, null);
                } else {
                    StringBuilder R2 = a.R("Cannot create TypeBindings for class ");
                    R2.append(EnumMap.class.getName());
                    R2.append(" with ");
                    R2.append(2);
                    R2.append(" type parameter");
                    R2.append("s");
                    R2.append(": class expects ");
                    R2.append(length2);
                    throw new IllegalArgumentException(R2.toString());
                }
            }
            g gVar = (g) nVar.c(null, EnumMap.class, mVar);
            if (mVar.i()) {
                j i2 = gVar.i(Map.class);
                j o = i2.o();
                if (o.equals(jVar2)) {
                    j k2 = i2.k();
                    if (!k2.equals(jVar)) {
                        throw new IllegalArgumentException(String.format("Non-generic Map class %s did not resolve to something with value type %s but %s ", d.u(EnumMap.class), jVar, k2));
                    }
                } else {
                    throw new IllegalArgumentException(String.format("Non-generic Map class %s did not resolve to something with key type %s but %s ", d.u(EnumMap.class), jVar2, o));
                }
            }
            return gVar.K();
        }
    }
}
