package b.g.a.c.e0;

import b.g.a.a.c0;
import b.g.a.b.h;
import b.g.a.b.s.b;
import b.g.a.b.t.m;
import b.g.a.c.d;
import java.io.IOException;
/* compiled from: TypeSerializer.java */
/* loaded from: classes2.dex */
public abstract class g {
    public abstract g a(d dVar);

    public abstract String b();

    public abstract c0.a c();

    public b d(Object obj, h hVar) {
        b bVar = new b(obj, hVar);
        int ordinal = c().ordinal();
        if (ordinal == 0) {
            bVar.e = 3;
            bVar.d = b();
        } else if (ordinal == 1) {
            bVar.e = 2;
        } else if (ordinal == 2) {
            bVar.e = 1;
        } else if (ordinal == 3) {
            bVar.e = 5;
            bVar.d = b();
        } else if (ordinal == 4) {
            bVar.e = 4;
            bVar.d = b();
        } else {
            int i = m.a;
            throw new RuntimeException("Internal error: this code path should never get executed");
        }
        return bVar;
    }

    public abstract b e(b.g.a.b.d dVar, b bVar) throws IOException;

    public abstract b f(b.g.a.b.d dVar, b bVar) throws IOException;
}
