package b.g.a.c.e0;

import b.g.a.a.c0;
import b.g.a.c.e0.f;
import b.g.a.c.j;
import b.g.a.c.v;
import java.util.Collection;
/* compiled from: TypeResolverBuilder.java */
/* loaded from: classes2.dex */
public interface f<T extends f<T>> {
    T a(boolean z2);

    T b(c0.b bVar, e eVar);

    T c(String str);

    T d(Class<?> cls);

    g e(v vVar, j jVar, Collection<b> collection);

    T f(c0.a aVar);
}
