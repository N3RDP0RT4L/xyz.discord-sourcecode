package b.g.a.c;

import b.g.a.b.j;
import b.g.a.b.t.d;
import b.g.a.c.c0.b0;
import b.g.a.c.c0.c;
import b.g.a.c.c0.d0;
import b.g.a.c.c0.q;
import b.g.a.c.c0.r;
import b.g.a.c.c0.w;
import b.g.a.c.g0.k;
import b.g.a.c.i0.p;
import b.g.a.c.z.a;
import b.g.a.c.z.g;
import b.g.a.c.z.l;
import b.g.a.c.z.m;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
/* compiled from: SerializationConfig.java */
/* loaded from: classes2.dex */
public final class v extends m<w, v> implements Serializable {
    public static final j l = new d();
    public static final int m = l.c(w.class);
    private static final long serialVersionUID = 1;
    public final j _defaultPrettyPrinter;
    public final k _filterProvider;
    public final int _formatWriteFeatures;
    public final int _formatWriteFeaturesToChange;
    public final int _generatorFeatures;
    public final int _generatorFeaturesToChange;
    public final int _serFeatures;

    public v(a aVar, b.g.a.c.e0.d dVar, d0 d0Var, p pVar, g gVar) {
        super(aVar, dVar, d0Var, pVar, gVar);
        this._serFeatures = m;
        this._defaultPrettyPrinter = l;
        this._generatorFeatures = 0;
        this._generatorFeaturesToChange = 0;
        this._formatWriteFeatures = 0;
        this._formatWriteFeaturesToChange = 0;
    }

    @Override // b.g.a.c.z.m
    public v r(int i) {
        return new v(this, i, this._serFeatures, this._generatorFeatures, this._generatorFeaturesToChange, this._formatWriteFeatures, this._formatWriteFeaturesToChange);
    }

    public c u(j jVar) {
        w.c cVar;
        r rVar = (r) this._base._classIntrospector;
        q a = rVar.a(this, jVar);
        if (a != null) {
            return a;
        }
        boolean z2 = false;
        if (jVar.v() && !(jVar instanceof b.g.a.c.h0.a)) {
            Class<?> cls = jVar._class;
            if (b.g.a.c.i0.d.r(cls) && (Collection.class.isAssignableFrom(cls) || Map.class.isAssignableFrom(cls))) {
                z2 = true;
            }
        }
        q e = z2 ? q.e(this, jVar, rVar.b(this, jVar, this)) : null;
        if (e != null) {
            return e;
        }
        c b2 = rVar.b(this, jVar, this);
        if (jVar.A()) {
            Objects.requireNonNull((w.b) this._base._accessorNaming);
            cVar = new w.c(this, b2);
        } else {
            w.b bVar = (w.b) this._base._accessorNaming;
            cVar = new w(this, b2, bVar._setterPrefix, bVar._getterPrefix, bVar._isGetterPrefix, bVar._baseNameValidator);
        }
        return new q(new b0(this, true, jVar, b2, cVar));
    }

    public final boolean v(w wVar) {
        return (wVar.g() & this._serFeatures) != 0;
    }

    public v(v vVar, int i, int i2, int i3, int i4, int i5, int i6) {
        super(vVar, i);
        this._serFeatures = i2;
        this._defaultPrettyPrinter = vVar._defaultPrettyPrinter;
        this._generatorFeatures = i3;
        this._generatorFeaturesToChange = i4;
        this._formatWriteFeatures = i5;
        this._formatWriteFeaturesToChange = i6;
    }
}
