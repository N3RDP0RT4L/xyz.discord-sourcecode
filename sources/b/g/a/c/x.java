package b.g.a.c;

import b.g.a.a.i0;
import b.g.a.c.c0.b;
import b.g.a.c.c0.s;
import b.g.a.c.g0.j;
import b.g.a.c.g0.o;
import b.g.a.c.g0.p;
import b.g.a.c.g0.t.c;
import b.g.a.c.g0.t.m;
import b.g.a.c.g0.t.q;
import b.g.a.c.g0.u.u;
import b.g.a.c.h0.n;
import b.g.a.c.i0.d;
import b.g.a.c.z.i;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidDefinitionException;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Map;
/* compiled from: SerializerProvider.java */
/* loaded from: classes2.dex */
public abstract class x extends e {
    public static final n<Object> j = new c("Null key for a Map not allowed in JSON (use a converting NullKeySerializer?)");
    public static final n<Object> k = new q();
    public final v _config;
    public DateFormat _dateFormat;
    public n<Object> _keySerializer;
    public final m _knownSerializers;
    public n<Object> _nullKeySerializer;
    public n<Object> _nullValueSerializer;
    public final Class<?> _serializationView;
    public final p _serializerCache;
    public final b.g.a.c.g0.q _serializerFactory;
    public final boolean _stdNullValueSerializer;
    public n<Object> _unknownTypeSerializer;
    public transient i l;

    public x() {
        this._unknownTypeSerializer = k;
        this._nullValueSerializer = u.k;
        this._nullKeySerializer = j;
        this._config = null;
        this._serializerFactory = null;
        this._serializerCache = new p();
        this._knownSerializers = null;
        this._serializationView = null;
        this.l = null;
        this._stdNullValueSerializer = true;
    }

    public abstract Object A(s sVar, Class<?> cls) throws JsonMappingException;

    public abstract boolean B(Object obj) throws JsonMappingException;

    public final boolean C(p pVar) {
        return pVar.h(this._config._mapperFeatures);
    }

    public final boolean D(w wVar) {
        return this._config.v(wVar);
    }

    public <T> T E(c cVar, s sVar, String str, Object... objArr) throws JsonMappingException {
        String str2;
        String a = a(str, objArr);
        String str3 = "N/A";
        if (sVar != null) {
            String p = sVar.p();
            if (p == null) {
                str2 = "[N/A]";
            } else {
                Object[] objArr2 = new Object[1];
                if (p.length() > 500) {
                    p = p.substring(0, 500) + "]...[" + p.substring(p.length() - 500);
                }
                objArr2[0] = p;
                str2 = String.format("\"%s\"", objArr2);
            }
        } else {
            str2 = str3;
        }
        if (cVar != null) {
            str3 = d.u(cVar.a._class);
        }
        throw new InvalidDefinitionException(((j) this).o, String.format("Invalid definition for property %s (of type %s): %s", str2, str3, a), cVar, sVar);
    }

    public <T> T F(c cVar, String str, Object... objArr) throws JsonMappingException {
        throw new InvalidDefinitionException(((j) this).o, String.format("Invalid type definition for type %s: %s", d.u(cVar.a._class), a(str, objArr)), cVar, null);
    }

    public void G(String str, Object... objArr) throws JsonMappingException {
        throw new JsonMappingException(((j) this).o, a(str, objArr), null);
    }

    public abstract n<Object> H(b bVar, Object obj) throws JsonMappingException;

    @Override // b.g.a.c.e
    public final n d() {
        return this._config._base._typeFactory;
    }

    @Override // b.g.a.c.e
    public <T> T f(j jVar, String str) throws JsonMappingException {
        throw new InvalidDefinitionException(((j) this).o, str, jVar);
    }

    public n<Object> h(j jVar) throws JsonMappingException {
        try {
            n<Object> b2 = this._serializerFactory.b(this, jVar);
            if (b2 != null) {
                p pVar = this._serializerCache;
                synchronized (pVar) {
                    if (pVar.a.put(new b.g.a.c.i0.u(jVar, false), b2) == null) {
                        pVar.f699b.set(null);
                    }
                    if (b2 instanceof o) {
                        ((o) b2).b(this);
                    }
                }
            }
            return b2;
        } catch (IllegalArgumentException e) {
            throw new JsonMappingException(((j) this).o, a(d.h(e), new Object[0]), e);
        }
    }

    public n<Object> i(Class<?> cls) throws JsonMappingException {
        j b2 = this._config._base._typeFactory.b(null, cls, n.l);
        try {
            n<Object> b3 = this._serializerFactory.b(this, b2);
            if (b3 != null) {
                p pVar = this._serializerCache;
                synchronized (pVar) {
                    n<Object> put = pVar.a.put(new b.g.a.c.i0.u(cls, false), b3);
                    n<Object> put2 = pVar.a.put(new b.g.a.c.i0.u(b2, false), b3);
                    if (put == null || put2 == null) {
                        pVar.f699b.set(null);
                    }
                    if (b3 instanceof o) {
                        ((o) b3).b(this);
                    }
                }
            }
            return b3;
        } catch (IllegalArgumentException e) {
            throw new JsonMappingException(((j) this).o, a(d.h(e), new Object[0]), e);
        }
    }

    public final DateFormat j() {
        DateFormat dateFormat = this._dateFormat;
        if (dateFormat != null) {
            return dateFormat;
        }
        DateFormat dateFormat2 = (DateFormat) this._config._base._dateFormat.clone();
        this._dateFormat = dateFormat2;
        return dateFormat2;
    }

    public j k(j jVar, Class<?> cls) throws IllegalArgumentException {
        return jVar._class == cls ? jVar : this._config._base._typeFactory.h(jVar, cls, true);
    }

    public final void l(b.g.a.b.d dVar) throws IOException {
        if (this._stdNullValueSerializer) {
            dVar.A();
        } else {
            this._nullValueSerializer.f(null, dVar, this);
        }
    }

    public n<Object> m(j jVar, d dVar) throws JsonMappingException {
        n<Object> a = this._knownSerializers.a(jVar);
        if (a == null && (a = this._serializerCache.a(jVar)) == null && (a = h(jVar)) == null) {
            return x(jVar._class);
        }
        return z(a, dVar);
    }

    public n<Object> n(Class<?> cls, d dVar) throws JsonMappingException {
        n<Object> b2 = this._knownSerializers.b(cls);
        if (b2 == null && (b2 = this._serializerCache.b(cls)) == null && (b2 = this._serializerCache.a(this._config._base._typeFactory.b(null, cls, n.l))) == null && (b2 = i(cls)) == null) {
            return x(cls);
        }
        return z(b2, dVar);
    }

    public n<Object> o(j jVar, d dVar) throws JsonMappingException {
        n<Object> a = this._serializerFactory.a(this, jVar, this._keySerializer);
        if (a instanceof o) {
            ((o) a).b(this);
        }
        return z(a, dVar);
    }

    public abstract b.g.a.c.g0.t.u p(Object obj, i0<?> i0Var);

    public n<Object> q(j jVar, d dVar) throws JsonMappingException {
        n<Object> a = this._knownSerializers.a(jVar);
        if (a == null && (a = this._serializerCache.a(jVar)) == null && (a = h(jVar)) == null) {
            return x(jVar._class);
        }
        return y(a, dVar);
    }

    public n<Object> r(Class<?> cls, d dVar) throws JsonMappingException {
        n<Object> b2 = this._knownSerializers.b(cls);
        if (b2 == null && (b2 = this._serializerCache.b(cls)) == null && (b2 = this._serializerCache.a(this._config._base._typeFactory.b(null, cls, n.l))) == null && (b2 = i(cls)) == null) {
            return x(cls);
        }
        return y(b2, dVar);
    }

    public n<Object> s(j jVar) throws JsonMappingException {
        n<Object> a = this._knownSerializers.a(jVar);
        if (a != null) {
            return a;
        }
        n<Object> a2 = this._serializerCache.a(jVar);
        if (a2 != null) {
            return a2;
        }
        n<Object> h = h(jVar);
        return h == null ? x(jVar._class) : h;
    }

    public n<Object> t(j jVar, d dVar) throws JsonMappingException {
        if (jVar != null) {
            n<Object> a = this._knownSerializers.a(jVar);
            if (a == null && (a = this._serializerCache.a(jVar)) == null && (a = h(jVar)) == null) {
                return x(jVar._class);
            }
            return z(a, dVar);
        }
        G("Null passed for `valueType` of `findValueSerializer()`", new Object[0]);
        throw null;
    }

    public n<Object> u(Class<?> cls, d dVar) throws JsonMappingException {
        n<Object> b2 = this._knownSerializers.b(cls);
        if (b2 == null && (b2 = this._serializerCache.b(cls)) == null && (b2 = this._serializerCache.a(this._config._base._typeFactory.b(null, cls, n.l))) == null && (b2 = i(cls)) == null) {
            return x(cls);
        }
        return z(b2, dVar);
    }

    public final b v() {
        return this._config.e();
    }

    public Object w(Object obj) {
        Object obj2;
        i.a aVar = (i.a) this.l;
        Map<Object, Object> map = aVar.l;
        if (map == null || (obj2 = map.get(obj)) == null) {
            return aVar._shared.get(obj);
        }
        if (obj2 == i.a.k) {
            return null;
        }
        return obj2;
    }

    public n<Object> x(Class<?> cls) {
        if (cls == Object.class) {
            return this._unknownTypeSerializer;
        }
        return new q(cls);
    }

    public n<?> y(n<?> nVar, d dVar) throws JsonMappingException {
        return (nVar == null || !(nVar instanceof b.g.a.c.g0.i)) ? nVar : ((b.g.a.c.g0.i) nVar).a(this, dVar);
    }

    public n<?> z(n<?> nVar, d dVar) throws JsonMappingException {
        return (nVar == null || !(nVar instanceof b.g.a.c.g0.i)) ? nVar : ((b.g.a.c.g0.i) nVar).a(this, dVar);
    }

    public x(x xVar, v vVar, b.g.a.c.g0.q qVar) {
        this._unknownTypeSerializer = k;
        this._nullValueSerializer = u.k;
        n<Object> nVar = j;
        this._nullKeySerializer = nVar;
        this._serializerFactory = qVar;
        this._config = vVar;
        p pVar = xVar._serializerCache;
        this._serializerCache = pVar;
        this._unknownTypeSerializer = xVar._unknownTypeSerializer;
        this._keySerializer = xVar._keySerializer;
        n<Object> nVar2 = xVar._nullValueSerializer;
        this._nullValueSerializer = nVar2;
        this._nullKeySerializer = xVar._nullKeySerializer;
        this._stdNullValueSerializer = nVar2 == nVar;
        this._serializationView = vVar._view;
        this.l = vVar._attributes;
        m mVar = pVar.f699b.get();
        if (mVar == null) {
            synchronized (pVar) {
                mVar = pVar.f699b.get();
                if (mVar == null) {
                    m mVar2 = new m(pVar.a);
                    pVar.f699b.set(mVar2);
                    mVar = mVar2;
                }
            }
        }
        this._knownSerializers = mVar;
    }
}
