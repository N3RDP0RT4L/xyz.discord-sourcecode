package b.g.a.c.z;

import b.g.a.c.g0.g;
import b.g.a.c.g0.r;
import b.g.a.c.i0.b;
import java.io.Serializable;
/* compiled from: SerializerFactoryConfig.java */
/* loaded from: classes2.dex */
public final class o implements Serializable {
    public static final r[] j = new r[0];
    public static final g[] k = new g[0];
    private static final long serialVersionUID = 1;
    public final r[] _additionalKeySerializers;
    public final r[] _additionalSerializers;
    public final g[] _modifiers = k;

    public o() {
        r[] rVarArr = j;
        this._additionalSerializers = rVarArr;
        this._additionalKeySerializers = rVarArr;
    }

    public boolean a() {
        return this._modifiers.length > 0;
    }

    public Iterable<g> b() {
        return new b(this._modifiers);
    }
}
