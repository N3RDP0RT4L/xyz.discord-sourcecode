package b.g.a.c.z;

import b.c.a.y.b;
import java.io.Serializable;
/* compiled from: CoercionConfig.java */
/* loaded from: classes2.dex */
public class c implements Serializable {
    public static final int j = 10;
    private static final long serialVersionUID = 1;
    public final b[] _coercionsByShape = new b[j];
    public Boolean _acceptBlankAsEmpty = Boolean.FALSE;

    static {
        b.com$fasterxml$jackson$databind$cfg$CoercionInputShape$s$values();
    }
}
