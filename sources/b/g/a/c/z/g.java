package b.g.a.c.z;

import b.g.a.a.p;
import b.g.a.a.z;
import b.g.a.c.c0.g0;
import java.io.Serializable;
import java.util.Map;
/* compiled from: ConfigOverrides.java */
/* loaded from: classes2.dex */
public class g implements Serializable {
    private static final long serialVersionUID = 1;
    public p.b _defaultInclusion;
    public z.a _defaultSetterInfo;
    public g0<?> _visibilityChecker;
    public Map<Class<?>, ?> _overrides = null;
    public Boolean _defaultMergeable = null;
    public Boolean _defaultLeniency = null;

    public g() {
        p.b bVar = p.b.j;
        p.b bVar2 = p.b.j;
        z.a aVar = z.a.j;
        g0.a aVar2 = g0.a.j;
        this._defaultInclusion = bVar2;
        this._defaultSetterInfo = aVar;
        this._visibilityChecker = aVar2;
    }

    public f a(Class<?> cls) {
        Map<Class<?>, ?> map = this._overrides;
        if (map == null) {
            return null;
        }
        return (f) map.get(cls);
    }
}
