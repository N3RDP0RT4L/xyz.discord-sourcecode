package b.g.a.c.z;

import b.g.a.a.i;
import b.g.a.a.p;
import b.g.a.a.z;
import b.g.a.c.b;
import b.g.a.c.c0.c;
import b.g.a.c.c0.g0;
import b.g.a.c.c0.q;
import b.g.a.c.c0.r;
import b.g.a.c.c0.t;
import b.g.a.c.c0.z;
import b.g.a.c.h0.n;
import b.g.a.c.j;
import b.g.a.c.z.l;
import java.io.Serializable;
import java.util.Objects;
/* compiled from: MapperConfig.java */
/* loaded from: classes2.dex */
public abstract class l<T extends l<T>> implements t.a, Serializable {
    private static final long serialVersionUID = 2;
    public final a _base;
    public final int _mapperFeatures;

    static {
        p.b bVar = p.b.j;
        p.b bVar2 = p.b.j;
        i.d dVar = i.d.j;
    }

    public l(a aVar, int i) {
        this._base = aVar;
        this._mapperFeatures = i;
    }

    public static <F extends Enum<F> & e> int c(Class<F> cls) {
        int i = 0;
        for (Enum r3 : (Enum[]) cls.getEnumConstants()) {
            e eVar = (e) r3;
            if (eVar.f()) {
                i |= eVar.g();
            }
        }
        return i;
    }

    public final boolean b() {
        return q(b.g.a.c.p.CAN_OVERRIDE_ACCESS_MODIFIERS);
    }

    public final j d(Class<?> cls) {
        return this._base._typeFactory.b(null, cls, n.l);
    }

    public b e() {
        if (q(b.g.a.c.p.USE_ANNOTATIONS)) {
            return this._base._annotationIntrospector;
        }
        return z.j;
    }

    public abstract f f(Class<?> cls);

    public abstract p.b g(Class<?> cls, Class<?> cls2);

    public abstract Boolean h();

    public abstract i.d i(Class<?> cls);

    public abstract p.b j(Class<?> cls);

    public p.b k(Class<?> cls, p.b bVar) {
        Objects.requireNonNull(f(cls));
        return bVar;
    }

    public abstract z.a l();

    public abstract g0<?> m(Class<?> cls, c cVar);

    public b.g.a.c.c n(j jVar) {
        r rVar = (r) this._base._classIntrospector;
        q a = rVar.a(this, jVar);
        return a == null ? q.e(this, jVar, rVar.b(this, jVar, this)) : a;
    }

    public b.g.a.c.c o(Class<?> cls) {
        return n(this._base._typeFactory.b(null, cls, n.l));
    }

    public final boolean p() {
        return q(b.g.a.c.p.USE_ANNOTATIONS);
    }

    public final boolean q(b.g.a.c.p pVar) {
        return pVar.h(this._mapperFeatures);
    }

    public l(l<T> lVar, int i) {
        this._base = lVar._base;
        this._mapperFeatures = i;
    }
}
