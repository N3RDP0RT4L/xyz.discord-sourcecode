package b.g.a.c.z;

import b.g.a.c.a;
import b.g.a.c.a0.c;
import b.g.a.c.a0.g;
import b.g.a.c.a0.h;
import java.io.Serializable;
/* compiled from: DeserializerFactoryConfig.java */
/* loaded from: classes2.dex */
public class j implements Serializable {
    public static final g[] j = new g[0];
    public static final c[] k = new c[0];
    public static final a[] l = new a[0];
    public static final b.g.a.c.a0.j[] m = new b.g.a.c.a0.j[0];
    public static final h[] n = {new b.g.a.c.a0.l.a()};
    private static final long serialVersionUID = 1;
    public final g[] _additionalDeserializers = j;
    public final h[] _additionalKeyDeserializers = n;
    public final c[] _modifiers = k;
    public final a[] _abstractTypeResolvers = l;
    public final b.g.a.c.a0.j[] _valueInstantiators = m;
}
