package b.g.a.c.a0;

import b.g.a.c.t;
import b.g.a.c.z.j;
import java.io.Serializable;
/* compiled from: BasicDeserializerFactory.java */
/* loaded from: classes2.dex */
public abstract class a extends f implements Serializable {
    public final j _factoryConfig;

    static {
        new t("@JsonUnwrapped");
    }

    public a(j jVar) {
        this._factoryConfig = jVar;
    }
}
