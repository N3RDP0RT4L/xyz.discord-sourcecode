package b.g.a.c.a0;

import b.g.a.c.i0.h;
import b.g.a.c.j;
import b.g.a.c.k;
import java.io.Serializable;
import java.util.HashMap;
/* compiled from: DeserializerCache.java */
/* loaded from: classes2.dex */
public final class e implements Serializable {
    private static final long serialVersionUID = 1;
    public final HashMap<j, k<Object>> _incompleteDeserializers = new HashMap<>(8);
    public final h<j, k<Object>> _cachedDeserializers = new h<>(Math.min(64, 500), 2000);

    public Object writeReplace() {
        this._incompleteDeserializers.clear();
        return this;
    }
}
