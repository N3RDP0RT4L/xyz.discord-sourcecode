package b.g.a.c.a0;

import b.g.a.c.g;
import java.io.Serializable;
import java.util.List;
/* compiled from: DefaultDeserializationContext.java */
/* loaded from: classes2.dex */
public abstract class d extends g implements Serializable {
    private static final long serialVersionUID = 1;
    private List<?> _objectIdResolvers;

    /* compiled from: DefaultDeserializationContext.java */
    /* loaded from: classes2.dex */
    public static final class a extends d {
        private static final long serialVersionUID = 1;

        public a(f fVar) {
            super(fVar, null);
        }
    }

    public d(f fVar, e eVar) {
        super(fVar, null);
    }
}
