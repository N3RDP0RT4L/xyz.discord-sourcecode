package b.g.a.c.b0;

import b.g.a.c.c0.b;
import b.g.a.c.c0.m;
import b.g.a.c.c0.n;
import b.g.a.c.t;
import java.beans.ConstructorProperties;
import java.beans.Transient;
/* compiled from: Java7SupportImpl.java */
/* loaded from: classes2.dex */
public class d extends c {
    @Override // b.g.a.c.b0.c
    public t a(m mVar) {
        ConstructorProperties b2;
        n nVar = mVar._owner;
        if (nVar == null || (b2 = nVar.b(ConstructorProperties.class)) == null) {
            return null;
        }
        String[] value = b2.value();
        int i = mVar._index;
        if (i < value.length) {
            return t.a(value[i]);
        }
        return null;
    }

    @Override // b.g.a.c.b0.c
    public Boolean b(b bVar) {
        Transient b2 = bVar.b(Transient.class);
        if (b2 != null) {
            return Boolean.valueOf(b2.value());
        }
        return null;
    }

    @Override // b.g.a.c.b0.c
    public Boolean c(b bVar) {
        if (bVar.b(ConstructorProperties.class) != null) {
            return Boolean.TRUE;
        }
        return null;
    }
}
