package b.g.a.c.b0;

import b.g.a.c.c0.b;
import b.g.a.c.c0.m;
import b.g.a.c.i0.d;
import b.g.a.c.t;
/* compiled from: Java7Support.java */
/* loaded from: classes2.dex */
public abstract class c {
    public static final c a;

    static {
        c cVar;
        try {
            cVar = (c) d.g(Class.forName("b.g.a.c.b0.d"), false);
        } catch (Throwable unused) {
            cVar = null;
        }
        a = cVar;
    }

    public abstract t a(m mVar);

    public abstract Boolean b(b bVar);

    public abstract Boolean c(b bVar);
}
