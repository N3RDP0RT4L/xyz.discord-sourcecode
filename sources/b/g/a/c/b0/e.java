package b.g.a.c.b0;

import b.g.a.b.d;
import b.g.a.b.h;
import b.g.a.b.s.b;
import b.g.a.c.e0.g;
import b.g.a.c.g0.u.p0;
import b.g.a.c.x;
import java.io.IOException;
import java.nio.file.Path;
/* compiled from: NioPathSerializer.java */
/* loaded from: classes2.dex */
public class e extends p0<Path> {
    private static final long serialVersionUID = 1;

    public e() {
        super(Path.class);
    }

    @Override // b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        dVar.j0(((Path) obj).toUri().toString());
    }

    @Override // b.g.a.c.g0.u.p0, b.g.a.c.n
    public void g(Object obj, d dVar, x xVar, g gVar) throws IOException {
        Path path = (Path) obj;
        b d = gVar.d(path, h.VALUE_STRING);
        d.f671b = Path.class;
        b e = gVar.e(dVar, d);
        dVar.j0(path.toUri().toString());
        gVar.f(dVar, e);
    }
}
