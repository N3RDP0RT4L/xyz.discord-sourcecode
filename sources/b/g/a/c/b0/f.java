package b.g.a.c.b0;

import b.d.b.a.a;
import b.g.a.c.c;
import b.g.a.c.g0.r;
import b.g.a.c.g0.u.k;
import b.g.a.c.i0.d;
import b.g.a.c.j;
import b.g.a.c.n;
import b.g.a.c.v;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
/* compiled from: OptionalHandlerFactory.java */
/* loaded from: classes2.dex */
public class f implements Serializable {
    public static final Class<?> j;
    public static final a k;
    public static final f l;
    private static final long serialVersionUID = 1;
    private final Map<String, String> _sqlDeserializers;
    private final Map<String, Object> _sqlSerializers;

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: Unreachable block: B:5:0x0006
        	at jadx.core.dex.visitors.blocks.BlockProcessor.checkForUnreachableBlocks(BlockProcessor.java:92)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:52)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:44)
        */
    static {
        /*
            r0 = 0
            java.lang.Class<org.w3c.dom.Node> r1 = org.w3c.dom.Node.class
            java.lang.Class<org.w3c.dom.Document> r2 = org.w3c.dom.Document.class
            goto L7
        L6:
            r1 = r0
        L7:
            b.g.a.c.b0.f.j = r1
            b.g.a.c.b0.a r0 = b.g.a.c.b0.a.a     // Catch: java.lang.Throwable -> Lb
        Lb:
            b.g.a.c.b0.f.k = r0
            b.g.a.c.b0.f r0 = new b.g.a.c.b0.f
            r0.<init>()
            b.g.a.c.b0.f.l = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.c.b0.f.<clinit>():void");
    }

    public f() {
        HashMap hashMap = new HashMap();
        this._sqlDeserializers = hashMap;
        hashMap.put("java.sql.Date", "com.fasterxml.jackson.databind.deser.std.DateDeserializers$SqlDateDeserializer");
        hashMap.put("java.sql.Timestamp", "com.fasterxml.jackson.databind.deser.std.DateDeserializers$TimestampDeserializer");
        HashMap hashMap2 = new HashMap();
        this._sqlSerializers = hashMap2;
        hashMap2.put("java.sql.Timestamp", k.k);
        hashMap2.put("java.sql.Date", "com.fasterxml.jackson.databind.ser.std.SqlDateSerializer");
        hashMap2.put("java.sql.Time", "com.fasterxml.jackson.databind.ser.std.SqlTimeSerializer");
        hashMap2.put("java.sql.Blob", "com.fasterxml.jackson.databind.ext.SqlBlobSerializer");
        hashMap2.put("javax.sql.rowset.serial.SerialBlob", "com.fasterxml.jackson.databind.ext.SqlBlobSerializer");
    }

    public n<?> a(v vVar, j jVar, c cVar) {
        n<?> a;
        Class<?> cls = jVar._class;
        Class<?> cls2 = j;
        boolean z2 = true;
        if (cls2 != null && cls2.isAssignableFrom(cls)) {
            return (n) c("com.fasterxml.jackson.databind.ext.DOMSerializer", jVar);
        }
        a aVar = k;
        if (aVar != null && (a = aVar.a(cls)) != null) {
            return a;
        }
        String name = cls.getName();
        Object obj = this._sqlSerializers.get(name);
        if (obj == null) {
            if (!name.startsWith("javax.xml.")) {
                do {
                    cls = cls.getSuperclass();
                    if (cls == null || cls == Object.class) {
                        z2 = false;
                        break;
                    }
                } while (!cls.getName().startsWith("javax.xml."));
                if (!z2) {
                    return null;
                }
            }
            Object c = c("com.fasterxml.jackson.databind.ext.CoreXMLSerializers", jVar);
            if (c == null) {
                return null;
            }
            return ((r) c).b(vVar, jVar, cVar);
        } else if (obj instanceof n) {
            return (n) obj;
        } else {
            return (n) c((String) obj, jVar);
        }
    }

    public final Object b(Class<?> cls, j jVar) {
        try {
            return d.g(cls, false);
        } catch (Throwable th) {
            StringBuilder R = a.R("Failed to create instance of `");
            R.append(cls.getName());
            R.append("` for handling values of type ");
            R.append(d.n(jVar));
            R.append(", problem: (");
            R.append(th.getClass().getName());
            R.append(") ");
            R.append(th.getMessage());
            throw new IllegalStateException(R.toString());
        }
    }

    public final Object c(String str, j jVar) {
        try {
            return b(Class.forName(str), jVar);
        } catch (Throwable th) {
            StringBuilder W = a.W("Failed to find class `", str, "` for handling values of type ");
            W.append(d.n(jVar));
            W.append(", problem: (");
            W.append(th.getClass().getName());
            W.append(") ");
            W.append(th.getMessage());
            throw new IllegalStateException(W.toString());
        }
    }
}
