package b.g.a.c;

import b.g.a.a.i;
import b.g.a.a.p;
import b.g.a.c.z.l;
import java.io.Serializable;
/* compiled from: BeanProperty.java */
/* loaded from: classes2.dex */
public interface d {
    public static final i.d c = new i.d("", i.c.ANY, "", "", i.b.a, null);

    /* compiled from: BeanProperty.java */
    /* loaded from: classes2.dex */
    public static class a implements d, Serializable {
        private static final long serialVersionUID = 1;
        public final b.g.a.c.c0.i _member;
        public final s _metadata;
        public final t _name;
        public final j _type;
        public final t _wrapperName;

        public a(t tVar, j jVar, t tVar2, b.g.a.c.c0.i iVar, s sVar) {
            this._name = tVar;
            this._type = jVar;
            this._wrapperName = tVar2;
            this._metadata = sVar;
            this._member = iVar;
        }

        @Override // b.g.a.c.d
        public i.d a(l<?> lVar, Class<?> cls) {
            b.g.a.c.c0.i iVar;
            i.d h;
            i.d i = lVar.i(cls);
            b e = lVar.e();
            return (e == null || (iVar = this._member) == null || (h = e.h(iVar)) == null) ? i : i.k(h);
        }

        @Override // b.g.a.c.d
        public p.b b(l<?> lVar, Class<?> cls) {
            b.g.a.c.c0.i iVar;
            p.b z2;
            p.b g = lVar.g(cls, this._type._class);
            b e = lVar.e();
            return (e == null || (iVar = this._member) == null || (z2 = e.z(iVar)) == null) ? g : g.a(z2);
        }

        @Override // b.g.a.c.d
        public b.g.a.c.c0.i getMember() {
            return this._member;
        }

        @Override // b.g.a.c.d
        public j getType() {
            return this._type;
        }
    }

    static {
        p.b bVar = p.b.j;
        p.b bVar2 = p.b.j;
    }

    i.d a(l<?> lVar, Class<?> cls);

    p.b b(l<?> lVar, Class<?> cls);

    b.g.a.c.c0.i getMember();

    j getType();
}
