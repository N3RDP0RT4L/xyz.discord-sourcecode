package b.g.a.c;

import b.g.a.b.s.a;
import b.g.a.c.h0.m;
import b.g.a.c.h0.n;
import b.g.a.c.i0.d;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.List;
/* compiled from: JavaType.java */
/* loaded from: classes2.dex */
public abstract class j extends a implements Serializable, Type {
    private static final long serialVersionUID = 1;
    public final boolean _asStatic;
    public final Class<?> _class;
    public final int _hash;
    public final Object _typeHandler;
    public final Object _valueHandler;

    public j(Class<?> cls, int i, Object obj, Object obj2, boolean z2) {
        this._class = cls;
        this._hash = cls.getName().hashCode() + i;
        this._valueHandler = obj;
        this._typeHandler = obj2;
        this._asStatic = z2;
    }

    public final boolean A() {
        Class<?> cls = this._class;
        Annotation[] annotationArr = d.a;
        Class<? super Object> superclass = cls.getSuperclass();
        return superclass != null && "java.lang.Record".equals(superclass.getName());
    }

    public final boolean B(Class<?> cls) {
        Class<?> cls2 = this._class;
        return cls2 == cls || cls.isAssignableFrom(cls2);
    }

    public abstract j C(Class<?> cls, m mVar, j jVar, j[] jVarArr);

    public abstract j D(j jVar);

    public abstract j E(Object obj);

    public j F(j jVar) {
        Object obj = jVar._typeHandler;
        j H = obj != this._typeHandler ? H(obj) : this;
        Object obj2 = jVar._valueHandler;
        return obj2 != this._valueHandler ? H.I(obj2) : H;
    }

    public abstract j G();

    public abstract j H(Object obj);

    public abstract j I(Object obj);

    public abstract boolean equals(Object obj);

    public abstract j f(int i);

    public abstract int g();

    public j h(int i) {
        j f = f(i);
        return f == null ? n.k() : f;
    }

    public final int hashCode() {
        return this._hash;
    }

    public abstract j i(Class<?> cls);

    public abstract m j();

    public j k() {
        return null;
    }

    public abstract StringBuilder l(StringBuilder sb);

    public abstract StringBuilder m(StringBuilder sb);

    public abstract List<j> n();

    public j o() {
        return null;
    }

    /* renamed from: p */
    public j a() {
        return null;
    }

    public abstract j q();

    public boolean r() {
        return g() > 0;
    }

    public boolean s() {
        return (this._typeHandler == null && this._valueHandler == null) ? false : true;
    }

    public final boolean t(Class<?> cls) {
        return this._class == cls;
    }

    public boolean u() {
        return false;
    }

    public abstract boolean v();

    public final boolean w() {
        return d.q(this._class);
    }

    public final boolean x() {
        return Modifier.isFinal(this._class.getModifiers());
    }

    public final boolean y() {
        return this._class == Object.class;
    }

    public boolean z() {
        return false;
    }
}
