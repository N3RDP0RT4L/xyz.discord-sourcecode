package b.g.a.c;

import b.g.a.a.b;
import b.g.a.a.g;
import b.g.a.a.i;
import b.g.a.a.n;
import b.g.a.a.p;
import b.g.a.a.q;
import b.g.a.a.u;
import b.g.a.a.z;
import b.g.a.c.c0.a0;
import b.g.a.c.c0.c;
import b.g.a.c.c0.g;
import b.g.a.c.c0.g0;
import b.g.a.c.c0.i;
import b.g.a.c.c0.j;
import b.g.a.c.e0.f;
import b.g.a.c.i0.n;
import b.g.a.c.y.e;
import b.g.a.c.z.l;
import com.fasterxml.jackson.databind.JsonMappingException;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.List;
/* compiled from: AnnotationIntrospector.java */
/* loaded from: classes2.dex */
public abstract class b implements Serializable {

    /* compiled from: AnnotationIntrospector.java */
    /* loaded from: classes2.dex */
    public static class a {
        public final int a;

        public a(int i, String str) {
            this.a = i;
        }
    }

    public q.a A(l<?> lVar, b.g.a.c.c0.b bVar) {
        return q.a.j;
    }

    public Integer B(b.g.a.c.c0.b bVar) {
        return null;
    }

    public f<?> C(l<?> lVar, i iVar, j jVar) {
        return null;
    }

    public a D(i iVar) {
        return null;
    }

    public t E(l<?> lVar, g gVar, t tVar) {
        return null;
    }

    public t F(c cVar) {
        return null;
    }

    public Object G(i iVar) {
        return null;
    }

    public Object H(b.g.a.c.c0.b bVar) {
        return null;
    }

    public String[] I(c cVar) {
        return null;
    }

    public Boolean J(b.g.a.c.c0.b bVar) {
        return null;
    }

    public e.b K(b.g.a.c.c0.b bVar) {
        return null;
    }

    public Object L(b.g.a.c.c0.b bVar) {
        return null;
    }

    public z.a M(b.g.a.c.c0.b bVar) {
        return z.a.j;
    }

    public List<b.g.a.c.e0.b> N(b.g.a.c.c0.b bVar) {
        return null;
    }

    public String O(c cVar) {
        return null;
    }

    public f<?> P(l<?> lVar, c cVar, j jVar) {
        return null;
    }

    public n Q(i iVar) {
        return null;
    }

    public Class<?>[] R(b.g.a.c.c0.b bVar) {
        return null;
    }

    public Boolean S(b.g.a.c.c0.b bVar) {
        if (!(bVar instanceof j) || !T((j) bVar)) {
            return null;
        }
        return Boolean.TRUE;
    }

    @Deprecated
    public boolean T(j jVar) {
        return false;
    }

    public Boolean U(b.g.a.c.c0.b bVar) {
        return null;
    }

    public Boolean V(l<?> lVar, b.g.a.c.c0.b bVar) {
        return null;
    }

    public Boolean W(b.g.a.c.c0.b bVar) {
        if (!(bVar instanceof j) || !X((j) bVar)) {
            return null;
        }
        return Boolean.TRUE;
    }

    @Deprecated
    public boolean X(j jVar) {
        return false;
    }

    @Deprecated
    public boolean Y(b.g.a.c.c0.b bVar) {
        return false;
    }

    public boolean Z(i iVar) {
        return false;
    }

    public void a(l<?> lVar, c cVar, List<b.g.a.c.g0.c> list) {
    }

    public Boolean a0(i iVar) {
        return null;
    }

    public g0<?> b(c cVar, g0<?> g0Var) {
        return g0Var;
    }

    public boolean b0(Annotation annotation) {
        return false;
    }

    public Object c(b.g.a.c.c0.b bVar) {
        return null;
    }

    public Boolean c0(c cVar) {
        return null;
    }

    public g.a d(l<?> lVar, b.g.a.c.c0.b bVar) {
        if (!Y(bVar)) {
            return null;
        }
        g.a e = e(bVar);
        return e == null ? g.a.DEFAULT : e;
    }

    public Boolean d0(i iVar) {
        return null;
    }

    @Deprecated
    public g.a e(b.g.a.c.c0.b bVar) {
        return null;
    }

    public j e0(l<?> lVar, b.g.a.c.c0.b bVar, j jVar) throws JsonMappingException {
        return jVar;
    }

    public String[] f(Class<?> cls, Enum<?>[] enumArr, String[] strArr) {
        return strArr;
    }

    public j f0(l<?> lVar, j jVar, j jVar2) {
        return null;
    }

    public Object g(b.g.a.c.c0.b bVar) {
        return null;
    }

    public i.d h(b.g.a.c.c0.b bVar) {
        return i.d.j;
    }

    public String i(b.g.a.c.c0.i iVar) {
        return null;
    }

    public b.a j(b.g.a.c.c0.i iVar) {
        Object k = k(iVar);
        if (k != null) {
            return b.a.a(k, null);
        }
        return null;
    }

    @Deprecated
    public Object k(b.g.a.c.c0.i iVar) {
        return null;
    }

    public Object l(b.g.a.c.c0.b bVar) {
        return null;
    }

    public Boolean m(b.g.a.c.c0.b bVar) {
        return null;
    }

    public t n(b.g.a.c.c0.b bVar) {
        return null;
    }

    public t o(b.g.a.c.c0.b bVar) {
        return null;
    }

    public Object p(c cVar) {
        return null;
    }

    public Object q(b.g.a.c.c0.b bVar) {
        return null;
    }

    public a0 r(b.g.a.c.c0.b bVar) {
        return null;
    }

    public a0 s(b.g.a.c.c0.b bVar, a0 a0Var) {
        return a0Var;
    }

    public u.a t(b.g.a.c.c0.b bVar) {
        return null;
    }

    public f<?> u(l<?> lVar, b.g.a.c.c0.i iVar, j jVar) {
        return null;
    }

    public String v(b.g.a.c.c0.b bVar) {
        return null;
    }

    public String w(b.g.a.c.c0.b bVar) {
        return null;
    }

    public n.a x(l<?> lVar, b.g.a.c.c0.b bVar) {
        return y(bVar);
    }

    @Deprecated
    public n.a y(b.g.a.c.c0.b bVar) {
        return n.a.j;
    }

    public p.b z(b.g.a.c.c0.b bVar) {
        p.b bVar2 = p.b.j;
        return p.b.j;
    }
}
