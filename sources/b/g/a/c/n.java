package b.g.a.c;

import b.g.a.b.d;
import b.g.a.c.e0.g;
import b.g.a.c.g0.t.t;
import java.io.IOException;
/* compiled from: JsonSerializer.java */
/* loaded from: classes2.dex */
public abstract class n<T> {

    /* compiled from: JsonSerializer.java */
    /* loaded from: classes2.dex */
    public static abstract class a extends n<Object> {
    }

    public Class<T> c() {
        return null;
    }

    public boolean d(x xVar, T t) {
        return t == null;
    }

    public boolean e() {
        return this instanceof t;
    }

    public abstract void f(T t, d dVar, x xVar) throws IOException;

    public void g(T t, d dVar, x xVar, g gVar) throws IOException {
        Class c = c();
        if (c == null) {
            c = t.getClass();
        }
        xVar.f(xVar.b(c), String.format("Type id handling not implemented for type %s (by serializer of type %s)", c.getName(), getClass().getName()));
        throw null;
    }

    public n<T> h(b.g.a.c.i0.n nVar) {
        return this;
    }

    public boolean i() {
        return false;
    }
}
