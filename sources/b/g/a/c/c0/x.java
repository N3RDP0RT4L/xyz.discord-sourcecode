package b.g.a.c.c0;

import b.g.a.a.a0;
import b.g.a.a.b;
import b.g.a.a.b0;
import b.g.a.a.c0;
import b.g.a.a.d;
import b.g.a.a.d0;
import b.g.a.a.e0;
import b.g.a.a.f;
import b.g.a.a.f0;
import b.g.a.a.g;
import b.g.a.a.g0;
import b.g.a.a.h0;
import b.g.a.a.i;
import b.g.a.a.k0;
import b.g.a.a.n;
import b.g.a.a.o;
import b.g.a.a.p;
import b.g.a.a.q;
import b.g.a.a.r;
import b.g.a.a.s;
import b.g.a.a.t;
import b.g.a.a.u;
import b.g.a.a.v;
import b.g.a.a.w;
import b.g.a.a.y;
import b.g.a.a.z;
import b.g.a.c.b;
import b.g.a.c.c0.g0;
import b.g.a.c.g0.t.a;
import b.g.a.c.i0.e;
import b.g.a.c.i0.h;
import b.g.a.c.i0.k;
import b.g.a.c.i0.m;
import b.g.a.c.j;
import b.g.a.c.n;
import b.g.a.c.p;
import b.g.a.c.y.b;
import b.g.a.c.y.c;
import b.g.a.c.y.e;
import b.g.a.c.z.l;
import com.fasterxml.jackson.databind.JsonMappingException;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
/* compiled from: JacksonAnnotationIntrospector.java */
/* loaded from: classes2.dex */
public class x extends b implements Serializable {
    public static final Class<? extends Annotation>[] j = {e.class, g0.class, i.class, c0.class, b.g.a.a.x.class, e0.class, f.class, s.class};
    public static final Class<? extends Annotation>[] k = {c.class, g0.class, i.class, c0.class, e0.class, f.class, s.class, t.class};
    public static final b.g.a.c.b0.c l;
    private static final long serialVersionUID = 1;
    public transient h<Class<?>, Boolean> m = new h<>(48, 48);
    public boolean _cfgConstructorPropertiesImpliesCreator = true;

    static {
        b.g.a.c.b0.c cVar;
        try {
            cVar = b.g.a.c.b0.c.a;
        } catch (Throwable unused) {
            cVar = null;
        }
        l = cVar;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v0, types: [java.util.Set] */
    /* JADX WARN: Type inference failed for: r0v1, types: [java.util.Set] */
    /* JADX WARN: Type inference failed for: r0v3, types: [java.util.HashSet] */
    @Override // b.g.a.c.b
    public q.a A(l<?> lVar, b bVar) {
        ?? r0;
        q qVar = (q) bVar.b(q.class);
        if (qVar == null) {
            return q.a.j;
        }
        String[] value = qVar.value();
        if (value == null || value.length == 0) {
            r0 = Collections.emptySet();
        } else {
            r0 = new HashSet(value.length);
            for (String str : value) {
                r0.add(str);
            }
        }
        return new q.a(r0);
    }

    @Override // b.g.a.c.b
    public Integer B(b bVar) {
        int index;
        u uVar = (u) bVar.b(u.class);
        if (uVar == null || (index = uVar.index()) == -1) {
            return null;
        }
        return Integer.valueOf(index);
    }

    @Override // b.g.a.c.b
    public b.g.a.c.e0.f<?> C(l<?> lVar, i iVar, j jVar) {
        if (jVar.v() || jVar.b()) {
            return null;
        }
        return h0(lVar, iVar, jVar);
    }

    @Override // b.g.a.c.b
    public b.a D(i iVar) {
        s sVar = (s) iVar.b(s.class);
        if (sVar != null) {
            return new b.a(1, sVar.value());
        }
        f fVar = (f) iVar.b(f.class);
        if (fVar != null) {
            return new b.a(2, fVar.value());
        }
        return null;
    }

    @Override // b.g.a.c.b
    public b.g.a.c.t E(l<?> lVar, g gVar, b.g.a.c.t tVar) {
        return null;
    }

    @Override // b.g.a.c.b
    public b.g.a.c.t F(c cVar) {
        y yVar = (y) cVar.b(y.class);
        String str = null;
        if (yVar == null) {
            return null;
        }
        String namespace = yVar.namespace();
        if (namespace == null || !namespace.isEmpty()) {
            str = namespace;
        }
        return b.g.a.c.t.b(yVar.value(), str);
    }

    @Override // b.g.a.c.b
    public Object G(i iVar) {
        Class<?> g02;
        e eVar = (e) iVar.b(e.class);
        if (eVar == null || (g02 = g0(eVar.contentConverter())) == null || g02 == e.a.class) {
            return null;
        }
        return g02;
    }

    @Override // b.g.a.c.b
    public Object H(b bVar) {
        Class<?> g02;
        b.g.a.c.y.e eVar = (b.g.a.c.y.e) bVar.b(b.g.a.c.y.e.class);
        if (eVar == null || (g02 = g0(eVar.converter())) == null || g02 == e.a.class) {
            return null;
        }
        return g02;
    }

    @Override // b.g.a.c.b
    public String[] I(c cVar) {
        w wVar = (w) cVar.b(w.class);
        if (wVar == null) {
            return null;
        }
        return wVar.value();
    }

    @Override // b.g.a.c.b
    public Boolean J(b bVar) {
        w wVar = (w) bVar.b(w.class);
        if (wVar == null || !wVar.alphabetic()) {
            return null;
        }
        return Boolean.TRUE;
    }

    @Override // b.g.a.c.b
    public e.b K(b bVar) {
        b.g.a.c.y.e eVar = (b.g.a.c.y.e) bVar.b(b.g.a.c.y.e.class);
        if (eVar == null) {
            return null;
        }
        return eVar.typing();
    }

    @Override // b.g.a.c.b
    public Object L(b bVar) {
        Class<? extends n> using;
        b.g.a.c.y.e eVar = (b.g.a.c.y.e) bVar.b(b.g.a.c.y.e.class);
        if (eVar != null && (using = eVar.using()) != n.a.class) {
            return using;
        }
        b.g.a.a.x xVar = (b.g.a.a.x) bVar.b(b.g.a.a.x.class);
        if (xVar == null || !xVar.value()) {
            return null;
        }
        return new b.g.a.c.g0.u.e0(bVar.d());
    }

    @Override // b.g.a.c.b
    public z.a M(b bVar) {
        z zVar = (z) bVar.b(z.class);
        if (zVar == null) {
            return z.a.j;
        }
        h0 nulls = zVar.nulls();
        h0 contentNulls = zVar.contentNulls();
        h0 h0Var = h0.DEFAULT;
        if (nulls == null) {
            nulls = h0Var;
        }
        if (contentNulls == null) {
            contentNulls = h0Var;
        }
        if (nulls == h0Var && contentNulls == h0Var) {
            return z.a.j;
        }
        return new z.a(nulls, contentNulls);
    }

    @Override // b.g.a.c.b
    public List<b.g.a.c.e0.b> N(b bVar) {
        a0 a0Var = (a0) bVar.b(a0.class);
        if (a0Var == null) {
            return null;
        }
        a0.a[] value = a0Var.value();
        ArrayList arrayList = new ArrayList(value.length);
        for (a0.a aVar : value) {
            arrayList.add(new b.g.a.c.e0.b(aVar.value(), aVar.name()));
            for (String str : aVar.names()) {
                arrayList.add(new b.g.a.c.e0.b(aVar.value(), str));
            }
        }
        return arrayList;
    }

    @Override // b.g.a.c.b
    public String O(c cVar) {
        d0 d0Var = (d0) cVar.b(d0.class);
        if (d0Var == null) {
            return null;
        }
        return d0Var.value();
    }

    @Override // b.g.a.c.b
    public b.g.a.c.e0.f<?> P(l<?> lVar, c cVar, j jVar) {
        return h0(lVar, cVar, jVar);
    }

    @Override // b.g.a.c.b
    public b.g.a.c.i0.n Q(i iVar) {
        e0 e0Var = (e0) iVar.b(e0.class);
        if (e0Var == null || !e0Var.enabled()) {
            return null;
        }
        String prefix = e0Var.prefix();
        String suffix = e0Var.suffix();
        boolean z2 = false;
        boolean z3 = prefix != null && !prefix.isEmpty();
        if (suffix != null && !suffix.isEmpty()) {
            z2 = true;
        }
        if (z3) {
            if (z2) {
                return new k(prefix, suffix);
            }
            return new b.g.a.c.i0.l(prefix);
        } else if (z2) {
            return new m(suffix);
        } else {
            return b.g.a.c.i0.n.j;
        }
    }

    @Override // b.g.a.c.b
    public Class<?>[] R(b bVar) {
        g0 g0Var = (g0) bVar.b(g0.class);
        if (g0Var == null) {
            return null;
        }
        return g0Var.value();
    }

    @Override // b.g.a.c.b
    public Boolean S(b bVar) {
        b.g.a.a.c cVar = (b.g.a.a.c) bVar.b(b.g.a.a.c.class);
        if (cVar == null) {
            return null;
        }
        return Boolean.valueOf(cVar.enabled());
    }

    @Override // b.g.a.c.b
    @Deprecated
    public boolean T(j jVar) {
        return jVar.k(b.g.a.a.c.class);
    }

    @Override // b.g.a.c.b
    public Boolean U(b bVar) {
        d dVar = (d) bVar.b(d.class);
        if (dVar == null) {
            return null;
        }
        return Boolean.valueOf(dVar.enabled());
    }

    @Override // b.g.a.c.b
    public Boolean V(l<?> lVar, b bVar) {
        r rVar = (r) bVar.b(r.class);
        if (rVar == null) {
            return null;
        }
        return Boolean.valueOf(rVar.value());
    }

    @Override // b.g.a.c.b
    public Boolean W(b bVar) {
        f0 f0Var = (f0) bVar.b(f0.class);
        if (f0Var == null) {
            return null;
        }
        return Boolean.valueOf(f0Var.value());
    }

    @Override // b.g.a.c.b
    @Deprecated
    public boolean X(j jVar) {
        f0 f0Var = (f0) jVar.b(f0.class);
        return f0Var != null && f0Var.value();
    }

    @Override // b.g.a.c.b
    @Deprecated
    public boolean Y(b bVar) {
        b.g.a.c.b0.c cVar;
        Boolean c;
        g gVar = (g) bVar.b(g.class);
        if (gVar != null) {
            return gVar.mode() != g.a.DISABLED;
        }
        if (!this._cfgConstructorPropertiesImpliesCreator || !(bVar instanceof e) || (cVar = l) == null || (c = cVar.c(bVar)) == null) {
            return false;
        }
        return c.booleanValue();
    }

    @Override // b.g.a.c.b
    public boolean Z(i iVar) {
        Boolean b2;
        b.g.a.a.m mVar = (b.g.a.a.m) iVar.b(b.g.a.a.m.class);
        if (mVar != null) {
            return mVar.value();
        }
        b.g.a.c.b0.c cVar = l;
        if (cVar == null || (b2 = cVar.b(iVar)) == null) {
            return false;
        }
        return b2.booleanValue();
    }

    @Override // b.g.a.c.b
    public void a(l<?> lVar, c cVar, List<b.g.a.c.g0.c> list) {
        b.g.a.c.y.b bVar = (b.g.a.c.y.b) cVar.t.a(b.g.a.c.y.b.class);
        if (bVar != null) {
            boolean prepend = bVar.prepend();
            b.a[] attrs = bVar.attrs();
            int length = attrs.length;
            b.g.a.c.h0.c cVar2 = null;
            j jVar = null;
            int i = 0;
            while (i < length) {
                if (jVar == null) {
                    jVar = lVar._base._typeFactory.b(cVar2, Object.class, b.g.a.c.h0.n.l);
                }
                b.a aVar = attrs[i];
                b.g.a.c.s sVar = aVar.required() ? b.g.a.c.s.j : b.g.a.c.s.k;
                String value = aVar.value();
                b.g.a.c.t j02 = j0(aVar.propName(), aVar.propNamespace());
                if (!j02.c()) {
                    j02 = b.g.a.c.t.a(value);
                }
                a aVar2 = new a(value, b.g.a.c.i0.q.w(lVar, new f0(cVar, cVar.l, value, jVar), j02, sVar, aVar.include()), cVar.t, jVar);
                if (prepend) {
                    list.add(i, aVar2);
                } else {
                    list.add(aVar2);
                }
                i++;
                cVar2 = null;
            }
            b.AbstractC0083b[] props = bVar.props();
            int length2 = props.length;
            for (int i2 = 0; i2 < length2; i2++) {
                b.AbstractC0083b bVar2 = props[i2];
                b.g.a.c.s sVar2 = bVar2.required() ? b.g.a.c.s.j : b.g.a.c.s.k;
                b.g.a.c.t j03 = j0(bVar2.name(), bVar2.namespace());
                j d = lVar.d(bVar2.type());
                b.g.a.c.i0.q w = b.g.a.c.i0.q.w(lVar, new f0(cVar, cVar.l, j03._simpleName, d), j03, sVar2, bVar2.include());
                Class<? extends b.g.a.c.g0.s> value2 = bVar2.value();
                Objects.requireNonNull(lVar._base);
                b.g.a.c.g0.s k2 = ((b.g.a.c.g0.s) b.g.a.c.i0.d.g(value2, lVar.b())).k(lVar, cVar, w, d);
                if (prepend) {
                    list.add(i2, k2);
                } else {
                    list.add(k2);
                }
            }
        }
    }

    @Override // b.g.a.c.b
    public Boolean a0(i iVar) {
        u uVar = (u) iVar.b(u.class);
        if (uVar != null) {
            return Boolean.valueOf(uVar.required());
        }
        return null;
    }

    @Override // b.g.a.c.b
    public g0<?> b(c cVar, g0<?> g0Var) {
        b.g.a.a.e eVar = (b.g.a.a.e) cVar.b(b.g.a.a.e.class);
        if (eVar == null) {
            return g0Var;
        }
        g0.a aVar = (g0.a) g0Var;
        Objects.requireNonNull(aVar);
        return aVar.b(aVar.a(aVar._getterMinLevel, eVar.getterVisibility()), aVar.a(aVar._isGetterMinLevel, eVar.isGetterVisibility()), aVar.a(aVar._setterMinLevel, eVar.setterVisibility()), aVar.a(aVar._creatorMinLevel, eVar.creatorVisibility()), aVar.a(aVar._fieldMinLevel, eVar.fieldVisibility()));
    }

    @Override // b.g.a.c.b
    public boolean b0(Annotation annotation) {
        Class<? extends Annotation> annotationType = annotation.annotationType();
        Boolean bool = this.m.get(annotationType);
        if (bool == null) {
            bool = Boolean.valueOf(annotationType.getAnnotation(b.g.a.a.a.class) != null);
            this.m.putIfAbsent(annotationType, bool);
        }
        return bool.booleanValue();
    }

    @Override // b.g.a.c.b
    public Object c(b bVar) {
        Class<? extends n> contentUsing;
        b.g.a.c.y.e eVar = (b.g.a.c.y.e) bVar.b(b.g.a.c.y.e.class);
        if (eVar == null || (contentUsing = eVar.contentUsing()) == n.a.class) {
            return null;
        }
        return contentUsing;
    }

    @Override // b.g.a.c.b
    public Boolean c0(c cVar) {
        o oVar = (o) cVar.b(o.class);
        if (oVar == null) {
            return null;
        }
        return Boolean.valueOf(oVar.value());
    }

    @Override // b.g.a.c.b
    public g.a d(l<?> lVar, b bVar) {
        b.g.a.c.b0.c cVar;
        Boolean c;
        g gVar = (g) bVar.b(g.class);
        if (gVar != null) {
            return gVar.mode();
        }
        if (!this._cfgConstructorPropertiesImpliesCreator || !lVar.q(p.INFER_CREATOR_FROM_CONSTRUCTOR_PROPERTIES) || !(bVar instanceof e) || (cVar = l) == null || (c = cVar.c(bVar)) == null || !c.booleanValue()) {
            return null;
        }
        return g.a.PROPERTIES;
    }

    @Override // b.g.a.c.b
    public Boolean d0(i iVar) {
        return Boolean.valueOf(iVar.k(b0.class));
    }

    @Override // b.g.a.c.b
    @Deprecated
    public g.a e(b bVar) {
        g gVar = (g) bVar.b(g.class);
        if (gVar == null) {
            return null;
        }
        return gVar.mode();
    }

    @Override // b.g.a.c.b
    public j e0(l<?> lVar, b bVar, j jVar) throws JsonMappingException {
        j G;
        j G2;
        b.g.a.c.h0.n nVar = lVar._base._typeFactory;
        b.g.a.c.y.e eVar = (b.g.a.c.y.e) bVar.b(b.g.a.c.y.e.class);
        Class<?> g02 = eVar == null ? null : g0(eVar.as());
        if (g02 != null) {
            Class<?> cls = jVar._class;
            if (cls == g02) {
                jVar = jVar.G();
            } else {
                try {
                    if (g02.isAssignableFrom(cls)) {
                        jVar = nVar.g(jVar, g02);
                    } else if (cls.isAssignableFrom(g02)) {
                        jVar = nVar.h(jVar, g02, false);
                    } else if (i0(cls, g02)) {
                        jVar = jVar.G();
                    } else {
                        throw new JsonMappingException(null, String.format("Cannot refine serialization type %s into %s; types not related", jVar, g02.getName()));
                    }
                } catch (IllegalArgumentException e) {
                    throw new JsonMappingException(null, String.format("Failed to widen type %s with annotation (value %s), from '%s': %s", jVar, g02.getName(), bVar.c(), e.getMessage()), e);
                }
            }
        }
        if (jVar.z()) {
            j o = jVar.o();
            Class<?> g03 = eVar == null ? null : g0(eVar.keyAs());
            if (g03 != null) {
                Class<?> cls2 = o._class;
                if (cls2 == g03) {
                    G2 = o.G();
                } else {
                    try {
                        if (g03.isAssignableFrom(cls2)) {
                            G2 = nVar.g(o, g03);
                        } else if (cls2.isAssignableFrom(g03)) {
                            G2 = nVar.h(o, g03, false);
                        } else if (i0(cls2, g03)) {
                            G2 = o.G();
                        } else {
                            throw new JsonMappingException(null, String.format("Cannot refine serialization key type %s into %s; types not related", o, g03.getName()));
                        }
                    } catch (IllegalArgumentException e2) {
                        throw new JsonMappingException(null, String.format("Failed to widen key type of %s with concrete-type annotation (value %s), from '%s': %s", jVar, g03.getName(), bVar.c(), e2.getMessage()), e2);
                    }
                }
                jVar = ((b.g.a.c.h0.f) jVar).M(G2);
            }
        }
        j k2 = jVar.k();
        if (k2 == null) {
            return jVar;
        }
        Class<?> g04 = eVar == null ? null : g0(eVar.contentAs());
        if (g04 == null) {
            return jVar;
        }
        Class<?> cls3 = k2._class;
        if (cls3 == g04) {
            G = k2.G();
        } else {
            try {
                if (g04.isAssignableFrom(cls3)) {
                    G = nVar.g(k2, g04);
                } else if (cls3.isAssignableFrom(g04)) {
                    G = nVar.h(k2, g04, false);
                } else if (i0(cls3, g04)) {
                    G = k2.G();
                } else {
                    throw new JsonMappingException(null, String.format("Cannot refine serialization content type %s into %s; types not related", k2, g04.getName()));
                }
            } catch (IllegalArgumentException e3) {
                throw new JsonMappingException(null, String.format("Internal error: failed to refine value type of %s with concrete-type annotation (value %s), from '%s': %s", jVar, g04.getName(), bVar.c(), e3.getMessage()), e3);
            }
        }
        return jVar.D(G);
    }

    @Override // b.g.a.c.b
    public String[] f(Class<?> cls, Enum<?>[] enumArr, String[] strArr) {
        Field[] declaredFields;
        u uVar;
        HashMap hashMap = null;
        for (Field field : cls.getDeclaredFields()) {
            if (field.isEnumConstant() && (uVar = (u) field.getAnnotation(u.class)) != null) {
                String value = uVar.value();
                if (!value.isEmpty()) {
                    if (hashMap == null) {
                        hashMap = new HashMap();
                    }
                    hashMap.put(field.getName(), value);
                }
            }
        }
        if (hashMap != null) {
            int length = enumArr.length;
            for (int i = 0; i < length; i++) {
                String str = (String) hashMap.get(enumArr[i].name());
                if (str != null) {
                    strArr[i] = str;
                }
            }
        }
        return strArr;
    }

    @Override // b.g.a.c.b
    public j f0(l<?> lVar, j jVar, j jVar2) {
        Class<?> p = jVar.p(0);
        Class<?> p2 = jVar2.p(0);
        if (p.isPrimitive()) {
            if (!p2.isPrimitive()) {
                return jVar;
            }
        } else if (p2.isPrimitive()) {
            return jVar2;
        }
        if (p == String.class) {
            if (p2 != String.class) {
                return jVar;
            }
            return null;
        } else if (p2 == String.class) {
            return jVar2;
        } else {
            return null;
        }
    }

    @Override // b.g.a.c.b
    public Object g(b bVar) {
        b.g.a.a.h hVar = (b.g.a.a.h) bVar.b(b.g.a.a.h.class);
        if (hVar == null) {
            return null;
        }
        String value = hVar.value();
        if (!value.isEmpty()) {
            return value;
        }
        return null;
    }

    public Class<?> g0(Class<?> cls) {
        if (cls == null || b.g.a.c.i0.d.p(cls)) {
            return null;
        }
        return cls;
    }

    @Override // b.g.a.c.b
    public i.d h(b bVar) {
        i iVar = (i) bVar.b(i.class);
        if (iVar == null) {
            return null;
        }
        String pattern = iVar.pattern();
        i.c shape = iVar.shape();
        String locale = iVar.locale();
        String timezone = iVar.timezone();
        i.a[] with = iVar.with();
        i.a[] without = iVar.without();
        int i = 0;
        for (i.a aVar : with) {
            i |= 1 << aVar.ordinal();
        }
        int i2 = 0;
        for (i.a aVar2 : without) {
            i2 |= 1 << aVar2.ordinal();
        }
        return new i.d(pattern, shape, locale, timezone, new i.b(i, i2), iVar.lenient().f());
    }

    public b.g.a.c.e0.f<?> h0(l<?> lVar, b bVar, j jVar) {
        b.g.a.c.e0.f fVar;
        c0 c0Var = (c0) bVar.b(c0.class);
        b.g.a.c.y.g gVar = (b.g.a.c.y.g) bVar.b(b.g.a.c.y.g.class);
        b.g.a.c.e0.e eVar = null;
        if (gVar != null) {
            if (c0Var == null) {
                return null;
            }
            Class<? extends b.g.a.c.e0.f<?>> value = gVar.value();
            Objects.requireNonNull(lVar._base);
            fVar = (b.g.a.c.e0.f) b.g.a.c.i0.d.g(value, lVar.b());
        } else if (c0Var == null) {
            return null;
        } else {
            c0.b use = c0Var.use();
            c0.b bVar2 = c0.b.NONE;
            if (use == bVar2) {
                b.g.a.c.e0.h.j jVar2 = new b.g.a.c.e0.h.j();
                jVar2.g(bVar2, null);
                return jVar2;
            }
            fVar = new b.g.a.c.e0.h.j();
        }
        b.g.a.c.y.f fVar2 = (b.g.a.c.y.f) bVar.b(b.g.a.c.y.f.class);
        if (fVar2 != null) {
            Class<? extends b.g.a.c.e0.e> value2 = fVar2.value();
            Objects.requireNonNull(lVar._base);
            eVar = (b.g.a.c.e0.e) b.g.a.c.i0.d.g(value2, lVar.b());
        }
        if (eVar != null) {
            eVar.b(jVar);
        }
        b.g.a.c.e0.f b2 = fVar.b(c0Var.use(), eVar);
        c0.a include = c0Var.include();
        if (include == c0.a.EXTERNAL_PROPERTY && (bVar instanceof c)) {
            include = c0.a.PROPERTY;
        }
        b.g.a.c.e0.f c = b2.f(include).c(c0Var.property());
        Class<?> defaultImpl = c0Var.defaultImpl();
        if (defaultImpl != c0.c.class && !defaultImpl.isAnnotation()) {
            c = c.d(defaultImpl);
        }
        return c.a(c0Var.visible());
    }

    /* JADX WARN: Code restructure failed: missing block: B:9:0x0013, code lost:
        if (r3 != null) goto L11;
     */
    @Override // b.g.a.c.b
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public java.lang.String i(b.g.a.c.c0.i r3) {
        /*
            r2 = this;
            boolean r0 = r3 instanceof b.g.a.c.c0.m
            r1 = 0
            if (r0 == 0) goto L16
            b.g.a.c.c0.m r3 = (b.g.a.c.c0.m) r3
            b.g.a.c.c0.n r0 = r3._owner
            if (r0 == 0) goto L16
            b.g.a.c.b0.c r0 = b.g.a.c.c0.x.l
            if (r0 == 0) goto L16
            b.g.a.c.t r3 = r0.a(r3)
            if (r3 == 0) goto L16
            goto L17
        L16:
            r3 = r1
        L17:
            if (r3 != 0) goto L1a
            goto L1c
        L1a:
            java.lang.String r1 = r3._simpleName
        L1c:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.c.c0.x.i(b.g.a.c.c0.i):java.lang.String");
    }

    public final boolean i0(Class<?> cls, Class<?> cls2) {
        return cls.isPrimitive() ? cls == b.g.a.c.i0.d.v(cls2) : cls2.isPrimitive() && cls2 == b.g.a.c.i0.d.v(cls);
    }

    @Override // b.g.a.c.b
    public b.a j(i iVar) {
        String str;
        b.g.a.a.b bVar = (b.g.a.a.b) iVar.b(b.g.a.a.b.class);
        if (bVar == null) {
            return null;
        }
        b.a a = b.a.a(bVar.value(), bVar.useInput().f());
        if (a._id != null) {
            return a;
        }
        if (!(iVar instanceof j)) {
            str = iVar.d().getName();
        } else {
            j jVar = (j) iVar;
            if (jVar.o() == 0) {
                str = iVar.d().getName();
            } else {
                str = jVar.p(0).getName();
            }
        }
        return str.equals(a._id) ? a : new b.a(str, a._useInput);
    }

    public b.g.a.c.t j0(String str, String str2) {
        if (str.isEmpty()) {
            return b.g.a.c.t.j;
        }
        if (str2 == null || str2.isEmpty()) {
            return b.g.a.c.t.a(str);
        }
        return b.g.a.c.t.b(str, str2);
    }

    @Override // b.g.a.c.b
    @Deprecated
    public Object k(i iVar) {
        b.a j2 = j(iVar);
        if (j2 == null) {
            return null;
        }
        return j2._id;
    }

    @Override // b.g.a.c.b
    public Object l(b bVar) {
        Class<? extends n> keyUsing;
        b.g.a.c.y.e eVar = (b.g.a.c.y.e) bVar.b(b.g.a.c.y.e.class);
        if (eVar == null || (keyUsing = eVar.keyUsing()) == n.a.class) {
            return null;
        }
        return keyUsing;
    }

    @Override // b.g.a.c.b
    public Boolean m(b bVar) {
        t tVar = (t) bVar.b(t.class);
        if (tVar == null) {
            return null;
        }
        return tVar.value().f();
    }

    @Override // b.g.a.c.b
    public b.g.a.c.t n(b bVar) {
        boolean z2;
        z zVar = (z) bVar.b(z.class);
        boolean z3 = false;
        if (zVar != null) {
            String value = zVar.value();
            if (!value.isEmpty()) {
                return b.g.a.c.t.a(value);
            }
            z2 = true;
        } else {
            z2 = false;
        }
        u uVar = (u) bVar.b(u.class);
        String str = null;
        if (uVar != null) {
            String namespace = uVar.namespace();
            if (namespace == null || !namespace.isEmpty()) {
                str = namespace;
            }
            return b.g.a.c.t.b(uVar.value(), str);
        }
        if (!z2) {
            Class<? extends Annotation>[] clsArr = k;
            p pVar = ((i) bVar).k;
            if (pVar != null) {
                z3 = pVar.b(clsArr);
            }
            if (!z3) {
                return null;
            }
        }
        return b.g.a.c.t.j;
    }

    @Override // b.g.a.c.b
    public b.g.a.c.t o(b bVar) {
        boolean z2;
        b.g.a.a.j jVar = (b.g.a.a.j) bVar.b(b.g.a.a.j.class);
        boolean z3 = false;
        if (jVar != null) {
            String value = jVar.value();
            if (!value.isEmpty()) {
                return b.g.a.c.t.a(value);
            }
            z2 = true;
        } else {
            z2 = false;
        }
        u uVar = (u) bVar.b(u.class);
        String str = null;
        if (uVar != null) {
            String namespace = uVar.namespace();
            if (namespace == null || !namespace.isEmpty()) {
                str = namespace;
            }
            return b.g.a.c.t.b(uVar.value(), str);
        }
        if (!z2) {
            Class<? extends Annotation>[] clsArr = j;
            p pVar = ((i) bVar).k;
            if (pVar != null) {
                z3 = pVar.b(clsArr);
            }
            if (!z3) {
                return null;
            }
        }
        return b.g.a.c.t.j;
    }

    @Override // b.g.a.c.b
    public Object p(c cVar) {
        b.g.a.c.y.d dVar = (b.g.a.c.y.d) cVar.b(b.g.a.c.y.d.class);
        if (dVar == null) {
            return null;
        }
        return dVar.value();
    }

    @Override // b.g.a.c.b
    public Object q(b bVar) {
        Class<? extends n> nullsUsing;
        b.g.a.c.y.e eVar = (b.g.a.c.y.e) bVar.b(b.g.a.c.y.e.class);
        if (eVar == null || (nullsUsing = eVar.nullsUsing()) == n.a.class) {
            return null;
        }
        return nullsUsing;
    }

    @Override // b.g.a.c.b
    public a0 r(b bVar) {
        b.g.a.a.k kVar = (b.g.a.a.k) bVar.b(b.g.a.a.k.class);
        if (kVar == null || kVar.generator() == k0.class) {
            return null;
        }
        return new a0(b.g.a.c.t.a(kVar.property()), kVar.scope(), kVar.generator(), false, kVar.resolver());
    }

    public Object readResolve() {
        if (this.m == null) {
            this.m = new h<>(48, 48);
        }
        return this;
    }

    @Override // b.g.a.c.b
    public a0 s(b bVar, a0 a0Var) {
        b.g.a.a.l lVar = (b.g.a.a.l) bVar.b(b.g.a.a.l.class);
        if (lVar == null) {
            return a0Var;
        }
        if (a0Var == null) {
            a0Var = a0.a;
        }
        boolean alwaysAsId = lVar.alwaysAsId();
        return a0Var.f == alwaysAsId ? a0Var : new a0(a0Var.f677b, a0Var.e, a0Var.c, alwaysAsId, a0Var.d);
    }

    @Override // b.g.a.c.b
    public u.a t(b bVar) {
        u uVar = (u) bVar.b(u.class);
        if (uVar != null) {
            return uVar.access();
        }
        return null;
    }

    @Override // b.g.a.c.b
    public b.g.a.c.e0.f<?> u(l<?> lVar, i iVar, j jVar) {
        if (jVar.k() != null) {
            return h0(lVar, iVar, jVar);
        }
        throw new IllegalArgumentException("Must call method with a container or reference type (got " + jVar + ")");
    }

    @Override // b.g.a.c.b
    public String v(b bVar) {
        u uVar = (u) bVar.b(u.class);
        if (uVar == null) {
            return null;
        }
        String defaultValue = uVar.defaultValue();
        if (defaultValue.isEmpty()) {
            return null;
        }
        return defaultValue;
    }

    @Override // b.g.a.c.b
    public String w(b bVar) {
        v vVar = (v) bVar.b(v.class);
        if (vVar == null) {
            return null;
        }
        return vVar.value();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v0, types: [java.util.Set] */
    /* JADX WARN: Type inference failed for: r1v1, types: [java.util.Set] */
    /* JADX WARN: Type inference failed for: r1v3, types: [java.util.HashSet] */
    @Override // b.g.a.c.b
    public n.a x(l<?> lVar, b bVar) {
        ?? r1;
        b.g.a.a.n nVar = (b.g.a.a.n) bVar.b(b.g.a.a.n.class);
        if (nVar == null) {
            return n.a.j;
        }
        n.a aVar = n.a.j;
        String[] value = nVar.value();
        if (value == null || value.length == 0) {
            r1 = Collections.emptySet();
        } else {
            r1 = new HashSet(value.length);
            for (String str : value) {
                r1.add(str);
            }
        }
        return n.a.c(r1, nVar.ignoreUnknown(), nVar.allowGetters(), nVar.allowSetters(), false);
    }

    @Override // b.g.a.c.b
    @Deprecated
    public n.a y(b bVar) {
        return x(null, bVar);
    }

    @Override // b.g.a.c.b
    public p.b z(b bVar) {
        p.b bVar2;
        b.g.a.c.y.e eVar;
        p.b bVar3;
        p.a aVar = p.a.USE_DEFAULTS;
        b.g.a.a.p pVar = (b.g.a.a.p) bVar.b(b.g.a.a.p.class);
        if (pVar == null) {
            p.b bVar4 = p.b.j;
            bVar2 = p.b.j;
        } else {
            p.b bVar5 = p.b.j;
            p.a value = pVar.value();
            p.a content = pVar.content();
            if (value == aVar && content == aVar) {
                bVar2 = p.b.j;
            } else {
                Class<?> valueFilter = pVar.valueFilter();
                Class<?> cls = null;
                if (valueFilter == Void.class) {
                    valueFilter = null;
                }
                Class<?> contentFilter = pVar.contentFilter();
                if (contentFilter != Void.class) {
                    cls = contentFilter;
                }
                bVar2 = new p.b(value, content, valueFilter, cls);
            }
        }
        if (bVar2._valueInclusion != aVar || (eVar = (b.g.a.c.y.e) bVar.b(b.g.a.c.y.e.class)) == null) {
            return bVar2;
        }
        int ordinal = eVar.include().ordinal();
        if (ordinal == 0) {
            bVar3 = bVar2.b(p.a.ALWAYS);
        } else if (ordinal == 1) {
            bVar3 = bVar2.b(p.a.NON_NULL);
        } else if (ordinal == 2) {
            bVar3 = bVar2.b(p.a.NON_DEFAULT);
        } else if (ordinal != 3) {
            return bVar2;
        } else {
            bVar3 = bVar2.b(p.a.NON_EMPTY);
        }
        return bVar3;
    }
}
