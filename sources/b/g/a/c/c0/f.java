package b.g.a.c.c0;

import b.g.a.c.b;
import b.g.a.c.c0.o;
import b.g.a.c.i0.d;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
/* compiled from: AnnotatedCreatorCollector.java */
/* loaded from: classes2.dex */
public final class f extends u {
    public final e0 d;
    public final boolean e;
    public e f;

    public f(b bVar, e0 e0Var, boolean z2) {
        super(bVar);
        this.d = e0Var;
        this.e = z2;
    }

    public static boolean f(Method method) {
        return Modifier.isStatic(method.getModifiers()) && !method.isSynthetic();
    }

    public final p g(d.a aVar, d.a aVar2) {
        if (!this.e) {
            return new p();
        }
        Annotation[] annotationArr = aVar.f723b;
        if (annotationArr == null) {
            annotationArr = aVar.a.getDeclaredAnnotations();
            aVar.f723b = annotationArr;
        }
        o c = c(annotationArr);
        if (aVar2 != null) {
            Annotation[] annotationArr2 = aVar2.f723b;
            if (annotationArr2 == null) {
                annotationArr2 = aVar2.a.getDeclaredAnnotations();
                aVar2.f723b = annotationArr2;
            }
            c = b(c, annotationArr2);
        }
        return c.b();
    }

    public final p h(AnnotatedElement annotatedElement, AnnotatedElement annotatedElement2) {
        o c = c(annotatedElement.getDeclaredAnnotations());
        if (annotatedElement2 != null) {
            c = b(c, annotatedElement2.getDeclaredAnnotations());
        }
        return c.b();
    }

    public final p[] i(Annotation[][] annotationArr, Annotation[][] annotationArr2) {
        if (!this.e) {
            return u.a;
        }
        int length = annotationArr.length;
        p[] pVarArr = new p[length];
        for (int i = 0; i < length; i++) {
            o b2 = b(o.a.c, annotationArr[i]);
            if (annotationArr2 != null) {
                b2 = b(b2, annotationArr2[i]);
            }
            pVarArr[i] = b2.b();
        }
        return pVarArr;
    }

    public j j(Method method, e0 e0Var, Method method2) {
        int length = method.getParameterTypes().length;
        if (this.c == null) {
            return new j(e0Var, method, new p(), u.a(length));
        }
        if (length == 0) {
            return new j(e0Var, method, h(method, method2), u.a);
        }
        return new j(e0Var, method, h(method, method2), i(method.getParameterAnnotations(), method2 == null ? null : method2.getParameterAnnotations()));
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:26:0x007d  */
    /* JADX WARN: Type inference failed for: r3v6 */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.g.a.c.c0.e k(b.g.a.c.i0.d.a r9, b.g.a.c.i0.d.a r10) {
        /*
            r8 = this;
            int r0 = r9.a()
            b.g.a.c.b r1 = r8.c
            if (r1 != 0) goto L1b
            b.g.a.c.c0.e r10 = new b.g.a.c.c0.e
            b.g.a.c.c0.e0 r1 = r8.d
            java.lang.reflect.Constructor<?> r9 = r9.a
            b.g.a.c.c0.p r2 = new b.g.a.c.c0.p
            r2.<init>()
            b.g.a.c.c0.p[] r0 = b.g.a.c.c0.u.a(r0)
            r10.<init>(r1, r9, r2, r0)
            return r10
        L1b:
            if (r0 != 0) goto L2d
            b.g.a.c.c0.e r0 = new b.g.a.c.c0.e
            b.g.a.c.c0.e0 r1 = r8.d
            java.lang.reflect.Constructor<?> r2 = r9.a
            b.g.a.c.c0.p r9 = r8.g(r9, r10)
            b.g.a.c.c0.p[] r10 = b.g.a.c.c0.u.a
            r0.<init>(r1, r2, r9, r10)
            return r0
        L2d:
            java.lang.annotation.Annotation[][] r1 = r9.c
            if (r1 != 0) goto L39
            java.lang.reflect.Constructor<?> r1 = r9.a
            java.lang.annotation.Annotation[][] r1 = r1.getParameterAnnotations()
            r9.c = r1
        L39:
            int r2 = r1.length
            r3 = 0
            if (r0 == r2) goto La5
            java.lang.reflect.Constructor<?> r2 = r9.a
            java.lang.Class r2 = r2.getDeclaringClass()
            boolean r4 = b.g.a.c.i0.d.q(r2)
            r5 = 0
            r6 = 1
            r7 = 2
            if (r4 == 0) goto L5f
            int r4 = r1.length
            int r4 = r4 + r7
            if (r0 != r4) goto L5f
            int r2 = r1.length
            int r2 = r2 + r7
            java.lang.annotation.Annotation[][] r2 = new java.lang.annotation.Annotation[r2]
            int r4 = r1.length
            java.lang.System.arraycopy(r1, r5, r2, r7, r4)
            b.g.a.c.c0.p[] r1 = r8.i(r2, r3)
        L5c:
            r3 = r1
            r1 = r2
            goto L7a
        L5f:
            boolean r2 = r2.isMemberClass()
            if (r2 == 0) goto L7a
            int r2 = r1.length
            int r2 = r2 + r6
            if (r0 != r2) goto L7a
            int r2 = r1.length
            int r2 = r2 + r6
            java.lang.annotation.Annotation[][] r2 = new java.lang.annotation.Annotation[r2]
            int r4 = r1.length
            java.lang.System.arraycopy(r1, r5, r2, r6, r4)
            java.lang.annotation.Annotation[] r1 = b.g.a.c.c0.u.f690b
            r2[r5] = r1
            b.g.a.c.c0.p[] r1 = r8.i(r2, r3)
            goto L5c
        L7a:
            if (r3 == 0) goto L7d
            goto Lb9
        L7d:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            r2 = 3
            java.lang.Object[] r2 = new java.lang.Object[r2]
            java.lang.reflect.Constructor<?> r9 = r9.a
            java.lang.Class r9 = r9.getDeclaringClass()
            java.lang.String r9 = r9.getName()
            r2[r5] = r9
            java.lang.Integer r9 = java.lang.Integer.valueOf(r0)
            r2[r6] = r9
            int r9 = r1.length
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
            r2[r7] = r9
            java.lang.String r9 = "Internal error: constructor for %s has mismatch: %d parameters; %d sets of annotations"
            java.lang.String r9 = java.lang.String.format(r9, r2)
            r10.<init>(r9)
            throw r10
        La5:
            if (r10 != 0) goto La8
            goto Lb5
        La8:
            java.lang.annotation.Annotation[][] r0 = r10.c
            if (r0 != 0) goto Lb4
            java.lang.reflect.Constructor<?> r0 = r10.a
            java.lang.annotation.Annotation[][] r0 = r0.getParameterAnnotations()
            r10.c = r0
        Lb4:
            r3 = r0
        Lb5:
            b.g.a.c.c0.p[] r3 = r8.i(r1, r3)
        Lb9:
            b.g.a.c.c0.e r0 = new b.g.a.c.c0.e
            b.g.a.c.c0.e0 r1 = r8.d
            java.lang.reflect.Constructor<?> r2 = r9.a
            b.g.a.c.c0.p r9 = r8.g(r9, r10)
            r0.<init>(r1, r2, r9, r3)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.c.c0.f.k(b.g.a.c.i0.d$a, b.g.a.c.i0.d$a):b.g.a.c.c0.e");
    }
}
