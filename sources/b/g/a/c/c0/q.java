package b.g.a.c.c0;

import b.g.a.a.i;
import b.g.a.a.p;
import b.g.a.c.b;
import b.g.a.c.c;
import b.g.a.c.j;
import b.g.a.c.z.l;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
/* compiled from: BasicBeanDescription.java */
/* loaded from: classes2.dex */
public class q extends c {

    /* renamed from: b  reason: collision with root package name */
    public static final Class<?>[] f689b = new Class[0];
    public final b0 c;
    public final l<?> d;
    public final b e;
    public final c f;
    public Class<?>[] g;
    public boolean h;
    public List<s> i;
    public a0 j;

    public q(l<?> lVar, j jVar, c cVar, List<s> list) {
        super(jVar);
        this.c = null;
        this.d = lVar;
        if (lVar == null) {
            this.e = null;
        } else {
            this.e = lVar.e();
        }
        this.f = cVar;
        this.i = list;
    }

    public static q e(l<?> lVar, j jVar, c cVar) {
        return new q(lVar, jVar, cVar, Collections.emptyList());
    }

    @Override // b.g.a.c.c
    public i.d a(i.d dVar) {
        i.d dVar2;
        b bVar = this.e;
        if (bVar == null || (dVar2 = bVar.h(this.f)) == null) {
            dVar2 = null;
        }
        i.d i = this.d.i(this.f.l);
        return i != null ? dVar2 == null ? i : dVar2.k(i) : dVar2;
    }

    @Override // b.g.a.c.c
    public i b() {
        b0 b0Var = this.c;
        if (b0Var == null) {
            return null;
        }
        if (!b0Var.i) {
            b0Var.h();
        }
        LinkedList<i> linkedList = b0Var.r;
        if (linkedList == null) {
            return null;
        }
        if (linkedList.size() <= 1) {
            return b0Var.r.get(0);
        }
        b0Var.i("Multiple 'as-value' properties defined (%s vs %s)", b0Var.r.get(0), b0Var.r.get(1));
        throw null;
    }

    @Override // b.g.a.c.c
    public p.b c(p.b bVar) {
        p.b z2;
        b bVar2 = this.e;
        return (bVar2 == null || (z2 = bVar2.z(this.f)) == null) ? bVar : bVar == null ? z2 : bVar.a(z2);
    }

    public List<s> d() {
        if (this.i == null) {
            b0 b0Var = this.c;
            if (!b0Var.i) {
                b0Var.h();
            }
            this.i = new ArrayList(b0Var.j.values());
        }
        return this.i;
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public q(b.g.a.c.c0.b0 r3) {
        /*
            r2 = this;
            b.g.a.c.j r0 = r3.d
            b.g.a.c.c0.c r1 = r3.e
            r2.<init>(r0)
            r2.c = r3
            b.g.a.c.z.l<?> r0 = r3.a
            r2.d = r0
            if (r0 != 0) goto L13
            r0 = 0
            r2.e = r0
            goto L19
        L13:
            b.g.a.c.b r0 = r0.e()
            r2.e = r0
        L19:
            r2.f = r1
            b.g.a.c.b r0 = r3.g
            b.g.a.c.c0.c r1 = r3.e
            b.g.a.c.c0.a0 r0 = r0.r(r1)
            if (r0 == 0) goto L2d
            b.g.a.c.b r1 = r3.g
            b.g.a.c.c0.c r3 = r3.e
            b.g.a.c.c0.a0 r0 = r1.s(r3, r0)
        L2d:
            r2.j = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.c.c0.q.<init>(b.g.a.c.c0.b0):void");
    }
}
