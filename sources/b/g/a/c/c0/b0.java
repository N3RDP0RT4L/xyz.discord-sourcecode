package b.g.a.c.c0;

import b.d.b.a.a;
import b.g.a.a.b;
import b.g.a.a.g;
import b.g.a.c.b;
import b.g.a.c.c0.c0;
import b.g.a.c.j;
import b.g.a.c.p;
import b.g.a.c.t;
import b.g.a.c.z.l;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
/* compiled from: POJOPropertiesCollector.java */
/* loaded from: classes2.dex */
public class b0 {
    public final l<?> a;

    /* renamed from: b  reason: collision with root package name */
    public final a f678b;
    public final boolean c;
    public final j d;
    public final c e;
    public final g0<?> f;
    public final b g;
    public final boolean h;
    public boolean i;
    public LinkedHashMap<String, c0> j;
    public LinkedList<c0> k;
    public Map<t, t> l;
    public LinkedList<i> m;
    public LinkedList<i> n;
    public LinkedList<j> o;
    public LinkedList<i> p;
    public LinkedList<i> q;
    public LinkedList<i> r;

    /* renamed from: s  reason: collision with root package name */
    public HashSet<String> f679s;
    public LinkedHashMap<Object, i> t;

    public b0(l<?> lVar, boolean z2, j jVar, c cVar, a aVar) {
        this.a = lVar;
        this.c = z2;
        this.d = jVar;
        this.e = cVar;
        if (lVar.p()) {
            this.h = true;
            this.g = lVar.e();
        } else {
            this.h = false;
            this.g = z.j;
        }
        this.f = lVar.m(jVar._class, cVar);
        this.f678b = aVar;
        lVar.q(p.USE_STD_BEAN_NAMING);
    }

    public void a(Map<String, c0> map, m mVar) {
        c0 c0Var;
        g.a d;
        String i = this.g.i(mVar);
        if (i == null) {
            i = "";
        }
        t n = this.g.n(mVar);
        boolean z2 = n != null && !n.d();
        if (!z2) {
            if (!i.isEmpty() && (d = this.g.d(this.a, mVar._owner)) != null && d != g.a.DISABLED) {
                n = t.a(i);
            } else {
                return;
            }
        }
        t tVar = n;
        String b2 = b(i);
        if (!z2 || !b2.isEmpty()) {
            c0Var = f(map, b2);
        } else {
            String str = tVar._simpleName;
            c0Var = map.get(str);
            if (c0Var == null) {
                c0Var = new c0(this.a, this.g, this.c, tVar);
                map.put(str, c0Var);
            }
        }
        c0Var.r = new c0.d<>(mVar, c0Var.r, tVar, z2, true, false);
        this.k.add(c0Var);
    }

    public final String b(String str) {
        t tVar;
        Map<t, t> map = this.l;
        return (map == null || (tVar = map.get(e(str))) == null) ? str : tVar._simpleName;
    }

    public void c(String str) {
        if (!this.c && str != null) {
            if (this.f679s == null) {
                this.f679s = new HashSet<>();
            }
            this.f679s.add(str);
        }
    }

    public void d(b.a aVar, i iVar) {
        if (aVar != null) {
            Object obj = aVar._id;
            if (this.t == null) {
                this.t = new LinkedHashMap<>();
            }
            i put = this.t.put(obj, iVar);
            if (put != null && put.getClass() == iVar.getClass()) {
                String name = obj.getClass().getName();
                throw new IllegalArgumentException("Duplicate injectable value with id '" + obj + "' (of type " + name + ")");
            }
        }
    }

    public final t e(String str) {
        return t.b(str, null);
    }

    public c0 f(Map<String, c0> map, String str) {
        c0 c0Var = map.get(str);
        if (c0Var != null) {
            return c0Var;
        }
        c0 c0Var2 = new c0(this.a, this.g, this.c, t.a(str));
        map.put(str, c0Var2);
        return c0Var2;
    }

    public boolean g(c0 c0Var, List<c0> list) {
        if (list != null) {
            String str = c0Var.p._simpleName;
            int size = list.size();
            for (int i = 0; i < size; i++) {
                if (list.get(i).p._simpleName.equals(str)) {
                    list.set(i, c0Var);
                    return true;
                }
            }
        }
        return false;
    }

    /* JADX WARN: Removed duplicated region for block: B:432:0x0881  */
    /* JADX WARN: Removed duplicated region for block: B:443:0x08b2  */
    /* JADX WARN: Removed duplicated region for block: B:444:0x08b6  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void h() {
        /*
            Method dump skipped, instructions count: 2708
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.c.c0.b0.h():void");
    }

    public void i(String str, Object... objArr) {
        if (objArr.length > 0) {
            str = String.format(str, objArr);
        }
        StringBuilder R = a.R("Problem with definition of ");
        R.append(this.e);
        R.append(": ");
        R.append(str);
        throw new IllegalArgumentException(R.toString());
    }
}
