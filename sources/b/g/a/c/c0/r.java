package b.g.a.c.c0;

import b.g.a.c.c0.t;
import b.g.a.c.h0.a;
import b.g.a.c.h0.k;
import b.g.a.c.i0.d;
import b.g.a.c.j;
import b.g.a.c.l;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;
/* compiled from: BasicClassIntrospector.java */
/* loaded from: classes2.dex */
public class r extends t implements Serializable {
    public static final q k;
    public static final q l;
    public static final q m;
    public static final q n;
    private static final long serialVersionUID = 2;
    public static final Class<?> j = l.class;
    public static final q o = q.e(null, k.L(Object.class), new c(Object.class));

    static {
        k L = k.L(String.class);
        Class<?> cls = d.a;
        k = q.e(null, L, new c(String.class));
        Class cls2 = Boolean.TYPE;
        l = q.e(null, k.L(cls2), new c(cls2));
        Class cls3 = Integer.TYPE;
        m = q.e(null, k.L(cls3), new c(cls3));
        Class cls4 = Long.TYPE;
        n = q.e(null, k.L(cls4), new c(cls4));
    }

    public q a(b.g.a.c.z.l<?> lVar, j jVar) {
        Class<?> cls = jVar._class;
        if (cls.isPrimitive()) {
            if (cls == Integer.TYPE) {
                return m;
            }
            if (cls == Long.TYPE) {
                return n;
            }
            if (cls == Boolean.TYPE) {
                return l;
            }
            return null;
        } else if (d.r(cls)) {
            if (cls == Object.class) {
                return o;
            }
            if (cls == String.class) {
                return k;
            }
            if (cls == Integer.class) {
                return m;
            }
            if (cls == Long.class) {
                return n;
            }
            if (cls == Boolean.class) {
                return l;
            }
            return null;
        } else if (!j.isAssignableFrom(cls)) {
            return null;
        } else {
            Class<?> cls2 = d.a;
            return q.e(lVar, jVar, new c(cls));
        }
    }

    public c b(b.g.a.c.z.l<?> lVar, j jVar, t.a aVar) {
        Class<?> cls = d.a;
        Objects.requireNonNull(jVar);
        if ((jVar instanceof a) && d.i(lVar, jVar._class)) {
            return new c(jVar._class);
        }
        d dVar = new d(lVar, jVar, aVar);
        ArrayList arrayList = new ArrayList(8);
        Class<?> cls2 = jVar._class;
        if (!(cls2 == Object.class)) {
            if (cls2.isInterface()) {
                d.d(jVar, arrayList, false);
            } else {
                d.e(jVar, arrayList, false);
            }
        }
        return new c(jVar, dVar.h, arrayList, dVar.i, dVar.g(arrayList), dVar.f, dVar.d, aVar, lVar._base._typeFactory, dVar.j);
    }
}
