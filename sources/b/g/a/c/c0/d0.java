package b.g.a.c.c0;

import b.g.a.c.c0.t;
import b.g.a.c.h0.b;
import java.io.Serializable;
import java.util.Map;
/* compiled from: SimpleMixInResolver.java */
/* loaded from: classes2.dex */
public class d0 implements t.a, Serializable {
    private static final long serialVersionUID = 1;
    public Map<b, Class<?>> _localMixIns;
    public final t.a _overrides = null;

    public d0(t.a aVar) {
    }

    @Override // b.g.a.c.c0.t.a
    public Class<?> a(Class<?> cls) {
        Map<b, Class<?>> map;
        t.a aVar = this._overrides;
        Class<?> a = aVar == null ? null : aVar.a(cls);
        return (a != null || (map = this._localMixIns) == null) ? a : map.get(new b(cls));
    }

    public boolean b() {
        if (this._localMixIns != null) {
            return true;
        }
        t.a aVar = this._overrides;
        if (aVar == null) {
            return false;
        }
        if (aVar instanceof d0) {
            return ((d0) aVar).b();
        }
        return true;
    }
}
