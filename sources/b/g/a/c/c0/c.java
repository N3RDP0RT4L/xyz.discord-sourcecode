package b.g.a.c.c0;

import b.g.a.c.b;
import b.g.a.c.c0.e0;
import b.g.a.c.c0.h;
import b.g.a.c.c0.k;
import b.g.a.c.c0.t;
import b.g.a.c.h0.m;
import b.g.a.c.h0.n;
import b.g.a.c.i0.d;
import b.g.a.c.j;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
/* compiled from: AnnotatedClass.java */
/* loaded from: classes2.dex */
public final class c extends b implements e0 {
    public static final a j = new a(null, Collections.emptyList(), Collections.emptyList());
    public final j k;
    public final Class<?> l;
    public final m m;
    public final List<j> n;
    public final b o;
    public final n p;
    public final t.a q;
    public final Class<?> r;

    /* renamed from: s  reason: collision with root package name */
    public final boolean f680s;
    public final b.g.a.c.i0.a t;
    public a u;
    public l v;
    public List<g> w;

    /* renamed from: x  reason: collision with root package name */
    public transient Boolean f681x;

    /* compiled from: AnnotatedClass.java */
    /* loaded from: classes2.dex */
    public static final class a {
        public final e a;

        /* renamed from: b  reason: collision with root package name */
        public final List<e> f682b;
        public final List<j> c;

        public a(e eVar, List<e> list, List<j> list2) {
            this.a = eVar;
            this.f682b = list;
            this.c = list2;
        }
    }

    public c(j jVar, Class<?> cls, List<j> list, Class<?> cls2, b.g.a.c.i0.a aVar, m mVar, b bVar, t.a aVar2, n nVar, boolean z2) {
        this.k = jVar;
        this.l = cls;
        this.n = list;
        this.r = cls2;
        this.t = aVar;
        this.m = mVar;
        this.o = bVar;
        this.q = aVar2;
        this.p = nVar;
        this.f680s = z2;
    }

    @Override // b.g.a.c.c0.e0
    public j a(Type type) {
        return this.p.b(null, type, this.m);
    }

    @Override // b.g.a.c.c0.b
    public <A extends Annotation> A b(Class<A> cls) {
        return (A) this.t.a(cls);
    }

    @Override // b.g.a.c.c0.b
    public String c() {
        return this.l.getName();
    }

    @Override // b.g.a.c.c0.b
    public Class<?> d() {
        return this.l;
    }

    @Override // b.g.a.c.c0.b
    public j e() {
        return this.k;
    }

    @Override // b.g.a.c.c0.b
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return d.o(obj, c.class) && ((c) obj).l == this.l;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:157:0x02f3  */
    /* JADX WARN: Removed duplicated region for block: B:158:0x02f5  */
    /* JADX WARN: Removed duplicated region for block: B:165:0x0318  */
    /* JADX WARN: Removed duplicated region for block: B:65:0x0128  */
    /* JADX WARN: Removed duplicated region for block: B:72:0x0140  */
    /* JADX WARN: Removed duplicated region for block: B:73:0x0146  */
    /* JADX WARN: Type inference failed for: r9v2, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r9v3, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r9v4 */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final b.g.a.c.c0.c.a f() {
        /*
            Method dump skipped, instructions count: 870
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.c.c0.c.f():b.g.a.c.c0.c$a");
    }

    public Iterable<g> g() {
        List<g> list = this.w;
        if (list == null) {
            j jVar = this.k;
            if (jVar == null) {
                list = Collections.emptyList();
            } else {
                Map<String, h.a> f = new h(this.o, this.p, this.q, this.f680s).f(this, jVar, null);
                if (f == null) {
                    list = Collections.emptyList();
                } else {
                    ArrayList arrayList = new ArrayList(f.size());
                    for (h.a aVar : f.values()) {
                        arrayList.add(new g(aVar.a, aVar.f686b, aVar.c.b()));
                    }
                    list = arrayList;
                }
            }
            this.w = list;
        }
        return list;
    }

    public Iterable<j> h() {
        boolean z2;
        Class<?> a2;
        l lVar = this.v;
        if (lVar == null) {
            j jVar = this.k;
            if (jVar == null) {
                lVar = new l();
            } else {
                b bVar = this.o;
                t.a aVar = this.q;
                n nVar = this.p;
                List<j> list = this.n;
                Class<?> cls = this.r;
                k kVar = new k(bVar, aVar, this.f680s);
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                kVar.f(this, jVar._class, linkedHashMap, cls);
                Iterator<j> it = list.iterator();
                while (true) {
                    Class<?> cls2 = null;
                    if (!it.hasNext()) {
                        break;
                    }
                    j next = it.next();
                    t.a aVar2 = kVar.d;
                    if (aVar2 != null) {
                        cls2 = aVar2.a(next._class);
                    }
                    kVar.f(new e0.a(nVar, next.j()), next._class, linkedHashMap, cls2);
                }
                t.a aVar3 = kVar.d;
                if (aVar3 == null || (a2 = aVar3.a(Object.class)) == null) {
                    z2 = false;
                } else {
                    kVar.g(this, jVar._class, linkedHashMap, a2);
                    z2 = true;
                }
                if (z2 && kVar.c != null && !linkedHashMap.isEmpty()) {
                    for (Map.Entry entry : linkedHashMap.entrySet()) {
                        y yVar = (y) entry.getKey();
                        if ("hashCode".equals(yVar.f692b) && yVar.c.length == 0) {
                            try {
                                Method declaredMethod = Object.class.getDeclaredMethod(yVar.f692b, new Class[0]);
                                if (declaredMethod != null) {
                                    k.a aVar4 = (k.a) entry.getValue();
                                    aVar4.c = kVar.d(aVar4.c, declaredMethod.getDeclaredAnnotations());
                                    aVar4.f687b = declaredMethod;
                                }
                            } catch (Exception unused) {
                            }
                        }
                    }
                }
                if (linkedHashMap.isEmpty()) {
                    lVar = new l();
                } else {
                    LinkedHashMap linkedHashMap2 = new LinkedHashMap(linkedHashMap.size());
                    for (Map.Entry entry2 : linkedHashMap.entrySet()) {
                        k.a aVar5 = (k.a) entry2.getValue();
                        Method method = aVar5.f687b;
                        j jVar2 = method == null ? null : new j(aVar5.a, method, aVar5.c.b(), null);
                        if (jVar2 != null) {
                            linkedHashMap2.put(entry2.getKey(), jVar2);
                        }
                    }
                    lVar = new l(linkedHashMap2);
                }
            }
            this.v = lVar;
        }
        return lVar;
    }

    @Override // b.g.a.c.c0.b
    public int hashCode() {
        return this.l.getName().hashCode();
    }

    public String toString() {
        return b.d.b.a.a.n(this.l, b.d.b.a.a.R("[AnnotedClass "), "]");
    }

    public c(Class<?> cls) {
        this.k = null;
        this.l = cls;
        this.n = Collections.emptyList();
        this.r = null;
        this.t = o.a;
        this.m = m.l;
        this.o = null;
        this.q = null;
        this.p = null;
        this.f680s = false;
    }
}
