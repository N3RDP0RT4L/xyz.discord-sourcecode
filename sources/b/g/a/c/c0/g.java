package b.g.a.c.c0;

import b.g.a.c.i0.d;
import b.g.a.c.j;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
/* compiled from: AnnotatedField.java */
/* loaded from: classes2.dex */
public final class g extends i implements Serializable {
    private static final long serialVersionUID = 1;
    public a _serialization;
    public final transient Field l;

    /* compiled from: AnnotatedField.java */
    /* loaded from: classes2.dex */
    public static final class a implements Serializable {
        private static final long serialVersionUID = 1;
        public Class<?> clazz;
        public String name;

        public a(Field field) {
            this.clazz = field.getDeclaringClass();
            this.name = field.getName();
        }
    }

    public g(e0 e0Var, Field field, p pVar) {
        super(e0Var, pVar);
        this.l = field;
    }

    @Override // b.g.a.c.c0.b
    public String c() {
        return this.l.getName();
    }

    @Override // b.g.a.c.c0.b
    public Class<?> d() {
        return this.l.getType();
    }

    @Override // b.g.a.c.c0.b
    public j e() {
        return this.j.a(this.l.getGenericType());
    }

    @Override // b.g.a.c.c0.b
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return d.o(obj, g.class) && ((g) obj).l == this.l;
    }

    @Override // b.g.a.c.c0.i
    public Class<?> g() {
        return this.l.getDeclaringClass();
    }

    @Override // b.g.a.c.c0.b
    public int hashCode() {
        return this.l.getName().hashCode();
    }

    @Override // b.g.a.c.c0.i
    public Member i() {
        return this.l;
    }

    @Override // b.g.a.c.c0.i
    public Object j(Object obj) throws IllegalArgumentException {
        try {
            return this.l.get(obj);
        } catch (IllegalAccessException e) {
            StringBuilder R = b.d.b.a.a.R("Failed to getValue() for field ");
            R.append(h());
            R.append(": ");
            R.append(e.getMessage());
            throw new IllegalArgumentException(R.toString(), e);
        }
    }

    @Override // b.g.a.c.c0.i
    public b l(p pVar) {
        return new g(this.j, this.l, pVar);
    }

    public Object readResolve() {
        a aVar = this._serialization;
        Class<?> cls = aVar.clazz;
        try {
            Field declaredField = cls.getDeclaredField(aVar.name);
            if (!declaredField.isAccessible()) {
                d.d(declaredField, false);
            }
            return new g(null, declaredField, null);
        } catch (Exception unused) {
            StringBuilder R = b.d.b.a.a.R("Could not find method '");
            R.append(this._serialization.name);
            R.append("' from Class '");
            R.append(cls.getName());
            throw new IllegalArgumentException(R.toString());
        }
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("[field ");
        R.append(h());
        R.append("]");
        return R.toString();
    }

    public Object writeReplace() {
        return new g(new a(this.l));
    }

    public g(a aVar) {
        super(null, null);
        this.l = null;
        this._serialization = aVar;
    }
}
