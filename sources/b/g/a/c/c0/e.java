package b.g.a.c.c0;

import b.g.a.c.i0.d;
import b.g.a.c.j;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.lang.reflect.Type;
/* compiled from: AnnotatedConstructor.java */
/* loaded from: classes2.dex */
public final class e extends n {
    private static final long serialVersionUID = 1;
    public final Constructor<?> _constructor;
    public a _serialization;

    /* compiled from: AnnotatedConstructor.java */
    /* loaded from: classes2.dex */
    public static final class a implements Serializable {
        private static final long serialVersionUID = 1;
        public Class<?>[] args;
        public Class<?> clazz;

        public a(Constructor<?> constructor) {
            this.clazz = constructor.getDeclaringClass();
            this.args = constructor.getParameterTypes();
        }
    }

    public e(e0 e0Var, Constructor<?> constructor, p pVar, p[] pVarArr) {
        super(e0Var, pVar, pVarArr);
        if (constructor != null) {
            this._constructor = constructor;
            return;
        }
        throw new IllegalArgumentException("Null constructor not allowed");
    }

    @Override // b.g.a.c.c0.b
    public String c() {
        return this._constructor.getName();
    }

    @Override // b.g.a.c.c0.b
    public Class<?> d() {
        return this._constructor.getDeclaringClass();
    }

    @Override // b.g.a.c.c0.b
    public j e() {
        return this.j.a(d());
    }

    @Override // b.g.a.c.c0.b
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return d.o(obj, e.class) && ((e) obj)._constructor == this._constructor;
    }

    @Override // b.g.a.c.c0.i
    public Class<?> g() {
        return this._constructor.getDeclaringClass();
    }

    @Override // b.g.a.c.c0.b
    public int hashCode() {
        return this._constructor.getName().hashCode();
    }

    @Override // b.g.a.c.c0.i
    public Member i() {
        return this._constructor;
    }

    @Override // b.g.a.c.c0.i
    public Object j(Object obj) throws UnsupportedOperationException {
        StringBuilder R = b.d.b.a.a.R("Cannot call getValue() on constructor of ");
        R.append(g().getName());
        throw new UnsupportedOperationException(R.toString());
    }

    @Override // b.g.a.c.c0.i
    public b l(p pVar) {
        return new e(this.j, this._constructor, pVar, this._paramAnnotations);
    }

    @Override // b.g.a.c.c0.n
    public j n(int i) {
        Type[] genericParameterTypes = this._constructor.getGenericParameterTypes();
        if (i >= genericParameterTypes.length) {
            return null;
        }
        return this.j.a(genericParameterTypes[i]);
    }

    public Object readResolve() {
        a aVar = this._serialization;
        Class<?> cls = aVar.clazz;
        try {
            Constructor<?> declaredConstructor = cls.getDeclaredConstructor(aVar.args);
            if (!declaredConstructor.isAccessible()) {
                d.d(declaredConstructor, false);
            }
            return new e(null, declaredConstructor, null, null);
        } catch (Exception unused) {
            StringBuilder R = b.d.b.a.a.R("Could not find constructor with ");
            R.append(this._serialization.args.length);
            R.append(" args from Class '");
            R.append(cls.getName());
            throw new IllegalArgumentException(R.toString());
        }
    }

    public String toString() {
        int length = this._constructor.getParameterTypes().length;
        Object[] objArr = new Object[4];
        objArr[0] = d.u(this._constructor.getDeclaringClass());
        objArr[1] = Integer.valueOf(length);
        objArr[2] = length == 1 ? "" : "s";
        objArr[3] = this.k;
        return String.format("[constructor for %s (%d arg%s), annotations: %s", objArr);
    }

    public Object writeReplace() {
        return new e(new a(this._constructor));
    }

    public e(a aVar) {
        super(null, null, null);
        this._constructor = null;
        this._serialization = aVar;
    }
}
