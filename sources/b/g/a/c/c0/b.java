package b.g.a.c.c0;

import b.g.a.c.j;
import java.lang.annotation.Annotation;
/* compiled from: Annotated.java */
/* loaded from: classes2.dex */
public abstract class b {
    public abstract <A extends Annotation> A b(Class<A> cls);

    public abstract String c();

    public abstract Class<?> d();

    public abstract j e();

    public abstract boolean equals(Object obj);

    public abstract int hashCode();
}
