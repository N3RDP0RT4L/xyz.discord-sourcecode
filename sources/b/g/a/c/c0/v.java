package b.g.a.c.c0;

import b.g.a.a.i;
import b.g.a.a.p;
import b.g.a.c.b;
import b.g.a.c.d;
import b.g.a.c.s;
import b.g.a.c.z.l;
import java.io.Serializable;
/* compiled from: ConcreteBeanPropertyBase.java */
/* loaded from: classes2.dex */
public abstract class v implements d, Serializable {
    private static final long serialVersionUID = 1;
    public final s _metadata;

    public v(s sVar) {
        this._metadata = sVar == null ? s.l : sVar;
    }

    @Override // b.g.a.c.d
    public i.d a(l<?> lVar, Class<?> cls) {
        i member;
        i.d i = lVar.i(cls);
        b e = lVar.e();
        i.d h = (e == null || (member = getMember()) == null) ? null : e.h(member);
        return i == null ? h == null ? d.c : h : h == null ? i : i.k(h);
    }

    @Override // b.g.a.c.d
    public p.b b(l<?> lVar, Class<?> cls) {
        b e = lVar.e();
        i member = getMember();
        if (member == null) {
            return lVar.j(cls);
        }
        p.b g = lVar.g(cls, member.d());
        if (e == null) {
            return g;
        }
        p.b z2 = e.z(member);
        return g == null ? z2 : g.a(z2);
    }

    public v(v vVar) {
        this._metadata = vVar._metadata;
    }
}
