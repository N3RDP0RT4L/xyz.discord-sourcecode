package b.g.a.c.c0;

import b.g.a.c.b;
import b.g.a.c.c0.o;
import b.g.a.c.c0.t;
import b.g.a.c.i0.d;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
/* compiled from: AnnotatedMethodCollector.java */
/* loaded from: classes2.dex */
public class k extends u {
    public final t.a d;
    public final boolean e;

    /* compiled from: AnnotatedMethodCollector.java */
    /* loaded from: classes2.dex */
    public static final class a {
        public e0 a;

        /* renamed from: b  reason: collision with root package name */
        public Method f687b;
        public o c;

        public a(e0 e0Var, Method method, o oVar) {
            this.a = e0Var;
            this.f687b = method;
            this.c = oVar;
        }
    }

    public k(b bVar, t.a aVar, boolean z2) {
        super(bVar);
        this.d = bVar == null ? null : aVar;
        this.e = z2;
    }

    public static boolean h(Method method) {
        return !Modifier.isStatic(method.getModifiers()) && !method.isSynthetic() && !method.isBridge() && method.getParameterTypes().length <= 2;
    }

    public final void f(e0 e0Var, Class<?> cls, Map<y, a> map, Class<?> cls2) {
        Method[] k;
        if (cls2 != null) {
            g(e0Var, cls, map, cls2);
        }
        if (cls != null) {
            for (Method method : d.k(cls)) {
                if (h(method)) {
                    y yVar = new y(method);
                    a aVar = map.get(yVar);
                    if (aVar == null) {
                        map.put(yVar, new a(e0Var, method, this.c == null ? o.a.c : c(method.getDeclaredAnnotations())));
                    } else {
                        if (this.e) {
                            aVar.c = d(aVar.c, method.getDeclaredAnnotations());
                        }
                        Method method2 = aVar.f687b;
                        if (method2 == null) {
                            aVar.f687b = method;
                        } else if (Modifier.isAbstract(method2.getModifiers()) && !Modifier.isAbstract(method.getModifiers())) {
                            aVar.f687b = method;
                            aVar.a = e0Var;
                        }
                    }
                }
            }
        }
    }

    public void g(e0 e0Var, Class<?> cls, Map<y, a> map, Class<?> cls2) {
        List<Class> list;
        Method[] declaredMethods;
        if (this.c != null) {
            Annotation[] annotationArr = d.a;
            if (cls2 == cls || cls2 == Object.class) {
                list = Collections.emptyList();
            } else {
                list = new ArrayList(8);
                d.a(cls2, cls, list, true);
            }
            for (Class cls3 : list) {
                for (Method method : cls3.getDeclaredMethods()) {
                    if (h(method)) {
                        y yVar = new y(method);
                        a aVar = map.get(yVar);
                        Annotation[] declaredAnnotations = method.getDeclaredAnnotations();
                        if (aVar == null) {
                            map.put(yVar, new a(e0Var, null, c(declaredAnnotations)));
                        } else {
                            aVar.c = d(aVar.c, declaredAnnotations);
                        }
                    }
                }
            }
        }
    }
}
