package b.g.a.c.c0;

import b.d.b.a.a;
import b.g.a.a.i0;
import b.g.a.c.i0.d;
import b.g.a.c.t;
/* compiled from: ObjectIdInfo.java */
/* loaded from: classes2.dex */
public class a0 {
    public static final a0 a = new a0(t.k, Object.class, null, false, null);

    /* renamed from: b  reason: collision with root package name */
    public final t f677b;
    public final Class<? extends i0<?>> c;
    public final Class<?> d;
    public final Class<?> e;
    public final boolean f;

    /* JADX WARN: Incorrect type for immutable var: ssa=java.lang.Class<?>, code=java.lang.Class, for r5v0, types: [java.lang.Class<?>] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public a0(b.g.a.c.t r1, java.lang.Class<?> r2, java.lang.Class<? extends b.g.a.a.i0<?>> r3, boolean r4, java.lang.Class r5) {
        /*
            r0 = this;
            r0.<init>()
            r0.f677b = r1
            r0.e = r2
            r0.c = r3
            r0.f = r4
            if (r5 != 0) goto Lf
            java.lang.Class<b.g.a.a.n0> r5 = b.g.a.a.n0.class
        Lf:
            r0.d = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.c.c0.a0.<init>(b.g.a.c.t, java.lang.Class, java.lang.Class, boolean, java.lang.Class):void");
    }

    public String toString() {
        StringBuilder R = a.R("ObjectIdInfo: propName=");
        R.append(this.f677b);
        R.append(", scope=");
        R.append(d.u(this.e));
        R.append(", generatorType=");
        R.append(d.u(this.c));
        R.append(", alwaysAsId=");
        R.append(this.f);
        return R.toString();
    }
}
