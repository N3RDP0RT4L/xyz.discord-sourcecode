package b.g.a.c.c0;

import b.g.a.c.h0.m;
import b.g.a.c.h0.n;
import b.g.a.c.j;
import java.lang.reflect.Type;
/* compiled from: TypeResolutionContext.java */
/* loaded from: classes2.dex */
public interface e0 {

    /* compiled from: TypeResolutionContext.java */
    /* loaded from: classes2.dex */
    public static class a implements e0 {
        public final n j;
        public final m k;

        public a(n nVar, m mVar) {
            this.j = nVar;
            this.k = mVar;
        }

        @Override // b.g.a.c.c0.e0
        public j a(Type type) {
            return this.j.b(null, type, this.k);
        }
    }

    /* compiled from: TypeResolutionContext.java */
    /* loaded from: classes2.dex */
    public static class b implements e0 {
        public final n j;

        public b(n nVar) {
            this.j = nVar;
        }

        @Override // b.g.a.c.c0.e0
        public j a(Type type) {
            return this.j.b(null, type, n.l);
        }
    }

    j a(Type type);
}
