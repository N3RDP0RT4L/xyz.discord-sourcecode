package b.g.a.c.c0;

import b.g.a.c.i0.a;
import java.lang.annotation.Annotation;
import java.util.HashMap;
/* compiled from: AnnotationMap.java */
/* loaded from: classes2.dex */
public final class p implements a {
    public HashMap<Class<?>, Annotation> j;

    public p() {
    }

    public static p c(p pVar, p pVar2) {
        HashMap<Class<?>, Annotation> hashMap;
        HashMap<Class<?>, Annotation> hashMap2;
        if (pVar == null || (hashMap = pVar.j) == null || hashMap.isEmpty()) {
            return pVar2;
        }
        if (pVar2 == null || (hashMap2 = pVar2.j) == null || hashMap2.isEmpty()) {
            return pVar;
        }
        HashMap hashMap3 = new HashMap();
        for (Annotation annotation : pVar2.j.values()) {
            hashMap3.put(annotation.annotationType(), annotation);
        }
        for (Annotation annotation2 : pVar.j.values()) {
            hashMap3.put(annotation2.annotationType(), annotation2);
        }
        return new p(hashMap3);
    }

    @Override // b.g.a.c.i0.a
    public <A extends Annotation> A a(Class<A> cls) {
        HashMap<Class<?>, Annotation> hashMap = this.j;
        if (hashMap == null) {
            return null;
        }
        return (A) hashMap.get(cls);
    }

    public boolean b(Class<? extends Annotation>[] clsArr) {
        if (this.j != null) {
            for (Class<? extends Annotation> cls : clsArr) {
                if (this.j.containsKey(cls)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override // b.g.a.c.i0.a
    public int size() {
        HashMap<Class<?>, Annotation> hashMap = this.j;
        if (hashMap == null) {
            return 0;
        }
        return hashMap.size();
    }

    public String toString() {
        HashMap<Class<?>, Annotation> hashMap = this.j;
        return hashMap == null ? "[null]" : hashMap.toString();
    }

    public p(HashMap<Class<?>, Annotation> hashMap) {
        this.j = hashMap;
    }
}
