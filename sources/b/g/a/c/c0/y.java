package b.g.a.c.c0;

import b.d.b.a.a;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
/* compiled from: MemberKey.java */
/* loaded from: classes2.dex */
public final class y {
    public static final Class<?>[] a = new Class[0];

    /* renamed from: b  reason: collision with root package name */
    public final String f692b;
    public final Class<?>[] c;

    public y(Method method) {
        String name = method.getName();
        Class<?>[] parameterTypes = method.getParameterTypes();
        this.f692b = name;
        this.c = parameterTypes == null ? a : parameterTypes;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != y.class) {
            return false;
        }
        y yVar = (y) obj;
        if (!this.f692b.equals(yVar.f692b)) {
            return false;
        }
        Class<?>[] clsArr = yVar.c;
        int length = this.c.length;
        if (clsArr.length != length) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            if (clsArr[i] != this.c[i]) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return this.f692b.hashCode() + this.c.length;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f692b);
        sb.append("(");
        return a.A(sb, this.c.length, "-args)");
    }

    public y(Constructor<?> constructor) {
        Class<?>[] parameterTypes = constructor.getParameterTypes();
        this.f692b = "";
        this.c = parameterTypes == null ? a : parameterTypes;
    }
}
