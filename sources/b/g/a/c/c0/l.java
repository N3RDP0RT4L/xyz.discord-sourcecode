package b.g.a.c.c0;

import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
/* compiled from: AnnotatedMethodMap.java */
/* loaded from: classes2.dex */
public final class l implements Iterable<j> {
    public Map<y, j> j;

    public l() {
    }

    @Override // java.lang.Iterable
    public Iterator<j> iterator() {
        Map<y, j> map = this.j;
        if (map == null) {
            return Collections.emptyIterator();
        }
        return map.values().iterator();
    }

    public l(Map<y, j> map) {
        this.j = map;
    }
}
