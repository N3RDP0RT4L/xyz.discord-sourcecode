package b.g.a.c.c0;

import b.g.a.a.p;
import b.g.a.c.b;
import b.g.a.c.h0.n;
import b.g.a.c.j;
import b.g.a.c.s;
import b.g.a.c.t;
import b.g.a.c.z.l;
import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
/* compiled from: POJOPropertyBuilder.java */
/* loaded from: classes2.dex */
public class c0 extends s implements Comparable<c0> {
    public static final b.a k = new b.a(1, "");
    public final boolean l;
    public final l<?> m;
    public final b.g.a.c.b n;
    public final t o;
    public final t p;
    public d<g> q;
    public d<m> r;

    /* renamed from: s  reason: collision with root package name */
    public d<j> f683s;
    public d<j> t;
    public transient s u;
    public transient b.a v;

    /* compiled from: POJOPropertyBuilder.java */
    /* loaded from: classes2.dex */
    public class a implements e<Class<?>[]> {
        public a() {
        }

        @Override // b.g.a.c.c0.c0.e
        public Class<?>[] a(i iVar) {
            return c0.this.n.R(iVar);
        }
    }

    /* compiled from: POJOPropertyBuilder.java */
    /* loaded from: classes2.dex */
    public class b implements e<b.a> {
        public b() {
        }

        @Override // b.g.a.c.c0.c0.e
        public b.a a(i iVar) {
            return c0.this.n.D(iVar);
        }
    }

    /* compiled from: POJOPropertyBuilder.java */
    /* loaded from: classes2.dex */
    public class c implements e<Boolean> {
        public c() {
        }

        @Override // b.g.a.c.c0.c0.e
        public Boolean a(i iVar) {
            return c0.this.n.d0(iVar);
        }
    }

    /* compiled from: POJOPropertyBuilder.java */
    /* loaded from: classes2.dex */
    public static final class d<T> {
        public final T a;

        /* renamed from: b  reason: collision with root package name */
        public final d<T> f684b;
        public final t c;
        public final boolean d;
        public final boolean e;
        public final boolean f;

        public d(T t, d<T> dVar, t tVar, boolean z2, boolean z3, boolean z4) {
            this.a = t;
            this.f684b = dVar;
            t tVar2 = (tVar == null || tVar.d()) ? null : tVar;
            this.c = tVar2;
            if (z2) {
                if (tVar2 == null) {
                    throw new IllegalArgumentException("Cannot pass true for 'explName' if name is null/empty");
                } else if (!tVar.c()) {
                    z2 = false;
                }
            }
            this.d = z2;
            this.e = z3;
            this.f = z4;
        }

        public d<T> a(d<T> dVar) {
            d<T> dVar2 = this.f684b;
            if (dVar2 == null) {
                return c(dVar);
            }
            return c(dVar2.a(dVar));
        }

        public d<T> b() {
            d<T> dVar = this.f684b;
            if (dVar == null) {
                return this;
            }
            d<T> b2 = dVar.b();
            if (this.c != null) {
                if (b2.c == null) {
                    return c(null);
                }
                return c(b2);
            } else if (b2.c != null) {
                return b2;
            } else {
                boolean z2 = this.e;
                if (z2 == b2.e) {
                    return c(b2);
                }
                return z2 ? c(null) : b2;
            }
        }

        public d<T> c(d<T> dVar) {
            return dVar == this.f684b ? this : new d<>(this.a, dVar, this.c, this.d, this.e, this.f);
        }

        public d<T> d() {
            d<T> d;
            if (this.f) {
                d<T> dVar = this.f684b;
                if (dVar == null) {
                    return null;
                }
                return dVar.d();
            }
            d<T> dVar2 = this.f684b;
            return (dVar2 == null || (d = dVar2.d()) == this.f684b) ? this : c(d);
        }

        public d<T> e() {
            return this.f684b == null ? this : new d<>(this.a, null, this.c, this.d, this.e, this.f);
        }

        public d<T> f() {
            d<T> dVar = this.f684b;
            d<T> f = dVar == null ? null : dVar.f();
            return this.e ? c(f) : f;
        }

        public String toString() {
            String format = String.format("%s[visible=%b,ignore=%b,explicitName=%b]", this.a.toString(), Boolean.valueOf(this.e), Boolean.valueOf(this.f), Boolean.valueOf(this.d));
            if (this.f684b == null) {
                return format;
            }
            StringBuilder V = b.d.b.a.a.V(format, ", ");
            V.append(this.f684b.toString());
            return V.toString();
        }
    }

    /* compiled from: POJOPropertyBuilder.java */
    /* loaded from: classes2.dex */
    public interface e<T> {
        T a(i iVar);
    }

    public c0(l<?> lVar, b.g.a.c.b bVar, boolean z2, t tVar) {
        this.m = lVar;
        this.n = bVar;
        this.p = tVar;
        this.o = tVar;
        this.l = z2;
    }

    public static <T> d<T> N(d<T> dVar, d<T> dVar2) {
        if (dVar == null) {
            return dVar2;
        }
        if (dVar2 == null) {
            return dVar;
        }
        d<T> dVar3 = dVar.f684b;
        if (dVar3 == null) {
            return dVar.c(dVar2);
        }
        return dVar.c(dVar3.a(dVar2));
    }

    public final <T extends i> d<T> A(d<T> dVar, p pVar) {
        i iVar = (i) dVar.a.l(pVar);
        d<T> dVar2 = dVar.f684b;
        if (dVar2 != null) {
            dVar = dVar.c(A(dVar2, pVar));
        }
        return iVar == dVar.a ? dVar : new d<>(iVar, dVar.f684b, dVar.c, dVar.d, dVar.e, dVar.f);
    }

    public final void B(Collection<t> collection, Map<t, c0> map, d<?> dVar) {
        for (d dVar2 = dVar; dVar2 != null; dVar2 = dVar2.f684b) {
            t tVar = dVar2.c;
            if (dVar2.d && tVar != null) {
                c0 c0Var = map.get(tVar);
                if (c0Var == null) {
                    c0Var = new c0(this.m, this.n, this.l, this.p, tVar);
                    map.put(tVar, c0Var);
                }
                if (dVar == this.q) {
                    c0Var.q = dVar2.c(c0Var.q);
                } else if (dVar == this.f683s) {
                    c0Var.f683s = dVar2.c(c0Var.f683s);
                } else if (dVar == this.t) {
                    c0Var.t = dVar2.c(c0Var.t);
                } else if (dVar == this.r) {
                    c0Var.r = dVar2.c(c0Var.r);
                } else {
                    throw new IllegalStateException("Internal error: mismatched accessors, property: " + this);
                }
            } else if (dVar2.e) {
                StringBuilder R = b.d.b.a.a.R("Conflicting/ambiguous property name definitions (implicit name ");
                t tVar2 = this.o;
                Annotation[] annotationArr = b.g.a.c.i0.d.a;
                R.append(tVar2 == null ? "[null]" : b.g.a.c.i0.d.c(tVar2._simpleName));
                R.append("): found multiple explicit names: ");
                R.append(collection);
                R.append(", but also implicit accessor: ");
                R.append(dVar2);
                throw new IllegalStateException(R.toString());
            }
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:0:?, code lost:
        r2 = r2;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.util.Set<b.g.a.c.t> C(b.g.a.c.c0.c0.d<? extends b.g.a.c.c0.i> r2, java.util.Set<b.g.a.c.t> r3) {
        /*
            r1 = this;
        L0:
            if (r2 == 0) goto L1a
            boolean r0 = r2.d
            if (r0 == 0) goto L17
            b.g.a.c.t r0 = r2.c
            if (r0 != 0) goto Lb
            goto L17
        Lb:
            if (r3 != 0) goto L12
            java.util.HashSet r3 = new java.util.HashSet
            r3.<init>()
        L12:
            b.g.a.c.t r0 = r2.c
            r3.add(r0)
        L17:
            b.g.a.c.c0.c0$d<T> r2 = r2.f684b
            goto L0
        L1a:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.c.c0.c0.C(b.g.a.c.c0.c0$d, java.util.Set):java.util.Set");
    }

    public final <T extends i> p D(d<T> dVar) {
        p pVar = dVar.a.k;
        d<T> dVar2 = dVar.f684b;
        return dVar2 != null ? p.c(pVar, D(dVar2)) : pVar;
    }

    public int E(j jVar) {
        String c2 = jVar.c();
        if (!c2.startsWith("get") || c2.length() <= 3) {
            return (!c2.startsWith("is") || c2.length() <= 2) ? 3 : 2;
        }
        return 1;
    }

    public final p F(int i, d<? extends i>... dVarArr) {
        d<? extends i> dVar = dVarArr[i];
        p pVar = ((i) dVar.a).k;
        d<? extends i> dVar2 = dVar.f684b;
        if (dVar2 != null) {
            pVar = p.c(pVar, D(dVar2));
        }
        do {
            i++;
            if (i >= dVarArr.length) {
                return pVar;
            }
        } while (dVarArr[i] == null);
        return p.c(pVar, F(i, dVarArr));
    }

    public final <T> d<T> G(d<T> dVar) {
        return dVar == null ? dVar : dVar.d();
    }

    public final <T> d<T> H(d<T> dVar) {
        return dVar == null ? dVar : dVar.f();
    }

    public int I(j jVar) {
        String c2 = jVar.c();
        return (!c2.startsWith("set") || c2.length() <= 3) ? 2 : 1;
    }

    public final <T> d<T> J(d<T> dVar) {
        return dVar == null ? dVar : dVar.b();
    }

    public void K(c0 c0Var) {
        this.q = N(this.q, c0Var.q);
        this.r = N(this.r, c0Var.r);
        this.f683s = N(this.f683s, c0Var.f683s);
        this.t = N(this.t, c0Var.t);
    }

    public Set<t> L() {
        Set<t> C = C(this.r, C(this.t, C(this.f683s, C(this.q, null))));
        return C == null ? Collections.emptySet() : C;
    }

    public <T> T M(e<T> eVar) {
        d<g> dVar;
        d<j> dVar2;
        T t = null;
        if (this.n == null) {
            return null;
        }
        if (this.l) {
            d<j> dVar3 = this.f683s;
            if (dVar3 != null) {
                t = eVar.a(dVar3.a);
            }
        } else {
            d<m> dVar4 = this.r;
            if (dVar4 != null) {
                t = eVar.a(dVar4.a);
            }
            if (t == null && (dVar2 = this.t) != null) {
                t = eVar.a(dVar2.a);
            }
        }
        return (t != null || (dVar = this.q) == null) ? t : eVar.a(dVar.a);
    }

    @Override // java.lang.Comparable
    public int compareTo(c0 c0Var) {
        c0 c0Var2 = c0Var;
        if (this.r != null) {
            if (c0Var2.r == null) {
                return -1;
            }
        } else if (c0Var2.r != null) {
            return 1;
        }
        return p().compareTo(c0Var2.p());
    }

    @Override // b.g.a.c.c0.s
    public boolean f() {
        return (this.r == null && this.t == null && this.q == null) ? false : true;
    }

    @Override // b.g.a.c.c0.s
    public p.b g() {
        i j = j();
        b.g.a.c.b bVar = this.n;
        p.b z2 = bVar == null ? null : bVar.z(j);
        if (z2 != null) {
            return z2;
        }
        p.b bVar2 = p.b.j;
        return p.b.j;
    }

    @Override // b.g.a.c.c0.s
    public b.a h() {
        b.a aVar = this.v;
        if (aVar == null) {
            b.a aVar2 = (b.a) M(new b());
            this.v = aVar2 == null ? k : aVar2;
            return aVar2;
        } else if (aVar == k) {
            return null;
        } else {
            return aVar;
        }
    }

    @Override // b.g.a.c.c0.s
    public Class<?>[] i() {
        return (Class[]) M(new a());
    }

    @Override // b.g.a.c.c0.s
    public m k() {
        d dVar = this.r;
        if (dVar == null) {
            return null;
        }
        do {
            T t = dVar.a;
            if (((m) t)._owner instanceof b.g.a.c.c0.e) {
                return (m) t;
            }
            dVar = dVar.f684b;
        } while (dVar != null);
        return this.r.a;
    }

    @Override // b.g.a.c.c0.s
    public g l() {
        d<g> dVar = this.q;
        if (dVar == null) {
            return null;
        }
        g gVar = dVar.a;
        for (d dVar2 = dVar.f684b; dVar2 != null; dVar2 = dVar2.f684b) {
            g gVar2 = (g) dVar2.a;
            Class<?> g = gVar.g();
            Class<?> g2 = gVar2.g();
            if (g != g2) {
                if (g.isAssignableFrom(g2)) {
                    gVar = gVar2;
                } else if (g2.isAssignableFrom(g)) {
                }
            }
            StringBuilder R = b.d.b.a.a.R("Multiple fields representing property \"");
            R.append(p());
            R.append("\": ");
            R.append(gVar.h());
            R.append(" vs ");
            R.append(gVar2.h());
            throw new IllegalArgumentException(R.toString());
        }
        return gVar;
    }

    @Override // b.g.a.c.c0.s
    public t m() {
        return this.o;
    }

    @Override // b.g.a.c.c0.s
    public j n() {
        d<j> dVar = this.f683s;
        if (dVar == null) {
            return null;
        }
        d<j> dVar2 = dVar.f684b;
        if (dVar2 == null) {
            return dVar.a;
        }
        for (d<j> dVar3 = dVar2; dVar3 != null; dVar3 = dVar3.f684b) {
            Class<?> g = dVar.a.g();
            Class<?> g2 = dVar3.a.g();
            if (g != g2) {
                if (!g.isAssignableFrom(g2)) {
                    if (g2.isAssignableFrom(g)) {
                        continue;
                    }
                }
                dVar = dVar3;
            }
            int E = E(dVar3.a);
            int E2 = E(dVar.a);
            if (E != E2) {
                if (E >= E2) {
                }
                dVar = dVar3;
            } else {
                StringBuilder R = b.d.b.a.a.R("Conflicting getter definitions for property \"");
                R.append(p());
                R.append("\": ");
                R.append(dVar.a.h());
                R.append(" vs ");
                R.append(dVar3.a.h());
                throw new IllegalArgumentException(R.toString());
            }
        }
        this.f683s = dVar.e();
        return dVar.a;
    }

    /* JADX WARN: Removed duplicated region for block: B:26:0x0044  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x004a  */
    @Override // b.g.a.c.c0.s
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.g.a.c.s o() {
        /*
            Method dump skipped, instructions count: 358
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.c.c0.c0.o():b.g.a.c.s");
    }

    @Override // b.g.a.c.c0.s
    public String p() {
        t tVar = this.o;
        if (tVar == null) {
            return null;
        }
        return tVar._simpleName;
    }

    @Override // b.g.a.c.c0.s
    public i q() {
        if (this.l) {
            return j();
        }
        i k2 = k();
        if (k2 == null && (k2 = s()) == null) {
            k2 = l();
        }
        return k2 == null ? j() : k2;
    }

    @Override // b.g.a.c.c0.s
    public Class<?> r() {
        j jVar;
        if (this.l) {
            b.g.a.c.c0.b n = n();
            if (n == null && (n = l()) == null) {
                jVar = n.k();
            } else {
                jVar = n.e();
            }
        } else {
            b.g.a.c.c0.b k2 = k();
            if (k2 == null) {
                j s2 = s();
                if (s2 != null) {
                    jVar = s2.n(0);
                } else {
                    k2 = l();
                }
            }
            if (k2 == null && (k2 = n()) == null) {
                jVar = n.k();
            } else {
                jVar = k2.e();
            }
        }
        return jVar._class;
    }

    @Override // b.g.a.c.c0.s
    public j s() {
        d<j> dVar = this.t;
        if (dVar == null) {
            return null;
        }
        d<j> dVar2 = dVar.f684b;
        if (dVar2 == null) {
            return dVar.a;
        }
        for (d<j> dVar3 = dVar2; dVar3 != null; dVar3 = dVar3.f684b) {
            Class<?> g = dVar.a.g();
            Class<?> g2 = dVar3.a.g();
            if (g != g2) {
                if (!g.isAssignableFrom(g2)) {
                    if (g2.isAssignableFrom(g)) {
                        continue;
                    }
                }
                dVar = dVar3;
            }
            j jVar = dVar3.a;
            j jVar2 = dVar.a;
            int I = I(jVar);
            int I2 = I(jVar2);
            if (I != I2) {
                if (I >= I2) {
                }
                dVar = dVar3;
            } else {
                b.g.a.c.b bVar = this.n;
                if (bVar != null) {
                    j f02 = bVar.f0(this.m, jVar2, jVar);
                    if (f02 != jVar2) {
                        if (f02 != jVar) {
                        }
                        dVar = dVar3;
                    } else {
                        continue;
                    }
                }
                throw new IllegalArgumentException(String.format("Conflicting setter definitions for property \"%s\": %s vs %s", p(), dVar.a.h(), dVar3.a.h()));
            }
        }
        this.t = dVar.e();
        return dVar.a;
    }

    @Override // b.g.a.c.c0.s
    public t t() {
        b.g.a.c.b bVar;
        if (q() == null || (bVar = this.n) == null) {
            return null;
        }
        Objects.requireNonNull(bVar);
        return null;
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("[Property '");
        R.append(this.o);
        R.append("'; ctors: ");
        R.append(this.r);
        R.append(", field(s): ");
        R.append(this.q);
        R.append(", getter(s): ");
        R.append(this.f683s);
        R.append(", setter(s): ");
        R.append(this.t);
        R.append("]");
        return R.toString();
    }

    @Override // b.g.a.c.c0.s
    public boolean u() {
        return x(this.q) || x(this.f683s) || x(this.t) || w(this.r);
    }

    @Override // b.g.a.c.c0.s
    public boolean v() {
        Boolean bool = (Boolean) M(new c());
        return bool != null && bool.booleanValue();
    }

    public final <T> boolean w(d<T> dVar) {
        while (dVar != null) {
            if (dVar.c != null && dVar.d) {
                return true;
            }
            dVar = dVar.f684b;
        }
        return false;
    }

    public final <T> boolean x(d<T> dVar) {
        while (dVar != null) {
            t tVar = dVar.c;
            if (tVar != null && tVar.c()) {
                return true;
            }
            dVar = dVar.f684b;
        }
        return false;
    }

    public final <T> boolean y(d<T> dVar) {
        while (dVar != null) {
            if (dVar.f) {
                return true;
            }
            dVar = dVar.f684b;
        }
        return false;
    }

    public final <T> boolean z(d<T> dVar) {
        while (dVar != null) {
            if (dVar.e) {
                return true;
            }
            dVar = dVar.f684b;
        }
        return false;
    }

    public c0(l<?> lVar, b.g.a.c.b bVar, boolean z2, t tVar, t tVar2) {
        this.m = lVar;
        this.n = bVar;
        this.p = tVar;
        this.o = tVar2;
        this.l = z2;
    }

    public c0(c0 c0Var, t tVar) {
        this.m = c0Var.m;
        this.n = c0Var.n;
        this.p = c0Var.p;
        this.o = tVar;
        this.q = c0Var.q;
        this.r = c0Var.r;
        this.f683s = c0Var.f683s;
        this.t = c0Var.t;
        this.l = c0Var.l;
    }
}
