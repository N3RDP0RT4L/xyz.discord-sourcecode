package b.g.a.c.c0;

import b.g.a.c.i0.d;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
/* compiled from: AnnotatedMethod.java */
/* loaded from: classes2.dex */
public final class j extends n implements Serializable {
    private static final long serialVersionUID = 1;
    public Class<?>[] _paramClasses;
    public a _serialization;
    public final transient Method l;

    /* compiled from: AnnotatedMethod.java */
    /* loaded from: classes2.dex */
    public static final class a implements Serializable {
        private static final long serialVersionUID = 1;
        public Class<?>[] args;
        public Class<?> clazz;
        public String name;

        public a(Method method) {
            this.clazz = method.getDeclaringClass();
            this.name = method.getName();
            this.args = method.getParameterTypes();
        }
    }

    public j(e0 e0Var, Method method, p pVar, p[] pVarArr) {
        super(e0Var, pVar, pVarArr);
        if (method != null) {
            this.l = method;
            return;
        }
        throw new IllegalArgumentException("Cannot construct AnnotatedMethod with null Method");
    }

    @Override // b.g.a.c.c0.b
    public String c() {
        return this.l.getName();
    }

    @Override // b.g.a.c.c0.b
    public Class<?> d() {
        return this.l.getReturnType();
    }

    @Override // b.g.a.c.c0.b
    public b.g.a.c.j e() {
        return this.j.a(this.l.getGenericReturnType());
    }

    @Override // b.g.a.c.c0.b
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return d.o(obj, j.class) && ((j) obj).l == this.l;
    }

    @Override // b.g.a.c.c0.i
    public Class<?> g() {
        return this.l.getDeclaringClass();
    }

    @Override // b.g.a.c.c0.i
    public String h() {
        String h = super.h();
        int o = o();
        if (o == 0) {
            return b.d.b.a.a.v(h, "()");
        }
        if (o != 1) {
            return String.format("%s(%d params)", super.h(), Integer.valueOf(o()));
        }
        StringBuilder V = b.d.b.a.a.V(h, "(");
        V.append(p(0).getName());
        V.append(")");
        return V.toString();
    }

    @Override // b.g.a.c.c0.b
    public int hashCode() {
        return this.l.getName().hashCode();
    }

    @Override // b.g.a.c.c0.i
    public Member i() {
        return this.l;
    }

    @Override // b.g.a.c.c0.i
    public Object j(Object obj) throws IllegalArgumentException {
        try {
            return this.l.invoke(obj, null);
        } catch (IllegalAccessException | InvocationTargetException e) {
            StringBuilder R = b.d.b.a.a.R("Failed to getValue() with method ");
            R.append(h());
            R.append(": ");
            R.append(e.getMessage());
            throw new IllegalArgumentException(R.toString(), e);
        }
    }

    @Override // b.g.a.c.c0.i
    public b l(p pVar) {
        return new j(this.j, this.l, pVar, this._paramAnnotations);
    }

    @Override // b.g.a.c.c0.n
    public b.g.a.c.j n(int i) {
        Type[] genericParameterTypes = this.l.getGenericParameterTypes();
        if (i >= genericParameterTypes.length) {
            return null;
        }
        return this.j.a(genericParameterTypes[i]);
    }

    public int o() {
        if (this._paramClasses == null) {
            this._paramClasses = this.l.getParameterTypes();
        }
        return this._paramClasses.length;
    }

    public Class<?> p(int i) {
        if (this._paramClasses == null) {
            this._paramClasses = this.l.getParameterTypes();
        }
        Class<?>[] clsArr = this._paramClasses;
        if (i >= clsArr.length) {
            return null;
        }
        return clsArr[i];
    }

    public Object readResolve() {
        a aVar = this._serialization;
        Class<?> cls = aVar.clazz;
        try {
            Method declaredMethod = cls.getDeclaredMethod(aVar.name, aVar.args);
            if (!declaredMethod.isAccessible()) {
                d.d(declaredMethod, false);
            }
            return new j(null, declaredMethod, null, null);
        } catch (Exception unused) {
            StringBuilder R = b.d.b.a.a.R("Could not find method '");
            R.append(this._serialization.name);
            R.append("' from Class '");
            R.append(cls.getName());
            throw new IllegalArgumentException(R.toString());
        }
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("[method ");
        R.append(h());
        R.append("]");
        return R.toString();
    }

    public Object writeReplace() {
        return new j(new a(this.l));
    }

    public j(a aVar) {
        super(null, null, null);
        this.l = null;
        this._serialization = aVar;
    }
}
