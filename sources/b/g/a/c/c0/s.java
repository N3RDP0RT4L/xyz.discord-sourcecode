package b.g.a.c.c0;

import b.g.a.a.p;
import b.g.a.c.b;
import b.g.a.c.t;
/* compiled from: BeanPropertyDefinition.java */
/* loaded from: classes2.dex */
public abstract class s {
    public static final p.b j = p.b.j;

    static {
        p.b bVar = p.b.j;
    }

    public boolean f() {
        b k = k();
        if (k == null && (k = s()) == null) {
            k = l();
        }
        return k != null;
    }

    public abstract p.b g();

    public b.a h() {
        return null;
    }

    public Class<?>[] i() {
        return null;
    }

    public i j() {
        j n = n();
        return n == null ? l() : n;
    }

    public abstract m k();

    public abstract g l();

    public abstract t m();

    public abstract j n();

    public abstract b.g.a.c.s o();

    public abstract String p();

    public abstract i q();

    public abstract Class<?> r();

    public abstract j s();

    public abstract t t();

    public abstract boolean u();

    public boolean v() {
        return false;
    }
}
