package b.g.a.c.c0;

import b.g.a.c.i0.d;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Member;
import java.util.HashMap;
/* compiled from: AnnotatedMember.java */
/* loaded from: classes2.dex */
public abstract class i extends b implements Serializable {
    private static final long serialVersionUID = 1;
    public final transient e0 j;
    public final transient p k;

    public i(e0 e0Var, p pVar) {
        this.j = e0Var;
        this.k = pVar;
    }

    @Override // b.g.a.c.c0.b
    public final <A extends Annotation> A b(Class<A> cls) {
        HashMap<Class<?>, Annotation> hashMap;
        p pVar = this.k;
        if (pVar == null || (hashMap = pVar.j) == null) {
            return null;
        }
        return (A) hashMap.get(cls);
    }

    public final void f(boolean z2) {
        Member i = i();
        if (i != null) {
            d.d(i, z2);
        }
    }

    public abstract Class<?> g();

    public String h() {
        return g().getName() + "#" + c();
    }

    public abstract Member i();

    public abstract Object j(Object obj) throws UnsupportedOperationException, IllegalArgumentException;

    public final boolean k(Class<?> cls) {
        HashMap<Class<?>, Annotation> hashMap;
        p pVar = this.k;
        if (pVar == null || (hashMap = pVar.j) == null) {
            return false;
        }
        return hashMap.containsKey(cls);
    }

    public abstract b l(p pVar);
}
