package b.g.a.c.c0;

import b.g.a.c.b;
import b.g.a.c.c0.o;
import b.g.a.c.c0.t;
import b.g.a.c.h0.m;
import b.g.a.c.i0.a;
import b.g.a.c.j;
import b.g.a.c.z.l;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
/* compiled from: AnnotatedClassResolver.java */
/* loaded from: classes2.dex */
public class d {
    public static final Class<?> a = List.class;

    /* renamed from: b  reason: collision with root package name */
    public static final Class<?> f685b = Map.class;
    public final l<?> c;
    public final b d;
    public final t.a e;
    public final m f;
    public final j g;
    public final Class<?> h;
    public final Class<?> i;
    public final boolean j;

    public d(l<?> lVar, j jVar, t.a aVar) {
        this.c = lVar;
        this.g = jVar;
        Class<?> cls = jVar._class;
        this.h = cls;
        this.e = aVar;
        this.f = jVar.j();
        Class<?> cls2 = null;
        b e = lVar.p() ? lVar.e() : null;
        this.d = e;
        this.i = aVar != null ? aVar.a(cls) : cls2;
        this.j = e != null && (!b.g.a.c.i0.d.r(cls) || !jVar.v());
    }

    public static void d(j jVar, List<j> list, boolean z2) {
        Class<?> cls = jVar._class;
        if (z2) {
            if (!f(list, cls)) {
                list.add(jVar);
                if (cls == a || cls == f685b) {
                    return;
                }
            } else {
                return;
            }
        }
        for (j jVar2 : jVar.n()) {
            d(jVar2, list, true);
        }
    }

    public static void e(j jVar, List<j> list, boolean z2) {
        Class<?> cls = jVar._class;
        if (!(cls == Object.class || cls == Enum.class)) {
            if (z2) {
                if (!f(list, cls)) {
                    list.add(jVar);
                } else {
                    return;
                }
            }
            for (j jVar2 : jVar.n()) {
                d(jVar2, list, true);
            }
            j q = jVar.q();
            if (q != null) {
                e(q, list, true);
            }
        }
    }

    public static boolean f(List<j> list, Class<?> cls) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (list.get(i)._class == cls) {
                return true;
            }
        }
        return false;
    }

    public static c h(l<?> lVar, Class<?> cls) {
        if (cls.isArray() && i(lVar, cls)) {
            return new c(cls);
        }
        d dVar = new d(lVar, cls, lVar);
        List<j> emptyList = Collections.emptyList();
        return new c(null, cls, emptyList, dVar.i, dVar.g(emptyList), dVar.f, dVar.d, lVar, lVar._base._typeFactory, dVar.j);
    }

    public static boolean i(l<?> lVar, Class<?> cls) {
        return lVar == null || ((b.g.a.c.z.m) lVar)._mixIns.a(cls) == null;
    }

    public final o a(o oVar, Annotation[] annotationArr) {
        if (annotationArr != null) {
            for (Annotation annotation : annotationArr) {
                if (!oVar.d(annotation)) {
                    oVar = oVar.a(annotation);
                    if (this.d.b0(annotation)) {
                        oVar = c(oVar, annotation);
                    }
                }
            }
        }
        return oVar;
    }

    public final o b(o oVar, Class<?> cls, Class<?> cls2) {
        if (cls2 != null) {
            oVar = a(oVar, b.g.a.c.i0.d.i(cls2));
            Iterator it = ((ArrayList) b.g.a.c.i0.d.j(cls2, cls, false)).iterator();
            while (it.hasNext()) {
                oVar = a(oVar, b.g.a.c.i0.d.i((Class) it.next()));
            }
        }
        return oVar;
    }

    public final o c(o oVar, Annotation annotation) {
        Annotation[] i;
        for (Annotation annotation2 : b.g.a.c.i0.d.i(annotation.annotationType())) {
            if (!(annotation2 instanceof Target) && !(annotation2 instanceof Retention) && !oVar.d(annotation2)) {
                oVar = oVar.a(annotation2);
                if (this.d.b0(annotation2)) {
                    oVar = c(oVar, annotation2);
                }
            }
        }
        return oVar;
    }

    public final a g(List<j> list) {
        if (this.d == null) {
            return o.a;
        }
        t.a aVar = this.e;
        boolean z2 = aVar != null && (!(aVar instanceof d0) || ((d0) aVar).b());
        if (!(z2 || this.j)) {
            return o.a;
        }
        o oVar = o.a.c;
        Class<?> cls = this.i;
        if (cls != null) {
            oVar = b(oVar, this.h, cls);
        }
        if (this.j) {
            oVar = a(oVar, b.g.a.c.i0.d.i(this.h));
        }
        for (j jVar : list) {
            if (z2) {
                Class<?> cls2 = jVar._class;
                oVar = b(oVar, cls2, this.e.a(cls2));
            }
            if (this.j) {
                oVar = a(oVar, b.g.a.c.i0.d.i(jVar._class));
            }
        }
        if (z2) {
            oVar = b(oVar, Object.class, this.e.a(Object.class));
        }
        return oVar.c();
    }

    public d(l<?> lVar, Class<?> cls, t.a aVar) {
        this.c = lVar;
        Class<?> cls2 = null;
        this.g = null;
        this.h = cls;
        this.e = aVar;
        this.f = m.l;
        if (lVar == null) {
            this.d = null;
            this.i = null;
        } else {
            this.d = lVar.p() ? lVar.e() : null;
            this.i = aVar != null ? aVar.a(cls) : cls2;
        }
        this.j = this.d != null;
    }
}
