package b.g.a.c.c0;

import b.d.b.a.a;
import b.g.a.c.i0.d;
import b.g.a.c.j;
import java.lang.reflect.Member;
/* compiled from: AnnotatedParameter.java */
/* loaded from: classes2.dex */
public final class m extends i {
    private static final long serialVersionUID = 1;
    public final int _index;
    public final n _owner;
    public final j _type;

    public m(n nVar, j jVar, e0 e0Var, p pVar, int i) {
        super(e0Var, pVar);
        this._owner = nVar;
        this._type = jVar;
        this._index = i;
    }

    @Override // b.g.a.c.c0.b
    public String c() {
        return "";
    }

    @Override // b.g.a.c.c0.b
    public Class<?> d() {
        return this._type._class;
    }

    @Override // b.g.a.c.c0.b
    public j e() {
        return this._type;
    }

    @Override // b.g.a.c.c0.b
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!d.o(obj, m.class)) {
            return false;
        }
        m mVar = (m) obj;
        return mVar._owner.equals(this._owner) && mVar._index == this._index;
    }

    @Override // b.g.a.c.c0.i
    public Class<?> g() {
        return this._owner.g();
    }

    @Override // b.g.a.c.c0.b
    public int hashCode() {
        return this._owner.hashCode() + this._index;
    }

    @Override // b.g.a.c.c0.i
    public Member i() {
        return this._owner.i();
    }

    @Override // b.g.a.c.c0.i
    public Object j(Object obj) throws UnsupportedOperationException {
        StringBuilder R = a.R("Cannot call getValue() on constructor parameter of ");
        R.append(g().getName());
        throw new UnsupportedOperationException(R.toString());
    }

    @Override // b.g.a.c.c0.i
    public b l(p pVar) {
        if (pVar == this.k) {
            return this;
        }
        n nVar = this._owner;
        int i = this._index;
        nVar._paramAnnotations[i] = pVar;
        return nVar.m(i);
    }

    public String toString() {
        StringBuilder R = a.R("[parameter #");
        R.append(this._index);
        R.append(", annotations: ");
        R.append(this.k);
        R.append("]");
        return R.toString();
    }
}
