package b.g.a.c.c0;

import b.g.a.c.b;
import b.g.a.c.c0.e0;
import b.g.a.c.c0.o;
import b.g.a.c.c0.t;
import b.g.a.c.h0.n;
import b.g.a.c.i0.d;
import b.g.a.c.j;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
/* compiled from: AnnotatedFieldCollector.java */
/* loaded from: classes2.dex */
public class h extends u {
    public final n d;
    public final t.a e;
    public final boolean f;

    /* compiled from: AnnotatedFieldCollector.java */
    /* loaded from: classes2.dex */
    public static final class a {
        public final e0 a;

        /* renamed from: b  reason: collision with root package name */
        public final Field f686b;
        public o c = o.a.c;

        public a(e0 e0Var, Field field) {
            this.a = e0Var;
            this.f686b = field;
        }
    }

    public h(b bVar, n nVar, t.a aVar, boolean z2) {
        super(bVar);
        this.d = nVar;
        this.e = bVar == null ? null : aVar;
        this.f = z2;
    }

    public final Map<String, a> f(e0 e0Var, j jVar, Map<String, a> map) {
        Field[] declaredFields;
        t.a aVar;
        Class<?> a2;
        Field[] declaredFields2;
        a aVar2;
        j q = jVar.q();
        if (q == null) {
            return map;
        }
        Class<?> cls = jVar._class;
        Map<String, a> f = f(new e0.a(this.d, q.j()), q, map);
        for (Field field : cls.getDeclaredFields()) {
            if (g(field)) {
                if (f == null) {
                    f = new LinkedHashMap<>();
                }
                a aVar3 = new a(e0Var, field);
                if (this.f) {
                    aVar3.c = b(aVar3.c, field.getDeclaredAnnotations());
                }
                f.put(field.getName(), aVar3);
            }
        }
        if (!(f == null || (aVar = this.e) == null || (a2 = aVar.a(cls)) == null)) {
            Iterator it = ((ArrayList) d.j(a2, cls, true)).iterator();
            while (it.hasNext()) {
                for (Field field2 : ((Class) it.next()).getDeclaredFields()) {
                    if (g(field2) && (aVar2 = f.get(field2.getName())) != null) {
                        aVar2.c = b(aVar2.c, field2.getDeclaredAnnotations());
                    }
                }
            }
        }
        return f;
    }

    public final boolean g(Field field) {
        return !field.isSynthetic() && !Modifier.isStatic(field.getModifiers());
    }
}
