package b.g.a.c.g0;

import b.g.a.c.c;
import b.g.a.c.c0.i;
import b.g.a.c.g0.t.j;
import b.g.a.c.n;
import b.g.a.c.p;
import b.g.a.c.v;
import java.util.Collections;
import java.util.List;
/* compiled from: BeanSerializerBuilder.java */
/* loaded from: classes2.dex */
public class e {
    public static final c[] a = new c[0];

    /* renamed from: b  reason: collision with root package name */
    public final c f697b;
    public v c;
    public List<c> d = Collections.emptyList();
    public c[] e;
    public a f;
    public Object g;
    public i h;
    public j i;

    public e(c cVar) {
        this.f697b = cVar;
    }

    public n<?> a() {
        c[] cVarArr;
        if (this.h != null && this.c.q(p.CAN_OVERRIDE_ACCESS_MODIFIERS)) {
            this.h.f(this.c.q(p.OVERRIDE_PUBLIC_ACCESS_MODIFIERS));
        }
        a aVar = this.f;
        if (aVar != null) {
            aVar.f696b.f(this.c.q(p.OVERRIDE_PUBLIC_ACCESS_MODIFIERS));
        }
        List<c> list = this.d;
        if (list != null && !list.isEmpty()) {
            List<c> list2 = this.d;
            cVarArr = (c[]) list2.toArray(new c[list2.size()]);
            if (this.c.q(p.CAN_OVERRIDE_ACCESS_MODIFIERS)) {
                for (c cVar : cVarArr) {
                    cVar._member.f(this.c.q(p.OVERRIDE_PUBLIC_ACCESS_MODIFIERS));
                }
            }
        } else if (this.f == null && this.i == null) {
            return null;
        } else {
            cVarArr = a;
        }
        c[] cVarArr2 = this.e;
        if (cVarArr2 == null || cVarArr2.length == this.d.size()) {
            return new d(this.f697b.a, this, cVarArr, this.e);
        }
        throw new IllegalStateException(String.format("Mismatch between `properties` size (%d), `filteredProperties` (%s): should have as many (or `null` for latter)", Integer.valueOf(this.d.size()), Integer.valueOf(this.e.length)));
    }
}
