package b.g.a.c.g0.u;

import b.g.a.a.i;
import b.g.a.c.d;
import b.g.a.c.e0.g;
import b.g.a.c.g0.i;
import b.g.a.c.n;
import b.g.a.c.x;
import com.fasterxml.jackson.databind.JsonMappingException;
import java.io.IOException;
/* compiled from: BooleanSerializer.java */
@b.g.a.c.y.a
/* loaded from: classes2.dex */
public final class e extends p0<Object> implements i {
    private static final long serialVersionUID = 1;
    public final boolean _forPrimitive;

    /* compiled from: BooleanSerializer.java */
    /* loaded from: classes2.dex */
    public static final class a extends p0<Object> implements i {
        private static final long serialVersionUID = 1;
        public final boolean _forPrimitive;

        public a(boolean z2) {
            super(z2 ? Boolean.TYPE : Boolean.class, false);
            this._forPrimitive = z2;
        }

        @Override // b.g.a.c.g0.i
        public n<?> a(x xVar, d dVar) throws JsonMappingException {
            i.d l = l(xVar, dVar, Boolean.class);
            return (l == null || l.e().f()) ? this : new e(this._forPrimitive);
        }

        @Override // b.g.a.c.n
        public void f(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
            dVar.H(!Boolean.FALSE.equals(obj));
        }

        @Override // b.g.a.c.g0.u.p0, b.g.a.c.n
        public final void g(Object obj, b.g.a.b.d dVar, x xVar, g gVar) throws IOException {
            dVar.s(Boolean.TRUE.equals(obj));
        }
    }

    public e(boolean z2) {
        super(z2 ? Boolean.TYPE : Boolean.class, false);
        this._forPrimitive = z2;
    }

    @Override // b.g.a.c.g0.i
    public n<?> a(x xVar, d dVar) throws JsonMappingException {
        i.d l = l(xVar, dVar, Boolean.class);
        return (l == null || !l.e().f()) ? this : new a(this._forPrimitive);
    }

    @Override // b.g.a.c.n
    public void f(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
        dVar.s(Boolean.TRUE.equals(obj));
    }

    @Override // b.g.a.c.g0.u.p0, b.g.a.c.n
    public final void g(Object obj, b.g.a.b.d dVar, x xVar, g gVar) throws IOException {
        dVar.s(Boolean.TRUE.equals(obj));
    }
}
