package b.g.a.c.g0.u;

import b.g.a.a.i;
import b.g.a.c.b;
import b.g.a.c.c0.i;
import b.g.a.c.d;
import b.g.a.c.g0.m;
import b.g.a.c.i0.e;
import b.g.a.c.j;
import b.g.a.c.n;
import b.g.a.c.w;
import b.g.a.c.x;
import b.g.a.c.z.i;
import com.fasterxml.jackson.databind.JsonMappingException;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Objects;
/* compiled from: StdSerializer.java */
/* loaded from: classes2.dex */
public abstract class q0<T> extends n<T> implements Serializable {
    public static final Object j = new Object();
    private static final long serialVersionUID = 1;
    public final Class<T> _handledType;

    public q0(Class<T> cls) {
        this._handledType = cls;
    }

    public static final boolean j(Object obj, Object obj2) {
        return (obj == null || obj2 == null) ? false : true;
    }

    @Override // b.g.a.c.n
    public Class<T> c() {
        return this._handledType;
    }

    public n<?> k(x xVar, d dVar, n<?> nVar) throws JsonMappingException {
        n<?> nVar2;
        i member;
        Object G;
        Object obj = j;
        Map map = (Map) xVar.w(obj);
        if (map == null) {
            map = new IdentityHashMap();
            i.a aVar = (i.a) xVar.l;
            Map<Object, Object> map2 = aVar.l;
            if (map2 == null) {
                HashMap hashMap = new HashMap();
                hashMap.put(obj, map == null ? i.a.k : map);
                aVar = new i.a(aVar._shared, hashMap);
            } else {
                map2.put(obj, map);
            }
            xVar.l = aVar;
        } else if (map.get(dVar) != null) {
            return nVar;
        }
        map.put(dVar, Boolean.TRUE);
        try {
            b v = xVar.v();
            if (!j(v, dVar) || (member = dVar.getMember()) == null || (G = v.G(member)) == null) {
                nVar2 = nVar;
            } else {
                e<Object, Object> c = xVar.c(dVar.getMember(), G);
                j a = c.a(xVar.d());
                nVar2 = new j0(c, a, (nVar != null || a.y()) ? nVar : xVar.s(a));
            }
            return nVar2 != null ? xVar.z(nVar2, dVar) : nVar;
        } finally {
            map.remove(dVar);
        }
    }

    public i.d l(x xVar, d dVar, Class<?> cls) {
        if (dVar != null) {
            return dVar.a(xVar._config, cls);
        }
        return xVar._config.i(cls);
    }

    public m m(x xVar, Object obj, Object obj2) throws JsonMappingException {
        Objects.requireNonNull(xVar._config);
        Class<T> cls = this._handledType;
        xVar.g(cls, "Cannot resolve PropertyFilter with id '" + obj + "'; no FilterProvider configured");
        throw null;
    }

    public void n(x xVar, Throwable th, Object obj, int i) throws IOException {
        while ((th instanceof InvocationTargetException) && th.getCause() != null) {
            th = th.getCause();
        }
        b.g.a.c.i0.d.w(th);
        boolean z2 = xVar == null || xVar.D(w.WRAP_EXCEPTIONS);
        if (th instanceof IOException) {
            if (!z2 || !(th instanceof JsonMappingException)) {
                throw ((IOException) th);
            }
        } else if (!z2) {
            b.g.a.c.i0.d.x(th);
        }
        throw JsonMappingException.f(th, new JsonMappingException.a(obj, i));
    }

    public void o(x xVar, Throwable th, Object obj, String str) throws IOException {
        while ((th instanceof InvocationTargetException) && th.getCause() != null) {
            th = th.getCause();
        }
        b.g.a.c.i0.d.w(th);
        boolean z2 = xVar == null || xVar.D(w.WRAP_EXCEPTIONS);
        if (th instanceof IOException) {
            if (!z2 || !(th instanceof JsonMappingException)) {
                throw ((IOException) th);
            }
        } else if (!z2) {
            b.g.a.c.i0.d.x(th);
        }
        int i = JsonMappingException.j;
        throw JsonMappingException.f(th, new JsonMappingException.a(obj, str));
    }

    public q0(j jVar) {
        this._handledType = (Class<T>) jVar._class;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public q0(Class<?> cls, boolean z2) {
        this._handledType = cls;
    }

    public q0(q0<?> q0Var) {
        this._handledType = (Class<T>) q0Var._handledType;
    }
}
