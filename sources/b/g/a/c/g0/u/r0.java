package b.g.a.c.g0.u;

import b.g.a.b.d;
import b.g.a.c.e0.g;
import b.g.a.c.x;
import b.g.a.c.y.a;
import java.io.IOException;
/* compiled from: StringSerializer.java */
@a
/* loaded from: classes2.dex */
public final class r0 extends p0<Object> {
    private static final long serialVersionUID = 1;

    public r0() {
        super(String.class, false);
    }

    @Override // b.g.a.c.n
    public boolean d(x xVar, Object obj) {
        return ((String) obj).isEmpty();
    }

    @Override // b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        dVar.j0((String) obj);
    }

    @Override // b.g.a.c.g0.u.p0, b.g.a.c.n
    public final void g(Object obj, d dVar, x xVar, g gVar) throws IOException {
        dVar.j0((String) obj);
    }
}
