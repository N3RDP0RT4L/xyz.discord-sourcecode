package b.g.a.c.g0.u;

import b.g.a.b.d;
import b.g.a.c.e0.g;
import b.g.a.c.g0.h;
import b.g.a.c.g0.t.l;
import b.g.a.c.n;
import b.g.a.c.w;
import b.g.a.c.x;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
/* compiled from: CollectionSerializer.java */
/* loaded from: classes2.dex */
public class j extends b<Collection<?>> {
    private static final long serialVersionUID = 1;

    public j(b.g.a.c.j jVar, boolean z2, g gVar, n<Object> nVar) {
        super(Collection.class, jVar, z2, gVar, nVar);
    }

    @Override // b.g.a.c.n
    public boolean d(x xVar, Object obj) {
        return ((Collection) obj).isEmpty();
    }

    @Override // b.g.a.c.g0.u.b, b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        Collection<?> collection = (Collection) obj;
        int size = collection.size();
        if (size != 1 || ((this._unwrapSingle != null || !xVar.D(w.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) && this._unwrapSingle != Boolean.TRUE)) {
            dVar.b0(collection, size);
            s(collection, dVar, xVar);
            dVar.t();
            return;
        }
        s(collection, dVar, xVar);
    }

    @Override // b.g.a.c.g0.h
    public h<?> p(g gVar) {
        return new j(this, this._property, gVar, this._elementSerializer, this._unwrapSingle);
    }

    @Override // b.g.a.c.g0.h
    public boolean q(Object obj) {
        return ((Collection) obj).size() == 1;
    }

    @Override // b.g.a.c.g0.u.b
    public b<Collection<?>> t(b.g.a.c.d dVar, g gVar, n nVar, Boolean bool) {
        return new j(this, dVar, gVar, nVar, bool);
    }

    /* renamed from: u */
    public void s(Collection<?> collection, d dVar, x xVar) throws IOException {
        n<Object> nVar;
        dVar.e(collection);
        n<Object> nVar2 = this._elementSerializer;
        int i = 0;
        if (nVar2 != null) {
            Iterator<?> it = collection.iterator();
            if (it.hasNext()) {
                g gVar = this._valueTypeSerializer;
                do {
                    Object next = it.next();
                    if (next == null) {
                        try {
                            xVar.l(dVar);
                        } catch (Exception e) {
                            n(xVar, e, collection, i);
                            throw null;
                        }
                    } else if (gVar == null) {
                        nVar2.f(next, dVar, xVar);
                    } else {
                        nVar2.g(next, dVar, xVar, gVar);
                    }
                    i++;
                } while (it.hasNext());
                return;
            }
            return;
        }
        Iterator<?> it2 = collection.iterator();
        if (it2.hasNext()) {
            l lVar = this._dynamicSerializers;
            g gVar2 = this._valueTypeSerializer;
            do {
                try {
                    Object next2 = it2.next();
                    if (next2 == null) {
                        xVar.l(dVar);
                    } else {
                        Class<?> cls = next2.getClass();
                        n<Object> c = lVar.c(cls);
                        if (c == null) {
                            if (this._elementType.r()) {
                                l.d a = lVar.a(xVar.k(this._elementType, cls), xVar, this._property);
                                l lVar2 = a.f704b;
                                if (lVar != lVar2) {
                                    this._dynamicSerializers = lVar2;
                                }
                                nVar = a.a;
                            } else {
                                nVar = r(lVar, cls, xVar);
                            }
                            c = nVar;
                            lVar = this._dynamicSerializers;
                        }
                        if (gVar2 == null) {
                            c.f(next2, dVar, xVar);
                        } else {
                            c.g(next2, dVar, xVar, gVar2);
                        }
                    }
                    i++;
                } catch (Exception e2) {
                    n(xVar, e2, collection, i);
                    throw null;
                }
            } while (it2.hasNext());
        }
    }

    public j(j jVar, b.g.a.c.d dVar, g gVar, n<?> nVar, Boolean bool) {
        super(jVar, dVar, gVar, nVar, bool);
    }
}
