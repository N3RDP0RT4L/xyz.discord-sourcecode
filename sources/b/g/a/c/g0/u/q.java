package b.g.a.c.g0.u;

import b.d.b.a.a;
import b.g.a.b.d;
import b.g.a.b.h;
import b.g.a.b.s.b;
import b.g.a.c.e0.g;
import b.g.a.c.x;
import java.io.IOException;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
/* compiled from: InetSocketAddressSerializer.java */
/* loaded from: classes2.dex */
public class q extends p0<InetSocketAddress> {
    public q() {
        super(InetSocketAddress.class);
    }

    @Override // b.g.a.c.n
    public /* bridge */ /* synthetic */ void f(Object obj, d dVar, x xVar) throws IOException {
        p((InetSocketAddress) obj, dVar);
    }

    @Override // b.g.a.c.g0.u.p0, b.g.a.c.n
    public void g(Object obj, d dVar, x xVar, g gVar) throws IOException {
        InetSocketAddress inetSocketAddress = (InetSocketAddress) obj;
        b d = gVar.d(inetSocketAddress, h.VALUE_STRING);
        d.f671b = InetSocketAddress.class;
        b e = gVar.e(dVar, d);
        p(inetSocketAddress, dVar);
        gVar.f(dVar, e);
    }

    public void p(InetSocketAddress inetSocketAddress, d dVar) throws IOException {
        String str;
        InetAddress address = inetSocketAddress.getAddress();
        String hostName = address == null ? inetSocketAddress.getHostName() : address.toString().trim();
        int indexOf = hostName.indexOf(47);
        if (indexOf >= 0) {
            if (indexOf == 0) {
                if (address instanceof Inet6Address) {
                    StringBuilder R = a.R("[");
                    R.append(hostName.substring(1));
                    R.append("]");
                    str = R.toString();
                } else {
                    str = hostName.substring(1);
                }
                hostName = str;
            } else {
                hostName = hostName.substring(0, indexOf);
            }
        }
        StringBuilder V = a.V(hostName, ":");
        V.append(inetSocketAddress.getPort());
        dVar.j0(V.toString());
    }
}
