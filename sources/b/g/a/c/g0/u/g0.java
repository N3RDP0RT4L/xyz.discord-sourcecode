package b.g.a.c.g0.u;

import b.g.a.b.d;
import b.g.a.c.e0.g;
import b.g.a.c.m;
import b.g.a.c.x;
import b.g.a.c.y.a;
import java.io.IOException;
import java.util.Objects;
/* compiled from: SerializableSerializer.java */
@a
/* loaded from: classes2.dex */
public class g0 extends q0<m> {
    public static final g0 k = new g0();

    public g0() {
        super(m.class);
    }

    @Override // b.g.a.c.n
    public boolean d(x xVar, Object obj) {
        m mVar = (m) obj;
        if (!(mVar instanceof m.a)) {
            return false;
        }
        Objects.requireNonNull((m.a) mVar);
        return false;
    }

    @Override // b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        ((m) obj).c(dVar, xVar);
    }

    @Override // b.g.a.c.n
    public void g(Object obj, d dVar, x xVar, g gVar) throws IOException {
        ((m) obj).d(dVar, xVar, gVar);
    }
}
