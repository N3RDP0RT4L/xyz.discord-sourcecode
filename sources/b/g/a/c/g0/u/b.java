package b.g.a.c.g0.u;

import b.g.a.c.d;
import b.g.a.c.e0.g;
import b.g.a.c.g0.h;
import b.g.a.c.g0.i;
import b.g.a.c.g0.t.l;
import b.g.a.c.j;
import b.g.a.c.n;
import b.g.a.c.w;
import b.g.a.c.x;
import com.fasterxml.jackson.databind.JsonMappingException;
import java.io.IOException;
/* compiled from: AsArraySerializerBase.java */
/* loaded from: classes2.dex */
public abstract class b<T> extends h<T> implements i {
    public l _dynamicSerializers;
    public final n<Object> _elementSerializer;
    public final j _elementType;
    public final d _property;
    public final boolean _staticTyping;
    public final Boolean _unwrapSingle;
    public final g _valueTypeSerializer;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public b(Class<?> cls, j jVar, boolean z2, g gVar, n<Object> nVar) {
        super(cls, false);
        boolean z3 = false;
        this._elementType = jVar;
        if (z2 || (jVar != null && jVar.x())) {
            z3 = true;
        }
        this._staticTyping = z3;
        this._valueTypeSerializer = gVar;
        this._property = null;
        this._elementSerializer = nVar;
        this._dynamicSerializers = l.b.f702b;
        this._unwrapSingle = null;
    }

    /* JADX WARN: Removed duplicated region for block: B:15:0x0029  */
    /* JADX WARN: Removed duplicated region for block: B:17:0x0031  */
    @Override // b.g.a.c.g0.i
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.g.a.c.n<?> a(b.g.a.c.x r6, b.g.a.c.d r7) throws com.fasterxml.jackson.databind.JsonMappingException {
        /*
            r5 = this;
            b.g.a.c.e0.g r0 = r5._valueTypeSerializer
            if (r0 == 0) goto L8
            b.g.a.c.e0.g r0 = r0.a(r7)
        L8:
            r1 = 0
            if (r7 == 0) goto L20
            b.g.a.c.b r2 = r6.v()
            b.g.a.c.c0.i r3 = r7.getMember()
            if (r3 == 0) goto L20
            java.lang.Object r2 = r2.c(r3)
            if (r2 == 0) goto L20
            b.g.a.c.n r2 = r6.H(r3, r2)
            goto L21
        L20:
            r2 = r1
        L21:
            java.lang.Class<T> r3 = r5._handledType
            b.g.a.a.i$d r3 = r5.l(r6, r7, r3)
            if (r3 == 0) goto L2f
            b.g.a.a.i$a r1 = b.g.a.a.i.a.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED
            java.lang.Boolean r1 = r3.b(r1)
        L2f:
            if (r2 != 0) goto L33
            b.g.a.c.n<java.lang.Object> r2 = r5._elementSerializer
        L33:
            b.g.a.c.n r2 = r5.k(r6, r7, r2)
            if (r2 != 0) goto L4d
            b.g.a.c.j r3 = r5._elementType
            if (r3 == 0) goto L4d
            boolean r4 = r5._staticTyping
            if (r4 == 0) goto L4d
            boolean r3 = r3.y()
            if (r3 != 0) goto L4d
            b.g.a.c.j r2 = r5._elementType
            b.g.a.c.n r2 = r6.m(r2, r7)
        L4d:
            b.g.a.c.n<java.lang.Object> r6 = r5._elementSerializer
            if (r2 != r6) goto L63
            b.g.a.c.d r6 = r5._property
            if (r7 != r6) goto L63
            b.g.a.c.e0.g r6 = r5._valueTypeSerializer
            if (r6 != r0) goto L63
            java.lang.Boolean r6 = r5._unwrapSingle
            boolean r6 = java.util.Objects.equals(r6, r1)
            if (r6 != 0) goto L62
            goto L63
        L62:
            return r5
        L63:
            b.g.a.c.g0.u.b r6 = r5.t(r7, r0, r2, r1)
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.c.g0.u.b.a(b.g.a.c.x, b.g.a.c.d):b.g.a.c.n");
    }

    @Override // b.g.a.c.n
    public void f(T t, b.g.a.b.d dVar, x xVar) throws IOException {
        if (!xVar.D(w.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED) || !q(t)) {
            dVar.X(t);
            s(t, dVar, xVar);
            dVar.t();
            return;
        }
        s(t, dVar, xVar);
    }

    @Override // b.g.a.c.n
    public void g(T t, b.g.a.b.d dVar, x xVar, g gVar) throws IOException {
        b.g.a.b.s.b e = gVar.e(dVar, gVar.d(t, b.g.a.b.h.START_ARRAY));
        dVar.e(t);
        s(t, dVar, xVar);
        gVar.f(dVar, e);
    }

    public final n<Object> r(l lVar, Class<?> cls, x xVar) throws JsonMappingException {
        n<Object> n = xVar.n(cls, this._property);
        l b2 = lVar.b(cls, n);
        if (lVar != b2) {
            this._dynamicSerializers = b2;
        }
        return n;
    }

    public abstract void s(T t, b.g.a.b.d dVar, x xVar) throws IOException;

    public abstract b<T> t(d dVar, g gVar, n<?> nVar, Boolean bool);

    public b(b<?> bVar, d dVar, g gVar, n<?> nVar, Boolean bool) {
        super(bVar);
        this._elementType = bVar._elementType;
        this._staticTyping = bVar._staticTyping;
        this._valueTypeSerializer = gVar;
        this._property = dVar;
        this._elementSerializer = nVar;
        this._dynamicSerializers = l.b.f702b;
        this._unwrapSingle = bool;
    }
}
