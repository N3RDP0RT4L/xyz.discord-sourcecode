package b.g.a.c.g0.u;

import b.g.a.b.d;
import b.g.a.b.f;
import b.g.a.b.h;
import b.g.a.b.s.b;
import b.g.a.c.e0.g;
import b.g.a.c.y.a;
import java.io.IOException;
/* compiled from: NumberSerializers.java */
@a
/* loaded from: classes2.dex */
public class x extends w<Object> {
    public x(Class<?> cls) {
        super(cls, f.b.DOUBLE, "number");
    }

    @Override // b.g.a.c.n
    public void f(Object obj, d dVar, b.g.a.c.x xVar) throws IOException {
        dVar.C(((Double) obj).doubleValue());
    }

    @Override // b.g.a.c.g0.u.p0, b.g.a.c.n
    public void g(Object obj, d dVar, b.g.a.c.x xVar, g gVar) throws IOException {
        Double d = (Double) obj;
        double doubleValue = d.doubleValue();
        if (Double.isNaN(doubleValue) || Double.isInfinite(doubleValue)) {
            b e = gVar.e(dVar, gVar.d(obj, h.VALUE_NUMBER_FLOAT));
            dVar.C(d.doubleValue());
            gVar.f(dVar, e);
            return;
        }
        dVar.C(d.doubleValue());
    }
}
