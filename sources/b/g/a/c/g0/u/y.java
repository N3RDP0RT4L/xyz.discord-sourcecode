package b.g.a.c.g0.u;

import b.g.a.b.d;
import b.g.a.b.f;
import b.g.a.c.x;
import b.g.a.c.y.a;
import java.io.IOException;
/* compiled from: NumberSerializers.java */
@a
/* loaded from: classes2.dex */
public class y extends w<Object> {
    public static final y k = new y();

    public y() {
        super(Float.class, f.b.FLOAT, "number");
    }

    @Override // b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        dVar.D(((Float) obj).floatValue());
    }
}
