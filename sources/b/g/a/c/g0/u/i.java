package b.g.a.c.g0.u;

import b.g.a.b.d;
import b.g.a.c.x;
import java.io.IOException;
/* compiled from: ClassSerializer.java */
/* loaded from: classes2.dex */
public class i extends p0<Class<?>> {
    public i() {
        super(Class.class, false);
    }

    @Override // b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        dVar.j0(((Class) obj).getName());
    }
}
