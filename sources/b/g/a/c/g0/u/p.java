package b.g.a.c.g0.u;

import b.g.a.b.d;
import b.g.a.b.h;
import b.g.a.b.s.b;
import b.g.a.c.e0.g;
import b.g.a.c.g0.i;
import b.g.a.c.x;
import java.io.IOException;
import java.net.InetAddress;
/* compiled from: InetAddressSerializer.java */
/* loaded from: classes2.dex */
public class p extends p0<InetAddress> implements i {
    public final boolean _asNumeric;

    public p() {
        super(InetAddress.class);
        this._asNumeric = false;
    }

    /* JADX WARN: Removed duplicated region for block: B:12:0x001d  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0023 A[RETURN] */
    @Override // b.g.a.c.g0.i
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.g.a.c.n<?> a(b.g.a.c.x r2, b.g.a.c.d r3) throws com.fasterxml.jackson.databind.JsonMappingException {
        /*
            r1 = this;
            java.lang.Class<T> r0 = r1._handledType
            b.g.a.a.i$d r2 = r1.l(r2, r3, r0)
            if (r2 == 0) goto L18
            b.g.a.a.i$c r2 = r2.e()
            boolean r3 = r2.f()
            if (r3 != 0) goto L16
            b.g.a.a.i$c r3 = b.g.a.a.i.c.ARRAY
            if (r2 != r3) goto L18
        L16:
            r2 = 1
            goto L19
        L18:
            r2 = 0
        L19:
            boolean r3 = r1._asNumeric
            if (r2 == r3) goto L23
            b.g.a.c.g0.u.p r3 = new b.g.a.c.g0.u.p
            r3.<init>(r2)
            return r3
        L23:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.c.g0.u.p.a(b.g.a.c.x, b.g.a.c.d):b.g.a.c.n");
    }

    @Override // b.g.a.c.n
    public /* bridge */ /* synthetic */ void f(Object obj, d dVar, x xVar) throws IOException {
        p((InetAddress) obj, dVar);
    }

    @Override // b.g.a.c.g0.u.p0, b.g.a.c.n
    public void g(Object obj, d dVar, x xVar, g gVar) throws IOException {
        InetAddress inetAddress = (InetAddress) obj;
        b d = gVar.d(inetAddress, h.VALUE_STRING);
        d.f671b = InetAddress.class;
        b e = gVar.e(dVar, d);
        p(inetAddress, dVar);
        gVar.f(dVar, e);
    }

    public void p(InetAddress inetAddress, d dVar) throws IOException {
        String str;
        if (this._asNumeric) {
            str = inetAddress.getHostAddress();
        } else {
            str = inetAddress.toString().trim();
            int indexOf = str.indexOf(47);
            if (indexOf >= 0) {
                if (indexOf == 0) {
                    str = str.substring(1);
                } else {
                    str = str.substring(0, indexOf);
                }
            }
        }
        dVar.j0(str);
    }

    public p(boolean z2) {
        super(InetAddress.class);
        this._asNumeric = z2;
    }
}
