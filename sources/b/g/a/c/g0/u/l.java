package b.g.a.c.g0.u;

import b.g.a.a.i;
import b.g.a.c.d;
import b.g.a.c.g0.i;
import b.g.a.c.i0.r;
import b.g.a.c.n;
import b.g.a.c.w;
import b.g.a.c.x;
import b.g.a.c.z.a;
import com.fasterxml.jackson.databind.JsonMappingException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicReference;
/* compiled from: DateTimeSerializerBase.java */
/* loaded from: classes2.dex */
public abstract class l<T> extends p0<T> implements i {
    public final DateFormat _customFormat;
    public final AtomicReference<DateFormat> _reusedCustomFormat;
    public final Boolean _useTimestamp;

    public l(Class<T> cls, Boolean bool, DateFormat dateFormat) {
        super(cls);
        this._useTimestamp = bool;
        this._customFormat = dateFormat;
        this._reusedCustomFormat = dateFormat == null ? null : new AtomicReference<>();
    }

    @Override // b.g.a.c.g0.i
    public n<?> a(x xVar, d dVar) throws JsonMappingException {
        SimpleDateFormat simpleDateFormat;
        Locale locale;
        TimeZone timeZone;
        i.d l = l(xVar, dVar, this._handledType);
        if (l == null) {
            return this;
        }
        i.c e = l.e();
        if (e.f()) {
            return r(Boolean.TRUE, null);
        }
        if (l.h()) {
            if (l.g()) {
                locale = l.c();
            } else {
                locale = xVar._config._base._locale;
            }
            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(l.d(), locale);
            if (l.j()) {
                timeZone = l.f();
            } else {
                timeZone = xVar._config._base._timeZone;
                if (timeZone == null) {
                    timeZone = a.j;
                }
            }
            simpleDateFormat2.setTimeZone(timeZone);
            return r(Boolean.FALSE, simpleDateFormat2);
        }
        boolean g = l.g();
        boolean j = l.j();
        boolean z2 = true;
        boolean z3 = e == i.c.STRING;
        if (!g && !j && !z3) {
            return this;
        }
        DateFormat dateFormat = xVar._config._base._dateFormat;
        if (dateFormat instanceof r) {
            r rVar = (r) dateFormat;
            if (l.g()) {
                rVar = rVar.l(l.c());
            }
            if (l.j()) {
                rVar = rVar.m(l.f());
            }
            return r(Boolean.FALSE, rVar);
        }
        if (!(dateFormat instanceof SimpleDateFormat)) {
            xVar.g(this._handledType, String.format("Configured `DateFormat` (%s) not a `SimpleDateFormat`; cannot configure `Locale` or `TimeZone`", dateFormat.getClass().getName()));
        }
        SimpleDateFormat simpleDateFormat3 = (SimpleDateFormat) dateFormat;
        if (g) {
            simpleDateFormat = new SimpleDateFormat(simpleDateFormat3.toPattern(), l.c());
        } else {
            simpleDateFormat = (SimpleDateFormat) simpleDateFormat3.clone();
        }
        TimeZone f = l.f();
        if (f == null || f.equals(simpleDateFormat.getTimeZone())) {
            z2 = false;
        }
        if (z2) {
            simpleDateFormat.setTimeZone(f);
        }
        return r(Boolean.FALSE, simpleDateFormat);
    }

    @Override // b.g.a.c.n
    public boolean d(x xVar, T t) {
        return false;
    }

    public boolean p(x xVar) {
        Boolean bool = this._useTimestamp;
        if (bool != null) {
            return bool.booleanValue();
        }
        if (this._customFormat != null) {
            return false;
        }
        if (xVar != null) {
            return xVar.D(w.WRITE_DATES_AS_TIMESTAMPS);
        }
        StringBuilder R = b.d.b.a.a.R("Null SerializerProvider passed for ");
        R.append(this._handledType.getName());
        throw new IllegalArgumentException(R.toString());
    }

    public void q(Date date, b.g.a.b.d dVar, x xVar) throws IOException {
        if (this._customFormat == null) {
            Objects.requireNonNull(xVar);
            if (xVar.D(w.WRITE_DATES_AS_TIMESTAMPS)) {
                dVar.I(date.getTime());
            } else {
                dVar.j0(xVar.j().format(date));
            }
        } else {
            DateFormat andSet = this._reusedCustomFormat.getAndSet(null);
            if (andSet == null) {
                andSet = (DateFormat) this._customFormat.clone();
            }
            dVar.j0(andSet.format(date));
            this._reusedCustomFormat.compareAndSet(null, andSet);
        }
    }

    public abstract l<T> r(Boolean bool, DateFormat dateFormat);
}
