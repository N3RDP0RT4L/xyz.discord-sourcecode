package b.g.a.c.g0.u;

import b.g.a.a.i;
import b.g.a.c.d;
import b.g.a.c.g0.i;
import b.g.a.c.i0.f;
import b.g.a.c.n;
import b.g.a.c.w;
import b.g.a.c.x;
import b.g.a.c.y.a;
import com.fasterxml.jackson.databind.JsonMappingException;
import java.io.IOException;
import java.util.Objects;
/* compiled from: EnumSerializer.java */
@a
/* loaded from: classes2.dex */
public class m extends p0<Enum<?>> implements i {
    public static final /* synthetic */ int k = 0;
    private static final long serialVersionUID = 1;
    public final Boolean _serializeAsIndex;
    public final f _values;

    public m(f fVar, Boolean bool) {
        super(fVar.b(), false);
        this._values = fVar;
        this._serializeAsIndex = bool;
    }

    public static Boolean p(Class<?> cls, i.d dVar, boolean z2, Boolean bool) {
        i.c e = dVar == null ? null : dVar.e();
        if (e == null || e == i.c.ANY || e == i.c.SCALAR) {
            return bool;
        }
        if (e == i.c.STRING || e == i.c.NATURAL) {
            return Boolean.FALSE;
        }
        if (e.f() || e == i.c.ARRAY) {
            return Boolean.TRUE;
        }
        Object[] objArr = new Object[3];
        objArr[0] = e;
        objArr[1] = cls.getName();
        objArr[2] = z2 ? "class" : "property";
        throw new IllegalArgumentException(String.format("Unsupported serialization shape (%s) for Enum %s, not supported as %s annotation", objArr));
    }

    @Override // b.g.a.c.g0.i
    public n<?> a(x xVar, d dVar) throws JsonMappingException {
        i.d l = l(xVar, dVar, this._handledType);
        if (l != null) {
            Boolean p = p(this._handledType, l, false, this._serializeAsIndex);
            if (!Objects.equals(p, this._serializeAsIndex)) {
                return new m(this._values, p);
            }
        }
        return this;
    }

    @Override // b.g.a.c.n
    public void f(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
        boolean z2;
        Enum<?> r2 = (Enum) obj;
        Boolean bool = this._serializeAsIndex;
        if (bool != null) {
            z2 = bool.booleanValue();
        } else {
            z2 = xVar.D(w.WRITE_ENUMS_USING_INDEX);
        }
        if (z2) {
            dVar.H(r2.ordinal());
        } else if (xVar.D(w.WRITE_ENUMS_USING_TO_STRING)) {
            dVar.j0(r2.toString());
        } else {
            dVar.g0(this._values.c(r2));
        }
    }
}
