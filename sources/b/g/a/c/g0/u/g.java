package b.g.a.c.g0.u;

import b.g.a.b.b;
import b.g.a.b.d;
import b.g.a.c.i0.c;
import b.g.a.c.x;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Objects;
/* compiled from: ByteBufferSerializer.java */
/* loaded from: classes2.dex */
public class g extends p0<ByteBuffer> {
    public g() {
        super(ByteBuffer.class);
    }

    @Override // b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        ByteBuffer byteBuffer = (ByteBuffer) obj;
        if (byteBuffer.hasArray()) {
            int position = byteBuffer.position();
            byte[] array = byteBuffer.array();
            int arrayOffset = byteBuffer.arrayOffset() + position;
            int limit = byteBuffer.limit() - position;
            Objects.requireNonNull(dVar);
            dVar.n(b.f652b, array, arrayOffset, limit);
            return;
        }
        ByteBuffer asReadOnlyBuffer = byteBuffer.asReadOnlyBuffer();
        if (asReadOnlyBuffer.position() > 0) {
            asReadOnlyBuffer.rewind();
        }
        c cVar = new c(asReadOnlyBuffer);
        int remaining = asReadOnlyBuffer.remaining();
        Objects.requireNonNull(dVar);
        dVar.f(b.f652b, cVar, remaining);
        cVar.close();
    }
}
