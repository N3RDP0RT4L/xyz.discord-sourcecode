package b.g.a.c.g0.u;

import b.g.a.b.d;
import b.g.a.c.x;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;
/* compiled from: StdJdkSerializers.java */
/* loaded from: classes2.dex */
public class l0 extends p0<AtomicInteger> {
    public l0() {
        super(AtomicInteger.class, false);
    }

    @Override // b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        dVar.H(((AtomicInteger) obj).get());
    }
}
