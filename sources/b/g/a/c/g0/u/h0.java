package b.g.a.c.g0.u;

import b.g.a.c.d;
import b.g.a.c.e0.g;
import b.g.a.c.g0.i;
import b.g.a.c.n;
import b.g.a.c.x;
import java.io.IOException;
import java.util.Collection;
/* compiled from: StaticListSerializerBase.java */
/* loaded from: classes2.dex */
public abstract class h0<T extends Collection<?>> extends q0<T> implements i {
    public final Boolean _unwrapSingle;

    public h0(Class<?> cls) {
        super(cls, false);
        this._unwrapSingle = null;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:12:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:13:0x002a  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0031  */
    /* JADX WARN: Removed duplicated region for block: B:19:0x003b  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x0049  */
    @Override // b.g.a.c.g0.i
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.g.a.c.n<?> a(b.g.a.c.x r6, b.g.a.c.d r7) throws com.fasterxml.jackson.databind.JsonMappingException {
        /*
            r5 = this;
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            r1 = 0
            if (r7 == 0) goto L1a
            b.g.a.c.b r2 = r6.v()
            b.g.a.c.c0.i r3 = r7.getMember()
            if (r3 == 0) goto L1a
            java.lang.Object r2 = r2.c(r3)
            if (r2 == 0) goto L1a
            b.g.a.c.n r2 = r6.H(r3, r2)
            goto L1b
        L1a:
            r2 = r1
        L1b:
            java.lang.Class<T> r3 = r5._handledType
            b.g.a.a.i$d r3 = r5.l(r6, r7, r3)
            if (r3 == 0) goto L2a
            b.g.a.a.i$a r4 = b.g.a.a.i.a.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED
            java.lang.Boolean r3 = r3.b(r4)
            goto L2b
        L2a:
            r3 = r1
        L2b:
            b.g.a.c.n r2 = r5.k(r6, r7, r2)
            if (r2 != 0) goto L35
            b.g.a.c.n r2 = r6.n(r0, r7)
        L35:
            boolean r4 = b.g.a.c.i0.d.s(r2)
            if (r4 == 0) goto L49
            java.lang.Boolean r6 = r5._unwrapSingle
            boolean r6 = java.util.Objects.equals(r3, r6)
            if (r6 == 0) goto L44
            return r5
        L44:
            b.g.a.c.n r6 = r5.p(r7, r3)
            return r6
        L49:
            b.g.a.c.g0.u.j r7 = new b.g.a.c.g0.u.j
            b.g.a.c.j r6 = r6.b(r0)
            r0 = 1
            r7.<init>(r6, r0, r1, r2)
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.c.g0.u.h0.a(b.g.a.c.x, b.g.a.c.d):b.g.a.c.n");
    }

    @Override // b.g.a.c.n
    public boolean d(x xVar, Object obj) {
        Collection collection = (Collection) obj;
        return collection == null || collection.size() == 0;
    }

    public abstract n<?> p(d dVar, Boolean bool);

    /* renamed from: q */
    public abstract void g(T t, b.g.a.b.d dVar, x xVar, g gVar) throws IOException;

    public h0(h0<?> h0Var, Boolean bool) {
        super(h0Var);
        this._unwrapSingle = bool;
    }
}
