package b.g.a.c.g0.u;

import b.g.a.b.d;
import b.g.a.c.e0.g;
import b.g.a.c.g0.h;
import b.g.a.c.j;
import b.g.a.c.w;
import b.g.a.c.x;
import java.io.IOException;
import java.util.EnumSet;
import java.util.Iterator;
/* compiled from: EnumSetSerializer.java */
/* loaded from: classes2.dex */
public class n extends b<EnumSet<? extends Enum<?>>> {
    public n(j jVar) {
        super((Class<?>) EnumSet.class, jVar, true, (g) null, (b.g.a.c.n<Object>) null);
    }

    @Override // b.g.a.c.n
    public boolean d(x xVar, Object obj) {
        return ((EnumSet) obj).isEmpty();
    }

    @Override // b.g.a.c.g0.u.b, b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        EnumSet<? extends Enum<?>> enumSet = (EnumSet) obj;
        int size = enumSet.size();
        if (size != 1 || ((this._unwrapSingle != null || !xVar.D(w.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) && this._unwrapSingle != Boolean.TRUE)) {
            dVar.b0(enumSet, size);
            s(enumSet, dVar, xVar);
            dVar.t();
            return;
        }
        s(enumSet, dVar, xVar);
    }

    @Override // b.g.a.c.g0.h
    public h p(g gVar) {
        return this;
    }

    @Override // b.g.a.c.g0.h
    public boolean q(Object obj) {
        return ((EnumSet) obj).size() == 1;
    }

    @Override // b.g.a.c.g0.u.b
    public b<EnumSet<? extends Enum<?>>> t(b.g.a.c.d dVar, g gVar, b.g.a.c.n nVar, Boolean bool) {
        return new n(this, dVar, gVar, nVar, bool);
    }

    /* renamed from: u */
    public void s(EnumSet<? extends Enum<?>> enumSet, d dVar, x xVar) throws IOException {
        b.g.a.c.n<Object> nVar = this._elementSerializer;
        Iterator it = enumSet.iterator();
        while (it.hasNext()) {
            Enum r1 = (Enum) it.next();
            if (nVar == null) {
                nVar = xVar.n(r1.getDeclaringClass(), this._property);
            }
            nVar.f(r1, dVar, xVar);
        }
    }

    public n(n nVar, b.g.a.c.d dVar, g gVar, b.g.a.c.n<?> nVar2, Boolean bool) {
        super(nVar, dVar, gVar, nVar2, bool);
    }
}
