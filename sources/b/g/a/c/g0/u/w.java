package b.g.a.c.g0.u;

import b.g.a.a.i;
import b.g.a.b.f;
import b.g.a.c.d;
import b.g.a.c.g0.i;
import b.g.a.c.g0.u.v;
import b.g.a.c.n;
import b.g.a.c.x;
import com.fasterxml.jackson.databind.JsonMappingException;
import java.math.BigDecimal;
/* compiled from: NumberSerializers.java */
/* loaded from: classes2.dex */
public abstract class w<T> extends p0<T> implements i {
    public final boolean _isInt;
    public final f.b _numberType;
    public final String _schemaType;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public w(Class<?> cls, f.b bVar, String str) {
        super(cls, false);
        boolean z2 = false;
        this._numberType = bVar;
        this._schemaType = str;
        this._isInt = (bVar == f.b.INT || bVar == f.b.LONG || bVar == f.b.BIG_INTEGER) ? true : z2;
    }

    @Override // b.g.a.c.g0.i
    public n<?> a(x xVar, d dVar) throws JsonMappingException {
        i.d l = l(xVar, dVar, this._handledType);
        if (l == null || l.e().ordinal() != 8) {
            return this;
        }
        if (this._handledType != BigDecimal.class) {
            return t0.k;
        }
        v vVar = v.k;
        return v.a.k;
    }
}
