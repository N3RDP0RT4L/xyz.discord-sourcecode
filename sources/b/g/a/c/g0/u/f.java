package b.g.a.c.g0.u;

import b.g.a.b.d;
import b.g.a.b.h;
import b.g.a.b.s.b;
import b.g.a.c.e0.g;
import b.g.a.c.x;
import b.g.a.c.y.a;
import java.io.IOException;
/* compiled from: ByteArraySerializer.java */
@a
/* loaded from: classes2.dex */
public class f extends q0<byte[]> {
    private static final long serialVersionUID = 1;

    public f() {
        super(byte[].class);
    }

    @Override // b.g.a.c.n
    public boolean d(x xVar, Object obj) {
        return ((byte[]) obj).length == 0;
    }

    @Override // b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        byte[] bArr = (byte[]) obj;
        dVar.n(xVar._config._base._defaultBase64, bArr, 0, bArr.length);
    }

    @Override // b.g.a.c.n
    public void g(Object obj, d dVar, x xVar, g gVar) throws IOException {
        byte[] bArr = (byte[]) obj;
        b e = gVar.e(dVar, gVar.d(bArr, h.VALUE_EMBEDDED_OBJECT));
        dVar.n(xVar._config._base._defaultBase64, bArr, 0, bArr.length);
        gVar.f(dVar, e);
    }
}
