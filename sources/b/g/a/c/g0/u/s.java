package b.g.a.c.g0.u;

import b.g.a.a.c0;
import b.g.a.b.h;
import b.g.a.b.s.b;
import b.g.a.c.d;
import b.g.a.c.e0.g;
import b.g.a.c.g0.i;
import b.g.a.c.g0.t.l;
import b.g.a.c.j;
import b.g.a.c.n;
import b.g.a.c.p;
import b.g.a.c.x;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.RuntimeJsonMappingException;
import java.io.IOException;
import java.util.Objects;
/* compiled from: JsonValueSerializer.java */
@b.g.a.c.y.a
/* loaded from: classes2.dex */
public class s extends q0<Object> implements i {
    public final b.g.a.c.c0.i _accessor;
    public final boolean _forceTypeInformation;
    public final d _property;
    public final n<Object> _valueSerializer;
    public final j _valueType;
    public final g _valueTypeSerializer;
    public transient l k;

    /* compiled from: JsonValueSerializer.java */
    /* loaded from: classes2.dex */
    public static class a extends g {
        public final g a;

        /* renamed from: b  reason: collision with root package name */
        public final Object f710b;

        public a(g gVar, Object obj) {
            this.a = gVar;
            this.f710b = obj;
        }

        @Override // b.g.a.c.e0.g
        public g a(d dVar) {
            throw new UnsupportedOperationException();
        }

        @Override // b.g.a.c.e0.g
        public String b() {
            return this.a.b();
        }

        @Override // b.g.a.c.e0.g
        public c0.a c() {
            return this.a.c();
        }

        @Override // b.g.a.c.e0.g
        public b e(b.g.a.b.d dVar, b bVar) throws IOException {
            bVar.a = this.f710b;
            return this.a.e(dVar, bVar);
        }

        @Override // b.g.a.c.e0.g
        public b f(b.g.a.b.d dVar, b bVar) throws IOException {
            return this.a.f(dVar, bVar);
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public s(b.g.a.c.g0.u.s r2, b.g.a.c.d r3, b.g.a.c.e0.g r4, b.g.a.c.n<?> r5, boolean r6) {
        /*
            r1 = this;
            java.lang.Class<T> r0 = r2._handledType
            if (r0 != 0) goto L6
            java.lang.Class<java.lang.Object> r0 = java.lang.Object.class
        L6:
            r1.<init>(r0)
            b.g.a.c.c0.i r0 = r2._accessor
            r1._accessor = r0
            b.g.a.c.j r2 = r2._valueType
            r1._valueType = r2
            r1._valueTypeSerializer = r4
            r1._valueSerializer = r5
            r1._property = r3
            r1._forceTypeInformation = r6
            b.g.a.c.g0.t.l$b r2 = b.g.a.c.g0.t.l.b.f702b
            r1.k = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.c.g0.u.s.<init>(b.g.a.c.g0.u.s, b.g.a.c.d, b.g.a.c.e0.g, b.g.a.c.n, boolean):void");
    }

    @Override // b.g.a.c.g0.i
    public n<?> a(x xVar, d dVar) throws JsonMappingException {
        g gVar = this._valueTypeSerializer;
        if (gVar != null) {
            gVar = gVar.a(dVar);
        }
        n<?> nVar = this._valueSerializer;
        if (nVar != null) {
            return q(dVar, gVar, xVar.y(nVar, dVar), this._forceTypeInformation);
        }
        if (!xVar.C(p.USE_STATIC_TYPING) && !this._valueType.x()) {
            return dVar != this._property ? q(dVar, gVar, nVar, this._forceTypeInformation) : this;
        }
        n<Object> q = xVar.q(this._valueType, dVar);
        Class<?> cls = this._valueType._class;
        boolean z2 = false;
        if (!cls.isPrimitive() ? cls == String.class || cls == Integer.class || cls == Boolean.class || cls == Double.class : cls == Integer.TYPE || cls == Boolean.TYPE || cls == Double.TYPE) {
            z2 = b.g.a.c.i0.d.s(q);
        }
        return q(dVar, gVar, q, z2);
    }

    @Override // b.g.a.c.n
    public boolean d(x xVar, Object obj) {
        Object j = this._accessor.j(obj);
        if (j == null) {
            return true;
        }
        n<Object> nVar = this._valueSerializer;
        if (nVar == null) {
            try {
                nVar = p(xVar, j.getClass());
            } catch (JsonMappingException e) {
                throw new RuntimeJsonMappingException(e);
            }
        }
        return nVar.d(xVar, j);
    }

    @Override // b.g.a.c.n
    public void f(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
        try {
            Object j = this._accessor.j(obj);
            if (j == null) {
                xVar.l(dVar);
                return;
            }
            n<Object> nVar = this._valueSerializer;
            if (nVar == null) {
                nVar = p(xVar, j.getClass());
            }
            g gVar = this._valueTypeSerializer;
            if (gVar != null) {
                nVar.g(j, dVar, xVar, gVar);
            } else {
                nVar.f(j, dVar, xVar);
            }
        } catch (Exception e) {
            o(xVar, e, obj, this._accessor.c() + "()");
            throw null;
        }
    }

    @Override // b.g.a.c.n
    public void g(Object obj, b.g.a.b.d dVar, x xVar, g gVar) throws IOException {
        try {
            Object j = this._accessor.j(obj);
            if (j == null) {
                xVar.l(dVar);
                return;
            }
            n<Object> nVar = this._valueSerializer;
            if (nVar == null) {
                nVar = p(xVar, j.getClass());
            } else if (this._forceTypeInformation) {
                b e = gVar.e(dVar, gVar.d(obj, h.VALUE_STRING));
                nVar.f(j, dVar, xVar);
                gVar.f(dVar, e);
                return;
            }
            nVar.g(j, dVar, xVar, new a(gVar, obj));
        } catch (Exception e2) {
            o(xVar, e2, obj, this._accessor.c() + "()");
            throw null;
        }
    }

    public n<Object> p(x xVar, Class<?> cls) throws JsonMappingException {
        n<Object> c = this.k.c(cls);
        if (c != null) {
            return c;
        }
        if (this._valueType.r()) {
            j k = xVar.k(this._valueType, cls);
            n<Object> q = xVar.q(k, this._property);
            l lVar = this.k;
            Objects.requireNonNull(lVar);
            this.k = lVar.b(k._class, q);
            return q;
        }
        n<Object> r = xVar.r(cls, this._property);
        this.k = this.k.b(cls, r);
        return r;
    }

    public s q(d dVar, g gVar, n<?> nVar, boolean z2) {
        return (this._property == dVar && this._valueTypeSerializer == gVar && this._valueSerializer == nVar && z2 == this._forceTypeInformation) ? this : new s(this, dVar, gVar, nVar, z2);
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("(@JsonValue serializer for method ");
        R.append(this._accessor.g());
        R.append("#");
        R.append(this._accessor.c());
        R.append(")");
        return R.toString();
    }

    public s(b.g.a.c.c0.i iVar, g gVar, n<?> nVar) {
        super(iVar.e());
        this._accessor = iVar;
        this._valueType = iVar.e();
        this._valueTypeSerializer = gVar;
        this._valueSerializer = nVar;
        this._property = null;
        this._forceTypeInformation = true;
        this.k = l.b.f702b;
    }
}
