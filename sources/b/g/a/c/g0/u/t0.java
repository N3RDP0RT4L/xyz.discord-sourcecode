package b.g.a.c.g0.u;

import b.g.a.c.y.a;
/* compiled from: ToStringSerializer.java */
@a
/* loaded from: classes2.dex */
public class t0 extends u0 {
    public static final t0 k = new t0();

    public t0() {
        super(Object.class);
    }

    @Override // b.g.a.c.g0.u.u0
    public final String p(Object obj) {
        return obj.toString();
    }

    public t0(Class<?> cls) {
        super(cls);
    }
}
