package b.g.a.c.g0.u;

import b.g.a.b.d;
import b.g.a.c.e0.g;
import b.g.a.c.g0.h;
import b.g.a.c.g0.i;
import b.g.a.c.g0.t.l;
import b.g.a.c.j;
import b.g.a.c.n;
import b.g.a.c.w;
import b.g.a.c.x;
import b.g.a.c.y.a;
import java.io.IOException;
/* compiled from: ObjectArraySerializer.java */
@a
/* loaded from: classes2.dex */
public class d0 extends a<Object[]> implements i {
    public l _dynamicSerializers = l.b.f702b;
    public n<Object> _elementSerializer;
    public final j _elementType;
    public final boolean _staticTyping;
    public final g _valueTypeSerializer;

    public d0(j jVar, boolean z2, g gVar, n<Object> nVar) {
        super(Object[].class);
        this._elementType = jVar;
        this._staticTyping = z2;
        this._valueTypeSerializer = gVar;
        this._elementSerializer = nVar;
    }

    /* JADX WARN: Removed duplicated region for block: B:15:0x002a  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x0033  */
    @Override // b.g.a.c.g0.u.a, b.g.a.c.g0.i
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.g.a.c.n<?> a(b.g.a.c.x r8, b.g.a.c.d r9) throws com.fasterxml.jackson.databind.JsonMappingException {
        /*
            r7 = this;
            b.g.a.c.e0.g r0 = r7._valueTypeSerializer
            if (r0 == 0) goto L8
            b.g.a.c.e0.g r0 = r0.a(r9)
        L8:
            r4 = r0
            r0 = 0
            if (r9 == 0) goto L21
            b.g.a.c.c0.i r1 = r9.getMember()
            b.g.a.c.b r2 = r8.v()
            if (r1 == 0) goto L21
            java.lang.Object r2 = r2.c(r1)
            if (r2 == 0) goto L21
            b.g.a.c.n r1 = r8.H(r1, r2)
            goto L22
        L21:
            r1 = r0
        L22:
            java.lang.Class<T> r2 = r7._handledType
            b.g.a.a.i$d r2 = r7.l(r8, r9, r2)
            if (r2 == 0) goto L30
            b.g.a.a.i$a r0 = b.g.a.a.i.a.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED
            java.lang.Boolean r0 = r2.b(r0)
        L30:
            r6 = r0
            if (r1 != 0) goto L35
            b.g.a.c.n<java.lang.Object> r1 = r7._elementSerializer
        L35:
            b.g.a.c.n r0 = r7.k(r8, r9, r1)
            if (r0 != 0) goto L51
            b.g.a.c.j r1 = r7._elementType
            if (r1 == 0) goto L51
            boolean r2 = r7._staticTyping
            if (r2 == 0) goto L51
            boolean r1 = r1.y()
            if (r1 != 0) goto L51
            b.g.a.c.j r0 = r7._elementType
            b.g.a.c.n r8 = r8.m(r0, r9)
            r5 = r8
            goto L52
        L51:
            r5 = r0
        L52:
            b.g.a.c.d r8 = r7._property
            if (r8 != r9) goto L68
            b.g.a.c.n<java.lang.Object> r8 = r7._elementSerializer
            if (r5 != r8) goto L68
            b.g.a.c.e0.g r8 = r7._valueTypeSerializer
            if (r8 != r4) goto L68
            java.lang.Boolean r8 = r7._unwrapSingle
            boolean r8 = java.util.Objects.equals(r8, r6)
            if (r8 == 0) goto L68
            r8 = r7
            goto L70
        L68:
            b.g.a.c.g0.u.d0 r8 = new b.g.a.c.g0.u.d0
            r1 = r8
            r2 = r7
            r3 = r9
            r1.<init>(r2, r3, r4, r5, r6)
        L70:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.c.g0.u.d0.a(b.g.a.c.x, b.g.a.c.d):b.g.a.c.n");
    }

    @Override // b.g.a.c.n
    public boolean d(x xVar, Object obj) {
        return ((Object[]) obj).length == 0;
    }

    @Override // b.g.a.c.g0.u.a, b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        Object[] objArr = (Object[]) obj;
        int length = objArr.length;
        if (length != 1 || ((this._unwrapSingle != null || !xVar.D(w.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) && this._unwrapSingle != Boolean.TRUE)) {
            dVar.b0(objArr, length);
            t(objArr, dVar, xVar);
            dVar.t();
            return;
        }
        t(objArr, dVar, xVar);
    }

    @Override // b.g.a.c.g0.h
    public h<?> p(g gVar) {
        return new d0(this._elementType, this._staticTyping, gVar, this._elementSerializer);
    }

    @Override // b.g.a.c.g0.h
    public boolean q(Object obj) {
        return ((Object[]) obj).length == 1;
    }

    @Override // b.g.a.c.g0.u.a
    public n<?> s(b.g.a.c.d dVar, Boolean bool) {
        return new d0(this, dVar, this._valueTypeSerializer, this._elementSerializer, bool);
    }

    /* renamed from: u */
    public void t(Object[] objArr, d dVar, x xVar) throws IOException {
        Exception e;
        Object obj;
        Exception e2;
        Object obj2;
        l b2;
        int length = objArr.length;
        if (length != 0) {
            n<Object> nVar = this._elementSerializer;
            int i = 0;
            if (nVar != null) {
                int length2 = objArr.length;
                g gVar = this._valueTypeSerializer;
                Object obj3 = null;
                while (i < length2) {
                    try {
                        obj3 = objArr[i];
                        if (obj3 == null) {
                            xVar.l(dVar);
                        } else if (gVar == null) {
                            nVar.f(obj3, dVar, xVar);
                        } else {
                            nVar.g(obj3, dVar, xVar, gVar);
                        }
                        i++;
                    } catch (Exception e3) {
                        n(xVar, e3, obj3, i);
                        throw null;
                    }
                }
                return;
            }
            g gVar2 = this._valueTypeSerializer;
            if (gVar2 != null) {
                int length3 = objArr.length;
                try {
                    l lVar = this._dynamicSerializers;
                    obj2 = null;
                    while (i < length3) {
                        try {
                            obj2 = objArr[i];
                            if (obj2 == null) {
                                xVar.l(dVar);
                            } else {
                                Class<?> cls = obj2.getClass();
                                n<Object> c = lVar.c(cls);
                                if (c == null && lVar != (b2 = lVar.b(cls, (c = xVar.n(cls, this._property))))) {
                                    this._dynamicSerializers = b2;
                                }
                                c.g(obj2, dVar, xVar, gVar2);
                            }
                            i++;
                        } catch (Exception e4) {
                            e2 = e4;
                            n(xVar, e2, obj2, i);
                            throw null;
                        }
                    }
                } catch (Exception e5) {
                    e2 = e5;
                    obj2 = null;
                }
            } else {
                try {
                    l lVar2 = this._dynamicSerializers;
                    obj = null;
                    while (i < length) {
                        try {
                            obj = objArr[i];
                            if (obj == null) {
                                xVar.l(dVar);
                            } else {
                                Class<?> cls2 = obj.getClass();
                                n<Object> c2 = lVar2.c(cls2);
                                if (c2 == null) {
                                    if (this._elementType.r()) {
                                        l.d a = lVar2.a(xVar.k(this._elementType, cls2), xVar, this._property);
                                        l lVar3 = a.f704b;
                                        if (lVar2 != lVar3) {
                                            this._dynamicSerializers = lVar3;
                                        }
                                        c2 = a.a;
                                    } else {
                                        c2 = xVar.n(cls2, this._property);
                                        l b3 = lVar2.b(cls2, c2);
                                        if (lVar2 != b3) {
                                            this._dynamicSerializers = b3;
                                        }
                                    }
                                }
                                c2.f(obj, dVar, xVar);
                            }
                            i++;
                        } catch (Exception e6) {
                            e = e6;
                            n(xVar, e, obj, i);
                            throw null;
                        }
                    }
                } catch (Exception e7) {
                    e = e7;
                    obj = null;
                }
            }
        }
    }

    public d0(d0 d0Var, b.g.a.c.d dVar, g gVar, n<?> nVar, Boolean bool) {
        super(d0Var, dVar, bool);
        this._elementType = d0Var._elementType;
        this._valueTypeSerializer = gVar;
        this._staticTyping = d0Var._staticTyping;
        this._elementSerializer = nVar;
    }
}
