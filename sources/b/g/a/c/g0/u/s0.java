package b.g.a.c.g0.u;

import b.g.a.b.d;
import b.g.a.b.h;
import b.g.a.b.s.b;
import b.g.a.c.e0.g;
import b.g.a.c.x;
import java.io.IOException;
import java.util.TimeZone;
/* compiled from: TimeZoneSerializer.java */
/* loaded from: classes2.dex */
public class s0 extends p0<TimeZone> {
    public s0() {
        super(TimeZone.class);
    }

    @Override // b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        dVar.j0(((TimeZone) obj).getID());
    }

    @Override // b.g.a.c.g0.u.p0, b.g.a.c.n
    public void g(Object obj, d dVar, x xVar, g gVar) throws IOException {
        TimeZone timeZone = (TimeZone) obj;
        b d = gVar.d(timeZone, h.VALUE_STRING);
        d.f671b = TimeZone.class;
        b e = gVar.e(dVar, d);
        dVar.j0(timeZone.getID());
        gVar.f(dVar, e);
    }
}
