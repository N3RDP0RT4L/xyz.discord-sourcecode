package b.g.a.c.g0.u;

import b.g.a.c.n;
import b.g.a.c.w;
import b.g.a.c.x;
import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;
/* compiled from: StdArraySerializers.java */
/* loaded from: classes2.dex */
public class i0 {
    public static final HashMap<String, n<?>> a;

    /* compiled from: StdArraySerializers.java */
    @b.g.a.c.y.a
    /* loaded from: classes2.dex */
    public static class a extends b.g.a.c.g0.u.a<boolean[]> {
        static {
            b.g.a.c.h0.n.k.j(Boolean.class);
        }

        public a() {
            super(boolean[].class);
        }

        @Override // b.g.a.c.n
        public boolean d(x xVar, Object obj) {
            return ((boolean[]) obj).length == 0;
        }

        @Override // b.g.a.c.g0.u.a, b.g.a.c.n
        public void f(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
            boolean[] zArr = (boolean[]) obj;
            int length = zArr.length;
            if (length != 1 || !r(xVar)) {
                dVar.b0(zArr, length);
                u(zArr, dVar);
                dVar.t();
                return;
            }
            u(zArr, dVar);
        }

        @Override // b.g.a.c.g0.h
        public b.g.a.c.g0.h<?> p(b.g.a.c.e0.g gVar) {
            return this;
        }

        @Override // b.g.a.c.g0.h
        public boolean q(Object obj) {
            return ((boolean[]) obj).length == 1;
        }

        @Override // b.g.a.c.g0.u.a
        public n<?> s(b.g.a.c.d dVar, Boolean bool) {
            return new a(this, dVar, bool);
        }

        @Override // b.g.a.c.g0.u.a
        public /* bridge */ /* synthetic */ void t(boolean[] zArr, b.g.a.b.d dVar, x xVar) throws IOException {
            u(zArr, dVar);
        }

        public void u(boolean[] zArr, b.g.a.b.d dVar) throws IOException {
            for (boolean z2 : zArr) {
                dVar.s(z2);
            }
        }

        public a(a aVar, b.g.a.c.d dVar, Boolean bool) {
            super(aVar, dVar, bool);
        }
    }

    /* compiled from: StdArraySerializers.java */
    @b.g.a.c.y.a
    /* loaded from: classes2.dex */
    public static class b extends q0<char[]> {
        public b() {
            super(char[].class);
        }

        @Override // b.g.a.c.n
        public boolean d(x xVar, Object obj) {
            return ((char[]) obj).length == 0;
        }

        @Override // b.g.a.c.n
        public void f(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
            char[] cArr = (char[]) obj;
            if (xVar.D(w.WRITE_CHAR_ARRAYS_AS_JSON_ARRAYS)) {
                dVar.b0(cArr, cArr.length);
                int length = cArr.length;
                for (int i = 0; i < length; i++) {
                    dVar.m0(cArr, i, 1);
                }
                dVar.t();
                return;
            }
            dVar.m0(cArr, 0, cArr.length);
        }

        @Override // b.g.a.c.n
        public void g(Object obj, b.g.a.b.d dVar, x xVar, b.g.a.c.e0.g gVar) throws IOException {
            b.g.a.b.s.b bVar;
            char[] cArr = (char[]) obj;
            if (xVar.D(w.WRITE_CHAR_ARRAYS_AS_JSON_ARRAYS)) {
                bVar = gVar.e(dVar, gVar.d(cArr, b.g.a.b.h.START_ARRAY));
                int length = cArr.length;
                for (int i = 0; i < length; i++) {
                    dVar.m0(cArr, i, 1);
                }
            } else {
                bVar = gVar.e(dVar, gVar.d(cArr, b.g.a.b.h.VALUE_STRING));
                dVar.m0(cArr, 0, cArr.length);
            }
            gVar.f(dVar, bVar);
        }
    }

    /* compiled from: StdArraySerializers.java */
    @b.g.a.c.y.a
    /* loaded from: classes2.dex */
    public static class c extends b.g.a.c.g0.u.a<double[]> {
        static {
            b.g.a.c.h0.n.k.j(Double.TYPE);
        }

        public c() {
            super(double[].class);
        }

        @Override // b.g.a.c.n
        public boolean d(x xVar, Object obj) {
            return ((double[]) obj).length == 0;
        }

        @Override // b.g.a.c.g0.u.a, b.g.a.c.n
        public void f(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
            double[] dArr = (double[]) obj;
            int i = 0;
            if (dArr.length != 1 || !r(xVar)) {
                int length = dArr.length;
                Objects.requireNonNull(dVar);
                dVar.a(dArr.length, 0, length);
                dVar.b0(dArr, length);
                int i2 = length + 0;
                while (i < i2) {
                    dVar.C(dArr[i]);
                    i++;
                }
                dVar.t();
                return;
            }
            int length2 = dArr.length;
            while (i < length2) {
                dVar.C(dArr[i]);
                i++;
            }
        }

        @Override // b.g.a.c.g0.h
        public b.g.a.c.g0.h<?> p(b.g.a.c.e0.g gVar) {
            return this;
        }

        @Override // b.g.a.c.g0.h
        public boolean q(Object obj) {
            return ((double[]) obj).length == 1;
        }

        @Override // b.g.a.c.g0.u.a
        public n<?> s(b.g.a.c.d dVar, Boolean bool) {
            return new c(this, dVar, bool);
        }

        @Override // b.g.a.c.g0.u.a
        public void t(double[] dArr, b.g.a.b.d dVar, x xVar) throws IOException {
            for (double d : dArr) {
                dVar.C(d);
            }
        }

        public c(c cVar, b.g.a.c.d dVar, Boolean bool) {
            super(cVar, dVar, bool);
        }
    }

    /* compiled from: StdArraySerializers.java */
    @b.g.a.c.y.a
    /* loaded from: classes2.dex */
    public static class d extends h<float[]> {
        static {
            b.g.a.c.h0.n.k.j(Float.TYPE);
        }

        public d() {
            super(float[].class);
        }

        @Override // b.g.a.c.n
        public boolean d(x xVar, Object obj) {
            return ((float[]) obj).length == 0;
        }

        @Override // b.g.a.c.g0.u.a, b.g.a.c.n
        public void f(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
            float[] fArr = (float[]) obj;
            int length = fArr.length;
            if (length != 1 || !r(xVar)) {
                dVar.b0(fArr, length);
                u(fArr, dVar);
                dVar.t();
                return;
            }
            u(fArr, dVar);
        }

        @Override // b.g.a.c.g0.h
        public boolean q(Object obj) {
            return ((float[]) obj).length == 1;
        }

        @Override // b.g.a.c.g0.u.a
        public n<?> s(b.g.a.c.d dVar, Boolean bool) {
            return new d(this, dVar, bool);
        }

        @Override // b.g.a.c.g0.u.a
        public /* bridge */ /* synthetic */ void t(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
            u((float[]) obj, dVar);
        }

        public void u(float[] fArr, b.g.a.b.d dVar) throws IOException {
            for (float f : fArr) {
                dVar.D(f);
            }
        }

        public d(d dVar, b.g.a.c.d dVar2, Boolean bool) {
            super(dVar, dVar2, bool);
        }
    }

    /* compiled from: StdArraySerializers.java */
    @b.g.a.c.y.a
    /* loaded from: classes2.dex */
    public static class e extends b.g.a.c.g0.u.a<int[]> {
        static {
            b.g.a.c.h0.n.k.j(Integer.TYPE);
        }

        public e() {
            super(int[].class);
        }

        @Override // b.g.a.c.n
        public boolean d(x xVar, Object obj) {
            return ((int[]) obj).length == 0;
        }

        @Override // b.g.a.c.g0.u.a, b.g.a.c.n
        public void f(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
            int[] iArr = (int[]) obj;
            int i = 0;
            if (iArr.length != 1 || !r(xVar)) {
                int length = iArr.length;
                Objects.requireNonNull(dVar);
                dVar.a(iArr.length, 0, length);
                dVar.b0(iArr, length);
                int i2 = length + 0;
                while (i < i2) {
                    dVar.H(iArr[i]);
                    i++;
                }
                dVar.t();
                return;
            }
            int length2 = iArr.length;
            while (i < length2) {
                dVar.H(iArr[i]);
                i++;
            }
        }

        @Override // b.g.a.c.g0.h
        public b.g.a.c.g0.h<?> p(b.g.a.c.e0.g gVar) {
            return this;
        }

        @Override // b.g.a.c.g0.h
        public boolean q(Object obj) {
            return ((int[]) obj).length == 1;
        }

        @Override // b.g.a.c.g0.u.a
        public n<?> s(b.g.a.c.d dVar, Boolean bool) {
            return new e(this, dVar, bool);
        }

        @Override // b.g.a.c.g0.u.a
        public void t(int[] iArr, b.g.a.b.d dVar, x xVar) throws IOException {
            for (int i : iArr) {
                dVar.H(i);
            }
        }

        public e(e eVar, b.g.a.c.d dVar, Boolean bool) {
            super(eVar, dVar, bool);
        }
    }

    /* compiled from: StdArraySerializers.java */
    @b.g.a.c.y.a
    /* loaded from: classes2.dex */
    public static class f extends h<long[]> {
        static {
            b.g.a.c.h0.n.k.j(Long.TYPE);
        }

        public f() {
            super(long[].class);
        }

        @Override // b.g.a.c.n
        public boolean d(x xVar, Object obj) {
            return ((long[]) obj).length == 0;
        }

        @Override // b.g.a.c.g0.u.a, b.g.a.c.n
        public void f(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
            long[] jArr = (long[]) obj;
            int i = 0;
            if (jArr.length != 1 || !r(xVar)) {
                int length = jArr.length;
                Objects.requireNonNull(dVar);
                dVar.a(jArr.length, 0, length);
                dVar.b0(jArr, length);
                int i2 = length + 0;
                while (i < i2) {
                    dVar.I(jArr[i]);
                    i++;
                }
                dVar.t();
                return;
            }
            int length2 = jArr.length;
            while (i < length2) {
                dVar.I(jArr[i]);
                i++;
            }
        }

        @Override // b.g.a.c.g0.h
        public boolean q(Object obj) {
            return ((long[]) obj).length == 1;
        }

        @Override // b.g.a.c.g0.u.a
        public n<?> s(b.g.a.c.d dVar, Boolean bool) {
            return new f(this, dVar, bool);
        }

        @Override // b.g.a.c.g0.u.a
        public void t(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
            for (long j : (long[]) obj) {
                dVar.I(j);
            }
        }

        public f(f fVar, b.g.a.c.d dVar, Boolean bool) {
            super(fVar, dVar, bool);
        }
    }

    /* compiled from: StdArraySerializers.java */
    @b.g.a.c.y.a
    /* loaded from: classes2.dex */
    public static class g extends h<short[]> {
        static {
            b.g.a.c.h0.n.k.j(Short.TYPE);
        }

        public g() {
            super(short[].class);
        }

        @Override // b.g.a.c.n
        public boolean d(x xVar, Object obj) {
            return ((short[]) obj).length == 0;
        }

        @Override // b.g.a.c.g0.u.a, b.g.a.c.n
        public void f(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
            short[] sArr = (short[]) obj;
            int length = sArr.length;
            if (length != 1 || !r(xVar)) {
                dVar.b0(sArr, length);
                u(sArr, dVar);
                dVar.t();
                return;
            }
            u(sArr, dVar);
        }

        @Override // b.g.a.c.g0.h
        public boolean q(Object obj) {
            return ((short[]) obj).length == 1;
        }

        @Override // b.g.a.c.g0.u.a
        public n<?> s(b.g.a.c.d dVar, Boolean bool) {
            return new g(this, dVar, bool);
        }

        @Override // b.g.a.c.g0.u.a
        public /* bridge */ /* synthetic */ void t(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
            u((short[]) obj, dVar);
        }

        public void u(short[] sArr, b.g.a.b.d dVar) throws IOException {
            for (short s2 : sArr) {
                dVar.H(s2);
            }
        }

        public g(g gVar, b.g.a.c.d dVar, Boolean bool) {
            super(gVar, dVar, bool);
        }
    }

    /* compiled from: StdArraySerializers.java */
    /* loaded from: classes2.dex */
    public static abstract class h<T> extends b.g.a.c.g0.u.a<T> {
        public h(Class<T> cls) {
            super(cls);
        }

        @Override // b.g.a.c.g0.h
        public final b.g.a.c.g0.h<?> p(b.g.a.c.e0.g gVar) {
            return this;
        }

        public h(h<T> hVar, b.g.a.c.d dVar, Boolean bool) {
            super(hVar, dVar, bool);
        }
    }

    static {
        HashMap<String, n<?>> hashMap = new HashMap<>();
        a = hashMap;
        hashMap.put(boolean[].class.getName(), new a());
        hashMap.put(byte[].class.getName(), new b.g.a.c.g0.u.f());
        hashMap.put(char[].class.getName(), new b());
        hashMap.put(short[].class.getName(), new g());
        hashMap.put(int[].class.getName(), new e());
        hashMap.put(long[].class.getName(), new f());
        hashMap.put(float[].class.getName(), new d());
        hashMap.put(double[].class.getName(), new c());
    }
}
