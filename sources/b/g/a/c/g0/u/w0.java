package b.g.a.c.g0.u;

import b.g.a.b.d;
import b.g.a.c.g0.i;
import b.g.a.c.i0.s;
import b.g.a.c.x;
import java.io.IOException;
import java.util.Objects;
import java.util.UUID;
/* compiled from: UUIDSerializer.java */
/* loaded from: classes2.dex */
public class w0 extends p0<UUID> implements i {
    public static final char[] k = "0123456789abcdef".toCharArray();
    public final Boolean _asBinary;

    public w0() {
        super(UUID.class);
        this._asBinary = null;
    }

    public static final void p(int i, byte[] bArr, int i2) {
        bArr[i2] = (byte) (i >> 24);
        int i3 = i2 + 1;
        bArr[i3] = (byte) (i >> 16);
        int i4 = i3 + 1;
        bArr[i4] = (byte) (i >> 8);
        bArr[i4 + 1] = (byte) i;
    }

    public static void q(int i, char[] cArr, int i2) {
        char[] cArr2 = k;
        cArr[i2] = cArr2[(i >> 12) & 15];
        int i3 = i2 + 1;
        cArr[i3] = cArr2[(i >> 8) & 15];
        int i4 = i3 + 1;
        cArr[i4] = cArr2[(i >> 4) & 15];
        cArr[i4 + 1] = cArr2[i & 15];
    }

    /* JADX WARN: Removed duplicated region for block: B:13:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:15:0x0029 A[RETURN] */
    @Override // b.g.a.c.g0.i
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.g.a.c.n<?> a(b.g.a.c.x r2, b.g.a.c.d r3) throws com.fasterxml.jackson.databind.JsonMappingException {
        /*
            r1 = this;
            java.lang.Class<T> r0 = r1._handledType
            b.g.a.a.i$d r2 = r1.l(r2, r3, r0)
            if (r2 == 0) goto L1a
            b.g.a.a.i$c r2 = r2.e()
            b.g.a.a.i$c r3 = b.g.a.a.i.c.BINARY
            if (r2 != r3) goto L13
            java.lang.Boolean r2 = java.lang.Boolean.TRUE
            goto L1b
        L13:
            b.g.a.a.i$c r3 = b.g.a.a.i.c.STRING
            if (r2 != r3) goto L1a
            java.lang.Boolean r2 = java.lang.Boolean.FALSE
            goto L1b
        L1a:
            r2 = 0
        L1b:
            java.lang.Boolean r3 = r1._asBinary
            boolean r3 = java.util.Objects.equals(r2, r3)
            if (r3 != 0) goto L29
            b.g.a.c.g0.u.w0 r3 = new b.g.a.c.g0.u.w0
            r3.<init>(r2)
            return r3
        L29:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.c.g0.u.w0.a(b.g.a.c.x, b.g.a.c.d):b.g.a.c.n");
    }

    @Override // b.g.a.c.n
    public boolean d(x xVar, Object obj) {
        UUID uuid = (UUID) obj;
        return uuid.getLeastSignificantBits() == 0 && uuid.getMostSignificantBits() == 0;
    }

    @Override // b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        boolean z2;
        UUID uuid = (UUID) obj;
        Boolean bool = this._asBinary;
        if (bool != null) {
            z2 = bool.booleanValue();
        } else {
            if (!(dVar instanceof s)) {
                Objects.requireNonNull(dVar);
                if (dVar instanceof s) {
                    z2 = true;
                }
            }
            z2 = false;
        }
        if (z2) {
            byte[] bArr = new byte[16];
            long mostSignificantBits = uuid.getMostSignificantBits();
            long leastSignificantBits = uuid.getLeastSignificantBits();
            p((int) (mostSignificantBits >> 32), bArr, 0);
            p((int) mostSignificantBits, bArr, 4);
            p((int) (leastSignificantBits >> 32), bArr, 8);
            p((int) leastSignificantBits, bArr, 12);
            dVar.q(bArr);
            return;
        }
        char[] cArr = new char[36];
        long mostSignificantBits2 = uuid.getMostSignificantBits();
        int i = (int) (mostSignificantBits2 >> 32);
        q(i >> 16, cArr, 0);
        q(i, cArr, 4);
        cArr[8] = '-';
        int i2 = (int) mostSignificantBits2;
        q(i2 >>> 16, cArr, 9);
        cArr[13] = '-';
        q(i2, cArr, 14);
        cArr[18] = '-';
        long leastSignificantBits2 = uuid.getLeastSignificantBits();
        q((int) (leastSignificantBits2 >>> 48), cArr, 19);
        cArr[23] = '-';
        q((int) (leastSignificantBits2 >>> 32), cArr, 24);
        int i3 = (int) leastSignificantBits2;
        q(i3 >> 16, cArr, 28);
        q(i3, cArr, 32);
        dVar.m0(cArr, 0, 36);
    }

    public w0(Boolean bool) {
        super(UUID.class);
        this._asBinary = bool;
    }
}
