package b.g.a.c.g0.u;

import b.g.a.a.i;
import b.g.a.a.p;
import b.g.a.c.b;
import b.g.a.c.d;
import b.g.a.c.g0.h;
import b.g.a.c.g0.i;
import b.g.a.c.g0.t.l;
import b.g.a.c.h0.n;
import b.g.a.c.i0.g;
import b.g.a.c.j;
import b.g.a.c.v;
import b.g.a.c.w;
import b.g.a.c.x;
import b.g.a.c.y.a;
import com.fasterxml.jackson.databind.JsonMappingException;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
/* compiled from: MapSerializer.java */
@a
/* loaded from: classes2.dex */
public class t extends h<Map<?, ?>> implements i {
    public static final j k = n.k();
    public static final Object l = p.a.NON_EMPTY;
    private static final long serialVersionUID = 1;
    public l _dynamicValueSerializers;
    public final Object _filterId;
    public final Set<String> _ignoredEntries;
    public final Set<String> _includedEntries;
    public final g _inclusionChecker;
    public b.g.a.c.n<Object> _keySerializer;
    public final j _keyType;
    public final d _property;
    public final boolean _sortKeys;
    public final boolean _suppressNulls;
    public final Object _suppressableValue;
    public b.g.a.c.n<Object> _valueSerializer;
    public final j _valueType;
    public final boolean _valueTypeIsStatic;
    public final b.g.a.c.e0.g _valueTypeSerializer;

    public t(Set<String> set, Set<String> set2, j jVar, j jVar2, boolean z2, b.g.a.c.e0.g gVar, b.g.a.c.n<?> nVar, b.g.a.c.n<?> nVar2) {
        super(Map.class, false);
        g gVar2 = null;
        set = (set == null || set.isEmpty()) ? null : set;
        this._ignoredEntries = set;
        this._includedEntries = set2;
        this._keyType = jVar;
        this._valueType = jVar2;
        this._valueTypeIsStatic = z2;
        this._valueTypeSerializer = gVar;
        this._keySerializer = nVar;
        this._valueSerializer = nVar2;
        this._dynamicValueSerializers = l.b.f702b;
        this._property = null;
        this._filterId = null;
        this._sortKeys = false;
        this._suppressableValue = null;
        this._suppressNulls = false;
        if (set2 != null || (set != null && !set.isEmpty())) {
            gVar2 = new g(set, set2);
        }
        this._inclusionChecker = gVar2;
    }

    public static t s(Set<String> set, Set<String> set2, j jVar, boolean z2, b.g.a.c.e0.g gVar, b.g.a.c.n<Object> nVar, b.g.a.c.n<Object> nVar2, Object obj) {
        j jVar2;
        j jVar3;
        boolean z3;
        j jVar4;
        boolean z4 = true;
        if (jVar == null) {
            jVar3 = k;
            jVar2 = jVar3;
        } else {
            j o = jVar.o();
            if (jVar._class == Properties.class) {
                jVar4 = n.k();
            } else {
                jVar4 = jVar.k();
            }
            jVar2 = jVar4;
            jVar3 = o;
        }
        if (!z2) {
            if (jVar2 == null || !jVar2.x()) {
                z4 = false;
            }
            z3 = z4;
        } else {
            z3 = jVar2._class == Object.class ? false : z2;
        }
        t tVar = new t(set, set2, jVar3, jVar2, z3, gVar, nVar, nVar2);
        if (obj == null || tVar._filterId == obj) {
            return tVar;
        }
        b.g.a.c.i0.d.z(t.class, tVar, "withFilterId");
        return new t(tVar, obj, tVar._sortKeys);
    }

    @Override // b.g.a.c.g0.i
    public b.g.a.c.n<?> a(x xVar, d dVar) throws JsonMappingException {
        b.g.a.c.n<Object> nVar;
        b.g.a.c.n<?> nVar2;
        b.g.a.c.n<?> nVar3;
        Set<String> set;
        Set<String> set2;
        boolean z2;
        p.b bVar;
        p.a aVar;
        boolean z3;
        Object obj;
        Object g;
        Boolean b2;
        b v = xVar.v();
        Object obj2 = null;
        b.g.a.c.c0.i member = dVar == null ? null : dVar.getMember();
        if (q0.j(member, v)) {
            Object l2 = v.l(member);
            nVar2 = l2 != null ? xVar.H(member, l2) : null;
            Object c = v.c(member);
            nVar = c != null ? xVar.H(member, c) : null;
        } else {
            nVar2 = null;
            nVar = null;
        }
        if (nVar == null) {
            nVar = this._valueSerializer;
        }
        b.g.a.c.n<?> k2 = k(xVar, dVar, nVar);
        if (k2 == null && this._valueTypeIsStatic && !this._valueType.y()) {
            k2 = xVar.m(this._valueType, dVar);
        }
        b.g.a.c.n<?> nVar4 = k2;
        if (nVar2 == null) {
            nVar2 = this._keySerializer;
        }
        if (nVar2 == null) {
            nVar3 = xVar.o(this._keyType, dVar);
        } else {
            nVar3 = xVar.z(nVar2, dVar);
        }
        b.g.a.c.n<?> nVar5 = nVar3;
        Set<String> set3 = this._ignoredEntries;
        Set<String> set4 = this._includedEntries;
        if (q0.j(member, v)) {
            v vVar = xVar._config;
            Set<String> d = v.x(vVar, member).d();
            if (d != null && !d.isEmpty()) {
                set3 = set3 == null ? new HashSet<>() : new HashSet<>(set3);
                for (String str : d) {
                    set3.add(str);
                }
            }
            Set<String> set5 = v.A(vVar, member)._included;
            if (set5 != null) {
                set4 = set4 == null ? new HashSet<>() : new HashSet<>(set4);
                for (String str2 : set5) {
                    set4.add(str2);
                }
            }
            z2 = Boolean.TRUE.equals(v.J(member));
            set2 = set3;
            set = set4;
        } else {
            set2 = set3;
            set = set4;
            z2 = false;
        }
        i.d l3 = l(xVar, dVar, Map.class);
        if (!(l3 == null || (b2 = l3.b(i.a.WRITE_SORTED_MAP_ENTRIES)) == null)) {
            z2 = b2.booleanValue();
        }
        boolean z4 = z2;
        b.g.a.c.i0.d.z(t.class, this, "withResolved");
        t tVar = new t(this, dVar, nVar5, nVar4, set2, set);
        t tVar2 = z4 != tVar._sortKeys ? new t(tVar, this._filterId, z4) : tVar;
        if (!(member == null || (g = v.g(member)) == null || tVar2._filterId == g)) {
            b.g.a.c.i0.d.z(t.class, tVar2, "withFilterId");
            tVar2 = new t(tVar2, g, tVar2._sortKeys);
        }
        if (dVar != null) {
            bVar = dVar.b(xVar._config, Map.class);
        } else {
            bVar = xVar._config.j(Map.class);
        }
        if (bVar == null || (aVar = bVar._contentInclusion) == p.a.USE_DEFAULTS) {
            return tVar2;
        }
        int ordinal = aVar.ordinal();
        if (ordinal != 1) {
            if (ordinal != 2) {
                if (ordinal == 3) {
                    obj = l;
                } else if (ordinal == 4) {
                    obj = b.c.a.a0.d.t0(this._valueType);
                    if (obj != null && obj.getClass().isArray()) {
                        obj = b.c.a.a0.d.q0(obj);
                    }
                } else if (ordinal != 5) {
                    z3 = false;
                } else {
                    obj2 = xVar.A(null, bVar._contentFilter);
                    if (obj2 != null) {
                        z3 = xVar.B(obj2);
                    }
                }
                obj2 = obj;
            } else if (this._valueType.b()) {
                obj = l;
                obj2 = obj;
            }
            return tVar2.v(obj2, z3);
        }
        z3 = true;
        return tVar2.v(obj2, z3);
    }

    @Override // b.g.a.c.n
    public boolean d(x xVar, Object obj) {
        Map map = (Map) obj;
        if (!map.isEmpty()) {
            Object obj2 = this._suppressableValue;
            if (obj2 == null && !this._suppressNulls) {
                return false;
            }
            b.g.a.c.n<Object> nVar = this._valueSerializer;
            boolean z2 = l == obj2;
            if (nVar != null) {
                for (Object obj3 : map.values()) {
                    if (obj3 == null) {
                        if (!this._suppressNulls) {
                            return false;
                        }
                    } else if (z2) {
                        if (!nVar.d(xVar, obj3)) {
                            return false;
                        }
                    } else if (obj2 == null || !obj2.equals(map)) {
                        return false;
                    }
                }
            } else {
                for (Object obj4 : map.values()) {
                    if (obj4 != null) {
                        try {
                            b.g.a.c.n<Object> r = r(xVar, obj4);
                            if (z2) {
                                if (!r.d(xVar, obj4)) {
                                    return false;
                                }
                            } else if (obj2 == null || !obj2.equals(map)) {
                                return false;
                            }
                        } catch (JsonMappingException unused) {
                            return false;
                        }
                    } else if (!this._suppressNulls) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    @Override // b.g.a.c.n
    public void f(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
        Map<?, ?> map = (Map) obj;
        dVar.d0(map);
        u(map, dVar, xVar);
        dVar.u();
    }

    @Override // b.g.a.c.n
    public void g(Object obj, b.g.a.b.d dVar, x xVar, b.g.a.c.e0.g gVar) throws IOException {
        Map<?, ?> map = (Map) obj;
        dVar.e(map);
        b.g.a.b.s.b e = gVar.e(dVar, gVar.d(map, b.g.a.b.h.START_OBJECT));
        u(map, dVar, xVar);
        gVar.f(dVar, e);
    }

    @Override // b.g.a.c.g0.h
    public h p(b.g.a.c.e0.g gVar) {
        if (this._valueTypeSerializer == gVar) {
            return this;
        }
        b.g.a.c.i0.d.z(t.class, this, "_withValueTypeSerializer");
        return new t(this, gVar, this._suppressableValue, this._suppressNulls);
    }

    public final b.g.a.c.n<Object> r(x xVar, Object obj) throws JsonMappingException {
        Class<?> cls = obj.getClass();
        b.g.a.c.n<Object> c = this._dynamicValueSerializers.c(cls);
        if (c != null) {
            return c;
        }
        if (this._valueType.r()) {
            l lVar = this._dynamicValueSerializers;
            l.d a = lVar.a(xVar.k(this._valueType, cls), xVar, this._property);
            l lVar2 = a.f704b;
            if (lVar != lVar2) {
                this._dynamicValueSerializers = lVar2;
            }
            return a.a;
        }
        l lVar3 = this._dynamicValueSerializers;
        d dVar = this._property;
        Objects.requireNonNull(lVar3);
        b.g.a.c.n<Object> n = xVar.n(cls, dVar);
        l b2 = lVar3.b(cls, n);
        if (lVar3 != b2) {
            this._dynamicValueSerializers = b2;
        }
        return n;
    }

    public void t(Map<?, ?> map, b.g.a.b.d dVar, x xVar, Object obj) throws IOException {
        b.g.a.c.n<Object> nVar;
        b.g.a.c.n<Object> nVar2;
        boolean z2 = l == obj;
        for (Map.Entry<?, ?> entry : map.entrySet()) {
            Object key = entry.getKey();
            if (key == null) {
                nVar = xVar._nullKeySerializer;
            } else {
                g gVar = this._inclusionChecker;
                if (gVar == null || !gVar.a(key)) {
                    nVar = this._keySerializer;
                }
            }
            Object value = entry.getValue();
            if (value != null) {
                nVar2 = this._valueSerializer;
                if (nVar2 == null) {
                    nVar2 = r(xVar, value);
                }
                if (!z2) {
                    if (obj != null && obj.equals(value)) {
                    }
                    nVar.f(key, dVar, xVar);
                    nVar2.g(value, dVar, xVar, this._valueTypeSerializer);
                } else if (nVar2.d(xVar, value)) {
                    continue;
                } else {
                    nVar.f(key, dVar, xVar);
                    nVar2.g(value, dVar, xVar, this._valueTypeSerializer);
                }
            } else if (this._suppressNulls) {
                continue;
            } else {
                nVar2 = xVar._nullValueSerializer;
                nVar.f(key, dVar, xVar);
                try {
                    nVar2.g(value, dVar, xVar, this._valueTypeSerializer);
                } catch (Exception e) {
                    o(xVar, e, map, String.valueOf(key));
                    throw null;
                }
            }
        }
    }

    public void u(Map<?, ?> map, b.g.a.b.d dVar, x xVar) throws IOException {
        b.g.a.c.n<Object> nVar;
        b.g.a.c.n<Object> nVar2;
        Exception e;
        Object obj;
        TreeMap treeMap;
        b.g.a.c.n<Object> nVar3;
        if (!map.isEmpty()) {
            boolean z2 = true;
            if ((this._sortKeys || xVar.D(w.ORDER_MAP_ENTRIES_BY_KEYS)) && !(map instanceof SortedMap)) {
                if ((map instanceof HashMap) && map.containsKey(null)) {
                    treeMap = new TreeMap();
                    for (Map.Entry<?, ?> entry : map.entrySet()) {
                        Object key = entry.getKey();
                        if (key == null) {
                            Object value = entry.getValue();
                            b.g.a.c.n<Object> nVar4 = xVar._nullKeySerializer;
                            if (value != null) {
                                nVar3 = this._valueSerializer;
                                if (nVar3 == null) {
                                    nVar3 = r(xVar, value);
                                }
                                Object obj2 = this._suppressableValue;
                                if (obj2 == l) {
                                    if (nVar3.d(xVar, value)) {
                                        continue;
                                    }
                                    nVar4.f(null, dVar, xVar);
                                    nVar3.f(value, dVar, xVar);
                                } else {
                                    if (obj2 != null && obj2.equals(value)) {
                                    }
                                    nVar4.f(null, dVar, xVar);
                                    nVar3.f(value, dVar, xVar);
                                }
                            } else if (this._suppressNulls) {
                                continue;
                            } else {
                                nVar3 = xVar._nullValueSerializer;
                                try {
                                    nVar4.f(null, dVar, xVar);
                                    nVar3.f(value, dVar, xVar);
                                } catch (Exception e2) {
                                    o(xVar, e2, value, "");
                                    throw null;
                                }
                            }
                        } else {
                            treeMap.put(key, entry.getValue());
                        }
                    }
                } else {
                    treeMap = new TreeMap(map);
                }
                map = treeMap;
            }
            Object obj3 = this._filterId;
            if (obj3 == null) {
                Object obj4 = this._suppressableValue;
                if (obj4 == null && !this._suppressNulls) {
                    b.g.a.c.n<Object> nVar5 = this._valueSerializer;
                    if (nVar5 != null) {
                        b.g.a.c.n<Object> nVar6 = this._keySerializer;
                        b.g.a.c.e0.g gVar = this._valueTypeSerializer;
                        for (Map.Entry<?, ?> entry2 : map.entrySet()) {
                            Object key2 = entry2.getKey();
                            g gVar2 = this._inclusionChecker;
                            if (gVar2 == null || !gVar2.a(key2)) {
                                if (key2 == null) {
                                    xVar._nullKeySerializer.f(null, dVar, xVar);
                                } else {
                                    nVar6.f(key2, dVar, xVar);
                                }
                                Object value2 = entry2.getValue();
                                if (value2 == null) {
                                    xVar.l(dVar);
                                } else if (gVar == null) {
                                    try {
                                        nVar5.f(value2, dVar, xVar);
                                    } catch (Exception e3) {
                                        o(xVar, e3, map, String.valueOf(key2));
                                        throw null;
                                    }
                                } else {
                                    nVar5.g(value2, dVar, xVar, gVar);
                                }
                            }
                        }
                    } else if (this._valueTypeSerializer != null) {
                        t(map, dVar, xVar, null);
                    } else {
                        b.g.a.c.n<Object> nVar7 = this._keySerializer;
                        try {
                            obj = null;
                            for (Map.Entry<?, ?> entry3 : map.entrySet()) {
                                try {
                                    Object value3 = entry3.getValue();
                                    obj = entry3.getKey();
                                    if (obj == null) {
                                        xVar._nullKeySerializer.f(null, dVar, xVar);
                                    } else {
                                        g gVar3 = this._inclusionChecker;
                                        if (gVar3 == null || !gVar3.a(obj)) {
                                            nVar7.f(obj, dVar, xVar);
                                        }
                                    }
                                    if (value3 == null) {
                                        xVar.l(dVar);
                                    } else {
                                        b.g.a.c.n<Object> nVar8 = this._valueSerializer;
                                        if (nVar8 == null) {
                                            nVar8 = r(xVar, value3);
                                        }
                                        nVar8.f(value3, dVar, xVar);
                                    }
                                } catch (Exception e4) {
                                    e = e4;
                                    o(xVar, e, map, String.valueOf(obj));
                                    throw null;
                                }
                            }
                        } catch (Exception e5) {
                            e = e5;
                            obj = null;
                        }
                    }
                } else if (this._valueTypeSerializer != null) {
                    t(map, dVar, xVar, obj4);
                } else {
                    if (l != obj4) {
                        z2 = false;
                    }
                    for (Map.Entry<?, ?> entry4 : map.entrySet()) {
                        Object key3 = entry4.getKey();
                        if (key3 == null) {
                            nVar = xVar._nullKeySerializer;
                        } else {
                            g gVar4 = this._inclusionChecker;
                            if (gVar4 == null || !gVar4.a(key3)) {
                                nVar = this._keySerializer;
                            }
                        }
                        Object value4 = entry4.getValue();
                        if (value4 != null) {
                            nVar2 = this._valueSerializer;
                            if (nVar2 == null) {
                                nVar2 = r(xVar, value4);
                            }
                            if (z2) {
                                if (nVar2.d(xVar, value4)) {
                                    continue;
                                }
                                nVar.f(key3, dVar, xVar);
                                nVar2.f(value4, dVar, xVar);
                            } else {
                                if (obj4 != null && obj4.equals(value4)) {
                                }
                                nVar.f(key3, dVar, xVar);
                                nVar2.f(value4, dVar, xVar);
                            }
                        } else if (this._suppressNulls) {
                            continue;
                        } else {
                            nVar2 = xVar._nullValueSerializer;
                            try {
                                nVar.f(key3, dVar, xVar);
                                nVar2.f(value4, dVar, xVar);
                            } catch (Exception e6) {
                                o(xVar, e6, map, String.valueOf(key3));
                                throw null;
                            }
                        }
                    }
                }
            } else {
                m(xVar, obj3, map);
                throw null;
            }
        }
    }

    public t v(Object obj, boolean z2) {
        if (obj == this._suppressableValue && z2 == this._suppressNulls) {
            return this;
        }
        b.g.a.c.i0.d.z(t.class, this, "withContentInclusion");
        return new t(this, this._valueTypeSerializer, obj, z2);
    }

    public t(t tVar, d dVar, b.g.a.c.n<?> nVar, b.g.a.c.n<?> nVar2, Set<String> set, Set<String> set2) {
        super(Map.class, false);
        g gVar = null;
        set = (set == null || set.isEmpty()) ? null : set;
        this._ignoredEntries = set;
        this._includedEntries = set2;
        this._keyType = tVar._keyType;
        this._valueType = tVar._valueType;
        this._valueTypeIsStatic = tVar._valueTypeIsStatic;
        this._valueTypeSerializer = tVar._valueTypeSerializer;
        this._keySerializer = nVar;
        this._valueSerializer = nVar2;
        this._dynamicValueSerializers = l.b.f702b;
        this._property = dVar;
        this._filterId = tVar._filterId;
        this._sortKeys = tVar._sortKeys;
        this._suppressableValue = tVar._suppressableValue;
        this._suppressNulls = tVar._suppressNulls;
        if (set2 != null || (set != null && !set.isEmpty())) {
            gVar = new g(set, set2);
        }
        this._inclusionChecker = gVar;
    }

    public t(t tVar, b.g.a.c.e0.g gVar, Object obj, boolean z2) {
        super(Map.class, false);
        this._ignoredEntries = tVar._ignoredEntries;
        this._includedEntries = tVar._includedEntries;
        this._keyType = tVar._keyType;
        this._valueType = tVar._valueType;
        this._valueTypeIsStatic = tVar._valueTypeIsStatic;
        this._valueTypeSerializer = gVar;
        this._keySerializer = tVar._keySerializer;
        this._valueSerializer = tVar._valueSerializer;
        this._dynamicValueSerializers = tVar._dynamicValueSerializers;
        this._property = tVar._property;
        this._filterId = tVar._filterId;
        this._sortKeys = tVar._sortKeys;
        this._suppressableValue = obj;
        this._suppressNulls = z2;
        this._inclusionChecker = tVar._inclusionChecker;
    }

    public t(t tVar, Object obj, boolean z2) {
        super(Map.class, false);
        this._ignoredEntries = tVar._ignoredEntries;
        this._includedEntries = tVar._includedEntries;
        this._keyType = tVar._keyType;
        this._valueType = tVar._valueType;
        this._valueTypeIsStatic = tVar._valueTypeIsStatic;
        this._valueTypeSerializer = tVar._valueTypeSerializer;
        this._keySerializer = tVar._keySerializer;
        this._valueSerializer = tVar._valueSerializer;
        this._dynamicValueSerializers = l.b.f702b;
        this._property = tVar._property;
        this._filterId = obj;
        this._sortKeys = z2;
        this._suppressableValue = tVar._suppressableValue;
        this._suppressNulls = tVar._suppressNulls;
        this._inclusionChecker = tVar._inclusionChecker;
    }
}
