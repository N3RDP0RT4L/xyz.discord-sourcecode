package b.g.a.c.g0.u;

import b.g.a.c.g0.t.l;
import b.g.a.c.i0.f;
import b.g.a.c.n;
import b.g.a.c.w;
import b.g.a.c.x;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
/* compiled from: StdKeySerializers.java */
/* loaded from: classes2.dex */
public abstract class o0 {
    public static final n<Object> a = new d();

    /* compiled from: StdKeySerializers.java */
    /* loaded from: classes2.dex */
    public static class a extends q0<Object> {
        public final int _typeId;

        public a(int i, Class<?> cls) {
            super(cls, false);
            this._typeId = i;
        }

        @Override // b.g.a.c.n
        public void f(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
            String str;
            switch (this._typeId) {
                case 1:
                    Date date = (Date) obj;
                    Objects.requireNonNull(xVar);
                    if (xVar.D(w.WRITE_DATE_KEYS_AS_TIMESTAMPS)) {
                        dVar.y(String.valueOf(date.getTime()));
                        return;
                    } else {
                        dVar.y(xVar.j().format(date));
                        return;
                    }
                case 2:
                    long timeInMillis = ((Calendar) obj).getTimeInMillis();
                    Objects.requireNonNull(xVar);
                    if (xVar.D(w.WRITE_DATE_KEYS_AS_TIMESTAMPS)) {
                        dVar.y(String.valueOf(timeInMillis));
                        return;
                    } else {
                        dVar.y(xVar.j().format(new Date(timeInMillis)));
                        return;
                    }
                case 3:
                    dVar.y(((Class) obj).getName());
                    return;
                case 4:
                    if (xVar.D(w.WRITE_ENUMS_USING_TO_STRING)) {
                        str = obj.toString();
                    } else {
                        Enum r3 = (Enum) obj;
                        if (xVar.D(w.WRITE_ENUM_KEYS_USING_INDEX)) {
                            str = String.valueOf(r3.ordinal());
                        } else {
                            str = r3.name();
                        }
                    }
                    dVar.y(str);
                    return;
                case 5:
                case 6:
                    long longValue = ((Number) obj).longValue();
                    Objects.requireNonNull(dVar);
                    dVar.y(Long.toString(longValue));
                    return;
                case 7:
                    dVar.y(xVar._config._base._defaultBase64.a((byte[]) obj));
                    return;
                default:
                    dVar.y(obj.toString());
                    return;
            }
        }
    }

    /* compiled from: StdKeySerializers.java */
    /* loaded from: classes2.dex */
    public static class b extends q0<Object> {
        public transient l k = l.b.f702b;

        public b() {
            super(String.class, false);
        }

        @Override // b.g.a.c.n
        public void f(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
            Class<?> cls = obj.getClass();
            l lVar = this.k;
            n<Object> c = lVar.c(cls);
            if (c == null) {
                if (cls == Object.class) {
                    c = new a(8, cls);
                    this.k = lVar.b(cls, c);
                } else {
                    c = xVar.o(xVar._config._base._typeFactory.b(null, cls, b.g.a.c.h0.n.l), null);
                    l b2 = lVar.b(cls, c);
                    if (lVar != b2) {
                        this.k = b2;
                    }
                }
            }
            c.f(obj, dVar, xVar);
        }

        public Object readResolve() {
            this.k = l.b.f702b;
            return this;
        }
    }

    /* compiled from: StdKeySerializers.java */
    /* loaded from: classes2.dex */
    public static class c extends q0<Object> {
        public final f _values;

        public c(Class<?> cls, f fVar) {
            super(cls, false);
            this._values = fVar;
        }

        @Override // b.g.a.c.n
        public void f(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
            if (xVar.D(w.WRITE_ENUMS_USING_TO_STRING)) {
                dVar.y(obj.toString());
                return;
            }
            Enum<?> r2 = (Enum) obj;
            if (xVar.D(w.WRITE_ENUM_KEYS_USING_INDEX)) {
                dVar.y(String.valueOf(r2.ordinal()));
            } else {
                dVar.x(this._values.c(r2));
            }
        }
    }

    /* compiled from: StdKeySerializers.java */
    /* loaded from: classes2.dex */
    public static class d extends q0<Object> {
        public d() {
            super(String.class, false);
        }

        @Override // b.g.a.c.n
        public void f(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
            dVar.y((String) obj);
        }
    }
}
