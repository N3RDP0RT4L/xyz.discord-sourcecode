package b.g.a.c.g0.u;

import b.g.a.b.d;
import b.g.a.b.h;
import b.g.a.c.e0.g;
import b.g.a.c.i0.s;
import b.g.a.c.x;
import b.g.a.c.y.a;
import java.io.IOException;
import java.util.Objects;
/* compiled from: TokenBufferSerializer.java */
@a
/* loaded from: classes2.dex */
public class v0 extends q0<s> {
    public v0() {
        super(s.class);
    }

    @Override // b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        Objects.requireNonNull((s) obj);
        Objects.requireNonNull(null);
        throw null;
    }

    @Override // b.g.a.c.n
    public void g(Object obj, d dVar, x xVar, g gVar) throws IOException {
        s sVar = (s) obj;
        gVar.e(dVar, gVar.d(sVar, h.VALUE_EMBEDDED_OBJECT));
        Objects.requireNonNull(sVar);
        Objects.requireNonNull(null);
        throw null;
    }
}
