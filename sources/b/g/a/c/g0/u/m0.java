package b.g.a.c.g0.u;

import b.g.a.b.d;
import b.g.a.c.x;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;
/* compiled from: StdJdkSerializers.java */
/* loaded from: classes2.dex */
public class m0 extends p0<AtomicLong> {
    public m0() {
        super(AtomicLong.class, false);
    }

    @Override // b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        dVar.I(((AtomicLong) obj).get());
    }
}
