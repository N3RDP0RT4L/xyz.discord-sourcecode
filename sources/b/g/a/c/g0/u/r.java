package b.g.a.c.g0.u;

import b.g.a.b.d;
import b.g.a.c.e0.g;
import b.g.a.c.g0.h;
import b.g.a.c.j;
import b.g.a.c.n;
import b.g.a.c.w;
import b.g.a.c.x;
import b.g.a.c.y.a;
import java.io.IOException;
import java.util.Iterator;
/* compiled from: IterableSerializer.java */
@a
/* loaded from: classes2.dex */
public class r extends b<Iterable<?>> {
    public r(j jVar, boolean z2, g gVar) {
        super(Iterable.class, jVar, z2, gVar, (n<Object>) null);
    }

    @Override // b.g.a.c.n
    public boolean d(x xVar, Object obj) {
        return !((Iterable) obj).iterator().hasNext();
    }

    @Override // b.g.a.c.g0.u.b, b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        Iterable<?> iterable = (Iterable) obj;
        if (((this._unwrapSingle != null || !xVar.D(w.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) && this._unwrapSingle != Boolean.TRUE) || !q(iterable)) {
            dVar.X(iterable);
            s(iterable, dVar, xVar);
            dVar.t();
            return;
        }
        s(iterable, dVar, xVar);
    }

    @Override // b.g.a.c.g0.h
    public h<?> p(g gVar) {
        return new r(this, this._property, gVar, this._elementSerializer, this._unwrapSingle);
    }

    @Override // b.g.a.c.g0.u.b
    public b<Iterable<?>> t(b.g.a.c.d dVar, g gVar, n nVar, Boolean bool) {
        return new r(this, dVar, gVar, nVar, bool);
    }

    /* renamed from: u */
    public boolean q(Iterable<?> iterable) {
        if (iterable == null) {
            return false;
        }
        Iterator<?> it = iterable.iterator();
        if (!it.hasNext()) {
            return false;
        }
        it.next();
        return !it.hasNext();
    }

    /* renamed from: v */
    public void s(Iterable<?> iterable, d dVar, x xVar) throws IOException {
        n<Object> nVar;
        Iterator<?> it = iterable.iterator();
        if (it.hasNext()) {
            g gVar = this._valueTypeSerializer;
            Class<?> cls = null;
            n<Object> nVar2 = null;
            do {
                Object next = it.next();
                if (next == null) {
                    xVar.l(dVar);
                } else {
                    n<Object> nVar3 = this._elementSerializer;
                    if (nVar3 == null) {
                        Class<?> cls2 = next.getClass();
                        if (cls2 != cls) {
                            nVar2 = xVar.u(cls2, this._property);
                            cls = cls2;
                        }
                        nVar = nVar2;
                    } else {
                        nVar = nVar2;
                        nVar2 = nVar3;
                    }
                    if (gVar == null) {
                        nVar2.f(next, dVar, xVar);
                    } else {
                        nVar2.g(next, dVar, xVar, gVar);
                    }
                    nVar2 = nVar;
                }
            } while (it.hasNext());
        }
    }

    public r(r rVar, b.g.a.c.d dVar, g gVar, n<?> nVar, Boolean bool) {
        super(rVar, dVar, gVar, nVar, bool);
    }
}
