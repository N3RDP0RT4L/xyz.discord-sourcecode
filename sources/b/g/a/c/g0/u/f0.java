package b.g.a.c.g0.u;

import b.g.a.a.p;
import b.g.a.c.d;
import b.g.a.c.e0.g;
import b.g.a.c.g0.i;
import b.g.a.c.g0.t.l;
import b.g.a.c.i0.n;
import b.g.a.c.j;
import b.g.a.c.x;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.RuntimeJsonMappingException;
import java.io.IOException;
/* compiled from: ReferenceTypeSerializer.java */
/* loaded from: classes2.dex */
public abstract class f0<T> extends q0<T> implements i {
    private static final long serialVersionUID = 1;
    public final d _property;
    public final j _referredType;
    public final boolean _suppressNulls;
    public final Object _suppressableValue;
    public final n _unwrapper;
    public final b.g.a.c.n<Object> _valueSerializer;
    public final g _valueTypeSerializer;
    public transient l k;

    public f0(b.g.a.c.h0.i iVar, g gVar, b.g.a.c.n nVar) {
        super(iVar);
        this._referredType = iVar._referencedType;
        this._property = null;
        this._valueTypeSerializer = gVar;
        this._valueSerializer = nVar;
        this._unwrapper = null;
        this._suppressableValue = null;
        this._suppressNulls = false;
        this.k = l.b.f702b;
    }

    /* JADX WARN: Code restructure failed: missing block: B:33:0x005f, code lost:
        if (r6 == b.g.a.c.y.e.b.DYNAMIC) goto L19;
     */
    /* JADX WARN: Code restructure failed: missing block: B:72:0x00db, code lost:
        if (r8._referredType.b() != false) goto L70;
     */
    /* JADX WARN: Removed duplicated region for block: B:15:0x0027  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x006a  */
    /* JADX WARN: Removed duplicated region for block: B:76:0x00e4  */
    @Override // b.g.a.c.g0.i
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.g.a.c.n<?> a(b.g.a.c.x r9, b.g.a.c.d r10) throws com.fasterxml.jackson.databind.JsonMappingException {
        /*
            Method dump skipped, instructions count: 237
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.c.g0.u.f0.a(b.g.a.c.x, b.g.a.c.d):b.g.a.c.n");
    }

    @Override // b.g.a.c.n
    public boolean d(x xVar, T t) {
        if (!s(t)) {
            return true;
        }
        Object q = q(t);
        if (q == null) {
            return this._suppressNulls;
        }
        if (this._suppressableValue == null) {
            return false;
        }
        b.g.a.c.n<Object> nVar = this._valueSerializer;
        if (nVar == null) {
            try {
                nVar = p(xVar, q.getClass());
            } catch (JsonMappingException e) {
                throw new RuntimeJsonMappingException(e);
            }
        }
        Object obj = this._suppressableValue;
        if (obj == p.a.NON_EMPTY) {
            return nVar.d(xVar, q);
        }
        return obj.equals(q);
    }

    @Override // b.g.a.c.n
    public boolean e() {
        return this._unwrapper != null;
    }

    @Override // b.g.a.c.n
    public void f(T t, b.g.a.b.d dVar, x xVar) throws IOException {
        Object r = r(t);
        if (r != null) {
            b.g.a.c.n<Object> nVar = this._valueSerializer;
            if (nVar == null) {
                nVar = p(xVar, r.getClass());
            }
            g gVar = this._valueTypeSerializer;
            if (gVar != null) {
                nVar.g(r, dVar, xVar, gVar);
            } else {
                nVar.f(r, dVar, xVar);
            }
        } else if (this._unwrapper == null) {
            xVar.l(dVar);
        }
    }

    @Override // b.g.a.c.n
    public void g(T t, b.g.a.b.d dVar, x xVar, g gVar) throws IOException {
        Object r = r(t);
        if (r != null) {
            b.g.a.c.n<Object> nVar = this._valueSerializer;
            if (nVar == null) {
                nVar = p(xVar, r.getClass());
            }
            nVar.g(r, dVar, xVar, gVar);
        } else if (this._unwrapper == null) {
            xVar.l(dVar);
        }
    }

    @Override // b.g.a.c.n
    public b.g.a.c.n<T> h(n nVar) {
        b.g.a.c.n<?> nVar2 = this._valueSerializer;
        if (nVar2 != null && (nVar2 = nVar2.h(nVar)) == this._valueSerializer) {
            return this;
        }
        n nVar3 = this._unwrapper;
        if (nVar3 != null) {
            nVar = new n.a(nVar, nVar3);
        }
        return (this._valueSerializer == nVar2 && nVar3 == nVar) ? this : u(this._property, this._valueTypeSerializer, nVar2, nVar);
    }

    public final b.g.a.c.n<Object> p(x xVar, Class<?> cls) throws JsonMappingException {
        b.g.a.c.n<Object> nVar;
        b.g.a.c.n<Object> c = this.k.c(cls);
        if (c != null) {
            return c;
        }
        if (this._referredType.r()) {
            nVar = xVar.q(xVar.k(this._referredType, cls), this._property);
        } else {
            nVar = xVar.r(cls, this._property);
        }
        n nVar2 = this._unwrapper;
        if (nVar2 != null) {
            nVar = nVar.h(nVar2);
        }
        b.g.a.c.n<Object> nVar3 = nVar;
        this.k = this.k.b(cls, nVar3);
        return nVar3;
    }

    public abstract Object q(T t);

    public abstract Object r(T t);

    public abstract boolean s(T t);

    public abstract f0<T> t(Object obj, boolean z2);

    public abstract f0<T> u(d dVar, g gVar, b.g.a.c.n<?> nVar, n nVar2);

    public f0(f0<?> f0Var, d dVar, g gVar, b.g.a.c.n<?> nVar, n nVar2, Object obj, boolean z2) {
        super(f0Var);
        this._referredType = f0Var._referredType;
        this.k = l.b.f702b;
        this._property = dVar;
        this._valueTypeSerializer = gVar;
        this._valueSerializer = nVar;
        this._unwrapper = nVar2;
        this._suppressableValue = obj;
        this._suppressNulls = z2;
    }
}
