package b.g.a.c.g0.u;

import b.g.a.c.d;
import b.g.a.c.e0.g;
import b.g.a.c.g0.i;
import b.g.a.c.g0.o;
import b.g.a.c.i0.e;
import b.g.a.c.j;
import b.g.a.c.n;
import b.g.a.c.x;
import com.fasterxml.jackson.databind.JsonMappingException;
import java.io.IOException;
/* compiled from: StdDelegatingSerializer.java */
/* loaded from: classes2.dex */
public class j0 extends q0<Object> implements i, o {
    public final e<Object, ?> _converter;
    public final n<Object> _delegateSerializer;
    public final j _delegateType;

    public j0(e<Object, ?> eVar, j jVar, n<?> nVar) {
        super(jVar);
        this._converter = eVar;
        this._delegateType = jVar;
        this._delegateSerializer = nVar;
    }

    @Override // b.g.a.c.g0.i
    public n<?> a(x xVar, d dVar) throws JsonMappingException {
        n<?> nVar = this._delegateSerializer;
        j jVar = this._delegateType;
        if (nVar == null) {
            if (jVar == null) {
                jVar = this._converter.a(xVar.d());
            }
            if (!jVar.y()) {
                nVar = xVar.s(jVar);
            }
        }
        if (nVar instanceof i) {
            nVar = xVar.z(nVar, dVar);
        }
        if (nVar == this._delegateSerializer && jVar == this._delegateType) {
            return this;
        }
        e<Object, ?> eVar = this._converter;
        b.g.a.c.i0.d.z(j0.class, this, "withDelegate");
        return new j0(eVar, jVar, nVar);
    }

    @Override // b.g.a.c.g0.o
    public void b(x xVar) throws JsonMappingException {
        n<Object> nVar = this._delegateSerializer;
        if (nVar != null && (nVar instanceof o)) {
            ((o) nVar).b(xVar);
        }
    }

    @Override // b.g.a.c.n
    public boolean d(x xVar, Object obj) {
        Object convert = this._converter.convert(obj);
        if (convert == null) {
            return true;
        }
        n<Object> nVar = this._delegateSerializer;
        if (nVar == null) {
            return obj == null;
        }
        return nVar.d(xVar, convert);
    }

    @Override // b.g.a.c.n
    public void f(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
        Object convert = this._converter.convert(obj);
        if (convert == null) {
            xVar.l(dVar);
            return;
        }
        n<Object> nVar = this._delegateSerializer;
        if (nVar == null) {
            nVar = p(convert, xVar);
        }
        nVar.f(convert, dVar, xVar);
    }

    @Override // b.g.a.c.n
    public void g(Object obj, b.g.a.b.d dVar, x xVar, g gVar) throws IOException {
        Object convert = this._converter.convert(obj);
        n<Object> nVar = this._delegateSerializer;
        if (nVar == null) {
            nVar = p(obj, xVar);
        }
        nVar.g(convert, dVar, xVar, gVar);
    }

    public n<Object> p(Object obj, x xVar) throws JsonMappingException {
        Class<?> cls = obj.getClass();
        n<Object> b2 = xVar._knownSerializers.b(cls);
        if (b2 != null) {
            return b2;
        }
        n<Object> b3 = xVar._serializerCache.b(cls);
        if (b3 != null) {
            return b3;
        }
        n<Object> a = xVar._serializerCache.a(xVar._config._base._typeFactory.b(null, cls, b.g.a.c.h0.n.l));
        if (a != null) {
            return a;
        }
        n<Object> i = xVar.i(cls);
        return i == null ? xVar.x(cls) : i;
    }
}
