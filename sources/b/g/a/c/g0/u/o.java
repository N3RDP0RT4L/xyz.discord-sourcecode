package b.g.a.c.g0.u;

import b.g.a.b.d;
import b.g.a.c.x;
import java.io.File;
import java.io.IOException;
/* compiled from: FileSerializer.java */
/* loaded from: classes2.dex */
public class o extends p0<File> {
    public o() {
        super(File.class);
    }

    @Override // b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        dVar.j0(((File) obj).getAbsolutePath());
    }
}
