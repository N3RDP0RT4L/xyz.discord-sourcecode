package b.g.a.c.g0.u;

import b.g.a.b.d;
import b.g.a.b.h;
import b.g.a.b.s.b;
import b.g.a.c.e0.g;
import b.g.a.c.x;
import java.io.IOException;
/* compiled from: RawSerializer.java */
/* loaded from: classes2.dex */
public class e0<T> extends q0<T> {
    public e0(Class<?> cls) {
        super(cls, false);
    }

    @Override // b.g.a.c.n
    public void f(T t, d dVar, x xVar) throws IOException {
        dVar.V(t.toString());
    }

    @Override // b.g.a.c.n
    public void g(T t, d dVar, x xVar, g gVar) throws IOException {
        b e = gVar.e(dVar, gVar.d(t, h.VALUE_EMBEDDED_OBJECT));
        dVar.V(t.toString());
        gVar.f(dVar, e);
    }
}
