package b.g.a.c.g0.u;

import b.g.a.b.d;
import b.g.a.b.h;
import b.g.a.b.s.b;
import b.g.a.c.e0.g;
import b.g.a.c.x;
import java.io.IOException;
/* compiled from: ToStringSerializerBase.java */
/* loaded from: classes2.dex */
public abstract class u0 extends q0<Object> {
    public u0(Class<?> cls) {
        super(cls, false);
    }

    @Override // b.g.a.c.n
    public boolean d(x xVar, Object obj) {
        return p(obj).isEmpty();
    }

    @Override // b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        dVar.j0(p(obj));
    }

    @Override // b.g.a.c.n
    public void g(Object obj, d dVar, x xVar, g gVar) throws IOException {
        b e = gVar.e(dVar, gVar.d(obj, h.VALUE_STRING));
        f(obj, dVar, xVar);
        gVar.f(dVar, e);
    }

    public abstract String p(Object obj);
}
