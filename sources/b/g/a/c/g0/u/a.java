package b.g.a.c.g0.u;

import b.g.a.a.i;
import b.g.a.b.s.b;
import b.g.a.c.d;
import b.g.a.c.e0.g;
import b.g.a.c.g0.h;
import b.g.a.c.g0.i;
import b.g.a.c.n;
import b.g.a.c.w;
import b.g.a.c.x;
import com.fasterxml.jackson.databind.JsonMappingException;
import java.io.IOException;
import java.util.Objects;
/* compiled from: ArraySerializerBase.java */
/* loaded from: classes2.dex */
public abstract class a<T> extends h<T> implements i {
    public final d _property;
    public final Boolean _unwrapSingle;

    public a(Class<T> cls) {
        super(cls);
        this._property = null;
        this._unwrapSingle = null;
    }

    public n<?> a(x xVar, d dVar) throws JsonMappingException {
        i.d l;
        if (!(dVar == null || (l = l(xVar, dVar, this._handledType)) == null)) {
            Boolean b2 = l.b(i.a.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED);
            if (!Objects.equals(b2, this._unwrapSingle)) {
                return s(dVar, b2);
            }
        }
        return this;
    }

    @Override // b.g.a.c.n
    public void f(T t, b.g.a.b.d dVar, x xVar) throws IOException {
        if (!r(xVar) || !q(t)) {
            dVar.X(t);
            t(t, dVar, xVar);
            dVar.t();
            return;
        }
        t(t, dVar, xVar);
    }

    @Override // b.g.a.c.n
    public final void g(T t, b.g.a.b.d dVar, x xVar, g gVar) throws IOException {
        b e = gVar.e(dVar, gVar.d(t, b.g.a.b.h.START_ARRAY));
        dVar.e(t);
        t(t, dVar, xVar);
        gVar.f(dVar, e);
    }

    public final boolean r(x xVar) {
        Boolean bool = this._unwrapSingle;
        if (bool == null) {
            return xVar.D(w.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED);
        }
        return bool.booleanValue();
    }

    public abstract n<?> s(d dVar, Boolean bool);

    public abstract void t(T t, b.g.a.b.d dVar, x xVar) throws IOException;

    public a(a<?> aVar, d dVar, Boolean bool) {
        super(aVar._handledType, false);
        this._property = dVar;
        this._unwrapSingle = bool;
    }
}
