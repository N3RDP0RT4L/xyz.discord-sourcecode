package b.g.a.c.g0.u;

import b.g.a.a.i;
import b.g.a.a.i0;
import b.g.a.a.l0;
import b.g.a.c.b;
import b.g.a.c.c0.a0;
import b.g.a.c.e0.g;
import b.g.a.c.g0.a;
import b.g.a.c.g0.c;
import b.g.a.c.g0.e;
import b.g.a.c.g0.h;
import b.g.a.c.g0.i;
import b.g.a.c.g0.o;
import b.g.a.c.g0.t.k;
import b.g.a.c.g0.t.u;
import b.g.a.c.i0.f;
import b.g.a.c.i0.n;
import b.g.a.c.j;
import b.g.a.c.t;
import b.g.a.c.v;
import b.g.a.c.x;
import com.fasterxml.jackson.databind.JsonMappingException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
/* compiled from: BeanSerializerBase.java */
/* loaded from: classes2.dex */
public abstract class d extends q0<Object> implements i, o {
    public static final c[] k = new c[0];
    public final a _anyGetterWriter;
    public final j _beanType;
    public final c[] _filteredProps;
    public final b.g.a.c.g0.t.j _objectIdWriter;
    public final Object _propertyFilterId;
    public final c[] _props;
    public final i.c _serializationShape;
    public final b.g.a.c.c0.i _typeId;

    static {
        new t("#object-ref");
    }

    public d(j jVar, e eVar, c[] cVarArr, c[] cVarArr2) {
        super(jVar);
        this._beanType = jVar;
        this._props = cVarArr;
        this._filteredProps = cVarArr2;
        if (eVar == null) {
            this._typeId = null;
            this._anyGetterWriter = null;
            this._propertyFilterId = null;
            this._objectIdWriter = null;
            this._serializationShape = null;
            return;
        }
        this._typeId = eVar.h;
        this._anyGetterWriter = eVar.f;
        this._propertyFilterId = eVar.g;
        this._objectIdWriter = eVar.i;
        this._serializationShape = eVar.f697b.a(null).e();
    }

    public static final c[] t(c[] cVarArr, n nVar) {
        if (cVarArr == null || cVarArr.length == 0 || nVar == null || nVar == n.j) {
            return cVarArr;
        }
        int length = cVarArr.length;
        c[] cVarArr2 = new c[length];
        for (int i = 0; i < length; i++) {
            c cVar = cVarArr[i];
            if (cVar != null) {
                cVarArr2[i] = cVar.g(nVar);
            }
        }
        return cVarArr2;
    }

    @Override // b.g.a.c.g0.i
    public b.g.a.c.n<?> a(x xVar, b.g.a.c.d dVar) throws JsonMappingException {
        i.c cVar;
        int i;
        Set<String> set;
        Set<String> set2;
        Object obj;
        d dVar2;
        Object obj2;
        a0 s2;
        b v = xVar.v();
        c[] cVarArr = null;
        b.g.a.c.c0.i member = (dVar == null || v == null) ? null : dVar.getMember();
        v vVar = xVar._config;
        i.d l = l(xVar, dVar, this._handledType);
        char c = 0;
        if (l == null || !l.i()) {
            cVar = null;
        } else {
            cVar = l.e();
            if (!(cVar == i.c.ANY || cVar == this._serializationShape)) {
                if (this._beanType.w()) {
                    int ordinal = cVar.ordinal();
                    if (ordinal == 5 || ordinal == 7 || ordinal == 8) {
                        vVar.n(this._beanType);
                        Class<?> cls = this._beanType._class;
                        return xVar.y(new m(f.a(xVar._config, cls), m.p(cls, l, true, null)), dVar);
                    }
                } else if (cVar == i.c.NATURAL && ((!this._beanType.z() || !Map.class.isAssignableFrom(this._handledType)) && Map.Entry.class.isAssignableFrom(this._handledType))) {
                    j i2 = this._beanType.i(Map.Entry.class);
                    return xVar.y(new b.g.a.c.g0.t.i(this._beanType, i2.h(0), i2.h(1), false, null, dVar), dVar);
                }
            }
        }
        b.g.a.c.g0.t.j jVar = this._objectIdWriter;
        if (member != null) {
            set = v.x(vVar, member).d();
            set2 = v.A(vVar, member)._included;
            a0 r = v.r(member);
            if (r != null) {
                a0 s3 = v.s(member, r);
                Class<? extends i0<?>> cls2 = s3.c;
                j jVar2 = xVar.d().i(xVar.b(cls2), i0.class)[0];
                if (cls2 == l0.class) {
                    String str = s3.f677b._simpleName;
                    int length = this._props.length;
                    i = 0;
                    while (i != length) {
                        c cVar2 = this._props[i];
                        if (str.equals(cVar2._name._value)) {
                            jVar = b.g.a.c.g0.t.j.a(cVar2._declaredType, null, new k(s3, cVar2), s3.f);
                            obj = v.g(member);
                            if (obj != null || ((obj2 = this._propertyFilterId) != null && obj.equals(obj2))) {
                                obj = null;
                            }
                        } else {
                            i++;
                            c = 0;
                        }
                    }
                    j jVar3 = this._beanType;
                    Object[] objArr = new Object[2];
                    objArr[c] = b.g.a.c.i0.d.u(this._handledType);
                    objArr[1] = str == null ? "[null]" : b.g.a.c.i0.d.c(str);
                    xVar.f(jVar3, String.format("Invalid Object Id definition for %s: cannot find property with name %s", objArr));
                    throw null;
                }
                jVar = b.g.a.c.g0.t.j.a(jVar2, s3.f677b, xVar.e(member, s3), s3.f);
            } else if (!(jVar == null || (s2 = v.s(member, null)) == null)) {
                jVar = this._objectIdWriter;
                boolean z2 = s2.f;
                if (z2 != jVar.e) {
                    jVar = new b.g.a.c.g0.t.j(jVar.a, jVar.f700b, jVar.c, jVar.d, z2);
                }
            }
            i = 0;
            obj = v.g(member);
            if (obj != null) {
            }
            obj = null;
        } else {
            obj = null;
            set2 = null;
            set = null;
            i = 0;
        }
        if (i > 0) {
            c[] cVarArr2 = this._props;
            c[] cVarArr3 = (c[]) Arrays.copyOf(cVarArr2, cVarArr2.length);
            c cVar3 = cVarArr3[i];
            System.arraycopy(cVarArr3, 0, cVarArr3, 1, i);
            cVarArr3[0] = cVar3;
            c[] cVarArr4 = this._filteredProps;
            if (cVarArr4 != null) {
                cVarArr = (c[]) Arrays.copyOf(cVarArr4, cVarArr4.length);
                c cVar4 = cVarArr[i];
                System.arraycopy(cVarArr, 0, cVarArr, 1, i);
                cVarArr[0] = cVar4;
            }
            dVar2 = y(cVarArr3, cVarArr);
        } else {
            dVar2 = this;
        }
        if (jVar != null) {
            b.g.a.c.g0.t.j jVar4 = new b.g.a.c.g0.t.j(jVar.a, jVar.f700b, jVar.c, xVar.t(jVar.a, dVar), jVar.e);
            if (jVar4 != this._objectIdWriter) {
                dVar2 = dVar2.x(jVar4);
            }
        }
        if ((set != null && !set.isEmpty()) || set2 != null) {
            dVar2 = dVar2.v(set, set2);
        }
        if (obj != null) {
            dVar2 = dVar2.w(obj);
        }
        if (cVar == null) {
            cVar = this._serializationShape;
        }
        return cVar == i.c.ARRAY ? dVar2.s() : dVar2;
    }

    @Override // b.g.a.c.g0.o
    public void b(x xVar) throws JsonMappingException {
        c cVar;
        g gVar;
        b.g.a.c.c0.b bVar;
        Object H;
        b.g.a.c.n<Object> nVar;
        c cVar2;
        c[] cVarArr = this._filteredProps;
        int length = cVarArr == null ? 0 : cVarArr.length;
        int length2 = this._props.length;
        for (int i = 0; i < length2; i++) {
            c cVar3 = this._props[i];
            boolean z2 = true;
            if (!cVar3._suppressNulls) {
                if (!(cVar3._nullSerializer != null) && (nVar = xVar._nullValueSerializer) != null) {
                    cVar3.e(nVar);
                    if (i < length && (cVar2 = this._filteredProps[i]) != null) {
                        cVar2.e(nVar);
                    }
                }
            }
            if (cVar3._serializer == null) {
                z2 = false;
            }
            if (!z2) {
                b v = xVar.v();
                b.g.a.c.n<Object> nVar2 = null;
                if (!(v == null || (bVar = cVar3._member) == null || (H = v.H(bVar)) == null)) {
                    b.g.a.c.i0.e<Object, Object> c = xVar.c(cVar3._member, H);
                    j a = c.a(xVar.d());
                    if (!a.y()) {
                        nVar2 = xVar.t(a, cVar3);
                    }
                    nVar2 = new j0(c, a, nVar2);
                }
                if (nVar2 == null) {
                    j jVar = cVar3._cfgSerializationType;
                    if (jVar == null) {
                        jVar = cVar3._declaredType;
                        if (!jVar.x()) {
                            if (jVar.v() || jVar.g() > 0) {
                                cVar3._nonTrivialBaseType = jVar;
                            }
                        }
                    }
                    nVar2 = xVar.t(jVar, cVar3);
                    if (jVar.v() && (gVar = (g) jVar.k()._typeHandler) != null && (nVar2 instanceof h)) {
                        h hVar = (h) nVar2;
                        Objects.requireNonNull(hVar);
                        nVar2 = hVar.p(gVar);
                    }
                }
                if (i >= length || (cVar = this._filteredProps[i]) == null) {
                    cVar3.f(nVar2);
                } else {
                    cVar.f(nVar2);
                }
            }
        }
        a aVar = this._anyGetterWriter;
        if (aVar != null) {
            b.g.a.c.n<?> nVar3 = aVar.c;
            if (nVar3 instanceof b.g.a.c.g0.i) {
                b.g.a.c.n<?> y2 = xVar.y(nVar3, aVar.a);
                aVar.c = y2;
                if (y2 instanceof t) {
                    aVar.d = (t) y2;
                }
            }
        }
    }

    @Override // b.g.a.c.n
    public void g(Object obj, b.g.a.b.d dVar, x xVar, g gVar) throws IOException {
        if (this._objectIdWriter != null) {
            dVar.e(obj);
            p(obj, dVar, xVar, gVar);
            return;
        }
        dVar.e(obj);
        b.g.a.b.s.b r = r(gVar, obj, b.g.a.b.h.START_OBJECT);
        gVar.e(dVar, r);
        Object obj2 = this._propertyFilterId;
        if (obj2 != null) {
            if (this._filteredProps != null) {
                Class<?> cls = xVar._serializationView;
            }
            m(xVar, obj2, obj);
            throw null;
        }
        u(obj, dVar, xVar);
        gVar.f(dVar, r);
    }

    @Override // b.g.a.c.n
    public boolean i() {
        return this._objectIdWriter != null;
    }

    public final void p(Object obj, b.g.a.b.d dVar, x xVar, g gVar) throws IOException {
        b.g.a.c.g0.t.j jVar = this._objectIdWriter;
        u p = xVar.p(obj, jVar.c);
        if (!p.a(dVar, xVar, jVar)) {
            if (p.f709b == null) {
                p.f709b = p.a.c(obj);
            }
            Object obj2 = p.f709b;
            if (jVar.e) {
                jVar.d.f(obj2, dVar, xVar);
                return;
            }
            b.g.a.c.g0.t.j jVar2 = this._objectIdWriter;
            b.g.a.b.s.b r = r(gVar, obj, b.g.a.b.h.START_OBJECT);
            gVar.e(dVar, r);
            p.c = true;
            Objects.requireNonNull(dVar);
            b.g.a.b.k kVar = jVar2.f700b;
            if (kVar != null) {
                dVar.x(kVar);
                jVar2.d.f(p.f709b, dVar, xVar);
            }
            Object obj3 = this._propertyFilterId;
            if (obj3 == null) {
                u(obj, dVar, xVar);
                gVar.f(dVar, r);
                return;
            }
            m(xVar, obj3, obj);
            throw null;
        }
    }

    public final void q(Object obj, b.g.a.b.d dVar, x xVar, boolean z2) throws IOException {
        b.g.a.c.g0.t.j jVar = this._objectIdWriter;
        u p = xVar.p(obj, jVar.c);
        if (!p.a(dVar, xVar, jVar)) {
            if (p.f709b == null) {
                p.f709b = p.a.c(obj);
            }
            Object obj2 = p.f709b;
            if (jVar.e) {
                jVar.d.f(obj2, dVar, xVar);
                return;
            }
            if (z2) {
                dVar.d0(obj);
            }
            p.c = true;
            b.g.a.b.k kVar = jVar.f700b;
            if (kVar != null) {
                dVar.x(kVar);
                jVar.d.f(p.f709b, dVar, xVar);
            }
            Object obj3 = this._propertyFilterId;
            if (obj3 == null) {
                u(obj, dVar, xVar);
                if (z2) {
                    dVar.u();
                    return;
                }
                return;
            }
            m(xVar, obj3, obj);
            throw null;
        }
    }

    public final b.g.a.b.s.b r(g gVar, Object obj, b.g.a.b.h hVar) {
        b.g.a.c.c0.i iVar = this._typeId;
        if (iVar == null) {
            return gVar.d(obj, hVar);
        }
        Object j = iVar.j(obj);
        if (j == null) {
            j = "";
        }
        b.g.a.b.s.b d = gVar.d(obj, hVar);
        d.c = j;
        return d;
    }

    public abstract d s();

    public void u(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
        String str = "[anySetter]";
        c[] cVarArr = this._filteredProps;
        if (cVarArr == null || xVar._serializationView == null) {
            cVarArr = this._props;
        }
        int i = 0;
        try {
            int length = cVarArr.length;
            while (i < length) {
                c cVar = cVarArr[i];
                if (cVar != null) {
                    cVar.i(obj, dVar, xVar);
                }
                i++;
            }
            a aVar = this._anyGetterWriter;
            if (aVar != null) {
                aVar.a(obj, dVar, xVar);
            }
        } catch (Exception e) {
            if (i != cVarArr.length) {
                str = cVarArr[i]._name._value;
            }
            o(xVar, e, obj, str);
            throw null;
        } catch (StackOverflowError e2) {
            JsonMappingException jsonMappingException = new JsonMappingException(dVar, "Infinite recursion (StackOverflowError)", e2);
            if (i != cVarArr.length) {
                str = cVarArr[i]._name._value;
            }
            jsonMappingException.e(new JsonMappingException.a(obj, str));
            throw jsonMappingException;
        }
    }

    public abstract d v(Set<String> set, Set<String> set2);

    public abstract d w(Object obj);

    public abstract d x(b.g.a.c.g0.t.j jVar);

    public abstract d y(c[] cVarArr, c[] cVarArr2);

    public d(d dVar, c[] cVarArr, c[] cVarArr2) {
        super(dVar._handledType);
        this._beanType = dVar._beanType;
        this._props = cVarArr;
        this._filteredProps = cVarArr2;
        this._typeId = dVar._typeId;
        this._anyGetterWriter = dVar._anyGetterWriter;
        this._objectIdWriter = dVar._objectIdWriter;
        this._propertyFilterId = dVar._propertyFilterId;
        this._serializationShape = dVar._serializationShape;
    }

    public d(d dVar, b.g.a.c.g0.t.j jVar, Object obj) {
        super(dVar._handledType);
        this._beanType = dVar._beanType;
        this._props = dVar._props;
        this._filteredProps = dVar._filteredProps;
        this._typeId = dVar._typeId;
        this._anyGetterWriter = dVar._anyGetterWriter;
        this._objectIdWriter = jVar;
        this._propertyFilterId = obj;
        this._serializationShape = dVar._serializationShape;
    }

    public d(d dVar, Set<String> set, Set<String> set2) {
        super(dVar._handledType);
        this._beanType = dVar._beanType;
        c[] cVarArr = dVar._props;
        c[] cVarArr2 = dVar._filteredProps;
        int length = cVarArr.length;
        ArrayList arrayList = new ArrayList(length);
        c[] cVarArr3 = null;
        ArrayList arrayList2 = cVarArr2 == null ? null : new ArrayList(length);
        for (int i = 0; i < length; i++) {
            c cVar = cVarArr[i];
            if (!b.c.a.a0.d.b2(cVar._name._value, set, set2)) {
                arrayList.add(cVar);
                if (cVarArr2 != null) {
                    arrayList2.add(cVarArr2[i]);
                }
            }
        }
        this._props = (c[]) arrayList.toArray(new c[arrayList.size()]);
        this._filteredProps = arrayList2 != null ? (c[]) arrayList2.toArray(new c[arrayList2.size()]) : cVarArr3;
        this._typeId = dVar._typeId;
        this._anyGetterWriter = dVar._anyGetterWriter;
        this._objectIdWriter = dVar._objectIdWriter;
        this._propertyFilterId = dVar._propertyFilterId;
        this._serializationShape = dVar._serializationShape;
    }
}
