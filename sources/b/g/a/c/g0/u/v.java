package b.g.a.c.g0.u;

import b.g.a.a.i;
import b.g.a.b.d;
import b.g.a.c.g0.i;
import b.g.a.c.n;
import b.g.a.c.x;
import com.fasterxml.jackson.databind.JsonMappingException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
/* compiled from: NumberSerializer.java */
@b.g.a.c.y.a
/* loaded from: classes2.dex */
public class v extends p0<Number> implements i {
    public static final v k = new v(Number.class);
    public final boolean _isInt;

    /* compiled from: NumberSerializer.java */
    /* loaded from: classes2.dex */
    public static final class a extends u0 {
        public static final a k = new a();

        public a() {
            super(BigDecimal.class);
        }

        @Override // b.g.a.c.g0.u.u0, b.g.a.c.n
        public boolean d(x xVar, Object obj) {
            return false;
        }

        @Override // b.g.a.c.g0.u.u0, b.g.a.c.n
        public void f(Object obj, d dVar, x xVar) throws IOException {
            String str;
            if (dVar.d(d.a.WRITE_BIGDECIMAL_AS_PLAIN)) {
                BigDecimal bigDecimal = (BigDecimal) obj;
                int scale = bigDecimal.scale();
                if (scale >= -9999 && scale <= 9999) {
                    str = bigDecimal.toPlainString();
                } else {
                    xVar.G(String.format("Attempt to write plain `java.math.BigDecimal` (see JsonGenerator.Feature.WRITE_BIGDECIMAL_AS_PLAIN) with illegal scale (%d): needs to be between [-%d, %d]", Integer.valueOf(bigDecimal.scale()), 9999, 9999), new Object[0]);
                    throw null;
                }
            } else {
                str = obj.toString();
            }
            dVar.j0(str);
        }

        @Override // b.g.a.c.g0.u.u0
        public String p(Object obj) {
            throw new IllegalStateException();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public v(Class<? extends Number> cls) {
        super(cls, false);
        boolean z2 = false;
        this._isInt = cls == BigInteger.class ? true : z2;
    }

    @Override // b.g.a.c.g0.i
    public n<?> a(x xVar, b.g.a.c.d dVar) throws JsonMappingException {
        i.d l = l(xVar, dVar, this._handledType);
        if (l == null || l.e().ordinal() != 8) {
            return this;
        }
        if (this._handledType == BigDecimal.class) {
            return a.k;
        }
        return t0.k;
    }

    @Override // b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        Number number = (Number) obj;
        if (number instanceof BigDecimal) {
            dVar.L((BigDecimal) number);
        } else if (number instanceof BigInteger) {
            dVar.N((BigInteger) number);
        } else if (number instanceof Long) {
            dVar.I(number.longValue());
        } else if (number instanceof Double) {
            dVar.C(number.doubleValue());
        } else if (number instanceof Float) {
            dVar.D(number.floatValue());
        } else if ((number instanceof Integer) || (number instanceof Byte) || (number instanceof Short)) {
            dVar.H(number.intValue());
        } else {
            dVar.J(number.toString());
        }
    }
}
