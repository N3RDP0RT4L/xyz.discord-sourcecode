package b.g.a.c.g0.u;

import b.g.a.b.d;
import b.g.a.c.x;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
/* compiled from: StdJdkSerializers.java */
/* loaded from: classes2.dex */
public class k0 extends p0<AtomicBoolean> {
    public k0() {
        super(AtomicBoolean.class, false);
    }

    @Override // b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        dVar.s(((AtomicBoolean) obj).get());
    }
}
