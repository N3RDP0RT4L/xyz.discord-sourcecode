package b.g.a.c.g0.u;

import b.g.a.b.d;
import b.g.a.b.f;
import b.g.a.c.e0.g;
import b.g.a.c.x;
import b.g.a.c.y.a;
import java.io.IOException;
/* compiled from: NumberSerializers.java */
@a
/* loaded from: classes2.dex */
public class a0 extends w<Object> {
    public a0(Class<?> cls) {
        super(cls, f.b.INT, "integer");
    }

    @Override // b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        dVar.H(((Integer) obj).intValue());
    }

    @Override // b.g.a.c.g0.u.p0, b.g.a.c.n
    public void g(Object obj, d dVar, x xVar, g gVar) throws IOException {
        f(obj, dVar, xVar);
    }
}
