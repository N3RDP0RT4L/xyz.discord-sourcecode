package b.g.a.c.g0.u;

import b.g.a.b.d;
import b.g.a.b.h;
import b.g.a.b.s.b;
import b.g.a.c.e0.g;
import b.g.a.c.x;
import java.io.IOException;
/* compiled from: StdScalarSerializer.java */
/* loaded from: classes2.dex */
public abstract class p0<T> extends q0<T> {
    public p0(Class<T> cls) {
        super(cls);
    }

    @Override // b.g.a.c.n
    public void g(T t, d dVar, x xVar, g gVar) throws IOException {
        b e = gVar.e(dVar, gVar.d(t, h.VALUE_STRING));
        f(t, dVar, xVar);
        gVar.f(dVar, e);
    }

    public p0(Class<?> cls, boolean z2) {
        super(cls);
    }
}
