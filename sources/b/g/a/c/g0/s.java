package b.g.a.c.g0;

import b.g.a.a.p;
import b.g.a.b.d;
import b.g.a.c.c0.c;
import b.g.a.c.e0.g;
import b.g.a.c.g0.t.l;
import b.g.a.c.j;
import b.g.a.c.n;
import b.g.a.c.x;
import java.io.Serializable;
/* compiled from: VirtualBeanPropertyWriter.java */
/* loaded from: classes2.dex */
public abstract class s extends c implements Serializable {
    private static final long serialVersionUID = 1;

    public s() {
    }

    @Override // b.g.a.c.g0.c
    public void h(Object obj, d dVar, x xVar) throws Exception {
        Object j = j(obj, dVar, xVar);
        if (j == null) {
            n<Object> nVar = this._nullSerializer;
            if (nVar != null) {
                nVar.f(null, dVar, xVar);
            } else {
                dVar.A();
            }
        } else {
            n<Object> nVar2 = this._serializer;
            if (nVar2 == null) {
                Class<?> cls = j.getClass();
                l lVar = this.n;
                n<Object> c = lVar.c(cls);
                nVar2 = c == null ? c(lVar, cls, xVar) : c;
            }
            Object obj2 = this._suppressableValue;
            if (obj2 != null) {
                if (p.a.NON_EMPTY == obj2) {
                    if (nVar2.d(xVar, j)) {
                        n<Object> nVar3 = this._nullSerializer;
                        if (nVar3 != null) {
                            nVar3.f(null, dVar, xVar);
                            return;
                        } else {
                            dVar.A();
                            return;
                        }
                    }
                } else if (obj2.equals(j)) {
                    n<Object> nVar4 = this._nullSerializer;
                    if (nVar4 != null) {
                        nVar4.f(null, dVar, xVar);
                        return;
                    } else {
                        dVar.A();
                        return;
                    }
                }
            }
            if (j != obj || !d(dVar, xVar, nVar2)) {
                g gVar = this._typeSerializer;
                if (gVar == null) {
                    nVar2.f(j, dVar, xVar);
                } else {
                    nVar2.g(j, dVar, xVar, gVar);
                }
            }
        }
    }

    @Override // b.g.a.c.g0.c
    public void i(Object obj, d dVar, x xVar) throws Exception {
        Object j = j(obj, dVar, xVar);
        if (j != null) {
            n<Object> nVar = this._serializer;
            if (nVar == null) {
                Class<?> cls = j.getClass();
                l lVar = this.n;
                n<Object> c = lVar.c(cls);
                nVar = c == null ? c(lVar, cls, xVar) : c;
            }
            Object obj2 = this._suppressableValue;
            if (obj2 != null) {
                if (p.a.NON_EMPTY == obj2) {
                    if (nVar.d(xVar, j)) {
                        return;
                    }
                } else if (obj2.equals(j)) {
                    return;
                }
            }
            if (j != obj || !d(dVar, xVar, nVar)) {
                dVar.x(this._name);
                g gVar = this._typeSerializer;
                if (gVar == null) {
                    nVar.f(j, dVar, xVar);
                } else {
                    nVar.g(j, dVar, xVar, gVar);
                }
            }
        } else if (this._nullSerializer != null) {
            dVar.x(this._name);
            this._nullSerializer.f(null, dVar, xVar);
        }
    }

    public abstract Object j(Object obj, d dVar, x xVar) throws Exception;

    public abstract s k(b.g.a.c.z.l<?> lVar, c cVar, b.g.a.c.c0.s sVar, j jVar);

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public s(b.g.a.c.c0.s r15, b.g.a.c.i0.a r16, b.g.a.c.j r17, b.g.a.c.n<?> r18, b.g.a.c.e0.g r19, b.g.a.c.j r20, b.g.a.a.p.b r21, java.lang.Class<?>[] r22) {
        /*
            r14 = this;
            r0 = r21
            b.g.a.a.p$a r1 = b.g.a.a.p.a.USE_DEFAULTS
            b.g.a.a.p$a r2 = b.g.a.a.p.a.ALWAYS
            b.g.a.c.c0.i r5 = r15.q()
            r3 = 0
            if (r0 != 0) goto Lf
            r11 = 0
            goto L17
        Lf:
            b.g.a.a.p$a r4 = r0._valueInclusion
            if (r4 == r2) goto L16
            if (r4 == r1) goto L16
            r3 = 1
        L16:
            r11 = r3
        L17:
            if (r0 != 0) goto L1d
            java.lang.Boolean r0 = java.lang.Boolean.FALSE
        L1b:
            r12 = r0
            goto L2d
        L1d:
            b.g.a.a.p$a r0 = r0._valueInclusion
            if (r0 == r2) goto L2b
            b.g.a.a.p$a r2 = b.g.a.a.p.a.NON_NULL
            if (r0 == r2) goto L2b
            if (r0 != r1) goto L28
            goto L2b
        L28:
            b.g.a.a.p$a r0 = b.g.a.a.p.a.NON_EMPTY
            goto L1b
        L2b:
            r0 = 0
            goto L1b
        L2d:
            r8 = 0
            r9 = 0
            r10 = 0
            r13 = 0
            r3 = r14
            r4 = r15
            r6 = r16
            r7 = r17
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.c.g0.s.<init>(b.g.a.c.c0.s, b.g.a.c.i0.a, b.g.a.c.j, b.g.a.c.n, b.g.a.c.e0.g, b.g.a.c.j, b.g.a.a.p$b, java.lang.Class[]):void");
    }
}
