package b.g.a.c.g0;

import b.d.b.a.a;
import b.g.a.a.p;
import b.g.a.c.b;
import b.g.a.c.c;
import b.g.a.c.j;
import b.g.a.c.v;
import b.g.a.c.y.e;
import com.fasterxml.jackson.databind.JsonMappingException;
/* compiled from: PropertyBuilder.java */
/* loaded from: classes2.dex */
public class l {
    public final v a;

    /* renamed from: b  reason: collision with root package name */
    public final c f698b;
    public final b c;
    public Object d;
    public final p.b e;
    public final boolean f;

    public l(v vVar, c cVar) {
        this.a = vVar;
        this.f698b = cVar;
        p.b bVar = p.b.j;
        p.b bVar2 = p.b.j;
        p.b c = cVar.c(bVar2);
        vVar.k(cVar.a._class, bVar2);
        bVar2 = c != null ? c.a(bVar2) : bVar2;
        p.b bVar3 = vVar._configOverrides._defaultInclusion;
        this.e = bVar3 == null ? bVar2 : bVar3.a(bVar2);
        this.f = bVar2._valueInclusion == p.a.NON_DEFAULT;
        this.c = vVar.e();
    }

    public j a(b.g.a.c.c0.b bVar, boolean z2, j jVar) throws JsonMappingException {
        j e02 = this.c.e0(this.a, bVar, jVar);
        boolean z3 = true;
        if (e02 != jVar) {
            Class<?> cls = e02._class;
            Class<?> cls2 = jVar._class;
            if (!cls.isAssignableFrom(cls2) && !cls2.isAssignableFrom(cls)) {
                StringBuilder R = a.R("Illegal concrete-type annotation for method '");
                R.append(bVar.c());
                R.append("': class ");
                R.append(cls.getName());
                R.append(" not a super-type of (declared) class ");
                R.append(cls2.getName());
                throw new IllegalArgumentException(R.toString());
            }
            z2 = true;
            jVar = e02;
        }
        e.b K = this.c.K(bVar);
        if (!(K == null || K == e.b.DEFAULT_TYPING)) {
            if (K != e.b.STATIC) {
                z3 = false;
            }
            z2 = z3;
        }
        if (z2) {
            return jVar.G();
        }
        return null;
    }
}
