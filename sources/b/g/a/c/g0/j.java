package b.g.a.c.g0;

import b.g.a.a.i0;
import b.g.a.b.d;
import b.g.a.b.k;
import b.g.a.c.c0.b;
import b.g.a.c.c0.s;
import b.g.a.c.g0.t.u;
import b.g.a.c.n;
import b.g.a.c.t;
import b.g.a.c.v;
import b.g.a.c.w;
import b.g.a.c.x;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidDefinitionException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Objects;
/* compiled from: DefaultSerializerProvider.java */
/* loaded from: classes2.dex */
public abstract class j extends x implements Serializable {
    private static final long serialVersionUID = 1;
    public transient Map<Object, u> m;
    public transient ArrayList<i0<?>> n;
    public transient d o;

    /* compiled from: DefaultSerializerProvider.java */
    /* loaded from: classes2.dex */
    public static final class a extends j {
        private static final long serialVersionUID = 1;

        public a() {
        }

        @Override // b.g.a.c.g0.j
        public j K(v vVar, q qVar) {
            return new a(this, vVar, qVar);
        }

        public a(x xVar, v vVar, q qVar) {
            super(xVar, vVar, qVar);
        }
    }

    public j() {
    }

    @Override // b.g.a.c.x
    public Object A(s sVar, Class<?> cls) {
        if (cls == null) {
            return null;
        }
        Objects.requireNonNull(this._config._base);
        return b.g.a.c.i0.d.g(cls, this._config.b());
    }

    @Override // b.g.a.c.x
    public boolean B(Object obj) throws JsonMappingException {
        try {
            return obj.equals(null);
        } catch (Throwable th) {
            InvalidDefinitionException invalidDefinitionException = new InvalidDefinitionException(this.o, String.format("Problem determining whether filter of type '%s' should filter out `null` values: (%s) %s", obj.getClass().getName(), th.getClass().getName(), b.g.a.c.i0.d.h(th)), b(obj.getClass()));
            invalidDefinitionException.initCause(th);
            throw invalidDefinitionException;
        }
    }

    @Override // b.g.a.c.x
    public n<Object> H(b bVar, Object obj) throws JsonMappingException {
        n<Object> nVar;
        if (obj instanceof n) {
            nVar = (n) obj;
        } else if (obj instanceof Class) {
            Class cls = (Class) obj;
            if (cls == n.a.class || b.g.a.c.i0.d.p(cls)) {
                return null;
            }
            if (n.class.isAssignableFrom(cls)) {
                Objects.requireNonNull(this._config._base);
                nVar = (n) b.g.a.c.i0.d.g(cls, this._config.b());
            } else {
                b.g.a.c.j e = bVar.e();
                StringBuilder R = b.d.b.a.a.R("AnnotationIntrospector returned Class ");
                R.append(cls.getName());
                R.append("; expected Class<JsonSerializer>");
                f(e, R.toString());
                throw null;
            }
        } else {
            b.g.a.c.j e2 = bVar.e();
            StringBuilder R2 = b.d.b.a.a.R("AnnotationIntrospector returned serializer definition of type ");
            R2.append(obj.getClass().getName());
            R2.append("; expected type JsonSerializer or Class<JsonSerializer> instead");
            f(e2, R2.toString());
            throw null;
        }
        if (nVar instanceof o) {
            ((o) nVar).b(this);
        }
        return nVar;
    }

    public final void I(d dVar, Object obj, n<Object> nVar, t tVar) throws IOException {
        try {
            dVar.c0();
            v vVar = this._config;
            k kVar = tVar._encodedSimple;
            if (kVar == null) {
                if (vVar == null) {
                    kVar = new b.g.a.b.p.j(tVar._simpleName);
                } else {
                    kVar = new b.g.a.b.p.j(tVar._simpleName);
                }
                tVar._encodedSimple = kVar;
            }
            dVar.x(kVar);
            nVar.f(obj, dVar, this);
            dVar.u();
        } catch (Exception e) {
            throw J(dVar, e);
        }
    }

    public final IOException J(d dVar, Exception exc) {
        if (exc instanceof IOException) {
            return (IOException) exc;
        }
        String h = b.g.a.c.i0.d.h(exc);
        if (h == null) {
            StringBuilder R = b.d.b.a.a.R("[no message for ");
            R.append(exc.getClass().getName());
            R.append("]");
            h = R.toString();
        }
        return new JsonMappingException(dVar, h, exc);
    }

    public abstract j K(v vVar, q qVar);

    /* JADX WARN: Removed duplicated region for block: B:32:0x0054  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x00a7  */
    /* JADX WARN: Removed duplicated region for block: B:75:0x0119  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void L(b.g.a.b.d r9, java.lang.Object r10) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 307
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.c.g0.j.L(b.g.a.b.d, java.lang.Object):void");
    }

    @Override // b.g.a.c.x
    public u p(Object obj, i0<?> i0Var) {
        Map<Object, u> map;
        Map<Object, u> map2 = this.m;
        if (map2 == null) {
            if (D(w.USE_EQUALITY_FOR_OBJECT_ID)) {
                map = new HashMap<>();
            } else {
                map = new IdentityHashMap<>();
            }
            this.m = map;
        } else {
            u uVar = map2.get(obj);
            if (uVar != null) {
                return uVar;
            }
        }
        i0<?> i0Var2 = null;
        ArrayList<i0<?>> arrayList = this.n;
        if (arrayList != null) {
            int i = 0;
            int size = arrayList.size();
            while (true) {
                if (i >= size) {
                    break;
                }
                i0<?> i0Var3 = this.n.get(i);
                if (i0Var3.a(i0Var)) {
                    i0Var2 = i0Var3;
                    break;
                }
                i++;
            }
        } else {
            this.n = new ArrayList<>(8);
        }
        if (i0Var2 == null) {
            i0Var2 = i0Var.e(this);
            this.n.add(i0Var2);
        }
        u uVar2 = new u(i0Var2);
        this.m.put(obj, uVar2);
        return uVar2;
    }

    public j(x xVar, v vVar, q qVar) {
        super(xVar, vVar, qVar);
    }
}
