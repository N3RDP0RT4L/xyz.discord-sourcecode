package b.g.a.c.g0;

import b.g.a.c.g0.t.m;
import b.g.a.c.i0.u;
import b.g.a.c.j;
import b.g.a.c.n;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicReference;
/* compiled from: SerializerCache.java */
/* loaded from: classes2.dex */
public final class p {
    public final HashMap<u, n<Object>> a = new HashMap<>(64);

    /* renamed from: b  reason: collision with root package name */
    public final AtomicReference<m> f699b = new AtomicReference<>();

    public n<Object> a(j jVar) {
        n<Object> nVar;
        synchronized (this) {
            nVar = this.a.get(new u(jVar, false));
        }
        return nVar;
    }

    public n<Object> b(Class<?> cls) {
        n<Object> nVar;
        synchronized (this) {
            nVar = this.a.get(new u(cls, false));
        }
        return nVar;
    }
}
