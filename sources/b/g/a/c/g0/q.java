package b.g.a.c.g0;

import b.g.a.c.e0.g;
import b.g.a.c.j;
import b.g.a.c.n;
import b.g.a.c.v;
import b.g.a.c.x;
import com.fasterxml.jackson.databind.JsonMappingException;
/* compiled from: SerializerFactory.java */
/* loaded from: classes2.dex */
public abstract class q {
    public abstract n<Object> a(x xVar, j jVar, n<Object> nVar) throws JsonMappingException;

    public abstract n<Object> b(x xVar, j jVar) throws JsonMappingException;

    public abstract g c(v vVar, j jVar) throws JsonMappingException;
}
