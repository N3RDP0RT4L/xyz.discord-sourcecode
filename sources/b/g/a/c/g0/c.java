package b.g.a.c.g0;

import b.g.a.a.p;
import b.g.a.b.d;
import b.g.a.c.c0.i;
import b.g.a.c.c0.s;
import b.g.a.c.e0.g;
import b.g.a.c.g0.t.l;
import b.g.a.c.j;
import b.g.a.c.n;
import b.g.a.c.t;
import b.g.a.c.w;
import b.g.a.c.x;
import b.g.a.c.y.a;
import com.fasterxml.jackson.databind.JsonMappingException;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
/* compiled from: BeanPropertyWriter.java */
@a
/* loaded from: classes2.dex */
public class c extends n implements Serializable {
    public static final /* synthetic */ int j = 0;
    private static final long serialVersionUID = 1;
    public final j _cfgSerializationType;
    public final j _declaredType;
    public final Class<?>[] _includeInViews;
    public final i _member;
    public final b.g.a.b.p.j _name;
    public j _nonTrivialBaseType;
    public n<Object> _nullSerializer;
    public n<Object> _serializer;
    public final boolean _suppressNulls;
    public final Object _suppressableValue;
    public g _typeSerializer;
    public final t _wrapperName;
    public final transient b.g.a.c.i0.a k;
    public transient Method l;
    public transient Field m;
    public transient l n;
    public transient HashMap<Object, Object> o;

    public c(s sVar, i iVar, b.g.a.c.i0.a aVar, j jVar, n<?> nVar, g gVar, j jVar2, boolean z2, Object obj, Class<?>[] clsArr) {
        super(sVar);
        this._member = iVar;
        this.k = aVar;
        this._name = new b.g.a.b.p.j(sVar.p());
        this._wrapperName = sVar.t();
        this._declaredType = jVar;
        this._serializer = nVar;
        this.n = nVar == null ? l.b.f702b : null;
        this._typeSerializer = gVar;
        this._cfgSerializationType = jVar2;
        if (iVar instanceof b.g.a.c.c0.g) {
            this.l = null;
            this.m = (Field) iVar.i();
        } else if (iVar instanceof b.g.a.c.c0.j) {
            this.l = (Method) iVar.i();
            this.m = null;
        } else {
            this.l = null;
            this.m = null;
        }
        this._suppressNulls = z2;
        this._suppressableValue = obj;
        this._nullSerializer = null;
        this._includeInViews = clsArr;
    }

    public n<Object> c(l lVar, Class<?> cls, x xVar) throws JsonMappingException {
        l.d dVar;
        j jVar = this._nonTrivialBaseType;
        if (jVar != null) {
            j k = xVar.k(jVar, cls);
            n<Object> q = xVar.q(k, this);
            dVar = new l.d(q, lVar.b(k._class, q));
        } else {
            n<Object> r = xVar.r(cls, this);
            dVar = new l.d(r, lVar.b(cls, r));
        }
        l lVar2 = dVar.f704b;
        if (lVar != lVar2) {
            this.n = lVar2;
        }
        return dVar.a;
    }

    public boolean d(d dVar, x xVar, n nVar) throws IOException {
        if (nVar.i()) {
            return false;
        }
        if (xVar.D(w.FAIL_ON_SELF_REFERENCES)) {
            if (!(nVar instanceof b.g.a.c.g0.u.d)) {
                return false;
            }
            xVar.f(this._declaredType, "Direct self-reference leading to cycle");
            throw null;
        } else if (!xVar.D(w.WRITE_SELF_REFERENCES_AS_NULL)) {
            return false;
        } else {
            if (this._nullSerializer == null) {
                return true;
            }
            if (!dVar.c().b()) {
                dVar.x(this._name);
            }
            this._nullSerializer.f(null, dVar, xVar);
            return true;
        }
    }

    public void e(n<Object> nVar) {
        n<Object> nVar2 = this._nullSerializer;
        if (nVar2 == null || nVar2 == nVar) {
            this._nullSerializer = nVar;
            return;
        }
        throw new IllegalStateException(String.format("Cannot override _nullSerializer: had a %s, trying to set to %s", b.g.a.c.i0.d.e(this._nullSerializer), b.g.a.c.i0.d.e(nVar)));
    }

    public void f(n<Object> nVar) {
        n<Object> nVar2 = this._serializer;
        if (nVar2 == null || nVar2 == nVar) {
            this._serializer = nVar;
            return;
        }
        throw new IllegalStateException(String.format("Cannot override _serializer: had a %s, trying to set to %s", b.g.a.c.i0.d.e(this._serializer), b.g.a.c.i0.d.e(nVar)));
    }

    public c g(b.g.a.c.i0.n nVar) {
        String a = nVar.a(this._name._value);
        return a.equals(this._name._value) ? this : new c(this, t.a(a));
    }

    @Override // b.g.a.c.d
    public i getMember() {
        return this._member;
    }

    @Override // b.g.a.c.d
    public j getType() {
        return this._declaredType;
    }

    public void h(Object obj, d dVar, x xVar) throws Exception {
        Method method = this.l;
        Object invoke = method == null ? this.m.get(obj) : method.invoke(obj, null);
        if (invoke == null) {
            n<Object> nVar = this._nullSerializer;
            if (nVar != null) {
                nVar.f(null, dVar, xVar);
            } else {
                dVar.A();
            }
        } else {
            n<Object> nVar2 = this._serializer;
            if (nVar2 == null) {
                Class<?> cls = invoke.getClass();
                l lVar = this.n;
                n<Object> c = lVar.c(cls);
                nVar2 = c == null ? c(lVar, cls, xVar) : c;
            }
            Object obj2 = this._suppressableValue;
            if (obj2 != null) {
                if (p.a.NON_EMPTY == obj2) {
                    if (nVar2.d(xVar, invoke)) {
                        n<Object> nVar3 = this._nullSerializer;
                        if (nVar3 != null) {
                            nVar3.f(null, dVar, xVar);
                            return;
                        } else {
                            dVar.A();
                            return;
                        }
                    }
                } else if (obj2.equals(invoke)) {
                    n<Object> nVar4 = this._nullSerializer;
                    if (nVar4 != null) {
                        nVar4.f(null, dVar, xVar);
                        return;
                    } else {
                        dVar.A();
                        return;
                    }
                }
            }
            if (invoke != obj || !d(dVar, xVar, nVar2)) {
                g gVar = this._typeSerializer;
                if (gVar == null) {
                    nVar2.f(invoke, dVar, xVar);
                } else {
                    nVar2.g(invoke, dVar, xVar, gVar);
                }
            }
        }
    }

    public void i(Object obj, d dVar, x xVar) throws Exception {
        Method method = this.l;
        Object invoke = method == null ? this.m.get(obj) : method.invoke(obj, null);
        if (invoke != null) {
            n<Object> nVar = this._serializer;
            if (nVar == null) {
                Class<?> cls = invoke.getClass();
                l lVar = this.n;
                n<Object> c = lVar.c(cls);
                nVar = c == null ? c(lVar, cls, xVar) : c;
            }
            Object obj2 = this._suppressableValue;
            if (obj2 != null) {
                if (p.a.NON_EMPTY == obj2) {
                    if (nVar.d(xVar, invoke)) {
                        return;
                    }
                } else if (obj2.equals(invoke)) {
                    return;
                }
            }
            if (invoke != obj || !d(dVar, xVar, nVar)) {
                dVar.x(this._name);
                g gVar = this._typeSerializer;
                if (gVar == null) {
                    nVar.f(invoke, dVar, xVar);
                } else {
                    nVar.g(invoke, dVar, xVar, gVar);
                }
            }
        } else if (this._nullSerializer != null) {
            dVar.x(this._name);
            this._nullSerializer.f(null, dVar, xVar);
        }
    }

    public Object readResolve() {
        i iVar = this._member;
        if (iVar instanceof b.g.a.c.c0.g) {
            this.l = null;
            this.m = (Field) iVar.i();
        } else if (iVar instanceof b.g.a.c.c0.j) {
            this.l = (Method) iVar.i();
            this.m = null;
        }
        if (this._serializer == null) {
            this.n = l.b.f702b;
        }
        return this;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(40);
        sb.append("property '");
        sb.append(this._name._value);
        sb.append("' (");
        if (this.l != null) {
            sb.append("via method ");
            sb.append(this.l.getDeclaringClass().getName());
            sb.append("#");
            sb.append(this.l.getName());
        } else if (this.m != null) {
            sb.append("field \"");
            sb.append(this.m.getDeclaringClass().getName());
            sb.append("#");
            sb.append(this.m.getName());
        } else {
            sb.append("virtual");
        }
        if (this._serializer == null) {
            sb.append(", no static serializer");
        } else {
            StringBuilder R = b.d.b.a.a.R(", static serializer of type ");
            R.append(this._serializer.getClass().getName());
            sb.append(R.toString());
        }
        sb.append(')');
        return sb.toString();
    }

    public c() {
        super(b.g.a.c.s.l);
        this._member = null;
        this.k = null;
        this._name = null;
        this._wrapperName = null;
        this._includeInViews = null;
        this._declaredType = null;
        this._serializer = null;
        this.n = null;
        this._typeSerializer = null;
        this._cfgSerializationType = null;
        this.l = null;
        this.m = null;
        this._suppressNulls = false;
        this._suppressableValue = null;
        this._nullSerializer = null;
    }

    public c(c cVar, t tVar) {
        super(cVar);
        this._name = new b.g.a.b.p.j(tVar._simpleName);
        this._wrapperName = cVar._wrapperName;
        this.k = cVar.k;
        this._declaredType = cVar._declaredType;
        this._member = cVar._member;
        this.l = cVar.l;
        this.m = cVar.m;
        this._serializer = cVar._serializer;
        this._nullSerializer = cVar._nullSerializer;
        if (cVar.o != null) {
            this.o = new HashMap<>(cVar.o);
        }
        this._cfgSerializationType = cVar._cfgSerializationType;
        this.n = cVar.n;
        this._suppressNulls = cVar._suppressNulls;
        this._suppressableValue = cVar._suppressableValue;
        this._includeInViews = cVar._includeInViews;
        this._typeSerializer = cVar._typeSerializer;
        this._nonTrivialBaseType = cVar._nonTrivialBaseType;
    }

    public c(c cVar, b.g.a.b.p.j jVar) {
        super(cVar);
        this._name = jVar;
        this._wrapperName = cVar._wrapperName;
        this._member = cVar._member;
        this.k = cVar.k;
        this._declaredType = cVar._declaredType;
        this.l = cVar.l;
        this.m = cVar.m;
        this._serializer = cVar._serializer;
        this._nullSerializer = cVar._nullSerializer;
        if (cVar.o != null) {
            this.o = new HashMap<>(cVar.o);
        }
        this._cfgSerializationType = cVar._cfgSerializationType;
        this.n = cVar.n;
        this._suppressNulls = cVar._suppressNulls;
        this._suppressableValue = cVar._suppressableValue;
        this._includeInViews = cVar._includeInViews;
        this._typeSerializer = cVar._typeSerializer;
        this._nonTrivialBaseType = cVar._nonTrivialBaseType;
    }
}
