package b.g.a.c.g0.t;

import b.g.a.c.d;
import b.g.a.c.e0.g;
import b.g.a.c.g0.i;
import b.g.a.c.n;
import b.g.a.c.x;
import com.fasterxml.jackson.databind.JsonMappingException;
import java.io.IOException;
/* compiled from: TypeWrappedSerializer.java */
/* loaded from: classes2.dex */
public final class p extends n<Object> implements i {
    public final g j;
    public final n<Object> k;

    public p(g gVar, n<?> nVar) {
        this.j = gVar;
        this.k = nVar;
    }

    @Override // b.g.a.c.g0.i
    public n<?> a(x xVar, d dVar) throws JsonMappingException {
        n<?> nVar = this.k;
        if (nVar instanceof i) {
            nVar = xVar.z(nVar, dVar);
        }
        return nVar == this.k ? this : new p(this.j, nVar);
    }

    @Override // b.g.a.c.n
    public Class<Object> c() {
        return Object.class;
    }

    @Override // b.g.a.c.n
    public void f(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
        this.k.g(obj, dVar, xVar, this.j);
    }

    @Override // b.g.a.c.n
    public void g(Object obj, b.g.a.b.d dVar, x xVar, g gVar) throws IOException {
        this.k.g(obj, dVar, xVar, gVar);
    }
}
