package b.g.a.c.g0.t;

import b.g.a.a.i0;
import b.g.a.b.k;
import b.g.a.c.n;
import b.g.a.c.t;
/* compiled from: ObjectIdWriter.java */
/* loaded from: classes2.dex */
public final class j {
    public final b.g.a.c.j a;

    /* renamed from: b  reason: collision with root package name */
    public final k f700b;
    public final i0<?> c;
    public final n<Object> d;
    public final boolean e;

    public j(b.g.a.c.j jVar, k kVar, i0<?> i0Var, n<?> nVar, boolean z2) {
        this.a = jVar;
        this.f700b = kVar;
        this.c = i0Var;
        this.d = nVar;
        this.e = z2;
    }

    public static j a(b.g.a.c.j jVar, t tVar, i0<?> i0Var, boolean z2) {
        b.g.a.b.p.j jVar2 = null;
        String str = tVar == null ? null : tVar._simpleName;
        if (str != null) {
            jVar2 = new b.g.a.b.p.j(str);
        }
        return new j(jVar, jVar2, i0Var, null, z2);
    }
}
