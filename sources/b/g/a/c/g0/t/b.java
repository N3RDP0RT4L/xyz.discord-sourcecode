package b.g.a.c.g0.t;

import b.d.b.a.a;
import b.g.a.b.h;
import b.g.a.c.e0.g;
import b.g.a.c.g0.c;
import b.g.a.c.g0.u.d;
import b.g.a.c.n;
import b.g.a.c.w;
import b.g.a.c.x;
import com.fasterxml.jackson.databind.JsonMappingException;
import java.io.IOException;
import java.util.Set;
/* compiled from: BeanAsArraySerializer.java */
/* loaded from: classes2.dex */
public class b extends d {
    private static final long serialVersionUID = 1;
    public final d _defaultSerializer;

    public b(d dVar, Set<String> set, Set<String> set2) {
        super(dVar, set, set2);
        this._defaultSerializer = dVar;
    }

    @Override // b.g.a.c.n
    public final void f(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
        if (xVar.D(w.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) {
            c[] cVarArr = this._filteredProps;
            if (cVarArr == null || xVar._serializationView == null) {
                cVarArr = this._props;
            }
            boolean z2 = true;
            if (cVarArr.length != 1) {
                z2 = false;
            }
            if (z2) {
                z(obj, dVar, xVar);
                return;
            }
        }
        dVar.X(obj);
        z(obj, dVar, xVar);
        dVar.t();
    }

    @Override // b.g.a.c.g0.u.d, b.g.a.c.n
    public void g(Object obj, b.g.a.b.d dVar, x xVar, g gVar) throws IOException {
        if (this._objectIdWriter != null) {
            p(obj, dVar, xVar, gVar);
            return;
        }
        b.g.a.b.s.b r = r(gVar, obj, h.START_ARRAY);
        gVar.e(dVar, r);
        dVar.e(obj);
        z(obj, dVar, xVar);
        gVar.f(dVar, r);
    }

    @Override // b.g.a.c.n
    public n<Object> h(b.g.a.c.i0.n nVar) {
        return this._defaultSerializer.h(nVar);
    }

    @Override // b.g.a.c.g0.u.d
    public d s() {
        return this;
    }

    public String toString() {
        StringBuilder R = a.R("BeanAsArraySerializer for ");
        R.append(this._handledType.getName());
        return R.toString();
    }

    @Override // b.g.a.c.g0.u.d
    public d v(Set set, Set set2) {
        return new b(this, set, set2);
    }

    @Override // b.g.a.c.g0.u.d
    public d w(Object obj) {
        return new b(this, this._objectIdWriter, obj);
    }

    @Override // b.g.a.c.g0.u.d
    public d x(j jVar) {
        return this._defaultSerializer.x(jVar);
    }

    @Override // b.g.a.c.g0.u.d
    public d y(c[] cVarArr, c[] cVarArr2) {
        return this;
    }

    public final void z(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
        String str = "[anySetter]";
        c[] cVarArr = this._filteredProps;
        if (cVarArr == null || xVar._serializationView == null) {
            cVarArr = this._props;
        }
        int i = 0;
        try {
            int length = cVarArr.length;
            while (i < length) {
                c cVar = cVarArr[i];
                if (cVar == null) {
                    dVar.A();
                } else {
                    cVar.h(obj, dVar, xVar);
                }
                i++;
            }
        } catch (Exception e) {
            if (i != cVarArr.length) {
                str = cVarArr[i]._name._value;
            }
            o(xVar, e, obj, str);
            throw null;
        } catch (StackOverflowError e2) {
            JsonMappingException jsonMappingException = new JsonMappingException(dVar, "Infinite recursion (StackOverflowError)", e2);
            if (i != cVarArr.length) {
                str = cVarArr[i]._name._value;
            }
            jsonMappingException.e(new JsonMappingException.a(obj, str));
            throw jsonMappingException;
        }
    }

    public b(d dVar, j jVar, Object obj) {
        super(dVar, jVar, obj);
        this._defaultSerializer = dVar;
    }

    public b(d dVar) {
        super(dVar, (j) null, dVar._propertyFilterId);
        this._defaultSerializer = dVar;
    }
}
