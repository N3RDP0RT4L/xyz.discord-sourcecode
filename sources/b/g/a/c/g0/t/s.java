package b.g.a.c.g0.t;

import b.g.a.a.p;
import b.g.a.b.d;
import b.g.a.b.p.j;
import b.g.a.c.e0.g;
import b.g.a.c.g0.c;
import b.g.a.c.i0.n;
import b.g.a.c.x;
import com.fasterxml.jackson.databind.JsonMappingException;
import java.io.Serializable;
import java.lang.reflect.Method;
/* compiled from: UnwrappingBeanPropertyWriter.java */
/* loaded from: classes2.dex */
public class s extends c implements Serializable {
    private static final long serialVersionUID = 1;
    public final n _nameTransformer;

    public s(s sVar, n nVar, j jVar) {
        super(sVar, jVar);
        this._nameTransformer = nVar;
    }

    @Override // b.g.a.c.g0.c
    public b.g.a.c.n<Object> c(l lVar, Class<?> cls, x xVar) throws JsonMappingException {
        b.g.a.c.n<Object> nVar;
        b.g.a.c.j jVar = this._nonTrivialBaseType;
        if (jVar != null) {
            nVar = xVar.t(xVar.k(jVar, cls), this);
        } else {
            nVar = xVar.u(cls, this);
        }
        n nVar2 = this._nameTransformer;
        if (nVar.e() && (nVar instanceof t)) {
            nVar2 = new n.a(nVar2, ((t) nVar)._nameTransformer);
        }
        b.g.a.c.n<Object> h = nVar.h(nVar2);
        this.n = this.n.b(cls, h);
        return h;
    }

    @Override // b.g.a.c.g0.c
    public void f(b.g.a.c.n<Object> nVar) {
        if (nVar != null) {
            n nVar2 = this._nameTransformer;
            if (nVar.e() && (nVar instanceof t)) {
                nVar2 = new n.a(nVar2, ((t) nVar)._nameTransformer);
            }
            nVar = nVar.h(nVar2);
        }
        super.f(nVar);
    }

    @Override // b.g.a.c.g0.c
    public c g(n nVar) {
        return new s(this, new n.a(nVar, this._nameTransformer), new j(nVar.a(this._name._value)));
    }

    @Override // b.g.a.c.g0.c
    public void i(Object obj, d dVar, x xVar) throws Exception {
        Method method = this.l;
        Object invoke = method == null ? this.m.get(obj) : method.invoke(obj, null);
        if (invoke != null) {
            b.g.a.c.n<Object> nVar = this._serializer;
            if (nVar == null) {
                Class<?> cls = invoke.getClass();
                l lVar = this.n;
                b.g.a.c.n<Object> c = lVar.c(cls);
                nVar = c == null ? c(lVar, cls, xVar) : c;
            }
            Object obj2 = this._suppressableValue;
            if (obj2 != null) {
                if (p.a.NON_EMPTY == obj2) {
                    if (nVar.d(xVar, invoke)) {
                        return;
                    }
                } else if (obj2.equals(invoke)) {
                    return;
                }
            }
            if (invoke != obj || !d(dVar, xVar, nVar)) {
                if (!nVar.e()) {
                    dVar.x(this._name);
                }
                g gVar = this._typeSerializer;
                if (gVar == null) {
                    nVar.f(invoke, dVar, xVar);
                } else {
                    nVar.g(invoke, dVar, xVar, gVar);
                }
            }
        }
    }

    public s(c cVar, n nVar) {
        super(cVar, cVar._name);
        this._nameTransformer = nVar;
    }
}
