package b.g.a.c.g0.t;

import b.d.b.a.a;
import b.g.a.a.i0;
import b.g.a.a.l0;
import b.g.a.c.c0.a0;
import b.g.a.c.g0.c;
import java.lang.reflect.Method;
/* compiled from: PropertyBasedObjectIdGenerator.java */
/* loaded from: classes2.dex */
public class k extends l0 {
    private static final long serialVersionUID = 1;
    public final c _property;

    public k(Class<?> cls, c cVar) {
        super(cls);
        this._property = cVar;
    }

    @Override // b.g.a.a.j0, b.g.a.a.i0
    public boolean a(i0<?> i0Var) {
        if (i0Var.getClass() != k.class) {
            return false;
        }
        k kVar = (k) i0Var;
        return kVar._scope == this._scope && kVar._property == this._property;
    }

    @Override // b.g.a.a.i0
    public i0<Object> b(Class<?> cls) {
        return cls == this._scope ? this : new k(cls, this._property);
    }

    @Override // b.g.a.a.i0
    public Object c(Object obj) {
        try {
            c cVar = this._property;
            Method method = cVar.l;
            return method == null ? cVar.m.get(obj) : method.invoke(obj, null);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e2) {
            StringBuilder R = a.R("Problem accessing property '");
            R.append(this._property._name._value);
            R.append("': ");
            R.append(e2.getMessage());
            throw new IllegalStateException(R.toString(), e2);
        }
    }

    @Override // b.g.a.a.i0
    public i0<Object> e(Object obj) {
        return this;
    }

    public k(a0 a0Var, c cVar) {
        super(a0Var.e);
        this._property = cVar;
    }
}
