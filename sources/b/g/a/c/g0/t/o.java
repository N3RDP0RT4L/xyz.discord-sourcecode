package b.g.a.c.g0.t;

import b.g.a.b.d;
import b.g.a.b.h;
import b.g.a.b.s.b;
import b.g.a.c.e0.g;
import b.g.a.c.g0.u.h0;
import b.g.a.c.n;
import b.g.a.c.w;
import b.g.a.c.x;
import b.g.a.c.y.a;
import java.io.IOException;
import java.util.Collection;
/* compiled from: StringCollectionSerializer.java */
@a
/* loaded from: classes2.dex */
public class o extends h0<Collection<String>> {
    public static final o k = new o();

    public o() {
        super(Collection.class);
    }

    @Override // b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        Collection<String> collection = (Collection) obj;
        int size = collection.size();
        if (size != 1 || ((this._unwrapSingle != null || !xVar.D(w.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) && this._unwrapSingle != Boolean.TRUE)) {
            dVar.b0(collection, size);
            r(collection, dVar, xVar);
            dVar.t();
            return;
        }
        r(collection, dVar, xVar);
    }

    @Override // b.g.a.c.g0.u.h0, b.g.a.c.n
    public void g(Object obj, d dVar, x xVar, g gVar) throws IOException {
        Collection<String> collection = (Collection) obj;
        b e = gVar.e(dVar, gVar.d(collection, h.START_ARRAY));
        dVar.e(collection);
        r(collection, dVar, xVar);
        gVar.f(dVar, e);
    }

    @Override // b.g.a.c.g0.u.h0
    public n<?> p(b.g.a.c.d dVar, Boolean bool) {
        return new o(this, bool);
    }

    @Override // b.g.a.c.g0.u.h0
    public void q(Collection<String> collection, d dVar, x xVar, g gVar) throws IOException {
        b e = gVar.e(dVar, gVar.d(collection, h.START_ARRAY));
        dVar.e(collection);
        r(collection, dVar, xVar);
        gVar.f(dVar, e);
    }

    public final void r(Collection<String> collection, d dVar, x xVar) throws IOException {
        int i = 0;
        try {
            for (String str : collection) {
                if (str == null) {
                    xVar.l(dVar);
                } else {
                    dVar.j0(str);
                }
                i++;
            }
        } catch (Exception e) {
            n(xVar, e, collection, i);
            throw null;
        }
    }

    public o(o oVar, Boolean bool) {
        super(oVar, bool);
    }
}
