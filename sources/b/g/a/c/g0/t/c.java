package b.g.a.c.g0.t;

import b.g.a.b.d;
import b.g.a.c.g0.u.q0;
import b.g.a.c.x;
import java.io.IOException;
/* compiled from: FailingSerializer.java */
/* loaded from: classes2.dex */
public class c extends q0<Object> {
    public final String _msg;

    public c(String str) {
        super(Object.class);
        this._msg = str;
    }

    @Override // b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        xVar.G(this._msg, new Object[0]);
        throw null;
    }
}
