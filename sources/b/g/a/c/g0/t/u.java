package b.g.a.c.g0.t;

import b.g.a.a.i0;
import b.g.a.b.d;
import b.g.a.c.x;
import java.io.IOException;
import java.util.Objects;
/* compiled from: WritableObjectId.java */
/* loaded from: classes2.dex */
public final class u {
    public final i0<?> a;

    /* renamed from: b  reason: collision with root package name */
    public Object f709b;
    public boolean c = false;

    public u(i0<?> i0Var) {
        this.a = i0Var;
    }

    public boolean a(d dVar, x xVar, j jVar) throws IOException {
        if (this.f709b == null) {
            return false;
        }
        if (!this.c && !jVar.e) {
            return false;
        }
        Objects.requireNonNull(dVar);
        jVar.d.f(this.f709b, dVar, xVar);
        return true;
    }
}
