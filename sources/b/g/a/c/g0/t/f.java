package b.g.a.c.g0.t;

import b.g.a.b.d;
import b.g.a.c.e0.g;
import b.g.a.c.g0.h;
import b.g.a.c.g0.t.l;
import b.g.a.c.g0.u.b;
import b.g.a.c.j;
import b.g.a.c.n;
import b.g.a.c.w;
import b.g.a.c.x;
import b.g.a.c.y.a;
import java.io.IOException;
import java.util.List;
/* compiled from: IndexedListSerializer.java */
@a
/* loaded from: classes2.dex */
public final class f extends b<List<?>> {
    private static final long serialVersionUID = 1;

    public f(j jVar, boolean z2, g gVar, n<Object> nVar) {
        super(List.class, jVar, z2, gVar, nVar);
    }

    @Override // b.g.a.c.n
    public boolean d(x xVar, Object obj) {
        return ((List) obj).isEmpty();
    }

    @Override // b.g.a.c.g0.u.b, b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        List<?> list = (List) obj;
        int size = list.size();
        if (size != 1 || ((this._unwrapSingle != null || !xVar.D(w.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) && this._unwrapSingle != Boolean.TRUE)) {
            dVar.b0(list, size);
            s(list, dVar, xVar);
            dVar.t();
            return;
        }
        s(list, dVar, xVar);
    }

    @Override // b.g.a.c.g0.h
    public h<?> p(g gVar) {
        return new f(this, this._property, gVar, this._elementSerializer, this._unwrapSingle);
    }

    @Override // b.g.a.c.g0.h
    public boolean q(Object obj) {
        return ((List) obj).size() == 1;
    }

    @Override // b.g.a.c.g0.u.b
    public b<List<?>> t(b.g.a.c.d dVar, g gVar, n nVar, Boolean bool) {
        return new f(this, dVar, gVar, nVar, bool);
    }

    /* renamed from: u */
    public void s(List<?> list, d dVar, x xVar) throws IOException {
        n<Object> nVar;
        n<Object> nVar2;
        n<Object> nVar3 = this._elementSerializer;
        int i = 0;
        if (nVar3 != null) {
            int size = list.size();
            if (size != 0) {
                g gVar = this._valueTypeSerializer;
                while (i < size) {
                    Object obj = list.get(i);
                    if (obj == null) {
                        try {
                            xVar.l(dVar);
                        } catch (Exception e) {
                            n(xVar, e, list, i);
                            throw null;
                        }
                    } else if (gVar == null) {
                        nVar3.f(obj, dVar, xVar);
                    } else {
                        nVar3.g(obj, dVar, xVar, gVar);
                    }
                    i++;
                }
            }
        } else if (this._valueTypeSerializer != null) {
            int size2 = list.size();
            if (size2 != 0) {
                try {
                    g gVar2 = this._valueTypeSerializer;
                    l lVar = this._dynamicSerializers;
                    while (i < size2) {
                        Object obj2 = list.get(i);
                        if (obj2 == null) {
                            xVar.l(dVar);
                        } else {
                            Class<?> cls = obj2.getClass();
                            n<Object> c = lVar.c(cls);
                            if (c == null) {
                                if (this._elementType.r()) {
                                    l.d a = lVar.a(xVar.k(this._elementType, cls), xVar, this._property);
                                    l lVar2 = a.f704b;
                                    if (lVar != lVar2) {
                                        this._dynamicSerializers = lVar2;
                                    }
                                    nVar2 = a.a;
                                } else {
                                    nVar2 = r(lVar, cls, xVar);
                                }
                                c = nVar2;
                                lVar = this._dynamicSerializers;
                            }
                            c.g(obj2, dVar, xVar, gVar2);
                        }
                        i++;
                    }
                } catch (Exception e2) {
                    n(xVar, e2, list, i);
                    throw null;
                }
            }
        } else {
            int size3 = list.size();
            if (size3 != 0) {
                try {
                    l lVar3 = this._dynamicSerializers;
                    while (i < size3) {
                        Object obj3 = list.get(i);
                        if (obj3 == null) {
                            xVar.l(dVar);
                        } else {
                            Class<?> cls2 = obj3.getClass();
                            n<Object> c2 = lVar3.c(cls2);
                            if (c2 == null) {
                                if (this._elementType.r()) {
                                    l.d a2 = lVar3.a(xVar.k(this._elementType, cls2), xVar, this._property);
                                    l lVar4 = a2.f704b;
                                    if (lVar3 != lVar4) {
                                        this._dynamicSerializers = lVar4;
                                    }
                                    nVar = a2.a;
                                } else {
                                    nVar = r(lVar3, cls2, xVar);
                                }
                                c2 = nVar;
                                lVar3 = this._dynamicSerializers;
                            }
                            c2.f(obj3, dVar, xVar);
                        }
                        i++;
                    }
                } catch (Exception e3) {
                    n(xVar, e3, list, i);
                    throw null;
                }
            }
        }
    }

    public f(f fVar, b.g.a.c.d dVar, g gVar, n<?> nVar, Boolean bool) {
        super(fVar, dVar, gVar, nVar, bool);
    }
}
