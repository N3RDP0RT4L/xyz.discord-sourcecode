package b.g.a.c.g0.t;

import b.g.a.b.d;
import b.g.a.c.e0.g;
import b.g.a.c.g0.t.l;
import b.g.a.c.g0.u.b;
import b.g.a.c.j;
import b.g.a.c.n;
import b.g.a.c.x;
import b.g.a.c.y.a;
import java.io.IOException;
import java.util.Iterator;
/* compiled from: IteratorSerializer.java */
@a
/* loaded from: classes2.dex */
public class h extends b<Iterator<?>> {
    public h(j jVar, boolean z2, g gVar) {
        super(Iterator.class, jVar, z2, gVar, (n<Object>) null);
    }

    @Override // b.g.a.c.n
    public boolean d(x xVar, Object obj) {
        return !((Iterator) obj).hasNext();
    }

    @Override // b.g.a.c.g0.u.b, b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        Iterator<?> it = (Iterator) obj;
        dVar.X(it);
        s(it, dVar, xVar);
        dVar.t();
    }

    @Override // b.g.a.c.g0.h
    public b.g.a.c.g0.h<?> p(g gVar) {
        return new h(this, this._property, gVar, this._elementSerializer, this._unwrapSingle);
    }

    @Override // b.g.a.c.g0.h
    public /* bridge */ /* synthetic */ boolean q(Object obj) {
        Iterator it = (Iterator) obj;
        return false;
    }

    @Override // b.g.a.c.g0.u.b
    public b<Iterator<?>> t(b.g.a.c.d dVar, g gVar, n nVar, Boolean bool) {
        return new h(this, dVar, gVar, nVar, bool);
    }

    /* renamed from: u */
    public void s(Iterator<?> it, d dVar, x xVar) throws IOException {
        n<Object> nVar;
        if (it.hasNext()) {
            n<Object> nVar2 = this._elementSerializer;
            if (nVar2 == null) {
                g gVar = this._valueTypeSerializer;
                l lVar = this._dynamicSerializers;
                do {
                    Object next = it.next();
                    if (next == null) {
                        xVar.l(dVar);
                    } else {
                        Class<?> cls = next.getClass();
                        n<Object> c = lVar.c(cls);
                        if (c == null) {
                            if (this._elementType.r()) {
                                l.d a = lVar.a(xVar.k(this._elementType, cls), xVar, this._property);
                                l lVar2 = a.f704b;
                                if (lVar != lVar2) {
                                    this._dynamicSerializers = lVar2;
                                }
                                nVar = a.a;
                            } else {
                                nVar = r(lVar, cls, xVar);
                            }
                            c = nVar;
                            lVar = this._dynamicSerializers;
                        }
                        if (gVar == null) {
                            c.f(next, dVar, xVar);
                        } else {
                            c.g(next, dVar, xVar, gVar);
                        }
                    }
                } while (it.hasNext());
                return;
            }
            g gVar2 = this._valueTypeSerializer;
            do {
                Object next2 = it.next();
                if (next2 == null) {
                    xVar.l(dVar);
                } else if (gVar2 == null) {
                    nVar2.f(next2, dVar, xVar);
                } else {
                    nVar2.g(next2, dVar, xVar, gVar2);
                }
            } while (it.hasNext());
        }
    }

    public h(h hVar, b.g.a.c.d dVar, g gVar, n<?> nVar, Boolean bool) {
        super(hVar, dVar, gVar, nVar, bool);
    }
}
