package b.g.a.c.g0.t;

import b.d.b.a.a;
import b.g.a.c.e0.g;
import b.g.a.c.g0.c;
import b.g.a.c.g0.u.d;
import b.g.a.c.i0.n;
import b.g.a.c.w;
import b.g.a.c.x;
import java.io.IOException;
import java.io.Serializable;
import java.util.Set;
/* compiled from: UnwrappingBeanSerializer.java */
/* loaded from: classes2.dex */
public class t extends d implements Serializable {
    private static final long serialVersionUID = 1;
    public final n _nameTransformer;

    public t(t tVar, j jVar, Object obj) {
        super(tVar, jVar, obj);
        this._nameTransformer = tVar._nameTransformer;
    }

    @Override // b.g.a.c.n
    public final void f(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
        dVar.e(obj);
        if (this._objectIdWriter != null) {
            q(obj, dVar, xVar, false);
            return;
        }
        Object obj2 = this._propertyFilterId;
        if (obj2 != null) {
            if (this._filteredProps != null) {
                Class<?> cls = xVar._serializationView;
            }
            m(xVar, obj2, obj);
            throw null;
        }
        u(obj, dVar, xVar);
    }

    @Override // b.g.a.c.g0.u.d, b.g.a.c.n
    public void g(Object obj, b.g.a.b.d dVar, x xVar, g gVar) throws IOException {
        if (xVar.D(w.FAIL_ON_UNWRAPPED_TYPE_IDENTIFIERS)) {
            xVar.g(this._handledType, "Unwrapped property requires use of type information: cannot serialize without disabling `SerializationFeature.FAIL_ON_UNWRAPPED_TYPE_IDENTIFIERS`");
        }
        dVar.e(obj);
        if (this._objectIdWriter != null) {
            p(obj, dVar, xVar, gVar);
            return;
        }
        Object obj2 = this._propertyFilterId;
        if (obj2 != null) {
            c[] cVarArr = this._filteredProps;
            m(xVar, obj2, obj);
            throw null;
        }
        u(obj, dVar, xVar);
    }

    @Override // b.g.a.c.n
    public b.g.a.c.n<Object> h(n nVar) {
        return new t(this, nVar);
    }

    @Override // b.g.a.c.g0.u.d
    public d s() {
        return this;
    }

    public String toString() {
        StringBuilder R = a.R("UnwrappingBeanSerializer for ");
        R.append(this._handledType.getName());
        return R.toString();
    }

    @Override // b.g.a.c.g0.u.d
    public d v(Set<String> set, Set<String> set2) {
        return new t(this, set, set2);
    }

    @Override // b.g.a.c.g0.u.d
    public d w(Object obj) {
        return new t(this, this._objectIdWriter, obj);
    }

    @Override // b.g.a.c.g0.u.d
    public d x(j jVar) {
        return new t(this, jVar);
    }

    @Override // b.g.a.c.g0.u.d
    public d y(c[] cVarArr, c[] cVarArr2) {
        return new t(this, cVarArr, cVarArr2);
    }

    public t(t tVar, Set<String> set, Set<String> set2) {
        super(tVar, set, set2);
        this._nameTransformer = tVar._nameTransformer;
    }

    public t(t tVar, c[] cVarArr, c[] cVarArr2) {
        super(tVar, cVarArr, cVarArr2);
        this._nameTransformer = tVar._nameTransformer;
    }

    public t(t tVar, j jVar) {
        super(tVar, jVar, tVar._propertyFilterId);
        this._nameTransformer = tVar._nameTransformer;
    }

    public t(d dVar, n nVar) {
        super(dVar, d.t(dVar._props, nVar), d.t(dVar._filteredProps, nVar));
        this._nameTransformer = nVar;
    }
}
