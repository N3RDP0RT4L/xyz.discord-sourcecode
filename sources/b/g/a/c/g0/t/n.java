package b.g.a.c.g0.t;

import b.g.a.b.d;
import b.g.a.c.e0.g;
import b.g.a.c.g0.h;
import b.g.a.c.g0.i;
import b.g.a.c.w;
import b.g.a.c.x;
import b.g.a.c.y.a;
import java.io.IOException;
/* compiled from: StringArraySerializer.java */
@a
/* loaded from: classes2.dex */
public class n extends b.g.a.c.g0.u.a<String[]> implements i {
    public static final n k = new n();
    public final b.g.a.c.n<Object> _elementSerializer;

    static {
        b.g.a.c.h0.n.k.j(String.class);
    }

    public n() {
        super(String[].class);
        this._elementSerializer = null;
    }

    /* JADX WARN: Removed duplicated region for block: B:12:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:13:0x0028  */
    /* JADX WARN: Removed duplicated region for block: B:15:0x002b  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x0033  */
    /* JADX WARN: Removed duplicated region for block: B:21:0x0040  */
    @Override // b.g.a.c.g0.u.a, b.g.a.c.g0.i
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.g.a.c.n<?> a(b.g.a.c.x r5, b.g.a.c.d r6) throws com.fasterxml.jackson.databind.JsonMappingException {
        /*
            r4 = this;
            r0 = 0
            if (r6 == 0) goto L18
            b.g.a.c.b r1 = r5.v()
            b.g.a.c.c0.i r2 = r6.getMember()
            if (r2 == 0) goto L18
            java.lang.Object r1 = r1.c(r2)
            if (r1 == 0) goto L18
            b.g.a.c.n r1 = r5.H(r2, r1)
            goto L19
        L18:
            r1 = r0
        L19:
            java.lang.Class<java.lang.String[]> r2 = java.lang.String[].class
            b.g.a.a.i$a r3 = b.g.a.a.i.a.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED
            b.g.a.a.i$d r2 = r4.l(r5, r6, r2)
            if (r2 == 0) goto L28
            java.lang.Boolean r2 = r2.b(r3)
            goto L29
        L28:
            r2 = r0
        L29:
            if (r1 != 0) goto L2d
            b.g.a.c.n<java.lang.Object> r1 = r4._elementSerializer
        L2d:
            b.g.a.c.n r1 = r4.k(r5, r6, r1)
            if (r1 != 0) goto L39
            java.lang.Class<java.lang.String> r1 = java.lang.String.class
            b.g.a.c.n r1 = r5.n(r1, r6)
        L39:
            boolean r5 = b.g.a.c.i0.d.s(r1)
            if (r5 == 0) goto L40
            goto L41
        L40:
            r0 = r1
        L41:
            b.g.a.c.n<java.lang.Object> r5 = r4._elementSerializer
            if (r0 != r5) goto L4e
            java.lang.Boolean r5 = r4._unwrapSingle
            boolean r5 = java.util.Objects.equals(r2, r5)
            if (r5 == 0) goto L4e
            return r4
        L4e:
            b.g.a.c.g0.t.n r5 = new b.g.a.c.g0.t.n
            r5.<init>(r4, r6, r0, r2)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.c.g0.t.n.a(b.g.a.c.x, b.g.a.c.d):b.g.a.c.n");
    }

    @Override // b.g.a.c.n
    public boolean d(x xVar, Object obj) {
        return ((String[]) obj).length == 0;
    }

    @Override // b.g.a.c.g0.u.a, b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        String[] strArr = (String[]) obj;
        int length = strArr.length;
        if (length != 1 || ((this._unwrapSingle != null || !xVar.D(w.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) && this._unwrapSingle != Boolean.TRUE)) {
            dVar.b0(strArr, length);
            t(strArr, dVar, xVar);
            dVar.t();
            return;
        }
        t(strArr, dVar, xVar);
    }

    @Override // b.g.a.c.g0.h
    public h<?> p(g gVar) {
        return this;
    }

    @Override // b.g.a.c.g0.h
    public boolean q(Object obj) {
        return ((String[]) obj).length == 1;
    }

    @Override // b.g.a.c.g0.u.a
    public b.g.a.c.n<?> s(b.g.a.c.d dVar, Boolean bool) {
        return new n(this, dVar, this._elementSerializer, bool);
    }

    /* renamed from: u */
    public void t(String[] strArr, d dVar, x xVar) throws IOException {
        int length = strArr.length;
        if (length != 0) {
            b.g.a.c.n<Object> nVar = this._elementSerializer;
            int i = 0;
            if (nVar != null) {
                int length2 = strArr.length;
                while (i < length2) {
                    if (strArr[i] == null) {
                        xVar.l(dVar);
                    } else {
                        nVar.f(strArr[i], dVar, xVar);
                    }
                    i++;
                }
                return;
            }
            while (i < length) {
                if (strArr[i] == null) {
                    dVar.A();
                } else {
                    dVar.j0(strArr[i]);
                }
                i++;
            }
        }
    }

    public n(n nVar, b.g.a.c.d dVar, b.g.a.c.n<?> nVar2, Boolean bool) {
        super(nVar, dVar, bool);
        this._elementSerializer = nVar2;
    }
}
