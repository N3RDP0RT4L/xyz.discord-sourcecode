package b.g.a.c.g0.t;

import b.g.a.b.d;
import b.g.a.b.h;
import b.g.a.b.s.b;
import b.g.a.c.g0.u.h0;
import b.g.a.c.n;
import b.g.a.c.w;
import b.g.a.c.x;
import b.g.a.c.y.a;
import java.io.IOException;
import java.util.List;
/* compiled from: IndexedStringListSerializer.java */
@a
/* loaded from: classes2.dex */
public final class g extends h0<List<String>> {
    public static final g k = new g();
    private static final long serialVersionUID = 1;

    public g() {
        super(List.class);
    }

    @Override // b.g.a.c.n
    public void f(Object obj, d dVar, x xVar) throws IOException {
        List<String> list = (List) obj;
        int size = list.size();
        if (size != 1 || ((this._unwrapSingle != null || !xVar.D(w.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED)) && this._unwrapSingle != Boolean.TRUE)) {
            dVar.b0(list, size);
            r(list, dVar, xVar, size);
            dVar.t();
            return;
        }
        r(list, dVar, xVar, 1);
    }

    @Override // b.g.a.c.g0.u.h0
    public n<?> p(b.g.a.c.d dVar, Boolean bool) {
        return new g(this, bool);
    }

    public final void r(List<String> list, d dVar, x xVar, int i) throws IOException {
        for (int i2 = 0; i2 < i; i2++) {
            try {
                String str = list.get(i2);
                if (str == null) {
                    xVar.l(dVar);
                } else {
                    dVar.j0(str);
                }
            } catch (Exception e) {
                n(xVar, e, list, i2);
                throw null;
            }
        }
    }

    /* renamed from: s */
    public void q(List<String> list, d dVar, x xVar, b.g.a.c.e0.g gVar) throws IOException {
        b e = gVar.e(dVar, gVar.d(list, h.START_ARRAY));
        dVar.e(list);
        r(list, dVar, xVar, list.size());
        gVar.f(dVar, e);
    }

    public g(g gVar, Boolean bool) {
        super(gVar, bool);
    }
}
