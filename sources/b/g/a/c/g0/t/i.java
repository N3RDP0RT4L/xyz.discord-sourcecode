package b.g.a.c.g0.t;

import b.g.a.a.p;
import b.g.a.c.b;
import b.g.a.c.d;
import b.g.a.c.e0.g;
import b.g.a.c.g0.h;
import b.g.a.c.g0.t.l;
import b.g.a.c.j;
import b.g.a.c.n;
import b.g.a.c.x;
import b.g.a.c.y.a;
import com.fasterxml.jackson.databind.JsonMappingException;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;
/* compiled from: MapEntrySerializer.java */
@a
/* loaded from: classes2.dex */
public class i extends h<Map.Entry<?, ?>> implements b.g.a.c.g0.i {
    public l _dynamicValueSerializers;
    public final j _entryType;
    public n<Object> _keySerializer;
    public final j _keyType;
    public final d _property;
    public final boolean _suppressNulls;
    public final Object _suppressableValue;
    public n<Object> _valueSerializer;
    public final j _valueType;
    public final boolean _valueTypeIsStatic;
    public final g _valueTypeSerializer;

    public i(j jVar, j jVar2, j jVar3, boolean z2, g gVar, d dVar) {
        super(jVar);
        this._entryType = jVar;
        this._keyType = jVar2;
        this._valueType = jVar3;
        this._valueTypeIsStatic = z2;
        this._valueTypeSerializer = gVar;
        this._property = dVar;
        this._dynamicValueSerializers = l.b.f702b;
        this._suppressableValue = null;
        this._suppressNulls = false;
    }

    @Override // b.g.a.c.g0.i
    public n<?> a(x xVar, d dVar) throws JsonMappingException {
        n<?> nVar;
        n<Object> nVar2;
        n<?> nVar3;
        boolean z2;
        Object obj;
        p.b b2;
        p.a aVar;
        Object obj2 = p.a.NON_EMPTY;
        b v = xVar.v();
        b.g.a.c.c0.i member = dVar == null ? null : dVar.getMember();
        if (member == null || v == null) {
            nVar2 = null;
            nVar = null;
        } else {
            Object l = v.l(member);
            nVar = l != null ? xVar.H(member, l) : null;
            Object c = v.c(member);
            nVar2 = c != null ? xVar.H(member, c) : null;
        }
        if (nVar2 == null) {
            nVar2 = this._valueSerializer;
        }
        n<?> k = k(xVar, dVar, nVar2);
        if (k == null && this._valueTypeIsStatic && !this._valueType.y()) {
            k = xVar.m(this._valueType, dVar);
        }
        n<?> nVar4 = k;
        if (nVar == null) {
            nVar = this._keySerializer;
        }
        if (nVar == null) {
            nVar3 = xVar.o(this._keyType, dVar);
        } else {
            nVar3 = xVar.z(nVar, dVar);
        }
        n<?> nVar5 = nVar3;
        Object obj3 = this._suppressableValue;
        boolean z3 = this._suppressNulls;
        if (dVar == null || (b2 = dVar.b(xVar._config, null)) == null || (aVar = b2._contentInclusion) == p.a.USE_DEFAULTS) {
            obj = obj3;
            z2 = z3;
        } else {
            int ordinal = aVar.ordinal();
            if (ordinal != 1) {
                if (ordinal != 2) {
                    if (ordinal != 3) {
                        if (ordinal == 4) {
                            obj2 = b.c.a.a0.d.t0(this._valueType);
                            if (obj2 != null && obj2.getClass().isArray()) {
                                obj2 = b.c.a.a0.d.q0(obj2);
                            }
                        } else if (ordinal != 5) {
                            obj = null;
                            z2 = false;
                        } else {
                            obj2 = xVar.A(null, b2._contentFilter);
                            if (obj2 != null) {
                                z2 = xVar.B(obj2);
                                obj = obj2;
                            }
                        }
                    }
                } else if (!this._valueType.b()) {
                    obj2 = null;
                }
                obj = obj2;
            } else {
                obj = null;
            }
            z2 = true;
        }
        return new i(this, nVar5, nVar4, obj, z2);
    }

    @Override // b.g.a.c.n
    public boolean d(x xVar, Object obj) {
        Object value = ((Map.Entry) obj).getValue();
        if (value == null) {
            return this._suppressNulls;
        }
        if (this._suppressableValue != null) {
            n<Object> nVar = this._valueSerializer;
            if (nVar == null) {
                Class<?> cls = value.getClass();
                n<Object> c = this._dynamicValueSerializers.c(cls);
                if (c == null) {
                    try {
                        l lVar = this._dynamicValueSerializers;
                        d dVar = this._property;
                        Objects.requireNonNull(lVar);
                        n<Object> n = xVar.n(cls, dVar);
                        l b2 = lVar.b(cls, n);
                        if (lVar != b2) {
                            this._dynamicValueSerializers = b2;
                        }
                        nVar = n;
                    } catch (JsonMappingException unused) {
                    }
                } else {
                    nVar = c;
                }
            }
            Object obj2 = this._suppressableValue;
            if (obj2 == p.a.NON_EMPTY) {
                return nVar.d(xVar, value);
            }
            return obj2.equals(value);
        }
        return false;
    }

    @Override // b.g.a.c.n
    public void f(Object obj, b.g.a.b.d dVar, x xVar) throws IOException {
        Map.Entry<?, ?> entry = (Map.Entry) obj;
        dVar.d0(entry);
        r(entry, dVar, xVar);
        dVar.u();
    }

    @Override // b.g.a.c.n
    public void g(Object obj, b.g.a.b.d dVar, x xVar, g gVar) throws IOException {
        Map.Entry<?, ?> entry = (Map.Entry) obj;
        dVar.e(entry);
        b.g.a.b.s.b e = gVar.e(dVar, gVar.d(entry, b.g.a.b.h.START_OBJECT));
        r(entry, dVar, xVar);
        gVar.f(dVar, e);
    }

    @Override // b.g.a.c.g0.h
    public h<?> p(g gVar) {
        return new i(this, this._keySerializer, this._valueSerializer, this._suppressableValue, this._suppressNulls);
    }

    public void r(Map.Entry<?, ?> entry, b.g.a.b.d dVar, x xVar) throws IOException {
        n<Object> nVar;
        n<Object> nVar2;
        g gVar = this._valueTypeSerializer;
        Object key = entry.getKey();
        if (key == null) {
            nVar = xVar._nullKeySerializer;
        } else {
            nVar = this._keySerializer;
        }
        Object value = entry.getValue();
        if (value != null) {
            nVar2 = this._valueSerializer;
            if (nVar2 == null) {
                Class<?> cls = value.getClass();
                n<Object> c = this._dynamicValueSerializers.c(cls);
                if (c != null) {
                    nVar2 = c;
                } else if (this._valueType.r()) {
                    l lVar = this._dynamicValueSerializers;
                    l.d a = lVar.a(xVar.k(this._valueType, cls), xVar, this._property);
                    l lVar2 = a.f704b;
                    if (lVar != lVar2) {
                        this._dynamicValueSerializers = lVar2;
                    }
                    nVar2 = a.a;
                } else {
                    l lVar3 = this._dynamicValueSerializers;
                    d dVar2 = this._property;
                    Objects.requireNonNull(lVar3);
                    n<Object> n = xVar.n(cls, dVar2);
                    l b2 = lVar3.b(cls, n);
                    if (lVar3 != b2) {
                        this._dynamicValueSerializers = b2;
                    }
                    nVar2 = n;
                }
            }
            Object obj = this._suppressableValue;
            if (obj != null && ((obj == p.a.NON_EMPTY && nVar2.d(xVar, value)) || this._suppressableValue.equals(value))) {
                return;
            }
        } else if (!this._suppressNulls) {
            nVar2 = xVar._nullValueSerializer;
        } else {
            return;
        }
        nVar.f(key, dVar, xVar);
        try {
            if (gVar == null) {
                nVar2.f(value, dVar, xVar);
            } else {
                nVar2.g(value, dVar, xVar, gVar);
            }
        } catch (Exception e) {
            o(xVar, e, entry, b.d.b.a.a.u("", key));
            throw null;
        }
    }

    public i(i iVar, n nVar, n nVar2, Object obj, boolean z2) {
        super(Map.class, false);
        this._entryType = iVar._entryType;
        this._keyType = iVar._keyType;
        this._valueType = iVar._valueType;
        this._valueTypeIsStatic = iVar._valueTypeIsStatic;
        this._valueTypeSerializer = iVar._valueTypeSerializer;
        this._keySerializer = nVar;
        this._valueSerializer = nVar2;
        this._dynamicValueSerializers = l.b.f702b;
        this._property = iVar._property;
        this._suppressableValue = obj;
        this._suppressNulls = z2;
    }
}
