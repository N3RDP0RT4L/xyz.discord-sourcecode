package b.g.a.c.g0.t;

import b.g.a.c.i0.u;
import b.g.a.c.j;
import b.g.a.c.n;
import java.util.Map;
/* compiled from: ReadOnlyClassToSerializerMap.java */
/* loaded from: classes2.dex */
public final class m {
    public final a[] a;

    /* renamed from: b  reason: collision with root package name */
    public final int f707b;

    /* compiled from: ReadOnlyClassToSerializerMap.java */
    /* loaded from: classes2.dex */
    public static final class a {
        public final n<Object> a;

        /* renamed from: b  reason: collision with root package name */
        public final a f708b;
        public final Class<?> c;
        public final j d;
        public final boolean e;

        public a(a aVar, u uVar, n<Object> nVar) {
            this.f708b = aVar;
            this.a = nVar;
            this.e = uVar.d;
            this.c = uVar.f726b;
            this.d = uVar.c;
        }
    }

    public m(Map<u, n<Object>> map) {
        int size = map.size();
        int i = 8;
        while (i < (size <= 64 ? size + size : size + (size >> 2))) {
            i += i;
        }
        this.f707b = i - 1;
        a[] aVarArr = new a[i];
        for (Map.Entry<u, n<Object>> entry : map.entrySet()) {
            u key = entry.getKey();
            int i2 = key.a & this.f707b;
            aVarArr[i2] = new a(aVarArr[i2], key, entry.getValue());
        }
        this.a = aVarArr;
    }

    public n<Object> a(j jVar) {
        boolean z2;
        a aVar = this.a[(jVar._hash - 1) & this.f707b];
        if (aVar == null) {
            return null;
        }
        if (!aVar.e && jVar.equals(aVar.d)) {
            return aVar.a;
        }
        do {
            aVar = aVar.f708b;
            if (aVar == null) {
                return null;
            }
            if (aVar.e || !jVar.equals(aVar.d)) {
                z2 = false;
                continue;
            } else {
                z2 = true;
                continue;
            }
        } while (!z2);
        return aVar.a;
    }

    public n<Object> b(Class<?> cls) {
        boolean z2;
        a aVar = this.a[cls.getName().hashCode() & this.f707b];
        if (aVar == null) {
            return null;
        }
        if (aVar.c == cls && !aVar.e) {
            return aVar.a;
        }
        do {
            aVar = aVar.f708b;
            if (aVar == null) {
                return null;
            }
            if (aVar.c != cls || aVar.e) {
                z2 = false;
                continue;
            } else {
                z2 = true;
                continue;
            }
        } while (!z2);
        return aVar.a;
    }
}
