package b.g.a.c.g0.t;

import b.g.a.c.j;
import b.g.a.c.n;
import b.g.a.c.x;
import com.fasterxml.jackson.databind.JsonMappingException;
import java.util.Arrays;
/* compiled from: PropertySerializerMap.java */
/* loaded from: classes2.dex */
public abstract class l {
    public final boolean a;

    /* compiled from: PropertySerializerMap.java */
    /* loaded from: classes2.dex */
    public static final class a extends l {

        /* renamed from: b  reason: collision with root package name */
        public final Class<?> f701b;
        public final Class<?> c;
        public final n<Object> d;
        public final n<Object> e;

        public a(l lVar, Class<?> cls, n<Object> nVar, Class<?> cls2, n<Object> nVar2) {
            super(lVar);
            this.f701b = cls;
            this.d = nVar;
            this.c = cls2;
            this.e = nVar2;
        }

        @Override // b.g.a.c.g0.t.l
        public l b(Class<?> cls, n<Object> nVar) {
            return new c(this, new f[]{new f(this.f701b, this.d), new f(this.c, this.e), new f(cls, nVar)});
        }

        @Override // b.g.a.c.g0.t.l
        public n<Object> c(Class<?> cls) {
            if (cls == this.f701b) {
                return this.d;
            }
            if (cls == this.c) {
                return this.e;
            }
            return null;
        }
    }

    /* compiled from: PropertySerializerMap.java */
    /* loaded from: classes2.dex */
    public static final class b extends l {

        /* renamed from: b  reason: collision with root package name */
        public static final b f702b = new b(false);

        public b(boolean z2) {
            super(z2);
        }

        @Override // b.g.a.c.g0.t.l
        public l b(Class<?> cls, n<Object> nVar) {
            return new e(this, cls, nVar);
        }

        @Override // b.g.a.c.g0.t.l
        public n<Object> c(Class<?> cls) {
            return null;
        }
    }

    /* compiled from: PropertySerializerMap.java */
    /* loaded from: classes2.dex */
    public static final class c extends l {

        /* renamed from: b  reason: collision with root package name */
        public final f[] f703b;

        public c(l lVar, f[] fVarArr) {
            super(lVar);
            this.f703b = fVarArr;
        }

        @Override // b.g.a.c.g0.t.l
        public l b(Class<?> cls, n<Object> nVar) {
            f[] fVarArr = this.f703b;
            int length = fVarArr.length;
            if (length == 8) {
                return this.a ? new e(this, cls, nVar) : this;
            }
            f[] fVarArr2 = (f[]) Arrays.copyOf(fVarArr, length + 1);
            fVarArr2[length] = new f(cls, nVar);
            return new c(this, fVarArr2);
        }

        /* JADX WARN: Removed duplicated region for block: B:22:0x0036  */
        /* JADX WARN: Removed duplicated region for block: B:26:0x0040  */
        /* JADX WARN: Removed duplicated region for block: B:30:0x004a  */
        /* JADX WARN: Removed duplicated region for block: B:34:0x0054  */
        /* JADX WARN: Removed duplicated region for block: B:36:0x0057 A[ORIG_RETURN, RETURN] */
        @Override // b.g.a.c.g0.t.l
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public b.g.a.c.n<java.lang.Object> c(java.lang.Class<?> r4) {
            /*
                r3 = this;
                b.g.a.c.g0.t.l$f[] r0 = r3.f703b
                r1 = 0
                r1 = r0[r1]
                java.lang.Class<?> r2 = r1.a
                if (r2 != r4) goto Lc
                b.g.a.c.n<java.lang.Object> r4 = r1.f706b
                return r4
            Lc:
                r1 = 1
                r1 = r0[r1]
                java.lang.Class<?> r2 = r1.a
                if (r2 != r4) goto L16
                b.g.a.c.n<java.lang.Object> r4 = r1.f706b
                return r4
            L16:
                r1 = 2
                r1 = r0[r1]
                java.lang.Class<?> r2 = r1.a
                if (r2 != r4) goto L20
                b.g.a.c.n<java.lang.Object> r4 = r1.f706b
                return r4
            L20:
                int r1 = r0.length
                switch(r1) {
                    case 4: goto L4d;
                    case 5: goto L43;
                    case 6: goto L39;
                    case 7: goto L2f;
                    case 8: goto L25;
                    default: goto L24;
                }
            L24:
                goto L57
            L25:
                r1 = 7
                r1 = r0[r1]
                java.lang.Class<?> r2 = r1.a
                if (r2 != r4) goto L2f
                b.g.a.c.n<java.lang.Object> r4 = r1.f706b
                return r4
            L2f:
                r1 = 6
                r1 = r0[r1]
                java.lang.Class<?> r2 = r1.a
                if (r2 != r4) goto L39
                b.g.a.c.n<java.lang.Object> r4 = r1.f706b
                return r4
            L39:
                r1 = 5
                r1 = r0[r1]
                java.lang.Class<?> r2 = r1.a
                if (r2 != r4) goto L43
                b.g.a.c.n<java.lang.Object> r4 = r1.f706b
                return r4
            L43:
                r1 = 4
                r1 = r0[r1]
                java.lang.Class<?> r2 = r1.a
                if (r2 != r4) goto L4d
                b.g.a.c.n<java.lang.Object> r4 = r1.f706b
                return r4
            L4d:
                r1 = 3
                r0 = r0[r1]
                java.lang.Class<?> r1 = r0.a
                if (r1 != r4) goto L57
                b.g.a.c.n<java.lang.Object> r4 = r0.f706b
                return r4
            L57:
                r4 = 0
                return r4
            */
            throw new UnsupportedOperationException("Method not decompiled: b.g.a.c.g0.t.l.c.c(java.lang.Class):b.g.a.c.n");
        }
    }

    /* compiled from: PropertySerializerMap.java */
    /* loaded from: classes2.dex */
    public static final class d {
        public final n<Object> a;

        /* renamed from: b  reason: collision with root package name */
        public final l f704b;

        public d(n<Object> nVar, l lVar) {
            this.a = nVar;
            this.f704b = lVar;
        }
    }

    /* compiled from: PropertySerializerMap.java */
    /* loaded from: classes2.dex */
    public static final class e extends l {

        /* renamed from: b  reason: collision with root package name */
        public final Class<?> f705b;
        public final n<Object> c;

        public e(l lVar, Class<?> cls, n<Object> nVar) {
            super(lVar);
            this.f705b = cls;
            this.c = nVar;
        }

        @Override // b.g.a.c.g0.t.l
        public l b(Class<?> cls, n<Object> nVar) {
            return new a(this, this.f705b, this.c, cls, nVar);
        }

        @Override // b.g.a.c.g0.t.l
        public n<Object> c(Class<?> cls) {
            if (cls == this.f705b) {
                return this.c;
            }
            return null;
        }
    }

    /* compiled from: PropertySerializerMap.java */
    /* loaded from: classes2.dex */
    public static final class f {
        public final Class<?> a;

        /* renamed from: b  reason: collision with root package name */
        public final n<Object> f706b;

        public f(Class<?> cls, n<Object> nVar) {
            this.a = cls;
            this.f706b = nVar;
        }
    }

    public l(boolean z2) {
        this.a = z2;
    }

    public final d a(j jVar, x xVar, b.g.a.c.d dVar) throws JsonMappingException {
        n<Object> m = xVar.m(jVar, dVar);
        return new d(m, b(jVar._class, m));
    }

    public abstract l b(Class<?> cls, n<Object> nVar);

    public abstract n<Object> c(Class<?> cls);

    public l(l lVar) {
        this.a = lVar.a;
    }
}
