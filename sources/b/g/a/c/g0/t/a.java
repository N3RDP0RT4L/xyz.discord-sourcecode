package b.g.a.c.g0.t;

import b.g.a.b.d;
import b.g.a.c.c0.c;
import b.g.a.c.g0.s;
import b.g.a.c.j;
import b.g.a.c.x;
import b.g.a.c.z.l;
/* compiled from: AttributePropertyWriter.java */
/* loaded from: classes2.dex */
public class a extends s {
    private static final long serialVersionUID = 1;
    public final String _attrName;

    public a(String str, b.g.a.c.c0.s sVar, b.g.a.c.i0.a aVar, j jVar) {
        super(sVar, aVar, jVar, null, null, null, sVar.g(), null);
        this._attrName = str;
    }

    @Override // b.g.a.c.g0.s
    public Object j(Object obj, d dVar, x xVar) throws Exception {
        return xVar.w(this._attrName);
    }

    @Override // b.g.a.c.g0.s
    public s k(l<?> lVar, c cVar, b.g.a.c.c0.s sVar, j jVar) {
        throw new IllegalStateException("Should not be called on this type");
    }
}
