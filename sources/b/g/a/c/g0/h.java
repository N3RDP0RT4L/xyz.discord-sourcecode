package b.g.a.c.g0;

import b.g.a.c.e0.g;
import b.g.a.c.g0.u.q0;
import b.g.a.c.j;
/* compiled from: ContainerSerializer.java */
/* loaded from: classes2.dex */
public abstract class h<T> extends q0<T> {
    public h(Class<T> cls) {
        super(cls);
    }

    public abstract h<?> p(g gVar);

    public abstract boolean q(T t);

    public h(j jVar) {
        super(jVar);
    }

    public h(Class<?> cls, boolean z2) {
        super(cls, z2);
    }

    public h(h<?> hVar) {
        super(hVar._handledType, false);
    }
}
