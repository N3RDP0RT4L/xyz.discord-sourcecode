package b.g.a.c.g0;

import b.d.b.a.a;
import b.g.a.c.b;
import b.g.a.c.c;
import b.g.a.c.c0.q;
import b.g.a.c.g0.u.j0;
import b.g.a.c.i0.d;
import b.g.a.c.i0.e;
import b.g.a.c.j;
import b.g.a.c.n;
import b.g.a.c.v;
import b.g.a.c.x;
import b.g.a.c.z.o;
import com.fasterxml.jackson.databind.JsonMappingException;
import java.io.Serializable;
import java.util.Objects;
/* compiled from: BeanSerializerFactory.java */
/* loaded from: classes2.dex */
public class f extends b implements Serializable {
    public static final f l = new f(null);
    private static final long serialVersionUID = 1;

    public f(o oVar) {
        super(null);
    }

    @Override // b.g.a.c.g0.q
    public n<Object> b(x xVar, j jVar) throws JsonMappingException {
        j e02;
        Object H;
        v vVar = xVar._config;
        c u = vVar.u(jVar);
        n<?> f = f(xVar, ((q) u).f);
        if (f != null) {
            return f;
        }
        b e = vVar.e();
        boolean z2 = false;
        e eVar = null;
        if (e == null) {
            e02 = jVar;
        } else {
            try {
                e02 = e.e0(vVar, ((q) u).f, jVar);
            } catch (JsonMappingException e2) {
                xVar.F(u, e2.getMessage(), new Object[0]);
                throw null;
            }
        }
        if (e02 != jVar) {
            if (!e02.t(jVar._class)) {
                u = vVar.u(e02);
            }
            z2 = true;
        }
        q qVar = (q) u;
        b bVar = qVar.e;
        if (!(bVar == null || (H = bVar.H(qVar.f)) == null)) {
            if (H instanceof e) {
                eVar = (e) H;
            } else if (H instanceof Class) {
                Class cls = (Class) H;
                if (cls != e.a.class && !d.p(cls)) {
                    if (e.class.isAssignableFrom(cls)) {
                        Objects.requireNonNull(qVar.d._base);
                        eVar = (e) d.g(cls, qVar.d.b());
                    } else {
                        throw new IllegalStateException(a.n(cls, a.R("AnnotationIntrospector returned Class "), "; expected Class<Converter>"));
                    }
                }
            } else {
                StringBuilder R = a.R("AnnotationIntrospector returned Converter definition of type ");
                R.append(H.getClass().getName());
                R.append("; expected type Converter or Class<Converter> instead");
                throw new IllegalStateException(R.toString());
            }
        }
        if (eVar == null) {
            return i(xVar, e02, u, z2);
        }
        j a = eVar.a(xVar.d());
        if (!a.t(e02._class)) {
            u = vVar.u(a);
            f = f(xVar, ((q) u).f);
        }
        if (f == null && !a.y()) {
            f = i(xVar, a, u, true);
        }
        return new j0(eVar, a, f);
    }

    /* JADX WARN: Removed duplicated region for block: B:118:0x0259  */
    /* JADX WARN: Removed duplicated region for block: B:132:0x02a1  */
    /* JADX WARN: Removed duplicated region for block: B:135:0x02b0  */
    /* JADX WARN: Removed duplicated region for block: B:136:0x02b6  */
    /* JADX WARN: Removed duplicated region for block: B:97:0x0218  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.g.a.c.g0.c h(b.g.a.c.x r17, b.g.a.c.c0.s r18, b.g.a.c.g0.l r19, boolean r20, b.g.a.c.c0.i r21) throws com.fasterxml.jackson.databind.JsonMappingException {
        /*
            Method dump skipped, instructions count: 723
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.c.g0.f.h(b.g.a.c.x, b.g.a.c.c0.s, b.g.a.c.g0.l, boolean, b.g.a.c.c0.i):b.g.a.c.g0.c");
    }

    /* JADX WARN: Code restructure failed: missing block: B:254:0x04d7, code lost:
        r2 = 3;
     */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:160:0x02fc  */
    /* JADX WARN: Removed duplicated region for block: B:395:0x073e  */
    /* JADX WARN: Removed duplicated region for block: B:397:0x0745  */
    /* JADX WARN: Removed duplicated region for block: B:411:0x0792  */
    /* JADX WARN: Removed duplicated region for block: B:412:0x0795  */
    /* JADX WARN: Removed duplicated region for block: B:583:0x0b1b  */
    /* JADX WARN: Removed duplicated region for block: B:589:0x0b6e  */
    /* JADX WARN: Removed duplicated region for block: B:606:0x0bad  */
    /* JADX WARN: Removed duplicated region for block: B:649:0x0c71  */
    /* JADX WARN: Removed duplicated region for block: B:653:0x0c9d  */
    /* JADX WARN: Type inference failed for: r26v0, types: [b.g.a.c.g0.b, b.g.a.c.g0.f] */
    /* JADX WARN: Type inference failed for: r5v43, types: [b.g.a.c.n] */
    /* JADX WARN: Type inference failed for: r5v44 */
    /* JADX WARN: Type inference failed for: r5v68, types: [b.g.a.c.g0.t.i] */
    /* JADX WARN: Type inference failed for: r5v69 */
    /* JADX WARN: Type inference failed for: r5v70 */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.g.a.c.n<?> i(b.g.a.c.x r27, b.g.a.c.j r28, b.g.a.c.c r29, boolean r30) throws com.fasterxml.jackson.databind.JsonMappingException {
        /*
            Method dump skipped, instructions count: 3299
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.c.g0.f.i(b.g.a.c.x, b.g.a.c.j, b.g.a.c.c, boolean):b.g.a.c.n");
    }

    public Iterable<r> j() {
        return new b.g.a.c.i0.b(this._factoryConfig._additionalSerializers);
    }
}
