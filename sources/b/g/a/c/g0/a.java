package b.g.a.c.g0;

import b.g.a.c.c0.i;
import b.g.a.c.d;
import b.g.a.c.g0.u.t;
import b.g.a.c.n;
import b.g.a.c.x;
import java.util.Map;
/* compiled from: AnyGetterWriter.java */
/* loaded from: classes2.dex */
public class a {
    public final d a;

    /* renamed from: b  reason: collision with root package name */
    public final i f696b;
    public n<Object> c;
    public t d;

    public a(d dVar, i iVar, n<?> nVar) {
        this.f696b = iVar;
        this.a = dVar;
        this.c = nVar;
        if (nVar instanceof t) {
            this.d = (t) nVar;
        }
    }

    public void a(Object obj, b.g.a.b.d dVar, x xVar) throws Exception {
        Object j = this.f696b.j(obj);
        if (j != null) {
            if (j instanceof Map) {
                t tVar = this.d;
                if (tVar != null) {
                    tVar.u((Map) j, dVar, xVar);
                } else {
                    this.c.f(j, dVar, xVar);
                }
            } else {
                xVar.f(this.a.getType(), String.format("Value returned by 'any-getter' %s() not java.util.Map but %s", this.f696b.c(), j.getClass().getName()));
                throw null;
            }
        }
    }
}
