package b.g.a.c.g0;

import b.g.a.a.p;
import b.g.a.c.c0.c;
import b.g.a.c.c0.d;
import b.g.a.c.c0.q;
import b.g.a.c.e0.f;
import b.g.a.c.e0.g;
import b.g.a.c.g0.u.a0;
import b.g.a.c.g0.u.b0;
import b.g.a.c.g0.u.c0;
import b.g.a.c.g0.u.e;
import b.g.a.c.g0.u.g0;
import b.g.a.c.g0.u.h;
import b.g.a.c.g0.u.i;
import b.g.a.c.g0.u.j0;
import b.g.a.c.g0.u.k;
import b.g.a.c.g0.u.k0;
import b.g.a.c.g0.u.l0;
import b.g.a.c.g0.u.m0;
import b.g.a.c.g0.u.r0;
import b.g.a.c.g0.u.t0;
import b.g.a.c.g0.u.u;
import b.g.a.c.g0.u.v;
import b.g.a.c.g0.u.v0;
import b.g.a.c.g0.u.w0;
import b.g.a.c.g0.u.x;
import b.g.a.c.g0.u.y;
import b.g.a.c.g0.u.z;
import b.g.a.c.i0.s;
import b.g.a.c.j;
import b.g.a.c.m;
import b.g.a.c.n;
import b.g.a.c.y.e;
import b.g.a.c.z.o;
import com.fasterxml.jackson.databind.JsonMappingException;
import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;
/* compiled from: BasicSerializerFactory.java */
/* loaded from: classes2.dex */
public abstract class b extends q implements Serializable {
    public static final HashMap<String, n<?>> j;
    public static final HashMap<String, Class<? extends n<?>>> k;
    public final o _factoryConfig;

    static {
        HashMap<String, Class<? extends n<?>>> hashMap = new HashMap<>();
        HashMap<String, n<?>> hashMap2 = new HashMap<>();
        hashMap2.put(String.class.getName(), new r0());
        t0 t0Var = t0.k;
        hashMap2.put(StringBuffer.class.getName(), t0Var);
        hashMap2.put(StringBuilder.class.getName(), t0Var);
        hashMap2.put(Character.class.getName(), t0Var);
        hashMap2.put(Character.TYPE.getName(), t0Var);
        hashMap2.put(Integer.class.getName(), new a0(Integer.class));
        Class cls = Integer.TYPE;
        hashMap2.put(cls.getName(), new a0(cls));
        hashMap2.put(Long.class.getName(), new b0(Long.class));
        Class cls2 = Long.TYPE;
        hashMap2.put(cls2.getName(), new b0(cls2));
        String name = Byte.class.getName();
        z zVar = z.k;
        hashMap2.put(name, zVar);
        hashMap2.put(Byte.TYPE.getName(), zVar);
        String name2 = Short.class.getName();
        c0 c0Var = c0.k;
        hashMap2.put(name2, c0Var);
        hashMap2.put(Short.TYPE.getName(), c0Var);
        hashMap2.put(Double.class.getName(), new x(Double.class));
        hashMap2.put(Double.TYPE.getName(), new x(Double.TYPE));
        String name3 = Float.class.getName();
        y yVar = y.k;
        hashMap2.put(name3, yVar);
        hashMap2.put(Float.TYPE.getName(), yVar);
        hashMap2.put(Boolean.TYPE.getName(), new e(true));
        hashMap2.put(Boolean.class.getName(), new e(false));
        hashMap2.put(BigInteger.class.getName(), new v(BigInteger.class));
        hashMap2.put(BigDecimal.class.getName(), new v(BigDecimal.class));
        hashMap2.put(Calendar.class.getName(), h.k);
        hashMap2.put(Date.class.getName(), k.k);
        HashMap hashMap3 = new HashMap();
        hashMap3.put(URL.class, new t0(URL.class));
        hashMap3.put(URI.class, new t0(URI.class));
        hashMap3.put(Currency.class, new t0(Currency.class));
        hashMap3.put(UUID.class, new w0());
        hashMap3.put(Pattern.class, new t0(Pattern.class));
        hashMap3.put(Locale.class, new t0(Locale.class));
        hashMap3.put(AtomicBoolean.class, k0.class);
        hashMap3.put(AtomicInteger.class, l0.class);
        hashMap3.put(AtomicLong.class, m0.class);
        hashMap3.put(File.class, b.g.a.c.g0.u.o.class);
        hashMap3.put(Class.class, i.class);
        u uVar = u.k;
        hashMap3.put(Void.class, uVar);
        hashMap3.put(Void.TYPE, uVar);
        for (Map.Entry entry : hashMap3.entrySet()) {
            Object value = entry.getValue();
            if (value instanceof n) {
                hashMap2.put(((Class) entry.getKey()).getName(), (n) value);
            } else {
                hashMap.put(((Class) entry.getKey()).getName(), (Class) value);
            }
        }
        hashMap.put(s.class.getName(), v0.class);
        j = hashMap2;
        k = hashMap;
    }

    public b(o oVar) {
        this._factoryConfig = oVar == null ? new o() : oVar;
    }

    /* JADX WARN: Removed duplicated region for block: B:100:0x0163  */
    /* JADX WARN: Removed duplicated region for block: B:104:0x0184  */
    /* JADX WARN: Removed duplicated region for block: B:98:0x015d  */
    @Override // b.g.a.c.g0.q
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.g.a.c.n<java.lang.Object> a(b.g.a.c.x r13, b.g.a.c.j r14, b.g.a.c.n<java.lang.Object> r15) throws com.fasterxml.jackson.databind.JsonMappingException {
        /*
            Method dump skipped, instructions count: 460
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.g.a.c.g0.b.a(b.g.a.c.x, b.g.a.c.j, b.g.a.c.n):b.g.a.c.n");
    }

    @Override // b.g.a.c.g0.q
    public g c(b.g.a.c.v vVar, j jVar) {
        ArrayList arrayList;
        c cVar = ((q) vVar.o(jVar._class)).f;
        f<?> P = vVar.e().P(vVar, cVar, jVar);
        if (P == null) {
            P = vVar._base._typeResolverBuilder;
            arrayList = null;
        } else {
            b.g.a.c.e0.h.i iVar = (b.g.a.c.e0.h.i) vVar._subtypeResolver;
            Objects.requireNonNull(iVar);
            b.g.a.c.b e = vVar.e();
            HashMap<b.g.a.c.e0.b, b.g.a.c.e0.b> hashMap = new HashMap<>();
            LinkedHashSet<b.g.a.c.e0.b> linkedHashSet = iVar._registeredSubtypes;
            if (linkedHashSet != null) {
                Class<?> cls = cVar.l;
                Iterator<b.g.a.c.e0.b> it = linkedHashSet.iterator();
                while (it.hasNext()) {
                    b.g.a.c.e0.b next = it.next();
                    if (cls.isAssignableFrom(next._class)) {
                        iVar.b(d.h(vVar, next._class), next, vVar, e, hashMap);
                    }
                }
            }
            iVar.b(cVar, new b.g.a.c.e0.b(cVar.l, null), vVar, e, hashMap);
            arrayList = new ArrayList(hashMap.values());
        }
        if (P == null) {
            return null;
        }
        return P.e(vVar, jVar, arrayList);
    }

    public p.b d(b.g.a.c.x xVar, b.g.a.c.c cVar, j jVar, Class<?> cls) throws JsonMappingException {
        b.g.a.c.v vVar = xVar._config;
        p.b c = cVar.c(vVar._configOverrides._defaultInclusion);
        vVar._configOverrides.a(cls);
        vVar.k(jVar._class, null);
        return c;
    }

    public final n<?> e(b.g.a.c.x xVar, j jVar, b.g.a.c.c cVar) throws JsonMappingException {
        if (m.class.isAssignableFrom(jVar._class)) {
            return g0.k;
        }
        b.g.a.c.c0.i b2 = cVar.b();
        if (b2 == null) {
            return null;
        }
        if (xVar._config.b()) {
            b.g.a.c.i0.d.d(b2.i(), xVar.C(b.g.a.c.p.OVERRIDE_PUBLIC_ACCESS_MODIFIERS));
        }
        j e = b2.e();
        n<Object> f = f(xVar, b2);
        if (f == null) {
            f = (n) e._valueHandler;
        }
        g gVar = (g) e._typeHandler;
        if (gVar == null) {
            gVar = c(xVar._config, e);
        }
        return new b.g.a.c.g0.u.s(b2, gVar, f);
    }

    public n<Object> f(b.g.a.c.x xVar, b.g.a.c.c0.b bVar) throws JsonMappingException {
        Object L = xVar.v().L(bVar);
        b.g.a.c.i0.e<Object, Object> eVar = null;
        if (L == null) {
            return null;
        }
        n<Object> H = xVar.H(bVar, L);
        Object H2 = xVar.v().H(bVar);
        if (H2 != null) {
            eVar = xVar.c(bVar, H2);
        }
        return eVar == null ? H : new j0(eVar, eVar.a(xVar.d()), H);
    }

    public boolean g(b.g.a.c.v vVar, b.g.a.c.c cVar, g gVar) {
        e.b K = vVar.e().K(((q) cVar).f);
        if (K == null || K == e.b.DEFAULT_TYPING) {
            return vVar.q(b.g.a.c.p.USE_STATIC_TYPING);
        }
        return K == e.b.STATIC;
    }
}
