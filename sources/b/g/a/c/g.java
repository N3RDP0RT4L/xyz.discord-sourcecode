package b.g.a.c;

import b.g.a.b.l;
import b.g.a.b.t.h;
import b.g.a.c.a0.e;
import b.g.a.c.a0.f;
import b.g.a.c.h0.n;
import b.g.a.c.i0.i;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidDefinitionException;
import java.io.Serializable;
import java.util.Objects;
/* compiled from: DeserializationContext.java */
/* loaded from: classes2.dex */
public abstract class g extends e implements Serializable {
    private static final long serialVersionUID = 1;
    public final e _cache;
    public final f _config;
    public i<j> _currentType;
    public final f _factory;
    public final int _featureFlags;
    public final i _injectableValues;
    public final h<l> _readCapabilities;
    public final Class<?> _view;

    public g(f fVar, e eVar) {
        Objects.requireNonNull(fVar, "Cannot pass null DeserializerFactory");
        this._factory = fVar;
        this._cache = eVar == null ? new e() : eVar;
        this._featureFlags = 0;
        this._readCapabilities = null;
        this._config = null;
        this._view = null;
    }

    @Override // b.g.a.c.e
    public final n d() {
        return this._config._base._typeFactory;
    }

    @Override // b.g.a.c.e
    public <T> T f(j jVar, String str) throws JsonMappingException {
        throw new InvalidDefinitionException((b.g.a.b.f) null, str, jVar);
    }
}
