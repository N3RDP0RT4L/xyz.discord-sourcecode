package b.g.a.c;

import b.g.a.c.c0.g;
import b.g.a.c.c0.j;
import b.g.a.c.c0.m;
import b.g.a.c.z.l;
import java.io.Serializable;
/* compiled from: PropertyNamingStrategy.java */
/* loaded from: classes2.dex */
public class u implements Serializable {
    @Deprecated
    public static final u j = new c();
    @Deprecated
    public static final u k = new b();
    private static final long serialVersionUID = 2;

    /* compiled from: PropertyNamingStrategy.java */
    @Deprecated
    /* loaded from: classes2.dex */
    public static abstract class a extends u {
        @Override // b.g.a.c.u
        public String a(l<?> lVar, m mVar, String str) {
            return e(str);
        }

        @Override // b.g.a.c.u
        public String b(l<?> lVar, g gVar, String str) {
            return e(str);
        }

        @Override // b.g.a.c.u
        public String c(l<?> lVar, j jVar, String str) {
            return e(str);
        }

        @Override // b.g.a.c.u
        public String d(l<?> lVar, j jVar, String str) {
            return e(str);
        }

        public abstract String e(String str);
    }

    /* compiled from: PropertyNamingStrategy.java */
    @Deprecated
    /* loaded from: classes2.dex */
    public static class b extends a {
        @Override // b.g.a.c.u.a
        public String e(String str) {
            if (str == null) {
                return str;
            }
            int length = str.length();
            StringBuilder sb = new StringBuilder(length * 2);
            int i = 0;
            boolean z2 = false;
            for (int i2 = 0; i2 < length; i2++) {
                char charAt = str.charAt(i2);
                if (i2 > 0 || charAt != '_') {
                    if (Character.isUpperCase(charAt)) {
                        if (!z2 && i > 0 && sb.charAt(i - 1) != '_') {
                            sb.append('_');
                            i++;
                        }
                        charAt = Character.toLowerCase(charAt);
                        z2 = true;
                    } else {
                        z2 = false;
                    }
                    sb.append(charAt);
                    i++;
                }
            }
            return i > 0 ? sb.toString() : str;
        }
    }

    /* compiled from: PropertyNamingStrategy.java */
    @Deprecated
    /* loaded from: classes2.dex */
    public static class c extends a {
        @Override // b.g.a.c.u.a
        public String e(String str) {
            char charAt;
            char upperCase;
            if (str == null || str.isEmpty() || charAt == (upperCase = Character.toUpperCase((charAt = str.charAt(0))))) {
                return str;
            }
            StringBuilder sb = new StringBuilder(str);
            sb.setCharAt(0, upperCase);
            return sb.toString();
        }
    }

    public String a(l<?> lVar, m mVar, String str) {
        return str;
    }

    public String b(l<?> lVar, g gVar, String str) {
        return str;
    }

    public String c(l<?> lVar, j jVar, String str) {
        return str;
    }

    public String d(l<?> lVar, j jVar, String str) {
        return str;
    }
}
