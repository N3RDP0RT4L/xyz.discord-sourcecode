package b.g.a.c;

import b.g.a.b.c;
import b.g.a.b.i;
/* compiled from: MappingJsonFactory.java */
/* loaded from: classes2.dex */
public class q extends c {
    private static final long serialVersionUID = -1;

    public q() {
        super(null);
        this._objectCodec = new r(this, null, null);
    }

    @Override // b.g.a.b.c
    public i b() {
        return (r) this._objectCodec;
    }

    public q(r rVar) {
        super(rVar);
    }
}
