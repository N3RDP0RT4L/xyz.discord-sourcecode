package b.c.a.z;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import androidx.browser.trusted.sharing.ShareTarget;
import b.c.a.d;
import b.c.a.e;
import b.c.a.p;
import b.d.b.a.a;
import com.discord.restapi.RestAPIBuilder;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.ZipInputStream;
/* compiled from: NetworkFetcher.java */
/* loaded from: classes.dex */
public class c {
    public final Context a;

    /* renamed from: b  reason: collision with root package name */
    public final String f443b;
    @Nullable
    public final b c;

    public c(Context context, String str, @Nullable String str2) {
        Context applicationContext = context.getApplicationContext();
        this.a = applicationContext;
        this.f443b = str;
        if (str2 == null) {
            this.c = null;
        } else {
            this.c = new b(applicationContext);
        }
    }

    @WorkerThread
    public final p<d> a() throws IOException {
        StringBuilder R = a.R("Fetching ");
        R.append(this.f443b);
        b.c.a.b0.c.a(R.toString());
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(this.f443b).openConnection();
        httpURLConnection.setRequestMethod(ShareTarget.METHOD_GET);
        try {
            httpURLConnection.connect();
            if (httpURLConnection.getErrorStream() == null && httpURLConnection.getResponseCode() == 200) {
                p<d> c = c(httpURLConnection);
                StringBuilder sb = new StringBuilder();
                sb.append("Completed fetch from network. Success: ");
                sb.append(c.a != null);
                b.c.a.b0.c.a(sb.toString());
                return c;
            }
            String b2 = b(httpURLConnection);
            return new p<>(new IllegalArgumentException("Unable to fetch " + this.f443b + ". Failed with " + httpURLConnection.getResponseCode() + "\n" + b2));
        } catch (Exception e) {
            return new p<>(e);
        } finally {
            httpURLConnection.disconnect();
        }
    }

    public final String b(HttpURLConnection httpURLConnection) throws IOException {
        httpURLConnection.getResponseCode();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getErrorStream()));
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                try {
                    String readLine = bufferedReader.readLine();
                    if (readLine != null) {
                        sb.append(readLine);
                        sb.append('\n');
                    } else {
                        try {
                            break;
                        } catch (Exception unused) {
                        }
                    }
                } catch (Exception e) {
                    throw e;
                }
            } catch (Throwable th) {
                try {
                    bufferedReader.close();
                } catch (Exception unused2) {
                }
                throw th;
            }
        }
        bufferedReader.close();
        return sb.toString();
    }

    @Nullable
    public final p<d> c(HttpURLConnection httpURLConnection) throws IOException {
        p<d> pVar;
        a aVar;
        String contentType = httpURLConnection.getContentType();
        if (contentType == null) {
            contentType = RestAPIBuilder.CONTENT_TYPE_JSON;
        }
        if (contentType.contains("application/zip")) {
            b.c.a.b0.c.a("Handling zip response.");
            aVar = a.ZIP;
            b bVar = this.c;
            if (bVar == null) {
                pVar = e.d(new ZipInputStream(httpURLConnection.getInputStream()), null);
            } else {
                pVar = e.d(new ZipInputStream(new FileInputStream(bVar.c(this.f443b, httpURLConnection.getInputStream(), aVar))), this.f443b);
            }
        } else {
            b.c.a.b0.c.a("Received json response.");
            aVar = a.JSON;
            b bVar2 = this.c;
            if (bVar2 == null) {
                pVar = e.b(httpURLConnection.getInputStream(), null);
            } else {
                pVar = e.b(new FileInputStream(new File(bVar2.c(this.f443b, httpURLConnection.getInputStream(), aVar).getAbsolutePath())), this.f443b);
            }
        }
        b bVar3 = this.c;
        if (!(bVar3 == null || pVar.a == null)) {
            File file = new File(bVar3.b(), b.a(this.f443b, aVar, true));
            File file2 = new File(file.getAbsolutePath().replace(".temp", ""));
            boolean renameTo = file.renameTo(file2);
            b.c.a.b0.c.a("Copying temp file to real file (" + file2 + ")");
            if (!renameTo) {
                StringBuilder R = a.R("Unable to rename cache file ");
                R.append(file.getAbsolutePath());
                R.append(" to ");
                R.append(file2.getAbsolutePath());
                R.append(".");
                b.c.a.b0.c.b(R.toString());
            }
        }
        return pVar;
    }
}
