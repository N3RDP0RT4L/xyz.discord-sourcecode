package b.c.a;

import androidx.annotation.Nullable;
import java.util.Arrays;
/* compiled from: LottieResult.java */
/* loaded from: classes.dex */
public final class p<V> {
    @Nullable
    public final V a;
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public final Throwable f369b;

    public p(V v) {
        this.a = v;
        this.f369b = null;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof p)) {
            return false;
        }
        p pVar = (p) obj;
        V v = this.a;
        if (v != null && v.equals(pVar.a)) {
            return true;
        }
        Throwable th = this.f369b;
        if (th == null || pVar.f369b == null) {
            return false;
        }
        return th.toString().equals(this.f369b.toString());
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.a, this.f369b});
    }

    public p(Throwable th) {
        this.f369b = th;
        this.a = null;
    }
}
