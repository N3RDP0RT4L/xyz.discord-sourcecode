package b.c.a;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.widget.ImageView;
import androidx.annotation.FloatRange;
import androidx.annotation.IntRange;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.c.a.a0.i0.c;
import b.c.a.a0.s;
import b.c.a.y.m.e;
import com.google.android.material.shadow.ShadowDrawableWrapper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
/* compiled from: LottieDrawable.java */
/* loaded from: classes.dex */
public class j extends Drawable implements Drawable.Callback, Animatable {
    public boolean A;
    public b.c.a.d k;
    public final b.c.a.b0.d l;
    public final ValueAnimator.AnimatorUpdateListener q;
    @Nullable
    public ImageView.ScaleType r;
    @Nullable

    /* renamed from: s  reason: collision with root package name */
    public b.c.a.x.b f348s;
    @Nullable
    public String t;
    @Nullable
    public b.c.a.b u;
    @Nullable
    public b.c.a.x.a v;
    public boolean w;
    @Nullable

    /* renamed from: x  reason: collision with root package name */
    public b.c.a.y.m.c f349x;

    /* renamed from: z  reason: collision with root package name */
    public boolean f351z;
    public final Matrix j = new Matrix();
    public float m = 1.0f;
    public boolean n = true;
    public boolean o = false;
    public final ArrayList<o> p = new ArrayList<>();

    /* renamed from: y  reason: collision with root package name */
    public int f350y = 255;
    public boolean B = true;
    public boolean C = false;

    /* compiled from: LottieDrawable.java */
    /* loaded from: classes.dex */
    public class a implements o {
        public final /* synthetic */ String a;

        public a(String str) {
            this.a = str;
        }

        @Override // b.c.a.j.o
        public void a(b.c.a.d dVar) {
            j.this.q(this.a);
        }
    }

    /* compiled from: LottieDrawable.java */
    /* loaded from: classes.dex */
    public class b implements o {
        public final /* synthetic */ int a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ int f353b;

        public b(int i, int i2) {
            this.a = i;
            this.f353b = i2;
        }

        @Override // b.c.a.j.o
        public void a(b.c.a.d dVar) {
            j.this.p(this.a, this.f353b);
        }
    }

    /* compiled from: LottieDrawable.java */
    /* loaded from: classes.dex */
    public class c implements o {
        public final /* synthetic */ int a;

        public c(int i) {
            this.a = i;
        }

        @Override // b.c.a.j.o
        public void a(b.c.a.d dVar) {
            j.this.l(this.a);
        }
    }

    /* compiled from: LottieDrawable.java */
    /* loaded from: classes.dex */
    public class d implements o {
        public final /* synthetic */ float a;

        public d(float f) {
            this.a = f;
        }

        @Override // b.c.a.j.o
        public void a(b.c.a.d dVar) {
            j.this.u(this.a);
        }
    }

    /* compiled from: LottieDrawable.java */
    /* loaded from: classes.dex */
    public class e implements o {
        public final /* synthetic */ b.c.a.y.f a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ Object f356b;
        public final /* synthetic */ b.c.a.c0.c c;

        public e(b.c.a.y.f fVar, Object obj, b.c.a.c0.c cVar) {
            this.a = fVar;
            this.f356b = obj;
            this.c = cVar;
        }

        @Override // b.c.a.j.o
        public void a(b.c.a.d dVar) {
            j.this.a(this.a, this.f356b, this.c);
        }
    }

    /* compiled from: LottieDrawable.java */
    /* loaded from: classes.dex */
    public class f implements ValueAnimator.AnimatorUpdateListener {
        public f() {
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            j jVar = j.this;
            b.c.a.y.m.c cVar = jVar.f349x;
            if (cVar != null) {
                cVar.o(jVar.l.h());
            }
        }
    }

    /* compiled from: LottieDrawable.java */
    /* loaded from: classes.dex */
    public class g implements o {
        public g() {
        }

        @Override // b.c.a.j.o
        public void a(b.c.a.d dVar) {
            j.this.j();
        }
    }

    /* compiled from: LottieDrawable.java */
    /* loaded from: classes.dex */
    public class h implements o {
        public h() {
        }

        @Override // b.c.a.j.o
        public void a(b.c.a.d dVar) {
            j.this.k();
        }
    }

    /* compiled from: LottieDrawable.java */
    /* loaded from: classes.dex */
    public class i implements o {
        public final /* synthetic */ int a;

        public i(int i) {
            this.a = i;
        }

        @Override // b.c.a.j.o
        public void a(b.c.a.d dVar) {
            j.this.r(this.a);
        }
    }

    /* compiled from: LottieDrawable.java */
    /* renamed from: b.c.a.j$j  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public class C0060j implements o {
        public final /* synthetic */ float a;

        public C0060j(float f) {
            this.a = f;
        }

        @Override // b.c.a.j.o
        public void a(b.c.a.d dVar) {
            j.this.t(this.a);
        }
    }

    /* compiled from: LottieDrawable.java */
    /* loaded from: classes.dex */
    public class k implements o {
        public final /* synthetic */ int a;

        public k(int i) {
            this.a = i;
        }

        @Override // b.c.a.j.o
        public void a(b.c.a.d dVar) {
            j.this.m(this.a);
        }
    }

    /* compiled from: LottieDrawable.java */
    /* loaded from: classes.dex */
    public class l implements o {
        public final /* synthetic */ float a;

        public l(float f) {
            this.a = f;
        }

        @Override // b.c.a.j.o
        public void a(b.c.a.d dVar) {
            j.this.o(this.a);
        }
    }

    /* compiled from: LottieDrawable.java */
    /* loaded from: classes.dex */
    public class m implements o {
        public final /* synthetic */ String a;

        public m(String str) {
            this.a = str;
        }

        @Override // b.c.a.j.o
        public void a(b.c.a.d dVar) {
            j.this.s(this.a);
        }
    }

    /* compiled from: LottieDrawable.java */
    /* loaded from: classes.dex */
    public class n implements o {
        public final /* synthetic */ String a;

        public n(String str) {
            this.a = str;
        }

        @Override // b.c.a.j.o
        public void a(b.c.a.d dVar) {
            j.this.n(this.a);
        }
    }

    /* compiled from: LottieDrawable.java */
    /* loaded from: classes.dex */
    public interface o {
        void a(b.c.a.d dVar);
    }

    public j() {
        b.c.a.b0.d dVar = new b.c.a.b0.d();
        this.l = dVar;
        new HashSet();
        f fVar = new f();
        this.q = fVar;
        dVar.j.add(fVar);
    }

    public <T> void a(b.c.a.y.f fVar, T t, b.c.a.c0.c<T> cVar) {
        List list;
        b.c.a.y.m.c cVar2 = this.f349x;
        if (cVar2 == null) {
            this.p.add(new e(fVar, t, cVar));
            return;
        }
        boolean z2 = true;
        if (fVar == b.c.a.y.f.a) {
            cVar2.g(t, cVar);
        } else {
            b.c.a.y.g gVar = fVar.c;
            if (gVar != null) {
                gVar.g(t, cVar);
            } else {
                if (cVar2 == null) {
                    b.c.a.b0.c.b("Cannot resolve KeyPath. Composition is not set yet.");
                    list = Collections.emptyList();
                } else {
                    ArrayList arrayList = new ArrayList();
                    this.f349x.c(fVar, 0, arrayList, new b.c.a.y.f(new String[0]));
                    list = arrayList;
                }
                for (int i2 = 0; i2 < list.size(); i2++) {
                    ((b.c.a.y.f) list.get(i2)).c.g(t, cVar);
                }
                z2 = true ^ list.isEmpty();
            }
        }
        if (z2) {
            invalidateSelf();
            if (t == b.c.a.o.A) {
                u(g());
            }
        }
    }

    public final void b() {
        b.c.a.d dVar = this.k;
        c.a aVar = s.a;
        Rect rect = dVar.j;
        b.c.a.y.m.e eVar = new b.c.a.y.m.e(Collections.emptyList(), dVar, "__container", -1L, e.a.PRE_COMP, -1L, null, Collections.emptyList(), new b.c.a.y.k.l(null, null, null, null, null, null, null, null, null), 0, 0, 0, 0.0f, 0.0f, rect.width(), rect.height(), null, null, Collections.emptyList(), 1, null, false);
        b.c.a.d dVar2 = this.k;
        this.f349x = new b.c.a.y.m.c(this, eVar, dVar2.i, dVar2);
    }

    public void c() {
        b.c.a.b0.d dVar = this.l;
        if (dVar.t) {
            dVar.cancel();
        }
        this.k = null;
        this.f349x = null;
        this.f348s = null;
        b.c.a.b0.d dVar2 = this.l;
        dVar2.f340s = null;
        dVar2.q = -2.14748365E9f;
        dVar2.r = 2.14748365E9f;
        invalidateSelf();
    }

    public final void d(@NonNull Canvas canvas) {
        float f2;
        float f3;
        int i2 = -1;
        if (ImageView.ScaleType.FIT_XY == this.r) {
            if (this.f349x != null) {
                Rect bounds = getBounds();
                float width = bounds.width() / this.k.j.width();
                float height = bounds.height() / this.k.j.height();
                if (this.B) {
                    float min = Math.min(width, height);
                    if (min < 1.0f) {
                        f3 = 1.0f / min;
                        width /= f3;
                        height /= f3;
                    } else {
                        f3 = 1.0f;
                    }
                    if (f3 > 1.0f) {
                        i2 = canvas.save();
                        float width2 = bounds.width() / 2.0f;
                        float height2 = bounds.height() / 2.0f;
                        float f4 = width2 * min;
                        float f5 = min * height2;
                        canvas.translate(width2 - f4, height2 - f5);
                        canvas.scale(f3, f3, f4, f5);
                    }
                }
                this.j.reset();
                this.j.preScale(width, height);
                this.f349x.f(canvas, this.j, this.f350y);
                if (i2 > 0) {
                    canvas.restoreToCount(i2);
                }
            }
        } else if (this.f349x != null) {
            float f6 = this.m;
            float min2 = Math.min(canvas.getWidth() / this.k.j.width(), canvas.getHeight() / this.k.j.height());
            if (f6 > min2) {
                f2 = this.m / min2;
            } else {
                min2 = f6;
                f2 = 1.0f;
            }
            if (f2 > 1.0f) {
                i2 = canvas.save();
                float width3 = this.k.j.width() / 2.0f;
                float height3 = this.k.j.height() / 2.0f;
                float f7 = width3 * min2;
                float f8 = height3 * min2;
                float f9 = this.m;
                canvas.translate((width3 * f9) - f7, (f9 * height3) - f8);
                canvas.scale(f2, f2, f7, f8);
            }
            this.j.reset();
            this.j.preScale(min2, min2);
            this.f349x.f(canvas, this.j, this.f350y);
            if (i2 > 0) {
                canvas.restoreToCount(i2);
            }
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(@NonNull Canvas canvas) {
        this.C = false;
        if (this.o) {
            try {
                d(canvas);
            } catch (Throwable unused) {
                Objects.requireNonNull((b.c.a.b0.b) b.c.a.b0.c.a);
            }
        } else {
            d(canvas);
        }
        b.c.a.c.a("Drawable#draw");
    }

    public float e() {
        return this.l.i();
    }

    public float f() {
        return this.l.j();
    }

    @FloatRange(from = ShadowDrawableWrapper.COS_45, to = 1.0d)
    public float g() {
        return this.l.h();
    }

    @Override // android.graphics.drawable.Drawable
    public int getAlpha() {
        return this.f350y;
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        b.c.a.d dVar = this.k;
        if (dVar == null) {
            return -1;
        }
        return (int) (dVar.j.height() * this.m);
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        b.c.a.d dVar = this.k;
        if (dVar == null) {
            return -1;
        }
        return (int) (dVar.j.width() * this.m);
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    public int h() {
        return this.l.getRepeatCount();
    }

    public boolean i() {
        b.c.a.b0.d dVar = this.l;
        if (dVar == null) {
            return false;
        }
        return dVar.t;
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void invalidateDrawable(@NonNull Drawable drawable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.invalidateDrawable(this);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void invalidateSelf() {
        if (!this.C) {
            this.C = true;
            Drawable.Callback callback = getCallback();
            if (callback != null) {
                callback.invalidateDrawable(this);
            }
        }
    }

    @Override // android.graphics.drawable.Animatable
    public boolean isRunning() {
        return i();
    }

    @MainThread
    public void j() {
        if (this.f349x == null) {
            this.p.add(new g());
            return;
        }
        if (this.n || h() == 0) {
            b.c.a.b0.d dVar = this.l;
            dVar.t = true;
            boolean k2 = dVar.k();
            for (Animator.AnimatorListener animatorListener : dVar.k) {
                if (Build.VERSION.SDK_INT >= 26) {
                    animatorListener.onAnimationStart(dVar, k2);
                } else {
                    animatorListener.onAnimationStart(dVar);
                }
            }
            dVar.n((int) (dVar.k() ? dVar.i() : dVar.j()));
            dVar.n = 0L;
            dVar.p = 0;
            dVar.l();
        }
        if (!this.n) {
            l((int) (this.l.l < 0.0f ? f() : e()));
            this.l.g();
        }
    }

    @MainThread
    public void k() {
        if (this.f349x == null) {
            this.p.add(new h());
            return;
        }
        if (this.n || h() == 0) {
            b.c.a.b0.d dVar = this.l;
            dVar.t = true;
            dVar.l();
            dVar.n = 0L;
            if (dVar.k() && dVar.o == dVar.j()) {
                dVar.o = dVar.i();
            } else if (!dVar.k() && dVar.o == dVar.i()) {
                dVar.o = dVar.j();
            }
        }
        if (!this.n) {
            l((int) (this.l.l < 0.0f ? f() : e()));
            this.l.g();
        }
    }

    public void l(int i2) {
        if (this.k == null) {
            this.p.add(new c(i2));
        } else {
            this.l.n(i2);
        }
    }

    public void m(int i2) {
        if (this.k == null) {
            this.p.add(new k(i2));
            return;
        }
        b.c.a.b0.d dVar = this.l;
        dVar.o(dVar.q, i2 + 0.99f);
    }

    public void n(String str) {
        b.c.a.d dVar = this.k;
        if (dVar == null) {
            this.p.add(new n(str));
            return;
        }
        b.c.a.y.i d2 = dVar.d(str);
        if (d2 != null) {
            m((int) (d2.f406b + d2.c));
            return;
        }
        throw new IllegalArgumentException(b.d.b.a.a.w("Cannot find marker with name ", str, "."));
    }

    public void o(@FloatRange(from = 0.0d, to = 1.0d) float f2) {
        b.c.a.d dVar = this.k;
        if (dVar == null) {
            this.p.add(new l(f2));
        } else {
            m((int) b.c.a.b0.f.e(dVar.k, dVar.l, f2));
        }
    }

    public void p(int i2, int i3) {
        if (this.k == null) {
            this.p.add(new b(i2, i3));
        } else {
            this.l.o(i2, i3 + 0.99f);
        }
    }

    public void q(String str) {
        b.c.a.d dVar = this.k;
        if (dVar == null) {
            this.p.add(new a(str));
            return;
        }
        b.c.a.y.i d2 = dVar.d(str);
        if (d2 != null) {
            int i2 = (int) d2.f406b;
            p(i2, ((int) d2.c) + i2);
            return;
        }
        throw new IllegalArgumentException(b.d.b.a.a.w("Cannot find marker with name ", str, "."));
    }

    public void r(int i2) {
        if (this.k == null) {
            this.p.add(new i(i2));
            return;
        }
        b.c.a.b0.d dVar = this.l;
        dVar.o(i2, (int) dVar.r);
    }

    public void s(String str) {
        b.c.a.d dVar = this.k;
        if (dVar == null) {
            this.p.add(new m(str));
            return;
        }
        b.c.a.y.i d2 = dVar.d(str);
        if (d2 != null) {
            r((int) d2.f406b);
            return;
        }
        throw new IllegalArgumentException(b.d.b.a.a.w("Cannot find marker with name ", str, "."));
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void scheduleDrawable(@NonNull Drawable drawable, @NonNull Runnable runnable, long j) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.scheduleDrawable(this, runnable, j);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(@IntRange(from = 0, to = 255) int i2) {
        this.f350y = i2;
        invalidateSelf();
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(@Nullable ColorFilter colorFilter) {
        b.c.a.b0.c.b("Use addColorFilter instead.");
    }

    @Override // android.graphics.drawable.Animatable
    @MainThread
    public void start() {
        j();
    }

    @Override // android.graphics.drawable.Animatable
    @MainThread
    public void stop() {
        this.p.clear();
        this.l.g();
    }

    public void t(float f2) {
        b.c.a.d dVar = this.k;
        if (dVar == null) {
            this.p.add(new C0060j(f2));
        } else {
            r((int) b.c.a.b0.f.e(dVar.k, dVar.l, f2));
        }
    }

    public void u(@FloatRange(from = 0.0d, to = 1.0d) float f2) {
        b.c.a.d dVar = this.k;
        if (dVar == null) {
            this.p.add(new d(f2));
            return;
        }
        this.l.n(b.c.a.b0.f.e(dVar.k, dVar.l, f2));
        b.c.a.c.a("Drawable#setProgress");
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void unscheduleDrawable(@NonNull Drawable drawable, @NonNull Runnable runnable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.unscheduleDrawable(this, runnable);
        }
    }

    public final void v() {
        b.c.a.d dVar = this.k;
        if (dVar != null) {
            float f2 = this.m;
            setBounds(0, 0, (int) (dVar.j.width() * f2), (int) (this.k.j.height() * f2));
        }
    }
}
