package b.c.a;

import android.graphics.ColorFilter;
import android.graphics.PointF;
import b.c.a.c0.d;
/* compiled from: LottieProperty.java */
/* loaded from: classes.dex */
public interface o {
    public static final Float i;
    public static final Float m;
    public static final Float n;
    public static final Integer a = 1;

    /* renamed from: b  reason: collision with root package name */
    public static final Integer f364b = 2;
    public static final Integer c = 3;
    public static final Integer d = 4;
    public static final PointF e = new PointF();
    public static final PointF f = new PointF();
    public static final PointF g = new PointF();
    public static final PointF h = new PointF();
    public static final PointF j = new PointF();
    public static final d k = new d();
    public static final Float l = Float.valueOf(1.0f);
    public static final Float o = Float.valueOf(2.0f);
    public static final Float p = Float.valueOf(3.0f);
    public static final Float q = Float.valueOf(4.0f);
    public static final Float r = Float.valueOf(5.0f);

    /* renamed from: s  reason: collision with root package name */
    public static final Float f365s = Float.valueOf(6.0f);
    public static final Float t = Float.valueOf(7.0f);
    public static final Float u = Float.valueOf(8.0f);
    public static final Float v = Float.valueOf(9.0f);
    public static final Float w = Float.valueOf(10.0f);

    /* renamed from: x  reason: collision with root package name */
    public static final Float f366x = Float.valueOf(11.0f);

    /* renamed from: y  reason: collision with root package name */
    public static final Float f367y = Float.valueOf(12.0f);

    /* renamed from: z  reason: collision with root package name */
    public static final Float f368z = Float.valueOf(12.1f);
    public static final Float A = Float.valueOf(13.0f);
    public static final Float B = Float.valueOf(14.0f);
    public static final ColorFilter C = new ColorFilter();
    public static final Integer[] D = new Integer[0];

    static {
        Float valueOf = Float.valueOf(0.0f);
        i = valueOf;
        m = valueOf;
        n = valueOf;
    }
}
