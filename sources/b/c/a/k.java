package b.c.a;

import android.graphics.Bitmap;
import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;
/* compiled from: LottieImageAsset.java */
/* loaded from: classes.dex */
public class k {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final int f363b;
    public final String c;
    public final String d;
    @Nullable
    public Bitmap e;

    @RestrictTo({RestrictTo.Scope.LIBRARY})
    public k(int i, int i2, String str, String str2, String str3) {
        this.a = i;
        this.f363b = i2;
        this.c = str;
        this.d = str2;
    }
}
