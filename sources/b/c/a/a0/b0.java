package b.c.a.a0;

import android.graphics.PointF;
import b.c.a.a0.i0.c;
import b.c.a.b0.f;
import b.c.a.y.a;
import b.c.a.y.l.k;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
/* compiled from: ShapeDataParser.java */
/* loaded from: classes.dex */
public class b0 implements h0<k> {
    public static final b0 a = new b0();

    /* renamed from: b  reason: collision with root package name */
    public static final c.a f323b = c.a.a("c", "v", "i", "o");

    @Override // b.c.a.a0.h0
    public k a(c cVar, float f) throws IOException {
        if (cVar.u() == c.b.BEGIN_ARRAY) {
            cVar.a();
        }
        cVar.b();
        List<PointF> list = null;
        List<PointF> list2 = null;
        List<PointF> list3 = null;
        boolean z2 = false;
        while (cVar.e()) {
            int y2 = cVar.y(f323b);
            if (y2 == 0) {
                z2 = cVar.f();
            } else if (y2 == 1) {
                list = p.c(cVar, f);
            } else if (y2 == 2) {
                list2 = p.c(cVar, f);
            } else if (y2 != 3) {
                cVar.A();
                cVar.C();
            } else {
                list3 = p.c(cVar, f);
            }
        }
        cVar.d();
        if (cVar.u() == c.b.END_ARRAY) {
            cVar.c();
        }
        if (list == null || list2 == null || list3 == null) {
            throw new IllegalArgumentException("Shape data was missing information.");
        } else if (list.isEmpty()) {
            return new k(new PointF(), false, Collections.emptyList());
        } else {
            int size = list.size();
            PointF pointF = list.get(0);
            ArrayList arrayList = new ArrayList(size);
            for (int i = 1; i < size; i++) {
                PointF pointF2 = list.get(i);
                int i2 = i - 1;
                arrayList.add(new a(f.a(list.get(i2), list3.get(i2)), f.a(pointF2, list2.get(i)), pointF2));
            }
            if (z2) {
                PointF pointF3 = list.get(0);
                int i3 = size - 1;
                arrayList.add(new a(f.a(list.get(i3), list3.get(i3)), f.a(pointF3, list2.get(0)), pointF3));
            }
            return new k(pointF, z2, arrayList);
        }
    }
}
