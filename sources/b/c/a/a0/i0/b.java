package b.c.a.a0.i0;

import androidx.annotation.Nullable;
import java.io.IOException;
/* compiled from: JsonEncodingException.java */
/* loaded from: classes.dex */
public final class b extends IOException {
    public b(@Nullable String str) {
        super(str);
    }
}
