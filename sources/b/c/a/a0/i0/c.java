package b.c.a.a0.i0;

import andhook.lib.xposed.ClassUtils;
import g0.o;
import java.io.Closeable;
import java.io.IOException;
import java.util.Arrays;
/* compiled from: JsonReader.java */
/* loaded from: classes.dex */
public abstract class c implements Closeable {
    public static final String[] j = new String[128];
    public int k;
    public int[] l = new int[32];
    public String[] m = new String[32];
    public int[] n = new int[32];

    /* compiled from: JsonReader.java */
    /* loaded from: classes.dex */
    public static final class a {
        public final String[] a;

        /* renamed from: b  reason: collision with root package name */
        public final o f329b;

        public a(String[] strArr, o oVar) {
            this.a = strArr;
            this.f329b = oVar;
        }

        /* JADX WARN: Removed duplicated region for block: B:19:0x003a A[Catch: IOException -> 0x006b, TryCatch #0 {IOException -> 0x006b, blocks: (B:2:0x0000, B:3:0x000a, B:5:0x000d, B:7:0x001e, B:9:0x0026, B:19:0x003a, B:20:0x003d, B:21:0x0042, B:23:0x0047, B:24:0x004a, B:25:0x0059), top: B:30:0x0000 }] */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public static b.c.a.a0.i0.c.a a(java.lang.String... r12) {
            /*
                int r0 = r12.length     // Catch: java.io.IOException -> L6b
                okio.ByteString[] r0 = new okio.ByteString[r0]     // Catch: java.io.IOException -> L6b
                g0.e r1 = new g0.e     // Catch: java.io.IOException -> L6b
                r1.<init>()     // Catch: java.io.IOException -> L6b
                r2 = 0
                r3 = 0
            La:
                int r4 = r12.length     // Catch: java.io.IOException -> L6b
                if (r3 >= r4) goto L59
                r4 = r12[r3]     // Catch: java.io.IOException -> L6b
                java.lang.String[] r5 = b.c.a.a0.i0.c.j     // Catch: java.io.IOException -> L6b
                r6 = 34
                r1.T(r6)     // Catch: java.io.IOException -> L6b
                int r7 = r4.length()     // Catch: java.io.IOException -> L6b
                r8 = 0
                r9 = 0
            L1c:
                if (r8 >= r7) goto L45
                char r10 = r4.charAt(r8)     // Catch: java.io.IOException -> L6b
                r11 = 128(0x80, float:1.794E-43)
                if (r10 >= r11) goto L2b
                r10 = r5[r10]     // Catch: java.io.IOException -> L6b
                if (r10 != 0) goto L38
                goto L42
            L2b:
                r11 = 8232(0x2028, float:1.1535E-41)
                if (r10 != r11) goto L32
                java.lang.String r10 = "\\u2028"
                goto L38
            L32:
                r11 = 8233(0x2029, float:1.1537E-41)
                if (r10 != r11) goto L42
                java.lang.String r10 = "\\u2029"
            L38:
                if (r9 >= r8) goto L3d
                r1.c0(r4, r9, r8)     // Catch: java.io.IOException -> L6b
            L3d:
                r1.b0(r10)     // Catch: java.io.IOException -> L6b
                int r9 = r8 + 1
            L42:
                int r8 = r8 + 1
                goto L1c
            L45:
                if (r9 >= r7) goto L4a
                r1.c0(r4, r9, r7)     // Catch: java.io.IOException -> L6b
            L4a:
                r1.T(r6)     // Catch: java.io.IOException -> L6b
                r1.readByte()     // Catch: java.io.IOException -> L6b
                okio.ByteString r4 = r1.x()     // Catch: java.io.IOException -> L6b
                r0[r3] = r4     // Catch: java.io.IOException -> L6b
                int r3 = r3 + 1
                goto La
            L59:
                b.c.a.a0.i0.c$a r1 = new b.c.a.a0.i0.c$a     // Catch: java.io.IOException -> L6b
                java.lang.Object r12 = r12.clone()     // Catch: java.io.IOException -> L6b
                java.lang.String[] r12 = (java.lang.String[]) r12     // Catch: java.io.IOException -> L6b
                g0.o$a r2 = g0.o.k     // Catch: java.io.IOException -> L6b
                g0.o r0 = r2.c(r0)     // Catch: java.io.IOException -> L6b
                r1.<init>(r12, r0)     // Catch: java.io.IOException -> L6b
                return r1
            L6b:
                r12 = move-exception
                java.lang.AssertionError r0 = new java.lang.AssertionError
                r0.<init>(r12)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: b.c.a.a0.i0.c.a.a(java.lang.String[]):b.c.a.a0.i0.c$a");
        }
    }

    /* compiled from: JsonReader.java */
    /* loaded from: classes.dex */
    public enum b {
        BEGIN_ARRAY,
        END_ARRAY,
        BEGIN_OBJECT,
        END_OBJECT,
        NAME,
        STRING,
        NUMBER,
        BOOLEAN,
        NULL,
        END_DOCUMENT
    }

    static {
        for (int i = 0; i <= 31; i++) {
            j[i] = String.format("\\u%04x", Integer.valueOf(i));
        }
        String[] strArr = j;
        strArr[34] = "\\\"";
        strArr[92] = "\\\\";
        strArr[9] = "\\t";
        strArr[8] = "\\b";
        strArr[10] = "\\n";
        strArr[13] = "\\r";
        strArr[12] = "\\f";
    }

    public abstract void A() throws IOException;

    public abstract void C() throws IOException;

    public final b.c.a.a0.i0.b D(String str) throws b.c.a.a0.i0.b {
        StringBuilder V = b.d.b.a.a.V(str, " at path ");
        V.append(getPath());
        throw new b.c.a.a0.i0.b(V.toString());
    }

    public abstract void a() throws IOException;

    public abstract void b() throws IOException;

    public abstract void c() throws IOException;

    public abstract void d() throws IOException;

    public abstract boolean e() throws IOException;

    public abstract boolean f() throws IOException;

    public final String getPath() {
        int i = this.k;
        int[] iArr = this.l;
        String[] strArr = this.m;
        int[] iArr2 = this.n;
        StringBuilder O = b.d.b.a.a.O(ClassUtils.INNER_CLASS_SEPARATOR_CHAR);
        for (int i2 = 0; i2 < i; i2++) {
            int i3 = iArr[i2];
            if (i3 == 1 || i3 == 2) {
                O.append('[');
                O.append(iArr2[i2]);
                O.append(']');
            } else if (i3 == 3 || i3 == 4 || i3 == 5) {
                O.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
                if (strArr[i2] != null) {
                    O.append(strArr[i2]);
                }
            }
        }
        return O.toString();
    }

    public abstract double n() throws IOException;

    public abstract int q() throws IOException;

    public abstract String s() throws IOException;

    public abstract String t() throws IOException;

    public abstract b u() throws IOException;

    public final void x(int i) {
        int i2 = this.k;
        int[] iArr = this.l;
        if (i2 == iArr.length) {
            if (i2 != 256) {
                this.l = Arrays.copyOf(iArr, iArr.length * 2);
                String[] strArr = this.m;
                this.m = (String[]) Arrays.copyOf(strArr, strArr.length * 2);
                int[] iArr2 = this.n;
                this.n = Arrays.copyOf(iArr2, iArr2.length * 2);
            } else {
                StringBuilder R = b.d.b.a.a.R("Nesting too deep at ");
                R.append(getPath());
                throw new b.c.a.a0.i0.a(R.toString());
            }
        }
        int[] iArr3 = this.l;
        int i3 = this.k;
        this.k = i3 + 1;
        iArr3[i3] = i;
    }

    public abstract int y(a aVar) throws IOException;
}
