package b.c.a.a0.i0;

import androidx.annotation.Nullable;
import b.c.a.a0.i0.c;
import b.d.b.a.a;
import g0.e;
import g0.g;
import java.io.EOFException;
import java.io.IOException;
import java.util.Objects;
import okio.ByteString;
/* compiled from: JsonUtf8Reader.java */
/* loaded from: classes.dex */
public final class d extends c {
    public static final ByteString o = ByteString.h("'\\");
    public static final ByteString p = ByteString.h("\"\\");
    public static final ByteString q = ByteString.h("{}[]:, \n\t\r\f/\\;#=");
    public static final ByteString r = ByteString.h("\n\r");

    /* renamed from: s  reason: collision with root package name */
    public static final ByteString f331s = ByteString.h("*/");
    public final g t;
    public final e u;
    public int v = 0;
    public long w;

    /* renamed from: x  reason: collision with root package name */
    public int f332x;
    @Nullable

    /* renamed from: y  reason: collision with root package name */
    public String f333y;

    public d(g gVar) {
        Objects.requireNonNull(gVar, "source == null");
        this.t = gVar;
        this.u = gVar.g();
        x(6);
    }

    @Override // b.c.a.a0.i0.c
    public void A() throws IOException {
        int i = this.v;
        if (i == 0) {
            i = I();
        }
        if (i == 14) {
            U();
        } else if (i == 13) {
            T(p);
        } else if (i == 12) {
            T(o);
        } else if (i != 15) {
            StringBuilder R = a.R("Expected a name but was ");
            R.append(u());
            R.append(" at path ");
            R.append(getPath());
            throw new a(R.toString());
        }
        this.v = 0;
        this.m[this.k - 1] = "null";
    }

    @Override // b.c.a.a0.i0.c
    public void C() throws IOException {
        int i = 0;
        do {
            int i2 = this.v;
            if (i2 == 0) {
                i2 = I();
            }
            if (i2 == 3) {
                x(1);
            } else if (i2 == 1) {
                x(3);
            } else {
                if (i2 == 4) {
                    i--;
                    if (i >= 0) {
                        this.k--;
                    } else {
                        StringBuilder R = a.R("Expected a value but was ");
                        R.append(u());
                        R.append(" at path ");
                        R.append(getPath());
                        throw new a(R.toString());
                    }
                } else if (i2 == 2) {
                    i--;
                    if (i >= 0) {
                        this.k--;
                    } else {
                        StringBuilder R2 = a.R("Expected a value but was ");
                        R2.append(u());
                        R2.append(" at path ");
                        R2.append(getPath());
                        throw new a(R2.toString());
                    }
                } else if (i2 == 14 || i2 == 10) {
                    U();
                } else if (i2 == 9 || i2 == 13) {
                    T(p);
                } else if (i2 == 8 || i2 == 12) {
                    T(o);
                } else if (i2 == 17) {
                    this.u.skip(this.f332x);
                } else if (i2 == 18) {
                    StringBuilder R3 = a.R("Expected a value but was ");
                    R3.append(u());
                    R3.append(" at path ");
                    R3.append(getPath());
                    throw new a(R3.toString());
                }
                this.v = 0;
            }
            i++;
            this.v = 0;
        } while (i != 0);
        int[] iArr = this.n;
        int i3 = this.k;
        int i4 = i3 - 1;
        iArr[i4] = iArr[i4] + 1;
        this.m[i3 - 1] = "null";
    }

    public final void H() throws IOException {
        D("Use JsonReader.setLenient(true) to accept malformed JSON");
        throw null;
    }

    /* JADX WARN: Code restructure failed: missing block: B:123:0x01aa, code lost:
        if (L(r2) != false) goto L159;
     */
    /* JADX WARN: Code restructure failed: missing block: B:124:0x01ad, code lost:
        if (r1 != 2) goto L136;
     */
    /* JADX WARN: Code restructure failed: missing block: B:125:0x01af, code lost:
        if (r6 == false) goto L135;
     */
    /* JADX WARN: Code restructure failed: missing block: B:127:0x01b5, code lost:
        if (r7 != Long.MIN_VALUE) goto L129;
     */
    /* JADX WARN: Code restructure failed: missing block: B:128:0x01b7, code lost:
        if (r9 == false) goto L135;
     */
    /* JADX WARN: Code restructure failed: missing block: B:130:0x01bd, code lost:
        if (r7 != 0) goto L132;
     */
    /* JADX WARN: Code restructure failed: missing block: B:131:0x01bf, code lost:
        if (r9 != false) goto L135;
     */
    /* JADX WARN: Code restructure failed: missing block: B:132:0x01c1, code lost:
        if (r9 == false) goto L133;
     */
    /* JADX WARN: Code restructure failed: missing block: B:133:0x01c4, code lost:
        r7 = -r7;
     */
    /* JADX WARN: Code restructure failed: missing block: B:134:0x01c5, code lost:
        r17.w = r7;
        r17.u.skip(r5);
        r15 = 16;
        r17.v = 16;
     */
    /* JADX WARN: Code restructure failed: missing block: B:135:0x01d2, code lost:
        r2 = 2;
     */
    /* JADX WARN: Code restructure failed: missing block: B:136:0x01d3, code lost:
        if (r1 == r2) goto L141;
     */
    /* JADX WARN: Code restructure failed: missing block: B:138:0x01d6, code lost:
        if (r1 == 4) goto L141;
     */
    /* JADX WARN: Code restructure failed: missing block: B:140:0x01d9, code lost:
        if (r1 != 7) goto L159;
     */
    /* JADX WARN: Code restructure failed: missing block: B:141:0x01db, code lost:
        r17.f332x = r5;
        r15 = 17;
        r17.v = 17;
     */
    /* JADX WARN: Code restructure failed: missing block: B:84:0x0133, code lost:
        r2 = 2;
     */
    /* JADX WARN: Removed duplicated region for block: B:161:0x0213 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:162:0x0214  */
    /* JADX WARN: Removed duplicated region for block: B:80:0x0122 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:81:0x0123  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final int I() throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 713
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.c.a.a0.i0.d.I():int");
    }

    public final int J(String str, c.a aVar) {
        int length = aVar.a.length;
        for (int i = 0; i < length; i++) {
            if (str.equals(aVar.a[i])) {
                this.v = 0;
                this.m[this.k - 1] = str;
                return i;
            }
        }
        return -1;
    }

    public final boolean L(int i) throws IOException {
        if (i == 9 || i == 10 || i == 12 || i == 13 || i == 32) {
            return false;
        }
        if (i != 35) {
            if (i == 44) {
                return false;
            }
            if (!(i == 47 || i == 61)) {
                if (i == 123 || i == 125 || i == 58) {
                    return false;
                }
                if (i != 59) {
                    switch (i) {
                        case 91:
                        case 93:
                            return false;
                        case 92:
                            break;
                        default:
                            return true;
                    }
                }
            }
        }
        H();
        throw null;
    }

    public final int N(boolean z2) throws IOException {
        int i = 0;
        while (true) {
            int i2 = i + 1;
            if (this.t.j(i2)) {
                byte q2 = this.u.q(i);
                if (q2 == 10 || q2 == 32 || q2 == 13 || q2 == 9) {
                    i = i2;
                } else {
                    this.u.skip(i2 - 1);
                    if (q2 == 47) {
                        if (!this.t.j(2L)) {
                            return q2;
                        }
                        H();
                        throw null;
                    } else if (q2 != 35) {
                        return q2;
                    } else {
                        H();
                        throw null;
                    }
                }
            } else if (!z2) {
                return -1;
            } else {
                throw new EOFException("End of input");
            }
        }
    }

    public final String O(ByteString byteString) throws IOException {
        StringBuilder sb = null;
        while (true) {
            long E = this.t.E(byteString);
            if (E == -1) {
                D("Unterminated string");
                throw null;
            } else if (this.u.q(E) == 92) {
                if (sb == null) {
                    sb = new StringBuilder();
                }
                sb.append(this.u.H(E));
                this.u.readByte();
                sb.append(S());
            } else if (sb == null) {
                String H = this.u.H(E);
                this.u.readByte();
                return H;
            } else {
                sb.append(this.u.H(E));
                this.u.readByte();
                return sb.toString();
            }
        }
    }

    public final String R() throws IOException {
        long E = this.t.E(q);
        return E != -1 ? this.u.H(E) : this.u.D();
    }

    public final char S() throws IOException {
        int i;
        int i2;
        if (this.t.j(1L)) {
            byte readByte = this.u.readByte();
            if (readByte == 10 || readByte == 34 || readByte == 39 || readByte == 47 || readByte == 92) {
                return (char) readByte;
            }
            if (readByte == 98) {
                return '\b';
            }
            if (readByte == 102) {
                return '\f';
            }
            if (readByte == 110) {
                return '\n';
            }
            if (readByte == 114) {
                return '\r';
            }
            if (readByte == 116) {
                return '\t';
            }
            if (readByte != 117) {
                StringBuilder R = a.R("Invalid escape sequence: \\");
                R.append((char) readByte);
                D(R.toString());
                throw null;
            } else if (this.t.j(4L)) {
                char c = 0;
                for (int i3 = 0; i3 < 4; i3++) {
                    byte q2 = this.u.q(i3);
                    char c2 = (char) (c << 4);
                    if (q2 < 48 || q2 > 57) {
                        if (q2 >= 97 && q2 <= 102) {
                            i2 = q2 - 97;
                        } else if (q2 < 65 || q2 > 70) {
                            StringBuilder R2 = a.R("\\u");
                            R2.append(this.u.H(4L));
                            D(R2.toString());
                            throw null;
                        } else {
                            i2 = q2 - 65;
                        }
                        i = i2 + 10;
                    } else {
                        i = q2 - 48;
                    }
                    c = (char) (i + c2);
                }
                this.u.skip(4L);
                return c;
            } else {
                StringBuilder R3 = a.R("Unterminated escape sequence at path ");
                R3.append(getPath());
                throw new EOFException(R3.toString());
            }
        } else {
            D("Unterminated escape sequence");
            throw null;
        }
    }

    public final void T(ByteString byteString) throws IOException {
        while (true) {
            long E = this.t.E(byteString);
            if (E == -1) {
                D("Unterminated string");
                throw null;
            } else if (this.u.q(E) == 92) {
                this.u.skip(E + 1);
                S();
            } else {
                this.u.skip(E + 1);
                return;
            }
        }
    }

    public final void U() throws IOException {
        long E = this.t.E(q);
        e eVar = this.u;
        if (E == -1) {
            E = eVar.k;
        }
        eVar.skip(E);
    }

    @Override // b.c.a.a0.i0.c
    public void a() throws IOException {
        int i = this.v;
        if (i == 0) {
            i = I();
        }
        if (i == 3) {
            x(1);
            this.n[this.k - 1] = 0;
            this.v = 0;
            return;
        }
        StringBuilder R = a.R("Expected BEGIN_ARRAY but was ");
        R.append(u());
        R.append(" at path ");
        R.append(getPath());
        throw new a(R.toString());
    }

    @Override // b.c.a.a0.i0.c
    public void b() throws IOException {
        int i = this.v;
        if (i == 0) {
            i = I();
        }
        if (i == 1) {
            x(3);
            this.v = 0;
            return;
        }
        StringBuilder R = a.R("Expected BEGIN_OBJECT but was ");
        R.append(u());
        R.append(" at path ");
        R.append(getPath());
        throw new a(R.toString());
    }

    @Override // b.c.a.a0.i0.c
    public void c() throws IOException {
        int i = this.v;
        if (i == 0) {
            i = I();
        }
        if (i == 4) {
            int i2 = this.k - 1;
            this.k = i2;
            int[] iArr = this.n;
            int i3 = i2 - 1;
            iArr[i3] = iArr[i3] + 1;
            this.v = 0;
            return;
        }
        StringBuilder R = a.R("Expected END_ARRAY but was ");
        R.append(u());
        R.append(" at path ");
        R.append(getPath());
        throw new a(R.toString());
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.v = 0;
        this.l[0] = 8;
        this.k = 1;
        e eVar = this.u;
        eVar.skip(eVar.k);
        this.t.close();
    }

    @Override // b.c.a.a0.i0.c
    public void d() throws IOException {
        int i = this.v;
        if (i == 0) {
            i = I();
        }
        if (i == 2) {
            int i2 = this.k - 1;
            this.k = i2;
            this.m[i2] = null;
            int[] iArr = this.n;
            int i3 = i2 - 1;
            iArr[i3] = iArr[i3] + 1;
            this.v = 0;
            return;
        }
        StringBuilder R = a.R("Expected END_OBJECT but was ");
        R.append(u());
        R.append(" at path ");
        R.append(getPath());
        throw new a(R.toString());
    }

    @Override // b.c.a.a0.i0.c
    public boolean e() throws IOException {
        int i = this.v;
        if (i == 0) {
            i = I();
        }
        return (i == 2 || i == 4 || i == 18) ? false : true;
    }

    @Override // b.c.a.a0.i0.c
    public boolean f() throws IOException {
        int i = this.v;
        if (i == 0) {
            i = I();
        }
        if (i == 5) {
            this.v = 0;
            int[] iArr = this.n;
            int i2 = this.k - 1;
            iArr[i2] = iArr[i2] + 1;
            return true;
        } else if (i == 6) {
            this.v = 0;
            int[] iArr2 = this.n;
            int i3 = this.k - 1;
            iArr2[i3] = iArr2[i3] + 1;
            return false;
        } else {
            StringBuilder R = a.R("Expected a boolean but was ");
            R.append(u());
            R.append(" at path ");
            R.append(getPath());
            throw new a(R.toString());
        }
    }

    @Override // b.c.a.a0.i0.c
    public double n() throws IOException {
        int i = this.v;
        if (i == 0) {
            i = I();
        }
        if (i == 16) {
            this.v = 0;
            int[] iArr = this.n;
            int i2 = this.k - 1;
            iArr[i2] = iArr[i2] + 1;
            return this.w;
        }
        if (i == 17) {
            this.f333y = this.u.H(this.f332x);
        } else if (i == 9) {
            this.f333y = O(p);
        } else if (i == 8) {
            this.f333y = O(o);
        } else if (i == 10) {
            this.f333y = R();
        } else if (i != 11) {
            StringBuilder R = a.R("Expected a double but was ");
            R.append(u());
            R.append(" at path ");
            R.append(getPath());
            throw new a(R.toString());
        }
        this.v = 11;
        try {
            double parseDouble = Double.parseDouble(this.f333y);
            if (Double.isNaN(parseDouble) || Double.isInfinite(parseDouble)) {
                throw new b("JSON forbids NaN and infinities: " + parseDouble + " at path " + getPath());
            }
            this.f333y = null;
            this.v = 0;
            int[] iArr2 = this.n;
            int i3 = this.k - 1;
            iArr2[i3] = iArr2[i3] + 1;
            return parseDouble;
        } catch (NumberFormatException unused) {
            StringBuilder R2 = a.R("Expected a double but was ");
            R2.append(this.f333y);
            R2.append(" at path ");
            R2.append(getPath());
            throw new a(R2.toString());
        }
    }

    @Override // b.c.a.a0.i0.c
    public int q() throws IOException {
        String str;
        int i = this.v;
        if (i == 0) {
            i = I();
        }
        if (i == 16) {
            long j = this.w;
            int i2 = (int) j;
            if (j == i2) {
                this.v = 0;
                int[] iArr = this.n;
                int i3 = this.k - 1;
                iArr[i3] = iArr[i3] + 1;
                return i2;
            }
            StringBuilder R = a.R("Expected an int but was ");
            R.append(this.w);
            R.append(" at path ");
            R.append(getPath());
            throw new a(R.toString());
        }
        if (i == 17) {
            this.f333y = this.u.H(this.f332x);
        } else if (i == 9 || i == 8) {
            if (i == 9) {
                str = O(p);
            } else {
                str = O(o);
            }
            this.f333y = str;
            try {
                int parseInt = Integer.parseInt(str);
                this.v = 0;
                int[] iArr2 = this.n;
                int i4 = this.k - 1;
                iArr2[i4] = iArr2[i4] + 1;
                return parseInt;
            } catch (NumberFormatException unused) {
            }
        } else if (i != 11) {
            StringBuilder R2 = a.R("Expected an int but was ");
            R2.append(u());
            R2.append(" at path ");
            R2.append(getPath());
            throw new a(R2.toString());
        }
        this.v = 11;
        try {
            double parseDouble = Double.parseDouble(this.f333y);
            int i5 = (int) parseDouble;
            if (i5 == parseDouble) {
                this.f333y = null;
                this.v = 0;
                int[] iArr3 = this.n;
                int i6 = this.k - 1;
                iArr3[i6] = iArr3[i6] + 1;
                return i5;
            }
            StringBuilder R3 = a.R("Expected an int but was ");
            R3.append(this.f333y);
            R3.append(" at path ");
            R3.append(getPath());
            throw new a(R3.toString());
        } catch (NumberFormatException unused2) {
            StringBuilder R4 = a.R("Expected an int but was ");
            R4.append(this.f333y);
            R4.append(" at path ");
            R4.append(getPath());
            throw new a(R4.toString());
        }
    }

    @Override // b.c.a.a0.i0.c
    public String s() throws IOException {
        String str;
        int i = this.v;
        if (i == 0) {
            i = I();
        }
        if (i == 14) {
            str = R();
        } else if (i == 13) {
            str = O(p);
        } else if (i == 12) {
            str = O(o);
        } else if (i == 15) {
            str = this.f333y;
        } else {
            StringBuilder R = a.R("Expected a name but was ");
            R.append(u());
            R.append(" at path ");
            R.append(getPath());
            throw new a(R.toString());
        }
        this.v = 0;
        this.m[this.k - 1] = str;
        return str;
    }

    @Override // b.c.a.a0.i0.c
    public String t() throws IOException {
        String str;
        int i = this.v;
        if (i == 0) {
            i = I();
        }
        if (i == 10) {
            str = R();
        } else if (i == 9) {
            str = O(p);
        } else if (i == 8) {
            str = O(o);
        } else if (i == 11) {
            str = this.f333y;
            this.f333y = null;
        } else if (i == 16) {
            str = Long.toString(this.w);
        } else if (i == 17) {
            str = this.u.H(this.f332x);
        } else {
            StringBuilder R = a.R("Expected a string but was ");
            R.append(u());
            R.append(" at path ");
            R.append(getPath());
            throw new a(R.toString());
        }
        this.v = 0;
        int[] iArr = this.n;
        int i2 = this.k - 1;
        iArr[i2] = iArr[i2] + 1;
        return str;
    }

    public String toString() {
        StringBuilder R = a.R("JsonReader(");
        R.append(this.t);
        R.append(")");
        return R.toString();
    }

    @Override // b.c.a.a0.i0.c
    public c.b u() throws IOException {
        int i = this.v;
        if (i == 0) {
            i = I();
        }
        switch (i) {
            case 1:
                return c.b.BEGIN_OBJECT;
            case 2:
                return c.b.END_OBJECT;
            case 3:
                return c.b.BEGIN_ARRAY;
            case 4:
                return c.b.END_ARRAY;
            case 5:
            case 6:
                return c.b.BOOLEAN;
            case 7:
                return c.b.NULL;
            case 8:
            case 9:
            case 10:
            case 11:
                return c.b.STRING;
            case 12:
            case 13:
            case 14:
            case 15:
                return c.b.NAME;
            case 16:
            case 17:
                return c.b.NUMBER;
            case 18:
                return c.b.END_DOCUMENT;
            default:
                throw new AssertionError();
        }
    }

    @Override // b.c.a.a0.i0.c
    public int y(c.a aVar) throws IOException {
        int i = this.v;
        if (i == 0) {
            i = I();
        }
        if (i < 12 || i > 15) {
            return -1;
        }
        if (i == 15) {
            return J(this.f333y, aVar);
        }
        int v0 = this.t.v0(aVar.f329b);
        if (v0 != -1) {
            this.v = 0;
            this.m[this.k - 1] = aVar.a[v0];
            return v0;
        }
        String str = this.m[this.k - 1];
        String s2 = s();
        int J = J(s2, aVar);
        if (J == -1) {
            this.v = 15;
            this.f333y = s2;
            this.m[this.k - 1] = str;
        }
        return J;
    }
}
