package b.c.a.a0;

import android.graphics.Color;
import android.graphics.PointF;
import androidx.annotation.ColorInt;
import b.c.a.a0.i0.c;
import b.d.b.a.a;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/* compiled from: JsonUtils.java */
/* loaded from: classes.dex */
public class p {
    public static final c.a a = c.a.a("x", "y");

    @ColorInt
    public static int a(c cVar) throws IOException {
        cVar.a();
        int n = (int) (cVar.n() * 255.0d);
        int n2 = (int) (cVar.n() * 255.0d);
        int n3 = (int) (cVar.n() * 255.0d);
        while (cVar.e()) {
            cVar.C();
        }
        cVar.c();
        return Color.argb(255, n, n2, n3);
    }

    public static PointF b(c cVar, float f) throws IOException {
        int ordinal = cVar.u().ordinal();
        if (ordinal == 0) {
            cVar.a();
            float n = (float) cVar.n();
            float n2 = (float) cVar.n();
            while (cVar.u() != c.b.END_ARRAY) {
                cVar.C();
            }
            cVar.c();
            return new PointF(n * f, n2 * f);
        } else if (ordinal == 2) {
            cVar.b();
            float f2 = 0.0f;
            float f3 = 0.0f;
            while (cVar.e()) {
                int y2 = cVar.y(a);
                if (y2 == 0) {
                    f2 = d(cVar);
                } else if (y2 != 1) {
                    cVar.A();
                    cVar.C();
                } else {
                    f3 = d(cVar);
                }
            }
            cVar.d();
            return new PointF(f2 * f, f3 * f);
        } else if (ordinal == 6) {
            float n3 = (float) cVar.n();
            float n4 = (float) cVar.n();
            while (cVar.e()) {
                cVar.C();
            }
            return new PointF(n3 * f, n4 * f);
        } else {
            StringBuilder R = a.R("Unknown point starts with ");
            R.append(cVar.u());
            throw new IllegalArgumentException(R.toString());
        }
    }

    public static List<PointF> c(c cVar, float f) throws IOException {
        ArrayList arrayList = new ArrayList();
        cVar.a();
        while (cVar.u() == c.b.BEGIN_ARRAY) {
            cVar.a();
            arrayList.add(b(cVar, f));
            cVar.c();
        }
        cVar.c();
        return arrayList;
    }

    public static float d(c cVar) throws IOException {
        c.b u = cVar.u();
        int ordinal = u.ordinal();
        if (ordinal == 0) {
            cVar.a();
            float n = (float) cVar.n();
            while (cVar.e()) {
                cVar.C();
            }
            cVar.c();
            return n;
        } else if (ordinal == 6) {
            return (float) cVar.n();
        } else {
            throw new IllegalArgumentException("Unknown value for token of type " + u);
        }
    }
}
