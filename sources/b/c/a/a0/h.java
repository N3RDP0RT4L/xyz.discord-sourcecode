package b.c.a.a0;

import b.c.a.a0.i0.c;
import b.c.a.y.b;
import b.c.a.y.c;
import java.io.IOException;
/* compiled from: DocumentDataParser.java */
/* loaded from: classes.dex */
public class h implements h0<c> {
    public static final h a = new h();

    /* renamed from: b  reason: collision with root package name */
    public static final c.a f328b = c.a.a("t", "f", "s", "j", "tr", "lh", "ls", "fc", "sc", "sw", "of");

    @Override // b.c.a.a0.h0
    public b.c.a.y.c a(b.c.a.a0.i0.c cVar, float f) throws IOException {
        cVar.b();
        String str = null;
        String str2 = null;
        float f2 = 0.0f;
        int i = 3;
        int i2 = 0;
        float f3 = 0.0f;
        float f4 = 0.0f;
        int i3 = 0;
        int i4 = 0;
        float f5 = 0.0f;
        boolean z2 = true;
        while (cVar.e()) {
            switch (cVar.y(f328b)) {
                case 0:
                    str = cVar.t();
                    break;
                case 1:
                    str2 = cVar.t();
                    break;
                case 2:
                    f2 = (float) cVar.n();
                    break;
                case 3:
                    int q = cVar.q();
                    if (q <= 2 && q >= 0) {
                        i = b.com$airbnb$lottie$model$DocumentData$Justification$s$values()[q];
                        break;
                    } else {
                        i = 3;
                        break;
                    }
                case 4:
                    i2 = cVar.q();
                    break;
                case 5:
                    f3 = (float) cVar.n();
                    break;
                case 6:
                    f4 = (float) cVar.n();
                    break;
                case 7:
                    i3 = p.a(cVar);
                    break;
                case 8:
                    i4 = p.a(cVar);
                    break;
                case 9:
                    f5 = (float) cVar.n();
                    break;
                case 10:
                    z2 = cVar.f();
                    break;
                default:
                    cVar.A();
                    cVar.C();
                    break;
            }
        }
        cVar.d();
        return new b.c.a.y.c(str, str2, f2, i, i2, f3, f4, i3, i4, f5, z2);
    }
}
