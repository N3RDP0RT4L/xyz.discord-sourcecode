package b.c.a.a0;

import android.graphics.Color;
import b.c.a.a0.i0.c;
import java.io.IOException;
/* compiled from: ColorParser.java */
/* loaded from: classes.dex */
public class f implements h0<Integer> {
    public static final f a = new f();

    @Override // b.c.a.a0.h0
    public Integer a(c cVar, float f) throws IOException {
        boolean z2 = cVar.u() == c.b.BEGIN_ARRAY;
        if (z2) {
            cVar.a();
        }
        double n = cVar.n();
        double n2 = cVar.n();
        double n3 = cVar.n();
        double n4 = cVar.n();
        if (z2) {
            cVar.c();
        }
        if (n <= 1.0d && n2 <= 1.0d && n3 <= 1.0d) {
            n *= 255.0d;
            n2 *= 255.0d;
            n3 *= 255.0d;
            if (n4 <= 1.0d) {
                n4 *= 255.0d;
            }
        }
        return Integer.valueOf(Color.argb((int) n4, (int) n, (int) n2, (int) n3));
    }
}
