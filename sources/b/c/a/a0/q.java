package b.c.a.a0;

import android.graphics.PathMeasure;
import android.graphics.PointF;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import androidx.collection.SparseArrayCompat;
import androidx.core.view.animation.PathInterpolatorCompat;
import b.c.a.a0.i0.c;
import b.c.a.b0.f;
import b.c.a.b0.g;
import b.c.a.c0.a;
import b.c.a.d;
import java.io.IOException;
import java.lang.ref.WeakReference;
/* compiled from: KeyframeParser.java */
/* loaded from: classes.dex */
public class q {

    /* renamed from: b  reason: collision with root package name */
    public static SparseArrayCompat<WeakReference<Interpolator>> f337b;
    public static final Interpolator a = new LinearInterpolator();
    public static c.a c = c.a.a("t", "s", "e", "o", "i", "h", "to", "ti");

    public static <T> a<T> a(c cVar, d dVar, float f, h0<T> h0Var, boolean z2) throws IOException {
        T t;
        WeakReference<Interpolator> weakReference;
        Interpolator interpolator;
        if (!z2) {
            return new a<>(h0Var.a(cVar, f));
        }
        cVar.b();
        int i = 1;
        Interpolator interpolator2 = null;
        PointF pointF = null;
        PointF pointF2 = null;
        T t2 = null;
        T t3 = null;
        PointF pointF3 = null;
        PointF pointF4 = null;
        boolean z3 = false;
        float f2 = 0.0f;
        while (cVar.e()) {
            switch (cVar.y(c)) {
                case 0:
                    f2 = (float) cVar.n();
                    break;
                case 1:
                    t3 = h0Var.a(cVar, f);
                    continue;
                case 2:
                    t2 = h0Var.a(cVar, f);
                    continue;
                case 3:
                    pointF = p.b(cVar, f);
                    continue;
                case 4:
                    pointF2 = p.b(cVar, f);
                    continue;
                case 5:
                    if (cVar.q() == i) {
                        z3 = true;
                        continue;
                    } else {
                        z3 = false;
                    }
                case 6:
                    pointF4 = p.b(cVar, f);
                    continue;
                case 7:
                    pointF3 = p.b(cVar, f);
                    continue;
                default:
                    cVar.C();
                    break;
            }
            i = 1;
        }
        cVar.d();
        if (z3) {
            interpolator2 = a;
            t = t3;
        } else {
            if (pointF == null || pointF2 == null) {
                interpolator2 = a;
            } else {
                float f3 = -f;
                pointF.x = f.b(pointF.x, f3, f);
                pointF.y = f.b(pointF.y, -100.0f, 100.0f);
                pointF2.x = f.b(pointF2.x, f3, f);
                float b2 = f.b(pointF2.y, -100.0f, 100.0f);
                pointF2.y = b2;
                float f4 = pointF.x;
                float f5 = pointF.y;
                float f6 = pointF2.x;
                PathMeasure pathMeasure = g.a;
                int i2 = f4 != 0.0f ? (int) (527 * f4) : 17;
                if (f5 != 0.0f) {
                    i2 = (int) (i2 * 31 * f5);
                }
                if (f6 != 0.0f) {
                    i2 = (int) (i2 * 31 * f6);
                }
                if (b2 != 0.0f) {
                    i2 = (int) (i2 * 31 * b2);
                }
                synchronized (q.class) {
                    if (f337b == null) {
                        f337b = new SparseArrayCompat<>();
                    }
                    weakReference = f337b.get(i2);
                }
                if (weakReference != null) {
                    interpolator2 = weakReference.get();
                }
                if (weakReference == null || interpolator2 == null) {
                    pointF.x /= f;
                    pointF.y /= f;
                    float f7 = pointF2.x / f;
                    pointF2.x = f7;
                    float f8 = pointF2.y / f;
                    pointF2.y = f8;
                    try {
                        interpolator = PathInterpolatorCompat.create(pointF.x, pointF.y, f7, f8);
                    } catch (IllegalArgumentException e) {
                        if (e.getMessage().equals("The Path cannot loop back on itself.")) {
                            interpolator = PathInterpolatorCompat.create(Math.min(pointF.x, 1.0f), pointF.y, Math.max(pointF2.x, 0.0f), pointF2.y);
                        } else {
                            interpolator = new LinearInterpolator();
                        }
                    }
                    interpolator2 = interpolator;
                    try {
                        WeakReference<Interpolator> weakReference2 = new WeakReference<>(interpolator2);
                        synchronized (q.class) {
                            f337b.put(i2, weakReference2);
                        }
                    } catch (ArrayIndexOutOfBoundsException unused) {
                    }
                }
            }
            t = t2;
        }
        a<T> aVar = new a<>(dVar, t3, t, interpolator2, f2, null);
        aVar.m = pointF4;
        aVar.n = pointF3;
        return aVar;
    }
}
