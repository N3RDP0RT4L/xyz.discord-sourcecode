package b.c.a.a0;

import b.c.a.a0.i0.c;
import b.c.a.c0.d;
import java.io.IOException;
/* compiled from: ScaleXYParser.java */
/* loaded from: classes.dex */
public class a0 implements h0<d> {
    public static final a0 a = new a0();

    @Override // b.c.a.a0.h0
    public d a(c cVar, float f) throws IOException {
        boolean z2 = cVar.u() == c.b.BEGIN_ARRAY;
        if (z2) {
            cVar.a();
        }
        float n = (float) cVar.n();
        float n2 = (float) cVar.n();
        while (cVar.e()) {
            cVar.C();
        }
        if (z2) {
            cVar.c();
        }
        return new d((n / 100.0f) * f, (n2 / 100.0f) * f);
    }
}
