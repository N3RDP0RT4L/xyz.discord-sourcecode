package b.c.a.a0;

import android.graphics.PointF;
import b.c.a.a0.i0.c;
import b.c.a.b0.g;
import b.c.a.d;
import b.c.a.w.c.h;
import b.c.a.y.k.b;
import b.c.a.y.k.e;
import b.c.a.y.k.i;
import b.c.a.y.k.m;
import java.io.IOException;
import java.util.ArrayList;
/* compiled from: AnimatablePathValueParser.java */
/* loaded from: classes.dex */
public class a {
    public static c.a a = c.a.a("k", "x", "y");

    public static e a(c cVar, d dVar) throws IOException {
        ArrayList arrayList = new ArrayList();
        if (cVar.u() == c.b.BEGIN_ARRAY) {
            cVar.a();
            while (cVar.e()) {
                arrayList.add(new h(dVar, q.a(cVar, dVar, g.c(), v.a, cVar.u() == c.b.BEGIN_OBJECT)));
            }
            cVar.c();
            r.b(arrayList);
        } else {
            arrayList.add(new b.c.a.c0.a(p.b(cVar, g.c())));
        }
        return new e(arrayList);
    }

    public static m<PointF, PointF> b(c cVar, d dVar) throws IOException {
        c.b bVar = c.b.STRING;
        cVar.b();
        e eVar = null;
        b bVar2 = null;
        b bVar3 = null;
        boolean z2 = false;
        while (cVar.u() != c.b.END_OBJECT) {
            int y2 = cVar.y(a);
            if (y2 == 0) {
                eVar = a(cVar, dVar);
            } else if (y2 != 1) {
                if (y2 != 2) {
                    cVar.A();
                    cVar.C();
                } else if (cVar.u() == bVar) {
                    cVar.C();
                    z2 = true;
                } else {
                    bVar3 = d.q1(cVar, dVar);
                }
            } else if (cVar.u() == bVar) {
                cVar.C();
                z2 = true;
            } else {
                bVar2 = d.q1(cVar, dVar);
            }
        }
        cVar.d();
        if (z2) {
            dVar.a("Lottie doesn't support expressions.");
        }
        return eVar != null ? eVar : new i(bVar2, bVar3);
    }
}
