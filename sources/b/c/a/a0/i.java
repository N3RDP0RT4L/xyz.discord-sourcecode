package b.c.a.a0;

import b.c.a.a0.i0.c;
import java.io.IOException;
/* compiled from: FloatParser.java */
/* loaded from: classes.dex */
public class i implements h0<Float> {
    public static final i a = new i();

    @Override // b.c.a.a0.h0
    public Float a(c cVar, float f) throws IOException {
        return Float.valueOf(p.d(cVar) * f);
    }
}
