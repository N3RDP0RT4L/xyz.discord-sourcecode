package b.c.a.a0;

import android.graphics.Rect;
import androidx.collection.LongSparseArray;
import androidx.collection.SparseArrayCompat;
import b.c.a.a0.i0.c;
import b.c.a.b0.g;
import b.c.a.d;
import b.c.a.k;
import b.c.a.y.e;
import b.c.a.y.i;
import b.c.a.y.l.m;
import b.c.a.y.m.e;
import com.discord.models.domain.ModelAuditLogEntry;
import com.google.android.material.shadow.ShadowDrawableWrapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
/* compiled from: LottieCompositionMoshiParser.java */
/* loaded from: classes.dex */
public class t {
    public static final c.a a = c.a.a("w", "h", "ip", "op", "fr", "v", "layers", "assets", "fonts", "chars", "markers");

    /* renamed from: b  reason: collision with root package name */
    public static c.a f339b = c.a.a(ModelAuditLogEntry.CHANGE_KEY_ID, "layers", "w", "h", "p", "u");
    public static final c.a c = c.a.a("list");
    public static final c.a d = c.a.a("cm", "tm", "dr");

    public static d a(c cVar) throws IOException {
        ArrayList arrayList;
        SparseArrayCompat<e> sparseArrayCompat;
        ArrayList arrayList2;
        float f;
        float f2;
        float f3;
        float c2 = g.c();
        LongSparseArray<b.c.a.y.m.e> longSparseArray = new LongSparseArray<>();
        ArrayList arrayList3 = new ArrayList();
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        HashMap hashMap3 = new HashMap();
        ArrayList arrayList4 = new ArrayList();
        SparseArrayCompat<e> sparseArrayCompat2 = new SparseArrayCompat<>();
        d dVar = new d();
        cVar.b();
        float f4 = 0.0f;
        int i = 0;
        int i2 = 0;
        float f5 = 0.0f;
        float f6 = 0.0f;
        while (cVar.e()) {
            float f7 = f4;
            switch (cVar.y(a)) {
                case 0:
                    i = cVar.q();
                    break;
                case 1:
                    i2 = cVar.q();
                    break;
                case 2:
                    arrayList = arrayList4;
                    sparseArrayCompat = sparseArrayCompat2;
                    f5 = (float) cVar.n();
                    f4 = f7;
                    arrayList4 = arrayList;
                    sparseArrayCompat2 = sparseArrayCompat;
                case 3:
                    arrayList = arrayList4;
                    sparseArrayCompat = sparseArrayCompat2;
                    f6 = ((float) cVar.n()) - 0.01f;
                    f4 = f7;
                    arrayList4 = arrayList;
                    sparseArrayCompat2 = sparseArrayCompat;
                case 4:
                    arrayList = arrayList4;
                    sparseArrayCompat = sparseArrayCompat2;
                    f4 = (float) cVar.n();
                    arrayList4 = arrayList;
                    sparseArrayCompat2 = sparseArrayCompat;
                case 5:
                    arrayList2 = arrayList4;
                    sparseArrayCompat = sparseArrayCompat2;
                    f = f5;
                    f2 = f6;
                    String[] split = cVar.t().split("\\.");
                    int parseInt = Integer.parseInt(split[0]);
                    boolean z2 = true;
                    int parseInt2 = Integer.parseInt(split[1]);
                    int parseInt3 = Integer.parseInt(split[2]);
                    if (parseInt < 4 || (parseInt <= 4 && (parseInt2 < 4 || (parseInt2 <= 4 && parseInt3 < 0)))) {
                        z2 = false;
                    }
                    if (!z2) {
                        dVar.a("Lottie only supports bodymovin >= 4.4.0");
                    }
                    f4 = f7;
                    f5 = f;
                    arrayList4 = arrayList2;
                    f6 = f2;
                    sparseArrayCompat2 = sparseArrayCompat;
                case 6:
                    arrayList2 = arrayList4;
                    sparseArrayCompat = sparseArrayCompat2;
                    f = f5;
                    f2 = f6;
                    cVar.a();
                    int i3 = 0;
                    while (cVar.e()) {
                        b.c.a.y.m.e a2 = s.a(cVar, dVar);
                        if (a2.e == e.a.IMAGE) {
                            i3++;
                        }
                        arrayList3.add(a2);
                        longSparseArray.put(a2.d, a2);
                        if (i3 > 4) {
                            b.c.a.b0.c.b("You have " + i3 + " images. Lottie should primarily be used with shapes. If you are using Adobe Illustrator, convert the Illustrator layers to shape layers.");
                        }
                    }
                    cVar.c();
                    f4 = f7;
                    f5 = f;
                    arrayList4 = arrayList2;
                    f6 = f2;
                    sparseArrayCompat2 = sparseArrayCompat;
                case 7:
                    arrayList2 = arrayList4;
                    sparseArrayCompat = sparseArrayCompat2;
                    f = f5;
                    f2 = f6;
                    cVar.a();
                    while (cVar.e()) {
                        ArrayList arrayList5 = new ArrayList();
                        LongSparseArray longSparseArray2 = new LongSparseArray();
                        cVar.b();
                        String str = null;
                        String str2 = null;
                        String str3 = null;
                        int i4 = 0;
                        int i5 = 0;
                        while (cVar.e()) {
                            int y2 = cVar.y(f339b);
                            if (y2 == 0) {
                                str = cVar.t();
                            } else if (y2 == 1) {
                                cVar.a();
                                while (cVar.e()) {
                                    b.c.a.y.m.e a3 = s.a(cVar, dVar);
                                    longSparseArray2.put(a3.d, a3);
                                    arrayList5.add(a3);
                                }
                                cVar.c();
                            } else if (y2 == 2) {
                                i4 = cVar.q();
                            } else if (y2 == 3) {
                                i5 = cVar.q();
                            } else if (y2 == 4) {
                                str2 = cVar.t();
                            } else if (y2 != 5) {
                                cVar.A();
                                cVar.C();
                            } else {
                                str3 = cVar.t();
                            }
                        }
                        cVar.d();
                        if (str2 != null) {
                            hashMap2.put(str, new k(i4, i5, str, str2, str3));
                        } else {
                            hashMap.put(str, arrayList5);
                        }
                    }
                    cVar.c();
                    f4 = f7;
                    f5 = f;
                    arrayList4 = arrayList2;
                    f6 = f2;
                    sparseArrayCompat2 = sparseArrayCompat;
                case 8:
                    f = f5;
                    f2 = f6;
                    cVar.b();
                    while (cVar.e()) {
                        if (cVar.y(c) != 0) {
                            cVar.A();
                            cVar.C();
                        } else {
                            cVar.a();
                            while (cVar.e()) {
                                c.a aVar = k.a;
                                cVar.b();
                                String str4 = null;
                                String str5 = null;
                                String str6 = null;
                                float f8 = 0.0f;
                                while (cVar.e()) {
                                    ArrayList arrayList6 = arrayList4;
                                    int y3 = cVar.y(k.a);
                                    if (y3 != 0) {
                                        sparseArrayCompat2 = sparseArrayCompat2;
                                        if (y3 == 1) {
                                            str5 = cVar.t();
                                        } else if (y3 == 2) {
                                            str6 = cVar.t();
                                        } else if (y3 != 3) {
                                            cVar.A();
                                            cVar.C();
                                        } else {
                                            f8 = (float) cVar.n();
                                        }
                                        arrayList4 = arrayList6;
                                    } else {
                                        str4 = cVar.t();
                                        arrayList4 = arrayList6;
                                    }
                                }
                                arrayList4 = arrayList4;
                                cVar.d();
                                hashMap3.put(str5, new b.c.a.y.d(str4, str5, str6, f8));
                            }
                            cVar.c();
                        }
                    }
                    arrayList2 = arrayList4;
                    sparseArrayCompat = sparseArrayCompat2;
                    cVar.d();
                    f4 = f7;
                    f5 = f;
                    arrayList4 = arrayList2;
                    f6 = f2;
                    sparseArrayCompat2 = sparseArrayCompat;
                case 9:
                    f = f5;
                    f2 = f6;
                    cVar.a();
                    while (cVar.e()) {
                        c.a aVar2 = j.a;
                        ArrayList arrayList7 = new ArrayList();
                        cVar.b();
                        double d2 = ShadowDrawableWrapper.COS_45;
                        double d3 = 0.0d;
                        String str7 = null;
                        String str8 = null;
                        char c3 = 0;
                        while (cVar.e()) {
                            int y4 = cVar.y(j.a);
                            if (y4 == 0) {
                                c3 = cVar.t().charAt(0);
                            } else if (y4 == 1) {
                                d2 = cVar.n();
                            } else if (y4 == 2) {
                                d3 = cVar.n();
                            } else if (y4 == 3) {
                                str7 = cVar.t();
                            } else if (y4 == 4) {
                                str8 = cVar.t();
                            } else if (y4 != 5) {
                                cVar.A();
                                cVar.C();
                            } else {
                                cVar.b();
                                while (cVar.e()) {
                                    if (cVar.y(j.f334b) != 0) {
                                        cVar.A();
                                        cVar.C();
                                    } else {
                                        cVar.a();
                                        while (cVar.e()) {
                                            arrayList7.add((m) g.a(cVar, dVar));
                                        }
                                        cVar.c();
                                    }
                                }
                                cVar.d();
                            }
                        }
                        cVar.d();
                        b.c.a.y.e eVar = new b.c.a.y.e(arrayList7, c3, d2, d3, str7, str8);
                        sparseArrayCompat2.put(eVar.hashCode(), eVar);
                    }
                    cVar.c();
                    arrayList2 = arrayList4;
                    sparseArrayCompat = sparseArrayCompat2;
                    f4 = f7;
                    f5 = f;
                    arrayList4 = arrayList2;
                    f6 = f2;
                    sparseArrayCompat2 = sparseArrayCompat;
                case 10:
                    cVar.a();
                    while (cVar.e()) {
                        cVar.b();
                        String str9 = null;
                        float f9 = 0.0f;
                        float f10 = 0.0f;
                        while (cVar.e()) {
                            int y5 = cVar.y(d);
                            if (y5 != 0) {
                                float f11 = f6;
                                if (y5 == 1) {
                                    f3 = f5;
                                    f9 = (float) cVar.n();
                                } else if (y5 != 2) {
                                    cVar.A();
                                    cVar.C();
                                    f6 = f11;
                                } else {
                                    f3 = f5;
                                    f10 = (float) cVar.n();
                                }
                                f6 = f11;
                                f5 = f3;
                            } else {
                                str9 = cVar.t();
                            }
                        }
                        f5 = f5;
                        f6 = f6;
                        cVar.d();
                        arrayList4.add(new i(str9, f9, f10));
                    }
                    f = f5;
                    f2 = f6;
                    cVar.c();
                    arrayList2 = arrayList4;
                    sparseArrayCompat = sparseArrayCompat2;
                    f4 = f7;
                    f5 = f;
                    arrayList4 = arrayList2;
                    f6 = f2;
                    sparseArrayCompat2 = sparseArrayCompat;
                default:
                    arrayList2 = arrayList4;
                    sparseArrayCompat = sparseArrayCompat2;
                    f = f5;
                    f2 = f6;
                    cVar.A();
                    cVar.C();
                    f4 = f7;
                    f5 = f;
                    arrayList4 = arrayList2;
                    f6 = f2;
                    sparseArrayCompat2 = sparseArrayCompat;
            }
            f4 = f7;
        }
        dVar.j = new Rect(0, 0, (int) (i * c2), (int) (i2 * c2));
        dVar.k = f5;
        dVar.l = f6;
        dVar.m = f4;
        dVar.i = arrayList3;
        dVar.h = longSparseArray;
        dVar.c = hashMap;
        dVar.d = hashMap2;
        dVar.g = sparseArrayCompat2;
        dVar.e = hashMap3;
        dVar.f = arrayList4;
        return dVar;
    }
}
