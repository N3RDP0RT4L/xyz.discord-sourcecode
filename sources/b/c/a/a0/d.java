package b.c.a.a0;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.PowerManager;
import android.os.Process;
import android.os.Trace;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RecentlyNonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.text.TextUtilsCompat;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import b.c.a.a0.i0.c;
import b.c.a.b0.g;
import b.f.d.d.i;
import b.f.g.e.b0;
import b.f.g.e.d0;
import b.f.g.e.p;
import b.f.g.e.r;
import b.f.g.e.s;
import b.f.g.e.t;
import b.f.g.e.u;
import b.f.g.e.v;
import b.f.g.e.w;
import b.f.g.e.x;
import b.f.g.e.z;
import b.f.j.d.e;
import b.f.m.h;
import b.g.a.a.p;
import b.g.a.c.c0.e0;
import b.g.a.c.h0.m;
import b.g.a.c.j;
import b.i.a.b.i.d;
import b.i.a.c.b3.b;
import b.i.a.c.b3.t.f;
import b.i.a.c.x2.o;
import com.adjust.sdk.Constants;
import com.discord.api.stageinstance.StageInstance;
import com.discord.api.stageinstance.StageInstancePrivacyLevel;
import com.discord.api.voice.state.StageRequestToSpeakState;
import com.discord.api.voice.state.VoiceState;
import com.discord.app.AppPermissionsRequests;
import com.discord.i18n.RenderContext;
import com.discord.player.MediaSource;
import com.discord.player.MediaType;
import com.discord.simpleast.core.node.Node;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.logging.LoggingProvider;
import com.facebook.cache.common.CacheKey;
import com.facebook.common.file.FileUtils$CreateDirectoryException;
import com.facebook.common.file.FileUtils$FileDeleteException;
import com.facebook.common.file.FileUtils$RenameException;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.drawable.ScalingUtils$ScaleType;
import com.facebook.soloader.SysUtil$LollipopSysdeps;
import com.facebook.soloader.SysUtil$MarshmallowSysdeps;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader$ParseException;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.material.shadow.ShadowDrawableWrapper;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import d0.t.k;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ScheduledExecutorService;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import org.checkerframework.checker.nullness.qual.EnsuresNonNull;
import org.checkerframework.dataflow.qual.Pure;
import org.objectweb.asm.Opcodes;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
/* compiled from: AnimatableValueParser.java */
/* loaded from: classes.dex */
public class d {
    public static boolean a;

    /* renamed from: b  reason: collision with root package name */
    public static ScheduledExecutorService f325b;
    @Nullable
    public static Boolean c;
    @Nullable
    public static Boolean d;
    @Nullable
    public static Boolean e;
    @Nullable
    public static Boolean f;

    /* compiled from: ArrayBuilders.java */
    /* loaded from: classes2.dex */
    public static class a {
        public final /* synthetic */ Class a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ int f326b;
        public final /* synthetic */ Object c;

        public a(Class cls, int i, Object obj) {
            this.a = cls;
            this.f326b = i;
            this.c = obj;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(b.g.a.c.i0.d.o(obj, this.a) && Array.getLength(obj) == this.f326b)) {
                return false;
            }
            for (int i = 0; i < this.f326b; i++) {
                Object obj2 = Array.get(this.c, i);
                Object obj3 = Array.get(obj, i);
                if (!(obj2 == obj3 || obj2 == null || obj2.equals(obj3))) {
                    return false;
                }
            }
            return true;
        }
    }

    @NonNull
    @EnsuresNonNull({"#1"})
    public static <T> T A(@Nullable T t) {
        Objects.requireNonNull(t, "null reference");
        return t;
    }

    public static ScalingUtils$ScaleType A0(TypedArray typedArray, int i) {
        switch (typedArray.getInt(i, -2)) {
            case -1:
                return null;
            case 0:
                ScalingUtils$ScaleType scalingUtils$ScaleType = ScalingUtils$ScaleType.a;
                return z.l;
            case 1:
                ScalingUtils$ScaleType scalingUtils$ScaleType2 = ScalingUtils$ScaleType.a;
                return x.l;
            case 2:
                ScalingUtils$ScaleType scalingUtils$ScaleType3 = ScalingUtils$ScaleType.a;
                return v.l;
            case 3:
                ScalingUtils$ScaleType scalingUtils$ScaleType4 = ScalingUtils$ScaleType.a;
                return w.l;
            case 4:
                ScalingUtils$ScaleType scalingUtils$ScaleType5 = ScalingUtils$ScaleType.a;
                return r.l;
            case 5:
                ScalingUtils$ScaleType scalingUtils$ScaleType6 = ScalingUtils$ScaleType.a;
                return t.l;
            case 6:
                ScalingUtils$ScaleType scalingUtils$ScaleType7 = ScalingUtils$ScaleType.a;
                return s.l;
            case 7:
                ScalingUtils$ScaleType scalingUtils$ScaleType8 = ScalingUtils$ScaleType.a;
                return b0.l;
            case 8:
                ScalingUtils$ScaleType scalingUtils$ScaleType9 = ScalingUtils$ScaleType.a;
                return u.l;
            default:
                throw new RuntimeException("XML attribute not specified!");
        }
    }

    public static boolean A1(e0 e0Var, j jVar, Type type) {
        if (!jVar.B(e0Var.a(type)._class)) {
            return false;
        }
        ParameterizedType h1 = h1(type);
        if (h1 == null || !Objects.equals(jVar._class, h1.getRawType())) {
            return true;
        }
        Type[] actualTypeArguments = h1.getActualTypeArguments();
        m j = jVar.j();
        if (j.j() != actualTypeArguments.length) {
            return false;
        }
        for (int i = 0; i < j.j(); i++) {
            if (!A1(e0Var, j.f(i), actualTypeArguments[i])) {
                return false;
            }
        }
        return true;
    }

    public static void A2(Parcel parcel, int i) {
        int dataPosition = parcel.dataPosition();
        parcel.setDataPosition(i - 4);
        parcel.writeInt(dataPosition - i);
        parcel.setDataPosition(dataPosition);
    }

    public static void B(boolean z2) {
        if (!z2) {
            throw new IllegalStateException();
        }
    }

    @NonNull
    public static String B0(int i) {
        switch (i) {
            case -1:
                return "SUCCESS_CACHE";
            case 0:
                return "SUCCESS";
            case 1:
            case 9:
            case 11:
            case 12:
            default:
                return b.d.b.a.a.f(32, "unknown status code: ", i);
            case 2:
                return "SERVICE_VERSION_UPDATE_REQUIRED";
            case 3:
                return "SERVICE_DISABLED";
            case 4:
                return "SIGN_IN_REQUIRED";
            case 5:
                return "INVALID_ACCOUNT";
            case 6:
                return "RESOLUTION_REQUIRED";
            case 7:
                return "NETWORK_ERROR";
            case 8:
                return "INTERNAL_ERROR";
            case 10:
                return "DEVELOPER_ERROR";
            case 13:
                return "ERROR";
            case 14:
                return "INTERRUPTED";
            case 15:
                return "TIMEOUT";
            case 16:
                return "CANCELED";
            case 17:
                return "API_NOT_CONNECTED";
            case 18:
                return "DEAD_CLIENT";
            case 19:
                return "REMOTE_EXCEPTION";
            case 20:
                return "CONNECTION_SUSPENDED_DURING_CALL";
            case 21:
                return "RECONNECTION_TIMED_OUT_DURING_UPDATE";
            case 22:
                return "RECONNECTION_TIMED_OUT";
        }
    }

    public static int B1(InputStream inputStream, byte[] bArr, int i, int i2) throws IOException {
        if (i2 >= 0) {
            int i3 = 0;
            while (i3 < i2) {
                int read = inputStream.read(bArr, i + i3, i2 - i3);
                if (read == -1) {
                    break;
                }
                i3 += read;
            }
            return i3;
        }
        throw new IndexOutOfBoundsException("len is negative");
    }

    public static <T extends Parcelable> void B2(Parcel parcel, T t, int i) {
        int dataPosition = parcel.dataPosition();
        parcel.writeInt(1);
        int dataPosition2 = parcel.dataPosition();
        t.writeToParcel(parcel, i);
        int dataPosition3 = parcel.dataPosition();
        parcel.setDataPosition(dataPosition);
        parcel.writeInt(dataPosition3 - dataPosition2);
        parcel.setDataPosition(dataPosition3);
    }

    public static void C(boolean z2, Object obj) {
        if (!z2) {
            throw new IllegalStateException(String.valueOf(obj));
        }
    }

    public static String[] C0() {
        if (Build.VERSION.SDK_INT >= 23) {
            return SysUtil$MarshmallowSysdeps.getSupportedAbis();
        }
        return SysUtil$LollipopSysdeps.getSupportedAbis();
    }

    public static void C1(FileChannel fileChannel, ByteBuffer byteBuffer, int i, long j) throws IOException {
        int read;
        byteBuffer.position(0);
        byteBuffer.limit(i);
        while (byteBuffer.remaining() > 0 && (read = fileChannel.read(byteBuffer, j)) != -1) {
            j += read;
        }
        if (byteBuffer.remaining() <= 0) {
            byteBuffer.position(0);
            return;
        }
        throw new h("ELF file truncated");
    }

    @Pure
    public static void D(boolean z2) {
        if (!z2) {
            throw new IllegalStateException();
        }
    }

    public static String D0(String str) {
        return b.d.b.a.a.v("TransportRuntime.", str);
    }

    public static int D1(InputStream inputStream) throws IOException {
        return ((((byte) (inputStream.read() & 255)) << 16) & ItemTouchHelper.ACTION_MODE_DRAG_MASK) | ((((byte) (inputStream.read() & 255)) << 8) & 65280) | (((byte) (inputStream.read() & 255)) & 255);
    }

    @Pure
    public static void E(boolean z2, Object obj) {
        if (!z2) {
            throw new IllegalStateException(String.valueOf(obj));
        }
    }

    public static Pair<Integer, Integer> E0(InputStream inputStream) throws IOException {
        inputStream.skip(7L);
        short read = (short) (inputStream.read() & 255);
        short read2 = (short) (inputStream.read() & 255);
        short read3 = (short) (inputStream.read() & 255);
        if (read == 157 && read2 == 1 && read3 == 42) {
            return new Pair<>(Integer.valueOf(n0(inputStream)), Integer.valueOf(n0(inputStream)));
        }
        return null;
    }

    public static boolean E1(@RecentlyNonNull Parcel parcel, int i) {
        x2(parcel, i, 4);
        return parcel.readInt() != 0;
    }

    public static void F(boolean z2) {
        if (!z2) {
            throw new IllegalStateException();
        }
    }

    public static Pair<Integer, Integer> F0(InputStream inputStream) throws IOException {
        w0(inputStream);
        if (((byte) (inputStream.read() & 255)) != 47) {
            return null;
        }
        int read = ((byte) inputStream.read()) & 255;
        return new Pair<>(Integer.valueOf(((((byte) inputStream.read()) & 255) | ((read & 63) << 8)) + 1), Integer.valueOf(((((((byte) inputStream.read()) & 255) & 15) << 10) | ((((byte) inputStream.read()) & 255) << 2) | ((read & Opcodes.CHECKCAST) >> 6)) + 1));
    }

    @RecentlyNonNull
    public static IBinder F1(@RecentlyNonNull Parcel parcel, int i) {
        int M1 = M1(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (M1 == 0) {
            return null;
        }
        IBinder readStrongBinder = parcel.readStrongBinder();
        parcel.setDataPosition(dataPosition + M1);
        return readStrongBinder;
    }

    public static void G(boolean z2, @RecentlyNonNull Object obj) {
        if (!z2) {
            throw new IllegalStateException(String.valueOf(obj));
        }
    }

    public static long G0(FileChannel fileChannel, ByteBuffer byteBuffer, long j) throws IOException {
        C1(fileChannel, byteBuffer, 4, j);
        return byteBuffer.getInt() & 4294967295L;
    }

    public static int G1(@RecentlyNonNull Parcel parcel, int i) {
        x2(parcel, i, 4);
        return parcel.readInt();
    }

    @EnsuresNonNull({"#1"})
    @Pure
    public static <T> T H(@Nullable T t) {
        if (t != null) {
            return t;
        }
        throw new IllegalStateException();
    }

    public static final boolean H0(Long l, long j) {
        return l != null && (l.longValue() & j) == j;
    }

    public static long H1(@RecentlyNonNull Parcel parcel, int i) {
        x2(parcel, i, 8);
        return parcel.readLong();
    }

    public static boolean I(byte[] bArr, String str) {
        if (bArr.length != str.length()) {
            return false;
        }
        for (int i = 0; i < bArr.length; i++) {
            if (str.charAt(i) != bArr[i]) {
                return false;
            }
        }
        return true;
    }

    public static boolean I0(byte[] bArr, byte[] bArr2, int i) {
        Objects.requireNonNull(bArr);
        Objects.requireNonNull(bArr2);
        if (bArr2.length + i > bArr.length) {
            return false;
        }
        for (int i2 = 0; i2 < bArr2.length; i2++) {
            if (bArr[i + i2] != bArr2[i2]) {
                return false;
            }
        }
        return true;
    }

    public static int I1(b.i.a.c.f3.x xVar) {
        int i = 0;
        while (xVar.a() != 0) {
            int t = xVar.t();
            i += t;
            if (t != 255) {
                return i;
            }
        }
        return -1;
    }

    public static void J(long j, b.i.a.c.f3.x xVar, b.i.a.c.x2.w[] wVarArr) {
        while (true) {
            boolean z2 = true;
            if (xVar.a() > 1) {
                int I1 = I1(xVar);
                int I12 = I1(xVar);
                int i = xVar.f980b + I12;
                if (I12 == -1 || I12 > xVar.a()) {
                    Log.w("CeaUtil", "Skipping remainder of malformed SEI NAL unit.");
                    i = xVar.c;
                } else if (I1 == 4 && I12 >= 8) {
                    int t = xVar.t();
                    int y2 = xVar.y();
                    int f2 = y2 == 49 ? xVar.f() : 0;
                    int t2 = xVar.t();
                    if (y2 == 47) {
                        xVar.F(1);
                    }
                    boolean z3 = t == 181 && (y2 == 49 || y2 == 47) && t2 == 3;
                    if (y2 == 49) {
                        if (f2 != 1195456820) {
                            z2 = false;
                        }
                        z3 &= z2;
                    }
                    if (z3) {
                        K(j, xVar, wVarArr);
                    }
                }
                xVar.E(i);
            } else {
                return;
            }
        }
    }

    public static int J0(int i, int i2) {
        return ((i + 31) * 31) + i2;
    }

    public static int J1(InputStream inputStream, int i, boolean z2) throws IOException {
        int i2;
        int i3 = 0;
        for (int i4 = 0; i4 < i; i4++) {
            int read = inputStream.read();
            if (read != -1) {
                if (z2) {
                    i2 = (read & 255) << (i4 * 8);
                } else {
                    i3 <<= 8;
                    i2 = read & 255;
                }
                i3 |= i2;
            } else {
                throw new IOException("no more bytes");
            }
        }
        return i3;
    }

    public static void K(long j, b.i.a.c.f3.x xVar, b.i.a.c.x2.w[] wVarArr) {
        int t = xVar.t();
        if ((t & 64) != 0) {
            xVar.F(1);
            int i = (t & 31) * 3;
            int i2 = xVar.f980b;
            for (b.i.a.c.x2.w wVar : wVarArr) {
                xVar.E(i2);
                wVar.c(xVar, i);
                if (j != -9223372036854775807L) {
                    wVar.d(j, 1, i, 0, null);
                }
            }
        }
    }

    public static int K0(Object obj, Object obj2) {
        int i = 0;
        int hashCode = obj == null ? 0 : obj.hashCode();
        if (obj2 != null) {
            i = obj2.hashCode();
        }
        return J0(hashCode, i);
    }

    public static long K1(b.i.a.c.f3.x xVar, int i, int i2) {
        byte[] bArr;
        xVar.E(i);
        if (xVar.a() < 5) {
            return -9223372036854775807L;
        }
        int f2 = xVar.f();
        if ((8388608 & f2) != 0 || ((2096896 & f2) >> 8) != i2) {
            return -9223372036854775807L;
        }
        if (((f2 & 32) != 0) && xVar.t() >= 7 && xVar.a() >= 7) {
            if ((xVar.t() & 16) == 16) {
                System.arraycopy(xVar.a, xVar.f980b, new byte[6], 0, 6);
                xVar.f980b += 6;
                return ((bArr[0] & 255) << 25) | ((bArr[1] & 255) << 17) | ((bArr[2] & 255) << 9) | ((bArr[3] & 255) << 1) | ((bArr[4] & 255) >> 7);
            }
        }
        return -9223372036854775807L;
    }

    public static void L(Drawable drawable, Drawable drawable2) {
        if (drawable != null && drawable != drawable2) {
            drawable.setBounds(drawable2.getBounds());
            drawable.setChangingConfigurations(drawable2.getChangingConfigurations());
            drawable.setLevel(drawable2.getLevel());
            drawable.setVisible(drawable2.isVisible(), false);
            drawable.setState(drawable2.getState());
        }
    }

    public static void L0(String str, String str2) {
        Log.i(D0(str), str2);
    }

    public static o.a L1(b.i.a.c.f3.x xVar) {
        xVar.F(1);
        int v = xVar.v();
        long j = xVar.f980b + v;
        int i = v / 18;
        long[] jArr = new long[i];
        long[] jArr2 = new long[i];
        int i2 = 0;
        while (true) {
            if (i2 >= i) {
                break;
            }
            long m = xVar.m();
            if (m == -1) {
                jArr = Arrays.copyOf(jArr, i2);
                jArr2 = Arrays.copyOf(jArr2, i2);
                break;
            }
            jArr[i2] = m;
            jArr2[i2] = xVar.m();
            xVar.F(2);
            i2++;
        }
        xVar.F((int) (j - xVar.f980b));
        return new o.a(jArr, jArr2);
    }

    @RecentlyNonNull
    public static Bundle M(@RecentlyNonNull Parcel parcel, int i) {
        int M1 = M1(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (M1 == 0) {
            return null;
        }
        Bundle readBundle = parcel.readBundle();
        parcel.setDataPosition(dataPosition + M1);
        return readBundle;
    }

    public static int M0(int i) {
        int i2 = 0;
        while (i > 0) {
            i2++;
            i >>>= 1;
        }
        return i2;
    }

    public static int M1(@RecentlyNonNull Parcel parcel, int i) {
        return (i & (-65536)) != -65536 ? (char) (i >> 16) : parcel.readInt();
    }

    @RecentlyNonNull
    public static byte[] N(@RecentlyNonNull Parcel parcel, int i) {
        int M1 = M1(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (M1 == 0) {
            return null;
        }
        byte[] createByteArray = parcel.createByteArray();
        parcel.setDataPosition(dataPosition + M1);
        return createByteArray;
    }

    public static <T> DataSource<T> N0(Throwable th) {
        b.f.e.h hVar = new b.f.e.h();
        Objects.requireNonNull(th);
        hVar.k(th, null);
        return hVar;
    }

    public static b.i.a.c.x2.z N1(b.i.a.c.f3.x xVar, boolean z2, boolean z3) throws ParserException {
        if (z2) {
            n2(3, xVar, false);
        }
        String q = xVar.q((int) xVar.j());
        long j = xVar.j();
        String[] strArr = new String[(int) j];
        int length = q.length() + 11 + 4;
        for (int i = 0; i < j; i++) {
            strArr[i] = xVar.q((int) xVar.j());
            length = length + 4 + strArr[i].length();
        }
        if (!z3 || (xVar.t() & 1) != 0) {
            return new b.i.a.c.x2.z(q, strArr, length + 1);
        }
        throw ParserException.a("framing bit expected to be set", null);
    }

    @RecentlyNonNull
    public static int[] O(@RecentlyNonNull Parcel parcel, int i) {
        int M1 = M1(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (M1 == 0) {
            return null;
        }
        int[] createIntArray = parcel.createIntArray();
        parcel.setDataPosition(dataPosition + M1);
        return createIntArray;
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Code restructure failed: missing block: B:100:0x01a2, code lost:
        if (r0.equals("video/mp2t") == false) goto L107;
     */
    /* JADX WARN: Removed duplicated region for block: B:101:0x01a5  */
    /* JADX WARN: Removed duplicated region for block: B:104:0x01b1  */
    /* JADX WARN: Removed duplicated region for block: B:107:0x01bc  */
    /* JADX WARN: Removed duplicated region for block: B:109:0x01c2 A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:110:0x01c4 A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:111:0x01c6 A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:112:0x01c9 A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:113:0x01cc A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:114:0x01ce A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:115:0x01d0 A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:116:0x01d3 A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:117:0x01d6 A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:118:0x01d9 A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:119:0x01db A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:120:0x01dd A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:121:0x01e0 A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:122:0x01e3 A[ORIG_RETURN, RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:29:0x0072  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x007b  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x0085  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x0094  */
    /* JADX WARN: Removed duplicated region for block: B:42:0x00a2  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x00ae  */
    /* JADX WARN: Removed duplicated region for block: B:48:0x00ba  */
    /* JADX WARN: Removed duplicated region for block: B:51:0x00c8  */
    /* JADX WARN: Removed duplicated region for block: B:54:0x00d6  */
    /* JADX WARN: Removed duplicated region for block: B:57:0x00e5  */
    /* JADX WARN: Removed duplicated region for block: B:60:0x00f1  */
    /* JADX WARN: Removed duplicated region for block: B:63:0x00ff  */
    /* JADX WARN: Removed duplicated region for block: B:66:0x010d  */
    /* JADX WARN: Removed duplicated region for block: B:69:0x011b  */
    /* JADX WARN: Removed duplicated region for block: B:72:0x0129  */
    /* JADX WARN: Removed duplicated region for block: B:75:0x0137  */
    /* JADX WARN: Removed duplicated region for block: B:78:0x0146  */
    /* JADX WARN: Removed duplicated region for block: B:81:0x0154  */
    /* JADX WARN: Removed duplicated region for block: B:84:0x0162  */
    /* JADX WARN: Removed duplicated region for block: B:87:0x016e  */
    /* JADX WARN: Removed duplicated region for block: B:90:0x0179  */
    /* JADX WARN: Removed duplicated region for block: B:93:0x0184  */
    /* JADX WARN: Removed duplicated region for block: B:96:0x018f  */
    /* JADX WARN: Removed duplicated region for block: B:99:0x019b  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static int O0(java.util.Map<java.lang.String, java.util.List<java.lang.String>> r17) {
        /*
            Method dump skipped, instructions count: 636
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.c.a.a0.d.O0(java.util.Map):int");
    }

    public static void O1(b.C0092b bVar) {
        bVar.k = -3.4028235E38f;
        bVar.j = Integer.MIN_VALUE;
        CharSequence charSequence = bVar.a;
        if (charSequence instanceof Spanned) {
            if (!(charSequence instanceof Spannable)) {
                bVar.a = SpannableString.valueOf(charSequence);
            }
            CharSequence charSequence2 = bVar.a;
            Objects.requireNonNull(charSequence2);
            P1((Spannable) charSequence2, b.i.a.c.d3.h.a);
        }
    }

    public static final MediaSource P(MediaType mediaType, String str, String str2) {
        d0.z.d.m.checkNotNullParameter(mediaType, "mediaType");
        d0.z.d.m.checkNotNullParameter(str, "progressiveMediaUri");
        d0.z.d.m.checkNotNullParameter(str2, "featureTag");
        Uri parse = Uri.parse(str);
        d0.z.d.m.checkNotNullExpressionValue(parse, "Uri.parse(progressiveMediaUri)");
        return new MediaSource(parse, str2, mediaType);
    }

    public static int P0(Uri uri) {
        String lastPathSegment = uri.getLastPathSegment();
        if (lastPathSegment == null) {
            return -1;
        }
        if (lastPathSegment.endsWith(".ac3") || lastPathSegment.endsWith(".ec3")) {
            return 0;
        }
        if (lastPathSegment.endsWith(".ac4")) {
            return 1;
        }
        if (lastPathSegment.endsWith(".adts") || lastPathSegment.endsWith(".aac")) {
            return 2;
        }
        if (lastPathSegment.endsWith(".amr")) {
            return 3;
        }
        if (lastPathSegment.endsWith(".flac")) {
            return 4;
        }
        if (lastPathSegment.endsWith(".flv")) {
            return 5;
        }
        if (lastPathSegment.startsWith(".mk", lastPathSegment.length() - 4) || lastPathSegment.endsWith(".webm")) {
            return 6;
        }
        if (lastPathSegment.endsWith(".mp3")) {
            return 7;
        }
        if (lastPathSegment.endsWith(".mp4") || lastPathSegment.startsWith(".m4", lastPathSegment.length() - 4) || lastPathSegment.startsWith(".mp4", lastPathSegment.length() - 5) || lastPathSegment.startsWith(".cmf", lastPathSegment.length() - 5)) {
            return 8;
        }
        if (lastPathSegment.startsWith(".og", lastPathSegment.length() - 4) || lastPathSegment.endsWith(".opus")) {
            return 9;
        }
        if (lastPathSegment.endsWith(".ps") || lastPathSegment.endsWith(".mpeg") || lastPathSegment.endsWith(".mpg") || lastPathSegment.endsWith(".m2p")) {
            return 10;
        }
        if (lastPathSegment.endsWith(".ts") || lastPathSegment.startsWith(".ts", lastPathSegment.length() - 4)) {
            return 11;
        }
        if (lastPathSegment.endsWith(".wav") || lastPathSegment.endsWith(".wave")) {
            return 12;
        }
        if (lastPathSegment.endsWith(".vtt") || lastPathSegment.endsWith(".webvtt")) {
            return 13;
        }
        return (lastPathSegment.endsWith(".jpg") || lastPathSegment.endsWith(".jpeg")) ? 14 : -1;
    }

    public static void P1(Spannable spannable, b.i.b.a.h<Object> hVar) {
        Object[] spans;
        for (Object obj : spannable.getSpans(0, spannable.length(), Object.class)) {
            if (hVar.apply(obj)) {
                spannable.removeSpan(obj);
            }
        }
    }

    @RecentlyNonNull
    public static <T extends Parcelable> T Q(@RecentlyNonNull Parcel parcel, int i, @RecentlyNonNull Parcelable.Creator<T> creator) {
        int M1 = M1(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (M1 == 0) {
            return null;
        }
        T createFromParcel = creator.createFromParcel(parcel);
        parcel.setDataPosition(dataPosition + M1);
        return createFromParcel;
    }

    public static void Q0(String str) {
        throw new IllegalArgumentException(b.d.b.a.a.v("Unknown library: ", str));
    }

    public static void Q1(File file, File file2) throws FileUtils$RenameException {
        Objects.requireNonNull(file);
        file2.delete();
        if (!file.renameTo(file2)) {
            final Throwable th = null;
            if (file2.exists()) {
                th = new FileUtils$FileDeleteException(file2.getAbsolutePath());
            } else if (!file.getParentFile().exists()) {
                final String absolutePath = file.getAbsolutePath();
                th = new FileNotFoundException(absolutePath) { // from class: com.facebook.common.file.FileUtils$ParentDirNotFoundException
                };
            } else if (!file.exists()) {
                th = new FileNotFoundException(file.getAbsolutePath());
            }
            StringBuilder R = b.d.b.a.a.R("Unknown error renaming ");
            R.append(file.getAbsolutePath());
            R.append(" to ");
            R.append(file2.getAbsolutePath());
            final String sb = R.toString();
            throw new IOException(sb, th) { // from class: com.facebook.common.file.FileUtils$RenameException
                {
                    initCause(th);
                }
            };
        }
    }

    @RecentlyNonNull
    public static String R(@RecentlyNonNull Parcel parcel, int i) {
        int M1 = M1(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (M1 == 0) {
            return null;
        }
        String readString = parcel.readString();
        parcel.setDataPosition(dataPosition + M1);
        return readString;
    }

    public static boolean R0(XmlPullParser xmlPullParser, String str) throws XmlPullParserException {
        return (xmlPullParser.getEventType() == 3) && xmlPullParser.getName().equals(str);
    }

    public static final RenderContext R1(Function1<? super RenderContext, Unit> function1, Object... objArr) {
        d0.z.d.m.checkNotNullParameter(function1, "init");
        d0.z.d.m.checkNotNullParameter(objArr, "orderedArguments");
        RenderContext renderContext = new RenderContext();
        function1.invoke(renderContext);
        if (!(objArr.length == 0)) {
            if (!(!renderContext.a.isEmpty())) {
                renderContext.c = k.toList(objArr);
            } else {
                throw new IllegalArgumentException("must provide named arguments OR formatArgs, not both.");
            }
        }
        return renderContext;
    }

    @RecentlyNonNull
    public static String[] S(@RecentlyNonNull Parcel parcel, int i) {
        int M1 = M1(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (M1 == 0) {
            return null;
        }
        String[] createStringArray = parcel.createStringArray();
        parcel.setDataPosition(dataPosition + M1);
        return createStringArray;
    }

    public static boolean S0(int i, int i2, e eVar) {
        return eVar == null ? ((float) o0(i)) >= 2048.0f && o0(i2) >= 2048 : o0(i) >= eVar.a && o0(i2) >= eVar.f562b;
    }

    public static /* synthetic */ void S1(AppPermissionsRequests appPermissionsRequests, Function0 function0, Function0 function02, int i, Object obj) {
        int i2 = i & 1;
        appPermissionsRequests.requestMicrophone(null, function02);
    }

    @RecentlyNonNull
    public static ArrayList<String> T(@RecentlyNonNull Parcel parcel, int i) {
        int M1 = M1(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (M1 == 0) {
            return null;
        }
        ArrayList<String> createStringArrayList = parcel.createStringArrayList();
        parcel.setDataPosition(dataPosition + M1);
        return createStringArrayList;
    }

    public static boolean T0(b.f.j.j.e eVar, e eVar2) {
        if (eVar == null) {
            return false;
        }
        eVar.x();
        int i = eVar.m;
        if (i == 90 || i == 270) {
            eVar.x();
            int i2 = eVar.p;
            eVar.x();
            return S0(i2, eVar.o, eVar2);
        }
        eVar.x();
        int i3 = eVar.o;
        eVar.x();
        return S0(i3, eVar.p, eVar2);
    }

    @Nullable
    public static f T1(@Nullable f fVar, @Nullable String[] strArr, Map<String, f> map) {
        int i = 0;
        if (fVar == null) {
            if (strArr == null) {
                return null;
            }
            if (strArr.length == 1) {
                return map.get(strArr[0]);
            }
            if (strArr.length > 1) {
                f fVar2 = new f();
                int length = strArr.length;
                while (i < length) {
                    fVar2.a(map.get(strArr[i]));
                    i++;
                }
                return fVar2;
            }
        } else if (strArr != null && strArr.length == 1) {
            fVar.a(map.get(strArr[0]));
            return fVar;
        } else if (strArr != null && strArr.length > 1) {
            int length2 = strArr.length;
            while (i < length2) {
                fVar.a(map.get(strArr[i]));
                i++;
            }
        }
        return fVar;
    }

    @RecentlyNonNull
    public static <T> T[] U(@RecentlyNonNull Parcel parcel, int i, @RecentlyNonNull Parcelable.Creator<T> creator) {
        int M1 = M1(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (M1 == 0) {
            return null;
        }
        T[] tArr = (T[]) parcel.createTypedArray(creator);
        parcel.setDataPosition(dataPosition + M1);
        return tArr;
    }

    public static final boolean U0(Context context) {
        d0.z.d.m.checkNotNullParameter(context, "$this$isLtr");
        return TextUtilsCompat.getLayoutDirectionFromLocale(Locale.getDefault()) == 0;
    }

    public static float U1(int i, float f2, int i2, int i3) {
        float f3;
        if (f2 == -3.4028235E38f) {
            return -3.4028235E38f;
        }
        if (i == 0) {
            f3 = i3;
        } else if (i == 1) {
            f3 = i2;
        } else if (i != 2) {
            return -3.4028235E38f;
        } else {
            return f2;
        }
        return f2 * f3;
    }

    @RecentlyNonNull
    public static <T> ArrayList<T> V(@RecentlyNonNull Parcel parcel, int i, @RecentlyNonNull Parcelable.Creator<T> creator) {
        int M1 = M1(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (M1 == 0) {
            return null;
        }
        ArrayList<T> createTypedArrayList = parcel.createTypedArrayList(creator);
        parcel.setDataPosition(dataPosition + M1);
        return createTypedArrayList;
    }

    public static final boolean V0(VoiceState voiceState) {
        d0.z.d.m.checkNotNullParameter(voiceState, "$this$isMutedByAnySource");
        return voiceState.h() || voiceState.e() || voiceState.l();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v1, types: [b.i.a.b.i.d$a] */
    public static <TInput, TResult, TException extends Throwable> TResult V1(int i, TInput tinput, b.i.a.b.i.b<TInput, TResult, TException> bVar, b.i.a.b.j.s.a<TInput, TResult> aVar) throws Throwable {
        TResult tresult;
        if (i < 1) {
            return (TResult) bVar.a(tinput);
        }
        do {
            tresult = (TResult) bVar.a(tinput);
            d.a aVar2 = (d.a) tinput;
            d.b bVar2 = (d.b) tresult;
            URL url = bVar2.f742b;
            if (url != null) {
                X("CctTransportBackend", "Following redirect to: %s", url);
                tinput = new d.a(bVar2.f742b, aVar2.f741b, aVar2.c);
            } else {
                tinput = null;
            }
            if (tinput == null) {
                break;
            }
            i--;
        } while (i >= 1);
        return tresult;
    }

    public static String W(String str) {
        StringBuilder Q = b.d.b.a.a.Q(b.d.b.a.a.b(str, b.d.b.a.a.b(str, 5)), ".", str, ",.", str);
        Q.append(" *");
        return Q.toString();
    }

    public static final boolean W0(StageInstance stageInstance) {
        d0.z.d.m.checkNotNullParameter(stageInstance, "$this$isPublic");
        return stageInstance.e() == StageInstancePrivacyLevel.PUBLIC;
    }

    public static String W1(CacheKey cacheKey) throws UnsupportedEncodingException {
        byte[] bytes = cacheKey.b().getBytes(Constants.ENCODING);
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(Constants.SHA1);
            messageDigest.update(bytes, 0, bytes.length);
            return Base64.encodeToString(messageDigest.digest(), 11);
        } catch (NoSuchAlgorithmException e2) {
            throw new RuntimeException(e2);
        }
    }

    public static void X(String str, String str2, Object obj) {
        Log.d(D0(str), String.format(str2, obj));
    }

    public static final boolean X0(VoiceState voiceState) {
        d0.z.d.m.checkNotNullParameter(voiceState, "$this$isRemoveSignal");
        return voiceState.a() == null;
    }

    public static void X1(Drawable drawable, Drawable.Callback callback, b.f.g.e.e0 e0Var) {
        if (drawable != null) {
            drawable.setCallback(callback);
            if (drawable instanceof d0) {
                ((d0) drawable).b(e0Var);
            }
        }
    }

    public static void Y(String str, String str2, Object... objArr) {
        Log.d(D0(str), String.format(str2, objArr));
    }

    public static boolean Y0(XmlPullParser xmlPullParser, String str) throws XmlPullParserException {
        return (xmlPullParser.getEventType() == 2) && xmlPullParser.getName().equals(str);
    }

    public static void Y1(MediaFormat mediaFormat, List<byte[]> list) {
        for (int i = 0; i < list.size(); i++) {
            mediaFormat.setByteBuffer(b.d.b.a.a.f(15, "csd-", i), ByteBuffer.wrap(list.get(i)));
        }
    }

    public static boolean Z(File file) {
        File[] listFiles;
        if (file.isDirectory() && (listFiles = file.listFiles()) != null) {
            for (File file2 : listFiles) {
                Z(file2);
            }
        }
        return file.delete();
    }

    @TargetApi(20)
    public static boolean Z0(@RecentlyNonNull Context context) {
        PackageManager packageManager = context.getPackageManager();
        if (c == null) {
            c = Boolean.valueOf(packageManager.hasSystemFeature("android.hardware.type.watch"));
        }
        return c.booleanValue();
    }

    public static void Z1(Drawable drawable, b.f.g.e.e eVar) {
        if (drawable != null && eVar != null) {
            int i = eVar.a;
            if (i != -1) {
                drawable.setAlpha(i);
            }
            if (eVar.f502b) {
                drawable.setColorFilter(eVar.c);
            }
            int i2 = eVar.d;
            boolean z2 = false;
            if (i2 != -1) {
                drawable.setDither(i2 != 0);
            }
            int i3 = eVar.e;
            if (i3 != -1) {
                if (i3 != 0) {
                    z2 = true;
                }
                drawable.setFilterBitmap(z2);
            }
        }
    }

    public static float a(float f2) {
        return f2 <= 0.04045f ? f2 / 12.92f : (float) Math.pow((f2 + 0.055f) / 1.055f, 2.4000000953674316d);
    }

    /* JADX WARN: Removed duplicated region for block: B:40:0x009c  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x00b2  */
    /* JADX WARN: Removed duplicated region for block: B:53:0x00dd  */
    /* JADX WARN: Removed duplicated region for block: B:54:0x00e0  */
    /* JADX WARN: Removed duplicated region for block: B:57:0x00ea  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static int a0(b.f.j.d.f r14, b.f.j.d.e r15, b.f.j.j.e r16, int r17) {
        /*
            Method dump skipped, instructions count: 255
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.c.a.a0.d.a0(b.f.j.d.f, b.f.j.d.e, b.f.j.j.e, int):int");
    }

    @TargetApi(26)
    public static boolean a1(@RecentlyNonNull Context context) {
        if (!Z0(context)) {
            return false;
        }
        if (!(Build.VERSION.SDK_INT >= 24)) {
            return true;
        }
        if (d == null) {
            d = Boolean.valueOf(context.getPackageManager().hasSystemFeature("cn.google"));
        }
        return d.booleanValue() && !b.i.a.f.e.o.f.A0();
    }

    public static <TResult> void a2(Status status, @Nullable TResult tresult, TaskCompletionSource<TResult> taskCompletionSource) {
        if (status.w0()) {
            taskCompletionSource.a.s(tresult);
            return;
        }
        taskCompletionSource.a.t(new ApiException(status));
    }

    public static float b(float f2) {
        return f2 <= 0.0031308f ? f2 * 12.92f : (float) ((Math.pow(f2, 0.4166666567325592d) * 1.0549999475479126d) - 0.054999999701976776d);
    }

    public static void b0(File file) throws IOException {
        if (file.isDirectory()) {
            File[] listFiles = file.listFiles();
            if (listFiles != null) {
                for (File file2 : listFiles) {
                    b0(file2);
                }
            } else {
                return;
            }
        }
        File parentFile = file.getParentFile();
        if (parentFile != null && !parentFile.canWrite() && !parentFile.setWritable(true)) {
            Log.e("SysUtil", "Enable write permission failed: " + parentFile);
        }
        if (!file.delete() && file.exists()) {
            throw new IOException("Could not delete file " + file);
        }
    }

    public static final void b1(String str, String str2) {
        d0.z.d.m.checkNotNullParameter(str, "tag");
        d0.z.d.m.checkNotNullParameter(str2, NotificationCompat.CATEGORY_MESSAGE);
        Logger.d$default(LoggingProvider.INSTANCE.get(), str, str2, null, 4, null);
    }

    public static boolean b2(Object obj, Collection<String> collection, Collection<String> collection2) {
        if (collection == null && collection2 == null) {
            return false;
        }
        if (collection2 == null) {
            return collection.contains(obj);
        }
        if (collection == null) {
            return !collection2.contains(obj);
        }
        return !collection2.contains(obj) || collection.contains(obj);
    }

    public static void c(Spannable spannable, Object obj, int i, int i2, int i3) {
        Object[] spans;
        for (Object obj2 : spannable.getSpans(i, i2, obj.getClass())) {
            if (spannable.getSpanStart(obj2) == i && spannable.getSpanEnd(obj2) == i2 && spannable.getSpanFlags(obj2) == i3) {
                spannable.removeSpan(obj2);
            }
        }
        spannable.setSpan(obj, i, i2, i3);
    }

    public static void c0(String str, String str2, Throwable th) {
        Log.e(D0(str), str2, th);
    }

    public static final void c1(String str, String str2, Throwable th) {
        d0.z.d.m.checkNotNullParameter(str, "tag");
        d0.z.d.m.checkNotNullParameter(str2, NotificationCompat.CATEGORY_MESSAGE);
        Logger.e$default(LoggingProvider.INSTANCE.get(), str, str2, th, null, 8, null);
    }

    public static long c2(InputStream inputStream, long j) throws IOException {
        i(Boolean.valueOf(j >= 0));
        long j2 = j;
        while (j2 > 0) {
            long skip = inputStream.skip(j2);
            if (skip <= 0) {
                if (inputStream.read() == -1) {
                    return j - j2;
                }
                skip = 1;
            }
            j2 -= skip;
        }
        return j;
    }

    public static int d(int i, int i2, int i3) {
        return Math.min(Math.max(0, i3 - i), i2);
    }

    public static void d0() {
        if (b.i.a.c.f3.e0.a >= 18) {
            Trace.endSection();
        }
    }

    public static final void d1(String str, String str2) {
        d0.z.d.m.checkNotNullParameter(str, "tag");
        d0.z.d.m.checkNotNullParameter(str2, NotificationCompat.CATEGORY_MESSAGE);
        Logger.i$default(LoggingProvider.INSTANCE.get(), str, str2, null, 4, null);
    }

    public static void d2(@RecentlyNonNull Parcel parcel, int i) {
        parcel.setDataPosition(parcel.dataPosition() + M1(parcel, i));
    }

    public static byte[] e(String str) {
        try {
            return str.getBytes("ASCII");
        } catch (UnsupportedEncodingException e2) {
            throw new RuntimeException("ASCII not found!", e2);
        }
    }

    public static synchronized void e0() {
        synchronized (d.class) {
            if (!a) {
                b.f.m.n.a.c("native-imagetranscoder");
                a = true;
            }
        }
    }

    public static final void e1(String str, String str2) {
        d0.z.d.m.checkNotNullParameter(str, "tag");
        d0.z.d.m.checkNotNullParameter(str2, NotificationCompat.CATEGORY_MESSAGE);
        Logger.v$default(LoggingProvider.INSTANCE.get(), str, str2, null, 4, null);
    }

    public static final <P extends Parcelable> Bundle e2(P p) {
        d0.z.d.m.checkNotNullParameter(p, "$this$toBundle");
        Bundle bundle = new Bundle();
        bundle.putParcelable("intent_args_key", p);
        return bundle;
    }

    public static void f(String str) {
        if (b.i.a.c.f3.e0.a >= 18) {
            Trace.beginSection(str);
        }
    }

    public static void f0(@RecentlyNonNull Parcel parcel, int i) {
        if (parcel.dataPosition() != i) {
            throw new SafeParcelReader$ParseException(b.d.b.a.a.f(37, "Overread allowed size end=", i), parcel);
        }
    }

    public static final void f1(String str, String str2) {
        d0.z.d.m.checkNotNullParameter(str, "tag");
        d0.z.d.m.checkNotNullParameter(str2, NotificationCompat.CATEGORY_MESSAGE);
        Logger.w$default(LoggingProvider.INSTANCE.get(), str, str2, null, 4, null);
    }

    public static String f2(@ColorInt int i) {
        return b.i.a.c.f3.e0.k("rgba(%d,%d,%d,%.3f)", Integer.valueOf(Color.red(i)), Integer.valueOf(Color.green(i)), Integer.valueOf(Color.blue(i)), Double.valueOf(Color.alpha(i) / 255.0d));
    }

    public static List<byte[]> g(byte[] bArr) {
        ArrayList arrayList = new ArrayList(3);
        arrayList.add(bArr);
        arrayList.add(h(((((bArr[11] & 255) << 8) | (bArr[10] & 255)) * 1000000000) / 48000));
        arrayList.add(h(80000000L));
        return arrayList;
    }

    public static boolean g0(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public static boolean g1(b.f.j.t.a aVar, CloseableReference<Bitmap> closeableReference) {
        return false;
    }

    public static final <P extends Parcelable> Intent g2(P p) {
        d0.z.d.m.checkNotNullParameter(p, "$this$toIntent");
        Intent putExtra = new Intent().putExtra("intent_args_key", p);
        d0.z.d.m.checkNotNullExpressionValue(putExtra, "Intent().putExtra(INTENT_ARGS, this)");
        return putExtra;
    }

    public static byte[] h(long j) {
        return ByteBuffer.allocate(8).order(ByteOrder.nativeOrder()).putLong(j).array();
    }

    public static boolean h0(@Nullable Object obj, @Nullable Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public static ParameterizedType h1(Type type) {
        if (type instanceof ParameterizedType) {
            return (ParameterizedType) type;
        }
        if (type instanceof WildcardType) {
            WildcardType wildcardType = (WildcardType) type;
            if (wildcardType.getLowerBounds().length != 0) {
                return null;
            }
            Type[] upperBounds = wildcardType.getUpperBounds();
            if (upperBounds.length == 1) {
                return h1(upperBounds[0]);
            }
        }
        return null;
    }

    public static i h2(Object obj) {
        return new i(obj.getClass().getSimpleName(), null);
    }

    public static void i(Boolean bool) {
        if (bool != null && !bool.booleanValue()) {
            throw new IllegalArgumentException();
        }
    }

    public static int i0(float f2, int i, int i2) {
        if (i == i2) {
            return i;
        }
        float f3 = ((i >> 24) & 255) / 255.0f;
        float a2 = a(((i >> 16) & 255) / 255.0f);
        float a3 = a(((i >> 8) & 255) / 255.0f);
        float a4 = a((i & 255) / 255.0f);
        float a5 = a(((i2 >> 16) & 255) / 255.0f);
        float a6 = a(((i2 >> 8) & 255) / 255.0f);
        float a7 = a((i2 & 255) / 255.0f);
        float a8 = b.d.b.a.a.a(((i2 >> 24) & 255) / 255.0f, f3, f2, f3);
        float a9 = b.d.b.a.a.a(a5, a2, f2, a2);
        float a10 = b.d.b.a.a.a(a6, a3, f2, a3);
        float a11 = b.d.b.a.a.a(a7, a4, f2, a4);
        int round = Math.round(b(a9) * 255.0f) << 16;
        return Math.round(b(a11) * 255.0f) | round | (Math.round(a8 * 255.0f) << 24) | (Math.round(b(a10) * 255.0f) << 8);
    }

    public static TypeVariable<?> i1(Type type) {
        if (type instanceof TypeVariable) {
            return (TypeVariable) type;
        }
        if (type instanceof WildcardType) {
            WildcardType wildcardType = (WildcardType) type;
            if (wildcardType.getLowerBounds().length != 0) {
                return null;
            }
            Type[] upperBounds = wildcardType.getUpperBounds();
            if (upperBounds.length == 1) {
                return i1(upperBounds[0]);
            }
        }
        return null;
    }

    public static void i2(Collection<? extends Node> collection, b.a.t.b.c.a aVar) {
        for (Node node : collection) {
            j2(node, aVar);
        }
    }

    @Pure
    public static void j(boolean z2) {
        if (!z2) {
            throw new IllegalArgumentException();
        }
    }

    public static String[] j0(FileChannel fileChannel) throws IOException {
        long j;
        long j2;
        long j3;
        long j4;
        long j5;
        long j6;
        long j7;
        long j8;
        long j9;
        long j10;
        long j11;
        long j12;
        long j13;
        long j14;
        long j15;
        long j16;
        int i = 8;
        ByteBuffer allocate = ByteBuffer.allocate(8);
        allocate.order(ByteOrder.LITTLE_ENDIAN);
        long G0 = G0(fileChannel, allocate, 0L);
        if (G0 == 1179403647) {
            C1(fileChannel, allocate, 1, 4L);
            boolean z2 = ((short) (allocate.get() & 255)) == 1;
            C1(fileChannel, allocate, 1, 5L);
            if (((short) (allocate.get() & 255)) == 2) {
                allocate.order(ByteOrder.BIG_ENDIAN);
            }
            if (z2) {
                j = G0(fileChannel, allocate, 28L);
            } else {
                C1(fileChannel, allocate, 8, 32L);
                j = allocate.getLong();
            }
            long j17 = 44;
            if (!z2) {
                j17 = 56;
            }
            C1(fileChannel, allocate, 2, j17);
            long j18 = allocate.getShort() & 65535;
            C1(fileChannel, allocate, 2, z2 ? 42L : 54L);
            int i2 = 65535 & allocate.getShort();
            if (j18 == 65535) {
                if (z2) {
                    j16 = G0(fileChannel, allocate, 32L);
                } else {
                    C1(fileChannel, allocate, 8, 40L);
                    j16 = allocate.getLong();
                }
                if (z2) {
                    j18 = G0(fileChannel, allocate, j16 + 28);
                } else {
                    j18 = G0(fileChannel, allocate, j16 + 44);
                }
            }
            long j19 = 0;
            long j20 = j;
            while (true) {
                if (j19 >= j18) {
                    j2 = 0;
                    break;
                }
                if (z2) {
                    j15 = G0(fileChannel, allocate, j20 + 0);
                } else {
                    j15 = G0(fileChannel, allocate, 0 + j20);
                }
                if (j15 != 2) {
                    j20 += i2;
                    j19++;
                } else if (z2) {
                    j2 = G0(fileChannel, allocate, j20 + 4);
                } else {
                    C1(fileChannel, allocate, 8, j20 + 8);
                    j2 = allocate.getLong();
                }
            }
            long j21 = 0;
            if (j2 != 0) {
                int i3 = 0;
                long j22 = j2;
                long j23 = 0;
                while (true) {
                    long j24 = j22 + j21;
                    if (z2) {
                        j3 = G0(fileChannel, allocate, j24);
                    } else {
                        C1(fileChannel, allocate, i, j24);
                        j3 = allocate.getLong();
                    }
                    if (j3 == 1) {
                        j4 = j2;
                        if (i3 != Integer.MAX_VALUE) {
                            i3++;
                        } else {
                            throw new h("malformed DT_NEEDED section");
                        }
                    } else {
                        j4 = j2;
                        if (j3 == 5) {
                            if (z2) {
                                j14 = G0(fileChannel, allocate, j22 + 4);
                            } else {
                                C1(fileChannel, allocate, 8, j22 + 8);
                                j14 = allocate.getLong();
                            }
                            j23 = j14;
                        }
                    }
                    long j25 = 16;
                    j22 += z2 ? 8L : 16L;
                    long j26 = 0;
                    if (j3 != 0) {
                        i = 8;
                        j2 = j4;
                        j21 = 0;
                    } else if (j23 != 0) {
                        int i4 = 0;
                        int i5 = i3;
                        while (true) {
                            if (i4 >= j18) {
                                j5 = 0;
                                break;
                            }
                            if (z2) {
                                j9 = G0(fileChannel, allocate, j + j26);
                            } else {
                                j9 = G0(fileChannel, allocate, j + j26);
                            }
                            if (j9 == 1) {
                                if (z2) {
                                    j11 = G0(fileChannel, allocate, j + 8);
                                } else {
                                    C1(fileChannel, allocate, 8, j25 + j);
                                    j11 = allocate.getLong();
                                }
                                if (z2) {
                                    j12 = G0(fileChannel, allocate, 20 + j);
                                    j10 = j18;
                                } else {
                                    j10 = j18;
                                    C1(fileChannel, allocate, 8, 40 + j);
                                    j12 = allocate.getLong();
                                }
                                if (j11 <= j23 && j23 < j12 + j11) {
                                    if (z2) {
                                        j13 = G0(fileChannel, allocate, j + 4);
                                    } else {
                                        C1(fileChannel, allocate, 8, j + 8);
                                        j13 = allocate.getLong();
                                    }
                                    j5 = (j23 - j11) + j13;
                                }
                            } else {
                                j10 = j18;
                            }
                            j += i2;
                            i4++;
                            j25 = 16;
                            j26 = 0;
                            j18 = j10;
                        }
                        if (j5 != 0) {
                            String[] strArr = new String[i5];
                            long j27 = 0;
                            long j28 = j4;
                            int i6 = 0;
                            while (true) {
                                long j29 = j27 + j28;
                                if (z2) {
                                    j6 = G0(fileChannel, allocate, j29);
                                } else {
                                    C1(fileChannel, allocate, 8, j29);
                                    j6 = allocate.getLong();
                                }
                                if (j6 == 1) {
                                    if (z2) {
                                        j8 = G0(fileChannel, allocate, 4 + j28);
                                    } else {
                                        C1(fileChannel, allocate, 8, j28 + 8);
                                        j8 = allocate.getLong();
                                    }
                                    long j30 = j8 + j5;
                                    StringBuilder sb = new StringBuilder();
                                    while (true) {
                                        j30++;
                                        j7 = j5;
                                        C1(fileChannel, allocate, 1, j30);
                                        short s2 = (short) (allocate.get() & 255);
                                        if (s2 == 0) {
                                            break;
                                        }
                                        sb.append((char) s2);
                                        j5 = j7;
                                    }
                                    strArr[i6] = sb.toString();
                                    if (i6 != Integer.MAX_VALUE) {
                                        i6++;
                                    } else {
                                        throw new h("malformed DT_NEEDED section");
                                    }
                                } else {
                                    j7 = j5;
                                }
                                j28 += z2 ? 8L : 16L;
                                if (j6 != 0) {
                                    j27 = 0;
                                    j5 = j7;
                                } else if (i6 == i5) {
                                    return strArr;
                                } else {
                                    throw new h("malformed DT_NEEDED section");
                                }
                            }
                        } else {
                            throw new h("did not find file offset of DT_STRTAB table");
                        }
                    } else {
                        throw new h("Dynamic section string-table not found");
                    }
                }
            } else {
                throw new h("ELF file does not contain dynamic linking information");
            }
        } else {
            StringBuilder R = b.d.b.a.a.R("file is not ELF: 0x");
            R.append(Long.toHexString(G0));
            throw new h(R.toString());
        }
    }

    public static void j1(MediaFormat mediaFormat, String str, int i) {
        if (i != -1) {
            mediaFormat.setInteger(str, i);
        }
    }

    public static void j2(Node node, b.a.t.b.c.a aVar) {
        if (node.hasChildren()) {
            for (Node node2 : node.getChildren()) {
                j2(node2, aVar);
            }
        }
        aVar.processNode(node);
    }

    public static void k(boolean z2, Object obj) {
        if (!z2) {
            throw new IllegalArgumentException(String.valueOf(obj));
        }
    }

    public static String k0(String str, Object... objArr) {
        int indexOf;
        String valueOf = String.valueOf(str);
        StringBuilder sb = new StringBuilder((objArr.length * 16) + valueOf.length());
        int i = 0;
        int i2 = 0;
        while (i < objArr.length && (indexOf = valueOf.indexOf("%s", i2)) != -1) {
            sb.append(valueOf.substring(i2, indexOf));
            i++;
            sb.append(objArr[i]);
            i2 = indexOf + 2;
        }
        sb.append(valueOf.substring(i2));
        if (i < objArr.length) {
            sb.append(" [");
            int i3 = i + 1;
            sb.append(objArr[i]);
            while (i3 < objArr.length) {
                sb.append(", ");
                i3++;
                sb.append(objArr[i3]);
            }
            sb.append(']');
        }
        return sb.toString();
    }

    public static String k1(String str, String str2) {
        int length = str.length() - str2.length();
        if (length < 0 || length > 1) {
            throw new IllegalArgumentException("Invalid input received");
        }
        StringBuilder sb = new StringBuilder(str2.length() + str.length());
        for (int i = 0; i < str.length(); i++) {
            sb.append(str.charAt(i));
            if (str2.length() > i) {
                sb.append(str2.charAt(i));
            }
        }
        return sb.toString();
    }

    public static void k2(Node node, b.a.t.b.c.a aVar) {
        aVar.processNode(node);
        if (node.hasChildren()) {
            for (Node node2 : node.getChildren()) {
                k2(node2, aVar);
            }
        }
    }

    public static void l(boolean z2) {
        if (!z2) {
            throw new IllegalArgumentException();
        }
    }

    @NonNull
    public static ApiException l0(@NonNull Status status) {
        if (status.r != null) {
            return new ResolvableApiException(status);
        }
        return new ApiException(status);
    }

    public static void l1(File file) throws FileUtils$CreateDirectoryException {
        if (file.exists()) {
            if (!file.isDirectory()) {
                if (!file.delete()) {
                    throw new FileUtils$CreateDirectoryException(file.getAbsolutePath(), new FileUtils$FileDeleteException(file.getAbsolutePath()));
                }
            } else {
                return;
            }
        }
        if (!file.mkdirs() && !file.isDirectory()) {
            throw new FileUtils$CreateDirectoryException(file.getAbsolutePath());
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:143:0x024a, code lost:
        if (r15 != false) goto L158;
     */
    /* JADX WARN: Code restructure failed: missing block: B:157:0x0264, code lost:
        if (r6 != false) goto L158;
     */
    /* JADX WARN: Code restructure failed: missing block: B:158:0x0266, code lost:
        r4 = true;
     */
    /* JADX WARN: Finally extract failed */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static b.f.g.f.a l2(b.f.g.f.a r22, android.content.Context r23, android.util.AttributeSet r24) {
        /*
            Method dump skipped, instructions count: 736
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.c.a.a0.d.l2(b.f.g.f.a, android.content.Context, android.util.AttributeSet):b.f.g.f.a");
    }

    @Pure
    public static void m(boolean z2, Object obj) {
        if (!z2) {
            throw new IllegalArgumentException(String.valueOf(obj));
        }
    }

    public static void m0(File file) throws IOException {
        if (file.isDirectory()) {
            File[] listFiles = file.listFiles();
            if (listFiles != null) {
                for (File file2 : listFiles) {
                    m0(file2);
                }
                return;
            }
            throw new IOException("cannot list directory " + file);
        } else if (!file.getPath().endsWith("_lock")) {
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
            try {
                randomAccessFile.getFD().sync();
                randomAccessFile.close();
            } catch (Throwable th) {
                try {
                    throw th;
                } catch (Throwable th2) {
                    try {
                        randomAccessFile.close();
                    } catch (Throwable th3) {
                        th.addSuppressed(th3);
                    }
                    throw th2;
                }
            }
        }
    }

    public static int m1(int i, int i2) {
        if (i2 == 255) {
            return i;
        }
        if (i2 == 0) {
            return i & ViewCompat.MEASURED_SIZE_MASK;
        }
        int i3 = i2 + (i2 >> 7);
        return (i & ViewCompat.MEASURED_SIZE_MASK) | ((((i >>> 24) * i3) >> 8) << 24);
    }

    public static int m2(@RecentlyNonNull Parcel parcel) {
        int readInt = parcel.readInt();
        int M1 = M1(parcel, readInt);
        int dataPosition = parcel.dataPosition();
        if (((char) readInt) != 20293) {
            String valueOf = String.valueOf(Integer.toHexString(readInt));
            throw new SafeParcelReader$ParseException(valueOf.length() != 0 ? "Expected object header. Got 0x".concat(valueOf) : new String("Expected object header. Got 0x"), parcel);
        }
        int i = M1 + dataPosition;
        if (i >= dataPosition && i <= parcel.dataSize()) {
            return i;
        }
        throw new SafeParcelReader$ParseException(b.d.b.a.a.g(54, "Size read is invalid start=", dataPosition, " end=", i), parcel);
    }

    public static void n(boolean z2, @RecentlyNonNull String str, @RecentlyNonNull Object... objArr) {
        if (!z2) {
            throw new IllegalArgumentException(String.format(str, objArr));
        }
    }

    public static int n0(InputStream inputStream) throws IOException {
        return ((((byte) inputStream.read()) << 8) & 65280) | (((byte) inputStream.read()) & 255);
    }

    public static final Integer n1(JsonReader jsonReader) {
        d0.z.d.m.checkNotNullParameter(jsonReader, "$this$nextIntOrNull");
        if (jsonReader.N() != JsonToken.NULL) {
            return Integer.valueOf(jsonReader.y());
        }
        jsonReader.H();
        return null;
    }

    public static boolean n2(int i, b.i.a.c.f3.x xVar, boolean z2) throws ParserException {
        if (xVar.a() < 7) {
            if (z2) {
                return false;
            }
            throw b.d.b.a.a.t0(29, "too short header: ", xVar.a(), null);
        } else if (xVar.t() != i) {
            if (z2) {
                return false;
            }
            String valueOf = String.valueOf(Integer.toHexString(i));
            throw ParserException.a(valueOf.length() != 0 ? "expected header type ".concat(valueOf) : new String("expected header type "), null);
        } else if (xVar.t() == 118 && xVar.t() == 111 && xVar.t() == 114 && xVar.t() == 98 && xVar.t() == 105 && xVar.t() == 115) {
            return true;
        } else {
            if (z2) {
                return false;
            }
            throw ParserException.a("expected characters 'vorbis'", null);
        }
    }

    public static void o(boolean z2, @RecentlyNonNull Object obj) {
        if (!z2) {
            throw new IllegalArgumentException(String.valueOf(obj));
        }
    }

    public static int o0(int i) {
        return (int) (i * 1.3333334f);
    }

    @Nullable
    public static <T> List<b.c.a.c0.a<T>> o1(c cVar, b.c.a.d dVar, h0<T> h0Var) throws IOException {
        return r.a(cVar, dVar, 1.0f, h0Var);
    }

    public static void o2(File file, b.f.d.c.a aVar) {
        aVar.b(file);
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                if (file2.isDirectory()) {
                    o2(file2, aVar);
                } else {
                    aVar.a(file2);
                }
            }
        }
        aVar.c(file);
    }

    public static void p(int i, int i2, int i3, int i4, int i5) {
        boolean z2 = true;
        i(Boolean.valueOf(i4 >= 0));
        i(Boolean.valueOf(i >= 0));
        i(Boolean.valueOf(i3 >= 0));
        i(Boolean.valueOf(i + i4 <= i5));
        if (i3 + i4 > i2) {
            z2 = false;
        }
        i(Boolean.valueOf(z2));
    }

    public static p p0(Drawable drawable) {
        if (drawable == null) {
            return null;
        }
        if (drawable instanceof p) {
            return (p) drawable;
        }
        if (drawable instanceof b.f.g.e.d) {
            return p0(((b.f.g.e.d) drawable).l());
        }
        if (drawable instanceof b.f.g.e.b) {
            b.f.g.e.b bVar = (b.f.g.e.b) drawable;
            int length = bVar.l.length;
            for (int i = 0; i < length; i++) {
                p p0 = p0(bVar.a(i));
                if (p0 != null) {
                    return p0;
                }
            }
        }
        return null;
    }

    public static b.c.a.y.k.a p1(c cVar, b.c.a.d dVar) throws IOException {
        return new b.c.a.y.k.a(o1(cVar, dVar, f.a));
    }

    public static void p2(@RecentlyNonNull Parcel parcel, int i, @RecentlyNonNull Bundle bundle, boolean z2) {
        if (bundle != null) {
            int y2 = y2(parcel, i);
            parcel.writeBundle(bundle);
            A2(parcel, y2);
        } else if (z2) {
            parcel.writeInt(i | 0);
        }
    }

    @Pure
    public static void q(boolean z2, @Nullable String str) throws ParserException {
        if (!z2) {
            throw ParserException.a(str, null);
        }
    }

    public static Object q0(Object obj) {
        return new a(obj.getClass(), Array.getLength(obj), obj);
    }

    public static b.c.a.y.k.b q1(c cVar, b.c.a.d dVar) throws IOException {
        return r1(cVar, dVar, true);
    }

    public static void q2(@RecentlyNonNull Parcel parcel, int i, @RecentlyNonNull byte[] bArr, boolean z2) {
        if (bArr != null) {
            int y2 = y2(parcel, i);
            parcel.writeByteArray(bArr);
            A2(parcel, y2);
        } else if (z2) {
            parcel.writeInt(i | 0);
        }
    }

    public static boolean r(b.i.a.c.x2.i iVar) throws IOException {
        b.i.a.c.f3.x xVar = new b.i.a.c.f3.x(8);
        if (b.i.a.c.x2.l0.d.a(iVar, xVar).a != 1380533830) {
            return false;
        }
        iVar.o(xVar.a, 0, 4);
        xVar.E(0);
        int f2 = xVar.f();
        if (f2 == 1463899717) {
            return true;
        }
        StringBuilder sb = new StringBuilder(34);
        sb.append("Unsupported form type: ");
        sb.append(f2);
        Log.e("WavHeaderReader", sb.toString());
        return false;
    }

    @Nullable
    public static String r0(XmlPullParser xmlPullParser, String str) {
        int attributeCount = xmlPullParser.getAttributeCount();
        for (int i = 0; i < attributeCount; i++) {
            if (xmlPullParser.getAttributeName(i).equals(str)) {
                return xmlPullParser.getAttributeValue(i);
            }
        }
        return null;
    }

    public static b.c.a.y.k.b r1(c cVar, b.c.a.d dVar, boolean z2) throws IOException {
        return new b.c.a.y.k.b(r.a(cVar, dVar, z2 ? g.c() : 1.0f, i.a));
    }

    public static void r2(@RecentlyNonNull Parcel parcel, int i, @RecentlyNonNull IBinder iBinder, boolean z2) {
        if (iBinder != null) {
            int y2 = y2(parcel, i);
            parcel.writeStrongBinder(iBinder);
            A2(parcel, y2);
        } else if (z2) {
            parcel.writeInt(i | 0);
        }
    }

    public static void s(@RecentlyNonNull Handler handler) {
        Looper myLooper = Looper.myLooper();
        if (myLooper != handler.getLooper()) {
            String name = myLooper != null ? myLooper.getThread().getName() : "null current looper";
            String name2 = handler.getLooper().getThread().getName();
            StringBuilder sb = new StringBuilder(String.valueOf(name2).length() + 36 + String.valueOf(name).length());
            b.d.b.a.a.q0(sb, "Must be called on ", name2, " thread, but got ", name);
            sb.append(".");
            throw new IllegalStateException(sb.toString());
        }
    }

    public static int s0(int i) {
        if (i == 3) {
            return 180;
        }
        if (i != 6) {
            return i != 8 ? 0 : 270;
        }
        return 90;
    }

    public static b.c.a.y.k.d s1(c cVar, b.c.a.d dVar) throws IOException {
        return new b.c.a.y.k.d(o1(cVar, dVar, o.a));
    }

    public static void s2(@RecentlyNonNull Parcel parcel, int i, @RecentlyNonNull Parcelable parcelable, int i2, boolean z2) {
        if (parcelable != null) {
            int y2 = y2(parcel, i);
            parcelable.writeToParcel(parcel, i2);
            A2(parcel, y2);
        } else if (z2) {
            parcel.writeInt(i | 0);
        }
    }

    @Pure
    public static int t(int i, int i2, int i3) {
        if (i >= i2 && i < i3) {
            return i;
        }
        throw new IndexOutOfBoundsException();
    }

    public static Object t0(j jVar) {
        Class<?> cls = jVar._class;
        Class<?> v = b.g.a.c.i0.d.v(cls);
        if (v != null) {
            if (v == Integer.TYPE) {
                return 0;
            }
            if (v == Long.TYPE) {
                return 0L;
            }
            if (v == Boolean.TYPE) {
                return Boolean.FALSE;
            }
            if (v == Double.TYPE) {
                return Double.valueOf((double) ShadowDrawableWrapper.COS_45);
            }
            if (v == Float.TYPE) {
                return Float.valueOf(0.0f);
            }
            if (v == Byte.TYPE) {
                return (byte) 0;
            }
            if (v == Short.TYPE) {
                return (short) 0;
            }
            if (v == Character.TYPE) {
                return (char) 0;
            }
            throw new IllegalArgumentException(b.d.b.a.a.n(v, b.d.b.a.a.R("Class "), " is not a primitive type"));
        } else if (jVar.v() || jVar.b()) {
            return p.a.NON_EMPTY;
        } else {
            if (cls == String.class) {
                return "";
            }
            if (jVar.B(Date.class)) {
                return new Date(0L);
            }
            if (!jVar.B(Calendar.class)) {
                return null;
            }
            GregorianCalendar gregorianCalendar = new GregorianCalendar();
            gregorianCalendar.setTimeInMillis(0L);
            return gregorianCalendar;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:59:0x01a5  */
    /* JADX WARN: Removed duplicated region for block: B:70:0x01bc A[SYNTHETIC] */
    @androidx.annotation.Nullable
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static java.util.ArrayList<b.i.a.c.g3.z.h.a> t1(b.i.a.c.f3.x r27) {
        /*
            Method dump skipped, instructions count: 448
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.c.a.a0.d.t1(b.i.a.c.f3.x):java.util.ArrayList");
    }

    public static void t2(@RecentlyNonNull Parcel parcel, int i, @RecentlyNonNull String str, boolean z2) {
        if (str != null) {
            int y2 = y2(parcel, i);
            parcel.writeString(str);
            A2(parcel, y2);
        } else if (z2) {
            parcel.writeInt(i | 0);
        }
    }

    public static void u(@RecentlyNonNull String str) {
        if (!(Looper.getMainLooper() == Looper.myLooper())) {
            throw new IllegalStateException(str);
        }
    }

    public static Drawable u0(Context context, TypedArray typedArray, int i) {
        int resourceId = typedArray.getResourceId(i, 0);
        if (resourceId == 0) {
            return null;
        }
        return context.getResources().getDrawable(resourceId);
    }

    public static b.c.a.y.k.f u1(c cVar, b.c.a.d dVar) throws IOException {
        return new b.c.a.y.k.f(r.a(cVar, dVar, g.c(), w.a));
    }

    public static void u2(@RecentlyNonNull Parcel parcel, int i, @RecentlyNonNull String[] strArr, boolean z2) {
        if (strArr != null) {
            int y2 = y2(parcel, i);
            parcel.writeStringArray(strArr);
            A2(parcel, y2);
        } else if (z2) {
            parcel.writeInt(i | 0);
        }
    }

    @RecentlyNonNull
    @EnsuresNonNull({"#1"})
    public static String v(@Nullable String str, @RecentlyNonNull Object obj) {
        if (!TextUtils.isEmpty(str)) {
            return str;
        }
        throw new IllegalArgumentException(String.valueOf(obj));
    }

    @RecentlyNonNull
    public static String v0(@RecentlyNonNull PowerManager.WakeLock wakeLock, @RecentlyNonNull String str) {
        String valueOf = String.valueOf(String.valueOf(System.identityHashCode(wakeLock) | (Process.myPid() << 32)));
        String str2 = null;
        if (true == TextUtils.isEmpty(null)) {
            str2 = "";
        }
        String valueOf2 = String.valueOf(str2);
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    @Nullable
    public static b.i.a.c.x2.i0.j v1(byte[] bArr) {
        b.i.a.c.f3.x xVar = new b.i.a.c.f3.x(bArr);
        if (xVar.c < 32) {
            return null;
        }
        xVar.E(0);
        if (xVar.f() != xVar.a() + 4 || xVar.f() != 1886614376) {
            return null;
        }
        int f2 = (xVar.f() >> 24) & 255;
        if (f2 > 1) {
            b.d.b.a.a.d0(37, "Unsupported pssh version: ", f2, "PsshAtomUtil");
            return null;
        }
        UUID uuid = new UUID(xVar.m(), xVar.m());
        if (f2 == 1) {
            xVar.F(xVar.w() * 16);
        }
        int w = xVar.w();
        if (w != xVar.a()) {
            return null;
        }
        byte[] bArr2 = new byte[w];
        System.arraycopy(xVar.a, xVar.f980b, bArr2, 0, w);
        xVar.f980b += w;
        return new b.i.a.c.x2.i0.j(uuid, f2, bArr2);
    }

    public static <T extends Parcelable> void v2(@RecentlyNonNull Parcel parcel, int i, @RecentlyNonNull T[] tArr, int i2, boolean z2) {
        if (tArr != null) {
            int y2 = y2(parcel, i);
            parcel.writeInt(tArr.length);
            for (T t : tArr) {
                if (t == null) {
                    parcel.writeInt(0);
                } else {
                    B2(parcel, t, i2);
                }
            }
            A2(parcel, y2);
        } else if (z2) {
            parcel.writeInt(i | 0);
        }
    }

    @RecentlyNonNull
    @EnsuresNonNull({"#1"})
    public static String w(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            return str;
        }
        throw new IllegalArgumentException("Given String is empty or null");
    }

    public static int w0(InputStream inputStream) throws IOException {
        return ((((byte) inputStream.read()) << 24) & ViewCompat.MEASURED_STATE_MASK) | ((((byte) inputStream.read()) << 16) & ItemTouchHelper.ACTION_MODE_DRAG_MASK) | ((((byte) inputStream.read()) << 8) & 65280) | (((byte) inputStream.read()) & 255);
    }

    @Nullable
    public static byte[] w1(byte[] bArr, UUID uuid) {
        b.i.a.c.x2.i0.j v1 = v1(bArr);
        if (v1 == null) {
            return null;
        }
        if (uuid.equals(v1.a)) {
            return v1.c;
        }
        String valueOf = String.valueOf(uuid);
        String valueOf2 = String.valueOf(v1.a);
        b.d.b.a.a.o0(b.d.b.a.a.Q(valueOf2.length() + valueOf.length() + 33, "UUID mismatch. Expected: ", valueOf, ", got: ", valueOf2), ".", "PsshAtomUtil");
        return null;
    }

    public static <T extends Parcelable> void w2(@RecentlyNonNull Parcel parcel, int i, @RecentlyNonNull List<T> list, boolean z2) {
        if (list != null) {
            int y2 = y2(parcel, i);
            int size = list.size();
            parcel.writeInt(size);
            for (int i2 = 0; i2 < size; i2++) {
                T t = list.get(i2);
                if (t == null) {
                    parcel.writeInt(0);
                } else {
                    B2(parcel, t, 0);
                }
            }
            A2(parcel, y2);
        } else if (z2) {
            parcel.writeInt(i | 0);
        }
    }

    public static void x(@RecentlyNonNull String str) {
        if (Looper.getMainLooper() == Looper.myLooper()) {
            throw new IllegalStateException(str);
        }
    }

    public static final Integer x0(Intent intent, String str) {
        d0.z.d.m.checkNotNullParameter(intent, "$this$getIntExtraOrNull");
        d0.z.d.m.checkNotNullParameter(str, "key");
        Integer valueOf = Integer.valueOf(intent.getIntExtra(str, Integer.MIN_VALUE));
        if (!(valueOf.intValue() == Integer.MIN_VALUE)) {
            return valueOf;
        }
        return null;
    }

    public static boolean x1(b.i.a.c.x2.i iVar, byte[] bArr, int i, int i2, boolean z2) throws IOException {
        try {
            return iVar.e(bArr, i, i2, z2);
        } catch (EOFException e2) {
            if (z2) {
                return false;
            }
            throw e2;
        }
    }

    public static void x2(Parcel parcel, int i, int i2) {
        int M1 = M1(parcel, i);
        if (M1 != i2) {
            String hexString = Integer.toHexString(M1);
            StringBuilder sb = new StringBuilder(String.valueOf(hexString).length() + 46);
            sb.append("Expected size ");
            sb.append(i2);
            sb.append(" got ");
            sb.append(M1);
            throw new SafeParcelReader$ParseException(b.d.b.a.a.J(sb, " (0x", hexString, ")"), parcel);
        }
    }

    public static <T> T y(T t, Object obj) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(String.valueOf(obj));
    }

    public static final StageRequestToSpeakState y0(VoiceState voiceState) {
        if (voiceState == null) {
            return StageRequestToSpeakState.NONE;
        }
        if (voiceState.l() && voiceState.f() != null) {
            return StageRequestToSpeakState.REQUESTED_TO_SPEAK;
        }
        if (voiceState.l() || voiceState.f() == null) {
            return !voiceState.l() ? StageRequestToSpeakState.ON_STAGE : StageRequestToSpeakState.NONE;
        }
        return StageRequestToSpeakState.REQUESTED_TO_SPEAK_AND_AWAITING_USER_ACK;
    }

    @Nullable
    public static Metadata y1(b.i.a.c.x2.i iVar, boolean z2) throws IOException {
        b.i.a.c.z2.k.a aVar;
        if (z2) {
            aVar = null;
        } else {
            int i = b.i.a.c.z2.k.b.a;
            aVar = b.i.a.c.z2.k.a.a;
        }
        b.i.a.c.f3.x xVar = new b.i.a.c.f3.x(10);
        Metadata metadata = null;
        int i2 = 0;
        while (true) {
            try {
                iVar.o(xVar.a, 0, 10);
                xVar.E(0);
                if (xVar.v() != 4801587) {
                    break;
                }
                xVar.F(3);
                int s2 = xVar.s();
                int i3 = s2 + 10;
                if (metadata == null) {
                    byte[] bArr = new byte[i3];
                    System.arraycopy(xVar.a, 0, bArr, 0, 10);
                    iVar.o(bArr, 10, s2);
                    metadata = new b.i.a.c.z2.k.b(aVar).d(bArr, i3);
                } else {
                    iVar.g(s2);
                }
                i2 += i3;
            } catch (EOFException unused) {
            }
        }
        iVar.k();
        iVar.g(i2);
        if (metadata == null || metadata.j.length == 0) {
            return null;
        }
        return metadata;
    }

    public static int y2(Parcel parcel, int i) {
        parcel.writeInt(i | (-65536));
        parcel.writeInt(0);
        return parcel.dataPosition();
    }

    @NonNull
    @EnsuresNonNull({"#1"})
    public static <T> T z(@RecentlyNonNull T t, @RecentlyNonNull Object obj) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(String.valueOf(obj));
    }

    public static List<String> z0(CacheKey cacheKey) {
        try {
            if (!(cacheKey instanceof b.f.b.a.c)) {
                ArrayList arrayList = new ArrayList(1);
                arrayList.add(cacheKey.a() ? cacheKey.b() : W1(cacheKey));
                return arrayList;
            }
            Objects.requireNonNull((b.f.b.a.c) cacheKey);
            throw null;
        } catch (UnsupportedEncodingException e2) {
            throw new RuntimeException(e2);
        }
    }

    public static int z1(b.i.a.c.x2.i iVar, byte[] bArr, int i, int i2) throws IOException {
        int i3 = 0;
        while (i3 < i2) {
            int i4 = iVar.i(bArr, i + i3, i2 - i3);
            if (i4 == -1) {
                break;
            }
            i3 += i4;
        }
        return i3;
    }

    public static void z2(Parcel parcel, int i, int i2, int i3) {
        if (i2 != i3) {
            String hexString = Integer.toHexString(i2);
            StringBuilder sb = new StringBuilder(String.valueOf(hexString).length() + 46);
            sb.append("Expected size ");
            sb.append(i3);
            sb.append(" got ");
            sb.append(i2);
            throw new SafeParcelReader$ParseException(b.d.b.a.a.J(sb, " (0x", hexString, ")"), parcel);
        }
    }
}
