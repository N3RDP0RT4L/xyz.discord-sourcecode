package b.c.a;

import android.graphics.Rect;
import androidx.annotation.RestrictTo;
import androidx.collection.LongSparseArray;
import androidx.collection.SparseArrayCompat;
import b.c.a.b0.c;
import b.c.a.y.i;
import b.c.a.y.m.e;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
/* compiled from: LottieComposition.java */
/* loaded from: classes.dex */
public class d {
    public Map<String, List<e>> c;
    public Map<String, k> d;
    public Map<String, b.c.a.y.d> e;
    public List<i> f;
    public SparseArrayCompat<b.c.a.y.e> g;
    public LongSparseArray<e> h;
    public List<e> i;
    public Rect j;
    public float k;
    public float l;
    public float m;
    public boolean n;
    public final s a = new s();

    /* renamed from: b  reason: collision with root package name */
    public final HashSet<String> f347b = new HashSet<>();
    public int o = 0;

    @RestrictTo({RestrictTo.Scope.LIBRARY})
    public void a(String str) {
        c.b(str);
        this.f347b.add(str);
    }

    public float b() {
        return (c() / this.m) * 1000.0f;
    }

    public float c() {
        return this.l - this.k;
    }

    /* JADX WARN: Code restructure failed: missing block: B:10:0x003a, code lost:
        if (r3.substring(0, r3.length() - 1).equalsIgnoreCase(r7) != false) goto L12;
     */
    @androidx.annotation.Nullable
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.c.a.y.i d(java.lang.String r7) {
        /*
            r6 = this;
            java.util.List<b.c.a.y.i> r0 = r6.f
            r0.size()
            r0 = 0
            r1 = 0
        L7:
            java.util.List<b.c.a.y.i> r2 = r6.f
            int r2 = r2.size()
            if (r1 >= r2) goto L44
            java.util.List<b.c.a.y.i> r2 = r6.f
            java.lang.Object r2 = r2.get(r1)
            b.c.a.y.i r2 = (b.c.a.y.i) r2
            java.lang.String r3 = r2.a
            boolean r3 = r3.equalsIgnoreCase(r7)
            r4 = 1
            if (r3 == 0) goto L21
            goto L3e
        L21:
            java.lang.String r3 = r2.a
            java.lang.String r5 = "\r"
            boolean r3 = r3.endsWith(r5)
            if (r3 == 0) goto L3d
            java.lang.String r3 = r2.a
            int r5 = r3.length()
            int r5 = r5 - r4
            java.lang.String r3 = r3.substring(r0, r5)
            boolean r3 = r3.equalsIgnoreCase(r7)
            if (r3 == 0) goto L3d
            goto L3e
        L3d:
            r4 = 0
        L3e:
            if (r4 == 0) goto L41
            return r2
        L41:
            int r1 = r1 + 1
            goto L7
        L44:
            r7 = 0
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: b.c.a.d.d(java.lang.String):b.c.a.y.i");
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY})
    public e e(long j) {
        return this.h.get(j);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("LottieComposition:\n");
        for (e eVar : this.i) {
            sb.append(eVar.a("\t"));
        }
        return sb.toString();
    }
}
