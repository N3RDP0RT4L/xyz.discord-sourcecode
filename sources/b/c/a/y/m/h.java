package b.c.a.y.m;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import androidx.annotation.Nullable;
import b.c.a.c0.c;
import b.c.a.j;
import b.c.a.o;
import b.c.a.w.c.a;
import b.c.a.w.c.p;
/* compiled from: SolidLayer.java */
/* loaded from: classes.dex */
public class h extends b {
    public final e B;
    @Nullable
    public a<ColorFilter, ColorFilter> C;

    /* renamed from: y  reason: collision with root package name */
    public final Paint f438y;

    /* renamed from: x  reason: collision with root package name */
    public final RectF f437x = new RectF();

    /* renamed from: z  reason: collision with root package name */
    public final float[] f439z = new float[8];
    public final Path A = new Path();

    public h(j jVar, e eVar) {
        super(jVar, eVar);
        b.c.a.w.a aVar = new b.c.a.w.a();
        this.f438y = aVar;
        this.B = eVar;
        aVar.setAlpha(0);
        aVar.setStyle(Paint.Style.FILL);
        aVar.setColor(eVar.l);
    }

    @Override // b.c.a.y.m.b, b.c.a.w.b.e
    public void d(RectF rectF, Matrix matrix, boolean z2) {
        super.d(rectF, matrix, z2);
        RectF rectF2 = this.f437x;
        e eVar = this.B;
        rectF2.set(0.0f, 0.0f, eVar.j, eVar.k);
        this.m.mapRect(this.f437x);
        rectF.set(this.f437x);
    }

    @Override // b.c.a.y.m.b, b.c.a.y.g
    public <T> void g(T t, @Nullable c<T> cVar) {
        this.v.c(t, cVar);
        if (t != o.C) {
            return;
        }
        if (cVar == null) {
            this.C = null;
        } else {
            this.C = new p(cVar, null);
        }
    }

    @Override // b.c.a.y.m.b
    public void j(Canvas canvas, Matrix matrix, int i) {
        int alpha = Color.alpha(this.B.l);
        if (alpha != 0) {
            a<Integer, Integer> aVar = this.v.j;
            int intValue = (int) ((((alpha / 255.0f) * (aVar == null ? 100 : aVar.e().intValue())) / 100.0f) * (i / 255.0f) * 255.0f);
            this.f438y.setAlpha(intValue);
            a<ColorFilter, ColorFilter> aVar2 = this.C;
            if (aVar2 != null) {
                this.f438y.setColorFilter(aVar2.e());
            }
            if (intValue > 0) {
                float[] fArr = this.f439z;
                fArr[0] = 0.0f;
                fArr[1] = 0.0f;
                e eVar = this.B;
                int i2 = eVar.j;
                fArr[2] = i2;
                fArr[3] = 0.0f;
                fArr[4] = i2;
                int i3 = eVar.k;
                fArr[5] = i3;
                fArr[6] = 0.0f;
                fArr[7] = i3;
                matrix.mapPoints(fArr);
                this.A.reset();
                Path path = this.A;
                float[] fArr2 = this.f439z;
                path.moveTo(fArr2[0], fArr2[1]);
                Path path2 = this.A;
                float[] fArr3 = this.f439z;
                path2.lineTo(fArr3[2], fArr3[3]);
                Path path3 = this.A;
                float[] fArr4 = this.f439z;
                path3.lineTo(fArr4[4], fArr4[5]);
                Path path4 = this.A;
                float[] fArr5 = this.f439z;
                path4.lineTo(fArr5[6], fArr5[7]);
                Path path5 = this.A;
                float[] fArr6 = this.f439z;
                path5.lineTo(fArr6[0], fArr6[1]);
                this.A.close();
                canvas.drawPath(this.A, this.f438y);
            }
        }
    }
}
