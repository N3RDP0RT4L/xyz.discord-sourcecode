package b.c.a.y.m;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.c.a.b0.g;
import b.c.a.c0.c;
import b.c.a.j;
import b.c.a.k;
import b.c.a.o;
import b.c.a.w.c.a;
import b.c.a.w.c.p;
import b.c.a.x.b;
import java.io.IOException;
import org.objectweb.asm.Opcodes;
/* compiled from: ImageLayer.java */
/* loaded from: classes.dex */
public class d extends b {
    @Nullable
    public a<ColorFilter, ColorFilter> A;

    /* renamed from: x  reason: collision with root package name */
    public final Paint f431x = new b.c.a.w.a(3);

    /* renamed from: y  reason: collision with root package name */
    public final Rect f432y = new Rect();

    /* renamed from: z  reason: collision with root package name */
    public final Rect f433z = new Rect();

    public d(j jVar, e eVar) {
        super(jVar, eVar);
    }

    @Override // b.c.a.y.m.b, b.c.a.w.b.e
    public void d(RectF rectF, Matrix matrix, boolean z2) {
        super.d(rectF, matrix, z2);
        Bitmap q = q();
        if (q != null) {
            rectF.set(0.0f, 0.0f, g.c() * q.getWidth(), g.c() * q.getHeight());
            this.m.mapRect(rectF);
        }
    }

    @Override // b.c.a.y.m.b, b.c.a.y.g
    public <T> void g(T t, @Nullable c<T> cVar) {
        this.v.c(t, cVar);
        if (t != o.C) {
            return;
        }
        if (cVar == null) {
            this.A = null;
        } else {
            this.A = new p(cVar, null);
        }
    }

    @Override // b.c.a.y.m.b
    public void j(@NonNull Canvas canvas, Matrix matrix, int i) {
        Bitmap q = q();
        if (q != null && !q.isRecycled()) {
            float c = g.c();
            this.f431x.setAlpha(i);
            a<ColorFilter, ColorFilter> aVar = this.A;
            if (aVar != null) {
                this.f431x.setColorFilter(aVar.e());
            }
            canvas.save();
            canvas.concat(matrix);
            this.f432y.set(0, 0, q.getWidth(), q.getHeight());
            this.f433z.set(0, 0, (int) (q.getWidth() * c), (int) (q.getHeight() * c));
            canvas.drawBitmap(q, this.f432y, this.f433z, this.f431x);
            canvas.restore();
        }
    }

    @Nullable
    public final Bitmap q() {
        b bVar;
        k kVar;
        String str = this.o.g;
        j jVar = this.n;
        if (jVar.getCallback() == null) {
            bVar = null;
        } else {
            b bVar2 = jVar.f348s;
            if (bVar2 != null) {
                Drawable.Callback callback = jVar.getCallback();
                Context context = (callback != null && (callback instanceof View)) ? ((View) callback).getContext() : null;
                if (!((context == null && bVar2.f394b == null) || bVar2.f394b.equals(context))) {
                    jVar.f348s = null;
                }
            }
            if (jVar.f348s == null) {
                jVar.f348s = new b(jVar.getCallback(), jVar.t, jVar.u, jVar.k.d);
            }
            bVar = jVar.f348s;
        }
        if (bVar == null || (kVar = bVar.e.get(str)) == null) {
            return null;
        }
        Bitmap bitmap = kVar.e;
        if (bitmap != null) {
            return bitmap;
        }
        b.c.a.b bVar3 = bVar.d;
        if (bVar3 != null) {
            Bitmap a = bVar3.a(kVar);
            if (a == null) {
                return a;
            }
            bVar.a(str, a);
            return a;
        }
        String str2 = kVar.d;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = true;
        options.inDensity = Opcodes.IF_ICMPNE;
        if (!str2.startsWith("data:") || str2.indexOf("base64,") <= 0) {
            try {
                if (!TextUtils.isEmpty(bVar.c)) {
                    Bitmap e = g.e(BitmapFactory.decodeStream(bVar.f394b.getAssets().open(bVar.c + str2), null, options), kVar.a, kVar.f363b);
                    bVar.a(str, e);
                    return e;
                }
                throw new IllegalStateException("You must set an images folder before loading an image. Set it with LottieComposition#setImagesFolder or LottieDrawable#setImagesFolder");
            } catch (IOException e2) {
                b.c.a.b0.c.c("Unable to open asset.", e2);
                return null;
            }
        } else {
            try {
                byte[] decode = Base64.decode(str2.substring(str2.indexOf(44) + 1), 0);
                Bitmap decodeByteArray = BitmapFactory.decodeByteArray(decode, 0, decode.length, options);
                bVar.a(str, decodeByteArray);
                return decodeByteArray;
            } catch (IllegalArgumentException e3) {
                b.c.a.b0.c.c("data URL did not have correct base64 format.", e3);
                return null;
            }
        }
    }
}
