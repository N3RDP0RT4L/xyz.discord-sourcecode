package b.c.a.y.m;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import androidx.annotation.CallSuper;
import androidx.annotation.FloatRange;
import androidx.annotation.Nullable;
import b.c.a.c0.d;
import b.c.a.j;
import b.c.a.s;
import b.c.a.w.b.e;
import b.c.a.w.c.a;
import b.c.a.w.c.c;
import b.c.a.w.c.o;
import b.c.a.y.g;
import b.c.a.y.k.l;
import b.c.a.y.l.f;
import b.c.a.y.l.k;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
/* compiled from: BaseLayer.java */
/* loaded from: classes.dex */
public abstract class b implements e, a.b, g {
    public final Paint f;
    public final String l;
    public final j n;
    public final e o;
    @Nullable
    public b.c.a.w.c.g p;
    @Nullable
    public c q;
    @Nullable
    public b r;
    @Nullable

    /* renamed from: s  reason: collision with root package name */
    public b f427s;
    public List<b> t;
    public final o v;
    public final Path a = new Path();

    /* renamed from: b  reason: collision with root package name */
    public final Matrix f426b = new Matrix();
    public final Paint c = new b.c.a.w.a(1);
    public final Paint d = new b.c.a.w.a(1, PorterDuff.Mode.DST_IN);
    public final Paint e = new b.c.a.w.a(1, PorterDuff.Mode.DST_OUT);
    public final Paint g = new b.c.a.w.a(PorterDuff.Mode.CLEAR);
    public final RectF h = new RectF();
    public final RectF i = new RectF();
    public final RectF j = new RectF();
    public final RectF k = new RectF();
    public final Matrix m = new Matrix();
    public final List<a<?, ?>> u = new ArrayList();
    public boolean w = true;

    public b(j jVar, e eVar) {
        boolean z2 = true;
        b.c.a.w.a aVar = new b.c.a.w.a(1);
        this.f = aVar;
        this.n = jVar;
        this.o = eVar;
        this.l = b.d.b.a.a.H(new StringBuilder(), eVar.c, "#draw");
        if (eVar.u == 3) {
            aVar.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
        } else {
            aVar.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        }
        l lVar = eVar.i;
        Objects.requireNonNull(lVar);
        o oVar = new o(lVar);
        this.v = oVar;
        oVar.b(this);
        List<f> list = eVar.h;
        if (list != null && !list.isEmpty()) {
            b.c.a.w.c.g gVar = new b.c.a.w.c.g(eVar.h);
            this.p = gVar;
            for (a<k, Path> aVar2 : gVar.a) {
                aVar2.a.add(this);
            }
            for (a<Integer, Integer> aVar3 : this.p.f391b) {
                e(aVar3);
                aVar3.a.add(this);
            }
        }
        if (!this.o.t.isEmpty()) {
            c cVar = new c(this.o.t);
            this.q = cVar;
            cVar.f388b = true;
            cVar.a.add(new a(this));
            p(this.q.e().floatValue() != 1.0f ? false : z2);
            e(this.q);
            return;
        }
        p(true);
    }

    @Override // b.c.a.w.c.a.b
    public void a() {
        this.n.invalidateSelf();
    }

    @Override // b.c.a.w.b.c
    public void b(List<b.c.a.w.b.c> list, List<b.c.a.w.b.c> list2) {
    }

    @Override // b.c.a.y.g
    public void c(b.c.a.y.f fVar, int i, List<b.c.a.y.f> list, b.c.a.y.f fVar2) {
        if (fVar.e(this.o.c, i)) {
            if (!"__container".equals(this.o.c)) {
                fVar2 = fVar2.a(this.o.c);
                if (fVar.c(this.o.c, i)) {
                    list.add(fVar2.g(this));
                }
            }
            if (fVar.f(this.o.c, i)) {
                n(fVar, fVar.d(this.o.c, i) + i, list, fVar2);
            }
        }
    }

    @Override // b.c.a.w.b.e
    @CallSuper
    public void d(RectF rectF, Matrix matrix, boolean z2) {
        this.h.set(0.0f, 0.0f, 0.0f, 0.0f);
        h();
        this.m.set(matrix);
        if (z2) {
            List<b> list = this.t;
            if (list != null) {
                for (int size = list.size() - 1; size >= 0; size--) {
                    this.m.preConcat(this.t.get(size).v.e());
                }
            } else {
                b bVar = this.f427s;
                if (bVar != null) {
                    this.m.preConcat(bVar.v.e());
                }
            }
        }
        this.m.preConcat(this.v.e());
    }

    public void e(@Nullable a<?, ?> aVar) {
        if (aVar != null) {
            this.u.add(aVar);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:112:0x03aa A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:39:0x012c  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x0134  */
    /* JADX WARN: Removed duplicated region for block: B:77:0x024b  */
    @Override // b.c.a.w.b.e
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void f(android.graphics.Canvas r18, android.graphics.Matrix r19, int r20) {
        /*
            Method dump skipped, instructions count: 1004
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.c.a.y.m.b.f(android.graphics.Canvas, android.graphics.Matrix, int):void");
    }

    @Override // b.c.a.y.g
    @CallSuper
    public <T> void g(T t, @Nullable b.c.a.c0.c<T> cVar) {
        this.v.c(t, cVar);
    }

    @Override // b.c.a.w.b.c
    public String getName() {
        return this.o.c;
    }

    public final void h() {
        if (this.t == null) {
            if (this.f427s == null) {
                this.t = Collections.emptyList();
                return;
            }
            this.t = new ArrayList();
            for (b bVar = this.f427s; bVar != null; bVar = bVar.f427s) {
                this.t.add(bVar);
            }
        }
    }

    public final void i(Canvas canvas) {
        RectF rectF = this.h;
        canvas.drawRect(rectF.left - 1.0f, rectF.top - 1.0f, rectF.right + 1.0f, rectF.bottom + 1.0f, this.g);
        b.c.a.c.a("Layer#clearLayer");
    }

    public abstract void j(Canvas canvas, Matrix matrix, int i);

    public boolean k() {
        b.c.a.w.c.g gVar = this.p;
        return gVar != null && !gVar.a.isEmpty();
    }

    public boolean l() {
        return this.r != null;
    }

    public final void m(float f) {
        s sVar = this.n.k.a;
        String str = this.o.c;
        if (sVar.a) {
            b.c.a.b0.e eVar = sVar.c.get(str);
            if (eVar == null) {
                eVar = new b.c.a.b0.e();
                sVar.c.put(str, eVar);
            }
            float f2 = eVar.a + f;
            eVar.a = f2;
            int i = eVar.f341b + 1;
            eVar.f341b = i;
            if (i == Integer.MAX_VALUE) {
                eVar.a = f2 / 2.0f;
                eVar.f341b = i / 2;
            }
            if (str.equals("__container")) {
                for (s.a aVar : sVar.f371b) {
                    aVar.a(f);
                }
            }
        }
    }

    public void n(b.c.a.y.f fVar, int i, List<b.c.a.y.f> list, b.c.a.y.f fVar2) {
    }

    public void o(@FloatRange(from = 0.0d, to = 1.0d) float f) {
        o oVar = this.v;
        a<Integer, Integer> aVar = oVar.j;
        if (aVar != null) {
            aVar.h(f);
        }
        a<?, Float> aVar2 = oVar.m;
        if (aVar2 != null) {
            aVar2.h(f);
        }
        a<?, Float> aVar3 = oVar.n;
        if (aVar3 != null) {
            aVar3.h(f);
        }
        a<PointF, PointF> aVar4 = oVar.f;
        if (aVar4 != null) {
            aVar4.h(f);
        }
        a<?, PointF> aVar5 = oVar.g;
        if (aVar5 != null) {
            aVar5.h(f);
        }
        a<d, d> aVar6 = oVar.h;
        if (aVar6 != null) {
            aVar6.h(f);
        }
        a<Float, Float> aVar7 = oVar.i;
        if (aVar7 != null) {
            aVar7.h(f);
        }
        c cVar = oVar.k;
        if (cVar != null) {
            cVar.h(f);
        }
        c cVar2 = oVar.l;
        if (cVar2 != null) {
            cVar2.h(f);
        }
        if (this.p != null) {
            for (int i = 0; i < this.p.a.size(); i++) {
                this.p.a.get(i).h(f);
            }
        }
        float f2 = this.o.m;
        if (f2 != 0.0f) {
            f /= f2;
        }
        c cVar3 = this.q;
        if (cVar3 != null) {
            cVar3.h(f / f2);
        }
        b bVar = this.r;
        if (bVar != null) {
            bVar.o(bVar.o.m * f);
        }
        for (int i2 = 0; i2 < this.u.size(); i2++) {
            this.u.get(i2).h(f);
        }
    }

    public final void p(boolean z2) {
        if (z2 != this.w) {
            this.w = z2;
            this.n.invalidateSelf();
        }
    }
}
