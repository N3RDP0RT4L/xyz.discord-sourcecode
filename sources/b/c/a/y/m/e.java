package b.c.a.y.m;

import androidx.annotation.Nullable;
import b.c.a.d;
import b.c.a.y.k.j;
import b.c.a.y.k.k;
import b.c.a.y.k.l;
import b.c.a.y.l.b;
import b.c.a.y.l.f;
import java.util.List;
import java.util.Locale;
/* compiled from: Layer.java */
/* loaded from: classes.dex */
public class e {
    public final List<b> a;

    /* renamed from: b  reason: collision with root package name */
    public final d f434b;
    public final String c;
    public final long d;
    public final a e;
    public final long f;
    @Nullable
    public final String g;
    public final List<f> h;
    public final l i;
    public final int j;
    public final int k;
    public final int l;
    public final float m;
    public final float n;
    public final int o;
    public final int p;
    @Nullable
    public final j q;
    @Nullable
    public final k r;
    @Nullable

    /* renamed from: s  reason: collision with root package name */
    public final b.c.a.y.k.b f435s;
    public final List<b.c.a.c0.a<Float>> t;
    public final int u;
    public final boolean v;

    /* compiled from: Layer.java */
    /* loaded from: classes.dex */
    public enum a {
        PRE_COMP,
        SOLID,
        IMAGE,
        NULL,
        SHAPE,
        TEXT,
        UNKNOWN
    }

    /* JADX WARN: Incorrect types in method signature: (Ljava/util/List<Lb/c/a/y/l/b;>;Lb/c/a/d;Ljava/lang/String;JLb/c/a/y/m/e$a;JLjava/lang/String;Ljava/util/List<Lb/c/a/y/l/f;>;Lb/c/a/y/k/l;IIIFFIILb/c/a/y/k/j;Lb/c/a/y/k/k;Ljava/util/List<Lb/c/a/c0/a<Ljava/lang/Float;>;>;Ljava/lang/Object;Lb/c/a/y/k/b;Z)V */
    public e(List list, d dVar, String str, long j, a aVar, long j2, @Nullable String str2, List list2, l lVar, int i, int i2, int i3, float f, float f2, int i4, int i5, @Nullable j jVar, @Nullable k kVar, List list3, int i6, @Nullable b.c.a.y.k.b bVar, boolean z2) {
        this.a = list;
        this.f434b = dVar;
        this.c = str;
        this.d = j;
        this.e = aVar;
        this.f = j2;
        this.g = str2;
        this.h = list2;
        this.i = lVar;
        this.j = i;
        this.k = i2;
        this.l = i3;
        this.m = f;
        this.n = f2;
        this.o = i4;
        this.p = i5;
        this.q = jVar;
        this.r = kVar;
        this.t = list3;
        this.u = i6;
        this.f435s = bVar;
        this.v = z2;
    }

    public String a(String str) {
        StringBuilder R = b.d.b.a.a.R(str);
        R.append(this.c);
        R.append("\n");
        e e = this.f434b.e(this.f);
        if (e != null) {
            R.append("\t\tParents: ");
            R.append(e.c);
            e e2 = this.f434b.e(e.f);
            while (e2 != null) {
                R.append("->");
                R.append(e2.c);
                e2 = this.f434b.e(e2.f);
            }
            R.append(str);
            R.append("\n");
        }
        if (!this.h.isEmpty()) {
            R.append(str);
            R.append("\tMasks: ");
            R.append(this.h.size());
            R.append("\n");
        }
        if (!(this.j == 0 || this.k == 0)) {
            R.append(str);
            R.append("\tBackground: ");
            R.append(String.format(Locale.US, "%dx%d %X\n", Integer.valueOf(this.j), Integer.valueOf(this.k), Integer.valueOf(this.l)));
        }
        if (!this.a.isEmpty()) {
            R.append(str);
            R.append("\tShapes:\n");
            for (b bVar : this.a) {
                R.append(str);
                R.append("\t\t");
                R.append(bVar);
                R.append("\n");
            }
        }
        return R.toString();
    }

    public String toString() {
        return a("");
    }
}
