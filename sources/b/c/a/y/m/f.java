package b.c.a.y.m;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;
import b.c.a.j;
/* compiled from: NullLayer.java */
/* loaded from: classes.dex */
public class f extends b {
    public f(j jVar, e eVar) {
        super(jVar, eVar);
    }

    @Override // b.c.a.y.m.b, b.c.a.w.b.e
    public void d(RectF rectF, Matrix matrix, boolean z2) {
        super.d(rectF, matrix, z2);
        rectF.set(0.0f, 0.0f, 0.0f, 0.0f);
    }

    @Override // b.c.a.y.m.b
    public void j(Canvas canvas, Matrix matrix, int i) {
    }
}
