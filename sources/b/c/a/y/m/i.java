package b.c.a.y.m;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Typeface;
import androidx.annotation.Nullable;
import androidx.collection.LongSparseArray;
import b.c.a.b0.g;
import b.c.a.c0.c;
import b.c.a.d;
import b.c.a.j;
import b.c.a.o;
import b.c.a.w.c.n;
import b.c.a.w.c.p;
import b.c.a.y.e;
import b.c.a.y.k.k;
import b.c.a.y.l.m;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
/* compiled from: TextLayer.java */
/* loaded from: classes.dex */
public class i extends b.c.a.y.m.b {
    public final n E;
    public final j F;
    public final d G;
    @Nullable
    public b.c.a.w.c.a<Integer, Integer> H;
    @Nullable
    public b.c.a.w.c.a<Integer, Integer> I;
    @Nullable
    public b.c.a.w.c.a<Integer, Integer> J;
    @Nullable
    public b.c.a.w.c.a<Integer, Integer> K;
    @Nullable
    public b.c.a.w.c.a<Float, Float> L;
    @Nullable
    public b.c.a.w.c.a<Float, Float> M;
    @Nullable
    public b.c.a.w.c.a<Float, Float> N;
    @Nullable
    public b.c.a.w.c.a<Float, Float> O;
    @Nullable
    public b.c.a.w.c.a<Float, Float> P;

    /* renamed from: x  reason: collision with root package name */
    public final StringBuilder f440x = new StringBuilder(2);

    /* renamed from: y  reason: collision with root package name */
    public final RectF f441y = new RectF();

    /* renamed from: z  reason: collision with root package name */
    public final Matrix f442z = new Matrix();
    public final Paint A = new a(this, 1);
    public final Paint B = new b(this, 1);
    public final Map<e, List<b.c.a.w.b.d>> C = new HashMap();
    public final LongSparseArray<String> D = new LongSparseArray<>();

    /* compiled from: TextLayer.java */
    /* loaded from: classes.dex */
    public class a extends Paint {
        public a(i iVar, int i) {
            super(i);
            setStyle(Paint.Style.FILL);
        }
    }

    /* compiled from: TextLayer.java */
    /* loaded from: classes.dex */
    public class b extends Paint {
        public b(i iVar, int i) {
            super(i);
            setStyle(Paint.Style.STROKE);
        }
    }

    public i(j jVar, e eVar) {
        super(jVar, eVar);
        b.c.a.y.k.b bVar;
        b.c.a.y.k.b bVar2;
        b.c.a.y.k.a aVar;
        b.c.a.y.k.a aVar2;
        this.F = jVar;
        this.G = eVar.f434b;
        n nVar = new n(eVar.q.a);
        this.E = nVar;
        nVar.a.add(this);
        e(nVar);
        k kVar = eVar.r;
        if (!(kVar == null || (aVar2 = kVar.a) == null)) {
            b.c.a.w.c.a<Integer, Integer> a2 = aVar2.a();
            this.H = a2;
            a2.a.add(this);
            e(this.H);
        }
        if (!(kVar == null || (aVar = kVar.f409b) == null)) {
            b.c.a.w.c.a<Integer, Integer> a3 = aVar.a();
            this.J = a3;
            a3.a.add(this);
            e(this.J);
        }
        if (!(kVar == null || (bVar2 = kVar.c) == null)) {
            b.c.a.w.c.a<Float, Float> a4 = bVar2.a();
            this.L = a4;
            a4.a.add(this);
            e(this.L);
        }
        if (kVar != null && (bVar = kVar.d) != null) {
            b.c.a.w.c.a<Float, Float> a5 = bVar.a();
            this.N = a5;
            a5.a.add(this);
            e(this.N);
        }
    }

    @Override // b.c.a.y.m.b, b.c.a.w.b.e
    public void d(RectF rectF, Matrix matrix, boolean z2) {
        super.d(rectF, matrix, z2);
        rectF.set(0.0f, 0.0f, this.G.j.width(), this.G.j.height());
    }

    @Override // b.c.a.y.m.b, b.c.a.y.g
    public <T> void g(T t, @Nullable c<T> cVar) {
        this.v.c(t, cVar);
        if (t == o.a) {
            b.c.a.w.c.a<Integer, Integer> aVar = this.I;
            if (aVar != null) {
                this.u.remove(aVar);
            }
            if (cVar == null) {
                this.I = null;
                return;
            }
            p pVar = new p(cVar, null);
            this.I = pVar;
            pVar.a.add(this);
            e(this.I);
        } else if (t == o.f364b) {
            b.c.a.w.c.a<Integer, Integer> aVar2 = this.K;
            if (aVar2 != null) {
                this.u.remove(aVar2);
            }
            if (cVar == null) {
                this.K = null;
                return;
            }
            p pVar2 = new p(cVar, null);
            this.K = pVar2;
            pVar2.a.add(this);
            e(this.K);
        } else if (t == o.o) {
            b.c.a.w.c.a<Float, Float> aVar3 = this.M;
            if (aVar3 != null) {
                this.u.remove(aVar3);
            }
            if (cVar == null) {
                this.M = null;
                return;
            }
            p pVar3 = new p(cVar, null);
            this.M = pVar3;
            pVar3.a.add(this);
            e(this.M);
        } else if (t == o.p) {
            b.c.a.w.c.a<Float, Float> aVar4 = this.O;
            if (aVar4 != null) {
                this.u.remove(aVar4);
            }
            if (cVar == null) {
                this.O = null;
                return;
            }
            p pVar4 = new p(cVar, null);
            this.O = pVar4;
            pVar4.a.add(this);
            e(this.O);
        } else if (t == o.B) {
            b.c.a.w.c.a<Float, Float> aVar5 = this.P;
            if (aVar5 != null) {
                this.u.remove(aVar5);
            }
            if (cVar == null) {
                this.P = null;
                return;
            }
            p pVar5 = new p(cVar, null);
            this.P = pVar5;
            pVar5.a.add(this);
            e(this.P);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v4, types: [T, java.lang.String] */
    /* JADX WARN: Type inference failed for: r6v0, types: [T, java.lang.Object, java.lang.String] */
    @Override // b.c.a.y.m.b
    public void j(Canvas canvas, Matrix matrix, int i) {
        b.c.a.x.a aVar;
        float f;
        String str;
        float f2;
        float f3;
        List<String> list;
        int i2;
        String str2;
        List<b.c.a.w.b.d> list2;
        float f4;
        String str3;
        int i3;
        float f5;
        canvas.save();
        if (!(this.F.k.g.size() > 0)) {
            canvas.setMatrix(matrix);
        }
        b.c.a.y.c e = this.E.e();
        b.c.a.y.d dVar = this.G.e.get(e.f401b);
        if (dVar == null) {
            canvas.restore();
            return;
        }
        b.c.a.w.c.a<Integer, Integer> aVar2 = this.I;
        if (aVar2 != null) {
            this.A.setColor(aVar2.e().intValue());
        } else {
            b.c.a.w.c.a<Integer, Integer> aVar3 = this.H;
            if (aVar3 != null) {
                this.A.setColor(aVar3.e().intValue());
            } else {
                this.A.setColor(e.h);
            }
        }
        b.c.a.w.c.a<Integer, Integer> aVar4 = this.K;
        if (aVar4 != null) {
            this.B.setColor(aVar4.e().intValue());
        } else {
            b.c.a.w.c.a<Integer, Integer> aVar5 = this.J;
            if (aVar5 != null) {
                this.B.setColor(aVar5.e().intValue());
            } else {
                this.B.setColor(e.i);
            }
        }
        b.c.a.w.c.a<Integer, Integer> aVar6 = this.v.j;
        int intValue = ((aVar6 == null ? 100 : aVar6.e().intValue()) * 255) / 100;
        this.A.setAlpha(intValue);
        this.B.setAlpha(intValue);
        b.c.a.w.c.a<Float, Float> aVar7 = this.M;
        if (aVar7 != null) {
            this.B.setStrokeWidth(aVar7.e().floatValue());
        } else {
            b.c.a.w.c.a<Float, Float> aVar8 = this.L;
            if (aVar8 != null) {
                this.B.setStrokeWidth(aVar8.e().floatValue());
            } else {
                this.B.setStrokeWidth(g.c() * e.j * g.d(matrix));
            }
        }
        if (this.F.k.g.size() > 0) {
            b.c.a.w.c.a<Float, Float> aVar9 = this.P;
            if (aVar9 != null) {
                f3 = aVar9.e().floatValue();
            } else {
                f3 = e.c;
            }
            float f6 = f3 / 100.0f;
            float d = g.d(matrix);
            String str4 = e.a;
            float c = g.c() * e.f;
            List<String> t = t(str4);
            int size = t.size();
            int i4 = 0;
            while (i4 < size) {
                String str5 = t.get(i4);
                float f7 = 0.0f;
                int i5 = 0;
                while (i5 < str5.length()) {
                    e eVar = this.G.g.get(e.a(str5.charAt(i5), dVar.a, dVar.c));
                    if (eVar == null) {
                        f5 = c;
                        i3 = i4;
                        str3 = str5;
                    } else {
                        str3 = str5;
                        double d2 = eVar.c;
                        f5 = c;
                        i3 = i4;
                        f7 = (float) ((d2 * f6 * g.c() * d) + f7);
                    }
                    i5++;
                    str5 = str3;
                    c = f5;
                    i4 = i3;
                }
                float f8 = c;
                int i6 = i4;
                String str6 = str5;
                canvas.save();
                q(e.d, canvas, f7);
                canvas.translate(0.0f, (i6 * f8) - (((size - 1) * f8) / 2.0f));
                int i7 = 0;
                while (i7 < str6.length()) {
                    String str7 = str6;
                    e eVar2 = this.G.g.get(e.a(str7.charAt(i7), dVar.a, dVar.c));
                    if (eVar2 == null) {
                        list = t;
                        i2 = size;
                        str2 = str7;
                    } else {
                        if (this.C.containsKey(eVar2)) {
                            list2 = this.C.get(eVar2);
                            list = t;
                            i2 = size;
                            str2 = str7;
                        } else {
                            List<m> list3 = eVar2.a;
                            int size2 = list3.size();
                            ArrayList arrayList = new ArrayList(size2);
                            list = t;
                            for (int i8 = 0; i8 < size2; i8++) {
                                size = size;
                                str7 = str7;
                                list3 = list3;
                                arrayList.add(new b.c.a.w.b.d(this.F, this, list3.get(i8)));
                            }
                            i2 = size;
                            str2 = str7;
                            this.C.put(eVar2, arrayList);
                            list2 = arrayList;
                        }
                        for (int i9 = 0; i9 < list2.size(); i9++) {
                            Path path = list2.get(i9).getPath();
                            path.computeBounds(this.f441y, false);
                            this.f442z.set(matrix);
                            list2 = list2;
                            this.f442z.preTranslate(0.0f, (-e.g) * g.c());
                            this.f442z.preScale(f6, f6);
                            path.transform(this.f442z);
                            if (e.k) {
                                s(path, this.A, canvas);
                                s(path, this.B, canvas);
                            } else {
                                s(path, this.B, canvas);
                                s(path, this.A, canvas);
                            }
                        }
                        float c2 = g.c() * ((float) eVar2.c) * f6 * d;
                        float f9 = e.e / 10.0f;
                        b.c.a.w.c.a<Float, Float> aVar10 = this.O;
                        if (aVar10 != null) {
                            f4 = aVar10.e().floatValue();
                        } else {
                            b.c.a.w.c.a<Float, Float> aVar11 = this.N;
                            if (aVar11 != null) {
                                f4 = aVar11.e().floatValue();
                            }
                            canvas.translate((f9 * d) + c2, 0.0f);
                        }
                        f9 += f4;
                        canvas.translate((f9 * d) + c2, 0.0f);
                    }
                    i7++;
                    t = list;
                    str6 = str2;
                    size = i2;
                }
                canvas.restore();
                i4 = i6 + 1;
                c = f8;
            }
        } else {
            float d3 = g.d(matrix);
            j jVar = this.F;
            ?? r6 = dVar.a;
            ?? r3 = dVar.c;
            Typeface typeface = null;
            if (jVar.getCallback() == null) {
                aVar = null;
            } else {
                if (jVar.v == null) {
                    jVar.v = new b.c.a.x.a(jVar.getCallback());
                }
                aVar = jVar.v;
            }
            if (aVar != null) {
                b.c.a.y.j<String> jVar2 = aVar.a;
                jVar2.a = r6;
                jVar2.f407b = r3;
                typeface = aVar.f393b.get(jVar2);
                if (typeface == null) {
                    Typeface typeface2 = aVar.c.get(r6);
                    if (typeface2 == null) {
                        StringBuilder V = b.d.b.a.a.V("fonts/", r6);
                        V.append(aVar.e);
                        typeface2 = Typeface.createFromAsset(aVar.d, V.toString());
                        aVar.c.put(r6, typeface2);
                    }
                    boolean contains = r3.contains("Italic");
                    boolean contains2 = r3.contains("Bold");
                    int i10 = (!contains || !contains2) ? contains ? 2 : contains2 ? 1 : 0 : 3;
                    typeface = typeface2.getStyle() == i10 ? typeface2 : Typeface.create(typeface2, i10);
                    aVar.f393b.put(aVar.a, typeface);
                }
            }
            if (typeface != null) {
                String str8 = e.a;
                Objects.requireNonNull(this.F);
                this.A.setTypeface(typeface);
                b.c.a.w.c.a<Float, Float> aVar12 = this.P;
                if (aVar12 != null) {
                    f = aVar12.e().floatValue();
                } else {
                    f = e.c;
                }
                this.A.setTextSize(g.c() * f);
                this.B.setTypeface(this.A.getTypeface());
                this.B.setTextSize(this.A.getTextSize());
                float c3 = g.c() * e.f;
                List<String> t2 = t(str8);
                int size3 = t2.size();
                for (int i11 = 0; i11 < size3; i11++) {
                    String str9 = t2.get(i11);
                    q(e.d, canvas, this.B.measureText(str9));
                    canvas.translate(0.0f, (i11 * c3) - (((size3 - 1) * c3) / 2.0f));
                    int i12 = 0;
                    while (i12 < str9.length()) {
                        int codePointAt = str9.codePointAt(i12);
                        int charCount = Character.charCount(codePointAt) + i12;
                        while (charCount < str9.length()) {
                            int codePointAt2 = str9.codePointAt(charCount);
                            if (!(Character.getType(codePointAt2) == 16 || Character.getType(codePointAt2) == 27 || Character.getType(codePointAt2) == 6 || Character.getType(codePointAt2) == 28 || Character.getType(codePointAt2) == 19)) {
                                break;
                            }
                            charCount += Character.charCount(codePointAt2);
                            codePointAt = (codePointAt * 31) + codePointAt2;
                        }
                        size3 = size3;
                        c3 = c3;
                        long j = codePointAt;
                        if (this.D.containsKey(j)) {
                            str = this.D.get(j);
                        } else {
                            this.f440x.setLength(0);
                            int i13 = i12;
                            while (i13 < charCount) {
                                int codePointAt3 = str9.codePointAt(i13);
                                this.f440x.appendCodePoint(codePointAt3);
                                i13 += Character.charCount(codePointAt3);
                            }
                            String sb = this.f440x.toString();
                            this.D.put(j, sb);
                            str = sb;
                        }
                        i12 += str.length();
                        if (e.k) {
                            r(str, this.A, canvas);
                            r(str, this.B, canvas);
                        } else {
                            r(str, this.B, canvas);
                            r(str, this.A, canvas);
                        }
                        float measureText = this.A.measureText(str, 0, 1);
                        float f10 = e.e / 10.0f;
                        b.c.a.w.c.a<Float, Float> aVar13 = this.O;
                        if (aVar13 != null) {
                            f2 = aVar13.e().floatValue();
                        } else {
                            b.c.a.w.c.a<Float, Float> aVar14 = this.N;
                            if (aVar14 != null) {
                                f2 = aVar14.e().floatValue();
                            } else {
                                canvas.translate((f10 * d3) + measureText, 0.0f);
                            }
                        }
                        f10 += f2;
                        canvas.translate((f10 * d3) + measureText, 0.0f);
                    }
                    canvas.setMatrix(matrix);
                }
            }
        }
        canvas.restore();
    }

    public final void q(int i, Canvas canvas, float f) {
        int h = b.c.a.y.b.h(i);
        if (h == 1) {
            canvas.translate(-f, 0.0f);
        } else if (h == 2) {
            canvas.translate((-f) / 2.0f, 0.0f);
        }
    }

    public final void r(String str, Paint paint, Canvas canvas) {
        if (paint.getColor() != 0) {
            if (paint.getStyle() != Paint.Style.STROKE || paint.getStrokeWidth() != 0.0f) {
                canvas.drawText(str, 0, str.length(), 0.0f, 0.0f, paint);
            }
        }
    }

    public final void s(Path path, Paint paint, Canvas canvas) {
        if (paint.getColor() != 0) {
            if (paint.getStyle() != Paint.Style.STROKE || paint.getStrokeWidth() != 0.0f) {
                canvas.drawPath(path, paint);
            }
        }
    }

    public final List<String> t(String str) {
        return Arrays.asList(str.replaceAll("\r\n", "\r").replaceAll("\n", "\r").split("\r"));
    }
}
