package b.c.a.y.m;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import androidx.annotation.FloatRange;
import androidx.annotation.Nullable;
import androidx.collection.LongSparseArray;
import b.c.a.b0.g;
import b.c.a.d;
import b.c.a.j;
import b.c.a.o;
import b.c.a.w.c.a;
import b.c.a.w.c.p;
import b.c.a.y.f;
import b.c.a.y.k.b;
import java.util.ArrayList;
import java.util.List;
/* compiled from: CompositionLayer.java */
/* loaded from: classes.dex */
public class c extends b {
    @Nullable

    /* renamed from: x  reason: collision with root package name */
    public a<Float, Float> f428x;

    /* renamed from: y  reason: collision with root package name */
    public final List<b> f429y = new ArrayList();

    /* renamed from: z  reason: collision with root package name */
    public final RectF f430z = new RectF();
    public final RectF A = new RectF();
    public Paint B = new Paint();

    public c(j jVar, e eVar, List<e> list, d dVar) {
        super(jVar, eVar);
        int i;
        b bVar;
        b bVar2;
        b bVar3 = eVar.f435s;
        if (bVar3 != null) {
            a<Float, Float> a = bVar3.a();
            this.f428x = a;
            e(a);
            this.f428x.a.add(this);
        } else {
            this.f428x = null;
        }
        LongSparseArray longSparseArray = new LongSparseArray(dVar.i.size());
        int size = list.size() - 1;
        b bVar4 = null;
        while (true) {
            if (size >= 0) {
                e eVar2 = list.get(size);
                int ordinal = eVar2.e.ordinal();
                if (ordinal == 0) {
                    bVar2 = new c(jVar, eVar2, dVar.c.get(eVar2.g), dVar);
                } else if (ordinal == 1) {
                    bVar2 = new h(jVar, eVar2);
                } else if (ordinal == 2) {
                    bVar2 = new d(jVar, eVar2);
                } else if (ordinal == 3) {
                    bVar2 = new f(jVar, eVar2);
                } else if (ordinal == 4) {
                    bVar2 = new g(jVar, eVar2);
                } else if (ordinal != 5) {
                    StringBuilder R = b.d.b.a.a.R("Unknown layer type ");
                    R.append(eVar2.e);
                    b.c.a.b0.c.b(R.toString());
                    bVar2 = null;
                } else {
                    bVar2 = new i(jVar, eVar2);
                }
                if (bVar2 != null) {
                    longSparseArray.put(bVar2.o.d, bVar2);
                    if (bVar4 != null) {
                        bVar4.r = bVar2;
                        bVar4 = null;
                    } else {
                        this.f429y.add(0, bVar2);
                        int h = b.c.a.y.b.h(eVar2.u);
                        if (h == 1 || h == 2) {
                            bVar4 = bVar2;
                        }
                    }
                }
                size--;
            }
        }
        for (i = 0; i < longSparseArray.size(); i++) {
            b bVar5 = (b) longSparseArray.get(longSparseArray.keyAt(i));
            if (!(bVar5 == null || (bVar = (b) longSparseArray.get(bVar5.o.f)) == null)) {
                bVar5.f427s = bVar;
            }
        }
    }

    @Override // b.c.a.y.m.b, b.c.a.w.b.e
    public void d(RectF rectF, Matrix matrix, boolean z2) {
        super.d(rectF, matrix, z2);
        for (int size = this.f429y.size() - 1; size >= 0; size--) {
            this.f430z.set(0.0f, 0.0f, 0.0f, 0.0f);
            this.f429y.get(size).d(this.f430z, this.m, true);
            rectF.union(this.f430z);
        }
    }

    @Override // b.c.a.y.m.b, b.c.a.y.g
    public <T> void g(T t, @Nullable b.c.a.c0.c<T> cVar) {
        this.v.c(t, cVar);
        if (t != o.A) {
            return;
        }
        if (cVar == null) {
            a<Float, Float> aVar = this.f428x;
            if (aVar != null) {
                aVar.i(null);
                return;
            }
            return;
        }
        p pVar = new p(cVar, null);
        this.f428x = pVar;
        pVar.a.add(this);
        e(this.f428x);
    }

    @Override // b.c.a.y.m.b
    public void j(Canvas canvas, Matrix matrix, int i) {
        RectF rectF = this.A;
        e eVar = this.o;
        rectF.set(0.0f, 0.0f, eVar.o, eVar.p);
        matrix.mapRect(this.A);
        boolean z2 = this.n.A && this.f429y.size() > 1 && i != 255;
        if (z2) {
            this.B.setAlpha(i);
            g.f(canvas, this.A, this.B, 31);
        } else {
            canvas.save();
        }
        if (z2) {
            i = 255;
        }
        for (int size = this.f429y.size() - 1; size >= 0; size--) {
            if (!this.A.isEmpty() ? canvas.clipRect(this.A) : true) {
                this.f429y.get(size).f(canvas, matrix, i);
            }
        }
        canvas.restore();
        b.c.a.c.a("CompositionLayer#draw");
    }

    @Override // b.c.a.y.m.b
    public void n(f fVar, int i, List<f> list, f fVar2) {
        for (int i2 = 0; i2 < this.f429y.size(); i2++) {
            this.f429y.get(i2).c(fVar, i, list, fVar2);
        }
    }

    @Override // b.c.a.y.m.b
    public void o(@FloatRange(from = 0.0d, to = 1.0d) float f) {
        super.o(f);
        if (this.f428x != null) {
            f = ((this.f428x.e().floatValue() * this.o.f434b.m) - this.o.f434b.k) / (this.n.k.c() + 0.01f);
        }
        if (this.f428x == null) {
            e eVar = this.o;
            f -= eVar.n / eVar.f434b.c();
        }
        float f2 = this.o.m;
        if (f2 != 0.0f) {
            f /= f2;
        }
        int size = this.f429y.size();
        while (true) {
            size--;
            if (size >= 0) {
                this.f429y.get(size).o(f);
            } else {
                return;
            }
        }
    }
}
