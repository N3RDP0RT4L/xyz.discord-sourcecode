package b.c.a.y.m;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;
import androidx.annotation.NonNull;
import b.c.a.j;
import b.c.a.w.b.d;
import b.c.a.y.f;
import b.c.a.y.l.m;
import java.util.Collections;
import java.util.List;
/* compiled from: ShapeLayer.java */
/* loaded from: classes.dex */
public class g extends b {

    /* renamed from: x  reason: collision with root package name */
    public final d f436x;

    public g(j jVar, e eVar) {
        super(jVar, eVar);
        d dVar = new d(jVar, this, new m("__container", eVar.a, false));
        this.f436x = dVar;
        dVar.b(Collections.emptyList(), Collections.emptyList());
    }

    @Override // b.c.a.y.m.b, b.c.a.w.b.e
    public void d(RectF rectF, Matrix matrix, boolean z2) {
        super.d(rectF, matrix, z2);
        this.f436x.d(rectF, this.m, z2);
    }

    @Override // b.c.a.y.m.b
    public void j(@NonNull Canvas canvas, Matrix matrix, int i) {
        this.f436x.f(canvas, matrix, i);
    }

    @Override // b.c.a.y.m.b
    public void n(f fVar, int i, List<f> list, f fVar2) {
        this.f436x.c(fVar, i, list, fVar2);
    }
}
