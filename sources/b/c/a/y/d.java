package b.c.a.y;

import androidx.annotation.RestrictTo;
/* compiled from: Font.java */
@RestrictTo({RestrictTo.Scope.LIBRARY})
/* loaded from: classes.dex */
public class d {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final String f402b;
    public final String c;

    public d(String str, String str2, String str3, float f) {
        this.a = str;
        this.f402b = str2;
        this.c = str3;
    }
}
