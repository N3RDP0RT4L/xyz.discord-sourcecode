package b.c.a.y;

import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;
import androidx.core.util.Pair;
import b.d.b.a.a;
/* compiled from: MutablePair.java */
@RestrictTo({RestrictTo.Scope.LIBRARY})
/* loaded from: classes.dex */
public class j<T> {
    @Nullable
    public T a;
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public T f407b;

    public boolean equals(Object obj) {
        if (!(obj instanceof Pair)) {
            return false;
        }
        Pair pair = (Pair) obj;
        F f = pair.first;
        Object obj2 = this.a;
        if (!(f == obj2 || (f != 0 && f.equals(obj2)))) {
            return false;
        }
        S s2 = pair.second;
        Object obj3 = this.f407b;
        return s2 == obj3 || (s2 != 0 && s2.equals(obj3));
    }

    public int hashCode() {
        T t = this.a;
        int i = 0;
        int hashCode = t == null ? 0 : t.hashCode();
        T t2 = this.f407b;
        if (t2 != null) {
            i = t2.hashCode();
        }
        return hashCode ^ i;
    }

    public String toString() {
        StringBuilder R = a.R("Pair{");
        R.append(String.valueOf(this.a));
        R.append(" ");
        R.append(String.valueOf(this.f407b));
        R.append("}");
        return R.toString();
    }
}
