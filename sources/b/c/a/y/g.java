package b.c.a.y;

import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;
import b.c.a.c0.c;
import java.util.List;
/* compiled from: KeyPathElement.java */
@RestrictTo({RestrictTo.Scope.LIBRARY})
/* loaded from: classes.dex */
public interface g {
    void c(f fVar, int i, List<f> list, f fVar2);

    <T> void g(T t, @Nullable c<T> cVar);
}
