package b.c.a.y;

import androidx.annotation.ColorInt;
import androidx.annotation.RestrictTo;
import b.d.b.a.a;
/* compiled from: DocumentData.java */
@RestrictTo({RestrictTo.Scope.LIBRARY})
/* loaded from: classes.dex */
public class c {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final String f401b;
    public final float c;
    public final int d;
    public final int e;
    public final float f;
    public final float g;
    @ColorInt
    public final int h;
    @ColorInt
    public final int i;
    public final float j;
    public final boolean k;

    public c(String str, String str2, float f, int i, int i2, float f2, float f3, @ColorInt int i3, @ColorInt int i4, float f4, boolean z2) {
        this.a = str;
        this.f401b = str2;
        this.c = f;
        this.d = i;
        this.e = i2;
        this.f = f2;
        this.g = f3;
        this.h = i3;
        this.i = i4;
        this.j = f4;
        this.k = z2;
    }

    public int hashCode() {
        String str = this.f401b;
        int h = ((b.h(this.d) + (((int) (a.m(str, this.a.hashCode() * 31, 31) + this.c)) * 31)) * 31) + this.e;
        long floatToRawIntBits = Float.floatToRawIntBits(this.f);
        return (((h * 31) + ((int) (floatToRawIntBits ^ (floatToRawIntBits >>> 32)))) * 31) + this.h;
    }
}
