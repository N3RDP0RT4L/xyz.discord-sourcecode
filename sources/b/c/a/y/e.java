package b.c.a.y;

import androidx.annotation.RestrictTo;
import b.c.a.y.l.m;
import b.d.b.a.a;
import java.util.List;
/* compiled from: FontCharacter.java */
@RestrictTo({RestrictTo.Scope.LIBRARY})
/* loaded from: classes.dex */
public class e {
    public final List<m> a;

    /* renamed from: b  reason: collision with root package name */
    public final char f403b;
    public final double c;
    public final String d;
    public final String e;

    public e(List<m> list, char c, double d, double d2, String str, String str2) {
        this.a = list;
        this.f403b = c;
        this.c = d2;
        this.d = str;
        this.e = str2;
    }

    public static int a(char c, String str, String str2) {
        return str2.hashCode() + a.m(str, (c + 0) * 31, 31);
    }

    public int hashCode() {
        return a(this.f403b, this.e, this.d);
    }
}
