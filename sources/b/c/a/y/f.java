package b.c.a.y;

import androidx.annotation.CheckResult;
import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;
import b.d.b.a.a;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/* compiled from: KeyPath.java */
/* loaded from: classes.dex */
public class f {
    public static final f a = new f("COMPOSITION");

    /* renamed from: b  reason: collision with root package name */
    public final List<String> f404b;
    @Nullable
    public g c;

    public f(String... strArr) {
        this.f404b = Arrays.asList(strArr);
    }

    @CheckResult
    @RestrictTo({RestrictTo.Scope.LIBRARY})
    public f a(String str) {
        f fVar = new f(this);
        fVar.f404b.add(str);
        return fVar;
    }

    public final boolean b() {
        List<String> list = this.f404b;
        return list.get(list.size() - 1).equals("**");
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY})
    public boolean c(String str, int i) {
        if (i >= this.f404b.size()) {
            return false;
        }
        boolean z2 = i == this.f404b.size() - 1;
        String str2 = this.f404b.get(i);
        if (!str2.equals("**")) {
            return (z2 || (i == this.f404b.size() + (-2) && b())) && (str2.equals(str) || str2.equals("*"));
        }
        if (!z2 && this.f404b.get(i + 1).equals(str)) {
            return i == this.f404b.size() + (-2) || (i == this.f404b.size() + (-3) && b());
        }
        if (z2) {
            return true;
        }
        int i2 = i + 1;
        if (i2 < this.f404b.size() - 1) {
            return false;
        }
        return this.f404b.get(i2).equals(str);
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY})
    public int d(String str, int i) {
        if ("__container".equals(str)) {
            return 0;
        }
        if (!this.f404b.get(i).equals("**")) {
            return 1;
        }
        return (i != this.f404b.size() - 1 && this.f404b.get(i + 1).equals(str)) ? 2 : 0;
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY})
    public boolean e(String str, int i) {
        if ("__container".equals(str)) {
            return true;
        }
        if (i >= this.f404b.size()) {
            return false;
        }
        return this.f404b.get(i).equals(str) || this.f404b.get(i).equals("**") || this.f404b.get(i).equals("*");
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY})
    public boolean f(String str, int i) {
        return "__container".equals(str) || i < this.f404b.size() - 1 || this.f404b.get(i).equals("**");
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY})
    public f g(g gVar) {
        f fVar = new f(this);
        fVar.c = gVar;
        return fVar;
    }

    public String toString() {
        StringBuilder R = a.R("KeyPath{keys=");
        R.append(this.f404b);
        R.append(",resolved=");
        R.append(this.c != null);
        R.append('}');
        return R.toString();
    }

    public f(f fVar) {
        this.f404b = new ArrayList(fVar.f404b);
        this.c = fVar.c;
    }
}
