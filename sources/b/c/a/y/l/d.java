package b.c.a.y.l;

import android.graphics.Path;
import b.c.a.j;
import b.c.a.w.b.h;
import b.c.a.y.k.b;
import b.c.a.y.k.c;
import b.c.a.y.k.f;
/* compiled from: GradientFill.java */
/* loaded from: classes.dex */
public class d implements b {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final Path.FillType f413b;
    public final c c;
    public final b.c.a.y.k.d d;
    public final f e;
    public final f f;
    public final String g;
    public final boolean h;

    public d(String str, int i, Path.FillType fillType, c cVar, b.c.a.y.k.d dVar, f fVar, f fVar2, b bVar, b bVar2, boolean z2) {
        this.a = i;
        this.f413b = fillType;
        this.c = cVar;
        this.d = dVar;
        this.e = fVar;
        this.f = fVar2;
        this.g = str;
        this.h = z2;
    }

    @Override // b.c.a.y.l.b
    public b.c.a.w.b.c a(j jVar, b.c.a.y.m.b bVar) {
        return new h(jVar, bVar, this);
    }
}
