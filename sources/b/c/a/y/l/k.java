package b.c.a.y.l;

import android.graphics.PointF;
import b.c.a.y.a;
import java.util.ArrayList;
import java.util.List;
/* compiled from: ShapeData.java */
/* loaded from: classes.dex */
public class k {
    public final List<a> a;

    /* renamed from: b  reason: collision with root package name */
    public PointF f420b;
    public boolean c;

    public k(PointF pointF, boolean z2, List<a> list) {
        this.f420b = pointF;
        this.c = z2;
        this.a = new ArrayList(list);
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("ShapeData{numCurves=");
        R.append(this.a.size());
        R.append("closed=");
        R.append(this.c);
        R.append('}');
        return R.toString();
    }

    public k() {
        this.a = new ArrayList();
    }
}
