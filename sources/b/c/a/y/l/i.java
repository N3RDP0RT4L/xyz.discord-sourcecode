package b.c.a.y.l;

import android.graphics.PointF;
import b.c.a.j;
import b.c.a.w.b.c;
import b.c.a.w.b.o;
import b.c.a.y.k.b;
import b.c.a.y.k.f;
import b.c.a.y.k.m;
import b.d.b.a.a;
/* compiled from: RectangleShape.java */
/* loaded from: classes.dex */
public class i implements b {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final m<PointF, PointF> f418b;
    public final f c;
    public final b d;
    public final boolean e;

    public i(String str, m<PointF, PointF> mVar, f fVar, b bVar, boolean z2) {
        this.a = str;
        this.f418b = mVar;
        this.c = fVar;
        this.d = bVar;
        this.e = z2;
    }

    @Override // b.c.a.y.l.b
    public c a(j jVar, b.c.a.y.m.b bVar) {
        return new o(jVar, bVar, this);
    }

    public String toString() {
        StringBuilder R = a.R("RectangleShape{position=");
        R.append(this.f418b);
        R.append(", size=");
        R.append(this.c);
        R.append('}');
        return R.toString();
    }
}
