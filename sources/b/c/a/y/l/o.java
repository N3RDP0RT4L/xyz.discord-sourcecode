package b.c.a.y.l;

import androidx.annotation.Nullable;
import b.c.a.j;
import b.c.a.w.b.c;
import b.c.a.w.b.r;
import b.c.a.y.k.a;
import b.c.a.y.k.b;
import b.c.a.y.k.d;
import java.util.List;
/* compiled from: ShapeStroke.java */
/* loaded from: classes.dex */
public class o implements b {
    public final String a;
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public final b f424b;
    public final List<b> c;
    public final a d;
    public final d e;
    public final b f;
    public final int g;
    public final int h;
    public final float i;
    public final boolean j;

    /* JADX WARN: Incorrect types in method signature: (Ljava/lang/String;Lb/c/a/y/k/b;Ljava/util/List<Lb/c/a/y/k/b;>;Lb/c/a/y/k/a;Lb/c/a/y/k/d;Lb/c/a/y/k/b;Ljava/lang/Object;Ljava/lang/Object;FZ)V */
    public o(String str, @Nullable b bVar, List list, a aVar, d dVar, b bVar2, int i, int i2, float f, boolean z2) {
        this.a = str;
        this.f424b = bVar;
        this.c = list;
        this.d = aVar;
        this.e = dVar;
        this.f = bVar2;
        this.g = i;
        this.h = i2;
        this.i = f;
        this.j = z2;
    }

    @Override // b.c.a.y.l.b
    public c a(j jVar, b.c.a.y.m.b bVar) {
        return new r(jVar, bVar, this);
    }
}
