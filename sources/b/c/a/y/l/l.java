package b.c.a.y.l;

import android.graphics.Path;
import androidx.annotation.Nullable;
import b.c.a.j;
import b.c.a.w.b.c;
import b.c.a.w.b.g;
import b.c.a.y.k.a;
import b.c.a.y.k.d;
import b.c.a.y.m.b;
/* compiled from: ShapeFill.java */
/* loaded from: classes.dex */
public class l implements b {
    public final boolean a;

    /* renamed from: b  reason: collision with root package name */
    public final Path.FillType f421b;
    public final String c;
    @Nullable
    public final a d;
    @Nullable
    public final d e;
    public final boolean f;

    public l(String str, boolean z2, Path.FillType fillType, @Nullable a aVar, @Nullable d dVar, boolean z3) {
        this.c = str;
        this.a = z2;
        this.f421b = fillType;
        this.d = aVar;
        this.e = dVar;
        this.f = z3;
    }

    @Override // b.c.a.y.l.b
    public c a(j jVar, b bVar) {
        return new g(jVar, bVar, this);
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("ShapeFill{color=, fillEnabled=");
        R.append(this.a);
        R.append('}');
        return R.toString();
    }
}
