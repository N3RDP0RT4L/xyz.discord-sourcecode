package b.c.a.y.l;

import b.c.a.j;
import b.c.a.w.b.c;
import b.c.a.w.b.d;
import b.c.a.y.m.b;
import b.d.b.a.a;
import java.util.Arrays;
import java.util.List;
/* compiled from: ShapeGroup.java */
/* loaded from: classes.dex */
public class m implements b {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final List<b> f422b;
    public final boolean c;

    public m(String str, List<b> list, boolean z2) {
        this.a = str;
        this.f422b = list;
        this.c = z2;
    }

    @Override // b.c.a.y.l.b
    public c a(j jVar, b bVar) {
        return new d(jVar, bVar, this);
    }

    public String toString() {
        StringBuilder R = a.R("ShapeGroup{name='");
        R.append(this.a);
        R.append("' Shapes: ");
        R.append(Arrays.toString(this.f422b.toArray()));
        R.append('}');
        return R.toString();
    }
}
