package b.c.a.y.l;

import b.c.a.j;
import b.c.a.w.b.c;
import b.c.a.w.b.s;
import b.c.a.y.k.b;
import b.d.b.a.a;
/* compiled from: ShapeTrimPath.java */
/* loaded from: classes.dex */
public class p implements b {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final int f425b;
    public final b c;
    public final b d;
    public final b e;
    public final boolean f;

    public p(String str, int i, b bVar, b bVar2, b bVar3, boolean z2) {
        this.a = str;
        this.f425b = i;
        this.c = bVar;
        this.d = bVar2;
        this.e = bVar3;
        this.f = z2;
    }

    @Override // b.c.a.y.l.b
    public c a(j jVar, b.c.a.y.m.b bVar) {
        return new s(bVar, this);
    }

    public String toString() {
        StringBuilder R = a.R("Trim Path: {start: ");
        R.append(this.c);
        R.append(", end: ");
        R.append(this.d);
        R.append(", offset: ");
        R.append(this.e);
        R.append("}");
        return R.toString();
    }
}
