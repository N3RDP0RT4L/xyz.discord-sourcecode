package b.c.a.y.l;

import android.graphics.PointF;
import b.c.a.j;
import b.c.a.w.b.c;
import b.c.a.y.k.f;
import b.c.a.y.k.m;
import b.c.a.y.m.b;
/* compiled from: CircleShape.java */
/* loaded from: classes.dex */
public class a implements b {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final m<PointF, PointF> f411b;
    public final f c;
    public final boolean d;
    public final boolean e;

    public a(String str, m<PointF, PointF> mVar, f fVar, boolean z2, boolean z3) {
        this.a = str;
        this.f411b = mVar;
        this.c = fVar;
        this.d = z2;
        this.e = z3;
    }

    @Override // b.c.a.y.l.b
    public c a(j jVar, b bVar) {
        return new b.c.a.w.b.f(jVar, bVar, this);
    }
}
