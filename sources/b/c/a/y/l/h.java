package b.c.a.y.l;

import android.graphics.PointF;
import b.c.a.j;
import b.c.a.w.b.c;
import b.c.a.w.b.n;
import b.c.a.y.k.b;
import b.c.a.y.k.m;
/* compiled from: PolystarShape.java */
/* loaded from: classes.dex */
public class h implements b {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final a f417b;
    public final b c;
    public final m<PointF, PointF> d;
    public final b e;
    public final b f;
    public final b g;
    public final b h;
    public final b i;
    public final boolean j;

    /* compiled from: PolystarShape.java */
    /* loaded from: classes.dex */
    public enum a {
        STAR(1),
        POLYGON(2);
        
        private final int value;

        a(int i) {
            this.value = i;
        }

        public static a f(int i) {
            a[] values = values();
            for (int i2 = 0; i2 < 2; i2++) {
                a aVar = values[i2];
                if (aVar.value == i) {
                    return aVar;
                }
            }
            return null;
        }
    }

    public h(String str, a aVar, b bVar, m<PointF, PointF> mVar, b bVar2, b bVar3, b bVar4, b bVar5, b bVar6, boolean z2) {
        this.a = str;
        this.f417b = aVar;
        this.c = bVar;
        this.d = mVar;
        this.e = bVar2;
        this.f = bVar3;
        this.g = bVar4;
        this.h = bVar5;
        this.i = bVar6;
        this.j = z2;
    }

    @Override // b.c.a.y.l.b
    public c a(j jVar, b.c.a.y.m.b bVar) {
        return new n(jVar, bVar, this);
    }
}
