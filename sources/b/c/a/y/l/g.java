package b.c.a.y.l;

import androidx.annotation.Nullable;
import b.c.a.j;
import b.c.a.w.b.c;
import b.c.a.w.b.l;
import b.c.a.y.m.b;
/* compiled from: MergePaths.java */
/* loaded from: classes.dex */
public class g implements b {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final a f416b;
    public final boolean c;

    /* compiled from: MergePaths.java */
    /* loaded from: classes.dex */
    public enum a {
        MERGE,
        ADD,
        SUBTRACT,
        INTERSECT,
        EXCLUDE_INTERSECTIONS
    }

    public g(String str, a aVar, boolean z2) {
        this.a = str;
        this.f416b = aVar;
        this.c = z2;
    }

    @Override // b.c.a.y.l.b
    @Nullable
    public c a(j jVar, b bVar) {
        if (jVar.w) {
            return new l(this);
        }
        b.c.a.b0.c.b("Animation contains merge paths but they are disabled.");
        return null;
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("MergePaths{mode=");
        R.append(this.f416b);
        R.append('}');
        return R.toString();
    }
}
