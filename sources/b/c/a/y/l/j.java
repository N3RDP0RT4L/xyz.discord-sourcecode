package b.c.a.y.l;

import androidx.annotation.Nullable;
import b.c.a.w.b.c;
import b.c.a.w.b.p;
import b.c.a.y.k.b;
import b.c.a.y.k.l;
/* compiled from: Repeater.java */
/* loaded from: classes.dex */
public class j implements b {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final b f419b;
    public final b c;
    public final l d;
    public final boolean e;

    public j(String str, b bVar, b bVar2, l lVar, boolean z2) {
        this.a = str;
        this.f419b = bVar;
        this.c = bVar2;
        this.d = lVar;
        this.e = z2;
    }

    @Override // b.c.a.y.l.b
    @Nullable
    public c a(b.c.a.j jVar, b.c.a.y.m.b bVar) {
        return new p(jVar, bVar, this);
    }
}
