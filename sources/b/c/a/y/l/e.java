package b.c.a.y.l;

import androidx.annotation.Nullable;
import b.c.a.j;
import b.c.a.w.b.i;
import b.c.a.y.k.b;
import b.c.a.y.k.c;
import b.c.a.y.k.d;
import b.c.a.y.k.f;
import java.util.List;
/* compiled from: GradientStroke.java */
/* loaded from: classes.dex */
public class e implements b {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final int f414b;
    public final c c;
    public final d d;
    public final f e;
    public final f f;
    public final b g;
    public final int h;
    public final int i;
    public final float j;
    public final List<b> k;
    @Nullable
    public final b l;
    public final boolean m;

    /* JADX WARN: Incorrect types in method signature: (Ljava/lang/String;Ljava/lang/Object;Lb/c/a/y/k/c;Lb/c/a/y/k/d;Lb/c/a/y/k/f;Lb/c/a/y/k/f;Lb/c/a/y/k/b;Ljava/lang/Object;Ljava/lang/Object;FLjava/util/List<Lb/c/a/y/k/b;>;Lb/c/a/y/k/b;Z)V */
    public e(String str, int i, c cVar, d dVar, f fVar, f fVar2, b bVar, int i2, int i3, float f, List list, @Nullable b bVar2, boolean z2) {
        this.a = str;
        this.f414b = i;
        this.c = cVar;
        this.d = dVar;
        this.e = fVar;
        this.f = fVar2;
        this.g = bVar;
        this.h = i2;
        this.i = i3;
        this.j = f;
        this.k = list;
        this.l = bVar2;
        this.m = z2;
    }

    @Override // b.c.a.y.l.b
    public b.c.a.w.b.c a(j jVar, b.c.a.y.m.b bVar) {
        return new i(jVar, bVar, this);
    }
}
