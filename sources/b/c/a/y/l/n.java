package b.c.a.y.l;

import b.c.a.j;
import b.c.a.w.b.c;
import b.c.a.w.b.q;
import b.c.a.y.k.h;
import b.c.a.y.m.b;
import b.d.b.a.a;
/* compiled from: ShapePath.java */
/* loaded from: classes.dex */
public class n implements b {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final int f423b;
    public final h c;
    public final boolean d;

    public n(String str, int i, h hVar, boolean z2) {
        this.a = str;
        this.f423b = i;
        this.c = hVar;
        this.d = z2;
    }

    @Override // b.c.a.y.l.b
    public c a(j jVar, b bVar) {
        return new q(jVar, bVar, this);
    }

    public String toString() {
        StringBuilder R = a.R("ShapePath{name=");
        R.append(this.a);
        R.append(", index=");
        return a.z(R, this.f423b, '}');
    }
}
