package b.c.a.y.k;

import android.graphics.PointF;
import b.c.a.c0.a;
import b.c.a.w.c.j;
import java.util.List;
/* compiled from: AnimatablePointValue.java */
/* loaded from: classes.dex */
public class f extends n<PointF, PointF> {
    public f(List<a<PointF>> list) {
        super((List) list);
    }

    @Override // b.c.a.y.k.m
    public b.c.a.w.c.a<PointF, PointF> a() {
        return new j(this.a);
    }
}
