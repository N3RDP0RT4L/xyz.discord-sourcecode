package b.c.a.y.k;

import android.graphics.PointF;
import androidx.annotation.Nullable;
import b.c.a.j;
import b.c.a.w.b.c;
import b.c.a.y.l.b;
/* compiled from: AnimatableTransform.java */
/* loaded from: classes.dex */
public class l implements b {
    @Nullable
    public final e a;
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public final m<PointF, PointF> f410b;
    @Nullable
    public final g c;
    @Nullable
    public final b d;
    @Nullable
    public final d e;
    @Nullable
    public final b f;
    @Nullable
    public final b g;
    @Nullable
    public final b h;
    @Nullable
    public final b i;

    public l() {
        this(null, null, null, null, null, null, null, null, null);
    }

    @Override // b.c.a.y.l.b
    @Nullable
    public c a(j jVar, b.c.a.y.m.b bVar) {
        return null;
    }

    public l(@Nullable e eVar, @Nullable m<PointF, PointF> mVar, @Nullable g gVar, @Nullable b bVar, @Nullable d dVar, @Nullable b bVar2, @Nullable b bVar3, @Nullable b bVar4, @Nullable b bVar5) {
        this.a = eVar;
        this.f410b = mVar;
        this.c = gVar;
        this.d = bVar;
        this.e = dVar;
        this.h = bVar2;
        this.i = bVar3;
        this.f = bVar4;
        this.g = bVar5;
    }
}
