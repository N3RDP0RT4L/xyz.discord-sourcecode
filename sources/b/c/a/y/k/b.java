package b.c.a.y.k;

import b.c.a.w.c.a;
import b.c.a.w.c.c;
import java.util.List;
/* compiled from: AnimatableFloatValue.java */
/* loaded from: classes.dex */
public class b extends n<Float, Float> {
    public b() {
        super(Float.valueOf(0.0f));
    }

    @Override // b.c.a.y.k.m
    public a<Float, Float> a() {
        return new c(this.a);
    }

    public b(List<b.c.a.c0.a<Float>> list) {
        super((List) list);
    }
}
