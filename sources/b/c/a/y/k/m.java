package b.c.a.y.k;

import b.c.a.w.c.a;
import java.util.List;
/* compiled from: AnimatableValue.java */
/* loaded from: classes.dex */
public interface m<K, A> {
    a<K, A> a();

    List<b.c.a.c0.a<K>> b();

    boolean isStatic();
}
