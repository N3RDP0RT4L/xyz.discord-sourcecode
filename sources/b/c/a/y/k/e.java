package b.c.a.y.k;

import android.graphics.PointF;
import b.c.a.c0.a;
import b.c.a.w.c.i;
import b.c.a.w.c.j;
import java.util.List;
/* compiled from: AnimatablePathValue.java */
/* loaded from: classes.dex */
public class e implements m<PointF, PointF> {
    public final List<a<PointF>> a;

    public e(List<a<PointF>> list) {
        this.a = list;
    }

    @Override // b.c.a.y.k.m
    public b.c.a.w.c.a<PointF, PointF> a() {
        if (this.a.get(0).d()) {
            return new j(this.a);
        }
        return new i(this.a);
    }

    @Override // b.c.a.y.k.m
    public List<a<PointF>> b() {
        return this.a;
    }

    @Override // b.c.a.y.k.m
    public boolean isStatic() {
        return this.a.size() == 1 && this.a.get(0).d();
    }
}
