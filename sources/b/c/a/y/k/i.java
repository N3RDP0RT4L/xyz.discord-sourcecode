package b.c.a.y.k;

import android.graphics.PointF;
import b.c.a.w.c.a;
import b.c.a.w.c.m;
import java.util.List;
/* compiled from: AnimatableSplitDimensionPathValue.java */
/* loaded from: classes.dex */
public class i implements m<PointF, PointF> {
    public final b a;

    /* renamed from: b  reason: collision with root package name */
    public final b f408b;

    public i(b bVar, b bVar2) {
        this.a = bVar;
        this.f408b = bVar2;
    }

    @Override // b.c.a.y.k.m
    public a<PointF, PointF> a() {
        return new m(this.a.a(), this.f408b.a());
    }

    @Override // b.c.a.y.k.m
    public List<b.c.a.c0.a<PointF>> b() {
        throw new UnsupportedOperationException("Cannot call getKeyframes on AnimatableSplitDimensionPathValue.");
    }

    @Override // b.c.a.y.k.m
    public boolean isStatic() {
        return this.a.isStatic() && this.f408b.isStatic();
    }
}
