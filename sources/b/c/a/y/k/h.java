package b.c.a.y.k;

import android.graphics.Path;
import b.c.a.c0.a;
import b.c.a.w.c.l;
import b.c.a.y.l.k;
import java.util.List;
/* compiled from: AnimatableShapeValue.java */
/* loaded from: classes.dex */
public class h extends n<k, Path> {
    public h(List<a<k>> list) {
        super((List) list);
    }

    @Override // b.c.a.y.k.m
    public b.c.a.w.c.a<k, Path> a() {
        return new l(this.a);
    }
}
