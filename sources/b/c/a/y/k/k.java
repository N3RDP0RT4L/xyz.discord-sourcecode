package b.c.a.y.k;

import androidx.annotation.Nullable;
/* compiled from: AnimatableTextProperties.java */
/* loaded from: classes.dex */
public class k {
    @Nullable
    public final a a;
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public final a f409b;
    @Nullable
    public final b c;
    @Nullable
    public final b d;

    public k(@Nullable a aVar, @Nullable a aVar2, @Nullable b bVar, @Nullable b bVar2) {
        this.a = aVar;
        this.f409b = aVar2;
        this.c = bVar;
        this.d = bVar2;
    }
}
