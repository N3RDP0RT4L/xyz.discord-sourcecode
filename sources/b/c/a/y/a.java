package b.c.a.y;

import android.graphics.PointF;
import androidx.annotation.RestrictTo;
/* compiled from: CubicCurveData.java */
@RestrictTo({RestrictTo.Scope.LIBRARY})
/* loaded from: classes.dex */
public class a {
    public final PointF a;

    /* renamed from: b  reason: collision with root package name */
    public final PointF f395b;
    public final PointF c;

    public a() {
        this.a = new PointF();
        this.f395b = new PointF();
        this.c = new PointF();
    }

    public a(PointF pointF, PointF pointF2, PointF pointF3) {
        this.a = pointF;
        this.f395b = pointF2;
        this.c = pointF3;
    }
}
