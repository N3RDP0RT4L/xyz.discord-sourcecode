package b.c.a.y;

import androidx.annotation.RestrictTo;
import androidx.collection.LruCache;
import b.c.a.d;
/* compiled from: LottieCompositionCache.java */
@RestrictTo({RestrictTo.Scope.LIBRARY})
/* loaded from: classes.dex */
public class h {
    public static final h a = new h();

    /* renamed from: b  reason: collision with root package name */
    public final LruCache<String, d> f405b = new LruCache<>(20);
}
