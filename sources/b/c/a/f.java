package b.c.a;

import android.content.Context;
import java.util.concurrent.Callable;
/* compiled from: LottieCompositionFactory.java */
/* loaded from: classes.dex */
public class f implements Callable<p<d>> {
    public final /* synthetic */ Context j;
    public final /* synthetic */ String k;
    public final /* synthetic */ String l;

    public f(Context context, String str, String str2) {
        this.j = context;
        this.k = str;
        this.l = str2;
    }

    /* JADX WARN: Removed duplicated region for block: B:21:0x0077  */
    @Override // java.util.concurrent.Callable
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.c.a.p<b.c.a.d> call() throws java.lang.Exception {
        /*
            r10 = this;
            android.content.Context r0 = r10.j
            java.lang.String r1 = r10.k
            java.lang.String r2 = r10.l
            b.c.a.z.c r3 = new b.c.a.z.c
            r3.<init>(r0, r1, r2)
            b.c.a.z.a r0 = b.c.a.z.a.ZIP
            b.c.a.z.b r1 = r3.c
            r2 = 0
            if (r1 != 0) goto L14
            goto L9a
        L14:
            java.lang.String r4 = r3.f443b
            java.io.File r5 = new java.io.File     // Catch: java.io.FileNotFoundException -> L73
            java.io.File r6 = r1.b()     // Catch: java.io.FileNotFoundException -> L73
            b.c.a.z.a r7 = b.c.a.z.a.JSON     // Catch: java.io.FileNotFoundException -> L73
            r8 = 0
            java.lang.String r9 = b.c.a.z.b.a(r4, r7, r8)     // Catch: java.io.FileNotFoundException -> L73
            r5.<init>(r6, r9)     // Catch: java.io.FileNotFoundException -> L73
            boolean r6 = r5.exists()     // Catch: java.io.FileNotFoundException -> L73
            if (r6 == 0) goto L2d
            goto L42
        L2d:
            java.io.File r5 = new java.io.File     // Catch: java.io.FileNotFoundException -> L73
            java.io.File r1 = r1.b()     // Catch: java.io.FileNotFoundException -> L73
            java.lang.String r6 = b.c.a.z.b.a(r4, r0, r8)     // Catch: java.io.FileNotFoundException -> L73
            r5.<init>(r1, r6)     // Catch: java.io.FileNotFoundException -> L73
            boolean r1 = r5.exists()     // Catch: java.io.FileNotFoundException -> L73
            if (r1 == 0) goto L41
            goto L42
        L41:
            r5 = r2
        L42:
            if (r5 != 0) goto L45
            goto L73
        L45:
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch: java.io.FileNotFoundException -> L73
            r1.<init>(r5)     // Catch: java.io.FileNotFoundException -> L73
            java.lang.String r6 = r5.getAbsolutePath()
            java.lang.String r8 = ".zip"
            boolean r6 = r6.endsWith(r8)
            if (r6 == 0) goto L57
            r7 = r0
        L57:
            java.lang.String r6 = "Cache hit for "
            java.lang.String r8 = " at "
            java.lang.StringBuilder r4 = b.d.b.a.a.W(r6, r4, r8)
            java.lang.String r5 = r5.getAbsolutePath()
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            b.c.a.b0.c.a(r4)
            androidx.core.util.Pair r4 = new androidx.core.util.Pair
            r4.<init>(r7, r1)
            goto L74
        L73:
            r4 = r2
        L74:
            if (r4 != 0) goto L77
            goto L9a
        L77:
            F r1 = r4.first
            b.c.a.z.a r1 = (b.c.a.z.a) r1
            S r4 = r4.second
            java.io.InputStream r4 = (java.io.InputStream) r4
            if (r1 != r0) goto L8d
            java.util.zip.ZipInputStream r0 = new java.util.zip.ZipInputStream
            r0.<init>(r4)
            java.lang.String r1 = r3.f443b
            b.c.a.p r0 = b.c.a.e.d(r0, r1)
            goto L93
        L8d:
            java.lang.String r0 = r3.f443b
            b.c.a.p r0 = b.c.a.e.b(r4, r0)
        L93:
            V r0 = r0.a
            if (r0 == 0) goto L9a
            r2 = r0
            b.c.a.d r2 = (b.c.a.d) r2
        L9a:
            if (r2 == 0) goto La2
            b.c.a.p r0 = new b.c.a.p
            r0.<init>(r2)
            goto Lc5
        La2:
            java.lang.String r0 = "Animation for "
            java.lang.StringBuilder r0 = b.d.b.a.a.R(r0)
            java.lang.String r1 = r3.f443b
            r0.append(r1)
            java.lang.String r1 = " not found in cache. Fetching from network."
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            b.c.a.b0.c.a(r0)
            b.c.a.p r0 = r3.a()     // Catch: java.io.IOException -> Lbe
            goto Lc5
        Lbe:
            r0 = move-exception
            b.c.a.p r1 = new b.c.a.p
            r1.<init>(r0)
            r0 = r1
        Lc5:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.c.a.f.call():java.lang.Object");
    }
}
