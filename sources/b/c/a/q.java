package b.c.a;

import b.c.a.b0.c;
import java.util.ArrayList;
import java.util.Iterator;
/* compiled from: LottieTask.java */
/* loaded from: classes.dex */
public class q implements Runnable {
    public final /* synthetic */ r j;

    public q(r rVar) {
        this.j = rVar;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.lang.Runnable
    public void run() {
        if (this.j.e != null) {
            p<T> pVar = this.j.e;
            V v = pVar.a;
            if (v != 0) {
                r rVar = this.j;
                synchronized (rVar) {
                    Iterator it = new ArrayList(rVar.f370b).iterator();
                    while (it.hasNext()) {
                        ((l) it.next()).a(v);
                    }
                }
                return;
            }
            r rVar2 = this.j;
            Throwable th = pVar.f369b;
            synchronized (rVar2) {
                ArrayList arrayList = new ArrayList(rVar2.c);
                if (arrayList.isEmpty()) {
                    c.c("Lottie encountered an error but no failure listener was added:", th);
                    return;
                }
                Iterator it2 = arrayList.iterator();
                while (it2.hasNext()) {
                    ((l) it2.next()).a(th);
                }
            }
        }
    }
}
