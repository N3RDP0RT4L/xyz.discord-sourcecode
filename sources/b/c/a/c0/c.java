package b.c.a.c0;

import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;
/* compiled from: LottieValueCallback.java */
/* loaded from: classes.dex */
public class c<T> {
    public final b<T> a = new b<>();
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public T f345b;

    public c(@Nullable T t) {
        this.f345b = null;
        this.f345b = t;
    }

    @Nullable
    @RestrictTo({RestrictTo.Scope.LIBRARY})
    public final T a(float f, float f2, T t, T t2, float f3, float f4, float f5) {
        b<T> bVar = this.a;
        bVar.a = t;
        bVar.f344b = t2;
        return this.f345b;
    }
}
