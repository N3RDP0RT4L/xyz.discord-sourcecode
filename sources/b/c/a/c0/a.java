package b.c.a.c0;

import android.graphics.PointF;
import android.view.animation.Interpolator;
import androidx.annotation.FloatRange;
import androidx.annotation.Nullable;
import b.c.a.d;
/* compiled from: Keyframe.java */
/* loaded from: classes.dex */
public class a<T> {
    @Nullable
    public final d a;
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public final T f343b;
    @Nullable
    public T c;
    @Nullable
    public final Interpolator d;
    public final float e;
    @Nullable
    public Float f;
    public float g;
    public float h;
    public int i;
    public int j;
    public float k;
    public float l;
    public PointF m;
    public PointF n;

    public a(d dVar, @Nullable T t, @Nullable T t2, @Nullable Interpolator interpolator, float f, @Nullable Float f2) {
        this.g = -3987645.8f;
        this.h = -3987645.8f;
        this.i = 784923401;
        this.j = 784923401;
        this.k = Float.MIN_VALUE;
        this.l = Float.MIN_VALUE;
        this.m = null;
        this.n = null;
        this.a = dVar;
        this.f343b = t;
        this.c = t2;
        this.d = interpolator;
        this.e = f;
        this.f = f2;
    }

    public boolean a(@FloatRange(from = 0.0d, to = 1.0d) float f) {
        return f >= c() && f < b();
    }

    public float b() {
        if (this.a == null) {
            return 1.0f;
        }
        if (this.l == Float.MIN_VALUE) {
            if (this.f == null) {
                this.l = 1.0f;
            } else {
                this.l = ((this.f.floatValue() - this.e) / this.a.c()) + c();
            }
        }
        return this.l;
    }

    public float c() {
        d dVar = this.a;
        if (dVar == null) {
            return 0.0f;
        }
        if (this.k == Float.MIN_VALUE) {
            this.k = (this.e - dVar.k) / dVar.c();
        }
        return this.k;
    }

    public boolean d() {
        return this.d == null;
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("Keyframe{startValue=");
        R.append(this.f343b);
        R.append(", endValue=");
        R.append(this.c);
        R.append(", startFrame=");
        R.append(this.e);
        R.append(", endFrame=");
        R.append(this.f);
        R.append(", interpolator=");
        R.append(this.d);
        R.append('}');
        return R.toString();
    }

    public a(T t) {
        this.g = -3987645.8f;
        this.h = -3987645.8f;
        this.i = 784923401;
        this.j = 784923401;
        this.k = Float.MIN_VALUE;
        this.l = Float.MIN_VALUE;
        this.m = null;
        this.n = null;
        this.a = null;
        this.f343b = t;
        this.c = t;
        this.d = null;
        this.e = Float.MIN_VALUE;
        this.f = Float.valueOf(Float.MAX_VALUE);
    }
}
