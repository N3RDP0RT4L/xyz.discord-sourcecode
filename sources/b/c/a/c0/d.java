package b.c.a.c0;
/* compiled from: ScaleXY.java */
/* loaded from: classes.dex */
public class d {
    public float a;

    /* renamed from: b  reason: collision with root package name */
    public float f346b;

    public d() {
        this.a = 1.0f;
        this.f346b = 1.0f;
    }

    public String toString() {
        return this.a + "x" + this.f346b;
    }

    public d(float f, float f2) {
        this.a = f;
        this.f346b = f2;
    }
}
