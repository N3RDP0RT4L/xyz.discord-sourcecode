package b.c.a.w.c;

import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PointF;
import b.c.a.c0.a;
import b.c.a.c0.c;
import java.util.List;
/* compiled from: PathKeyframeAnimation.java */
/* loaded from: classes.dex */
public class i extends f<PointF> {
    public h k;
    public final PointF i = new PointF();
    public final float[] j = new float[2];
    public PathMeasure l = new PathMeasure();

    public i(List<? extends a<PointF>> list) {
        super(list);
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // b.c.a.w.c.a
    public Object f(a aVar, float f) {
        PointF pointF;
        h hVar = (h) aVar;
        Path path = hVar.o;
        if (path == null) {
            return (PointF) aVar.f343b;
        }
        c<A> cVar = this.e;
        if (cVar != 0 && (pointF = (PointF) cVar.a(hVar.e, hVar.f.floatValue(), hVar.f343b, hVar.c, d(), f, this.d)) != null) {
            return pointF;
        }
        if (this.k != hVar) {
            this.l.setPath(path, false);
            this.k = hVar;
        }
        PathMeasure pathMeasure = this.l;
        pathMeasure.getPosTan(pathMeasure.getLength() * f, this.j, null);
        PointF pointF2 = this.i;
        float[] fArr = this.j;
        pointF2.set(fArr[0], fArr[1]);
        return this.i;
    }
}
