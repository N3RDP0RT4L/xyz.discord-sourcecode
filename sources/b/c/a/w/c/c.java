package b.c.a.w.c;

import b.c.a.b0.f;
import b.c.a.c0.a;
import java.util.List;
/* compiled from: FloatKeyframeAnimation.java */
/* loaded from: classes.dex */
public class c extends f<Float> {
    public c(List<a<Float>> list) {
        super(list);
    }

    @Override // b.c.a.w.c.a
    public Object f(a aVar, float f) {
        return Float.valueOf(k(aVar, f));
    }

    public float j() {
        return k(a(), c());
    }

    public float k(a<Float> aVar, float f) {
        Float f2;
        if (aVar.f343b == null || aVar.c == null) {
            throw new IllegalStateException("Missing values for keyframe.");
        }
        b.c.a.c0.c<A> cVar = this.e;
        if (cVar != 0 && (f2 = (Float) cVar.a(aVar.e, aVar.f.floatValue(), aVar.f343b, aVar.c, f, d(), this.d)) != null) {
            return f2.floatValue();
        }
        if (aVar.g == -3987645.8f) {
            aVar.g = aVar.f343b.floatValue();
        }
        float f3 = aVar.g;
        if (aVar.h == -3987645.8f) {
            aVar.h = aVar.c.floatValue();
        }
        return f.e(f3, aVar.h, f);
    }
}
