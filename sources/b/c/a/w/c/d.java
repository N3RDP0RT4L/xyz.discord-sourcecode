package b.c.a.w.c;

import b.c.a.b0.f;
import b.c.a.c0.a;
import b.c.a.y.l.c;
import java.util.List;
import java.util.Objects;
/* compiled from: GradientColorKeyframeAnimation.java */
/* loaded from: classes.dex */
public class d extends f<c> {
    public final c i;

    public d(List<a<c>> list) {
        super(list);
        int i = 0;
        c cVar = list.get(0).f343b;
        i = cVar != null ? cVar.f412b.length : i;
        this.i = new c(new float[i], new int[i]);
    }

    @Override // b.c.a.w.c.a
    public Object f(a aVar, float f) {
        c cVar = this.i;
        c cVar2 = (c) aVar.f343b;
        c cVar3 = (c) aVar.c;
        Objects.requireNonNull(cVar);
        if (cVar2.f412b.length == cVar3.f412b.length) {
            for (int i = 0; i < cVar2.f412b.length; i++) {
                cVar.a[i] = f.e(cVar2.a[i], cVar3.a[i], f);
                cVar.f412b[i] = b.c.a.a0.d.i0(f, cVar2.f412b[i], cVar3.f412b[i]);
            }
            return this.i;
        }
        StringBuilder R = b.d.b.a.a.R("Cannot interpolate between gradients. Lengths vary (");
        R.append(cVar2.f412b.length);
        R.append(" vs ");
        throw new IllegalArgumentException(b.d.b.a.a.A(R, cVar3.f412b.length, ")"));
    }
}
