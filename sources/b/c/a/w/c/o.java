package b.c.a.w.c;

import android.graphics.Matrix;
import android.graphics.PointF;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.c.a.c0.c;
import b.c.a.c0.d;
import b.c.a.w.c.a;
import b.c.a.y.k.b;
import b.c.a.y.k.e;
import b.c.a.y.k.g;
import b.c.a.y.k.l;
import b.c.a.y.k.m;
import java.util.Collections;
/* compiled from: TransformKeyframeAnimation.java */
/* loaded from: classes.dex */
public class o {
    public final Matrix a = new Matrix();

    /* renamed from: b  reason: collision with root package name */
    public final Matrix f392b;
    public final Matrix c;
    public final Matrix d;
    public final float[] e;
    @NonNull
    public a<PointF, PointF> f;
    @NonNull
    public a<?, PointF> g;
    @NonNull
    public a<d, d> h;
    @NonNull
    public a<Float, Float> i;
    @NonNull
    public a<Integer, Integer> j;
    @Nullable
    public c k;
    @Nullable
    public c l;
    @Nullable
    public a<?, Float> m;
    @Nullable
    public a<?, Float> n;

    public o(l lVar) {
        e eVar = lVar.a;
        this.f = eVar == null ? null : eVar.a();
        m<PointF, PointF> mVar = lVar.f410b;
        this.g = mVar == null ? null : mVar.a();
        g gVar = lVar.c;
        this.h = gVar == null ? null : gVar.a();
        b bVar = lVar.d;
        this.i = bVar == null ? null : bVar.a();
        b bVar2 = lVar.f;
        c cVar = bVar2 == null ? null : (c) bVar2.a();
        this.k = cVar;
        if (cVar != null) {
            this.f392b = new Matrix();
            this.c = new Matrix();
            this.d = new Matrix();
            this.e = new float[9];
        } else {
            this.f392b = null;
            this.c = null;
            this.d = null;
            this.e = null;
        }
        b bVar3 = lVar.g;
        this.l = bVar3 == null ? null : (c) bVar3.a();
        b.c.a.y.k.d dVar = lVar.e;
        if (dVar != null) {
            this.j = dVar.a();
        }
        b bVar4 = lVar.h;
        if (bVar4 != null) {
            this.m = bVar4.a();
        } else {
            this.m = null;
        }
        b bVar5 = lVar.i;
        if (bVar5 != null) {
            this.n = bVar5.a();
        } else {
            this.n = null;
        }
    }

    public void a(b.c.a.y.m.b bVar) {
        bVar.e(this.j);
        bVar.e(this.m);
        bVar.e(this.n);
        bVar.e(this.f);
        bVar.e(this.g);
        bVar.e(this.h);
        bVar.e(this.i);
        bVar.e(this.k);
        bVar.e(this.l);
    }

    public void b(a.b bVar) {
        a<Integer, Integer> aVar = this.j;
        if (aVar != null) {
            aVar.a.add(bVar);
        }
        a<?, Float> aVar2 = this.m;
        if (aVar2 != null) {
            aVar2.a.add(bVar);
        }
        a<?, Float> aVar3 = this.n;
        if (aVar3 != null) {
            aVar3.a.add(bVar);
        }
        a<PointF, PointF> aVar4 = this.f;
        if (aVar4 != null) {
            aVar4.a.add(bVar);
        }
        a<?, PointF> aVar5 = this.g;
        if (aVar5 != null) {
            aVar5.a.add(bVar);
        }
        a<d, d> aVar6 = this.h;
        if (aVar6 != null) {
            aVar6.a.add(bVar);
        }
        a<Float, Float> aVar7 = this.i;
        if (aVar7 != null) {
            aVar7.a.add(bVar);
        }
        c cVar = this.k;
        if (cVar != null) {
            cVar.a.add(bVar);
        }
        c cVar2 = this.l;
        if (cVar2 != null) {
            cVar2.a.add(bVar);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public <T> boolean c(T t, @Nullable c<T> cVar) {
        c cVar2;
        c cVar3;
        a<?, Float> aVar;
        a<?, Float> aVar2;
        if (t == b.c.a.o.e) {
            a<PointF, PointF> aVar3 = this.f;
            if (aVar3 == null) {
                this.f = new p(cVar, new PointF());
                return true;
            }
            c<PointF> cVar4 = aVar3.e;
            aVar3.e = cVar;
            return true;
        } else if (t == b.c.a.o.f) {
            a<?, PointF> aVar4 = this.g;
            if (aVar4 == null) {
                this.g = new p(cVar, new PointF());
                return true;
            }
            c<PointF> cVar5 = aVar4.e;
            aVar4.e = cVar;
            return true;
        } else if (t == b.c.a.o.k) {
            a<d, d> aVar5 = this.h;
            if (aVar5 == null) {
                this.h = new p(cVar, new d());
                return true;
            }
            c<d> cVar6 = aVar5.e;
            aVar5.e = cVar;
            return true;
        } else if (t == b.c.a.o.l) {
            a<Float, Float> aVar6 = this.i;
            if (aVar6 == null) {
                this.i = new p(cVar, Float.valueOf(0.0f));
                return true;
            }
            c<Float> cVar7 = aVar6.e;
            aVar6.e = cVar;
            return true;
        } else if (t == b.c.a.o.c) {
            a<Integer, Integer> aVar7 = this.j;
            if (aVar7 == null) {
                this.j = new p(cVar, 100);
                return true;
            }
            c<Integer> cVar8 = aVar7.e;
            aVar7.e = cVar;
            return true;
        } else if (t != b.c.a.o.f367y || (aVar2 = this.m) == null) {
            if (t != b.c.a.o.f368z || (aVar = this.n) == null) {
                if (t == b.c.a.o.m && (cVar3 = this.k) != null) {
                    if (cVar3 == null) {
                        this.k = new c(Collections.singletonList(new b.c.a.c0.a(Float.valueOf(0.0f))));
                    }
                    c cVar9 = this.k;
                    Object obj = cVar9.e;
                    cVar9.e = cVar;
                    return true;
                } else if (t != b.c.a.o.n || (cVar2 = this.l) == null) {
                    return false;
                } else {
                    if (cVar2 == null) {
                        this.l = new c(Collections.singletonList(new b.c.a.c0.a(Float.valueOf(0.0f))));
                    }
                    c cVar10 = this.l;
                    Object obj2 = cVar10.e;
                    cVar10.e = cVar;
                    return true;
                }
            } else if (aVar == null) {
                this.n = new p(cVar, 100);
                return true;
            } else {
                c<Float> cVar11 = aVar.e;
                aVar.e = cVar;
                return true;
            }
        } else if (aVar2 == null) {
            this.m = new p(cVar, 100);
            return true;
        } else {
            c<Float> cVar12 = aVar2.e;
            aVar2.e = cVar;
            return true;
        }
    }

    public final void d() {
        for (int i = 0; i < 9; i++) {
            this.e[i] = 0.0f;
        }
    }

    public Matrix e() {
        float f;
        this.a.reset();
        a<?, PointF> aVar = this.g;
        if (aVar != null) {
            PointF e = aVar.e();
            float f2 = e.x;
            if (!(f2 == 0.0f && e.y == 0.0f)) {
                this.a.preTranslate(f2, e.y);
            }
        }
        a<Float, Float> aVar2 = this.i;
        if (aVar2 != null) {
            if (aVar2 instanceof p) {
                f = aVar2.e().floatValue();
            } else {
                f = ((c) aVar2).j();
            }
            if (f != 0.0f) {
                this.a.preRotate(f);
            }
        }
        if (this.k != null) {
            c cVar = this.l;
            float cos = cVar == null ? 0.0f : (float) Math.cos(Math.toRadians((-cVar.j()) + 90.0f));
            c cVar2 = this.l;
            float sin = cVar2 == null ? 1.0f : (float) Math.sin(Math.toRadians((-cVar2.j()) + 90.0f));
            d();
            float[] fArr = this.e;
            fArr[0] = cos;
            fArr[1] = sin;
            float f3 = -sin;
            fArr[3] = f3;
            fArr[4] = cos;
            fArr[8] = 1.0f;
            this.f392b.setValues(fArr);
            d();
            float[] fArr2 = this.e;
            fArr2[0] = 1.0f;
            fArr2[3] = (float) Math.tan(Math.toRadians(this.k.j()));
            fArr2[4] = 1.0f;
            fArr2[8] = 1.0f;
            this.c.setValues(fArr2);
            d();
            float[] fArr3 = this.e;
            fArr3[0] = cos;
            fArr3[1] = f3;
            fArr3[3] = sin;
            fArr3[4] = cos;
            fArr3[8] = 1.0f;
            this.d.setValues(fArr3);
            this.c.preConcat(this.f392b);
            this.d.preConcat(this.c);
            this.a.preConcat(this.d);
        }
        a<d, d> aVar3 = this.h;
        if (aVar3 != null) {
            d e2 = aVar3.e();
            float f4 = e2.a;
            if (!(f4 == 1.0f && e2.f346b == 1.0f)) {
                this.a.preScale(f4, e2.f346b);
            }
        }
        a<PointF, PointF> aVar4 = this.f;
        if (aVar4 != null) {
            PointF e3 = aVar4.e();
            float f5 = e3.x;
            if (!(f5 == 0.0f && e3.y == 0.0f)) {
                this.a.preTranslate(-f5, -e3.y);
            }
        }
        return this.a;
    }

    public Matrix f(float f) {
        a<?, PointF> aVar = this.g;
        PointF pointF = null;
        PointF e = aVar == null ? null : aVar.e();
        a<d, d> aVar2 = this.h;
        d e2 = aVar2 == null ? null : aVar2.e();
        this.a.reset();
        if (e != null) {
            this.a.preTranslate(e.x * f, e.y * f);
        }
        if (e2 != null) {
            double d = f;
            this.a.preScale((float) Math.pow(e2.a, d), (float) Math.pow(e2.f346b, d));
        }
        a<Float, Float> aVar3 = this.i;
        if (aVar3 != null) {
            float floatValue = aVar3.e().floatValue();
            a<PointF, PointF> aVar4 = this.f;
            if (aVar4 != null) {
                pointF = aVar4.e();
            }
            Matrix matrix = this.a;
            float f2 = floatValue * f;
            float f3 = 0.0f;
            float f4 = pointF == null ? 0.0f : pointF.x;
            if (pointF != null) {
                f3 = pointF.y;
            }
            matrix.preRotate(f2, f4, f3);
        }
        return this.a;
    }
}
