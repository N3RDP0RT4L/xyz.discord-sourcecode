package b.c.a.w.c;

import androidx.annotation.Nullable;
import b.c.a.c0.a;
import b.c.a.c0.c;
import java.util.Collections;
/* compiled from: ValueCallbackKeyframeAnimation.java */
/* loaded from: classes.dex */
public class p<K, A> extends a<K, A> {
    public final A i;

    public p(c<A> cVar, @Nullable A a) {
        super(Collections.emptyList());
        this.e = cVar;
        this.i = a;
    }

    @Override // b.c.a.w.c.a
    public float b() {
        return 1.0f;
    }

    @Override // b.c.a.w.c.a
    public A e() {
        c<A> cVar = this.e;
        A a = this.i;
        float f = this.d;
        return cVar.a(0.0f, 0.0f, a, a, f, f, f);
    }

    @Override // b.c.a.w.c.a
    public A f(a<K> aVar, float f) {
        return e();
    }

    @Override // b.c.a.w.c.a
    public void g() {
        if (this.e != null) {
            super.g();
        }
    }

    @Override // b.c.a.w.c.a
    public void h(float f) {
        this.d = f;
    }
}
