package b.c.a.w.c;

import android.graphics.PointF;
import b.c.a.c0.a;
import b.c.a.c0.c;
import java.util.List;
/* compiled from: PointKeyframeAnimation.java */
/* loaded from: classes.dex */
public class j extends f<PointF> {
    public final PointF i = new PointF();

    public j(List<a<PointF>> list) {
        super(list);
    }

    @Override // b.c.a.w.c.a
    public Object f(a aVar, float f) {
        T t;
        PointF pointF;
        T t2 = aVar.f343b;
        if (t2 == 0 || (t = aVar.c) == 0) {
            throw new IllegalStateException("Missing values for keyframe.");
        }
        PointF pointF2 = (PointF) t2;
        PointF pointF3 = (PointF) t;
        c<A> cVar = this.e;
        if (cVar != 0 && (pointF = (PointF) cVar.a(aVar.e, aVar.f.floatValue(), pointF2, pointF3, f, d(), this.d)) != null) {
            return pointF;
        }
        PointF pointF4 = this.i;
        float f2 = pointF2.x;
        float a = b.d.b.a.a.a(pointF3.x, f2, f, f2);
        float f3 = pointF2.y;
        pointF4.set(a, ((pointF3.y - f3) * f) + f3);
        return this.i;
    }
}
