package b.c.a.w.c;

import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PointF;
import androidx.annotation.Nullable;
import b.c.a.b0.g;
import b.c.a.c0.a;
import b.c.a.d;
/* compiled from: PathKeyframe.java */
/* loaded from: classes.dex */
public class h extends a<PointF> {
    @Nullable
    public Path o;
    public final a<PointF> p;

    public h(d dVar, a<PointF> aVar) {
        super(dVar, aVar.f343b, aVar.c, aVar.d, aVar.e, aVar.f);
        this.p = aVar;
        e();
    }

    public void e() {
        T t;
        T t2 = this.c;
        boolean z2 = (t2 == 0 || (t = this.f343b) == 0 || !((PointF) t).equals(((PointF) t2).x, ((PointF) t2).y)) ? false : true;
        T t3 = this.c;
        if (t3 != 0 && !z2) {
            PointF pointF = (PointF) this.f343b;
            PointF pointF2 = (PointF) t3;
            a<PointF> aVar = this.p;
            PointF pointF3 = aVar.m;
            PointF pointF4 = aVar.n;
            PathMeasure pathMeasure = g.a;
            Path path = new Path();
            path.moveTo(pointF.x, pointF.y);
            if (pointF3 == null || pointF4 == null || (pointF3.length() == 0.0f && pointF4.length() == 0.0f)) {
                path.lineTo(pointF2.x, pointF2.y);
            } else {
                float f = pointF.x;
                float f2 = pointF2.x;
                float f3 = pointF2.y;
                path.cubicTo(pointF3.x + f, pointF.y + pointF3.y, f2 + pointF4.x, f3 + pointF4.y, f2, f3);
            }
            this.o = path;
        }
    }
}
