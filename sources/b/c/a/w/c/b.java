package b.c.a.w.c;

import b.c.a.a0.d;
import b.c.a.b0.f;
import b.c.a.c0.a;
import b.c.a.c0.c;
import java.util.List;
/* compiled from: ColorKeyframeAnimation.java */
/* loaded from: classes.dex */
public class b extends f<Integer> {
    public b(List<a<Integer>> list) {
        super(list);
    }

    @Override // b.c.a.w.c.a
    public Object f(a aVar, float f) {
        return Integer.valueOf(j(aVar, f));
    }

    public int j(a<Integer> aVar, float f) {
        Integer num;
        Integer num2 = aVar.f343b;
        if (num2 == null || aVar.c == null) {
            throw new IllegalStateException("Missing values for keyframe.");
        }
        int intValue = num2.intValue();
        int intValue2 = aVar.c.intValue();
        c<A> cVar = this.e;
        if (cVar == 0 || (num = (Integer) cVar.a(aVar.e, aVar.f.floatValue(), Integer.valueOf(intValue), Integer.valueOf(intValue2), f, d(), this.d)) == null) {
            return d.i0(f.b(f, 0.0f, 1.0f), intValue, intValue2);
        }
        return num.intValue();
    }
}
