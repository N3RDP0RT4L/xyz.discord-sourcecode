package b.c.a.w.c;

import android.graphics.Path;
import android.graphics.PointF;
import b.c.a.b0.c;
import b.c.a.b0.f;
import b.c.a.c0.a;
import b.c.a.y.l.k;
import java.util.List;
/* compiled from: ShapeKeyframeAnimation.java */
/* loaded from: classes.dex */
public class l extends a<k, Path> {
    public final k i = new k();
    public final Path j = new Path();

    public l(List<a<k>> list) {
        super(list);
    }

    @Override // b.c.a.w.c.a
    public Path f(a<k> aVar, float f) {
        List<b.c.a.y.a> list;
        k kVar = aVar.f343b;
        k kVar2 = aVar.c;
        k kVar3 = this.i;
        if (kVar3.f420b == null) {
            kVar3.f420b = new PointF();
        }
        kVar3.c = kVar.c || kVar2.c;
        if (kVar.a.size() != kVar2.a.size()) {
            StringBuilder R = b.d.b.a.a.R("Curves must have the same number of control points. Shape 1: ");
            R.append(kVar.a.size());
            R.append("\tShape 2: ");
            R.append(kVar2.a.size());
            c.b(R.toString());
        }
        int min = Math.min(kVar.a.size(), kVar2.a.size());
        if (kVar3.a.size() < min) {
            for (int size = kVar3.a.size(); size < min; size++) {
                kVar3.a.add(new b.c.a.y.a());
            }
        } else if (kVar3.a.size() > min) {
            for (int size2 = kVar3.a.size() - 1; size2 >= min; size2--) {
                kVar3.a.remove(list.size() - 1);
            }
        }
        PointF pointF = kVar.f420b;
        PointF pointF2 = kVar2.f420b;
        float e = f.e(pointF.x, pointF2.x, f);
        float e2 = f.e(pointF.y, pointF2.y, f);
        if (kVar3.f420b == null) {
            kVar3.f420b = new PointF();
        }
        kVar3.f420b.set(e, e2);
        for (int size3 = kVar3.a.size() - 1; size3 >= 0; size3--) {
            b.c.a.y.a aVar2 = kVar.a.get(size3);
            b.c.a.y.a aVar3 = kVar2.a.get(size3);
            PointF pointF3 = aVar2.a;
            PointF pointF4 = aVar2.f395b;
            PointF pointF5 = aVar2.c;
            PointF pointF6 = aVar3.a;
            PointF pointF7 = aVar3.f395b;
            PointF pointF8 = aVar3.c;
            kVar3.a.get(size3).a.set(f.e(pointF3.x, pointF6.x, f), f.e(pointF3.y, pointF6.y, f));
            kVar3.a.get(size3).f395b.set(f.e(pointF4.x, pointF7.x, f), f.e(pointF4.y, pointF7.y, f));
            kVar3.a.get(size3).c.set(f.e(pointF5.x, pointF8.x, f), f.e(pointF5.y, pointF8.y, f));
        }
        k kVar4 = this.i;
        Path path = this.j;
        path.reset();
        PointF pointF9 = kVar4.f420b;
        path.moveTo(pointF9.x, pointF9.y);
        f.a.set(pointF9.x, pointF9.y);
        for (int i = 0; i < kVar4.a.size(); i++) {
            b.c.a.y.a aVar4 = kVar4.a.get(i);
            PointF pointF10 = aVar4.a;
            PointF pointF11 = aVar4.f395b;
            PointF pointF12 = aVar4.c;
            if (!pointF10.equals(f.a) || !pointF11.equals(pointF12)) {
                path.cubicTo(pointF10.x, pointF10.y, pointF11.x, pointF11.y, pointF12.x, pointF12.y);
            } else {
                path.lineTo(pointF12.x, pointF12.y);
            }
            f.a.set(pointF12.x, pointF12.y);
        }
        if (kVar4.c) {
            path.close();
        }
        return this.j;
    }
}
