package b.c.a.w.c;

import b.c.a.b0.f;
import b.c.a.c0.a;
import b.c.a.c0.c;
import b.c.a.c0.d;
import java.util.List;
/* compiled from: ScaleKeyframeAnimation.java */
/* loaded from: classes.dex */
public class k extends f<d> {
    public final d i = new d();

    public k(List<a<d>> list) {
        super(list);
    }

    @Override // b.c.a.w.c.a
    public Object f(a aVar, float f) {
        T t;
        d dVar;
        T t2 = aVar.f343b;
        if (t2 == 0 || (t = aVar.c) == 0) {
            throw new IllegalStateException("Missing values for keyframe.");
        }
        d dVar2 = (d) t2;
        d dVar3 = (d) t;
        c<A> cVar = this.e;
        if (cVar != 0 && (dVar = (d) cVar.a(aVar.e, aVar.f.floatValue(), dVar2, dVar3, f, d(), this.d)) != null) {
            return dVar;
        }
        d dVar4 = this.i;
        float e = f.e(dVar2.a, dVar3.a, f);
        float e2 = f.e(dVar2.f346b, dVar3.f346b, f);
        dVar4.a = e;
        dVar4.f346b = e2;
        return this.i;
    }
}
