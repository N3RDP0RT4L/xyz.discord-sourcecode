package b.c.a.w.c;

import android.graphics.Path;
import b.c.a.y.l.f;
import b.c.a.y.l.k;
import java.util.ArrayList;
import java.util.List;
/* compiled from: MaskKeyframeAnimation.java */
/* loaded from: classes.dex */
public class g {
    public final List<a<k, Path>> a;

    /* renamed from: b  reason: collision with root package name */
    public final List<a<Integer, Integer>> f391b;
    public final List<f> c;

    public g(List<f> list) {
        this.c = list;
        this.a = new ArrayList(list.size());
        this.f391b = new ArrayList(list.size());
        for (int i = 0; i < list.size(); i++) {
            this.a.add(list.get(i).f415b.a());
            this.f391b.add(list.get(i).c.a());
        }
    }
}
