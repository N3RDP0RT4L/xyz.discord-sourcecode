package b.c.a.w.b;

import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import androidx.annotation.Nullable;
import b.c.a.c0.c;
import b.c.a.j;
import b.c.a.w.c.a;
import b.c.a.y.f;
import b.c.a.y.l.i;
import b.c.a.y.m.b;
import java.util.List;
/* compiled from: RectangleContent.java */
/* loaded from: classes.dex */
public class o implements a.b, k, m {
    public final String c;
    public final boolean d;
    public final j e;
    public final a<?, PointF> f;
    public final a<?, PointF> g;
    public final a<?, Float> h;
    public boolean j;
    public final Path a = new Path();

    /* renamed from: b  reason: collision with root package name */
    public final RectF f383b = new RectF();
    public b i = new b();

    public o(j jVar, b bVar, i iVar) {
        this.c = iVar.a;
        this.d = iVar.e;
        this.e = jVar;
        a<PointF, PointF> a = iVar.f418b.a();
        this.f = a;
        a<PointF, PointF> a2 = iVar.c.a();
        this.g = a2;
        a<Float, Float> a3 = iVar.d.a();
        this.h = a3;
        bVar.e(a);
        bVar.e(a2);
        bVar.e(a3);
        a.a.add(this);
        a2.a.add(this);
        a3.a.add(this);
    }

    @Override // b.c.a.w.c.a.b
    public void a() {
        this.j = false;
        this.e.invalidateSelf();
    }

    @Override // b.c.a.w.b.c
    public void b(List<c> list, List<c> list2) {
        for (int i = 0; i < list.size(); i++) {
            c cVar = list.get(i);
            if (cVar instanceof s) {
                s sVar = (s) cVar;
                if (sVar.c == 1) {
                    this.i.a.add(sVar);
                    sVar.f387b.add(this);
                }
            }
        }
    }

    @Override // b.c.a.y.g
    public void c(f fVar, int i, List<f> list, f fVar2) {
        b.c.a.b0.f.f(fVar, i, list, fVar2, this);
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // b.c.a.y.g
    public <T> void g(T t, @Nullable c<T> cVar) {
        if (t == b.c.a.o.h) {
            a<?, PointF> aVar = this.g;
            c<PointF> cVar2 = aVar.e;
            aVar.e = cVar;
        } else if (t == b.c.a.o.j) {
            a<?, PointF> aVar2 = this.f;
            c<PointF> cVar3 = aVar2.e;
            aVar2.e = cVar;
        } else if (t == b.c.a.o.i) {
            a<?, Float> aVar3 = this.h;
            c<Float> cVar4 = aVar3.e;
            aVar3.e = cVar;
        }
    }

    @Override // b.c.a.w.b.c
    public String getName() {
        return this.c;
    }

    @Override // b.c.a.w.b.m
    public Path getPath() {
        if (this.j) {
            return this.a;
        }
        this.a.reset();
        if (this.d) {
            this.j = true;
            return this.a;
        }
        PointF e = this.g.e();
        float f = e.x / 2.0f;
        float f2 = e.y / 2.0f;
        a<?, Float> aVar = this.h;
        float j = aVar == null ? 0.0f : ((b.c.a.w.c.c) aVar).j();
        float min = Math.min(f, f2);
        if (j > min) {
            j = min;
        }
        PointF e2 = this.f.e();
        this.a.moveTo(e2.x + f, (e2.y - f2) + j);
        this.a.lineTo(e2.x + f, (e2.y + f2) - j);
        int i = (j > 0.0f ? 1 : (j == 0.0f ? 0 : -1));
        if (i > 0) {
            RectF rectF = this.f383b;
            float f3 = e2.x;
            float f4 = j * 2.0f;
            float f5 = e2.y;
            rectF.set((f3 + f) - f4, (f5 + f2) - f4, f3 + f, f5 + f2);
            this.a.arcTo(this.f383b, 0.0f, 90.0f, false);
        }
        this.a.lineTo((e2.x - f) + j, e2.y + f2);
        if (i > 0) {
            RectF rectF2 = this.f383b;
            float f6 = e2.x;
            float f7 = e2.y;
            float f8 = j * 2.0f;
            rectF2.set(f6 - f, (f7 + f2) - f8, (f6 - f) + f8, f7 + f2);
            this.a.arcTo(this.f383b, 90.0f, 90.0f, false);
        }
        this.a.lineTo(e2.x - f, (e2.y - f2) + j);
        if (i > 0) {
            RectF rectF3 = this.f383b;
            float f9 = e2.x;
            float f10 = e2.y;
            float f11 = j * 2.0f;
            rectF3.set(f9 - f, f10 - f2, (f9 - f) + f11, (f10 - f2) + f11);
            this.a.arcTo(this.f383b, 180.0f, 90.0f, false);
        }
        this.a.lineTo((e2.x + f) - j, e2.y - f2);
        if (i > 0) {
            RectF rectF4 = this.f383b;
            float f12 = e2.x;
            float f13 = j * 2.0f;
            float f14 = e2.y;
            rectF4.set((f12 + f) - f13, f14 - f2, f12 + f, (f14 - f2) + f13);
            this.a.arcTo(this.f383b, 270.0f, 90.0f, false);
        }
        this.a.close();
        this.i.a(this.a);
        this.j = true;
        return this.a;
    }
}
