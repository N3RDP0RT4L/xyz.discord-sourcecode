package b.c.a.w.b;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import androidx.annotation.Nullable;
import b.c.a.c0.c;
import b.c.a.j;
import b.c.a.w.c.a;
import b.c.a.w.c.p;
import b.c.a.y.l.o;
import b.c.a.y.m.b;
/* compiled from: StrokeContent.java */
/* loaded from: classes.dex */
public class r extends a {
    public final b o;
    public final String p;
    public final boolean q;
    public final a<Integer, Integer> r;
    @Nullable

    /* renamed from: s  reason: collision with root package name */
    public a<ColorFilter, ColorFilter> f386s;

    public r(j jVar, b bVar, o oVar) {
        super(jVar, bVar, b.c.a.y.b.k(oVar.g), b.c.a.y.b.l(oVar.h), oVar.i, oVar.e, oVar.f, oVar.c, oVar.f424b);
        this.o = bVar;
        this.p = oVar.a;
        this.q = oVar.j;
        a<Integer, Integer> a = oVar.d.a();
        this.r = a;
        a.a.add(this);
        bVar.e(a);
    }

    @Override // b.c.a.w.b.a, b.c.a.w.b.e
    public void f(Canvas canvas, Matrix matrix, int i) {
        if (!this.q) {
            Paint paint = this.i;
            b.c.a.w.c.b bVar = (b.c.a.w.c.b) this.r;
            paint.setColor(bVar.j(bVar.a(), bVar.c()));
            a<ColorFilter, ColorFilter> aVar = this.f386s;
            if (aVar != null) {
                this.i.setColorFilter(aVar.e());
            }
            super.f(canvas, matrix, i);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // b.c.a.w.b.a, b.c.a.y.g
    public <T> void g(T t, @Nullable c<T> cVar) {
        super.g(t, cVar);
        if (t == b.c.a.o.f364b) {
            a<Integer, Integer> aVar = this.r;
            c<Integer> cVar2 = aVar.e;
            aVar.e = cVar;
        } else if (t == b.c.a.o.C) {
            a<ColorFilter, ColorFilter> aVar2 = this.f386s;
            if (aVar2 != null) {
                this.o.u.remove(aVar2);
            }
            if (cVar == 0) {
                this.f386s = null;
                return;
            }
            p pVar = new p(cVar, null);
            this.f386s = pVar;
            pVar.a.add(this);
            this.o.e(this.r);
        }
    }

    @Override // b.c.a.w.b.c
    public String getName() {
        return this.p;
    }
}
