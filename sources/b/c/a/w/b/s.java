package b.c.a.w.b;

import b.c.a.w.c.a;
import b.c.a.y.l.p;
import b.c.a.y.m.b;
import java.util.ArrayList;
import java.util.List;
/* compiled from: TrimPathContent.java */
/* loaded from: classes.dex */
public class s implements c, a.b {
    public final boolean a;

    /* renamed from: b  reason: collision with root package name */
    public final List<a.b> f387b = new ArrayList();
    public final int c;
    public final a<?, Float> d;
    public final a<?, Float> e;
    public final a<?, Float> f;

    public s(b bVar, p pVar) {
        this.a = pVar.f;
        this.c = pVar.f425b;
        a<Float, Float> a = pVar.c.a();
        this.d = a;
        a<Float, Float> a2 = pVar.d.a();
        this.e = a2;
        a<Float, Float> a3 = pVar.e.a();
        this.f = a3;
        bVar.e(a);
        bVar.e(a2);
        bVar.e(a3);
        a.a.add(this);
        a2.a.add(this);
        a3.a.add(this);
    }

    @Override // b.c.a.w.c.a.b
    public void a() {
        for (int i = 0; i < this.f387b.size(); i++) {
            this.f387b.get(i).a();
        }
    }

    @Override // b.c.a.w.b.c
    public void b(List<c> list, List<c> list2) {
    }
}
