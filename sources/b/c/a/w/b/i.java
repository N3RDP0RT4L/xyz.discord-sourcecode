package b.c.a.w.b;

import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Shader;
import androidx.annotation.Nullable;
import androidx.collection.LongSparseArray;
import b.c.a.j;
import b.c.a.o;
import b.c.a.w.c.a;
import b.c.a.w.c.p;
import b.c.a.y.l.c;
import b.c.a.y.l.e;
import b.c.a.y.m.b;
/* compiled from: GradientStrokeContent.java */
/* loaded from: classes.dex */
public class i extends a {
    public final String o;
    public final boolean p;
    public final LongSparseArray<LinearGradient> q = new LongSparseArray<>();
    public final LongSparseArray<RadialGradient> r = new LongSparseArray<>();

    /* renamed from: s  reason: collision with root package name */
    public final RectF f378s = new RectF();
    public final int t;
    public final int u;
    public final a<c, c> v;
    public final a<PointF, PointF> w;

    /* renamed from: x  reason: collision with root package name */
    public final a<PointF, PointF> f379x;
    @Nullable

    /* renamed from: y  reason: collision with root package name */
    public p f380y;

    public i(j jVar, b bVar, e eVar) {
        super(jVar, bVar, b.c.a.y.b.k(eVar.h), b.c.a.y.b.l(eVar.i), eVar.j, eVar.d, eVar.g, eVar.k, eVar.l);
        this.o = eVar.a;
        this.t = eVar.f414b;
        this.p = eVar.m;
        this.u = (int) (jVar.k.b() / 32.0f);
        a<c, c> a = eVar.c.a();
        this.v = a;
        a.a.add(this);
        bVar.e(a);
        a<PointF, PointF> a2 = eVar.e.a();
        this.w = a2;
        a2.a.add(this);
        bVar.e(a2);
        a<PointF, PointF> a3 = eVar.f.a();
        this.f379x = a3;
        a3.a.add(this);
        bVar.e(a3);
    }

    public final int[] e(int[] iArr) {
        p pVar = this.f380y;
        if (pVar != null) {
            Integer[] numArr = (Integer[]) pVar.e();
            int i = 0;
            if (iArr.length == numArr.length) {
                while (i < iArr.length) {
                    iArr[i] = numArr[i].intValue();
                    i++;
                }
            } else {
                iArr = new int[numArr.length];
                while (i < numArr.length) {
                    iArr[i] = numArr[i].intValue();
                    i++;
                }
            }
        }
        return iArr;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // b.c.a.w.b.a, b.c.a.w.b.e
    public void f(Canvas canvas, Matrix matrix, int i) {
        RadialGradient radialGradient;
        float f;
        float f2;
        if (!this.p) {
            d(this.f378s, matrix, false);
            if (this.t == 1) {
                long h = h();
                radialGradient = this.q.get(h);
                if (radialGradient == null) {
                    PointF e = this.w.e();
                    PointF e2 = this.f379x.e();
                    c e3 = this.v.e();
                    radialGradient = new LinearGradient(e.x, e.y, e2.x, e2.y, e(e3.f412b), e3.a, Shader.TileMode.CLAMP);
                    this.q.put(h, radialGradient);
                }
            } else {
                long h2 = h();
                radialGradient = this.r.get(h2);
                if (radialGradient == null) {
                    PointF e4 = this.w.e();
                    PointF e5 = this.f379x.e();
                    c e6 = this.v.e();
                    int[] e7 = e(e6.f412b);
                    float[] fArr = e6.a;
                    radialGradient = new RadialGradient(e4.x, e4.y, (float) Math.hypot(e5.x - f, e5.y - f2), e7, fArr, Shader.TileMode.CLAMP);
                    this.r.put(h2, radialGradient);
                }
            }
            radialGradient.setLocalMatrix(matrix);
            this.i.setShader(radialGradient);
            super.f(canvas, matrix, i);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // b.c.a.w.b.a, b.c.a.y.g
    public <T> void g(T t, @Nullable b.c.a.c0.c<T> cVar) {
        super.g(t, cVar);
        if (t == o.D) {
            p pVar = this.f380y;
            if (pVar != null) {
                this.f.u.remove(pVar);
            }
            if (cVar == null) {
                this.f380y = null;
                return;
            }
            p pVar2 = new p(cVar, null);
            this.f380y = pVar2;
            pVar2.a.add(this);
            this.f.e(this.f380y);
        }
    }

    @Override // b.c.a.w.b.c
    public String getName() {
        return this.o;
    }

    public final int h() {
        int round = Math.round(this.w.d * this.u);
        int round2 = Math.round(this.f379x.d * this.u);
        int round3 = Math.round(this.v.d * this.u);
        int i = 17;
        if (round != 0) {
            i = 527 * round;
        }
        if (round2 != 0) {
            i = i * 31 * round2;
        }
        return round3 != 0 ? i * 31 * round3 : i;
    }
}
