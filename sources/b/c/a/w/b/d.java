package b.c.a.w.b;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import androidx.annotation.Nullable;
import b.c.a.c0.c;
import b.c.a.j;
import b.c.a.w.c.a;
import b.c.a.w.c.o;
import b.c.a.y.f;
import b.c.a.y.g;
import b.c.a.y.k.l;
import b.c.a.y.m.b;
import java.util.ArrayList;
import java.util.List;
/* compiled from: ContentGroup.java */
/* loaded from: classes.dex */
public class d implements e, m, a.b, g {
    public Paint a;

    /* renamed from: b  reason: collision with root package name */
    public RectF f374b;
    public final Matrix c;
    public final Path d;
    public final RectF e;
    public final String f;
    public final boolean g;
    public final List<c> h;
    public final j i;
    @Nullable
    public List<m> j;
    @Nullable
    public o k;

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public d(b.c.a.j r8, b.c.a.y.m.b r9, b.c.a.y.l.m r10) {
        /*
            r7 = this;
            java.lang.String r3 = r10.a
            boolean r4 = r10.c
            java.util.List<b.c.a.y.l.b> r0 = r10.f422b
            java.util.ArrayList r5 = new java.util.ArrayList
            int r1 = r0.size()
            r5.<init>(r1)
            r1 = 0
            r2 = 0
        L11:
            int r6 = r0.size()
            if (r2 >= r6) goto L29
            java.lang.Object r6 = r0.get(r2)
            b.c.a.y.l.b r6 = (b.c.a.y.l.b) r6
            b.c.a.w.b.c r6 = r6.a(r8, r9)
            if (r6 == 0) goto L26
            r5.add(r6)
        L26:
            int r2 = r2 + 1
            goto L11
        L29:
            java.util.List<b.c.a.y.l.b> r10 = r10.f422b
        L2b:
            int r0 = r10.size()
            if (r1 >= r0) goto L42
            java.lang.Object r0 = r10.get(r1)
            b.c.a.y.l.b r0 = (b.c.a.y.l.b) r0
            boolean r2 = r0 instanceof b.c.a.y.k.l
            if (r2 == 0) goto L3f
            b.c.a.y.k.l r0 = (b.c.a.y.k.l) r0
            r6 = r0
            goto L44
        L3f:
            int r1 = r1 + 1
            goto L2b
        L42:
            r10 = 0
            r6 = r10
        L44:
            r0 = r7
            r1 = r8
            r2 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.c.a.w.b.d.<init>(b.c.a.j, b.c.a.y.m.b, b.c.a.y.l.m):void");
    }

    @Override // b.c.a.w.c.a.b
    public void a() {
        this.i.invalidateSelf();
    }

    @Override // b.c.a.w.b.c
    public void b(List<c> list, List<c> list2) {
        ArrayList arrayList = new ArrayList(this.h.size() + list.size());
        arrayList.addAll(list);
        for (int size = this.h.size() - 1; size >= 0; size--) {
            c cVar = this.h.get(size);
            cVar.b(arrayList, this.h.subList(0, size));
            arrayList.add(cVar);
        }
    }

    @Override // b.c.a.y.g
    public void c(f fVar, int i, List<f> list, f fVar2) {
        if (fVar.e(this.f, i)) {
            if (!"__container".equals(this.f)) {
                fVar2 = fVar2.a(this.f);
                if (fVar.c(this.f, i)) {
                    list.add(fVar2.g(this));
                }
            }
            if (fVar.f(this.f, i)) {
                int d = fVar.d(this.f, i) + i;
                for (int i2 = 0; i2 < this.h.size(); i2++) {
                    c cVar = this.h.get(i2);
                    if (cVar instanceof g) {
                        ((g) cVar).c(fVar, d, list, fVar2);
                    }
                }
            }
        }
    }

    @Override // b.c.a.w.b.e
    public void d(RectF rectF, Matrix matrix, boolean z2) {
        this.c.set(matrix);
        o oVar = this.k;
        if (oVar != null) {
            this.c.preConcat(oVar.e());
        }
        this.e.set(0.0f, 0.0f, 0.0f, 0.0f);
        for (int size = this.h.size() - 1; size >= 0; size--) {
            c cVar = this.h.get(size);
            if (cVar instanceof e) {
                ((e) cVar).d(this.e, this.c, z2);
                rectF.union(this.e);
            }
        }
    }

    public List<m> e() {
        if (this.j == null) {
            this.j = new ArrayList();
            for (int i = 0; i < this.h.size(); i++) {
                c cVar = this.h.get(i);
                if (cVar instanceof m) {
                    this.j.add((m) cVar);
                }
            }
        }
        return this.j;
    }

    @Override // b.c.a.w.b.e
    public void f(Canvas canvas, Matrix matrix, int i) {
        boolean z2;
        a<Integer, Integer> aVar;
        if (!this.g) {
            this.c.set(matrix);
            o oVar = this.k;
            if (oVar != null) {
                this.c.preConcat(oVar.e());
                i = (int) (((((this.k.j == null ? 100 : aVar.e().intValue()) / 100.0f) * i) / 255.0f) * 255.0f);
            }
            boolean z3 = false;
            if (this.i.A) {
                int i2 = 0;
                int i3 = 0;
                while (true) {
                    if (i2 >= this.h.size()) {
                        z2 = false;
                        break;
                    }
                    if ((this.h.get(i2) instanceof e) && (i3 = i3 + 1) >= 2) {
                        z2 = true;
                        break;
                    }
                    i2++;
                }
                if (z2 && i != 255) {
                    z3 = true;
                }
            }
            if (z3) {
                this.f374b.set(0.0f, 0.0f, 0.0f, 0.0f);
                d(this.f374b, this.c, true);
                this.a.setAlpha(i);
                b.c.a.b0.g.f(canvas, this.f374b, this.a, 31);
            }
            if (z3) {
                i = 255;
            }
            for (int size = this.h.size() - 1; size >= 0; size--) {
                c cVar = this.h.get(size);
                if (cVar instanceof e) {
                    ((e) cVar).f(canvas, this.c, i);
                }
            }
            if (z3) {
                canvas.restore();
            }
        }
    }

    @Override // b.c.a.y.g
    public <T> void g(T t, @Nullable c<T> cVar) {
        o oVar = this.k;
        if (oVar != null) {
            oVar.c(t, cVar);
        }
    }

    @Override // b.c.a.w.b.c
    public String getName() {
        return this.f;
    }

    @Override // b.c.a.w.b.m
    public Path getPath() {
        this.c.reset();
        o oVar = this.k;
        if (oVar != null) {
            this.c.set(oVar.e());
        }
        this.d.reset();
        if (this.g) {
            return this.d;
        }
        for (int size = this.h.size() - 1; size >= 0; size--) {
            c cVar = this.h.get(size);
            if (cVar instanceof m) {
                this.d.addPath(((m) cVar).getPath(), this.c);
            }
        }
        return this.d;
    }

    public d(j jVar, b bVar, String str, boolean z2, List<c> list, @Nullable l lVar) {
        this.a = new b.c.a.w.a();
        this.f374b = new RectF();
        this.c = new Matrix();
        this.d = new Path();
        this.e = new RectF();
        this.f = str;
        this.i = jVar;
        this.g = z2;
        this.h = list;
        if (lVar != null) {
            o oVar = new o(lVar);
            this.k = oVar;
            oVar.a(bVar);
            this.k.b(this);
        }
        ArrayList arrayList = new ArrayList();
        int size = list.size();
        while (true) {
            size--;
            if (size < 0) {
                break;
            }
            c cVar = list.get(size);
            if (cVar instanceof j) {
                arrayList.add((j) cVar);
            }
        }
        int size2 = arrayList.size();
        while (true) {
            size2--;
            if (size2 >= 0) {
                ((j) arrayList.get(size2)).e(list.listIterator(list.size()));
            } else {
                return;
            }
        }
    }
}
