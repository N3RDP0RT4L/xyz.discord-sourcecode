package b.c.a.w.b;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.RectF;
import androidx.annotation.Nullable;
import b.c.a.c0.c;
import b.c.a.j;
import b.c.a.w.c.a;
import b.c.a.w.c.o;
import b.c.a.y.f;
import b.c.a.y.k.l;
import b.c.a.y.m.b;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
/* compiled from: RepeaterContent.java */
/* loaded from: classes.dex */
public class p implements e, m, j, a.b, k {
    public final Matrix a = new Matrix();

    /* renamed from: b  reason: collision with root package name */
    public final Path f384b = new Path();
    public final j c;
    public final b d;
    public final String e;
    public final boolean f;
    public final a<Float, Float> g;
    public final a<Float, Float> h;
    public final o i;
    public d j;

    public p(j jVar, b bVar, b.c.a.y.l.j jVar2) {
        this.c = jVar;
        this.d = bVar;
        this.e = jVar2.a;
        this.f = jVar2.e;
        a<Float, Float> a = jVar2.f419b.a();
        this.g = a;
        bVar.e(a);
        a.a.add(this);
        a<Float, Float> a2 = jVar2.c.a();
        this.h = a2;
        bVar.e(a2);
        a2.a.add(this);
        l lVar = jVar2.d;
        Objects.requireNonNull(lVar);
        o oVar = new o(lVar);
        this.i = oVar;
        oVar.a(bVar);
        oVar.b(this);
    }

    @Override // b.c.a.w.c.a.b
    public void a() {
        this.c.invalidateSelf();
    }

    @Override // b.c.a.w.b.c
    public void b(List<c> list, List<c> list2) {
        this.j.b(list, list2);
    }

    @Override // b.c.a.y.g
    public void c(f fVar, int i, List<f> list, f fVar2) {
        b.c.a.b0.f.f(fVar, i, list, fVar2, this);
    }

    @Override // b.c.a.w.b.e
    public void d(RectF rectF, Matrix matrix, boolean z2) {
        this.j.d(rectF, matrix, z2);
    }

    @Override // b.c.a.w.b.j
    public void e(ListIterator<c> listIterator) {
        if (this.j == null) {
            while (listIterator.hasPrevious() && listIterator.previous() != this) {
            }
            ArrayList arrayList = new ArrayList();
            while (listIterator.hasPrevious()) {
                arrayList.add(listIterator.previous());
                listIterator.remove();
            }
            Collections.reverse(arrayList);
            this.j = new d(this.c, this.d, "Repeater", this.f, arrayList, null);
        }
    }

    @Override // b.c.a.w.b.e
    public void f(Canvas canvas, Matrix matrix, int i) {
        float floatValue = this.g.e().floatValue();
        float floatValue2 = this.h.e().floatValue();
        float floatValue3 = this.i.m.e().floatValue() / 100.0f;
        float floatValue4 = this.i.n.e().floatValue() / 100.0f;
        for (int i2 = ((int) floatValue) - 1; i2 >= 0; i2--) {
            this.a.set(matrix);
            float f = i2;
            this.a.preConcat(this.i.f(f + floatValue2));
            this.j.f(canvas, this.a, (int) (b.c.a.b0.f.e(floatValue3, floatValue4, f / floatValue) * i));
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // b.c.a.y.g
    public <T> void g(T t, @Nullable c<T> cVar) {
        if (!this.i.c(t, cVar)) {
            if (t == b.c.a.o.q) {
                a<Float, Float> aVar = this.g;
                c<Float> cVar2 = aVar.e;
                aVar.e = cVar;
            } else if (t == b.c.a.o.r) {
                a<Float, Float> aVar2 = this.h;
                c<Float> cVar3 = aVar2.e;
                aVar2.e = cVar;
            }
        }
    }

    @Override // b.c.a.w.b.c
    public String getName() {
        return this.e;
    }

    @Override // b.c.a.w.b.m
    public Path getPath() {
        Path path = this.j.getPath();
        this.f384b.reset();
        float floatValue = this.g.e().floatValue();
        float floatValue2 = this.h.e().floatValue();
        for (int i = ((int) floatValue) - 1; i >= 0; i--) {
            this.a.set(this.i.f(i + floatValue2));
            this.f384b.addPath(path, this.a);
        }
        return this.f384b;
    }
}
