package b.c.a.w.b;

import android.graphics.Path;
import b.c.a.j;
import b.c.a.w.c.a;
import b.c.a.y.l.k;
import b.c.a.y.l.n;
import b.c.a.y.m.b;
import java.util.List;
/* compiled from: ShapeContent.java */
/* loaded from: classes.dex */
public class q implements m, a.b {

    /* renamed from: b  reason: collision with root package name */
    public final boolean f385b;
    public final j c;
    public final a<?, Path> d;
    public boolean e;
    public final Path a = new Path();
    public b f = new b();

    public q(j jVar, b bVar, n nVar) {
        this.f385b = nVar.d;
        this.c = jVar;
        a<k, Path> a = nVar.c.a();
        this.d = a;
        bVar.e(a);
        a.a.add(this);
    }

    @Override // b.c.a.w.c.a.b
    public void a() {
        this.e = false;
        this.c.invalidateSelf();
    }

    @Override // b.c.a.w.b.c
    public void b(List<c> list, List<c> list2) {
        for (int i = 0; i < list.size(); i++) {
            c cVar = list.get(i);
            if (cVar instanceof s) {
                s sVar = (s) cVar;
                if (sVar.c == 1) {
                    this.f.a.add(sVar);
                    sVar.f387b.add(this);
                }
            }
        }
    }

    @Override // b.c.a.w.b.m
    public Path getPath() {
        if (this.e) {
            return this.a;
        }
        this.a.reset();
        if (this.f385b) {
            this.e = true;
            return this.a;
        }
        this.a.set(this.d.e());
        this.a.setFillType(Path.FillType.EVEN_ODD);
        this.f.a(this.a);
        this.e = true;
        return this.a;
    }
}
