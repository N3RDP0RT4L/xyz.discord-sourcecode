package b.c.a.w.b;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Shader;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.collection.LongSparseArray;
import b.c.a.j;
import b.c.a.o;
import b.c.a.w.c.a;
import b.c.a.w.c.p;
import b.c.a.y.f;
import b.c.a.y.l.c;
import b.c.a.y.l.d;
import b.c.a.y.m.b;
import java.util.ArrayList;
import java.util.List;
/* compiled from: GradientFillContent.java */
/* loaded from: classes.dex */
public class h implements e, a.b, k {
    @NonNull
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f377b;
    public final b c;
    public final Path f;
    public final int j;
    public final a<c, c> k;
    public final a<Integer, Integer> l;
    public final a<PointF, PointF> m;
    public final a<PointF, PointF> n;
    @Nullable
    public a<ColorFilter, ColorFilter> o;
    @Nullable
    public p p;
    public final j q;
    public final int r;
    public final LongSparseArray<LinearGradient> d = new LongSparseArray<>();
    public final LongSparseArray<RadialGradient> e = new LongSparseArray<>();
    public final Paint g = new b.c.a.w.a(1);
    public final RectF h = new RectF();
    public final List<m> i = new ArrayList();

    public h(j jVar, b bVar, d dVar) {
        Path path = new Path();
        this.f = path;
        this.c = bVar;
        this.a = dVar.g;
        this.f377b = dVar.h;
        this.q = jVar;
        this.j = dVar.a;
        path.setFillType(dVar.f413b);
        this.r = (int) (jVar.k.b() / 32.0f);
        a<c, c> a = dVar.c.a();
        this.k = a;
        a.a.add(this);
        bVar.e(a);
        a<Integer, Integer> a2 = dVar.d.a();
        this.l = a2;
        a2.a.add(this);
        bVar.e(a2);
        a<PointF, PointF> a3 = dVar.e.a();
        this.m = a3;
        a3.a.add(this);
        bVar.e(a3);
        a<PointF, PointF> a4 = dVar.f.a();
        this.n = a4;
        a4.a.add(this);
        bVar.e(a4);
    }

    @Override // b.c.a.w.c.a.b
    public void a() {
        this.q.invalidateSelf();
    }

    @Override // b.c.a.w.b.c
    public void b(List<c> list, List<c> list2) {
        for (int i = 0; i < list2.size(); i++) {
            c cVar = list2.get(i);
            if (cVar instanceof m) {
                this.i.add((m) cVar);
            }
        }
    }

    @Override // b.c.a.y.g
    public void c(f fVar, int i, List<f> list, f fVar2) {
        b.c.a.b0.f.f(fVar, i, list, fVar2, this);
    }

    @Override // b.c.a.w.b.e
    public void d(RectF rectF, Matrix matrix, boolean z2) {
        this.f.reset();
        for (int i = 0; i < this.i.size(); i++) {
            this.f.addPath(this.i.get(i).getPath(), matrix);
        }
        this.f.computeBounds(rectF, false);
        rectF.set(rectF.left - 1.0f, rectF.top - 1.0f, rectF.right + 1.0f, rectF.bottom + 1.0f);
    }

    public final int[] e(int[] iArr) {
        p pVar = this.p;
        if (pVar != null) {
            Integer[] numArr = (Integer[]) pVar.e();
            int i = 0;
            if (iArr.length == numArr.length) {
                while (i < iArr.length) {
                    iArr[i] = numArr[i].intValue();
                    i++;
                }
            } else {
                iArr = new int[numArr.length];
                while (i < numArr.length) {
                    iArr[i] = numArr[i].intValue();
                    i++;
                }
            }
        }
        return iArr;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // b.c.a.w.b.e
    public void f(Canvas canvas, Matrix matrix, int i) {
        RadialGradient radialGradient;
        if (!this.f377b) {
            this.f.reset();
            for (int i2 = 0; i2 < this.i.size(); i2++) {
                this.f.addPath(this.i.get(i2).getPath(), matrix);
            }
            this.f.computeBounds(this.h, false);
            if (this.j == 1) {
                long h = h();
                radialGradient = this.d.get(h);
                if (radialGradient == null) {
                    PointF e = this.m.e();
                    PointF e2 = this.n.e();
                    c e3 = this.k.e();
                    LinearGradient linearGradient = new LinearGradient(e.x, e.y, e2.x, e2.y, e(e3.f412b), e3.a, Shader.TileMode.CLAMP);
                    this.d.put(h, linearGradient);
                    radialGradient = linearGradient;
                }
            } else {
                long h2 = h();
                radialGradient = this.e.get(h2);
                if (radialGradient == null) {
                    PointF e4 = this.m.e();
                    PointF e5 = this.n.e();
                    c e6 = this.k.e();
                    int[] e7 = e(e6.f412b);
                    float[] fArr = e6.a;
                    float f = e4.x;
                    float f2 = e4.y;
                    float hypot = (float) Math.hypot(e5.x - f, e5.y - f2);
                    radialGradient = new RadialGradient(f, f2, hypot <= 0.0f ? 0.001f : hypot, e7, fArr, Shader.TileMode.CLAMP);
                    this.e.put(h2, radialGradient);
                }
            }
            radialGradient.setLocalMatrix(matrix);
            this.g.setShader(radialGradient);
            a<ColorFilter, ColorFilter> aVar = this.o;
            if (aVar != null) {
                this.g.setColorFilter(aVar.e());
            }
            this.g.setAlpha(b.c.a.b0.f.c((int) ((((i / 255.0f) * this.l.e().intValue()) / 100.0f) * 255.0f), 0, 255));
            canvas.drawPath(this.f, this.g);
            b.c.a.c.a("GradientFillContent#draw");
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // b.c.a.y.g
    public <T> void g(T t, @Nullable b.c.a.c0.c<T> cVar) {
        if (t == o.d) {
            a<Integer, Integer> aVar = this.l;
            b.c.a.c0.c<Integer> cVar2 = aVar.e;
            aVar.e = cVar;
        } else if (t == o.C) {
            a<ColorFilter, ColorFilter> aVar2 = this.o;
            if (aVar2 != null) {
                this.c.u.remove(aVar2);
            }
            if (cVar == 0) {
                this.o = null;
                return;
            }
            p pVar = new p(cVar, null);
            this.o = pVar;
            pVar.a.add(this);
            this.c.e(this.o);
        } else if (t == o.D) {
            p pVar2 = this.p;
            if (pVar2 != null) {
                this.c.u.remove(pVar2);
            }
            if (cVar == 0) {
                this.p = null;
                return;
            }
            p pVar3 = new p(cVar, null);
            this.p = pVar3;
            pVar3.a.add(this);
            this.c.e(this.p);
        }
    }

    @Override // b.c.a.w.b.c
    public String getName() {
        return this.a;
    }

    public final int h() {
        int round = Math.round(this.m.d * this.r);
        int round2 = Math.round(this.n.d * this.r);
        int round3 = Math.round(this.k.d * this.r);
        int i = 17;
        if (round != 0) {
            i = 527 * round;
        }
        if (round2 != 0) {
            i = i * 31 * round2;
        }
        return round3 != 0 ? i * 31 * round3 : i;
    }
}
