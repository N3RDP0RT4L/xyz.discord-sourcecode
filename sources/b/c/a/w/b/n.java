package b.c.a.w.b;

import android.graphics.Path;
import android.graphics.PointF;
import androidx.annotation.Nullable;
import b.c.a.c0.c;
import b.c.a.j;
import b.c.a.o;
import b.c.a.w.c.a;
import b.c.a.y.f;
import b.c.a.y.l.h;
import b.c.a.y.m.b;
import com.google.android.material.shadow.ShadowDrawableWrapper;
import java.util.List;
/* compiled from: PolystarContent.java */
/* loaded from: classes.dex */
public class n implements m, a.b, k {

    /* renamed from: b  reason: collision with root package name */
    public final String f382b;
    public final j c;
    public final h.a d;
    public final boolean e;
    public final a<?, Float> f;
    public final a<?, PointF> g;
    public final a<?, Float> h;
    @Nullable
    public final a<?, Float> i;
    public final a<?, Float> j;
    @Nullable
    public final a<?, Float> k;
    public final a<?, Float> l;
    public boolean n;
    public final Path a = new Path();
    public b m = new b();

    public n(j jVar, b bVar, h hVar) {
        this.c = jVar;
        this.f382b = hVar.a;
        h.a aVar = hVar.f417b;
        this.d = aVar;
        this.e = hVar.j;
        a<Float, Float> a = hVar.c.a();
        this.f = a;
        a<PointF, PointF> a2 = hVar.d.a();
        this.g = a2;
        a<Float, Float> a3 = hVar.e.a();
        this.h = a3;
        a<Float, Float> a4 = hVar.g.a();
        this.j = a4;
        a<Float, Float> a5 = hVar.i.a();
        this.l = a5;
        h.a aVar2 = h.a.STAR;
        if (aVar == aVar2) {
            this.i = hVar.f.a();
            this.k = hVar.h.a();
        } else {
            this.i = null;
            this.k = null;
        }
        bVar.e(a);
        bVar.e(a2);
        bVar.e(a3);
        bVar.e(a4);
        bVar.e(a5);
        if (aVar == aVar2) {
            bVar.e(this.i);
            bVar.e(this.k);
        }
        a.a.add(this);
        a2.a.add(this);
        a3.a.add(this);
        a4.a.add(this);
        a5.a.add(this);
        if (aVar == aVar2) {
            this.i.a.add(this);
            this.k.a.add(this);
        }
    }

    @Override // b.c.a.w.c.a.b
    public void a() {
        this.n = false;
        this.c.invalidateSelf();
    }

    @Override // b.c.a.w.b.c
    public void b(List<c> list, List<c> list2) {
        for (int i = 0; i < list.size(); i++) {
            c cVar = list.get(i);
            if (cVar instanceof s) {
                s sVar = (s) cVar;
                if (sVar.c == 1) {
                    this.m.a.add(sVar);
                    sVar.f387b.add(this);
                }
            }
        }
    }

    @Override // b.c.a.y.g
    public void c(f fVar, int i, List<f> list, f fVar2) {
        b.c.a.b0.f.f(fVar, i, list, fVar2, this);
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // b.c.a.y.g
    public <T> void g(T t, @Nullable c<T> cVar) {
        a<?, Float> aVar;
        a<?, Float> aVar2;
        if (t == o.f365s) {
            a<?, Float> aVar3 = this.f;
            c<Float> cVar2 = aVar3.e;
            aVar3.e = cVar;
        } else if (t == o.t) {
            a<?, Float> aVar4 = this.h;
            c<Float> cVar3 = aVar4.e;
            aVar4.e = cVar;
        } else if (t == o.j) {
            a<?, PointF> aVar5 = this.g;
            c<PointF> cVar4 = aVar5.e;
            aVar5.e = cVar;
        } else if (t == o.u && (aVar2 = this.i) != null) {
            c<Float> cVar5 = aVar2.e;
            aVar2.e = cVar;
        } else if (t == o.v) {
            a<?, Float> aVar6 = this.j;
            c<Float> cVar6 = aVar6.e;
            aVar6.e = cVar;
        } else if (t == o.w && (aVar = this.k) != null) {
            c<Float> cVar7 = aVar.e;
            aVar.e = cVar;
        } else if (t == o.f366x) {
            a<?, Float> aVar7 = this.l;
            c<Float> cVar8 = aVar7.e;
            aVar7.e = cVar;
        }
    }

    @Override // b.c.a.w.b.c
    public String getName() {
        return this.f382b;
    }

    @Override // b.c.a.w.b.m
    public Path getPath() {
        float f;
        float f2;
        float f3;
        float f4;
        double d;
        float f5;
        float f6;
        float f7;
        float f8;
        float f9;
        double d2;
        float f10;
        float f11;
        double d3;
        double d4;
        double d5;
        if (this.n) {
            return this.a;
        }
        this.a.reset();
        if (this.e) {
            this.n = true;
            return this.a;
        }
        int ordinal = this.d.ordinal();
        double d6 = ShadowDrawableWrapper.COS_45;
        if (ordinal == 0) {
            float floatValue = this.f.e().floatValue();
            a<?, Float> aVar = this.h;
            if (aVar != null) {
                d6 = aVar.e().floatValue();
            }
            double radians = Math.toRadians(d6 - 90.0d);
            double d7 = floatValue;
            float f12 = (float) (6.283185307179586d / d7);
            float f13 = f12 / 2.0f;
            float f14 = floatValue - ((int) floatValue);
            int i = (f14 > 0.0f ? 1 : (f14 == 0.0f ? 0 : -1));
            if (i != 0) {
                radians += (1.0f - f14) * f13;
            }
            float floatValue2 = this.j.e().floatValue();
            float floatValue3 = this.i.e().floatValue();
            a<?, Float> aVar2 = this.k;
            float floatValue4 = aVar2 != null ? aVar2.e().floatValue() / 100.0f : 0.0f;
            a<?, Float> aVar3 = this.l;
            float floatValue5 = aVar3 != null ? aVar3.e().floatValue() / 100.0f : 0.0f;
            if (i != 0) {
                f5 = b.d.b.a.a.a(floatValue2, floatValue3, f14, floatValue3);
                double d8 = f5;
                f2 = floatValue3;
                f = floatValue4;
                f4 = (float) (Math.cos(radians) * d8);
                f3 = (float) (d8 * Math.sin(radians));
                this.a.moveTo(f4, f3);
                d = radians + ((f12 * f14) / 2.0f);
            } else {
                f2 = floatValue3;
                f = floatValue4;
                double d9 = floatValue2;
                float cos = (float) (Math.cos(radians) * d9);
                f3 = (float) (Math.sin(radians) * d9);
                this.a.moveTo(cos, f3);
                d = radians + f13;
                f4 = cos;
                f5 = 0.0f;
            }
            double ceil = Math.ceil(d7) * 2.0d;
            int i2 = 0;
            boolean z2 = false;
            while (true) {
                double d10 = i2;
                if (d10 >= ceil) {
                    break;
                }
                float f15 = z2 ? floatValue2 : f2;
                int i3 = (f5 > 0.0f ? 1 : (f5 == 0.0f ? 0 : -1));
                if (i3 == 0 || d10 != ceil - 2.0d) {
                    f6 = f12;
                    f7 = f13;
                } else {
                    f7 = (f12 * f14) / 2.0f;
                    f6 = f12;
                }
                if (i3 == 0 || d10 != ceil - 1.0d) {
                    f8 = f5;
                    f5 = f15;
                    f9 = f7;
                } else {
                    f9 = f7;
                    f8 = f5;
                }
                double d11 = f5;
                float cos2 = (float) (Math.cos(d) * d11);
                float sin = (float) (d11 * Math.sin(d));
                if (f == 0.0f && floatValue5 == 0.0f) {
                    this.a.lineTo(cos2, sin);
                    f10 = sin;
                    d2 = d;
                    f11 = floatValue5;
                } else {
                    d2 = d;
                    float f16 = f3;
                    f11 = floatValue5;
                    double atan2 = (float) (Math.atan2(f3, f4) - 1.5707963267948966d);
                    float cos3 = (float) Math.cos(atan2);
                    float sin2 = (float) Math.sin(atan2);
                    f10 = sin;
                    double atan22 = (float) (Math.atan2(sin, cos2) - 1.5707963267948966d);
                    float cos4 = (float) Math.cos(atan22);
                    float sin3 = (float) Math.sin(atan22);
                    float f17 = z2 ? f : f11;
                    float f18 = z2 ? f11 : f;
                    float f19 = (z2 ? f2 : floatValue2) * f17 * 0.47829f;
                    float f20 = cos3 * f19;
                    float f21 = f19 * sin2;
                    float f22 = (z2 ? floatValue2 : f2) * f18 * 0.47829f;
                    float f23 = cos4 * f22;
                    float f24 = f22 * sin3;
                    if (i != 0) {
                        if (i2 == 0) {
                            f20 *= f14;
                            f21 *= f14;
                        } else if (d10 == ceil - 1.0d) {
                            f23 *= f14;
                            f24 *= f14;
                        }
                    }
                    this.a.cubicTo(f4 - f20, f16 - f21, cos2 + f23, f10 + f24, cos2, f10);
                }
                d = d2 + f9;
                z2 = !z2;
                i2++;
                f4 = cos2;
                f5 = f8;
                f12 = f6;
                f3 = f10;
                floatValue5 = f11;
            }
            PointF e = this.g.e();
            this.a.offset(e.x, e.y);
            this.a.close();
        } else if (ordinal == 1) {
            int floor = (int) Math.floor(this.f.e().floatValue());
            a<?, Float> aVar4 = this.h;
            if (aVar4 != null) {
                d6 = aVar4.e().floatValue();
            }
            double radians2 = Math.toRadians(d6 - 90.0d);
            double d12 = floor;
            float floatValue6 = this.l.e().floatValue() / 100.0f;
            float floatValue7 = this.j.e().floatValue();
            double d13 = floatValue7;
            float cos5 = (float) (Math.cos(radians2) * d13);
            float sin4 = (float) (Math.sin(radians2) * d13);
            this.a.moveTo(cos5, sin4);
            double d14 = (float) (6.283185307179586d / d12);
            double d15 = radians2 + d14;
            double ceil2 = Math.ceil(d12);
            int i4 = 0;
            while (i4 < ceil2) {
                float cos6 = (float) (Math.cos(d15) * d13);
                ceil2 = ceil2;
                float sin5 = (float) (Math.sin(d15) * d13);
                if (floatValue6 != 0.0f) {
                    d5 = d13;
                    d4 = d15;
                    double atan23 = (float) (Math.atan2(sin4, cos5) - 1.5707963267948966d);
                    float cos7 = (float) Math.cos(atan23);
                    d3 = d14;
                    double atan24 = (float) (Math.atan2(sin5, cos6) - 1.5707963267948966d);
                    float f25 = floatValue7 * floatValue6 * 0.25f;
                    this.a.cubicTo(cos5 - (cos7 * f25), sin4 - (((float) Math.sin(atan23)) * f25), cos6 + (((float) Math.cos(atan24)) * f25), sin5 + (f25 * ((float) Math.sin(atan24))), cos6, sin5);
                } else {
                    d4 = d15;
                    d5 = d13;
                    d3 = d14;
                    this.a.lineTo(cos6, sin5);
                }
                d15 = d4 + d3;
                i4++;
                sin4 = sin5;
                cos5 = cos6;
                d13 = d5;
                d14 = d3;
            }
            PointF e2 = this.g.e();
            this.a.offset(e2.x, e2.y);
            this.a.close();
        }
        this.a.close();
        this.m.a(this.a);
        this.n = true;
        return this.a;
    }
}
