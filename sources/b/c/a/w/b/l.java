package b.c.a.w.b;

import android.annotation.TargetApi;
import android.graphics.Matrix;
import android.graphics.Path;
import b.c.a.w.c.o;
import b.c.a.y.l.g;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
/* compiled from: MergePathsContent.java */
@TargetApi(19)
/* loaded from: classes.dex */
public class l implements m, j {
    public final Path a = new Path();

    /* renamed from: b  reason: collision with root package name */
    public final Path f381b = new Path();
    public final Path c = new Path();
    public final List<m> d = new ArrayList();
    public final g e;

    public l(g gVar) {
        this.e = gVar;
    }

    @TargetApi(19)
    public final void a(Path.Op op) {
        Matrix matrix;
        Matrix matrix2;
        this.f381b.reset();
        this.a.reset();
        for (int size = this.d.size() - 1; size >= 1; size--) {
            m mVar = this.d.get(size);
            if (mVar instanceof d) {
                d dVar = (d) mVar;
                List<m> e = dVar.e();
                for (int size2 = e.size() - 1; size2 >= 0; size2--) {
                    Path path = e.get(size2).getPath();
                    o oVar = dVar.k;
                    if (oVar != null) {
                        matrix2 = oVar.e();
                    } else {
                        dVar.c.reset();
                        matrix2 = dVar.c;
                    }
                    path.transform(matrix2);
                    this.f381b.addPath(path);
                }
            } else {
                this.f381b.addPath(mVar.getPath());
            }
        }
        m mVar2 = this.d.get(0);
        if (mVar2 instanceof d) {
            d dVar2 = (d) mVar2;
            List<m> e2 = dVar2.e();
            for (int i = 0; i < e2.size(); i++) {
                Path path2 = e2.get(i).getPath();
                o oVar2 = dVar2.k;
                if (oVar2 != null) {
                    matrix = oVar2.e();
                } else {
                    dVar2.c.reset();
                    matrix = dVar2.c;
                }
                path2.transform(matrix);
                this.a.addPath(path2);
            }
        } else {
            this.a.set(mVar2.getPath());
        }
        this.c.op(this.a, this.f381b, op);
    }

    @Override // b.c.a.w.b.c
    public void b(List<c> list, List<c> list2) {
        for (int i = 0; i < this.d.size(); i++) {
            this.d.get(i).b(list, list2);
        }
    }

    @Override // b.c.a.w.b.j
    public void e(ListIterator<c> listIterator) {
        while (listIterator.hasPrevious() && listIterator.previous() != this) {
        }
        while (listIterator.hasPrevious()) {
            c previous = listIterator.previous();
            if (previous instanceof m) {
                this.d.add((m) previous);
                listIterator.remove();
            }
        }
    }

    @Override // b.c.a.w.b.m
    public Path getPath() {
        this.c.reset();
        g gVar = this.e;
        if (gVar.c) {
            return this.c;
        }
        int ordinal = gVar.f416b.ordinal();
        if (ordinal == 0) {
            for (int i = 0; i < this.d.size(); i++) {
                this.c.addPath(this.d.get(i).getPath());
            }
        } else if (ordinal == 1) {
            a(Path.Op.UNION);
        } else if (ordinal == 2) {
            a(Path.Op.REVERSE_DIFFERENCE);
        } else if (ordinal == 3) {
            a(Path.Op.INTERSECT);
        } else if (ordinal == 4) {
            a(Path.Op.XOR);
        }
        return this.c;
    }
}
