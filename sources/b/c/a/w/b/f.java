package b.c.a.w.b;

import android.graphics.Path;
import android.graphics.PointF;
import androidx.annotation.Nullable;
import b.c.a.c0.c;
import b.c.a.j;
import b.c.a.o;
import b.c.a.w.c.a;
import b.c.a.y.m.b;
import java.util.List;
/* compiled from: EllipseContent.java */
/* loaded from: classes.dex */
public class f implements m, a.b, k {

    /* renamed from: b  reason: collision with root package name */
    public final String f375b;
    public final j c;
    public final a<?, PointF> d;
    public final a<?, PointF> e;
    public final b.c.a.y.l.a f;
    public boolean h;
    public final Path a = new Path();
    public b g = new b();

    public f(j jVar, b bVar, b.c.a.y.l.a aVar) {
        this.f375b = aVar.a;
        this.c = jVar;
        a<PointF, PointF> a = aVar.c.a();
        this.d = a;
        a<PointF, PointF> a2 = aVar.f411b.a();
        this.e = a2;
        this.f = aVar;
        bVar.e(a);
        bVar.e(a2);
        a.a.add(this);
        a2.a.add(this);
    }

    @Override // b.c.a.w.c.a.b
    public void a() {
        this.h = false;
        this.c.invalidateSelf();
    }

    @Override // b.c.a.w.b.c
    public void b(List<c> list, List<c> list2) {
        for (int i = 0; i < list.size(); i++) {
            c cVar = list.get(i);
            if (cVar instanceof s) {
                s sVar = (s) cVar;
                if (sVar.c == 1) {
                    this.g.a.add(sVar);
                    sVar.f387b.add(this);
                }
            }
        }
    }

    @Override // b.c.a.y.g
    public void c(b.c.a.y.f fVar, int i, List<b.c.a.y.f> list, b.c.a.y.f fVar2) {
        b.c.a.b0.f.f(fVar, i, list, fVar2, this);
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // b.c.a.y.g
    public <T> void g(T t, @Nullable c<T> cVar) {
        if (t == o.g) {
            a<?, PointF> aVar = this.d;
            c<PointF> cVar2 = aVar.e;
            aVar.e = cVar;
        } else if (t == o.j) {
            a<?, PointF> aVar2 = this.e;
            c<PointF> cVar3 = aVar2.e;
            aVar2.e = cVar;
        }
    }

    @Override // b.c.a.w.b.c
    public String getName() {
        return this.f375b;
    }

    @Override // b.c.a.w.b.m
    public Path getPath() {
        if (this.h) {
            return this.a;
        }
        this.a.reset();
        if (this.f.e) {
            this.h = true;
            return this.a;
        }
        PointF e = this.d.e();
        float f = e.x / 2.0f;
        float f2 = e.y / 2.0f;
        float f3 = f * 0.55228f;
        float f4 = 0.55228f * f2;
        this.a.reset();
        if (this.f.d) {
            float f5 = -f2;
            this.a.moveTo(0.0f, f5);
            float f6 = 0.0f - f3;
            float f7 = -f;
            float f8 = 0.0f - f4;
            this.a.cubicTo(f6, f5, f7, f8, f7, 0.0f);
            float f9 = f4 + 0.0f;
            this.a.cubicTo(f7, f9, f6, f2, 0.0f, f2);
            float f10 = f3 + 0.0f;
            this.a.cubicTo(f10, f2, f, f9, f, 0.0f);
            this.a.cubicTo(f, f8, f10, f5, 0.0f, f5);
        } else {
            float f11 = -f2;
            this.a.moveTo(0.0f, f11);
            float f12 = f3 + 0.0f;
            float f13 = 0.0f - f4;
            this.a.cubicTo(f12, f11, f, f13, f, 0.0f);
            float f14 = f4 + 0.0f;
            this.a.cubicTo(f, f14, f12, f2, 0.0f, f2);
            float f15 = 0.0f - f3;
            float f16 = -f;
            this.a.cubicTo(f15, f2, f16, f14, f16, 0.0f);
            this.a.cubicTo(f16, f13, f15, f11, 0.0f, f11);
        }
        PointF e2 = this.e.e();
        this.a.offset(e2.x, e2.y);
        this.a.close();
        this.g.a(this.a);
        this.h = true;
        return this.a;
    }
}
