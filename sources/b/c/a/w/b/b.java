package b.c.a.w.b;

import android.graphics.Path;
import android.graphics.PathMeasure;
import b.c.a.b0.g;
import b.c.a.w.c.c;
import java.util.ArrayList;
import java.util.List;
/* compiled from: CompoundTrimPathContent.java */
/* loaded from: classes.dex */
public class b {
    public List<s> a = new ArrayList();

    public void a(Path path) {
        for (int size = this.a.size() - 1; size >= 0; size--) {
            s sVar = this.a.get(size);
            PathMeasure pathMeasure = g.a;
            if (sVar != null && !sVar.a) {
                g.a(path, ((c) sVar.d).j() / 100.0f, ((c) sVar.e).j() / 100.0f, ((c) sVar.f).j() / 360.0f);
            }
        }
    }
}
