package b.e.b;

import andhook.lib.HookHelper;
import andhook.lib.xposed.ClassUtils;
import b.d.b.a.a;
import com.discord.widgets.chat.input.MentionUtilsKt;
import h0.a.a.f;
import h0.a.a.p;
import h0.a.a.q;
import h0.a.a.w;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import org.objectweb.asm.Opcodes;
/* compiled from: MethodAccess.java */
/* loaded from: classes2.dex */
public abstract class d {
    public String[] a;

    /* renamed from: b  reason: collision with root package name */
    public Class[][] f451b;

    public static void a(Class cls, ArrayList<Method> arrayList) {
        Method[] declaredMethods;
        for (Method method : cls.getDeclaredMethods()) {
            if (!Modifier.isPrivate(method.getModifiers())) {
                arrayList.add(method);
            }
        }
    }

    public static d b(Class cls) {
        Class[][] clsArr;
        Class<?> cls2;
        String str;
        q qVar;
        q qVar2;
        p pVar;
        int i;
        p[] pVarArr;
        int i2;
        Class[] clsArr2;
        String str2;
        Class[][] clsArr3;
        ArrayList arrayList = new ArrayList();
        boolean isInterface = cls.isInterface();
        if (!isInterface) {
            for (Class cls3 = cls; cls3 != Object.class; cls3 = cls3.getSuperclass()) {
                a(cls3, arrayList);
            }
        } else {
            e(cls, arrayList);
        }
        int size = arrayList.size();
        String[] strArr = new String[size];
        Class[][] clsArr4 = new Class[size];
        Class[] clsArr5 = new Class[size];
        for (int i3 = 0; i3 < size; i3++) {
            Method method = (Method) arrayList.get(i3);
            strArr[i3] = method.getName();
            clsArr4[i3] = method.getParameterTypes();
            clsArr5[i3] = method.getReturnType();
        }
        String name = cls.getName();
        String v = a.v(name, "MethodAccess");
        if (v.startsWith("java.")) {
            v = a.v("reflectasm.", v);
        }
        a b2 = a.b(cls);
        try {
            cls2 = b2.loadClass(v);
            clsArr = clsArr4;
        } catch (ClassNotFoundException unused) {
            synchronized (b2) {
                try {
                    cls2 = b2.loadClass(v);
                    clsArr = clsArr4;
                } catch (ClassNotFoundException unused2) {
                    String replace = v.replace(ClassUtils.PACKAGE_SEPARATOR_CHAR, MentionUtilsKt.SLASH_CHAR);
                    String replace2 = name.replace(ClassUtils.PACKAGE_SEPARATOR_CHAR, MentionUtilsKt.SLASH_CHAR);
                    f fVar = new f(1);
                    fVar.c(Opcodes.V1_1, 33, replace, null, "com/esotericsoftware/reflectasm/MethodAccess", null);
                    q e = fVar.e(1, HookHelper.constructorName, "()V", null, null);
                    e.t(25, 0);
                    e.o(Opcodes.INVOKESPECIAL, "com/esotericsoftware/reflectasm/MethodAccess", HookHelper.constructorName, "()V");
                    e.e(Opcodes.RETURN);
                    e.n(0, 0);
                    q e2 = fVar.e(Opcodes.LOR, "invoke", "(Ljava/lang/Object;I[Ljava/lang/Object;)Ljava/lang/Object;", null, null);
                    if (!arrayList.isEmpty()) {
                        e2.t(25, 1);
                        e2.s(Opcodes.CHECKCAST, replace2);
                        e2.t(58, 4);
                        e2.t(21, 2);
                        p[] pVarArr2 = new p[size];
                        for (int i4 = 0; i4 < size; i4++) {
                            pVarArr2[i4] = new p();
                        }
                        p pVar2 = new p();
                        e2.q(0, size - 1, pVar2, pVarArr2);
                        StringBuilder sb = new StringBuilder(128);
                        int i5 = 0;
                        while (i5 < size) {
                            e2.i(pVarArr2[i5]);
                            if (i5 == 0) {
                                i = i5;
                                pVar = pVar2;
                                pVarArr = pVarArr2;
                                qVar2 = e2;
                                e2.c(1, 1, new Object[]{replace2}, 0, null);
                            } else {
                                i = i5;
                                pVar = pVar2;
                                pVarArr = pVarArr2;
                                qVar2 = e2;
                                qVar2.c(3, 0, null, 0, null);
                            }
                            e2 = qVar2;
                            e2.t(25, 4);
                            sb.setLength(0);
                            sb.append('(');
                            int i6 = i;
                            Class[] clsArr6 = clsArr4[i6];
                            Class cls4 = clsArr5[i6];
                            int i7 = 0;
                            size = size;
                            while (i7 < clsArr6.length) {
                                clsArr5 = clsArr5;
                                e2.t(25, 3);
                                e2.g(16, i7);
                                e2.e(50);
                                w j = w.j(clsArr6[i7]);
                                switch (j.i()) {
                                    case 1:
                                        clsArr3 = clsArr4;
                                        str2 = v;
                                        clsArr2 = clsArr6;
                                        e2.s(Opcodes.CHECKCAST, "java/lang/Boolean");
                                        e2.o(Opcodes.INVOKEVIRTUAL, "java/lang/Boolean", "booleanValue", "()Z");
                                        break;
                                    case 2:
                                        clsArr3 = clsArr4;
                                        str2 = v;
                                        clsArr2 = clsArr6;
                                        e2.s(Opcodes.CHECKCAST, "java/lang/Character");
                                        e2.o(Opcodes.INVOKEVIRTUAL, "java/lang/Character", "charValue", "()C");
                                        break;
                                    case 3:
                                        clsArr3 = clsArr4;
                                        str2 = v;
                                        clsArr2 = clsArr6;
                                        e2.s(Opcodes.CHECKCAST, "java/lang/Byte");
                                        e2.o(Opcodes.INVOKEVIRTUAL, "java/lang/Byte", "byteValue", "()B");
                                        break;
                                    case 4:
                                        clsArr3 = clsArr4;
                                        str2 = v;
                                        clsArr2 = clsArr6;
                                        e2.s(Opcodes.CHECKCAST, "java/lang/Short");
                                        e2.o(Opcodes.INVOKEVIRTUAL, "java/lang/Short", "shortValue", "()S");
                                        break;
                                    case 5:
                                        clsArr3 = clsArr4;
                                        str2 = v;
                                        clsArr2 = clsArr6;
                                        e2.s(Opcodes.CHECKCAST, "java/lang/Integer");
                                        e2.o(Opcodes.INVOKEVIRTUAL, "java/lang/Integer", "intValue", "()I");
                                        break;
                                    case 6:
                                        clsArr3 = clsArr4;
                                        str2 = v;
                                        clsArr2 = clsArr6;
                                        e2.s(Opcodes.CHECKCAST, "java/lang/Float");
                                        e2.o(Opcodes.INVOKEVIRTUAL, "java/lang/Float", "floatValue", "()F");
                                        break;
                                    case 7:
                                        clsArr3 = clsArr4;
                                        str2 = v;
                                        clsArr2 = clsArr6;
                                        e2.s(Opcodes.CHECKCAST, "java/lang/Long");
                                        e2.o(Opcodes.INVOKEVIRTUAL, "java/lang/Long", "longValue", "()J");
                                        break;
                                    case 8:
                                        clsArr2 = clsArr6;
                                        e2.s(Opcodes.CHECKCAST, "java/lang/Double");
                                        clsArr3 = clsArr4;
                                        str2 = v;
                                        e2.o(Opcodes.INVOKEVIRTUAL, "java/lang/Double", "doubleValue", "()D");
                                        break;
                                    case 9:
                                        clsArr2 = clsArr6;
                                        e2.s(Opcodes.CHECKCAST, j.d());
                                        clsArr3 = clsArr4;
                                        str2 = v;
                                        break;
                                    case 10:
                                        clsArr2 = clsArr6;
                                        e2.s(Opcodes.CHECKCAST, j.g());
                                        clsArr3 = clsArr4;
                                        str2 = v;
                                        break;
                                    default:
                                        clsArr3 = clsArr4;
                                        str2 = v;
                                        clsArr2 = clsArr6;
                                        break;
                                }
                                sb.append(j.d());
                                i7++;
                                clsArr6 = clsArr2;
                                clsArr4 = clsArr3;
                                v = str2;
                            }
                            clsArr4 = clsArr4;
                            clsArr5 = clsArr5;
                            v = v;
                            sb.append(')');
                            sb.append(w.e(cls4));
                            if (isInterface) {
                                i2 = Opcodes.INVOKEINTERFACE;
                            } else {
                                i2 = Modifier.isStatic(((Method) arrayList.get(i6)).getModifiers()) ? Opcodes.INVOKESTATIC : Opcodes.INVOKEVIRTUAL;
                            }
                            e2.o(i2, replace2, strArr[i6], sb.toString());
                            switch (w.j(cls4).i()) {
                                case 0:
                                    e2.e(1);
                                    break;
                                case 1:
                                    e2.o(Opcodes.INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;");
                                    break;
                                case 2:
                                    e2.o(Opcodes.INVOKESTATIC, "java/lang/Character", "valueOf", "(C)Ljava/lang/Character;");
                                    break;
                                case 3:
                                    e2.o(Opcodes.INVOKESTATIC, "java/lang/Byte", "valueOf", "(B)Ljava/lang/Byte;");
                                    break;
                                case 4:
                                    e2.o(Opcodes.INVOKESTATIC, "java/lang/Short", "valueOf", "(S)Ljava/lang/Short;");
                                    break;
                                case 5:
                                    e2.o(Opcodes.INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;");
                                    break;
                                case 6:
                                    e2.o(Opcodes.INVOKESTATIC, "java/lang/Float", "valueOf", "(F)Ljava/lang/Float;");
                                    break;
                                case 7:
                                    e2.o(Opcodes.INVOKESTATIC, "java/lang/Long", "valueOf", "(J)Ljava/lang/Long;");
                                    break;
                                case 8:
                                    e2.o(Opcodes.INVOKESTATIC, "java/lang/Double", "valueOf", "(D)Ljava/lang/Double;");
                                    break;
                            }
                            e2.e(Opcodes.ARETURN);
                            i5 = i6 + 1;
                            pVarArr2 = pVarArr;
                            pVar2 = pVar;
                        }
                        clsArr = clsArr4;
                        str = v;
                        e2.i(pVar2);
                        qVar = e2;
                        e2.c(3, 0, null, 0, null);
                    } else {
                        clsArr = clsArr4;
                        str = v;
                        qVar = e2;
                    }
                    qVar.s(Opcodes.NEW, "java/lang/IllegalArgumentException");
                    qVar.e(89);
                    qVar.s(Opcodes.NEW, "java/lang/StringBuilder");
                    qVar.e(89);
                    qVar.j("Method not found: ");
                    qVar.o(Opcodes.INVOKESPECIAL, "java/lang/StringBuilder", HookHelper.constructorName, "(Ljava/lang/String;)V");
                    qVar.t(21, 2);
                    qVar.o(Opcodes.INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;");
                    qVar.o(Opcodes.INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;");
                    qVar.o(Opcodes.INVOKESPECIAL, "java/lang/IllegalArgumentException", HookHelper.constructorName, "(Ljava/lang/String;)V");
                    qVar.e(Opcodes.ATHROW);
                    qVar.n(0, 0);
                    v = str;
                    cls2 = b2.a(v, fVar.b());
                }
            }
        }
        try {
            d dVar = (d) cls2.newInstance();
            dVar.a = strArr;
            dVar.f451b = clsArr;
            return dVar;
        } catch (Throwable th) {
            throw new RuntimeException(a.v("Error constructing method access class: ", v), th);
        }
    }

    public static void e(Class cls, ArrayList<Method> arrayList) {
        a(cls, arrayList);
        for (Class<?> cls2 : cls.getInterfaces()) {
            e(cls2, arrayList);
        }
    }

    public int c(String str, Class... clsArr) {
        int length = this.a.length;
        for (int i = 0; i < length; i++) {
            if (this.a[i].equals(str) && Arrays.equals(clsArr, this.f451b[i])) {
                return i;
            }
        }
        StringBuilder W = a.W("Unable to find non-private method: ", str, " ");
        W.append(Arrays.toString(clsArr));
        throw new IllegalArgumentException(W.toString());
    }

    public abstract Object d(Object obj, int i, Object... objArr);
}
