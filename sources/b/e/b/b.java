package b.e.b;

import andhook.lib.HookHelper;
import h0.a.a.f;
import h0.a.a.q;
import org.objectweb.asm.Opcodes;
/* compiled from: ConstructorAccess.java */
/* loaded from: classes2.dex */
public abstract class b<T> {
    /* JADX WARN: Removed duplicated region for block: B:45:0x0139  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static <T> b.e.b.b<T> a(java.lang.Class<T> r20) {
        /*
            Method dump skipped, instructions count: 459
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.e.b.b.a(java.lang.Class):b.e.b.b");
    }

    public static void b(f fVar, String str, String str2) {
        q e = fVar.e(1, "newInstance", "(Ljava/lang/Object;)Ljava/lang/Object;", null, null);
        if (str2 != null) {
            e.s(Opcodes.NEW, str);
            e.e(89);
            e.t(25, 1);
            e.s(Opcodes.CHECKCAST, str2);
            e.e(89);
            e.o(Opcodes.INVOKEVIRTUAL, "java/lang/Object", "getClass", "()Ljava/lang/Class;");
            e.e(87);
            e.o(Opcodes.INVOKESPECIAL, str, HookHelper.constructorName, "(L" + str2 + ";)V");
            e.e(Opcodes.ARETURN);
            e.n(4, 2);
            return;
        }
        e.s(Opcodes.NEW, "java/lang/UnsupportedOperationException");
        e.e(89);
        e.j("Not an inner class.");
        e.o(Opcodes.INVOKESPECIAL, "java/lang/UnsupportedOperationException", HookHelper.constructorName, "(Ljava/lang/String;)V");
        e.e(Opcodes.ATHROW);
        e.n(3, 2);
    }

    public abstract T c();
}
