package b.e.b;

import andhook.lib.HookHelper;
import andhook.lib.xposed.ClassUtils;
import b.d.b.a.a;
import com.discord.widgets.chat.input.MentionUtilsKt;
import h0.a.a.f;
import h0.a.a.p;
import h0.a.a.q;
import h0.a.a.w;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import org.objectweb.asm.Opcodes;
/* compiled from: FieldAccess.java */
/* loaded from: classes2.dex */
public abstract class c {
    public String[] a;

    public static c a(Class cls) {
        Class<?> cls2;
        Field[] declaredFields;
        ArrayList arrayList = new ArrayList();
        Class cls3 = cls;
        while (true) {
            if (cls3 == Object.class) {
                break;
            }
            for (Field field : cls3.getDeclaredFields()) {
                int modifiers = field.getModifiers();
                if (!Modifier.isStatic(modifiers) && !Modifier.isPrivate(modifiers)) {
                    arrayList.add(field);
                }
            }
            cls3 = cls3.getSuperclass();
        }
        int size = arrayList.size();
        String[] strArr = new String[size];
        Class[] clsArr = new Class[arrayList.size()];
        for (int i = 0; i < size; i++) {
            strArr[i] = ((Field) arrayList.get(i)).getName();
            clsArr[i] = ((Field) arrayList.get(i)).getType();
        }
        String name = cls.getName();
        String v = a.v(name, "FieldAccess");
        if (v.startsWith("java.")) {
            v = a.v("reflectasm.", v);
        }
        a b2 = a.b(cls);
        try {
            cls2 = b2.loadClass(v);
        } catch (ClassNotFoundException unused) {
            synchronized (b2) {
                try {
                    cls2 = b2.loadClass(v);
                } catch (ClassNotFoundException unused2) {
                    String replace = v.replace(ClassUtils.PACKAGE_SEPARATOR_CHAR, MentionUtilsKt.SLASH_CHAR);
                    String replace2 = name.replace(ClassUtils.PACKAGE_SEPARATOR_CHAR, MentionUtilsKt.SLASH_CHAR);
                    f fVar = new f(0);
                    fVar.c(Opcodes.V1_1, 33, replace, null, "com/esotericsoftware/reflectasm/FieldAccess", null);
                    q e = fVar.e(1, HookHelper.constructorName, "()V", null, null);
                    e.t(25, 0);
                    e.o(Opcodes.INVOKESPECIAL, "com/esotericsoftware/reflectasm/FieldAccess", HookHelper.constructorName, "()V");
                    e.e(Opcodes.RETURN);
                    e.n(1, 1);
                    m(fVar, replace2, arrayList);
                    p(fVar, replace2, arrayList);
                    n(fVar, replace2, arrayList, w.f3698b);
                    q(fVar, replace2, arrayList, w.f3698b);
                    n(fVar, replace2, arrayList, w.d);
                    q(fVar, replace2, arrayList, w.d);
                    n(fVar, replace2, arrayList, w.e);
                    q(fVar, replace2, arrayList, w.e);
                    n(fVar, replace2, arrayList, w.f);
                    q(fVar, replace2, arrayList, w.f);
                    n(fVar, replace2, arrayList, w.h);
                    q(fVar, replace2, arrayList, w.h);
                    n(fVar, replace2, arrayList, w.i);
                    q(fVar, replace2, arrayList, w.i);
                    n(fVar, replace2, arrayList, w.g);
                    q(fVar, replace2, arrayList, w.g);
                    n(fVar, replace2, arrayList, w.c);
                    q(fVar, replace2, arrayList, w.c);
                    o(fVar, replace2, arrayList);
                    cls2 = b2.a(v, fVar.b());
                }
            }
        }
        try {
            c cVar = (c) cls2.newInstance();
            cVar.a = strArr;
            return cVar;
        } catch (Throwable th) {
            throw new RuntimeException(a.v("Error constructing field access class: ", v), th);
        }
    }

    public static void m(f fVar, String str, ArrayList<Field> arrayList) {
        int i;
        q e = fVar.e(1, "get", "(Ljava/lang/Object;I)Ljava/lang/Object;", null, null);
        e.t(21, 2);
        if (!arrayList.isEmpty()) {
            i = 5;
            int size = arrayList.size();
            p[] pVarArr = new p[size];
            for (int i2 = 0; i2 < size; i2++) {
                pVarArr[i2] = new p();
            }
            p pVar = new p();
            e.q(0, size - 1, pVar, pVarArr);
            for (int i3 = 0; i3 < size; i3++) {
                Field field = arrayList.get(i3);
                e.i(pVarArr[i3]);
                e.c(3, 0, null, 0, null);
                e.t(25, 1);
                e.s(Opcodes.CHECKCAST, str);
                e.b(180, str, field.getName(), w.e(field.getType()));
                switch (w.j(field.getType()).i()) {
                    case 1:
                        e.o(Opcodes.INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;");
                        break;
                    case 2:
                        e.o(Opcodes.INVOKESTATIC, "java/lang/Character", "valueOf", "(C)Ljava/lang/Character;");
                        break;
                    case 3:
                        e.o(Opcodes.INVOKESTATIC, "java/lang/Byte", "valueOf", "(B)Ljava/lang/Byte;");
                        break;
                    case 4:
                        e.o(Opcodes.INVOKESTATIC, "java/lang/Short", "valueOf", "(S)Ljava/lang/Short;");
                        break;
                    case 5:
                        e.o(Opcodes.INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;");
                        break;
                    case 6:
                        e.o(Opcodes.INVOKESTATIC, "java/lang/Float", "valueOf", "(F)Ljava/lang/Float;");
                        break;
                    case 7:
                        e.o(Opcodes.INVOKESTATIC, "java/lang/Long", "valueOf", "(J)Ljava/lang/Long;");
                        break;
                    case 8:
                        e.o(Opcodes.INVOKESTATIC, "java/lang/Double", "valueOf", "(D)Ljava/lang/Double;");
                        break;
                }
                e.e(Opcodes.ARETURN);
            }
            e.i(pVar);
            e.c(3, 0, null, 0, null);
        } else {
            i = 6;
        }
        r(e);
        e.n(i, 3);
    }

    public static void n(f fVar, String str, ArrayList<Field> arrayList, w wVar) {
        String str2;
        int i;
        String d = wVar.d();
        int i2 = wVar.i();
        int i3 = Opcodes.IRETURN;
        switch (i2) {
            case 1:
                str2 = "getBoolean";
                break;
            case 2:
                str2 = "getChar";
                break;
            case 3:
                str2 = "getByte";
                break;
            case 4:
                str2 = "getShort";
                break;
            case 5:
                str2 = "getInt";
                break;
            case 6:
                i3 = Opcodes.FRETURN;
                str2 = "getFloat";
                break;
            case 7:
                i3 = Opcodes.LRETURN;
                str2 = "getLong";
                break;
            case 8:
                i3 = 175;
                str2 = "getDouble";
                break;
            default:
                i3 = Opcodes.ARETURN;
                str2 = "get";
                break;
        }
        q e = fVar.e(1, str2, a.v("(Ljava/lang/Object;I)", d), null, null);
        e.t(21, 2);
        if (!arrayList.isEmpty()) {
            int size = arrayList.size();
            p[] pVarArr = new p[size];
            p pVar = new p();
            boolean z2 = false;
            for (int i4 = 0; i4 < size; i4++) {
                if (w.j(arrayList.get(i4).getType()).equals(wVar)) {
                    pVarArr[i4] = new p();
                } else {
                    pVarArr[i4] = pVar;
                    z2 = true;
                }
            }
            p pVar2 = new p();
            e.q(0, size - 1, pVar2, pVarArr);
            for (int i5 = 0; i5 < size; i5++) {
                Field field = arrayList.get(i5);
                if (!pVarArr[i5].equals(pVar)) {
                    e.i(pVarArr[i5]);
                    e.c(3, 0, null, 0, null);
                    e.t(25, 1);
                    e.s(Opcodes.CHECKCAST, str);
                    e.b(180, str, field.getName(), d);
                    e.e(i3);
                }
            }
            if (z2) {
                e.i(pVar);
                e.c(3, 0, null, 0, null);
                s(e, wVar.c());
            }
            e.i(pVar2);
            e.c(3, 0, null, 0, null);
            i = 5;
        } else {
            i = 6;
        }
        r(e);
        e.n(i, 3);
    }

    public static void o(f fVar, String str, ArrayList<Field> arrayList) {
        int i;
        q e = fVar.e(1, "getString", "(Ljava/lang/Object;I)Ljava/lang/String;", null, null);
        e.t(21, 2);
        if (!arrayList.isEmpty()) {
            int size = arrayList.size();
            p[] pVarArr = new p[size];
            p pVar = new p();
            boolean z2 = false;
            for (int i2 = 0; i2 < size; i2++) {
                if (arrayList.get(i2).getType().equals(String.class)) {
                    pVarArr[i2] = new p();
                } else {
                    pVarArr[i2] = pVar;
                    z2 = true;
                }
            }
            p pVar2 = new p();
            e.q(0, size - 1, pVar2, pVarArr);
            for (int i3 = 0; i3 < size; i3++) {
                if (!pVarArr[i3].equals(pVar)) {
                    e.i(pVarArr[i3]);
                    e.c(3, 0, null, 0, null);
                    e.t(25, 1);
                    e.s(Opcodes.CHECKCAST, str);
                    e.b(180, str, arrayList.get(i3).getName(), "Ljava/lang/String;");
                    e.e(Opcodes.ARETURN);
                }
            }
            if (z2) {
                e.i(pVar);
                e.c(3, 0, null, 0, null);
                s(e, "String");
            }
            e.i(pVar2);
            e.c(3, 0, null, 0, null);
            i = 5;
        } else {
            i = 6;
        }
        r(e);
        e.n(i, 3);
    }

    public static void p(f fVar, String str, ArrayList<Field> arrayList) {
        int i;
        q e = fVar.e(1, "set", "(Ljava/lang/Object;ILjava/lang/Object;)V", null, null);
        e.t(21, 2);
        if (!arrayList.isEmpty()) {
            i = 5;
            int size = arrayList.size();
            p[] pVarArr = new p[size];
            for (int i2 = 0; i2 < size; i2++) {
                pVarArr[i2] = new p();
            }
            p pVar = new p();
            e.q(0, size - 1, pVar, pVarArr);
            for (int i3 = 0; i3 < size; i3++) {
                Field field = arrayList.get(i3);
                w j = w.j(field.getType());
                e.i(pVarArr[i3]);
                e.c(3, 0, null, 0, null);
                e.t(25, 1);
                e.s(Opcodes.CHECKCAST, str);
                e.t(25, 3);
                switch (j.i()) {
                    case 1:
                        e.s(Opcodes.CHECKCAST, "java/lang/Boolean");
                        e.o(Opcodes.INVOKEVIRTUAL, "java/lang/Boolean", "booleanValue", "()Z");
                        break;
                    case 2:
                        e.s(Opcodes.CHECKCAST, "java/lang/Character");
                        e.o(Opcodes.INVOKEVIRTUAL, "java/lang/Character", "charValue", "()C");
                        break;
                    case 3:
                        e.s(Opcodes.CHECKCAST, "java/lang/Byte");
                        e.o(Opcodes.INVOKEVIRTUAL, "java/lang/Byte", "byteValue", "()B");
                        break;
                    case 4:
                        e.s(Opcodes.CHECKCAST, "java/lang/Short");
                        e.o(Opcodes.INVOKEVIRTUAL, "java/lang/Short", "shortValue", "()S");
                        break;
                    case 5:
                        e.s(Opcodes.CHECKCAST, "java/lang/Integer");
                        e.o(Opcodes.INVOKEVIRTUAL, "java/lang/Integer", "intValue", "()I");
                        break;
                    case 6:
                        e.s(Opcodes.CHECKCAST, "java/lang/Float");
                        e.o(Opcodes.INVOKEVIRTUAL, "java/lang/Float", "floatValue", "()F");
                        break;
                    case 7:
                        e.s(Opcodes.CHECKCAST, "java/lang/Long");
                        e.o(Opcodes.INVOKEVIRTUAL, "java/lang/Long", "longValue", "()J");
                        break;
                    case 8:
                        e.s(Opcodes.CHECKCAST, "java/lang/Double");
                        e.o(Opcodes.INVOKEVIRTUAL, "java/lang/Double", "doubleValue", "()D");
                        break;
                    case 9:
                        e.s(Opcodes.CHECKCAST, j.d());
                        break;
                    case 10:
                        e.s(Opcodes.CHECKCAST, j.g());
                        break;
                }
                e.b(Opcodes.PUTFIELD, str, field.getName(), j.d());
                e.e(Opcodes.RETURN);
            }
            e.i(pVar);
            e.c(3, 0, null, 0, null);
        } else {
            i = 6;
        }
        r(e);
        e.n(i, 4);
    }

    /* JADX WARN: Removed duplicated region for block: B:18:0x005c  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x010d  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static void q(h0.a.a.f r19, java.lang.String r20, java.util.ArrayList<java.lang.reflect.Field> r21, h0.a.a.w r22) {
        /*
            Method dump skipped, instructions count: 298
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.e.b.c.q(h0.a.a.f, java.lang.String, java.util.ArrayList, h0.a.a.w):void");
    }

    public static q r(q qVar) {
        qVar.s(Opcodes.NEW, "java/lang/IllegalArgumentException");
        qVar.e(89);
        qVar.s(Opcodes.NEW, "java/lang/StringBuilder");
        qVar.e(89);
        qVar.j("Field not found: ");
        qVar.o(Opcodes.INVOKESPECIAL, "java/lang/StringBuilder", HookHelper.constructorName, "(Ljava/lang/String;)V");
        qVar.t(21, 2);
        qVar.o(Opcodes.INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;");
        qVar.o(Opcodes.INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;");
        qVar.o(Opcodes.INVOKESPECIAL, "java/lang/IllegalArgumentException", HookHelper.constructorName, "(Ljava/lang/String;)V");
        qVar.e(Opcodes.ATHROW);
        return qVar;
    }

    public static q s(q qVar, String str) {
        qVar.s(Opcodes.NEW, "java/lang/IllegalArgumentException");
        qVar.e(89);
        qVar.s(Opcodes.NEW, "java/lang/StringBuilder");
        qVar.e(89);
        qVar.j("Field not declared as " + str + ": ");
        qVar.o(Opcodes.INVOKESPECIAL, "java/lang/StringBuilder", HookHelper.constructorName, "(Ljava/lang/String;)V");
        qVar.t(21, 2);
        qVar.o(Opcodes.INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;");
        qVar.o(Opcodes.INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;");
        qVar.o(Opcodes.INVOKESPECIAL, "java/lang/IllegalArgumentException", HookHelper.constructorName, "(Ljava/lang/String;)V");
        qVar.e(Opcodes.ATHROW);
        return qVar;
    }

    public abstract void A(Object obj, int i, long j);

    public abstract void B(Object obj, int i, short s2);

    public abstract Object b(Object obj, int i);

    public abstract boolean c(Object obj, int i);

    public abstract byte d(Object obj, int i);

    public abstract char e(Object obj, int i);

    public abstract double f(Object obj, int i);

    public abstract float g(Object obj, int i);

    public int h(String str) {
        int length = this.a.length;
        for (int i = 0; i < length; i++) {
            if (this.a[i].equals(str)) {
                return i;
            }
        }
        throw new IllegalArgumentException(a.v("Unable to find non-private field: ", str));
    }

    public abstract int i(Object obj, int i);

    public abstract long j(Object obj, int i);

    public abstract short k(Object obj, int i);

    public abstract String l(Object obj, int i);

    public abstract void t(Object obj, int i, Object obj2);

    public abstract void u(Object obj, int i, boolean z2);

    public abstract void v(Object obj, int i, byte b2);

    public abstract void w(Object obj, int i, char c);

    public abstract void x(Object obj, int i, double d);

    public abstract void y(Object obj, int i, float f);

    public abstract void z(Object obj, int i, int i2);
}
