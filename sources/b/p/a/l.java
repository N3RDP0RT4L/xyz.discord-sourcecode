package b.p.a;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import androidx.annotation.RequiresApi;
import d0.z.d.m;
/* compiled from: SwipeDismissTouchListener.kt */
/* loaded from: classes3.dex */
public final class l implements View.OnTouchListener {
    public final int j;
    public final int k;
    public final long l;
    public int m = 1;
    public float n;
    public float o;
    public boolean p;
    public int q;
    public VelocityTracker r;

    /* renamed from: s  reason: collision with root package name */
    public float f1970s;
    public final View t;
    public final a u;

    /* compiled from: SwipeDismissTouchListener.kt */
    /* loaded from: classes3.dex */
    public interface a {
        void a(View view, boolean z2);

        boolean b();

        void onDismiss(View view);
    }

    /* compiled from: SwipeDismissTouchListener.kt */
    /* loaded from: classes3.dex */
    public static final class b extends AnimatorListenerAdapter {
        public b(MotionEvent motionEvent, View view) {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            m.checkNotNullParameter(animator, "animation");
            l lVar = l.this;
            ViewGroup.LayoutParams layoutParams = lVar.t.getLayoutParams();
            int height = lVar.t.getHeight();
            ValueAnimator duration = ValueAnimator.ofInt(height, 1).setDuration(lVar.l);
            duration.addListener(new m(lVar, layoutParams, height));
            duration.addUpdateListener(new n(lVar, layoutParams));
            duration.start();
        }
    }

    public l(View view, a aVar) {
        m.checkNotNullParameter(view, "mView");
        m.checkNotNullParameter(aVar, "mCallbacks");
        this.t = view;
        this.u = aVar;
        ViewConfiguration viewConfiguration = ViewConfiguration.get(view.getContext());
        m.checkNotNullExpressionValue(viewConfiguration, "vc");
        this.j = viewConfiguration.getScaledTouchSlop();
        this.k = viewConfiguration.getScaledMinimumFlingVelocity() * 16;
        Context context = view.getContext();
        m.checkNotNullExpressionValue(context, "mView.context");
        this.l = context.getResources().getInteger(17694720);
    }

    @Override // android.view.View.OnTouchListener
    @RequiresApi(api = 12)
    public boolean onTouch(View view, MotionEvent motionEvent) {
        boolean z2;
        m.checkNotNullParameter(view, "view");
        m.checkNotNullParameter(motionEvent, "motionEvent");
        motionEvent.offsetLocation(this.f1970s, 0.0f);
        if (this.m < 2) {
            this.m = this.t.getWidth();
        }
        int actionMasked = motionEvent.getActionMasked();
        boolean z3 = true;
        if (actionMasked != 0) {
            if (actionMasked == 1) {
                VelocityTracker velocityTracker = this.r;
                if (velocityTracker != null) {
                    float rawX = motionEvent.getRawX() - this.n;
                    velocityTracker.addMovement(motionEvent);
                    velocityTracker.computeCurrentVelocity(1000);
                    float xVelocity = velocityTracker.getXVelocity();
                    float abs = Math.abs(xVelocity);
                    float abs2 = Math.abs(velocityTracker.getYVelocity());
                    if (Math.abs(rawX) > this.m / 2 && this.p) {
                        z2 = rawX > ((float) 0);
                    } else if (this.k > abs || abs2 >= abs || !this.p) {
                        z2 = false;
                        z3 = false;
                    } else {
                        float f = 0;
                        boolean z4 = ((xVelocity > f ? 1 : (xVelocity == f ? 0 : -1)) < 0) == ((rawX > f ? 1 : (rawX == f ? 0 : -1)) < 0);
                        if (velocityTracker.getXVelocity() <= f) {
                            z3 = false;
                        }
                        z2 = z3;
                        z3 = z4;
                    }
                    if (z3) {
                        this.t.animate().translationX(z2 ? this.m : -this.m).alpha(0.0f).setDuration(this.l).setListener(new b(motionEvent, view));
                    } else if (this.p) {
                        this.t.animate().translationX(0.0f).alpha(1.0f).setDuration(this.l).setListener(null);
                        this.u.a(view, false);
                    }
                    velocityTracker.recycle();
                    this.r = null;
                    this.f1970s = 0.0f;
                    this.n = 0.0f;
                    this.o = 0.0f;
                    this.p = false;
                }
            } else if (actionMasked == 2) {
                VelocityTracker velocityTracker2 = this.r;
                if (velocityTracker2 != null) {
                    velocityTracker2.addMovement(motionEvent);
                    float rawX2 = motionEvent.getRawX() - this.n;
                    float rawY = motionEvent.getRawY() - this.o;
                    if (Math.abs(rawX2) > this.j && Math.abs(rawY) < Math.abs(rawX2) / 2) {
                        this.p = true;
                        this.q = rawX2 > ((float) 0) ? this.j : -this.j;
                        this.t.getParent().requestDisallowInterceptTouchEvent(true);
                        MotionEvent obtain = MotionEvent.obtain(motionEvent);
                        m.checkNotNullExpressionValue(obtain, "cancelEvent");
                        obtain.setAction((motionEvent.getActionIndex() << 8) | 3);
                        this.t.onTouchEvent(obtain);
                        obtain.recycle();
                    }
                    if (this.p) {
                        this.f1970s = rawX2;
                        this.t.setTranslationX(rawX2 - this.q);
                        this.t.setAlpha(Math.max(0.0f, Math.min(1.0f, 1.0f - ((Math.abs(rawX2) * 2.0f) / this.m))));
                        return true;
                    }
                }
            } else if (actionMasked != 3) {
                view.performClick();
                return false;
            } else {
                VelocityTracker velocityTracker3 = this.r;
                if (velocityTracker3 != null) {
                    this.t.animate().translationX(0.0f).alpha(1.0f).setDuration(this.l).setListener(null);
                    velocityTracker3.recycle();
                    this.r = null;
                    this.f1970s = 0.0f;
                    this.n = 0.0f;
                    this.o = 0.0f;
                    this.p = false;
                }
            }
            return false;
        }
        this.n = motionEvent.getRawX();
        this.o = motionEvent.getRawY();
        if (this.u.b()) {
            VelocityTracker obtain2 = VelocityTracker.obtain();
            this.r = obtain2;
            m.checkNotNull(obtain2);
            obtain2.addMovement(motionEvent);
        }
        this.u.a(view, true);
        return false;
    }
}
