package b.h.a.a;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RSRuntimeException;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
/* compiled from: AndroidStockBlurImpl.java */
@TargetApi(17)
/* loaded from: classes2.dex */
public class a implements c {
    public static Boolean a;

    /* renamed from: b  reason: collision with root package name */
    public RenderScript f735b;
    public ScriptIntrinsicBlur c;
    public Allocation d;
    public Allocation e;

    @Override // b.h.a.a.c
    public void a(Bitmap bitmap, Bitmap bitmap2) {
        this.d.copyFrom(bitmap);
        this.c.setInput(this.d);
        this.c.forEach(this.e);
        this.e.copyTo(bitmap2);
    }

    @Override // b.h.a.a.c
    public boolean b(Context context, Bitmap bitmap, float f) {
        boolean z2 = true;
        if (this.f735b == null) {
            try {
                RenderScript create = RenderScript.create(context);
                this.f735b = create;
                this.c = ScriptIntrinsicBlur.create(create, Element.U8_4(create));
            } catch (RSRuntimeException e) {
                if (a == null && context != null) {
                    a = Boolean.valueOf((context.getApplicationInfo().flags & 2) != 0);
                }
                if (a != Boolean.TRUE) {
                    z2 = false;
                }
                if (!z2) {
                    release();
                    return false;
                }
                throw e;
            }
        }
        this.c.setRadius(f);
        Allocation createFromBitmap = Allocation.createFromBitmap(this.f735b, bitmap, Allocation.MipmapControl.MIPMAP_NONE, 1);
        this.d = createFromBitmap;
        this.e = Allocation.createTyped(this.f735b, createFromBitmap.getType());
        return true;
    }

    @Override // b.h.a.a.c
    public void release() {
        Allocation allocation = this.d;
        if (allocation != null) {
            allocation.destroy();
            this.d = null;
        }
        Allocation allocation2 = this.e;
        if (allocation2 != null) {
            allocation2.destroy();
            this.e = null;
        }
        ScriptIntrinsicBlur scriptIntrinsicBlur = this.c;
        if (scriptIntrinsicBlur != null) {
            scriptIntrinsicBlur.destroy();
            this.c = null;
        }
        RenderScript renderScript = this.f735b;
        if (renderScript != null) {
            renderScript.destroy();
            this.f735b = null;
        }
    }
}
