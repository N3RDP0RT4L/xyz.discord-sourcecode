package b.q.a.k;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
/* compiled from: FastBitmapDrawable.java */
/* loaded from: classes3.dex */
public class a extends Drawable {

    /* renamed from: b  reason: collision with root package name */
    public Bitmap f1980b;
    public int d;
    public int e;
    public final Paint a = new Paint(2);
    public int c = 255;

    public a(Bitmap bitmap) {
        this.f1980b = bitmap;
        if (bitmap != null) {
            this.d = bitmap.getWidth();
            this.e = this.f1980b.getHeight();
            return;
        }
        this.e = 0;
        this.d = 0;
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        Bitmap bitmap = this.f1980b;
        if (bitmap != null && !bitmap.isRecycled()) {
            canvas.drawBitmap(this.f1980b, (Rect) null, getBounds(), this.a);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public int getAlpha() {
        return this.c;
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        return this.e;
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        return this.d;
    }

    @Override // android.graphics.drawable.Drawable
    public int getMinimumHeight() {
        return this.e;
    }

    @Override // android.graphics.drawable.Drawable
    public int getMinimumWidth() {
        return this.d;
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        this.c = i;
        this.a.setAlpha(i);
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        this.a.setColorFilter(colorFilter);
    }

    @Override // android.graphics.drawable.Drawable
    public void setFilterBitmap(boolean z2) {
        this.a.setFilterBitmap(z2);
    }
}
