package b.q.a.k;

import android.util.Log;
import com.adjust.sdk.Constants;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Objects;
/* compiled from: ImageHeaderParser.java */
/* loaded from: classes3.dex */
public class b {
    public static final byte[] a = "Exif\u0000\u0000".getBytes(Charset.forName(Constants.ENCODING));

    /* renamed from: b  reason: collision with root package name */
    public static final int[] f1981b = {0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8};
    public final a c;

    /* compiled from: ImageHeaderParser.java */
    /* loaded from: classes3.dex */
    public interface a {
    }

    /* compiled from: ImageHeaderParser.java */
    /* renamed from: b.q.a.k.b$b  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static class C0180b implements a {
        public final InputStream a;

        public C0180b(InputStream inputStream) {
            this.a = inputStream;
        }

        public int a() throws IOException {
            return ((this.a.read() << 8) & 65280) | (this.a.read() & 255);
        }
    }

    public b(InputStream inputStream) {
        this.c = new C0180b(inputStream);
    }

    public int a() throws IOException {
        int i;
        int a2 = ((C0180b) this.c).a();
        if (!((a2 & 65496) == 65496 || a2 == 19789 || a2 == 18761)) {
            if (Log.isLoggable("ImageHeaderParser", 3)) {
                b.d.b.a.a.l0("Parser doesn't handle magic number: ", a2, "ImageHeaderParser");
            }
            return -1;
        }
        while (true) {
            short read = (short) (((C0180b) this.c).a.read() & 255);
            if (read == 255) {
                short read2 = (short) (((C0180b) this.c).a.read() & 255);
                if (read2 == 218) {
                    break;
                } else if (read2 != 217) {
                    i = ((C0180b) this.c).a() - 2;
                    if (read2 == 225) {
                        break;
                    }
                    long j = i;
                    C0180b bVar = (C0180b) this.c;
                    Objects.requireNonNull(bVar);
                    long j2 = 0;
                    if (j >= 0) {
                        long j3 = j;
                        while (j3 > 0) {
                            long skip = bVar.a.skip(j3);
                            if (skip <= 0) {
                                if (bVar.a.read() == -1) {
                                    break;
                                }
                                skip = 1;
                            }
                            j3 -= skip;
                        }
                        j2 = j - j3;
                    }
                    if (j2 != j) {
                        if (Log.isLoggable("ImageHeaderParser", 3)) {
                            StringBuilder U = b.d.b.a.a.U("Unable to skip enough data, type: ", read2, ", wanted to skip: ", i, ", but actually skipped: ");
                            U.append(j2);
                            Log.d("ImageHeaderParser", U.toString());
                        }
                    }
                } else if (Log.isLoggable("ImageHeaderParser", 3)) {
                    Log.d("ImageHeaderParser", "Found MARKER_EOI in exif segment");
                }
            } else if (Log.isLoggable("ImageHeaderParser", 3)) {
                b.d.b.a.a.l0("Unknown segmentId=", read, "ImageHeaderParser");
            }
        }
        i = -1;
        if (i == -1) {
            if (Log.isLoggable("ImageHeaderParser", 3)) {
                Log.d("ImageHeaderParser", "Failed to parse exif segment length, or exif segment not found");
            }
            return -1;
        }
        byte[] bArr = new byte[i];
        C0180b bVar2 = (C0180b) this.c;
        Objects.requireNonNull(bVar2);
        int i2 = i;
        while (i2 > 0) {
            int read3 = bVar2.a.read(bArr, i - i2, i2);
            if (read3 == -1) {
                break;
            }
            i2 -= read3;
        }
        int i3 = i - i2;
        if (i3 == i) {
            boolean z2 = i > a.length;
            if (z2) {
                int i4 = 0;
                while (true) {
                    byte[] bArr2 = a;
                    if (i4 >= bArr2.length) {
                        break;
                    } else if (bArr[i4] != bArr2[i4]) {
                        z2 = false;
                        break;
                    } else {
                        i4++;
                    }
                }
            }
            if (z2) {
                ByteBuffer wrap = ByteBuffer.wrap(bArr);
                ByteOrder byteOrder = ByteOrder.BIG_ENDIAN;
                ByteBuffer byteBuffer = (ByteBuffer) wrap.order(byteOrder).limit(i);
                short s2 = byteBuffer.getShort(6);
                if (s2 != 19789) {
                    if (s2 == 18761) {
                        byteOrder = ByteOrder.LITTLE_ENDIAN;
                    } else if (Log.isLoggable("ImageHeaderParser", 3)) {
                        b.d.b.a.a.l0("Unknown endianness = ", s2, "ImageHeaderParser");
                    }
                }
                byteBuffer.order(byteOrder);
                int i5 = byteBuffer.getInt(10) + 6;
                short s3 = byteBuffer.getShort(i5);
                for (int i6 = 0; i6 < s3; i6++) {
                    int i7 = (i6 * 12) + i5 + 2;
                    short s4 = byteBuffer.getShort(i7);
                    if (s4 == 274) {
                        short s5 = byteBuffer.getShort(i7 + 2);
                        if (s5 >= 1 && s5 <= 12) {
                            int i8 = byteBuffer.getInt(i7 + 4);
                            if (i8 >= 0) {
                                if (Log.isLoggable("ImageHeaderParser", 3)) {
                                    StringBuilder U2 = b.d.b.a.a.U("Got tagIndex=", i6, " tagType=", s4, " formatCode=");
                                    U2.append((int) s5);
                                    U2.append(" componentCount=");
                                    U2.append(i8);
                                    Log.d("ImageHeaderParser", U2.toString());
                                }
                                int i9 = i8 + f1981b[s5];
                                if (i9 <= 4) {
                                    int i10 = i7 + 8;
                                    if (i10 < 0 || i10 > byteBuffer.remaining()) {
                                        if (Log.isLoggable("ImageHeaderParser", 3)) {
                                            Log.d("ImageHeaderParser", "Illegal tagValueOffset=" + i10 + " tagType=" + ((int) s4));
                                        }
                                    } else if (i9 >= 0 && i9 + i10 <= byteBuffer.remaining()) {
                                        return byteBuffer.getShort(i10);
                                    } else {
                                        if (Log.isLoggable("ImageHeaderParser", 3)) {
                                            b.d.b.a.a.l0("Illegal number of bytes for TI tag data tagType=", s4, "ImageHeaderParser");
                                        }
                                    }
                                } else if (Log.isLoggable("ImageHeaderParser", 3)) {
                                    b.d.b.a.a.l0("Got byte count > 4, not orientation, continuing, formatCode=", s5, "ImageHeaderParser");
                                }
                            } else if (Log.isLoggable("ImageHeaderParser", 3)) {
                                Log.d("ImageHeaderParser", "Negative tiff component count");
                            }
                        } else if (Log.isLoggable("ImageHeaderParser", 3)) {
                            b.d.b.a.a.l0("Got invalid format code = ", s5, "ImageHeaderParser");
                        }
                    }
                }
                return -1;
            } else if (!Log.isLoggable("ImageHeaderParser", 3)) {
                return -1;
            } else {
                Log.d("ImageHeaderParser", "Missing jpeg exif preamble");
                return -1;
            }
        } else if (!Log.isLoggable("ImageHeaderParser", 3)) {
            return -1;
        } else {
            Log.d("ImageHeaderParser", "Unable to read exif segment data, length: " + i + ", actually read: " + i3);
            return -1;
        }
    }
}
