package b.q.a.l;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import androidx.annotation.IntRange;
import androidx.annotation.Nullable;
import b.i.a.f.e.o.f;
import b.q.a.h.c;
import b.q.a.l.c;
import com.yalantis.ucrop.UCropActivity;
import java.lang.ref.WeakReference;
import java.util.Arrays;
/* compiled from: CropImageView.java */
/* loaded from: classes3.dex */
public class a extends c {
    public float A;
    public float B;
    public c C;
    public Runnable D;
    public Runnable E;
    public float F;
    public float G;
    public int H;
    public int I;
    public long J;

    /* renamed from: y  reason: collision with root package name */
    public final RectF f1983y;

    /* renamed from: z  reason: collision with root package name */
    public final Matrix f1984z;

    /* compiled from: CropImageView.java */
    /* renamed from: b.q.a.l.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static class RunnableC0181a implements Runnable {
        public final WeakReference<a> j;
        public final long k;
        public final long l = System.currentTimeMillis();
        public final float m;
        public final float n;
        public final float o;
        public final float p;
        public final float q;
        public final float r;

        /* renamed from: s  reason: collision with root package name */
        public final boolean f1985s;

        public RunnableC0181a(a aVar, long j, float f, float f2, float f3, float f4, float f5, float f6, boolean z2) {
            this.j = new WeakReference<>(aVar);
            this.k = j;
            this.m = f;
            this.n = f2;
            this.o = f3;
            this.p = f4;
            this.q = f5;
            this.r = f6;
            this.f1985s = z2;
        }

        @Override // java.lang.Runnable
        public void run() {
            a aVar = this.j.get();
            if (aVar != null) {
                float min = (float) Math.min(this.k, System.currentTimeMillis() - this.l);
                float f = this.o;
                float f2 = (float) this.k;
                float f3 = (min / f2) - 1.0f;
                float f4 = (f3 * f3 * f3) + 1.0f;
                float f5 = (f * f4) + 0.0f;
                float f6 = (f4 * this.p) + 0.0f;
                float T = f.T(min, 0.0f, this.r, f2);
                if (min < ((float) this.k)) {
                    float[] fArr = aVar.k;
                    aVar.g(f5 - (fArr[0] - this.m), f6 - (fArr[1] - this.n));
                    if (!this.f1985s) {
                        aVar.l(this.q + T, aVar.f1983y.centerX(), aVar.f1983y.centerY());
                    }
                    if (!aVar.j(aVar.j)) {
                        aVar.post(this);
                    }
                }
            }
        }
    }

    /* compiled from: CropImageView.java */
    /* loaded from: classes3.dex */
    public static class b implements Runnable {
        public final WeakReference<a> j;
        public final long k;
        public final long l = System.currentTimeMillis();
        public final float m;
        public final float n;
        public final float o;
        public final float p;

        public b(a aVar, long j, float f, float f2, float f3, float f4) {
            this.j = new WeakReference<>(aVar);
            this.k = j;
            this.m = f;
            this.n = f2;
            this.o = f3;
            this.p = f4;
        }

        @Override // java.lang.Runnable
        public void run() {
            a aVar = this.j.get();
            if (aVar != null) {
                float min = (float) Math.min(this.k, System.currentTimeMillis() - this.l);
                float T = f.T(min, 0.0f, this.n, (float) this.k);
                if (min < ((float) this.k)) {
                    aVar.l(this.m + T, this.o, this.p);
                    aVar.post(this);
                    return;
                }
                aVar.setImageToWrapCropBounds(true);
            }
        }
    }

    public a(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @Override // b.q.a.l.c
    public void d() {
        int i;
        int i2;
        int i3;
        super.d();
        Drawable drawable = getDrawable();
        if (drawable != null) {
            float intrinsicWidth = drawable.getIntrinsicWidth();
            float intrinsicHeight = drawable.getIntrinsicHeight();
            if (this.A == 0.0f) {
                this.A = intrinsicWidth / intrinsicHeight;
            }
            int i4 = this.n;
            float f = i4;
            float f2 = this.A;
            int i5 = (int) (f / f2);
            int i6 = this.o;
            if (i5 > i6) {
                float f3 = i6;
                this.f1983y.set((i4 - ((int) (f2 * f3))) / 2, 0.0f, i2 + i3, f3);
            } else {
                this.f1983y.set(0.0f, (i6 - i5) / 2, f, i5 + i);
            }
            h(intrinsicWidth, intrinsicHeight);
            float width = this.f1983y.width();
            float height = this.f1983y.height();
            float max = Math.max(this.f1983y.width() / intrinsicWidth, this.f1983y.height() / intrinsicHeight);
            RectF rectF = this.f1983y;
            float f4 = ((height - (intrinsicHeight * max)) / 2.0f) + rectF.top;
            this.m.reset();
            this.m.postScale(max, max);
            this.m.postTranslate(((width - (intrinsicWidth * max)) / 2.0f) + rectF.left, f4);
            setImageMatrix(this.m);
            c cVar = this.C;
            if (cVar != null) {
                ((d) cVar).a.k.setTargetAspectRatio(this.A);
            }
            c.a aVar = this.p;
            if (aVar != null) {
                ((UCropActivity.a) aVar).b(getCurrentScale());
                ((UCropActivity.a) this.p).a(getCurrentAngle());
            }
        }
    }

    @Override // b.q.a.l.c
    public void f(float f, float f2, float f3) {
        if (f > 1.0f && getCurrentScale() * f <= getMaxScale()) {
            super.f(f, f2, f3);
        } else if (f < 1.0f && getCurrentScale() * f >= getMinScale()) {
            super.f(f, f2, f3);
        }
    }

    @Nullable
    public b.q.a.h.c getCropBoundsChangeListener() {
        return this.C;
    }

    public float getMaxScale() {
        return this.F;
    }

    public float getMinScale() {
        return this.G;
    }

    public float getTargetAspectRatio() {
        return this.A;
    }

    public final void h(float f, float f2) {
        float min = Math.min(Math.min(this.f1983y.width() / f, this.f1983y.width() / f2), Math.min(this.f1983y.height() / f2, this.f1983y.height() / f));
        this.G = min;
        this.F = min * this.B;
    }

    public void i() {
        removeCallbacks(this.D);
        removeCallbacks(this.E);
    }

    public boolean j(float[] fArr) {
        this.f1984z.reset();
        this.f1984z.setRotate(-getCurrentAngle());
        float[] copyOf = Arrays.copyOf(fArr, fArr.length);
        this.f1984z.mapPoints(copyOf);
        float[] f02 = f.f0(this.f1983y);
        this.f1984z.mapPoints(f02);
        return f.y1(copyOf).contains(f.y1(f02));
    }

    public void k(float f) {
        e(f, this.f1983y.centerX(), this.f1983y.centerY());
    }

    public void l(float f, float f2, float f3) {
        if (f <= getMaxScale()) {
            f(f / getCurrentScale(), f2, f3);
        }
    }

    public void setCropBoundsChangeListener(@Nullable b.q.a.h.c cVar) {
        this.C = cVar;
    }

    public void setCropRect(RectF rectF) {
        this.A = rectF.width() / rectF.height();
        this.f1983y.set(rectF.left - getPaddingLeft(), rectF.top - getPaddingTop(), rectF.right - getPaddingRight(), rectF.bottom - getPaddingBottom());
        Drawable drawable = getDrawable();
        if (drawable != null) {
            h(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        }
        setImageToWrapCropBounds(true);
    }

    public void setImageToWrapCropBounds(boolean z2) {
        float f;
        float f2;
        float f3;
        float f4;
        if (this.t && !j(this.j)) {
            float[] fArr = this.k;
            float f5 = fArr[0];
            float f6 = fArr[1];
            float currentScale = getCurrentScale();
            float centerX = this.f1983y.centerX() - f5;
            float centerY = this.f1983y.centerY() - f6;
            this.f1984z.reset();
            this.f1984z.setTranslate(centerX, centerY);
            float[] fArr2 = this.j;
            float[] copyOf = Arrays.copyOf(fArr2, fArr2.length);
            this.f1984z.mapPoints(copyOf);
            boolean j = j(copyOf);
            if (j) {
                this.f1984z.reset();
                this.f1984z.setRotate(-getCurrentAngle());
                float[] fArr3 = this.j;
                float[] copyOf2 = Arrays.copyOf(fArr3, fArr3.length);
                float[] f02 = f.f0(this.f1983y);
                this.f1984z.mapPoints(copyOf2);
                this.f1984z.mapPoints(f02);
                RectF y1 = f.y1(copyOf2);
                RectF y12 = f.y1(f02);
                float f7 = y1.left - y12.left;
                float f8 = y1.top - y12.top;
                float f9 = y1.right - y12.right;
                float f10 = y1.bottom - y12.bottom;
                float[] fArr4 = new float[4];
                if (f7 <= 0.0f) {
                    f7 = 0.0f;
                }
                fArr4[0] = f7;
                if (f8 <= 0.0f) {
                    f8 = 0.0f;
                }
                fArr4[1] = f8;
                if (f9 >= 0.0f) {
                    f9 = 0.0f;
                }
                fArr4[2] = f9;
                if (f10 >= 0.0f) {
                    f10 = 0.0f;
                }
                fArr4[3] = f10;
                this.f1984z.reset();
                this.f1984z.setRotate(getCurrentAngle());
                this.f1984z.mapPoints(fArr4);
                f4 = -(fArr4[0] + fArr4[2]);
                f3 = -(fArr4[1] + fArr4[3]);
                f = currentScale;
                f2 = 0.0f;
            } else {
                RectF rectF = new RectF(this.f1983y);
                this.f1984z.reset();
                this.f1984z.setRotate(getCurrentAngle());
                this.f1984z.mapRect(rectF);
                float[] fArr5 = this.j;
                f = currentScale;
                float[] fArr6 = {(float) Math.sqrt(Math.pow(fArr5[1] - fArr5[3], 2.0d) + Math.pow(fArr5[0] - fArr5[2], 2.0d)), (float) Math.sqrt(Math.pow(fArr5[3] - fArr5[5], 2.0d) + Math.pow(fArr5[2] - fArr5[4], 2.0d))};
                f2 = (Math.max(rectF.width() / fArr6[0], rectF.height() / fArr6[1]) * f) - f;
                f4 = centerX;
                f3 = centerY;
            }
            if (z2) {
                RunnableC0181a aVar = new RunnableC0181a(this, this.J, f5, f6, f4, f3, f, f2, j);
                this.D = aVar;
                post(aVar);
                return;
            }
            g(f4, f3);
            if (!j) {
                l(f + f2, this.f1983y.centerX(), this.f1983y.centerY());
            }
        }
    }

    public void setImageToWrapCropBoundsAnimDuration(@IntRange(from = 100) long j) {
        if (j > 0) {
            this.J = j;
            return;
        }
        throw new IllegalArgumentException("Animation duration cannot be negative value.");
    }

    public void setMaxResultImageSizeX(@IntRange(from = 10) int i) {
        this.H = i;
    }

    public void setMaxResultImageSizeY(@IntRange(from = 10) int i) {
        this.I = i;
    }

    public void setMaxScaleMultiplier(float f) {
        this.B = f;
    }

    public void setTargetAspectRatio(float f) {
        Drawable drawable = getDrawable();
        if (drawable == null) {
            this.A = f;
            return;
        }
        if (f == 0.0f) {
            this.A = drawable.getIntrinsicWidth() / drawable.getIntrinsicHeight();
        } else {
            this.A = f;
        }
        b.q.a.h.c cVar = this.C;
        if (cVar != null) {
            ((d) cVar).a.k.setTargetAspectRatio(this.A);
        }
    }

    public a(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f1983y = new RectF();
        this.f1984z = new Matrix();
        this.B = 10.0f;
        this.E = null;
        this.H = 0;
        this.I = 0;
        this.J = 500L;
    }
}
