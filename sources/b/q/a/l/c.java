package b.q.a.l;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import b.i.a.f.e.o.f;
import b.q.a.i.b;
import com.yalantis.ucrop.UCropActivity;
/* compiled from: TransformImageView.java */
/* loaded from: classes3.dex */
public class c extends AppCompatImageView {
    public final float[] j;
    public final float[] k;
    public final float[] l;
    public Matrix m;
    public int n;
    public int o;
    public a p;
    public float[] q;
    public float[] r;

    /* renamed from: s  reason: collision with root package name */
    public boolean f1986s;
    public boolean t;
    public int u;
    public String v;
    public String w;

    /* renamed from: x  reason: collision with root package name */
    public b f1987x;

    /* compiled from: TransformImageView.java */
    /* loaded from: classes3.dex */
    public interface a {
    }

    public c(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public float a(@NonNull Matrix matrix) {
        matrix.getValues(this.l);
        float[] fArr = this.l;
        matrix.getValues(fArr);
        return (float) (-(Math.atan2(fArr[1], this.l[0]) * 57.29577951308232d));
    }

    public float b(@NonNull Matrix matrix) {
        matrix.getValues(this.l);
        double pow = Math.pow(this.l[0], 2.0d);
        matrix.getValues(this.l);
        return (float) Math.sqrt(Math.pow(this.l[3], 2.0d) + pow);
    }

    public void c() {
        setScaleType(ImageView.ScaleType.MATRIX);
    }

    public void d() {
        Drawable drawable = getDrawable();
        if (drawable != null) {
            float intrinsicWidth = drawable.getIntrinsicWidth();
            float intrinsicHeight = drawable.getIntrinsicHeight();
            Log.d("TransformImageView", String.format("Image size: [%d:%d]", Integer.valueOf((int) intrinsicWidth), Integer.valueOf((int) intrinsicHeight)));
            RectF rectF = new RectF(0.0f, 0.0f, intrinsicWidth, intrinsicHeight);
            this.q = f.f0(rectF);
            this.r = f.d0(rectF);
            this.t = true;
            a aVar = this.p;
            if (aVar != null) {
                UCropActivity.a aVar2 = (UCropActivity.a) aVar;
                UCropActivity.this.v.animate().alpha(1.0f).setDuration(300L).setInterpolator(new AccelerateInterpolator());
                UCropActivity.this.H.setClickable(false);
                UCropActivity uCropActivity = UCropActivity.this;
                uCropActivity.u = false;
                uCropActivity.supportInvalidateOptionsMenu();
            }
        }
    }

    public void e(float f, float f2, float f3) {
        if (f != 0.0f) {
            this.m.postRotate(f, f2, f3);
            setImageMatrix(this.m);
            a aVar = this.p;
            if (aVar != null) {
                ((UCropActivity.a) aVar).a(a(this.m));
            }
        }
    }

    public void f(float f, float f2, float f3) {
        if (f != 0.0f) {
            this.m.postScale(f, f, f2, f3);
            setImageMatrix(this.m);
            a aVar = this.p;
            if (aVar != null) {
                ((UCropActivity.a) aVar).b(b(this.m));
            }
        }
    }

    public void g(float f, float f2) {
        if (f != 0.0f || f2 != 0.0f) {
            this.m.postTranslate(f, f2);
            setImageMatrix(this.m);
        }
    }

    public float getCurrentAngle() {
        return a(this.m);
    }

    public float getCurrentScale() {
        return b(this.m);
    }

    public b getExifInfo() {
        return this.f1987x;
    }

    public String getImageInputPath() {
        return this.v;
    }

    public String getImageOutputPath() {
        return this.w;
    }

    public int getMaxBitmapSize() {
        int i;
        if (this.u <= 0) {
            WindowManager windowManager = (WindowManager) getContext().getSystemService("window");
            Point point = new Point();
            if (windowManager != null) {
                windowManager.getDefaultDisplay().getSize(point);
            }
            int i2 = point.x;
            int i3 = point.y;
            int sqrt = (int) Math.sqrt(Math.pow(i3, 2.0d) + Math.pow(i2, 2.0d));
            Canvas canvas = new Canvas();
            int min = Math.min(canvas.getMaximumBitmapWidth(), canvas.getMaximumBitmapHeight());
            if (min > 0) {
                sqrt = Math.min(sqrt, min);
            }
            try {
                i = f.m0();
            } catch (Exception e) {
                Log.d("EglUtils", "getMaxTextureSize: ", e);
                i = 0;
            }
            if (i > 0) {
                sqrt = Math.min(sqrt, i);
            }
            b.d.b.a.a.l0("maxBitmapSize: ", sqrt, "BitmapLoadUtils");
            this.u = sqrt;
        }
        return this.u;
    }

    @Nullable
    public Bitmap getViewBitmap() {
        if (getDrawable() == null || !(getDrawable() instanceof b.q.a.k.a)) {
            return null;
        }
        return ((b.q.a.k.a) getDrawable()).f1980b;
    }

    @Override // android.view.View
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
        super.onLayout(z2, i, i2, i3, i4);
        if (z2 || (this.f1986s && !this.t)) {
            int paddingLeft = getPaddingLeft();
            int paddingTop = getPaddingTop();
            this.n = (getWidth() - getPaddingRight()) - paddingLeft;
            this.o = (getHeight() - getPaddingBottom()) - paddingTop;
            d();
        }
    }

    @Override // androidx.appcompat.widget.AppCompatImageView, android.widget.ImageView
    public void setImageBitmap(Bitmap bitmap) {
        setImageDrawable(new b.q.a.k.a(bitmap));
    }

    @Override // android.widget.ImageView
    public void setImageMatrix(Matrix matrix) {
        super.setImageMatrix(matrix);
        this.m.set(matrix);
        this.m.mapPoints(this.j, this.q);
        this.m.mapPoints(this.k, this.r);
    }

    public void setMaxBitmapSize(int i) {
        this.u = i;
    }

    @Override // android.widget.ImageView
    public void setScaleType(ImageView.ScaleType scaleType) {
        if (scaleType == ImageView.ScaleType.MATRIX) {
            super.setScaleType(scaleType);
        } else {
            Log.w("TransformImageView", "Invalid ScaleType. Only ScaleType.MATRIX can be used");
        }
    }

    public void setTransformImageListener(a aVar) {
        this.p = aVar;
    }

    public c(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.j = new float[8];
        this.k = new float[2];
        this.l = new float[9];
        this.m = new Matrix();
        this.f1986s = false;
        this.t = false;
        this.u = 0;
        c();
    }
}
