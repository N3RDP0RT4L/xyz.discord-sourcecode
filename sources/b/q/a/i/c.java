package b.q.a.i;

import android.graphics.RectF;
/* compiled from: ImageState.java */
/* loaded from: classes3.dex */
public class c {
    public RectF a;

    /* renamed from: b  reason: collision with root package name */
    public RectF f1976b;
    public float c;
    public float d;

    public c(RectF rectF, RectF rectF2, float f, float f2) {
        this.a = rectF;
        this.f1976b = rectF2;
        this.c = f;
        this.d = f2;
    }
}
