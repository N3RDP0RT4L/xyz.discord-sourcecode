package b.q.a.i;

import android.graphics.Bitmap;
/* compiled from: CropParameters.java */
/* loaded from: classes3.dex */
public class a {
    public int a;

    /* renamed from: b  reason: collision with root package name */
    public int f1974b;
    public Bitmap.CompressFormat c;
    public int d;
    public String e;
    public String f;
    public b g;

    public a(int i, int i2, Bitmap.CompressFormat compressFormat, int i3, String str, String str2, b bVar) {
        this.a = i;
        this.f1974b = i2;
        this.c = compressFormat;
        this.d = i3;
        this.e = str;
        this.f = str2;
        this.g = bVar;
    }
}
