package b.q.a.j;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.q.a.l.c;
import com.adjust.sdk.Constants;
import com.yalantis.ucrop.UCropActivity;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
/* compiled from: BitmapLoadTask.java */
/* loaded from: classes3.dex */
public class b extends AsyncTask<Void, Void, a> {
    public final Context a;

    /* renamed from: b  reason: collision with root package name */
    public Uri f1978b;
    public Uri c;
    public final int d;
    public final int e;
    public final b.q.a.h.b f;

    public b(@NonNull Context context, @NonNull Uri uri, @Nullable Uri uri2, int i, int i2, b.q.a.h.b bVar) {
        this.a = context;
        this.f1978b = uri;
        this.c = uri2;
        this.d = i;
        this.e = i2;
        this.f = bVar;
    }

    public final void a(@NonNull Uri uri, @Nullable Uri uri2) throws NullPointerException, IOException {
        Throwable th;
        InputStream inputStream;
        Log.d("BitmapWorkerTask", "copyFile");
        Objects.requireNonNull(uri2, "Output Uri is null - cannot copy image");
        FileOutputStream fileOutputStream = null;
        try {
            inputStream = this.a.getContentResolver().openInputStream(uri);
            try {
                FileOutputStream fileOutputStream2 = new FileOutputStream(new File(uri2.getPath()));
                try {
                    if (inputStream != null) {
                        byte[] bArr = new byte[1024];
                        while (true) {
                            int read = inputStream.read(bArr);
                            if (read > 0) {
                                fileOutputStream2.write(bArr, 0, read);
                            } else {
                                try {
                                    break;
                                } catch (IOException unused) {
                                }
                            }
                        }
                        fileOutputStream2.close();
                        try {
                            inputStream.close();
                        } catch (IOException unused2) {
                        }
                        this.f1978b = this.c;
                        return;
                    }
                    throw new NullPointerException("InputStream for given input Uri is null");
                } catch (Throwable th2) {
                    th = th2;
                    fileOutputStream = fileOutputStream2;
                    if (fileOutputStream != null) {
                        try {
                            fileOutputStream.close();
                        } catch (IOException unused3) {
                        }
                    }
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException unused4) {
                        }
                    }
                    this.f1978b = this.c;
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
            }
        } catch (Throwable th4) {
            th = th4;
            inputStream = null;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:46:0x0085 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:52:0x0080 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void b(@androidx.annotation.NonNull android.net.Uri r6, @androidx.annotation.Nullable android.net.Uri r7) throws java.lang.NullPointerException, java.io.IOException {
        /*
            r5 = this;
            java.lang.String r0 = "BitmapWorkerTask"
            java.lang.String r1 = "downloadFile"
            android.util.Log.d(r0, r1)
            java.lang.String r0 = "Output Uri is null - cannot download image"
            java.util.Objects.requireNonNull(r7, r0)
            f0.x r0 = new f0.x
            r0.<init>()
            r1 = 0
            okhttp3.Request$a r2 = new okhttp3.Request$a     // Catch: java.lang.Throwable -> L7b
            r2.<init>()     // Catch: java.lang.Throwable -> L7b
            java.lang.String r6 = r6.toString()     // Catch: java.lang.Throwable -> L7b
            r2.f(r6)     // Catch: java.lang.Throwable -> L7b
            okhttp3.Request r6 = r2.a()     // Catch: java.lang.Throwable -> L7b
            f0.e r6 = r0.b(r6)     // Catch: java.lang.Throwable -> L7b
            f0.e0.g.e r6 = (f0.e0.g.e) r6     // Catch: java.lang.Throwable -> L78
            okhttp3.Response r6 = r6.execute()     // Catch: java.lang.Throwable -> L78
            okhttp3.ResponseBody r2 = r6.p     // Catch: java.lang.Throwable -> L75
            g0.g r2 = r2.c()     // Catch: java.lang.Throwable -> L75
            android.content.Context r3 = r5.a     // Catch: java.lang.Throwable -> L71
            android.content.ContentResolver r3 = r3.getContentResolver()     // Catch: java.lang.Throwable -> L71
            java.io.OutputStream r7 = r3.openOutputStream(r7)     // Catch: java.lang.Throwable -> L71
            if (r7 == 0) goto L69
            java.lang.String r3 = "$this$sink"
            d0.z.d.m.checkParameterIsNotNull(r7, r3)     // Catch: java.lang.Throwable -> L71
            g0.p r3 = new g0.p     // Catch: java.lang.Throwable -> L71
            g0.y r4 = new g0.y     // Catch: java.lang.Throwable -> L71
            r4.<init>()     // Catch: java.lang.Throwable -> L71
            r3.<init>(r7, r4)     // Catch: java.lang.Throwable -> L71
            r2.k0(r3)     // Catch: java.lang.Throwable -> L67
            r2.close()     // Catch: java.io.IOException -> L53
        L53:
            r3.close()     // Catch: java.io.IOException -> L56
        L56:
            okhttp3.ResponseBody r6 = r6.p
            if (r6 == 0) goto L5d
            r6.close()     // Catch: java.io.IOException -> L5d
        L5d:
            f0.q r6 = r0.m
            r6.a()
            android.net.Uri r6 = r5.c
            r5.f1978b = r6
            return
        L67:
            r7 = move-exception
            goto L73
        L69:
            java.lang.NullPointerException r7 = new java.lang.NullPointerException     // Catch: java.lang.Throwable -> L71
            java.lang.String r3 = "OutputStream for given output Uri is null"
            r7.<init>(r3)     // Catch: java.lang.Throwable -> L71
            throw r7     // Catch: java.lang.Throwable -> L71
        L71:
            r7 = move-exception
            r3 = r1
        L73:
            r1 = r2
            goto L7e
        L75:
            r7 = move-exception
            r3 = r1
            goto L7e
        L78:
            r6 = move-exception
            r7 = r6
            goto L7c
        L7b:
            r7 = move-exception
        L7c:
            r6 = r1
            r3 = r6
        L7e:
            if (r1 == 0) goto L83
            r1.close()     // Catch: java.io.IOException -> L83
        L83:
            if (r3 == 0) goto L88
            r3.close()     // Catch: java.io.IOException -> L88
        L88:
            if (r6 == 0) goto L91
            okhttp3.ResponseBody r6 = r6.p
            if (r6 == 0) goto L91
            r6.close()     // Catch: java.io.IOException -> L91
        L91:
            f0.q r6 = r0.m
            r6.a()
            android.net.Uri r6 = r5.c
            r5.f1978b = r6
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: b.q.a.j.b.b(android.net.Uri, android.net.Uri):void");
    }

    public final void c() throws NullPointerException, IOException {
        String scheme = this.f1978b.getScheme();
        Log.d("BitmapWorkerTask", "Uri scheme: " + scheme);
        if ("http".equals(scheme) || Constants.SCHEME.equals(scheme)) {
            try {
                b(this.f1978b, this.c);
            } catch (IOException | NullPointerException e) {
                Log.e("BitmapWorkerTask", "Downloading failed", e);
                throw e;
            }
        } else if ("content".equals(scheme)) {
            try {
                a(this.f1978b, this.c);
            } catch (IOException | NullPointerException e2) {
                Log.e("BitmapWorkerTask", "Copying failed", e2);
                throw e2;
            }
        } else if (!"file".equals(scheme)) {
            Log.e("BitmapWorkerTask", "Invalid Uri scheme " + scheme);
            throw new IllegalArgumentException(b.d.b.a.a.v("Invalid Uri scheme", scheme));
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:62:0x013c  */
    /* JADX WARN: Removed duplicated region for block: B:63:0x013e  */
    /* JADX WARN: Removed duplicated region for block: B:64:0x0141  */
    /* JADX WARN: Removed duplicated region for block: B:65:0x0144  */
    /* JADX WARN: Removed duplicated region for block: B:76:0x015e  */
    /* JADX WARN: Removed duplicated region for block: B:78:0x0164  */
    /* JADX WARN: Removed duplicated region for block: B:81:0x0170  */
    /* JADX WARN: Removed duplicated region for block: B:88:0x0194  */
    @Override // android.os.AsyncTask
    @androidx.annotation.NonNull
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.q.a.j.b.a doInBackground(java.lang.Void[] r14) {
        /*
            Method dump skipped, instructions count: 440
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.q.a.j.b.doInBackground(java.lang.Object[]):java.lang.Object");
    }

    @Override // android.os.AsyncTask
    public void onPostExecute(@NonNull a aVar) {
        a aVar2 = aVar;
        Exception exc = aVar2.c;
        if (exc == null) {
            b.q.a.h.b bVar = this.f;
            Bitmap bitmap = aVar2.a;
            b.q.a.i.b bVar2 = aVar2.f1979b;
            String path = this.f1978b.getPath();
            Uri uri = this.c;
            String path2 = uri == null ? null : uri.getPath();
            c cVar = ((b.q.a.l.b) bVar).a;
            cVar.v = path;
            cVar.w = path2;
            cVar.f1987x = bVar2;
            cVar.f1986s = true;
            cVar.setImageBitmap(bitmap);
            return;
        }
        b.q.a.l.b bVar3 = (b.q.a.l.b) this.f;
        Objects.requireNonNull(bVar3);
        Log.e("TransformImageView", "onFailure: setImageUri", exc);
        c.a aVar3 = bVar3.a.p;
        if (aVar3 != null) {
            UCropActivity.a aVar4 = (UCropActivity.a) aVar3;
            UCropActivity.this.b(exc);
            UCropActivity.this.finish();
        }
    }

    /* compiled from: BitmapLoadTask.java */
    /* loaded from: classes3.dex */
    public static class a {
        public Bitmap a;

        /* renamed from: b  reason: collision with root package name */
        public b.q.a.i.b f1979b;
        public Exception c;

        public a(@NonNull Bitmap bitmap, @NonNull b.q.a.i.b bVar) {
            this.a = bitmap;
            this.f1979b = bVar;
        }

        public a(@NonNull Exception exc) {
            this.c = exc;
        }
    }
}
