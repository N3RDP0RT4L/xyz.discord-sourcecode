package b.q.a.j;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.RectF;
import android.net.Uri;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.q.a.g;
import b.q.a.i.c;
import com.yalantis.ucrop.UCropActivity;
import java.io.File;
import java.lang.ref.WeakReference;
/* compiled from: BitmapCropTask.java */
/* loaded from: classes3.dex */
public class a extends AsyncTask<Void, Void, Throwable> {
    public final WeakReference<Context> a;

    /* renamed from: b  reason: collision with root package name */
    public Bitmap f1977b;
    public final RectF c;
    public final RectF d;
    public float e;
    public float f;
    public final int g;
    public final int h;
    public final Bitmap.CompressFormat i;
    public final int j;
    public final String k;
    public final String l;
    public final b.q.a.h.a m;
    public int n;
    public int o;
    public int p;
    public int q;

    public a(@NonNull Context context, @Nullable Bitmap bitmap, @NonNull c cVar, @NonNull b.q.a.i.a aVar, @Nullable b.q.a.h.a aVar2) {
        this.a = new WeakReference<>(context);
        this.f1977b = bitmap;
        this.c = cVar.a;
        this.d = cVar.f1976b;
        this.e = cVar.c;
        this.f = cVar.d;
        this.g = aVar.a;
        this.h = aVar.f1974b;
        this.i = aVar.c;
        this.j = aVar.d;
        this.k = aVar.e;
        this.l = aVar.f;
        this.m = aVar2;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:115:0x0268 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:119:0x0261 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:38:0x0162  */
    /* JADX WARN: Removed duplicated region for block: B:65:0x01e3  */
    /* JADX WARN: Removed duplicated region for block: B:84:0x026c  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final boolean a() throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 699
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b.q.a.j.a.a():boolean");
    }

    @Override // android.os.AsyncTask
    @Nullable
    public Throwable doInBackground(Void[] voidArr) {
        Bitmap bitmap = this.f1977b;
        if (bitmap == null) {
            return new NullPointerException("ViewBitmap is null");
        }
        if (bitmap.isRecycled()) {
            return new NullPointerException("ViewBitmap is recycled");
        }
        if (this.d.isEmpty()) {
            return new NullPointerException("CurrentImageRect is empty");
        }
        try {
            a();
            this.f1977b = null;
            return null;
        } catch (Throwable th) {
            return th;
        }
    }

    @Override // android.os.AsyncTask
    public void onPostExecute(@Nullable Throwable th) {
        Throwable th2 = th;
        b.q.a.h.a aVar = this.m;
        if (aVar == null) {
            return;
        }
        if (th2 == null) {
            Uri fromFile = Uri.fromFile(new File(this.l));
            b.q.a.h.a aVar2 = this.m;
            int i = this.p;
            int i2 = this.q;
            int i3 = this.n;
            int i4 = this.o;
            g gVar = (g) aVar2;
            UCropActivity uCropActivity = gVar.a;
            uCropActivity.setResult(-1, new Intent().putExtra("com.yalantis.ucrop.OutputUri", fromFile).putExtra("com.yalantis.ucrop.CropAspectRatio", uCropActivity.w.getTargetAspectRatio()).putExtra("com.yalantis.ucrop.ImageWidth", i3).putExtra("com.yalantis.ucrop.ImageHeight", i4).putExtra("com.yalantis.ucrop.OffsetX", i).putExtra("com.yalantis.ucrop.OffsetY", i2));
            gVar.a.finish();
            return;
        }
        g gVar2 = (g) aVar;
        gVar2.a.b(th2);
        gVar2.a.finish();
    }
}
