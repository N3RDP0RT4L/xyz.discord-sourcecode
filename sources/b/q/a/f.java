package b.q.a;

import com.yalantis.ucrop.UCropActivity;
import com.yalantis.ucrop.view.GestureCropImageView;
import com.yalantis.ucrop.view.widget.HorizontalProgressWheelView;
/* compiled from: UCropActivity.java */
/* loaded from: classes3.dex */
public class f implements HorizontalProgressWheelView.a {
    public final /* synthetic */ UCropActivity a;

    public f(UCropActivity uCropActivity) {
        this.a = uCropActivity;
    }

    @Override // com.yalantis.ucrop.view.widget.HorizontalProgressWheelView.a
    public void a() {
        this.a.w.setImageToWrapCropBounds(true);
    }

    @Override // com.yalantis.ucrop.view.widget.HorizontalProgressWheelView.a
    public void b(float f, float f2) {
        if (f > 0.0f) {
            GestureCropImageView gestureCropImageView = this.a.w;
            gestureCropImageView.l((((this.a.w.getMaxScale() - this.a.w.getMinScale()) / 15000.0f) * f) + gestureCropImageView.getCurrentScale(), gestureCropImageView.f1983y.centerX(), gestureCropImageView.f1983y.centerY());
            return;
        }
        GestureCropImageView gestureCropImageView2 = this.a.w;
        float maxScale = (((this.a.w.getMaxScale() - this.a.w.getMinScale()) / 15000.0f) * f) + gestureCropImageView2.getCurrentScale();
        float centerX = gestureCropImageView2.f1983y.centerX();
        float centerY = gestureCropImageView2.f1983y.centerY();
        if (maxScale >= gestureCropImageView2.getMinScale()) {
            gestureCropImageView2.f(maxScale / gestureCropImageView2.getCurrentScale(), centerX, centerY);
        }
    }

    @Override // com.yalantis.ucrop.view.widget.HorizontalProgressWheelView.a
    public void c() {
        this.a.w.i();
    }
}
