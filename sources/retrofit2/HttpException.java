package retrofit2;

import java.util.Objects;
import okhttp3.Response;
/* loaded from: classes3.dex */
public class HttpException extends RuntimeException {
    private final int code;
    public final transient Response<?> j;
    private final String message;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public HttpException(Response<?> response) {
        super("HTTP " + response.a.m + " " + response.a.l);
        Objects.requireNonNull(response, "response == null");
        Response response2 = response.a;
        this.code = response2.m;
        this.message = response2.l;
        this.j = response;
    }

    public int a() {
        return this.code;
    }
}
