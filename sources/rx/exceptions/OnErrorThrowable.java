package rx.exceptions;

import java.util.HashSet;
import java.util.Set;
/* loaded from: classes3.dex */
public final class OnErrorThrowable extends RuntimeException {
    private static final long serialVersionUID = -569558213262703934L;
    private final boolean hasValue;
    private final Object value;

    /* loaded from: classes3.dex */
    public static class OnNextValue extends RuntimeException {
        private static final long serialVersionUID = -3454462756050397899L;
        private final Object value;

        /* loaded from: classes3.dex */
        public static final class a {
            public static final Set<Class<?>> a;

            static {
                HashSet hashSet = new HashSet();
                hashSet.add(Boolean.class);
                hashSet.add(Character.class);
                hashSet.add(Byte.class);
                hashSet.add(Short.class);
                hashSet.add(Integer.class);
                hashSet.add(Long.class);
                hashSet.add(Float.class);
                hashSet.add(Double.class);
                a = hashSet;
            }
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public OnNextValue(java.lang.Object r4) {
            /*
                r3 = this;
                java.lang.String r0 = "OnError while emitting onNext value: "
                java.lang.StringBuilder r0 = b.d.b.a.a.R(r0)
                if (r4 != 0) goto Lb
                java.lang.String r1 = "null"
                goto L52
            Lb:
                java.util.Set<java.lang.Class<?>> r1 = rx.exceptions.OnErrorThrowable.OnNextValue.a.a
                java.lang.Class r2 = r4.getClass()
                boolean r1 = r1.contains(r2)
                if (r1 == 0) goto L1c
                java.lang.String r1 = r4.toString()
                goto L52
            L1c:
                boolean r1 = r4 instanceof java.lang.String
                if (r1 == 0) goto L24
                r1 = r4
                java.lang.String r1 = (java.lang.String) r1
                goto L52
            L24:
                boolean r1 = r4 instanceof java.lang.Enum
                if (r1 == 0) goto L30
                r1 = r4
                java.lang.Enum r1 = (java.lang.Enum) r1
                java.lang.String r1 = r1.name()
                goto L52
            L30:
                j0.o.o r1 = j0.o.o.a
                j0.o.b r1 = r1.b()
                java.util.Objects.requireNonNull(r1)
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.Class r2 = r4.getClass()
                java.lang.String r2 = r2.getName()
                r1.append(r2)
                java.lang.String r2 = ".class"
                r1.append(r2)
                java.lang.String r1 = r1.toString()
            L52:
                r0.append(r1)
                java.lang.String r0 = r0.toString()
                r3.<init>(r0)
                boolean r0 = r4 instanceof java.io.Serializable
                if (r0 == 0) goto L61
                goto L6b
            L61:
                java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch: java.lang.Throwable -> L66
                goto L6b
            L66:
                r4 = move-exception
                java.lang.String r4 = r4.getMessage()
            L6b:
                r3.value = r4
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: rx.exceptions.OnErrorThrowable.OnNextValue.<init>(java.lang.Object):void");
        }

        public Object a() {
            return this.value;
        }
    }

    public static Throwable a(Throwable th, Object obj) {
        if (th == null) {
            th = new NullPointerException();
        }
        int i = 0;
        int i2 = 0;
        Throwable th2 = th;
        while (true) {
            if (th2.getCause() == null) {
                break;
            }
            i2++;
            if (i2 >= 25) {
                th2 = new RuntimeException("Stack too deep to get final cause");
                break;
            }
            th2 = th2.getCause();
        }
        if ((th2 instanceof OnNextValue) && ((OnNextValue) th2).a() == obj) {
            return th;
        }
        OnNextValue onNextValue = new OnNextValue(obj);
        HashSet hashSet = new HashSet();
        Throwable th3 = th;
        while (true) {
            if (th3.getCause() != null) {
                i++;
                if (i >= 25) {
                    break;
                }
                th3 = th3.getCause();
                if (!hashSet.contains(th3.getCause())) {
                    hashSet.add(th3.getCause());
                }
            }
            try {
                th3.initCause(onNextValue);
                break;
            } catch (Throwable unused) {
            }
        }
        return th;
    }
}
