package rx.subjects;

import b.i.a.f.e.o.f;
import j0.l.a.e;
import j0.q.c;
import java.util.ArrayList;
import rx.Observable;
import rx.functions.Action1;
/* loaded from: classes3.dex */
public final class BehaviorSubject<T> extends Subject<T, T> {
    public static final Object[] k = new Object[0];
    public final c<T> l;

    /* loaded from: classes3.dex */
    public static class a implements Action1<c.b<T>> {
        public final /* synthetic */ c j;

        public a(c cVar) {
            this.j = cVar;
        }

        /* JADX WARN: Removed duplicated region for block: B:42:0x005b  */
        @Override // rx.functions.Action1
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public void call(java.lang.Object r9) {
            /*
                r8 = this;
                j0.q.c$b r9 = (j0.q.c.b) r9
                j0.q.c r0 = r8.j
                java.lang.Object r0 = r0.latest
                monitor-enter(r9)
                boolean r1 = r9.k     // Catch: java.lang.Throwable -> L66
                if (r1 == 0) goto L64
                boolean r1 = r9.l     // Catch: java.lang.Throwable -> L66
                if (r1 == 0) goto L10
                goto L64
            L10:
                r1 = 0
                r9.k = r1     // Catch: java.lang.Throwable -> L66
                r2 = 1
                if (r0 == 0) goto L18
                r3 = 1
                goto L19
            L18:
                r3 = 0
            L19:
                r9.l = r3     // Catch: java.lang.Throwable -> L66
                monitor-exit(r9)     // Catch: java.lang.Throwable -> L66
                if (r0 == 0) goto L65
                r3 = 0
                r4 = r3
                r5 = 1
            L21:
                if (r4 == 0) goto L3b
                java.util.Iterator r4 = r4.iterator()     // Catch: java.lang.Throwable -> L39
            L27:
                boolean r6 = r4.hasNext()     // Catch: java.lang.Throwable -> L39
                if (r6 == 0) goto L3b
                java.lang.Object r6 = r4.next()     // Catch: java.lang.Throwable -> L39
                if (r6 == 0) goto L27
                rx.Subscriber<? super T> r7 = r9.j     // Catch: java.lang.Throwable -> L39
                j0.l.a.e.a(r7, r6)     // Catch: java.lang.Throwable -> L39
                goto L27
            L39:
                r0 = move-exception
                goto L58
            L3b:
                if (r5 == 0) goto L43
                rx.Subscriber<? super T> r4 = r9.j     // Catch: java.lang.Throwable -> L39
                j0.l.a.e.a(r4, r0)     // Catch: java.lang.Throwable -> L39
                r5 = 0
            L43:
                monitor-enter(r9)     // Catch: java.lang.Throwable -> L39
                java.util.List<java.lang.Object> r4 = r9.m     // Catch: java.lang.Throwable -> L50
                r9.m = r3     // Catch: java.lang.Throwable -> L50
                if (r4 != 0) goto L4e
                r9.l = r1     // Catch: java.lang.Throwable -> L50
                monitor-exit(r9)     // Catch: java.lang.Throwable -> L56
                goto L65
            L4e:
                monitor-exit(r9)     // Catch: java.lang.Throwable -> L50
                goto L21
            L50:
                r0 = move-exception
                r2 = 0
            L52:
                monitor-exit(r9)     // Catch: java.lang.Throwable -> L56
                throw r0     // Catch: java.lang.Throwable -> L54
            L54:
                r0 = move-exception
                goto L59
            L56:
                r0 = move-exception
                goto L52
            L58:
                r2 = 0
            L59:
                if (r2 != 0) goto L63
                monitor-enter(r9)
                r9.l = r1     // Catch: java.lang.Throwable -> L60
                monitor-exit(r9)     // Catch: java.lang.Throwable -> L60
                goto L63
            L60:
                r0 = move-exception
                monitor-exit(r9)     // Catch: java.lang.Throwable -> L60
                throw r0
            L63:
                throw r0
            L64:
                monitor-exit(r9)     // Catch: java.lang.Throwable -> L66
            L65:
                return
            L66:
                r0 = move-exception
                monitor-exit(r9)     // Catch: java.lang.Throwable -> L66
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: rx.subjects.BehaviorSubject.a.call(java.lang.Object):void");
        }
    }

    public BehaviorSubject(Observable.a<T> aVar, c<T> cVar) {
        super(aVar);
        this.l = cVar;
    }

    public static <T> BehaviorSubject<T> k0() {
        return m0(null, false);
    }

    public static <T> BehaviorSubject<T> l0(T t) {
        return m0(t, true);
    }

    public static <T> BehaviorSubject<T> m0(T t, boolean z2) {
        c cVar = new c();
        if (z2) {
            if (t == null) {
                t = (T) e.f3743b;
            }
            cVar.latest = t;
        }
        a aVar = new a(cVar);
        cVar.onAdded = aVar;
        cVar.onTerminated = aVar;
        return new BehaviorSubject<>(cVar, cVar);
    }

    public T n0() {
        Object obj = this.l.latest;
        if (obj != null && !(obj instanceof e.c) && !e.c(obj)) {
            return (T) e.b(obj);
        }
        return null;
    }

    @Override // j0.g
    public void onCompleted() {
        if (this.l.latest == null || this.l.active) {
            Object obj = e.a;
            for (c.b<T> bVar : this.l.b(obj)) {
                bVar.a(obj);
            }
        }
    }

    @Override // j0.g
    public void onError(Throwable th) {
        if (this.l.latest == null || this.l.active) {
            e.c cVar = new e.c(th);
            ArrayList arrayList = null;
            for (c.b<T> bVar : this.l.b(cVar)) {
                try {
                    bVar.a(cVar);
                } catch (Throwable th2) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(th2);
                }
            }
            f.n1(arrayList);
        }
    }

    @Override // j0.g
    public void onNext(T t) {
        if (this.l.latest == null || this.l.active) {
            if (t == null) {
                t = (T) e.f3743b;
            }
            c<T> cVar = this.l;
            cVar.latest = t;
            for (c.b bVar : cVar.get().e) {
                bVar.a(t);
            }
        }
    }
}
