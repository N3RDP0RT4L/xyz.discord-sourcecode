package rx.functions;
/* loaded from: classes3.dex */
public interface FuncN<R> {
    R call(Object... objArr);
}
