package okio;

import andhook.lib.HookHelper;
import b.i.a.f.e.o.f;
import d0.g0.c;
import d0.t.j;
import d0.z.d.m;
import g0.e;
import g0.z.b;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Arrays;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ByteString.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0005\n\u0002\b\u0004\n\u0002\u0010\u0012\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0002\b\u0014\b\u0016\u0018\u0000 \u00132\u00020\u00012\b\u0012\u0004\u0012\u00020\u00000\u0002:\u0001CB\u0011\b\u0000\u0012\u0006\u0010:\u001a\u00020\u001d¢\u0006\u0004\bA\u0010BJ\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\n\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u000f\u0010\r\u001a\u00020\fH\u0016¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\fH\u0016¢\u0006\u0004\b\u000f\u0010\u000eJ\u0017\u0010\u0011\u001a\u00020\u00002\u0006\u0010\u0010\u001a\u00020\fH\u0010¢\u0006\u0004\b\u0011\u0010\u0012J\u000f\u0010\u0013\u001a\u00020\fH\u0016¢\u0006\u0004\b\u0013\u0010\u000eJ\u000f\u0010\u0014\u001a\u00020\u0000H\u0016¢\u0006\u0004\b\u0014\u0010\u0015J\u0017\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u0017\u001a\u00020\u0016H\u0010¢\u0006\u0004\b\u0019\u0010\u001aJ\u000f\u0010\u001b\u001a\u00020\u0016H\u0010¢\u0006\u0004\b\u001b\u0010\u001cJ\u000f\u0010\u001e\u001a\u00020\u001dH\u0010¢\u0006\u0004\b\u001e\u0010\u001fJ\u0017\u0010!\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020 H\u0016¢\u0006\u0004\b!\u0010\"J'\u0010'\u001a\u00020\u00052\u0006\u0010$\u001a\u00020#2\u0006\u0010%\u001a\u00020\u00162\u0006\u0010&\u001a\u00020\u0016H\u0010¢\u0006\u0004\b'\u0010(J/\u0010,\u001a\u00020+2\u0006\u0010%\u001a\u00020\u00162\u0006\u0010)\u001a\u00020\u00002\u0006\u0010*\u001a\u00020\u00162\u0006\u0010&\u001a\u00020\u0016H\u0016¢\u0006\u0004\b,\u0010-J/\u0010.\u001a\u00020+2\u0006\u0010%\u001a\u00020\u00162\u0006\u0010)\u001a\u00020\u001d2\u0006\u0010*\u001a\u00020\u00162\u0006\u0010&\u001a\u00020\u0016H\u0016¢\u0006\u0004\b.\u0010/J\u001a\u00101\u001a\u00020+2\b\u0010)\u001a\u0004\u0018\u000100H\u0096\u0002¢\u0006\u0004\b1\u00102J\u000f\u00103\u001a\u00020\u0016H\u0016¢\u0006\u0004\b3\u0010\u001cJ\u000f\u00104\u001a\u00020\fH\u0016¢\u0006\u0004\b4\u0010\u000eR$\u00109\u001a\u0004\u0018\u00010\f8\u0000@\u0000X\u0080\u000e¢\u0006\u0012\n\u0004\b\u0019\u00105\u001a\u0004\b6\u0010\u000e\"\u0004\b7\u00108R\u001c\u0010:\u001a\u00020\u001d8\u0000@\u0000X\u0080\u0004¢\u0006\f\n\u0004\b:\u0010;\u001a\u0004\b<\u0010\u001fR\"\u00103\u001a\u00020\u00168\u0000@\u0000X\u0080\u000e¢\u0006\u0012\n\u0004\b\u001e\u0010=\u001a\u0004\b>\u0010\u001c\"\u0004\b?\u0010@¨\u0006D"}, d2 = {"Lokio/ByteString;", "Ljava/io/Serializable;", "", "Ljava/io/ObjectInputStream;", "in", "", "readObject", "(Ljava/io/ObjectInputStream;)V", "Ljava/io/ObjectOutputStream;", "out", "writeObject", "(Ljava/io/ObjectOutputStream;)V", "", "q", "()Ljava/lang/String;", "f", "algorithm", "g", "(Ljava/lang/String;)Lokio/ByteString;", "k", "p", "()Lokio/ByteString;", "", "pos", "", "m", "(I)B", "j", "()I", "", "l", "()[B", "Ljava/io/OutputStream;", "r", "(Ljava/io/OutputStream;)V", "Lg0/e;", "buffer", "offset", "byteCount", "s", "(Lg0/e;II)V", "other", "otherOffset", "", "n", "(ILokio/ByteString;II)Z", "o", "(I[BII)Z", "", "equals", "(Ljava/lang/Object;)Z", "hashCode", "toString", "Ljava/lang/String;", "getUtf8$okio", "setUtf8$okio", "(Ljava/lang/String;)V", "utf8", "data", "[B", "i", "I", "getHashCode$okio", "setHashCode$okio", "(I)V", HookHelper.constructorName, "([B)V", "a", "okio"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes3.dex */
public class ByteString implements Serializable, Comparable<ByteString> {
    private static final long serialVersionUID = 1;
    private final byte[] data;
    public transient int l;
    public transient String m;
    public static final a k = new a(null);
    public static final ByteString j = new ByteString(new byte[0]);

    /* compiled from: ByteString.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public static ByteString d(a aVar, byte[] bArr, int i, int i2, int i3) {
            if ((i3 & 1) != 0) {
                i = 0;
            }
            if ((i3 & 2) != 0) {
                i2 = bArr.length;
            }
            m.checkParameterIsNotNull(bArr, "$this$toByteString");
            f.B(bArr.length, i, i2);
            return new ByteString(j.copyOfRange(bArr, i, i2 + i));
        }

        public final ByteString a(String str) {
            m.checkParameterIsNotNull(str, "$this$decodeHex");
            if (str.length() % 2 == 0) {
                int length = str.length() / 2;
                byte[] bArr = new byte[length];
                for (int i = 0; i < length; i++) {
                    int i2 = i * 2;
                    bArr[i] = (byte) (b.a(str.charAt(i2 + 1)) + (b.a(str.charAt(i2)) << 4));
                }
                return new ByteString(bArr);
            }
            throw new IllegalArgumentException(b.d.b.a.a.v("Unexpected hex string: ", str).toString());
        }

        public final ByteString b(String str, Charset charset) {
            m.checkParameterIsNotNull(str, "$this$encode");
            m.checkParameterIsNotNull(charset, "charset");
            byte[] bytes = str.getBytes(charset);
            m.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
            return new ByteString(bytes);
        }

        public final ByteString c(String str) {
            m.checkParameterIsNotNull(str, "$this$encodeUtf8");
            m.checkParameterIsNotNull(str, "$this$asUtf8ToByteArray");
            byte[] bytes = str.getBytes(c.a);
            m.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
            ByteString byteString = new ByteString(bytes);
            byteString.m = str;
            return byteString;
        }
    }

    public ByteString(byte[] bArr) {
        m.checkParameterIsNotNull(bArr, "data");
        this.data = bArr;
    }

    public static final ByteString h(String str) {
        m.checkParameterIsNotNull(str, "$this$encodeUtf8");
        m.checkParameterIsNotNull(str, "$this$asUtf8ToByteArray");
        byte[] bytes = str.getBytes(c.a);
        m.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
        ByteString byteString = new ByteString(bytes);
        byteString.m = str;
        return byteString;
    }

    private final void readObject(ObjectInputStream objectInputStream) throws IOException {
        int readInt = objectInputStream.readInt();
        m.checkParameterIsNotNull(objectInputStream, "$this$readByteString");
        int i = 0;
        if (readInt >= 0) {
            byte[] bArr = new byte[readInt];
            while (i < readInt) {
                int read = objectInputStream.read(bArr, i, readInt - i);
                if (read != -1) {
                    i += read;
                } else {
                    throw new EOFException();
                }
            }
            ByteString byteString = new ByteString(bArr);
            Field declaredField = ByteString.class.getDeclaredField("data");
            m.checkExpressionValueIsNotNull(declaredField, "field");
            declaredField.setAccessible(true);
            declaredField.set(this, byteString.data);
            return;
        }
        throw new IllegalArgumentException(b.d.b.a.a.p("byteCount < 0: ", readInt).toString());
    }

    private final void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeInt(this.data.length);
        objectOutputStream.write(this.data);
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0030 A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:11:0x0032 A[ORIG_RETURN, RETURN] */
    @Override // java.lang.Comparable
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public int compareTo(okio.ByteString r8) {
        /*
            r7 = this;
            okio.ByteString r8 = (okio.ByteString) r8
            java.lang.String r0 = "other"
            d0.z.d.m.checkParameterIsNotNull(r8, r0)
            int r0 = r7.j()
            int r1 = r8.j()
            int r2 = java.lang.Math.min(r0, r1)
            r3 = 0
            r4 = 0
        L15:
            if (r4 >= r2) goto L2b
            byte r5 = r7.m(r4)
            r5 = r5 & 255(0xff, float:3.57E-43)
            byte r6 = r8.m(r4)
            r6 = r6 & 255(0xff, float:3.57E-43)
            if (r5 != r6) goto L28
            int r4 = r4 + 1
            goto L15
        L28:
            if (r5 >= r6) goto L32
            goto L30
        L2b:
            if (r0 != r1) goto L2e
            goto L33
        L2e:
            if (r0 >= r1) goto L32
        L30:
            r3 = -1
            goto L33
        L32:
            r3 = 1
        L33:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: okio.ByteString.compareTo(java.lang.Object):int");
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ByteString) {
            ByteString byteString = (ByteString) obj;
            int j2 = byteString.j();
            byte[] bArr = this.data;
            if (j2 == bArr.length && byteString.o(0, bArr, 0, bArr.length)) {
                return true;
            }
        }
        return false;
    }

    public String f() {
        byte[] bArr = this.data;
        byte[] bArr2 = g0.a.a;
        byte[] bArr3 = g0.a.a;
        m.checkParameterIsNotNull(bArr, "$this$encodeBase64");
        m.checkParameterIsNotNull(bArr3, "map");
        byte[] bArr4 = new byte[((bArr.length + 2) / 3) * 4];
        int length = bArr.length - (bArr.length % 3);
        int i = 0;
        int i2 = 0;
        while (i < length) {
            int i3 = i + 1;
            byte b2 = bArr[i];
            int i4 = i3 + 1;
            byte b3 = bArr[i3];
            i = i4 + 1;
            byte b4 = bArr[i4];
            int i5 = i2 + 1;
            bArr4[i2] = bArr3[(b2 & 255) >> 2];
            int i6 = i5 + 1;
            bArr4[i5] = bArr3[((b2 & 3) << 4) | ((b3 & 255) >> 4)];
            int i7 = i6 + 1;
            bArr4[i6] = bArr3[((b3 & 15) << 2) | ((b4 & 255) >> 6)];
            i2 = i7 + 1;
            bArr4[i7] = bArr3[b4 & 63];
        }
        int length2 = bArr.length - length;
        if (length2 == 1) {
            byte b5 = bArr[i];
            int i8 = i2 + 1;
            bArr4[i2] = bArr3[(b5 & 255) >> 2];
            int i9 = i8 + 1;
            bArr4[i8] = bArr3[(b5 & 3) << 4];
            byte b6 = (byte) 61;
            bArr4[i9] = b6;
            bArr4[i9 + 1] = b6;
        } else if (length2 == 2) {
            int i10 = i + 1;
            byte b7 = bArr[i];
            byte b8 = bArr[i10];
            int i11 = i2 + 1;
            bArr4[i2] = bArr3[(b7 & 255) >> 2];
            int i12 = i11 + 1;
            bArr4[i11] = bArr3[((b7 & 3) << 4) | ((b8 & 255) >> 4)];
            bArr4[i12] = bArr3[(b8 & 15) << 2];
            bArr4[i12 + 1] = (byte) 61;
        }
        m.checkParameterIsNotNull(bArr4, "$this$toUtf8String");
        return new String(bArr4, c.a);
    }

    public ByteString g(String str) {
        m.checkParameterIsNotNull(str, "algorithm");
        byte[] digest = MessageDigest.getInstance(str).digest(this.data);
        m.checkExpressionValueIsNotNull(digest, "MessageDigest.getInstance(algorithm).digest(data)");
        return new ByteString(digest);
    }

    public int hashCode() {
        int i = this.l;
        if (i != 0) {
            return i;
        }
        int hashCode = Arrays.hashCode(this.data);
        this.l = hashCode;
        return hashCode;
    }

    public final byte[] i() {
        return this.data;
    }

    public int j() {
        return this.data.length;
    }

    public String k() {
        byte[] bArr = this.data;
        char[] cArr = new char[bArr.length * 2];
        int i = 0;
        for (byte b2 : bArr) {
            int i2 = i + 1;
            char[] cArr2 = b.a;
            cArr[i] = cArr2[(b2 >> 4) & 15];
            i = i2 + 1;
            cArr[i2] = cArr2[b2 & 15];
        }
        return new String(cArr);
    }

    public byte[] l() {
        return this.data;
    }

    public byte m(int i) {
        return this.data[i];
    }

    public boolean n(int i, ByteString byteString, int i2, int i3) {
        m.checkParameterIsNotNull(byteString, "other");
        return byteString.o(i2, this.data, i, i3);
    }

    public boolean o(int i, byte[] bArr, int i2, int i3) {
        m.checkParameterIsNotNull(bArr, "other");
        if (i >= 0) {
            byte[] bArr2 = this.data;
            if (i <= bArr2.length - i3 && i2 >= 0 && i2 <= bArr.length - i3 && f.h(bArr2, i, bArr, i2, i3)) {
                return true;
            }
        }
        return false;
    }

    public ByteString p() {
        byte b2;
        int i = 0;
        while (true) {
            byte[] bArr = this.data;
            if (i >= bArr.length) {
                return this;
            }
            byte b3 = bArr[i];
            byte b4 = (byte) 65;
            if (b3 < b4 || b3 > (b2 = (byte) 90)) {
                i++;
            } else {
                byte[] copyOf = Arrays.copyOf(bArr, bArr.length);
                m.checkExpressionValueIsNotNull(copyOf, "java.util.Arrays.copyOf(this, size)");
                copyOf[i] = (byte) (b3 + 32);
                for (int i2 = i + 1; i2 < copyOf.length; i2++) {
                    byte b5 = copyOf[i2];
                    if (b5 >= b4 && b5 <= b2) {
                        copyOf[i2] = (byte) (b5 + 32);
                    }
                }
                return new ByteString(copyOf);
            }
        }
    }

    public String q() {
        String str = this.m;
        if (str != null) {
            return str;
        }
        byte[] l = l();
        m.checkParameterIsNotNull(l, "$this$toUtf8String");
        String str2 = new String(l, c.a);
        this.m = str2;
        return str2;
    }

    public void r(OutputStream outputStream) throws IOException {
        m.checkParameterIsNotNull(outputStream, "out");
        outputStream.write(this.data);
    }

    public void s(e eVar, int i, int i2) {
        m.checkParameterIsNotNull(eVar, "buffer");
        m.checkParameterIsNotNull(this, "$this$commonWrite");
        m.checkParameterIsNotNull(eVar, "buffer");
        eVar.S(this.data, i, i2);
    }

    /* JADX WARN: Code restructure failed: missing block: B:100:0x0117, code lost:
        if (r4 == 64) goto L181;
     */
    /* JADX WARN: Code restructure failed: missing block: B:103:0x0120, code lost:
        if (r4 == 64) goto L181;
     */
    /* JADX WARN: Code restructure failed: missing block: B:129:0x015e, code lost:
        if (r4 == 64) goto L181;
     */
    /* JADX WARN: Code restructure failed: missing block: B:135:0x0171, code lost:
        if (r4 == 64) goto L181;
     */
    /* JADX WARN: Code restructure failed: missing block: B:141:0x0182, code lost:
        if (r4 == 64) goto L181;
     */
    /* JADX WARN: Code restructure failed: missing block: B:147:0x0191, code lost:
        if (r4 == 64) goto L181;
     */
    /* JADX WARN: Code restructure failed: missing block: B:150:0x01a7, code lost:
        if (r4 == 64) goto L181;
     */
    /* JADX WARN: Code restructure failed: missing block: B:153:0x01af, code lost:
        if (r4 == 64) goto L181;
     */
    /* JADX WARN: Code restructure failed: missing block: B:156:0x01b6, code lost:
        if (r4 == 64) goto L181;
     */
    /* JADX WARN: Code restructure failed: missing block: B:179:0x01eb, code lost:
        if (r4 == 64) goto L181;
     */
    /* JADX WARN: Code restructure failed: missing block: B:180:0x01ee, code lost:
        r5 = -1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:53:0x0089, code lost:
        if (r4 == 64) goto L181;
     */
    /* JADX WARN: Code restructure failed: missing block: B:59:0x009a, code lost:
        if (r4 == 64) goto L181;
     */
    /* JADX WARN: Code restructure failed: missing block: B:62:0x00a5, code lost:
        if (r4 == 64) goto L181;
     */
    /* JADX WARN: Code restructure failed: missing block: B:85:0x00e3, code lost:
        if (r4 == 64) goto L181;
     */
    /* JADX WARN: Code restructure failed: missing block: B:91:0x00f6, code lost:
        if (r4 == 64) goto L181;
     */
    /* JADX WARN: Code restructure failed: missing block: B:97:0x0105, code lost:
        if (r4 == 64) goto L181;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public java.lang.String toString() {
        /*
            Method dump skipped, instructions count: 718
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: okio.ByteString.toString():java.lang.String");
    }
}
