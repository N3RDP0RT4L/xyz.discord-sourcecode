package kotlin.reflect.jvm.internal.impl.utils;

import andhook.lib.HookHelper;
import d0.e0.p.d.m0.p.c;
/* loaded from: classes3.dex */
public class WrappedValues {
    public static final Object a = new a();

    /* loaded from: classes3.dex */
    public static class WrappedProcessCanceledException extends RuntimeException {
    }

    /* loaded from: classes3.dex */
    public static class a {
        public String toString() {
            return "NULL_VALUE";
        }
    }

    /* loaded from: classes3.dex */
    public static final class b {
        public final Throwable a;

        public b(Throwable th, a aVar) {
            if (th != null) {
                this.a = th;
            } else {
                a(0);
                throw null;
            }
        }

        public static /* synthetic */ void a(int i) {
            String str = i != 1 ? "Argument for @NotNull parameter '%s' of %s.%s must not be null" : "@NotNull method %s.%s must not return null";
            Object[] objArr = new Object[i != 1 ? 3 : 2];
            if (i != 1) {
                objArr[0] = "throwable";
            } else {
                objArr[0] = "kotlin/reflect/jvm/internal/impl/utils/WrappedValues$ThrowableWrapper";
            }
            if (i != 1) {
                objArr[1] = "kotlin/reflect/jvm/internal/impl/utils/WrappedValues$ThrowableWrapper";
            } else {
                objArr[1] = "getThrowable";
            }
            if (i != 1) {
                objArr[2] = HookHelper.constructorName;
            }
            String format = String.format(str, objArr);
            if (i == 1) {
                throw new IllegalStateException(format);
            }
        }

        public Throwable getThrowable() {
            Throwable th = this.a;
            if (th != null) {
                return th;
            }
            a(1);
            throw null;
        }

        public String toString() {
            return this.a.toString();
        }
    }

    public static /* synthetic */ void a(int i) {
        String str = (i == 1 || i == 2) ? "@NotNull method %s.%s must not return null" : "Argument for @NotNull parameter '%s' of %s.%s must not be null";
        Object[] objArr = new Object[(i == 1 || i == 2) ? 2 : 3];
        if (i == 1 || i == 2) {
            objArr[0] = "kotlin/reflect/jvm/internal/impl/utils/WrappedValues";
        } else if (i != 3) {
            objArr[0] = "value";
        } else {
            objArr[0] = "throwable";
        }
        if (i == 1 || i == 2) {
            objArr[1] = "escapeNull";
        } else {
            objArr[1] = "kotlin/reflect/jvm/internal/impl/utils/WrappedValues";
        }
        if (!(i == 1 || i == 2)) {
            if (i == 3) {
                objArr[2] = "escapeThrowable";
            } else if (i != 4) {
                objArr[2] = "unescapeNull";
            } else {
                objArr[2] = "unescapeExceptionOrNull";
            }
        }
        String format = String.format(str, objArr);
        if (i == 1 || i == 2) {
            throw new IllegalStateException(format);
        }
    }

    public static <V> Object escapeNull(V v) {
        return v == null ? a : v;
    }

    public static Object escapeThrowable(Throwable th) {
        if (th != null) {
            return new b(th, null);
        }
        a(3);
        throw null;
    }

    public static <V> V unescapeExceptionOrNull(Object obj) {
        if (obj != null) {
            return (V) unescapeNull(unescapeThrowable(obj));
        }
        a(4);
        throw null;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static <V> V unescapeNull(Object obj) {
        if (obj == 0) {
            a(0);
            throw null;
        } else if (obj == a) {
            return null;
        } else {
            return obj;
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static <V> V unescapeThrowable(Object obj) {
        if (!(obj instanceof b)) {
            return obj;
        }
        throw c.rethrow(((b) obj).getThrowable());
    }
}
