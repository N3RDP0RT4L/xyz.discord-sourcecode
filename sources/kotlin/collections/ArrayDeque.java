package kotlin.collections;

import andhook.lib.HookHelper;
import androidx.exifinterface.media.ExifInterface;
import com.esotericsoftware.kryo.io.Util;
import d0.d0.f;
import d0.t.c;
import d0.t.e;
import d0.t.h;
import d0.t.j;
import d0.t.k;
import d0.t.n;
import d0.z.d.m;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ArrayDeque.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0010\u001e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u001f\n\u0002\u0010\u0011\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0011\b\u0007\u0018\u0000 D*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u00028\u00000\u0002:\u0001EB\t\b\u0016¢\u0006\u0004\bB\u0010/B\u0017\b\u0016\u0012\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00028\u00000\r¢\u0006\u0004\bB\u0010CJ\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\t\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\u000b\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u000b\u0010\nJ%\u0010\u000f\u001a\u00020\u00052\u0006\u0010\f\u001a\u00020\u00032\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00028\u00000\rH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u000f\u0010\u0012\u001a\u00020\u0011H\u0016¢\u0006\u0004\b\u0012\u0010\u0013J\u0015\u0010\u0015\u001a\u00020\u00052\u0006\u0010\u0014\u001a\u00028\u0000¢\u0006\u0004\b\u0015\u0010\u0016J\u0015\u0010\u0017\u001a\u00020\u00052\u0006\u0010\u0014\u001a\u00028\u0000¢\u0006\u0004\b\u0017\u0010\u0016J\r\u0010\u0018\u001a\u00028\u0000¢\u0006\u0004\b\u0018\u0010\u0019J\u000f\u0010\u001a\u001a\u0004\u0018\u00018\u0000¢\u0006\u0004\b\u001a\u0010\u0019J\r\u0010\u001b\u001a\u00028\u0000¢\u0006\u0004\b\u001b\u0010\u0019J\u0017\u0010\u001c\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00028\u0000H\u0016¢\u0006\u0004\b\u001c\u0010\u001dJ\u001f\u0010\u001c\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00028\u0000H\u0016¢\u0006\u0004\b\u001c\u0010\u001eJ\u001d\u0010\u001f\u001a\u00020\u00112\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00028\u00000\rH\u0016¢\u0006\u0004\b\u001f\u0010 J%\u0010\u001f\u001a\u00020\u00112\u0006\u0010\b\u001a\u00020\u00032\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00028\u00000\rH\u0016¢\u0006\u0004\b\u001f\u0010!J\u0018\u0010\"\u001a\u00028\u00002\u0006\u0010\b\u001a\u00020\u0003H\u0096\u0002¢\u0006\u0004\b\"\u0010#J \u0010$\u001a\u00028\u00002\u0006\u0010\b\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00028\u0000H\u0096\u0002¢\u0006\u0004\b$\u0010%J\u0018\u0010&\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00028\u0000H\u0096\u0002¢\u0006\u0004\b&\u0010\u001dJ\u0017\u0010'\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00028\u0000H\u0016¢\u0006\u0004\b'\u0010(J\u0017\u0010)\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00028\u0000H\u0016¢\u0006\u0004\b)\u0010(J\u0017\u0010*\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00028\u0000H\u0016¢\u0006\u0004\b*\u0010\u001dJ\u0017\u0010+\u001a\u00028\u00002\u0006\u0010\b\u001a\u00020\u0003H\u0016¢\u0006\u0004\b+\u0010#J\u001d\u0010,\u001a\u00020\u00112\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00028\u00000\rH\u0016¢\u0006\u0004\b,\u0010 J\u001d\u0010-\u001a\u00020\u00112\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00028\u00000\rH\u0016¢\u0006\u0004\b-\u0010 J\u000f\u0010.\u001a\u00020\u0005H\u0016¢\u0006\u0004\b.\u0010/J)\u00103\u001a\b\u0012\u0004\u0012\u00028\u000101\"\u0004\b\u0001\u001002\f\u00102\u001a\b\u0012\u0004\u0012\u00028\u000101H\u0016¢\u0006\u0004\b3\u00104J\u0017\u00103\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010501H\u0016¢\u0006\u0004\b3\u00106R\u0016\u00109\u001a\u00020\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b7\u00108R$\u0010>\u001a\u00020\u00032\u0006\u0010:\u001a\u00020\u00038\u0016@RX\u0096\u000e¢\u0006\f\n\u0004\b;\u00108\u001a\u0004\b<\u0010=R\u001e\u0010A\u001a\n\u0012\u0006\u0012\u0004\u0018\u000105018\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b?\u0010@¨\u0006F"}, d2 = {"Lkotlin/collections/ArrayDeque;", ExifInterface.LONGITUDE_EAST, "Ld0/t/e;", "", "minCapacity", "", "e", "(I)V", "index", "g", "(I)I", "d", "internalIndex", "", "elements", "c", "(ILjava/util/Collection;)V", "", "isEmpty", "()Z", "element", "addFirst", "(Ljava/lang/Object;)V", "addLast", "removeFirst", "()Ljava/lang/Object;", "removeFirstOrNull", "removeLast", "add", "(Ljava/lang/Object;)Z", "(ILjava/lang/Object;)V", "addAll", "(Ljava/util/Collection;)Z", "(ILjava/util/Collection;)Z", "get", "(I)Ljava/lang/Object;", "set", "(ILjava/lang/Object;)Ljava/lang/Object;", "contains", "indexOf", "(Ljava/lang/Object;)I", "lastIndexOf", "remove", "removeAt", "removeAll", "retainAll", "clear", "()V", ExifInterface.GPS_DIRECTION_TRUE, "", "array", "toArray", "([Ljava/lang/Object;)[Ljava/lang/Object;", "", "()[Ljava/lang/Object;", "l", "I", "head", "<set-?>", "n", "getSize", "()I", "size", "m", "[Ljava/lang/Object;", "elementData", HookHelper.constructorName, "(Ljava/util/Collection;)V", "k", "a", "kotlin-stdlib"}, k = 1, mv = {1, 5, 1})
/* loaded from: classes3.dex */
public final class ArrayDeque<E> extends e<E> {
    public int l;
    public Object[] m;
    public int n;
    public static final a k = new a(null);
    public static final Object[] j = new Object[0];

    /* compiled from: ArrayDeque.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final int newCapacity$kotlin_stdlib(int i, int i2) {
            int i3 = i + (i >> 1);
            if (i3 - i2 < 0) {
                i3 = i2;
            }
            if (i3 - Util.MAX_SAFE_ARRAY_SIZE <= 0) {
                return i3;
            }
            if (i2 > 2147483639) {
                return Integer.MAX_VALUE;
            }
            return Util.MAX_SAFE_ARRAY_SIZE;
        }
    }

    public ArrayDeque() {
        this.m = j;
    }

    public static final int access$negativeMod(ArrayDeque arrayDeque, int i) {
        Objects.requireNonNull(arrayDeque);
        return i < 0 ? i + arrayDeque.m.length : i;
    }

    public static final int access$positiveMod(ArrayDeque arrayDeque, int i) {
        Object[] objArr = arrayDeque.m;
        return i >= objArr.length ? i - objArr.length : i;
    }

    @Override // java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean add(E e) {
        addLast(e);
        return true;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean addAll(Collection<? extends E> collection) {
        m.checkNotNullParameter(collection, "elements");
        if (collection.isEmpty()) {
            return false;
        }
        e(collection.size() + size());
        c(access$positiveMod(this, this.l + size()), collection);
        return true;
    }

    public final void addFirst(E e) {
        e(size() + 1);
        int d = d(this.l);
        this.l = d;
        this.m[d] = e;
        this.n = size() + 1;
    }

    public final void addLast(E e) {
        e(size() + 1);
        this.m[access$positiveMod(this, this.l + size())] = e;
        this.n = size() + 1;
    }

    public final void c(int i, Collection<? extends E> collection) {
        Iterator<? extends E> it = collection.iterator();
        int length = this.m.length;
        while (i < length && it.hasNext()) {
            this.m[i] = it.next();
            i++;
        }
        int i2 = this.l;
        for (int i3 = 0; i3 < i2 && it.hasNext(); i3++) {
            this.m[i3] = it.next();
        }
        this.n = collection.size() + size();
    }

    @Override // java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.util.List
    public void clear() {
        int access$positiveMod = access$positiveMod(this, this.l + size());
        int i = this.l;
        if (i < access$positiveMod) {
            j.fill(this.m, (Object) null, i, access$positiveMod);
        } else if (!isEmpty()) {
            Object[] objArr = this.m;
            j.fill(objArr, (Object) null, this.l, objArr.length);
            j.fill(this.m, (Object) null, 0, access$positiveMod);
        }
        this.l = 0;
        this.n = 0;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean contains(Object obj) {
        return indexOf(obj) != -1;
    }

    public final int d(int i) {
        return i == 0 ? k.getLastIndex(this.m) : i - 1;
    }

    public final void e(int i) {
        if (i >= 0) {
            Object[] objArr = this.m;
            if (i > objArr.length) {
                if (objArr == j) {
                    this.m = new Object[f.coerceAtLeast(i, 10)];
                    return;
                }
                Object[] objArr2 = new Object[k.newCapacity$kotlin_stdlib(objArr.length, i)];
                Object[] objArr3 = this.m;
                j.copyInto(objArr3, objArr2, 0, this.l, objArr3.length);
                Object[] objArr4 = this.m;
                int length = objArr4.length;
                int i2 = this.l;
                j.copyInto(objArr4, objArr2, length - i2, 0, i2);
                this.l = 0;
                this.m = objArr2;
                return;
            }
            return;
        }
        throw new IllegalStateException("Deque is too big.");
    }

    public final int g(int i) {
        if (i == k.getLastIndex(this.m)) {
            return 0;
        }
        return i + 1;
    }

    @Override // java.util.AbstractList, java.util.List
    public E get(int i) {
        c.j.checkElementIndex$kotlin_stdlib(i, size());
        return (E) this.m[access$positiveMod(this, this.l + i)];
    }

    @Override // d0.t.e
    public int getSize() {
        return this.n;
    }

    @Override // java.util.AbstractList, java.util.List
    public int indexOf(Object obj) {
        int i;
        int access$positiveMod = access$positiveMod(this, this.l + size());
        int i2 = this.l;
        if (i2 < access$positiveMod) {
            while (i2 < access$positiveMod) {
                if (m.areEqual(obj, this.m[i2])) {
                    i = this.l;
                } else {
                    i2++;
                }
            }
            return -1;
        } else if (i2 < access$positiveMod) {
            return -1;
        } else {
            int length = this.m.length;
            while (true) {
                if (i2 >= length) {
                    for (int i3 = 0; i3 < access$positiveMod; i3++) {
                        if (m.areEqual(obj, this.m[i3])) {
                            i2 = i3 + this.m.length;
                            i = this.l;
                        }
                    }
                    return -1;
                } else if (m.areEqual(obj, this.m[i2])) {
                    i = this.l;
                    break;
                } else {
                    i2++;
                }
            }
        }
        return i2 - i;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override // java.util.AbstractList, java.util.List
    public int lastIndexOf(Object obj) {
        int lastIndex;
        int i;
        int access$positiveMod = access$positiveMod(this, this.l + size());
        int i2 = this.l;
        if (i2 < access$positiveMod) {
            lastIndex = access$positiveMod - 1;
            if (lastIndex < i2) {
                return -1;
            }
            while (!m.areEqual(obj, this.m[lastIndex])) {
                if (lastIndex == i2) {
                    return -1;
                }
                lastIndex--;
            }
            i = this.l;
        } else if (i2 <= access$positiveMod) {
            return -1;
        } else {
            int i3 = access$positiveMod - 1;
            while (true) {
                if (i3 < 0) {
                    lastIndex = k.getLastIndex(this.m);
                    int i4 = this.l;
                    if (lastIndex < i4) {
                        return -1;
                    }
                    while (!m.areEqual(obj, this.m[lastIndex])) {
                        if (lastIndex == i4) {
                            return -1;
                        }
                        lastIndex--;
                    }
                    i = this.l;
                } else if (m.areEqual(obj, this.m[i3])) {
                    lastIndex = i3 + this.m.length;
                    i = this.l;
                    break;
                } else {
                    i3--;
                }
            }
        }
        return lastIndex - i;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean remove(Object obj) {
        int indexOf = indexOf(obj);
        if (indexOf == -1) {
            return false;
        }
        remove(indexOf);
        return true;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean removeAll(Collection<? extends Object> collection) {
        m.checkNotNullParameter(collection, "elements");
        boolean z2 = false;
        z2 = false;
        z2 = false;
        if (!isEmpty()) {
            if (!(this.m.length == 0)) {
                int access$positiveMod = access$positiveMod(this, this.l + size());
                int i = this.l;
                if (this.l < access$positiveMod) {
                    for (int i2 = this.l; i2 < access$positiveMod; i2++) {
                        Object obj = this.m[i2];
                        if (!collection.contains(obj)) {
                            i++;
                            this.m[i] = obj;
                        } else {
                            z2 = true;
                        }
                    }
                    j.fill(this.m, (Object) null, i, access$positiveMod);
                } else {
                    int length = this.m.length;
                    boolean z3 = false;
                    for (int i3 = this.l; i3 < length; i3++) {
                        Object obj2 = this.m[i3];
                        this.m[i3] = null;
                        if (!collection.contains(obj2)) {
                            i++;
                            this.m[i] = obj2;
                        } else {
                            z3 = true;
                        }
                    }
                    i = access$positiveMod(this, i);
                    for (int i4 = 0; i4 < access$positiveMod; i4++) {
                        Object obj3 = this.m[i4];
                        this.m[i4] = null;
                        if (!collection.contains(obj3)) {
                            this.m[i] = obj3;
                            i = g(i);
                        } else {
                            z3 = true;
                        }
                    }
                    z2 = z3;
                }
                if (z2) {
                    this.n = access$negativeMod(this, i - this.l);
                }
            }
        }
        return z2;
    }

    @Override // d0.t.e
    public E removeAt(int i) {
        c.j.checkElementIndex$kotlin_stdlib(i, size());
        if (i == n.getLastIndex(this)) {
            return removeLast();
        }
        if (i == 0) {
            return removeFirst();
        }
        int access$positiveMod = access$positiveMod(this, this.l + i);
        E e = (E) this.m[access$positiveMod];
        if (i < (size() >> 1)) {
            int i2 = this.l;
            if (access$positiveMod >= i2) {
                Object[] objArr = this.m;
                j.copyInto(objArr, objArr, i2 + 1, i2, access$positiveMod);
            } else {
                Object[] objArr2 = this.m;
                j.copyInto(objArr2, objArr2, 1, 0, access$positiveMod);
                Object[] objArr3 = this.m;
                objArr3[0] = objArr3[objArr3.length - 1];
                int i3 = this.l;
                j.copyInto(objArr3, objArr3, i3 + 1, i3, objArr3.length - 1);
            }
            Object[] objArr4 = this.m;
            int i4 = this.l;
            objArr4[i4] = null;
            this.l = g(i4);
        } else {
            int access$positiveMod2 = access$positiveMod(this, this.l + n.getLastIndex(this));
            if (access$positiveMod <= access$positiveMod2) {
                Object[] objArr5 = this.m;
                j.copyInto(objArr5, objArr5, access$positiveMod, access$positiveMod + 1, access$positiveMod2 + 1);
            } else {
                Object[] objArr6 = this.m;
                j.copyInto(objArr6, objArr6, access$positiveMod, access$positiveMod + 1, objArr6.length);
                Object[] objArr7 = this.m;
                objArr7[objArr7.length - 1] = objArr7[0];
                j.copyInto(objArr7, objArr7, 0, 1, access$positiveMod2 + 1);
            }
            this.m[access$positiveMod2] = null;
        }
        this.n = size() - 1;
        return e;
    }

    public final E removeFirst() {
        if (!isEmpty()) {
            E e = (E) this.m[this.l];
            Object[] objArr = this.m;
            int i = this.l;
            objArr[i] = null;
            this.l = g(i);
            this.n = size() - 1;
            return e;
        }
        throw new NoSuchElementException("ArrayDeque is empty.");
    }

    public final E removeFirstOrNull() {
        if (isEmpty()) {
            return null;
        }
        return removeFirst();
    }

    public final E removeLast() {
        if (!isEmpty()) {
            int access$positiveMod = access$positiveMod(this, this.l + n.getLastIndex(this));
            E e = (E) this.m[access$positiveMod];
            this.m[access$positiveMod] = null;
            this.n = size() - 1;
            return e;
        }
        throw new NoSuchElementException("ArrayDeque is empty.");
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public boolean retainAll(Collection<? extends Object> collection) {
        m.checkNotNullParameter(collection, "elements");
        boolean z2 = false;
        z2 = false;
        z2 = false;
        if (!isEmpty()) {
            if (!(this.m.length == 0)) {
                int access$positiveMod = access$positiveMod(this, this.l + size());
                int i = this.l;
                if (this.l < access$positiveMod) {
                    for (int i2 = this.l; i2 < access$positiveMod; i2++) {
                        Object obj = this.m[i2];
                        if (collection.contains(obj)) {
                            i++;
                            this.m[i] = obj;
                        } else {
                            z2 = true;
                        }
                    }
                    j.fill(this.m, (Object) null, i, access$positiveMod);
                } else {
                    int length = this.m.length;
                    boolean z3 = false;
                    for (int i3 = this.l; i3 < length; i3++) {
                        Object obj2 = this.m[i3];
                        this.m[i3] = null;
                        if (collection.contains(obj2)) {
                            i++;
                            this.m[i] = obj2;
                        } else {
                            z3 = true;
                        }
                    }
                    i = access$positiveMod(this, i);
                    for (int i4 = 0; i4 < access$positiveMod; i4++) {
                        Object obj3 = this.m[i4];
                        this.m[i4] = null;
                        if (collection.contains(obj3)) {
                            this.m[i] = obj3;
                            i = g(i);
                        } else {
                            z3 = true;
                        }
                    }
                    z2 = z3;
                }
                if (z2) {
                    this.n = access$negativeMod(this, i - this.l);
                }
            }
        }
        return z2;
    }

    @Override // java.util.AbstractList, java.util.List
    public E set(int i, E e) {
        c.j.checkElementIndex$kotlin_stdlib(i, size());
        int access$positiveMod = access$positiveMod(this, this.l + i);
        E e2 = (E) this.m[access$positiveMod];
        this.m[access$positiveMod] = e;
        return e2;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public <T> T[] toArray(T[] tArr) {
        m.checkNotNullParameter(tArr, "array");
        if (tArr.length < size()) {
            tArr = (T[]) h.arrayOfNulls(tArr, size());
        }
        Objects.requireNonNull(tArr, "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
        int access$positiveMod = access$positiveMod(this, this.l + size());
        int i = this.l;
        if (i < access$positiveMod) {
            j.copyInto$default(this.m, tArr, 0, i, access$positiveMod, 2, (Object) null);
        } else if (!isEmpty()) {
            Object[] objArr = this.m;
            j.copyInto(objArr, tArr, 0, this.l, objArr.length);
            Object[] objArr2 = this.m;
            j.copyInto(objArr2, tArr, objArr2.length - this.l, 0, access$positiveMod);
        }
        if (tArr.length > size()) {
            tArr[size()] = null;
        }
        return tArr;
    }

    public ArrayDeque(Collection<? extends E> collection) {
        m.checkNotNullParameter(collection, "elements");
        boolean z2 = false;
        Object[] array = collection.toArray(new Object[0]);
        Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
        this.m = array;
        this.n = array.length;
        if (array.length == 0 ? true : z2) {
            this.m = j;
        }
    }

    @Override // java.util.AbstractList, java.util.List
    public void add(int i, E e) {
        c.j.checkPositionIndex$kotlin_stdlib(i, size());
        if (i == size()) {
            addLast(e);
        } else if (i == 0) {
            addFirst(e);
        } else {
            e(size() + 1);
            int access$positiveMod = access$positiveMod(this, this.l + i);
            if (i < ((size() + 1) >> 1)) {
                int d = d(access$positiveMod);
                int d2 = d(this.l);
                int i2 = this.l;
                if (d >= i2) {
                    Object[] objArr = this.m;
                    objArr[d2] = objArr[i2];
                    j.copyInto(objArr, objArr, i2, i2 + 1, d + 1);
                } else {
                    Object[] objArr2 = this.m;
                    j.copyInto(objArr2, objArr2, i2 - 1, i2, objArr2.length);
                    Object[] objArr3 = this.m;
                    objArr3[objArr3.length - 1] = objArr3[0];
                    j.copyInto(objArr3, objArr3, 0, 1, d + 1);
                }
                this.m[d] = e;
                this.l = d2;
            } else {
                int access$positiveMod2 = access$positiveMod(this, this.l + size());
                if (access$positiveMod < access$positiveMod2) {
                    Object[] objArr4 = this.m;
                    j.copyInto(objArr4, objArr4, access$positiveMod + 1, access$positiveMod, access$positiveMod2);
                } else {
                    Object[] objArr5 = this.m;
                    j.copyInto(objArr5, objArr5, 1, 0, access$positiveMod2);
                    Object[] objArr6 = this.m;
                    objArr6[0] = objArr6[objArr6.length - 1];
                    j.copyInto(objArr6, objArr6, access$positiveMod + 1, access$positiveMod, objArr6.length - 1);
                }
                this.m[access$positiveMod] = e;
            }
            this.n = size() + 1;
        }
    }

    @Override // java.util.AbstractList, java.util.List
    public boolean addAll(int i, Collection<? extends E> collection) {
        m.checkNotNullParameter(collection, "elements");
        c.j.checkPositionIndex$kotlin_stdlib(i, size());
        if (collection.isEmpty()) {
            return false;
        }
        if (i == size()) {
            return addAll(collection);
        }
        e(collection.size() + size());
        int access$positiveMod = access$positiveMod(this, this.l + size());
        int access$positiveMod2 = access$positiveMod(this, this.l + i);
        int size = collection.size();
        if (i < ((size() + 1) >> 1)) {
            int i2 = this.l;
            int i3 = i2 - size;
            if (access$positiveMod2 < i2) {
                Object[] objArr = this.m;
                j.copyInto(objArr, objArr, i3, i2, objArr.length);
                if (size >= access$positiveMod2) {
                    Object[] objArr2 = this.m;
                    j.copyInto(objArr2, objArr2, objArr2.length - size, 0, access$positiveMod2);
                } else {
                    Object[] objArr3 = this.m;
                    j.copyInto(objArr3, objArr3, objArr3.length - size, 0, size);
                    Object[] objArr4 = this.m;
                    j.copyInto(objArr4, objArr4, 0, size, access$positiveMod2);
                }
            } else if (i3 >= 0) {
                Object[] objArr5 = this.m;
                j.copyInto(objArr5, objArr5, i3, i2, access$positiveMod2);
            } else {
                Object[] objArr6 = this.m;
                i3 += objArr6.length;
                int i4 = access$positiveMod2 - i2;
                int length = objArr6.length - i3;
                if (length >= i4) {
                    j.copyInto(objArr6, objArr6, i3, i2, access$positiveMod2);
                } else {
                    j.copyInto(objArr6, objArr6, i3, i2, i2 + length);
                    Object[] objArr7 = this.m;
                    j.copyInto(objArr7, objArr7, 0, this.l + length, access$positiveMod2);
                }
            }
            this.l = i3;
            int i5 = access$positiveMod2 - size;
            if (i5 < 0) {
                i5 += this.m.length;
            }
            c(i5, collection);
        } else {
            int i6 = access$positiveMod2 + size;
            if (access$positiveMod2 < access$positiveMod) {
                int i7 = size + access$positiveMod;
                Object[] objArr8 = this.m;
                if (i7 <= objArr8.length) {
                    j.copyInto(objArr8, objArr8, i6, access$positiveMod2, access$positiveMod);
                } else if (i6 >= objArr8.length) {
                    j.copyInto(objArr8, objArr8, i6 - objArr8.length, access$positiveMod2, access$positiveMod);
                } else {
                    int length2 = access$positiveMod - (i7 - objArr8.length);
                    j.copyInto(objArr8, objArr8, 0, length2, access$positiveMod);
                    Object[] objArr9 = this.m;
                    j.copyInto(objArr9, objArr9, i6, access$positiveMod2, length2);
                }
            } else {
                Object[] objArr10 = this.m;
                j.copyInto(objArr10, objArr10, size, 0, access$positiveMod);
                Object[] objArr11 = this.m;
                if (i6 >= objArr11.length) {
                    j.copyInto(objArr11, objArr11, i6 - objArr11.length, access$positiveMod2, objArr11.length);
                } else {
                    j.copyInto(objArr11, objArr11, 0, objArr11.length - size, objArr11.length);
                    Object[] objArr12 = this.m;
                    j.copyInto(objArr12, objArr12, i6, access$positiveMod2, objArr12.length - size);
                }
            }
            c(access$positiveMod2, collection);
        }
        return true;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public Object[] toArray() {
        return toArray(new Object[size()]);
    }
}
