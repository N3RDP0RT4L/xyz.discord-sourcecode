package kotlin.jvm.functions;

import d0.c;
import kotlin.Metadata;
/* compiled from: Functions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bf\u0018\u0000*\u0006\b\u0000\u0010\u0001 \u00012\b\u0012\u0004\u0012\u00028\u00000\u0002J\u0010\u0010\u0003\u001a\u00028\u0000H¦\u0002¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lkotlin/jvm/functions/Function0;", "R", "Ld0/c;", "invoke", "()Ljava/lang/Object;", "kotlin-stdlib"}, k = 1, mv = {1, 5, 1})
/* loaded from: classes3.dex */
public interface Function0<R> extends c<R> {
    R invoke();
}
