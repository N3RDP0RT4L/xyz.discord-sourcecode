package kotlin.ranges;

import andhook.lib.HookHelper;
import d0.d0.b;
import d0.x.c;
import java.util.Iterator;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: Progressions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u001c\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0013\b\u0016\u0018\u0000  2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001!B!\b\u0000\u0012\u0006\u0010\u001c\u001a\u00020\u0002\u0012\u0006\u0010\u001d\u001a\u00020\u0002\u0012\u0006\u0010\u0015\u001a\u00020\u0002¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010\u0004\u001a\u00020\u0003H\u0096\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u000f\u0010\u0007\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\u000b\u001a\u00020\u00062\b\u0010\n\u001a\u0004\u0018\u00010\tH\u0096\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u000f\u0010\r\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u0010\u001a\u00020\u000fH\u0016¢\u0006\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0015\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u000eR\u0019\u0010\u0018\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0013\u001a\u0004\b\u0017\u0010\u000eR\u0019\u0010\u001b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u0013\u001a\u0004\b\u001a\u0010\u000e¨\u0006\""}, d2 = {"Lkotlin/ranges/IntProgression;", "", "", "Ld0/t/c0;", "iterator", "()Ld0/t/c0;", "", "isEmpty", "()Z", "", "other", "equals", "(Ljava/lang/Object;)Z", "hashCode", "()I", "", "toString", "()Ljava/lang/String;", "m", "I", "getStep", "step", "k", "getFirst", "first", "l", "getLast", "last", "start", "endInclusive", HookHelper.constructorName, "(III)V", "j", "a", "kotlin-stdlib"}, k = 1, mv = {1, 5, 1})
/* loaded from: classes3.dex */
public class IntProgression implements Iterable<Integer>, d0.z.d.g0.a {
    public static final a j = new a(null);
    public final int k;
    public final int l;
    public final int m;

    /* compiled from: Progressions.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final IntProgression fromClosedRange(int i, int i2, int i3) {
            return new IntProgression(i, i2, i3);
        }
    }

    public IntProgression(int i, int i2, int i3) {
        if (i3 == 0) {
            throw new IllegalArgumentException("Step must be non-zero.");
        } else if (i3 != Integer.MIN_VALUE) {
            this.k = i;
            this.l = c.getProgressionLastElement(i, i2, i3);
            this.m = i3;
        } else {
            throw new IllegalArgumentException("Step must be greater than Int.MIN_VALUE to avoid overflow on negation.");
        }
    }

    public boolean equals(Object obj) {
        if (obj instanceof IntProgression) {
            if (!isEmpty() || !((IntProgression) obj).isEmpty()) {
                IntProgression intProgression = (IntProgression) obj;
                if (!(this.k == intProgression.k && this.l == intProgression.l && this.m == intProgression.m)) {
                }
            }
            return true;
        }
        return false;
    }

    public final int getFirst() {
        return this.k;
    }

    public final int getLast() {
        return this.l;
    }

    public final int getStep() {
        return this.m;
    }

    public int hashCode() {
        if (isEmpty()) {
            return -1;
        }
        return (((this.k * 31) + this.l) * 31) + this.m;
    }

    public boolean isEmpty() {
        if (this.m > 0) {
            if (this.k > this.l) {
                return true;
            }
        } else if (this.k < this.l) {
            return true;
        }
        return false;
    }

    public String toString() {
        int i;
        StringBuilder sb;
        if (this.m > 0) {
            sb = new StringBuilder();
            sb.append(this.k);
            sb.append("..");
            sb.append(this.l);
            sb.append(" step ");
            i = this.m;
        } else {
            sb = new StringBuilder();
            sb.append(this.k);
            sb.append(" downTo ");
            sb.append(this.l);
            sb.append(" step ");
            i = -this.m;
        }
        sb.append(i);
        return sb.toString();
    }

    @Override // java.lang.Iterable
    /* renamed from: iterator */
    public Iterator<Integer> iterator2() {
        return new b(this.k, this.l, this.m);
    }
}
