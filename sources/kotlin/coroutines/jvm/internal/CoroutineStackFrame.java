package kotlin.coroutines.jvm.internal;

import kotlin.Metadata;
/* compiled from: CoroutineStackFrame.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\n\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\bg\u0018\u00002\u00020\u0001¨\u0006\u0002"}, d2 = {"Lkotlin/coroutines/jvm/internal/CoroutineStackFrame;", "", "kotlin-stdlib"}, k = 1, mv = {1, 5, 1})
/* loaded from: classes3.dex */
public interface CoroutineStackFrame {
}
