package j0.l.c;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicReference;
/* compiled from: GenericScheduledExecutorService.java */
/* loaded from: classes3.dex */
public final class d implements k {

    /* renamed from: b  reason: collision with root package name */
    public static final ScheduledExecutorService f3761b;
    public static int d;
    public final AtomicReference<ScheduledExecutorService[]> e = new AtomicReference<>(a);
    public static final ScheduledExecutorService[] a = new ScheduledExecutorService[0];
    public static final d c = new d();

    static {
        ScheduledExecutorService newScheduledThreadPool = Executors.newScheduledThreadPool(0);
        f3761b = newScheduledThreadPool;
        newScheduledThreadPool.shutdown();
    }

    public d() {
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        availableProcessors = availableProcessors > 4 ? availableProcessors / 2 : availableProcessors;
        availableProcessors = availableProcessors > 8 ? 8 : availableProcessors;
        ScheduledExecutorService[] scheduledExecutorServiceArr = new ScheduledExecutorService[availableProcessors];
        int i = 0;
        for (int i2 = 0; i2 < availableProcessors; i2++) {
            scheduledExecutorServiceArr[i2] = Executors.newScheduledThreadPool(1, e.j);
        }
        if (this.e.compareAndSet(a, scheduledExecutorServiceArr)) {
            while (i < availableProcessors) {
                ScheduledExecutorService scheduledExecutorService = scheduledExecutorServiceArr[i];
                if (!g.g(scheduledExecutorService) && (scheduledExecutorService instanceof ScheduledThreadPoolExecutor)) {
                    g.e((ScheduledThreadPoolExecutor) scheduledExecutorService);
                }
                i++;
            }
            return;
        }
        while (i < availableProcessors) {
            scheduledExecutorServiceArr[i].shutdownNow();
            i++;
        }
    }

    @Override // j0.l.c.k
    public void shutdown() {
        ScheduledExecutorService[] scheduledExecutorServiceArr;
        ScheduledExecutorService[] scheduledExecutorServiceArr2;
        do {
            scheduledExecutorServiceArr = this.e.get();
            scheduledExecutorServiceArr2 = a;
            if (scheduledExecutorServiceArr == scheduledExecutorServiceArr2) {
                return;
            }
        } while (!this.e.compareAndSet(scheduledExecutorServiceArr, scheduledExecutorServiceArr2));
        for (ScheduledExecutorService scheduledExecutorService : scheduledExecutorServiceArr) {
            g.l.remove(scheduledExecutorService);
            scheduledExecutorService.shutdownNow();
        }
    }
}
