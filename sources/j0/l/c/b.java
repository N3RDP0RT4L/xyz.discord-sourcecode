package j0.l.c;

import j0.l.e.j;
import j0.o.l;
import java.util.Objects;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import rx.Scheduler;
import rx.Subscription;
import rx.functions.Action0;
import rx.internal.util.SubscriptionList;
import rx.subscriptions.CompositeSubscription;
/* compiled from: EventLoopsScheduler.java */
/* loaded from: classes3.dex */
public final class b extends Scheduler implements k {
    public static final int a;

    /* renamed from: b  reason: collision with root package name */
    public static final c f3759b;
    public static final C0410b c;
    public final ThreadFactory d;
    public final AtomicReference<C0410b> e;

    /* compiled from: EventLoopsScheduler.java */
    /* loaded from: classes3.dex */
    public static final class a extends Scheduler.Worker {
        public final SubscriptionList j;
        public final CompositeSubscription k;
        public final SubscriptionList l;
        public final c m;

        /* compiled from: EventLoopsScheduler.java */
        /* renamed from: j0.l.c.b$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public class C0408a implements Action0 {
            public final /* synthetic */ Action0 j;

            public C0408a(Action0 action0) {
                this.j = action0;
            }

            @Override // rx.functions.Action0
            public void call() {
                if (!a.this.l.k) {
                    this.j.call();
                }
            }
        }

        /* compiled from: EventLoopsScheduler.java */
        /* renamed from: j0.l.c.b$a$b  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public class C0409b implements Action0 {
            public final /* synthetic */ Action0 j;

            public C0409b(Action0 action0) {
                this.j = action0;
            }

            @Override // rx.functions.Action0
            public void call() {
                if (!a.this.l.k) {
                    this.j.call();
                }
            }
        }

        public a(c cVar) {
            SubscriptionList subscriptionList = new SubscriptionList();
            this.j = subscriptionList;
            CompositeSubscription compositeSubscription = new CompositeSubscription();
            this.k = compositeSubscription;
            this.l = new SubscriptionList(subscriptionList, compositeSubscription);
            this.m = cVar;
        }

        @Override // rx.Scheduler.Worker
        public Subscription a(Action0 action0) {
            if (this.l.k) {
                return j0.r.c.a;
            }
            c cVar = this.m;
            C0408a aVar = new C0408a(action0);
            SubscriptionList subscriptionList = this.j;
            Objects.requireNonNull(cVar);
            j jVar = new j(l.d(aVar), subscriptionList);
            subscriptionList.a(jVar);
            jVar.a(cVar.p.submit(jVar));
            return jVar;
        }

        @Override // rx.Scheduler.Worker
        public Subscription b(Action0 action0, long j, TimeUnit timeUnit) {
            Future<?> future;
            if (this.l.k) {
                return j0.r.c.a;
            }
            c cVar = this.m;
            C0409b bVar = new C0409b(action0);
            CompositeSubscription compositeSubscription = this.k;
            Objects.requireNonNull(cVar);
            j jVar = new j(l.d(bVar), compositeSubscription);
            compositeSubscription.a(jVar);
            if (j <= 0) {
                future = cVar.p.submit(jVar);
            } else {
                future = cVar.p.schedule(jVar, j, timeUnit);
            }
            jVar.a(future);
            return jVar;
        }

        @Override // rx.Subscription
        public boolean isUnsubscribed() {
            return this.l.k;
        }

        @Override // rx.Subscription
        public void unsubscribe() {
            this.l.unsubscribe();
        }
    }

    /* compiled from: EventLoopsScheduler.java */
    /* renamed from: j0.l.c.b$b  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0410b {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final c[] f3760b;
        public long c;

        public C0410b(ThreadFactory threadFactory, int i) {
            this.a = i;
            this.f3760b = new c[i];
            for (int i2 = 0; i2 < i; i2++) {
                this.f3760b[i2] = new c(threadFactory);
            }
        }

        public c a() {
            int i = this.a;
            if (i == 0) {
                return b.f3759b;
            }
            c[] cVarArr = this.f3760b;
            long j = this.c;
            this.c = 1 + j;
            return cVarArr[(int) (j % i)];
        }
    }

    /* compiled from: EventLoopsScheduler.java */
    /* loaded from: classes3.dex */
    public static final class c extends g {
        public c(ThreadFactory threadFactory) {
            super(threadFactory);
        }
    }

    static {
        int intValue = Integer.getInteger("rx.scheduler.max-computation-threads", 0).intValue();
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        if (intValue <= 0 || intValue > availableProcessors) {
            intValue = availableProcessors;
        }
        a = intValue;
        c cVar = new c(j.j);
        f3759b = cVar;
        cVar.unsubscribe();
        c = new C0410b(null, 0);
    }

    public b(ThreadFactory threadFactory) {
        this.d = threadFactory;
        C0410b bVar = c;
        AtomicReference<C0410b> atomicReference = new AtomicReference<>(bVar);
        this.e = atomicReference;
        C0410b bVar2 = new C0410b(threadFactory, a);
        if (!atomicReference.compareAndSet(bVar, bVar2)) {
            for (c cVar : bVar2.f3760b) {
                cVar.unsubscribe();
            }
        }
    }

    @Override // rx.Scheduler
    public Scheduler.Worker a() {
        return new a(this.e.get().a());
    }

    @Override // j0.l.c.k
    public void shutdown() {
        C0410b bVar;
        C0410b bVar2;
        do {
            bVar = this.e.get();
            bVar2 = c;
            if (bVar == bVar2) {
                return;
            }
        } while (!this.e.compareAndSet(bVar, bVar2));
        for (c cVar : bVar.f3760b) {
            cVar.unsubscribe();
        }
    }
}
