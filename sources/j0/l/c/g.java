package j0.l.c;

import b.i.a.f.e.o.f;
import j0.l.e.h;
import j0.l.e.j;
import j0.o.l;
import j0.r.c;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import rx.Scheduler;
import rx.Subscription;
import rx.functions.Action0;
/* compiled from: NewThreadWorker.java */
/* loaded from: classes3.dex */
public class g extends Scheduler.Worker implements Subscription {
    public static final boolean j;
    public static volatile Object n;
    public final ScheduledExecutorService p;
    public volatile boolean q;
    public static final Object o = new Object();
    public static final ConcurrentHashMap<ScheduledThreadPoolExecutor, ScheduledThreadPoolExecutor> l = new ConcurrentHashMap<>();
    public static final AtomicReference<ScheduledExecutorService> m = new AtomicReference<>();
    public static final int k = Integer.getInteger("rx.scheduler.jdk6.purge-frequency-millis", 1000).intValue();

    /* compiled from: NewThreadWorker.java */
    /* loaded from: classes3.dex */
    public static class a implements Runnable {
        @Override // java.lang.Runnable
        public void run() {
            try {
                Iterator<ScheduledThreadPoolExecutor> it = g.l.keySet().iterator();
                while (it.hasNext()) {
                    ScheduledThreadPoolExecutor next = it.next();
                    if (!next.isShutdown()) {
                        next.purge();
                    } else {
                        it.remove();
                    }
                }
            } catch (Throwable th) {
                f.o1(th);
                l.b(th);
            }
        }
    }

    static {
        boolean z2 = Boolean.getBoolean("rx.scheduler.jdk6.purge-force");
        int i = h.a;
        j = !z2 && (i == 0 || i >= 21);
    }

    public g(ThreadFactory threadFactory) {
        ScheduledExecutorService newScheduledThreadPool = Executors.newScheduledThreadPool(1, threadFactory);
        if (!g(newScheduledThreadPool) && (newScheduledThreadPool instanceof ScheduledThreadPoolExecutor)) {
            e((ScheduledThreadPoolExecutor) newScheduledThreadPool);
        }
        this.p = newScheduledThreadPool;
    }

    public static Method d(ScheduledExecutorService scheduledExecutorService) {
        Method[] methods;
        for (Method method : scheduledExecutorService.getClass().getMethods()) {
            if (method.getName().equals("setRemoveOnCancelPolicy")) {
                Class<?>[] parameterTypes = method.getParameterTypes();
                if (parameterTypes.length == 1 && parameterTypes[0] == Boolean.TYPE) {
                    return method;
                }
            }
        }
        return null;
    }

    public static void e(ScheduledThreadPoolExecutor scheduledThreadPoolExecutor) {
        while (true) {
            AtomicReference<ScheduledExecutorService> atomicReference = m;
            if (atomicReference.get() != null) {
                break;
            }
            ScheduledExecutorService newScheduledThreadPool = Executors.newScheduledThreadPool(1, new j("RxSchedulerPurge-"));
            if (atomicReference.compareAndSet(null, newScheduledThreadPool)) {
                a aVar = new a();
                int i = k;
                newScheduledThreadPool.scheduleAtFixedRate(aVar, i, i, TimeUnit.MILLISECONDS);
                break;
            }
            newScheduledThreadPool.shutdownNow();
        }
        l.putIfAbsent(scheduledThreadPoolExecutor, scheduledThreadPoolExecutor);
    }

    public static boolean g(ScheduledExecutorService scheduledExecutorService) {
        Method method;
        if (j) {
            if (scheduledExecutorService instanceof ScheduledThreadPoolExecutor) {
                Object obj = n;
                Object obj2 = o;
                if (obj == obj2) {
                    return false;
                }
                if (obj == null) {
                    method = d(scheduledExecutorService);
                    if (method != null) {
                        obj2 = method;
                    }
                    n = obj2;
                } else {
                    method = (Method) obj;
                }
            } else {
                method = d(scheduledExecutorService);
            }
            if (method != null) {
                try {
                    method.invoke(scheduledExecutorService, Boolean.TRUE);
                    return true;
                } catch (IllegalAccessException e) {
                    l.b(e);
                } catch (IllegalArgumentException e2) {
                    l.b(e2);
                } catch (InvocationTargetException e3) {
                    l.b(e3);
                }
            }
        }
        return false;
    }

    @Override // rx.Scheduler.Worker
    public Subscription a(Action0 action0) {
        if (this.q) {
            return c.a;
        }
        return f(action0, 0L, null);
    }

    @Override // rx.Scheduler.Worker
    public Subscription b(Action0 action0, long j2, TimeUnit timeUnit) {
        if (this.q) {
            return c.a;
        }
        return f(action0, j2, timeUnit);
    }

    public j f(Action0 action0, long j2, TimeUnit timeUnit) {
        Future<?> future;
        j jVar = new j(l.d(action0));
        if (j2 <= 0) {
            future = this.p.submit(jVar);
        } else {
            future = this.p.schedule(jVar, j2, timeUnit);
        }
        jVar.a(future);
        return jVar;
    }

    @Override // rx.Subscription
    public boolean isUnsubscribed() {
        return this.q;
    }

    @Override // rx.Subscription
    public void unsubscribe() {
        this.q = true;
        this.p.shutdownNow();
        l.remove(this.p);
    }
}
