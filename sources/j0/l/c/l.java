package j0.l.c;

import b.i.a.f.e.o.f;
import java.util.Objects;
import rx.Scheduler;
import rx.functions.Action0;
/* compiled from: SleepingAction.java */
/* loaded from: classes3.dex */
public class l implements Action0 {
    public final Action0 j;
    public final Scheduler.Worker k;
    public final long l;

    public l(Action0 action0, Scheduler.Worker worker, long j) {
        this.j = action0;
        this.k = worker;
        this.l = j;
    }

    @Override // rx.functions.Action0
    public void call() {
        if (!this.k.isUnsubscribed()) {
            long j = this.l;
            Objects.requireNonNull(this.k);
            long currentTimeMillis = j - System.currentTimeMillis();
            if (currentTimeMillis > 0) {
                try {
                    Thread.sleep(currentTimeMillis);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    f.V0(e);
                    throw null;
                }
            }
            if (!this.k.isUnsubscribed()) {
                this.j.call();
            }
        }
    }
}
