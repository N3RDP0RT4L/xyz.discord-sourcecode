package j0.l.c;

import j0.l.c.i;
import j0.l.d.a;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import rx.Scheduler;
import rx.functions.Action0;
/* compiled from: SchedulePeriodicHelper.java */
/* loaded from: classes3.dex */
public final class h implements Action0 {
    public long j;
    public long k;
    public long l;
    public final /* synthetic */ long m;
    public final /* synthetic */ long n;
    public final /* synthetic */ Action0 o;
    public final /* synthetic */ a p;
    public final /* synthetic */ i.a q;
    public final /* synthetic */ Scheduler.Worker r;

    /* renamed from: s  reason: collision with root package name */
    public final /* synthetic */ long f3762s;

    public h(long j, long j2, Action0 action0, a aVar, i.a aVar2, Scheduler.Worker worker, long j3) {
        this.m = j;
        this.n = j2;
        this.o = action0;
        this.p = aVar;
        this.q = aVar2;
        this.r = worker;
        this.f3762s = j3;
        this.k = j;
        this.l = j2;
    }

    @Override // rx.functions.Action0
    public void call() {
        long j;
        long j2;
        this.o.call();
        if (!this.p.isUnsubscribed()) {
            i.a aVar = this.q;
            if (aVar != null) {
                j = aVar.a();
            } else {
                TimeUnit timeUnit = TimeUnit.MILLISECONDS;
                Objects.requireNonNull(this.r);
                j = timeUnit.toNanos(System.currentTimeMillis());
            }
            long j3 = i.a;
            long j4 = this.k;
            if (j + j3 >= j4) {
                long j5 = this.f3762s;
                if (j < j4 + j5 + j3) {
                    long j6 = this.l;
                    long j7 = this.j + 1;
                    this.j = j7;
                    j2 = (j7 * j5) + j6;
                    this.k = j;
                    this.p.a(this.r.b(this, j2 - j, TimeUnit.NANOSECONDS));
                }
            }
            long j8 = this.f3762s;
            j2 = j + j8;
            long j9 = this.j + 1;
            this.j = j9;
            this.l = j2 - (j8 * j9);
            this.k = j;
            this.p.a(this.r.b(this, j2 - j, TimeUnit.NANOSECONDS));
        }
    }
}
