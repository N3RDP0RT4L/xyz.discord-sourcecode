package j0.l.c;

import j0.l.c.j;
import java.util.Iterator;
import java.util.Objects;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import rx.Scheduler;
import rx.Subscription;
import rx.functions.Action0;
import rx.subscriptions.CompositeSubscription;
/* compiled from: CachedThreadScheduler.java */
/* loaded from: classes3.dex */
public final class a extends Scheduler implements k {
    public static final c c;
    public static final C0405a d;
    public final ThreadFactory e;
    public final AtomicReference<C0405a> f;

    /* renamed from: b  reason: collision with root package name */
    public static final TimeUnit f3757b = TimeUnit.SECONDS;
    public static final long a = Integer.getInteger("rx.io-scheduler.keepalive", 60).intValue();

    /* compiled from: CachedThreadScheduler.java */
    /* renamed from: j0.l.c.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0405a {
        public final ThreadFactory a;

        /* renamed from: b  reason: collision with root package name */
        public final long f3758b;
        public final ConcurrentLinkedQueue<c> c;
        public final CompositeSubscription d;
        public final ScheduledExecutorService e;
        public final Future<?> f;

        /* compiled from: CachedThreadScheduler.java */
        /* renamed from: j0.l.c.a$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public class ThreadFactoryC0406a implements ThreadFactory {
            public final /* synthetic */ ThreadFactory j;

            public ThreadFactoryC0406a(C0405a aVar, ThreadFactory threadFactory) {
                this.j = threadFactory;
            }

            @Override // java.util.concurrent.ThreadFactory
            public Thread newThread(Runnable runnable) {
                Thread newThread = this.j.newThread(runnable);
                newThread.setName(newThread.getName() + " (Evictor)");
                return newThread;
            }
        }

        /* compiled from: CachedThreadScheduler.java */
        /* renamed from: j0.l.c.a$a$b */
        /* loaded from: classes3.dex */
        public class b implements Runnable {
            public b() {
            }

            @Override // java.lang.Runnable
            public void run() {
                C0405a aVar = C0405a.this;
                if (!aVar.c.isEmpty()) {
                    long nanoTime = System.nanoTime();
                    Iterator<c> it = aVar.c.iterator();
                    while (it.hasNext()) {
                        c next = it.next();
                        if (next.r > nanoTime) {
                            return;
                        }
                        if (aVar.c.remove(next)) {
                            aVar.d.c(next);
                        }
                    }
                }
            }
        }

        public C0405a(ThreadFactory threadFactory, long j, TimeUnit timeUnit) {
            ScheduledFuture<?> scheduledFuture;
            this.a = threadFactory;
            long nanos = timeUnit != null ? timeUnit.toNanos(j) : 0L;
            this.f3758b = nanos;
            this.c = new ConcurrentLinkedQueue<>();
            this.d = new CompositeSubscription();
            ScheduledExecutorService scheduledExecutorService = null;
            if (timeUnit != null) {
                scheduledExecutorService = Executors.newScheduledThreadPool(1, new ThreadFactoryC0406a(this, threadFactory));
                g.g(scheduledExecutorService);
                scheduledFuture = scheduledExecutorService.scheduleWithFixedDelay(new b(), nanos, nanos, TimeUnit.NANOSECONDS);
            } else {
                scheduledFuture = null;
            }
            this.e = scheduledExecutorService;
            this.f = scheduledFuture;
        }

        public void a() {
            try {
                Future<?> future = this.f;
                if (future != null) {
                    future.cancel(true);
                }
                ScheduledExecutorService scheduledExecutorService = this.e;
                if (scheduledExecutorService != null) {
                    scheduledExecutorService.shutdownNow();
                }
            } finally {
                this.d.unsubscribe();
            }
        }
    }

    /* compiled from: CachedThreadScheduler.java */
    /* loaded from: classes3.dex */
    public static final class b extends Scheduler.Worker implements Action0 {
        public final C0405a k;
        public final c l;
        public final CompositeSubscription j = new CompositeSubscription();
        public final AtomicBoolean m = new AtomicBoolean();

        /* compiled from: CachedThreadScheduler.java */
        /* renamed from: j0.l.c.a$b$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public class C0407a implements Action0 {
            public final /* synthetic */ Action0 j;

            public C0407a(Action0 action0) {
                this.j = action0;
            }

            @Override // rx.functions.Action0
            public void call() {
                if (!b.this.j.k) {
                    this.j.call();
                }
            }
        }

        public b(C0405a aVar) {
            c cVar;
            c cVar2;
            this.k = aVar;
            if (aVar.d.k) {
                cVar = a.c;
            } else {
                while (true) {
                    if (aVar.c.isEmpty()) {
                        cVar2 = new c(aVar.a);
                        aVar.d.a(cVar2);
                        break;
                    }
                    cVar2 = aVar.c.poll();
                    if (cVar2 != null) {
                        break;
                    }
                }
                cVar = cVar2;
            }
            this.l = cVar;
        }

        @Override // rx.Scheduler.Worker
        public Subscription a(Action0 action0) {
            return b(action0, 0L, null);
        }

        @Override // rx.Scheduler.Worker
        public Subscription b(Action0 action0, long j, TimeUnit timeUnit) {
            if (this.j.k) {
                return j0.r.c.a;
            }
            j f = this.l.f(new C0407a(action0), j, timeUnit);
            this.j.a(f);
            f.cancel.a(new j.c(f, this.j));
            return f;
        }

        @Override // rx.functions.Action0
        public void call() {
            C0405a aVar = this.k;
            c cVar = this.l;
            Objects.requireNonNull(aVar);
            cVar.r = System.nanoTime() + aVar.f3758b;
            aVar.c.offer(cVar);
        }

        @Override // rx.Subscription
        public boolean isUnsubscribed() {
            return this.j.k;
        }

        @Override // rx.Subscription
        public void unsubscribe() {
            if (this.m.compareAndSet(false, true)) {
                this.l.a(this);
            }
            this.j.unsubscribe();
        }
    }

    /* compiled from: CachedThreadScheduler.java */
    /* loaded from: classes3.dex */
    public static final class c extends g {
        public long r = 0;

        public c(ThreadFactory threadFactory) {
            super(threadFactory);
        }
    }

    static {
        c cVar = new c(j0.l.e.j.j);
        c = cVar;
        cVar.unsubscribe();
        C0405a aVar = new C0405a(null, 0L, null);
        d = aVar;
        aVar.a();
    }

    public a(ThreadFactory threadFactory) {
        this.e = threadFactory;
        C0405a aVar = d;
        AtomicReference<C0405a> atomicReference = new AtomicReference<>(aVar);
        this.f = atomicReference;
        C0405a aVar2 = new C0405a(threadFactory, a, f3757b);
        if (!atomicReference.compareAndSet(aVar, aVar2)) {
            aVar2.a();
        }
    }

    @Override // rx.Scheduler
    public Scheduler.Worker a() {
        return new b(this.f.get());
    }

    @Override // j0.l.c.k
    public void shutdown() {
        C0405a aVar;
        C0405a aVar2;
        do {
            aVar = this.f.get();
            aVar2 = d;
            if (aVar == aVar2) {
                return;
            }
        } while (!this.f.compareAndSet(aVar, aVar2));
        aVar.a();
    }
}
