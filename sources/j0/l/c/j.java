package j0.l.c;

import j0.o.l;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import rx.Subscription;
import rx.exceptions.OnErrorNotImplementedException;
import rx.functions.Action0;
import rx.internal.util.SubscriptionList;
import rx.subscriptions.CompositeSubscription;
/* compiled from: ScheduledAction.java */
/* loaded from: classes3.dex */
public final class j extends AtomicReference<Thread> implements Runnable, Subscription {
    private static final long serialVersionUID = -3962399486978279857L;
    public final Action0 action;
    public final SubscriptionList cancel;

    /* compiled from: ScheduledAction.java */
    /* loaded from: classes3.dex */
    public final class a implements Subscription {
        public final Future<?> j;

        public a(Future<?> future) {
            this.j = future;
        }

        @Override // rx.Subscription
        public boolean isUnsubscribed() {
            return this.j.isCancelled();
        }

        @Override // rx.Subscription
        public void unsubscribe() {
            if (j.this.get() != Thread.currentThread()) {
                this.j.cancel(true);
            } else {
                this.j.cancel(false);
            }
        }
    }

    /* compiled from: ScheduledAction.java */
    /* loaded from: classes3.dex */
    public static final class b extends AtomicBoolean implements Subscription {
        private static final long serialVersionUID = 247232374289553518L;
        public final SubscriptionList parent;

        /* renamed from: s  reason: collision with root package name */
        public final j f3764s;

        public b(j jVar, SubscriptionList subscriptionList) {
            this.f3764s = jVar;
            this.parent = subscriptionList;
        }

        @Override // rx.Subscription
        public boolean isUnsubscribed() {
            return this.f3764s.cancel.k;
        }

        @Override // rx.Subscription
        public void unsubscribe() {
            if (compareAndSet(false, true)) {
                SubscriptionList subscriptionList = this.parent;
                j jVar = this.f3764s;
                if (!subscriptionList.k) {
                    synchronized (subscriptionList) {
                        List<Subscription> list = subscriptionList.j;
                        if (!subscriptionList.k && list != null) {
                            boolean remove = list.remove(jVar);
                            if (remove) {
                                jVar.unsubscribe();
                            }
                        }
                    }
                }
            }
        }
    }

    /* compiled from: ScheduledAction.java */
    /* loaded from: classes3.dex */
    public static final class c extends AtomicBoolean implements Subscription {
        private static final long serialVersionUID = 247232374289553518L;
        public final CompositeSubscription parent;

        /* renamed from: s  reason: collision with root package name */
        public final j f3765s;

        public c(j jVar, CompositeSubscription compositeSubscription) {
            this.f3765s = jVar;
            this.parent = compositeSubscription;
        }

        @Override // rx.Subscription
        public boolean isUnsubscribed() {
            return this.f3765s.cancel.k;
        }

        @Override // rx.Subscription
        public void unsubscribe() {
            if (compareAndSet(false, true)) {
                this.parent.c(this.f3765s);
            }
        }
    }

    public j(Action0 action0) {
        this.action = action0;
        this.cancel = new SubscriptionList();
    }

    public void a(Future<?> future) {
        this.cancel.a(new a(future));
    }

    @Override // rx.Subscription
    public boolean isUnsubscribed() {
        return this.cancel.k;
    }

    @Override // java.lang.Runnable
    public void run() {
        try {
            try {
                lazySet(Thread.currentThread());
                this.action.call();
            } finally {
                unsubscribe();
            }
        } catch (OnErrorNotImplementedException e) {
            IllegalStateException illegalStateException = new IllegalStateException("Exception thrown on Scheduler.Worker thread. Add `onError` handling.", e);
            l.b(illegalStateException);
            Thread currentThread = Thread.currentThread();
            currentThread.getUncaughtExceptionHandler().uncaughtException(currentThread, illegalStateException);
        } catch (Throwable th) {
            IllegalStateException illegalStateException2 = new IllegalStateException("Fatal Exception thrown on Scheduler.Worker thread.", th);
            l.b(illegalStateException2);
            Thread currentThread2 = Thread.currentThread();
            currentThread2.getUncaughtExceptionHandler().uncaughtException(currentThread2, illegalStateException2);
        }
    }

    @Override // rx.Subscription
    public void unsubscribe() {
        if (!this.cancel.k) {
            this.cancel.unsubscribe();
        }
    }

    public j(Action0 action0, CompositeSubscription compositeSubscription) {
        this.action = action0;
        this.cancel = new SubscriptionList(new c(this, compositeSubscription));
    }

    public j(Action0 action0, SubscriptionList subscriptionList) {
        this.action = action0;
        this.cancel = new SubscriptionList(new b(this, subscriptionList));
    }
}
