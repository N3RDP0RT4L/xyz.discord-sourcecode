package j0.l.e.n;

import j0.l.e.m;
import java.util.AbstractQueue;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicReferenceArray;
/* compiled from: AtomicReferenceArrayQueue.java */
/* loaded from: classes3.dex */
public abstract class a<E> extends AbstractQueue<E> {
    public final AtomicReferenceArray<E> j;
    public final int k;

    public a(int i) {
        int b2 = m.b(i);
        this.k = b2 - 1;
        this.j = new AtomicReferenceArray<>(b2);
    }

    @Override // java.util.AbstractQueue, java.util.AbstractCollection, java.util.Collection
    public void clear() {
        while (true) {
            if (poll() == null && isEmpty()) {
                return;
            }
        }
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
    public Iterator<E> iterator() {
        throw new UnsupportedOperationException();
    }
}
