package j0.l.e.n;

import java.util.concurrent.atomic.AtomicReference;
/* compiled from: LinkedQueueNode.java */
/* loaded from: classes3.dex */
public final class b<E> extends AtomicReference<b<E>> {
    private static final long serialVersionUID = 2404266111789071508L;
    private E value;

    public b() {
    }

    public E a() {
        E e = this.value;
        this.value = null;
        return e;
    }

    public E b() {
        return this.value;
    }

    public b<E> c() {
        return get();
    }

    public b(E e) {
        this.value = e;
    }
}
