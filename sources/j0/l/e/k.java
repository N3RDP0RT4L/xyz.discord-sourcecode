package j0.l.e;

import j0.o.l;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import rx.Observable;
import rx.Producer;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
/* compiled from: ScalarSynchronousObservable.java */
/* loaded from: classes3.dex */
public final class k<T> extends Observable<T> {
    public static final boolean k = Boolean.valueOf(System.getProperty("rx.just.strong-mode", "false")).booleanValue();
    public final T l;

    /* compiled from: ScalarSynchronousObservable.java */
    /* loaded from: classes3.dex */
    public class a implements j0.k.b<Action0, Subscription> {
        public final /* synthetic */ j0.l.c.b j;

        public a(k kVar, j0.l.c.b bVar) {
            this.j = bVar;
        }

        @Override // j0.k.b
        public Subscription call(Action0 action0) {
            return this.j.e.get().a().f(action0, -1L, TimeUnit.NANOSECONDS);
        }
    }

    /* compiled from: ScalarSynchronousObservable.java */
    /* loaded from: classes3.dex */
    public class b implements j0.k.b<Action0, Subscription> {
        public final /* synthetic */ Scheduler j;

        public b(k kVar, Scheduler scheduler) {
            this.j = scheduler;
        }

        @Override // j0.k.b
        public Subscription call(Action0 action0) {
            Scheduler.Worker a = this.j.a();
            a.a(new l(this, action0, a));
            return a;
        }
    }

    /* compiled from: ScalarSynchronousObservable.java */
    /* loaded from: classes3.dex */
    public class c implements Observable.a<R> {
        public final /* synthetic */ j0.k.b j;

        public c(j0.k.b bVar) {
            this.j = bVar;
        }

        @Override // rx.functions.Action1
        public void call(Object obj) {
            Producer producer;
            Subscriber subscriber = (Subscriber) obj;
            Observable observable = (Observable) this.j.call(k.this.l);
            if (observable instanceof k) {
                T t = ((k) observable).l;
                if (k.k) {
                    producer = new j0.l.b.c(subscriber, t);
                } else {
                    producer = new g(subscriber, t);
                }
                subscriber.setProducer(producer);
                return;
            }
            observable.i0(new j0.n.e(subscriber, subscriber));
        }
    }

    /* compiled from: ScalarSynchronousObservable.java */
    /* loaded from: classes3.dex */
    public static final class d<T> implements Observable.a<T> {
        public final T j;

        public d(T t) {
            this.j = t;
        }

        @Override // rx.functions.Action1
        public void call(Object obj) {
            Producer producer;
            Subscriber subscriber = (Subscriber) obj;
            T t = this.j;
            if (k.k) {
                producer = new j0.l.b.c(subscriber, t);
            } else {
                producer = new g(subscriber, t);
            }
            subscriber.setProducer(producer);
        }
    }

    /* compiled from: ScalarSynchronousObservable.java */
    /* loaded from: classes3.dex */
    public static final class e<T> implements Observable.a<T> {
        public final T j;
        public final j0.k.b<Action0, Subscription> k;

        public e(T t, j0.k.b<Action0, Subscription> bVar) {
            this.j = t;
            this.k = bVar;
        }

        @Override // rx.functions.Action1
        public void call(Object obj) {
            Subscriber subscriber = (Subscriber) obj;
            subscriber.setProducer(new f(subscriber, this.j, this.k));
        }
    }

    /* compiled from: ScalarSynchronousObservable.java */
    /* loaded from: classes3.dex */
    public static final class f<T> extends AtomicBoolean implements Producer, Action0 {
        private static final long serialVersionUID = -2466317989629281651L;
        public final Subscriber<? super T> actual;
        public final j0.k.b<Action0, Subscription> onSchedule;
        public final T value;

        public f(Subscriber<? super T> subscriber, T t, j0.k.b<Action0, Subscription> bVar) {
            this.actual = subscriber;
            this.value = t;
            this.onSchedule = bVar;
        }

        @Override // rx.functions.Action0
        public void call() {
            Subscriber<? super T> subscriber = this.actual;
            if (!subscriber.isUnsubscribed()) {
                Object obj = (T) this.value;
                try {
                    subscriber.onNext(obj);
                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onCompleted();
                    }
                } catch (Throwable th) {
                    b.i.a.f.e.o.f.p1(th, subscriber, obj);
                }
            }
        }

        @Override // rx.Producer
        public void j(long j) {
            int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
            if (i < 0) {
                throw new IllegalArgumentException(b.d.b.a.a.s("n >= 0 required but it was ", j));
            } else if (i != 0 && compareAndSet(false, true)) {
                this.actual.add(this.onSchedule.call(this));
            }
        }

        @Override // java.util.concurrent.atomic.AtomicBoolean
        public String toString() {
            StringBuilder R = b.d.b.a.a.R("ScalarAsyncProducer[");
            R.append(this.value);
            R.append(", ");
            R.append(get());
            R.append("]");
            return R.toString();
        }
    }

    /* compiled from: ScalarSynchronousObservable.java */
    /* loaded from: classes3.dex */
    public static final class g<T> implements Producer {
        public final Subscriber<? super T> j;
        public final T k;
        public boolean l;

        public g(Subscriber<? super T> subscriber, T t) {
            this.j = subscriber;
            this.k = t;
        }

        @Override // rx.Producer
        public void j(long j) {
            if (!this.l) {
                int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
                if (i < 0) {
                    throw new IllegalStateException(b.d.b.a.a.s("n >= required but it was ", j));
                } else if (i != 0) {
                    this.l = true;
                    Subscriber<? super T> subscriber = this.j;
                    if (!subscriber.isUnsubscribed()) {
                        Object obj = (T) this.k;
                        try {
                            subscriber.onNext(obj);
                            if (!subscriber.isUnsubscribed()) {
                                subscriber.onCompleted();
                            }
                        } catch (Throwable th) {
                            b.i.a.f.e.o.f.p1(th, subscriber, obj);
                        }
                    }
                }
            }
        }
    }

    public k(T t) {
        super(l.a(new d(t)));
        this.l = t;
    }

    public <R> Observable<R> k0(j0.k.b<? super T, ? extends Observable<? extends R>> bVar) {
        return Observable.h0(new c(bVar));
    }

    public Observable<T> l0(Scheduler scheduler) {
        j0.k.b bVar;
        if (scheduler instanceof j0.l.c.b) {
            bVar = new a(this, (j0.l.c.b) scheduler);
        } else {
            bVar = new b(this, scheduler);
        }
        return Observable.h0(new e(this.l, bVar));
    }
}
