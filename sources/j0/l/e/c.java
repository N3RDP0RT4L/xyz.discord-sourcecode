package j0.l.e;

import androidx.recyclerview.widget.RecyclerView;
import java.util.concurrent.atomic.AtomicLong;
import rx.Producer;
/* compiled from: BackpressureDrainManager.java */
/* loaded from: classes3.dex */
public final class c extends AtomicLong implements Producer {
    private static final long serialVersionUID = 2826241102729529449L;
    public final a actual;
    public boolean emitting;
    public Throwable exception;
    public volatile boolean terminated;

    /* compiled from: BackpressureDrainManager.java */
    /* loaded from: classes3.dex */
    public interface a {
    }

    public c(a aVar) {
        this.actual = aVar;
    }

    /* JADX WARN: Code restructure failed: missing block: B:107:?, code lost:
        return;
     */
    /* JADX WARN: Code restructure failed: missing block: B:18:0x002a, code lost:
        r1 = r14.exception;
        r5 = (j0.l.a.a1.a) r5;
     */
    /* JADX WARN: Code restructure failed: missing block: B:19:0x002e, code lost:
        if (r1 == null) goto L21;
     */
    /* JADX WARN: Code restructure failed: missing block: B:20:0x0030, code lost:
        r5.l.onError(r1);
     */
    /* JADX WARN: Code restructure failed: missing block: B:21:0x0036, code lost:
        r5.l.onCompleted();
     */
    /* JADX WARN: Code restructure failed: missing block: B:22:0x003b, code lost:
        return;
     */
    /* JADX WARN: Code restructure failed: missing block: B:56:0x0099, code lost:
        r14.emitting = false;
     */
    /* JADX WARN: Code restructure failed: missing block: B:58:0x009c, code lost:
        return;
     */
    /* JADX WARN: Removed duplicated region for block: B:73:0x00c2  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void a() {
        /*
            Method dump skipped, instructions count: 206
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: j0.l.e.c.a():void");
    }

    @Override // rx.Producer
    public void j(long j) {
        boolean z2;
        if (j != 0) {
            while (true) {
                long j2 = get();
                boolean z3 = true;
                z2 = j2 == 0;
                long j3 = RecyclerView.FOREVER_NS;
                if (j2 == RecyclerView.FOREVER_NS) {
                    break;
                }
                if (j == RecyclerView.FOREVER_NS) {
                    j3 = j;
                } else {
                    if (j2 <= RecyclerView.FOREVER_NS - j) {
                        j3 = j2 + j;
                    }
                    z3 = z2;
                }
                if (compareAndSet(j2, j3)) {
                    z2 = z3;
                    break;
                }
            }
            if (z2) {
                a();
            }
        }
    }
}
