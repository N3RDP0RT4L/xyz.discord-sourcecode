package j0.l.e.o;

import java.util.Objects;
/* compiled from: SpscArrayQueue.java */
/* loaded from: classes3.dex */
public final class n<E> {
    public n(int i) {
        super(i);
    }

    public final long i() {
        return y.a.getLongVolatile(this, o.q);
    }

    public boolean isEmpty() {
        return j() == i();
    }

    public final long j() {
        return y.a.getLongVolatile(this, r.p);
    }

    public final void k(long j) {
        y.a.putOrderedLong(this, o.q, j);
    }

    public final void l(long j) {
        y.a.putOrderedLong(this, r.p, j);
    }

    public boolean offer(E e) {
        Objects.requireNonNull(e, "null elements not allowed");
        E[] eArr = this.n;
        long j = this.producerIndex;
        long c = c(j);
        if (e(eArr, c) != null) {
            return false;
        }
        g(eArr, c, e);
        l(j + 1);
        return true;
    }

    public E peek() {
        return e(this.n, c(this.consumerIndex));
    }

    public E poll() {
        long j = this.consumerIndex;
        long c = c(j);
        E[] eArr = this.n;
        E e = e(eArr, c);
        if (e == null) {
            return null;
        }
        g(eArr, c, null);
        k(j + 1);
        return e;
    }

    public int size() {
        long i = i();
        while (true) {
            long j = j();
            long i2 = i();
            if (i == i2) {
                return (int) (j - i2);
            }
            i = i2;
        }
    }
}
