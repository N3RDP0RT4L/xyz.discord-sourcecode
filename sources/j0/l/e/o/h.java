package j0.l.e.o;

import java.util.Objects;
/* compiled from: SpmcArrayQueue.java */
/* loaded from: classes3.dex */
public final class h<E> {
    public h(int i) {
        super(i);
    }

    public boolean isEmpty() {
        return l() == i();
    }

    public boolean offer(E e) {
        Objects.requireNonNull(e, "Null is not a valid element");
        E[] eArr = this.n;
        long j = this.m;
        long i = i();
        long c = c(i);
        if (e(eArr, c) == null) {
            h(eArr, c, e);
            j(i + 1);
            return true;
        } else if (i - l() > j) {
            return false;
        } else {
            do {
            } while (e(eArr, c) != null);
            h(eArr, c, e);
            j(i + 1);
            return true;
        }
    }

    public E peek() {
        E e;
        long j = this.q;
        do {
            long l = l();
            if (l >= j) {
                long i = i();
                if (l >= i) {
                    return null;
                }
                this.q = i;
            }
            e = e(this.n, c(l));
        } while (e == null);
        return e;
    }

    public E poll() {
        long l;
        long j = this.q;
        do {
            l = l();
            if (l >= j) {
                long i = i();
                if (l >= i) {
                    return null;
                }
                this.q = i;
            }
        } while (!k(l, 1 + l));
        long c = c(l);
        E[] eArr = this.n;
        E d = d(eArr, c);
        g(eArr, c, null);
        return d;
    }

    public int size() {
        long l = l();
        while (true) {
            long i = i();
            long l2 = l();
            if (l == l2) {
                return (int) (i - l2);
            }
            l = l2;
        }
    }
}
