package j0.l.e.o;

import j0.l.e.m;
import java.util.Iterator;
import sun.misc.Unsafe;
/* compiled from: ConcurrentCircularArrayQueue.java */
/* loaded from: classes3.dex */
public abstract class e<E> extends f<E> {
    public static final int j;
    public static final long k;
    public static final int l;
    public final long m;
    public final E[] n;

    static {
        int intValue = Integer.getInteger("sparse.shift", 0).intValue();
        j = intValue;
        Unsafe unsafe = y.a;
        int arrayIndexScale = unsafe.arrayIndexScale(Object[].class);
        if (4 == arrayIndexScale) {
            l = intValue + 2;
        } else if (8 == arrayIndexScale) {
            l = intValue + 3;
        } else {
            throw new IllegalStateException("Unknown pointer size");
        }
        k = unsafe.arrayBaseOffset(Object[].class) + (32 << (l - intValue));
    }

    public e(int i) {
        int b2 = m.b(i);
        this.m = b2 - 1;
        this.n = (E[]) new Object[(b2 << j) + 64];
    }

    public final long c(long j2) {
        return k + ((j2 & this.m) << l);
    }

    @Override // java.util.AbstractQueue, java.util.AbstractCollection, java.util.Collection
    public void clear() {
        while (true) {
            if (poll() == null && isEmpty()) {
                return;
            }
        }
    }

    public final E d(E[] eArr, long j2) {
        return (E) y.a.getObject(eArr, j2);
    }

    public final E e(E[] eArr, long j2) {
        return (E) y.a.getObjectVolatile(eArr, j2);
    }

    public final void g(E[] eArr, long j2, E e) {
        y.a.putOrderedObject(eArr, j2, e);
    }

    public final void h(E[] eArr, long j2, E e) {
        y.a.putObject(eArr, j2, e);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
    public Iterator<E> iterator() {
        throw new UnsupportedOperationException();
    }
}
