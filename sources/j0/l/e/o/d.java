package j0.l.e.o;

import j0.l.e.n.b;
/* compiled from: BaseLinkedQueue.java */
/* loaded from: classes3.dex */
public abstract class d<E> extends b<E> {
    public static final long j = y.a(d.class, "producerNode");
    public b<E> producerNode;

    public final b<E> c() {
        return (b) y.a.getObjectVolatile(this, j);
    }
}
