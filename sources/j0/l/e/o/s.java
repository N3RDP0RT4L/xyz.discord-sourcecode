package j0.l.e.o;

import j0.l.e.n.b;
import java.util.Iterator;
import java.util.Objects;
/* compiled from: SpscLinkedQueue.java */
/* loaded from: classes3.dex */
public final class s<E> {
    public s() {
        b<E> bVar = new b<>();
        this.producerNode = bVar;
        this.consumerNode = bVar;
        bVar.lazySet(null);
    }

    public final boolean isEmpty() {
        return d() == c();
    }

    public final Iterator iterator() {
        throw new UnsupportedOperationException();
    }

    public boolean offer(E e) {
        Objects.requireNonNull(e, "null elements not allowed");
        b<E> bVar = new b<>(e);
        this.producerNode.lazySet(bVar);
        this.producerNode = bVar;
        return true;
    }

    public E peek() {
        b<E> c = this.consumerNode.c();
        if (c != null) {
            return c.b();
        }
        return null;
    }

    public E poll() {
        b<E> c = this.consumerNode.c();
        if (c == null) {
            return null;
        }
        E a = c.a();
        this.consumerNode = c;
        return a;
    }

    public final int size() {
        b<E> c;
        b<E> d = d();
        b<E> c2 = c();
        int i = 0;
        while (d != c2 && i < Integer.MAX_VALUE) {
            do {
                c = d.c();
            } while (c == null);
            i++;
            d = c;
        }
        return i;
    }
}
