package j0.l.e.o;

import j0.l.e.n.b;
/* compiled from: BaseLinkedQueue.java */
/* loaded from: classes3.dex */
public abstract class a<E> extends c<E> {
    public static final long k = y.a(a.class, "consumerNode");
    public b<E> consumerNode;

    public final b<E> d() {
        return (b) y.a.getObjectVolatile(this, k);
    }
}
