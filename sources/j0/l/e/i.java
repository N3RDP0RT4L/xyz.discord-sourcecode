package j0.l.e;

import b.d.b.a.a;
import j0.l.a.e;
import j0.l.e.n.c;
import j0.l.e.o.h;
import j0.l.e.o.n;
import java.io.PrintStream;
import java.util.Queue;
import rx.Subscription;
import rx.exceptions.MissingBackpressureException;
/* compiled from: RxRingBuffer.java */
/* loaded from: classes3.dex */
public class i implements Subscription {
    public static final int j;
    public Queue<Object> k;
    public volatile Object l;

    static {
        int i = h.f3767b ? 16 : 128;
        String property = System.getProperty("rx.ring-buffer.size");
        if (property != null) {
            try {
                i = Integer.parseInt(property);
            } catch (NumberFormatException e) {
                PrintStream printStream = System.err;
                StringBuilder W = a.W("Failed to set 'rx.buffer.size' with value ", property, " => ");
                W.append(e.getMessage());
                printStream.println(W.toString());
            }
        }
        j = i;
    }

    public i(boolean z2, int i) {
        this.k = z2 ? new h<>(i) : new n<>(i);
    }

    public void a(Object obj) throws MissingBackpressureException {
        boolean z2;
        boolean z3;
        synchronized (this) {
            Queue<Object> queue = this.k;
            z2 = true;
            z3 = false;
            if (queue != null) {
                if (obj == null) {
                    obj = e.f3743b;
                }
                z3 = !queue.offer(obj);
                z2 = false;
            }
        }
        if (z2) {
            throw new IllegalStateException("This instance has been unsubscribed and the queue is no longer usable.");
        } else if (z3) {
            throw new MissingBackpressureException();
        }
    }

    public Object b() {
        synchronized (this) {
            Queue<Object> queue = this.k;
            if (queue == null) {
                return null;
            }
            Object peek = queue.peek();
            Object obj = this.l;
            if (peek == null && obj != null && queue.peek() == null) {
                peek = obj;
            }
            return peek;
        }
    }

    public Object c() {
        synchronized (this) {
            Queue<Object> queue = this.k;
            if (queue == null) {
                return null;
            }
            Object poll = queue.poll();
            Object obj = this.l;
            if (poll == null && obj != null && queue.peek() == null) {
                this.l = null;
                poll = obj;
            }
            return poll;
        }
    }

    @Override // rx.Subscription
    public boolean isUnsubscribed() {
        return this.k == null;
    }

    @Override // rx.Subscription
    public void unsubscribe() {
        synchronized (this) {
        }
    }

    public i() {
        this.k = new c(j);
    }
}
