package j0.l.e;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;
/* compiled from: RxThreadFactory.java */
/* loaded from: classes3.dex */
public final class j extends AtomicLong implements ThreadFactory {
    public static final ThreadFactory j = new a();
    private static final long serialVersionUID = -8841098858898482335L;
    public final String prefix;

    /* compiled from: RxThreadFactory.java */
    /* loaded from: classes3.dex */
    public static class a implements ThreadFactory {
        @Override // java.util.concurrent.ThreadFactory
        public Thread newThread(Runnable runnable) {
            throw new AssertionError("No threads allowed.");
        }
    }

    public j(String str) {
        this.prefix = str;
    }

    @Override // java.util.concurrent.ThreadFactory
    public Thread newThread(Runnable runnable) {
        Thread thread = new Thread(runnable, this.prefix + incrementAndGet());
        thread.setDaemon(true);
        return thread;
    }
}
