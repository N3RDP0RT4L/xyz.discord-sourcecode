package j0.l.e;
/* compiled from: PlatformDependent.java */
/* loaded from: classes3.dex */
public final class h {
    public static final int a;

    /* renamed from: b  reason: collision with root package name */
    public static final boolean f3767b;

    static {
        int i;
        boolean z2 = false;
        try {
            i = ((Integer) Class.forName("android.os.Build$VERSION").getField("SDK_INT").get(null)).intValue();
        } catch (Exception unused) {
            i = 0;
        }
        a = i;
        if (i != 0) {
            z2 = true;
        }
        f3767b = z2;
    }
}
