package j0.l.e;

import j0.g;
import rx.Subscriber;
/* compiled from: ObserverSubscriber.java */
/* loaded from: classes3.dex */
public final class f<T> extends Subscriber<T> {
    public final g<? super T> j;

    public f(g<? super T> gVar) {
        this.j = gVar;
    }

    @Override // j0.g
    public void onCompleted() {
        this.j.onCompleted();
    }

    @Override // j0.g
    public void onError(Throwable th) {
        this.j.onError(th);
    }

    @Override // j0.g
    public void onNext(T t) {
        this.j.onNext(t);
    }
}
