package j0.l.a;

import androidx.recyclerview.widget.RecyclerView;
import b.i.a.f.e.o.f;
import java.util.Iterator;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import rx.Observable;
import rx.Producer;
import rx.Subscriber;
/* compiled from: OnSubscribeFromIterable.java */
/* loaded from: classes3.dex */
public final class q<T> implements Observable.a<T> {
    public final Iterable<? extends T> j;

    /* compiled from: OnSubscribeFromIterable.java */
    /* loaded from: classes3.dex */
    public static final class a<T> extends AtomicLong implements Producer {
        private static final long serialVersionUID = -8730475647105475802L;
        private final Iterator<? extends T> it;
        private final Subscriber<? super T> o;

        public a(Subscriber<? super T> subscriber, Iterator<? extends T> it) {
            this.o = subscriber;
            this.it = it;
        }

        @Override // rx.Producer
        public void j(long j) {
            if (get() != RecyclerView.FOREVER_NS) {
                if (j == RecyclerView.FOREVER_NS && compareAndSet(0L, RecyclerView.FOREVER_NS)) {
                    Subscriber<? super T> subscriber = this.o;
                    Iterator<? extends T> it = this.it;
                    while (!subscriber.isUnsubscribed()) {
                        try {
                            subscriber.onNext((T) it.next());
                            if (!subscriber.isUnsubscribed()) {
                                try {
                                    if (!it.hasNext()) {
                                        if (!subscriber.isUnsubscribed()) {
                                            subscriber.onCompleted();
                                            return;
                                        }
                                        return;
                                    }
                                } catch (Throwable th) {
                                    f.o1(th);
                                    subscriber.onError(th);
                                    return;
                                }
                            } else {
                                return;
                            }
                        } catch (Throwable th2) {
                            f.o1(th2);
                            subscriber.onError(th2);
                            return;
                        }
                    }
                } else if (j > 0 && f.c0(this, j) == 0) {
                    Subscriber<? super T> subscriber2 = this.o;
                    Iterator<? extends T> it2 = this.it;
                    do {
                        long j2 = 0;
                        while (true) {
                            if (j2 == j) {
                                j = get();
                                if (j2 == j) {
                                    j = f.U0(this, j2);
                                }
                            } else if (!subscriber2.isUnsubscribed()) {
                                try {
                                    subscriber2.onNext((T) it2.next());
                                    if (!subscriber2.isUnsubscribed()) {
                                        try {
                                            if (it2.hasNext()) {
                                                j2++;
                                            } else if (!subscriber2.isUnsubscribed()) {
                                                subscriber2.onCompleted();
                                                return;
                                            } else {
                                                return;
                                            }
                                        } catch (Throwable th3) {
                                            f.o1(th3);
                                            subscriber2.onError(th3);
                                            return;
                                        }
                                    } else {
                                        return;
                                    }
                                } catch (Throwable th4) {
                                    f.o1(th4);
                                    subscriber2.onError(th4);
                                    return;
                                }
                            } else {
                                return;
                            }
                        }
                    } while (j != 0);
                }
            }
        }
    }

    public q(Iterable<? extends T> iterable) {
        Objects.requireNonNull(iterable, "iterable must not be null");
        this.j = iterable;
    }

    @Override // rx.functions.Action1
    public void call(Object obj) {
        Subscriber subscriber = (Subscriber) obj;
        try {
            Iterator<? extends T> it = this.j.iterator();
            boolean hasNext = it.hasNext();
            if (subscriber.isUnsubscribed()) {
                return;
            }
            if (!hasNext) {
                subscriber.onCompleted();
            } else {
                subscriber.setProducer(new a(subscriber, it));
            }
        } catch (Throwable th) {
            f.o1(th);
            subscriber.onError(th);
        }
    }
}
