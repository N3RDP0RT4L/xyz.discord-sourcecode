package j0.l.a;

import j0.l.e.i;
import j0.l.e.n.e;
import j0.o.l;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;
import rx.Observable;
import rx.Producer;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.CompositeException;
import rx.subscriptions.SerialSubscription;
/* compiled from: OperatorSwitch.java */
/* loaded from: classes3.dex */
public final class y1<T> implements Observable.b<T, Observable<? extends T>> {
    public final boolean j;

    /* compiled from: OperatorSwitch.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public static final y1<Object> a = new y1<>(false);
    }

    /* compiled from: OperatorSwitch.java */
    /* loaded from: classes3.dex */
    public static final class b<T> extends Subscriber<T> {
        public final long j;
        public final c<T> k;

        public b(long j, c<T> cVar) {
            this.j = j;
            this.k = cVar;
        }

        @Override // j0.g
        public void onCompleted() {
            c<T> cVar = this.k;
            long j = this.j;
            synchronized (cVar) {
                if (cVar.n.get() == j) {
                    cVar.v = false;
                    cVar.f3755s = null;
                    cVar.b();
                }
            }
        }

        @Override // j0.g
        public void onError(Throwable th) {
            boolean z2;
            c<T> cVar = this.k;
            long j = this.j;
            synchronized (cVar) {
                if (cVar.n.get() == j) {
                    z2 = cVar.c(th);
                    cVar.v = false;
                    cVar.f3755s = null;
                } else {
                    z2 = true;
                }
            }
            if (z2) {
                cVar.b();
            } else {
                l.b(th);
            }
        }

        @Override // j0.g
        public void onNext(T t) {
            c<T> cVar = this.k;
            synchronized (cVar) {
                if (cVar.n.get() == this.j) {
                    e<Object> eVar = cVar.o;
                    if (t == null) {
                        t = (T) e.f3743b;
                    }
                    eVar.e(this, t);
                    cVar.b();
                }
            }
        }

        @Override // rx.Subscriber
        public void setProducer(Producer producer) {
            c<T> cVar = this.k;
            long j = this.j;
            synchronized (cVar) {
                if (cVar.n.get() == j) {
                    long j2 = cVar.r;
                    cVar.f3755s = producer;
                    producer.j(j2);
                }
            }
        }
    }

    /* compiled from: OperatorSwitch.java */
    /* loaded from: classes3.dex */
    public static final class c<T> extends Subscriber<Observable<? extends T>> {
        public static final Throwable j = new Throwable("Terminal error");
        public final Subscriber<? super T> k;
        public final boolean m;
        public boolean p;
        public boolean q;
        public long r;

        /* renamed from: s  reason: collision with root package name */
        public Producer f3755s;
        public volatile boolean t;
        public Throwable u;
        public boolean v;
        public final SerialSubscription l = new SerialSubscription();
        public final AtomicLong n = new AtomicLong();
        public final e<Object> o = new e<>(i.j);

        public c(Subscriber<? super T> subscriber, boolean z2) {
            this.k = subscriber;
            this.m = z2;
        }

        public boolean a(boolean z2, boolean z3, Throwable th, e<Object> eVar, Subscriber<? super T> subscriber, boolean z4) {
            if (this.m) {
                if (!z2 || z3 || !z4) {
                    return false;
                }
                if (th != null) {
                    subscriber.onError(th);
                } else {
                    subscriber.onCompleted();
                }
                return true;
            } else if (th != null) {
                eVar.clear();
                subscriber.onError(th);
                return true;
            } else if (!z2 || z3 || !z4) {
                return false;
            } else {
                subscriber.onCompleted();
                return true;
            }
        }

        /* JADX WARN: Code restructure failed: missing block: B:31:0x0072, code lost:
            if (r18 != 0) goto L38;
         */
        /* JADX WARN: Code restructure failed: missing block: B:33:0x0078, code lost:
            if (r11.isUnsubscribed() == false) goto L35;
         */
        /* JADX WARN: Code restructure failed: missing block: B:34:0x007a, code lost:
            return;
         */
        /* JADX WARN: Code restructure failed: missing block: B:36:0x008b, code lost:
            if (a(r20.t, r0, r14, r9, r11, r9.isEmpty()) == false) goto L38;
         */
        /* JADX WARN: Code restructure failed: missing block: B:37:0x008d, code lost:
            return;
         */
        /* JADX WARN: Code restructure failed: missing block: B:38:0x008e, code lost:
            monitor-enter(r20);
         */
        /* JADX WARN: Code restructure failed: missing block: B:39:0x008f, code lost:
            r0 = r20.r;
         */
        /* JADX WARN: Code restructure failed: missing block: B:40:0x0098, code lost:
            if (r0 == androidx.recyclerview.widget.RecyclerView.FOREVER_NS) goto L42;
         */
        /* JADX WARN: Code restructure failed: missing block: B:41:0x009a, code lost:
            r0 = r0 - r16;
            r20.r = r0;
         */
        /* JADX WARN: Code restructure failed: missing block: B:42:0x009e, code lost:
            r12 = r0;
         */
        /* JADX WARN: Code restructure failed: missing block: B:43:0x00a2, code lost:
            if (r20.q != false) goto L47;
         */
        /* JADX WARN: Code restructure failed: missing block: B:44:0x00a4, code lost:
            r20.p = false;
         */
        /* JADX WARN: Code restructure failed: missing block: B:45:0x00a6, code lost:
            monitor-exit(r20);
         */
        /* JADX WARN: Code restructure failed: missing block: B:46:0x00a7, code lost:
            return;
         */
        /* JADX WARN: Code restructure failed: missing block: B:47:0x00a8, code lost:
            r20.q = false;
            r15 = r20.t;
            r0 = r20.v;
            r14 = r20.u;
         */
        /* JADX WARN: Code restructure failed: missing block: B:48:0x00b0, code lost:
            if (r14 == null) goto L54;
         */
        /* JADX WARN: Code restructure failed: missing block: B:49:0x00b2, code lost:
            r1 = j0.l.a.y1.c.j;
         */
        /* JADX WARN: Code restructure failed: missing block: B:50:0x00b4, code lost:
            if (r14 == r1) goto L54;
         */
        /* JADX WARN: Code restructure failed: missing block: B:52:0x00b8, code lost:
            if (r20.m != false) goto L54;
         */
        /* JADX WARN: Code restructure failed: missing block: B:53:0x00ba, code lost:
            r20.u = r1;
         */
        /* JADX WARN: Code restructure failed: missing block: B:54:0x00bc, code lost:
            monitor-exit(r20);
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public void b() {
            /*
                Method dump skipped, instructions count: 197
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: j0.l.a.y1.c.b():void");
        }

        public boolean c(Throwable th) {
            Throwable th2 = this.u;
            if (th2 == j) {
                return false;
            }
            if (th2 == null) {
                this.u = th;
            } else if (th2 instanceof CompositeException) {
                ArrayList arrayList = new ArrayList(((CompositeException) th2).b());
                arrayList.add(th);
                this.u = new CompositeException(arrayList);
            } else {
                this.u = new CompositeException(th2, th);
            }
            return true;
        }

        @Override // j0.g
        public void onCompleted() {
            this.t = true;
            b();
        }

        @Override // j0.g
        public void onError(Throwable th) {
            boolean c;
            synchronized (this) {
                c = c(th);
            }
            if (c) {
                this.t = true;
                b();
                return;
            }
            l.b(th);
        }

        @Override // j0.g
        public void onNext(Object obj) {
            b bVar;
            Observable observable = (Observable) obj;
            long incrementAndGet = this.n.incrementAndGet();
            Subscription subscription = this.l.j.get();
            if (subscription == j0.l.d.b.INSTANCE) {
                subscription = j0.r.c.a;
            }
            if (subscription != null) {
                subscription.unsubscribe();
            }
            synchronized (this) {
                bVar = new b(incrementAndGet, this);
                this.v = true;
                this.f3755s = null;
            }
            this.l.a(bVar);
            observable.i0(bVar);
        }
    }

    public y1(boolean z2) {
        this.j = z2;
    }

    @Override // j0.k.b
    public Object call(Object obj) {
        Subscriber subscriber = (Subscriber) obj;
        c cVar = new c(subscriber, this.j);
        subscriber.add(cVar);
        cVar.k.add(cVar.l);
        cVar.k.add(new j0.r.a(new z1(cVar)));
        cVar.k.setProducer(new a2(cVar));
        return cVar;
    }
}
