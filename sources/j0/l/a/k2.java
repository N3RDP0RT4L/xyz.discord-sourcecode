package j0.l.a;

import androidx.recyclerview.widget.RecyclerView;
import java.util.Objects;
import rx.Subscriber;
/* compiled from: OperatorThrottleFirst.java */
/* loaded from: classes3.dex */
public class k2 extends Subscriber<T> {
    public long j = -1;
    public final /* synthetic */ Subscriber k;
    public final /* synthetic */ l2 l;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public k2(l2 l2Var, Subscriber subscriber, Subscriber subscriber2) {
        super(subscriber);
        this.l = l2Var;
        this.k = subscriber2;
    }

    @Override // j0.g
    public void onCompleted() {
        this.k.onCompleted();
    }

    @Override // j0.g
    public void onError(Throwable th) {
        this.k.onError(th);
    }

    @Override // j0.g
    public void onNext(T t) {
        Objects.requireNonNull(this.l.k);
        long currentTimeMillis = System.currentTimeMillis();
        long j = this.j;
        if (j == -1 || currentTimeMillis < j || currentTimeMillis - j >= this.l.j) {
            this.j = currentTimeMillis;
            this.k.onNext(t);
        }
    }

    @Override // rx.Subscriber
    public void onStart() {
        request(RecyclerView.FOREVER_NS);
    }
}
