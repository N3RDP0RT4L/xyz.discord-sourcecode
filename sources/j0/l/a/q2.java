package j0.l.a;

import b.i.a.f.e.o.f;
import j0.g;
import j0.k.d;
import j0.l.e.i;
import j0.l.e.o.y;
import java.util.concurrent.atomic.AtomicLong;
import rx.Observable;
import rx.Producer;
import rx.Subscriber;
import rx.exceptions.MissingBackpressureException;
import rx.functions.Func2;
import rx.functions.FuncN;
import rx.subscriptions.CompositeSubscription;
/* compiled from: OperatorZip.java */
/* loaded from: classes3.dex */
public final class q2<R> implements Observable.b<R, Observable<?>[]> {
    public final FuncN<? extends R> j;

    /* compiled from: OperatorZip.java */
    /* loaded from: classes3.dex */
    public static final class a<R> extends AtomicLong {
        public static final int j = (int) (i.j * 0.7d);
        private static final long serialVersionUID = 5995274816189928317L;
        public final g<? super R> child;
        private final CompositeSubscription childSubscription;
        public int emitted;
        private AtomicLong requested;
        private volatile Object[] subscribers;
        private final FuncN<? extends R> zipFunction;

        /* compiled from: OperatorZip.java */
        /* renamed from: j0.l.a.q2$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public final class C0401a extends Subscriber {
            public final i j;

            public C0401a() {
                i iVar;
                int i = i.j;
                if (y.b()) {
                    iVar = new i(true, i.j);
                } else {
                    iVar = new i();
                }
                this.j = iVar;
            }

            @Override // j0.g
            public void onCompleted() {
                i iVar = this.j;
                if (iVar.l == null) {
                    iVar.l = e.a;
                }
                a.this.b();
            }

            @Override // j0.g
            public void onError(Throwable th) {
                a.this.child.onError(th);
            }

            @Override // j0.g
            public void onNext(Object obj) {
                try {
                    this.j.a(obj);
                } catch (MissingBackpressureException e) {
                    a.this.child.onError(e);
                }
                a.this.b();
            }

            @Override // rx.Subscriber
            public void onStart() {
                request(i.j);
            }
        }

        public a(Subscriber<? super R> subscriber, FuncN<? extends R> funcN) {
            CompositeSubscription compositeSubscription = new CompositeSubscription();
            this.childSubscription = compositeSubscription;
            this.child = subscriber;
            this.zipFunction = funcN;
            subscriber.add(compositeSubscription);
        }

        public void a(Observable[] observableArr, AtomicLong atomicLong) {
            Object[] objArr = new Object[observableArr.length];
            for (int i = 0; i < observableArr.length; i++) {
                C0401a aVar = new C0401a();
                objArr[i] = aVar;
                this.childSubscription.a(aVar);
            }
            this.requested = atomicLong;
            this.subscribers = objArr;
            for (int i2 = 0; i2 < observableArr.length; i2++) {
                observableArr[i2].i0((C0401a) objArr[i2]);
            }
        }

        public void b() {
            Object[] objArr = this.subscribers;
            if (objArr != null && getAndIncrement() == 0) {
                int length = objArr.length;
                g<? super R> gVar = this.child;
                AtomicLong atomicLong = this.requested;
                while (true) {
                    Object[] objArr2 = new Object[length];
                    boolean z2 = true;
                    for (int i = 0; i < length; i++) {
                        Object b2 = ((C0401a) objArr[i]).j.b();
                        if (b2 == null) {
                            z2 = false;
                        } else if (e.c(b2)) {
                            gVar.onCompleted();
                            this.childSubscription.unsubscribe();
                            return;
                        } else {
                            objArr2[i] = e.b(b2);
                        }
                    }
                    if (z2 && atomicLong.get() > 0) {
                        try {
                            gVar.onNext((R) this.zipFunction.call(objArr2));
                            atomicLong.decrementAndGet();
                            this.emitted++;
                            for (Object obj : objArr) {
                                i iVar = ((C0401a) obj).j;
                                iVar.c();
                                if (e.c(iVar.b())) {
                                    gVar.onCompleted();
                                    this.childSubscription.unsubscribe();
                                    return;
                                }
                            }
                            if (this.emitted > j) {
                                for (Object obj2 : objArr) {
                                    ((C0401a) obj2).request(this.emitted);
                                }
                                this.emitted = 0;
                            }
                        } catch (Throwable th) {
                            f.p1(th, gVar, objArr2);
                            return;
                        }
                    } else if (decrementAndGet() <= 0) {
                        return;
                    }
                }
            }
        }
    }

    /* compiled from: OperatorZip.java */
    /* loaded from: classes3.dex */
    public static final class b<R> extends AtomicLong implements Producer {
        private static final long serialVersionUID = -1216676403723546796L;
        public final a<R> zipper;

        public b(a<R> aVar) {
            this.zipper = aVar;
        }

        @Override // rx.Producer
        public void j(long j) {
            f.c0(this, j);
            this.zipper.b();
        }
    }

    /* compiled from: OperatorZip.java */
    /* loaded from: classes3.dex */
    public final class c extends Subscriber<Observable[]> {
        public final Subscriber<? super R> j;
        public final a<R> k;
        public final b<R> l;
        public boolean m;

        public c(q2 q2Var, Subscriber<? super R> subscriber, a<R> aVar, b<R> bVar) {
            this.j = subscriber;
            this.k = aVar;
            this.l = bVar;
        }

        @Override // j0.g
        public void onCompleted() {
            if (!this.m) {
                this.j.onCompleted();
            }
        }

        @Override // j0.g
        public void onError(Throwable th) {
            this.j.onError(th);
        }

        @Override // j0.g
        public void onNext(Object obj) {
            Observable[] observableArr = (Observable[]) obj;
            if (observableArr == null || observableArr.length == 0) {
                this.j.onCompleted();
                return;
            }
            this.m = true;
            this.k.a(observableArr, this.l);
        }
    }

    public q2(Func2 func2) {
        this.j = new d(func2);
    }

    @Override // j0.k.b
    public Object call(Object obj) {
        Subscriber subscriber = (Subscriber) obj;
        a aVar = new a(subscriber, this.j);
        b bVar = new b(aVar);
        c cVar = new c(this, subscriber, aVar, bVar);
        subscriber.add(cVar);
        subscriber.setProducer(bVar);
        return cVar;
    }
}
