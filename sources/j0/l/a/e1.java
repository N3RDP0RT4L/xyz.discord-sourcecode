package j0.l.a;

import b.i.a.f.e.o.f;
import j0.o.l;
import rx.Observable;
import rx.Producer;
import rx.Subscriber;
import rx.subscriptions.SerialSubscription;
/* compiled from: OperatorOnErrorResumeNextViaFunction.java */
/* loaded from: classes3.dex */
public class e1 extends Subscriber<T> {
    public boolean j;
    public long k;
    public final /* synthetic */ Subscriber l;
    public final /* synthetic */ j0.l.b.a m;
    public final /* synthetic */ SerialSubscription n;
    public final /* synthetic */ f1 o;

    /* compiled from: OperatorOnErrorResumeNextViaFunction.java */
    /* loaded from: classes3.dex */
    public class a extends Subscriber<T> {
        public a() {
        }

        @Override // j0.g
        public void onCompleted() {
            e1.this.l.onCompleted();
        }

        @Override // j0.g
        public void onError(Throwable th) {
            e1.this.l.onError(th);
        }

        @Override // j0.g
        public void onNext(T t) {
            e1.this.l.onNext(t);
        }

        @Override // rx.Subscriber
        public void setProducer(Producer producer) {
            e1.this.m.c(producer);
        }
    }

    public e1(f1 f1Var, Subscriber subscriber, j0.l.b.a aVar, SerialSubscription serialSubscription) {
        this.o = f1Var;
        this.l = subscriber;
        this.m = aVar;
        this.n = serialSubscription;
    }

    @Override // j0.g
    public void onCompleted() {
        if (!this.j) {
            this.j = true;
            this.l.onCompleted();
        }
    }

    @Override // j0.g
    public void onError(Throwable th) {
        if (this.j) {
            f.o1(th);
            l.b(th);
            return;
        }
        this.j = true;
        try {
            unsubscribe();
            a aVar = new a();
            this.n.a(aVar);
            long j = this.k;
            if (j != 0) {
                this.m.b(j);
            }
            ((Observable) this.o.j.call(th)).i0(aVar);
        } catch (Throwable th2) {
            Subscriber subscriber = this.l;
            f.o1(th2);
            subscriber.onError(th2);
        }
    }

    @Override // j0.g
    public void onNext(T t) {
        if (!this.j) {
            this.k++;
            this.l.onNext(t);
        }
    }

    @Override // rx.Subscriber
    public void setProducer(Producer producer) {
        this.m.c(producer);
    }
}
