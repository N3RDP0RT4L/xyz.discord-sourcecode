package j0.l.a;

import j0.l.a.h1;
import j0.r.a;
import java.util.concurrent.atomic.AtomicReference;
import rx.Observable;
import rx.Subscriber;
/* compiled from: OperatorPublish.java */
/* loaded from: classes3.dex */
public final class g1 implements Observable.a<T> {
    public final /* synthetic */ AtomicReference j;

    public g1(AtomicReference atomicReference) {
        this.j = atomicReference;
    }

    @Override // rx.functions.Action1
    public void call(Object obj) {
        boolean z2;
        Subscriber subscriber = (Subscriber) obj;
        while (true) {
            h1.b bVar = (h1.b) this.j.get();
            if (bVar == null || bVar.isUnsubscribed()) {
                h1.b bVar2 = new h1.b(this.j);
                bVar2.add(new a(new i1(bVar2)));
                if (!this.j.compareAndSet(bVar, bVar2)) {
                    continue;
                } else {
                    bVar = bVar2;
                }
            }
            h1.a aVar = new h1.a(bVar, subscriber);
            while (true) {
                h1.a[] aVarArr = bVar.o.get();
                z2 = false;
                if (aVarArr != h1.b.k) {
                    int length = aVarArr.length;
                    h1.a[] aVarArr2 = new h1.a[length + 1];
                    System.arraycopy(aVarArr, 0, aVarArr2, 0, length);
                    aVarArr2[length] = aVar;
                    if (bVar.o.compareAndSet(aVarArr, aVarArr2)) {
                        z2 = true;
                        break;
                    }
                } else {
                    break;
                }
            }
            if (z2) {
                subscriber.add(aVar);
                subscriber.setProducer(aVar);
                return;
            }
        }
    }
}
