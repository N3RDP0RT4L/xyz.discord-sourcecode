package j0.l.a;

import androidx.recyclerview.widget.RecyclerView;
import j0.l.a.e;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import rx.Observable;
import rx.Producer;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.OnErrorThrowable;
import rx.functions.Action1;
import rx.functions.Func0;
/* compiled from: OperatorReplay.java */
/* loaded from: classes3.dex */
public final class j1<T> extends j0.m.b<T> implements Subscription {
    public static final Func0 k = new a();
    public final Observable<? extends T> l;
    public final AtomicReference<f<T>> m;
    public final Func0<? extends e<T>> n;

    /* compiled from: OperatorReplay.java */
    /* loaded from: classes3.dex */
    public static class a implements Func0 {
        @Override // rx.functions.Func0, java.util.concurrent.Callable
        public Object call() {
            return new h(16);
        }
    }

    /* compiled from: OperatorReplay.java */
    /* loaded from: classes3.dex */
    public static class b<T> extends AtomicReference<d> implements e<T> {
        private static final long serialVersionUID = 2346567790059478686L;
        public long index;
        public int size;
        public d tail;

        public b() {
            d dVar = new d(null, 0L);
            this.tail = dVar;
            set(dVar);
        }

        public void a() {
        }

        @Override // j0.l.a.j1.e
        public final void complete() {
            Object obj = j0.l.a.e.a;
            long j = this.index + 1;
            this.index = j;
            d dVar = new d(obj, j);
            this.tail.set(dVar);
            this.tail = dVar;
            this.size++;
        }

        @Override // j0.l.a.j1.e
        public final void g(T t) {
            if (t == null) {
                t = (T) j0.l.a.e.f3743b;
            }
            long j = this.index + 1;
            this.index = j;
            d dVar = new d(t, j);
            this.tail.set(dVar);
            this.tail = dVar;
            this.size++;
            a();
        }

        @Override // j0.l.a.j1.e
        public final void h(c<T> cVar) {
            Subscriber<? super T> subscriber;
            d dVar;
            synchronized (cVar) {
                if (cVar.emitting) {
                    cVar.missed = true;
                    return;
                }
                cVar.emitting = true;
                while (!cVar.isUnsubscribed()) {
                    d dVar2 = (d) cVar.index;
                    if (dVar2 == null) {
                        dVar2 = get();
                        cVar.index = dVar2;
                        cVar.a(dVar2.index);
                    }
                    if (!cVar.isUnsubscribed() && (subscriber = cVar.child) != null) {
                        long j = cVar.get();
                        long j2 = 0;
                        while (j2 != j && (dVar = dVar2.get()) != null) {
                            Object obj = dVar.value;
                            try {
                                if (j0.l.a.e.a(subscriber, obj)) {
                                    cVar.index = null;
                                    return;
                                }
                                j2++;
                                if (!cVar.isUnsubscribed()) {
                                    dVar2 = dVar;
                                } else {
                                    return;
                                }
                            } catch (Throwable th) {
                                cVar.index = null;
                                b.i.a.f.e.o.f.o1(th);
                                cVar.unsubscribe();
                                if (!(obj instanceof e.c) && !j0.l.a.e.c(obj)) {
                                    subscriber.onError(OnErrorThrowable.a(th, j0.l.a.e.b(obj)));
                                    return;
                                }
                                return;
                            }
                        }
                        if (j2 != 0) {
                            cVar.index = dVar2;
                            if (j != RecyclerView.FOREVER_NS) {
                                cVar.b(j2);
                            }
                        }
                        synchronized (cVar) {
                            if (!cVar.missed) {
                                cVar.emitting = false;
                                return;
                            }
                            cVar.missed = false;
                        }
                    } else {
                        return;
                    }
                }
            }
        }

        @Override // j0.l.a.j1.e
        public final void i(Throwable th) {
            e.c cVar = new e.c(th);
            long j = this.index + 1;
            this.index = j;
            d dVar = new d(cVar, j);
            this.tail.set(dVar);
            this.tail = dVar;
            this.size++;
        }
    }

    /* compiled from: OperatorReplay.java */
    /* loaded from: classes3.dex */
    public static final class c<T> extends AtomicLong implements Producer, Subscription {
        private static final long serialVersionUID = -4453897557930727610L;
        public Subscriber<? super T> child;
        public boolean emitting;
        public Object index;
        public boolean missed;
        public final f<T> parent;
        public final AtomicLong totalRequested = new AtomicLong();

        public c(f<T> fVar, Subscriber<? super T> subscriber) {
            this.parent = fVar;
            this.child = subscriber;
        }

        public void a(long j) {
            long j2;
            long j3;
            do {
                j2 = this.totalRequested.get();
                j3 = j2 + j;
                if (j3 < 0) {
                    j3 = RecyclerView.FOREVER_NS;
                }
            } while (!this.totalRequested.compareAndSet(j2, j3));
        }

        public long b(long j) {
            long j2;
            long j3;
            if (j > 0) {
                do {
                    j2 = get();
                    if (j2 == Long.MIN_VALUE) {
                        return Long.MIN_VALUE;
                    }
                    j3 = j2 - j;
                    if (j3 < 0) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("More produced (");
                        sb.append(j);
                        sb.append(") than requested (");
                        throw new IllegalStateException(b.d.b.a.a.B(sb, j2, ")"));
                    }
                } while (!compareAndSet(j2, j3));
                return j3;
            }
            throw new IllegalArgumentException("Cant produce zero or less");
        }

        @Override // rx.Subscription
        public boolean isUnsubscribed() {
            return get() == Long.MIN_VALUE;
        }

        @Override // rx.Producer
        public void j(long j) {
            long j2;
            long j3;
            int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
            if (i >= 0) {
                do {
                    j2 = get();
                    if (j2 != Long.MIN_VALUE) {
                        if (j2 < 0 || i != 0) {
                            j3 = j2 + j;
                            if (j3 < 0) {
                                j3 = RecyclerView.FOREVER_NS;
                            }
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                } while (!compareAndSet(j2, j3));
                a(j);
                this.parent.c(this);
                this.parent.k.h(this);
            }
        }

        @Override // rx.Subscription
        public void unsubscribe() {
            if (get() != Long.MIN_VALUE && getAndSet(Long.MIN_VALUE) != Long.MIN_VALUE) {
                f<T> fVar = this.parent;
                if (!fVar.m) {
                    synchronized (fVar.n) {
                        if (!fVar.m) {
                            j0.l.e.g<c<T>> gVar = fVar.n;
                            c<T>[] cVarArr = gVar.d;
                            int i = gVar.a;
                            int b2 = j0.l.e.g.b(hashCode()) & i;
                            c<T> cVar = cVarArr[b2];
                            boolean z2 = true;
                            if (cVar != null) {
                                if (!cVar.equals(this)) {
                                    while (true) {
                                        b2 = (b2 + 1) & i;
                                        c<T> cVar2 = cVarArr[b2];
                                        if (cVar2 != null) {
                                            if (cVar2.equals(this)) {
                                                gVar.c(b2, cVarArr, i);
                                                break;
                                            }
                                        } else {
                                            break;
                                        }
                                    }
                                } else {
                                    gVar.c(b2, cVarArr, i);
                                }
                            }
                            if (fVar.n.f3766b != 0) {
                                z2 = false;
                            }
                            if (z2) {
                                fVar.o = f.j;
                            }
                            fVar.p++;
                        }
                    }
                }
                this.parent.c(this);
                this.child = null;
            }
        }
    }

    /* compiled from: OperatorReplay.java */
    /* loaded from: classes3.dex */
    public static final class d extends AtomicReference<d> {
        private static final long serialVersionUID = 245354315435971818L;
        public final long index;
        public final Object value;

        public d(Object obj, long j) {
            this.value = obj;
            this.index = j;
        }
    }

    /* compiled from: OperatorReplay.java */
    /* loaded from: classes3.dex */
    public interface e<T> {
        void complete();

        void g(T t);

        void h(c<T> cVar);

        void i(Throwable th);
    }

    /* compiled from: OperatorReplay.java */
    /* loaded from: classes3.dex */
    public static final class f<T> extends Subscriber<T> implements Subscription {
        public static final c[] j = new c[0];
        public final e<T> k;
        public boolean l;
        public volatile boolean m;
        public volatile long p;
        public long q;

        /* renamed from: s  reason: collision with root package name */
        public boolean f3746s;
        public boolean t;
        public long u;
        public long v;
        public volatile Producer w;

        /* renamed from: x  reason: collision with root package name */
        public List<c<T>> f3747x;

        /* renamed from: y  reason: collision with root package name */
        public boolean f3748y;
        public final j0.l.e.g<c<T>> n = new j0.l.e.g<>();
        public c<T>[] o = j;
        public final AtomicBoolean r = new AtomicBoolean();

        public f(e<T> eVar) {
            this.k = eVar;
            request(0L);
        }

        public c<T>[] a() {
            c<T>[] cVarArr;
            synchronized (this.n) {
                c<T>[] cVarArr2 = this.n.d;
                int length = cVarArr2.length;
                cVarArr = new c[length];
                System.arraycopy(cVarArr2, 0, cVarArr, 0, length);
            }
            return cVarArr;
        }

        public void b(long j2, long j3) {
            long j4 = this.v;
            Producer producer = this.w;
            long j5 = j2 - j3;
            if (j5 != 0) {
                this.u = j2;
                if (producer == null) {
                    long j6 = j4 + j5;
                    if (j6 < 0) {
                        j6 = RecyclerView.FOREVER_NS;
                    }
                    this.v = j6;
                } else if (j4 != 0) {
                    this.v = 0L;
                    producer.j(j4 + j5);
                } else {
                    producer.j(j5);
                }
            } else if (j4 != 0 && producer != null) {
                this.v = 0L;
                producer.j(j4);
            }
        }

        public void c(c<T> cVar) {
            long j2;
            List<c<T>> list;
            boolean z2;
            long j3;
            c<T>[] a;
            c<T>[] a2;
            if (!isUnsubscribed()) {
                synchronized (this) {
                    if (this.f3746s) {
                        if (cVar != null) {
                            List list2 = this.f3747x;
                            if (list2 == null) {
                                list2 = new ArrayList();
                                this.f3747x = list2;
                            }
                            list2.add(cVar);
                        } else {
                            this.f3748y = true;
                        }
                        this.t = true;
                        return;
                    }
                    this.f3746s = true;
                    long j4 = this.u;
                    if (cVar != null) {
                        j2 = Math.max(j4, cVar.totalRequested.get());
                    } else {
                        long j5 = j4;
                        for (c<T> cVar2 : a()) {
                            if (cVar2 != null) {
                                j5 = Math.max(j5, cVar2.totalRequested.get());
                            }
                        }
                        j2 = j5;
                    }
                    b(j2, j4);
                    while (!isUnsubscribed()) {
                        synchronized (this) {
                            if (!this.t) {
                                this.f3746s = false;
                                return;
                            }
                            this.t = false;
                            list = this.f3747x;
                            this.f3747x = null;
                            z2 = this.f3748y;
                            this.f3748y = false;
                        }
                        long j6 = this.u;
                        if (list != null) {
                            j3 = j6;
                            for (c<T> cVar3 : list) {
                                j3 = Math.max(j3, cVar3.totalRequested.get());
                            }
                        } else {
                            j3 = j6;
                        }
                        if (z2) {
                            for (c<T> cVar4 : a()) {
                                if (cVar4 != null) {
                                    j3 = Math.max(j3, cVar4.totalRequested.get());
                                }
                            }
                        }
                        b(j3, j6);
                    }
                }
            }
        }

        public void d() {
            c<T>[] cVarArr = this.o;
            if (this.q != this.p) {
                synchronized (this.n) {
                    cVarArr = this.o;
                    c<T>[] cVarArr2 = this.n.d;
                    int length = cVarArr2.length;
                    if (cVarArr.length != length) {
                        cVarArr = new c[length];
                        this.o = cVarArr;
                    }
                    System.arraycopy(cVarArr2, 0, cVarArr, 0, length);
                    this.q = this.p;
                }
            }
            e<T> eVar = this.k;
            for (c<T> cVar : cVarArr) {
                if (cVar != null) {
                    eVar.h(cVar);
                }
            }
        }

        @Override // j0.g
        public void onCompleted() {
            if (!this.l) {
                this.l = true;
                try {
                    this.k.complete();
                    d();
                } finally {
                    unsubscribe();
                }
            }
        }

        @Override // j0.g
        public void onError(Throwable th) {
            if (!this.l) {
                this.l = true;
                try {
                    this.k.i(th);
                    d();
                } finally {
                    unsubscribe();
                }
            }
        }

        @Override // j0.g
        public void onNext(T t) {
            if (!this.l) {
                this.k.g(t);
                d();
            }
        }

        @Override // rx.Subscriber
        public void setProducer(Producer producer) {
            if (this.w == null) {
                this.w = producer;
                c(null);
                d();
                return;
            }
            throw new IllegalStateException("Only a single producer can be set on a Subscriber.");
        }
    }

    /* compiled from: OperatorReplay.java */
    /* loaded from: classes3.dex */
    public static final class g<T> extends b<T> {
        private static final long serialVersionUID = -5898283885385201806L;
        public final int limit;

        public g(int i) {
            this.limit = i;
        }

        @Override // j0.l.a.j1.b
        public void a() {
            if (this.size > this.limit) {
                d dVar = get().get();
                if (dVar != null) {
                    this.size--;
                    set(dVar);
                    return;
                }
                throw new IllegalStateException("Empty list!");
            }
        }
    }

    /* compiled from: OperatorReplay.java */
    /* loaded from: classes3.dex */
    public static final class h<T> extends ArrayList<Object> implements e<T> {
        private static final long serialVersionUID = 7063189396499112664L;
        public volatile int size;

        public h(int i) {
            super(i);
        }

        @Override // j0.l.a.j1.e
        public void complete() {
            add(j0.l.a.e.a);
            this.size++;
        }

        @Override // j0.l.a.j1.e
        public void g(T t) {
            if (t == null) {
                t = (T) j0.l.a.e.f3743b;
            }
            add(t);
            this.size++;
        }

        @Override // j0.l.a.j1.e
        public void h(c<T> cVar) {
            synchronized (cVar) {
                if (cVar.emitting) {
                    cVar.missed = true;
                    return;
                }
                cVar.emitting = true;
                while (!cVar.isUnsubscribed()) {
                    int i = this.size;
                    Integer num = (Integer) cVar.index;
                    int intValue = num != null ? num.intValue() : 0;
                    Subscriber<? super T> subscriber = cVar.child;
                    if (subscriber != null) {
                        long j = cVar.get();
                        long j2 = 0;
                        while (j2 != j && intValue < i) {
                            Object obj = get(intValue);
                            try {
                                if (!j0.l.a.e.a(subscriber, obj) && !cVar.isUnsubscribed()) {
                                    intValue++;
                                    j2++;
                                } else {
                                    return;
                                }
                            } catch (Throwable th) {
                                b.i.a.f.e.o.f.o1(th);
                                cVar.unsubscribe();
                                if (!(obj instanceof e.c) && !j0.l.a.e.c(obj)) {
                                    subscriber.onError(OnErrorThrowable.a(th, j0.l.a.e.b(obj)));
                                    return;
                                }
                                return;
                            }
                        }
                        if (j2 != 0) {
                            cVar.index = Integer.valueOf(intValue);
                            if (j != RecyclerView.FOREVER_NS) {
                                cVar.b(j2);
                            }
                        }
                        synchronized (cVar) {
                            if (!cVar.missed) {
                                cVar.emitting = false;
                                return;
                            }
                            cVar.missed = false;
                        }
                    } else {
                        return;
                    }
                }
            }
        }

        @Override // j0.l.a.j1.e
        public void i(Throwable th) {
            add(new e.c(th));
            this.size++;
        }
    }

    public j1(Observable.a<T> aVar, Observable<? extends T> observable, AtomicReference<f<T>> atomicReference, Func0<? extends e<T>> func0) {
        super(aVar);
        this.l = observable;
        this.m = atomicReference;
        this.n = func0;
    }

    @Override // rx.Subscription
    public boolean isUnsubscribed() {
        f<T> fVar = this.m.get();
        return fVar == null || fVar.isUnsubscribed();
    }

    @Override // j0.m.b
    public void k0(Action1<? super Subscription> action1) {
        f<T> fVar;
        while (true) {
            fVar = this.m.get();
            if (fVar != null && !fVar.isUnsubscribed()) {
                break;
            }
            f<T> fVar2 = new f<>(this.n.call());
            fVar2.add(new j0.r.a(new m1(fVar2)));
            if (this.m.compareAndSet(fVar, fVar2)) {
                fVar = fVar2;
                break;
            }
        }
        boolean z2 = true;
        if (fVar.r.get() || !fVar.r.compareAndSet(false, true)) {
            z2 = false;
        }
        ((z) action1).call(fVar);
        if (z2) {
            this.l.i0(fVar);
        }
    }

    @Override // rx.Subscription
    public void unsubscribe() {
        this.m.lazySet(null);
    }
}
