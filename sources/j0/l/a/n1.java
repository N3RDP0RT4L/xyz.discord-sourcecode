package j0.l.a;

import androidx.recyclerview.widget.RecyclerView;
import b.i.a.f.e.o.f;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.functions.Action0;
import rx.observers.SerializedSubscriber;
/* compiled from: OperatorSampleWithTime.java */
/* loaded from: classes3.dex */
public final class n1<T> implements Observable.b<T, T> {
    public final long j;
    public final TimeUnit k;
    public final Scheduler l;

    /* compiled from: OperatorSampleWithTime.java */
    /* loaded from: classes3.dex */
    public static final class a<T> extends Subscriber<T> implements Action0 {
        public static final Object j = new Object();
        public final Subscriber<? super T> k;
        public final AtomicReference<Object> l = new AtomicReference<>(j);

        public a(Subscriber<? super T> subscriber) {
            this.k = subscriber;
        }

        public final void a() {
            AtomicReference<Object> atomicReference = this.l;
            Object obj = j;
            Object andSet = atomicReference.getAndSet(obj);
            if (andSet != obj) {
                try {
                    this.k.onNext(andSet);
                } catch (Throwable th) {
                    f.o1(th);
                    onError(th);
                }
            }
        }

        @Override // rx.functions.Action0
        public void call() {
            a();
        }

        @Override // j0.g
        public void onCompleted() {
            a();
            this.k.onCompleted();
            unsubscribe();
        }

        @Override // j0.g
        public void onError(Throwable th) {
            this.k.onError(th);
            unsubscribe();
        }

        @Override // j0.g
        public void onNext(T t) {
            this.l.set(t);
        }

        @Override // rx.Subscriber
        public void onStart() {
            request(RecyclerView.FOREVER_NS);
        }
    }

    public n1(long j, TimeUnit timeUnit, Scheduler scheduler) {
        this.j = j;
        this.k = timeUnit;
        this.l = scheduler;
    }

    @Override // j0.k.b
    public Object call(Object obj) {
        Subscriber subscriber = (Subscriber) obj;
        SerializedSubscriber serializedSubscriber = new SerializedSubscriber(subscriber);
        Scheduler.Worker a2 = this.l.a();
        subscriber.add(a2);
        a aVar = new a(serializedSubscriber);
        subscriber.add(aVar);
        long j = this.j;
        a2.c(aVar, j, j, this.k);
        return aVar;
    }
}
