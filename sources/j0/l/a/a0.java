package j0.l.a;

import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;
/* compiled from: OnSubscribeRefCount.java */
/* loaded from: classes3.dex */
public class a0 extends Subscriber<T> {
    public final /* synthetic */ Subscriber j;
    public final /* synthetic */ CompositeSubscription k;
    public final /* synthetic */ c0 l;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public a0(c0 c0Var, Subscriber subscriber, Subscriber subscriber2, CompositeSubscription compositeSubscription) {
        super(subscriber);
        this.l = c0Var;
        this.j = subscriber2;
        this.k = compositeSubscription;
    }

    public void a() {
        this.l.m.lock();
        try {
            if (this.l.k == this.k) {
                Object obj = this.l.j;
                if (obj instanceof Subscription) {
                    ((Subscription) obj).unsubscribe();
                }
                this.l.k.unsubscribe();
                this.l.k = new CompositeSubscription();
                this.l.l.set(0);
            }
        } finally {
            this.l.m.unlock();
        }
    }

    @Override // j0.g
    public void onCompleted() {
        a();
        this.j.onCompleted();
    }

    @Override // j0.g
    public void onError(Throwable th) {
        a();
        this.j.onError(th);
    }

    @Override // j0.g
    public void onNext(T t) {
        this.j.onNext(t);
    }
}
