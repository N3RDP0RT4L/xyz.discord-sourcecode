package j0.l.a;

import b.i.a.f.e.o.f;
import rx.Subscriber;
import rx.functions.Func2;
/* compiled from: OperatorSkipWhile.java */
/* loaded from: classes3.dex */
public class u1 extends Subscriber<T> {
    public boolean j = true;
    public int k;
    public final /* synthetic */ Subscriber l;
    public final /* synthetic */ w1 m;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public u1(w1 w1Var, Subscriber subscriber, Subscriber subscriber2) {
        super(subscriber);
        this.m = w1Var;
        this.l = subscriber2;
    }

    @Override // j0.g
    public void onCompleted() {
        this.l.onCompleted();
    }

    @Override // j0.g
    public void onError(Throwable th) {
        this.l.onError(th);
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // j0.g
    public void onNext(T t) {
        if (!this.j) {
            this.l.onNext(t);
            return;
        }
        try {
            Func2<? super T, Integer, Boolean> func2 = this.m.j;
            int i = this.k;
            this.k = i + 1;
            if (!func2.call(t, Integer.valueOf(i)).booleanValue()) {
                this.j = false;
                this.l.onNext(t);
                return;
            }
            request(1L);
        } catch (Throwable th) {
            f.p1(th, this.l, t);
        }
    }
}
