package j0.l.a;

import androidx.recyclerview.widget.RecyclerView;
import b.i.a.f.e.o.f;
import j0.l.b.b;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import rx.Subscriber;
/* compiled from: OperatorToObservableList.java */
/* loaded from: classes3.dex */
public class m2 extends Subscriber<T> {
    public boolean j;
    public List<T> k = new LinkedList();
    public final /* synthetic */ b l;
    public final /* synthetic */ Subscriber m;

    public m2(n2 n2Var, b bVar, Subscriber subscriber) {
        this.l = bVar;
        this.m = subscriber;
    }

    @Override // j0.g
    public void onCompleted() {
        if (!this.j) {
            this.j = true;
            try {
                ArrayList arrayList = new ArrayList(this.k);
                this.k = null;
                this.l.b(arrayList);
            } catch (Throwable th) {
                f.o1(th);
                onError(th);
            }
        }
    }

    @Override // j0.g
    public void onError(Throwable th) {
        this.m.onError(th);
    }

    @Override // j0.g
    public void onNext(T t) {
        if (!this.j) {
            this.k.add(t);
        }
    }

    @Override // rx.Subscriber
    public void onStart() {
        request(RecyclerView.FOREVER_NS);
    }
}
