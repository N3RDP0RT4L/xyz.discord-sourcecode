package j0.l.a;

import j0.k.b;
import rx.Observable;
/* compiled from: OperatorOnErrorResumeNextViaFunction.java */
/* loaded from: classes3.dex */
public final class d1 implements b<Throwable, Observable<? extends T>> {
    public final /* synthetic */ Observable j;

    public d1(Observable observable) {
        this.j = observable;
    }

    @Override // j0.k.b
    public Object call(Throwable th) {
        return this.j;
    }
}
