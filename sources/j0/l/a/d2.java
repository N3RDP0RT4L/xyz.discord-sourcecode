package j0.l.a;

import rx.Subscriber;
/* compiled from: OperatorTakeUntil.java */
/* loaded from: classes3.dex */
public class d2 extends Subscriber<T> {
    public final /* synthetic */ Subscriber j;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public d2(f2 f2Var, Subscriber subscriber, boolean z2, Subscriber subscriber2) {
        super(subscriber, z2);
        this.j = subscriber2;
    }

    @Override // j0.g
    public void onCompleted() {
        try {
            this.j.onCompleted();
        } finally {
            this.j.unsubscribe();
        }
    }

    @Override // j0.g
    public void onError(Throwable th) {
        try {
            this.j.onError(th);
        } finally {
            this.j.unsubscribe();
        }
    }

    @Override // j0.g
    public void onNext(T t) {
        this.j.onNext(t);
    }
}
