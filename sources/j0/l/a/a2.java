package j0.l.a;

import b.d.b.a.a;
import b.i.a.f.e.o.f;
import j0.l.a.y1;
import rx.Producer;
/* compiled from: OperatorSwitch.java */
/* loaded from: classes3.dex */
public class a2 implements Producer {
    public final /* synthetic */ y1.c j;

    public a2(y1.c cVar) {
        this.j = cVar;
    }

    @Override // rx.Producer
    public void j(long j) {
        Producer producer;
        int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
        if (i > 0) {
            y1.c cVar = this.j;
            synchronized (cVar) {
                producer = cVar.f3755s;
                cVar.r = f.f(cVar.r, j);
            }
            if (producer != null) {
                producer.j(j);
            }
            cVar.b();
        } else if (i < 0) {
            throw new IllegalArgumentException(a.s("n >= 0 expected but it was ", j));
        }
    }
}
