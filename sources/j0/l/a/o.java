package j0.l.a;

import androidx.recyclerview.widget.RecyclerView;
import b.i.a.f.e.o.f;
import java.util.concurrent.atomic.AtomicLong;
import rx.Observable;
import rx.Producer;
import rx.Subscriber;
/* compiled from: OnSubscribeFromArray.java */
/* loaded from: classes3.dex */
public final class o<T> implements Observable.a<T> {
    public final T[] j;

    /* compiled from: OnSubscribeFromArray.java */
    /* loaded from: classes3.dex */
    public static final class a<T> extends AtomicLong implements Producer {
        private static final long serialVersionUID = 3534218984725836979L;
        public final T[] array;
        public final Subscriber<? super T> child;
        public int index;

        public a(Subscriber<? super T> subscriber, T[] tArr) {
            this.child = subscriber;
            this.array = tArr;
        }

        @Override // rx.Producer
        public void j(long j) {
            int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
            if (i < 0) {
                throw new IllegalArgumentException(b.d.b.a.a.s("n >= 0 required but it was ", j));
            } else if (j == RecyclerView.FOREVER_NS) {
                if (f.c0(this, j) == 0) {
                    Subscriber<? super T> subscriber = this.child;
                    for (T t : this.array) {
                        Object obj = (Object) t;
                        if (!subscriber.isUnsubscribed()) {
                            subscriber.onNext(obj);
                        } else {
                            return;
                        }
                    }
                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onCompleted();
                    }
                }
            } else if (i != 0 && f.c0(this, j) == 0) {
                Subscriber<? super T> subscriber2 = this.child;
                T[] tArr = this.array;
                int length = tArr.length;
                int i2 = this.index;
                do {
                    long j2 = 0;
                    while (true) {
                        if (j == 0 || i2 == length) {
                            j = get() + j2;
                            if (j == 0) {
                                this.index = i2;
                                j = addAndGet(j2);
                            }
                        } else if (!subscriber2.isUnsubscribed()) {
                            subscriber2.onNext((Object) tArr[i2]);
                            i2++;
                            if (i2 != length) {
                                j--;
                                j2--;
                            } else if (!subscriber2.isUnsubscribed()) {
                                subscriber2.onCompleted();
                                return;
                            } else {
                                return;
                            }
                        } else {
                            return;
                        }
                    }
                } while (j != 0);
            }
        }
    }

    public o(T[] tArr) {
        this.j = tArr;
    }

    @Override // rx.functions.Action1
    public void call(Object obj) {
        Subscriber subscriber = (Subscriber) obj;
        subscriber.setProducer(new a(subscriber, this.j));
    }
}
