package j0.l.a;

import j0.l.e.o.t;
import j0.l.e.o.y;
import j0.o.l;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import rx.Emitter;
import rx.Observable;
import rx.Producer;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.MissingBackpressureException;
import rx.functions.Action1;
import rx.subscriptions.SerialSubscription;
/* compiled from: OnSubscribeCreate.java */
/* loaded from: classes3.dex */
public final class i<T> implements Observable.a<T> {
    public final Action1<Emitter<T>> j;
    public final Emitter.BackpressureMode k;

    /* compiled from: OnSubscribeCreate.java */
    /* loaded from: classes3.dex */
    public static abstract class a<T> extends AtomicLong implements Emitter<T>, Producer, Subscription {
        private static final long serialVersionUID = 7326289992464377023L;
        public final Subscriber<? super T> actual;
        public final SerialSubscription serial = new SerialSubscription();

        public a(Subscriber<? super T> subscriber) {
            this.actual = subscriber;
        }

        public void a() {
        }

        public void b() {
        }

        @Override // rx.Subscription
        public final boolean isUnsubscribed() {
            return this.serial.isUnsubscribed();
        }

        @Override // rx.Producer
        public final void j(long j) {
            if (b.i.a.f.e.o.f.A1(j)) {
                b.i.a.f.e.o.f.c0(this, j);
                a();
            }
        }

        @Override // j0.g
        public void onCompleted() {
            if (!this.actual.isUnsubscribed()) {
                try {
                    this.actual.onCompleted();
                } finally {
                    this.serial.unsubscribe();
                }
            }
        }

        @Override // j0.g
        public void onError(Throwable th) {
            if (!this.actual.isUnsubscribed()) {
                try {
                    this.actual.onError(th);
                } finally {
                    this.serial.unsubscribe();
                }
            }
        }

        @Override // rx.Subscription
        public final void unsubscribe() {
            this.serial.j.unsubscribe();
            b();
        }
    }

    /* compiled from: OnSubscribeCreate.java */
    /* loaded from: classes3.dex */
    public static final class b<T> extends a<T> {
        private static final long serialVersionUID = 2427151001689639875L;
        public volatile boolean done;
        public Throwable error;
        public final Queue<Object> queue;
        public final AtomicInteger wip;

        public b(Subscriber<? super T> subscriber, int i) {
            super(subscriber);
            this.queue = y.b() ? new t<>(i) : new j0.l.e.n.g<>(i);
            this.wip = new AtomicInteger();
        }

        @Override // j0.l.a.i.a
        public void a() {
            c();
        }

        @Override // j0.l.a.i.a
        public void b() {
            if (this.wip.getAndIncrement() == 0) {
                this.queue.clear();
            }
        }

        public void c() {
            int i;
            if (this.wip.getAndIncrement() == 0) {
                Subscriber<? super T> subscriber = this.actual;
                Queue<Object> queue = this.queue;
                int i2 = 1;
                do {
                    long j = get();
                    long j2 = 0;
                    while (true) {
                        i = (j2 > j ? 1 : (j2 == j ? 0 : -1));
                        if (i == 0) {
                            break;
                        } else if (subscriber.isUnsubscribed()) {
                            queue.clear();
                            return;
                        } else {
                            boolean z2 = this.done;
                            Object poll = queue.poll();
                            boolean z3 = poll == null;
                            if (z2 && z3) {
                                Throwable th = this.error;
                                if (th != null) {
                                    super.onError(th);
                                    return;
                                } else {
                                    super.onCompleted();
                                    return;
                                }
                            } else if (z3) {
                                break;
                            } else {
                                subscriber.onNext((Object) j0.l.a.e.b(poll));
                                j2++;
                            }
                        }
                    }
                    if (i == 0) {
                        if (subscriber.isUnsubscribed()) {
                            queue.clear();
                            return;
                        }
                        boolean z4 = this.done;
                        boolean isEmpty = queue.isEmpty();
                        if (z4 && isEmpty) {
                            Throwable th2 = this.error;
                            if (th2 != null) {
                                super.onError(th2);
                                return;
                            } else {
                                super.onCompleted();
                                return;
                            }
                        }
                    }
                    if (j2 != 0) {
                        b.i.a.f.e.o.f.U0(this, j2);
                    }
                    i2 = this.wip.addAndGet(-i2);
                } while (i2 != 0);
            }
        }

        @Override // j0.l.a.i.a, j0.g
        public void onCompleted() {
            this.done = true;
            c();
        }

        @Override // j0.l.a.i.a, j0.g
        public void onError(Throwable th) {
            this.error = th;
            this.done = true;
            c();
        }

        @Override // j0.g
        public void onNext(T t) {
            Queue<Object> queue = this.queue;
            if (t == null) {
                t = (T) j0.l.a.e.f3743b;
            }
            queue.offer(t);
            c();
        }
    }

    /* compiled from: OnSubscribeCreate.java */
    /* loaded from: classes3.dex */
    public static final class c<T> extends f<T> {
        private static final long serialVersionUID = 8360058422307496563L;

        public c(Subscriber<? super T> subscriber) {
            super(subscriber);
        }

        @Override // j0.l.a.i.f
        public void c() {
        }
    }

    /* compiled from: OnSubscribeCreate.java */
    /* loaded from: classes3.dex */
    public static final class d<T> extends f<T> {
        private static final long serialVersionUID = 338953216916120960L;
        private boolean done;

        public d(Subscriber<? super T> subscriber) {
            super(subscriber);
        }

        @Override // j0.l.a.i.f
        public void c() {
            onError(new MissingBackpressureException("create: could not emit value due to lack of requests"));
        }

        @Override // j0.l.a.i.a, j0.g
        public void onCompleted() {
            if (!this.done) {
                this.done = true;
                super.onCompleted();
            }
        }

        @Override // j0.l.a.i.a, j0.g
        public void onError(Throwable th) {
            if (this.done) {
                l.b(th);
                return;
            }
            this.done = true;
            super.onError(th);
        }

        @Override // j0.l.a.i.f, j0.g
        public void onNext(T t) {
            if (!this.done) {
                super.onNext(t);
            }
        }
    }

    /* compiled from: OnSubscribeCreate.java */
    /* loaded from: classes3.dex */
    public static final class e<T> extends a<T> {
        private static final long serialVersionUID = 4023437720691792495L;
        public volatile boolean done;
        public Throwable error;
        public final AtomicReference<Object> queue = new AtomicReference<>();
        public final AtomicInteger wip = new AtomicInteger();

        public e(Subscriber<? super T> subscriber) {
            super(subscriber);
        }

        @Override // j0.l.a.i.a
        public void a() {
            c();
        }

        @Override // j0.l.a.i.a
        public void b() {
            if (this.wip.getAndIncrement() == 0) {
                this.queue.lazySet(null);
            }
        }

        public void c() {
            boolean z2;
            int i;
            if (this.wip.getAndIncrement() == 0) {
                Subscriber<? super T> subscriber = this.actual;
                AtomicReference<Object> atomicReference = this.queue;
                int i2 = 1;
                do {
                    long j = get();
                    long j2 = 0;
                    while (true) {
                        z2 = false;
                        i = (j2 > j ? 1 : (j2 == j ? 0 : -1));
                        if (i == 0) {
                            break;
                        } else if (subscriber.isUnsubscribed()) {
                            atomicReference.lazySet(null);
                            return;
                        } else {
                            boolean z3 = this.done;
                            Object andSet = atomicReference.getAndSet(null);
                            boolean z4 = andSet == null;
                            if (z3 && z4) {
                                Throwable th = this.error;
                                if (th != null) {
                                    super.onError(th);
                                    return;
                                } else {
                                    super.onCompleted();
                                    return;
                                }
                            } else if (z4) {
                                break;
                            } else {
                                subscriber.onNext((Object) j0.l.a.e.b(andSet));
                                j2++;
                            }
                        }
                    }
                    if (i == 0) {
                        if (subscriber.isUnsubscribed()) {
                            atomicReference.lazySet(null);
                            return;
                        }
                        boolean z5 = this.done;
                        if (atomicReference.get() == null) {
                            z2 = true;
                        }
                        if (z5 && z2) {
                            Throwable th2 = this.error;
                            if (th2 != null) {
                                super.onError(th2);
                                return;
                            } else {
                                super.onCompleted();
                                return;
                            }
                        }
                    }
                    if (j2 != 0) {
                        b.i.a.f.e.o.f.U0(this, j2);
                    }
                    i2 = this.wip.addAndGet(-i2);
                } while (i2 != 0);
            }
        }

        @Override // j0.l.a.i.a, j0.g
        public void onCompleted() {
            this.done = true;
            c();
        }

        @Override // j0.l.a.i.a, j0.g
        public void onError(Throwable th) {
            this.error = th;
            this.done = true;
            c();
        }

        @Override // j0.g
        public void onNext(T t) {
            AtomicReference<Object> atomicReference = this.queue;
            if (t == null) {
                t = (T) j0.l.a.e.f3743b;
            }
            atomicReference.set(t);
            c();
        }
    }

    /* compiled from: OnSubscribeCreate.java */
    /* loaded from: classes3.dex */
    public static abstract class f<T> extends a<T> {
        private static final long serialVersionUID = 4127754106204442833L;

        public f(Subscriber<? super T> subscriber) {
            super(subscriber);
        }

        public abstract void c();

        public void onNext(T t) {
            if (!this.actual.isUnsubscribed()) {
                if (get() != 0) {
                    this.actual.onNext(t);
                    b.i.a.f.e.o.f.U0(this, 1L);
                    return;
                }
                c();
            }
        }
    }

    /* compiled from: OnSubscribeCreate.java */
    /* loaded from: classes3.dex */
    public static final class g<T> extends a<T> {
        private static final long serialVersionUID = 3776720187248809713L;

        public g(Subscriber<? super T> subscriber) {
            super(subscriber);
        }

        @Override // j0.g
        public void onNext(T t) {
            long j;
            if (!this.actual.isUnsubscribed()) {
                this.actual.onNext(t);
                do {
                    j = get();
                    if (j == 0) {
                        return;
                    }
                } while (!compareAndSet(j, j - 1));
            }
        }
    }

    public i(Action1<Emitter<T>> action1, Emitter.BackpressureMode backpressureMode) {
        this.j = action1;
        this.k = backpressureMode;
    }

    @Override // rx.functions.Action1
    public void call(Object obj) {
        a aVar;
        Subscriber subscriber = (Subscriber) obj;
        int ordinal = this.k.ordinal();
        if (ordinal == 0) {
            aVar = new g(subscriber);
        } else if (ordinal == 1) {
            aVar = new d(subscriber);
        } else if (ordinal == 3) {
            aVar = new c(subscriber);
        } else if (ordinal != 4) {
            aVar = new b(subscriber, j0.l.e.i.j);
        } else {
            aVar = new e(subscriber);
        }
        subscriber.add(aVar);
        subscriber.setProducer(aVar);
        this.j.call(aVar);
    }
}
