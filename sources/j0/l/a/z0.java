package j0.l.a;

import b.i.a.f.e.o.f;
import j0.l.c.m;
import j0.l.e.i;
import j0.l.e.n.c;
import j0.l.e.o.n;
import j0.l.e.o.y;
import j0.o.l;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicLong;
import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.exceptions.MissingBackpressureException;
import rx.functions.Action0;
/* compiled from: OperatorObserveOn.java */
/* loaded from: classes3.dex */
public final class z0<T> implements Observable.b<T, T> {
    public final Scheduler j;
    public final boolean k;
    public final int l;

    /* compiled from: OperatorObserveOn.java */
    /* loaded from: classes3.dex */
    public static final class a<T> extends Subscriber<T> implements Action0 {
        public final Subscriber<? super T> j;
        public final Scheduler.Worker k;
        public final boolean l;
        public final Queue<Object> m;
        public final int n;
        public volatile boolean o;
        public final AtomicLong p = new AtomicLong();
        public final AtomicLong q = new AtomicLong();
        public Throwable r;

        /* renamed from: s  reason: collision with root package name */
        public long f3756s;

        public a(Scheduler scheduler, Subscriber<? super T> subscriber, boolean z2, int i) {
            this.j = subscriber;
            this.k = scheduler.a();
            this.l = z2;
            i = i <= 0 ? i.j : i;
            this.n = i - (i >> 2);
            if (y.b()) {
                this.m = new n(i);
            } else {
                this.m = new c(i);
            }
            request(i);
        }

        public boolean a(boolean z2, boolean z3, Subscriber<? super T> subscriber, Queue<Object> queue) {
            if (subscriber.isUnsubscribed()) {
                queue.clear();
                return true;
            } else if (!z2) {
                return false;
            } else {
                if (!this.l) {
                    Throwable th = this.r;
                    if (th != null) {
                        queue.clear();
                        try {
                            subscriber.onError(th);
                            return true;
                        } finally {
                        }
                    } else if (!z3) {
                        return false;
                    } else {
                        try {
                            subscriber.onCompleted();
                            return true;
                        } finally {
                        }
                    }
                } else if (!z3) {
                    return false;
                } else {
                    Throwable th2 = this.r;
                    try {
                        if (th2 != null) {
                            subscriber.onError(th2);
                        } else {
                            subscriber.onCompleted();
                        }
                        return false;
                    } finally {
                    }
                }
            }
        }

        public void b() {
            if (this.q.getAndIncrement() == 0) {
                this.k.a(this);
            }
        }

        @Override // rx.functions.Action0
        public void call() {
            int i;
            long j = this.f3756s;
            Queue<Object> queue = this.m;
            Subscriber<? super T> subscriber = this.j;
            long j2 = 1;
            do {
                long j3 = this.p.get();
                while (true) {
                    i = (j3 > j ? 1 : (j3 == j ? 0 : -1));
                    if (i == 0) {
                        break;
                    }
                    boolean z2 = this.o;
                    Object poll = queue.poll();
                    boolean z3 = poll == null;
                    if (!a(z2, z3, subscriber, queue)) {
                        if (z3) {
                            break;
                        }
                        subscriber.onNext((Object) e.b(poll));
                        j++;
                        if (j == this.n) {
                            j3 = f.U0(this.p, j);
                            request(j);
                            j = 0;
                        }
                    } else {
                        return;
                    }
                }
                if (i != 0 || !a(this.o, queue.isEmpty(), subscriber, queue)) {
                    this.f3756s = j;
                    j2 = this.q.addAndGet(-j2);
                } else {
                    return;
                }
            } while (j2 != 0);
        }

        @Override // j0.g
        public void onCompleted() {
            if (!isUnsubscribed() && !this.o) {
                this.o = true;
                b();
            }
        }

        @Override // j0.g
        public void onError(Throwable th) {
            if (isUnsubscribed() || this.o) {
                l.b(th);
                return;
            }
            this.r = th;
            this.o = true;
            b();
        }

        @Override // j0.g
        public void onNext(T t) {
            if (!isUnsubscribed() && !this.o) {
                Queue<Object> queue = this.m;
                if (t == null) {
                    t = (T) e.f3743b;
                }
                if (!queue.offer(t)) {
                    onError(new MissingBackpressureException());
                } else {
                    b();
                }
            }
        }
    }

    public z0(Scheduler scheduler, boolean z2, int i) {
        this.j = scheduler;
        this.k = z2;
        this.l = i <= 0 ? i.j : i;
    }

    @Override // j0.k.b
    public Object call(Object obj) {
        Subscriber subscriber = (Subscriber) obj;
        Scheduler scheduler = this.j;
        if (scheduler instanceof m) {
            return subscriber;
        }
        a aVar = new a(scheduler, subscriber, this.k, this.l);
        Subscriber<? super T> subscriber2 = aVar.j;
        subscriber2.setProducer(new y0(aVar));
        subscriber2.add(aVar.k);
        subscriber2.add(aVar);
        return aVar;
    }
}
