package j0.l.a;

import j0.h;
import j0.i;
import rx.Observable;
/* compiled from: OnSubscribeSingle.java */
/* loaded from: classes3.dex */
public class e0<T> implements h.a<T> {
    public final Observable<T> j;

    public e0(Observable<T> observable) {
        this.j = observable;
    }

    @Override // rx.functions.Action1
    public void call(Object obj) {
        i iVar = (i) obj;
        d0 d0Var = new d0(this, iVar);
        iVar.j.a(d0Var);
        this.j.i0(d0Var);
    }
}
