package j0.l.a;

import androidx.recyclerview.widget.RecyclerView;
import j0.l.a.e;
import j0.l.e.i;
import j0.l.e.n.c;
import j0.l.e.o.n;
import j0.l.e.o.y;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import rx.Observable;
import rx.Producer;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.MissingBackpressureException;
import rx.functions.Action1;
/* compiled from: OperatorPublish.java */
/* loaded from: classes3.dex */
public final class h1<T> extends j0.m.b<T> {
    public final Observable<? extends T> k;
    public final AtomicReference<b<T>> l;

    /* compiled from: OperatorPublish.java */
    /* loaded from: classes3.dex */
    public static final class a<T> extends AtomicLong implements Producer, Subscription {
        private static final long serialVersionUID = -4453897557930727610L;
        public final Subscriber<? super T> child;
        public final b<T> parent;

        public a(b<T> bVar, Subscriber<? super T> subscriber) {
            this.parent = bVar;
            this.child = subscriber;
            lazySet(-4611686018427387904L);
        }

        public long a(long j) {
            long j2;
            long j3;
            if (j > 0) {
                do {
                    j2 = get();
                    if (j2 == -4611686018427387904L) {
                        throw new IllegalStateException("Produced without request");
                    } else if (j2 == Long.MIN_VALUE) {
                        return Long.MIN_VALUE;
                    } else {
                        j3 = j2 - j;
                        if (j3 < 0) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("More produced (");
                            sb.append(j);
                            sb.append(") than requested (");
                            throw new IllegalStateException(b.d.b.a.a.B(sb, j2, ")"));
                        }
                    }
                } while (!compareAndSet(j2, j3));
                return j3;
            }
            throw new IllegalArgumentException("Cant produce zero or less");
        }

        @Override // rx.Subscription
        public boolean isUnsubscribed() {
            return get() == Long.MIN_VALUE;
        }

        @Override // rx.Producer
        public void j(long j) {
            long j2;
            long j3;
            int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
            if (i >= 0) {
                do {
                    j2 = get();
                    if (j2 != Long.MIN_VALUE) {
                        if (j2 >= 0 && i == 0) {
                            return;
                        }
                        if (j2 == -4611686018427387904L) {
                            j3 = j;
                        } else {
                            j3 = j2 + j;
                            if (j3 < 0) {
                                j3 = RecyclerView.FOREVER_NS;
                            }
                        }
                    } else {
                        return;
                    }
                } while (!compareAndSet(j2, j3));
                this.parent.b();
            }
        }

        @Override // rx.Subscription
        public void unsubscribe() {
            a[] aVarArr;
            a[] aVarArr2;
            if (get() != Long.MIN_VALUE && getAndSet(Long.MIN_VALUE) != Long.MIN_VALUE) {
                b<T> bVar = this.parent;
                do {
                    aVarArr = bVar.o.get();
                    if (aVarArr != b.j && aVarArr != b.k) {
                        int length = aVarArr.length;
                        int i = 0;
                        while (true) {
                            if (i >= length) {
                                i = -1;
                                break;
                            } else if (aVarArr[i].equals(this)) {
                                break;
                            } else {
                                i++;
                            }
                        }
                        if (i < 0) {
                            break;
                        } else if (length == 1) {
                            aVarArr2 = b.j;
                        } else {
                            a[] aVarArr3 = new a[length - 1];
                            System.arraycopy(aVarArr, 0, aVarArr3, 0, i);
                            System.arraycopy(aVarArr, i + 1, aVarArr3, i, (length - i) - 1);
                            aVarArr2 = aVarArr3;
                        }
                    } else {
                        break;
                    }
                } while (!bVar.o.compareAndSet(aVarArr, aVarArr2));
                this.parent.b();
            }
        }
    }

    /* compiled from: OperatorPublish.java */
    /* loaded from: classes3.dex */
    public static final class b<T> extends Subscriber<T> implements Subscription {
        public static final a[] j = new a[0];
        public static final a[] k = new a[0];
        public final Queue<Object> l;
        public final AtomicReference<b<T>> m;
        public volatile Object n;
        public final AtomicReference<a[]> o;
        public final AtomicBoolean p;
        public boolean q;
        public boolean r;

        public b(AtomicReference<b<T>> atomicReference) {
            this.l = y.b() ? new n<>(i.j) : new c<>(i.j);
            this.o = new AtomicReference<>(j);
            this.m = atomicReference;
            this.p = new AtomicBoolean();
        }

        public boolean a(Object obj, boolean z2) {
            int i = 0;
            if (obj != null) {
                if (!e.c(obj)) {
                    Throwable th = ((e.c) obj).e;
                    this.m.compareAndSet(this, null);
                    try {
                        a[] andSet = this.o.getAndSet(k);
                        int length = andSet.length;
                        while (i < length) {
                            andSet[i].child.onError(th);
                            i++;
                        }
                        return true;
                    } finally {
                    }
                } else if (z2) {
                    this.m.compareAndSet(this, null);
                    try {
                        a[] andSet2 = this.o.getAndSet(k);
                        int length2 = andSet2.length;
                        while (i < length2) {
                            andSet2[i].child.onCompleted();
                            i++;
                        }
                        return true;
                    } finally {
                    }
                }
            }
            return false;
        }

        public void b() {
            boolean z2;
            Throwable th;
            Throwable th2;
            long j2;
            synchronized (this) {
                if (this.q) {
                    this.r = true;
                    return;
                }
                this.q = true;
                this.r = false;
                while (true) {
                    try {
                        Object obj = this.n;
                        boolean isEmpty = this.l.isEmpty();
                        if (!a(obj, isEmpty)) {
                            if (!isEmpty) {
                                a[] aVarArr = this.o.get();
                                int length = aVarArr.length;
                                long j3 = RecyclerView.FOREVER_NS;
                                int i = 0;
                                for (a aVar : aVarArr) {
                                    long j4 = aVar.get();
                                    if (j4 >= 0) {
                                        j3 = Math.min(j3, j4);
                                    } else if (j4 == Long.MIN_VALUE) {
                                        i++;
                                    }
                                }
                                if (length != i) {
                                    int i2 = 0;
                                    while (true) {
                                        j2 = i2;
                                        if (j2 >= j3) {
                                            break;
                                        }
                                        Object obj2 = this.n;
                                        Object poll = this.l.poll();
                                        boolean z3 = poll == null;
                                        if (!a(obj2, z3)) {
                                            if (z3) {
                                                isEmpty = z3;
                                                break;
                                            }
                                            Object b2 = e.b(poll);
                                            for (a aVar2 : aVarArr) {
                                                if (aVar2.get() > 0) {
                                                    aVar2.child.onNext(b2);
                                                    aVar2.a(1L);
                                                }
                                            }
                                            i2++;
                                            isEmpty = z3;
                                        } else {
                                            return;
                                        }
                                    }
                                    if (i2 > 0) {
                                        request(j2);
                                    }
                                    if (j3 != 0 && !isEmpty) {
                                    }
                                } else if (!a(this.n, this.l.poll() == null)) {
                                    request(1L);
                                } else {
                                    return;
                                }
                            }
                            synchronized (this) {
                                try {
                                    if (!this.r) {
                                        this.q = false;
                                        try {
                                            return;
                                        } catch (Throwable th3) {
                                            th2 = th3;
                                            z2 = true;
                                            while (true) {
                                                try {
                                                    try {
                                                        break;
                                                    } catch (Throwable th4) {
                                                        th = th4;
                                                        if (!z2) {
                                                            synchronized (this) {
                                                                this.q = false;
                                                            }
                                                        }
                                                        throw th;
                                                    }
                                                } catch (Throwable th5) {
                                                    th2 = th5;
                                                }
                                            }
                                            throw th2;
                                        }
                                    } else {
                                        this.r = false;
                                    }
                                } catch (Throwable th6) {
                                    th2 = th6;
                                    z2 = false;
                                }
                            }
                        } else {
                            return;
                        }
                    } catch (Throwable th7) {
                        th = th7;
                        z2 = false;
                    }
                }
            }
        }

        @Override // j0.g
        public void onCompleted() {
            if (this.n == null) {
                this.n = e.a;
                b();
            }
        }

        @Override // j0.g
        public void onError(Throwable th) {
            if (this.n == null) {
                this.n = new e.c(th);
                b();
            }
        }

        @Override // j0.g
        public void onNext(T t) {
            Queue<Object> queue = this.l;
            if (t == null) {
                t = (T) e.f3743b;
            }
            if (!queue.offer(t)) {
                MissingBackpressureException missingBackpressureException = new MissingBackpressureException();
                if (this.n == null) {
                    this.n = new e.c(missingBackpressureException);
                    b();
                    return;
                }
                return;
            }
            b();
        }

        @Override // rx.Subscriber
        public void onStart() {
            request(i.j);
        }
    }

    public h1(Observable.a<T> aVar, Observable<? extends T> observable, AtomicReference<b<T>> atomicReference) {
        super(aVar);
        this.k = observable;
        this.l = atomicReference;
    }

    @Override // j0.m.b
    public void k0(Action1<? super Subscription> action1) {
        b<T> bVar;
        while (true) {
            bVar = this.l.get();
            if (bVar != null && !bVar.isUnsubscribed()) {
                break;
            }
            b<T> bVar2 = new b<>(this.l);
            bVar2.add(new j0.r.a(new i1(bVar2)));
            if (this.l.compareAndSet(bVar, bVar2)) {
                bVar = bVar2;
                break;
            }
        }
        boolean z2 = true;
        if (bVar.p.get() || !bVar.p.compareAndSet(false, true)) {
            z2 = false;
        }
        ((z) action1).call(bVar);
        if (z2) {
            this.k.i0(bVar);
        }
    }
}
