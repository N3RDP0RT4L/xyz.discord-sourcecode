package j0.l.a;

import b.i.a.f.e.o.f;
import j0.l.b.b;
import java.util.concurrent.Callable;
import rx.Observable;
import rx.Subscriber;
/* compiled from: OnSubscribeFromCallable.java */
/* loaded from: classes3.dex */
public final class p<T> implements Observable.a<T> {
    public final Callable<? extends T> j;

    public p(Callable<? extends T> callable) {
        this.j = callable;
    }

    @Override // rx.functions.Action1
    public void call(Object obj) {
        Subscriber subscriber = (Subscriber) obj;
        b bVar = new b(subscriber);
        subscriber.setProducer(bVar);
        try {
            bVar.b(this.j.call());
        } catch (Throwable th) {
            f.o1(th);
            subscriber.onError(th);
        }
    }
}
