package j0.l.a;

import b.i.a.f.e.o.f;
import j0.l.b.a;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import rx.Producer;
import rx.Scheduler;
import rx.functions.Action0;
/* compiled from: OnSubscribeRedo.java */
/* loaded from: classes3.dex */
public class x implements Producer {
    public final /* synthetic */ AtomicLong j;
    public final /* synthetic */ a k;
    public final /* synthetic */ AtomicBoolean l;
    public final /* synthetic */ Scheduler.Worker m;
    public final /* synthetic */ Action0 n;

    public x(y yVar, AtomicLong atomicLong, a aVar, AtomicBoolean atomicBoolean, Scheduler.Worker worker, Action0 action0) {
        this.j = atomicLong;
        this.k = aVar;
        this.l = atomicBoolean;
        this.m = worker;
        this.n = action0;
    }

    @Override // rx.Producer
    public void j(long j) {
        if (j > 0) {
            f.c0(this.j, j);
            this.k.j(j);
            if (this.l.compareAndSet(true, false)) {
                this.m.a(this.n);
            }
        }
    }
}
