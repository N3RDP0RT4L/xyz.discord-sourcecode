package j0.l.a;

import b.d.b.a.a;
import rx.Observable;
import rx.Subscriber;
/* compiled from: OperatorSkip.java */
/* loaded from: classes3.dex */
public final class t1<T> implements Observable.b<T, T> {
    public final int j;

    public t1(int i) {
        if (i >= 0) {
            this.j = i;
            return;
        }
        throw new IllegalArgumentException(a.p("n >= 0 required but it was ", i));
    }

    @Override // j0.k.b
    public Object call(Object obj) {
        Subscriber subscriber = (Subscriber) obj;
        return new s1(this, subscriber, subscriber);
    }
}
