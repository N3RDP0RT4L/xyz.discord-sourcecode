package j0.l.a;

import rx.Observable;
import rx.Producer;
import rx.Scheduler;
import rx.Subscriber;
import rx.functions.Action0;
/* compiled from: OperatorSubscribeOn.java */
/* loaded from: classes3.dex */
public final class x1<T> implements Observable.a<T> {
    public final Scheduler j;
    public final Observable<T> k;
    public final boolean l;

    /* compiled from: OperatorSubscribeOn.java */
    /* loaded from: classes3.dex */
    public static final class a<T> extends Subscriber<T> implements Action0 {
        public final Subscriber<? super T> j;
        public final boolean k;
        public final Scheduler.Worker l;
        public Observable<T> m;
        public Thread n;

        /* compiled from: OperatorSubscribeOn.java */
        /* renamed from: j0.l.a.x1$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public class C0402a implements Producer {
            public final /* synthetic */ Producer j;

            /* compiled from: OperatorSubscribeOn.java */
            /* renamed from: j0.l.a.x1$a$a$a  reason: collision with other inner class name */
            /* loaded from: classes3.dex */
            public class C0403a implements Action0 {
                public final /* synthetic */ long j;

                public C0403a(long j) {
                    this.j = j;
                }

                @Override // rx.functions.Action0
                public void call() {
                    C0402a.this.j.j(this.j);
                }
            }

            public C0402a(Producer producer) {
                this.j = producer;
            }

            @Override // rx.Producer
            public void j(long j) {
                if (a.this.n != Thread.currentThread()) {
                    a aVar = a.this;
                    if (aVar.k) {
                        aVar.l.a(new C0403a(j));
                        return;
                    }
                }
                this.j.j(j);
            }
        }

        public a(Subscriber<? super T> subscriber, boolean z2, Scheduler.Worker worker, Observable<T> observable) {
            this.j = subscriber;
            this.k = z2;
            this.l = worker;
            this.m = observable;
        }

        @Override // rx.functions.Action0
        public void call() {
            Observable<T> observable = this.m;
            this.m = null;
            this.n = Thread.currentThread();
            observable.i0(this);
        }

        @Override // j0.g
        public void onCompleted() {
            try {
                this.j.onCompleted();
            } finally {
                this.l.unsubscribe();
            }
        }

        @Override // j0.g
        public void onError(Throwable th) {
            try {
                this.j.onError(th);
            } finally {
                this.l.unsubscribe();
            }
        }

        @Override // j0.g
        public void onNext(T t) {
            this.j.onNext(t);
        }

        @Override // rx.Subscriber
        public void setProducer(Producer producer) {
            this.j.setProducer(new C0402a(producer));
        }
    }

    public x1(Observable<T> observable, Scheduler scheduler, boolean z2) {
        this.j = scheduler;
        this.k = observable;
        this.l = z2;
    }

    @Override // rx.functions.Action1
    public void call(Object obj) {
        Subscriber subscriber = (Subscriber) obj;
        Scheduler.Worker a2 = this.j.a();
        a aVar = new a(subscriber, this.l, a2, this.k);
        subscriber.add(aVar);
        subscriber.add(a2);
        a2.a(aVar);
    }
}
