package j0.l.a;

import b.i.a.f.e.o.f;
import j0.l.e.d;
import j0.l.e.k;
import j0.l.e.o.n;
import j0.l.e.o.y;
import j0.o.l;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import rx.Observable;
import rx.Producer;
import rx.Subscriber;
import rx.exceptions.MissingBackpressureException;
import rx.observers.SerializedSubscriber;
import rx.subscriptions.SerialSubscription;
/* compiled from: OnSubscribeConcatMap.java */
/* loaded from: classes3.dex */
public final class h<T, R> implements Observable.a<R> {
    public final Observable<? extends T> j;
    public final j0.k.b<? super T, ? extends Observable<? extends R>> k;

    /* compiled from: OnSubscribeConcatMap.java */
    /* loaded from: classes3.dex */
    public static final class a<T, R> implements Producer {
        public final R j;
        public final c<T, R> k;
        public boolean l;

        public a(R r, c<T, R> cVar) {
            this.j = r;
            this.k = cVar;
        }

        @Override // rx.Producer
        public void j(long j) {
            if (!this.l && j > 0) {
                this.l = true;
                c<T, R> cVar = this.k;
                cVar.j.onNext(this.j);
                cVar.m.b(1L);
                cVar.f3745s = false;
                cVar.a();
            }
        }
    }

    /* compiled from: OnSubscribeConcatMap.java */
    /* loaded from: classes3.dex */
    public static final class b<T, R> extends Subscriber<R> {
        public final c<T, R> j;
        public long k;

        public b(c<T, R> cVar) {
            this.j = cVar;
        }

        @Override // j0.g
        public void onCompleted() {
            c<T, R> cVar = this.j;
            long j = this.k;
            if (j != 0) {
                cVar.m.b(j);
            }
            cVar.f3745s = false;
            cVar.a();
        }

        @Override // j0.g
        public void onError(Throwable th) {
            c<T, R> cVar = this.j;
            long j = this.k;
            if (!d.f(cVar.p, th)) {
                l.b(th);
            } else if (cVar.l == 0) {
                Throwable h = d.h(cVar.p);
                if (!d.g(h)) {
                    cVar.j.onError(h);
                }
                cVar.unsubscribe();
            } else {
                if (j != 0) {
                    cVar.m.b(j);
                }
                cVar.f3745s = false;
                cVar.a();
            }
        }

        @Override // j0.g
        public void onNext(R r) {
            this.k++;
            this.j.j.onNext(r);
        }

        @Override // rx.Subscriber
        public void setProducer(Producer producer) {
            this.j.m.c(producer);
        }
    }

    /* compiled from: OnSubscribeConcatMap.java */
    /* loaded from: classes3.dex */
    public static final class c<T, R> extends Subscriber<T> {
        public final Subscriber<? super R> j;
        public final j0.k.b<? super T, ? extends Observable<? extends R>> k;
        public final int l;
        public final Queue<Object> n;
        public final SerialSubscription q;
        public volatile boolean r;

        /* renamed from: s  reason: collision with root package name */
        public volatile boolean f3745s;
        public final j0.l.b.a m = new j0.l.b.a();
        public final AtomicInteger o = new AtomicInteger();
        public final AtomicReference<Throwable> p = new AtomicReference<>();

        public c(Subscriber<? super R> subscriber, j0.k.b<? super T, ? extends Observable<? extends R>> bVar, int i, int i2) {
            Queue<Object> queue;
            this.j = subscriber;
            this.k = bVar;
            this.l = i2;
            if (y.b()) {
                queue = new n<>(i);
            } else {
                queue = new j0.l.e.n.c<>(i);
            }
            this.n = queue;
            this.q = new SerialSubscription();
            request(i);
        }

        public void a() {
            if (this.o.getAndIncrement() == 0) {
                int i = this.l;
                while (!this.j.isUnsubscribed()) {
                    if (!this.f3745s) {
                        if (i != 1 || this.p.get() == null) {
                            boolean z2 = this.r;
                            Object poll = this.n.poll();
                            boolean z3 = poll == null;
                            if (z2 && z3) {
                                Throwable h = d.h(this.p);
                                if (h == null) {
                                    this.j.onCompleted();
                                    return;
                                } else if (!d.g(h)) {
                                    this.j.onError(h);
                                    return;
                                } else {
                                    return;
                                }
                            } else if (!z3) {
                                try {
                                    Observable<? extends R> call = this.k.call((Object) e.b(poll));
                                    if (call == null) {
                                        b(new NullPointerException("The source returned by the mapper was null"));
                                        return;
                                    } else if (call != j0.l.a.c.k) {
                                        if (call instanceof k) {
                                            this.f3745s = true;
                                            this.m.c(new a(((k) call).l, this));
                                        } else {
                                            b bVar = new b(this);
                                            this.q.a(bVar);
                                            if (!bVar.isUnsubscribed()) {
                                                this.f3745s = true;
                                                call.i0(bVar);
                                            } else {
                                                return;
                                            }
                                        }
                                        request(1L);
                                    } else {
                                        request(1L);
                                    }
                                } catch (Throwable th) {
                                    f.o1(th);
                                    b(th);
                                    return;
                                }
                            }
                        } else {
                            Throwable h2 = d.h(this.p);
                            if (!d.g(h2)) {
                                this.j.onError(h2);
                                return;
                            }
                            return;
                        }
                    }
                    if (this.o.decrementAndGet() == 0) {
                        return;
                    }
                }
            }
        }

        public void b(Throwable th) {
            unsubscribe();
            if (d.f(this.p, th)) {
                Throwable h = d.h(this.p);
                if (!d.g(h)) {
                    this.j.onError(h);
                    return;
                }
                return;
            }
            l.b(th);
        }

        @Override // j0.g
        public void onCompleted() {
            this.r = true;
            a();
        }

        @Override // j0.g
        public void onError(Throwable th) {
            if (d.f(this.p, th)) {
                this.r = true;
                if (this.l == 0) {
                    Throwable h = d.h(this.p);
                    if (!d.g(h)) {
                        this.j.onError(h);
                    }
                    this.q.j.unsubscribe();
                    return;
                }
                a();
                return;
            }
            l.b(th);
        }

        @Override // j0.g
        public void onNext(T t) {
            Queue<Object> queue = this.n;
            if (t == null) {
                t = (T) e.f3743b;
            }
            if (!queue.offer(t)) {
                unsubscribe();
                onError(new MissingBackpressureException());
                return;
            }
            a();
        }
    }

    public h(Observable<? extends T> observable, j0.k.b<? super T, ? extends Observable<? extends R>> bVar, int i, int i2) {
        this.j = observable;
        this.k = bVar;
    }

    @Override // rx.functions.Action1
    public void call(Object obj) {
        Subscriber subscriber = (Subscriber) obj;
        c cVar = new c(new SerializedSubscriber(subscriber), this.k, 2, 0);
        subscriber.add(cVar);
        subscriber.add(cVar.q);
        subscriber.setProducer(new g(this, cVar));
        if (!subscriber.isUnsubscribed()) {
            this.j.i0(cVar);
        }
    }
}
