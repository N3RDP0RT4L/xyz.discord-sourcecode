package j0.l.a;

import j0.k.b;
import rx.functions.Func2;
/* compiled from: OperatorSkipWhile.java */
/* loaded from: classes3.dex */
public final class v1 implements Func2<T, Integer, Boolean> {
    public final /* synthetic */ b j;

    public v1(b bVar) {
        this.j = bVar;
    }

    @Override // rx.functions.Func2
    public Boolean call(Object obj, Integer num) {
        return (Boolean) this.j.call(obj);
    }
}
