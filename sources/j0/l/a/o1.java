package j0.l.a;

import androidx.recyclerview.widget.RecyclerView;
import j0.g;
import j0.l.e.n.f;
import j0.l.e.o.s;
import j0.l.e.o.y;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicLong;
import rx.Observable;
import rx.Producer;
import rx.Subscriber;
import rx.functions.Func0;
import rx.functions.Func2;
/* compiled from: OperatorScan.java */
/* loaded from: classes3.dex */
public final class o1<R, T> implements Observable.b<R, T> {
    public static final Object j = new Object();
    public final Func0<R> k;
    public final Func2<R, ? super T, R> l;

    /* compiled from: OperatorScan.java */
    /* loaded from: classes3.dex */
    public class a implements Func0<R> {
        public final /* synthetic */ Object j;

        public a(Object obj) {
            this.j = obj;
        }

        @Override // rx.functions.Func0, java.util.concurrent.Callable
        public R call() {
            return (R) this.j;
        }
    }

    /* compiled from: OperatorScan.java */
    /* loaded from: classes3.dex */
    public static final class b<R> implements Producer, g<R> {
        public final Subscriber<? super R> j;
        public final Queue<Object> k;
        public boolean l;
        public boolean m;
        public long n;
        public final AtomicLong o;
        public volatile Producer p;
        public volatile boolean q;
        public Throwable r;

        public b(R r, Subscriber<? super R> subscriber) {
            Queue<Object> queue;
            this.j = subscriber;
            if (y.b()) {
                queue = new s<>();
            } else {
                queue = new f<>();
            }
            this.k = queue;
            queue.offer(r == null ? (R) e.f3743b : r);
            this.o = new AtomicLong();
        }

        public boolean a(boolean z2, boolean z3, Subscriber<? super R> subscriber) {
            if (subscriber.isUnsubscribed()) {
                return true;
            }
            if (!z2) {
                return false;
            }
            Throwable th = this.r;
            if (th != null) {
                subscriber.onError(th);
                return true;
            } else if (!z3) {
                return false;
            } else {
                subscriber.onCompleted();
                return true;
            }
        }

        public void b() {
            synchronized (this) {
                if (this.l) {
                    this.m = true;
                    return;
                }
                this.l = true;
                Subscriber<? super R> subscriber = this.j;
                Queue<Object> queue = this.k;
                AtomicLong atomicLong = this.o;
                long j = atomicLong.get();
                while (!a(this.q, queue.isEmpty(), subscriber)) {
                    long j2 = 0;
                    while (j2 != j) {
                        boolean z2 = this.q;
                        Object poll = queue.poll();
                        boolean z3 = poll == null;
                        if (!a(z2, z3, subscriber)) {
                            if (z3) {
                                break;
                            }
                            Object obj = (Object) e.b(poll);
                            try {
                                subscriber.onNext(obj);
                                j2++;
                            } catch (Throwable th) {
                                b.i.a.f.e.o.f.p1(th, subscriber, obj);
                                return;
                            }
                        } else {
                            return;
                        }
                    }
                    if (!(j2 == 0 || j == RecyclerView.FOREVER_NS)) {
                        j = b.i.a.f.e.o.f.U0(atomicLong, j2);
                    }
                    synchronized (this) {
                        if (!this.m) {
                            this.l = false;
                            return;
                        }
                        this.m = false;
                    }
                }
            }
        }

        @Override // rx.Producer
        public void j(long j) {
            int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
            if (i < 0) {
                throw new IllegalArgumentException(b.d.b.a.a.s("n >= required but it was ", j));
            } else if (i != 0) {
                b.i.a.f.e.o.f.c0(this.o, j);
                Producer producer = this.p;
                if (producer == null) {
                    synchronized (this.o) {
                        producer = this.p;
                        if (producer == null) {
                            this.n = b.i.a.f.e.o.f.f(this.n, j);
                        }
                    }
                }
                if (producer != null) {
                    producer.j(j);
                }
                b();
            }
        }

        @Override // j0.g
        public void onCompleted() {
            this.q = true;
            b();
        }

        @Override // j0.g
        public void onError(Throwable th) {
            this.r = th;
            this.q = true;
            b();
        }

        @Override // j0.g
        public void onNext(R r) {
            Queue<Object> queue = this.k;
            if (r == null) {
                r = (R) e.f3743b;
            }
            queue.offer(r);
            b();
        }
    }

    public o1(R r, Func2<R, ? super T, R> func2) {
        this.k = new a(r);
        this.l = func2;
    }

    @Override // j0.k.b
    public Object call(Object obj) {
        Subscriber subscriber = (Subscriber) obj;
        R call = this.k.call();
        if (call == j) {
            return new p1(this, subscriber, subscriber);
        }
        b bVar = new b(call, subscriber);
        q1 q1Var = new q1(this, call, bVar);
        subscriber.add(q1Var);
        subscriber.setProducer(bVar);
        return q1Var;
    }
}
