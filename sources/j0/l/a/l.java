package j0.l.a;

import b.i.a.f.e.o.f;
import j0.k.b;
import rx.Observable;
import rx.Producer;
import rx.Subscriber;
import rx.exceptions.OnErrorThrowable;
/* compiled from: OnSubscribeFilter.java */
/* loaded from: classes3.dex */
public final class l<T> implements Observable.a<T> {
    public final Observable<T> j;
    public final b<? super T, Boolean> k;

    /* compiled from: OnSubscribeFilter.java */
    /* loaded from: classes3.dex */
    public static final class a<T> extends Subscriber<T> {
        public final Subscriber<? super T> j;
        public final b<? super T, Boolean> k;
        public boolean l;

        public a(Subscriber<? super T> subscriber, b<? super T, Boolean> bVar) {
            this.j = subscriber;
            this.k = bVar;
            request(0L);
        }

        @Override // j0.g
        public void onCompleted() {
            if (!this.l) {
                this.j.onCompleted();
            }
        }

        @Override // j0.g
        public void onError(Throwable th) {
            if (this.l) {
                j0.o.l.b(th);
                return;
            }
            this.l = true;
            this.j.onError(th);
        }

        @Override // j0.g
        public void onNext(T t) {
            try {
                if (this.k.call(t).booleanValue()) {
                    this.j.onNext(t);
                } else {
                    request(1L);
                }
            } catch (Throwable th) {
                f.o1(th);
                unsubscribe();
                onError(OnErrorThrowable.a(th, t));
            }
        }

        @Override // rx.Subscriber
        public void setProducer(Producer producer) {
            super.setProducer(producer);
            this.j.setProducer(producer);
        }
    }

    public l(Observable<T> observable, b<? super T, Boolean> bVar) {
        this.j = observable;
        this.k = bVar;
    }

    @Override // rx.functions.Action1
    public void call(Object obj) {
        Subscriber subscriber = (Subscriber) obj;
        a aVar = new a(subscriber, this.k);
        subscriber.add(aVar);
        this.j.i0(aVar);
    }
}
