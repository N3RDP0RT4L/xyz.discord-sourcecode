package j0.l.a;

import j0.o.l;
import rx.Subscriber;
/* compiled from: DeferredScalarSubscriberSafe.java */
/* loaded from: classes3.dex */
public abstract class b<T, R> extends a<T, R> {
    public boolean n;

    public b(Subscriber<? super R> subscriber) {
        super(subscriber);
    }

    @Override // j0.g
    public void onCompleted() {
        if (!this.n) {
            this.n = true;
            if (this.k) {
                R r = this.l;
                Subscriber<? super R> subscriber = this.j;
                do {
                    int i = this.m.get();
                    if (i != 2 && i != 3 && !subscriber.isUnsubscribed()) {
                        if (i == 1) {
                            subscriber.onNext(r);
                            if (!subscriber.isUnsubscribed()) {
                                subscriber.onCompleted();
                            }
                            this.m.lazySet(3);
                            return;
                        }
                        this.l = r;
                    } else {
                        return;
                    }
                } while (!this.m.compareAndSet(0, 2));
                return;
            }
            this.j.onCompleted();
        }
    }

    @Override // j0.g
    public void onError(Throwable th) {
        if (!this.n) {
            this.n = true;
            this.l = null;
            this.j.onError(th);
            return;
        }
        l.b(th);
    }
}
