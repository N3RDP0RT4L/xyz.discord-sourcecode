package j0.l.a;

import androidx.recyclerview.widget.RecyclerView;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import rx.Producer;
import rx.Subscriber;
/* compiled from: DeferredScalarSubscriber.java */
/* loaded from: classes3.dex */
public abstract class a<T, R> extends Subscriber<T> {
    public final Subscriber<? super R> j;
    public boolean k;
    public R l;
    public final AtomicInteger m = new AtomicInteger();

    /* compiled from: DeferredScalarSubscriber.java */
    /* renamed from: j0.l.a.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0400a implements Producer {
        public final a<?, ?> j;

        public C0400a(a<?, ?> aVar) {
            this.j = aVar;
        }

        @Override // rx.Producer
        public void j(long j) {
            a<?, ?> aVar = this.j;
            Objects.requireNonNull(aVar);
            int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
            if (i < 0) {
                throw new IllegalArgumentException(b.d.b.a.a.s("n >= 0 required but it was ", j));
            } else if (i != 0) {
                Subscriber<? super Object> subscriber = aVar.j;
                do {
                    int i2 = aVar.m.get();
                    if (i2 != 1 && i2 != 3 && !subscriber.isUnsubscribed()) {
                        if (i2 == 2) {
                            if (aVar.m.compareAndSet(2, 3)) {
                                subscriber.onNext((R) aVar.l);
                                if (!subscriber.isUnsubscribed()) {
                                    subscriber.onCompleted();
                                    return;
                                }
                                return;
                            }
                            return;
                        }
                    } else {
                        return;
                    }
                } while (!aVar.m.compareAndSet(0, 1));
            }
        }
    }

    public a(Subscriber<? super R> subscriber) {
        this.j = subscriber;
    }

    @Override // rx.Subscriber
    public final void setProducer(Producer producer) {
        producer.j(RecyclerView.FOREVER_NS);
    }
}
