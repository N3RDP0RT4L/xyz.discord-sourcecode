package j0.l.a;

import j0.n.e;
import j0.r.a;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Action0;
/* compiled from: OperatorDoOnUnsubscribe.java */
/* loaded from: classes3.dex */
public class w0<T> implements Observable.b<T, T> {
    public final Action0 j;

    public w0(Action0 action0) {
        this.j = action0;
    }

    @Override // j0.k.b
    public Object call(Object obj) {
        Subscriber subscriber = (Subscriber) obj;
        subscriber.add(new a(this.j));
        return new e(subscriber, subscriber);
    }
}
