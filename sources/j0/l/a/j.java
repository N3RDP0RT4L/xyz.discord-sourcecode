package j0.l.a;

import b.i.a.f.e.o.f;
import j0.n.e;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Func0;
/* compiled from: OnSubscribeDefer.java */
/* loaded from: classes3.dex */
public final class j<T> implements Observable.a<T> {
    public final Func0<? extends Observable<? extends T>> j;

    public j(Func0<? extends Observable<? extends T>> func0) {
        this.j = func0;
    }

    @Override // rx.functions.Action1
    public void call(Object obj) {
        Subscriber subscriber = (Subscriber) obj;
        try {
            this.j.call().i0(new e(subscriber, subscriber));
        } catch (Throwable th) {
            f.o1(th);
            subscriber.onError(th);
        }
    }
}
