package j0.l.a;

import j0.n.e;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Action0;
/* compiled from: OperatorDoOnSubscribe.java */
/* loaded from: classes3.dex */
public class v0<T> implements Observable.b<T, T> {
    public final Action0 j;

    public v0(Action0 action0) {
        this.j = action0;
    }

    @Override // j0.k.b
    public Object call(Object obj) {
        Subscriber subscriber = (Subscriber) obj;
        this.j.call();
        return new e(subscriber, subscriber);
    }
}
