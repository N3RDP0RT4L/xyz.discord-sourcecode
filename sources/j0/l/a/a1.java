package j0.l.a;

import androidx.recyclerview.widget.RecyclerView;
import j0.a;
import j0.l.e.c;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Action0;
/* compiled from: OperatorOnBackpressureBuffer.java */
/* loaded from: classes3.dex */
public class a1<T> implements Observable.b<T, T> {
    public final a.b j = a.C0397a.a;

    /* compiled from: OperatorOnBackpressureBuffer.java */
    /* loaded from: classes3.dex */
    public static final class a<T> extends Subscriber<T> implements c.a {
        public final Subscriber<? super T> l;
        public final a.b p;
        public final ConcurrentLinkedQueue<Object> j = new ConcurrentLinkedQueue<>();
        public final AtomicBoolean m = new AtomicBoolean(false);
        public final AtomicLong k = null;
        public final Action0 o = null;
        public final c n = new c(this);

        public a(Subscriber<? super T> subscriber, Long l, Action0 action0, a.b bVar) {
            this.l = subscriber;
            this.p = bVar;
        }

        @Override // j0.g
        public void onCompleted() {
            if (!this.m.get()) {
                c cVar = this.n;
                cVar.terminated = true;
                cVar.a();
            }
        }

        @Override // j0.g
        public void onError(Throwable th) {
            if (!this.m.get()) {
                c cVar = this.n;
                if (!cVar.terminated) {
                    cVar.exception = th;
                    cVar.terminated = true;
                    cVar.a();
                }
            }
        }

        /* JADX WARN: Removed duplicated region for block: B:39:0x0045 A[EXC_TOP_SPLITTER, SYNTHETIC] */
        /* JADX WARN: Removed duplicated region for block: B:42:0x006b A[EDGE_INSN: B:42:0x006b->B:30:0x006b ?: BREAK  , SYNTHETIC] */
        @Override // j0.g
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public void onNext(T r8) {
            /*
                r7 = this;
                java.util.concurrent.atomic.AtomicLong r0 = r7.k
                r1 = 0
                r2 = 1
                if (r0 != 0) goto L8
            L6:
                r1 = 1
                goto L6b
            L8:
                java.util.concurrent.atomic.AtomicLong r0 = r7.k
                long r3 = r0.get()
                r5 = 0
                int r0 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r0 > 0) goto L5e
                j0.a$b r0 = r7.p     // Catch: rx.exceptions.MissingBackpressureException -> L2f
                boolean r0 = r0.a()     // Catch: rx.exceptions.MissingBackpressureException -> L2f
                if (r0 == 0) goto L40
                java.util.concurrent.ConcurrentLinkedQueue<java.lang.Object> r0 = r7.j     // Catch: rx.exceptions.MissingBackpressureException -> L2f
                java.lang.Object r0 = r0.poll()     // Catch: rx.exceptions.MissingBackpressureException -> L2f
                java.util.concurrent.atomic.AtomicLong r5 = r7.k     // Catch: rx.exceptions.MissingBackpressureException -> L2f
                if (r5 == 0) goto L2b
                if (r0 == 0) goto L2b
                r5.incrementAndGet()     // Catch: rx.exceptions.MissingBackpressureException -> L2f
            L2b:
                if (r0 == 0) goto L40
                r0 = 1
                goto L41
            L2f:
                r0 = move-exception
                java.util.concurrent.atomic.AtomicBoolean r5 = r7.m
                boolean r5 = r5.compareAndSet(r1, r2)
                if (r5 == 0) goto L40
                r7.unsubscribe()
                rx.Subscriber<? super T> r5 = r7.l
                r5.onError(r0)
            L40:
                r0 = 0
            L41:
                rx.functions.Action0 r5 = r7.o
                if (r5 == 0) goto L5b
                r5.call()     // Catch: java.lang.Throwable -> L49
                goto L5b
            L49:
                r0 = move-exception
                b.i.a.f.e.o.f.o1(r0)
                j0.l.e.c r3 = r7.n
                boolean r4 = r3.terminated
                if (r4 != 0) goto L6b
                r3.exception = r0
                r3.terminated = r2
                r3.a()
                goto L6b
            L5b:
                if (r0 != 0) goto L5e
                goto L6b
            L5e:
                java.util.concurrent.atomic.AtomicLong r0 = r7.k
                r5 = 1
                long r5 = r3 - r5
                boolean r0 = r0.compareAndSet(r3, r5)
                if (r0 == 0) goto L8
                goto L6
            L6b:
                if (r1 != 0) goto L6e
                return
            L6e:
                java.util.concurrent.ConcurrentLinkedQueue<java.lang.Object> r0 = r7.j
                if (r8 != 0) goto L74
                java.lang.Object r8 = j0.l.a.e.f3743b
            L74:
                r0.offer(r8)
                j0.l.e.c r8 = r7.n
                r8.a()
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: j0.l.a.a1.a.onNext(java.lang.Object):void");
        }

        @Override // rx.Subscriber
        public void onStart() {
            request(RecyclerView.FOREVER_NS);
        }
    }

    /* compiled from: OperatorOnBackpressureBuffer.java */
    /* loaded from: classes3.dex */
    public static final class b {
        public static final a1<?> a = new a1<>();
    }

    public a1() {
        int i = j0.a.a;
    }

    @Override // j0.k.b
    public Object call(Object obj) {
        Subscriber subscriber = (Subscriber) obj;
        a aVar = new a(subscriber, null, null, this.j);
        subscriber.add(aVar);
        subscriber.setProducer(aVar.n);
        return aVar;
    }
}
