package j0.l.a;

import b.i.a.f.e.o.f;
import j0.k.b;
import j0.o.l;
import rx.Observable;
import rx.Subscriber;
/* compiled from: OnSubscribeLift.java */
/* loaded from: classes3.dex */
public final class r<T, R> implements Observable.a<R> {
    public final Observable.a<T> j;
    public final Observable.b<? extends R, ? super T> k;

    public r(Observable.a<T> aVar, Observable.b<? extends R, ? super T> bVar) {
        this.j = aVar;
        this.k = bVar;
    }

    @Override // rx.functions.Action1
    public void call(Object obj) {
        Subscriber subscriber = (Subscriber) obj;
        try {
            Observable.b bVar = this.k;
            b<Observable.b, Observable.b> bVar2 = l.i;
            if (bVar2 != null) {
                bVar = bVar2.call(bVar);
            }
            Subscriber subscriber2 = (Subscriber) bVar.call(subscriber);
            subscriber2.onStart();
            this.j.call(subscriber2);
        } catch (Throwable th) {
            f.o1(th);
            subscriber.onError(th);
        }
    }
}
