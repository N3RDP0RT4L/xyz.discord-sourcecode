package j0.l.a;

import j0.i;
import java.util.NoSuchElementException;
import rx.Subscriber;
/* compiled from: OnSubscribeSingle.java */
/* loaded from: classes3.dex */
public class d0 extends Subscriber<T> {
    public boolean j;
    public boolean k;
    public T l;
    public final /* synthetic */ i m;

    public d0(e0 e0Var, i iVar) {
        this.m = iVar;
    }

    @Override // j0.g
    public void onCompleted() {
        if (!this.j) {
            if (this.k) {
                this.m.b(this.l);
            } else {
                this.m.a(new NoSuchElementException("Observable emitted no items"));
            }
        }
    }

    @Override // j0.g
    public void onError(Throwable th) {
        this.m.a(th);
        unsubscribe();
    }

    @Override // j0.g
    public void onNext(T t) {
        if (this.k) {
            this.j = true;
            this.m.a(new IllegalArgumentException("Observable emitted too many elements"));
            unsubscribe();
            return;
        }
        this.k = true;
        this.l = t;
    }

    @Override // rx.Subscriber
    public void onStart() {
        request(2L);
    }
}
