package j0.l.a;

import b.i.a.f.e.o.f;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.functions.Action0;
import rx.observers.SerializedSubscriber;
/* compiled from: OperatorBufferWithTime.java */
/* loaded from: classes3.dex */
public final class n0<T> implements Observable.b<List<T>, T> {
    public final long j;
    public final long k;
    public final TimeUnit l;
    public final int m;
    public final Scheduler n;

    /* compiled from: OperatorBufferWithTime.java */
    /* loaded from: classes3.dex */
    public final class a extends Subscriber<T> {
        public final Subscriber<? super List<T>> j;
        public final Scheduler.Worker k;
        public List<T> l = new ArrayList();
        public boolean m;

        public a(Subscriber<? super List<T>> subscriber, Scheduler.Worker worker) {
            this.j = subscriber;
            this.k = worker;
        }

        @Override // j0.g
        public void onCompleted() {
            try {
                this.k.unsubscribe();
                synchronized (this) {
                    if (!this.m) {
                        this.m = true;
                        List<T> list = this.l;
                        this.l = null;
                        this.j.onNext(list);
                        this.j.onCompleted();
                        unsubscribe();
                    }
                }
            } catch (Throwable th) {
                Subscriber<? super List<T>> subscriber = this.j;
                f.o1(th);
                subscriber.onError(th);
            }
        }

        @Override // j0.g
        public void onError(Throwable th) {
            synchronized (this) {
                if (!this.m) {
                    this.m = true;
                    this.l = null;
                    this.j.onError(th);
                    unsubscribe();
                }
            }
        }

        @Override // j0.g
        public void onNext(T t) {
            List<T> list;
            synchronized (this) {
                if (!this.m) {
                    this.l.add(t);
                    if (this.l.size() == n0.this.m) {
                        list = this.l;
                        this.l = new ArrayList();
                    } else {
                        list = null;
                    }
                    if (list != null) {
                        this.j.onNext(list);
                    }
                }
            }
        }
    }

    /* compiled from: OperatorBufferWithTime.java */
    /* loaded from: classes3.dex */
    public final class b extends Subscriber<T> {
        public final Subscriber<? super List<T>> j;
        public final Scheduler.Worker k;
        public final List<List<T>> l = new LinkedList();
        public boolean m;

        /* compiled from: OperatorBufferWithTime.java */
        /* loaded from: classes3.dex */
        public class a implements Action0 {
            public final /* synthetic */ List j;

            public a(List list) {
                this.j = list;
            }

            @Override // rx.functions.Action0
            public void call() {
                boolean z2;
                b bVar = b.this;
                List<T> list = this.j;
                synchronized (bVar) {
                    if (!bVar.m) {
                        Iterator<List<T>> it = bVar.l.iterator();
                        while (true) {
                            if (it.hasNext()) {
                                if (it.next() == list) {
                                    it.remove();
                                    z2 = true;
                                    break;
                                }
                            } else {
                                z2 = false;
                                break;
                            }
                        }
                        if (z2) {
                            try {
                                bVar.j.onNext(list);
                            } catch (Throwable th) {
                                f.o1(th);
                                bVar.onError(th);
                            }
                        }
                    }
                }
            }
        }

        public b(Subscriber<? super List<T>> subscriber, Scheduler.Worker worker) {
            this.j = subscriber;
            this.k = worker;
        }

        public void a() {
            ArrayList arrayList = new ArrayList();
            synchronized (this) {
                if (!this.m) {
                    this.l.add(arrayList);
                    Scheduler.Worker worker = this.k;
                    a aVar = new a(arrayList);
                    n0 n0Var = n0.this;
                    worker.b(aVar, n0Var.j, n0Var.l);
                }
            }
        }

        @Override // j0.g
        public void onCompleted() {
            try {
                synchronized (this) {
                    if (!this.m) {
                        this.m = true;
                        LinkedList<List> linkedList = new LinkedList(this.l);
                        this.l.clear();
                        for (List list : linkedList) {
                            this.j.onNext(list);
                        }
                        this.j.onCompleted();
                        unsubscribe();
                    }
                }
            } catch (Throwable th) {
                Subscriber<? super List<T>> subscriber = this.j;
                f.o1(th);
                subscriber.onError(th);
            }
        }

        @Override // j0.g
        public void onError(Throwable th) {
            synchronized (this) {
                if (!this.m) {
                    this.m = true;
                    this.l.clear();
                    this.j.onError(th);
                    unsubscribe();
                }
            }
        }

        @Override // j0.g
        public void onNext(T t) {
            synchronized (this) {
                if (!this.m) {
                    Iterator<List<T>> it = this.l.iterator();
                    LinkedList<List> linkedList = null;
                    while (it.hasNext()) {
                        List<T> next = it.next();
                        next.add(t);
                        if (next.size() == n0.this.m) {
                            it.remove();
                            if (linkedList == null) {
                                linkedList = new LinkedList();
                            }
                            linkedList.add(next);
                        }
                    }
                    if (linkedList != null) {
                        for (List list : linkedList) {
                            this.j.onNext(list);
                        }
                    }
                }
            }
        }
    }

    public n0(long j, long j2, TimeUnit timeUnit, int i, Scheduler scheduler) {
        this.j = j;
        this.k = j2;
        this.l = timeUnit;
        this.m = i;
        this.n = scheduler;
    }

    @Override // j0.k.b
    public Object call(Object obj) {
        Subscriber subscriber = (Subscriber) obj;
        Scheduler.Worker a2 = this.n.a();
        SerializedSubscriber serializedSubscriber = new SerializedSubscriber(subscriber);
        if (this.j == this.k) {
            a aVar = new a(serializedSubscriber, a2);
            aVar.add(a2);
            subscriber.add(aVar);
            Scheduler.Worker worker = aVar.k;
            m0 m0Var = new m0(aVar);
            long j = this.j;
            worker.c(m0Var, j, j, this.l);
            return aVar;
        }
        b bVar = new b(serializedSubscriber, a2);
        bVar.add(a2);
        subscriber.add(bVar);
        bVar.a();
        Scheduler.Worker worker2 = bVar.k;
        o0 o0Var = new o0(bVar);
        long j2 = this.k;
        worker2.c(o0Var, j2, j2, this.l);
        return bVar;
    }
}
