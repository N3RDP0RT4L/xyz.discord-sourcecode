package j0.l.a;

import j0.l.e.i;
import j0.l.e.n.e;
import j0.o.l;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import rx.Observable;
import rx.Producer;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.CompositeException;
import rx.functions.FuncN;
/* compiled from: OnSubscribeCombineLatest.java */
/* loaded from: classes3.dex */
public final class f<T, R> implements Observable.a<R> {
    public final Iterable<? extends Observable<? extends T>> j;
    public final FuncN<? extends R> k;
    public final int l;

    /* compiled from: OnSubscribeCombineLatest.java */
    /* loaded from: classes3.dex */
    public static final class a<T, R> extends Subscriber<T> {
        public final b<T, R> j;
        public final int k;
        public boolean l;

        public a(b<T, R> bVar, int i) {
            this.j = bVar;
            this.k = i;
            request(bVar.bufferSize);
        }

        @Override // j0.g
        public void onCompleted() {
            if (!this.l) {
                this.l = true;
                this.j.c(null, this.k);
            }
        }

        @Override // j0.g
        public void onError(Throwable th) {
            Throwable th2;
            Throwable th3;
            if (this.l) {
                l.b(th);
                return;
            }
            AtomicReference<Throwable> atomicReference = this.j.error;
            do {
                th2 = atomicReference.get();
                if (th2 == null) {
                    th3 = th;
                } else if (th2 instanceof CompositeException) {
                    ArrayList arrayList = new ArrayList(((CompositeException) th2).b());
                    arrayList.add(th);
                    th3 = new CompositeException(arrayList);
                } else {
                    th3 = new CompositeException(Arrays.asList(th2, th));
                }
            } while (!atomicReference.compareAndSet(th2, th3));
            this.l = true;
            this.j.c(null, this.k);
        }

        @Override // j0.g
        public void onNext(T t) {
            if (!this.l) {
                b<T, R> bVar = this.j;
                if (t == null) {
                    t = (T) e.f3743b;
                }
                bVar.c(t, this.k);
            }
        }
    }

    /* compiled from: OnSubscribeCombineLatest.java */
    /* loaded from: classes3.dex */
    public static final class b<T, R> extends AtomicInteger implements Producer, Subscription {
        public static final Object j = new Object();
        private static final long serialVersionUID = 8567835998786448817L;
        public int active;
        public final Subscriber<? super R> actual;
        public final int bufferSize;
        public volatile boolean cancelled;
        public final FuncN<? extends R> combiner;
        public int complete;
        public final boolean delayError;
        public volatile boolean done;
        public final Object[] latest;
        public final e<Object> queue;
        public final a<T, R>[] subscribers;
        public final AtomicLong requested = new AtomicLong();
        public final AtomicReference<Throwable> error = new AtomicReference<>();

        public b(Subscriber<? super R> subscriber, FuncN<? extends R> funcN, int i, int i2, boolean z2) {
            this.actual = subscriber;
            this.combiner = funcN;
            this.bufferSize = i2;
            this.delayError = z2;
            Object[] objArr = new Object[i];
            this.latest = objArr;
            Arrays.fill(objArr, j);
            this.subscribers = new a[i];
            this.queue = new e<>(i2);
        }

        public void a(Queue<?> queue) {
            queue.clear();
            for (a<T, R> aVar : this.subscribers) {
                aVar.unsubscribe();
            }
        }

        public boolean b(boolean z2, boolean z3, Subscriber<?> subscriber, Queue<?> queue, boolean z4) {
            if (this.cancelled) {
                a(queue);
                return true;
            } else if (!z2) {
                return false;
            } else {
                if (!z4) {
                    Throwable th = this.error.get();
                    if (th != null) {
                        a(queue);
                        subscriber.onError(th);
                        return true;
                    } else if (!z3) {
                        return false;
                    } else {
                        subscriber.onCompleted();
                        return true;
                    }
                } else if (!z3) {
                    return false;
                } else {
                    Throwable th2 = this.error.get();
                    if (th2 != null) {
                        subscriber.onError(th2);
                    } else {
                        subscriber.onCompleted();
                    }
                    return true;
                }
            }
        }

        public void c(Object obj, int i) {
            boolean z2;
            a<T, R> aVar = this.subscribers[i];
            synchronized (this) {
                Object[] objArr = this.latest;
                int length = objArr.length;
                Object obj2 = objArr[i];
                int i2 = this.active;
                Object obj3 = j;
                if (obj2 == obj3) {
                    i2++;
                    this.active = i2;
                }
                int i3 = this.complete;
                if (obj == null) {
                    i3++;
                    this.complete = i3;
                } else {
                    objArr[i] = e.b(obj);
                }
                boolean z3 = false;
                z2 = i2 == length;
                if (i3 == length || (obj == null && obj2 == obj3)) {
                    z3 = true;
                }
                if (z3) {
                    this.done = true;
                } else if (obj != null && z2) {
                    this.queue.e(aVar, this.latest.clone());
                } else if (obj == null && this.error.get() != null && (obj2 == obj3 || !this.delayError)) {
                    this.done = true;
                }
            }
            if (z2 || obj == null) {
                d();
            } else {
                aVar.request(1L);
            }
        }

        /* JADX WARN: Code restructure failed: missing block: B:32:0x0093, code lost:
            if (r3 == 0) goto L36;
         */
        /* JADX WARN: Code restructure failed: missing block: B:34:0x009c, code lost:
            if (r13 == androidx.recyclerview.widget.RecyclerView.FOREVER_NS) goto L36;
         */
        /* JADX WARN: Code restructure failed: missing block: B:35:0x009e, code lost:
            b.i.a.f.e.o.f.U0(r10, r3);
         */
        /* JADX WARN: Code restructure failed: missing block: B:36:0x00a1, code lost:
            r12 = addAndGet(-r12);
         */
        /* JADX WARN: Code restructure failed: missing block: B:37:0x00a6, code lost:
            if (r12 != 0) goto L6;
         */
        /* JADX WARN: Code restructure failed: missing block: B:38:0x00a8, code lost:
            return;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public void d() {
            /*
                r19 = this;
                r7 = r19
                int r0 = r19.getAndIncrement()
                if (r0 == 0) goto L9
                return
            L9:
                j0.l.e.n.e<java.lang.Object> r8 = r7.queue
                rx.Subscriber<? super R> r9 = r7.actual
                boolean r0 = r7.delayError
                java.util.concurrent.atomic.AtomicLong r10 = r7.requested
                r11 = 1
                r12 = 1
            L13:
                boolean r2 = r7.done
                boolean r3 = r8.isEmpty()
                r1 = r19
                r4 = r9
                r5 = r8
                r6 = r0
                boolean r1 = r1.b(r2, r3, r4, r5, r6)
                if (r1 == 0) goto L25
                return
            L25:
                long r13 = r10.get()
                r5 = 0
            L2b:
                int r1 = (r5 > r13 ? 1 : (r5 == r13 ? 0 : -1))
                if (r1 == 0) goto L8e
                boolean r2 = r7.done
                java.lang.Object r1 = r8.peek()
                r4 = r1
                j0.l.a.f$a r4 = (j0.l.a.f.a) r4
                if (r4 != 0) goto L3d
                r16 = 1
                goto L40
            L3d:
                r1 = 0
                r16 = 0
            L40:
                r1 = r19
                r3 = r16
                r15 = r4
                r4 = r9
                r17 = r5
                r5 = r8
                r6 = r0
                boolean r1 = r1.b(r2, r3, r4, r5, r6)
                if (r1 == 0) goto L51
                return
            L51:
                if (r16 == 0) goto L56
                r3 = r17
                goto L8f
            L56:
                r8.poll()
                java.lang.Object r1 = r8.poll()
                java.lang.Object[] r1 = (java.lang.Object[]) r1
                if (r1 != 0) goto L71
                r7.cancelled = r11
                r7.a(r8)
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "Broken queue?! Sender received but not the array."
                r0.<init>(r1)
                r9.onError(r0)
                return
            L71:
                rx.functions.FuncN<? extends R> r2 = r7.combiner     // Catch: java.lang.Throwable -> L84
                java.lang.Object r1 = r2.call(r1)     // Catch: java.lang.Throwable -> L84
                r9.onNext(r1)
                r1 = 1
                r15.request(r1)
                r3 = r17
                long r5 = r3 + r1
                goto L2b
            L84:
                r0 = move-exception
                r7.cancelled = r11
                r7.a(r8)
                r9.onError(r0)
                return
            L8e:
                r3 = r5
            L8f:
                r1 = 0
                int r5 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
                if (r5 == 0) goto La1
                r1 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
                int r5 = (r13 > r1 ? 1 : (r13 == r1 ? 0 : -1))
                if (r5 == 0) goto La1
                b.i.a.f.e.o.f.U0(r10, r3)
            La1:
                int r1 = -r12
                int r12 = r7.addAndGet(r1)
                if (r12 != 0) goto L13
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: j0.l.a.f.b.d():void");
        }

        @Override // rx.Subscription
        public boolean isUnsubscribed() {
            return this.cancelled;
        }

        @Override // rx.Producer
        public void j(long j2) {
            int i = (j2 > 0L ? 1 : (j2 == 0L ? 0 : -1));
            if (i < 0) {
                throw new IllegalArgumentException(b.d.b.a.a.s("n >= required but it was ", j2));
            } else if (i != 0) {
                b.i.a.f.e.o.f.c0(this.requested, j2);
                d();
            }
        }

        @Override // rx.Subscription
        public void unsubscribe() {
            if (!this.cancelled) {
                this.cancelled = true;
                if (getAndIncrement() == 0) {
                    a(this.queue);
                }
            }
        }
    }

    public f(Iterable<? extends Observable<? extends T>> iterable, FuncN<? extends R> funcN) {
        int i = i.j;
        this.j = iterable;
        this.k = funcN;
        this.l = i;
    }

    @Override // rx.functions.Action1
    public void call(Object obj) {
        Observable[] observableArr;
        int i;
        Subscriber subscriber = (Subscriber) obj;
        Iterable<? extends Observable<? extends T>> iterable = this.j;
        if (iterable instanceof List) {
            List list = (List) iterable;
            observableArr = (Observable[]) list.toArray(new Observable[list.size()]);
            i = observableArr.length;
        } else {
            Observable[] observableArr2 = new Observable[8];
            int i2 = 0;
            for (Observable<? extends T> observable : iterable) {
                if (i2 == observableArr2.length) {
                    Observable[] observableArr3 = new Observable[(i2 >> 2) + i2];
                    System.arraycopy(observableArr2, 0, observableArr3, 0, i2);
                    observableArr2 = observableArr3;
                }
                i2++;
                observableArr2[i2] = observable;
            }
            observableArr = observableArr2;
            i = i2;
        }
        if (i == 0) {
            subscriber.onCompleted();
            return;
        }
        b bVar = new b(subscriber, this.k, i, this.l, false);
        a<T, R>[] aVarArr = bVar.subscribers;
        int length = aVarArr.length;
        for (int i3 = 0; i3 < length; i3++) {
            aVarArr[i3] = new a<>(bVar, i3);
        }
        bVar.lazySet(0);
        bVar.actual.add(bVar);
        bVar.actual.setProducer(bVar);
        for (int i4 = 0; i4 < length && !bVar.cancelled; i4++) {
            observableArr[i4].U(aVarArr[i4]);
        }
    }
}
