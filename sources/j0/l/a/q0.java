package j0.l.a;

import java.util.concurrent.TimeUnit;
import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.observers.SerializedSubscriber;
import rx.subscriptions.SerialSubscription;
/* compiled from: OperatorDebounceWithTime.java */
/* loaded from: classes3.dex */
public final class q0<T> implements Observable.b<T, T> {
    public final long j;
    public final TimeUnit k;
    public final Scheduler l;

    /* compiled from: OperatorDebounceWithTime.java */
    /* loaded from: classes3.dex */
    public static final class a<T> {
        public int a;

        /* renamed from: b  reason: collision with root package name */
        public T f3750b;
        public boolean c;
        public boolean d;
        public boolean e;
    }

    public q0(long j, TimeUnit timeUnit, Scheduler scheduler) {
        this.j = j;
        this.k = timeUnit;
        this.l = scheduler;
    }

    @Override // j0.k.b
    public Object call(Object obj) {
        Subscriber subscriber = (Subscriber) obj;
        Scheduler.Worker a2 = this.l.a();
        SerializedSubscriber serializedSubscriber = new SerializedSubscriber(subscriber);
        SerialSubscription serialSubscription = new SerialSubscription();
        serializedSubscriber.add(a2);
        serializedSubscriber.add(serialSubscription);
        return new p0(this, subscriber, serialSubscription, a2, serializedSubscriber);
    }
}
