package j0.l.a;

import j0.f;
import j0.k.b;
import j0.l.c.m;
import j0.n.a;
import j0.n.d;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.subjects.BehaviorSubject;
import rx.subjects.SerializedSubject;
import rx.subscriptions.SerialSubscription;
/* compiled from: OnSubscribeRedo.java */
/* loaded from: classes3.dex */
public final class y<T> implements Observable.a<T> {
    public final Observable<T> j;
    public final b<? super Observable<? extends f<?>>, ? extends Observable<?>> k;
    public final boolean l;
    public final boolean m;

    public y(Observable<T> observable, b<? super Observable<? extends f<?>>, ? extends Observable<?>> bVar, boolean z2, boolean z3, Scheduler scheduler) {
        this.j = observable;
        this.k = bVar;
        this.l = z2;
        this.m = z3;
    }

    @Override // rx.functions.Action1
    public void call(Object obj) {
        Subscriber subscriber = (Subscriber) obj;
        AtomicBoolean atomicBoolean = new AtomicBoolean(true);
        AtomicLong atomicLong = new AtomicLong();
        m.a aVar = new m.a();
        subscriber.add(aVar);
        SerialSubscription serialSubscription = new SerialSubscription();
        subscriber.add(serialSubscription);
        SerializedSubject serializedSubject = new SerializedSubject(BehaviorSubject.k0());
        serializedSubject.U(new d(a.a));
        j0.l.b.a aVar2 = new j0.l.b.a();
        t tVar = new t(this, subscriber, serializedSubject, aVar2, atomicLong, serialSubscription);
        aVar.a(new w(this, this.k.call(Observable.h0(new r(serializedSubject.j, new v(this)))), subscriber, atomicLong, aVar, tVar, atomicBoolean));
        subscriber.setProducer(new x(this, atomicLong, aVar2, atomicBoolean, aVar, tVar));
    }
}
