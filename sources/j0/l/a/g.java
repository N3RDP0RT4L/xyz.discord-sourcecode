package j0.l.a;

import b.d.b.a.a;
import j0.l.a.h;
import java.util.Objects;
import rx.Producer;
/* compiled from: OnSubscribeConcatMap.java */
/* loaded from: classes3.dex */
public class g implements Producer {
    public final /* synthetic */ h.c j;

    public g(h hVar, h.c cVar) {
        this.j = cVar;
    }

    @Override // rx.Producer
    public void j(long j) {
        h.c cVar = this.j;
        Objects.requireNonNull(cVar);
        int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
        if (i > 0) {
            cVar.m.j(j);
        } else if (i < 0) {
            throw new IllegalArgumentException(a.s("n >= 0 required but it was ", j));
        }
    }
}
