package j0.l.a;

import b.i.a.f.e.o.f;
import rx.Subscriber;
import rx.functions.Action0;
/* compiled from: OnSubscribeTimerOnce.java */
/* loaded from: classes3.dex */
public class h0 implements Action0 {
    public final /* synthetic */ Subscriber j;

    public h0(i0 i0Var, Subscriber subscriber) {
        this.j = subscriber;
    }

    @Override // rx.functions.Action0
    public void call() {
        try {
            this.j.onNext(0L);
            this.j.onCompleted();
        } catch (Throwable th) {
            Subscriber subscriber = this.j;
            f.o1(th);
            subscriber.onError(th);
        }
    }
}
