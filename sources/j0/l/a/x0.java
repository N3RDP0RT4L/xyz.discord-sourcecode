package j0.l.a;

import androidx.recyclerview.widget.RecyclerView;
import b.i.a.f.e.o.f;
import j0.l.e.i;
import j0.l.e.k;
import j0.l.e.n.g;
import j0.l.e.o.n;
import j0.l.e.o.y;
import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicLong;
import rx.Observable;
import rx.Producer;
import rx.Subscriber;
import rx.exceptions.CompositeException;
import rx.exceptions.MissingBackpressureException;
import rx.exceptions.OnErrorThrowable;
import rx.subscriptions.CompositeSubscription;
/* compiled from: OperatorMerge.java */
/* loaded from: classes3.dex */
public final class x0<T> implements Observable.b<T, Observable<? extends T>> {
    public final boolean j;

    /* compiled from: OperatorMerge.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public static final x0<Object> a = new x0<>(true, Integer.MAX_VALUE);
    }

    /* compiled from: OperatorMerge.java */
    /* loaded from: classes3.dex */
    public static final class b {
        public static final x0<Object> a = new x0<>(false, Integer.MAX_VALUE);
    }

    /* compiled from: OperatorMerge.java */
    /* loaded from: classes3.dex */
    public static final class c<T> extends Subscriber<T> {
        public static final int j = i.j / 4;
        public final e<T> k;
        public final long l;
        public volatile boolean m;
        public volatile i n;
        public int o;

        public c(e<T> eVar, long j2) {
            this.k = eVar;
            this.l = j2;
        }

        public void a(long j2) {
            int i = this.o - ((int) j2);
            if (i > j) {
                this.o = i;
                return;
            }
            int i2 = i.j;
            this.o = i2;
            int i3 = i2 - i;
            if (i3 > 0) {
                request(i3);
            }
        }

        @Override // j0.g
        public void onCompleted() {
            this.m = true;
            this.k.b();
        }

        @Override // j0.g
        public void onError(Throwable th) {
            this.k.d().offer(th);
            this.m = true;
            this.k.b();
        }

        @Override // j0.g
        public void onNext(T t) {
            boolean z2;
            e<T> eVar = this.k;
            long j2 = eVar.n.get();
            if (j2 != 0) {
                synchronized (eVar) {
                    j2 = eVar.n.get();
                    if (eVar.f3751s || j2 == 0) {
                        z2 = false;
                    } else {
                        eVar.f3751s = true;
                        z2 = true;
                    }
                }
            } else {
                z2 = false;
            }
            if (z2) {
                i iVar = this.n;
                if (iVar != null) {
                    Queue<Object> queue = iVar.k;
                    if (!(queue == null || queue.isEmpty())) {
                        eVar.f(this, t);
                        eVar.c();
                        return;
                    }
                }
                try {
                    eVar.k.onNext(t);
                    if (j2 != RecyclerView.FOREVER_NS) {
                        eVar.n.a(1);
                    }
                    a(1L);
                    synchronized (eVar) {
                        try {
                            if (!eVar.t) {
                                eVar.f3751s = false;
                            } else {
                                eVar.t = false;
                                eVar.c();
                            }
                        }
                    }
                }
            } else {
                eVar.f(this, t);
                eVar.b();
            }
        }

        @Override // rx.Subscriber
        public void onStart() {
            int i = i.j;
            this.o = i;
            request(i);
        }
    }

    /* compiled from: OperatorMerge.java */
    /* loaded from: classes3.dex */
    public static final class d<T> extends AtomicLong implements Producer {
        private static final long serialVersionUID = -1214379189873595503L;
        public final e<T> subscriber;

        public d(e<T> eVar) {
            this.subscriber = eVar;
        }

        public long a(int i) {
            return addAndGet(-i);
        }

        @Override // rx.Producer
        public void j(long j) {
            int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
            if (i > 0) {
                if (get() != RecyclerView.FOREVER_NS) {
                    f.c0(this, j);
                    this.subscriber.b();
                }
            } else if (i < 0) {
                throw new IllegalArgumentException("n >= 0 required");
            }
        }
    }

    /* compiled from: OperatorMerge.java */
    /* loaded from: classes3.dex */
    public static final class e<T> extends Subscriber<Observable<? extends T>> {
        public static final c<?>[] j = new c[0];
        public int A;
        public final Subscriber<? super T> k;
        public final boolean l;
        public final int m;
        public d<T> n;
        public volatile Queue<Object> o;
        public volatile CompositeSubscription p;
        public volatile ConcurrentLinkedQueue<Throwable> q;
        public volatile boolean r;

        /* renamed from: s  reason: collision with root package name */
        public boolean f3751s;
        public boolean t;
        public final Object u = new Object();
        public volatile c<?>[] v = j;
        public long w;

        /* renamed from: x  reason: collision with root package name */
        public long f3752x;

        /* renamed from: y  reason: collision with root package name */
        public int f3753y;

        /* renamed from: z  reason: collision with root package name */
        public final int f3754z;

        public e(Subscriber<? super T> subscriber, boolean z2, int i) {
            this.k = subscriber;
            this.l = z2;
            this.m = i;
            if (i == Integer.MAX_VALUE) {
                this.f3754z = Integer.MAX_VALUE;
                request(RecyclerView.FOREVER_NS);
                return;
            }
            this.f3754z = Math.max(1, i >> 1);
            request(i);
        }

        public boolean a() {
            if (this.k.isUnsubscribed()) {
                return true;
            }
            ConcurrentLinkedQueue<Throwable> concurrentLinkedQueue = this.q;
            if (this.l || concurrentLinkedQueue == null || concurrentLinkedQueue.isEmpty()) {
                return false;
            }
            try {
                h();
                return true;
            } finally {
                unsubscribe();
            }
        }

        public void b() {
            synchronized (this) {
                if (this.f3751s) {
                    this.t = true;
                    return;
                }
                this.f3751s = true;
                c();
            }
        }

        /* JADX WARN: Code restructure failed: missing block: B:100:0x0151, code lost:
            r4 = r9.m;
            r13 = r9.n;
         */
        /* JADX WARN: Code restructure failed: missing block: B:101:0x0155, code lost:
            if (r4 == false) goto L115;
         */
        /* JADX WARN: Code restructure failed: missing block: B:102:0x0157, code lost:
            if (r13 == null) goto L111;
         */
        /* JADX WARN: Code restructure failed: missing block: B:103:0x0159, code lost:
            r4 = r13.k;
         */
        /* JADX WARN: Code restructure failed: missing block: B:104:0x015b, code lost:
            if (r4 == null) goto L109;
         */
        /* JADX WARN: Code restructure failed: missing block: B:106:0x0161, code lost:
            if (r4.isEmpty() == false) goto L108;
         */
        /* JADX WARN: Code restructure failed: missing block: B:108:0x0164, code lost:
            r4 = false;
         */
        /* JADX WARN: Code restructure failed: missing block: B:109:0x0166, code lost:
            r4 = true;
         */
        /* JADX WARN: Code restructure failed: missing block: B:110:0x0167, code lost:
            if (r4 == false) goto L115;
         */
        /* JADX WARN: Code restructure failed: missing block: B:111:0x0169, code lost:
            g(r9);
         */
        /* JADX WARN: Code restructure failed: missing block: B:112:0x0170, code lost:
            if (a() == false) goto L114;
         */
        /* JADX WARN: Code restructure failed: missing block: B:113:0x0172, code lost:
            return;
         */
        /* JADX WARN: Code restructure failed: missing block: B:114:0x0173, code lost:
            r6 = r6 + 1;
            r8 = true;
         */
        /* JADX WARN: Code restructure failed: missing block: B:115:0x0176, code lost:
            if (r3 != 0) goto L117;
         */
        /* JADX WARN: Code restructure failed: missing block: B:117:0x0179, code lost:
            r0 = r0 + 1;
         */
        /* JADX WARN: Code restructure failed: missing block: B:118:0x017b, code lost:
            if (r0 != r7) goto L191;
         */
        /* JADX WARN: Code restructure failed: missing block: B:119:0x017d, code lost:
            r0 = 0;
         */
        /* JADX WARN: Code restructure failed: missing block: B:122:0x0186, code lost:
            r23.f3753y = r0;
            r23.f3752x = r5[r0].l;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public void c() {
            /*
                Method dump skipped, instructions count: 454
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: j0.l.a.x0.e.c():void");
        }

        public Queue<Throwable> d() {
            ConcurrentLinkedQueue<Throwable> concurrentLinkedQueue = this.q;
            if (concurrentLinkedQueue == null) {
                synchronized (this) {
                    concurrentLinkedQueue = this.q;
                    if (concurrentLinkedQueue == null) {
                        concurrentLinkedQueue = new ConcurrentLinkedQueue<>();
                        this.q = concurrentLinkedQueue;
                    }
                }
            }
            return concurrentLinkedQueue;
        }

        public void e(T t) {
            Queue<Object> queue;
            Queue<Object> queue2 = this.o;
            if (queue2 == null) {
                int i = this.m;
                if (i == Integer.MAX_VALUE) {
                    queue2 = new g<>(i.j);
                } else {
                    if (!(((i + (-1)) & i) == 0)) {
                        queue = new j0.l.e.n.d<>(i);
                    } else if (y.b()) {
                        queue = new n<>(i);
                    } else {
                        queue = new j0.l.e.n.c<>(i);
                    }
                    queue2 = queue;
                }
                this.o = queue2;
            }
            if (!queue2.offer(t == null ? j0.l.a.e.f3743b : t)) {
                unsubscribe();
                d().offer(OnErrorThrowable.a(new MissingBackpressureException(), t));
                this.r = true;
                b();
            }
        }

        public void f(c<T> cVar, T t) {
            i iVar = cVar.n;
            if (iVar == null) {
                if (y.b()) {
                    iVar = new i(false, i.j);
                } else {
                    iVar = new i();
                }
                cVar.add(iVar);
                cVar.n = iVar;
            }
            if (t == null) {
                try {
                    t = (T) j0.l.a.e.f3743b;
                } catch (IllegalStateException e) {
                    if (!cVar.isUnsubscribed()) {
                        cVar.unsubscribe();
                        cVar.onError(e);
                        return;
                    }
                    return;
                } catch (MissingBackpressureException e2) {
                    cVar.unsubscribe();
                    cVar.onError(e2);
                    return;
                }
            }
            iVar.a(t);
        }

        public void g(c<T> cVar) {
            i iVar = cVar.n;
            if (iVar != null) {
                synchronized (iVar) {
                }
            }
            this.p.c(cVar);
            synchronized (this.u) {
                c<?>[] cVarArr = this.v;
                int length = cVarArr.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        i = -1;
                        break;
                    } else if (cVar.equals(cVarArr[i])) {
                        break;
                    } else {
                        i++;
                    }
                }
                if (i >= 0) {
                    if (length == 1) {
                        this.v = j;
                        return;
                    }
                    c<?>[] cVarArr2 = new c[length - 1];
                    System.arraycopy(cVarArr, 0, cVarArr2, 0, i);
                    System.arraycopy(cVarArr, i + 1, cVarArr2, i, (length - i) - 1);
                    this.v = cVarArr2;
                }
            }
        }

        public final void h() {
            ArrayList arrayList = new ArrayList(this.q);
            if (arrayList.size() == 1) {
                this.k.onError((Throwable) arrayList.get(0));
            } else {
                this.k.onError(new CompositeException(arrayList));
            }
        }

        @Override // j0.g
        public void onCompleted() {
            this.r = true;
            b();
        }

        @Override // j0.g
        public void onError(Throwable th) {
            d().offer(th);
            this.r = true;
            b();
        }

        @Override // j0.g
        public void onNext(Object obj) {
            boolean z2;
            Observable<Object> observable = (Observable) obj;
            if (observable != null) {
                boolean z3 = true;
                if (observable == j0.l.a.c.k) {
                    int i = this.A + 1;
                    if (i == this.f3754z) {
                        this.A = 0;
                        request(i);
                        return;
                    }
                    this.A = i;
                } else if (observable instanceof k) {
                    T t = ((k) observable).l;
                    long j2 = this.n.get();
                    if (j2 != 0) {
                        synchronized (this) {
                            j2 = this.n.get();
                            if (this.f3751s || j2 == 0) {
                                z2 = false;
                            } else {
                                this.f3751s = true;
                                z2 = true;
                            }
                        }
                    } else {
                        z2 = false;
                    }
                    if (z2) {
                        Queue<Object> queue = this.o;
                        if (queue == null || queue.isEmpty()) {
                            try {
                                this.k.onNext(t);
                                if (j2 != RecyclerView.FOREVER_NS) {
                                    try {
                                        this.n.a(1);
                                    }
                                }
                                int i2 = this.A + 1;
                                if (i2 == this.f3754z) {
                                    this.A = 0;
                                    request(i2);
                                } else {
                                    this.A = i2;
                                }
                                synchronized (this) {
                                    if (!this.t) {
                                        this.f3751s = false;
                                    } else {
                                        this.t = false;
                                        c();
                                    }
                                }
                            }
                        } else {
                            e(t);
                            c();
                        }
                    } else {
                        e(t);
                        b();
                    }
                } else {
                    long j3 = this.w;
                    this.w = 1 + j3;
                    c<?> cVar = new c<>(this, j3);
                    CompositeSubscription compositeSubscription = this.p;
                    if (compositeSubscription == null) {
                        synchronized (this) {
                            compositeSubscription = this.p;
                            if (compositeSubscription == null) {
                                compositeSubscription = new CompositeSubscription();
                                this.p = compositeSubscription;
                            } else {
                                z3 = false;
                            }
                        }
                        if (z3) {
                            add(compositeSubscription);
                        }
                    }
                    compositeSubscription.a(cVar);
                    synchronized (this.u) {
                        c<?>[] cVarArr = this.v;
                        int length = cVarArr.length;
                        c<?>[] cVarArr2 = new c[length + 1];
                        System.arraycopy(cVarArr, 0, cVarArr2, 0, length);
                        cVarArr2[length] = cVar;
                        this.v = cVarArr2;
                    }
                    observable.i0(cVar);
                    b();
                }
            }
        }
    }

    public x0(boolean z2, int i) {
        this.j = z2;
    }

    @Override // j0.k.b
    public Object call(Object obj) {
        Subscriber subscriber = (Subscriber) obj;
        e eVar = new e(subscriber, this.j, Integer.MAX_VALUE);
        d<T> dVar = new d<>(eVar);
        eVar.n = dVar;
        subscriber.add(eVar);
        subscriber.setProducer(dVar);
        return eVar;
    }
}
