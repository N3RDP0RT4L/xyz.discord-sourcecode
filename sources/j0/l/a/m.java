package j0.l.a;

import b.d.b.a.a;
import b.i.a.f.e.o.f;
import j0.l.a.n;
import java.util.Objects;
import rx.Producer;
/* compiled from: OnSubscribeFlattenIterable.java */
/* loaded from: classes3.dex */
public class m implements Producer {
    public final /* synthetic */ n.a j;

    public m(n nVar, n.a aVar) {
        this.j = aVar;
    }

    @Override // rx.Producer
    public void j(long j) {
        n.a aVar = this.j;
        Objects.requireNonNull(aVar);
        int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
        if (i > 0) {
            f.c0(aVar.o, j);
            aVar.b();
        } else if (i < 0) {
            throw new IllegalStateException(a.s("n >= 0 required but it was ", j));
        }
    }
}
