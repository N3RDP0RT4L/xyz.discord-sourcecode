package j0.l.a;

import androidx.recyclerview.widget.RecyclerView;
import b.i.a.f.e.o.f;
import j0.l.b.b;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import rx.Subscriber;
/* compiled from: OperatorToObservableSortedList.java */
/* loaded from: classes3.dex */
public class p2 extends Subscriber<T> {
    public List<T> j = new ArrayList(10);
    public boolean k;
    public final /* synthetic */ b l;
    public final /* synthetic */ Subscriber m;
    public final /* synthetic */ o2 n;

    public p2(o2 o2Var, b bVar, Subscriber subscriber) {
        this.n = o2Var;
        this.l = bVar;
        this.m = subscriber;
    }

    @Override // j0.g
    public void onCompleted() {
        if (!this.k) {
            this.k = true;
            List<T> list = this.j;
            this.j = null;
            try {
                Collections.sort(list, this.n.j);
                this.l.b(list);
            } catch (Throwable th) {
                f.o1(th);
                onError(th);
            }
        }
    }

    @Override // j0.g
    public void onError(Throwable th) {
        this.m.onError(th);
    }

    @Override // j0.g
    public void onNext(T t) {
        if (!this.k) {
            this.j.add(t);
        }
    }

    @Override // rx.Subscriber
    public void onStart() {
        request(RecyclerView.FOREVER_NS);
    }
}
