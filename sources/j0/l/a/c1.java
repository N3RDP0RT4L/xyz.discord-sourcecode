package j0.l.a;

import j0.k.b;
import j0.l.e.k;
import rx.Observable;
/* compiled from: OperatorOnErrorResumeNextViaFunction.java */
/* loaded from: classes3.dex */
public final class c1 implements b<Throwable, Observable<? extends T>> {
    public final /* synthetic */ b j;

    public c1(b bVar) {
        this.j = bVar;
    }

    @Override // j0.k.b
    public Object call(Throwable th) {
        return new k(this.j.call(th));
    }
}
