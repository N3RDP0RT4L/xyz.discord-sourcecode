package j0.l.a;

import androidx.recyclerview.widget.RecyclerView;
import b.i.a.f.e.o.f;
import j0.l.a.q;
import j0.l.e.d;
import j0.l.e.i;
import j0.l.e.n.c;
import j0.l.e.n.e;
import j0.l.e.o.y;
import j0.o.l;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import rx.Observable;
import rx.Subscriber;
import rx.exceptions.MissingBackpressureException;
/* compiled from: OnSubscribeFlattenIterable.java */
/* loaded from: classes3.dex */
public final class n<T, R> implements Observable.a<R> {
    public final Observable<? extends T> j;
    public final j0.k.b<? super T, ? extends Iterable<? extends R>> k;
    public final int l;

    /* compiled from: OnSubscribeFlattenIterable.java */
    /* loaded from: classes3.dex */
    public static final class a<T, R> extends Subscriber<T> {
        public final Subscriber<? super R> j;
        public final j0.k.b<? super T, ? extends Iterable<? extends R>> k;
        public final long l;
        public final Queue<Object> m;
        public volatile boolean q;
        public long r;

        /* renamed from: s  reason: collision with root package name */
        public Iterator<? extends R> f3749s;
        public final AtomicReference<Throwable> n = new AtomicReference<>();
        public final AtomicInteger p = new AtomicInteger();
        public final AtomicLong o = new AtomicLong();

        public a(Subscriber<? super R> subscriber, j0.k.b<? super T, ? extends Iterable<? extends R>> bVar, int i) {
            this.j = subscriber;
            this.k = bVar;
            if (i == Integer.MAX_VALUE) {
                this.l = RecyclerView.FOREVER_NS;
                this.m = new e(i.j);
            } else {
                this.l = i - (i >> 2);
                if (y.b()) {
                    this.m = new j0.l.e.o.n(i);
                } else {
                    this.m = new c(i);
                }
            }
            request(i);
        }

        public boolean a(boolean z2, boolean z3, Subscriber<?> subscriber, Queue<?> queue) {
            if (subscriber.isUnsubscribed()) {
                queue.clear();
                this.f3749s = null;
                return true;
            } else if (!z2) {
                return false;
            } else {
                if (this.n.get() != null) {
                    Throwable h = d.h(this.n);
                    unsubscribe();
                    queue.clear();
                    this.f3749s = null;
                    subscriber.onError(h);
                    return true;
                } else if (!z3) {
                    return false;
                } else {
                    subscriber.onCompleted();
                    return true;
                }
            }
        }

        /* JADX WARN: Removed duplicated region for block: B:27:0x0066  */
        /* JADX WARN: Removed duplicated region for block: B:73:0x00cf A[SYNTHETIC] */
        /* JADX WARN: Removed duplicated region for block: B:77:0x00d8 A[SYNTHETIC] */
        /* JADX WARN: Removed duplicated region for block: B:80:0x0010 A[SYNTHETIC] */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public void b() {
            /*
                Method dump skipped, instructions count: 217
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: j0.l.a.n.a.b():void");
        }

        @Override // j0.g
        public void onCompleted() {
            this.q = true;
            b();
        }

        @Override // j0.g
        public void onError(Throwable th) {
            if (d.f(this.n, th)) {
                this.q = true;
                b();
                return;
            }
            l.b(th);
        }

        @Override // j0.g
        public void onNext(T t) {
            Queue<Object> queue = this.m;
            if (t == null) {
                t = (T) e.f3743b;
            }
            if (!queue.offer(t)) {
                unsubscribe();
                onError(new MissingBackpressureException());
                return;
            }
            b();
        }
    }

    /* compiled from: OnSubscribeFlattenIterable.java */
    /* loaded from: classes3.dex */
    public static final class b<T, R> implements Observable.a<R> {
        public final T j;
        public final j0.k.b<? super T, ? extends Iterable<? extends R>> k;

        public b(T t, j0.k.b<? super T, ? extends Iterable<? extends R>> bVar) {
            this.j = t;
            this.k = bVar;
        }

        @Override // rx.functions.Action1
        public void call(Object obj) {
            Subscriber subscriber = (Subscriber) obj;
            try {
                Iterator<? extends R> it = this.k.call((T) this.j).iterator();
                if (!it.hasNext()) {
                    subscriber.onCompleted();
                } else {
                    subscriber.setProducer(new q.a(subscriber, it));
                }
            } catch (Throwable th) {
                f.p1(th, subscriber, this.j);
            }
        }
    }

    public n(Observable<? extends T> observable, j0.k.b<? super T, ? extends Iterable<? extends R>> bVar, int i) {
        this.j = observable;
        this.k = bVar;
        this.l = i;
    }

    @Override // rx.functions.Action1
    public void call(Object obj) {
        Subscriber subscriber = (Subscriber) obj;
        a aVar = new a(subscriber, this.k, this.l);
        subscriber.add(aVar);
        subscriber.setProducer(new m(this, aVar));
        this.j.i0(aVar);
    }
}
