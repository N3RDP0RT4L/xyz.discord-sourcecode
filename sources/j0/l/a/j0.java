package j0.l.a;

import b.i.a.f.e.o.f;
import rx.Scheduler;
import rx.Subscriber;
import rx.functions.Action0;
/* compiled from: OnSubscribeTimerPeriodically.java */
/* loaded from: classes3.dex */
public class j0 implements Action0 {
    public long j;
    public final /* synthetic */ Subscriber k;
    public final /* synthetic */ Scheduler.Worker l;

    public j0(k0 k0Var, Subscriber subscriber, Scheduler.Worker worker) {
        this.k = subscriber;
        this.l = worker;
    }

    @Override // rx.functions.Action0
    public void call() {
        try {
            Subscriber subscriber = this.k;
            long j = this.j;
            this.j = 1 + j;
            subscriber.onNext(Long.valueOf(j));
        } catch (Throwable th) {
            try {
                this.l.unsubscribe();
            } finally {
                Subscriber subscriber2 = this.k;
                f.o1(th);
                subscriber2.onError(th);
            }
        }
    }
}
