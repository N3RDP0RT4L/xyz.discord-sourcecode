package j0.o;

import j0.k.b;
import java.util.Objects;
import rx.Observable;
/* compiled from: RxJavaHooks.java */
/* loaded from: classes3.dex */
public final class k implements b<Observable.b, Observable.b> {
    @Override // j0.k.b
    public Observable.b call(Observable.b bVar) {
        Observable.b bVar2 = bVar;
        Objects.requireNonNull(o.a.c());
        return bVar2;
    }
}
