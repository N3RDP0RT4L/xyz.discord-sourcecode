package j0.o;

import j0.k.b;
import java.util.Objects;
import rx.Subscription;
/* compiled from: RxJavaHooks.java */
/* loaded from: classes3.dex */
public final class h implements b<Subscription, Subscription> {
    @Override // j0.k.b
    public Subscription call(Subscription subscription) {
        Subscription subscription2 = subscription;
        Objects.requireNonNull(o.a.c());
        return subscription2;
    }
}
