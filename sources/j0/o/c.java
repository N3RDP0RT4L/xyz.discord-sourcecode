package j0.o;

import j0.k.b;
import java.util.Objects;
import rx.Observable;
/* compiled from: RxJavaHooks.java */
/* loaded from: classes3.dex */
public final class c implements b<Observable.a, Observable.a> {
    @Override // j0.k.b
    public Observable.a call(Observable.a aVar) {
        Observable.a aVar2 = aVar;
        Objects.requireNonNull(o.a.c());
        return aVar2;
    }
}
