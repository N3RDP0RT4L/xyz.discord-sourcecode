package j0.o;

import java.util.Properties;
import java.util.concurrent.atomic.AtomicReference;
/* compiled from: RxJavaPlugins.java */
/* loaded from: classes3.dex */
public class o {
    public static final o a = new o();

    /* renamed from: b  reason: collision with root package name */
    public static final j0.o.b f3774b = new a();
    public final AtomicReference<j0.o.b> c = new AtomicReference<>();
    public final AtomicReference<m> d = new AtomicReference<>();
    public final AtomicReference<q> e = new AtomicReference<>();
    public final AtomicReference<j0.o.a> f = new AtomicReference<>();
    public final AtomicReference<p> g = new AtomicReference<>();

    /* compiled from: RxJavaPlugins.java */
    /* loaded from: classes3.dex */
    public static class a extends j0.o.b {
    }

    /* compiled from: RxJavaPlugins.java */
    /* loaded from: classes3.dex */
    public class b extends j0.o.a {
        public b(o oVar) {
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:14:0x0060, code lost:
        r2 = "rxjava.plugin." + r7.substring(0, r7.length() - 6).substring(14) + ".impl";
        r1 = r10.getProperty(r2);
     */
    /* JADX WARN: Code restructure failed: missing block: B:15:0x0087, code lost:
        if (r1 == null) goto L17;
     */
    /* JADX WARN: Code restructure failed: missing block: B:18:0x00a8, code lost:
        throw new java.lang.IllegalStateException("Implementing class declaration for " + r0 + " missing: " + r2);
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static java.lang.Object d(java.lang.Class<?> r9, java.util.Properties r10) {
        /*
            java.lang.Object r10 = r10.clone()
            java.util.Properties r10 = (java.util.Properties) r10
            java.lang.String r0 = r9.getSimpleName()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "rxjava.plugin."
            r1.append(r2)
            r1.append(r0)
            java.lang.String r3 = ".implementation"
            r1.append(r3)
            java.lang.String r1 = r1.toString()
            java.lang.String r1 = r10.getProperty(r1)
            if (r1 != 0) goto Lad
            java.lang.String r3 = ".class"
            java.lang.String r4 = ".impl"
            java.util.Set r5 = r10.entrySet()     // Catch: java.lang.SecurityException -> La9
            java.util.Iterator r5 = r5.iterator()     // Catch: java.lang.SecurityException -> La9
        L32:
            boolean r6 = r5.hasNext()     // Catch: java.lang.SecurityException -> La9
            if (r6 == 0) goto Lad
            java.lang.Object r6 = r5.next()     // Catch: java.lang.SecurityException -> La9
            java.util.Map$Entry r6 = (java.util.Map.Entry) r6     // Catch: java.lang.SecurityException -> La9
            java.lang.Object r7 = r6.getKey()     // Catch: java.lang.SecurityException -> La9
            java.lang.String r7 = r7.toString()     // Catch: java.lang.SecurityException -> La9
            boolean r8 = r7.startsWith(r2)     // Catch: java.lang.SecurityException -> La9
            if (r8 == 0) goto L32
            boolean r8 = r7.endsWith(r3)     // Catch: java.lang.SecurityException -> La9
            if (r8 == 0) goto L32
            java.lang.Object r6 = r6.getValue()     // Catch: java.lang.SecurityException -> La9
            java.lang.String r6 = r6.toString()     // Catch: java.lang.SecurityException -> La9
            boolean r6 = r0.equals(r6)     // Catch: java.lang.SecurityException -> La9
            if (r6 == 0) goto L32
            r3 = 0
            int r5 = r7.length()     // Catch: java.lang.SecurityException -> La9
            int r5 = r5 + (-6)
            java.lang.String r3 = r7.substring(r3, r5)     // Catch: java.lang.SecurityException -> La9
            r5 = 14
            java.lang.String r3 = r3.substring(r5)     // Catch: java.lang.SecurityException -> La9
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch: java.lang.SecurityException -> La9
            r5.<init>()     // Catch: java.lang.SecurityException -> La9
            r5.append(r2)     // Catch: java.lang.SecurityException -> La9
            r5.append(r3)     // Catch: java.lang.SecurityException -> La9
            r5.append(r4)     // Catch: java.lang.SecurityException -> La9
            java.lang.String r2 = r5.toString()     // Catch: java.lang.SecurityException -> La9
            java.lang.String r1 = r10.getProperty(r2)     // Catch: java.lang.SecurityException -> La9
            if (r1 == 0) goto L8a
            goto Lad
        L8a:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException     // Catch: java.lang.SecurityException -> La9
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch: java.lang.SecurityException -> La9
            r3.<init>()     // Catch: java.lang.SecurityException -> La9
            java.lang.String r4 = "Implementing class declaration for "
            r3.append(r4)     // Catch: java.lang.SecurityException -> La9
            r3.append(r0)     // Catch: java.lang.SecurityException -> La9
            java.lang.String r4 = " missing: "
            r3.append(r4)     // Catch: java.lang.SecurityException -> La9
            r3.append(r2)     // Catch: java.lang.SecurityException -> La9
            java.lang.String r2 = r3.toString()     // Catch: java.lang.SecurityException -> La9
            r10.<init>(r2)     // Catch: java.lang.SecurityException -> La9
            throw r10     // Catch: java.lang.SecurityException -> La9
        La9:
            r10 = move-exception
            r10.printStackTrace()
        Lad:
            if (r1 == 0) goto L106
            java.lang.Class r10 = java.lang.Class.forName(r1)     // Catch: java.lang.IllegalAccessException -> Lbc java.lang.InstantiationException -> Lc9 java.lang.ClassNotFoundException -> Ld6 java.lang.ClassCastException -> Le3
            java.lang.Class r9 = r10.asSubclass(r9)     // Catch: java.lang.IllegalAccessException -> Lbc java.lang.InstantiationException -> Lc9 java.lang.ClassNotFoundException -> Ld6 java.lang.ClassCastException -> Le3
            java.lang.Object r9 = r9.newInstance()     // Catch: java.lang.IllegalAccessException -> Lbc java.lang.InstantiationException -> Lc9 java.lang.ClassNotFoundException -> Ld6 java.lang.ClassCastException -> Le3
            return r9
        Lbc:
            r9 = move-exception
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r2 = " implementation not able to be accessed: "
            java.lang.String r0 = b.d.b.a.a.w(r0, r2, r1)
            r10.<init>(r0, r9)
            throw r10
        Lc9:
            r9 = move-exception
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r2 = " implementation not able to be instantiated: "
            java.lang.String r0 = b.d.b.a.a.w(r0, r2, r1)
            r10.<init>(r0, r9)
            throw r10
        Ld6:
            r9 = move-exception
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r2 = " implementation class not found: "
            java.lang.String r0 = b.d.b.a.a.w(r0, r2, r1)
            r10.<init>(r0, r9)
            throw r10
        Le3:
            r9 = move-exception
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r0)
            java.lang.String r3 = " implementation is not an instance of "
            r2.append(r3)
            r2.append(r0)
            java.lang.String r0 = ": "
            r2.append(r0)
            r2.append(r1)
            java.lang.String r0 = r2.toString()
            r10.<init>(r0, r9)
            throw r10
        L106:
            r9 = 0
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: j0.o.o.d(java.lang.Class, java.util.Properties):java.lang.Object");
    }

    public static Properties g() {
        try {
            return System.getProperties();
        } catch (SecurityException unused) {
            return new Properties();
        }
    }

    public j0.o.a a() {
        if (this.f.get() == null) {
            Object d = d(j0.o.a.class, g());
            if (d == null) {
                this.f.compareAndSet(null, new b(this));
            } else {
                this.f.compareAndSet(null, (j0.o.a) d);
            }
        }
        return this.f.get();
    }

    public j0.o.b b() {
        if (this.c.get() == null) {
            Object d = d(j0.o.b.class, g());
            if (d == null) {
                this.c.compareAndSet(null, f3774b);
            } else {
                this.c.compareAndSet(null, (j0.o.b) d);
            }
        }
        return this.c.get();
    }

    public m c() {
        if (this.d.get() == null) {
            Object d = d(m.class, g());
            if (d == null) {
                this.d.compareAndSet(null, n.a);
            } else {
                this.d.compareAndSet(null, (m) d);
            }
        }
        return this.d.get();
    }

    public p e() {
        if (this.g.get() == null) {
            Object d = d(p.class, g());
            if (d == null) {
                this.g.compareAndSet(null, p.a);
            } else {
                this.g.compareAndSet(null, (p) d);
            }
        }
        return this.g.get();
    }

    public q f() {
        if (this.e.get() == null) {
            Object d = d(q.class, g());
            if (d == null) {
                this.e.compareAndSet(null, r.a);
            } else {
                this.e.compareAndSet(null, (q) d);
            }
        }
        return this.e.get();
    }
}
