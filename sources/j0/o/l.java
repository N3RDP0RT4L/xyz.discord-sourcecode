package j0.o;

import b.d.b.a.a;
import j0.d;
import j0.h;
import j0.k.b;
import java.io.PrintStream;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func2;
/* compiled from: RxJavaHooks.java */
/* loaded from: classes3.dex */
public final class l {
    public static volatile Action1<Throwable> a = new f();
    public static volatile Func2<Observable, Observable.a, Observable.a> e = new g();
    public static volatile b<Subscription, Subscription> g = new h();
    public static volatile b<Action0, Action0> f = new i();
    public static volatile b<Throwable, Throwable> h = new j();
    public static volatile b<Observable.b, Observable.b> i = new k();

    /* renamed from: b  reason: collision with root package name */
    public static volatile b<Observable.a, Observable.a> f3773b = new c();
    public static volatile b<h.a, h.a> c = new d();
    public static volatile b<d.a, d.a> d = new e();

    public static <T> Observable.a<T> a(Observable.a<T> aVar) {
        b<Observable.a, Observable.a> bVar = f3773b;
        return bVar != null ? bVar.call(aVar) : aVar;
    }

    public static void b(Throwable th) {
        Action1<Throwable> action1 = a;
        if (action1 != null) {
            try {
                action1.call(th);
                return;
            } catch (Throwable th2) {
                PrintStream printStream = System.err;
                StringBuilder R = a.R("The onError handler threw an Exception. It shouldn't. => ");
                R.append(th2.getMessage());
                printStream.println(R.toString());
                th2.printStackTrace();
                Thread currentThread = Thread.currentThread();
                currentThread.getUncaughtExceptionHandler().uncaughtException(currentThread, th2);
            }
        }
        Thread currentThread2 = Thread.currentThread();
        currentThread2.getUncaughtExceptionHandler().uncaughtException(currentThread2, th);
    }

    public static Throwable c(Throwable th) {
        b<Throwable, Throwable> bVar = h;
        return bVar != null ? bVar.call(th) : th;
    }

    public static Action0 d(Action0 action0) {
        b<Action0, Action0> bVar = f;
        return bVar != null ? bVar.call(action0) : action0;
    }
}
