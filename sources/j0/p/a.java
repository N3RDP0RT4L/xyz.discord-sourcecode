package j0.p;

import j0.l.c.b;
import j0.l.c.f;
import j0.l.c.k;
import j0.l.e.j;
import j0.o.o;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import rx.Scheduler;
/* compiled from: Schedulers.java */
/* loaded from: classes3.dex */
public final class a {
    public static final AtomicReference<a> a = new AtomicReference<>();

    /* renamed from: b  reason: collision with root package name */
    public final Scheduler f3775b = new b(new j("RxComputationScheduler-"));
    public final Scheduler c = new j0.l.c.a(new j("RxIoScheduler-"));
    public final Scheduler d = new f(new j("RxNewThreadScheduler-"));

    public a() {
        Objects.requireNonNull(o.a.e());
    }

    public static Scheduler a() {
        return b().f3775b;
    }

    public static a b() {
        while (true) {
            AtomicReference<a> atomicReference = a;
            a aVar = atomicReference.get();
            if (aVar != null) {
                return aVar;
            }
            a aVar2 = new a();
            if (atomicReference.compareAndSet(null, aVar2)) {
                return aVar2;
            }
            synchronized (aVar2) {
                Scheduler scheduler = aVar2.f3775b;
                if (scheduler instanceof k) {
                    ((k) scheduler).shutdown();
                }
                Scheduler scheduler2 = aVar2.c;
                if (scheduler2 instanceof k) {
                    ((k) scheduler2).shutdown();
                }
                Scheduler scheduler3 = aVar2.d;
                if (scheduler3 instanceof k) {
                    ((k) scheduler3).shutdown();
                }
            }
        }
    }

    public static Scheduler c() {
        return b().c;
    }
}
