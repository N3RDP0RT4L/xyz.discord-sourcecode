package j0.n;

import j0.g;
import rx.Subscriber;
/* compiled from: Subscribers.java */
/* loaded from: classes3.dex */
public final class d extends Subscriber<T> {
    public final /* synthetic */ g j;

    public d(g gVar) {
        this.j = gVar;
    }

    @Override // j0.g
    public void onCompleted() {
        this.j.onCompleted();
    }

    @Override // j0.g
    public void onError(Throwable th) {
        this.j.onError(th);
    }

    @Override // j0.g
    public void onNext(T t) {
        this.j.onNext(t);
    }
}
