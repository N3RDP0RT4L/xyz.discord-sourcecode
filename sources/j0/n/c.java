package j0.n;

import b.i.a.f.e.o.f;
import j0.g;
import j0.l.a.e;
/* compiled from: SerializedObserver.java */
/* loaded from: classes3.dex */
public class c<T> implements g<T> {
    public final g<? super T> j;
    public boolean k;
    public volatile boolean l;
    public a m;

    /* compiled from: SerializedObserver.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public Object[] a;

        /* renamed from: b  reason: collision with root package name */
        public int f3772b;

        public void a(Object obj) {
            int i = this.f3772b;
            Object[] objArr = this.a;
            if (objArr == null) {
                objArr = new Object[16];
                this.a = objArr;
            } else if (i == objArr.length) {
                Object[] objArr2 = new Object[(i >> 2) + i];
                System.arraycopy(objArr, 0, objArr2, 0, i);
                this.a = objArr2;
                objArr = objArr2;
            }
            objArr[i] = obj;
            this.f3772b = i + 1;
        }
    }

    public c(g<? super T> gVar) {
        this.j = gVar;
    }

    @Override // j0.g
    public void onCompleted() {
        if (!this.l) {
            synchronized (this) {
                if (!this.l) {
                    this.l = true;
                    if (this.k) {
                        a aVar = this.m;
                        if (aVar == null) {
                            aVar = new a();
                            this.m = aVar;
                        }
                        aVar.a(e.a);
                        return;
                    }
                    this.k = true;
                    this.j.onCompleted();
                }
            }
        }
    }

    @Override // j0.g
    public void onError(Throwable th) {
        f.o1(th);
        if (!this.l) {
            synchronized (this) {
                if (!this.l) {
                    this.l = true;
                    if (this.k) {
                        a aVar = this.m;
                        if (aVar == null) {
                            aVar = new a();
                            this.m = aVar;
                        }
                        aVar.a(new e.c(th));
                        return;
                    }
                    this.k = true;
                    this.j.onError(th);
                }
            }
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:63:0x002d, code lost:
        continue;
     */
    @Override // j0.g
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void onNext(T r7) {
        /*
            r6 = this;
            boolean r0 = r6.l
            if (r0 == 0) goto L5
            return
        L5:
            monitor-enter(r6)
            boolean r0 = r6.l     // Catch: java.lang.Throwable -> L6f
            if (r0 == 0) goto Lc
            monitor-exit(r6)     // Catch: java.lang.Throwable -> L6f
            return
        Lc:
            boolean r0 = r6.k     // Catch: java.lang.Throwable -> L6f
            if (r0 == 0) goto L24
            j0.n.c$a r0 = r6.m     // Catch: java.lang.Throwable -> L6f
            if (r0 != 0) goto L1b
            j0.n.c$a r0 = new j0.n.c$a     // Catch: java.lang.Throwable -> L6f
            r0.<init>()     // Catch: java.lang.Throwable -> L6f
            r6.m = r0     // Catch: java.lang.Throwable -> L6f
        L1b:
            if (r7 != 0) goto L1f
            java.lang.Object r7 = j0.l.a.e.f3743b     // Catch: java.lang.Throwable -> L6f
        L1f:
            r0.a(r7)     // Catch: java.lang.Throwable -> L6f
            monitor-exit(r6)     // Catch: java.lang.Throwable -> L6f
            return
        L24:
            r0 = 1
            r6.k = r0     // Catch: java.lang.Throwable -> L6f
            monitor-exit(r6)     // Catch: java.lang.Throwable -> L6f
            j0.g<? super T> r1 = r6.j     // Catch: java.lang.Throwable -> L66
            r1.onNext(r7)     // Catch: java.lang.Throwable -> L66
        L2d:
            monitor-enter(r6)
            j0.n.c$a r1 = r6.m     // Catch: java.lang.Throwable -> L63
            r2 = 0
            if (r1 != 0) goto L37
            r6.k = r2     // Catch: java.lang.Throwable -> L63
            monitor-exit(r6)     // Catch: java.lang.Throwable -> L63
            return
        L37:
            r3 = 0
            r6.m = r3     // Catch: java.lang.Throwable -> L63
            monitor-exit(r6)     // Catch: java.lang.Throwable -> L63
            java.lang.Object[] r1 = r1.a
            int r3 = r1.length
        L3e:
            if (r2 >= r3) goto L2d
            r4 = r1[r2]
            if (r4 != 0) goto L45
            goto L2d
        L45:
            j0.g<? super T> r5 = r6.j     // Catch: java.lang.Throwable -> L53
            boolean r4 = j0.l.a.e.a(r5, r4)     // Catch: java.lang.Throwable -> L53
            if (r4 == 0) goto L50
            r6.l = r0     // Catch: java.lang.Throwable -> L53
            return
        L50:
            int r2 = r2 + 1
            goto L3e
        L53:
            r1 = move-exception
            r6.l = r0
            b.i.a.f.e.o.f.o1(r1)
            j0.g<? super T> r0 = r6.j
            java.lang.Throwable r7 = rx.exceptions.OnErrorThrowable.a(r1, r7)
            r0.onError(r7)
            return
        L63:
            r7 = move-exception
            monitor-exit(r6)     // Catch: java.lang.Throwable -> L63
            throw r7
        L66:
            r1 = move-exception
            r6.l = r0
            j0.g<? super T> r0 = r6.j
            b.i.a.f.e.o.f.p1(r1, r0, r7)
            return
        L6f:
            r7 = move-exception
            monitor-exit(r6)     // Catch: java.lang.Throwable -> L6f
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: j0.n.c.onNext(java.lang.Object):void");
    }
}
