package j0;

import rx.Subscription;
import rx.internal.util.SubscriptionList;
/* compiled from: SingleSubscriber.java */
/* loaded from: classes3.dex */
public abstract class i<T> implements Subscription {
    public final SubscriptionList j = new SubscriptionList();

    public abstract void a(Throwable th);

    public abstract void b(T t);

    @Override // rx.Subscription
    public final boolean isUnsubscribed() {
        return this.j.k;
    }

    @Override // rx.Subscription
    public final void unsubscribe() {
        this.j.unsubscribe();
    }
}
