package j0.r;

import j0.l.d.a;
import rx.Subscription;
/* compiled from: MultipleAssignmentSubscription.java */
/* loaded from: classes3.dex */
public final class b implements Subscription {
    public final a j = new a();

    public void a(Subscription subscription) {
        this.j.a(subscription);
    }

    @Override // rx.Subscription
    public boolean isUnsubscribed() {
        return this.j.isUnsubscribed();
    }

    @Override // rx.Subscription
    public void unsubscribe() {
        this.j.unsubscribe();
    }
}
