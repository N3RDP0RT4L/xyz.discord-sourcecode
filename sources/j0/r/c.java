package j0.r;

import rx.Subscription;
/* compiled from: Subscriptions.java */
/* loaded from: classes3.dex */
public final class c {
    public static final a a = new a();

    /* compiled from: Subscriptions.java */
    /* loaded from: classes3.dex */
    public static final class a implements Subscription {
        @Override // rx.Subscription
        public boolean isUnsubscribed() {
            return true;
        }

        @Override // rx.Subscription
        public void unsubscribe() {
        }
    }
}
