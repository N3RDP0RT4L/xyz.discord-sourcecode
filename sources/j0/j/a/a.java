package j0.j.a;

import java.util.concurrent.atomic.AtomicReference;
/* compiled from: RxAndroidPlugins.java */
/* loaded from: classes3.dex */
public final class a {
    public static final a a = new a();

    /* renamed from: b  reason: collision with root package name */
    public final AtomicReference<b> f3741b = new AtomicReference<>();

    public b a() {
        if (this.f3741b.get() == null) {
            this.f3741b.compareAndSet(null, b.a);
        }
        return this.f3741b.get();
    }
}
