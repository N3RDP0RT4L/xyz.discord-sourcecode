package j0.j.b;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import j0.o.o;
import j0.r.c;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import rx.Scheduler;
import rx.Subscription;
import rx.exceptions.OnErrorNotImplementedException;
import rx.functions.Action0;
/* compiled from: LooperScheduler.java */
/* loaded from: classes3.dex */
public class b extends Scheduler {
    public final Handler a;

    /* compiled from: LooperScheduler.java */
    /* loaded from: classes3.dex */
    public static class a extends Scheduler.Worker {
        public final Handler j;
        public final j0.j.a.b k = j0.j.a.a.a.a();
        public volatile boolean l;

        public a(Handler handler) {
            this.j = handler;
        }

        @Override // rx.Scheduler.Worker
        public Subscription a(Action0 action0) {
            return b(action0, 0L, TimeUnit.MILLISECONDS);
        }

        @Override // rx.Scheduler.Worker
        public Subscription b(Action0 action0, long j, TimeUnit timeUnit) {
            if (this.l) {
                return c.a;
            }
            Objects.requireNonNull(this.k);
            Handler handler = this.j;
            RunnableC0398b bVar = new RunnableC0398b(action0, handler);
            Message obtain = Message.obtain(handler, bVar);
            obtain.obj = this;
            this.j.sendMessageDelayed(obtain, timeUnit.toMillis(j));
            if (!this.l) {
                return bVar;
            }
            this.j.removeCallbacks(bVar);
            return c.a;
        }

        @Override // rx.Subscription
        public boolean isUnsubscribed() {
            return this.l;
        }

        @Override // rx.Subscription
        public void unsubscribe() {
            this.l = true;
            this.j.removeCallbacksAndMessages(this);
        }
    }

    /* compiled from: LooperScheduler.java */
    /* renamed from: j0.j.b.b$b  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class RunnableC0398b implements Runnable, Subscription {
        public final Action0 j;
        public final Handler k;
        public volatile boolean l;

        public RunnableC0398b(Action0 action0, Handler handler) {
            this.j = action0;
            this.k = handler;
        }

        @Override // rx.Subscription
        public boolean isUnsubscribed() {
            return this.l;
        }

        @Override // java.lang.Runnable
        public void run() {
            IllegalStateException illegalStateException;
            try {
                this.j.call();
            } catch (Throwable th) {
                if (th instanceof OnErrorNotImplementedException) {
                    illegalStateException = new IllegalStateException("Exception thrown on Scheduler.Worker thread. Add `onError` handling.", th);
                } else {
                    illegalStateException = new IllegalStateException("Fatal Exception thrown on Scheduler.Worker thread.", th);
                }
                Objects.requireNonNull(o.a.b());
                Thread currentThread = Thread.currentThread();
                currentThread.getUncaughtExceptionHandler().uncaughtException(currentThread, illegalStateException);
            }
        }

        @Override // rx.Subscription
        public void unsubscribe() {
            this.l = true;
            this.k.removeCallbacks(this);
        }
    }

    public b(Looper looper) {
        this.a = new Handler(looper);
    }

    @Override // rx.Scheduler
    public Scheduler.Worker a() {
        return new a(this.a);
    }
}
