package j0.j.b;

import android.os.Looper;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import rx.Scheduler;
/* compiled from: AndroidSchedulers.java */
/* loaded from: classes3.dex */
public final class a {
    public static final AtomicReference<a> a = new AtomicReference<>();

    /* renamed from: b  reason: collision with root package name */
    public final Scheduler f3742b = new b(Looper.getMainLooper());

    public a() {
        Objects.requireNonNull(j0.j.a.a.a.a());
    }

    public static Scheduler a() {
        AtomicReference<a> atomicReference;
        a aVar;
        do {
            atomicReference = a;
            aVar = atomicReference.get();
            if (aVar != null) {
                break;
            }
            aVar = new a();
        } while (!atomicReference.compareAndSet(null, aVar));
        return aVar.f3742b;
    }
}
