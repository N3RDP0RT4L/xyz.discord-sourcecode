package j0.k;

import rx.functions.Func4;
import rx.functions.FuncN;
/* compiled from: Functions.java */
/* loaded from: classes3.dex */
public final class f implements FuncN<R> {
    public final /* synthetic */ Func4 a;

    public f(Func4 func4) {
        this.a = func4;
    }

    /* JADX WARN: Type inference failed for: r6v3, types: [R, java.lang.Object] */
    @Override // rx.functions.FuncN
    public R call(Object... objArr) {
        if (objArr.length == 4) {
            return this.a.call(objArr[0], objArr[1], objArr[2], objArr[3]);
        }
        throw new IllegalArgumentException("Func4 expecting 4 arguments.");
    }
}
