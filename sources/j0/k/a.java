package j0.k;

import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Action2;
import rx.functions.Action3;
/* compiled from: Actions.java */
/* loaded from: classes3.dex */
public final class a {
    public static final C0399a a = new C0399a();

    /* compiled from: Actions.java */
    /* renamed from: j0.k.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0399a<T0, T1, T2, T3, T4, T5, T6, T7, T8> implements Action0, Action1<T0>, Action2<T0, T1>, Action3<T0, T1, T2> {
        @Override // rx.functions.Action0
        public void call() {
        }

        @Override // rx.functions.Action1
        public void call(T0 t0) {
        }

        @Override // rx.functions.Action2
        public void call(T0 t0, T1 t1) {
        }

        @Override // rx.functions.Action3
        public void call(T0 t0, T1 t1, T2 t2) {
        }
    }
}
