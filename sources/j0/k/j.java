package j0.k;

import rx.functions.Func8;
import rx.functions.FuncN;
/* compiled from: Functions.java */
/* loaded from: classes3.dex */
public final class j implements FuncN<R> {
    public final /* synthetic */ Func8 a;

    public j(Func8 func8) {
        this.a = func8;
    }

    /* JADX WARN: Type inference failed for: r12v2, types: [R, java.lang.Object] */
    @Override // rx.functions.FuncN
    public R call(Object... objArr) {
        if (objArr.length == 8) {
            return this.a.call(objArr[0], objArr[1], objArr[2], objArr[3], objArr[4], objArr[5], objArr[6], objArr[7]);
        }
        throw new IllegalArgumentException("Func8 expecting 8 arguments.");
    }
}
