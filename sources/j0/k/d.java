package j0.k;

import rx.functions.Func2;
import rx.functions.FuncN;
/* compiled from: Functions.java */
/* loaded from: classes3.dex */
public final class d implements FuncN<R> {
    public final /* synthetic */ Func2 a;

    public d(Func2 func2) {
        this.a = func2;
    }

    /* JADX WARN: Type inference failed for: r4v3, types: [R, java.lang.Object] */
    @Override // rx.functions.FuncN
    public R call(Object... objArr) {
        if (objArr.length == 2) {
            return this.a.call(objArr[0], objArr[1]);
        }
        throw new IllegalArgumentException("Func2 expecting 2 arguments.");
    }
}
