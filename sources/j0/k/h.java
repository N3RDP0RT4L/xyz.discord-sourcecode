package j0.k;

import rx.functions.Func6;
import rx.functions.FuncN;
/* compiled from: Functions.java */
/* loaded from: classes3.dex */
public final class h implements FuncN<R> {
    public final /* synthetic */ Func6 a;

    public h(Func6 func6) {
        this.a = func6;
    }

    /* JADX WARN: Type inference failed for: r10v2, types: [R, java.lang.Object] */
    @Override // rx.functions.FuncN
    public R call(Object... objArr) {
        if (objArr.length == 6) {
            return this.a.call(objArr[0], objArr[1], objArr[2], objArr[3], objArr[4], objArr[5]);
        }
        throw new IllegalArgumentException("Func6 expecting 6 arguments.");
    }
}
