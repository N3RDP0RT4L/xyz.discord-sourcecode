package j0.k;

import rx.functions.Func3;
import rx.functions.FuncN;
/* compiled from: Functions.java */
/* loaded from: classes3.dex */
public final class e implements FuncN<R> {
    public final /* synthetic */ Func3 a;

    public e(Func3 func3) {
        this.a = func3;
    }

    /* JADX WARN: Type inference failed for: r5v3, types: [R, java.lang.Object] */
    @Override // rx.functions.FuncN
    public R call(Object... objArr) {
        if (objArr.length == 3) {
            return this.a.call(objArr[0], objArr[1], objArr[2]);
        }
        throw new IllegalArgumentException("Func3 expecting 3 arguments.");
    }
}
