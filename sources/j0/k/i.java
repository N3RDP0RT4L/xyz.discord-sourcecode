package j0.k;

import rx.functions.Func7;
import rx.functions.FuncN;
/* compiled from: Functions.java */
/* loaded from: classes3.dex */
public final class i implements FuncN<R> {
    public final /* synthetic */ Func7 a;

    public i(Func7 func7) {
        this.a = func7;
    }

    /* JADX WARN: Type inference failed for: r11v2, types: [R, java.lang.Object] */
    @Override // rx.functions.FuncN
    public R call(Object... objArr) {
        if (objArr.length == 7) {
            return this.a.call(objArr[0], objArr[1], objArr[2], objArr[3], objArr[4], objArr[5], objArr[6]);
        }
        throw new IllegalArgumentException("Func7 expecting 7 arguments.");
    }
}
