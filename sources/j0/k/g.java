package j0.k;

import rx.functions.Func5;
import rx.functions.FuncN;
/* compiled from: Functions.java */
/* loaded from: classes3.dex */
public final class g implements FuncN<R> {
    public final /* synthetic */ Func5 a;

    public g(Func5 func5) {
        this.a = func5;
    }

    /* JADX WARN: Type inference failed for: r9v2, types: [R, java.lang.Object] */
    @Override // rx.functions.FuncN
    public R call(Object... objArr) {
        if (objArr.length == 5) {
            return this.a.call(objArr[0], objArr[1], objArr[2], objArr[3], objArr[4]);
        }
        throw new IllegalArgumentException("Func5 expecting 5 arguments.");
    }
}
