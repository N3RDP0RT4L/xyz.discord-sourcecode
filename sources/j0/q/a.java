package j0.q;

import b.i.a.f.e.o.f;
import j0.g;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import rx.Observable;
import rx.Producer;
import rx.Subscriber;
import rx.Subscription;
import rx.subjects.Subject;
/* compiled from: ReplaySubject.java */
/* loaded from: classes3.dex */
public final class a<T> extends Subject<T, T> {
    public static final Object[] k = new Object[0];
    public final d<T> l;

    /* compiled from: ReplaySubject.java */
    /* renamed from: j0.q.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public interface AbstractC0416a<T> {
    }

    /* compiled from: ReplaySubject.java */
    /* loaded from: classes3.dex */
    public static final class b<T> extends AtomicInteger implements Producer, Subscription {
        private static final long serialVersionUID = -5006209596735204567L;
        public final Subscriber<? super T> actual;
        public int index;
        public Object node;
        public final AtomicLong requested = new AtomicLong();
        public final d<T> state;
        public int tailIndex;

        public b(Subscriber<? super T> subscriber, d<T> dVar) {
            this.actual = subscriber;
            this.state = dVar;
        }

        @Override // rx.Subscription
        public boolean isUnsubscribed() {
            return this.actual.isUnsubscribed();
        }

        @Override // rx.Producer
        public void j(long j) {
            int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
            if (i > 0) {
                f.c0(this.requested, j);
                ((c) this.state.buffer).a(this);
            } else if (i < 0) {
                throw new IllegalArgumentException(b.d.b.a.a.s("n >= required but it was ", j));
            }
        }

        @Override // rx.Subscription
        public void unsubscribe() {
            this.state.a(this);
        }
    }

    /* compiled from: ReplaySubject.java */
    /* loaded from: classes3.dex */
    public static final class c<T> implements AbstractC0416a<T> {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public volatile C0417a<T> f3776b;
        public C0417a<T> c;
        public int d;
        public volatile boolean e;
        public Throwable f;

        /* compiled from: ReplaySubject.java */
        /* renamed from: j0.q.a$c$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0417a<T> extends AtomicReference<C0417a<T>> {
            private static final long serialVersionUID = 3713592843205853725L;
            public final T value;

            public C0417a(T t) {
                this.value = t;
            }
        }

        public c(int i) {
            this.a = i;
            C0417a<T> aVar = new C0417a<>(null);
            this.c = aVar;
            this.f3776b = aVar;
        }

        /* JADX WARN: Code restructure failed: missing block: B:29:0x005e, code lost:
            if (r14 != 0) goto L44;
         */
        /* JADX WARN: Code restructure failed: missing block: B:31:0x0064, code lost:
            if (r2.isUnsubscribed() == false) goto L34;
         */
        /* JADX WARN: Code restructure failed: missing block: B:32:0x0066, code lost:
            r18.node = null;
         */
        /* JADX WARN: Code restructure failed: missing block: B:33:0x0068, code lost:
            return;
         */
        /* JADX WARN: Code restructure failed: missing block: B:34:0x0069, code lost:
            r3 = r17.e;
         */
        /* JADX WARN: Code restructure failed: missing block: B:35:0x006f, code lost:
            if (r7.get() != null) goto L37;
         */
        /* JADX WARN: Code restructure failed: missing block: B:36:0x0071, code lost:
            r12 = true;
         */
        /* JADX WARN: Code restructure failed: missing block: B:37:0x0072, code lost:
            if (r3 == false) goto L44;
         */
        /* JADX WARN: Code restructure failed: missing block: B:38:0x0074, code lost:
            if (r12 == false) goto L44;
         */
        /* JADX WARN: Code restructure failed: missing block: B:39:0x0076, code lost:
            r18.node = null;
            r1 = r17.f;
         */
        /* JADX WARN: Code restructure failed: missing block: B:40:0x007a, code lost:
            if (r1 == null) goto L42;
         */
        /* JADX WARN: Code restructure failed: missing block: B:41:0x007c, code lost:
            r2.onError(r1);
         */
        /* JADX WARN: Code restructure failed: missing block: B:42:0x0080, code lost:
            r2.onCompleted();
         */
        /* JADX WARN: Code restructure failed: missing block: B:43:0x0083, code lost:
            return;
         */
        /* JADX WARN: Code restructure failed: missing block: B:45:0x0086, code lost:
            if (r10 == 0) goto L49;
         */
        /* JADX WARN: Code restructure failed: missing block: B:47:0x008f, code lost:
            if (r5 == androidx.recyclerview.widget.RecyclerView.FOREVER_NS) goto L49;
         */
        /* JADX WARN: Code restructure failed: missing block: B:48:0x0091, code lost:
            b.i.a.f.e.o.f.U0(r18.requested, r10);
         */
        /* JADX WARN: Code restructure failed: missing block: B:49:0x0096, code lost:
            r18.node = r7;
            r4 = r18.addAndGet(-r4);
         */
        /* JADX WARN: Code restructure failed: missing block: B:60:?, code lost:
            return;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public void a(j0.q.a.b<T> r18) {
            /*
                r17 = this;
                r0 = r17
                r1 = r18
                int r2 = r18.getAndIncrement()
                if (r2 == 0) goto Lb
                return
            Lb:
                rx.Subscriber<? super T> r2 = r1.actual
                r4 = 1
            Le:
                java.util.concurrent.atomic.AtomicLong r5 = r1.requested
                long r5 = r5.get()
                java.lang.Object r7 = r1.node
                j0.q.a$c$a r7 = (j0.q.a.c.C0417a) r7
                r8 = 0
                if (r7 != 0) goto L1e
                j0.q.a$c$a<T> r7 = r0.f3776b
            L1e:
                r10 = r8
            L1f:
                r12 = 0
                r13 = 0
                int r14 = (r10 > r5 ? 1 : (r10 == r5 ? 0 : -1))
                if (r14 == 0) goto L5e
                boolean r15 = r2.isUnsubscribed()
                if (r15 == 0) goto L2e
                r1.node = r13
                return
            L2e:
                boolean r15 = r0.e
                java.lang.Object r16 = r7.get()
                r3 = r16
                j0.q.a$c$a r3 = (j0.q.a.c.C0417a) r3
                if (r3 != 0) goto L3d
                r16 = 1
                goto L3f
            L3d:
                r16 = 0
            L3f:
                if (r15 == 0) goto L51
                if (r16 == 0) goto L51
                r1.node = r13
                java.lang.Throwable r1 = r0.f
                if (r1 == 0) goto L4d
                r2.onError(r1)
                goto L50
            L4d:
                r2.onCompleted()
            L50:
                return
            L51:
                if (r16 == 0) goto L54
                goto L5e
            L54:
                T r7 = r3.value
                r2.onNext(r7)
                r12 = 1
                long r10 = r10 + r12
                r7 = r3
                goto L1f
            L5e:
                if (r14 != 0) goto L84
                boolean r3 = r2.isUnsubscribed()
                if (r3 == 0) goto L69
                r1.node = r13
                return
            L69:
                boolean r3 = r0.e
                java.lang.Object r14 = r7.get()
                if (r14 != 0) goto L72
                r12 = 1
            L72:
                if (r3 == 0) goto L84
                if (r12 == 0) goto L84
                r1.node = r13
                java.lang.Throwable r1 = r0.f
                if (r1 == 0) goto L80
                r2.onError(r1)
                goto L83
            L80:
                r2.onCompleted()
            L83:
                return
            L84:
                int r3 = (r10 > r8 ? 1 : (r10 == r8 ? 0 : -1))
                if (r3 == 0) goto L96
                r8 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
                int r3 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
                if (r3 == 0) goto L96
                java.util.concurrent.atomic.AtomicLong r3 = r1.requested
                b.i.a.f.e.o.f.U0(r3, r10)
            L96:
                r1.node = r7
                int r3 = -r4
                int r4 = r1.addAndGet(r3)
                if (r4 != 0) goto Le
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: j0.q.a.c.a(j0.q.a$b):void");
        }
    }

    /* compiled from: ReplaySubject.java */
    /* loaded from: classes3.dex */
    public static final class d<T> extends AtomicReference<b<T>[]> implements Observable.a<T>, g<T> {
        public static final b[] j = new b[0];
        public static final b[] k = new b[0];
        private static final long serialVersionUID = 5952362471246910544L;
        public final AbstractC0416a<T> buffer;

        public d(AbstractC0416a<T> aVar) {
            this.buffer = aVar;
            lazySet(j);
        }

        public void a(b<T> bVar) {
            b<T>[] bVarArr;
            b[] bVarArr2;
            do {
                bVarArr = get();
                if (bVarArr != k && bVarArr != j) {
                    int length = bVarArr.length;
                    int i = 0;
                    while (true) {
                        if (i >= length) {
                            i = -1;
                            break;
                        } else if (bVarArr[i] == bVar) {
                            break;
                        } else {
                            i++;
                        }
                    }
                    if (i >= 0) {
                        if (length == 1) {
                            bVarArr2 = j;
                        } else {
                            b[] bVarArr3 = new b[length - 1];
                            System.arraycopy(bVarArr, 0, bVarArr3, 0, i);
                            System.arraycopy(bVarArr, i + 1, bVarArr3, i, (length - i) - 1);
                            bVarArr2 = bVarArr3;
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            } while (!compareAndSet(bVarArr, bVarArr2));
        }

        @Override // rx.functions.Action1
        public void call(Object obj) {
            boolean z2;
            Subscriber subscriber = (Subscriber) obj;
            b<T> bVar = new b<>(subscriber, this);
            subscriber.add(bVar);
            subscriber.setProducer(bVar);
            while (true) {
                b<T>[] bVarArr = get();
                z2 = false;
                if (bVarArr != k) {
                    int length = bVarArr.length;
                    b[] bVarArr2 = new b[length + 1];
                    System.arraycopy(bVarArr, 0, bVarArr2, 0, length);
                    bVarArr2[length] = bVar;
                    if (compareAndSet(bVarArr, bVarArr2)) {
                        z2 = true;
                        break;
                    }
                } else {
                    break;
                }
            }
            if (!z2 || !bVar.isUnsubscribed()) {
                ((c) this.buffer).a(bVar);
            } else {
                a(bVar);
            }
        }

        @Override // j0.g
        public void onCompleted() {
            AbstractC0416a<T> aVar = this.buffer;
            ((c) aVar).e = true;
            for (b<T> bVar : getAndSet(k)) {
                ((c) aVar).a(bVar);
            }
        }

        @Override // j0.g
        public void onError(Throwable th) {
            AbstractC0416a<T> aVar = this.buffer;
            c cVar = (c) aVar;
            cVar.f = th;
            cVar.e = true;
            ArrayList arrayList = null;
            for (b<T> bVar : getAndSet(k)) {
                try {
                    ((c) aVar).a(bVar);
                } catch (Throwable th2) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(th2);
                }
            }
            f.n1(arrayList);
        }

        @Override // j0.g
        public void onNext(T t) {
            AbstractC0416a<T> aVar = this.buffer;
            c cVar = (c) aVar;
            Objects.requireNonNull(cVar);
            c.C0417a<T> aVar2 = new c.C0417a<>(t);
            cVar.c.set(aVar2);
            cVar.c = aVar2;
            int i = cVar.d;
            if (i == cVar.a) {
                cVar.f3776b = cVar.f3776b.get();
            } else {
                cVar.d = i + 1;
            }
            for (b<T> bVar : get()) {
                ((c) aVar).a(bVar);
            }
        }
    }

    public a(d<T> dVar) {
        super(dVar);
        this.l = dVar;
    }

    @Override // j0.g
    public void onCompleted() {
        this.l.onCompleted();
    }

    @Override // j0.g
    public void onError(Throwable th) {
        this.l.onError(th);
    }

    @Override // j0.g
    public void onNext(T t) {
        this.l.onNext(t);
    }
}
