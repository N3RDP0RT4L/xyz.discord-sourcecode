package j0.q;

import j0.g;
import j0.k.a;
import j0.l.a.e;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;
/* compiled from: SubjectSubscriptionManager.java */
/* loaded from: classes3.dex */
public final class c<T> extends AtomicReference<a<T>> implements Observable.a<T> {
    private static final long serialVersionUID = 6035251036011671568L;
    public boolean active = true;
    public volatile Object latest;
    public Action1<b<T>> onAdded;
    public Action1<b<T>> onStart;
    public Action1<b<T>> onTerminated;

    /* compiled from: SubjectSubscriptionManager.java */
    /* loaded from: classes3.dex */
    public static final class a<T> {
        public static final b[] a;

        /* renamed from: b  reason: collision with root package name */
        public static final a f3777b;
        public static final a c;
        public final boolean d;
        public final b[] e;

        static {
            b[] bVarArr = new b[0];
            a = bVarArr;
            f3777b = new a(true, bVarArr);
            c = new a(false, bVarArr);
        }

        public a(boolean z2, b[] bVarArr) {
            this.d = z2;
            this.e = bVarArr;
        }
    }

    /* compiled from: SubjectSubscriptionManager.java */
    /* loaded from: classes3.dex */
    public static final class b<T> implements g<T> {
        public final Subscriber<? super T> j;
        public boolean k = true;
        public boolean l;
        public List<Object> m;
        public boolean n;

        public b(Subscriber<? super T> subscriber) {
            this.j = subscriber;
        }

        public void a(Object obj) {
            if (!this.n) {
                synchronized (this) {
                    this.k = false;
                    if (this.l) {
                        if (this.m == null) {
                            this.m = new ArrayList();
                        }
                        this.m.add(obj);
                        return;
                    }
                    this.n = true;
                }
            }
            e.a(this.j, obj);
        }

        @Override // j0.g
        public void onCompleted() {
            this.j.onCompleted();
        }

        @Override // j0.g
        public void onError(Throwable th) {
            this.j.onError(th);
        }

        @Override // j0.g
        public void onNext(T t) {
            this.j.onNext(t);
        }
    }

    public c() {
        super(a.c);
        a.C0399a aVar = j0.k.a.a;
        this.onStart = aVar;
        this.onAdded = aVar;
        this.onTerminated = aVar;
    }

    public void a(b<T> bVar) {
        a<T> aVar;
        a<T> aVar2;
        do {
            aVar = get();
            if (!aVar.d) {
                b<T>[] bVarArr = aVar.e;
                int length = bVarArr.length;
                if (length != 1 || bVarArr[0] != bVar) {
                    if (length != 0) {
                        int i = length - 1;
                        b[] bVarArr2 = new b[i];
                        int i2 = 0;
                        for (b<T> bVar2 : bVarArr) {
                            if (bVar2 != bVar) {
                                if (i2 != i) {
                                    i2++;
                                    bVarArr2[i2] = bVar2;
                                }
                            }
                        }
                        if (i2 == 0) {
                            aVar2 = a.c;
                        } else {
                            if (i2 < i) {
                                b[] bVarArr3 = new b[i2];
                                System.arraycopy(bVarArr2, 0, bVarArr3, 0, i2);
                                bVarArr2 = bVarArr3;
                            }
                            aVar2 = new a<>(aVar.d, bVarArr2);
                        }
                    }
                    aVar2 = aVar;
                    break;
                } else {
                    aVar2 = a.c;
                }
                if (aVar2 == aVar) {
                    return;
                }
            } else {
                return;
            }
        } while (!compareAndSet(aVar, aVar2));
    }

    public b<T>[] b(Object obj) {
        this.latest = obj;
        this.active = false;
        if (get().d) {
            return a.a;
        }
        return getAndSet(a.f3777b).e;
    }

    @Override // rx.functions.Action1
    public void call(Object obj) {
        boolean z2;
        Subscriber subscriber = (Subscriber) obj;
        b<T> bVar = new b<>(subscriber);
        subscriber.add(new j0.r.a(new j0.q.b(this, bVar)));
        this.onStart.call(bVar);
        if (!subscriber.isUnsubscribed()) {
            while (true) {
                a<T> aVar = get();
                z2 = false;
                if (!aVar.d) {
                    b[] bVarArr = aVar.e;
                    int length = bVarArr.length;
                    b[] bVarArr2 = new b[length + 1];
                    System.arraycopy(bVarArr, 0, bVarArr2, 0, length);
                    bVarArr2[length] = bVar;
                    if (compareAndSet(aVar, new a(aVar.d, bVarArr2))) {
                        this.onAdded.call(bVar);
                        z2 = true;
                        break;
                    }
                } else {
                    this.onTerminated.call(bVar);
                    break;
                }
            }
            if (z2 && subscriber.isUnsubscribed()) {
                a(bVar);
            }
        }
    }
}
