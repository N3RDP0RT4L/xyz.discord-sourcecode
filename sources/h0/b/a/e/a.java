package h0.b.a.e;

import java.io.ObjectInputStream;
import java.lang.reflect.Method;
import org.objenesis.ObjenesisException;
/* compiled from: PercInstantiator.java */
/* loaded from: classes3.dex */
public class a<T> implements h0.b.a.a<T> {
    public final Method a;

    /* renamed from: b  reason: collision with root package name */
    public final Object[] f3704b;

    public a(Class<T> cls) {
        Object[] objArr = {null, Boolean.FALSE};
        this.f3704b = objArr;
        objArr[0] = cls;
        try {
            Method declaredMethod = ObjectInputStream.class.getDeclaredMethod("newInstance", Class.class, Boolean.TYPE);
            this.a = declaredMethod;
            declaredMethod.setAccessible(true);
        } catch (NoSuchMethodException e) {
            throw new ObjenesisException(e);
        } catch (RuntimeException e2) {
            throw new ObjenesisException(e2);
        }
    }

    @Override // h0.b.a.a
    public T newInstance() {
        try {
            return (T) this.a.invoke(null, this.f3704b);
        } catch (Exception e) {
            throw new ObjenesisException(e);
        }
    }
}
