package h0.b.a.f;

import h0.b.a.a;
import java.lang.reflect.Field;
import org.objenesis.ObjenesisException;
import sun.misc.Unsafe;
/* compiled from: UnsafeFactoryInstantiator.java */
/* loaded from: classes3.dex */
public class b<T> implements a<T> {
    public static Unsafe a;

    /* renamed from: b  reason: collision with root package name */
    public final Class<T> f3705b;

    public b(Class<T> cls) {
        if (a == null) {
            try {
                Field declaredField = Unsafe.class.getDeclaredField("theUnsafe");
                declaredField.setAccessible(true);
                try {
                    a = (Unsafe) declaredField.get(null);
                } catch (IllegalAccessException e) {
                    throw new ObjenesisException(e);
                }
            } catch (NoSuchFieldException e2) {
                throw new ObjenesisException(e2);
            }
        }
        this.f3705b = cls;
    }

    @Override // h0.b.a.a
    public T newInstance() {
        try {
            Class<T> cls = this.f3705b;
            return cls.cast(a.allocateInstance(cls));
        } catch (InstantiationException e) {
            throw new ObjenesisException(e);
        }
    }
}
