package h0.b.a.b;

import java.io.ObjectInputStream;
import java.lang.reflect.Method;
import org.objenesis.ObjenesisException;
/* compiled from: Android10Instantiator.java */
/* loaded from: classes3.dex */
public class a<T> implements h0.b.a.a<T> {
    public final Class<T> a;

    /* renamed from: b  reason: collision with root package name */
    public final Method f3700b;

    public a(Class<T> cls) {
        this.a = cls;
        try {
            Method declaredMethod = ObjectInputStream.class.getDeclaredMethod("newInstance", Class.class, Class.class);
            declaredMethod.setAccessible(true);
            this.f3700b = declaredMethod;
        } catch (NoSuchMethodException e) {
            throw new ObjenesisException(e);
        } catch (RuntimeException e2) {
            throw new ObjenesisException(e2);
        }
    }

    @Override // h0.b.a.a
    public T newInstance() {
        try {
            Class<T> cls = this.a;
            return cls.cast(this.f3700b.invoke(null, cls, Object.class));
        } catch (Exception e) {
            throw new ObjenesisException(e);
        }
    }
}
