package h0.b.b;

import h0.b.a.a;
import h0.b.a.c.b;
import java.io.Serializable;
/* compiled from: StdInstantiatorStrategy.java */
/* loaded from: classes3.dex */
public class c implements a {
    @Override // h0.b.b.a
    public <T> a<T> newInstantiatorOf(Class<T> cls) {
        if (b.b("Java HotSpot") || b.b("OpenJDK")) {
            if (!(b.d != null)) {
                return new h0.b.a.f.a(cls);
            }
            if (Serializable.class.isAssignableFrom(cls)) {
                return new b(cls);
            }
            return new h0.b.a.c.a(cls);
        } else if (b.b("Dalvik")) {
            if (b.c) {
                return new h0.b.a.f.b(cls);
            }
            int i = b.f3706b;
            if (i <= 10) {
                return new h0.b.a.b.a(cls);
            }
            if (i <= 17) {
                return new h0.b.a.b.b(cls);
            }
            return new h0.b.a.b.c(cls);
        } else if (b.b("BEA")) {
            return new h0.b.a.f.a(cls);
        } else {
            if (b.b("GNU libgcj")) {
                return new h0.b.a.d.a(cls);
            }
            if (b.b("PERC")) {
                return new h0.b.a.e.a(cls);
            }
            return new h0.b.a.f.b(cls);
        }
    }
}
