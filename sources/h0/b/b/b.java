package h0.b.b;

import org.objenesis.ObjenesisException;
/* compiled from: PlatformDescription.java */
/* loaded from: classes3.dex */
public final class b {
    public static final String a = System.getProperty("java.vm.name");

    /* renamed from: b  reason: collision with root package name */
    public static final int f3706b = a();
    public static final boolean c;
    public static final String d;

    static {
        String property;
        System.getProperty("java.specification.version");
        System.getProperty("java.runtime.version");
        System.getProperty("java.vm.info");
        System.getProperty("java.vm.version");
        System.getProperty("java.vm.vendor");
        boolean z2 = false;
        if (!(a() == 0 || (property = System.getProperty("java.boot.class.path")) == null || !property.toLowerCase().contains("core-oj.jar"))) {
            z2 = true;
        }
        c = z2;
        d = System.getProperty("com.google.appengine.runtime.version");
    }

    public static int a() {
        Class cls;
        if (!b("Dalvik")) {
            return 0;
        }
        try {
            try {
                try {
                    try {
                        return ((Integer) Class.forName("android.os.Build$VERSION").getField("SDK_INT").get(null)).intValue();
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                } catch (NoSuchFieldException unused) {
                    try {
                        return Integer.parseInt((String) cls.getField("SDK").get(null));
                    } catch (IllegalAccessException e2) {
                        throw new RuntimeException(e2);
                    }
                }
            } catch (NoSuchFieldException e3) {
                throw new ObjenesisException(e3);
            }
        } catch (ClassNotFoundException e4) {
            throw new ObjenesisException(e4);
        }
    }

    public static boolean b(String str) {
        return a.startsWith(str);
    }
}
