package h0.a.a;

import java.util.Objects;
/* compiled from: AnnotationWriter.java */
/* loaded from: classes3.dex */
public final class a {
    public final v a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f3668b;
    public final c c;
    public final int d;
    public int e;
    public final a f;
    public a g;

    public a(v vVar, boolean z2, c cVar, a aVar) {
        this.a = vVar;
        this.f3668b = z2;
        this.c = cVar;
        int i = cVar.f3671b;
        this.d = i == 0 ? -1 : i - 2;
        this.f = aVar;
        if (aVar != null) {
            aVar.g = this;
        }
    }

    public static int b(String str, a[] aVarArr, int i) {
        int i2 = (i * 2) + 7;
        for (int i3 = 0; i3 < i; i3++) {
            a aVar = aVarArr[i3];
            i2 += aVar == null ? 0 : aVar.a(str) - 8;
        }
        return i2;
    }

    public static void d(int i, a[] aVarArr, int i2, c cVar) {
        a aVar;
        int i3 = (i2 * 2) + 1;
        for (int i4 = 0; i4 < i2; i4++) {
            i3 += aVarArr[i4] == null ? 0 : aVar.a(null) - 8;
        }
        cVar.j(i);
        cVar.i(i3);
        cVar.g(i2);
        for (int i5 = 0; i5 < i2; i5++) {
            a aVar2 = null;
            int i6 = 0;
            for (a aVar3 = aVarArr[i5]; aVar3 != null; aVar3 = aVar3.f) {
                aVar3.g();
                i6++;
                aVar2 = aVar3;
            }
            cVar.j(i6);
            while (aVar2 != null) {
                c cVar2 = aVar2.c;
                cVar.h(cVar2.a, 0, cVar2.f3671b);
                aVar2 = aVar2.g;
            }
        }
    }

    public int a(String str) {
        if (str != null) {
            this.a.l(str);
        }
        int i = 8;
        for (a aVar = this; aVar != null; aVar = aVar.f) {
            i += aVar.c.f3671b;
        }
        return i;
    }

    public void c(int i, c cVar) {
        int i2 = 2;
        a aVar = null;
        int i3 = 0;
        for (a aVar2 = this; aVar2 != null; aVar2 = aVar2.f) {
            aVar2.g();
            i2 += aVar2.c.f3671b;
            i3++;
            aVar = aVar2;
        }
        cVar.j(i);
        cVar.i(i2);
        cVar.j(i3);
        while (aVar != null) {
            c cVar2 = aVar.c;
            cVar.h(cVar2.a, 0, cVar2.f3671b);
            aVar = aVar.g;
        }
    }

    public void e(String str, Object obj) {
        this.e++;
        if (this.f3668b) {
            this.c.j(this.a.l(str));
        }
        if (obj instanceof String) {
            this.c.e(115, this.a.l((String) obj));
        } else if (obj instanceof Byte) {
            this.c.e(66, this.a.e(((Byte) obj).byteValue()).a);
        } else if (obj instanceof Boolean) {
            this.c.e(90, this.a.e(((Boolean) obj).booleanValue() ? 1 : 0).a);
        } else if (obj instanceof Character) {
            this.c.e(67, this.a.e(((Character) obj).charValue()).a);
        } else if (obj instanceof Short) {
            this.c.e(83, this.a.e(((Short) obj).shortValue()).a);
        } else if (obj instanceof w) {
            this.c.e(99, this.a.l(((w) obj).d()));
        } else {
            int i = 0;
            if (obj instanceof byte[]) {
                byte[] bArr = (byte[]) obj;
                this.c.e(91, bArr.length);
                int length = bArr.length;
                while (i < length) {
                    this.c.e(66, this.a.e(bArr[i]).a);
                    i++;
                }
            } else if (obj instanceof boolean[]) {
                boolean[] zArr = (boolean[]) obj;
                this.c.e(91, zArr.length);
                int length2 = zArr.length;
                while (i < length2) {
                    this.c.e(90, this.a.e(zArr[i] ? 1 : 0).a);
                    i++;
                }
            } else if (obj instanceof short[]) {
                short[] sArr = (short[]) obj;
                this.c.e(91, sArr.length);
                int length3 = sArr.length;
                while (i < length3) {
                    this.c.e(83, this.a.e(sArr[i]).a);
                    i++;
                }
            } else if (obj instanceof char[]) {
                char[] cArr = (char[]) obj;
                this.c.e(91, cArr.length);
                int length4 = cArr.length;
                while (i < length4) {
                    this.c.e(67, this.a.e(cArr[i]).a);
                    i++;
                }
            } else if (obj instanceof int[]) {
                int[] iArr = (int[]) obj;
                this.c.e(91, iArr.length);
                int length5 = iArr.length;
                while (i < length5) {
                    this.c.e(73, this.a.e(iArr[i]).a);
                    i++;
                }
            } else if (obj instanceof long[]) {
                long[] jArr = (long[]) obj;
                this.c.e(91, jArr.length);
                int length6 = jArr.length;
                while (i < length6) {
                    this.c.e(74, this.a.g(5, jArr[i]).a);
                    i++;
                }
            } else if (obj instanceof float[]) {
                float[] fArr = (float[]) obj;
                this.c.e(91, fArr.length);
                int length7 = fArr.length;
                while (i < length7) {
                    float f = fArr[i];
                    c cVar = this.c;
                    v vVar = this.a;
                    Objects.requireNonNull(vVar);
                    cVar.e(70, vVar.f(4, Float.floatToRawIntBits(f)).a);
                    i++;
                }
            } else if (obj instanceof double[]) {
                double[] dArr = (double[]) obj;
                this.c.e(91, dArr.length);
                int length8 = dArr.length;
                while (i < length8) {
                    double d = dArr[i];
                    c cVar2 = this.c;
                    v vVar2 = this.a;
                    Objects.requireNonNull(vVar2);
                    cVar2.e(68, vVar2.g(6, Double.doubleToRawLongBits(d)).a);
                    i++;
                }
            } else {
                u b2 = this.a.b(obj);
                this.c.e(".s.IFJDCS".charAt(b2.f3696b), b2.a);
            }
        }
    }

    public a f(String str) {
        this.e++;
        if (this.f3668b) {
            this.c.j(this.a.l(str));
        }
        this.c.e(91, 0);
        return new a(this.a, false, this.c, null);
    }

    public void g() {
        int i = this.d;
        if (i != -1) {
            byte[] bArr = this.c.a;
            int i2 = this.e;
            bArr[i] = (byte) (i2 >>> 8);
            bArr[i + 1] = (byte) i2;
        }
    }
}
