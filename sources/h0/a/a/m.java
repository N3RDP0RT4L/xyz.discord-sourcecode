package h0.a.a;

import andhook.lib.xposed.ClassUtils;
import androidx.core.view.ViewCompat;
import com.discord.widgets.chat.input.MentionUtilsKt;
import h0.a.a.v;
import java.util.Objects;
/* compiled from: Frame.java */
/* loaded from: classes3.dex */
public class m {
    public p a;

    /* renamed from: b  reason: collision with root package name */
    public int[] f3683b;
    public int[] c;
    public int[] d;
    public int[] e;
    public short f;
    public short g;
    public int h;
    public int[] i;

    public m(p pVar) {
        this.a = pVar;
    }

    public static int c(v vVar, Object obj) {
        if (obj instanceof Integer) {
            return 16777216 | ((Integer) obj).intValue();
        }
        if (obj instanceof String) {
            return d(vVar, w.h((String) obj).d(), 0);
        }
        return vVar.p("", ((p) obj).e) | 50331648;
    }

    public static int d(v vVar, String str, int i) {
        char charAt = str.charAt(i);
        int i2 = 16777218;
        if (charAt == 'F') {
            return 16777218;
        }
        if (charAt == 'L') {
            return vVar.n(str.substring(i + 1, str.length() - 1)) | 33554432;
        }
        if (charAt != 'S') {
            if (charAt == 'V') {
                return 0;
            }
            if (charAt != 'I') {
                if (charAt == 'J') {
                    return 16777220;
                }
                if (charAt != 'Z') {
                    if (charAt != '[') {
                        switch (charAt) {
                            case 'B':
                            case 'C':
                                break;
                            case 'D':
                                return 16777219;
                            default:
                                throw new IllegalArgumentException();
                        }
                    } else {
                        int i3 = i + 1;
                        while (str.charAt(i3) == '[') {
                            i3++;
                        }
                        char charAt2 = str.charAt(i3);
                        if (charAt2 != 'F') {
                            if (charAt2 == 'L') {
                                i2 = vVar.n(str.substring(i3 + 1, str.length() - 1)) | 33554432;
                            } else if (charAt2 == 'S') {
                                i2 = 16777228;
                            } else if (charAt2 == 'Z') {
                                i2 = 16777225;
                            } else if (charAt2 == 'I') {
                                i2 = 16777217;
                            } else if (charAt2 != 'J') {
                                switch (charAt2) {
                                    case 'B':
                                        i2 = 16777226;
                                        break;
                                    case 'C':
                                        i2 = 16777227;
                                        break;
                                    case 'D':
                                        i2 = 16777219;
                                        break;
                                    default:
                                        throw new IllegalArgumentException();
                                }
                            } else {
                                i2 = 16777220;
                            }
                        }
                        return ((i3 - i) << 28) | i2;
                    }
                }
            }
        }
        return 16777217;
    }

    public static boolean g(v vVar, int i, int[] iArr, int i2) {
        int min;
        int n;
        int i3;
        int i4 = i;
        int i5 = iArr[i2];
        if (i5 == i4) {
            return false;
        }
        if ((268435455 & i4) == 16777221) {
            if (i5 == 16777221) {
                return false;
            }
            i4 = 16777221;
        }
        if (i5 == 0) {
            iArr[i2] = i4;
            return true;
        }
        int i6 = i5 & (-268435456);
        int i7 = 16777216;
        if (i6 != 0 || (i5 & 251658240) == 33554432) {
            if (i4 == 16777221) {
                return false;
            }
            String str = "java/lang/Object";
            if ((i4 & ViewCompat.MEASURED_STATE_MASK) != ((-16777216) & i5)) {
                int i8 = i4 & (-268435456);
                if (i8 != 0 || (i4 & 251658240) == 33554432) {
                    if (!(i8 == 0 || (i4 & 251658240) == 33554432)) {
                        i8 -= 268435456;
                    }
                    if (!(i6 == 0 || (i5 & 251658240) == 33554432)) {
                        i6 -= 268435456;
                    }
                    min = Math.min(i8, i6) | 33554432;
                    n = vVar.n(str);
                    i7 = min | n;
                }
            } else if ((i5 & 251658240) == 33554432) {
                int i9 = (i4 & (-268435456)) | 33554432;
                int i10 = i4 & 1048575;
                int i11 = 1048575 & i5;
                long j = i10 | (i11 << 32);
                int r = v.r(130, i10 + i11);
                v.a q = vVar.q(r);
                while (true) {
                    if (q != null) {
                        if (q.f3696b == 130 && q.h == r && q.f == j) {
                            i3 = q.g;
                            break;
                        }
                        q = q.i;
                    } else {
                        v.a[] aVarArr = vVar.l;
                        String str2 = aVarArr[i10].e;
                        String str3 = aVarArr[i11].e;
                        Objects.requireNonNull(vVar.a);
                        ClassLoader classLoader = f.class.getClassLoader();
                        try {
                            Class<?> cls = Class.forName(str2.replace(MentionUtilsKt.SLASH_CHAR, ClassUtils.PACKAGE_SEPARATOR_CHAR), false, classLoader);
                            try {
                                Class<?> cls2 = Class.forName(str3.replace(MentionUtilsKt.SLASH_CHAR, ClassUtils.PACKAGE_SEPARATOR_CHAR), false, classLoader);
                                if (cls.isAssignableFrom(cls2)) {
                                    str = str2;
                                } else if (cls2.isAssignableFrom(cls)) {
                                    str = str3;
                                } else if (!cls.isInterface() && !cls2.isInterface()) {
                                    do {
                                        cls = cls.getSuperclass();
                                    } while (!cls.isAssignableFrom(cls2));
                                    str = cls.getName().replace(ClassUtils.PACKAGE_SEPARATOR_CHAR, MentionUtilsKt.SLASH_CHAR);
                                }
                                int n2 = vVar.n(str);
                                v.a aVar = new v.a(vVar.k, 130, j, r);
                                vVar.v(aVar);
                                aVar.g = n2;
                                i3 = n2;
                            } catch (ClassNotFoundException e) {
                                throw new TypeNotPresentException(str3, e);
                            }
                        } catch (ClassNotFoundException e2) {
                            throw new TypeNotPresentException(str2, e2);
                        }
                    }
                }
                i7 = i9 | i3;
            } else {
                min = ((i4 & (-268435456)) - 268435456) | 33554432;
                n = vVar.n(str);
                i7 = min | n;
            }
        } else if (i5 == 16777221) {
            if ((i4 & (-268435456)) == 0 && (i4 & 251658240) != 33554432) {
                i4 = 16777216;
            }
            i7 = i4;
        }
        if (i7 == i5) {
            return false;
        }
        iArr[i2] = i7;
        return true;
    }

    public final void a(r rVar) {
        int i;
        int[] iArr = this.f3683b;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        loop0: while (true) {
            int i5 = 0;
            do {
                int i6 = 2;
                if (i3 >= iArr.length) {
                    break loop0;
                }
                i = iArr[i3];
                if (!(i == 16777220 || i == 16777219)) {
                    i6 = 1;
                }
                i3 += i6;
                i5++;
            } while (i == 16777216);
            i4 += i5;
        }
        int[] iArr2 = this.c;
        int i7 = 0;
        int i8 = 0;
        while (i7 < iArr2.length) {
            int i9 = iArr2[i7];
            i7 += (i9 == 16777220 || i9 == 16777219) ? 2 : 1;
            i8++;
        }
        rVar.A(this.a.e, i4, i8);
        int i10 = 3;
        int i11 = 0;
        while (true) {
            i4--;
            if (i4 > 0) {
                int i12 = iArr[i11];
                i11 += (i12 == 16777220 || i12 == 16777219) ? 2 : 1;
                i10++;
                rVar.W[i10] = i12;
            }
        }
        while (true) {
            i8--;
            if (i8 > 0) {
                int i13 = iArr2[i2];
                i2 += (i13 == 16777220 || i13 == 16777219) ? 2 : 1;
                i10++;
                rVar.W[i10] = i13;
            } else {
                rVar.z();
                return;
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Failed to find switch 'out' block
        	at jadx.core.dex.visitors.regions.RegionMaker.processSwitch(RegionMaker.java:817)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:160)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:94)
        	at jadx.core.dex.visitors.regions.RegionMaker.processSwitch(RegionMaker.java:856)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:160)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:94)
        	at jadx.core.dex.visitors.regions.RegionMaker.processSwitch(RegionMaker.java:856)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:160)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:94)
        	at jadx.core.dex.visitors.regions.RegionMaker.processSwitch(RegionMaker.java:856)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:160)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:94)
        	at jadx.core.dex.visitors.regions.RegionMaker.processSwitch(RegionMaker.java:856)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:160)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:94)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:52)
        */
    /* JADX WARN: Removed duplicated region for block: B:100:0x02e5  */
    /* JADX WARN: Removed duplicated region for block: B:101:0x02ed  */
    /* JADX WARN: Removed duplicated region for block: B:102:0x02f8  */
    /* JADX WARN: Removed duplicated region for block: B:118:0x035f  */
    /* JADX WARN: Removed duplicated region for block: B:119:0x0366  */
    /* JADX WARN: Removed duplicated region for block: B:120:0x036a  */
    /* JADX WARN: Removed duplicated region for block: B:121:0x0371  */
    /* JADX WARN: Removed duplicated region for block: B:99:0x02da  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void b(int r17, int r18, h0.a.a.u r19, h0.a.a.v r20) {
        /*
            Method dump skipped, instructions count: 1272
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: h0.a.a.m.b(int, int, h0.a.a.u, h0.a.a.v):void");
    }

    /* JADX WARN: Removed duplicated region for block: B:23:0x004f A[LOOP:0: B:7:0x000d->B:23:0x004f, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0037 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final int e(h0.a.a.v r9, int r10) {
        /*
            r8 = this;
            r0 = 16777222(0x1000006, float:2.3509904E-38)
            if (r10 == r0) goto Lc
            r1 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r1 = r1 & r10
            r2 = 50331648(0x3000000, float:3.761582E-37)
            if (r1 != r2) goto L52
        Lc:
            r1 = 0
        Ld:
            int r2 = r8.h
            if (r1 >= r2) goto L52
            int[] r2 = r8.i
            r2 = r2[r1]
            r3 = -268435456(0xfffffffff0000000, float:-1.58456325E29)
            r3 = r3 & r2
            r4 = 251658240(0xf000000, float:6.3108872E-30)
            r4 = r4 & r2
            r5 = 1048575(0xfffff, float:1.469367E-39)
            r6 = r2 & r5
            r7 = 67108864(0x4000000, float:1.5046328E-36)
            if (r4 != r7) goto L2a
            int[] r2 = r8.f3683b
            r2 = r2[r6]
        L28:
            int r2 = r2 + r3
            goto L35
        L2a:
            r7 = 83886080(0x5000000, float:6.018531E-36)
            if (r4 != r7) goto L35
            int[] r2 = r8.c
            int r4 = r2.length
            int r4 = r4 - r6
            r2 = r2[r4]
            goto L28
        L35:
            if (r10 != r2) goto L4f
            r1 = 33554432(0x2000000, float:9.403955E-38)
            if (r10 != r0) goto L43
            java.lang.String r10 = r9.d
            int r9 = r9.n(r10)
        L41:
            r9 = r9 | r1
            return r9
        L43:
            r10 = r10 & r5
            h0.a.a.v$a[] r0 = r9.l
            r10 = r0[r10]
            java.lang.String r10 = r10.e
            int r9 = r9.n(r10)
            goto L41
        L4f:
            int r1 = r1 + 1
            goto Ld
        L52:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: h0.a.a.m.e(h0.a.a.v, int):int");
    }

    public final int f(int i) {
        int[] iArr = this.d;
        if (iArr == null || i >= iArr.length) {
            return i | 67108864;
        }
        int i2 = iArr[i];
        if (i2 != 0) {
            return i2;
        }
        int i3 = i | 67108864;
        iArr[i] = i3;
        return i3;
    }

    /* JADX WARN: Code restructure failed: missing block: B:21:0x0051, code lost:
        if (r8 == 16777219) goto L22;
     */
    /* JADX WARN: Code restructure failed: missing block: B:28:0x0069, code lost:
        if (r8 == 16777219) goto L22;
     */
    /* JADX WARN: Code restructure failed: missing block: B:62:0x00f6, code lost:
        if (r6 == 16777219) goto L63;
     */
    /* JADX WARN: Code restructure failed: missing block: B:69:0x010e, code lost:
        if (r6 == 16777219) goto L63;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final boolean h(h0.a.a.v r20, h0.a.a.m r21, int r22) {
        /*
            Method dump skipped, instructions count: 294
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: h0.a.a.m.h(h0.a.a.v, h0.a.a.m, int):boolean");
    }

    public final int i() {
        short s2 = this.g;
        if (s2 > 0) {
            int[] iArr = this.e;
            short s3 = (short) (s2 - 1);
            this.g = s3;
            return iArr[s3];
        }
        short s4 = (short) (this.f - 1);
        this.f = s4;
        return 83886080 | (-s4);
    }

    public final void j(int i) {
        short s2 = this.g;
        if (s2 >= i) {
            this.g = (short) (s2 - i);
            return;
        }
        this.f = (short) (this.f - (i - s2));
        this.g = (short) 0;
    }

    public final void k(String str) {
        char charAt = str.charAt(0);
        if (charAt == '(') {
            j((w.b(str) >> 2) - 1);
        } else if (charAt == 'J' || charAt == 'D') {
            j(2);
        } else {
            j(1);
        }
    }

    public final void l(int i) {
        if (this.e == null) {
            this.e = new int[10];
        }
        int length = this.e.length;
        short s2 = this.g;
        if (s2 >= length) {
            int[] iArr = new int[Math.max(s2 + 1, length * 2)];
            System.arraycopy(this.e, 0, iArr, 0, length);
            this.e = iArr;
        }
        int[] iArr2 = this.e;
        short s3 = this.g;
        short s4 = (short) (s3 + 1);
        this.g = s4;
        iArr2[s3] = i;
        short s5 = (short) (this.f + s4);
        p pVar = this.a;
        if (s5 > pVar.i) {
            pVar.i = s5;
        }
    }

    public final void m(v vVar, String str) {
        int i = 0;
        if (str.charAt(0) == '(') {
            i = str.indexOf(41) + 1;
        }
        int d = d(vVar, str, i);
        if (d != 0) {
            l(d);
            if (d == 16777220 || d == 16777219) {
                l(16777216);
            }
        }
    }

    public final void n(v vVar, int i, String str, int i2) {
        int[] iArr = new int[i2];
        this.f3683b = iArr;
        this.c = new int[0];
        int i3 = 1;
        if ((i & 8) != 0) {
            i3 = 0;
        } else if ((i & 262144) == 0) {
            iArr[0] = 33554432 | vVar.n(vVar.d);
        } else {
            iArr[0] = 16777222;
        }
        for (w wVar : w.a(str)) {
            int d = d(vVar, wVar.d(), 0);
            int[] iArr2 = this.f3683b;
            int i4 = i3 + 1;
            iArr2[i3] = d;
            if (d == 16777220 || d == 16777219) {
                i3 = i4 + 1;
                iArr2[i4] = 16777216;
            } else {
                i3 = i4;
            }
        }
        while (i3 < i2) {
            i3++;
            this.f3683b[i3] = 16777216;
        }
    }

    public final void o(int i, int i2) {
        if (this.d == null) {
            this.d = new int[10];
        }
        int length = this.d.length;
        if (i >= length) {
            int[] iArr = new int[Math.max(i + 1, length * 2)];
            System.arraycopy(this.d, 0, iArr, 0, length);
            this.d = iArr;
        }
        this.d[i] = i2;
    }
}
