package h0.a.a;

import andhook.lib.xposed.ClassUtils;
/* compiled from: TypePath.java */
/* loaded from: classes3.dex */
public final class x {
    public final byte[] a;

    /* renamed from: b  reason: collision with root package name */
    public final int f3699b;

    public x(byte[] bArr, int i) {
        this.a = bArr;
        this.f3699b = i;
    }

    public static void a(x xVar, c cVar) {
        if (xVar == null) {
            cVar.g(0);
            return;
        }
        byte[] bArr = xVar.a;
        int i = xVar.f3699b;
        cVar.h(bArr, i, (bArr[i] * 2) + 1);
    }

    public String toString() {
        byte b2 = this.a[this.f3699b];
        StringBuilder sb = new StringBuilder(b2 * 2);
        for (int i = 0; i < b2; i++) {
            byte[] bArr = this.a;
            int i2 = this.f3699b;
            int i3 = i * 2;
            byte b3 = bArr[i3 + i2 + 1];
            if (b3 == 0) {
                sb.append('[');
            } else if (b3 == 1) {
                sb.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
            } else if (b3 == 2) {
                sb.append('*');
            } else if (b3 == 3) {
                sb.append((int) bArr[i3 + i2 + 2]);
                sb.append(';');
            } else {
                throw new AssertionError();
            }
        }
        return sb.toString();
    }
}
