package h0.a.a;

import b.d.b.a.a;
/* compiled from: ClassReader.java */
/* loaded from: classes3.dex */
public class d {
    public final byte[] a;

    /* renamed from: b  reason: collision with root package name */
    public final int[] f3672b;
    public final String[] c;
    public final g[] d;
    public final int[] e;
    public final int f;
    public final int g;

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    public d(byte[] bArr, int i, boolean z2) {
        int i2;
        this.a = bArr;
        if (z2) {
            int i3 = i + 6;
            if (q(i3) > 56) {
                StringBuilder R = a.R("Unsupported class file major version ");
                R.append((int) q(i3));
                throw new IllegalArgumentException(R.toString());
            }
        }
        int u = u(i + 8);
        this.f3672b = new int[u];
        this.c = new String[u];
        int i4 = i + 10;
        int i5 = 1;
        int i6 = 0;
        boolean z3 = false;
        boolean z4 = false;
        while (true) {
            int i7 = 4;
            if (i5 < u) {
                int i8 = i5 + 1;
                int i9 = i4 + 1;
                this.f3672b[i5] = i9;
                switch (bArr[i4]) {
                    case 1:
                        i7 = u(i9) + 3;
                        if (i7 > i6) {
                            i6 = i7;
                        }
                        i2 = i7;
                        break;
                    case 2:
                    case 13:
                    case 14:
                    default:
                        throw new IllegalArgumentException();
                    case 3:
                    case 4:
                    case 9:
                    case 10:
                    case 11:
                    case 12:
                        i2 = 5;
                        break;
                    case 5:
                    case 6:
                        i7 = 9;
                        i8++;
                        i2 = i7;
                        break;
                    case 7:
                    case 8:
                    case 16:
                    case 19:
                    case 20:
                        i2 = 3;
                        break;
                    case 15:
                        i2 = i7;
                        break;
                    case 17:
                        z3 = true;
                        i2 = 5;
                        break;
                    case 18:
                        z4 = true;
                        i2 = 5;
                        break;
                }
                i4 += i2;
                i5 = i8;
            } else {
                this.f = i6;
                this.g = i4;
                int[] iArr = null;
                this.d = z3 ? new g[u] : null;
                if (z3 || z4) {
                    char[] cArr = new char[i6];
                    int c = c();
                    int u2 = u(c - 2);
                    while (true) {
                        if (u2 > 0) {
                            String t = t(c, cArr);
                            int l = l(c + 2);
                            int i10 = c + 6;
                            if ("BootstrapMethods".equals(t)) {
                                int u3 = u(i10);
                                int[] iArr2 = new int[u3];
                                int i11 = i10 + 2;
                                for (int i12 = 0; i12 < u3; i12++) {
                                    iArr2[i12] = i11;
                                    i11 += (u(i11 + 2) * 2) + 4;
                                }
                                iArr = iArr2;
                            } else {
                                c = i10 + l;
                                u2--;
                            }
                        }
                    }
                }
                this.e = iArr;
                return;
            }
        }
    }

    public final void a(int i, p[] pVarArr) {
        if (pVarArr[i] == null) {
            if (pVarArr[i] == null) {
                pVarArr[i] = new p();
            }
            p pVar = pVarArr[i];
            pVar.f3686b = (short) (pVar.f3686b | 1);
        }
    }

    public final p b(int i, p[] pVarArr) {
        if (pVarArr[i] == null) {
            pVarArr[i] = new p();
        }
        p pVar = pVarArr[i];
        pVar.f3686b = (short) (pVar.f3686b & (-2));
        return pVar;
    }

    public final int c() {
        int i = this.g;
        int u = (u(i + 6) * 2) + i + 8;
        int u2 = u(u);
        int i2 = u + 2;
        while (true) {
            u2--;
            if (u2 <= 0) {
                break;
            }
            int u3 = u(i2 + 6);
            i2 += 8;
            while (true) {
                u3--;
                if (u3 > 0) {
                    i2 += l(i2 + 2) + 6;
                }
            }
        }
        int u4 = u(i2);
        int i3 = i2 + 2;
        while (true) {
            u4--;
            if (u4 <= 0) {
                return i3 + 2;
            }
            int u5 = u(i3 + 6);
            i3 += 8;
            while (true) {
                u5--;
                if (u5 > 0) {
                    i3 += l(i3 + 2) + 6;
                }
            }
        }
    }

    public final int d(int[] iArr, int i) {
        if (iArr == null || i >= iArr.length || f(iArr[i]) < 67) {
            return -1;
        }
        return u(iArr[i] + 1);
    }

    public final b e(b[] bVarArr, String str, int i, int i2, char[] cArr, int i3, p[] pVarArr) {
        for (b bVar : bVarArr) {
            if (bVar.a.equals(str)) {
                b bVar2 = new b(bVar.a);
                byte[] bArr = new byte[i2];
                bVar2.f3669b = bArr;
                System.arraycopy(this.a, i, bArr, 0, i2);
                return bVar2;
            }
        }
        b bVar3 = new b(str);
        byte[] bArr2 = new byte[i2];
        bVar3.f3669b = bArr2;
        System.arraycopy(this.a, i, bArr2, 0, i2);
        return bVar3;
    }

    public int f(int i) {
        return this.a[i] & 255;
    }

    public String g(int i, char[] cArr) {
        return t(this.f3672b[u(i)], cArr);
    }

    /* JADX WARN: Removed duplicated region for block: B:210:0x04c7  */
    /* JADX WARN: Removed duplicated region for block: B:265:0x066f  */
    /* JADX WARN: Removed duplicated region for block: B:269:0x0683  */
    /* JADX WARN: Removed duplicated region for block: B:274:0x0695  */
    /* JADX WARN: Removed duplicated region for block: B:276:0x06b7  */
    /* JADX WARN: Removed duplicated region for block: B:290:0x06f7  */
    /* JADX WARN: Removed duplicated region for block: B:291:0x070b  */
    /* JADX WARN: Removed duplicated region for block: B:300:0x0755  */
    /* JADX WARN: Removed duplicated region for block: B:305:0x077e  */
    /* JADX WARN: Removed duplicated region for block: B:307:0x078f  */
    /* JADX WARN: Removed duplicated region for block: B:324:0x0845  */
    /* JADX WARN: Removed duplicated region for block: B:337:0x089e  */
    /* JADX WARN: Removed duplicated region for block: B:345:0x092f  */
    /* JADX WARN: Removed duplicated region for block: B:349:0x0972  */
    /* JADX WARN: Removed duplicated region for block: B:350:0x098c  */
    /* JADX WARN: Removed duplicated region for block: B:351:0x09a6  */
    /* JADX WARN: Removed duplicated region for block: B:352:0x09be  */
    /* JADX WARN: Removed duplicated region for block: B:354:0x09d8  */
    /* JADX WARN: Removed duplicated region for block: B:355:0x09f1  */
    /* JADX WARN: Removed duplicated region for block: B:359:0x0a17  */
    /* JADX WARN: Removed duplicated region for block: B:360:0x0a31  */
    /* JADX WARN: Removed duplicated region for block: B:361:0x0a4b  */
    /* JADX WARN: Removed duplicated region for block: B:363:0x0a63  */
    /* JADX WARN: Removed duplicated region for block: B:366:0x0a80  */
    /* JADX WARN: Removed duplicated region for block: B:374:0x0ab0  */
    /* JADX WARN: Removed duplicated region for block: B:378:0x0ab7  */
    /* JADX WARN: Removed duplicated region for block: B:379:0x0ad9  */
    /* JADX WARN: Removed duplicated region for block: B:487:0x068f A[SYNTHETIC] */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:358:0x0a13 -> B:365:0x0a7e). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void h(h0.a.a.q r48, h0.a.a.h r49, int r50) {
        /*
            Method dump skipped, instructions count: 4120
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: h0.a.a.d.h(h0.a.a.q, h0.a.a.h, int):void");
    }

    public Object i(int i, char[] cArr) {
        int[] iArr = this.f3672b;
        int i2 = iArr[i];
        byte b2 = this.a[i2 - 1];
        switch (b2) {
            case 3:
                return Integer.valueOf(l(i2));
            case 4:
                return Float.valueOf(Float.intBitsToFloat(l(i2)));
            case 5:
                return Long.valueOf(m(i2));
            case 6:
                return Double.valueOf(Double.longBitsToDouble(m(i2)));
            case 7:
                return w.h(t(i2, cArr));
            case 8:
                return t(i2, cArr);
            default:
                switch (b2) {
                    case 15:
                        int f = f(i2);
                        int i3 = this.f3672b[u(i2 + 1)];
                        int i4 = this.f3672b[u(i3 + 2)];
                        return new n(f, g(i3, cArr), t(i4, cArr), t(i4 + 2, cArr), this.a[i3 - 1] == 11);
                    case 16:
                        String t = t(i2, cArr);
                        return new w(11, t, 0, t.length());
                    case 17:
                        g gVar = this.d[i];
                        if (gVar != null) {
                            return gVar;
                        }
                        int i5 = iArr[i];
                        int i6 = iArr[u(i5 + 2)];
                        String t2 = t(i6, cArr);
                        String t3 = t(i6 + 2, cArr);
                        int i7 = this.e[u(i5)];
                        n nVar = (n) i(u(i7), cArr);
                        int u = u(i7 + 2);
                        Object[] objArr = new Object[u];
                        int i8 = i7 + 4;
                        for (int i9 = 0; i9 < u; i9++) {
                            objArr[i9] = i(u(i8), cArr);
                            i8 += 2;
                        }
                        g[] gVarArr = this.d;
                        g gVar2 = new g(t2, t3, nVar, objArr);
                        gVarArr[i] = gVar2;
                        return gVar2;
                    default:
                        throw new IllegalArgumentException();
                }
        }
    }

    public final int j(a aVar, int i, String str, char[] cArr) {
        int i2 = 0;
        if (aVar == null) {
            int i3 = this.a[i] & 255;
            if (i3 == 64) {
                return k(null, i + 3, true, cArr);
            }
            if (i3 != 91) {
                return i3 != 101 ? i + 3 : i + 5;
            }
            return k(null, i + 1, false, cArr);
        }
        int i4 = i + 1;
        int i5 = this.a[i] & 255;
        if (i5 != 64) {
            if (i5 != 70) {
                if (i5 == 83) {
                    aVar.e(str, Short.valueOf((short) l(this.f3672b[u(i4)])));
                } else if (i5 == 99) {
                    String t = t(i4, cArr);
                    aVar.e(str, w.k(t, 0, t.length()));
                } else if (i5 == 101) {
                    String t2 = t(i4, cArr);
                    String t3 = t(i4 + 2, cArr);
                    aVar.e++;
                    if (aVar.f3668b) {
                        aVar.c.j(aVar.a.l(str));
                    }
                    c cVar = aVar.c;
                    cVar.e(101, aVar.a.l(t2));
                    cVar.j(aVar.a.l(t3));
                    return i4 + 4;
                } else if (i5 == 115) {
                    aVar.e(str, t(i4, cArr));
                } else if (!(i5 == 73 || i5 == 74)) {
                    if (i5 == 90) {
                        aVar.e(str, l(this.f3672b[u(i4)]) == 0 ? Boolean.FALSE : Boolean.TRUE);
                    } else if (i5 != 91) {
                        switch (i5) {
                            case 66:
                                aVar.e(str, Byte.valueOf((byte) l(this.f3672b[u(i4)])));
                                break;
                            case 67:
                                aVar.e(str, Character.valueOf((char) l(this.f3672b[u(i4)])));
                                break;
                            case 68:
                                break;
                            default:
                                throw new IllegalArgumentException();
                        }
                    } else {
                        int u = u(i4);
                        int i6 = i4 + 2;
                        if (u == 0) {
                            return k(aVar.f(str), i6 - 2, false, cArr);
                        }
                        int i7 = this.a[i6] & 255;
                        if (i7 == 70) {
                            float[] fArr = new float[u];
                            while (i2 < u) {
                                fArr[i2] = Float.intBitsToFloat(l(this.f3672b[u(i6 + 1)]));
                                i6 += 3;
                                i2++;
                            }
                            aVar.e(str, fArr);
                            return i6;
                        } else if (i7 == 83) {
                            short[] sArr = new short[u];
                            while (i2 < u) {
                                sArr[i2] = (short) l(this.f3672b[u(i6 + 1)]);
                                i6 += 3;
                                i2++;
                            }
                            aVar.e(str, sArr);
                            return i6;
                        } else if (i7 == 90) {
                            boolean[] zArr = new boolean[u];
                            for (int i8 = 0; i8 < u; i8++) {
                                zArr[i8] = l(this.f3672b[u(i6 + 1)]) != 0;
                                i6 += 3;
                            }
                            aVar.e(str, zArr);
                            return i6;
                        } else if (i7 == 73) {
                            int[] iArr = new int[u];
                            while (i2 < u) {
                                iArr[i2] = l(this.f3672b[u(i6 + 1)]);
                                i6 += 3;
                                i2++;
                            }
                            aVar.e(str, iArr);
                            return i6;
                        } else if (i7 != 74) {
                            switch (i7) {
                                case 66:
                                    byte[] bArr = new byte[u];
                                    while (i2 < u) {
                                        bArr[i2] = (byte) l(this.f3672b[u(i6 + 1)]);
                                        i6 += 3;
                                        i2++;
                                    }
                                    aVar.e(str, bArr);
                                    return i6;
                                case 67:
                                    char[] cArr2 = new char[u];
                                    while (i2 < u) {
                                        cArr2[i2] = (char) l(this.f3672b[u(i6 + 1)]);
                                        i6 += 3;
                                        i2++;
                                    }
                                    aVar.e(str, cArr2);
                                    return i6;
                                case 68:
                                    double[] dArr = new double[u];
                                    while (i2 < u) {
                                        dArr[i2] = Double.longBitsToDouble(m(this.f3672b[u(i6 + 1)]));
                                        i6 += 3;
                                        i2++;
                                    }
                                    aVar.e(str, dArr);
                                    return i6;
                                default:
                                    return k(aVar.f(str), i6 - 2, false, cArr);
                            }
                        } else {
                            long[] jArr = new long[u];
                            while (i2 < u) {
                                jArr[i2] = m(this.f3672b[u(i6 + 1)]);
                                i6 += 3;
                                i2++;
                            }
                            aVar.e(str, jArr);
                            return i6;
                        }
                    }
                }
                return i4 + 2;
            }
            aVar.e(str, i(u(i4), cArr));
            return i4 + 2;
        }
        String t4 = t(i4, cArr);
        aVar.e++;
        if (aVar.f3668b) {
            aVar.c.j(aVar.a.l(str));
        }
        c cVar2 = aVar.c;
        cVar2.e(64, aVar.a.l(t4));
        cVar2.j(0);
        return k(new a(aVar.a, true, aVar.c, null), i4 + 2, true, cArr);
    }

    public final int k(a aVar, int i, boolean z2, char[] cArr) {
        int u = u(i);
        int i2 = i + 2;
        if (!z2) {
            while (true) {
                u--;
                if (u <= 0) {
                    break;
                }
                i2 = j(aVar, i2, null, cArr);
            }
        } else {
            while (true) {
                u--;
                if (u <= 0) {
                    break;
                }
                i2 = j(aVar, i2 + 2, t(i2, cArr), cArr);
            }
        }
        if (aVar != null) {
            aVar.g();
        }
        return i2;
    }

    public int l(int i) {
        byte[] bArr = this.a;
        return (bArr[i + 3] & 255) | ((bArr[i] & 255) << 24) | ((bArr[i + 1] & 255) << 16) | ((bArr[i + 2] & 255) << 8);
    }

    public long m(int i) {
        return (l(i) << 32) | (l(i + 4) & 4294967295L);
    }

    public String n(int i, char[] cArr) {
        return t(this.f3672b[u(i)], cArr);
    }

    public String o(int i, char[] cArr) {
        return t(this.f3672b[u(i)], cArr);
    }

    public final void p(q qVar, h hVar, int i, boolean z2) {
        a aVar;
        int i2 = i + 1;
        int i3 = this.a[i] & 255;
        r rVar = (r) qVar;
        if (z2) {
            rVar.D = i3;
        } else {
            rVar.F = i3;
        }
        char[] cArr = hVar.c;
        for (int i4 = 0; i4 < i3; i4++) {
            int u = u(i2);
            i2 += 2;
            while (true) {
                u--;
                if (u > 0) {
                    String t = t(i2, cArr);
                    int i5 = i2 + 2;
                    c cVar = new c();
                    cVar.j(rVar.d.l(t));
                    cVar.j(0);
                    if (z2) {
                        if (rVar.E == null) {
                            rVar.E = new a[w.a(rVar.i).length];
                        }
                        a[] aVarArr = rVar.E;
                        aVar = new a(rVar.d, true, cVar, aVarArr[i4]);
                        aVarArr[i4] = aVar;
                    } else {
                        if (rVar.G == null) {
                            rVar.G = new a[w.a(rVar.i).length];
                        }
                        a[] aVarArr2 = rVar.G;
                        aVar = new a(rVar.d, true, cVar, aVarArr2[i4]);
                        aVarArr2[i4] = aVar;
                    }
                    i2 = k(aVar, i5, true, cArr);
                }
            }
        }
    }

    public short q(int i) {
        byte[] bArr = this.a;
        return (short) ((bArr[i + 1] & 255) | ((bArr[i] & 255) << 8));
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Removed duplicated region for block: B:13:0x0025  */
    /* JADX WARN: Removed duplicated region for block: B:21:0x007d  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x007f  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final int r(h0.a.a.h r10, int r11) {
        /*
            r9 = this;
            int r0 = r9.l(r11)
            int r1 = r0 >>> 24
            r2 = 1
            if (r1 == 0) goto L70
            if (r1 == r2) goto L70
            r3 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            switch(r1) {
                case 16: goto L25;
                case 17: goto L25;
                case 18: goto L25;
                case 19: goto L19;
                case 20: goto L19;
                case 21: goto L19;
                case 22: goto L70;
                case 23: goto L25;
                default: goto L10;
            }
        L10:
            switch(r1) {
                case 64: goto L2a;
                case 65: goto L2a;
                case 66: goto L25;
                case 67: goto L23;
                case 68: goto L23;
                case 69: goto L23;
                case 70: goto L23;
                case 71: goto L1c;
                case 72: goto L1c;
                case 73: goto L1c;
                case 74: goto L1c;
                case 75: goto L1c;
                default: goto L13;
            }
        L13:
            java.lang.IllegalArgumentException r10 = new java.lang.IllegalArgumentException
            r10.<init>()
            throw r10
        L19:
            r0 = r0 & r3
            int r11 = r11 + r2
            goto L75
        L1c:
            r1 = -16776961(0xffffffffff0000ff, float:-1.7014636E38)
            r0 = r0 & r1
            int r11 = r11 + 4
            goto L75
        L23:
            r0 = r0 & r3
            goto L27
        L25:
            r0 = r0 & (-256(0xffffffffffffff00, float:NaN))
        L27:
            int r11 = r11 + 3
            goto L75
        L2a:
            r0 = r0 & r3
            int r1 = r11 + 1
            int r1 = r9.u(r1)
            int r11 = r11 + 3
            h0.a.a.p[] r3 = new h0.a.a.p[r1]
            r10.j = r3
            h0.a.a.p[] r3 = new h0.a.a.p[r1]
            r10.k = r3
            int[] r3 = new int[r1]
            r10.l = r3
            r3 = 0
        L40:
            if (r3 >= r1) goto L75
            int r4 = r9.u(r11)
            int r5 = r11 + 2
            int r5 = r9.u(r5)
            int r6 = r11 + 4
            int r6 = r9.u(r6)
            int r11 = r11 + 6
            h0.a.a.p[] r7 = r10.j
            h0.a.a.p[] r8 = r10.g
            h0.a.a.p r8 = r9.b(r4, r8)
            r7[r3] = r8
            h0.a.a.p[] r7 = r10.k
            int r4 = r4 + r5
            h0.a.a.p[] r5 = r10.g
            h0.a.a.p r4 = r9.b(r4, r5)
            r7[r3] = r4
            int[] r4 = r10.l
            r4[r3] = r6
            int r3 = r3 + 1
            goto L40
        L70:
            r1 = -65536(0xffffffffffff0000, float:NaN)
            r0 = r0 & r1
            int r11 = r11 + 2
        L75:
            r10.h = r0
            int r0 = r9.f(r11)
            if (r0 != 0) goto L7f
            r1 = 0
            goto L86
        L7f:
            h0.a.a.x r1 = new h0.a.a.x
            byte[] r3 = r9.a
            r1.<init>(r3, r11)
        L86:
            r10.i = r1
            int r11 = r11 + r2
            int r0 = r0 * 2
            int r0 = r0 + r11
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: h0.a.a.d.r(h0.a.a.h, int):int");
    }

    /* JADX WARN: Removed duplicated region for block: B:18:0x005a  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x00ab  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final int[] s(h0.a.a.q r12, h0.a.a.h r13, int r14, boolean r15) {
        /*
            r11 = this;
            char[] r0 = r13.c
            int r1 = r11.u(r14)
            int[] r2 = new int[r1]
            int r14 = r14 + 2
            r3 = 0
            r4 = 0
        Lc:
            if (r4 >= r1) goto Lb8
            r2[r4] = r14
            int r5 = r11.l(r14)
            int r6 = r5 >>> 24
            r7 = 23
            if (r6 == r7) goto L4e
            switch(r6) {
                case 16: goto L4e;
                case 17: goto L4e;
                case 18: goto L4e;
                default: goto L1d;
            }
        L1d:
            switch(r6) {
                case 64: goto L29;
                case 65: goto L29;
                case 66: goto L4e;
                case 67: goto L4e;
                case 68: goto L4e;
                case 69: goto L4e;
                case 70: goto L4e;
                case 71: goto L26;
                case 72: goto L26;
                case 73: goto L26;
                case 74: goto L26;
                case 75: goto L26;
                default: goto L20;
            }
        L20:
            java.lang.IllegalArgumentException r12 = new java.lang.IllegalArgumentException
            r12.<init>()
            throw r12
        L26:
            int r14 = r14 + 4
            goto L50
        L29:
            int r7 = r14 + 1
            int r7 = r11.u(r7)
            int r14 = r14 + 3
        L31:
            int r8 = r7 + (-1)
            if (r7 <= 0) goto L50
            int r7 = r11.u(r14)
            int r9 = r14 + 2
            int r9 = r11.u(r9)
            int r14 = r14 + 6
            h0.a.a.p[] r10 = r13.g
            r11.b(r7, r10)
            int r7 = r7 + r9
            h0.a.a.p[] r9 = r13.g
            r11.b(r7, r9)
            r7 = r8
            goto L31
        L4e:
            int r14 = r14 + 3
        L50:
            int r7 = r11.f(r14)
            r8 = 66
            r9 = 0
            r10 = 1
            if (r6 != r8) goto Lab
            if (r7 != 0) goto L5d
            goto L64
        L5d:
            h0.a.a.x r9 = new h0.a.a.x
            byte[] r6 = r11.a
            r9.<init>(r6, r14)
        L64:
            int r7 = r7 * 2
            int r7 = r7 + r10
            int r7 = r7 + r14
            java.lang.String r14 = r11.t(r7, r0)
            int r7 = r7 + 2
            r5 = r5 & (-256(0xffffffffffffff00, float:NaN))
            r6 = r12
            h0.a.a.r r6 = (h0.a.a.r) r6
            java.util.Objects.requireNonNull(r6)
            h0.a.a.c r8 = new h0.a.a.c
            r8.<init>()
            b.i.a.f.e.o.f.W0(r5, r8)
            h0.a.a.x.a(r9, r8)
            h0.a.a.v r5 = r6.d
            int r14 = r5.l(r14)
            r8.j(r14)
            r8.j(r3)
            if (r15 == 0) goto L9b
            h0.a.a.a r14 = new h0.a.a.a
            h0.a.a.v r5 = r6.d
            h0.a.a.a r9 = r6.w
            r14.<init>(r5, r10, r8, r9)
            r6.w = r14
            goto La6
        L9b:
            h0.a.a.a r14 = new h0.a.a.a
            h0.a.a.v r5 = r6.d
            h0.a.a.a r9 = r6.f3691x
            r14.<init>(r5, r10, r8, r9)
            r6.f3691x = r14
        La6:
            int r14 = r11.k(r14, r7, r10, r0)
            goto Lb4
        Lab:
            int r7 = r7 * 2
            int r7 = r7 + 3
            int r7 = r7 + r14
            int r14 = r11.k(r9, r7, r10, r0)
        Lb4:
            int r4 = r4 + 1
            goto Lc
        Lb8:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: h0.a.a.d.s(h0.a.a.q, h0.a.a.h, int, boolean):int[]");
    }

    public String t(int i, char[] cArr) {
        int u = u(i);
        if (i == 0 || u == 0) {
            return null;
        }
        return w(u, cArr);
    }

    public int u(int i) {
        byte[] bArr = this.a;
        return (bArr[i + 1] & 255) | ((bArr[i] & 255) << 8);
    }

    public final String v(int i, int i2, char[] cArr) {
        int i3;
        int i4 = i2 + i;
        byte[] bArr = this.a;
        int i5 = 0;
        while (i < i4) {
            int i6 = i + 1;
            byte b2 = bArr[i];
            if ((b2 & 128) == 0) {
                i3 = i5 + 1;
                cArr[i5] = (char) (b2 & Byte.MAX_VALUE);
            } else if ((b2 & 224) == 192) {
                i5++;
                int i7 = (b2 & 31) << 6;
                i = i6 + 1;
                cArr[i5] = (char) (i7 + (bArr[i6] & 63));
            } else {
                i3 = i5 + 1;
                int i8 = i6 + 1;
                int i9 = ((b2 & 15) << 12) + ((bArr[i6] & 63) << 6);
                i6 = i8 + 1;
                cArr[i5] = (char) (i9 + (bArr[i8] & 63));
            }
            i = i6;
            i5 = i3;
        }
        return new String(cArr, 0, i5);
    }

    public final String w(int i, char[] cArr) {
        String[] strArr = this.c;
        String str = strArr[i];
        if (str != null) {
            return str;
        }
        int i2 = this.f3672b[i];
        String v = v(i2 + 2, u(i2), cArr);
        strArr[i] = v;
        return v;
    }

    public final int x(int i, Object[] objArr, int i2, char[] cArr, p[] pVarArr) {
        int i3 = i + 1;
        switch (this.a[i] & 255) {
            case 0:
                objArr[i2] = t.a;
                return i3;
            case 1:
                objArr[i2] = t.f3695b;
                return i3;
            case 2:
                objArr[i2] = t.c;
                return i3;
            case 3:
                objArr[i2] = t.d;
                return i3;
            case 4:
                objArr[i2] = t.e;
                return i3;
            case 5:
                objArr[i2] = t.f;
                return i3;
            case 6:
                objArr[i2] = t.g;
                return i3;
            case 7:
                objArr[i2] = g(i3, cArr);
                break;
            case 8:
                objArr[i2] = b(u(i3), pVarArr);
                break;
            default:
                throw new IllegalArgumentException();
        }
        return i3 + 2;
    }
}
