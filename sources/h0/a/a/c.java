package h0.a.a;

import org.objectweb.asm.Opcodes;
/* compiled from: ByteVector.java */
/* loaded from: classes3.dex */
public class c {
    public byte[] a;

    /* renamed from: b  reason: collision with root package name */
    public int f3671b;

    public c() {
        this.a = new byte[64];
    }

    public final c a(String str, int i, int i2) {
        int i3;
        int length = str.length();
        int i4 = i;
        int i5 = i4;
        while (i4 < length) {
            char charAt = str.charAt(i4);
            i5 = (charAt < 1 || charAt > 127) ? charAt <= 2047 ? i5 + 2 : i5 + 3 : i5 + 1;
            i4++;
        }
        if (i5 <= i2) {
            int i6 = this.f3671b;
            int i7 = (i6 - i) - 2;
            if (i7 >= 0) {
                byte[] bArr = this.a;
                bArr[i7] = (byte) (i5 >>> 8);
                bArr[i7 + 1] = (byte) i5;
            }
            if ((i6 + i5) - i > this.a.length) {
                b(i5 - i);
            }
            int i8 = this.f3671b;
            while (i < length) {
                char charAt2 = str.charAt(i);
                if (charAt2 >= 1 && charAt2 <= 127) {
                    i3 = i8 + 1;
                    this.a[i8] = (byte) charAt2;
                } else if (charAt2 <= 2047) {
                    byte[] bArr2 = this.a;
                    int i9 = i8 + 1;
                    bArr2[i8] = (byte) (((charAt2 >> 6) & 31) | Opcodes.CHECKCAST);
                    i8 = i9 + 1;
                    bArr2[i9] = (byte) ((charAt2 & '?') | 128);
                    i++;
                } else {
                    byte[] bArr3 = this.a;
                    int i10 = i8 + 1;
                    bArr3[i8] = (byte) (((charAt2 >> '\f') & 15) | 224);
                    int i11 = i10 + 1;
                    bArr3[i10] = (byte) (((charAt2 >> 6) & 63) | 128);
                    i3 = i11 + 1;
                    bArr3[i11] = (byte) ((charAt2 & '?') | 128);
                }
                i8 = i3;
                i++;
            }
            this.f3671b = i8;
            return this;
        }
        throw new IllegalArgumentException("UTF8 string too large");
    }

    public final void b(int i) {
        byte[] bArr = this.a;
        int length = bArr.length * 2;
        int i2 = this.f3671b;
        int i3 = i + i2;
        if (length <= i3) {
            length = i3;
        }
        byte[] bArr2 = new byte[length];
        System.arraycopy(bArr, 0, bArr2, 0, i2);
        this.a = bArr2;
    }

    public final c c(int i, int i2) {
        int i3 = this.f3671b;
        if (i3 + 2 > this.a.length) {
            b(2);
        }
        byte[] bArr = this.a;
        int i4 = i3 + 1;
        bArr[i3] = (byte) i;
        bArr[i4] = (byte) i2;
        this.f3671b = i4 + 1;
        return this;
    }

    public final c d(int i, int i2, int i3) {
        int i4 = this.f3671b;
        if (i4 + 4 > this.a.length) {
            b(4);
        }
        byte[] bArr = this.a;
        int i5 = i4 + 1;
        bArr[i4] = (byte) i;
        int i6 = i5 + 1;
        bArr[i5] = (byte) i2;
        int i7 = i6 + 1;
        bArr[i6] = (byte) (i3 >>> 8);
        bArr[i7] = (byte) i3;
        this.f3671b = i7 + 1;
        return this;
    }

    public final c e(int i, int i2) {
        int i3 = this.f3671b;
        if (i3 + 3 > this.a.length) {
            b(3);
        }
        byte[] bArr = this.a;
        int i4 = i3 + 1;
        bArr[i3] = (byte) i;
        int i5 = i4 + 1;
        bArr[i4] = (byte) (i2 >>> 8);
        bArr[i5] = (byte) i2;
        this.f3671b = i5 + 1;
        return this;
    }

    public final c f(int i, int i2, int i3) {
        int i4 = this.f3671b;
        if (i4 + 5 > this.a.length) {
            b(5);
        }
        byte[] bArr = this.a;
        int i5 = i4 + 1;
        bArr[i4] = (byte) i;
        int i6 = i5 + 1;
        bArr[i5] = (byte) (i2 >>> 8);
        int i7 = i6 + 1;
        bArr[i6] = (byte) i2;
        int i8 = i7 + 1;
        bArr[i7] = (byte) (i3 >>> 8);
        bArr[i8] = (byte) i3;
        this.f3671b = i8 + 1;
        return this;
    }

    public c g(int i) {
        int i2 = this.f3671b;
        int i3 = i2 + 1;
        if (i3 > this.a.length) {
            b(1);
        }
        this.a[i2] = (byte) i;
        this.f3671b = i3;
        return this;
    }

    public c h(byte[] bArr, int i, int i2) {
        if (this.f3671b + i2 > this.a.length) {
            b(i2);
        }
        if (bArr != null) {
            System.arraycopy(bArr, i, this.a, this.f3671b, i2);
        }
        this.f3671b += i2;
        return this;
    }

    public c i(int i) {
        int i2 = this.f3671b;
        if (i2 + 4 > this.a.length) {
            b(4);
        }
        byte[] bArr = this.a;
        int i3 = i2 + 1;
        bArr[i2] = (byte) (i >>> 24);
        int i4 = i3 + 1;
        bArr[i3] = (byte) (i >>> 16);
        int i5 = i4 + 1;
        bArr[i4] = (byte) (i >>> 8);
        bArr[i5] = (byte) i;
        this.f3671b = i5 + 1;
        return this;
    }

    public c j(int i) {
        int i2 = this.f3671b;
        if (i2 + 2 > this.a.length) {
            b(2);
        }
        byte[] bArr = this.a;
        int i3 = i2 + 1;
        bArr[i2] = (byte) (i >>> 8);
        bArr[i3] = (byte) i;
        this.f3671b = i3 + 1;
        return this;
    }

    public c(int i) {
        this.a = new byte[i];
    }
}
