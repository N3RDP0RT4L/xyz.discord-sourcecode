package h0.a.a;
/* compiled from: FieldVisitor.java */
/* loaded from: classes3.dex */
public abstract class k {
    public k a;

    public k(int i) {
        if (i == 393216 || i == 327680 || i == 262144 || i == 458752) {
            this.a = null;
            return;
        }
        throw new IllegalArgumentException();
    }
}
