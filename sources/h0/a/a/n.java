package h0.a.a;

import andhook.lib.xposed.ClassUtils;
import b.d.b.a.a;
/* compiled from: Handle.java */
/* loaded from: classes3.dex */
public final class n {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final String f3684b;
    public final String c;
    public final String d;
    public final boolean e;

    public n(int i, String str, String str2, String str3, boolean z2) {
        this.a = i;
        this.f3684b = str;
        this.c = str2;
        this.d = str3;
        this.e = z2;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof n)) {
            return false;
        }
        n nVar = (n) obj;
        return this.a == nVar.a && this.e == nVar.e && this.f3684b.equals(nVar.f3684b) && this.c.equals(nVar.c) && this.d.equals(nVar.d);
    }

    public int hashCode() {
        return (this.d.hashCode() * this.c.hashCode() * this.f3684b.hashCode()) + this.a + (this.e ? 64 : 0);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f3684b);
        sb.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
        sb.append(this.c);
        sb.append(this.d);
        sb.append(" (");
        sb.append(this.a);
        return a.G(sb, this.e ? " itf" : "", ')');
    }
}
