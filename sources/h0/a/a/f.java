package h0.a.a;

import org.objectweb.asm.Opcodes;
/* compiled from: ClassWriter.java */
/* loaded from: classes3.dex */
public class f extends e {
    public b A;
    public int B;
    public int a;

    /* renamed from: b  reason: collision with root package name */
    public final v f3673b = new v(this);
    public int c;
    public int d;
    public int e;
    public int f;
    public int[] g;
    public l h;
    public l i;
    public r j;
    public r k;
    public int l;
    public c m;
    public int n;
    public int o;
    public int p;
    public int q;
    public c r;

    /* renamed from: s  reason: collision with root package name */
    public a f3674s;
    public a t;
    public a u;
    public a v;
    public s w;

    /* renamed from: x  reason: collision with root package name */
    public int f3675x;

    /* renamed from: y  reason: collision with root package name */
    public int f3676y;

    /* renamed from: z  reason: collision with root package name */
    public c f3677z;

    public f(int i) {
        super(Opcodes.ASM7);
        if ((i & 2) != 0) {
            this.B = 4;
        } else if ((i & 1) != 0) {
            this.B = 1;
        } else {
            this.B = 0;
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:397:0x0aa5, code lost:
        if (r1.f3692y != 0) goto L407;
     */
    /* JADX WARN: Removed duplicated region for block: B:409:0x0ad2  */
    /* JADX WARN: Removed duplicated region for block: B:410:0x0ad7  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final byte[] a(byte[] r54, boolean r55) {
        /*
            Method dump skipped, instructions count: 3085
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: h0.a.a.f.a(byte[], boolean):byte[]");
    }

    /* JADX WARN: Removed duplicated region for block: B:339:0x06b8  */
    /* JADX WARN: Removed duplicated region for block: B:342:0x06c8  */
    /* JADX WARN: Removed duplicated region for block: B:345:0x06ef  */
    /* JADX WARN: Removed duplicated region for block: B:347:0x06f6  */
    /* JADX WARN: Removed duplicated region for block: B:349:0x06fd  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public byte[] b() throws org.objectweb.asm.ClassTooLargeException, org.objectweb.asm.MethodTooLargeException {
        /*
            Method dump skipped, instructions count: 1802
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: h0.a.a.f.b():byte[]");
    }

    public final void c(int i, int i2, String str, String str2, String str3, String[] strArr) {
        this.a = i;
        this.c = i2;
        v vVar = this.f3673b;
        int i3 = i & 65535;
        vVar.c = i3;
        vVar.d = str;
        this.d = vVar.m(7, str).a;
        if (str2 != null) {
            this.p = this.f3673b.l(str2);
        }
        this.e = str3 == null ? 0 : this.f3673b.m(7, str3).a;
        if (strArr != null && strArr.length > 0) {
            int length = strArr.length;
            this.f = length;
            this.g = new int[length];
            for (int i4 = 0; i4 < this.f; i4++) {
                this.g[i4] = this.f3673b.c(strArr[i4]).a;
            }
        }
        if (this.B == 1 && i3 >= 51) {
            this.B = 2;
        }
    }

    public final a d(String str, boolean z2) {
        c cVar = new c();
        cVar.j(this.f3673b.l(str));
        cVar.j(0);
        if (z2) {
            a aVar = new a(this.f3673b, true, cVar, this.f3674s);
            this.f3674s = aVar;
            return aVar;
        }
        a aVar2 = new a(this.f3673b, true, cVar, this.t);
        this.t = aVar2;
        return aVar2;
    }

    public final q e(int i, String str, String str2, String str3, String[] strArr) {
        r rVar = new r(this.f3673b, i, str, str2, str3, strArr, this.B);
        if (this.j == null) {
            this.j = rVar;
        } else {
            this.k.f3687b = rVar;
        }
        this.k = rVar;
        return rVar;
    }

    public final a f(int i, x xVar, String str, boolean z2) {
        c cVar = new c();
        b.i.a.f.e.o.f.W0(i, cVar);
        x.a(xVar, cVar);
        cVar.j(this.f3673b.l(str));
        cVar.j(0);
        if (z2) {
            a aVar = new a(this.f3673b, true, cVar, this.u);
            this.u = aVar;
            return aVar;
        }
        a aVar2 = new a(this.f3673b, true, cVar, this.v);
        this.v = aVar2;
        return aVar2;
    }
}
