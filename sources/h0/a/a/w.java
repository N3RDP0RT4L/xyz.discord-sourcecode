package h0.a.a;

import andhook.lib.xposed.ClassUtils;
import b.d.b.a.a;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.swift.sandhook.annotation.MethodReflectParams;
/* compiled from: Type.java */
/* loaded from: classes3.dex */
public final class w {
    public static final w a = new w(0, "VZCBSIFJD", 0, 1);

    /* renamed from: b  reason: collision with root package name */
    public static final w f3698b = new w(1, "VZCBSIFJD", 1, 2);
    public static final w c = new w(2, "VZCBSIFJD", 2, 3);
    public static final w d = new w(3, "VZCBSIFJD", 3, 4);
    public static final w e = new w(4, "VZCBSIFJD", 4, 5);
    public static final w f = new w(5, "VZCBSIFJD", 5, 6);
    public static final w g = new w(6, "VZCBSIFJD", 6, 7);
    public static final w h = new w(7, "VZCBSIFJD", 7, 8);
    public static final w i = new w(8, "VZCBSIFJD", 8, 9);
    public final int j;
    public final String k;
    public final int l;
    public final int m;

    public w(int i2, String str, int i3, int i4) {
        this.j = i2;
        this.k = str;
        this.l = i3;
        this.m = i4;
    }

    public static w[] a(String str) {
        int i2 = 0;
        int i3 = 1;
        int i4 = 0;
        while (str.charAt(i3) != ')') {
            while (str.charAt(i3) == '[') {
                i3++;
            }
            int i5 = i3 + 1;
            i3 = str.charAt(i3) == 'L' ? str.indexOf(59, i5) + 1 : i5;
            i4++;
        }
        w[] wVarArr = new w[i4];
        int i6 = 1;
        while (str.charAt(i6) != ')') {
            int i7 = i6;
            while (str.charAt(i7) == '[') {
                i7++;
            }
            int i8 = i7 + 1;
            if (str.charAt(i7) == 'L') {
                i8 = str.indexOf(59, i8) + 1;
            }
            i2++;
            wVarArr[i2] = k(str, i6, i8);
            i6 = i8;
        }
        return wVarArr;
    }

    public static int b(String str) {
        int i2 = 1;
        char charAt = str.charAt(1);
        int i3 = 1;
        int i4 = 1;
        while (charAt != ')') {
            if (charAt == 'J' || charAt == 'D') {
                i3++;
                i4 += 2;
            } else {
                while (str.charAt(i3) == '[') {
                    i3++;
                }
                int i5 = i3 + 1;
                if (str.charAt(i3) == 'L') {
                    i5 = str.indexOf(59, i5) + 1;
                }
                i4++;
                i3 = i5;
            }
            charAt = str.charAt(i3);
        }
        char charAt2 = str.charAt(i3 + 1);
        if (charAt2 == 'V') {
            return i4 << 2;
        }
        if (charAt2 == 'J' || charAt2 == 'D') {
            i2 = 2;
        }
        return (i4 << 2) | i2;
    }

    public static String e(Class<?> cls) {
        char c2;
        StringBuilder sb = new StringBuilder();
        while (cls.isArray()) {
            sb.append('[');
            cls = cls.getComponentType();
        }
        if (cls.isPrimitive()) {
            if (cls == Integer.TYPE) {
                c2 = 'I';
            } else if (cls == Void.TYPE) {
                c2 = 'V';
            } else if (cls == Boolean.TYPE) {
                c2 = 'Z';
            } else if (cls == Byte.TYPE) {
                c2 = 'B';
            } else if (cls == Character.TYPE) {
                c2 = 'C';
            } else if (cls == Short.TYPE) {
                c2 = 'S';
            } else if (cls == Double.TYPE) {
                c2 = 'D';
            } else if (cls == Float.TYPE) {
                c2 = 'F';
            } else if (cls == Long.TYPE) {
                c2 = 'J';
            } else {
                throw new AssertionError();
            }
            sb.append(c2);
        } else {
            sb.append('L');
            String name = cls.getName();
            int length = name.length();
            for (int i2 = 0; i2 < length; i2++) {
                char charAt = name.charAt(i2);
                if (charAt == '.') {
                    charAt = MentionUtilsKt.SLASH_CHAR;
                }
                sb.append(charAt);
            }
            sb.append(';');
        }
        return sb.toString();
    }

    public static w h(String str) {
        return new w(str.charAt(0) == '[' ? 9 : 12, str, 0, str.length());
    }

    public static w j(Class<?> cls) {
        if (!cls.isPrimitive()) {
            String e2 = e(cls);
            return k(e2, 0, e2.length());
        } else if (cls == Integer.TYPE) {
            return f;
        } else {
            if (cls == Void.TYPE) {
                return a;
            }
            if (cls == Boolean.TYPE) {
                return f3698b;
            }
            if (cls == Byte.TYPE) {
                return d;
            }
            if (cls == Character.TYPE) {
                return c;
            }
            if (cls == Short.TYPE) {
                return e;
            }
            if (cls == Double.TYPE) {
                return i;
            }
            if (cls == Float.TYPE) {
                return g;
            }
            if (cls == Long.TYPE) {
                return h;
            }
            throw new AssertionError();
        }
    }

    public static w k(String str, int i2, int i3) {
        char charAt = str.charAt(i2);
        if (charAt == '(') {
            return new w(11, str, i2, i3);
        }
        if (charAt == 'F') {
            return g;
        }
        if (charAt == 'L') {
            return new w(10, str, i2 + 1, i3 - 1);
        }
        if (charAt == 'S') {
            return e;
        }
        if (charAt == 'V') {
            return a;
        }
        if (charAt == 'I') {
            return f;
        }
        if (charAt == 'J') {
            return h;
        }
        if (charAt == 'Z') {
            return f3698b;
        }
        if (charAt == '[') {
            return new w(9, str, i2, i3);
        }
        switch (charAt) {
            case 'B':
                return d;
            case 'C':
                return c;
            case 'D':
                return i;
            default:
                throw new IllegalArgumentException();
        }
    }

    public String c() {
        switch (this.j) {
            case 0:
                return "void";
            case 1:
                return MethodReflectParams.BOOLEAN;
            case 2:
                return MethodReflectParams.CHAR;
            case 3:
                return MethodReflectParams.BYTE;
            case 4:
                return MethodReflectParams.SHORT;
            case 5:
                return MethodReflectParams.INT;
            case 6:
                return MethodReflectParams.FLOAT;
            case 7:
                return "long";
            case 8:
                return MethodReflectParams.DOUBLE;
            case 9:
                StringBuilder sb = new StringBuilder(k(this.k, this.l + f(), this.m).c());
                for (int f2 = f(); f2 > 0; f2--) {
                    sb.append("[]");
                }
                return sb.toString();
            case 10:
            case 12:
                return this.k.substring(this.l, this.m).replace(MentionUtilsKt.SLASH_CHAR, ClassUtils.PACKAGE_SEPARATOR_CHAR);
            case 11:
            default:
                throw new AssertionError();
        }
    }

    public String d() {
        int i2 = this.j;
        if (i2 == 10) {
            return this.k.substring(this.l - 1, this.m + 1);
        }
        if (i2 != 12) {
            return this.k.substring(this.l, this.m);
        }
        StringBuilder O = a.O('L');
        O.append((CharSequence) this.k, this.l, this.m);
        O.append(';');
        return O.toString();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof w)) {
            return false;
        }
        w wVar = (w) obj;
        int i2 = this.j;
        int i3 = 10;
        if (i2 == 12) {
            i2 = 10;
        }
        int i4 = wVar.j;
        if (i4 != 12) {
            i3 = i4;
        }
        if (i2 != i3) {
            return false;
        }
        int i5 = this.l;
        int i6 = this.m;
        int i7 = wVar.l;
        if (i6 - i5 != wVar.m - i7) {
            return false;
        }
        while (i5 < i6) {
            if (this.k.charAt(i5) != wVar.k.charAt(i7)) {
                return false;
            }
            i5++;
            i7++;
        }
        return true;
    }

    public int f() {
        int i2 = 1;
        while (this.k.charAt(this.l + i2) == '[') {
            i2++;
        }
        return i2;
    }

    public String g() {
        return this.k.substring(this.l, this.m);
    }

    public int hashCode() {
        int i2 = this.j;
        int i3 = (i2 == 12 ? 10 : i2) * 13;
        if (i2 >= 9) {
            int i4 = this.m;
            for (int i5 = this.l; i5 < i4; i5++) {
                i3 = (this.k.charAt(i5) + i3) * 17;
            }
        }
        return i3;
    }

    public int i() {
        int i2 = this.j;
        if (i2 == 12) {
            return 10;
        }
        return i2;
    }

    public String toString() {
        return d();
    }
}
