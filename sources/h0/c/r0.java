package h0.c;

import org.webrtc.CalledByNative;
import org.webrtc.VideoEncoder;
/* compiled from: VideoEncoder.java */
/* loaded from: classes3.dex */
public final /* synthetic */ class r0 {
    @CalledByNative
    public static long a(VideoEncoder videoEncoder) {
        return 0L;
    }

    @CalledByNative
    public static VideoEncoder.ResolutionBitrateLimits[] b(VideoEncoder videoEncoder) {
        return new VideoEncoder.ResolutionBitrateLimits[0];
    }

    @CalledByNative
    public static boolean c(VideoEncoder videoEncoder) {
        return true;
    }
}
