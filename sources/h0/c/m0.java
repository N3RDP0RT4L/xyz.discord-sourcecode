package h0.c;

import android.media.MediaRecorder;
import org.webrtc.CameraVideoCapturer;
/* compiled from: CameraVideoCapturer.java */
/* loaded from: classes3.dex */
public final /* synthetic */ class m0 {
    @Deprecated
    public static void a(CameraVideoCapturer _this, MediaRecorder mediaRecorder, CameraVideoCapturer.MediaRecorderHandler mediaRecorderHandler) {
        throw new UnsupportedOperationException("Deprecated and not implemented.");
    }

    @Deprecated
    public static void b(CameraVideoCapturer _this, CameraVideoCapturer.MediaRecorderHandler mediaRecorderHandler) {
        throw new UnsupportedOperationException("Deprecated and not implemented.");
    }
}
