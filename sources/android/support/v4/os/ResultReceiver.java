package android.support.v4.os;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import androidx.annotation.RestrictTo;
import java.util.Objects;
import x.a.b.c.a;
@SuppressLint({"BanParcelableUsage"})
@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP_PREFIX})
/* loaded from: classes.dex */
public class ResultReceiver implements Parcelable {
    public static final Parcelable.Creator<ResultReceiver> CREATOR = new a();
    public x.a.b.c.a j;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<ResultReceiver> {
        @Override // android.os.Parcelable.Creator
        public ResultReceiver createFromParcel(Parcel parcel) {
            return new ResultReceiver(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public ResultReceiver[] newArray(int i) {
            return new ResultReceiver[i];
        }
    }

    /* loaded from: classes.dex */
    public class b extends a.AbstractBinderC0440a {
        public b() {
        }

        @Override // x.a.b.c.a
        public void r0(int i, Bundle bundle) {
            Objects.requireNonNull(ResultReceiver.this);
            ResultReceiver.this.a(i, bundle);
        }
    }

    public ResultReceiver(Parcel parcel) {
        x.a.b.c.a aVar;
        IBinder readStrongBinder = parcel.readStrongBinder();
        int i = a.AbstractBinderC0440a.a;
        if (readStrongBinder == null) {
            aVar = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("android.support.v4.os.IResultReceiver");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof x.a.b.c.a)) {
                aVar = new a.AbstractBinderC0440a.C0441a(readStrongBinder);
            } else {
                aVar = (x.a.b.c.a) queryLocalInterface;
            }
        }
        this.j = aVar;
    }

    public void a(int i, Bundle bundle) {
    }

    public void b(int i, Bundle bundle) {
        x.a.b.c.a aVar = this.j;
        if (aVar != null) {
            try {
                aVar.r0(i, bundle);
            } catch (RemoteException unused) {
            }
        }
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        synchronized (this) {
            if (this.j == null) {
                this.j = new b();
            }
            parcel.writeStrongBinder(this.j.asBinder());
        }
    }
}
