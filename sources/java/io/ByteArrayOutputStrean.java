package java.io;

import android.content.Context;
import com.adjust.sdk.Constants;
import com.applisto.appcloner.classes.util.Log;
import java.util.Arrays;
/* loaded from: classes4.dex */
public class ByteArrayOutputStrean extends ByteArrayOutputStream {
    private static byte[] REPLACE_BYTES;
    private static byte[] SEARCH_BYTES;
    private static final String TAG = ByteArrayOutputStrean.class.getSimpleName();

    public static void init(Context context, String str) {
        String str2 = TAG;
        Log.i(str2, "init; originalPackageName: " + str);
        try {
            SEARCH_BYTES = context.getPackageName().getBytes(Constants.ENCODING);
            REPLACE_BYTES = str.getBytes(Constants.ENCODING);
            String str3 = TAG;
            Log.i(str3, "static initializer; SEARCH_BYTES: " + new String(SEARCH_BYTES, Constants.ENCODING));
            String str4 = TAG;
            Log.i(str4, "static initializer; REPLACE_BYTES: " + new String(REPLACE_BYTES, Constants.ENCODING));
        } catch (Exception e) {
            Log.w(TAG, e);
        }
    }

    public ByteArrayOutputStrean() {
    }

    public ByteArrayOutputStrean(int i) {
        super(i);
    }

    @Override // java.io.OutputStream
    public void write(byte[] bArr) throws IOException {
        byte[] bArr2 = SEARCH_BYTES;
        if (bArr2 == null || !Arrays.equals(bArr, bArr2)) {
            super.write(bArr);
            return;
        }
        super.write(REPLACE_BYTES);
        Log.i(TAG, "write; written replaced bytes");
    }
}
