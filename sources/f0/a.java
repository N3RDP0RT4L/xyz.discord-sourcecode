package f0;

import b.i.a.f.e.o.f;
import com.adjust.sdk.Constants;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.g0.t;
import d0.z.d.m;
import f0.e0.c;
import f0.w;
import java.net.Proxy;
import java.net.ProxySelector;
import java.util.List;
import java.util.Objects;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;
/* compiled from: Address.kt */
/* loaded from: classes3.dex */
public final class a {
    public final w a;

    /* renamed from: b  reason: collision with root package name */
    public final List<y> f3573b;
    public final List<m> c;
    public final s d;
    public final SocketFactory e;
    public final SSLSocketFactory f;
    public final HostnameVerifier g;
    public final g h;
    public final c i;
    public final Proxy j;
    public final ProxySelector k;

    public a(String str, int i, s sVar, SocketFactory socketFactory, SSLSocketFactory sSLSocketFactory, HostnameVerifier hostnameVerifier, g gVar, c cVar, Proxy proxy, List<? extends y> list, List<m> list2, ProxySelector proxySelector) {
        m.checkParameterIsNotNull(str, "uriHost");
        m.checkParameterIsNotNull(sVar, "dns");
        m.checkParameterIsNotNull(socketFactory, "socketFactory");
        m.checkParameterIsNotNull(cVar, "proxyAuthenticator");
        m.checkParameterIsNotNull(list, "protocols");
        m.checkParameterIsNotNull(list2, "connectionSpecs");
        m.checkParameterIsNotNull(proxySelector, "proxySelector");
        this.d = sVar;
        this.e = socketFactory;
        this.f = sSLSocketFactory;
        this.g = hostnameVerifier;
        this.h = gVar;
        this.i = cVar;
        this.j = proxy;
        this.k = proxySelector;
        w.a aVar = new w.a();
        String str2 = sSLSocketFactory != null ? Constants.SCHEME : "http";
        m.checkParameterIsNotNull(str2, "scheme");
        boolean z2 = true;
        if (t.equals(str2, "http", true)) {
            aVar.f3654b = "http";
        } else if (t.equals(str2, Constants.SCHEME, true)) {
            aVar.f3654b = Constants.SCHEME;
        } else {
            throw new IllegalArgumentException(b.d.b.a.a.v("unexpected scheme: ", str2));
        }
        m.checkParameterIsNotNull(str, "host");
        String r1 = f.r1(w.b.d(w.f3653b, str, 0, 0, false, 7));
        if (r1 != null) {
            aVar.e = r1;
            if ((1 > i || 65535 < i) ? false : z2) {
                aVar.f = i;
                this.a = aVar.b();
                this.f3573b = c.z(list);
                this.c = c.z(list2);
                return;
            }
            throw new IllegalArgumentException(b.d.b.a.a.p("unexpected port: ", i).toString());
        }
        throw new IllegalArgumentException(b.d.b.a.a.v("unexpected host: ", str));
    }

    public final boolean a(a aVar) {
        m.checkParameterIsNotNull(aVar, "that");
        return m.areEqual(this.d, aVar.d) && m.areEqual(this.i, aVar.i) && m.areEqual(this.f3573b, aVar.f3573b) && m.areEqual(this.c, aVar.c) && m.areEqual(this.k, aVar.k) && m.areEqual(this.j, aVar.j) && m.areEqual(this.f, aVar.f) && m.areEqual(this.g, aVar.g) && m.areEqual(this.h, aVar.h) && this.a.h == aVar.a.h;
    }

    public boolean equals(Object obj) {
        if (obj instanceof a) {
            a aVar = (a) obj;
            if (m.areEqual(this.a, aVar.a) && a(aVar)) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        int hashCode = this.d.hashCode();
        int hashCode2 = this.i.hashCode();
        int hashCode3 = this.f3573b.hashCode();
        int hashCode4 = this.c.hashCode();
        int hashCode5 = this.k.hashCode();
        int hashCode6 = Objects.hashCode(this.j);
        int hashCode7 = Objects.hashCode(this.f);
        int hashCode8 = Objects.hashCode(this.g);
        return Objects.hashCode(this.h) + ((hashCode8 + ((hashCode7 + ((hashCode6 + ((hashCode5 + ((hashCode4 + ((hashCode3 + ((hashCode2 + ((hashCode + ((this.a.hashCode() + 527) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31);
    }

    public String toString() {
        Object obj;
        StringBuilder sb;
        StringBuilder R = b.d.b.a.a.R("Address{");
        R.append(this.a.g);
        R.append(MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR);
        R.append(this.a.h);
        R.append(", ");
        if (this.j != null) {
            sb = b.d.b.a.a.R("proxy=");
            obj = this.j;
        } else {
            sb = b.d.b.a.a.R("proxySelector=");
            obj = this.k;
        }
        sb.append(obj);
        R.append(sb.toString());
        R.append("}");
        return R.toString();
    }
}
