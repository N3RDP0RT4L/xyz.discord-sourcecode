package f0;

import d0.z.d.m;
import g0.n;
import g0.y;
import java.io.File;
import java.io.FileInputStream;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;
/* compiled from: RequestBody.kt */
/* loaded from: classes3.dex */
public final class z extends RequestBody {
    public final /* synthetic */ File a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ MediaType f3664b;

    public z(File file, MediaType mediaType) {
        this.a = file;
        this.f3664b = mediaType;
    }

    @Override // okhttp3.RequestBody
    public long contentLength() {
        return this.a.length();
    }

    @Override // okhttp3.RequestBody
    public MediaType contentType() {
        return this.f3664b;
    }

    @Override // okhttp3.RequestBody
    public void writeTo(BufferedSink bufferedSink) {
        m.checkParameterIsNotNull(bufferedSink, "sink");
        File file = this.a;
        m.checkParameterIsNotNull(file, "$this$source");
        FileInputStream fileInputStream = new FileInputStream(file);
        m.checkParameterIsNotNull(fileInputStream, "$this$source");
        th = null;
        try {
            bufferedSink.P(new n(fileInputStream, new y()));
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }
}
