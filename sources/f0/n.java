package f0;

import a0.a.a.b;
import d0.g0.t;
import d0.g0.w;
import d0.z.d.m;
import f0.e0.c;
import f0.e0.h.c;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kotlin.TypeCastException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement;
/* compiled from: Cookie.kt */
/* loaded from: classes3.dex */
public final class n {
    public final String f;
    public final String g;
    public final long h;
    public final String i;
    public final String j;
    public final boolean k;
    public final boolean l;
    public final boolean m;
    public final boolean n;
    public static final a e = new a(null);
    public static final Pattern a = Pattern.compile("(\\d{2,4})[^\\d]*");

    /* renamed from: b  reason: collision with root package name */
    public static final Pattern f3648b = Pattern.compile("(?i)(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec).*");
    public static final Pattern c = Pattern.compile("(\\d{1,2})[^\\d]*");
    public static final Pattern d = Pattern.compile("(\\d{1,2}):(\\d{1,2}):(\\d{1,2})[^\\d]*");

    /* compiled from: Cookie.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final int a(String str, int i, int i2, boolean z2) {
            while (i < i2) {
                char charAt = str.charAt(i);
                if (((charAt < ' ' && charAt != '\t') || charAt >= 127 || ('0' <= charAt && '9' >= charAt) || (('a' <= charAt && 'z' >= charAt) || (('A' <= charAt && 'Z' >= charAt) || charAt == ':'))) == (!z2)) {
                    return i;
                }
                i++;
            }
            return i2;
        }

        public final boolean b(String str, String str2) {
            if (m.areEqual(str, str2)) {
                return true;
            }
            if (t.endsWith$default(str, str2, false, 2, null) && str.charAt((str.length() - str2.length()) - 1) == '.') {
                byte[] bArr = c.a;
                m.checkParameterIsNotNull(str, "$this$canParseAsIpAddress");
                if (!c.f.matches(str)) {
                    return true;
                }
            }
            return false;
        }

        public final long c(String str, int i, int i2) {
            int a = a(str, i, i2, false);
            Matcher matcher = n.d.matcher(str);
            int i3 = -1;
            int i4 = -1;
            int i5 = -1;
            int i6 = -1;
            int i7 = -1;
            int i8 = -1;
            while (a < i2) {
                int a2 = a(str, a + 1, i2, true);
                matcher.region(a, a2);
                if (i4 == -1 && matcher.usePattern(n.d).matches()) {
                    String group = matcher.group(1);
                    m.checkExpressionValueIsNotNull(group, "matcher.group(1)");
                    i4 = Integer.parseInt(group);
                    String group2 = matcher.group(2);
                    m.checkExpressionValueIsNotNull(group2, "matcher.group(2)");
                    i7 = Integer.parseInt(group2);
                    String group3 = matcher.group(3);
                    m.checkExpressionValueIsNotNull(group3, "matcher.group(3)");
                    i8 = Integer.parseInt(group3);
                } else if (i5 != -1 || !matcher.usePattern(n.c).matches()) {
                    if (i6 == -1) {
                        Pattern pattern = n.f3648b;
                        if (matcher.usePattern(pattern).matches()) {
                            String group4 = matcher.group(1);
                            m.checkExpressionValueIsNotNull(group4, "matcher.group(1)");
                            Locale locale = Locale.US;
                            m.checkExpressionValueIsNotNull(locale, "Locale.US");
                            if (group4 != null) {
                                String lowerCase = group4.toLowerCase(locale);
                                m.checkExpressionValueIsNotNull(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                                String pattern2 = pattern.pattern();
                                m.checkExpressionValueIsNotNull(pattern2, "MONTH_PATTERN.pattern()");
                                i6 = w.indexOf$default((CharSequence) pattern2, lowerCase, 0, false, 6, (Object) null) / 4;
                            } else {
                                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                            }
                        }
                    }
                    if (i3 == -1 && matcher.usePattern(n.a).matches()) {
                        String group5 = matcher.group(1);
                        m.checkExpressionValueIsNotNull(group5, "matcher.group(1)");
                        i3 = Integer.parseInt(group5);
                    }
                } else {
                    String group6 = matcher.group(1);
                    m.checkExpressionValueIsNotNull(group6, "matcher.group(1)");
                    i5 = Integer.parseInt(group6);
                }
                a = a(str, a2 + 1, i2, false);
            }
            if (70 <= i3 && 99 >= i3) {
                i3 += 1900;
            }
            if (i3 >= 0 && 69 >= i3) {
                i3 += 2000;
            }
            if (i3 >= 1601) {
                if (i6 != -1) {
                    if (1 <= i5 && 31 >= i5) {
                        if (i4 >= 0 && 23 >= i4) {
                            if (i7 >= 0 && 59 >= i7) {
                                if (i8 >= 0 && 59 >= i8) {
                                    GregorianCalendar gregorianCalendar = new GregorianCalendar(c.e);
                                    gregorianCalendar.setLenient(false);
                                    gregorianCalendar.set(1, i3);
                                    gregorianCalendar.set(2, i6 - 1);
                                    gregorianCalendar.set(5, i5);
                                    gregorianCalendar.set(11, i4);
                                    gregorianCalendar.set(12, i7);
                                    gregorianCalendar.set(13, i8);
                                    gregorianCalendar.set(14, 0);
                                    return gregorianCalendar.getTimeInMillis();
                                }
                                throw new IllegalArgumentException("Failed requirement.".toString());
                            }
                            throw new IllegalArgumentException("Failed requirement.".toString());
                        }
                        throw new IllegalArgumentException("Failed requirement.".toString());
                    }
                    throw new IllegalArgumentException("Failed requirement.".toString());
                }
                throw new IllegalArgumentException("Failed requirement.".toString());
            }
            throw new IllegalArgumentException("Failed requirement.".toString());
        }
    }

    public n(String str, String str2, long j, String str3, String str4, boolean z2, boolean z3, boolean z4, boolean z5, DefaultConstructorMarker defaultConstructorMarker) {
        this.f = str;
        this.g = str2;
        this.h = j;
        this.i = str3;
        this.j = str4;
        this.k = z2;
        this.l = z3;
        this.m = z4;
        this.n = z5;
    }

    /* JADX WARN: Code restructure failed: missing block: B:12:0x0046, code lost:
        if (f0.e0.c.f.matches(r0) == false) goto L13;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final boolean a(f0.w r8) {
        /*
            r7 = this;
            java.lang.String r0 = "url"
            d0.z.d.m.checkParameterIsNotNull(r8, r0)
            boolean r0 = r7.n
            r1 = 0
            r2 = 2
            r3 = 1
            r4 = 0
            if (r0 == 0) goto L16
            java.lang.String r0 = r8.g
            java.lang.String r5 = r7.i
            boolean r0 = d0.z.d.m.areEqual(r0, r5)
            goto L4b
        L16:
            java.lang.String r0 = r8.g
            java.lang.String r5 = r7.i
            boolean r6 = d0.z.d.m.areEqual(r0, r5)
            if (r6 == 0) goto L21
            goto L48
        L21:
            boolean r6 = d0.g0.t.endsWith$default(r0, r5, r4, r2, r1)
            if (r6 == 0) goto L4a
            int r6 = r0.length()
            int r5 = r5.length()
            int r6 = r6 - r5
            int r6 = r6 - r3
            char r5 = r0.charAt(r6)
            r6 = 46
            if (r5 != r6) goto L4a
            byte[] r5 = f0.e0.c.a
            java.lang.String r5 = "$this$canParseAsIpAddress"
            d0.z.d.m.checkParameterIsNotNull(r0, r5)
            kotlin.text.Regex r5 = f0.e0.c.f
            boolean r0 = r5.matches(r0)
            if (r0 != 0) goto L4a
        L48:
            r0 = 1
            goto L4b
        L4a:
            r0 = 0
        L4b:
            if (r0 != 0) goto L4e
            return r4
        L4e:
            java.lang.String r0 = r7.j
            java.lang.String r5 = r8.b()
            boolean r6 = d0.z.d.m.areEqual(r5, r0)
            if (r6 == 0) goto L5b
            goto L76
        L5b:
            boolean r6 = d0.g0.t.startsWith$default(r5, r0, r4, r2, r1)
            if (r6 == 0) goto L78
            java.lang.String r6 = "/"
            boolean r1 = d0.g0.t.endsWith$default(r0, r6, r4, r2, r1)
            if (r1 == 0) goto L6a
            goto L76
        L6a:
            int r0 = r0.length()
            char r0 = r5.charAt(r0)
            r1 = 47
            if (r0 != r1) goto L78
        L76:
            r0 = 1
            goto L79
        L78:
            r0 = 0
        L79:
            if (r0 != 0) goto L7c
            return r4
        L7c:
            boolean r0 = r7.k
            if (r0 == 0) goto L86
            boolean r8 = r8.c
            if (r8 == 0) goto L85
            goto L86
        L85:
            r3 = 0
        L86:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: f0.n.a(f0.w):boolean");
    }

    public boolean equals(Object obj) {
        if (obj instanceof n) {
            n nVar = (n) obj;
            if (m.areEqual(nVar.f, this.f) && m.areEqual(nVar.g, this.g) && nVar.h == this.h && m.areEqual(nVar.i, this.i) && m.areEqual(nVar.j, this.j) && nVar.k == this.k && nVar.l == this.l && nVar.m == this.m && nVar.n == this.n) {
                return true;
            }
        }
        return false;
    }

    @IgnoreJRERequirement
    public int hashCode() {
        int m = b.d.b.a.a.m(this.g, b.d.b.a.a.m(this.f, 527, 31), 31);
        int m2 = b.d.b.a.a.m(this.j, b.d.b.a.a.m(this.i, (b.a(this.h) + m) * 31, 31), 31);
        int a2 = b.a.f.c.a(this.l);
        int a3 = b.a.f.c.a(this.m);
        return b.a.f.c.a(this.n) + ((a3 + ((a2 + ((b.a.f.c.a(this.k) + m2) * 31)) * 31)) * 31);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f);
        sb.append('=');
        sb.append(this.g);
        if (this.m) {
            if (this.h == Long.MIN_VALUE) {
                sb.append("; max-age=0");
            } else {
                sb.append("; expires=");
                Date date = new Date(this.h);
                c.a aVar = f0.e0.h.c.a;
                m.checkParameterIsNotNull(date, "$this$toHttpDateString");
                String format = f0.e0.h.c.a.get().format(date);
                m.checkExpressionValueIsNotNull(format, "STANDARD_DATE_FORMAT.get().format(this)");
                sb.append(format);
            }
        }
        if (!this.n) {
            sb.append("; domain=");
            sb.append(this.i);
        }
        sb.append("; path=");
        sb.append(this.j);
        if (this.k) {
            sb.append("; secure");
        }
        if (this.l) {
            sb.append("; httponly");
        }
        String sb2 = sb.toString();
        m.checkExpressionValueIsNotNull(sb2, "toString()");
        return sb2;
    }
}
