package f0;

import g0.g;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
/* compiled from: ResponseBody.kt */
/* loaded from: classes3.dex */
public final class b0 extends ResponseBody {
    public final /* synthetic */ g l;
    public final /* synthetic */ MediaType m;
    public final /* synthetic */ long n;

    public b0(g gVar, MediaType mediaType, long j) {
        this.l = gVar;
        this.m = mediaType;
        this.n = j;
    }

    @Override // okhttp3.ResponseBody
    public long a() {
        return this.n;
    }

    @Override // okhttp3.ResponseBody
    public MediaType b() {
        return this.m;
    }

    @Override // okhttp3.ResponseBody
    public g c() {
        return this.l;
    }
}
