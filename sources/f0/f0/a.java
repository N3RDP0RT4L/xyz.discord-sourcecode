package f0.f0;

import androidx.recyclerview.widget.RecyclerView;
import b.i.a.f.e.o.f;
import com.discord.analytics.utils.RegistrationSteps;
import d0.g0.t;
import d0.t.n0;
import d0.z.d.m;
import f0.e0.g.j;
import f0.e0.h.g;
import f0.k;
import g0.e;
import g0.l;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
/* compiled from: HttpLoggingInterceptor.kt */
/* loaded from: classes3.dex */
public final class a implements Interceptor {

    /* renamed from: b  reason: collision with root package name */
    public volatile Set<String> f3641b = n0.emptySet();
    public volatile EnumC0390a c = EnumC0390a.NONE;
    public final b d;

    /* compiled from: HttpLoggingInterceptor.kt */
    /* renamed from: f0.f0.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public enum EnumC0390a {
        NONE,
        BASIC,
        HEADERS,
        BODY
    }

    /* compiled from: HttpLoggingInterceptor.kt */
    /* loaded from: classes3.dex */
    public interface b {
        void log(String str);
    }

    public a(b bVar) {
        m.checkParameterIsNotNull(bVar, "logger");
        this.d = bVar;
    }

    public final boolean a(Headers headers) {
        String c = headers.c("Content-Encoding");
        return c != null && !t.equals(c, RegistrationSteps.IDENTITY, true) && !t.equals(c, "gzip", true);
    }

    public final void b(Headers headers, int i) {
        int i2 = i * 2;
        String str = this.f3641b.contains(headers.k[i2]) ? "██" : headers.k[i2 + 1];
        b bVar = this.d;
        bVar.log(headers.k[i2] + ": " + str);
    }

    /* JADX WARN: Finally extract failed */
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) throws IOException {
        String str;
        String str2;
        String str3;
        Charset charset;
        Charset charset2;
        m.checkParameterIsNotNull(chain, "chain");
        EnumC0390a aVar = this.c;
        g gVar = (g) chain;
        Request request = gVar.f;
        if (aVar == EnumC0390a.NONE) {
            return gVar.a(request);
        }
        boolean z2 = aVar == EnumC0390a.BODY;
        boolean z3 = z2 || aVar == EnumC0390a.HEADERS;
        RequestBody requestBody = request.e;
        k b2 = gVar.b();
        StringBuilder R = b.d.b.a.a.R("--> ");
        R.append(request.c);
        R.append(' ');
        R.append(request.f3784b);
        if (b2 != null) {
            StringBuilder R2 = b.d.b.a.a.R(" ");
            R2.append(((j) b2).m());
            str = R2.toString();
        } else {
            str = "";
        }
        R.append(str);
        String sb = R.toString();
        if (!z3 && requestBody != null) {
            StringBuilder V = b.d.b.a.a.V(sb, " (");
            V.append(requestBody.contentLength());
            V.append("-byte body)");
            sb = V.toString();
        }
        this.d.log(sb);
        if (z3) {
            Headers headers = request.d;
            if (requestBody != null) {
                MediaType contentType = requestBody.contentType();
                if (contentType != null && headers.c("Content-Type") == null) {
                    this.d.log("Content-Type: " + contentType);
                }
                if (requestBody.contentLength() != -1 && headers.c("Content-Length") == null) {
                    b bVar = this.d;
                    StringBuilder R3 = b.d.b.a.a.R("Content-Length: ");
                    R3.append(requestBody.contentLength());
                    bVar.log(R3.toString());
                }
            }
            int size = headers.size();
            for (int i = 0; i < size; i++) {
                b(headers, i);
            }
            if (!z2 || requestBody == null) {
                b bVar2 = this.d;
                StringBuilder R4 = b.d.b.a.a.R("--> END ");
                R4.append(request.c);
                bVar2.log(R4.toString());
            } else if (a(request.d)) {
                b bVar3 = this.d;
                StringBuilder R5 = b.d.b.a.a.R("--> END ");
                R5.append(request.c);
                R5.append(" (encoded body omitted)");
                bVar3.log(R5.toString());
            } else if (requestBody.isDuplex()) {
                b bVar4 = this.d;
                StringBuilder R6 = b.d.b.a.a.R("--> END ");
                R6.append(request.c);
                R6.append(" (duplex request body omitted)");
                bVar4.log(R6.toString());
            } else if (requestBody.isOneShot()) {
                b bVar5 = this.d;
                StringBuilder R7 = b.d.b.a.a.R("--> END ");
                R7.append(request.c);
                R7.append(" (one-shot body omitted)");
                bVar5.log(R7.toString());
            } else {
                e eVar = new e();
                requestBody.writeTo(eVar);
                MediaType contentType2 = requestBody.contentType();
                if (contentType2 == null || (charset2 = contentType2.a(StandardCharsets.UTF_8)) == null) {
                    charset2 = StandardCharsets.UTF_8;
                    m.checkExpressionValueIsNotNull(charset2, "UTF_8");
                }
                this.d.log("");
                if (f.F0(eVar)) {
                    this.d.log(eVar.M(charset2));
                    b bVar6 = this.d;
                    StringBuilder R8 = b.d.b.a.a.R("--> END ");
                    R8.append(request.c);
                    R8.append(" (");
                    R8.append(requestBody.contentLength());
                    R8.append("-byte body)");
                    bVar6.log(R8.toString());
                } else {
                    b bVar7 = this.d;
                    StringBuilder R9 = b.d.b.a.a.R("--> END ");
                    R9.append(request.c);
                    R9.append(" (binary ");
                    R9.append(requestBody.contentLength());
                    R9.append("-byte body omitted)");
                    bVar7.log(R9.toString());
                }
            }
        }
        long nanoTime = System.nanoTime();
        try {
            Response a = gVar.a(request);
            long millis = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - nanoTime);
            ResponseBody responseBody = a.p;
            if (responseBody == null) {
                m.throwNpe();
            }
            long a2 = responseBody.a();
            String str4 = a2 != -1 ? a2 + "-byte" : "unknown-length";
            b bVar8 = this.d;
            StringBuilder R10 = b.d.b.a.a.R("<-- ");
            R10.append(a.m);
            if (a.l.length() == 0) {
                str2 = "-byte body omitted)";
                str3 = "";
            } else {
                String str5 = a.l;
                StringBuilder sb2 = new StringBuilder();
                str2 = "-byte body omitted)";
                sb2.append(String.valueOf(' '));
                sb2.append(str5);
                str3 = sb2.toString();
            }
            R10.append(str3);
            R10.append(' ');
            R10.append(a.j.f3784b);
            R10.append(" (");
            R10.append(millis);
            R10.append("ms");
            R10.append(!z3 ? b.d.b.a.a.w(", ", str4, " body") : "");
            R10.append(')');
            bVar8.log(R10.toString());
            if (z3) {
                Headers headers2 = a.o;
                int size2 = headers2.size();
                for (int i2 = 0; i2 < size2; i2++) {
                    b(headers2, i2);
                }
                if (!z2 || !f0.e0.h.e.a(a)) {
                    this.d.log("<-- END HTTP");
                } else if (a(a.o)) {
                    this.d.log("<-- END HTTP (encoded body omitted)");
                } else {
                    g0.g c = responseBody.c();
                    c.j(RecyclerView.FOREVER_NS);
                    e g = c.g();
                    Long l = null;
                    if (t.equals("gzip", headers2.c("Content-Encoding"), true)) {
                        Long valueOf = Long.valueOf(g.k);
                        l lVar = new l(g.clone());
                        try {
                            g = new e();
                            g.P(lVar);
                            d0.y.b.closeFinally(lVar, null);
                            l = valueOf;
                        } finally {
                            try {
                                throw th;
                            } catch (Throwable th) {
                            }
                        }
                    }
                    MediaType b3 = responseBody.b();
                    if (b3 == null || (charset = b3.a(StandardCharsets.UTF_8)) == null) {
                        charset = StandardCharsets.UTF_8;
                        m.checkExpressionValueIsNotNull(charset, "UTF_8");
                    }
                    if (!f.F0(g)) {
                        this.d.log("");
                        b bVar9 = this.d;
                        StringBuilder R11 = b.d.b.a.a.R("<-- END HTTP (binary ");
                        R11.append(g.k);
                        R11.append(str2);
                        bVar9.log(R11.toString());
                        return a;
                    }
                    if (a2 != 0) {
                        this.d.log("");
                        this.d.log(g.clone().M(charset));
                    }
                    if (l != null) {
                        b bVar10 = this.d;
                        StringBuilder R12 = b.d.b.a.a.R("<-- END HTTP (");
                        R12.append(g.k);
                        R12.append("-byte, ");
                        R12.append(l);
                        R12.append("-gzipped-byte body)");
                        bVar10.log(R12.toString());
                    } else {
                        b bVar11 = this.d;
                        StringBuilder R13 = b.d.b.a.a.R("<-- END HTTP (");
                        R13.append(g.k);
                        R13.append("-byte body)");
                        bVar11.log(R13.toString());
                    }
                }
            }
            return a;
        } catch (Exception e) {
            this.d.log("<-- HTTP FAILED: " + e);
            throw e;
        }
    }
}
