package f0;

import andhook.lib.xposed.callbacks.XCallback;
import com.discord.api.permission.Permission;
import d0.t.r;
import d0.t.u;
import d0.z.d.m;
import f0.e;
import f0.e0.g.l;
import f0.e0.k.h;
import f0.e0.m.c;
import f0.e0.m.d;
import f0.t;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.ProxySelector;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;
import kotlin.TypeCastException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
/* compiled from: OkHttpClient.kt */
/* loaded from: classes3.dex */
public class x implements Cloneable, e.a {
    public final SocketFactory A;
    public final SSLSocketFactory B;
    public final X509TrustManager C;
    public final List<m> D;
    public final List<y> E;
    public final HostnameVerifier F;
    public final g G;
    public final c H;
    public final int I;
    public final int J;
    public final int K;
    public final int L;
    public final int M;
    public final long N;
    public final l O;
    public final q m;
    public final l n;
    public final List<Interceptor> o;
    public final List<Interceptor> p;
    public final t.b q;
    public final boolean r;

    /* renamed from: s  reason: collision with root package name */
    public final c f3655s;
    public final boolean t;
    public final boolean u;
    public final p v;
    public final s w;

    /* renamed from: x  reason: collision with root package name */
    public final Proxy f3656x;

    /* renamed from: y  reason: collision with root package name */
    public final ProxySelector f3657y;

    /* renamed from: z  reason: collision with root package name */
    public final c f3658z;
    public static final b l = new b(null);
    public static final List<y> j = f0.e0.c.m(y.HTTP_2, y.HTTP_1_1);
    public static final List<m> k = f0.e0.c.m(m.c, m.d);

    /* compiled from: OkHttpClient.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public int A;
        public l C;
        public t.b e;
        public c g;
        public Proxy l;
        public ProxySelector m;
        public c n;
        public SocketFactory o;
        public SSLSocketFactory p;
        public X509TrustManager q;
        public c v;
        public int w;
        public q a = new q();

        /* renamed from: b  reason: collision with root package name */
        public l f3659b = new l();
        public final List<Interceptor> c = new ArrayList();
        public final List<Interceptor> d = new ArrayList();
        public boolean f = true;
        public boolean h = true;
        public boolean i = true;
        public p j = p.a;
        public s k = s.a;
        public List<m> r = x.k;

        /* renamed from: s  reason: collision with root package name */
        public List<? extends y> f3660s = x.j;
        public HostnameVerifier t = d.a;
        public g u = g.a;

        /* renamed from: x  reason: collision with root package name */
        public int f3661x = XCallback.PRIORITY_HIGHEST;

        /* renamed from: y  reason: collision with root package name */
        public int f3662y = XCallback.PRIORITY_HIGHEST;

        /* renamed from: z  reason: collision with root package name */
        public int f3663z = XCallback.PRIORITY_HIGHEST;
        public long B = Permission.VIEW_CHANNEL;

        public a() {
            t tVar = t.a;
            m.checkParameterIsNotNull(tVar, "$this$asFactory");
            this.e = new f0.e0.a(tVar);
            c cVar = c.a;
            this.g = cVar;
            this.n = cVar;
            SocketFactory socketFactory = SocketFactory.getDefault();
            m.checkExpressionValueIsNotNull(socketFactory, "SocketFactory.getDefault()");
            this.o = socketFactory;
            b bVar = x.l;
        }

        public final a a(long j, TimeUnit timeUnit) {
            m.checkParameterIsNotNull(timeUnit, "unit");
            this.f3662y = f0.e0.c.b("timeout", j, timeUnit);
            return this;
        }

        public final a b(SSLSocketFactory sSLSocketFactory, X509TrustManager x509TrustManager) {
            m.checkParameterIsNotNull(sSLSocketFactory, "sslSocketFactory");
            m.checkParameterIsNotNull(x509TrustManager, "trustManager");
            if ((!m.areEqual(sSLSocketFactory, this.p)) || (!m.areEqual(x509TrustManager, this.q))) {
                this.C = null;
            }
            this.p = sSLSocketFactory;
            m.checkParameterIsNotNull(x509TrustManager, "trustManager");
            h.a aVar = h.c;
            this.v = h.a.b(x509TrustManager);
            this.q = x509TrustManager;
            return this;
        }
    }

    /* compiled from: OkHttpClient.kt */
    /* loaded from: classes3.dex */
    public static final class b {
        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    public x(a aVar) {
        ProxySelector proxySelector;
        boolean z2;
        boolean z3;
        m.checkParameterIsNotNull(aVar, "builder");
        this.m = aVar.a;
        this.n = aVar.f3659b;
        this.o = f0.e0.c.z(aVar.c);
        this.p = f0.e0.c.z(aVar.d);
        this.q = aVar.e;
        this.r = aVar.f;
        this.f3655s = aVar.g;
        this.t = aVar.h;
        this.u = aVar.i;
        this.v = aVar.j;
        this.w = aVar.k;
        Proxy proxy = aVar.l;
        this.f3656x = proxy;
        if (proxy != null) {
            proxySelector = f0.e0.l.a.a;
        } else {
            proxySelector = aVar.m;
            proxySelector = proxySelector == null ? ProxySelector.getDefault() : proxySelector;
            if (proxySelector == null) {
                proxySelector = f0.e0.l.a.a;
            }
        }
        this.f3657y = proxySelector;
        this.f3658z = aVar.n;
        this.A = aVar.o;
        List<m> list = aVar.r;
        this.D = list;
        this.E = aVar.f3660s;
        this.F = aVar.t;
        this.I = aVar.w;
        this.J = aVar.f3661x;
        this.K = aVar.f3662y;
        this.L = aVar.f3663z;
        this.M = aVar.A;
        this.N = aVar.B;
        l lVar = aVar.C;
        this.O = lVar == null ? new l() : lVar;
        boolean z4 = false;
        if (!(list instanceof Collection) || !list.isEmpty()) {
            for (m mVar : list) {
                if (mVar.e) {
                    z2 = false;
                    break;
                }
            }
        }
        z2 = true;
        if (z2) {
            this.B = null;
            this.H = null;
            this.C = null;
            this.G = g.a;
        } else {
            SSLSocketFactory sSLSocketFactory = aVar.p;
            if (sSLSocketFactory != null) {
                this.B = sSLSocketFactory;
                c cVar = aVar.v;
                if (cVar == null) {
                    m.throwNpe();
                }
                this.H = cVar;
                X509TrustManager x509TrustManager = aVar.q;
                if (x509TrustManager == null) {
                    m.throwNpe();
                }
                this.C = x509TrustManager;
                g gVar = aVar.u;
                if (cVar == null) {
                    m.throwNpe();
                }
                this.G = gVar.b(cVar);
            } else {
                h.a aVar2 = h.c;
                X509TrustManager n = h.a.n();
                this.C = n;
                h hVar = h.a;
                if (n == null) {
                    m.throwNpe();
                }
                this.B = hVar.m(n);
                if (n == null) {
                    m.throwNpe();
                }
                m.checkParameterIsNotNull(n, "trustManager");
                c b2 = h.a.b(n);
                this.H = b2;
                g gVar2 = aVar.u;
                if (b2 == null) {
                    m.throwNpe();
                }
                this.G = gVar2.b(b2);
            }
        }
        List<Interceptor> list2 = this.o;
        if (list2 == null) {
            throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.List<okhttp3.Interceptor?>");
        } else if (!list2.contains(null)) {
            List<Interceptor> list3 = this.p;
            if (list3 == null) {
                throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.List<okhttp3.Interceptor?>");
            } else if (!list3.contains(null)) {
                List<m> list4 = this.D;
                if (!(list4 instanceof Collection) || !list4.isEmpty()) {
                    for (m mVar2 : list4) {
                        if (mVar2.e) {
                            z3 = false;
                            break;
                        }
                    }
                }
                z3 = true;
                if (z3) {
                    if (!(this.B == null)) {
                        throw new IllegalStateException("Check failed.".toString());
                    } else if (!(this.H == null)) {
                        throw new IllegalStateException("Check failed.".toString());
                    } else if (!(this.C == null ? true : z4)) {
                        throw new IllegalStateException("Check failed.".toString());
                    } else if (!m.areEqual(this.G, g.a)) {
                        throw new IllegalStateException("Check failed.".toString());
                    }
                } else if (this.B == null) {
                    throw new IllegalStateException("sslSocketFactory == null".toString());
                } else if (this.H == null) {
                    throw new IllegalStateException("certificateChainCleaner == null".toString());
                } else if (this.C == null) {
                    throw new IllegalStateException("x509TrustManager == null".toString());
                }
            } else {
                StringBuilder R = b.d.b.a.a.R("Null network interceptor: ");
                R.append(this.p);
                throw new IllegalStateException(R.toString().toString());
            }
        } else {
            StringBuilder R2 = b.d.b.a.a.R("Null interceptor: ");
            R2.append(this.o);
            throw new IllegalStateException(R2.toString().toString());
        }
    }

    @Override // f0.e.a
    public e b(Request request) {
        m.checkParameterIsNotNull(request, "request");
        return new f0.e0.g.e(this, request, false);
    }

    public Object clone() {
        return super.clone();
    }

    public a f() {
        m.checkParameterIsNotNull(this, "okHttpClient");
        a aVar = new a();
        aVar.a = this.m;
        aVar.f3659b = this.n;
        r.addAll(aVar.c, this.o);
        r.addAll(aVar.d, this.p);
        aVar.e = this.q;
        aVar.f = this.r;
        aVar.g = this.f3655s;
        aVar.h = this.t;
        aVar.i = this.u;
        aVar.j = this.v;
        aVar.k = this.w;
        aVar.l = this.f3656x;
        aVar.m = this.f3657y;
        aVar.n = this.f3658z;
        aVar.o = this.A;
        aVar.p = this.B;
        aVar.q = this.C;
        aVar.r = this.D;
        aVar.f3660s = this.E;
        aVar.t = this.F;
        aVar.u = this.G;
        aVar.v = this.H;
        aVar.w = this.I;
        aVar.f3661x = this.J;
        aVar.f3662y = this.K;
        aVar.f3663z = this.L;
        aVar.A = this.M;
        aVar.B = this.N;
        aVar.C = this.O;
        return aVar;
    }

    public WebSocket g(Request request, WebSocketListener webSocketListener) {
        m.checkParameterIsNotNull(request, "request");
        m.checkParameterIsNotNull(webSocketListener, "listener");
        f0.e0.n.d dVar = new f0.e0.n.d(f0.e0.f.d.a, request, webSocketListener, new Random(), this.M, null, this.N);
        m.checkParameterIsNotNull(this, "client");
        if (dVar.u.b("Sec-WebSocket-Extensions") != null) {
            dVar.i(new ProtocolException("Request header not permitted: 'Sec-WebSocket-Extensions'"), null);
        } else {
            a f = f();
            t tVar = t.a;
            m.checkParameterIsNotNull(tVar, "eventListener");
            byte[] bArr = f0.e0.c.a;
            m.checkParameterIsNotNull(tVar, "$this$asFactory");
            f.e = new f0.e0.a(tVar);
            List<y> list = f0.e0.n.d.a;
            m.checkParameterIsNotNull(list, "protocols");
            List mutableList = u.toMutableList((Collection) list);
            y yVar = y.H2_PRIOR_KNOWLEDGE;
            boolean z2 = false;
            if (mutableList.contains(yVar) || mutableList.contains(y.HTTP_1_1)) {
                if (!mutableList.contains(yVar) || mutableList.size() <= 1) {
                    z2 = true;
                }
                if (!z2) {
                    throw new IllegalArgumentException(("protocols containing h2_prior_knowledge cannot use other protocols: " + mutableList).toString());
                } else if (!(!mutableList.contains(y.HTTP_1_0))) {
                    throw new IllegalArgumentException(("protocols must not contain http/1.0: " + mutableList).toString());
                } else if (!mutableList.contains(null)) {
                    mutableList.remove(y.SPDY_3);
                    if (!m.areEqual(mutableList, f.f3660s)) {
                        f.C = null;
                    }
                    List<? extends y> unmodifiableList = Collections.unmodifiableList(mutableList);
                    m.checkExpressionValueIsNotNull(unmodifiableList, "Collections.unmodifiableList(protocolsCopy)");
                    f.f3660s = unmodifiableList;
                    x xVar = new x(f);
                    Request.a aVar = new Request.a(dVar.u);
                    aVar.b("Upgrade", "websocket");
                    aVar.b("Connection", "Upgrade");
                    aVar.b("Sec-WebSocket-Key", dVar.f3628b);
                    aVar.b("Sec-WebSocket-Version", "13");
                    aVar.b("Sec-WebSocket-Extensions", "permessage-deflate");
                    Request a2 = aVar.a();
                    f0.e0.g.e eVar = new f0.e0.g.e(xVar, a2, true);
                    dVar.c = eVar;
                    eVar.e(new f0.e0.n.e(dVar, a2));
                } else {
                    throw new IllegalArgumentException("protocols must not contain null".toString());
                }
            } else {
                throw new IllegalArgumentException(("protocols must contain h2_prior_knowledge or http/1.1: " + mutableList).toString());
            }
        }
        return dVar;
    }

    public x() {
        this(new a());
    }
}
