package f0;

import d0.z.d.m;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;
import okio.ByteString;
/* compiled from: RequestBody.kt */
/* loaded from: classes3.dex */
public final class a0 extends RequestBody {
    public final /* synthetic */ ByteString a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ MediaType f3574b;

    public a0(ByteString byteString, MediaType mediaType) {
        this.a = byteString;
        this.f3574b = mediaType;
    }

    @Override // okhttp3.RequestBody
    public long contentLength() {
        return this.a.j();
    }

    @Override // okhttp3.RequestBody
    public MediaType contentType() {
        return this.f3574b;
    }

    @Override // okhttp3.RequestBody
    public void writeTo(BufferedSink bufferedSink) {
        m.checkParameterIsNotNull(bufferedSink, "sink");
        bufferedSink.e0(this.a);
    }
}
