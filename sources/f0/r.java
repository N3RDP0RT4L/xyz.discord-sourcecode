package f0;

import b.d.b.a.a;
import d0.t.k;
import d0.z.d.m;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
/* compiled from: Dns.kt */
/* loaded from: classes3.dex */
public final class r implements s {
    @Override // f0.s
    public List<InetAddress> a(String str) {
        m.checkParameterIsNotNull(str, "hostname");
        try {
            InetAddress[] allByName = InetAddress.getAllByName(str);
            m.checkExpressionValueIsNotNull(allByName, "InetAddress.getAllByName(hostname)");
            return k.toList(allByName);
        } catch (NullPointerException e) {
            UnknownHostException unknownHostException = new UnknownHostException(a.v("Broken system behaviour for dns lookup of ", str));
            unknownHostException.initCause(e);
            throw unknownHostException;
        }
    }
}
