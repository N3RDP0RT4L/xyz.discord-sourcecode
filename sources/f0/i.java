package f0;

import b.d.b.a.a;
import d0.z.d.m;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
/* compiled from: Challenge.kt */
/* loaded from: classes3.dex */
public final class i {
    public final Map<String, String> a;

    /* renamed from: b  reason: collision with root package name */
    public final String f3643b;

    public i(String str, Map<String, String> map) {
        String str2;
        m.checkParameterIsNotNull(str, "scheme");
        m.checkParameterIsNotNull(map, "authParams");
        this.f3643b = str;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            if (key != null) {
                Locale locale = Locale.US;
                m.checkExpressionValueIsNotNull(locale, "US");
                str2 = key.toLowerCase(locale);
                m.checkExpressionValueIsNotNull(str2, "(this as java.lang.String).toLowerCase(locale)");
            } else {
                str2 = null;
            }
            linkedHashMap.put(str2, value);
        }
        Map<String, String> unmodifiableMap = Collections.unmodifiableMap(linkedHashMap);
        m.checkExpressionValueIsNotNull(unmodifiableMap, "unmodifiableMap<String?, String>(newAuthParams)");
        this.a = unmodifiableMap;
    }

    public boolean equals(Object obj) {
        if (obj instanceof i) {
            i iVar = (i) obj;
            if (m.areEqual(iVar.f3643b, this.f3643b) && m.areEqual(iVar.a, this.a)) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        return this.a.hashCode() + a.m(this.f3643b, 899, 31);
    }

    public String toString() {
        return this.f3643b + " authParams=" + this.a;
    }
}
