package f0;

import androidx.browser.trusted.sharing.ShareTarget;
import d0.z.d.m;
import f0.e0.c;
import g0.e;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;
/* compiled from: FormBody.kt */
/* loaded from: classes3.dex */
public final class u extends RequestBody {
    public static final MediaType a = MediaType.a.a(ShareTarget.ENCODING_TYPE_URL_ENCODED);

    /* renamed from: b  reason: collision with root package name */
    public final List<String> f3650b;
    public final List<String> c;

    /* compiled from: FormBody.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public final Charset c = null;
        public final List<String> a = new ArrayList();

        /* renamed from: b  reason: collision with root package name */
        public final List<String> f3651b = new ArrayList();
    }

    static {
        MediaType.a aVar = MediaType.c;
    }

    public u(List<String> list, List<String> list2) {
        m.checkParameterIsNotNull(list, "encodedNames");
        m.checkParameterIsNotNull(list2, "encodedValues");
        this.f3650b = c.z(list);
        this.c = c.z(list2);
    }

    public final long a(BufferedSink bufferedSink, boolean z2) {
        e eVar;
        if (z2) {
            eVar = new e();
        } else {
            if (bufferedSink == null) {
                m.throwNpe();
            }
            eVar = bufferedSink.g();
        }
        int size = this.f3650b.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                eVar.T(38);
            }
            eVar.b0(this.f3650b.get(i));
            eVar.T(61);
            eVar.b0(this.c.get(i));
        }
        if (!z2) {
            return 0L;
        }
        long j = eVar.k;
        eVar.skip(j);
        return j;
    }

    @Override // okhttp3.RequestBody
    public long contentLength() {
        return a(null, true);
    }

    @Override // okhttp3.RequestBody
    public MediaType contentType() {
        return a;
    }

    @Override // okhttp3.RequestBody
    public void writeTo(BufferedSink bufferedSink) throws IOException {
        m.checkParameterIsNotNull(bufferedSink, "sink");
        a(bufferedSink, false);
    }
}
