package f0.e0;

import androidx.core.app.NotificationCompat;
import d0.z.d.m;
import f0.e;
import f0.t;
/* compiled from: Util.kt */
/* loaded from: classes3.dex */
public final class a implements t.b {
    public final /* synthetic */ t a;

    public a(t tVar) {
        this.a = tVar;
    }

    @Override // f0.t.b
    public t a(e eVar) {
        m.checkParameterIsNotNull(eVar, NotificationCompat.CATEGORY_CALL);
        return this.a;
    }
}
