package f0.e0.j;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import okio.ByteString;
/* compiled from: Header.kt */
/* loaded from: classes3.dex */
public final class b {
    public static final ByteString a;

    /* renamed from: b  reason: collision with root package name */
    public static final ByteString f3605b;
    public static final ByteString c;
    public static final ByteString d;
    public static final ByteString e;
    public static final ByteString f;
    public final int g;
    public final ByteString h;
    public final ByteString i;

    static {
        ByteString.a aVar = ByteString.k;
        a = aVar.c(":");
        f3605b = aVar.c(":status");
        c = aVar.c(":method");
        d = aVar.c(":path");
        e = aVar.c(":scheme");
        f = aVar.c(":authority");
    }

    public b(ByteString byteString, ByteString byteString2) {
        m.checkParameterIsNotNull(byteString, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkParameterIsNotNull(byteString2, "value");
        this.h = byteString;
        this.i = byteString2;
        this.g = byteString.j() + 32 + byteString2.j();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        return m.areEqual(this.h, bVar.h) && m.areEqual(this.i, bVar.i);
    }

    public int hashCode() {
        ByteString byteString = this.h;
        int i = 0;
        int hashCode = (byteString != null ? byteString.hashCode() : 0) * 31;
        ByteString byteString2 = this.i;
        if (byteString2 != null) {
            i = byteString2.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        return this.h.q() + ": " + this.i.q();
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b(java.lang.String r2, java.lang.String r3) {
        /*
            r1 = this;
            java.lang.String r0 = "name"
            d0.z.d.m.checkParameterIsNotNull(r2, r0)
            java.lang.String r0 = "value"
            d0.z.d.m.checkParameterIsNotNull(r3, r0)
            okio.ByteString$a r0 = okio.ByteString.k
            okio.ByteString r2 = r0.c(r2)
            okio.ByteString r3 = r0.c(r3)
            r1.<init>(r2, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: f0.e0.j.b.<init>(java.lang.String, java.lang.String):void");
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public b(ByteString byteString, String str) {
        this(byteString, ByteString.k.c(str));
        m.checkParameterIsNotNull(byteString, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkParameterIsNotNull(str, "value");
    }
}
