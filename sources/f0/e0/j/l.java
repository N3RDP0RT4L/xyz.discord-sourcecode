package f0.e0.j;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import f0.e0.c;
import f0.e0.g.j;
import f0.e0.h.d;
import f0.e0.h.e;
import f0.e0.h.g;
import f0.e0.j.n;
import f0.w;
import f0.x;
import f0.y;
import g0.v;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import kotlin.TypeCastException;
import okhttp3.Headers;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.http2.ConnectionShutdownException;
import okhttp3.internal.http2.StreamResetException;
import okio.ByteString;
/* compiled from: Http2ExchangeCodec.kt */
/* loaded from: classes3.dex */
public final class l implements d {
    public static final List<String> a = c.m("connection", "host", "keep-alive", "proxy-connection", "te", "transfer-encoding", "encoding", "upgrade", ":method", ":path", ":scheme", ":authority");

    /* renamed from: b  reason: collision with root package name */
    public static final List<String> f3615b = c.m("connection", "host", "keep-alive", "proxy-connection", "te", "transfer-encoding", "encoding", "upgrade");
    public volatile n c;
    public final y d;
    public volatile boolean e;
    public final j f;
    public final g g;
    public final e h;

    public l(x xVar, j jVar, g gVar, e eVar) {
        m.checkParameterIsNotNull(xVar, "client");
        m.checkParameterIsNotNull(jVar, "connection");
        m.checkParameterIsNotNull(gVar, "chain");
        m.checkParameterIsNotNull(eVar, "http2Connection");
        this.f = jVar;
        this.g = gVar;
        this.h = eVar;
        List<y> list = xVar.E;
        y yVar = y.H2_PRIOR_KNOWLEDGE;
        this.d = !list.contains(yVar) ? y.HTTP_2 : yVar;
    }

    @Override // f0.e0.h.d
    public void a() {
        n nVar = this.c;
        if (nVar == null) {
            m.throwNpe();
        }
        ((n.a) nVar.g()).close();
    }

    @Override // f0.e0.h.d
    public void b(Request request) {
        int i;
        n nVar;
        m.checkParameterIsNotNull(request, "request");
        if (this.c == null) {
            boolean z2 = false;
            boolean z3 = request.e != null;
            m.checkParameterIsNotNull(request, "request");
            Headers headers = request.d;
            ArrayList arrayList = new ArrayList(headers.size() + 4);
            arrayList.add(new b(b.c, request.c));
            ByteString byteString = b.d;
            w wVar = request.f3784b;
            m.checkParameterIsNotNull(wVar, "url");
            String b2 = wVar.b();
            String d = wVar.d();
            if (d != null) {
                b2 = b2 + '?' + d;
            }
            arrayList.add(new b(byteString, b2));
            String b3 = request.b("Host");
            if (b3 != null) {
                arrayList.add(new b(b.f, b3));
            }
            arrayList.add(new b(b.e, request.f3784b.d));
            int size = headers.size();
            for (int i2 = 0; i2 < size; i2++) {
                String d2 = headers.d(i2);
                Locale locale = Locale.US;
                m.checkExpressionValueIsNotNull(locale, "Locale.US");
                if (d2 != null) {
                    String lowerCase = d2.toLowerCase(locale);
                    m.checkExpressionValueIsNotNull(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                    if (!a.contains(lowerCase) || (m.areEqual(lowerCase, "te") && m.areEqual(headers.g(i2), "trailers"))) {
                        arrayList.add(new b(lowerCase, headers.g(i2)));
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            }
            e eVar = this.h;
            Objects.requireNonNull(eVar);
            m.checkParameterIsNotNull(arrayList, "requestHeaders");
            boolean z4 = !z3;
            synchronized (eVar.K) {
                synchronized (eVar) {
                    if (eVar.q > 1073741823) {
                        eVar.e(a.REFUSED_STREAM);
                    }
                    if (!eVar.r) {
                        i = eVar.q;
                        eVar.q = i + 2;
                        nVar = new n(i, eVar, z4, false, null);
                        if (!z3 || eVar.H >= eVar.I || nVar.c >= nVar.d) {
                            z2 = true;
                        }
                        if (nVar.i()) {
                            eVar.n.put(Integer.valueOf(i), nVar);
                        }
                    } else {
                        throw new ConnectionShutdownException();
                    }
                }
                eVar.K.e(z4, i, arrayList);
            }
            if (z2) {
                eVar.K.flush();
            }
            this.c = nVar;
            if (this.e) {
                n nVar2 = this.c;
                if (nVar2 == null) {
                    m.throwNpe();
                }
                nVar2.e(a.CANCEL);
                throw new IOException("Canceled");
            }
            n nVar3 = this.c;
            if (nVar3 == null) {
                m.throwNpe();
            }
            TimeUnit timeUnit = TimeUnit.MILLISECONDS;
            nVar3.i.g(this.g.h, timeUnit);
            n nVar4 = this.c;
            if (nVar4 == null) {
                m.throwNpe();
            }
            nVar4.j.g(this.g.i, timeUnit);
        }
    }

    @Override // f0.e0.h.d
    public g0.x c(Response response) {
        m.checkParameterIsNotNull(response, "response");
        n nVar = this.c;
        if (nVar == null) {
            m.throwNpe();
        }
        return nVar.g;
    }

    @Override // f0.e0.h.d
    public void cancel() {
        this.e = true;
        n nVar = this.c;
        if (nVar != null) {
            nVar.e(a.CANCEL);
        }
    }

    @Override // f0.e0.h.d
    public Response.a d(boolean z2) {
        Headers headers;
        n nVar = this.c;
        if (nVar == null) {
            m.throwNpe();
        }
        synchronized (nVar) {
            nVar.i.i();
            while (nVar.e.isEmpty() && nVar.k == null) {
                nVar.l();
            }
            nVar.i.m();
            if (!nVar.e.isEmpty()) {
                Headers removeFirst = nVar.e.removeFirst();
                m.checkExpressionValueIsNotNull(removeFirst, "headersQueue.removeFirst()");
                headers = removeFirst;
            } else {
                Throwable th = nVar.l;
                if (th == null) {
                    a aVar = nVar.k;
                    if (aVar == null) {
                        m.throwNpe();
                    }
                    th = new StreamResetException(aVar);
                }
                throw th;
            }
        }
        y yVar = this.d;
        m.checkParameterIsNotNull(headers, "headerBlock");
        m.checkParameterIsNotNull(yVar, "protocol");
        ArrayList arrayList = new ArrayList(20);
        int size = headers.size();
        f0.e0.h.j jVar = null;
        for (int i = 0; i < size; i++) {
            String d = headers.d(i);
            String g = headers.g(i);
            if (m.areEqual(d, ":status")) {
                jVar = f0.e0.h.j.a("HTTP/1.1 " + g);
            } else if (!f3615b.contains(d)) {
                m.checkParameterIsNotNull(d, ModelAuditLogEntry.CHANGE_KEY_NAME);
                m.checkParameterIsNotNull(g, "value");
                arrayList.add(d);
                arrayList.add(d0.g0.w.trim(g).toString());
            }
        }
        if (jVar != null) {
            Response.a aVar2 = new Response.a();
            aVar2.f(yVar);
            aVar2.c = jVar.f3602b;
            aVar2.e(jVar.c);
            Object[] array = arrayList.toArray(new String[0]);
            if (array != null) {
                aVar2.d(new Headers((String[]) array, null));
                if (!z2 || aVar2.c != 100) {
                    return aVar2;
                }
                return null;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new ProtocolException("Expected ':status' header not present");
    }

    @Override // f0.e0.h.d
    public j e() {
        return this.f;
    }

    @Override // f0.e0.h.d
    public void f() {
        this.h.K.flush();
    }

    @Override // f0.e0.h.d
    public long g(Response response) {
        m.checkParameterIsNotNull(response, "response");
        if (!e.a(response)) {
            return 0L;
        }
        return c.l(response);
    }

    @Override // f0.e0.h.d
    public v h(Request request, long j) {
        m.checkParameterIsNotNull(request, "request");
        n nVar = this.c;
        if (nVar == null) {
            m.throwNpe();
        }
        return nVar.g();
    }
}
