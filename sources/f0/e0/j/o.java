package f0.e0.j;

import b.d.b.a.a;
import com.discord.api.permission.Permission;
import d0.z.d.m;
import f0.e0.j.c;
import g0.e;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import okio.BufferedSink;
/* compiled from: Http2Writer.kt */
/* loaded from: classes3.dex */
public final class o implements Closeable {
    public static final Logger j = Logger.getLogger(d.class.getName());
    public final e k;
    public int l = 16384;
    public boolean m;
    public final c.b n;
    public final BufferedSink o;
    public final boolean p;

    public o(BufferedSink bufferedSink, boolean z2) {
        m.checkParameterIsNotNull(bufferedSink, "sink");
        this.o = bufferedSink;
        this.p = z2;
        e eVar = new e();
        this.k = eVar;
        this.n = new c.b(0, false, eVar, 3);
    }

    public final synchronized void a(s sVar) throws IOException {
        m.checkParameterIsNotNull(sVar, "peerSettings");
        if (!this.m) {
            int i = this.l;
            int i2 = sVar.a;
            if ((i2 & 32) != 0) {
                i = sVar.f3619b[5];
            }
            this.l = i;
            int i3 = i2 & 2;
            int i4 = -1;
            if ((i3 != 0 ? sVar.f3619b[1] : -1) != -1) {
                c.b bVar = this.n;
                if (i3 != 0) {
                    i4 = sVar.f3619b[1];
                }
                bVar.h = i4;
                int min = Math.min(i4, 16384);
                int i5 = bVar.c;
                if (i5 != min) {
                    if (min < i5) {
                        bVar.a = Math.min(bVar.a, min);
                    }
                    bVar.f3608b = true;
                    bVar.c = min;
                    int i6 = bVar.g;
                    if (min < i6) {
                        if (min == 0) {
                            bVar.a();
                        } else {
                            bVar.b(i6 - min);
                        }
                    }
                }
            }
            c(0, 0, 4, 1);
            this.o.flush();
        } else {
            throw new IOException("closed");
        }
    }

    public final synchronized void b(boolean z2, int i, e eVar, int i2) throws IOException {
        if (!this.m) {
            c(i, i2, 0, z2 ? 1 : 0);
            if (i2 > 0) {
                BufferedSink bufferedSink = this.o;
                if (eVar == null) {
                    m.throwNpe();
                }
                bufferedSink.write(eVar, i2);
            }
        } else {
            throw new IOException("closed");
        }
    }

    public final void c(int i, int i2, int i3, int i4) throws IOException {
        Logger logger = j;
        if (logger.isLoggable(Level.FINE)) {
            logger.fine(d.e.b(false, i, i2, i3, i4));
        }
        boolean z2 = true;
        if (i2 <= this.l) {
            if ((((int) Permission.USE_APPLICATION_COMMANDS) & i) != 0) {
                z2 = false;
            }
            if (z2) {
                BufferedSink bufferedSink = this.o;
                byte[] bArr = f0.e0.c.a;
                m.checkParameterIsNotNull(bufferedSink, "$this$writeMedium");
                bufferedSink.writeByte((i2 >>> 16) & 255);
                bufferedSink.writeByte((i2 >>> 8) & 255);
                bufferedSink.writeByte(i2 & 255);
                this.o.writeByte(i3 & 255);
                this.o.writeByte(i4 & 255);
                this.o.writeInt(i & Integer.MAX_VALUE);
                return;
            }
            throw new IllegalArgumentException(a.p("reserved bit set: ", i).toString());
        }
        StringBuilder R = a.R("FRAME_SIZE_ERROR length > ");
        R.append(this.l);
        R.append(": ");
        R.append(i2);
        throw new IllegalArgumentException(R.toString().toString());
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public synchronized void close() throws IOException {
        this.m = true;
        this.o.close();
    }

    public final synchronized void d(int i, a aVar, byte[] bArr) throws IOException {
        m.checkParameterIsNotNull(aVar, "errorCode");
        m.checkParameterIsNotNull(bArr, "debugData");
        if (!this.m) {
            boolean z2 = false;
            if (aVar.f() != -1) {
                c(0, bArr.length + 8, 7, 0);
                this.o.writeInt(i);
                this.o.writeInt(aVar.f());
                if (bArr.length == 0) {
                    z2 = true;
                }
                if (!z2) {
                    this.o.write(bArr);
                }
                this.o.flush();
            } else {
                throw new IllegalArgumentException("errorCode.httpCode == -1".toString());
            }
        } else {
            throw new IOException("closed");
        }
    }

    public final synchronized void e(boolean z2, int i, List<b> list) throws IOException {
        m.checkParameterIsNotNull(list, "headerBlock");
        if (!this.m) {
            this.n.e(list);
            long j2 = this.k.k;
            long min = Math.min(this.l, j2);
            int i2 = (j2 > min ? 1 : (j2 == min ? 0 : -1));
            int i3 = i2 == 0 ? 4 : 0;
            if (z2) {
                i3 |= 1;
            }
            c(i, (int) min, 1, i3);
            this.o.write(this.k, min);
            if (i2 > 0) {
                s(i, j2 - min);
            }
        } else {
            throw new IOException("closed");
        }
    }

    public final synchronized void f(boolean z2, int i, int i2) throws IOException {
        if (!this.m) {
            c(0, 8, 6, z2 ? 1 : 0);
            this.o.writeInt(i);
            this.o.writeInt(i2);
            this.o.flush();
        } else {
            throw new IOException("closed");
        }
    }

    public final synchronized void flush() throws IOException {
        if (!this.m) {
            this.o.flush();
        } else {
            throw new IOException("closed");
        }
    }

    public final synchronized void n(int i, a aVar) throws IOException {
        m.checkParameterIsNotNull(aVar, "errorCode");
        if (!this.m) {
            if (aVar.f() != -1) {
                c(i, 4, 3, 0);
                this.o.writeInt(aVar.f());
                this.o.flush();
            } else {
                throw new IllegalArgumentException("Failed requirement.".toString());
            }
        } else {
            throw new IOException("closed");
        }
    }

    public final synchronized void q(int i, long j2) throws IOException {
        if (!this.m) {
            if (j2 != 0 && j2 <= 2147483647L) {
                c(i, 4, 8, 0);
                this.o.writeInt((int) j2);
                this.o.flush();
            } else {
                throw new IllegalArgumentException(("windowSizeIncrement == 0 || windowSizeIncrement > 0x7fffffffL: " + j2).toString());
            }
        } else {
            throw new IOException("closed");
        }
    }

    public final void s(int i, long j2) throws IOException {
        while (j2 > 0) {
            long min = Math.min(this.l, j2);
            j2 -= min;
            c(i, (int) min, 9, j2 == 0 ? 4 : 0);
            this.o.write(this.k, min);
        }
    }
}
