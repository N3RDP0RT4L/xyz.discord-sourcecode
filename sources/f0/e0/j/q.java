package f0.e0.j;

import d0.z.d.m;
import g0.e;
import g0.g;
import java.io.IOException;
import java.util.List;
/* compiled from: PushObserver.kt */
/* loaded from: classes3.dex */
public final class q implements r {
    @Override // f0.e0.j.r
    public boolean a(int i, List<b> list) {
        m.checkParameterIsNotNull(list, "requestHeaders");
        return true;
    }

    @Override // f0.e0.j.r
    public boolean b(int i, List<b> list, boolean z2) {
        m.checkParameterIsNotNull(list, "responseHeaders");
        return true;
    }

    @Override // f0.e0.j.r
    public void c(int i, a aVar) {
        m.checkParameterIsNotNull(aVar, "errorCode");
    }

    @Override // f0.e0.j.r
    public boolean d(int i, g gVar, int i2, boolean z2) throws IOException {
        m.checkParameterIsNotNull(gVar, "source");
        ((e) gVar).skip(i2);
        return true;
    }
}
