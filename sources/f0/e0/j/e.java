package f0.e0.j;

import d0.z.d.m;
import f0.e0.j.m;
import f0.e0.k.h;
import g0.g;
import java.io.Closeable;
import java.io.IOException;
import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import okio.BufferedSink;
import okio.ByteString;
/* compiled from: Http2Connection.kt */
/* loaded from: classes3.dex */
public final class e implements Closeable {
    public static final s j;
    public static final e k = null;
    public long A;
    public long B;
    public long C;
    public final s D;
    public s E;
    public long F;
    public long G;
    public long H;
    public long I;
    public final Socket J;
    public final o K;
    public final d L;
    public final Set<Integer> M;
    public final boolean l;
    public final c m;
    public final Map<Integer, n> n = new LinkedHashMap();
    public final String o;
    public int p;
    public int q;
    public boolean r;

    /* renamed from: s  reason: collision with root package name */
    public final f0.e0.f.d f3610s;
    public final f0.e0.f.c t;
    public final f0.e0.f.c u;
    public final f0.e0.f.c v;
    public final r w;

    /* renamed from: x  reason: collision with root package name */
    public long f3611x;

    /* renamed from: y  reason: collision with root package name */
    public long f3612y;

    /* renamed from: z  reason: collision with root package name */
    public long f3613z;

    /* compiled from: TaskQueue.kt */
    /* loaded from: classes3.dex */
    public static final class a extends f0.e0.f.a {
        public final /* synthetic */ e e;
        public final /* synthetic */ long f;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(String str, String str2, e eVar, long j) {
            super(str2, true);
            this.e = eVar;
            this.f = j;
        }

        @Override // f0.e0.f.a
        public long a() {
            e eVar;
            boolean z2;
            synchronized (this.e) {
                eVar = this.e;
                long j = eVar.f3612y;
                long j2 = eVar.f3611x;
                if (j < j2) {
                    z2 = true;
                } else {
                    eVar.f3611x = j2 + 1;
                    z2 = false;
                }
            }
            if (z2) {
                f0.e0.j.a aVar = f0.e0.j.a.PROTOCOL_ERROR;
                eVar.a(aVar, aVar, null);
                return -1L;
            }
            eVar.q(false, 1, 0);
            return this.f;
        }
    }

    /* compiled from: Http2Connection.kt */
    /* loaded from: classes3.dex */
    public static final class b {
        public Socket a;

        /* renamed from: b  reason: collision with root package name */
        public String f3614b;
        public g c;
        public BufferedSink d;
        public c e = c.a;
        public r f = r.a;
        public int g;
        public boolean h;
        public final f0.e0.f.d i;

        public b(boolean z2, f0.e0.f.d dVar) {
            m.checkParameterIsNotNull(dVar, "taskRunner");
            this.h = z2;
            this.i = dVar;
        }
    }

    /* compiled from: Http2Connection.kt */
    /* loaded from: classes3.dex */
    public static abstract class c {
        public static final c a = new a();

        /* compiled from: Http2Connection.kt */
        /* loaded from: classes3.dex */
        public static final class a extends c {
            @Override // f0.e0.j.e.c
            public void b(n nVar) throws IOException {
                m.checkParameterIsNotNull(nVar, "stream");
                nVar.c(f0.e0.j.a.REFUSED_STREAM, null);
            }
        }

        public void a(e eVar, s sVar) {
            m.checkParameterIsNotNull(eVar, "connection");
            m.checkParameterIsNotNull(sVar, "settings");
        }

        public abstract void b(n nVar) throws IOException;
    }

    /* compiled from: Http2Connection.kt */
    /* loaded from: classes3.dex */
    public final class d implements m.b, Function0<Unit> {
        public final m j;
        public final /* synthetic */ e k;

        /* compiled from: TaskQueue.kt */
        /* loaded from: classes3.dex */
        public static final class a extends f0.e0.f.a {
            public final /* synthetic */ n e;
            public final /* synthetic */ d f;
            public final /* synthetic */ List g;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public a(String str, boolean z2, String str2, boolean z3, n nVar, d dVar, n nVar2, int i, List list, boolean z4) {
                super(str2, z3);
                this.e = nVar;
                this.f = dVar;
                this.g = list;
            }

            @Override // f0.e0.f.a
            public long a() {
                try {
                    this.f.k.m.b(this.e);
                    return -1L;
                } catch (IOException e) {
                    h.a aVar = h.c;
                    h hVar = h.a;
                    StringBuilder R = b.d.b.a.a.R("Http2Connection.Listener failure for ");
                    R.append(this.f.k.o);
                    hVar.i(R.toString(), 4, e);
                    try {
                        this.e.c(f0.e0.j.a.PROTOCOL_ERROR, e);
                        return -1L;
                    } catch (IOException unused) {
                        return -1L;
                    }
                }
            }
        }

        /* compiled from: TaskQueue.kt */
        /* loaded from: classes3.dex */
        public static final class b extends f0.e0.f.a {
            public final /* synthetic */ d e;
            public final /* synthetic */ int f;
            public final /* synthetic */ int g;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public b(String str, boolean z2, String str2, boolean z3, d dVar, int i, int i2) {
                super(str2, z3);
                this.e = dVar;
                this.f = i;
                this.g = i2;
            }

            @Override // f0.e0.f.a
            public long a() {
                this.e.k.q(true, this.f, this.g);
                return -1L;
            }
        }

        /* compiled from: TaskQueue.kt */
        /* loaded from: classes3.dex */
        public static final class c extends f0.e0.f.a {
            public final /* synthetic */ d e;
            public final /* synthetic */ boolean f;
            public final /* synthetic */ s g;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public c(String str, boolean z2, String str2, boolean z3, d dVar, boolean z4, s sVar) {
                super(str2, z3);
                this.e = dVar;
                this.f = z4;
                this.g = sVar;
            }

            /* JADX WARN: Can't wrap try/catch for region: R(9:(2:15|(11:17|21|22|63|23|24|61|25|28|29|(5:(1:32)|33|(3:35|f5|41)|65|66)(1:44))(2:18|19))|63|23|24|61|25|28|29|(0)(0)) */
            /* JADX WARN: Code restructure failed: missing block: B:26:0x00db, code lost:
                r0 = move-exception;
             */
            /* JADX WARN: Code restructure failed: missing block: B:27:0x00dc, code lost:
                r2 = r13.k;
                r3 = f0.e0.j.a.PROTOCOL_ERROR;
                r2.a(r3, r3, r0);
             */
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Removed duplicated region for block: B:31:0x00ea  */
            /* JADX WARN: Removed duplicated region for block: B:44:0x010b A[ORIG_RETURN, RETURN] */
            /* JADX WARN: Type inference failed for: r10v0, types: [T, java.lang.Object, f0.e0.j.s] */
            /* JADX WARN: Type inference failed for: r3v15, types: [T, f0.e0.j.s] */
            @Override // f0.e0.f.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public long a() {
                /*
                    Method dump skipped, instructions count: 286
                    To view this dump add '--comments-level debug' option
                */
                throw new UnsupportedOperationException("Method not decompiled: f0.e0.j.e.d.c.a():long");
            }
        }

        public d(e eVar, m mVar) {
            d0.z.d.m.checkParameterIsNotNull(mVar, "reader");
            this.k = eVar;
            this.j = mVar;
        }

        @Override // f0.e0.j.m.b
        public void a() {
        }

        @Override // f0.e0.j.m.b
        public void b(boolean z2, s sVar) {
            d0.z.d.m.checkParameterIsNotNull(sVar, "settings");
            f0.e0.f.c cVar = this.k.t;
            String H = b.d.b.a.a.H(new StringBuilder(), this.k.o, " applyAndAckSettings");
            cVar.c(new c(H, true, H, true, this, z2, sVar), 0L);
        }

        @Override // f0.e0.j.m.b
        public void c(boolean z2, int i, int i2, List<f0.e0.j.b> list) {
            d0.z.d.m.checkParameterIsNotNull(list, "headerBlock");
            if (this.k.c(i)) {
                e eVar = this.k;
                Objects.requireNonNull(eVar);
                d0.z.d.m.checkParameterIsNotNull(list, "requestHeaders");
                String str = eVar.o + '[' + i + "] onHeaders";
                eVar.u.c(new h(str, true, str, true, eVar, i, list, z2), 0L);
                return;
            }
            synchronized (this.k) {
                n b2 = this.k.b(i);
                if (b2 == null) {
                    e eVar2 = this.k;
                    if (!eVar2.r) {
                        if (i > eVar2.p) {
                            if (i % 2 != eVar2.q % 2) {
                                n nVar = new n(i, this.k, false, z2, f0.e0.c.w(list));
                                e eVar3 = this.k;
                                eVar3.p = i;
                                eVar3.n.put(Integer.valueOf(i), nVar);
                                String str2 = this.k.o + '[' + i + "] onStream";
                                this.k.f3610s.f().c(new a(str2, true, str2, true, nVar, this, b2, i, list, z2), 0L);
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    return;
                }
                b2.j(f0.e0.c.w(list), z2);
            }
        }

        @Override // f0.e0.j.m.b
        public void d(int i, long j) {
            if (i == 0) {
                synchronized (this.k) {
                    e eVar = this.k;
                    eVar.I += j;
                    eVar.notifyAll();
                }
                return;
            }
            n b2 = this.k.b(i);
            if (b2 != null) {
                synchronized (b2) {
                    b2.d += j;
                    if (j > 0) {
                        b2.notifyAll();
                    }
                }
            }
        }

        /* JADX WARN: Code restructure failed: missing block: B:42:0x00f1, code lost:
            throw new kotlin.TypeCastException("null cannot be cast to non-null type java.lang.Object");
         */
        /* JADX WARN: Code restructure failed: missing block: B:54:0x0108, code lost:
            if (r18 == false) goto L70;
         */
        /* JADX WARN: Code restructure failed: missing block: B:55:0x010a, code lost:
            r3.j(f0.e0.c.f3577b, true);
         */
        /* JADX WARN: Code restructure failed: missing block: B:56:0x010f, code lost:
            return;
         */
        /* JADX WARN: Code restructure failed: missing block: B:70:?, code lost:
            return;
         */
        @Override // f0.e0.j.m.b
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public void e(boolean r18, int r19, g0.g r20, int r21) throws java.io.IOException {
            /*
                Method dump skipped, instructions count: 272
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: f0.e0.j.e.d.e(boolean, int, g0.g, int):void");
        }

        @Override // f0.e0.j.m.b
        public void f(boolean z2, int i, int i2) {
            if (z2) {
                synchronized (this.k) {
                    if (i == 1) {
                        this.k.f3612y++;
                    } else if (i == 2) {
                        this.k.A++;
                    } else if (i == 3) {
                        e eVar = this.k;
                        eVar.B++;
                        eVar.notifyAll();
                    }
                }
                return;
            }
            f0.e0.f.c cVar = this.k.t;
            String H = b.d.b.a.a.H(new StringBuilder(), this.k.o, " ping");
            cVar.c(new b(H, true, H, true, this, i, i2), 0L);
        }

        @Override // f0.e0.j.m.b
        public void g(int i, int i2, int i3, boolean z2) {
        }

        @Override // f0.e0.j.m.b
        public void h(int i, f0.e0.j.a aVar) {
            d0.z.d.m.checkParameterIsNotNull(aVar, "errorCode");
            if (this.k.c(i)) {
                e eVar = this.k;
                Objects.requireNonNull(eVar);
                d0.z.d.m.checkParameterIsNotNull(aVar, "errorCode");
                String str = eVar.o + '[' + i + "] onReset";
                eVar.u.c(new j(str, true, str, true, eVar, i, aVar), 0L);
                return;
            }
            n d = this.k.d(i);
            if (d != null) {
                d.k(aVar);
            }
        }

        @Override // f0.e0.j.m.b
        public void i(int i, int i2, List<f0.e0.j.b> list) {
            d0.z.d.m.checkParameterIsNotNull(list, "requestHeaders");
            e eVar = this.k;
            Objects.requireNonNull(eVar);
            d0.z.d.m.checkParameterIsNotNull(list, "requestHeaders");
            synchronized (eVar) {
                if (eVar.M.contains(Integer.valueOf(i2))) {
                    eVar.s(i2, f0.e0.j.a.PROTOCOL_ERROR);
                    return;
                }
                eVar.M.add(Integer.valueOf(i2));
                String str = eVar.o + '[' + i2 + "] onRequest";
                eVar.u.c(new i(str, true, str, true, eVar, i2, list), 0L);
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v0, types: [f0.e0.j.a] */
        /* JADX WARN: Type inference failed for: r0v3 */
        /* JADX WARN: Type inference failed for: r0v5, types: [kotlin.Unit] */
        @Override // kotlin.jvm.functions.Function0
        public Unit invoke() {
            Throwable th;
            f0.e0.j.a aVar;
            f0.e0.j.a aVar2 = f0.e0.j.a.INTERNAL_ERROR;
            IOException e = null;
            try {
                try {
                    this.j.b(this);
                    while (this.j.a(false, this)) {
                    }
                    f0.e0.j.a aVar3 = f0.e0.j.a.NO_ERROR;
                    try {
                        this.k.a(aVar3, f0.e0.j.a.CANCEL, null);
                        aVar = aVar3;
                    } catch (IOException e2) {
                        e = e2;
                        f0.e0.j.a aVar4 = f0.e0.j.a.PROTOCOL_ERROR;
                        e eVar = this.k;
                        eVar.a(aVar4, aVar4, e);
                        aVar = eVar;
                        f0.e0.c.d(this.j);
                        aVar2 = Unit.a;
                        return aVar2;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    this.k.a(aVar, aVar2, e);
                    f0.e0.c.d(this.j);
                    throw th;
                }
            } catch (IOException e3) {
                e = e3;
            } catch (Throwable th3) {
                th = th3;
                aVar = aVar2;
                this.k.a(aVar, aVar2, e);
                f0.e0.c.d(this.j);
                throw th;
            }
            f0.e0.c.d(this.j);
            aVar2 = Unit.a;
            return aVar2;
        }

        @Override // f0.e0.j.m.b
        public void j(int i, f0.e0.j.a aVar, ByteString byteString) {
            int i2;
            n[] nVarArr;
            d0.z.d.m.checkParameterIsNotNull(aVar, "errorCode");
            d0.z.d.m.checkParameterIsNotNull(byteString, "debugData");
            byteString.j();
            synchronized (this.k) {
                Object[] array = this.k.n.values().toArray(new n[0]);
                if (array != null) {
                    nVarArr = (n[]) array;
                    this.k.r = true;
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                }
            }
            for (n nVar : nVarArr) {
                if (nVar.m > i && nVar.h()) {
                    nVar.k(f0.e0.j.a.REFUSED_STREAM);
                    this.k.d(nVar.m);
                }
            }
        }
    }

    /* compiled from: TaskQueue.kt */
    /* renamed from: f0.e0.j.e$e  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0387e extends f0.e0.f.a {
        public final /* synthetic */ e e;
        public final /* synthetic */ int f;
        public final /* synthetic */ f0.e0.j.a g;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public C0387e(String str, boolean z2, String str2, boolean z3, e eVar, int i, f0.e0.j.a aVar) {
            super(str2, z3);
            this.e = eVar;
            this.f = i;
            this.g = aVar;
        }

        @Override // f0.e0.f.a
        public long a() {
            try {
                e eVar = this.e;
                int i = this.f;
                f0.e0.j.a aVar = this.g;
                Objects.requireNonNull(eVar);
                d0.z.d.m.checkParameterIsNotNull(aVar, "statusCode");
                eVar.K.n(i, aVar);
                return -1L;
            } catch (IOException e) {
                e eVar2 = this.e;
                f0.e0.j.a aVar2 = f0.e0.j.a.PROTOCOL_ERROR;
                eVar2.a(aVar2, aVar2, e);
                return -1L;
            }
        }
    }

    /* compiled from: TaskQueue.kt */
    /* loaded from: classes3.dex */
    public static final class f extends f0.e0.f.a {
        public final /* synthetic */ e e;
        public final /* synthetic */ int f;
        public final /* synthetic */ long g;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public f(String str, boolean z2, String str2, boolean z3, e eVar, int i, long j) {
            super(str2, z3);
            this.e = eVar;
            this.f = i;
            this.g = j;
        }

        @Override // f0.e0.f.a
        public long a() {
            try {
                this.e.K.q(this.f, this.g);
                return -1L;
            } catch (IOException e) {
                e eVar = this.e;
                f0.e0.j.a aVar = f0.e0.j.a.PROTOCOL_ERROR;
                eVar.a(aVar, aVar, e);
                return -1L;
            }
        }
    }

    static {
        s sVar = new s();
        sVar.c(7, 65535);
        sVar.c(5, 16384);
        j = sVar;
    }

    public e(b bVar) {
        d0.z.d.m.checkParameterIsNotNull(bVar, "builder");
        boolean z2 = bVar.h;
        this.l = z2;
        this.m = bVar.e;
        String str = bVar.f3614b;
        if (str == null) {
            d0.z.d.m.throwUninitializedPropertyAccessException("connectionName");
        }
        this.o = str;
        this.q = bVar.h ? 3 : 2;
        f0.e0.f.d dVar = bVar.i;
        this.f3610s = dVar;
        f0.e0.f.c f2 = dVar.f();
        this.t = f2;
        this.u = dVar.f();
        this.v = dVar.f();
        this.w = bVar.f;
        s sVar = new s();
        if (bVar.h) {
            sVar.c(7, 16777216);
        }
        this.D = sVar;
        s sVar2 = j;
        this.E = sVar2;
        this.I = sVar2.a();
        Socket socket = bVar.a;
        if (socket == null) {
            d0.z.d.m.throwUninitializedPropertyAccessException("socket");
        }
        this.J = socket;
        BufferedSink bufferedSink = bVar.d;
        if (bufferedSink == null) {
            d0.z.d.m.throwUninitializedPropertyAccessException("sink");
        }
        this.K = new o(bufferedSink, z2);
        g gVar = bVar.c;
        if (gVar == null) {
            d0.z.d.m.throwUninitializedPropertyAccessException("source");
        }
        this.L = new d(this, new m(gVar, z2));
        this.M = new LinkedHashSet();
        int i = bVar.g;
        if (i != 0) {
            long nanos = TimeUnit.MILLISECONDS.toNanos(i);
            String v = b.d.b.a.a.v(str, " ping");
            f2.c(new a(v, v, this, nanos), nanos);
        }
    }

    public final void a(f0.e0.j.a aVar, f0.e0.j.a aVar2, IOException iOException) {
        int i;
        d0.z.d.m.checkParameterIsNotNull(aVar, "connectionCode");
        d0.z.d.m.checkParameterIsNotNull(aVar2, "streamCode");
        byte[] bArr = f0.e0.c.a;
        try {
            e(aVar);
        } catch (IOException unused) {
        }
        n[] nVarArr = null;
        synchronized (this) {
            if (!this.n.isEmpty()) {
                Object[] array = this.n.values().toArray(new n[0]);
                if (array != null) {
                    nVarArr = (n[]) array;
                    this.n.clear();
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                }
            }
        }
        if (nVarArr != null) {
            for (n nVar : nVarArr) {
                try {
                    nVar.c(aVar2, iOException);
                } catch (IOException unused2) {
                }
            }
        }
        try {
            this.K.close();
        } catch (IOException unused3) {
        }
        try {
            this.J.close();
        } catch (IOException unused4) {
        }
        this.t.f();
        this.u.f();
        this.v.f();
    }

    public final synchronized n b(int i) {
        return this.n.get(Integer.valueOf(i));
    }

    public final boolean c(int i) {
        return i != 0 && (i & 1) == 0;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        a(f0.e0.j.a.NO_ERROR, f0.e0.j.a.CANCEL, null);
    }

    public final synchronized n d(int i) {
        n remove;
        remove = this.n.remove(Integer.valueOf(i));
        notifyAll();
        return remove;
    }

    public final void e(f0.e0.j.a aVar) throws IOException {
        d0.z.d.m.checkParameterIsNotNull(aVar, "statusCode");
        synchronized (this.K) {
            synchronized (this) {
                if (!this.r) {
                    this.r = true;
                    this.K.d(this.p, aVar, f0.e0.c.a);
                }
            }
        }
    }

    public final synchronized void f(long j2) {
        long j3 = this.F + j2;
        this.F = j3;
        long j4 = j3 - this.G;
        if (j4 >= this.D.a() / 2) {
            t(0, j4);
            this.G += j4;
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:16:0x0036, code lost:
        throw new java.io.IOException("stream closed");
     */
    /* JADX WARN: Code restructure failed: missing block: B:18:0x0038, code lost:
        r5 = (int) java.lang.Math.min(r13, r6 - r4);
        r3.element = r5;
        r4 = java.lang.Math.min(r5, r9.K.l);
        r3.element = r4;
        r9.H += r4;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void n(int r10, boolean r11, g0.e r12, long r13) throws java.io.IOException {
        /*
            r9 = this;
            r0 = 0
            r1 = 0
            int r3 = (r13 > r1 ? 1 : (r13 == r1 ? 0 : -1))
            if (r3 != 0) goto Ld
            f0.e0.j.o r13 = r9.K
            r13.b(r11, r10, r12, r0)
            return
        Ld:
            int r3 = (r13 > r1 ? 1 : (r13 == r1 ? 0 : -1))
            if (r3 <= 0) goto L72
            kotlin.jvm.internal.Ref$IntRef r3 = new kotlin.jvm.internal.Ref$IntRef
            r3.<init>()
            monitor-enter(r9)
        L17:
            long r4 = r9.H     // Catch: java.lang.Throwable -> L61 java.lang.InterruptedException -> L63
            long r6 = r9.I     // Catch: java.lang.Throwable -> L61 java.lang.InterruptedException -> L63
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 < 0) goto L37
            java.util.Map<java.lang.Integer, f0.e0.j.n> r4 = r9.n     // Catch: java.lang.Throwable -> L61 java.lang.InterruptedException -> L63
            java.lang.Integer r5 = java.lang.Integer.valueOf(r10)     // Catch: java.lang.Throwable -> L61 java.lang.InterruptedException -> L63
            boolean r4 = r4.containsKey(r5)     // Catch: java.lang.Throwable -> L61 java.lang.InterruptedException -> L63
            if (r4 == 0) goto L2f
            r9.wait()     // Catch: java.lang.Throwable -> L61 java.lang.InterruptedException -> L63
            goto L17
        L2f:
            java.io.IOException r10 = new java.io.IOException     // Catch: java.lang.Throwable -> L61 java.lang.InterruptedException -> L63
            java.lang.String r11 = "stream closed"
            r10.<init>(r11)     // Catch: java.lang.Throwable -> L61 java.lang.InterruptedException -> L63
            throw r10     // Catch: java.lang.Throwable -> L61 java.lang.InterruptedException -> L63
        L37:
            long r6 = r6 - r4
            long r4 = java.lang.Math.min(r13, r6)     // Catch: java.lang.Throwable -> L61
            int r5 = (int) r4     // Catch: java.lang.Throwable -> L61
            r3.element = r5     // Catch: java.lang.Throwable -> L61
            f0.e0.j.o r4 = r9.K     // Catch: java.lang.Throwable -> L61
            int r4 = r4.l     // Catch: java.lang.Throwable -> L61
            int r4 = java.lang.Math.min(r5, r4)     // Catch: java.lang.Throwable -> L61
            r3.element = r4     // Catch: java.lang.Throwable -> L61
            long r5 = r9.H     // Catch: java.lang.Throwable -> L61
            long r7 = (long) r4     // Catch: java.lang.Throwable -> L61
            long r5 = r5 + r7
            r9.H = r5     // Catch: java.lang.Throwable -> L61
            monitor-exit(r9)
            long r5 = (long) r4
            long r13 = r13 - r5
            f0.e0.j.o r3 = r9.K
            if (r11 == 0) goto L5c
            int r5 = (r13 > r1 ? 1 : (r13 == r1 ? 0 : -1))
            if (r5 != 0) goto L5c
            r5 = 1
            goto L5d
        L5c:
            r5 = 0
        L5d:
            r3.b(r5, r10, r12, r4)
            goto Ld
        L61:
            r10 = move-exception
            goto L70
        L63:
            java.lang.Thread r10 = java.lang.Thread.currentThread()     // Catch: java.lang.Throwable -> L61
            r10.interrupt()     // Catch: java.lang.Throwable -> L61
            java.io.InterruptedIOException r10 = new java.io.InterruptedIOException     // Catch: java.lang.Throwable -> L61
            r10.<init>()     // Catch: java.lang.Throwable -> L61
            throw r10     // Catch: java.lang.Throwable -> L61
        L70:
            monitor-exit(r9)
            throw r10
        L72:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: f0.e0.j.e.n(int, boolean, g0.e, long):void");
    }

    public final void q(boolean z2, int i, int i2) {
        try {
            this.K.f(z2, i, i2);
        } catch (IOException e) {
            f0.e0.j.a aVar = f0.e0.j.a.PROTOCOL_ERROR;
            a(aVar, aVar, e);
        }
    }

    public final void s(int i, f0.e0.j.a aVar) {
        d0.z.d.m.checkParameterIsNotNull(aVar, "errorCode");
        String str = this.o + '[' + i + "] writeSynReset";
        this.t.c(new C0387e(str, true, str, true, this, i, aVar), 0L);
    }

    public final void t(int i, long j2) {
        String str = this.o + '[' + i + "] windowUpdate";
        this.t.c(new f(str, true, str, true, this, i, j2), 0L);
    }
}
