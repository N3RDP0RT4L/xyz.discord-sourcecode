package f0.e0.j;

import com.discord.api.permission.Permission;
import f0.e0.j.c;
import g0.e;
import g0.g;
import g0.x;
import g0.y;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import okio.ByteString;
/* compiled from: Http2Reader.kt */
/* loaded from: classes3.dex */
public final class m implements Closeable {
    public static final Logger j;
    public static final m k = null;
    public final a l;
    public final c.a m;
    public final g n;
    public final boolean o;

    /* compiled from: Http2Reader.kt */
    /* loaded from: classes3.dex */
    public static final class a implements x {
        public int j;
        public int k;
        public int l;
        public int m;
        public int n;
        public final g o;

        public a(g gVar) {
            d0.z.d.m.checkParameterIsNotNull(gVar, "source");
            this.o = gVar;
        }

        @Override // g0.x, java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
        }

        @Override // g0.x
        public long i0(e eVar, long j) throws IOException {
            int i;
            int readInt;
            d0.z.d.m.checkParameterIsNotNull(eVar, "sink");
            do {
                int i2 = this.m;
                if (i2 == 0) {
                    this.o.skip(this.n);
                    this.n = 0;
                    if ((this.k & 4) != 0) {
                        return -1L;
                    }
                    i = this.l;
                    int t = f0.e0.c.t(this.o);
                    this.m = t;
                    this.j = t;
                    int readByte = this.o.readByte() & 255;
                    this.k = this.o.readByte() & 255;
                    m mVar = m.k;
                    Logger logger = m.j;
                    if (logger.isLoggable(Level.FINE)) {
                        logger.fine(d.e.b(true, this.l, this.j, readByte, this.k));
                    }
                    readInt = this.o.readInt() & Integer.MAX_VALUE;
                    this.l = readInt;
                    if (readByte != 9) {
                        throw new IOException(readByte + " != TYPE_CONTINUATION");
                    }
                } else {
                    long i02 = this.o.i0(eVar, Math.min(j, i2));
                    if (i02 == -1) {
                        return -1L;
                    }
                    this.m -= (int) i02;
                    return i02;
                }
            } while (readInt == i);
            throw new IOException("TYPE_CONTINUATION streamId changed");
        }

        @Override // g0.x
        public y timeout() {
            return this.o.timeout();
        }
    }

    /* compiled from: Http2Reader.kt */
    /* loaded from: classes3.dex */
    public interface b {
        void a();

        void b(boolean z2, s sVar);

        void c(boolean z2, int i, int i2, List<f0.e0.j.b> list);

        void d(int i, long j);

        void e(boolean z2, int i, g gVar, int i2) throws IOException;

        void f(boolean z2, int i, int i2);

        void g(int i, int i2, int i3, boolean z2);

        void h(int i, f0.e0.j.a aVar);

        void i(int i, int i2, List<f0.e0.j.b> list) throws IOException;

        void j(int i, f0.e0.j.a aVar, ByteString byteString);
    }

    static {
        Logger logger = Logger.getLogger(d.class.getName());
        d0.z.d.m.checkExpressionValueIsNotNull(logger, "Logger.getLogger(Http2::class.java.name)");
        j = logger;
    }

    public m(g gVar, boolean z2) {
        d0.z.d.m.checkParameterIsNotNull(gVar, "source");
        this.n = gVar;
        this.o = z2;
        a aVar = new a(gVar);
        this.l = aVar;
        this.m = new c.a(aVar, 4096, 0, 4);
    }

    /* JADX WARN: Code restructure failed: missing block: B:93:0x01e0, code lost:
        throw new java.io.IOException(b.d.b.a.a.p("PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: ", r11));
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final boolean a(boolean r17, f0.e0.j.m.b r18) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 826
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: f0.e0.j.m.a(boolean, f0.e0.j.m$b):boolean");
    }

    public final void b(b bVar) throws IOException {
        d0.z.d.m.checkParameterIsNotNull(bVar, "handler");
        if (!this.o) {
            g gVar = this.n;
            ByteString byteString = d.a;
            ByteString o = gVar.o(byteString.j());
            Logger logger = j;
            if (logger.isLoggable(Level.FINE)) {
                StringBuilder R = b.d.b.a.a.R("<< CONNECTION ");
                R.append(o.k());
                logger.fine(f0.e0.c.j(R.toString(), new Object[0]));
            }
            if (!d0.z.d.m.areEqual(byteString, o)) {
                StringBuilder R2 = b.d.b.a.a.R("Expected a connection header but was ");
                R2.append(o.q());
                throw new IOException(R2.toString());
            }
        } else if (!a(true, bVar)) {
            throw new IOException("Required SETTINGS preface not received");
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:58:0x004c A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:61:0x0040 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.util.List<f0.e0.j.b> c(int r2, int r3, int r4, int r5) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 326
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: f0.e0.j.m.c(int, int, int, int):java.util.List");
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.n.close();
    }

    public final void d(b bVar, int i) throws IOException {
        int readInt = this.n.readInt();
        boolean z2 = (readInt & ((int) Permission.USE_APPLICATION_COMMANDS)) != 0;
        byte readByte = this.n.readByte();
        byte[] bArr = f0.e0.c.a;
        bVar.g(i, readInt & Integer.MAX_VALUE, (readByte & 255) + 1, z2);
    }
}
