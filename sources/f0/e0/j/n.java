package f0.e0.j;

import com.discord.api.permission.Permission;
import d0.z.d.m;
import g0.e;
import g0.v;
import g0.x;
import g0.y;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.SocketTimeoutException;
import java.util.ArrayDeque;
import java.util.Objects;
import kotlin.TypeCastException;
import okhttp3.Headers;
import okhttp3.internal.http2.StreamResetException;
/* compiled from: Http2Stream.kt */
/* loaded from: classes3.dex */
public final class n {
    public long a;

    /* renamed from: b  reason: collision with root package name */
    public long f3616b;
    public long c;
    public long d;
    public final ArrayDeque<Headers> e;
    public boolean f;
    public final b g;
    public final a h;
    public final c i = new c();
    public final c j = new c();
    public f0.e0.j.a k;
    public IOException l;
    public final int m;
    public final e n;

    /* compiled from: Http2Stream.kt */
    /* loaded from: classes3.dex */
    public final class a implements v {
        public final e j = new e();
        public boolean k;
        public boolean l;

        public a(boolean z2) {
            this.l = z2;
        }

        public final void a(boolean z2) throws IOException {
            long min;
            boolean z3;
            synchronized (n.this) {
                n.this.j.i();
                while (true) {
                    n nVar = n.this;
                    if (nVar.c < nVar.d || this.l || this.k || nVar.f() != null) {
                        break;
                    }
                    n.this.l();
                }
                n.this.j.m();
                n.this.b();
                n nVar2 = n.this;
                min = Math.min(nVar2.d - nVar2.c, this.j.k);
                n nVar3 = n.this;
                nVar3.c += min;
                if (z2 && min == this.j.k) {
                    if (nVar3.f() == null) {
                        z3 = true;
                    }
                }
                z3 = false;
            }
            n.this.j.i();
            try {
                n nVar4 = n.this;
                nVar4.n.n(nVar4.m, z3, this.j, min);
            } finally {
                n.this.j.m();
            }
        }

        @Override // g0.v, java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
            n nVar = n.this;
            byte[] bArr = f0.e0.c.a;
            synchronized (nVar) {
                if (!this.k) {
                    boolean z2 = false;
                    boolean z3 = n.this.f() == null;
                    n nVar2 = n.this;
                    if (!nVar2.h.l) {
                        if (this.j.k > 0) {
                            z2 = true;
                        }
                        if (z2) {
                            while (this.j.k > 0) {
                                a(true);
                            }
                        } else if (z3) {
                            nVar2.n.n(nVar2.m, true, null, 0L);
                        }
                    }
                    synchronized (n.this) {
                        this.k = true;
                    }
                    n.this.n.K.flush();
                    n.this.a();
                }
            }
        }

        @Override // g0.v, java.io.Flushable
        public void flush() throws IOException {
            n nVar = n.this;
            byte[] bArr = f0.e0.c.a;
            synchronized (nVar) {
                n.this.b();
            }
            while (this.j.k > 0) {
                a(false);
                n.this.n.K.flush();
            }
        }

        @Override // g0.v
        public y timeout() {
            return n.this.j;
        }

        @Override // g0.v
        public void write(e eVar, long j) throws IOException {
            m.checkParameterIsNotNull(eVar, "source");
            byte[] bArr = f0.e0.c.a;
            this.j.write(eVar, j);
            while (this.j.k >= Permission.EMBED_LINKS) {
                a(false);
            }
        }
    }

    /* compiled from: Http2Stream.kt */
    /* loaded from: classes3.dex */
    public final class b implements x {
        public final e j = new e();
        public final e k = new e();
        public boolean l;
        public final long m;
        public boolean n;

        public b(long j, boolean z2) {
            this.m = j;
            this.n = z2;
        }

        public final void a(long j) {
            n nVar = n.this;
            byte[] bArr = f0.e0.c.a;
            nVar.n.f(j);
        }

        @Override // g0.x, java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
            long j;
            synchronized (n.this) {
                this.l = true;
                e eVar = this.k;
                j = eVar.k;
                eVar.skip(j);
                n nVar = n.this;
                if (nVar != null) {
                    nVar.notifyAll();
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.Object");
                }
            }
            if (j > 0) {
                a(j);
            }
            n.this.a();
        }

        /* JADX WARN: Code restructure failed: missing block: B:47:0x00ae, code lost:
            throw new java.io.IOException("stream closed");
         */
        @Override // g0.x
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public long i0(g0.e r12, long r13) throws java.io.IOException {
            /*
                Method dump skipped, instructions count: 203
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: f0.e0.j.n.b.i0(g0.e, long):long");
        }

        @Override // g0.x
        public y timeout() {
            return n.this.i;
        }
    }

    /* compiled from: Http2Stream.kt */
    /* loaded from: classes3.dex */
    public final class c extends g0.b {
        public c() {
        }

        @Override // g0.b
        public IOException k(IOException iOException) {
            SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
            if (iOException != null) {
                socketTimeoutException.initCause(iOException);
            }
            return socketTimeoutException;
        }

        @Override // g0.b
        public void l() {
            n.this.e(f0.e0.j.a.CANCEL);
            e eVar = n.this.n;
            synchronized (eVar) {
                long j = eVar.A;
                long j2 = eVar.f3613z;
                if (j >= j2) {
                    eVar.f3613z = j2 + 1;
                    eVar.C = System.nanoTime() + 1000000000;
                    f0.e0.f.c cVar = eVar.t;
                    String H = b.d.b.a.a.H(new StringBuilder(), eVar.o, " ping");
                    cVar.c(new k(H, true, H, true, eVar), 0L);
                }
            }
        }

        public final void m() throws IOException {
            if (j()) {
                throw new SocketTimeoutException("timeout");
            }
        }
    }

    public n(int i, e eVar, boolean z2, boolean z3, Headers headers) {
        m.checkParameterIsNotNull(eVar, "connection");
        this.m = i;
        this.n = eVar;
        this.d = eVar.E.a();
        ArrayDeque<Headers> arrayDeque = new ArrayDeque<>();
        this.e = arrayDeque;
        this.g = new b(eVar.D.a(), z3);
        this.h = new a(z2);
        if (headers != null) {
            if (!h()) {
                arrayDeque.add(headers);
                return;
            }
            throw new IllegalStateException("locally-initiated streams shouldn't have headers yet".toString());
        } else if (!h()) {
            throw new IllegalStateException("remotely-initiated streams should have headers".toString());
        }
    }

    public final void a() throws IOException {
        boolean z2;
        boolean i;
        byte[] bArr = f0.e0.c.a;
        synchronized (this) {
            b bVar = this.g;
            if (!bVar.n && bVar.l) {
                a aVar = this.h;
                if (aVar.l || aVar.k) {
                    z2 = true;
                    i = i();
                }
            }
            z2 = false;
            i = i();
        }
        if (z2) {
            c(f0.e0.j.a.CANCEL, null);
        } else if (!i) {
            this.n.d(this.m);
        }
    }

    public final void b() throws IOException {
        a aVar = this.h;
        if (aVar.k) {
            throw new IOException("stream closed");
        } else if (aVar.l) {
            throw new IOException("stream finished");
        } else if (this.k != null) {
            Throwable th = this.l;
            if (th == null) {
                f0.e0.j.a aVar2 = this.k;
                if (aVar2 == null) {
                    m.throwNpe();
                }
                th = new StreamResetException(aVar2);
            }
            throw th;
        }
    }

    public final void c(f0.e0.j.a aVar, IOException iOException) throws IOException {
        m.checkParameterIsNotNull(aVar, "rstStatusCode");
        if (d(aVar, iOException)) {
            e eVar = this.n;
            int i = this.m;
            Objects.requireNonNull(eVar);
            m.checkParameterIsNotNull(aVar, "statusCode");
            eVar.K.n(i, aVar);
        }
    }

    public final boolean d(f0.e0.j.a aVar, IOException iOException) {
        byte[] bArr = f0.e0.c.a;
        synchronized (this) {
            if (this.k != null) {
                return false;
            }
            if (this.g.n && this.h.l) {
                return false;
            }
            this.k = aVar;
            this.l = iOException;
            notifyAll();
            this.n.d(this.m);
            return true;
        }
    }

    public final void e(f0.e0.j.a aVar) {
        m.checkParameterIsNotNull(aVar, "errorCode");
        if (d(aVar, null)) {
            this.n.s(this.m, aVar);
        }
    }

    public final synchronized f0.e0.j.a f() {
        return this.k;
    }

    public final v g() {
        synchronized (this) {
            if (!(this.f || h())) {
                throw new IllegalStateException("reply before requesting the sink".toString());
            }
        }
        return this.h;
    }

    public final boolean h() {
        return this.n.l == ((this.m & 1) == 1);
    }

    public final synchronized boolean i() {
        if (this.k != null) {
            return false;
        }
        b bVar = this.g;
        if (bVar.n || bVar.l) {
            a aVar = this.h;
            if (aVar.l || aVar.k) {
                if (this.f) {
                    return false;
                }
            }
        }
        return true;
    }

    /* JADX WARN: Removed duplicated region for block: B:11:0x001f A[Catch: all -> 0x0035, TryCatch #0 {, blocks: (B:4:0x0008, B:8:0x0010, B:9:0x0016, B:11:0x001f, B:12:0x0023), top: B:19:0x0008 }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void j(okhttp3.Headers r3, boolean r4) {
        /*
            r2 = this;
            java.lang.String r0 = "headers"
            d0.z.d.m.checkParameterIsNotNull(r3, r0)
            byte[] r0 = f0.e0.c.a
            monitor-enter(r2)
            boolean r0 = r2.f     // Catch: java.lang.Throwable -> L35
            r1 = 1
            if (r0 == 0) goto L16
            if (r4 != 0) goto L10
            goto L16
        L10:
            f0.e0.j.n$b r3 = r2.g     // Catch: java.lang.Throwable -> L35
            java.util.Objects.requireNonNull(r3)     // Catch: java.lang.Throwable -> L35
            goto L1d
        L16:
            r2.f = r1     // Catch: java.lang.Throwable -> L35
            java.util.ArrayDeque<okhttp3.Headers> r0 = r2.e     // Catch: java.lang.Throwable -> L35
            r0.add(r3)     // Catch: java.lang.Throwable -> L35
        L1d:
            if (r4 == 0) goto L23
            f0.e0.j.n$b r3 = r2.g     // Catch: java.lang.Throwable -> L35
            r3.n = r1     // Catch: java.lang.Throwable -> L35
        L23:
            boolean r3 = r2.i()     // Catch: java.lang.Throwable -> L35
            r2.notifyAll()     // Catch: java.lang.Throwable -> L35
            monitor-exit(r2)
            if (r3 != 0) goto L34
            f0.e0.j.e r3 = r2.n
            int r4 = r2.m
            r3.d(r4)
        L34:
            return
        L35:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: f0.e0.j.n.j(okhttp3.Headers, boolean):void");
    }

    public final synchronized void k(f0.e0.j.a aVar) {
        m.checkParameterIsNotNull(aVar, "errorCode");
        if (this.k == null) {
            this.k = aVar;
            notifyAll();
        }
    }

    public final void l() throws InterruptedIOException {
        try {
            wait();
        } catch (InterruptedException unused) {
            Thread.currentThread().interrupt();
            throw new InterruptedIOException();
        }
    }
}
