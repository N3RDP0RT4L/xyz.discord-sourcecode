package f0.e0.j;

import g0.g;
import java.io.IOException;
import java.util.List;
/* compiled from: PushObserver.kt */
/* loaded from: classes3.dex */
public interface r {
    public static final r a = new q();

    boolean a(int i, List<b> list);

    boolean b(int i, List<b> list, boolean z2);

    void c(int i, a aVar);

    boolean d(int i, g gVar, int i2, boolean z2) throws IOException;
}
