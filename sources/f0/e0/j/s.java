package f0.e0.j;

import d0.z.d.m;
/* compiled from: Settings.kt */
/* loaded from: classes3.dex */
public final class s {
    public int a;

    /* renamed from: b  reason: collision with root package name */
    public final int[] f3619b = new int[10];

    public final int a() {
        if ((this.a & 128) != 0) {
            return this.f3619b[7];
        }
        return 65535;
    }

    public final void b(s sVar) {
        m.checkParameterIsNotNull(sVar, "other");
        for (int i = 0; i < 10; i++) {
            boolean z2 = true;
            if (((1 << i) & sVar.a) == 0) {
                z2 = false;
            }
            if (z2) {
                c(i, sVar.f3619b[i]);
            }
        }
    }

    public final s c(int i, int i2) {
        if (i >= 0) {
            int[] iArr = this.f3619b;
            if (i < iArr.length) {
                this.a = (1 << i) | this.a;
                iArr[i] = i2;
            }
        }
        return this;
    }
}
