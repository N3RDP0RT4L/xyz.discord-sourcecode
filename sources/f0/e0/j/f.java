package f0.e0.j;

import f0.e0.f.a;
import f0.e0.j.e;
import kotlin.jvm.internal.Ref$LongRef;
import kotlin.jvm.internal.Ref$ObjectRef;
/* compiled from: TaskQueue.kt */
/* loaded from: classes3.dex */
public final class f extends a {
    public final /* synthetic */ e.d e;
    public final /* synthetic */ Ref$ObjectRef f;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public f(String str, boolean z2, String str2, boolean z3, e.d dVar, boolean z4, Ref$ObjectRef ref$ObjectRef, s sVar, Ref$LongRef ref$LongRef, Ref$ObjectRef ref$ObjectRef2) {
        super(str2, z3);
        this.e = dVar;
        this.f = ref$ObjectRef;
    }

    @Override // f0.e0.f.a
    public long a() {
        e eVar = this.e.k;
        eVar.m.a(eVar, (s) this.f.element);
        return -1L;
    }
}
