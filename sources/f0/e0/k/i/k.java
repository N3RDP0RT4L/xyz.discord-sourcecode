package f0.e0.k.i;

import f0.y;
import java.util.List;
import javax.net.ssl.SSLSocket;
/* compiled from: SocketAdapter.kt */
/* loaded from: classes3.dex */
public interface k {
    boolean a(SSLSocket sSLSocket);

    boolean b();

    String c(SSLSocket sSLSocket);

    void d(SSLSocket sSLSocket, String str, List<? extends y> list);
}
