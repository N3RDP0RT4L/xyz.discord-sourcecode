package f0.e0.k.i;

import d0.z.d.m;
import f0.e0.k.c;
import f0.e0.k.h;
import f0.e0.k.i.j;
import f0.y;
import java.util.List;
import javax.net.ssl.SSLSocket;
import kotlin.TypeCastException;
import org.bouncycastle.jsse.BCSSLParameters;
import org.bouncycastle.jsse.BCSSLSocket;
/* compiled from: BouncyCastleSocketAdapter.kt */
/* loaded from: classes3.dex */
public final class g implements k {
    public static final j.a a = new a();

    /* compiled from: BouncyCastleSocketAdapter.kt */
    /* loaded from: classes3.dex */
    public static final class a implements j.a {
        @Override // f0.e0.k.i.j.a
        public boolean a(SSLSocket sSLSocket) {
            m.checkParameterIsNotNull(sSLSocket, "sslSocket");
            c.a aVar = c.e;
            return c.d && (sSLSocket instanceof BCSSLSocket);
        }

        @Override // f0.e0.k.i.j.a
        public k b(SSLSocket sSLSocket) {
            m.checkParameterIsNotNull(sSLSocket, "sslSocket");
            return new g();
        }
    }

    @Override // f0.e0.k.i.k
    public boolean a(SSLSocket sSLSocket) {
        m.checkParameterIsNotNull(sSLSocket, "sslSocket");
        return sSLSocket instanceof BCSSLSocket;
    }

    @Override // f0.e0.k.i.k
    public boolean b() {
        c.a aVar = c.e;
        return c.d;
    }

    @Override // f0.e0.k.i.k
    public String c(SSLSocket sSLSocket) {
        m.checkParameterIsNotNull(sSLSocket, "sslSocket");
        String applicationProtocol = ((BCSSLSocket) sSLSocket).getApplicationProtocol();
        if (applicationProtocol != null && !m.areEqual(applicationProtocol, "")) {
            return applicationProtocol;
        }
        return null;
    }

    @Override // f0.e0.k.i.k
    public void d(SSLSocket sSLSocket, String str, List<? extends y> list) {
        m.checkParameterIsNotNull(sSLSocket, "sslSocket");
        m.checkParameterIsNotNull(list, "protocols");
        if (a(sSLSocket)) {
            BCSSLSocket bCSSLSocket = (BCSSLSocket) sSLSocket;
            BCSSLParameters parameters = bCSSLSocket.getParameters();
            m.checkExpressionValueIsNotNull(parameters, "sslParameters");
            Object[] array = h.c.a(list).toArray(new String[0]);
            if (array != null) {
                parameters.setApplicationProtocols((String[]) array);
                bCSSLSocket.setParameters(parameters);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }
}
