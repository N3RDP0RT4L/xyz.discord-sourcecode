package f0.e0.k.i;

import andhook.lib.xposed.ClassUtils;
import b.d.b.a.a;
import d0.g0.t;
import d0.z.d.m;
import f0.e0.k.i.f;
import f0.e0.k.i.j;
import javax.net.ssl.SSLSocket;
/* compiled from: AndroidSocketAdapter.kt */
/* loaded from: classes3.dex */
public final class e implements j.a {
    public final /* synthetic */ String a;

    public e(String str) {
        this.a = str;
    }

    @Override // f0.e0.k.i.j.a
    public boolean a(SSLSocket sSLSocket) {
        m.checkParameterIsNotNull(sSLSocket, "sslSocket");
        String name = sSLSocket.getClass().getName();
        m.checkExpressionValueIsNotNull(name, "sslSocket.javaClass.name");
        return t.startsWith$default(name, a.G(new StringBuilder(), this.a, ClassUtils.PACKAGE_SEPARATOR_CHAR), false, 2, null);
    }

    @Override // f0.e0.k.i.j.a
    public k b(SSLSocket sSLSocket) {
        m.checkParameterIsNotNull(sSLSocket, "sslSocket");
        f.a aVar = f.f3625b;
        Class<?> cls = sSLSocket.getClass();
        Class<?> cls2 = cls;
        while (cls2 != null && (!m.areEqual(cls2.getSimpleName(), "OpenSSLSocketImpl"))) {
            cls2 = cls2.getSuperclass();
            if (cls2 == null) {
                throw new AssertionError("No OpenSSLSocketImpl superclass of socket of type " + cls);
            }
        }
        if (cls2 == null) {
            m.throwNpe();
        }
        return new f(cls2);
    }
}
