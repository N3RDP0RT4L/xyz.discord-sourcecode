package f0.e0.k.i;

import android.util.Log;
import b.d.b.a.a;
import com.discord.utilities.rest.SendUtils;
import d0.g0.w;
import d0.g0.y;
import d0.z.d.m;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
/* compiled from: AndroidLog.kt */
/* loaded from: classes3.dex */
public final class d extends Handler {
    public static final d a = new d();

    @Override // java.util.logging.Handler
    public void close() {
    }

    @Override // java.util.logging.Handler
    public void flush() {
    }

    @Override // java.util.logging.Handler
    public void publish(LogRecord logRecord) {
        int i;
        int min;
        m.checkParameterIsNotNull(logRecord, "record");
        c cVar = c.c;
        String loggerName = logRecord.getLoggerName();
        m.checkExpressionValueIsNotNull(loggerName, "record.loggerName");
        if (logRecord.getLevel().intValue() > Level.INFO.intValue()) {
            i = 5;
        } else {
            i = logRecord.getLevel().intValue() == Level.INFO.intValue() ? 4 : 3;
        }
        String message = logRecord.getMessage();
        m.checkExpressionValueIsNotNull(message, "record.message");
        Throwable thrown = logRecord.getThrown();
        m.checkParameterIsNotNull(loggerName, "loggerName");
        m.checkParameterIsNotNull(message, "message");
        String str = c.f3624b.get(loggerName);
        if (str == null) {
            str = y.take(loggerName, 23);
        }
        if (Log.isLoggable(str, i)) {
            if (thrown != null) {
                StringBuilder V = a.V(message, "\n");
                V.append(Log.getStackTraceString(thrown));
                message = V.toString();
            }
            int i2 = 0;
            int length = message.length();
            while (i2 < length) {
                int indexOf$default = w.indexOf$default((CharSequence) message, '\n', i2, false, 4, (Object) null);
                if (indexOf$default == -1) {
                    indexOf$default = length;
                }
                while (true) {
                    min = Math.min(indexOf$default, i2 + SendUtils.MAX_MESSAGE_CHARACTER_COUNT_PREMIUM);
                    String substring = message.substring(i2, min);
                    m.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                    Log.println(i, str, substring);
                    if (min >= indexOf$default) {
                        break;
                    }
                    i2 = min;
                }
                i2 = min + 1;
            }
        }
    }
}
