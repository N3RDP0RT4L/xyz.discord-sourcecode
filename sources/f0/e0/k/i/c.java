package f0.e0.k.i;

import d0.t.h0;
import d0.z.d.m;
import f0.e0.j.d;
import f0.x;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.logging.Logger;
/* compiled from: AndroidLog.kt */
/* loaded from: classes3.dex */
public final class c {
    public static final CopyOnWriteArraySet<Logger> a = new CopyOnWriteArraySet<>();

    /* renamed from: b  reason: collision with root package name */
    public static final Map<String, String> f3624b;
    public static final c c = null;

    static {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        Package r1 = x.class.getPackage();
        String name = r1 != null ? r1.getName() : null;
        if (name != null) {
            linkedHashMap.put(name, "OkHttp");
        }
        String name2 = x.class.getName();
        m.checkExpressionValueIsNotNull(name2, "OkHttpClient::class.java.name");
        linkedHashMap.put(name2, "okhttp.OkHttpClient");
        String name3 = d.class.getName();
        m.checkExpressionValueIsNotNull(name3, "Http2::class.java.name");
        linkedHashMap.put(name3, "okhttp.Http2");
        String name4 = f0.e0.f.d.class.getName();
        m.checkExpressionValueIsNotNull(name4, "TaskRunner::class.java.name");
        linkedHashMap.put(name4, "okhttp.TaskRunner");
        f3624b = h0.toMap(linkedHashMap);
    }
}
