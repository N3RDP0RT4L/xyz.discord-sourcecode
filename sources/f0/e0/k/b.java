package f0.e0.k;

import android.net.http.X509TrustManagerExtensions;
import android.os.Build;
import android.security.NetworkSecurityPolicy;
import d0.t.n;
import d0.z.d.m;
import f0.e0.k.i.f;
import f0.e0.k.i.g;
import f0.e0.k.i.h;
import f0.e0.k.i.i;
import f0.e0.k.i.j;
import f0.e0.k.i.k;
import f0.e0.k.i.l;
import f0.e0.m.c;
import f0.e0.m.e;
import f0.y;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.X509TrustManager;
import kotlin.TypeCastException;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: AndroidPlatform.kt */
/* loaded from: classes3.dex */
public final class b extends h {
    public static final boolean d;
    public static final a e = new a(null);
    public final List<k> f;
    public final h g;

    /* compiled from: AndroidPlatform.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: AndroidPlatform.kt */
    /* renamed from: f0.e0.k.b$b  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0388b implements e {
        public final X509TrustManager a;

        /* renamed from: b  reason: collision with root package name */
        public final Method f3620b;

        public C0388b(X509TrustManager x509TrustManager, Method method) {
            m.checkParameterIsNotNull(x509TrustManager, "trustManager");
            m.checkParameterIsNotNull(method, "findByIssuerAndSignatureMethod");
            this.a = x509TrustManager;
            this.f3620b = method;
        }

        @Override // f0.e0.m.e
        public X509Certificate a(X509Certificate x509Certificate) {
            m.checkParameterIsNotNull(x509Certificate, "cert");
            try {
                Object invoke = this.f3620b.invoke(this.a, x509Certificate);
                if (invoke != null) {
                    return ((TrustAnchor) invoke).getTrustedCert();
                }
                throw new TypeCastException("null cannot be cast to non-null type java.security.cert.TrustAnchor");
            } catch (IllegalAccessException e) {
                throw new AssertionError("unable to get issues and signature", e);
            } catch (InvocationTargetException unused) {
                return null;
            }
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof C0388b)) {
                return false;
            }
            C0388b bVar = (C0388b) obj;
            return m.areEqual(this.a, bVar.a) && m.areEqual(this.f3620b, bVar.f3620b);
        }

        public int hashCode() {
            X509TrustManager x509TrustManager = this.a;
            int i = 0;
            int hashCode = (x509TrustManager != null ? x509TrustManager.hashCode() : 0) * 31;
            Method method = this.f3620b;
            if (method != null) {
                i = method.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("CustomTrustRootIndex(trustManager=");
            R.append(this.a);
            R.append(", findByIssuerAndSignatureMethod=");
            R.append(this.f3620b);
            R.append(")");
            return R.toString();
        }
    }

    static {
        boolean z2 = false;
        if (h.c.c() && Build.VERSION.SDK_INT < 30) {
            z2 = true;
        }
        d = z2;
    }

    public b() {
        l lVar;
        Method method;
        Method method2;
        k[] kVarArr = new k[4];
        l.a aVar = l.h;
        m.checkParameterIsNotNull("com.android.org.conscrypt", "packageName");
        Method method3 = null;
        try {
            Class<?> cls = Class.forName("com.android.org.conscrypt.OpenSSLSocketImpl");
            Class<?> cls2 = Class.forName("com.android.org.conscrypt.OpenSSLSocketFactoryImpl");
            Class<?> cls3 = Class.forName("com.android.org.conscrypt.SSLParametersImpl");
            m.checkExpressionValueIsNotNull(cls3, "paramsClass");
            lVar = new l(cls, cls2, cls3);
        } catch (Exception e2) {
            h.a.i("unable to load android socket classes", 5, e2);
            lVar = null;
        }
        kVarArr[0] = lVar;
        f.a aVar2 = f.f3625b;
        kVarArr[1] = new j(f.a);
        kVarArr[2] = new j(i.a);
        kVarArr[3] = new j(g.a);
        List listOfNotNull = n.listOfNotNull((Object[]) kVarArr);
        ArrayList arrayList = new ArrayList();
        for (Object obj : listOfNotNull) {
            if (((k) obj).b()) {
                arrayList.add(obj);
            }
        }
        this.f = arrayList;
        try {
            Class<?> cls4 = Class.forName("dalvik.system.CloseGuard");
            Method method4 = cls4.getMethod("get", new Class[0]);
            method = cls4.getMethod("open", String.class);
            method2 = cls4.getMethod("warnIfOpen", new Class[0]);
            method3 = method4;
        } catch (Exception unused) {
            method2 = null;
            method = null;
        }
        this.g = new h(method3, method, method2);
    }

    @Override // f0.e0.k.h
    public c b(X509TrustManager x509TrustManager) {
        X509TrustManagerExtensions x509TrustManagerExtensions;
        m.checkParameterIsNotNull(x509TrustManager, "trustManager");
        m.checkParameterIsNotNull(x509TrustManager, "trustManager");
        f0.e0.k.i.b bVar = null;
        try {
            x509TrustManagerExtensions = new X509TrustManagerExtensions(x509TrustManager);
        } catch (IllegalArgumentException unused) {
            x509TrustManagerExtensions = null;
        }
        if (x509TrustManagerExtensions != null) {
            bVar = new f0.e0.k.i.b(x509TrustManager, x509TrustManagerExtensions);
        }
        return bVar != null ? bVar : super.b(x509TrustManager);
    }

    @Override // f0.e0.k.h
    public e c(X509TrustManager x509TrustManager) {
        m.checkParameterIsNotNull(x509TrustManager, "trustManager");
        try {
            Method declaredMethod = x509TrustManager.getClass().getDeclaredMethod("findTrustAnchorByIssuerAndSignature", X509Certificate.class);
            m.checkExpressionValueIsNotNull(declaredMethod, "method");
            declaredMethod.setAccessible(true);
            return new C0388b(x509TrustManager, declaredMethod);
        } catch (NoSuchMethodException unused) {
            return super.c(x509TrustManager);
        }
    }

    @Override // f0.e0.k.h
    public void d(SSLSocket sSLSocket, String str, List<y> list) {
        Object obj;
        m.checkParameterIsNotNull(sSLSocket, "sslSocket");
        m.checkParameterIsNotNull(list, "protocols");
        Iterator<T> it = this.f.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (((k) obj).a(sSLSocket)) {
                break;
            }
        }
        k kVar = (k) obj;
        if (kVar != null) {
            kVar.d(sSLSocket, str, list);
        }
    }

    @Override // f0.e0.k.h
    public void e(Socket socket, InetSocketAddress inetSocketAddress, int i) throws IOException {
        m.checkParameterIsNotNull(socket, "socket");
        m.checkParameterIsNotNull(inetSocketAddress, "address");
        try {
            socket.connect(inetSocketAddress, i);
        } catch (ClassCastException e2) {
            if (Build.VERSION.SDK_INT == 26) {
                throw new IOException("Exception in connect", e2);
            }
            throw e2;
        }
    }

    @Override // f0.e0.k.h
    public String f(SSLSocket sSLSocket) {
        Object obj;
        m.checkParameterIsNotNull(sSLSocket, "sslSocket");
        Iterator<T> it = this.f.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (((k) obj).a(sSLSocket)) {
                break;
            }
        }
        k kVar = (k) obj;
        if (kVar != null) {
            return kVar.c(sSLSocket);
        }
        return null;
    }

    @Override // f0.e0.k.h
    public Object g(String str) {
        m.checkParameterIsNotNull(str, "closer");
        h hVar = this.g;
        Objects.requireNonNull(hVar);
        m.checkParameterIsNotNull(str, "closer");
        Method method = hVar.a;
        if (method == null) {
            return null;
        }
        try {
            Object invoke = method.invoke(null, new Object[0]);
            Method method2 = hVar.f3626b;
            if (method2 == null) {
                m.throwNpe();
            }
            method2.invoke(invoke, str);
            return invoke;
        } catch (Exception unused) {
            return null;
        }
    }

    @Override // f0.e0.k.h
    public boolean h(String str) {
        m.checkParameterIsNotNull(str, "hostname");
        int i = Build.VERSION.SDK_INT;
        if (i >= 24) {
            return NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted(str);
        }
        if (i < 23) {
            return true;
        }
        NetworkSecurityPolicy networkSecurityPolicy = NetworkSecurityPolicy.getInstance();
        m.checkExpressionValueIsNotNull(networkSecurityPolicy, "NetworkSecurityPolicy.getInstance()");
        return networkSecurityPolicy.isCleartextTrafficPermitted();
    }

    @Override // f0.e0.k.h
    public void k(String str, Object obj) {
        m.checkParameterIsNotNull(str, "message");
        h hVar = this.g;
        Objects.requireNonNull(hVar);
        boolean z2 = false;
        if (obj != null) {
            try {
                Method method = hVar.c;
                if (method == null) {
                    m.throwNpe();
                }
                method.invoke(obj, new Object[0]);
                z2 = true;
            } catch (Exception unused) {
            }
        }
        if (!z2) {
            h.j(this, str, 5, null, 4, null);
        }
    }
}
