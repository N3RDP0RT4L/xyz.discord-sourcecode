package f0.e0.k;

import d0.t.o;
import d0.z.d.m;
import f0.e0.m.b;
import f0.e0.m.c;
import f0.y;
import g0.e;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import kotlin.TypeCastException;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: Platform.kt */
/* loaded from: classes3.dex */
public class h {
    public static volatile h a;

    /* renamed from: b */
    public static final Logger f3622b;
    public static final a c;

    /* compiled from: Platform.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final List<String> a(List<? extends y> list) {
            m.checkParameterIsNotNull(list, "protocols");
            ArrayList<y> arrayList = new ArrayList();
            for (Object obj : list) {
                if (((y) obj) != y.HTTP_1_0) {
                    arrayList.add(obj);
                }
            }
            ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(arrayList, 10));
            for (y yVar : arrayList) {
                arrayList2.add(yVar.toString());
            }
            return arrayList2;
        }

        public final byte[] b(List<? extends y> list) {
            m.checkParameterIsNotNull(list, "protocols");
            e eVar = new e();
            Iterator it = ((ArrayList) a(list)).iterator();
            while (it.hasNext()) {
                String str = (String) it.next();
                eVar.T(str.length());
                eVar.b0(str);
            }
            return eVar.Z(eVar.k);
        }

        public final boolean c() {
            return m.areEqual("Dalvik", System.getProperty("java.vm.name"));
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:34:0x00ae, code lost:
        if (r0 != null) goto L64;
     */
    /* JADX WARN: Code restructure failed: missing block: B:41:0x00d4, code lost:
        if (r0 != null) goto L64;
     */
    /* JADX WARN: Code restructure failed: missing block: B:48:0x00fa, code lost:
        if (r0 != null) goto L64;
     */
    /* JADX WARN: Code restructure failed: missing block: B:57:0x0122, code lost:
        if (java.lang.Integer.parseInt(r0) >= 9) goto L62;
     */
    /* JADX WARN: Removed duplicated region for block: B:63:0x018b  */
    static {
        /*
            Method dump skipped, instructions count: 415
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: f0.e0.k.h.<clinit>():void");
    }

    public static /* synthetic */ void j(h hVar, String str, int i, Throwable th, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 4;
        }
        int i3 = i2 & 4;
        hVar.i(str, i, null);
    }

    public void a(SSLSocket sSLSocket) {
        m.checkParameterIsNotNull(sSLSocket, "sslSocket");
    }

    public c b(X509TrustManager x509TrustManager) {
        m.checkParameterIsNotNull(x509TrustManager, "trustManager");
        return new f0.e0.m.a(c(x509TrustManager));
    }

    public f0.e0.m.e c(X509TrustManager x509TrustManager) {
        m.checkParameterIsNotNull(x509TrustManager, "trustManager");
        X509Certificate[] acceptedIssuers = x509TrustManager.getAcceptedIssuers();
        m.checkExpressionValueIsNotNull(acceptedIssuers, "trustManager.acceptedIssuers");
        return new b((X509Certificate[]) Arrays.copyOf(acceptedIssuers, acceptedIssuers.length));
    }

    public void d(SSLSocket sSLSocket, String str, List<y> list) {
        m.checkParameterIsNotNull(sSLSocket, "sslSocket");
        m.checkParameterIsNotNull(list, "protocols");
    }

    public void e(Socket socket, InetSocketAddress inetSocketAddress, int i) throws IOException {
        m.checkParameterIsNotNull(socket, "socket");
        m.checkParameterIsNotNull(inetSocketAddress, "address");
        socket.connect(inetSocketAddress, i);
    }

    public String f(SSLSocket sSLSocket) {
        m.checkParameterIsNotNull(sSLSocket, "sslSocket");
        return null;
    }

    public Object g(String str) {
        m.checkParameterIsNotNull(str, "closer");
        if (f3622b.isLoggable(Level.FINE)) {
            return new Throwable(str);
        }
        return null;
    }

    public boolean h(String str) {
        m.checkParameterIsNotNull(str, "hostname");
        return true;
    }

    public void i(String str, int i, Throwable th) {
        m.checkParameterIsNotNull(str, "message");
        f3622b.log(i == 5 ? Level.WARNING : Level.INFO, str, th);
    }

    public void k(String str, Object obj) {
        m.checkParameterIsNotNull(str, "message");
        if (obj == null) {
            str = b.d.b.a.a.v(str, " To see where this was allocated, set the OkHttpClient logger level to FINE: Logger.getLogger(OkHttpClient.class.getName()).setLevel(Level.FINE);");
        }
        i(str, 5, (Throwable) obj);
    }

    public SSLContext l() {
        SSLContext sSLContext = SSLContext.getInstance("TLS");
        m.checkExpressionValueIsNotNull(sSLContext, "SSLContext.getInstance(\"TLS\")");
        return sSLContext;
    }

    public SSLSocketFactory m(X509TrustManager x509TrustManager) {
        m.checkParameterIsNotNull(x509TrustManager, "trustManager");
        try {
            SSLContext l = l();
            l.init(null, new TrustManager[]{x509TrustManager}, null);
            SSLSocketFactory socketFactory = l.getSocketFactory();
            m.checkExpressionValueIsNotNull(socketFactory, "newSSLContext().apply {\n…ll)\n      }.socketFactory");
            return socketFactory;
        } catch (GeneralSecurityException e) {
            throw new AssertionError("No System TLS: " + e, e);
        }
    }

    public X509TrustManager n() {
        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        trustManagerFactory.init((KeyStore) null);
        m.checkExpressionValueIsNotNull(trustManagerFactory, "factory");
        TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
        if (trustManagers == null) {
            m.throwNpe();
        }
        boolean z2 = true;
        if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
            z2 = false;
        }
        if (z2) {
            TrustManager trustManager = trustManagers[0];
            if (trustManager != null) {
                return (X509TrustManager) trustManager;
            }
            throw new TypeCastException("null cannot be cast to non-null type javax.net.ssl.X509TrustManager");
        }
        StringBuilder R = b.d.b.a.a.R("Unexpected default trust managers: ");
        String arrays = Arrays.toString(trustManagers);
        m.checkExpressionValueIsNotNull(arrays, "java.util.Arrays.toString(this)");
        R.append(arrays);
        throw new IllegalStateException(R.toString().toString());
    }

    public String toString() {
        String simpleName = getClass().getSimpleName();
        m.checkExpressionValueIsNotNull(simpleName, "javaClass.simpleName");
        return simpleName;
    }
}
