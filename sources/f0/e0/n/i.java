package f0.e0.n;

import b.d.b.a.a;
import d0.z.d.m;
import g0.e;
import java.io.Closeable;
import java.io.IOException;
import java.util.Random;
import okio.BufferedSink;
import okio.ByteString;
/* compiled from: WebSocketWriter.kt */
/* loaded from: classes3.dex */
public final class i implements Closeable {
    public final e j = new e();
    public final e k;
    public boolean l;
    public a m;
    public final byte[] n;
    public final e.a o;
    public final boolean p;
    public final BufferedSink q;
    public final Random r;

    /* renamed from: s  reason: collision with root package name */
    public final boolean f3640s;
    public final boolean t;
    public final long u;

    public i(boolean z2, BufferedSink bufferedSink, Random random, boolean z3, boolean z4, long j) {
        m.checkParameterIsNotNull(bufferedSink, "sink");
        m.checkParameterIsNotNull(random, "random");
        this.p = z2;
        this.q = bufferedSink;
        this.r = random;
        this.f3640s = z3;
        this.t = z4;
        this.u = j;
        this.k = bufferedSink.g();
        e.a aVar = null;
        this.n = z2 ? new byte[4] : null;
        this.o = z2 ? new e.a() : aVar;
    }

    public final void a(int i, ByteString byteString) throws IOException {
        String str;
        ByteString byteString2 = ByteString.j;
        if (!(i == 0 && byteString == null)) {
            if (i != 0) {
                if (i < 1000 || i >= 5000) {
                    str = a.p("Code must be in range [1000,5000): ", i);
                } else {
                    str = ((1004 > i || 1006 < i) && (1015 > i || 2999 < i)) ? null : a.q("Code ", i, " is reserved and may not be used.");
                }
                if (!(str == null)) {
                    if (str == null) {
                        m.throwNpe();
                    }
                    throw new IllegalArgumentException(str.toString());
                }
            }
            e eVar = new e();
            eVar.X(i);
            if (byteString != null) {
                eVar.O(byteString);
            }
            byteString2 = eVar.x();
        }
        try {
            b(8, byteString2);
        } finally {
            this.l = true;
        }
    }

    public final void b(int i, ByteString byteString) throws IOException {
        if (!this.l) {
            int j = byteString.j();
            if (((long) j) <= 125) {
                this.k.T(i | 128);
                if (this.p) {
                    this.k.T(j | 128);
                    Random random = this.r;
                    byte[] bArr = this.n;
                    if (bArr == null) {
                        m.throwNpe();
                    }
                    random.nextBytes(bArr);
                    this.k.R(this.n);
                    if (j > 0) {
                        e eVar = this.k;
                        long j2 = eVar.k;
                        eVar.O(byteString);
                        e eVar2 = this.k;
                        e.a aVar = this.o;
                        if (aVar == null) {
                            m.throwNpe();
                        }
                        eVar2.u(aVar);
                        this.o.b(j2);
                        g.a(this.o, this.n);
                        this.o.close();
                    }
                } else {
                    this.k.T(j);
                    this.k.O(byteString);
                }
                this.q.flush();
                return;
            }
            throw new IllegalArgumentException("Payload size must be less than or equal to 125".toString());
        }
        throw new IOException("closed");
    }

    /* JADX WARN: Removed duplicated region for block: B:33:0x00a0  */
    /* JADX WARN: Removed duplicated region for block: B:41:0x00be  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void c(int r14, okio.ByteString r15) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 462
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: f0.e0.n.i.c(int, okio.ByteString):void");
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        a aVar = this.m;
        if (aVar != null) {
            aVar.l.close();
        }
    }
}
