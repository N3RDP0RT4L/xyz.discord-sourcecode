package f0.e0.n;

import g0.e;
import g0.h;
import java.io.Closeable;
import java.io.IOException;
import java.util.zip.Deflater;
/* compiled from: MessageDeflater.kt */
/* loaded from: classes3.dex */
public final class a implements Closeable {
    public final e j;
    public final Deflater k;
    public final h l;
    public final boolean m;

    public a(boolean z2) {
        this.m = z2;
        e eVar = new e();
        this.j = eVar;
        Deflater deflater = new Deflater(-1, true);
        this.k = deflater;
        this.l = new h(eVar, deflater);
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.l.close();
    }
}
