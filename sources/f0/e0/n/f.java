package f0.e0.n;

import b.d.b.a.a;
import d0.z.d.m;
/* compiled from: WebSocketExtensions.kt */
/* loaded from: classes3.dex */
public final class f {
    public final boolean a;

    /* renamed from: b  reason: collision with root package name */
    public final Integer f3636b;
    public final boolean c;
    public final Integer d;
    public final boolean e;
    public final boolean f;

    public f() {
        this.a = false;
        this.f3636b = null;
        this.c = false;
        this.d = null;
        this.e = false;
        this.f = false;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        return this.a == fVar.a && m.areEqual(this.f3636b, fVar.f3636b) && this.c == fVar.c && m.areEqual(this.d, fVar.d) && this.e == fVar.e && this.f == fVar.f;
    }

    public int hashCode() {
        boolean z2 = this.a;
        int i = 1;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int i4 = i2 * 31;
        Integer num = this.f3636b;
        int i5 = 0;
        int hashCode = (i4 + (num != null ? num.hashCode() : 0)) * 31;
        boolean z3 = this.c;
        if (z3) {
            z3 = true;
        }
        int i6 = z3 ? 1 : 0;
        int i7 = z3 ? 1 : 0;
        int i8 = (hashCode + i6) * 31;
        Integer num2 = this.d;
        if (num2 != null) {
            i5 = num2.hashCode();
        }
        int i9 = (i8 + i5) * 31;
        boolean z4 = this.e;
        if (z4) {
            z4 = true;
        }
        int i10 = z4 ? 1 : 0;
        int i11 = z4 ? 1 : 0;
        int i12 = (i9 + i10) * 31;
        boolean z5 = this.f;
        if (!z5) {
            i = z5 ? 1 : 0;
        }
        return i12 + i;
    }

    public String toString() {
        StringBuilder R = a.R("WebSocketExtensions(perMessageDeflate=");
        R.append(this.a);
        R.append(", clientMaxWindowBits=");
        R.append(this.f3636b);
        R.append(", clientNoContextTakeover=");
        R.append(this.c);
        R.append(", serverMaxWindowBits=");
        R.append(this.d);
        R.append(", serverNoContextTakeover=");
        R.append(this.e);
        R.append(", unknownValues=");
        return a.M(R, this.f, ")");
    }

    public f(boolean z2, Integer num, boolean z3, Integer num2, boolean z4, boolean z5) {
        this.a = z2;
        this.f3636b = num;
        this.c = z3;
        this.d = num2;
        this.e = z4;
        this.f = z5;
    }
}
