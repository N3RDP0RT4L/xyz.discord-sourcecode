package f0.e0.n;

import androidx.core.app.NotificationCompat;
import androidx.core.view.PointerIconCompat;
import d0.g0.s;
import d0.g0.t;
import d0.g0.w;
import d0.z.d.m;
import f0.e0.g.c;
import f0.e0.n.d;
import f0.f;
import java.io.IOException;
import okhttp3.Headers;
import okhttp3.Request;
import okhttp3.Response;
/* compiled from: RealWebSocket.kt */
/* loaded from: classes3.dex */
public final class e implements f {
    public final /* synthetic */ d a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ Request f3635b;

    public e(d dVar, Request request) {
        this.a = dVar;
        this.f3635b = request;
    }

    @Override // f0.f
    public void a(f0.e eVar, Response response) {
        int intValue;
        m.checkParameterIsNotNull(eVar, NotificationCompat.CATEGORY_CALL);
        m.checkParameterIsNotNull(response, "response");
        c cVar = response.v;
        try {
            this.a.h(response, cVar);
            d.c d = cVar.d();
            Headers headers = response.o;
            m.checkParameterIsNotNull(headers, "responseHeaders");
            int size = headers.size();
            int i = 0;
            int i2 = 0;
            boolean z2 = false;
            boolean z3 = false;
            boolean z4 = false;
            Integer num = null;
            Integer num2 = null;
            boolean z5 = false;
            while (i2 < size) {
                if (t.equals(headers.d(i2), "Sec-WebSocket-Extensions", true)) {
                    String g = headers.g(i2);
                    int i3 = 0;
                    while (i3 < g.length()) {
                        int h = f0.e0.c.h(g, ',', i3, i, 4);
                        int f = f0.e0.c.f(g, ';', i3, h);
                        String C = f0.e0.c.C(g, i3, f);
                        int i4 = f + 1;
                        if (t.equals(C, "permessage-deflate", true)) {
                            if (z2) {
                                z5 = true;
                            }
                            while (i4 < h) {
                                int f2 = f0.e0.c.f(g, ';', i4, h);
                                int f3 = f0.e0.c.f(g, '=', i4, f2);
                                String C2 = f0.e0.c.C(g, i4, f3);
                                String removeSurrounding = f3 < f2 ? w.removeSurrounding(f0.e0.c.C(g, f3 + 1, f2), "\"") : null;
                                i4 = f2 + 1;
                                if (t.equals(C2, "client_max_window_bits", true)) {
                                    if (num != null) {
                                        z5 = true;
                                    }
                                    num = removeSurrounding != null ? s.toIntOrNull(removeSurrounding) : null;
                                    if (num != null) {
                                    }
                                    z5 = true;
                                } else if (t.equals(C2, "client_no_context_takeover", true)) {
                                    if (z3) {
                                        z5 = true;
                                    }
                                    if (removeSurrounding != null) {
                                        z5 = true;
                                    }
                                    z3 = true;
                                } else if (t.equals(C2, "server_max_window_bits", true)) {
                                    if (num2 != null) {
                                        z5 = true;
                                    }
                                    num2 = removeSurrounding != null ? s.toIntOrNull(removeSurrounding) : null;
                                    if (num2 != null) {
                                    }
                                    z5 = true;
                                } else {
                                    if (t.equals(C2, "server_no_context_takeover", true)) {
                                        if (z4) {
                                            z5 = true;
                                        }
                                        if (removeSurrounding != null) {
                                            z5 = true;
                                        }
                                        z4 = true;
                                    }
                                    z5 = true;
                                }
                            }
                            i3 = i4;
                            z2 = true;
                        } else {
                            i3 = i4;
                            z5 = true;
                        }
                        i = 0;
                    }
                }
                i2++;
                i = 0;
            }
            this.a.f3631y = new f(z2, num, z3, num2, z4, z5);
            if (!(!z5 && num == null && (num2 == null || (8 <= (intValue = num2.intValue()) && 15 >= intValue)))) {
                synchronized (this.a) {
                    this.a.k.clear();
                    this.a.e(PointerIconCompat.TYPE_ALIAS, "unexpected Sec-WebSocket-Extensions in response header");
                }
            }
            try {
                this.a.j(f0.e0.c.g + " WebSocket " + this.f3635b.f3784b.h(), d);
                d dVar = this.a;
                dVar.v.onOpen(dVar, response);
                this.a.k();
            } catch (Exception e) {
                this.a.i(e, null);
            }
        } catch (IOException e2) {
            if (cVar != null) {
                cVar.a(-1L, true, true, null);
            }
            this.a.i(e2, response);
            byte[] bArr = f0.e0.c.a;
            m.checkParameterIsNotNull(response, "$this$closeQuietly");
            try {
                response.close();
            } catch (RuntimeException e3) {
                throw e3;
            } catch (Exception unused) {
            }
        }
    }

    @Override // f0.f
    public void b(f0.e eVar, IOException iOException) {
        m.checkParameterIsNotNull(eVar, NotificationCompat.CATEGORY_CALL);
        m.checkParameterIsNotNull(iOException, "e");
        this.a.i(iOException, null);
    }
}
