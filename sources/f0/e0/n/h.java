package f0.e0.n;

import d0.z.d.m;
import f0.e0.c;
import g0.e;
import g0.g;
import java.io.Closeable;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.concurrent.TimeUnit;
import okio.ByteString;
import org.objectweb.asm.Opcodes;
/* compiled from: WebSocketReader.kt */
/* loaded from: classes3.dex */
public final class h implements Closeable {
    public boolean j;
    public int k;
    public long l;
    public boolean m;
    public boolean n;
    public boolean o;
    public final e p = new e();
    public final e q = new e();
    public c r;

    /* renamed from: s  reason: collision with root package name */
    public final byte[] f3637s;
    public final e.a t;
    public final boolean u;
    public final g v;
    public final a w;

    /* renamed from: x  reason: collision with root package name */
    public final boolean f3638x;

    /* renamed from: y  reason: collision with root package name */
    public final boolean f3639y;

    /* compiled from: WebSocketReader.kt */
    /* loaded from: classes3.dex */
    public interface a {
        void b(ByteString byteString) throws IOException;

        void c(String str) throws IOException;

        void d(ByteString byteString);

        void f(ByteString byteString);

        void g(int i, String str);
    }

    public h(boolean z2, g gVar, a aVar, boolean z3, boolean z4) {
        m.checkParameterIsNotNull(gVar, "source");
        m.checkParameterIsNotNull(aVar, "frameCallback");
        this.u = z2;
        this.v = gVar;
        this.w = aVar;
        this.f3638x = z3;
        this.f3639y = z4;
        e.a aVar2 = null;
        this.f3637s = z2 ? null : new byte[4];
        this.t = !z2 ? new e.a() : aVar2;
    }

    public final void a() throws IOException {
        String str;
        String str2;
        long j = this.l;
        if (j > 0) {
            this.v.B(this.p, j);
            if (!this.u) {
                e eVar = this.p;
                e.a aVar = this.t;
                if (aVar == null) {
                    m.throwNpe();
                }
                eVar.u(aVar);
                this.t.b(0L);
                e.a aVar2 = this.t;
                byte[] bArr = this.f3637s;
                if (bArr == null) {
                    m.throwNpe();
                }
                g.a(aVar2, bArr);
                this.t.close();
            }
        }
        switch (this.k) {
            case 8:
                short s2 = 1005;
                e eVar2 = this.p;
                long j2 = eVar2.k;
                if (j2 != 1) {
                    if (j2 != 0) {
                        s2 = eVar2.readShort();
                        str = this.p.D();
                        if (s2 < 1000 || s2 >= 5000) {
                            str2 = b.d.b.a.a.p("Code must be in range [1000,5000): ", s2);
                        } else {
                            str2 = ((1004 > s2 || 1006 < s2) && (1015 > s2 || 2999 < s2)) ? null : b.d.b.a.a.q("Code ", s2, " is reserved and may not be used.");
                        }
                        if (str2 != null) {
                            throw new ProtocolException(str2);
                        }
                    } else {
                        str = "";
                    }
                    this.w.g(s2, str);
                    this.j = true;
                    return;
                }
                throw new ProtocolException("Malformed close payload length of 1.");
            case 9:
                this.w.d(this.p.x());
                return;
            case 10:
                this.w.f(this.p.x());
                return;
            default:
                StringBuilder R = b.d.b.a.a.R("Unknown control opcode: ");
                R.append(c.x(this.k));
                throw new ProtocolException(R.toString());
        }
    }

    /* JADX WARN: Finally extract failed */
    public final void b() throws IOException, ProtocolException {
        if (!this.j) {
            long h = this.v.timeout().h();
            this.v.timeout().b();
            try {
                byte readByte = this.v.readByte();
                byte[] bArr = c.a;
                int i = readByte & 255;
                this.v.timeout().g(h, TimeUnit.NANOSECONDS);
                int i2 = i & 15;
                this.k = i2;
                boolean z2 = false;
                boolean z3 = (i & 128) != 0;
                this.m = z3;
                boolean z4 = (i & 8) != 0;
                this.n = z4;
                if (!z4 || z3) {
                    boolean z5 = (i & 64) != 0;
                    if (i2 == 1 || i2 == 2) {
                        if (!z5) {
                            this.o = false;
                        } else if (this.f3638x) {
                            this.o = true;
                        } else {
                            throw new ProtocolException("Unexpected rsv1 flag");
                        }
                    } else if (z5) {
                        throw new ProtocolException("Unexpected rsv1 flag");
                    }
                    if (!((i & 32) != 0)) {
                        if (!((i & 16) != 0)) {
                            int readByte2 = this.v.readByte() & 255;
                            if ((readByte2 & 128) != 0) {
                                z2 = true;
                            }
                            if (z2 == this.u) {
                                throw new ProtocolException(this.u ? "Server-sent frames must not be masked." : "Client-sent frames must be masked.");
                            }
                            long j = readByte2 & Opcodes.LAND;
                            this.l = j;
                            if (j == 126) {
                                this.l = this.v.readShort() & 65535;
                            } else if (j == ((long) Opcodes.LAND)) {
                                long readLong = this.v.readLong();
                                this.l = readLong;
                                if (readLong < 0) {
                                    StringBuilder R = b.d.b.a.a.R("Frame length 0x");
                                    String hexString = Long.toHexString(this.l);
                                    m.checkExpressionValueIsNotNull(hexString, "java.lang.Long.toHexString(this)");
                                    R.append(hexString);
                                    R.append(" > 0x7FFFFFFFFFFFFFFF");
                                    throw new ProtocolException(R.toString());
                                }
                            }
                            if (this.n && this.l > 125) {
                                throw new ProtocolException("Control frame must be less than 125B.");
                            } else if (z2) {
                                g gVar = this.v;
                                byte[] bArr2 = this.f3637s;
                                if (bArr2 == null) {
                                    m.throwNpe();
                                }
                                gVar.readFully(bArr2);
                            }
                        } else {
                            throw new ProtocolException("Unexpected rsv3 flag");
                        }
                    } else {
                        throw new ProtocolException("Unexpected rsv2 flag");
                    }
                } else {
                    throw new ProtocolException("Control frames must be final.");
                }
            } catch (Throwable th) {
                this.v.timeout().g(h, TimeUnit.NANOSECONDS);
                throw th;
            }
        } else {
            throw new IOException("closed");
        }
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        c cVar = this.r;
        if (cVar != null) {
            cVar.l.close();
        }
    }
}
