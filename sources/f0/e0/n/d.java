package f0.e0.n;

import androidx.browser.trusted.sharing.ShareTarget;
import androidx.core.app.NotificationCompat;
import androidx.core.view.PointerIconCompat;
import com.adjust.sdk.Constants;
import com.discord.api.permission.Permission;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.g0.t;
import d0.t.m;
import f0.e0.n.h;
import f0.y;
import g0.g;
import java.io.Closeable;
import java.io.IOException;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.util.ArrayDeque;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import kotlin.jvm.internal.Ref$IntRef;
import kotlin.jvm.internal.Ref$ObjectRef;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.BufferedSink;
import okio.ByteString;
/* compiled from: RealWebSocket.kt */
/* loaded from: classes3.dex */
public final class d implements WebSocket, h.a {
    public static final List<y> a = m.listOf(y.HTTP_1_1);

    /* renamed from: b  reason: collision with root package name */
    public final String f3628b;
    public f0.e c;
    public f0.e0.f.a d;
    public h e;
    public i f;
    public f0.e0.f.c g;
    public String h;
    public c i;
    public long l;
    public boolean m;
    public String o;
    public boolean p;
    public int q;
    public int r;

    /* renamed from: s  reason: collision with root package name */
    public int f3629s;
    public boolean t;
    public final Request u;
    public final WebSocketListener v;
    public final Random w;

    /* renamed from: x  reason: collision with root package name */
    public final long f3630x;

    /* renamed from: z  reason: collision with root package name */
    public long f3632z;

    /* renamed from: y  reason: collision with root package name */
    public f0.e0.n.f f3631y = null;
    public final ArrayDeque<ByteString> j = new ArrayDeque<>();
    public final ArrayDeque<Object> k = new ArrayDeque<>();
    public int n = -1;

    /* compiled from: RealWebSocket.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final ByteString f3633b;
        public final long c;

        public a(int i, ByteString byteString, long j) {
            this.a = i;
            this.f3633b = byteString;
            this.c = j;
        }
    }

    /* compiled from: RealWebSocket.kt */
    /* loaded from: classes3.dex */
    public static final class b {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final ByteString f3634b;

        public b(int i, ByteString byteString) {
            d0.z.d.m.checkParameterIsNotNull(byteString, "data");
            this.a = i;
            this.f3634b = byteString;
        }
    }

    /* compiled from: RealWebSocket.kt */
    /* loaded from: classes3.dex */
    public static abstract class c implements Closeable {
        public final boolean j;
        public final g k;
        public final BufferedSink l;

        public c(boolean z2, g gVar, BufferedSink bufferedSink) {
            d0.z.d.m.checkParameterIsNotNull(gVar, "source");
            d0.z.d.m.checkParameterIsNotNull(bufferedSink, "sink");
            this.j = z2;
            this.k = gVar;
            this.l = bufferedSink;
        }
    }

    /* compiled from: RealWebSocket.kt */
    /* renamed from: f0.e0.n.d$d  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public final class C0389d extends f0.e0.f.a {
        public C0389d() {
            super(b.d.b.a.a.H(new StringBuilder(), d.this.h, " writer"), false, 2);
        }

        @Override // f0.e0.f.a
        public long a() {
            try {
                return d.this.m() ? 0L : -1L;
            } catch (IOException e) {
                d.this.i(e, null);
                return -1L;
            }
        }
    }

    /* compiled from: TaskQueue.kt */
    /* loaded from: classes3.dex */
    public static final class e extends f0.e0.f.a {
        public final /* synthetic */ long e;
        public final /* synthetic */ d f;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public e(String str, String str2, long j, d dVar, String str3, c cVar, f0.e0.n.f fVar) {
            super(str2, true);
            this.e = j;
            this.f = dVar;
        }

        @Override // f0.e0.f.a
        public long a() {
            d dVar = this.f;
            synchronized (dVar) {
                if (!dVar.p) {
                    i iVar = dVar.f;
                    if (iVar != null) {
                        int i = dVar.t ? dVar.q : -1;
                        dVar.q++;
                        dVar.t = true;
                        if (i != -1) {
                            StringBuilder R = b.d.b.a.a.R("sent ping but didn't receive pong within ");
                            R.append(dVar.f3630x);
                            R.append("ms (after ");
                            R.append(i - 1);
                            R.append(" successful ping/pongs)");
                            dVar.i(new SocketTimeoutException(R.toString()), null);
                        } else {
                            try {
                                ByteString byteString = ByteString.j;
                                d0.z.d.m.checkParameterIsNotNull(byteString, "payload");
                                iVar.b(9, byteString);
                            } catch (IOException e) {
                                dVar.i(e, null);
                            }
                        }
                    }
                }
            }
            return this.e;
        }
    }

    /* compiled from: TaskQueue.kt */
    /* loaded from: classes3.dex */
    public static final class f extends f0.e0.f.a {
        public final /* synthetic */ d e;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public f(String str, boolean z2, String str2, boolean z3, d dVar, i iVar, ByteString byteString, Ref$ObjectRef ref$ObjectRef, Ref$IntRef ref$IntRef, Ref$ObjectRef ref$ObjectRef2, Ref$ObjectRef ref$ObjectRef3, Ref$ObjectRef ref$ObjectRef4, Ref$ObjectRef ref$ObjectRef5) {
            super(str2, z3);
            this.e = dVar;
        }

        @Override // f0.e0.f.a
        public long a() {
            f0.e eVar = this.e.c;
            if (eVar == null) {
                d0.z.d.m.throwNpe();
            }
            eVar.cancel();
            return -1L;
        }
    }

    public d(f0.e0.f.d dVar, Request request, WebSocketListener webSocketListener, Random random, long j, f0.e0.n.f fVar, long j2) {
        d0.z.d.m.checkParameterIsNotNull(dVar, "taskRunner");
        d0.z.d.m.checkParameterIsNotNull(request, "originalRequest");
        d0.z.d.m.checkParameterIsNotNull(webSocketListener, "listener");
        d0.z.d.m.checkParameterIsNotNull(random, "random");
        this.u = request;
        this.v = webSocketListener;
        this.w = random;
        this.f3630x = j;
        this.f3632z = j2;
        this.g = dVar.f();
        if (d0.z.d.m.areEqual(ShareTarget.METHOD_GET, request.c)) {
            ByteString.a aVar = ByteString.k;
            byte[] bArr = new byte[16];
            random.nextBytes(bArr);
            this.f3628b = ByteString.a.d(aVar, bArr, 0, 0, 3).f();
            return;
        }
        StringBuilder R = b.d.b.a.a.R("Request must be GET: ");
        R.append(request.c);
        throw new IllegalArgumentException(R.toString().toString());
    }

    @Override // okhttp3.WebSocket
    public boolean a(String str) {
        d0.z.d.m.checkParameterIsNotNull(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
        ByteString c2 = ByteString.k.c(str);
        synchronized (this) {
            if (!this.p && !this.m) {
                if (this.l + c2.j() > Permission.MOVE_MEMBERS) {
                    e(PointerIconCompat.TYPE_CONTEXT_MENU, null);
                    return false;
                }
                this.l += c2.j();
                this.k.add(new b(1, c2));
                l();
                return true;
            }
            return false;
        }
    }

    @Override // f0.e0.n.h.a
    public void b(ByteString byteString) throws IOException {
        d0.z.d.m.checkParameterIsNotNull(byteString, "bytes");
        this.v.onMessage(this, byteString);
    }

    @Override // f0.e0.n.h.a
    public void c(String str) throws IOException {
        d0.z.d.m.checkParameterIsNotNull(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
        this.v.onMessage(this, str);
    }

    @Override // f0.e0.n.h.a
    public synchronized void d(ByteString byteString) {
        d0.z.d.m.checkParameterIsNotNull(byteString, "payload");
        if (!this.p && (!this.m || !this.k.isEmpty())) {
            this.j.add(byteString);
            l();
            this.r++;
        }
    }

    @Override // okhttp3.WebSocket
    public boolean e(int i, String str) {
        String str2;
        synchronized (this) {
            ByteString byteString = null;
            if (i < 1000 || i >= 5000) {
                str2 = "Code must be in range [1000,5000): " + i;
            } else if ((1004 > i || 1006 < i) && (1015 > i || 2999 < i)) {
                str2 = null;
            } else {
                str2 = "Code " + i + " is reserved and may not be used.";
            }
            if (!(str2 == null)) {
                if (str2 == null) {
                    d0.z.d.m.throwNpe();
                }
                throw new IllegalArgumentException(str2.toString());
            }
            if (str != null) {
                byteString = ByteString.k.c(str);
                if (!(((long) byteString.j()) <= 123)) {
                    throw new IllegalArgumentException(("reason.size() > 123: " + str).toString());
                }
            }
            if (!this.p && !this.m) {
                this.m = true;
                this.k.add(new a(i, byteString, 60000L));
                l();
                return true;
            }
            return false;
        }
    }

    @Override // f0.e0.n.h.a
    public synchronized void f(ByteString byteString) {
        d0.z.d.m.checkParameterIsNotNull(byteString, "payload");
        this.f3629s++;
        this.t = false;
    }

    @Override // f0.e0.n.h.a
    public void g(int i, String str) {
        c cVar;
        i iVar;
        h hVar;
        d0.z.d.m.checkParameterIsNotNull(str, ModelAuditLogEntry.CHANGE_KEY_REASON);
        boolean z2 = false;
        if (i != -1) {
            synchronized (this) {
                if (this.n == -1) {
                    z2 = true;
                }
                if (z2) {
                    this.n = i;
                    this.o = str;
                    cVar = null;
                    if (!this.m || !this.k.isEmpty()) {
                        hVar = null;
                        iVar = null;
                    } else {
                        c cVar2 = this.i;
                        this.i = null;
                        hVar = this.e;
                        this.e = null;
                        iVar = this.f;
                        this.f = null;
                        this.g.f();
                        cVar = cVar2;
                    }
                } else {
                    throw new IllegalStateException("already closed".toString());
                }
            }
            try {
                this.v.onClosing(this, i, str);
                if (cVar != null) {
                    this.v.onClosed(this, i, str);
                }
                if (cVar != null) {
                    byte[] bArr = f0.e0.c.a;
                    d0.z.d.m.checkParameterIsNotNull(cVar, "$this$closeQuietly");
                    try {
                        cVar.close();
                    } catch (RuntimeException e2) {
                        throw e2;
                    } catch (Exception unused) {
                    }
                }
                if (hVar != null) {
                    byte[] bArr2 = f0.e0.c.a;
                    d0.z.d.m.checkParameterIsNotNull(hVar, "$this$closeQuietly");
                    try {
                        hVar.close();
                    } catch (RuntimeException e3) {
                        throw e3;
                    } catch (Exception unused2) {
                    }
                }
                if (iVar != null) {
                    byte[] bArr3 = f0.e0.c.a;
                    d0.z.d.m.checkParameterIsNotNull(iVar, "$this$closeQuietly");
                    try {
                        iVar.close();
                    } catch (RuntimeException e4) {
                        throw e4;
                    } catch (Exception unused3) {
                    }
                }
            } catch (Throwable th) {
                if (cVar != null) {
                    byte[] bArr4 = f0.e0.c.a;
                    d0.z.d.m.checkParameterIsNotNull(cVar, "$this$closeQuietly");
                    try {
                        cVar.close();
                    } catch (RuntimeException e5) {
                        throw e5;
                    } catch (Exception unused4) {
                    }
                }
                if (hVar != null) {
                    byte[] bArr5 = f0.e0.c.a;
                    d0.z.d.m.checkParameterIsNotNull(hVar, "$this$closeQuietly");
                    try {
                        hVar.close();
                    } catch (RuntimeException e6) {
                        throw e6;
                    } catch (Exception unused5) {
                    }
                }
                if (iVar != null) {
                    byte[] bArr6 = f0.e0.c.a;
                    d0.z.d.m.checkParameterIsNotNull(iVar, "$this$closeQuietly");
                    try {
                        iVar.close();
                    } catch (RuntimeException e7) {
                        throw e7;
                    } catch (Exception unused6) {
                    }
                }
                throw th;
            }
        } else {
            throw new IllegalArgumentException("Failed requirement.".toString());
        }
    }

    public final void h(Response response, f0.e0.g.c cVar) throws IOException {
        d0.z.d.m.checkParameterIsNotNull(response, "response");
        if (response.m == 101) {
            String a2 = Response.a(response, "Connection", null, 2);
            if (t.equals("Upgrade", a2, true)) {
                String a3 = Response.a(response, "Upgrade", null, 2);
                if (t.equals("websocket", a3, true)) {
                    String a4 = Response.a(response, "Sec-WebSocket-Accept", null, 2);
                    ByteString.a aVar = ByteString.k;
                    String f2 = aVar.c(this.f3628b + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11").g(Constants.SHA1).f();
                    if (!d0.z.d.m.areEqual(f2, a4)) {
                        throw new ProtocolException("Expected 'Sec-WebSocket-Accept' header value '" + f2 + "' but was '" + a4 + '\'');
                    } else if (cVar == null) {
                        throw new ProtocolException("Web Socket exchange missing: bad interceptor?");
                    }
                } else {
                    throw new ProtocolException("Expected 'Upgrade' header value 'websocket' but was '" + a3 + '\'');
                }
            } else {
                throw new ProtocolException("Expected 'Connection' header value 'Upgrade' but was '" + a2 + '\'');
            }
        } else {
            StringBuilder R = b.d.b.a.a.R("Expected HTTP 101 response but was '");
            R.append(response.m);
            R.append(' ');
            throw new ProtocolException(b.d.b.a.a.G(R, response.l, '\''));
        }
    }

    public final void i(Exception exc, Response response) {
        d0.z.d.m.checkParameterIsNotNull(exc, "e");
        synchronized (this) {
            if (!this.p) {
                this.p = true;
                c cVar = this.i;
                this.i = null;
                h hVar = this.e;
                this.e = null;
                i iVar = this.f;
                this.f = null;
                this.g.f();
                try {
                    this.v.onFailure(this, exc, response);
                    if (cVar != null) {
                        byte[] bArr = f0.e0.c.a;
                        d0.z.d.m.checkParameterIsNotNull(cVar, "$this$closeQuietly");
                        try {
                            cVar.close();
                        } catch (RuntimeException e2) {
                            throw e2;
                        } catch (Exception unused) {
                        }
                    }
                    if (hVar != null) {
                        byte[] bArr2 = f0.e0.c.a;
                        d0.z.d.m.checkParameterIsNotNull(hVar, "$this$closeQuietly");
                        try {
                            hVar.close();
                        } catch (RuntimeException e3) {
                            throw e3;
                        } catch (Exception unused2) {
                        }
                    }
                    if (iVar != null) {
                        byte[] bArr3 = f0.e0.c.a;
                        d0.z.d.m.checkParameterIsNotNull(iVar, "$this$closeQuietly");
                        try {
                            iVar.close();
                        } catch (RuntimeException e4) {
                            throw e4;
                        } catch (Exception unused3) {
                        }
                    }
                } catch (Throwable th) {
                    if (cVar != null) {
                        byte[] bArr4 = f0.e0.c.a;
                        d0.z.d.m.checkParameterIsNotNull(cVar, "$this$closeQuietly");
                        try {
                            cVar.close();
                        } catch (RuntimeException e5) {
                            throw e5;
                        } catch (Exception unused4) {
                        }
                    }
                    if (hVar != null) {
                        byte[] bArr5 = f0.e0.c.a;
                        d0.z.d.m.checkParameterIsNotNull(hVar, "$this$closeQuietly");
                        try {
                            hVar.close();
                        } catch (RuntimeException e6) {
                            throw e6;
                        } catch (Exception unused5) {
                        }
                    }
                    if (iVar != null) {
                        byte[] bArr6 = f0.e0.c.a;
                        d0.z.d.m.checkParameterIsNotNull(iVar, "$this$closeQuietly");
                        try {
                            iVar.close();
                        } catch (RuntimeException e7) {
                            throw e7;
                        } catch (Exception unused6) {
                        }
                    }
                    throw th;
                }
            }
        }
    }

    public final void j(String str, c cVar) throws IOException {
        boolean z2;
        boolean z3;
        d0.z.d.m.checkParameterIsNotNull(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        d0.z.d.m.checkParameterIsNotNull(cVar, "streams");
        f0.e0.n.f fVar = this.f3631y;
        if (fVar == null) {
            d0.z.d.m.throwNpe();
        }
        synchronized (this) {
            this.h = str;
            this.i = cVar;
            boolean z4 = cVar.j;
            BufferedSink bufferedSink = cVar.l;
            Random random = this.w;
            boolean z5 = fVar.a;
            if (z4) {
                z2 = fVar.c;
            } else {
                z2 = fVar.e;
            }
            this.f = new i(z4, bufferedSink, random, z5, z2, this.f3632z);
            this.d = new C0389d();
            long j = this.f3630x;
            if (j != 0) {
                long nanos = TimeUnit.MILLISECONDS.toNanos(j);
                String str2 = str + " ping";
                this.g.c(new e(str2, str2, nanos, this, str, cVar, fVar), nanos);
            }
            if (!this.k.isEmpty()) {
                l();
            }
        }
        boolean z6 = cVar.j;
        g gVar = cVar.k;
        boolean z7 = fVar.a;
        if (!z6) {
            z3 = fVar.c;
        } else {
            z3 = fVar.e;
        }
        this.e = new h(z6, gVar, this, z7, z3);
    }

    /* JADX WARN: Removed duplicated region for block: B:71:0x00f2 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:72:0x00e5 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void k() throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 311
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: f0.e0.n.d.k():void");
    }

    public final void l() {
        byte[] bArr = f0.e0.c.a;
        f0.e0.f.a aVar = this.d;
        if (aVar != null) {
            this.g.c(aVar, 0L);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:29:0x00f7  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x010c A[Catch: all -> 0x01d4, TryCatch #9 {all -> 0x01d4, blocks: (B:31:0x00fb, B:32:0x00fe, B:33:0x010c, B:36:0x0116, B:38:0x011a, B:39:0x011d, B:40:0x0124, B:42:0x0131, B:44:0x0138, B:45:0x013f, B:46:0x0140, B:49:0x0146, B:51:0x014a, B:52:0x014d, B:41:0x0125), top: B:124:0x00f5 }] */
    /* JADX WARN: Removed duplicated region for block: B:62:0x0177  */
    /* JADX WARN: Removed duplicated region for block: B:69:0x018d  */
    /* JADX WARN: Removed duplicated region for block: B:76:0x01a3  */
    /* JADX WARN: Type inference failed for: r1v11 */
    /* JADX WARN: Type inference failed for: r1v23, types: [f0.e0.n.i] */
    /* JADX WARN: Type inference failed for: r1v26, types: [kotlin.jvm.internal.Ref$ObjectRef] */
    /* JADX WARN: Type inference failed for: r2v15, types: [boolean] */
    /* JADX WARN: Type inference failed for: r2v19 */
    /* JADX WARN: Type inference failed for: r2v25, types: [f0.e0.n.d$c, T] */
    /* JADX WARN: Type inference failed for: r2v26, types: [T, f0.e0.n.h] */
    /* JADX WARN: Type inference failed for: r2v27, types: [T, f0.e0.n.i] */
    /* JADX WARN: Type inference failed for: r2v6 */
    /* JADX WARN: Type inference failed for: r3v16, types: [T, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r3v3, types: [okio.ByteString, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r3v5 */
    /* JADX WARN: Type inference failed for: r3v9 */
    /* JADX WARN: Type inference failed for: r4v4, types: [T, java.lang.String] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final boolean m() throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 543
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: f0.e0.n.d.m():boolean");
    }
}
