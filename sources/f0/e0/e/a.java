package f0.e0.e;

import androidx.core.app.NotificationCompat;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.g0.t;
import d0.g0.w;
import d0.z.d.m;
import f0.e0.g.c;
import f0.e0.g.e;
import f0.e0.h.g;
import f0.v;
import f0.y;
import java.io.IOException;
import java.util.ArrayList;
import kotlin.TypeCastException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
/* compiled from: CacheInterceptor.kt */
/* loaded from: classes3.dex */
public final class a implements Interceptor {

    /* renamed from: b  reason: collision with root package name */
    public static final C0383a f3579b = new C0383a(null);

    /* compiled from: CacheInterceptor.kt */
    /* renamed from: f0.e0.e.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0383a {
        public C0383a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public static final Response a(C0383a aVar, Response response) {
            if ((response != null ? response.p : null) == null) {
                return response;
            }
            m.checkParameterIsNotNull(response, "response");
            Request request = response.j;
            y yVar = response.k;
            int i = response.m;
            String str = response.l;
            v vVar = response.n;
            Headers.a e = response.o.e();
            Response response2 = response.q;
            Response response3 = response.r;
            Response response4 = response.f3787s;
            long j = response.t;
            long j2 = response.u;
            c cVar = response.v;
            if (!(i >= 0)) {
                throw new IllegalStateException(b.d.b.a.a.p("code < 0: ", i).toString());
            } else if (request == null) {
                throw new IllegalStateException("request == null".toString());
            } else if (yVar == null) {
                throw new IllegalStateException("protocol == null".toString());
            } else if (str != null) {
                return new Response(request, yVar, str, i, vVar, e.c(), null, response2, response3, response4, j, j2, cVar);
            } else {
                throw new IllegalStateException("message == null".toString());
            }
        }

        public final boolean b(String str) {
            return t.equals("Content-Length", str, true) || t.equals("Content-Encoding", str, true) || t.equals("Content-Type", str, true);
        }

        public final boolean c(String str) {
            return !t.equals("Connection", str, true) && !t.equals("Keep-Alive", str, true) && !t.equals("Proxy-Authenticate", str, true) && !t.equals("Proxy-Authorization", str, true) && !t.equals("TE", str, true) && !t.equals("Trailers", str, true) && !t.equals("Transfer-Encoding", str, true) && !t.equals("Upgrade", str, true);
        }
    }

    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) throws IOException {
        int i;
        Headers headers;
        m.checkParameterIsNotNull(chain, "chain");
        g gVar = (g) chain;
        e eVar = gVar.f3600b;
        System.currentTimeMillis();
        Request request = gVar.f;
        m.checkParameterIsNotNull(request, "request");
        b bVar = new b(request, null);
        if (request != null && request.a().k) {
            bVar = new b(null, null);
        }
        Request request2 = bVar.a;
        Response response = bVar.f3580b;
        if (!(eVar instanceof e)) {
        }
        if (request2 == null && response == null) {
            Response.a aVar = new Response.a();
            aVar.g(gVar.f);
            aVar.f(y.HTTP_1_1);
            aVar.c = 504;
            aVar.e("Unsatisfiable Request (only-if-cached)");
            aVar.g = f0.e0.c.c;
            aVar.k = -1L;
            aVar.l = System.currentTimeMillis();
            Response a = aVar.a();
            m.checkParameterIsNotNull(eVar, NotificationCompat.CATEGORY_CALL);
            m.checkParameterIsNotNull(a, "response");
            return a;
        } else if (request2 == null) {
            if (response == null) {
                m.throwNpe();
            }
            Response.a aVar2 = new Response.a(response);
            aVar2.b(C0383a.a(f3579b, response));
            Response a2 = aVar2.a();
            m.checkParameterIsNotNull(eVar, NotificationCompat.CATEGORY_CALL);
            m.checkParameterIsNotNull(a2, "response");
            return a2;
        } else {
            if (response != null) {
                m.checkParameterIsNotNull(eVar, NotificationCompat.CATEGORY_CALL);
                m.checkParameterIsNotNull(response, "cachedResponse");
            }
            Response a3 = ((g) chain).a(request2);
            if (response != null) {
                if (a3.m == 304) {
                    Response.a aVar3 = new Response.a(response);
                    C0383a aVar4 = f3579b;
                    Headers headers2 = response.o;
                    Headers headers3 = a3.o;
                    ArrayList arrayList = new ArrayList(20);
                    int i2 = 0;
                    for (int size = headers2.size(); i2 < size; size = i) {
                        String d = headers2.d(i2);
                        String g = headers2.g(i2);
                        if (t.equals("Warning", d, true)) {
                            headers = headers2;
                            i = size;
                            if (t.startsWith$default(g, "1", false, 2, null)) {
                                i2++;
                                headers2 = headers;
                            }
                        } else {
                            headers = headers2;
                            i = size;
                        }
                        if (aVar4.b(d) || !aVar4.c(d) || headers3.c(d) == null) {
                            m.checkParameterIsNotNull(d, ModelAuditLogEntry.CHANGE_KEY_NAME);
                            m.checkParameterIsNotNull(g, "value");
                            arrayList.add(d);
                            arrayList.add(w.trim(g).toString());
                        }
                        i2++;
                        headers2 = headers;
                    }
                    int size2 = headers3.size();
                    for (int i3 = 0; i3 < size2; i3++) {
                        String d2 = headers3.d(i3);
                        if (!aVar4.b(d2) && aVar4.c(d2)) {
                            String g2 = headers3.g(i3);
                            m.checkParameterIsNotNull(d2, ModelAuditLogEntry.CHANGE_KEY_NAME);
                            m.checkParameterIsNotNull(g2, "value");
                            arrayList.add(d2);
                            arrayList.add(w.trim(g2).toString());
                        }
                    }
                    Object[] array = arrayList.toArray(new String[0]);
                    if (array != null) {
                        aVar3.d(new Headers((String[]) array, null));
                        aVar3.k = a3.t;
                        aVar3.l = a3.u;
                        C0383a aVar5 = f3579b;
                        aVar3.b(C0383a.a(aVar5, response));
                        Response a4 = C0383a.a(aVar5, a3);
                        aVar3.c("networkResponse", a4);
                        aVar3.h = a4;
                        aVar3.a();
                        ResponseBody responseBody = a3.p;
                        if (responseBody == null) {
                            m.throwNpe();
                        }
                        responseBody.close();
                        m.throwNpe();
                        throw null;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                }
                ResponseBody responseBody2 = response.p;
                if (responseBody2 != null) {
                    byte[] bArr = f0.e0.c.a;
                    m.checkParameterIsNotNull(responseBody2, "$this$closeQuietly");
                    try {
                        responseBody2.close();
                    } catch (RuntimeException e) {
                        throw e;
                    } catch (Exception unused) {
                    }
                }
            }
            Response.a aVar6 = new Response.a(a3);
            C0383a aVar7 = f3579b;
            aVar6.b(C0383a.a(aVar7, response));
            Response a5 = C0383a.a(aVar7, a3);
            aVar6.c("networkResponse", a5);
            aVar6.h = a5;
            return aVar6.a();
        }
    }
}
