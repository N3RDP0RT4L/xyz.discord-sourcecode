package f0.e0.f;

import b.d.b.a.a;
import b.i.a.f.e.o.f;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import f0.e0.f.d;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.RejectedExecutionException;
import java.util.logging.Level;
/* compiled from: TaskQueue.kt */
/* loaded from: classes3.dex */
public final class c {
    public boolean a;

    /* renamed from: b  reason: collision with root package name */
    public a f3582b;
    public final List<a> c = new ArrayList();
    public boolean d;
    public final d e;
    public final String f;

    public c(d dVar, String str) {
        m.checkParameterIsNotNull(dVar, "taskRunner");
        m.checkParameterIsNotNull(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        this.e = dVar;
        this.f = str;
    }

    public static /* synthetic */ void d(c cVar, a aVar, long j, int i) {
        if ((i & 2) != 0) {
            j = 0;
        }
        cVar.c(aVar, j);
    }

    public final void a() {
        byte[] bArr = f0.e0.c.a;
        synchronized (this.e) {
            if (b()) {
                this.e.e(this);
            }
        }
    }

    public final boolean b() {
        a aVar = this.f3582b;
        if (aVar != null) {
            if (aVar == null) {
                m.throwNpe();
            }
            if (aVar.d) {
                this.d = true;
            }
        }
        boolean z2 = false;
        for (int size = this.c.size() - 1; size >= 0; size--) {
            if (this.c.get(size).d) {
                a aVar2 = this.c.get(size);
                Objects.requireNonNull(d.c);
                if (d.f3583b.isLoggable(Level.FINE)) {
                    f.e(aVar2, this, "canceled");
                }
                this.c.remove(size);
                z2 = true;
            }
        }
        return z2;
    }

    public final void c(a aVar, long j) {
        m.checkParameterIsNotNull(aVar, "task");
        synchronized (this.e) {
            if (!this.a) {
                if (e(aVar, j, false)) {
                    this.e.e(this);
                }
            } else if (aVar.d) {
                Objects.requireNonNull(d.c);
                if (d.f3583b.isLoggable(Level.FINE)) {
                    f.e(aVar, this, "schedule canceled (queue is shutdown)");
                }
            } else {
                Objects.requireNonNull(d.c);
                if (d.f3583b.isLoggable(Level.FINE)) {
                    f.e(aVar, this, "schedule failed (queue is shutdown)");
                }
                throw new RejectedExecutionException();
            }
        }
    }

    public final boolean e(a aVar, long j, boolean z2) {
        String str;
        m.checkParameterIsNotNull(aVar, "task");
        Objects.requireNonNull(aVar);
        m.checkParameterIsNotNull(this, "queue");
        c cVar = aVar.a;
        if (cVar != this) {
            if (cVar == null) {
                aVar.a = this;
            } else {
                throw new IllegalStateException("task is in multiple queues".toString());
            }
        }
        long c = this.e.j.c();
        long j2 = c + j;
        int indexOf = this.c.indexOf(aVar);
        if (indexOf != -1) {
            if (aVar.f3581b <= j2) {
                d.b bVar = d.c;
                if (d.f3583b.isLoggable(Level.FINE)) {
                    f.e(aVar, this, "already scheduled");
                }
                return false;
            }
            this.c.remove(indexOf);
        }
        aVar.f3581b = j2;
        d.b bVar2 = d.c;
        if (d.f3583b.isLoggable(Level.FINE)) {
            if (z2) {
                StringBuilder R = a.R("run again after ");
                R.append(f.a0(j2 - c));
                str = R.toString();
            } else {
                StringBuilder R2 = a.R("scheduled after ");
                R2.append(f.a0(j2 - c));
                str = R2.toString();
            }
            f.e(aVar, this, str);
        }
        Iterator<a> it = this.c.iterator();
        int i = 0;
        while (true) {
            if (!it.hasNext()) {
                i = -1;
                break;
            }
            if (it.next().f3581b - c > j) {
                break;
            }
            i++;
        }
        if (i == -1) {
            i = this.c.size();
        }
        this.c.add(i, aVar);
        return i == 0;
    }

    public final void f() {
        byte[] bArr = f0.e0.c.a;
        synchronized (this.e) {
            this.a = true;
            if (b()) {
                this.e.e(this);
            }
        }
    }

    public String toString() {
        return this.f;
    }
}
