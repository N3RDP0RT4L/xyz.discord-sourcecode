package f0.e0.d;

import d0.t.u;
import d0.z.d.m;
import f0.c;
import f0.s;
import f0.w;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;
import java.util.List;
import kotlin.TypeCastException;
/* compiled from: JavaNetAuthenticator.kt */
/* loaded from: classes3.dex */
public final class b implements c {

    /* renamed from: b  reason: collision with root package name */
    public final s f3578b;

    public b(s sVar, int i) {
        s sVar2 = (i & 1) != 0 ? s.a : null;
        m.checkParameterIsNotNull(sVar2, "defaultDns");
        this.f3578b = sVar2;
    }

    /* JADX WARN: Removed duplicated region for block: B:20:0x006f  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x0083  */
    /* JADX WARN: Removed duplicated region for block: B:61:0x01a6  */
    /* JADX WARN: Removed duplicated region for block: B:62:0x01ac  */
    /* JADX WARN: Removed duplicated region for block: B:65:0x01d8  */
    /* JADX WARN: Removed duplicated region for block: B:67:0x01e7  */
    @Override // f0.c
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public okhttp3.Request a(f0.c0 r21, okhttp3.Response r22) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 511
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: f0.e0.d.b.a(f0.c0, okhttp3.Response):okhttp3.Request");
    }

    public final InetAddress b(Proxy proxy, w wVar, s sVar) throws IOException {
        Proxy.Type type = proxy.type();
        if (type != null && a.a[type.ordinal()] == 1) {
            return (InetAddress) u.first((List<? extends Object>) sVar.a(wVar.g));
        }
        SocketAddress address = proxy.address();
        if (address != null) {
            InetAddress address2 = ((InetSocketAddress) address).getAddress();
            m.checkExpressionValueIsNotNull(address2, "(address() as InetSocketAddress).address");
            return address2;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.net.InetSocketAddress");
    }
}
