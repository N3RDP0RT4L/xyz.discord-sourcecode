package f0.e0.g;

import androidx.browser.trusted.sharing.ShareTarget;
import d0.z.d.m;
import f0.e0.h.g;
import f0.x;
import java.io.IOException;
import java.util.Objects;
import okhttp3.Interceptor;
import okhttp3.Response;
import okhttp3.internal.connection.RouteException;
/* compiled from: ConnectInterceptor.kt */
/* loaded from: classes3.dex */
public final class a implements Interceptor {

    /* renamed from: b  reason: collision with root package name */
    public static final a f3584b = new a();

    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) throws IOException {
        m.checkParameterIsNotNull(chain, "chain");
        g gVar = (g) chain;
        e eVar = gVar.f3600b;
        Objects.requireNonNull(eVar);
        m.checkParameterIsNotNull(gVar, "chain");
        synchronized (eVar) {
            if (!eVar.u) {
                throw new IllegalStateException("released".toString());
            } else if (!(!eVar.t)) {
                throw new IllegalStateException("Check failed.".toString());
            } else if (!(!eVar.f3588s)) {
                throw new IllegalStateException("Check failed.".toString());
            }
        }
        d dVar = eVar.o;
        if (dVar == null) {
            m.throwNpe();
        }
        x xVar = eVar.f3590y;
        Objects.requireNonNull(dVar);
        m.checkParameterIsNotNull(xVar, "client");
        m.checkParameterIsNotNull(gVar, "chain");
        try {
            c cVar = new c(eVar, eVar.k, dVar, dVar.a(gVar.g, gVar.h, gVar.i, xVar.M, xVar.r, !m.areEqual(gVar.f.c, ShareTarget.METHOD_GET)).k(xVar, gVar));
            eVar.r = cVar;
            eVar.w = cVar;
            synchronized (eVar) {
                eVar.f3588s = true;
                eVar.t = true;
            }
            if (!eVar.v) {
                return g.d(gVar, 0, cVar, null, 0, 0, 0, 61).a(gVar.f);
            }
            throw new IOException("Canceled");
        } catch (IOException e) {
            dVar.c(e);
            throw new RouteException(e);
        } catch (RouteException e2) {
            dVar.c(e2.c());
            throw e2;
        }
    }
}
