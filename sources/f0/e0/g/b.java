package f0.e0.g;

import b.d.b.a.a;
import d0.t.k;
import f0.e0.c;
import f0.j;
import f0.m;
import java.io.IOException;
import java.net.UnknownServiceException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import javax.net.ssl.SSLSocket;
/* compiled from: ConnectionSpecSelector.kt */
/* loaded from: classes3.dex */
public final class b {
    public int a;

    /* renamed from: b  reason: collision with root package name */
    public boolean f3585b;
    public boolean c;
    public final List<m> d;

    public b(List<m> list) {
        d0.z.d.m.checkParameterIsNotNull(list, "connectionSpecs");
        this.d = list;
    }

    public final m a(SSLSocket sSLSocket) throws IOException {
        m mVar;
        boolean z2;
        String[] strArr;
        String[] strArr2;
        d0.z.d.m.checkParameterIsNotNull(sSLSocket, "sslSocket");
        int i = this.a;
        int size = this.d.size();
        while (true) {
            if (i >= size) {
                mVar = null;
                break;
            }
            mVar = this.d.get(i);
            if (mVar.b(sSLSocket)) {
                this.a = i + 1;
                break;
            }
            i++;
        }
        if (mVar == null) {
            StringBuilder R = a.R("Unable to find acceptable protocols. isFallback=");
            R.append(this.c);
            R.append(',');
            R.append(" modes=");
            R.append(this.d);
            R.append(',');
            R.append(" supported protocols=");
            String[] enabledProtocols = sSLSocket.getEnabledProtocols();
            if (enabledProtocols == null) {
                d0.z.d.m.throwNpe();
            }
            String arrays = Arrays.toString(enabledProtocols);
            d0.z.d.m.checkExpressionValueIsNotNull(arrays, "java.util.Arrays.toString(this)");
            R.append(arrays);
            throw new UnknownServiceException(R.toString());
        }
        int i2 = this.a;
        int size2 = this.d.size();
        while (true) {
            if (i2 >= size2) {
                z2 = false;
                break;
            } else if (this.d.get(i2).b(sSLSocket)) {
                z2 = true;
                break;
            } else {
                i2++;
            }
        }
        this.f3585b = z2;
        boolean z3 = this.c;
        d0.z.d.m.checkParameterIsNotNull(sSLSocket, "sslSocket");
        if (mVar.g != null) {
            String[] enabledCipherSuites = sSLSocket.getEnabledCipherSuites();
            d0.z.d.m.checkExpressionValueIsNotNull(enabledCipherSuites, "sslSocket.enabledCipherSuites");
            String[] strArr3 = mVar.g;
            j.b bVar = j.f3645s;
            Comparator<String> comparator = j.a;
            strArr = c.q(enabledCipherSuites, strArr3, j.a);
        } else {
            strArr = sSLSocket.getEnabledCipherSuites();
        }
        if (mVar.h != null) {
            String[] enabledProtocols2 = sSLSocket.getEnabledProtocols();
            d0.z.d.m.checkExpressionValueIsNotNull(enabledProtocols2, "sslSocket.enabledProtocols");
            strArr2 = c.q(enabledProtocols2, mVar.h, d0.u.a.naturalOrder());
        } else {
            strArr2 = sSLSocket.getEnabledProtocols();
        }
        String[] supportedCipherSuites = sSLSocket.getSupportedCipherSuites();
        d0.z.d.m.checkExpressionValueIsNotNull(supportedCipherSuites, "supportedCipherSuites");
        j.b bVar2 = j.f3645s;
        Comparator<String> comparator2 = j.a;
        Comparator<String> comparator3 = j.a;
        byte[] bArr = c.a;
        d0.z.d.m.checkParameterIsNotNull(supportedCipherSuites, "$this$indexOf");
        d0.z.d.m.checkParameterIsNotNull("TLS_FALLBACK_SCSV", "value");
        d0.z.d.m.checkParameterIsNotNull(comparator3, "comparator");
        int length = supportedCipherSuites.length;
        int i3 = 0;
        while (true) {
            if (i3 >= length) {
                i3 = -1;
                break;
            }
            if (((j.a) comparator3).compare(supportedCipherSuites[i3], "TLS_FALLBACK_SCSV") == 0) {
                break;
            }
            i3++;
        }
        if (z3 && i3 != -1) {
            d0.z.d.m.checkExpressionValueIsNotNull(strArr, "cipherSuitesIntersection");
            String str = supportedCipherSuites[i3];
            d0.z.d.m.checkExpressionValueIsNotNull(str, "supportedCipherSuites[indexOfFallbackScsv]");
            d0.z.d.m.checkParameterIsNotNull(strArr, "$this$concat");
            d0.z.d.m.checkParameterIsNotNull(str, "value");
            Object[] copyOf = Arrays.copyOf(strArr, strArr.length + 1);
            d0.z.d.m.checkExpressionValueIsNotNull(copyOf, "java.util.Arrays.copyOf(this, newSize)");
            strArr = (String[]) copyOf;
            strArr[k.getLastIndex(strArr)] = str;
        }
        m.a aVar = new m.a(mVar);
        d0.z.d.m.checkExpressionValueIsNotNull(strArr, "cipherSuitesIntersection");
        aVar.b((String[]) Arrays.copyOf(strArr, strArr.length));
        d0.z.d.m.checkExpressionValueIsNotNull(strArr2, "tlsVersionsIntersection");
        aVar.e((String[]) Arrays.copyOf(strArr2, strArr2.length));
        m a = aVar.a();
        if (a.c() != null) {
            sSLSocket.setEnabledProtocols(a.h);
        }
        if (a.a() != null) {
            sSLSocket.setEnabledCipherSuites(a.g);
        }
        return mVar;
    }
}
