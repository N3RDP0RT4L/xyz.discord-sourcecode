package f0.e0.g;

import androidx.core.app.NotificationCompat;
import d0.z.d.m;
import f0.e0.k.h;
import f0.f;
import f0.q;
import f0.t;
import f0.x;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.net.Socket;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import okhttp3.Request;
import okhttp3.Response;
/* compiled from: RealCall.kt */
/* loaded from: classes3.dex */
public final class e implements f0.e {
    public final boolean A;
    public final k j;
    public final t k;
    public final c l;
    public Object n;
    public d o;
    public j p;
    public boolean q;
    public f0.e0.g.c r;

    /* renamed from: s  reason: collision with root package name */
    public boolean f3588s;
    public boolean t;
    public volatile boolean v;
    public volatile f0.e0.g.c w;

    /* renamed from: x  reason: collision with root package name */
    public volatile j f3589x;

    /* renamed from: y  reason: collision with root package name */
    public final x f3590y;

    /* renamed from: z  reason: collision with root package name */
    public final Request f3591z;
    public final AtomicBoolean m = new AtomicBoolean();
    public boolean u = true;

    /* compiled from: RealCall.kt */
    /* loaded from: classes3.dex */
    public final class a implements Runnable {
        public volatile AtomicInteger j = new AtomicInteger(0);
        public final f k;
        public final /* synthetic */ e l;

        public a(e eVar, f fVar) {
            m.checkParameterIsNotNull(fVar, "responseCallback");
            this.l = eVar;
            this.k = fVar;
        }

        public final String a() {
            return this.l.f3591z.f3784b.g;
        }

        @Override // java.lang.Runnable
        public void run() {
            IOException e;
            e eVar;
            Throwable th;
            StringBuilder R = b.d.b.a.a.R("OkHttp ");
            R.append(this.l.f3591z.f3784b.h());
            String sb = R.toString();
            Thread currentThread = Thread.currentThread();
            m.checkExpressionValueIsNotNull(currentThread, "currentThread");
            String name = currentThread.getName();
            currentThread.setName(sb);
            try {
                this.l.l.i();
                boolean z2 = false;
                try {
                    try {
                        this.k.a(this.l, this.l.j());
                        eVar = this.l;
                    } catch (IOException e2) {
                        e = e2;
                        z2 = true;
                        if (z2) {
                            h.a aVar = h.c;
                            h hVar = h.a;
                            hVar.i("Callback failure for " + e.b(this.l), 4, e);
                        } else {
                            this.k.b(this.l, e);
                        }
                        eVar = this.l;
                        eVar.f3590y.m.c(this);
                    } catch (Throwable th2) {
                        th = th2;
                        z2 = true;
                        this.l.cancel();
                        if (!z2) {
                            IOException iOException = new IOException("canceled due to " + th);
                            iOException.addSuppressed(th);
                            this.k.b(this.l, iOException);
                        }
                        throw th;
                    }
                } catch (IOException e3) {
                    e = e3;
                } catch (Throwable th3) {
                    th = th3;
                }
                eVar.f3590y.m.c(this);
            } finally {
                currentThread.setName(name);
            }
        }
    }

    /* compiled from: RealCall.kt */
    /* loaded from: classes3.dex */
    public static final class b extends WeakReference<e> {
        public final Object a;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(e eVar, Object obj) {
            super(eVar);
            m.checkParameterIsNotNull(eVar, "referent");
            this.a = obj;
        }
    }

    /* compiled from: RealCall.kt */
    /* loaded from: classes3.dex */
    public static final class c extends g0.b {
        public c() {
        }

        @Override // g0.b
        public void l() {
            e.this.cancel();
        }
    }

    public e(x xVar, Request request, boolean z2) {
        m.checkParameterIsNotNull(xVar, "client");
        m.checkParameterIsNotNull(request, "originalRequest");
        this.f3590y = xVar;
        this.f3591z = request;
        this.A = z2;
        this.j = xVar.n.a;
        this.k = xVar.q.a(this);
        c cVar = new c();
        cVar.g(xVar.I, TimeUnit.MILLISECONDS);
        this.l = cVar;
    }

    public static final String b(e eVar) {
        StringBuilder sb = new StringBuilder();
        sb.append(eVar.v ? "canceled " : "");
        sb.append(eVar.A ? "web socket" : NotificationCompat.CATEGORY_CALL);
        sb.append(" to ");
        sb.append(eVar.f3591z.f3784b.h());
        return sb.toString();
    }

    @Override // f0.e
    public Request c() {
        return this.f3591z;
    }

    @Override // f0.e
    public void cancel() {
        Socket socket;
        if (!this.v) {
            this.v = true;
            f0.e0.g.c cVar = this.w;
            if (cVar != null) {
                cVar.f.cancel();
            }
            j jVar = this.f3589x;
            if (!(jVar == null || (socket = jVar.f3592b) == null)) {
                f0.e0.c.e(socket);
            }
            Objects.requireNonNull(this.k);
            m.checkParameterIsNotNull(this, NotificationCompat.CATEGORY_CALL);
        }
    }

    public Object clone() {
        return new e(this.f3590y, this.f3591z, this.A);
    }

    @Override // f0.e
    public boolean d() {
        return this.v;
    }

    @Override // f0.e
    public void e(f fVar) {
        a aVar;
        m.checkParameterIsNotNull(fVar, "responseCallback");
        if (this.m.compareAndSet(false, true)) {
            h();
            q qVar = this.f3590y.m;
            a aVar2 = new a(this, fVar);
            Objects.requireNonNull(qVar);
            m.checkParameterIsNotNull(aVar2, NotificationCompat.CATEGORY_CALL);
            synchronized (qVar) {
                qVar.f3649b.add(aVar2);
                if (!aVar2.l.A) {
                    String a2 = aVar2.a();
                    Iterator<a> it = qVar.c.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            Iterator<a> it2 = qVar.f3649b.iterator();
                            while (true) {
                                if (!it2.hasNext()) {
                                    aVar = null;
                                    break;
                                }
                                aVar = it2.next();
                                if (m.areEqual(aVar.a(), a2)) {
                                    break;
                                }
                            }
                        } else {
                            aVar = it.next();
                            if (m.areEqual(aVar.a(), a2)) {
                                break;
                            }
                        }
                    }
                    if (aVar != null) {
                        m.checkParameterIsNotNull(aVar, "other");
                        aVar2.j = aVar.j;
                    }
                }
            }
            qVar.d();
            return;
        }
        throw new IllegalStateException("Already Executed".toString());
    }

    @Override // f0.e
    public Response execute() {
        if (this.m.compareAndSet(false, true)) {
            this.l.i();
            h();
            try {
                q qVar = this.f3590y.m;
                synchronized (qVar) {
                    m.checkParameterIsNotNull(this, NotificationCompat.CATEGORY_CALL);
                    qVar.d.add(this);
                }
                return j();
            } finally {
                q qVar2 = this.f3590y.m;
                Objects.requireNonNull(qVar2);
                m.checkParameterIsNotNull(this, NotificationCompat.CATEGORY_CALL);
                qVar2.b(qVar2.d, this);
            }
        } else {
            throw new IllegalStateException("Already Executed".toString());
        }
    }

    public final void f(j jVar) {
        m.checkParameterIsNotNull(jVar, "connection");
        byte[] bArr = f0.e0.c.a;
        if (this.p == null) {
            this.p = jVar;
            jVar.o.add(new b(this, this.n));
            return;
        }
        throw new IllegalStateException("Check failed.".toString());
    }

    public final <E extends IOException> E g(E e) {
        InterruptedIOException interruptedIOException;
        Socket m;
        byte[] bArr = f0.e0.c.a;
        j jVar = this.p;
        if (jVar != null) {
            synchronized (jVar) {
                m = m();
            }
            if (this.p == null) {
                if (m != null) {
                    f0.e0.c.e(m);
                }
                Objects.requireNonNull(this.k);
                m.checkParameterIsNotNull(this, NotificationCompat.CATEGORY_CALL);
                m.checkParameterIsNotNull(jVar, "connection");
            } else {
                if (!(m == null)) {
                    throw new IllegalStateException("Check failed.".toString());
                }
            }
        }
        if (!this.q && this.l.j()) {
            InterruptedIOException interruptedIOException2 = new InterruptedIOException("timeout");
            interruptedIOException = interruptedIOException2;
            if (e != null) {
                interruptedIOException2.initCause(e);
                interruptedIOException = interruptedIOException2;
            }
        } else {
            interruptedIOException = e;
        }
        if (e != null) {
            t tVar = this.k;
            if (interruptedIOException == null) {
                m.throwNpe();
            }
            Objects.requireNonNull(tVar);
            m.checkParameterIsNotNull(this, NotificationCompat.CATEGORY_CALL);
            m.checkParameterIsNotNull(interruptedIOException, "ioe");
        } else {
            Objects.requireNonNull(this.k);
            m.checkParameterIsNotNull(this, NotificationCompat.CATEGORY_CALL);
        }
        return interruptedIOException;
    }

    public final void h() {
        h.a aVar = h.c;
        this.n = h.a.g("response.body().close()");
        Objects.requireNonNull(this.k);
        m.checkParameterIsNotNull(this, NotificationCompat.CATEGORY_CALL);
    }

    public final void i(boolean z2) {
        f0.e0.g.c cVar;
        synchronized (this) {
            if (!this.u) {
                throw new IllegalStateException("released".toString());
            }
        }
        if (z2 && (cVar = this.w) != null) {
            cVar.f.cancel();
            cVar.c.k(cVar, true, true, null);
        }
        this.r = null;
    }

    /* JADX WARN: Removed duplicated region for block: B:26:0x0095  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final okhttp3.Response j() throws java.io.IOException {
        /*
            r10 = this;
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            f0.x r0 = r10.f3590y
            java.util.List<okhttp3.Interceptor> r0 = r0.o
            d0.t.r.addAll(r2, r0)
            f0.e0.h.i r0 = new f0.e0.h.i
            f0.x r1 = r10.f3590y
            r0.<init>(r1)
            r2.add(r0)
            f0.e0.h.a r0 = new f0.e0.h.a
            f0.x r1 = r10.f3590y
            f0.p r1 = r1.v
            r0.<init>(r1)
            r2.add(r0)
            f0.e0.e.a r0 = new f0.e0.e.a
            f0.x r1 = r10.f3590y
            java.util.Objects.requireNonNull(r1)
            r0.<init>()
            r2.add(r0)
            f0.e0.g.a r0 = f0.e0.g.a.f3584b
            r2.add(r0)
            boolean r0 = r10.A
            if (r0 != 0) goto L3f
            f0.x r0 = r10.f3590y
            java.util.List<okhttp3.Interceptor> r0 = r0.p
            d0.t.r.addAll(r2, r0)
        L3f:
            f0.e0.h.b r0 = new f0.e0.h.b
            boolean r1 = r10.A
            r0.<init>(r1)
            r2.add(r0)
            f0.e0.h.g r9 = new f0.e0.h.g
            r3 = 0
            r4 = 0
            okhttp3.Request r5 = r10.f3591z
            f0.x r0 = r10.f3590y
            int r6 = r0.J
            int r7 = r0.K
            int r8 = r0.L
            r0 = r9
            r1 = r10
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            r0 = 0
            r1 = 0
            okhttp3.Request r2 = r10.f3591z     // Catch: java.lang.Throwable -> L7e java.io.IOException -> L80
            okhttp3.Response r2 = r9.a(r2)     // Catch: java.lang.Throwable -> L7e java.io.IOException -> L80
            boolean r3 = r10.v     // Catch: java.lang.Throwable -> L7e java.io.IOException -> L80
            if (r3 != 0) goto L6c
            r10.l(r1)
            return r2
        L6c:
            java.lang.String r3 = "$this$closeQuietly"
            d0.z.d.m.checkParameterIsNotNull(r2, r3)     // Catch: java.lang.Throwable -> L7e java.io.IOException -> L80
            r2.close()     // Catch: java.lang.Exception -> L74 java.lang.RuntimeException -> L7c java.lang.Throwable -> L7e
        L74:
            java.io.IOException r2 = new java.io.IOException     // Catch: java.lang.Throwable -> L7e java.io.IOException -> L80
            java.lang.String r3 = "Canceled"
            r2.<init>(r3)     // Catch: java.lang.Throwable -> L7e java.io.IOException -> L80
            throw r2     // Catch: java.lang.Throwable -> L7e java.io.IOException -> L80
        L7c:
            r2 = move-exception
            throw r2     // Catch: java.lang.Throwable -> L7e java.io.IOException -> L80
        L7e:
            r2 = move-exception
            goto L93
        L80:
            r0 = move-exception
            java.io.IOException r0 = r10.l(r0)     // Catch: java.lang.Throwable -> L90
            if (r0 != 0) goto L8f
            kotlin.TypeCastException r0 = new kotlin.TypeCastException     // Catch: java.lang.Throwable -> L90
            java.lang.String r2 = "null cannot be cast to non-null type kotlin.Throwable"
            r0.<init>(r2)     // Catch: java.lang.Throwable -> L90
            throw r0     // Catch: java.lang.Throwable -> L90
        L8f:
            throw r0     // Catch: java.lang.Throwable -> L90
        L90:
            r0 = move-exception
            r2 = r0
            r0 = 1
        L93:
            if (r0 != 0) goto L98
            r10.l(r1)
        L98:
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: f0.e0.g.e.j():okhttp3.Response");
    }

    /* JADX WARN: Removed duplicated region for block: B:17:0x0023 A[Catch: all -> 0x0019, TryCatch #1 {all -> 0x0019, blocks: (B:8:0x0014, B:14:0x001d, B:17:0x0023, B:19:0x0027, B:20:0x0029, B:22:0x002d, B:27:0x0036, B:29:0x003a), top: B:50:0x0014 }] */
    /* JADX WARN: Removed duplicated region for block: B:19:0x0027 A[Catch: all -> 0x0019, TryCatch #1 {all -> 0x0019, blocks: (B:8:0x0014, B:14:0x001d, B:17:0x0023, B:19:0x0027, B:20:0x0029, B:22:0x002d, B:27:0x0036, B:29:0x003a), top: B:50:0x0014 }] */
    /* JADX WARN: Removed duplicated region for block: B:31:0x003e  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final <E extends java.io.IOException> E k(f0.e0.g.c r3, boolean r4, boolean r5, E r6) {
        /*
            r2 = this;
            java.lang.String r0 = "exchange"
            d0.z.d.m.checkParameterIsNotNull(r3, r0)
            f0.e0.g.c r0 = r2.w
            boolean r3 = d0.z.d.m.areEqual(r3, r0)
            r0 = 1
            r3 = r3 ^ r0
            if (r3 == 0) goto L10
            return r6
        L10:
            monitor-enter(r2)
            r3 = 0
            if (r4 == 0) goto L1b
            boolean r1 = r2.f3588s     // Catch: java.lang.Throwable -> L19
            if (r1 != 0) goto L21
            goto L1b
        L19:
            r3 = move-exception
            goto L42
        L1b:
            if (r5 == 0) goto L44
            boolean r1 = r2.t     // Catch: java.lang.Throwable -> L19
            if (r1 == 0) goto L44
        L21:
            if (r4 == 0) goto L25
            r2.f3588s = r3     // Catch: java.lang.Throwable -> L19
        L25:
            if (r5 == 0) goto L29
            r2.t = r3     // Catch: java.lang.Throwable -> L19
        L29:
            boolean r4 = r2.f3588s     // Catch: java.lang.Throwable -> L19
            if (r4 != 0) goto L33
            boolean r5 = r2.t     // Catch: java.lang.Throwable -> L19
            if (r5 != 0) goto L33
            r5 = 1
            goto L34
        L33:
            r5 = 0
        L34:
            if (r4 != 0) goto L3f
            boolean r4 = r2.t     // Catch: java.lang.Throwable -> L19
            if (r4 != 0) goto L3f
            boolean r4 = r2.u     // Catch: java.lang.Throwable -> L19
            if (r4 != 0) goto L3f
            r3 = 1
        L3f:
            r4 = r3
            r3 = r5
            goto L45
        L42:
            monitor-exit(r2)
            throw r3
        L44:
            r4 = 0
        L45:
            monitor-exit(r2)
            if (r3 == 0) goto L5a
            r3 = 0
            r2.w = r3
            f0.e0.g.j r3 = r2.p
            if (r3 == 0) goto L5a
            monitor-enter(r3)
            int r5 = r3.l     // Catch: java.lang.Throwable -> L57
            int r5 = r5 + r0
            r3.l = r5     // Catch: java.lang.Throwable -> L57
            monitor-exit(r3)
            goto L5a
        L57:
            r4 = move-exception
            monitor-exit(r3)
            throw r4
        L5a:
            if (r4 == 0) goto L61
            java.io.IOException r3 = r2.g(r6)
            return r3
        L61:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: f0.e0.g.e.k(f0.e0.g.c, boolean, boolean, java.io.IOException):java.io.IOException");
    }

    public final IOException l(IOException iOException) {
        boolean z2;
        synchronized (this) {
            z2 = false;
            if (this.u) {
                this.u = false;
                if (!this.f3588s) {
                    if (!this.t) {
                        z2 = true;
                    }
                }
            }
        }
        return z2 ? g(iOException) : iOException;
    }

    public final Socket m() {
        j jVar = this.p;
        if (jVar == null) {
            m.throwNpe();
        }
        byte[] bArr = f0.e0.c.a;
        List<Reference<e>> list = jVar.o;
        Iterator<Reference<e>> it = list.iterator();
        boolean z2 = false;
        int i = 0;
        while (true) {
            if (!it.hasNext()) {
                i = -1;
                break;
            } else if (m.areEqual(it.next().get(), this)) {
                break;
            } else {
                i++;
            }
        }
        if (i != -1) {
            list.remove(i);
            this.p = null;
            if (list.isEmpty()) {
                jVar.p = System.nanoTime();
                k kVar = this.j;
                Objects.requireNonNull(kVar);
                m.checkParameterIsNotNull(jVar, "connection");
                byte[] bArr2 = f0.e0.c.a;
                if (jVar.i || kVar.e == 0) {
                    jVar.i = true;
                    kVar.d.remove(jVar);
                    if (kVar.d.isEmpty()) {
                        kVar.f3593b.a();
                    }
                    z2 = true;
                } else {
                    f0.e0.f.c.d(kVar.f3593b, kVar.c, 0L, 2);
                }
                if (z2) {
                    return jVar.n();
                }
            }
            return null;
        }
        throw new IllegalStateException("Check failed.".toString());
    }

    public final void n() {
        if (!this.q) {
            this.q = true;
            this.l.j();
            return;
        }
        throw new IllegalStateException("Check failed.".toString());
    }
}
