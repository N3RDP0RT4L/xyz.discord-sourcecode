package f0.e0.g;

import androidx.core.app.NotificationCompat;
import f0.a;
import f0.c0;
import f0.e0.g.m;
import f0.t;
import f0.w;
import java.io.IOException;
import okhttp3.internal.http2.ConnectionShutdownException;
import okhttp3.internal.http2.StreamResetException;
/* compiled from: ExchangeFinder.kt */
/* loaded from: classes3.dex */
public final class d {
    public m.a a;

    /* renamed from: b  reason: collision with root package name */
    public m f3587b;
    public int c;
    public int d;
    public int e;
    public c0 f;
    public final k g;
    public final a h;
    public final e i;
    public final t j;

    public d(k kVar, a aVar, e eVar, t tVar) {
        d0.z.d.m.checkParameterIsNotNull(kVar, "connectionPool");
        d0.z.d.m.checkParameterIsNotNull(aVar, "address");
        d0.z.d.m.checkParameterIsNotNull(eVar, NotificationCompat.CATEGORY_CALL);
        d0.z.d.m.checkParameterIsNotNull(tVar, "eventListener");
        this.g = kVar;
        this.h = aVar;
        this.i = eVar;
        this.j = tVar;
    }

    /* JADX WARN: Finally extract failed */
    /* JADX WARN: Removed duplicated region for block: B:134:0x0322  */
    /* JADX WARN: Removed duplicated region for block: B:160:0x02b2 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:171:0x0321 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final f0.e0.g.j a(int r16, int r17, int r18, int r19, boolean r20, boolean r21) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 879
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: f0.e0.g.d.a(int, int, int, int, boolean, boolean):f0.e0.g.j");
    }

    public final boolean b(w wVar) {
        d0.z.d.m.checkParameterIsNotNull(wVar, "url");
        w wVar2 = this.h.a;
        return wVar.h == wVar2.h && d0.z.d.m.areEqual(wVar.g, wVar2.g);
    }

    public final void c(IOException iOException) {
        d0.z.d.m.checkParameterIsNotNull(iOException, "e");
        this.f = null;
        if ((iOException instanceof StreamResetException) && ((StreamResetException) iOException).errorCode == f0.e0.j.a.REFUSED_STREAM) {
            this.c++;
        } else if (iOException instanceof ConnectionShutdownException) {
            this.d++;
        } else {
            this.e++;
        }
    }
}
