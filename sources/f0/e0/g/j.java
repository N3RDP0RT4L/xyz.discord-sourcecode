package f0.e0.g;

import androidx.core.app.NotificationCompat;
import androidx.recyclerview.widget.RecyclerView;
import b.i.a.f.e.o.f;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.z.d.m;
import f0.c0;
import f0.e0.c;
import f0.e0.h.d;
import f0.e0.i.b;
import f0.e0.j.a;
import f0.e0.j.e;
import f0.e0.j.l;
import f0.e0.j.n;
import f0.e0.j.o;
import f0.e0.j.s;
import f0.e0.k.h;
import f0.k;
import f0.t;
import f0.v;
import f0.x;
import f0.y;
import g0.g;
import g0.q;
import g0.r;
import java.io.IOException;
import java.lang.ref.Reference;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import okio.BufferedSink;
/* compiled from: RealConnection.kt */
/* loaded from: classes3.dex */
public final class j extends e.c implements k {

    /* renamed from: b  reason: collision with root package name */
    public Socket f3592b;
    public Socket c;
    public v d;
    public y e;
    public e f;
    public g g;
    public BufferedSink h;
    public boolean i;
    public boolean j;
    public int k;
    public int l;
    public int m;
    public int n = 1;
    public final List<Reference<e>> o = new ArrayList();
    public long p = RecyclerView.FOREVER_NS;
    public final c0 q;

    public j(k kVar, c0 c0Var) {
        m.checkParameterIsNotNull(kVar, "connectionPool");
        m.checkParameterIsNotNull(c0Var, "route");
        this.q = c0Var;
    }

    @Override // f0.e0.j.e.c
    public synchronized void a(e eVar, s sVar) {
        m.checkParameterIsNotNull(eVar, "connection");
        m.checkParameterIsNotNull(sVar, "settings");
        this.n = (sVar.a & 16) != 0 ? sVar.f3619b[4] : Integer.MAX_VALUE;
    }

    @Override // f0.e0.j.e.c
    public void b(n nVar) throws IOException {
        m.checkParameterIsNotNull(nVar, "stream");
        nVar.c(a.REFUSED_STREAM, null);
    }

    /* JADX WARN: Removed duplicated region for block: B:44:0x00e8  */
    /* JADX WARN: Removed duplicated region for block: B:47:0x00ef  */
    /* JADX WARN: Removed duplicated region for block: B:50:0x011c  */
    /* JADX WARN: Removed duplicated region for block: B:51:0x0122  */
    /* JADX WARN: Removed duplicated region for block: B:53:0x0127  */
    /* JADX WARN: Removed duplicated region for block: B:83:0x015b A[EDGE_INSN: B:83:0x015b->B:70:0x015b ?: BREAK  , SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void c(int r17, int r18, int r19, int r20, boolean r21, f0.e r22, f0.t r23) {
        /*
            Method dump skipped, instructions count: 373
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: f0.e0.g.j.c(int, int, int, int, boolean, f0.e, f0.t):void");
    }

    public final void d(x xVar, c0 c0Var, IOException iOException) {
        m.checkParameterIsNotNull(xVar, "client");
        m.checkParameterIsNotNull(c0Var, "failedRoute");
        m.checkParameterIsNotNull(iOException, "failure");
        if (c0Var.f3575b.type() != Proxy.Type.DIRECT) {
            f0.a aVar = c0Var.a;
            aVar.k.connectFailed(aVar.a.i(), c0Var.f3575b.address(), iOException);
        }
        l lVar = xVar.O;
        synchronized (lVar) {
            m.checkParameterIsNotNull(c0Var, "failedRoute");
            lVar.a.add(c0Var);
        }
    }

    public final void e(int i, int i2, f0.e eVar, t tVar) throws IOException {
        Socket socket;
        int i3;
        c0 c0Var = this.q;
        Proxy proxy = c0Var.f3575b;
        f0.a aVar = c0Var.a;
        Proxy.Type type = proxy.type();
        if (type != null && ((i3 = f.a[type.ordinal()]) == 1 || i3 == 2)) {
            socket = aVar.e.createSocket();
            if (socket == null) {
                m.throwNpe();
            }
        } else {
            socket = new Socket(proxy);
        }
        this.f3592b = socket;
        InetSocketAddress inetSocketAddress = this.q.c;
        Objects.requireNonNull(tVar);
        m.checkParameterIsNotNull(eVar, NotificationCompat.CATEGORY_CALL);
        m.checkParameterIsNotNull(inetSocketAddress, "inetSocketAddress");
        m.checkParameterIsNotNull(proxy, "proxy");
        socket.setSoTimeout(i2);
        try {
            h.a aVar2 = h.c;
            h.a.e(socket, this.q.c, i);
            try {
                g0.x d1 = f.d1(socket);
                m.checkParameterIsNotNull(d1, "$this$buffer");
                this.g = new r(d1);
                g0.v b1 = f.b1(socket);
                m.checkParameterIsNotNull(b1, "$this$buffer");
                this.h = new q(b1);
            } catch (NullPointerException e) {
                if (m.areEqual(e.getMessage(), "throw with null exception")) {
                    throw new IOException(e);
                }
            }
        } catch (ConnectException e2) {
            StringBuilder R = b.d.b.a.a.R("Failed to connect to ");
            R.append(this.q.c);
            ConnectException connectException = new ConnectException(R.toString());
            connectException.initCause(e2);
            throw connectException;
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:40:0x0184, code lost:
        if (r3 == null) goto L49;
     */
    /* JADX WARN: Code restructure failed: missing block: B:41:0x0186, code lost:
        r5 = r19.f3592b;
     */
    /* JADX WARN: Code restructure failed: missing block: B:42:0x0188, code lost:
        if (r5 == null) goto L55;
     */
    /* JADX WARN: Code restructure failed: missing block: B:43:0x018a, code lost:
        f0.e0.c.e(r5);
     */
    /* JADX WARN: Code restructure failed: missing block: B:44:0x018d, code lost:
        r5 = null;
        r19.f3592b = null;
        r19.h = null;
        r19.g = null;
        r6 = r19.q;
        r8 = r6.c;
        r6 = r6.f3575b;
        java.util.Objects.requireNonNull(r24);
        d0.z.d.m.checkParameterIsNotNull(r23, androidx.core.app.NotificationCompat.CATEGORY_CALL);
        d0.z.d.m.checkParameterIsNotNull(r8, "inetSocketAddress");
        d0.z.d.m.checkParameterIsNotNull(r6, "proxy");
        r7 = r7 + 1;
        r6 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:47:0x01b9, code lost:
        return;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void f(int r20, int r21, int r22, f0.e r23, f0.t r24) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 442
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: f0.e0.g.j.f(int, int, int, f0.e, f0.t):void");
    }

    /* JADX WARN: Removed duplicated region for block: B:55:0x019f  */
    /* JADX WARN: Removed duplicated region for block: B:57:0x01a8  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void g(f0.e0.g.b r12, int r13, f0.e r14, f0.t r15) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 428
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: f0.e0.g.j.g(f0.e0.g.b, int, f0.e, f0.t):void");
    }

    /* JADX WARN: Code restructure failed: missing block: B:54:0x00d4, code lost:
        if (r8 == false) goto L56;
     */
    /* JADX WARN: Removed duplicated region for block: B:58:0x00db A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:69:0x00dc A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final boolean h(f0.a r7, java.util.List<f0.c0> r8) {
        /*
            Method dump skipped, instructions count: 265
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: f0.e0.g.j.h(f0.a, java.util.List):boolean");
    }

    /* JADX WARN: Finally extract failed */
    public final boolean i(boolean z2) {
        long j;
        byte[] bArr = c.a;
        long nanoTime = System.nanoTime();
        Socket socket = this.f3592b;
        if (socket == null) {
            m.throwNpe();
        }
        Socket socket2 = this.c;
        if (socket2 == null) {
            m.throwNpe();
        }
        g gVar = this.g;
        if (gVar == null) {
            m.throwNpe();
        }
        if (socket.isClosed() || socket2.isClosed() || socket2.isInputShutdown() || socket2.isOutputShutdown()) {
            return false;
        }
        e eVar = this.f;
        if (eVar != null) {
            synchronized (eVar) {
                if (eVar.r) {
                    return false;
                }
                if (eVar.A < eVar.f3613z) {
                    if (nanoTime >= eVar.C) {
                        return false;
                    }
                }
                return true;
            }
        }
        synchronized (this) {
            j = nanoTime - this.p;
        }
        if (j < 10000000000L || !z2) {
            return true;
        }
        m.checkParameterIsNotNull(socket2, "$this$isHealthy");
        m.checkParameterIsNotNull(gVar, "source");
        try {
            int soTimeout = socket2.getSoTimeout();
            try {
                socket2.setSoTimeout(1);
                boolean z3 = !gVar.w();
                socket2.setSoTimeout(soTimeout);
                return z3;
            } catch (Throwable th) {
                socket2.setSoTimeout(soTimeout);
                throw th;
            }
        } catch (SocketTimeoutException unused) {
            return true;
        } catch (IOException unused2) {
            return false;
        }
    }

    public final boolean j() {
        return this.f != null;
    }

    public final d k(x xVar, f0.e0.h.g gVar) throws SocketException {
        m.checkParameterIsNotNull(xVar, "client");
        m.checkParameterIsNotNull(gVar, "chain");
        Socket socket = this.c;
        if (socket == null) {
            m.throwNpe();
        }
        g gVar2 = this.g;
        if (gVar2 == null) {
            m.throwNpe();
        }
        BufferedSink bufferedSink = this.h;
        if (bufferedSink == null) {
            m.throwNpe();
        }
        e eVar = this.f;
        if (eVar != null) {
            return new l(xVar, this, gVar, eVar);
        }
        socket.setSoTimeout(gVar.h);
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        gVar2.timeout().g(gVar.h, timeUnit);
        bufferedSink.timeout().g(gVar.i, timeUnit);
        return new b(xVar, this, gVar2, bufferedSink);
    }

    public final synchronized void l() {
        this.i = true;
    }

    public y m() {
        y yVar = this.e;
        if (yVar == null) {
            m.throwNpe();
        }
        return yVar;
    }

    public Socket n() {
        Socket socket = this.c;
        if (socket == null) {
            m.throwNpe();
        }
        return socket;
    }

    public final void o(int i) throws IOException {
        String str;
        int a;
        Socket socket = this.c;
        if (socket == null) {
            m.throwNpe();
        }
        g gVar = this.g;
        if (gVar == null) {
            m.throwNpe();
        }
        BufferedSink bufferedSink = this.h;
        if (bufferedSink == null) {
            m.throwNpe();
        }
        socket.setSoTimeout(0);
        f0.e0.f.d dVar = f0.e0.f.d.a;
        e.b bVar = new e.b(true, dVar);
        String str2 = this.q.a.a.g;
        m.checkParameterIsNotNull(socket, "socket");
        m.checkParameterIsNotNull(str2, "peerName");
        m.checkParameterIsNotNull(gVar, "source");
        m.checkParameterIsNotNull(bufferedSink, "sink");
        bVar.a = socket;
        if (bVar.h) {
            str = c.g + ' ' + str2;
        } else {
            str = b.d.b.a.a.v("MockWebServer ", str2);
        }
        bVar.f3614b = str;
        bVar.c = gVar;
        bVar.d = bufferedSink;
        m.checkParameterIsNotNull(this, "listener");
        bVar.e = this;
        bVar.g = i;
        e eVar = new e(bVar);
        this.f = eVar;
        e eVar2 = e.k;
        s sVar = e.j;
        this.n = (sVar.a & 16) != 0 ? sVar.f3619b[4] : Integer.MAX_VALUE;
        m.checkParameterIsNotNull(dVar, "taskRunner");
        o oVar = eVar.K;
        synchronized (oVar) {
            if (oVar.m) {
                throw new IOException("closed");
            } else if (oVar.p) {
                Logger logger = o.j;
                if (logger.isLoggable(Level.FINE)) {
                    logger.fine(c.j(">> CONNECTION " + f0.e0.j.d.a.k(), new Object[0]));
                }
                oVar.o.e0(f0.e0.j.d.a);
                oVar.o.flush();
            }
        }
        o oVar2 = eVar.K;
        s sVar2 = eVar.D;
        synchronized (oVar2) {
            m.checkParameterIsNotNull(sVar2, "settings");
            if (!oVar2.m) {
                oVar2.c(0, Integer.bitCount(sVar2.a) * 6, 4, 0);
                int i2 = 0;
                while (i2 < 10) {
                    if (((1 << i2) & sVar2.a) != 0) {
                        oVar2.o.writeShort(i2 != 4 ? i2 != 7 ? i2 : 4 : 3);
                        oVar2.o.writeInt(sVar2.f3619b[i2]);
                    }
                    i2++;
                }
                oVar2.o.flush();
            } else {
                throw new IOException("closed");
            }
        }
        if (eVar.D.a() != 65535) {
            eVar.K.q(0, a - 65535);
        }
        f0.e0.f.c f = dVar.f();
        String str3 = eVar.o;
        f.c(new f0.e0.f.b(eVar.L, str3, true, str3, true), 0L);
    }

    public String toString() {
        Object obj;
        StringBuilder R = b.d.b.a.a.R("Connection{");
        R.append(this.q.a.a.g);
        R.append(MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR);
        R.append(this.q.a.a.h);
        R.append(',');
        R.append(" proxy=");
        R.append(this.q.f3575b);
        R.append(" hostAddress=");
        R.append(this.q.c);
        R.append(" cipherSuite=");
        v vVar = this.d;
        if (vVar == null || (obj = vVar.c) == null) {
            obj = "none";
        }
        R.append(obj);
        R.append(" protocol=");
        R.append(this.e);
        R.append('}');
        return R.toString();
    }
}
