package f0.e0.g;

import androidx.core.app.NotificationCompat;
import d0.z.d.m;
import f0.c0;
import f0.e0.f.c;
import f0.e0.f.d;
import f0.e0.g.e;
import f0.e0.k.h;
import java.lang.ref.Reference;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
/* compiled from: RealConnectionPool.kt */
/* loaded from: classes3.dex */
public final class k {
    public final long a;

    /* renamed from: b  reason: collision with root package name */
    public final c f3593b;
    public final a c = new a(b.d.b.a.a.H(new StringBuilder(), f0.e0.c.g, " ConnectionPool"));
    public final ConcurrentLinkedQueue<j> d = new ConcurrentLinkedQueue<>();
    public final int e;

    /* compiled from: RealConnectionPool.kt */
    /* loaded from: classes3.dex */
    public static final class a extends f0.e0.f.a {
        public a(String str) {
            super(str, true);
        }

        @Override // f0.e0.f.a
        public long a() {
            k kVar = k.this;
            long nanoTime = System.nanoTime();
            Iterator<j> it = kVar.d.iterator();
            j jVar = null;
            long j = Long.MIN_VALUE;
            int i = 0;
            int i2 = 0;
            while (it.hasNext()) {
                j next = it.next();
                m.checkExpressionValueIsNotNull(next, "connection");
                synchronized (next) {
                    if (kVar.b(next, nanoTime) > 0) {
                        i2++;
                    } else {
                        i++;
                        long j2 = nanoTime - next.p;
                        if (j2 > j) {
                            jVar = next;
                            j = j2;
                        }
                    }
                }
            }
            long j3 = kVar.a;
            if (j >= j3 || i > kVar.e) {
                if (jVar == null) {
                    m.throwNpe();
                }
                synchronized (jVar) {
                    if (!jVar.o.isEmpty()) {
                        return 0L;
                    }
                    if (jVar.p + j != nanoTime) {
                        return 0L;
                    }
                    jVar.i = true;
                    kVar.d.remove(jVar);
                    f0.e0.c.e(jVar.n());
                    if (!kVar.d.isEmpty()) {
                        return 0L;
                    }
                    kVar.f3593b.a();
                    return 0L;
                }
            } else if (i > 0) {
                return j3 - j;
            } else {
                if (i2 > 0) {
                    return j3;
                }
                return -1L;
            }
        }
    }

    public k(d dVar, int i, long j, TimeUnit timeUnit) {
        m.checkParameterIsNotNull(dVar, "taskRunner");
        m.checkParameterIsNotNull(timeUnit, "timeUnit");
        this.e = i;
        this.a = timeUnit.toNanos(j);
        this.f3593b = dVar.f();
        if (!(j > 0)) {
            throw new IllegalArgumentException(b.d.b.a.a.s("keepAliveDuration <= 0: ", j).toString());
        }
    }

    public final boolean a(f0.a aVar, e eVar, List<c0> list, boolean z2) {
        m.checkParameterIsNotNull(aVar, "address");
        m.checkParameterIsNotNull(eVar, NotificationCompat.CATEGORY_CALL);
        Iterator<j> it = this.d.iterator();
        while (it.hasNext()) {
            j next = it.next();
            m.checkExpressionValueIsNotNull(next, "connection");
            synchronized (next) {
                if (z2) {
                    if (!next.j()) {
                    }
                }
                if (next.h(aVar, list)) {
                    eVar.f(next);
                    return true;
                }
            }
        }
        return false;
    }

    public final int b(j jVar, long j) {
        byte[] bArr = f0.e0.c.a;
        List<Reference<e>> list = jVar.o;
        int i = 0;
        while (i < list.size()) {
            Reference<e> reference = list.get(i);
            if (reference.get() != null) {
                i++;
            } else {
                StringBuilder R = b.d.b.a.a.R("A connection to ");
                R.append(jVar.q.a.a);
                R.append(" was leaked. ");
                R.append("Did you forget to close a response body?");
                String sb = R.toString();
                h.a aVar = h.c;
                h.a.k(sb, ((e.b) reference).a);
                list.remove(i);
                jVar.i = true;
                if (list.isEmpty()) {
                    jVar.p = j - this.a;
                    return 0;
                }
            }
        }
        return list.size();
    }
}
