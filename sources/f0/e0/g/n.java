package f0.e0.g;

import d0.t.m;
import d0.z.d.o;
import f0.e0.c;
import f0.w;
import java.net.Proxy;
import java.net.URI;
import java.util.List;
import kotlin.jvm.functions.Function0;
/* compiled from: RouteSelector.kt */
/* loaded from: classes3.dex */
public final class n extends o implements Function0<List<? extends Proxy>> {
    public final /* synthetic */ Proxy $proxy;
    public final /* synthetic */ w $url;
    public final /* synthetic */ m this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public n(m mVar, Proxy proxy, w wVar) {
        super(0);
        this.this$0 = mVar;
        this.$proxy = proxy;
        this.$url = wVar;
    }

    @Override // kotlin.jvm.functions.Function0
    public final List<? extends Proxy> invoke() {
        Proxy proxy = this.$proxy;
        if (proxy != null) {
            return m.listOf(proxy);
        }
        URI i = this.$url.i();
        if (i.getHost() == null) {
            return c.m(Proxy.NO_PROXY);
        }
        List<Proxy> select = this.this$0.e.k.select(i);
        return select == null || select.isEmpty() ? c.m(Proxy.NO_PROXY) : c.z(select);
    }
}
