package f0.e0.g;

import androidx.core.app.NotificationCompat;
import d0.t.n;
import f0.c0;
import f0.e;
import f0.t;
import f0.w;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
/* compiled from: RouteSelector.kt */
/* loaded from: classes3.dex */
public final class m {
    public List<? extends Proxy> a;
    public final f0.a e;
    public final l f;
    public final e g;
    public final t h;
    public List<? extends InetSocketAddress> c = n.emptyList();
    public final List<c0> d = new ArrayList();

    /* renamed from: b  reason: collision with root package name */
    public int f3594b = 0;

    /* compiled from: RouteSelector.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public int a;

        /* renamed from: b  reason: collision with root package name */
        public final List<c0> f3595b;

        public a(List<c0> list) {
            d0.z.d.m.checkParameterIsNotNull(list, "routes");
            this.f3595b = list;
        }

        public final boolean a() {
            return this.a < this.f3595b.size();
        }

        public final c0 b() {
            if (a()) {
                List<c0> list = this.f3595b;
                int i = this.a;
                this.a = i + 1;
                return list.get(i);
            }
            throw new NoSuchElementException();
        }
    }

    public m(f0.a aVar, l lVar, e eVar, t tVar) {
        d0.z.d.m.checkParameterIsNotNull(aVar, "address");
        d0.z.d.m.checkParameterIsNotNull(lVar, "routeDatabase");
        d0.z.d.m.checkParameterIsNotNull(eVar, NotificationCompat.CATEGORY_CALL);
        d0.z.d.m.checkParameterIsNotNull(tVar, "eventListener");
        this.e = aVar;
        this.f = lVar;
        this.g = eVar;
        this.h = tVar;
        this.a = n.emptyList();
        w wVar = aVar.a;
        n nVar = new n(this, aVar.j, wVar);
        Objects.requireNonNull(tVar);
        d0.z.d.m.checkParameterIsNotNull(eVar, NotificationCompat.CATEGORY_CALL);
        d0.z.d.m.checkParameterIsNotNull(wVar, "url");
        List<? extends Proxy> invoke = nVar.invoke();
        this.a = invoke;
        d0.z.d.m.checkParameterIsNotNull(eVar, NotificationCompat.CATEGORY_CALL);
        d0.z.d.m.checkParameterIsNotNull(wVar, "url");
        d0.z.d.m.checkParameterIsNotNull(invoke, "proxies");
    }

    public final boolean a() {
        return b() || (this.d.isEmpty() ^ true);
    }

    public final boolean b() {
        return this.f3594b < this.a.size();
    }
}
