package f0.e0.m;

import java.security.cert.Certificate;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
/* compiled from: CertificateChainCleaner.kt */
/* loaded from: classes3.dex */
public abstract class c {
    public abstract List<Certificate> a(List<? extends Certificate> list, String str) throws SSLPeerUnverifiedException;
}
