package f0.e0.h;

import androidx.browser.trusted.sharing.ShareTarget;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import f0.a;
import f0.c0;
import f0.e0.g.c;
import f0.e0.g.d;
import f0.e0.g.e;
import f0.e0.g.j;
import f0.e0.g.k;
import f0.e0.g.m;
import f0.g;
import f0.t;
import f0.v;
import f0.w;
import f0.x;
import f0.y;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.SocketTimeoutException;
import java.security.cert.CertificateException;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocketFactory;
import kotlin.text.Regex;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.connection.RouteException;
import okhttp3.internal.http2.ConnectionShutdownException;
/* compiled from: RetryAndFollowUpInterceptor.kt */
/* loaded from: classes3.dex */
public final class i implements Interceptor {

    /* renamed from: b  reason: collision with root package name */
    public final x f3601b;

    public i(x xVar) {
        m.checkParameterIsNotNull(xVar, "client");
        this.f3601b = xVar;
    }

    public final Request a(Response response, c cVar) throws IOException {
        c0 c0Var;
        String a;
        j jVar;
        RequestBody requestBody = null;
        if (cVar == null || (jVar = cVar.f3586b) == null) {
            c0Var = null;
        } else {
            c0Var = jVar.q;
        }
        int i = response.m;
        Request request = response.j;
        String str = request.c;
        boolean z2 = false;
        if (!(i == 307 || i == 308)) {
            if (i == 401) {
                return this.f3601b.f3655s.a(c0Var, response);
            }
            if (i == 421) {
                RequestBody requestBody2 = request.e;
                if ((requestBody2 != null && requestBody2.isOneShot()) || cVar == null || !(!m.areEqual(cVar.e.h.a.g, cVar.f3586b.q.a.a.g))) {
                    return null;
                }
                j jVar2 = cVar.f3586b;
                synchronized (jVar2) {
                    jVar2.j = true;
                }
                return response.j;
            } else if (i == 503) {
                Response response2 = response.f3787s;
                if ((response2 == null || response2.m != 503) && c(response, Integer.MAX_VALUE) == 0) {
                    return response.j;
                }
                return null;
            } else if (i == 407) {
                if (c0Var == null) {
                    m.throwNpe();
                }
                if (c0Var.f3575b.type() == Proxy.Type.HTTP) {
                    return this.f3601b.f3658z.a(c0Var, response);
                }
                throw new ProtocolException("Received HTTP_PROXY_AUTH (407) code while not using proxy");
            } else if (i != 408) {
                switch (i) {
                    case 300:
                    case 301:
                    case 302:
                    case 303:
                        break;
                    default:
                        return null;
                }
            } else if (!this.f3601b.r) {
                return null;
            } else {
                RequestBody requestBody3 = request.e;
                if (requestBody3 != null && requestBody3.isOneShot()) {
                    return null;
                }
                Response response3 = response.f3787s;
                if ((response3 == null || response3.m != 408) && c(response, 0) <= 0) {
                    return response.j;
                }
                return null;
            }
        }
        if (!this.f3601b.t || (a = Response.a(response, "Location", null, 2)) == null) {
            return null;
        }
        w wVar = response.j.f3784b;
        Objects.requireNonNull(wVar);
        m.checkParameterIsNotNull(a, "link");
        w.a g = wVar.g(a);
        w b2 = g != null ? g.b() : null;
        if (b2 == null) {
            return null;
        }
        if (!m.areEqual(b2.d, response.j.f3784b.d) && !this.f3601b.u) {
            return null;
        }
        Request.a aVar = new Request.a(response.j);
        if (f.a(str)) {
            int i2 = response.m;
            m.checkParameterIsNotNull(str, "method");
            if (m.areEqual(str, "PROPFIND") || i2 == 308 || i2 == 307) {
                z2 = true;
            }
            m.checkParameterIsNotNull(str, "method");
            if (!(!m.areEqual(str, "PROPFIND")) || i2 == 308 || i2 == 307) {
                if (z2) {
                    requestBody = response.j.e;
                }
                aVar.c(str, requestBody);
            } else {
                aVar.c(ShareTarget.METHOD_GET, null);
            }
            if (!z2) {
                aVar.d("Transfer-Encoding");
                aVar.d("Content-Length");
                aVar.d("Content-Type");
            }
        }
        if (!f0.e0.c.a(response.j.f3784b, b2)) {
            aVar.d("Authorization");
        }
        aVar.g(b2);
        return aVar.a();
    }

    public final boolean b(IOException iOException, e eVar, Request request, boolean z2) {
        boolean z3;
        f0.e0.g.m mVar;
        j jVar;
        if (!this.f3601b.r) {
            return false;
        }
        if (z2) {
            RequestBody requestBody = request.e;
            if ((requestBody != null && requestBody.isOneShot()) || (iOException instanceof FileNotFoundException)) {
                return false;
            }
        }
        if (!(!(iOException instanceof ProtocolException) && (!(iOException instanceof InterruptedIOException) ? (!(iOException instanceof SSLHandshakeException) || !(iOException.getCause() instanceof CertificateException)) && !(iOException instanceof SSLPeerUnverifiedException) : (iOException instanceof SocketTimeoutException) && !z2))) {
            return false;
        }
        d dVar = eVar.o;
        if (dVar == null) {
            m.throwNpe();
        }
        int i = dVar.c;
        if (i == 0 && dVar.d == 0 && dVar.e == 0) {
            z3 = false;
        } else {
            if (dVar.f == null) {
                c0 c0Var = null;
                if (i <= 1 && dVar.d <= 1 && dVar.e <= 0 && (jVar = dVar.i.p) != null) {
                    synchronized (jVar) {
                        if (jVar.k == 0) {
                            if (f0.e0.c.a(jVar.q.a.a, dVar.h.a)) {
                                c0Var = jVar.q;
                            }
                        }
                    }
                }
                if (c0Var != null) {
                    dVar.f = c0Var;
                } else {
                    m.a aVar = dVar.a;
                    if ((aVar == null || !aVar.a()) && (mVar = dVar.f3587b) != null) {
                        z3 = mVar.a();
                    }
                }
            }
            z3 = true;
        }
        return z3;
    }

    public final int c(Response response, int i) {
        String a = Response.a(response, "Retry-After", null, 2);
        if (a == null) {
            return i;
        }
        if (!new Regex("\\d+").matches(a)) {
            return Integer.MAX_VALUE;
        }
        Integer valueOf = Integer.valueOf(a);
        d0.z.d.m.checkExpressionValueIsNotNull(valueOf, "Integer.valueOf(header)");
        return valueOf.intValue();
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Throwable th;
        Response response;
        int i;
        List list;
        e eVar;
        Throwable th2;
        g gVar;
        i iVar;
        Response response2;
        boolean z2;
        List list2;
        e eVar2;
        g gVar2;
        HostnameVerifier hostnameVerifier;
        SSLSocketFactory sSLSocketFactory;
        e eVar3 = this;
        d0.z.d.m.checkParameterIsNotNull(chain, "chain");
        g gVar3 = (g) chain;
        Request request = gVar3.f;
        e eVar4 = gVar3.f3600b;
        boolean z3 = true;
        List emptyList = n.emptyList();
        Response response3 = null;
        int i2 = 0;
        Request request2 = request;
        boolean z4 = true;
        while (true) {
            Objects.requireNonNull(eVar4);
            d0.z.d.m.checkParameterIsNotNull(request2, "request");
            if (eVar4.r == null) {
                synchronized (eVar4) {
                    try {
                        try {
                            if (!(eVar4.t ^ z3)) {
                                throw new IllegalStateException("cannot make a new request because the previous response is still open: please call response.close()".toString());
                            } else if (!(eVar4.f3588s ^ z3)) {
                                throw new IllegalStateException("Check failed.".toString());
                            }
                        } catch (Throwable th3) {
                            th = th3;
                            throw th;
                        }
                    } catch (Throwable th4) {
                        th = th4;
                        eVar3 = eVar4;
                    }
                }
                if (z4) {
                    k kVar = eVar4.j;
                    w wVar = request2.f3784b;
                    if (wVar.c) {
                        x xVar = eVar4.f3590y;
                        SSLSocketFactory sSLSocketFactory2 = xVar.B;
                        if (sSLSocketFactory2 != null) {
                            HostnameVerifier hostnameVerifier2 = xVar.F;
                            gVar2 = xVar.G;
                            sSLSocketFactory = sSLSocketFactory2;
                            hostnameVerifier = hostnameVerifier2;
                        } else {
                            throw new IllegalStateException("CLEARTEXT-only client");
                        }
                    } else {
                        sSLSocketFactory = null;
                        hostnameVerifier = null;
                        gVar2 = null;
                    }
                    String str = wVar.g;
                    int i3 = wVar.h;
                    x xVar2 = eVar4.f3590y;
                    list = emptyList;
                    i = i2;
                    response = response3;
                    a aVar = new a(str, i3, xVar2.w, xVar2.A, sSLSocketFactory, hostnameVerifier, gVar2, xVar2.f3658z, xVar2.f3656x, xVar2.E, xVar2.D, xVar2.f3657y);
                    t tVar = eVar4.k;
                    eVar4.o = new d(kVar, aVar, eVar4, tVar);
                    eVar = tVar;
                } else {
                    list = emptyList;
                    response = response3;
                    i = i2;
                    eVar = eVar3;
                }
                try {
                    if (!eVar4.v) {
                        try {
                            try {
                                Response a = gVar3.a(request2);
                                if (response != null) {
                                    try {
                                        d0.z.d.m.checkParameterIsNotNull(a, "response");
                                        Request request3 = a.j;
                                        y yVar = a.k;
                                        int i4 = a.m;
                                        String str2 = a.l;
                                        v vVar = a.n;
                                        Headers.a e = a.o.e();
                                        ResponseBody responseBody = a.p;
                                        Response response4 = a.q;
                                        Response response5 = a.r;
                                        long j = a.t;
                                        gVar = gVar3;
                                        eVar2 = eVar4;
                                        try {
                                            long j2 = a.u;
                                            c cVar = a.v;
                                            Response response6 = response;
                                            d0.z.d.m.checkParameterIsNotNull(response6, "response");
                                            Request request4 = response6.j;
                                            y yVar2 = response6.k;
                                            int i5 = response6.m;
                                            String str3 = response6.l;
                                            v vVar2 = response6.n;
                                            Headers.a e2 = response6.o.e();
                                            Response response7 = response6.q;
                                            Response response8 = response6.r;
                                            Response response9 = response6.f3787s;
                                            long j3 = response6.t;
                                            long j4 = response6.u;
                                            c cVar2 = response6.v;
                                            if (!(i5 >= 0)) {
                                                throw new IllegalStateException(("code < 0: " + i5).toString());
                                            } else if (request4 == null) {
                                                throw new IllegalStateException("request == null".toString());
                                            } else if (yVar2 == null) {
                                                throw new IllegalStateException("protocol == null".toString());
                                            } else if (str3 != null) {
                                                Response response10 = new Response(request4, yVar2, str3, i5, vVar2, e2.c(), null, response7, response8, response9, j3, j4, cVar2);
                                                if (response10.p == null) {
                                                    if (!(i4 >= 0)) {
                                                        throw new IllegalStateException(("code < 0: " + i4).toString());
                                                    } else if (request3 == null) {
                                                        throw new IllegalStateException("request == null".toString());
                                                    } else if (yVar == null) {
                                                        throw new IllegalStateException("protocol == null".toString());
                                                    } else if (str2 != null) {
                                                        a = new Response(request3, yVar, str2, i4, vVar, e.c(), responseBody, response4, response5, response10, j, j2, cVar);
                                                    } else {
                                                        throw new IllegalStateException("message == null".toString());
                                                    }
                                                } else {
                                                    throw new IllegalArgumentException("priorResponse.body != null".toString());
                                                }
                                            } else {
                                                throw new IllegalStateException("message == null".toString());
                                            }
                                        } catch (Throwable th5) {
                                            th2 = th5;
                                            eVar = eVar2;
                                            eVar.i(true);
                                            throw th2;
                                        }
                                    } catch (Throwable th6) {
                                        th2 = th6;
                                        eVar2 = eVar4;
                                    }
                                } else {
                                    gVar = gVar3;
                                    eVar2 = eVar4;
                                }
                                response3 = a;
                                eVar = eVar2;
                            } catch (IOException e3) {
                                gVar = gVar3;
                                eVar = eVar4;
                                response2 = response;
                                iVar = this;
                                if (iVar.b(e3, eVar, request2, !(e3 instanceof ConnectionShutdownException))) {
                                    list2 = u.plus((Collection<? extends IOException>) list, e3);
                                    z2 = true;
                                    eVar.i(z2);
                                    emptyList = list2;
                                    response3 = response2;
                                    i2 = i;
                                    z4 = false;
                                    eVar4 = eVar;
                                    eVar3 = iVar;
                                    gVar3 = gVar;
                                    z3 = true;
                                } else {
                                    f0.e0.c.D(e3, list);
                                    throw e3;
                                }
                            }
                        } catch (RouteException e4) {
                            gVar = gVar3;
                            eVar = eVar4;
                            List list3 = list;
                            response2 = response;
                            iVar = this;
                            if (iVar.b(e4.c(), eVar, request2, false)) {
                                list2 = u.plus((Collection<? extends IOException>) list3, e4.b());
                                z2 = true;
                                eVar.i(z2);
                                emptyList = list2;
                                response3 = response2;
                                i2 = i;
                                z4 = false;
                                eVar4 = eVar;
                                eVar3 = iVar;
                                gVar3 = gVar;
                                z3 = true;
                            } else {
                                IOException b2 = e4.b();
                                f0.e0.c.D(b2, list3);
                                throw b2;
                            }
                        }
                        try {
                            c cVar3 = eVar.r;
                            iVar = this;
                            try {
                                Request a2 = iVar.a(response3, cVar3);
                                if (a2 == null) {
                                    if (cVar3 != null && cVar3.a) {
                                        eVar.n();
                                    }
                                    eVar.i(false);
                                    return response3;
                                }
                                RequestBody requestBody = a2.e;
                                if (requestBody == null || !requestBody.isOneShot()) {
                                    ResponseBody responseBody2 = response3.p;
                                    if (responseBody2 != null) {
                                        byte[] bArr = f0.e0.c.a;
                                        d0.z.d.m.checkParameterIsNotNull(responseBody2, "$this$closeQuietly");
                                        try {
                                            responseBody2.close();
                                        } catch (RuntimeException e5) {
                                            throw e5;
                                        } catch (Exception unused) {
                                        }
                                    }
                                    i2 = i + 1;
                                    if (i2 <= 20) {
                                        eVar.i(true);
                                        request2 = a2;
                                        emptyList = list;
                                        z4 = true;
                                        eVar4 = eVar;
                                        eVar3 = iVar;
                                        gVar3 = gVar;
                                        z3 = true;
                                    } else {
                                        throw new ProtocolException("Too many follow-up requests: " + i2);
                                    }
                                } else {
                                    eVar.i(false);
                                    return response3;
                                }
                            } catch (Throwable th7) {
                                th2 = th7;
                                eVar.i(true);
                                throw th2;
                            }
                        } catch (Throwable th8) {
                            th2 = th8;
                            eVar.i(true);
                            throw th2;
                        }
                    } else {
                        throw new IOException("Canceled");
                    }
                } catch (Throwable th9) {
                    th2 = th9;
                    eVar = eVar4;
                }
            } else {
                throw new IllegalStateException("Check failed.".toString());
            }
        }
    }
}
