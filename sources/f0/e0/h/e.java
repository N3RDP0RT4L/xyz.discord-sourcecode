package f0.e0.h;

import d0.g0.t;
import d0.z.d.m;
import f0.e0.c;
import okhttp3.Response;
import okio.ByteString;
/* compiled from: HttpHeaders.kt */
/* loaded from: classes3.dex */
public final class e {
    public static final ByteString a;

    /* renamed from: b  reason: collision with root package name */
    public static final ByteString f3599b;

    static {
        ByteString.a aVar = ByteString.k;
        a = aVar.c("\"\\");
        f3599b = aVar.c("\t ,=");
    }

    public static final boolean a(Response response) {
        m.checkParameterIsNotNull(response, "$this$promisesBody");
        if (m.areEqual(response.j.c, "HEAD")) {
            return false;
        }
        int i = response.m;
        return (((i >= 100 && i < 200) || i == 204 || i == 304) && c.l(response) == -1 && !t.equals("chunked", Response.a(response, "Transfer-Encoding", null, 2), true)) ? false : true;
    }

    /* JADX WARN: Code restructure failed: missing block: B:79:0x0086, code lost:
        continue;
     */
    /* JADX WARN: Code restructure failed: missing block: B:80:0x0086, code lost:
        continue;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final void b(g0.e r19, java.util.List<f0.i> r20) throws java.io.EOFException {
        /*
            Method dump skipped, instructions count: 289
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: f0.e0.h.e.b(g0.e, java.util.List):void");
    }

    public static final String c(g0.e eVar) {
        long E = eVar.E(f3599b);
        if (E == -1) {
            E = eVar.k;
        }
        if (E != 0) {
            return eVar.H(E);
        }
        return null;
    }

    /* JADX WARN: Code restructure failed: missing block: B:103:0x020e, code lost:
        if (r5.b(r0, r8) == false) goto L108;
     */
    /* JADX WARN: Code restructure failed: missing block: B:107:0x0223, code lost:
        if (okhttp3.internal.publicsuffix.PublicSuffixDatabase.c.a(r8) == null) goto L108;
     */
    /* JADX WARN: Removed duplicated region for block: B:126:0x0277  */
    /* JADX WARN: Removed duplicated region for block: B:154:0x0282 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final void d(f0.p r41, f0.w r42, okhttp3.Headers r43) {
        /*
            Method dump skipped, instructions count: 682
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: f0.e0.h.e.d(f0.p, f0.w, okhttp3.Headers):void");
    }

    public static final boolean e(g0.e eVar) {
        boolean z2 = false;
        while (!eVar.w()) {
            byte q = eVar.q(0L);
            if (q == 9 || q == 32) {
                eVar.readByte();
            } else if (q != 44) {
                break;
            } else {
                eVar.readByte();
                z2 = true;
            }
        }
        return z2;
    }
}
