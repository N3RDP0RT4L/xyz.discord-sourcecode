package f0.e0.h;

import b.d.b.a.a;
import d0.g0.t;
import d0.z.d.m;
import f0.y;
import java.io.IOException;
import java.net.ProtocolException;
/* compiled from: StatusLine.kt */
/* loaded from: classes3.dex */
public final class j {
    public final y a;

    /* renamed from: b  reason: collision with root package name */
    public final int f3602b;
    public final String c;

    public j(y yVar, int i, String str) {
        m.checkParameterIsNotNull(yVar, "protocol");
        m.checkParameterIsNotNull(str, "message");
        this.a = yVar;
        this.f3602b = i;
        this.c = str;
    }

    public static final j a(String str) throws IOException {
        String str2;
        y yVar = y.HTTP_1_0;
        m.checkParameterIsNotNull(str, "statusLine");
        int i = 9;
        if (t.startsWith$default(str, "HTTP/1.", false, 2, null)) {
            if (str.length() < 9 || str.charAt(8) != ' ') {
                throw new ProtocolException(a.v("Unexpected status line: ", str));
            }
            int charAt = str.charAt(7) - '0';
            if (charAt != 0) {
                if (charAt == 1) {
                    yVar = y.HTTP_1_1;
                } else {
                    throw new ProtocolException(a.v("Unexpected status line: ", str));
                }
            }
        } else if (t.startsWith$default(str, "ICY ", false, 2, null)) {
            i = 4;
        } else {
            throw new ProtocolException(a.v("Unexpected status line: ", str));
        }
        int i2 = i + 3;
        if (str.length() >= i2) {
            try {
                String substring = str.substring(i, i2);
                m.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                int parseInt = Integer.parseInt(substring);
                if (str.length() <= i2) {
                    str2 = "";
                } else if (str.charAt(i2) == ' ') {
                    str2 = str.substring(i + 4);
                    m.checkExpressionValueIsNotNull(str2, "(this as java.lang.String).substring(startIndex)");
                } else {
                    throw new ProtocolException(a.v("Unexpected status line: ", str));
                }
                return new j(yVar, parseInt, str2);
            } catch (NumberFormatException unused) {
                throw new ProtocolException(a.v("Unexpected status line: ", str));
            }
        } else {
            throw new ProtocolException(a.v("Unexpected status line: ", str));
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (this.a == y.HTTP_1_0) {
            sb.append("HTTP/1.0");
        } else {
            sb.append("HTTP/1.1");
        }
        sb.append(' ');
        sb.append(this.f3602b);
        sb.append(' ');
        sb.append(this.c);
        String sb2 = sb.toString();
        m.checkExpressionValueIsNotNull(sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }
}
