package f0.e0.h;

import okhttp3.Interceptor;
/* compiled from: CallServerInterceptor.kt */
/* loaded from: classes3.dex */
public final class b implements Interceptor {

    /* renamed from: b  reason: collision with root package name */
    public final boolean f3597b;

    public b(boolean z2) {
        this.f3597b = z2;
    }

    /* JADX WARN: Code restructure failed: missing block: B:76:0x0261, code lost:
        if (d0.g0.t.equals("close", okhttp3.Response.a(r6, "Connection", null, 2), true) != false) goto L78;
     */
    @Override // okhttp3.Interceptor
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public okhttp3.Response intercept(okhttp3.Interceptor.Chain r31) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 797
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: f0.e0.h.b.intercept(okhttp3.Interceptor$Chain):okhttp3.Response");
    }
}
