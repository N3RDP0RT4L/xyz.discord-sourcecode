package f0.e0.h;

import d0.g0.t;
import d0.z.d.m;
import f0.e0.c;
import f0.n;
import f0.p;
import g0.l;
import g0.r;
import java.io.IOException;
import java.util.List;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
/* compiled from: BridgeInterceptor.kt */
/* loaded from: classes3.dex */
public final class a implements Interceptor {

    /* renamed from: b  reason: collision with root package name */
    public final p f3596b;

    public a(p pVar) {
        m.checkParameterIsNotNull(pVar, "cookieJar");
        this.f3596b = pVar;
    }

    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) throws IOException {
        boolean z2;
        ResponseBody responseBody;
        m.checkParameterIsNotNull(chain, "chain");
        g gVar = (g) chain;
        Request request = gVar.f;
        Request.a aVar = new Request.a(request);
        RequestBody requestBody = request.e;
        if (requestBody != null) {
            MediaType contentType = requestBody.contentType();
            if (contentType != null) {
                aVar.b("Content-Type", contentType.d);
            }
            long contentLength = requestBody.contentLength();
            if (contentLength != -1) {
                aVar.b("Content-Length", String.valueOf(contentLength));
                aVar.d("Transfer-Encoding");
            } else {
                aVar.b("Transfer-Encoding", "chunked");
                aVar.d("Content-Length");
            }
        }
        int i = 0;
        if (request.b("Host") == null) {
            aVar.b("Host", c.y(request.f3784b, false));
        }
        if (request.b("Connection") == null) {
            aVar.b("Connection", "Keep-Alive");
        }
        if (request.b("Accept-Encoding") == null && request.b("Range") == null) {
            aVar.b("Accept-Encoding", "gzip");
            z2 = true;
        } else {
            z2 = false;
        }
        List<n> b2 = this.f3596b.b(request.f3784b);
        if (!b2.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (Object obj : b2) {
                i++;
                if (i < 0) {
                    d0.t.n.throwIndexOverflow();
                }
                n nVar = (n) obj;
                if (i > 0) {
                    sb.append("; ");
                }
                sb.append(nVar.f);
                sb.append('=');
                sb.append(nVar.g);
            }
            String sb2 = sb.toString();
            m.checkExpressionValueIsNotNull(sb2, "StringBuilder().apply(builderAction).toString()");
            aVar.b("Cookie", sb2);
        }
        if (request.b("User-Agent") == null) {
            aVar.b("User-Agent", "okhttp/4.8.0");
        }
        Response a = gVar.a(aVar.a());
        e.d(this.f3596b, request.f3784b, a.o);
        Response.a aVar2 = new Response.a(a);
        aVar2.g(request);
        if (z2 && t.equals("gzip", Response.a(a, "Content-Encoding", null, 2), true) && e.a(a) && (responseBody = a.p) != null) {
            l lVar = new l(responseBody.c());
            Headers.a e = a.o.e();
            e.d("Content-Encoding");
            e.d("Content-Length");
            aVar2.d(e.c());
            String a2 = Response.a(a, "Content-Type", null, 2);
            m.checkParameterIsNotNull(lVar, "$this$buffer");
            aVar2.g = new h(a2, -1L, new r(lVar));
        }
        return aVar2.a();
    }
}
