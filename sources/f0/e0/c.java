package f0.e0;

import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.g0.w;
import d0.t.h0;
import d0.t.u;
import d0.z.d.m;
import f0.b0;
import f0.e0.j.b;
import f0.x;
import g0.e;
import g0.g;
import g0.o;
import java.io.Closeable;
import java.io.IOException;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import kotlin.TypeCastException;
import kotlin.text.Regex;
import okhttp3.Headers;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.ByteString;
/* compiled from: Util.kt */
/* loaded from: classes3.dex */
public final class c {
    public static final byte[] a;

    /* renamed from: b  reason: collision with root package name */
    public static final Headers f3577b = Headers.j.c(new String[0]);
    public static final ResponseBody c;
    public static final o d;
    public static final TimeZone e;
    public static final Regex f;
    public static final String g;

    static {
        byte[] bArr = new byte[0];
        a = bArr;
        m.checkParameterIsNotNull(bArr, "$this$toResponseBody");
        e eVar = new e();
        eVar.R(bArr);
        m.checkParameterIsNotNull(eVar, "$this$asResponseBody");
        c = new b0(eVar, null, 0);
        RequestBody.Companion.d(RequestBody.Companion, bArr, null, 0, 0, 7);
        o.a aVar = o.k;
        ByteString.a aVar2 = ByteString.k;
        d = aVar.c(aVar2.a("efbbbf"), aVar2.a("feff"), aVar2.a("fffe"), aVar2.a("0000ffff"), aVar2.a("ffff0000"));
        TimeZone timeZone = TimeZone.getTimeZone("GMT");
        if (timeZone == null) {
            m.throwNpe();
        }
        e = timeZone;
        f = new Regex("([0-9a-fA-F]*:[0-9a-fA-F:.]*)|([\\d.]+)");
        String name = x.class.getName();
        m.checkExpressionValueIsNotNull(name, "OkHttpClient::class.java.name");
        g = w.removeSuffix(w.removePrefix(name, "okhttp3."), "Client");
    }

    public static final <K, V> Map<K, V> A(Map<K, ? extends V> map) {
        m.checkParameterIsNotNull(map, "$this$toImmutableMap");
        if (map.isEmpty()) {
            return h0.emptyMap();
        }
        Map<K, V> unmodifiableMap = Collections.unmodifiableMap(new LinkedHashMap(map));
        m.checkExpressionValueIsNotNull(unmodifiableMap, "Collections.unmodifiableMap(LinkedHashMap(this))");
        return unmodifiableMap;
    }

    public static final int B(String str, int i) {
        if (str != null) {
            try {
                long parseLong = Long.parseLong(str);
                if (parseLong > Integer.MAX_VALUE) {
                    return Integer.MAX_VALUE;
                }
                if (parseLong < 0) {
                    return 0;
                }
                return (int) parseLong;
            } catch (NumberFormatException unused) {
            }
        }
        return i;
    }

    public static final String C(String str, int i, int i2) {
        m.checkParameterIsNotNull(str, "$this$trimSubstring");
        int o = o(str, i, i2);
        String substring = str.substring(o, p(str, o, i2));
        m.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        return substring;
    }

    public static final Throwable D(Exception exc, List<? extends Exception> list) {
        m.checkParameterIsNotNull(exc, "$this$withSuppressed");
        m.checkParameterIsNotNull(list, "suppressed");
        if (list.size() > 1) {
            System.out.println(list);
        }
        for (Exception exc2 : list) {
            exc.addSuppressed(exc2);
        }
        return exc;
    }

    public static final boolean a(f0.w wVar, f0.w wVar2) {
        m.checkParameterIsNotNull(wVar, "$this$canReuseConnectionFor");
        m.checkParameterIsNotNull(wVar2, "other");
        return m.areEqual(wVar.g, wVar2.g) && wVar.h == wVar2.h && m.areEqual(wVar.d, wVar2.d);
    }

    public static final int b(String str, long j, TimeUnit timeUnit) {
        m.checkParameterIsNotNull(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        boolean z2 = false;
        int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
        if (i >= 0) {
            if (timeUnit != null) {
                long millis = timeUnit.toMillis(j);
                if (millis <= ((long) Integer.MAX_VALUE)) {
                    if (millis != 0 || i <= 0) {
                        z2 = true;
                    }
                    if (z2) {
                        return (int) millis;
                    }
                    throw new IllegalArgumentException(a.v(str, " too small.").toString());
                }
                throw new IllegalArgumentException(a.v(str, " too large.").toString());
            }
            throw new IllegalStateException("unit == null".toString());
        }
        throw new IllegalStateException(a.v(str, " < 0").toString());
    }

    public static final void c(long j, long j2, long j3) {
        if ((j2 | j3) < 0 || j2 > j || j - j2 < j3) {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    public static final void d(Closeable closeable) {
        m.checkParameterIsNotNull(closeable, "$this$closeQuietly");
        try {
            closeable.close();
        } catch (RuntimeException e2) {
            throw e2;
        } catch (Exception unused) {
        }
    }

    public static final void e(Socket socket) {
        m.checkParameterIsNotNull(socket, "$this$closeQuietly");
        try {
            socket.close();
        } catch (AssertionError e2) {
            throw e2;
        } catch (RuntimeException e3) {
            throw e3;
        } catch (Exception unused) {
        }
    }

    public static final int f(String str, char c2, int i, int i2) {
        m.checkParameterIsNotNull(str, "$this$delimiterOffset");
        while (i < i2) {
            if (str.charAt(i) == c2) {
                return i;
            }
            i++;
        }
        return i2;
    }

    public static final int g(String str, String str2, int i, int i2) {
        m.checkParameterIsNotNull(str, "$this$delimiterOffset");
        m.checkParameterIsNotNull(str2, "delimiters");
        while (i < i2) {
            if (w.contains$default((CharSequence) str2, str.charAt(i), false, 2, (Object) null)) {
                return i;
            }
            i++;
        }
        return i2;
    }

    public static /* synthetic */ int h(String str, char c2, int i, int i2, int i3) {
        if ((i3 & 2) != 0) {
            i = 0;
        }
        if ((i3 & 4) != 0) {
            i2 = str.length();
        }
        return f(str, c2, i, i2);
    }

    public static final boolean i(g0.x xVar, int i, TimeUnit timeUnit) {
        m.checkParameterIsNotNull(xVar, "$this$discard");
        m.checkParameterIsNotNull(timeUnit, "timeUnit");
        try {
            return v(xVar, i, timeUnit);
        } catch (IOException unused) {
            return false;
        }
    }

    public static final String j(String str, Object... objArr) {
        m.checkParameterIsNotNull(str, "format");
        m.checkParameterIsNotNull(objArr, "args");
        Locale locale = Locale.US;
        m.checkExpressionValueIsNotNull(locale, "Locale.US");
        Object[] copyOf = Arrays.copyOf(objArr, objArr.length);
        String format = String.format(locale, str, Arrays.copyOf(copyOf, copyOf.length));
        m.checkExpressionValueIsNotNull(format, "java.lang.String.format(locale, format, *args)");
        return format;
    }

    public static final boolean k(String[] strArr, String[] strArr2, Comparator<? super String> comparator) {
        m.checkParameterIsNotNull(strArr, "$this$hasIntersection");
        m.checkParameterIsNotNull(comparator, "comparator");
        if (!(strArr.length == 0) && strArr2 != null) {
            if (!(strArr2.length == 0)) {
                for (String str : strArr) {
                    for (String str2 : strArr2) {
                        if (comparator.compare(str, str2) == 0) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static final long l(Response response) {
        m.checkParameterIsNotNull(response, "$this$headersContentLength");
        String c2 = response.o.c("Content-Length");
        if (c2 != null) {
            m.checkParameterIsNotNull(c2, "$this$toLongOrDefault");
            try {
                return Long.parseLong(c2);
            } catch (NumberFormatException unused) {
            }
        }
        return -1L;
    }

    @SafeVarargs
    public static final <T> List<T> m(T... tArr) {
        m.checkParameterIsNotNull(tArr, "elements");
        Object[] objArr = (Object[]) tArr.clone();
        List<T> unmodifiableList = Collections.unmodifiableList(Arrays.asList(Arrays.copyOf(objArr, objArr.length)));
        m.checkExpressionValueIsNotNull(unmodifiableList, "Collections.unmodifiable…sList(*elements.clone()))");
        return unmodifiableList;
    }

    public static final int n(String str) {
        m.checkParameterIsNotNull(str, "$this$indexOfControlOrNonAscii");
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt <= 31 || charAt >= 127) {
                return i;
            }
        }
        return -1;
    }

    public static final int o(String str, int i, int i2) {
        m.checkParameterIsNotNull(str, "$this$indexOfFirstNonAsciiWhitespace");
        while (i < i2) {
            char charAt = str.charAt(i);
            if (charAt != '\t' && charAt != '\n' && charAt != '\f' && charAt != '\r' && charAt != ' ') {
                return i;
            }
            i++;
        }
        return i2;
    }

    public static final int p(String str, int i, int i2) {
        m.checkParameterIsNotNull(str, "$this$indexOfLastNonAsciiWhitespace");
        int i3 = i2 - 1;
        if (i3 >= i) {
            while (true) {
                char charAt = str.charAt(i3);
                if (charAt == '\t' || charAt == '\n' || charAt == '\f' || charAt == '\r' || charAt == ' ') {
                    if (i3 == i) {
                        break;
                    }
                    i3--;
                } else {
                    return i3 + 1;
                }
            }
        }
        return i;
    }

    public static final String[] q(String[] strArr, String[] strArr2, Comparator<? super String> comparator) {
        m.checkParameterIsNotNull(strArr, "$this$intersect");
        m.checkParameterIsNotNull(strArr2, "other");
        m.checkParameterIsNotNull(comparator, "comparator");
        ArrayList arrayList = new ArrayList();
        for (String str : strArr) {
            int length = strArr2.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                } else if (comparator.compare(str, strArr2[i]) == 0) {
                    arrayList.add(str);
                    break;
                } else {
                    i++;
                }
            }
        }
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            return (String[]) array;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    public static final int r(char c2) {
        if ('0' <= c2 && '9' >= c2) {
            return c2 - '0';
        }
        char c3 = 'a';
        if ('a' > c2 || 'f' < c2) {
            c3 = 'A';
            if ('A' > c2 || 'F' < c2) {
                return -1;
            }
        }
        return (c2 - c3) + 10;
    }

    public static final Charset s(g gVar, Charset charset) throws IOException {
        m.checkParameterIsNotNull(gVar, "$this$readBomAsCharset");
        m.checkParameterIsNotNull(charset, "default");
        int v0 = gVar.v0(d);
        if (v0 == -1) {
            return charset;
        }
        if (v0 == 0) {
            Charset charset2 = StandardCharsets.UTF_8;
            m.checkExpressionValueIsNotNull(charset2, "UTF_8");
            return charset2;
        } else if (v0 == 1) {
            Charset charset3 = StandardCharsets.UTF_16BE;
            m.checkExpressionValueIsNotNull(charset3, "UTF_16BE");
            return charset3;
        } else if (v0 == 2) {
            Charset charset4 = StandardCharsets.UTF_16LE;
            m.checkExpressionValueIsNotNull(charset4, "UTF_16LE");
            return charset4;
        } else if (v0 == 3) {
            return d0.g0.c.d.UTF32_BE();
        } else {
            if (v0 == 4) {
                return d0.g0.c.d.UTF32_LE();
            }
            throw new AssertionError();
        }
    }

    public static final int t(g gVar) throws IOException {
        m.checkParameterIsNotNull(gVar, "$this$readMedium");
        return (gVar.readByte() & 255) | ((gVar.readByte() & 255) << 16) | ((gVar.readByte() & 255) << 8);
    }

    public static final int u(e eVar, byte b2) {
        m.checkParameterIsNotNull(eVar, "$this$skipAll");
        int i = 0;
        while (!eVar.w() && eVar.q(0L) == b2) {
            i++;
            eVar.readByte();
        }
        return i;
    }

    /* JADX WARN: Code restructure failed: missing block: B:12:0x0053, code lost:
        if (r5 == androidx.recyclerview.widget.RecyclerView.FOREVER_NS) goto L13;
     */
    /* JADX WARN: Code restructure failed: missing block: B:13:0x0055, code lost:
        r11.timeout().a();
     */
    /* JADX WARN: Code restructure failed: missing block: B:14:0x005d, code lost:
        r11.timeout().d(r0 + r5);
     */
    /* JADX WARN: Code restructure failed: missing block: B:22:0x0080, code lost:
        if (r5 != androidx.recyclerview.widget.RecyclerView.FOREVER_NS) goto L14;
     */
    /* JADX WARN: Code restructure failed: missing block: B:23:0x0083, code lost:
        return r12;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final boolean v(g0.x r11, int r12, java.util.concurrent.TimeUnit r13) throws java.io.IOException {
        /*
            java.lang.String r0 = "$this$skipAll"
            d0.z.d.m.checkParameterIsNotNull(r11, r0)
            java.lang.String r0 = "timeUnit"
            d0.z.d.m.checkParameterIsNotNull(r13, r0)
            long r0 = java.lang.System.nanoTime()
            g0.y r2 = r11.timeout()
            boolean r2 = r2.e()
            r3 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            if (r2 == 0) goto L27
            g0.y r2 = r11.timeout()
            long r5 = r2.c()
            long r5 = r5 - r0
            goto L28
        L27:
            r5 = r3
        L28:
            g0.y r2 = r11.timeout()
            long r7 = (long) r12
            long r12 = r13.toNanos(r7)
            long r12 = java.lang.Math.min(r5, r12)
            long r12 = r12 + r0
            r2.d(r12)
            g0.e r12 = new g0.e     // Catch: java.lang.Throwable -> L66 java.io.InterruptedIOException -> L7c
            r12.<init>()     // Catch: java.lang.Throwable -> L66 java.io.InterruptedIOException -> L7c
        L3e:
            r7 = 8192(0x2000, double:4.0474E-320)
            long r7 = r11.i0(r12, r7)     // Catch: java.lang.Throwable -> L66 java.io.InterruptedIOException -> L7c
            r9 = -1
            int r13 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r13 == 0) goto L50
            long r7 = r12.k     // Catch: java.lang.Throwable -> L66 java.io.InterruptedIOException -> L7c
            r12.skip(r7)     // Catch: java.lang.Throwable -> L66 java.io.InterruptedIOException -> L7c
            goto L3e
        L50:
            r12 = 1
            int r13 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r13 != 0) goto L5d
        L55:
            g0.y r11 = r11.timeout()
            r11.a()
            goto L83
        L5d:
            g0.y r11 = r11.timeout()
            long r0 = r0 + r5
            r11.d(r0)
            goto L83
        L66:
            r12 = move-exception
            int r13 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r13 != 0) goto L73
            g0.y r11 = r11.timeout()
            r11.a()
            goto L7b
        L73:
            g0.y r11 = r11.timeout()
            long r0 = r0 + r5
            r11.d(r0)
        L7b:
            throw r12
        L7c:
            r12 = 0
            int r13 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r13 != 0) goto L5d
            goto L55
        L83:
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: f0.e0.c.v(g0.x, int, java.util.concurrent.TimeUnit):boolean");
    }

    public static final Headers w(List<b> list) {
        m.checkParameterIsNotNull(list, "$this$toHeaders");
        ArrayList arrayList = new ArrayList(20);
        for (b bVar : list) {
            ByteString byteString = bVar.h;
            ByteString byteString2 = bVar.i;
            String q = byteString.q();
            String q2 = byteString2.q();
            m.checkParameterIsNotNull(q, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkParameterIsNotNull(q2, "value");
            arrayList.add(q);
            arrayList.add(w.trim(q2).toString());
        }
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            return new Headers((String[]) array, null);
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    public static final String x(int i) {
        String hexString = Integer.toHexString(i);
        m.checkExpressionValueIsNotNull(hexString, "Integer.toHexString(this)");
        return hexString;
    }

    /* JADX WARN: Removed duplicated region for block: B:21:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final java.lang.String y(f0.w r5, boolean r6) {
        /*
            java.lang.String r0 = "$this$toHostHeader"
            d0.z.d.m.checkParameterIsNotNull(r5, r0)
            java.lang.String r0 = r5.g
            r1 = 0
            r2 = 2
            r3 = 0
            java.lang.String r4 = ":"
            boolean r0 = d0.g0.w.contains$default(r0, r4, r1, r2, r3)
            if (r0 == 0) goto L21
            r0 = 91
            java.lang.StringBuilder r0 = b.d.b.a.a.O(r0)
            java.lang.String r1 = r5.g
            r2 = 93
            java.lang.String r0 = b.d.b.a.a.G(r0, r1, r2)
            goto L23
        L21:
            java.lang.String r0 = r5.g
        L23:
            if (r6 != 0) goto L56
            int r6 = r5.h
            java.lang.String r1 = r5.d
            java.lang.String r2 = "scheme"
            d0.z.d.m.checkParameterIsNotNull(r1, r2)
            int r2 = r1.hashCode()
            r3 = 3213448(0x310888, float:4.503E-39)
            if (r2 == r3) goto L48
            r3 = 99617003(0x5f008eb, float:2.2572767E-35)
            if (r2 == r3) goto L3d
            goto L53
        L3d:
            java.lang.String r2 = "https"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L53
            r1 = 443(0x1bb, float:6.21E-43)
            goto L54
        L48:
            java.lang.String r2 = "http"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L53
            r1 = 80
            goto L54
        L53:
            r1 = -1
        L54:
            if (r6 == r1) goto L6c
        L56:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r6.append(r0)
            r0 = 58
            r6.append(r0)
            int r5 = r5.h
            r6.append(r5)
            java.lang.String r0 = r6.toString()
        L6c:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: f0.e0.c.y(f0.w, boolean):java.lang.String");
    }

    public static final <T> List<T> z(List<? extends T> list) {
        m.checkParameterIsNotNull(list, "$this$toImmutableList");
        List<T> unmodifiableList = Collections.unmodifiableList(u.toMutableList((Collection) list));
        m.checkExpressionValueIsNotNull(unmodifiableList, "Collections.unmodifiableList(toMutableList())");
        return unmodifiableList;
    }
}
