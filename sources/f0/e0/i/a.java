package f0.e0.i;

import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.g0.w;
import d0.z.d.m;
import g0.g;
import java.util.ArrayList;
import kotlin.TypeCastException;
import okhttp3.Headers;
/* compiled from: HeadersReader.kt */
/* loaded from: classes3.dex */
public final class a {
    public long a = 262144;

    /* renamed from: b  reason: collision with root package name */
    public final g f3603b;

    public a(g gVar) {
        m.checkParameterIsNotNull(gVar, "source");
        this.f3603b = gVar;
    }

    public final Headers a() {
        ArrayList arrayList = new ArrayList(20);
        while (true) {
            String b2 = b();
            if (b2.length() == 0) {
                break;
            }
            m.checkParameterIsNotNull(b2, "line");
            int indexOf$default = w.indexOf$default((CharSequence) b2, (char) MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR, 1, false, 4, (Object) null);
            if (indexOf$default != -1) {
                String substring = b2.substring(0, indexOf$default);
                m.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                String substring2 = b2.substring(indexOf$default + 1);
                m.checkExpressionValueIsNotNull(substring2, "(this as java.lang.String).substring(startIndex)");
                m.checkParameterIsNotNull(substring, ModelAuditLogEntry.CHANGE_KEY_NAME);
                m.checkParameterIsNotNull(substring2, "value");
                arrayList.add(substring);
                arrayList.add(w.trim(substring2).toString());
            } else if (b2.charAt(0) == ':') {
                String substring3 = b2.substring(1);
                m.checkExpressionValueIsNotNull(substring3, "(this as java.lang.String).substring(startIndex)");
                m.checkParameterIsNotNull("", ModelAuditLogEntry.CHANGE_KEY_NAME);
                m.checkParameterIsNotNull(substring3, "value");
                arrayList.add("");
                arrayList.add(w.trim(substring3).toString());
            } else {
                m.checkParameterIsNotNull("", ModelAuditLogEntry.CHANGE_KEY_NAME);
                m.checkParameterIsNotNull(b2, "value");
                arrayList.add("");
                arrayList.add(w.trim(b2).toString());
            }
        }
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            return new Headers((String[]) array, null);
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    public final String b() {
        String G = this.f3603b.G(this.a);
        this.a -= G.length();
        return G;
    }
}
