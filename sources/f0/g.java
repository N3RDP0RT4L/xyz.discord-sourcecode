package f0;

import com.adjust.sdk.Constants;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.g0.t;
import d0.t.n;
import d0.t.u;
import d0.z.d.e0;
import d0.z.d.m;
import f0.e0.m.c;
import java.security.Principal;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.net.ssl.SSLPeerUnverifiedException;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import okio.ByteString;
/* compiled from: CertificatePinner.kt */
/* loaded from: classes3.dex */
public final class g {
    public final Set<b> c;
    public final c d;

    /* renamed from: b  reason: collision with root package name */
    public static final a f3642b = new a(null);
    public static final g a = new g(u.toSet(new ArrayList()), null, 2);

    /* compiled from: CertificatePinner.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final String a(Certificate certificate) {
            m.checkParameterIsNotNull(certificate, "certificate");
            return "sha256/" + b((X509Certificate) certificate).f();
        }

        public final ByteString b(X509Certificate x509Certificate) {
            m.checkParameterIsNotNull(x509Certificate, "$this$sha256Hash");
            ByteString.a aVar = ByteString.k;
            PublicKey publicKey = x509Certificate.getPublicKey();
            m.checkExpressionValueIsNotNull(publicKey, "publicKey");
            byte[] encoded = publicKey.getEncoded();
            m.checkExpressionValueIsNotNull(encoded, "publicKey.encoded");
            return ByteString.a.d(aVar, encoded, 0, 0, 3).g(Constants.SHA256);
        }
    }

    /* compiled from: CertificatePinner.kt */
    /* loaded from: classes3.dex */
    public static final class b {
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            Objects.requireNonNull(bVar);
            if (!m.areEqual((Object) null, (Object) null)) {
                return false;
            }
            Objects.requireNonNull(bVar);
            if (!m.areEqual((Object) null, (Object) null)) {
                return false;
            }
            Objects.requireNonNull(bVar);
            return !(m.areEqual((Object) null, (Object) null) ^ true);
        }

        public int hashCode() {
            throw null;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append((String) null);
            sb.append(MentionUtilsKt.SLASH_CHAR);
            throw null;
        }
    }

    public g(Set set, c cVar, int i) {
        int i2 = i & 2;
        m.checkParameterIsNotNull(set, "pins");
        this.c = set;
        this.d = null;
    }

    public final void a(String str, Function0<? extends List<? extends X509Certificate>> function0) {
        m.checkParameterIsNotNull(str, "hostname");
        m.checkParameterIsNotNull(function0, "cleanedPeerCertificatesFn");
        m.checkParameterIsNotNull(str, "hostname");
        Set<b> set = this.c;
        List<b> emptyList = n.emptyList();
        for (Object obj : set) {
            Objects.requireNonNull((b) obj);
            m.checkParameterIsNotNull(str, "hostname");
            if (t.startsWith$default(null, "**.", false, 2, null)) {
                throw null;
            } else if (t.startsWith$default(null, "*.", false, 2, null)) {
                throw null;
            } else if (m.areEqual(str, (Object) null)) {
                if (emptyList.isEmpty()) {
                    emptyList = new ArrayList();
                }
                e0.asMutableList(emptyList).add(obj);
            }
        }
        if (!emptyList.isEmpty()) {
            List<? extends X509Certificate> invoke = function0.invoke();
            for (X509Certificate x509Certificate : invoke) {
                Iterator it = emptyList.iterator();
                if (it.hasNext()) {
                    Objects.requireNonNull((b) it.next());
                    throw null;
                }
            }
            StringBuilder V = b.d.b.a.a.V("Certificate pinning failure!", "\n  Peer certificate chain:");
            for (X509Certificate x509Certificate2 : invoke) {
                V.append("\n    ");
                m.checkParameterIsNotNull(x509Certificate2, "certificate");
                if (x509Certificate2 instanceof X509Certificate) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("sha256/");
                    m.checkParameterIsNotNull(x509Certificate2, "$this$sha256Hash");
                    ByteString.a aVar = ByteString.k;
                    PublicKey publicKey = x509Certificate2.getPublicKey();
                    m.checkExpressionValueIsNotNull(publicKey, "publicKey");
                    byte[] encoded = publicKey.getEncoded();
                    m.checkExpressionValueIsNotNull(encoded, "publicKey.encoded");
                    sb.append(ByteString.a.d(aVar, encoded, 0, 0, 3).g(Constants.SHA256).f());
                    V.append(sb.toString());
                    V.append(": ");
                    Principal subjectDN = x509Certificate2.getSubjectDN();
                    m.checkExpressionValueIsNotNull(subjectDN, "element.subjectDN");
                    V.append(subjectDN.getName());
                } else {
                    throw new IllegalArgumentException("Certificate pinning requires X509 certificates".toString());
                }
            }
            V.append("\n  Pinned certificates for ");
            V.append(str);
            V.append(":");
            for (b bVar : emptyList) {
                V.append("\n    ");
                V.append(bVar);
            }
            String sb2 = V.toString();
            m.checkExpressionValueIsNotNull(sb2, "StringBuilder().apply(builderAction).toString()");
            throw new SSLPeerUnverifiedException(sb2);
        }
    }

    public final g b(c cVar) {
        m.checkParameterIsNotNull(cVar, "certificateChainCleaner");
        return m.areEqual(this.d, cVar) ? this : new g(this.c, cVar);
    }

    public boolean equals(Object obj) {
        if (obj instanceof g) {
            g gVar = (g) obj;
            if (m.areEqual(gVar.c, this.c) && m.areEqual(gVar.d, this.d)) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        int hashCode = (this.c.hashCode() + 1517) * 41;
        c cVar = this.d;
        return hashCode + (cVar != null ? cVar.hashCode() : 0);
    }

    public g(Set<b> set, c cVar) {
        m.checkParameterIsNotNull(set, "pins");
        this.c = set;
        this.d = cVar;
    }
}
