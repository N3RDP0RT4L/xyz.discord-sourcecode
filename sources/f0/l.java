package f0;

import d0.z.d.m;
import f0.e0.f.d;
import f0.e0.g.k;
import java.util.concurrent.TimeUnit;
/* compiled from: ConnectionPool.kt */
/* loaded from: classes3.dex */
public final class l {
    public final k a;

    public l() {
        TimeUnit timeUnit = TimeUnit.MINUTES;
        m.checkParameterIsNotNull(timeUnit, "timeUnit");
        k kVar = new k(d.a, 5, 5L, timeUnit);
        m.checkParameterIsNotNull(kVar, "delegate");
        this.a = kVar;
    }
}
