package f0;

import com.adjust.sdk.Constants;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.google.android.material.badge.BadgeDrawable;
import d0.g0.c;
import d0.t.o;
import d0.z.d.m;
import g0.e;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import kotlin.TypeCastException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.text.Regex;
import org.objectweb.asm.Opcodes;
/* compiled from: HttpUrl.kt */
/* loaded from: classes3.dex */
public final class w {
    public final boolean c;
    public final String d;
    public final String e;
    public final String f;
    public final String g;
    public final int h;
    public final List<String> i;
    public final List<String> j;
    public final String k;
    public final String l;

    /* renamed from: b  reason: collision with root package name */
    public static final b f3653b = new b(null);
    public static final char[] a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    /* compiled from: HttpUrl.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public static final C0391a a = new C0391a(null);

        /* renamed from: b  reason: collision with root package name */
        public String f3654b;
        public String e;
        public final List<String> g;
        public List<String> h;
        public String i;
        public String c = "";
        public String d = "";
        public int f = -1;

        /* compiled from: HttpUrl.kt */
        /* renamed from: f0.w$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0391a {
            public C0391a(DefaultConstructorMarker defaultConstructorMarker) {
            }
        }

        public a() {
            ArrayList arrayList = new ArrayList();
            this.g = arrayList;
            arrayList.add("");
        }

        public final a a(String str, String str2) {
            m.checkParameterIsNotNull(str, "encodedName");
            if (this.h == null) {
                this.h = new ArrayList();
            }
            List<String> list = this.h;
            if (list == null) {
                m.throwNpe();
            }
            b bVar = w.f3653b;
            list.add(b.a(bVar, str, 0, 0, " \"'<>#&=", true, false, true, false, null, 211));
            List<String> list2 = this.h;
            if (list2 == null) {
                m.throwNpe();
            }
            list2.add(str2 != null ? b.a(bVar, str2, 0, 0, " \"'<>#&=", true, false, true, false, null, 211) : null);
            return this;
        }

        public final w b() {
            ArrayList arrayList;
            String str = this.f3654b;
            if (str != null) {
                b bVar = w.f3653b;
                String d = b.d(bVar, this.c, 0, 0, false, 7);
                String d2 = b.d(bVar, this.d, 0, 0, false, 7);
                String str2 = this.e;
                if (str2 != null) {
                    int c = c();
                    List<String> list = this.g;
                    ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(list, 10));
                    for (String str3 : list) {
                        arrayList2.add(b.d(w.f3653b, str3, 0, 0, false, 7));
                    }
                    List<String> list2 = this.h;
                    if (list2 != null) {
                        arrayList = new ArrayList(o.collectionSizeOrDefault(list2, 10));
                        for (String str4 : list2) {
                            arrayList.add(str4 != null ? b.d(w.f3653b, str4, 0, 0, true, 3) : null);
                        }
                    } else {
                        arrayList = null;
                    }
                    String str5 = this.i;
                    return new w(str, d, d2, str2, c, arrayList2, arrayList, str5 != null ? b.d(w.f3653b, str5, 0, 0, false, 7) : null, toString());
                }
                throw new IllegalStateException("host == null");
            }
            throw new IllegalStateException("scheme == null");
        }

        public final int c() {
            int i = this.f;
            if (i != -1) {
                return i;
            }
            String str = this.f3654b;
            if (str == null) {
                m.throwNpe();
            }
            m.checkParameterIsNotNull(str, "scheme");
            int hashCode = str.hashCode();
            if (hashCode != 3213448) {
                if (hashCode == 99617003 && str.equals(Constants.SCHEME)) {
                    return 443;
                }
            } else if (str.equals("http")) {
                return 80;
            }
            return -1;
        }

        public final a d(String str) {
            List<String> list;
            if (str != null) {
                b bVar = w.f3653b;
                String a2 = b.a(bVar, str, 0, 0, " \"'<>#", true, false, true, false, null, 211);
                if (a2 != null) {
                    list = bVar.e(a2);
                    this.h = list;
                    return this;
                }
            }
            list = null;
            this.h = list;
            return this;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
            jadx.core.utils.exceptions.JadxRuntimeException: Unreachable block: B:109:0x0260
            	at jadx.core.dex.visitors.blocks.BlockProcessor.checkForUnreachableBlocks(BlockProcessor.java:92)
            	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:52)
            	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:44)
            */
        public final f0.w.a e(f0.w r30, java.lang.String r31) {
            /*
                Method dump skipped, instructions count: 1194
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: f0.w.a.e(f0.w, java.lang.String):f0.w$a");
        }

        /* JADX WARN: Code restructure failed: missing block: B:16:0x0033, code lost:
            if ((r9.d.length() > 0) != false) goto L17;
         */
        /* JADX WARN: Code restructure failed: missing block: B:49:0x00b8, code lost:
            if (r1 != r5) goto L50;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public java.lang.String toString() {
            /*
                Method dump skipped, instructions count: 345
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: f0.w.a.toString():java.lang.String");
        }
    }

    /* compiled from: HttpUrl.kt */
    /* loaded from: classes3.dex */
    public static final class b {
        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public static String a(b bVar, String str, int i, int i2, String str2, boolean z2, boolean z3, boolean z4, boolean z5, Charset charset, int i3) {
            String str3;
            boolean z6;
            String str4;
            int i4 = (i3 & 1) != 0 ? 0 : i;
            int length = (i3 & 2) != 0 ? str.length() : i2;
            boolean z7 = (i3 & 8) != 0 ? false : z2;
            boolean z8 = (i3 & 16) != 0 ? false : z3;
            boolean z9 = (i3 & 32) != 0 ? false : z4;
            boolean z10 = (i3 & 64) != 0 ? false : z5;
            Charset charset2 = (i3 & 128) != 0 ? null : charset;
            m.checkParameterIsNotNull(str, "$this$canonicalize");
            m.checkParameterIsNotNull(str2, "encodeSet");
            int i5 = i4;
            while (i5 < length) {
                int codePointAt = str.codePointAt(i5);
                int i6 = 2;
                if (codePointAt < 32 || codePointAt == 127 || (codePointAt >= 128 && !z10)) {
                    str3 = "(this as java.lang.Strin…ing(startIndex, endIndex)";
                } else {
                    str3 = "(this as java.lang.Strin…ing(startIndex, endIndex)";
                    if (!d0.g0.w.contains$default((CharSequence) str2, (char) codePointAt, false, 2, (Object) null) && ((codePointAt != 37 || (z7 && (!z8 || bVar.c(str, i5, length)))) && (codePointAt != 43 || !z9))) {
                        i5 += Character.charCount(codePointAt);
                    }
                }
                e eVar = new e();
                eVar.c0(str, i4, i5);
                e eVar2 = null;
                while (i5 < length) {
                    int codePointAt2 = str.codePointAt(i5);
                    if (!z7 || !(codePointAt2 == 9 || codePointAt2 == 10 || codePointAt2 == 12 || codePointAt2 == 13)) {
                        if (codePointAt2 != 43 || !z9) {
                            if (codePointAt2 < 32 || codePointAt2 == 127 || (codePointAt2 >= 128 && !z10)) {
                                z6 = z9;
                            } else {
                                z6 = z9;
                                if (!d0.g0.w.contains$default((CharSequence) str2, (char) codePointAt2, false, i6, (Object) null) && (codePointAt2 != 37 || (z7 && (!z8 || bVar.c(str, i5, length))))) {
                                    eVar.d0(codePointAt2);
                                    str4 = str3;
                                    i5 += Character.charCount(codePointAt2);
                                    i6 = 2;
                                    z9 = z6;
                                    str3 = str4;
                                }
                            }
                            if (eVar2 == null) {
                                eVar2 = new e();
                            }
                            if (charset2 == null || m.areEqual(charset2, StandardCharsets.UTF_8)) {
                                str4 = str3;
                                eVar2.d0(codePointAt2);
                            } else {
                                int charCount = Character.charCount(codePointAt2) + i5;
                                m.checkParameterIsNotNull(str, "string");
                                m.checkParameterIsNotNull(charset2, "charset");
                                boolean z11 = true;
                                if (i5 >= 0) {
                                    if (charCount >= i5) {
                                        if (charCount > str.length()) {
                                            z11 = false;
                                        }
                                        if (!z11) {
                                            StringBuilder S = b.d.b.a.a.S("endIndex > string.length: ", charCount, " > ");
                                            S.append(str.length());
                                            throw new IllegalArgumentException(S.toString().toString());
                                        } else if (m.areEqual(charset2, c.a)) {
                                            eVar2.c0(str, i5, charCount);
                                            str4 = str3;
                                        } else {
                                            String substring = str.substring(i5, charCount);
                                            str4 = str3;
                                            m.checkExpressionValueIsNotNull(substring, str4);
                                            if (substring != null) {
                                                byte[] bytes = substring.getBytes(charset2);
                                                m.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
                                                eVar2.S(bytes, 0, bytes.length);
                                            } else {
                                                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                                            }
                                        }
                                    } else {
                                        throw new IllegalArgumentException(b.d.b.a.a.r("endIndex < beginIndex: ", charCount, " < ", i5).toString());
                                    }
                                } else {
                                    throw new IllegalArgumentException(b.d.b.a.a.p("beginIndex < 0: ", i5).toString());
                                }
                            }
                            while (!eVar2.w()) {
                                int readByte = eVar2.readByte() & 255;
                                eVar.T(37);
                                char[] cArr = w.a;
                                eVar.T(cArr[(readByte >> 4) & 15]);
                                eVar.T(cArr[readByte & 15]);
                            }
                            i5 += Character.charCount(codePointAt2);
                            i6 = 2;
                            z9 = z6;
                            str3 = str4;
                        } else {
                            eVar.b0(z7 ? BadgeDrawable.DEFAULT_EXCEED_MAX_BADGE_NUMBER_SUFFIX : "%2B");
                        }
                    }
                    z6 = z9;
                    str4 = str3;
                    i5 += Character.charCount(codePointAt2);
                    i6 = 2;
                    z9 = z6;
                    str3 = str4;
                }
                return eVar.D();
            }
            String substring2 = str.substring(i4, length);
            m.checkExpressionValueIsNotNull(substring2, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            return substring2;
        }

        public static String d(b bVar, String str, int i, int i2, boolean z2, int i3) {
            int i4;
            if ((i3 & 1) != 0) {
                i = 0;
            }
            if ((i3 & 2) != 0) {
                i2 = str.length();
            }
            if ((i3 & 4) != 0) {
                z2 = false;
            }
            m.checkParameterIsNotNull(str, "$this$percentDecode");
            int i5 = i;
            while (i5 < i2) {
                char charAt = str.charAt(i5);
                if (charAt == '%' || (charAt == '+' && z2)) {
                    e eVar = new e();
                    eVar.c0(str, i, i5);
                    while (i5 < i2) {
                        int codePointAt = str.codePointAt(i5);
                        if (codePointAt != 37 || (i4 = i5 + 2) >= i2) {
                            if (codePointAt == 43 && z2) {
                                eVar.T(32);
                                i5++;
                            }
                            eVar.d0(codePointAt);
                            i5 += Character.charCount(codePointAt);
                        } else {
                            int r = f0.e0.c.r(str.charAt(i5 + 1));
                            int r2 = f0.e0.c.r(str.charAt(i4));
                            if (!(r == -1 || r2 == -1)) {
                                eVar.T((r << 4) + r2);
                                i5 = Character.charCount(codePointAt) + i4;
                            }
                            eVar.d0(codePointAt);
                            i5 += Character.charCount(codePointAt);
                        }
                    }
                    return eVar.D();
                }
                i5++;
            }
            String substring = str.substring(i, i2);
            m.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            return substring;
        }

        public final int b(String str) {
            m.checkParameterIsNotNull(str, "scheme");
            int hashCode = str.hashCode();
            if (hashCode != 3213448) {
                if (hashCode == 99617003 && str.equals(Constants.SCHEME)) {
                    return 443;
                }
            } else if (str.equals("http")) {
                return 80;
            }
            return -1;
        }

        public final boolean c(String str, int i, int i2) {
            int i3 = i + 2;
            return i3 < i2 && str.charAt(i) == '%' && f0.e0.c.r(str.charAt(i + 1)) != -1 && f0.e0.c.r(str.charAt(i3)) != -1;
        }

        public final List<String> e(String str) {
            m.checkParameterIsNotNull(str, "$this$toQueryNamesAndValues");
            ArrayList arrayList = new ArrayList();
            int i = 0;
            while (i <= str.length()) {
                int indexOf$default = d0.g0.w.indexOf$default((CharSequence) str, '&', i, false, 4, (Object) null);
                if (indexOf$default == -1) {
                    indexOf$default = str.length();
                }
                int i2 = indexOf$default;
                int indexOf$default2 = d0.g0.w.indexOf$default((CharSequence) str, '=', i, false, 4, (Object) null);
                if (indexOf$default2 == -1 || indexOf$default2 > i2) {
                    String substring = str.substring(i, i2);
                    m.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                    arrayList.add(substring);
                    arrayList.add(null);
                } else {
                    String substring2 = str.substring(i, indexOf$default2);
                    m.checkExpressionValueIsNotNull(substring2, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                    arrayList.add(substring2);
                    String substring3 = str.substring(indexOf$default2 + 1, i2);
                    m.checkExpressionValueIsNotNull(substring3, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                    arrayList.add(substring3);
                }
                i = i2 + 1;
            }
            return arrayList;
        }
    }

    public w(String str, String str2, String str3, String str4, int i, List<String> list, List<String> list2, String str5, String str6) {
        m.checkParameterIsNotNull(str, "scheme");
        m.checkParameterIsNotNull(str2, "username");
        m.checkParameterIsNotNull(str3, "password");
        m.checkParameterIsNotNull(str4, "host");
        m.checkParameterIsNotNull(list, "pathSegments");
        m.checkParameterIsNotNull(str6, "url");
        this.d = str;
        this.e = str2;
        this.f = str3;
        this.g = str4;
        this.h = i;
        this.i = list;
        this.j = list2;
        this.k = str5;
        this.l = str6;
        this.c = m.areEqual(str, Constants.SCHEME);
    }

    public final String a() {
        if (this.f.length() == 0) {
            return "";
        }
        int indexOf$default = d0.g0.w.indexOf$default((CharSequence) this.l, (char) MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR, this.d.length() + 3, false, 4, (Object) null) + 1;
        int indexOf$default2 = d0.g0.w.indexOf$default((CharSequence) this.l, (char) MentionUtilsKt.MENTIONS_CHAR, 0, false, 6, (Object) null);
        String str = this.l;
        if (str != null) {
            String substring = str.substring(indexOf$default, indexOf$default2);
            m.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            return substring;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    public final String b() {
        int indexOf$default = d0.g0.w.indexOf$default((CharSequence) this.l, (char) MentionUtilsKt.SLASH_CHAR, this.d.length() + 3, false, 4, (Object) null);
        String str = this.l;
        int g = f0.e0.c.g(str, "?#", indexOf$default, str.length());
        String str2 = this.l;
        if (str2 != null) {
            String substring = str2.substring(indexOf$default, g);
            m.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            return substring;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    public final List<String> c() {
        int indexOf$default = d0.g0.w.indexOf$default((CharSequence) this.l, (char) MentionUtilsKt.SLASH_CHAR, this.d.length() + 3, false, 4, (Object) null);
        String str = this.l;
        int g = f0.e0.c.g(str, "?#", indexOf$default, str.length());
        ArrayList arrayList = new ArrayList();
        while (indexOf$default < g) {
            int i = indexOf$default + 1;
            int f = f0.e0.c.f(this.l, MentionUtilsKt.SLASH_CHAR, i, g);
            String str2 = this.l;
            if (str2 != null) {
                String substring = str2.substring(i, f);
                m.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                arrayList.add(substring);
                indexOf$default = f;
            } else {
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
        }
        return arrayList;
    }

    public final String d() {
        if (this.j == null) {
            return null;
        }
        int indexOf$default = d0.g0.w.indexOf$default((CharSequence) this.l, '?', 0, false, 6, (Object) null) + 1;
        String str = this.l;
        int f = f0.e0.c.f(str, MentionUtilsKt.CHANNELS_CHAR, indexOf$default, str.length());
        String str2 = this.l;
        if (str2 != null) {
            String substring = str2.substring(indexOf$default, f);
            m.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            return substring;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    public final String e() {
        if (this.e.length() == 0) {
            return "";
        }
        int length = this.d.length() + 3;
        String str = this.l;
        int g = f0.e0.c.g(str, ":@", length, str.length());
        String str2 = this.l;
        if (str2 != null) {
            String substring = str2.substring(length, g);
            m.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            return substring;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    public boolean equals(Object obj) {
        return (obj instanceof w) && m.areEqual(((w) obj).l, this.l);
    }

    public final a f() {
        int i;
        String str;
        a aVar = new a();
        aVar.f3654b = this.d;
        String e = e();
        m.checkParameterIsNotNull(e, "<set-?>");
        aVar.c = e;
        String a2 = a();
        m.checkParameterIsNotNull(a2, "<set-?>");
        aVar.d = a2;
        aVar.e = this.g;
        int i2 = this.h;
        String str2 = this.d;
        m.checkParameterIsNotNull(str2, "scheme");
        int hashCode = str2.hashCode();
        int i3 = -1;
        if (hashCode != 3213448) {
            if (hashCode == 99617003 && str2.equals(Constants.SCHEME)) {
                i = 443;
            }
            i = -1;
        } else {
            if (str2.equals("http")) {
                i = 80;
            }
            i = -1;
        }
        if (i2 != i) {
            i3 = this.h;
        }
        aVar.f = i3;
        aVar.g.clear();
        aVar.g.addAll(c());
        aVar.d(d());
        if (this.k == null) {
            str = null;
        } else {
            int indexOf$default = d0.g0.w.indexOf$default((CharSequence) this.l, (char) MentionUtilsKt.CHANNELS_CHAR, 0, false, 6, (Object) null) + 1;
            String str3 = this.l;
            if (str3 != null) {
                str = str3.substring(indexOf$default);
                m.checkExpressionValueIsNotNull(str, "(this as java.lang.String).substring(startIndex)");
            } else {
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
        }
        aVar.i = str;
        return aVar;
    }

    public final a g(String str) {
        m.checkParameterIsNotNull(str, "link");
        try {
            a aVar = new a();
            aVar.e(this, str);
            return aVar;
        } catch (IllegalArgumentException unused) {
            return null;
        }
    }

    public final String h() {
        a g = g("/...");
        if (g == null) {
            m.throwNpe();
        }
        Objects.requireNonNull(g);
        m.checkParameterIsNotNull("", "username");
        b bVar = f3653b;
        g.c = b.a(bVar, "", 0, 0, " \"':;<=>@[]^`{}|/\\?#", false, false, false, false, null, 251);
        m.checkParameterIsNotNull("", "password");
        g.d = b.a(bVar, "", 0, 0, " \"':;<=>@[]^`{}|/\\?#", false, false, false, false, null, 251);
        return g.b().l;
    }

    public int hashCode() {
        return this.l.hashCode();
    }

    public final URI i() {
        a f = f();
        String str = f.e;
        String str2 = null;
        f.e = str != null ? new Regex("[\"<>^`{|}]").replace(str, "") : null;
        int size = f.g.size();
        for (int i = 0; i < size; i++) {
            List<String> list = f.g;
            list.set(i, b.a(f3653b, list.get(i), 0, 0, "[]", true, true, false, false, null, 227));
        }
        List<String> list2 = f.h;
        if (list2 != null) {
            int size2 = list2.size();
            for (int i2 = 0; i2 < size2; i2++) {
                String str3 = list2.get(i2);
                list2.set(i2, str3 != null ? b.a(f3653b, str3, 0, 0, "\\^`{|}", true, true, true, false, null, Opcodes.MONITOREXIT) : null);
            }
        }
        String str4 = f.i;
        if (str4 != null) {
            str2 = b.a(f3653b, str4, 0, 0, " \"#<>\\^`{|}", true, true, false, true, null, Opcodes.IF_ICMPGT);
        }
        f.i = str2;
        String aVar = f.toString();
        try {
            return new URI(aVar);
        } catch (URISyntaxException e) {
            try {
                URI create = URI.create(new Regex("[\\u0000-\\u001F\\u007F-\\u009F\\p{javaWhitespace}]").replace(aVar, ""));
                m.checkExpressionValueIsNotNull(create, "URI.create(stripped)");
                return create;
            } catch (Exception unused) {
                throw new RuntimeException(e);
            }
        }
    }

    public String toString() {
        return this.l;
    }
}
