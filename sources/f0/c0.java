package f0;

import b.d.b.a.a;
import d0.z.d.m;
import java.net.InetSocketAddress;
import java.net.Proxy;
/* compiled from: Route.kt */
/* loaded from: classes3.dex */
public final class c0 {
    public final a a;

    /* renamed from: b  reason: collision with root package name */
    public final Proxy f3575b;
    public final InetSocketAddress c;

    public c0(a aVar, Proxy proxy, InetSocketAddress inetSocketAddress) {
        m.checkParameterIsNotNull(aVar, "address");
        m.checkParameterIsNotNull(proxy, "proxy");
        m.checkParameterIsNotNull(inetSocketAddress, "socketAddress");
        this.a = aVar;
        this.f3575b = proxy;
        this.c = inetSocketAddress;
    }

    public final boolean a() {
        return this.a.f != null && this.f3575b.type() == Proxy.Type.HTTP;
    }

    public boolean equals(Object obj) {
        if (obj instanceof c0) {
            c0 c0Var = (c0) obj;
            if (m.areEqual(c0Var.a, this.a) && m.areEqual(c0Var.f3575b, this.f3575b) && m.areEqual(c0Var.c, this.c)) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        int hashCode = this.f3575b.hashCode();
        return this.c.hashCode() + ((hashCode + ((this.a.hashCode() + 527) * 31)) * 31);
    }

    public String toString() {
        StringBuilder R = a.R("Route{");
        R.append(this.c);
        R.append('}');
        return R.toString();
    }
}
