package f0;

import d0.z.d.m;
import java.io.IOException;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: Protocol.kt */
/* loaded from: classes3.dex */
public enum y {
    HTTP_1_0("http/1.0"),
    HTTP_1_1("http/1.1"),
    SPDY_3("spdy/3.1"),
    HTTP_2("h2"),
    H2_PRIOR_KNOWLEDGE("h2_prior_knowledge"),
    QUIC("quic");
    
    public static final a q = new a(null);
    private final String protocol;

    /* compiled from: Protocol.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final y a(String str) throws IOException {
            m.checkParameterIsNotNull(str, "protocol");
            y yVar = y.HTTP_1_0;
            if (!m.areEqual(str, yVar.protocol)) {
                yVar = y.HTTP_1_1;
                if (!m.areEqual(str, yVar.protocol)) {
                    yVar = y.H2_PRIOR_KNOWLEDGE;
                    if (!m.areEqual(str, yVar.protocol)) {
                        yVar = y.HTTP_2;
                        if (!m.areEqual(str, yVar.protocol)) {
                            yVar = y.SPDY_3;
                            if (!m.areEqual(str, yVar.protocol)) {
                                yVar = y.QUIC;
                                if (!m.areEqual(str, yVar.protocol)) {
                                    throw new IOException(b.d.b.a.a.v("Unexpected protocol: ", str));
                                }
                            }
                        }
                    }
                }
            }
            return yVar;
        }
    }

    y(String str) {
        this.protocol = str;
    }

    @Override // java.lang.Enum
    public String toString() {
        return this.protocol;
    }
}
