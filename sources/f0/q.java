package f0;

import androidx.core.app.NotificationCompat;
import d0.z.d.m;
import f0.e0.g.e;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
/* compiled from: Dispatcher.kt */
/* loaded from: classes3.dex */
public final class q {
    public ExecutorService a;

    /* renamed from: b  reason: collision with root package name */
    public final ArrayDeque<e.a> f3649b = new ArrayDeque<>();
    public final ArrayDeque<e.a> c = new ArrayDeque<>();
    public final ArrayDeque<e> d = new ArrayDeque<>();

    public final synchronized void a() {
        Iterator<e.a> it = this.f3649b.iterator();
        while (it.hasNext()) {
            it.next().l.cancel();
        }
        Iterator<e.a> it2 = this.c.iterator();
        while (it2.hasNext()) {
            it2.next().l.cancel();
        }
        Iterator<e> it3 = this.d.iterator();
        while (it3.hasNext()) {
            it3.next().cancel();
        }
    }

    public final <T> void b(Deque<T> deque, T t) {
        synchronized (this) {
            if (!deque.remove(t)) {
                throw new AssertionError("Call wasn't in-flight!");
            }
        }
        d();
    }

    public final void c(e.a aVar) {
        m.checkParameterIsNotNull(aVar, NotificationCompat.CATEGORY_CALL);
        aVar.j.decrementAndGet();
        b(this.c, aVar);
    }

    /* JADX WARN: Removed duplicated region for block: B:25:0x0067  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final boolean d() {
        /*
            Method dump skipped, instructions count: 249
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: f0.q.d():boolean");
    }
}
