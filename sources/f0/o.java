package f0;

import d0.t.n;
import d0.z.d.m;
import java.util.List;
/* compiled from: CookieJar.kt */
/* loaded from: classes3.dex */
public final class o implements p {
    @Override // f0.p
    public void a(w wVar, List<n> list) {
        m.checkParameterIsNotNull(wVar, "url");
        m.checkParameterIsNotNull(list, "cookies");
    }

    @Override // f0.p
    public List<n> b(w wVar) {
        m.checkParameterIsNotNull(wVar, "url");
        return n.emptyList();
    }
}
