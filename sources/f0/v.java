package f0;

import d0.g;
import d0.t.n;
import d0.z.d.m;
import d0.z.d.o;
import f0.e0.c;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import kotlin.Lazy;
import kotlin.jvm.functions.Function0;
/* compiled from: Handshake.kt */
/* loaded from: classes3.dex */
public final class v {
    public final Lazy a;

    /* renamed from: b  reason: collision with root package name */
    public final d0 f3652b;
    public final j c;
    public final List<Certificate> d;

    /* compiled from: Handshake.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function0<List<? extends Certificate>> {
        public final /* synthetic */ List $peerCertificatesCopy;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(List list) {
            super(0);
            this.$peerCertificatesCopy = list;
        }

        @Override // kotlin.jvm.functions.Function0
        public List<? extends Certificate> invoke() {
            return this.$peerCertificatesCopy;
        }
    }

    /* compiled from: Handshake.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function0<List<? extends Certificate>> {
        public final /* synthetic */ Function0 $peerCertificatesFn;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(Function0 function0) {
            super(0);
            this.$peerCertificatesFn = function0;
        }

        @Override // kotlin.jvm.functions.Function0
        public List<? extends Certificate> invoke() {
            try {
                return (List) this.$peerCertificatesFn.invoke();
            } catch (SSLPeerUnverifiedException unused) {
                return n.emptyList();
            }
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public v(d0 d0Var, j jVar, List<? extends Certificate> list, Function0<? extends List<? extends Certificate>> function0) {
        m.checkParameterIsNotNull(d0Var, "tlsVersion");
        m.checkParameterIsNotNull(jVar, "cipherSuite");
        m.checkParameterIsNotNull(list, "localCertificates");
        m.checkParameterIsNotNull(function0, "peerCertificatesFn");
        this.f3652b = d0Var;
        this.c = jVar;
        this.d = list;
        this.a = g.lazy(new b(function0));
    }

    public static final v a(SSLSession sSLSession) throws IOException {
        List<Certificate> list;
        m.checkParameterIsNotNull(sSLSession, "$this$handshake");
        String cipherSuite = sSLSession.getCipherSuite();
        if (cipherSuite != null) {
            int hashCode = cipherSuite.hashCode();
            if (hashCode == 1019404634 ? cipherSuite.equals("TLS_NULL_WITH_NULL_NULL") : hashCode == 1208658923 && cipherSuite.equals("SSL_NULL_WITH_NULL_NULL")) {
                throw new IOException(b.d.b.a.a.v("cipherSuite == ", cipherSuite));
            }
            j b2 = j.f3645s.b(cipherSuite);
            String protocol = sSLSession.getProtocol();
            if (protocol == null) {
                throw new IllegalStateException("tlsVersion == null".toString());
            } else if (!m.areEqual("NONE", protocol)) {
                d0 a2 = d0.p.a(protocol);
                try {
                    list = d(sSLSession.getPeerCertificates());
                } catch (SSLPeerUnverifiedException unused) {
                    list = n.emptyList();
                }
                return new v(a2, b2, d(sSLSession.getLocalCertificates()), new a(list));
            } else {
                throw new IOException("tlsVersion == NONE");
            }
        } else {
            throw new IllegalStateException("cipherSuite == null".toString());
        }
    }

    public static final List<Certificate> d(Certificate[] certificateArr) {
        if (certificateArr != null) {
            return c.m((Certificate[]) Arrays.copyOf(certificateArr, certificateArr.length));
        }
        return n.emptyList();
    }

    public final String b(Certificate certificate) {
        if (certificate instanceof X509Certificate) {
            return ((X509Certificate) certificate).getSubjectDN().toString();
        }
        String type = certificate.getType();
        m.checkExpressionValueIsNotNull(type, "type");
        return type;
    }

    public final List<Certificate> c() {
        return (List) this.a.getValue();
    }

    public boolean equals(Object obj) {
        if (obj instanceof v) {
            v vVar = (v) obj;
            if (vVar.f3652b == this.f3652b && m.areEqual(vVar.c, this.c) && m.areEqual(vVar.c(), c()) && m.areEqual(vVar.d, this.d)) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        int hashCode = this.c.hashCode();
        int hashCode2 = c().hashCode();
        return this.d.hashCode() + ((hashCode2 + ((hashCode + ((this.f3652b.hashCode() + 527) * 31)) * 31)) * 31);
    }

    public String toString() {
        List<Certificate> c = c();
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(c, 10));
        for (Certificate certificate : c) {
            arrayList.add(b(certificate));
        }
        String obj = arrayList.toString();
        StringBuilder V = b.d.b.a.a.V("Handshake{", "tlsVersion=");
        V.append(this.f3652b);
        V.append(' ');
        V.append("cipherSuite=");
        V.append(this.c);
        V.append(' ');
        V.append("peerCertificates=");
        V.append(obj);
        V.append(' ');
        V.append("localCertificates=");
        List<Certificate> list = this.d;
        ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(list, 10));
        for (Certificate certificate2 : list) {
            arrayList2.add(b(certificate2));
        }
        V.append(arrayList2);
        V.append('}');
        return V.toString();
    }
}
