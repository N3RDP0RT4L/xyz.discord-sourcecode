package i0;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.concurrent.Executor;
import org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement;
/* compiled from: Platform.java */
/* loaded from: classes3.dex */
public class u {
    public static final u a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f3730b;
    public final Constructor<MethodHandles.Lookup> c;

    /* compiled from: Platform.java */
    /* loaded from: classes3.dex */
    public static final class a extends u {

        /* compiled from: Platform.java */
        /* renamed from: i0.u$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class ExecutorC0396a implements Executor {
            public final Handler j = new Handler(Looper.getMainLooper());

            @Override // java.util.concurrent.Executor
            public void execute(Runnable runnable) {
                this.j.post(runnable);
            }
        }

        public a() {
            super(Build.VERSION.SDK_INT >= 24);
        }

        @Override // i0.u
        public Executor a() {
            return new ExecutorC0396a();
        }

        @Override // i0.u
        public Object b(Method method, Class<?> cls, Object obj, Object... objArr) throws Throwable {
            if (Build.VERSION.SDK_INT >= 26) {
                return u.super.b(method, cls, obj, objArr);
            }
            throw new UnsupportedOperationException("Calling default methods on API 24 and 25 is not supported");
        }
    }

    static {
        u uVar;
        if ("Dalvik".equals(System.getProperty("java.vm.name"))) {
            uVar = new a();
        } else {
            uVar = new u(true);
        }
        a = uVar;
    }

    public u(boolean z2) {
        this.f3730b = z2;
        Constructor<MethodHandles.Lookup> constructor = null;
        if (z2) {
            try {
                constructor = MethodHandles.Lookup.class.getDeclaredConstructor(Class.class, Integer.TYPE);
                constructor.setAccessible(true);
            } catch (NoClassDefFoundError | NoSuchMethodException unused) {
            }
        }
        this.c = constructor;
    }

    public Executor a() {
        return null;
    }

    @IgnoreJRERequirement
    public Object b(Method method, Class<?> cls, Object obj, Object... objArr) throws Throwable {
        Constructor<MethodHandles.Lookup> constructor = this.c;
        return (constructor != null ? constructor.newInstance(cls, -1) : MethodHandles.lookup()).unreflectSpecial(method, cls).bindTo(obj).invokeWithArguments(objArr);
    }
}
