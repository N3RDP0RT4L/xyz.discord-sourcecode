package i0;

import androidx.browser.trusted.sharing.ShareTarget;
import b.d.b.a.a;
import f0.e;
import i0.c0;
import i0.f0.b;
import i0.f0.c;
import i0.f0.d;
import i0.f0.e;
import i0.f0.f;
import i0.f0.g;
import i0.f0.h;
import i0.f0.i;
import i0.f0.j;
import i0.f0.k;
import i0.f0.l;
import i0.f0.m;
import i0.f0.n;
import i0.f0.o;
import i0.f0.p;
import i0.f0.q;
import i0.f0.r;
import i0.f0.s;
import i0.f0.u;
import i0.f0.v;
import i0.f0.x;
import i0.f0.y;
import i0.j;
import i0.t;
import i0.w;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import java.net.URI;
import java.util.Map;
import kotlin.coroutines.Continuation;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
/* compiled from: ServiceMethod.java */
/* loaded from: classes3.dex */
public abstract class z<T> {
    public static <T> z<T> b(y yVar, Method method) {
        Annotation[] annotationArr;
        Type type;
        boolean z2;
        Type[] genericParameterTypes;
        int i;
        int i2;
        t<?> tVar;
        int i3;
        int i4;
        int i5;
        t<?> tVar2;
        t<?> tVar3;
        int i6;
        t<?> tVar4;
        t<?> tVar5;
        t<?> cVar;
        w.a aVar = new w.a(yVar, method);
        for (Annotation annotation : aVar.e) {
            if (annotation instanceof b) {
                aVar.b("DELETE", ((b) annotation).value(), false);
            } else if (annotation instanceof f) {
                aVar.b(ShareTarget.METHOD_GET, ((f) annotation).value(), false);
            } else if (annotation instanceof g) {
                aVar.b("HEAD", ((g) annotation).value(), false);
            } else if (annotation instanceof n) {
                aVar.b("PATCH", ((n) annotation).value(), true);
            } else if (annotation instanceof o) {
                aVar.b(ShareTarget.METHOD_POST, ((o) annotation).value(), true);
            } else if (annotation instanceof p) {
                aVar.b("PUT", ((p) annotation).value(), true);
            } else if (annotation instanceof m) {
                aVar.b("OPTIONS", ((m) annotation).value(), false);
            } else if (annotation instanceof h) {
                h hVar = (h) annotation;
                aVar.b(hVar.method(), hVar.path(), hVar.hasBody());
            } else if (annotation instanceof k) {
                String[] value = ((k) annotation).value();
                if (value.length != 0) {
                    Headers.a aVar2 = new Headers.a();
                    for (String str : value) {
                        int indexOf = str.indexOf(58);
                        if (indexOf == -1 || indexOf == 0 || indexOf == str.length() - 1) {
                            throw c0.j(aVar.d, "@Headers value must be in the form \"Name: Value\". Found: \"%s\"", str);
                        }
                        String substring = str.substring(0, indexOf);
                        String trim = str.substring(indexOf + 1).trim();
                        if ("Content-Type".equalsIgnoreCase(substring)) {
                            try {
                                aVar.v = MediaType.b(trim);
                            } catch (IllegalArgumentException e) {
                                throw c0.k(aVar.d, e, "Malformed content type: %s", trim);
                            }
                        } else {
                            aVar2.a(substring, trim);
                        }
                    }
                    aVar.u = aVar2.c();
                } else {
                    throw c0.j(aVar.d, "@Headers annotation is empty.", new Object[0]);
                }
            } else if (annotation instanceof l) {
                if (!aVar.r) {
                    aVar.f3735s = true;
                } else {
                    throw c0.j(aVar.d, "Only one encoding annotation is allowed.", new Object[0]);
                }
            } else if (!(annotation instanceof e)) {
                continue;
            } else if (!aVar.f3735s) {
                aVar.r = true;
            } else {
                throw c0.j(aVar.d, "Only one encoding annotation is allowed.", new Object[0]);
            }
        }
        if (aVar.p != null) {
            if (!aVar.q) {
                if (aVar.f3735s) {
                    throw c0.j(aVar.d, "Multipart can only be specified on HTTP methods with request body (e.g., @POST).", new Object[0]);
                } else if (aVar.r) {
                    throw c0.j(aVar.d, "FormUrlEncoded can only be specified on HTTP methods with request body (e.g., @POST).", new Object[0]);
                }
            }
            int length = aVar.f.length;
            aVar.f3736x = new t[length];
            int i7 = length - 1;
            int i8 = 0;
            while (i8 < length) {
                t<?>[] tVarArr = aVar.f3736x;
                Type type2 = aVar.g[i8];
                Annotation[] annotationArr2 = aVar.f[i8];
                boolean z3 = i8 == i7;
                if (annotationArr2 != null) {
                    int length2 = annotationArr2.length;
                    tVar = null;
                    int i9 = 0;
                    while (i9 < length2) {
                        Annotation annotation2 = annotationArr2[i9];
                        length = length;
                        if (annotation2 instanceof y) {
                            aVar.c(i8, type2);
                            if (aVar.o) {
                                throw c0.l(aVar.d, i8, "Multiple @Url method annotations found.", new Object[0]);
                            } else if (aVar.k) {
                                throw c0.l(aVar.d, i8, "@Path parameters may not be used with @Url.", new Object[0]);
                            } else if (aVar.l) {
                                throw c0.l(aVar.d, i8, "A @Url parameter must not come after a @Query.", new Object[0]);
                            } else if (aVar.m) {
                                throw c0.l(aVar.d, i8, "A @Url parameter must not come after a @QueryName.", new Object[0]);
                            } else if (aVar.n) {
                                throw c0.l(aVar.d, i8, "A @Url parameter must not come after a @QueryMap.", new Object[0]);
                            } else if (aVar.t == null) {
                                aVar.o = true;
                                if (type2 == f0.w.class || type2 == String.class || type2 == URI.class || ((type2 instanceof Class) && "android.net.Uri".equals(((Class) type2).getName()))) {
                                    tVar2 = new t.n(aVar.d, i8);
                                    i5 = i7;
                                    i3 = i9;
                                    i4 = length2;
                                } else {
                                    throw c0.l(aVar.d, i8, "@Url must be okhttp3.HttpUrl, String, java.net.URI, or android.net.Uri type.", new Object[0]);
                                }
                            } else {
                                throw c0.l(aVar.d, i8, "@Url cannot be used with @%s URL", aVar.p);
                            }
                        } else {
                            i5 = i7;
                            if (annotation2 instanceof s) {
                                aVar.c(i8, type2);
                                if (aVar.l) {
                                    throw c0.l(aVar.d, i8, "A @Path parameter must not come after a @Query.", new Object[0]);
                                } else if (aVar.m) {
                                    throw c0.l(aVar.d, i8, "A @Path parameter must not come after a @QueryName.", new Object[0]);
                                } else if (aVar.n) {
                                    throw c0.l(aVar.d, i8, "A @Path parameter must not come after a @QueryMap.", new Object[0]);
                                } else if (aVar.o) {
                                    throw c0.l(aVar.d, i8, "@Path parameters may not be used with @Url.", new Object[0]);
                                } else if (aVar.t != null) {
                                    aVar.k = true;
                                    s sVar = (s) annotation2;
                                    String value2 = sVar.value();
                                    if (!w.a.f3734b.matcher(value2).matches()) {
                                        throw c0.l(aVar.d, i8, "@Path parameter name must match %s. Found: %s", w.a.a.pattern(), value2);
                                    } else if (aVar.w.contains(value2)) {
                                        i3 = i9;
                                        i6 = length2;
                                        tVar3 = new t.i<>(aVar.d, i8, value2, aVar.c.e(type2, annotationArr2), sVar.encoded());
                                    } else {
                                        throw c0.l(aVar.d, i8, "URL \"%s\" does not contain \"{%s}\".", aVar.t, value2);
                                    }
                                } else {
                                    throw c0.l(aVar.d, i8, "@Path can only be used with relative url on @%s", aVar.p);
                                }
                            } else {
                                i3 = i9;
                                i6 = length2;
                                if (annotation2 instanceof i0.f0.t) {
                                    aVar.c(i8, type2);
                                    i0.f0.t tVar6 = (i0.f0.t) annotation2;
                                    String value3 = tVar6.value();
                                    boolean encoded = tVar6.encoded();
                                    Class<?> f = c0.f(type2);
                                    aVar.l = true;
                                    if (Iterable.class.isAssignableFrom(f)) {
                                        if (type2 instanceof ParameterizedType) {
                                            tVar3 = new r(new t.j(value3, aVar.c.e(c0.e(0, (ParameterizedType) type2), annotationArr2), encoded));
                                        } else {
                                            throw c0.l(aVar.d, i8, a.o(f, new StringBuilder(), " must include generic type (e.g., ", "<String>)"), new Object[0]);
                                        }
                                    } else if (f.isArray()) {
                                        tVar3 = new s(new t.j(value3, aVar.c.e(w.a.a(f.getComponentType()), annotationArr2), encoded));
                                    } else {
                                        tVar4 = new t.j<>(value3, aVar.c.e(type2, annotationArr2), encoded);
                                        i4 = i6;
                                        tVar2 = tVar4;
                                    }
                                } else if (annotation2 instanceof v) {
                                    aVar.c(i8, type2);
                                    boolean encoded2 = ((v) annotation2).encoded();
                                    Class<?> f2 = c0.f(type2);
                                    aVar.m = true;
                                    if (Iterable.class.isAssignableFrom(f2)) {
                                        if (type2 instanceof ParameterizedType) {
                                            tVar3 = new r(new t.l(aVar.c.e(c0.e(0, (ParameterizedType) type2), annotationArr2), encoded2));
                                        } else {
                                            throw c0.l(aVar.d, i8, a.o(f2, new StringBuilder(), " must include generic type (e.g., ", "<String>)"), new Object[0]);
                                        }
                                    } else if (f2.isArray()) {
                                        tVar3 = new s(new t.l(aVar.c.e(w.a.a(f2.getComponentType()), annotationArr2), encoded2));
                                    } else {
                                        cVar = new t.l<>(aVar.c.e(type2, annotationArr2), encoded2);
                                        i4 = i6;
                                        tVar2 = cVar;
                                    }
                                } else {
                                    if (annotation2 instanceof u) {
                                        aVar.c(i8, type2);
                                        Class<?> f3 = c0.f(type2);
                                        aVar.n = true;
                                        if (Map.class.isAssignableFrom(f3)) {
                                            Type g = c0.g(type2, f3, Map.class);
                                            if (g instanceof ParameterizedType) {
                                                ParameterizedType parameterizedType = (ParameterizedType) g;
                                                Type e2 = c0.e(0, parameterizedType);
                                                if (String.class == e2) {
                                                    cVar = new t.k<>(aVar.d, i8, aVar.c.e(c0.e(1, parameterizedType), annotationArr2), ((u) annotation2).encoded());
                                                } else {
                                                    throw c0.l(aVar.d, i8, a.y("@QueryMap keys must be of type String: ", e2), new Object[0]);
                                                }
                                            } else {
                                                throw c0.l(aVar.d, i8, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                                            }
                                        } else {
                                            throw c0.l(aVar.d, i8, "@QueryMap parameter type must be Map.", new Object[0]);
                                        }
                                    } else if (annotation2 instanceof i) {
                                        aVar.c(i8, type2);
                                        String value4 = ((i) annotation2).value();
                                        Class<?> f4 = c0.f(type2);
                                        if (Iterable.class.isAssignableFrom(f4)) {
                                            if (type2 instanceof ParameterizedType) {
                                                tVar3 = new r(new t.d(value4, aVar.c.e(c0.e(0, (ParameterizedType) type2), annotationArr2)));
                                            } else {
                                                throw c0.l(aVar.d, i8, a.o(f4, new StringBuilder(), " must include generic type (e.g., ", "<String>)"), new Object[0]);
                                            }
                                        } else if (f4.isArray()) {
                                            tVar3 = new s(new t.d(value4, aVar.c.e(w.a.a(f4.getComponentType()), annotationArr2)));
                                        } else {
                                            cVar = new t.d<>(value4, aVar.c.e(type2, annotationArr2));
                                        }
                                    } else if (annotation2 instanceof j) {
                                        if (type2 == Headers.class) {
                                            tVar3 = new t.f(aVar.d, i8);
                                        } else {
                                            aVar.c(i8, type2);
                                            Class<?> f5 = c0.f(type2);
                                            if (Map.class.isAssignableFrom(f5)) {
                                                Type g2 = c0.g(type2, f5, Map.class);
                                                if (g2 instanceof ParameterizedType) {
                                                    ParameterizedType parameterizedType2 = (ParameterizedType) g2;
                                                    Type e3 = c0.e(0, parameterizedType2);
                                                    if (String.class == e3) {
                                                        tVar5 = new t.e<>(aVar.d, i8, aVar.c.e(c0.e(1, parameterizedType2), annotationArr2));
                                                        i4 = i6;
                                                        tVar2 = tVar5;
                                                    } else {
                                                        throw c0.l(aVar.d, i8, a.y("@HeaderMap keys must be of type String: ", e3), new Object[0]);
                                                    }
                                                } else {
                                                    throw c0.l(aVar.d, i8, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                                                }
                                            } else {
                                                throw c0.l(aVar.d, i8, "@HeaderMap parameter type must be Map.", new Object[0]);
                                            }
                                        }
                                    } else if (annotation2 instanceof c) {
                                        aVar.c(i8, type2);
                                        if (aVar.r) {
                                            c cVar2 = (c) annotation2;
                                            String value5 = cVar2.value();
                                            boolean encoded3 = cVar2.encoded();
                                            aVar.h = true;
                                            Class<?> f6 = c0.f(type2);
                                            if (Iterable.class.isAssignableFrom(f6)) {
                                                if (type2 instanceof ParameterizedType) {
                                                    tVar3 = new r(new t.b(value5, aVar.c.e(c0.e(0, (ParameterizedType) type2), annotationArr2), encoded3));
                                                } else {
                                                    throw c0.l(aVar.d, i8, a.o(f6, new StringBuilder(), " must include generic type (e.g., ", "<String>)"), new Object[0]);
                                                }
                                            } else if (f6.isArray()) {
                                                tVar3 = new s(new t.b(value5, aVar.c.e(w.a.a(f6.getComponentType()), annotationArr2), encoded3));
                                            } else {
                                                tVar4 = new t.b<>(value5, aVar.c.e(type2, annotationArr2), encoded3);
                                                i4 = i6;
                                                tVar2 = tVar4;
                                            }
                                        } else {
                                            throw c0.l(aVar.d, i8, "@Field parameters can only be used with form encoding.", new Object[0]);
                                        }
                                    } else if (annotation2 instanceof d) {
                                        aVar.c(i8, type2);
                                        if (aVar.r) {
                                            Class<?> f7 = c0.f(type2);
                                            if (Map.class.isAssignableFrom(f7)) {
                                                Type g3 = c0.g(type2, f7, Map.class);
                                                if (g3 instanceof ParameterizedType) {
                                                    ParameterizedType parameterizedType3 = (ParameterizedType) g3;
                                                    Type e4 = c0.e(0, parameterizedType3);
                                                    if (String.class == e4) {
                                                        h<T, String> e5 = aVar.c.e(c0.e(1, parameterizedType3), annotationArr2);
                                                        aVar.h = true;
                                                        cVar = new t.c<>(aVar.d, i8, e5, ((d) annotation2).encoded());
                                                    } else {
                                                        throw c0.l(aVar.d, i8, a.y("@FieldMap keys must be of type String: ", e4), new Object[0]);
                                                    }
                                                } else {
                                                    throw c0.l(aVar.d, i8, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                                                }
                                            } else {
                                                throw c0.l(aVar.d, i8, "@FieldMap parameter type must be Map.", new Object[0]);
                                            }
                                        } else {
                                            throw c0.l(aVar.d, i8, "@FieldMap parameters can only be used with form encoding.", new Object[0]);
                                        }
                                    } else if (annotation2 instanceof q) {
                                        aVar.c(i8, type2);
                                        if (aVar.f3735s) {
                                            q qVar = (q) annotation2;
                                            aVar.i = true;
                                            String value6 = qVar.value();
                                            Class<?> f8 = c0.f(type2);
                                            if (value6.isEmpty()) {
                                                if (Iterable.class.isAssignableFrom(f8)) {
                                                    if (!(type2 instanceof ParameterizedType)) {
                                                        throw c0.l(aVar.d, i8, a.o(f8, new StringBuilder(), " must include generic type (e.g., ", "<String>)"), new Object[0]);
                                                    } else if (MultipartBody.Part.class.isAssignableFrom(c0.f(c0.e(0, (ParameterizedType) type2)))) {
                                                        tVar5 = new r(t.m.a);
                                                    } else {
                                                        throw c0.l(aVar.d, i8, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", new Object[0]);
                                                    }
                                                } else if (f8.isArray()) {
                                                    if (MultipartBody.Part.class.isAssignableFrom(f8.getComponentType())) {
                                                        tVar5 = new s(t.m.a);
                                                    } else {
                                                        throw c0.l(aVar.d, i8, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", new Object[0]);
                                                    }
                                                } else if (MultipartBody.Part.class.isAssignableFrom(f8)) {
                                                    tVar3 = t.m.a;
                                                } else {
                                                    throw c0.l(aVar.d, i8, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", new Object[0]);
                                                }
                                                i4 = i6;
                                                tVar2 = tVar5;
                                            } else {
                                                i4 = i6;
                                                Headers c = Headers.j.c("Content-Disposition", a.w("form-data; name=\"", value6, "\""), "Content-Transfer-Encoding", qVar.encoding());
                                                if (Iterable.class.isAssignableFrom(f8)) {
                                                    if (type2 instanceof ParameterizedType) {
                                                        Type e6 = c0.e(0, (ParameterizedType) type2);
                                                        if (!MultipartBody.Part.class.isAssignableFrom(c0.f(e6))) {
                                                            tVar2 = new r(new t.g(aVar.d, i8, c, aVar.c.c(e6, annotationArr2, aVar.e)));
                                                        } else {
                                                            throw c0.l(aVar.d, i8, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", new Object[0]);
                                                        }
                                                    } else {
                                                        throw c0.l(aVar.d, i8, a.o(f8, new StringBuilder(), " must include generic type (e.g., ", "<String>)"), new Object[0]);
                                                    }
                                                } else if (f8.isArray()) {
                                                    Class<?> a = w.a.a(f8.getComponentType());
                                                    if (!MultipartBody.Part.class.isAssignableFrom(a)) {
                                                        tVar2 = new s(new t.g(aVar.d, i8, c, aVar.c.c(a, annotationArr2, aVar.e)));
                                                    } else {
                                                        throw c0.l(aVar.d, i8, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", new Object[0]);
                                                    }
                                                } else if (!MultipartBody.Part.class.isAssignableFrom(f8)) {
                                                    tVar3 = new t.g<>(aVar.d, i8, c, aVar.c.c(type2, annotationArr2, aVar.e));
                                                    tVar2 = tVar3;
                                                } else {
                                                    throw c0.l(aVar.d, i8, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", new Object[0]);
                                                }
                                            }
                                        } else {
                                            throw c0.l(aVar.d, i8, "@Part parameters can only be used with multipart encoding.", new Object[0]);
                                        }
                                    } else {
                                        i4 = i6;
                                        if (annotation2 instanceof r) {
                                            aVar.c(i8, type2);
                                            if (aVar.f3735s) {
                                                aVar.i = true;
                                                Class<?> f9 = c0.f(type2);
                                                if (Map.class.isAssignableFrom(f9)) {
                                                    Type g4 = c0.g(type2, f9, Map.class);
                                                    if (g4 instanceof ParameterizedType) {
                                                        ParameterizedType parameterizedType4 = (ParameterizedType) g4;
                                                        Type e7 = c0.e(0, parameterizedType4);
                                                        if (String.class == e7) {
                                                            Type e8 = c0.e(1, parameterizedType4);
                                                            if (!MultipartBody.Part.class.isAssignableFrom(c0.f(e8))) {
                                                                tVar2 = new t.h<>(aVar.d, i8, aVar.c.c(e8, annotationArr2, aVar.e), ((r) annotation2).encoding());
                                                            } else {
                                                                throw c0.l(aVar.d, i8, "@PartMap values cannot be MultipartBody.Part. Use @Part List<Part> or a different value type instead.", new Object[0]);
                                                            }
                                                        } else {
                                                            throw c0.l(aVar.d, i8, a.y("@PartMap keys must be of type String: ", e7), new Object[0]);
                                                        }
                                                    } else {
                                                        throw c0.l(aVar.d, i8, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                                                    }
                                                } else {
                                                    throw c0.l(aVar.d, i8, "@PartMap parameter type must be Map.", new Object[0]);
                                                }
                                            } else {
                                                throw c0.l(aVar.d, i8, "@PartMap parameters can only be used with multipart encoding.", new Object[0]);
                                            }
                                        } else if (annotation2 instanceof i0.f0.a) {
                                            aVar.c(i8, type2);
                                            if (aVar.r || aVar.f3735s) {
                                                throw c0.l(aVar.d, i8, "@Body parameters cannot be used with form or multi-part encoding.", new Object[0]);
                                            } else if (!aVar.j) {
                                                try {
                                                    h<T, RequestBody> c2 = aVar.c.c(type2, annotationArr2, aVar.e);
                                                    aVar.j = true;
                                                    tVar2 = new t.a<>(aVar.d, i8, c2);
                                                } catch (RuntimeException e9) {
                                                    throw c0.m(aVar.d, e9, i8, "Unable to create @Body converter for %s", type2);
                                                }
                                            } else {
                                                throw c0.l(aVar.d, i8, "Multiple @Body method annotations found.", new Object[0]);
                                            }
                                        } else if (annotation2 instanceof x) {
                                            aVar.c(i8, type2);
                                            Class<?> f10 = c0.f(type2);
                                            for (int i10 = i8 - 1; i10 >= 0; i10--) {
                                                t<?> tVar7 = aVar.f3736x[i10];
                                                if ((tVar7 instanceof t.o) && ((t.o) tVar7).a.equals(f10)) {
                                                    Method method2 = aVar.d;
                                                    StringBuilder R = a.R("@Tag type ");
                                                    R.append(f10.getName());
                                                    R.append(" is duplicate of parameter #");
                                                    R.append(i10 + 1);
                                                    R.append(" and would always overwrite its value.");
                                                    throw c0.l(method2, i8, R.toString(), new Object[0]);
                                                }
                                            }
                                            tVar2 = new t.o<>(f10);
                                        } else {
                                            tVar2 = null;
                                        }
                                    }
                                    i4 = i6;
                                    tVar2 = cVar;
                                }
                            }
                            i4 = i6;
                            tVar2 = tVar3;
                        }
                        if (tVar2 != null) {
                            if (tVar == null) {
                                tVar = tVar2;
                            } else {
                                throw c0.l(aVar.d, i8, "Multiple Retrofit annotations found, only one allowed.", new Object[0]);
                            }
                        }
                        i9 = i3 + 1;
                        i7 = i5;
                        length2 = i4;
                    }
                    i2 = length;
                    i = i7;
                } else {
                    i2 = length;
                    i = i7;
                    tVar = null;
                }
                if (tVar == null) {
                    if (z3) {
                        try {
                            if (c0.f(type2) == Continuation.class) {
                                aVar.f3737y = true;
                                tVar = null;
                            }
                        } catch (NoClassDefFoundError unused) {
                        }
                    }
                    throw c0.l(aVar.d, i8, "No Retrofit annotation found.", new Object[0]);
                }
                tVarArr[i8] = tVar;
                i8++;
                length = i2;
                i7 = i;
            }
            if (aVar.t != null || aVar.o) {
                boolean z4 = aVar.r;
                if (!z4 && !aVar.f3735s && !aVar.q && aVar.j) {
                    throw c0.j(aVar.d, "Non-body HTTP method cannot contain @Body.", new Object[0]);
                } else if (z4 && !aVar.h) {
                    throw c0.j(aVar.d, "Form-encoded method must contain at least one @Field.", new Object[0]);
                } else if (!aVar.f3735s || aVar.i) {
                    w wVar = new w(aVar);
                    Type genericReturnType = method.getGenericReturnType();
                    if (c0.h(genericReturnType)) {
                        throw c0.j(method, "Method return type must not include a type variable or wildcard: %s", genericReturnType);
                    } else if (genericReturnType != Void.TYPE) {
                        boolean z5 = wVar.k;
                        Annotation[] annotations = method.getAnnotations();
                        if (z5) {
                            Type type3 = ((ParameterizedType) method.getGenericParameterTypes()[genericParameterTypes.length - 1]).getActualTypeArguments()[0];
                            if (type3 instanceof WildcardType) {
                                type3 = ((WildcardType) type3).getLowerBounds()[0];
                            }
                            if (c0.f(type3) != Response.class || !(type3 instanceof ParameterizedType)) {
                                z2 = false;
                            } else {
                                type3 = c0.e(0, (ParameterizedType) type3);
                                z2 = true;
                            }
                            type = new c0.b(null, d.class, type3);
                            if (!c0.i(annotations, a0.class)) {
                                Annotation[] annotationArr3 = new Annotation[annotations.length + 1];
                                annotationArr3[0] = b0.a;
                                System.arraycopy(annotations, 0, annotationArr3, 1, annotations.length);
                                annotations = annotationArr3;
                            }
                        } else {
                            type = method.getGenericReturnType();
                            z2 = false;
                        }
                        try {
                            e<?, ?> a2 = yVar.a(type, annotations);
                            Type a3 = a2.a();
                            if (a3 == okhttp3.Response.class) {
                                StringBuilder R2 = a.R("'");
                                R2.append(c0.f(a3).getName());
                                R2.append("' is not a valid response body type. Did you mean ResponseBody?");
                                throw c0.j(method, R2.toString(), new Object[0]);
                            } else if (a3 == Response.class) {
                                throw c0.j(method, "Response must include generic type (e.g., Response<String>)", new Object[0]);
                            } else if (!wVar.c.equals("HEAD") || Void.class.equals(a3)) {
                                try {
                                    h<ResponseBody, T> d = yVar.d(a3, method.getAnnotations());
                                    e.a aVar3 = yVar.f3739b;
                                    if (!z5) {
                                        return new j.a(wVar, aVar3, d, a2);
                                    }
                                    if (z2) {
                                        return new j.c(wVar, aVar3, d, a2);
                                    }
                                    return new j.b(wVar, aVar3, d, a2, false);
                                } catch (RuntimeException e10) {
                                    throw c0.k(method, e10, "Unable to create converter for %s", a3);
                                }
                            } else {
                                throw c0.j(method, "HEAD method must use Void as response type.", new Object[0]);
                            }
                        } catch (RuntimeException e11) {
                            throw c0.k(method, e11, "Unable to create call adapter for %s", type);
                        }
                    } else {
                        throw c0.j(method, "Service methods cannot return void.", new Object[0]);
                    }
                } else {
                    throw c0.j(aVar.d, "Multipart method must contain at least one @Part.", new Object[0]);
                }
            } else {
                throw c0.j(aVar.d, "Missing either @%s URL or @Url parameter.", aVar.p);
            }
        } else {
            throw c0.j(aVar.d, "HTTP method annotation is required (e.g., @GET, @POST, etc.).", new Object[0]);
        }
    }

    public abstract T a(Object[] objArr);
}
