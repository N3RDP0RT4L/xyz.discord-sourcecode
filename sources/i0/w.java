package i0;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import okhttp3.Headers;
import okhttp3.MediaType;
/* compiled from: RequestFactory.java */
/* loaded from: classes3.dex */
public final class w {
    public final Method a;

    /* renamed from: b  reason: collision with root package name */
    public final f0.w f3733b;
    public final String c;
    public final String d;
    public final Headers e;
    public final MediaType f;
    public final boolean g;
    public final boolean h;
    public final boolean i;
    public final t<?>[] j;
    public final boolean k;

    /* compiled from: RequestFactory.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public static final Pattern a = Pattern.compile("\\{([a-zA-Z][a-zA-Z0-9_-]*)\\}");

        /* renamed from: b  reason: collision with root package name */
        public static final Pattern f3734b = Pattern.compile("[a-zA-Z][a-zA-Z0-9_-]*");
        public final y c;
        public final Method d;
        public final Annotation[] e;
        public final Annotation[][] f;
        public final Type[] g;
        public boolean h;
        public boolean i;
        public boolean j;
        public boolean k;
        public boolean l;
        public boolean m;
        public boolean n;
        public boolean o;
        public String p;
        public boolean q;
        public boolean r;

        /* renamed from: s  reason: collision with root package name */
        public boolean f3735s;
        public String t;
        public Headers u;
        public MediaType v;
        public Set<String> w;

        /* renamed from: x  reason: collision with root package name */
        public t<?>[] f3736x;

        /* renamed from: y  reason: collision with root package name */
        public boolean f3737y;

        public a(y yVar, Method method) {
            this.c = yVar;
            this.d = method;
            this.e = method.getAnnotations();
            this.g = method.getGenericParameterTypes();
            this.f = method.getParameterAnnotations();
        }

        public static Class<?> a(Class<?> cls) {
            return Boolean.TYPE == cls ? Boolean.class : Byte.TYPE == cls ? Byte.class : Character.TYPE == cls ? Character.class : Double.TYPE == cls ? Double.class : Float.TYPE == cls ? Float.class : Integer.TYPE == cls ? Integer.class : Long.TYPE == cls ? Long.class : Short.TYPE == cls ? Short.class : cls;
        }

        public final void b(String str, String str2, boolean z2) {
            String str3 = this.p;
            if (str3 == null) {
                this.p = str;
                this.q = z2;
                if (!str2.isEmpty()) {
                    int indexOf = str2.indexOf(63);
                    if (indexOf != -1 && indexOf < str2.length() - 1) {
                        String substring = str2.substring(indexOf + 1);
                        if (a.matcher(substring).find()) {
                            throw c0.j(this.d, "URL query string \"%s\" must not have replace block. For dynamic query parameters use @Query.", substring);
                        }
                    }
                    this.t = str2;
                    Matcher matcher = a.matcher(str2);
                    LinkedHashSet linkedHashSet = new LinkedHashSet();
                    while (matcher.find()) {
                        linkedHashSet.add(matcher.group(1));
                    }
                    this.w = linkedHashSet;
                    return;
                }
                return;
            }
            throw c0.j(this.d, "Only one HTTP method is allowed. Found: %s and %s.", str3, str);
        }

        public final void c(int i, Type type) {
            if (c0.h(type)) {
                throw c0.l(this.d, i, "Parameter type must not include a type variable or wildcard: %s", type);
            }
        }
    }

    public w(a aVar) {
        this.a = aVar.d;
        this.f3733b = aVar.c.c;
        this.c = aVar.p;
        this.d = aVar.t;
        this.e = aVar.u;
        this.f = aVar.v;
        this.g = aVar.q;
        this.h = aVar.r;
        this.i = aVar.f3735s;
        this.j = aVar.f3736x;
        this.k = aVar.f3737y;
    }
}
