package i0;

import d0.z.d.m;
import f0.b0;
import g0.e;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.Objects;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
/* compiled from: Utils.java */
/* loaded from: classes3.dex */
public final class c0 {
    public static final Type[] a = new Type[0];

    /* compiled from: Utils.java */
    /* loaded from: classes3.dex */
    public static final class a implements GenericArrayType {
        public final Type j;

        public a(Type type) {
            this.j = type;
        }

        public boolean equals(Object obj) {
            return (obj instanceof GenericArrayType) && c0.c(this, (GenericArrayType) obj);
        }

        @Override // java.lang.reflect.GenericArrayType
        public Type getGenericComponentType() {
            return this.j;
        }

        public int hashCode() {
            return this.j.hashCode();
        }

        public String toString() {
            return c0.p(this.j) + "[]";
        }
    }

    /* compiled from: Utils.java */
    /* loaded from: classes3.dex */
    public static final class b implements ParameterizedType {
        public final Type j;
        public final Type k;
        public final Type[] l;

        public b(Type type, Type type2, Type... typeArr) {
            if (type2 instanceof Class) {
                if ((type == null) != (((Class) type2).getEnclosingClass() != null ? false : true)) {
                    throw new IllegalArgumentException();
                }
            }
            for (Type type3 : typeArr) {
                Objects.requireNonNull(type3, "typeArgument == null");
                c0.b(type3);
            }
            this.j = type;
            this.k = type2;
            this.l = (Type[]) typeArr.clone();
        }

        public boolean equals(Object obj) {
            return (obj instanceof ParameterizedType) && c0.c(this, (ParameterizedType) obj);
        }

        @Override // java.lang.reflect.ParameterizedType
        public Type[] getActualTypeArguments() {
            return (Type[]) this.l.clone();
        }

        @Override // java.lang.reflect.ParameterizedType
        public Type getOwnerType() {
            return this.j;
        }

        @Override // java.lang.reflect.ParameterizedType
        public Type getRawType() {
            return this.k;
        }

        public int hashCode() {
            int hashCode = Arrays.hashCode(this.l) ^ this.k.hashCode();
            Type type = this.j;
            return hashCode ^ (type != null ? type.hashCode() : 0);
        }

        public String toString() {
            Type[] typeArr = this.l;
            if (typeArr.length == 0) {
                return c0.p(this.k);
            }
            StringBuilder sb = new StringBuilder((typeArr.length + 1) * 30);
            sb.append(c0.p(this.k));
            sb.append("<");
            sb.append(c0.p(this.l[0]));
            for (int i = 1; i < this.l.length; i++) {
                sb.append(", ");
                sb.append(c0.p(this.l[i]));
            }
            sb.append(">");
            return sb.toString();
        }
    }

    /* compiled from: Utils.java */
    /* loaded from: classes3.dex */
    public static final class c implements WildcardType {
        public final Type j;
        public final Type k;

        public c(Type[] typeArr, Type[] typeArr2) {
            if (typeArr2.length > 1) {
                throw new IllegalArgumentException();
            } else if (typeArr.length != 1) {
                throw new IllegalArgumentException();
            } else if (typeArr2.length == 1) {
                Objects.requireNonNull(typeArr2[0]);
                c0.b(typeArr2[0]);
                if (typeArr[0] == Object.class) {
                    this.k = typeArr2[0];
                    this.j = Object.class;
                    return;
                }
                throw new IllegalArgumentException();
            } else {
                Objects.requireNonNull(typeArr[0]);
                c0.b(typeArr[0]);
                this.k = null;
                this.j = typeArr[0];
            }
        }

        public boolean equals(Object obj) {
            return (obj instanceof WildcardType) && c0.c(this, (WildcardType) obj);
        }

        @Override // java.lang.reflect.WildcardType
        public Type[] getLowerBounds() {
            Type type = this.k;
            return type != null ? new Type[]{type} : c0.a;
        }

        @Override // java.lang.reflect.WildcardType
        public Type[] getUpperBounds() {
            return new Type[]{this.j};
        }

        public int hashCode() {
            Type type = this.k;
            return (type != null ? type.hashCode() + 31 : 1) ^ (this.j.hashCode() + 31);
        }

        public String toString() {
            if (this.k != null) {
                StringBuilder R = b.d.b.a.a.R("? super ");
                R.append(c0.p(this.k));
                return R.toString();
            } else if (this.j == Object.class) {
                return "?";
            } else {
                StringBuilder R2 = b.d.b.a.a.R("? extends ");
                R2.append(c0.p(this.j));
                return R2.toString();
            }
        }
    }

    public static ResponseBody a(ResponseBody responseBody) throws IOException {
        e eVar = new e();
        responseBody.c().k0(eVar);
        MediaType b2 = responseBody.b();
        long a2 = responseBody.a();
        m.checkParameterIsNotNull(eVar, "content");
        m.checkParameterIsNotNull(eVar, "$this$asResponseBody");
        return new b0(eVar, b2, a2);
    }

    public static void b(Type type) {
        if ((type instanceof Class) && ((Class) type).isPrimitive()) {
            throw new IllegalArgumentException();
        }
    }

    public static boolean c(Type type, Type type2) {
        if (type == type2) {
            return true;
        }
        if (type instanceof Class) {
            return type.equals(type2);
        }
        if (type instanceof ParameterizedType) {
            if (!(type2 instanceof ParameterizedType)) {
                return false;
            }
            ParameterizedType parameterizedType = (ParameterizedType) type;
            ParameterizedType parameterizedType2 = (ParameterizedType) type2;
            Type ownerType = parameterizedType.getOwnerType();
            Type ownerType2 = parameterizedType2.getOwnerType();
            return (ownerType == ownerType2 || (ownerType != null && ownerType.equals(ownerType2))) && parameterizedType.getRawType().equals(parameterizedType2.getRawType()) && Arrays.equals(parameterizedType.getActualTypeArguments(), parameterizedType2.getActualTypeArguments());
        } else if (type instanceof GenericArrayType) {
            if (!(type2 instanceof GenericArrayType)) {
                return false;
            }
            return c(((GenericArrayType) type).getGenericComponentType(), ((GenericArrayType) type2).getGenericComponentType());
        } else if (type instanceof WildcardType) {
            if (!(type2 instanceof WildcardType)) {
                return false;
            }
            WildcardType wildcardType = (WildcardType) type;
            WildcardType wildcardType2 = (WildcardType) type2;
            return Arrays.equals(wildcardType.getUpperBounds(), wildcardType2.getUpperBounds()) && Arrays.equals(wildcardType.getLowerBounds(), wildcardType2.getLowerBounds());
        } else if (!(type instanceof TypeVariable) || !(type2 instanceof TypeVariable)) {
            return false;
        } else {
            TypeVariable typeVariable = (TypeVariable) type;
            TypeVariable typeVariable2 = (TypeVariable) type2;
            return typeVariable.getGenericDeclaration() == typeVariable2.getGenericDeclaration() && typeVariable.getName().equals(typeVariable2.getName());
        }
    }

    public static Type d(Type type, Class<?> cls, Class<?> cls2) {
        if (cls2 == cls) {
            return type;
        }
        if (cls2.isInterface()) {
            Class<?>[] interfaces = cls.getInterfaces();
            int length = interfaces.length;
            for (int i = 0; i < length; i++) {
                if (interfaces[i] == cls2) {
                    return cls.getGenericInterfaces()[i];
                }
                if (cls2.isAssignableFrom(interfaces[i])) {
                    return d(cls.getGenericInterfaces()[i], interfaces[i], cls2);
                }
            }
        }
        if (!cls.isInterface()) {
            while (cls != Object.class) {
                Class<? super Object> superclass = cls.getSuperclass();
                if (superclass == cls2) {
                    return cls.getGenericSuperclass();
                }
                if (cls2.isAssignableFrom(superclass)) {
                    return d(cls.getGenericSuperclass(), superclass, cls2);
                }
                cls = superclass;
            }
        }
        return cls2;
    }

    public static Type e(int i, ParameterizedType parameterizedType) {
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        if (i < 0 || i >= actualTypeArguments.length) {
            StringBuilder S = b.d.b.a.a.S("Index ", i, " not in range [0,");
            S.append(actualTypeArguments.length);
            S.append(") for ");
            S.append(parameterizedType);
            throw new IllegalArgumentException(S.toString());
        }
        Type type = actualTypeArguments[i];
        return type instanceof WildcardType ? ((WildcardType) type).getUpperBounds()[0] : type;
    }

    public static Class<?> f(Type type) {
        Objects.requireNonNull(type, "type == null");
        if (type instanceof Class) {
            return (Class) type;
        }
        if (type instanceof ParameterizedType) {
            Type rawType = ((ParameterizedType) type).getRawType();
            if (rawType instanceof Class) {
                return (Class) rawType;
            }
            throw new IllegalArgumentException();
        } else if (type instanceof GenericArrayType) {
            return Array.newInstance(f(((GenericArrayType) type).getGenericComponentType()), 0).getClass();
        } else {
            if (type instanceof TypeVariable) {
                return Object.class;
            }
            if (type instanceof WildcardType) {
                return f(((WildcardType) type).getUpperBounds()[0]);
            }
            throw new IllegalArgumentException("Expected a Class, ParameterizedType, or GenericArrayType, but <" + type + "> is of type " + type.getClass().getName());
        }
    }

    public static Type g(Type type, Class<?> cls, Class<?> cls2) {
        if (cls2.isAssignableFrom(cls)) {
            return n(type, cls, d(type, cls, cls2));
        }
        throw new IllegalArgumentException();
    }

    public static boolean h(Type type) {
        if (type instanceof Class) {
            return false;
        }
        if (type instanceof ParameterizedType) {
            for (Type type2 : ((ParameterizedType) type).getActualTypeArguments()) {
                if (h(type2)) {
                    return true;
                }
            }
            return false;
        } else if (type instanceof GenericArrayType) {
            return h(((GenericArrayType) type).getGenericComponentType());
        } else {
            if ((type instanceof TypeVariable) || (type instanceof WildcardType)) {
                return true;
            }
            throw new IllegalArgumentException("Expected a Class, ParameterizedType, or GenericArrayType, but <" + type + "> is of type " + (type == null ? "null" : type.getClass().getName()));
        }
    }

    public static boolean i(Annotation[] annotationArr, Class<? extends Annotation> cls) {
        for (Annotation annotation : annotationArr) {
            if (cls.isInstance(annotation)) {
                return true;
            }
        }
        return false;
    }

    public static RuntimeException j(Method method, String str, Object... objArr) {
        return k(method, null, str, objArr);
    }

    public static RuntimeException k(Method method, Throwable th, String str, Object... objArr) {
        StringBuilder V = b.d.b.a.a.V(String.format(str, objArr), "\n    for method ");
        V.append(method.getDeclaringClass().getSimpleName());
        V.append(".");
        V.append(method.getName());
        return new IllegalArgumentException(V.toString(), th);
    }

    public static RuntimeException l(Method method, int i, String str, Object... objArr) {
        StringBuilder V = b.d.b.a.a.V(str, " (parameter #");
        V.append(i + 1);
        V.append(")");
        return j(method, V.toString(), objArr);
    }

    public static RuntimeException m(Method method, Throwable th, int i, String str, Object... objArr) {
        StringBuilder V = b.d.b.a.a.V(str, " (parameter #");
        V.append(i + 1);
        V.append(")");
        return k(method, th, V.toString(), objArr);
    }

    /* JADX WARN: Code restructure failed: missing block: B:0:?, code lost:
        r10 = r10;
     */
    /* JADX WARN: Removed duplicated region for block: B:23:0x0043 A[LOOP:0: B:2:0x0000->B:23:0x0043, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:76:0x0042 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static java.lang.reflect.Type n(java.lang.reflect.Type r8, java.lang.Class<?> r9, java.lang.reflect.Type r10) {
        /*
            Method dump skipped, instructions count: 258
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: i0.c0.n(java.lang.reflect.Type, java.lang.Class, java.lang.reflect.Type):java.lang.reflect.Type");
    }

    public static void o(Throwable th) {
        if (th instanceof VirtualMachineError) {
            throw ((VirtualMachineError) th);
        } else if (th instanceof ThreadDeath) {
            throw ((ThreadDeath) th);
        } else if (th instanceof LinkageError) {
            throw ((LinkageError) th);
        }
    }

    public static String p(Type type) {
        return type instanceof Class ? ((Class) type).getName() : type.toString();
    }
}
