package i0;

import java.io.IOException;
import java.lang.reflect.Array;
/* compiled from: ParameterHandler.java */
/* loaded from: classes3.dex */
public class s extends t<Object> {
    public final /* synthetic */ t a;

    public s(t tVar) {
        this.a = tVar;
    }

    @Override // i0.t
    public void a(v vVar, Object obj) throws IOException {
        if (obj != null) {
            int length = Array.getLength(obj);
            for (int i = 0; i < length; i++) {
                this.a.a(vVar, Array.get(obj, i));
            }
        }
    }
}
