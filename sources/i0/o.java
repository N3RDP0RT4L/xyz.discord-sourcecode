package i0;

import androidx.core.app.NotificationCompat;
import d0.k;
import d0.l;
import d0.z.d.m;
import kotlinx.coroutines.CancellableContinuation;
import retrofit2.Response;
/* compiled from: KotlinExtensions.kt */
/* loaded from: classes3.dex */
public final class o implements f<T> {
    public final /* synthetic */ CancellableContinuation a;

    public o(CancellableContinuation cancellableContinuation) {
        this.a = cancellableContinuation;
    }

    @Override // i0.f
    public void a(d<T> dVar, Throwable th) {
        m.checkParameterIsNotNull(dVar, NotificationCompat.CATEGORY_CALL);
        m.checkParameterIsNotNull(th, "t");
        CancellableContinuation cancellableContinuation = this.a;
        k.a aVar = k.j;
        cancellableContinuation.resumeWith(k.m73constructorimpl(l.createFailure(th)));
    }

    @Override // i0.f
    public void b(d<T> dVar, Response<T> response) {
        m.checkParameterIsNotNull(dVar, NotificationCompat.CATEGORY_CALL);
        m.checkParameterIsNotNull(response, "response");
        CancellableContinuation cancellableContinuation = this.a;
        k.a aVar = k.j;
        cancellableContinuation.resumeWith(k.m73constructorimpl(response));
    }
}
