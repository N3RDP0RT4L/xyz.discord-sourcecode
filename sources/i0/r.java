package i0;

import java.io.IOException;
/* compiled from: ParameterHandler.java */
/* loaded from: classes3.dex */
public class r extends t<Iterable<T>> {
    public final /* synthetic */ t a;

    public r(t tVar) {
        this.a = tVar;
    }

    @Override // i0.t
    public void a(v vVar, Object obj) throws IOException {
        Iterable<Object> iterable = (Iterable) obj;
        if (iterable != null) {
            for (Object obj2 : iterable) {
                this.a.a(vVar, obj2);
            }
        }
    }
}
