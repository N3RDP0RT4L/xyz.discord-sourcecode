package i0;

import andhook.lib.xposed.ClassUtils;
import androidx.core.app.NotificationCompat;
import d0.k;
import d0.l;
import java.lang.reflect.Method;
import java.util.Objects;
import kotlin.KotlinNullPointerException;
import kotlinx.coroutines.CancellableContinuation;
import okhttp3.Request;
import retrofit2.HttpException;
import retrofit2.Response;
/* compiled from: KotlinExtensions.kt */
/* loaded from: classes3.dex */
public final class m implements f<T> {
    public final /* synthetic */ CancellableContinuation a;

    public m(CancellableContinuation cancellableContinuation) {
        this.a = cancellableContinuation;
    }

    @Override // i0.f
    public void a(d<T> dVar, Throwable th) {
        d0.z.d.m.checkParameterIsNotNull(dVar, NotificationCompat.CATEGORY_CALL);
        d0.z.d.m.checkParameterIsNotNull(th, "t");
        CancellableContinuation cancellableContinuation = this.a;
        k.a aVar = k.j;
        cancellableContinuation.resumeWith(k.m73constructorimpl(l.createFailure(th)));
    }

    @Override // i0.f
    public void b(d<T> dVar, Response<T> response) {
        d0.z.d.m.checkParameterIsNotNull(dVar, NotificationCompat.CATEGORY_CALL);
        d0.z.d.m.checkParameterIsNotNull(response, "response");
        if (response.a()) {
            T t = response.f3796b;
            if (t == 0) {
                Request c = dVar.c();
                Objects.requireNonNull(c);
                d0.z.d.m.checkParameterIsNotNull(k.class, "type");
                Object cast = k.class.cast(c.f.get(k.class));
                if (cast == null) {
                    d0.z.d.m.throwNpe();
                }
                d0.z.d.m.checkExpressionValueIsNotNull(cast, "call.request().tag(Invocation::class.java)!!");
                Method method = ((k) cast).a;
                StringBuilder sb = new StringBuilder();
                sb.append("Response from ");
                d0.z.d.m.checkExpressionValueIsNotNull(method, "method");
                Class<?> declaringClass = method.getDeclaringClass();
                d0.z.d.m.checkExpressionValueIsNotNull(declaringClass, "method.declaringClass");
                sb.append(declaringClass.getName());
                sb.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
                sb.append(method.getName());
                sb.append(" was null but response body type was declared as non-null");
                KotlinNullPointerException kotlinNullPointerException = new KotlinNullPointerException(sb.toString());
                CancellableContinuation cancellableContinuation = this.a;
                k.a aVar = k.j;
                cancellableContinuation.resumeWith(k.m73constructorimpl(l.createFailure(kotlinNullPointerException)));
                return;
            }
            CancellableContinuation cancellableContinuation2 = this.a;
            k.a aVar2 = k.j;
            cancellableContinuation2.resumeWith(k.m73constructorimpl(t));
            return;
        }
        CancellableContinuation cancellableContinuation3 = this.a;
        HttpException httpException = new HttpException(response);
        k.a aVar3 = k.j;
        cancellableContinuation3.resumeWith(k.m73constructorimpl(l.createFailure(httpException)));
    }
}
