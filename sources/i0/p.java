package i0;

import d0.z.d.m;
import f0.e;
import f0.f;
import f0.u;
import f0.w;
import f0.y;
import g0.g;
import g0.j;
import g0.r;
import g0.x;
import i0.v;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
/* compiled from: OkHttpCall.java */
/* loaded from: classes3.dex */
public final class p<T> implements d<T> {
    public final w j;
    public final Object[] k;
    public final e.a l;
    public final h<ResponseBody, T> m;
    public volatile boolean n;
    public e o;
    public Throwable p;
    public boolean q;

    /* compiled from: OkHttpCall.java */
    /* loaded from: classes3.dex */
    public class a implements f {
        public final /* synthetic */ f a;

        public a(f fVar) {
            this.a = fVar;
        }

        @Override // f0.f
        public void a(e eVar, Response response) {
            try {
                try {
                    this.a.b(p.this, p.this.g(response));
                } catch (Throwable th) {
                    c0.o(th);
                    th.printStackTrace();
                }
            } catch (Throwable th2) {
                c0.o(th2);
                try {
                    this.a.a(p.this, th2);
                } catch (Throwable th3) {
                    c0.o(th3);
                    th3.printStackTrace();
                }
            }
        }

        @Override // f0.f
        public void b(e eVar, IOException iOException) {
            try {
                this.a.a(p.this, iOException);
            } catch (Throwable th) {
                c0.o(th);
                th.printStackTrace();
            }
        }
    }

    /* compiled from: OkHttpCall.java */
    /* loaded from: classes3.dex */
    public static final class b extends ResponseBody {
        public final ResponseBody l;
        public final g m;
        public IOException n;

        /* compiled from: OkHttpCall.java */
        /* loaded from: classes3.dex */
        public class a extends j {
            public a(x xVar) {
                super(xVar);
            }

            @Override // g0.x
            public long i0(g0.e eVar, long j) throws IOException {
                try {
                    m.checkParameterIsNotNull(eVar, "sink");
                    return this.j.i0(eVar, j);
                } catch (IOException e) {
                    b.this.n = e;
                    throw e;
                }
            }
        }

        public b(ResponseBody responseBody) {
            this.l = responseBody;
            a aVar = new a(responseBody.c());
            m.checkParameterIsNotNull(aVar, "$this$buffer");
            this.m = new r(aVar);
        }

        @Override // okhttp3.ResponseBody
        public long a() {
            return this.l.a();
        }

        @Override // okhttp3.ResponseBody
        public MediaType b() {
            return this.l.b();
        }

        @Override // okhttp3.ResponseBody
        public g c() {
            return this.m;
        }

        @Override // okhttp3.ResponseBody, java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            this.l.close();
        }
    }

    /* compiled from: OkHttpCall.java */
    /* loaded from: classes3.dex */
    public static final class c extends ResponseBody {
        public final MediaType l;
        public final long m;

        public c(MediaType mediaType, long j) {
            this.l = mediaType;
            this.m = j;
        }

        @Override // okhttp3.ResponseBody
        public long a() {
            return this.m;
        }

        @Override // okhttp3.ResponseBody
        public MediaType b() {
            return this.l;
        }

        @Override // okhttp3.ResponseBody
        public g c() {
            throw new IllegalStateException("Cannot read raw response body of a converted body.");
        }
    }

    public p(w wVar, Object[] objArr, e.a aVar, h<ResponseBody, T> hVar) {
        this.j = wVar;
        this.k = objArr;
        this.l = aVar;
        this.m = hVar;
    }

    @Override // i0.d
    public void C(f<T> fVar) {
        e eVar;
        Throwable th;
        synchronized (this) {
            if (!this.q) {
                this.q = true;
                eVar = this.o;
                th = this.p;
                if (eVar == null && th == null) {
                    e b2 = b();
                    this.o = b2;
                    eVar = b2;
                }
            } else {
                throw new IllegalStateException("Already executed.");
            }
        }
        if (th != null) {
            fVar.a(this, th);
            return;
        }
        if (this.n) {
            eVar.cancel();
        }
        eVar.e(new a(fVar));
    }

    @Override // i0.d
    public d L() {
        return new p(this.j, this.k, this.l, this.m);
    }

    public final e b() throws IOException {
        w wVar;
        e.a aVar = this.l;
        w wVar2 = this.j;
        Object[] objArr = this.k;
        t<?>[] tVarArr = wVar2.j;
        int length = objArr.length;
        if (length == tVarArr.length) {
            v vVar = new v(wVar2.c, wVar2.f3733b, wVar2.d, wVar2.e, wVar2.f, wVar2.g, wVar2.h, wVar2.i);
            if (wVar2.k) {
                length--;
            }
            ArrayList arrayList = new ArrayList(length);
            for (int i = 0; i < length; i++) {
                arrayList.add(objArr[i]);
                tVarArr[i].a(vVar, objArr[i]);
            }
            w.a aVar2 = vVar.f;
            if (aVar2 != null) {
                wVar = aVar2.b();
            } else {
                w wVar3 = vVar.d;
                String str = vVar.e;
                Objects.requireNonNull(wVar3);
                m.checkParameterIsNotNull(str, "link");
                w.a g = wVar3.g(str);
                wVar = g != null ? g.b() : null;
                if (wVar == null) {
                    StringBuilder R = b.d.b.a.a.R("Malformed URL. Base: ");
                    R.append(vVar.d);
                    R.append(", Relative: ");
                    R.append(vVar.e);
                    throw new IllegalArgumentException(R.toString());
                }
            }
            v.a aVar3 = vVar.m;
            if (aVar3 == null) {
                u.a aVar4 = vVar.l;
                if (aVar4 != null) {
                    aVar3 = new u(aVar4.a, aVar4.f3651b);
                } else {
                    MultipartBody.a aVar5 = vVar.k;
                    if (aVar5 != null) {
                        aVar3 = aVar5.b();
                    } else if (vVar.j) {
                        aVar3 = RequestBody.create((MediaType) null, new byte[0]);
                    }
                }
            }
            MediaType mediaType = vVar.i;
            if (mediaType != null) {
                if (aVar3 != null) {
                    aVar3 = new v.a(aVar3, mediaType);
                } else {
                    vVar.h.a("Content-Type", mediaType.d);
                }
            }
            Request.a aVar6 = vVar.g;
            aVar6.g(wVar);
            Headers c2 = vVar.h.c();
            m.checkParameterIsNotNull(c2, "headers");
            aVar6.c = c2.e();
            aVar6.c(vVar.c, aVar3);
            aVar6.e(k.class, new k(wVar2.a, arrayList));
            e b2 = aVar.b(aVar6.a());
            Objects.requireNonNull(b2, "Call.Factory returned null.");
            return b2;
        }
        throw new IllegalArgumentException(b.d.b.a.a.A(b.d.b.a.a.S("Argument count (", length, ") doesn't match expected count ("), tVarArr.length, ")"));
    }

    @Override // i0.d
    public synchronized Request c() {
        try {
        } catch (IOException e) {
            throw new RuntimeException("Unable to create request.", e);
        }
        return f().c();
    }

    @Override // i0.d
    public void cancel() {
        e eVar;
        this.n = true;
        synchronized (this) {
            eVar = this.o;
        }
        if (eVar != null) {
            eVar.cancel();
        }
    }

    public Object clone() throws CloneNotSupportedException {
        return new p(this.j, this.k, this.l, this.m);
    }

    @Override // i0.d
    public boolean d() {
        boolean z2 = true;
        if (this.n) {
            return true;
        }
        synchronized (this) {
            e eVar = this.o;
            if (eVar == null || !eVar.d()) {
                z2 = false;
            }
        }
        return z2;
    }

    @Override // i0.d
    public retrofit2.Response<T> execute() throws IOException {
        e f;
        synchronized (this) {
            if (!this.q) {
                this.q = true;
                f = f();
            } else {
                throw new IllegalStateException("Already executed.");
            }
        }
        if (this.n) {
            f.cancel();
        }
        return g(f.execute());
    }

    public final e f() throws IOException {
        e eVar = this.o;
        if (eVar != null) {
            return eVar;
        }
        Throwable th = this.p;
        if (th == null) {
            try {
                e b2 = b();
                this.o = b2;
                return b2;
            } catch (IOException | Error | RuntimeException e) {
                c0.o(e);
                this.p = e;
                throw e;
            }
        } else if (th instanceof IOException) {
            throw ((IOException) th);
        } else if (th instanceof RuntimeException) {
            throw ((RuntimeException) th);
        } else {
            throw ((Error) th);
        }
    }

    /* JADX WARN: Finally extract failed */
    public retrofit2.Response<T> g(Response response) throws IOException {
        ResponseBody responseBody = response.p;
        m.checkParameterIsNotNull(response, "response");
        Request request = response.j;
        y yVar = response.k;
        int i = response.m;
        String str = response.l;
        f0.v vVar = response.n;
        Headers.a e = response.o.e();
        Response response2 = response.q;
        Response response3 = response.r;
        Response response4 = response.f3787s;
        long j = response.t;
        long j2 = response.u;
        f0.e0.g.c cVar = response.v;
        c cVar2 = new c(responseBody.b(), responseBody.a());
        if (!(i >= 0)) {
            throw new IllegalStateException(b.d.b.a.a.p("code < 0: ", i).toString());
        } else if (request == null) {
            throw new IllegalStateException("request == null".toString());
        } else if (yVar == null) {
            throw new IllegalStateException("protocol == null".toString());
        } else if (str != null) {
            Response response5 = new Response(request, yVar, str, i, vVar, e.c(), cVar2, response2, response3, response4, j, j2, cVar);
            int i2 = response5.m;
            if (i2 < 200 || i2 >= 300) {
                try {
                    ResponseBody a2 = c0.a(responseBody);
                    if (!response5.b()) {
                        retrofit2.Response<T> response6 = new retrofit2.Response<>(response5, null, a2);
                        responseBody.close();
                        return response6;
                    }
                    throw new IllegalArgumentException("rawResponse should not be successful response");
                } catch (Throwable th) {
                    responseBody.close();
                    throw th;
                }
            } else if (i2 == 204 || i2 == 205) {
                responseBody.close();
                return retrofit2.Response.b(null, response5);
            } else {
                b bVar = new b(responseBody);
                try {
                    return retrofit2.Response.b(this.m.convert(bVar), response5);
                } catch (RuntimeException e2) {
                    IOException iOException = bVar.n;
                    if (iOException == null) {
                        throw e2;
                    }
                    throw iOException;
                }
            }
        } else {
            throw new IllegalStateException("message == null".toString());
        }
    }
}
