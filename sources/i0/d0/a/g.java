package i0.d0.a;

import i0.c0;
import i0.e;
import i0.y;
import j0.d;
import j0.h;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import retrofit2.Response;
import rx.Observable;
import rx.Scheduler;
/* compiled from: RxJavaCallAdapterFactory.java */
/* loaded from: classes3.dex */
public final class g extends e.a {
    public g(Scheduler scheduler, boolean z2) {
    }

    @Override // i0.e.a
    public e<?, ?> a(Type type, Annotation[] annotationArr, y yVar) {
        boolean z2;
        boolean z3;
        Type type2;
        Type type3;
        Class<?> f = c0.f(type);
        boolean z4 = true;
        boolean z5 = f == h.class;
        boolean z6 = f == d.class;
        if (f != Observable.class && !z5 && !z6) {
            return null;
        }
        if (z6) {
            return new f(Void.class, null, false, false, true, false, true);
        }
        if (!(type instanceof ParameterizedType)) {
            String str = z5 ? "Single" : "Observable";
            throw new IllegalStateException(str + " return type must be parameterized as " + str + "<Foo> or " + str + "<? extends Foo>");
        }
        Type e = c0.e(0, (ParameterizedType) type);
        Class<?> f2 = c0.f(e);
        if (f2 == Response.class) {
            if (e instanceof ParameterizedType) {
                type3 = c0.e(0, (ParameterizedType) e);
                z4 = false;
            } else {
                throw new IllegalStateException("Response must be parameterized as Response<Foo> or Response<? extends Foo>");
            }
        } else if (f2 != d.class) {
            type2 = e;
            z3 = false;
            z2 = true;
            return new f(type2, null, false, z3, z2, z5, false);
        } else if (e instanceof ParameterizedType) {
            type3 = c0.e(0, (ParameterizedType) e);
        } else {
            throw new IllegalStateException("Result must be parameterized as Result<Foo> or Result<? extends Foo>");
        }
        type2 = type3;
        z3 = z4;
        z2 = false;
        return new f(type2, null, false, z3, z2, z5, false);
    }
}
