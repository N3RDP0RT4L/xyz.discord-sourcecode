package i0.d0.a;

import b.d.b.a.a;
import retrofit2.Response;
/* compiled from: Result.java */
/* loaded from: classes3.dex */
public final class d<T> {
    public final Response<T> a;

    /* renamed from: b  reason: collision with root package name */
    public final Throwable f3707b;

    public d(Response<T> response, Throwable th) {
        this.a = response;
        this.f3707b = th;
    }

    public String toString() {
        if (this.f3707b != null) {
            StringBuilder R = a.R("Result{isError=true, error=\"");
            R.append(this.f3707b);
            R.append("\"}");
            return R.toString();
        }
        StringBuilder R2 = a.R("Result{isError=false, response=");
        R2.append(this.a);
        R2.append('}');
        return R2.toString();
    }
}
