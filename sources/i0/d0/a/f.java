package i0.d0.a;

import i0.e;
import java.lang.reflect.Type;
import rx.Scheduler;
/* compiled from: RxJavaCallAdapter.java */
/* loaded from: classes3.dex */
public final class f<R> implements e<R, Object> {
    public final Type a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f3708b;
    public final boolean c;
    public final boolean d;
    public final boolean e;

    public f(Type type, Scheduler scheduler, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
        this.a = type;
        this.f3708b = z3;
        this.c = z4;
        this.d = z5;
        this.e = z6;
    }

    @Override // i0.e
    public Type a() {
        return this.a;
    }

    /* JADX WARN: Removed duplicated region for block: B:11:0x0027  */
    /* JADX WARN: Removed duplicated region for block: B:13:0x0032  */
    @Override // i0.e
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public java.lang.Object b(i0.d<R> r3) {
        /*
            r2 = this;
            i0.d0.a.c r0 = new i0.d0.a.c
            r0.<init>(r3)
            boolean r3 = r2.f3708b
            if (r3 == 0) goto L10
            i0.d0.a.e r3 = new i0.d0.a.e
            r3.<init>(r0)
        Le:
            r0 = r3
            goto L1a
        L10:
            boolean r3 = r2.c
            if (r3 == 0) goto L1a
            i0.d0.a.a r3 = new i0.d0.a.a
            r3.<init>(r0)
            goto Le
        L1a:
            rx.Observable r3 = new rx.Observable
            rx.Observable$a r0 = j0.o.l.a(r0)
            r3.<init>(r0)
            boolean r0 = r2.d
            if (r0 == 0) goto L32
            j0.h r0 = new j0.h
            j0.l.a.e0 r1 = new j0.l.a.e0
            r1.<init>(r3)
            r0.<init>(r1)
            return r0
        L32:
            boolean r0 = r2.e
            if (r0 == 0) goto L52
            j0.c r0 = new j0.c
            r0.<init>(r3)
            j0.d r3 = new j0.d     // Catch: java.lang.Throwable -> L41 java.lang.NullPointerException -> L50
            r3.<init>(r0)     // Catch: java.lang.Throwable -> L41 java.lang.NullPointerException -> L50
            return r3
        L41:
            r3 = move-exception
            j0.o.l.b(r3)
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            java.lang.String r1 = "Actually not, but can't pass out an exception otherwise..."
            r0.<init>(r1)
            r0.initCause(r3)
            throw r0
        L50:
            r3 = move-exception
            throw r3
        L52:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: i0.d0.a.f.b(i0.d):java.lang.Object");
    }
}
