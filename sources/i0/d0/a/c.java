package i0.d0.a;

import b.i.a.f.e.o.f;
import i0.d;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
/* compiled from: CallExecuteOnSubscribe.java */
/* loaded from: classes3.dex */
public final class c<T> implements Observable.a<Response<T>> {
    public final d<T> j;

    public c(d<T> dVar) {
        this.j = dVar;
    }

    @Override // rx.functions.Action1
    public void call(Object obj) {
        Subscriber subscriber = (Subscriber) obj;
        d<T> L = this.j.L();
        b bVar = new b(L, subscriber);
        subscriber.add(bVar);
        subscriber.setProducer(bVar);
        try {
            bVar.c(L.execute());
        } catch (Throwable th) {
            f.o1(th);
            bVar.b(th);
        }
    }
}
