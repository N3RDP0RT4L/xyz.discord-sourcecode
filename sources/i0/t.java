package i0;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Objects;
import okhttp3.Headers;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
/* compiled from: ParameterHandler.java */
/* loaded from: classes3.dex */
public abstract class t<T> {

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class a<T> extends t<T> {
        public final Method a;

        /* renamed from: b  reason: collision with root package name */
        public final int f3717b;
        public final i0.h<T, RequestBody> c;

        public a(Method method, int i, i0.h<T, RequestBody> hVar) {
            this.a = method;
            this.f3717b = i;
            this.c = hVar;
        }

        @Override // i0.t
        public void a(v vVar, T t) {
            if (t != null) {
                try {
                    vVar.m = this.c.convert(t);
                } catch (IOException e) {
                    Method method = this.a;
                    int i = this.f3717b;
                    throw c0.m(method, e, i, "Unable to convert " + t + " to RequestBody", new Object[0]);
                }
            } else {
                throw c0.l(this.a, this.f3717b, "Body parameter value must not be null.", new Object[0]);
            }
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class b<T> extends t<T> {
        public final String a;

        /* renamed from: b  reason: collision with root package name */
        public final i0.h<T, String> f3718b;
        public final boolean c;

        public b(String str, i0.h<T, String> hVar, boolean z2) {
            Objects.requireNonNull(str, "name == null");
            this.a = str;
            this.f3718b = hVar;
            this.c = z2;
        }

        @Override // i0.t
        public void a(v vVar, T t) throws IOException {
            String convert;
            if (t != null && (convert = this.f3718b.convert(t)) != null) {
                vVar.a(this.a, convert, this.c);
            }
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class c<T> extends t<Map<String, T>> {
        public final Method a;

        /* renamed from: b  reason: collision with root package name */
        public final int f3719b;
        public final i0.h<T, String> c;
        public final boolean d;

        public c(Method method, int i, i0.h<T, String> hVar, boolean z2) {
            this.a = method;
            this.f3719b = i;
            this.c = hVar;
            this.d = z2;
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // i0.t
        public void a(v vVar, Object obj) throws IOException {
            Map map = (Map) obj;
            if (map != null) {
                for (Map.Entry entry : map.entrySet()) {
                    String str = (String) entry.getKey();
                    if (str != null) {
                        Object value = entry.getValue();
                        if (value != null) {
                            String str2 = (String) this.c.convert(value);
                            if (str2 != null) {
                                vVar.a(str, str2, this.d);
                            } else {
                                Method method = this.a;
                                int i = this.f3719b;
                                throw c0.l(method, i, "Field map value '" + value + "' converted to null by " + this.c.getClass().getName() + " for key '" + str + "'.", new Object[0]);
                            }
                        } else {
                            throw c0.l(this.a, this.f3719b, b.d.b.a.a.w("Field map contained null value for key '", str, "'."), new Object[0]);
                        }
                    } else {
                        throw c0.l(this.a, this.f3719b, "Field map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw c0.l(this.a, this.f3719b, "Field map was null.", new Object[0]);
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class d<T> extends t<T> {
        public final String a;

        /* renamed from: b  reason: collision with root package name */
        public final i0.h<T, String> f3720b;

        public d(String str, i0.h<T, String> hVar) {
            Objects.requireNonNull(str, "name == null");
            this.a = str;
            this.f3720b = hVar;
        }

        @Override // i0.t
        public void a(v vVar, T t) throws IOException {
            String convert;
            if (t != null && (convert = this.f3720b.convert(t)) != null) {
                vVar.b(this.a, convert);
            }
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class e<T> extends t<Map<String, T>> {
        public final Method a;

        /* renamed from: b  reason: collision with root package name */
        public final int f3721b;
        public final i0.h<T, String> c;

        public e(Method method, int i, i0.h<T, String> hVar) {
            this.a = method;
            this.f3721b = i;
            this.c = hVar;
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // i0.t
        public void a(v vVar, Object obj) throws IOException {
            Map map = (Map) obj;
            if (map != null) {
                for (Map.Entry entry : map.entrySet()) {
                    String str = (String) entry.getKey();
                    if (str != null) {
                        Object value = entry.getValue();
                        if (value != null) {
                            vVar.b(str, (String) this.c.convert(value));
                        } else {
                            throw c0.l(this.a, this.f3721b, b.d.b.a.a.w("Header map contained null value for key '", str, "'."), new Object[0]);
                        }
                    } else {
                        throw c0.l(this.a, this.f3721b, "Header map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw c0.l(this.a, this.f3721b, "Header map was null.", new Object[0]);
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class f extends t<Headers> {
        public final Method a;

        /* renamed from: b  reason: collision with root package name */
        public final int f3722b;

        public f(Method method, int i) {
            this.a = method;
            this.f3722b = i;
        }

        @Override // i0.t
        public void a(v vVar, Headers headers) throws IOException {
            Headers headers2 = headers;
            if (headers2 != null) {
                Headers.a aVar = vVar.h;
                Objects.requireNonNull(aVar);
                d0.z.d.m.checkParameterIsNotNull(headers2, "headers");
                int size = headers2.size();
                for (int i = 0; i < size; i++) {
                    aVar.b(headers2.d(i), headers2.g(i));
                }
                return;
            }
            throw c0.l(this.a, this.f3722b, "Headers parameter must not be null.", new Object[0]);
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class g<T> extends t<T> {
        public final Method a;

        /* renamed from: b  reason: collision with root package name */
        public final int f3723b;
        public final Headers c;
        public final i0.h<T, RequestBody> d;

        public g(Method method, int i, Headers headers, i0.h<T, RequestBody> hVar) {
            this.a = method;
            this.f3723b = i;
            this.c = headers;
            this.d = hVar;
        }

        @Override // i0.t
        public void a(v vVar, T t) {
            if (t != null) {
                try {
                    vVar.c(this.c, this.d.convert(t));
                } catch (IOException e) {
                    Method method = this.a;
                    int i = this.f3723b;
                    throw c0.l(method, i, "Unable to convert " + t + " to RequestBody", e);
                }
            }
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class h<T> extends t<Map<String, T>> {
        public final Method a;

        /* renamed from: b  reason: collision with root package name */
        public final int f3724b;
        public final i0.h<T, RequestBody> c;
        public final String d;

        public h(Method method, int i, i0.h<T, RequestBody> hVar, String str) {
            this.a = method;
            this.f3724b = i;
            this.c = hVar;
            this.d = str;
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // i0.t
        public void a(v vVar, Object obj) throws IOException {
            Map map = (Map) obj;
            if (map != null) {
                for (Map.Entry entry : map.entrySet()) {
                    String str = (String) entry.getKey();
                    if (str != null) {
                        Object value = entry.getValue();
                        if (value != null) {
                            vVar.c(Headers.j.c("Content-Disposition", b.d.b.a.a.w("form-data; name=\"", str, "\""), "Content-Transfer-Encoding", this.d), (RequestBody) this.c.convert(value));
                        } else {
                            throw c0.l(this.a, this.f3724b, b.d.b.a.a.w("Part map contained null value for key '", str, "'."), new Object[0]);
                        }
                    } else {
                        throw c0.l(this.a, this.f3724b, "Part map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw c0.l(this.a, this.f3724b, "Part map was null.", new Object[0]);
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class i<T> extends t<T> {
        public final Method a;

        /* renamed from: b  reason: collision with root package name */
        public final int f3725b;
        public final String c;
        public final i0.h<T, String> d;
        public final boolean e;

        public i(Method method, int i, String str, i0.h<T, String> hVar, boolean z2) {
            this.a = method;
            this.f3725b = i;
            Objects.requireNonNull(str, "name == null");
            this.c = str;
            this.d = hVar;
            this.e = z2;
        }

        /* JADX WARN: Removed duplicated region for block: B:49:0x00e5  */
        /* JADX WARN: Removed duplicated region for block: B:51:0x00e8  */
        @Override // i0.t
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public void a(i0.v r18, T r19) throws java.io.IOException {
            /*
                Method dump skipped, instructions count: 275
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: i0.t.i.a(i0.v, java.lang.Object):void");
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class j<T> extends t<T> {
        public final String a;

        /* renamed from: b  reason: collision with root package name */
        public final i0.h<T, String> f3726b;
        public final boolean c;

        public j(String str, i0.h<T, String> hVar, boolean z2) {
            Objects.requireNonNull(str, "name == null");
            this.a = str;
            this.f3726b = hVar;
            this.c = z2;
        }

        @Override // i0.t
        public void a(v vVar, T t) throws IOException {
            String convert;
            if (t != null && (convert = this.f3726b.convert(t)) != null) {
                vVar.d(this.a, convert, this.c);
            }
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class k<T> extends t<Map<String, T>> {
        public final Method a;

        /* renamed from: b  reason: collision with root package name */
        public final int f3727b;
        public final i0.h<T, String> c;
        public final boolean d;

        public k(Method method, int i, i0.h<T, String> hVar, boolean z2) {
            this.a = method;
            this.f3727b = i;
            this.c = hVar;
            this.d = z2;
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // i0.t
        public void a(v vVar, Object obj) throws IOException {
            Map map = (Map) obj;
            if (map != null) {
                for (Map.Entry entry : map.entrySet()) {
                    String str = (String) entry.getKey();
                    if (str != null) {
                        Object value = entry.getValue();
                        if (value != null) {
                            String str2 = (String) this.c.convert(value);
                            if (str2 != null) {
                                vVar.d(str, str2, this.d);
                            } else {
                                Method method = this.a;
                                int i = this.f3727b;
                                throw c0.l(method, i, "Query map value '" + value + "' converted to null by " + this.c.getClass().getName() + " for key '" + str + "'.", new Object[0]);
                            }
                        } else {
                            throw c0.l(this.a, this.f3727b, b.d.b.a.a.w("Query map contained null value for key '", str, "'."), new Object[0]);
                        }
                    } else {
                        throw c0.l(this.a, this.f3727b, "Query map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw c0.l(this.a, this.f3727b, "Query map was null", new Object[0]);
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class l<T> extends t<T> {
        public final i0.h<T, String> a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f3728b;

        public l(i0.h<T, String> hVar, boolean z2) {
            this.a = hVar;
            this.f3728b = z2;
        }

        @Override // i0.t
        public void a(v vVar, T t) throws IOException {
            if (t != null) {
                vVar.d(this.a.convert(t), null, this.f3728b);
            }
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class m extends t<MultipartBody.Part> {
        public static final m a = new m();

        @Override // i0.t
        public void a(v vVar, MultipartBody.Part part) throws IOException {
            MultipartBody.Part part2 = part;
            if (part2 != null) {
                vVar.k.a(part2);
            }
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class n extends t<Object> {
        public final Method a;

        /* renamed from: b  reason: collision with root package name */
        public final int f3729b;

        public n(Method method, int i) {
            this.a = method;
            this.f3729b = i;
        }

        @Override // i0.t
        public void a(v vVar, Object obj) {
            if (obj != null) {
                Objects.requireNonNull(vVar);
                vVar.e = obj.toString();
                return;
            }
            throw c0.l(this.a, this.f3729b, "@Url parameter is null.", new Object[0]);
        }
    }

    /* compiled from: ParameterHandler.java */
    /* loaded from: classes3.dex */
    public static final class o<T> extends t<T> {
        public final Class<T> a;

        public o(Class<T> cls) {
            this.a = cls;
        }

        @Override // i0.t
        public void a(v vVar, T t) {
            vVar.g.e(this.a, t);
        }
    }

    public abstract void a(v vVar, T t) throws IOException;
}
