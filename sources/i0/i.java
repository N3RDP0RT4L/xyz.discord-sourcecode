package i0;

import i0.e;
import i0.i;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.concurrent.Executor;
import okhttp3.Request;
import retrofit2.Response;
/* compiled from: DefaultCallAdapterFactory.java */
/* loaded from: classes3.dex */
public final class i extends e.a {
    public final Executor a;

    /* compiled from: DefaultCallAdapterFactory.java */
    /* loaded from: classes3.dex */
    public class a implements e<Object, d<?>> {
        public final /* synthetic */ Type a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ Executor f3712b;

        public a(i iVar, Type type, Executor executor) {
            this.a = type;
            this.f3712b = executor;
        }

        @Override // i0.e
        public Type a() {
            return this.a;
        }

        @Override // i0.e
        public d<?> b(d<Object> dVar) {
            Executor executor = this.f3712b;
            return executor == null ? dVar : new b(executor, dVar);
        }
    }

    /* compiled from: DefaultCallAdapterFactory.java */
    /* loaded from: classes3.dex */
    public static final class b<T> implements d<T> {
        public final Executor j;
        public final d<T> k;

        /* compiled from: DefaultCallAdapterFactory.java */
        /* loaded from: classes3.dex */
        public class a implements f<T> {
            public final /* synthetic */ f a;

            public a(f fVar) {
                this.a = fVar;
            }

            @Override // i0.f
            public void a(d<T> dVar, final Throwable th) {
                Executor executor = b.this.j;
                final f fVar = this.a;
                executor.execute(new Runnable() { // from class: i0.a
                    @Override // java.lang.Runnable
                    public final void run() {
                        i.b.a aVar = i.b.a.this;
                        fVar.a(i.b.this, th);
                    }
                });
            }

            @Override // i0.f
            public void b(d<T> dVar, final Response<T> response) {
                Executor executor = b.this.j;
                final f fVar = this.a;
                executor.execute(new Runnable() { // from class: i0.b
                    @Override // java.lang.Runnable
                    public final void run() {
                        i.b.a aVar = i.b.a.this;
                        f fVar2 = fVar;
                        Response response2 = response;
                        if (i.b.this.k.d()) {
                            fVar2.a(i.b.this, new IOException("Canceled"));
                        } else {
                            fVar2.b(i.b.this, response2);
                        }
                    }
                });
            }
        }

        public b(Executor executor, d<T> dVar) {
            this.j = executor;
            this.k = dVar;
        }

        @Override // i0.d
        public void C(f<T> fVar) {
            this.k.C(new a(fVar));
        }

        @Override // i0.d
        /* renamed from: L */
        public d<T> clone() {
            return new b(this.j, this.k.L());
        }

        @Override // i0.d
        public Request c() {
            return this.k.c();
        }

        @Override // i0.d
        public void cancel() {
            this.k.cancel();
        }

        @Override // i0.d
        public boolean d() {
            return this.k.d();
        }

        @Override // i0.d
        public Response<T> execute() throws IOException {
            return this.k.execute();
        }
    }

    public i(Executor executor) {
        this.a = executor;
    }

    @Override // i0.e.a
    public e<?, ?> a(Type type, Annotation[] annotationArr, y yVar) {
        Executor executor = null;
        if (c0.f(type) != d.class) {
            return null;
        }
        if (type instanceof ParameterizedType) {
            Type e = c0.e(0, (ParameterizedType) type);
            if (!c0.i(annotationArr, a0.class)) {
                executor = this.a;
            }
            return new a(this, e, executor);
        }
        throw new IllegalArgumentException("Call return type must be parameterized as Call<Foo> or Call<? extends Foo>");
    }
}
