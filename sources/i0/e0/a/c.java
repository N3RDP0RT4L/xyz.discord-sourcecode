package i0.e0.a;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import g0.g;
import i0.h;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.Charset;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
/* compiled from: GsonResponseBodyConverter.java */
/* loaded from: classes3.dex */
public final class c<T> implements h<ResponseBody, T> {
    public final Gson a;

    /* renamed from: b  reason: collision with root package name */
    public final TypeAdapter<T> f3710b;

    public c(Gson gson, TypeAdapter<T> typeAdapter) {
        this.a = gson;
        this.f3710b = typeAdapter;
    }

    @Override // i0.h
    public Object convert(ResponseBody responseBody) throws IOException {
        Charset charset;
        ResponseBody responseBody2 = responseBody;
        Gson gson = this.a;
        Reader reader = responseBody2.k;
        if (reader == null) {
            g c = responseBody2.c();
            MediaType b2 = responseBody2.b();
            if (b2 == null || (charset = b2.a(d0.g0.c.a)) == null) {
                charset = d0.g0.c.a;
            }
            reader = new ResponseBody.a(c, charset);
            responseBody2.k = reader;
        }
        JsonReader k = gson.k(reader);
        try {
            T read = this.f3710b.read(k);
            if (k.N() == JsonToken.END_DOCUMENT) {
                return read;
            }
            throw new JsonIOException("JSON document was not fully consumed.");
        } finally {
            responseBody2.close();
        }
    }
}
