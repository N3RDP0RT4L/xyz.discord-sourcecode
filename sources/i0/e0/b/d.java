package i0.e0.b;

import b.d.b.a.a;
import i0.h;
import java.io.IOException;
import okhttp3.ResponseBody;
/* compiled from: ScalarResponseBodyConverters.java */
/* loaded from: classes3.dex */
public final class d implements h<ResponseBody, Character> {
    public static final d a = new d();

    @Override // i0.h
    public Character convert(ResponseBody responseBody) throws IOException {
        String d = responseBody.d();
        if (d.length() == 1) {
            return Character.valueOf(d.charAt(0));
        }
        StringBuilder R = a.R("Expected body of length 1 for Character conversion but was ");
        R.append(d.length());
        throw new IOException(R.toString());
    }
}
