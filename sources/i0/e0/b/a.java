package i0.e0.b;

import i0.h;
import java.io.IOException;
import okhttp3.MediaType;
import okhttp3.RequestBody;
/* compiled from: ScalarRequestBodyConverter.java */
/* loaded from: classes3.dex */
public final class a<T> implements h<T, RequestBody> {
    public static final a<Object> a = new a<>();

    /* renamed from: b  reason: collision with root package name */
    public static final MediaType f3711b = MediaType.a.a("text/plain; charset=UTF-8");

    static {
        MediaType.a aVar = MediaType.c;
    }

    @Override // i0.h
    public RequestBody convert(Object obj) throws IOException {
        return RequestBody.create(f3711b, String.valueOf(obj));
    }
}
