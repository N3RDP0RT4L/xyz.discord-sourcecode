package i0.e0.b;

import i0.h;
import java.io.IOException;
import okhttp3.ResponseBody;
/* compiled from: ScalarResponseBodyConverters.java */
/* loaded from: classes3.dex */
public final class f implements h<ResponseBody, Float> {
    public static final f a = new f();

    @Override // i0.h
    public Float convert(ResponseBody responseBody) throws IOException {
        return Float.valueOf(responseBody.d());
    }
}
