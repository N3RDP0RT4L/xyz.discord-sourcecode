package i0.e0.b;

import i0.h;
import java.io.IOException;
import okhttp3.ResponseBody;
/* compiled from: ScalarResponseBodyConverters.java */
/* loaded from: classes3.dex */
public final class c implements h<ResponseBody, Byte> {
    public static final c a = new c();

    @Override // i0.h
    public Byte convert(ResponseBody responseBody) throws IOException {
        return Byte.valueOf(responseBody.d());
    }
}
