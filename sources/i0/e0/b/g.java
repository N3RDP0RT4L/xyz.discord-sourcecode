package i0.e0.b;

import i0.h;
import java.io.IOException;
import okhttp3.ResponseBody;
/* compiled from: ScalarResponseBodyConverters.java */
/* loaded from: classes3.dex */
public final class g implements h<ResponseBody, Integer> {
    public static final g a = new g();

    @Override // i0.h
    public Integer convert(ResponseBody responseBody) throws IOException {
        return Integer.valueOf(responseBody.d());
    }
}
