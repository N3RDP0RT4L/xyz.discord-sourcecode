package i0.e0.b;

import i0.h;
import java.io.IOException;
import okhttp3.ResponseBody;
/* compiled from: ScalarResponseBodyConverters.java */
/* loaded from: classes3.dex */
public final class i implements h<ResponseBody, Short> {
    public static final i a = new i();

    @Override // i0.h
    public Short convert(ResponseBody responseBody) throws IOException {
        return Short.valueOf(responseBody.d());
    }
}
