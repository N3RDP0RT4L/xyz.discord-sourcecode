package i0.e0.b;

import i0.h;
import java.io.IOException;
import okhttp3.ResponseBody;
/* compiled from: ScalarResponseBodyConverters.java */
/* loaded from: classes3.dex */
public final class j implements h<ResponseBody, String> {
    public static final j a = new j();

    @Override // i0.h
    public String convert(ResponseBody responseBody) throws IOException {
        return responseBody.d();
    }
}
