package i0;

import retrofit2.Response;
/* compiled from: Callback.java */
/* loaded from: classes3.dex */
public interface f<T> {
    void a(d<T> dVar, Throwable th);

    void b(d<T> dVar, Response<T> response);
}
