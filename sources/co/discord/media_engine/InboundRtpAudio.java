package co.discord.media_engine;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: Statistics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\bU\b\u0086\b\u0018\u00002\u00020\u0001B\u0095\u0002\u0012\u0006\u00100\u001a\u00020\u0002\u0012\n\u00101\u001a\u00060\u0005j\u0002`\u0006\u0012\u0006\u00102\u001a\u00020\t\u0012\u0006\u00103\u001a\u00020\u0005\u0012\u0006\u00104\u001a\u00020\u0005\u0012\u0006\u00105\u001a\u00020\u0005\u0012\u0006\u00106\u001a\u00020\u000f\u0012\u0006\u00107\u001a\u00020\u0012\u0012\u0006\u00108\u001a\u00020\u0005\u0012\u0006\u00109\u001a\u00020\u0005\u0012\u0006\u0010:\u001a\u00020\u0005\u0012\u0006\u0010;\u001a\u00020\u0005\u0012\u0006\u0010<\u001a\u00020\u0019\u0012\u0006\u0010=\u001a\u00020\u0019\u0012\u0006\u0010>\u001a\u00020\u0019\u0012\u0006\u0010?\u001a\u00020\u0019\u0012\u0006\u0010@\u001a\u00020\u0019\u0012\b\u0010A\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010B\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010C\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010D\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010E\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010F\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010G\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010H\u001a\u0004\u0018\u00010(\u0012\b\u0010I\u001a\u0004\u0018\u00010(\u0012\b\u0010J\u001a\u0004\u0018\u00010(\u0012\b\u0010K\u001a\u0004\u0018\u00010(\u0012\b\u0010L\u001a\u0004\u0018\u00010(\u0012\b\u0010M\u001a\u0004\u0018\u00010(¢\u0006\u0004\b{\u0010|J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0014\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\f\u0010\bJ\u0010\u0010\r\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\r\u0010\bJ\u0010\u0010\u000e\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u000e\u0010\bJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0015\u0010\bJ\u0010\u0010\u0016\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0016\u0010\bJ\u0010\u0010\u0017\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0017\u0010\bJ\u0010\u0010\u0018\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0018\u0010\bJ\u0010\u0010\u001a\u001a\u00020\u0019HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u001c\u001a\u00020\u0019HÆ\u0003¢\u0006\u0004\b\u001c\u0010\u001bJ\u0010\u0010\u001d\u001a\u00020\u0019HÆ\u0003¢\u0006\u0004\b\u001d\u0010\u001bJ\u0010\u0010\u001e\u001a\u00020\u0019HÆ\u0003¢\u0006\u0004\b\u001e\u0010\u001bJ\u0010\u0010\u001f\u001a\u00020\u0019HÆ\u0003¢\u0006\u0004\b\u001f\u0010\u001bJ\u0012\u0010 \u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b \u0010!J\u0012\u0010\"\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\"\u0010!J\u0012\u0010#\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b#\u0010!J\u0012\u0010$\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b$\u0010!J\u0012\u0010%\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b%\u0010!J\u0012\u0010&\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b&\u0010!J\u0012\u0010'\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b'\u0010!J\u0012\u0010)\u001a\u0004\u0018\u00010(HÆ\u0003¢\u0006\u0004\b)\u0010*J\u0012\u0010+\u001a\u0004\u0018\u00010(HÆ\u0003¢\u0006\u0004\b+\u0010*J\u0012\u0010,\u001a\u0004\u0018\u00010(HÆ\u0003¢\u0006\u0004\b,\u0010*J\u0012\u0010-\u001a\u0004\u0018\u00010(HÆ\u0003¢\u0006\u0004\b-\u0010*J\u0012\u0010.\u001a\u0004\u0018\u00010(HÆ\u0003¢\u0006\u0004\b.\u0010*J\u0012\u0010/\u001a\u0004\u0018\u00010(HÆ\u0003¢\u0006\u0004\b/\u0010*JÚ\u0002\u0010N\u001a\u00020\u00002\b\b\u0002\u00100\u001a\u00020\u00022\f\b\u0002\u00101\u001a\u00060\u0005j\u0002`\u00062\b\b\u0002\u00102\u001a\u00020\t2\b\b\u0002\u00103\u001a\u00020\u00052\b\b\u0002\u00104\u001a\u00020\u00052\b\b\u0002\u00105\u001a\u00020\u00052\b\b\u0002\u00106\u001a\u00020\u000f2\b\b\u0002\u00107\u001a\u00020\u00122\b\b\u0002\u00108\u001a\u00020\u00052\b\b\u0002\u00109\u001a\u00020\u00052\b\b\u0002\u0010:\u001a\u00020\u00052\b\b\u0002\u0010;\u001a\u00020\u00052\b\b\u0002\u0010<\u001a\u00020\u00192\b\b\u0002\u0010=\u001a\u00020\u00192\b\b\u0002\u0010>\u001a\u00020\u00192\b\b\u0002\u0010?\u001a\u00020\u00192\b\b\u0002\u0010@\u001a\u00020\u00192\n\b\u0002\u0010A\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010B\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010C\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010D\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010E\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010F\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010G\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010H\u001a\u0004\u0018\u00010(2\n\b\u0002\u0010I\u001a\u0004\u0018\u00010(2\n\b\u0002\u0010J\u001a\u0004\u0018\u00010(2\n\b\u0002\u0010K\u001a\u0004\u0018\u00010(2\n\b\u0002\u0010L\u001a\u0004\u0018\u00010(2\n\b\u0002\u0010M\u001a\u0004\u0018\u00010(HÆ\u0001¢\u0006\u0004\bN\u0010OJ\u0010\u0010P\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\bP\u0010\u0004J\u0010\u0010Q\u001a\u00020\u0019HÖ\u0001¢\u0006\u0004\bQ\u0010\u001bJ\u001a\u0010S\u001a\u00020\u00122\b\u0010R\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\bS\u0010TR\u001b\u0010L\u001a\u0004\u0018\u00010(8\u0006@\u0006¢\u0006\f\n\u0004\bL\u0010U\u001a\u0004\bV\u0010*R\u001d\u00101\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010W\u001a\u0004\bX\u0010\bR\u0019\u0010:\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010W\u001a\u0004\bY\u0010\bR\u001b\u0010B\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010Z\u001a\u0004\b[\u0010!R\u001b\u0010I\u001a\u0004\u0018\u00010(8\u0006@\u0006¢\u0006\f\n\u0004\bI\u0010U\u001a\u0004\b\\\u0010*R\u001b\u0010F\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\bF\u0010Z\u001a\u0004\b]\u0010!R\u0019\u00109\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010W\u001a\u0004\b^\u0010\bR\u0019\u00103\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010W\u001a\u0004\b_\u0010\bR\u0019\u0010?\u001a\u00020\u00198\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010`\u001a\u0004\ba\u0010\u001bR\u0019\u0010<\u001a\u00020\u00198\u0006@\u0006¢\u0006\f\n\u0004\b<\u0010`\u001a\u0004\bb\u0010\u001bR\u0019\u00104\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010W\u001a\u0004\bc\u0010\bR\u0019\u00102\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010d\u001a\u0004\be\u0010\u000bR\u0019\u0010;\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010W\u001a\u0004\bf\u0010\bR\u001b\u0010D\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\bD\u0010Z\u001a\u0004\bg\u0010!R\u001b\u0010K\u001a\u0004\u0018\u00010(8\u0006@\u0006¢\u0006\f\n\u0004\bK\u0010U\u001a\u0004\bh\u0010*R\u001b\u0010J\u001a\u0004\u0018\u00010(8\u0006@\u0006¢\u0006\f\n\u0004\bJ\u0010U\u001a\u0004\bi\u0010*R\u0019\u0010=\u001a\u00020\u00198\u0006@\u0006¢\u0006\f\n\u0004\b=\u0010`\u001a\u0004\bj\u0010\u001bR\u0019\u00100\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010k\u001a\u0004\bl\u0010\u0004R\u001b\u0010A\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\bA\u0010Z\u001a\u0004\bm\u0010!R\u0019\u00108\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010W\u001a\u0004\bn\u0010\bR\u0019\u00106\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010o\u001a\u0004\bp\u0010\u0011R\u001b\u0010C\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\bC\u0010Z\u001a\u0004\bq\u0010!R\u0019\u0010>\u001a\u00020\u00198\u0006@\u0006¢\u0006\f\n\u0004\b>\u0010`\u001a\u0004\br\u0010\u001bR\u0019\u0010@\u001a\u00020\u00198\u0006@\u0006¢\u0006\f\n\u0004\b@\u0010`\u001a\u0004\bs\u0010\u001bR\u001b\u0010E\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\bE\u0010Z\u001a\u0004\bt\u0010!R\u0019\u00105\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010W\u001a\u0004\bu\u0010\bR\u001b\u0010G\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\bG\u0010Z\u001a\u0004\bv\u0010!R\u001b\u0010M\u001a\u0004\u0018\u00010(8\u0006@\u0006¢\u0006\f\n\u0004\bM\u0010U\u001a\u0004\bw\u0010*R\u001b\u0010H\u001a\u0004\u0018\u00010(8\u0006@\u0006¢\u0006\f\n\u0004\bH\u0010U\u001a\u0004\bx\u0010*R\u0019\u00107\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010y\u001a\u0004\bz\u0010\u0014¨\u0006}"}, d2 = {"Lco/discord/media_engine/InboundRtpAudio;", "", "", "component1", "()Ljava/lang/String;", "", "Lco/discord/media_engine/U32;", "component2", "()J", "Lco/discord/media_engine/StatsCodec;", "component3", "()Lco/discord/media_engine/StatsCodec;", "component4", "component5", "component6", "", "component7", "()F", "", "component8", "()Z", "component9", "component10", "component11", "component12", "", "component13", "()I", "component14", "component15", "component16", "component17", "component18", "()Ljava/lang/Long;", "component19", "component20", "component21", "component22", "component23", "component24", "Lco/discord/media_engine/PlayoutMetric;", "component25", "()Lco/discord/media_engine/PlayoutMetric;", "component26", "component27", "component28", "component29", "component30", "type", "ssrc", "codec", "bytesReceived", "packetsReceived", "packetsLost", "audioLevel", "audioDetected", "jitter", "jitterBuffer", "jitterBufferPreferred", "delayEstimate", "decodingCNG", "decodingMutedOutput", "decodingNormal", "decodingPLC", "decodingPLCCNG", "opSilence", "opNormal", "opMerge", "opExpand", "opAccelerate", "opPreemptiveExpand", "opCNG", "audioJitterBuffer", "audioJitterDelay", "audioJitterTarget", "audioPlayoutUnderruns", "relativeReceptionDelay", "relativePlayoutDelay", "copy", "(Ljava/lang/String;JLco/discord/media_engine/StatsCodec;JJJFZJJJJIIIIILjava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;)Lco/discord/media_engine/InboundRtpAudio;", "toString", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Lco/discord/media_engine/PlayoutMetric;", "getRelativeReceptionDelay", "J", "getSsrc", "getJitterBufferPreferred", "Ljava/lang/Long;", "getOpNormal", "getAudioJitterDelay", "getOpPreemptiveExpand", "getJitterBuffer", "getBytesReceived", "I", "getDecodingPLC", "getDecodingCNG", "getPacketsReceived", "Lco/discord/media_engine/StatsCodec;", "getCodec", "getDelayEstimate", "getOpExpand", "getAudioPlayoutUnderruns", "getAudioJitterTarget", "getDecodingMutedOutput", "Ljava/lang/String;", "getType", "getOpSilence", "getJitter", "F", "getAudioLevel", "getOpMerge", "getDecodingNormal", "getDecodingPLCCNG", "getOpAccelerate", "getPacketsLost", "getOpCNG", "getRelativePlayoutDelay", "getAudioJitterBuffer", "Z", "getAudioDetected", HookHelper.constructorName, "(Ljava/lang/String;JLco/discord/media_engine/StatsCodec;JJJFZJJJJIIIIILjava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;)V", "android_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class InboundRtpAudio {
    private final boolean audioDetected;
    private final PlayoutMetric audioJitterBuffer;
    private final PlayoutMetric audioJitterDelay;
    private final PlayoutMetric audioJitterTarget;
    private final float audioLevel;
    private final PlayoutMetric audioPlayoutUnderruns;
    private final long bytesReceived;
    private final StatsCodec codec;
    private final int decodingCNG;
    private final int decodingMutedOutput;
    private final int decodingNormal;
    private final int decodingPLC;
    private final int decodingPLCCNG;
    private final long delayEstimate;
    private final long jitter;
    private final long jitterBuffer;
    private final long jitterBufferPreferred;
    private final Long opAccelerate;
    private final Long opCNG;
    private final Long opExpand;
    private final Long opMerge;
    private final Long opNormal;
    private final Long opPreemptiveExpand;
    private final Long opSilence;
    private final long packetsLost;
    private final long packetsReceived;
    private final PlayoutMetric relativePlayoutDelay;
    private final PlayoutMetric relativeReceptionDelay;
    private final long ssrc;
    private final String type;

    public InboundRtpAudio(String str, long j, StatsCodec statsCodec, long j2, long j3, long j4, float f, boolean z2, long j5, long j6, long j7, long j8, int i, int i2, int i3, int i4, int i5, Long l, Long l2, Long l3, Long l4, Long l5, Long l6, Long l7, PlayoutMetric playoutMetric, PlayoutMetric playoutMetric2, PlayoutMetric playoutMetric3, PlayoutMetric playoutMetric4, PlayoutMetric playoutMetric5, PlayoutMetric playoutMetric6) {
        m.checkNotNullParameter(str, "type");
        m.checkNotNullParameter(statsCodec, "codec");
        this.type = str;
        this.ssrc = j;
        this.codec = statsCodec;
        this.bytesReceived = j2;
        this.packetsReceived = j3;
        this.packetsLost = j4;
        this.audioLevel = f;
        this.audioDetected = z2;
        this.jitter = j5;
        this.jitterBuffer = j6;
        this.jitterBufferPreferred = j7;
        this.delayEstimate = j8;
        this.decodingCNG = i;
        this.decodingMutedOutput = i2;
        this.decodingNormal = i3;
        this.decodingPLC = i4;
        this.decodingPLCCNG = i5;
        this.opSilence = l;
        this.opNormal = l2;
        this.opMerge = l3;
        this.opExpand = l4;
        this.opAccelerate = l5;
        this.opPreemptiveExpand = l6;
        this.opCNG = l7;
        this.audioJitterBuffer = playoutMetric;
        this.audioJitterDelay = playoutMetric2;
        this.audioJitterTarget = playoutMetric3;
        this.audioPlayoutUnderruns = playoutMetric4;
        this.relativeReceptionDelay = playoutMetric5;
        this.relativePlayoutDelay = playoutMetric6;
    }

    public final String component1() {
        return this.type;
    }

    public final long component10() {
        return this.jitterBuffer;
    }

    public final long component11() {
        return this.jitterBufferPreferred;
    }

    public final long component12() {
        return this.delayEstimate;
    }

    public final int component13() {
        return this.decodingCNG;
    }

    public final int component14() {
        return this.decodingMutedOutput;
    }

    public final int component15() {
        return this.decodingNormal;
    }

    public final int component16() {
        return this.decodingPLC;
    }

    public final int component17() {
        return this.decodingPLCCNG;
    }

    public final Long component18() {
        return this.opSilence;
    }

    public final Long component19() {
        return this.opNormal;
    }

    public final long component2() {
        return this.ssrc;
    }

    public final Long component20() {
        return this.opMerge;
    }

    public final Long component21() {
        return this.opExpand;
    }

    public final Long component22() {
        return this.opAccelerate;
    }

    public final Long component23() {
        return this.opPreemptiveExpand;
    }

    public final Long component24() {
        return this.opCNG;
    }

    public final PlayoutMetric component25() {
        return this.audioJitterBuffer;
    }

    public final PlayoutMetric component26() {
        return this.audioJitterDelay;
    }

    public final PlayoutMetric component27() {
        return this.audioJitterTarget;
    }

    public final PlayoutMetric component28() {
        return this.audioPlayoutUnderruns;
    }

    public final PlayoutMetric component29() {
        return this.relativeReceptionDelay;
    }

    public final StatsCodec component3() {
        return this.codec;
    }

    public final PlayoutMetric component30() {
        return this.relativePlayoutDelay;
    }

    public final long component4() {
        return this.bytesReceived;
    }

    public final long component5() {
        return this.packetsReceived;
    }

    public final long component6() {
        return this.packetsLost;
    }

    public final float component7() {
        return this.audioLevel;
    }

    public final boolean component8() {
        return this.audioDetected;
    }

    public final long component9() {
        return this.jitter;
    }

    public final InboundRtpAudio copy(String str, long j, StatsCodec statsCodec, long j2, long j3, long j4, float f, boolean z2, long j5, long j6, long j7, long j8, int i, int i2, int i3, int i4, int i5, Long l, Long l2, Long l3, Long l4, Long l5, Long l6, Long l7, PlayoutMetric playoutMetric, PlayoutMetric playoutMetric2, PlayoutMetric playoutMetric3, PlayoutMetric playoutMetric4, PlayoutMetric playoutMetric5, PlayoutMetric playoutMetric6) {
        m.checkNotNullParameter(str, "type");
        m.checkNotNullParameter(statsCodec, "codec");
        return new InboundRtpAudio(str, j, statsCodec, j2, j3, j4, f, z2, j5, j6, j7, j8, i, i2, i3, i4, i5, l, l2, l3, l4, l5, l6, l7, playoutMetric, playoutMetric2, playoutMetric3, playoutMetric4, playoutMetric5, playoutMetric6);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof InboundRtpAudio)) {
            return false;
        }
        InboundRtpAudio inboundRtpAudio = (InboundRtpAudio) obj;
        return m.areEqual(this.type, inboundRtpAudio.type) && this.ssrc == inboundRtpAudio.ssrc && m.areEqual(this.codec, inboundRtpAudio.codec) && this.bytesReceived == inboundRtpAudio.bytesReceived && this.packetsReceived == inboundRtpAudio.packetsReceived && this.packetsLost == inboundRtpAudio.packetsLost && Float.compare(this.audioLevel, inboundRtpAudio.audioLevel) == 0 && this.audioDetected == inboundRtpAudio.audioDetected && this.jitter == inboundRtpAudio.jitter && this.jitterBuffer == inboundRtpAudio.jitterBuffer && this.jitterBufferPreferred == inboundRtpAudio.jitterBufferPreferred && this.delayEstimate == inboundRtpAudio.delayEstimate && this.decodingCNG == inboundRtpAudio.decodingCNG && this.decodingMutedOutput == inboundRtpAudio.decodingMutedOutput && this.decodingNormal == inboundRtpAudio.decodingNormal && this.decodingPLC == inboundRtpAudio.decodingPLC && this.decodingPLCCNG == inboundRtpAudio.decodingPLCCNG && m.areEqual(this.opSilence, inboundRtpAudio.opSilence) && m.areEqual(this.opNormal, inboundRtpAudio.opNormal) && m.areEqual(this.opMerge, inboundRtpAudio.opMerge) && m.areEqual(this.opExpand, inboundRtpAudio.opExpand) && m.areEqual(this.opAccelerate, inboundRtpAudio.opAccelerate) && m.areEqual(this.opPreemptiveExpand, inboundRtpAudio.opPreemptiveExpand) && m.areEqual(this.opCNG, inboundRtpAudio.opCNG) && m.areEqual(this.audioJitterBuffer, inboundRtpAudio.audioJitterBuffer) && m.areEqual(this.audioJitterDelay, inboundRtpAudio.audioJitterDelay) && m.areEqual(this.audioJitterTarget, inboundRtpAudio.audioJitterTarget) && m.areEqual(this.audioPlayoutUnderruns, inboundRtpAudio.audioPlayoutUnderruns) && m.areEqual(this.relativeReceptionDelay, inboundRtpAudio.relativeReceptionDelay) && m.areEqual(this.relativePlayoutDelay, inboundRtpAudio.relativePlayoutDelay);
    }

    public final boolean getAudioDetected() {
        return this.audioDetected;
    }

    public final PlayoutMetric getAudioJitterBuffer() {
        return this.audioJitterBuffer;
    }

    public final PlayoutMetric getAudioJitterDelay() {
        return this.audioJitterDelay;
    }

    public final PlayoutMetric getAudioJitterTarget() {
        return this.audioJitterTarget;
    }

    public final float getAudioLevel() {
        return this.audioLevel;
    }

    public final PlayoutMetric getAudioPlayoutUnderruns() {
        return this.audioPlayoutUnderruns;
    }

    public final long getBytesReceived() {
        return this.bytesReceived;
    }

    public final StatsCodec getCodec() {
        return this.codec;
    }

    public final int getDecodingCNG() {
        return this.decodingCNG;
    }

    public final int getDecodingMutedOutput() {
        return this.decodingMutedOutput;
    }

    public final int getDecodingNormal() {
        return this.decodingNormal;
    }

    public final int getDecodingPLC() {
        return this.decodingPLC;
    }

    public final int getDecodingPLCCNG() {
        return this.decodingPLCCNG;
    }

    public final long getDelayEstimate() {
        return this.delayEstimate;
    }

    public final long getJitter() {
        return this.jitter;
    }

    public final long getJitterBuffer() {
        return this.jitterBuffer;
    }

    public final long getJitterBufferPreferred() {
        return this.jitterBufferPreferred;
    }

    public final Long getOpAccelerate() {
        return this.opAccelerate;
    }

    public final Long getOpCNG() {
        return this.opCNG;
    }

    public final Long getOpExpand() {
        return this.opExpand;
    }

    public final Long getOpMerge() {
        return this.opMerge;
    }

    public final Long getOpNormal() {
        return this.opNormal;
    }

    public final Long getOpPreemptiveExpand() {
        return this.opPreemptiveExpand;
    }

    public final Long getOpSilence() {
        return this.opSilence;
    }

    public final long getPacketsLost() {
        return this.packetsLost;
    }

    public final long getPacketsReceived() {
        return this.packetsReceived;
    }

    public final PlayoutMetric getRelativePlayoutDelay() {
        return this.relativePlayoutDelay;
    }

    public final PlayoutMetric getRelativeReceptionDelay() {
        return this.relativeReceptionDelay;
    }

    public final long getSsrc() {
        return this.ssrc;
    }

    public final String getType() {
        return this.type;
    }

    public int hashCode() {
        String str = this.type;
        int i = 0;
        int a = (b.a(this.ssrc) + ((str != null ? str.hashCode() : 0) * 31)) * 31;
        StatsCodec statsCodec = this.codec;
        int hashCode = statsCodec != null ? statsCodec.hashCode() : 0;
        int a2 = b.a(this.bytesReceived);
        int a3 = b.a(this.packetsReceived);
        int floatToIntBits = (Float.floatToIntBits(this.audioLevel) + ((b.a(this.packetsLost) + ((a3 + ((a2 + ((a + hashCode) * 31)) * 31)) * 31)) * 31)) * 31;
        boolean z2 = this.audioDetected;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int a4 = b.a(this.jitter);
        int a5 = b.a(this.jitterBuffer);
        int a6 = (((((((((((b.a(this.delayEstimate) + ((b.a(this.jitterBufferPreferred) + ((a5 + ((a4 + ((floatToIntBits + i2) * 31)) * 31)) * 31)) * 31)) * 31) + this.decodingCNG) * 31) + this.decodingMutedOutput) * 31) + this.decodingNormal) * 31) + this.decodingPLC) * 31) + this.decodingPLCCNG) * 31;
        Long l = this.opSilence;
        int hashCode2 = (a6 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.opNormal;
        int hashCode3 = (hashCode2 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.opMerge;
        int hashCode4 = (hashCode3 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.opExpand;
        int hashCode5 = (hashCode4 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.opAccelerate;
        int hashCode6 = (hashCode5 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.opPreemptiveExpand;
        int hashCode7 = (hashCode6 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.opCNG;
        int hashCode8 = (hashCode7 + (l7 != null ? l7.hashCode() : 0)) * 31;
        PlayoutMetric playoutMetric = this.audioJitterBuffer;
        int hashCode9 = (hashCode8 + (playoutMetric != null ? playoutMetric.hashCode() : 0)) * 31;
        PlayoutMetric playoutMetric2 = this.audioJitterDelay;
        int hashCode10 = (hashCode9 + (playoutMetric2 != null ? playoutMetric2.hashCode() : 0)) * 31;
        PlayoutMetric playoutMetric3 = this.audioJitterTarget;
        int hashCode11 = (hashCode10 + (playoutMetric3 != null ? playoutMetric3.hashCode() : 0)) * 31;
        PlayoutMetric playoutMetric4 = this.audioPlayoutUnderruns;
        int hashCode12 = (hashCode11 + (playoutMetric4 != null ? playoutMetric4.hashCode() : 0)) * 31;
        PlayoutMetric playoutMetric5 = this.relativeReceptionDelay;
        int hashCode13 = (hashCode12 + (playoutMetric5 != null ? playoutMetric5.hashCode() : 0)) * 31;
        PlayoutMetric playoutMetric6 = this.relativePlayoutDelay;
        if (playoutMetric6 != null) {
            i = playoutMetric6.hashCode();
        }
        return hashCode13 + i;
    }

    public String toString() {
        StringBuilder R = a.R("InboundRtpAudio(type=");
        R.append(this.type);
        R.append(", ssrc=");
        R.append(this.ssrc);
        R.append(", codec=");
        R.append(this.codec);
        R.append(", bytesReceived=");
        R.append(this.bytesReceived);
        R.append(", packetsReceived=");
        R.append(this.packetsReceived);
        R.append(", packetsLost=");
        R.append(this.packetsLost);
        R.append(", audioLevel=");
        R.append(this.audioLevel);
        R.append(", audioDetected=");
        R.append(this.audioDetected);
        R.append(", jitter=");
        R.append(this.jitter);
        R.append(", jitterBuffer=");
        R.append(this.jitterBuffer);
        R.append(", jitterBufferPreferred=");
        R.append(this.jitterBufferPreferred);
        R.append(", delayEstimate=");
        R.append(this.delayEstimate);
        R.append(", decodingCNG=");
        R.append(this.decodingCNG);
        R.append(", decodingMutedOutput=");
        R.append(this.decodingMutedOutput);
        R.append(", decodingNormal=");
        R.append(this.decodingNormal);
        R.append(", decodingPLC=");
        R.append(this.decodingPLC);
        R.append(", decodingPLCCNG=");
        R.append(this.decodingPLCCNG);
        R.append(", opSilence=");
        R.append(this.opSilence);
        R.append(", opNormal=");
        R.append(this.opNormal);
        R.append(", opMerge=");
        R.append(this.opMerge);
        R.append(", opExpand=");
        R.append(this.opExpand);
        R.append(", opAccelerate=");
        R.append(this.opAccelerate);
        R.append(", opPreemptiveExpand=");
        R.append(this.opPreemptiveExpand);
        R.append(", opCNG=");
        R.append(this.opCNG);
        R.append(", audioJitterBuffer=");
        R.append(this.audioJitterBuffer);
        R.append(", audioJitterDelay=");
        R.append(this.audioJitterDelay);
        R.append(", audioJitterTarget=");
        R.append(this.audioJitterTarget);
        R.append(", audioPlayoutUnderruns=");
        R.append(this.audioPlayoutUnderruns);
        R.append(", relativeReceptionDelay=");
        R.append(this.relativeReceptionDelay);
        R.append(", relativePlayoutDelay=");
        R.append(this.relativePlayoutDelay);
        R.append(")");
        return R.toString();
    }
}
