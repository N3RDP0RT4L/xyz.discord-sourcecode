package co.discord.media_engine;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: StreamParameters.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b#\b\u0086\b\u0018\u00002\u00020\u0001BG\u0012\u0006\u0010\u0012\u001a\u00020\u0002\u0012\u0006\u0010\u0013\u001a\u00020\u0005\u0012\u0006\u0010\u0014\u001a\u00020\b\u0012\u0006\u0010\u0015\u001a\u00020\b\u0012\u0006\u0010\u0016\u001a\u00020\f\u0012\u0006\u0010\u0017\u001a\u00020\b\u0012\u0006\u0010\u0018\u001a\u00020\b\u0012\u0006\u0010\u0019\u001a\u00020\b¢\u0006\u0004\b-\u0010.J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u000b\u0010\nJ\u0010\u0010\r\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u000f\u0010\nJ\u0010\u0010\u0010\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u0010\u0010\nJ\u0010\u0010\u0011\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u0011\u0010\nJ`\u0010\u001a\u001a\u00020\u00002\b\b\u0002\u0010\u0012\u001a\u00020\u00022\b\b\u0002\u0010\u0013\u001a\u00020\u00052\b\b\u0002\u0010\u0014\u001a\u00020\b2\b\b\u0002\u0010\u0015\u001a\u00020\b2\b\b\u0002\u0010\u0016\u001a\u00020\f2\b\b\u0002\u0010\u0017\u001a\u00020\b2\b\b\u0002\u0010\u0018\u001a\u00020\b2\b\b\u0002\u0010\u0019\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u001c\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u001c\u0010\u0007J\u0010\u0010\u001d\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u001d\u0010\nJ\u001a\u0010\u001f\u001a\u00020\f2\b\u0010\u001e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001f\u0010 R\u0019\u0010\u0018\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010!\u001a\u0004\b\"\u0010\nR\u0019\u0010\u0012\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010#\u001a\u0004\b$\u0010\u0004R\u0019\u0010\u0019\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010!\u001a\u0004\b%\u0010\nR\u0019\u0010\u0016\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010&\u001a\u0004\b'\u0010\u000eR\u0019\u0010\u0014\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010!\u001a\u0004\b(\u0010\nR\u0019\u0010\u0013\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010)\u001a\u0004\b*\u0010\u0007R\u0019\u0010\u0015\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010!\u001a\u0004\b+\u0010\nR\u0019\u0010\u0017\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010!\u001a\u0004\b,\u0010\n¨\u0006/"}, d2 = {"Lco/discord/media_engine/StreamParameters;", "", "Lco/discord/media_engine/MediaType;", "component1", "()Lco/discord/media_engine/MediaType;", "", "component2", "()Ljava/lang/String;", "", "component3", "()I", "component4", "", "component5", "()Z", "component6", "component7", "component8", "type", "rid", "ssrc", "rtxSsrc", "active", "maxBitrate", "quality", "maxPixelCount", "copy", "(Lco/discord/media_engine/MediaType;Ljava/lang/String;IIZIII)Lco/discord/media_engine/StreamParameters;", "toString", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "I", "getQuality", "Lco/discord/media_engine/MediaType;", "getType", "getMaxPixelCount", "Z", "getActive", "getSsrc", "Ljava/lang/String;", "getRid", "getRtxSsrc", "getMaxBitrate", HookHelper.constructorName, "(Lco/discord/media_engine/MediaType;Ljava/lang/String;IIZIII)V", "android_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StreamParameters {
    private final boolean active;
    private final int maxBitrate;
    private final int maxPixelCount;
    private final int quality;
    private final String rid;
    private final int rtxSsrc;
    private final int ssrc;
    private final MediaType type;

    public StreamParameters(MediaType mediaType, String str, int i, int i2, boolean z2, int i3, int i4, int i5) {
        m.checkNotNullParameter(mediaType, "type");
        m.checkNotNullParameter(str, "rid");
        this.type = mediaType;
        this.rid = str;
        this.ssrc = i;
        this.rtxSsrc = i2;
        this.active = z2;
        this.maxBitrate = i3;
        this.quality = i4;
        this.maxPixelCount = i5;
    }

    public final MediaType component1() {
        return this.type;
    }

    public final String component2() {
        return this.rid;
    }

    public final int component3() {
        return this.ssrc;
    }

    public final int component4() {
        return this.rtxSsrc;
    }

    public final boolean component5() {
        return this.active;
    }

    public final int component6() {
        return this.maxBitrate;
    }

    public final int component7() {
        return this.quality;
    }

    public final int component8() {
        return this.maxPixelCount;
    }

    public final StreamParameters copy(MediaType mediaType, String str, int i, int i2, boolean z2, int i3, int i4, int i5) {
        m.checkNotNullParameter(mediaType, "type");
        m.checkNotNullParameter(str, "rid");
        return new StreamParameters(mediaType, str, i, i2, z2, i3, i4, i5);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StreamParameters)) {
            return false;
        }
        StreamParameters streamParameters = (StreamParameters) obj;
        return m.areEqual(this.type, streamParameters.type) && m.areEqual(this.rid, streamParameters.rid) && this.ssrc == streamParameters.ssrc && this.rtxSsrc == streamParameters.rtxSsrc && this.active == streamParameters.active && this.maxBitrate == streamParameters.maxBitrate && this.quality == streamParameters.quality && this.maxPixelCount == streamParameters.maxPixelCount;
    }

    public final boolean getActive() {
        return this.active;
    }

    public final int getMaxBitrate() {
        return this.maxBitrate;
    }

    public final int getMaxPixelCount() {
        return this.maxPixelCount;
    }

    public final int getQuality() {
        return this.quality;
    }

    public final String getRid() {
        return this.rid;
    }

    public final int getRtxSsrc() {
        return this.rtxSsrc;
    }

    public final int getSsrc() {
        return this.ssrc;
    }

    public final MediaType getType() {
        return this.type;
    }

    public int hashCode() {
        MediaType mediaType = this.type;
        int i = 0;
        int hashCode = (mediaType != null ? mediaType.hashCode() : 0) * 31;
        String str = this.rid;
        if (str != null) {
            i = str.hashCode();
        }
        int i2 = (((((hashCode + i) * 31) + this.ssrc) * 31) + this.rtxSsrc) * 31;
        boolean z2 = this.active;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        return ((((((i2 + i3) * 31) + this.maxBitrate) * 31) + this.quality) * 31) + this.maxPixelCount;
    }

    public String toString() {
        StringBuilder R = a.R("StreamParameters(type=");
        R.append(this.type);
        R.append(", rid=");
        R.append(this.rid);
        R.append(", ssrc=");
        R.append(this.ssrc);
        R.append(", rtxSsrc=");
        R.append(this.rtxSsrc);
        R.append(", active=");
        R.append(this.active);
        R.append(", maxBitrate=");
        R.append(this.maxBitrate);
        R.append(", quality=");
        R.append(this.quality);
        R.append(", maxPixelCount=");
        return a.A(R, this.maxPixelCount, ")");
    }
}
