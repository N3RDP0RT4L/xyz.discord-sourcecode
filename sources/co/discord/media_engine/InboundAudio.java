package co.discord.media_engine;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import java.util.Arrays;
import kotlin.Metadata;
/* compiled from: VoiceQuality.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u0006\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0013\b\u0082\b\u0018\u00002\u00020\u0001BM\u0012\u0006\u0010\u0016\u001a\u00020\u0002\u0012\u0006\u0010\u0017\u001a\u00020\u0002\u0012\u0006\u0010\u0018\u001a\u00020\u0006\u0012\u0006\u0010\u0019\u001a\u00020\u0006\u0012\u0006\u0010\u001a\u001a\u00020\n\u0012\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\n0\r\u0012\u0006\u0010\u001c\u001a\u00020\u0010\u0012\u0006\u0010\u001d\u001a\u00020\u0013¢\u0006\u0004\b6\u00107J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\t\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\t\u0010\bJ\u0010\u0010\u000b\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0016\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\n0\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015Jf\u0010\u001e\u001a\u00020\u00002\b\b\u0002\u0010\u0016\u001a\u00020\u00022\b\b\u0002\u0010\u0017\u001a\u00020\u00022\b\b\u0002\u0010\u0018\u001a\u00020\u00062\b\b\u0002\u0010\u0019\u001a\u00020\u00062\b\b\u0002\u0010\u001a\u001a\u00020\n2\u000e\b\u0002\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\n0\r2\b\b\u0002\u0010\u001c\u001a\u00020\u00102\b\b\u0002\u0010\u001d\u001a\u00020\u0013HÆ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010!\u001a\u00020 HÖ\u0001¢\u0006\u0004\b!\u0010\"J\u0010\u0010#\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b#\u0010\fJ\u001a\u0010&\u001a\u00020%2\b\u0010$\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b&\u0010'R\u0019\u0010\u001c\u001a\u00020\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010(\u001a\u0004\b)\u0010\u0012R\u0019\u0010\u001d\u001a\u00020\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010*\u001a\u0004\b+\u0010\u0015R\u001f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\n0\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010,\u001a\u0004\b-\u0010\u000fR\u0019\u0010\u0018\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010.\u001a\u0004\b/\u0010\bR\u0019\u0010\u001a\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00100\u001a\u0004\b1\u0010\fR\u0019\u0010\u0016\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u00102\u001a\u0004\b3\u0010\u0004R\u0019\u0010\u0019\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010.\u001a\u0004\b4\u0010\bR\u0019\u0010\u0017\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u00102\u001a\u0004\b5\u0010\u0004¨\u00068"}, d2 = {"Lco/discord/media_engine/InboundAudio;", "", "", "component1", "()J", "component2", "", "component3", "()D", "component4", "", "component5", "()I", "", "component6", "()[Ljava/lang/Integer;", "Lco/discord/media_engine/InboundBufferStats;", "component7", "()Lco/discord/media_engine/InboundBufferStats;", "Lco/discord/media_engine/InboundFrameOpStats;", "component8", "()Lco/discord/media_engine/InboundFrameOpStats;", "packetsReceived", "packetsLost", "mos", "mosSum", "mosCount", "mosBuckets", "bufferStats", "frameOpStats", "copy", "(JJDDI[Ljava/lang/Integer;Lco/discord/media_engine/InboundBufferStats;Lco/discord/media_engine/InboundFrameOpStats;)Lco/discord/media_engine/InboundAudio;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lco/discord/media_engine/InboundBufferStats;", "getBufferStats", "Lco/discord/media_engine/InboundFrameOpStats;", "getFrameOpStats", "[Ljava/lang/Integer;", "getMosBuckets", "D", "getMos", "I", "getMosCount", "J", "getPacketsReceived", "getMosSum", "getPacketsLost", HookHelper.constructorName, "(JJDDI[Ljava/lang/Integer;Lco/discord/media_engine/InboundBufferStats;Lco/discord/media_engine/InboundFrameOpStats;)V", "android_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class InboundAudio {
    private final InboundBufferStats bufferStats;
    private final InboundFrameOpStats frameOpStats;
    private final double mos;
    private final Integer[] mosBuckets;
    private final int mosCount;
    private final double mosSum;
    private final long packetsLost;
    private final long packetsReceived;

    public InboundAudio(long j, long j2, double d, double d2, int i, Integer[] numArr, InboundBufferStats inboundBufferStats, InboundFrameOpStats inboundFrameOpStats) {
        m.checkNotNullParameter(numArr, "mosBuckets");
        m.checkNotNullParameter(inboundBufferStats, "bufferStats");
        m.checkNotNullParameter(inboundFrameOpStats, "frameOpStats");
        this.packetsReceived = j;
        this.packetsLost = j2;
        this.mos = d;
        this.mosSum = d2;
        this.mosCount = i;
        this.mosBuckets = numArr;
        this.bufferStats = inboundBufferStats;
        this.frameOpStats = inboundFrameOpStats;
    }

    public final long component1() {
        return this.packetsReceived;
    }

    public final long component2() {
        return this.packetsLost;
    }

    public final double component3() {
        return this.mos;
    }

    public final double component4() {
        return this.mosSum;
    }

    public final int component5() {
        return this.mosCount;
    }

    public final Integer[] component6() {
        return this.mosBuckets;
    }

    public final InboundBufferStats component7() {
        return this.bufferStats;
    }

    public final InboundFrameOpStats component8() {
        return this.frameOpStats;
    }

    public final InboundAudio copy(long j, long j2, double d, double d2, int i, Integer[] numArr, InboundBufferStats inboundBufferStats, InboundFrameOpStats inboundFrameOpStats) {
        m.checkNotNullParameter(numArr, "mosBuckets");
        m.checkNotNullParameter(inboundBufferStats, "bufferStats");
        m.checkNotNullParameter(inboundFrameOpStats, "frameOpStats");
        return new InboundAudio(j, j2, d, d2, i, numArr, inboundBufferStats, inboundFrameOpStats);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof InboundAudio)) {
            return false;
        }
        InboundAudio inboundAudio = (InboundAudio) obj;
        return this.packetsReceived == inboundAudio.packetsReceived && this.packetsLost == inboundAudio.packetsLost && Double.compare(this.mos, inboundAudio.mos) == 0 && Double.compare(this.mosSum, inboundAudio.mosSum) == 0 && this.mosCount == inboundAudio.mosCount && m.areEqual(this.mosBuckets, inboundAudio.mosBuckets) && m.areEqual(this.bufferStats, inboundAudio.bufferStats) && m.areEqual(this.frameOpStats, inboundAudio.frameOpStats);
    }

    public final InboundBufferStats getBufferStats() {
        return this.bufferStats;
    }

    public final InboundFrameOpStats getFrameOpStats() {
        return this.frameOpStats;
    }

    public final double getMos() {
        return this.mos;
    }

    public final Integer[] getMosBuckets() {
        return this.mosBuckets;
    }

    public final int getMosCount() {
        return this.mosCount;
    }

    public final double getMosSum() {
        return this.mosSum;
    }

    public final long getPacketsLost() {
        return this.packetsLost;
    }

    public final long getPacketsReceived() {
        return this.packetsReceived;
    }

    public int hashCode() {
        int a = b.a(this.packetsLost);
        int doubleToLongBits = (((Double.doubleToLongBits(this.mosSum) + ((Double.doubleToLongBits(this.mos) + ((a + (b.a(this.packetsReceived) * 31)) * 31)) * 31)) * 31) + this.mosCount) * 31;
        Integer[] numArr = this.mosBuckets;
        int i = 0;
        int hashCode = (doubleToLongBits + (numArr != null ? Arrays.hashCode(numArr) : 0)) * 31;
        InboundBufferStats inboundBufferStats = this.bufferStats;
        int hashCode2 = (hashCode + (inboundBufferStats != null ? inboundBufferStats.hashCode() : 0)) * 31;
        InboundFrameOpStats inboundFrameOpStats = this.frameOpStats;
        if (inboundFrameOpStats != null) {
            i = inboundFrameOpStats.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("InboundAudio(packetsReceived=");
        R.append(this.packetsReceived);
        R.append(", packetsLost=");
        R.append(this.packetsLost);
        R.append(", mos=");
        R.append(this.mos);
        R.append(", mosSum=");
        R.append(this.mosSum);
        R.append(", mosCount=");
        R.append(this.mosCount);
        R.append(", mosBuckets=");
        R.append(Arrays.toString(this.mosBuckets));
        R.append(", bufferStats=");
        R.append(this.bufferStats);
        R.append(", frameOpStats=");
        R.append(this.frameOpStats);
        R.append(")");
        return R.toString();
    }
}
