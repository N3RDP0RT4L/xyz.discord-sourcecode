package co.discord.media_engine;

import andhook.lib.HookHelper;
import android.media.AudioRecord;
import android.os.Process;
import android.util.Log;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.nio.ByteBuffer;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.webrtc.ThreadUtils;
import org.webrtc.TimestampAligner;
/* compiled from: SoundshareAudioSource.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u0000 62\u00020\u0001:\u000276B\u0007¢\u0006\u0004\b5\u0010+J\u0017\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u001f\u0010\n\u001a\u00020\t2\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\n\u0010\u000bJ\"\u0010\u000f\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u00072\b\u0010\u000e\u001a\u0004\u0018\u00010\rH\u0082 ¢\u0006\u0004\b\u000f\u0010\u0010J(\u0010\u0011\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u00072\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010\b\u001a\u00020\u0007H\u0082 ¢\u0006\u0004\b\u0011\u0010\u0012J0\u0010\u0015\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u00072\u0006\u0010\u0013\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0002H\u0082 ¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\u0007H\u0082 ¢\u0006\u0004\b\u0017\u0010\u0018J\u0018\u0010\u0019\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u0007H\u0082 ¢\u0006\u0004\b\u0019\u0010\u001aJ\u0017\u0010\u001d\u001a\u00020\t2\u0006\u0010\u001c\u001a\u00020\u001bH\u0002¢\u0006\u0004\b\u001d\u0010\u001eJ\u0017\u0010\u001f\u001a\u00020\t2\u0006\u0010\u001c\u001a\u00020\u001bH\u0002¢\u0006\u0004\b\u001f\u0010\u001eJ\u0017\u0010 \u001a\u00020\t2\u0006\u0010\u001c\u001a\u00020\u001bH\u0002¢\u0006\u0004\b \u0010\u001eJ\u0015\u0010$\u001a\u00020#2\u0006\u0010\"\u001a\u00020!¢\u0006\u0004\b$\u0010%J\r\u0010&\u001a\u00020#¢\u0006\u0004\b&\u0010'J%\u0010(\u001a\u00020\t2\u0006\u0010\u0013\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b(\u0010)J\r\u0010*\u001a\u00020\t¢\u0006\u0004\b*\u0010+R\u001c\u0010-\u001a\b\u0018\u00010,R\u00020\u00008\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b-\u0010.R\u0019\u0010\f\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010/\u001a\u0004\b0\u0010\u0018R\u0018\u0010\u000e\u001a\u0004\u0018\u00010\r8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u000e\u00101R\u0016\u00102\u001a\u00020#8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b2\u00103R\u0018\u0010\"\u001a\u0004\u0018\u00010!8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\"\u00104¨\u00068"}, d2 = {"Lco/discord/media_engine/SoundshareAudioSource;", "", "", "channels", "channelCountToConfiguration", "(I)I", "bytes", "", "timestampNanos", "", "dataIsRecorded", "(IJ)V", "nativeInstance", "Ljava/nio/ByteBuffer;", "byteBuffer", "nativeCacheDirectBufferAddress", "(JLjava/nio/ByteBuffer;)V", "nativeDataIsRecorded", "(JIJ)V", "sampleRate", "bitDepth", "nativeSetSampleFormat", "(JIII)V", "nativeCreateInstance", "()J", "nativeDestroyInstance", "(J)V", "", "errorMessage", "reportSoundshareAudioSourceInitError", "(Ljava/lang/String;)V", "reportSoundshareAudioSourceStartError", "reportSoundshareAudioSourceError", "Landroid/media/AudioRecord;", "audioRecord", "", "startRecording", "(Landroid/media/AudioRecord;)Z", "stopRecording", "()Z", "setSampleFormat", "(III)V", "release", "()V", "Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;", "audioThread", "Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;", "J", "getNativeInstance", "Ljava/nio/ByteBuffer;", "released", "Z", "Landroid/media/AudioRecord;", HookHelper.constructorName, "Companion", "AudioRecordThread", "android_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class SoundshareAudioSource {
    private static final long AUDIO_RECORD_THREAD_JOIN_TIMEOUT_MS = 2000;
    private static final int BITS_PER_SAMPLE = 16;
    private static final int BUFFERS_PER_SECOND = 100;
    private static final int BUFFER_SIZE_FACTOR = 2;
    private static final int CALLBACK_BUFFER_SIZE_MS = 10;
    public static final Companion Companion = new Companion(null);
    private static final String TAG = "SoundshareAudioSource";
    private static volatile boolean microphoneMute;
    private AudioRecord audioRecord;
    private AudioRecordThread audioThread;
    private ByteBuffer byteBuffer;
    private final long nativeInstance = nativeCreateInstance();
    private boolean released;

    /* compiled from: SoundshareAudioSource.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u0012\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0082\u0004\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\u0016\u001a\u00020\u0015\u0012\u0006\u0010\r\u001a\u00020\f\u0012\u0006\u0010\u0010\u001a\u00020\u000f\u0012\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\u0017\u0010\u0018J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\r\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00068\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0016\u0010\u0010\u001a\u00020\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014¨\u0006\u0019"}, d2 = {"Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;", "Ljava/lang/Thread;", "", "run", "()V", "stopThread", "", "timestamp", "J", "", "emptyBytes", "[B", "Landroid/media/AudioRecord;", "audioRecord", "Landroid/media/AudioRecord;", "Ljava/nio/ByteBuffer;", "byteBuffer", "Ljava/nio/ByteBuffer;", "", "keepAlive", "Z", "", ModelAuditLogEntry.CHANGE_KEY_NAME, HookHelper.constructorName, "(Lco/discord/media_engine/SoundshareAudioSource;Ljava/lang/String;Landroid/media/AudioRecord;Ljava/nio/ByteBuffer;J)V", "android_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final class AudioRecordThread extends Thread {
        private final AudioRecord audioRecord;
        private final ByteBuffer byteBuffer;
        private final byte[] emptyBytes;
        private volatile boolean keepAlive = true;
        public final /* synthetic */ SoundshareAudioSource this$0;
        private long timestamp;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AudioRecordThread(SoundshareAudioSource soundshareAudioSource, String str, AudioRecord audioRecord, ByteBuffer byteBuffer, long j) {
            super(str);
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkNotNullParameter(audioRecord, "audioRecord");
            m.checkNotNullParameter(byteBuffer, "byteBuffer");
            this.this$0 = soundshareAudioSource;
            this.audioRecord = audioRecord;
            this.byteBuffer = byteBuffer;
            this.timestamp = j;
            this.emptyBytes = new byte[byteBuffer.capacity()];
        }

        @Override // java.lang.Thread, java.lang.Runnable
        public void run() {
            Process.setThreadPriority(-19);
            SoundshareAudioSource.Companion.assertTrue(this.audioRecord.getRecordingState() == 3);
            while (this.keepAlive) {
                AudioRecord audioRecord = this.audioRecord;
                ByteBuffer byteBuffer = this.byteBuffer;
                int read = audioRecord.read(byteBuffer, byteBuffer.capacity());
                this.timestamp = TimestampAligner.getRtcTimeNanos();
                if (read == this.byteBuffer.capacity()) {
                    if (SoundshareAudioSource.microphoneMute) {
                        this.byteBuffer.clear();
                        this.byteBuffer.put(this.emptyBytes);
                    }
                    if (this.keepAlive) {
                        this.this$0.dataIsRecorded(read, this.timestamp);
                    }
                } else {
                    String str = "AudioRecord.read failed: " + read;
                    Log.e(SoundshareAudioSource.TAG, str);
                    if (read == -3) {
                        this.keepAlive = false;
                        this.this$0.reportSoundshareAudioSourceError(str);
                    }
                }
            }
            try {
                this.audioRecord.stop();
            } catch (IllegalStateException e) {
                StringBuilder R = a.R("AudioRecord.stop failed: ");
                R.append(e.getMessage());
                Log.e(SoundshareAudioSource.TAG, R.toString());
            }
        }

        public final void stopThread() {
            this.keepAlive = false;
        }
    }

    /* compiled from: SoundshareAudioSource.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0015\u0010\b\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\b\u0010\u0006R\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0016\u0010\u000f\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000f\u0010\u000eR\u0016\u0010\u0010\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0010\u0010\u000eR\u0016\u0010\u0011\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0011\u0010\u000eR\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0015\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016¨\u0006\u0019"}, d2 = {"Lco/discord/media_engine/SoundshareAudioSource$Companion;", "", "", "condition", "", "assertTrue", "(Z)V", ModelAuditLogEntry.CHANGE_KEY_MUTE, "setMicrophoneMute", "", "AUDIO_RECORD_THREAD_JOIN_TIMEOUT_MS", "J", "", "BITS_PER_SAMPLE", "I", "BUFFERS_PER_SECOND", "BUFFER_SIZE_FACTOR", "CALLBACK_BUFFER_SIZE_MS", "", "TAG", "Ljava/lang/String;", "microphoneMute", "Z", HookHelper.constructorName, "()V", "android_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final void assertTrue(boolean z2) {
            if (!z2) {
                throw new AssertionError("Expected condition to be true");
            }
        }

        public final void setMicrophoneMute(boolean z2) {
            Log.w(SoundshareAudioSource.TAG, "setMicrophoneMute(" + z2 + ')');
            SoundshareAudioSource.microphoneMute = z2;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    private final int channelCountToConfiguration(int i) {
        return i == 1 ? 16 : 12;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void dataIsRecorded(int i, long j) {
        if (!this.released) {
            nativeDataIsRecorded(this.nativeInstance, i, j);
        }
    }

    private final native synchronized void nativeCacheDirectBufferAddress(long j, ByteBuffer byteBuffer);

    private final native synchronized long nativeCreateInstance();

    private final native void nativeDataIsRecorded(long j, int i, long j2);

    private final native synchronized void nativeDestroyInstance(long j);

    private final native void nativeSetSampleFormat(long j, int i, int i2, int i3);

    /* JADX INFO: Access modifiers changed from: private */
    public final void reportSoundshareAudioSourceError(String str) {
        Log.e(TAG, "Run-time recording error: " + str);
    }

    private final void reportSoundshareAudioSourceInitError(String str) {
        Log.e(TAG, "Init recording error: " + str);
    }

    private final void reportSoundshareAudioSourceStartError(String str) {
        Log.e(TAG, "Start recording error: " + str);
    }

    public final long getNativeInstance() {
        return this.nativeInstance;
    }

    public final synchronized void release() {
        if (!this.released) {
            AudioRecord audioRecord = this.audioRecord;
            if (audioRecord != null) {
                audioRecord.release();
            }
            this.audioRecord = null;
            nativeDestroyInstance(this.nativeInstance);
            this.released = true;
        }
    }

    public final void setSampleFormat(int i, int i2, int i3) {
        nativeSetSampleFormat(this.nativeInstance, i, i2, i3);
    }

    public final boolean startRecording(AudioRecord audioRecord) {
        m.checkNotNullParameter(audioRecord, "audioRecord");
        int channelCount = audioRecord.getChannelCount();
        int sampleRate = audioRecord.getSampleRate();
        if (this.audioRecord != null) {
            reportSoundshareAudioSourceInitError("StartRecording called twice without StopRecording.");
            return false;
        }
        this.audioRecord = audioRecord;
        setSampleFormat(sampleRate, 16, channelCount);
        ByteBuffer allocateDirect = ByteBuffer.allocateDirect((sampleRate / 100) * channelCount * 2);
        this.byteBuffer = allocateDirect;
        nativeCacheDirectBufferAddress(this.nativeInstance, allocateDirect);
        int minBufferSize = AudioRecord.getMinBufferSize(sampleRate, channelCountToConfiguration(channelCount), 2);
        if (minBufferSize == -1 || minBufferSize == -2) {
            reportSoundshareAudioSourceInitError(a.p("AudioRecord.getMinBufferSize failed: ", minBufferSize));
            return false;
        }
        Math.max(minBufferSize * 2, allocateDirect.capacity());
        if (audioRecord.getState() != 1) {
            reportSoundshareAudioSourceInitError("Failed to create a new AudioRecord instance");
            release();
            return false;
        }
        try {
            Companion.assertTrue(this.audioThread == null);
            long rtcTimeNanos = TimestampAligner.getRtcTimeNanos();
            try {
                audioRecord.startRecording();
                if (audioRecord.getRecordingState() != 3) {
                    reportSoundshareAudioSourceStartError("AudioRecord.startRecording failed - incorrect state :" + audioRecord.getRecordingState());
                    return false;
                }
                m.checkNotNullExpressionValue(allocateDirect, "byteBuffer");
                AudioRecordThread audioRecordThread = new AudioRecordThread(this, "SoundshareThread", audioRecord, allocateDirect, rtcTimeNanos);
                this.audioThread = audioRecordThread;
                m.checkNotNull(audioRecordThread);
                audioRecordThread.start();
                return true;
            } catch (IllegalStateException e) {
                reportSoundshareAudioSourceStartError("AudioRecord.startRecording failed: " + e.getMessage());
                return false;
            }
        } catch (Throwable th) {
            Log.e(TAG, "SoundshareAudioSource.startRecording fail hard!", th);
            throw th;
        }
    }

    public final boolean stopRecording() {
        AudioRecordThread audioRecordThread = this.audioThread;
        if (audioRecordThread == null) {
            return false;
        }
        audioRecordThread.stopThread();
        if (!ThreadUtils.joinUninterruptibly(audioRecordThread, AUDIO_RECORD_THREAD_JOIN_TIMEOUT_MS)) {
            Log.e(TAG, "Join of SoundshareThread timed out");
        }
        this.audioThread = null;
        return true;
    }
}
