package co.discord.media_engine;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: Statistics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b6\b\u0086\b\u0018\u00002\u00020\u0001B\u0083\u0001\u0012\u0006\u0010\u001e\u001a\u00020\u0002\u0012\n\u0010\u001f\u001a\u00060\u0005j\u0002`\u0006\u0012\u0006\u0010 \u001a\u00020\t\u0012\u0006\u0010!\u001a\u00020\u0005\u0012\u0006\u0010\"\u001a\u00020\u0005\u0012\u0006\u0010#\u001a\u00020\u000e\u0012\u0006\u0010$\u001a\u00020\u0011\u0012\u0006\u0010%\u001a\u00020\u0011\u0012\u0006\u0010&\u001a\u00020\u0015\u0012\u0006\u0010'\u001a\u00020\u0005\u0012\u0006\u0010(\u001a\u00020\u0005\u0012\u0006\u0010)\u001a\u00020\u0015\u0012\u0006\u0010*\u001a\u00020\u0005\u0012\u0006\u0010+\u001a\u00020\u0015\u0012\u0006\u0010,\u001a\u00020\u0005¢\u0006\u0004\bI\u0010JJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0014\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\f\u0010\bJ\u0010\u0010\r\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\r\u0010\bJ\u0010\u0010\u000f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0013J\u0010\u0010\u0016\u001a\u00020\u0015HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0018\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0018\u0010\bJ\u0010\u0010\u0019\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0019\u0010\bJ\u0010\u0010\u001a\u001a\u00020\u0015HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u0017J\u0010\u0010\u001b\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u001b\u0010\bJ\u0010\u0010\u001c\u001a\u00020\u0015HÆ\u0003¢\u0006\u0004\b\u001c\u0010\u0017J\u0010\u0010\u001d\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u001d\u0010\bJª\u0001\u0010-\u001a\u00020\u00002\b\b\u0002\u0010\u001e\u001a\u00020\u00022\f\b\u0002\u0010\u001f\u001a\u00060\u0005j\u0002`\u00062\b\b\u0002\u0010 \u001a\u00020\t2\b\b\u0002\u0010!\u001a\u00020\u00052\b\b\u0002\u0010\"\u001a\u00020\u00052\b\b\u0002\u0010#\u001a\u00020\u000e2\b\b\u0002\u0010$\u001a\u00020\u00112\b\b\u0002\u0010%\u001a\u00020\u00112\b\b\u0002\u0010&\u001a\u00020\u00152\b\b\u0002\u0010'\u001a\u00020\u00052\b\b\u0002\u0010(\u001a\u00020\u00052\b\b\u0002\u0010)\u001a\u00020\u00152\b\b\u0002\u0010*\u001a\u00020\u00052\b\b\u0002\u0010+\u001a\u00020\u00152\b\b\u0002\u0010,\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b-\u0010.J\u0010\u0010/\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b/\u0010\u0004J\u0010\u00100\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b0\u0010\u0010J\u001a\u00102\u001a\u00020\u00152\b\u00101\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b2\u00103R\u0019\u0010*\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b*\u00104\u001a\u0004\b5\u0010\bR\u0019\u0010#\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b#\u00106\u001a\u0004\b7\u0010\u0010R\u0019\u0010%\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b%\u00108\u001a\u0004\b9\u0010\u0013R\u0019\u0010'\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b'\u00104\u001a\u0004\b:\u0010\bR\u0019\u0010,\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b,\u00104\u001a\u0004\b;\u0010\bR\u001d\u0010\u001f\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00104\u001a\u0004\b<\u0010\bR\u0019\u0010 \u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010=\u001a\u0004\b>\u0010\u000bR\u0019\u0010\"\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00104\u001a\u0004\b?\u0010\bR\u0019\u0010&\u001a\u00020\u00158\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010@\u001a\u0004\bA\u0010\u0017R\u0019\u0010\u001e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010B\u001a\u0004\bC\u0010\u0004R\u0019\u0010)\u001a\u00020\u00158\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010@\u001a\u0004\bD\u0010\u0017R\u0019\u0010+\u001a\u00020\u00158\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010@\u001a\u0004\bE\u0010\u0017R\u0019\u0010!\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b!\u00104\u001a\u0004\bF\u0010\bR\u0019\u0010$\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b$\u00108\u001a\u0004\bG\u0010\u0013R\u0019\u0010(\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b(\u00104\u001a\u0004\bH\u0010\b¨\u0006K"}, d2 = {"Lco/discord/media_engine/OutboundRtpAudio;", "", "", "component1", "()Ljava/lang/String;", "", "Lco/discord/media_engine/U32;", "component2", "()J", "Lco/discord/media_engine/StatsCodec;", "component3", "()Lco/discord/media_engine/StatsCodec;", "component4", "component5", "", "component6", "()I", "", "component7", "()F", "component8", "", "component9", "()Z", "component10", "component11", "component12", "component13", "component14", "component15", "type", "ssrc", "codec", "bytesSent", "packetsSent", "packetsLost", "fractionLost", "audioLevel", "audioDetected", "framesCaptured", "framesRendered", "noiseCancellerIsEnabled", "noiseCancellerProcessTime", "voiceActivityDetectorIsEnabled", "voiceActivityDetectorProcessTime", "copy", "(Ljava/lang/String;JLco/discord/media_engine/StatsCodec;JJIFFZJJZJZJ)Lco/discord/media_engine/OutboundRtpAudio;", "toString", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "J", "getNoiseCancellerProcessTime", "I", "getPacketsLost", "F", "getAudioLevel", "getFramesCaptured", "getVoiceActivityDetectorProcessTime", "getSsrc", "Lco/discord/media_engine/StatsCodec;", "getCodec", "getPacketsSent", "Z", "getAudioDetected", "Ljava/lang/String;", "getType", "getNoiseCancellerIsEnabled", "getVoiceActivityDetectorIsEnabled", "getBytesSent", "getFractionLost", "getFramesRendered", HookHelper.constructorName, "(Ljava/lang/String;JLco/discord/media_engine/StatsCodec;JJIFFZJJZJZJ)V", "android_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class OutboundRtpAudio {
    private final boolean audioDetected;
    private final float audioLevel;
    private final long bytesSent;
    private final StatsCodec codec;
    private final float fractionLost;
    private final long framesCaptured;
    private final long framesRendered;
    private final boolean noiseCancellerIsEnabled;
    private final long noiseCancellerProcessTime;
    private final int packetsLost;
    private final long packetsSent;
    private final long ssrc;
    private final String type;
    private final boolean voiceActivityDetectorIsEnabled;
    private final long voiceActivityDetectorProcessTime;

    public OutboundRtpAudio(String str, long j, StatsCodec statsCodec, long j2, long j3, int i, float f, float f2, boolean z2, long j4, long j5, boolean z3, long j6, boolean z4, long j7) {
        m.checkNotNullParameter(str, "type");
        m.checkNotNullParameter(statsCodec, "codec");
        this.type = str;
        this.ssrc = j;
        this.codec = statsCodec;
        this.bytesSent = j2;
        this.packetsSent = j3;
        this.packetsLost = i;
        this.fractionLost = f;
        this.audioLevel = f2;
        this.audioDetected = z2;
        this.framesCaptured = j4;
        this.framesRendered = j5;
        this.noiseCancellerIsEnabled = z3;
        this.noiseCancellerProcessTime = j6;
        this.voiceActivityDetectorIsEnabled = z4;
        this.voiceActivityDetectorProcessTime = j7;
    }

    public final String component1() {
        return this.type;
    }

    public final long component10() {
        return this.framesCaptured;
    }

    public final long component11() {
        return this.framesRendered;
    }

    public final boolean component12() {
        return this.noiseCancellerIsEnabled;
    }

    public final long component13() {
        return this.noiseCancellerProcessTime;
    }

    public final boolean component14() {
        return this.voiceActivityDetectorIsEnabled;
    }

    public final long component15() {
        return this.voiceActivityDetectorProcessTime;
    }

    public final long component2() {
        return this.ssrc;
    }

    public final StatsCodec component3() {
        return this.codec;
    }

    public final long component4() {
        return this.bytesSent;
    }

    public final long component5() {
        return this.packetsSent;
    }

    public final int component6() {
        return this.packetsLost;
    }

    public final float component7() {
        return this.fractionLost;
    }

    public final float component8() {
        return this.audioLevel;
    }

    public final boolean component9() {
        return this.audioDetected;
    }

    public final OutboundRtpAudio copy(String str, long j, StatsCodec statsCodec, long j2, long j3, int i, float f, float f2, boolean z2, long j4, long j5, boolean z3, long j6, boolean z4, long j7) {
        m.checkNotNullParameter(str, "type");
        m.checkNotNullParameter(statsCodec, "codec");
        return new OutboundRtpAudio(str, j, statsCodec, j2, j3, i, f, f2, z2, j4, j5, z3, j6, z4, j7);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof OutboundRtpAudio)) {
            return false;
        }
        OutboundRtpAudio outboundRtpAudio = (OutboundRtpAudio) obj;
        return m.areEqual(this.type, outboundRtpAudio.type) && this.ssrc == outboundRtpAudio.ssrc && m.areEqual(this.codec, outboundRtpAudio.codec) && this.bytesSent == outboundRtpAudio.bytesSent && this.packetsSent == outboundRtpAudio.packetsSent && this.packetsLost == outboundRtpAudio.packetsLost && Float.compare(this.fractionLost, outboundRtpAudio.fractionLost) == 0 && Float.compare(this.audioLevel, outboundRtpAudio.audioLevel) == 0 && this.audioDetected == outboundRtpAudio.audioDetected && this.framesCaptured == outboundRtpAudio.framesCaptured && this.framesRendered == outboundRtpAudio.framesRendered && this.noiseCancellerIsEnabled == outboundRtpAudio.noiseCancellerIsEnabled && this.noiseCancellerProcessTime == outboundRtpAudio.noiseCancellerProcessTime && this.voiceActivityDetectorIsEnabled == outboundRtpAudio.voiceActivityDetectorIsEnabled && this.voiceActivityDetectorProcessTime == outboundRtpAudio.voiceActivityDetectorProcessTime;
    }

    public final boolean getAudioDetected() {
        return this.audioDetected;
    }

    public final float getAudioLevel() {
        return this.audioLevel;
    }

    public final long getBytesSent() {
        return this.bytesSent;
    }

    public final StatsCodec getCodec() {
        return this.codec;
    }

    public final float getFractionLost() {
        return this.fractionLost;
    }

    public final long getFramesCaptured() {
        return this.framesCaptured;
    }

    public final long getFramesRendered() {
        return this.framesRendered;
    }

    public final boolean getNoiseCancellerIsEnabled() {
        return this.noiseCancellerIsEnabled;
    }

    public final long getNoiseCancellerProcessTime() {
        return this.noiseCancellerProcessTime;
    }

    public final int getPacketsLost() {
        return this.packetsLost;
    }

    public final long getPacketsSent() {
        return this.packetsSent;
    }

    public final long getSsrc() {
        return this.ssrc;
    }

    public final String getType() {
        return this.type;
    }

    public final boolean getVoiceActivityDetectorIsEnabled() {
        return this.voiceActivityDetectorIsEnabled;
    }

    public final long getVoiceActivityDetectorProcessTime() {
        return this.voiceActivityDetectorProcessTime;
    }

    public int hashCode() {
        String str = this.type;
        int i = 0;
        int a = (b.a(this.ssrc) + ((str != null ? str.hashCode() : 0) * 31)) * 31;
        StatsCodec statsCodec = this.codec;
        if (statsCodec != null) {
            i = statsCodec.hashCode();
        }
        int a2 = b.a(this.bytesSent);
        int a3 = b.a(this.packetsSent);
        int floatToIntBits = (Float.floatToIntBits(this.audioLevel) + ((Float.floatToIntBits(this.fractionLost) + ((((a3 + ((a2 + ((a + i) * 31)) * 31)) * 31) + this.packetsLost) * 31)) * 31)) * 31;
        boolean z2 = this.audioDetected;
        int i2 = 1;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        int a4 = (b.a(this.framesRendered) + ((b.a(this.framesCaptured) + ((floatToIntBits + i3) * 31)) * 31)) * 31;
        boolean z3 = this.noiseCancellerIsEnabled;
        if (z3) {
            z3 = true;
        }
        int i5 = z3 ? 1 : 0;
        int i6 = z3 ? 1 : 0;
        int a5 = (b.a(this.noiseCancellerProcessTime) + ((a4 + i5) * 31)) * 31;
        boolean z4 = this.voiceActivityDetectorIsEnabled;
        if (!z4) {
            i2 = z4 ? 1 : 0;
        }
        return b.a(this.voiceActivityDetectorProcessTime) + ((a5 + i2) * 31);
    }

    public String toString() {
        StringBuilder R = a.R("OutboundRtpAudio(type=");
        R.append(this.type);
        R.append(", ssrc=");
        R.append(this.ssrc);
        R.append(", codec=");
        R.append(this.codec);
        R.append(", bytesSent=");
        R.append(this.bytesSent);
        R.append(", packetsSent=");
        R.append(this.packetsSent);
        R.append(", packetsLost=");
        R.append(this.packetsLost);
        R.append(", fractionLost=");
        R.append(this.fractionLost);
        R.append(", audioLevel=");
        R.append(this.audioLevel);
        R.append(", audioDetected=");
        R.append(this.audioDetected);
        R.append(", framesCaptured=");
        R.append(this.framesCaptured);
        R.append(", framesRendered=");
        R.append(this.framesRendered);
        R.append(", noiseCancellerIsEnabled=");
        R.append(this.noiseCancellerIsEnabled);
        R.append(", noiseCancellerProcessTime=");
        R.append(this.noiseCancellerProcessTime);
        R.append(", voiceActivityDetectorIsEnabled=");
        R.append(this.voiceActivityDetectorIsEnabled);
        R.append(", voiceActivityDetectorProcessTime=");
        return a.B(R, this.voiceActivityDetectorProcessTime, ")");
    }
}
