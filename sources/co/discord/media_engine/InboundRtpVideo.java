package co.discord.media_engine;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: Statistics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0010\u0006\n\u0002\b!\n\u0002\u0010\u000b\n\u0002\b%\b\u0086\b\u0018\u00002\u00020\u0001BÝ\u0001\u0012\u0006\u0010)\u001a\u00020\u0002\u0012\n\u0010*\u001a\u00060\u0005j\u0002`\u0006\u0012\b\u0010+\u001a\u0004\u0018\u00010\t\u0012\u0006\u0010,\u001a\u00020\u0005\u0012\u0006\u0010-\u001a\u00020\r\u0012\u0006\u0010.\u001a\u00020\r\u0012\u0006\u0010/\u001a\u00020\r\u0012\u0006\u00100\u001a\u00020\r\u0012\u0006\u00101\u001a\u00020\u0013\u0012\u0006\u00102\u001a\u00020\u0002\u0012\u0006\u00103\u001a\u00020\r\u0012\u0006\u00104\u001a\u00020\r\u0012\u0006\u00105\u001a\u00020\r\u0012\u0006\u00106\u001a\u00020\r\u0012\u0006\u00107\u001a\u00020\r\u0012\u0006\u00108\u001a\u00020\r\u0012\u0006\u00109\u001a\u00020\u0005\u0012\u0006\u0010:\u001a\u00020\u0005\u0012\u0006\u0010;\u001a\u00020\u0005\u0012\u0006\u0010<\u001a\u00020\u0005\u0012\u0006\u0010=\u001a\u00020\u0005\u0012\u0006\u0010>\u001a\u00020\u0005\u0012\u0006\u0010?\u001a\u00020\u0005\u0012\u0006\u0010@\u001a\u00020\u0005\u0012\u0006\u0010A\u001a\u00020\u0005\u0012\u0006\u0010B\u001a\u00020&¢\u0006\u0004\bk\u0010lJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0014\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\f\u0010\bJ\u0010\u0010\u000e\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u0011\u0010\u000fJ\u0010\u0010\u0012\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u0012\u0010\u000fJ\u0010\u0010\u0014\u001a\u00020\u0013HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0004J\u0010\u0010\u0017\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u0017\u0010\u000fJ\u0010\u0010\u0018\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u0018\u0010\u000fJ\u0010\u0010\u0019\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u0019\u0010\u000fJ\u0010\u0010\u001a\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u001a\u0010\u000fJ\u0010\u0010\u001b\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u001b\u0010\u000fJ\u0010\u0010\u001c\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u001c\u0010\u000fJ\u0010\u0010\u001d\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u001d\u0010\bJ\u0010\u0010\u001e\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u001e\u0010\bJ\u0010\u0010\u001f\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u001f\u0010\bJ\u0010\u0010 \u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b \u0010\bJ\u0010\u0010!\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b!\u0010\bJ\u0010\u0010\"\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\"\u0010\bJ\u0010\u0010#\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b#\u0010\bJ\u0010\u0010$\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b$\u0010\bJ\u0010\u0010%\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b%\u0010\bJ\u0010\u0010'\u001a\u00020&HÆ\u0003¢\u0006\u0004\b'\u0010(J\u009a\u0002\u0010C\u001a\u00020\u00002\b\b\u0002\u0010)\u001a\u00020\u00022\f\b\u0002\u0010*\u001a\u00060\u0005j\u0002`\u00062\n\b\u0002\u0010+\u001a\u0004\u0018\u00010\t2\b\b\u0002\u0010,\u001a\u00020\u00052\b\b\u0002\u0010-\u001a\u00020\r2\b\b\u0002\u0010.\u001a\u00020\r2\b\b\u0002\u0010/\u001a\u00020\r2\b\b\u0002\u00100\u001a\u00020\r2\b\b\u0002\u00101\u001a\u00020\u00132\b\b\u0002\u00102\u001a\u00020\u00022\b\b\u0002\u00103\u001a\u00020\r2\b\b\u0002\u00104\u001a\u00020\r2\b\b\u0002\u00105\u001a\u00020\r2\b\b\u0002\u00106\u001a\u00020\r2\b\b\u0002\u00107\u001a\u00020\r2\b\b\u0002\u00108\u001a\u00020\r2\b\b\u0002\u00109\u001a\u00020\u00052\b\b\u0002\u0010:\u001a\u00020\u00052\b\b\u0002\u0010;\u001a\u00020\u00052\b\b\u0002\u0010<\u001a\u00020\u00052\b\b\u0002\u0010=\u001a\u00020\u00052\b\b\u0002\u0010>\u001a\u00020\u00052\b\b\u0002\u0010?\u001a\u00020\u00052\b\b\u0002\u0010@\u001a\u00020\u00052\b\b\u0002\u0010A\u001a\u00020\u00052\b\b\u0002\u0010B\u001a\u00020&HÆ\u0001¢\u0006\u0004\bC\u0010DJ\u0010\u0010E\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\bE\u0010\u0004J\u0010\u0010F\u001a\u00020\rHÖ\u0001¢\u0006\u0004\bF\u0010\u000fJ\u001a\u0010I\u001a\u00020H2\b\u0010G\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\bI\u0010JR\u0019\u00104\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010K\u001a\u0004\bL\u0010\u000fR\u0019\u00106\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010K\u001a\u0004\bM\u0010\u000fR\u0019\u00105\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010K\u001a\u0004\bN\u0010\u000fR\u0019\u0010.\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010K\u001a\u0004\bO\u0010\u000fR\u001d\u0010*\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010P\u001a\u0004\bQ\u0010\bR\u0019\u00101\u001a\u00020\u00138\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010R\u001a\u0004\bS\u0010\u0015R\u0019\u00103\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010K\u001a\u0004\bT\u0010\u000fR\u0019\u0010=\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b=\u0010P\u001a\u0004\bU\u0010\bR\u0019\u0010,\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010P\u001a\u0004\bV\u0010\bR\u0019\u00107\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010K\u001a\u0004\bW\u0010\u000fR\u0019\u0010>\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b>\u0010P\u001a\u0004\bX\u0010\bR\u0019\u0010@\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b@\u0010P\u001a\u0004\bY\u0010\bR\u0019\u0010<\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b<\u0010P\u001a\u0004\bZ\u0010\bR\u0019\u0010-\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010K\u001a\u0004\b[\u0010\u000fR\u0019\u0010?\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010P\u001a\u0004\b\\\u0010\bR\u0019\u0010A\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\bA\u0010P\u001a\u0004\b]\u0010\bR\u0019\u0010)\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010^\u001a\u0004\b_\u0010\u0004R\u0019\u00100\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010K\u001a\u0004\b`\u0010\u000fR\u001b\u0010+\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010a\u001a\u0004\bb\u0010\u000bR\u0019\u00102\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010^\u001a\u0004\bc\u0010\u0004R\u0019\u00108\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010K\u001a\u0004\bd\u0010\u000fR\u0019\u0010;\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010P\u001a\u0004\be\u0010\bR\u0019\u00109\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010P\u001a\u0004\bf\u0010\bR\u0019\u0010:\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010P\u001a\u0004\bg\u0010\bR\u0019\u0010B\u001a\u00020&8\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010h\u001a\u0004\bi\u0010(R\u0019\u0010/\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010K\u001a\u0004\bj\u0010\u000f¨\u0006m"}, d2 = {"Lco/discord/media_engine/InboundRtpVideo;", "", "", "component1", "()Ljava/lang/String;", "", "Lco/discord/media_engine/U32;", "component2", "()J", "Lco/discord/media_engine/StatsCodec;", "component3", "()Lco/discord/media_engine/StatsCodec;", "component4", "", "component5", "()I", "component6", "component7", "component8", "Lco/discord/media_engine/Resolution;", "component9", "()Lco/discord/media_engine/Resolution;", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component19", "component20", "component21", "component22", "component23", "component24", "component25", "", "component26", "()D", "type", "ssrc", "codec", "bytesReceived", "packetsReceived", "packetsLost", ModelAuditLogEntry.CHANGE_KEY_BITRATE, "averageDecodeTime", "resolution", "decoderImplementationName", "framesDecoded", "framesDropped", "framesReceived", "frameRateDecode", "frameRateNetwork", "frameRateRender", "firCount", "nackCount", "pliCount", "qpSum", "freezeCount", "pauseCount", "totalFreezesDuration", "totalPausesDuration", "totalFramesDuration", "sumOfSquaredFramesDurations", "copy", "(Ljava/lang/String;JLco/discord/media_engine/StatsCodec;JIIIILco/discord/media_engine/Resolution;Ljava/lang/String;IIIIIIJJJJJJJJJD)Lco/discord/media_engine/InboundRtpVideo;", "toString", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getFramesDropped", "getFrameRateDecode", "getFramesReceived", "getPacketsLost", "J", "getSsrc", "Lco/discord/media_engine/Resolution;", "getResolution", "getFramesDecoded", "getFreezeCount", "getBytesReceived", "getFrameRateNetwork", "getPauseCount", "getTotalPausesDuration", "getQpSum", "getPacketsReceived", "getTotalFreezesDuration", "getTotalFramesDuration", "Ljava/lang/String;", "getType", "getAverageDecodeTime", "Lco/discord/media_engine/StatsCodec;", "getCodec", "getDecoderImplementationName", "getFrameRateRender", "getPliCount", "getFirCount", "getNackCount", "D", "getSumOfSquaredFramesDurations", "getBitrate", HookHelper.constructorName, "(Ljava/lang/String;JLco/discord/media_engine/StatsCodec;JIIIILco/discord/media_engine/Resolution;Ljava/lang/String;IIIIIIJJJJJJJJJD)V", "android_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class InboundRtpVideo {
    private final int averageDecodeTime;
    private final int bitrate;
    private final long bytesReceived;
    private final StatsCodec codec;
    private final String decoderImplementationName;
    private final long firCount;
    private final int frameRateDecode;
    private final int frameRateNetwork;
    private final int frameRateRender;
    private final int framesDecoded;
    private final int framesDropped;
    private final int framesReceived;
    private final long freezeCount;
    private final long nackCount;
    private final int packetsLost;
    private final int packetsReceived;
    private final long pauseCount;
    private final long pliCount;
    private final long qpSum;
    private final Resolution resolution;
    private final long ssrc;
    private final double sumOfSquaredFramesDurations;
    private final long totalFramesDuration;
    private final long totalFreezesDuration;
    private final long totalPausesDuration;
    private final String type;

    public InboundRtpVideo(String str, long j, StatsCodec statsCodec, long j2, int i, int i2, int i3, int i4, Resolution resolution, String str2, int i5, int i6, int i7, int i8, int i9, int i10, long j3, long j4, long j5, long j6, long j7, long j8, long j9, long j10, long j11, double d) {
        m.checkNotNullParameter(str, "type");
        m.checkNotNullParameter(resolution, "resolution");
        m.checkNotNullParameter(str2, "decoderImplementationName");
        this.type = str;
        this.ssrc = j;
        this.codec = statsCodec;
        this.bytesReceived = j2;
        this.packetsReceived = i;
        this.packetsLost = i2;
        this.bitrate = i3;
        this.averageDecodeTime = i4;
        this.resolution = resolution;
        this.decoderImplementationName = str2;
        this.framesDecoded = i5;
        this.framesDropped = i6;
        this.framesReceived = i7;
        this.frameRateDecode = i8;
        this.frameRateNetwork = i9;
        this.frameRateRender = i10;
        this.firCount = j3;
        this.nackCount = j4;
        this.pliCount = j5;
        this.qpSum = j6;
        this.freezeCount = j7;
        this.pauseCount = j8;
        this.totalFreezesDuration = j9;
        this.totalPausesDuration = j10;
        this.totalFramesDuration = j11;
        this.sumOfSquaredFramesDurations = d;
    }

    public final String component1() {
        return this.type;
    }

    public final String component10() {
        return this.decoderImplementationName;
    }

    public final int component11() {
        return this.framesDecoded;
    }

    public final int component12() {
        return this.framesDropped;
    }

    public final int component13() {
        return this.framesReceived;
    }

    public final int component14() {
        return this.frameRateDecode;
    }

    public final int component15() {
        return this.frameRateNetwork;
    }

    public final int component16() {
        return this.frameRateRender;
    }

    public final long component17() {
        return this.firCount;
    }

    public final long component18() {
        return this.nackCount;
    }

    public final long component19() {
        return this.pliCount;
    }

    public final long component2() {
        return this.ssrc;
    }

    public final long component20() {
        return this.qpSum;
    }

    public final long component21() {
        return this.freezeCount;
    }

    public final long component22() {
        return this.pauseCount;
    }

    public final long component23() {
        return this.totalFreezesDuration;
    }

    public final long component24() {
        return this.totalPausesDuration;
    }

    public final long component25() {
        return this.totalFramesDuration;
    }

    public final double component26() {
        return this.sumOfSquaredFramesDurations;
    }

    public final StatsCodec component3() {
        return this.codec;
    }

    public final long component4() {
        return this.bytesReceived;
    }

    public final int component5() {
        return this.packetsReceived;
    }

    public final int component6() {
        return this.packetsLost;
    }

    public final int component7() {
        return this.bitrate;
    }

    public final int component8() {
        return this.averageDecodeTime;
    }

    public final Resolution component9() {
        return this.resolution;
    }

    public final InboundRtpVideo copy(String str, long j, StatsCodec statsCodec, long j2, int i, int i2, int i3, int i4, Resolution resolution, String str2, int i5, int i6, int i7, int i8, int i9, int i10, long j3, long j4, long j5, long j6, long j7, long j8, long j9, long j10, long j11, double d) {
        m.checkNotNullParameter(str, "type");
        m.checkNotNullParameter(resolution, "resolution");
        m.checkNotNullParameter(str2, "decoderImplementationName");
        return new InboundRtpVideo(str, j, statsCodec, j2, i, i2, i3, i4, resolution, str2, i5, i6, i7, i8, i9, i10, j3, j4, j5, j6, j7, j8, j9, j10, j11, d);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof InboundRtpVideo)) {
            return false;
        }
        InboundRtpVideo inboundRtpVideo = (InboundRtpVideo) obj;
        return m.areEqual(this.type, inboundRtpVideo.type) && this.ssrc == inboundRtpVideo.ssrc && m.areEqual(this.codec, inboundRtpVideo.codec) && this.bytesReceived == inboundRtpVideo.bytesReceived && this.packetsReceived == inboundRtpVideo.packetsReceived && this.packetsLost == inboundRtpVideo.packetsLost && this.bitrate == inboundRtpVideo.bitrate && this.averageDecodeTime == inboundRtpVideo.averageDecodeTime && m.areEqual(this.resolution, inboundRtpVideo.resolution) && m.areEqual(this.decoderImplementationName, inboundRtpVideo.decoderImplementationName) && this.framesDecoded == inboundRtpVideo.framesDecoded && this.framesDropped == inboundRtpVideo.framesDropped && this.framesReceived == inboundRtpVideo.framesReceived && this.frameRateDecode == inboundRtpVideo.frameRateDecode && this.frameRateNetwork == inboundRtpVideo.frameRateNetwork && this.frameRateRender == inboundRtpVideo.frameRateRender && this.firCount == inboundRtpVideo.firCount && this.nackCount == inboundRtpVideo.nackCount && this.pliCount == inboundRtpVideo.pliCount && this.qpSum == inboundRtpVideo.qpSum && this.freezeCount == inboundRtpVideo.freezeCount && this.pauseCount == inboundRtpVideo.pauseCount && this.totalFreezesDuration == inboundRtpVideo.totalFreezesDuration && this.totalPausesDuration == inboundRtpVideo.totalPausesDuration && this.totalFramesDuration == inboundRtpVideo.totalFramesDuration && Double.compare(this.sumOfSquaredFramesDurations, inboundRtpVideo.sumOfSquaredFramesDurations) == 0;
    }

    public final int getAverageDecodeTime() {
        return this.averageDecodeTime;
    }

    public final int getBitrate() {
        return this.bitrate;
    }

    public final long getBytesReceived() {
        return this.bytesReceived;
    }

    public final StatsCodec getCodec() {
        return this.codec;
    }

    public final String getDecoderImplementationName() {
        return this.decoderImplementationName;
    }

    public final long getFirCount() {
        return this.firCount;
    }

    public final int getFrameRateDecode() {
        return this.frameRateDecode;
    }

    public final int getFrameRateNetwork() {
        return this.frameRateNetwork;
    }

    public final int getFrameRateRender() {
        return this.frameRateRender;
    }

    public final int getFramesDecoded() {
        return this.framesDecoded;
    }

    public final int getFramesDropped() {
        return this.framesDropped;
    }

    public final int getFramesReceived() {
        return this.framesReceived;
    }

    public final long getFreezeCount() {
        return this.freezeCount;
    }

    public final long getNackCount() {
        return this.nackCount;
    }

    public final int getPacketsLost() {
        return this.packetsLost;
    }

    public final int getPacketsReceived() {
        return this.packetsReceived;
    }

    public final long getPauseCount() {
        return this.pauseCount;
    }

    public final long getPliCount() {
        return this.pliCount;
    }

    public final long getQpSum() {
        return this.qpSum;
    }

    public final Resolution getResolution() {
        return this.resolution;
    }

    public final long getSsrc() {
        return this.ssrc;
    }

    public final double getSumOfSquaredFramesDurations() {
        return this.sumOfSquaredFramesDurations;
    }

    public final long getTotalFramesDuration() {
        return this.totalFramesDuration;
    }

    public final long getTotalFreezesDuration() {
        return this.totalFreezesDuration;
    }

    public final long getTotalPausesDuration() {
        return this.totalPausesDuration;
    }

    public final String getType() {
        return this.type;
    }

    public int hashCode() {
        String str = this.type;
        int i = 0;
        int a = (b.a(this.ssrc) + ((str != null ? str.hashCode() : 0) * 31)) * 31;
        StatsCodec statsCodec = this.codec;
        int a2 = (((((((((b.a(this.bytesReceived) + ((a + (statsCodec != null ? statsCodec.hashCode() : 0)) * 31)) * 31) + this.packetsReceived) * 31) + this.packetsLost) * 31) + this.bitrate) * 31) + this.averageDecodeTime) * 31;
        Resolution resolution = this.resolution;
        int hashCode = (a2 + (resolution != null ? resolution.hashCode() : 0)) * 31;
        String str2 = this.decoderImplementationName;
        if (str2 != null) {
            i = str2.hashCode();
        }
        int a3 = b.a(this.firCount);
        int a4 = b.a(this.nackCount);
        int a5 = b.a(this.pliCount);
        int a6 = b.a(this.qpSum);
        int a7 = b.a(this.freezeCount);
        int a8 = b.a(this.pauseCount);
        int a9 = b.a(this.totalFreezesDuration);
        int a10 = b.a(this.totalPausesDuration);
        return Double.doubleToLongBits(this.sumOfSquaredFramesDurations) + ((b.a(this.totalFramesDuration) + ((a10 + ((a9 + ((a8 + ((a7 + ((a6 + ((a5 + ((a4 + ((a3 + ((((((((((((((hashCode + i) * 31) + this.framesDecoded) * 31) + this.framesDropped) * 31) + this.framesReceived) * 31) + this.frameRateDecode) * 31) + this.frameRateNetwork) * 31) + this.frameRateRender) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31);
    }

    public String toString() {
        StringBuilder R = a.R("InboundRtpVideo(type=");
        R.append(this.type);
        R.append(", ssrc=");
        R.append(this.ssrc);
        R.append(", codec=");
        R.append(this.codec);
        R.append(", bytesReceived=");
        R.append(this.bytesReceived);
        R.append(", packetsReceived=");
        R.append(this.packetsReceived);
        R.append(", packetsLost=");
        R.append(this.packetsLost);
        R.append(", bitrate=");
        R.append(this.bitrate);
        R.append(", averageDecodeTime=");
        R.append(this.averageDecodeTime);
        R.append(", resolution=");
        R.append(this.resolution);
        R.append(", decoderImplementationName=");
        R.append(this.decoderImplementationName);
        R.append(", framesDecoded=");
        R.append(this.framesDecoded);
        R.append(", framesDropped=");
        R.append(this.framesDropped);
        R.append(", framesReceived=");
        R.append(this.framesReceived);
        R.append(", frameRateDecode=");
        R.append(this.frameRateDecode);
        R.append(", frameRateNetwork=");
        R.append(this.frameRateNetwork);
        R.append(", frameRateRender=");
        R.append(this.frameRateRender);
        R.append(", firCount=");
        R.append(this.firCount);
        R.append(", nackCount=");
        R.append(this.nackCount);
        R.append(", pliCount=");
        R.append(this.pliCount);
        R.append(", qpSum=");
        R.append(this.qpSum);
        R.append(", freezeCount=");
        R.append(this.freezeCount);
        R.append(", pauseCount=");
        R.append(this.pauseCount);
        R.append(", totalFreezesDuration=");
        R.append(this.totalFreezesDuration);
        R.append(", totalPausesDuration=");
        R.append(this.totalPausesDuration);
        R.append(", totalFramesDuration=");
        R.append(this.totalFramesDuration);
        R.append(", sumOfSquaredFramesDurations=");
        R.append(this.sumOfSquaredFramesDurations);
        R.append(")");
        return R.toString();
    }
}
