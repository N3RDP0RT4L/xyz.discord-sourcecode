package co.discord.media_engine;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.adjust.sdk.Constants;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: VoiceQuality.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0011\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\r\b\u0082\b\u0018\u00002\u00020\u0001BM\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b&\u0010'J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0004J\u0012\u0010\b\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0004J\u0012\u0010\t\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0004J\u0012\u0010\n\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\n\u0010\u0004Jd\u0010\u0012\u001a\u00020\u00002\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0018\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u001a\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u001b\u0010\r\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001e\u001a\u0004\b\u001f\u0010\u0004R\u001b\u0010\u000b\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001e\u001a\u0004\b \u0010\u0004R\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001e\u001a\u0004\b!\u0010\u0004R\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001e\u001a\u0004\b\"\u0010\u0004R\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001e\u001a\u0004\b#\u0010\u0004R\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001e\u001a\u0004\b$\u0010\u0004R\u001b\u0010\f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001e\u001a\u0004\b%\u0010\u0004¨\u0006("}, d2 = {"Lco/discord/media_engine/InboundFrameOpStats;", "", "", "component1", "()Ljava/lang/Long;", "component2", "component3", "component4", "component5", "component6", "component7", NotificationCompat.GROUP_KEY_SILENT, Constants.NORMAL, "merged", "expanded", "accelerated", "preemptiveExpanded", "cng", "copy", "(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;)Lco/discord/media_engine/InboundFrameOpStats;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Long;", "getMerged", "getSilent", "getPreemptiveExpanded", "getExpanded", "getAccelerated", "getCng", "getNormal", HookHelper.constructorName, "(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;)V", "android_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class InboundFrameOpStats {
    private final Long accelerated;
    private final Long cng;
    private final Long expanded;
    private final Long merged;
    private final Long normal;
    private final Long preemptiveExpanded;
    private final Long silent;

    public InboundFrameOpStats(Long l, Long l2, Long l3, Long l4, Long l5, Long l6, Long l7) {
        this.silent = l;
        this.normal = l2;
        this.merged = l3;
        this.expanded = l4;
        this.accelerated = l5;
        this.preemptiveExpanded = l6;
        this.cng = l7;
    }

    public static /* synthetic */ InboundFrameOpStats copy$default(InboundFrameOpStats inboundFrameOpStats, Long l, Long l2, Long l3, Long l4, Long l5, Long l6, Long l7, int i, Object obj) {
        if ((i & 1) != 0) {
            l = inboundFrameOpStats.silent;
        }
        if ((i & 2) != 0) {
            l2 = inboundFrameOpStats.normal;
        }
        Long l8 = l2;
        if ((i & 4) != 0) {
            l3 = inboundFrameOpStats.merged;
        }
        Long l9 = l3;
        if ((i & 8) != 0) {
            l4 = inboundFrameOpStats.expanded;
        }
        Long l10 = l4;
        if ((i & 16) != 0) {
            l5 = inboundFrameOpStats.accelerated;
        }
        Long l11 = l5;
        if ((i & 32) != 0) {
            l6 = inboundFrameOpStats.preemptiveExpanded;
        }
        Long l12 = l6;
        if ((i & 64) != 0) {
            l7 = inboundFrameOpStats.cng;
        }
        return inboundFrameOpStats.copy(l, l8, l9, l10, l11, l12, l7);
    }

    public final Long component1() {
        return this.silent;
    }

    public final Long component2() {
        return this.normal;
    }

    public final Long component3() {
        return this.merged;
    }

    public final Long component4() {
        return this.expanded;
    }

    public final Long component5() {
        return this.accelerated;
    }

    public final Long component6() {
        return this.preemptiveExpanded;
    }

    public final Long component7() {
        return this.cng;
    }

    public final InboundFrameOpStats copy(Long l, Long l2, Long l3, Long l4, Long l5, Long l6, Long l7) {
        return new InboundFrameOpStats(l, l2, l3, l4, l5, l6, l7);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof InboundFrameOpStats)) {
            return false;
        }
        InboundFrameOpStats inboundFrameOpStats = (InboundFrameOpStats) obj;
        return m.areEqual(this.silent, inboundFrameOpStats.silent) && m.areEqual(this.normal, inboundFrameOpStats.normal) && m.areEqual(this.merged, inboundFrameOpStats.merged) && m.areEqual(this.expanded, inboundFrameOpStats.expanded) && m.areEqual(this.accelerated, inboundFrameOpStats.accelerated) && m.areEqual(this.preemptiveExpanded, inboundFrameOpStats.preemptiveExpanded) && m.areEqual(this.cng, inboundFrameOpStats.cng);
    }

    public final Long getAccelerated() {
        return this.accelerated;
    }

    public final Long getCng() {
        return this.cng;
    }

    public final Long getExpanded() {
        return this.expanded;
    }

    public final Long getMerged() {
        return this.merged;
    }

    public final Long getNormal() {
        return this.normal;
    }

    public final Long getPreemptiveExpanded() {
        return this.preemptiveExpanded;
    }

    public final Long getSilent() {
        return this.silent;
    }

    public int hashCode() {
        Long l = this.silent;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.normal;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.merged;
        int hashCode3 = (hashCode2 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.expanded;
        int hashCode4 = (hashCode3 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.accelerated;
        int hashCode5 = (hashCode4 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.preemptiveExpanded;
        int hashCode6 = (hashCode5 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.cng;
        if (l7 != null) {
            i = l7.hashCode();
        }
        return hashCode6 + i;
    }

    public String toString() {
        StringBuilder R = a.R("InboundFrameOpStats(silent=");
        R.append(this.silent);
        R.append(", normal=");
        R.append(this.normal);
        R.append(", merged=");
        R.append(this.merged);
        R.append(", expanded=");
        R.append(this.expanded);
        R.append(", accelerated=");
        R.append(this.accelerated);
        R.append(", preemptiveExpanded=");
        R.append(this.preemptiveExpanded);
        R.append(", cng=");
        return a.F(R, this.cng, ")");
    }
}
