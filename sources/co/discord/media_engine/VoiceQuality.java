package co.discord.media_engine;

import andhook.lib.HookHelper;
import com.google.android.material.shadow.ShadowDrawableWrapper;
import d0.z.d.m;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Metadata;
/* compiled from: VoiceQuality.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u0019\u0010\u001aJ!\u0010\u0006\u001a\u00020\u00052\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00010\u0002¢\u0006\u0004\b\u0006\u0010\u0007J!\u0010\b\u001a\u00020\u00052\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00010\u0002¢\u0006\u0004\b\b\u0010\u0007J!\u0010\t\u001a\u00020\u00052\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00010\u0002¢\u0006\u0004\b\t\u0010\u0007J!\u0010\n\u001a\u00020\u00052\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00010\u0002¢\u0006\u0004\b\n\u0010\u0007J!\u0010\u000b\u001a\u00020\u00052\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00010\u0002¢\u0006\u0004\b\u000b\u0010\u0007J\u0015\u0010\u000e\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0011\u001a\u00020\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0014\u001a\u00020\u00138\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\"\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00160\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018¨\u0006\u001b"}, d2 = {"Lco/discord/media_engine/VoiceQuality;", "", "", "", "result", "", "getMosStats", "(Ljava/util/Map;)V", "getPacketStats", "getBufferStats", "getFrameOpStats", "getDurationStats", "Lco/discord/media_engine/Stats;", "stats", "update", "(Lco/discord/media_engine/Stats;)V", "Lco/discord/media_engine/OutboundAudio;", "_outboundStats", "Lco/discord/media_engine/OutboundAudio;", "Lco/discord/media_engine/Duration;", "_duration", "Lco/discord/media_engine/Duration;", "Lco/discord/media_engine/InboundAudio;", "_inboundStats", "Ljava/util/Map;", HookHelper.constructorName, "()V", "android_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class VoiceQuality {
    private Map<String, InboundAudio> _inboundStats = new LinkedHashMap();
    private OutboundAudio _outboundStats = new OutboundAudio(0, 0);
    private Duration _duration = new Duration(0, 0, 0, 0);

    public final void getBufferStats(Map<String, Object> map) {
        PlayoutMetric audioJitterBuffer;
        m.checkNotNullParameter(map, "result");
        PlayoutMetric playoutMetric = null;
        InboundBufferStats inboundBufferStats = null;
        for (Map.Entry<String, InboundAudio> entry : this._inboundStats.entrySet()) {
            InboundAudio value = entry.getValue();
            if (inboundBufferStats == null) {
                inboundBufferStats = value.getBufferStats();
            } else {
                PlayoutMetric audioJitterBuffer2 = inboundBufferStats.getAudioJitterBuffer();
                if (!(audioJitterBuffer2 == null || (audioJitterBuffer = value.getBufferStats().getAudioJitterBuffer()) == null || audioJitterBuffer.getP75() <= audioJitterBuffer2.getP75())) {
                    inboundBufferStats = value.getBufferStats();
                }
            }
        }
        VoiceQualityKt.access$explodePlayoutMetric("audio_jitter_buffer", inboundBufferStats != null ? inboundBufferStats.getAudioJitterBuffer() : null, map);
        VoiceQualityKt.access$explodePlayoutMetric("audio_jitter_target", inboundBufferStats != null ? inboundBufferStats.getAudioJitterTarget() : null, map);
        VoiceQualityKt.access$explodePlayoutMetric("audio_jitter_delay", inboundBufferStats != null ? inboundBufferStats.getAudioJitterDelay() : null, map);
        VoiceQualityKt.access$explodePlayoutMetric("relative_reception_delay", inboundBufferStats != null ? inboundBufferStats.getRelativeReceptionDelay() : null, map);
        if (inboundBufferStats != null) {
            playoutMetric = inboundBufferStats.getRelativePlayoutDelay();
        }
        VoiceQualityKt.access$explodePlayoutMetric("relative_playout_delay", playoutMetric, map);
    }

    public final void getDurationStats(Map<String, Object> map) {
        m.checkNotNullParameter(map, "result");
        map.put("duration_listening", Integer.valueOf(this._duration.getListening()));
        map.put("duration_speaking", Integer.valueOf(this._duration.getSpeaking()));
        map.put("duration_participation", Integer.valueOf(this._duration.getParticipation()));
        map.put("duration_connected", Integer.valueOf(this._duration.getConnected()));
    }

    public final void getFrameOpStats(Map<String, Object> map) {
        m.checkNotNullParameter(map, "result");
        Long l = null;
        Long l2 = null;
        Long l3 = null;
        Long l4 = null;
        Long l5 = null;
        Long l6 = null;
        Long l7 = null;
        for (Map.Entry<String, InboundAudio> entry : this._inboundStats.entrySet()) {
            InboundAudio value = entry.getValue();
            Long silent = value.getFrameOpStats().getSilent();
            long j = 0;
            if (silent != null) {
                l = Long.valueOf((l != null ? l.longValue() : 0L) + silent.longValue());
            }
            Long normal = value.getFrameOpStats().getNormal();
            if (normal != null) {
                l2 = Long.valueOf((l2 != null ? l2.longValue() : 0L) + normal.longValue());
            }
            Long merged = value.getFrameOpStats().getMerged();
            if (merged != null) {
                l3 = Long.valueOf((l3 != null ? l3.longValue() : 0L) + merged.longValue());
            }
            Long expanded = value.getFrameOpStats().getExpanded();
            if (expanded != null) {
                l4 = Long.valueOf((l4 != null ? l4.longValue() : 0L) + expanded.longValue());
            }
            Long accelerated = value.getFrameOpStats().getAccelerated();
            if (accelerated != null) {
                l5 = Long.valueOf((l5 != null ? l5.longValue() : 0L) + accelerated.longValue());
            }
            Long preemptiveExpanded = value.getFrameOpStats().getPreemptiveExpanded();
            if (preemptiveExpanded != null) {
                l6 = Long.valueOf((l6 != null ? l6.longValue() : 0L) + preemptiveExpanded.longValue());
            }
            Long cng = value.getFrameOpStats().getCng();
            if (cng != null) {
                long longValue = cng.longValue();
                if (l7 != null) {
                    j = l7.longValue();
                }
                l7 = Long.valueOf(j + longValue);
            }
        }
        if (l != null) {
            map.put("frame_op_silent", Long.valueOf(l.longValue()));
        }
        if (l2 != null) {
            map.put("frame_op_normal", Long.valueOf(l2.longValue()));
        }
        if (l3 != null) {
            map.put("frame_op_merged", Long.valueOf(l3.longValue()));
        }
        if (l4 != null) {
            map.put("frame_op_expanded", Long.valueOf(l4.longValue()));
        }
        if (l5 != null) {
            map.put("frame_op_accelerated", Long.valueOf(l5.longValue()));
        }
        if (l6 != null) {
            map.put("frame_op_preemptive_expanded", Long.valueOf(l6.longValue()));
        }
        if (l7 != null) {
            map.put("frame_op_cng", Long.valueOf(l7.longValue()));
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final void getMosStats(Map<String, Object> map) {
        m.checkNotNullParameter(map, "result");
        Object[] objArr = new Integer[5];
        int i = 0;
        objArr[0] = 0;
        objArr[1] = 0;
        objArr[2] = 0;
        objArr[3] = 0;
        objArr[4] = 0;
        double d = ShadowDrawableWrapper.COS_45;
        int i2 = 0;
        for (Map.Entry<String, InboundAudio> entry : this._inboundStats.entrySet()) {
            InboundAudio value = entry.getValue();
            d += value.getMosSum();
            i2 += value.getMosCount();
            for (int i3 = 0; i3 <= 4; i3++) {
                objArr[i3] = Integer.valueOf(value.getMosBuckets()[i3].intValue() + objArr[i3].intValue());
            }
        }
        if (i2 > 0) {
            i = Double.valueOf(d / i2);
        }
        map.put("mos_mean", i);
        map.put("mos_1", objArr[1]);
        map.put("mos_2", objArr[2]);
        map.put("mos_3", objArr[3]);
        map.put("mos_4", objArr[4]);
    }

    public final void getPacketStats(Map<String, Object> map) {
        m.checkNotNullParameter(map, "result");
        long j = 0;
        long j2 = 0;
        for (Map.Entry<String, InboundAudio> entry : this._inboundStats.entrySet()) {
            InboundAudio value = entry.getValue();
            j += value.getPacketsReceived();
            j2 += value.getPacketsLost();
        }
        map.put("packets_sent", Long.valueOf(this._outboundStats.getPacketsSent()));
        map.put("packets_sent_lost", Integer.valueOf(this._outboundStats.getPacketsLost()));
        map.put("packets_received", Long.valueOf(j));
        map.put("packets_received_lost", Long.valueOf(j2));
    }

    public final void update(Stats stats) {
        OutboundAudio outboundAudio;
        boolean z2;
        int i;
        boolean z3;
        long j;
        Iterator<Map.Entry<String, InboundRtpAudio>> it;
        InboundAudio inboundAudio;
        m.checkNotNullParameter(stats, "stats");
        Duration duration = this._duration;
        duration.setConnected(duration.getConnected() + 1);
        long packetsSent = this._outboundStats.getPacketsSent();
        long j2 = 0;
        long j3 = 0;
        for (Map.Entry<String, InboundAudio> entry : this._inboundStats.entrySet()) {
            j3 += entry.getValue().getPacketsReceived();
        }
        OutboundRtpAudio outboundRtpAudio = stats.getOutboundRtpAudio();
        if (outboundRtpAudio != null) {
            outboundAudio = new OutboundAudio(outboundRtpAudio.getPacketsSent(), outboundRtpAudio.getPacketsLost());
        } else {
            outboundAudio = new OutboundAudio(0L, 0);
        }
        this._outboundStats = outboundAudio;
        Iterator<Map.Entry<String, InboundRtpAudio>> it2 = stats.getInboundRtpAudio().entrySet().iterator();
        while (it2.hasNext()) {
            Map.Entry<String, InboundRtpAudio> next = it2.next();
            String key = next.getKey();
            InboundRtpAudio value = next.getValue();
            Transport transport = stats.getTransport();
            int ping = transport != null ? transport.getPing() : 0;
            long packetsReceived = value.getPacketsReceived();
            long packetsLost = value.getPacketsLost();
            int jitterBuffer = (int) value.getJitterBuffer();
            InboundBufferStats inboundBufferStats = new InboundBufferStats(value.getAudioJitterBuffer(), value.getAudioJitterTarget(), value.getAudioJitterDelay(), value.getRelativeReceptionDelay(), value.getRelativePlayoutDelay());
            InboundFrameOpStats inboundFrameOpStats = new InboundFrameOpStats(value.getOpSilence(), value.getOpNormal(), value.getOpMerge(), value.getOpExpand(), value.getOpAccelerate(), value.getOpPreemptiveExpand(), value.getOpCNG());
            Map<String, InboundAudio> map = this._inboundStats;
            InboundAudio inboundAudio2 = map.get(key);
            if (inboundAudio2 != null) {
                long packetsReceived2 = packetsReceived - inboundAudio2.getPacketsReceived();
                long packetsLost2 = packetsLost - inboundAudio2.getPacketsLost();
                double d = ShadowDrawableWrapper.COS_45;
                Integer[] mosBuckets = inboundAudio2.getMosBuckets();
                j = 0;
                if (packetsReceived2 <= 0 || packetsLost2 < 0) {
                    it = it2;
                } else {
                    it = it2;
                    double access$_calculateMos = VoiceQualityKt.access$_calculateMos(ping + jitterBuffer, VoiceQualityKt.access$clamp(packetsLost2 / (packetsReceived2 + packetsLost2), ShadowDrawableWrapper.COS_45, 1.0d));
                    int floor = (int) Math.floor(access$_calculateMos);
                    mosBuckets[floor] = Integer.valueOf(mosBuckets[floor].intValue() + 1);
                    d = access$_calculateMos;
                }
                inboundAudio = new InboundAudio(packetsReceived, packetsLost, d, inboundAudio2.getMosSum() + d, inboundAudio2.getMosCount() + (d > ((double) 0) ? 1 : 0), mosBuckets, inboundBufferStats, inboundFrameOpStats);
            } else {
                it = it2;
                j = j2;
                inboundAudio = new InboundAudio(packetsReceived, packetsLost, ShadowDrawableWrapper.COS_45, ShadowDrawableWrapper.COS_45, 0, new Integer[]{0, 0, 0, 0, 0}, inboundBufferStats, inboundFrameOpStats);
            }
            map.put(key, inboundAudio);
            it2 = it;
            j2 = j;
        }
        long j4 = j2;
        if (this._outboundStats.getPacketsSent() > packetsSent) {
            Duration duration2 = this._duration;
            duration2.setSpeaking(duration2.getSpeaking() + 1);
            z2 = true;
        } else {
            z2 = false;
        }
        for (Map.Entry<String, InboundAudio> entry2 : this._inboundStats.entrySet()) {
            j4 += entry2.getValue().getPacketsReceived();
        }
        if (j4 > j3) {
            Duration duration3 = this._duration;
            i = 1;
            duration3.setListening(duration3.getListening() + 1);
            z3 = true;
        } else {
            i = 1;
            z3 = false;
        }
        if (z2 || z3) {
            Duration duration4 = this._duration;
            duration4.setParticipation(duration4.getParticipation() + i);
        }
    }
}
