package co.discord.media_engine;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.Map;
import java.util.Objects;
import kotlin.Metadata;
/* compiled from: Codecs.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010$\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u0011\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B;\u0012\u0006\u0010\r\u001a\u00020\u0002\u0012\u0006\u0010\u000e\u001a\u00020\u0005\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\u0006\u0010\u0010\u001a\u00020\u0002\u0012\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\n¢\u0006\u0004\b)\u0010*J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\t\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0004J\u001c\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJN\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\r\u001a\u00020\u00022\b\b\u0002\u0010\u000e\u001a\u00020\u00052\b\b\u0002\u0010\u000f\u001a\u00020\u00022\b\b\u0002\u0010\u0010\u001a\u00020\u00022\u0014\b\u0002\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\nHÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0007J\u0010\u0010\u0015\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0004J\u001a\u0010\u0018\u001a\u00020\u00172\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001a\u001a\u0004\b\u001b\u0010\u0004R\u0019\u0010\u000e\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001c\u001a\u0004\b\u001d\u0010\u0007R\u0019\u0010\u0010\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001a\u001a\u0004\b\u001e\u0010\u0004R\u001f\u0010 \u001a\b\u0012\u0004\u0012\u00020\u00050\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#R\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001a\u001a\u0004\b$\u0010\u0004R%\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010%\u001a\u0004\b&\u0010\fR\u001f\u0010'\u001a\b\u0012\u0004\u0012\u00020\u00050\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010!\u001a\u0004\b(\u0010#¨\u0006+"}, d2 = {"Lco/discord/media_engine/AudioDecoder;", "", "", "component1", "()I", "", "component2", "()Ljava/lang/String;", "component3", "component4", "", "component5", "()Ljava/util/Map;", "type", ModelAuditLogEntry.CHANGE_KEY_NAME, "freq", "channels", "params", "copy", "(ILjava/lang/String;IILjava/util/Map;)Lco/discord/media_engine/AudioDecoder;", "toString", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getType", "Ljava/lang/String;", "getName", "getChannels", "", "paramsKeys", "[Ljava/lang/String;", "getParamsKeys", "()[Ljava/lang/String;", "getFreq", "Ljava/util/Map;", "getParams", "paramsValues", "getParamsValues", HookHelper.constructorName, "(ILjava/lang/String;IILjava/util/Map;)V", "android_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AudioDecoder {
    private final int channels;
    private final int freq;
    private final String name;
    private final Map<String, String> params;
    private final String[] paramsKeys;
    private final String[] paramsValues;
    private final int type;

    public AudioDecoder(int i, String str, int i2, int i3, Map<String, String> map) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(map, "params");
        this.type = i;
        this.name = str;
        this.freq = i2;
        this.channels = i3;
        this.params = map;
        Object[] array = map.keySet().toArray(new String[0]);
        Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
        this.paramsKeys = (String[]) array;
        Object[] array2 = map.values().toArray(new String[0]);
        Objects.requireNonNull(array2, "null cannot be cast to non-null type kotlin.Array<T>");
        this.paramsValues = (String[]) array2;
    }

    public static /* synthetic */ AudioDecoder copy$default(AudioDecoder audioDecoder, int i, String str, int i2, int i3, Map map, int i4, Object obj) {
        if ((i4 & 1) != 0) {
            i = audioDecoder.type;
        }
        if ((i4 & 2) != 0) {
            str = audioDecoder.name;
        }
        String str2 = str;
        if ((i4 & 4) != 0) {
            i2 = audioDecoder.freq;
        }
        int i5 = i2;
        if ((i4 & 8) != 0) {
            i3 = audioDecoder.channels;
        }
        int i6 = i3;
        Map<String, String> map2 = map;
        if ((i4 & 16) != 0) {
            map2 = audioDecoder.params;
        }
        return audioDecoder.copy(i, str2, i5, i6, map2);
    }

    public final int component1() {
        return this.type;
    }

    public final String component2() {
        return this.name;
    }

    public final int component3() {
        return this.freq;
    }

    public final int component4() {
        return this.channels;
    }

    public final Map<String, String> component5() {
        return this.params;
    }

    public final AudioDecoder copy(int i, String str, int i2, int i3, Map<String, String> map) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(map, "params");
        return new AudioDecoder(i, str, i2, i3, map);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AudioDecoder)) {
            return false;
        }
        AudioDecoder audioDecoder = (AudioDecoder) obj;
        return this.type == audioDecoder.type && m.areEqual(this.name, audioDecoder.name) && this.freq == audioDecoder.freq && this.channels == audioDecoder.channels && m.areEqual(this.params, audioDecoder.params);
    }

    public final int getChannels() {
        return this.channels;
    }

    public final int getFreq() {
        return this.freq;
    }

    public final String getName() {
        return this.name;
    }

    public final Map<String, String> getParams() {
        return this.params;
    }

    public final String[] getParamsKeys() {
        return this.paramsKeys;
    }

    public final String[] getParamsValues() {
        return this.paramsValues;
    }

    public final int getType() {
        return this.type;
    }

    public int hashCode() {
        int i = this.type * 31;
        String str = this.name;
        int i2 = 0;
        int hashCode = (((((i + (str != null ? str.hashCode() : 0)) * 31) + this.freq) * 31) + this.channels) * 31;
        Map<String, String> map = this.params;
        if (map != null) {
            i2 = map.hashCode();
        }
        return hashCode + i2;
    }

    public String toString() {
        StringBuilder R = a.R("AudioDecoder(type=");
        R.append(this.type);
        R.append(", name=");
        R.append(this.name);
        R.append(", freq=");
        R.append(this.freq);
        R.append(", channels=");
        R.append(this.channels);
        R.append(", params=");
        return a.L(R, this.params, ")");
    }
}
