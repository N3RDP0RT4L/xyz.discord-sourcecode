package co.discord.media_engine;

import b.d.b.a.a;
import com.google.android.material.shadow.ShadowDrawableWrapper;
import java.util.Map;
import kotlin.Metadata;
/* compiled from: VoiceQuality.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\f\u001a5\u0010\b\u001a\u00020\u00072\u0006\u0010\u0001\u001a\u00020\u00002\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00050\u0004H\u0002¢\u0006\u0004\b\b\u0010\t\u001a\u001f\u0010\r\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\nH\u0002¢\u0006\u0004\b\r\u0010\u000e\u001a\u001f\u0010\u0010\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\nH\u0002¢\u0006\u0004\b\u0010\u0010\u000e\u001a'\u0010\u0014\u001a\u00020\n2\u0006\u0010\u0011\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\n2\u0006\u0010\u0013\u001a\u00020\nH\u0002¢\u0006\u0004\b\u0014\u0010\u0015¨\u0006\u0016"}, d2 = {"", "key", "Lco/discord/media_engine/PlayoutMetric;", "metric", "", "", "result", "", "explodePlayoutMetric", "(Ljava/lang/String;Lco/discord/media_engine/PlayoutMetric;Ljava/util/Map;)V", "", "rtt", "lossRate", "_calculateMos", "(DD)D", "delay", "_calculateR", "x", "min", "max", "clamp", "(DDD)D", "android_release"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class VoiceQualityKt {
    /* JADX INFO: Access modifiers changed from: private */
    public static final double _calculateMos(double d, double d2) {
        double _calculateR = _calculateR(d, d2);
        if (_calculateR < 0) {
            return 1.0d;
        }
        double d3 = 100;
        if (_calculateR > d3) {
            return 4.5d;
        }
        return ((d3 - _calculateR) * (_calculateR - 60) * 7.1E-6d * _calculateR) + (0.035d * _calculateR) + 1;
    }

    private static final double _calculateR(double d, double d2) {
        double d3 = (0.024d * d) + (d > 177.3d ? (d - 177.3d) * 0.11d : ShadowDrawableWrapper.COS_45);
        double d4 = 10;
        return (93.4d - d3) - (((122 * d2) / (d2 + d4)) + d4);
    }

    public static final /* synthetic */ double access$_calculateMos(double d, double d2) {
        return _calculateMos(d, d2);
    }

    public static final /* synthetic */ double access$clamp(double d, double d2, double d3) {
        return clamp(d, d2, d3);
    }

    public static final /* synthetic */ void access$explodePlayoutMetric(String str, PlayoutMetric playoutMetric, Map map) {
        explodePlayoutMetric(str, playoutMetric, map);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static final double clamp(double d, double d2, double d3) {
        return d < d2 ? d2 : d3 < d ? d3 : d;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static final void explodePlayoutMetric(String str, PlayoutMetric playoutMetric, Map<String, Object> map) {
        int i = 0;
        map.put(a.v(str, "_mean"), playoutMetric != null ? Double.valueOf(playoutMetric.getMean()) : 0);
        map.put(str + "_p75", playoutMetric != null ? Double.valueOf(playoutMetric.getP75()) : 0);
        map.put(str + "_p95", playoutMetric != null ? Double.valueOf(playoutMetric.getP95()) : 0);
        map.put(str + "_p99", playoutMetric != null ? Double.valueOf(playoutMetric.getP99()) : 0);
        String str2 = str + "_max";
        if (playoutMetric != null) {
            i = Double.valueOf(playoutMetric.getMax());
        }
        map.put(str2, i);
    }
}
