package co.discord.media_engine.internal;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: NativeStatistics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b1\n\u0002\u0010\u000b\n\u0002\b \b\u0080\b\u0018\u00002\u00020\u0001B£\u0002\u0012\n\u0010#\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010$\u001a\u00060\u0006j\u0002`\u0007\u0012\u0006\u0010%\u001a\u00020\n\u0012\n\u0010&\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010'\u001a\u00060\u0006j\u0002`\u000e\u0012\n\u0010(\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010)\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010*\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010+\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010,\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010-\u001a\u00060\u0006j\u0002`\u000e\u0012\n\u0010.\u001a\u00060\u0006j\u0002`\u000e\u0012\n\u0010/\u001a\u00060\u0006j\u0002`\u000e\u0012\n\u00100\u001a\u00060\u0006j\u0002`\u000e\u0012\n\u00101\u001a\u00060\u0006j\u0002`\u000e\u0012\n\u00102\u001a\u00060\u0006j\u0002`\u0007\u0012\n\u00103\u001a\u00060\u0006j\u0002`\u0007\u0012\n\u00104\u001a\u00060\u0006j\u0002`\u0007\u0012\n\u00105\u001a\u00060\u0006j\u0002`\u0007\u0012\n\u00106\u001a\u00060\u0006j\u0002`\u0007\u0012\n\u00107\u001a\u00060\u0006j\u0002`\u0007\u0012\n\u00108\u001a\u00060\u0006j\u0002`\u0007\u0012\n\u00109\u001a\u00060\u0006j\u0002`\u000e\u0012\n\u0010:\u001a\u00060\u0006j\u0002`\u000e¢\u0006\u0004\b^\u0010_J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\b\u001a\u00060\u0006j\u0002`\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0014\u0010\r\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\r\u0010\u0005J\u0014\u0010\u000f\u001a\u00060\u0006j\u0002`\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\tJ\u0014\u0010\u0010\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0005J\u0014\u0010\u0011\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0005J\u0014\u0010\u0012\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0005J\u0014\u0010\u0013\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0005J\u0014\u0010\u0014\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0005J\u0014\u0010\u0015\u001a\u00060\u0006j\u0002`\u000eHÆ\u0003¢\u0006\u0004\b\u0015\u0010\tJ\u0014\u0010\u0016\u001a\u00060\u0006j\u0002`\u000eHÆ\u0003¢\u0006\u0004\b\u0016\u0010\tJ\u0014\u0010\u0017\u001a\u00060\u0006j\u0002`\u000eHÆ\u0003¢\u0006\u0004\b\u0017\u0010\tJ\u0014\u0010\u0018\u001a\u00060\u0006j\u0002`\u000eHÆ\u0003¢\u0006\u0004\b\u0018\u0010\tJ\u0014\u0010\u0019\u001a\u00060\u0006j\u0002`\u000eHÆ\u0003¢\u0006\u0004\b\u0019\u0010\tJ\u0014\u0010\u001a\u001a\u00060\u0006j\u0002`\u0007HÆ\u0003¢\u0006\u0004\b\u001a\u0010\tJ\u0014\u0010\u001b\u001a\u00060\u0006j\u0002`\u0007HÆ\u0003¢\u0006\u0004\b\u001b\u0010\tJ\u0014\u0010\u001c\u001a\u00060\u0006j\u0002`\u0007HÆ\u0003¢\u0006\u0004\b\u001c\u0010\tJ\u0014\u0010\u001d\u001a\u00060\u0006j\u0002`\u0007HÆ\u0003¢\u0006\u0004\b\u001d\u0010\tJ\u0014\u0010\u001e\u001a\u00060\u0006j\u0002`\u0007HÆ\u0003¢\u0006\u0004\b\u001e\u0010\tJ\u0014\u0010\u001f\u001a\u00060\u0006j\u0002`\u0007HÆ\u0003¢\u0006\u0004\b\u001f\u0010\tJ\u0014\u0010 \u001a\u00060\u0006j\u0002`\u0007HÆ\u0003¢\u0006\u0004\b \u0010\tJ\u0014\u0010!\u001a\u00060\u0006j\u0002`\u000eHÆ\u0003¢\u0006\u0004\b!\u0010\tJ\u0014\u0010\"\u001a\u00060\u0006j\u0002`\u000eHÆ\u0003¢\u0006\u0004\b\"\u0010\tJÜ\u0002\u0010;\u001a\u00020\u00002\f\b\u0002\u0010#\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010$\u001a\u00060\u0006j\u0002`\u00072\b\b\u0002\u0010%\u001a\u00020\n2\f\b\u0002\u0010&\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010'\u001a\u00060\u0006j\u0002`\u000e2\f\b\u0002\u0010(\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010)\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010*\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010+\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010,\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010-\u001a\u00060\u0006j\u0002`\u000e2\f\b\u0002\u0010.\u001a\u00060\u0006j\u0002`\u000e2\f\b\u0002\u0010/\u001a\u00060\u0006j\u0002`\u000e2\f\b\u0002\u00100\u001a\u00060\u0006j\u0002`\u000e2\f\b\u0002\u00101\u001a\u00060\u0006j\u0002`\u000e2\f\b\u0002\u00102\u001a\u00060\u0006j\u0002`\u00072\f\b\u0002\u00103\u001a\u00060\u0006j\u0002`\u00072\f\b\u0002\u00104\u001a\u00060\u0006j\u0002`\u00072\f\b\u0002\u00105\u001a\u00060\u0006j\u0002`\u00072\f\b\u0002\u00106\u001a\u00060\u0006j\u0002`\u00072\f\b\u0002\u00107\u001a\u00060\u0006j\u0002`\u00072\f\b\u0002\u00108\u001a\u00060\u0006j\u0002`\u00072\f\b\u0002\u00109\u001a\u00060\u0006j\u0002`\u000e2\f\b\u0002\u0010:\u001a\u00060\u0006j\u0002`\u000eHÆ\u0001¢\u0006\u0004\b;\u0010<J\u0010\u0010=\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b=\u0010\fJ\u0010\u0010>\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b>\u0010\u0005J\u001a\u0010A\u001a\u00020@2\b\u0010?\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\bA\u0010BR\u001d\u0010-\u001a\u00060\u0006j\u0002`\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010C\u001a\u0004\bD\u0010\tR\u001d\u0010'\u001a\u00060\u0006j\u0002`\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010C\u001a\u0004\bE\u0010\tR\u001d\u00105\u001a\u00060\u0006j\u0002`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010C\u001a\u0004\bF\u0010\tR\u001d\u0010:\u001a\u00060\u0006j\u0002`\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010C\u001a\u0004\bG\u0010\tR\u001d\u0010/\u001a\u00060\u0006j\u0002`\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010C\u001a\u0004\bH\u0010\tR\u001d\u0010.\u001a\u00060\u0006j\u0002`\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010C\u001a\u0004\bI\u0010\tR\u001d\u0010)\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010J\u001a\u0004\bK\u0010\u0005R\u001d\u0010(\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010J\u001a\u0004\bL\u0010\u0005R\u001d\u00109\u001a\u00060\u0006j\u0002`\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010C\u001a\u0004\bM\u0010\tR\u001d\u0010+\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010J\u001a\u0004\bN\u0010\u0005R\u001d\u00101\u001a\u00060\u0006j\u0002`\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010C\u001a\u0004\bO\u0010\tR\u001d\u00102\u001a\u00060\u0006j\u0002`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010C\u001a\u0004\bP\u0010\tR\u001d\u0010#\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010J\u001a\u0004\bQ\u0010\u0005R\u001d\u0010*\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010J\u001a\u0004\bR\u0010\u0005R\u001d\u0010,\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010J\u001a\u0004\bS\u0010\u0005R\u0019\u0010%\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010T\u001a\u0004\bU\u0010\fR\u001d\u00103\u001a\u00060\u0006j\u0002`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010C\u001a\u0004\bV\u0010\tR\u001d\u00106\u001a\u00060\u0006j\u0002`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010C\u001a\u0004\bW\u0010\tR\u001d\u00107\u001a\u00060\u0006j\u0002`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010C\u001a\u0004\bX\u0010\tR\u001d\u0010$\u001a\u00060\u0006j\u0002`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010C\u001a\u0004\bY\u0010\tR\u001d\u0010&\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010J\u001a\u0004\bZ\u0010\u0005R\u001d\u00104\u001a\u00060\u0006j\u0002`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010C\u001a\u0004\b[\u0010\tR\u001d\u00108\u001a\u00060\u0006j\u0002`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010C\u001a\u0004\b\\\u0010\tR\u001d\u00100\u001a\u00060\u0006j\u0002`\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010C\u001a\u0004\b]\u0010\t¨\u0006`"}, d2 = {"Lco/discord/media_engine/internal/InboundAudio;", "", "", "Lco/discord/media_engine/internal/I32;", "component1", "()I", "", "Lco/discord/media_engine/internal/U64;", "component2", "()J", "", "component3", "()Ljava/lang/String;", "component4", "Lco/discord/media_engine/internal/U32;", "component5", "component6", "component7", "component8", "component9", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component19", "component20", "component21", "component22", "component23", "component24", "audioLevel", "bytesReceived", "codecName", "codecPayloadType", "delayEstimate", "decodingCNG", "decodingMutedOutput", "decodingNormal", "decodingPLC", "decodingPLCCNG", "jitter", "jitterBuffer", "jitterBufferPreferred", "packetsLost", "packetsReceived", "opSilence", "opNormal", "opMerge", "opExpand", "opAccelerate", "opPreemptiveExpand", "opCNG", "speaking", "ssrc", "copy", "(IJLjava/lang/String;IJIIIIIJJJJJJJJJJJJJJ)Lco/discord/media_engine/internal/InboundAudio;", "toString", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getJitter", "getDelayEstimate", "getOpExpand", "getSsrc", "getJitterBufferPreferred", "getJitterBuffer", "I", "getDecodingMutedOutput", "getDecodingCNG", "getSpeaking", "getDecodingPLC", "getPacketsReceived", "getOpSilence", "getAudioLevel", "getDecodingNormal", "getDecodingPLCCNG", "Ljava/lang/String;", "getCodecName", "getOpNormal", "getOpAccelerate", "getOpPreemptiveExpand", "getBytesReceived", "getCodecPayloadType", "getOpMerge", "getOpCNG", "getPacketsLost", HookHelper.constructorName, "(IJLjava/lang/String;IJIIIIIJJJJJJJJJJJJJJ)V", "android_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class InboundAudio {
    private final int audioLevel;
    private final long bytesReceived;
    private final String codecName;
    private final int codecPayloadType;
    private final int decodingCNG;
    private final int decodingMutedOutput;
    private final int decodingNormal;
    private final int decodingPLC;
    private final int decodingPLCCNG;
    private final long delayEstimate;
    private final long jitter;
    private final long jitterBuffer;
    private final long jitterBufferPreferred;
    private final long opAccelerate;
    private final long opCNG;
    private final long opExpand;
    private final long opMerge;
    private final long opNormal;
    private final long opPreemptiveExpand;
    private final long opSilence;
    private final long packetsLost;
    private final long packetsReceived;
    private final long speaking;
    private final long ssrc;

    public InboundAudio(int i, long j, String str, int i2, long j2, int i3, int i4, int i5, int i6, int i7, long j3, long j4, long j5, long j6, long j7, long j8, long j9, long j10, long j11, long j12, long j13, long j14, long j15, long j16) {
        m.checkNotNullParameter(str, "codecName");
        this.audioLevel = i;
        this.bytesReceived = j;
        this.codecName = str;
        this.codecPayloadType = i2;
        this.delayEstimate = j2;
        this.decodingCNG = i3;
        this.decodingMutedOutput = i4;
        this.decodingNormal = i5;
        this.decodingPLC = i6;
        this.decodingPLCCNG = i7;
        this.jitter = j3;
        this.jitterBuffer = j4;
        this.jitterBufferPreferred = j5;
        this.packetsLost = j6;
        this.packetsReceived = j7;
        this.opSilence = j8;
        this.opNormal = j9;
        this.opMerge = j10;
        this.opExpand = j11;
        this.opAccelerate = j12;
        this.opPreemptiveExpand = j13;
        this.opCNG = j14;
        this.speaking = j15;
        this.ssrc = j16;
    }

    public final int component1() {
        return this.audioLevel;
    }

    public final int component10() {
        return this.decodingPLCCNG;
    }

    public final long component11() {
        return this.jitter;
    }

    public final long component12() {
        return this.jitterBuffer;
    }

    public final long component13() {
        return this.jitterBufferPreferred;
    }

    public final long component14() {
        return this.packetsLost;
    }

    public final long component15() {
        return this.packetsReceived;
    }

    public final long component16() {
        return this.opSilence;
    }

    public final long component17() {
        return this.opNormal;
    }

    public final long component18() {
        return this.opMerge;
    }

    public final long component19() {
        return this.opExpand;
    }

    public final long component2() {
        return this.bytesReceived;
    }

    public final long component20() {
        return this.opAccelerate;
    }

    public final long component21() {
        return this.opPreemptiveExpand;
    }

    public final long component22() {
        return this.opCNG;
    }

    public final long component23() {
        return this.speaking;
    }

    public final long component24() {
        return this.ssrc;
    }

    public final String component3() {
        return this.codecName;
    }

    public final int component4() {
        return this.codecPayloadType;
    }

    public final long component5() {
        return this.delayEstimate;
    }

    public final int component6() {
        return this.decodingCNG;
    }

    public final int component7() {
        return this.decodingMutedOutput;
    }

    public final int component8() {
        return this.decodingNormal;
    }

    public final int component9() {
        return this.decodingPLC;
    }

    public final InboundAudio copy(int i, long j, String str, int i2, long j2, int i3, int i4, int i5, int i6, int i7, long j3, long j4, long j5, long j6, long j7, long j8, long j9, long j10, long j11, long j12, long j13, long j14, long j15, long j16) {
        m.checkNotNullParameter(str, "codecName");
        return new InboundAudio(i, j, str, i2, j2, i3, i4, i5, i6, i7, j3, j4, j5, j6, j7, j8, j9, j10, j11, j12, j13, j14, j15, j16);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof InboundAudio)) {
            return false;
        }
        InboundAudio inboundAudio = (InboundAudio) obj;
        return this.audioLevel == inboundAudio.audioLevel && this.bytesReceived == inboundAudio.bytesReceived && m.areEqual(this.codecName, inboundAudio.codecName) && this.codecPayloadType == inboundAudio.codecPayloadType && this.delayEstimate == inboundAudio.delayEstimate && this.decodingCNG == inboundAudio.decodingCNG && this.decodingMutedOutput == inboundAudio.decodingMutedOutput && this.decodingNormal == inboundAudio.decodingNormal && this.decodingPLC == inboundAudio.decodingPLC && this.decodingPLCCNG == inboundAudio.decodingPLCCNG && this.jitter == inboundAudio.jitter && this.jitterBuffer == inboundAudio.jitterBuffer && this.jitterBufferPreferred == inboundAudio.jitterBufferPreferred && this.packetsLost == inboundAudio.packetsLost && this.packetsReceived == inboundAudio.packetsReceived && this.opSilence == inboundAudio.opSilence && this.opNormal == inboundAudio.opNormal && this.opMerge == inboundAudio.opMerge && this.opExpand == inboundAudio.opExpand && this.opAccelerate == inboundAudio.opAccelerate && this.opPreemptiveExpand == inboundAudio.opPreemptiveExpand && this.opCNG == inboundAudio.opCNG && this.speaking == inboundAudio.speaking && this.ssrc == inboundAudio.ssrc;
    }

    public final int getAudioLevel() {
        return this.audioLevel;
    }

    public final long getBytesReceived() {
        return this.bytesReceived;
    }

    public final String getCodecName() {
        return this.codecName;
    }

    public final int getCodecPayloadType() {
        return this.codecPayloadType;
    }

    public final int getDecodingCNG() {
        return this.decodingCNG;
    }

    public final int getDecodingMutedOutput() {
        return this.decodingMutedOutput;
    }

    public final int getDecodingNormal() {
        return this.decodingNormal;
    }

    public final int getDecodingPLC() {
        return this.decodingPLC;
    }

    public final int getDecodingPLCCNG() {
        return this.decodingPLCCNG;
    }

    public final long getDelayEstimate() {
        return this.delayEstimate;
    }

    public final long getJitter() {
        return this.jitter;
    }

    public final long getJitterBuffer() {
        return this.jitterBuffer;
    }

    public final long getJitterBufferPreferred() {
        return this.jitterBufferPreferred;
    }

    public final long getOpAccelerate() {
        return this.opAccelerate;
    }

    public final long getOpCNG() {
        return this.opCNG;
    }

    public final long getOpExpand() {
        return this.opExpand;
    }

    public final long getOpMerge() {
        return this.opMerge;
    }

    public final long getOpNormal() {
        return this.opNormal;
    }

    public final long getOpPreemptiveExpand() {
        return this.opPreemptiveExpand;
    }

    public final long getOpSilence() {
        return this.opSilence;
    }

    public final long getPacketsLost() {
        return this.packetsLost;
    }

    public final long getPacketsReceived() {
        return this.packetsReceived;
    }

    public final long getSpeaking() {
        return this.speaking;
    }

    public final long getSsrc() {
        return this.ssrc;
    }

    public int hashCode() {
        int a = (b.a(this.bytesReceived) + (this.audioLevel * 31)) * 31;
        String str = this.codecName;
        int hashCode = str != null ? str.hashCode() : 0;
        int a2 = b.a(this.delayEstimate);
        int a3 = b.a(this.jitter);
        int a4 = b.a(this.jitterBuffer);
        int a5 = b.a(this.jitterBufferPreferred);
        int a6 = b.a(this.packetsLost);
        int a7 = b.a(this.packetsReceived);
        int a8 = b.a(this.opSilence);
        int a9 = b.a(this.opNormal);
        int a10 = b.a(this.opMerge);
        int a11 = b.a(this.opExpand);
        int a12 = b.a(this.opAccelerate);
        int a13 = b.a(this.opPreemptiveExpand);
        int a14 = b.a(this.opCNG);
        return b.a(this.ssrc) + ((b.a(this.speaking) + ((a14 + ((a13 + ((a12 + ((a11 + ((a10 + ((a9 + ((a8 + ((a7 + ((a6 + ((a5 + ((a4 + ((a3 + ((((((((((((a2 + ((((a + hashCode) * 31) + this.codecPayloadType) * 31)) * 31) + this.decodingCNG) * 31) + this.decodingMutedOutput) * 31) + this.decodingNormal) * 31) + this.decodingPLC) * 31) + this.decodingPLCCNG) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31);
    }

    public String toString() {
        StringBuilder R = a.R("InboundAudio(audioLevel=");
        R.append(this.audioLevel);
        R.append(", bytesReceived=");
        R.append(this.bytesReceived);
        R.append(", codecName=");
        R.append(this.codecName);
        R.append(", codecPayloadType=");
        R.append(this.codecPayloadType);
        R.append(", delayEstimate=");
        R.append(this.delayEstimate);
        R.append(", decodingCNG=");
        R.append(this.decodingCNG);
        R.append(", decodingMutedOutput=");
        R.append(this.decodingMutedOutput);
        R.append(", decodingNormal=");
        R.append(this.decodingNormal);
        R.append(", decodingPLC=");
        R.append(this.decodingPLC);
        R.append(", decodingPLCCNG=");
        R.append(this.decodingPLCCNG);
        R.append(", jitter=");
        R.append(this.jitter);
        R.append(", jitterBuffer=");
        R.append(this.jitterBuffer);
        R.append(", jitterBufferPreferred=");
        R.append(this.jitterBufferPreferred);
        R.append(", packetsLost=");
        R.append(this.packetsLost);
        R.append(", packetsReceived=");
        R.append(this.packetsReceived);
        R.append(", opSilence=");
        R.append(this.opSilence);
        R.append(", opNormal=");
        R.append(this.opNormal);
        R.append(", opMerge=");
        R.append(this.opMerge);
        R.append(", opExpand=");
        R.append(this.opExpand);
        R.append(", opAccelerate=");
        R.append(this.opAccelerate);
        R.append(", opPreemptiveExpand=");
        R.append(this.opPreemptiveExpand);
        R.append(", opCNG=");
        R.append(this.opCNG);
        R.append(", speaking=");
        R.append(this.speaking);
        R.append(", ssrc=");
        return a.B(R, this.ssrc, ")");
    }
}
