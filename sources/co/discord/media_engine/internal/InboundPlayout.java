package co.discord.media_engine.internal;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import co.discord.media_engine.PlayoutMetric;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: NativeStatistics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0017\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0010\b\u0080\b\u0018\u00002\u00020\u0001BW\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\u0006\u0010\u0010\u001a\u00020\u0002\u0012\u0006\u0010\u0011\u001a\u00020\u0002\u0012\u0006\u0010\u0012\u001a\u00020\u0002\u0012\u0006\u0010\u0013\u001a\u00020\u0002\u0012\u0006\u0010\u0014\u001a\u00020\u0002\u0012\u0006\u0010\u0015\u001a\u00020\u0002\u0012\u0006\u0010\u0016\u001a\u00020\u0002\u0012\u0006\u0010\u0017\u001a\u00020\u0002¢\u0006\u0004\b/\u00100J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0004J\u0010\u0010\b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\t\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0004J\u0010\u0010\n\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\n\u0010\u0004J\u0010\u0010\u000b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u000b\u0010\u0004J\u0010\u0010\f\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\f\u0010\u0004J\u0010\u0010\r\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\r\u0010\u0004Jt\u0010\u0018\u001a\u00020\u00002\b\b\u0002\u0010\u000e\u001a\u00020\u00022\b\b\u0002\u0010\u000f\u001a\u00020\u00022\b\b\u0002\u0010\u0010\u001a\u00020\u00022\b\b\u0002\u0010\u0011\u001a\u00020\u00022\b\b\u0002\u0010\u0012\u001a\u00020\u00022\b\b\u0002\u0010\u0013\u001a\u00020\u00022\b\b\u0002\u0010\u0014\u001a\u00020\u00022\b\b\u0002\u0010\u0015\u001a\u00020\u00022\b\b\u0002\u0010\u0016\u001a\u00020\u00022\b\b\u0002\u0010\u0017\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u001b\u001a\u00020\u001aHÖ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001e\u001a\u00020\u001dHÖ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u001a\u0010\"\u001a\u00020!2\b\u0010 \u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\"\u0010#R\u0019\u0010\u0016\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010$\u001a\u0004\b%\u0010\u0004R\u0019\u0010\u0012\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010$\u001a\u0004\b&\u0010\u0004R\u0019\u0010\u0010\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010$\u001a\u0004\b'\u0010\u0004R\u0019\u0010\u0014\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010$\u001a\u0004\b(\u0010\u0004R\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010$\u001a\u0004\b)\u0010\u0004R\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010$\u001a\u0004\b*\u0010\u0004R\u0019\u0010\u0011\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010$\u001a\u0004\b+\u0010\u0004R\u0019\u0010\u0013\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010$\u001a\u0004\b,\u0010\u0004R\u0019\u0010\u0015\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010$\u001a\u0004\b-\u0010\u0004R\u0019\u0010\u0017\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010$\u001a\u0004\b.\u0010\u0004¨\u00061"}, d2 = {"Lco/discord/media_engine/internal/InboundPlayout;", "", "Lco/discord/media_engine/PlayoutMetric;", "component1", "()Lco/discord/media_engine/PlayoutMetric;", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "component10", "audioJitterBuffer", "audioJitterDelay", "audioJitterTarget", "audioPlayoutUnderruns", "audioCaptureOverruns", "videoJitterBuffer", "videoJitterDelay", "videoJitterTarget", "relativeReceptionDelay", "relativePlayoutDelay", "copy", "(Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;)Lco/discord/media_engine/internal/InboundPlayout;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lco/discord/media_engine/PlayoutMetric;", "getRelativeReceptionDelay", "getAudioCaptureOverruns", "getAudioJitterTarget", "getVideoJitterDelay", "getAudioJitterBuffer", "getAudioJitterDelay", "getAudioPlayoutUnderruns", "getVideoJitterBuffer", "getVideoJitterTarget", "getRelativePlayoutDelay", HookHelper.constructorName, "(Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;)V", "android_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class InboundPlayout {
    private final PlayoutMetric audioCaptureOverruns;
    private final PlayoutMetric audioJitterBuffer;
    private final PlayoutMetric audioJitterDelay;
    private final PlayoutMetric audioJitterTarget;
    private final PlayoutMetric audioPlayoutUnderruns;
    private final PlayoutMetric relativePlayoutDelay;
    private final PlayoutMetric relativeReceptionDelay;
    private final PlayoutMetric videoJitterBuffer;
    private final PlayoutMetric videoJitterDelay;
    private final PlayoutMetric videoJitterTarget;

    public InboundPlayout(PlayoutMetric playoutMetric, PlayoutMetric playoutMetric2, PlayoutMetric playoutMetric3, PlayoutMetric playoutMetric4, PlayoutMetric playoutMetric5, PlayoutMetric playoutMetric6, PlayoutMetric playoutMetric7, PlayoutMetric playoutMetric8, PlayoutMetric playoutMetric9, PlayoutMetric playoutMetric10) {
        m.checkNotNullParameter(playoutMetric, "audioJitterBuffer");
        m.checkNotNullParameter(playoutMetric2, "audioJitterDelay");
        m.checkNotNullParameter(playoutMetric3, "audioJitterTarget");
        m.checkNotNullParameter(playoutMetric4, "audioPlayoutUnderruns");
        m.checkNotNullParameter(playoutMetric5, "audioCaptureOverruns");
        m.checkNotNullParameter(playoutMetric6, "videoJitterBuffer");
        m.checkNotNullParameter(playoutMetric7, "videoJitterDelay");
        m.checkNotNullParameter(playoutMetric8, "videoJitterTarget");
        m.checkNotNullParameter(playoutMetric9, "relativeReceptionDelay");
        m.checkNotNullParameter(playoutMetric10, "relativePlayoutDelay");
        this.audioJitterBuffer = playoutMetric;
        this.audioJitterDelay = playoutMetric2;
        this.audioJitterTarget = playoutMetric3;
        this.audioPlayoutUnderruns = playoutMetric4;
        this.audioCaptureOverruns = playoutMetric5;
        this.videoJitterBuffer = playoutMetric6;
        this.videoJitterDelay = playoutMetric7;
        this.videoJitterTarget = playoutMetric8;
        this.relativeReceptionDelay = playoutMetric9;
        this.relativePlayoutDelay = playoutMetric10;
    }

    public final PlayoutMetric component1() {
        return this.audioJitterBuffer;
    }

    public final PlayoutMetric component10() {
        return this.relativePlayoutDelay;
    }

    public final PlayoutMetric component2() {
        return this.audioJitterDelay;
    }

    public final PlayoutMetric component3() {
        return this.audioJitterTarget;
    }

    public final PlayoutMetric component4() {
        return this.audioPlayoutUnderruns;
    }

    public final PlayoutMetric component5() {
        return this.audioCaptureOverruns;
    }

    public final PlayoutMetric component6() {
        return this.videoJitterBuffer;
    }

    public final PlayoutMetric component7() {
        return this.videoJitterDelay;
    }

    public final PlayoutMetric component8() {
        return this.videoJitterTarget;
    }

    public final PlayoutMetric component9() {
        return this.relativeReceptionDelay;
    }

    public final InboundPlayout copy(PlayoutMetric playoutMetric, PlayoutMetric playoutMetric2, PlayoutMetric playoutMetric3, PlayoutMetric playoutMetric4, PlayoutMetric playoutMetric5, PlayoutMetric playoutMetric6, PlayoutMetric playoutMetric7, PlayoutMetric playoutMetric8, PlayoutMetric playoutMetric9, PlayoutMetric playoutMetric10) {
        m.checkNotNullParameter(playoutMetric, "audioJitterBuffer");
        m.checkNotNullParameter(playoutMetric2, "audioJitterDelay");
        m.checkNotNullParameter(playoutMetric3, "audioJitterTarget");
        m.checkNotNullParameter(playoutMetric4, "audioPlayoutUnderruns");
        m.checkNotNullParameter(playoutMetric5, "audioCaptureOverruns");
        m.checkNotNullParameter(playoutMetric6, "videoJitterBuffer");
        m.checkNotNullParameter(playoutMetric7, "videoJitterDelay");
        m.checkNotNullParameter(playoutMetric8, "videoJitterTarget");
        m.checkNotNullParameter(playoutMetric9, "relativeReceptionDelay");
        m.checkNotNullParameter(playoutMetric10, "relativePlayoutDelay");
        return new InboundPlayout(playoutMetric, playoutMetric2, playoutMetric3, playoutMetric4, playoutMetric5, playoutMetric6, playoutMetric7, playoutMetric8, playoutMetric9, playoutMetric10);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof InboundPlayout)) {
            return false;
        }
        InboundPlayout inboundPlayout = (InboundPlayout) obj;
        return m.areEqual(this.audioJitterBuffer, inboundPlayout.audioJitterBuffer) && m.areEqual(this.audioJitterDelay, inboundPlayout.audioJitterDelay) && m.areEqual(this.audioJitterTarget, inboundPlayout.audioJitterTarget) && m.areEqual(this.audioPlayoutUnderruns, inboundPlayout.audioPlayoutUnderruns) && m.areEqual(this.audioCaptureOverruns, inboundPlayout.audioCaptureOverruns) && m.areEqual(this.videoJitterBuffer, inboundPlayout.videoJitterBuffer) && m.areEqual(this.videoJitterDelay, inboundPlayout.videoJitterDelay) && m.areEqual(this.videoJitterTarget, inboundPlayout.videoJitterTarget) && m.areEqual(this.relativeReceptionDelay, inboundPlayout.relativeReceptionDelay) && m.areEqual(this.relativePlayoutDelay, inboundPlayout.relativePlayoutDelay);
    }

    public final PlayoutMetric getAudioCaptureOverruns() {
        return this.audioCaptureOverruns;
    }

    public final PlayoutMetric getAudioJitterBuffer() {
        return this.audioJitterBuffer;
    }

    public final PlayoutMetric getAudioJitterDelay() {
        return this.audioJitterDelay;
    }

    public final PlayoutMetric getAudioJitterTarget() {
        return this.audioJitterTarget;
    }

    public final PlayoutMetric getAudioPlayoutUnderruns() {
        return this.audioPlayoutUnderruns;
    }

    public final PlayoutMetric getRelativePlayoutDelay() {
        return this.relativePlayoutDelay;
    }

    public final PlayoutMetric getRelativeReceptionDelay() {
        return this.relativeReceptionDelay;
    }

    public final PlayoutMetric getVideoJitterBuffer() {
        return this.videoJitterBuffer;
    }

    public final PlayoutMetric getVideoJitterDelay() {
        return this.videoJitterDelay;
    }

    public final PlayoutMetric getVideoJitterTarget() {
        return this.videoJitterTarget;
    }

    public int hashCode() {
        PlayoutMetric playoutMetric = this.audioJitterBuffer;
        int i = 0;
        int hashCode = (playoutMetric != null ? playoutMetric.hashCode() : 0) * 31;
        PlayoutMetric playoutMetric2 = this.audioJitterDelay;
        int hashCode2 = (hashCode + (playoutMetric2 != null ? playoutMetric2.hashCode() : 0)) * 31;
        PlayoutMetric playoutMetric3 = this.audioJitterTarget;
        int hashCode3 = (hashCode2 + (playoutMetric3 != null ? playoutMetric3.hashCode() : 0)) * 31;
        PlayoutMetric playoutMetric4 = this.audioPlayoutUnderruns;
        int hashCode4 = (hashCode3 + (playoutMetric4 != null ? playoutMetric4.hashCode() : 0)) * 31;
        PlayoutMetric playoutMetric5 = this.audioCaptureOverruns;
        int hashCode5 = (hashCode4 + (playoutMetric5 != null ? playoutMetric5.hashCode() : 0)) * 31;
        PlayoutMetric playoutMetric6 = this.videoJitterBuffer;
        int hashCode6 = (hashCode5 + (playoutMetric6 != null ? playoutMetric6.hashCode() : 0)) * 31;
        PlayoutMetric playoutMetric7 = this.videoJitterDelay;
        int hashCode7 = (hashCode6 + (playoutMetric7 != null ? playoutMetric7.hashCode() : 0)) * 31;
        PlayoutMetric playoutMetric8 = this.videoJitterTarget;
        int hashCode8 = (hashCode7 + (playoutMetric8 != null ? playoutMetric8.hashCode() : 0)) * 31;
        PlayoutMetric playoutMetric9 = this.relativeReceptionDelay;
        int hashCode9 = (hashCode8 + (playoutMetric9 != null ? playoutMetric9.hashCode() : 0)) * 31;
        PlayoutMetric playoutMetric10 = this.relativePlayoutDelay;
        if (playoutMetric10 != null) {
            i = playoutMetric10.hashCode();
        }
        return hashCode9 + i;
    }

    public String toString() {
        StringBuilder R = a.R("InboundPlayout(audioJitterBuffer=");
        R.append(this.audioJitterBuffer);
        R.append(", audioJitterDelay=");
        R.append(this.audioJitterDelay);
        R.append(", audioJitterTarget=");
        R.append(this.audioJitterTarget);
        R.append(", audioPlayoutUnderruns=");
        R.append(this.audioPlayoutUnderruns);
        R.append(", audioCaptureOverruns=");
        R.append(this.audioCaptureOverruns);
        R.append(", videoJitterBuffer=");
        R.append(this.videoJitterBuffer);
        R.append(", videoJitterDelay=");
        R.append(this.videoJitterDelay);
        R.append(", videoJitterTarget=");
        R.append(this.videoJitterTarget);
        R.append(", relativeReceptionDelay=");
        R.append(this.relativeReceptionDelay);
        R.append(", relativePlayoutDelay=");
        R.append(this.relativePlayoutDelay);
        R.append(")");
        return R.toString();
    }
}
