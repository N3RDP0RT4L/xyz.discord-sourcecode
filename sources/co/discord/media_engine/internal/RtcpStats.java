package co.discord.media_engine.internal;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import kotlin.Metadata;
/* compiled from: NativeStatistics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u000e\b\u0080\b\u0018\u00002\u00020\u0001B[\u0012\n\u0010\u0010\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010\u0011\u001a\u00060\u0002j\u0002`\u0006\u0012\n\u0010\u0012\u001a\u00060\bj\u0002`\t\u0012\n\u0010\u0013\u001a\u00060\bj\u0002`\t\u0012\n\u0010\u0014\u001a\u00060\bj\u0002`\t\u0012\n\u0010\u0015\u001a\u00060\bj\u0002`\t\u0012\n\u0010\u0016\u001a\u00060\bj\u0002`\t¢\u0006\u0004\b*\u0010+J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\u0007\u001a\u00060\u0002j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J\u0014\u0010\n\u001a\u00060\bj\u0002`\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0014\u0010\f\u001a\u00060\bj\u0002`\tHÆ\u0003¢\u0006\u0004\b\f\u0010\u000bJ\u0014\u0010\r\u001a\u00060\bj\u0002`\tHÆ\u0003¢\u0006\u0004\b\r\u0010\u000bJ\u0014\u0010\u000e\u001a\u00060\bj\u0002`\tHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000bJ\u0014\u0010\u000f\u001a\u00060\bj\u0002`\tHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u000bJr\u0010\u0017\u001a\u00020\u00002\f\b\u0002\u0010\u0010\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010\u0011\u001a\u00060\u0002j\u0002`\u00062\f\b\u0002\u0010\u0012\u001a\u00060\bj\u0002`\t2\f\b\u0002\u0010\u0013\u001a\u00060\bj\u0002`\t2\f\b\u0002\u0010\u0014\u001a\u00060\bj\u0002`\t2\f\b\u0002\u0010\u0015\u001a\u00060\bj\u0002`\t2\f\b\u0002\u0010\u0016\u001a\u00060\bj\u0002`\tHÆ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u001a\u001a\u00020\u0019HÖ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u001c\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u001c\u0010\u0005J\u001a\u0010\u001f\u001a\u00020\u001e2\b\u0010\u001d\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001f\u0010 R\u001d\u0010\u0011\u001a\u00060\u0002j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010!\u001a\u0004\b\"\u0010\u0005R\u001d\u0010\u0015\u001a\u00060\bj\u0002`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010#\u001a\u0004\b$\u0010\u000bR\u001d\u0010\u0014\u001a\u00060\bj\u0002`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010#\u001a\u0004\b%\u0010\u000bR\u001d\u0010\u0016\u001a\u00060\bj\u0002`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010#\u001a\u0004\b&\u0010\u000bR\u001d\u0010\u0013\u001a\u00060\bj\u0002`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010#\u001a\u0004\b'\u0010\u000bR\u001d\u0010\u0012\u001a\u00060\bj\u0002`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010#\u001a\u0004\b(\u0010\u000bR\u001d\u0010\u0010\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010!\u001a\u0004\b)\u0010\u0005¨\u0006,"}, d2 = {"Lco/discord/media_engine/internal/RtcpStats;", "", "", "Lco/discord/media_engine/internal/U8;", "component1", "()I", "Lco/discord/media_engine/internal/I32;", "component2", "", "Lco/discord/media_engine/internal/U32;", "component3", "()J", "component4", "component5", "component6", "component7", "fractionLost", "packetsLost", "firPackets", "nackPackets", "nackRequests", "pliPackets", "uniqueNackRequests", "copy", "(IIJJJJJ)Lco/discord/media_engine/internal/RtcpStats;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getPacketsLost", "J", "getPliPackets", "getNackRequests", "getUniqueNackRequests", "getNackPackets", "getFirPackets", "getFractionLost", HookHelper.constructorName, "(IIJJJJJ)V", "android_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class RtcpStats {
    private final long firPackets;
    private final int fractionLost;
    private final long nackPackets;
    private final long nackRequests;
    private final int packetsLost;
    private final long pliPackets;
    private final long uniqueNackRequests;

    public RtcpStats(int i, int i2, long j, long j2, long j3, long j4, long j5) {
        this.fractionLost = i;
        this.packetsLost = i2;
        this.firPackets = j;
        this.nackPackets = j2;
        this.nackRequests = j3;
        this.pliPackets = j4;
        this.uniqueNackRequests = j5;
    }

    public final int component1() {
        return this.fractionLost;
    }

    public final int component2() {
        return this.packetsLost;
    }

    public final long component3() {
        return this.firPackets;
    }

    public final long component4() {
        return this.nackPackets;
    }

    public final long component5() {
        return this.nackRequests;
    }

    public final long component6() {
        return this.pliPackets;
    }

    public final long component7() {
        return this.uniqueNackRequests;
    }

    public final RtcpStats copy(int i, int i2, long j, long j2, long j3, long j4, long j5) {
        return new RtcpStats(i, i2, j, j2, j3, j4, j5);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof RtcpStats)) {
            return false;
        }
        RtcpStats rtcpStats = (RtcpStats) obj;
        return this.fractionLost == rtcpStats.fractionLost && this.packetsLost == rtcpStats.packetsLost && this.firPackets == rtcpStats.firPackets && this.nackPackets == rtcpStats.nackPackets && this.nackRequests == rtcpStats.nackRequests && this.pliPackets == rtcpStats.pliPackets && this.uniqueNackRequests == rtcpStats.uniqueNackRequests;
    }

    public final long getFirPackets() {
        return this.firPackets;
    }

    public final int getFractionLost() {
        return this.fractionLost;
    }

    public final long getNackPackets() {
        return this.nackPackets;
    }

    public final long getNackRequests() {
        return this.nackRequests;
    }

    public final int getPacketsLost() {
        return this.packetsLost;
    }

    public final long getPliPackets() {
        return this.pliPackets;
    }

    public final long getUniqueNackRequests() {
        return this.uniqueNackRequests;
    }

    public int hashCode() {
        int a = b.a(this.firPackets);
        int a2 = b.a(this.nackPackets);
        int a3 = b.a(this.nackRequests);
        int a4 = b.a(this.pliPackets);
        return b.a(this.uniqueNackRequests) + ((a4 + ((a3 + ((a2 + ((a + (((this.fractionLost * 31) + this.packetsLost) * 31)) * 31)) * 31)) * 31)) * 31);
    }

    public String toString() {
        StringBuilder R = a.R("RtcpStats(fractionLost=");
        R.append(this.fractionLost);
        R.append(", packetsLost=");
        R.append(this.packetsLost);
        R.append(", firPackets=");
        R.append(this.firPackets);
        R.append(", nackPackets=");
        R.append(this.nackPackets);
        R.append(", nackRequests=");
        R.append(this.nackRequests);
        R.append(", pliPackets=");
        R.append(this.pliPackets);
        R.append(", uniqueNackRequests=");
        return a.B(R, this.uniqueNackRequests, ")");
    }
}
