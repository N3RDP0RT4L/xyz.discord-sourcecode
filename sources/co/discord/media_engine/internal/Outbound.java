package co.discord.media_engine.internal;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.Arrays;
import kotlin.Metadata;
import org.webrtc.MediaStreamTrack;
/* compiled from: NativeStatistics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0080\b\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0005\u0012\u000e\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\b¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0018\u0010\n\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ8\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00052\u0010\b\u0002\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\bHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0004J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004R\u001b\u0010\r\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001b\u001a\u0004\b\u001c\u0010\u0007R!\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001d\u001a\u0004\b\u001e\u0010\u000b¨\u0006!"}, d2 = {"Lco/discord/media_engine/internal/Outbound;", "", "", "component1", "()Ljava/lang/String;", "Lco/discord/media_engine/internal/OutboundAudio;", "component2", "()Lco/discord/media_engine/internal/OutboundAudio;", "", "Lco/discord/media_engine/internal/OutboundVideo;", "component3", "()[Lco/discord/media_engine/internal/OutboundVideo;", ModelAuditLogEntry.CHANGE_KEY_ID, MediaStreamTrack.AUDIO_TRACK_KIND, "videos", "copy", "(Ljava/lang/String;Lco/discord/media_engine/internal/OutboundAudio;[Lco/discord/media_engine/internal/OutboundVideo;)Lco/discord/media_engine/internal/Outbound;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getId", "Lco/discord/media_engine/internal/OutboundAudio;", "getAudio", "[Lco/discord/media_engine/internal/OutboundVideo;", "getVideos", HookHelper.constructorName, "(Ljava/lang/String;Lco/discord/media_engine/internal/OutboundAudio;[Lco/discord/media_engine/internal/OutboundVideo;)V", "android_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class Outbound {
    private final OutboundAudio audio;

    /* renamed from: id  reason: collision with root package name */
    private final String f1992id;
    private final OutboundVideo[] videos;

    public Outbound(String str, OutboundAudio outboundAudio, OutboundVideo[] outboundVideoArr) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_ID);
        this.f1992id = str;
        this.audio = outboundAudio;
        this.videos = outboundVideoArr;
    }

    public static /* synthetic */ Outbound copy$default(Outbound outbound, String str, OutboundAudio outboundAudio, OutboundVideo[] outboundVideoArr, int i, Object obj) {
        if ((i & 1) != 0) {
            str = outbound.f1992id;
        }
        if ((i & 2) != 0) {
            outboundAudio = outbound.audio;
        }
        if ((i & 4) != 0) {
            outboundVideoArr = outbound.videos;
        }
        return outbound.copy(str, outboundAudio, outboundVideoArr);
    }

    public final String component1() {
        return this.f1992id;
    }

    public final OutboundAudio component2() {
        return this.audio;
    }

    public final OutboundVideo[] component3() {
        return this.videos;
    }

    public final Outbound copy(String str, OutboundAudio outboundAudio, OutboundVideo[] outboundVideoArr) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_ID);
        return new Outbound(str, outboundAudio, outboundVideoArr);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Outbound)) {
            return false;
        }
        Outbound outbound = (Outbound) obj;
        return m.areEqual(this.f1992id, outbound.f1992id) && m.areEqual(this.audio, outbound.audio) && m.areEqual(this.videos, outbound.videos);
    }

    public final OutboundAudio getAudio() {
        return this.audio;
    }

    public final String getId() {
        return this.f1992id;
    }

    public final OutboundVideo[] getVideos() {
        return this.videos;
    }

    public int hashCode() {
        String str = this.f1992id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        OutboundAudio outboundAudio = this.audio;
        int hashCode2 = (hashCode + (outboundAudio != null ? outboundAudio.hashCode() : 0)) * 31;
        OutboundVideo[] outboundVideoArr = this.videos;
        if (outboundVideoArr != null) {
            i = Arrays.hashCode(outboundVideoArr);
        }
        return hashCode2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("Outbound(id=");
        R.append(this.f1992id);
        R.append(", audio=");
        R.append(this.audio);
        R.append(", videos=");
        R.append(Arrays.toString(this.videos));
        R.append(")");
        return R.toString();
    }
}
