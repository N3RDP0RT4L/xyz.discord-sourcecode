package co.discord.media_engine.internal;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import com.hammerandchisel.libdiscord.R;
import d0.y.h;
import d0.z.d.m;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import kotlin.Metadata;
/* compiled from: AssetManagement.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b\u0014\u0010\u0015J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\t\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0015\u0010\r\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\r\u0010\u000eR\u0019\u0010\u0010\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013¨\u0006\u0016"}, d2 = {"Lco/discord/media_engine/internal/AssetManagement;", "", "Ljava/io/File;", "dir", "", ModelAuditLogEntry.CHANGE_KEY_NAME, "", "copy", "(Ljava/io/File;Ljava/lang/String;)V", "cleanup", "()V", "", "enabled", "ensureKrispModelsCopied", "(Z)V", "Landroid/content/Context;", "ctx", "Landroid/content/Context;", "getCtx", "()Landroid/content/Context;", HookHelper.constructorName, "(Landroid/content/Context;)V", "android_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AssetManagement {
    private final Context ctx;

    public AssetManagement(Context context) {
        m.checkNotNullParameter(context, "ctx");
        this.ctx = context;
    }

    private final void cleanup() {
        try {
            File file = new File(this.ctx.getFilesDir(), "thz");
            if (file.exists()) {
                h.deleteRecursively(file);
            }
        } catch (Exception e) {
            StringBuilder R = a.R("Failed removing krisp model files: ");
            R.append(e.getMessage());
            R.append(": ");
            R.append(e.toString());
            Log.e("DiscordKrisp", R.toString());
        }
    }

    private final void copy(File file, String str) {
        AssetManager assets = this.ctx.getAssets();
        InputStream open = assets.open("thz/" + str);
        m.checkNotNullExpressionValue(open, "ctx.assets.open(\"thz/\" + name)");
        FileOutputStream fileOutputStream = new FileOutputStream(new File(file, str));
        d0.y.a.copyTo(open, fileOutputStream, 1024);
        open.close();
        fileOutputStream.close();
    }

    public final void ensureKrispModelsCopied(boolean z2) {
        if (z2) {
            try {
                File file = new File(this.ctx.getFilesDir(), "thz");
                String string = this.ctx.getString(R.string.krisp_model_version);
                m.checkNotNullExpressionValue(string, "ctx.getString(R.string.krisp_model_version)");
                if (!new File(file, string).exists()) {
                    cleanup();
                    File file2 = new File(this.ctx.getFilesDir(), "thz");
                    file2.mkdir();
                    File file3 = new File(file2, string);
                    file3.mkdir();
                    String[] list = this.ctx.getAssets().list("thz");
                    if (list == null) {
                        list = new String[0];
                    }
                    for (String str : list) {
                        m.checkNotNullExpressionValue(str, "file");
                        copy(file3, str);
                    }
                }
            } catch (Exception e) {
                StringBuilder R = a.R("Failed copying krisp model files: ");
                R.append(e.getMessage());
                R.append(": ");
                R.append(e.toString());
                Log.e("DiscordKrisp", R.toString());
                cleanup();
            }
        } else {
            cleanup();
        }
    }

    public final Context getCtx() {
        return this.ctx;
    }
}
