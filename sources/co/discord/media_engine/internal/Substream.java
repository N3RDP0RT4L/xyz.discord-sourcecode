package co.discord.media_engine.internal;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: NativeStatistics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0010\u000e\n\u0002\b\u0019\b\u0080\b\u0018\u00002\u00020\u0001B\u0083\u0001\u0012\n\u0010\u001c\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\u001d\u001a\u00020\u0006\u0012\n\u0010\u001e\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\u001f\u001a\u00020\n\u0012\u0006\u0010 \u001a\u00020\n\u0012\n\u0010!\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010\"\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010#\u001a\u00020\u0010\u0012\u0006\u0010$\u001a\u00020\u0013\u0012\n\u0010%\u001a\u00060\u0016j\u0002`\u0017\u0012\n\u0010&\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010'\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\bA\u0010BJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0014\u0010\t\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\t\u0010\u0005J\u0010\u0010\u000b\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\r\u0010\fJ\u0014\u0010\u000e\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u000e\u0010\u0005J\u0014\u0010\u000f\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0005J\u0010\u0010\u0011\u001a\u00020\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0014\u0010\u0018\u001a\u00060\u0016j\u0002`\u0017HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0019J\u0014\u0010\u001a\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u0005J\u0014\u0010\u001b\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u001b\u0010\u0005J¤\u0001\u0010(\u001a\u00020\u00002\f\b\u0002\u0010\u001c\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u001d\u001a\u00020\u00062\f\b\u0002\u0010\u001e\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u001f\u001a\u00020\n2\b\b\u0002\u0010 \u001a\u00020\n2\f\b\u0002\u0010!\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010\"\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010#\u001a\u00020\u00102\b\b\u0002\u0010$\u001a\u00020\u00132\f\b\u0002\u0010%\u001a\u00060\u0016j\u0002`\u00172\f\b\u0002\u0010&\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010'\u001a\u00060\u0002j\u0002`\u0003HÆ\u0001¢\u0006\u0004\b(\u0010)J\u0010\u0010+\u001a\u00020*HÖ\u0001¢\u0006\u0004\b+\u0010,J\u0010\u0010-\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b-\u0010\u0005J\u001a\u0010/\u001a\u00020\n2\b\u0010.\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b/\u00100R\u001d\u0010!\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b!\u00101\u001a\u0004\b2\u0010\u0005R\u0019\u0010 \u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b \u00103\u001a\u0004\b \u0010\fR\u0019\u0010#\u001a\u00020\u00108\u0006@\u0006¢\u0006\f\n\u0004\b#\u00104\u001a\u0004\b5\u0010\u0012R\u0019\u0010\u001d\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00106\u001a\u0004\b7\u0010\bR\u001d\u0010\"\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00101\u001a\u0004\b8\u0010\u0005R\u0019\u0010\u001f\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00103\u001a\u0004\b\u001f\u0010\fR\u001d\u0010&\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b&\u00101\u001a\u0004\b9\u0010\u0005R\u001d\u0010\u001e\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00101\u001a\u0004\b:\u0010\u0005R\u0019\u0010$\u001a\u00020\u00138\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010;\u001a\u0004\b<\u0010\u0015R\u001d\u0010\u001c\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u00101\u001a\u0004\b=\u0010\u0005R\u001d\u0010'\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b'\u00101\u001a\u0004\b>\u0010\u0005R\u001d\u0010%\u001a\u00060\u0016j\u0002`\u00178\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010?\u001a\u0004\b@\u0010\u0019¨\u0006C"}, d2 = {"Lco/discord/media_engine/internal/Substream;", "", "", "Lco/discord/media_engine/internal/I32;", "component1", "()I", "Lco/discord/media_engine/internal/FrameCounts;", "component2", "()Lco/discord/media_engine/internal/FrameCounts;", "component3", "", "component4", "()Z", "component5", "component6", "component7", "Lco/discord/media_engine/internal/RtcpStats;", "component8", "()Lco/discord/media_engine/internal/RtcpStats;", "Lco/discord/media_engine/internal/RtpStats;", "component9", "()Lco/discord/media_engine/internal/RtpStats;", "", "Lco/discord/media_engine/internal/U32;", "component10", "()J", "component11", "component12", "avgDelay", "frameCounts", "height", "isFlexFEC", "isRTX", "maxDelay", "retransmitBitrate", "rtcpStats", "rtpStats", "ssrc", "totalBitrate", "width", "copy", "(ILco/discord/media_engine/internal/FrameCounts;IZZIILco/discord/media_engine/internal/RtcpStats;Lco/discord/media_engine/internal/RtpStats;JII)Lco/discord/media_engine/internal/Substream;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "I", "getMaxDelay", "Z", "Lco/discord/media_engine/internal/RtcpStats;", "getRtcpStats", "Lco/discord/media_engine/internal/FrameCounts;", "getFrameCounts", "getRetransmitBitrate", "getTotalBitrate", "getHeight", "Lco/discord/media_engine/internal/RtpStats;", "getRtpStats", "getAvgDelay", "getWidth", "J", "getSsrc", HookHelper.constructorName, "(ILco/discord/media_engine/internal/FrameCounts;IZZIILco/discord/media_engine/internal/RtcpStats;Lco/discord/media_engine/internal/RtpStats;JII)V", "android_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class Substream {
    private final int avgDelay;
    private final FrameCounts frameCounts;
    private final int height;
    private final boolean isFlexFEC;
    private final boolean isRTX;
    private final int maxDelay;
    private final int retransmitBitrate;
    private final RtcpStats rtcpStats;
    private final RtpStats rtpStats;
    private final long ssrc;
    private final int totalBitrate;
    private final int width;

    public Substream(int i, FrameCounts frameCounts, int i2, boolean z2, boolean z3, int i3, int i4, RtcpStats rtcpStats, RtpStats rtpStats, long j, int i5, int i6) {
        m.checkNotNullParameter(frameCounts, "frameCounts");
        m.checkNotNullParameter(rtcpStats, "rtcpStats");
        m.checkNotNullParameter(rtpStats, "rtpStats");
        this.avgDelay = i;
        this.frameCounts = frameCounts;
        this.height = i2;
        this.isFlexFEC = z2;
        this.isRTX = z3;
        this.maxDelay = i3;
        this.retransmitBitrate = i4;
        this.rtcpStats = rtcpStats;
        this.rtpStats = rtpStats;
        this.ssrc = j;
        this.totalBitrate = i5;
        this.width = i6;
    }

    public final int component1() {
        return this.avgDelay;
    }

    public final long component10() {
        return this.ssrc;
    }

    public final int component11() {
        return this.totalBitrate;
    }

    public final int component12() {
        return this.width;
    }

    public final FrameCounts component2() {
        return this.frameCounts;
    }

    public final int component3() {
        return this.height;
    }

    public final boolean component4() {
        return this.isFlexFEC;
    }

    public final boolean component5() {
        return this.isRTX;
    }

    public final int component6() {
        return this.maxDelay;
    }

    public final int component7() {
        return this.retransmitBitrate;
    }

    public final RtcpStats component8() {
        return this.rtcpStats;
    }

    public final RtpStats component9() {
        return this.rtpStats;
    }

    public final Substream copy(int i, FrameCounts frameCounts, int i2, boolean z2, boolean z3, int i3, int i4, RtcpStats rtcpStats, RtpStats rtpStats, long j, int i5, int i6) {
        m.checkNotNullParameter(frameCounts, "frameCounts");
        m.checkNotNullParameter(rtcpStats, "rtcpStats");
        m.checkNotNullParameter(rtpStats, "rtpStats");
        return new Substream(i, frameCounts, i2, z2, z3, i3, i4, rtcpStats, rtpStats, j, i5, i6);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Substream)) {
            return false;
        }
        Substream substream = (Substream) obj;
        return this.avgDelay == substream.avgDelay && m.areEqual(this.frameCounts, substream.frameCounts) && this.height == substream.height && this.isFlexFEC == substream.isFlexFEC && this.isRTX == substream.isRTX && this.maxDelay == substream.maxDelay && this.retransmitBitrate == substream.retransmitBitrate && m.areEqual(this.rtcpStats, substream.rtcpStats) && m.areEqual(this.rtpStats, substream.rtpStats) && this.ssrc == substream.ssrc && this.totalBitrate == substream.totalBitrate && this.width == substream.width;
    }

    public final int getAvgDelay() {
        return this.avgDelay;
    }

    public final FrameCounts getFrameCounts() {
        return this.frameCounts;
    }

    public final int getHeight() {
        return this.height;
    }

    public final int getMaxDelay() {
        return this.maxDelay;
    }

    public final int getRetransmitBitrate() {
        return this.retransmitBitrate;
    }

    public final RtcpStats getRtcpStats() {
        return this.rtcpStats;
    }

    public final RtpStats getRtpStats() {
        return this.rtpStats;
    }

    public final long getSsrc() {
        return this.ssrc;
    }

    public final int getTotalBitrate() {
        return this.totalBitrate;
    }

    public final int getWidth() {
        return this.width;
    }

    public int hashCode() {
        int i = this.avgDelay * 31;
        FrameCounts frameCounts = this.frameCounts;
        int i2 = 0;
        int hashCode = (((i + (frameCounts != null ? frameCounts.hashCode() : 0)) * 31) + this.height) * 31;
        boolean z2 = this.isFlexFEC;
        int i3 = 1;
        if (z2) {
            z2 = true;
        }
        int i4 = z2 ? 1 : 0;
        int i5 = z2 ? 1 : 0;
        int i6 = (hashCode + i4) * 31;
        boolean z3 = this.isRTX;
        if (!z3) {
            i3 = z3 ? 1 : 0;
        }
        int i7 = (((((i6 + i3) * 31) + this.maxDelay) * 31) + this.retransmitBitrate) * 31;
        RtcpStats rtcpStats = this.rtcpStats;
        int hashCode2 = (i7 + (rtcpStats != null ? rtcpStats.hashCode() : 0)) * 31;
        RtpStats rtpStats = this.rtpStats;
        if (rtpStats != null) {
            i2 = rtpStats.hashCode();
        }
        return ((((b.a(this.ssrc) + ((hashCode2 + i2) * 31)) * 31) + this.totalBitrate) * 31) + this.width;
    }

    public final boolean isFlexFEC() {
        return this.isFlexFEC;
    }

    public final boolean isRTX() {
        return this.isRTX;
    }

    public String toString() {
        StringBuilder R = a.R("Substream(avgDelay=");
        R.append(this.avgDelay);
        R.append(", frameCounts=");
        R.append(this.frameCounts);
        R.append(", height=");
        R.append(this.height);
        R.append(", isFlexFEC=");
        R.append(this.isFlexFEC);
        R.append(", isRTX=");
        R.append(this.isRTX);
        R.append(", maxDelay=");
        R.append(this.maxDelay);
        R.append(", retransmitBitrate=");
        R.append(this.retransmitBitrate);
        R.append(", rtcpStats=");
        R.append(this.rtcpStats);
        R.append(", rtpStats=");
        R.append(this.rtpStats);
        R.append(", ssrc=");
        R.append(this.ssrc);
        R.append(", totalBitrate=");
        R.append(this.totalBitrate);
        R.append(", width=");
        return a.A(R, this.width, ")");
    }
}
