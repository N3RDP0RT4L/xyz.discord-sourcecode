package co.discord.media_engine.internal;

import a0.a.a.b;
import andhook.lib.HookHelper;
import androidx.constraintlayout.solver.widgets.analyzer.BasicMeasure;
import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: NativeStatistics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\n\n\u0002\u0010\u0006\n\u0002\u0018\u0002\n\u0002\b'\n\u0002\u0010\u000b\n\u0002\b-\b\u0080\b\u0018\u00002\u00020\u0001Bé\u0002\u0012\u0006\u00106\u001a\u00020\u0002\u0012\n\u00107\u001a\u00060\u0005j\u0002`\u0006\u0012\n\u00108\u001a\u00060\u0005j\u0002`\u0006\u0012\n\u00109\u001a\u00060\u0005j\u0002`\u0006\u0012\n\u0010:\u001a\u00060\u0005j\u0002`\u0006\u0012\n\u0010;\u001a\u00060\u0005j\u0002`\u0006\u0012\u0006\u0010<\u001a\u00020\u0002\u0012\u0006\u0010=\u001a\u00020\u000e\u0012\n\u0010>\u001a\u00060\u0011j\u0002`\u0012\u0012\n\u0010?\u001a\u00060\u0011j\u0002`\u0012\u0012\n\u0010@\u001a\u00060\u0005j\u0002`\u0006\u0012\n\u0010A\u001a\u00060\u0005j\u0002`\u0006\u0012\n\u0010B\u001a\u00060\u0005j\u0002`\u0006\u0012\n\u0010C\u001a\u00060\u0005j\u0002`\u0006\u0012\n\u0010D\u001a\u00060\u0005j\u0002`\u0006\u0012\n\u0010E\u001a\u00060\u0005j\u0002`\u0006\u0012\n\u0010F\u001a\u00060\u0011j\u0002`\u001c\u0012\n\u0010G\u001a\u00060\u0005j\u0002`\u0006\u0012\n\u0010H\u001a\u00060\u0005j\u0002`\u0006\u0012\u0006\u0010I\u001a\u00020 \u0012\u0006\u0010J\u001a\u00020#\u0012\n\u0010K\u001a\u00060\u0011j\u0002`\u0012\u0012\u0006\u0010L\u001a\u00020'\u0012\u0006\u0010M\u001a\u00020'\u0012\u0006\u0010N\u001a\u00020\u0005\u0012\n\u0010O\u001a\u00060\u0005j\u0002`\u0006\u0012\n\u0010P\u001a\u00060\u0011j\u0002`\u0012\u0012\n\u0010Q\u001a\u00060\u0011j\u0002`\u0012\u0012\n\u0010R\u001a\u00060\u0011j\u0002`\u0012\u0012\n\u0010S\u001a\u00060\u0011j\u0002`\u0012\u0012\n\u0010T\u001a\u00060\u0011j\u0002`\u0012\u0012\n\u0010U\u001a\u000602j\u0002`3¢\u0006\u0006\b\u0086\u0001\u0010\u0087\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0014\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0014\u0010\t\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\t\u0010\bJ\u0014\u0010\n\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\n\u0010\bJ\u0014\u0010\u000b\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u000b\u0010\bJ\u0014\u0010\f\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\f\u0010\bJ\u0010\u0010\r\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\r\u0010\u0004J\u0010\u0010\u000f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0014\u0010\u0013\u001a\u00060\u0011j\u0002`\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0014\u0010\u0015\u001a\u00060\u0011j\u0002`\u0012HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0014J\u0014\u0010\u0016\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0016\u0010\bJ\u0014\u0010\u0017\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0017\u0010\bJ\u0014\u0010\u0018\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0018\u0010\bJ\u0014\u0010\u0019\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0019\u0010\bJ\u0014\u0010\u001a\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u001a\u0010\bJ\u0014\u0010\u001b\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u001b\u0010\bJ\u0014\u0010\u001d\u001a\u00060\u0011j\u0002`\u001cHÆ\u0003¢\u0006\u0004\b\u001d\u0010\u0014J\u0014\u0010\u001e\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u001e\u0010\bJ\u0014\u0010\u001f\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u001f\u0010\bJ\u0010\u0010!\u001a\u00020 HÆ\u0003¢\u0006\u0004\b!\u0010\"J\u0010\u0010$\u001a\u00020#HÆ\u0003¢\u0006\u0004\b$\u0010%J\u0014\u0010&\u001a\u00060\u0011j\u0002`\u0012HÆ\u0003¢\u0006\u0004\b&\u0010\u0014J\u0010\u0010(\u001a\u00020'HÆ\u0003¢\u0006\u0004\b(\u0010)J\u0010\u0010*\u001a\u00020'HÆ\u0003¢\u0006\u0004\b*\u0010)J\u0010\u0010+\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b+\u0010\bJ\u0014\u0010,\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b,\u0010\bJ\u0014\u0010-\u001a\u00060\u0011j\u0002`\u0012HÆ\u0003¢\u0006\u0004\b-\u0010\u0014J\u0014\u0010.\u001a\u00060\u0011j\u0002`\u0012HÆ\u0003¢\u0006\u0004\b.\u0010\u0014J\u0014\u0010/\u001a\u00060\u0011j\u0002`\u0012HÆ\u0003¢\u0006\u0004\b/\u0010\u0014J\u0014\u00100\u001a\u00060\u0011j\u0002`\u0012HÆ\u0003¢\u0006\u0004\b0\u0010\u0014J\u0014\u00101\u001a\u00060\u0011j\u0002`\u0012HÆ\u0003¢\u0006\u0004\b1\u0010\u0014J\u0014\u00104\u001a\u000602j\u0002`3HÆ\u0003¢\u0006\u0004\b4\u00105J°\u0003\u0010V\u001a\u00020\u00002\b\b\u0002\u00106\u001a\u00020\u00022\f\b\u0002\u00107\u001a\u00060\u0005j\u0002`\u00062\f\b\u0002\u00108\u001a\u00060\u0005j\u0002`\u00062\f\b\u0002\u00109\u001a\u00060\u0005j\u0002`\u00062\f\b\u0002\u0010:\u001a\u00060\u0005j\u0002`\u00062\f\b\u0002\u0010;\u001a\u00060\u0005j\u0002`\u00062\b\b\u0002\u0010<\u001a\u00020\u00022\b\b\u0002\u0010=\u001a\u00020\u000e2\f\b\u0002\u0010>\u001a\u00060\u0011j\u0002`\u00122\f\b\u0002\u0010?\u001a\u00060\u0011j\u0002`\u00122\f\b\u0002\u0010@\u001a\u00060\u0005j\u0002`\u00062\f\b\u0002\u0010A\u001a\u00060\u0005j\u0002`\u00062\f\b\u0002\u0010B\u001a\u00060\u0005j\u0002`\u00062\f\b\u0002\u0010C\u001a\u00060\u0005j\u0002`\u00062\f\b\u0002\u0010D\u001a\u00060\u0005j\u0002`\u00062\f\b\u0002\u0010E\u001a\u00060\u0005j\u0002`\u00062\f\b\u0002\u0010F\u001a\u00060\u0011j\u0002`\u001c2\f\b\u0002\u0010G\u001a\u00060\u0005j\u0002`\u00062\f\b\u0002\u0010H\u001a\u00060\u0005j\u0002`\u00062\b\b\u0002\u0010I\u001a\u00020 2\b\b\u0002\u0010J\u001a\u00020#2\f\b\u0002\u0010K\u001a\u00060\u0011j\u0002`\u00122\b\b\u0002\u0010L\u001a\u00020'2\b\b\u0002\u0010M\u001a\u00020'2\b\b\u0002\u0010N\u001a\u00020\u00052\f\b\u0002\u0010O\u001a\u00060\u0005j\u0002`\u00062\f\b\u0002\u0010P\u001a\u00060\u0011j\u0002`\u00122\f\b\u0002\u0010Q\u001a\u00060\u0011j\u0002`\u00122\f\b\u0002\u0010R\u001a\u00060\u0011j\u0002`\u00122\f\b\u0002\u0010S\u001a\u00060\u0011j\u0002`\u00122\f\b\u0002\u0010T\u001a\u00060\u0011j\u0002`\u00122\f\b\u0002\u0010U\u001a\u000602j\u0002`3HÆ\u0001¢\u0006\u0004\bV\u0010WJ\u0010\u0010X\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\bX\u0010\u0004J\u0010\u0010Y\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\bY\u0010\bJ\u001a\u0010\\\u001a\u00020[2\b\u0010Z\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\\\u0010]R\u001d\u0010C\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\bC\u0010^\u001a\u0004\b_\u0010\bR\u001d\u0010S\u001a\u00060\u0011j\u0002`\u00128\u0006@\u0006¢\u0006\f\n\u0004\bS\u0010`\u001a\u0004\ba\u0010\u0014R\u001d\u0010O\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\bO\u0010^\u001a\u0004\bb\u0010\bR\u001d\u0010G\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\bG\u0010^\u001a\u0004\bc\u0010\bR\u0019\u0010L\u001a\u00020'8\u0006@\u0006¢\u0006\f\n\u0004\bL\u0010d\u001a\u0004\be\u0010)R\u0019\u0010M\u001a\u00020'8\u0006@\u0006¢\u0006\f\n\u0004\bM\u0010d\u001a\u0004\bf\u0010)R\u001d\u0010P\u001a\u00060\u0011j\u0002`\u00128\u0006@\u0006¢\u0006\f\n\u0004\bP\u0010`\u001a\u0004\bg\u0010\u0014R\u001d\u0010K\u001a\u00060\u0011j\u0002`\u00128\u0006@\u0006¢\u0006\f\n\u0004\bK\u0010`\u001a\u0004\bh\u0010\u0014R\u0019\u0010N\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\bN\u0010^\u001a\u0004\bi\u0010\bR\u001d\u00109\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010^\u001a\u0004\bj\u0010\bR\u001d\u0010:\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010^\u001a\u0004\bk\u0010\bR\u0019\u0010=\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b=\u0010l\u001a\u0004\bm\u0010\u0010R\u001d\u0010H\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\bH\u0010^\u001a\u0004\bn\u0010\bR\u001d\u00107\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010^\u001a\u0004\bo\u0010\bR\u001d\u0010U\u001a\u000602j\u0002`38\u0006@\u0006¢\u0006\f\n\u0004\bU\u0010p\u001a\u0004\bq\u00105R\u0019\u0010<\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b<\u0010r\u001a\u0004\bs\u0010\u0004R\u001d\u0010?\u001a\u00060\u0011j\u0002`\u00128\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010`\u001a\u0004\bt\u0010\u0014R\u001d\u0010A\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\bA\u0010^\u001a\u0004\bu\u0010\bR\u001d\u0010>\u001a\u00060\u0011j\u0002`\u00128\u0006@\u0006¢\u0006\f\n\u0004\b>\u0010`\u001a\u0004\bv\u0010\u0014R\u0019\u0010I\u001a\u00020 8\u0006@\u0006¢\u0006\f\n\u0004\bI\u0010w\u001a\u0004\bx\u0010\"R\u001d\u0010E\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\bE\u0010^\u001a\u0004\by\u0010\bR\u001d\u0010F\u001a\u00060\u0011j\u0002`\u001c8\u0006@\u0006¢\u0006\f\n\u0004\bF\u0010`\u001a\u0004\bz\u0010\u0014R\u001d\u0010B\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010^\u001a\u0004\b{\u0010\bR\u001d\u0010@\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b@\u0010^\u001a\u0004\b|\u0010\bR\u001d\u0010R\u001a\u00060\u0011j\u0002`\u00128\u0006@\u0006¢\u0006\f\n\u0004\bR\u0010`\u001a\u0004\b}\u0010\u0014R\u001d\u0010D\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\bD\u0010^\u001a\u0004\b~\u0010\bR\u001d\u00108\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010^\u001a\u0004\b\u007f\u0010\bR\u001a\u00106\u001a\u00020\u00028\u0006@\u0006¢\u0006\r\n\u0004\b6\u0010r\u001a\u0005\b\u0080\u0001\u0010\u0004R\u001b\u0010J\u001a\u00020#8\u0006@\u0006¢\u0006\u000e\n\u0005\bJ\u0010\u0081\u0001\u001a\u0005\b\u0082\u0001\u0010%R\u001e\u0010T\u001a\u00060\u0011j\u0002`\u00128\u0006@\u0006¢\u0006\r\n\u0004\bT\u0010`\u001a\u0005\b\u0083\u0001\u0010\u0014R\u001e\u0010;\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\r\n\u0004\b;\u0010^\u001a\u0005\b\u0084\u0001\u0010\bR\u001e\u0010Q\u001a\u00060\u0011j\u0002`\u00128\u0006@\u0006¢\u0006\r\n\u0004\bQ\u0010`\u001a\u0005\b\u0085\u0001\u0010\u0014¨\u0006\u0088\u0001"}, d2 = {"Lco/discord/media_engine/internal/InboundVideo;", "", "", "component1", "()Ljava/lang/String;", "", "Lco/discord/media_engine/internal/I32;", "component2", "()I", "component3", "component4", "component5", "component6", "component7", "Lco/discord/media_engine/internal/FrameCounts;", "component8", "()Lco/discord/media_engine/internal/FrameCounts;", "", "Lco/discord/media_engine/internal/U32;", "component9", "()J", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "Lco/discord/media_engine/internal/I64;", "component17", "component18", "component19", "Lco/discord/media_engine/internal/InboundRtcpStats;", "component20", "()Lco/discord/media_engine/internal/InboundRtcpStats;", "Lco/discord/media_engine/internal/InboundRtpStats;", "component21", "()Lco/discord/media_engine/internal/InboundRtpStats;", "component22", "", "component23", "()F", "component24", "component25", "component26", "component27", "component28", "component29", "component30", "component31", "", "Lco/discord/media_engine/internal/F64;", "component32", "()D", "codecName", "codecPayloadType", "currentDelay", "currentPayloadType", "decodeFrameRate", "decode", "decoderImplementationName", "frameCounts", "framesDecoded", "framesRendered", "framesDropped", "height", "jitterBuffer", "maxDecode", "minPlayoutDelay", "networkFrameRate", "qpSum", "renderDelay", "renderFrameRate", "rtcpStats", "rtpStats", "ssrc", "syncOffset", "targetDelay", "totalBitrate", "width", "freezeCount", "pauseCount", "totalFreezesDuration", "totalPausesDuration", "totalFramesDuration", "sumOfSquaredFramesDurations", "copy", "(Ljava/lang/String;IIIIILjava/lang/String;Lco/discord/media_engine/internal/FrameCounts;JJIIIIIIJIILco/discord/media_engine/internal/InboundRtcpStats;Lco/discord/media_engine/internal/InboundRtpStats;JFFIIJJJJJD)Lco/discord/media_engine/internal/InboundVideo;", "toString", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getMaxDecode", "J", "getTotalPausesDuration", "getWidth", "getRenderDelay", "F", "getSyncOffset", "getTargetDelay", "getFreezeCount", "getSsrc", "getTotalBitrate", "getCurrentPayloadType", "getDecodeFrameRate", "Lco/discord/media_engine/internal/FrameCounts;", "getFrameCounts", "getRenderFrameRate", "getCodecPayloadType", "D", "getSumOfSquaredFramesDurations", "Ljava/lang/String;", "getDecoderImplementationName", "getFramesRendered", "getHeight", "getFramesDecoded", "Lco/discord/media_engine/internal/InboundRtcpStats;", "getRtcpStats", "getNetworkFrameRate", "getQpSum", "getJitterBuffer", "getFramesDropped", "getTotalFreezesDuration", "getMinPlayoutDelay", "getCurrentDelay", "getCodecName", "Lco/discord/media_engine/internal/InboundRtpStats;", "getRtpStats", "getTotalFramesDuration", "getDecode", "getPauseCount", HookHelper.constructorName, "(Ljava/lang/String;IIIIILjava/lang/String;Lco/discord/media_engine/internal/FrameCounts;JJIIIIIIJIILco/discord/media_engine/internal/InboundRtcpStats;Lco/discord/media_engine/internal/InboundRtpStats;JFFIIJJJJJD)V", "android_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class InboundVideo {
    private final String codecName;
    private final int codecPayloadType;
    private final int currentDelay;
    private final int currentPayloadType;
    private final int decode;
    private final int decodeFrameRate;
    private final String decoderImplementationName;
    private final FrameCounts frameCounts;
    private final long framesDecoded;
    private final int framesDropped;
    private final long framesRendered;
    private final long freezeCount;
    private final int height;
    private final int jitterBuffer;
    private final int maxDecode;
    private final int minPlayoutDelay;
    private final int networkFrameRate;
    private final long pauseCount;
    private final long qpSum;
    private final int renderDelay;
    private final int renderFrameRate;
    private final InboundRtcpStats rtcpStats;
    private final InboundRtpStats rtpStats;
    private final long ssrc;
    private final double sumOfSquaredFramesDurations;
    private final float syncOffset;
    private final float targetDelay;
    private final int totalBitrate;
    private final long totalFramesDuration;
    private final long totalFreezesDuration;
    private final long totalPausesDuration;
    private final int width;

    public InboundVideo(String str, int i, int i2, int i3, int i4, int i5, String str2, FrameCounts frameCounts, long j, long j2, int i6, int i7, int i8, int i9, int i10, int i11, long j3, int i12, int i13, InboundRtcpStats inboundRtcpStats, InboundRtpStats inboundRtpStats, long j4, float f, float f2, int i14, int i15, long j5, long j6, long j7, long j8, long j9, double d) {
        m.checkNotNullParameter(str, "codecName");
        m.checkNotNullParameter(str2, "decoderImplementationName");
        m.checkNotNullParameter(frameCounts, "frameCounts");
        m.checkNotNullParameter(inboundRtcpStats, "rtcpStats");
        m.checkNotNullParameter(inboundRtpStats, "rtpStats");
        this.codecName = str;
        this.codecPayloadType = i;
        this.currentDelay = i2;
        this.currentPayloadType = i3;
        this.decodeFrameRate = i4;
        this.decode = i5;
        this.decoderImplementationName = str2;
        this.frameCounts = frameCounts;
        this.framesDecoded = j;
        this.framesRendered = j2;
        this.framesDropped = i6;
        this.height = i7;
        this.jitterBuffer = i8;
        this.maxDecode = i9;
        this.minPlayoutDelay = i10;
        this.networkFrameRate = i11;
        this.qpSum = j3;
        this.renderDelay = i12;
        this.renderFrameRate = i13;
        this.rtcpStats = inboundRtcpStats;
        this.rtpStats = inboundRtpStats;
        this.ssrc = j4;
        this.syncOffset = f;
        this.targetDelay = f2;
        this.totalBitrate = i14;
        this.width = i15;
        this.freezeCount = j5;
        this.pauseCount = j6;
        this.totalFreezesDuration = j7;
        this.totalPausesDuration = j8;
        this.totalFramesDuration = j9;
        this.sumOfSquaredFramesDurations = d;
    }

    public static /* synthetic */ InboundVideo copy$default(InboundVideo inboundVideo, String str, int i, int i2, int i3, int i4, int i5, String str2, FrameCounts frameCounts, long j, long j2, int i6, int i7, int i8, int i9, int i10, int i11, long j3, int i12, int i13, InboundRtcpStats inboundRtcpStats, InboundRtpStats inboundRtpStats, long j4, float f, float f2, int i14, int i15, long j5, long j6, long j7, long j8, long j9, double d, int i16, Object obj) {
        String str3 = (i16 & 1) != 0 ? inboundVideo.codecName : str;
        int i17 = (i16 & 2) != 0 ? inboundVideo.codecPayloadType : i;
        int i18 = (i16 & 4) != 0 ? inboundVideo.currentDelay : i2;
        int i19 = (i16 & 8) != 0 ? inboundVideo.currentPayloadType : i3;
        int i20 = (i16 & 16) != 0 ? inboundVideo.decodeFrameRate : i4;
        int i21 = (i16 & 32) != 0 ? inboundVideo.decode : i5;
        String str4 = (i16 & 64) != 0 ? inboundVideo.decoderImplementationName : str2;
        FrameCounts frameCounts2 = (i16 & 128) != 0 ? inboundVideo.frameCounts : frameCounts;
        long j10 = (i16 & 256) != 0 ? inboundVideo.framesDecoded : j;
        long j11 = (i16 & 512) != 0 ? inboundVideo.framesRendered : j2;
        int i22 = (i16 & 1024) != 0 ? inboundVideo.framesDropped : i6;
        int i23 = (i16 & 2048) != 0 ? inboundVideo.height : i7;
        int i24 = (i16 & 4096) != 0 ? inboundVideo.jitterBuffer : i8;
        int i25 = (i16 & 8192) != 0 ? inboundVideo.maxDecode : i9;
        int i26 = (i16 & 16384) != 0 ? inboundVideo.minPlayoutDelay : i10;
        int i27 = i22;
        int i28 = (i16 & 32768) != 0 ? inboundVideo.networkFrameRate : i11;
        long j12 = (i16 & 65536) != 0 ? inboundVideo.qpSum : j3;
        int i29 = (i16 & 131072) != 0 ? inboundVideo.renderDelay : i12;
        return inboundVideo.copy(str3, i17, i18, i19, i20, i21, str4, frameCounts2, j10, j11, i27, i23, i24, i25, i26, i28, j12, i29, (262144 & i16) != 0 ? inboundVideo.renderFrameRate : i13, (i16 & 524288) != 0 ? inboundVideo.rtcpStats : inboundRtcpStats, (i16 & 1048576) != 0 ? inboundVideo.rtpStats : inboundRtpStats, (i16 & 2097152) != 0 ? inboundVideo.ssrc : j4, (i16 & 4194304) != 0 ? inboundVideo.syncOffset : f, (8388608 & i16) != 0 ? inboundVideo.targetDelay : f2, (i16 & 16777216) != 0 ? inboundVideo.totalBitrate : i14, (i16 & 33554432) != 0 ? inboundVideo.width : i15, (i16 & 67108864) != 0 ? inboundVideo.freezeCount : j5, (i16 & 134217728) != 0 ? inboundVideo.pauseCount : j6, (i16 & 268435456) != 0 ? inboundVideo.totalFreezesDuration : j7, (i16 & 536870912) != 0 ? inboundVideo.totalPausesDuration : j8, (i16 & BasicMeasure.EXACTLY) != 0 ? inboundVideo.totalFramesDuration : j9, (i16 & Integer.MIN_VALUE) != 0 ? inboundVideo.sumOfSquaredFramesDurations : d);
    }

    public final String component1() {
        return this.codecName;
    }

    public final long component10() {
        return this.framesRendered;
    }

    public final int component11() {
        return this.framesDropped;
    }

    public final int component12() {
        return this.height;
    }

    public final int component13() {
        return this.jitterBuffer;
    }

    public final int component14() {
        return this.maxDecode;
    }

    public final int component15() {
        return this.minPlayoutDelay;
    }

    public final int component16() {
        return this.networkFrameRate;
    }

    public final long component17() {
        return this.qpSum;
    }

    public final int component18() {
        return this.renderDelay;
    }

    public final int component19() {
        return this.renderFrameRate;
    }

    public final int component2() {
        return this.codecPayloadType;
    }

    public final InboundRtcpStats component20() {
        return this.rtcpStats;
    }

    public final InboundRtpStats component21() {
        return this.rtpStats;
    }

    public final long component22() {
        return this.ssrc;
    }

    public final float component23() {
        return this.syncOffset;
    }

    public final float component24() {
        return this.targetDelay;
    }

    public final int component25() {
        return this.totalBitrate;
    }

    public final int component26() {
        return this.width;
    }

    public final long component27() {
        return this.freezeCount;
    }

    public final long component28() {
        return this.pauseCount;
    }

    public final long component29() {
        return this.totalFreezesDuration;
    }

    public final int component3() {
        return this.currentDelay;
    }

    public final long component30() {
        return this.totalPausesDuration;
    }

    public final long component31() {
        return this.totalFramesDuration;
    }

    public final double component32() {
        return this.sumOfSquaredFramesDurations;
    }

    public final int component4() {
        return this.currentPayloadType;
    }

    public final int component5() {
        return this.decodeFrameRate;
    }

    public final int component6() {
        return this.decode;
    }

    public final String component7() {
        return this.decoderImplementationName;
    }

    public final FrameCounts component8() {
        return this.frameCounts;
    }

    public final long component9() {
        return this.framesDecoded;
    }

    public final InboundVideo copy(String str, int i, int i2, int i3, int i4, int i5, String str2, FrameCounts frameCounts, long j, long j2, int i6, int i7, int i8, int i9, int i10, int i11, long j3, int i12, int i13, InboundRtcpStats inboundRtcpStats, InboundRtpStats inboundRtpStats, long j4, float f, float f2, int i14, int i15, long j5, long j6, long j7, long j8, long j9, double d) {
        m.checkNotNullParameter(str, "codecName");
        m.checkNotNullParameter(str2, "decoderImplementationName");
        m.checkNotNullParameter(frameCounts, "frameCounts");
        m.checkNotNullParameter(inboundRtcpStats, "rtcpStats");
        m.checkNotNullParameter(inboundRtpStats, "rtpStats");
        return new InboundVideo(str, i, i2, i3, i4, i5, str2, frameCounts, j, j2, i6, i7, i8, i9, i10, i11, j3, i12, i13, inboundRtcpStats, inboundRtpStats, j4, f, f2, i14, i15, j5, j6, j7, j8, j9, d);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof InboundVideo)) {
            return false;
        }
        InboundVideo inboundVideo = (InboundVideo) obj;
        return m.areEqual(this.codecName, inboundVideo.codecName) && this.codecPayloadType == inboundVideo.codecPayloadType && this.currentDelay == inboundVideo.currentDelay && this.currentPayloadType == inboundVideo.currentPayloadType && this.decodeFrameRate == inboundVideo.decodeFrameRate && this.decode == inboundVideo.decode && m.areEqual(this.decoderImplementationName, inboundVideo.decoderImplementationName) && m.areEqual(this.frameCounts, inboundVideo.frameCounts) && this.framesDecoded == inboundVideo.framesDecoded && this.framesRendered == inboundVideo.framesRendered && this.framesDropped == inboundVideo.framesDropped && this.height == inboundVideo.height && this.jitterBuffer == inboundVideo.jitterBuffer && this.maxDecode == inboundVideo.maxDecode && this.minPlayoutDelay == inboundVideo.minPlayoutDelay && this.networkFrameRate == inboundVideo.networkFrameRate && this.qpSum == inboundVideo.qpSum && this.renderDelay == inboundVideo.renderDelay && this.renderFrameRate == inboundVideo.renderFrameRate && m.areEqual(this.rtcpStats, inboundVideo.rtcpStats) && m.areEqual(this.rtpStats, inboundVideo.rtpStats) && this.ssrc == inboundVideo.ssrc && Float.compare(this.syncOffset, inboundVideo.syncOffset) == 0 && Float.compare(this.targetDelay, inboundVideo.targetDelay) == 0 && this.totalBitrate == inboundVideo.totalBitrate && this.width == inboundVideo.width && this.freezeCount == inboundVideo.freezeCount && this.pauseCount == inboundVideo.pauseCount && this.totalFreezesDuration == inboundVideo.totalFreezesDuration && this.totalPausesDuration == inboundVideo.totalPausesDuration && this.totalFramesDuration == inboundVideo.totalFramesDuration && Double.compare(this.sumOfSquaredFramesDurations, inboundVideo.sumOfSquaredFramesDurations) == 0;
    }

    public final String getCodecName() {
        return this.codecName;
    }

    public final int getCodecPayloadType() {
        return this.codecPayloadType;
    }

    public final int getCurrentDelay() {
        return this.currentDelay;
    }

    public final int getCurrentPayloadType() {
        return this.currentPayloadType;
    }

    public final int getDecode() {
        return this.decode;
    }

    public final int getDecodeFrameRate() {
        return this.decodeFrameRate;
    }

    public final String getDecoderImplementationName() {
        return this.decoderImplementationName;
    }

    public final FrameCounts getFrameCounts() {
        return this.frameCounts;
    }

    public final long getFramesDecoded() {
        return this.framesDecoded;
    }

    public final int getFramesDropped() {
        return this.framesDropped;
    }

    public final long getFramesRendered() {
        return this.framesRendered;
    }

    public final long getFreezeCount() {
        return this.freezeCount;
    }

    public final int getHeight() {
        return this.height;
    }

    public final int getJitterBuffer() {
        return this.jitterBuffer;
    }

    public final int getMaxDecode() {
        return this.maxDecode;
    }

    public final int getMinPlayoutDelay() {
        return this.minPlayoutDelay;
    }

    public final int getNetworkFrameRate() {
        return this.networkFrameRate;
    }

    public final long getPauseCount() {
        return this.pauseCount;
    }

    public final long getQpSum() {
        return this.qpSum;
    }

    public final int getRenderDelay() {
        return this.renderDelay;
    }

    public final int getRenderFrameRate() {
        return this.renderFrameRate;
    }

    public final InboundRtcpStats getRtcpStats() {
        return this.rtcpStats;
    }

    public final InboundRtpStats getRtpStats() {
        return this.rtpStats;
    }

    public final long getSsrc() {
        return this.ssrc;
    }

    public final double getSumOfSquaredFramesDurations() {
        return this.sumOfSquaredFramesDurations;
    }

    public final float getSyncOffset() {
        return this.syncOffset;
    }

    public final float getTargetDelay() {
        return this.targetDelay;
    }

    public final int getTotalBitrate() {
        return this.totalBitrate;
    }

    public final long getTotalFramesDuration() {
        return this.totalFramesDuration;
    }

    public final long getTotalFreezesDuration() {
        return this.totalFreezesDuration;
    }

    public final long getTotalPausesDuration() {
        return this.totalPausesDuration;
    }

    public final int getWidth() {
        return this.width;
    }

    public int hashCode() {
        String str = this.codecName;
        int i = 0;
        int hashCode = (((((((((((str != null ? str.hashCode() : 0) * 31) + this.codecPayloadType) * 31) + this.currentDelay) * 31) + this.currentPayloadType) * 31) + this.decodeFrameRate) * 31) + this.decode) * 31;
        String str2 = this.decoderImplementationName;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        FrameCounts frameCounts = this.frameCounts;
        int hashCode3 = frameCounts != null ? frameCounts.hashCode() : 0;
        int a = b.a(this.framesDecoded);
        int a2 = (((((b.a(this.qpSum) + ((((((((((((((b.a(this.framesRendered) + ((a + ((hashCode2 + hashCode3) * 31)) * 31)) * 31) + this.framesDropped) * 31) + this.height) * 31) + this.jitterBuffer) * 31) + this.maxDecode) * 31) + this.minPlayoutDelay) * 31) + this.networkFrameRate) * 31)) * 31) + this.renderDelay) * 31) + this.renderFrameRate) * 31;
        InboundRtcpStats inboundRtcpStats = this.rtcpStats;
        int hashCode4 = (a2 + (inboundRtcpStats != null ? inboundRtcpStats.hashCode() : 0)) * 31;
        InboundRtpStats inboundRtpStats = this.rtpStats;
        if (inboundRtpStats != null) {
            i = inboundRtpStats.hashCode();
        }
        int a3 = b.a(this.ssrc);
        int floatToIntBits = Float.floatToIntBits(this.syncOffset);
        int floatToIntBits2 = Float.floatToIntBits(this.targetDelay);
        int a4 = b.a(this.freezeCount);
        int a5 = b.a(this.pauseCount);
        int a6 = b.a(this.totalFreezesDuration);
        int a7 = b.a(this.totalPausesDuration);
        return Double.doubleToLongBits(this.sumOfSquaredFramesDurations) + ((b.a(this.totalFramesDuration) + ((a7 + ((a6 + ((a5 + ((a4 + ((((((floatToIntBits2 + ((floatToIntBits + ((a3 + ((hashCode4 + i) * 31)) * 31)) * 31)) * 31) + this.totalBitrate) * 31) + this.width) * 31)) * 31)) * 31)) * 31)) * 31)) * 31);
    }

    public String toString() {
        StringBuilder R = a.R("InboundVideo(codecName=");
        R.append(this.codecName);
        R.append(", codecPayloadType=");
        R.append(this.codecPayloadType);
        R.append(", currentDelay=");
        R.append(this.currentDelay);
        R.append(", currentPayloadType=");
        R.append(this.currentPayloadType);
        R.append(", decodeFrameRate=");
        R.append(this.decodeFrameRate);
        R.append(", decode=");
        R.append(this.decode);
        R.append(", decoderImplementationName=");
        R.append(this.decoderImplementationName);
        R.append(", frameCounts=");
        R.append(this.frameCounts);
        R.append(", framesDecoded=");
        R.append(this.framesDecoded);
        R.append(", framesRendered=");
        R.append(this.framesRendered);
        R.append(", framesDropped=");
        R.append(this.framesDropped);
        R.append(", height=");
        R.append(this.height);
        R.append(", jitterBuffer=");
        R.append(this.jitterBuffer);
        R.append(", maxDecode=");
        R.append(this.maxDecode);
        R.append(", minPlayoutDelay=");
        R.append(this.minPlayoutDelay);
        R.append(", networkFrameRate=");
        R.append(this.networkFrameRate);
        R.append(", qpSum=");
        R.append(this.qpSum);
        R.append(", renderDelay=");
        R.append(this.renderDelay);
        R.append(", renderFrameRate=");
        R.append(this.renderFrameRate);
        R.append(", rtcpStats=");
        R.append(this.rtcpStats);
        R.append(", rtpStats=");
        R.append(this.rtpStats);
        R.append(", ssrc=");
        R.append(this.ssrc);
        R.append(", syncOffset=");
        R.append(this.syncOffset);
        R.append(", targetDelay=");
        R.append(this.targetDelay);
        R.append(", totalBitrate=");
        R.append(this.totalBitrate);
        R.append(", width=");
        R.append(this.width);
        R.append(", freezeCount=");
        R.append(this.freezeCount);
        R.append(", pauseCount=");
        R.append(this.pauseCount);
        R.append(", totalFreezesDuration=");
        R.append(this.totalFreezesDuration);
        R.append(", totalPausesDuration=");
        R.append(this.totalPausesDuration);
        R.append(", totalFramesDuration=");
        R.append(this.totalFramesDuration);
        R.append(", sumOfSquaredFramesDurations=");
        R.append(this.sumOfSquaredFramesDurations);
        R.append(")");
        return R.toString();
    }
}
