package co.discord.media_engine.internal;

import andhook.lib.HookHelper;
import b.i.a.f.e.o.f;
import co.discord.media_engine.InboundRtpAudio;
import co.discord.media_engine.InboundRtpVideo;
import co.discord.media_engine.OutboundRtpAudio;
import co.discord.media_engine.OutboundRtpVideo;
import co.discord.media_engine.PlayoutMetric;
import co.discord.media_engine.Resolution;
import co.discord.media_engine.Stats;
import co.discord.media_engine.StatsCodec;
import co.discord.media_engine.Transport;
import com.google.gson.Gson;
import d0.t.k;
import d0.z.d.m;
import java.util.LinkedHashMap;
import kotlin.Metadata;
import org.webrtc.MediaStreamTrack;
/* compiled from: TransformStats.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\bÀ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0007\u0010\u0006J\u0017\u0010\n\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u000e\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\fH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0011\u001a\u00020\u0010H\u0007¢\u0006\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0016\u001a\u00020\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017¨\u0006\u001a"}, d2 = {"Lco/discord/media_engine/internal/TransformStats;", "", "Lco/discord/media_engine/internal/RtpStats;", "rtpStats", "", "sumBytes", "(Lco/discord/media_engine/internal/RtpStats;)J", "sumPackets", "Lco/discord/media_engine/PlayoutMetric;", "metric", "convertPlayoutMetricToMs", "(Lco/discord/media_engine/PlayoutMetric;)Lco/discord/media_engine/PlayoutMetric;", "Lco/discord/media_engine/internal/InboundPlayout;", "playout", "transformPlayoutStats", "(Lco/discord/media_engine/internal/InboundPlayout;)Lco/discord/media_engine/internal/InboundPlayout;", "", "stats", "Lco/discord/media_engine/Stats;", "transform", "(Ljava/lang/String;)Lco/discord/media_engine/Stats;", "Lcom/google/gson/Gson;", "gson", "Lcom/google/gson/Gson;", HookHelper.constructorName, "()V", "android_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TransformStats {
    public static final TransformStats INSTANCE = new TransformStats();
    private static final Gson gson = new Gson();

    private TransformStats() {
    }

    private final PlayoutMetric convertPlayoutMetricToMs(PlayoutMetric playoutMetric) {
        double d = 1000;
        return new PlayoutMetric(Math.rint(playoutMetric.getLast() * d), Math.rint(playoutMetric.getMean() * d), Math.rint(playoutMetric.getP75() * d), Math.rint(playoutMetric.getP95() * d), Math.rint(playoutMetric.getP99() * d), Math.rint(playoutMetric.getMax() * d));
    }

    private final long sumBytes(RtpStats rtpStats) {
        return rtpStats.getTransmitted().getPaddingBytes() + rtpStats.getTransmitted().getPayloadBytes() + rtpStats.getTransmitted().getHeaderBytes() + rtpStats.getRetransmitted().getPaddingBytes() + rtpStats.getRetransmitted().getPayloadBytes() + rtpStats.getRetransmitted().getHeaderBytes() + rtpStats.getFec().getPaddingBytes() + rtpStats.getFec().getPayloadBytes() + rtpStats.getFec().getHeaderBytes();
    }

    private final long sumPackets(RtpStats rtpStats) {
        return rtpStats.getTransmitted().getPackets() + rtpStats.getRetransmitted().getPackets() + rtpStats.getFec().getPackets();
    }

    public static final Stats transform(String str) {
        OutboundRtpAudio outboundRtpAudio;
        long j;
        Substream substream;
        long j2;
        OutboundRtpVideo outboundRtpVideo;
        long j3;
        Transport transport;
        Substream[] substreams;
        OutboundVideo[] videos;
        OutboundAudio audio;
        m.checkNotNullParameter(str, "stats");
        NativeStats nativeStats = (NativeStats) f.E1(NativeStats.class).cast(gson.g(str, NativeStats.class));
        Outbound outbound = nativeStats.getOutbound();
        if (outbound == null || (audio = outbound.getAudio()) == null) {
            j = 0;
            outboundRtpAudio = null;
        } else {
            j = audio.getBytesSent() + 0;
            long ssrc = audio.getSsrc();
            StatsCodec statsCodec = new StatsCodec(audio.getCodecPayloadType(), audio.getCodecName());
            long packetsSent = audio.getPacketsSent();
            int max = Math.max(0, audio.getPacketsLost());
            float fractionLost = audio.getFractionLost() * 100;
            float audioLevel = audio.getAudioLevel() / 32768.0f;
            boolean z2 = audio.getSpeaking() > 0;
            long framesCaptured = audio.getFramesCaptured();
            long framesRendered = audio.getFramesRendered();
            Boolean noiseCancellerIsEnabled = audio.getNoiseCancellerIsEnabled();
            boolean booleanValue = noiseCancellerIsEnabled != null ? noiseCancellerIsEnabled.booleanValue() : false;
            Long noiseCancellerProcessTime = audio.getNoiseCancellerProcessTime();
            long longValue = noiseCancellerProcessTime != null ? noiseCancellerProcessTime.longValue() : 0L;
            Boolean voiceActivityDetectorIsEnabled = audio.getVoiceActivityDetectorIsEnabled();
            boolean booleanValue2 = voiceActivityDetectorIsEnabled != null ? voiceActivityDetectorIsEnabled.booleanValue() : false;
            Long voiceActivityDetectorProcessTime = audio.getVoiceActivityDetectorProcessTime();
            outboundRtpAudio = new OutboundRtpAudio(MediaStreamTrack.AUDIO_TRACK_KIND, ssrc, statsCodec, j, packetsSent, max, fractionLost, audioLevel, z2, framesCaptured, framesRendered, booleanValue, longValue, booleanValue2, voiceActivityDetectorProcessTime != null ? voiceActivityDetectorProcessTime.longValue() : 0L);
        }
        Outbound outbound2 = nativeStats.getOutbound();
        OutboundVideo outboundVideo = (outbound2 == null || (videos = outbound2.getVideos()) == null) ? null : (OutboundVideo) k.firstOrNull(videos);
        if (!(outboundVideo == null || (substreams = outboundVideo.getSubstreams()) == null)) {
            int length = substreams.length;
            for (int i = 0; i < length; i++) {
                substream = substreams[i];
                if (!substream.isFlexFEC() && !substream.isRTX()) {
                    break;
                }
            }
        }
        substream = null;
        if (substream != null) {
            long j4 = 0;
            for (Substream substream2 : outboundVideo.getSubstreams()) {
                j4 += INSTANCE.sumBytes(substream2.getRtpStats());
            }
            j2 = j + j4;
            long ssrc2 = substream.getSsrc();
            StatsCodec statsCodec2 = new StatsCodec(outboundVideo.getCodecPayloadType(), outboundVideo.getCodecName());
            long j5 = 0;
            for (Substream substream3 : outboundVideo.getSubstreams()) {
                j5 += INSTANCE.sumPackets(substream3.getRtpStats());
            }
            outboundRtpVideo = new OutboundRtpVideo(MediaStreamTrack.VIDEO_TRACK_KIND, ssrc2, statsCodec2, j4, j5, substream.getRtcpStats().getPacketsLost(), substream.getRtcpStats().getFractionLost(), outboundVideo.getMediaBitrate(), outboundVideo.getTargetMediaBitrate(), outboundVideo.getEncodeUsage(), outboundVideo.getEncoderImplementationName(), outboundVideo.getAvgEncodeTime(), new Resolution(substream.getWidth(), substream.getHeight()), substream.getFrameCounts().getKeyFrames() + substream.getFrameCounts().getDeltaFrames(), outboundVideo.getFramesEncoded(), outboundVideo.getInputFrameRate(), outboundVideo.getEncodeFrameRate(), substream.getRtcpStats().getFirPackets(), substream.getRtcpStats().getNackPackets(), substream.getRtcpStats().getPliPackets(), outboundVideo.getQpSum(), outboundVideo.getBwLimitedResolution(), outboundVideo.getCpuLimitedResolution());
        } else {
            j2 = j;
            outboundRtpVideo = null;
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        Inbound[] inbound = nativeStats.getInbound();
        if (inbound != null) {
            long j6 = 0;
            for (Inbound inbound2 : inbound) {
                String id2 = inbound2.getId();
                long bytesReceived = inbound2.getAudio().getBytesReceived() + j6;
                InboundAudio audio2 = inbound2.getAudio();
                InboundPlayout transformPlayoutStats = inbound2.getPlayout() != null ? INSTANCE.transformPlayoutStats(inbound2.getPlayout()) : null;
                InboundRtpAudio inboundRtpAudio = (InboundRtpAudio) linkedHashMap.put(id2, new InboundRtpAudio(MediaStreamTrack.AUDIO_TRACK_KIND, audio2.getSsrc(), new StatsCodec(audio2.getCodecPayloadType(), audio2.getCodecName()), bytesReceived, audio2.getPacketsReceived(), audio2.getPacketsLost(), audio2.getAudioLevel() / 32768.0f, audio2.getSpeaking() > 0, audio2.getJitter(), audio2.getJitterBuffer(), audio2.getJitterBufferPreferred(), audio2.getDelayEstimate(), audio2.getDecodingCNG(), audio2.getDecodingMutedOutput(), audio2.getDecodingNormal(), audio2.getDecodingPLC(), audio2.getDecodingPLCCNG(), Long.valueOf(audio2.getOpSilence()), Long.valueOf(audio2.getOpNormal()), Long.valueOf(audio2.getOpMerge()), Long.valueOf(audio2.getOpExpand()), Long.valueOf(audio2.getOpAccelerate()), Long.valueOf(audio2.getOpPreemptiveExpand()), Long.valueOf(audio2.getOpCNG()), transformPlayoutStats != null ? transformPlayoutStats.getAudioJitterBuffer() : null, transformPlayoutStats != null ? transformPlayoutStats.getAudioJitterDelay() : null, transformPlayoutStats != null ? transformPlayoutStats.getAudioJitterTarget() : null, transformPlayoutStats != null ? transformPlayoutStats.getAudioPlayoutUnderruns() : null, transformPlayoutStats != null ? transformPlayoutStats.getRelativeReceptionDelay() : null, transformPlayoutStats != null ? transformPlayoutStats.getRelativePlayoutDelay() : null));
                if (inbound2.getVideo() != null) {
                    InboundVideo video = inbound2.getVideo();
                    long payloadBytes = video.getRtpStats().getPayloadBytes() + video.getRtpStats().getPaddingBytes() + video.getRtpStats().getHeaderBytes();
                    bytesReceived += payloadBytes;
                    InboundRtpVideo inboundRtpVideo = (InboundRtpVideo) linkedHashMap2.put(id2, new InboundRtpVideo(MediaStreamTrack.VIDEO_TRACK_KIND, video.getSsrc(), video.getCodecPayloadType() != -1 ? new StatsCodec(video.getCodecPayloadType(), video.getCodecName()) : null, payloadBytes, (int) video.getRtpStats().getPackets(), video.getRtpStats().getPacketsLost(), video.getTotalBitrate(), video.getDecode(), new Resolution(video.getWidth(), video.getHeight()), video.getDecoderImplementationName(), (int) video.getFramesDecoded(), video.getFramesDropped(), video.getFrameCounts().getKeyFrames() + video.getFrameCounts().getDeltaFrames(), video.getDecodeFrameRate(), video.getNetworkFrameRate(), video.getRenderFrameRate(), video.getRtcpStats().getFirPackets(), video.getRtcpStats().getNackPackets(), video.getRtcpStats().getPliPackets(), video.getQpSum(), video.getFreezeCount(), video.getPauseCount(), video.getTotalFreezesDuration(), video.getTotalPausesDuration(), video.getTotalFramesDuration(), video.getSumOfSquaredFramesDurations()));
                }
                j6 = bytesReceived;
            }
            j3 = j6;
        } else {
            j3 = 0;
        }
        Transport transport2 = nativeStats.getTransport();
        if (transport2 != null) {
            transport = new Transport(transport2.getSendBandwidth(), j3, j2, transport2.getRtt(), Integer.valueOf((int) transport2.getDecryptionFailures()), transport2.getLocalAddress(), transport2.getReceiverReports());
        } else {
            transport = new Transport(0, j3, j2, 0, 0, "", null);
        }
        return new Stats(transport, outboundRtpAudio, outboundRtpVideo, linkedHashMap, linkedHashMap2);
    }

    private final InboundPlayout transformPlayoutStats(InboundPlayout inboundPlayout) {
        return new InboundPlayout(convertPlayoutMetricToMs(inboundPlayout.getAudioJitterBuffer()), convertPlayoutMetricToMs(inboundPlayout.getAudioJitterDelay()), convertPlayoutMetricToMs(inboundPlayout.getAudioJitterTarget()), convertPlayoutMetricToMs(inboundPlayout.getAudioPlayoutUnderruns()), convertPlayoutMetricToMs(inboundPlayout.getAudioCaptureOverruns()), convertPlayoutMetricToMs(inboundPlayout.getVideoJitterBuffer()), convertPlayoutMetricToMs(inboundPlayout.getVideoJitterDelay()), convertPlayoutMetricToMs(inboundPlayout.getVideoJitterTarget()), convertPlayoutMetricToMs(inboundPlayout.getRelativeReceptionDelay()), convertPlayoutMetricToMs(inboundPlayout.getRelativePlayoutDelay()));
    }
}
