package co.discord.media_engine.internal;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
import org.webrtc.MediaStreamTrack;
/* compiled from: NativeStatistics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\r\b\u0080\b\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\u0006\u0010\u000f\u001a\u00020\u0005\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\b\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\u000b¢\u0006\u0004\b$\u0010%J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ<\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u000e\u001a\u00020\u00022\b\b\u0002\u0010\u000f\u001a\u00020\u00052\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u000bHÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0004J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u001a\u0010\u001a\u001a\u00020\u00192\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR\u0019\u0010\u000f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001c\u001a\u0004\b\u001d\u0010\u0007R\u001b\u0010\u0010\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001e\u001a\u0004\b\u001f\u0010\nR\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010 \u001a\u0004\b!\u0010\u0004R\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\"\u001a\u0004\b#\u0010\r¨\u0006&"}, d2 = {"Lco/discord/media_engine/internal/Inbound;", "", "", "component1", "()Ljava/lang/String;", "Lco/discord/media_engine/internal/InboundAudio;", "component2", "()Lco/discord/media_engine/internal/InboundAudio;", "Lco/discord/media_engine/internal/InboundVideo;", "component3", "()Lco/discord/media_engine/internal/InboundVideo;", "Lco/discord/media_engine/internal/InboundPlayout;", "component4", "()Lco/discord/media_engine/internal/InboundPlayout;", ModelAuditLogEntry.CHANGE_KEY_ID, MediaStreamTrack.AUDIO_TRACK_KIND, MediaStreamTrack.VIDEO_TRACK_KIND, "playout", "copy", "(Ljava/lang/String;Lco/discord/media_engine/internal/InboundAudio;Lco/discord/media_engine/internal/InboundVideo;Lco/discord/media_engine/internal/InboundPlayout;)Lco/discord/media_engine/internal/Inbound;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lco/discord/media_engine/internal/InboundAudio;", "getAudio", "Lco/discord/media_engine/internal/InboundVideo;", "getVideo", "Ljava/lang/String;", "getId", "Lco/discord/media_engine/internal/InboundPlayout;", "getPlayout", HookHelper.constructorName, "(Ljava/lang/String;Lco/discord/media_engine/internal/InboundAudio;Lco/discord/media_engine/internal/InboundVideo;Lco/discord/media_engine/internal/InboundPlayout;)V", "android_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class Inbound {
    private final InboundAudio audio;

    /* renamed from: id  reason: collision with root package name */
    private final String f1991id;
    private final InboundPlayout playout;
    private final InboundVideo video;

    public Inbound(String str, InboundAudio inboundAudio, InboundVideo inboundVideo, InboundPlayout inboundPlayout) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_ID);
        m.checkNotNullParameter(inboundAudio, MediaStreamTrack.AUDIO_TRACK_KIND);
        this.f1991id = str;
        this.audio = inboundAudio;
        this.video = inboundVideo;
        this.playout = inboundPlayout;
    }

    public static /* synthetic */ Inbound copy$default(Inbound inbound, String str, InboundAudio inboundAudio, InboundVideo inboundVideo, InboundPlayout inboundPlayout, int i, Object obj) {
        if ((i & 1) != 0) {
            str = inbound.f1991id;
        }
        if ((i & 2) != 0) {
            inboundAudio = inbound.audio;
        }
        if ((i & 4) != 0) {
            inboundVideo = inbound.video;
        }
        if ((i & 8) != 0) {
            inboundPlayout = inbound.playout;
        }
        return inbound.copy(str, inboundAudio, inboundVideo, inboundPlayout);
    }

    public final String component1() {
        return this.f1991id;
    }

    public final InboundAudio component2() {
        return this.audio;
    }

    public final InboundVideo component3() {
        return this.video;
    }

    public final InboundPlayout component4() {
        return this.playout;
    }

    public final Inbound copy(String str, InboundAudio inboundAudio, InboundVideo inboundVideo, InboundPlayout inboundPlayout) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_ID);
        m.checkNotNullParameter(inboundAudio, MediaStreamTrack.AUDIO_TRACK_KIND);
        return new Inbound(str, inboundAudio, inboundVideo, inboundPlayout);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Inbound)) {
            return false;
        }
        Inbound inbound = (Inbound) obj;
        return m.areEqual(this.f1991id, inbound.f1991id) && m.areEqual(this.audio, inbound.audio) && m.areEqual(this.video, inbound.video) && m.areEqual(this.playout, inbound.playout);
    }

    public final InboundAudio getAudio() {
        return this.audio;
    }

    public final String getId() {
        return this.f1991id;
    }

    public final InboundPlayout getPlayout() {
        return this.playout;
    }

    public final InboundVideo getVideo() {
        return this.video;
    }

    public int hashCode() {
        String str = this.f1991id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        InboundAudio inboundAudio = this.audio;
        int hashCode2 = (hashCode + (inboundAudio != null ? inboundAudio.hashCode() : 0)) * 31;
        InboundVideo inboundVideo = this.video;
        int hashCode3 = (hashCode2 + (inboundVideo != null ? inboundVideo.hashCode() : 0)) * 31;
        InboundPlayout inboundPlayout = this.playout;
        if (inboundPlayout != null) {
            i = inboundPlayout.hashCode();
        }
        return hashCode3 + i;
    }

    public String toString() {
        StringBuilder R = a.R("Inbound(id=");
        R.append(this.f1991id);
        R.append(", audio=");
        R.append(this.audio);
        R.append(", video=");
        R.append(this.video);
        R.append(", playout=");
        R.append(this.playout);
        R.append(")");
        return R.toString();
    }
}
