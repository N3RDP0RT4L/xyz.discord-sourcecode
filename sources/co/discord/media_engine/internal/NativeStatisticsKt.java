package co.discord.media_engine.internal;

import kotlin.Metadata;
/* compiled from: NativeStatistics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\u0006\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0007*\n\u0010\u0001\"\u00020\u00002\u00020\u0000*\n\u0010\u0003\"\u00020\u00022\u00020\u0002*\n\u0010\u0005\"\u00020\u00042\u00020\u0004*\n\u0010\u0006\"\u00020\u00042\u00020\u0004*\n\u0010\b\"\u00020\u00072\u00020\u0007*\n\u0010\t\"\u00020\u00042\u00020\u0004*\n\u0010\n\"\u00020\u00042\u00020\u0004*\n\u0010\u000b\"\u00020\u00072\u00020\u0007*\n\u0010\f\"\u00020\u00072\u00020\u0007*\n\u0010\r\"\u00020\u00042\u00020\u0004¨\u0006\u000e"}, d2 = {"", "F32", "", "F64", "", "I16", "I32", "", "I64", "I8", "U16", "U32", "U64", "U8", "android_release"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class NativeStatisticsKt {
}
