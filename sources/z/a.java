package z;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;
/* compiled from: AndroidExecutors.java */
/* loaded from: classes.dex */
public final class a {
    public static final a a = new a();

    /* renamed from: b  reason: collision with root package name */
    public static final int f3829b;
    public static final int c;
    public static final int d;
    public final Executor e = new b(null);

    /* compiled from: AndroidExecutors.java */
    /* loaded from: classes.dex */
    public static class b implements Executor {
        public b(C0442a aVar) {
        }

        @Override // java.util.concurrent.Executor
        public void execute(Runnable runnable) {
            new Handler(Looper.getMainLooper()).post(runnable);
        }
    }

    static {
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        f3829b = availableProcessors;
        c = availableProcessors + 1;
        d = (availableProcessors * 2) + 1;
    }
}
