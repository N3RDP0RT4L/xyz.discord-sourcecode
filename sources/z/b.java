package z;

import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
/* compiled from: BoltsExecutors.java */
/* loaded from: classes.dex */
public final class b {
    public static final b a = new b();

    /* renamed from: b  reason: collision with root package name */
    public final ExecutorService f3830b;
    public final Executor c;

    /* compiled from: BoltsExecutors.java */
    /* renamed from: z.b$b  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static class ExecutorC0443b implements Executor {
        public ThreadLocal<Integer> j = new ThreadLocal<>();

        public ExecutorC0443b(a aVar) {
        }

        public final int a() {
            Integer num = this.j.get();
            if (num == null) {
                num = 0;
            }
            int intValue = num.intValue() - 1;
            if (intValue == 0) {
                this.j.remove();
            } else {
                this.j.set(Integer.valueOf(intValue));
            }
            return intValue;
        }

        @Override // java.util.concurrent.Executor
        public void execute(Runnable runnable) {
            Integer num = this.j.get();
            if (num == null) {
                num = 0;
            }
            int intValue = num.intValue() + 1;
            this.j.set(Integer.valueOf(intValue));
            try {
                if (intValue <= 15) {
                    runnable.run();
                } else {
                    b.a.f3830b.execute(runnable);
                }
            } finally {
                a();
            }
        }
    }

    public b() {
        ThreadPoolExecutor threadPoolExecutor;
        String property = System.getProperty("java.runtime.name");
        if (!(property == null ? false : property.toLowerCase(Locale.US).contains("android"))) {
            threadPoolExecutor = Executors.newCachedThreadPool();
        } else {
            z.a aVar = z.a.a;
            ThreadPoolExecutor threadPoolExecutor2 = new ThreadPoolExecutor(z.a.c, z.a.d, 1L, TimeUnit.SECONDS, new LinkedBlockingQueue());
            threadPoolExecutor2.allowCoreThreadTimeOut(true);
            threadPoolExecutor = threadPoolExecutor2;
        }
        this.f3830b = threadPoolExecutor;
        Executors.newSingleThreadScheduledExecutor();
        this.c = new ExecutorC0443b(null);
    }
}
