package z;

import bolts.ExecutorException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
/* compiled from: Task.java */
/* loaded from: classes.dex */
public class g<TResult> {
    public static final ExecutorService a;

    /* renamed from: b  reason: collision with root package name */
    public static final Executor f3832b;
    public static final Executor c = a.a.e;
    public static g<?> d = new g<>((Object) null);
    public static g<Boolean> e = new g<>(Boolean.TRUE);
    public static g<Boolean> f = new g<>(Boolean.FALSE);
    public boolean h;
    public boolean i;
    public TResult j;
    public Exception k;
    public boolean l;
    public final Object g = new Object();
    public List<c<TResult, Void>> m = new ArrayList();

    static {
        b bVar = b.a;
        a = bVar.f3830b;
        f3832b = bVar.c;
        new g(true);
    }

    public g() {
    }

    public static <TResult> g<TResult> a(Callable<TResult> callable, Executor executor) {
        h hVar = new h();
        try {
            executor.execute(new f(hVar, callable));
        } catch (Exception e2) {
            hVar.b(new ExecutorException(e2));
        }
        return hVar.a;
    }

    public static <TResult> g<TResult> c(Exception exc) {
        boolean z2;
        g<TResult> gVar = new g<>();
        synchronized (gVar.g) {
            z2 = false;
            if (!gVar.h) {
                gVar.h = true;
                gVar.k = exc;
                gVar.l = false;
                gVar.g.notifyAll();
                gVar.f();
                z2 = true;
            }
        }
        if (z2) {
            return gVar;
        }
        throw new IllegalStateException("Cannot set the error on a completed task.");
    }

    public <TContinuationResult> g<TContinuationResult> b(c<TResult, TContinuationResult> cVar) {
        boolean z2;
        Executor executor = f3832b;
        h hVar = new h();
        synchronized (this.g) {
            synchronized (this.g) {
                z2 = this.h;
            }
            if (!z2) {
                this.m.add(new d(this, hVar, cVar, executor));
            }
        }
        if (z2) {
            try {
                executor.execute(new e(hVar, cVar, this));
            } catch (Exception e2) {
                hVar.b(new ExecutorException(e2));
            }
        }
        return (g<TResult>) hVar.a;
    }

    public Exception d() {
        Exception exc;
        synchronized (this.g) {
            exc = this.k;
            if (exc != null) {
                this.l = true;
            }
        }
        return exc;
    }

    public boolean e() {
        boolean z2;
        synchronized (this.g) {
            z2 = d() != null;
        }
        return z2;
    }

    public final void f() {
        synchronized (this.g) {
            for (c<TResult, Void> cVar : this.m) {
                try {
                    cVar.a(this);
                } catch (RuntimeException e2) {
                    throw e2;
                } catch (Exception e3) {
                    throw new RuntimeException(e3);
                }
            }
            this.m = null;
        }
    }

    public boolean g() {
        synchronized (this.g) {
            if (this.h) {
                return false;
            }
            this.h = true;
            this.i = true;
            this.g.notifyAll();
            f();
            return true;
        }
    }

    public boolean h(TResult tresult) {
        synchronized (this.g) {
            if (this.h) {
                return false;
            }
            this.h = true;
            this.j = tresult;
            this.g.notifyAll();
            f();
            return true;
        }
    }

    public g(TResult tresult) {
        h(tresult);
    }

    public g(boolean z2) {
        if (z2) {
            g();
        } else {
            h(null);
        }
    }
}
