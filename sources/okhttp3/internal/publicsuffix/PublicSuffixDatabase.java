package okhttp3.internal.publicsuffix;

import andhook.lib.xposed.ClassUtils;
import d0.f0.q;
import d0.g0.w;
import d0.t.m;
import d0.t.n;
import d0.t.u;
import f0.e0.c;
import f0.e0.k.h;
import g0.l;
import g0.r;
import g0.y;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.net.IDN;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import kotlin.TypeCastException;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: PublicSuffixDatabase.kt */
/* loaded from: classes3.dex */
public final class PublicSuffixDatabase {
    public final AtomicBoolean e = new AtomicBoolean(false);
    public final CountDownLatch f = new CountDownLatch(1);
    public byte[] g;
    public byte[] h;
    public static final a d = new a(null);
    public static final byte[] a = {(byte) 42};

    /* renamed from: b  reason: collision with root package name */
    public static final List<String> f3789b = m.listOf("*");
    public static final PublicSuffixDatabase c = new PublicSuffixDatabase();

    /* compiled from: PublicSuffixDatabase.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public static final String a(a aVar, byte[] bArr, byte[][] bArr2, int i) {
            int i2;
            boolean z2;
            int i3;
            int i4;
            int length = bArr.length;
            int i5 = 0;
            while (i5 < length) {
                int i6 = (i5 + length) / 2;
                while (i6 > -1 && bArr[i6] != ((byte) 10)) {
                    i6--;
                }
                int i7 = i6 + 1;
                int i8 = 1;
                while (true) {
                    i2 = i7 + i8;
                    if (bArr[i2] == ((byte) 10)) {
                        break;
                    }
                    i8++;
                }
                int i9 = i2 - i7;
                int i10 = i;
                boolean z3 = false;
                int i11 = 0;
                int i12 = 0;
                while (true) {
                    if (z3) {
                        i3 = 46;
                        z2 = false;
                    } else {
                        byte b2 = bArr2[i10][i11];
                        byte[] bArr3 = c.a;
                        i3 = b2 & 255;
                        z2 = z3;
                    }
                    byte b3 = bArr[i7 + i12];
                    byte[] bArr4 = c.a;
                    i4 = i3 - (b3 & 255);
                    if (i4 != 0) {
                        break;
                    }
                    i12++;
                    i11++;
                    if (i12 == i9) {
                        break;
                    } else if (bArr2[i10].length != i11) {
                        z3 = z2;
                    } else if (i10 == bArr2.length - 1) {
                        break;
                    } else {
                        i10++;
                        z3 = true;
                        i11 = -1;
                    }
                }
                if (i4 >= 0) {
                    if (i4 <= 0) {
                        int i13 = i9 - i12;
                        int length2 = bArr2[i10].length - i11;
                        int length3 = bArr2.length;
                        for (int i14 = i10 + 1; i14 < length3; i14++) {
                            length2 += bArr2[i14].length;
                        }
                        if (length2 >= i13) {
                            if (length2 <= i13) {
                                Charset charset = StandardCharsets.UTF_8;
                                d0.z.d.m.checkExpressionValueIsNotNull(charset, "UTF_8");
                                return new String(bArr, i7, i9, charset);
                            }
                        }
                    }
                    i5 = i2 + 1;
                }
                length = i7 - 1;
            }
            return null;
        }
    }

    public final String a(String str) {
        String str2;
        String str3;
        String str4;
        List<String> list;
        int i;
        int i2;
        List<String> list2;
        d0.z.d.m.checkParameterIsNotNull(str, "domain");
        String unicode = IDN.toUnicode(str);
        d0.z.d.m.checkExpressionValueIsNotNull(unicode, "unicodeDomain");
        List<String> c2 = c(unicode);
        if (this.e.get() || !this.e.compareAndSet(false, true)) {
            try {
                this.f.await();
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
            }
        } else {
            boolean z2 = false;
            while (true) {
                try {
                    try {
                        b();
                        if (!z2) {
                            break;
                        }
                        Thread.currentThread().interrupt();
                        break;
                    } catch (InterruptedIOException unused2) {
                        Thread.interrupted();
                        z2 = true;
                    } catch (IOException e) {
                        h.a aVar = h.c;
                        h.a.i("Failed to read public suffix list", 5, e);
                        if (z2) {
                            Thread.currentThread().interrupt();
                        }
                    }
                } catch (Throwable th) {
                    if (z2) {
                        Thread.currentThread().interrupt();
                    }
                    throw th;
                }
            }
        }
        if (this.g != null) {
            int size = c2.size();
            byte[][] bArr = new byte[size];
            for (int i3 = 0; i3 < size; i3++) {
                String str5 = c2.get(i3);
                Charset charset = StandardCharsets.UTF_8;
                d0.z.d.m.checkExpressionValueIsNotNull(charset, "UTF_8");
                if (str5 != null) {
                    byte[] bytes = str5.getBytes(charset);
                    d0.z.d.m.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
                    bArr[i3] = bytes;
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            }
            int i4 = 0;
            while (true) {
                if (i4 >= size) {
                    str2 = null;
                    break;
                }
                a aVar2 = d;
                byte[] bArr2 = this.g;
                if (bArr2 == null) {
                    d0.z.d.m.throwUninitializedPropertyAccessException("publicSuffixListBytes");
                }
                str2 = a.a(aVar2, bArr2, bArr, i4);
                if (str2 != null) {
                    break;
                }
                i4++;
            }
            if (size > 1) {
                byte[][] bArr3 = (byte[][]) bArr.clone();
                int length = bArr3.length - 1;
                for (int i5 = 0; i5 < length; i5++) {
                    bArr3[i5] = a;
                    a aVar3 = d;
                    byte[] bArr4 = this.g;
                    if (bArr4 == null) {
                        d0.z.d.m.throwUninitializedPropertyAccessException("publicSuffixListBytes");
                    }
                    String a2 = a.a(aVar3, bArr4, bArr3, i5);
                    if (a2 != null) {
                        str3 = a2;
                        break;
                    }
                }
            }
            str3 = null;
            if (str3 != null) {
                int i6 = size - 1;
                for (int i7 = 0; i7 < i6; i7++) {
                    a aVar4 = d;
                    byte[] bArr5 = this.h;
                    if (bArr5 == null) {
                        d0.z.d.m.throwUninitializedPropertyAccessException("publicSuffixExceptionListBytes");
                    }
                    str4 = a.a(aVar4, bArr5, bArr, i7);
                    if (str4 != null) {
                        break;
                    }
                }
            }
            str4 = null;
            if (str4 != null) {
                list = w.split$default((CharSequence) ('!' + str4), new char[]{ClassUtils.PACKAGE_SEPARATOR_CHAR}, false, 0, 6, (Object) null);
            } else if (str2 == null && str3 == null) {
                list = f3789b;
            } else {
                if (str2 == null || (list2 = w.split$default((CharSequence) str2, new char[]{ClassUtils.PACKAGE_SEPARATOR_CHAR}, false, 0, 6, (Object) null)) == null) {
                    list2 = n.emptyList();
                }
                if (str3 == null || (list = w.split$default((CharSequence) str3, new char[]{ClassUtils.PACKAGE_SEPARATOR_CHAR}, false, 0, 6, (Object) null)) == null) {
                    list = n.emptyList();
                }
                if (list2.size() > list.size()) {
                    list = list2;
                }
            }
            if (c2.size() == list.size() && list.get(0).charAt(0) != '!') {
                return null;
            }
            if (list.get(0).charAt(0) == '!') {
                i = c2.size();
                i2 = list.size();
            } else {
                i = c2.size();
                i2 = list.size() + 1;
            }
            return q.joinToString$default(q.drop(u.asSequence(c(str)), i - i2), ".", null, null, 0, null, null, 62, null);
        }
        throw new IllegalStateException("Unable to load publicsuffixes.gz resource from the classpath.".toString());
    }

    public final void b() throws IOException {
        InputStream resourceAsStream = PublicSuffixDatabase.class.getResourceAsStream("publicsuffixes.gz");
        if (resourceAsStream != null) {
            d0.z.d.m.checkParameterIsNotNull(resourceAsStream, "$this$source");
            l lVar = new l(new g0.n(resourceAsStream, new y()));
            d0.z.d.m.checkParameterIsNotNull(lVar, "$this$buffer");
            r rVar = new r(lVar);
            th = null;
            try {
                byte[] Z = rVar.Z(rVar.readInt());
                byte[] Z2 = rVar.Z(rVar.readInt());
                synchronized (this) {
                    if (Z == null) {
                        d0.z.d.m.throwNpe();
                    }
                    this.g = Z;
                    if (Z2 == null) {
                        d0.z.d.m.throwNpe();
                    }
                    this.h = Z2;
                }
                this.f.countDown();
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
    }

    public final List<String> c(String str) {
        List<String> split$default = w.split$default((CharSequence) str, new char[]{ClassUtils.PACKAGE_SEPARATOR_CHAR}, false, 0, 6, (Object) null);
        return d0.z.d.m.areEqual((String) u.last((List<? extends Object>) split$default), "") ? u.dropLast(split$default, 1) : split$default;
    }
}
