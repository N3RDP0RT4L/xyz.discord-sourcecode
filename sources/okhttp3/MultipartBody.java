package okhttp3;

import androidx.browser.trusted.sharing.ShareTarget;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.g0.w;
import d0.z.d.m;
import f0.e0.c;
import g0.e;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import okhttp3.MediaType;
import okio.BufferedSink;
import okio.ByteString;
/* compiled from: MultipartBody.kt */
/* loaded from: classes3.dex */
public final class MultipartBody extends RequestBody {
    public static final byte[] e;
    public final MediaType g;
    public long h = -1;
    public final ByteString i;
    public final MediaType j;
    public final List<Part> k;
    public static final b f = new b(null);
    public static final MediaType a = MediaType.a.a("multipart/mixed");

    /* renamed from: b  reason: collision with root package name */
    public static final MediaType f3781b = MediaType.a.a(ShareTarget.ENCODING_TYPE_MULTIPART);
    public static final byte[] c = {(byte) 58, (byte) 32};
    public static final byte[] d = {(byte) 13, (byte) 10};

    /* compiled from: MultipartBody.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001R\u001b\u0010\u0005\u001a\u0004\u0018\u00010\u00028\u0007@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0019\u0010\n\u001a\u00020\u00078\u0007@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000b¨\u0006\f"}, d2 = {"Lokhttp3/MultipartBody$Part;", "", "Lokhttp3/Headers;", "a", "Lokhttp3/Headers;", "headers", "()Lokhttp3/Headers;", "Lokhttp3/RequestBody;", "b", "Lokhttp3/RequestBody;", "body", "()Lokhttp3/RequestBody;", "okhttp"}, k = 1, mv = {1, 4, 0})
    /* loaded from: classes3.dex */
    public static final class Part {
        public final Headers a;

        /* renamed from: b  reason: collision with root package name */
        public final RequestBody f3782b;

        public Part(Headers headers, RequestBody requestBody, DefaultConstructorMarker defaultConstructorMarker) {
            this.a = headers;
            this.f3782b = requestBody;
        }

        public static final Part a(Headers headers, RequestBody requestBody) {
            m.checkParameterIsNotNull(requestBody, "body");
            boolean z2 = false;
            if (headers.c("Content-Type") == null) {
                if (headers.c("Content-Length") == null) {
                    z2 = true;
                }
                if (z2) {
                    return new Part(headers, requestBody, null);
                }
                throw new IllegalArgumentException("Unexpected header: Content-Length".toString());
            }
            throw new IllegalArgumentException("Unexpected header: Content-Type".toString());
        }

        public static final Part b(String str, String str2, RequestBody requestBody) {
            m.checkParameterIsNotNull(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkParameterIsNotNull(requestBody, "body");
            StringBuilder sb = new StringBuilder();
            sb.append("form-data; name=");
            b bVar = MultipartBody.f;
            bVar.a(sb, str);
            if (str2 != null) {
                sb.append("; filename=");
                bVar.a(sb, str2);
            }
            String sb2 = sb.toString();
            m.checkExpressionValueIsNotNull(sb2, "StringBuilder().apply(builderAction).toString()");
            ArrayList arrayList = new ArrayList(20);
            m.checkParameterIsNotNull("Content-Disposition", ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkParameterIsNotNull(sb2, "value");
            if (1 != 0) {
                for (int i = 0; i < 19; i++) {
                    char charAt = "Content-Disposition".charAt(i);
                    if (!('!' <= charAt && '~' >= charAt)) {
                        throw new IllegalArgumentException(c.j("Unexpected char %#04x at %d in header name: %s", Integer.valueOf(charAt), Integer.valueOf(i), "Content-Disposition").toString());
                    }
                }
                m.checkParameterIsNotNull("Content-Disposition", ModelAuditLogEntry.CHANGE_KEY_NAME);
                m.checkParameterIsNotNull(sb2, "value");
                arrayList.add("Content-Disposition");
                arrayList.add(w.trim(sb2).toString());
                Object[] array = arrayList.toArray(new String[0]);
                if (array != null) {
                    return a(new Headers((String[]) array, null), requestBody);
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            }
            throw new IllegalArgumentException("name is empty".toString());
        }
    }

    /* compiled from: MultipartBody.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public final ByteString a;

        /* renamed from: b  reason: collision with root package name */
        public MediaType f3783b = MultipartBody.a;
        public final List<Part> c = new ArrayList();

        public a() {
            String uuid = UUID.randomUUID().toString();
            m.checkExpressionValueIsNotNull(uuid, "UUID.randomUUID().toString()");
            m.checkParameterIsNotNull(uuid, "boundary");
            this.a = ByteString.k.c(uuid);
        }

        public final a a(Part part) {
            m.checkParameterIsNotNull(part, "part");
            this.c.add(part);
            return this;
        }

        public final MultipartBody b() {
            if (!this.c.isEmpty()) {
                return new MultipartBody(this.a, this.f3783b, c.z(this.c));
            }
            throw new IllegalStateException("Multipart body must have at least one part.".toString());
        }

        public final a c(MediaType mediaType) {
            m.checkParameterIsNotNull(mediaType, "type");
            if (m.areEqual(mediaType.e, "multipart")) {
                this.f3783b = mediaType;
                return this;
            }
            throw new IllegalArgumentException(("multipart != " + mediaType).toString());
        }
    }

    /* compiled from: MultipartBody.kt */
    /* loaded from: classes3.dex */
    public static final class b {
        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final void a(StringBuilder sb, String str) {
            m.checkParameterIsNotNull(sb, "$this$appendQuotedString");
            m.checkParameterIsNotNull(str, "key");
            sb.append('\"');
            int length = str.length();
            for (int i = 0; i < length; i++) {
                char charAt = str.charAt(i);
                if (charAt == '\n') {
                    sb.append("%0A");
                } else if (charAt == '\r') {
                    sb.append("%0D");
                } else if (charAt == '\"') {
                    sb.append("%22");
                } else {
                    sb.append(charAt);
                }
            }
            sb.append('\"');
        }
    }

    static {
        MediaType.a aVar = MediaType.c;
        MediaType.a.a("multipart/alternative");
        MediaType.a.a("multipart/digest");
        MediaType.a.a("multipart/parallel");
        byte b2 = (byte) 45;
        e = new byte[]{b2, b2};
    }

    public MultipartBody(ByteString byteString, MediaType mediaType, List<Part> list) {
        m.checkParameterIsNotNull(byteString, "boundaryByteString");
        m.checkParameterIsNotNull(mediaType, "type");
        m.checkParameterIsNotNull(list, "parts");
        this.i = byteString;
        this.j = mediaType;
        this.k = list;
        MediaType.a aVar = MediaType.c;
        this.g = MediaType.a.a(mediaType + "; boundary=" + byteString.q());
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final long a(BufferedSink bufferedSink, boolean z2) throws IOException {
        e eVar;
        if (z2) {
            bufferedSink = new e();
            eVar = bufferedSink;
        } else {
            eVar = 0;
        }
        int size = this.k.size();
        long j = 0;
        for (int i = 0; i < size; i++) {
            Part part = this.k.get(i);
            Headers headers = part.a;
            RequestBody requestBody = part.f3782b;
            if (bufferedSink == null) {
                m.throwNpe();
            }
            bufferedSink.write(e);
            bufferedSink.e0(this.i);
            bufferedSink.write(d);
            if (headers != null) {
                int size2 = headers.size();
                for (int i2 = 0; i2 < size2; i2++) {
                    bufferedSink.K(headers.d(i2)).write(c).K(headers.g(i2)).write(d);
                }
            }
            MediaType contentType = requestBody.contentType();
            if (contentType != null) {
                bufferedSink.K("Content-Type: ").K(contentType.d).write(d);
            }
            long contentLength = requestBody.contentLength();
            if (contentLength != -1) {
                bufferedSink.K("Content-Length: ").q0(contentLength).write(d);
            } else if (z2) {
                if (eVar == 0) {
                    m.throwNpe();
                }
                eVar.skip(eVar.k);
                return -1L;
            }
            byte[] bArr = d;
            bufferedSink.write(bArr);
            if (z2) {
                j += contentLength;
            } else {
                requestBody.writeTo(bufferedSink);
            }
            bufferedSink.write(bArr);
        }
        if (bufferedSink == null) {
            m.throwNpe();
        }
        byte[] bArr2 = e;
        bufferedSink.write(bArr2);
        bufferedSink.e0(this.i);
        bufferedSink.write(bArr2);
        bufferedSink.write(d);
        if (!z2) {
            return j;
        }
        if (eVar == 0) {
            m.throwNpe();
        }
        long j2 = eVar.k;
        long j3 = j + j2;
        eVar.skip(j2);
        return j3;
    }

    @Override // okhttp3.RequestBody
    public long contentLength() throws IOException {
        long j = this.h;
        if (j != -1) {
            return j;
        }
        long a2 = a(null, true);
        this.h = a2;
        return a2;
    }

    @Override // okhttp3.RequestBody
    public MediaType contentType() {
        return this.g;
    }

    @Override // okhttp3.RequestBody
    public void writeTo(BufferedSink bufferedSink) throws IOException {
        m.checkParameterIsNotNull(bufferedSink, "sink");
        a(bufferedSink, false);
    }
}
