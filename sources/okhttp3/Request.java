package okhttp3;

import andhook.lib.HookHelper;
import androidx.browser.trusted.sharing.ShareTarget;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.g0.t;
import d0.t.h0;
import d0.t.n;
import d0.z.d.m;
import f0.d;
import f0.e0.c;
import f0.e0.h.f;
import f0.w;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Pair;
import okhttp3.Headers;
/* compiled from: Request.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001\u0013BC\b\u0000\u0012\u0006\u0010\u001f\u001a\u00020\u001d\u0012\u0006\u0010\n\u001a\u00020\u0002\u0012\u0006\u0010\u001b\u001a\u00020\u0018\u0012\b\u0010$\u001a\u0004\u0018\u00010!\u0012\u0016\u0010\u0011\u001a\u0012\u0012\b\u0012\u0006\u0012\u0002\b\u00030\f\u0012\u0004\u0012\u00020\u00010\u000b¢\u0006\u0004\b&\u0010'J\u0017\u0010\u0004\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u000f\u0010\u0006\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0006\u0010\u0007R\u0019\u0010\n\u001a\u00020\u00028\u0007@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u0007R,\u0010\u0011\u001a\u0012\u0012\b\u0012\u0006\u0012\u0002\b\u00030\f\u0012\u0004\u0012\u00020\u00010\u000b8\u0000@\u0000X\u0080\u0004¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u0018\u0010\u0015\u001a\u0004\u0018\u00010\u00128\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u0013\u0010\u0017\u001a\u00020\u00128G@\u0006¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0016R\u0019\u0010\u001b\u001a\u00020\u00188\u0007@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001cR\u0019\u0010\u001f\u001a\u00020\u001d8\u0007@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u001e\u001a\u0004\b\u001f\u0010 R\u001b\u0010$\u001a\u0004\u0018\u00010!8\u0007@\u0006¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%¨\u0006("}, d2 = {"Lokhttp3/Request;", "", "", ModelAuditLogEntry.CHANGE_KEY_NAME, "b", "(Ljava/lang/String;)Ljava/lang/String;", "toString", "()Ljava/lang/String;", "c", "Ljava/lang/String;", "method", "", "Ljava/lang/Class;", "f", "Ljava/util/Map;", "getTags$okhttp", "()Ljava/util/Map;", ModelAuditLogEntry.CHANGE_KEY_TAGS, "Lf0/d;", "a", "Lf0/d;", "lazyCacheControl", "()Lf0/d;", "cacheControl", "Lokhttp3/Headers;", "d", "Lokhttp3/Headers;", "headers", "()Lokhttp3/Headers;", "Lf0/w;", "Lf0/w;", "url", "()Lf0/w;", "Lokhttp3/RequestBody;", "e", "Lokhttp3/RequestBody;", "body", "()Lokhttp3/RequestBody;", HookHelper.constructorName, "(Lf0/w;Ljava/lang/String;Lokhttp3/Headers;Lokhttp3/RequestBody;Ljava/util/Map;)V", "okhttp"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes3.dex */
public final class Request {
    public d a;

    /* renamed from: b  reason: collision with root package name */
    public final w f3784b;
    public final String c;
    public final Headers d;
    public final RequestBody e;
    public final Map<Class<?>, Object> f;

    public Request(w wVar, String str, Headers headers, RequestBody requestBody, Map<Class<?>, ? extends Object> map) {
        m.checkParameterIsNotNull(wVar, "url");
        m.checkParameterIsNotNull(str, "method");
        m.checkParameterIsNotNull(headers, "headers");
        m.checkParameterIsNotNull(map, ModelAuditLogEntry.CHANGE_KEY_TAGS);
        this.f3784b = wVar;
        this.c = str;
        this.d = headers;
        this.e = requestBody;
        this.f = map;
    }

    public final d a() {
        d dVar = this.a;
        if (dVar != null) {
            return dVar;
        }
        d b2 = d.a.b(this.d);
        this.a = b2;
        return b2;
    }

    public final String b(String str) {
        m.checkParameterIsNotNull(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        return this.d.c(str);
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("Request{method=");
        R.append(this.c);
        R.append(", url=");
        R.append(this.f3784b);
        if (this.d.size() != 0) {
            R.append(", headers=[");
            int i = 0;
            for (Pair<? extends String, ? extends String> pair : this.d) {
                i++;
                if (i < 0) {
                    n.throwIndexOverflow();
                }
                Pair<? extends String, ? extends String> pair2 = pair;
                String component1 = pair2.component1();
                String component2 = pair2.component2();
                if (i > 0) {
                    R.append(", ");
                }
                R.append(component1);
                R.append(MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR);
                R.append(component2);
            }
            R.append(']');
        }
        if (!this.f.isEmpty()) {
            R.append(", tags=");
            R.append(this.f);
        }
        R.append('}');
        String sb = R.toString();
        m.checkExpressionValueIsNotNull(sb, "StringBuilder().apply(builderAction).toString()");
        return sb;
    }

    /* compiled from: Request.kt */
    /* loaded from: classes3.dex */
    public static class a {
        public w a;

        /* renamed from: b  reason: collision with root package name */
        public String f3785b;
        public Headers.a c;
        public RequestBody d;
        public Map<Class<?>, Object> e;

        public a() {
            this.e = new LinkedHashMap();
            this.f3785b = ShareTarget.METHOD_GET;
            this.c = new Headers.a();
        }

        public Request a() {
            w wVar = this.a;
            if (wVar != null) {
                return new Request(wVar, this.f3785b, this.c.c(), this.d, c.A(this.e));
            }
            throw new IllegalStateException("url == null".toString());
        }

        public a b(String str, String str2) {
            m.checkParameterIsNotNull(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkParameterIsNotNull(str2, "value");
            Headers.a aVar = this.c;
            Objects.requireNonNull(aVar);
            m.checkParameterIsNotNull(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkParameterIsNotNull(str2, "value");
            Headers.b bVar = Headers.j;
            bVar.a(str);
            bVar.b(str2, str);
            aVar.d(str);
            aVar.b(str, str2);
            return this;
        }

        public a c(String str, RequestBody requestBody) {
            m.checkParameterIsNotNull(str, "method");
            boolean z2 = false;
            if (str.length() > 0) {
                if (requestBody == null) {
                    m.checkParameterIsNotNull(str, "method");
                    if (m.areEqual(str, ShareTarget.METHOD_POST) || m.areEqual(str, "PUT") || m.areEqual(str, "PATCH") || m.areEqual(str, "PROPPATCH") || m.areEqual(str, "REPORT")) {
                        z2 = true;
                    }
                    if (!(!z2)) {
                        throw new IllegalArgumentException(b.d.b.a.a.w("method ", str, " must have a request body.").toString());
                    }
                } else if (!f.a(str)) {
                    throw new IllegalArgumentException(b.d.b.a.a.w("method ", str, " must not have a request body.").toString());
                }
                this.f3785b = str;
                this.d = requestBody;
                return this;
            }
            throw new IllegalArgumentException("method.isEmpty() == true".toString());
        }

        public a d(String str) {
            m.checkParameterIsNotNull(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            this.c.d(str);
            return this;
        }

        public <T> a e(Class<? super T> cls, T t) {
            m.checkParameterIsNotNull(cls, "type");
            if (t == null) {
                this.e.remove(cls);
            } else {
                if (this.e.isEmpty()) {
                    this.e = new LinkedHashMap();
                }
                Map<Class<?>, Object> map = this.e;
                T cast = cls.cast(t);
                if (cast == null) {
                    m.throwNpe();
                }
                map.put(cls, cast);
            }
            return this;
        }

        public a f(String str) {
            m.checkParameterIsNotNull(str, "url");
            if (t.startsWith(str, "ws:", true)) {
                StringBuilder R = b.d.b.a.a.R("http:");
                String substring = str.substring(3);
                m.checkExpressionValueIsNotNull(substring, "(this as java.lang.String).substring(startIndex)");
                R.append(substring);
                str = R.toString();
            } else if (t.startsWith(str, "wss:", true)) {
                StringBuilder R2 = b.d.b.a.a.R("https:");
                String substring2 = str.substring(4);
                m.checkExpressionValueIsNotNull(substring2, "(this as java.lang.String).substring(startIndex)");
                R2.append(substring2);
                str = R2.toString();
            }
            m.checkParameterIsNotNull(str, "$this$toHttpUrl");
            w.a aVar = new w.a();
            aVar.e(null, str);
            g(aVar.b());
            return this;
        }

        public a g(w wVar) {
            m.checkParameterIsNotNull(wVar, "url");
            this.a = wVar;
            return this;
        }

        public a(Request request) {
            Map<Class<?>, Object> map;
            m.checkParameterIsNotNull(request, "request");
            this.e = new LinkedHashMap();
            this.a = request.f3784b;
            this.f3785b = request.c;
            this.d = request.e;
            if (request.f.isEmpty()) {
                map = new LinkedHashMap<>();
            } else {
                map = h0.toMutableMap(request.f);
            }
            this.e = map;
            this.c = request.d.e();
        }
    }
}
