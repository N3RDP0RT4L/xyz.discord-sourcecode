package okhttp3;

import andhook.lib.HookHelper;
import d0.z.d.m;
import f0.e0.c;
import g0.g;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ResponseBody.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b&\u0018\u0000 \u00162\u00020\u0001:\u0002\u0006\u0003B\u0007¢\u0006\u0004\b\u0015\u0010\u0010J\u0011\u0010\u0003\u001a\u0004\u0018\u00010\u0002H&¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0006\u001a\u00020\u0005H&¢\u0006\u0004\b\u0006\u0010\u0007J\u000f\u0010\t\u001a\u00020\bH&¢\u0006\u0004\b\t\u0010\nJ\r\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000f\u001a\u00020\u000eH\u0016¢\u0006\u0004\b\u000f\u0010\u0010R\u0018\u0010\u0014\u001a\u0004\u0018\u00010\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013¨\u0006\u0017"}, d2 = {"Lokhttp3/ResponseBody;", "Ljava/io/Closeable;", "Lokhttp3/MediaType;", "b", "()Lokhttp3/MediaType;", "", "a", "()J", "Lg0/g;", "c", "()Lg0/g;", "", "d", "()Ljava/lang/String;", "", "close", "()V", "Ljava/io/Reader;", "k", "Ljava/io/Reader;", "reader", HookHelper.constructorName, "j", "okhttp"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes3.dex */
public abstract class ResponseBody implements Closeable {
    public static final b j = new b(null);
    public Reader k;

    /* compiled from: ResponseBody.kt */
    /* loaded from: classes3.dex */
    public static final class a extends Reader {
        public boolean j;
        public Reader k;
        public final g l;
        public final Charset m;

        public a(g gVar, Charset charset) {
            m.checkParameterIsNotNull(gVar, "source");
            m.checkParameterIsNotNull(charset, "charset");
            this.l = gVar;
            this.m = charset;
        }

        @Override // java.io.Reader, java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
            this.j = true;
            Reader reader = this.k;
            if (reader != null) {
                reader.close();
            } else {
                this.l.close();
            }
        }

        @Override // java.io.Reader
        public int read(char[] cArr, int i, int i2) throws IOException {
            m.checkParameterIsNotNull(cArr, "cbuf");
            if (!this.j) {
                Reader reader = this.k;
                if (reader == null) {
                    reader = new InputStreamReader(this.l.u0(), c.s(this.l, this.m));
                    this.k = reader;
                }
                return reader.read(cArr, i, i2);
            }
            throw new IOException("Stream closed");
        }
    }

    /* compiled from: ResponseBody.kt */
    /* loaded from: classes3.dex */
    public static final class b {
        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    public abstract long a();

    public abstract MediaType b();

    public abstract g c();

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        c.d(c());
    }

    public final String d() throws IOException {
        Charset charset;
        g c = c();
        try {
            MediaType b2 = b();
            if (b2 == null || (charset = b2.a(d0.g0.c.a)) == null) {
                charset = d0.g0.c.a;
            }
            th = null;
            return c.M(c.s(c, charset));
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }
}
