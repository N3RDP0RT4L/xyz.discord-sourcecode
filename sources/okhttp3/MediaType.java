package okhttp3;

import d0.g0.t;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: MediaType.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u0011\n\u0002\b\u0007\u0018\u0000 \u001b2\u00020\u0001:\u0001\u0004J\u001d\u0010\u0004\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0007¢\u0006\u0004\b\u0004\u0010\u0005J\u000f\u0010\u0007\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\u000b\u001a\u00020\n2\b\u0010\t\u001a\u0004\u0018\u00010\u0001H\u0096\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u000f\u0010\u000e\u001a\u00020\rH\u0016¢\u0006\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0012\u001a\u00020\u00068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0014\u001a\u00020\u00068\u0007@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0011\u001a\u0004\b\u0014\u0010\bR\u001c\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00060\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u0019\u0010\u001a\u001a\u00020\u00068\u0007@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u0011\u001a\u0004\b\u001a\u0010\b¨\u0006\u001c"}, d2 = {"Lokhttp3/MediaType;", "", "Ljava/nio/charset/Charset;", "defaultValue", "a", "(Ljava/nio/charset/Charset;)Ljava/nio/charset/Charset;", "", "toString", "()Ljava/lang/String;", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "hashCode", "()I", "d", "Ljava/lang/String;", "mediaType", "f", "subtype", "", "g", "[Ljava/lang/String;", "parameterNamesAndValues", "e", "type", "c", "okhttp"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes3.dex */
public final class MediaType {
    public static final Pattern a = Pattern.compile("([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)/([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)");

    /* renamed from: b  reason: collision with root package name */
    public static final Pattern f3780b = Pattern.compile(";\\s*(?:([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)=(?:([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)|\"([^\"]*)\"))?");
    public static final a c = null;
    public final String d;
    public final String e;
    public final String f;
    public final String[] g;

    /* compiled from: MediaType.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public static final MediaType a(String str) {
            m.checkParameterIsNotNull(str, "$this$toMediaType");
            Matcher matcher = MediaType.a.matcher(str);
            if (matcher.lookingAt()) {
                String group = matcher.group(1);
                m.checkExpressionValueIsNotNull(group, "typeSubtype.group(1)");
                Locale locale = Locale.US;
                m.checkExpressionValueIsNotNull(locale, "Locale.US");
                if (group != null) {
                    String lowerCase = group.toLowerCase(locale);
                    m.checkExpressionValueIsNotNull(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                    String group2 = matcher.group(2);
                    m.checkExpressionValueIsNotNull(group2, "typeSubtype.group(2)");
                    m.checkExpressionValueIsNotNull(locale, "Locale.US");
                    if (group2 != null) {
                        String lowerCase2 = group2.toLowerCase(locale);
                        m.checkExpressionValueIsNotNull(lowerCase2, "(this as java.lang.String).toLowerCase(locale)");
                        ArrayList arrayList = new ArrayList();
                        Matcher matcher2 = MediaType.f3780b.matcher(str);
                        int end = matcher.end();
                        while (end < str.length()) {
                            matcher2.region(end, str.length());
                            if (matcher2.lookingAt()) {
                                String group3 = matcher2.group(1);
                                if (group3 == null) {
                                    end = matcher2.end();
                                } else {
                                    String group4 = matcher2.group(2);
                                    if (group4 == null) {
                                        group4 = matcher2.group(3);
                                    } else if (t.startsWith$default(group4, "'", false, 2, null) && t.endsWith$default(group4, "'", false, 2, null) && group4.length() > 2) {
                                        group4 = group4.substring(1, group4.length() - 1);
                                        m.checkExpressionValueIsNotNull(group4, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                                    }
                                    arrayList.add(group3);
                                    arrayList.add(group4);
                                    end = matcher2.end();
                                }
                            } else {
                                StringBuilder R = b.d.b.a.a.R("Parameter is not formatted correctly: \"");
                                String substring = str.substring(end);
                                m.checkExpressionValueIsNotNull(substring, "(this as java.lang.String).substring(startIndex)");
                                R.append(substring);
                                R.append("\" for: \"");
                                R.append(str);
                                R.append('\"');
                                throw new IllegalArgumentException(R.toString().toString());
                            }
                        }
                        Object[] array = arrayList.toArray(new String[0]);
                        if (array != null) {
                            return new MediaType(str, lowerCase, lowerCase2, (String[]) array, null);
                        }
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
            throw new IllegalArgumentException(("No subtype found for: \"" + str + '\"').toString());
        }

        public static final MediaType b(String str) {
            m.checkParameterIsNotNull(str, "$this$toMediaTypeOrNull");
            try {
                return a(str);
            } catch (IllegalArgumentException unused) {
                return null;
            }
        }
    }

    public MediaType(String str, String str2, String str3, String[] strArr, DefaultConstructorMarker defaultConstructorMarker) {
        this.d = str;
        this.e = str2;
        this.f = str3;
        this.g = strArr;
    }

    public static final MediaType b(String str) {
        return a.a(str);
    }

    /* JADX WARN: Removed duplicated region for block: B:16:0x003d A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:21:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.nio.charset.Charset a(java.nio.charset.Charset r7) {
        /*
            r6 = this;
            java.lang.String r0 = "charset"
            java.lang.String r1 = "name"
            d0.z.d.m.checkParameterIsNotNull(r0, r1)
            java.lang.String[] r1 = r6.g
            kotlin.ranges.IntRange r1 = d0.t.k.getIndices(r1)
            r2 = 2
            kotlin.ranges.IntProgression r1 = d0.d0.f.step(r1, r2)
            int r2 = r1.getFirst()
            int r3 = r1.getLast()
            int r1 = r1.getStep()
            if (r1 < 0) goto L23
            if (r2 > r3) goto L3a
            goto L25
        L23:
            if (r2 < r3) goto L3a
        L25:
            java.lang.String[] r4 = r6.g
            r4 = r4[r2]
            r5 = 1
            boolean r4 = d0.g0.t.equals(r4, r0, r5)
            if (r4 == 0) goto L36
            java.lang.String[] r0 = r6.g
            int r2 = r2 + r5
            r0 = r0[r2]
            goto L3b
        L36:
            if (r2 == r3) goto L3a
            int r2 = r2 + r1
            goto L25
        L3a:
            r0 = 0
        L3b:
            if (r0 == 0) goto L41
            java.nio.charset.Charset r7 = java.nio.charset.Charset.forName(r0)     // Catch: java.lang.IllegalArgumentException -> L41
        L41:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.MediaType.a(java.nio.charset.Charset):java.nio.charset.Charset");
    }

    public boolean equals(Object obj) {
        return (obj instanceof MediaType) && m.areEqual(((MediaType) obj).d, this.d);
    }

    public int hashCode() {
        return this.d.hashCode();
    }

    public String toString() {
        return this.d;
    }
}
