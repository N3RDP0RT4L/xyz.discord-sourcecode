package okhttp3;

import andhook.lib.HookHelper;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import f0.e0.g.c;
import f0.v;
import f0.y;
import java.io.Closeable;
import java.util.Objects;
import kotlin.Metadata;
import okhttp3.Headers;
/* compiled from: Response.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u00002\u00020\u0001:\u0001CB}\b\u0000\u0012\u0006\u0010\u000b\u001a\u00020\b\u0012\u0006\u00108\u001a\u000205\u0012\u0006\u0010/\u001a\u00020\u0005\u0012\u0006\u0010\u0010\u001a\u00020\r\u0012\b\u00103\u001a\u0004\u0018\u000100\u0012\u0006\u0010=\u001a\u00020:\u0012\b\u0010&\u001a\u0004\u0018\u00010#\u0012\b\u0010\u0014\u001a\u0004\u0018\u00010\u0000\u0012\b\u0010@\u001a\u0004\u0018\u00010\u0000\u0012\b\u0010\"\u001a\u0004\u0018\u00010\u0000\u0012\u0006\u0010 \u001a\u00020\u0016\u0012\u0006\u0010\u0019\u001a\u00020\u0016\u0012\b\u0010+\u001a\u0004\u0018\u00010(¢\u0006\u0004\bA\u0010BJ\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0006\u0010\u0007R\u0019\u0010\u000b\u001a\u00020\b8\u0007@\u0006¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\fR\u0019\u0010\u0010\u001a\u00020\r8\u0007@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u00008\u0007@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u0019\u0010\u0019\u001a\u00020\u00168\u0007@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u0013\u0010\u001e\u001a\u00020\u001b8F@\u0006¢\u0006\u0006\u001a\u0004\b\u001c\u0010\u001dR\u0019\u0010 \u001a\u00020\u00168\u0007@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u0018\u001a\u0004\b \u0010\u001aR\u001b\u0010\"\u001a\u0004\u0018\u00010\u00008\u0007@\u0006¢\u0006\f\n\u0004\b!\u0010\u0013\u001a\u0004\b\"\u0010\u0015R\u001b\u0010&\u001a\u0004\u0018\u00010#8\u0007@\u0006¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'R\u001e\u0010+\u001a\u0004\u0018\u00010(8\u0001@\u0000X\u0080\u0004¢\u0006\f\n\u0004\b)\u0010*\u001a\u0004\b+\u0010,R\u0019\u0010/\u001a\u00020\u00058\u0007@\u0006¢\u0006\f\n\u0004\b-\u0010.\u001a\u0004\b/\u0010\u0007R\u001b\u00103\u001a\u0004\u0018\u0001008\u0007@\u0006¢\u0006\f\n\u0004\b1\u00102\u001a\u0004\b3\u00104R\u0019\u00108\u001a\u0002058\u0007@\u0006¢\u0006\f\n\u0004\b6\u00107\u001a\u0004\b8\u00109R\u0019\u0010=\u001a\u00020:8\u0007@\u0006¢\u0006\f\n\u0004\b;\u0010<\u001a\u0004\b=\u0010>R\u001b\u0010@\u001a\u0004\u0018\u00010\u00008\u0007@\u0006¢\u0006\f\n\u0004\b?\u0010\u0013\u001a\u0004\b@\u0010\u0015¨\u0006D"}, d2 = {"Lokhttp3/Response;", "Ljava/io/Closeable;", "", "close", "()V", "", "toString", "()Ljava/lang/String;", "Lokhttp3/Request;", "j", "Lokhttp3/Request;", "request", "()Lokhttp3/Request;", "", "m", "I", ModelAuditLogEntry.CHANGE_KEY_CODE, "()I", "q", "Lokhttp3/Response;", "networkResponse", "()Lokhttp3/Response;", "", "u", "J", "receivedResponseAtMillis", "()J", "", "b", "()Z", "isSuccessful", "t", "sentRequestAtMillis", "s", "priorResponse", "Lokhttp3/ResponseBody;", "p", "Lokhttp3/ResponseBody;", "body", "()Lokhttp3/ResponseBody;", "Lf0/e0/g/c;", "v", "Lf0/e0/g/c;", "exchange", "()Lf0/e0/g/c;", "l", "Ljava/lang/String;", "message", "Lf0/v;", "n", "Lf0/v;", "handshake", "()Lf0/v;", "Lf0/y;", "k", "Lf0/y;", "protocol", "()Lf0/y;", "Lokhttp3/Headers;", "o", "Lokhttp3/Headers;", "headers", "()Lokhttp3/Headers;", "r", "cacheResponse", HookHelper.constructorName, "(Lokhttp3/Request;Lf0/y;Ljava/lang/String;ILf0/v;Lokhttp3/Headers;Lokhttp3/ResponseBody;Lokhttp3/Response;Lokhttp3/Response;Lokhttp3/Response;JJLf0/e0/g/c;)V", "a", "okhttp"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes3.dex */
public final class Response implements Closeable {
    public final Request j;
    public final y k;
    public final String l;
    public final int m;
    public final v n;
    public final Headers o;
    public final ResponseBody p;
    public final Response q;
    public final Response r;

    /* renamed from: s  reason: collision with root package name */
    public final Response f3787s;
    public final long t;
    public final long u;
    public final c v;

    public Response(Request request, y yVar, String str, int i, v vVar, Headers headers, ResponseBody responseBody, Response response, Response response2, Response response3, long j, long j2, c cVar) {
        m.checkParameterIsNotNull(request, "request");
        m.checkParameterIsNotNull(yVar, "protocol");
        m.checkParameterIsNotNull(str, "message");
        m.checkParameterIsNotNull(headers, "headers");
        this.j = request;
        this.k = yVar;
        this.l = str;
        this.m = i;
        this.n = vVar;
        this.o = headers;
        this.p = responseBody;
        this.q = response;
        this.r = response2;
        this.f3787s = response3;
        this.t = j;
        this.u = j2;
        this.v = cVar;
    }

    public static String a(Response response, String str, String str2, int i) {
        int i2 = i & 2;
        Objects.requireNonNull(response);
        m.checkParameterIsNotNull(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        String c = response.o.c(str);
        if (c != null) {
            return c;
        }
        return null;
    }

    public final boolean b() {
        int i = this.m;
        return 200 <= i && 299 >= i;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        ResponseBody responseBody = this.p;
        if (responseBody != null) {
            responseBody.close();
            return;
        }
        throw new IllegalStateException("response is not eligible for a body and must not be closed".toString());
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("Response{protocol=");
        R.append(this.k);
        R.append(", code=");
        R.append(this.m);
        R.append(", message=");
        R.append(this.l);
        R.append(", url=");
        R.append(this.j.f3784b);
        R.append('}');
        return R.toString();
    }

    /* compiled from: Response.kt */
    /* loaded from: classes3.dex */
    public static class a {
        public Request a;

        /* renamed from: b  reason: collision with root package name */
        public y f3788b;
        public int c;
        public String d;
        public v e;
        public Headers.a f;
        public ResponseBody g;
        public Response h;
        public Response i;
        public Response j;
        public long k;
        public long l;
        public c m;

        public a() {
            this.c = -1;
            this.f = new Headers.a();
        }

        public Response a() {
            int i = this.c;
            if (i >= 0) {
                Request request = this.a;
                if (request != null) {
                    y yVar = this.f3788b;
                    if (yVar != null) {
                        String str = this.d;
                        if (str != null) {
                            return new Response(request, yVar, str, i, this.e, this.f.c(), this.g, this.h, this.i, this.j, this.k, this.l, this.m);
                        }
                        throw new IllegalStateException("message == null".toString());
                    }
                    throw new IllegalStateException("protocol == null".toString());
                }
                throw new IllegalStateException("request == null".toString());
            }
            StringBuilder R = b.d.b.a.a.R("code < 0: ");
            R.append(this.c);
            throw new IllegalStateException(R.toString().toString());
        }

        public a b(Response response) {
            c("cacheResponse", response);
            this.i = response;
            return this;
        }

        public final void c(String str, Response response) {
            if (response != null) {
                boolean z2 = false;
                if (response.p == null) {
                    if (response.q == null) {
                        if (response.r == null) {
                            if (response.f3787s == null) {
                                z2 = true;
                            }
                            if (!z2) {
                                throw new IllegalArgumentException(b.d.b.a.a.v(str, ".priorResponse != null").toString());
                            }
                            return;
                        }
                        throw new IllegalArgumentException(b.d.b.a.a.v(str, ".cacheResponse != null").toString());
                    }
                    throw new IllegalArgumentException(b.d.b.a.a.v(str, ".networkResponse != null").toString());
                }
                throw new IllegalArgumentException(b.d.b.a.a.v(str, ".body != null").toString());
            }
        }

        public a d(Headers headers) {
            m.checkParameterIsNotNull(headers, "headers");
            this.f = headers.e();
            return this;
        }

        public a e(String str) {
            m.checkParameterIsNotNull(str, "message");
            this.d = str;
            return this;
        }

        public a f(y yVar) {
            m.checkParameterIsNotNull(yVar, "protocol");
            this.f3788b = yVar;
            return this;
        }

        public a g(Request request) {
            m.checkParameterIsNotNull(request, "request");
            this.a = request;
            return this;
        }

        public a(Response response) {
            m.checkParameterIsNotNull(response, "response");
            this.c = -1;
            this.a = response.j;
            this.f3788b = response.k;
            this.c = response.m;
            this.d = response.l;
            this.e = response.n;
            this.f = response.o.e();
            this.g = response.p;
            this.h = response.q;
            this.i = response.r;
            this.j = response.f3787s;
            this.k = response.t;
            this.l = response.u;
            this.m = response.v;
        }
    }
}
