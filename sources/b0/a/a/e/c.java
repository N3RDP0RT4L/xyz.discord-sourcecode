package b0.a.a.e;

import android.graphics.Bitmap;
import d0.z.d.m;
import java.io.File;
/* compiled from: DefaultConstraint.kt */
/* loaded from: classes3.dex */
public final class c implements b {
    public boolean a;

    /* renamed from: b  reason: collision with root package name */
    public final int f1988b;
    public final int c;
    public final Bitmap.CompressFormat d;
    public final int e;

    public c(int i, int i2, Bitmap.CompressFormat compressFormat, int i3) {
        m.checkParameterIsNotNull(compressFormat, "format");
        this.f1988b = i;
        this.c = i2;
        this.d = compressFormat;
        this.e = i3;
    }

    /* JADX WARN: Removed duplicated region for block: B:36:0x0111  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x0113  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x0164  */
    @Override // b0.a.a.e.b
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public java.io.File a(java.io.File r18) {
        /*
            Method dump skipped, instructions count: 401
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: b0.a.a.e.c.a(java.io.File):java.io.File");
    }

    @Override // b0.a.a.e.b
    public boolean b(File file) {
        m.checkParameterIsNotNull(file, "imageFile");
        return this.a;
    }
}
