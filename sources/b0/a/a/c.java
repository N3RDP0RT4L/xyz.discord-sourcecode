package b0.a.a;

import android.graphics.Bitmap;
/* loaded from: classes3.dex */
public final /* synthetic */ class c {
    public static final /* synthetic */ int[] a;

    static {
        int[] iArr = new int[Bitmap.CompressFormat.values().length];
        a = iArr;
        iArr[Bitmap.CompressFormat.PNG.ordinal()] = 1;
        iArr[Bitmap.CompressFormat.WEBP.ordinal()] = 2;
    }
}
