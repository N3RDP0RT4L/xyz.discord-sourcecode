package me.dm7.barcodescanner.zxing;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import b.i.e.g;
import b.i.e.h;
import com.google.zxing.Result;
import e0.a.a.a.d;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.List;
/* loaded from: classes3.dex */
public class ZXingScannerView extends e0.a.a.a.a {
    public static final List<b.i.e.a> D;
    public g E;
    public List<b.i.e.a> F;
    public b G;

    /* loaded from: classes3.dex */
    public class a implements Runnable {
        public final /* synthetic */ Result j;

        public a(Result result) {
            this.j = result;
        }

        @Override // java.lang.Runnable
        public void run() {
            ZXingScannerView zXingScannerView = ZXingScannerView.this;
            b bVar = zXingScannerView.G;
            zXingScannerView.G = null;
            d dVar = zXingScannerView.k;
            if (dVar != null) {
                dVar.e();
            }
            if (bVar != null) {
                bVar.handleResult(this.j);
            }
        }
    }

    /* loaded from: classes3.dex */
    public interface b {
        void handleResult(Result result);
    }

    static {
        ArrayList arrayList = new ArrayList();
        D = arrayList;
        arrayList.add(b.i.e.a.AZTEC);
        arrayList.add(b.i.e.a.CODABAR);
        arrayList.add(b.i.e.a.CODE_39);
        arrayList.add(b.i.e.a.CODE_93);
        arrayList.add(b.i.e.a.CODE_128);
        arrayList.add(b.i.e.a.DATA_MATRIX);
        arrayList.add(b.i.e.a.EAN_8);
        arrayList.add(b.i.e.a.EAN_13);
        arrayList.add(b.i.e.a.ITF);
        arrayList.add(b.i.e.a.MAXICODE);
        arrayList.add(b.i.e.a.PDF_417);
        arrayList.add(b.i.e.a.QR_CODE);
        arrayList.add(b.i.e.a.RSS_14);
        arrayList.add(b.i.e.a.RSS_EXPANDED);
        arrayList.add(b.i.e.a.UPC_A);
        arrayList.add(b.i.e.a.UPC_E);
        arrayList.add(b.i.e.a.UPC_EAN_EXTENSION);
    }

    public ZXingScannerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        c();
    }

    public h b(byte[] bArr, int i, int i2) {
        Rect rect;
        synchronized (this) {
            if (this.m == null) {
                Rect framingRect = ((e0.a.a.a.g) this.l).getFramingRect();
                int width = this.l.getWidth();
                int height = this.l.getHeight();
                if (!(framingRect == null || width == 0 || height == 0)) {
                    Rect rect2 = new Rect(framingRect);
                    if (i < width) {
                        rect2.left = (rect2.left * i) / width;
                        rect2.right = (rect2.right * i) / width;
                    }
                    if (i2 < height) {
                        rect2.top = (rect2.top * i2) / height;
                        rect2.bottom = (rect2.bottom * i2) / height;
                    }
                    this.m = rect2;
                }
                rect = null;
            }
            rect = this.m;
        }
        if (rect == null) {
            return null;
        }
        try {
            return new h(bArr, i, i2, rect.left, rect.top, rect.width(), rect.height(), false);
        } catch (Exception unused) {
            return null;
        }
    }

    public final void c() {
        EnumMap enumMap = new EnumMap(b.i.e.d.class);
        enumMap.put((EnumMap) b.i.e.d.POSSIBLE_FORMATS, (b.i.e.d) getFormats());
        g gVar = new g();
        this.E = gVar;
        gVar.c(enumMap);
    }

    public Collection<b.i.e.a> getFormats() {
        List<b.i.e.a> list = this.F;
        return list == null ? D : list;
    }

    /* JADX WARN: Finally extract failed */
    /* JADX WARN: Removed duplicated region for block: B:29:0x0079 A[Catch: RuntimeException -> 0x00ee, TRY_LEAVE, TryCatch #3 {RuntimeException -> 0x00ee, blocks: (B:5:0x0007, B:7:0x001e, B:12:0x002b, B:19:0x004b, B:23:0x0054, B:24:0x0063, B:25:0x0066, B:27:0x0072, B:29:0x0079, B:34:0x0090, B:36:0x0097, B:37:0x009c, B:38:0x009d, B:40:0x00a5, B:45:0x00c1, B:47:0x00c8, B:48:0x00cd, B:49:0x00ce, B:52:0x00d6, B:53:0x00e8, B:41:0x00b4, B:43:0x00ba, B:44:0x00bd, B:30:0x0083, B:32:0x0089, B:33:0x008c), top: B:57:0x0007, inners: #5, #4 }] */
    /* JADX WARN: Removed duplicated region for block: B:52:0x00d6 A[Catch: RuntimeException -> 0x00ee, TryCatch #3 {RuntimeException -> 0x00ee, blocks: (B:5:0x0007, B:7:0x001e, B:12:0x002b, B:19:0x004b, B:23:0x0054, B:24:0x0063, B:25:0x0066, B:27:0x0072, B:29:0x0079, B:34:0x0090, B:36:0x0097, B:37:0x009c, B:38:0x009d, B:40:0x00a5, B:45:0x00c1, B:47:0x00c8, B:48:0x00cd, B:49:0x00ce, B:52:0x00d6, B:53:0x00e8, B:41:0x00b4, B:43:0x00ba, B:44:0x00bd, B:30:0x0083, B:32:0x0089, B:33:0x008c), top: B:57:0x0007, inners: #5, #4 }] */
    /* JADX WARN: Removed duplicated region for block: B:53:0x00e8 A[Catch: RuntimeException -> 0x00ee, TRY_LEAVE, TryCatch #3 {RuntimeException -> 0x00ee, blocks: (B:5:0x0007, B:7:0x001e, B:12:0x002b, B:19:0x004b, B:23:0x0054, B:24:0x0063, B:25:0x0066, B:27:0x0072, B:29:0x0079, B:34:0x0090, B:36:0x0097, B:37:0x009c, B:38:0x009d, B:40:0x00a5, B:45:0x00c1, B:47:0x00c8, B:48:0x00cd, B:49:0x00ce, B:52:0x00d6, B:53:0x00e8, B:41:0x00b4, B:43:0x00ba, B:44:0x00bd, B:30:0x0083, B:32:0x0089, B:33:0x008c), top: B:57:0x0007, inners: #5, #4 }] */
    @Override // android.hardware.Camera.PreviewCallback
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void onPreviewFrame(byte[] r17, android.hardware.Camera r18) {
        /*
            Method dump skipped, instructions count: 249
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: me.dm7.barcodescanner.zxing.ZXingScannerView.onPreviewFrame(byte[], android.hardware.Camera):void");
    }

    public void setFormats(List<b.i.e.a> list) {
        this.F = list;
        c();
    }

    public void setResultHandler(b bVar) {
        this.G = bVar;
    }
}
