package me.dm7.barcodescanner.core;
/* loaded from: classes3.dex */
public final class R {

    /* loaded from: classes3.dex */
    public static final class a {
        public static final int notification_action_color_filter = 0x7f0601c5;
        public static final int notification_icon_bg_color = 0x7f0601c6;
        public static final int notification_material_background_media_default_color = 0x7f0601c7;
        public static final int primary_text_default_material_dark = 0x7f06024f;
        public static final int ripple_material_light = 0x7f060257;
        public static final int secondary_text_default_material_dark = 0x7f06025a;
        public static final int secondary_text_default_material_light = 0x7f06025b;
        public static final int viewfinder_border = 0x7f060325;
        public static final int viewfinder_laser = 0x7f060326;
        public static final int viewfinder_mask = 0x7f060327;

        private a() {
        }
    }

    /* loaded from: classes3.dex */
    public static final class b {
        public static final int cancel_button_image_alpha = 0x7f0b0007;
        public static final int status_bar_notification_info_maxnum = 0x7f0b001b;
        public static final int viewfinder_border_length = 0x7f0b0025;
        public static final int viewfinder_border_width = 0x7f0b0026;

        private b() {
        }
    }

    /* loaded from: classes3.dex */
    public static final class c {
        public static final int BarcodeScannerView_borderAlpha = 0x00000000;
        public static final int BarcodeScannerView_borderColor = 0x00000001;
        public static final int BarcodeScannerView_borderLength = 0x00000002;
        public static final int BarcodeScannerView_borderWidth = 0x00000003;
        public static final int BarcodeScannerView_cornerRadius = 0x00000004;
        public static final int BarcodeScannerView_finderOffset = 0x00000005;
        public static final int BarcodeScannerView_laserColor = 0x00000006;
        public static final int BarcodeScannerView_laserEnabled = 0x00000007;
        public static final int BarcodeScannerView_maskColor = 0x00000008;
        public static final int BarcodeScannerView_roundedCorner = 0x00000009;
        public static final int BarcodeScannerView_shouldScaleToFill = 0x0000000a;
        public static final int BarcodeScannerView_squaredFinder = 0x0000000b;
        public static final int ColorStateListItem_alpha = 0x00000002;
        public static final int ColorStateListItem_android_alpha = 0x00000001;
        public static final int ColorStateListItem_android_color = 0x00000000;
        public static final int CoordinatorLayout_Layout_android_layout_gravity = 0x00000000;
        public static final int CoordinatorLayout_Layout_layout_anchor = 0x00000001;
        public static final int CoordinatorLayout_Layout_layout_anchorGravity = 0x00000002;
        public static final int CoordinatorLayout_Layout_layout_behavior = 0x00000003;
        public static final int CoordinatorLayout_Layout_layout_dodgeInsetEdges = 0x00000004;
        public static final int CoordinatorLayout_Layout_layout_insetEdge = 0x00000005;
        public static final int CoordinatorLayout_Layout_layout_keyline = 0x00000006;
        public static final int CoordinatorLayout_keylines = 0x00000000;
        public static final int CoordinatorLayout_statusBarBackground = 0x00000001;
        public static final int FontFamilyFont_android_font = 0x00000000;
        public static final int FontFamilyFont_android_fontStyle = 0x00000002;
        public static final int FontFamilyFont_android_fontVariationSettings = 0x00000004;
        public static final int FontFamilyFont_android_fontWeight = 0x00000001;
        public static final int FontFamilyFont_android_ttcIndex = 0x00000003;
        public static final int FontFamilyFont_font = 0x00000005;
        public static final int FontFamilyFont_fontStyle = 0x00000006;
        public static final int FontFamilyFont_fontVariationSettings = 0x00000007;
        public static final int FontFamilyFont_fontWeight = 0x00000008;
        public static final int FontFamilyFont_ttcIndex = 0x00000009;
        public static final int FontFamily_fontProviderAuthority = 0x00000000;
        public static final int FontFamily_fontProviderCerts = 0x00000001;
        public static final int FontFamily_fontProviderFetchStrategy = 0x00000002;
        public static final int FontFamily_fontProviderFetchTimeout = 0x00000003;
        public static final int FontFamily_fontProviderPackage = 0x00000004;
        public static final int FontFamily_fontProviderQuery = 0x00000005;
        public static final int FontFamily_fontProviderSystemFontFamily = 0x00000006;
        public static final int GradientColorItem_android_color = 0x00000000;
        public static final int GradientColorItem_android_offset = 0x00000001;
        public static final int GradientColor_android_centerColor = 0x00000007;
        public static final int GradientColor_android_centerX = 0x00000003;
        public static final int GradientColor_android_centerY = 0x00000004;
        public static final int GradientColor_android_endColor = 0x00000001;
        public static final int GradientColor_android_endX = 0x0000000a;
        public static final int GradientColor_android_endY = 0x0000000b;
        public static final int GradientColor_android_gradientRadius = 0x00000005;
        public static final int GradientColor_android_startColor = 0x00000000;
        public static final int GradientColor_android_startX = 0x00000008;
        public static final int GradientColor_android_startY = 0x00000009;
        public static final int GradientColor_android_tileMode = 0x00000006;
        public static final int GradientColor_android_type = 0x00000002;
        public static final int[] BarcodeScannerView = {xyz.discord.R.attr.borderAlpha, xyz.discord.R.attr.borderColor, xyz.discord.R.attr.borderLength, xyz.discord.R.attr.borderWidth, xyz.discord.R.attr.cornerRadius, xyz.discord.R.attr.finderOffset, xyz.discord.R.attr.laserColor, xyz.discord.R.attr.laserEnabled, xyz.discord.R.attr.maskColor, xyz.discord.R.attr.roundedCorner, xyz.discord.R.attr.shouldScaleToFill, xyz.discord.R.attr.squaredFinder};
        public static final int[] ColorStateListItem = {16843173, 16843551, xyz.discord.R.attr.alpha};
        public static final int[] CoordinatorLayout = {xyz.discord.R.attr.keylines, xyz.discord.R.attr.statusBarBackground};
        public static final int[] CoordinatorLayout_Layout = {16842931, xyz.discord.R.attr.layout_anchor, xyz.discord.R.attr.layout_anchorGravity, xyz.discord.R.attr.layout_behavior, xyz.discord.R.attr.layout_dodgeInsetEdges, xyz.discord.R.attr.layout_insetEdge, xyz.discord.R.attr.layout_keyline};
        public static final int[] FontFamily = {xyz.discord.R.attr.fontProviderAuthority, xyz.discord.R.attr.fontProviderCerts, xyz.discord.R.attr.fontProviderFetchStrategy, xyz.discord.R.attr.fontProviderFetchTimeout, xyz.discord.R.attr.fontProviderPackage, xyz.discord.R.attr.fontProviderQuery, xyz.discord.R.attr.fontProviderSystemFontFamily};
        public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, 16844143, 16844144, xyz.discord.R.attr.font, xyz.discord.R.attr.fontStyle, xyz.discord.R.attr.fontVariationSettings, xyz.discord.R.attr.fontWeight, xyz.discord.R.attr.ttcIndex};
        public static final int[] GradientColor = {16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051};
        public static final int[] GradientColorItem = {16843173, 16844052};

        private c() {
        }
    }

    private R() {
    }
}
