package e0.a.a.a;

import android.hardware.Camera;
import androidx.annotation.NonNull;
/* compiled from: CameraWrapper.java */
/* loaded from: classes3.dex */
public class e {
    public final Camera a;

    /* renamed from: b  reason: collision with root package name */
    public final int f3570b;

    public e(@NonNull Camera camera, int i) {
        this.a = camera;
        this.f3570b = i;
    }
}
