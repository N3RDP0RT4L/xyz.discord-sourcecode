package e0.a.a.a;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.View;
import b.i.a.f.e.o.f;
import me.dm7.barcodescanner.core.R;
import org.objectweb.asm.Opcodes;
/* compiled from: ViewFinderView.java */
/* loaded from: classes3.dex */
public class g extends View implements f {
    public static final int[] j = {0, 64, 128, Opcodes.CHECKCAST, 255, Opcodes.CHECKCAST, 128, 64};
    public Rect k;
    public int l;
    public final int m;
    public final int n;
    public final int o;
    public final int p;
    public final int q;
    public Paint r;

    /* renamed from: s  reason: collision with root package name */
    public Paint f3571s;
    public Paint t;
    public int u;
    public boolean v;
    public boolean w;

    /* renamed from: x  reason: collision with root package name */
    public int f3572x = 0;

    public g(Context context) {
        super(context);
        int color = getResources().getColor(R.a.viewfinder_laser);
        this.m = color;
        int color2 = getResources().getColor(R.a.viewfinder_mask);
        this.n = color2;
        int color3 = getResources().getColor(R.a.viewfinder_border);
        this.o = color3;
        int integer = getResources().getInteger(R.b.viewfinder_border_width);
        this.p = integer;
        int integer2 = getResources().getInteger(R.b.viewfinder_border_length);
        this.q = integer2;
        Paint paint = new Paint();
        this.r = paint;
        paint.setColor(color);
        this.r.setStyle(Paint.Style.FILL);
        Paint paint2 = new Paint();
        this.f3571s = paint2;
        paint2.setColor(color2);
        Paint paint3 = new Paint();
        this.t = paint3;
        paint3.setColor(color3);
        this.t.setStyle(Paint.Style.STROKE);
        this.t.setStrokeWidth(integer);
        this.t.setAntiAlias(true);
        this.u = integer2;
    }

    public synchronized void a() {
        int i;
        int i2;
        int i3;
        Point point = new Point(getWidth(), getHeight());
        int q0 = f.q0(getContext());
        if (this.v) {
            if (q0 != 1) {
                i3 = getHeight();
            } else {
                i3 = getWidth();
            }
            i2 = (int) (i3 * 0.625f);
            i = i2;
        } else if (q0 != 1) {
            int height = (int) (getHeight() * 0.625f);
            i2 = (int) (height * 1.4f);
            i = height;
        } else {
            i2 = (int) (getWidth() * 0.75f);
            i = (int) (i2 * 0.75f);
        }
        if (i2 > getWidth()) {
            i2 = getWidth() - 50;
        }
        if (i > getHeight()) {
            i = getHeight() - 50;
        }
        int i4 = (point.x - i2) / 2;
        int i5 = (point.y - i) / 2;
        int i6 = this.f3572x;
        this.k = new Rect(i4 + i6, i5 + i6, (i4 + i2) - i6, (i5 + i) - i6);
    }

    public Rect getFramingRect() {
        return this.k;
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        if (getFramingRect() != null) {
            int width = canvas.getWidth();
            int height = canvas.getHeight();
            Rect framingRect = getFramingRect();
            float f = width;
            canvas.drawRect(0.0f, 0.0f, f, framingRect.top, this.f3571s);
            canvas.drawRect(0.0f, framingRect.top, framingRect.left, framingRect.bottom + 1, this.f3571s);
            canvas.drawRect(framingRect.right + 1, framingRect.top, f, framingRect.bottom + 1, this.f3571s);
            canvas.drawRect(0.0f, framingRect.bottom + 1, f, height, this.f3571s);
            Rect framingRect2 = getFramingRect();
            Path path = new Path();
            path.moveTo(framingRect2.left, framingRect2.top + this.u);
            path.lineTo(framingRect2.left, framingRect2.top);
            path.lineTo(framingRect2.left + this.u, framingRect2.top);
            canvas.drawPath(path, this.t);
            path.moveTo(framingRect2.right, framingRect2.top + this.u);
            path.lineTo(framingRect2.right, framingRect2.top);
            path.lineTo(framingRect2.right - this.u, framingRect2.top);
            canvas.drawPath(path, this.t);
            path.moveTo(framingRect2.right, framingRect2.bottom - this.u);
            path.lineTo(framingRect2.right, framingRect2.bottom);
            path.lineTo(framingRect2.right - this.u, framingRect2.bottom);
            canvas.drawPath(path, this.t);
            path.moveTo(framingRect2.left, framingRect2.bottom - this.u);
            path.lineTo(framingRect2.left, framingRect2.bottom);
            path.lineTo(framingRect2.left + this.u, framingRect2.bottom);
            canvas.drawPath(path, this.t);
            if (this.w) {
                Rect framingRect3 = getFramingRect();
                Paint paint = this.r;
                int[] iArr = j;
                paint.setAlpha(iArr[this.l]);
                this.l = (this.l + 1) % iArr.length;
                int height2 = (framingRect3.height() / 2) + framingRect3.top;
                canvas.drawRect(framingRect3.left + 2, height2 - 1, framingRect3.right - 1, height2 + 2, this.r);
                postInvalidateDelayed(80L, framingRect3.left - 10, framingRect3.top - 10, framingRect3.right + 10, framingRect3.bottom + 10);
            }
        }
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        a();
    }

    public void setBorderAlpha(float f) {
        this.t.setAlpha((int) (f * 255.0f));
    }

    public void setBorderColor(int i) {
        this.t.setColor(i);
    }

    public void setBorderCornerRadius(int i) {
        this.t.setPathEffect(new CornerPathEffect(i));
    }

    public void setBorderCornerRounded(boolean z2) {
        if (z2) {
            this.t.setStrokeJoin(Paint.Join.ROUND);
        } else {
            this.t.setStrokeJoin(Paint.Join.BEVEL);
        }
    }

    public void setBorderLineLength(int i) {
        this.u = i;
    }

    public void setBorderStrokeWidth(int i) {
        this.t.setStrokeWidth(i);
    }

    public void setLaserColor(int i) {
        this.r.setColor(i);
    }

    public void setLaserEnabled(boolean z2) {
        this.w = z2;
    }

    public void setMaskColor(int i) {
        this.f3571s.setColor(i);
    }

    public void setSquareViewFinder(boolean z2) {
        this.v = z2;
    }

    public void setViewFinderOffset(int i) {
        this.f3572x = i;
    }
}
