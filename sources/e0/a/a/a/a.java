package e0.a.a.a;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import androidx.annotation.ColorInt;
import androidx.core.view.ViewCompat;
import b.i.a.f.e.o.f;
import me.dm7.barcodescanner.core.R;
/* compiled from: BarcodeScannerView.java */
/* loaded from: classes3.dex */
public abstract class a extends FrameLayout implements Camera.PreviewCallback {
    public float A;
    public int B;
    public e j;
    public d k;
    public f l;
    public Rect m;
    public c n;
    public Boolean o;
    public boolean r;
    @ColorInt

    /* renamed from: s  reason: collision with root package name */
    public int f3565s;
    @ColorInt
    public int t;
    public int u;
    public int v;
    public int w;

    /* renamed from: x  reason: collision with root package name */
    public boolean f3566x;

    /* renamed from: y  reason: collision with root package name */
    public int f3567y;

    /* renamed from: z  reason: collision with root package name */
    public boolean f3568z;
    public boolean p = true;
    public boolean q = true;
    public float C = 0.1f;

    /* JADX WARN: Finally extract failed */
    public a(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.r = true;
        this.f3565s = getResources().getColor(R.a.viewfinder_laser);
        this.t = getResources().getColor(R.a.viewfinder_border);
        this.u = getResources().getColor(R.a.viewfinder_mask);
        this.v = getResources().getInteger(R.b.viewfinder_border_width);
        this.w = getResources().getInteger(R.b.viewfinder_border_length);
        this.f3566x = false;
        this.f3567y = 0;
        this.f3568z = false;
        this.A = 1.0f;
        this.B = 0;
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.c.BarcodeScannerView, 0, 0);
        try {
            setShouldScaleToFill(obtainStyledAttributes.getBoolean(R.c.BarcodeScannerView_shouldScaleToFill, true));
            this.r = obtainStyledAttributes.getBoolean(R.c.BarcodeScannerView_laserEnabled, this.r);
            this.f3565s = obtainStyledAttributes.getColor(R.c.BarcodeScannerView_laserColor, this.f3565s);
            this.t = obtainStyledAttributes.getColor(R.c.BarcodeScannerView_borderColor, this.t);
            this.u = obtainStyledAttributes.getColor(R.c.BarcodeScannerView_maskColor, this.u);
            this.v = obtainStyledAttributes.getDimensionPixelSize(R.c.BarcodeScannerView_borderWidth, this.v);
            this.w = obtainStyledAttributes.getDimensionPixelSize(R.c.BarcodeScannerView_borderLength, this.w);
            this.f3566x = obtainStyledAttributes.getBoolean(R.c.BarcodeScannerView_roundedCorner, this.f3566x);
            this.f3567y = obtainStyledAttributes.getDimensionPixelSize(R.c.BarcodeScannerView_cornerRadius, this.f3567y);
            this.f3568z = obtainStyledAttributes.getBoolean(R.c.BarcodeScannerView_squaredFinder, this.f3568z);
            this.A = obtainStyledAttributes.getFloat(R.c.BarcodeScannerView_borderAlpha, this.A);
            this.B = obtainStyledAttributes.getDimensionPixelSize(R.c.BarcodeScannerView_finderOffset, this.B);
            obtainStyledAttributes.recycle();
            g gVar = new g(getContext());
            gVar.setBorderColor(this.t);
            gVar.setLaserColor(this.f3565s);
            gVar.setLaserEnabled(this.r);
            gVar.setBorderStrokeWidth(this.v);
            gVar.setBorderLineLength(this.w);
            gVar.setMaskColor(this.u);
            gVar.setBorderCornerRounded(this.f3566x);
            gVar.setBorderCornerRadius(this.f3567y);
            gVar.setSquareViewFinder(this.f3568z);
            gVar.setViewFinderOffset(this.B);
            this.l = gVar;
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }

    public void a() {
        if (this.j != null) {
            this.k.e();
            d dVar = this.k;
            dVar.j = null;
            dVar.p = null;
            this.j.a.release();
            this.j = null;
        }
        c cVar = this.n;
        if (cVar != null) {
            cVar.quit();
            this.n = null;
        }
    }

    public boolean getFlash() {
        e eVar = this.j;
        return eVar != null && f.C0(eVar.a) && this.j.a.getParameters().getFlashMode().equals("torch");
    }

    public int getRotationCount() {
        return this.k.getDisplayOrientation() / 90;
    }

    public void setAspectTolerance(float f) {
        this.C = f;
    }

    public void setAutoFocus(boolean z2) {
        this.p = z2;
        d dVar = this.k;
        if (dVar != null) {
            dVar.setAutoFocus(z2);
        }
    }

    public void setBorderAlpha(float f) {
        this.A = f;
        ((g) this.l).setBorderAlpha(f);
        g gVar = (g) this.l;
        gVar.a();
        gVar.invalidate();
    }

    public void setBorderColor(int i) {
        this.t = i;
        ((g) this.l).setBorderColor(i);
        g gVar = (g) this.l;
        gVar.a();
        gVar.invalidate();
    }

    public void setBorderCornerRadius(int i) {
        this.f3567y = i;
        ((g) this.l).setBorderCornerRadius(i);
        g gVar = (g) this.l;
        gVar.a();
        gVar.invalidate();
    }

    public void setBorderLineLength(int i) {
        this.w = i;
        ((g) this.l).setBorderLineLength(i);
        g gVar = (g) this.l;
        gVar.a();
        gVar.invalidate();
    }

    public void setBorderStrokeWidth(int i) {
        this.v = i;
        ((g) this.l).setBorderStrokeWidth(i);
        g gVar = (g) this.l;
        gVar.a();
        gVar.invalidate();
    }

    public void setFlash(boolean z2) {
        this.o = Boolean.valueOf(z2);
        e eVar = this.j;
        if (eVar != null && f.C0(eVar.a)) {
            Camera.Parameters parameters = this.j.a.getParameters();
            if (z2) {
                if (!parameters.getFlashMode().equals("torch")) {
                    parameters.setFlashMode("torch");
                } else {
                    return;
                }
            } else if (!parameters.getFlashMode().equals("off")) {
                parameters.setFlashMode("off");
            } else {
                return;
            }
            this.j.a.setParameters(parameters);
        }
    }

    public void setIsBorderCornerRounded(boolean z2) {
        this.f3566x = z2;
        ((g) this.l).setBorderCornerRounded(z2);
        g gVar = (g) this.l;
        gVar.a();
        gVar.invalidate();
    }

    public void setLaserColor(int i) {
        this.f3565s = i;
        ((g) this.l).setLaserColor(i);
        g gVar = (g) this.l;
        gVar.a();
        gVar.invalidate();
    }

    public void setLaserEnabled(boolean z2) {
        this.r = z2;
        ((g) this.l).setLaserEnabled(z2);
        g gVar = (g) this.l;
        gVar.a();
        gVar.invalidate();
    }

    public void setMaskColor(int i) {
        this.u = i;
        ((g) this.l).setMaskColor(i);
        g gVar = (g) this.l;
        gVar.a();
        gVar.invalidate();
    }

    public void setShouldScaleToFill(boolean z2) {
        this.q = z2;
    }

    public void setSquareViewFinder(boolean z2) {
        this.f3568z = z2;
        ((g) this.l).setSquareViewFinder(z2);
        g gVar = (g) this.l;
        gVar.a();
        gVar.invalidate();
    }

    public void setupCameraPreview(e eVar) {
        this.j = eVar;
        if (eVar != null) {
            setupLayout(eVar);
            g gVar = (g) this.l;
            gVar.a();
            gVar.invalidate();
            Boolean bool = this.o;
            if (bool != null) {
                setFlash(bool.booleanValue());
            }
            setAutoFocus(this.p);
        }
    }

    public final void setupLayout(e eVar) {
        removeAllViews();
        d dVar = new d(getContext(), eVar, this);
        this.k = dVar;
        dVar.setAspectTolerance(this.C);
        this.k.setShouldScaleToFill(this.q);
        if (!this.q) {
            RelativeLayout relativeLayout = new RelativeLayout(getContext());
            relativeLayout.setGravity(17);
            relativeLayout.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
            relativeLayout.addView(this.k);
            addView(relativeLayout);
        } else {
            addView(this.k);
        }
        f fVar = this.l;
        if (fVar instanceof View) {
            addView((View) fVar);
            return;
        }
        throw new IllegalArgumentException("IViewFinder object returned by 'createViewFinderView()' should be instance of android.view.View");
    }
}
