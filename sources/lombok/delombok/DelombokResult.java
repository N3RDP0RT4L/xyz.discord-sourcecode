package lombok.delombok;

import com.sun.tools.javac.tree.JCTree;
import java.io.IOException;
import java.io.Writer;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.tools.JavaFileObject;
import lombok.javac.CommentInfo;
/* loaded from: xyz.discord_v112014.apk:lombok/delombok/DelombokResult.SCL.lombok */
public class DelombokResult {
    private final List<CommentInfo> comments;
    private final List<Integer> textBlockStarts;
    private final JCTree.JCCompilationUnit compilationUnit;
    private final boolean changed;
    private final FormatPreferences formatPreferences;

    public DelombokResult(List<CommentInfo> comments, List<Integer> textBlockStarts, JCTree.JCCompilationUnit compilationUnit, boolean changed, FormatPreferences formatPreferences) {
        this.comments = comments;
        this.textBlockStarts = textBlockStarts;
        this.compilationUnit = compilationUnit;
        this.changed = changed;
        this.formatPreferences = formatPreferences;
    }

    public void print(Writer out) throws IOException {
        CharSequence content;
        if (this.changed || (content = getContent()) == null) {
            if (this.formatPreferences.generateDelombokComment()) {
                out.write("// Generated by delombok at ");
                out.write(String.valueOf(new Date()));
                out.write(System.getProperty("line.separator"));
            }
            com.sun.tools.javac.util.List<CommentInfo> comments_ = this.comments instanceof com.sun.tools.javac.util.List ? (com.sun.tools.javac.util.List) this.comments : com.sun.tools.javac.util.List.from((CommentInfo[]) this.comments.toArray(new CommentInfo[0]));
            int[] textBlockStarts_ = new int[this.textBlockStarts.size()];
            int idx = 0;
            Iterator<Integer> it = this.textBlockStarts.iterator();
            while (it.hasNext()) {
                int tbs = it.next().intValue();
                int i = idx;
                idx++;
                textBlockStarts_[i] = tbs;
            }
            FormatPreferences preferences = new FormatPreferenceScanner().scan(this.formatPreferences, getContent());
            this.compilationUnit.accept(new PrettyPrinter(out, this.compilationUnit, comments_, textBlockStarts_, preferences));
            return;
        }
        out.write(content.toString());
    }

    private CharSequence getContent() throws IOException {
        JavaFileObject sourceFile = this.compilationUnit.getSourceFile();
        if (sourceFile == null) {
            return null;
        }
        return sourceFile.getCharContent(true);
    }

    public boolean isChanged() {
        return this.changed;
    }
}
