package lombok.patcher.scripts;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import lombok.patcher.PatchScript;
import lombok.patcher.TargetMatcher;
import lombok.patcher.TransplantMapper;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
/* loaded from: xyz.discord_v112014.apk:lombok/patcher/scripts/MethodLevelPatchScript.SCL.lombok */
public abstract class MethodLevelPatchScript extends PatchScript {
    private final Set<String> affectedClasses;
    private final Collection<TargetMatcher> matchers;

    protected abstract PatchScript.MethodPatcher createPatcher(ClassWriter classWriter, String str, TransplantMapper transplantMapper);

    public String describeMatchers() {
        if (this.matchers.size() == 0) {
            return "(No matchers)";
        }
        if (this.matchers.size() == 1) {
            return this.matchers.iterator().next().describe();
        }
        StringBuilder out = new StringBuilder("(");
        Iterator<TargetMatcher> it = this.matchers.iterator();
        while (it.hasNext()) {
            TargetMatcher tm = it.next();
            out.append(tm.describe()).append(", ");
        }
        out.setLength(out.length() - 2);
        return out.append(")").toString();
    }

    public MethodLevelPatchScript(Collection<TargetMatcher> matchers) {
        this.matchers = matchers;
        Set<String> affected = new HashSet<>();
        Iterator<TargetMatcher> it = matchers.iterator();
        while (it.hasNext()) {
            TargetMatcher t = it.next();
            affected.addAll(t.getAffectedClasses());
        }
        this.affectedClasses = Collections.unmodifiableSet(affected);
    }

    @Override // lombok.patcher.PatchScript
    public Collection<String> getClassesToReload() {
        return this.affectedClasses;
    }

    @Override // lombok.patcher.PatchScript
    public boolean wouldPatch(String className) {
        return classMatches(className, this.affectedClasses);
    }

    @Override // lombok.patcher.PatchScript
    public byte[] patch(String className, byte[] byteCode, TransplantMapper transplantMapper) {
        if (!classMatches(className, this.affectedClasses)) {
            return null;
        }
        return runASM(byteCode, true, transplantMapper);
    }

    @Override // lombok.patcher.PatchScript
    protected final ClassVisitor createClassVisitor(ClassWriter writer, String classSpec, TransplantMapper transplantMapper) {
        PatchScript.MethodPatcher patcher = createPatcher(writer, classSpec, transplantMapper);
        Iterator<TargetMatcher> it = this.matchers.iterator();
        while (it.hasNext()) {
            TargetMatcher matcher = it.next();
            patcher.addTargetMatcher(matcher);
        }
        return patcher;
    }
}
