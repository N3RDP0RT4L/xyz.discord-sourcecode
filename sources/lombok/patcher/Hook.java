package lombok.patcher;

import andhook.lib.HookHelper;
import androidx.exifinterface.media.ExifInterface;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.swift.sandhook.annotation.MethodReflectParams;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
/* loaded from: xyz.discord_v112014.apk:lombok/patcher/Hook.SCL.lombok */
public class Hook {
    private final String className;
    private final String methodName;
    private final String returnType;
    private final List<String> parameterTypes;
    private static final Map<String, String> PRIMITIVES;

    public Hook(String className, String methodName, String returnType, String... parameterTypes) {
        if (className == null) {
            throw new NullPointerException("classSpec");
        } else if (methodName == null) {
            throw new NullPointerException("methodName");
        } else if (returnType == null) {
            throw new NullPointerException("returnType");
        } else if (parameterTypes == null) {
            throw new NullPointerException("parameterTypes");
        } else {
            this.className = className;
            this.methodName = methodName;
            this.returnType = returnType;
            List<String> params = new ArrayList<>();
            for (String param : parameterTypes) {
                params.add(param);
            }
            this.parameterTypes = Collections.unmodifiableList(params);
        }
    }

    public boolean isConstructor() {
        return HookHelper.constructorName.equals(this.methodName);
    }

    public String getClassName() {
        return this.className;
    }

    public String getMethodName() {
        return this.methodName;
    }

    public String getReturnType() {
        return this.returnType;
    }

    public List<String> getParameterTypes() {
        return this.parameterTypes;
    }

    public String getClassSpec() {
        return convertType(this.className);
    }

    public String getMethodDescriptor() {
        StringBuilder out = new StringBuilder();
        out.append("(");
        Iterator<String> it = this.parameterTypes.iterator();
        while (it.hasNext()) {
            String p = it.next();
            out.append(toSpec(p));
        }
        out.append(")");
        out.append(toSpec(this.returnType));
        return out.toString();
    }

    static {
        Map<String, String> m = new HashMap<>();
        m.put(MethodReflectParams.INT, "I");
        m.put("long", "J");
        m.put(MethodReflectParams.SHORT, ExifInterface.LATITUDE_SOUTH);
        m.put(MethodReflectParams.BYTE, "B");
        m.put(MethodReflectParams.CHAR, "C");
        m.put(MethodReflectParams.DOUBLE, "D");
        m.put(MethodReflectParams.FLOAT, "F");
        m.put("void", ExifInterface.GPS_MEASUREMENT_INTERRUPTED);
        m.put(MethodReflectParams.BOOLEAN, "Z");
        PRIMITIVES = Collections.unmodifiableMap(m);
    }

    public static String toSpec(String type) {
        StringBuilder out = new StringBuilder();
        while (type.endsWith("[]")) {
            type = type.substring(0, type.length() - 2);
            out.append("[");
        }
        String p = PRIMITIVES.get(type);
        if (p != null) {
            out.append(p);
            return out.toString();
        }
        out.append("L");
        out.append(convertType(type));
        out.append(';');
        return out.toString();
    }

    public static String convertType(String type) {
        String[] split;
        StringBuilder out = new StringBuilder();
        for (String part : type.split("\\.")) {
            if (out.length() > 0) {
                out.append(MentionUtilsKt.SLASH_CHAR);
            }
            out.append(part);
        }
        return out.toString();
    }

    public int hashCode() {
        int result = (31 * 1) + (this.className == null ? 0 : this.className.hashCode());
        return (31 * ((31 * ((31 * result) + (this.methodName == null ? 0 : this.methodName.hashCode()))) + (this.parameterTypes == null ? 0 : this.parameterTypes.hashCode()))) + (this.returnType == null ? 0 : this.returnType.hashCode());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Hook other = (Hook) obj;
        if (this.className == null) {
            if (other.className != null) {
                return false;
            }
        } else if (!this.className.equals(other.className)) {
            return false;
        }
        if (this.methodName == null) {
            if (other.methodName != null) {
                return false;
            }
        } else if (!this.methodName.equals(other.methodName)) {
            return false;
        }
        if (this.parameterTypes == null) {
            if (other.parameterTypes != null) {
                return false;
            }
        } else if (!this.parameterTypes.equals(other.parameterTypes)) {
            return false;
        }
        return this.returnType == null ? other.returnType == null : this.returnType.equals(other.returnType);
    }

    public String toString() {
        return "Hook [className=" + this.className + ", methodName=" + this.methodName + ", returnType=" + this.returnType + ", parameterTypes=" + this.parameterTypes + "]";
    }
}
