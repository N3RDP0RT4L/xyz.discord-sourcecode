package lombok.javac;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
/* loaded from: xyz.discord_v112014.apk:lombok/javac/ResolutionResetNeeded.SCL.lombok */
public @interface ResolutionResetNeeded {
}
