package lombok.installer;

import com.zwitserloot.cmdreader.CmdReader;
import com.zwitserloot.cmdreader.Description;
import com.zwitserloot.cmdreader.InvalidCommandLineException;
import com.zwitserloot.cmdreader.Sequential;
import com.zwitserloot.cmdreader.Shorthand;
import java.awt.HeadlessException;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import lombok.Lombok;
import lombok.core.LombokApp;
import lombok.core.SpiLoadUtil;
import lombok.core.Version;
import lombok.installer.OsUtils;
import lombok.patcher.ClassRootFinder;
/* loaded from: xyz.discord_v112014.apk:lombok/installer/Installer.SCL.lombok */
public class Installer {
    static final URI ABOUT_LOMBOK_URL = URI.create("https://projectlombok.org");
    static final List<IdeLocationProvider> locationProviders;

    static {
        List<IdeLocationProvider> list = new ArrayList<>();
        try {
            Iterator it = SpiLoadUtil.findServices(IdeLocationProvider.class).iterator();
            while (it.hasNext()) {
                IdeLocationProvider provider = (IdeLocationProvider) it.next();
                list.add(provider);
            }
            locationProviders = Collections.unmodifiableList(list);
        } catch (IOException e) {
            throw Lombok.sneakyThrow(e);
        }
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public static List<Pattern> getIdeExecutableNames() {
        List<Pattern> list = new ArrayList<>();
        Iterator<IdeLocationProvider> it = locationProviders.iterator();
        while (it.hasNext()) {
            IdeLocationProvider provider = it.next();
            Pattern p = provider.getLocationSelectors();
            if (p != null) {
                list.add(p);
            }
        }
        return list;
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public static IdeLocation tryAllProviders(String location) throws CorruptedIdeLocationException {
        Iterator<IdeLocationProvider> it = locationProviders.iterator();
        while (it.hasNext()) {
            IdeLocationProvider provider = it.next();
            IdeLocation loc = provider.create(location);
            if (loc != null) {
                return loc;
            }
        }
        return null;
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public static void autoDiscover(List<IdeLocation> locations, List<CorruptedIdeLocationException> problems) {
        Iterator<IdeLocationProvider> it = locationProviders.iterator();
        while (it.hasNext()) {
            IdeLocationProvider provider = it.next();
            provider.findIdes(locations, problems);
        }
    }

    public static boolean isSelf(String jar) {
        String self = ClassRootFinder.findClassRootOfClass(Installer.class);
        if (self == null) {
            return false;
        }
        File a = new File(jar).getAbsoluteFile();
        File b2 = new File(self).getAbsoluteFile();
        try {
            a = a.getCanonicalFile();
        } catch (IOException unused) {
        }
        try {
            b2 = b2.getCanonicalFile();
        } catch (IOException unused2) {
        }
        return a.equals(b2);
    }

    /* loaded from: xyz.discord_v112014.apk:lombok/installer/Installer$GraphicalInstallerApp.SCL.lombok */
    public static class GraphicalInstallerApp extends LombokApp {
        @Override // lombok.core.LombokApp
        public String getAppName() {
            return "installer";
        }

        @Override // lombok.core.LombokApp
        public String getAppDescription() {
            return "Runs the graphical installer tool (default).";
        }

        @Override // lombok.core.LombokApp
        public List<String> getAppAliases() {
            return Arrays.asList("");
        }

        @Override // lombok.core.LombokApp
        public int runApp(List<String> args) throws Exception {
            return Installer.guiInstaller();
        }
    }

    /* loaded from: xyz.discord_v112014.apk:lombok/installer/Installer$CommandLineInstallerApp.SCL.lombok */
    public static class CommandLineInstallerApp extends LombokApp {
        @Override // lombok.core.LombokApp
        public String getAppName() {
            return "install";
        }

        @Override // lombok.core.LombokApp
        public String getAppDescription() {
            return "Runs the 'handsfree' command line scriptable installer.";
        }

        @Override // lombok.core.LombokApp
        public int runApp(List<String> args) throws Exception {
            return Installer.cliInstaller(false, args);
        }
    }

    /* loaded from: xyz.discord_v112014.apk:lombok/installer/Installer$CommandLineUninstallerApp.SCL.lombok */
    public static class CommandLineUninstallerApp extends LombokApp {
        @Override // lombok.core.LombokApp
        public String getAppName() {
            return "uninstall";
        }

        @Override // lombok.core.LombokApp
        public String getAppDescription() {
            return "Runs the 'handsfree' command line scriptable uninstaller.";
        }

        @Override // lombok.core.LombokApp
        public int runApp(List<String> args) throws Exception {
            return Installer.cliInstaller(true, args);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Type inference failed for: r0v14, types: [java.lang.Throwable, java.lang.Object] */
    public static int guiInstaller() {
        int intValue;
        if (OsUtils.getOS() == OsUtils.OS.MAC_OS_X) {
            System.setProperty("com.apple.mrj.application.apple.menu.about.name", "Lombok Installer");
            System.setProperty("com.apple.macos.use-file-dialog-packages", "true");
        }
        try {
            SwingUtilities.invokeLater(new Runnable() { // from class: lombok.installer.Installer.1
                @Override // java.lang.Runnable
                public void run() {
                    try {
                        try {
                            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                        } catch (Exception unused) {
                        }
                        new InstallerGUI().show();
                    } catch (HeadlessException unused2) {
                        Installer.printHeadlessInfo();
                    }
                }
            });
            synchronized (InstallerGUI.exitMarker) {
                while (!Thread.interrupted() && InstallerGUI.exitMarker.get() == 0) {
                    try {
                        InstallerGUI.exitMarker.wait();
                    } catch (InterruptedException unused) {
                        return 1;
                    }
                }
                Integer errCode = InstallerGUI.exitMarker.get();
                intValue = errCode == null ? 1 : errCode.intValue();
            }
            return intValue;
        } catch (HeadlessException unused2) {
            printHeadlessInfo();
            return 1;
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* loaded from: xyz.discord_v112014.apk:lombok/installer/Installer$CmdArgs.SCL.lombok */
    public static class CmdArgs {
        @Description("Specify paths to a location to install/uninstall. Use 'auto' to apply to all automatically discoverable installations.")
        @Sequential
        List<String> path = new ArrayList();
        @Description("Shows this help text")
        @Shorthand({"h", "?"})
        boolean help;

        private CmdArgs() {
        }
    }

    public static int cliInstaller(boolean uninstall, List<String> rawArgs) {
        CmdReader<CmdArgs> reader = CmdReader.of(CmdArgs.class);
        try {
            CmdArgs args = reader.make((String[]) rawArgs.toArray(new String[0]));
            if (args.help) {
                System.out.println(generateCliHelp(uninstall, reader));
                return 0;
            } else if (args.path.isEmpty()) {
                System.err.println("ERROR: Nothing to do!");
                System.err.println("--------------------------");
                System.err.println(generateCliHelp(uninstall, reader));
                return 1;
            } else {
                List<IdeLocation> locations = new ArrayList<>();
                List<CorruptedIdeLocationException> problems = new ArrayList<>();
                if (args.path.contains("auto")) {
                    autoDiscover(locations, problems);
                }
                Iterator<String> it = args.path.iterator();
                while (it.hasNext()) {
                    String rawPath = it.next();
                    if (!rawPath.equals("auto")) {
                        try {
                            IdeLocation loc = tryAllProviders(rawPath);
                            if (loc != null) {
                                locations.add(loc);
                            } else {
                                problems.add(new CorruptedIdeLocationException("Can't find any IDE at: " + rawPath, null, null));
                            }
                        } catch (CorruptedIdeLocationException e) {
                            problems.add(e);
                        }
                    }
                }
                int validLocations = locations.size();
                Iterator<IdeLocation> it2 = locations.iterator();
                while (it2.hasNext()) {
                    IdeLocation loc2 = it2.next();
                    if (uninstall) {
                        try {
                            loc2.uninstall();
                        } catch (InstallException e2) {
                            if (e2.isWarning()) {
                                System.err.printf("Warning while installing at %s:\n", loc2.getName());
                            } else {
                                System.err.printf("Installation at %s failed:\n", loc2.getName());
                                validLocations--;
                            }
                            System.err.println(e2.getMessage());
                        } catch (UninstallException e3) {
                            if (e3.isWarning()) {
                                System.err.printf("Warning while uninstalling at %s:\n", loc2.getName());
                            } else {
                                System.err.printf("Uninstall at %s failed:\n", loc2.getName());
                                validLocations--;
                            }
                            System.err.println(e3.getMessage());
                        }
                    } else {
                        loc2.install();
                    }
                    PrintStream printStream = System.out;
                    Object[] objArr = new Object[3];
                    objArr[0] = uninstall ? "uninstalled" : "installed";
                    objArr[1] = uninstall ? "from" : "to";
                    objArr[2] = loc2.getName();
                    printStream.printf("Lombok %s %s: %s\n", objArr);
                }
                Iterator<CorruptedIdeLocationException> it3 = problems.iterator();
                while (it3.hasNext()) {
                    CorruptedIdeLocationException problem = it3.next();
                    System.err.println("WARNING: " + problem.getMessage());
                }
                if (validLocations != 0) {
                    return 0;
                }
                System.err.println("WARNING: Zero valid locations found; so nothing was done!");
                return 0;
            }
        } catch (InvalidCommandLineException e4) {
            System.err.println(e4.getMessage());
            System.err.println("--------------------------");
            System.err.println(generateCliHelp(uninstall, reader));
            return 1;
        }
    }

    private static String generateCliHelp(boolean uninstall, CmdReader<CmdArgs> reader) {
        return reader.generateCommandLineHelp("java -jar lombok.jar " + (uninstall ? "uninstall" : "install"));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static void printHeadlessInfo() {
        System.out.printf("About lombok v%s\nLombok makes java better by providing very spicy additions to the Java programming language,such as using @Getter to automatically generate a getter method for any field.\n\nBrowse to %s for more information. To install lombok on Eclipse, re-run this jar file on a graphical computer system - this message is being shown because your terminal is not graphics capable.\nAlternatively, use the command line installer (java -jar lombok.jar install --help).\nIf you are just using 'javac' or a tool that calls on javac, no installation is neccessary; just make sure lombok.jar is in the classpath when you compile. Example:\n\n   java -cp lombok.jar MyCode.java\n", Version.getVersion(), ABOUT_LOMBOK_URL);
    }
}
