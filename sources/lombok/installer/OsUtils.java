package lombok.installer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import lombok.core.Version;
/* loaded from: xyz.discord_v112014.apk:lombok/installer/OsUtils.SCL.lombok */
public final class OsUtils {
    private static final AtomicBoolean windowsDriveInfoLibLoaded = new AtomicBoolean(false);

    private OsUtils() {
    }

    private static void loadWindowsDriveInfoLib() throws IOException {
        if (windowsDriveInfoLibLoaded.compareAndSet(false, true)) {
            String prefix = "lombok-" + Version.getVersion() + "-";
            File temp = File.createTempFile("lombok", ".mark");
            File dll1 = new File(temp.getParentFile(), String.valueOf(prefix) + "WindowsDriveInfo-i386.dll");
            File dll2 = new File(temp.getParentFile(), String.valueOf(prefix) + "WindowsDriveInfo-x86_64.dll");
            temp.delete();
            dll1.deleteOnExit();
            dll2.deleteOnExit();
            try {
                if (unpackDLL("WindowsDriveInfo-i386.binary", dll1)) {
                    System.load(dll1.getAbsolutePath());
                    return;
                }
            } catch (Throwable unused) {
            }
            try {
                if (unpackDLL("WindowsDriveInfo-x86_64.binary", dll2)) {
                    System.load(dll2.getAbsolutePath());
                }
            } catch (Throwable unused2) {
            }
        }
    }

    /* JADX WARN: Finally extract failed */
    private static boolean unpackDLL(String dllName, File target) throws IOException {
        InputStream in = OsUtils.class.getResourceAsStream(dllName);
        try {
            try {
                FileOutputStream out = new FileOutputStream(target);
                try {
                    byte[] b2 = new byte[32000];
                    while (true) {
                        int r = in.read(b2);
                        if (r == -1) {
                            out.close();
                            in.close();
                            return true;
                        }
                        out.write(b2, 0, r);
                    }
                } catch (Throwable th) {
                    out.close();
                    throw th;
                }
            } catch (Throwable th2) {
                in.close();
                throw th2;
            }
        } catch (IOException unused) {
            boolean z2 = target.exists() && target.canRead();
            in.close();
            return z2;
        }
    }

    public static List<String> getDrivesOnWindows() throws Throwable {
        loadWindowsDriveInfoLib();
        List<String> drives = new ArrayList<>();
        WindowsDriveInfo info = new WindowsDriveInfo();
        Iterator<String> it = info.getLogicalDrives().iterator();
        while (it.hasNext()) {
            String drive = it.next();
            if (info.isFixedDisk(drive)) {
                drives.add(drive);
            }
        }
        return drives;
    }

    /* loaded from: xyz.discord_v112014.apk:lombok/installer/OsUtils$OS.SCL.lombok */
    public enum OS {
        MAC_OS_X("\n"),
        WINDOWS("\r\n"),
        UNIX("\n");
        
        private final String lineEnding;

        OS(String lineEnding) {
            this.lineEnding = lineEnding;
        }

        public String getLineEnding() {
            return this.lineEnding;
        }
    }

    public static OS getOS() {
        String prop = System.getProperty("os.name", "").toLowerCase();
        if (!prop.matches("^.*\\bmac\\b.*$") && !prop.matches("^.*\\bdarwin\\b.*$")) {
            return prop.matches("^.*\\bwin(dows|32|64)?\\b.*$") ? OS.WINDOWS : OS.UNIX;
        }
        return OS.MAC_OS_X;
    }
}
