package lombok.eclipse;

import androidx.recyclerview.widget.RecyclerView;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;
import lombok.ConfigurationKeys;
import lombok.core.LombokConfiguration;
import lombok.core.debug.DebugSnapshotStore;
import lombok.core.debug.HistogramTracker;
import lombok.eclipse.handlers.EclipseHandlerUtil;
import lombok.patcher.Symbols;
import lombok.permit.Permit;
import org.eclipse.jdt.internal.compiler.ast.AbstractMethodDeclaration;
import org.eclipse.jdt.internal.compiler.ast.Annotation;
import org.eclipse.jdt.internal.compiler.ast.Argument;
import org.eclipse.jdt.internal.compiler.ast.CompilationUnitDeclaration;
import org.eclipse.jdt.internal.compiler.ast.FieldDeclaration;
import org.eclipse.jdt.internal.compiler.ast.LocalDeclaration;
import org.eclipse.jdt.internal.compiler.ast.TypeDeclaration;
import org.eclipse.jdt.internal.compiler.ast.TypeReference;
import org.eclipse.jdt.internal.compiler.parser.Parser;
/* loaded from: xyz.discord_v112014.apk:lombok/eclipse/TransformEclipseAST.SCL.lombok */
public class TransformEclipseAST {
    private final EclipseAST ast;
    private static final Field astCacheField;
    private static final HandlerLibrary handlers;
    public static boolean disableLombok;
    private static final HistogramTracker lombokTracker;
    private static Map<CompilationUnitDeclaration, State> transformationStates = Collections.synchronizedMap(new WeakHashMap());

    /* JADX INFO: Access modifiers changed from: private */
    /* loaded from: xyz.discord_v112014.apk:lombok/eclipse/TransformEclipseAST$State.SCL.lombok */
    public enum State {
        DIET,
        FULL
    }

    static {
        disableLombok = false;
        String v = System.getProperty("lombok.histogram");
        if (v == null) {
            lombokTracker = null;
        } else if (v.toLowerCase().equals("sysout")) {
            lombokTracker = new HistogramTracker("lombok.histogram", System.out);
        } else {
            lombokTracker = new HistogramTracker("lombok.histogram");
        }
        Field f = null;
        HandlerLibrary h = null;
        if (System.getProperty("lombok.disable") != null) {
            disableLombok = true;
            astCacheField = null;
            handlers = null;
            return;
        }
        try {
            h = HandlerLibrary.load();
        } catch (Throwable t) {
            try {
                EclipseHandlerUtil.error(null, "Problem initializing lombok", t);
            } catch (Throwable unused) {
                System.err.println("Problem initializing lombok");
                t.printStackTrace();
            }
            disableLombok = true;
        }
        try {
            f = Permit.getField(CompilationUnitDeclaration.class, "$lombokAST");
        } catch (Throwable unused2) {
        }
        astCacheField = f;
        handlers = h;
    }

    public static void transform_swapped(CompilationUnitDeclaration ast, Parser parser) {
        transform(parser, ast);
    }

    public static EclipseAST getAST(CompilationUnitDeclaration ast, boolean forceRebuild) {
        EclipseAST existing = null;
        if (astCacheField != null) {
            try {
                existing = (EclipseAST) astCacheField.get(ast);
            } catch (Exception unused) {
            }
        }
        if (existing == null) {
            existing = new EclipseAST(ast);
            if (astCacheField != null) {
                try {
                    astCacheField.set(ast, existing);
                } catch (Exception unused2) {
                }
            }
        } else {
            existing.rebuild(forceRebuild);
        }
        return existing;
    }

    public static boolean alreadyTransformed(CompilationUnitDeclaration ast) {
        State state = transformationStates.get(ast);
        if (state == State.FULL) {
            return true;
        }
        if (state != State.DIET) {
            transformationStates.put(ast, State.DIET);
            return false;
        } else if (!EclipseAST.isComplete(ast)) {
            return true;
        } else {
            transformationStates.put(ast, State.FULL);
            return false;
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v18 */
    /* JADX WARN: Type inference failed for: r0v27 */
    /* JADX WARN: Type inference failed for: r0v28 */
    public static void transform(Parser parser, CompilationUnitDeclaration ast) {
        if (!disableLombok && !Symbols.hasSymbol("lombok.disable") && !alreadyTransformed(ast) && !Boolean.TRUE.equals(LombokConfiguration.read(ConfigurationKeys.LOMBOK_DISABLE, EclipseAST.getAbsoluteFileLocation(ast)))) {
            try {
                DebugSnapshotStore.INSTANCE.snapshot(ast, "transform entry", new Object[0]);
                char start = lombokTracker == null ? 0 : lombokTracker.start();
                EclipseAST existing = getAST(ast, false);
                new TransformEclipseAST(existing).go();
                if (lombokTracker != null) {
                    lombokTracker.end(start);
                }
                DebugSnapshotStore.INSTANCE.snapshot(ast, "transform exit", new Object[0]);
            } catch (Throwable t) {
                DebugSnapshotStore.INSTANCE.snapshot(ast, "transform error: %s", t.getClass().getSimpleName());
                try {
                    String message = "Lombok can't parse this source: " + t.toString();
                    EclipseAST.addProblemToCompilationResult(ast.getFileName(), ast.compilationResult, false, message, 0, 0);
                    t.printStackTrace();
                } catch (Throwable t2) {
                    try {
                        EclipseHandlerUtil.error(ast, "Can't create an error in the problems dialog while adding: " + t.toString(), t2);
                    } catch (Throwable unused) {
                        disableLombok = true;
                    }
                }
            }
        }
    }

    public TransformEclipseAST(EclipseAST ast) {
        this.ast = ast;
    }

    /* JADX WARN: Type inference failed for: r0v17, types: [long] */
    public void go() {
        char c = 0;
        Iterator<Long> it = handlers.getPriorities().iterator();
        while (it.hasNext()) {
            Long d = it.next();
            if (c <= d.longValue()) {
                AnnotationVisitor visitor = new AnnotationVisitor(d.longValue());
                this.ast.traverse(visitor);
                long nextPriority = visitor.getNextPriority();
                c = Math.min(nextPriority, handlers.callASTVisitors(this.ast, d.longValue(), this.ast.isCompleteParse()));
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* loaded from: xyz.discord_v112014.apk:lombok/eclipse/TransformEclipseAST$AnnotationVisitor.SCL.lombok */
    public static class AnnotationVisitor extends EclipseASTAdapter {
        private final long priority;
        private long nextPriority = RecyclerView.FOREVER_NS;

        public AnnotationVisitor(long priority) {
            this.priority = priority;
        }

        public long getNextPriority() {
            return this.nextPriority;
        }

        @Override // lombok.eclipse.EclipseASTAdapter, lombok.eclipse.EclipseASTVisitor
        public void visitAnnotationOnField(FieldDeclaration field, EclipseNode annotationNode, Annotation annotation) {
            CompilationUnitDeclaration top = annotationNode.top().get();
            this.nextPriority = Math.min(this.nextPriority, TransformEclipseAST.handlers.handleAnnotation(top, annotationNode, annotation, this.priority));
        }

        @Override // lombok.eclipse.EclipseASTAdapter, lombok.eclipse.EclipseASTVisitor
        public void visitAnnotationOnMethodArgument(Argument arg, AbstractMethodDeclaration method, EclipseNode annotationNode, Annotation annotation) {
            CompilationUnitDeclaration top = annotationNode.top().get();
            this.nextPriority = Math.min(this.nextPriority, TransformEclipseAST.handlers.handleAnnotation(top, annotationNode, annotation, this.priority));
        }

        @Override // lombok.eclipse.EclipseASTAdapter, lombok.eclipse.EclipseASTVisitor
        public void visitAnnotationOnLocal(LocalDeclaration local, EclipseNode annotationNode, Annotation annotation) {
            CompilationUnitDeclaration top = annotationNode.top().get();
            this.nextPriority = Math.min(this.nextPriority, TransformEclipseAST.handlers.handleAnnotation(top, annotationNode, annotation, this.priority));
        }

        @Override // lombok.eclipse.EclipseASTAdapter, lombok.eclipse.EclipseASTVisitor
        public void visitAnnotationOnMethod(AbstractMethodDeclaration method, EclipseNode annotationNode, Annotation annotation) {
            CompilationUnitDeclaration top = annotationNode.top().get();
            this.nextPriority = Math.min(this.nextPriority, TransformEclipseAST.handlers.handleAnnotation(top, annotationNode, annotation, this.priority));
        }

        @Override // lombok.eclipse.EclipseASTAdapter, lombok.eclipse.EclipseASTVisitor
        public void visitAnnotationOnType(TypeDeclaration type, EclipseNode annotationNode, Annotation annotation) {
            CompilationUnitDeclaration top = annotationNode.top().get();
            this.nextPriority = Math.min(this.nextPriority, TransformEclipseAST.handlers.handleAnnotation(top, annotationNode, annotation, this.priority));
        }

        @Override // lombok.eclipse.EclipseASTAdapter, lombok.eclipse.EclipseASTVisitor
        public void visitAnnotationOnTypeUse(TypeReference typeUse, EclipseNode annotationNode, Annotation annotation) {
            CompilationUnitDeclaration top = annotationNode.top().get();
            this.nextPriority = Math.min(this.nextPriority, TransformEclipseAST.handlers.handleAnnotation(top, annotationNode, annotation, this.priority));
        }
    }
}
