package lombok.launch;

import andhook.lib.AndHook;
import andhook.lib.HookHelper;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.app.Application;
import android.util.Property;
import androidx.activity.result.ActivityResultLauncher;
import co.discord.media_engine.DeviceType;
import com.discord.databinding.WidgetDirectoryChannelBinding;
import com.discord.widgets.directories.WidgetDirectoriesViewModel;
import com.discord.widgets.directories.WidgetDirectoryChannel;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;
import lombok.eclipse.EcjAugments;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.IAnnotatable;
import org.eclipse.jdt.core.IAnnotation;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.AbstractTypeDeclaration;
import org.eclipse.jdt.core.dom.Annotation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Name;
import org.eclipse.jdt.core.dom.NormalAnnotation;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleMemberAnnotation;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.rewrite.ListRewrite;
import org.eclipse.jdt.core.search.SearchMatch;
import org.eclipse.jdt.internal.compiler.ast.ASTNode;
import org.eclipse.jdt.internal.compiler.ast.AbstractMethodDeclaration;
import org.eclipse.jdt.internal.compiler.ast.CompilationUnitDeclaration;
import org.eclipse.jdt.internal.compiler.ast.Expression;
import org.eclipse.jdt.internal.compiler.ast.FieldDeclaration;
import org.eclipse.jdt.internal.compiler.ast.ForeachStatement;
import org.eclipse.jdt.internal.compiler.ast.LocalDeclaration;
import org.eclipse.jdt.internal.compiler.ast.MessageSend;
import org.eclipse.jdt.internal.compiler.ast.TypeDeclaration;
import org.eclipse.jdt.internal.compiler.lookup.BlockScope;
import org.eclipse.jdt.internal.compiler.lookup.MethodBinding;
import org.eclipse.jdt.internal.compiler.lookup.Scope;
import org.eclipse.jdt.internal.compiler.lookup.TypeBinding;
import org.eclipse.jdt.internal.compiler.parser.Parser;
import org.eclipse.jdt.internal.compiler.problem.ProblemReporter;
import org.eclipse.jdt.internal.core.SourceField;
import org.eclipse.jdt.internal.core.dom.rewrite.NodeRewriteEvent;
import org.eclipse.jdt.internal.core.dom.rewrite.RewriteEvent;
import org.eclipse.jdt.internal.core.dom.rewrite.TokenScanner;
import org.eclipse.jdt.internal.corext.refactoring.SearchResultGroup;
import org.eclipse.jdt.internal.corext.refactoring.structure.ASTNodeSearchUtil;
/* loaded from: xyz.discord_v112014.apk:Class50/lombok/launch/PatchFixesHider.SCL.lombok */
final class PatchFixesHider {
    PatchFixesHider() {
    }

    /* loaded from: xyz.discord_v112014.apk:Class50/lombok/launch/PatchFixesHider$Util.SCL.lombok */
    public static final class Util {
        private static ClassLoader shadowLoader;

        public static Class<?> shadowLoadClass(String name) {
            try {
                if (shadowLoader == null) {
                    try {
                        Class.forName("lombok.core.LombokNode");
                        shadowLoader = Util.class.getClassLoader();
                    } catch (ClassNotFoundException unused) {
                        shadowLoader = Main.getShadowClassLoader();
                    }
                }
                return Class.forName(name, true, shadowLoader);
            } catch (ClassNotFoundException e) {
                throw sneakyThrow(e);
            }
        }

        public static Method findMethod(Class<?> type, String name, Class<?>... clsArr) {
            try {
                return type.getDeclaredMethod(name, clsArr);
            } catch (NoSuchMethodException e) {
                throw sneakyThrow(e);
            }
        }

        public static Object invokeMethod(Method method, Object... args) {
            try {
                return method.invoke(null, args);
            } catch (IllegalAccessException e) {
                throw sneakyThrow(e);
            } catch (InvocationTargetException e2) {
                throw sneakyThrow(e2.getCause());
            }
        }

        private static RuntimeException sneakyThrow(Throwable t) {
            if (t == null) {
                throw new NullPointerException("t");
            }
            sneakyThrow0(t);
            return null;
        }

        private static <T extends Throwable> void sneakyThrow0(Throwable t) throws Throwable {
            throw t;
        }
    }

    /* loaded from: xyz.discord_v112014.apk:Class50/lombok/launch/PatchFixesHider$LombokDeps.SCL.lombok */
    public static final class LombokDeps {
        public static final Method ADD_LOMBOK_NOTES;
        public static final Method POST_COMPILER_BYTES_STRING;
        public static final Method POST_COMPILER_OUTPUTSTREAM;
        public static final Method POST_COMPILER_BUFFEREDOUTPUTSTREAM_STRING_STRING;

        /* JADX WARN: Multi-variable type inference failed */
        public LombokDeps() {
            PatchFixesHider.super.value();
        }

        /* JADX WARN: Type inference failed for: r0v1, types: [void, java.lang.Class, java.lang.Object] */
        /* JADX WARN: Type inference failed for: r0v3, types: [void, java.lang.reflect.Method] */
        /* JADX WARN: Type inference failed for: r0v5, types: [void, java.lang.reflect.Method] */
        /* JADX WARN: Type inference failed for: r0v7, types: [void, java.lang.reflect.Method] */
        /* JADX WARN: Type inference failed for: r0v9, types: [void, java.lang.reflect.Method] */
        /* JADX WARN: Type inference failed for: r2v1, types: [java.lang.Class[], java.lang.String] */
        /* JADX WARN: Type inference failed for: r2v3, types: [java.lang.Class[], java.lang.String] */
        /* JADX WARN: Type inference failed for: r2v5, types: [java.lang.Class[], java.lang.String] */
        /* JADX WARN: Type inference failed for: r2v7, types: [java.lang.Class[], java.lang.String] */
        static {
            ?? disableLogging = AndHook.disableLogging("lombok.eclipse.agent.PatchFixesShadowLoaded");
            ADD_LOMBOK_NOTES = AndHook.hookNoBackup(disableLogging, "addLombokNotesToEclipseAboutDialog", new Class[]{String.class, String.class}, disableLogging);
            POST_COMPILER_BYTES_STRING = AndHook.hookNoBackup(disableLogging, "runPostCompiler", new Class[]{byte[].class, String.class}, disableLogging);
            POST_COMPILER_OUTPUTSTREAM = AndHook.hookNoBackup(disableLogging, "runPostCompiler", new Class[]{OutputStream.class}, disableLogging);
            POST_COMPILER_BUFFEREDOUTPUTSTREAM_STRING_STRING = AndHook.hookNoBackup(disableLogging, "runPostCompiler", new Class[]{BufferedOutputStream.class, String.class, String.class}, disableLogging);
        }

        /* JADX WARN: Type inference failed for: r0v1, types: [java.lang.String, java.lang.reflect.Method] */
        public static String addLombokNotesToEclipseAboutDialog(String origReturnValue, String key) {
            try {
                Object[] objArr = {origReturnValue, key};
                return (String) HookHelper.findClass(ADD_LOMBOK_NOTES);
            } catch (Throwable unused) {
                return origReturnValue;
            }
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.String, java.lang.reflect.Method] */
        public static byte[] runPostCompiler(byte[] bytes, String fileName) {
            Object[] objArr = {bytes, fileName};
            return (byte[]) HookHelper.findClass(POST_COMPILER_BYTES_STRING);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.String, java.lang.reflect.Method] */
        public static OutputStream runPostCompiler(OutputStream out) throws IOException {
            ?? r0 = POST_COMPILER_OUTPUTSTREAM;
            new Object[1][0] = out;
            return (OutputStream) HookHelper.findClass(r0);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.String, java.lang.reflect.Method] */
        public static BufferedOutputStream runPostCompiler(BufferedOutputStream out, String path, String name) throws IOException {
            Object[] objArr = {out, path, name};
            return (BufferedOutputStream) HookHelper.findClass(POST_COMPILER_BUFFEREDOUTPUTSTREAM_STRING_STRING);
        }
    }

    /* loaded from: xyz.discord_v112014.apk:Class50/lombok/launch/PatchFixesHider$Transform.SCL.lombok */
    public static final class Transform {
        private static final Method TRANSFORM;
        private static final Method TRANSFORM_SWAPPED;

        static {
            Class<?> shadowed = Util.shadowLoadClass("lombok.eclipse.TransformEclipseAST");
            TRANSFORM = Util.findMethod(shadowed, "transform", Parser.class, CompilationUnitDeclaration.class);
            TRANSFORM_SWAPPED = Util.findMethod(shadowed, "transform_swapped", CompilationUnitDeclaration.class, Parser.class);
        }

        public static void transform(Parser parser, CompilationUnitDeclaration ast) throws IOException {
            Util.invokeMethod(TRANSFORM, parser, ast);
        }

        public static void transform_swapped(CompilationUnitDeclaration ast, Parser parser) throws IOException {
            Util.invokeMethod(TRANSFORM_SWAPPED, ast, parser);
        }
    }

    /* loaded from: xyz.discord_v112014.apk:Class50/lombok/launch/PatchFixesHider$Delegate.SCL.lombok */
    public static final class Delegate {
        private static final Method HANDLE_DELEGATE_FOR_TYPE = null;
        private static final Method GET_CHILDREN = null;

        /*  JADX ERROR: ArrayIndexOutOfBoundsException in pass: SSATransform
            java.lang.ArrayIndexOutOfBoundsException: Index 3 out of bounds for length 3
            	at jadx.core.dex.visitors.ssa.RenameState.startVar(RenameState.java:58)
            	at jadx.core.dex.visitors.ssa.RenameState.init(RenameState.java:28)
            	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:136)
            	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
            	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
            */
        public Delegate(int r3, int r4, int r5, int r6) {
            /*
                r2 = this;
                r0 = r2
                lombok.launch.PatchFixesHider.super.playTogether(r2)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: lombok.launch.PatchFixesHider.Delegate.<init>(int, int, int, int):void");
        }

        /* JADX WARN: Type inference failed for: r0v1, types: [void, android.animation.Animator$AnimatorListener] */
        /* JADX WARN: Type inference failed for: r0v3, types: [void, java.lang.reflect.Method] */
        /* JADX WARN: Type inference failed for: r0v5, types: [void, java.lang.reflect.Method] */
        static DeviceType[] values() {
            ?? onAnimationStart = Animator.AnimatorListener.onAnimationStart("lombok.eclipse.agent.PatchDelegatePortal", "lombok.eclipse.agent.PatchDelegatePortal");
            new Class[1][0] = Object.class;
            HANDLE_DELEGATE_FOR_TYPE = Animator.removeListener(onAnimationStart);
            Class[] clsArr = {Object.class, Object.class};
            GET_CHILDREN = Animator.removeListener(onAnimationStart);
            return;
        }

        /* JADX WARN: Not initialized variable reg: 6, insn: 0x0009: MOVE  (r4 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY]) = (r6 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY] A[D('classScope' java.lang.Object)]), block:B:2:0x0000 */
        /* JADX WARN: Type inference failed for: r0v1, types: [float] */
        /* JADX WARN: Type inference failed for: r0v2, types: [android.animation.Keyframe, java.lang.Boolean, float] */
        /* JADX WARN: Type inference failed for: r0v3, types: [android.animation.Keyframe, int] */
        public static int component1() {
            Object classScope;
            Method method = HANDLE_DELEGATE_FOR_TYPE;
            new Object[1][0] = classScope;
            ?? r0 = (Boolean) Keyframe.getFraction();
            return r0.ofInt(r0);
        }

        /* JADX WARN: Not initialized variable reg: 6, insn: 0x0009: MOVE  (r4 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY]) = (r6 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY] A[D('returnValue' java.lang.Object)]), block:B:2:0x0000 */
        /* JADX WARN: Not initialized variable reg: 7, insn: 0x000d: MOVE  (r4 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY]) = (r7 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY] A[D('javaElement' java.lang.Object)]), block:B:2:0x0000 */
        /* JADX WARN: Type inference failed for: r0v1, types: [float] */
        /* JADX WARN: Type inference failed for: r0v2, types: [java.lang.Object[], int] */
        public static int component2() {
            Object returnValue;
            Object javaElement;
            Method method = GET_CHILDREN;
            Object[] objArr = {returnValue, javaElement};
            return (Object[]) Keyframe.getFraction();
        }
    }

    /* loaded from: xyz.discord_v112014.apk:Class50/lombok/launch/PatchFixesHider$ValPortal.SCL.lombok */
    public static final class ValPortal {
        private static final Method COPY_INITIALIZATION_OF_FOR_EACH_ITERABLE;
        private static final Method COPY_INITIALIZATION_OF_LOCAL_DECLARATION;
        private static final Method ADD_FINAL_AND_VAL_ANNOTATION_TO_VARIABLE_DECLARATION_STATEMENT;
        private static final Method ADD_FINAL_AND_VAL_ANNOTATION_TO_SINGLE_VARIABLE_DECLARATION;

        static {
            Class<?> shadowed = Util.shadowLoadClass("lombok.eclipse.agent.PatchValEclipsePortal");
            COPY_INITIALIZATION_OF_FOR_EACH_ITERABLE = Util.findMethod(shadowed, "copyInitializationOfForEachIterable", Object.class);
            COPY_INITIALIZATION_OF_LOCAL_DECLARATION = Util.findMethod(shadowed, "copyInitializationOfLocalDeclaration", Object.class);
            ADD_FINAL_AND_VAL_ANNOTATION_TO_VARIABLE_DECLARATION_STATEMENT = Util.findMethod(shadowed, "addFinalAndValAnnotationToVariableDeclarationStatement", Object.class, Object.class, Object.class);
            ADD_FINAL_AND_VAL_ANNOTATION_TO_SINGLE_VARIABLE_DECLARATION = Util.findMethod(shadowed, "addFinalAndValAnnotationToSingleVariableDeclaration", Object.class, Object.class, Object.class);
        }

        public static void copyInitializationOfForEachIterable(Object parser) {
            Util.invokeMethod(COPY_INITIALIZATION_OF_FOR_EACH_ITERABLE, parser);
        }

        public static void copyInitializationOfLocalDeclaration(Object parser) {
            Util.invokeMethod(COPY_INITIALIZATION_OF_LOCAL_DECLARATION, parser);
        }

        public static void addFinalAndValAnnotationToVariableDeclarationStatement(Object converter, Object out, Object in) {
            Util.invokeMethod(ADD_FINAL_AND_VAL_ANNOTATION_TO_VARIABLE_DECLARATION_STATEMENT, converter, out, in);
        }

        public static void addFinalAndValAnnotationToSingleVariableDeclaration(Object converter, Object out, Object in) {
            Util.invokeMethod(ADD_FINAL_AND_VAL_ANNOTATION_TO_SINGLE_VARIABLE_DECLARATION, converter, out, in);
        }
    }

    /* loaded from: xyz.discord_v112014.apk:Class50/lombok/launch/PatchFixesHider$Val.SCL.lombok */
    public static final class Val {
        private static final Method SKIP_RESOLVE_INITIALIZER_IF_ALREADY_CALLED;
        private static final Method SKIP_RESOLVE_INITIALIZER_IF_ALREADY_CALLED2;
        private static final Method HANDLE_VAL_FOR_LOCAL_DECLARATION;
        private static final Method HANDLE_VAL_FOR_FOR_EACH;

        static {
            Class<?> shadowed = Util.shadowLoadClass("lombok.eclipse.agent.PatchVal");
            SKIP_RESOLVE_INITIALIZER_IF_ALREADY_CALLED = Util.findMethod(shadowed, "skipResolveInitializerIfAlreadyCalled", Expression.class, BlockScope.class);
            SKIP_RESOLVE_INITIALIZER_IF_ALREADY_CALLED2 = Util.findMethod(shadowed, "skipResolveInitializerIfAlreadyCalled2", Expression.class, BlockScope.class, LocalDeclaration.class);
            HANDLE_VAL_FOR_LOCAL_DECLARATION = Util.findMethod(shadowed, "handleValForLocalDeclaration", LocalDeclaration.class, BlockScope.class);
            HANDLE_VAL_FOR_FOR_EACH = Util.findMethod(shadowed, "handleValForForEach", ForeachStatement.class, BlockScope.class);
        }

        public static TypeBinding skipResolveInitializerIfAlreadyCalled(Expression expr, BlockScope scope) {
            return (TypeBinding) Util.invokeMethod(SKIP_RESOLVE_INITIALIZER_IF_ALREADY_CALLED, expr, scope);
        }

        public static TypeBinding skipResolveInitializerIfAlreadyCalled2(Expression expr, BlockScope scope, LocalDeclaration decl) {
            return (TypeBinding) Util.invokeMethod(SKIP_RESOLVE_INITIALIZER_IF_ALREADY_CALLED2, expr, scope, decl);
        }

        public static boolean handleValForLocalDeclaration(LocalDeclaration local, BlockScope scope) {
            return ((Boolean) Util.invokeMethod(HANDLE_VAL_FOR_LOCAL_DECLARATION, local, scope)).booleanValue();
        }

        public static boolean handleValForForEach(ForeachStatement forEach, BlockScope scope) {
            return ((Boolean) Util.invokeMethod(HANDLE_VAL_FOR_FOR_EACH, forEach, scope)).booleanValue();
        }
    }

    /* loaded from: xyz.discord_v112014.apk:Class50/lombok/launch/PatchFixesHider$ExtensionMethod.SCL.lombok */
    public static final class ExtensionMethod {
        private static final Method RESOLVE_TYPE = ValueAnimator.cancel();
        private static final Method ERROR_NO_METHOD_FOR = ValueAnimator.cancel();
        private static final Method INVALID_METHOD = ValueAnimator.cancel();
        private static final Method INVALID_METHOD2 = ValueAnimator.cancel();
        private static final Method NON_STATIC_ACCESS_TO_STATIC_METHOD = ValueAnimator.cancel();

        /* JADX WARN: Multi-variable type inference failed */
        public ExtensionMethod() {
            PatchFixesHider.super.dismiss();
        }

        /* JADX WARN: Type inference failed for: r0v11, types: [void, java.lang.reflect.Method] */
        /* JADX WARN: Type inference failed for: r0v3, types: [void, java.lang.reflect.Method] */
        /* JADX WARN: Type inference failed for: r0v5, types: [void, java.lang.reflect.Method] */
        /* JADX WARN: Type inference failed for: r0v7, types: [void, java.lang.reflect.Method] */
        /* JADX WARN: Type inference failed for: r0v9, types: [void, java.lang.reflect.Method] */
        static {
            AnimatorSet.play("lombok.eclipse.agent.PatchExtensionMethod");
            Class[] clsArr = {TypeBinding.class, MessageSend.class, BlockScope.class};
            Class[] clsArr2 = {ProblemReporter.class, MessageSend.class, TypeBinding.class, TypeBinding[].class};
            Class[] clsArr3 = {ProblemReporter.class, MessageSend.class, MethodBinding.class};
            Class[] clsArr4 = {ProblemReporter.class, MessageSend.class, MethodBinding.class, Scope.class};
            Class[] clsArr5 = {ProblemReporter.class, ASTNode.class, MethodBinding.class, MessageSend.class};
        }

        /* JADX WARN: Not initialized variable reg: 8, insn: 0x0011: MOVE  (r4 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY]) = 
          (r8 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY] A[D('scope' org.eclipse.jdt.internal.compiler.lookup.BlockScope)])
        , block:B:2:0x0000 */
        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.String, java.lang.reflect.Method] */
        /* JADX WARN: Type inference failed for: r0v2, types: [void, org.eclipse.jdt.internal.compiler.lookup.TypeBinding] */
        public static void access$configureUi(WidgetDirectoryChannel widgetDirectoryChannel, WidgetDirectoriesViewModel.ViewState viewState) {
            BlockScope scope;
            Object[] objArr = {widgetDirectoryChannel, viewState, scope};
        }

        /* JADX WARN: Not initialized variable reg: 7, insn: 0x000d: MOVE  (r4 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY]) = 
          (r7 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY] A[D('messageSend' org.eclipse.jdt.internal.compiler.ast.MessageSend)])
        , block:B:2:0x0000 */
        /* JADX WARN: Not initialized variable reg: 8, insn: 0x0011: MOVE  (r4 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY]) = 
          (r8 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY] A[D('recType' org.eclipse.jdt.internal.compiler.lookup.TypeBinding)])
        , block:B:2:0x0000 */
        /* JADX WARN: Not initialized variable reg: 9, insn: 0x0015: MOVE  (r4 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY]) = 
          (r9 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY] A[D('params' org.eclipse.jdt.internal.compiler.lookup.TypeBinding[])])
        , block:B:2:0x0000 */
        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.String, java.lang.reflect.Method] */
        public static ActivityResultLauncher access$getActivityResult$p(WidgetDirectoryChannel widgetDirectoryChannel) {
            MessageSend messageSend;
            TypeBinding recType;
            TypeBinding[] params;
            Object[] objArr = {widgetDirectoryChannel, messageSend, recType, params};
            Application.getSystemService((String) ERROR_NO_METHOD_FOR);
            return;
        }

        /* JADX WARN: Not initialized variable reg: 6, insn: 0x0009: MOVE  (r4 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY]) = 
          (r6 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY] A[D('problemReporter' org.eclipse.jdt.internal.compiler.problem.ProblemReporter)])
        , block:B:2:0x0000 */
        /* JADX WARN: Not initialized variable reg: 7, insn: 0x000d: MOVE  (r4 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY]) = 
          (r7 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY] A[D('messageSend' org.eclipse.jdt.internal.compiler.ast.MessageSend)])
        , block:B:2:0x0000 */
        /* JADX WARN: Not initialized variable reg: 8, insn: 0x0011: MOVE  (r4 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY]) = 
          (r8 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY] A[D('method' org.eclipse.jdt.internal.compiler.lookup.MethodBinding)])
        , block:B:2:0x0000 */
        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.String, java.lang.reflect.Method] */
        public static int access$getTAB_LAYOUT_MARGINS$cp() {
            ProblemReporter problemReporter;
            MessageSend messageSend;
            MethodBinding method;
            Object[] objArr = {problemReporter, messageSend, method};
            Application.getSystemService((String) INVALID_METHOD);
            return;
        }

        /* JADX WARN: Not initialized variable reg: 8, insn: 0x0011: MOVE  (r4 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY]) = 
          (r8 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY] A[D('method' org.eclipse.jdt.internal.compiler.lookup.MethodBinding)])
        , block:B:2:0x0000 */
        /* JADX WARN: Not initialized variable reg: 9, insn: 0x0015: MOVE  (r4 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY]) = 
          (r9 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY] A[D('scope' org.eclipse.jdt.internal.compiler.lookup.Scope)])
        , block:B:2:0x0000 */
        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.String, java.lang.reflect.Method] */
        public static void access$onViewBindingDestroy(WidgetDirectoryChannel widgetDirectoryChannel, WidgetDirectoryChannelBinding widgetDirectoryChannelBinding) {
            MethodBinding method;
            Scope scope;
            Object[] objArr = {widgetDirectoryChannel, widgetDirectoryChannelBinding, method, scope};
            Application.getSystemService((String) INVALID_METHOD2);
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Not initialized variable reg: 8, insn: 0x0011: MOVE  (r4 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY]) = 
          (r8 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY] A[D('method' org.eclipse.jdt.internal.compiler.lookup.MethodBinding)])
        , block:B:2:0x0000 */
        /* JADX WARN: Not initialized variable reg: 9, insn: 0x0015: MOVE  (r4 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY]) = 
          (r9 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY] A[D('messageSend' org.eclipse.jdt.internal.compiler.ast.MessageSend)])
        , block:B:2:0x0000 */
        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.String, java.lang.reflect.Method] */
        public static void bindGestureObservers(boolean z2, WidgetDirectoryChannelBinding widgetDirectoryChannelBinding) {
            MethodBinding method;
            MessageSend messageSend;
            Object[] objArr = {z2, widgetDirectoryChannelBinding, method, messageSend};
            Application.getSystemService((String) NON_STATIC_ACCESS_TO_STATIC_METHOD);
        }
    }

    /* loaded from: xyz.discord_v112014.apk:Class50/lombok/launch/PatchFixesHider$Javadoc.SCL.lombok */
    public static final class Javadoc {
        private static final Method GET_HTML = null;
        private static final Method PRINT_METHOD = null;

        /* JADX WARN: Multi-variable type inference failed */
        public Javadoc() {
            PatchFixesHider.super.getValues();
            return;
        }

        /*  JADX ERROR: IllegalArgumentException in pass: ConstructorVisitor
            java.lang.IllegalArgumentException: Illegal Capacity: -1
            	at java.base/java.util.ArrayList.<init>(ArrayList.java:160)
            	at jadx.core.dex.nodes.InsnNode.<init>(InsnNode.java:37)
            	at jadx.core.dex.instructions.BaseInvokeNode.<init>(BaseInvokeNode.java:11)
            	at jadx.core.dex.instructions.mods.ConstructorInsn.<init>(ConstructorInsn.java:28)
            	at jadx.core.dex.visitors.ConstructorVisitor.processInvoke(ConstructorVisitor.java:66)
            	at jadx.core.dex.visitors.ConstructorVisitor.replaceInvoke(ConstructorVisitor.java:53)
            	at jadx.core.dex.visitors.ConstructorVisitor.visit(ConstructorVisitor.java:36)
            */
        static d0.e0.p.d.m0.f.b.C0325b.c getArrayElement(
        /*  JADX ERROR: IllegalArgumentException in pass: ConstructorVisitor
            java.lang.IllegalArgumentException: Illegal Capacity: -1
            	at java.base/java.util.ArrayList.<init>(ArrayList.java:160)
            	at jadx.core.dex.nodes.InsnNode.<init>(InsnNode.java:37)
            	at jadx.core.dex.instructions.BaseInvokeNode.<init>(BaseInvokeNode.java:11)
            	at jadx.core.dex.instructions.mods.ConstructorInsn.<init>(ConstructorInsn.java:28)
            	at jadx.core.dex.visitors.ConstructorVisitor.processInvoke(ConstructorVisitor.java:66)
            	at jadx.core.dex.visitors.ConstructorVisitor.replaceInvoke(ConstructorVisitor.java:53)
            */
        /*  JADX ERROR: Method generation error
            jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r7v0 ??
            	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:227)
            	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:212)
            	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:165)
            	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:364)
            	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:304)
            	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$3(ClassGen.java:270)
            	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
            	at java.base/java.util.ArrayList.forEach(ArrayList.java:1511)
            	at java.base/java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
            	at java.base/java.util.stream.Sink$ChainedReference.end(Sink.java:258)
            */

        /* JADX WARN: Not initialized variable reg: 6, insn: 0x0009: MOVE  (r4 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY]) = (r6 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY] A[D('original' java.lang.String)]), block:B:2:0x0000 */
        /* JADX WARN: Not initialized variable reg: 7, insn: 0x000d: MOVE  (r4 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY]) = (r7 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY] A[D('member' org.eclipse.jdt.core.IJavaElement)]), block:B:2:0x0000 */
        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, java.lang.Object[], java.lang.reflect.Method] */
        /* JADX WARN: Type inference failed for: r0v2, types: [java.util.List, java.lang.String] */
        /* JADX WARN: Type inference failed for: r1v1, types: [java.lang.Object[], android.util.Property] */
        public static List getArrayElementList() {
            String original;
            IJavaElement member;
            ?? r0 = GET_HTML;
            return (String) ObjectAnimator.ofObject(r0, (Property<??, V>) new Object[]{original, member}, (TypeEvaluator) 1, (Object[]) r0);
        }

        /* JADX WARN: Not initialized variable reg: 6, insn: 0x0009: MOVE  (r4 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY]) = 
          (r6 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY] A[D('methodDeclaration' org.eclipse.jdt.internal.compiler.ast.AbstractMethodDeclaration)])
        , block:B:2:0x0000 */
        /* JADX WARN: Not initialized variable reg: 7, insn: 0x000d: MOVE  (r4 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY]) = (r7 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY] A[D('tab' int)]), block:B:2:0x0000 */
        /* JADX WARN: Not initialized variable reg: 8, insn: 0x0014: MOVE  (r4 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY]) = (r8 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY] A[D('output' java.lang.StringBuffer)]), block:B:2:0x0000 */
        /* JADX WARN: Not initialized variable reg: 9, insn: 0x0018: MOVE  (r4 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY]) = 
          (r9 I:??[int, float, boolean, short, byte, char, OBJECT, ARRAY] A[D('type' org.eclipse.jdt.internal.compiler.ast.TypeDeclaration)])
        , block:B:2:0x0000 */
        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, java.lang.Object[], java.lang.reflect.Method] */
        /* JADX WARN: Type inference failed for: r0v2, types: [java.lang.StringBuffer, int] */
        /* JADX WARN: Type inference failed for: r1v1, types: [java.lang.Object[], android.util.Property] */
        /* JADX WARN: Type inference failed for: r7v0, types: [float[], android.util.Property] */
        public static int getClassId() {
            AbstractMethodDeclaration methodDeclaration;
            ?? r7;
            StringBuffer output;
            TypeDeclaration type;
            ?? r0 = PRINT_METHOD;
            return (StringBuffer) ObjectAnimator.ofObject(r0, (Property<??, V>) new Object[]{methodDeclaration, PropertyValuesHolder.ofFloat((Property<?, Float>) r7, (float[]) r7), output, type}, (TypeEvaluator) 3, (Object[]) r0);
        }
    }

    /* loaded from: xyz.discord_v112014.apk:Class50/lombok/launch/PatchFixesHider$PatchFixes.SCL.lombok */
    public static final class PatchFixes {
        public static final int ALREADY_PROCESSED_FLAG = 8388608;

        public static boolean isGenerated(org.eclipse.jdt.core.dom.ASTNode node) {
            boolean result = false;
            try {
                result = ((Boolean) node.getClass().getField("$isGenerated").get(node)).booleanValue();
                if (!result && node.getParent() != null && (node.getParent() instanceof QualifiedName)) {
                    result = isGenerated(node.getParent());
                }
            } catch (Exception unused) {
            }
            return result;
        }

        public static boolean isGenerated(ASTNode node) {
            boolean result = false;
            try {
                result = node.getClass().getField("$generatedBy").get(node) != null;
            } catch (Exception unused) {
            }
            return result;
        }

        public static boolean isListRewriteOnGeneratedNode(ListRewrite rewrite) {
            return isGenerated(rewrite.getParent());
        }

        public static boolean returnFalse(Object object) {
            return false;
        }

        public static boolean returnTrue(Object object) {
            return true;
        }

        public static List removeGeneratedNodes(List list) {
            try {
                List realNodes = new ArrayList(list.size());
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    Object node = it.next();
                    if (!isGenerated((org.eclipse.jdt.core.dom.ASTNode) node)) {
                        realNodes.add(node);
                    }
                }
                return realNodes;
            } catch (Exception unused) {
                return list;
            }
        }

        public static String getRealMethodDeclarationSource(String original, Object processor, MethodDeclaration declaration) throws Exception {
            if (!isGenerated((org.eclipse.jdt.core.dom.ASTNode) declaration)) {
                return original;
            }
            List<Annotation> annotations = new ArrayList<>();
            Iterator it = declaration.modifiers().iterator();
            while (it.hasNext()) {
                Object modifier = it.next();
                if (modifier instanceof Annotation) {
                    Annotation annotation = (Annotation) modifier;
                    String qualifiedAnnotationName = annotation.resolveTypeBinding().getQualifiedName();
                    if (!"java.lang.Override".equals(qualifiedAnnotationName) && !"java.lang.SuppressWarnings".equals(qualifiedAnnotationName)) {
                        annotations.add(annotation);
                    }
                }
            }
            StringBuilder signature = new StringBuilder();
            addAnnotations(annotations, signature);
            if (((Boolean) processor.getClass().getDeclaredField("fPublic").get(processor)).booleanValue()) {
                signature.append("public ");
            }
            if (((Boolean) processor.getClass().getDeclaredField("fAbstract").get(processor)).booleanValue()) {
                signature.append("abstract ");
            }
            signature.append(declaration.getReturnType2().toString()).append(" ").append(declaration.getName().getFullyQualifiedName()).append("(");
            boolean first = true;
            Iterator it2 = declaration.parameters().iterator();
            while (it2.hasNext()) {
                Object parameter = it2.next();
                if (!first) {
                    signature.append(", ");
                }
                first = false;
                signature.append(parameter);
            }
            signature.append(");");
            return signature.toString();
        }

        public static void addAnnotations(List<Annotation> annotations, StringBuilder signature) {
            Iterator<Annotation> it = annotations.iterator();
            while (it.hasNext()) {
                SingleMemberAnnotation singleMemberAnnotation = (Annotation) it.next();
                List<String> values = new ArrayList<>();
                if (singleMemberAnnotation.isSingleMemberAnnotation()) {
                    SingleMemberAnnotation smAnn = singleMemberAnnotation;
                    values.add(smAnn.getValue().toString());
                } else if (singleMemberAnnotation.isNormalAnnotation()) {
                    NormalAnnotation normalAnn = (NormalAnnotation) singleMemberAnnotation;
                    Iterator it2 = normalAnn.values().iterator();
                    while (it2.hasNext()) {
                        Object value = it2.next();
                        values.add(value.toString());
                    }
                }
                signature.append("@").append(singleMemberAnnotation.resolveTypeBinding().getQualifiedName());
                if (!values.isEmpty()) {
                    signature.append("(");
                    boolean first = true;
                    Iterator<String> it3 = values.iterator();
                    while (it3.hasNext()) {
                        String string = it3.next();
                        if (!first) {
                            signature.append(", ");
                        }
                        first = false;
                        signature.append('\"').append(string).append('\"');
                    }
                    signature.append(")");
                }
                signature.append(" ");
            }
        }

        public static MethodDeclaration getRealMethodDeclarationNode(IMethod sourceMethod, CompilationUnit cuUnit) throws JavaModelException {
            AbstractTypeDeclaration typeDeclaration;
            MethodDeclaration methodDeclarationNode = ASTNodeSearchUtil.getMethodDeclarationNode(sourceMethod, cuUnit);
            if (isGenerated((org.eclipse.jdt.core.dom.ASTNode) methodDeclarationNode)) {
                Stack<IType> typeStack = new Stack<>();
                for (IType declaringType = sourceMethod.getDeclaringType(); declaringType != null; declaringType = declaringType.getDeclaringType()) {
                    typeStack.push(declaringType);
                }
                IType rootType = typeStack.pop();
                AbstractTypeDeclaration findTypeDeclaration = findTypeDeclaration(rootType, cuUnit.types());
                while (true) {
                    typeDeclaration = findTypeDeclaration;
                    if (typeStack.isEmpty() || typeDeclaration == null) {
                        break;
                    }
                    findTypeDeclaration = findTypeDeclaration(typeStack.pop(), typeDeclaration.bodyDeclarations());
                }
                if (typeStack.isEmpty() && typeDeclaration != null) {
                    String methodName = sourceMethod.getElementName();
                    Iterator it = typeDeclaration.bodyDeclarations().iterator();
                    while (it.hasNext()) {
                        Object declaration = it.next();
                        if (declaration instanceof MethodDeclaration) {
                            MethodDeclaration methodDeclaration = (MethodDeclaration) declaration;
                            if (methodDeclaration.getName().toString().equals(methodName)) {
                                return methodDeclaration;
                            }
                        }
                    }
                }
            }
            return methodDeclarationNode;
        }

        public static AbstractTypeDeclaration findTypeDeclaration(IType searchType, List<?> nodes) {
            Iterator<?> it = nodes.iterator();
            while (it.hasNext()) {
                Object object = it.next();
                if (object instanceof AbstractTypeDeclaration) {
                    AbstractTypeDeclaration typeDeclaration = (AbstractTypeDeclaration) object;
                    if (typeDeclaration.getName().toString().equals(searchType.getElementName())) {
                        return typeDeclaration;
                    }
                }
            }
            return null;
        }

        public static int getSourceEndFixed(int sourceEnd, ASTNode node) throws Exception {
            ASTNode object;
            if (sourceEnd != -1 || (object = (ASTNode) node.getClass().getField("$generatedBy").get(node)) == null) {
                return sourceEnd;
            }
            return object.sourceEnd;
        }

        public static int fixRetrieveStartingCatchPosition(int original, int start) {
            return original == -1 ? start : original;
        }

        public static int fixRetrieveIdentifierEndPosition(int original, int start, int end) {
            if (original != -1 && original >= start) {
                return original;
            }
            return end;
        }

        public static int fixRetrieveEllipsisStartPosition(int original, int end) {
            return original == -1 ? end : original;
        }

        public static int fixRetrieveStartBlockPosition(int original, int start) {
            return original == -1 ? start : original;
        }

        public static int fixRetrieveRightBraceOrSemiColonPosition(int original, int end) {
            return original == -1 ? end : original;
        }

        public static int fixRetrieveRightBraceOrSemiColonPosition(int retVal, AbstractMethodDeclaration amd) {
            if (retVal != -1 || amd == null) {
                return retVal;
            }
            boolean isGenerated = EcjAugments.ASTNode_generatedBy.get(amd) != null;
            if (isGenerated) {
                return amd.declarationSourceEnd;
            }
            return -1;
        }

        public static int fixRetrieveRightBraceOrSemiColonPosition(int retVal, FieldDeclaration fd) {
            if (retVal != -1 || fd == null) {
                return retVal;
            }
            boolean isGenerated = EcjAugments.ASTNode_generatedBy.get(fd) != null;
            if (isGenerated) {
                return fd.declarationSourceEnd;
            }
            return -1;
        }

        public static int fixRetrieveProperRightBracketPosition(int retVal, Type type) {
            if (retVal != -1 || type == null) {
                return retVal;
            }
            if (isGenerated((org.eclipse.jdt.core.dom.ASTNode) type)) {
                return (type.getStartPosition() + type.getLength()) - 1;
            }
            return -1;
        }

        public static boolean checkBit24(Object node) throws Exception {
            int bits = ((Integer) node.getClass().getField("bits").get(node)).intValue();
            return (bits & 8388608) != 0;
        }

        public static boolean skipRewritingGeneratedNodes(org.eclipse.jdt.core.dom.ASTNode node) throws Exception {
            return ((Boolean) node.getClass().getField("$isGenerated").get(node)).booleanValue();
        }

        public static void setIsGeneratedFlag(org.eclipse.jdt.core.dom.ASTNode domNode, ASTNode internalNode) throws Exception {
            if (internalNode != null && domNode != null) {
                boolean isGenerated = EcjAugments.ASTNode_generatedBy.get(internalNode) != null;
                if (isGenerated) {
                    domNode.getClass().getField("$isGenerated").set(domNode, true);
                }
            }
        }

        public static void setIsGeneratedFlagForName(Name name, Object internalNode) throws Exception {
            if (internalNode instanceof ASTNode) {
                boolean isGenerated = EcjAugments.ASTNode_generatedBy.get((ASTNode) internalNode) != null;
                if (isGenerated) {
                    name.getClass().getField("$isGenerated").set(name, true);
                }
            }
        }

        public static RewriteEvent[] listRewriteHandleGeneratedMethods(RewriteEvent parent) {
            RewriteEvent[] children = parent.getChildren();
            List<RewriteEvent> newChildren = new ArrayList<>();
            List<RewriteEvent> modifiedChildren = new ArrayList<>();
            for (RewriteEvent child : children) {
                boolean isGenerated = isGenerated((org.eclipse.jdt.core.dom.ASTNode) child.getOriginalValue());
                if (isGenerated) {
                    boolean isReplacedOrRemoved = child.getChangeKind() == 4 || child.getChangeKind() == 2;
                    boolean convertingFromMethod = child.getOriginalValue() instanceof MethodDeclaration;
                    if (isReplacedOrRemoved && convertingFromMethod && child.getNewValue() != null) {
                        modifiedChildren.add(new NodeRewriteEvent((Object) null, child.getNewValue()));
                    }
                } else {
                    newChildren.add(child);
                }
            }
            newChildren.addAll(modifiedChildren);
            return (RewriteEvent[]) newChildren.toArray(new RewriteEvent[0]);
        }

        public static int getTokenEndOffsetFixed(TokenScanner scanner, int token, int startOffset, Object domNode) throws CoreException {
            boolean isGenerated = false;
            try {
                isGenerated = ((Boolean) domNode.getClass().getField("$isGenerated").get(domNode)).booleanValue();
            } catch (Exception unused) {
            }
            if (isGenerated) {
                return -1;
            }
            return scanner.getTokenEndOffset(token, startOffset);
        }

        public static IMethod[] removeGeneratedMethods(IMethod[] methods) throws Exception {
            List<IMethod> result = new ArrayList<>();
            for (IMethod m : methods) {
                if (m.getNameRange().getLength() > 0 && !m.getNameRange().equals(m.getSourceRange())) {
                    result.add(m);
                }
            }
            return result.size() == methods.length ? methods : (IMethod[]) result.toArray(new IMethod[0]);
        }

        public static SearchMatch[] removeGenerated(SearchMatch[] returnValue) {
            List<SearchMatch> result = new ArrayList<>();
            for (SearchMatch searchResult : returnValue) {
                if (searchResult.getElement() instanceof IField) {
                    IField field = (IField) searchResult.getElement();
                    IAnnotation annotation = field.getAnnotation("Generated");
                    if (annotation != null) {
                    }
                }
                result.add(searchResult);
            }
            return (SearchMatch[]) result.toArray(new SearchMatch[0]);
        }

        public static SearchResultGroup[] createFakeSearchResult(SearchResultGroup[] returnValue, Object processor) throws Exception {
            Field declaredField;
            if ((returnValue == null || returnValue.length == 0) && (declaredField = processor.getClass().getDeclaredField("fField")) != null) {
                declaredField.setAccessible(true);
                SourceField fField = (SourceField) declaredField.get(processor);
                IAnnotation dataAnnotation = fField.getDeclaringType().getAnnotation("Data");
                if (dataAnnotation != null) {
                    return new SearchResultGroup[]{new SearchResultGroup((IResource) null, new SearchMatch[1])};
                }
            }
            return returnValue;
        }

        public static SimpleName[] removeGeneratedSimpleNames(SimpleName[] in) throws Exception {
            Field f = SimpleName.class.getField("$isGenerated");
            int count = 0;
            for (int i = 0; i < in.length; i++) {
                if (in[i] == null || !((Boolean) f.get(in[i])).booleanValue()) {
                    count++;
                }
            }
            if (count == in.length) {
                return in;
            }
            SimpleName[] newSimpleNames = new SimpleName[count];
            int count2 = 0;
            for (int i2 = 0; i2 < in.length; i2++) {
                if (in[i2] == null || !((Boolean) f.get(in[i2])).booleanValue()) {
                    int i3 = count2;
                    count2++;
                    newSimpleNames[i3] = in[i2];
                }
            }
            return newSimpleNames;
        }

        public static org.eclipse.jdt.internal.compiler.ast.Annotation[] convertAnnotations(org.eclipse.jdt.internal.compiler.ast.Annotation[] out, IAnnotatable annotatable) {
            try {
                IAnnotation[] in = annotatable.getAnnotations();
                if (out == null) {
                    return null;
                }
                int toWrite = 0;
                for (int idx = 0; idx < out.length; idx++) {
                    String oName = new String(out[idx].type.getLastToken());
                    boolean found = false;
                    int length = in.length;
                    int i = 0;
                    while (true) {
                        if (i >= length) {
                            break;
                        }
                        IAnnotation i2 = in[i];
                        String name = i2.getElementName();
                        int li = name.lastIndexOf(46);
                        if (li > -1) {
                            name = name.substring(li + 1);
                        }
                        if (name.equals(oName)) {
                            found = true;
                            break;
                        }
                        i++;
                    }
                    if (!found) {
                        out[idx] = null;
                    } else {
                        toWrite++;
                    }
                }
                org.eclipse.jdt.internal.compiler.ast.Annotation[] replace = out;
                if (toWrite < out.length) {
                    replace = new org.eclipse.jdt.internal.compiler.ast.Annotation[toWrite];
                    int idx2 = 0;
                    for (int i3 = 0; i3 < out.length; i3++) {
                        if (out[i3] != null) {
                            int i4 = idx2;
                            idx2++;
                            replace[i4] = out[i3];
                        }
                    }
                }
                return replace;
            } catch (Exception unused) {
                return out;
            }
        }
    }
}
