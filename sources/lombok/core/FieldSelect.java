package lombok.core;
/* loaded from: xyz.discord_v112014.apk:lombok/core/FieldSelect.SCL.lombok */
public class FieldSelect {
    private final String finalPart;

    public FieldSelect(String finalPart) {
        this.finalPart = finalPart;
    }

    public String getFinalPart() {
        return this.finalPart;
    }
}
