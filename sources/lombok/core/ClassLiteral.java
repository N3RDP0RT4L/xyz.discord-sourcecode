package lombok.core;
/* loaded from: xyz.discord_v112014.apk:lombok/core/ClassLiteral.SCL.lombok */
public class ClassLiteral {
    private final String className;

    public ClassLiteral(String className) {
        this.className = className;
    }

    public String getClassName() {
        return this.className;
    }
}
