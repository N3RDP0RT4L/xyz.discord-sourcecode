package lombok.core.runtimeDependencies;

import com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel;
import com.zwitserloot.cmdreader.CmdReader;
import com.zwitserloot.cmdreader.Description;
import com.zwitserloot.cmdreader.InvalidCommandLineException;
import com.zwitserloot.cmdreader.Mandatory;
import com.zwitserloot.cmdreader.Requires;
import com.zwitserloot.cmdreader.Shorthand;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarOutputStream;
import java.util.zip.ZipEntry;
import lombok.core.LombokApp;
import lombok.core.SpiLoadUtil;
/* loaded from: xyz.discord_v112014.apk:lombok/core/runtimeDependencies/CreateLombokRuntimeApp.SCL.lombok */
public class CreateLombokRuntimeApp extends LombokApp {
    private List<RuntimeDependencyInfo> infoObjects;

    @Override // lombok.core.LombokApp
    public String getAppName() {
        return "createRuntime";
    }

    @Override // lombok.core.LombokApp
    public String getAppDescription() {
        return "Creates a small lombok-runtime.jar with the runtime\ndependencies of all lombok transformations that have them,\nand prints the names of each lombok transformation that\nrequires the lombok-runtime.jar at runtime.";
    }

    @Override // lombok.core.LombokApp
    public List<String> getAppAliases() {
        return Arrays.asList("runtime");
    }

    /* loaded from: xyz.discord_v112014.apk:lombok/core/runtimeDependencies/CreateLombokRuntimeApp$CmdArgs.SCL.lombok */
    private static class CmdArgs {
        @Description("Prints those lombok transformations that require lombok-runtime.jar.")
        @Mandatory(onlyIfNot = {"create"})
        @Shorthand({"p"})
        boolean print;
        @Description("Creates the lombok-runtime.jar.")
        @Mandatory(onlyIfNot = {"print"})
        @Shorthand({"c"})
        boolean create;
        @Requires({"create"})
        @Description("Where to write the lombok-runtime.jar. Defaults to the current working directory.")
        @Shorthand({"o"})
        String output;
        @Description("Shows this help text")
        boolean help;

        private CmdArgs() {
        }
    }

    @Override // lombok.core.LombokApp
    public int runApp(List<String> rawArgs) throws Exception {
        CmdReader<CmdArgs> reader = CmdReader.of(CmdArgs.class);
        try {
            CmdArgs args = reader.make((String[]) rawArgs.toArray(new String[0]));
            if (args.help) {
                printHelp(reader, null, System.out);
                return 0;
            }
            initializeInfoObjects();
            if (args.print) {
                printRuntimeDependents();
            }
            int errCode = 0;
            if (args.create) {
                File out = new File("./lombok-runtime.jar");
                if (args.output != null) {
                    out = new File(args.output);
                    if (out.isDirectory()) {
                        out = new File(out, "lombok-runtime.jar");
                    }
                }
                try {
                    errCode = writeRuntimeJar(out);
                } catch (Exception e) {
                    System.err.println("ERROR: Creating " + canonical(out) + " failed: ");
                    e.printStackTrace();
                    return 1;
                }
            }
            return errCode;
        } catch (InvalidCommandLineException e2) {
            printHelp(reader, e2.getMessage(), System.err);
            return 1;
        }
    }

    private void printRuntimeDependents() {
        List<String> descriptions = new ArrayList<>();
        Iterator<RuntimeDependencyInfo> it = this.infoObjects.iterator();
        while (it.hasNext()) {
            RuntimeDependencyInfo info = it.next();
            descriptions.addAll(info.getRuntimeDependentsDescriptions());
        }
        if (descriptions.isEmpty()) {
            System.out.println("Not printing dependents: No lombok transformations currently have any runtime dependencies!");
            return;
        }
        System.out.println("Using any of these lombok features means your app will need lombok-runtime.jar:");
        Iterator<String> it2 = descriptions.iterator();
        while (it2.hasNext()) {
            String desc = it2.next();
            System.out.println(desc);
        }
    }

    private int writeRuntimeJar(File outFile) throws Exception {
        Map<String, Class<?>> deps = new LinkedHashMap<>();
        Iterator<RuntimeDependencyInfo> it = this.infoObjects.iterator();
        while (it.hasNext()) {
            RuntimeDependencyInfo info = it.next();
            List<String> depNames = info.getRuntimeDependencies();
            if (depNames != null) {
                Iterator<String> it2 = depNames.iterator();
                while (it2.hasNext()) {
                    String depName = it2.next();
                    if (!deps.containsKey(depName)) {
                        deps.put(depName, info.getClass());
                    }
                }
            }
        }
        if (deps.isEmpty()) {
            System.out.println("Not generating lombok-runtime.jar: No lombok transformations currently have any runtime dependencies!");
            return 1;
        }
        OutputStream out = new FileOutputStream(outFile);
        try {
            JarOutputStream jar = new JarOutputStream(out);
            deps.put("LICENSE", CreateLombokRuntimeApp.class);
            deps.put("AUTHORS", CreateLombokRuntimeApp.class);
            Iterator<Map.Entry<String, Class<?>>> it3 = deps.entrySet().iterator();
            while (it3.hasNext()) {
                Map.Entry<String, Class<?>> dep = it3.next();
                InputStream in = dep.getValue().getResourceAsStream(AutocompleteViewModel.COMMAND_DISCOVER_TOKEN + dep.getKey());
                if (in == null) {
                    throw new Fail(String.format("Dependency %s contributed by %s cannot be found", dep.getKey(), dep.getValue()));
                }
                writeIntoJar(jar, dep.getKey(), in);
                if (in != null) {
                    in.close();
                }
            }
            jar.close();
            out.close();
            System.out.println("Successfully created: " + canonical(outFile));
            return 0;
        } catch (Throwable t) {
            try {
                out.close();
            } catch (Throwable unused) {
            }
            if (0 == 0) {
                outFile.delete();
            }
            if (t instanceof Fail) {
                System.err.println(t.getMessage());
                return 1;
            } else if (t instanceof Exception) {
                throw ((Exception) t);
            } else if (t instanceof Error) {
                throw ((Error) t);
            } else {
                throw new Exception(t);
            }
        }
    }

    private void writeIntoJar(JarOutputStream jar, String depName, InputStream in) throws IOException {
        jar.putNextEntry(new ZipEntry(depName));
        byte[] b2 = new byte[65536];
        while (true) {
            int r = in.read(b2);
            if (r == -1) {
                jar.closeEntry();
                in.close();
                return;
            }
            jar.write(b2, 0, r);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* loaded from: xyz.discord_v112014.apk:lombok/core/runtimeDependencies/CreateLombokRuntimeApp$Fail.SCL.lombok */
    public static class Fail extends Exception {
        Fail(String message) {
            super(message);
        }
    }

    private void initializeInfoObjects() throws IOException {
        this.infoObjects = SpiLoadUtil.readAllFromIterator(SpiLoadUtil.findServices(RuntimeDependencyInfo.class));
    }

    private static String canonical(File out) {
        try {
            return out.getCanonicalPath();
        } catch (Exception unused) {
            return out.getAbsolutePath();
        }
    }

    private void printHelp(CmdReader<CmdArgs> reader, String message, PrintStream out) {
        if (message != null) {
            out.println(message);
            out.println("----------------------------");
        }
        out.println(reader.generateCommandLineHelp("java -jar lombok.jar createRuntime"));
    }
}
