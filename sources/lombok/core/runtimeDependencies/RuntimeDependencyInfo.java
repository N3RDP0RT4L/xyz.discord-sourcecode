package lombok.core.runtimeDependencies;

import java.util.List;
/* loaded from: xyz.discord_v112014.apk:lombok/core/runtimeDependencies/RuntimeDependencyInfo.SCL.lombok */
public interface RuntimeDependencyInfo {
    List<String> getRuntimeDependentsDescriptions();

    List<String> getRuntimeDependencies();
}
