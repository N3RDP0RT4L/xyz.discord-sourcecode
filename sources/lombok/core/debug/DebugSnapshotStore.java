package lombok.core.debug;

import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import org.eclipse.jdt.internal.compiler.ast.CompilationUnitDeclaration;
/* loaded from: xyz.discord_v112014.apk:lombok/core/debug/DebugSnapshotStore.SCL.lombok */
public class DebugSnapshotStore {
    public static final DebugSnapshotStore INSTANCE = new DebugSnapshotStore();
    public static final boolean GLOBAL_DSS_DISABLE_SWITCH = true;
    private final Map<CompilationUnitDeclaration, List<DebugSnapshot>> map = new WeakHashMap();

    /*  JADX ERROR: NullPointerException in pass: BlockProcessor
        java.lang.NullPointerException: Cannot invoke "jadx.core.dex.nodes.BlockNode.getPredecessors()" because "to" is null
        	at jadx.core.dex.visitors.blocks.BlockSplitter.removeConnection(BlockSplitter.java:156)
        	at jadx.core.dex.visitors.blocks.BlockExceptionHandler.removeExcHandler(BlockExceptionHandler.java:328)
        	at jadx.core.dex.visitors.blocks.BlockExceptionHandler.lambda$prepareTryBlocks$2(BlockExceptionHandler.java:205)
        	at java.base/java.util.ArrayList.forEach(ArrayList.java:1511)
        	at jadx.core.dex.visitors.blocks.BlockExceptionHandler.prepareTryBlocks(BlockExceptionHandler.java:205)
        	at jadx.core.dex.visitors.blocks.BlockExceptionHandler.process(BlockExceptionHandler.java:58)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.independentBlockTreeMod(BlockProcessor.java:459)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:51)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:44)
        */
    public void snapshot(org.eclipse.jdt.internal.compiler.ast.CompilationUnitDeclaration r8, java.lang.String r9, java.lang.Object... r10) {
        /*
            r7 = this;
            return
            lombok.core.debug.DebugSnapshot r0 = new lombok.core.debug.DebugSnapshot
            r1 = r0
            r2 = r8
            r3 = 1
            r4 = r9
            r5 = r10
            r1.<init>(r2, r3, r4, r5)
            r11 = r0
            r0 = r7
            java.util.Map<org.eclipse.jdt.internal.compiler.ast.CompilationUnitDeclaration, java.util.List<lombok.core.debug.DebugSnapshot>> r0 = r0.map
            r1 = r0
            r13 = r1
            monitor-enter(r0)
            r0 = r7
            java.util.Map<org.eclipse.jdt.internal.compiler.ast.CompilationUnitDeclaration, java.util.List<lombok.core.debug.DebugSnapshot>> r0 = r0.map     // Catch: java.lang.Throwable -> L67
            r1 = r8
            java.lang.Object r0 = r0.get(r1)     // Catch: java.lang.Throwable -> L67
            java.util.List r0 = (java.util.List) r0     // Catch: java.lang.Throwable -> L67
            r12 = r0
            r0 = r12
            if (r0 != 0) goto L4d
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch: java.lang.Throwable -> L67
            r1 = r0
            r1.<init>()     // Catch: java.lang.Throwable -> L67
            r12 = r0
            r0 = r7
            java.util.Map<org.eclipse.jdt.internal.compiler.ast.CompilationUnitDeclaration, java.util.List<lombok.core.debug.DebugSnapshot>> r0 = r0.map     // Catch: java.lang.Throwable -> L67
            r1 = r8
            r2 = r12
            java.lang.Object r0 = r0.put(r1, r2)     // Catch: java.lang.Throwable -> L67
            r0 = r12
            r1 = r11
            boolean r0 = r0.add(r1)     // Catch: java.lang.Throwable -> L67
            goto L61
        L4d:
            r0 = r12
            boolean r0 = r0.isEmpty()     // Catch: java.lang.Throwable -> L67
            if (r0 != 0) goto L61
            r0 = r12
            r1 = r11
            boolean r0 = r0.add(r1)     // Catch: java.lang.Throwable -> L67
        L61:
            r0 = r13
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L67
            goto L6b
        L67:
            r1 = move-exception
            monitor-exit(r1)     // Catch: java.lang.Throwable -> L67
            throw r0     // Catch: java.lang.Throwable -> L67
        L6b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: lombok.core.debug.DebugSnapshotStore.snapshot(org.eclipse.jdt.internal.compiler.ast.CompilationUnitDeclaration, java.lang.String, java.lang.Object[]):void");
    }

    /*  JADX ERROR: NullPointerException in pass: BlockProcessor
        java.lang.NullPointerException: Cannot invoke "jadx.core.dex.nodes.BlockNode.getPredecessors()" because "to" is null
        	at jadx.core.dex.visitors.blocks.BlockSplitter.removeConnection(BlockSplitter.java:156)
        	at jadx.core.dex.visitors.blocks.BlockExceptionHandler.removeExcHandler(BlockExceptionHandler.java:328)
        	at jadx.core.dex.visitors.blocks.BlockExceptionHandler.lambda$prepareTryBlocks$2(BlockExceptionHandler.java:205)
        	at java.base/java.util.ArrayList.forEach(ArrayList.java:1511)
        	at jadx.core.dex.visitors.blocks.BlockExceptionHandler.prepareTryBlocks(BlockExceptionHandler.java:205)
        	at jadx.core.dex.visitors.blocks.BlockExceptionHandler.process(BlockExceptionHandler.java:58)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.independentBlockTreeMod(BlockProcessor.java:459)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:51)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:44)
        */
    public void log(org.eclipse.jdt.internal.compiler.ast.CompilationUnitDeclaration r8, java.lang.String r9, java.lang.Object... r10) {
        /*
            r7 = this;
            return
            lombok.core.debug.DebugSnapshot r0 = new lombok.core.debug.DebugSnapshot
            r1 = r0
            r2 = r8
            r3 = -1
            r4 = r9
            r5 = r10
            r1.<init>(r2, r3, r4, r5)
            r11 = r0
            r0 = r7
            java.util.Map<org.eclipse.jdt.internal.compiler.ast.CompilationUnitDeclaration, java.util.List<lombok.core.debug.DebugSnapshot>> r0 = r0.map
            r1 = r0
            r13 = r1
            monitor-enter(r0)
            r0 = r7
            java.util.Map<org.eclipse.jdt.internal.compiler.ast.CompilationUnitDeclaration, java.util.List<lombok.core.debug.DebugSnapshot>> r0 = r0.map     // Catch: java.lang.Throwable -> L67
            r1 = r8
            java.lang.Object r0 = r0.get(r1)     // Catch: java.lang.Throwable -> L67
            java.util.List r0 = (java.util.List) r0     // Catch: java.lang.Throwable -> L67
            r12 = r0
            r0 = r12
            if (r0 != 0) goto L4d
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch: java.lang.Throwable -> L67
            r1 = r0
            r1.<init>()     // Catch: java.lang.Throwable -> L67
            r12 = r0
            r0 = r7
            java.util.Map<org.eclipse.jdt.internal.compiler.ast.CompilationUnitDeclaration, java.util.List<lombok.core.debug.DebugSnapshot>> r0 = r0.map     // Catch: java.lang.Throwable -> L67
            r1 = r8
            r2 = r12
            java.lang.Object r0 = r0.put(r1, r2)     // Catch: java.lang.Throwable -> L67
            r0 = r12
            r1 = r11
            boolean r0 = r0.add(r1)     // Catch: java.lang.Throwable -> L67
            goto L61
        L4d:
            r0 = r12
            boolean r0 = r0.isEmpty()     // Catch: java.lang.Throwable -> L67
            if (r0 != 0) goto L61
            r0 = r12
            r1 = r11
            boolean r0 = r0.add(r1)     // Catch: java.lang.Throwable -> L67
        L61:
            r0 = r13
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L67
            goto L6b
        L67:
            r1 = move-exception
            monitor-exit(r1)     // Catch: java.lang.Throwable -> L67
            throw r0     // Catch: java.lang.Throwable -> L67
        L6b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: lombok.core.debug.DebugSnapshotStore.log(org.eclipse.jdt.internal.compiler.ast.CompilationUnitDeclaration, java.lang.String, java.lang.Object[]):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: Failed to lock block: B:3:0x0088
        	at jadx.core.dex.nodes.BlockNode.lock(BlockNode.java:79)
        	at jadx.core.utils.ImmutableList.forEach(ImmutableList.java:113)
        	at jadx.core.dex.nodes.MethodNode.finishBasicBlocks(MethodNode.java:312)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:81)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:44)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Dominance frontier not set for block: B:3:0x0088
        	at jadx.core.dex.nodes.BlockNode.lock(BlockNode.java:76)
        	... 4 more
        */
    public java.lang.String print(org.eclipse.jdt.internal.compiler.ast.CompilationUnitDeclaration r11, java.lang.String r12, java.lang.Object... r13) {
        /*
            Method dump skipped, instructions count: 367
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: lombok.core.debug.DebugSnapshotStore.print(org.eclipse.jdt.internal.compiler.ast.CompilationUnitDeclaration, java.lang.String, java.lang.Object[]):java.lang.String");
    }
}
