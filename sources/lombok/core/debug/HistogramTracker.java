package lombok.core.debug;

import java.io.PrintStream;
import java.util.GregorianCalendar;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicStampedReference;
/* loaded from: xyz.discord_v112014.apk:lombok/core/debug/HistogramTracker.SCL.lombok */
public class HistogramTracker {
    private static final long[] RANGES = {250001, 500001, 1000001, 2000001, 4000001, 8000001, 16000001, 32000001, 64000001, 128000001, 256000001, 512000001, 1024000001, 2048000001, 10000000001L};
    private static final long REPORT_WINDOW = 60000;
    private final String category;
    private final AtomicStampedReference<long[]> bars;
    private final AtomicBoolean addedSysHook;
    private final PrintStream out;

    public HistogramTracker(String category) {
        this.bars = new AtomicStampedReference<>(new long[RANGES.length + 2], 0);
        this.addedSysHook = new AtomicBoolean(false);
        this.category = category;
        this.out = null;
        printInit();
    }

    public HistogramTracker(String category, PrintStream out) {
        this.bars = new AtomicStampedReference<>(new long[RANGES.length + 2], 0);
        this.addedSysHook = new AtomicBoolean(false);
        this.category = category;
        this.out = out;
        printInit();
    }

    private void printInit() {
        if (this.category == null) {
            if (this.out == null) {
                ProblemReporter.info("Initialized histogram", null);
            } else {
                this.out.println("Initialized histogram");
            }
        } else if (this.out == null) {
            ProblemReporter.info(String.format("Initialized histogram tracker for '%s'", this.category), null);
        } else {
            this.out.printf("Initialized histogram tracker for '%s'%n", this.category);
        }
    }

    public long start() {
        return System.nanoTime();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v6, types: [long] */
    /* JADX WARN: Type inference failed for: r14v1 */
    /* JADX WARN: Type inference failed for: r14v2 */
    /* JADX WARN: Type inference failed for: r14v3 */
    public void end(long startToken) {
        long[] newBars;
        if (!this.addedSysHook.getAndSet(true)) {
            Runtime.getRuntime().addShutdownHook(new Thread("Histogram Printer") { // from class: lombok.core.debug.HistogramTracker.1
                @Override // java.lang.Thread, java.lang.Runnable
                public void run() {
                    int[] currentInterval = new int[1];
                    long[] b2 = (long[]) HistogramTracker.this.bars.get(currentInterval);
                    HistogramTracker.this.printReport(currentInterval[0], b2);
                }
            });
        }
        long end = System.nanoTime();
        long now = System.currentTimeMillis();
        char c = end - startToken;
        int i = (c > 0L ? 1 : (c == 0L ? 0 : -1));
        ?? r14 = c;
        if (i < 0) {
            r14 = 0;
        }
        int interval = (int) (now / 60000);
        int[] currentInterval = new int[1];
        long[] bars = this.bars.get(currentInterval);
        if (currentInterval[0] != interval) {
            printReport(currentInterval[0], bars);
            newBars = new long[RANGES.length + 2];
            if (!this.bars.compareAndSet(bars, newBars, currentInterval[0], interval)) {
                newBars = this.bars.get(currentInterval);
            }
        } else {
            newBars = bars;
        }
        long[] jArr = newBars;
        int length = RANGES.length + 1;
        jArr[length] = jArr[length] + r14;
        for (int i2 = 0; i2 < RANGES.length; i2++) {
            if (r14 < RANGES[i2]) {
                long[] jArr2 = newBars;
                int i3 = i2;
                jArr2[i3] = jArr2[i3] + 1;
                return;
            }
        }
        long[] jArr3 = newBars;
        int length2 = RANGES.length;
        jArr3[length2] = jArr3[length2] + 1;
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v20 */
    /* JADX WARN: Type inference failed for: r13v3 */
    public void printReport(int interval, long[] bars) {
        StringBuilder sb = new StringBuilder();
        if (this.category != null) {
            sb.append(this.category).append(" ");
        }
        sb.append("[");
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTimeInMillis(interval * 60000);
        int hour = gc.get(11);
        int minute = gc.get(12);
        if (hour < 10) {
            sb.append('0');
        }
        sb.append(hour).append(":");
        if (minute < 10) {
            sb.append('0');
        }
        sb.append(minute).append("] {");
        char c = bars[RANGES.length];
        int count = 0;
        int lastZeroPos = sb.length();
        int i = 0;
        while (i < RANGES.length) {
            long sum = c + bars[i];
            sb.append(bars[i]);
            if (bars[i] != 0) {
                lastZeroPos = sb.length();
            }
            sb.append(" ");
            count++;
            if (count == 3) {
                sb.append("-- ");
            }
            if (count == 9) {
                sb.append("-- ");
            }
            i++;
            c = sum;
        }
        if (c != 0) {
            sb.setLength(lastZeroPos);
            double millis = bars[RANGES.length + 1] / 1000000.0d;
            long over = bars[RANGES.length];
            if (over > 0) {
                sb.append(" -- ").append(bars[RANGES.length]);
            }
            sb.append("} total calls: ").append((long) c).append(" total time (millis): ").append((int) (millis + 0.5d));
            if (this.out == null) {
                ProblemReporter.info(sb.toString(), null);
            } else {
                this.out.println(sb.toString());
            }
        }
    }
}
