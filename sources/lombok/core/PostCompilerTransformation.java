package lombok.core;
/* loaded from: xyz.discord_v112014.apk:lombok/core/PostCompilerTransformation.SCL.lombok */
public interface PostCompilerTransformation {
    byte[] applyTransformations(byte[] bArr, String str, DiagnosticsReceiver diagnosticsReceiver);
}
