package lombok.core;
/* loaded from: xyz.discord_v112014.apk:lombok/core/Augments.SCL.lombok */
public final class Augments {
    public static final FieldAugment<ClassLoader, Boolean> ClassLoader_lombokAlreadyAddedTo = FieldAugment.augment(ClassLoader.class, Boolean.TYPE, "lombok$alreadyAddedTo");

    private Augments() {
    }
}
