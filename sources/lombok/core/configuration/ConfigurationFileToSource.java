package lombok.core.configuration;
/* loaded from: xyz.discord_v112014.apk:lombok/core/configuration/ConfigurationFileToSource.SCL.lombok */
public interface ConfigurationFileToSource {
    ConfigurationSource parsed(ConfigurationFile configurationFile);
}
