package lombok.core.configuration;

import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;
import lombok.ConfigurationKeys;
import lombok.core.SpiLoadUtil;
/* loaded from: xyz.discord_v112014.apk:lombok/core/configuration/ConfigurationKeysLoader.SCL.lombok */
public interface ConfigurationKeysLoader {

    /* loaded from: xyz.discord_v112014.apk:lombok/core/configuration/ConfigurationKeysLoader$LoaderLoader.SCL.lombok */
    public static class LoaderLoader {
        private static final AtomicBoolean alreadyLoaded = new AtomicBoolean(false);

        private LoaderLoader() {
        }

        public static void loadAllConfigurationKeys() {
            if (!alreadyLoaded.get()) {
                try {
                    Class.forName(ConfigurationKeys.class.getName());
                } catch (Throwable unused) {
                }
                try {
                    try {
                        Iterator<ConfigurationKeysLoader> iterator = SpiLoadUtil.findServices(ConfigurationKeysLoader.class, ConfigurationKeysLoader.class.getClassLoader()).iterator();
                        while (iterator.hasNext()) {
                            try {
                                iterator.next();
                            } catch (Exception unused2) {
                            }
                        }
                    } catch (IOException e) {
                        throw new RuntimeException("Can't load config keys; services file issue.", e);
                    }
                } finally {
                    alreadyLoaded.set(true);
                }
            }
        }
    }
}
