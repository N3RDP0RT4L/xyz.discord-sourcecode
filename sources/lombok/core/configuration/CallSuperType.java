package lombok.core.configuration;
/* loaded from: xyz.discord_v112014.apk:lombok/core/configuration/CallSuperType.SCL.lombok */
public enum CallSuperType {
    CALL,
    SKIP,
    WARN
}
