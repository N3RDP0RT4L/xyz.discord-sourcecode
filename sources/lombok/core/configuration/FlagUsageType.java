package lombok.core.configuration;
/* loaded from: xyz.discord_v112014.apk:lombok/core/configuration/FlagUsageType.SCL.lombok */
public enum FlagUsageType {
    WARNING,
    ERROR,
    ALLOW
}
