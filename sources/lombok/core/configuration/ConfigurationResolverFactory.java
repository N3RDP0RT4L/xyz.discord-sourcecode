package lombok.core.configuration;

import java.net.URI;
/* loaded from: xyz.discord_v112014.apk:lombok/core/configuration/ConfigurationResolverFactory.SCL.lombok */
public interface ConfigurationResolverFactory {
    ConfigurationResolver createResolver(URI uri);
}
