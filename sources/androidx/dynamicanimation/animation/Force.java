package androidx.dynamicanimation.animation;
/* loaded from: classes.dex */
public interface Force {
    float getAcceleration(float f, float f2);

    boolean isAtEquilibrium(float f, float f2);
}
