package androidx.transition;

import android.view.View;
import androidx.annotation.NonNull;
import b.d.b.a.a;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
/* loaded from: classes.dex */
public class TransitionValues {
    public View view;
    public final Map<String, Object> values = new HashMap();
    public final ArrayList<Transition> mTargetedTransitions = new ArrayList<>();

    @Deprecated
    public TransitionValues() {
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof TransitionValues)) {
            return false;
        }
        TransitionValues transitionValues = (TransitionValues) obj;
        return this.view == transitionValues.view && this.values.equals(transitionValues.values);
    }

    public int hashCode() {
        return this.values.hashCode() + (this.view.hashCode() * 31);
    }

    public String toString() {
        StringBuilder R = a.R("TransitionValues@");
        R.append(Integer.toHexString(hashCode()));
        R.append(":\n");
        StringBuilder V = a.V(R.toString(), "    view = ");
        V.append(this.view);
        V.append("\n");
        String v = a.v(V.toString(), "    values:");
        for (String str : this.values.keySet()) {
            v = v + "    " + str + ": " + this.values.get(str) + "\n";
        }
        return v;
    }

    public TransitionValues(@NonNull View view) {
        this.view = view;
    }
}
