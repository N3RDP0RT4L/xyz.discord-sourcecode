package androidx.browser.trusted;

import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import x.a.a.d.a;
/* loaded from: classes.dex */
public class TrustedWebActivityCallbackRemote {
    private final a mCallbackBinder;

    private TrustedWebActivityCallbackRemote(@NonNull a aVar) {
        this.mCallbackBinder = aVar;
    }

    @Nullable
    public static TrustedWebActivityCallbackRemote fromBinder(@Nullable IBinder iBinder) {
        a asInterface = iBinder == null ? null : a.AbstractBinderC0433a.asInterface(iBinder);
        if (asInterface == null) {
            return null;
        }
        return new TrustedWebActivityCallbackRemote(asInterface);
    }

    public void runExtraCallback(@NonNull String str, @NonNull Bundle bundle) throws RemoteException {
        this.mCallbackBinder.onExtraCallback(str, bundle);
    }
}
