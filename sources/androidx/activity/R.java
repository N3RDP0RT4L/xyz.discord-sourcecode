package androidx.activity;
/* loaded from: classes.dex */
public final class R {

    /* loaded from: classes.dex */
    public static final class attr {
        public static final int alpha = 0x7f04003b;
        public static final int font = 0x7f0402e7;
        public static final int fontProviderAuthority = 0x7f0402e9;
        public static final int fontProviderCerts = 0x7f0402ea;
        public static final int fontProviderFetchStrategy = 0x7f0402eb;
        public static final int fontProviderFetchTimeout = 0x7f0402ec;
        public static final int fontProviderPackage = 0x7f0402ed;
        public static final int fontProviderQuery = 0x7f0402ee;
        public static final int fontStyle = 0x7f0402f0;
        public static final int fontVariationSettings = 0x7f0402f1;
        public static final int fontWeight = 0x7f0402f2;
        public static final int ttcIndex = 0x7f0406c8;

        private attr() {
        }
    }

    /* loaded from: classes.dex */
    public static final class color {
        public static final int notification_action_color_filter = 0x7f0601c5;
        public static final int notification_icon_bg_color = 0x7f0601c6;
        public static final int ripple_material_light = 0x7f060257;
        public static final int secondary_text_default_material_light = 0x7f06025b;

        private color() {
        }
    }

    /* loaded from: classes.dex */
    public static final class dimen {
        public static final int compat_button_inset_horizontal_material = 0x7f07008d;
        public static final int compat_button_inset_vertical_material = 0x7f07008e;
        public static final int compat_button_padding_horizontal_material = 0x7f07008f;
        public static final int compat_button_padding_vertical_material = 0x7f070090;
        public static final int compat_control_corner_material = 0x7f070091;
        public static final int compat_notification_large_icon_max_height = 0x7f070092;
        public static final int compat_notification_large_icon_max_width = 0x7f070093;
        public static final int notification_action_icon_size = 0x7f0701fc;
        public static final int notification_action_text_size = 0x7f0701fd;
        public static final int notification_big_circle_margin = 0x7f0701fe;
        public static final int notification_content_margin_start = 0x7f0701ff;
        public static final int notification_large_icon_height = 0x7f070200;
        public static final int notification_large_icon_width = 0x7f070201;
        public static final int notification_main_column_padding_top = 0x7f070202;
        public static final int notification_media_narrow_margin = 0x7f070203;
        public static final int notification_right_icon_size = 0x7f070204;
        public static final int notification_right_side_padding_top = 0x7f070205;
        public static final int notification_small_icon_background_padding = 0x7f070206;
        public static final int notification_small_icon_size_as_large = 0x7f070207;
        public static final int notification_subtext_size = 0x7f070208;
        public static final int notification_top_pad = 0x7f070209;
        public static final int notification_top_pad_large_text = 0x7f07020a;

        private dimen() {
        }
    }

    /* loaded from: classes.dex */
    public static final class drawable {
        public static final int notification_action_background = 0x7f0806a4;
        public static final int notification_bg = 0x7f0806a5;
        public static final int notification_bg_low = 0x7f0806a6;
        public static final int notification_bg_low_normal = 0x7f0806a7;
        public static final int notification_bg_low_pressed = 0x7f0806a8;
        public static final int notification_bg_normal = 0x7f0806a9;
        public static final int notification_bg_normal_pressed = 0x7f0806aa;
        public static final int notification_icon_background = 0x7f0806ab;
        public static final int notification_template_icon_bg = 0x7f0806ac;
        public static final int notification_template_icon_low_bg = 0x7f0806ad;
        public static final int notification_tile_bg = 0x7f0806ae;
        public static final int notify_panel_notification_icon_bg = 0x7f0806af;

        private drawable() {
        }
    }

    /* loaded from: classes.dex */
    public static final class id {
        public static final int accessibility_action_clickable_span = 0x7f0a0023;
        public static final int accessibility_custom_action_0 = 0x7f0a0024;
        public static final int accessibility_custom_action_1 = 0x7f0a0025;
        public static final int accessibility_custom_action_10 = 0x7f0a0026;
        public static final int accessibility_custom_action_11 = 0x7f0a0027;
        public static final int accessibility_custom_action_12 = 0x7f0a0028;
        public static final int accessibility_custom_action_13 = 0x7f0a0029;
        public static final int accessibility_custom_action_14 = 0x7f0a002a;
        public static final int accessibility_custom_action_15 = 0x7f0a002b;
        public static final int accessibility_custom_action_16 = 0x7f0a002c;
        public static final int accessibility_custom_action_17 = 0x7f0a002d;
        public static final int accessibility_custom_action_18 = 0x7f0a002e;
        public static final int accessibility_custom_action_19 = 0x7f0a002f;
        public static final int accessibility_custom_action_2 = 0x7f0a0030;
        public static final int accessibility_custom_action_20 = 0x7f0a0031;
        public static final int accessibility_custom_action_21 = 0x7f0a0032;
        public static final int accessibility_custom_action_22 = 0x7f0a0033;
        public static final int accessibility_custom_action_23 = 0x7f0a0034;
        public static final int accessibility_custom_action_24 = 0x7f0a0035;
        public static final int accessibility_custom_action_25 = 0x7f0a0036;
        public static final int accessibility_custom_action_26 = 0x7f0a0037;
        public static final int accessibility_custom_action_27 = 0x7f0a0038;
        public static final int accessibility_custom_action_28 = 0x7f0a0039;
        public static final int accessibility_custom_action_29 = 0x7f0a003a;
        public static final int accessibility_custom_action_3 = 0x7f0a003b;
        public static final int accessibility_custom_action_30 = 0x7f0a003c;
        public static final int accessibility_custom_action_31 = 0x7f0a003d;
        public static final int accessibility_custom_action_4 = 0x7f0a003e;
        public static final int accessibility_custom_action_5 = 0x7f0a003f;
        public static final int accessibility_custom_action_6 = 0x7f0a0040;
        public static final int accessibility_custom_action_7 = 0x7f0a0041;
        public static final int accessibility_custom_action_8 = 0x7f0a0042;
        public static final int accessibility_custom_action_9 = 0x7f0a0043;
        public static final int action_container = 0x7f0a0059;
        public static final int action_divider = 0x7f0a005c;
        public static final int action_image = 0x7f0a005d;
        public static final int action_text = 0x7f0a0066;
        public static final int actions = 0x7f0a0067;
        public static final int async = 0x7f0a00d5;
        public static final int blocking = 0x7f0a0172;
        public static final int chronometer = 0x7f0a03a3;
        public static final int dialog_button = 0x7f0a04b5;
        public static final int forever = 0x7f0a0650;
        public static final int icon = 0x7f0a0872;
        public static final int icon_group = 0x7f0a0875;
        public static final int info = 0x7f0a08a9;
        public static final int italic = 0x7f0a08e3;
        public static final int line1 = 0x7f0a0958;
        public static final int line3 = 0x7f0a0959;
        public static final int normal = 0x7f0a0a7f;
        public static final int notification_background = 0x7f0a0a8a;
        public static final int notification_main_column = 0x7f0a0a8b;
        public static final int notification_main_column_container = 0x7f0a0a8c;
        public static final int right_icon = 0x7f0a0c17;
        public static final int right_side = 0x7f0a0c18;
        public static final int tag_accessibility_actions = 0x7f0a0f18;
        public static final int tag_accessibility_clickable_spans = 0x7f0a0f19;
        public static final int tag_accessibility_heading = 0x7f0a0f1a;
        public static final int tag_accessibility_pane_title = 0x7f0a0f1b;
        public static final int tag_screen_reader_focusable = 0x7f0a0f1f;
        public static final int tag_transition_group = 0x7f0a0f21;
        public static final int tag_unhandled_key_event_manager = 0x7f0a0f22;
        public static final int tag_unhandled_key_listeners = 0x7f0a0f23;
        public static final int text = 0x7f0a0f30;
        public static final int text2 = 0x7f0a0f32;
        public static final int time = 0x7f0a0f90;
        public static final int title = 0x7f0a0f97;
        public static final int view_tree_lifecycle_owner = 0x7f0a1092;
        public static final int view_tree_saved_state_registry_owner = 0x7f0a1093;
        public static final int view_tree_view_model_store_owner = 0x7f0a1094;

        private id() {
        }
    }

    /* loaded from: classes.dex */
    public static final class integer {
        public static final int status_bar_notification_info_maxnum = 0x7f0b001b;

        private integer() {
        }
    }

    /* loaded from: classes.dex */
    public static final class layout {
        public static final int custom_dialog = 0x7f0d003b;
        public static final int notification_action = 0x7f0d00da;
        public static final int notification_action_tombstone = 0x7f0d00db;
        public static final int notification_template_custom_big = 0x7f0d00e3;
        public static final int notification_template_icon_group = 0x7f0d00e4;
        public static final int notification_template_part_chronometer = 0x7f0d00e8;
        public static final int notification_template_part_time = 0x7f0d00e9;

        private layout() {
        }
    }

    /* loaded from: classes.dex */
    public static final class string {
        public static final int status_bar_notification_info_overflow = 0x7f12213b;

        private string() {
        }
    }

    /* loaded from: classes.dex */
    public static final class style {
        public static final int TextAppearance_Compat_Notification = 0x7f130250;
        public static final int TextAppearance_Compat_Notification_Info = 0x7f130251;
        public static final int TextAppearance_Compat_Notification_Line2 = 0x7f130253;
        public static final int TextAppearance_Compat_Notification_Time = 0x7f130256;
        public static final int TextAppearance_Compat_Notification_Title = 0x7f130258;
        public static final int Widget_Compat_NotificationActionContainer = 0x7f13043e;
        public static final int Widget_Compat_NotificationActionText = 0x7f13043f;

        private style() {
        }
    }

    /* loaded from: classes.dex */
    public static final class styleable {
        public static final int ColorStateListItem_alpha = 0x00000002;
        public static final int ColorStateListItem_android_alpha = 0x00000001;
        public static final int ColorStateListItem_android_color = 0x00000000;
        public static final int FontFamilyFont_android_font = 0x00000000;
        public static final int FontFamilyFont_android_fontStyle = 0x00000002;
        public static final int FontFamilyFont_android_fontVariationSettings = 0x00000004;
        public static final int FontFamilyFont_android_fontWeight = 0x00000001;
        public static final int FontFamilyFont_android_ttcIndex = 0x00000003;
        public static final int FontFamilyFont_font = 0x00000005;
        public static final int FontFamilyFont_fontStyle = 0x00000006;
        public static final int FontFamilyFont_fontVariationSettings = 0x00000007;
        public static final int FontFamilyFont_fontWeight = 0x00000008;
        public static final int FontFamilyFont_ttcIndex = 0x00000009;
        public static final int FontFamily_fontProviderAuthority = 0x00000000;
        public static final int FontFamily_fontProviderCerts = 0x00000001;
        public static final int FontFamily_fontProviderFetchStrategy = 0x00000002;
        public static final int FontFamily_fontProviderFetchTimeout = 0x00000003;
        public static final int FontFamily_fontProviderPackage = 0x00000004;
        public static final int FontFamily_fontProviderQuery = 0x00000005;
        public static final int FontFamily_fontProviderSystemFontFamily = 0x00000006;
        public static final int GradientColorItem_android_color = 0x00000000;
        public static final int GradientColorItem_android_offset = 0x00000001;
        public static final int GradientColor_android_centerColor = 0x00000007;
        public static final int GradientColor_android_centerX = 0x00000003;
        public static final int GradientColor_android_centerY = 0x00000004;
        public static final int GradientColor_android_endColor = 0x00000001;
        public static final int GradientColor_android_endX = 0x0000000a;
        public static final int GradientColor_android_endY = 0x0000000b;
        public static final int GradientColor_android_gradientRadius = 0x00000005;
        public static final int GradientColor_android_startColor = 0x00000000;
        public static final int GradientColor_android_startX = 0x00000008;
        public static final int GradientColor_android_startY = 0x00000009;
        public static final int GradientColor_android_tileMode = 0x00000006;
        public static final int GradientColor_android_type = 0x00000002;
        public static final int[] ColorStateListItem = {16843173, 16843551, xyz.discord.R.attr.alpha};
        public static final int[] FontFamily = {xyz.discord.R.attr.fontProviderAuthority, xyz.discord.R.attr.fontProviderCerts, xyz.discord.R.attr.fontProviderFetchStrategy, xyz.discord.R.attr.fontProviderFetchTimeout, xyz.discord.R.attr.fontProviderPackage, xyz.discord.R.attr.fontProviderQuery, xyz.discord.R.attr.fontProviderSystemFontFamily};
        public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, 16844143, 16844144, xyz.discord.R.attr.font, xyz.discord.R.attr.fontStyle, xyz.discord.R.attr.fontVariationSettings, xyz.discord.R.attr.fontWeight, xyz.discord.R.attr.ttcIndex};
        public static final int[] GradientColor = {16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051};
        public static final int[] GradientColorItem = {16843173, 16844052};

        private styleable() {
        }
    }

    private R() {
    }
}
