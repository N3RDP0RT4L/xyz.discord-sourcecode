package androidx.core.util;

import androidx.annotation.NonNull;
import b.d.b.a.a;
/* loaded from: classes.dex */
public class Pair<F, S> {
    public final F first;
    public final S second;

    public Pair(F f, S s2) {
        this.first = f;
        this.second = s2;
    }

    @NonNull
    public static <A, B> Pair<A, B> create(A a, B b2) {
        return new Pair<>(a, b2);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Pair)) {
            return false;
        }
        Pair pair = (Pair) obj;
        return ObjectsCompat.equals(pair.first, this.first) && ObjectsCompat.equals(pair.second, this.second);
    }

    public int hashCode() {
        F f = this.first;
        int i = 0;
        int hashCode = f == null ? 0 : f.hashCode();
        S s2 = this.second;
        if (s2 != null) {
            i = s2.hashCode();
        }
        return hashCode ^ i;
    }

    @NonNull
    public String toString() {
        StringBuilder R = a.R("Pair{");
        R.append(this.first);
        R.append(" ");
        R.append(this.second);
        R.append("}");
        return R.toString();
    }
}
