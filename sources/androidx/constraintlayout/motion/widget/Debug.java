package androidx.constraintlayout.motion.widget;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import b.d.b.a.a;
import com.applisto.appcloner.classes.BuildConfig;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
/* loaded from: classes.dex */
public class Debug {
    public static void dumpLayoutParams(ViewGroup viewGroup, String str) {
        Field[] fields;
        StackTraceElement stackTraceElement = new Throwable().getStackTrace()[1];
        StringBuilder R = a.R(".(");
        R.append(stackTraceElement.getFileName());
        R.append(":");
        R.append(stackTraceElement.getLineNumber());
        R.append(") ");
        R.append(str);
        R.append("  ");
        String sb = R.toString();
        int childCount = viewGroup.getChildCount();
        System.out.println(str + " children " + childCount);
        for (int i = 0; i < childCount; i++) {
            View childAt = viewGroup.getChildAt(i);
            PrintStream printStream = System.out;
            StringBuilder V = a.V(sb, "     ");
            V.append(getName(childAt));
            printStream.println(V.toString());
            ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
            for (Field field : layoutParams.getClass().getFields()) {
                try {
                    Object obj = field.get(layoutParams);
                    if (field.getName().contains("To") && !obj.toString().equals("-1")) {
                        System.out.println(sb + "       " + field.getName() + " " + obj);
                    }
                } catch (IllegalAccessException unused) {
                }
            }
        }
    }

    public static void dumpPoc(Object obj) {
        Field[] fields;
        StackTraceElement stackTraceElement = new Throwable().getStackTrace()[1];
        StringBuilder R = a.R(".(");
        R.append(stackTraceElement.getFileName());
        R.append(":");
        R.append(stackTraceElement.getLineNumber());
        R.append(")");
        String sb = R.toString();
        Class<?> cls = obj.getClass();
        PrintStream printStream = System.out;
        StringBuilder V = a.V(sb, "------------- ");
        V.append(cls.getName());
        V.append(" --------------------");
        printStream.println(V.toString());
        for (Field field : cls.getFields()) {
            try {
                Object obj2 = field.get(obj);
                if (field.getName().startsWith("layout_constraint") && ((!(obj2 instanceof Integer) || !obj2.toString().equals("-1")) && ((!(obj2 instanceof Integer) || !obj2.toString().equals("0")) && ((!(obj2 instanceof Float) || !obj2.toString().equals(BuildConfig.VERSION_NAME)) && (!(obj2 instanceof Float) || !obj2.toString().equals("0.5")))))) {
                    System.out.println(sb + "    " + field.getName() + " " + obj2);
                }
            } catch (IllegalAccessException unused) {
            }
        }
        PrintStream printStream2 = System.out;
        StringBuilder V2 = a.V(sb, "------------- ");
        V2.append(cls.getSimpleName());
        V2.append(" --------------------");
        printStream2.println(V2.toString());
    }

    public static String getActionType(MotionEvent motionEvent) {
        Field[] fields;
        int action = motionEvent.getAction();
        for (Field field : MotionEvent.class.getFields()) {
            try {
                if (Modifier.isStatic(field.getModifiers()) && field.getType().equals(Integer.TYPE) && field.getInt(null) == action) {
                    return field.getName();
                }
            } catch (IllegalAccessException unused) {
            }
        }
        return "---";
    }

    public static String getCallFrom(int i) {
        StackTraceElement stackTraceElement = new Throwable().getStackTrace()[i + 2];
        StringBuilder R = a.R(".(");
        R.append(stackTraceElement.getFileName());
        R.append(":");
        R.append(stackTraceElement.getLineNumber());
        R.append(")");
        return R.toString();
    }

    public static String getLoc() {
        StackTraceElement stackTraceElement = new Throwable().getStackTrace()[1];
        StringBuilder R = a.R(".(");
        R.append(stackTraceElement.getFileName());
        R.append(":");
        R.append(stackTraceElement.getLineNumber());
        R.append(") ");
        R.append(stackTraceElement.getMethodName());
        R.append("()");
        return R.toString();
    }

    public static String getLocation() {
        StackTraceElement stackTraceElement = new Throwable().getStackTrace()[1];
        StringBuilder R = a.R(".(");
        R.append(stackTraceElement.getFileName());
        R.append(":");
        R.append(stackTraceElement.getLineNumber());
        R.append(")");
        return R.toString();
    }

    public static String getLocation2() {
        StackTraceElement stackTraceElement = new Throwable().getStackTrace()[2];
        StringBuilder R = a.R(".(");
        R.append(stackTraceElement.getFileName());
        R.append(":");
        R.append(stackTraceElement.getLineNumber());
        R.append(")");
        return R.toString();
    }

    public static String getName(View view) {
        try {
            return view.getContext().getResources().getResourceEntryName(view.getId());
        } catch (Exception unused) {
            return "UNKNOWN";
        }
    }

    public static String getState(MotionLayout motionLayout, int i) {
        return i == -1 ? "UNDEFINED" : motionLayout.getContext().getResources().getResourceEntryName(i);
    }

    public static void logStack(String str, String str2, int i) {
        StackTraceElement[] stackTrace = new Throwable().getStackTrace();
        int min = Math.min(i, stackTrace.length - 1);
        String str3 = " ";
        for (int i2 = 1; i2 <= min; i2++) {
            StackTraceElement stackTraceElement = stackTrace[i2];
            StringBuilder R = a.R(".(");
            R.append(stackTrace[i2].getFileName());
            R.append(":");
            R.append(stackTrace[i2].getLineNumber());
            R.append(") ");
            R.append(stackTrace[i2].getMethodName());
            String sb = R.toString();
            str3 = a.v(str3, " ");
            Log.v(str, str2 + str3 + sb + str3);
        }
    }

    public static void printStack(String str, int i) {
        StackTraceElement[] stackTrace = new Throwable().getStackTrace();
        int min = Math.min(i, stackTrace.length - 1);
        String str2 = " ";
        for (int i2 = 1; i2 <= min; i2++) {
            StackTraceElement stackTraceElement = stackTrace[i2];
            StringBuilder R = a.R(".(");
            R.append(stackTrace[i2].getFileName());
            R.append(":");
            R.append(stackTrace[i2].getLineNumber());
            R.append(") ");
            String sb = R.toString();
            str2 = a.v(str2, " ");
            PrintStream printStream = System.out;
            printStream.println(str + str2 + sb + str2);
        }
    }

    public static String getName(Context context, int i) {
        if (i == -1) {
            return "UNKNOWN";
        }
        try {
            return context.getResources().getResourceEntryName(i);
        } catch (Exception unused) {
            return a.p("?", i);
        }
    }

    public static String getName(Context context, int[] iArr) {
        String str;
        try {
            String str2 = iArr.length + "[";
            int i = 0;
            while (i < iArr.length) {
                StringBuilder sb = new StringBuilder();
                sb.append(str2);
                sb.append(i == 0 ? "" : " ");
                String sb2 = sb.toString();
                try {
                    str = context.getResources().getResourceEntryName(iArr[i]);
                } catch (Resources.NotFoundException unused) {
                    str = "? " + iArr[i] + " ";
                }
                str2 = sb2 + str;
                i++;
            }
            return str2 + "]";
        } catch (Exception e) {
            Log.v("DEBUG", e.toString());
            return "UNKNOWN";
        }
    }

    public static void dumpLayoutParams(ViewGroup.LayoutParams layoutParams, String str) {
        Field[] fields;
        StackTraceElement stackTraceElement = new Throwable().getStackTrace()[1];
        StringBuilder R = a.R(".(");
        R.append(stackTraceElement.getFileName());
        R.append(":");
        R.append(stackTraceElement.getLineNumber());
        R.append(") ");
        R.append(str);
        R.append("  ");
        String sb = R.toString();
        PrintStream printStream = System.out;
        StringBuilder W = a.W(" >>>>>>>>>>>>>>>>>>. dump ", sb, "  ");
        W.append(layoutParams.getClass().getName());
        printStream.println(W.toString());
        for (Field field : layoutParams.getClass().getFields()) {
            try {
                Object obj = field.get(layoutParams);
                String name = field.getName();
                if (name.contains("To") && !obj.toString().equals("-1")) {
                    System.out.println(sb + "       " + name + " " + obj);
                }
            } catch (IllegalAccessException unused) {
            }
        }
        System.out.println(" <<<<<<<<<<<<<<<<< dump " + sb);
    }
}
