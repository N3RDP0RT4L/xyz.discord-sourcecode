package androidx.room;

import andhook.lib.xposed.ClassUtils;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.room.RoomDatabase;
import b.d.b.a.a;
/* loaded from: classes.dex */
public class Room {
    private static final String CURSOR_CONV_SUFFIX = "_CursorConverter";
    public static final String LOG_TAG = "ROOM";
    public static final String MASTER_TABLE_NAME = "room_master_table";

    @NonNull
    public static <T extends RoomDatabase> RoomDatabase.Builder<T> databaseBuilder(@NonNull Context context, @NonNull Class<T> cls, @NonNull String str) {
        if (str != null && str.trim().length() != 0) {
            return new RoomDatabase.Builder<>(context, cls, str);
        }
        throw new IllegalArgumentException("Cannot build a database with null or empty name. If you are trying to create an in memory database, use Room.inMemoryDatabaseBuilder");
    }

    @NonNull
    public static <T, C> T getGeneratedImplementation(Class<C> cls, String str) {
        String str2;
        String name = cls.getPackage().getName();
        String canonicalName = cls.getCanonicalName();
        if (!name.isEmpty()) {
            canonicalName = canonicalName.substring(name.length() + 1);
        }
        String str3 = canonicalName.replace(ClassUtils.PACKAGE_SEPARATOR_CHAR, '_') + str;
        try {
            if (name.isEmpty()) {
                str2 = str3;
            } else {
                str2 = name + "." + str3;
            }
            return (T) Class.forName(str2).newInstance();
        } catch (ClassNotFoundException unused) {
            StringBuilder R = a.R("cannot find implementation for ");
            R.append(cls.getCanonicalName());
            R.append(". ");
            R.append(str3);
            R.append(" does not exist");
            throw new RuntimeException(R.toString());
        } catch (IllegalAccessException unused2) {
            StringBuilder R2 = a.R("Cannot access the constructor");
            R2.append(cls.getCanonicalName());
            throw new RuntimeException(R2.toString());
        } catch (InstantiationException unused3) {
            StringBuilder R3 = a.R("Failed to create an instance of ");
            R3.append(cls.getCanonicalName());
            throw new RuntimeException(R3.toString());
        }
    }

    @NonNull
    public static <T extends RoomDatabase> RoomDatabase.Builder<T> inMemoryDatabaseBuilder(@NonNull Context context, @NonNull Class<T> cls) {
        return new RoomDatabase.Builder<>(context, cls, null);
    }
}
