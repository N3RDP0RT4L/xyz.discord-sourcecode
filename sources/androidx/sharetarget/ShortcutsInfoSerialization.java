package androidx.sharetarget;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import androidx.annotation.AnyThread;
import androidx.annotation.WorkerThread;
import androidx.collection.ArrayMap;
import androidx.core.content.pm.ShortcutInfoCompat;
import androidx.core.util.AtomicFile;
import b.d.b.a.a;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;
/* loaded from: classes.dex */
public class ShortcutsInfoSerialization {
    private static final String ATTR_ACTION = "action";
    private static final String ATTR_COMPONENT = "component";
    private static final String ATTR_DISABLED_MSG = "disabled_message";
    private static final String ATTR_ICON_BMP_PATH = "icon_bitmap_path";
    private static final String ATTR_ICON_RES_NAME = "icon_resource_name";
    private static final String ATTR_ID = "id";
    private static final String ATTR_LONG_LABEL = "long_label";
    private static final String ATTR_NAME = "name";
    private static final String ATTR_RANK = "rank";
    private static final String ATTR_SHORT_LABEL = "short_label";
    private static final String ATTR_TARGET_CLASS = "targetClass";
    private static final String ATTR_TARGET_PACKAGE = "targetPackage";
    private static final String TAG = "ShortcutInfoCompatSaver";
    private static final String TAG_CATEGORY = "categories";
    private static final String TAG_INTENT = "intent";
    private static final String TAG_ROOT = "share_targets";
    private static final String TAG_TARGET = "target";

    /* loaded from: classes.dex */
    public static class ShortcutContainer {
        public final String mBitmapPath;
        public final String mResourceName;
        public final ShortcutInfoCompat mShortcutInfo;

        @AnyThread
        public ShortcutContainer(ShortcutInfoCompat shortcutInfoCompat, String str, String str2) {
            this.mShortcutInfo = shortcutInfoCompat;
            this.mResourceName = str;
            this.mBitmapPath = str2;
        }
    }

    private ShortcutsInfoSerialization() {
    }

    @WorkerThread
    private static String getAttributeValue(XmlPullParser xmlPullParser, String str) {
        String attributeValue = xmlPullParser.getAttributeValue("http://schemas.android.com/apk/res/android", str);
        return attributeValue == null ? xmlPullParser.getAttributeValue(null, str) : attributeValue;
    }

    @WorkerThread
    public static Map<String, ShortcutContainer> loadFromXml(File file, Context context) {
        FileInputStream fileInputStream;
        ShortcutContainer parseShortcutContainer;
        ShortcutInfoCompat shortcutInfoCompat;
        ArrayMap arrayMap = new ArrayMap();
        try {
            fileInputStream = new FileInputStream(file);
        } catch (Exception e) {
            file.delete();
            Log.e("ShortcutInfoCompatSaver", "Failed to load saved values from file " + file.getAbsolutePath() + ". Old state removed, new added", e);
        }
        if (!file.exists()) {
            fileInputStream.close();
            return arrayMap;
        }
        XmlPullParser newPullParser = Xml.newPullParser();
        newPullParser.setInput(fileInputStream, "UTF_8");
        while (true) {
            int next = newPullParser.next();
            if (next == 1) {
                break;
            } else if (next == 2 && newPullParser.getName().equals(TAG_TARGET) && (parseShortcutContainer = parseShortcutContainer(newPullParser, context)) != null && (shortcutInfoCompat = parseShortcutContainer.mShortcutInfo) != null) {
                arrayMap.put(shortcutInfoCompat.getId(), parseShortcutContainer);
            }
        }
        fileInputStream.close();
        return arrayMap;
    }

    @WorkerThread
    private static ComponentName parseComponentName(XmlPullParser xmlPullParser) {
        String attributeValue = getAttributeValue(xmlPullParser, ATTR_COMPONENT);
        if (TextUtils.isEmpty(attributeValue)) {
            return null;
        }
        return ComponentName.unflattenFromString(attributeValue);
    }

    @WorkerThread
    private static Intent parseIntent(XmlPullParser xmlPullParser) {
        String attributeValue = getAttributeValue(xmlPullParser, ATTR_ACTION);
        String attributeValue2 = getAttributeValue(xmlPullParser, ATTR_TARGET_PACKAGE);
        String attributeValue3 = getAttributeValue(xmlPullParser, ATTR_TARGET_CLASS);
        if (attributeValue == null) {
            return null;
        }
        Intent intent = new Intent(attributeValue);
        if (!TextUtils.isEmpty(attributeValue2) && !TextUtils.isEmpty(attributeValue3)) {
            intent.setClassName(attributeValue2, attributeValue3);
        }
        return intent;
    }

    @WorkerThread
    private static ShortcutContainer parseShortcutContainer(XmlPullParser xmlPullParser, Context context) throws Exception {
        if (!xmlPullParser.getName().equals(TAG_TARGET)) {
            return null;
        }
        String attributeValue = getAttributeValue(xmlPullParser, "id");
        String attributeValue2 = getAttributeValue(xmlPullParser, ATTR_SHORT_LABEL);
        if (TextUtils.isEmpty(attributeValue) || TextUtils.isEmpty(attributeValue2)) {
            return null;
        }
        int parseInt = Integer.parseInt(getAttributeValue(xmlPullParser, ATTR_RANK));
        String attributeValue3 = getAttributeValue(xmlPullParser, ATTR_LONG_LABEL);
        String attributeValue4 = getAttributeValue(xmlPullParser, ATTR_DISABLED_MSG);
        ComponentName parseComponentName = parseComponentName(xmlPullParser);
        String attributeValue5 = getAttributeValue(xmlPullParser, ATTR_ICON_RES_NAME);
        String attributeValue6 = getAttributeValue(xmlPullParser, ATTR_ICON_BMP_PATH);
        ArrayList arrayList = new ArrayList();
        HashSet hashSet = new HashSet();
        while (true) {
            int next = xmlPullParser.next();
            if (next != 1) {
                if (next != 2) {
                    if (next == 3 && xmlPullParser.getName().equals(TAG_TARGET)) {
                        break;
                    }
                } else {
                    String name = xmlPullParser.getName();
                    name.hashCode();
                    if (name.equals(TAG_INTENT)) {
                        Intent parseIntent = parseIntent(xmlPullParser);
                        if (parseIntent != null) {
                            arrayList.add(parseIntent);
                        }
                    } else if (name.equals(TAG_CATEGORY)) {
                        String attributeValue7 = getAttributeValue(xmlPullParser, "name");
                        if (!TextUtils.isEmpty(attributeValue7)) {
                            hashSet.add(attributeValue7);
                        }
                    }
                }
            } else {
                break;
            }
        }
        ShortcutInfoCompat.Builder rank = new ShortcutInfoCompat.Builder(context, attributeValue).setShortLabel(attributeValue2).setRank(parseInt);
        if (!TextUtils.isEmpty(attributeValue3)) {
            rank.setLongLabel(attributeValue3);
        }
        if (!TextUtils.isEmpty(attributeValue4)) {
            rank.setDisabledMessage(attributeValue4);
        }
        if (parseComponentName != null) {
            rank.setActivity(parseComponentName);
        }
        if (!arrayList.isEmpty()) {
            rank.setIntents((Intent[]) arrayList.toArray(new Intent[0]));
        }
        if (!hashSet.isEmpty()) {
            rank.setCategories(hashSet);
        }
        return new ShortcutContainer(rank.build(), attributeValue5, attributeValue6);
    }

    public static void saveAsXml(List<ShortcutContainer> list, File file) {
        Exception e;
        FileOutputStream startWrite;
        AtomicFile atomicFile = new AtomicFile(file);
        FileOutputStream fileOutputStream = null;
        try {
            startWrite = atomicFile.startWrite();
        } catch (Exception e2) {
            e = e2;
        }
        try {
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(startWrite);
            XmlSerializer newSerializer = Xml.newSerializer();
            newSerializer.setOutput(bufferedOutputStream, "UTF_8");
            newSerializer.startDocument(null, Boolean.TRUE);
            newSerializer.startTag(null, TAG_ROOT);
            for (ShortcutContainer shortcutContainer : list) {
                serializeShortcutContainer(newSerializer, shortcutContainer);
            }
            newSerializer.endTag(null, TAG_ROOT);
            newSerializer.endDocument();
            bufferedOutputStream.flush();
            startWrite.flush();
            atomicFile.finishWrite(startWrite);
        } catch (Exception e3) {
            e = e3;
            fileOutputStream = startWrite;
            StringBuilder R = a.R("Failed to write to file ");
            R.append(atomicFile.getBaseFile());
            Log.e("ShortcutInfoCompatSaver", R.toString(), e);
            atomicFile.failWrite(fileOutputStream);
            StringBuilder R2 = a.R("Failed to write to file ");
            R2.append(atomicFile.getBaseFile());
            throw new RuntimeException(R2.toString(), e);
        }
    }

    @WorkerThread
    private static void serializeAttribute(XmlSerializer xmlSerializer, String str, String str2) throws IOException {
        if (!TextUtils.isEmpty(str2)) {
            xmlSerializer.attribute(null, str, str2);
        }
    }

    @WorkerThread
    private static void serializeCategory(XmlSerializer xmlSerializer, String str) throws IOException {
        if (!TextUtils.isEmpty(str)) {
            xmlSerializer.startTag(null, TAG_CATEGORY);
            serializeAttribute(xmlSerializer, "name", str);
            xmlSerializer.endTag(null, TAG_CATEGORY);
        }
    }

    @WorkerThread
    private static void serializeIntent(XmlSerializer xmlSerializer, Intent intent) throws IOException {
        xmlSerializer.startTag(null, TAG_INTENT);
        serializeAttribute(xmlSerializer, ATTR_ACTION, intent.getAction());
        if (intent.getComponent() != null) {
            serializeAttribute(xmlSerializer, ATTR_TARGET_PACKAGE, intent.getComponent().getPackageName());
            serializeAttribute(xmlSerializer, ATTR_TARGET_CLASS, intent.getComponent().getClassName());
        }
        xmlSerializer.endTag(null, TAG_INTENT);
    }

    @WorkerThread
    private static void serializeShortcutContainer(XmlSerializer xmlSerializer, ShortcutContainer shortcutContainer) throws IOException {
        xmlSerializer.startTag(null, TAG_TARGET);
        ShortcutInfoCompat shortcutInfoCompat = shortcutContainer.mShortcutInfo;
        serializeAttribute(xmlSerializer, "id", shortcutInfoCompat.getId());
        serializeAttribute(xmlSerializer, ATTR_SHORT_LABEL, shortcutInfoCompat.getShortLabel().toString());
        serializeAttribute(xmlSerializer, ATTR_RANK, Integer.toString(shortcutInfoCompat.getRank()));
        if (!TextUtils.isEmpty(shortcutInfoCompat.getLongLabel())) {
            serializeAttribute(xmlSerializer, ATTR_LONG_LABEL, shortcutInfoCompat.getLongLabel().toString());
        }
        if (!TextUtils.isEmpty(shortcutInfoCompat.getDisabledMessage())) {
            serializeAttribute(xmlSerializer, ATTR_DISABLED_MSG, shortcutInfoCompat.getDisabledMessage().toString());
        }
        if (shortcutInfoCompat.getActivity() != null) {
            serializeAttribute(xmlSerializer, ATTR_COMPONENT, shortcutInfoCompat.getActivity().flattenToString());
        }
        if (!TextUtils.isEmpty(shortcutContainer.mResourceName)) {
            serializeAttribute(xmlSerializer, ATTR_ICON_RES_NAME, shortcutContainer.mResourceName);
        }
        if (!TextUtils.isEmpty(shortcutContainer.mBitmapPath)) {
            serializeAttribute(xmlSerializer, ATTR_ICON_BMP_PATH, shortcutContainer.mBitmapPath);
        }
        for (Intent intent : shortcutInfoCompat.getIntents()) {
            serializeIntent(xmlSerializer, intent);
        }
        for (String str : shortcutInfoCompat.getCategories()) {
            serializeCategory(xmlSerializer, str);
        }
        xmlSerializer.endTag(null, TAG_TARGET);
    }
}
