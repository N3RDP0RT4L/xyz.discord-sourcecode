package androidx.work;

import b.i.b.d.a.a;
import d0.k;
import d0.l;
import java.util.concurrent.CancellationException;
import kotlin.Metadata;
import kotlinx.coroutines.CancellableContinuation;
/* compiled from: ListenableFuture.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\u0010\u0005\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"R", "", "run", "()V", "androidx/work/ListenableFutureKt$await$2$1", "<anonymous>"}, k = 3, mv = {1, 4, 0})
/* loaded from: classes.dex */
public final class ListenableFutureKt$await$$inlined$suspendCancellableCoroutine$lambda$2 implements Runnable {
    public final /* synthetic */ CancellableContinuation $cancellableContinuation;
    public final /* synthetic */ a $this_await$inlined;

    public ListenableFutureKt$await$$inlined$suspendCancellableCoroutine$lambda$2(CancellableContinuation cancellableContinuation, a aVar) {
        this.$cancellableContinuation = cancellableContinuation;
        this.$this_await$inlined = aVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        try {
            CancellableContinuation cancellableContinuation = this.$cancellableContinuation;
            V v = this.$this_await$inlined.get();
            k.a aVar = k.j;
            cancellableContinuation.resumeWith(k.m73constructorimpl(v));
        } catch (Throwable th) {
            Throwable cause = th.getCause();
            if (cause == null) {
                cause = th;
            }
            if (th instanceof CancellationException) {
                this.$cancellableContinuation.k(cause);
                return;
            }
            CancellableContinuation cancellableContinuation2 = this.$cancellableContinuation;
            k.a aVar2 = k.j;
            cancellableContinuation2.resumeWith(k.m73constructorimpl(l.createFailure(cause)));
        }
    }
}
