package androidx.work;

import android.content.Context;
import androidx.annotation.NonNull;
import b.i.b.d.a.a;
import java.util.UUID;
/* loaded from: classes.dex */
public interface ForegroundUpdater {
    @NonNull
    a<Void> setForegroundAsync(@NonNull Context context, @NonNull UUID uuid, @NonNull ForegroundInfo foregroundInfo);
}
