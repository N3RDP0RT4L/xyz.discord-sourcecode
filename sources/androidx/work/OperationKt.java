package androidx.work;

import androidx.work.Operation;
import b.i.b.d.a.a;
import d0.w.h.b;
import d0.w.h.c;
import d0.w.i.a.g;
import d0.z.d.m;
import java.util.concurrent.ExecutionException;
import kotlin.Metadata;
import kotlin.coroutines.Continuation;
import s.a.l;
/* compiled from: Operation.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u001a\u001f\u0010\u0003\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001*\u00020\u0000H\u0086Hø\u0001\u0000¢\u0006\u0004\b\u0003\u0010\u0004\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0005"}, d2 = {"Landroidx/work/Operation;", "Landroidx/work/Operation$State$SUCCESS;", "kotlin.jvm.PlatformType", "await", "(Landroidx/work/Operation;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "work-runtime-ktx_release"}, k = 2, mv = {1, 4, 0})
/* loaded from: classes.dex */
public final class OperationKt {
    public static final Object await(Operation operation, Continuation<? super Operation.State.SUCCESS> continuation) {
        a<Operation.State.SUCCESS> result = operation.getResult();
        m.checkExpressionValueIsNotNull(result, "result");
        if (result.isDone()) {
            try {
                return result.get();
            } catch (ExecutionException e) {
                Throwable cause = e.getCause();
                if (cause == null) {
                    throw e;
                }
                throw cause;
            }
        } else {
            l lVar = new l(b.intercepted(continuation), 1);
            result.addListener(new OperationKt$await$$inlined$suspendCancellableCoroutine$lambda$1(lVar, result), DirectExecutor.INSTANCE);
            Object u = lVar.u();
            if (u != c.getCOROUTINE_SUSPENDED()) {
                return u;
            }
            g.probeCoroutineSuspended(continuation);
            return u;
        }
    }

    private static final Object await$$forInline(Operation operation, Continuation continuation) {
        a<Operation.State.SUCCESS> result = operation.getResult();
        m.checkExpressionValueIsNotNull(result, "result");
        if (result.isDone()) {
            try {
                return result.get();
            } catch (ExecutionException e) {
                Throwable cause = e.getCause();
                if (cause == null) {
                    throw e;
                }
                throw cause;
            }
        } else {
            d0.z.d.l.mark(0);
            l lVar = new l(b.intercepted(continuation), 1);
            result.addListener(new OperationKt$await$$inlined$suspendCancellableCoroutine$lambda$1(lVar, result), DirectExecutor.INSTANCE);
            Object u = lVar.u();
            if (u == c.getCOROUTINE_SUSPENDED()) {
                g.probeCoroutineSuspended(continuation);
            }
            d0.z.d.l.mark(1);
            return u;
        }
    }
}
