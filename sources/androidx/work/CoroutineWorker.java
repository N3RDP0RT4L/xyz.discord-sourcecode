package androidx.work;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.work.ListenableWorker;
import androidx.work.impl.utils.futures.SettableFuture;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import b.i.a.f.e.o.f;
import b.i.b.d.a.a;
import d0.k;
import d0.w.h.b;
import d0.w.h.c;
import d0.w.i.a.g;
import d0.z.d.m;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlinx.coroutines.CancellableContinuation;
import kotlinx.coroutines.CoroutineDispatcher;
import s.a.f1;
import s.a.k0;
import s.a.l;
import s.a.u;
/* compiled from: CoroutineWorker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b&\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010$\u001a\u00020#\u0012\u0006\u0010&\u001a\u00020%¢\u0006\u0004\b'\u0010(J\u0013\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0013\u0010\u0006\u001a\u00020\u0003H¦@ø\u0001\u0000¢\u0006\u0004\b\u0006\u0010\u0007J\u001b\u0010\u000b\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\bH\u0086@ø\u0001\u0000¢\u0006\u0004\b\u000b\u0010\fJ\u001b\u0010\u000f\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\rH\u0086@ø\u0001\u0000¢\u0006\u0004\b\u000f\u0010\u0010J\r\u0010\u0011\u001a\u00020\n¢\u0006\u0004\b\u0011\u0010\u0012R\"\u0010\u0014\u001a\u00020\u00138\u0016@\u0017X\u0097\u0004¢\u0006\u0012\n\u0004\b\u0014\u0010\u0015\u0012\u0004\b\u0018\u0010\u0012\u001a\u0004\b\u0016\u0010\u0017R\u001c\u0010\u001a\u001a\u00020\u00198\u0000@\u0000X\u0080\u0004¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u001dR\"\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00030\u001e8\u0000@\u0000X\u0080\u0004¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006)"}, d2 = {"Landroidx/work/CoroutineWorker;", "Landroidx/work/ListenableWorker;", "Lb/i/b/d/a/a;", "Landroidx/work/ListenableWorker$Result;", "startWork", "()Lb/i/b/d/a/a;", "doWork", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "Landroidx/work/Data;", "data", "", "setProgress", "(Landroidx/work/Data;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "Landroidx/work/ForegroundInfo;", "foregroundInfo", "setForeground", "(Landroidx/work/ForegroundInfo;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "onStopped", "()V", "Lkotlinx/coroutines/CoroutineDispatcher;", "coroutineContext", "Lkotlinx/coroutines/CoroutineDispatcher;", "getCoroutineContext", "()Lkotlinx/coroutines/CoroutineDispatcher;", "coroutineContext$annotations", "Ls/a/u;", "job", "Ls/a/u;", "getJob$work_runtime_ktx_release", "()Ls/a/u;", "Landroidx/work/impl/utils/futures/SettableFuture;", "future", "Landroidx/work/impl/utils/futures/SettableFuture;", "getFuture$work_runtime_ktx_release", "()Landroidx/work/impl/utils/futures/SettableFuture;", "Landroid/content/Context;", "appContext", "Landroidx/work/WorkerParameters;", "params", HookHelper.constructorName, "(Landroid/content/Context;Landroidx/work/WorkerParameters;)V", "work-runtime-ktx_release"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes.dex */
public abstract class CoroutineWorker extends ListenableWorker {
    private final SettableFuture<ListenableWorker.Result> future;
    private final u job = new f1(null);
    private final CoroutineDispatcher coroutineContext = k0.a;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CoroutineWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
        m.checkParameterIsNotNull(context, "appContext");
        m.checkParameterIsNotNull(workerParameters, "params");
        SettableFuture<ListenableWorker.Result> create = SettableFuture.create();
        m.checkExpressionValueIsNotNull(create, "SettableFuture.create()");
        this.future = create;
        Runnable runnable = new Runnable() { // from class: androidx.work.CoroutineWorker.1
            @Override // java.lang.Runnable
            public final void run() {
                if (CoroutineWorker.this.getFuture$work_runtime_ktx_release().isCancelled()) {
                    f.t(CoroutineWorker.this.getJob$work_runtime_ktx_release(), null, 1, null);
                }
            }
        };
        TaskExecutor taskExecutor = getTaskExecutor();
        m.checkExpressionValueIsNotNull(taskExecutor, "taskExecutor");
        create.addListener(runnable, taskExecutor.getBackgroundExecutor());
    }

    public static /* synthetic */ void coroutineContext$annotations() {
    }

    public abstract Object doWork(Continuation<? super ListenableWorker.Result> continuation);

    public CoroutineDispatcher getCoroutineContext() {
        return this.coroutineContext;
    }

    public final SettableFuture<ListenableWorker.Result> getFuture$work_runtime_ktx_release() {
        return this.future;
    }

    public final u getJob$work_runtime_ktx_release() {
        return this.job;
    }

    @Override // androidx.work.ListenableWorker
    public final void onStopped() {
        super.onStopped();
        this.future.cancel(false);
    }

    public final Object setForeground(ForegroundInfo foregroundInfo, Continuation<? super Unit> continuation) {
        Object obj;
        final a<Void> foregroundAsync = setForegroundAsync(foregroundInfo);
        m.checkExpressionValueIsNotNull(foregroundAsync, "setForegroundAsync(foregroundInfo)");
        if (foregroundAsync.isDone()) {
            try {
                obj = foregroundAsync.get();
            } catch (ExecutionException e) {
                Throwable cause = e.getCause();
                if (cause == null) {
                    throw e;
                }
                throw cause;
            }
        } else {
            final l lVar = new l(b.intercepted(continuation), 1);
            foregroundAsync.addListener(new Runnable() { // from class: androidx.work.CoroutineWorker$await$$inlined$suspendCancellableCoroutine$lambda$2
                @Override // java.lang.Runnable
                public final void run() {
                    try {
                        CancellableContinuation cancellableContinuation = CancellableContinuation.this;
                        V v = foregroundAsync.get();
                        k.a aVar = k.j;
                        cancellableContinuation.resumeWith(k.m73constructorimpl(v));
                    } catch (Throwable th) {
                        Throwable cause2 = th.getCause();
                        if (cause2 == null) {
                            cause2 = th;
                        }
                        if (th instanceof CancellationException) {
                            CancellableContinuation.this.k(cause2);
                            return;
                        }
                        CancellableContinuation cancellableContinuation2 = CancellableContinuation.this;
                        k.a aVar2 = k.j;
                        cancellableContinuation2.resumeWith(k.m73constructorimpl(d0.l.createFailure(cause2)));
                    }
                }
            }, DirectExecutor.INSTANCE);
            obj = lVar.u();
            if (obj == c.getCOROUTINE_SUSPENDED()) {
                g.probeCoroutineSuspended(continuation);
            }
        }
        return obj == c.getCOROUTINE_SUSPENDED() ? obj : Unit.a;
    }

    public final Object setProgress(Data data, Continuation<? super Unit> continuation) {
        Object obj;
        final a<Void> progressAsync = setProgressAsync(data);
        m.checkExpressionValueIsNotNull(progressAsync, "setProgressAsync(data)");
        if (progressAsync.isDone()) {
            try {
                obj = progressAsync.get();
            } catch (ExecutionException e) {
                Throwable cause = e.getCause();
                if (cause == null) {
                    throw e;
                }
                throw cause;
            }
        } else {
            final l lVar = new l(b.intercepted(continuation), 1);
            progressAsync.addListener(new Runnable() { // from class: androidx.work.CoroutineWorker$await$$inlined$suspendCancellableCoroutine$lambda$1
                @Override // java.lang.Runnable
                public final void run() {
                    try {
                        CancellableContinuation cancellableContinuation = CancellableContinuation.this;
                        V v = progressAsync.get();
                        k.a aVar = k.j;
                        cancellableContinuation.resumeWith(k.m73constructorimpl(v));
                    } catch (Throwable th) {
                        Throwable cause2 = th.getCause();
                        if (cause2 == null) {
                            cause2 = th;
                        }
                        if (th instanceof CancellationException) {
                            CancellableContinuation.this.k(cause2);
                            return;
                        }
                        CancellableContinuation cancellableContinuation2 = CancellableContinuation.this;
                        k.a aVar2 = k.j;
                        cancellableContinuation2.resumeWith(k.m73constructorimpl(d0.l.createFailure(cause2)));
                    }
                }
            }, DirectExecutor.INSTANCE);
            obj = lVar.u();
            if (obj == c.getCOROUTINE_SUSPENDED()) {
                g.probeCoroutineSuspended(continuation);
            }
        }
        return obj == c.getCOROUTINE_SUSPENDED() ? obj : Unit.a;
    }

    @Override // androidx.work.ListenableWorker
    public final a<ListenableWorker.Result> startWork() {
        f.H0(f.c(getCoroutineContext().plus(this.job)), null, null, new CoroutineWorker$startWork$1(this, null), 3, null);
        return this.future;
    }
}
