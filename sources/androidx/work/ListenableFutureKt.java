package androidx.work;

import androidx.annotation.RestrictTo;
import b.i.b.d.a.a;
import d0.w.h.b;
import d0.w.h.c;
import d0.w.i.a.g;
import java.util.concurrent.ExecutionException;
import kotlin.Metadata;
import kotlin.coroutines.Continuation;
import s.a.l;
/* compiled from: ListenableFuture.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a#\u0010\u0002\u001a\u00028\u0000\"\u0004\b\u0000\u0010\u0000*\b\u0012\u0004\u0012\u00028\u00000\u0001H\u0087Hø\u0001\u0000¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0004"}, d2 = {"R", "Lb/i/b/d/a/a;", "await", "(Lb/i/b/d/a/a;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "work-runtime-ktx_release"}, k = 2, mv = {1, 4, 0})
/* loaded from: classes.dex */
public final class ListenableFutureKt {
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    public static final <R> Object await(a<R> aVar, Continuation<? super R> continuation) {
        if (aVar.isDone()) {
            try {
                return aVar.get();
            } catch (ExecutionException e) {
                Throwable cause = e.getCause();
                if (cause == null) {
                    throw e;
                }
                throw cause;
            }
        } else {
            l lVar = new l(b.intercepted(continuation), 1);
            aVar.addListener(new ListenableFutureKt$await$$inlined$suspendCancellableCoroutine$lambda$1(lVar, aVar), DirectExecutor.INSTANCE);
            Object u = lVar.u();
            if (u == c.getCOROUTINE_SUSPENDED()) {
                g.probeCoroutineSuspended(continuation);
            }
            return u;
        }
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    private static final Object await$$forInline(a aVar, Continuation continuation) {
        if (aVar.isDone()) {
            try {
                return aVar.get();
            } catch (ExecutionException e) {
                Throwable cause = e.getCause();
                if (cause == null) {
                    throw e;
                }
                throw cause;
            }
        } else {
            d0.z.d.l.mark(0);
            l lVar = new l(b.intercepted(continuation), 1);
            aVar.addListener(new ListenableFutureKt$await$$inlined$suspendCancellableCoroutine$lambda$1(lVar, aVar), DirectExecutor.INSTANCE);
            Object u = lVar.u();
            if (u == c.getCOROUTINE_SUSPENDED()) {
                g.probeCoroutineSuspended(continuation);
            }
            d0.z.d.l.mark(1);
            return u;
        }
    }
}
