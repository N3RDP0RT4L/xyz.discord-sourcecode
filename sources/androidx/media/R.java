package androidx.media;
/* loaded from: classes.dex */
public final class R {

    /* loaded from: classes.dex */
    public static final class color {
        public static final int notification_material_background_media_default_color = 0x7f0601c7;
        public static final int primary_text_default_material_dark = 0x7f06024f;
        public static final int secondary_text_default_material_dark = 0x7f06025a;

        private color() {
        }
    }

    /* loaded from: classes.dex */
    public static final class id {
        public static final int action0 = 0x7f0a004a;
        public static final int action_divider = 0x7f0a005c;
        public static final int cancel_action = 0x7f0a01e9;
        public static final int chronometer = 0x7f0a03a3;
        public static final int end_padder = 0x7f0a059a;
        public static final int icon = 0x7f0a0872;
        public static final int info = 0x7f0a08a9;
        public static final int line1 = 0x7f0a0958;
        public static final int line3 = 0x7f0a0959;
        public static final int media_actions = 0x7f0a0998;
        public static final int media_controller_compat_view_tag = 0x7f0a099a;
        public static final int notification_main_column = 0x7f0a0a8b;
        public static final int notification_main_column_container = 0x7f0a0a8c;
        public static final int right_side = 0x7f0a0c18;
        public static final int status_bar_latest_event_content = 0x7f0a0e92;
        public static final int text = 0x7f0a0f30;
        public static final int text2 = 0x7f0a0f32;
        public static final int time = 0x7f0a0f90;
        public static final int title = 0x7f0a0f97;

        private id() {
        }
    }

    /* loaded from: classes.dex */
    public static final class integer {
        public static final int cancel_button_image_alpha = 0x7f0b0007;

        private integer() {
        }
    }

    /* loaded from: classes.dex */
    public static final class layout {
        public static final int notification_media_action = 0x7f0d00dc;
        public static final int notification_media_cancel_action = 0x7f0d00dd;
        public static final int notification_template_big_media = 0x7f0d00df;
        public static final int notification_template_big_media_custom = 0x7f0d00e0;
        public static final int notification_template_big_media_narrow = 0x7f0d00e1;
        public static final int notification_template_big_media_narrow_custom = 0x7f0d00e2;
        public static final int notification_template_lines_media = 0x7f0d00e5;
        public static final int notification_template_media = 0x7f0d00e6;
        public static final int notification_template_media_custom = 0x7f0d00e7;

        private layout() {
        }
    }

    /* loaded from: classes.dex */
    public static final class style {
        public static final int TextAppearance_Compat_Notification_Info_Media = 0x7f130252;
        public static final int TextAppearance_Compat_Notification_Line2_Media = 0x7f130254;
        public static final int TextAppearance_Compat_Notification_Media = 0x7f130255;
        public static final int TextAppearance_Compat_Notification_Time_Media = 0x7f130257;
        public static final int TextAppearance_Compat_Notification_Title_Media = 0x7f130259;

        private style() {
        }
    }

    private R() {
    }
}
