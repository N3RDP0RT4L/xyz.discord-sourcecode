package d0.c0.f;

import d0.z.d.m;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
/* compiled from: PlatformThreadLocalRandom.kt */
/* loaded from: classes3.dex */
public final class a extends d0.c0.a {
    @Override // d0.c0.a
    public Random getImpl() {
        ThreadLocalRandom current = ThreadLocalRandom.current();
        m.checkNotNullExpressionValue(current, "ThreadLocalRandom.current()");
        return current;
    }

    @Override // d0.c0.c
    public int nextInt(int i, int i2) {
        return ThreadLocalRandom.current().nextInt(i, i2);
    }
}
