package d0.c0;

import d0.x.b;
import d0.z.d.m;
import java.io.Serializable;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: Random.kt */
/* loaded from: classes3.dex */
public abstract class c {
    public static final a k = new a(null);
    public static final c j = b.a.defaultPlatformRandom();

    /* compiled from: Random.kt */
    /* loaded from: classes3.dex */
    public static final class a extends c implements Serializable {

        /* compiled from: Random.kt */
        /* renamed from: d0.c0.c$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0271a implements Serializable {
            public static final C0271a j = new C0271a();
            private static final long serialVersionUID = 0;

            private final Object readResolve() {
                return c.k;
            }
        }

        public a() {
        }

        private final Object writeReplace() {
            return C0271a.j;
        }

        @Override // d0.c0.c
        public int nextBits(int i) {
            return c.j.nextBits(i);
        }

        @Override // d0.c0.c
        public byte[] nextBytes(byte[] bArr) {
            m.checkNotNullParameter(bArr, "array");
            return c.j.nextBytes(bArr);
        }

        @Override // d0.c0.c
        public int nextInt() {
            return c.j.nextInt();
        }

        @Override // d0.c0.c
        public long nextLong() {
            return c.j.nextLong();
        }

        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        @Override // d0.c0.c
        public byte[] nextBytes(int i) {
            return c.j.nextBytes(i);
        }

        @Override // d0.c0.c
        public int nextInt(int i) {
            return c.j.nextInt(i);
        }

        @Override // d0.c0.c
        public byte[] nextBytes(byte[] bArr, int i, int i2) {
            m.checkNotNullParameter(bArr, "array");
            return c.j.nextBytes(bArr, i, i2);
        }

        @Override // d0.c0.c
        public int nextInt(int i, int i2) {
            return c.j.nextInt(i, i2);
        }
    }

    public abstract int nextBits(int i);

    /* JADX WARN: Removed duplicated region for block: B:12:0x001a  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x0084  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public byte[] nextBytes(byte[] r7, int r8, int r9) {
        /*
            r6 = this;
            java.lang.String r0 = "array"
            d0.z.d.m.checkNotNullParameter(r7, r0)
            int r0 = r7.length
            r1 = 0
            r2 = 1
            if (r8 >= 0) goto Lb
            goto L15
        Lb:
            if (r0 < r8) goto L15
            int r0 = r7.length
            if (r9 >= 0) goto L11
            goto L15
        L11:
            if (r0 < r9) goto L15
            r0 = 1
            goto L16
        L15:
            r0 = 0
        L16:
            java.lang.String r3 = "fromIndex ("
            if (r0 == 0) goto L84
            if (r8 > r9) goto L1d
            goto L1e
        L1d:
            r2 = 0
        L1e:
            if (r2 == 0) goto L5e
            int r0 = r9 - r8
            int r0 = r0 / 4
            r2 = 0
        L25:
            if (r2 >= r0) goto L48
            int r3 = r6.nextInt()
            byte r4 = (byte) r3
            r7[r8] = r4
            int r4 = r8 + 1
            int r5 = r3 >>> 8
            byte r5 = (byte) r5
            r7[r4] = r5
            int r4 = r8 + 2
            int r5 = r3 >>> 16
            byte r5 = (byte) r5
            r7[r4] = r5
            int r4 = r8 + 3
            int r3 = r3 >>> 24
            byte r3 = (byte) r3
            r7[r4] = r3
            int r8 = r8 + 4
            int r2 = r2 + 1
            goto L25
        L48:
            int r9 = r9 - r8
            int r0 = r9 * 8
            int r0 = r6.nextBits(r0)
        L4f:
            if (r1 >= r9) goto L5d
            int r2 = r8 + r1
            int r3 = r1 * 8
            int r3 = r0 >>> r3
            byte r3 = (byte) r3
            r7[r2] = r3
            int r1 = r1 + 1
            goto L4f
        L5d:
            return r7
        L5e:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r7.append(r3)
            r7.append(r8)
            java.lang.String r8 = ") must be not greater than toIndex ("
            r7.append(r8)
            r7.append(r9)
            java.lang.String r8 = ")."
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            java.lang.IllegalArgumentException r8 = new java.lang.IllegalArgumentException
            java.lang.String r7 = r7.toString()
            r8.<init>(r7)
            throw r8
        L84:
            java.lang.String r0 = ") or toIndex ("
            java.lang.String r1 = ") are out of range: 0.."
            java.lang.StringBuilder r8 = b.d.b.a.a.U(r3, r8, r0, r9, r1)
            int r7 = r7.length
            r9 = 46
            java.lang.String r7 = b.d.b.a.a.z(r8, r7, r9)
            java.lang.IllegalArgumentException r8 = new java.lang.IllegalArgumentException
            java.lang.String r7 = r7.toString()
            r8.<init>(r7)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.c0.c.nextBytes(byte[], int, int):byte[]");
    }

    public abstract int nextInt();

    public int nextInt(int i) {
        return nextInt(0, i);
    }

    public long nextLong() {
        return (nextInt() << 32) + nextInt();
    }

    public int nextInt(int i, int i2) {
        int i3;
        int nextInt;
        int i4;
        d.checkRangeBounds(i, i2);
        int i5 = i2 - i;
        if (i5 > 0 || i5 == Integer.MIN_VALUE) {
            if (((-i5) & i5) == i5) {
                i3 = nextBits(d.fastLog2(i5));
            } else {
                do {
                    nextInt = nextInt() >>> 1;
                    i4 = nextInt % i5;
                } while ((i5 - 1) + (nextInt - i4) < 0);
                i3 = i4;
            }
            return i + i3;
        }
        while (true) {
            int nextInt2 = nextInt();
            if (i <= nextInt2 && i2 > nextInt2) {
                return nextInt2;
            }
        }
    }

    public byte[] nextBytes(byte[] bArr) {
        m.checkNotNullParameter(bArr, "array");
        return nextBytes(bArr, 0, bArr.length);
    }

    public byte[] nextBytes(int i) {
        return nextBytes(new byte[i]);
    }
}
