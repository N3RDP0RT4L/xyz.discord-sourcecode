package d0.f0;

import d0.z.d.m;
import java.util.Iterator;
import java.util.NoSuchElementException;
import kotlin.sequences.Sequence;
/* compiled from: Sequences.kt */
/* loaded from: classes3.dex */
public final class r<T> implements Sequence<T>, e<T> {
    public final Sequence<T> a;

    /* renamed from: b  reason: collision with root package name */
    public final int f3547b;
    public final int c;

    /* compiled from: Sequences.kt */
    /* loaded from: classes3.dex */
    public static final class a implements Iterator<T>, d0.z.d.g0.a {
        public final Iterator<T> j;
        public int k;

        public a() {
            this.j = r.this.a.iterator();
        }

        public final void a() {
            while (this.k < r.this.f3547b && this.j.hasNext()) {
                this.j.next();
                this.k++;
            }
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            a();
            return this.k < r.this.c && this.j.hasNext();
        }

        @Override // java.util.Iterator
        public T next() {
            a();
            if (this.k < r.this.c) {
                this.k++;
                return this.j.next();
            }
            throw new NoSuchElementException();
        }

        @Override // java.util.Iterator
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public r(Sequence<? extends T> sequence, int i, int i2) {
        m.checkNotNullParameter(sequence, "sequence");
        this.a = sequence;
        this.f3547b = i;
        this.c = i2;
        boolean z2 = true;
        if (i >= 0) {
            if (i2 >= 0) {
                if (!(i2 < i ? false : z2)) {
                    throw new IllegalArgumentException(b.d.b.a.a.r("endIndex should be not less than startIndex, but was ", i2, " < ", i).toString());
                }
                return;
            }
            throw new IllegalArgumentException(b.d.b.a.a.p("endIndex should be non-negative, but is ", i2).toString());
        }
        throw new IllegalArgumentException(b.d.b.a.a.p("startIndex should be non-negative, but is ", i).toString());
    }

    @Override // d0.f0.e
    public Sequence<T> drop(int i) {
        int i2 = this.c;
        int i3 = this.f3547b;
        return i >= i2 - i3 ? n.emptySequence() : new r(this.a, i3 + i, i2);
    }

    @Override // kotlin.sequences.Sequence
    public Iterator<T> iterator() {
        return new a();
    }

    @Override // d0.f0.e
    public Sequence<T> take(int i) {
        int i2 = this.c;
        int i3 = this.f3547b;
        return i >= i2 - i3 ? this : new r(this.a, i3, i + i3);
    }
}
