package d0.f0;

import d0.w.h.b;
import d0.z.d.m;
import java.util.Iterator;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlin.sequences.Sequence;
/* compiled from: SequenceBuilder.kt */
/* loaded from: classes3.dex */
public class l {

    /* compiled from: Sequences.kt */
    /* loaded from: classes3.dex */
    public static final class a implements Sequence<T> {
        public final /* synthetic */ Function2 a;

        public a(Function2 function2) {
            this.a = function2;
        }

        @Override // kotlin.sequences.Sequence
        public Iterator<T> iterator() {
            return l.iterator(this.a);
        }
    }

    public static final <T> Iterator<T> iterator(Function2<? super k<? super T>, ? super Continuation<? super Unit>, ? extends Object> function2) {
        m.checkNotNullParameter(function2, "block");
        j jVar = new j();
        jVar.setNextStep(b.createCoroutineUnintercepted(function2, jVar, jVar));
        return jVar;
    }

    public static final <T> Sequence<T> sequence(Function2<? super k<? super T>, ? super Continuation<? super Unit>, ? extends Object> function2) {
        m.checkNotNullParameter(function2, "block");
        return new a(function2);
    }
}
