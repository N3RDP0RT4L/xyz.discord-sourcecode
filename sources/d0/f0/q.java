package d0.f0;

import d0.g0.l;
import d0.t.n;
import d0.t.n0;
import d0.t.u;
import d0.z.d.k;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;
import kotlin.jvm.functions.Function1;
import kotlin.sequences.Sequence;
/* compiled from: _Sequences.kt */
/* loaded from: classes3.dex */
public class q extends p {

    /* compiled from: Iterables.kt */
    /* loaded from: classes3.dex */
    public static final class a implements Iterable<T>, d0.z.d.g0.a {
        public final /* synthetic */ Sequence j;

        public a(Sequence sequence) {
            this.j = sequence;
        }

        @Override // java.lang.Iterable
        public Iterator<T> iterator() {
            return this.j.iterator();
        }
    }

    /* compiled from: _Sequences.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function1<T, T> {
        public static final b j = new b();

        public b() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public final T invoke(T t) {
            return t;
        }
    }

    /* compiled from: _Sequences.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function1<T, Boolean> {
        public static final c j = new c();

        public c() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        /* renamed from: invoke */
        public final Boolean invoke2(T t) {
            return t == 0 ? 1 : null;
        }
    }

    /* compiled from: _Sequences.kt */
    /* loaded from: classes3.dex */
    public static final /* synthetic */ class d extends k implements Function1<Sequence<? extends R>, Iterator<? extends R>> {
        public static final d j = new d();

        public d() {
            super(1, Sequence.class, "iterator", "iterator()Ljava/util/Iterator;", 0);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public final Iterator<R> invoke(Sequence<? extends R> sequence) {
            m.checkNotNullParameter(sequence, "p1");
            return sequence.iterator();
        }
    }

    /* compiled from: _Sequences.kt */
    /* loaded from: classes3.dex */
    public static final class e implements Sequence<T> {
        public final /* synthetic */ Sequence a;

        public e(Sequence<? extends T> sequence) {
            this.a = sequence;
        }

        @Override // kotlin.sequences.Sequence
        public Iterator<T> iterator() {
            List mutableList = q.toMutableList(this.a);
            d0.t.q.sort(mutableList);
            return mutableList.iterator();
        }
    }

    /* compiled from: _Sequences.kt */
    /* loaded from: classes3.dex */
    public static final class f implements Sequence<T> {
        public final /* synthetic */ Sequence a;

        /* renamed from: b */
        public final /* synthetic */ Comparator f3546b;

        public f(Sequence<? extends T> sequence, Comparator comparator) {
            this.a = sequence;
            this.f3546b = comparator;
        }

        @Override // kotlin.sequences.Sequence
        public Iterator<T> iterator() {
            List mutableList = q.toMutableList(this.a);
            d0.t.q.sortWith(mutableList, this.f3546b);
            return mutableList.iterator();
        }
    }

    public static final <T> boolean any(Sequence<? extends T> sequence) {
        m.checkNotNullParameter(sequence, "$this$any");
        return sequence.iterator().hasNext();
    }

    public static final <T> Iterable<T> asIterable(Sequence<? extends T> sequence) {
        m.checkNotNullParameter(sequence, "$this$asIterable");
        return new a(sequence);
    }

    public static final <T> int count(Sequence<? extends T> sequence) {
        m.checkNotNullParameter(sequence, "$this$count");
        Iterator<? extends T> it = sequence.iterator();
        int i = 0;
        while (it.hasNext()) {
            it.next();
            i++;
            if (i < 0) {
                n.throwCountOverflow();
            }
        }
        return i;
    }

    public static final <T> Sequence<T> distinct(Sequence<? extends T> sequence) {
        m.checkNotNullParameter(sequence, "$this$distinct");
        return distinctBy(sequence, b.j);
    }

    public static final <T, K> Sequence<T> distinctBy(Sequence<? extends T> sequence, Function1<? super T, ? extends K> function1) {
        m.checkNotNullParameter(sequence, "$this$distinctBy");
        m.checkNotNullParameter(function1, "selector");
        return new d0.f0.c(sequence, function1);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> Sequence<T> drop(Sequence<? extends T> sequence, int i) {
        m.checkNotNullParameter(sequence, "$this$drop");
        if (i >= 0) {
            return i == 0 ? sequence : sequence instanceof d0.f0.e ? ((d0.f0.e) sequence).drop(i) : new d0.f0.d(sequence, i);
        }
        throw new IllegalArgumentException(b.d.b.a.a.q("Requested element count ", i, " is less than zero.").toString());
    }

    public static final <T> Sequence<T> filter(Sequence<? extends T> sequence, Function1<? super T, Boolean> function1) {
        m.checkNotNullParameter(sequence, "$this$filter");
        m.checkNotNullParameter(function1, "predicate");
        return new g(sequence, true, function1);
    }

    public static final <T> Sequence<T> filterNot(Sequence<? extends T> sequence, Function1<? super T, Boolean> function1) {
        m.checkNotNullParameter(sequence, "$this$filterNot");
        m.checkNotNullParameter(function1, "predicate");
        return new g(sequence, false, function1);
    }

    public static final <T> Sequence<T> filterNotNull(Sequence<? extends T> sequence) {
        m.checkNotNullParameter(sequence, "$this$filterNotNull");
        Sequence<T> filterNot = filterNot(sequence, c.j);
        Objects.requireNonNull(filterNot, "null cannot be cast to non-null type kotlin.sequences.Sequence<T>");
        return filterNot;
    }

    public static final <T> T firstOrNull(Sequence<? extends T> sequence) {
        m.checkNotNullParameter(sequence, "$this$firstOrNull");
        Iterator<? extends T> it = sequence.iterator();
        if (!it.hasNext()) {
            return null;
        }
        return it.next();
    }

    public static final <T, R> Sequence<R> flatMap(Sequence<? extends T> sequence, Function1<? super T, ? extends Sequence<? extends R>> function1) {
        m.checkNotNullParameter(sequence, "$this$flatMap");
        m.checkNotNullParameter(function1, "transform");
        return new h(sequence, function1, d.j);
    }

    public static final <T, A extends Appendable> A joinTo(Sequence<? extends T> sequence, A a2, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, Function1<? super T, ? extends CharSequence> function1) {
        m.checkNotNullParameter(sequence, "$this$joinTo");
        m.checkNotNullParameter(a2, "buffer");
        m.checkNotNullParameter(charSequence, "separator");
        m.checkNotNullParameter(charSequence2, "prefix");
        m.checkNotNullParameter(charSequence3, "postfix");
        m.checkNotNullParameter(charSequence4, "truncated");
        a2.append(charSequence2);
        int i2 = 0;
        for (T t : sequence) {
            i2++;
            if (i2 > 1) {
                a2.append(charSequence);
            }
            if (i >= 0 && i2 > i) {
                break;
            }
            l.appendElement(a2, t, function1);
        }
        if (i >= 0 && i2 > i) {
            a2.append(charSequence4);
        }
        a2.append(charSequence3);
        return a2;
    }

    public static final <T> String joinToString(Sequence<? extends T> sequence, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, Function1<? super T, ? extends CharSequence> function1) {
        m.checkNotNullParameter(sequence, "$this$joinToString");
        m.checkNotNullParameter(charSequence, "separator");
        m.checkNotNullParameter(charSequence2, "prefix");
        m.checkNotNullParameter(charSequence3, "postfix");
        m.checkNotNullParameter(charSequence4, "truncated");
        String sb = ((StringBuilder) joinTo(sequence, new StringBuilder(), charSequence, charSequence2, charSequence3, i, charSequence4, function1)).toString();
        m.checkNotNullExpressionValue(sb, "joinTo(StringBuilder(), …ed, transform).toString()");
        return sb;
    }

    public static /* synthetic */ String joinToString$default(Sequence sequence, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, Function1 function1, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            charSequence = ", ";
        }
        CharSequence charSequence5 = "";
        CharSequence charSequence6 = (i2 & 2) != 0 ? charSequence5 : charSequence2;
        if ((i2 & 4) == 0) {
            charSequence5 = charSequence3;
        }
        int i3 = (i2 & 8) != 0 ? -1 : i;
        if ((i2 & 16) != 0) {
            charSequence4 = "...";
        }
        CharSequence charSequence7 = charSequence4;
        if ((i2 & 32) != 0) {
            function1 = null;
        }
        return joinToString(sequence, charSequence, charSequence6, charSequence5, i3, charSequence7, function1);
    }

    public static final <T> T last(Sequence<? extends T> sequence) {
        m.checkNotNullParameter(sequence, "$this$last");
        Iterator<? extends T> it = sequence.iterator();
        if (it.hasNext()) {
            T next = it.next();
            while (it.hasNext()) {
                next = it.next();
            }
            return next;
        }
        throw new NoSuchElementException("Sequence is empty.");
    }

    public static final <T, R> Sequence<R> map(Sequence<? extends T> sequence, Function1<? super T, ? extends R> function1) {
        m.checkNotNullParameter(sequence, "$this$map");
        m.checkNotNullParameter(function1, "transform");
        return new u(sequence, function1);
    }

    public static final <T, R> Sequence<R> mapNotNull(Sequence<? extends T> sequence, Function1<? super T, ? extends R> function1) {
        m.checkNotNullParameter(sequence, "$this$mapNotNull");
        m.checkNotNullParameter(function1, "transform");
        return filterNotNull(new u(sequence, function1));
    }

    public static final <T> Sequence<T> plus(Sequence<? extends T> sequence, T t) {
        m.checkNotNullParameter(sequence, "$this$plus");
        return n.flatten(n.sequenceOf(sequence, n.sequenceOf(t)));
    }

    public static final <T extends Comparable<? super T>> Sequence<T> sorted(Sequence<? extends T> sequence) {
        m.checkNotNullParameter(sequence, "$this$sorted");
        return new e(sequence);
    }

    public static final <T> Sequence<T> sortedWith(Sequence<? extends T> sequence, Comparator<? super T> comparator) {
        m.checkNotNullParameter(sequence, "$this$sortedWith");
        m.checkNotNullParameter(comparator, "comparator");
        return new f(sequence, comparator);
    }

    public static final <T> Sequence<T> take(Sequence<? extends T> sequence, int i) {
        m.checkNotNullParameter(sequence, "$this$take");
        if (!(i >= 0)) {
            throw new IllegalArgumentException(b.d.b.a.a.q("Requested element count ", i, " is less than zero.").toString());
        } else if (i == 0) {
            return n.emptySequence();
        } else {
            return sequence instanceof d0.f0.e ? ((d0.f0.e) sequence).take(i) : new s(sequence, i);
        }
    }

    public static final <T> Sequence<T> takeWhile(Sequence<? extends T> sequence, Function1<? super T, Boolean> function1) {
        m.checkNotNullParameter(sequence, "$this$takeWhile");
        m.checkNotNullParameter(function1, "predicate");
        return new t(sequence, function1);
    }

    public static final <T, C extends Collection<? super T>> C toCollection(Sequence<? extends T> sequence, C c2) {
        m.checkNotNullParameter(sequence, "$this$toCollection");
        m.checkNotNullParameter(c2, "destination");
        for (T t : sequence) {
            c2.add(t);
        }
        return c2;
    }

    public static final <T> HashSet<T> toHashSet(Sequence<? extends T> sequence) {
        m.checkNotNullParameter(sequence, "$this$toHashSet");
        return (HashSet) toCollection(sequence, new HashSet());
    }

    public static final <T> List<T> toList(Sequence<? extends T> sequence) {
        m.checkNotNullParameter(sequence, "$this$toList");
        return n.optimizeReadOnlyList(toMutableList(sequence));
    }

    public static final <T> List<T> toMutableList(Sequence<? extends T> sequence) {
        m.checkNotNullParameter(sequence, "$this$toMutableList");
        return (List) toCollection(sequence, new ArrayList());
    }

    public static final <T> Set<T> toSet(Sequence<? extends T> sequence) {
        m.checkNotNullParameter(sequence, "$this$toSet");
        return n0.optimizeReadOnlySet((Set) toCollection(sequence, new LinkedHashSet()));
    }

    public static final <T> Sequence<T> plus(Sequence<? extends T> sequence, Iterable<? extends T> iterable) {
        m.checkNotNullParameter(sequence, "$this$plus");
        m.checkNotNullParameter(iterable, "elements");
        return n.flatten(n.sequenceOf(sequence, u.asSequence(iterable)));
    }

    public static final <T> Sequence<T> plus(Sequence<? extends T> sequence, Sequence<? extends T> sequence2) {
        m.checkNotNullParameter(sequence, "$this$plus");
        m.checkNotNullParameter(sequence2, "elements");
        return n.flatten(n.sequenceOf(sequence, sequence2));
    }
}
