package d0.f0;

import d0.z.d.m;
import java.util.Iterator;
import kotlin.jvm.functions.Function1;
import kotlin.sequences.Sequence;
/* compiled from: Sequences.kt */
/* loaded from: classes3.dex */
public final class c<T, K> implements Sequence<T> {
    public final Sequence<T> a;

    /* renamed from: b  reason: collision with root package name */
    public final Function1<T, K> f3541b;

    /* JADX WARN: Multi-variable type inference failed */
    public c(Sequence<? extends T> sequence, Function1<? super T, ? extends K> function1) {
        m.checkNotNullParameter(sequence, "source");
        m.checkNotNullParameter(function1, "keySelector");
        this.a = sequence;
        this.f3541b = function1;
    }

    @Override // kotlin.sequences.Sequence
    public Iterator<T> iterator() {
        return new b(this.a.iterator(), this.f3541b);
    }
}
