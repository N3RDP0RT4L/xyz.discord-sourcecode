package d0.f0;

import d0.w.h.c;
import java.util.Iterator;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.sequences.Sequence;
/* compiled from: SequenceBuilder.kt */
/* loaded from: classes3.dex */
public abstract class k<T> {
    public abstract Object yield(T t, Continuation<? super Unit> continuation);

    public abstract Object yieldAll(Iterator<? extends T> it, Continuation<? super Unit> continuation);

    public final Object yieldAll(Sequence<? extends T> sequence, Continuation<? super Unit> continuation) {
        Object yieldAll = yieldAll(sequence.iterator(), continuation);
        return yieldAll == c.getCOROUTINE_SUSPENDED() ? yieldAll : Unit.a;
    }
}
