package d0.f0;

import andhook.lib.xposed.ClassUtils;
import d0.z.d.m;
import java.util.Iterator;
import java.util.NoSuchElementException;
import kotlin.sequences.Sequence;
/* compiled from: Sequences.kt */
/* loaded from: classes3.dex */
public final class s<T> implements Sequence<T>, e<T> {
    public final Sequence<T> a;

    /* renamed from: b  reason: collision with root package name */
    public final int f3548b;

    /* compiled from: Sequences.kt */
    /* loaded from: classes3.dex */
    public static final class a implements Iterator<T>, d0.z.d.g0.a {
        public int j;
        public final Iterator<T> k;

        public a(s sVar) {
            this.j = sVar.f3548b;
            this.k = sVar.a.iterator();
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            return this.j > 0 && this.k.hasNext();
        }

        @Override // java.util.Iterator
        public T next() {
            int i = this.j;
            if (i != 0) {
                this.j = i - 1;
                return this.k.next();
            }
            throw new NoSuchElementException();
        }

        @Override // java.util.Iterator
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public s(Sequence<? extends T> sequence, int i) {
        m.checkNotNullParameter(sequence, "sequence");
        this.a = sequence;
        this.f3548b = i;
        if (!(i >= 0)) {
            throw new IllegalArgumentException(("count must be non-negative, but was " + i + ClassUtils.PACKAGE_SEPARATOR_CHAR).toString());
        }
    }

    @Override // d0.f0.e
    public Sequence<T> drop(int i) {
        int i2 = this.f3548b;
        return i >= i2 ? n.emptySequence() : new r(this.a, i, i2);
    }

    @Override // kotlin.sequences.Sequence
    public Iterator<T> iterator() {
        return new a(this);
    }

    @Override // d0.f0.e
    public Sequence<T> take(int i) {
        return i >= this.f3548b ? this : new s(this.a, i);
    }
}
