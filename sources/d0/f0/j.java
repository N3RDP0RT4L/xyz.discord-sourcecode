package d0.f0;

import d0.k;
import d0.l;
import d0.w.f;
import d0.w.h.c;
import d0.w.i.a.g;
import d0.z.d.g0.a;
import d0.z.d.m;
import java.util.Iterator;
import java.util.NoSuchElementException;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
/* compiled from: SequenceBuilder.kt */
/* loaded from: classes3.dex */
public final class j<T> extends k<T> implements Iterator<T>, Continuation<Unit>, a {
    public int j;
    public T k;
    public Iterator<? extends T> l;
    public Continuation<? super Unit> m;

    public final Throwable b() {
        int i = this.j;
        if (i == 4) {
            return new NoSuchElementException();
        }
        if (i == 5) {
            return new IllegalStateException("Iterator has failed.");
        }
        StringBuilder R = b.d.b.a.a.R("Unexpected state of the iterator: ");
        R.append(this.j);
        return new IllegalStateException(R.toString());
    }

    @Override // kotlin.coroutines.Continuation
    public CoroutineContext getContext() {
        return f.j;
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        while (true) {
            int i = this.j;
            if (i != 0) {
                if (i == 1) {
                    Iterator<? extends T> it = this.l;
                    m.checkNotNull(it);
                    if (it.hasNext()) {
                        this.j = 2;
                        return true;
                    }
                    this.l = null;
                } else if (i == 2 || i == 3) {
                    return true;
                } else {
                    if (i == 4) {
                        return false;
                    }
                    throw b();
                }
            }
            this.j = 5;
            Continuation<? super Unit> continuation = this.m;
            m.checkNotNull(continuation);
            this.m = null;
            Unit unit = Unit.a;
            k.a aVar = k.j;
            continuation.resumeWith(k.m73constructorimpl(unit));
        }
    }

    @Override // java.util.Iterator
    public T next() {
        int i = this.j;
        if (i == 0 || i == 1) {
            if (hasNext()) {
                return next();
            }
            throw new NoSuchElementException();
        } else if (i == 2) {
            this.j = 1;
            Iterator<? extends T> it = this.l;
            m.checkNotNull(it);
            return it.next();
        } else if (i == 3) {
            this.j = 0;
            T t = this.k;
            this.k = null;
            return t;
        } else {
            throw b();
        }
    }

    @Override // java.util.Iterator
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @Override // kotlin.coroutines.Continuation
    public void resumeWith(Object obj) {
        l.throwOnFailure(obj);
        this.j = 4;
    }

    public final void setNextStep(Continuation<? super Unit> continuation) {
        this.m = continuation;
    }

    @Override // d0.f0.k
    public Object yield(T t, Continuation<? super Unit> continuation) {
        this.k = t;
        this.j = 3;
        this.m = continuation;
        Object coroutine_suspended = c.getCOROUTINE_SUSPENDED();
        if (coroutine_suspended == c.getCOROUTINE_SUSPENDED()) {
            g.probeCoroutineSuspended(continuation);
        }
        return coroutine_suspended == c.getCOROUTINE_SUSPENDED() ? coroutine_suspended : Unit.a;
    }

    @Override // d0.f0.k
    public Object yieldAll(Iterator<? extends T> it, Continuation<? super Unit> continuation) {
        if (!it.hasNext()) {
            return Unit.a;
        }
        this.l = it;
        this.j = 2;
        this.m = continuation;
        Object coroutine_suspended = c.getCOROUTINE_SUSPENDED();
        if (coroutine_suspended == c.getCOROUTINE_SUSPENDED()) {
            g.probeCoroutineSuspended(continuation);
        }
        return coroutine_suspended == c.getCOROUTINE_SUSPENDED() ? coroutine_suspended : Unit.a;
    }
}
