package d0.f0;

import d0.z.d.m;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicReference;
import kotlin.sequences.Sequence;
/* compiled from: SequencesJVM.kt */
/* loaded from: classes3.dex */
public final class a<T> implements Sequence<T> {
    public final AtomicReference<Sequence<T>> a;

    public a(Sequence<? extends T> sequence) {
        m.checkNotNullParameter(sequence, "sequence");
        this.a = new AtomicReference<>(sequence);
    }

    @Override // kotlin.sequences.Sequence
    public Iterator<T> iterator() {
        Sequence<T> andSet = this.a.getAndSet(null);
        if (andSet != null) {
            return andSet.iterator();
        }
        throw new IllegalStateException("This sequence can be consumed only once.");
    }
}
