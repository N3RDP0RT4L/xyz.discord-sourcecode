package d0.f0;

import d0.z.d.m;
import java.util.Iterator;
import java.util.NoSuchElementException;
import kotlin.jvm.functions.Function1;
import kotlin.sequences.Sequence;
/* compiled from: Sequences.kt */
/* loaded from: classes3.dex */
public final class g<T> implements Sequence<T> {
    public final Sequence<T> a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f3543b;
    public final Function1<T, Boolean> c;

    /* compiled from: Sequences.kt */
    /* loaded from: classes3.dex */
    public static final class a implements Iterator<T>, d0.z.d.g0.a {
        public final Iterator<T> j;
        public int k = -1;
        public T l;

        public a() {
            this.j = g.this.a.iterator();
        }

        public final void a() {
            while (this.j.hasNext()) {
                T next = this.j.next();
                if (((Boolean) g.this.c.invoke(next)).booleanValue() == g.this.f3543b) {
                    this.l = next;
                    this.k = 1;
                    return;
                }
            }
            this.k = 0;
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            if (this.k == -1) {
                a();
            }
            return this.k == 1;
        }

        @Override // java.util.Iterator
        public T next() {
            if (this.k == -1) {
                a();
            }
            if (this.k != 0) {
                T t = this.l;
                this.l = null;
                this.k = -1;
                return t;
            }
            throw new NoSuchElementException();
        }

        @Override // java.util.Iterator
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public g(Sequence<? extends T> sequence, boolean z2, Function1<? super T, Boolean> function1) {
        m.checkNotNullParameter(sequence, "sequence");
        m.checkNotNullParameter(function1, "predicate");
        this.a = sequence;
        this.f3543b = z2;
        this.c = function1;
    }

    @Override // kotlin.sequences.Sequence
    public Iterator<T> iterator() {
        return new a();
    }
}
