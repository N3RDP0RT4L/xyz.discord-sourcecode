package d0.f0;

import d0.z.d.m;
import java.util.Iterator;
import java.util.NoSuchElementException;
import kotlin.jvm.functions.Function1;
import kotlin.sequences.Sequence;
/* compiled from: Sequences.kt */
/* loaded from: classes3.dex */
public final class h<T, R, E> implements Sequence<E> {
    public final Sequence<T> a;

    /* renamed from: b  reason: collision with root package name */
    public final Function1<T, R> f3544b;
    public final Function1<R, Iterator<E>> c;

    /* compiled from: Sequences.kt */
    /* loaded from: classes3.dex */
    public static final class a implements Iterator<E>, d0.z.d.g0.a {
        public final Iterator<T> j;
        public Iterator<? extends E> k;

        public a() {
            this.j = h.this.a.iterator();
        }

        public final boolean a() {
            Iterator<? extends E> it = this.k;
            if (it != null && !it.hasNext()) {
                this.k = null;
            }
            while (true) {
                if (this.k == null) {
                    if (this.j.hasNext()) {
                        Iterator<? extends E> it2 = (Iterator) h.this.c.invoke(h.this.f3544b.invoke(this.j.next()));
                        if (it2.hasNext()) {
                            this.k = it2;
                            break;
                        }
                    } else {
                        return false;
                    }
                } else {
                    break;
                }
            }
            return true;
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            return a();
        }

        @Override // java.util.Iterator
        public E next() {
            if (a()) {
                Iterator<? extends E> it = this.k;
                m.checkNotNull(it);
                return it.next();
            }
            throw new NoSuchElementException();
        }

        @Override // java.util.Iterator
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public h(Sequence<? extends T> sequence, Function1<? super T, ? extends R> function1, Function1<? super R, ? extends Iterator<? extends E>> function12) {
        m.checkNotNullParameter(sequence, "sequence");
        m.checkNotNullParameter(function1, "transformer");
        m.checkNotNullParameter(function12, "iterator");
        this.a = sequence;
        this.f3544b = function1;
        this.c = function12;
    }

    @Override // kotlin.sequences.Sequence
    public Iterator<E> iterator() {
        return new a();
    }
}
