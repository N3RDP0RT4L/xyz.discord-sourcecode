package d0.f0;

import d0.t.k;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Iterator;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.sequences.Sequence;
/* compiled from: Sequences.kt */
/* loaded from: classes3.dex */
public class n extends m {

    /* compiled from: Sequences.kt */
    /* loaded from: classes3.dex */
    public static final class a implements Sequence<T> {
        public final /* synthetic */ Iterator a;

        public a(Iterator it) {
            this.a = it;
        }

        @Override // kotlin.sequences.Sequence
        public Iterator<T> iterator() {
            return this.a;
        }
    }

    /* compiled from: Sequences.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function1<Sequence<? extends T>, Iterator<? extends T>> {
        public static final b j = new b();

        public b() {
            super(1);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public final Iterator<T> invoke(Sequence<? extends T> sequence) {
            m.checkNotNullParameter(sequence, "it");
            return sequence.iterator();
        }
    }

    /* compiled from: Sequences.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function1<Iterable<? extends T>, Iterator<? extends T>> {
        public static final c j = new c();

        public c() {
            super(1);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public final Iterator<T> invoke(Iterable<? extends T> iterable) {
            m.checkNotNullParameter(iterable, "it");
            return iterable.iterator();
        }
    }

    /* compiled from: Sequences.kt */
    /* loaded from: classes3.dex */
    public static final class d extends o implements Function1<T, T> {
        public final /* synthetic */ Function0 $nextFunction;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public d(Function0 function0) {
            super(1);
            this.$nextFunction = function0;
        }

        /* JADX WARN: Type inference failed for: r2v2, types: [T, java.lang.Object] */
        @Override // kotlin.jvm.functions.Function1
        public final T invoke(T t) {
            m.checkNotNullParameter(t, "it");
            return this.$nextFunction.invoke();
        }
    }

    /* compiled from: Sequences.kt */
    /* loaded from: classes3.dex */
    public static final class e extends o implements Function0<T> {
        public final /* synthetic */ Object $seed;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public e(Object obj) {
            super(0);
            this.$seed = obj;
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [T, java.lang.Object] */
        @Override // kotlin.jvm.functions.Function0
        public final T invoke() {
            return this.$seed;
        }
    }

    public static final <T> Sequence<T> asSequence(Iterator<? extends T> it) {
        m.checkNotNullParameter(it, "$this$asSequence");
        return constrainOnce(new a(it));
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> Sequence<T> constrainOnce(Sequence<? extends T> sequence) {
        m.checkNotNullParameter(sequence, "$this$constrainOnce");
        return sequence instanceof d0.f0.a ? sequence : new d0.f0.a(sequence);
    }

    public static final <T> Sequence<T> emptySequence() {
        return f.a;
    }

    public static final <T> Sequence<T> flatten(Sequence<? extends Sequence<? extends T>> sequence) {
        m.checkNotNullParameter(sequence, "$this$flatten");
        b bVar = b.j;
        if (sequence instanceof u) {
            return ((u) sequence).flatten$kotlin_stdlib(bVar);
        }
        return new h(sequence, o.j, bVar);
    }

    public static final <T> Sequence<T> flattenSequenceOfIterable(Sequence<? extends Iterable<? extends T>> sequence) {
        m.checkNotNullParameter(sequence, "$this$flatten");
        c cVar = c.j;
        if (sequence instanceof u) {
            return ((u) sequence).flatten$kotlin_stdlib(cVar);
        }
        return new h(sequence, o.j, cVar);
    }

    public static final <T> Sequence<T> generateSequence(Function0<? extends T> function0) {
        m.checkNotNullParameter(function0, "nextFunction");
        return constrainOnce(new i(function0, new d(function0)));
    }

    public static final <T> Sequence<T> sequenceOf(T... tArr) {
        m.checkNotNullParameter(tArr, "elements");
        return tArr.length == 0 ? emptySequence() : k.asSequence(tArr);
    }

    public static final <T> Sequence<T> generateSequence(T t, Function1<? super T, ? extends T> function1) {
        m.checkNotNullParameter(function1, "nextFunction");
        if (t == null) {
            return f.a;
        }
        return new i(new e(t), function1);
    }

    public static final <T> Sequence<T> generateSequence(Function0<? extends T> function0, Function1<? super T, ? extends T> function1) {
        m.checkNotNullParameter(function0, "seedFunction");
        m.checkNotNullParameter(function1, "nextFunction");
        return new i(function0, function1);
    }
}
