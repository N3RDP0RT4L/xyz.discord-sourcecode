package d0;

import d0.z.d.g0.a;
import d0.z.d.m;
import java.util.Collection;
/* compiled from: UIntArray.kt */
/* loaded from: classes3.dex */
public final class q implements Collection<p>, a {
    /* renamed from: constructor-impl  reason: not valid java name */
    public static int[] m82constructorimpl(int i) {
        return m83constructorimpl(new int[i]);
    }

    /* renamed from: constructor-impl  reason: not valid java name */
    public static int[] m83constructorimpl(int[] iArr) {
        m.checkNotNullParameter(iArr, "storage");
        return iArr;
    }

    /* renamed from: get-pVg5ArA  reason: not valid java name */
    public static final int m84getpVg5ArA(int[] iArr, int i) {
        return p.m81constructorimpl(iArr[i]);
    }

    /* renamed from: getSize-impl  reason: not valid java name */
    public static int m85getSizeimpl(int[] iArr) {
        return iArr.length;
    }

    /* renamed from: set-VXSXFK8  reason: not valid java name */
    public static final void m86setVXSXFK8(int[] iArr, int i, int i2) {
        iArr[i] = i2;
    }
}
