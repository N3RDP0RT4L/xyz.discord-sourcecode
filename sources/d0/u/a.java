package d0.u;

import d0.z.d.m;
import java.util.Comparator;
import kotlin.jvm.functions.Function1;
/* compiled from: Comparisons.kt */
/* loaded from: classes3.dex */
public class a {

    /* compiled from: Comparisons.kt */
    /* renamed from: d0.u.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0376a<T> implements Comparator {
        public final /* synthetic */ Function1[] j;

        public C0376a(Function1[] function1Arr) {
            this.j = function1Arr;
        }

        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return a.access$compareValuesByImpl(t, t2, this.j);
        }
    }

    /* compiled from: Comparisons.kt */
    /* loaded from: classes3.dex */
    public static final class b<T> implements Comparator {
        public final /* synthetic */ Comparator j;
        public final /* synthetic */ Comparator k;

        public b(Comparator comparator, Comparator comparator2) {
            this.j = comparator;
            this.k = comparator2;
        }

        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            int compare = this.j.compare(t, t2);
            return compare != 0 ? compare : this.k.compare(t, t2);
        }
    }

    public static final int access$compareValuesByImpl(Object obj, Object obj2, Function1[] function1Arr) {
        for (Function1 function1 : function1Arr) {
            int compareValues = compareValues((Comparable) function1.invoke(obj), (Comparable) function1.invoke(obj2));
            if (compareValues != 0) {
                return compareValues;
            }
        }
        return 0;
    }

    public static final <T> Comparator<T> compareBy(Function1<? super T, ? extends Comparable<?>>... function1Arr) {
        m.checkNotNullParameter(function1Arr, "selectors");
        if (function1Arr.length > 0) {
            return new C0376a(function1Arr);
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }

    public static final <T extends Comparable<?>> int compareValues(T t, T t2) {
        if (t == t2) {
            return 0;
        }
        if (t == null) {
            return -1;
        }
        if (t2 == null) {
            return 1;
        }
        return t.compareTo(t2);
    }

    public static final <T extends Comparable<? super T>> Comparator<T> naturalOrder() {
        return c.j;
    }

    public static final <T> Comparator<T> then(Comparator<T> comparator, Comparator<? super T> comparator2) {
        m.checkNotNullParameter(comparator, "$this$then");
        m.checkNotNullParameter(comparator2, "comparator");
        return new b(comparator, comparator2);
    }
}
