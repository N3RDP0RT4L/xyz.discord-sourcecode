package d0;

import java.io.Serializable;
import kotlin.Lazy;
/* compiled from: Lazy.kt */
/* loaded from: classes3.dex */
public final class d<T> implements Lazy<T>, Serializable {
    private final T value;

    public d(T t) {
        this.value = t;
    }

    @Override // kotlin.Lazy
    public T getValue() {
        return this.value;
    }

    @Override // kotlin.Lazy
    public boolean isInitialized() {
        return true;
    }

    public String toString() {
        return String.valueOf(getValue());
    }
}
