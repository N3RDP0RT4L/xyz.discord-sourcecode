package d0;

import d0.z.d.m;
import kotlin.Lazy;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.functions.Function0;
/* compiled from: LazyJVM.kt */
/* loaded from: classes3.dex */
public class g {
    public static final <T> Lazy<T> lazy(Function0<? extends T> function0) {
        m.checkNotNullParameter(function0, "initializer");
        return new n(function0, null, 2, null);
    }

    public static final <T> Lazy<T> lazy(i iVar, Function0<? extends T> function0) {
        m.checkNotNullParameter(iVar, "mode");
        m.checkNotNullParameter(function0, "initializer");
        int ordinal = iVar.ordinal();
        if (ordinal == 0) {
            return new n(function0, null, 2, null);
        }
        if (ordinal == 1) {
            return new m(function0);
        }
        if (ordinal == 2) {
            return new s(function0);
        }
        throw new NoWhenBranchMatchedException();
    }
}
