package d0;

import d0.z.d.m;
import java.io.Serializable;
import kotlin.Lazy;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: LazyJVM.kt */
/* loaded from: classes3.dex */
public final class n<T> implements Lazy<T>, Serializable {
    private volatile Object _value;
    private Function0<? extends T> initializer;
    private final Object lock;

    public n(Function0<? extends T> function0, Object obj) {
        m.checkNotNullParameter(function0, "initializer");
        this.initializer = function0;
        this._value = r.a;
        this.lock = obj == null ? this : obj;
    }

    private final Object writeReplace() {
        return new d(getValue());
    }

    @Override // kotlin.Lazy
    public T getValue() {
        T t;
        T t2 = (T) this._value;
        r rVar = r.a;
        if (t2 != rVar) {
            return t2;
        }
        synchronized (this.lock) {
            t = (T) this._value;
            if (t == rVar) {
                Function0<? extends T> function0 = this.initializer;
                m.checkNotNull(function0);
                t = function0.invoke();
                this._value = t;
                this.initializer = null;
            }
        }
        return t;
    }

    @Override // kotlin.Lazy
    public boolean isInitialized() {
        return this._value != r.a;
    }

    public String toString() {
        return isInitialized() ? String.valueOf(getValue()) : "Lazy value not initialized yet.";
    }

    public /* synthetic */ n(Function0 function0, Object obj, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(function0, (i & 2) != 0 ? null : obj);
    }
}
