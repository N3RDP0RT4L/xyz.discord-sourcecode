package d0;

import d0.k;
import d0.z.d.m;
/* compiled from: Result.kt */
/* loaded from: classes3.dex */
public final class l {
    public static final Object createFailure(Throwable th) {
        m.checkNotNullParameter(th, "exception");
        return new k.b(th);
    }

    public static final void throwOnFailure(Object obj) {
        if (obj instanceof k.b) {
            throw ((k.b) obj).exception;
        }
    }
}
