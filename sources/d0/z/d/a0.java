package d0.z.d;

import d0.e0.c;
import d0.e0.e;
import d0.e0.g;
import kotlin.reflect.KDeclarationContainer;
import kotlin.reflect.KFunction;
import kotlin.reflect.KMutableProperty0;
import kotlin.reflect.KProperty0;
/* compiled from: Reflection.java */
/* loaded from: classes3.dex */
public class a0 {
    public static final b0 a;

    /* renamed from: b  reason: collision with root package name */
    public static final c[] f3564b;

    static {
        b0 b0Var = null;
        try {
            b0Var = (b0) Class.forName("d0.e0.p.d.d0").newInstance();
        } catch (ClassCastException | ClassNotFoundException | IllegalAccessException | InstantiationException unused) {
        }
        if (b0Var == null) {
            b0Var = new b0();
        }
        a = b0Var;
        f3564b = new c[0];
    }

    public static KFunction function(j jVar) {
        return a.function(jVar);
    }

    public static c getOrCreateKotlinClass(Class cls) {
        return a.getOrCreateKotlinClass(cls);
    }

    public static KDeclarationContainer getOrCreateKotlinPackage(Class cls) {
        return a.getOrCreateKotlinPackage(cls, "");
    }

    public static KMutableProperty0 mutableProperty0(p pVar) {
        return a.mutableProperty0(pVar);
    }

    public static e mutableProperty1(r rVar) {
        return a.mutableProperty1(rVar);
    }

    public static KProperty0 property0(v vVar) {
        return a.property0(vVar);
    }

    public static g property1(x xVar) {
        return a.property1(xVar);
    }

    public static String renderLambdaToString(o oVar) {
        return a.renderLambdaToString(oVar);
    }

    public static KDeclarationContainer getOrCreateKotlinPackage(Class cls, String str) {
        return a.getOrCreateKotlinPackage(cls, str);
    }

    public static String renderLambdaToString(i iVar) {
        return a.renderLambdaToString(iVar);
    }
}
