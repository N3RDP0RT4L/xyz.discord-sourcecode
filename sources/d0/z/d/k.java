package d0.z.d;

import d0.e0.c;
import kotlin.reflect.KDeclarationContainer;
/* compiled from: FunctionReferenceImpl.java */
/* loaded from: classes3.dex */
public class k extends j {
    public k(int i, KDeclarationContainer kDeclarationContainer, String str, String str2) {
        super(i, d.NO_RECEIVER, ((e) kDeclarationContainer).getJClass(), str, str2, !(kDeclarationContainer instanceof c) ? 1 : 0);
    }

    public k(int i, Class cls, String str, String str2, int i2) {
        super(i, d.NO_RECEIVER, cls, str, str2, i2);
    }

    public k(int i, Object obj, Class cls, String str, String str2, int i2) {
        super(i, obj, cls, str, str2, i2);
    }
}
