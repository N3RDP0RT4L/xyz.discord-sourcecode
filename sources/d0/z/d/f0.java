package d0.z.d;

import d0.e0.h;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: TypeParameterReference.kt */
/* loaded from: classes3.dex */
public final class f0 implements h {
    public static final a j = new a(null);

    /* compiled from: TypeParameterReference.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final String toString(h hVar) {
            m.checkNotNullParameter(hVar, "typeParameter");
            StringBuilder sb = new StringBuilder();
            int ordinal = hVar.getVariance().ordinal();
            if (ordinal == 1) {
                sb.append("in ");
            } else if (ordinal == 2) {
                sb.append("out ");
            }
            sb.append(hVar.getName());
            String sb2 = sb.toString();
            m.checkNotNullExpressionValue(sb2, "StringBuilder().apply(builderAction).toString()");
            return sb2;
        }
    }
}
