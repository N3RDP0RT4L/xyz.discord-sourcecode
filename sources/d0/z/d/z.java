package d0.z.d;

import b.d.b.a.a;
import kotlin.reflect.KCallable;
import kotlin.reflect.KProperty;
/* compiled from: PropertyReference.java */
/* loaded from: classes3.dex */
public abstract class z extends d implements KProperty {
    public z() {
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof z) {
            z zVar = (z) obj;
            return getOwner().equals(zVar.getOwner()) && getName().equals(zVar.getName()) && getSignature().equals(zVar.getSignature()) && m.areEqual(getBoundReceiver(), zVar.getBoundReceiver());
        } else if (obj instanceof KProperty) {
            return obj.equals(compute());
        } else {
            return false;
        }
    }

    public int hashCode() {
        int hashCode = getName().hashCode();
        return getSignature().hashCode() + ((hashCode + (getOwner().hashCode() * 31)) * 31);
    }

    @Override // kotlin.reflect.KProperty
    public boolean isConst() {
        return getReflected().isConst();
    }

    @Override // kotlin.reflect.KProperty
    public boolean isLateinit() {
        return getReflected().isLateinit();
    }

    public String toString() {
        KCallable compute = compute();
        if (compute != this) {
            return compute.toString();
        }
        StringBuilder R = a.R("property ");
        R.append(getName());
        R.append(" (Kotlin reflection is not available)");
        return R.toString();
    }

    public z(Object obj) {
        super(obj);
    }

    @Override // d0.z.d.d
    public KProperty getReflected() {
        return (KProperty) super.getReflected();
    }

    public z(Object obj, Class cls, String str, String str2, int i) {
        super(obj, cls, str, str2, (i & 1) == 1);
    }
}
