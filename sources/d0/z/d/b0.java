package d0.z.d;

import d0.e0.c;
import d0.e0.e;
import d0.e0.g;
import kotlin.reflect.KDeclarationContainer;
import kotlin.reflect.KFunction;
import kotlin.reflect.KMutableProperty0;
import kotlin.reflect.KProperty0;
/* compiled from: ReflectionFactory.java */
/* loaded from: classes3.dex */
public class b0 {
    public KFunction function(j jVar) {
        return jVar;
    }

    public c getOrCreateKotlinClass(Class cls) {
        return new f(cls);
    }

    public KDeclarationContainer getOrCreateKotlinPackage(Class cls, String str) {
        return new u(cls, str);
    }

    public KMutableProperty0 mutableProperty0(p pVar) {
        return pVar;
    }

    public e mutableProperty1(r rVar) {
        return rVar;
    }

    public KProperty0 property0(v vVar) {
        return vVar;
    }

    public g property1(x xVar) {
        return xVar;
    }

    public String renderLambdaToString(o oVar) {
        return renderLambdaToString((i) oVar);
    }

    public String renderLambdaToString(i iVar) {
        String obj = iVar.getClass().getGenericInterfaces()[0].toString();
        return obj.startsWith("kotlin.jvm.functions.") ? obj.substring(21) : obj;
    }
}
