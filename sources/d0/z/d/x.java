package d0.z.d;

import d0.e0.g;
import kotlin.reflect.KCallable;
/* compiled from: PropertyReference1.java */
/* loaded from: classes3.dex */
public abstract class x extends z implements g {
    public x() {
    }

    @Override // d0.z.d.d
    public KCallable computeReflected() {
        return a0.property1(this);
    }

    @Override // d0.e0.g
    public g.a getGetter() {
        return ((g) getReflected()).getGetter();
    }

    @Override // kotlin.jvm.functions.Function1
    public Object invoke(Object obj) {
        return get(obj);
    }

    public x(Object obj, Class cls, String str, String str2, int i) {
        super(obj, cls, str, str2, i);
    }
}
