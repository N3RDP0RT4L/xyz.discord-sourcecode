package d0.z.d;

import d0.e0.c;
import kotlin.reflect.KDeclarationContainer;
/* compiled from: PropertyReference1Impl.java */
/* loaded from: classes3.dex */
public class y extends x {
    public y(KDeclarationContainer kDeclarationContainer, String str, String str2) {
        super(d.NO_RECEIVER, ((e) kDeclarationContainer).getJClass(), str, str2, !(kDeclarationContainer instanceof c) ? 1 : 0);
    }

    @Override // d0.e0.g
    public Object get(Object obj) {
        return getGetter().call(obj);
    }

    public y(Class cls, String str, String str2, int i) {
        super(d.NO_RECEIVER, cls, str, str2, i);
    }
}
