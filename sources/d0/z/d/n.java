package d0.z.d;

import java.lang.reflect.Type;
import kotlin.reflect.KType;
/* compiled from: KTypeBase.kt */
/* loaded from: classes3.dex */
public interface n extends KType {
    Type getJavaType();
}
