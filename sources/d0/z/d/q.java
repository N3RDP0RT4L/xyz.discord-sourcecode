package d0.z.d;

import d0.e0.c;
import kotlin.reflect.KDeclarationContainer;
/* compiled from: MutablePropertyReference0Impl.java */
/* loaded from: classes3.dex */
public class q extends p {
    public q(KDeclarationContainer kDeclarationContainer, String str, String str2) {
        super(d.NO_RECEIVER, ((e) kDeclarationContainer).getJClass(), str, str2, !(kDeclarationContainer instanceof c) ? 1 : 0);
    }

    public Object get() {
        return getGetter().call(new Object[0]);
    }

    public void set(Object obj) {
        getSetter().call(obj);
    }

    public q(Class cls, String str, String str2, int i) {
        super(d.NO_RECEIVER, cls, str, str2, i);
    }

    public q(Object obj, Class cls, String str, String str2, int i) {
        super(obj, cls, str, str2, i);
    }
}
