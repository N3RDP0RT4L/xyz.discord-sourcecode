package d0.z.d;

import kotlin.reflect.KCallable;
import kotlin.reflect.KProperty0;
/* compiled from: PropertyReference0.java */
/* loaded from: classes3.dex */
public abstract class v extends z implements KProperty0 {
    public v() {
    }

    @Override // d0.z.d.d
    public KCallable computeReflected() {
        return a0.property0(this);
    }

    @Override // kotlin.reflect.KProperty0
    public Object getDelegate() {
        return ((KProperty0) getReflected()).getDelegate();
    }

    @Override // kotlin.jvm.functions.Function0
    public Object invoke() {
        return get();
    }

    public v(Object obj) {
        super(obj);
    }

    @Override // kotlin.reflect.KProperty0
    public KProperty0.Getter getGetter() {
        return ((KProperty0) getReflected()).getGetter();
    }

    public v(Object obj, Class cls, String str, String str2, int i) {
        super(obj, cls, str, str2, i);
    }
}
