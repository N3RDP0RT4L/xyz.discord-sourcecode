package d0.z.d;

import andhook.lib.xposed.ClassUtils;
import com.swift.sandhook.annotation.MethodReflectParams;
import d0.e0.c;
import d0.g0.w;
import d0.t.g0;
import d0.t.h0;
import d0.t.n;
import d0.t.o;
import d0.z.c.b;
import d0.z.c.d;
import d0.z.c.e;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Pair;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function10;
import kotlin.jvm.functions.Function11;
import kotlin.jvm.functions.Function12;
import kotlin.jvm.functions.Function13;
import kotlin.jvm.functions.Function14;
import kotlin.jvm.functions.Function15;
import kotlin.jvm.functions.Function16;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function22;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.functions.Function4;
import kotlin.jvm.functions.Function5;
import kotlin.jvm.functions.Function6;
import kotlin.jvm.functions.Function7;
import kotlin.jvm.functions.Function8;
import kotlin.jvm.functions.Function9;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ClassReference.kt */
/* loaded from: classes3.dex */
public final class f implements c<Object>, e {
    public static final Map<Class<? extends d0.c<?>>, Integer> j;
    public static final HashMap<String, String> k;
    public static final HashMap<String, String> l;
    public static final HashMap<String, String> m;
    public static final Map<String, String> n;
    public static final a o = new a(null);
    public final Class<?> p;

    /* compiled from: ClassReference.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final String getClassQualifiedName(Class<?> cls) {
            String str;
            m.checkNotNullParameter(cls, "jClass");
            String str2 = null;
            if (cls.isAnonymousClass() || cls.isLocalClass()) {
                return null;
            }
            if (cls.isArray()) {
                Class<?> componentType = cls.getComponentType();
                m.checkNotNullExpressionValue(componentType, "componentType");
                if (componentType.isPrimitive() && (str = (String) f.m.get(componentType.getName())) != null) {
                    str2 = b.d.b.a.a.v(str, "Array");
                }
                return str2 != null ? str2 : "kotlin.Array";
            }
            String str3 = (String) f.m.get(cls.getName());
            return str3 != null ? str3 : cls.getCanonicalName();
        }

        /* JADX WARN: Code restructure failed: missing block: B:10:0x0040, code lost:
            if (r1 != null) goto L15;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public final java.lang.String getClassSimpleName(java.lang.Class<?> r8) {
            /*
                r7 = this;
                java.lang.String r0 = "jClass"
                d0.z.d.m.checkNotNullParameter(r8, r0)
                boolean r0 = r8.isAnonymousClass()
                java.lang.String r1 = "Array"
                r2 = 0
                if (r0 == 0) goto L11
            Le:
                r1 = r2
                goto Lb6
            L11:
                boolean r0 = r8.isLocalClass()
                if (r0 == 0) goto L73
                java.lang.String r0 = r8.getSimpleName()
                java.lang.reflect.Method r1 = r8.getEnclosingMethod()
                java.lang.String r3 = "$"
                r4 = 2
                java.lang.String r5 = "name"
                if (r1 == 0) goto L43
                d0.z.d.m.checkNotNullExpressionValue(r0, r5)
                java.lang.StringBuilder r6 = new java.lang.StringBuilder
                r6.<init>()
                java.lang.String r1 = r1.getName()
                r6.append(r1)
                r6.append(r3)
                java.lang.String r1 = r6.toString()
                java.lang.String r1 = d0.g0.w.substringAfter$default(r0, r1, r2, r4, r2)
                if (r1 == 0) goto L43
                goto L66
            L43:
                java.lang.reflect.Constructor r8 = r8.getEnclosingConstructor()
                if (r8 == 0) goto L65
                d0.z.d.m.checkNotNullExpressionValue(r0, r5)
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r8 = r8.getName()
                r1.append(r8)
                r1.append(r3)
                java.lang.String r8 = r1.toString()
                java.lang.String r8 = d0.g0.w.substringAfter$default(r0, r8, r2, r4, r2)
                r1 = r8
                goto L66
            L65:
                r1 = r2
            L66:
                if (r1 == 0) goto L69
                goto Lb6
            L69:
                d0.z.d.m.checkNotNullExpressionValue(r0, r5)
                r8 = 36
                java.lang.String r1 = d0.g0.w.substringAfter$default(r0, r8, r2, r4, r2)
                goto Lb6
            L73:
                boolean r0 = r8.isArray()
                if (r0 == 0) goto La0
                java.lang.Class r8 = r8.getComponentType()
                java.lang.String r0 = "componentType"
                d0.z.d.m.checkNotNullExpressionValue(r8, r0)
                boolean r0 = r8.isPrimitive()
                if (r0 == 0) goto L9c
                java.util.Map r0 = d0.z.d.f.access$getSimpleNames$cp()
                java.lang.String r8 = r8.getName()
                java.lang.Object r8 = r0.get(r8)
                java.lang.String r8 = (java.lang.String) r8
                if (r8 == 0) goto L9c
                java.lang.String r2 = b.d.b.a.a.v(r8, r1)
            L9c:
                if (r2 == 0) goto Lb6
                goto Le
            La0:
                java.util.Map r0 = d0.z.d.f.access$getSimpleNames$cp()
                java.lang.String r1 = r8.getName()
                java.lang.Object r0 = r0.get(r1)
                r1 = r0
                java.lang.String r1 = (java.lang.String) r1
                if (r1 == 0) goto Lb2
                goto Lb6
            Lb2:
                java.lang.String r1 = r8.getSimpleName()
            Lb6:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.z.d.f.a.getClassSimpleName(java.lang.Class):java.lang.String");
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    static {
        int i = 0;
        List listOf = n.listOf((Object[]) new Class[]{Function0.class, Function1.class, Function2.class, Function3.class, Function4.class, Function5.class, Function6.class, Function7.class, Function8.class, Function9.class, Function10.class, Function11.class, Function12.class, Function13.class, Function14.class, Function15.class, Function16.class, d0.z.c.a.class, b.class, d0.z.c.c.class, d.class, e.class, Function22.class});
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(listOf, 10));
        for (Object obj : listOf) {
            i++;
            if (i < 0) {
                n.throwIndexOverflow();
            }
            arrayList.add(d0.o.to((Class) obj, Integer.valueOf(i)));
        }
        j = h0.toMap(arrayList);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(MethodReflectParams.BOOLEAN, "kotlin.Boolean");
        hashMap.put(MethodReflectParams.CHAR, "kotlin.Char");
        hashMap.put(MethodReflectParams.BYTE, "kotlin.Byte");
        hashMap.put(MethodReflectParams.SHORT, "kotlin.Short");
        hashMap.put(MethodReflectParams.INT, "kotlin.Int");
        hashMap.put(MethodReflectParams.FLOAT, "kotlin.Float");
        hashMap.put("long", "kotlin.Long");
        hashMap.put(MethodReflectParams.DOUBLE, "kotlin.Double");
        k = hashMap;
        HashMap<String, String> hashMap2 = new HashMap<>();
        hashMap2.put("java.lang.Boolean", "kotlin.Boolean");
        hashMap2.put("java.lang.Character", "kotlin.Char");
        hashMap2.put("java.lang.Byte", "kotlin.Byte");
        hashMap2.put("java.lang.Short", "kotlin.Short");
        hashMap2.put("java.lang.Integer", "kotlin.Int");
        hashMap2.put("java.lang.Float", "kotlin.Float");
        hashMap2.put("java.lang.Long", "kotlin.Long");
        hashMap2.put("java.lang.Double", "kotlin.Double");
        l = hashMap2;
        HashMap<String, String> hashMap3 = new HashMap<>();
        hashMap3.put("java.lang.Object", "kotlin.Any");
        hashMap3.put("java.lang.String", "kotlin.String");
        hashMap3.put("java.lang.CharSequence", "kotlin.CharSequence");
        hashMap3.put("java.lang.Throwable", "kotlin.Throwable");
        hashMap3.put("java.lang.Cloneable", "kotlin.Cloneable");
        hashMap3.put("java.lang.Number", "kotlin.Number");
        hashMap3.put("java.lang.Comparable", "kotlin.Comparable");
        hashMap3.put("java.lang.Enum", "kotlin.Enum");
        hashMap3.put("java.lang.annotation.Annotation", "kotlin.Annotation");
        hashMap3.put("java.lang.Iterable", "kotlin.collections.Iterable");
        hashMap3.put("java.util.Iterator", "kotlin.collections.Iterator");
        hashMap3.put("java.util.Collection", "kotlin.collections.Collection");
        hashMap3.put("java.util.List", "kotlin.collections.List");
        hashMap3.put("java.util.Set", "kotlin.collections.Set");
        hashMap3.put("java.util.ListIterator", "kotlin.collections.ListIterator");
        hashMap3.put("java.util.Map", "kotlin.collections.Map");
        hashMap3.put("java.util.Map$Entry", "kotlin.collections.Map.Entry");
        hashMap3.put("kotlin.jvm.internal.StringCompanionObject", "kotlin.String.Companion");
        hashMap3.put("kotlin.jvm.internal.EnumCompanionObject", "kotlin.Enum.Companion");
        hashMap3.putAll(hashMap);
        hashMap3.putAll(hashMap2);
        Collection<String> values = hashMap.values();
        m.checkNotNullExpressionValue(values, "primitiveFqNames.values");
        for (String str : values) {
            StringBuilder sb = new StringBuilder();
            sb.append("kotlin.jvm.internal.");
            m.checkNotNullExpressionValue(str, "kotlinName");
            sb.append(w.substringAfterLast$default(str, ClassUtils.PACKAGE_SEPARATOR_CHAR, null, 2, null));
            sb.append("CompanionObject");
            Pair pair = d0.o.to(sb.toString(), str + ".Companion");
            hashMap3.put(pair.getFirst(), pair.getSecond());
        }
        for (Map.Entry<Class<? extends d0.c<?>>, Integer> entry : j.entrySet()) {
            int intValue = entry.getValue().intValue();
            hashMap3.put(entry.getKey().getName(), "kotlin.Function" + intValue);
        }
        m = hashMap3;
        LinkedHashMap linkedHashMap = new LinkedHashMap(g0.mapCapacity(hashMap3.size()));
        for (Map.Entry entry2 : hashMap3.entrySet()) {
            linkedHashMap.put(entry2.getKey(), w.substringAfterLast$default((String) entry2.getValue(), ClassUtils.PACKAGE_SEPARATOR_CHAR, null, 2, null));
        }
        n = linkedHashMap;
    }

    public f(Class<?> cls) {
        m.checkNotNullParameter(cls, "jClass");
        this.p = cls;
    }

    public boolean equals(Object obj) {
        return (obj instanceof f) && m.areEqual(d0.z.a.getJavaObjectType(this), d0.z.a.getJavaObjectType((c) obj));
    }

    @Override // d0.z.d.e
    public Class<?> getJClass() {
        return this.p;
    }

    @Override // d0.e0.c
    public Object getObjectInstance() {
        throw new d0.z.b();
    }

    @Override // d0.e0.c
    public String getQualifiedName() {
        return o.getClassQualifiedName(getJClass());
    }

    @Override // d0.e0.c
    public List<c<? extends Object>> getSealedSubclasses() {
        throw new d0.z.b();
    }

    @Override // d0.e0.c
    public String getSimpleName() {
        return o.getClassSimpleName(getJClass());
    }

    public int hashCode() {
        return d0.z.a.getJavaObjectType(this).hashCode();
    }

    public String toString() {
        return getJClass().toString() + " (Kotlin reflection is not available)";
    }
}
