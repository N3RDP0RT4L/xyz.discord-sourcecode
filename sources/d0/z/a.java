package d0.z;

import com.swift.sandhook.annotation.MethodReflectParams;
import d0.e0.c;
import d0.z.d.a0;
import d0.z.d.e;
import d0.z.d.m;
import java.lang.annotation.Annotation;
import java.util.Objects;
/* compiled from: JvmClassMapping.kt */
/* loaded from: classes3.dex */
public final class a {
    public static final <T extends Annotation> c<? extends T> getAnnotationClass(T t) {
        m.checkNotNullParameter(t, "$this$annotationClass");
        Class<? extends Annotation> annotationType = t.annotationType();
        m.checkNotNullExpressionValue(annotationType, "(this as java.lang.annot…otation).annotationType()");
        c<? extends T> kotlinClass = getKotlinClass(annotationType);
        Objects.requireNonNull(kotlinClass, "null cannot be cast to non-null type kotlin.reflect.KClass<out T>");
        return kotlinClass;
    }

    public static final <T> Class<T> getJavaClass(c<T> cVar) {
        m.checkNotNullParameter(cVar, "$this$java");
        Class<T> cls = (Class<T>) ((e) cVar).getJClass();
        Objects.requireNonNull(cls, "null cannot be cast to non-null type java.lang.Class<T>");
        return cls;
    }

    public static final <T> Class<T> getJavaObjectType(c<T> cVar) {
        m.checkNotNullParameter(cVar, "$this$javaObjectType");
        Class<T> cls = (Class<T>) ((e) cVar).getJClass();
        if (!cls.isPrimitive()) {
            return cls;
        }
        String name = cls.getName();
        switch (name.hashCode()) {
            case -1325958191:
                return name.equals(MethodReflectParams.DOUBLE) ? Double.class : cls;
            case 104431:
                return name.equals(MethodReflectParams.INT) ? Integer.class : cls;
            case 3039496:
                return name.equals(MethodReflectParams.BYTE) ? Byte.class : cls;
            case 3052374:
                return name.equals(MethodReflectParams.CHAR) ? Character.class : cls;
            case 3327612:
                return name.equals("long") ? Long.class : cls;
            case 3625364:
                return name.equals("void") ? Void.class : cls;
            case 64711720:
                return name.equals(MethodReflectParams.BOOLEAN) ? Boolean.class : cls;
            case 97526364:
                return name.equals(MethodReflectParams.FLOAT) ? Float.class : cls;
            case 109413500:
                return name.equals(MethodReflectParams.SHORT) ? Short.class : cls;
            default:
                return cls;
        }
    }

    public static final <T> Class<T> getJavaPrimitiveType(c<T> cVar) {
        m.checkNotNullParameter(cVar, "$this$javaPrimitiveType");
        Class<T> cls = (Class<T>) ((e) cVar).getJClass();
        if (cls.isPrimitive()) {
            return cls;
        }
        String name = cls.getName();
        switch (name.hashCode()) {
            case -2056817302:
                if (name.equals("java.lang.Integer")) {
                    return Integer.TYPE;
                }
                break;
            case -527879800:
                if (name.equals("java.lang.Float")) {
                    return Float.TYPE;
                }
                break;
            case -515992664:
                if (name.equals("java.lang.Short")) {
                    return Short.TYPE;
                }
                break;
            case 155276373:
                if (name.equals("java.lang.Character")) {
                    return Character.TYPE;
                }
                break;
            case 344809556:
                if (name.equals("java.lang.Boolean")) {
                    return Boolean.TYPE;
                }
                break;
            case 398507100:
                if (name.equals("java.lang.Byte")) {
                    return Byte.TYPE;
                }
                break;
            case 398795216:
                if (name.equals("java.lang.Long")) {
                    return Long.TYPE;
                }
                break;
            case 399092968:
                if (name.equals("java.lang.Void")) {
                    return Void.TYPE;
                }
                break;
            case 761287205:
                if (name.equals("java.lang.Double")) {
                    return Double.TYPE;
                }
                break;
        }
        return null;
    }

    public static final <T> c<T> getKotlinClass(Class<T> cls) {
        m.checkNotNullParameter(cls, "$this$kotlin");
        return a0.getOrCreateKotlinClass(cls);
    }
}
