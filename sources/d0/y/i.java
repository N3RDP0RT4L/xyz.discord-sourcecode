package d0.y;

import d0.z.d.m;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
/* compiled from: ReadWrite.kt */
/* loaded from: classes3.dex */
public final class i {
    public static final long copyTo(Reader reader, Writer writer, int i) {
        m.checkNotNullParameter(reader, "$this$copyTo");
        m.checkNotNullParameter(writer, "out");
        char[] cArr = new char[i];
        int read = reader.read(cArr);
        long j = 0;
        while (read >= 0) {
            writer.write(cArr, 0, read);
            j += read;
            read = reader.read(cArr);
        }
        return j;
    }

    public static /* synthetic */ long copyTo$default(Reader reader, Writer writer, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 8192;
        }
        return copyTo(reader, writer, i);
    }

    public static final String readText(Reader reader) {
        m.checkNotNullParameter(reader, "$this$readText");
        StringWriter stringWriter = new StringWriter();
        copyTo$default(reader, stringWriter, 0, 2, null);
        String stringWriter2 = stringWriter.toString();
        m.checkNotNullExpressionValue(stringWriter2, "buffer.toString()");
        return stringWriter2;
    }
}
