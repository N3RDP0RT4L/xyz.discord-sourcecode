package d0.y;

import d0.z.d.m;
import java.io.File;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Iterator;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.io.AccessDeniedException;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.sequences.Sequence;
/* compiled from: FileTreeWalk.kt */
/* loaded from: classes3.dex */
public final class d implements Sequence<File> {
    public final File a;

    /* renamed from: b  reason: collision with root package name */
    public final e f3560b;
    public final Function1<File, Boolean> c = null;
    public final Function1<File, Unit> d = null;
    public final Function2<File, IOException, Unit> e = null;
    public final int f = Integer.MAX_VALUE;

    /* compiled from: FileTreeWalk.kt */
    /* loaded from: classes3.dex */
    public static abstract class a extends c {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(File file) {
            super(file);
            m.checkNotNullParameter(file, "rootDir");
        }
    }

    /* compiled from: FileTreeWalk.kt */
    /* loaded from: classes3.dex */
    public final class b extends d0.t.b<File> {
        public final ArrayDeque<c> l;

        /* compiled from: FileTreeWalk.kt */
        /* loaded from: classes3.dex */
        public final class a extends a {

            /* renamed from: b  reason: collision with root package name */
            public boolean f3561b;
            public File[] c;
            public int d;
            public boolean e;
            public final /* synthetic */ b f;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public a(b bVar, File file) {
                super(file);
                m.checkNotNullParameter(file, "rootDir");
                this.f = bVar;
            }

            @Override // d0.y.d.c
            public File step() {
                if (!this.e && this.c == null) {
                    Function1 function1 = d.this.c;
                    if (function1 != null && !((Boolean) function1.invoke(getRoot())).booleanValue()) {
                        return null;
                    }
                    File[] listFiles = getRoot().listFiles();
                    this.c = listFiles;
                    if (listFiles == null) {
                        Function2 function2 = d.this.e;
                        if (function2 != null) {
                            Unit unit = (Unit) function2.invoke(getRoot(), new AccessDeniedException(getRoot(), null, "Cannot list files in a directory", 2, null));
                        }
                        this.e = true;
                    }
                }
                File[] fileArr = this.c;
                if (fileArr != null) {
                    int i = this.d;
                    m.checkNotNull(fileArr);
                    if (i < fileArr.length) {
                        File[] fileArr2 = this.c;
                        m.checkNotNull(fileArr2);
                        int i2 = this.d;
                        this.d = i2 + 1;
                        return fileArr2[i2];
                    }
                }
                if (!this.f3561b) {
                    this.f3561b = true;
                    return getRoot();
                }
                Function1 function12 = d.this.d;
                if (function12 != null) {
                    Unit unit2 = (Unit) function12.invoke(getRoot());
                }
                return null;
            }
        }

        /* compiled from: FileTreeWalk.kt */
        /* renamed from: d0.y.d$b$b  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public final class C0382b extends c {

            /* renamed from: b  reason: collision with root package name */
            public boolean f3562b;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C0382b(b bVar, File file) {
                super(file);
                m.checkNotNullParameter(file, "rootFile");
            }

            @Override // d0.y.d.c
            public File step() {
                if (this.f3562b) {
                    return null;
                }
                this.f3562b = true;
                return getRoot();
            }
        }

        /* compiled from: FileTreeWalk.kt */
        /* loaded from: classes3.dex */
        public final class c extends a {

            /* renamed from: b  reason: collision with root package name */
            public boolean f3563b;
            public File[] c;
            public int d;
            public final /* synthetic */ b e;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public c(b bVar, File file) {
                super(file);
                m.checkNotNullParameter(file, "rootDir");
                this.e = bVar;
            }

            /* JADX WARN: Code restructure failed: missing block: B:29:0x0085, code lost:
                if (r0.length == 0) goto L30;
             */
            @Override // d0.y.d.c
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public java.io.File step() {
                /*
                    r10 = this;
                    boolean r0 = r10.f3563b
                    r1 = 0
                    if (r0 != 0) goto L28
                    d0.y.d$b r0 = r10.e
                    d0.y.d r0 = d0.y.d.this
                    kotlin.jvm.functions.Function1 r0 = d0.y.d.access$getOnEnter$p(r0)
                    if (r0 == 0) goto L20
                    java.io.File r2 = r10.getRoot()
                    java.lang.Object r0 = r0.invoke(r2)
                    java.lang.Boolean r0 = (java.lang.Boolean) r0
                    boolean r0 = r0.booleanValue()
                    if (r0 != 0) goto L20
                    return r1
                L20:
                    r0 = 1
                    r10.f3563b = r0
                    java.io.File r0 = r10.getRoot()
                    return r0
                L28:
                    java.io.File[] r0 = r10.c
                    if (r0 == 0) goto L4a
                    int r2 = r10.d
                    d0.z.d.m.checkNotNull(r0)
                    int r0 = r0.length
                    if (r2 >= r0) goto L35
                    goto L4a
                L35:
                    d0.y.d$b r0 = r10.e
                    d0.y.d r0 = d0.y.d.this
                    kotlin.jvm.functions.Function1 r0 = d0.y.d.access$getOnLeave$p(r0)
                    if (r0 == 0) goto L49
                    java.io.File r2 = r10.getRoot()
                    java.lang.Object r0 = r0.invoke(r2)
                    kotlin.Unit r0 = (kotlin.Unit) r0
                L49:
                    return r1
                L4a:
                    java.io.File[] r0 = r10.c
                    if (r0 != 0) goto L9c
                    java.io.File r0 = r10.getRoot()
                    java.io.File[] r0 = r0.listFiles()
                    r10.c = r0
                    if (r0 != 0) goto L7d
                    d0.y.d$b r0 = r10.e
                    d0.y.d r0 = d0.y.d.this
                    kotlin.jvm.functions.Function2 r0 = d0.y.d.access$getOnFail$p(r0)
                    if (r0 == 0) goto L7d
                    java.io.File r2 = r10.getRoot()
                    kotlin.io.AccessDeniedException r9 = new kotlin.io.AccessDeniedException
                    java.io.File r4 = r10.getRoot()
                    r5 = 0
                    r7 = 2
                    r8 = 0
                    java.lang.String r6 = "Cannot list files in a directory"
                    r3 = r9
                    r3.<init>(r4, r5, r6, r7, r8)
                    java.lang.Object r0 = r0.invoke(r2, r9)
                    kotlin.Unit r0 = (kotlin.Unit) r0
                L7d:
                    java.io.File[] r0 = r10.c
                    if (r0 == 0) goto L87
                    d0.z.d.m.checkNotNull(r0)
                    int r0 = r0.length
                    if (r0 != 0) goto L9c
                L87:
                    d0.y.d$b r0 = r10.e
                    d0.y.d r0 = d0.y.d.this
                    kotlin.jvm.functions.Function1 r0 = d0.y.d.access$getOnLeave$p(r0)
                    if (r0 == 0) goto L9b
                    java.io.File r2 = r10.getRoot()
                    java.lang.Object r0 = r0.invoke(r2)
                    kotlin.Unit r0 = (kotlin.Unit) r0
                L9b:
                    return r1
                L9c:
                    java.io.File[] r0 = r10.c
                    d0.z.d.m.checkNotNull(r0)
                    int r1 = r10.d
                    int r2 = r1 + 1
                    r10.d = r2
                    r0 = r0[r1]
                    return r0
                */
                throw new UnsupportedOperationException("Method not decompiled: d0.y.d.b.c.step():java.io.File");
            }
        }

        public b() {
            ArrayDeque<c> arrayDeque = new ArrayDeque<>();
            this.l = arrayDeque;
            if (d.this.a.isDirectory()) {
                arrayDeque.push(c(d.this.a));
            } else if (d.this.a.isFile()) {
                arrayDeque.push(new C0382b(this, d.this.a));
            } else {
                this.j = 3;
            }
        }

        /* JADX WARN: Code restructure failed: missing block: B:16:0x0042, code lost:
            r3.k = r1;
            r3.j = 1;
         */
        /* JADX WARN: Code restructure failed: missing block: B:28:?, code lost:
            return;
         */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r1v0 */
        /* JADX WARN: Type inference failed for: r1v2, types: [java.lang.Object, java.io.File] */
        @Override // d0.t.b
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public void a() {
            /*
                r3 = this;
            L0:
                java.util.ArrayDeque<d0.y.d$c> r0 = r3.l
                java.lang.Object r0 = r0.peek()
                d0.y.d$c r0 = (d0.y.d.c) r0
                if (r0 == 0) goto L3f
                java.io.File r1 = r0.step()
                if (r1 != 0) goto L16
                java.util.ArrayDeque<d0.y.d$c> r0 = r3.l
                r0.pop()
                goto L0
            L16:
                java.io.File r0 = r0.getRoot()
                boolean r0 = d0.z.d.m.areEqual(r1, r0)
                if (r0 != 0) goto L40
                boolean r0 = r1.isDirectory()
                if (r0 == 0) goto L40
                java.util.ArrayDeque<d0.y.d$c> r0 = r3.l
                int r0 = r0.size()
                d0.y.d r2 = d0.y.d.this
                int r2 = d0.y.d.access$getMaxDepth$p(r2)
                if (r0 < r2) goto L35
                goto L40
            L35:
                java.util.ArrayDeque<d0.y.d$c> r0 = r3.l
                d0.y.d$a r1 = r3.c(r1)
                r0.push(r1)
                goto L0
            L3f:
                r1 = 0
            L40:
                if (r1 == 0) goto L48
                r3.k = r1
                r0 = 1
                r3.j = r0
                goto L4b
            L48:
                r0 = 3
                r3.j = r0
            L4b:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.y.d.b.a():void");
        }

        public final a c(File file) {
            int ordinal = d.this.f3560b.ordinal();
            if (ordinal == 0) {
                return new c(this, file);
            }
            if (ordinal == 1) {
                return new a(this, file);
            }
            throw new NoWhenBranchMatchedException();
        }
    }

    /* compiled from: FileTreeWalk.kt */
    /* loaded from: classes3.dex */
    public static abstract class c {
        public final File a;

        public c(File file) {
            m.checkNotNullParameter(file, "root");
            this.a = file;
        }

        public final File getRoot() {
            return this.a;
        }

        public abstract File step();
    }

    public d(File file, e eVar) {
        m.checkNotNullParameter(file, "start");
        m.checkNotNullParameter(eVar, "direction");
        this.a = file;
        this.f3560b = eVar;
    }

    @Override // kotlin.sequences.Sequence
    public Iterator<File> iterator() {
        return new b();
    }
}
