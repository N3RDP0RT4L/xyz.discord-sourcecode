package d0.y;

import java.io.Closeable;
/* compiled from: Closeable.kt */
/* loaded from: classes3.dex */
public final class b {
    public static final void closeFinally(Closeable closeable, Throwable th) {
        if (closeable != null) {
            if (th == null) {
                closeable.close();
                return;
            }
            try {
                closeable.close();
            } catch (Throwable th2) {
                d0.b.addSuppressed(th, th2);
            }
        }
    }
}
