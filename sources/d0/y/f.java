package d0.y;

import d0.g0.c;
import d0.z.d.m;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
/* compiled from: FileReadWrite.kt */
/* loaded from: classes3.dex */
public class f {
    public static final String readText(File file, Charset charset) {
        InputStreamReader inputStreamReader;
        m.checkNotNullParameter(file, "$this$readText");
        m.checkNotNullParameter(charset, "charset");
        try {
            th = null;
            return i.readText(new InputStreamReader(new FileInputStream(file), charset));
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    public static /* synthetic */ String readText$default(File file, Charset charset, int i, Object obj) {
        if ((i & 1) != 0) {
            charset = c.a;
        }
        return readText(file, charset);
    }
}
