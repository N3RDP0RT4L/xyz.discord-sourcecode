package d0;

import andhook.lib.xposed.ClassUtils;
import d0.z.d.m;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: KotlinVersion.kt */
/* loaded from: classes3.dex */
public final class e implements Comparable<e> {
    public static final e j = f.get();
    public final int k;
    public final int l;
    public final int m;
    public final int n;

    /* compiled from: KotlinVersion.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    static {
        new a(null);
    }

    public e(int i, int i2, int i3) {
        this.l = i;
        this.m = i2;
        this.n = i3;
        if (i >= 0 && 255 >= i && i2 >= 0 && 255 >= i2 && i3 >= 0 && 255 >= i3) {
            this.k = (i << 16) + (i2 << 8) + i3;
            return;
        }
        throw new IllegalArgumentException(("Version components are out of range: " + i + ClassUtils.PACKAGE_SEPARATOR_CHAR + i2 + ClassUtils.PACKAGE_SEPARATOR_CHAR + i3).toString());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof e)) {
            obj = null;
        }
        e eVar = (e) obj;
        return eVar != null && this.k == eVar.k;
    }

    public int hashCode() {
        return this.k;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.l);
        sb.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
        sb.append(this.m);
        sb.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
        sb.append(this.n);
        return sb.toString();
    }

    public int compareTo(e eVar) {
        m.checkNotNullParameter(eVar, "other");
        return this.k - eVar.k;
    }
}
