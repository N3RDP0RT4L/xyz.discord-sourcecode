package d0.g0;

import d0.d0.f;
import d0.z.d.m;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import kotlin.Pair;
import kotlin.jvm.functions.Function2;
import kotlin.ranges.IntRange;
import kotlin.sequences.Sequence;
/* compiled from: Strings.kt */
/* loaded from: classes3.dex */
public final class d implements Sequence<IntRange> {
    public final CharSequence a;

    /* renamed from: b  reason: collision with root package name */
    public final int f3552b;
    public final int c;
    public final Function2<CharSequence, Integer, Pair<Integer, Integer>> d;

    /* compiled from: Strings.kt */
    /* loaded from: classes3.dex */
    public static final class a implements Iterator<IntRange>, d0.z.d.g0.a {
        public int j = -1;
        public int k;
        public int l;
        public IntRange m;
        public int n;

        public a() {
            int coerceIn = f.coerceIn(d.this.f3552b, 0, d.this.a.length());
            this.k = coerceIn;
            this.l = coerceIn;
        }

        /* JADX WARN: Code restructure failed: missing block: B:8:0x0021, code lost:
            if (r0 < r6.o.c) goto L9;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public final void a() {
            /*
                r6 = this;
                int r0 = r6.l
                r1 = 0
                if (r0 >= 0) goto Lc
                r6.j = r1
                r0 = 0
                r6.m = r0
                goto L9e
            Lc:
                d0.g0.d r0 = d0.g0.d.this
                int r0 = d0.g0.d.access$getLimit$p(r0)
                r2 = -1
                r3 = 1
                if (r0 <= 0) goto L23
                int r0 = r6.n
                int r0 = r0 + r3
                r6.n = r0
                d0.g0.d r4 = d0.g0.d.this
                int r4 = d0.g0.d.access$getLimit$p(r4)
                if (r0 >= r4) goto L31
            L23:
                int r0 = r6.l
                d0.g0.d r4 = d0.g0.d.this
                java.lang.CharSequence r4 = d0.g0.d.access$getInput$p(r4)
                int r4 = r4.length()
                if (r0 <= r4) goto L47
            L31:
                int r0 = r6.k
                kotlin.ranges.IntRange r1 = new kotlin.ranges.IntRange
                d0.g0.d r4 = d0.g0.d.this
                java.lang.CharSequence r4 = d0.g0.d.access$getInput$p(r4)
                int r4 = d0.g0.w.getLastIndex(r4)
                r1.<init>(r0, r4)
                r6.m = r1
                r6.l = r2
                goto L9c
            L47:
                d0.g0.d r0 = d0.g0.d.this
                kotlin.jvm.functions.Function2 r0 = d0.g0.d.access$getGetNextMatch$p(r0)
                d0.g0.d r4 = d0.g0.d.this
                java.lang.CharSequence r4 = d0.g0.d.access$getInput$p(r4)
                int r5 = r6.l
                java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
                java.lang.Object r0 = r0.invoke(r4, r5)
                kotlin.Pair r0 = (kotlin.Pair) r0
                if (r0 != 0) goto L77
                int r0 = r6.k
                kotlin.ranges.IntRange r1 = new kotlin.ranges.IntRange
                d0.g0.d r4 = d0.g0.d.this
                java.lang.CharSequence r4 = d0.g0.d.access$getInput$p(r4)
                int r4 = d0.g0.w.getLastIndex(r4)
                r1.<init>(r0, r4)
                r6.m = r1
                r6.l = r2
                goto L9c
            L77:
                java.lang.Object r2 = r0.component1()
                java.lang.Number r2 = (java.lang.Number) r2
                int r2 = r2.intValue()
                java.lang.Object r0 = r0.component2()
                java.lang.Number r0 = (java.lang.Number) r0
                int r0 = r0.intValue()
                int r4 = r6.k
                kotlin.ranges.IntRange r4 = d0.d0.f.until(r4, r2)
                r6.m = r4
                int r2 = r2 + r0
                r6.k = r2
                if (r0 != 0) goto L99
                r1 = 1
            L99:
                int r2 = r2 + r1
                r6.l = r2
            L9c:
                r6.j = r3
            L9e:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.g0.d.a.a():void");
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            if (this.j == -1) {
                a();
            }
            return this.j == 1;
        }

        @Override // java.util.Iterator
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.Iterator
        public IntRange next() {
            if (this.j == -1) {
                a();
            }
            if (this.j != 0) {
                IntRange intRange = this.m;
                Objects.requireNonNull(intRange, "null cannot be cast to non-null type kotlin.ranges.IntRange");
                this.m = null;
                this.j = -1;
                return intRange;
            }
            throw new NoSuchElementException();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public d(CharSequence charSequence, int i, int i2, Function2<? super CharSequence, ? super Integer, Pair<Integer, Integer>> function2) {
        m.checkNotNullParameter(charSequence, "input");
        m.checkNotNullParameter(function2, "getNextMatch");
        this.a = charSequence;
        this.f3552b = i;
        this.c = i2;
        this.d = function2;
    }

    @Override // kotlin.sequences.Sequence
    public Iterator<IntRange> iterator() {
        return new a();
    }
}
