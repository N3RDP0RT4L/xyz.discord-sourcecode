package d0.g0;

import d0.z.d.m;
/* compiled from: StringBuilderJVM.kt */
/* loaded from: classes3.dex */
public class p extends o {
    public static final StringBuilder clear(StringBuilder sb) {
        m.checkNotNullParameter(sb, "$this$clear");
        sb.setLength(0);
        return sb;
    }
}
