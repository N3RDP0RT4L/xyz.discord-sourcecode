package d0.g0;

import b.d.b.a.a;
import d0.z.d.m;
import kotlin.ranges.IntRange;
/* compiled from: Regex.kt */
/* loaded from: classes3.dex */
public final class e {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final IntRange f3553b;

    public e(String str, IntRange intRange) {
        m.checkNotNullParameter(str, "value");
        m.checkNotNullParameter(intRange, "range");
        this.a = str;
        this.f3553b = intRange;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof e)) {
            return false;
        }
        e eVar = (e) obj;
        return m.areEqual(this.a, eVar.a) && m.areEqual(this.f3553b, eVar.f3553b);
    }

    public final IntRange getRange() {
        return this.f3553b;
    }

    public final String getValue() {
        return this.a;
    }

    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        IntRange intRange = this.f3553b;
        if (intRange != null) {
            i = intRange.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = a.R("MatchGroup(value=");
        R.append(this.a);
        R.append(", range=");
        R.append(this.f3553b);
        R.append(")");
        return R.toString();
    }
}
