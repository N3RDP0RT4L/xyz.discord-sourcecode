package d0.g0;

import d0.z.d.m;
import kotlin.jvm.functions.Function1;
/* compiled from: Appendable.kt */
/* loaded from: classes3.dex */
public class l {
    public static final <T> void appendElement(Appendable appendable, T t, Function1<? super T, ? extends CharSequence> function1) {
        m.checkNotNullParameter(appendable, "$this$appendElement");
        if (function1 != null) {
            appendable.append(function1.invoke(t));
            return;
        }
        if (t != null ? t instanceof CharSequence : true) {
            appendable.append((CharSequence) t);
        } else if (t instanceof Character) {
            appendable.append(((Character) t).charValue());
        } else {
            appendable.append(String.valueOf(t));
        }
    }
}
