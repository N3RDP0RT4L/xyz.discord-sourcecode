package d0.g0;

import com.adjust.sdk.Constants;
import d0.z.d.m;
import java.nio.charset.Charset;
/* compiled from: Charsets.kt */
/* loaded from: classes3.dex */
public final class c {
    public static final Charset a;

    /* renamed from: b  reason: collision with root package name */
    public static Charset f3551b;
    public static Charset c;
    public static final c d = new c();

    static {
        Charset forName = Charset.forName(Constants.ENCODING);
        m.checkNotNullExpressionValue(forName, "Charset.forName(\"UTF-8\")");
        a = forName;
        m.checkNotNullExpressionValue(Charset.forName("UTF-16"), "Charset.forName(\"UTF-16\")");
        m.checkNotNullExpressionValue(Charset.forName("UTF-16BE"), "Charset.forName(\"UTF-16BE\")");
        m.checkNotNullExpressionValue(Charset.forName("UTF-16LE"), "Charset.forName(\"UTF-16LE\")");
        m.checkNotNullExpressionValue(Charset.forName("US-ASCII"), "Charset.forName(\"US-ASCII\")");
        m.checkNotNullExpressionValue(Charset.forName("ISO-8859-1"), "Charset.forName(\"ISO-8859-1\")");
    }

    public final Charset UTF32_BE() {
        Charset charset = c;
        if (charset != null) {
            return charset;
        }
        Charset forName = Charset.forName("UTF-32BE");
        m.checkNotNullExpressionValue(forName, "Charset.forName(\"UTF-32BE\")");
        c = forName;
        return forName;
    }

    public final Charset UTF32_LE() {
        Charset charset = f3551b;
        if (charset != null) {
            return charset;
        }
        Charset forName = Charset.forName("UTF-32LE");
        m.checkNotNullExpressionValue(forName, "Charset.forName(\"UTF-32LE\")");
        f3551b = forName;
        return forName;
    }
}
