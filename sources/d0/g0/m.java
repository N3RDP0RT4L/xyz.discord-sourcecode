package d0.g0;

import d0.t.n;
import d0.t.u;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import kotlin.jvm.functions.Function1;
/* compiled from: Indent.kt */
/* loaded from: classes3.dex */
public class m extends l {

    /* compiled from: Indent.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<String, String> {
        public static final a j = new a();

        public a() {
            super(1);
        }

        public final String invoke(String str) {
            d0.z.d.m.checkNotNullParameter(str, "line");
            return str;
        }
    }

    /* compiled from: Indent.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function1<String, String> {
        public final /* synthetic */ String $indent;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(String str) {
            super(1);
            this.$indent = str;
        }

        public final String invoke(String str) {
            d0.z.d.m.checkNotNullParameter(str, "line");
            return b.d.b.a.a.H(new StringBuilder(), this.$indent, str);
        }
    }

    public static final Function1<String, String> a(String str) {
        return str.length() == 0 ? a.j : new b(str);
    }

    public static final String replaceIndent(String str, String str2) {
        int i;
        String invoke;
        d0.z.d.m.checkNotNullParameter(str, "$this$replaceIndent");
        d0.z.d.m.checkNotNullParameter(str2, "newIndent");
        List<String> lines = w.lines(str);
        ArrayList arrayList = new ArrayList();
        for (Object obj : lines) {
            if (!t.isBlank((String) obj)) {
                arrayList.add(obj);
            }
        }
        ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(arrayList, 10));
        Iterator it = arrayList.iterator();
        while (true) {
            i = 0;
            if (!it.hasNext()) {
                break;
            }
            String str3 = (String) it.next();
            int length = str3.length();
            while (true) {
                if (i >= length) {
                    i = -1;
                    break;
                } else if (!d0.g0.a.isWhitespace(str3.charAt(i))) {
                    break;
                } else {
                    i++;
                }
            }
            if (i == -1) {
                i = str3.length();
            }
            arrayList2.add(Integer.valueOf(i));
        }
        Integer num = (Integer) u.minOrNull(arrayList2);
        int intValue = num != null ? num.intValue() : 0;
        int size = (lines.size() * str2.length()) + str.length();
        Function1<String, String> a2 = a(str2);
        int lastIndex = n.getLastIndex(lines);
        ArrayList arrayList3 = new ArrayList();
        for (Object obj2 : lines) {
            i++;
            if (i < 0) {
                n.throwIndexOverflow();
            }
            String str4 = (String) obj2;
            if ((i == 0 || i == lastIndex) && t.isBlank(str4)) {
                str4 = null;
            } else {
                String drop = y.drop(str4, intValue);
                if (!(drop == null || (invoke = a2.invoke(drop)) == null)) {
                    str4 = invoke;
                }
            }
            if (str4 != null) {
                arrayList3.add(str4);
            }
        }
        String sb = ((StringBuilder) u.joinTo$default(arrayList3, new StringBuilder(size), "\n", null, null, 0, null, null, 124, null)).toString();
        d0.z.d.m.checkNotNullExpressionValue(sb, "mapIndexedNotNull { inde…\"\\n\")\n        .toString()");
        return sb;
    }

    public static final String replaceIndentByMargin(String str, String str2, String str3) {
        int i;
        String invoke;
        d0.z.d.m.checkNotNullParameter(str, "$this$replaceIndentByMargin");
        d0.z.d.m.checkNotNullParameter(str2, "newIndent");
        d0.z.d.m.checkNotNullParameter(str3, "marginPrefix");
        if (!t.isBlank(str3)) {
            List<String> lines = w.lines(str);
            int size = (lines.size() * str2.length()) + str.length();
            Function1<String, String> a2 = a(str2);
            int lastIndex = n.getLastIndex(lines);
            ArrayList arrayList = new ArrayList();
            int i2 = 0;
            for (Object obj : lines) {
                i2++;
                if (i2 < 0) {
                    n.throwIndexOverflow();
                }
                String str4 = (String) obj;
                String str5 = null;
                if (!(i2 == 0 || i2 == lastIndex) || !t.isBlank(str4)) {
                    int length = str4.length();
                    int i3 = 0;
                    while (true) {
                        if (i3 >= length) {
                            i = -1;
                            break;
                        } else if (!d0.g0.a.isWhitespace(str4.charAt(i3))) {
                            i = i3;
                            break;
                        } else {
                            i3++;
                        }
                    }
                    if (i != -1) {
                        int i4 = i;
                        if (t.startsWith$default(str4, str3, i, false, 4, null)) {
                            Objects.requireNonNull(str4, "null cannot be cast to non-null type java.lang.String");
                            str5 = str4.substring(str3.length() + i4);
                            d0.z.d.m.checkNotNullExpressionValue(str5, "(this as java.lang.String).substring(startIndex)");
                        }
                    }
                    if (!(str5 == null || (invoke = a2.invoke(str5)) == null)) {
                        str4 = invoke;
                    }
                    str5 = str4;
                }
                if (str5 != null) {
                    arrayList.add(str5);
                }
            }
            String sb = ((StringBuilder) u.joinTo$default(arrayList, new StringBuilder(size), "\n", null, null, 0, null, null, 124, null)).toString();
            d0.z.d.m.checkNotNullExpressionValue(sb, "mapIndexedNotNull { inde…\"\\n\")\n        .toString()");
            return sb;
        }
        throw new IllegalArgumentException("marginPrefix must be non-blank string.".toString());
    }

    public static final String trimIndent(String str) {
        d0.z.d.m.checkNotNullParameter(str, "$this$trimIndent");
        return replaceIndent(str, "");
    }

    public static final String trimMargin(String str, String str2) {
        d0.z.d.m.checkNotNullParameter(str, "$this$trimMargin");
        d0.z.d.m.checkNotNullParameter(str2, "marginPrefix");
        return replaceIndentByMargin(str, "", str2);
    }

    public static /* synthetic */ String trimMargin$default(String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str2 = "|";
        }
        return trimMargin(str, str2);
    }
}
