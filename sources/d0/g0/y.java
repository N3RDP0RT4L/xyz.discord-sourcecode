package d0.g0;

import d0.d0.f;
import d0.t.p0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import kotlin.jvm.functions.Function1;
/* compiled from: _Strings.kt */
/* loaded from: classes3.dex */
public class y extends x {

    /* compiled from: _Strings.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<CharSequence, String> {
        public static final a j = new a();

        public a() {
            super(1);
        }

        public final String invoke(CharSequence charSequence) {
            m.checkNotNullParameter(charSequence, "it");
            return charSequence.toString();
        }
    }

    public static final List<String> chunked(CharSequence charSequence, int i) {
        m.checkNotNullParameter(charSequence, "$this$chunked");
        return windowed(charSequence, i, i, true);
    }

    public static final CharSequence drop(CharSequence charSequence, int i) {
        m.checkNotNullParameter(charSequence, "$this$drop");
        if (i >= 0) {
            return charSequence.subSequence(f.coerceAtMost(i, charSequence.length()), charSequence.length());
        }
        throw new IllegalArgumentException(b.d.b.a.a.q("Requested character count ", i, " is less than zero.").toString());
    }

    public static final String dropLast(String str, int i) {
        m.checkNotNullParameter(str, "$this$dropLast");
        if (i >= 0) {
            return take(str, f.coerceAtLeast(str.length() - i, 0));
        }
        throw new IllegalArgumentException(b.d.b.a.a.q("Requested character count ", i, " is less than zero.").toString());
    }

    public static final char first(CharSequence charSequence) {
        m.checkNotNullParameter(charSequence, "$this$first");
        if (!(charSequence.length() == 0)) {
            return charSequence.charAt(0);
        }
        throw new NoSuchElementException("Char sequence is empty.");
    }

    public static final Character firstOrNull(CharSequence charSequence) {
        m.checkNotNullParameter(charSequence, "$this$firstOrNull");
        if (charSequence.length() == 0) {
            return null;
        }
        return Character.valueOf(charSequence.charAt(0));
    }

    public static final Character getOrNull(CharSequence charSequence, int i) {
        m.checkNotNullParameter(charSequence, "$this$getOrNull");
        if (i < 0 || i > w.getLastIndex(charSequence)) {
            return null;
        }
        return Character.valueOf(charSequence.charAt(i));
    }

    public static final char last(CharSequence charSequence) {
        m.checkNotNullParameter(charSequence, "$this$last");
        if (!(charSequence.length() == 0)) {
            return charSequence.charAt(w.getLastIndex(charSequence));
        }
        throw new NoSuchElementException("Char sequence is empty.");
    }

    public static final Character singleOrNull(CharSequence charSequence) {
        m.checkNotNullParameter(charSequence, "$this$singleOrNull");
        if (charSequence.length() == 1) {
            return Character.valueOf(charSequence.charAt(0));
        }
        return null;
    }

    public static final String take(String str, int i) {
        m.checkNotNullParameter(str, "$this$take");
        if (i >= 0) {
            String substring = str.substring(0, f.coerceAtMost(i, str.length()));
            m.checkNotNullExpressionValue(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            return substring;
        }
        throw new IllegalArgumentException(b.d.b.a.a.q("Requested character count ", i, " is less than zero.").toString());
    }

    public static final String takeLast(String str, int i) {
        m.checkNotNullParameter(str, "$this$takeLast");
        if (i >= 0) {
            int length = str.length();
            String substring = str.substring(length - f.coerceAtMost(i, length));
            m.checkNotNullExpressionValue(substring, "(this as java.lang.String).substring(startIndex)");
            return substring;
        }
        throw new IllegalArgumentException(b.d.b.a.a.q("Requested character count ", i, " is less than zero.").toString());
    }

    public static final List<String> windowed(CharSequence charSequence, int i, int i2, boolean z2) {
        m.checkNotNullParameter(charSequence, "$this$windowed");
        return windowed(charSequence, i, i2, z2, a.j);
    }

    public static final <R> List<R> windowed(CharSequence charSequence, int i, int i2, boolean z2, Function1<? super CharSequence, ? extends R> function1) {
        m.checkNotNullParameter(charSequence, "$this$windowed");
        m.checkNotNullParameter(function1, "transform");
        p0.checkWindowSizeStep(i, i2);
        int length = charSequence.length();
        int i3 = 0;
        ArrayList arrayList = new ArrayList((length / i2) + (length % i2 == 0 ? 0 : 1));
        while (i3 >= 0 && length > i3) {
            int i4 = i3 + i;
            if (i4 < 0 || i4 > length) {
                if (!z2) {
                    break;
                }
                i4 = length;
            }
            arrayList.add(function1.invoke(charSequence.subSequence(i3, i4)));
            i3 += i2;
        }
        return arrayList;
    }

    public static final String drop(String str, int i) {
        m.checkNotNullParameter(str, "$this$drop");
        if (i >= 0) {
            String substring = str.substring(f.coerceAtMost(i, str.length()));
            m.checkNotNullExpressionValue(substring, "(this as java.lang.String).substring(startIndex)");
            return substring;
        }
        throw new IllegalArgumentException(b.d.b.a.a.q("Requested character count ", i, " is less than zero.").toString());
    }
}
