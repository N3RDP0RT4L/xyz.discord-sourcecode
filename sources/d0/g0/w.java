package d0.g0;

import d0.d0.f;
import d0.f0.q;
import d0.t.j;
import d0.t.k;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Pair;
import kotlin.jvm.functions.Function1;
import kotlin.ranges.IntProgression;
import kotlin.ranges.IntRange;
import kotlin.sequences.Sequence;
/* compiled from: Strings.kt */
/* loaded from: classes3.dex */
public class w extends t {

    /* compiled from: Strings.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<IntRange, String> {
        public final /* synthetic */ CharSequence $this_splitToSequence;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(CharSequence charSequence) {
            super(1);
            this.$this_splitToSequence = charSequence;
        }

        public final String invoke(IntRange intRange) {
            m.checkNotNullParameter(intRange, "it");
            return w.substring(this.$this_splitToSequence, intRange);
        }
    }

    public static final Pair access$findAnyOf(CharSequence charSequence, Collection collection, int i, boolean z2, boolean z3) {
        Object obj;
        Object obj2;
        if (z2 || collection.size() != 1) {
            IntProgression intRange = !z3 ? new IntRange(f.coerceAtLeast(i, 0), charSequence.length()) : f.downTo(f.coerceAtMost(i, getLastIndex(charSequence)), 0);
            if (charSequence instanceof String) {
                int first = intRange.getFirst();
                int last = intRange.getLast();
                int step = intRange.getStep();
                if (step >= 0) {
                    if (first > last) {
                        return null;
                    }
                } else if (first < last) {
                    return null;
                }
                while (true) {
                    Iterator it = collection.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            obj2 = null;
                            break;
                        }
                        obj2 = it.next();
                        String str = (String) obj2;
                        if (t.regionMatches(str, 0, (String) charSequence, first, str.length(), z2)) {
                            break;
                        }
                    }
                    String str2 = (String) obj2;
                    if (str2 != null) {
                        return d0.o.to(Integer.valueOf(first), str2);
                    }
                    if (first == last) {
                        return null;
                    }
                    first += step;
                }
            } else {
                int first2 = intRange.getFirst();
                int last2 = intRange.getLast();
                int step2 = intRange.getStep();
                if (step2 >= 0) {
                    if (first2 > last2) {
                        return null;
                    }
                } else if (first2 < last2) {
                    return null;
                }
                while (true) {
                    Iterator it2 = collection.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            obj = null;
                            break;
                        }
                        obj = it2.next();
                        String str3 = (String) obj;
                        if (regionMatchesImpl(str3, 0, charSequence, first2, str3.length(), z2)) {
                            break;
                        }
                    }
                    String str4 = (String) obj;
                    if (str4 != null) {
                        return d0.o.to(Integer.valueOf(first2), str4);
                    }
                    if (first2 == last2) {
                        return null;
                    }
                    first2 += step2;
                }
            }
        } else {
            String str5 = (String) u.single(collection);
            int indexOf$default = !z3 ? indexOf$default(charSequence, str5, i, false, 4, (Object) null) : lastIndexOf$default(charSequence, str5, i, false, 4, (Object) null);
            if (indexOf$default < 0) {
                return null;
            }
            return d0.o.to(Integer.valueOf(indexOf$default), str5);
        }
    }

    public static final int b(CharSequence charSequence, CharSequence charSequence2, int i, int i2, boolean z2, boolean z3) {
        IntProgression intRange = !z3 ? new IntRange(f.coerceAtLeast(i, 0), f.coerceAtMost(i2, charSequence.length())) : f.downTo(f.coerceAtMost(i, getLastIndex(charSequence)), f.coerceAtLeast(i2, 0));
        if (!(charSequence instanceof String) || !(charSequence2 instanceof String)) {
            int first = intRange.getFirst();
            int last = intRange.getLast();
            int step = intRange.getStep();
            if (step >= 0) {
                if (first > last) {
                    return -1;
                }
            } else if (first < last) {
                return -1;
            }
            while (!regionMatchesImpl(charSequence2, 0, charSequence, first, charSequence2.length(), z2)) {
                if (first == last) {
                    return -1;
                }
                first += step;
            }
            return first;
        }
        int first2 = intRange.getFirst();
        int last2 = intRange.getLast();
        int step2 = intRange.getStep();
        if (step2 >= 0) {
            if (first2 > last2) {
                return -1;
            }
        } else if (first2 < last2) {
            return -1;
        }
        while (!t.regionMatches((String) charSequence2, 0, (String) charSequence, first2, charSequence2.length(), z2)) {
            if (first2 == last2) {
                return -1;
            }
            first2 += step2;
        }
        return first2;
    }

    public static /* synthetic */ int c(CharSequence charSequence, CharSequence charSequence2, int i, int i2, boolean z2, boolean z3, int i3) {
        return b(charSequence, charSequence2, i, i2, z2, (i3 & 16) != 0 ? false : z3);
    }

    public static final boolean contains(CharSequence charSequence, CharSequence charSequence2, boolean z2) {
        m.checkNotNullParameter(charSequence, "$this$contains");
        m.checkNotNullParameter(charSequence2, "other");
        if (charSequence2 instanceof String) {
            if (indexOf$default(charSequence, (String) charSequence2, 0, z2, 2, (Object) null) >= 0) {
                return true;
            }
        } else if (c(charSequence, charSequence2, 0, charSequence.length(), z2, false, 16) >= 0) {
            return true;
        }
        return false;
    }

    public static /* synthetic */ boolean contains$default(CharSequence charSequence, CharSequence charSequence2, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        return contains(charSequence, charSequence2, z2);
    }

    public static Sequence d(CharSequence charSequence, String[] strArr, int i, boolean z2, int i2, int i3) {
        if ((i3 & 2) != 0) {
            i = 0;
        }
        if ((i3 & 4) != 0) {
            z2 = false;
        }
        if ((i3 & 8) != 0) {
            i2 = 0;
        }
        requireNonNegativeLimit(i2);
        return new d(charSequence, i, i2, new v(j.asList(strArr), z2));
    }

    public static final List<String> e(CharSequence charSequence, String str, boolean z2, int i) {
        requireNonNegativeLimit(i);
        int i2 = 0;
        int indexOf = indexOf(charSequence, str, 0, z2);
        if (indexOf == -1 || i == 1) {
            return d0.t.m.listOf(charSequence.toString());
        }
        boolean z3 = i > 0;
        int i3 = 10;
        if (z3) {
            i3 = f.coerceAtMost(i, 10);
        }
        ArrayList arrayList = new ArrayList(i3);
        do {
            arrayList.add(charSequence.subSequence(i2, indexOf).toString());
            i2 = str.length() + indexOf;
            if (z3 && arrayList.size() == i - 1) {
                break;
            }
            indexOf = indexOf(charSequence, str, i2, z2);
        } while (indexOf != -1);
        arrayList.add(charSequence.subSequence(i2, charSequence.length()).toString());
        return arrayList;
    }

    public static final boolean endsWith(CharSequence charSequence, char c, boolean z2) {
        m.checkNotNullParameter(charSequence, "$this$endsWith");
        return charSequence.length() > 0 && b.equals(charSequence.charAt(getLastIndex(charSequence)), c, z2);
    }

    public static /* synthetic */ boolean endsWith$default(CharSequence charSequence, char c, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        return endsWith(charSequence, c, z2);
    }

    public static final IntRange getIndices(CharSequence charSequence) {
        m.checkNotNullParameter(charSequence, "$this$indices");
        return new IntRange(0, charSequence.length() - 1);
    }

    public static final int getLastIndex(CharSequence charSequence) {
        m.checkNotNullParameter(charSequence, "$this$lastIndex");
        return charSequence.length() - 1;
    }

    public static final int indexOf(CharSequence charSequence, char c, int i, boolean z2) {
        m.checkNotNullParameter(charSequence, "$this$indexOf");
        if (z2 || !(charSequence instanceof String)) {
            return indexOfAny(charSequence, new char[]{c}, i, z2);
        }
        return ((String) charSequence).indexOf(c, i);
    }

    public static /* synthetic */ int indexOf$default(CharSequence charSequence, char c, int i, boolean z2, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        if ((i2 & 4) != 0) {
            z2 = false;
        }
        return indexOf(charSequence, c, i, z2);
    }

    public static final int indexOfAny(CharSequence charSequence, char[] cArr, int i, boolean z2) {
        boolean z3;
        m.checkNotNullParameter(charSequence, "$this$indexOfAny");
        m.checkNotNullParameter(cArr, "chars");
        if (z2 || cArr.length != 1 || !(charSequence instanceof String)) {
            int coerceAtLeast = f.coerceAtLeast(i, 0);
            int lastIndex = getLastIndex(charSequence);
            if (coerceAtLeast > lastIndex) {
                return -1;
            }
            while (true) {
                char charAt = charSequence.charAt(coerceAtLeast);
                int length = cArr.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        z3 = false;
                        break;
                    } else if (b.equals(cArr[i2], charAt, z2)) {
                        z3 = true;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (z3) {
                    return coerceAtLeast;
                }
                if (coerceAtLeast == lastIndex) {
                    return -1;
                }
                coerceAtLeast++;
            }
        } else {
            return ((String) charSequence).indexOf(k.single(cArr), i);
        }
    }

    public static final int lastIndexOf(CharSequence charSequence, char c, int i, boolean z2) {
        m.checkNotNullParameter(charSequence, "$this$lastIndexOf");
        if (z2 || !(charSequence instanceof String)) {
            return lastIndexOfAny(charSequence, new char[]{c}, i, z2);
        }
        return ((String) charSequence).lastIndexOf(c, i);
    }

    public static /* synthetic */ int lastIndexOf$default(CharSequence charSequence, char c, int i, boolean z2, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = getLastIndex(charSequence);
        }
        if ((i2 & 4) != 0) {
            z2 = false;
        }
        return lastIndexOf(charSequence, c, i, z2);
    }

    public static final int lastIndexOfAny(CharSequence charSequence, char[] cArr, int i, boolean z2) {
        m.checkNotNullParameter(charSequence, "$this$lastIndexOfAny");
        m.checkNotNullParameter(cArr, "chars");
        if (z2 || cArr.length != 1 || !(charSequence instanceof String)) {
            for (int coerceAtMost = f.coerceAtMost(i, getLastIndex(charSequence)); coerceAtMost >= 0; coerceAtMost--) {
                char charAt = charSequence.charAt(coerceAtMost);
                int length = cArr.length;
                boolean z3 = false;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        break;
                    } else if (b.equals(cArr[i2], charAt, z2)) {
                        z3 = true;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (z3) {
                    return coerceAtMost;
                }
            }
            return -1;
        }
        return ((String) charSequence).lastIndexOf(k.single(cArr), i);
    }

    public static final Sequence<String> lineSequence(CharSequence charSequence) {
        m.checkNotNullParameter(charSequence, "$this$lineSequence");
        return splitToSequence$default(charSequence, new String[]{"\r\n", "\n", "\r"}, false, 0, 6, null);
    }

    public static final List<String> lines(CharSequence charSequence) {
        m.checkNotNullParameter(charSequence, "$this$lines");
        return q.toList(lineSequence(charSequence));
    }

    public static final boolean regionMatchesImpl(CharSequence charSequence, int i, CharSequence charSequence2, int i2, int i3, boolean z2) {
        m.checkNotNullParameter(charSequence, "$this$regionMatchesImpl");
        m.checkNotNullParameter(charSequence2, "other");
        if (i2 < 0 || i < 0 || i > charSequence.length() - i3 || i2 > charSequence2.length() - i3) {
            return false;
        }
        for (int i4 = 0; i4 < i3; i4++) {
            if (!b.equals(charSequence.charAt(i + i4), charSequence2.charAt(i2 + i4), z2)) {
                return false;
            }
        }
        return true;
    }

    public static final String removePrefix(String str, CharSequence charSequence) {
        m.checkNotNullParameter(str, "$this$removePrefix");
        m.checkNotNullParameter(charSequence, "prefix");
        if (!startsWith$default((CharSequence) str, charSequence, false, 2, (Object) null)) {
            return str;
        }
        String substring = str.substring(charSequence.length());
        m.checkNotNullExpressionValue(substring, "(this as java.lang.String).substring(startIndex)");
        return substring;
    }

    public static final String removeSuffix(String str, CharSequence charSequence) {
        m.checkNotNullParameter(str, "$this$removeSuffix");
        m.checkNotNullParameter(charSequence, "suffix");
        if (!endsWith$default((CharSequence) str, charSequence, false, 2, (Object) null)) {
            return str;
        }
        String substring = str.substring(0, str.length() - charSequence.length());
        m.checkNotNullExpressionValue(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        return substring;
    }

    public static final String removeSurrounding(String str, CharSequence charSequence, CharSequence charSequence2) {
        m.checkNotNullParameter(str, "$this$removeSurrounding");
        m.checkNotNullParameter(charSequence, "prefix");
        m.checkNotNullParameter(charSequence2, "suffix");
        if (str.length() < charSequence2.length() + charSequence.length() || !startsWith$default((CharSequence) str, charSequence, false, 2, (Object) null) || !endsWith$default((CharSequence) str, charSequence2, false, 2, (Object) null)) {
            return str;
        }
        String substring = str.substring(charSequence.length(), str.length() - charSequence2.length());
        m.checkNotNullExpressionValue(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        return substring;
    }

    public static final String replaceAfterLast(String str, char c, String str2, String str3) {
        b.d.b.a.a.n0(str, "$this$replaceAfterLast", str2, "replacement", str3, "missingDelimiterValue");
        int lastIndexOf$default = lastIndexOf$default((CharSequence) str, c, 0, false, 6, (Object) null);
        return lastIndexOf$default == -1 ? str3 : replaceRange(str, lastIndexOf$default + 1, str.length(), str2).toString();
    }

    public static /* synthetic */ String replaceAfterLast$default(String str, char c, String str2, String str3, int i, Object obj) {
        if ((i & 4) != 0) {
            str3 = str;
        }
        return replaceAfterLast(str, c, str2, str3);
    }

    public static final CharSequence replaceRange(CharSequence charSequence, int i, int i2, CharSequence charSequence2) {
        m.checkNotNullParameter(charSequence, "$this$replaceRange");
        m.checkNotNullParameter(charSequence2, "replacement");
        if (i2 >= i) {
            StringBuilder sb = new StringBuilder();
            sb.append(charSequence, 0, i);
            m.checkNotNullExpressionValue(sb, "this.append(value, startIndex, endIndex)");
            sb.append(charSequence2);
            sb.append(charSequence, i2, charSequence.length());
            m.checkNotNullExpressionValue(sb, "this.append(value, startIndex, endIndex)");
            return sb;
        }
        throw new IndexOutOfBoundsException("End index (" + i2 + ") is less than start index (" + i + ").");
    }

    public static final void requireNonNegativeLimit(int i) {
        if (!(i >= 0)) {
            throw new IllegalArgumentException(b.d.b.a.a.p("Limit must be non-negative, but was ", i).toString());
        }
    }

    public static final List<String> split(CharSequence charSequence, String[] strArr, boolean z2, int i) {
        m.checkNotNullParameter(charSequence, "$this$split");
        m.checkNotNullParameter(strArr, "delimiters");
        boolean z3 = true;
        if (strArr.length == 1) {
            String str = strArr[0];
            if (str.length() != 0) {
                z3 = false;
            }
            if (!z3) {
                return e(charSequence, str, z2, i);
            }
        }
        Iterable<IntRange> asIterable = q.asIterable(d(charSequence, strArr, 0, z2, i, 2));
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(asIterable, 10));
        for (IntRange intRange : asIterable) {
            arrayList.add(substring(charSequence, intRange));
        }
        return arrayList;
    }

    public static /* synthetic */ List split$default(CharSequence charSequence, String[] strArr, boolean z2, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z2 = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return split(charSequence, strArr, z2, i);
    }

    public static final Sequence<String> splitToSequence(CharSequence charSequence, String[] strArr, boolean z2, int i) {
        m.checkNotNullParameter(charSequence, "$this$splitToSequence");
        m.checkNotNullParameter(strArr, "delimiters");
        return q.map(d(charSequence, strArr, 0, z2, i, 2), new a(charSequence));
    }

    public static /* synthetic */ Sequence splitToSequence$default(CharSequence charSequence, String[] strArr, boolean z2, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z2 = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return splitToSequence(charSequence, strArr, z2, i);
    }

    public static final boolean startsWith(CharSequence charSequence, char c, boolean z2) {
        m.checkNotNullParameter(charSequence, "$this$startsWith");
        return charSequence.length() > 0 && b.equals(charSequence.charAt(0), c, z2);
    }

    public static /* synthetic */ boolean startsWith$default(CharSequence charSequence, char c, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        return startsWith(charSequence, c, z2);
    }

    public static final String substring(CharSequence charSequence, IntRange intRange) {
        m.checkNotNullParameter(charSequence, "$this$substring");
        m.checkNotNullParameter(intRange, "range");
        return charSequence.subSequence(intRange.getStart().intValue(), intRange.getEndInclusive().intValue() + 1).toString();
    }

    public static final String substringAfter(String str, char c, String str2) {
        m.checkNotNullParameter(str, "$this$substringAfter");
        m.checkNotNullParameter(str2, "missingDelimiterValue");
        int indexOf$default = indexOf$default((CharSequence) str, c, 0, false, 6, (Object) null);
        if (indexOf$default == -1) {
            return str2;
        }
        String substring = str.substring(indexOf$default + 1, str.length());
        m.checkNotNullExpressionValue(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        return substring;
    }

    public static /* synthetic */ String substringAfter$default(String str, char c, String str2, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = str;
        }
        return substringAfter(str, c, str2);
    }

    public static final String substringAfterLast(String str, char c, String str2) {
        m.checkNotNullParameter(str, "$this$substringAfterLast");
        m.checkNotNullParameter(str2, "missingDelimiterValue");
        int lastIndexOf$default = lastIndexOf$default((CharSequence) str, c, 0, false, 6, (Object) null);
        if (lastIndexOf$default == -1) {
            return str2;
        }
        String substring = str.substring(lastIndexOf$default + 1, str.length());
        m.checkNotNullExpressionValue(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        return substring;
    }

    public static /* synthetic */ String substringAfterLast$default(String str, char c, String str2, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = str;
        }
        return substringAfterLast(str, c, str2);
    }

    public static final String substringBefore(String str, char c, String str2) {
        m.checkNotNullParameter(str, "$this$substringBefore");
        m.checkNotNullParameter(str2, "missingDelimiterValue");
        int indexOf$default = indexOf$default((CharSequence) str, c, 0, false, 6, (Object) null);
        if (indexOf$default == -1) {
            return str2;
        }
        String substring = str.substring(0, indexOf$default);
        m.checkNotNullExpressionValue(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        return substring;
    }

    public static /* synthetic */ String substringBefore$default(String str, char c, String str2, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = str;
        }
        return substringBefore(str, c, str2);
    }

    public static final String substringBeforeLast(String str, char c, String str2) {
        m.checkNotNullParameter(str, "$this$substringBeforeLast");
        m.checkNotNullParameter(str2, "missingDelimiterValue");
        int lastIndexOf$default = lastIndexOf$default((CharSequence) str, c, 0, false, 6, (Object) null);
        if (lastIndexOf$default == -1) {
            return str2;
        }
        String substring = str.substring(0, lastIndexOf$default);
        m.checkNotNullExpressionValue(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        return substring;
    }

    public static /* synthetic */ String substringBeforeLast$default(String str, char c, String str2, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = str;
        }
        return substringBeforeLast(str, c, str2);
    }

    public static final CharSequence trim(CharSequence charSequence) {
        m.checkNotNullParameter(charSequence, "$this$trim");
        int length = charSequence.length() - 1;
        int i = 0;
        boolean z2 = false;
        while (i <= length) {
            boolean isWhitespace = d0.g0.a.isWhitespace(charSequence.charAt(!z2 ? i : length));
            if (!z2) {
                if (!isWhitespace) {
                    z2 = true;
                } else {
                    i++;
                }
            } else if (!isWhitespace) {
                break;
            } else {
                length--;
            }
        }
        return charSequence.subSequence(i, length + 1);
    }

    public static final String trimStart(String str, char... cArr) {
        CharSequence charSequence;
        m.checkNotNullParameter(str, "$this$trimStart");
        m.checkNotNullParameter(cArr, "chars");
        int length = str.length();
        int i = 0;
        while (true) {
            if (i >= length) {
                charSequence = "";
                break;
            } else if (!k.contains(cArr, str.charAt(i))) {
                charSequence = str.subSequence(i, str.length());
                break;
            } else {
                i++;
            }
        }
        return charSequence.toString();
    }

    public static /* synthetic */ boolean contains$default(CharSequence charSequence, char c, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        return contains(charSequence, c, z2);
    }

    public static final boolean endsWith(CharSequence charSequence, CharSequence charSequence2, boolean z2) {
        m.checkNotNullParameter(charSequence, "$this$endsWith");
        m.checkNotNullParameter(charSequence2, "suffix");
        if (z2 || !(charSequence instanceof String) || !(charSequence2 instanceof String)) {
            return regionMatchesImpl(charSequence, charSequence.length() - charSequence2.length(), charSequence2, 0, charSequence2.length(), z2);
        }
        return t.endsWith$default((String) charSequence, (String) charSequence2, false, 2, null);
    }

    public static /* synthetic */ boolean endsWith$default(CharSequence charSequence, CharSequence charSequence2, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        return endsWith(charSequence, charSequence2, z2);
    }

    public static /* synthetic */ int indexOf$default(CharSequence charSequence, String str, int i, boolean z2, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        if ((i2 & 4) != 0) {
            z2 = false;
        }
        return indexOf(charSequence, str, i, z2);
    }

    public static /* synthetic */ int lastIndexOf$default(CharSequence charSequence, String str, int i, boolean z2, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = getLastIndex(charSequence);
        }
        if ((i2 & 4) != 0) {
            z2 = false;
        }
        return lastIndexOf(charSequence, str, i, z2);
    }

    public static /* synthetic */ List split$default(CharSequence charSequence, char[] cArr, boolean z2, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z2 = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return split(charSequence, cArr, z2, i);
    }

    public static final boolean startsWith(CharSequence charSequence, CharSequence charSequence2, boolean z2) {
        m.checkNotNullParameter(charSequence, "$this$startsWith");
        m.checkNotNullParameter(charSequence2, "prefix");
        if (z2 || !(charSequence instanceof String) || !(charSequence2 instanceof String)) {
            return regionMatchesImpl(charSequence, 0, charSequence2, 0, charSequence2.length(), z2);
        }
        return t.startsWith$default((String) charSequence, (String) charSequence2, false, 2, null);
    }

    public static /* synthetic */ boolean startsWith$default(CharSequence charSequence, CharSequence charSequence2, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        return startsWith(charSequence, charSequence2, z2);
    }

    public static /* synthetic */ String substringAfter$default(String str, String str2, String str3, int i, Object obj) {
        if ((i & 2) != 0) {
            str3 = str;
        }
        return substringAfter(str, str2, str3);
    }

    public static /* synthetic */ String substringBefore$default(String str, String str2, String str3, int i, Object obj) {
        if ((i & 2) != 0) {
            str3 = str;
        }
        return substringBefore(str, str2, str3);
    }

    public static /* synthetic */ String substringBeforeLast$default(String str, String str2, String str3, int i, Object obj) {
        if ((i & 2) != 0) {
            str3 = str;
        }
        return substringBeforeLast(str, str2, str3);
    }

    public static final String removeSurrounding(String str, CharSequence charSequence) {
        m.checkNotNullParameter(str, "$this$removeSurrounding");
        m.checkNotNullParameter(charSequence, "delimiter");
        return removeSurrounding(str, charSequence, charSequence);
    }

    public static final String substringAfter(String str, String str2, String str3) {
        b.d.b.a.a.n0(str, "$this$substringAfter", str2, "delimiter", str3, "missingDelimiterValue");
        int indexOf$default = indexOf$default((CharSequence) str, str2, 0, false, 6, (Object) null);
        if (indexOf$default == -1) {
            return str3;
        }
        String substring = str.substring(str2.length() + indexOf$default, str.length());
        m.checkNotNullExpressionValue(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        return substring;
    }

    public static final String substringBefore(String str, String str2, String str3) {
        b.d.b.a.a.n0(str, "$this$substringBefore", str2, "delimiter", str3, "missingDelimiterValue");
        int indexOf$default = indexOf$default((CharSequence) str, str2, 0, false, 6, (Object) null);
        if (indexOf$default == -1) {
            return str3;
        }
        String substring = str.substring(0, indexOf$default);
        m.checkNotNullExpressionValue(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        return substring;
    }

    public static final String substringBeforeLast(String str, String str2, String str3) {
        b.d.b.a.a.n0(str, "$this$substringBeforeLast", str2, "delimiter", str3, "missingDelimiterValue");
        int lastIndexOf$default = lastIndexOf$default((CharSequence) str, str2, 0, false, 6, (Object) null);
        if (lastIndexOf$default == -1) {
            return str3;
        }
        String substring = str.substring(0, lastIndexOf$default);
        m.checkNotNullExpressionValue(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        return substring;
    }

    public static final boolean contains(CharSequence charSequence, char c, boolean z2) {
        m.checkNotNullParameter(charSequence, "$this$contains");
        return indexOf$default(charSequence, c, 0, z2, 2, (Object) null) >= 0;
    }

    public static final int indexOf(CharSequence charSequence, String str, int i, boolean z2) {
        m.checkNotNullParameter(charSequence, "$this$indexOf");
        m.checkNotNullParameter(str, "string");
        if (z2 || !(charSequence instanceof String)) {
            return c(charSequence, str, i, charSequence.length(), z2, false, 16);
        }
        return ((String) charSequence).indexOf(str, i);
    }

    public static final int lastIndexOf(CharSequence charSequence, String str, int i, boolean z2) {
        m.checkNotNullParameter(charSequence, "$this$lastIndexOf");
        m.checkNotNullParameter(str, "string");
        if (z2 || !(charSequence instanceof String)) {
            return b(charSequence, str, i, 0, z2, true);
        }
        return ((String) charSequence).lastIndexOf(str, i);
    }

    public static final List<String> split(CharSequence charSequence, char[] cArr, boolean z2, int i) {
        m.checkNotNullParameter(charSequence, "$this$split");
        m.checkNotNullParameter(cArr, "delimiters");
        if (cArr.length == 1) {
            return e(charSequence, String.valueOf(cArr[0]), z2, i);
        }
        requireNonNegativeLimit(i);
        Iterable<IntRange> asIterable = q.asIterable(new d(charSequence, 0, i, new u(cArr, z2)));
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(asIterable, 10));
        for (IntRange intRange : asIterable) {
            arrayList.add(substring(charSequence, intRange));
        }
        return arrayList;
    }
}
