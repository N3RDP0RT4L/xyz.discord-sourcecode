package d0.g0;

import d0.z.d.m;
/* compiled from: StringBuilder.kt */
/* loaded from: classes3.dex */
public class q extends p {
    public static final StringBuilder append(StringBuilder sb, String... strArr) {
        m.checkNotNullParameter(sb, "$this$append");
        m.checkNotNullParameter(strArr, "value");
        for (String str : strArr) {
            sb.append(str);
        }
        return sb;
    }
}
