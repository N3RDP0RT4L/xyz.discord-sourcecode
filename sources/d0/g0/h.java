package d0.g0;

import java.util.regex.Matcher;
import kotlin.text.MatchResult;
/* compiled from: Regex.kt */
/* loaded from: classes3.dex */
public final class h {
    public static final MatchResult access$findNext(Matcher matcher, int i, CharSequence charSequence) {
        if (!matcher.find(i)) {
            return null;
        }
        return new g(matcher, charSequence);
    }

    public static final MatchResult access$matchEntire(Matcher matcher, CharSequence charSequence) {
        if (!matcher.matches()) {
            return null;
        }
        return new g(matcher, charSequence);
    }
}
