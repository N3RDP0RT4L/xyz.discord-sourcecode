package d0.g0;

import d0.d0.f;
import d0.f0.q;
import d0.t.c;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import kotlin.jvm.functions.Function1;
import kotlin.ranges.IntRange;
import kotlin.text.MatchResult;
/* compiled from: Regex.kt */
/* loaded from: classes3.dex */
public final class g implements MatchResult {
    public final f a = new b();

    /* renamed from: b  reason: collision with root package name */
    public List<String> f3554b;
    public final Matcher c;
    public final CharSequence d;

    /* compiled from: Regex.kt */
    /* loaded from: classes3.dex */
    public static final class a extends c<String> {
        public a() {
        }

        @Override // d0.t.a, java.util.Collection, java.util.List
        public final /* bridge */ boolean contains(Object obj) {
            if (obj instanceof String) {
                return contains((String) obj);
            }
            return false;
        }

        @Override // d0.t.a
        public int getSize() {
            return g.this.c.groupCount() + 1;
        }

        @Override // d0.t.c, java.util.List
        public final /* bridge */ int indexOf(Object obj) {
            if (obj instanceof String) {
                return indexOf((String) obj);
            }
            return -1;
        }

        @Override // d0.t.c, java.util.List
        public final /* bridge */ int lastIndexOf(Object obj) {
            if (obj instanceof String) {
                return lastIndexOf((String) obj);
            }
            return -1;
        }

        public /* bridge */ boolean contains(String str) {
            return super.contains((Object) str);
        }

        @Override // d0.t.c, java.util.List
        public String get(int i) {
            String group = g.this.c.group(i);
            return group != null ? group : "";
        }

        public /* bridge */ int indexOf(String str) {
            return super.indexOf((Object) str);
        }

        public /* bridge */ int lastIndexOf(String str) {
            return super.lastIndexOf((Object) str);
        }
    }

    /* compiled from: Regex.kt */
    /* loaded from: classes3.dex */
    public static final class b extends d0.t.a<e> implements f {

        /* compiled from: Regex.kt */
        /* loaded from: classes3.dex */
        public static final class a extends o implements Function1<Integer, e> {
            public a() {
                super(1);
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ e invoke(Integer num) {
                return invoke(num.intValue());
            }

            public final e invoke(int i) {
                return b.this.get(i);
            }
        }

        public b() {
        }

        @Override // d0.t.a, java.util.Collection, java.util.List
        public final /* bridge */ boolean contains(Object obj) {
            if (obj != null ? obj instanceof e : true) {
                return contains((e) obj);
            }
            return false;
        }

        public e get(int i) {
            IntRange until;
            until = f.until(r0.start(i), g.this.c.end(i));
            if (until.getStart().intValue() < 0) {
                return null;
            }
            String group = g.this.c.group(i);
            m.checkNotNullExpressionValue(group, "matchResult.group(index)");
            return new e(group, until);
        }

        @Override // d0.t.a
        public int getSize() {
            return g.this.c.groupCount() + 1;
        }

        @Override // d0.t.a, java.util.Collection
        public boolean isEmpty() {
            return false;
        }

        @Override // java.util.Collection, java.lang.Iterable
        public Iterator<e> iterator() {
            return q.map(u.asSequence(n.getIndices(this)), new a()).iterator();
        }

        public /* bridge */ boolean contains(e eVar) {
            return super.contains((Object) eVar);
        }
    }

    public g(Matcher matcher, CharSequence charSequence) {
        m.checkNotNullParameter(matcher, "matcher");
        m.checkNotNullParameter(charSequence, "input");
        this.c = matcher;
        this.d = charSequence;
    }

    @Override // kotlin.text.MatchResult
    public MatchResult.b getDestructured() {
        return MatchResult.a.getDestructured(this);
    }

    @Override // kotlin.text.MatchResult
    public List<String> getGroupValues() {
        if (this.f3554b == null) {
            this.f3554b = new a();
        }
        List<String> list = this.f3554b;
        m.checkNotNull(list);
        return list;
    }

    @Override // kotlin.text.MatchResult
    public f getGroups() {
        return this.a;
    }

    @Override // kotlin.text.MatchResult
    public IntRange getRange() {
        IntRange until;
        until = f.until(r0.start(), this.c.end());
        return until;
    }

    @Override // kotlin.text.MatchResult
    public String getValue() {
        String group = this.c.group();
        m.checkNotNullExpressionValue(group, "matchResult.group()");
        return group;
    }

    @Override // kotlin.text.MatchResult
    public MatchResult next() {
        int end = this.c.end() + (this.c.end() == this.c.start() ? 1 : 0);
        if (end > this.d.length()) {
            return null;
        }
        Matcher matcher = this.c.pattern().matcher(this.d);
        m.checkNotNullExpressionValue(matcher, "matcher.pattern().matcher(input)");
        return h.access$findNext(matcher, end, this.d);
    }
}
