package d0.g0;

import d0.z.d.m;
/* compiled from: StringNumberConversionsJVM.kt */
/* loaded from: classes3.dex */
public class r extends q {
    public static final Double toDoubleOrNull(String str) {
        m.checkNotNullParameter(str, "$this$toDoubleOrNull");
        try {
            if (j.a.matches(str)) {
                return Double.valueOf(Double.parseDouble(str));
            }
            return null;
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    public static final Float toFloatOrNull(String str) {
        m.checkNotNullParameter(str, "$this$toFloatOrNull");
        try {
            if (j.a.matches(str)) {
                return Float.valueOf(Float.parseFloat(str));
            }
            return null;
        } catch (NumberFormatException unused) {
            return null;
        }
    }
}
