package d0.g0;

import andhook.lib.xposed.ClassUtils;
import b.d.b.a.a;
import d0.d0.f;
import d0.t.c0;
import d0.z.d.d0;
import d0.z.d.m;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Objects;
import kotlin.ranges.IntRange;
/* compiled from: StringsJVM.kt */
/* loaded from: classes3.dex */
public class t extends s {
    public static final String capitalize(String str) {
        m.checkNotNullParameter(str, "$this$capitalize");
        Locale locale = Locale.getDefault();
        m.checkNotNullExpressionValue(locale, "Locale.getDefault()");
        return capitalize(str, locale);
    }

    public static final int compareTo(String str, String str2, boolean z2) {
        m.checkNotNullParameter(str, "$this$compareTo");
        m.checkNotNullParameter(str2, "other");
        if (z2) {
            return str.compareToIgnoreCase(str2);
        }
        return str.compareTo(str2);
    }

    public static final String decapitalize(String str) {
        m.checkNotNullParameter(str, "$this$decapitalize");
        if (!(str.length() > 0) || Character.isLowerCase(str.charAt(0))) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        String substring = str.substring(0, 1);
        m.checkNotNullExpressionValue(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        Objects.requireNonNull(substring, "null cannot be cast to non-null type java.lang.String");
        String lowerCase = substring.toLowerCase();
        m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase()");
        sb.append(lowerCase);
        String substring2 = str.substring(1);
        m.checkNotNullExpressionValue(substring2, "(this as java.lang.String).substring(startIndex)");
        sb.append(substring2);
        return sb.toString();
    }

    public static final boolean endsWith(String str, String str2, boolean z2) {
        m.checkNotNullParameter(str, "$this$endsWith");
        m.checkNotNullParameter(str2, "suffix");
        if (!z2) {
            return str.endsWith(str2);
        }
        return regionMatches(str, str.length() - str2.length(), str2, 0, str2.length(), true);
    }

    public static /* synthetic */ boolean endsWith$default(String str, String str2, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        return endsWith(str, str2, z2);
    }

    public static final boolean equals(String str, String str2, boolean z2) {
        if (str == null) {
            return str2 == null;
        }
        if (!z2) {
            return str.equals(str2);
        }
        return str.equalsIgnoreCase(str2);
    }

    public static /* synthetic */ boolean equals$default(String str, String str2, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        return equals(str, str2, z2);
    }

    public static final Comparator<String> getCASE_INSENSITIVE_ORDER(d0 d0Var) {
        m.checkNotNullParameter(d0Var, "$this$CASE_INSENSITIVE_ORDER");
        Comparator<String> comparator = String.CASE_INSENSITIVE_ORDER;
        m.checkNotNullExpressionValue(comparator, "java.lang.String.CASE_INSENSITIVE_ORDER");
        return comparator;
    }

    public static final boolean isBlank(CharSequence charSequence) {
        boolean z2;
        m.checkNotNullParameter(charSequence, "$this$isBlank");
        if (charSequence.length() != 0) {
            IntRange indices = w.getIndices(charSequence);
            if (!(indices instanceof Collection) || !((Collection) indices).isEmpty()) {
                Iterator<Integer> it = indices.iterator();
                while (it.hasNext()) {
                    if (!a.isWhitespace(charSequence.charAt(((c0) it).nextInt()))) {
                        z2 = false;
                        break;
                    }
                }
            }
            z2 = true;
            if (!z2) {
                return false;
            }
        }
        return true;
    }

    public static final boolean regionMatches(String str, int i, String str2, int i2, int i3, boolean z2) {
        m.checkNotNullParameter(str, "$this$regionMatches");
        m.checkNotNullParameter(str2, "other");
        if (!z2) {
            return str.regionMatches(i, str2, i2, i3);
        }
        return str.regionMatches(z2, i, str2, i2, i3);
    }

    public static final String repeat(CharSequence charSequence, int i) {
        m.checkNotNullParameter(charSequence, "$this$repeat");
        int i2 = 1;
        if (!(i >= 0)) {
            throw new IllegalArgumentException(("Count 'n' must be non-negative, but was " + i + ClassUtils.PACKAGE_SEPARATOR_CHAR).toString());
        } else if (i == 0) {
            return "";
        } else {
            if (i == 1) {
                return charSequence.toString();
            }
            int length = charSequence.length();
            if (length == 0) {
                return "";
            }
            if (length != 1) {
                StringBuilder sb = new StringBuilder(charSequence.length() * i);
                if (1 <= i) {
                    while (true) {
                        sb.append(charSequence);
                        if (i2 == i) {
                            break;
                        }
                        i2++;
                    }
                }
                String sb2 = sb.toString();
                m.checkNotNullExpressionValue(sb2, "sb.toString()");
                return sb2;
            }
            char charAt = charSequence.charAt(0);
            char[] cArr = new char[i];
            for (int i3 = 0; i3 < i; i3++) {
                cArr[i3] = charAt;
            }
            return new String(cArr);
        }
    }

    public static final String replace(String str, char c, char c2, boolean z2) {
        m.checkNotNullParameter(str, "$this$replace");
        if (!z2) {
            String replace = str.replace(c, c2);
            m.checkNotNullExpressionValue(replace, "(this as java.lang.Strin…replace(oldChar, newChar)");
            return replace;
        }
        StringBuilder sb = new StringBuilder(str.length());
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (b.equals(charAt, c, z2)) {
                charAt = c2;
            }
            sb.append(charAt);
        }
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "StringBuilder(capacity).…builderAction).toString()");
        return sb2;
    }

    public static /* synthetic */ String replace$default(String str, char c, char c2, boolean z2, int i, Object obj) {
        if ((i & 4) != 0) {
            z2 = false;
        }
        return replace(str, c, c2, z2);
    }

    public static final String replaceFirst(String str, String str2, String str3, boolean z2) {
        a.n0(str, "$this$replaceFirst", str2, "oldValue", str3, "newValue");
        int indexOf$default = w.indexOf$default(str, str2, 0, z2, 2, (Object) null);
        return indexOf$default < 0 ? str : w.replaceRange(str, indexOf$default, str2.length() + indexOf$default, str3).toString();
    }

    public static /* synthetic */ String replaceFirst$default(String str, String str2, String str3, boolean z2, int i, Object obj) {
        if ((i & 4) != 0) {
            z2 = false;
        }
        return replaceFirst(str, str2, str3, z2);
    }

    public static final boolean startsWith(String str, String str2, boolean z2) {
        m.checkNotNullParameter(str, "$this$startsWith");
        m.checkNotNullParameter(str2, "prefix");
        if (!z2) {
            return str.startsWith(str2);
        }
        return regionMatches(str, 0, str2, 0, str2.length(), z2);
    }

    public static /* synthetic */ boolean startsWith$default(String str, String str2, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        return startsWith(str, str2, z2);
    }

    public static final String capitalize(String str, Locale locale) {
        m.checkNotNullParameter(str, "$this$capitalize");
        m.checkNotNullParameter(locale, "locale");
        if (!(str.length() > 0)) {
            return str;
        }
        char charAt = str.charAt(0);
        if (!Character.isLowerCase(charAt)) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        char titleCase = Character.toTitleCase(charAt);
        if (titleCase != Character.toUpperCase(charAt)) {
            sb.append(titleCase);
        } else {
            String substring = str.substring(0, 1);
            m.checkNotNullExpressionValue(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            Objects.requireNonNull(substring, "null cannot be cast to non-null type java.lang.String");
            String upperCase = substring.toUpperCase(locale);
            m.checkNotNullExpressionValue(upperCase, "(this as java.lang.String).toUpperCase(locale)");
            sb.append(upperCase);
        }
        String substring2 = str.substring(1);
        m.checkNotNullExpressionValue(substring2, "(this as java.lang.String).substring(startIndex)");
        sb.append(substring2);
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }

    public static /* synthetic */ String replace$default(String str, String str2, String str3, boolean z2, int i, Object obj) {
        if ((i & 4) != 0) {
            z2 = false;
        }
        return replace(str, str2, str3, z2);
    }

    public static /* synthetic */ boolean startsWith$default(String str, String str2, int i, boolean z2, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            z2 = false;
        }
        return startsWith(str, str2, i, z2);
    }

    public static final boolean startsWith(String str, String str2, int i, boolean z2) {
        m.checkNotNullParameter(str, "$this$startsWith");
        m.checkNotNullParameter(str2, "prefix");
        if (!z2) {
            return str.startsWith(str2, i);
        }
        return regionMatches(str, i, str2, 0, str2.length(), z2);
    }

    public static final String replace(String str, String str2, String str3, boolean z2) {
        m.checkNotNullParameter(str, "$this$replace");
        m.checkNotNullParameter(str2, "oldValue");
        m.checkNotNullParameter(str3, "newValue");
        int i = 0;
        int indexOf = w.indexOf(str, str2, 0, z2);
        if (indexOf < 0) {
            return str;
        }
        int length = str2.length();
        int coerceAtLeast = f.coerceAtLeast(length, 1);
        int length2 = str3.length() + (str.length() - length);
        if (length2 >= 0) {
            StringBuilder sb = new StringBuilder(length2);
            do {
                sb.append((CharSequence) str, i, indexOf);
                sb.append(str3);
                i = indexOf + length;
                if (indexOf >= str.length()) {
                    break;
                }
                indexOf = w.indexOf(str, str2, indexOf + coerceAtLeast, z2);
            } while (indexOf > 0);
            sb.append((CharSequence) str, i, str.length());
            String sb2 = sb.toString();
            m.checkNotNullExpressionValue(sb2, "stringBuilder.append(this, i, length).toString()");
            return sb2;
        }
        throw new OutOfMemoryError();
    }
}
