package d0.v;

import d0.z.d.m;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: Thread.kt */
/* loaded from: classes3.dex */
public final class a {

    /* compiled from: Thread.kt */
    /* renamed from: d0.v.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0377a extends Thread {
        public final /* synthetic */ Function0 j;

        public C0377a(Function0 function0) {
            this.j = function0;
        }

        @Override // java.lang.Thread, java.lang.Runnable
        public void run() {
            this.j.invoke();
        }
    }

    public static final Thread thread(boolean z2, boolean z3, ClassLoader classLoader, String str, int i, Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "block");
        C0377a aVar = new C0377a(function0);
        if (z3) {
            aVar.setDaemon(true);
        }
        if (i > 0) {
            aVar.setPriority(i);
        }
        if (str != null) {
            aVar.setName(str);
        }
        if (classLoader != null) {
            aVar.setContextClassLoader(classLoader);
        }
        if (z2) {
            aVar.start();
        }
        return aVar;
    }
}
