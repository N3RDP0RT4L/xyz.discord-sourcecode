package d0.e0;

import java.util.List;
import kotlin.reflect.KType;
/* compiled from: KTypeParameter.kt */
/* loaded from: classes3.dex */
public interface h extends d {
    String getName();

    List<KType> getUpperBounds();

    j getVariance();
}
