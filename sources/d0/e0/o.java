package d0.e0;

import d0.z.d.m;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: TypesJVM.kt */
/* loaded from: classes3.dex */
public final class o implements WildcardType, Type {
    public final Type l;
    public final Type m;
    public static final a k = new a(null);
    public static final o j = new o(null, null);

    /* compiled from: TypesJVM.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final o getSTAR() {
            return o.j;
        }
    }

    public o(Type type, Type type2) {
        this.l = type;
        this.m = type2;
    }

    public boolean equals(Object obj) {
        if (obj instanceof WildcardType) {
            WildcardType wildcardType = (WildcardType) obj;
            if (Arrays.equals(getUpperBounds(), wildcardType.getUpperBounds()) && Arrays.equals(getLowerBounds(), wildcardType.getLowerBounds())) {
                return true;
            }
        }
        return false;
    }

    @Override // java.lang.reflect.WildcardType
    public Type[] getLowerBounds() {
        Type type = this.m;
        return type == null ? new Type[0] : new Type[]{type};
    }

    @Override // java.lang.reflect.Type
    public String getTypeName() {
        if (this.m != null) {
            StringBuilder R = b.d.b.a.a.R("? super ");
            R.append(n.access$typeToString(this.m));
            return R.toString();
        }
        Type type = this.l;
        if (type == null || !(!m.areEqual(type, Object.class))) {
            return "?";
        }
        StringBuilder R2 = b.d.b.a.a.R("? extends ");
        R2.append(n.access$typeToString(this.l));
        return R2.toString();
    }

    @Override // java.lang.reflect.WildcardType
    public Type[] getUpperBounds() {
        Type[] typeArr = new Type[1];
        Type type = this.l;
        if (type == null) {
            type = Object.class;
        }
        typeArr[0] = type;
        return typeArr;
    }

    public int hashCode() {
        return Arrays.hashCode(getUpperBounds()) ^ Arrays.hashCode(getLowerBounds());
    }

    public String toString() {
        return getTypeName();
    }
}
