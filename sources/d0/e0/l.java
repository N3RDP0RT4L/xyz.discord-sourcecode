package d0.e0;

import b.d.b.a.a;
import d0.j;
import d0.t.o;
import d0.z.d.m;
import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import kotlin.reflect.KType;
/* compiled from: TypesJVM.kt */
/* loaded from: classes3.dex */
public final class l implements TypeVariable<GenericDeclaration>, Type {
    public final h j;

    public l(h hVar) {
        m.checkNotNullParameter(hVar, "typeParameter");
        this.j = hVar;
    }

    public boolean equals(Object obj) {
        if (obj instanceof TypeVariable) {
            TypeVariable typeVariable = (TypeVariable) obj;
            if (m.areEqual(getName(), typeVariable.getName()) && m.areEqual(getGenericDeclaration(), typeVariable.getGenericDeclaration())) {
                return true;
            }
        }
        return false;
    }

    @Override // java.lang.reflect.TypeVariable
    public Type[] getBounds() {
        Type a;
        List<KType> upperBounds = this.j.getUpperBounds();
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(upperBounds, 10));
        for (KType kType : upperBounds) {
            a = n.a(kType, true);
            arrayList.add(a);
        }
        Object[] array = arrayList.toArray(new Type[0]);
        Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
        return (Type[]) array;
    }

    @Override // java.lang.reflect.TypeVariable
    public GenericDeclaration getGenericDeclaration() {
        StringBuilder R = a.R("getGenericDeclaration() is not yet supported for type variables created from KType: ");
        R.append(this.j);
        throw new j(a.v("An operation is not implemented: ", R.toString()));
    }

    @Override // java.lang.reflect.TypeVariable
    public String getName() {
        return this.j.getName();
    }

    @Override // java.lang.reflect.Type
    public String getTypeName() {
        return getName();
    }

    public int hashCode() {
        return getName().hashCode() ^ getGenericDeclaration().hashCode();
    }

    public String toString() {
        return getTypeName();
    }
}
