package d0.e0.p.d.n0;
/* compiled from: IntTreePMap.java */
/* loaded from: classes3.dex */
public final class d<V> {
    public static final d<Object> a = new d<>(c.a);

    /* renamed from: b  reason: collision with root package name */
    public final c<V> f3539b;

    public d(c<V> cVar) {
        this.f3539b = cVar;
    }

    public static <V> d<V> empty() {
        return (d<V>) a;
    }

    public V get(int i) {
        return this.f3539b.a(i);
    }

    public d<V> plus(int i, V v) {
        c<V> b2 = this.f3539b.b(i, v);
        return b2 == this.f3539b ? this : new d<>(b2);
    }
}
