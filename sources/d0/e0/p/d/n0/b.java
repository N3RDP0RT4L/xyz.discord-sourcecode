package d0.e0.p.d.n0;
/* compiled from: HashPMap.java */
/* loaded from: classes3.dex */
public final class b<K, V> {
    public static final b<Object, Object> a = new b<>(d.empty(), 0);

    /* renamed from: b  reason: collision with root package name */
    public final d<a<e<K, V>>> f3537b;
    public final int c;

    public b(d<a<e<K, V>>> dVar, int i) {
        this.f3537b = dVar;
        this.c = i;
    }

    public static <K, V> b<K, V> empty() {
        b<K, V> bVar = (b<K, V>) a;
        if (bVar != null) {
            return bVar;
        }
        throw new IllegalStateException(String.format("@NotNull method %s.%s must not return null", "kotlin/reflect/jvm/internal/pcollections/HashPMap", "empty"));
    }

    public V get(Object obj) {
        a<e<K, V>> aVar = this.f3537b.get(obj.hashCode());
        a aVar2 = aVar;
        if (aVar == null) {
            aVar2 = a.empty();
        }
        while (aVar2 != null && aVar2.size() > 0) {
            e eVar = (e) aVar2.k;
            if (eVar.key.equals(obj)) {
                return eVar.value;
            }
            aVar2 = aVar2.l;
        }
        return null;
    }

    public b<K, V> plus(K k, V v) {
        a<e<K, V>> aVar = this.f3537b.get(k.hashCode());
        if (aVar == null) {
            aVar = a.empty();
        }
        int size = aVar.size();
        int i = 0;
        a aVar2 = aVar;
        while (aVar2 != null && aVar2.size() > 0) {
            if (((e) aVar2.k).key.equals(k)) {
                break;
            }
            aVar2 = aVar2.l;
            i++;
        }
        i = -1;
        if (i != -1) {
            aVar = aVar.minus(i);
        }
        a<e<K, V>> plus = aVar.plus(new e<>(k, v));
        return new b<>(this.f3537b.plus(k.hashCode(), plus), plus.size() + (this.c - size));
    }
}
