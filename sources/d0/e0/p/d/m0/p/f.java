package d0.e0.p.d.m0.p;

import b.d.b.a.a;
import d0.z.d.m;
/* compiled from: numbers.kt */
/* loaded from: classes3.dex */
public final class f {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final int f3536b;

    public f(String str, int i) {
        m.checkNotNullParameter(str, "number");
        this.a = str;
        this.f3536b = i;
    }

    public final String component1() {
        return this.a;
    }

    public final int component2() {
        return this.f3536b;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        return m.areEqual(this.a, fVar.a) && this.f3536b == fVar.f3536b;
    }

    public int hashCode() {
        return (this.a.hashCode() * 31) + this.f3536b;
    }

    public String toString() {
        StringBuilder R = a.R("NumberWithRadix(number=");
        R.append(this.a);
        R.append(", radix=");
        return a.z(R, this.f3536b, ')');
    }
}
