package d0.e0.p.d.m0.p;

import d0.g0.t;
import d0.z.d.m;
/* compiled from: numbers.kt */
/* loaded from: classes3.dex */
public final class g {
    public static final f extractRadix(String str) {
        m.checkNotNullParameter(str, "value");
        if (t.startsWith$default(str, "0x", false, 2, null) || t.startsWith$default(str, "0X", false, 2, null)) {
            String substring = str.substring(2);
            m.checkNotNullExpressionValue(substring, "(this as java.lang.String).substring(startIndex)");
            return new f(substring, 16);
        } else if (!t.startsWith$default(str, "0b", false, 2, null) && !t.startsWith$default(str, "0B", false, 2, null)) {
            return new f(str, 10);
        } else {
            String substring2 = str.substring(2);
            m.checkNotNullExpressionValue(substring2, "(this as java.lang.String).substring(startIndex)");
            return new f(substring2, 2);
        }
    }
}
