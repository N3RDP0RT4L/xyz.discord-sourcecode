package d0.e0.p.d.m0.p;
/* compiled from: JavaTypeEnhancementState.kt */
/* loaded from: classes3.dex */
public enum h {
    IGNORE("ignore"),
    WARN("warn"),
    STRICT("strict");
    
    private final String description;

    static {
        new Object(null) { // from class: d0.e0.p.d.m0.p.h.a
        };
    }

    h(String str) {
        this.description = str;
    }

    public final String getDescription() {
        return this.description;
    }

    public final boolean isIgnore() {
        return this == IGNORE;
    }

    public final boolean isWarning() {
        return this == WARN;
    }
}
