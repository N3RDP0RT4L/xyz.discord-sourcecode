package d0.e0.p.d.m0.p;

import d0.z.d.m;
/* compiled from: exceptionUtils.kt */
/* loaded from: classes3.dex */
public final class c {
    public static final boolean isProcessCanceledException(Throwable th) {
        m.checkNotNullParameter(th, "<this>");
        Class<?> cls = th.getClass();
        while (!m.areEqual(cls.getCanonicalName(), "com.intellij.openapi.progress.ProcessCanceledException")) {
            cls = cls.getSuperclass();
            if (cls == null) {
                return false;
            }
        }
        return true;
    }

    public static final RuntimeException rethrow(Throwable th) {
        m.checkNotNullParameter(th, "e");
        throw th;
    }
}
