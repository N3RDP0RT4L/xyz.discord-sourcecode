package d0.e0.p.d.m0.p;

import d0.z.d.o;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function3;
/* compiled from: functions.kt */
/* loaded from: classes3.dex */
public final class d {
    public static final Function1<Object, Boolean> a = b.j;

    /* renamed from: b  reason: collision with root package name */
    public static final Function3<Object, Object, Object, Unit> f3534b = e.j;

    /* compiled from: functions.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1 {
        public static final a j = new a();

        public a() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public final Void invoke(Object obj) {
            return null;
        }
    }

    /* compiled from: functions.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function1<Object, Boolean> {
        public static final b j = new b();

        public b() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final Boolean invoke2(Object obj) {
            return 1;
        }
    }

    /* compiled from: functions.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function1<Object, Unit> {
        public static final c j = new c();

        public c() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Object obj) {
        }
    }

    /* compiled from: functions.kt */
    /* renamed from: d0.e0.p.d.m0.p.d$d  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0369d extends o implements Function2<Object, Object, Unit> {
        public static final C0369d j = new C0369d();

        public C0369d() {
            super(2);
        }

        @Override // kotlin.jvm.functions.Function2
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Object obj, Object obj2) {
        }
    }

    /* compiled from: functions.kt */
    /* loaded from: classes3.dex */
    public static final class e extends o implements Function3<Object, Object, Object, Unit> {
        public static final e j = new e();

        public e() {
            super(3);
        }

        @Override // kotlin.jvm.functions.Function3
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Object obj, Object obj2, Object obj3) {
        }
    }

    /* compiled from: functions.kt */
    /* loaded from: classes3.dex */
    public static final class f extends o implements Function1<Object, Object> {
        public static final f j = new f();

        public f() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public final Object invoke(Object obj) {
            return obj;
        }
    }

    static {
        f fVar = f.j;
        a aVar = a.j;
        c cVar = c.j;
        C0369d dVar = C0369d.j;
    }

    public static final <T> Function1<T, Boolean> alwaysTrue() {
        return (Function1<T, Boolean>) a;
    }

    public static final Function3<Object, Object, Object, Unit> getDO_NOTHING_3() {
        return f3534b;
    }
}
