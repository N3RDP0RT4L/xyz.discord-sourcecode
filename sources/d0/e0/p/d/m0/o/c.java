package d0.e0.p.d.m0.o;

import d0.z.d.m;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: modifierChecks.kt */
/* loaded from: classes3.dex */
public abstract class c {
    public final boolean a;

    /* compiled from: modifierChecks.kt */
    /* loaded from: classes3.dex */
    public static final class a extends c {

        /* renamed from: b  reason: collision with root package name */
        public static final a f3517b = new a();

        public a() {
            super(false, null);
        }
    }

    /* compiled from: modifierChecks.kt */
    /* loaded from: classes3.dex */
    public static final class b extends c {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(String str) {
            super(false, null);
            m.checkNotNullParameter(str, "error");
        }
    }

    /* compiled from: modifierChecks.kt */
    /* renamed from: d0.e0.p.d.m0.o.c$c  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0366c extends c {

        /* renamed from: b  reason: collision with root package name */
        public static final C0366c f3518b = new C0366c();

        public C0366c() {
            super(true, null);
        }
    }

    public c(boolean z2, DefaultConstructorMarker defaultConstructorMarker) {
        this.a = z2;
    }

    public final boolean isSuccess() {
        return this.a;
    }
}
