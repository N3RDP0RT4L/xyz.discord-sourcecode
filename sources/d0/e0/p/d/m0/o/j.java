package d0.e0.p.d.m0.o;

import d0.e0.p.d.m0.g.e;
import d0.t.n0;
import d0.z.d.m;
import java.util.Set;
import kotlin.text.Regex;
/* compiled from: OperatorNameConventions.kt */
/* loaded from: classes3.dex */
public final class j {
    public static final e A;
    public static final e B;
    public static final e C;
    public static final e D;
    public static final e E;
    public static final Set<e> F;
    public static final Set<e> G;
    public static final Set<e> H;
    public static final e a;

    /* renamed from: b  reason: collision with root package name */
    public static final e f3523b;
    public static final e c;
    public static final e d;
    public static final e e;
    public static final e f;
    public static final e g;
    public static final e h;
    public static final e i;
    public static final e j;
    public static final e k;
    public static final e l;
    public static final Regex m = new Regex("component\\d+");
    public static final e n;
    public static final e o;
    public static final e p;
    public static final e q;
    public static final e r;

    /* renamed from: s  reason: collision with root package name */
    public static final e f3524s;
    public static final e t;
    public static final e u;
    public static final e v;
    public static final e w;

    /* renamed from: x  reason: collision with root package name */
    public static final e f3525x;

    /* renamed from: y  reason: collision with root package name */
    public static final e f3526y;

    /* renamed from: z  reason: collision with root package name */
    public static final e f3527z;

    static {
        e identifier = e.identifier("getValue");
        m.checkNotNullExpressionValue(identifier, "identifier(\"getValue\")");
        a = identifier;
        e identifier2 = e.identifier("setValue");
        m.checkNotNullExpressionValue(identifier2, "identifier(\"setValue\")");
        f3523b = identifier2;
        e identifier3 = e.identifier("provideDelegate");
        m.checkNotNullExpressionValue(identifier3, "identifier(\"provideDelegate\")");
        c = identifier3;
        e identifier4 = e.identifier("equals");
        m.checkNotNullExpressionValue(identifier4, "identifier(\"equals\")");
        d = identifier4;
        e identifier5 = e.identifier("compareTo");
        m.checkNotNullExpressionValue(identifier5, "identifier(\"compareTo\")");
        e = identifier5;
        e identifier6 = e.identifier("contains");
        m.checkNotNullExpressionValue(identifier6, "identifier(\"contains\")");
        f = identifier6;
        e identifier7 = e.identifier("invoke");
        m.checkNotNullExpressionValue(identifier7, "identifier(\"invoke\")");
        g = identifier7;
        e identifier8 = e.identifier("iterator");
        m.checkNotNullExpressionValue(identifier8, "identifier(\"iterator\")");
        h = identifier8;
        e identifier9 = e.identifier("get");
        m.checkNotNullExpressionValue(identifier9, "identifier(\"get\")");
        i = identifier9;
        e identifier10 = e.identifier("set");
        m.checkNotNullExpressionValue(identifier10, "identifier(\"set\")");
        j = identifier10;
        e identifier11 = e.identifier("next");
        m.checkNotNullExpressionValue(identifier11, "identifier(\"next\")");
        k = identifier11;
        e identifier12 = e.identifier("hasNext");
        m.checkNotNullExpressionValue(identifier12, "identifier(\"hasNext\")");
        l = identifier12;
        m.checkNotNullExpressionValue(e.identifier("toString"), "identifier(\"toString\")");
        m.checkNotNullExpressionValue(e.identifier("and"), "identifier(\"and\")");
        m.checkNotNullExpressionValue(e.identifier("or"), "identifier(\"or\")");
        e identifier13 = e.identifier("inc");
        m.checkNotNullExpressionValue(identifier13, "identifier(\"inc\")");
        n = identifier13;
        e identifier14 = e.identifier("dec");
        m.checkNotNullExpressionValue(identifier14, "identifier(\"dec\")");
        o = identifier14;
        e identifier15 = e.identifier("plus");
        m.checkNotNullExpressionValue(identifier15, "identifier(\"plus\")");
        p = identifier15;
        e identifier16 = e.identifier("minus");
        m.checkNotNullExpressionValue(identifier16, "identifier(\"minus\")");
        q = identifier16;
        e identifier17 = e.identifier("not");
        m.checkNotNullExpressionValue(identifier17, "identifier(\"not\")");
        r = identifier17;
        e identifier18 = e.identifier("unaryMinus");
        m.checkNotNullExpressionValue(identifier18, "identifier(\"unaryMinus\")");
        f3524s = identifier18;
        e identifier19 = e.identifier("unaryPlus");
        m.checkNotNullExpressionValue(identifier19, "identifier(\"unaryPlus\")");
        t = identifier19;
        e identifier20 = e.identifier("times");
        m.checkNotNullExpressionValue(identifier20, "identifier(\"times\")");
        u = identifier20;
        e identifier21 = e.identifier("div");
        m.checkNotNullExpressionValue(identifier21, "identifier(\"div\")");
        v = identifier21;
        e identifier22 = e.identifier("mod");
        m.checkNotNullExpressionValue(identifier22, "identifier(\"mod\")");
        w = identifier22;
        e identifier23 = e.identifier("rem");
        m.checkNotNullExpressionValue(identifier23, "identifier(\"rem\")");
        f3525x = identifier23;
        e identifier24 = e.identifier("rangeTo");
        m.checkNotNullExpressionValue(identifier24, "identifier(\"rangeTo\")");
        f3526y = identifier24;
        e identifier25 = e.identifier("timesAssign");
        m.checkNotNullExpressionValue(identifier25, "identifier(\"timesAssign\")");
        f3527z = identifier25;
        e identifier26 = e.identifier("divAssign");
        m.checkNotNullExpressionValue(identifier26, "identifier(\"divAssign\")");
        A = identifier26;
        e identifier27 = e.identifier("modAssign");
        m.checkNotNullExpressionValue(identifier27, "identifier(\"modAssign\")");
        B = identifier27;
        e identifier28 = e.identifier("remAssign");
        m.checkNotNullExpressionValue(identifier28, "identifier(\"remAssign\")");
        C = identifier28;
        e identifier29 = e.identifier("plusAssign");
        m.checkNotNullExpressionValue(identifier29, "identifier(\"plusAssign\")");
        D = identifier29;
        e identifier30 = e.identifier("minusAssign");
        m.checkNotNullExpressionValue(identifier30, "identifier(\"minusAssign\")");
        E = identifier30;
        n0.setOf((Object[]) new e[]{identifier13, identifier14, identifier19, identifier18, identifier17});
        F = n0.setOf((Object[]) new e[]{identifier19, identifier18, identifier17});
        G = n0.setOf((Object[]) new e[]{identifier20, identifier15, identifier16, identifier21, identifier22, identifier23, identifier24});
        H = n0.setOf((Object[]) new e[]{identifier25, identifier26, identifier27, identifier28, identifier29, identifier30});
        n0.setOf((Object[]) new e[]{identifier, identifier2, identifier3});
    }
}
