package d0.e0.p.d.m0.o;

import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.q0;
import d0.e0.p.d.m0.c.x;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.o.f;
import d0.e0.p.d.m0.o.k;
import d0.e0.p.d.m0.o.l;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: modifierChecks.kt */
/* loaded from: classes3.dex */
public final class i extends d0.e0.p.d.m0.o.a {
    public static final i a = new i();

    /* renamed from: b  reason: collision with root package name */
    public static final List<d> f3522b;

    /* compiled from: modifierChecks.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<x, String> {
        public static final a j = new a();

        public a() {
            super(1);
        }

        public final String invoke(x xVar) {
            Boolean bool;
            m.checkNotNullParameter(xVar, "<this>");
            List<c1> valueParameters = xVar.getValueParameters();
            m.checkNotNullExpressionValue(valueParameters, "valueParameters");
            c1 c1Var = (c1) u.lastOrNull((List<? extends Object>) valueParameters);
            if (c1Var == null) {
                bool = null;
            } else {
                bool = Boolean.valueOf(!d0.e0.p.d.m0.k.x.a.declaresOrInheritsDefaultValue(c1Var) && c1Var.getVarargElementType() == null);
            }
            boolean areEqual = m.areEqual(bool, Boolean.TRUE);
            i iVar = i.a;
            if (!areEqual) {
                return "last parameter should not have a default value or be a vararg";
            }
            return null;
        }
    }

    /* compiled from: modifierChecks.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function1<x, String> {
        public static final b j = new b();

        public b() {
            super(1);
        }

        /* JADX WARN: Code restructure failed: missing block: B:23:0x0060, code lost:
            if (r5 != false) goto L24;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public final java.lang.String invoke(d0.e0.p.d.m0.c.x r5) {
            /*
                r4 = this;
                java.lang.String r0 = "<this>"
                d0.z.d.m.checkNotNullParameter(r5, r0)
                d0.e0.p.d.m0.o.i r0 = d0.e0.p.d.m0.o.i.a
                d0.e0.p.d.m0.c.m r0 = r5.getContainingDeclaration()
                java.lang.String r1 = "containingDeclaration"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                boolean r1 = r0 instanceof d0.e0.p.d.m0.c.e
                r2 = 0
                r3 = 1
                if (r1 == 0) goto L20
                d0.e0.p.d.m0.c.e r0 = (d0.e0.p.d.m0.c.e) r0
                boolean r0 = d0.e0.p.d.m0.b.h.isAny(r0)
                if (r0 == 0) goto L20
                r0 = 1
                goto L21
            L20:
                r0 = 0
            L21:
                if (r0 != 0) goto L62
                java.util.Collection r5 = r5.getOverriddenDescriptors()
                java.lang.String r0 = "overriddenDescriptors"
                d0.z.d.m.checkNotNullExpressionValue(r5, r0)
                boolean r0 = r5.isEmpty()
                if (r0 == 0) goto L33
                goto L5f
            L33:
                java.util.Iterator r5 = r5.iterator()
            L37:
                boolean r0 = r5.hasNext()
                if (r0 == 0) goto L5f
                java.lang.Object r0 = r5.next()
                d0.e0.p.d.m0.c.x r0 = (d0.e0.p.d.m0.c.x) r0
                d0.e0.p.d.m0.c.m r0 = r0.getContainingDeclaration()
                java.lang.String r1 = "it.containingDeclaration"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                boolean r1 = r0 instanceof d0.e0.p.d.m0.c.e
                if (r1 == 0) goto L5a
                d0.e0.p.d.m0.c.e r0 = (d0.e0.p.d.m0.c.e) r0
                boolean r0 = d0.e0.p.d.m0.b.h.isAny(r0)
                if (r0 == 0) goto L5a
                r0 = 1
                goto L5b
            L5a:
                r0 = 0
            L5b:
                if (r0 == 0) goto L37
                r5 = 1
                goto L60
            L5f:
                r5 = 0
            L60:
                if (r5 == 0) goto L63
            L62:
                r2 = 1
            L63:
                if (r2 != 0) goto L68
                java.lang.String r5 = "must override ''equals()'' in Any"
                goto L69
            L68:
                r5 = 0
            L69:
                return r5
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.o.i.b.invoke(d0.e0.p.d.m0.c.x):java.lang.String");
        }
    }

    /* compiled from: modifierChecks.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function1<x, String> {
        public static final c j = new c();

        public c() {
            super(1);
        }

        public final String invoke(x xVar) {
            boolean z2;
            m.checkNotNullParameter(xVar, "<this>");
            q0 dispatchReceiverParameter = xVar.getDispatchReceiverParameter();
            if (dispatchReceiverParameter == null) {
                dispatchReceiverParameter = xVar.getExtensionReceiverParameter();
            }
            i iVar = i.a;
            boolean z3 = false;
            if (dispatchReceiverParameter != null) {
                c0 returnType = xVar.getReturnType();
                if (returnType == null) {
                    z2 = false;
                } else {
                    c0 type = dispatchReceiverParameter.getType();
                    m.checkNotNullExpressionValue(type, "receiver.type");
                    z2 = d0.e0.p.d.m0.n.o1.a.isSubtypeOf(returnType, type);
                }
                if (z2) {
                    z3 = true;
                }
            }
            if (!z3) {
                return "receiver must be a supertype of the return type";
            }
            return null;
        }
    }

    static {
        e eVar = j.i;
        f.b bVar = f.b.f3521b;
        d0.e0.p.d.m0.o.b[] bVarArr = {bVar, new l.a(1)};
        e eVar2 = j.j;
        d0.e0.p.d.m0.o.b[] bVarArr2 = {bVar, new l.a(2)};
        e eVar3 = j.a;
        h hVar = h.a;
        e eVar4 = e.a;
        e eVar5 = j.f;
        l.d dVar = l.d.f3532b;
        k.a aVar = k.a.d;
        e eVar6 = j.h;
        l.c cVar = l.c.f3531b;
        f3522b = n.listOf((Object[]) new d[]{new d(eVar, bVarArr, (Function1) null, 4, (DefaultConstructorMarker) null), new d(eVar2, bVarArr2, a.j), new d(eVar3, new d0.e0.p.d.m0.o.b[]{bVar, hVar, new l.a(2), eVar4}, (Function1) null, 4, (DefaultConstructorMarker) null), new d(j.f3523b, new d0.e0.p.d.m0.o.b[]{bVar, hVar, new l.a(3), eVar4}, (Function1) null, 4, (DefaultConstructorMarker) null), new d(j.c, new d0.e0.p.d.m0.o.b[]{bVar, hVar, new l.b(2), eVar4}, (Function1) null, 4, (DefaultConstructorMarker) null), new d(j.g, new d0.e0.p.d.m0.o.b[]{bVar}, (Function1) null, 4, (DefaultConstructorMarker) null), new d(eVar5, new d0.e0.p.d.m0.o.b[]{bVar, dVar, hVar, aVar}, (Function1) null, 4, (DefaultConstructorMarker) null), new d(eVar6, new d0.e0.p.d.m0.o.b[]{bVar, cVar}, (Function1) null, 4, (DefaultConstructorMarker) null), new d(j.k, new d0.e0.p.d.m0.o.b[]{bVar, cVar}, (Function1) null, 4, (DefaultConstructorMarker) null), new d(j.l, new d0.e0.p.d.m0.o.b[]{bVar, cVar, aVar}, (Function1) null, 4, (DefaultConstructorMarker) null), new d(j.f3526y, new d0.e0.p.d.m0.o.b[]{bVar, dVar, hVar}, (Function1) null, 4, (DefaultConstructorMarker) null), new d(j.d, new d0.e0.p.d.m0.o.b[]{f.a.f3520b}, b.j), new d(j.e, new d0.e0.p.d.m0.o.b[]{bVar, k.b.d, dVar, hVar}, (Function1) null, 4, (DefaultConstructorMarker) null), new d(j.G, new d0.e0.p.d.m0.o.b[]{bVar, dVar, hVar}, (Function1) null, 4, (DefaultConstructorMarker) null), new d(j.F, new d0.e0.p.d.m0.o.b[]{bVar, cVar}, (Function1) null, 4, (DefaultConstructorMarker) null), new d(n.listOf((Object[]) new e[]{j.n, j.o}), new d0.e0.p.d.m0.o.b[]{bVar}, c.j), new d(j.H, new d0.e0.p.d.m0.o.b[]{bVar, k.c.d, dVar, hVar}, (Function1) null, 4, (DefaultConstructorMarker) null), new d(j.m, new d0.e0.p.d.m0.o.b[]{bVar, cVar}, (Function1) null, 4, (DefaultConstructorMarker) null)});
    }

    @Override // d0.e0.p.d.m0.o.a
    public List<d> getChecks$descriptors() {
        return f3522b;
    }
}
