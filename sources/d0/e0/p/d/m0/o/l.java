package d0.e0.p.d.m0.o;

import d0.e0.p.d.m0.c.x;
import d0.e0.p.d.m0.o.b;
import d0.z.d.m;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: modifierChecks.kt */
/* loaded from: classes3.dex */
public abstract class l implements d0.e0.p.d.m0.o.b {
    public final String a;

    /* compiled from: modifierChecks.kt */
    /* loaded from: classes3.dex */
    public static final class a extends l {

        /* renamed from: b  reason: collision with root package name */
        public final int f3529b;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public a(int r3) {
            /*
                r2 = this;
                java.lang.String r0 = "must have at least "
                java.lang.String r1 = " value parameter"
                java.lang.StringBuilder r0 = b.d.b.a.a.S(r0, r3, r1)
                r1 = 1
                if (r3 <= r1) goto Le
                java.lang.String r1 = "s"
                goto L10
            Le:
                java.lang.String r1 = ""
            L10:
                r0.append(r1)
                java.lang.String r0 = r0.toString()
                r1 = 0
                r2.<init>(r0, r1)
                r2.f3529b = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.o.l.a.<init>(int):void");
        }

        @Override // d0.e0.p.d.m0.o.b
        public boolean check(x xVar) {
            m.checkNotNullParameter(xVar, "functionDescriptor");
            return xVar.getValueParameters().size() >= this.f3529b;
        }
    }

    /* compiled from: modifierChecks.kt */
    /* loaded from: classes3.dex */
    public static final class b extends l {

        /* renamed from: b  reason: collision with root package name */
        public final int f3530b;

        public b(int i) {
            super(b.d.b.a.a.q("must have exactly ", i, " value parameters"), null);
            this.f3530b = i;
        }

        @Override // d0.e0.p.d.m0.o.b
        public boolean check(x xVar) {
            m.checkNotNullParameter(xVar, "functionDescriptor");
            return xVar.getValueParameters().size() == this.f3530b;
        }
    }

    /* compiled from: modifierChecks.kt */
    /* loaded from: classes3.dex */
    public static final class c extends l {

        /* renamed from: b  reason: collision with root package name */
        public static final c f3531b = new c();

        public c() {
            super("must have no value parameters", null);
        }

        @Override // d0.e0.p.d.m0.o.b
        public boolean check(x xVar) {
            m.checkNotNullParameter(xVar, "functionDescriptor");
            return xVar.getValueParameters().isEmpty();
        }
    }

    /* compiled from: modifierChecks.kt */
    /* loaded from: classes3.dex */
    public static final class d extends l {

        /* renamed from: b  reason: collision with root package name */
        public static final d f3532b = new d();

        public d() {
            super("must have a single value parameter", null);
        }

        @Override // d0.e0.p.d.m0.o.b
        public boolean check(x xVar) {
            m.checkNotNullParameter(xVar, "functionDescriptor");
            return xVar.getValueParameters().size() == 1;
        }
    }

    public l(String str, DefaultConstructorMarker defaultConstructorMarker) {
        this.a = str;
    }

    @Override // d0.e0.p.d.m0.o.b
    public String getDescription() {
        return this.a;
    }

    @Override // d0.e0.p.d.m0.o.b
    public String invoke(x xVar) {
        return b.a.invoke(this, xVar);
    }
}
