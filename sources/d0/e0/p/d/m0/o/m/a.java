package d0.e0.p.d.m0.o.m;

import d0.g0.t;
import d0.g0.w;
import d0.z.d.m;
import java.util.Iterator;
import java.util.Objects;
/* compiled from: capitalizeDecapitalize.kt */
/* loaded from: classes3.dex */
public final class a {
    public static final boolean a(String str, int i, boolean z2) {
        char charAt = str.charAt(i);
        if (z2) {
            return 'A' <= charAt && charAt <= 'Z';
        }
        return Character.isUpperCase(charAt);
    }

    public static final String b(String str, boolean z2) {
        if (z2) {
            return toLowerCaseAsciiOnly(str);
        }
        Objects.requireNonNull(str, "null cannot be cast to non-null type java.lang.String");
        String lowerCase = str.toLowerCase();
        m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase()");
        return lowerCase;
    }

    public static final String capitalizeAsciiOnly(String str) {
        m.checkNotNullParameter(str, "<this>");
        boolean z2 = false;
        if (str.length() == 0) {
            return str;
        }
        char charAt = str.charAt(0);
        if ('a' <= charAt && charAt <= 'z') {
            z2 = true;
        }
        if (!z2) {
            return str;
        }
        char upperCase = Character.toUpperCase(charAt);
        String substring = str.substring(1);
        m.checkNotNullExpressionValue(substring, "(this as java.lang.String).substring(startIndex)");
        return String.valueOf(upperCase) + substring;
    }

    public static final String decapitalizeAsciiOnly(String str) {
        m.checkNotNullParameter(str, "<this>");
        boolean z2 = false;
        if (str.length() == 0) {
            return str;
        }
        char charAt = str.charAt(0);
        if ('A' <= charAt && charAt <= 'Z') {
            z2 = true;
        }
        if (!z2) {
            return str;
        }
        char lowerCase = Character.toLowerCase(charAt);
        String substring = str.substring(1);
        m.checkNotNullExpressionValue(substring, "(this as java.lang.String).substring(startIndex)");
        return String.valueOf(lowerCase) + substring;
    }

    public static final String decapitalizeSmartForCompiler(String str, boolean z2) {
        Integer num;
        m.checkNotNullParameter(str, "<this>");
        if ((str.length() == 0) || !a(str, 0, z2)) {
            return str;
        }
        if (str.length() == 1 || !a(str, 1, z2)) {
            return z2 ? decapitalizeAsciiOnly(str) : t.decapitalize(str);
        }
        Iterator<Integer> it = w.getIndices(str).iterator();
        while (true) {
            if (!it.hasNext()) {
                num = null;
                break;
            }
            num = it.next();
            if (!a(str, num.intValue(), z2)) {
                break;
            }
        }
        Integer num2 = num;
        if (num2 == null) {
            return b(str, z2);
        }
        int intValue = num2.intValue() - 1;
        String substring = str.substring(0, intValue);
        m.checkNotNullExpressionValue(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        String b2 = b(substring, z2);
        String substring2 = str.substring(intValue);
        m.checkNotNullExpressionValue(substring2, "(this as java.lang.String).substring(startIndex)");
        return m.stringPlus(b2, substring2);
    }

    public static final String toLowerCaseAsciiOnly(String str) {
        m.checkNotNullParameter(str, "<this>");
        StringBuilder sb = new StringBuilder(str.length());
        int length = str.length();
        int i = 0;
        while (i < length) {
            char charAt = str.charAt(i);
            i++;
            if ('A' <= charAt && charAt <= 'Z') {
                charAt = Character.toLowerCase(charAt);
            }
            sb.append(charAt);
        }
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "builder.toString()");
        return sb2;
    }
}
