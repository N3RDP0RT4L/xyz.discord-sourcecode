package d0.e0.p.d.m0.o;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.x;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.o.c;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Collection;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.text.Regex;
/* compiled from: modifierChecks.kt */
/* loaded from: classes3.dex */
public final class d {
    public final e a;

    /* renamed from: b  reason: collision with root package name */
    public final Regex f3519b;
    public final Collection<e> c;
    public final Function1<x, String> d;
    public final d0.e0.p.d.m0.o.b[] e;

    /* compiled from: modifierChecks.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1 {
        public static final a j = new a();

        public a() {
            super(1);
        }

        public final Void invoke(x xVar) {
            m.checkNotNullParameter(xVar, "<this>");
            return null;
        }
    }

    /* compiled from: modifierChecks.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function1 {
        public static final b j = new b();

        public b() {
            super(1);
        }

        public final Void invoke(x xVar) {
            m.checkNotNullParameter(xVar, "<this>");
            return null;
        }
    }

    /* compiled from: modifierChecks.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function1 {
        public static final c j = new c();

        public c() {
            super(1);
        }

        public final Void invoke(x xVar) {
            m.checkNotNullParameter(xVar, "<this>");
            return null;
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public d(e eVar, Regex regex, Collection<e> collection, Function1<? super x, String> function1, d0.e0.p.d.m0.o.b... bVarArr) {
        this.a = null;
        this.f3519b = regex;
        this.c = collection;
        this.d = function1;
        this.e = bVarArr;
    }

    public final d0.e0.p.d.m0.o.c checkAll(x xVar) {
        m.checkNotNullParameter(xVar, "functionDescriptor");
        d0.e0.p.d.m0.o.b[] bVarArr = this.e;
        int length = bVarArr.length;
        int i = 0;
        while (i < length) {
            d0.e0.p.d.m0.o.b bVar = bVarArr[i];
            i++;
            String invoke = bVar.invoke(xVar);
            if (invoke != null) {
                return new c.b(invoke);
            }
        }
        String invoke2 = this.d.invoke(xVar);
        if (invoke2 != null) {
            return new c.b(invoke2);
        }
        return c.C0366c.f3518b;
    }

    public final boolean isApplicable(x xVar) {
        m.checkNotNullParameter(xVar, "functionDescriptor");
        if (this.a != null && !m.areEqual(xVar.getName(), this.a)) {
            return false;
        }
        if (this.f3519b != null) {
            String asString = xVar.getName().asString();
            m.checkNotNullExpressionValue(asString, "functionDescriptor.name.asString()");
            if (!this.f3519b.matches(asString)) {
                return false;
            }
        }
        Collection<e> collection = this.c;
        return collection == null || collection.contains(xVar.getName());
    }

    public /* synthetic */ d(e eVar, d0.e0.p.d.m0.o.b[] bVarArr, Function1 function1, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(eVar, bVarArr, (i & 4) != 0 ? a.j : function1);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public d(e eVar, d0.e0.p.d.m0.o.b[] bVarArr, Function1<? super x, String> function1) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(bVarArr, "checks");
        m.checkNotNullParameter(function1, "additionalChecks");
        d0.e0.p.d.m0.o.b[] bVarArr2 = new d0.e0.p.d.m0.o.b[bVarArr.length];
        System.arraycopy(bVarArr, 0, bVarArr2, 0, bVarArr.length);
        this.a = eVar;
        this.f3519b = null;
        this.c = null;
        this.d = function1;
        this.e = bVarArr2;
    }

    public /* synthetic */ d(Regex regex, d0.e0.p.d.m0.o.b[] bVarArr, Function1 function1, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(regex, bVarArr, (i & 4) != 0 ? b.j : function1);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public d(kotlin.text.Regex r8, d0.e0.p.d.m0.o.b[] r9, kotlin.jvm.functions.Function1<? super d0.e0.p.d.m0.c.x, java.lang.String> r10) {
        /*
            r7 = this;
            java.lang.String r0 = "regex"
            d0.z.d.m.checkNotNullParameter(r8, r0)
            java.lang.String r0 = "checks"
            d0.z.d.m.checkNotNullParameter(r9, r0)
            java.lang.String r0 = "additionalChecks"
            d0.z.d.m.checkNotNullParameter(r10, r0)
            int r0 = r9.length
            d0.e0.p.d.m0.o.b[] r6 = new d0.e0.p.d.m0.o.b[r0]
            int r0 = r9.length
            r1 = 0
            java.lang.System.arraycopy(r9, r1, r6, r1, r0)
            r2 = 0
            r4 = 0
            r1 = r7
            r3 = r8
            r5 = r10
            r1.<init>(r2, r3, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.o.d.<init>(kotlin.text.Regex, d0.e0.p.d.m0.o.b[], kotlin.jvm.functions.Function1):void");
    }

    public /* synthetic */ d(Collection collection, d0.e0.p.d.m0.o.b[] bVarArr, Function1 function1, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(collection, bVarArr, (i & 4) != 0 ? c.j : function1);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public d(java.util.Collection<d0.e0.p.d.m0.g.e> r8, d0.e0.p.d.m0.o.b[] r9, kotlin.jvm.functions.Function1<? super d0.e0.p.d.m0.c.x, java.lang.String> r10) {
        /*
            r7 = this;
            java.lang.String r0 = "nameList"
            d0.z.d.m.checkNotNullParameter(r8, r0)
            java.lang.String r0 = "checks"
            d0.z.d.m.checkNotNullParameter(r9, r0)
            java.lang.String r0 = "additionalChecks"
            d0.z.d.m.checkNotNullParameter(r10, r0)
            int r0 = r9.length
            d0.e0.p.d.m0.o.b[] r6 = new d0.e0.p.d.m0.o.b[r0]
            int r0 = r9.length
            r1 = 0
            java.lang.System.arraycopy(r9, r1, r6, r1, r0)
            r2 = 0
            r3 = 0
            r1 = r7
            r4 = r8
            r5 = r10
            r1.<init>(r2, r3, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.o.d.<init>(java.util.Collection, d0.e0.p.d.m0.o.b[], kotlin.jvm.functions.Function1):void");
    }
}
