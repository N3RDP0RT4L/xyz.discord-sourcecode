package d0.e0.p.d.m0.j;

import d0.e0.p.d.m0.n.c0;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.jvm.functions.Function1;
/* compiled from: DescriptorRendererImpl.kt */
/* loaded from: classes3.dex */
public final class f extends o implements Function1<c0, CharSequence> {
    public final /* synthetic */ d this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public f(d dVar) {
        super(1);
        this.this$0 = dVar;
    }

    public final CharSequence invoke(c0 c0Var) {
        d dVar = this.this$0;
        m.checkNotNullExpressionValue(c0Var, "it");
        return dVar.renderType(c0Var);
    }
}
