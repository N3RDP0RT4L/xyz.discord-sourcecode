package d0.e0.p.d.m0.j;

import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.y0;
import d0.e0.p.d.m0.j.b;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.w0;
import d0.t.n0;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: DescriptorRenderer.kt */
/* loaded from: classes3.dex */
public abstract class c {
    public static final k a;

    /* renamed from: b */
    public static final c f3411b;
    public static final c c;

    /* compiled from: DescriptorRenderer.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<d0.e0.p.d.m0.j.h, Unit> {
        public static final a j = new a();

        public a() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(d0.e0.p.d.m0.j.h hVar) {
            invoke2(hVar);
            return Unit.a;
        }

        /* renamed from: invoke */
        public final void invoke2(d0.e0.p.d.m0.j.h hVar) {
            m.checkNotNullParameter(hVar, "<this>");
            hVar.setWithDefinedIn(false);
            hVar.setModifiers(n0.emptySet());
        }
    }

    /* compiled from: DescriptorRenderer.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function1<d0.e0.p.d.m0.j.h, Unit> {
        public static final b j = new b();

        public b() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(d0.e0.p.d.m0.j.h hVar) {
            invoke2(hVar);
            return Unit.a;
        }

        /* renamed from: invoke */
        public final void invoke2(d0.e0.p.d.m0.j.h hVar) {
            m.checkNotNullParameter(hVar, "<this>");
            hVar.setWithDefinedIn(false);
            hVar.setModifiers(n0.emptySet());
            hVar.setWithoutSuperTypes(true);
        }
    }

    /* compiled from: DescriptorRenderer.kt */
    /* renamed from: d0.e0.p.d.m0.j.c$c */
    /* loaded from: classes3.dex */
    public static final class C0340c extends o implements Function1<d0.e0.p.d.m0.j.h, Unit> {
        public static final C0340c j = new C0340c();

        public C0340c() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(d0.e0.p.d.m0.j.h hVar) {
            invoke2(hVar);
            return Unit.a;
        }

        /* renamed from: invoke */
        public final void invoke2(d0.e0.p.d.m0.j.h hVar) {
            m.checkNotNullParameter(hVar, "<this>");
            hVar.setWithDefinedIn(false);
        }
    }

    /* compiled from: DescriptorRenderer.kt */
    /* loaded from: classes3.dex */
    public static final class d extends o implements Function1<d0.e0.p.d.m0.j.h, Unit> {
        public static final d j = new d();

        public d() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(d0.e0.p.d.m0.j.h hVar) {
            invoke2(hVar);
            return Unit.a;
        }

        /* renamed from: invoke */
        public final void invoke2(d0.e0.p.d.m0.j.h hVar) {
            m.checkNotNullParameter(hVar, "<this>");
            hVar.setModifiers(n0.emptySet());
            hVar.setClassifierNamePolicy(b.C0339b.a);
            hVar.setParameterNameRenderingPolicy(n.ONLY_NON_SYNTHESIZED);
        }
    }

    /* compiled from: DescriptorRenderer.kt */
    /* loaded from: classes3.dex */
    public static final class e extends o implements Function1<d0.e0.p.d.m0.j.h, Unit> {
        public static final e j = new e();

        public e() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(d0.e0.p.d.m0.j.h hVar) {
            invoke2(hVar);
            return Unit.a;
        }

        /* renamed from: invoke */
        public final void invoke2(d0.e0.p.d.m0.j.h hVar) {
            m.checkNotNullParameter(hVar, "<this>");
            hVar.setDebugMode(true);
            hVar.setClassifierNamePolicy(b.a.a);
            hVar.setModifiers(d0.e0.p.d.m0.j.g.k);
        }
    }

    /* compiled from: DescriptorRenderer.kt */
    /* loaded from: classes3.dex */
    public static final class f extends o implements Function1<d0.e0.p.d.m0.j.h, Unit> {
        public static final f j = new f();

        public f() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(d0.e0.p.d.m0.j.h hVar) {
            invoke2(hVar);
            return Unit.a;
        }

        /* renamed from: invoke */
        public final void invoke2(d0.e0.p.d.m0.j.h hVar) {
            m.checkNotNullParameter(hVar, "<this>");
            hVar.setModifiers(d0.e0.p.d.m0.j.g.j);
        }
    }

    /* compiled from: DescriptorRenderer.kt */
    /* loaded from: classes3.dex */
    public static final class g extends o implements Function1<d0.e0.p.d.m0.j.h, Unit> {
        public static final g j = new g();

        public g() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(d0.e0.p.d.m0.j.h hVar) {
            invoke2(hVar);
            return Unit.a;
        }

        /* renamed from: invoke */
        public final void invoke2(d0.e0.p.d.m0.j.h hVar) {
            m.checkNotNullParameter(hVar, "<this>");
            hVar.setModifiers(d0.e0.p.d.m0.j.g.k);
        }
    }

    /* compiled from: DescriptorRenderer.kt */
    /* loaded from: classes3.dex */
    public static final class h extends o implements Function1<d0.e0.p.d.m0.j.h, Unit> {
        public static final h j = new h();

        public h() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(d0.e0.p.d.m0.j.h hVar) {
            invoke2(hVar);
            return Unit.a;
        }

        /* renamed from: invoke */
        public final void invoke2(d0.e0.p.d.m0.j.h hVar) {
            m.checkNotNullParameter(hVar, "<this>");
            hVar.setTextFormat(p.HTML);
            hVar.setModifiers(d0.e0.p.d.m0.j.g.k);
        }
    }

    /* compiled from: DescriptorRenderer.kt */
    /* loaded from: classes3.dex */
    public static final class i extends o implements Function1<d0.e0.p.d.m0.j.h, Unit> {
        public static final i j = new i();

        public i() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(d0.e0.p.d.m0.j.h hVar) {
            invoke2(hVar);
            return Unit.a;
        }

        /* renamed from: invoke */
        public final void invoke2(d0.e0.p.d.m0.j.h hVar) {
            m.checkNotNullParameter(hVar, "<this>");
            hVar.setWithDefinedIn(false);
            hVar.setModifiers(n0.emptySet());
            hVar.setClassifierNamePolicy(b.C0339b.a);
            hVar.setWithoutTypeParameters(true);
            hVar.setParameterNameRenderingPolicy(n.NONE);
            hVar.setReceiverAfterName(true);
            hVar.setRenderCompanionObjectName(true);
            hVar.setWithoutSuperTypes(true);
            hVar.setStartFromName(true);
        }
    }

    /* compiled from: DescriptorRenderer.kt */
    /* loaded from: classes3.dex */
    public static final class j extends o implements Function1<d0.e0.p.d.m0.j.h, Unit> {
        public static final j j = new j();

        public j() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(d0.e0.p.d.m0.j.h hVar) {
            invoke2(hVar);
            return Unit.a;
        }

        /* renamed from: invoke */
        public final void invoke2(d0.e0.p.d.m0.j.h hVar) {
            m.checkNotNullParameter(hVar, "<this>");
            hVar.setClassifierNamePolicy(b.C0339b.a);
            hVar.setParameterNameRenderingPolicy(n.ONLY_NON_SYNTHESIZED);
        }
    }

    /* compiled from: DescriptorRenderer.kt */
    /* loaded from: classes3.dex */
    public static final class k {
        public k(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final String getClassifierKindPrefix(d0.e0.p.d.m0.c.i iVar) {
            m.checkNotNullParameter(iVar, "classifier");
            if (iVar instanceof y0) {
                return "typealias";
            }
            if (iVar instanceof d0.e0.p.d.m0.c.e) {
                d0.e0.p.d.m0.c.e eVar = (d0.e0.p.d.m0.c.e) iVar;
                if (eVar.isCompanionObject()) {
                    return "companion object";
                }
                int ordinal = eVar.getKind().ordinal();
                if (ordinal == 0) {
                    return "class";
                }
                if (ordinal == 1) {
                    return "interface";
                }
                if (ordinal == 2) {
                    return "enum class";
                }
                if (ordinal == 3) {
                    return "enum entry";
                }
                if (ordinal == 4) {
                    return "annotation class";
                }
                if (ordinal == 5) {
                    return "object";
                }
                throw new NoWhenBranchMatchedException();
            }
            throw new AssertionError(m.stringPlus("Unexpected classifier: ", iVar));
        }

        public final c withOptions(Function1<? super d0.e0.p.d.m0.j.h, Unit> function1) {
            m.checkNotNullParameter(function1, "changeOptions");
            d0.e0.p.d.m0.j.i iVar = new d0.e0.p.d.m0.j.i();
            function1.invoke(iVar);
            iVar.lock();
            return new d0.e0.p.d.m0.j.d(iVar);
        }
    }

    /* compiled from: DescriptorRenderer.kt */
    /* loaded from: classes3.dex */
    public interface l {

        /* compiled from: DescriptorRenderer.kt */
        /* loaded from: classes3.dex */
        public static final class a implements l {
            public static final a a = new a();

            @Override // d0.e0.p.d.m0.j.c.l
            public void appendAfterValueParameter(c1 c1Var, int i, int i2, StringBuilder sb) {
                m.checkNotNullParameter(c1Var, "parameter");
                m.checkNotNullParameter(sb, "builder");
                if (i != i2 - 1) {
                    sb.append(", ");
                }
            }

            @Override // d0.e0.p.d.m0.j.c.l
            public void appendAfterValueParameters(int i, StringBuilder sb) {
                m.checkNotNullParameter(sb, "builder");
                sb.append(")");
            }

            @Override // d0.e0.p.d.m0.j.c.l
            public void appendBeforeValueParameter(c1 c1Var, int i, int i2, StringBuilder sb) {
                m.checkNotNullParameter(c1Var, "parameter");
                m.checkNotNullParameter(sb, "builder");
            }

            @Override // d0.e0.p.d.m0.j.c.l
            public void appendBeforeValueParameters(int i, StringBuilder sb) {
                m.checkNotNullParameter(sb, "builder");
                sb.append("(");
            }
        }

        void appendAfterValueParameter(c1 c1Var, int i, int i2, StringBuilder sb);

        void appendAfterValueParameters(int i, StringBuilder sb);

        void appendBeforeValueParameter(c1 c1Var, int i, int i2, StringBuilder sb);

        void appendBeforeValueParameters(int i, StringBuilder sb);
    }

    static {
        k kVar = new k(null);
        a = kVar;
        kVar.withOptions(C0340c.j);
        kVar.withOptions(a.j);
        kVar.withOptions(b.j);
        kVar.withOptions(d.j);
        kVar.withOptions(i.j);
        f3411b = kVar.withOptions(f.j);
        kVar.withOptions(g.j);
        kVar.withOptions(j.j);
        c = kVar.withOptions(e.j);
        kVar.withOptions(h.j);
    }

    public static /* synthetic */ String renderAnnotation$default(c cVar, d0.e0.p.d.m0.c.g1.c cVar2, d0.e0.p.d.m0.c.g1.e eVar, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 2) != 0) {
                eVar = null;
            }
            return cVar.renderAnnotation(cVar2, eVar);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: renderAnnotation");
    }

    public abstract String render(d0.e0.p.d.m0.c.m mVar);

    public abstract String renderAnnotation(d0.e0.p.d.m0.c.g1.c cVar, d0.e0.p.d.m0.c.g1.e eVar);

    public abstract String renderFlexibleType(String str, String str2, d0.e0.p.d.m0.b.h hVar);

    public abstract String renderFqName(d0.e0.p.d.m0.g.c cVar);

    public abstract String renderName(d0.e0.p.d.m0.g.e eVar, boolean z2);

    public abstract String renderType(c0 c0Var);

    public abstract String renderTypeProjection(w0 w0Var);

    public final c withOptions(Function1<? super d0.e0.p.d.m0.j.h, Unit> function1) {
        m.checkNotNullParameter(function1, "changeOptions");
        d0.e0.p.d.m0.j.i copy = ((d0.e0.p.d.m0.j.d) this).getOptions().copy();
        function1.invoke(copy);
        copy.lock();
        return new d0.e0.p.d.m0.j.d(copy);
    }
}
