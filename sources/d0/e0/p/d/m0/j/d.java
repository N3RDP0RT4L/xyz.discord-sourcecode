package d0.e0.p.d.m0.j;

import andhook.lib.xposed.ClassUtils;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.e0.p.d.m0.b.h;
import d0.e0.p.d.m0.b.k;
import d0.e0.p.d.m0.c.a1;
import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.d0;
import d0.e0.p.d.m0.c.d1;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.e0;
import d0.e0.p.d.m0.c.f;
import d0.e0.p.d.m0.c.i;
import d0.e0.p.d.m0.c.j0;
import d0.e0.p.d.m0.c.l;
import d0.e0.p.d.m0.c.l0;
import d0.e0.p.d.m0.c.m0;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.o;
import d0.e0.p.d.m0.c.o0;
import d0.e0.p.d.m0.c.p;
import d0.e0.p.d.m0.c.p0;
import d0.e0.p.d.m0.c.q0;
import d0.e0.p.d.m0.c.t;
import d0.e0.p.d.m0.c.u;
import d0.e0.p.d.m0.c.v;
import d0.e0.p.d.m0.c.v0;
import d0.e0.p.d.m0.c.x;
import d0.e0.p.d.m0.c.y;
import d0.e0.p.d.m0.c.y0;
import d0.e0.p.d.m0.c.z;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.j.c;
import d0.e0.p.d.m0.k.v.r;
import d0.e0.p.d.m0.n.e1;
import d0.e0.p.d.m0.n.h1;
import d0.e0.p.d.m0.n.i1;
import d0.e0.p.d.m0.n.j1;
import d0.e0.p.d.m0.n.s;
import d0.e0.p.d.m0.n.u0;
import d0.e0.p.d.m0.n.w0;
import d0.g;
import d0.g0.w;
import d0.t.n;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import kotlin.Lazy;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: DescriptorRendererImpl.kt */
/* loaded from: classes3.dex */
public final class d extends d0.e0.p.d.m0.j.c implements h {
    public final i d;
    public final Lazy e = g.lazy(new c());

    /* compiled from: DescriptorRendererImpl.kt */
    /* loaded from: classes3.dex */
    public final class a implements o<Unit, StringBuilder> {
        public final /* synthetic */ d a;

        public a(d dVar) {
            m.checkNotNullParameter(dVar, "this$0");
            this.a = dVar;
        }

        public final void a(m0 m0Var, StringBuilder sb, String str) {
            int ordinal = this.a.getPropertyAccessorRenderingPolicy().ordinal();
            if (ordinal == 0) {
                this.a.p(m0Var, sb);
                sb.append(m.stringPlus(str, " for "));
                d dVar = this.a;
                n0 correspondingProperty = m0Var.getCorrespondingProperty();
                m.checkNotNullExpressionValue(correspondingProperty, "descriptor.correspondingProperty");
                d.access$renderProperty(dVar, correspondingProperty, sb);
            } else if (ordinal == 1) {
                visitFunctionDescriptor2((x) m0Var, sb);
            }
        }

        @Override // d0.e0.p.d.m0.c.o
        public /* bridge */ /* synthetic */ Unit visitClassDescriptor(e eVar, StringBuilder sb) {
            visitClassDescriptor2(eVar, sb);
            return Unit.a;
        }

        @Override // d0.e0.p.d.m0.c.o
        public /* bridge */ /* synthetic */ Unit visitConstructorDescriptor(l lVar, StringBuilder sb) {
            visitConstructorDescriptor2(lVar, sb);
            return Unit.a;
        }

        @Override // d0.e0.p.d.m0.c.o
        public /* bridge */ /* synthetic */ Unit visitFunctionDescriptor(x xVar, StringBuilder sb) {
            visitFunctionDescriptor2(xVar, sb);
            return Unit.a;
        }

        @Override // d0.e0.p.d.m0.c.o
        public /* bridge */ /* synthetic */ Unit visitModuleDeclaration(c0 c0Var, StringBuilder sb) {
            visitModuleDeclaration2(c0Var, sb);
            return Unit.a;
        }

        @Override // d0.e0.p.d.m0.c.o
        public /* bridge */ /* synthetic */ Unit visitPackageFragmentDescriptor(e0 e0Var, StringBuilder sb) {
            visitPackageFragmentDescriptor2(e0Var, sb);
            return Unit.a;
        }

        @Override // d0.e0.p.d.m0.c.o
        public /* bridge */ /* synthetic */ Unit visitPackageViewDescriptor(j0 j0Var, StringBuilder sb) {
            visitPackageViewDescriptor2(j0Var, sb);
            return Unit.a;
        }

        @Override // d0.e0.p.d.m0.c.o
        public /* bridge */ /* synthetic */ Unit visitPropertyDescriptor(n0 n0Var, StringBuilder sb) {
            visitPropertyDescriptor2(n0Var, sb);
            return Unit.a;
        }

        @Override // d0.e0.p.d.m0.c.o
        public /* bridge */ /* synthetic */ Unit visitPropertyGetterDescriptor(o0 o0Var, StringBuilder sb) {
            visitPropertyGetterDescriptor2(o0Var, sb);
            return Unit.a;
        }

        @Override // d0.e0.p.d.m0.c.o
        public /* bridge */ /* synthetic */ Unit visitPropertySetterDescriptor(p0 p0Var, StringBuilder sb) {
            visitPropertySetterDescriptor2(p0Var, sb);
            return Unit.a;
        }

        @Override // d0.e0.p.d.m0.c.o
        public /* bridge */ /* synthetic */ Unit visitReceiverParameterDescriptor(q0 q0Var, StringBuilder sb) {
            visitReceiverParameterDescriptor2(q0Var, sb);
            return Unit.a;
        }

        @Override // d0.e0.p.d.m0.c.o
        public /* bridge */ /* synthetic */ Unit visitTypeAliasDescriptor(y0 y0Var, StringBuilder sb) {
            visitTypeAliasDescriptor2(y0Var, sb);
            return Unit.a;
        }

        @Override // d0.e0.p.d.m0.c.o
        public /* bridge */ /* synthetic */ Unit visitTypeParameterDescriptor(z0 z0Var, StringBuilder sb) {
            visitTypeParameterDescriptor2(z0Var, sb);
            return Unit.a;
        }

        @Override // d0.e0.p.d.m0.c.o
        public /* bridge */ /* synthetic */ Unit visitValueParameterDescriptor(c1 c1Var, StringBuilder sb) {
            visitValueParameterDescriptor2(c1Var, sb);
            return Unit.a;
        }

        /* renamed from: visitClassDescriptor  reason: avoid collision after fix types in other method */
        public void visitClassDescriptor2(e eVar, StringBuilder sb) {
            m.checkNotNullParameter(eVar, "descriptor");
            m.checkNotNullParameter(sb, "builder");
            d.access$renderClass(this.a, eVar, sb);
        }

        /* renamed from: visitConstructorDescriptor  reason: avoid collision after fix types in other method */
        public void visitConstructorDescriptor2(l lVar, StringBuilder sb) {
            m.checkNotNullParameter(lVar, "constructorDescriptor");
            m.checkNotNullParameter(sb, "builder");
            d.access$renderConstructor(this.a, lVar, sb);
        }

        /* renamed from: visitFunctionDescriptor  reason: avoid collision after fix types in other method */
        public void visitFunctionDescriptor2(x xVar, StringBuilder sb) {
            m.checkNotNullParameter(xVar, "descriptor");
            m.checkNotNullParameter(sb, "builder");
            d.access$renderFunction(this.a, xVar, sb);
        }

        /* renamed from: visitModuleDeclaration  reason: avoid collision after fix types in other method */
        public void visitModuleDeclaration2(c0 c0Var, StringBuilder sb) {
            m.checkNotNullParameter(c0Var, "descriptor");
            m.checkNotNullParameter(sb, "builder");
            this.a.t(c0Var, sb, true);
        }

        /* renamed from: visitPackageFragmentDescriptor  reason: avoid collision after fix types in other method */
        public void visitPackageFragmentDescriptor2(e0 e0Var, StringBuilder sb) {
            m.checkNotNullParameter(e0Var, "descriptor");
            m.checkNotNullParameter(sb, "builder");
            d.access$renderPackageFragment(this.a, e0Var, sb);
        }

        /* renamed from: visitPackageViewDescriptor  reason: avoid collision after fix types in other method */
        public void visitPackageViewDescriptor2(j0 j0Var, StringBuilder sb) {
            m.checkNotNullParameter(j0Var, "descriptor");
            m.checkNotNullParameter(sb, "builder");
            d.access$renderPackageView(this.a, j0Var, sb);
        }

        /* renamed from: visitPropertyDescriptor  reason: avoid collision after fix types in other method */
        public void visitPropertyDescriptor2(n0 n0Var, StringBuilder sb) {
            m.checkNotNullParameter(n0Var, "descriptor");
            m.checkNotNullParameter(sb, "builder");
            d.access$renderProperty(this.a, n0Var, sb);
        }

        /* renamed from: visitPropertyGetterDescriptor  reason: avoid collision after fix types in other method */
        public void visitPropertyGetterDescriptor2(o0 o0Var, StringBuilder sb) {
            m.checkNotNullParameter(o0Var, "descriptor");
            m.checkNotNullParameter(sb, "builder");
            a(o0Var, sb, "getter");
        }

        /* renamed from: visitPropertySetterDescriptor  reason: avoid collision after fix types in other method */
        public void visitPropertySetterDescriptor2(p0 p0Var, StringBuilder sb) {
            m.checkNotNullParameter(p0Var, "descriptor");
            m.checkNotNullParameter(sb, "builder");
            a(p0Var, sb, "setter");
        }

        /* renamed from: visitReceiverParameterDescriptor  reason: avoid collision after fix types in other method */
        public void visitReceiverParameterDescriptor2(q0 q0Var, StringBuilder sb) {
            m.checkNotNullParameter(q0Var, "descriptor");
            m.checkNotNullParameter(sb, "builder");
            sb.append(q0Var.getName());
        }

        /* renamed from: visitTypeAliasDescriptor  reason: avoid collision after fix types in other method */
        public void visitTypeAliasDescriptor2(y0 y0Var, StringBuilder sb) {
            m.checkNotNullParameter(y0Var, "descriptor");
            m.checkNotNullParameter(sb, "builder");
            d.access$renderTypeAlias(this.a, y0Var, sb);
        }

        /* renamed from: visitTypeParameterDescriptor  reason: avoid collision after fix types in other method */
        public void visitTypeParameterDescriptor2(z0 z0Var, StringBuilder sb) {
            m.checkNotNullParameter(z0Var, "descriptor");
            m.checkNotNullParameter(sb, "builder");
            this.a.C(z0Var, sb, true);
        }

        /* renamed from: visitValueParameterDescriptor  reason: avoid collision after fix types in other method */
        public void visitValueParameterDescriptor2(c1 c1Var, StringBuilder sb) {
            m.checkNotNullParameter(c1Var, "descriptor");
            m.checkNotNullParameter(sb, "builder");
            this.a.G(c1Var, true, sb, true);
        }
    }

    /* compiled from: DescriptorRendererImpl.kt */
    /* loaded from: classes3.dex */
    public static final class b extends d0.z.d.o implements Function1<w0, CharSequence> {
        public b() {
            super(1);
        }

        public final CharSequence invoke(w0 w0Var) {
            m.checkNotNullParameter(w0Var, "it");
            if (w0Var.isStarProjection()) {
                return "*";
            }
            d dVar = d.this;
            d0.e0.p.d.m0.n.c0 type = w0Var.getType();
            m.checkNotNullExpressionValue(type, "it.type");
            String renderType = dVar.renderType(type);
            if (w0Var.getProjectionKind() == j1.INVARIANT) {
                return renderType;
            }
            return w0Var.getProjectionKind() + ' ' + renderType;
        }
    }

    /* compiled from: DescriptorRendererImpl.kt */
    /* loaded from: classes3.dex */
    public static final class c extends d0.z.d.o implements Function0<d> {

        /* compiled from: DescriptorRendererImpl.kt */
        /* loaded from: classes3.dex */
        public static final class a extends d0.z.d.o implements Function1<h, Unit> {
            public static final a j = new a();

            public a() {
                super(1);
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(h hVar) {
                invoke2(hVar);
                return Unit.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(h hVar) {
                m.checkNotNullParameter(hVar, "<this>");
                hVar.setExcludedTypeAnnotationClasses(d0.t.o0.plus((Set) hVar.getExcludedTypeAnnotationClasses(), (Iterable) d0.t.m.listOf(k.a.f3200x)));
                hVar.setAnnotationArgumentsRenderingPolicy(d0.e0.p.d.m0.j.a.ALWAYS_PARENTHESIZED);
            }
        }

        public c() {
            super(0);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final d invoke() {
            return (d) d.this.withOptions(a.j);
        }
    }

    /* compiled from: DescriptorRendererImpl.kt */
    /* renamed from: d0.e0.p.d.m0.j.d$d  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0341d extends d0.z.d.o implements Function1<d0.e0.p.d.m0.k.v.g<?>, CharSequence> {
        public C0341d() {
            super(1);
        }

        public final CharSequence invoke(d0.e0.p.d.m0.k.v.g<?> gVar) {
            m.checkNotNullParameter(gVar, "it");
            return d.this.k(gVar);
        }
    }

    public d(i iVar) {
        m.checkNotNullParameter(iVar, "options");
        this.d = iVar;
        iVar.isLocked();
    }

    public static final void access$renderClass(d dVar, e eVar, StringBuilder sb) {
        d0.e0.p.d.m0.c.d unsubstitutedPrimaryConstructor;
        Objects.requireNonNull(dVar);
        boolean z2 = eVar.getKind() == f.ENUM_ENTRY;
        if (!dVar.getStartFromName()) {
            dVar.h(sb, eVar, null);
            if (!z2) {
                u visibility = eVar.getVisibility();
                m.checkNotNullExpressionValue(visibility, "klass.visibility");
                dVar.I(visibility, sb);
            }
            if (!(eVar.getKind() == f.INTERFACE && eVar.getModality() == z.ABSTRACT) && (!eVar.getKind().isSingleton() || eVar.getModality() != z.FINAL)) {
                z modality = eVar.getModality();
                m.checkNotNullExpressionValue(modality, "klass.modality");
                dVar.q(modality, sb, dVar.e(eVar));
            }
            dVar.p(eVar, sb);
            dVar.s(sb, dVar.getModifiers().contains(g.INNER) && eVar.isInner(), "inner");
            dVar.s(sb, dVar.getModifiers().contains(g.DATA) && eVar.isData(), "data");
            dVar.s(sb, dVar.getModifiers().contains(g.INLINE) && eVar.isInline(), "inline");
            dVar.s(sb, dVar.getModifiers().contains(g.VALUE) && eVar.isValue(), "value");
            dVar.s(sb, dVar.getModifiers().contains(g.FUN) && eVar.isFun(), "fun");
            sb.append(dVar.n(d0.e0.p.d.m0.j.c.a.getClassifierKindPrefix(eVar)));
        }
        if (!d0.e0.p.d.m0.k.e.isCompanionObject(eVar)) {
            if (!dVar.getStartFromName()) {
                dVar.B(sb);
            }
            dVar.t(eVar, sb, true);
        } else {
            if (dVar.getRenderCompanionObjectName()) {
                if (dVar.getStartFromName()) {
                    sb.append("companion object");
                }
                dVar.B(sb);
                d0.e0.p.d.m0.c.m containingDeclaration = eVar.getContainingDeclaration();
                if (containingDeclaration != null) {
                    sb.append("of ");
                    d0.e0.p.d.m0.g.e name = containingDeclaration.getName();
                    m.checkNotNullExpressionValue(name, "containingDeclaration.name");
                    sb.append(dVar.renderName(name, false));
                }
            }
            if (dVar.getVerbose() || !m.areEqual(eVar.getName(), d0.e0.p.d.m0.g.g.f3397b)) {
                if (!dVar.getStartFromName()) {
                    dVar.B(sb);
                }
                d0.e0.p.d.m0.g.e name2 = eVar.getName();
                m.checkNotNullExpressionValue(name2, "descriptor.name");
                sb.append(dVar.renderName(name2, true));
            }
        }
        if (!z2) {
            List<z0> declaredTypeParameters = eVar.getDeclaredTypeParameters();
            m.checkNotNullExpressionValue(declaredTypeParameters, "klass.declaredTypeParameters");
            dVar.E(declaredTypeParameters, sb, false);
            dVar.j(eVar, sb);
            if (!eVar.getKind().isSingleton() && dVar.getClassWithPrimaryConstructor() && (unsubstitutedPrimaryConstructor = eVar.getUnsubstitutedPrimaryConstructor()) != null) {
                sb.append(" ");
                dVar.h(sb, unsubstitutedPrimaryConstructor, null);
                u visibility2 = unsubstitutedPrimaryConstructor.getVisibility();
                m.checkNotNullExpressionValue(visibility2, "primaryConstructor.visibility");
                dVar.I(visibility2, sb);
                sb.append(dVar.n("constructor"));
                List<c1> valueParameters = unsubstitutedPrimaryConstructor.getValueParameters();
                m.checkNotNullExpressionValue(valueParameters, "primaryConstructor.valueParameters");
                dVar.H(valueParameters, unsubstitutedPrimaryConstructor.hasSynthesizedParameterNames(), sb);
            }
            if (!dVar.getWithoutSuperTypes() && !h.isNothing(eVar.getDefaultType())) {
                Collection<d0.e0.p.d.m0.n.c0> supertypes = eVar.getTypeConstructor().getSupertypes();
                m.checkNotNullExpressionValue(supertypes, "klass.typeConstructor.supertypes");
                if (!supertypes.isEmpty() && (supertypes.size() != 1 || !h.isAnyOrNullableAny(supertypes.iterator().next()))) {
                    dVar.B(sb);
                    sb.append(": ");
                    d0.t.u.joinTo$default(supertypes, sb, ", ", null, null, 0, null, new f(dVar), 60, null);
                }
            }
            dVar.J(declaredTypeParameters, sb);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:18:0x0049  */
    /* JADX WARN: Removed duplicated region for block: B:21:0x0063  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x00b3  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x00d6  */
    /* JADX WARN: Removed duplicated region for block: B:48:0x00ff  */
    /* JADX WARN: Removed duplicated region for block: B:55:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final void access$renderConstructor(d0.e0.p.d.m0.j.d r18, d0.e0.p.d.m0.c.l r19, java.lang.StringBuilder r20) {
        /*
            Method dump skipped, instructions count: 266
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.j.d.access$renderConstructor(d0.e0.p.d.m0.j.d, d0.e0.p.d.m0.c.l, java.lang.StringBuilder):void");
    }

    /* JADX WARN: Removed duplicated region for block: B:29:0x0077  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final void access$renderFunction(d0.e0.p.d.m0.j.d r7, d0.e0.p.d.m0.c.x r8, java.lang.StringBuilder r9) {
        /*
            Method dump skipped, instructions count: 342
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.j.d.access$renderFunction(d0.e0.p.d.m0.j.d, d0.e0.p.d.m0.c.x, java.lang.StringBuilder):void");
    }

    public static final void access$renderPackageFragment(d dVar, e0 e0Var, StringBuilder sb) {
        Objects.requireNonNull(dVar);
        dVar.x(e0Var.getFqName(), "package-fragment", sb);
        if (dVar.getDebugMode()) {
            sb.append(" in ");
            dVar.t(e0Var.getContainingDeclaration(), sb, false);
        }
    }

    public static final void access$renderPackageView(d dVar, j0 j0Var, StringBuilder sb) {
        Objects.requireNonNull(dVar);
        dVar.x(j0Var.getFqName(), "package", sb);
        if (dVar.getDebugMode()) {
            sb.append(" in context of ");
            dVar.t(j0Var.getModule(), sb, false);
        }
    }

    public static final void access$renderProperty(d dVar, n0 n0Var, StringBuilder sb) {
        if (!dVar.getStartFromName()) {
            if (!dVar.getStartFromDeclarationKeyword()) {
                if (dVar.getModifiers().contains(g.ANNOTATIONS)) {
                    dVar.h(sb, n0Var, null);
                    v backingField = n0Var.getBackingField();
                    if (backingField != null) {
                        dVar.h(sb, backingField, d0.e0.p.d.m0.c.g1.e.FIELD);
                    }
                    v delegateField = n0Var.getDelegateField();
                    if (delegateField != null) {
                        dVar.h(sb, delegateField, d0.e0.p.d.m0.c.g1.e.PROPERTY_DELEGATE_FIELD);
                    }
                    if (dVar.getPropertyAccessorRenderingPolicy() == o.NONE) {
                        o0 getter = n0Var.getGetter();
                        if (getter != null) {
                            dVar.h(sb, getter, d0.e0.p.d.m0.c.g1.e.PROPERTY_GETTER);
                        }
                        p0 setter = n0Var.getSetter();
                        if (setter != null) {
                            dVar.h(sb, setter, d0.e0.p.d.m0.c.g1.e.PROPERTY_SETTER);
                            List<c1> valueParameters = setter.getValueParameters();
                            m.checkNotNullExpressionValue(valueParameters, "setter.valueParameters");
                            c1 c1Var = (c1) d0.t.u.single((List<? extends Object>) valueParameters);
                            m.checkNotNullExpressionValue(c1Var, "it");
                            dVar.h(sb, c1Var, d0.e0.p.d.m0.c.g1.e.SETTER_PARAMETER);
                        }
                    }
                }
                u visibility = n0Var.getVisibility();
                m.checkNotNullExpressionValue(visibility, "property.visibility");
                dVar.I(visibility, sb);
                dVar.s(sb, dVar.getModifiers().contains(g.CONST) && n0Var.isConst(), "const");
                dVar.p(n0Var, sb);
                dVar.r(n0Var, sb);
                dVar.w(n0Var, sb);
                dVar.s(sb, dVar.getModifiers().contains(g.LATEINIT) && n0Var.isLateInit(), "lateinit");
                dVar.o(n0Var, sb);
            }
            dVar.F(n0Var, sb, false);
            List<z0> typeParameters = n0Var.getTypeParameters();
            m.checkNotNullExpressionValue(typeParameters, "property.typeParameters");
            dVar.E(typeParameters, sb, true);
            dVar.z(n0Var, sb);
        }
        dVar.t(n0Var, sb, true);
        sb.append(": ");
        d0.e0.p.d.m0.n.c0 type = n0Var.getType();
        m.checkNotNullExpressionValue(type, "property.type");
        sb.append(dVar.renderType(type));
        dVar.A(n0Var, sb);
        dVar.m(n0Var, sb);
        List<z0> typeParameters2 = n0Var.getTypeParameters();
        m.checkNotNullExpressionValue(typeParameters2, "property.typeParameters");
        dVar.J(typeParameters2, sb);
    }

    public static final void access$renderTypeAlias(d dVar, y0 y0Var, StringBuilder sb) {
        dVar.h(sb, y0Var, null);
        u visibility = y0Var.getVisibility();
        m.checkNotNullExpressionValue(visibility, "typeAlias.visibility");
        dVar.I(visibility, sb);
        dVar.p(y0Var, sb);
        sb.append(dVar.n("typealias"));
        sb.append(" ");
        dVar.t(y0Var, sb, true);
        List<z0> declaredTypeParameters = y0Var.getDeclaredTypeParameters();
        m.checkNotNullExpressionValue(declaredTypeParameters, "typeAlias.declaredTypeParameters");
        dVar.E(declaredTypeParameters, sb, false);
        dVar.j(y0Var, sb);
        sb.append(" = ");
        sb.append(dVar.renderType(y0Var.getUnderlyingType()));
    }

    public static /* synthetic */ void i(d dVar, StringBuilder sb, d0.e0.p.d.m0.c.g1.a aVar, d0.e0.p.d.m0.c.g1.e eVar, int i) {
        int i2 = i & 2;
        dVar.h(sb, aVar, null);
    }

    public final void A(d0.e0.p.d.m0.c.a aVar, StringBuilder sb) {
        q0 extensionReceiverParameter;
        if (getReceiverAfterName() && (extensionReceiverParameter = aVar.getExtensionReceiverParameter()) != null) {
            sb.append(" on ");
            d0.e0.p.d.m0.n.c0 type = extensionReceiverParameter.getType();
            m.checkNotNullExpressionValue(type, "receiver.type");
            sb.append(renderType(type));
        }
    }

    public final void B(StringBuilder sb) {
        int length = sb.length();
        if (length == 0 || sb.charAt(length - 1) != ' ') {
            sb.append(' ');
        }
    }

    public final void C(z0 z0Var, StringBuilder sb, boolean z2) {
        if (z2) {
            sb.append(f());
        }
        if (getVerbose()) {
            sb.append("/*");
            sb.append(z0Var.getIndex());
            sb.append("*/ ");
        }
        s(sb, z0Var.isReified(), "reified");
        String label = z0Var.getVariance().getLabel();
        boolean z3 = true;
        s(sb, label.length() > 0, label);
        h(sb, z0Var, null);
        t(z0Var, sb, z2);
        int size = z0Var.getUpperBounds().size();
        if ((size > 1 && !z2) || size == 1) {
            d0.e0.p.d.m0.n.c0 next = z0Var.getUpperBounds().iterator().next();
            if (!h.isDefaultBound(next)) {
                sb.append(" : ");
                m.checkNotNullExpressionValue(next, "upperBound");
                sb.append(renderType(next));
            }
        } else if (z2) {
            for (d0.e0.p.d.m0.n.c0 c0Var : z0Var.getUpperBounds()) {
                if (!h.isDefaultBound(c0Var)) {
                    if (z3) {
                        sb.append(" : ");
                    } else {
                        sb.append(" & ");
                    }
                    m.checkNotNullExpressionValue(c0Var, "upperBound");
                    sb.append(renderType(c0Var));
                    z3 = false;
                }
            }
        }
        if (z2) {
            sb.append(d());
        }
    }

    public final void D(StringBuilder sb, List<? extends z0> list) {
        Iterator<? extends z0> it = list.iterator();
        while (it.hasNext()) {
            C(it.next(), sb, false);
            if (it.hasNext()) {
                sb.append(", ");
            }
        }
    }

    public final void E(List<? extends z0> list, StringBuilder sb, boolean z2) {
        if (!getWithoutTypeParameters() && (!list.isEmpty())) {
            sb.append(f());
            D(sb, list);
            sb.append(d());
            if (z2) {
                sb.append(" ");
            }
        }
    }

    public final void F(d1 d1Var, StringBuilder sb, boolean z2) {
        if (z2 || !(d1Var instanceof c1)) {
            sb.append(n(d1Var.isVar() ? "var" : "val"));
            sb.append(" ");
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:49:0x00e1, code lost:
        if ((getDebugMode() ? r9.declaresDefaultValue() : d0.e0.p.d.m0.k.x.a.declaresOrInheritsDefaultValue(r9)) != false) goto L51;
     */
    /* JADX WARN: Removed duplicated region for block: B:21:0x006a  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x007f  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x0085  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x0087  */
    /* JADX WARN: Removed duplicated region for block: B:29:0x008a  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x008c  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x00a1  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x00d2  */
    /* JADX WARN: Removed duplicated region for block: B:52:0x00e7  */
    /* JADX WARN: Removed duplicated region for block: B:54:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void G(d0.e0.p.d.m0.c.c1 r9, boolean r10, java.lang.StringBuilder r11, boolean r12) {
        /*
            Method dump skipped, instructions count: 252
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.j.d.G(d0.e0.p.d.m0.c.c1, boolean, java.lang.StringBuilder, boolean):void");
    }

    /* JADX WARN: Code restructure failed: missing block: B:9:0x0018, code lost:
        if (r8 == false) goto L11;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void H(java.util.Collection<? extends d0.e0.p.d.m0.c.c1> r7, boolean r8, java.lang.StringBuilder r9) {
        /*
            r6 = this;
            d0.e0.p.d.m0.j.n r0 = r6.getParameterNameRenderingPolicy()
            int r0 = r0.ordinal()
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L1c
            if (r0 == r1) goto L18
            r8 = 2
            if (r0 != r8) goto L12
            goto L1b
        L12:
            kotlin.NoWhenBranchMatchedException r7 = new kotlin.NoWhenBranchMatchedException
            r7.<init>()
            throw r7
        L18:
            if (r8 != 0) goto L1b
            goto L1c
        L1b:
            r1 = 0
        L1c:
            int r8 = r7.size()
            d0.e0.p.d.m0.j.c$l r0 = r6.getValueParametersHandler()
            r0.appendBeforeValueParameters(r8, r9)
            java.util.Iterator r7 = r7.iterator()
            r0 = 0
        L2c:
            boolean r3 = r7.hasNext()
            if (r3 == 0) goto L4d
            int r3 = r0 + 1
            java.lang.Object r4 = r7.next()
            d0.e0.p.d.m0.c.c1 r4 = (d0.e0.p.d.m0.c.c1) r4
            d0.e0.p.d.m0.j.c$l r5 = r6.getValueParametersHandler()
            r5.appendBeforeValueParameter(r4, r0, r8, r9)
            r6.G(r4, r1, r9, r2)
            d0.e0.p.d.m0.j.c$l r5 = r6.getValueParametersHandler()
            r5.appendAfterValueParameter(r4, r0, r8, r9)
            r0 = r3
            goto L2c
        L4d:
            d0.e0.p.d.m0.j.c$l r7 = r6.getValueParametersHandler()
            r7.appendAfterValueParameters(r8, r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.j.d.H(java.util.Collection, boolean, java.lang.StringBuilder):void");
    }

    public final boolean I(u uVar, StringBuilder sb) {
        if (!getModifiers().contains(g.VISIBILITY)) {
            return false;
        }
        if (getNormalizedVisibilities()) {
            uVar = uVar.normalize();
        }
        if (!getRenderDefaultVisibility() && m.areEqual(uVar, t.k)) {
            return false;
        }
        sb.append(n(uVar.getInternalDisplayName()));
        sb.append(" ");
        return true;
    }

    public final void J(List<? extends z0> list, StringBuilder sb) {
        if (!getWithoutTypeParameters()) {
            ArrayList arrayList = new ArrayList(0);
            for (z0 z0Var : list) {
                List<d0.e0.p.d.m0.n.c0> upperBounds = z0Var.getUpperBounds();
                m.checkNotNullExpressionValue(upperBounds, "typeParameter.upperBounds");
                for (d0.e0.p.d.m0.n.c0 c0Var : d0.t.u.drop(upperBounds, 1)) {
                    StringBuilder sb2 = new StringBuilder();
                    d0.e0.p.d.m0.g.e name = z0Var.getName();
                    m.checkNotNullExpressionValue(name, "typeParameter.name");
                    sb2.append(renderName(name, false));
                    sb2.append(" : ");
                    m.checkNotNullExpressionValue(c0Var, "it");
                    sb2.append(renderType(c0Var));
                    arrayList.add(sb2.toString());
                }
            }
            if (!arrayList.isEmpty()) {
                sb.append(" ");
                sb.append(n("where"));
                sb.append(" ");
                d0.t.u.joinTo$default(arrayList, sb, ", ", null, null, 0, null, null, 124, null);
            }
        }
    }

    public final String K(String str, String str2, String str3, String str4, String str5) {
        if (d0.g0.t.startsWith$default(str, str2, false, 2, null) && d0.g0.t.startsWith$default(str3, str4, false, 2, null)) {
            int length = str2.length();
            Objects.requireNonNull(str, "null cannot be cast to non-null type java.lang.String");
            String substring = str.substring(length);
            m.checkNotNullExpressionValue(substring, "(this as java.lang.String).substring(startIndex)");
            int length2 = str4.length();
            Objects.requireNonNull(str3, "null cannot be cast to non-null type java.lang.String");
            String substring2 = str3.substring(length2);
            m.checkNotNullExpressionValue(substring2, "(this as java.lang.String).substring(startIndex)");
            String stringPlus = m.stringPlus(str5, substring);
            if (m.areEqual(substring, substring2)) {
                return stringPlus;
            }
            if (b(substring, substring2)) {
                return m.stringPlus(stringPlus, "!");
            }
        }
        return null;
    }

    public final boolean L(d0.e0.p.d.m0.n.c0 c0Var) {
        boolean z2;
        if (!d0.e0.p.d.m0.b.g.isBuiltinFunctionalType(c0Var)) {
            return false;
        }
        List<w0> arguments = c0Var.getArguments();
        if (!(arguments instanceof Collection) || !arguments.isEmpty()) {
            for (w0 w0Var : arguments) {
                if (w0Var.isStarProjection()) {
                    z2 = false;
                    break;
                }
            }
        }
        z2 = true;
        return z2;
    }

    public final void a(StringBuilder sb, List<? extends w0> list) {
        d0.t.u.joinTo$default(list, sb, ", ", null, null, 0, null, new b(), 60, null);
    }

    public final boolean b(String str, String str2) {
        if (!m.areEqual(str, d0.g0.t.replace$default(str2, "?", "", false, 4, (Object) null)) && (!d0.g0.t.endsWith$default(str2, "?", false, 2, null) || !m.areEqual(m.stringPlus(str, "?"), str2))) {
            if (!m.areEqual('(' + str + ")?", str2)) {
                return false;
            }
        }
        return true;
    }

    public final String c(String str) {
        return getTextFormat().escape(str);
    }

    public final String d() {
        return getTextFormat().escape(">");
    }

    public final z e(y yVar) {
        f fVar = f.INTERFACE;
        if (yVar instanceof e) {
            return ((e) yVar).getKind() == fVar ? z.ABSTRACT : z.FINAL;
        }
        d0.e0.p.d.m0.c.m containingDeclaration = yVar.getContainingDeclaration();
        e eVar = containingDeclaration instanceof e ? (e) containingDeclaration : null;
        if (eVar != null && (yVar instanceof d0.e0.p.d.m0.c.b)) {
            d0.e0.p.d.m0.c.b bVar = (d0.e0.p.d.m0.c.b) yVar;
            Collection<? extends d0.e0.p.d.m0.c.b> overriddenDescriptors = bVar.getOverriddenDescriptors();
            m.checkNotNullExpressionValue(overriddenDescriptors, "this.overriddenDescriptors");
            if ((!overriddenDescriptors.isEmpty()) && eVar.getModality() != z.FINAL) {
                return z.OPEN;
            }
            if (eVar.getKind() != fVar || m.areEqual(bVar.getVisibility(), t.a)) {
                return z.FINAL;
            }
            z modality = bVar.getModality();
            z zVar = z.ABSTRACT;
            return modality == zVar ? zVar : z.OPEN;
        }
        return z.FINAL;
    }

    public final String f() {
        return getTextFormat().escape("<");
    }

    public final void g(StringBuilder sb, d0.e0.p.d.m0.n.a aVar) {
        p textFormat = getTextFormat();
        p pVar = p.HTML;
        if (textFormat == pVar) {
            sb.append("<font color=\"808080\"><i>");
        }
        sb.append(" /* = ");
        v(sb, aVar.getExpandedType());
        sb.append(" */");
        if (getTextFormat() == pVar) {
            sb.append("</i></font>");
        }
    }

    public boolean getActualPropertiesInPrimaryConstructor() {
        return this.d.getActualPropertiesInPrimaryConstructor();
    }

    public boolean getAlwaysRenderModifiers() {
        return this.d.getAlwaysRenderModifiers();
    }

    @Override // d0.e0.p.d.m0.j.h
    public d0.e0.p.d.m0.j.a getAnnotationArgumentsRenderingPolicy() {
        return this.d.getAnnotationArgumentsRenderingPolicy();
    }

    public Function1<d0.e0.p.d.m0.c.g1.c, Boolean> getAnnotationFilter() {
        return this.d.getAnnotationFilter();
    }

    public boolean getBoldOnlyForNamesInHtml() {
        return this.d.getBoldOnlyForNamesInHtml();
    }

    public boolean getClassWithPrimaryConstructor() {
        return this.d.getClassWithPrimaryConstructor();
    }

    public d0.e0.p.d.m0.j.b getClassifierNamePolicy() {
        return this.d.getClassifierNamePolicy();
    }

    @Override // d0.e0.p.d.m0.j.h
    public boolean getDebugMode() {
        return this.d.getDebugMode();
    }

    public Function1<c1, String> getDefaultParameterValueRenderer() {
        return this.d.getDefaultParameterValueRenderer();
    }

    public boolean getEachAnnotationOnNewLine() {
        return this.d.getEachAnnotationOnNewLine();
    }

    @Override // d0.e0.p.d.m0.j.h
    public boolean getEnhancedTypes() {
        return this.d.getEnhancedTypes();
    }

    public Set<d0.e0.p.d.m0.g.b> getExcludedAnnotationClasses() {
        return this.d.getExcludedAnnotationClasses();
    }

    @Override // d0.e0.p.d.m0.j.h
    public Set<d0.e0.p.d.m0.g.b> getExcludedTypeAnnotationClasses() {
        return this.d.getExcludedTypeAnnotationClasses();
    }

    public boolean getIncludeAdditionalModifiers() {
        return this.d.getIncludeAdditionalModifiers();
    }

    public boolean getIncludeAnnotationArguments() {
        return this.d.getIncludeAnnotationArguments();
    }

    public boolean getIncludeEmptyAnnotationArguments() {
        return this.d.getIncludeEmptyAnnotationArguments();
    }

    public boolean getIncludePropertyConstant() {
        return this.d.getIncludePropertyConstant();
    }

    public boolean getInformativeErrorType() {
        return this.d.getInformativeErrorType();
    }

    public Set<g> getModifiers() {
        return this.d.getModifiers();
    }

    public boolean getNormalizedVisibilities() {
        return this.d.getNormalizedVisibilities();
    }

    public final i getOptions() {
        return this.d;
    }

    public m getOverrideRenderingPolicy() {
        return this.d.getOverrideRenderingPolicy();
    }

    public n getParameterNameRenderingPolicy() {
        return this.d.getParameterNameRenderingPolicy();
    }

    public boolean getParameterNamesInFunctionalTypes() {
        return this.d.getParameterNamesInFunctionalTypes();
    }

    public boolean getPresentableUnresolvedTypes() {
        return this.d.getPresentableUnresolvedTypes();
    }

    public o getPropertyAccessorRenderingPolicy() {
        return this.d.getPropertyAccessorRenderingPolicy();
    }

    public boolean getReceiverAfterName() {
        return this.d.getReceiverAfterName();
    }

    public boolean getRenderCompanionObjectName() {
        return this.d.getRenderCompanionObjectName();
    }

    public boolean getRenderConstructorDelegation() {
        return this.d.getRenderConstructorDelegation();
    }

    public boolean getRenderConstructorKeyword() {
        return this.d.getRenderConstructorKeyword();
    }

    public boolean getRenderDefaultAnnotationArguments() {
        return this.d.getRenderDefaultAnnotationArguments();
    }

    public boolean getRenderDefaultModality() {
        return this.d.getRenderDefaultModality();
    }

    public boolean getRenderDefaultVisibility() {
        return this.d.getRenderDefaultVisibility();
    }

    public boolean getRenderPrimaryConstructorParametersAsProperties() {
        return this.d.getRenderPrimaryConstructorParametersAsProperties();
    }

    public boolean getRenderTypeExpansions() {
        return this.d.getRenderTypeExpansions();
    }

    public boolean getRenderUnabbreviatedType() {
        return this.d.getRenderUnabbreviatedType();
    }

    public boolean getSecondaryConstructorsAsPrimary() {
        return this.d.getSecondaryConstructorsAsPrimary();
    }

    public boolean getStartFromDeclarationKeyword() {
        return this.d.getStartFromDeclarationKeyword();
    }

    public boolean getStartFromName() {
        return this.d.getStartFromName();
    }

    public p getTextFormat() {
        return this.d.getTextFormat();
    }

    public Function1<d0.e0.p.d.m0.n.c0, d0.e0.p.d.m0.n.c0> getTypeNormalizer() {
        return this.d.getTypeNormalizer();
    }

    public boolean getUninferredTypeParameterAsName() {
        return this.d.getUninferredTypeParameterAsName();
    }

    public boolean getUnitReturnType() {
        return this.d.getUnitReturnType();
    }

    public c.l getValueParametersHandler() {
        return this.d.getValueParametersHandler();
    }

    public boolean getVerbose() {
        return this.d.getVerbose();
    }

    public boolean getWithDefinedIn() {
        return this.d.getWithDefinedIn();
    }

    public boolean getWithSourceFileForTopLevel() {
        return this.d.getWithSourceFileForTopLevel();
    }

    public boolean getWithoutReturnType() {
        return this.d.getWithoutReturnType();
    }

    public boolean getWithoutSuperTypes() {
        return this.d.getWithoutSuperTypes();
    }

    public boolean getWithoutTypeParameters() {
        return this.d.getWithoutTypeParameters();
    }

    public final void h(StringBuilder sb, d0.e0.p.d.m0.c.g1.a aVar, d0.e0.p.d.m0.c.g1.e eVar) {
        if (getModifiers().contains(g.ANNOTATIONS)) {
            Set<d0.e0.p.d.m0.g.b> excludedTypeAnnotationClasses = aVar instanceof d0.e0.p.d.m0.n.c0 ? getExcludedTypeAnnotationClasses() : getExcludedAnnotationClasses();
            Function1<d0.e0.p.d.m0.c.g1.c, Boolean> annotationFilter = getAnnotationFilter();
            for (d0.e0.p.d.m0.c.g1.c cVar : aVar.getAnnotations()) {
                if (!d0.t.u.contains(excludedTypeAnnotationClasses, cVar.getFqName()) && !m.areEqual(cVar.getFqName(), k.a.f3201y) && (annotationFilter == null || annotationFilter.invoke(cVar).booleanValue())) {
                    sb.append(renderAnnotation(cVar, eVar));
                    if (getEachAnnotationOnNewLine()) {
                        sb.append('\n');
                        m.checkNotNullExpressionValue(sb, "append('\\n')");
                    } else {
                        sb.append(" ");
                    }
                }
            }
        }
    }

    public final void j(i iVar, StringBuilder sb) {
        List<z0> declaredTypeParameters = iVar.getDeclaredTypeParameters();
        m.checkNotNullExpressionValue(declaredTypeParameters, "classifier.declaredTypeParameters");
        List<z0> parameters = iVar.getTypeConstructor().getParameters();
        m.checkNotNullExpressionValue(parameters, "classifier.typeConstructor.parameters");
        if (getVerbose() && iVar.isInner() && parameters.size() > declaredTypeParameters.size()) {
            sb.append(" /*captured type parameters: ");
            D(sb, parameters.subList(declaredTypeParameters.size(), parameters.size()));
            sb.append("*/");
        }
    }

    public final String k(d0.e0.p.d.m0.k.v.g<?> gVar) {
        if (gVar instanceof d0.e0.p.d.m0.k.v.b) {
            return d0.t.u.joinToString$default(((d0.e0.p.d.m0.k.v.b) gVar).getValue(), ", ", "{", "}", 0, null, new C0341d(), 24, null);
        }
        if (gVar instanceof d0.e0.p.d.m0.k.v.a) {
            return w.removePrefix(d0.e0.p.d.m0.j.c.renderAnnotation$default(this, ((d0.e0.p.d.m0.k.v.a) gVar).getValue(), null, 2, null), "@");
        }
        if (!(gVar instanceof r)) {
            return gVar.toString();
        }
        r.b value = ((r) gVar).getValue();
        if (value instanceof r.b.a) {
            return ((r.b.a) value).getType() + "::class";
        } else if (value instanceof r.b.C0345b) {
            r.b.C0345b bVar = (r.b.C0345b) value;
            String asString = bVar.getClassId().asSingleFqName().asString();
            m.checkNotNullExpressionValue(asString, "classValue.classId.asSingleFqName().asString()");
            for (int i = 0; i < bVar.getArrayDimensions(); i++) {
                asString = "kotlin.Array<" + asString + '>';
            }
            return m.stringPlus(asString, "::class");
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    public final void l(StringBuilder sb, d0.e0.p.d.m0.n.c0 c0Var) {
        h(sb, c0Var, null);
        if (d0.e0.p.d.m0.n.e0.isError(c0Var)) {
            if ((c0Var instanceof h1) && getPresentableUnresolvedTypes()) {
                sb.append(((h1) c0Var).getPresentableName());
            } else if (!(c0Var instanceof s) || getInformativeErrorType()) {
                sb.append(c0Var.getConstructor().toString());
            } else {
                sb.append(((s) c0Var).getPresentableName());
            }
            sb.append(renderTypeArguments(c0Var.getArguments()));
        } else {
            u0 constructor = c0Var.getConstructor();
            l0 buildPossiblyInnerType = a1.buildPossiblyInnerType(c0Var);
            if (buildPossiblyInnerType == null) {
                sb.append(renderTypeConstructor(constructor));
                sb.append(renderTypeArguments(c0Var.getArguments()));
            } else {
                y(sb, buildPossiblyInnerType);
            }
        }
        if (c0Var.isMarkedNullable()) {
            sb.append("?");
        }
        if (d0.e0.p.d.m0.n.m0.isDefinitelyNotNullType(c0Var)) {
            sb.append("!!");
        }
    }

    public final void m(d1 d1Var, StringBuilder sb) {
        d0.e0.p.d.m0.k.v.g<?> compileTimeInitializer;
        if (getIncludePropertyConstant() && (compileTimeInitializer = d1Var.getCompileTimeInitializer()) != null) {
            sb.append(" = ");
            sb.append(c(k(compileTimeInitializer)));
        }
    }

    public final String n(String str) {
        int ordinal = getTextFormat().ordinal();
        if (ordinal == 0) {
            return str;
        }
        if (ordinal == 1) {
            return getBoldOnlyForNamesInHtml() ? str : b.d.b.a.a.w("<b>", str, "</b>");
        }
        throw new NoWhenBranchMatchedException();
    }

    public final void o(d0.e0.p.d.m0.c.b bVar, StringBuilder sb) {
        if (getModifiers().contains(g.MEMBER_KIND) && getVerbose() && bVar.getKind() != b.a.DECLARATION) {
            sb.append("/*");
            String name = bVar.getKind().name();
            Objects.requireNonNull(name, "null cannot be cast to non-null type java.lang.String");
            String lowerCase = name.toLowerCase();
            m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase()");
            sb.append(lowerCase);
            sb.append("*/ ");
        }
    }

    public final void p(y yVar, StringBuilder sb) {
        s(sb, yVar.isExternal(), "external");
        boolean z2 = false;
        s(sb, getModifiers().contains(g.EXPECT) && yVar.isExpect(), "expect");
        if (getModifiers().contains(g.ACTUAL) && yVar.isActual()) {
            z2 = true;
        }
        s(sb, z2, "actual");
    }

    public final void q(z zVar, StringBuilder sb, z zVar2) {
        if (getRenderDefaultModality() || zVar != zVar2) {
            boolean contains = getModifiers().contains(g.MODALITY);
            String name = zVar.name();
            Objects.requireNonNull(name, "null cannot be cast to non-null type java.lang.String");
            String lowerCase = name.toLowerCase();
            m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase()");
            s(sb, contains, lowerCase);
        }
    }

    public final void r(d0.e0.p.d.m0.c.b bVar, StringBuilder sb) {
        if (d0.e0.p.d.m0.k.e.isTopLevelDeclaration(bVar) && bVar.getModality() == z.FINAL) {
            return;
        }
        if (getOverrideRenderingPolicy() != m.RENDER_OVERRIDE || bVar.getModality() != z.OPEN || !(!bVar.getOverriddenDescriptors().isEmpty())) {
            z modality = bVar.getModality();
            m.checkNotNullExpressionValue(modality, "callable.modality");
            q(modality, sb, e(bVar));
        }
    }

    @Override // d0.e0.p.d.m0.j.c
    public String render(d0.e0.p.d.m0.c.m mVar) {
        String name;
        m.checkNotNullParameter(mVar, "declarationDescriptor");
        StringBuilder sb = new StringBuilder();
        mVar.accept(new a(this), sb);
        if (getWithDefinedIn() && !(mVar instanceof e0) && !(mVar instanceof j0)) {
            if (mVar instanceof c0) {
                sb.append(" is a module");
            } else {
                d0.e0.p.d.m0.c.m containingDeclaration = mVar.getContainingDeclaration();
                if (containingDeclaration != null && !(containingDeclaration instanceof c0)) {
                    sb.append(" ");
                    sb.append(renderMessage("defined in"));
                    sb.append(" ");
                    d0.e0.p.d.m0.g.c fqName = d0.e0.p.d.m0.k.e.getFqName(containingDeclaration);
                    m.checkNotNullExpressionValue(fqName, "getFqName(containingDeclaration)");
                    sb.append(fqName.isRoot() ? "root package" : renderFqName(fqName));
                    if (getWithSourceFileForTopLevel() && (containingDeclaration instanceof e0) && (mVar instanceof p) && (name = ((v0.a) ((p) mVar).getSource().getContainingFile()).getName()) != null) {
                        sb.append(" ");
                        sb.append(renderMessage("in file"));
                        sb.append(" ");
                        sb.append(name);
                    }
                }
            }
        }
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }

    @Override // d0.e0.p.d.m0.j.c
    public String renderAnnotation(d0.e0.p.d.m0.c.g1.c cVar, d0.e0.p.d.m0.c.g1.e eVar) {
        d0.e0.p.d.m0.c.d unsubstitutedPrimaryConstructor;
        m.checkNotNullParameter(cVar, "annotation");
        StringBuilder sb = new StringBuilder();
        sb.append(MentionUtilsKt.MENTIONS_CHAR);
        if (eVar != null) {
            sb.append(m.stringPlus(eVar.getRenderName(), ":"));
        }
        d0.e0.p.d.m0.n.c0 type = cVar.getType();
        sb.append(renderType(type));
        if (getIncludeAnnotationArguments()) {
            Map<d0.e0.p.d.m0.g.e, d0.e0.p.d.m0.k.v.g<?>> allValueArguments = cVar.getAllValueArguments();
            List list = null;
            e annotationClass = getRenderDefaultAnnotationArguments() ? d0.e0.p.d.m0.k.x.a.getAnnotationClass(cVar) : null;
            List<c1> valueParameters = (annotationClass == null || (unsubstitutedPrimaryConstructor = annotationClass.getUnsubstitutedPrimaryConstructor()) == null) ? null : unsubstitutedPrimaryConstructor.getValueParameters();
            if (valueParameters != null) {
                ArrayList arrayList = new ArrayList();
                for (Object obj : valueParameters) {
                    if (((c1) obj).declaresDefaultValue()) {
                        arrayList.add(obj);
                    }
                }
                ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(arrayList, 10));
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    arrayList2.add(((c1) it.next()).getName());
                }
                list = arrayList2;
            }
            if (list == null) {
                list = n.emptyList();
            }
            ArrayList arrayList3 = new ArrayList();
            for (Object obj2 : list) {
                d0.e0.p.d.m0.g.e eVar2 = (d0.e0.p.d.m0.g.e) obj2;
                m.checkNotNullExpressionValue(eVar2, "it");
                if (!allValueArguments.containsKey(eVar2)) {
                    arrayList3.add(obj2);
                }
            }
            ArrayList arrayList4 = new ArrayList(d0.t.o.collectionSizeOrDefault(arrayList3, 10));
            Iterator it2 = arrayList3.iterator();
            while (it2.hasNext()) {
                arrayList4.add(m.stringPlus(((d0.e0.p.d.m0.g.e) it2.next()).asString(), " = ..."));
            }
            Set<Map.Entry<d0.e0.p.d.m0.g.e, d0.e0.p.d.m0.k.v.g<?>>> entrySet = allValueArguments.entrySet();
            ArrayList arrayList5 = new ArrayList(d0.t.o.collectionSizeOrDefault(entrySet, 10));
            Iterator<T> it3 = entrySet.iterator();
            while (it3.hasNext()) {
                Map.Entry entry = (Map.Entry) it3.next();
                d0.e0.p.d.m0.g.e eVar3 = (d0.e0.p.d.m0.g.e) entry.getKey();
                d0.e0.p.d.m0.k.v.g<?> gVar = (d0.e0.p.d.m0.k.v.g) entry.getValue();
                StringBuilder sb2 = new StringBuilder();
                sb2.append(eVar3.asString());
                sb2.append(" = ");
                sb2.append(!list.contains(eVar3) ? k(gVar) : "...");
                arrayList5.add(sb2.toString());
            }
            List sorted = d0.t.u.sorted(d0.t.u.plus((Collection) arrayList4, (Iterable) arrayList5));
            if (getIncludeEmptyAnnotationArguments() || (!sorted.isEmpty())) {
                d0.t.u.joinTo$default(sorted, sb, ", ", "(", ")", 0, null, null, 112, null);
            }
        }
        if (getVerbose() && (d0.e0.p.d.m0.n.e0.isError(type) || (type.getConstructor().getDeclarationDescriptor() instanceof d0.b))) {
            sb.append(" /* annotation class not found */");
        }
        String sb3 = sb.toString();
        m.checkNotNullExpressionValue(sb3, "StringBuilder().apply(builderAction).toString()");
        return sb3;
    }

    public String renderClassifierName(d0.e0.p.d.m0.c.h hVar) {
        m.checkNotNullParameter(hVar, "klass");
        if (d0.e0.p.d.m0.n.t.isError(hVar)) {
            return hVar.getTypeConstructor().toString();
        }
        return getClassifierNamePolicy().renderClassifier(hVar, this);
    }

    @Override // d0.e0.p.d.m0.j.c
    public String renderFlexibleType(String str, String str2, h hVar) {
        m.checkNotNullParameter(str, "lowerRendered");
        m.checkNotNullParameter(str2, "upperRendered");
        m.checkNotNullParameter(hVar, "builtIns");
        if (!b(str, str2)) {
            d0.e0.p.d.m0.j.b classifierNamePolicy = getClassifierNamePolicy();
            e collection = hVar.getCollection();
            m.checkNotNullExpressionValue(collection, "builtIns.collection");
            String substringBefore$default = w.substringBefore$default(classifierNamePolicy.renderClassifier(collection, this), "Collection", (String) null, 2, (Object) null);
            String stringPlus = m.stringPlus(substringBefore$default, "Mutable");
            String K = K(str, stringPlus, str2, substringBefore$default, substringBefore$default + "(Mutable)");
            if (K != null) {
                return K;
            }
            String K2 = K(str, m.stringPlus(substringBefore$default, "MutableMap.MutableEntry"), str2, m.stringPlus(substringBefore$default, "Map.Entry"), m.stringPlus(substringBefore$default, "(Mutable)Map.(Mutable)Entry"));
            if (K2 != null) {
                return K2;
            }
            d0.e0.p.d.m0.j.b classifierNamePolicy2 = getClassifierNamePolicy();
            e array = hVar.getArray();
            m.checkNotNullExpressionValue(array, "builtIns.array");
            String substringBefore$default2 = w.substringBefore$default(classifierNamePolicy2.renderClassifier(array, this), "Array", (String) null, 2, (Object) null);
            String K3 = K(str, m.stringPlus(substringBefore$default2, getTextFormat().escape("Array<")), str2, m.stringPlus(substringBefore$default2, getTextFormat().escape("Array<out ")), m.stringPlus(substringBefore$default2, getTextFormat().escape("Array<(out) ")));
            if (K3 != null) {
                return K3;
            }
            return '(' + str + ".." + str2 + ')';
        } else if (!d0.g0.t.startsWith$default(str2, "(", false, 2, null)) {
            return m.stringPlus(str, "!");
        } else {
            return '(' + str + ")!";
        }
    }

    @Override // d0.e0.p.d.m0.j.c
    public String renderFqName(d0.e0.p.d.m0.g.c cVar) {
        m.checkNotNullParameter(cVar, "fqName");
        List<d0.e0.p.d.m0.g.e> pathSegments = cVar.pathSegments();
        m.checkNotNullExpressionValue(pathSegments, "fqName.pathSegments()");
        return c(q.renderFqName(pathSegments));
    }

    public String renderMessage(String str) {
        m.checkNotNullParameter(str, "message");
        int ordinal = getTextFormat().ordinal();
        if (ordinal == 0) {
            return str;
        }
        if (ordinal == 1) {
            return b.d.b.a.a.w("<i>", str, "</i>");
        }
        throw new NoWhenBranchMatchedException();
    }

    @Override // d0.e0.p.d.m0.j.c
    public String renderName(d0.e0.p.d.m0.g.e eVar, boolean z2) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        String c2 = c(q.render(eVar));
        return (!getBoldOnlyForNamesInHtml() || getTextFormat() != p.HTML || !z2) ? c2 : b.d.b.a.a.w("<b>", c2, "</b>");
    }

    @Override // d0.e0.p.d.m0.j.c
    public String renderType(d0.e0.p.d.m0.n.c0 c0Var) {
        m.checkNotNullParameter(c0Var, "type");
        StringBuilder sb = new StringBuilder();
        u(sb, getTypeNormalizer().invoke(c0Var));
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }

    public String renderTypeArguments(List<? extends w0> list) {
        m.checkNotNullParameter(list, "typeArguments");
        if (list.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(f());
        a(sb, list);
        sb.append(d());
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }

    public String renderTypeConstructor(u0 u0Var) {
        m.checkNotNullParameter(u0Var, "typeConstructor");
        d0.e0.p.d.m0.c.h declarationDescriptor = u0Var.getDeclarationDescriptor();
        boolean z2 = true;
        if (!(declarationDescriptor instanceof z0 ? true : declarationDescriptor instanceof e)) {
            z2 = declarationDescriptor instanceof y0;
        }
        if (z2) {
            return renderClassifierName(declarationDescriptor);
        }
        if (declarationDescriptor == null) {
            return u0Var.toString();
        }
        throw new IllegalStateException(m.stringPlus("Unexpected classifier: ", declarationDescriptor.getClass()).toString());
    }

    @Override // d0.e0.p.d.m0.j.c
    public String renderTypeProjection(w0 w0Var) {
        m.checkNotNullParameter(w0Var, "typeProjection");
        StringBuilder sb = new StringBuilder();
        a(sb, d0.t.m.listOf(w0Var));
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }

    public final void s(StringBuilder sb, boolean z2, String str) {
        if (z2) {
            sb.append(n(str));
            sb.append(" ");
        }
    }

    @Override // d0.e0.p.d.m0.j.h
    public void setAnnotationArgumentsRenderingPolicy(d0.e0.p.d.m0.j.a aVar) {
        m.checkNotNullParameter(aVar, "<set-?>");
        this.d.setAnnotationArgumentsRenderingPolicy(aVar);
    }

    @Override // d0.e0.p.d.m0.j.h
    public void setClassifierNamePolicy(d0.e0.p.d.m0.j.b bVar) {
        m.checkNotNullParameter(bVar, "<set-?>");
        this.d.setClassifierNamePolicy(bVar);
    }

    @Override // d0.e0.p.d.m0.j.h
    public void setDebugMode(boolean z2) {
        this.d.setDebugMode(z2);
    }

    @Override // d0.e0.p.d.m0.j.h
    public void setExcludedTypeAnnotationClasses(Set<d0.e0.p.d.m0.g.b> set) {
        m.checkNotNullParameter(set, "<set-?>");
        this.d.setExcludedTypeAnnotationClasses(set);
    }

    @Override // d0.e0.p.d.m0.j.h
    public void setModifiers(Set<? extends g> set) {
        m.checkNotNullParameter(set, "<set-?>");
        this.d.setModifiers(set);
    }

    @Override // d0.e0.p.d.m0.j.h
    public void setParameterNameRenderingPolicy(n nVar) {
        m.checkNotNullParameter(nVar, "<set-?>");
        this.d.setParameterNameRenderingPolicy(nVar);
    }

    @Override // d0.e0.p.d.m0.j.h
    public void setReceiverAfterName(boolean z2) {
        this.d.setReceiverAfterName(z2);
    }

    @Override // d0.e0.p.d.m0.j.h
    public void setRenderCompanionObjectName(boolean z2) {
        this.d.setRenderCompanionObjectName(z2);
    }

    @Override // d0.e0.p.d.m0.j.h
    public void setStartFromName(boolean z2) {
        this.d.setStartFromName(z2);
    }

    @Override // d0.e0.p.d.m0.j.h
    public void setTextFormat(p pVar) {
        m.checkNotNullParameter(pVar, "<set-?>");
        this.d.setTextFormat(pVar);
    }

    @Override // d0.e0.p.d.m0.j.h
    public void setWithDefinedIn(boolean z2) {
        this.d.setWithDefinedIn(z2);
    }

    @Override // d0.e0.p.d.m0.j.h
    public void setWithoutSuperTypes(boolean z2) {
        this.d.setWithoutSuperTypes(z2);
    }

    @Override // d0.e0.p.d.m0.j.h
    public void setWithoutTypeParameters(boolean z2) {
        this.d.setWithoutTypeParameters(z2);
    }

    public final void t(d0.e0.p.d.m0.c.m mVar, StringBuilder sb, boolean z2) {
        d0.e0.p.d.m0.g.e name = mVar.getName();
        m.checkNotNullExpressionValue(name, "descriptor.name");
        sb.append(renderName(name, z2));
    }

    public final void u(StringBuilder sb, d0.e0.p.d.m0.n.c0 c0Var) {
        i1 unwrap = c0Var.unwrap();
        d0.e0.p.d.m0.n.a aVar = unwrap instanceof d0.e0.p.d.m0.n.a ? (d0.e0.p.d.m0.n.a) unwrap : null;
        if (aVar == null) {
            v(sb, c0Var);
        } else if (getRenderTypeExpansions()) {
            v(sb, aVar.getExpandedType());
        } else {
            v(sb, aVar.getAbbreviation());
            if (getRenderUnabbreviatedType()) {
                g(sb, aVar);
            }
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:71:0x0129  */
    /* JADX WARN: Removed duplicated region for block: B:74:0x0131  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void v(java.lang.StringBuilder r12, d0.e0.p.d.m0.n.c0 r13) {
        /*
            Method dump skipped, instructions count: 462
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.j.d.v(java.lang.StringBuilder, d0.e0.p.d.m0.n.c0):void");
    }

    public final void w(d0.e0.p.d.m0.c.b bVar, StringBuilder sb) {
        if (getModifiers().contains(g.OVERRIDE) && (!bVar.getOverriddenDescriptors().isEmpty()) && getOverrideRenderingPolicy() != m.RENDER_OPEN) {
            s(sb, true, "override");
            if (getVerbose()) {
                sb.append("/*");
                sb.append(bVar.getOverriddenDescriptors().size());
                sb.append("*/ ");
            }
        }
    }

    public final void x(d0.e0.p.d.m0.g.b bVar, String str, StringBuilder sb) {
        sb.append(n(str));
        d0.e0.p.d.m0.g.c unsafe = bVar.toUnsafe();
        m.checkNotNullExpressionValue(unsafe, "fqName.toUnsafe()");
        String renderFqName = renderFqName(unsafe);
        if (renderFqName.length() > 0) {
            sb.append(" ");
            sb.append(renderFqName);
        }
    }

    public final void y(StringBuilder sb, l0 l0Var) {
        StringBuilder sb2;
        l0 outerType = l0Var.getOuterType();
        if (outerType == null) {
            sb2 = null;
        } else {
            y(sb, outerType);
            sb.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
            d0.e0.p.d.m0.g.e name = l0Var.getClassifierDescriptor().getName();
            m.checkNotNullExpressionValue(name, "possiblyInnerType.classifierDescriptor.name");
            sb.append(renderName(name, false));
            sb2 = sb;
        }
        if (sb2 == null) {
            u0 typeConstructor = l0Var.getClassifierDescriptor().getTypeConstructor();
            m.checkNotNullExpressionValue(typeConstructor, "possiblyInnerType.classifierDescriptor.typeConstructor");
            sb.append(renderTypeConstructor(typeConstructor));
        }
        sb.append(renderTypeArguments(l0Var.getArguments()));
    }

    public final void z(d0.e0.p.d.m0.c.a aVar, StringBuilder sb) {
        q0 extensionReceiverParameter = aVar.getExtensionReceiverParameter();
        if (extensionReceiverParameter != null) {
            h(sb, extensionReceiverParameter, d0.e0.p.d.m0.c.g1.e.RECEIVER);
            d0.e0.p.d.m0.n.c0 type = extensionReceiverParameter.getType();
            m.checkNotNullExpressionValue(type, "receiver.type");
            String renderType = renderType(type);
            if (L(type) && !e1.isNullableType(type)) {
                renderType = '(' + renderType + ')';
            }
            sb.append(renderType);
            sb.append(".");
        }
    }
}
