package d0.e0.p.d.m0.j;

import d0.b0.a;
import d0.z.d.m;
import kotlin.reflect.KProperty;
/* compiled from: Delegates.kt */
/* loaded from: classes3.dex */
public final class j extends a<T> {
    public final /* synthetic */ Object a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ i f3421b;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public j(Object obj, Object obj2, i iVar) {
        super(obj2);
        this.a = obj;
        this.f3421b = iVar;
    }

    @Override // d0.b0.a
    public boolean beforeChange(KProperty<?> kProperty, T t, T t2) {
        m.checkNotNullParameter(kProperty, "property");
        if (!this.f3421b.isLocked()) {
            return true;
        }
        throw new IllegalStateException("Cannot modify readonly DescriptorRendererOptions");
    }
}
