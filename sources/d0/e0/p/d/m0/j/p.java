package d0.e0.p.d.m0.j;

import d0.g0.t;
import d0.z.d.m;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: DescriptorRenderer.kt */
/* loaded from: classes3.dex */
public enum p {
    PLAIN { // from class: d0.e0.p.d.m0.j.p.b
        @Override // d0.e0.p.d.m0.j.p
        public String escape(String str) {
            m.checkNotNullParameter(str, "string");
            return str;
        }
    },
    HTML { // from class: d0.e0.p.d.m0.j.p.a
        @Override // d0.e0.p.d.m0.j.p
        public String escape(String str) {
            m.checkNotNullParameter(str, "string");
            return t.replace$default(t.replace$default(str, "<", "&lt;", false, 4, (Object) null), ">", "&gt;", false, 4, (Object) null);
        }
    };

    p(DefaultConstructorMarker defaultConstructorMarker) {
    }

    public abstract String escape(String str);
}
