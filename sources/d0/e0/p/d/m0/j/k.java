package d0.e0.p.d.m0.j;

import d0.e0.p.d.m0.g.b;
import d0.t.n0;
import java.util.Set;
/* compiled from: DescriptorRenderer.kt */
/* loaded from: classes3.dex */
public final class k {
    public static final k a = new k();

    /* renamed from: b  reason: collision with root package name */
    public static final Set<b> f3422b = n0.setOf((Object[]) new b[]{new b("kotlin.internal.NoInfer"), new b("kotlin.internal.Exact")});

    public final Set<b> getInternalAnnotationsForResolve() {
        return f3422b;
    }
}
