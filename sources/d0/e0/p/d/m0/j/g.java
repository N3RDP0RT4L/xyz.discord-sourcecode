package d0.e0.p.d.m0.j;

import d0.t.k;
import d0.t.u;
import java.util.ArrayList;
import java.util.Set;
/* compiled from: DescriptorRenderer.kt */
/* loaded from: classes3.dex */
public enum g {
    VISIBILITY(true),
    MODALITY(true),
    OVERRIDE(true),
    ANNOTATIONS(false),
    INNER(true),
    MEMBER_KIND(true),
    DATA(true),
    INLINE(true),
    EXPECT(true),
    ACTUAL(true),
    CONST(true),
    LATEINIT(true),
    FUN(true),
    VALUE(true);
    
    public static final Set<g> j;
    public static final Set<g> k;
    private final boolean includeByDefault;

    static {
        new Object(null) { // from class: d0.e0.p.d.m0.j.g.a
        };
        g[] values = values();
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < 14; i++) {
            g gVar = values[i];
            if (gVar.getIncludeByDefault()) {
                arrayList.add(gVar);
            }
        }
        j = u.toSet(arrayList);
        k = k.toSet(values());
    }

    g(boolean z2) {
        this.includeByDefault = z2;
    }

    public final boolean getIncludeByDefault() {
        return this.includeByDefault;
    }
}
