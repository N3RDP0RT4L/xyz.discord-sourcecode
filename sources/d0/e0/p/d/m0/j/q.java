package d0.e0.p.d.m0.j;

import d0.e0.p.d.m0.g.c;
import d0.e0.p.d.m0.g.e;
import d0.z.d.m;
import java.util.List;
/* compiled from: RenderingUtils.kt */
/* loaded from: classes3.dex */
public final class q {
    /* JADX WARN: Code restructure failed: missing block: B:19:0x003f, code lost:
        if (r0 != false) goto L20;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final java.lang.String render(d0.e0.p.d.m0.g.e r7) {
        /*
            java.lang.String r0 = "<this>"
            d0.z.d.m.checkNotNullParameter(r7, r0)
            boolean r0 = r7.isSpecial()
            java.lang.String r1 = "asString()"
            r2 = 0
            if (r0 == 0) goto Lf
            goto L42
        Lf:
            java.lang.String r0 = r7.asString()
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            java.util.Set<java.lang.String> r3 = d0.e0.p.d.m0.j.l.a
            boolean r3 = r3.contains(r0)
            r4 = 1
            if (r3 != 0) goto L41
            r3 = 0
        L20:
            int r5 = r0.length()
            if (r3 >= r5) goto L3e
            char r5 = r0.charAt(r3)
            boolean r6 = java.lang.Character.isLetterOrDigit(r5)
            if (r6 != 0) goto L36
            r6 = 95
            if (r5 == r6) goto L36
            r5 = 1
            goto L37
        L36:
            r5 = 0
        L37:
            if (r5 == 0) goto L3b
            r0 = 1
            goto L3f
        L3b:
            int r3 = r3 + 1
            goto L20
        L3e:
            r0 = 0
        L3f:
            if (r0 == 0) goto L42
        L41:
            r2 = 1
        L42:
            if (r2 == 0) goto L67
            r0 = 96
            java.lang.String r7 = r7.asString()
            d0.z.d.m.checkNotNullExpressionValue(r7, r1)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r0 = java.lang.String.valueOf(r0)
            r1.append(r0)
            r1.append(r7)
            java.lang.String r7 = r1.toString()
            java.lang.String r0 = "`"
            java.lang.String r7 = d0.z.d.m.stringPlus(r7, r0)
            goto L6e
        L67:
            java.lang.String r7 = r7.asString()
            d0.z.d.m.checkNotNullExpressionValue(r7, r1)
        L6e:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.j.q.render(d0.e0.p.d.m0.g.e):java.lang.String");
    }

    public static final String renderFqName(List<e> list) {
        m.checkNotNullParameter(list, "pathSegments");
        StringBuilder sb = new StringBuilder();
        for (e eVar : list) {
            if (sb.length() > 0) {
                sb.append(".");
            }
            sb.append(render(eVar));
        }
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }

    public static final String render(c cVar) {
        m.checkNotNullParameter(cVar, "<this>");
        List<e> pathSegments = cVar.pathSegments();
        m.checkNotNullExpressionValue(pathSegments, "pathSegments()");
        return renderFqName(pathSegments);
    }
}
