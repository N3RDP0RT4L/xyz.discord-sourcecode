package d0.e0.p.d.m0.j;

import andhook.lib.xposed.ClassUtils;
import d0.e0.p.d.m0.c.e0;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.g.e;
import d0.t.s;
import d0.z.d.m;
import java.util.ArrayList;
/* compiled from: ClassifierNamePolicy.kt */
/* loaded from: classes3.dex */
public interface b {

    /* compiled from: ClassifierNamePolicy.kt */
    /* loaded from: classes3.dex */
    public static final class a implements b {
        public static final a a = new a();

        @Override // d0.e0.p.d.m0.j.b
        public String renderClassifier(h hVar, d0.e0.p.d.m0.j.c cVar) {
            m.checkNotNullParameter(hVar, "classifier");
            m.checkNotNullParameter(cVar, "renderer");
            if (hVar instanceof z0) {
                e name = ((z0) hVar).getName();
                m.checkNotNullExpressionValue(name, "classifier.name");
                return cVar.renderName(name, false);
            }
            d0.e0.p.d.m0.g.c fqName = d0.e0.p.d.m0.k.e.getFqName(hVar);
            m.checkNotNullExpressionValue(fqName, "getFqName(classifier)");
            return cVar.renderFqName(fqName);
        }
    }

    /* compiled from: ClassifierNamePolicy.kt */
    /* renamed from: d0.e0.p.d.m0.j.b$b  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0339b implements b {
        public static final C0339b a = new C0339b();

        @Override // d0.e0.p.d.m0.j.b
        public String renderClassifier(h hVar, d0.e0.p.d.m0.j.c cVar) {
            boolean z2;
            m.checkNotNullParameter(hVar, "classifier");
            m.checkNotNullParameter(cVar, "renderer");
            if (hVar instanceof z0) {
                e name = ((z0) hVar).getName();
                m.checkNotNullExpressionValue(name, "classifier.name");
                return cVar.renderName(name, false);
            }
            ArrayList arrayList = new ArrayList();
            h hVar2 = hVar;
            do {
                arrayList.add(hVar2.getName());
                d0.e0.p.d.m0.c.m containingDeclaration = hVar2.getContainingDeclaration();
                z2 = containingDeclaration instanceof d0.e0.p.d.m0.c.e;
                hVar2 = containingDeclaration;
            } while (z2);
            return q.renderFqName(s.asReversedMutable(arrayList));
        }
    }

    /* compiled from: ClassifierNamePolicy.kt */
    /* loaded from: classes3.dex */
    public static final class c implements b {
        public static final c a = new c();

        public final String a(h hVar) {
            String str;
            e name = hVar.getName();
            m.checkNotNullExpressionValue(name, "descriptor.name");
            String render = q.render(name);
            if (hVar instanceof z0) {
                return render;
            }
            d0.e0.p.d.m0.c.m containingDeclaration = hVar.getContainingDeclaration();
            m.checkNotNullExpressionValue(containingDeclaration, "descriptor.containingDeclaration");
            if (containingDeclaration instanceof d0.e0.p.d.m0.c.e) {
                str = a((h) containingDeclaration);
            } else if (containingDeclaration instanceof e0) {
                d0.e0.p.d.m0.g.c unsafe = ((e0) containingDeclaration).getFqName().toUnsafe();
                m.checkNotNullExpressionValue(unsafe, "descriptor.fqName.toUnsafe()");
                str = q.render(unsafe);
            } else {
                str = null;
            }
            if (str == null || m.areEqual(str, "")) {
                return render;
            }
            return ((Object) str) + ClassUtils.PACKAGE_SEPARATOR_CHAR + render;
        }

        @Override // d0.e0.p.d.m0.j.b
        public String renderClassifier(h hVar, d0.e0.p.d.m0.j.c cVar) {
            m.checkNotNullParameter(hVar, "classifier");
            m.checkNotNullParameter(cVar, "renderer");
            return a(hVar);
        }
    }

    String renderClassifier(h hVar, d0.e0.p.d.m0.j.c cVar);
}
