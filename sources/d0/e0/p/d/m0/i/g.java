package d0.e0.p.d.m0.i;

import d0.e0.p.d.m0.i.a;
import d0.e0.p.d.m0.i.f;
import d0.e0.p.d.m0.i.h;
import d0.e0.p.d.m0.i.n;
import d0.e0.p.d.m0.i.w;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.reflect.jvm.internal.impl.protobuf.CodedOutputStream;
/* compiled from: GeneratedMessageLite.java */
/* loaded from: classes3.dex */
public abstract class g extends d0.e0.p.d.m0.i.a implements Serializable {

    /* compiled from: GeneratedMessageLite.java */
    /* loaded from: classes3.dex */
    public static abstract class b<MessageType extends g, BuilderType extends b> extends a.AbstractC0335a<BuilderType> {
        public d0.e0.p.d.m0.i.c j = d0.e0.p.d.m0.i.c.j;

        public final d0.e0.p.d.m0.i.c getUnknownFields() {
            return this.j;
        }

        public abstract BuilderType mergeFrom(MessageType messagetype);

        public final BuilderType setUnknownFields(d0.e0.p.d.m0.i.c cVar) {
            this.j = cVar;
            return this;
        }

        public BuilderType clone() {
            throw new UnsupportedOperationException("This is supposed to be overridden by subclasses.");
        }
    }

    /* compiled from: GeneratedMessageLite.java */
    /* loaded from: classes3.dex */
    public static abstract class c<MessageType extends d<MessageType>, BuilderType extends c<MessageType, BuilderType>> extends b<MessageType, BuilderType> implements o {
        public d0.e0.p.d.m0.i.f<e> k = d0.e0.p.d.m0.i.f.emptySet();
        public boolean l;

        public final void b(MessageType messagetype) {
            if (!this.l) {
                this.k = this.k.clone();
                this.l = true;
            }
            this.k.mergeFrom(((d) messagetype).extensions);
        }
    }

    /* compiled from: GeneratedMessageLite.java */
    /* loaded from: classes3.dex */
    public static final class e implements f.a<e> {
        public final h.b<?> j;
        public final int k;
        public final w.b l;
        public final boolean m;
        public final boolean n;

        public e(h.b<?> bVar, int i, w.b bVar2, boolean z2, boolean z3) {
            this.j = bVar;
            this.k = i;
            this.l = bVar2;
            this.m = z2;
            this.n = z3;
        }

        public h.b<?> getEnumType() {
            return this.j;
        }

        @Override // d0.e0.p.d.m0.i.f.a
        public w.c getLiteJavaType() {
            return this.l.getJavaType();
        }

        @Override // d0.e0.p.d.m0.i.f.a
        public w.b getLiteType() {
            return this.l;
        }

        @Override // d0.e0.p.d.m0.i.f.a
        public int getNumber() {
            return this.k;
        }

        @Override // d0.e0.p.d.m0.i.f.a
        public n.a internalMergeFrom(n.a aVar, n nVar) {
            return ((b) aVar).mergeFrom((g) nVar);
        }

        @Override // d0.e0.p.d.m0.i.f.a
        public boolean isPacked() {
            return this.n;
        }

        @Override // d0.e0.p.d.m0.i.f.a
        public boolean isRepeated() {
            return this.m;
        }

        public int compareTo(e eVar) {
            return this.k - eVar.k;
        }
    }

    /* compiled from: GeneratedMessageLite.java */
    /* loaded from: classes3.dex */
    public static class f<ContainingType extends n, Type> {
        public final ContainingType a;

        /* renamed from: b  reason: collision with root package name */
        public final Type f3403b;
        public final n c;
        public final e d;
        public final Method e;

        public f(ContainingType containingtype, Type type, n nVar, e eVar, Class cls) {
            if (containingtype == null) {
                throw new IllegalArgumentException("Null containingTypeDefaultInstance");
            } else if (eVar.getLiteType() == w.b.t && nVar == null) {
                throw new IllegalArgumentException("Null messageDefaultInstance");
            } else {
                this.a = containingtype;
                this.f3403b = type;
                this.c = nVar;
                this.d = eVar;
                if (h.a.class.isAssignableFrom(cls)) {
                    try {
                        this.e = cls.getMethod("valueOf", Integer.TYPE);
                    } catch (NoSuchMethodException e) {
                        String name = cls.getName();
                        StringBuilder sb = new StringBuilder(name.length() + 45 + 7);
                        b.d.b.a.a.q0(sb, "Generated message class \"", name, "\" missing method \"", "valueOf");
                        sb.append("\".");
                        throw new RuntimeException(sb.toString(), e);
                    }
                } else {
                    this.e = null;
                }
            }
        }

        public Object a(Object obj) {
            if (this.d.getLiteJavaType() != w.c.ENUM) {
                return obj;
            }
            try {
                return this.e.invoke(null, (Integer) obj);
            } catch (IllegalAccessException e) {
                throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e);
            } catch (InvocationTargetException e2) {
                Throwable cause = e2.getCause();
                if (cause instanceof RuntimeException) {
                    throw ((RuntimeException) cause);
                } else if (cause instanceof Error) {
                    throw ((Error) cause);
                } else {
                    throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
                }
            }
        }

        public Object b(Object obj) {
            return this.d.getLiteJavaType() == w.c.ENUM ? Integer.valueOf(((h.a) obj).getNumber()) : obj;
        }

        public ContainingType getContainingTypeDefaultInstance() {
            return this.a;
        }

        public n getMessageDefaultInstance() {
            return this.c;
        }

        public int getNumber() {
            return this.d.getNumber();
        }
    }

    public g() {
    }

    public static <ContainingType extends n, Type> f<ContainingType, Type> newRepeatedGeneratedExtension(ContainingType containingtype, n nVar, h.b<?> bVar, int i, w.b bVar2, boolean z2, Class cls) {
        return new f<>(containingtype, Collections.emptyList(), nVar, new e(bVar, i, bVar2, true, z2), cls);
    }

    public static <ContainingType extends n, Type> f<ContainingType, Type> newSingularGeneratedExtension(ContainingType containingtype, Type type, n nVar, h.b<?> bVar, int i, w.b bVar2, Class cls) {
        return new f<>(containingtype, type, nVar, new e(bVar, i, bVar2, false, false), cls);
    }

    /* compiled from: GeneratedMessageLite.java */
    /* loaded from: classes3.dex */
    public static abstract class d<MessageType extends d<MessageType>> extends g implements o {
        private final d0.e0.p.d.m0.i.f<e> extensions;

        /* compiled from: GeneratedMessageLite.java */
        /* loaded from: classes3.dex */
        public class a {
            public final Iterator<Map.Entry<e, Object>> a;

            /* renamed from: b  reason: collision with root package name */
            public Map.Entry<e, Object> f3402b;
            public final boolean c;

            public a(boolean z2, a aVar) {
                Iterator<Map.Entry<e, Object>> it = d.this.extensions.iterator();
                this.a = it;
                if (it.hasNext()) {
                    this.f3402b = it.next();
                }
                this.c = z2;
            }

            public void writeUntil(int i, CodedOutputStream codedOutputStream) throws IOException {
                while (true) {
                    Map.Entry<e, Object> entry = this.f3402b;
                    if (entry != null && entry.getKey().getNumber() < i) {
                        e key = this.f3402b.getKey();
                        if (!this.c || key.getLiteJavaType() != w.c.MESSAGE || key.isRepeated()) {
                            d0.e0.p.d.m0.i.f.writeField(key, this.f3402b.getValue(), codedOutputStream);
                        } else {
                            codedOutputStream.writeMessageSetExtension(key.getNumber(), (n) this.f3402b.getValue());
                        }
                        if (this.a.hasNext()) {
                            this.f3402b = this.a.next();
                        } else {
                            this.f3402b = null;
                        }
                    } else {
                        return;
                    }
                }
            }
        }

        public d() {
            this.extensions = d0.e0.p.d.m0.i.f.newFieldSet();
        }

        public boolean b() {
            return this.extensions.isInitialized();
        }

        public int c() {
            return this.extensions.getSerializedSize();
        }

        public void d() {
            this.extensions.makeImmutable();
        }

        public d<MessageType>.a e() {
            return new a(false, null);
        }

        /* JADX WARN: Removed duplicated region for block: B:17:0x0044  */
        /* JADX WARN: Removed duplicated region for block: B:18:0x004a  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public boolean f(d0.e0.p.d.m0.i.d r8, kotlin.reflect.jvm.internal.impl.protobuf.CodedOutputStream r9, d0.e0.p.d.m0.i.e r10, int r11) throws java.io.IOException {
            /*
                Method dump skipped, instructions count: 294
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.i.g.d.f(d0.e0.p.d.m0.i.d, kotlin.reflect.jvm.internal.impl.protobuf.CodedOutputStream, d0.e0.p.d.m0.i.e, int):boolean");
        }

        public final void g(f<MessageType, ?> fVar) {
            if (fVar.getContainingTypeDefaultInstance() != getDefaultInstanceForType()) {
                throw new IllegalArgumentException("This extension is for a different message type.  Please make sure that you are not suppressing any generics type warnings.");
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r1v5, types: [Type, java.util.ArrayList] */
        public final <Type> Type getExtension(f<MessageType, Type> fVar) {
            g(fVar);
            Type type = (Type) this.extensions.getField(fVar.d);
            if (type == null) {
                return fVar.f3403b;
            }
            if (!fVar.d.isRepeated()) {
                return (Type) fVar.a(type);
            }
            if (fVar.d.getLiteJavaType() != w.c.ENUM) {
                return type;
            }
            ?? r1 = (Type) new ArrayList();
            for (Object obj : (List) type) {
                r1.add(fVar.a(obj));
            }
            return r1;
        }

        public final <Type> int getExtensionCount(f<MessageType, List<Type>> fVar) {
            g(fVar);
            return this.extensions.getRepeatedFieldCount(fVar.d);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public final <Type> boolean hasExtension(f<MessageType, Type> fVar) {
            g(fVar);
            return this.extensions.hasField(fVar.d);
        }

        public d(c<MessageType, ?> cVar) {
            cVar.k.makeImmutable();
            cVar.l = false;
            this.extensions = cVar.k;
        }

        public final <Type> Type getExtension(f<MessageType, List<Type>> fVar, int i) {
            g(fVar);
            return (Type) fVar.a(this.extensions.getRepeatedField(fVar.d, i));
        }
    }

    public g(b bVar) {
    }
}
