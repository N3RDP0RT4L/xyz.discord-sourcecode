package d0.e0.p.d.m0.i;

import java.io.IOException;
/* compiled from: LazyFieldLite.java */
/* loaded from: classes3.dex */
public class j {
    public volatile boolean a;

    /* renamed from: b  reason: collision with root package name */
    public volatile n f3404b;

    public int getSerializedSize() {
        if (this.a) {
            return this.f3404b.getSerializedSize();
        }
        throw null;
    }

    public n getValue(n nVar) {
        if (this.f3404b == null) {
            synchronized (this) {
                if (this.f3404b == null) {
                    try {
                        this.f3404b = nVar;
                    } catch (IOException unused) {
                    }
                }
            }
        }
        return this.f3404b;
    }

    public n setValue(n nVar) {
        n nVar2 = this.f3404b;
        this.f3404b = nVar;
        this.a = true;
        return nVar2;
    }
}
