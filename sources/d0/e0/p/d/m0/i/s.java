package d0.e0.p.d.m0.i;

import d0.e0.p.d.m0.i.f;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
/* compiled from: SmallSortedMap.java */
/* loaded from: classes3.dex */
public final class s extends t<FieldDescriptorType, Object> {
    public s(int i) {
        super(i, null);
    }

    @Override // d0.e0.p.d.m0.i.t
    public void makeImmutable() {
        if (!isImmutable()) {
            for (int i = 0; i < getNumArrayEntries(); i++) {
                Map.Entry<FieldDescriptorType, Object> arrayEntryAt = getArrayEntryAt(i);
                if (((f.a) arrayEntryAt.getKey()).isRepeated()) {
                    arrayEntryAt.setValue(Collections.unmodifiableList((List) arrayEntryAt.getValue()));
                }
            }
            Iterator it = getOverflowEntries().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                if (((f.a) entry.getKey()).isRepeated()) {
                    entry.setValue(Collections.unmodifiableList((List) entry.getValue()));
                }
            }
        }
        super.makeImmutable();
    }

    @Override // java.util.AbstractMap, java.util.Map
    public /* bridge */ /* synthetic */ Object put(Object obj, Object obj2) {
        return super.put((s) ((f.a) obj), (f.a) obj2);
    }
}
