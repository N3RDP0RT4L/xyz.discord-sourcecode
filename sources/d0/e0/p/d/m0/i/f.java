package d0.e0.p.d.m0.i;

import d0.e0.p.d.m0.i.f.a;
import d0.e0.p.d.m0.i.h;
import d0.e0.p.d.m0.i.i;
import d0.e0.p.d.m0.i.n;
import d0.e0.p.d.m0.i.w;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.reflect.jvm.internal.impl.protobuf.CodedOutputStream;
/* compiled from: FieldSet.java */
/* loaded from: classes3.dex */
public final class f<FieldDescriptorType extends a<FieldDescriptorType>> {
    public static final f a = new f(true);
    public boolean c;
    public boolean d = false;

    /* renamed from: b  reason: collision with root package name */
    public final t<FieldDescriptorType, Object> f3401b = new s(16);

    /* compiled from: FieldSet.java */
    /* loaded from: classes3.dex */
    public interface a<T extends a<T>> extends Comparable<T> {
        w.c getLiteJavaType();

        w.b getLiteType();

        int getNumber();

        n.a internalMergeFrom(n.a aVar, n nVar);

        boolean isPacked();

        boolean isRepeated();
    }

    public f() {
        int i = t.j;
    }

    public static int b(w.b bVar, Object obj) {
        switch (bVar.ordinal()) {
            case 0:
                return CodedOutputStream.computeDoubleSizeNoTag(((Double) obj).doubleValue());
            case 1:
                return CodedOutputStream.computeFloatSizeNoTag(((Float) obj).floatValue());
            case 2:
                return CodedOutputStream.computeInt64SizeNoTag(((Long) obj).longValue());
            case 3:
                return CodedOutputStream.computeUInt64SizeNoTag(((Long) obj).longValue());
            case 4:
                return CodedOutputStream.computeInt32SizeNoTag(((Integer) obj).intValue());
            case 5:
                return CodedOutputStream.computeFixed64SizeNoTag(((Long) obj).longValue());
            case 6:
                return CodedOutputStream.computeFixed32SizeNoTag(((Integer) obj).intValue());
            case 7:
                return CodedOutputStream.computeBoolSizeNoTag(((Boolean) obj).booleanValue());
            case 8:
                return CodedOutputStream.computeStringSizeNoTag((String) obj);
            case 9:
                return CodedOutputStream.computeGroupSizeNoTag((n) obj);
            case 10:
                if (obj instanceof i) {
                    return CodedOutputStream.computeLazyFieldSizeNoTag((i) obj);
                }
                return CodedOutputStream.computeMessageSizeNoTag((n) obj);
            case 11:
                if (obj instanceof c) {
                    return CodedOutputStream.computeBytesSizeNoTag((c) obj);
                }
                return CodedOutputStream.computeByteArraySizeNoTag((byte[]) obj);
            case 12:
                return CodedOutputStream.computeUInt32SizeNoTag(((Integer) obj).intValue());
            case 13:
                if (obj instanceof h.a) {
                    return CodedOutputStream.computeEnumSizeNoTag(((h.a) obj).getNumber());
                }
                return CodedOutputStream.computeEnumSizeNoTag(((Integer) obj).intValue());
            case 14:
                return CodedOutputStream.computeSFixed32SizeNoTag(((Integer) obj).intValue());
            case 15:
                return CodedOutputStream.computeSFixed64SizeNoTag(((Long) obj).longValue());
            case 16:
                return CodedOutputStream.computeSInt32SizeNoTag(((Integer) obj).intValue());
            case 17:
                return CodedOutputStream.computeSInt64SizeNoTag(((Long) obj).longValue());
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
    }

    public static int c(w.b bVar, boolean z2) {
        if (z2) {
            return 2;
        }
        return bVar.getWireType();
    }

    public static int computeFieldSize(a<?> aVar, Object obj) {
        int computeTagSize;
        int b2;
        w.b liteType = aVar.getLiteType();
        int number = aVar.getNumber();
        if (aVar.isRepeated()) {
            int i = 0;
            if (aVar.isPacked()) {
                for (Object obj2 : (List) obj) {
                    i += b(liteType, obj2);
                }
                computeTagSize = CodedOutputStream.computeTagSize(number) + i;
                b2 = CodedOutputStream.computeRawVarint32Size(i);
            } else {
                for (Object obj3 : (List) obj) {
                    int computeTagSize2 = CodedOutputStream.computeTagSize(number);
                    if (liteType == w.b.f3406s) {
                        computeTagSize2 *= 2;
                    }
                    i += b(liteType, obj3) + computeTagSize2;
                }
                return i;
            }
        } else {
            computeTagSize = CodedOutputStream.computeTagSize(number);
            if (liteType == w.b.f3406s) {
                computeTagSize *= 2;
            }
            b2 = b(liteType, obj);
        }
        return b2 + computeTagSize;
    }

    public static <T extends a<T>> f<T> emptySet() {
        return a;
    }

    /* JADX WARN: Code restructure failed: missing block: B:11:0x0020, code lost:
        if ((r3 instanceof d0.e0.p.d.m0.i.h.a) == false) goto L16;
     */
    /* JADX WARN: Code restructure failed: missing block: B:15:0x0029, code lost:
        if ((r3 instanceof byte[]) == false) goto L16;
     */
    /* JADX WARN: Code restructure failed: missing block: B:16:0x002c, code lost:
        r0 = false;
     */
    /* JADX WARN: Code restructure failed: missing block: B:7:0x0017, code lost:
        if ((r3 instanceof d0.e0.p.d.m0.i.i) == false) goto L16;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static void f(d0.e0.p.d.m0.i.w.b r2, java.lang.Object r3) {
        /*
            java.util.Objects.requireNonNull(r3)
            d0.e0.p.d.m0.i.w$c r2 = r2.getJavaType()
            int r2 = r2.ordinal()
            r0 = 1
            r1 = 0
            switch(r2) {
                case 0: goto L3e;
                case 1: goto L3b;
                case 2: goto L38;
                case 3: goto L35;
                case 4: goto L32;
                case 5: goto L2f;
                case 6: goto L23;
                case 7: goto L1a;
                case 8: goto L11;
                default: goto L10;
            }
        L10:
            goto L40
        L11:
            boolean r2 = r3 instanceof d0.e0.p.d.m0.i.n
            if (r2 != 0) goto L2d
            boolean r2 = r3 instanceof d0.e0.p.d.m0.i.i
            if (r2 == 0) goto L2c
            goto L2d
        L1a:
            boolean r2 = r3 instanceof java.lang.Integer
            if (r2 != 0) goto L2d
            boolean r2 = r3 instanceof d0.e0.p.d.m0.i.h.a
            if (r2 == 0) goto L2c
            goto L2d
        L23:
            boolean r2 = r3 instanceof d0.e0.p.d.m0.i.c
            if (r2 != 0) goto L2d
            boolean r2 = r3 instanceof byte[]
            if (r2 == 0) goto L2c
            goto L2d
        L2c:
            r0 = 0
        L2d:
            r1 = r0
            goto L40
        L2f:
            boolean r1 = r3 instanceof java.lang.String
            goto L40
        L32:
            boolean r1 = r3 instanceof java.lang.Boolean
            goto L40
        L35:
            boolean r1 = r3 instanceof java.lang.Double
            goto L40
        L38:
            boolean r1 = r3 instanceof java.lang.Float
            goto L40
        L3b:
            boolean r1 = r3 instanceof java.lang.Long
            goto L40
        L3e:
            boolean r1 = r3 instanceof java.lang.Integer
        L40:
            if (r1 == 0) goto L43
            return
        L43:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r3 = "Wrong object type used with protocol message reflection."
            r2.<init>(r3)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.i.f.f(d0.e0.p.d.m0.i.w$b, java.lang.Object):void");
    }

    public static void g(CodedOutputStream codedOutputStream, w.b bVar, int i, Object obj) throws IOException {
        if (bVar == w.b.f3406s) {
            codedOutputStream.writeGroup(i, (n) obj);
            return;
        }
        codedOutputStream.writeTag(i, c(bVar, false));
        h(codedOutputStream, bVar, obj);
    }

    public static void h(CodedOutputStream codedOutputStream, w.b bVar, Object obj) throws IOException {
        switch (bVar.ordinal()) {
            case 0:
                codedOutputStream.writeDoubleNoTag(((Double) obj).doubleValue());
                return;
            case 1:
                codedOutputStream.writeFloatNoTag(((Float) obj).floatValue());
                return;
            case 2:
                codedOutputStream.writeInt64NoTag(((Long) obj).longValue());
                return;
            case 3:
                codedOutputStream.writeUInt64NoTag(((Long) obj).longValue());
                return;
            case 4:
                codedOutputStream.writeInt32NoTag(((Integer) obj).intValue());
                return;
            case 5:
                codedOutputStream.writeFixed64NoTag(((Long) obj).longValue());
                return;
            case 6:
                codedOutputStream.writeFixed32NoTag(((Integer) obj).intValue());
                return;
            case 7:
                codedOutputStream.writeBoolNoTag(((Boolean) obj).booleanValue());
                return;
            case 8:
                codedOutputStream.writeStringNoTag((String) obj);
                return;
            case 9:
                codedOutputStream.writeGroupNoTag((n) obj);
                return;
            case 10:
                codedOutputStream.writeMessageNoTag((n) obj);
                return;
            case 11:
                if (obj instanceof c) {
                    codedOutputStream.writeBytesNoTag((c) obj);
                    return;
                } else {
                    codedOutputStream.writeByteArrayNoTag((byte[]) obj);
                    return;
                }
            case 12:
                codedOutputStream.writeUInt32NoTag(((Integer) obj).intValue());
                return;
            case 13:
                if (obj instanceof h.a) {
                    codedOutputStream.writeEnumNoTag(((h.a) obj).getNumber());
                    return;
                } else {
                    codedOutputStream.writeEnumNoTag(((Integer) obj).intValue());
                    return;
                }
            case 14:
                codedOutputStream.writeSFixed32NoTag(((Integer) obj).intValue());
                return;
            case 15:
                codedOutputStream.writeSFixed64NoTag(((Long) obj).longValue());
                return;
            case 16:
                codedOutputStream.writeSInt32NoTag(((Integer) obj).intValue());
                return;
            case 17:
                codedOutputStream.writeSInt64NoTag(((Long) obj).longValue());
                return;
            default:
                return;
        }
    }

    public static <T extends a<T>> f<T> newFieldSet() {
        return new f<>();
    }

    public static Object readPrimitiveField(d dVar, w.b bVar, boolean z2) throws IOException {
        switch (bVar.ordinal()) {
            case 0:
                return Double.valueOf(dVar.readDouble());
            case 1:
                return Float.valueOf(dVar.readFloat());
            case 2:
                return Long.valueOf(dVar.readInt64());
            case 3:
                return Long.valueOf(dVar.readUInt64());
            case 4:
                return Integer.valueOf(dVar.readInt32());
            case 5:
                return Long.valueOf(dVar.readFixed64());
            case 6:
                return Integer.valueOf(dVar.readFixed32());
            case 7:
                return Boolean.valueOf(dVar.readBool());
            case 8:
                if (z2) {
                    return dVar.readStringRequireUtf8();
                }
                return dVar.readString();
            case 9:
                throw new IllegalArgumentException("readPrimitiveField() cannot handle nested groups.");
            case 10:
                throw new IllegalArgumentException("readPrimitiveField() cannot handle embedded messages.");
            case 11:
                return dVar.readBytes();
            case 12:
                return Integer.valueOf(dVar.readUInt32());
            case 13:
                throw new IllegalArgumentException("readPrimitiveField() cannot handle enums.");
            case 14:
                return Integer.valueOf(dVar.readSFixed32());
            case 15:
                return Long.valueOf(dVar.readSFixed64());
            case 16:
                return Integer.valueOf(dVar.readSInt32());
            case 17:
                return Long.valueOf(dVar.readSInt64());
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
    }

    public static void writeField(a<?> aVar, Object obj, CodedOutputStream codedOutputStream) throws IOException {
        w.b liteType = aVar.getLiteType();
        int number = aVar.getNumber();
        if (aVar.isRepeated()) {
            List<Object> list = (List) obj;
            if (aVar.isPacked()) {
                codedOutputStream.writeTag(number, 2);
                int i = 0;
                for (Object obj2 : list) {
                    i += b(liteType, obj2);
                }
                codedOutputStream.writeRawVarint32(i);
                for (Object obj3 : list) {
                    h(codedOutputStream, liteType, obj3);
                }
                return;
            }
            for (Object obj4 : list) {
                g(codedOutputStream, liteType, number, obj4);
            }
        } else if (obj instanceof i) {
            g(codedOutputStream, liteType, number, ((i) obj).getValue());
        } else {
            g(codedOutputStream, liteType, number, obj);
        }
    }

    public final Object a(Object obj) {
        if (!(obj instanceof byte[])) {
            return obj;
        }
        byte[] bArr = (byte[]) obj;
        byte[] bArr2 = new byte[bArr.length];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        return bArr2;
    }

    public void addRepeatedField(FieldDescriptorType fielddescriptortype, Object obj) {
        List list;
        if (fielddescriptortype.isRepeated()) {
            f(fielddescriptortype.getLiteType(), obj);
            Object field = getField(fielddescriptortype);
            if (field == null) {
                list = new ArrayList();
                this.f3401b.put((t<FieldDescriptorType, Object>) fielddescriptortype, (FieldDescriptorType) list);
            } else {
                list = (List) field;
            }
            list.add(obj);
            return;
        }
        throw new IllegalArgumentException("addRepeatedField() can only be called on repeated fields.");
    }

    public final boolean d(Map.Entry<FieldDescriptorType, Object> entry) {
        FieldDescriptorType key = entry.getKey();
        if (key.getLiteJavaType() == w.c.MESSAGE) {
            if (key.isRepeated()) {
                for (n nVar : (List) entry.getValue()) {
                    if (!nVar.isInitialized()) {
                        return false;
                    }
                }
            } else {
                Object value = entry.getValue();
                if (value instanceof n) {
                    if (!((n) value).isInitialized()) {
                        return false;
                    }
                } else if (value instanceof i) {
                    return true;
                } else {
                    throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
                }
            }
        }
        return true;
    }

    public final void e(Map.Entry<FieldDescriptorType, Object> entry) {
        FieldDescriptorType key = entry.getKey();
        Object value = entry.getValue();
        if (value instanceof i) {
            value = ((i) value).getValue();
        }
        if (key.isRepeated()) {
            Object field = getField(key);
            if (field == null) {
                field = new ArrayList();
            }
            for (Object obj : (List) value) {
                ((List) field).add(a(obj));
            }
            this.f3401b.put((t<FieldDescriptorType, Object>) key, (FieldDescriptorType) field);
        } else if (key.getLiteJavaType() == w.c.MESSAGE) {
            Object field2 = getField(key);
            if (field2 == null) {
                this.f3401b.put((t<FieldDescriptorType, Object>) key, (FieldDescriptorType) a(value));
                return;
            }
            this.f3401b.put((t<FieldDescriptorType, Object>) key, (FieldDescriptorType) key.internalMergeFrom(((n) field2).toBuilder(), (n) value).build());
        } else {
            this.f3401b.put((t<FieldDescriptorType, Object>) key, (FieldDescriptorType) a(value));
        }
    }

    public Object getField(FieldDescriptorType fielddescriptortype) {
        Object obj = this.f3401b.get(fielddescriptortype);
        return obj instanceof i ? ((i) obj).getValue() : obj;
    }

    public Object getRepeatedField(FieldDescriptorType fielddescriptortype, int i) {
        if (fielddescriptortype.isRepeated()) {
            Object field = getField(fielddescriptortype);
            if (field != null) {
                return ((List) field).get(i);
            }
            throw new IndexOutOfBoundsException();
        }
        throw new IllegalArgumentException("getRepeatedField() can only be called on repeated fields.");
    }

    public int getRepeatedFieldCount(FieldDescriptorType fielddescriptortype) {
        if (fielddescriptortype.isRepeated()) {
            Object field = getField(fielddescriptortype);
            if (field == null) {
                return 0;
            }
            return ((List) field).size();
        }
        throw new IllegalArgumentException("getRepeatedField() can only be called on repeated fields.");
    }

    public int getSerializedSize() {
        int i = 0;
        for (int i2 = 0; i2 < this.f3401b.getNumArrayEntries(); i2++) {
            Map.Entry<FieldDescriptorType, Object> arrayEntryAt = this.f3401b.getArrayEntryAt(i2);
            i += computeFieldSize(arrayEntryAt.getKey(), arrayEntryAt.getValue());
        }
        for (Map.Entry<FieldDescriptorType, Object> entry : this.f3401b.getOverflowEntries()) {
            i += computeFieldSize(entry.getKey(), entry.getValue());
        }
        return i;
    }

    public boolean hasField(FieldDescriptorType fielddescriptortype) {
        if (!fielddescriptortype.isRepeated()) {
            return this.f3401b.get(fielddescriptortype) != null;
        }
        throw new IllegalArgumentException("hasField() can only be called on non-repeated fields.");
    }

    public boolean isInitialized() {
        for (int i = 0; i < this.f3401b.getNumArrayEntries(); i++) {
            if (!d(this.f3401b.getArrayEntryAt(i))) {
                return false;
            }
        }
        for (Map.Entry<FieldDescriptorType, Object> entry : this.f3401b.getOverflowEntries()) {
            if (!d(entry)) {
                return false;
            }
        }
        return true;
    }

    public Iterator<Map.Entry<FieldDescriptorType, Object>> iterator() {
        if (this.d) {
            return new i.c(this.f3401b.entrySet().iterator());
        }
        return this.f3401b.entrySet().iterator();
    }

    public void makeImmutable() {
        if (!this.c) {
            this.f3401b.makeImmutable();
            this.c = true;
        }
    }

    public void mergeFrom(f<FieldDescriptorType> fVar) {
        for (int i = 0; i < fVar.f3401b.getNumArrayEntries(); i++) {
            e(fVar.f3401b.getArrayEntryAt(i));
        }
        for (Map.Entry<FieldDescriptorType, Object> entry : fVar.f3401b.getOverflowEntries()) {
            e(entry);
        }
    }

    public void setField(FieldDescriptorType fielddescriptortype, Object obj) {
        if (!fielddescriptortype.isRepeated()) {
            f(fielddescriptortype.getLiteType(), obj);
        } else if (obj instanceof List) {
            ArrayList arrayList = new ArrayList();
            arrayList.addAll((List) obj);
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                f(fielddescriptortype.getLiteType(), it.next());
            }
            obj = arrayList;
        } else {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        }
        if (obj instanceof i) {
            this.d = true;
        }
        this.f3401b.put((t<FieldDescriptorType, Object>) fielddescriptortype, (FieldDescriptorType) obj);
    }

    public f<FieldDescriptorType> clone() {
        f<FieldDescriptorType> newFieldSet = newFieldSet();
        for (int i = 0; i < this.f3401b.getNumArrayEntries(); i++) {
            Map.Entry<FieldDescriptorType, Object> arrayEntryAt = this.f3401b.getArrayEntryAt(i);
            newFieldSet.setField(arrayEntryAt.getKey(), arrayEntryAt.getValue());
        }
        for (Map.Entry<FieldDescriptorType, Object> entry : this.f3401b.getOverflowEntries()) {
            newFieldSet.setField(entry.getKey(), entry.getValue());
        }
        newFieldSet.d = this.d;
        return newFieldSet;
    }

    public f(boolean z2) {
        int i = t.j;
        makeImmutable();
    }
}
