package d0.e0.p.d.m0.i;

import kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException;
/* compiled from: Parser.java */
/* loaded from: classes3.dex */
public interface p<MessageType> {
    MessageType parsePartialFrom(d dVar, e eVar) throws InvalidProtocolBufferException;
}
