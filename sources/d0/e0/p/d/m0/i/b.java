package d0.e0.p.d.m0.i;

import d0.e0.p.d.m0.i.a;
import d0.e0.p.d.m0.i.n;
import java.io.IOException;
import java.io.InputStream;
import kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException;
import kotlin.reflect.jvm.internal.impl.protobuf.UninitializedMessageException;
/* compiled from: AbstractParser.java */
/* loaded from: classes3.dex */
public abstract class b<MessageType extends n> implements p<MessageType> {
    static {
        e.getEmptyRegistry();
    }

    public final MessageType a(MessageType messagetype) throws InvalidProtocolBufferException {
        UninitializedMessageException uninitializedMessageException;
        if (messagetype == null || messagetype.isInitialized()) {
            return messagetype;
        }
        if (messagetype instanceof a) {
            uninitializedMessageException = new UninitializedMessageException((a) messagetype);
        } else {
            uninitializedMessageException = new UninitializedMessageException(messagetype);
        }
        throw uninitializedMessageException.asInvalidProtocolBufferException().setUnfinishedMessage(messagetype);
    }

    public MessageType parsePartialDelimitedFrom(InputStream inputStream, e eVar) throws InvalidProtocolBufferException {
        try {
            int read = inputStream.read();
            if (read == -1) {
                return null;
            }
            return parsePartialFrom(new a.AbstractC0335a.C0336a(inputStream, d.readRawVarint32(read, inputStream)), eVar);
        } catch (IOException e) {
            throw new InvalidProtocolBufferException(e.getMessage());
        }
    }

    public MessageType parsePartialFrom(InputStream inputStream, e eVar) throws InvalidProtocolBufferException {
        d newInstance = d.newInstance(inputStream);
        MessageType messagetype = (MessageType) parsePartialFrom(newInstance, eVar);
        try {
            newInstance.checkLastTagWas(0);
            return messagetype;
        } catch (InvalidProtocolBufferException e) {
            throw e.setUnfinishedMessage(messagetype);
        }
    }

    public MessageType parseDelimitedFrom(InputStream inputStream, e eVar) throws InvalidProtocolBufferException {
        MessageType parsePartialDelimitedFrom = parsePartialDelimitedFrom(inputStream, eVar);
        a(parsePartialDelimitedFrom);
        return parsePartialDelimitedFrom;
    }

    public MessageType parseFrom(InputStream inputStream, e eVar) throws InvalidProtocolBufferException {
        MessageType parsePartialFrom = parsePartialFrom(inputStream, eVar);
        a(parsePartialFrom);
        return parsePartialFrom;
    }
}
