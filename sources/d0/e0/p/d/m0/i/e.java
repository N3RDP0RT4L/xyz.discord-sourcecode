package d0.e0.p.d.m0.i;

import d0.e0.p.d.m0.i.g;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
/* compiled from: ExtensionRegistryLite.java */
/* loaded from: classes3.dex */
public class e {
    public static final e a = new e(true);

    /* renamed from: b  reason: collision with root package name */
    public final Map<a, g.f<?, ?>> f3399b;

    /* compiled from: ExtensionRegistryLite.java */
    /* loaded from: classes3.dex */
    public static final class a {
        public final Object a;

        /* renamed from: b  reason: collision with root package name */
        public final int f3400b;

        public a(Object obj, int i) {
            this.a = obj;
            this.f3400b = i;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return this.a == aVar.a && this.f3400b == aVar.f3400b;
        }

        public int hashCode() {
            return (System.identityHashCode(this.a) * 65535) + this.f3400b;
        }
    }

    public e() {
        this.f3399b = new HashMap();
    }

    public static e getEmptyRegistry() {
        return a;
    }

    public static e newInstance() {
        return new e();
    }

    public final void add(g.f<?, ?> fVar) {
        this.f3399b.put(new a(fVar.getContainingTypeDefaultInstance(), fVar.getNumber()), fVar);
    }

    public <ContainingType extends n> g.f<ContainingType, ?> findLiteExtensionByNumber(ContainingType containingtype, int i) {
        return (g.f<ContainingType, ?>) this.f3399b.get(new a(containingtype, i));
    }

    public e(boolean z2) {
        this.f3399b = Collections.emptyMap();
    }
}
