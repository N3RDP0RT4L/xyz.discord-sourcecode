package d0.e0.p.d.m0.i;

import d0.e0.p.d.m0.i.c;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.NoSuchElementException;
/* compiled from: LiteralByteString.java */
/* loaded from: classes3.dex */
public class m extends c {
    public final byte[] k;
    public int l = 0;

    /* compiled from: LiteralByteString.java */
    /* loaded from: classes3.dex */
    public class b implements c.a {
        public int j = 0;
        public final int k;

        public b(a aVar) {
            this.k = m.this.size();
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            return this.j < this.k;
        }

        @Override // d0.e0.p.d.m0.i.c.a
        public byte nextByte() {
            try {
                byte[] bArr = m.this.k;
                int i = this.j;
                this.j = i + 1;
                return bArr[i];
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new NoSuchElementException(e.getMessage());
            }
        }

        @Override // java.util.Iterator
        public void remove() {
            throw new UnsupportedOperationException();
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // java.util.Iterator
        public Byte next() {
            return Byte.valueOf(nextByte());
        }
    }

    public m(byte[] bArr) {
        this.k = bArr;
    }

    @Override // d0.e0.p.d.m0.i.c
    public void d(byte[] bArr, int i, int i2, int i3) {
        System.arraycopy(this.k, i, bArr, i2, i3);
    }

    @Override // d0.e0.p.d.m0.i.c
    public int e() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof c) || size() != ((c) obj).size()) {
            return false;
        }
        if (size() == 0) {
            return true;
        }
        if (obj instanceof m) {
            return l((m) obj, 0, size());
        }
        if (obj instanceof r) {
            return obj.equals(this);
        }
        String valueOf = String.valueOf(obj.getClass());
        throw new IllegalArgumentException(b.d.b.a.a.H(new StringBuilder(valueOf.length() + 49), "Has a new type of ByteString been created? Found ", valueOf));
    }

    @Override // d0.e0.p.d.m0.i.c
    public boolean g() {
        return true;
    }

    @Override // d0.e0.p.d.m0.i.c
    public int h(int i, int i2, int i3) {
        byte[] bArr = this.k;
        int m = m() + i2;
        for (int i4 = m; i4 < m + i3; i4++) {
            i = (i * 31) + bArr[i4];
        }
        return i;
    }

    public int hashCode() {
        int i = this.l;
        if (i == 0) {
            int size = size();
            i = h(size, 0, size);
            if (i == 0) {
                i = 1;
            }
            this.l = i;
        }
        return i;
    }

    @Override // d0.e0.p.d.m0.i.c
    public int i(int i, int i2, int i3) {
        int m = m() + i2;
        return v.partialIsValidUtf8(i, this.k, m, i3 + m);
    }

    @Override // d0.e0.p.d.m0.i.c
    public boolean isValidUtf8() {
        int m = m();
        return v.isValidUtf8(this.k, m, size() + m);
    }

    @Override // d0.e0.p.d.m0.i.c
    public int j() {
        return this.l;
    }

    @Override // d0.e0.p.d.m0.i.c
    public void k(OutputStream outputStream, int i, int i2) throws IOException {
        outputStream.write(this.k, m() + i, i2);
    }

    public boolean l(m mVar, int i, int i2) {
        if (i2 > mVar.size()) {
            int size = size();
            StringBuilder sb = new StringBuilder(40);
            sb.append("Length too large: ");
            sb.append(i2);
            sb.append(size);
            throw new IllegalArgumentException(sb.toString());
        } else if (i + i2 <= mVar.size()) {
            byte[] bArr = this.k;
            byte[] bArr2 = mVar.k;
            int m = m() + i2;
            int m2 = m();
            int m3 = mVar.m() + i;
            while (m2 < m) {
                if (bArr[m2] != bArr2[m3]) {
                    return false;
                }
                m2++;
                m3++;
            }
            return true;
        } else {
            int size2 = mVar.size();
            StringBuilder sb2 = new StringBuilder(59);
            sb2.append("Ran off end of other: ");
            sb2.append(i);
            sb2.append(", ");
            sb2.append(i2);
            sb2.append(", ");
            sb2.append(size2);
            throw new IllegalArgumentException(sb2.toString());
        }
    }

    public int m() {
        return 0;
    }

    @Override // d0.e0.p.d.m0.i.c
    public int size() {
        return this.k.length;
    }

    @Override // d0.e0.p.d.m0.i.c
    public String toString(String str) throws UnsupportedEncodingException {
        return new String(this.k, m(), size(), str);
    }

    @Override // d0.e0.p.d.m0.i.c, java.lang.Iterable
    /* renamed from: iterator */
    public Iterator<Byte> iterator2() {
        return new b(null);
    }
}
