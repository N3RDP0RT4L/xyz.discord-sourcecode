package d0.e0.p.d.m0.i;

import java.lang.Comparable;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
/* compiled from: SmallSortedMap.java */
/* loaded from: classes3.dex */
public class t<K extends Comparable<K>, V> extends AbstractMap<K, V> {
    public static final /* synthetic */ int j = 0;
    public final int k;
    public List<t<K, V>.b> l = Collections.emptyList();
    public Map<K, V> m = Collections.emptyMap();
    public boolean n;
    public volatile t<K, V>.d o;

    /* compiled from: SmallSortedMap.java */
    /* loaded from: classes3.dex */
    public static class a {
        public static final Iterator<Object> a = new C0337a();

        /* renamed from: b  reason: collision with root package name */
        public static final Iterable<Object> f3405b = new b();

        /* compiled from: SmallSortedMap.java */
        /* renamed from: d0.e0.p.d.m0.i.t$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static class C0337a implements Iterator<Object> {
            @Override // java.util.Iterator
            public boolean hasNext() {
                return false;
            }

            @Override // java.util.Iterator
            public Object next() {
                throw new NoSuchElementException();
            }

            @Override // java.util.Iterator
            public void remove() {
                throw new UnsupportedOperationException();
            }
        }

        /* compiled from: SmallSortedMap.java */
        /* loaded from: classes3.dex */
        public static class b implements Iterable<Object> {
            @Override // java.lang.Iterable
            public Iterator<Object> iterator() {
                return a.a;
            }
        }
    }

    /* compiled from: SmallSortedMap.java */
    /* loaded from: classes3.dex */
    public class b implements Comparable<t<K, V>.b>, Map.Entry<K, V> {
        public final K j;
        public V k;

        public b(t tVar, Map.Entry<K, V> entry) {
            V value = entry.getValue();
            t.this = tVar;
            this.j = entry.getKey();
            this.k = value;
        }

        @Override // java.lang.Comparable
        public /* bridge */ /* synthetic */ int compareTo(Object obj) {
            return compareTo((b) ((b) obj));
        }

        @Override // java.util.Map.Entry
        public boolean equals(Object obj) {
            boolean z2;
            boolean z3;
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            K k = this.j;
            Object key = entry.getKey();
            if (k == null) {
                z2 = key == null;
            } else {
                z2 = k.equals(key);
            }
            if (z2) {
                V v = this.k;
                Object value = entry.getValue();
                if (v == null) {
                    z3 = value == null;
                } else {
                    z3 = v.equals(value);
                }
                if (z3) {
                    return true;
                }
            }
            return false;
        }

        @Override // java.util.Map.Entry
        public V getValue() {
            return this.k;
        }

        @Override // java.util.Map.Entry
        public int hashCode() {
            K k = this.j;
            int i = 0;
            int hashCode = k == null ? 0 : k.hashCode();
            V v = this.k;
            if (v != null) {
                i = v.hashCode();
            }
            return hashCode ^ i;
        }

        @Override // java.util.Map.Entry
        public V setValue(V v) {
            t tVar = t.this;
            int i = t.j;
            tVar.b();
            V v2 = this.k;
            this.k = v;
            return v2;
        }

        public String toString() {
            String valueOf = String.valueOf(this.j);
            String valueOf2 = String.valueOf(this.k);
            return b.d.b.a.a.J(new StringBuilder(valueOf2.length() + valueOf.length() + 1), valueOf, "=", valueOf2);
        }

        public int compareTo(t<K, V>.b bVar) {
            return getKey().compareTo(bVar.getKey());
        }

        @Override // java.util.Map.Entry
        public K getKey() {
            return this.j;
        }

        public b(K k, V v) {
            this.j = k;
            this.k = v;
        }
    }

    /* compiled from: SmallSortedMap.java */
    /* loaded from: classes3.dex */
    public class c implements Iterator<Map.Entry<K, V>> {
        public int j = -1;
        public boolean k;
        public Iterator<Map.Entry<K, V>> l;

        public c(s sVar) {
        }

        public final Iterator<Map.Entry<K, V>> a() {
            if (this.l == null) {
                this.l = t.this.m.entrySet().iterator();
            }
            return this.l;
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            return this.j + 1 < t.this.l.size() || a().hasNext();
        }

        @Override // java.util.Iterator
        public void remove() {
            if (this.k) {
                this.k = false;
                t tVar = t.this;
                int i = t.j;
                tVar.b();
                if (this.j < t.this.l.size()) {
                    t tVar2 = t.this;
                    int i2 = this.j;
                    this.j = i2 - 1;
                    tVar2.d(i2);
                    return;
                }
                a().remove();
                return;
            }
            throw new IllegalStateException("remove() was called before next()");
        }

        @Override // java.util.Iterator
        public Map.Entry<K, V> next() {
            this.k = true;
            int i = this.j + 1;
            this.j = i;
            if (i < t.this.l.size()) {
                return t.this.l.get(this.j);
            }
            return a().next();
        }
    }

    /* compiled from: SmallSortedMap.java */
    /* loaded from: classes3.dex */
    public class d extends AbstractSet<Map.Entry<K, V>> {
        public d(s sVar) {
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public /* bridge */ /* synthetic */ boolean add(Object obj) {
            return add((Map.Entry) ((Map.Entry) obj));
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public void clear() {
            t.this.clear();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean contains(Object obj) {
            Map.Entry entry = (Map.Entry) obj;
            Object obj2 = t.this.get(entry.getKey());
            Object value = entry.getValue();
            return obj2 == value || (obj2 != null && obj2.equals(value));
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
        public Iterator<Map.Entry<K, V>> iterator() {
            return new c(null);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean remove(Object obj) {
            Map.Entry entry = (Map.Entry) obj;
            if (!contains(entry)) {
                return false;
            }
            t.this.remove(entry.getKey());
            return true;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public int size() {
            return t.this.size();
        }

        public boolean add(Map.Entry<K, V> entry) {
            if (contains(entry)) {
                return false;
            }
            t.this.put((t) entry.getKey(), (K) entry.getValue());
            return true;
        }
    }

    public t(int i, s sVar) {
        this.k = i;
    }

    public final int a(K k) {
        int size = this.l.size() - 1;
        if (size >= 0) {
            int compareTo = k.compareTo(this.l.get(size).getKey());
            if (compareTo > 0) {
                return -(size + 2);
            }
            if (compareTo == 0) {
                return size;
            }
        }
        int i = 0;
        while (i <= size) {
            int i2 = (i + size) / 2;
            int compareTo2 = k.compareTo(this.l.get(i2).getKey());
            if (compareTo2 < 0) {
                size = i2 - 1;
            } else if (compareTo2 <= 0) {
                return i2;
            } else {
                i = i2 + 1;
            }
        }
        return -(i + 1);
    }

    public final void b() {
        if (this.n) {
            throw new UnsupportedOperationException();
        }
    }

    public final SortedMap<K, V> c() {
        b();
        if (this.m.isEmpty() && !(this.m instanceof TreeMap)) {
            this.m = new TreeMap();
        }
        return (SortedMap) this.m;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public void clear() {
        b();
        if (!this.l.isEmpty()) {
            this.l.clear();
        }
        if (!this.m.isEmpty()) {
            this.m.clear();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public boolean containsKey(Object obj) {
        Comparable comparable = (Comparable) obj;
        return a(comparable) >= 0 || this.m.containsKey(comparable);
    }

    public final V d(int i) {
        b();
        V value = this.l.remove(i).getValue();
        if (!this.m.isEmpty()) {
            Iterator<Map.Entry<K, V>> it = c().entrySet().iterator();
            this.l.add(new b(this, it.next()));
            it.remove();
        }
        return value;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Set<Map.Entry<K, V>> entrySet() {
        if (this.o == null) {
            this.o = new d(null);
        }
        return this.o;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public V get(Object obj) {
        Comparable comparable = (Comparable) obj;
        int a2 = a(comparable);
        if (a2 >= 0) {
            return this.l.get(a2).getValue();
        }
        return this.m.get(comparable);
    }

    public Map.Entry<K, V> getArrayEntryAt(int i) {
        return this.l.get(i);
    }

    public int getNumArrayEntries() {
        return this.l.size();
    }

    public Iterable<Map.Entry<K, V>> getOverflowEntries() {
        return this.m.isEmpty() ? (Iterable<Map.Entry<K, V>>) a.f3405b : this.m.entrySet();
    }

    public boolean isImmutable() {
        return this.n;
    }

    public void makeImmutable() {
        if (!this.n) {
            this.m = this.m.isEmpty() ? Collections.emptyMap() : Collections.unmodifiableMap(this.m);
            this.n = true;
        }
    }

    public V put(K k, V v) {
        b();
        int a2 = a(k);
        if (a2 >= 0) {
            return this.l.get(a2).setValue(v);
        }
        b();
        if (this.l.isEmpty() && !(this.l instanceof ArrayList)) {
            this.l = new ArrayList(this.k);
        }
        int i = -(a2 + 1);
        if (i >= this.k) {
            return c().put(k, v);
        }
        int size = this.l.size();
        int i2 = this.k;
        if (size == i2) {
            t<K, V>.b remove = this.l.remove(i2 - 1);
            c().put((K) remove.getKey(), remove.getValue());
        }
        this.l.add(i, new b(k, v));
        return null;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public V remove(Object obj) {
        b();
        Comparable comparable = (Comparable) obj;
        int a2 = a(comparable);
        if (a2 >= 0) {
            return (V) d(a2);
        }
        if (this.m.isEmpty()) {
            return null;
        }
        return this.m.remove(comparable);
    }

    @Override // java.util.AbstractMap, java.util.Map
    public int size() {
        return this.m.size() + this.l.size();
    }
}
