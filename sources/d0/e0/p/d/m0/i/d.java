package d0.e0.p.d.m0.i;

import com.adjust.sdk.Constants;
import d0.e0.p.d.m0.i.n;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import kotlin.reflect.jvm.internal.impl.protobuf.CodedOutputStream;
import kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException;
import org.objectweb.asm.Opcodes;
/* compiled from: CodedInputStream.java */
/* loaded from: classes3.dex */
public final class d {
    public int d;
    public final InputStream f;
    public int g;
    public int j;
    public int i = Integer.MAX_VALUE;
    public final byte[] a = new byte[4096];
    public int c = 0;
    public int e = 0;
    public int h = 0;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f3398b = false;

    public d(InputStream inputStream) {
        this.f = inputStream;
    }

    public static int decodeZigZag32(int i) {
        return (-(i & 1)) ^ (i >>> 1);
    }

    public static long decodeZigZag64(long j) {
        return (-(j & 1)) ^ (j >>> 1);
    }

    public static d newInstance(InputStream inputStream) {
        return new d(inputStream);
    }

    public final byte[] a(int i) throws IOException {
        if (i > 0) {
            int i2 = this.h;
            int i3 = this.e;
            int i4 = i2 + i3 + i;
            int i5 = this.i;
            if (i4 > i5) {
                skipRawBytes((i5 - i2) - i3);
                throw InvalidProtocolBufferException.c();
            } else if (i < 4096) {
                byte[] bArr = new byte[i];
                int i6 = this.c - i3;
                System.arraycopy(this.a, i3, bArr, 0, i6);
                int i7 = this.c;
                this.e = i7;
                int i8 = i - i6;
                if (i7 - i7 >= i8 || e(i8)) {
                    System.arraycopy(this.a, 0, bArr, i6, i8);
                    this.e = i8;
                    return bArr;
                }
                throw InvalidProtocolBufferException.c();
            } else {
                int i9 = this.c;
                this.h = i2 + i9;
                this.e = 0;
                this.c = 0;
                int i10 = i9 - i3;
                int i11 = i - i10;
                ArrayList arrayList = new ArrayList();
                while (i11 > 0) {
                    int min = Math.min(i11, 4096);
                    byte[] bArr2 = new byte[min];
                    int i12 = 0;
                    while (i12 < min) {
                        InputStream inputStream = this.f;
                        int read = inputStream == null ? -1 : inputStream.read(bArr2, i12, min - i12);
                        if (read != -1) {
                            this.h += read;
                            i12 += read;
                        } else {
                            throw InvalidProtocolBufferException.c();
                        }
                    }
                    i11 -= min;
                    arrayList.add(bArr2);
                }
                byte[] bArr3 = new byte[i];
                System.arraycopy(this.a, i3, bArr3, 0, i10);
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    byte[] bArr4 = (byte[]) it.next();
                    System.arraycopy(bArr4, 0, bArr3, i10, bArr4.length);
                    i10 += bArr4.length;
                }
                return bArr3;
            }
        } else if (i == 0) {
            return h.a;
        } else {
            throw InvalidProtocolBufferException.a();
        }
    }

    public long b() throws IOException {
        long j = 0;
        for (int i = 0; i < 64; i += 7) {
            byte readRawByte = readRawByte();
            j |= (readRawByte & Byte.MAX_VALUE) << i;
            if ((readRawByte & 128) == 0) {
                return j;
            }
        }
        throw new InvalidProtocolBufferException("CodedInputStream encountered a malformed varint.");
    }

    public final void c() {
        int i = this.c + this.d;
        this.c = i;
        int i2 = this.h + i;
        int i3 = this.i;
        if (i2 > i3) {
            int i4 = i2 - i3;
            this.d = i4;
            this.c = i - i4;
            return;
        }
        this.d = 0;
    }

    public void checkLastTagWas(int i) throws InvalidProtocolBufferException {
        if (this.g != i) {
            throw new InvalidProtocolBufferException("Protocol message end-group tag did not match expected tag.");
        }
    }

    public final void d(int i) throws IOException {
        if (!e(i)) {
            throw InvalidProtocolBufferException.c();
        }
    }

    public final boolean e(int i) throws IOException {
        int i2 = this.e;
        int i3 = i2 + i;
        int i4 = this.c;
        if (i3 > i4) {
            if (this.h + i2 + i <= this.i && this.f != null) {
                if (i2 > 0) {
                    if (i4 > i2) {
                        byte[] bArr = this.a;
                        System.arraycopy(bArr, i2, bArr, 0, i4 - i2);
                    }
                    this.h += i2;
                    this.c -= i2;
                    this.e = 0;
                }
                InputStream inputStream = this.f;
                byte[] bArr2 = this.a;
                int i5 = this.c;
                int read = inputStream.read(bArr2, i5, bArr2.length - i5);
                if (read == 0 || read < -1 || read > this.a.length) {
                    StringBuilder sb = new StringBuilder(102);
                    sb.append("InputStream#read(byte[]) returned invalid result: ");
                    sb.append(read);
                    sb.append("\nThe InputStream implementation is buggy.");
                    throw new IllegalStateException(sb.toString());
                } else if (read > 0) {
                    this.c += read;
                    if ((this.h + i) - 67108864 <= 0) {
                        c();
                        if (this.c >= i) {
                            return true;
                        }
                        return e(i);
                    }
                    throw new InvalidProtocolBufferException("Protocol message was too large.  May be malicious.  Use CodedInputStream.setSizeLimit() to increase the size limit.");
                }
            }
            return false;
        }
        StringBuilder sb2 = new StringBuilder(77);
        sb2.append("refillBuffer() called when ");
        sb2.append(i);
        sb2.append(" bytes were already available in buffer");
        throw new IllegalStateException(sb2.toString());
    }

    public int getBytesUntilLimit() {
        int i = this.i;
        if (i == Integer.MAX_VALUE) {
            return -1;
        }
        return i - (this.h + this.e);
    }

    public boolean isAtEnd() throws IOException {
        return this.e == this.c && !e(1);
    }

    public void popLimit(int i) {
        this.i = i;
        c();
    }

    public int pushLimit(int i) throws InvalidProtocolBufferException {
        if (i >= 0) {
            int i2 = this.h + this.e + i;
            int i3 = this.i;
            if (i2 <= i3) {
                this.i = i2;
                c();
                return i3;
            }
            throw InvalidProtocolBufferException.c();
        }
        throw InvalidProtocolBufferException.a();
    }

    public boolean readBool() throws IOException {
        return readRawVarint64() != 0;
    }

    public c readBytes() throws IOException {
        int readRawVarint32 = readRawVarint32();
        int i = this.c;
        int i2 = this.e;
        if (readRawVarint32 <= i - i2 && readRawVarint32 > 0) {
            boolean z2 = this.f3398b;
            c copyFrom = c.copyFrom(this.a, i2, readRawVarint32);
            this.e += readRawVarint32;
            return copyFrom;
        } else if (readRawVarint32 == 0) {
            return c.j;
        } else {
            return new m(a(readRawVarint32));
        }
    }

    public double readDouble() throws IOException {
        return Double.longBitsToDouble(readRawLittleEndian64());
    }

    public int readEnum() throws IOException {
        return readRawVarint32();
    }

    public int readFixed32() throws IOException {
        return readRawLittleEndian32();
    }

    public long readFixed64() throws IOException {
        return readRawLittleEndian64();
    }

    public float readFloat() throws IOException {
        return Float.intBitsToFloat(readRawLittleEndian32());
    }

    public void readGroup(int i, n.a aVar, e eVar) throws IOException {
        int i2 = this.j;
        if (i2 < 64) {
            this.j = i2 + 1;
            aVar.mergeFrom(this, eVar);
            checkLastTagWas((i << 3) | 4);
            this.j--;
            return;
        }
        throw InvalidProtocolBufferException.b();
    }

    public int readInt32() throws IOException {
        return readRawVarint32();
    }

    public long readInt64() throws IOException {
        return readRawVarint64();
    }

    public void readMessage(n.a aVar, e eVar) throws IOException {
        int readRawVarint32 = readRawVarint32();
        if (this.j < 64) {
            int pushLimit = pushLimit(readRawVarint32);
            this.j++;
            aVar.mergeFrom(this, eVar);
            checkLastTagWas(0);
            this.j--;
            popLimit(pushLimit);
            return;
        }
        throw InvalidProtocolBufferException.b();
    }

    public byte readRawByte() throws IOException {
        if (this.e == this.c) {
            d(1);
        }
        byte[] bArr = this.a;
        int i = this.e;
        this.e = i + 1;
        return bArr[i];
    }

    public int readRawLittleEndian32() throws IOException {
        int i = this.e;
        if (this.c - i < 4) {
            d(4);
            i = this.e;
        }
        byte[] bArr = this.a;
        this.e = i + 4;
        return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
    }

    public long readRawLittleEndian64() throws IOException {
        int i = this.e;
        if (this.c - i < 8) {
            d(8);
            i = this.e;
        }
        byte[] bArr = this.a;
        this.e = i + 8;
        return ((bArr[i + 7] & 255) << 56) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16) | ((bArr[i + 3] & 255) << 24) | ((bArr[i + 4] & 255) << 32) | ((bArr[i + 5] & 255) << 40) | ((bArr[i + 6] & 255) << 48);
    }

    /* JADX WARN: Code restructure failed: missing block: B:32:0x007a, code lost:
        if (r2[r3] < 0) goto L33;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public int readRawVarint32() throws java.io.IOException {
        /*
            r9 = this;
            int r0 = r9.e
            int r1 = r9.c
            if (r1 != r0) goto L8
            goto L7c
        L8:
            byte[] r2 = r9.a
            int r3 = r0 + 1
            r0 = r2[r0]
            if (r0 < 0) goto L13
            r9.e = r3
            return r0
        L13:
            int r1 = r1 - r3
            r4 = 9
            if (r1 >= r4) goto L19
            goto L7c
        L19:
            int r1 = r3 + 1
            r3 = r2[r3]
            int r3 = r3 << 7
            r0 = r0 ^ r3
            long r3 = (long) r0
            r5 = 0
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 >= 0) goto L2d
            r5 = -128(0xffffffffffffff80, double:NaN)
        L29:
            long r2 = r3 ^ r5
            int r0 = (int) r2
            goto L82
        L2d:
            int r3 = r1 + 1
            r1 = r2[r1]
            int r1 = r1 << 14
            r0 = r0 ^ r1
            long r7 = (long) r0
            int r1 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r1 < 0) goto L3f
            r0 = 16256(0x3f80, double:8.0315E-320)
            long r0 = r0 ^ r7
            int r0 = (int) r0
        L3d:
            r1 = r3
            goto L82
        L3f:
            int r1 = r3 + 1
            r3 = r2[r3]
            int r3 = r3 << 21
            r0 = r0 ^ r3
            long r3 = (long) r0
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 >= 0) goto L4f
            r5 = -2080896(0xffffffffffe03f80, double:NaN)
            goto L29
        L4f:
            int r3 = r1 + 1
            r1 = r2[r1]
            int r4 = r1 << 28
            r0 = r0 ^ r4
            long r4 = (long) r0
            r6 = 266354560(0xfe03f80, double:1.315966377E-315)
            long r4 = r4 ^ r6
            int r0 = (int) r4
            if (r1 >= 0) goto L3d
            int r1 = r3 + 1
            r3 = r2[r3]
            if (r3 >= 0) goto L82
            int r3 = r1 + 1
            r1 = r2[r1]
            if (r1 >= 0) goto L3d
            int r1 = r3 + 1
            r3 = r2[r3]
            if (r3 >= 0) goto L82
            int r3 = r1 + 1
            r1 = r2[r1]
            if (r1 >= 0) goto L3d
            int r1 = r3 + 1
            r2 = r2[r3]
            if (r2 >= 0) goto L82
        L7c:
            long r0 = r9.b()
            int r1 = (int) r0
            return r1
        L82:
            r9.e = r1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.i.d.readRawVarint32():int");
    }

    /* JADX WARN: Code restructure failed: missing block: B:36:0x00b6, code lost:
        if (r2[r0] < 0) goto L37;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public long readRawVarint64() throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 194
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.i.d.readRawVarint64():long");
    }

    public int readSFixed32() throws IOException {
        return readRawLittleEndian32();
    }

    public long readSFixed64() throws IOException {
        return readRawLittleEndian64();
    }

    public int readSInt32() throws IOException {
        return decodeZigZag32(readRawVarint32());
    }

    public long readSInt64() throws IOException {
        return decodeZigZag64(readRawVarint64());
    }

    public String readString() throws IOException {
        int readRawVarint32 = readRawVarint32();
        if (readRawVarint32 > this.c - this.e || readRawVarint32 <= 0) {
            return readRawVarint32 == 0 ? "" : new String(a(readRawVarint32), Constants.ENCODING);
        }
        String str = new String(this.a, this.e, readRawVarint32, Constants.ENCODING);
        this.e += readRawVarint32;
        return str;
    }

    public String readStringRequireUtf8() throws IOException {
        byte[] bArr;
        int readRawVarint32 = readRawVarint32();
        int i = this.e;
        if (readRawVarint32 <= this.c - i && readRawVarint32 > 0) {
            bArr = this.a;
            this.e = i + readRawVarint32;
        } else if (readRawVarint32 == 0) {
            return "";
        } else {
            bArr = a(readRawVarint32);
            i = 0;
        }
        if (v.isValidUtf8(bArr, i, i + readRawVarint32)) {
            return new String(bArr, i, readRawVarint32, Constants.ENCODING);
        }
        throw new InvalidProtocolBufferException("Protocol message had invalid UTF-8.");
    }

    public int readTag() throws IOException {
        if (isAtEnd()) {
            this.g = 0;
            return 0;
        }
        int readRawVarint32 = readRawVarint32();
        this.g = readRawVarint32;
        if (w.getTagFieldNumber(readRawVarint32) != 0) {
            return this.g;
        }
        throw new InvalidProtocolBufferException("Protocol message contained an invalid tag (zero).");
    }

    public int readUInt32() throws IOException {
        return readRawVarint32();
    }

    public long readUInt64() throws IOException {
        return readRawVarint64();
    }

    public boolean skipField(int i, CodedOutputStream codedOutputStream) throws IOException {
        int i2 = i & 7;
        if (i2 == 0) {
            long readInt64 = readInt64();
            codedOutputStream.writeRawVarint32(i);
            codedOutputStream.writeUInt64NoTag(readInt64);
            return true;
        } else if (i2 == 1) {
            long readRawLittleEndian64 = readRawLittleEndian64();
            codedOutputStream.writeRawVarint32(i);
            codedOutputStream.writeFixed64NoTag(readRawLittleEndian64);
            return true;
        } else if (i2 == 2) {
            c readBytes = readBytes();
            codedOutputStream.writeRawVarint32(i);
            codedOutputStream.writeBytesNoTag(readBytes);
            return true;
        } else if (i2 == 3) {
            codedOutputStream.writeRawVarint32(i);
            skipMessage(codedOutputStream);
            int tagFieldNumber = (w.getTagFieldNumber(i) << 3) | 4;
            checkLastTagWas(tagFieldNumber);
            codedOutputStream.writeRawVarint32(tagFieldNumber);
            return true;
        } else if (i2 == 4) {
            return false;
        } else {
            if (i2 == 5) {
                int readRawLittleEndian32 = readRawLittleEndian32();
                codedOutputStream.writeRawVarint32(i);
                codedOutputStream.writeFixed32NoTag(readRawLittleEndian32);
                return true;
            }
            throw new InvalidProtocolBufferException("Protocol message tag had invalid wire type.");
        }
    }

    public void skipMessage(CodedOutputStream codedOutputStream) throws IOException {
        int readTag;
        do {
            readTag = readTag();
            if (readTag == 0) {
                return;
            }
        } while (skipField(readTag, codedOutputStream));
    }

    public void skipRawBytes(int i) throws IOException {
        int i2 = this.c;
        int i3 = this.e;
        if (i <= i2 - i3 && i >= 0) {
            this.e = i3 + i;
        } else if (i >= 0) {
            int i4 = this.h;
            int i5 = i4 + i3 + i;
            int i6 = this.i;
            if (i5 <= i6) {
                int i7 = i2 - i3;
                this.e = i2;
                d(1);
                while (true) {
                    int i8 = i - i7;
                    int i9 = this.c;
                    if (i8 > i9) {
                        i7 += i9;
                        this.e = i9;
                        d(1);
                    } else {
                        this.e = i8;
                        return;
                    }
                }
            } else {
                skipRawBytes((i6 - i4) - i3);
                throw InvalidProtocolBufferException.c();
            }
        } else {
            throw InvalidProtocolBufferException.a();
        }
    }

    public <T extends n> T readMessage(p<T> pVar, e eVar) throws IOException {
        int readRawVarint32 = readRawVarint32();
        if (this.j < 64) {
            int pushLimit = pushLimit(readRawVarint32);
            this.j++;
            T parsePartialFrom = pVar.parsePartialFrom(this, eVar);
            checkLastTagWas(0);
            this.j--;
            popLimit(pushLimit);
            return parsePartialFrom;
        }
        throw InvalidProtocolBufferException.b();
    }

    public static int readRawVarint32(int i, InputStream inputStream) throws IOException {
        if ((i & 128) == 0) {
            return i;
        }
        int i2 = i & Opcodes.LAND;
        int i3 = 7;
        while (i3 < 32) {
            int read = inputStream.read();
            if (read != -1) {
                i2 |= (read & Opcodes.LAND) << i3;
                if ((read & 128) == 0) {
                    return i2;
                }
                i3 += 7;
            } else {
                throw InvalidProtocolBufferException.c();
            }
        }
        while (i3 < 64) {
            int read2 = inputStream.read();
            if (read2 == -1) {
                throw InvalidProtocolBufferException.c();
            } else if ((read2 & 128) == 0) {
                return i2;
            } else {
                i3 += 7;
            }
        }
        throw new InvalidProtocolBufferException("CodedInputStream encountered a malformed varint.");
    }
}
