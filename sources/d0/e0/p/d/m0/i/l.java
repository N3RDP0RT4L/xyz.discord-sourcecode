package d0.e0.p.d.m0.i;

import java.util.List;
/* compiled from: LazyStringList.java */
/* loaded from: classes3.dex */
public interface l extends q {
    void add(c cVar);

    c getByteString(int i);

    List<?> getUnderlyingElements();

    l getUnmodifiableView();
}
