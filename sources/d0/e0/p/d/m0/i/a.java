package d0.e0.p.d.m0.i;

import d0.e0.p.d.m0.i.n;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import kotlin.reflect.jvm.internal.impl.protobuf.CodedOutputStream;
/* compiled from: AbstractMessageLite.java */
/* loaded from: classes3.dex */
public abstract class a implements n {
    public int memoizedHashCode = 0;

    public void writeDelimitedTo(OutputStream outputStream) throws IOException {
        int serializedSize = getSerializedSize();
        int computeRawVarint32Size = CodedOutputStream.computeRawVarint32Size(serializedSize) + serializedSize;
        if (computeRawVarint32Size > 4096) {
            computeRawVarint32Size = 4096;
        }
        CodedOutputStream newInstance = CodedOutputStream.newInstance(outputStream, computeRawVarint32Size);
        newInstance.writeRawVarint32(serializedSize);
        writeTo(newInstance);
        newInstance.flush();
    }

    /* compiled from: AbstractMessageLite.java */
    /* renamed from: d0.e0.p.d.m0.i.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static abstract class AbstractC0335a<BuilderType extends AbstractC0335a> implements n.a {
        @Override // d0.e0.p.d.m0.i.n.a
        public abstract BuilderType mergeFrom(d dVar, e eVar) throws IOException;

        /* compiled from: AbstractMessageLite.java */
        /* renamed from: d0.e0.p.d.m0.i.a$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0336a extends FilterInputStream {
            public int j;

            public C0336a(InputStream inputStream, int i) {
                super(inputStream);
                this.j = i;
            }

            @Override // java.io.FilterInputStream, java.io.InputStream
            public int available() throws IOException {
                return Math.min(super.available(), this.j);
            }

            @Override // java.io.FilterInputStream, java.io.InputStream
            public int read() throws IOException {
                if (this.j <= 0) {
                    return -1;
                }
                int read = super.read();
                if (read >= 0) {
                    this.j--;
                }
                return read;
            }

            @Override // java.io.FilterInputStream, java.io.InputStream
            public long skip(long j) throws IOException {
                long skip = super.skip(Math.min(j, this.j));
                if (skip >= 0) {
                    this.j = (int) (this.j - skip);
                }
                return skip;
            }

            @Override // java.io.FilterInputStream, java.io.InputStream
            public int read(byte[] bArr, int i, int i2) throws IOException {
                int i3 = this.j;
                if (i3 <= 0) {
                    return -1;
                }
                int read = super.read(bArr, i, Math.min(i2, i3));
                if (read >= 0) {
                    this.j -= read;
                }
                return read;
            }
        }
    }
}
