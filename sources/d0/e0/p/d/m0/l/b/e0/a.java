package d0.e0.p.d.m0.l.b.e0;

import d0.e0.p.d.m0.c.g1.c;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.m.j;
import d0.e0.p.d.m0.m.n;
import d0.e0.p.d.m0.m.o;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.y;
import java.util.Iterator;
import java.util.List;
import kotlin.jvm.functions.Function0;
import kotlin.reflect.KProperty;
/* compiled from: DeserializedAnnotations.kt */
/* loaded from: classes3.dex */
public class a implements g {
    public static final /* synthetic */ KProperty<Object>[] j = {a0.property1(new y(a0.getOrCreateKotlinClass(a.class), "annotations", "getAnnotations()Ljava/util/List;"))};
    public final j k;

    public a(o oVar, Function0<? extends List<? extends c>> function0) {
        m.checkNotNullParameter(oVar, "storageManager");
        m.checkNotNullParameter(function0, "compute");
        this.k = oVar.createLazyValue(function0);
    }

    @Override // d0.e0.p.d.m0.c.g1.g
    public c findAnnotation(b bVar) {
        return g.b.findAnnotation(this, bVar);
    }

    @Override // d0.e0.p.d.m0.c.g1.g
    public boolean hasAnnotation(b bVar) {
        return g.b.hasAnnotation(this, bVar);
    }

    @Override // d0.e0.p.d.m0.c.g1.g
    public boolean isEmpty() {
        return ((List) n.getValue(this.k, this, j[0])).isEmpty();
    }

    @Override // java.lang.Iterable
    public Iterator<c> iterator() {
        return ((List) n.getValue(this.k, this, j[0])).iterator();
    }
}
