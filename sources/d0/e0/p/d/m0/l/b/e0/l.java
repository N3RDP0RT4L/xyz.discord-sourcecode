package d0.e0.p.d.m0.l.b.e0;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.a1;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.i1.d;
import d0.e0.p.d.m0.c.i1.e;
import d0.e0.p.d.m0.c.i1.i0;
import d0.e0.p.d.m0.c.y0;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.f.r;
import d0.e0.p.d.m0.f.z.c;
import d0.e0.p.d.m0.f.z.i;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.l.b.e0.g;
import d0.e0.p.d.m0.m.o;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.c1;
import d0.e0.p.d.m0.n.e0;
import d0.e0.p.d.m0.n.e1;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.j1;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
/* compiled from: DeserializedMemberDescriptor.kt */
/* loaded from: classes3.dex */
public final class l extends e implements g {
    public j0 A;
    public g.a B = g.a.COMPATIBLE;
    public final o q;
    public final r r;

    /* renamed from: s  reason: collision with root package name */
    public final c f3467s;
    public final d0.e0.p.d.m0.f.z.g t;
    public final i u;
    public final f v;
    public Collection<? extends i0> w;

    /* renamed from: x  reason: collision with root package name */
    public j0 f3468x;

    /* renamed from: y  reason: collision with root package name */
    public j0 f3469y;

    /* renamed from: z  reason: collision with root package name */
    public List<? extends z0> f3470z;

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public l(d0.e0.p.d.m0.m.o r13, d0.e0.p.d.m0.c.m r14, d0.e0.p.d.m0.c.g1.g r15, d0.e0.p.d.m0.g.e r16, d0.e0.p.d.m0.c.u r17, d0.e0.p.d.m0.f.r r18, d0.e0.p.d.m0.f.z.c r19, d0.e0.p.d.m0.f.z.g r20, d0.e0.p.d.m0.f.z.i r21, d0.e0.p.d.m0.l.b.e0.f r22) {
        /*
            r12 = this;
            r6 = r12
            r7 = r13
            r8 = r18
            r9 = r19
            r10 = r20
            r11 = r21
            java.lang.String r0 = "storageManager"
            d0.z.d.m.checkNotNullParameter(r13, r0)
            java.lang.String r0 = "containingDeclaration"
            r1 = r14
            d0.z.d.m.checkNotNullParameter(r14, r0)
            java.lang.String r0 = "annotations"
            r2 = r15
            d0.z.d.m.checkNotNullParameter(r15, r0)
            java.lang.String r0 = "name"
            r3 = r16
            d0.z.d.m.checkNotNullParameter(r3, r0)
            java.lang.String r0 = "visibility"
            r5 = r17
            d0.z.d.m.checkNotNullParameter(r5, r0)
            java.lang.String r0 = "proto"
            d0.z.d.m.checkNotNullParameter(r8, r0)
            java.lang.String r0 = "nameResolver"
            d0.z.d.m.checkNotNullParameter(r9, r0)
            java.lang.String r0 = "typeTable"
            d0.z.d.m.checkNotNullParameter(r10, r0)
            java.lang.String r0 = "versionRequirementTable"
            d0.z.d.m.checkNotNullParameter(r11, r0)
            d0.e0.p.d.m0.c.u0 r4 = d0.e0.p.d.m0.c.u0.a
            java.lang.String r0 = "NO_SOURCE"
            d0.z.d.m.checkNotNullExpressionValue(r4, r0)
            r0 = r12
            r0.<init>(r1, r2, r3, r4, r5)
            r6.q = r7
            r6.r = r8
            r6.f3467s = r9
            r6.t = r10
            r6.u = r11
            r0 = r22
            r6.v = r0
            d0.e0.p.d.m0.l.b.e0.g$a r0 = d0.e0.p.d.m0.l.b.e0.g.a.COMPATIBLE
            r6.B = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.l.b.e0.l.<init>(d0.e0.p.d.m0.m.o, d0.e0.p.d.m0.c.m, d0.e0.p.d.m0.c.g1.g, d0.e0.p.d.m0.g.e, d0.e0.p.d.m0.c.u, d0.e0.p.d.m0.f.r, d0.e0.p.d.m0.f.z.c, d0.e0.p.d.m0.f.z.g, d0.e0.p.d.m0.f.z.i, d0.e0.p.d.m0.l.b.e0.f):void");
    }

    @Override // d0.e0.p.d.m0.c.y0
    public d0.e0.p.d.m0.c.e getClassDescriptor() {
        if (e0.isError(getExpandedType())) {
            return null;
        }
        h declarationDescriptor = getExpandedType().getConstructor().getDeclarationDescriptor();
        if (declarationDescriptor instanceof d0.e0.p.d.m0.c.e) {
            return (d0.e0.p.d.m0.c.e) declarationDescriptor;
        }
        return null;
    }

    @Override // d0.e0.p.d.m0.l.b.e0.g
    public f getContainerSource() {
        return this.v;
    }

    public g.a getCoroutinesExperimentalCompatibilityMode() {
        return this.B;
    }

    @Override // d0.e0.p.d.m0.c.h
    public j0 getDefaultType() {
        j0 j0Var = this.A;
        if (j0Var != null) {
            return j0Var;
        }
        m.throwUninitializedPropertyAccessException("defaultTypeImpl");
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.y0
    public j0 getExpandedType() {
        j0 j0Var = this.f3469y;
        if (j0Var != null) {
            return j0Var;
        }
        m.throwUninitializedPropertyAccessException("expandedType");
        throw null;
    }

    @Override // d0.e0.p.d.m0.l.b.e0.g
    public c getNameResolver() {
        return this.f3467s;
    }

    @Override // d0.e0.p.d.m0.l.b.e0.g
    public d0.e0.p.d.m0.f.z.g getTypeTable() {
        return this.t;
    }

    @Override // d0.e0.p.d.m0.c.y0
    public j0 getUnderlyingType() {
        j0 j0Var = this.f3468x;
        if (j0Var != null) {
            return j0Var;
        }
        m.throwUninitializedPropertyAccessException("underlyingType");
        throw null;
    }

    @Override // d0.e0.p.d.m0.l.b.e0.g
    public i getVersionRequirementTable() {
        return this.u;
    }

    @Override // d0.e0.p.d.m0.l.b.e0.g
    public List<d0.e0.p.d.m0.f.z.h> getVersionRequirements() {
        return g.b.getVersionRequirements(this);
    }

    public final void initialize(List<? extends z0> list, j0 j0Var, j0 j0Var2, g.a aVar) {
        m.checkNotNullParameter(list, "declaredTypeParameters");
        m.checkNotNullParameter(j0Var, "underlyingType");
        m.checkNotNullParameter(j0Var2, "expandedType");
        m.checkNotNullParameter(aVar, "isExperimentalCoroutineInReleaseEnvironment");
        initialize(list);
        this.f3468x = j0Var;
        this.f3469y = j0Var2;
        this.f3470z = a1.computeConstructorTypeParameters(this);
        d0.e0.p.d.m0.c.e classDescriptor = getClassDescriptor();
        d0.e0.p.d.m0.k.a0.i unsubstitutedMemberScope = classDescriptor == null ? null : classDescriptor.getUnsubstitutedMemberScope();
        if (unsubstitutedMemberScope == null) {
            unsubstitutedMemberScope = i.b.f3433b;
        }
        j0 makeUnsubstitutedType = e1.makeUnsubstitutedType(this, unsubstitutedMemberScope, new d(this));
        m.checkNotNullExpressionValue(makeUnsubstitutedType, "@OptIn(TypeRefinement::class)\n    protected fun computeDefaultType(): SimpleType =\n        TypeUtils.makeUnsubstitutedType(this, classDescriptor?.unsubstitutedMemberScope ?: MemberScope.Empty) { kotlinTypeRefiner ->\n            kotlinTypeRefiner.refineDescriptor(this)?.defaultType\n        }");
        this.A = makeUnsubstitutedType;
        this.w = getTypeAliasConstructors();
        this.B = aVar;
    }

    @Override // d0.e0.p.d.m0.l.b.e0.g
    public r getProto() {
        return this.r;
    }

    @Override // d0.e0.p.d.m0.c.w0
    public y0 substitute(c1 c1Var) {
        m.checkNotNullParameter(c1Var, "substitutor");
        if (c1Var.isEmpty()) {
            return this;
        }
        o oVar = this.q;
        d0.e0.p.d.m0.c.m containingDeclaration = getContainingDeclaration();
        m.checkNotNullExpressionValue(containingDeclaration, "containingDeclaration");
        d0.e0.p.d.m0.c.g1.g annotations = getAnnotations();
        m.checkNotNullExpressionValue(annotations, "annotations");
        d0.e0.p.d.m0.g.e name = getName();
        m.checkNotNullExpressionValue(name, ModelAuditLogEntry.CHANGE_KEY_NAME);
        l lVar = new l(oVar, containingDeclaration, annotations, name, getVisibility(), getProto(), getNameResolver(), getTypeTable(), getVersionRequirementTable(), getContainerSource());
        List<z0> declaredTypeParameters = getDeclaredTypeParameters();
        j0 underlyingType = getUnderlyingType();
        j1 j1Var = j1.INVARIANT;
        c0 safeSubstitute = c1Var.safeSubstitute(underlyingType, j1Var);
        m.checkNotNullExpressionValue(safeSubstitute, "substitutor.safeSubstitute(underlyingType, Variance.INVARIANT)");
        j0 asSimpleType = d0.e0.p.d.m0.n.a1.asSimpleType(safeSubstitute);
        c0 safeSubstitute2 = c1Var.safeSubstitute(getExpandedType(), j1Var);
        m.checkNotNullExpressionValue(safeSubstitute2, "substitutor.safeSubstitute(expandedType, Variance.INVARIANT)");
        lVar.initialize(declaredTypeParameters, asSimpleType, d0.e0.p.d.m0.n.a1.asSimpleType(safeSubstitute2), getCoroutinesExperimentalCompatibilityMode());
        return lVar;
    }
}
