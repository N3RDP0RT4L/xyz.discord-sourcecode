package d0.e0.p.d.m0.l.b.d0;

import andhook.lib.xposed.ClassUtils;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.e0.p.d.m0.g.b;
import d0.g0.t;
import d0.z.d.m;
/* compiled from: BuiltInSerializerProtocol.kt */
/* loaded from: classes3.dex */
public final class a extends d0.e0.p.d.m0.l.a {
    public static final a m = new a();

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public a() {
        /*
            r14 = this;
            d0.e0.p.d.m0.i.e r1 = d0.e0.p.d.m0.i.e.newInstance()
            d0.e0.p.d.m0.f.y.b.registerAllExtensions(r1)
            java.lang.String r0 = "newInstance().apply(BuiltInsProtoBuf::registerAllExtensions)"
            d0.z.d.m.checkNotNullExpressionValue(r1, r0)
            d0.e0.p.d.m0.i.g$f<d0.e0.p.d.m0.f.l, java.lang.Integer> r2 = d0.e0.p.d.m0.f.y.b.a
            java.lang.String r0 = "packageFqName"
            d0.z.d.m.checkNotNullExpressionValue(r2, r0)
            d0.e0.p.d.m0.i.g$f<d0.e0.p.d.m0.f.d, java.util.List<d0.e0.p.d.m0.f.b>> r3 = d0.e0.p.d.m0.f.y.b.c
            java.lang.String r0 = "constructorAnnotation"
            d0.z.d.m.checkNotNullExpressionValue(r3, r0)
            d0.e0.p.d.m0.i.g$f<d0.e0.p.d.m0.f.c, java.util.List<d0.e0.p.d.m0.f.b>> r4 = d0.e0.p.d.m0.f.y.b.f3382b
            java.lang.String r0 = "classAnnotation"
            d0.z.d.m.checkNotNullExpressionValue(r4, r0)
            d0.e0.p.d.m0.i.g$f<d0.e0.p.d.m0.f.i, java.util.List<d0.e0.p.d.m0.f.b>> r5 = d0.e0.p.d.m0.f.y.b.d
            java.lang.String r0 = "functionAnnotation"
            d0.z.d.m.checkNotNullExpressionValue(r5, r0)
            d0.e0.p.d.m0.i.g$f<d0.e0.p.d.m0.f.n, java.util.List<d0.e0.p.d.m0.f.b>> r6 = d0.e0.p.d.m0.f.y.b.e
            java.lang.String r0 = "propertyAnnotation"
            d0.z.d.m.checkNotNullExpressionValue(r6, r0)
            d0.e0.p.d.m0.i.g$f<d0.e0.p.d.m0.f.n, java.util.List<d0.e0.p.d.m0.f.b>> r7 = d0.e0.p.d.m0.f.y.b.f
            java.lang.String r0 = "propertyGetterAnnotation"
            d0.z.d.m.checkNotNullExpressionValue(r7, r0)
            d0.e0.p.d.m0.i.g$f<d0.e0.p.d.m0.f.n, java.util.List<d0.e0.p.d.m0.f.b>> r8 = d0.e0.p.d.m0.f.y.b.g
            java.lang.String r0 = "propertySetterAnnotation"
            d0.z.d.m.checkNotNullExpressionValue(r8, r0)
            d0.e0.p.d.m0.i.g$f<d0.e0.p.d.m0.f.g, java.util.List<d0.e0.p.d.m0.f.b>> r9 = d0.e0.p.d.m0.f.y.b.i
            java.lang.String r0 = "enumEntryAnnotation"
            d0.z.d.m.checkNotNullExpressionValue(r9, r0)
            d0.e0.p.d.m0.i.g$f<d0.e0.p.d.m0.f.n, d0.e0.p.d.m0.f.b$b$c> r10 = d0.e0.p.d.m0.f.y.b.h
            java.lang.String r0 = "compileTimeValue"
            d0.z.d.m.checkNotNullExpressionValue(r10, r0)
            d0.e0.p.d.m0.i.g$f<d0.e0.p.d.m0.f.u, java.util.List<d0.e0.p.d.m0.f.b>> r11 = d0.e0.p.d.m0.f.y.b.j
            java.lang.String r0 = "parameterAnnotation"
            d0.z.d.m.checkNotNullExpressionValue(r11, r0)
            d0.e0.p.d.m0.i.g$f<d0.e0.p.d.m0.f.q, java.util.List<d0.e0.p.d.m0.f.b>> r12 = d0.e0.p.d.m0.f.y.b.k
            java.lang.String r0 = "typeAnnotation"
            d0.z.d.m.checkNotNullExpressionValue(r12, r0)
            d0.e0.p.d.m0.i.g$f<d0.e0.p.d.m0.f.s, java.util.List<d0.e0.p.d.m0.f.b>> r13 = d0.e0.p.d.m0.f.y.b.l
            java.lang.String r0 = "typeParameterAnnotation"
            d0.z.d.m.checkNotNullExpressionValue(r13, r0)
            r0 = r14
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.l.b.d0.a.<init>():void");
    }

    public final String getBuiltInsFileName(b bVar) {
        String str;
        m.checkNotNullParameter(bVar, "fqName");
        if (bVar.isRoot()) {
            str = "default-package";
        } else {
            str = bVar.shortName().asString();
            m.checkNotNullExpressionValue(str, "fqName.shortName().asString()");
        }
        return m.stringPlus(str, ".kotlin_builtins");
    }

    public final String getBuiltInsFilePath(b bVar) {
        m.checkNotNullParameter(bVar, "fqName");
        StringBuilder sb = new StringBuilder();
        String asString = bVar.asString();
        m.checkNotNullExpressionValue(asString, "fqName.asString()");
        sb.append(t.replace$default(asString, (char) ClassUtils.PACKAGE_SEPARATOR_CHAR, (char) MentionUtilsKt.SLASH_CHAR, false, 4, (Object) null));
        sb.append(MentionUtilsKt.SLASH_CHAR);
        sb.append(getBuiltInsFileName(bVar));
        return sb.toString();
    }
}
