package d0.e0.p.d.m0.l.b;

import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.d0;
import d0.e0.p.d.m0.c.g1.c;
import d0.e0.p.d.m0.f.b;
import d0.e0.p.d.m0.f.i;
import d0.e0.p.d.m0.f.q;
import d0.e0.p.d.m0.f.s;
import d0.e0.p.d.m0.f.u;
import d0.e0.p.d.m0.f.z.e;
import d0.e0.p.d.m0.i.n;
import d0.e0.p.d.m0.k.v.g;
import d0.e0.p.d.m0.l.a;
import d0.e0.p.d.m0.l.b.y;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
/* compiled from: AnnotationAndConstantLoaderImpl.kt */
/* loaded from: classes3.dex */
public final class d implements c<c, g<?>> {
    public final a a;

    /* renamed from: b  reason: collision with root package name */
    public final e f3456b;

    public d(c0 c0Var, d0 d0Var, a aVar) {
        m.checkNotNullParameter(c0Var, "module");
        m.checkNotNullParameter(d0Var, "notFoundClasses");
        m.checkNotNullParameter(aVar, "protocol");
        this.a = aVar;
        this.f3456b = new e(c0Var, d0Var);
    }

    @Override // d0.e0.p.d.m0.l.b.c
    public List<c> loadCallableAnnotations(y yVar, n nVar, b bVar) {
        List<b> list;
        m.checkNotNullParameter(yVar, "container");
        m.checkNotNullParameter(nVar, "proto");
        m.checkNotNullParameter(bVar, "kind");
        if (nVar instanceof d0.e0.p.d.m0.f.d) {
            list = (List) ((d0.e0.p.d.m0.f.d) nVar).getExtension(this.a.getConstructorAnnotation());
        } else if (nVar instanceof i) {
            list = (List) ((i) nVar).getExtension(this.a.getFunctionAnnotation());
        } else if (nVar instanceof d0.e0.p.d.m0.f.n) {
            int ordinal = bVar.ordinal();
            if (ordinal == 1) {
                list = (List) ((d0.e0.p.d.m0.f.n) nVar).getExtension(this.a.getPropertyAnnotation());
            } else if (ordinal == 2) {
                list = (List) ((d0.e0.p.d.m0.f.n) nVar).getExtension(this.a.getPropertyGetterAnnotation());
            } else if (ordinal == 3) {
                list = (List) ((d0.e0.p.d.m0.f.n) nVar).getExtension(this.a.getPropertySetterAnnotation());
            } else {
                throw new IllegalStateException("Unsupported callable kind with property proto".toString());
            }
        } else {
            throw new IllegalStateException(m.stringPlus("Unknown message: ", nVar).toString());
        }
        if (list == null) {
            list = d0.t.n.emptyList();
        }
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
        for (b bVar2 : list) {
            arrayList.add(this.f3456b.deserializeAnnotation(bVar2, yVar.getNameResolver()));
        }
        return arrayList;
    }

    @Override // d0.e0.p.d.m0.l.b.c
    public List<c> loadClassAnnotations(y.a aVar) {
        m.checkNotNullParameter(aVar, "container");
        List<b> list = (List) aVar.getClassProto().getExtension(this.a.getClassAnnotation());
        if (list == null) {
            list = d0.t.n.emptyList();
        }
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
        for (b bVar : list) {
            arrayList.add(this.f3456b.deserializeAnnotation(bVar, aVar.getNameResolver()));
        }
        return arrayList;
    }

    @Override // d0.e0.p.d.m0.l.b.c
    public List<c> loadEnumEntryAnnotations(y yVar, d0.e0.p.d.m0.f.g gVar) {
        m.checkNotNullParameter(yVar, "container");
        m.checkNotNullParameter(gVar, "proto");
        List<b> list = (List) gVar.getExtension(this.a.getEnumEntryAnnotation());
        if (list == null) {
            list = d0.t.n.emptyList();
        }
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
        for (b bVar : list) {
            arrayList.add(this.f3456b.deserializeAnnotation(bVar, yVar.getNameResolver()));
        }
        return arrayList;
    }

    @Override // d0.e0.p.d.m0.l.b.c
    public List<c> loadExtensionReceiverParameterAnnotations(y yVar, n nVar, b bVar) {
        m.checkNotNullParameter(yVar, "container");
        m.checkNotNullParameter(nVar, "proto");
        m.checkNotNullParameter(bVar, "kind");
        return d0.t.n.emptyList();
    }

    @Override // d0.e0.p.d.m0.l.b.c
    public List<c> loadPropertyBackingFieldAnnotations(y yVar, d0.e0.p.d.m0.f.n nVar) {
        m.checkNotNullParameter(yVar, "container");
        m.checkNotNullParameter(nVar, "proto");
        return d0.t.n.emptyList();
    }

    @Override // d0.e0.p.d.m0.l.b.c
    public List<c> loadPropertyDelegateFieldAnnotations(y yVar, d0.e0.p.d.m0.f.n nVar) {
        m.checkNotNullParameter(yVar, "container");
        m.checkNotNullParameter(nVar, "proto");
        return d0.t.n.emptyList();
    }

    @Override // d0.e0.p.d.m0.l.b.c
    public List<c> loadTypeAnnotations(q qVar, d0.e0.p.d.m0.f.z.c cVar) {
        m.checkNotNullParameter(qVar, "proto");
        m.checkNotNullParameter(cVar, "nameResolver");
        List<b> list = (List) qVar.getExtension(this.a.getTypeAnnotation());
        if (list == null) {
            list = d0.t.n.emptyList();
        }
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
        for (b bVar : list) {
            arrayList.add(this.f3456b.deserializeAnnotation(bVar, cVar));
        }
        return arrayList;
    }

    @Override // d0.e0.p.d.m0.l.b.c
    public List<c> loadTypeParameterAnnotations(s sVar, d0.e0.p.d.m0.f.z.c cVar) {
        m.checkNotNullParameter(sVar, "proto");
        m.checkNotNullParameter(cVar, "nameResolver");
        List<b> list = (List) sVar.getExtension(this.a.getTypeParameterAnnotation());
        if (list == null) {
            list = d0.t.n.emptyList();
        }
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
        for (b bVar : list) {
            arrayList.add(this.f3456b.deserializeAnnotation(bVar, cVar));
        }
        return arrayList;
    }

    @Override // d0.e0.p.d.m0.l.b.c
    public List<c> loadValueParameterAnnotations(y yVar, n nVar, b bVar, int i, u uVar) {
        m.checkNotNullParameter(yVar, "container");
        m.checkNotNullParameter(nVar, "callableProto");
        m.checkNotNullParameter(bVar, "kind");
        m.checkNotNullParameter(uVar, "proto");
        List<b> list = (List) uVar.getExtension(this.a.getParameterAnnotation());
        if (list == null) {
            list = d0.t.n.emptyList();
        }
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
        for (b bVar2 : list) {
            arrayList.add(this.f3456b.deserializeAnnotation(bVar2, yVar.getNameResolver()));
        }
        return arrayList;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // d0.e0.p.d.m0.l.b.c
    public g<?> loadPropertyConstant(y yVar, d0.e0.p.d.m0.f.n nVar, d0.e0.p.d.m0.n.c0 c0Var) {
        m.checkNotNullParameter(yVar, "container");
        m.checkNotNullParameter(nVar, "proto");
        m.checkNotNullParameter(c0Var, "expectedType");
        b.C0325b.c cVar = (b.C0325b.c) e.getExtensionOrNull(nVar, this.a.getCompileTimeValue());
        if (cVar == null) {
            return null;
        }
        return this.f3456b.resolveValue(c0Var, cVar, yVar.getNameResolver());
    }
}
