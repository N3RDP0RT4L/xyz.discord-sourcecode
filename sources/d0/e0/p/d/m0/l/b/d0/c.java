package d0.e0.p.d.m0.l.b.d0;

import d0.e0.p.d.m0.b.b;
import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.l.b.o;
import d0.z.d.m;
import java.io.InputStream;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: BuiltInsPackageFragmentImpl.kt */
/* loaded from: classes3.dex */
public final class c extends o implements b {
    public static final a v = new a(null);

    /* compiled from: BuiltInsPackageFragmentImpl.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        /* JADX WARN: Finally extract failed */
        public final c create(d0.e0.p.d.m0.g.b bVar, d0.e0.p.d.m0.m.o oVar, c0 c0Var, InputStream inputStream, boolean z2) {
            m.checkNotNullParameter(bVar, "fqName");
            m.checkNotNullParameter(oVar, "storageManager");
            m.checkNotNullParameter(c0Var, "module");
            m.checkNotNullParameter(inputStream, "inputStream");
            try {
                d0.e0.p.d.m0.f.y.a readFrom = d0.e0.p.d.m0.f.y.a.f.readFrom(inputStream);
                if (readFrom == null) {
                    m.throwUninitializedPropertyAccessException("version");
                    throw null;
                } else if (readFrom.isCompatible()) {
                    d0.e0.p.d.m0.f.m parseFrom = d0.e0.p.d.m0.f.m.parseFrom(inputStream, d0.e0.p.d.m0.l.b.d0.a.m.getExtensionRegistry());
                    d0.y.b.closeFinally(inputStream, null);
                    m.checkNotNullExpressionValue(parseFrom, "proto");
                    return new c(bVar, oVar, c0Var, parseFrom, readFrom, z2, null);
                } else {
                    throw new UnsupportedOperationException("Kotlin built-in definition format version is not supported: expected " + d0.e0.p.d.m0.f.y.a.g + ", actual " + readFrom + ". Please update Kotlin");
                }
            } finally {
                try {
                    throw th;
                } catch (Throwable th) {
                }
            }
        }
    }

    public c(d0.e0.p.d.m0.g.b bVar, d0.e0.p.d.m0.m.o oVar, c0 c0Var, d0.e0.p.d.m0.f.m mVar, d0.e0.p.d.m0.f.y.a aVar, boolean z2, DefaultConstructorMarker defaultConstructorMarker) {
        super(bVar, oVar, c0Var, mVar, aVar, null);
    }
}
