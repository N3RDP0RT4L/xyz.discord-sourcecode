package d0.e0.p.d.m0.l.b;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.i1.a0;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.l.b.e0.h;
import d0.e0.p.d.m0.m.o;
import d0.z.d.m;
/* compiled from: DeserializedPackageFragment.kt */
/* loaded from: classes3.dex */
public abstract class n extends a0 {
    public final o o;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public n(b bVar, o oVar, c0 c0Var) {
        super(c0Var, bVar);
        m.checkNotNullParameter(bVar, "fqName");
        m.checkNotNullParameter(oVar, "storageManager");
        m.checkNotNullParameter(c0Var, "module");
        this.o = oVar;
    }

    public abstract g getClassDataFinder();

    public boolean hasTopLevelClass(e eVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        i memberScope = getMemberScope();
        return (memberScope instanceof h) && ((h) memberScope).getClassNames$deserialization().contains(eVar);
    }
}
