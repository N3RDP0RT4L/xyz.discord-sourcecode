package d0.e0.p.d.m0.l.b;

import d0.e0.p.d.m0.c.a;
import d0.e0.p.d.m0.c.x;
import d0.e0.p.d.m0.f.z.g;
import d0.z.d.m;
import kotlin.Pair;
/* compiled from: ContractDeserializer.kt */
/* loaded from: classes3.dex */
public interface i {
    public static final a a = a.a;

    /* compiled from: ContractDeserializer.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public static final /* synthetic */ a a = new a();

        /* renamed from: b  reason: collision with root package name */
        public static final i f3474b = new C0356a();

        /* compiled from: ContractDeserializer.kt */
        /* renamed from: d0.e0.p.d.m0.l.b.i$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0356a implements i {
            @Override // d0.e0.p.d.m0.l.b.i
            public Pair deserializeContractFromFunction(d0.e0.p.d.m0.f.i iVar, x xVar, g gVar, c0 c0Var) {
                m.checkNotNullParameter(iVar, "proto");
                m.checkNotNullParameter(xVar, "ownerFunction");
                m.checkNotNullParameter(gVar, "typeTable");
                m.checkNotNullParameter(c0Var, "typeDeserializer");
                return null;
            }
        }

        public final i getDEFAULT() {
            return f3474b;
        }
    }

    Pair<a.AbstractC0290a<?>, Object> deserializeContractFromFunction(d0.e0.p.d.m0.f.i iVar, x xVar, g gVar, c0 c0Var);
}
