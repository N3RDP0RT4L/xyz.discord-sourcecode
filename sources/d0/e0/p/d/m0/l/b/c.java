package d0.e0.p.d.m0.l.b;

import d0.e0.p.d.m0.f.g;
import d0.e0.p.d.m0.f.q;
import d0.e0.p.d.m0.f.s;
import d0.e0.p.d.m0.f.u;
import d0.e0.p.d.m0.i.n;
import d0.e0.p.d.m0.l.b.y;
import d0.e0.p.d.m0.n.c0;
import java.util.List;
/* compiled from: AnnotationAndConstantLoader.kt */
/* loaded from: classes3.dex */
public interface c<A, C> {
    List<A> loadCallableAnnotations(y yVar, n nVar, b bVar);

    List<A> loadClassAnnotations(y.a aVar);

    List<A> loadEnumEntryAnnotations(y yVar, g gVar);

    List<A> loadExtensionReceiverParameterAnnotations(y yVar, n nVar, b bVar);

    List<A> loadPropertyBackingFieldAnnotations(y yVar, d0.e0.p.d.m0.f.n nVar);

    C loadPropertyConstant(y yVar, d0.e0.p.d.m0.f.n nVar, c0 c0Var);

    List<A> loadPropertyDelegateFieldAnnotations(y yVar, d0.e0.p.d.m0.f.n nVar);

    List<A> loadTypeAnnotations(q qVar, d0.e0.p.d.m0.f.z.c cVar);

    List<A> loadTypeParameterAnnotations(s sVar, d0.e0.p.d.m0.f.z.c cVar);

    List<A> loadValueParameterAnnotations(y yVar, n nVar, b bVar, int i, u uVar);
}
