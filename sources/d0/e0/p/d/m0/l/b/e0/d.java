package d0.e0.p.d.m0.l.b.e0;

import andhook.lib.HookHelper;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.a1;
import d0.e0.p.d.m0.c.d0;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.r0;
import d0.e0.p.d.m0.c.t0;
import d0.e0.p.d.m0.c.u;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.c.x0;
import d0.e0.p.d.m0.c.z;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.f.c;
import d0.e0.p.d.m0.f.q;
import d0.e0.p.d.m0.f.s;
import d0.e0.p.d.m0.f.t;
import d0.e0.p.d.m0.f.z.i;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.k.a0.l;
import d0.e0.p.d.m0.l.b.l;
import d0.e0.p.d.m0.l.b.p;
import d0.e0.p.d.m0.l.b.w;
import d0.e0.p.d.m0.l.b.y;
import d0.e0.p.d.m0.m.j;
import d0.e0.p.d.m0.m.k;
import d0.e0.p.d.m0.n.c0;
import d0.t.g0;
import d0.t.n;
import d0.t.o0;
import d0.t.r;
import d0.z.d.a0;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.reflect.KDeclarationContainer;
/* compiled from: DeserializedClassDescriptor.kt */
/* loaded from: classes3.dex */
public final class d extends d0.e0.p.d.m0.c.i1.a implements m {
    public final m A;
    public final k<d0.e0.p.d.m0.c.d> B;
    public final j<Collection<d0.e0.p.d.m0.c.d>> C;
    public final k<d0.e0.p.d.m0.c.e> D;
    public final j<Collection<d0.e0.p.d.m0.c.e>> E;
    public final y.a F;
    public final d0.e0.p.d.m0.c.g1.g G;
    public final d0.e0.p.d.m0.f.c o;
    public final d0.e0.p.d.m0.f.z.a p;
    public final u0 q;
    public final d0.e0.p.d.m0.g.a r;

    /* renamed from: s  reason: collision with root package name */
    public final z f3459s;
    public final u t;
    public final d0.e0.p.d.m0.c.f u;
    public final l v;
    public final d0.e0.p.d.m0.k.a0.j w;

    /* renamed from: x  reason: collision with root package name */
    public final b f3460x;

    /* renamed from: y  reason: collision with root package name */
    public final r0<a> f3461y;

    /* renamed from: z  reason: collision with root package name */
    public final c f3462z;

    /* compiled from: DeserializedClassDescriptor.kt */
    /* loaded from: classes3.dex */
    public final class a extends d0.e0.p.d.m0.l.b.e0.h {
        public final d0.e0.p.d.m0.n.l1.g g;
        public final j<Collection<m>> h;
        public final j<Collection<c0>> i;
        public final /* synthetic */ d j;

        /* compiled from: DeserializedClassDescriptor.kt */
        /* renamed from: d0.e0.p.d.m0.l.b.e0.d$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0349a extends o implements Function0<List<? extends d0.e0.p.d.m0.g.e>> {
            public final /* synthetic */ List<d0.e0.p.d.m0.g.e> $it;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C0349a(List<d0.e0.p.d.m0.g.e> list) {
                super(0);
                this.$it = list;
            }

            @Override // kotlin.jvm.functions.Function0
            public final List<? extends d0.e0.p.d.m0.g.e> invoke() {
                return this.$it;
            }
        }

        /* compiled from: DeserializedClassDescriptor.kt */
        /* loaded from: classes3.dex */
        public static final class b extends o implements Function0<Collection<? extends m>> {
            public b() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            public final Collection<? extends m> invoke() {
                return a.this.b(d0.e0.p.d.m0.k.a0.d.m, d0.e0.p.d.m0.k.a0.i.a.getALL_NAME_FILTER(), d0.e0.p.d.m0.d.b.d.WHEN_GET_ALL_DESCRIPTORS);
            }
        }

        /* compiled from: DeserializedClassDescriptor.kt */
        /* loaded from: classes3.dex */
        public static final class c extends d0.e0.p.d.m0.k.i {
            public final /* synthetic */ List<D> a;

            public c(List<D> list) {
                this.a = list;
            }

            @Override // d0.e0.p.d.m0.k.j
            public void addFakeOverride(d0.e0.p.d.m0.c.b bVar) {
                d0.z.d.m.checkNotNullParameter(bVar, "fakeOverride");
                d0.e0.p.d.m0.k.k.resolveUnknownVisibilityForMember(bVar, null);
                this.a.add(bVar);
            }

            @Override // d0.e0.p.d.m0.k.i
            public void conflict(d0.e0.p.d.m0.c.b bVar, d0.e0.p.d.m0.c.b bVar2) {
                d0.z.d.m.checkNotNullParameter(bVar, "fromSuper");
                d0.z.d.m.checkNotNullParameter(bVar2, "fromCurrent");
            }
        }

        /* compiled from: DeserializedClassDescriptor.kt */
        /* renamed from: d0.e0.p.d.m0.l.b.e0.d$a$d  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0350d extends o implements Function0<Collection<? extends c0>> {
            public C0350d() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            public final Collection<? extends c0> invoke() {
                return a.this.g.refineSupertypes(a.this.j);
            }
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public a(d0.e0.p.d.m0.l.b.e0.d r8, d0.e0.p.d.m0.n.l1.g r9) {
            /*
                r7 = this;
                java.lang.String r0 = "this$0"
                d0.z.d.m.checkNotNullParameter(r8, r0)
                java.lang.String r0 = "kotlinTypeRefiner"
                d0.z.d.m.checkNotNullParameter(r9, r0)
                r7.j = r8
                d0.e0.p.d.m0.l.b.l r2 = r8.getC()
                d0.e0.p.d.m0.f.c r0 = r8.getClassProto()
                java.util.List r3 = r0.getFunctionList()
                java.lang.String r0 = "classProto.functionList"
                d0.z.d.m.checkNotNullExpressionValue(r3, r0)
                d0.e0.p.d.m0.f.c r0 = r8.getClassProto()
                java.util.List r4 = r0.getPropertyList()
                java.lang.String r0 = "classProto.propertyList"
                d0.z.d.m.checkNotNullExpressionValue(r4, r0)
                d0.e0.p.d.m0.f.c r0 = r8.getClassProto()
                java.util.List r5 = r0.getTypeAliasList()
                java.lang.String r0 = "classProto.typeAliasList"
                d0.z.d.m.checkNotNullExpressionValue(r5, r0)
                d0.e0.p.d.m0.f.c r0 = r8.getClassProto()
                java.util.List r0 = r0.getNestedClassNameList()
                java.lang.String r1 = "classProto.nestedClassNameList"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                d0.e0.p.d.m0.l.b.l r8 = r8.getC()
                d0.e0.p.d.m0.f.z.c r8 = r8.getNameResolver()
                java.util.ArrayList r1 = new java.util.ArrayList
                r6 = 10
                int r6 = d0.t.o.collectionSizeOrDefault(r0, r6)
                r1.<init>(r6)
                java.util.Iterator r0 = r0.iterator()
            L5b:
                boolean r6 = r0.hasNext()
                if (r6 == 0) goto L73
                java.lang.Object r6 = r0.next()
                java.lang.Number r6 = (java.lang.Number) r6
                int r6 = r6.intValue()
                d0.e0.p.d.m0.g.e r6 = d0.e0.p.d.m0.l.b.w.getName(r8, r6)
                r1.add(r6)
                goto L5b
            L73:
                d0.e0.p.d.m0.l.b.e0.d$a$a r6 = new d0.e0.p.d.m0.l.b.e0.d$a$a
                r6.<init>(r1)
                r1 = r7
                r1.<init>(r2, r3, r4, r5, r6)
                r7.g = r9
                d0.e0.p.d.m0.l.b.l r8 = r7.c
                d0.e0.p.d.m0.m.o r8 = r8.getStorageManager()
                d0.e0.p.d.m0.l.b.e0.d$a$b r9 = new d0.e0.p.d.m0.l.b.e0.d$a$b
                r9.<init>()
                d0.e0.p.d.m0.m.j r8 = r8.createLazyValue(r9)
                r7.h = r8
                d0.e0.p.d.m0.l.b.l r8 = r7.c
                d0.e0.p.d.m0.m.o r8 = r8.getStorageManager()
                d0.e0.p.d.m0.l.b.e0.d$a$d r9 = new d0.e0.p.d.m0.l.b.e0.d$a$d
                r9.<init>()
                d0.e0.p.d.m0.m.j r8 = r8.createLazyValue(r9)
                r7.i = r8
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.l.b.e0.d.a.<init>(d0.e0.p.d.m0.l.b.e0.d, d0.e0.p.d.m0.n.l1.g):void");
        }

        @Override // d0.e0.p.d.m0.l.b.e0.h
        public void a(Collection<m> collection, Function1<? super d0.e0.p.d.m0.g.e, Boolean> function1) {
            d0.z.d.m.checkNotNullParameter(collection, "result");
            d0.z.d.m.checkNotNullParameter(function1, "nameFilter");
            c cVar = this.j.f3462z;
            Collection<d0.e0.p.d.m0.c.e> all = cVar == null ? null : cVar.all();
            if (all == null) {
                all = n.emptyList();
            }
            collection.addAll(all);
        }

        @Override // d0.e0.p.d.m0.l.b.e0.h
        public void c(d0.e0.p.d.m0.g.e eVar, List<t0> list) {
            d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
            d0.z.d.m.checkNotNullParameter(list, "functions");
            ArrayList arrayList = new ArrayList();
            for (c0 c0Var : this.i.invoke()) {
                arrayList.addAll(c0Var.getMemberScope().getContributedFunctions(eVar, d0.e0.p.d.m0.d.b.d.FOR_ALREADY_TRACKED));
            }
            list.addAll(this.c.getComponents().getAdditionalClassPartsProvider().getFunctions(eVar, this.j));
            k(eVar, arrayList, list);
        }

        @Override // d0.e0.p.d.m0.l.b.e0.h
        public void d(d0.e0.p.d.m0.g.e eVar, List<n0> list) {
            d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
            d0.z.d.m.checkNotNullParameter(list, "descriptors");
            ArrayList arrayList = new ArrayList();
            for (c0 c0Var : this.i.invoke()) {
                arrayList.addAll(c0Var.getMemberScope().getContributedVariables(eVar, d0.e0.p.d.m0.d.b.d.FOR_ALREADY_TRACKED));
            }
            k(eVar, arrayList, list);
        }

        @Override // d0.e0.p.d.m0.l.b.e0.h
        public d0.e0.p.d.m0.g.a e(d0.e0.p.d.m0.g.e eVar) {
            d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
            d0.e0.p.d.m0.g.a createNestedClassId = this.j.r.createNestedClassId(eVar);
            d0.z.d.m.checkNotNullExpressionValue(createNestedClassId, "classId.createNestedClassId(name)");
            return createNestedClassId;
        }

        @Override // d0.e0.p.d.m0.l.b.e0.h
        public Set<d0.e0.p.d.m0.g.e> f() {
            List<c0> supertypes = this.j.f3460x.getSupertypes();
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            for (c0 c0Var : supertypes) {
                Set<d0.e0.p.d.m0.g.e> classifierNames = c0Var.getMemberScope().getClassifierNames();
                if (classifierNames == null) {
                    return null;
                }
                r.addAll(linkedHashSet, classifierNames);
            }
            return linkedHashSet;
        }

        @Override // d0.e0.p.d.m0.l.b.e0.h
        public Set<d0.e0.p.d.m0.g.e> g() {
            List<c0> supertypes = this.j.f3460x.getSupertypes();
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            for (c0 c0Var : supertypes) {
                r.addAll(linkedHashSet, c0Var.getMemberScope().getFunctionNames());
            }
            linkedHashSet.addAll(this.c.getComponents().getAdditionalClassPartsProvider().getFunctionsNames(this.j));
            return linkedHashSet;
        }

        @Override // d0.e0.p.d.m0.l.b.e0.h, d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.l
        public d0.e0.p.d.m0.c.h getContributedClassifier(d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.d.b.b bVar) {
            d0.e0.p.d.m0.c.e findEnumEntry;
            d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
            d0.z.d.m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
            recordLookup(eVar, bVar);
            c cVar = this.j.f3462z;
            return (cVar == null || (findEnumEntry = cVar.findEnumEntry(eVar)) == null) ? super.getContributedClassifier(eVar, bVar) : findEnumEntry;
        }

        @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.l
        public Collection<m> getContributedDescriptors(d0.e0.p.d.m0.k.a0.d dVar, Function1<? super d0.e0.p.d.m0.g.e, Boolean> function1) {
            d0.z.d.m.checkNotNullParameter(dVar, "kindFilter");
            d0.z.d.m.checkNotNullParameter(function1, "nameFilter");
            return this.h.invoke();
        }

        @Override // d0.e0.p.d.m0.l.b.e0.h, d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.i
        public Collection<t0> getContributedFunctions(d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.d.b.b bVar) {
            d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
            d0.z.d.m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
            recordLookup(eVar, bVar);
            return super.getContributedFunctions(eVar, bVar);
        }

        @Override // d0.e0.p.d.m0.l.b.e0.h, d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.i
        public Collection<n0> getContributedVariables(d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.d.b.b bVar) {
            d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
            d0.z.d.m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
            recordLookup(eVar, bVar);
            return super.getContributedVariables(eVar, bVar);
        }

        @Override // d0.e0.p.d.m0.l.b.e0.h
        public Set<d0.e0.p.d.m0.g.e> h() {
            List<c0> supertypes = this.j.f3460x.getSupertypes();
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            for (c0 c0Var : supertypes) {
                r.addAll(linkedHashSet, c0Var.getMemberScope().getVariableNames());
            }
            return linkedHashSet;
        }

        @Override // d0.e0.p.d.m0.l.b.e0.h
        public boolean j(t0 t0Var) {
            d0.z.d.m.checkNotNullParameter(t0Var, "function");
            return this.c.getComponents().getPlatformDependentDeclarationFilter().isFunctionAvailable(this.j, t0Var);
        }

        public final <D extends d0.e0.p.d.m0.c.b> void k(d0.e0.p.d.m0.g.e eVar, Collection<? extends D> collection, List<D> list) {
            this.c.getComponents().getKotlinTypeChecker().getOverridingUtil().generateOverridesInFunctionGroup(eVar, collection, new ArrayList(list), this.j, new c(list));
        }

        public void recordLookup(d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.d.b.b bVar) {
            d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
            d0.z.d.m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
            d0.e0.p.d.m0.d.a.record(this.c.getComponents().getLookupTracker(), bVar, this.j, eVar);
        }
    }

    /* compiled from: DeserializedClassDescriptor.kt */
    /* loaded from: classes3.dex */
    public final class c {
        public final Map<d0.e0.p.d.m0.g.e, d0.e0.p.d.m0.f.g> a;

        /* renamed from: b  reason: collision with root package name */
        public final d0.e0.p.d.m0.m.i<d0.e0.p.d.m0.g.e, d0.e0.p.d.m0.c.e> f3463b;
        public final j<Set<d0.e0.p.d.m0.g.e>> c;
        public final /* synthetic */ d d;

        /* compiled from: DeserializedClassDescriptor.kt */
        /* loaded from: classes3.dex */
        public static final class a extends o implements Function1<d0.e0.p.d.m0.g.e, d0.e0.p.d.m0.c.e> {
            public final /* synthetic */ d this$1;

            /* compiled from: DeserializedClassDescriptor.kt */
            /* renamed from: d0.e0.p.d.m0.l.b.e0.d$c$a$a  reason: collision with other inner class name */
            /* loaded from: classes3.dex */
            public static final class C0351a extends o implements Function0<List<? extends d0.e0.p.d.m0.c.g1.c>> {
                public final /* synthetic */ d0.e0.p.d.m0.f.g $proto;
                public final /* synthetic */ d this$0;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public C0351a(d dVar, d0.e0.p.d.m0.f.g gVar) {
                    super(0);
                    this.this$0 = dVar;
                    this.$proto = gVar;
                }

                @Override // kotlin.jvm.functions.Function0
                public final List<? extends d0.e0.p.d.m0.c.g1.c> invoke() {
                    return d0.t.u.toList(this.this$0.getC().getComponents().getAnnotationAndConstantLoader().loadEnumEntryAnnotations(this.this$0.getThisAsProtoContainer$deserialization(), this.$proto));
                }
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public a(d dVar) {
                super(1);
                this.this$1 = dVar;
            }

            public final d0.e0.p.d.m0.c.e invoke(d0.e0.p.d.m0.g.e eVar) {
                d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
                d0.e0.p.d.m0.f.g gVar = (d0.e0.p.d.m0.f.g) c.this.a.get(eVar);
                if (gVar == null) {
                    return null;
                }
                d dVar = this.this$1;
                return d0.e0.p.d.m0.c.i1.o.create(dVar.getC().getStorageManager(), dVar, eVar, c.this.c, new d0.e0.p.d.m0.l.b.e0.a(dVar.getC().getStorageManager(), new C0351a(dVar, gVar)), u0.a);
            }
        }

        /* compiled from: DeserializedClassDescriptor.kt */
        /* loaded from: classes3.dex */
        public static final class b extends o implements Function0<Set<? extends d0.e0.p.d.m0.g.e>> {
            public b() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            public final Set<? extends d0.e0.p.d.m0.g.e> invoke() {
                return c.access$computeEnumMemberNames(c.this);
            }
        }

        public c(d dVar) {
            d0.z.d.m.checkNotNullParameter(dVar, "this$0");
            this.d = dVar;
            List<d0.e0.p.d.m0.f.g> enumEntryList = dVar.getClassProto().getEnumEntryList();
            d0.z.d.m.checkNotNullExpressionValue(enumEntryList, "classProto.enumEntryList");
            LinkedHashMap linkedHashMap = new LinkedHashMap(d0.d0.f.coerceAtLeast(g0.mapCapacity(d0.t.o.collectionSizeOrDefault(enumEntryList, 10)), 16));
            for (Object obj : enumEntryList) {
                linkedHashMap.put(w.getName(dVar.getC().getNameResolver(), ((d0.e0.p.d.m0.f.g) obj).getName()), obj);
            }
            this.a = linkedHashMap;
            this.f3463b = this.d.getC().getStorageManager().createMemoizedFunctionWithNullableValues(new a(this.d));
            this.c = this.d.getC().getStorageManager().createLazyValue(new b());
        }

        public static final Set access$computeEnumMemberNames(c cVar) {
            Objects.requireNonNull(cVar);
            HashSet hashSet = new HashSet();
            for (c0 c0Var : cVar.d.getTypeConstructor().getSupertypes()) {
                for (m mVar : l.a.getContributedDescriptors$default(c0Var.getMemberScope(), null, null, 3, null)) {
                    if ((mVar instanceof t0) || (mVar instanceof n0)) {
                        hashSet.add(mVar.getName());
                    }
                }
            }
            List<d0.e0.p.d.m0.f.i> functionList = cVar.d.getClassProto().getFunctionList();
            d0.z.d.m.checkNotNullExpressionValue(functionList, "classProto.functionList");
            d dVar = cVar.d;
            for (d0.e0.p.d.m0.f.i iVar : functionList) {
                hashSet.add(w.getName(dVar.getC().getNameResolver(), iVar.getName()));
            }
            List<d0.e0.p.d.m0.f.n> propertyList = cVar.d.getClassProto().getPropertyList();
            d0.z.d.m.checkNotNullExpressionValue(propertyList, "classProto.propertyList");
            d dVar2 = cVar.d;
            for (d0.e0.p.d.m0.f.n nVar : propertyList) {
                hashSet.add(w.getName(dVar2.getC().getNameResolver(), nVar.getName()));
            }
            return o0.plus((Set) hashSet, (Iterable) hashSet);
        }

        public final Collection<d0.e0.p.d.m0.c.e> all() {
            Set<d0.e0.p.d.m0.g.e> keySet = this.a.keySet();
            ArrayList arrayList = new ArrayList();
            for (d0.e0.p.d.m0.g.e eVar : keySet) {
                d0.e0.p.d.m0.c.e findEnumEntry = findEnumEntry(eVar);
                if (findEnumEntry != null) {
                    arrayList.add(findEnumEntry);
                }
            }
            return arrayList;
        }

        public final d0.e0.p.d.m0.c.e findEnumEntry(d0.e0.p.d.m0.g.e eVar) {
            d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
            return this.f3463b.invoke(eVar);
        }
    }

    /* compiled from: DeserializedClassDescriptor.kt */
    /* renamed from: d0.e0.p.d.m0.l.b.e0.d$d  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0352d extends o implements Function0<List<? extends d0.e0.p.d.m0.c.g1.c>> {
        public C0352d() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final List<? extends d0.e0.p.d.m0.c.g1.c> invoke() {
            return d0.t.u.toList(d.this.getC().getComponents().getAnnotationAndConstantLoader().loadClassAnnotations(d.this.getThisAsProtoContainer$deserialization()));
        }
    }

    /* compiled from: DeserializedClassDescriptor.kt */
    /* loaded from: classes3.dex */
    public static final class e extends o implements Function0<d0.e0.p.d.m0.c.e> {
        public e() {
            super(0);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final d0.e0.p.d.m0.c.e invoke() {
            return d.access$computeCompanionObjectDescriptor(d.this);
        }
    }

    /* compiled from: DeserializedClassDescriptor.kt */
    /* loaded from: classes3.dex */
    public static final class f extends o implements Function0<Collection<? extends d0.e0.p.d.m0.c.d>> {
        public f() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final Collection<? extends d0.e0.p.d.m0.c.d> invoke() {
            return d.access$computeConstructors(d.this);
        }
    }

    /* compiled from: DeserializedClassDescriptor.kt */
    /* loaded from: classes3.dex */
    public /* synthetic */ class g extends d0.z.d.j implements Function1<d0.e0.p.d.m0.n.l1.g, a> {
        public g(d dVar) {
            super(1, dVar);
        }

        @Override // d0.z.d.d, kotlin.reflect.KCallable
        public final String getName() {
            return HookHelper.constructorName;
        }

        @Override // d0.z.d.d
        public final KDeclarationContainer getOwner() {
            return a0.getOrCreateKotlinClass(a.class);
        }

        @Override // d0.z.d.d
        public final String getSignature() {
            return "<init>(Lorg/jetbrains/kotlin/serialization/deserialization/descriptors/DeserializedClassDescriptor;Lorg/jetbrains/kotlin/types/checker/KotlinTypeRefiner;)V";
        }

        public final a invoke(d0.e0.p.d.m0.n.l1.g gVar) {
            d0.z.d.m.checkNotNullParameter(gVar, "p0");
            return new a((d) this.receiver, gVar);
        }
    }

    /* compiled from: DeserializedClassDescriptor.kt */
    /* loaded from: classes3.dex */
    public static final class h extends o implements Function0<d0.e0.p.d.m0.c.d> {
        public h() {
            super(0);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final d0.e0.p.d.m0.c.d invoke() {
            return d.access$computePrimaryConstructor(d.this);
        }
    }

    /* compiled from: DeserializedClassDescriptor.kt */
    /* loaded from: classes3.dex */
    public static final class i extends o implements Function0<Collection<? extends d0.e0.p.d.m0.c.e>> {
        public i() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final Collection<? extends d0.e0.p.d.m0.c.e> invoke() {
            return d.access$computeSubclassesForSealedClass(d.this);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public d(d0.e0.p.d.m0.l.b.l lVar, d0.e0.p.d.m0.f.c cVar, d0.e0.p.d.m0.f.z.c cVar2, d0.e0.p.d.m0.f.z.a aVar, u0 u0Var) {
        super(lVar.getStorageManager(), w.getClassId(cVar2, cVar.getFqName()).getShortClassName());
        d0.e0.p.d.m0.c.g1.g gVar;
        d0.z.d.m.checkNotNullParameter(lVar, "outerContext");
        d0.z.d.m.checkNotNullParameter(cVar, "classProto");
        d0.z.d.m.checkNotNullParameter(cVar2, "nameResolver");
        d0.z.d.m.checkNotNullParameter(aVar, "metadataVersion");
        d0.z.d.m.checkNotNullParameter(u0Var, "sourceElement");
        this.o = cVar;
        this.p = aVar;
        this.q = u0Var;
        this.r = w.getClassId(cVar2, cVar.getFqName());
        d0.e0.p.d.m0.l.b.z zVar = d0.e0.p.d.m0.l.b.z.a;
        this.f3459s = zVar.modality(d0.e0.p.d.m0.f.z.b.d.get(cVar.getFlags()));
        this.t = d0.e0.p.d.m0.l.b.a0.descriptorVisibility(zVar, d0.e0.p.d.m0.f.z.b.c.get(cVar.getFlags()));
        d0.e0.p.d.m0.c.f classKind = zVar.classKind(d0.e0.p.d.m0.f.z.b.e.get(cVar.getFlags()));
        this.u = classKind;
        List<s> typeParameterList = cVar.getTypeParameterList();
        d0.z.d.m.checkNotNullExpressionValue(typeParameterList, "classProto.typeParameterList");
        t typeTable = cVar.getTypeTable();
        d0.z.d.m.checkNotNullExpressionValue(typeTable, "classProto.typeTable");
        d0.e0.p.d.m0.f.z.g gVar2 = new d0.e0.p.d.m0.f.z.g(typeTable);
        i.a aVar2 = d0.e0.p.d.m0.f.z.i.a;
        d0.e0.p.d.m0.f.w versionRequirementTable = cVar.getVersionRequirementTable();
        d0.z.d.m.checkNotNullExpressionValue(versionRequirementTable, "classProto.versionRequirementTable");
        d0.e0.p.d.m0.l.b.l childContext = lVar.childContext(this, typeParameterList, cVar2, gVar2, aVar2.create(versionRequirementTable), aVar);
        this.v = childContext;
        d0.e0.p.d.m0.c.f fVar = d0.e0.p.d.m0.c.f.ENUM_CLASS;
        this.w = classKind == fVar ? new d0.e0.p.d.m0.k.a0.m(childContext.getStorageManager(), this) : i.b.f3433b;
        this.f3460x = new b(this);
        this.f3461y = r0.a.create(this, childContext.getStorageManager(), childContext.getComponents().getKotlinTypeChecker().getKotlinTypeRefiner(), new g(this));
        y.a aVar3 = null;
        this.f3462z = classKind == fVar ? new c(this) : null;
        m containingDeclaration = lVar.getContainingDeclaration();
        this.A = containingDeclaration;
        this.B = childContext.getStorageManager().createNullableLazyValue(new h());
        this.C = childContext.getStorageManager().createLazyValue(new f());
        this.D = childContext.getStorageManager().createNullableLazyValue(new e());
        this.E = childContext.getStorageManager().createLazyValue(new i());
        d0.e0.p.d.m0.f.z.c nameResolver = childContext.getNameResolver();
        d0.e0.p.d.m0.f.z.g typeTable2 = childContext.getTypeTable();
        d dVar = containingDeclaration instanceof d ? (d) containingDeclaration : null;
        this.F = new y.a(cVar, nameResolver, typeTable2, u0Var, dVar != null ? dVar.F : aVar3);
        if (!d0.e0.p.d.m0.f.z.b.f3384b.get(cVar.getFlags()).booleanValue()) {
            gVar = d0.e0.p.d.m0.c.g1.g.f.getEMPTY();
        } else {
            gVar = new n(childContext.getStorageManager(), new C0352d());
        }
        this.G = gVar;
    }

    public static final d0.e0.p.d.m0.c.e access$computeCompanionObjectDescriptor(d dVar) {
        if (!dVar.o.hasCompanionObjectName()) {
            return null;
        }
        d0.e0.p.d.m0.c.h contributedClassifier = dVar.b().getContributedClassifier(w.getName(dVar.v.getNameResolver(), dVar.o.getCompanionObjectName()), d0.e0.p.d.m0.d.b.d.FROM_DESERIALIZATION);
        if (contributedClassifier instanceof d0.e0.p.d.m0.c.e) {
            return (d0.e0.p.d.m0.c.e) contributedClassifier;
        }
        return null;
    }

    public static final Collection access$computeConstructors(d dVar) {
        List<d0.e0.p.d.m0.f.d> constructorList = dVar.o.getConstructorList();
        ArrayList Y = b.d.b.a.a.Y(constructorList, "classProto.constructorList");
        for (Object obj : constructorList) {
            Boolean bool = d0.e0.p.d.m0.f.z.b.l.get(((d0.e0.p.d.m0.f.d) obj).getFlags());
            d0.z.d.m.checkNotNullExpressionValue(bool, "IS_SECONDARY.get(it.flags)");
            if (bool.booleanValue()) {
                Y.add(obj);
            }
        }
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(Y, 10));
        Iterator it = Y.iterator();
        while (it.hasNext()) {
            d0.e0.p.d.m0.f.d dVar2 = (d0.e0.p.d.m0.f.d) it.next();
            d0.e0.p.d.m0.l.b.u memberDeserializer = dVar.getC().getMemberDeserializer();
            d0.z.d.m.checkNotNullExpressionValue(dVar2, "it");
            arrayList.add(memberDeserializer.loadConstructor(dVar2, false));
        }
        return d0.t.u.plus((Collection) d0.t.u.plus((Collection) arrayList, (Iterable) n.listOfNotNull(dVar.getUnsubstitutedPrimaryConstructor())), (Iterable) dVar.v.getComponents().getAdditionalClassPartsProvider().getConstructors(dVar));
    }

    public static final d0.e0.p.d.m0.c.d access$computePrimaryConstructor(d dVar) {
        Object obj;
        if (dVar.u.isSingleton()) {
            d0.e0.p.d.m0.c.i1.g createPrimaryConstructorForObject = d0.e0.p.d.m0.k.d.createPrimaryConstructorForObject(dVar, u0.a);
            createPrimaryConstructorForObject.setReturnType(dVar.getDefaultType());
            return createPrimaryConstructorForObject;
        }
        List<d0.e0.p.d.m0.f.d> constructorList = dVar.o.getConstructorList();
        d0.z.d.m.checkNotNullExpressionValue(constructorList, "classProto.constructorList");
        Iterator<T> it = constructorList.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (!d0.e0.p.d.m0.f.z.b.l.get(((d0.e0.p.d.m0.f.d) obj).getFlags()).booleanValue()) {
                break;
            }
        }
        d0.e0.p.d.m0.f.d dVar2 = (d0.e0.p.d.m0.f.d) obj;
        if (dVar2 == null) {
            return null;
        }
        return dVar.getC().getMemberDeserializer().loadConstructor(dVar2, true);
    }

    public static final Collection access$computeSubclassesForSealedClass(d dVar) {
        if (dVar.f3459s != z.SEALED) {
            return n.emptyList();
        }
        List<Integer> sealedSubclassFqNameList = dVar.o.getSealedSubclassFqNameList();
        d0.z.d.m.checkNotNullExpressionValue(sealedSubclassFqNameList, "fqNames");
        if (!(!sealedSubclassFqNameList.isEmpty())) {
            return d0.e0.p.d.m0.k.a.a.computeSealedSubclasses(dVar, false);
        }
        ArrayList arrayList = new ArrayList();
        for (Integer num : sealedSubclassFqNameList) {
            d0.e0.p.d.m0.l.b.j components = dVar.getC().getComponents();
            d0.e0.p.d.m0.f.z.c nameResolver = dVar.getC().getNameResolver();
            d0.z.d.m.checkNotNullExpressionValue(num, "index");
            d0.e0.p.d.m0.c.e deserializeClass = components.deserializeClass(w.getClassId(nameResolver, num.intValue()));
            if (deserializeClass != null) {
                arrayList.add(deserializeClass);
            }
        }
        return arrayList;
    }

    public final a b() {
        return this.f3461y.getScope(this.v.getComponents().getKotlinTypeChecker().getKotlinTypeRefiner());
    }

    @Override // d0.e0.p.d.m0.c.g1.a
    public d0.e0.p.d.m0.c.g1.g getAnnotations() {
        return this.G;
    }

    public final d0.e0.p.d.m0.l.b.l getC() {
        return this.v;
    }

    public final d0.e0.p.d.m0.f.c getClassProto() {
        return this.o;
    }

    @Override // d0.e0.p.d.m0.c.e
    public d0.e0.p.d.m0.c.e getCompanionObjectDescriptor() {
        return this.D.invoke();
    }

    @Override // d0.e0.p.d.m0.c.e
    public Collection<d0.e0.p.d.m0.c.d> getConstructors() {
        return this.C.invoke();
    }

    @Override // d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.n, d0.e0.p.d.m0.c.m
    public m getContainingDeclaration() {
        return this.A;
    }

    @Override // d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.i
    public List<z0> getDeclaredTypeParameters() {
        return this.v.getTypeDeserializer().getOwnTypeParameters();
    }

    @Override // d0.e0.p.d.m0.c.e
    public d0.e0.p.d.m0.c.f getKind() {
        return this.u;
    }

    public final d0.e0.p.d.m0.f.z.a getMetadataVersion() {
        return this.p;
    }

    @Override // d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.y
    public z getModality() {
        return this.f3459s;
    }

    @Override // d0.e0.p.d.m0.c.e
    public Collection<d0.e0.p.d.m0.c.e> getSealedSubclasses() {
        return this.E.invoke();
    }

    @Override // d0.e0.p.d.m0.c.p
    public u0 getSource() {
        return this.q;
    }

    public final y.a getThisAsProtoContainer$deserialization() {
        return this.F;
    }

    @Override // d0.e0.p.d.m0.c.h
    public d0.e0.p.d.m0.n.u0 getTypeConstructor() {
        return this.f3460x;
    }

    @Override // d0.e0.p.d.m0.c.i1.u
    public d0.e0.p.d.m0.k.a0.i getUnsubstitutedMemberScope(d0.e0.p.d.m0.n.l1.g gVar) {
        d0.z.d.m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
        return this.f3461y.getScope(gVar);
    }

    @Override // d0.e0.p.d.m0.c.e
    public d0.e0.p.d.m0.c.d getUnsubstitutedPrimaryConstructor() {
        return this.B.invoke();
    }

    @Override // d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.q, d0.e0.p.d.m0.c.y
    public u getVisibility() {
        return this.t;
    }

    public final boolean hasNestedClass$deserialization(d0.e0.p.d.m0.g.e eVar) {
        d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        return b().getClassNames$deserialization().contains(eVar);
    }

    @Override // d0.e0.p.d.m0.c.y
    public boolean isActual() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.e
    public boolean isCompanionObject() {
        return d0.e0.p.d.m0.f.z.b.e.get(this.o.getFlags()) == c.EnumC0329c.COMPANION_OBJECT;
    }

    @Override // d0.e0.p.d.m0.c.e
    public boolean isData() {
        Boolean bool = d0.e0.p.d.m0.f.z.b.g.get(this.o.getFlags());
        d0.z.d.m.checkNotNullExpressionValue(bool, "IS_DATA.get(classProto.flags)");
        return bool.booleanValue();
    }

    @Override // d0.e0.p.d.m0.c.y
    public boolean isExpect() {
        Boolean bool = d0.e0.p.d.m0.f.z.b.i.get(this.o.getFlags());
        d0.z.d.m.checkNotNullExpressionValue(bool, "IS_EXPECT_CLASS.get(classProto.flags)");
        return bool.booleanValue();
    }

    @Override // d0.e0.p.d.m0.c.y
    public boolean isExternal() {
        Boolean bool = d0.e0.p.d.m0.f.z.b.h.get(this.o.getFlags());
        d0.z.d.m.checkNotNullExpressionValue(bool, "IS_EXTERNAL_CLASS.get(classProto.flags)");
        return bool.booleanValue();
    }

    @Override // d0.e0.p.d.m0.c.e
    public boolean isFun() {
        Boolean bool = d0.e0.p.d.m0.f.z.b.k.get(this.o.getFlags());
        d0.z.d.m.checkNotNullExpressionValue(bool, "IS_FUN_INTERFACE.get(classProto.flags)");
        return bool.booleanValue();
    }

    @Override // d0.e0.p.d.m0.c.e
    public boolean isInline() {
        Boolean bool = d0.e0.p.d.m0.f.z.b.j.get(this.o.getFlags());
        d0.z.d.m.checkNotNullExpressionValue(bool, "IS_INLINE_CLASS.get(classProto.flags)");
        return bool.booleanValue() && this.p.isAtMost(1, 4, 1);
    }

    @Override // d0.e0.p.d.m0.c.i
    public boolean isInner() {
        Boolean bool = d0.e0.p.d.m0.f.z.b.f.get(this.o.getFlags());
        d0.z.d.m.checkNotNullExpressionValue(bool, "IS_INNER.get(classProto.flags)");
        return bool.booleanValue();
    }

    @Override // d0.e0.p.d.m0.c.e
    public boolean isValue() {
        Boolean bool = d0.e0.p.d.m0.f.z.b.j.get(this.o.getFlags());
        d0.z.d.m.checkNotNullExpressionValue(bool, "IS_INLINE_CLASS.get(classProto.flags)");
        return bool.booleanValue() && this.p.isAtLeast(1, 4, 2);
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("deserialized ");
        R.append(isExpect() ? "expect " : "");
        R.append("class ");
        R.append(getName());
        return R.toString();
    }

    /* compiled from: DeserializedClassDescriptor.kt */
    /* loaded from: classes3.dex */
    public final class b extends d0.e0.p.d.m0.n.b {
        public final j<List<z0>> c;
        public final /* synthetic */ d d;

        /* compiled from: DeserializedClassDescriptor.kt */
        /* loaded from: classes3.dex */
        public static final class a extends o implements Function0<List<? extends z0>> {
            public final /* synthetic */ d this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public a(d dVar) {
                super(0);
                this.this$0 = dVar;
            }

            @Override // kotlin.jvm.functions.Function0
            public final List<? extends z0> invoke() {
                return a1.computeConstructorTypeParameters(this.this$0);
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(d dVar) {
            super(dVar.getC().getStorageManager());
            d0.z.d.m.checkNotNullParameter(dVar, "this$0");
            this.d = dVar;
            this.c = dVar.getC().getStorageManager().createLazyValue(new a(dVar));
        }

        @Override // d0.e0.p.d.m0.n.g
        public Collection<c0> a() {
            d0.e0.p.d.m0.g.b asSingleFqName;
            List<q> supertypes = d0.e0.p.d.m0.f.z.f.supertypes(this.d.getClassProto(), this.d.getC().getTypeTable());
            d dVar = this.d;
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(supertypes, 10));
            for (q qVar : supertypes) {
                arrayList.add(dVar.getC().getTypeDeserializer().type(qVar));
            }
            List plus = d0.t.u.plus((Collection) arrayList, (Iterable) this.d.getC().getComponents().getAdditionalClassPartsProvider().getSupertypes(this.d));
            ArrayList<d0.b> arrayList2 = new ArrayList();
            Iterator it = plus.iterator();
            while (true) {
                d0.b bVar = null;
                if (!it.hasNext()) {
                    break;
                }
                d0.e0.p.d.m0.c.h declarationDescriptor = ((c0) it.next()).getConstructor().getDeclarationDescriptor();
                if (declarationDescriptor instanceof d0.b) {
                    bVar = (d0.b) declarationDescriptor;
                }
                if (bVar != null) {
                    arrayList2.add(bVar);
                }
            }
            if (!arrayList2.isEmpty()) {
                p errorReporter = this.d.getC().getComponents().getErrorReporter();
                d dVar2 = this.d;
                ArrayList arrayList3 = new ArrayList(d0.t.o.collectionSizeOrDefault(arrayList2, 10));
                for (d0.b bVar2 : arrayList2) {
                    d0.e0.p.d.m0.g.a classId = d0.e0.p.d.m0.k.x.a.getClassId(bVar2);
                    String asString = (classId == null || (asSingleFqName = classId.asSingleFqName()) == null) ? null : asSingleFqName.asString();
                    if (asString == null) {
                        asString = bVar2.getName().asString();
                    }
                    arrayList3.add(asString);
                }
                errorReporter.reportIncompleteHierarchy(dVar2, arrayList3);
            }
            return d0.t.u.toList(plus);
        }

        @Override // d0.e0.p.d.m0.n.g
        public x0 d() {
            return x0.a.a;
        }

        @Override // d0.e0.p.d.m0.n.u0
        public List<z0> getParameters() {
            return this.c.invoke();
        }

        @Override // d0.e0.p.d.m0.n.u0
        public boolean isDenotable() {
            return true;
        }

        public String toString() {
            String eVar = this.d.getName().toString();
            d0.z.d.m.checkNotNullExpressionValue(eVar, "name.toString()");
            return eVar;
        }

        @Override // d0.e0.p.d.m0.n.b, d0.e0.p.d.m0.n.g, d0.e0.p.d.m0.n.u0
        public d getDeclarationDescriptor() {
            return this.d;
        }
    }

    @Override // d0.e0.p.d.m0.c.e
    public d0.e0.p.d.m0.k.a0.j getStaticScope() {
        return this.w;
    }
}
