package d0.e0.p.d.m0.l.b;

import d0.e0.p.d.m0.n.j0;
/* compiled from: LocalClassifierTypeSettings.kt */
/* loaded from: classes3.dex */
public interface t {

    /* compiled from: LocalClassifierTypeSettings.kt */
    /* loaded from: classes3.dex */
    public static final class a implements t {
        public static final a a = new a();

        @Override // d0.e0.p.d.m0.l.b.t
        public j0 getReplacementTypeForLocalClassifiers() {
            return null;
        }
    }

    j0 getReplacementTypeForLocalClassifiers();
}
