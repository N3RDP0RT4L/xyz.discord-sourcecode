package d0.e0.p.d.m0.l.b;

import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.f.z.a;
import d0.e0.p.d.m0.f.z.c;
import d0.z.d.m;
/* compiled from: ClassData.kt */
/* loaded from: classes3.dex */
public final class f {
    public final c a;

    /* renamed from: b  reason: collision with root package name */
    public final d0.e0.p.d.m0.f.c f3471b;
    public final a c;
    public final u0 d;

    public f(c cVar, d0.e0.p.d.m0.f.c cVar2, a aVar, u0 u0Var) {
        m.checkNotNullParameter(cVar, "nameResolver");
        m.checkNotNullParameter(cVar2, "classProto");
        m.checkNotNullParameter(aVar, "metadataVersion");
        m.checkNotNullParameter(u0Var, "sourceElement");
        this.a = cVar;
        this.f3471b = cVar2;
        this.c = aVar;
        this.d = u0Var;
    }

    public final c component1() {
        return this.a;
    }

    public final d0.e0.p.d.m0.f.c component2() {
        return this.f3471b;
    }

    public final a component3() {
        return this.c;
    }

    public final u0 component4() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        return m.areEqual(this.a, fVar.a) && m.areEqual(this.f3471b, fVar.f3471b) && m.areEqual(this.c, fVar.c) && m.areEqual(this.d, fVar.d);
    }

    public int hashCode() {
        int hashCode = this.f3471b.hashCode();
        int hashCode2 = this.c.hashCode();
        return this.d.hashCode() + ((hashCode2 + ((hashCode + (this.a.hashCode() * 31)) * 31)) * 31);
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("ClassData(nameResolver=");
        R.append(this.a);
        R.append(", classProto=");
        R.append(this.f3471b);
        R.append(", metadataVersion=");
        R.append(this.c);
        R.append(", sourceElement=");
        R.append(this.d);
        R.append(')');
        return R.toString();
    }
}
