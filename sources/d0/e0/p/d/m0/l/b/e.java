package d0.e0.p.d.m0.l.b;

import d0.d0.f;
import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.d;
import d0.e0.p.d.m0.c.d0;
import d0.e0.p.d.m0.c.g1.c;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.c.w;
import d0.e0.p.d.m0.f.b;
import d0.e0.p.d.m0.k.v.a0;
import d0.e0.p.d.m0.k.v.g;
import d0.e0.p.d.m0.k.v.i;
import d0.e0.p.d.m0.k.v.j;
import d0.e0.p.d.m0.k.v.k;
import d0.e0.p.d.m0.k.v.l;
import d0.e0.p.d.m0.k.v.r;
import d0.e0.p.d.m0.k.v.s;
import d0.e0.p.d.m0.k.v.v;
import d0.e0.p.d.m0.k.v.x;
import d0.e0.p.d.m0.k.v.y;
import d0.e0.p.d.m0.k.v.z;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.t;
import d0.t.g0;
import d0.t.h0;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Pair;
import kotlin.ranges.IntRange;
/* compiled from: AnnotationDeserializer.kt */
/* loaded from: classes3.dex */
public final class e {
    public final c0 a;

    /* renamed from: b  reason: collision with root package name */
    public final d0 f3458b;

    /* compiled from: AnnotationDeserializer.kt */
    /* loaded from: classes3.dex */
    public /* synthetic */ class a {
        public static final /* synthetic */ int[] a = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};

        static {
            b.C0325b.c.EnumC0328c.values();
        }
    }

    public e(c0 c0Var, d0 d0Var) {
        m.checkNotNullParameter(c0Var, "module");
        m.checkNotNullParameter(d0Var, "notFoundClasses");
        this.a = c0Var;
        this.f3458b = d0Var;
    }

    public final boolean a(g<?> gVar, d0.e0.p.d.m0.n.c0 c0Var, b.C0325b.c cVar) {
        b.C0325b.c.EnumC0328c type = cVar.getType();
        int i = type == null ? -1 : a.a[type.ordinal()];
        if (i == 10) {
            h declarationDescriptor = c0Var.getConstructor().getDeclarationDescriptor();
            d0.e0.p.d.m0.c.e eVar = declarationDescriptor instanceof d0.e0.p.d.m0.c.e ? (d0.e0.p.d.m0.c.e) declarationDescriptor : null;
            if (eVar == null || d0.e0.p.d.m0.b.h.isKClass(eVar)) {
                return true;
            }
        } else if (i != 13) {
            return m.areEqual(gVar.getType(this.a), c0Var);
        } else {
            if ((gVar instanceof d0.e0.p.d.m0.k.v.b) && ((d0.e0.p.d.m0.k.v.b) gVar).getValue().size() == cVar.getArrayElementList().size()) {
                d0.e0.p.d.m0.n.c0 arrayElementType = this.a.getBuiltIns().getArrayElementType(c0Var);
                m.checkNotNullExpressionValue(arrayElementType, "builtIns.getArrayElementType(expectedType)");
                d0.e0.p.d.m0.k.v.b bVar = (d0.e0.p.d.m0.k.v.b) gVar;
                IntRange indices = n.getIndices(bVar.getValue());
                if ((indices instanceof Collection) && ((Collection) indices).isEmpty()) {
                    return true;
                }
                Iterator<Integer> it = indices.iterator();
                while (it.hasNext()) {
                    int nextInt = ((d0.t.c0) it).nextInt();
                    b.C0325b.c arrayElement = cVar.getArrayElement(nextInt);
                    m.checkNotNullExpressionValue(arrayElement, "value.getArrayElement(i)");
                    if (!a(bVar.getValue().get(nextInt), arrayElementType, arrayElement)) {
                    }
                }
                return true;
            }
            throw new IllegalStateException(m.stringPlus("Deserialized ArrayValue should have the same number of elements as the original array value: ", gVar).toString());
        }
        return false;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r6v0, types: [kotlin.Pair] */
    public final c deserializeAnnotation(b bVar, d0.e0.p.d.m0.f.z.c cVar) {
        m.checkNotNullParameter(bVar, "proto");
        m.checkNotNullParameter(cVar, "nameResolver");
        d0.e0.p.d.m0.c.e findNonGenericClassAcrossDependencies = w.findNonGenericClassAcrossDependencies(this.a, w.getClassId(cVar, bVar.getId()), this.f3458b);
        Map emptyMap = h0.emptyMap();
        if (bVar.getArgumentCount() != 0 && !t.isError(findNonGenericClassAcrossDependencies) && d0.e0.p.d.m0.k.e.isAnnotationClass(findNonGenericClassAcrossDependencies)) {
            Collection<d> constructors = findNonGenericClassAcrossDependencies.getConstructors();
            m.checkNotNullExpressionValue(constructors, "annotationClass.constructors");
            d dVar = (d) u.singleOrNull(constructors);
            if (dVar != null) {
                List<c1> valueParameters = dVar.getValueParameters();
                m.checkNotNullExpressionValue(valueParameters, "constructor.valueParameters");
                LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(o.collectionSizeOrDefault(valueParameters, 10)), 16));
                for (Object obj : valueParameters) {
                    linkedHashMap.put(((c1) obj).getName(), obj);
                }
                List<b.C0325b> argumentList = bVar.getArgumentList();
                ArrayList Y = b.d.b.a.a.Y(argumentList, "proto.argumentList");
                for (b.C0325b bVar2 : argumentList) {
                    m.checkNotNullExpressionValue(bVar2, "it");
                    c1 c1Var = (c1) linkedHashMap.get(w.getName(cVar, bVar2.getNameId()));
                    k kVar = null;
                    if (c1Var != null) {
                        d0.e0.p.d.m0.g.e name = w.getName(cVar, bVar2.getNameId());
                        d0.e0.p.d.m0.n.c0 type = c1Var.getType();
                        m.checkNotNullExpressionValue(type, "parameter.type");
                        b.C0325b.c value = bVar2.getValue();
                        m.checkNotNullExpressionValue(value, "proto.value");
                        g<?> resolveValue = resolveValue(type, value, cVar);
                        if (a(resolveValue, type, value)) {
                            kVar = resolveValue;
                        }
                        if (kVar == null) {
                            k.a aVar = k.f3446b;
                            StringBuilder R = b.d.b.a.a.R("Unexpected argument value: actual type ");
                            R.append(value.getType());
                            R.append(" != expected type ");
                            R.append(type);
                            kVar = aVar.create(R.toString());
                        }
                        kVar = new Pair(name, kVar);
                    }
                    if (kVar != null) {
                        Y.add(kVar);
                    }
                }
                emptyMap = h0.toMap(Y);
            }
        }
        return new d0.e0.p.d.m0.c.g1.d(findNonGenericClassAcrossDependencies.getDefaultType(), emptyMap, u0.a);
    }

    public final g<?> resolveValue(d0.e0.p.d.m0.n.c0 c0Var, b.C0325b.c cVar, d0.e0.p.d.m0.f.z.c cVar2) {
        g<?> gVar;
        m.checkNotNullParameter(c0Var, "expectedType");
        m.checkNotNullParameter(cVar, "value");
        m.checkNotNullParameter(cVar2, "nameResolver");
        Boolean bool = d0.e0.p.d.m0.f.z.b.M.get(cVar.getFlags());
        m.checkNotNullExpressionValue(bool, "IS_UNSIGNED.get(value.flags)");
        boolean booleanValue = bool.booleanValue();
        b.C0325b.c.EnumC0328c type = cVar.getType();
        switch (type == null ? -1 : a.a[type.ordinal()]) {
            case 1:
                byte intValue = (byte) cVar.getIntValue();
                if (booleanValue) {
                    gVar = new x(intValue);
                    break;
                } else {
                    gVar = new d0.e0.p.d.m0.k.v.d(intValue);
                    break;
                }
            case 2:
                return new d0.e0.p.d.m0.k.v.e((char) cVar.getIntValue());
            case 3:
                short intValue2 = (short) cVar.getIntValue();
                if (booleanValue) {
                    gVar = new a0(intValue2);
                    break;
                } else {
                    gVar = new v(intValue2);
                    break;
                }
            case 4:
                int intValue3 = (int) cVar.getIntValue();
                return booleanValue ? new y(intValue3) : new d0.e0.p.d.m0.k.v.m(intValue3);
            case 5:
                long intValue4 = cVar.getIntValue();
                return booleanValue ? new z(intValue4) : new s(intValue4);
            case 6:
                return new l(cVar.getFloatValue());
            case 7:
                return new i(cVar.getDoubleValue());
            case 8:
                return new d0.e0.p.d.m0.k.v.c(cVar.getIntValue() != 0);
            case 9:
                return new d0.e0.p.d.m0.k.v.w(cVar2.getString(cVar.getStringValue()));
            case 10:
                return new r(w.getClassId(cVar2, cVar.getClassId()), cVar.getArrayDimensionCount());
            case 11:
                return new j(w.getClassId(cVar2, cVar.getClassId()), w.getName(cVar2, cVar.getEnumValueId()));
            case 12:
                b annotation = cVar.getAnnotation();
                m.checkNotNullExpressionValue(annotation, "value.annotation");
                return new d0.e0.p.d.m0.k.v.a(deserializeAnnotation(annotation, cVar2));
            case 13:
                d0.e0.p.d.m0.k.v.h hVar = d0.e0.p.d.m0.k.v.h.a;
                List<b.C0325b.c> arrayElementList = cVar.getArrayElementList();
                m.checkNotNullExpressionValue(arrayElementList, "value.arrayElementList");
                ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(arrayElementList, 10));
                for (b.C0325b.c cVar3 : arrayElementList) {
                    j0 anyType = this.a.getBuiltIns().getAnyType();
                    m.checkNotNullExpressionValue(anyType, "builtIns.anyType");
                    m.checkNotNullExpressionValue(cVar3, "it");
                    arrayList.add(resolveValue(anyType, cVar3, cVar2));
                }
                return hVar.createArrayValue(arrayList, c0Var);
            default:
                StringBuilder R = b.d.b.a.a.R("Unsupported annotation argument type: ");
                R.append(cVar.getType());
                R.append(" (expected ");
                R.append(c0Var);
                R.append(')');
                throw new IllegalStateException(R.toString().toString());
        }
        return gVar;
    }
}
