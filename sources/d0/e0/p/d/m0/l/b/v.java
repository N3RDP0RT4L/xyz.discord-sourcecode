package d0.e0.p.d.m0.l.b;

import d0.e0.p.d.m0.c.g1.c;
import d0.e0.p.d.m0.i.n;
import d0.z.d.o;
import java.util.List;
import kotlin.jvm.functions.Function0;
/* compiled from: MemberDeserializer.kt */
/* loaded from: classes3.dex */
public final class v extends o implements Function0<List<? extends c>> {
    public final /* synthetic */ b $kind;
    public final /* synthetic */ n $proto;
    public final /* synthetic */ u this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public v(u uVar, n nVar, b bVar) {
        super(0);
        this.this$0 = uVar;
        this.$proto = nVar;
        this.$kind = bVar;
    }

    @Override // kotlin.jvm.functions.Function0
    public final List<? extends c> invoke() {
        List<c> list;
        u uVar = this.this$0;
        y a = uVar.a(uVar.a.getContainingDeclaration());
        if (a == null) {
            list = null;
        } else {
            list = this.this$0.a.getComponents().getAnnotationAndConstantLoader().loadExtensionReceiverParameterAnnotations(a, this.$proto, this.$kind);
        }
        return list != null ? list : d0.t.n.emptyList();
    }
}
