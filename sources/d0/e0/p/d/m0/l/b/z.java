package d0.e0.p.d.m0.l.b;

import d0.e0.p.d.m0.c.f;
import d0.e0.p.d.m0.f.c;
import d0.e0.p.d.m0.f.k;
import d0.e0.p.d.m0.f.q;
import d0.e0.p.d.m0.f.s;
import d0.e0.p.d.m0.f.x;
import d0.e0.p.d.m0.n.j1;
import d0.z.d.m;
import kotlin.NoWhenBranchMatchedException;
/* compiled from: ProtoEnumFlags.kt */
/* loaded from: classes3.dex */
public final class z {
    public static final z a = new z();

    /* compiled from: ProtoEnumFlags.kt */
    /* loaded from: classes3.dex */
    public /* synthetic */ class a {
        public static final /* synthetic */ int[] a = {1, 2, 3, 4};

        /* renamed from: b  reason: collision with root package name */
        public static final /* synthetic */ int[] f3483b = {1, 2, 3, 4, 5, 6, 7};
        public static final /* synthetic */ int[] c = {1, 2, 3};
        public static final /* synthetic */ int[] d = {1, 2, 3, 4};

        static {
            k.values();
            d0.e0.p.d.m0.c.z.values();
            d0.e0.p.d.m0.c.z zVar = d0.e0.p.d.m0.c.z.FINAL;
            d0.e0.p.d.m0.c.z zVar2 = d0.e0.p.d.m0.c.z.OPEN;
            d0.e0.p.d.m0.c.z zVar3 = d0.e0.p.d.m0.c.z.ABSTRACT;
            d0.e0.p.d.m0.c.z zVar4 = d0.e0.p.d.m0.c.z.SEALED;
            x.values();
            c.EnumC0329c.values();
            f.values();
            s.c.values();
            q.b.c.values();
            j1.values();
        }
    }

    public final f classKind(c.EnumC0329c cVar) {
        f fVar = f.CLASS;
        switch (cVar == null ? -1 : a.f3483b[cVar.ordinal()]) {
            case 1:
            default:
                return fVar;
            case 2:
                return f.INTERFACE;
            case 3:
                return f.ENUM_CLASS;
            case 4:
                return f.ENUM_ENTRY;
            case 5:
                return f.ANNOTATION_CLASS;
            case 6:
            case 7:
                return f.OBJECT;
        }
    }

    public final d0.e0.p.d.m0.c.z modality(k kVar) {
        int i = kVar == null ? -1 : a.a[kVar.ordinal()];
        if (i == 1) {
            return d0.e0.p.d.m0.c.z.FINAL;
        }
        if (i == 2) {
            return d0.e0.p.d.m0.c.z.OPEN;
        }
        if (i == 3) {
            return d0.e0.p.d.m0.c.z.ABSTRACT;
        }
        if (i != 4) {
            return d0.e0.p.d.m0.c.z.FINAL;
        }
        return d0.e0.p.d.m0.c.z.SEALED;
    }

    public final j1 variance(s.c cVar) {
        m.checkNotNullParameter(cVar, "variance");
        int ordinal = cVar.ordinal();
        if (ordinal == 0) {
            return j1.IN_VARIANCE;
        }
        if (ordinal == 1) {
            return j1.OUT_VARIANCE;
        }
        if (ordinal == 2) {
            return j1.INVARIANT;
        }
        throw new NoWhenBranchMatchedException();
    }

    public final j1 variance(q.b.c cVar) {
        m.checkNotNullParameter(cVar, "projection");
        int ordinal = cVar.ordinal();
        if (ordinal == 0) {
            return j1.IN_VARIANCE;
        }
        if (ordinal == 1) {
            return j1.OUT_VARIANCE;
        }
        if (ordinal == 2) {
            return j1.INVARIANT;
        }
        if (ordinal != 3) {
            throw new NoWhenBranchMatchedException();
        }
        throw new IllegalArgumentException(m.stringPlus("Only IN, OUT and INV are supported. Actual argument: ", cVar));
    }
}
