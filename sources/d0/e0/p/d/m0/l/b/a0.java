package d0.e0.p.d.m0.l.b;

import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.t;
import d0.e0.p.d.m0.c.u;
import d0.e0.p.d.m0.f.j;
import d0.e0.p.d.m0.f.x;
import d0.z.d.m;
/* compiled from: ProtoEnumFlagsUtils.kt */
/* loaded from: classes3.dex */
public final class a0 {

    /* compiled from: ProtoEnumFlagsUtils.kt */
    /* loaded from: classes3.dex */
    public /* synthetic */ class a {
        public static final /* synthetic */ int[] a = {1, 2, 3, 4};

        /* renamed from: b  reason: collision with root package name */
        public static final /* synthetic */ int[] f3454b = {1, 2, 4, 5, 3, 6};

        static {
            j.values();
            b.a.values();
            x.values();
        }
    }

    public static final u descriptorVisibility(z zVar, x xVar) {
        m.checkNotNullParameter(zVar, "<this>");
        switch (xVar == null ? -1 : a.f3454b[xVar.ordinal()]) {
            case 1:
                u uVar = t.d;
                m.checkNotNullExpressionValue(uVar, "INTERNAL");
                return uVar;
            case 2:
                u uVar2 = t.a;
                m.checkNotNullExpressionValue(uVar2, "PRIVATE");
                return uVar2;
            case 3:
                u uVar3 = t.f3272b;
                m.checkNotNullExpressionValue(uVar3, "PRIVATE_TO_THIS");
                return uVar3;
            case 4:
                u uVar4 = t.c;
                m.checkNotNullExpressionValue(uVar4, "PROTECTED");
                return uVar4;
            case 5:
                u uVar5 = t.e;
                m.checkNotNullExpressionValue(uVar5, "PUBLIC");
                return uVar5;
            case 6:
                u uVar6 = t.f;
                m.checkNotNullExpressionValue(uVar6, "LOCAL");
                return uVar6;
            default:
                u uVar7 = t.a;
                m.checkNotNullExpressionValue(uVar7, "PRIVATE");
                return uVar7;
        }
    }

    public static final b.a memberKind(z zVar, j jVar) {
        b.a aVar = b.a.DECLARATION;
        m.checkNotNullParameter(zVar, "<this>");
        int i = jVar == null ? -1 : a.a[jVar.ordinal()];
        if (i == 1) {
            return aVar;
        }
        if (i == 2) {
            return b.a.FAKE_OVERRIDE;
        }
        if (i != 3) {
            return i != 4 ? aVar : b.a.SYNTHESIZED;
        }
        return b.a.DELEGATION;
    }
}
