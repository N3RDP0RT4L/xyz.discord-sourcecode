package d0.e0.p.d.m0.l.b;

import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.f.c;
import d0.e0.p.d.m0.f.z.c;
import d0.e0.p.d.m0.f.z.g;
import d0.z.d.m;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ProtoContainer.kt */
/* loaded from: classes3.dex */
public abstract class y {
    public final c a;

    /* renamed from: b  reason: collision with root package name */
    public final g f3482b;
    public final u0 c;

    /* compiled from: ProtoContainer.kt */
    /* loaded from: classes3.dex */
    public static final class a extends y {
        public final d0.e0.p.d.m0.f.c d;
        public final a e;
        public final d0.e0.p.d.m0.g.a f;
        public final c.EnumC0329c g;
        public final boolean h;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(d0.e0.p.d.m0.f.c cVar, d0.e0.p.d.m0.f.z.c cVar2, g gVar, u0 u0Var, a aVar) {
            super(cVar2, gVar, u0Var, null);
            m.checkNotNullParameter(cVar, "classProto");
            m.checkNotNullParameter(cVar2, "nameResolver");
            m.checkNotNullParameter(gVar, "typeTable");
            this.d = cVar;
            this.e = aVar;
            this.f = w.getClassId(cVar2, cVar.getFqName());
            c.EnumC0329c cVar3 = d0.e0.p.d.m0.f.z.b.e.get(cVar.getFlags());
            this.g = cVar3 == null ? c.EnumC0329c.CLASS : cVar3;
            Boolean bool = d0.e0.p.d.m0.f.z.b.f.get(cVar.getFlags());
            m.checkNotNullExpressionValue(bool, "IS_INNER.get(classProto.flags)");
            this.h = bool.booleanValue();
        }

        @Override // d0.e0.p.d.m0.l.b.y
        public d0.e0.p.d.m0.g.b debugFqName() {
            d0.e0.p.d.m0.g.b asSingleFqName = this.f.asSingleFqName();
            m.checkNotNullExpressionValue(asSingleFqName, "classId.asSingleFqName()");
            return asSingleFqName;
        }

        public final d0.e0.p.d.m0.g.a getClassId() {
            return this.f;
        }

        public final d0.e0.p.d.m0.f.c getClassProto() {
            return this.d;
        }

        public final c.EnumC0329c getKind() {
            return this.g;
        }

        public final a getOuterClass() {
            return this.e;
        }

        public final boolean isInner() {
            return this.h;
        }
    }

    /* compiled from: ProtoContainer.kt */
    /* loaded from: classes3.dex */
    public static final class b extends y {
        public final d0.e0.p.d.m0.g.b d;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(d0.e0.p.d.m0.g.b bVar, d0.e0.p.d.m0.f.z.c cVar, g gVar, u0 u0Var) {
            super(cVar, gVar, u0Var, null);
            m.checkNotNullParameter(bVar, "fqName");
            m.checkNotNullParameter(cVar, "nameResolver");
            m.checkNotNullParameter(gVar, "typeTable");
            this.d = bVar;
        }

        @Override // d0.e0.p.d.m0.l.b.y
        public d0.e0.p.d.m0.g.b debugFqName() {
            return this.d;
        }
    }

    public y(d0.e0.p.d.m0.f.z.c cVar, g gVar, u0 u0Var, DefaultConstructorMarker defaultConstructorMarker) {
        this.a = cVar;
        this.f3482b = gVar;
        this.c = u0Var;
    }

    public abstract d0.e0.p.d.m0.g.b debugFqName();

    public final d0.e0.p.d.m0.f.z.c getNameResolver() {
        return this.a;
    }

    public final u0 getSource() {
        return this.c;
    }

    public final g getTypeTable() {
        return this.f3482b;
    }

    public String toString() {
        return ((Object) getClass().getSimpleName()) + ": " + debugFqName();
    }
}
