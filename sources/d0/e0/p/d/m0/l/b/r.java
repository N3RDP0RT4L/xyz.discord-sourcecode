package d0.e0.p.d.m0.l.b;

import d0.e0.p.d.m0.g.a;
import d0.z.d.m;
/* compiled from: IncompatibleVersionErrorData.kt */
/* loaded from: classes3.dex */
public final class r<T> {
    public final T a;

    /* renamed from: b  reason: collision with root package name */
    public final T f3479b;
    public final String c;
    public final a d;

    public r(T t, T t2, String str, a aVar) {
        m.checkNotNullParameter(str, "filePath");
        m.checkNotNullParameter(aVar, "classId");
        this.a = t;
        this.f3479b = t2;
        this.c = str;
        this.d = aVar;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof r)) {
            return false;
        }
        r rVar = (r) obj;
        return m.areEqual(this.a, rVar.a) && m.areEqual(this.f3479b, rVar.f3479b) && m.areEqual(this.c, rVar.c) && m.areEqual(this.d, rVar.d);
    }

    public int hashCode() {
        T t = this.a;
        int i = 0;
        int hashCode = (t == null ? 0 : t.hashCode()) * 31;
        T t2 = this.f3479b;
        if (t2 != null) {
            i = t2.hashCode();
        }
        return this.d.hashCode() + b.d.b.a.a.m(this.c, (hashCode + i) * 31, 31);
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("IncompatibleVersionErrorData(actualVersion=");
        R.append(this.a);
        R.append(", expectedVersion=");
        R.append(this.f3479b);
        R.append(", filePath=");
        R.append(this.c);
        R.append(", classId=");
        R.append(this.d);
        R.append(')');
        return R.toString();
    }
}
