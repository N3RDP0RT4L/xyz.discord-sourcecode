package d0.e0.p.d.m0.l.b.e0;

import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.c.y;
import d0.e0.p.d.m0.f.z.c;
import d0.e0.p.d.m0.f.z.h;
import d0.e0.p.d.m0.f.z.i;
import d0.e0.p.d.m0.i.n;
import java.util.List;
/* compiled from: DeserializedMemberDescriptor.kt */
/* loaded from: classes3.dex */
public interface g extends m, y {

    /* compiled from: DeserializedMemberDescriptor.kt */
    /* loaded from: classes3.dex */
    public enum a {
        COMPATIBLE,
        NEEDS_WRAPPER,
        INCOMPATIBLE
    }

    /* compiled from: DeserializedMemberDescriptor.kt */
    /* loaded from: classes3.dex */
    public static final class b {
        public static List<h> getVersionRequirements(g gVar) {
            d0.z.d.m.checkNotNullParameter(gVar, "this");
            return h.a.create(gVar.getProto(), gVar.getNameResolver(), gVar.getVersionRequirementTable());
        }
    }

    f getContainerSource();

    c getNameResolver();

    n getProto();

    d0.e0.p.d.m0.f.z.g getTypeTable();

    i getVersionRequirementTable();

    List<h> getVersionRequirements();
}
