package d0.e0.p.d.m0.l.b;

import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.d0;
import d0.e0.p.d.m0.c.e0;
import d0.e0.p.d.m0.c.f0;
import d0.e0.p.d.m0.c.g1.c;
import d0.e0.p.d.m0.c.h1.a;
import d0.e0.p.d.m0.c.h1.b;
import d0.e0.p.d.m0.c.h1.c;
import d0.e0.p.d.m0.c.h1.e;
import d0.e0.p.d.m0.f.z.i;
import d0.e0.p.d.m0.i.e;
import d0.e0.p.d.m0.k.v.g;
import d0.e0.p.d.m0.l.b.e0.f;
import d0.e0.p.d.m0.m.o;
import d0.e0.p.d.m0.n.l1.l;
import d0.t.n;
import d0.z.d.m;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: context.kt */
/* loaded from: classes3.dex */
public final class j {
    public final o a;

    /* renamed from: b  reason: collision with root package name */
    public final c0 f3475b;
    public final k c;
    public final g d;
    public final c<c, g<?>> e;
    public final f0 f;
    public final t g;
    public final p h;
    public final d0.e0.p.d.m0.d.b.c i;
    public final q j;
    public final Iterable<b> k;
    public final d0 l;
    public final i m;
    public final a n;
    public final d0.e0.p.d.m0.c.h1.c o;
    public final e p;
    public final l q;
    public final d0.e0.p.d.m0.k.z.a r;

    /* renamed from: s  reason: collision with root package name */
    public final d0.e0.p.d.m0.c.h1.e f3476s;
    public final h t;

    /* JADX WARN: Multi-variable type inference failed */
    public j(o oVar, c0 c0Var, k kVar, g gVar, c<? extends c, ? extends g<?>> cVar, f0 f0Var, t tVar, p pVar, d0.e0.p.d.m0.d.b.c cVar2, q qVar, Iterable<? extends b> iterable, d0 d0Var, i iVar, a aVar, d0.e0.p.d.m0.c.h1.c cVar3, e eVar, l lVar, d0.e0.p.d.m0.k.z.a aVar2, d0.e0.p.d.m0.c.h1.e eVar2) {
        m.checkNotNullParameter(oVar, "storageManager");
        m.checkNotNullParameter(c0Var, "moduleDescriptor");
        m.checkNotNullParameter(kVar, "configuration");
        m.checkNotNullParameter(gVar, "classDataFinder");
        m.checkNotNullParameter(cVar, "annotationAndConstantLoader");
        m.checkNotNullParameter(f0Var, "packageFragmentProvider");
        m.checkNotNullParameter(tVar, "localClassifierTypeSettings");
        m.checkNotNullParameter(pVar, "errorReporter");
        m.checkNotNullParameter(cVar2, "lookupTracker");
        m.checkNotNullParameter(qVar, "flexibleTypeDeserializer");
        m.checkNotNullParameter(iterable, "fictitiousClassDescriptorFactories");
        m.checkNotNullParameter(d0Var, "notFoundClasses");
        m.checkNotNullParameter(iVar, "contractDeserializer");
        m.checkNotNullParameter(aVar, "additionalClassPartsProvider");
        m.checkNotNullParameter(cVar3, "platformDependentDeclarationFilter");
        m.checkNotNullParameter(eVar, "extensionRegistryLite");
        m.checkNotNullParameter(lVar, "kotlinTypeChecker");
        m.checkNotNullParameter(aVar2, "samConversionResolver");
        m.checkNotNullParameter(eVar2, "platformDependentTypeTransformer");
        this.a = oVar;
        this.f3475b = c0Var;
        this.c = kVar;
        this.d = gVar;
        this.e = cVar;
        this.f = f0Var;
        this.g = tVar;
        this.h = pVar;
        this.i = cVar2;
        this.j = qVar;
        this.k = iterable;
        this.l = d0Var;
        this.m = iVar;
        this.n = aVar;
        this.o = cVar3;
        this.p = eVar;
        this.q = lVar;
        this.r = aVar2;
        this.f3476s = eVar2;
        this.t = new h(this);
    }

    public final l createContext(e0 e0Var, d0.e0.p.d.m0.f.z.c cVar, d0.e0.p.d.m0.f.z.g gVar, i iVar, d0.e0.p.d.m0.f.z.a aVar, f fVar) {
        m.checkNotNullParameter(e0Var, "descriptor");
        m.checkNotNullParameter(cVar, "nameResolver");
        m.checkNotNullParameter(gVar, "typeTable");
        m.checkNotNullParameter(iVar, "versionRequirementTable");
        m.checkNotNullParameter(aVar, "metadataVersion");
        return new l(this, cVar, e0Var, gVar, iVar, aVar, fVar, null, n.emptyList());
    }

    public final d0.e0.p.d.m0.c.e deserializeClass(d0.e0.p.d.m0.g.a aVar) {
        m.checkNotNullParameter(aVar, "classId");
        return h.deserializeClass$default(this.t, aVar, null, 2, null);
    }

    public final a getAdditionalClassPartsProvider() {
        return this.n;
    }

    public final c<c, g<?>> getAnnotationAndConstantLoader() {
        return this.e;
    }

    public final g getClassDataFinder() {
        return this.d;
    }

    public final h getClassDeserializer() {
        return this.t;
    }

    public final k getConfiguration() {
        return this.c;
    }

    public final i getContractDeserializer() {
        return this.m;
    }

    public final p getErrorReporter() {
        return this.h;
    }

    public final e getExtensionRegistryLite() {
        return this.p;
    }

    public final Iterable<b> getFictitiousClassDescriptorFactories() {
        return this.k;
    }

    public final q getFlexibleTypeDeserializer() {
        return this.j;
    }

    public final l getKotlinTypeChecker() {
        return this.q;
    }

    public final t getLocalClassifierTypeSettings() {
        return this.g;
    }

    public final d0.e0.p.d.m0.d.b.c getLookupTracker() {
        return this.i;
    }

    public final c0 getModuleDescriptor() {
        return this.f3475b;
    }

    public final d0 getNotFoundClasses() {
        return this.l;
    }

    public final f0 getPackageFragmentProvider() {
        return this.f;
    }

    public final d0.e0.p.d.m0.c.h1.c getPlatformDependentDeclarationFilter() {
        return this.o;
    }

    public final d0.e0.p.d.m0.c.h1.e getPlatformDependentTypeTransformer() {
        return this.f3476s;
    }

    public final o getStorageManager() {
        return this.a;
    }

    public /* synthetic */ j(o oVar, c0 c0Var, k kVar, g gVar, c cVar, f0 f0Var, t tVar, p pVar, d0.e0.p.d.m0.d.b.c cVar2, q qVar, Iterable iterable, d0 d0Var, i iVar, a aVar, d0.e0.p.d.m0.c.h1.c cVar3, e eVar, l lVar, d0.e0.p.d.m0.k.z.a aVar2, d0.e0.p.d.m0.c.h1.e eVar2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(oVar, c0Var, kVar, gVar, cVar, f0Var, tVar, pVar, cVar2, qVar, iterable, d0Var, iVar, (i & 8192) != 0 ? a.C0292a.a : aVar, (i & 16384) != 0 ? c.a.a : cVar3, eVar, (65536 & i) != 0 ? l.f3501b.getDefault() : lVar, aVar2, (i & 262144) != 0 ? e.a.a : eVar2);
    }
}
