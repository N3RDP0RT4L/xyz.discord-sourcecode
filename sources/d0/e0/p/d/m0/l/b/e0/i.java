package d0.e0.p.d.m0.l.b.e0;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.e0;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.g.a;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.a0.d;
import d0.t.n0;
import d0.t.r;
import d0.t.u;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import kotlin.jvm.functions.Function1;
/* compiled from: DeserializedPackageMemberScope.kt */
/* loaded from: classes3.dex */
public class i extends h {
    public final e0 g;
    public final b h;

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public i(d0.e0.p.d.m0.c.e0 r16, d0.e0.p.d.m0.f.l r17, d0.e0.p.d.m0.f.z.c r18, d0.e0.p.d.m0.f.z.a r19, d0.e0.p.d.m0.l.b.e0.f r20, d0.e0.p.d.m0.l.b.j r21, kotlin.jvm.functions.Function0<? extends java.util.Collection<d0.e0.p.d.m0.g.e>> r22) {
        /*
            r15 = this;
            r6 = r15
            r14 = r16
            java.lang.String r0 = "packageDescriptor"
            d0.z.d.m.checkNotNullParameter(r14, r0)
            java.lang.String r0 = "proto"
            r1 = r17
            d0.z.d.m.checkNotNullParameter(r1, r0)
            java.lang.String r0 = "nameResolver"
            r2 = r18
            d0.z.d.m.checkNotNullParameter(r2, r0)
            java.lang.String r0 = "metadataVersion"
            r3 = r19
            d0.z.d.m.checkNotNullParameter(r3, r0)
            java.lang.String r0 = "components"
            r4 = r21
            d0.z.d.m.checkNotNullParameter(r4, r0)
            java.lang.String r0 = "classNames"
            r5 = r22
            d0.z.d.m.checkNotNullParameter(r5, r0)
            d0.e0.p.d.m0.f.z.g r10 = new d0.e0.p.d.m0.f.z.g
            d0.e0.p.d.m0.f.t r0 = r17.getTypeTable()
            java.lang.String r7 = "proto.typeTable"
            d0.z.d.m.checkNotNullExpressionValue(r0, r7)
            r10.<init>(r0)
            d0.e0.p.d.m0.f.z.i$a r0 = d0.e0.p.d.m0.f.z.i.a
            d0.e0.p.d.m0.f.w r7 = r17.getVersionRequirementTable()
            java.lang.String r8 = "proto.versionRequirementTable"
            d0.z.d.m.checkNotNullExpressionValue(r7, r8)
            d0.e0.p.d.m0.f.z.i r11 = r0.create(r7)
            r7 = r21
            r8 = r16
            r9 = r18
            r12 = r19
            r13 = r20
            d0.e0.p.d.m0.l.b.l r2 = r7.createContext(r8, r9, r10, r11, r12, r13)
            java.util.List r3 = r17.getFunctionList()
            java.lang.String r0 = "proto.functionList"
            d0.z.d.m.checkNotNullExpressionValue(r3, r0)
            java.util.List r4 = r17.getPropertyList()
            java.lang.String r0 = "proto.propertyList"
            d0.z.d.m.checkNotNullExpressionValue(r4, r0)
            java.util.List r7 = r17.getTypeAliasList()
            java.lang.String r0 = "proto.typeAliasList"
            d0.z.d.m.checkNotNullExpressionValue(r7, r0)
            r0 = r15
            r1 = r2
            r2 = r3
            r3 = r4
            r4 = r7
            r0.<init>(r1, r2, r3, r4, r5)
            r6.g = r14
            d0.e0.p.d.m0.g.b r0 = r16.getFqName()
            r6.h = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.l.b.e0.i.<init>(d0.e0.p.d.m0.c.e0, d0.e0.p.d.m0.f.l, d0.e0.p.d.m0.f.z.c, d0.e0.p.d.m0.f.z.a, d0.e0.p.d.m0.l.b.e0.f, d0.e0.p.d.m0.l.b.j, kotlin.jvm.functions.Function0):void");
    }

    @Override // d0.e0.p.d.m0.l.b.e0.h
    public void a(Collection<m> collection, Function1<? super e, Boolean> function1) {
        d0.z.d.m.checkNotNullParameter(collection, "result");
        d0.z.d.m.checkNotNullParameter(function1, "nameFilter");
    }

    @Override // d0.e0.p.d.m0.l.b.e0.h
    public a e(e eVar) {
        d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        return new a(this.h, eVar);
    }

    @Override // d0.e0.p.d.m0.l.b.e0.h
    public Set<e> f() {
        return n0.emptySet();
    }

    @Override // d0.e0.p.d.m0.l.b.e0.h
    public Set<e> g() {
        return n0.emptySet();
    }

    @Override // d0.e0.p.d.m0.l.b.e0.h, d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.l
    public h getContributedClassifier(e eVar, d0.e0.p.d.m0.d.b.b bVar) {
        d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        d0.z.d.m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        recordLookup(eVar, bVar);
        return super.getContributedClassifier(eVar, bVar);
    }

    @Override // d0.e0.p.d.m0.l.b.e0.h
    public Set<e> h() {
        return n0.emptySet();
    }

    @Override // d0.e0.p.d.m0.l.b.e0.h
    public boolean i(e eVar) {
        boolean z2;
        d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        if (getClassNames$deserialization().contains(eVar)) {
            return true;
        }
        Iterable<d0.e0.p.d.m0.c.h1.b> fictitiousClassDescriptorFactories = this.c.getComponents().getFictitiousClassDescriptorFactories();
        if (!(fictitiousClassDescriptorFactories instanceof Collection) || !((Collection) fictitiousClassDescriptorFactories).isEmpty()) {
            for (d0.e0.p.d.m0.c.h1.b bVar : fictitiousClassDescriptorFactories) {
                if (bVar.shouldCreateClass(this.h, eVar)) {
                    z2 = true;
                    break;
                }
            }
        }
        z2 = false;
        return z2;
    }

    public void recordLookup(e eVar, d0.e0.p.d.m0.d.b.b bVar) {
        d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        d0.z.d.m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        d0.e0.p.d.m0.d.a.record(this.c.getComponents().getLookupTracker(), bVar, this.g, eVar);
    }

    @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.l
    public List<m> getContributedDescriptors(d dVar, Function1<? super e, Boolean> function1) {
        d0.z.d.m.checkNotNullParameter(dVar, "kindFilter");
        d0.z.d.m.checkNotNullParameter(function1, "nameFilter");
        Collection<m> b2 = b(dVar, function1, d0.e0.p.d.m0.d.b.d.WHEN_GET_ALL_DESCRIPTORS);
        Iterable<d0.e0.p.d.m0.c.h1.b> fictitiousClassDescriptorFactories = this.c.getComponents().getFictitiousClassDescriptorFactories();
        ArrayList arrayList = new ArrayList();
        for (d0.e0.p.d.m0.c.h1.b bVar : fictitiousClassDescriptorFactories) {
            r.addAll(arrayList, bVar.getAllContributedClassesIfPossible(this.h));
        }
        return u.plus((Collection) b2, (Iterable) arrayList);
    }
}
