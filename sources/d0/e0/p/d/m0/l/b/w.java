package d0.e0.p.d.m0.l.b;

import d0.e0.p.d.m0.f.z.c;
import d0.e0.p.d.m0.g.a;
import d0.e0.p.d.m0.g.e;
import d0.z.d.m;
/* compiled from: NameResolverUtil.kt */
/* loaded from: classes3.dex */
public final class w {
    public static final a getClassId(c cVar, int i) {
        m.checkNotNullParameter(cVar, "<this>");
        a fromString = a.fromString(cVar.getQualifiedClassName(i), cVar.isLocalClassName(i));
        m.checkNotNullExpressionValue(fromString, "fromString(getQualifiedClassName(index), isLocalClassName(index))");
        return fromString;
    }

    public static final e getName(c cVar, int i) {
        m.checkNotNullParameter(cVar, "<this>");
        e guessByFirstCharacter = e.guessByFirstCharacter(cVar.getString(i));
        m.checkNotNullExpressionValue(guessByFirstCharacter, "guessByFirstCharacter(getString(index))");
        return guessByFirstCharacter;
    }
}
