package d0.e0.p.d.m0.l.b;

import d0.e0.p.d.m0.b.k;
import d0.e0.p.d.m0.c.e;
import d0.t.m0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Set;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ClassDeserializer.kt */
/* loaded from: classes3.dex */
public final class h {
    public static final b a = new b(null);

    /* renamed from: b  reason: collision with root package name */
    public static final Set<d0.e0.p.d.m0.g.a> f3472b = m0.setOf(d0.e0.p.d.m0.g.a.topLevel(k.a.d.toSafe()));
    public final j c;
    public final Function1<a, e> d;

    /* compiled from: ClassDeserializer.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public final d0.e0.p.d.m0.g.a a;

        /* renamed from: b  reason: collision with root package name */
        public final f f3473b;

        public a(d0.e0.p.d.m0.g.a aVar, f fVar) {
            m.checkNotNullParameter(aVar, "classId");
            this.a = aVar;
            this.f3473b = fVar;
        }

        public boolean equals(Object obj) {
            return (obj instanceof a) && m.areEqual(this.a, ((a) obj).a);
        }

        public final f getClassData() {
            return this.f3473b;
        }

        public final d0.e0.p.d.m0.g.a getClassId() {
            return this.a;
        }

        public int hashCode() {
            return this.a.hashCode();
        }
    }

    /* compiled from: ClassDeserializer.kt */
    /* loaded from: classes3.dex */
    public static final class b {
        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final Set<d0.e0.p.d.m0.g.a> getBLACK_LIST() {
            return h.f3472b;
        }
    }

    /* compiled from: ClassDeserializer.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function1<a, e> {
        public c() {
            super(1);
        }

        public final e invoke(a aVar) {
            m.checkNotNullParameter(aVar, "key");
            return h.access$createClass(h.this, aVar);
        }
    }

    public h(j jVar) {
        m.checkNotNullParameter(jVar, "components");
        this.c = jVar;
        this.d = jVar.getStorageManager().createMemoizedFunctionWithNullableValues(new c());
    }

    /* JADX WARN: Removed duplicated region for block: B:41:0x00c4 A[EDGE_INSN: B:41:0x00c4->B:34:0x00c4 ?: BREAK  , SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final d0.e0.p.d.m0.c.e access$createClass(d0.e0.p.d.m0.l.b.h r12, d0.e0.p.d.m0.l.b.h.a r13) {
        /*
            Method dump skipped, instructions count: 252
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.l.b.h.access$createClass(d0.e0.p.d.m0.l.b.h, d0.e0.p.d.m0.l.b.h$a):d0.e0.p.d.m0.c.e");
    }

    public static /* synthetic */ e deserializeClass$default(h hVar, d0.e0.p.d.m0.g.a aVar, f fVar, int i, Object obj) {
        if ((i & 2) != 0) {
            fVar = null;
        }
        return hVar.deserializeClass(aVar, fVar);
    }

    public final e deserializeClass(d0.e0.p.d.m0.g.a aVar, f fVar) {
        m.checkNotNullParameter(aVar, "classId");
        return this.d.invoke(new a(aVar, fVar));
    }
}
