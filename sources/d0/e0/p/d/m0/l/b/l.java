package d0.e0.p.d.m0.l.b;

import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.f.s;
import d0.e0.p.d.m0.f.z.a;
import d0.e0.p.d.m0.f.z.c;
import d0.e0.p.d.m0.f.z.g;
import d0.e0.p.d.m0.f.z.i;
import d0.e0.p.d.m0.f.z.j;
import d0.e0.p.d.m0.l.b.e0.f;
import d0.e0.p.d.m0.m.o;
import java.util.List;
/* compiled from: context.kt */
/* loaded from: classes3.dex */
public final class l {
    public final j a;

    /* renamed from: b  reason: collision with root package name */
    public final c f3477b;
    public final m c;
    public final g d;
    public final i e;
    public final a f;
    public final f g;
    public final c0 h;
    public final u i;

    public l(j jVar, c cVar, m mVar, g gVar, i iVar, a aVar, f fVar, c0 c0Var, List<s> list) {
        String presentableString;
        d0.z.d.m.checkNotNullParameter(jVar, "components");
        d0.z.d.m.checkNotNullParameter(cVar, "nameResolver");
        d0.z.d.m.checkNotNullParameter(mVar, "containingDeclaration");
        d0.z.d.m.checkNotNullParameter(gVar, "typeTable");
        d0.z.d.m.checkNotNullParameter(iVar, "versionRequirementTable");
        d0.z.d.m.checkNotNullParameter(aVar, "metadataVersion");
        d0.z.d.m.checkNotNullParameter(list, "typeParameters");
        this.a = jVar;
        this.f3477b = cVar;
        this.c = mVar;
        this.d = gVar;
        this.e = iVar;
        this.f = aVar;
        this.g = fVar;
        StringBuilder R = b.d.b.a.a.R("Deserializer for \"");
        R.append(mVar.getName());
        R.append('\"');
        String sb = R.toString();
        String str = "[container not found]";
        if (fVar != null && (presentableString = fVar.getPresentableString()) != null) {
            str = presentableString;
        }
        this.h = new c0(this, c0Var, list, sb, str, false, 32, null);
        this.i = new u(this);
    }

    public static /* synthetic */ l childContext$default(l lVar, m mVar, List list, c cVar, g gVar, i iVar, a aVar, int i, Object obj) {
        if ((i & 4) != 0) {
            cVar = lVar.f3477b;
        }
        c cVar2 = cVar;
        if ((i & 8) != 0) {
            gVar = lVar.d;
        }
        g gVar2 = gVar;
        if ((i & 16) != 0) {
            iVar = lVar.e;
        }
        i iVar2 = iVar;
        if ((i & 32) != 0) {
            aVar = lVar.f;
        }
        return lVar.childContext(mVar, list, cVar2, gVar2, iVar2, aVar);
    }

    public final l childContext(m mVar, List<s> list, c cVar, g gVar, i iVar, a aVar) {
        d0.z.d.m.checkNotNullParameter(mVar, "descriptor");
        d0.z.d.m.checkNotNullParameter(list, "typeParameterProtos");
        d0.z.d.m.checkNotNullParameter(cVar, "nameResolver");
        d0.z.d.m.checkNotNullParameter(gVar, "typeTable");
        i iVar2 = iVar;
        d0.z.d.m.checkNotNullParameter(iVar2, "versionRequirementTable");
        d0.z.d.m.checkNotNullParameter(aVar, "metadataVersion");
        j jVar = this.a;
        if (!j.isVersionRequirementTableWrittenCorrectly(aVar)) {
            iVar2 = this.e;
        }
        return new l(jVar, cVar, mVar, gVar, iVar2, aVar, this.g, this.h, list);
    }

    public final j getComponents() {
        return this.a;
    }

    public final f getContainerSource() {
        return this.g;
    }

    public final m getContainingDeclaration() {
        return this.c;
    }

    public final u getMemberDeserializer() {
        return this.i;
    }

    public final c getNameResolver() {
        return this.f3477b;
    }

    public final o getStorageManager() {
        return this.a.getStorageManager();
    }

    public final c0 getTypeDeserializer() {
        return this.h;
    }

    public final g getTypeTable() {
        return this.d;
    }

    public final i getVersionRequirementTable() {
        return this.e;
    }
}
