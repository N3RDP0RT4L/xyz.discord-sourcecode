package d0.e0.p.d.m0.l.b;

import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.f.l;
import d0.e0.p.d.m0.f.m;
import d0.e0.p.d.m0.f.p;
import d0.e0.p.d.m0.f.z.d;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.l.b.e0.f;
import java.util.ArrayList;
import java.util.Collection;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: DeserializedPackageFragmentImpl.kt */
/* loaded from: classes3.dex */
public abstract class o extends n {
    public final d0.e0.p.d.m0.f.z.a p;
    public final f q;
    public final d r;

    /* renamed from: s  reason: collision with root package name */
    public final x f3478s;
    public m t;
    public i u;

    /* compiled from: DeserializedPackageFragmentImpl.kt */
    /* loaded from: classes3.dex */
    public static final class a extends d0.z.d.o implements Function1<d0.e0.p.d.m0.g.a, u0> {
        public a() {
            super(1);
        }

        public final u0 invoke(d0.e0.p.d.m0.g.a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "it");
            f fVar = o.this.q;
            if (fVar != null) {
                return fVar;
            }
            u0 u0Var = u0.a;
            d0.z.d.m.checkNotNullExpressionValue(u0Var, "NO_SOURCE");
            return u0Var;
        }
    }

    /* compiled from: DeserializedPackageFragmentImpl.kt */
    /* loaded from: classes3.dex */
    public static final class b extends d0.z.d.o implements Function0<Collection<? extends e>> {
        public b() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final Collection<? extends e> invoke() {
            Collection<d0.e0.p.d.m0.g.a> allClassIds = o.this.getClassDataFinder().getAllClassIds();
            ArrayList<d0.e0.p.d.m0.g.a> arrayList = new ArrayList();
            for (Object obj : allClassIds) {
                d0.e0.p.d.m0.g.a aVar = (d0.e0.p.d.m0.g.a) obj;
                if (!aVar.isNestedClass() && !h.a.getBLACK_LIST().contains(aVar)) {
                    arrayList.add(obj);
                }
            }
            ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(arrayList, 10));
            for (d0.e0.p.d.m0.g.a aVar2 : arrayList) {
                arrayList2.add(aVar2.getShortClassName());
            }
            return arrayList2;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public o(d0.e0.p.d.m0.g.b bVar, d0.e0.p.d.m0.m.o oVar, c0 c0Var, m mVar, d0.e0.p.d.m0.f.z.a aVar, f fVar) {
        super(bVar, oVar, c0Var);
        d0.z.d.m.checkNotNullParameter(bVar, "fqName");
        d0.z.d.m.checkNotNullParameter(oVar, "storageManager");
        d0.z.d.m.checkNotNullParameter(c0Var, "module");
        d0.z.d.m.checkNotNullParameter(mVar, "proto");
        d0.z.d.m.checkNotNullParameter(aVar, "metadataVersion");
        this.p = aVar;
        this.q = fVar;
        p strings = mVar.getStrings();
        d0.z.d.m.checkNotNullExpressionValue(strings, "proto.strings");
        d0.e0.p.d.m0.f.o qualifiedNames = mVar.getQualifiedNames();
        d0.z.d.m.checkNotNullExpressionValue(qualifiedNames, "proto.qualifiedNames");
        d dVar = new d(strings, qualifiedNames);
        this.r = dVar;
        this.f3478s = new x(mVar, dVar, aVar, new a());
        this.t = mVar;
    }

    @Override // d0.e0.p.d.m0.c.e0
    public i getMemberScope() {
        i iVar = this.u;
        if (iVar != null) {
            return iVar;
        }
        d0.z.d.m.throwUninitializedPropertyAccessException("_memberScope");
        throw null;
    }

    public void initialize(j jVar) {
        d0.z.d.m.checkNotNullParameter(jVar, "components");
        m mVar = this.t;
        if (mVar != null) {
            this.t = null;
            l lVar = mVar.getPackage();
            d0.z.d.m.checkNotNullExpressionValue(lVar, "proto.`package`");
            this.u = new d0.e0.p.d.m0.l.b.e0.i(this, lVar, this.r, this.p, this.q, jVar, new b());
            return;
        }
        throw new IllegalStateException("Repeated call to DeserializedPackageFragmentImpl::initialize".toString());
    }

    @Override // d0.e0.p.d.m0.l.b.n
    public x getClassDataFinder() {
        return this.f3478s;
    }
}
