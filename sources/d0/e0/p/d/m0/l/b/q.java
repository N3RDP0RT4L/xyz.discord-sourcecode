package d0.e0.p.d.m0.l.b;

import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.j0;
import d0.z.d.m;
/* compiled from: FlexibleTypeDeserializer.kt */
/* loaded from: classes3.dex */
public interface q {

    /* compiled from: FlexibleTypeDeserializer.kt */
    /* loaded from: classes3.dex */
    public static final class a implements q {
        public static final a a = new a();

        @Override // d0.e0.p.d.m0.l.b.q
        public c0 create(d0.e0.p.d.m0.f.q qVar, String str, j0 j0Var, j0 j0Var2) {
            m.checkNotNullParameter(qVar, "proto");
            m.checkNotNullParameter(str, "flexibleId");
            m.checkNotNullParameter(j0Var, "lowerBound");
            m.checkNotNullParameter(j0Var2, "upperBound");
            throw new IllegalArgumentException("This method should not be used.");
        }
    }

    c0 create(d0.e0.p.d.m0.f.q qVar, String str, j0 j0Var, j0 j0Var2);
}
