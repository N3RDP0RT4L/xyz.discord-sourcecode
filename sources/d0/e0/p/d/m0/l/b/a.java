package d0.e0.p.d.m0.l.b;

import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.e0;
import d0.e0.p.d.m0.c.i0;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.l.b.d0.c;
import d0.e0.p.d.m0.m.i;
import d0.e0.p.d.m0.m.o;
import d0.t.n;
import d0.t.n0;
import d0.z.d.m;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import kotlin.jvm.functions.Function1;
/* compiled from: AbstractDeserializedPackageFragmentProvider.kt */
/* loaded from: classes3.dex */
public abstract class a implements i0 {
    public final o a;

    /* renamed from: b  reason: collision with root package name */
    public final s f3453b;
    public final c0 c;
    public j d;
    public final i<b, e0> e;

    /* compiled from: AbstractDeserializedPackageFragmentProvider.kt */
    /* renamed from: d0.e0.p.d.m0.l.b.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0348a extends d0.z.d.o implements Function1<b, e0> {
        public C0348a() {
            super(1);
        }

        public final e0 invoke(b bVar) {
            m.checkNotNullParameter(bVar, "fqName");
            d0.e0.p.d.m0.b.q.o oVar = (d0.e0.p.d.m0.b.q.o) a.this;
            Objects.requireNonNull(oVar);
            m.checkNotNullParameter(bVar, "fqName");
            InputStream findBuiltInsData = oVar.f3453b.findBuiltInsData(bVar);
            c create = findBuiltInsData == null ? null : c.v.create(bVar, oVar.a, oVar.c, findBuiltInsData, false);
            if (create == null) {
                return null;
            }
            j jVar = a.this.d;
            if (jVar != null) {
                create.initialize(jVar);
                return create;
            }
            m.throwUninitializedPropertyAccessException("components");
            throw null;
        }
    }

    public a(o oVar, s sVar, c0 c0Var) {
        m.checkNotNullParameter(oVar, "storageManager");
        m.checkNotNullParameter(sVar, "finder");
        m.checkNotNullParameter(c0Var, "moduleDescriptor");
        this.a = oVar;
        this.f3453b = sVar;
        this.c = c0Var;
        this.e = oVar.createMemoizedFunctionWithNullableValues(new C0348a());
    }

    @Override // d0.e0.p.d.m0.c.i0
    public void collectPackageFragments(b bVar, Collection<e0> collection) {
        m.checkNotNullParameter(bVar, "fqName");
        m.checkNotNullParameter(collection, "packageFragments");
        d0.e0.p.d.m0.p.a.addIfNotNull(collection, this.e.invoke(bVar));
    }

    @Override // d0.e0.p.d.m0.c.f0
    public List<e0> getPackageFragments(b bVar) {
        m.checkNotNullParameter(bVar, "fqName");
        return n.listOfNotNull(this.e.invoke(bVar));
    }

    @Override // d0.e0.p.d.m0.c.f0
    public Collection<b> getSubPackagesOf(b bVar, Function1<? super e, Boolean> function1) {
        m.checkNotNullParameter(bVar, "fqName");
        m.checkNotNullParameter(function1, "nameFilter");
        return n0.emptySet();
    }
}
