package d0.e0.p.d.m0.l.b;

import d0.e0.p.d.m0.b.l;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.w;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.f.q;
import d0.e0.p.d.m0.f.s;
import d0.e0.p.d.m0.n.d0;
import d0.e0.p.d.m0.n.e0;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.j1;
import d0.e0.p.d.m0.n.m0;
import d0.e0.p.d.m0.n.n0;
import d0.e0.p.d.m0.n.o0;
import d0.e0.p.d.m0.n.t;
import d0.e0.p.d.m0.n.u0;
import d0.e0.p.d.m0.n.w0;
import d0.e0.p.d.m0.n.y0;
import d0.t.h0;
import d0.t.n;
import d0.t.u;
import d0.z.d.a0;
import d0.z.d.j;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KDeclarationContainer;
/* compiled from: TypeDeserializer.kt */
/* loaded from: classes3.dex */
public final class c0 {
    public final l a;

    /* renamed from: b  reason: collision with root package name */
    public final c0 f3455b;
    public final String c;
    public final String d;
    public boolean e;
    public final Function1<Integer, h> f;
    public final Function1<Integer, h> g;
    public final Map<Integer, z0> h;

    /* compiled from: TypeDeserializer.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<Integer, h> {
        public a() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ h invoke(Integer num) {
            return invoke(num.intValue());
        }

        public final h invoke(int i) {
            return c0.access$computeClassifierDescriptor(c0.this, i);
        }
    }

    /* compiled from: TypeDeserializer.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function0<List<? extends d0.e0.p.d.m0.c.g1.c>> {
        public final /* synthetic */ q $proto;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(q qVar) {
            super(0);
            this.$proto = qVar;
        }

        @Override // kotlin.jvm.functions.Function0
        public final List<? extends d0.e0.p.d.m0.c.g1.c> invoke() {
            return c0.this.a.getComponents().getAnnotationAndConstantLoader().loadTypeAnnotations(this.$proto, c0.this.a.getNameResolver());
        }
    }

    /* compiled from: TypeDeserializer.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function1<Integer, h> {
        public c() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ h invoke(Integer num) {
            return invoke(num.intValue());
        }

        public final h invoke(int i) {
            return c0.access$computeTypeAliasDescriptor(c0.this, i);
        }
    }

    /* compiled from: TypeDeserializer.kt */
    /* loaded from: classes3.dex */
    public /* synthetic */ class d extends j implements Function1<d0.e0.p.d.m0.g.a, d0.e0.p.d.m0.g.a> {
        public static final d j = new d();

        public d() {
            super(1);
        }

        @Override // d0.z.d.d, kotlin.reflect.KCallable
        public final String getName() {
            return "getOuterClassId";
        }

        @Override // d0.z.d.d
        public final KDeclarationContainer getOwner() {
            return a0.getOrCreateKotlinClass(d0.e0.p.d.m0.g.a.class);
        }

        @Override // d0.z.d.d
        public final String getSignature() {
            return "getOuterClassId()Lorg/jetbrains/kotlin/name/ClassId;";
        }

        public final d0.e0.p.d.m0.g.a invoke(d0.e0.p.d.m0.g.a aVar) {
            m.checkNotNullParameter(aVar, "p0");
            return aVar.getOuterClassId();
        }
    }

    /* compiled from: TypeDeserializer.kt */
    /* loaded from: classes3.dex */
    public static final class e extends o implements Function1<q, q> {
        public e() {
            super(1);
        }

        public final q invoke(q qVar) {
            m.checkNotNullParameter(qVar, "it");
            return d0.e0.p.d.m0.f.z.f.outerType(qVar, c0.this.a.getTypeTable());
        }
    }

    /* compiled from: TypeDeserializer.kt */
    /* loaded from: classes3.dex */
    public static final class f extends o implements Function1<q, Integer> {
        public static final f j = new f();

        public f() {
            super(1);
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final int invoke2(q qVar) {
            m.checkNotNullParameter(qVar, "it");
            return qVar.getArgumentCount();
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Integer invoke(q qVar) {
            return Integer.valueOf(invoke2(qVar));
        }
    }

    public c0(l lVar, c0 c0Var, List<s> list, String str, String str2, boolean z2) {
        Map<Integer, z0> map;
        m.checkNotNullParameter(lVar, "c");
        m.checkNotNullParameter(list, "typeParameterProtos");
        m.checkNotNullParameter(str, "debugName");
        m.checkNotNullParameter(str2, "containerPresentableName");
        this.a = lVar;
        this.f3455b = c0Var;
        this.c = str;
        this.d = str2;
        this.e = z2;
        this.f = lVar.getStorageManager().createMemoizedFunctionWithNullableValues(new a());
        this.g = lVar.getStorageManager().createMemoizedFunctionWithNullableValues(new c());
        if (list.isEmpty()) {
            map = h0.emptyMap();
        } else {
            map = new LinkedHashMap<>();
            int i = 0;
            for (s sVar : list) {
                i++;
                map.put(Integer.valueOf(sVar.getId()), new d0.e0.p.d.m0.l.b.e0.m(this.a, sVar, i));
            }
        }
        this.h = map;
    }

    public static final h access$computeClassifierDescriptor(c0 c0Var, int i) {
        d0.e0.p.d.m0.g.a classId = w.getClassId(c0Var.a.getNameResolver(), i);
        if (classId.isLocal()) {
            return c0Var.a.getComponents().deserializeClass(classId);
        }
        return w.findClassifierAcrossModuleDependencies(c0Var.a.getComponents().getModuleDescriptor(), classId);
    }

    public static final h access$computeTypeAliasDescriptor(c0 c0Var, int i) {
        d0.e0.p.d.m0.g.a classId = w.getClassId(c0Var.a.getNameResolver(), i);
        if (classId.isLocal()) {
            return null;
        }
        return w.findTypeAliasAcrossModuleDependencies(c0Var.a.getComponents().getModuleDescriptor(), classId);
    }

    public static final List<q.b> c(q qVar, c0 c0Var) {
        List<q.b> argumentList = qVar.getArgumentList();
        m.checkNotNullExpressionValue(argumentList, "argumentList");
        q outerType = d0.e0.p.d.m0.f.z.f.outerType(qVar, c0Var.a.getTypeTable());
        List<q.b> c2 = outerType == null ? null : c(outerType, c0Var);
        if (c2 == null) {
            c2 = n.emptyList();
        }
        return u.plus((Collection) argumentList, (Iterable) c2);
    }

    public static final d0.e0.p.d.m0.c.e d(c0 c0Var, q qVar, int i) {
        d0.e0.p.d.m0.g.a classId = w.getClassId(c0Var.a.getNameResolver(), i);
        List<Integer> mutableList = d0.f0.q.toMutableList(d0.f0.q.map(d0.f0.n.generateSequence(qVar, new e()), f.j));
        int count = d0.f0.q.count(d0.f0.n.generateSequence(classId, d.j));
        while (mutableList.size() < count) {
            mutableList.add(0);
        }
        return c0Var.a.getComponents().getNotFoundClasses().getClass(classId, mutableList);
    }

    public static /* synthetic */ j0 simpleType$default(c0 c0Var, q qVar, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = true;
        }
        return c0Var.simpleType(qVar, z2);
    }

    public final j0 a(int i) {
        if (w.getClassId(this.a.getNameResolver(), i).isLocal()) {
            return this.a.getComponents().getLocalClassifierTypeSettings().getReplacementTypeForLocalClassifiers();
        }
        return null;
    }

    public final j0 b(d0.e0.p.d.m0.n.c0 c0Var, d0.e0.p.d.m0.n.c0 c0Var2) {
        d0.e0.p.d.m0.b.h builtIns = d0.e0.p.d.m0.n.o1.a.getBuiltIns(c0Var);
        g annotations = c0Var.getAnnotations();
        d0.e0.p.d.m0.n.c0 receiverTypeFromFunctionType = d0.e0.p.d.m0.b.g.getReceiverTypeFromFunctionType(c0Var);
        List<w0> dropLast = u.dropLast(d0.e0.p.d.m0.b.g.getValueParameterTypesFromFunctionType(c0Var), 1);
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(dropLast, 10));
        for (w0 w0Var : dropLast) {
            arrayList.add(w0Var.getType());
        }
        return d0.e0.p.d.m0.b.g.createFunctionType(builtIns, annotations, receiverTypeFromFunctionType, arrayList, null, c0Var2, true).makeNullableAsSpecified(c0Var.isMarkedNullable());
    }

    public final u0 e(int i) {
        z0 z0Var = this.h.get(Integer.valueOf(i));
        u0 typeConstructor = z0Var == null ? null : z0Var.getTypeConstructor();
        if (typeConstructor != null) {
            return typeConstructor;
        }
        c0 c0Var = this.f3455b;
        if (c0Var == null) {
            return null;
        }
        return c0Var.e(i);
    }

    public final boolean getExperimentalSuspendFunctionTypeEncountered() {
        return this.e;
    }

    public final List<z0> getOwnTypeParameters() {
        return u.toList(this.h.values());
    }

    public final j0 simpleType(q qVar, boolean z2) {
        j0 j0Var;
        u0 u0Var;
        j0 j0Var2;
        j0 withAbbreviation;
        int size;
        Object obj;
        Object obj2;
        m.checkNotNullParameter(qVar, "proto");
        j0 j0Var3 = null;
        j0Var3 = null;
        j0Var3 = null;
        d0.e0.p.d.m0.g.b bVar = null;
        j0Var3 = null;
        if (qVar.hasClassName()) {
            j0Var = a(qVar.getClassName());
        } else {
            j0Var = qVar.hasTypeAliasName() ? a(qVar.getTypeAliasName()) : null;
        }
        if (j0Var != null) {
            return j0Var;
        }
        if (qVar.hasClassName()) {
            h invoke = this.f.invoke(Integer.valueOf(qVar.getClassName()));
            if (invoke == null) {
                invoke = d(this, qVar, qVar.getClassName());
            }
            u0Var = invoke.getTypeConstructor();
            m.checkNotNullExpressionValue(u0Var, "classifierDescriptors(proto.className) ?: notFoundClass(proto.className)).typeConstructor");
        } else if (qVar.hasTypeParameter()) {
            u0Var = e(qVar.getTypeParameter());
            if (u0Var == null) {
                StringBuilder R = b.d.b.a.a.R("Unknown type parameter ");
                R.append(qVar.getTypeParameter());
                R.append(". Please try recompiling module containing \"");
                R.append(this.d);
                R.append('\"');
                u0Var = t.createErrorTypeConstructor(R.toString());
                m.checkNotNullExpressionValue(u0Var, "createErrorTypeConstructor(\n                        \"Unknown type parameter ${proto.typeParameter}. Please try recompiling module containing \\\"$containerPresentableName\\\"\"\n                    )");
            }
        } else if (qVar.hasTypeParameterName()) {
            d0.e0.p.d.m0.c.m containingDeclaration = this.a.getContainingDeclaration();
            String string = this.a.getNameResolver().getString(qVar.getTypeParameterName());
            Iterator<T> it = getOwnTypeParameters().iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj2 = null;
                    break;
                }
                obj2 = it.next();
                if (m.areEqual(((z0) obj2).getName().asString(), string)) {
                    break;
                }
            }
            z0 z0Var = (z0) obj2;
            u0 typeConstructor = z0Var == null ? null : z0Var.getTypeConstructor();
            u0Var = typeConstructor == null ? t.createErrorTypeConstructor("Deserialized type parameter " + string + " in " + containingDeclaration) : typeConstructor;
            m.checkNotNullExpressionValue(u0Var, "{\n                val container = c.containingDeclaration\n                val name = c.nameResolver.getString(proto.typeParameterName)\n                val parameter = ownTypeParameters.find { it.name.asString() == name }\n                parameter?.typeConstructor ?: ErrorUtils.createErrorTypeConstructor(\"Deserialized type parameter $name in $container\")\n            }");
        } else if (qVar.hasTypeAliasName()) {
            h invoke2 = this.g.invoke(Integer.valueOf(qVar.getTypeAliasName()));
            if (invoke2 == null) {
                invoke2 = d(this, qVar, qVar.getTypeAliasName());
            }
            u0Var = invoke2.getTypeConstructor();
            m.checkNotNullExpressionValue(u0Var, "typeAliasDescriptors(proto.typeAliasName) ?: notFoundClass(proto.typeAliasName)).typeConstructor");
        } else {
            u0Var = t.createErrorTypeConstructor("Unknown type");
            m.checkNotNullExpressionValue(u0Var, "createErrorTypeConstructor(\"Unknown type\")");
        }
        if (t.isError(u0Var.getDeclarationDescriptor())) {
            j0 createErrorTypeWithCustomConstructor = t.createErrorTypeWithCustomConstructor(u0Var.toString(), u0Var);
            m.checkNotNullExpressionValue(createErrorTypeWithCustomConstructor, "createErrorTypeWithCustomConstructor(constructor.toString(), constructor)");
            return createErrorTypeWithCustomConstructor;
        }
        d0.e0.p.d.m0.l.b.e0.a aVar = new d0.e0.p.d.m0.l.b.e0.a(this.a.getStorageManager(), new b(qVar));
        List<q.b> c2 = c(qVar, this);
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(c2, 10));
        int i = 0;
        for (Object obj3 : c2) {
            i++;
            if (i < 0) {
                n.throwIndexOverflow();
            }
            q.b bVar2 = (q.b) obj3;
            List<z0> parameters = u0Var.getParameters();
            m.checkNotNullExpressionValue(parameters, "constructor.parameters");
            z0 z0Var2 = (z0) u.getOrNull(parameters, i);
            if (bVar2.getProjection() != q.b.c.STAR) {
                z zVar = z.a;
                q.b.c projection = bVar2.getProjection();
                m.checkNotNullExpressionValue(projection, "typeArgumentProto.projection");
                j1 variance = zVar.variance(projection);
                q type = d0.e0.p.d.m0.f.z.f.type(bVar2, this.a.getTypeTable());
                obj = type == null ? new y0(t.createErrorType("No type recorded")) : new y0(variance, type(type));
            } else if (z0Var2 == null) {
                obj = new n0(this.a.getComponents().getModuleDescriptor().getBuiltIns());
            } else {
                obj = new o0(z0Var2);
            }
            arrayList.add(obj);
        }
        List list = u.toList(arrayList);
        h declarationDescriptor = u0Var.getDeclarationDescriptor();
        boolean z3 = true;
        if (!z2 || !(declarationDescriptor instanceof d0.e0.p.d.m0.c.y0)) {
            Boolean bool = d0.e0.p.d.m0.f.z.b.a.get(qVar.getFlags());
            m.checkNotNullExpressionValue(bool, "SUSPEND_TYPE.get(proto.flags)");
            if (bool.booleanValue()) {
                boolean nullable = qVar.getNullable();
                int size2 = u0Var.getParameters().size() - list.size();
                if (size2 == 0) {
                    d0 d0Var = d0.a;
                    j0 simpleType$default = d0.simpleType$default(aVar, u0Var, list, nullable, null, 16, null);
                    if (d0.e0.p.d.m0.b.g.isFunctionType(simpleType$default)) {
                        boolean releaseCoroutines = this.a.getComponents().getConfiguration().getReleaseCoroutines();
                        w0 w0Var = (w0) u.lastOrNull((List<? extends Object>) d0.e0.p.d.m0.b.g.getValueParameterTypesFromFunctionType(simpleType$default));
                        d0.e0.p.d.m0.n.c0 type2 = w0Var == null ? null : w0Var.getType();
                        if (type2 != null) {
                            h declarationDescriptor2 = type2.getConstructor().getDeclarationDescriptor();
                            d0.e0.p.d.m0.g.b fqNameSafe = declarationDescriptor2 == null ? null : d0.e0.p.d.m0.k.x.a.getFqNameSafe(declarationDescriptor2);
                            if (type2.getArguments().size() == 1 && (l.isContinuation(fqNameSafe, true) || l.isContinuation(fqNameSafe, false))) {
                                d0.e0.p.d.m0.n.c0 type3 = ((w0) u.single((List<? extends Object>) type2.getArguments())).getType();
                                m.checkNotNullExpressionValue(type3, "continuationArgumentType.arguments.single().type");
                                d0.e0.p.d.m0.c.m containingDeclaration2 = this.a.getContainingDeclaration();
                                if (!(containingDeclaration2 instanceof d0.e0.p.d.m0.c.a)) {
                                    containingDeclaration2 = null;
                                }
                                d0.e0.p.d.m0.c.a aVar2 = (d0.e0.p.d.m0.c.a) containingDeclaration2;
                                if (aVar2 != null) {
                                    bVar = d0.e0.p.d.m0.k.x.a.fqNameOrNull(aVar2);
                                }
                                if (m.areEqual(bVar, b0.a)) {
                                    simpleType$default = b(simpleType$default, type3);
                                } else {
                                    if (!this.e && (!releaseCoroutines || !l.isContinuation(fqNameSafe, !releaseCoroutines))) {
                                        z3 = false;
                                    }
                                    this.e = z3;
                                    simpleType$default = b(simpleType$default, type3);
                                }
                            }
                            j0Var3 = simpleType$default;
                        }
                    }
                } else if (size2 == 1 && (size = list.size() - 1) >= 0) {
                    d0 d0Var2 = d0.a;
                    u0 typeConstructor2 = u0Var.getBuiltIns().getSuspendFunction(size).getTypeConstructor();
                    m.checkNotNullExpressionValue(typeConstructor2, "functionTypeConstructor.builtIns.getSuspendFunction(arity).typeConstructor");
                    j0Var3 = d0.simpleType$default(aVar, typeConstructor2, list, nullable, null, 16, null);
                }
                if (j0Var3 == null) {
                    j0Var2 = t.createErrorTypeWithArguments(m.stringPlus("Bad suspend function in metadata with constructor: ", u0Var), list);
                    m.checkNotNullExpressionValue(j0Var2, "createErrorTypeWithArguments(\n            \"Bad suspend function in metadata with constructor: $functionTypeConstructor\",\n            arguments\n        )");
                } else {
                    j0Var2 = j0Var3;
                }
            } else {
                d0 d0Var3 = d0.a;
                j0Var2 = d0.simpleType$default(aVar, u0Var, list, qVar.getNullable(), null, 16, null);
            }
        } else {
            d0 d0Var4 = d0.a;
            j0 computeExpandedType = d0.computeExpandedType((d0.e0.p.d.m0.c.y0) declarationDescriptor, list);
            if (!e0.isNullable(computeExpandedType) && !qVar.getNullable()) {
                z3 = false;
            }
            j0Var2 = computeExpandedType.makeNullableAsSpecified(z3).replaceAnnotations(g.f.create(u.plus((Iterable) aVar, (Iterable) computeExpandedType.getAnnotations())));
        }
        q abbreviatedType = d0.e0.p.d.m0.f.z.f.abbreviatedType(qVar, this.a.getTypeTable());
        if (!(abbreviatedType == null || (withAbbreviation = m0.withAbbreviation(j0Var2, simpleType(abbreviatedType, false))) == null)) {
            j0Var2 = withAbbreviation;
        }
        return qVar.hasClassName() ? this.a.getComponents().getPlatformDependentTypeTransformer().transformPlatformType(w.getClassId(this.a.getNameResolver(), qVar.getClassName()), j0Var2) : j0Var2;
    }

    public String toString() {
        String str = this.c;
        c0 c0Var = this.f3455b;
        return m.stringPlus(str, c0Var == null ? "" : m.stringPlus(". Child of ", c0Var.c));
    }

    public final d0.e0.p.d.m0.n.c0 type(q qVar) {
        m.checkNotNullParameter(qVar, "proto");
        if (!qVar.hasFlexibleTypeCapabilitiesId()) {
            return simpleType(qVar, true);
        }
        String string = this.a.getNameResolver().getString(qVar.getFlexibleTypeCapabilitiesId());
        j0 simpleType$default = simpleType$default(this, qVar, false, 2, null);
        q flexibleUpperBound = d0.e0.p.d.m0.f.z.f.flexibleUpperBound(qVar, this.a.getTypeTable());
        m.checkNotNull(flexibleUpperBound);
        return this.a.getComponents().getFlexibleTypeDeserializer().create(qVar, string, simpleType$default, simpleType$default(this, flexibleUpperBound, false, 2, null));
    }

    public /* synthetic */ c0(l lVar, c0 c0Var, List list, String str, String str2, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(lVar, c0Var, list, str, str2, (i & 32) != 0 ? false : z2);
    }
}
