package d0.e0.p.d.m0.l.b.e0;

import d0.e0.p.d.m0.c.g1.c;
import d0.e0.p.d.m0.m.o;
import d0.z.d.m;
import java.util.List;
import kotlin.jvm.functions.Function0;
/* compiled from: DeserializedAnnotations.kt */
/* loaded from: classes3.dex */
public final class n extends a {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public n(o oVar, Function0<? extends List<? extends c>> function0) {
        super(oVar, function0);
        m.checkNotNullParameter(oVar, "storageManager");
        m.checkNotNullParameter(function0, "compute");
    }

    @Override // d0.e0.p.d.m0.l.b.e0.a, d0.e0.p.d.m0.c.g1.g
    public boolean isEmpty() {
        return false;
    }
}
