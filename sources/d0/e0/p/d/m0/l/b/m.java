package d0.e0.p.d.m0.l.b;

import d0.e0.p.d.m0.c.e0;
import d0.e0.p.d.m0.c.f0;
import d0.e0.p.d.m0.c.h0;
import d0.e0.p.d.m0.g.a;
import d0.e0.p.d.m0.g.b;
/* compiled from: DeserializedClassDataFinder.kt */
/* loaded from: classes3.dex */
public final class m implements g {
    public final f0 a;

    public m(f0 f0Var) {
        d0.z.d.m.checkNotNullParameter(f0Var, "packageFragmentProvider");
        this.a = f0Var;
    }

    @Override // d0.e0.p.d.m0.l.b.g
    public f findClassData(a aVar) {
        f findClassData;
        d0.z.d.m.checkNotNullParameter(aVar, "classId");
        f0 f0Var = this.a;
        b packageFqName = aVar.getPackageFqName();
        d0.z.d.m.checkNotNullExpressionValue(packageFqName, "classId.packageFqName");
        for (e0 e0Var : h0.packageFragments(f0Var, packageFqName)) {
            if ((e0Var instanceof n) && (findClassData = ((n) e0Var).getClassDataFinder().findClassData(aVar)) != null) {
                return findClassData;
            }
        }
        return null;
    }
}
