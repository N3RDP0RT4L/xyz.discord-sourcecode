package d0.e0.p.d.m0.l.b.e0;

import d0.e0.p.d.m0.c.g1.c;
import d0.e0.p.d.m0.c.i1.b;
import d0.e0.p.d.m0.f.q;
import d0.e0.p.d.m0.f.s;
import d0.e0.p.d.m0.f.z.f;
import d0.e0.p.d.m0.l.b.l;
import d0.e0.p.d.m0.n.c0;
import d0.t.u;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.List;
import kotlin.jvm.functions.Function0;
/* compiled from: DeserializedTypeParameterDescriptor.kt */
/* loaded from: classes3.dex */
public final class m extends b {
    public final l t;
    public final s u;
    public final d0.e0.p.d.m0.l.b.e0.a v;

    /* compiled from: DeserializedTypeParameterDescriptor.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function0<List<? extends c>> {
        public a() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final List<? extends c> invoke() {
            return u.toList(m.this.t.getComponents().getAnnotationAndConstantLoader().loadTypeParameterAnnotations(m.this.getProto(), m.this.t.getNameResolver()));
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public m(d0.e0.p.d.m0.l.b.l r11, d0.e0.p.d.m0.f.s r12, int r13) {
        /*
            r10 = this;
            java.lang.String r0 = "c"
            d0.z.d.m.checkNotNullParameter(r11, r0)
            java.lang.String r0 = "proto"
            d0.z.d.m.checkNotNullParameter(r12, r0)
            d0.e0.p.d.m0.m.o r2 = r11.getStorageManager()
            d0.e0.p.d.m0.c.m r3 = r11.getContainingDeclaration()
            d0.e0.p.d.m0.f.z.c r0 = r11.getNameResolver()
            int r1 = r12.getName()
            d0.e0.p.d.m0.g.e r4 = d0.e0.p.d.m0.l.b.w.getName(r0, r1)
            d0.e0.p.d.m0.l.b.z r0 = d0.e0.p.d.m0.l.b.z.a
            d0.e0.p.d.m0.f.s$c r1 = r12.getVariance()
            java.lang.String r5 = "proto.variance"
            d0.z.d.m.checkNotNullExpressionValue(r1, r5)
            d0.e0.p.d.m0.n.j1 r5 = r0.variance(r1)
            boolean r6 = r12.getReified()
            d0.e0.p.d.m0.c.u0 r8 = d0.e0.p.d.m0.c.u0.a
            d0.e0.p.d.m0.c.x0$a r9 = d0.e0.p.d.m0.c.x0.a.a
            r1 = r10
            r7 = r13
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            r10.t = r11
            r10.u = r12
            d0.e0.p.d.m0.l.b.e0.a r12 = new d0.e0.p.d.m0.l.b.e0.a
            d0.e0.p.d.m0.m.o r11 = r11.getStorageManager()
            d0.e0.p.d.m0.l.b.e0.m$a r13 = new d0.e0.p.d.m0.l.b.e0.m$a
            r13.<init>()
            r12.<init>(r11, r13)
            r10.v = r12
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.l.b.e0.m.<init>(d0.e0.p.d.m0.l.b.l, d0.e0.p.d.m0.f.s, int):void");
    }

    @Override // d0.e0.p.d.m0.c.i1.f
    public void c(c0 c0Var) {
        d0.z.d.m.checkNotNullParameter(c0Var, "type");
        throw new IllegalStateException(d0.z.d.m.stringPlus("There should be no cycles for deserialized type parameters, but found for: ", this));
    }

    @Override // d0.e0.p.d.m0.c.i1.f
    public List<c0> d() {
        List<q> upperBounds = f.upperBounds(this.u, this.t.getTypeTable());
        if (upperBounds.isEmpty()) {
            return d0.t.m.listOf(d0.e0.p.d.m0.k.x.a.getBuiltIns(this).getDefaultBound());
        }
        d0.e0.p.d.m0.l.b.c0 typeDeserializer = this.t.getTypeDeserializer();
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(upperBounds, 10));
        for (q qVar : upperBounds) {
            arrayList.add(typeDeserializer.type(qVar));
        }
        return arrayList;
    }

    public final s getProto() {
        return this.u;
    }

    @Override // d0.e0.p.d.m0.c.g1.b, d0.e0.p.d.m0.c.g1.a
    public d0.e0.p.d.m0.l.b.e0.a getAnnotations() {
        return this.v;
    }
}
