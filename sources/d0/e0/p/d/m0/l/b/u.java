package d0.e0.p.d.m0.l.b;

import d0.e0.p.d.m0.b.g;
import d0.e0.p.d.m0.c.a;
import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.e0;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.i1.d0;
import d0.e0.p.d.m0.c.i1.l0;
import d0.e0.p.d.m0.c.i1.p;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.q0;
import d0.e0.p.d.m0.c.t0;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.c.y0;
import d0.e0.p.d.m0.c.z;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.f.i;
import d0.e0.p.d.m0.f.q;
import d0.e0.p.d.m0.f.r;
import d0.e0.p.d.m0.f.s;
import d0.e0.p.d.m0.f.v;
import d0.e0.p.d.m0.f.z.b;
import d0.e0.p.d.m0.f.z.f;
import d0.e0.p.d.m0.f.z.h;
import d0.e0.p.d.m0.i.n;
import d0.e0.p.d.m0.l.b.e0.g;
import d0.e0.p.d.m0.l.b.e0.j;
import d0.e0.p.d.m0.l.b.e0.k;
import d0.e0.p.d.m0.l.b.e0.l;
import d0.e0.p.d.m0.l.b.y;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.w0;
import d0.t.h0;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import d0.z.d.x;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import kotlin.Pair;
import kotlin.jvm.functions.Function0;
import kotlin.reflect.KDeclarationContainer;
/* compiled from: MemberDeserializer.kt */
/* loaded from: classes3.dex */
public final class u {
    public final l a;

    /* renamed from: b  reason: collision with root package name */
    public final d0.e0.p.d.m0.l.b.e f3480b;

    /* compiled from: MemberDeserializer.kt */
    /* loaded from: classes3.dex */
    public /* synthetic */ class a extends x {
        public static final a j = new a();

        @Override // d0.e0.g
        public Object get(Object obj) {
            return Boolean.valueOf(g.isSuspendFunctionType((c0) obj));
        }

        @Override // d0.z.d.d, kotlin.reflect.KCallable
        public String getName() {
            return "isSuspendFunctionType";
        }

        @Override // d0.z.d.d
        public KDeclarationContainer getOwner() {
            return a0.getOrCreateKotlinPackage(g.class, "deserialization");
        }

        @Override // d0.z.d.d
        public String getSignature() {
            return "isSuspendFunctionType(Lorg/jetbrains/kotlin/types/KotlinType;)Z";
        }
    }

    /* compiled from: MemberDeserializer.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function0<List<? extends d0.e0.p.d.m0.c.g1.c>> {
        public final /* synthetic */ d0.e0.p.d.m0.l.b.b $kind;
        public final /* synthetic */ n $proto;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(n nVar, d0.e0.p.d.m0.l.b.b bVar) {
            super(0);
            this.$proto = nVar;
            this.$kind = bVar;
        }

        @Override // kotlin.jvm.functions.Function0
        public final List<? extends d0.e0.p.d.m0.c.g1.c> invoke() {
            List<? extends d0.e0.p.d.m0.c.g1.c> list;
            u uVar = u.this;
            y a = uVar.a(uVar.a.getContainingDeclaration());
            if (a == null) {
                list = null;
            } else {
                list = d0.t.u.toList(u.this.a.getComponents().getAnnotationAndConstantLoader().loadCallableAnnotations(a, this.$proto, this.$kind));
            }
            return list != null ? list : d0.t.n.emptyList();
        }
    }

    /* compiled from: MemberDeserializer.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function0<List<? extends d0.e0.p.d.m0.c.g1.c>> {
        public final /* synthetic */ boolean $isDelegate;
        public final /* synthetic */ d0.e0.p.d.m0.f.n $proto;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(boolean z2, d0.e0.p.d.m0.f.n nVar) {
            super(0);
            this.$isDelegate = z2;
            this.$proto = nVar;
        }

        @Override // kotlin.jvm.functions.Function0
        public final List<? extends d0.e0.p.d.m0.c.g1.c> invoke() {
            List<? extends d0.e0.p.d.m0.c.g1.c> list;
            u uVar = u.this;
            y a = uVar.a(uVar.a.getContainingDeclaration());
            if (a == null) {
                list = null;
            } else {
                boolean z2 = this.$isDelegate;
                u uVar2 = u.this;
                d0.e0.p.d.m0.f.n nVar = this.$proto;
                list = z2 ? d0.t.u.toList(uVar2.a.getComponents().getAnnotationAndConstantLoader().loadPropertyDelegateFieldAnnotations(a, nVar)) : d0.t.u.toList(uVar2.a.getComponents().getAnnotationAndConstantLoader().loadPropertyBackingFieldAnnotations(a, nVar));
            }
            return list != null ? list : d0.t.n.emptyList();
        }
    }

    /* compiled from: MemberDeserializer.kt */
    /* loaded from: classes3.dex */
    public static final class d extends o implements Function0<d0.e0.p.d.m0.k.v.g<?>> {
        public final /* synthetic */ j $property;
        public final /* synthetic */ d0.e0.p.d.m0.f.n $proto;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public d(d0.e0.p.d.m0.f.n nVar, j jVar) {
            super(0);
            this.$proto = nVar;
            this.$property = jVar;
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final d0.e0.p.d.m0.k.v.g<?> invoke() {
            u uVar = u.this;
            y a = uVar.a(uVar.a.getContainingDeclaration());
            m.checkNotNull(a);
            d0.e0.p.d.m0.l.b.c<d0.e0.p.d.m0.c.g1.c, d0.e0.p.d.m0.k.v.g<?>> annotationAndConstantLoader = u.this.a.getComponents().getAnnotationAndConstantLoader();
            d0.e0.p.d.m0.f.n nVar = this.$proto;
            c0 returnType = this.$property.getReturnType();
            m.checkNotNullExpressionValue(returnType, "property.returnType");
            return annotationAndConstantLoader.loadPropertyConstant(a, nVar, returnType);
        }
    }

    /* compiled from: MemberDeserializer.kt */
    /* loaded from: classes3.dex */
    public static final class e extends o implements Function0<List<? extends d0.e0.p.d.m0.c.g1.c>> {
        public final /* synthetic */ n $callable;
        public final /* synthetic */ y $containerOfCallable;
        public final /* synthetic */ int $i;
        public final /* synthetic */ d0.e0.p.d.m0.l.b.b $kind;
        public final /* synthetic */ d0.e0.p.d.m0.f.u $proto;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public e(y yVar, n nVar, d0.e0.p.d.m0.l.b.b bVar, int i, d0.e0.p.d.m0.f.u uVar) {
            super(0);
            this.$containerOfCallable = yVar;
            this.$callable = nVar;
            this.$kind = bVar;
            this.$i = i;
            this.$proto = uVar;
        }

        @Override // kotlin.jvm.functions.Function0
        public final List<? extends d0.e0.p.d.m0.c.g1.c> invoke() {
            return d0.t.u.toList(u.this.a.getComponents().getAnnotationAndConstantLoader().loadValueParameterAnnotations(this.$containerOfCallable, this.$callable, this.$kind, this.$i, this.$proto));
        }
    }

    public u(l lVar) {
        m.checkNotNullParameter(lVar, "c");
        this.a = lVar;
        this.f3480b = new d0.e0.p.d.m0.l.b.e(lVar.getComponents().getModuleDescriptor(), lVar.getComponents().getNotFoundClasses());
    }

    public final y a(d0.e0.p.d.m0.c.m mVar) {
        if (mVar instanceof e0) {
            return new y.b(((e0) mVar).getFqName(), this.a.getNameResolver(), this.a.getTypeTable(), this.a.getContainerSource());
        }
        if (mVar instanceof d0.e0.p.d.m0.l.b.e0.d) {
            return ((d0.e0.p.d.m0.l.b.e0.d) mVar).getThisAsProtoContainer$deserialization();
        }
        return null;
    }

    public final g.a b(d0.e0.p.d.m0.l.b.e0.g gVar, c0 c0Var) {
        g.a aVar = g.a.COMPATIBLE;
        if (!i(gVar)) {
            return aVar;
        }
        for (z0 z0Var : c0Var.getOwnTypeParameters()) {
            z0Var.getUpperBounds();
        }
        return c0Var.getExperimentalSuspendFunctionTypeEncountered() ? g.a.INCOMPATIBLE : aVar;
    }

    public final g.a c(d0.e0.p.d.m0.l.b.e0.b bVar, q0 q0Var, Collection<? extends c1> collection, Collection<? extends z0> collection2, c0 c0Var, boolean z2) {
        boolean z3;
        g.a aVar;
        boolean z4;
        boolean z5;
        g.a aVar2 = g.a.NEEDS_WRAPPER;
        g.a aVar3 = g.a.INCOMPATIBLE;
        g.a aVar4 = g.a.COMPATIBLE;
        if (!i(bVar) || m.areEqual(d0.e0.p.d.m0.k.x.a.fqNameOrNull(bVar), b0.a)) {
            return aVar4;
        }
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(collection, 10));
        for (c1 c1Var : collection) {
            arrayList.add(c1Var.getType());
        }
        Boolean bool = null;
        List<c0> plus = d0.t.u.plus((Collection) arrayList, (Iterable) d0.t.n.listOfNotNull(q0Var == null ? null : q0Var.getType()));
        if (c0Var != null) {
            bool = Boolean.valueOf(d(c0Var));
        }
        if (m.areEqual(bool, Boolean.TRUE)) {
            return aVar3;
        }
        if (!(collection2 instanceof Collection) || !collection2.isEmpty()) {
            for (z0 z0Var : collection2) {
                List<c0> upperBounds = z0Var.getUpperBounds();
                m.checkNotNullExpressionValue(upperBounds, "typeParameter.upperBounds");
                if (!(upperBounds instanceof Collection) || !upperBounds.isEmpty()) {
                    for (c0 c0Var2 : upperBounds) {
                        m.checkNotNullExpressionValue(c0Var2, "it");
                        if (d(c0Var2)) {
                            z5 = true;
                            continue;
                            break;
                        }
                    }
                }
                z5 = false;
                continue;
                if (z5) {
                    z3 = true;
                    break;
                }
            }
        }
        z3 = false;
        if (z3) {
            return aVar3;
        }
        ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(plus, 10));
        for (c0 c0Var3 : plus) {
            m.checkNotNullExpressionValue(c0Var3, "type");
            if (!d0.e0.p.d.m0.b.g.isSuspendFunctionType(c0Var3) || c0Var3.getArguments().size() > 3) {
                if (!d(c0Var3)) {
                    aVar = aVar4;
                    arrayList2.add(aVar);
                }
                aVar = aVar3;
                arrayList2.add(aVar);
            } else {
                List<w0> arguments = c0Var3.getArguments();
                if (!(arguments instanceof Collection) || !arguments.isEmpty()) {
                    for (w0 w0Var : arguments) {
                        c0 type = w0Var.getType();
                        m.checkNotNullExpressionValue(type, "it.type");
                        if (d(type)) {
                            z4 = true;
                            break;
                        }
                    }
                }
                z4 = false;
                if (!z4) {
                    aVar = aVar2;
                    arrayList2.add(aVar);
                }
                aVar = aVar3;
                arrayList2.add(aVar);
            }
        }
        g.a aVar5 = (g.a) d0.t.u.maxOrNull((Iterable<? extends Comparable>) arrayList2);
        if (aVar5 == null) {
            aVar5 = aVar4;
        }
        if (!z2) {
            aVar2 = aVar4;
        }
        return (g.a) d0.u.b.maxOf(aVar2, aVar5);
    }

    public final boolean d(c0 c0Var) {
        return d0.e0.p.d.m0.n.o1.a.contains(c0Var, a.j);
    }

    public final d0.e0.p.d.m0.c.g1.g e(n nVar, int i, d0.e0.p.d.m0.l.b.b bVar) {
        if (!d0.e0.p.d.m0.f.z.b.f3384b.get(i).booleanValue()) {
            return d0.e0.p.d.m0.c.g1.g.f.getEMPTY();
        }
        return new d0.e0.p.d.m0.l.b.e0.n(this.a.getStorageManager(), new b(nVar, bVar));
    }

    public final q0 f() {
        d0.e0.p.d.m0.c.m containingDeclaration = this.a.getContainingDeclaration();
        d0.e0.p.d.m0.c.e eVar = containingDeclaration instanceof d0.e0.p.d.m0.c.e ? (d0.e0.p.d.m0.c.e) containingDeclaration : null;
        if (eVar == null) {
            return null;
        }
        return eVar.getThisAsReceiverParameter();
    }

    public final d0.e0.p.d.m0.c.g1.g g(d0.e0.p.d.m0.f.n nVar, boolean z2) {
        if (!d0.e0.p.d.m0.f.z.b.f3384b.get(nVar.getFlags()).booleanValue()) {
            return d0.e0.p.d.m0.c.g1.g.f.getEMPTY();
        }
        return new d0.e0.p.d.m0.l.b.e0.n(this.a.getStorageManager(), new c(z2, nVar));
    }

    public final List<c1> h(List<d0.e0.p.d.m0.f.u> list, n nVar, d0.e0.p.d.m0.l.b.b bVar) {
        d0.e0.p.d.m0.c.g1.g gVar;
        d0.e0.p.d.m0.c.a aVar = (d0.e0.p.d.m0.c.a) this.a.getContainingDeclaration();
        d0.e0.p.d.m0.c.m containingDeclaration = aVar.getContainingDeclaration();
        m.checkNotNullExpressionValue(containingDeclaration, "callableDescriptor.containingDeclaration");
        y a2 = a(containingDeclaration);
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(list, 10));
        int i = 0;
        for (Object obj : list) {
            i++;
            if (i < 0) {
                d0.t.n.throwIndexOverflow();
            }
            d0.e0.p.d.m0.f.u uVar = (d0.e0.p.d.m0.f.u) obj;
            int flags = uVar.hasFlags() ? uVar.getFlags() : 0;
            if (a2 == null || !b.d.b.a.a.s0(d0.e0.p.d.m0.f.z.b.f3384b, flags, "HAS_ANNOTATIONS.get(flags)")) {
                gVar = d0.e0.p.d.m0.c.g1.g.f.getEMPTY();
            } else {
                gVar = new d0.e0.p.d.m0.l.b.e0.n(this.a.getStorageManager(), new e(a2, nVar, bVar, i, uVar));
            }
            d0.e0.p.d.m0.g.e name = w.getName(this.a.getNameResolver(), uVar.getName());
            c0 type = this.a.getTypeDeserializer().type(f.type(uVar, this.a.getTypeTable()));
            boolean s0 = b.d.b.a.a.s0(d0.e0.p.d.m0.f.z.b.F, flags, "DECLARES_DEFAULT_VALUE.get(flags)");
            boolean s02 = b.d.b.a.a.s0(d0.e0.p.d.m0.f.z.b.G, flags, "IS_CROSSINLINE.get(flags)");
            boolean s03 = b.d.b.a.a.s0(d0.e0.p.d.m0.f.z.b.H, flags, "IS_NOINLINE.get(flags)");
            q varargElementType = f.varargElementType(uVar, this.a.getTypeTable());
            c0 type2 = varargElementType == null ? null : this.a.getTypeDeserializer().type(varargElementType);
            u0 u0Var = u0.a;
            m.checkNotNullExpressionValue(u0Var, "NO_SOURCE");
            ArrayList arrayList2 = arrayList;
            arrayList2.add(new l0(aVar, null, i, gVar, name, type, s0, s02, s03, type2, u0Var));
            arrayList = arrayList2;
        }
        return d0.t.u.toList(arrayList);
    }

    public final boolean i(d0.e0.p.d.m0.l.b.e0.g gVar) {
        boolean z2;
        boolean z3;
        if (!this.a.getComponents().getConfiguration().getReleaseCoroutines()) {
            return false;
        }
        List<h> versionRequirements = gVar.getVersionRequirements();
        if (!(versionRequirements instanceof Collection) || !versionRequirements.isEmpty()) {
            for (h hVar : versionRequirements) {
                if (!m.areEqual(hVar.getVersion(), new h.b(1, 3, 0, 4, null)) || hVar.getKind() != v.d.LANGUAGE_VERSION) {
                    z3 = false;
                    continue;
                } else {
                    z3 = true;
                    continue;
                }
                if (z3) {
                    z2 = false;
                    break;
                }
            }
        }
        z2 = true;
        return z2;
    }

    public final d0.e0.p.d.m0.c.d loadConstructor(d0.e0.p.d.m0.f.d dVar, boolean z2) {
        d0.e0.p.d.m0.l.b.e0.c cVar;
        g.a aVar;
        c0 typeDeserializer;
        m.checkNotNullParameter(dVar, "proto");
        d0.e0.p.d.m0.c.e eVar = (d0.e0.p.d.m0.c.e) this.a.getContainingDeclaration();
        int flags = dVar.getFlags();
        d0.e0.p.d.m0.l.b.b bVar = d0.e0.p.d.m0.l.b.b.FUNCTION;
        d0.e0.p.d.m0.l.b.e0.c cVar2 = new d0.e0.p.d.m0.l.b.e0.c(eVar, null, e(dVar, flags, bVar), z2, b.a.DECLARATION, dVar, this.a.getNameResolver(), this.a.getTypeTable(), this.a.getVersionRequirementTable(), this.a.getContainerSource(), null, 1024, null);
        u memberDeserializer = l.childContext$default(this.a, cVar2, d0.t.n.emptyList(), null, null, null, null, 60, null).getMemberDeserializer();
        List<d0.e0.p.d.m0.f.u> valueParameterList = dVar.getValueParameterList();
        m.checkNotNullExpressionValue(valueParameterList, "proto.valueParameterList");
        cVar2.initialize(memberDeserializer.h(valueParameterList, dVar, bVar), a0.descriptorVisibility(z.a, d0.e0.p.d.m0.f.z.b.c.get(dVar.getFlags())));
        cVar2.setReturnType(eVar.getDefaultType());
        boolean z3 = true;
        cVar2.setHasStableParameterNames(!d0.e0.p.d.m0.f.z.b.m.get(dVar.getFlags()).booleanValue());
        d0.e0.p.d.m0.c.m containingDeclaration = this.a.getContainingDeclaration();
        Boolean bool = null;
        d0.e0.p.d.m0.l.b.e0.d dVar2 = containingDeclaration instanceof d0.e0.p.d.m0.l.b.e0.d ? (d0.e0.p.d.m0.l.b.e0.d) containingDeclaration : null;
        l c2 = dVar2 == null ? null : dVar2.getC();
        if (!(c2 == null || (typeDeserializer = c2.getTypeDeserializer()) == null)) {
            bool = Boolean.valueOf(typeDeserializer.getExperimentalSuspendFunctionTypeEncountered());
        }
        if (!m.areEqual(bool, Boolean.TRUE) || !i(cVar2)) {
            z3 = false;
        }
        if (z3) {
            aVar = g.a.INCOMPATIBLE;
            cVar = cVar2;
        } else {
            Collection<? extends c1> valueParameters = cVar2.getValueParameters();
            m.checkNotNullExpressionValue(valueParameters, "descriptor.valueParameters");
            Collection<? extends z0> typeParameters = cVar2.getTypeParameters();
            m.checkNotNullExpressionValue(typeParameters, "descriptor.typeParameters");
            cVar = cVar2;
            aVar = c(cVar2, null, valueParameters, typeParameters, cVar2.getReturnType(), false);
        }
        cVar.setCoroutinesExperimentalCompatibilityMode$deserialization(aVar);
        return cVar;
    }

    public final t0 loadFunction(i iVar) {
        int i;
        d0.e0.p.d.m0.c.g1.g gVar;
        d0.e0.p.d.m0.f.z.i iVar2;
        c0 type;
        m.checkNotNullParameter(iVar, "proto");
        if (iVar.hasFlags()) {
            i = iVar.getFlags();
        } else {
            int oldFlags = iVar.getOldFlags();
            i = ((oldFlags >> 8) << 6) + (oldFlags & 63);
        }
        int i2 = i;
        d0.e0.p.d.m0.l.b.b bVar = d0.e0.p.d.m0.l.b.b.FUNCTION;
        d0.e0.p.d.m0.c.g1.g e2 = e(iVar, i2, bVar);
        if (f.hasReceiver(iVar)) {
            gVar = new d0.e0.p.d.m0.l.b.e0.a(this.a.getStorageManager(), new v(this, iVar, bVar));
        } else {
            gVar = d0.e0.p.d.m0.c.g1.g.f.getEMPTY();
        }
        if (m.areEqual(d0.e0.p.d.m0.k.x.a.getFqNameSafe(this.a.getContainingDeclaration()).child(w.getName(this.a.getNameResolver(), iVar.getName())), b0.a)) {
            iVar2 = d0.e0.p.d.m0.f.z.i.a.getEMPTY();
        } else {
            iVar2 = this.a.getVersionRequirementTable();
        }
        d0.e0.p.d.m0.f.z.i iVar3 = iVar2;
        d0.e0.p.d.m0.c.m containingDeclaration = this.a.getContainingDeclaration();
        d0.e0.p.d.m0.g.e name = w.getName(this.a.getNameResolver(), iVar.getName());
        z zVar = z.a;
        k kVar = new k(containingDeclaration, null, e2, name, a0.memberKind(zVar, d0.e0.p.d.m0.f.z.b.n.get(i2)), iVar, this.a.getNameResolver(), this.a.getTypeTable(), iVar3, this.a.getContainerSource(), null, 1024, null);
        l lVar = this.a;
        List<s> typeParameterList = iVar.getTypeParameterList();
        m.checkNotNullExpressionValue(typeParameterList, "proto.typeParameterList");
        l childContext$default = l.childContext$default(lVar, kVar, typeParameterList, null, null, null, null, 60, null);
        q receiverType = f.receiverType(iVar, this.a.getTypeTable());
        q0 q0Var = null;
        if (receiverType != null && (type = childContext$default.getTypeDeserializer().type(receiverType)) != null) {
            q0Var = d0.e0.p.d.m0.k.d.createExtensionReceiverParameterForCallable(kVar, type, gVar);
        }
        q0 f = f();
        List<z0> ownTypeParameters = childContext$default.getTypeDeserializer().getOwnTypeParameters();
        u memberDeserializer = childContext$default.getMemberDeserializer();
        List<d0.e0.p.d.m0.f.u> valueParameterList = iVar.getValueParameterList();
        m.checkNotNullExpressionValue(valueParameterList, "proto.valueParameterList");
        List<c1> h = memberDeserializer.h(valueParameterList, iVar, bVar);
        c0 type2 = childContext$default.getTypeDeserializer().type(f.returnType(iVar, this.a.getTypeTable()));
        z modality = zVar.modality(d0.e0.p.d.m0.f.z.b.d.get(i2));
        d0.e0.p.d.m0.c.u descriptorVisibility = a0.descriptorVisibility(zVar, d0.e0.p.d.m0.f.z.b.c.get(i2));
        Map<? extends a.AbstractC0290a<?>, ?> emptyMap = h0.emptyMap();
        b.C0334b bVar2 = d0.e0.p.d.m0.f.z.b.t;
        kVar.initialize(q0Var, f, ownTypeParameters, h, type2, modality, descriptorVisibility, emptyMap, c(kVar, q0Var, h, ownTypeParameters, type2, b.d.b.a.a.s0(bVar2, i2, "IS_SUSPEND.get(flags)")));
        Boolean bool = d0.e0.p.d.m0.f.z.b.o.get(i2);
        m.checkNotNullExpressionValue(bool, "IS_OPERATOR.get(flags)");
        kVar.setOperator(bool.booleanValue());
        Boolean bool2 = d0.e0.p.d.m0.f.z.b.p.get(i2);
        m.checkNotNullExpressionValue(bool2, "IS_INFIX.get(flags)");
        kVar.setInfix(bool2.booleanValue());
        Boolean bool3 = d0.e0.p.d.m0.f.z.b.f3385s.get(i2);
        m.checkNotNullExpressionValue(bool3, "IS_EXTERNAL_FUNCTION.get(flags)");
        kVar.setExternal(bool3.booleanValue());
        Boolean bool4 = d0.e0.p.d.m0.f.z.b.q.get(i2);
        m.checkNotNullExpressionValue(bool4, "IS_INLINE.get(flags)");
        kVar.setInline(bool4.booleanValue());
        Boolean bool5 = d0.e0.p.d.m0.f.z.b.r.get(i2);
        m.checkNotNullExpressionValue(bool5, "IS_TAILREC.get(flags)");
        kVar.setTailrec(bool5.booleanValue());
        Boolean bool6 = bVar2.get(i2);
        m.checkNotNullExpressionValue(bool6, "IS_SUSPEND.get(flags)");
        kVar.setSuspend(bool6.booleanValue());
        Boolean bool7 = d0.e0.p.d.m0.f.z.b.u.get(i2);
        m.checkNotNullExpressionValue(bool7, "IS_EXPECT_FUNCTION.get(flags)");
        kVar.setExpect(bool7.booleanValue());
        kVar.setHasStableParameterNames(!d0.e0.p.d.m0.f.z.b.v.get(i2).booleanValue());
        Pair<a.AbstractC0290a<?>, Object> deserializeContractFromFunction = this.a.getComponents().getContractDeserializer().deserializeContractFromFunction(iVar, kVar, this.a.getTypeTable(), childContext$default.getTypeDeserializer());
        if (deserializeContractFromFunction != null) {
            kVar.putInUserDataMap(deserializeContractFromFunction.getFirst(), deserializeContractFromFunction.getSecond());
        }
        return kVar;
    }

    public final n0 loadProperty(d0.e0.p.d.m0.f.n nVar) {
        int i;
        d0.e0.p.d.m0.l.b.b bVar;
        d0.e0.p.d.m0.f.n nVar2;
        d0.e0.p.d.m0.c.g1.g gVar;
        j jVar;
        q0 q0Var;
        d0 d0Var;
        z zVar;
        d0.e0.p.d.m0.c.i1.e0 e0Var;
        d0 d0Var2;
        c0 type;
        d0.e0.p.d.m0.l.b.b bVar2 = d0.e0.p.d.m0.l.b.b.PROPERTY_GETTER;
        m.checkNotNullParameter(nVar, "proto");
        if (nVar.hasFlags()) {
            i = nVar.getFlags();
        } else {
            int oldFlags = nVar.getOldFlags();
            i = ((oldFlags >> 8) << 6) + (oldFlags & 63);
        }
        int i2 = i;
        d0.e0.p.d.m0.c.m containingDeclaration = this.a.getContainingDeclaration();
        d0.e0.p.d.m0.c.g1.g e2 = e(nVar, i2, d0.e0.p.d.m0.l.b.b.PROPERTY);
        z zVar2 = z.a;
        b.d<d0.e0.p.d.m0.f.k> dVar = d0.e0.p.d.m0.f.z.b.d;
        z modality = zVar2.modality(dVar.get(i2));
        b.d<d0.e0.p.d.m0.f.x> dVar2 = d0.e0.p.d.m0.f.z.b.c;
        j jVar2 = new j(containingDeclaration, null, e2, modality, a0.descriptorVisibility(zVar2, dVar2.get(i2)), b.d.b.a.a.s0(d0.e0.p.d.m0.f.z.b.w, i2, "IS_VAR.get(flags)"), w.getName(this.a.getNameResolver(), nVar.getName()), a0.memberKind(zVar2, d0.e0.p.d.m0.f.z.b.n.get(i2)), b.d.b.a.a.s0(d0.e0.p.d.m0.f.z.b.A, i2, "IS_LATEINIT.get(flags)"), b.d.b.a.a.s0(d0.e0.p.d.m0.f.z.b.f3388z, i2, "IS_CONST.get(flags)"), b.d.b.a.a.s0(d0.e0.p.d.m0.f.z.b.C, i2, "IS_EXTERNAL_PROPERTY.get(flags)"), b.d.b.a.a.s0(d0.e0.p.d.m0.f.z.b.D, i2, "IS_DELEGATED.get(flags)"), b.d.b.a.a.s0(d0.e0.p.d.m0.f.z.b.E, i2, "IS_EXPECT_PROPERTY.get(flags)"), nVar, this.a.getNameResolver(), this.a.getTypeTable(), this.a.getVersionRequirementTable(), this.a.getContainerSource());
        l lVar = this.a;
        List<s> typeParameterList = nVar.getTypeParameterList();
        m.checkNotNullExpressionValue(typeParameterList, "proto.typeParameterList");
        l childContext$default = l.childContext$default(lVar, jVar2, typeParameterList, null, null, null, null, 60, null);
        boolean s0 = b.d.b.a.a.s0(d0.e0.p.d.m0.f.z.b.f3386x, i2, "HAS_GETTER.get(flags)");
        if (!s0 || !f.hasReceiver(nVar)) {
            nVar2 = nVar;
            bVar = bVar2;
            gVar = d0.e0.p.d.m0.c.g1.g.f.getEMPTY();
        } else {
            nVar2 = nVar;
            bVar = bVar2;
            gVar = new d0.e0.p.d.m0.l.b.e0.a(this.a.getStorageManager(), new v(this, nVar2, bVar));
        }
        c0 type2 = childContext$default.getTypeDeserializer().type(f.returnType(nVar2, this.a.getTypeTable()));
        List<z0> ownTypeParameters = childContext$default.getTypeDeserializer().getOwnTypeParameters();
        q0 f = f();
        q receiverType = f.receiverType(nVar2, this.a.getTypeTable());
        if (receiverType == null || (type = childContext$default.getTypeDeserializer().type(receiverType)) == null) {
            q0Var = null;
            jVar = jVar2;
        } else {
            jVar = jVar2;
            q0Var = d0.e0.p.d.m0.k.d.createExtensionReceiverParameterForCallable(jVar, type, gVar);
        }
        jVar.setType(type2, ownTypeParameters, f, q0Var);
        int accessorFlags = d0.e0.p.d.m0.f.z.b.getAccessorFlags(b.d.b.a.a.s0(d0.e0.p.d.m0.f.z.b.f3384b, i2, "HAS_ANNOTATIONS.get(flags)"), dVar2.get(i2), dVar.get(i2), false, false, false);
        if (s0) {
            int getterFlags = nVar.hasGetterFlags() ? nVar.getGetterFlags() : accessorFlags;
            boolean s02 = b.d.b.a.a.s0(d0.e0.p.d.m0.f.z.b.I, getterFlags, "IS_NOT_DEFAULT.get(getterFlags)");
            boolean s03 = b.d.b.a.a.s0(d0.e0.p.d.m0.f.z.b.J, getterFlags, "IS_EXTERNAL_ACCESSOR.get(getterFlags)");
            boolean s04 = b.d.b.a.a.s0(d0.e0.p.d.m0.f.z.b.K, getterFlags, "IS_INLINE_ACCESSOR.get(getterFlags)");
            d0.e0.p.d.m0.c.g1.g e3 = e(nVar2, getterFlags, bVar);
            if (s02) {
                zVar = zVar2;
                d0Var2 = new d0(jVar, e3, zVar2.modality(dVar.get(getterFlags)), a0.descriptorVisibility(zVar2, dVar2.get(getterFlags)), !s02, s03, s04, jVar.getKind(), null, u0.a);
            } else {
                zVar = zVar2;
                d0Var2 = d0.e0.p.d.m0.k.d.createDefaultGetter(jVar, e3);
                m.checkNotNullExpressionValue(d0Var2, "{\n                DescriptorFactory.createDefaultGetter(property, annotations)\n            }");
            }
            d0Var2.initialize(jVar.getReturnType());
            d0Var = d0Var2;
        } else {
            zVar = zVar2;
            d0Var = null;
        }
        if (b.d.b.a.a.s0(d0.e0.p.d.m0.f.z.b.f3387y, i2, "HAS_SETTER.get(flags)")) {
            if (nVar.hasSetterFlags()) {
                accessorFlags = nVar.getSetterFlags();
            }
            boolean s05 = b.d.b.a.a.s0(d0.e0.p.d.m0.f.z.b.I, accessorFlags, "IS_NOT_DEFAULT.get(setterFlags)");
            boolean s06 = b.d.b.a.a.s0(d0.e0.p.d.m0.f.z.b.J, accessorFlags, "IS_EXTERNAL_ACCESSOR.get(setterFlags)");
            boolean s07 = b.d.b.a.a.s0(d0.e0.p.d.m0.f.z.b.K, accessorFlags, "IS_INLINE_ACCESSOR.get(setterFlags)");
            d0.e0.p.d.m0.l.b.b bVar3 = d0.e0.p.d.m0.l.b.b.PROPERTY_SETTER;
            d0.e0.p.d.m0.c.g1.g e4 = e(nVar2, accessorFlags, bVar3);
            if (s05) {
                d0.e0.p.d.m0.c.i1.e0 e0Var2 = new d0.e0.p.d.m0.c.i1.e0(jVar, e4, zVar.modality(dVar.get(accessorFlags)), a0.descriptorVisibility(zVar, dVar2.get(accessorFlags)), !s05, s06, s07, jVar.getKind(), null, u0.a);
                e0Var2.initialize((c1) d0.t.u.single((List<? extends Object>) l.childContext$default(childContext$default, e0Var2, d0.t.n.emptyList(), null, null, null, null, 60, null).getMemberDeserializer().h(d0.t.m.listOf(nVar.getSetterValueParameter()), nVar2, bVar3)));
                e0Var = e0Var2;
            } else {
                d0.e0.p.d.m0.c.i1.e0 createDefaultSetter = d0.e0.p.d.m0.k.d.createDefaultSetter(jVar, e4, d0.e0.p.d.m0.c.g1.g.f.getEMPTY());
                m.checkNotNullExpressionValue(createDefaultSetter, "{\n                DescriptorFactory.createDefaultSetter(\n                    property, annotations,\n                    Annotations.EMPTY /* Otherwise the setter is not default, see DescriptorResolver.resolvePropertySetterDescriptor */\n                )\n            }");
                e0Var = createDefaultSetter;
            }
        } else {
            e0Var = null;
        }
        if (b.d.b.a.a.s0(d0.e0.p.d.m0.f.z.b.B, i2, "HAS_CONSTANT.get(flags)")) {
            jVar.setCompileTimeInitializer(this.a.getStorageManager().createNullableLazyValue(new d(nVar2, jVar)));
        }
        jVar.initialize(d0Var, e0Var, new p(g(nVar2, false), jVar), new p(g(nVar2, true), jVar), b(jVar, childContext$default.getTypeDeserializer()));
        return jVar;
    }

    public final y0 loadTypeAlias(r rVar) {
        m.checkNotNullParameter(rVar, "proto");
        g.a aVar = d0.e0.p.d.m0.c.g1.g.f;
        List<d0.e0.p.d.m0.f.b> annotationList = rVar.getAnnotationList();
        m.checkNotNullExpressionValue(annotationList, "proto.annotationList");
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(annotationList, 10));
        for (d0.e0.p.d.m0.f.b bVar : annotationList) {
            d0.e0.p.d.m0.l.b.e eVar = this.f3480b;
            m.checkNotNullExpressionValue(bVar, "it");
            arrayList.add(eVar.deserializeAnnotation(bVar, this.a.getNameResolver()));
        }
        l lVar = new l(this.a.getStorageManager(), this.a.getContainingDeclaration(), aVar.create(arrayList), w.getName(this.a.getNameResolver(), rVar.getName()), a0.descriptorVisibility(z.a, d0.e0.p.d.m0.f.z.b.c.get(rVar.getFlags())), rVar, this.a.getNameResolver(), this.a.getTypeTable(), this.a.getVersionRequirementTable(), this.a.getContainerSource());
        l lVar2 = this.a;
        List<s> typeParameterList = rVar.getTypeParameterList();
        m.checkNotNullExpressionValue(typeParameterList, "proto.typeParameterList");
        l childContext$default = l.childContext$default(lVar2, lVar, typeParameterList, null, null, null, null, 60, null);
        lVar.initialize(childContext$default.getTypeDeserializer().getOwnTypeParameters(), childContext$default.getTypeDeserializer().simpleType(f.underlyingType(rVar, this.a.getTypeTable()), false), childContext$default.getTypeDeserializer().simpleType(f.expandedType(rVar, this.a.getTypeTable()), false), b(lVar, childContext$default.getTypeDeserializer()));
        return lVar;
    }
}
