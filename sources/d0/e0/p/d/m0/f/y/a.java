package d0.e0.p.d.m0.f.y;

import d0.t.c0;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.io.DataInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.ranges.IntRange;
/* compiled from: BuiltInsBinaryVersion.kt */
/* loaded from: classes3.dex */
public final class a extends d0.e0.p.d.m0.f.z.a {
    public static final C0332a f = new C0332a(null);
    public static final a g = new a(1, 0, 7);

    /* compiled from: BuiltInsBinaryVersion.kt */
    /* renamed from: d0.e0.p.d.m0.f.y.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0332a {
        public C0332a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final a readFrom(InputStream inputStream) {
            m.checkNotNullParameter(inputStream, "stream");
            DataInputStream dataInputStream = new DataInputStream(inputStream);
            IntRange intRange = new IntRange(1, dataInputStream.readInt());
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(intRange, 10));
            Iterator<Integer> it = intRange.iterator();
            while (it.hasNext()) {
                ((c0) it).nextInt();
                arrayList.add(Integer.valueOf(dataInputStream.readInt()));
            }
            int[] intArray = u.toIntArray(arrayList);
            int[] iArr = new int[intArray.length];
            System.arraycopy(intArray, 0, iArr, 0, intArray.length);
            return new a(iArr);
        }
    }

    static {
        new a(new int[0]);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public a(int... r4) {
        /*
            r3 = this;
            java.lang.String r0 = "numbers"
            d0.z.d.m.checkNotNullParameter(r4, r0)
            int r0 = r4.length
            int[] r0 = new int[r0]
            int r1 = r4.length
            r2 = 0
            java.lang.System.arraycopy(r4, r2, r0, r2, r1)
            r3.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.f.y.a.<init>(int[]):void");
    }

    public boolean isCompatible() {
        return a(g);
    }
}
