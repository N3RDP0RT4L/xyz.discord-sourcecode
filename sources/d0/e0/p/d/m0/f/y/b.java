package d0.e0.p.d.m0.f.y;

import d0.e0.p.d.m0.f.b;
import d0.e0.p.d.m0.f.c;
import d0.e0.p.d.m0.f.d;
import d0.e0.p.d.m0.f.i;
import d0.e0.p.d.m0.f.l;
import d0.e0.p.d.m0.f.n;
import d0.e0.p.d.m0.f.q;
import d0.e0.p.d.m0.f.s;
import d0.e0.p.d.m0.f.u;
import d0.e0.p.d.m0.i.e;
import d0.e0.p.d.m0.i.g;
import d0.e0.p.d.m0.i.w;
import java.util.List;
import org.objectweb.asm.Opcodes;
/* compiled from: BuiltInsProtoBuf.java */
/* loaded from: classes3.dex */
public final class b {
    public static final g.f<l, Integer> a = g.newSingularGeneratedExtension(l.getDefaultInstance(), 0, null, null, Opcodes.DCMPL, w.b.n, Integer.class);

    /* renamed from: b  reason: collision with root package name */
    public static final g.f<c, List<d0.e0.p.d.m0.f.b>> f3382b;
    public static final g.f<d, List<d0.e0.p.d.m0.f.b>> c;
    public static final g.f<i, List<d0.e0.p.d.m0.f.b>> d;
    public static final g.f<n, List<d0.e0.p.d.m0.f.b>> e;
    public static final g.f<n, List<d0.e0.p.d.m0.f.b>> f;
    public static final g.f<n, List<d0.e0.p.d.m0.f.b>> g;
    public static final g.f<n, b.C0325b.c> h;
    public static final g.f<d0.e0.p.d.m0.f.g, List<d0.e0.p.d.m0.f.b>> i;
    public static final g.f<u, List<d0.e0.p.d.m0.f.b>> j;
    public static final g.f<q, List<d0.e0.p.d.m0.f.b>> k;
    public static final g.f<s, List<d0.e0.p.d.m0.f.b>> l;

    static {
        c defaultInstance = c.getDefaultInstance();
        d0.e0.p.d.m0.f.b defaultInstance2 = d0.e0.p.d.m0.f.b.getDefaultInstance();
        w.b bVar = w.b.t;
        f3382b = g.newRepeatedGeneratedExtension(defaultInstance, defaultInstance2, null, 150, bVar, false, d0.e0.p.d.m0.f.b.class);
        c = g.newRepeatedGeneratedExtension(d.getDefaultInstance(), d0.e0.p.d.m0.f.b.getDefaultInstance(), null, 150, bVar, false, d0.e0.p.d.m0.f.b.class);
        d = g.newRepeatedGeneratedExtension(i.getDefaultInstance(), d0.e0.p.d.m0.f.b.getDefaultInstance(), null, 150, bVar, false, d0.e0.p.d.m0.f.b.class);
        e = g.newRepeatedGeneratedExtension(n.getDefaultInstance(), d0.e0.p.d.m0.f.b.getDefaultInstance(), null, 150, bVar, false, d0.e0.p.d.m0.f.b.class);
        f = g.newRepeatedGeneratedExtension(n.getDefaultInstance(), d0.e0.p.d.m0.f.b.getDefaultInstance(), null, Opcodes.DCMPG, bVar, false, d0.e0.p.d.m0.f.b.class);
        g = g.newRepeatedGeneratedExtension(n.getDefaultInstance(), d0.e0.p.d.m0.f.b.getDefaultInstance(), null, 153, bVar, false, d0.e0.p.d.m0.f.b.class);
        h = g.newSingularGeneratedExtension(n.getDefaultInstance(), b.C0325b.c.getDefaultInstance(), b.C0325b.c.getDefaultInstance(), null, Opcodes.DCMPL, bVar, b.C0325b.c.class);
        i = g.newRepeatedGeneratedExtension(d0.e0.p.d.m0.f.g.getDefaultInstance(), d0.e0.p.d.m0.f.b.getDefaultInstance(), null, 150, bVar, false, d0.e0.p.d.m0.f.b.class);
        j = g.newRepeatedGeneratedExtension(u.getDefaultInstance(), d0.e0.p.d.m0.f.b.getDefaultInstance(), null, 150, bVar, false, d0.e0.p.d.m0.f.b.class);
        k = g.newRepeatedGeneratedExtension(q.getDefaultInstance(), d0.e0.p.d.m0.f.b.getDefaultInstance(), null, 150, bVar, false, d0.e0.p.d.m0.f.b.class);
        l = g.newRepeatedGeneratedExtension(s.getDefaultInstance(), d0.e0.p.d.m0.f.b.getDefaultInstance(), null, 150, bVar, false, d0.e0.p.d.m0.f.b.class);
    }

    public static void registerAllExtensions(e eVar) {
        eVar.add(a);
        eVar.add(f3382b);
        eVar.add(c);
        eVar.add(d);
        eVar.add(e);
        eVar.add(f);
        eVar.add(g);
        eVar.add(h);
        eVar.add(i);
        eVar.add(j);
        eVar.add(k);
        eVar.add(l);
    }
}
