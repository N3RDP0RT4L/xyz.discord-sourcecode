package d0.e0.p.d.m0.f;

import d0.e0.p.d.m0.i.c;
import d0.e0.p.d.m0.i.e;
import d0.e0.p.d.m0.i.g;
import d0.e0.p.d.m0.i.h;
import d0.e0.p.d.m0.i.o;
import d0.e0.p.d.m0.i.p;
import java.io.IOException;
import java.util.Objects;
import kotlin.reflect.jvm.internal.impl.protobuf.CodedOutputStream;
import kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException;
import kotlin.reflect.jvm.internal.impl.protobuf.UninitializedMessageException;
/* compiled from: ProtoBuf.java */
/* loaded from: classes3.dex */
public final class v extends g implements o {
    public static final v j;
    public static p<v> k = new a();
    private int bitField0_;
    private int errorCode_;
    private c level_;
    private byte memoizedIsInitialized;
    private int memoizedSerializedSize;
    private int message_;
    private final d0.e0.p.d.m0.i.c unknownFields;
    private int versionFull_;
    private d versionKind_;
    private int version_;

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static class a extends d0.e0.p.d.m0.i.b<v> {
        @Override // d0.e0.p.d.m0.i.p
        public v parsePartialFrom(d0.e0.p.d.m0.i.d dVar, e eVar) throws InvalidProtocolBufferException {
            return new v(dVar, eVar, null);
        }
    }

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static final class b extends g.b<v, b> implements o {
        public int k;
        public int l;
        public int m;
        public int o;
        public int p;
        public c n = c.ERROR;
        public d q = d.LANGUAGE_VERSION;

        public v buildPartial() {
            v vVar = new v(this, null);
            int i = this.k;
            int i2 = 1;
            if ((i & 1) != 1) {
                i2 = 0;
            }
            vVar.version_ = this.l;
            if ((i & 2) == 2) {
                i2 |= 2;
            }
            vVar.versionFull_ = this.m;
            if ((i & 4) == 4) {
                i2 |= 4;
            }
            vVar.level_ = this.n;
            if ((i & 8) == 8) {
                i2 |= 8;
            }
            vVar.errorCode_ = this.o;
            if ((i & 16) == 16) {
                i2 |= 16;
            }
            vVar.message_ = this.p;
            if ((i & 32) == 32) {
                i2 |= 32;
            }
            vVar.versionKind_ = this.q;
            vVar.bitField0_ = i2;
            return vVar;
        }

        public b setErrorCode(int i) {
            this.k |= 8;
            this.o = i;
            return this;
        }

        public b setLevel(c cVar) {
            Objects.requireNonNull(cVar);
            this.k |= 4;
            this.n = cVar;
            return this;
        }

        public b setMessage(int i) {
            this.k |= 16;
            this.p = i;
            return this;
        }

        public b setVersion(int i) {
            this.k |= 1;
            this.l = i;
            return this;
        }

        public b setVersionFull(int i) {
            this.k |= 2;
            this.m = i;
            return this;
        }

        public b setVersionKind(d dVar) {
            Objects.requireNonNull(dVar);
            this.k |= 32;
            this.q = dVar;
            return this;
        }

        @Override // d0.e0.p.d.m0.i.n.a
        public v build() {
            v buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw new UninitializedMessageException(buildPartial);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // d0.e0.p.d.m0.i.g.b
        public b clone() {
            return new b().mergeFrom(buildPartial());
        }

        public b mergeFrom(v vVar) {
            if (vVar == v.getDefaultInstance()) {
                return this;
            }
            if (vVar.hasVersion()) {
                setVersion(vVar.getVersion());
            }
            if (vVar.hasVersionFull()) {
                setVersionFull(vVar.getVersionFull());
            }
            if (vVar.hasLevel()) {
                setLevel(vVar.getLevel());
            }
            if (vVar.hasErrorCode()) {
                setErrorCode(vVar.getErrorCode());
            }
            if (vVar.hasMessage()) {
                setMessage(vVar.getMessage());
            }
            if (vVar.hasVersionKind()) {
                setVersionKind(vVar.getVersionKind());
            }
            setUnknownFields(getUnknownFields().concat(vVar.unknownFields));
            return this;
        }

        /* JADX WARN: Removed duplicated region for block: B:14:0x001d  */
        @Override // d0.e0.p.d.m0.i.a.AbstractC0335a, d0.e0.p.d.m0.i.n.a
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public d0.e0.p.d.m0.f.v.b mergeFrom(d0.e0.p.d.m0.i.d r3, d0.e0.p.d.m0.i.e r4) throws java.io.IOException {
            /*
                r2 = this;
                r0 = 0
                d0.e0.p.d.m0.i.p<d0.e0.p.d.m0.f.v> r1 = d0.e0.p.d.m0.f.v.k     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                d0.e0.p.d.m0.f.v r3 = (d0.e0.p.d.m0.f.v) r3     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                if (r3 == 0) goto Le
                r2.mergeFrom(r3)
            Le:
                return r2
            Lf:
                r3 = move-exception
                goto L1b
            L11:
                r3 = move-exception
                d0.e0.p.d.m0.i.n r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                d0.e0.p.d.m0.f.v r4 = (d0.e0.p.d.m0.f.v) r4     // Catch: java.lang.Throwable -> Lf
                throw r3     // Catch: java.lang.Throwable -> L19
            L19:
                r3 = move-exception
                r0 = r4
            L1b:
                if (r0 == 0) goto L20
                r2.mergeFrom(r0)
            L20:
                throw r3
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.f.v.b.mergeFrom(d0.e0.p.d.m0.i.d, d0.e0.p.d.m0.i.e):d0.e0.p.d.m0.f.v$b");
        }
    }

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public enum c implements h.a {
        WARNING(0),
        ERROR(1),
        HIDDEN(2);
        
        private final int value;

        c(int i) {
            this.value = i;
        }

        @Override // d0.e0.p.d.m0.i.h.a
        public final int getNumber() {
            return this.value;
        }

        public static c valueOf(int i) {
            if (i == 0) {
                return WARNING;
            }
            if (i == 1) {
                return ERROR;
            }
            if (i != 2) {
                return null;
            }
            return HIDDEN;
        }
    }

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public enum d implements h.a {
        LANGUAGE_VERSION(0),
        COMPILER_VERSION(1),
        API_VERSION(2);
        
        private final int value;

        d(int i) {
            this.value = i;
        }

        @Override // d0.e0.p.d.m0.i.h.a
        public final int getNumber() {
            return this.value;
        }

        public static d valueOf(int i) {
            if (i == 0) {
                return LANGUAGE_VERSION;
            }
            if (i == 1) {
                return COMPILER_VERSION;
            }
            if (i != 2) {
                return null;
            }
            return API_VERSION;
        }
    }

    static {
        v vVar = new v();
        j = vVar;
        vVar.version_ = 0;
        vVar.versionFull_ = 0;
        vVar.level_ = c.ERROR;
        vVar.errorCode_ = 0;
        vVar.message_ = 0;
        vVar.versionKind_ = d.LANGUAGE_VERSION;
    }

    public v(g.b bVar, d0.e0.p.d.m0.f.a aVar) {
        super(bVar);
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = bVar.getUnknownFields();
    }

    public static v getDefaultInstance() {
        return j;
    }

    public static b newBuilder(v vVar) {
        return newBuilder().mergeFrom(vVar);
    }

    public int getErrorCode() {
        return this.errorCode_;
    }

    public c getLevel() {
        return this.level_;
    }

    public int getMessage() {
        return this.message_;
    }

    @Override // d0.e0.p.d.m0.i.n
    public int getSerializedSize() {
        int i = this.memoizedSerializedSize;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.bitField0_ & 1) == 1) {
            i2 = 0 + CodedOutputStream.computeInt32Size(1, this.version_);
        }
        if ((this.bitField0_ & 2) == 2) {
            i2 += CodedOutputStream.computeInt32Size(2, this.versionFull_);
        }
        if ((this.bitField0_ & 4) == 4) {
            i2 += CodedOutputStream.computeEnumSize(3, this.level_.getNumber());
        }
        if ((this.bitField0_ & 8) == 8) {
            i2 += CodedOutputStream.computeInt32Size(4, this.errorCode_);
        }
        if ((this.bitField0_ & 16) == 16) {
            i2 += CodedOutputStream.computeInt32Size(5, this.message_);
        }
        if ((this.bitField0_ & 32) == 32) {
            i2 += CodedOutputStream.computeEnumSize(6, this.versionKind_.getNumber());
        }
        int size = this.unknownFields.size() + i2;
        this.memoizedSerializedSize = size;
        return size;
    }

    public int getVersion() {
        return this.version_;
    }

    public int getVersionFull() {
        return this.versionFull_;
    }

    public d getVersionKind() {
        return this.versionKind_;
    }

    public boolean hasErrorCode() {
        return (this.bitField0_ & 8) == 8;
    }

    public boolean hasLevel() {
        return (this.bitField0_ & 4) == 4;
    }

    public boolean hasMessage() {
        return (this.bitField0_ & 16) == 16;
    }

    public boolean hasVersion() {
        return (this.bitField0_ & 1) == 1;
    }

    public boolean hasVersionFull() {
        return (this.bitField0_ & 2) == 2;
    }

    public boolean hasVersionKind() {
        return (this.bitField0_ & 32) == 32;
    }

    @Override // d0.e0.p.d.m0.i.o
    public final boolean isInitialized() {
        byte b2 = this.memoizedIsInitialized;
        if (b2 == 1) {
            return true;
        }
        if (b2 == 0) {
            return false;
        }
        this.memoizedIsInitialized = (byte) 1;
        return true;
    }

    @Override // d0.e0.p.d.m0.i.n
    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        getSerializedSize();
        if ((this.bitField0_ & 1) == 1) {
            codedOutputStream.writeInt32(1, this.version_);
        }
        if ((this.bitField0_ & 2) == 2) {
            codedOutputStream.writeInt32(2, this.versionFull_);
        }
        if ((this.bitField0_ & 4) == 4) {
            codedOutputStream.writeEnum(3, this.level_.getNumber());
        }
        if ((this.bitField0_ & 8) == 8) {
            codedOutputStream.writeInt32(4, this.errorCode_);
        }
        if ((this.bitField0_ & 16) == 16) {
            codedOutputStream.writeInt32(5, this.message_);
        }
        if ((this.bitField0_ & 32) == 32) {
            codedOutputStream.writeEnum(6, this.versionKind_.getNumber());
        }
        codedOutputStream.writeRawBytes(this.unknownFields);
    }

    public static b newBuilder() {
        return new b();
    }

    @Override // d0.e0.p.d.m0.i.n
    public b newBuilderForType() {
        return newBuilder();
    }

    @Override // d0.e0.p.d.m0.i.n
    public b toBuilder() {
        return newBuilder(this);
    }

    public v() {
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = d0.e0.p.d.m0.i.c.j;
    }

    public v(d0.e0.p.d.m0.i.d dVar, e eVar, d0.e0.p.d.m0.f.a aVar) throws InvalidProtocolBufferException {
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        boolean z2 = false;
        this.version_ = 0;
        this.versionFull_ = 0;
        this.level_ = c.ERROR;
        this.errorCode_ = 0;
        this.message_ = 0;
        this.versionKind_ = d.LANGUAGE_VERSION;
        c.b newOutput = d0.e0.p.d.m0.i.c.newOutput();
        CodedOutputStream newInstance = CodedOutputStream.newInstance(newOutput, 1);
        while (!z2) {
            try {
                try {
                    try {
                        int readTag = dVar.readTag();
                        if (readTag != 0) {
                            if (readTag == 8) {
                                this.bitField0_ |= 1;
                                this.version_ = dVar.readInt32();
                            } else if (readTag == 16) {
                                this.bitField0_ |= 2;
                                this.versionFull_ = dVar.readInt32();
                            } else if (readTag == 24) {
                                int readEnum = dVar.readEnum();
                                c valueOf = c.valueOf(readEnum);
                                if (valueOf == null) {
                                    newInstance.writeRawVarint32(readTag);
                                    newInstance.writeRawVarint32(readEnum);
                                } else {
                                    this.bitField0_ |= 4;
                                    this.level_ = valueOf;
                                }
                            } else if (readTag == 32) {
                                this.bitField0_ |= 8;
                                this.errorCode_ = dVar.readInt32();
                            } else if (readTag == 40) {
                                this.bitField0_ |= 16;
                                this.message_ = dVar.readInt32();
                            } else if (readTag == 48) {
                                int readEnum2 = dVar.readEnum();
                                d valueOf2 = d.valueOf(readEnum2);
                                if (valueOf2 == null) {
                                    newInstance.writeRawVarint32(readTag);
                                    newInstance.writeRawVarint32(readEnum2);
                                } else {
                                    this.bitField0_ |= 32;
                                    this.versionKind_ = valueOf2;
                                }
                            } else if (!dVar.skipField(readTag, newInstance)) {
                            }
                        }
                        z2 = true;
                    } catch (IOException e) {
                        throw new InvalidProtocolBufferException(e.getMessage()).setUnfinishedMessage(this);
                    }
                } catch (InvalidProtocolBufferException e2) {
                    throw e2.setUnfinishedMessage(this);
                }
            } catch (Throwable th) {
                try {
                    newInstance.flush();
                } catch (IOException unused) {
                } catch (Throwable th2) {
                    this.unknownFields = newOutput.toByteString();
                    throw th2;
                }
                this.unknownFields = newOutput.toByteString();
                throw th;
            }
        }
        try {
            newInstance.flush();
        } catch (IOException unused2) {
        } catch (Throwable th3) {
            this.unknownFields = newOutput.toByteString();
            throw th3;
        }
        this.unknownFields = newOutput.toByteString();
    }
}
