package d0.e0.p.d.m0.f;

import d0.e0.p.d.m0.f.h;
import d0.e0.p.d.m0.i.e;
import d0.e0.p.d.m0.i.g;
import d0.e0.p.d.m0.i.h;
import d0.e0.p.d.m0.i.o;
import d0.e0.p.d.m0.i.p;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import kotlin.reflect.jvm.internal.impl.protobuf.CodedOutputStream;
import kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException;
import kotlin.reflect.jvm.internal.impl.protobuf.UninitializedMessageException;
/* compiled from: ProtoBuf.java */
/* loaded from: classes3.dex */
public final class f extends g implements o {
    public static final f j;
    public static p<f> k = new a();
    private int bitField0_;
    private h conclusionOfConditionalEffect_;
    private List<h> effectConstructorArgument_;
    private c effectType_;
    private d kind_;
    private byte memoizedIsInitialized;
    private int memoizedSerializedSize;
    private final d0.e0.p.d.m0.i.c unknownFields;

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static class a extends d0.e0.p.d.m0.i.b<f> {
        @Override // d0.e0.p.d.m0.i.p
        public f parsePartialFrom(d0.e0.p.d.m0.i.d dVar, e eVar) throws InvalidProtocolBufferException {
            return new f(dVar, eVar, null);
        }
    }

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static final class b extends g.b<f, b> implements o {
        public int k;
        public c l = c.RETURNS_CONSTANT;
        public List<h> m = Collections.emptyList();
        public h n = h.getDefaultInstance();
        public d o = d.AT_MOST_ONCE;

        public f buildPartial() {
            f fVar = new f(this, null);
            int i = this.k;
            int i2 = 1;
            if ((i & 1) != 1) {
                i2 = 0;
            }
            fVar.effectType_ = this.l;
            if ((this.k & 2) == 2) {
                this.m = Collections.unmodifiableList(this.m);
                this.k &= -3;
            }
            fVar.effectConstructorArgument_ = this.m;
            if ((i & 4) == 4) {
                i2 |= 2;
            }
            fVar.conclusionOfConditionalEffect_ = this.n;
            if ((i & 8) == 8) {
                i2 |= 4;
            }
            fVar.kind_ = this.o;
            fVar.bitField0_ = i2;
            return fVar;
        }

        public b mergeConclusionOfConditionalEffect(h hVar) {
            if ((this.k & 4) != 4 || this.n == h.getDefaultInstance()) {
                this.n = hVar;
            } else {
                this.n = h.newBuilder(this.n).mergeFrom(hVar).buildPartial();
            }
            this.k |= 4;
            return this;
        }

        public b setEffectType(c cVar) {
            Objects.requireNonNull(cVar);
            this.k |= 1;
            this.l = cVar;
            return this;
        }

        public b setKind(d dVar) {
            Objects.requireNonNull(dVar);
            this.k |= 8;
            this.o = dVar;
            return this;
        }

        @Override // d0.e0.p.d.m0.i.n.a
        public f build() {
            f buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw new UninitializedMessageException(buildPartial);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // d0.e0.p.d.m0.i.g.b
        public b clone() {
            return new b().mergeFrom(buildPartial());
        }

        public b mergeFrom(f fVar) {
            if (fVar == f.getDefaultInstance()) {
                return this;
            }
            if (fVar.hasEffectType()) {
                setEffectType(fVar.getEffectType());
            }
            if (!fVar.effectConstructorArgument_.isEmpty()) {
                if (this.m.isEmpty()) {
                    this.m = fVar.effectConstructorArgument_;
                    this.k &= -3;
                } else {
                    if ((this.k & 2) != 2) {
                        this.m = new ArrayList(this.m);
                        this.k |= 2;
                    }
                    this.m.addAll(fVar.effectConstructorArgument_);
                }
            }
            if (fVar.hasConclusionOfConditionalEffect()) {
                mergeConclusionOfConditionalEffect(fVar.getConclusionOfConditionalEffect());
            }
            if (fVar.hasKind()) {
                setKind(fVar.getKind());
            }
            setUnknownFields(getUnknownFields().concat(fVar.unknownFields));
            return this;
        }

        /* JADX WARN: Removed duplicated region for block: B:14:0x001d  */
        @Override // d0.e0.p.d.m0.i.a.AbstractC0335a, d0.e0.p.d.m0.i.n.a
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public d0.e0.p.d.m0.f.f.b mergeFrom(d0.e0.p.d.m0.i.d r3, d0.e0.p.d.m0.i.e r4) throws java.io.IOException {
            /*
                r2 = this;
                r0 = 0
                d0.e0.p.d.m0.i.p<d0.e0.p.d.m0.f.f> r1 = d0.e0.p.d.m0.f.f.k     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                d0.e0.p.d.m0.f.f r3 = (d0.e0.p.d.m0.f.f) r3     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                if (r3 == 0) goto Le
                r2.mergeFrom(r3)
            Le:
                return r2
            Lf:
                r3 = move-exception
                goto L1b
            L11:
                r3 = move-exception
                d0.e0.p.d.m0.i.n r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                d0.e0.p.d.m0.f.f r4 = (d0.e0.p.d.m0.f.f) r4     // Catch: java.lang.Throwable -> Lf
                throw r3     // Catch: java.lang.Throwable -> L19
            L19:
                r3 = move-exception
                r0 = r4
            L1b:
                if (r0 == 0) goto L20
                r2.mergeFrom(r0)
            L20:
                throw r3
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.f.f.b.mergeFrom(d0.e0.p.d.m0.i.d, d0.e0.p.d.m0.i.e):d0.e0.p.d.m0.f.f$b");
        }
    }

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public enum c implements h.a {
        RETURNS_CONSTANT(0),
        CALLS(1),
        RETURNS_NOT_NULL(2);
        
        private final int value;

        c(int i) {
            this.value = i;
        }

        @Override // d0.e0.p.d.m0.i.h.a
        public final int getNumber() {
            return this.value;
        }

        public static c valueOf(int i) {
            if (i == 0) {
                return RETURNS_CONSTANT;
            }
            if (i == 1) {
                return CALLS;
            }
            if (i != 2) {
                return null;
            }
            return RETURNS_NOT_NULL;
        }
    }

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public enum d implements h.a {
        AT_MOST_ONCE(0),
        EXACTLY_ONCE(1),
        AT_LEAST_ONCE(2);
        
        private final int value;

        d(int i) {
            this.value = i;
        }

        @Override // d0.e0.p.d.m0.i.h.a
        public final int getNumber() {
            return this.value;
        }

        public static d valueOf(int i) {
            if (i == 0) {
                return AT_MOST_ONCE;
            }
            if (i == 1) {
                return EXACTLY_ONCE;
            }
            if (i != 2) {
                return null;
            }
            return AT_LEAST_ONCE;
        }
    }

    static {
        f fVar = new f();
        j = fVar;
        fVar.h();
    }

    public f(g.b bVar, d0.e0.p.d.m0.f.a aVar) {
        super(bVar);
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = bVar.getUnknownFields();
    }

    public static f getDefaultInstance() {
        return j;
    }

    public static b newBuilder(f fVar) {
        return newBuilder().mergeFrom(fVar);
    }

    public h getConclusionOfConditionalEffect() {
        return this.conclusionOfConditionalEffect_;
    }

    public h getEffectConstructorArgument(int i) {
        return this.effectConstructorArgument_.get(i);
    }

    public int getEffectConstructorArgumentCount() {
        return this.effectConstructorArgument_.size();
    }

    public c getEffectType() {
        return this.effectType_;
    }

    public d getKind() {
        return this.kind_;
    }

    @Override // d0.e0.p.d.m0.i.n
    public int getSerializedSize() {
        int i = this.memoizedSerializedSize;
        if (i != -1) {
            return i;
        }
        int computeEnumSize = (this.bitField0_ & 1) == 1 ? CodedOutputStream.computeEnumSize(1, this.effectType_.getNumber()) + 0 : 0;
        for (int i2 = 0; i2 < this.effectConstructorArgument_.size(); i2++) {
            computeEnumSize += CodedOutputStream.computeMessageSize(2, this.effectConstructorArgument_.get(i2));
        }
        if ((this.bitField0_ & 2) == 2) {
            computeEnumSize += CodedOutputStream.computeMessageSize(3, this.conclusionOfConditionalEffect_);
        }
        if ((this.bitField0_ & 4) == 4) {
            computeEnumSize += CodedOutputStream.computeEnumSize(4, this.kind_.getNumber());
        }
        int size = this.unknownFields.size() + computeEnumSize;
        this.memoizedSerializedSize = size;
        return size;
    }

    public final void h() {
        this.effectType_ = c.RETURNS_CONSTANT;
        this.effectConstructorArgument_ = Collections.emptyList();
        this.conclusionOfConditionalEffect_ = h.getDefaultInstance();
        this.kind_ = d.AT_MOST_ONCE;
    }

    public boolean hasConclusionOfConditionalEffect() {
        return (this.bitField0_ & 2) == 2;
    }

    public boolean hasEffectType() {
        return (this.bitField0_ & 1) == 1;
    }

    public boolean hasKind() {
        return (this.bitField0_ & 4) == 4;
    }

    @Override // d0.e0.p.d.m0.i.o
    public final boolean isInitialized() {
        byte b2 = this.memoizedIsInitialized;
        if (b2 == 1) {
            return true;
        }
        if (b2 == 0) {
            return false;
        }
        for (int i = 0; i < getEffectConstructorArgumentCount(); i++) {
            if (!getEffectConstructorArgument(i).isInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
        }
        if (!hasConclusionOfConditionalEffect() || getConclusionOfConditionalEffect().isInitialized()) {
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }
        this.memoizedIsInitialized = (byte) 0;
        return false;
    }

    @Override // d0.e0.p.d.m0.i.n
    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        getSerializedSize();
        if ((this.bitField0_ & 1) == 1) {
            codedOutputStream.writeEnum(1, this.effectType_.getNumber());
        }
        for (int i = 0; i < this.effectConstructorArgument_.size(); i++) {
            codedOutputStream.writeMessage(2, this.effectConstructorArgument_.get(i));
        }
        if ((this.bitField0_ & 2) == 2) {
            codedOutputStream.writeMessage(3, this.conclusionOfConditionalEffect_);
        }
        if ((this.bitField0_ & 4) == 4) {
            codedOutputStream.writeEnum(4, this.kind_.getNumber());
        }
        codedOutputStream.writeRawBytes(this.unknownFields);
    }

    public static b newBuilder() {
        return new b();
    }

    @Override // d0.e0.p.d.m0.i.n
    public b newBuilderForType() {
        return newBuilder();
    }

    @Override // d0.e0.p.d.m0.i.n
    public b toBuilder() {
        return newBuilder(this);
    }

    public f() {
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = d0.e0.p.d.m0.i.c.j;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public f(d0.e0.p.d.m0.i.d dVar, e eVar, d0.e0.p.d.m0.f.a aVar) throws InvalidProtocolBufferException {
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        h();
        CodedOutputStream newInstance = CodedOutputStream.newInstance(d0.e0.p.d.m0.i.c.newOutput(), 1);
        boolean z2 = false;
        boolean z3 = false;
        while (!z2) {
            try {
                try {
                    int readTag = dVar.readTag();
                    if (readTag != 0) {
                        if (readTag == 8) {
                            int readEnum = dVar.readEnum();
                            c valueOf = c.valueOf(readEnum);
                            if (valueOf == null) {
                                newInstance.writeRawVarint32(readTag);
                                newInstance.writeRawVarint32(readEnum);
                            } else {
                                this.bitField0_ |= 1;
                                this.effectType_ = valueOf;
                            }
                        } else if (readTag == 18) {
                            if (!(z3 & true)) {
                                this.effectConstructorArgument_ = new ArrayList();
                                z3 |= true;
                            }
                            this.effectConstructorArgument_.add(dVar.readMessage(h.k, eVar));
                        } else if (readTag == 26) {
                            h.b builder = (this.bitField0_ & 2) == 2 ? this.conclusionOfConditionalEffect_.toBuilder() : null;
                            h hVar = (h) dVar.readMessage(h.k, eVar);
                            this.conclusionOfConditionalEffect_ = hVar;
                            if (builder != null) {
                                builder.mergeFrom(hVar);
                                this.conclusionOfConditionalEffect_ = builder.buildPartial();
                            }
                            this.bitField0_ |= 2;
                        } else if (readTag == 32) {
                            int readEnum2 = dVar.readEnum();
                            d valueOf2 = d.valueOf(readEnum2);
                            if (valueOf2 == null) {
                                newInstance.writeRawVarint32(readTag);
                                newInstance.writeRawVarint32(readEnum2);
                            } else {
                                this.bitField0_ |= 4;
                                this.kind_ = valueOf2;
                            }
                        } else if (!dVar.skipField(readTag, newInstance)) {
                        }
                    }
                    z2 = true;
                } catch (Throwable th) {
                    if (z3 & true) {
                        this.effectConstructorArgument_ = Collections.unmodifiableList(this.effectConstructorArgument_);
                    }
                    try {
                        newInstance.flush();
                    } catch (IOException unused) {
                        throw th;
                    } finally {
                    }
                }
            } catch (InvalidProtocolBufferException e) {
                throw e.setUnfinishedMessage(this);
            } catch (IOException e2) {
                throw new InvalidProtocolBufferException(e2.getMessage()).setUnfinishedMessage(this);
            }
        }
        if (z3 & true) {
            this.effectConstructorArgument_ = Collections.unmodifiableList(this.effectConstructorArgument_);
        }
        try {
            newInstance.flush();
        } catch (IOException unused2) {
        } finally {
        }
    }
}
