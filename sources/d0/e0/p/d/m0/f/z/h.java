package d0.e0.p.d.m0.f.z;

import andhook.lib.xposed.ClassUtils;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.f.c;
import d0.e0.p.d.m0.f.d;
import d0.e0.p.d.m0.f.i;
import d0.e0.p.d.m0.f.r;
import d0.e0.p.d.m0.f.v;
import d0.e0.p.d.m0.i.n;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.objectweb.asm.Opcodes;
/* compiled from: VersionRequirement.kt */
/* loaded from: classes3.dex */
public final class h {
    public static final a a = new a(null);

    /* renamed from: b  reason: collision with root package name */
    public final b f3391b;
    public final v.d c;
    public final d0.a d;
    public final Integer e;
    public final String f;

    /* compiled from: VersionRequirement.kt */
    /* loaded from: classes3.dex */
    public static final class b {
        public static final a a = new a(null);

        /* renamed from: b  reason: collision with root package name */
        public static final b f3392b = new b(256, 256, 256);
        public final int c;
        public final int d;
        public final int e;

        /* compiled from: VersionRequirement.kt */
        /* loaded from: classes3.dex */
        public static final class a {
            public a(DefaultConstructorMarker defaultConstructorMarker) {
            }

            public final b decode(Integer num, Integer num2) {
                if (num2 != null) {
                    return new b(num2.intValue() & 255, (num2.intValue() >> 8) & 255, (num2.intValue() >> 16) & 255);
                }
                if (num != null) {
                    return new b(num.intValue() & 7, (num.intValue() >> 3) & 15, (num.intValue() >> 7) & Opcodes.LAND);
                }
                return b.f3392b;
            }
        }

        public b(int i, int i2, int i3) {
            this.c = i;
            this.d = i2;
            this.e = i3;
        }

        public final String asString() {
            int i;
            StringBuilder sb;
            if (this.e == 0) {
                sb = new StringBuilder();
                sb.append(this.c);
                sb.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
                i = this.d;
            } else {
                sb = new StringBuilder();
                sb.append(this.c);
                sb.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
                sb.append(this.d);
                sb.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
                i = this.e;
            }
            sb.append(i);
            return sb.toString();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return this.c == bVar.c && this.d == bVar.d && this.e == bVar.e;
        }

        public int hashCode() {
            return (((this.c * 31) + this.d) * 31) + this.e;
        }

        public String toString() {
            return asString();
        }

        public /* synthetic */ b(int i, int i2, int i3, int i4, DefaultConstructorMarker defaultConstructorMarker) {
            this(i, i2, (i4 & 4) != 0 ? 0 : i3);
        }
    }

    public h(b bVar, v.d dVar, d0.a aVar, Integer num, String str) {
        m.checkNotNullParameter(bVar, "version");
        m.checkNotNullParameter(dVar, "kind");
        m.checkNotNullParameter(aVar, "level");
        this.f3391b = bVar;
        this.c = dVar;
        this.d = aVar;
        this.e = num;
        this.f = str;
    }

    public final v.d getKind() {
        return this.c;
    }

    public final b getVersion() {
        return this.f3391b;
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("since ");
        R.append(this.f3391b);
        R.append(' ');
        R.append(this.d);
        Integer num = this.e;
        String str = "";
        R.append(num != null ? m.stringPlus(" error ", num) : str);
        String str2 = this.f;
        if (str2 != null) {
            str = m.stringPlus(": ", str2);
        }
        R.append(str);
        return R.toString();
    }

    /* compiled from: VersionRequirement.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final List<h> create(n nVar, c cVar, i iVar) {
            List<Integer> list;
            m.checkNotNullParameter(nVar, "proto");
            m.checkNotNullParameter(cVar, "nameResolver");
            m.checkNotNullParameter(iVar, "table");
            if (nVar instanceof c) {
                list = ((c) nVar).getVersionRequirementList();
            } else if (nVar instanceof d) {
                list = ((d) nVar).getVersionRequirementList();
            } else if (nVar instanceof i) {
                list = ((i) nVar).getVersionRequirementList();
            } else if (nVar instanceof d0.e0.p.d.m0.f.n) {
                list = ((d0.e0.p.d.m0.f.n) nVar).getVersionRequirementList();
            } else if (nVar instanceof r) {
                list = ((r) nVar).getVersionRequirementList();
            } else {
                throw new IllegalStateException(m.stringPlus("Unexpected declaration: ", nVar.getClass()));
            }
            ArrayList Y = b.d.b.a.a.Y(list, "ids");
            for (Integer num : list) {
                m.checkNotNullExpressionValue(num, ModelAuditLogEntry.CHANGE_KEY_ID);
                h create = create(num.intValue(), cVar, iVar);
                if (create != null) {
                    Y.add(create);
                }
            }
            return Y;
        }

        public final h create(int i, c cVar, i iVar) {
            d0.a aVar;
            m.checkNotNullParameter(cVar, "nameResolver");
            m.checkNotNullParameter(iVar, "table");
            v vVar = iVar.get(i);
            String str = null;
            if (vVar == null) {
                return null;
            }
            b decode = b.a.decode(vVar.hasVersion() ? Integer.valueOf(vVar.getVersion()) : null, vVar.hasVersionFull() ? Integer.valueOf(vVar.getVersionFull()) : null);
            v.c level = vVar.getLevel();
            m.checkNotNull(level);
            int ordinal = level.ordinal();
            if (ordinal == 0) {
                aVar = d0.a.WARNING;
            } else if (ordinal == 1) {
                aVar = d0.a.ERROR;
            } else if (ordinal == 2) {
                aVar = d0.a.HIDDEN;
            } else {
                throw new NoWhenBranchMatchedException();
            }
            d0.a aVar2 = aVar;
            Integer valueOf = vVar.hasErrorCode() ? Integer.valueOf(vVar.getErrorCode()) : null;
            if (vVar.hasMessage()) {
                str = cVar.getString(vVar.getMessage());
            }
            v.d versionKind = vVar.getVersionKind();
            m.checkNotNullExpressionValue(versionKind, "info.versionKind");
            return new h(decode, versionKind, aVar2, valueOf, str);
        }
    }
}
