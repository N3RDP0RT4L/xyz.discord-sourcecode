package d0.e0.p.d.m0.f.z;

import d0.e0.p.d.m0.f.c;
import d0.e0.p.d.m0.f.j;
import d0.e0.p.d.m0.f.k;
import d0.e0.p.d.m0.f.x;
import d0.e0.p.d.m0.i.h;
/* compiled from: Flags.java */
/* loaded from: classes3.dex */
public class b {
    public static final C0334b A;
    public static final C0334b B;
    public static final C0334b C;
    public static final C0334b D;
    public static final C0334b E;
    public static final C0334b F;
    public static final C0334b G;
    public static final C0334b H;
    public static final C0334b I;
    public static final C0334b J;
    public static final C0334b K;
    public static final C0334b L;

    /* renamed from: b  reason: collision with root package name */
    public static final C0334b f3384b;
    public static final d<x> c;
    public static final d<k> d;
    public static final d<c.EnumC0329c> e;
    public static final C0334b f;
    public static final C0334b g;
    public static final C0334b h;
    public static final C0334b i;
    public static final C0334b j;
    public static final C0334b k;
    public static final C0334b l;
    public static final C0334b m;
    public static final d<j> n;
    public static final C0334b o;
    public static final C0334b p;
    public static final C0334b q;
    public static final C0334b r;

    /* renamed from: s  reason: collision with root package name */
    public static final C0334b f3385s;
    public static final C0334b t;
    public static final C0334b u;
    public static final C0334b v;
    public static final C0334b w;

    /* renamed from: x  reason: collision with root package name */
    public static final C0334b f3386x;

    /* renamed from: y  reason: collision with root package name */
    public static final C0334b f3387y;

    /* renamed from: z  reason: collision with root package name */
    public static final C0334b f3388z;
    public static final C0334b a = d.booleanFirst();
    public static final C0334b M = d.booleanFirst();

    /* compiled from: Flags.java */
    /* renamed from: d0.e0.p.d.m0.f.z.b$b  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static class C0334b extends d<Boolean> {
        public C0334b(int i) {
            super(i, 1, null);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // d0.e0.p.d.m0.f.z.b.d
        public Boolean get(int i) {
            Boolean valueOf = Boolean.valueOf((i & (1 << this.a)) != 0);
            if (valueOf != null) {
                return valueOf;
            }
            throw new IllegalStateException(String.format("@NotNull method %s.%s must not return null", "kotlin/reflect/jvm/internal/impl/metadata/deserialization/Flags$BooleanFlagField", "get"));
        }

        public int toFlags(Boolean bool) {
            if (bool.booleanValue()) {
                return 1 << this.a;
            }
            return 0;
        }
    }

    /* compiled from: Flags.java */
    /* loaded from: classes3.dex */
    public static class c<E extends h.a> extends d<E> {
        public final E[] c;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public c(int r5, E[] r6) {
            /*
                r4 = this;
                r0 = 1
                if (r6 == 0) goto L33
                int r1 = r6.length
                int r1 = r1 - r0
                if (r1 != 0) goto L8
                goto L12
            L8:
                r2 = 31
            La:
                if (r2 < 0) goto L1c
                int r3 = r0 << r2
                r3 = r3 & r1
                if (r3 == 0) goto L19
                int r0 = r0 + r2
            L12:
                r1 = 0
                r4.<init>(r5, r0, r1)
                r4.c = r6
                return
            L19:
                int r2 = r2 + (-1)
                goto La
            L1c:
                java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
                java.lang.String r0 = "Empty enum: "
                java.lang.StringBuilder r0 = b.d.b.a.a.R(r0)
                java.lang.Class r6 = r6.getClass()
                r0.append(r6)
                java.lang.String r6 = r0.toString()
                r5.<init>(r6)
                throw r5
            L33:
                r5 = 3
                java.lang.Object[] r5 = new java.lang.Object[r5]
                r6 = 0
                java.lang.String r1 = "enumEntries"
                r5[r6] = r1
                java.lang.String r6 = "kotlin/reflect/jvm/internal/impl/metadata/deserialization/Flags$EnumLiteFlagField"
                r5[r0] = r6
                r6 = 2
                java.lang.String r0 = "bitWidth"
                r5[r6] = r0
                java.lang.String r6 = "Argument for @NotNull parameter '%s' of %s.%s must not be null"
                java.lang.String r5 = java.lang.String.format(r6, r5)
                java.lang.IllegalArgumentException r6 = new java.lang.IllegalArgumentException
                r6.<init>(r5)
                throw r6
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.f.z.b.c.<init>(int, d0.e0.p.d.m0.i.h$a[]):void");
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // d0.e0.p.d.m0.f.z.b.d
        public /* bridge */ /* synthetic */ int toFlags(Object obj) {
            return toFlags((c<E>) ((h.a) obj));
        }

        @Override // d0.e0.p.d.m0.f.z.b.d
        public E get(int i) {
            E[] eArr;
            int i2 = this.a;
            int i3 = (i & (((1 << this.f3389b) - 1) << i2)) >> i2;
            for (E e : this.c) {
                if (e.getNumber() == i3) {
                    return e;
                }
            }
            return null;
        }

        public int toFlags(E e) {
            return e.getNumber() << this.a;
        }
    }

    /* compiled from: Flags.java */
    /* loaded from: classes3.dex */
    public static abstract class d<E> {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final int f3389b;

        public d(int i, int i2, a aVar) {
            this.a = i;
            this.f3389b = i2;
        }

        /* JADX WARN: Incorrect types in method signature: <E::Ld0/e0/p/d/m0/i/h$a;>(Ld0/e0/p/d/m0/f/z/b$d<*>;[TE;)Ld0/e0/p/d/m0/f/z/b$d<TE;>; */
        public static d after(d dVar, h.a[] aVarArr) {
            return new c(dVar.a + dVar.f3389b, aVarArr);
        }

        public static C0334b booleanAfter(d<?> dVar) {
            return new C0334b(dVar.a + dVar.f3389b);
        }

        public static C0334b booleanFirst() {
            return new C0334b(0);
        }

        public abstract E get(int i);

        public abstract int toFlags(E e);
    }

    static {
        C0334b booleanFirst = d.booleanFirst();
        f3384b = booleanFirst;
        d<x> after = d.after(booleanFirst, x.values());
        c = after;
        d<k> after2 = d.after(after, k.values());
        d = after2;
        d<c.EnumC0329c> after3 = d.after(after2, c.EnumC0329c.values());
        e = after3;
        C0334b booleanAfter = d.booleanAfter(after3);
        f = booleanAfter;
        C0334b booleanAfter2 = d.booleanAfter(booleanAfter);
        g = booleanAfter2;
        C0334b booleanAfter3 = d.booleanAfter(booleanAfter2);
        h = booleanAfter3;
        C0334b booleanAfter4 = d.booleanAfter(booleanAfter3);
        i = booleanAfter4;
        C0334b booleanAfter5 = d.booleanAfter(booleanAfter4);
        j = booleanAfter5;
        k = d.booleanAfter(booleanAfter5);
        C0334b booleanAfter6 = d.booleanAfter(after);
        l = booleanAfter6;
        m = d.booleanAfter(booleanAfter6);
        d<j> after4 = d.after(after2, j.values());
        n = after4;
        C0334b booleanAfter7 = d.booleanAfter(after4);
        o = booleanAfter7;
        C0334b booleanAfter8 = d.booleanAfter(booleanAfter7);
        p = booleanAfter8;
        C0334b booleanAfter9 = d.booleanAfter(booleanAfter8);
        q = booleanAfter9;
        C0334b booleanAfter10 = d.booleanAfter(booleanAfter9);
        r = booleanAfter10;
        C0334b booleanAfter11 = d.booleanAfter(booleanAfter10);
        f3385s = booleanAfter11;
        C0334b booleanAfter12 = d.booleanAfter(booleanAfter11);
        t = booleanAfter12;
        C0334b booleanAfter13 = d.booleanAfter(booleanAfter12);
        u = booleanAfter13;
        v = d.booleanAfter(booleanAfter13);
        C0334b booleanAfter14 = d.booleanAfter(after4);
        w = booleanAfter14;
        C0334b booleanAfter15 = d.booleanAfter(booleanAfter14);
        f3386x = booleanAfter15;
        C0334b booleanAfter16 = d.booleanAfter(booleanAfter15);
        f3387y = booleanAfter16;
        C0334b booleanAfter17 = d.booleanAfter(booleanAfter16);
        f3388z = booleanAfter17;
        C0334b booleanAfter18 = d.booleanAfter(booleanAfter17);
        A = booleanAfter18;
        C0334b booleanAfter19 = d.booleanAfter(booleanAfter18);
        B = booleanAfter19;
        C0334b booleanAfter20 = d.booleanAfter(booleanAfter19);
        C = booleanAfter20;
        C0334b booleanAfter21 = d.booleanAfter(booleanAfter20);
        D = booleanAfter21;
        E = d.booleanAfter(booleanAfter21);
        C0334b booleanAfter22 = d.booleanAfter(booleanFirst);
        F = booleanAfter22;
        C0334b booleanAfter23 = d.booleanAfter(booleanAfter22);
        G = booleanAfter23;
        H = d.booleanAfter(booleanAfter23);
        C0334b booleanAfter24 = d.booleanAfter(after2);
        I = booleanAfter24;
        C0334b booleanAfter25 = d.booleanAfter(booleanAfter24);
        J = booleanAfter25;
        K = d.booleanAfter(booleanAfter25);
        C0334b booleanFirst2 = d.booleanFirst();
        L = booleanFirst2;
        d.booleanAfter(booleanFirst2);
    }

    /* JADX WARN: Removed duplicated region for block: B:21:0x0036  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x003b  */
    /* JADX WARN: Removed duplicated region for block: B:23:0x0040  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x0045  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x004a  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static /* synthetic */ void a(int r5) {
        /*
            r0 = 3
            java.lang.Object[] r0 = new java.lang.Object[r0]
            r1 = 1
            r2 = 0
            r3 = 2
            if (r5 == r1) goto L2b
            if (r5 == r3) goto L26
            r4 = 5
            if (r5 == r4) goto L2b
            r4 = 6
            if (r5 == r4) goto L21
            r4 = 8
            if (r5 == r4) goto L2b
            r4 = 9
            if (r5 == r4) goto L21
            r4 = 11
            if (r5 == r4) goto L2b
            java.lang.String r4 = "visibility"
            r0[r2] = r4
            goto L2f
        L21:
            java.lang.String r4 = "memberKind"
            r0[r2] = r4
            goto L2f
        L26:
            java.lang.String r4 = "kind"
            r0[r2] = r4
            goto L2f
        L2b:
            java.lang.String r4 = "modality"
            r0[r2] = r4
        L2f:
            java.lang.String r2 = "kotlin/reflect/jvm/internal/impl/metadata/deserialization/Flags"
            r0[r1] = r2
            switch(r5) {
                case 3: goto L4a;
                case 4: goto L45;
                case 5: goto L45;
                case 6: goto L45;
                case 7: goto L40;
                case 8: goto L40;
                case 9: goto L40;
                case 10: goto L3b;
                case 11: goto L3b;
                default: goto L36;
            }
        L36:
            java.lang.String r5 = "getClassFlags"
            r0[r3] = r5
            goto L4e
        L3b:
            java.lang.String r5 = "getAccessorFlags"
            r0[r3] = r5
            goto L4e
        L40:
            java.lang.String r5 = "getPropertyFlags"
            r0[r3] = r5
            goto L4e
        L45:
            java.lang.String r5 = "getFunctionFlags"
            r0[r3] = r5
            goto L4e
        L4a:
            java.lang.String r5 = "getConstructorFlags"
            r0[r3] = r5
        L4e:
            java.lang.String r5 = "Argument for @NotNull parameter '%s' of %s.%s must not be null"
            java.lang.String r5 = java.lang.String.format(r5, r0)
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.f.z.b.a(int):void");
    }

    public static int getAccessorFlags(boolean z2, x xVar, k kVar, boolean z3, boolean z4, boolean z5) {
        if (xVar == null) {
            a(10);
            throw null;
        } else if (kVar != null) {
            return f3384b.toFlags(Boolean.valueOf(z2)) | d.toFlags(kVar) | c.toFlags(xVar) | I.toFlags(Boolean.valueOf(z3)) | J.toFlags(Boolean.valueOf(z4)) | K.toFlags(Boolean.valueOf(z5));
        } else {
            a(11);
            throw null;
        }
    }
}
