package d0.e0.p.d.m0.f.z;

import com.discord.widgets.chat.input.MentionUtilsKt;
import com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel;
import d0.e0.p.d.m0.f.o;
import d0.e0.p.d.m0.f.p;
import d0.t.u;
import d0.z.d.m;
import java.util.LinkedList;
import java.util.List;
import kotlin.Triple;
/* compiled from: NameResolverImpl.kt */
/* loaded from: classes3.dex */
public final class d implements c {
    public final p a;

    /* renamed from: b  reason: collision with root package name */
    public final o f3390b;

    public d(p pVar, o oVar) {
        m.checkNotNullParameter(pVar, "strings");
        m.checkNotNullParameter(oVar, "qualifiedNames");
        this.a = pVar;
        this.f3390b = oVar;
    }

    public final Triple<List<String>, List<String>, Boolean> a(int i) {
        LinkedList linkedList = new LinkedList();
        LinkedList linkedList2 = new LinkedList();
        boolean z2 = false;
        while (i != -1) {
            o.c qualifiedName = this.f3390b.getQualifiedName(i);
            String string = this.a.getString(qualifiedName.getShortName());
            o.c.EnumC0330c kind = qualifiedName.getKind();
            m.checkNotNull(kind);
            int ordinal = kind.ordinal();
            if (ordinal == 0) {
                linkedList2.addFirst(string);
            } else if (ordinal == 1) {
                linkedList.addFirst(string);
            } else if (ordinal == 2) {
                linkedList2.addFirst(string);
                z2 = true;
            }
            i = qualifiedName.getParentQualifiedName();
        }
        return new Triple<>(linkedList, linkedList2, Boolean.valueOf(z2));
    }

    @Override // d0.e0.p.d.m0.f.z.c
    public String getQualifiedClassName(int i) {
        Triple<List<String>, List<String>, Boolean> a = a(i);
        List<String> component1 = a.component1();
        String joinToString$default = u.joinToString$default(a.component2(), ".", null, null, 0, null, null, 62, null);
        if (component1.isEmpty()) {
            return joinToString$default;
        }
        return u.joinToString$default(component1, AutocompleteViewModel.COMMAND_DISCOVER_TOKEN, null, null, 0, null, null, 62, null) + MentionUtilsKt.SLASH_CHAR + joinToString$default;
    }

    @Override // d0.e0.p.d.m0.f.z.c
    public String getString(int i) {
        String string = this.a.getString(i);
        m.checkNotNullExpressionValue(string, "strings.getString(index)");
        return string;
    }

    @Override // d0.e0.p.d.m0.f.z.c
    public boolean isLocalClassName(int i) {
        return a(i).getThird().booleanValue();
    }
}
