package d0.e0.p.d.m0.f.z;
/* compiled from: NameResolver.kt */
/* loaded from: classes3.dex */
public interface c {
    String getQualifiedClassName(int i);

    String getString(int i);

    boolean isLocalClassName(int i);
}
