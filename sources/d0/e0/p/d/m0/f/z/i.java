package d0.e0.p.d.m0.f.z;

import d0.e0.p.d.m0.f.v;
import d0.e0.p.d.m0.f.w;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.util.List;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: VersionRequirement.kt */
/* loaded from: classes3.dex */
public final class i {
    public static final a a = new a(null);

    /* renamed from: b  reason: collision with root package name */
    public static final i f3393b = new i(n.emptyList());
    public final List<v> c;

    /* compiled from: VersionRequirement.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final i create(w wVar) {
            m.checkNotNullParameter(wVar, "table");
            if (wVar.getRequirementCount() == 0) {
                return getEMPTY();
            }
            List<v> requirementList = wVar.getRequirementList();
            m.checkNotNullExpressionValue(requirementList, "table.requirementList");
            return new i(requirementList, null);
        }

        public final i getEMPTY() {
            return i.f3393b;
        }
    }

    public i(List<v> list) {
        this.c = list;
    }

    public final v get(int i) {
        return (v) u.getOrNull(this.c, i);
    }

    public i(List list, DefaultConstructorMarker defaultConstructorMarker) {
        this.c = list;
    }
}
