package d0.e0.p.d.m0.f;

import d0.e0.p.d.m0.f.q;
import d0.e0.p.d.m0.f.u;
import d0.e0.p.d.m0.i.c;
import d0.e0.p.d.m0.i.d;
import d0.e0.p.d.m0.i.e;
import d0.e0.p.d.m0.i.g;
import d0.e0.p.d.m0.i.o;
import d0.e0.p.d.m0.i.p;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import kotlin.reflect.jvm.internal.impl.protobuf.CodedOutputStream;
import kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException;
import kotlin.reflect.jvm.internal.impl.protobuf.UninitializedMessageException;
/* compiled from: ProtoBuf.java */
/* loaded from: classes3.dex */
public final class n extends g.d<n> implements o {
    public static final n j;
    public static p<n> k = new a();
    private int bitField0_;
    private int flags_;
    private int getterFlags_;
    private byte memoizedIsInitialized;
    private int memoizedSerializedSize;
    private int name_;
    private int oldFlags_;
    private int receiverTypeId_;
    private q receiverType_;
    private int returnTypeId_;
    private q returnType_;
    private int setterFlags_;
    private u setterValueParameter_;
    private List<s> typeParameter_;
    private final c unknownFields;
    private List<Integer> versionRequirement_;

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static class a extends d0.e0.p.d.m0.i.b<n> {
        @Override // d0.e0.p.d.m0.i.p
        public n parsePartialFrom(d dVar, e eVar) throws InvalidProtocolBufferException {
            return new n(dVar, eVar, null);
        }
    }

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static final class b extends g.c<n, b> implements o {
        public int m;
        public int p;
        public int r;
        public int u;
        public int w;

        /* renamed from: x  reason: collision with root package name */
        public int f3373x;
        public int n = 518;
        public int o = 2054;
        public q q = q.getDefaultInstance();

        /* renamed from: s  reason: collision with root package name */
        public List<s> f3372s = Collections.emptyList();
        public q t = q.getDefaultInstance();
        public u v = u.getDefaultInstance();

        /* renamed from: y  reason: collision with root package name */
        public List<Integer> f3374y = Collections.emptyList();

        public n buildPartial() {
            n nVar = new n(this, null);
            int i = this.m;
            int i2 = 1;
            if ((i & 1) != 1) {
                i2 = 0;
            }
            nVar.flags_ = this.n;
            if ((i & 2) == 2) {
                i2 |= 2;
            }
            nVar.oldFlags_ = this.o;
            if ((i & 4) == 4) {
                i2 |= 4;
            }
            nVar.name_ = this.p;
            if ((i & 8) == 8) {
                i2 |= 8;
            }
            nVar.returnType_ = this.q;
            if ((i & 16) == 16) {
                i2 |= 16;
            }
            nVar.returnTypeId_ = this.r;
            if ((this.m & 32) == 32) {
                this.f3372s = Collections.unmodifiableList(this.f3372s);
                this.m &= -33;
            }
            nVar.typeParameter_ = this.f3372s;
            if ((i & 64) == 64) {
                i2 |= 32;
            }
            nVar.receiverType_ = this.t;
            if ((i & 128) == 128) {
                i2 |= 64;
            }
            nVar.receiverTypeId_ = this.u;
            if ((i & 256) == 256) {
                i2 |= 128;
            }
            nVar.setterValueParameter_ = this.v;
            if ((i & 512) == 512) {
                i2 |= 256;
            }
            nVar.getterFlags_ = this.w;
            if ((i & 1024) == 1024) {
                i2 |= 512;
            }
            nVar.setterFlags_ = this.f3373x;
            if ((this.m & 2048) == 2048) {
                this.f3374y = Collections.unmodifiableList(this.f3374y);
                this.m &= -2049;
            }
            nVar.versionRequirement_ = this.f3374y;
            nVar.bitField0_ = i2;
            return nVar;
        }

        public b mergeReceiverType(q qVar) {
            if ((this.m & 64) != 64 || this.t == q.getDefaultInstance()) {
                this.t = qVar;
            } else {
                this.t = q.newBuilder(this.t).mergeFrom(qVar).buildPartial();
            }
            this.m |= 64;
            return this;
        }

        public b mergeReturnType(q qVar) {
            if ((this.m & 8) != 8 || this.q == q.getDefaultInstance()) {
                this.q = qVar;
            } else {
                this.q = q.newBuilder(this.q).mergeFrom(qVar).buildPartial();
            }
            this.m |= 8;
            return this;
        }

        public b mergeSetterValueParameter(u uVar) {
            if ((this.m & 256) != 256 || this.v == u.getDefaultInstance()) {
                this.v = uVar;
            } else {
                this.v = u.newBuilder(this.v).mergeFrom(uVar).buildPartial();
            }
            this.m |= 256;
            return this;
        }

        public b setFlags(int i) {
            this.m |= 1;
            this.n = i;
            return this;
        }

        public b setGetterFlags(int i) {
            this.m |= 512;
            this.w = i;
            return this;
        }

        public b setName(int i) {
            this.m |= 4;
            this.p = i;
            return this;
        }

        public b setOldFlags(int i) {
            this.m |= 2;
            this.o = i;
            return this;
        }

        public b setReceiverTypeId(int i) {
            this.m |= 128;
            this.u = i;
            return this;
        }

        public b setReturnTypeId(int i) {
            this.m |= 16;
            this.r = i;
            return this;
        }

        public b setSetterFlags(int i) {
            this.m |= 1024;
            this.f3373x = i;
            return this;
        }

        @Override // d0.e0.p.d.m0.i.n.a
        public n build() {
            n buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw new UninitializedMessageException(buildPartial);
        }

        @Override // d0.e0.p.d.m0.i.g.b
        public b clone() {
            return new b().mergeFrom(buildPartial());
        }

        public b mergeFrom(n nVar) {
            if (nVar == n.getDefaultInstance()) {
                return this;
            }
            if (nVar.hasFlags()) {
                setFlags(nVar.getFlags());
            }
            if (nVar.hasOldFlags()) {
                setOldFlags(nVar.getOldFlags());
            }
            if (nVar.hasName()) {
                setName(nVar.getName());
            }
            if (nVar.hasReturnType()) {
                mergeReturnType(nVar.getReturnType());
            }
            if (nVar.hasReturnTypeId()) {
                setReturnTypeId(nVar.getReturnTypeId());
            }
            if (!nVar.typeParameter_.isEmpty()) {
                if (this.f3372s.isEmpty()) {
                    this.f3372s = nVar.typeParameter_;
                    this.m &= -33;
                } else {
                    if ((this.m & 32) != 32) {
                        this.f3372s = new ArrayList(this.f3372s);
                        this.m |= 32;
                    }
                    this.f3372s.addAll(nVar.typeParameter_);
                }
            }
            if (nVar.hasReceiverType()) {
                mergeReceiverType(nVar.getReceiverType());
            }
            if (nVar.hasReceiverTypeId()) {
                setReceiverTypeId(nVar.getReceiverTypeId());
            }
            if (nVar.hasSetterValueParameter()) {
                mergeSetterValueParameter(nVar.getSetterValueParameter());
            }
            if (nVar.hasGetterFlags()) {
                setGetterFlags(nVar.getGetterFlags());
            }
            if (nVar.hasSetterFlags()) {
                setSetterFlags(nVar.getSetterFlags());
            }
            if (!nVar.versionRequirement_.isEmpty()) {
                if (this.f3374y.isEmpty()) {
                    this.f3374y = nVar.versionRequirement_;
                    this.m &= -2049;
                } else {
                    if ((this.m & 2048) != 2048) {
                        this.f3374y = new ArrayList(this.f3374y);
                        this.m |= 2048;
                    }
                    this.f3374y.addAll(nVar.versionRequirement_);
                }
            }
            b(nVar);
            setUnknownFields(getUnknownFields().concat(nVar.unknownFields));
            return this;
        }

        /* JADX WARN: Removed duplicated region for block: B:14:0x001d  */
        @Override // d0.e0.p.d.m0.i.a.AbstractC0335a, d0.e0.p.d.m0.i.n.a
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public d0.e0.p.d.m0.f.n.b mergeFrom(d0.e0.p.d.m0.i.d r3, d0.e0.p.d.m0.i.e r4) throws java.io.IOException {
            /*
                r2 = this;
                r0 = 0
                d0.e0.p.d.m0.i.p<d0.e0.p.d.m0.f.n> r1 = d0.e0.p.d.m0.f.n.k     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                d0.e0.p.d.m0.f.n r3 = (d0.e0.p.d.m0.f.n) r3     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                if (r3 == 0) goto Le
                r2.mergeFrom(r3)
            Le:
                return r2
            Lf:
                r3 = move-exception
                goto L1b
            L11:
                r3 = move-exception
                d0.e0.p.d.m0.i.n r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                d0.e0.p.d.m0.f.n r4 = (d0.e0.p.d.m0.f.n) r4     // Catch: java.lang.Throwable -> Lf
                throw r3     // Catch: java.lang.Throwable -> L19
            L19:
                r3 = move-exception
                r0 = r4
            L1b:
                if (r0 == 0) goto L20
                r2.mergeFrom(r0)
            L20:
                throw r3
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.f.n.b.mergeFrom(d0.e0.p.d.m0.i.d, d0.e0.p.d.m0.i.e):d0.e0.p.d.m0.f.n$b");
        }
    }

    static {
        n nVar = new n();
        j = nVar;
        nVar.x();
    }

    public n(g.c cVar, d0.e0.p.d.m0.f.a aVar) {
        super(cVar);
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = cVar.getUnknownFields();
    }

    public static n getDefaultInstance() {
        return j;
    }

    public static b newBuilder(n nVar) {
        return newBuilder().mergeFrom(nVar);
    }

    public int getFlags() {
        return this.flags_;
    }

    public int getGetterFlags() {
        return this.getterFlags_;
    }

    public int getName() {
        return this.name_;
    }

    public int getOldFlags() {
        return this.oldFlags_;
    }

    public q getReceiverType() {
        return this.receiverType_;
    }

    public int getReceiverTypeId() {
        return this.receiverTypeId_;
    }

    public q getReturnType() {
        return this.returnType_;
    }

    public int getReturnTypeId() {
        return this.returnTypeId_;
    }

    @Override // d0.e0.p.d.m0.i.n
    public int getSerializedSize() {
        int i = this.memoizedSerializedSize;
        if (i != -1) {
            return i;
        }
        int computeInt32Size = (this.bitField0_ & 2) == 2 ? CodedOutputStream.computeInt32Size(1, this.oldFlags_) + 0 : 0;
        if ((this.bitField0_ & 4) == 4) {
            computeInt32Size += CodedOutputStream.computeInt32Size(2, this.name_);
        }
        if ((this.bitField0_ & 8) == 8) {
            computeInt32Size += CodedOutputStream.computeMessageSize(3, this.returnType_);
        }
        for (int i2 = 0; i2 < this.typeParameter_.size(); i2++) {
            computeInt32Size += CodedOutputStream.computeMessageSize(4, this.typeParameter_.get(i2));
        }
        if ((this.bitField0_ & 32) == 32) {
            computeInt32Size += CodedOutputStream.computeMessageSize(5, this.receiverType_);
        }
        if ((this.bitField0_ & 128) == 128) {
            computeInt32Size += CodedOutputStream.computeMessageSize(6, this.setterValueParameter_);
        }
        if ((this.bitField0_ & 256) == 256) {
            computeInt32Size += CodedOutputStream.computeInt32Size(7, this.getterFlags_);
        }
        if ((this.bitField0_ & 512) == 512) {
            computeInt32Size += CodedOutputStream.computeInt32Size(8, this.setterFlags_);
        }
        if ((this.bitField0_ & 16) == 16) {
            computeInt32Size += CodedOutputStream.computeInt32Size(9, this.returnTypeId_);
        }
        if ((this.bitField0_ & 64) == 64) {
            computeInt32Size += CodedOutputStream.computeInt32Size(10, this.receiverTypeId_);
        }
        if ((this.bitField0_ & 1) == 1) {
            computeInt32Size += CodedOutputStream.computeInt32Size(11, this.flags_);
        }
        int i3 = 0;
        for (int i4 = 0; i4 < this.versionRequirement_.size(); i4++) {
            i3 += CodedOutputStream.computeInt32SizeNoTag(this.versionRequirement_.get(i4).intValue());
        }
        int size = this.unknownFields.size() + c() + (getVersionRequirementList().size() * 2) + computeInt32Size + i3;
        this.memoizedSerializedSize = size;
        return size;
    }

    public int getSetterFlags() {
        return this.setterFlags_;
    }

    public u getSetterValueParameter() {
        return this.setterValueParameter_;
    }

    public s getTypeParameter(int i) {
        return this.typeParameter_.get(i);
    }

    public int getTypeParameterCount() {
        return this.typeParameter_.size();
    }

    public List<s> getTypeParameterList() {
        return this.typeParameter_;
    }

    public List<Integer> getVersionRequirementList() {
        return this.versionRequirement_;
    }

    public boolean hasFlags() {
        return (this.bitField0_ & 1) == 1;
    }

    public boolean hasGetterFlags() {
        return (this.bitField0_ & 256) == 256;
    }

    public boolean hasName() {
        return (this.bitField0_ & 4) == 4;
    }

    public boolean hasOldFlags() {
        return (this.bitField0_ & 2) == 2;
    }

    public boolean hasReceiverType() {
        return (this.bitField0_ & 32) == 32;
    }

    public boolean hasReceiverTypeId() {
        return (this.bitField0_ & 64) == 64;
    }

    public boolean hasReturnType() {
        return (this.bitField0_ & 8) == 8;
    }

    public boolean hasReturnTypeId() {
        return (this.bitField0_ & 16) == 16;
    }

    public boolean hasSetterFlags() {
        return (this.bitField0_ & 512) == 512;
    }

    public boolean hasSetterValueParameter() {
        return (this.bitField0_ & 128) == 128;
    }

    @Override // d0.e0.p.d.m0.i.o
    public final boolean isInitialized() {
        byte b2 = this.memoizedIsInitialized;
        if (b2 == 1) {
            return true;
        }
        if (b2 == 0) {
            return false;
        }
        if (!hasName()) {
            this.memoizedIsInitialized = (byte) 0;
            return false;
        } else if (!hasReturnType() || getReturnType().isInitialized()) {
            for (int i = 0; i < getTypeParameterCount(); i++) {
                if (!getTypeParameter(i).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            if (hasReceiverType() && !getReceiverType().isInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            } else if (hasSetterValueParameter() && !getSetterValueParameter().isInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            } else if (!b()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            } else {
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }
        } else {
            this.memoizedIsInitialized = (byte) 0;
            return false;
        }
    }

    @Override // d0.e0.p.d.m0.i.n
    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        getSerializedSize();
        g.d<MessageType>.a e = e();
        if ((this.bitField0_ & 2) == 2) {
            codedOutputStream.writeInt32(1, this.oldFlags_);
        }
        if ((this.bitField0_ & 4) == 4) {
            codedOutputStream.writeInt32(2, this.name_);
        }
        if ((this.bitField0_ & 8) == 8) {
            codedOutputStream.writeMessage(3, this.returnType_);
        }
        for (int i = 0; i < this.typeParameter_.size(); i++) {
            codedOutputStream.writeMessage(4, this.typeParameter_.get(i));
        }
        if ((this.bitField0_ & 32) == 32) {
            codedOutputStream.writeMessage(5, this.receiverType_);
        }
        if ((this.bitField0_ & 128) == 128) {
            codedOutputStream.writeMessage(6, this.setterValueParameter_);
        }
        if ((this.bitField0_ & 256) == 256) {
            codedOutputStream.writeInt32(7, this.getterFlags_);
        }
        if ((this.bitField0_ & 512) == 512) {
            codedOutputStream.writeInt32(8, this.setterFlags_);
        }
        if ((this.bitField0_ & 16) == 16) {
            codedOutputStream.writeInt32(9, this.returnTypeId_);
        }
        if ((this.bitField0_ & 64) == 64) {
            codedOutputStream.writeInt32(10, this.receiverTypeId_);
        }
        if ((this.bitField0_ & 1) == 1) {
            codedOutputStream.writeInt32(11, this.flags_);
        }
        for (int i2 = 0; i2 < this.versionRequirement_.size(); i2++) {
            codedOutputStream.writeInt32(31, this.versionRequirement_.get(i2).intValue());
        }
        e.writeUntil(19000, codedOutputStream);
        codedOutputStream.writeRawBytes(this.unknownFields);
    }

    public final void x() {
        this.flags_ = 518;
        this.oldFlags_ = 2054;
        this.name_ = 0;
        this.returnType_ = q.getDefaultInstance();
        this.returnTypeId_ = 0;
        this.typeParameter_ = Collections.emptyList();
        this.receiverType_ = q.getDefaultInstance();
        this.receiverTypeId_ = 0;
        this.setterValueParameter_ = u.getDefaultInstance();
        this.getterFlags_ = 0;
        this.setterFlags_ = 0;
        this.versionRequirement_ = Collections.emptyList();
    }

    public static b newBuilder() {
        return new b();
    }

    @Override // d0.e0.p.d.m0.i.o
    public n getDefaultInstanceForType() {
        return j;
    }

    @Override // d0.e0.p.d.m0.i.n
    public b newBuilderForType() {
        return newBuilder();
    }

    @Override // d0.e0.p.d.m0.i.n
    public b toBuilder() {
        return newBuilder(this);
    }

    public n() {
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = c.j;
    }

    /* JADX WARN: Finally extract failed */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v0 */
    /* JADX WARN: Type inference failed for: r4v1 */
    /* JADX WARN: Type inference failed for: r4v2, types: [boolean] */
    public n(d dVar, e eVar, d0.e0.p.d.m0.f.a aVar) throws InvalidProtocolBufferException {
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        x();
        c.b newOutput = c.newOutput();
        CodedOutputStream newInstance = CodedOutputStream.newInstance(newOutput, 1);
        boolean z2 = false;
        boolean z3 = false;
        while (true) {
            ?? r4 = 32;
            if (!z2) {
                try {
                    try {
                        int readTag = dVar.readTag();
                        q.c cVar = null;
                        u.b builder = null;
                        q.c builder2 = null;
                        switch (readTag) {
                            case 0:
                                break;
                            case 8:
                                this.bitField0_ |= 2;
                                this.oldFlags_ = dVar.readInt32();
                                continue;
                            case 16:
                                this.bitField0_ |= 4;
                                this.name_ = dVar.readInt32();
                                continue;
                            case 26:
                                cVar = (this.bitField0_ & 8) == 8 ? this.returnType_.toBuilder() : cVar;
                                q qVar = (q) dVar.readMessage(q.k, eVar);
                                this.returnType_ = qVar;
                                if (cVar != null) {
                                    cVar.mergeFrom(qVar);
                                    this.returnType_ = cVar.buildPartial();
                                }
                                this.bitField0_ |= 8;
                                continue;
                            case 34:
                                if (!(z3 & true)) {
                                    this.typeParameter_ = new ArrayList();
                                    z3 |= true;
                                }
                                this.typeParameter_.add(dVar.readMessage(s.k, eVar));
                                continue;
                            case 42:
                                builder2 = (this.bitField0_ & 32) == 32 ? this.receiverType_.toBuilder() : builder2;
                                q qVar2 = (q) dVar.readMessage(q.k, eVar);
                                this.receiverType_ = qVar2;
                                if (builder2 != null) {
                                    builder2.mergeFrom(qVar2);
                                    this.receiverType_ = builder2.buildPartial();
                                }
                                this.bitField0_ |= 32;
                                continue;
                            case 50:
                                builder = (this.bitField0_ & 128) == 128 ? this.setterValueParameter_.toBuilder() : builder;
                                u uVar = (u) dVar.readMessage(u.k, eVar);
                                this.setterValueParameter_ = uVar;
                                if (builder != null) {
                                    builder.mergeFrom(uVar);
                                    this.setterValueParameter_ = builder.buildPartial();
                                }
                                this.bitField0_ |= 128;
                                continue;
                            case 56:
                                this.bitField0_ |= 256;
                                this.getterFlags_ = dVar.readInt32();
                                continue;
                            case 64:
                                this.bitField0_ |= 512;
                                this.setterFlags_ = dVar.readInt32();
                                continue;
                            case 72:
                                this.bitField0_ |= 16;
                                this.returnTypeId_ = dVar.readInt32();
                                continue;
                            case 80:
                                this.bitField0_ |= 64;
                                this.receiverTypeId_ = dVar.readInt32();
                                continue;
                            case 88:
                                this.bitField0_ |= 1;
                                this.flags_ = dVar.readInt32();
                                continue;
                            case 248:
                                if (!(z3 & true)) {
                                    this.versionRequirement_ = new ArrayList();
                                    z3 |= true;
                                }
                                this.versionRequirement_.add(Integer.valueOf(dVar.readInt32()));
                                continue;
                            case 250:
                                int pushLimit = dVar.pushLimit(dVar.readRawVarint32());
                                if (!(z3 & true) && dVar.getBytesUntilLimit() > 0) {
                                    this.versionRequirement_ = new ArrayList();
                                    z3 |= true;
                                }
                                while (dVar.getBytesUntilLimit() > 0) {
                                    this.versionRequirement_.add(Integer.valueOf(dVar.readInt32()));
                                }
                                dVar.popLimit(pushLimit);
                                continue;
                                break;
                            default:
                                r4 = f(dVar, newInstance, eVar, readTag);
                                if (r4 == 0) {
                                    break;
                                } else {
                                    continue;
                                }
                        }
                        z2 = true;
                    } catch (Throwable th) {
                        if ((z3 & true) == r4) {
                            this.typeParameter_ = Collections.unmodifiableList(this.typeParameter_);
                        }
                        if (z3 & true) {
                            this.versionRequirement_ = Collections.unmodifiableList(this.versionRequirement_);
                        }
                        try {
                            newInstance.flush();
                        } catch (IOException unused) {
                            this.unknownFields = newOutput.toByteString();
                            d();
                            throw th;
                        } catch (Throwable th2) {
                            this.unknownFields = newOutput.toByteString();
                            throw th2;
                        }
                    }
                } catch (InvalidProtocolBufferException e) {
                    throw e.setUnfinishedMessage(this);
                } catch (IOException e2) {
                    throw new InvalidProtocolBufferException(e2.getMessage()).setUnfinishedMessage(this);
                }
            } else {
                if (z3 & true) {
                    this.typeParameter_ = Collections.unmodifiableList(this.typeParameter_);
                }
                if (z3 & true) {
                    this.versionRequirement_ = Collections.unmodifiableList(this.versionRequirement_);
                }
                try {
                    newInstance.flush();
                } catch (IOException unused2) {
                    this.unknownFields = newOutput.toByteString();
                    d();
                    return;
                } catch (Throwable th3) {
                    this.unknownFields = newOutput.toByteString();
                    throw th3;
                }
            }
        }
    }
}
