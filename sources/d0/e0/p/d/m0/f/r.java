package d0.e0.p.d.m0.f;

import d0.e0.p.d.m0.f.q;
import d0.e0.p.d.m0.i.c;
import d0.e0.p.d.m0.i.d;
import d0.e0.p.d.m0.i.e;
import d0.e0.p.d.m0.i.g;
import d0.e0.p.d.m0.i.o;
import d0.e0.p.d.m0.i.p;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import kotlin.reflect.jvm.internal.impl.protobuf.CodedOutputStream;
import kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException;
import kotlin.reflect.jvm.internal.impl.protobuf.UninitializedMessageException;
/* compiled from: ProtoBuf.java */
/* loaded from: classes3.dex */
public final class r extends g.d<r> implements o {
    public static final r j;
    public static p<r> k = new a();
    private List<d0.e0.p.d.m0.f.b> annotation_;
    private int bitField0_;
    private int expandedTypeId_;
    private q expandedType_;
    private int flags_;
    private byte memoizedIsInitialized;
    private int memoizedSerializedSize;
    private int name_;
    private List<s> typeParameter_;
    private int underlyingTypeId_;
    private q underlyingType_;
    private final c unknownFields;
    private List<Integer> versionRequirement_;

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static class a extends d0.e0.p.d.m0.i.b<r> {
        @Override // d0.e0.p.d.m0.i.p
        public r parsePartialFrom(d dVar, e eVar) throws InvalidProtocolBufferException {
            return new r(dVar, eVar, null);
        }
    }

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static final class b extends g.c<r, b> implements o {
        public int m;
        public int o;
        public int r;
        public int t;
        public int n = 6;
        public List<s> p = Collections.emptyList();
        public q q = q.getDefaultInstance();

        /* renamed from: s  reason: collision with root package name */
        public q f3379s = q.getDefaultInstance();
        public List<d0.e0.p.d.m0.f.b> u = Collections.emptyList();
        public List<Integer> v = Collections.emptyList();

        public r buildPartial() {
            r rVar = new r(this, null);
            int i = this.m;
            int i2 = 1;
            if ((i & 1) != 1) {
                i2 = 0;
            }
            rVar.flags_ = this.n;
            if ((i & 2) == 2) {
                i2 |= 2;
            }
            rVar.name_ = this.o;
            if ((this.m & 4) == 4) {
                this.p = Collections.unmodifiableList(this.p);
                this.m &= -5;
            }
            rVar.typeParameter_ = this.p;
            if ((i & 8) == 8) {
                i2 |= 4;
            }
            rVar.underlyingType_ = this.q;
            if ((i & 16) == 16) {
                i2 |= 8;
            }
            rVar.underlyingTypeId_ = this.r;
            if ((i & 32) == 32) {
                i2 |= 16;
            }
            rVar.expandedType_ = this.f3379s;
            if ((i & 64) == 64) {
                i2 |= 32;
            }
            rVar.expandedTypeId_ = this.t;
            if ((this.m & 128) == 128) {
                this.u = Collections.unmodifiableList(this.u);
                this.m &= -129;
            }
            rVar.annotation_ = this.u;
            if ((this.m & 256) == 256) {
                this.v = Collections.unmodifiableList(this.v);
                this.m &= -257;
            }
            rVar.versionRequirement_ = this.v;
            rVar.bitField0_ = i2;
            return rVar;
        }

        public b mergeExpandedType(q qVar) {
            if ((this.m & 32) != 32 || this.f3379s == q.getDefaultInstance()) {
                this.f3379s = qVar;
            } else {
                this.f3379s = q.newBuilder(this.f3379s).mergeFrom(qVar).buildPartial();
            }
            this.m |= 32;
            return this;
        }

        public b mergeUnderlyingType(q qVar) {
            if ((this.m & 8) != 8 || this.q == q.getDefaultInstance()) {
                this.q = qVar;
            } else {
                this.q = q.newBuilder(this.q).mergeFrom(qVar).buildPartial();
            }
            this.m |= 8;
            return this;
        }

        public b setExpandedTypeId(int i) {
            this.m |= 64;
            this.t = i;
            return this;
        }

        public b setFlags(int i) {
            this.m |= 1;
            this.n = i;
            return this;
        }

        public b setName(int i) {
            this.m |= 2;
            this.o = i;
            return this;
        }

        public b setUnderlyingTypeId(int i) {
            this.m |= 16;
            this.r = i;
            return this;
        }

        @Override // d0.e0.p.d.m0.i.n.a
        public r build() {
            r buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw new UninitializedMessageException(buildPartial);
        }

        @Override // d0.e0.p.d.m0.i.g.b
        public b clone() {
            return new b().mergeFrom(buildPartial());
        }

        public b mergeFrom(r rVar) {
            if (rVar == r.getDefaultInstance()) {
                return this;
            }
            if (rVar.hasFlags()) {
                setFlags(rVar.getFlags());
            }
            if (rVar.hasName()) {
                setName(rVar.getName());
            }
            if (!rVar.typeParameter_.isEmpty()) {
                if (this.p.isEmpty()) {
                    this.p = rVar.typeParameter_;
                    this.m &= -5;
                } else {
                    if ((this.m & 4) != 4) {
                        this.p = new ArrayList(this.p);
                        this.m |= 4;
                    }
                    this.p.addAll(rVar.typeParameter_);
                }
            }
            if (rVar.hasUnderlyingType()) {
                mergeUnderlyingType(rVar.getUnderlyingType());
            }
            if (rVar.hasUnderlyingTypeId()) {
                setUnderlyingTypeId(rVar.getUnderlyingTypeId());
            }
            if (rVar.hasExpandedType()) {
                mergeExpandedType(rVar.getExpandedType());
            }
            if (rVar.hasExpandedTypeId()) {
                setExpandedTypeId(rVar.getExpandedTypeId());
            }
            if (!rVar.annotation_.isEmpty()) {
                if (this.u.isEmpty()) {
                    this.u = rVar.annotation_;
                    this.m &= -129;
                } else {
                    if ((this.m & 128) != 128) {
                        this.u = new ArrayList(this.u);
                        this.m |= 128;
                    }
                    this.u.addAll(rVar.annotation_);
                }
            }
            if (!rVar.versionRequirement_.isEmpty()) {
                if (this.v.isEmpty()) {
                    this.v = rVar.versionRequirement_;
                    this.m &= -257;
                } else {
                    if ((this.m & 256) != 256) {
                        this.v = new ArrayList(this.v);
                        this.m |= 256;
                    }
                    this.v.addAll(rVar.versionRequirement_);
                }
            }
            b(rVar);
            setUnknownFields(getUnknownFields().concat(rVar.unknownFields));
            return this;
        }

        /* JADX WARN: Removed duplicated region for block: B:14:0x001d  */
        @Override // d0.e0.p.d.m0.i.a.AbstractC0335a, d0.e0.p.d.m0.i.n.a
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public d0.e0.p.d.m0.f.r.b mergeFrom(d0.e0.p.d.m0.i.d r3, d0.e0.p.d.m0.i.e r4) throws java.io.IOException {
            /*
                r2 = this;
                r0 = 0
                d0.e0.p.d.m0.i.p<d0.e0.p.d.m0.f.r> r1 = d0.e0.p.d.m0.f.r.k     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                d0.e0.p.d.m0.f.r r3 = (d0.e0.p.d.m0.f.r) r3     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                if (r3 == 0) goto Le
                r2.mergeFrom(r3)
            Le:
                return r2
            Lf:
                r3 = move-exception
                goto L1b
            L11:
                r3 = move-exception
                d0.e0.p.d.m0.i.n r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                d0.e0.p.d.m0.f.r r4 = (d0.e0.p.d.m0.f.r) r4     // Catch: java.lang.Throwable -> Lf
                throw r3     // Catch: java.lang.Throwable -> L19
            L19:
                r3 = move-exception
                r0 = r4
            L1b:
                if (r0 == 0) goto L20
                r2.mergeFrom(r0)
            L20:
                throw r3
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.f.r.b.mergeFrom(d0.e0.p.d.m0.i.d, d0.e0.p.d.m0.i.e):d0.e0.p.d.m0.f.r$b");
        }
    }

    static {
        r rVar = new r();
        j = rVar;
        rVar.v();
    }

    public r(g.c cVar, d0.e0.p.d.m0.f.a aVar) {
        super(cVar);
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = cVar.getUnknownFields();
    }

    public static r getDefaultInstance() {
        return j;
    }

    public static b newBuilder(r rVar) {
        return newBuilder().mergeFrom(rVar);
    }

    public static r parseDelimitedFrom(InputStream inputStream, e eVar) throws IOException {
        return (r) ((d0.e0.p.d.m0.i.b) k).parseDelimitedFrom(inputStream, eVar);
    }

    public d0.e0.p.d.m0.f.b getAnnotation(int i) {
        return this.annotation_.get(i);
    }

    public int getAnnotationCount() {
        return this.annotation_.size();
    }

    public List<d0.e0.p.d.m0.f.b> getAnnotationList() {
        return this.annotation_;
    }

    public q getExpandedType() {
        return this.expandedType_;
    }

    public int getExpandedTypeId() {
        return this.expandedTypeId_;
    }

    public int getFlags() {
        return this.flags_;
    }

    public int getName() {
        return this.name_;
    }

    @Override // d0.e0.p.d.m0.i.n
    public int getSerializedSize() {
        int i = this.memoizedSerializedSize;
        if (i != -1) {
            return i;
        }
        int computeInt32Size = (this.bitField0_ & 1) == 1 ? CodedOutputStream.computeInt32Size(1, this.flags_) + 0 : 0;
        if ((this.bitField0_ & 2) == 2) {
            computeInt32Size += CodedOutputStream.computeInt32Size(2, this.name_);
        }
        for (int i2 = 0; i2 < this.typeParameter_.size(); i2++) {
            computeInt32Size += CodedOutputStream.computeMessageSize(3, this.typeParameter_.get(i2));
        }
        if ((this.bitField0_ & 4) == 4) {
            computeInt32Size += CodedOutputStream.computeMessageSize(4, this.underlyingType_);
        }
        if ((this.bitField0_ & 8) == 8) {
            computeInt32Size += CodedOutputStream.computeInt32Size(5, this.underlyingTypeId_);
        }
        if ((this.bitField0_ & 16) == 16) {
            computeInt32Size += CodedOutputStream.computeMessageSize(6, this.expandedType_);
        }
        if ((this.bitField0_ & 32) == 32) {
            computeInt32Size += CodedOutputStream.computeInt32Size(7, this.expandedTypeId_);
        }
        for (int i3 = 0; i3 < this.annotation_.size(); i3++) {
            computeInt32Size += CodedOutputStream.computeMessageSize(8, this.annotation_.get(i3));
        }
        int i4 = 0;
        for (int i5 = 0; i5 < this.versionRequirement_.size(); i5++) {
            i4 += CodedOutputStream.computeInt32SizeNoTag(this.versionRequirement_.get(i5).intValue());
        }
        int size = this.unknownFields.size() + c() + (getVersionRequirementList().size() * 2) + computeInt32Size + i4;
        this.memoizedSerializedSize = size;
        return size;
    }

    public s getTypeParameter(int i) {
        return this.typeParameter_.get(i);
    }

    public int getTypeParameterCount() {
        return this.typeParameter_.size();
    }

    public List<s> getTypeParameterList() {
        return this.typeParameter_;
    }

    public q getUnderlyingType() {
        return this.underlyingType_;
    }

    public int getUnderlyingTypeId() {
        return this.underlyingTypeId_;
    }

    public List<Integer> getVersionRequirementList() {
        return this.versionRequirement_;
    }

    public boolean hasExpandedType() {
        return (this.bitField0_ & 16) == 16;
    }

    public boolean hasExpandedTypeId() {
        return (this.bitField0_ & 32) == 32;
    }

    public boolean hasFlags() {
        return (this.bitField0_ & 1) == 1;
    }

    public boolean hasName() {
        return (this.bitField0_ & 2) == 2;
    }

    public boolean hasUnderlyingType() {
        return (this.bitField0_ & 4) == 4;
    }

    public boolean hasUnderlyingTypeId() {
        return (this.bitField0_ & 8) == 8;
    }

    @Override // d0.e0.p.d.m0.i.o
    public final boolean isInitialized() {
        byte b2 = this.memoizedIsInitialized;
        if (b2 == 1) {
            return true;
        }
        if (b2 == 0) {
            return false;
        }
        if (!hasName()) {
            this.memoizedIsInitialized = (byte) 0;
            return false;
        }
        for (int i = 0; i < getTypeParameterCount(); i++) {
            if (!getTypeParameter(i).isInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
        }
        if (hasUnderlyingType() && !getUnderlyingType().isInitialized()) {
            this.memoizedIsInitialized = (byte) 0;
            return false;
        } else if (!hasExpandedType() || getExpandedType().isInitialized()) {
            for (int i2 = 0; i2 < getAnnotationCount(); i2++) {
                if (!getAnnotation(i2).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            if (!b()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        } else {
            this.memoizedIsInitialized = (byte) 0;
            return false;
        }
    }

    public final void v() {
        this.flags_ = 6;
        this.name_ = 0;
        this.typeParameter_ = Collections.emptyList();
        this.underlyingType_ = q.getDefaultInstance();
        this.underlyingTypeId_ = 0;
        this.expandedType_ = q.getDefaultInstance();
        this.expandedTypeId_ = 0;
        this.annotation_ = Collections.emptyList();
        this.versionRequirement_ = Collections.emptyList();
    }

    @Override // d0.e0.p.d.m0.i.n
    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        getSerializedSize();
        g.d<MessageType>.a e = e();
        if ((this.bitField0_ & 1) == 1) {
            codedOutputStream.writeInt32(1, this.flags_);
        }
        if ((this.bitField0_ & 2) == 2) {
            codedOutputStream.writeInt32(2, this.name_);
        }
        for (int i = 0; i < this.typeParameter_.size(); i++) {
            codedOutputStream.writeMessage(3, this.typeParameter_.get(i));
        }
        if ((this.bitField0_ & 4) == 4) {
            codedOutputStream.writeMessage(4, this.underlyingType_);
        }
        if ((this.bitField0_ & 8) == 8) {
            codedOutputStream.writeInt32(5, this.underlyingTypeId_);
        }
        if ((this.bitField0_ & 16) == 16) {
            codedOutputStream.writeMessage(6, this.expandedType_);
        }
        if ((this.bitField0_ & 32) == 32) {
            codedOutputStream.writeInt32(7, this.expandedTypeId_);
        }
        for (int i2 = 0; i2 < this.annotation_.size(); i2++) {
            codedOutputStream.writeMessage(8, this.annotation_.get(i2));
        }
        for (int i3 = 0; i3 < this.versionRequirement_.size(); i3++) {
            codedOutputStream.writeInt32(31, this.versionRequirement_.get(i3).intValue());
        }
        e.writeUntil(200, codedOutputStream);
        codedOutputStream.writeRawBytes(this.unknownFields);
    }

    public static b newBuilder() {
        return new b();
    }

    @Override // d0.e0.p.d.m0.i.o
    public r getDefaultInstanceForType() {
        return j;
    }

    @Override // d0.e0.p.d.m0.i.n
    public b newBuilderForType() {
        return newBuilder();
    }

    @Override // d0.e0.p.d.m0.i.n
    public b toBuilder() {
        return newBuilder(this);
    }

    public r() {
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = c.j;
    }

    /* JADX WARN: Finally extract failed */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v0 */
    /* JADX WARN: Type inference failed for: r4v1 */
    /* JADX WARN: Type inference failed for: r4v2, types: [boolean] */
    public r(d dVar, e eVar, d0.e0.p.d.m0.f.a aVar) throws InvalidProtocolBufferException {
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        v();
        c.b newOutput = c.newOutput();
        CodedOutputStream newInstance = CodedOutputStream.newInstance(newOutput, 1);
        boolean z2 = false;
        boolean z3 = false;
        while (true) {
            ?? r4 = 4;
            if (!z2) {
                try {
                    try {
                        int readTag = dVar.readTag();
                        q.c cVar = null;
                        switch (readTag) {
                            case 0:
                                break;
                            case 8:
                                this.bitField0_ |= 1;
                                this.flags_ = dVar.readInt32();
                                continue;
                            case 16:
                                this.bitField0_ |= 2;
                                this.name_ = dVar.readInt32();
                                continue;
                            case 26:
                                if (!(z3 & true)) {
                                    this.typeParameter_ = new ArrayList();
                                    z3 |= true;
                                }
                                this.typeParameter_.add(dVar.readMessage(s.k, eVar));
                                continue;
                            case 34:
                                cVar = (this.bitField0_ & 4) == 4 ? this.underlyingType_.toBuilder() : cVar;
                                q qVar = (q) dVar.readMessage(q.k, eVar);
                                this.underlyingType_ = qVar;
                                if (cVar != null) {
                                    cVar.mergeFrom(qVar);
                                    this.underlyingType_ = cVar.buildPartial();
                                }
                                this.bitField0_ |= 4;
                                continue;
                            case 40:
                                this.bitField0_ |= 8;
                                this.underlyingTypeId_ = dVar.readInt32();
                                continue;
                            case 50:
                                cVar = (this.bitField0_ & 16) == 16 ? this.expandedType_.toBuilder() : cVar;
                                q qVar2 = (q) dVar.readMessage(q.k, eVar);
                                this.expandedType_ = qVar2;
                                if (cVar != null) {
                                    cVar.mergeFrom(qVar2);
                                    this.expandedType_ = cVar.buildPartial();
                                }
                                this.bitField0_ |= 16;
                                continue;
                            case 56:
                                this.bitField0_ |= 32;
                                this.expandedTypeId_ = dVar.readInt32();
                                continue;
                            case 66:
                                if (!(z3 & true)) {
                                    this.annotation_ = new ArrayList();
                                    z3 |= true;
                                }
                                this.annotation_.add(dVar.readMessage(d0.e0.p.d.m0.f.b.k, eVar));
                                continue;
                            case 248:
                                if (!(z3 & true)) {
                                    this.versionRequirement_ = new ArrayList();
                                    z3 |= true;
                                }
                                this.versionRequirement_.add(Integer.valueOf(dVar.readInt32()));
                                continue;
                            case 250:
                                int pushLimit = dVar.pushLimit(dVar.readRawVarint32());
                                if (!(z3 & true) && dVar.getBytesUntilLimit() > 0) {
                                    this.versionRequirement_ = new ArrayList();
                                    z3 |= true;
                                }
                                while (dVar.getBytesUntilLimit() > 0) {
                                    this.versionRequirement_.add(Integer.valueOf(dVar.readInt32()));
                                }
                                dVar.popLimit(pushLimit);
                                continue;
                                break;
                            default:
                                r4 = f(dVar, newInstance, eVar, readTag);
                                if (r4 == 0) {
                                    break;
                                } else {
                                    continue;
                                }
                        }
                        z2 = true;
                    } catch (Throwable th) {
                        if ((z3 & true) == r4) {
                            this.typeParameter_ = Collections.unmodifiableList(this.typeParameter_);
                        }
                        if (z3 & true) {
                            this.annotation_ = Collections.unmodifiableList(this.annotation_);
                        }
                        if (z3 & true) {
                            this.versionRequirement_ = Collections.unmodifiableList(this.versionRequirement_);
                        }
                        try {
                            newInstance.flush();
                        } catch (IOException unused) {
                            this.unknownFields = newOutput.toByteString();
                            d();
                            throw th;
                        } catch (Throwable th2) {
                            this.unknownFields = newOutput.toByteString();
                            throw th2;
                        }
                    }
                } catch (InvalidProtocolBufferException e) {
                    throw e.setUnfinishedMessage(this);
                } catch (IOException e2) {
                    throw new InvalidProtocolBufferException(e2.getMessage()).setUnfinishedMessage(this);
                }
            } else {
                if (z3 & true) {
                    this.typeParameter_ = Collections.unmodifiableList(this.typeParameter_);
                }
                if (z3 & true) {
                    this.annotation_ = Collections.unmodifiableList(this.annotation_);
                }
                if (z3 & true) {
                    this.versionRequirement_ = Collections.unmodifiableList(this.versionRequirement_);
                }
                try {
                    newInstance.flush();
                } catch (IOException unused2) {
                    this.unknownFields = newOutput.toByteString();
                    d();
                    return;
                } catch (Throwable th3) {
                    this.unknownFields = newOutput.toByteString();
                    throw th3;
                }
            }
        }
    }
}
