package d0.e0.p.d.m0.f;

import d0.e0.p.d.m0.i.c;
import d0.e0.p.d.m0.i.e;
import d0.e0.p.d.m0.i.g;
import d0.e0.p.d.m0.i.o;
import d0.e0.p.d.m0.i.p;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import kotlin.reflect.jvm.internal.impl.protobuf.CodedOutputStream;
import kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException;
import kotlin.reflect.jvm.internal.impl.protobuf.UninitializedMessageException;
/* compiled from: ProtoBuf.java */
/* loaded from: classes3.dex */
public final class d extends g.d<d> implements o {
    public static final d j;
    public static p<d> k = new a();
    private int bitField0_;
    private int flags_;
    private byte memoizedIsInitialized;
    private int memoizedSerializedSize;
    private final c unknownFields;
    private List<u> valueParameter_;
    private List<Integer> versionRequirement_;

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static class a extends d0.e0.p.d.m0.i.b<d> {
        @Override // d0.e0.p.d.m0.i.p
        public d parsePartialFrom(d0.e0.p.d.m0.i.d dVar, e eVar) throws InvalidProtocolBufferException {
            return new d(dVar, eVar, null);
        }
    }

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static final class b extends g.c<d, b> implements o {
        public int m;
        public int n = 6;
        public List<u> o = Collections.emptyList();
        public List<Integer> p = Collections.emptyList();

        public d buildPartial() {
            d dVar = new d(this, null);
            int i = 1;
            if ((this.m & 1) != 1) {
                i = 0;
            }
            dVar.flags_ = this.n;
            if ((this.m & 2) == 2) {
                this.o = Collections.unmodifiableList(this.o);
                this.m &= -3;
            }
            dVar.valueParameter_ = this.o;
            if ((this.m & 4) == 4) {
                this.p = Collections.unmodifiableList(this.p);
                this.m &= -5;
            }
            dVar.versionRequirement_ = this.p;
            dVar.bitField0_ = i;
            return dVar;
        }

        public b setFlags(int i) {
            this.m |= 1;
            this.n = i;
            return this;
        }

        @Override // d0.e0.p.d.m0.i.n.a
        public d build() {
            d buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw new UninitializedMessageException(buildPartial);
        }

        @Override // d0.e0.p.d.m0.i.g.b
        public b clone() {
            return new b().mergeFrom(buildPartial());
        }

        public b mergeFrom(d dVar) {
            if (dVar == d.getDefaultInstance()) {
                return this;
            }
            if (dVar.hasFlags()) {
                setFlags(dVar.getFlags());
            }
            if (!dVar.valueParameter_.isEmpty()) {
                if (this.o.isEmpty()) {
                    this.o = dVar.valueParameter_;
                    this.m &= -3;
                } else {
                    if ((this.m & 2) != 2) {
                        this.o = new ArrayList(this.o);
                        this.m |= 2;
                    }
                    this.o.addAll(dVar.valueParameter_);
                }
            }
            if (!dVar.versionRequirement_.isEmpty()) {
                if (this.p.isEmpty()) {
                    this.p = dVar.versionRequirement_;
                    this.m &= -5;
                } else {
                    if ((this.m & 4) != 4) {
                        this.p = new ArrayList(this.p);
                        this.m |= 4;
                    }
                    this.p.addAll(dVar.versionRequirement_);
                }
            }
            b(dVar);
            setUnknownFields(getUnknownFields().concat(dVar.unknownFields));
            return this;
        }

        /* JADX WARN: Removed duplicated region for block: B:14:0x001d  */
        @Override // d0.e0.p.d.m0.i.a.AbstractC0335a, d0.e0.p.d.m0.i.n.a
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public d0.e0.p.d.m0.f.d.b mergeFrom(d0.e0.p.d.m0.i.d r3, d0.e0.p.d.m0.i.e r4) throws java.io.IOException {
            /*
                r2 = this;
                r0 = 0
                d0.e0.p.d.m0.i.p<d0.e0.p.d.m0.f.d> r1 = d0.e0.p.d.m0.f.d.k     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                d0.e0.p.d.m0.f.d r3 = (d0.e0.p.d.m0.f.d) r3     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                if (r3 == 0) goto Le
                r2.mergeFrom(r3)
            Le:
                return r2
            Lf:
                r3 = move-exception
                goto L1b
            L11:
                r3 = move-exception
                d0.e0.p.d.m0.i.n r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                d0.e0.p.d.m0.f.d r4 = (d0.e0.p.d.m0.f.d) r4     // Catch: java.lang.Throwable -> Lf
                throw r3     // Catch: java.lang.Throwable -> L19
            L19:
                r3 = move-exception
                r0 = r4
            L1b:
                if (r0 == 0) goto L20
                r2.mergeFrom(r0)
            L20:
                throw r3
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.f.d.b.mergeFrom(d0.e0.p.d.m0.i.d, d0.e0.p.d.m0.i.e):d0.e0.p.d.m0.f.d$b");
        }
    }

    static {
        d dVar = new d();
        j = dVar;
        dVar.flags_ = 6;
        dVar.valueParameter_ = Collections.emptyList();
        dVar.versionRequirement_ = Collections.emptyList();
    }

    public d(g.c cVar, d0.e0.p.d.m0.f.a aVar) {
        super(cVar);
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = cVar.getUnknownFields();
    }

    public static d getDefaultInstance() {
        return j;
    }

    public static b newBuilder(d dVar) {
        return newBuilder().mergeFrom(dVar);
    }

    public int getFlags() {
        return this.flags_;
    }

    @Override // d0.e0.p.d.m0.i.n
    public int getSerializedSize() {
        int i = this.memoizedSerializedSize;
        if (i != -1) {
            return i;
        }
        int computeInt32Size = (this.bitField0_ & 1) == 1 ? CodedOutputStream.computeInt32Size(1, this.flags_) + 0 : 0;
        for (int i2 = 0; i2 < this.valueParameter_.size(); i2++) {
            computeInt32Size += CodedOutputStream.computeMessageSize(2, this.valueParameter_.get(i2));
        }
        int i3 = 0;
        for (int i4 = 0; i4 < this.versionRequirement_.size(); i4++) {
            i3 += CodedOutputStream.computeInt32SizeNoTag(this.versionRequirement_.get(i4).intValue());
        }
        int size = this.unknownFields.size() + c() + (getVersionRequirementList().size() * 2) + computeInt32Size + i3;
        this.memoizedSerializedSize = size;
        return size;
    }

    public u getValueParameter(int i) {
        return this.valueParameter_.get(i);
    }

    public int getValueParameterCount() {
        return this.valueParameter_.size();
    }

    public List<u> getValueParameterList() {
        return this.valueParameter_;
    }

    public List<Integer> getVersionRequirementList() {
        return this.versionRequirement_;
    }

    public boolean hasFlags() {
        return (this.bitField0_ & 1) == 1;
    }

    @Override // d0.e0.p.d.m0.i.o
    public final boolean isInitialized() {
        byte b2 = this.memoizedIsInitialized;
        if (b2 == 1) {
            return true;
        }
        if (b2 == 0) {
            return false;
        }
        for (int i = 0; i < getValueParameterCount(); i++) {
            if (!getValueParameter(i).isInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
        }
        if (!b()) {
            this.memoizedIsInitialized = (byte) 0;
            return false;
        }
        this.memoizedIsInitialized = (byte) 1;
        return true;
    }

    @Override // d0.e0.p.d.m0.i.n
    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        getSerializedSize();
        g.d<MessageType>.a e = e();
        if ((this.bitField0_ & 1) == 1) {
            codedOutputStream.writeInt32(1, this.flags_);
        }
        for (int i = 0; i < this.valueParameter_.size(); i++) {
            codedOutputStream.writeMessage(2, this.valueParameter_.get(i));
        }
        for (int i2 = 0; i2 < this.versionRequirement_.size(); i2++) {
            codedOutputStream.writeInt32(31, this.versionRequirement_.get(i2).intValue());
        }
        e.writeUntil(19000, codedOutputStream);
        codedOutputStream.writeRawBytes(this.unknownFields);
    }

    public static b newBuilder() {
        return new b();
    }

    @Override // d0.e0.p.d.m0.i.o
    public d getDefaultInstanceForType() {
        return j;
    }

    @Override // d0.e0.p.d.m0.i.n
    public b newBuilderForType() {
        return newBuilder();
    }

    @Override // d0.e0.p.d.m0.i.n
    public b toBuilder() {
        return newBuilder(this);
    }

    public d() {
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = c.j;
    }

    /* JADX WARN: Finally extract failed */
    /* JADX WARN: Multi-variable type inference failed */
    public d(d0.e0.p.d.m0.i.d dVar, e eVar, d0.e0.p.d.m0.f.a aVar) throws InvalidProtocolBufferException {
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.flags_ = 6;
        this.valueParameter_ = Collections.emptyList();
        this.versionRequirement_ = Collections.emptyList();
        c.b newOutput = c.newOutput();
        CodedOutputStream newInstance = CodedOutputStream.newInstance(newOutput, 1);
        boolean z2 = false;
        boolean z3 = false;
        while (!z2) {
            try {
                try {
                    int readTag = dVar.readTag();
                    if (readTag != 0) {
                        if (readTag == 8) {
                            this.bitField0_ |= 1;
                            this.flags_ = dVar.readInt32();
                        } else if (readTag == 18) {
                            if (!(z3 & true)) {
                                this.valueParameter_ = new ArrayList();
                                z3 |= true;
                            }
                            this.valueParameter_.add(dVar.readMessage(u.k, eVar));
                        } else if (readTag == 248) {
                            if (!(z3 & true)) {
                                this.versionRequirement_ = new ArrayList();
                                z3 |= true;
                            }
                            this.versionRequirement_.add(Integer.valueOf(dVar.readInt32()));
                        } else if (readTag == 250) {
                            int pushLimit = dVar.pushLimit(dVar.readRawVarint32());
                            if (!(z3 & true) && dVar.getBytesUntilLimit() > 0) {
                                this.versionRequirement_ = new ArrayList();
                                z3 |= true;
                            }
                            while (dVar.getBytesUntilLimit() > 0) {
                                this.versionRequirement_.add(Integer.valueOf(dVar.readInt32()));
                            }
                            dVar.popLimit(pushLimit);
                        } else if (!f(dVar, newInstance, eVar, readTag)) {
                        }
                    }
                    z2 = true;
                } catch (Throwable th) {
                    if (z3 & true) {
                        this.valueParameter_ = Collections.unmodifiableList(this.valueParameter_);
                    }
                    if (z3 & true) {
                        this.versionRequirement_ = Collections.unmodifiableList(this.versionRequirement_);
                    }
                    try {
                        newInstance.flush();
                    } catch (IOException unused) {
                        this.unknownFields = newOutput.toByteString();
                        d();
                        throw th;
                    } catch (Throwable th2) {
                        this.unknownFields = newOutput.toByteString();
                        throw th2;
                    }
                }
            } catch (InvalidProtocolBufferException e) {
                throw e.setUnfinishedMessage(this);
            } catch (IOException e2) {
                throw new InvalidProtocolBufferException(e2.getMessage()).setUnfinishedMessage(this);
            }
        }
        if (z3 & true) {
            this.valueParameter_ = Collections.unmodifiableList(this.valueParameter_);
        }
        if (z3 & true) {
            this.versionRequirement_ = Collections.unmodifiableList(this.versionRequirement_);
        }
        try {
            newInstance.flush();
        } catch (IOException unused2) {
            this.unknownFields = newOutput.toByteString();
            d();
        } catch (Throwable th3) {
            this.unknownFields = newOutput.toByteString();
            throw th3;
        }
    }
}
