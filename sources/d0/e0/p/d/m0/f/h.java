package d0.e0.p.d.m0.f;

import d0.e0.p.d.m0.f.q;
import d0.e0.p.d.m0.i.d;
import d0.e0.p.d.m0.i.e;
import d0.e0.p.d.m0.i.g;
import d0.e0.p.d.m0.i.h;
import d0.e0.p.d.m0.i.o;
import d0.e0.p.d.m0.i.p;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import kotlin.reflect.jvm.internal.impl.protobuf.CodedOutputStream;
import kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException;
import kotlin.reflect.jvm.internal.impl.protobuf.UninitializedMessageException;
/* compiled from: ProtoBuf.java */
/* loaded from: classes3.dex */
public final class h extends g implements o {
    public static final h j;
    public static p<h> k = new a();
    private List<h> andArgument_;
    private int bitField0_;
    private c constantValue_;
    private int flags_;
    private int isInstanceTypeId_;
    private q isInstanceType_;
    private byte memoizedIsInitialized;
    private int memoizedSerializedSize;
    private List<h> orArgument_;
    private final d0.e0.p.d.m0.i.c unknownFields;
    private int valueParameterReference_;

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static class a extends d0.e0.p.d.m0.i.b<h> {
        @Override // d0.e0.p.d.m0.i.p
        public h parsePartialFrom(d dVar, e eVar) throws InvalidProtocolBufferException {
            return new h(dVar, eVar, null);
        }
    }

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static final class b extends g.b<h, b> implements o {
        public int k;
        public int l;
        public int m;
        public int p;
        public c n = c.TRUE;
        public q o = q.getDefaultInstance();
        public List<h> q = Collections.emptyList();
        public List<h> r = Collections.emptyList();

        public h buildPartial() {
            h hVar = new h(this, null);
            int i = this.k;
            int i2 = 1;
            if ((i & 1) != 1) {
                i2 = 0;
            }
            hVar.flags_ = this.l;
            if ((i & 2) == 2) {
                i2 |= 2;
            }
            hVar.valueParameterReference_ = this.m;
            if ((i & 4) == 4) {
                i2 |= 4;
            }
            hVar.constantValue_ = this.n;
            if ((i & 8) == 8) {
                i2 |= 8;
            }
            hVar.isInstanceType_ = this.o;
            if ((i & 16) == 16) {
                i2 |= 16;
            }
            hVar.isInstanceTypeId_ = this.p;
            if ((this.k & 32) == 32) {
                this.q = Collections.unmodifiableList(this.q);
                this.k &= -33;
            }
            hVar.andArgument_ = this.q;
            if ((this.k & 64) == 64) {
                this.r = Collections.unmodifiableList(this.r);
                this.k &= -65;
            }
            hVar.orArgument_ = this.r;
            hVar.bitField0_ = i2;
            return hVar;
        }

        public b mergeIsInstanceType(q qVar) {
            if ((this.k & 8) != 8 || this.o == q.getDefaultInstance()) {
                this.o = qVar;
            } else {
                this.o = q.newBuilder(this.o).mergeFrom(qVar).buildPartial();
            }
            this.k |= 8;
            return this;
        }

        public b setConstantValue(c cVar) {
            Objects.requireNonNull(cVar);
            this.k |= 4;
            this.n = cVar;
            return this;
        }

        public b setFlags(int i) {
            this.k |= 1;
            this.l = i;
            return this;
        }

        public b setIsInstanceTypeId(int i) {
            this.k |= 16;
            this.p = i;
            return this;
        }

        public b setValueParameterReference(int i) {
            this.k |= 2;
            this.m = i;
            return this;
        }

        @Override // d0.e0.p.d.m0.i.n.a
        public h build() {
            h buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw new UninitializedMessageException(buildPartial);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // d0.e0.p.d.m0.i.g.b
        public b clone() {
            return new b().mergeFrom(buildPartial());
        }

        public b mergeFrom(h hVar) {
            if (hVar == h.getDefaultInstance()) {
                return this;
            }
            if (hVar.hasFlags()) {
                setFlags(hVar.getFlags());
            }
            if (hVar.hasValueParameterReference()) {
                setValueParameterReference(hVar.getValueParameterReference());
            }
            if (hVar.hasConstantValue()) {
                setConstantValue(hVar.getConstantValue());
            }
            if (hVar.hasIsInstanceType()) {
                mergeIsInstanceType(hVar.getIsInstanceType());
            }
            if (hVar.hasIsInstanceTypeId()) {
                setIsInstanceTypeId(hVar.getIsInstanceTypeId());
            }
            if (!hVar.andArgument_.isEmpty()) {
                if (this.q.isEmpty()) {
                    this.q = hVar.andArgument_;
                    this.k &= -33;
                } else {
                    if ((this.k & 32) != 32) {
                        this.q = new ArrayList(this.q);
                        this.k |= 32;
                    }
                    this.q.addAll(hVar.andArgument_);
                }
            }
            if (!hVar.orArgument_.isEmpty()) {
                if (this.r.isEmpty()) {
                    this.r = hVar.orArgument_;
                    this.k &= -65;
                } else {
                    if ((this.k & 64) != 64) {
                        this.r = new ArrayList(this.r);
                        this.k |= 64;
                    }
                    this.r.addAll(hVar.orArgument_);
                }
            }
            setUnknownFields(getUnknownFields().concat(hVar.unknownFields));
            return this;
        }

        /* JADX WARN: Removed duplicated region for block: B:14:0x001d  */
        @Override // d0.e0.p.d.m0.i.a.AbstractC0335a, d0.e0.p.d.m0.i.n.a
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public d0.e0.p.d.m0.f.h.b mergeFrom(d0.e0.p.d.m0.i.d r3, d0.e0.p.d.m0.i.e r4) throws java.io.IOException {
            /*
                r2 = this;
                r0 = 0
                d0.e0.p.d.m0.i.p<d0.e0.p.d.m0.f.h> r1 = d0.e0.p.d.m0.f.h.k     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                d0.e0.p.d.m0.f.h r3 = (d0.e0.p.d.m0.f.h) r3     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                if (r3 == 0) goto Le
                r2.mergeFrom(r3)
            Le:
                return r2
            Lf:
                r3 = move-exception
                goto L1b
            L11:
                r3 = move-exception
                d0.e0.p.d.m0.i.n r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                d0.e0.p.d.m0.f.h r4 = (d0.e0.p.d.m0.f.h) r4     // Catch: java.lang.Throwable -> Lf
                throw r3     // Catch: java.lang.Throwable -> L19
            L19:
                r3 = move-exception
                r0 = r4
            L1b:
                if (r0 == 0) goto L20
                r2.mergeFrom(r0)
            L20:
                throw r3
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.f.h.b.mergeFrom(d0.e0.p.d.m0.i.d, d0.e0.p.d.m0.i.e):d0.e0.p.d.m0.f.h$b");
        }
    }

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public enum c implements h.a {
        TRUE(0),
        FALSE(1),
        NULL(2);
        
        private final int value;

        c(int i) {
            this.value = i;
        }

        @Override // d0.e0.p.d.m0.i.h.a
        public final int getNumber() {
            return this.value;
        }

        public static c valueOf(int i) {
            if (i == 0) {
                return TRUE;
            }
            if (i == 1) {
                return FALSE;
            }
            if (i != 2) {
                return null;
            }
            return NULL;
        }
    }

    static {
        h hVar = new h();
        j = hVar;
        hVar.l();
    }

    public h(g.b bVar, d0.e0.p.d.m0.f.a aVar) {
        super(bVar);
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = bVar.getUnknownFields();
    }

    public static h getDefaultInstance() {
        return j;
    }

    public static b newBuilder(h hVar) {
        return newBuilder().mergeFrom(hVar);
    }

    public h getAndArgument(int i) {
        return this.andArgument_.get(i);
    }

    public int getAndArgumentCount() {
        return this.andArgument_.size();
    }

    public c getConstantValue() {
        return this.constantValue_;
    }

    public int getFlags() {
        return this.flags_;
    }

    public q getIsInstanceType() {
        return this.isInstanceType_;
    }

    public int getIsInstanceTypeId() {
        return this.isInstanceTypeId_;
    }

    public h getOrArgument(int i) {
        return this.orArgument_.get(i);
    }

    public int getOrArgumentCount() {
        return this.orArgument_.size();
    }

    @Override // d0.e0.p.d.m0.i.n
    public int getSerializedSize() {
        int i = this.memoizedSerializedSize;
        if (i != -1) {
            return i;
        }
        int computeInt32Size = (this.bitField0_ & 1) == 1 ? CodedOutputStream.computeInt32Size(1, this.flags_) + 0 : 0;
        if ((this.bitField0_ & 2) == 2) {
            computeInt32Size += CodedOutputStream.computeInt32Size(2, this.valueParameterReference_);
        }
        if ((this.bitField0_ & 4) == 4) {
            computeInt32Size += CodedOutputStream.computeEnumSize(3, this.constantValue_.getNumber());
        }
        if ((this.bitField0_ & 8) == 8) {
            computeInt32Size += CodedOutputStream.computeMessageSize(4, this.isInstanceType_);
        }
        if ((this.bitField0_ & 16) == 16) {
            computeInt32Size += CodedOutputStream.computeInt32Size(5, this.isInstanceTypeId_);
        }
        for (int i2 = 0; i2 < this.andArgument_.size(); i2++) {
            computeInt32Size += CodedOutputStream.computeMessageSize(6, this.andArgument_.get(i2));
        }
        for (int i3 = 0; i3 < this.orArgument_.size(); i3++) {
            computeInt32Size += CodedOutputStream.computeMessageSize(7, this.orArgument_.get(i3));
        }
        int size = this.unknownFields.size() + computeInt32Size;
        this.memoizedSerializedSize = size;
        return size;
    }

    public int getValueParameterReference() {
        return this.valueParameterReference_;
    }

    public boolean hasConstantValue() {
        return (this.bitField0_ & 4) == 4;
    }

    public boolean hasFlags() {
        return (this.bitField0_ & 1) == 1;
    }

    public boolean hasIsInstanceType() {
        return (this.bitField0_ & 8) == 8;
    }

    public boolean hasIsInstanceTypeId() {
        return (this.bitField0_ & 16) == 16;
    }

    public boolean hasValueParameterReference() {
        return (this.bitField0_ & 2) == 2;
    }

    @Override // d0.e0.p.d.m0.i.o
    public final boolean isInitialized() {
        byte b2 = this.memoizedIsInitialized;
        if (b2 == 1) {
            return true;
        }
        if (b2 == 0) {
            return false;
        }
        if (!hasIsInstanceType() || getIsInstanceType().isInitialized()) {
            for (int i = 0; i < getAndArgumentCount(); i++) {
                if (!getAndArgument(i).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            for (int i2 = 0; i2 < getOrArgumentCount(); i2++) {
                if (!getOrArgument(i2).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }
        this.memoizedIsInitialized = (byte) 0;
        return false;
    }

    public final void l() {
        this.flags_ = 0;
        this.valueParameterReference_ = 0;
        this.constantValue_ = c.TRUE;
        this.isInstanceType_ = q.getDefaultInstance();
        this.isInstanceTypeId_ = 0;
        this.andArgument_ = Collections.emptyList();
        this.orArgument_ = Collections.emptyList();
    }

    @Override // d0.e0.p.d.m0.i.n
    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        getSerializedSize();
        if ((this.bitField0_ & 1) == 1) {
            codedOutputStream.writeInt32(1, this.flags_);
        }
        if ((this.bitField0_ & 2) == 2) {
            codedOutputStream.writeInt32(2, this.valueParameterReference_);
        }
        if ((this.bitField0_ & 4) == 4) {
            codedOutputStream.writeEnum(3, this.constantValue_.getNumber());
        }
        if ((this.bitField0_ & 8) == 8) {
            codedOutputStream.writeMessage(4, this.isInstanceType_);
        }
        if ((this.bitField0_ & 16) == 16) {
            codedOutputStream.writeInt32(5, this.isInstanceTypeId_);
        }
        for (int i = 0; i < this.andArgument_.size(); i++) {
            codedOutputStream.writeMessage(6, this.andArgument_.get(i));
        }
        for (int i2 = 0; i2 < this.orArgument_.size(); i2++) {
            codedOutputStream.writeMessage(7, this.orArgument_.get(i2));
        }
        codedOutputStream.writeRawBytes(this.unknownFields);
    }

    public static b newBuilder() {
        return new b();
    }

    @Override // d0.e0.p.d.m0.i.n
    public b newBuilderForType() {
        return newBuilder();
    }

    @Override // d0.e0.p.d.m0.i.n
    public b toBuilder() {
        return newBuilder(this);
    }

    public h() {
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = d0.e0.p.d.m0.i.c.j;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public h(d dVar, e eVar, d0.e0.p.d.m0.f.a aVar) throws InvalidProtocolBufferException {
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        l();
        CodedOutputStream newInstance = CodedOutputStream.newInstance(d0.e0.p.d.m0.i.c.newOutput(), 1);
        boolean z2 = false;
        boolean z3 = false;
        while (!z2) {
            try {
                try {
                    int readTag = dVar.readTag();
                    if (readTag != 0) {
                        if (readTag == 8) {
                            this.bitField0_ |= 1;
                            this.flags_ = dVar.readInt32();
                        } else if (readTag == 16) {
                            this.bitField0_ |= 2;
                            this.valueParameterReference_ = dVar.readInt32();
                        } else if (readTag == 24) {
                            int readEnum = dVar.readEnum();
                            c valueOf = c.valueOf(readEnum);
                            if (valueOf == null) {
                                newInstance.writeRawVarint32(readTag);
                                newInstance.writeRawVarint32(readEnum);
                            } else {
                                this.bitField0_ |= 4;
                                this.constantValue_ = valueOf;
                            }
                        } else if (readTag == 34) {
                            q.c builder = (this.bitField0_ & 8) == 8 ? this.isInstanceType_.toBuilder() : null;
                            q qVar = (q) dVar.readMessage(q.k, eVar);
                            this.isInstanceType_ = qVar;
                            if (builder != null) {
                                builder.mergeFrom(qVar);
                                this.isInstanceType_ = builder.buildPartial();
                            }
                            this.bitField0_ |= 8;
                        } else if (readTag == 40) {
                            this.bitField0_ |= 16;
                            this.isInstanceTypeId_ = dVar.readInt32();
                        } else if (readTag == 50) {
                            if (!(z3 & true)) {
                                this.andArgument_ = new ArrayList();
                                z3 |= true;
                            }
                            this.andArgument_.add(dVar.readMessage(k, eVar));
                        } else if (readTag == 58) {
                            if (!(z3 & true)) {
                                this.orArgument_ = new ArrayList();
                                z3 |= true;
                            }
                            this.orArgument_.add(dVar.readMessage(k, eVar));
                        } else if (!dVar.skipField(readTag, newInstance)) {
                        }
                    }
                    z2 = true;
                } catch (InvalidProtocolBufferException e) {
                    throw e.setUnfinishedMessage(this);
                } catch (IOException e2) {
                    throw new InvalidProtocolBufferException(e2.getMessage()).setUnfinishedMessage(this);
                }
            } catch (Throwable th) {
                if (z3 & true) {
                    this.andArgument_ = Collections.unmodifiableList(this.andArgument_);
                }
                if (z3 & true) {
                    this.orArgument_ = Collections.unmodifiableList(this.orArgument_);
                }
                try {
                    newInstance.flush();
                } catch (IOException unused) {
                    throw th;
                } finally {
                }
            }
        }
        if (z3 & true) {
            this.andArgument_ = Collections.unmodifiableList(this.andArgument_);
        }
        if (z3 & true) {
            this.orArgument_ = Collections.unmodifiableList(this.orArgument_);
        }
        try {
            newInstance.flush();
        } catch (IOException unused2) {
        } finally {
        }
    }
}
