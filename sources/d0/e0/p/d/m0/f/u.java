package d0.e0.p.d.m0.f;

import d0.e0.p.d.m0.f.q;
import d0.e0.p.d.m0.i.c;
import d0.e0.p.d.m0.i.d;
import d0.e0.p.d.m0.i.e;
import d0.e0.p.d.m0.i.g;
import d0.e0.p.d.m0.i.o;
import d0.e0.p.d.m0.i.p;
import java.io.IOException;
import kotlin.reflect.jvm.internal.impl.protobuf.CodedOutputStream;
import kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException;
import kotlin.reflect.jvm.internal.impl.protobuf.UninitializedMessageException;
/* compiled from: ProtoBuf.java */
/* loaded from: classes3.dex */
public final class u extends g.d<u> implements o {
    public static final u j;
    public static p<u> k = new a();
    private int bitField0_;
    private int flags_;
    private byte memoizedIsInitialized;
    private int memoizedSerializedSize;
    private int name_;
    private int typeId_;
    private q type_;
    private final c unknownFields;
    private int varargElementTypeId_;
    private q varargElementType_;

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static class a extends d0.e0.p.d.m0.i.b<u> {
        @Override // d0.e0.p.d.m0.i.p
        public u parsePartialFrom(d dVar, e eVar) throws InvalidProtocolBufferException {
            return new u(dVar, eVar, null);
        }
    }

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static final class b extends g.c<u, b> implements o {
        public int m;
        public int n;
        public int o;
        public int q;

        /* renamed from: s  reason: collision with root package name */
        public int f3381s;
        public q p = q.getDefaultInstance();
        public q r = q.getDefaultInstance();

        public u buildPartial() {
            u uVar = new u(this, null);
            int i = this.m;
            int i2 = 1;
            if ((i & 1) != 1) {
                i2 = 0;
            }
            uVar.flags_ = this.n;
            if ((i & 2) == 2) {
                i2 |= 2;
            }
            uVar.name_ = this.o;
            if ((i & 4) == 4) {
                i2 |= 4;
            }
            uVar.type_ = this.p;
            if ((i & 8) == 8) {
                i2 |= 8;
            }
            uVar.typeId_ = this.q;
            if ((i & 16) == 16) {
                i2 |= 16;
            }
            uVar.varargElementType_ = this.r;
            if ((i & 32) == 32) {
                i2 |= 32;
            }
            uVar.varargElementTypeId_ = this.f3381s;
            uVar.bitField0_ = i2;
            return uVar;
        }

        public b mergeType(q qVar) {
            if ((this.m & 4) != 4 || this.p == q.getDefaultInstance()) {
                this.p = qVar;
            } else {
                this.p = q.newBuilder(this.p).mergeFrom(qVar).buildPartial();
            }
            this.m |= 4;
            return this;
        }

        public b mergeVarargElementType(q qVar) {
            if ((this.m & 16) != 16 || this.r == q.getDefaultInstance()) {
                this.r = qVar;
            } else {
                this.r = q.newBuilder(this.r).mergeFrom(qVar).buildPartial();
            }
            this.m |= 16;
            return this;
        }

        public b setFlags(int i) {
            this.m |= 1;
            this.n = i;
            return this;
        }

        public b setName(int i) {
            this.m |= 2;
            this.o = i;
            return this;
        }

        public b setTypeId(int i) {
            this.m |= 8;
            this.q = i;
            return this;
        }

        public b setVarargElementTypeId(int i) {
            this.m |= 32;
            this.f3381s = i;
            return this;
        }

        @Override // d0.e0.p.d.m0.i.n.a
        public u build() {
            u buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw new UninitializedMessageException(buildPartial);
        }

        @Override // d0.e0.p.d.m0.i.g.b
        public b clone() {
            return new b().mergeFrom(buildPartial());
        }

        public b mergeFrom(u uVar) {
            if (uVar == u.getDefaultInstance()) {
                return this;
            }
            if (uVar.hasFlags()) {
                setFlags(uVar.getFlags());
            }
            if (uVar.hasName()) {
                setName(uVar.getName());
            }
            if (uVar.hasType()) {
                mergeType(uVar.getType());
            }
            if (uVar.hasTypeId()) {
                setTypeId(uVar.getTypeId());
            }
            if (uVar.hasVarargElementType()) {
                mergeVarargElementType(uVar.getVarargElementType());
            }
            if (uVar.hasVarargElementTypeId()) {
                setVarargElementTypeId(uVar.getVarargElementTypeId());
            }
            b(uVar);
            setUnknownFields(getUnknownFields().concat(uVar.unknownFields));
            return this;
        }

        /* JADX WARN: Removed duplicated region for block: B:14:0x001d  */
        @Override // d0.e0.p.d.m0.i.a.AbstractC0335a, d0.e0.p.d.m0.i.n.a
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public d0.e0.p.d.m0.f.u.b mergeFrom(d0.e0.p.d.m0.i.d r3, d0.e0.p.d.m0.i.e r4) throws java.io.IOException {
            /*
                r2 = this;
                r0 = 0
                d0.e0.p.d.m0.i.p<d0.e0.p.d.m0.f.u> r1 = d0.e0.p.d.m0.f.u.k     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                d0.e0.p.d.m0.f.u r3 = (d0.e0.p.d.m0.f.u) r3     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                if (r3 == 0) goto Le
                r2.mergeFrom(r3)
            Le:
                return r2
            Lf:
                r3 = move-exception
                goto L1b
            L11:
                r3 = move-exception
                d0.e0.p.d.m0.i.n r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                d0.e0.p.d.m0.f.u r4 = (d0.e0.p.d.m0.f.u) r4     // Catch: java.lang.Throwable -> Lf
                throw r3     // Catch: java.lang.Throwable -> L19
            L19:
                r3 = move-exception
                r0 = r4
            L1b:
                if (r0 == 0) goto L20
                r2.mergeFrom(r0)
            L20:
                throw r3
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.f.u.b.mergeFrom(d0.e0.p.d.m0.i.d, d0.e0.p.d.m0.i.e):d0.e0.p.d.m0.f.u$b");
        }
    }

    static {
        u uVar = new u();
        j = uVar;
        uVar.p();
    }

    public u(g.c cVar, d0.e0.p.d.m0.f.a aVar) {
        super(cVar);
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = cVar.getUnknownFields();
    }

    public static u getDefaultInstance() {
        return j;
    }

    public static b newBuilder(u uVar) {
        return newBuilder().mergeFrom(uVar);
    }

    public int getFlags() {
        return this.flags_;
    }

    public int getName() {
        return this.name_;
    }

    @Override // d0.e0.p.d.m0.i.n
    public int getSerializedSize() {
        int i = this.memoizedSerializedSize;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.bitField0_ & 1) == 1) {
            i2 = 0 + CodedOutputStream.computeInt32Size(1, this.flags_);
        }
        if ((this.bitField0_ & 2) == 2) {
            i2 += CodedOutputStream.computeInt32Size(2, this.name_);
        }
        if ((this.bitField0_ & 4) == 4) {
            i2 += CodedOutputStream.computeMessageSize(3, this.type_);
        }
        if ((this.bitField0_ & 16) == 16) {
            i2 += CodedOutputStream.computeMessageSize(4, this.varargElementType_);
        }
        if ((this.bitField0_ & 8) == 8) {
            i2 += CodedOutputStream.computeInt32Size(5, this.typeId_);
        }
        if ((this.bitField0_ & 32) == 32) {
            i2 += CodedOutputStream.computeInt32Size(6, this.varargElementTypeId_);
        }
        int size = this.unknownFields.size() + c() + i2;
        this.memoizedSerializedSize = size;
        return size;
    }

    public q getType() {
        return this.type_;
    }

    public int getTypeId() {
        return this.typeId_;
    }

    public q getVarargElementType() {
        return this.varargElementType_;
    }

    public int getVarargElementTypeId() {
        return this.varargElementTypeId_;
    }

    public boolean hasFlags() {
        return (this.bitField0_ & 1) == 1;
    }

    public boolean hasName() {
        return (this.bitField0_ & 2) == 2;
    }

    public boolean hasType() {
        return (this.bitField0_ & 4) == 4;
    }

    public boolean hasTypeId() {
        return (this.bitField0_ & 8) == 8;
    }

    public boolean hasVarargElementType() {
        return (this.bitField0_ & 16) == 16;
    }

    public boolean hasVarargElementTypeId() {
        return (this.bitField0_ & 32) == 32;
    }

    @Override // d0.e0.p.d.m0.i.o
    public final boolean isInitialized() {
        byte b2 = this.memoizedIsInitialized;
        if (b2 == 1) {
            return true;
        }
        if (b2 == 0) {
            return false;
        }
        if (!hasName()) {
            this.memoizedIsInitialized = (byte) 0;
            return false;
        } else if (hasType() && !getType().isInitialized()) {
            this.memoizedIsInitialized = (byte) 0;
            return false;
        } else if (hasVarargElementType() && !getVarargElementType().isInitialized()) {
            this.memoizedIsInitialized = (byte) 0;
            return false;
        } else if (!b()) {
            this.memoizedIsInitialized = (byte) 0;
            return false;
        } else {
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }
    }

    public final void p() {
        this.flags_ = 0;
        this.name_ = 0;
        this.type_ = q.getDefaultInstance();
        this.typeId_ = 0;
        this.varargElementType_ = q.getDefaultInstance();
        this.varargElementTypeId_ = 0;
    }

    @Override // d0.e0.p.d.m0.i.n
    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        getSerializedSize();
        g.d<MessageType>.a e = e();
        if ((this.bitField0_ & 1) == 1) {
            codedOutputStream.writeInt32(1, this.flags_);
        }
        if ((this.bitField0_ & 2) == 2) {
            codedOutputStream.writeInt32(2, this.name_);
        }
        if ((this.bitField0_ & 4) == 4) {
            codedOutputStream.writeMessage(3, this.type_);
        }
        if ((this.bitField0_ & 16) == 16) {
            codedOutputStream.writeMessage(4, this.varargElementType_);
        }
        if ((this.bitField0_ & 8) == 8) {
            codedOutputStream.writeInt32(5, this.typeId_);
        }
        if ((this.bitField0_ & 32) == 32) {
            codedOutputStream.writeInt32(6, this.varargElementTypeId_);
        }
        e.writeUntil(200, codedOutputStream);
        codedOutputStream.writeRawBytes(this.unknownFields);
    }

    public static b newBuilder() {
        return new b();
    }

    @Override // d0.e0.p.d.m0.i.o
    public u getDefaultInstanceForType() {
        return j;
    }

    @Override // d0.e0.p.d.m0.i.n
    public b newBuilderForType() {
        return newBuilder();
    }

    @Override // d0.e0.p.d.m0.i.n
    public b toBuilder() {
        return newBuilder(this);
    }

    public u() {
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = c.j;
    }

    public u(d dVar, e eVar, d0.e0.p.d.m0.f.a aVar) throws InvalidProtocolBufferException {
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        p();
        c.b newOutput = c.newOutput();
        CodedOutputStream newInstance = CodedOutputStream.newInstance(newOutput, 1);
        boolean z2 = false;
        while (!z2) {
            try {
                try {
                    try {
                        int readTag = dVar.readTag();
                        if (readTag != 0) {
                            if (readTag == 8) {
                                this.bitField0_ |= 1;
                                this.flags_ = dVar.readInt32();
                            } else if (readTag != 16) {
                                q.c cVar = null;
                                if (readTag == 26) {
                                    cVar = (this.bitField0_ & 4) == 4 ? this.type_.toBuilder() : cVar;
                                    q qVar = (q) dVar.readMessage(q.k, eVar);
                                    this.type_ = qVar;
                                    if (cVar != null) {
                                        cVar.mergeFrom(qVar);
                                        this.type_ = cVar.buildPartial();
                                    }
                                    this.bitField0_ |= 4;
                                } else if (readTag == 34) {
                                    cVar = (this.bitField0_ & 16) == 16 ? this.varargElementType_.toBuilder() : cVar;
                                    q qVar2 = (q) dVar.readMessage(q.k, eVar);
                                    this.varargElementType_ = qVar2;
                                    if (cVar != null) {
                                        cVar.mergeFrom(qVar2);
                                        this.varargElementType_ = cVar.buildPartial();
                                    }
                                    this.bitField0_ |= 16;
                                } else if (readTag == 40) {
                                    this.bitField0_ |= 8;
                                    this.typeId_ = dVar.readInt32();
                                } else if (readTag == 48) {
                                    this.bitField0_ |= 32;
                                    this.varargElementTypeId_ = dVar.readInt32();
                                } else if (!f(dVar, newInstance, eVar, readTag)) {
                                }
                            } else {
                                this.bitField0_ |= 2;
                                this.name_ = dVar.readInt32();
                            }
                        }
                        z2 = true;
                    } catch (IOException e) {
                        throw new InvalidProtocolBufferException(e.getMessage()).setUnfinishedMessage(this);
                    }
                } catch (InvalidProtocolBufferException e2) {
                    throw e2.setUnfinishedMessage(this);
                }
            } catch (Throwable th) {
                try {
                    newInstance.flush();
                } catch (IOException unused) {
                } catch (Throwable th2) {
                    this.unknownFields = newOutput.toByteString();
                    throw th2;
                }
                this.unknownFields = newOutput.toByteString();
                d();
                throw th;
            }
        }
        try {
            newInstance.flush();
        } catch (IOException unused2) {
        } catch (Throwable th3) {
            this.unknownFields = newOutput.toByteString();
            throw th3;
        }
        this.unknownFields = newOutput.toByteString();
        d();
    }
}
