package d0.e0.p.d.m0.f;

import d0.e0.p.d.m0.f.e;
import d0.e0.p.d.m0.f.q;
import d0.e0.p.d.m0.f.t;
import d0.e0.p.d.m0.i.c;
import d0.e0.p.d.m0.i.d;
import d0.e0.p.d.m0.i.e;
import d0.e0.p.d.m0.i.g;
import d0.e0.p.d.m0.i.o;
import d0.e0.p.d.m0.i.p;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import kotlin.reflect.jvm.internal.impl.protobuf.CodedOutputStream;
import kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException;
import kotlin.reflect.jvm.internal.impl.protobuf.UninitializedMessageException;
/* compiled from: ProtoBuf.java */
/* loaded from: classes3.dex */
public final class i extends g.d<i> implements o {
    public static final i j;
    public static p<i> k = new a();
    private int bitField0_;
    private e contract_;
    private int flags_;
    private byte memoizedIsInitialized;
    private int memoizedSerializedSize;
    private int name_;
    private int oldFlags_;
    private int receiverTypeId_;
    private q receiverType_;
    private int returnTypeId_;
    private q returnType_;
    private List<s> typeParameter_;
    private t typeTable_;
    private final c unknownFields;
    private List<u> valueParameter_;
    private List<Integer> versionRequirement_;

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static class a extends d0.e0.p.d.m0.i.b<i> {
        @Override // d0.e0.p.d.m0.i.p
        public i parsePartialFrom(d dVar, e eVar) throws InvalidProtocolBufferException {
            return new i(dVar, eVar, null);
        }
    }

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static final class b extends g.c<i, b> implements o {
        public int m;
        public int p;
        public int r;
        public int u;
        public int n = 6;
        public int o = 6;
        public q q = q.getDefaultInstance();

        /* renamed from: s  reason: collision with root package name */
        public List<s> f3369s = Collections.emptyList();
        public q t = q.getDefaultInstance();
        public List<u> v = Collections.emptyList();
        public t w = t.getDefaultInstance();

        /* renamed from: x  reason: collision with root package name */
        public List<Integer> f3370x = Collections.emptyList();

        /* renamed from: y  reason: collision with root package name */
        public e f3371y = e.getDefaultInstance();

        public i buildPartial() {
            i iVar = new i(this, null);
            int i = this.m;
            int i2 = 1;
            if ((i & 1) != 1) {
                i2 = 0;
            }
            iVar.flags_ = this.n;
            if ((i & 2) == 2) {
                i2 |= 2;
            }
            iVar.oldFlags_ = this.o;
            if ((i & 4) == 4) {
                i2 |= 4;
            }
            iVar.name_ = this.p;
            if ((i & 8) == 8) {
                i2 |= 8;
            }
            iVar.returnType_ = this.q;
            if ((i & 16) == 16) {
                i2 |= 16;
            }
            iVar.returnTypeId_ = this.r;
            if ((this.m & 32) == 32) {
                this.f3369s = Collections.unmodifiableList(this.f3369s);
                this.m &= -33;
            }
            iVar.typeParameter_ = this.f3369s;
            if ((i & 64) == 64) {
                i2 |= 32;
            }
            iVar.receiverType_ = this.t;
            if ((i & 128) == 128) {
                i2 |= 64;
            }
            iVar.receiverTypeId_ = this.u;
            if ((this.m & 256) == 256) {
                this.v = Collections.unmodifiableList(this.v);
                this.m &= -257;
            }
            iVar.valueParameter_ = this.v;
            if ((i & 512) == 512) {
                i2 |= 128;
            }
            iVar.typeTable_ = this.w;
            if ((this.m & 1024) == 1024) {
                this.f3370x = Collections.unmodifiableList(this.f3370x);
                this.m &= -1025;
            }
            iVar.versionRequirement_ = this.f3370x;
            if ((i & 2048) == 2048) {
                i2 |= 256;
            }
            iVar.contract_ = this.f3371y;
            iVar.bitField0_ = i2;
            return iVar;
        }

        public b mergeContract(e eVar) {
            if ((this.m & 2048) != 2048 || this.f3371y == e.getDefaultInstance()) {
                this.f3371y = eVar;
            } else {
                this.f3371y = e.newBuilder(this.f3371y).mergeFrom(eVar).buildPartial();
            }
            this.m |= 2048;
            return this;
        }

        public b mergeReceiverType(q qVar) {
            if ((this.m & 64) != 64 || this.t == q.getDefaultInstance()) {
                this.t = qVar;
            } else {
                this.t = q.newBuilder(this.t).mergeFrom(qVar).buildPartial();
            }
            this.m |= 64;
            return this;
        }

        public b mergeReturnType(q qVar) {
            if ((this.m & 8) != 8 || this.q == q.getDefaultInstance()) {
                this.q = qVar;
            } else {
                this.q = q.newBuilder(this.q).mergeFrom(qVar).buildPartial();
            }
            this.m |= 8;
            return this;
        }

        public b mergeTypeTable(t tVar) {
            if ((this.m & 512) != 512 || this.w == t.getDefaultInstance()) {
                this.w = tVar;
            } else {
                this.w = t.newBuilder(this.w).mergeFrom(tVar).buildPartial();
            }
            this.m |= 512;
            return this;
        }

        public b setFlags(int i) {
            this.m |= 1;
            this.n = i;
            return this;
        }

        public b setName(int i) {
            this.m |= 4;
            this.p = i;
            return this;
        }

        public b setOldFlags(int i) {
            this.m |= 2;
            this.o = i;
            return this;
        }

        public b setReceiverTypeId(int i) {
            this.m |= 128;
            this.u = i;
            return this;
        }

        public b setReturnTypeId(int i) {
            this.m |= 16;
            this.r = i;
            return this;
        }

        @Override // d0.e0.p.d.m0.i.n.a
        public i build() {
            i buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw new UninitializedMessageException(buildPartial);
        }

        @Override // d0.e0.p.d.m0.i.g.b
        public b clone() {
            return new b().mergeFrom(buildPartial());
        }

        public b mergeFrom(i iVar) {
            if (iVar == i.getDefaultInstance()) {
                return this;
            }
            if (iVar.hasFlags()) {
                setFlags(iVar.getFlags());
            }
            if (iVar.hasOldFlags()) {
                setOldFlags(iVar.getOldFlags());
            }
            if (iVar.hasName()) {
                setName(iVar.getName());
            }
            if (iVar.hasReturnType()) {
                mergeReturnType(iVar.getReturnType());
            }
            if (iVar.hasReturnTypeId()) {
                setReturnTypeId(iVar.getReturnTypeId());
            }
            if (!iVar.typeParameter_.isEmpty()) {
                if (this.f3369s.isEmpty()) {
                    this.f3369s = iVar.typeParameter_;
                    this.m &= -33;
                } else {
                    if ((this.m & 32) != 32) {
                        this.f3369s = new ArrayList(this.f3369s);
                        this.m |= 32;
                    }
                    this.f3369s.addAll(iVar.typeParameter_);
                }
            }
            if (iVar.hasReceiverType()) {
                mergeReceiverType(iVar.getReceiverType());
            }
            if (iVar.hasReceiverTypeId()) {
                setReceiverTypeId(iVar.getReceiverTypeId());
            }
            if (!iVar.valueParameter_.isEmpty()) {
                if (this.v.isEmpty()) {
                    this.v = iVar.valueParameter_;
                    this.m &= -257;
                } else {
                    if ((this.m & 256) != 256) {
                        this.v = new ArrayList(this.v);
                        this.m |= 256;
                    }
                    this.v.addAll(iVar.valueParameter_);
                }
            }
            if (iVar.hasTypeTable()) {
                mergeTypeTable(iVar.getTypeTable());
            }
            if (!iVar.versionRequirement_.isEmpty()) {
                if (this.f3370x.isEmpty()) {
                    this.f3370x = iVar.versionRequirement_;
                    this.m &= -1025;
                } else {
                    if ((this.m & 1024) != 1024) {
                        this.f3370x = new ArrayList(this.f3370x);
                        this.m |= 1024;
                    }
                    this.f3370x.addAll(iVar.versionRequirement_);
                }
            }
            if (iVar.hasContract()) {
                mergeContract(iVar.getContract());
            }
            b(iVar);
            setUnknownFields(getUnknownFields().concat(iVar.unknownFields));
            return this;
        }

        /* JADX WARN: Removed duplicated region for block: B:14:0x001d  */
        @Override // d0.e0.p.d.m0.i.a.AbstractC0335a, d0.e0.p.d.m0.i.n.a
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public d0.e0.p.d.m0.f.i.b mergeFrom(d0.e0.p.d.m0.i.d r3, d0.e0.p.d.m0.i.e r4) throws java.io.IOException {
            /*
                r2 = this;
                r0 = 0
                d0.e0.p.d.m0.i.p<d0.e0.p.d.m0.f.i> r1 = d0.e0.p.d.m0.f.i.k     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                d0.e0.p.d.m0.f.i r3 = (d0.e0.p.d.m0.f.i) r3     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                if (r3 == 0) goto Le
                r2.mergeFrom(r3)
            Le:
                return r2
            Lf:
                r3 = move-exception
                goto L1b
            L11:
                r3 = move-exception
                d0.e0.p.d.m0.i.n r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                d0.e0.p.d.m0.f.i r4 = (d0.e0.p.d.m0.f.i) r4     // Catch: java.lang.Throwable -> Lf
                throw r3     // Catch: java.lang.Throwable -> L19
            L19:
                r3 = move-exception
                r0 = r4
            L1b:
                if (r0 == 0) goto L20
                r2.mergeFrom(r0)
            L20:
                throw r3
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.f.i.b.mergeFrom(d0.e0.p.d.m0.i.d, d0.e0.p.d.m0.i.e):d0.e0.p.d.m0.f.i$b");
        }
    }

    static {
        i iVar = new i();
        j = iVar;
        iVar.y();
    }

    public i(g.c cVar, d0.e0.p.d.m0.f.a aVar) {
        super(cVar);
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = cVar.getUnknownFields();
    }

    public static i getDefaultInstance() {
        return j;
    }

    public static b newBuilder(i iVar) {
        return newBuilder().mergeFrom(iVar);
    }

    public static i parseFrom(InputStream inputStream, e eVar) throws IOException {
        return (i) ((d0.e0.p.d.m0.i.b) k).parseFrom(inputStream, eVar);
    }

    public e getContract() {
        return this.contract_;
    }

    public int getFlags() {
        return this.flags_;
    }

    public int getName() {
        return this.name_;
    }

    public int getOldFlags() {
        return this.oldFlags_;
    }

    public q getReceiverType() {
        return this.receiverType_;
    }

    public int getReceiverTypeId() {
        return this.receiverTypeId_;
    }

    public q getReturnType() {
        return this.returnType_;
    }

    public int getReturnTypeId() {
        return this.returnTypeId_;
    }

    @Override // d0.e0.p.d.m0.i.n
    public int getSerializedSize() {
        int i = this.memoizedSerializedSize;
        if (i != -1) {
            return i;
        }
        int computeInt32Size = (this.bitField0_ & 2) == 2 ? CodedOutputStream.computeInt32Size(1, this.oldFlags_) + 0 : 0;
        if ((this.bitField0_ & 4) == 4) {
            computeInt32Size += CodedOutputStream.computeInt32Size(2, this.name_);
        }
        if ((this.bitField0_ & 8) == 8) {
            computeInt32Size += CodedOutputStream.computeMessageSize(3, this.returnType_);
        }
        for (int i2 = 0; i2 < this.typeParameter_.size(); i2++) {
            computeInt32Size += CodedOutputStream.computeMessageSize(4, this.typeParameter_.get(i2));
        }
        if ((this.bitField0_ & 32) == 32) {
            computeInt32Size += CodedOutputStream.computeMessageSize(5, this.receiverType_);
        }
        for (int i3 = 0; i3 < this.valueParameter_.size(); i3++) {
            computeInt32Size += CodedOutputStream.computeMessageSize(6, this.valueParameter_.get(i3));
        }
        if ((this.bitField0_ & 16) == 16) {
            computeInt32Size += CodedOutputStream.computeInt32Size(7, this.returnTypeId_);
        }
        if ((this.bitField0_ & 64) == 64) {
            computeInt32Size += CodedOutputStream.computeInt32Size(8, this.receiverTypeId_);
        }
        if ((this.bitField0_ & 1) == 1) {
            computeInt32Size += CodedOutputStream.computeInt32Size(9, this.flags_);
        }
        if ((this.bitField0_ & 128) == 128) {
            computeInt32Size += CodedOutputStream.computeMessageSize(30, this.typeTable_);
        }
        int i4 = 0;
        for (int i5 = 0; i5 < this.versionRequirement_.size(); i5++) {
            i4 += CodedOutputStream.computeInt32SizeNoTag(this.versionRequirement_.get(i5).intValue());
        }
        int size = (getVersionRequirementList().size() * 2) + computeInt32Size + i4;
        if ((this.bitField0_ & 256) == 256) {
            size += CodedOutputStream.computeMessageSize(32, this.contract_);
        }
        int size2 = this.unknownFields.size() + c() + size;
        this.memoizedSerializedSize = size2;
        return size2;
    }

    public s getTypeParameter(int i) {
        return this.typeParameter_.get(i);
    }

    public int getTypeParameterCount() {
        return this.typeParameter_.size();
    }

    public List<s> getTypeParameterList() {
        return this.typeParameter_;
    }

    public t getTypeTable() {
        return this.typeTable_;
    }

    public u getValueParameter(int i) {
        return this.valueParameter_.get(i);
    }

    public int getValueParameterCount() {
        return this.valueParameter_.size();
    }

    public List<u> getValueParameterList() {
        return this.valueParameter_;
    }

    public List<Integer> getVersionRequirementList() {
        return this.versionRequirement_;
    }

    public boolean hasContract() {
        return (this.bitField0_ & 256) == 256;
    }

    public boolean hasFlags() {
        return (this.bitField0_ & 1) == 1;
    }

    public boolean hasName() {
        return (this.bitField0_ & 4) == 4;
    }

    public boolean hasOldFlags() {
        return (this.bitField0_ & 2) == 2;
    }

    public boolean hasReceiverType() {
        return (this.bitField0_ & 32) == 32;
    }

    public boolean hasReceiverTypeId() {
        return (this.bitField0_ & 64) == 64;
    }

    public boolean hasReturnType() {
        return (this.bitField0_ & 8) == 8;
    }

    public boolean hasReturnTypeId() {
        return (this.bitField0_ & 16) == 16;
    }

    public boolean hasTypeTable() {
        return (this.bitField0_ & 128) == 128;
    }

    @Override // d0.e0.p.d.m0.i.o
    public final boolean isInitialized() {
        byte b2 = this.memoizedIsInitialized;
        if (b2 == 1) {
            return true;
        }
        if (b2 == 0) {
            return false;
        }
        if (!hasName()) {
            this.memoizedIsInitialized = (byte) 0;
            return false;
        } else if (!hasReturnType() || getReturnType().isInitialized()) {
            for (int i = 0; i < getTypeParameterCount(); i++) {
                if (!getTypeParameter(i).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            if (!hasReceiverType() || getReceiverType().isInitialized()) {
                for (int i2 = 0; i2 < getValueParameterCount(); i2++) {
                    if (!getValueParameter(i2).isInitialized()) {
                        this.memoizedIsInitialized = (byte) 0;
                        return false;
                    }
                }
                if (hasTypeTable() && !getTypeTable().isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                } else if (hasContract() && !getContract().isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                } else if (!b()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                } else {
                    this.memoizedIsInitialized = (byte) 1;
                    return true;
                }
            } else {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
        } else {
            this.memoizedIsInitialized = (byte) 0;
            return false;
        }
    }

    @Override // d0.e0.p.d.m0.i.n
    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        getSerializedSize();
        g.d<MessageType>.a e = e();
        if ((this.bitField0_ & 2) == 2) {
            codedOutputStream.writeInt32(1, this.oldFlags_);
        }
        if ((this.bitField0_ & 4) == 4) {
            codedOutputStream.writeInt32(2, this.name_);
        }
        if ((this.bitField0_ & 8) == 8) {
            codedOutputStream.writeMessage(3, this.returnType_);
        }
        for (int i = 0; i < this.typeParameter_.size(); i++) {
            codedOutputStream.writeMessage(4, this.typeParameter_.get(i));
        }
        if ((this.bitField0_ & 32) == 32) {
            codedOutputStream.writeMessage(5, this.receiverType_);
        }
        for (int i2 = 0; i2 < this.valueParameter_.size(); i2++) {
            codedOutputStream.writeMessage(6, this.valueParameter_.get(i2));
        }
        if ((this.bitField0_ & 16) == 16) {
            codedOutputStream.writeInt32(7, this.returnTypeId_);
        }
        if ((this.bitField0_ & 64) == 64) {
            codedOutputStream.writeInt32(8, this.receiverTypeId_);
        }
        if ((this.bitField0_ & 1) == 1) {
            codedOutputStream.writeInt32(9, this.flags_);
        }
        if ((this.bitField0_ & 128) == 128) {
            codedOutputStream.writeMessage(30, this.typeTable_);
        }
        for (int i3 = 0; i3 < this.versionRequirement_.size(); i3++) {
            codedOutputStream.writeInt32(31, this.versionRequirement_.get(i3).intValue());
        }
        if ((this.bitField0_ & 256) == 256) {
            codedOutputStream.writeMessage(32, this.contract_);
        }
        e.writeUntil(19000, codedOutputStream);
        codedOutputStream.writeRawBytes(this.unknownFields);
    }

    public final void y() {
        this.flags_ = 6;
        this.oldFlags_ = 6;
        this.name_ = 0;
        this.returnType_ = q.getDefaultInstance();
        this.returnTypeId_ = 0;
        this.typeParameter_ = Collections.emptyList();
        this.receiverType_ = q.getDefaultInstance();
        this.receiverTypeId_ = 0;
        this.valueParameter_ = Collections.emptyList();
        this.typeTable_ = t.getDefaultInstance();
        this.versionRequirement_ = Collections.emptyList();
        this.contract_ = e.getDefaultInstance();
    }

    public static b newBuilder() {
        return new b();
    }

    @Override // d0.e0.p.d.m0.i.o
    public i getDefaultInstanceForType() {
        return j;
    }

    @Override // d0.e0.p.d.m0.i.n
    public b newBuilderForType() {
        return newBuilder();
    }

    @Override // d0.e0.p.d.m0.i.n
    public b toBuilder() {
        return newBuilder(this);
    }

    public i() {
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = c.j;
    }

    /* JADX WARN: Finally extract failed */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v0 */
    /* JADX WARN: Type inference failed for: r4v1 */
    /* JADX WARN: Type inference failed for: r4v2, types: [boolean] */
    public i(d dVar, e eVar, d0.e0.p.d.m0.f.a aVar) throws InvalidProtocolBufferException {
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        y();
        c.b newOutput = c.newOutput();
        CodedOutputStream newInstance = CodedOutputStream.newInstance(newOutput, 1);
        boolean z2 = false;
        boolean z3 = false;
        while (true) {
            ?? r4 = 32;
            if (!z2) {
                try {
                    try {
                        int readTag = dVar.readTag();
                        e.b bVar = null;
                        t.b builder = null;
                        q.c builder2 = null;
                        q.c builder3 = null;
                        switch (readTag) {
                            case 0:
                                break;
                            case 8:
                                this.bitField0_ |= 2;
                                this.oldFlags_ = dVar.readInt32();
                                continue;
                            case 16:
                                this.bitField0_ |= 4;
                                this.name_ = dVar.readInt32();
                                continue;
                            case 26:
                                builder3 = (this.bitField0_ & 8) == 8 ? this.returnType_.toBuilder() : builder3;
                                q qVar = (q) dVar.readMessage(q.k, eVar);
                                this.returnType_ = qVar;
                                if (builder3 != null) {
                                    builder3.mergeFrom(qVar);
                                    this.returnType_ = builder3.buildPartial();
                                }
                                this.bitField0_ |= 8;
                                continue;
                            case 34:
                                if (!(z3 & true)) {
                                    this.typeParameter_ = new ArrayList();
                                    z3 |= true;
                                }
                                this.typeParameter_.add(dVar.readMessage(s.k, eVar));
                                continue;
                            case 42:
                                builder2 = (this.bitField0_ & 32) == 32 ? this.receiverType_.toBuilder() : builder2;
                                q qVar2 = (q) dVar.readMessage(q.k, eVar);
                                this.receiverType_ = qVar2;
                                if (builder2 != null) {
                                    builder2.mergeFrom(qVar2);
                                    this.receiverType_ = builder2.buildPartial();
                                }
                                this.bitField0_ |= 32;
                                continue;
                            case 50:
                                if (!(z3 & true)) {
                                    this.valueParameter_ = new ArrayList();
                                    z3 |= true;
                                }
                                this.valueParameter_.add(dVar.readMessage(u.k, eVar));
                                continue;
                            case 56:
                                this.bitField0_ |= 16;
                                this.returnTypeId_ = dVar.readInt32();
                                continue;
                            case 64:
                                this.bitField0_ |= 64;
                                this.receiverTypeId_ = dVar.readInt32();
                                continue;
                            case 72:
                                this.bitField0_ |= 1;
                                this.flags_ = dVar.readInt32();
                                continue;
                            case 242:
                                builder = (this.bitField0_ & 128) == 128 ? this.typeTable_.toBuilder() : builder;
                                t tVar = (t) dVar.readMessage(t.k, eVar);
                                this.typeTable_ = tVar;
                                if (builder != null) {
                                    builder.mergeFrom(tVar);
                                    this.typeTable_ = builder.buildPartial();
                                }
                                this.bitField0_ |= 128;
                                continue;
                            case 248:
                                if (!(z3 & true)) {
                                    this.versionRequirement_ = new ArrayList();
                                    z3 |= true;
                                }
                                this.versionRequirement_.add(Integer.valueOf(dVar.readInt32()));
                                continue;
                            case 250:
                                int pushLimit = dVar.pushLimit(dVar.readRawVarint32());
                                if (!(z3 & true) && dVar.getBytesUntilLimit() > 0) {
                                    this.versionRequirement_ = new ArrayList();
                                    z3 |= true;
                                }
                                while (dVar.getBytesUntilLimit() > 0) {
                                    this.versionRequirement_.add(Integer.valueOf(dVar.readInt32()));
                                }
                                dVar.popLimit(pushLimit);
                                continue;
                                break;
                            case 258:
                                bVar = (this.bitField0_ & 256) == 256 ? this.contract_.toBuilder() : bVar;
                                e eVar2 = (e) dVar.readMessage(e.k, eVar);
                                this.contract_ = eVar2;
                                if (bVar != null) {
                                    bVar.mergeFrom(eVar2);
                                    this.contract_ = bVar.buildPartial();
                                }
                                this.bitField0_ |= 256;
                                continue;
                            default:
                                r4 = f(dVar, newInstance, eVar, readTag);
                                if (r4 == 0) {
                                    break;
                                } else {
                                    continue;
                                }
                        }
                        z2 = true;
                    } catch (Throwable th) {
                        if ((z3 & true) == r4) {
                            this.typeParameter_ = Collections.unmodifiableList(this.typeParameter_);
                        }
                        if (z3 & true) {
                            this.valueParameter_ = Collections.unmodifiableList(this.valueParameter_);
                        }
                        if (z3 & true) {
                            this.versionRequirement_ = Collections.unmodifiableList(this.versionRequirement_);
                        }
                        try {
                            newInstance.flush();
                        } catch (IOException unused) {
                            this.unknownFields = newOutput.toByteString();
                            d();
                            throw th;
                        } catch (Throwable th2) {
                            this.unknownFields = newOutput.toByteString();
                            throw th2;
                        }
                    }
                } catch (InvalidProtocolBufferException e) {
                    throw e.setUnfinishedMessage(this);
                } catch (IOException e2) {
                    throw new InvalidProtocolBufferException(e2.getMessage()).setUnfinishedMessage(this);
                }
            } else {
                if (z3 & true) {
                    this.typeParameter_ = Collections.unmodifiableList(this.typeParameter_);
                }
                if (z3 & true) {
                    this.valueParameter_ = Collections.unmodifiableList(this.valueParameter_);
                }
                if (z3 & true) {
                    this.versionRequirement_ = Collections.unmodifiableList(this.versionRequirement_);
                }
                try {
                    newInstance.flush();
                } catch (IOException unused2) {
                    this.unknownFields = newOutput.toByteString();
                    d();
                    return;
                } catch (Throwable th3) {
                    this.unknownFields = newOutput.toByteString();
                    throw th3;
                }
            }
        }
    }
}
