package d0.e0.p.d.m0.f;

import d0.e0.p.d.m0.f.t;
import d0.e0.p.d.m0.f.w;
import d0.e0.p.d.m0.i.c;
import d0.e0.p.d.m0.i.d;
import d0.e0.p.d.m0.i.e;
import d0.e0.p.d.m0.i.g;
import d0.e0.p.d.m0.i.h;
import d0.e0.p.d.m0.i.o;
import d0.e0.p.d.m0.i.p;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import kotlin.reflect.jvm.internal.impl.protobuf.CodedOutputStream;
import kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException;
import kotlin.reflect.jvm.internal.impl.protobuf.UninitializedMessageException;
/* compiled from: ProtoBuf.java */
/* loaded from: classes3.dex */
public final class c extends g.d<c> implements o {
    public static final c j;
    public static p<c> k = new a();
    private int bitField0_;
    private int companionObjectName_;
    private List<d> constructor_;
    private List<g> enumEntry_;
    private int flags_;
    private int fqName_;
    private List<i> function_;
    private byte memoizedIsInitialized;
    private int memoizedSerializedSize;
    private int nestedClassNameMemoizedSerializedSize;
    private List<Integer> nestedClassName_;
    private List<n> property_;
    private int sealedSubclassFqNameMemoizedSerializedSize;
    private List<Integer> sealedSubclassFqName_;
    private int supertypeIdMemoizedSerializedSize;
    private List<Integer> supertypeId_;
    private List<q> supertype_;
    private List<r> typeAlias_;
    private List<s> typeParameter_;
    private t typeTable_;
    private final d0.e0.p.d.m0.i.c unknownFields;
    private w versionRequirementTable_;
    private List<Integer> versionRequirement_;

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static class a extends d0.e0.p.d.m0.i.b<c> {
        @Override // d0.e0.p.d.m0.i.p
        public c parsePartialFrom(d dVar, e eVar) throws InvalidProtocolBufferException {
            return new c(dVar, eVar);
        }
    }

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static final class b extends g.c<c, b> implements o {
        public int m;
        public int o;
        public int p;
        public int n = 6;
        public List<s> q = Collections.emptyList();
        public List<q> r = Collections.emptyList();

        /* renamed from: s  reason: collision with root package name */
        public List<Integer> f3365s = Collections.emptyList();
        public List<Integer> t = Collections.emptyList();
        public List<d> u = Collections.emptyList();
        public List<i> v = Collections.emptyList();
        public List<n> w = Collections.emptyList();

        /* renamed from: x  reason: collision with root package name */
        public List<r> f3366x = Collections.emptyList();

        /* renamed from: y  reason: collision with root package name */
        public List<g> f3367y = Collections.emptyList();

        /* renamed from: z  reason: collision with root package name */
        public List<Integer> f3368z = Collections.emptyList();
        public t A = t.getDefaultInstance();
        public List<Integer> B = Collections.emptyList();
        public w C = w.getDefaultInstance();

        public c buildPartial() {
            c cVar = new c(this, (d0.e0.p.d.m0.f.a) null);
            int i = this.m;
            int i2 = 1;
            if ((i & 1) != 1) {
                i2 = 0;
            }
            cVar.flags_ = this.n;
            if ((i & 2) == 2) {
                i2 |= 2;
            }
            cVar.fqName_ = this.o;
            if ((i & 4) == 4) {
                i2 |= 4;
            }
            cVar.companionObjectName_ = this.p;
            if ((this.m & 8) == 8) {
                this.q = Collections.unmodifiableList(this.q);
                this.m &= -9;
            }
            cVar.typeParameter_ = this.q;
            if ((this.m & 16) == 16) {
                this.r = Collections.unmodifiableList(this.r);
                this.m &= -17;
            }
            cVar.supertype_ = this.r;
            if ((this.m & 32) == 32) {
                this.f3365s = Collections.unmodifiableList(this.f3365s);
                this.m &= -33;
            }
            cVar.supertypeId_ = this.f3365s;
            if ((this.m & 64) == 64) {
                this.t = Collections.unmodifiableList(this.t);
                this.m &= -65;
            }
            cVar.nestedClassName_ = this.t;
            if ((this.m & 128) == 128) {
                this.u = Collections.unmodifiableList(this.u);
                this.m &= -129;
            }
            cVar.constructor_ = this.u;
            if ((this.m & 256) == 256) {
                this.v = Collections.unmodifiableList(this.v);
                this.m &= -257;
            }
            cVar.function_ = this.v;
            if ((this.m & 512) == 512) {
                this.w = Collections.unmodifiableList(this.w);
                this.m &= -513;
            }
            cVar.property_ = this.w;
            if ((this.m & 1024) == 1024) {
                this.f3366x = Collections.unmodifiableList(this.f3366x);
                this.m &= -1025;
            }
            cVar.typeAlias_ = this.f3366x;
            if ((this.m & 2048) == 2048) {
                this.f3367y = Collections.unmodifiableList(this.f3367y);
                this.m &= -2049;
            }
            cVar.enumEntry_ = this.f3367y;
            if ((this.m & 4096) == 4096) {
                this.f3368z = Collections.unmodifiableList(this.f3368z);
                this.m &= -4097;
            }
            cVar.sealedSubclassFqName_ = this.f3368z;
            if ((i & 8192) == 8192) {
                i2 |= 8;
            }
            cVar.typeTable_ = this.A;
            if ((this.m & 16384) == 16384) {
                this.B = Collections.unmodifiableList(this.B);
                this.m &= -16385;
            }
            cVar.versionRequirement_ = this.B;
            if ((i & 32768) == 32768) {
                i2 |= 16;
            }
            cVar.versionRequirementTable_ = this.C;
            cVar.bitField0_ = i2;
            return cVar;
        }

        public b mergeTypeTable(t tVar) {
            if ((this.m & 8192) != 8192 || this.A == t.getDefaultInstance()) {
                this.A = tVar;
            } else {
                this.A = t.newBuilder(this.A).mergeFrom(tVar).buildPartial();
            }
            this.m |= 8192;
            return this;
        }

        public b mergeVersionRequirementTable(w wVar) {
            if ((this.m & 32768) != 32768 || this.C == w.getDefaultInstance()) {
                this.C = wVar;
            } else {
                this.C = w.newBuilder(this.C).mergeFrom(wVar).buildPartial();
            }
            this.m |= 32768;
            return this;
        }

        public b setCompanionObjectName(int i) {
            this.m |= 4;
            this.p = i;
            return this;
        }

        public b setFlags(int i) {
            this.m |= 1;
            this.n = i;
            return this;
        }

        public b setFqName(int i) {
            this.m |= 2;
            this.o = i;
            return this;
        }

        @Override // d0.e0.p.d.m0.i.n.a
        public c build() {
            c buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw new UninitializedMessageException(buildPartial);
        }

        @Override // d0.e0.p.d.m0.i.g.b
        public b clone() {
            return new b().mergeFrom(buildPartial());
        }

        public b mergeFrom(c cVar) {
            if (cVar == c.getDefaultInstance()) {
                return this;
            }
            if (cVar.hasFlags()) {
                setFlags(cVar.getFlags());
            }
            if (cVar.hasFqName()) {
                setFqName(cVar.getFqName());
            }
            if (cVar.hasCompanionObjectName()) {
                setCompanionObjectName(cVar.getCompanionObjectName());
            }
            if (!cVar.typeParameter_.isEmpty()) {
                if (this.q.isEmpty()) {
                    this.q = cVar.typeParameter_;
                    this.m &= -9;
                } else {
                    if ((this.m & 8) != 8) {
                        this.q = new ArrayList(this.q);
                        this.m |= 8;
                    }
                    this.q.addAll(cVar.typeParameter_);
                }
            }
            if (!cVar.supertype_.isEmpty()) {
                if (this.r.isEmpty()) {
                    this.r = cVar.supertype_;
                    this.m &= -17;
                } else {
                    if ((this.m & 16) != 16) {
                        this.r = new ArrayList(this.r);
                        this.m |= 16;
                    }
                    this.r.addAll(cVar.supertype_);
                }
            }
            if (!cVar.supertypeId_.isEmpty()) {
                if (this.f3365s.isEmpty()) {
                    this.f3365s = cVar.supertypeId_;
                    this.m &= -33;
                } else {
                    if ((this.m & 32) != 32) {
                        this.f3365s = new ArrayList(this.f3365s);
                        this.m |= 32;
                    }
                    this.f3365s.addAll(cVar.supertypeId_);
                }
            }
            if (!cVar.nestedClassName_.isEmpty()) {
                if (this.t.isEmpty()) {
                    this.t = cVar.nestedClassName_;
                    this.m &= -65;
                } else {
                    if ((this.m & 64) != 64) {
                        this.t = new ArrayList(this.t);
                        this.m |= 64;
                    }
                    this.t.addAll(cVar.nestedClassName_);
                }
            }
            if (!cVar.constructor_.isEmpty()) {
                if (this.u.isEmpty()) {
                    this.u = cVar.constructor_;
                    this.m &= -129;
                } else {
                    if ((this.m & 128) != 128) {
                        this.u = new ArrayList(this.u);
                        this.m |= 128;
                    }
                    this.u.addAll(cVar.constructor_);
                }
            }
            if (!cVar.function_.isEmpty()) {
                if (this.v.isEmpty()) {
                    this.v = cVar.function_;
                    this.m &= -257;
                } else {
                    if ((this.m & 256) != 256) {
                        this.v = new ArrayList(this.v);
                        this.m |= 256;
                    }
                    this.v.addAll(cVar.function_);
                }
            }
            if (!cVar.property_.isEmpty()) {
                if (this.w.isEmpty()) {
                    this.w = cVar.property_;
                    this.m &= -513;
                } else {
                    if ((this.m & 512) != 512) {
                        this.w = new ArrayList(this.w);
                        this.m |= 512;
                    }
                    this.w.addAll(cVar.property_);
                }
            }
            if (!cVar.typeAlias_.isEmpty()) {
                if (this.f3366x.isEmpty()) {
                    this.f3366x = cVar.typeAlias_;
                    this.m &= -1025;
                } else {
                    if ((this.m & 1024) != 1024) {
                        this.f3366x = new ArrayList(this.f3366x);
                        this.m |= 1024;
                    }
                    this.f3366x.addAll(cVar.typeAlias_);
                }
            }
            if (!cVar.enumEntry_.isEmpty()) {
                if (this.f3367y.isEmpty()) {
                    this.f3367y = cVar.enumEntry_;
                    this.m &= -2049;
                } else {
                    if ((this.m & 2048) != 2048) {
                        this.f3367y = new ArrayList(this.f3367y);
                        this.m |= 2048;
                    }
                    this.f3367y.addAll(cVar.enumEntry_);
                }
            }
            if (!cVar.sealedSubclassFqName_.isEmpty()) {
                if (this.f3368z.isEmpty()) {
                    this.f3368z = cVar.sealedSubclassFqName_;
                    this.m &= -4097;
                } else {
                    if ((this.m & 4096) != 4096) {
                        this.f3368z = new ArrayList(this.f3368z);
                        this.m |= 4096;
                    }
                    this.f3368z.addAll(cVar.sealedSubclassFqName_);
                }
            }
            if (cVar.hasTypeTable()) {
                mergeTypeTable(cVar.getTypeTable());
            }
            if (!cVar.versionRequirement_.isEmpty()) {
                if (this.B.isEmpty()) {
                    this.B = cVar.versionRequirement_;
                    this.m &= -16385;
                } else {
                    if ((this.m & 16384) != 16384) {
                        this.B = new ArrayList(this.B);
                        this.m |= 16384;
                    }
                    this.B.addAll(cVar.versionRequirement_);
                }
            }
            if (cVar.hasVersionRequirementTable()) {
                mergeVersionRequirementTable(cVar.getVersionRequirementTable());
            }
            b(cVar);
            setUnknownFields(getUnknownFields().concat(cVar.unknownFields));
            return this;
        }

        /* JADX WARN: Removed duplicated region for block: B:14:0x001d  */
        @Override // d0.e0.p.d.m0.i.a.AbstractC0335a, d0.e0.p.d.m0.i.n.a
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public d0.e0.p.d.m0.f.c.b mergeFrom(d0.e0.p.d.m0.i.d r3, d0.e0.p.d.m0.i.e r4) throws java.io.IOException {
            /*
                r2 = this;
                r0 = 0
                d0.e0.p.d.m0.i.p<d0.e0.p.d.m0.f.c> r1 = d0.e0.p.d.m0.f.c.k     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                d0.e0.p.d.m0.f.c r3 = (d0.e0.p.d.m0.f.c) r3     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                if (r3 == 0) goto Le
                r2.mergeFrom(r3)
            Le:
                return r2
            Lf:
                r3 = move-exception
                goto L1b
            L11:
                r3 = move-exception
                d0.e0.p.d.m0.i.n r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                d0.e0.p.d.m0.f.c r4 = (d0.e0.p.d.m0.f.c) r4     // Catch: java.lang.Throwable -> Lf
                throw r3     // Catch: java.lang.Throwable -> L19
            L19:
                r3 = move-exception
                r0 = r4
            L1b:
                if (r0 == 0) goto L20
                r2.mergeFrom(r0)
            L20:
                throw r3
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.f.c.b.mergeFrom(d0.e0.p.d.m0.i.d, d0.e0.p.d.m0.i.e):d0.e0.p.d.m0.f.c$b");
        }
    }

    /* compiled from: ProtoBuf.java */
    /* renamed from: d0.e0.p.d.m0.f.c$c  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public enum EnumC0329c implements h.a {
        CLASS(0),
        INTERFACE(1),
        ENUM_CLASS(2),
        ENUM_ENTRY(3),
        ANNOTATION_CLASS(4),
        OBJECT(5),
        COMPANION_OBJECT(6);
        
        private final int value;

        EnumC0329c(int i) {
            this.value = i;
        }

        @Override // d0.e0.p.d.m0.i.h.a
        public final int getNumber() {
            return this.value;
        }
    }

    static {
        c cVar = new c();
        j = cVar;
        cVar.K();
    }

    public c(g.c cVar, d0.e0.p.d.m0.f.a aVar) {
        super(cVar);
        this.supertypeIdMemoizedSerializedSize = -1;
        this.nestedClassNameMemoizedSerializedSize = -1;
        this.sealedSubclassFqNameMemoizedSerializedSize = -1;
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = cVar.getUnknownFields();
    }

    public static c getDefaultInstance() {
        return j;
    }

    public static b newBuilder(c cVar) {
        return newBuilder().mergeFrom(cVar);
    }

    public static c parseFrom(InputStream inputStream, e eVar) throws IOException {
        return (c) ((d0.e0.p.d.m0.i.b) k).parseFrom(inputStream, eVar);
    }

    public final void K() {
        this.flags_ = 6;
        this.fqName_ = 0;
        this.companionObjectName_ = 0;
        this.typeParameter_ = Collections.emptyList();
        this.supertype_ = Collections.emptyList();
        this.supertypeId_ = Collections.emptyList();
        this.nestedClassName_ = Collections.emptyList();
        this.constructor_ = Collections.emptyList();
        this.function_ = Collections.emptyList();
        this.property_ = Collections.emptyList();
        this.typeAlias_ = Collections.emptyList();
        this.enumEntry_ = Collections.emptyList();
        this.sealedSubclassFqName_ = Collections.emptyList();
        this.typeTable_ = t.getDefaultInstance();
        this.versionRequirement_ = Collections.emptyList();
        this.versionRequirementTable_ = w.getDefaultInstance();
    }

    public int getCompanionObjectName() {
        return this.companionObjectName_;
    }

    public d getConstructor(int i) {
        return this.constructor_.get(i);
    }

    public int getConstructorCount() {
        return this.constructor_.size();
    }

    public List<d> getConstructorList() {
        return this.constructor_;
    }

    public g getEnumEntry(int i) {
        return this.enumEntry_.get(i);
    }

    public int getEnumEntryCount() {
        return this.enumEntry_.size();
    }

    public List<g> getEnumEntryList() {
        return this.enumEntry_;
    }

    public int getFlags() {
        return this.flags_;
    }

    public int getFqName() {
        return this.fqName_;
    }

    public i getFunction(int i) {
        return this.function_.get(i);
    }

    public int getFunctionCount() {
        return this.function_.size();
    }

    public List<i> getFunctionList() {
        return this.function_;
    }

    public List<Integer> getNestedClassNameList() {
        return this.nestedClassName_;
    }

    public n getProperty(int i) {
        return this.property_.get(i);
    }

    public int getPropertyCount() {
        return this.property_.size();
    }

    public List<n> getPropertyList() {
        return this.property_;
    }

    public List<Integer> getSealedSubclassFqNameList() {
        return this.sealedSubclassFqName_;
    }

    @Override // d0.e0.p.d.m0.i.n
    public int getSerializedSize() {
        int i = this.memoizedSerializedSize;
        if (i != -1) {
            return i;
        }
        int computeInt32Size = (this.bitField0_ & 1) == 1 ? CodedOutputStream.computeInt32Size(1, this.flags_) + 0 : 0;
        int i2 = 0;
        for (int i3 = 0; i3 < this.supertypeId_.size(); i3++) {
            i2 += CodedOutputStream.computeInt32SizeNoTag(this.supertypeId_.get(i3).intValue());
        }
        int i4 = computeInt32Size + i2;
        if (!getSupertypeIdList().isEmpty()) {
            i4 = i4 + 1 + CodedOutputStream.computeInt32SizeNoTag(i2);
        }
        this.supertypeIdMemoizedSerializedSize = i2;
        if ((this.bitField0_ & 2) == 2) {
            i4 += CodedOutputStream.computeInt32Size(3, this.fqName_);
        }
        if ((this.bitField0_ & 4) == 4) {
            i4 += CodedOutputStream.computeInt32Size(4, this.companionObjectName_);
        }
        for (int i5 = 0; i5 < this.typeParameter_.size(); i5++) {
            i4 += CodedOutputStream.computeMessageSize(5, this.typeParameter_.get(i5));
        }
        for (int i6 = 0; i6 < this.supertype_.size(); i6++) {
            i4 += CodedOutputStream.computeMessageSize(6, this.supertype_.get(i6));
        }
        int i7 = 0;
        for (int i8 = 0; i8 < this.nestedClassName_.size(); i8++) {
            i7 += CodedOutputStream.computeInt32SizeNoTag(this.nestedClassName_.get(i8).intValue());
        }
        int i9 = i4 + i7;
        if (!getNestedClassNameList().isEmpty()) {
            i9 = i9 + 1 + CodedOutputStream.computeInt32SizeNoTag(i7);
        }
        this.nestedClassNameMemoizedSerializedSize = i7;
        for (int i10 = 0; i10 < this.constructor_.size(); i10++) {
            i9 += CodedOutputStream.computeMessageSize(8, this.constructor_.get(i10));
        }
        for (int i11 = 0; i11 < this.function_.size(); i11++) {
            i9 += CodedOutputStream.computeMessageSize(9, this.function_.get(i11));
        }
        for (int i12 = 0; i12 < this.property_.size(); i12++) {
            i9 += CodedOutputStream.computeMessageSize(10, this.property_.get(i12));
        }
        for (int i13 = 0; i13 < this.typeAlias_.size(); i13++) {
            i9 += CodedOutputStream.computeMessageSize(11, this.typeAlias_.get(i13));
        }
        for (int i14 = 0; i14 < this.enumEntry_.size(); i14++) {
            i9 += CodedOutputStream.computeMessageSize(13, this.enumEntry_.get(i14));
        }
        int i15 = 0;
        for (int i16 = 0; i16 < this.sealedSubclassFqName_.size(); i16++) {
            i15 += CodedOutputStream.computeInt32SizeNoTag(this.sealedSubclassFqName_.get(i16).intValue());
        }
        int i17 = i9 + i15;
        if (!getSealedSubclassFqNameList().isEmpty()) {
            i17 = i17 + 2 + CodedOutputStream.computeInt32SizeNoTag(i15);
        }
        this.sealedSubclassFqNameMemoizedSerializedSize = i15;
        if ((this.bitField0_ & 8) == 8) {
            i17 += CodedOutputStream.computeMessageSize(30, this.typeTable_);
        }
        int i18 = 0;
        for (int i19 = 0; i19 < this.versionRequirement_.size(); i19++) {
            i18 += CodedOutputStream.computeInt32SizeNoTag(this.versionRequirement_.get(i19).intValue());
        }
        int size = (getVersionRequirementList().size() * 2) + i17 + i18;
        if ((this.bitField0_ & 16) == 16) {
            size += CodedOutputStream.computeMessageSize(32, this.versionRequirementTable_);
        }
        int size2 = this.unknownFields.size() + c() + size;
        this.memoizedSerializedSize = size2;
        return size2;
    }

    public q getSupertype(int i) {
        return this.supertype_.get(i);
    }

    public int getSupertypeCount() {
        return this.supertype_.size();
    }

    public List<Integer> getSupertypeIdList() {
        return this.supertypeId_;
    }

    public List<q> getSupertypeList() {
        return this.supertype_;
    }

    public r getTypeAlias(int i) {
        return this.typeAlias_.get(i);
    }

    public int getTypeAliasCount() {
        return this.typeAlias_.size();
    }

    public List<r> getTypeAliasList() {
        return this.typeAlias_;
    }

    public s getTypeParameter(int i) {
        return this.typeParameter_.get(i);
    }

    public int getTypeParameterCount() {
        return this.typeParameter_.size();
    }

    public List<s> getTypeParameterList() {
        return this.typeParameter_;
    }

    public t getTypeTable() {
        return this.typeTable_;
    }

    public List<Integer> getVersionRequirementList() {
        return this.versionRequirement_;
    }

    public w getVersionRequirementTable() {
        return this.versionRequirementTable_;
    }

    public boolean hasCompanionObjectName() {
        return (this.bitField0_ & 4) == 4;
    }

    public boolean hasFlags() {
        return (this.bitField0_ & 1) == 1;
    }

    public boolean hasFqName() {
        return (this.bitField0_ & 2) == 2;
    }

    public boolean hasTypeTable() {
        return (this.bitField0_ & 8) == 8;
    }

    public boolean hasVersionRequirementTable() {
        return (this.bitField0_ & 16) == 16;
    }

    @Override // d0.e0.p.d.m0.i.o
    public final boolean isInitialized() {
        byte b2 = this.memoizedIsInitialized;
        if (b2 == 1) {
            return true;
        }
        if (b2 == 0) {
            return false;
        }
        if (!hasFqName()) {
            this.memoizedIsInitialized = (byte) 0;
            return false;
        }
        for (int i = 0; i < getTypeParameterCount(); i++) {
            if (!getTypeParameter(i).isInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
        }
        for (int i2 = 0; i2 < getSupertypeCount(); i2++) {
            if (!getSupertype(i2).isInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
        }
        for (int i3 = 0; i3 < getConstructorCount(); i3++) {
            if (!getConstructor(i3).isInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
        }
        for (int i4 = 0; i4 < getFunctionCount(); i4++) {
            if (!getFunction(i4).isInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
        }
        for (int i5 = 0; i5 < getPropertyCount(); i5++) {
            if (!getProperty(i5).isInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
        }
        for (int i6 = 0; i6 < getTypeAliasCount(); i6++) {
            if (!getTypeAlias(i6).isInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
        }
        for (int i7 = 0; i7 < getEnumEntryCount(); i7++) {
            if (!getEnumEntry(i7).isInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
        }
        if (hasTypeTable() && !getTypeTable().isInitialized()) {
            this.memoizedIsInitialized = (byte) 0;
            return false;
        } else if (!b()) {
            this.memoizedIsInitialized = (byte) 0;
            return false;
        } else {
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }
    }

    @Override // d0.e0.p.d.m0.i.n
    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        getSerializedSize();
        g.d<MessageType>.a e = e();
        if ((this.bitField0_ & 1) == 1) {
            codedOutputStream.writeInt32(1, this.flags_);
        }
        if (getSupertypeIdList().size() > 0) {
            codedOutputStream.writeRawVarint32(18);
            codedOutputStream.writeRawVarint32(this.supertypeIdMemoizedSerializedSize);
        }
        for (int i = 0; i < this.supertypeId_.size(); i++) {
            codedOutputStream.writeInt32NoTag(this.supertypeId_.get(i).intValue());
        }
        if ((this.bitField0_ & 2) == 2) {
            codedOutputStream.writeInt32(3, this.fqName_);
        }
        if ((this.bitField0_ & 4) == 4) {
            codedOutputStream.writeInt32(4, this.companionObjectName_);
        }
        for (int i2 = 0; i2 < this.typeParameter_.size(); i2++) {
            codedOutputStream.writeMessage(5, this.typeParameter_.get(i2));
        }
        for (int i3 = 0; i3 < this.supertype_.size(); i3++) {
            codedOutputStream.writeMessage(6, this.supertype_.get(i3));
        }
        if (getNestedClassNameList().size() > 0) {
            codedOutputStream.writeRawVarint32(58);
            codedOutputStream.writeRawVarint32(this.nestedClassNameMemoizedSerializedSize);
        }
        for (int i4 = 0; i4 < this.nestedClassName_.size(); i4++) {
            codedOutputStream.writeInt32NoTag(this.nestedClassName_.get(i4).intValue());
        }
        for (int i5 = 0; i5 < this.constructor_.size(); i5++) {
            codedOutputStream.writeMessage(8, this.constructor_.get(i5));
        }
        for (int i6 = 0; i6 < this.function_.size(); i6++) {
            codedOutputStream.writeMessage(9, this.function_.get(i6));
        }
        for (int i7 = 0; i7 < this.property_.size(); i7++) {
            codedOutputStream.writeMessage(10, this.property_.get(i7));
        }
        for (int i8 = 0; i8 < this.typeAlias_.size(); i8++) {
            codedOutputStream.writeMessage(11, this.typeAlias_.get(i8));
        }
        for (int i9 = 0; i9 < this.enumEntry_.size(); i9++) {
            codedOutputStream.writeMessage(13, this.enumEntry_.get(i9));
        }
        if (getSealedSubclassFqNameList().size() > 0) {
            codedOutputStream.writeRawVarint32(130);
            codedOutputStream.writeRawVarint32(this.sealedSubclassFqNameMemoizedSerializedSize);
        }
        for (int i10 = 0; i10 < this.sealedSubclassFqName_.size(); i10++) {
            codedOutputStream.writeInt32NoTag(this.sealedSubclassFqName_.get(i10).intValue());
        }
        if ((this.bitField0_ & 8) == 8) {
            codedOutputStream.writeMessage(30, this.typeTable_);
        }
        for (int i11 = 0; i11 < this.versionRequirement_.size(); i11++) {
            codedOutputStream.writeInt32(31, this.versionRequirement_.get(i11).intValue());
        }
        if ((this.bitField0_ & 16) == 16) {
            codedOutputStream.writeMessage(32, this.versionRequirementTable_);
        }
        e.writeUntil(19000, codedOutputStream);
        codedOutputStream.writeRawBytes(this.unknownFields);
    }

    public static b newBuilder() {
        return new b();
    }

    @Override // d0.e0.p.d.m0.i.o
    public c getDefaultInstanceForType() {
        return j;
    }

    @Override // d0.e0.p.d.m0.i.n
    public b newBuilderForType() {
        return newBuilder();
    }

    @Override // d0.e0.p.d.m0.i.n
    public b toBuilder() {
        return newBuilder(this);
    }

    public c() {
        this.supertypeIdMemoizedSerializedSize = -1;
        this.nestedClassNameMemoizedSerializedSize = -1;
        this.sealedSubclassFqNameMemoizedSerializedSize = -1;
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = d0.e0.p.d.m0.i.c.j;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public c(d dVar, e eVar) throws InvalidProtocolBufferException {
        this.supertypeIdMemoizedSerializedSize = -1;
        this.nestedClassNameMemoizedSerializedSize = -1;
        this.sealedSubclassFqNameMemoizedSerializedSize = -1;
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        K();
        c.b newOutput = d0.e0.p.d.m0.i.c.newOutput();
        CodedOutputStream newInstance = CodedOutputStream.newInstance(newOutput, 1);
        boolean z2 = false;
        boolean z3 = false;
        while (!z2) {
            try {
                try {
                    int readTag = dVar.readTag();
                    t.b bVar = null;
                    w.b bVar2 = null;
                    switch (readTag) {
                        case 0:
                            z2 = true;
                            break;
                        case 8:
                            this.bitField0_ |= 1;
                            this.flags_ = dVar.readInt32();
                            break;
                        case 16:
                            if (!(z3 & true)) {
                                this.supertypeId_ = new ArrayList();
                                z3 |= true;
                            }
                            this.supertypeId_.add(Integer.valueOf(dVar.readInt32()));
                            break;
                        case 18:
                            int pushLimit = dVar.pushLimit(dVar.readRawVarint32());
                            if (!(z3 & true) && dVar.getBytesUntilLimit() > 0) {
                                this.supertypeId_ = new ArrayList();
                                z3 |= true;
                            }
                            while (dVar.getBytesUntilLimit() > 0) {
                                this.supertypeId_.add(Integer.valueOf(dVar.readInt32()));
                            }
                            dVar.popLimit(pushLimit);
                            break;
                        case 24:
                            this.bitField0_ |= 2;
                            this.fqName_ = dVar.readInt32();
                            break;
                        case 32:
                            this.bitField0_ |= 4;
                            this.companionObjectName_ = dVar.readInt32();
                            break;
                        case 42:
                            if (!(z3 & true)) {
                                this.typeParameter_ = new ArrayList();
                                z3 |= true;
                            }
                            this.typeParameter_.add(dVar.readMessage(s.k, eVar));
                            break;
                        case 50:
                            if (!(z3 & true)) {
                                this.supertype_ = new ArrayList();
                                z3 |= true;
                            }
                            this.supertype_.add(dVar.readMessage(q.k, eVar));
                            break;
                        case 56:
                            if (!(z3 & true)) {
                                this.nestedClassName_ = new ArrayList();
                                z3 |= true;
                            }
                            this.nestedClassName_.add(Integer.valueOf(dVar.readInt32()));
                            break;
                        case 58:
                            int pushLimit2 = dVar.pushLimit(dVar.readRawVarint32());
                            if (!(z3 & true) && dVar.getBytesUntilLimit() > 0) {
                                this.nestedClassName_ = new ArrayList();
                                z3 |= true;
                            }
                            while (dVar.getBytesUntilLimit() > 0) {
                                this.nestedClassName_.add(Integer.valueOf(dVar.readInt32()));
                            }
                            dVar.popLimit(pushLimit2);
                            break;
                        case 66:
                            if (!(z3 & true)) {
                                this.constructor_ = new ArrayList();
                                z3 |= true;
                            }
                            this.constructor_.add(dVar.readMessage(d.k, eVar));
                            break;
                        case 74:
                            if (!(z3 & true)) {
                                this.function_ = new ArrayList();
                                z3 |= true;
                            }
                            this.function_.add(dVar.readMessage(i.k, eVar));
                            break;
                        case 82:
                            if (!(z3 & true)) {
                                this.property_ = new ArrayList();
                                z3 |= true;
                            }
                            this.property_.add(dVar.readMessage(n.k, eVar));
                            break;
                        case 90:
                            if (!(z3 & true)) {
                                this.typeAlias_ = new ArrayList();
                                z3 |= true;
                            }
                            this.typeAlias_.add(dVar.readMessage(r.k, eVar));
                            break;
                        case 106:
                            if (!(z3 & true)) {
                                this.enumEntry_ = new ArrayList();
                                z3 |= true;
                            }
                            this.enumEntry_.add(dVar.readMessage(g.k, eVar));
                            break;
                        case 128:
                            if (!(z3 & true)) {
                                this.sealedSubclassFqName_ = new ArrayList();
                                z3 |= true;
                            }
                            this.sealedSubclassFqName_.add(Integer.valueOf(dVar.readInt32()));
                            break;
                        case 130:
                            int pushLimit3 = dVar.pushLimit(dVar.readRawVarint32());
                            if (!(z3 & true) && dVar.getBytesUntilLimit() > 0) {
                                this.sealedSubclassFqName_ = new ArrayList();
                                z3 |= true;
                            }
                            while (dVar.getBytesUntilLimit() > 0) {
                                this.sealedSubclassFqName_.add(Integer.valueOf(dVar.readInt32()));
                            }
                            dVar.popLimit(pushLimit3);
                            break;
                        case 242:
                            t.b builder = (this.bitField0_ & 8) == 8 ? this.typeTable_.toBuilder() : bVar;
                            t tVar = (t) dVar.readMessage(t.k, eVar);
                            this.typeTable_ = tVar;
                            if (builder != null) {
                                builder.mergeFrom(tVar);
                                this.typeTable_ = builder.buildPartial();
                            }
                            this.bitField0_ |= 8;
                            break;
                        case 248:
                            if (!(z3 & true)) {
                                this.versionRequirement_ = new ArrayList();
                                z3 |= true;
                            }
                            this.versionRequirement_.add(Integer.valueOf(dVar.readInt32()));
                            break;
                        case 250:
                            int pushLimit4 = dVar.pushLimit(dVar.readRawVarint32());
                            if (!(z3 & true) && dVar.getBytesUntilLimit() > 0) {
                                this.versionRequirement_ = new ArrayList();
                                z3 |= true;
                            }
                            while (dVar.getBytesUntilLimit() > 0) {
                                this.versionRequirement_.add(Integer.valueOf(dVar.readInt32()));
                            }
                            dVar.popLimit(pushLimit4);
                            break;
                        case 258:
                            w.b builder2 = (this.bitField0_ & 16) == 16 ? this.versionRequirementTable_.toBuilder() : bVar2;
                            w wVar = (w) dVar.readMessage(w.k, eVar);
                            this.versionRequirementTable_ = wVar;
                            if (builder2 != null) {
                                builder2.mergeFrom(wVar);
                                this.versionRequirementTable_ = builder2.buildPartial();
                            }
                            this.bitField0_ |= 16;
                            break;
                        default:
                            if (f(dVar, newInstance, eVar, readTag)) {
                                break;
                            }
                            z2 = true;
                            break;
                    }
                } catch (InvalidProtocolBufferException e) {
                    throw e.setUnfinishedMessage(this);
                } catch (IOException e2) {
                    throw new InvalidProtocolBufferException(e2.getMessage()).setUnfinishedMessage(this);
                }
            } catch (Throwable th) {
                if (z3 & true) {
                    this.supertypeId_ = Collections.unmodifiableList(this.supertypeId_);
                }
                if (z3 & true) {
                    this.typeParameter_ = Collections.unmodifiableList(this.typeParameter_);
                }
                if (z3 & true) {
                    this.supertype_ = Collections.unmodifiableList(this.supertype_);
                }
                if (z3 & true) {
                    this.nestedClassName_ = Collections.unmodifiableList(this.nestedClassName_);
                }
                if (z3 & true) {
                    this.constructor_ = Collections.unmodifiableList(this.constructor_);
                }
                if (z3 & true) {
                    this.function_ = Collections.unmodifiableList(this.function_);
                }
                if (z3 & true) {
                    this.property_ = Collections.unmodifiableList(this.property_);
                }
                if (z3 & true) {
                    this.typeAlias_ = Collections.unmodifiableList(this.typeAlias_);
                }
                if (z3 & true) {
                    this.enumEntry_ = Collections.unmodifiableList(this.enumEntry_);
                }
                if (z3 & true) {
                    this.sealedSubclassFqName_ = Collections.unmodifiableList(this.sealedSubclassFqName_);
                }
                if (z3 & true) {
                    this.versionRequirement_ = Collections.unmodifiableList(this.versionRequirement_);
                }
                try {
                    newInstance.flush();
                } catch (IOException unused) {
                } catch (Throwable th2) {
                    this.unknownFields = newOutput.toByteString();
                    throw th2;
                }
                this.unknownFields = newOutput.toByteString();
                d();
                throw th;
            }
        }
        if (z3 & true) {
            this.supertypeId_ = Collections.unmodifiableList(this.supertypeId_);
        }
        if (z3 & true) {
            this.typeParameter_ = Collections.unmodifiableList(this.typeParameter_);
        }
        if (z3 & true) {
            this.supertype_ = Collections.unmodifiableList(this.supertype_);
        }
        if (z3 & true) {
            this.nestedClassName_ = Collections.unmodifiableList(this.nestedClassName_);
        }
        if (z3 & true) {
            this.constructor_ = Collections.unmodifiableList(this.constructor_);
        }
        if (z3 & true) {
            this.function_ = Collections.unmodifiableList(this.function_);
        }
        if (z3 & true) {
            this.property_ = Collections.unmodifiableList(this.property_);
        }
        if (z3 & true) {
            this.typeAlias_ = Collections.unmodifiableList(this.typeAlias_);
        }
        if (z3 & true) {
            this.enumEntry_ = Collections.unmodifiableList(this.enumEntry_);
        }
        if (z3 & true) {
            this.sealedSubclassFqName_ = Collections.unmodifiableList(this.sealedSubclassFqName_);
        }
        if (z3 & true) {
            this.versionRequirement_ = Collections.unmodifiableList(this.versionRequirement_);
        }
        try {
            newInstance.flush();
        } catch (IOException unused2) {
        } catch (Throwable th3) {
            this.unknownFields = newOutput.toByteString();
            throw th3;
        }
        this.unknownFields = newOutput.toByteString();
        d();
    }
}
