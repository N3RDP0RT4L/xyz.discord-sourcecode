package d0.e0.p.d.m0.f.a0.b;

import andhook.lib.HookHelper;
import d0.e0.p.d.m0.f.a0.a;
import d0.e0.p.d.m0.f.a0.b.e;
import d0.e0.p.d.m0.f.d;
import d0.e0.p.d.m0.f.i;
import d0.e0.p.d.m0.f.l;
import d0.e0.p.d.m0.f.n;
import d0.e0.p.d.m0.f.q;
import d0.e0.p.d.m0.f.u;
import d0.e0.p.d.m0.f.z.b;
import d0.e0.p.d.m0.f.z.c;
import d0.e0.p.d.m0.f.z.f;
import d0.e0.p.d.m0.f.z.g;
import d0.e0.p.d.m0.i.e;
import d0.e0.p.d.m0.i.g;
import d0.t.o;
import d0.z.d.m;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Pair;
/* compiled from: JvmProtoBufUtil.kt */
/* loaded from: classes3.dex */
public final class h {
    public static final h a = new h();

    /* renamed from: b  reason: collision with root package name */
    public static final e f3362b;

    static {
        e newInstance = e.newInstance();
        a.registerAllExtensions(newInstance);
        m.checkNotNullExpressionValue(newInstance, "newInstance().apply(JvmProtoBuf::registerAllExtensions)");
        f3362b = newInstance;
    }

    public static /* synthetic */ e.a getJvmFieldSignature$default(h hVar, n nVar, c cVar, g gVar, boolean z2, int i, Object obj) {
        if ((i & 8) != 0) {
            z2 = true;
        }
        return hVar.getJvmFieldSignature(nVar, cVar, gVar, z2);
    }

    public static final boolean isMovedFromInterfaceCompanion(n nVar) {
        m.checkNotNullParameter(nVar, "proto");
        b.C0334b is_moved_from_interface_companion = d.a.getIS_MOVED_FROM_INTERFACE_COMPANION();
        Object extension = nVar.getExtension(a.e);
        m.checkNotNullExpressionValue(extension, "proto.getExtension(JvmProtoBuf.flags)");
        Boolean bool = is_moved_from_interface_companion.get(((Number) extension).intValue());
        m.checkNotNullExpressionValue(bool, "JvmFlags.IS_MOVED_FROM_INTERFACE_COMPANION.get(proto.getExtension(JvmProtoBuf.flags))");
        return bool.booleanValue();
    }

    public static final Pair<g, d0.e0.p.d.m0.f.c> readClassDataFrom(String[] strArr, String[] strArr2) {
        m.checkNotNullParameter(strArr, "data");
        m.checkNotNullParameter(strArr2, "strings");
        byte[] decodeBytes = a.decodeBytes(strArr);
        m.checkNotNullExpressionValue(decodeBytes, "decodeBytes(data)");
        return readClassDataFrom(decodeBytes, strArr2);
    }

    public static final Pair<g, i> readFunctionDataFrom(String[] strArr, String[] strArr2) {
        m.checkNotNullParameter(strArr, "data");
        m.checkNotNullParameter(strArr2, "strings");
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(a.decodeBytes(strArr));
        a.e parseDelimitedFrom = a.e.parseDelimitedFrom(byteArrayInputStream, f3362b);
        m.checkNotNullExpressionValue(parseDelimitedFrom, "parseDelimitedFrom(this, EXTENSION_REGISTRY)");
        return new Pair<>(new g(parseDelimitedFrom, strArr2), i.parseFrom(byteArrayInputStream, f3362b));
    }

    public static final Pair<g, l> readPackageDataFrom(String[] strArr, String[] strArr2) {
        m.checkNotNullParameter(strArr, "data");
        m.checkNotNullParameter(strArr2, "strings");
        byte[] decodeBytes = a.decodeBytes(strArr);
        m.checkNotNullExpressionValue(decodeBytes, "decodeBytes(data)");
        return readPackageDataFrom(decodeBytes, strArr2);
    }

    public final String a(q qVar, c cVar) {
        if (!qVar.hasClassName()) {
            return null;
        }
        b bVar = b.a;
        return b.mapClass(cVar.getQualifiedClassName(qVar.getClassName()));
    }

    public final d0.e0.p.d.m0.i.e getEXTENSION_REGISTRY() {
        return f3362b;
    }

    public final e.b getJvmConstructorSignature(d dVar, c cVar, g gVar) {
        String str;
        m.checkNotNullParameter(dVar, "proto");
        m.checkNotNullParameter(cVar, "nameResolver");
        m.checkNotNullParameter(gVar, "typeTable");
        g.f<d, a.c> fVar = a.a;
        m.checkNotNullExpressionValue(fVar, "constructorSignature");
        a.c cVar2 = (a.c) d0.e0.p.d.m0.f.z.e.getExtensionOrNull(dVar, fVar);
        String string = (cVar2 == null || !cVar2.hasName()) ? HookHelper.constructorName : cVar.getString(cVar2.getName());
        if (cVar2 == null || !cVar2.hasDesc()) {
            List<u> valueParameterList = dVar.getValueParameterList();
            m.checkNotNullExpressionValue(valueParameterList, "proto.valueParameterList");
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(valueParameterList, 10));
            for (u uVar : valueParameterList) {
                m.checkNotNullExpressionValue(uVar, "it");
                String a2 = a(f.type(uVar, gVar), cVar);
                if (a2 == null) {
                    return null;
                }
                arrayList.add(a2);
            }
            str = d0.t.u.joinToString$default(arrayList, "", "(", ")V", 0, null, null, 56, null);
        } else {
            str = cVar.getString(cVar2.getDesc());
        }
        return new e.b(string, str);
    }

    public final e.a getJvmFieldSignature(n nVar, c cVar, d0.e0.p.d.m0.f.z.g gVar, boolean z2) {
        String str;
        m.checkNotNullParameter(nVar, "proto");
        m.checkNotNullParameter(cVar, "nameResolver");
        m.checkNotNullParameter(gVar, "typeTable");
        g.f<n, a.d> fVar = a.d;
        m.checkNotNullExpressionValue(fVar, "propertySignature");
        a.d dVar = (a.d) d0.e0.p.d.m0.f.z.e.getExtensionOrNull(nVar, fVar);
        if (dVar == null) {
            return null;
        }
        a.b field = dVar.hasField() ? dVar.getField() : null;
        if (field == null && z2) {
            return null;
        }
        int name = (field == null || !field.hasName()) ? nVar.getName() : field.getName();
        if (field == null || !field.hasDesc()) {
            str = a(f.returnType(nVar, gVar), cVar);
            if (str == null) {
                return null;
            }
        } else {
            str = cVar.getString(field.getDesc());
        }
        return new e.a(cVar.getString(name), str);
    }

    public final e.b getJvmMethodSignature(i iVar, c cVar, d0.e0.p.d.m0.f.z.g gVar) {
        String str;
        m.checkNotNullParameter(iVar, "proto");
        m.checkNotNullParameter(cVar, "nameResolver");
        m.checkNotNullParameter(gVar, "typeTable");
        g.f<i, a.c> fVar = a.f3356b;
        m.checkNotNullExpressionValue(fVar, "methodSignature");
        a.c cVar2 = (a.c) d0.e0.p.d.m0.f.z.e.getExtensionOrNull(iVar, fVar);
        int name = (cVar2 == null || !cVar2.hasName()) ? iVar.getName() : cVar2.getName();
        if (cVar2 == null || !cVar2.hasDesc()) {
            List listOfNotNull = d0.t.n.listOfNotNull(f.receiverType(iVar, gVar));
            List<u> valueParameterList = iVar.getValueParameterList();
            m.checkNotNullExpressionValue(valueParameterList, "proto.valueParameterList");
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(valueParameterList, 10));
            for (u uVar : valueParameterList) {
                m.checkNotNullExpressionValue(uVar, "it");
                arrayList.add(f.type(uVar, gVar));
            }
            List<q> plus = d0.t.u.plus((Collection) listOfNotNull, (Iterable) arrayList);
            ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(plus, 10));
            for (q qVar : plus) {
                String a2 = a(qVar, cVar);
                if (a2 == null) {
                    return null;
                }
                arrayList2.add(a2);
            }
            String a3 = a(f.returnType(iVar, gVar), cVar);
            if (a3 == null) {
                return null;
            }
            str = m.stringPlus(d0.t.u.joinToString$default(arrayList2, "", "(", ")", 0, null, null, 56, null), a3);
        } else {
            str = cVar.getString(cVar2.getDesc());
        }
        return new e.b(cVar.getString(name), str);
    }

    public static final Pair<g, d0.e0.p.d.m0.f.c> readClassDataFrom(byte[] bArr, String[] strArr) {
        m.checkNotNullParameter(bArr, "bytes");
        m.checkNotNullParameter(strArr, "strings");
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        a.e parseDelimitedFrom = a.e.parseDelimitedFrom(byteArrayInputStream, f3362b);
        m.checkNotNullExpressionValue(parseDelimitedFrom, "parseDelimitedFrom(this, EXTENSION_REGISTRY)");
        return new Pair<>(new g(parseDelimitedFrom, strArr), d0.e0.p.d.m0.f.c.parseFrom(byteArrayInputStream, f3362b));
    }

    public static final Pair<g, l> readPackageDataFrom(byte[] bArr, String[] strArr) {
        m.checkNotNullParameter(bArr, "bytes");
        m.checkNotNullParameter(strArr, "strings");
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        a.e parseDelimitedFrom = a.e.parseDelimitedFrom(byteArrayInputStream, f3362b);
        m.checkNotNullExpressionValue(parseDelimitedFrom, "parseDelimitedFrom(this, EXTENSION_REGISTRY)");
        return new Pair<>(new g(parseDelimitedFrom, strArr), l.parseFrom(byteArrayInputStream, f3362b));
    }
}
