package d0.e0.p.d.m0.f.a0.b;

import d0.z.d.m;
/* compiled from: utfEncoding.kt */
/* loaded from: classes3.dex */
public final class i {
    public static final byte[] stringsToBytes(String[] strArr) {
        int i;
        m.checkNotNullParameter(strArr, "strings");
        int i2 = 0;
        for (String str : strArr) {
            i2 += str.length();
        }
        byte[] bArr = new byte[i2];
        int length = strArr.length;
        int i3 = 0;
        int i4 = 0;
        while (i3 < length) {
            String str2 = strArr[i3];
            i3++;
            int length2 = str2.length() - 1;
            if (length2 >= 0) {
                int i5 = 0;
                while (true) {
                    i5++;
                    i = i4 + 1;
                    bArr[i4] = (byte) str2.charAt(i5);
                    if (i5 == length2) {
                        break;
                    }
                    i4 = i;
                }
                i4 = i;
            }
        }
        return bArr;
    }
}
