package d0.e0.p.d.m0.f.a0.b;

import andhook.lib.xposed.ClassUtils;
import d0.d0.f;
import d0.e0.p.d.m0.f.a0.a;
import d0.e0.p.d.m0.f.z.c;
import d0.g0.t;
import d0.t.g0;
import d0.t.n;
import d0.t.n0;
import d0.t.o;
import d0.t.u;
import d0.t.z;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: JvmNameResolver.kt */
/* loaded from: classes3.dex */
public final class g implements c {
    public static final a a;

    /* renamed from: b  reason: collision with root package name */
    public static final String f3361b;
    public static final List<String> c;
    public final a.e d;
    public final String[] e;
    public final Set<Integer> f;
    public final List<a.e.c> g;

    /* compiled from: JvmNameResolver.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final List<String> getPREDEFINED_STRINGS() {
            return g.c;
        }
    }

    static {
        a aVar = new a(null);
        a = aVar;
        String joinToString$default = u.joinToString$default(n.listOf((Object[]) new Character[]{'k', 'o', 't', 'l', 'i', 'n'}), "", null, null, 0, null, null, 62, null);
        f3361b = joinToString$default;
        c = n.listOf((Object[]) new String[]{m.stringPlus(joinToString$default, "/Any"), m.stringPlus(joinToString$default, "/Nothing"), m.stringPlus(joinToString$default, "/Unit"), m.stringPlus(joinToString$default, "/Throwable"), m.stringPlus(joinToString$default, "/Number"), m.stringPlus(joinToString$default, "/Byte"), m.stringPlus(joinToString$default, "/Double"), m.stringPlus(joinToString$default, "/Float"), m.stringPlus(joinToString$default, "/Int"), m.stringPlus(joinToString$default, "/Long"), m.stringPlus(joinToString$default, "/Short"), m.stringPlus(joinToString$default, "/Boolean"), m.stringPlus(joinToString$default, "/Char"), m.stringPlus(joinToString$default, "/CharSequence"), m.stringPlus(joinToString$default, "/String"), m.stringPlus(joinToString$default, "/Comparable"), m.stringPlus(joinToString$default, "/Enum"), m.stringPlus(joinToString$default, "/Array"), m.stringPlus(joinToString$default, "/ByteArray"), m.stringPlus(joinToString$default, "/DoubleArray"), m.stringPlus(joinToString$default, "/FloatArray"), m.stringPlus(joinToString$default, "/IntArray"), m.stringPlus(joinToString$default, "/LongArray"), m.stringPlus(joinToString$default, "/ShortArray"), m.stringPlus(joinToString$default, "/BooleanArray"), m.stringPlus(joinToString$default, "/CharArray"), m.stringPlus(joinToString$default, "/Cloneable"), m.stringPlus(joinToString$default, "/Annotation"), m.stringPlus(joinToString$default, "/collections/Iterable"), m.stringPlus(joinToString$default, "/collections/MutableIterable"), m.stringPlus(joinToString$default, "/collections/Collection"), m.stringPlus(joinToString$default, "/collections/MutableCollection"), m.stringPlus(joinToString$default, "/collections/List"), m.stringPlus(joinToString$default, "/collections/MutableList"), m.stringPlus(joinToString$default, "/collections/Set"), m.stringPlus(joinToString$default, "/collections/MutableSet"), m.stringPlus(joinToString$default, "/collections/Map"), m.stringPlus(joinToString$default, "/collections/MutableMap"), m.stringPlus(joinToString$default, "/collections/Map.Entry"), m.stringPlus(joinToString$default, "/collections/MutableMap.MutableEntry"), m.stringPlus(joinToString$default, "/collections/Iterator"), m.stringPlus(joinToString$default, "/collections/MutableIterator"), m.stringPlus(joinToString$default, "/collections/ListIterator"), m.stringPlus(joinToString$default, "/collections/MutableListIterator")});
        Iterable<z> withIndex = u.withIndex(aVar.getPREDEFINED_STRINGS());
        LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(o.collectionSizeOrDefault(withIndex, 10)), 16));
        for (z zVar : withIndex) {
            linkedHashMap.put((String) zVar.getValue(), Integer.valueOf(zVar.getIndex()));
        }
    }

    public g(a.e eVar, String[] strArr) {
        Set<Integer> set;
        m.checkNotNullParameter(eVar, "types");
        m.checkNotNullParameter(strArr, "strings");
        this.d = eVar;
        this.e = strArr;
        List<Integer> localNameList = eVar.getLocalNameList();
        if (localNameList.isEmpty()) {
            set = n0.emptySet();
        } else {
            m.checkNotNullExpressionValue(localNameList, "");
            set = u.toSet(localNameList);
        }
        this.f = set;
        ArrayList arrayList = new ArrayList();
        List<a.e.c> recordList = getTypes().getRecordList();
        arrayList.ensureCapacity(recordList.size());
        for (a.e.c cVar : recordList) {
            int range = cVar.getRange();
            for (int i = 0; i < range; i++) {
                arrayList.add(cVar);
            }
        }
        arrayList.trimToSize();
        this.g = arrayList;
    }

    @Override // d0.e0.p.d.m0.f.z.c
    public String getQualifiedClassName(int i) {
        return getString(i);
    }

    @Override // d0.e0.p.d.m0.f.z.c
    public String getString(int i) {
        String str;
        a.e.c cVar = this.g.get(i);
        if (cVar.hasString()) {
            str = cVar.getString();
        } else {
            if (cVar.hasPredefinedIndex()) {
                a aVar = a;
                int size = aVar.getPREDEFINED_STRINGS().size() - 1;
                int predefinedIndex = cVar.getPredefinedIndex();
                if (predefinedIndex >= 0 && predefinedIndex <= size) {
                    str = aVar.getPREDEFINED_STRINGS().get(cVar.getPredefinedIndex());
                }
            }
            str = this.e[i];
        }
        if (cVar.getSubstringIndexCount() >= 2) {
            List<Integer> substringIndexList = cVar.getSubstringIndexList();
            m.checkNotNullExpressionValue(substringIndexList, "substringIndexList");
            Integer num = substringIndexList.get(0);
            Integer num2 = substringIndexList.get(1);
            m.checkNotNullExpressionValue(num, "begin");
            if (num.intValue() >= 0) {
                int intValue = num.intValue();
                m.checkNotNullExpressionValue(num2, "end");
                if (intValue <= num2.intValue() && num2.intValue() <= str.length()) {
                    m.checkNotNullExpressionValue(str, "string");
                    str = str.substring(num.intValue(), num2.intValue());
                    m.checkNotNullExpressionValue(str, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                }
            }
        }
        String str2 = str;
        if (cVar.getReplaceCharCount() >= 2) {
            List<Integer> replaceCharList = cVar.getReplaceCharList();
            m.checkNotNullExpressionValue(replaceCharList, "replaceCharList");
            m.checkNotNullExpressionValue(str2, "string");
            str2 = t.replace$default(str2, (char) replaceCharList.get(0).intValue(), (char) replaceCharList.get(1).intValue(), false, 4, (Object) null);
        }
        String str3 = str2;
        a.e.c.EnumC0324c operation = cVar.getOperation();
        if (operation == null) {
            operation = a.e.c.EnumC0324c.NONE;
        }
        int ordinal = operation.ordinal();
        if (ordinal == 1) {
            m.checkNotNullExpressionValue(str3, "string");
            str3 = t.replace$default(str3, (char) ClassUtils.INNER_CLASS_SEPARATOR_CHAR, (char) ClassUtils.PACKAGE_SEPARATOR_CHAR, false, 4, (Object) null);
        } else if (ordinal == 2) {
            if (str3.length() >= 2) {
                m.checkNotNullExpressionValue(str3, "string");
                str3 = str3.substring(1, str3.length() - 1);
                m.checkNotNullExpressionValue(str3, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            }
            String str4 = str3;
            m.checkNotNullExpressionValue(str4, "string");
            str3 = t.replace$default(str4, (char) ClassUtils.INNER_CLASS_SEPARATOR_CHAR, (char) ClassUtils.PACKAGE_SEPARATOR_CHAR, false, 4, (Object) null);
        }
        m.checkNotNullExpressionValue(str3, "string");
        return str3;
    }

    public final a.e getTypes() {
        return this.d;
    }

    @Override // d0.e0.p.d.m0.f.z.c
    public boolean isLocalClassName(int i) {
        return this.f.contains(Integer.valueOf(i));
    }
}
