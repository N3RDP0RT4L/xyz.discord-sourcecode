package d0.e0.p.d.m0.f.a0;

import d0.e0.p.d.m0.f.i;
import d0.e0.p.d.m0.f.l;
import d0.e0.p.d.m0.f.n;
import d0.e0.p.d.m0.f.q;
import d0.e0.p.d.m0.f.s;
import d0.e0.p.d.m0.i.c;
import d0.e0.p.d.m0.i.g;
import d0.e0.p.d.m0.i.h;
import d0.e0.p.d.m0.i.o;
import d0.e0.p.d.m0.i.p;
import d0.e0.p.d.m0.i.w;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import kotlin.reflect.jvm.internal.impl.protobuf.CodedOutputStream;
import kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException;
import kotlin.reflect.jvm.internal.impl.protobuf.UninitializedMessageException;
/* compiled from: JvmProtoBuf.java */
/* loaded from: classes3.dex */
public final class a {
    public static final g.f<d0.e0.p.d.m0.f.d, c> a;

    /* renamed from: b  reason: collision with root package name */
    public static final g.f<i, c> f3356b;
    public static final g.f<i, Integer> c;
    public static final g.f<n, d> d;
    public static final g.f<n, Integer> e;
    public static final g.f<q, List<d0.e0.p.d.m0.f.b>> f;
    public static final g.f<q, Boolean> g = g.newSingularGeneratedExtension(q.getDefaultInstance(), Boolean.FALSE, null, null, 101, w.b.q, Boolean.class);
    public static final g.f<s, List<d0.e0.p.d.m0.f.b>> h;
    public static final g.f<d0.e0.p.d.m0.f.c, Integer> i;
    public static final g.f<d0.e0.p.d.m0.f.c, List<n>> j;
    public static final g.f<d0.e0.p.d.m0.f.c, Integer> k;
    public static final g.f<d0.e0.p.d.m0.f.c, Integer> l;
    public static final g.f<l, Integer> m;
    public static final g.f<l, List<n>> n;

    /* compiled from: JvmProtoBuf.java */
    /* loaded from: classes3.dex */
    public static final class b extends g implements o {
        public static final b j;
        public static p<b> k = new C0318a();
        private int bitField0_;
        private int desc_;
        private byte memoizedIsInitialized;
        private int memoizedSerializedSize;
        private int name_;
        private final d0.e0.p.d.m0.i.c unknownFields;

        /* compiled from: JvmProtoBuf.java */
        /* renamed from: d0.e0.p.d.m0.f.a0.a$b$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static class C0318a extends d0.e0.p.d.m0.i.b<b> {
            @Override // d0.e0.p.d.m0.i.p
            public b parsePartialFrom(d0.e0.p.d.m0.i.d dVar, d0.e0.p.d.m0.i.e eVar) throws InvalidProtocolBufferException {
                return new b(dVar, eVar, null);
            }
        }

        /* compiled from: JvmProtoBuf.java */
        /* renamed from: d0.e0.p.d.m0.f.a0.a$b$b  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0319b extends g.b<b, C0319b> implements o {
            public int k;
            public int l;
            public int m;

            public b buildPartial() {
                b bVar = new b(this, null);
                int i = this.k;
                int i2 = 1;
                if ((i & 1) != 1) {
                    i2 = 0;
                }
                bVar.name_ = this.l;
                if ((i & 2) == 2) {
                    i2 |= 2;
                }
                bVar.desc_ = this.m;
                bVar.bitField0_ = i2;
                return bVar;
            }

            public C0319b setDesc(int i) {
                this.k |= 2;
                this.m = i;
                return this;
            }

            public C0319b setName(int i) {
                this.k |= 1;
                this.l = i;
                return this;
            }

            @Override // d0.e0.p.d.m0.i.n.a
            public b build() {
                b buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw new UninitializedMessageException(buildPartial);
            }

            /* JADX WARN: Can't rename method to resolve collision */
            @Override // d0.e0.p.d.m0.i.g.b
            public C0319b clone() {
                return new C0319b().mergeFrom(buildPartial());
            }

            public C0319b mergeFrom(b bVar) {
                if (bVar == b.getDefaultInstance()) {
                    return this;
                }
                if (bVar.hasName()) {
                    setName(bVar.getName());
                }
                if (bVar.hasDesc()) {
                    setDesc(bVar.getDesc());
                }
                setUnknownFields(getUnknownFields().concat(bVar.unknownFields));
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:14:0x001d  */
            @Override // d0.e0.p.d.m0.i.a.AbstractC0335a, d0.e0.p.d.m0.i.n.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public d0.e0.p.d.m0.f.a0.a.b.C0319b mergeFrom(d0.e0.p.d.m0.i.d r3, d0.e0.p.d.m0.i.e r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    d0.e0.p.d.m0.i.p<d0.e0.p.d.m0.f.a0.a$b> r1 = d0.e0.p.d.m0.f.a0.a.b.k     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                    d0.e0.p.d.m0.f.a0.a$b r3 = (d0.e0.p.d.m0.f.a0.a.b) r3     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                    if (r3 == 0) goto Le
                    r2.mergeFrom(r3)
                Le:
                    return r2
                Lf:
                    r3 = move-exception
                    goto L1b
                L11:
                    r3 = move-exception
                    d0.e0.p.d.m0.i.n r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                    d0.e0.p.d.m0.f.a0.a$b r4 = (d0.e0.p.d.m0.f.a0.a.b) r4     // Catch: java.lang.Throwable -> Lf
                    throw r3     // Catch: java.lang.Throwable -> L19
                L19:
                    r3 = move-exception
                    r0 = r4
                L1b:
                    if (r0 == 0) goto L20
                    r2.mergeFrom(r0)
                L20:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.f.a0.a.b.C0319b.mergeFrom(d0.e0.p.d.m0.i.d, d0.e0.p.d.m0.i.e):d0.e0.p.d.m0.f.a0.a$b$b");
            }
        }

        static {
            b bVar = new b();
            j = bVar;
            bVar.name_ = 0;
            bVar.desc_ = 0;
        }

        public b(g.b bVar, C0317a aVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
            this.memoizedSerializedSize = -1;
            this.unknownFields = bVar.getUnknownFields();
        }

        public static b getDefaultInstance() {
            return j;
        }

        public static C0319b newBuilder(b bVar) {
            return newBuilder().mergeFrom(bVar);
        }

        public int getDesc() {
            return this.desc_;
        }

        public int getName() {
            return this.name_;
        }

        @Override // d0.e0.p.d.m0.i.n
        public int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if ((this.bitField0_ & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeInt32Size(1, this.name_);
            }
            if ((this.bitField0_ & 2) == 2) {
                i2 += CodedOutputStream.computeInt32Size(2, this.desc_);
            }
            int size = this.unknownFields.size() + i2;
            this.memoizedSerializedSize = size;
            return size;
        }

        public boolean hasDesc() {
            return (this.bitField0_ & 2) == 2;
        }

        public boolean hasName() {
            return (this.bitField0_ & 1) == 1;
        }

        @Override // d0.e0.p.d.m0.i.o
        public final boolean isInitialized() {
            byte b2 = this.memoizedIsInitialized;
            if (b2 == 1) {
                return true;
            }
            if (b2 == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // d0.e0.p.d.m0.i.n
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            getSerializedSize();
            if ((this.bitField0_ & 1) == 1) {
                codedOutputStream.writeInt32(1, this.name_);
            }
            if ((this.bitField0_ & 2) == 2) {
                codedOutputStream.writeInt32(2, this.desc_);
            }
            codedOutputStream.writeRawBytes(this.unknownFields);
        }

        public static C0319b newBuilder() {
            return new C0319b();
        }

        @Override // d0.e0.p.d.m0.i.n
        public C0319b newBuilderForType() {
            return newBuilder();
        }

        @Override // d0.e0.p.d.m0.i.n
        public C0319b toBuilder() {
            return newBuilder(this);
        }

        public b() {
            this.memoizedIsInitialized = (byte) -1;
            this.memoizedSerializedSize = -1;
            this.unknownFields = d0.e0.p.d.m0.i.c.j;
        }

        public b(d0.e0.p.d.m0.i.d dVar, d0.e0.p.d.m0.i.e eVar, C0317a aVar) throws InvalidProtocolBufferException {
            this.memoizedIsInitialized = (byte) -1;
            this.memoizedSerializedSize = -1;
            boolean z2 = false;
            this.name_ = 0;
            this.desc_ = 0;
            c.b newOutput = d0.e0.p.d.m0.i.c.newOutput();
            CodedOutputStream newInstance = CodedOutputStream.newInstance(newOutput, 1);
            while (!z2) {
                try {
                    try {
                        int readTag = dVar.readTag();
                        if (readTag != 0) {
                            if (readTag == 8) {
                                this.bitField0_ |= 1;
                                this.name_ = dVar.readInt32();
                            } else if (readTag == 16) {
                                this.bitField0_ |= 2;
                                this.desc_ = dVar.readInt32();
                            } else if (!dVar.skipField(readTag, newInstance)) {
                            }
                        }
                        z2 = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2.getMessage()).setUnfinishedMessage(this);
                    }
                } catch (Throwable th) {
                    try {
                        newInstance.flush();
                    } catch (IOException unused) {
                    } catch (Throwable th2) {
                        this.unknownFields = newOutput.toByteString();
                        throw th2;
                    }
                    this.unknownFields = newOutput.toByteString();
                    throw th;
                }
            }
            try {
                newInstance.flush();
            } catch (IOException unused2) {
            } catch (Throwable th3) {
                this.unknownFields = newOutput.toByteString();
                throw th3;
            }
            this.unknownFields = newOutput.toByteString();
        }
    }

    /* compiled from: JvmProtoBuf.java */
    /* loaded from: classes3.dex */
    public static final class c extends g implements o {
        public static final c j;
        public static p<c> k = new C0320a();
        private int bitField0_;
        private int desc_;
        private byte memoizedIsInitialized;
        private int memoizedSerializedSize;
        private int name_;
        private final d0.e0.p.d.m0.i.c unknownFields;

        /* compiled from: JvmProtoBuf.java */
        /* renamed from: d0.e0.p.d.m0.f.a0.a$c$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static class C0320a extends d0.e0.p.d.m0.i.b<c> {
            @Override // d0.e0.p.d.m0.i.p
            public c parsePartialFrom(d0.e0.p.d.m0.i.d dVar, d0.e0.p.d.m0.i.e eVar) throws InvalidProtocolBufferException {
                return new c(dVar, eVar, null);
            }
        }

        /* compiled from: JvmProtoBuf.java */
        /* loaded from: classes3.dex */
        public static final class b extends g.b<c, b> implements o {
            public int k;
            public int l;
            public int m;

            public c buildPartial() {
                c cVar = new c(this, null);
                int i = this.k;
                int i2 = 1;
                if ((i & 1) != 1) {
                    i2 = 0;
                }
                cVar.name_ = this.l;
                if ((i & 2) == 2) {
                    i2 |= 2;
                }
                cVar.desc_ = this.m;
                cVar.bitField0_ = i2;
                return cVar;
            }

            public b setDesc(int i) {
                this.k |= 2;
                this.m = i;
                return this;
            }

            public b setName(int i) {
                this.k |= 1;
                this.l = i;
                return this;
            }

            @Override // d0.e0.p.d.m0.i.n.a
            public c build() {
                c buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw new UninitializedMessageException(buildPartial);
            }

            /* JADX WARN: Can't rename method to resolve collision */
            @Override // d0.e0.p.d.m0.i.g.b
            public b clone() {
                return new b().mergeFrom(buildPartial());
            }

            public b mergeFrom(c cVar) {
                if (cVar == c.getDefaultInstance()) {
                    return this;
                }
                if (cVar.hasName()) {
                    setName(cVar.getName());
                }
                if (cVar.hasDesc()) {
                    setDesc(cVar.getDesc());
                }
                setUnknownFields(getUnknownFields().concat(cVar.unknownFields));
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:14:0x001d  */
            @Override // d0.e0.p.d.m0.i.a.AbstractC0335a, d0.e0.p.d.m0.i.n.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public d0.e0.p.d.m0.f.a0.a.c.b mergeFrom(d0.e0.p.d.m0.i.d r3, d0.e0.p.d.m0.i.e r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    d0.e0.p.d.m0.i.p<d0.e0.p.d.m0.f.a0.a$c> r1 = d0.e0.p.d.m0.f.a0.a.c.k     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                    d0.e0.p.d.m0.f.a0.a$c r3 = (d0.e0.p.d.m0.f.a0.a.c) r3     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                    if (r3 == 0) goto Le
                    r2.mergeFrom(r3)
                Le:
                    return r2
                Lf:
                    r3 = move-exception
                    goto L1b
                L11:
                    r3 = move-exception
                    d0.e0.p.d.m0.i.n r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                    d0.e0.p.d.m0.f.a0.a$c r4 = (d0.e0.p.d.m0.f.a0.a.c) r4     // Catch: java.lang.Throwable -> Lf
                    throw r3     // Catch: java.lang.Throwable -> L19
                L19:
                    r3 = move-exception
                    r0 = r4
                L1b:
                    if (r0 == 0) goto L20
                    r2.mergeFrom(r0)
                L20:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.f.a0.a.c.b.mergeFrom(d0.e0.p.d.m0.i.d, d0.e0.p.d.m0.i.e):d0.e0.p.d.m0.f.a0.a$c$b");
            }
        }

        static {
            c cVar = new c();
            j = cVar;
            cVar.name_ = 0;
            cVar.desc_ = 0;
        }

        public c(g.b bVar, C0317a aVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
            this.memoizedSerializedSize = -1;
            this.unknownFields = bVar.getUnknownFields();
        }

        public static c getDefaultInstance() {
            return j;
        }

        public static b newBuilder(c cVar) {
            return newBuilder().mergeFrom(cVar);
        }

        public int getDesc() {
            return this.desc_;
        }

        public int getName() {
            return this.name_;
        }

        @Override // d0.e0.p.d.m0.i.n
        public int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if ((this.bitField0_ & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeInt32Size(1, this.name_);
            }
            if ((this.bitField0_ & 2) == 2) {
                i2 += CodedOutputStream.computeInt32Size(2, this.desc_);
            }
            int size = this.unknownFields.size() + i2;
            this.memoizedSerializedSize = size;
            return size;
        }

        public boolean hasDesc() {
            return (this.bitField0_ & 2) == 2;
        }

        public boolean hasName() {
            return (this.bitField0_ & 1) == 1;
        }

        @Override // d0.e0.p.d.m0.i.o
        public final boolean isInitialized() {
            byte b2 = this.memoizedIsInitialized;
            if (b2 == 1) {
                return true;
            }
            if (b2 == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // d0.e0.p.d.m0.i.n
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            getSerializedSize();
            if ((this.bitField0_ & 1) == 1) {
                codedOutputStream.writeInt32(1, this.name_);
            }
            if ((this.bitField0_ & 2) == 2) {
                codedOutputStream.writeInt32(2, this.desc_);
            }
            codedOutputStream.writeRawBytes(this.unknownFields);
        }

        public static b newBuilder() {
            return new b();
        }

        @Override // d0.e0.p.d.m0.i.n
        public b newBuilderForType() {
            return newBuilder();
        }

        @Override // d0.e0.p.d.m0.i.n
        public b toBuilder() {
            return newBuilder(this);
        }

        public c() {
            this.memoizedIsInitialized = (byte) -1;
            this.memoizedSerializedSize = -1;
            this.unknownFields = d0.e0.p.d.m0.i.c.j;
        }

        public c(d0.e0.p.d.m0.i.d dVar, d0.e0.p.d.m0.i.e eVar, C0317a aVar) throws InvalidProtocolBufferException {
            this.memoizedIsInitialized = (byte) -1;
            this.memoizedSerializedSize = -1;
            boolean z2 = false;
            this.name_ = 0;
            this.desc_ = 0;
            c.b newOutput = d0.e0.p.d.m0.i.c.newOutput();
            CodedOutputStream newInstance = CodedOutputStream.newInstance(newOutput, 1);
            while (!z2) {
                try {
                    try {
                        int readTag = dVar.readTag();
                        if (readTag != 0) {
                            if (readTag == 8) {
                                this.bitField0_ |= 1;
                                this.name_ = dVar.readInt32();
                            } else if (readTag == 16) {
                                this.bitField0_ |= 2;
                                this.desc_ = dVar.readInt32();
                            } else if (!dVar.skipField(readTag, newInstance)) {
                            }
                        }
                        z2 = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2.getMessage()).setUnfinishedMessage(this);
                    }
                } catch (Throwable th) {
                    try {
                        newInstance.flush();
                    } catch (IOException unused) {
                    } catch (Throwable th2) {
                        this.unknownFields = newOutput.toByteString();
                        throw th2;
                    }
                    this.unknownFields = newOutput.toByteString();
                    throw th;
                }
            }
            try {
                newInstance.flush();
            } catch (IOException unused2) {
            } catch (Throwable th3) {
                this.unknownFields = newOutput.toByteString();
                throw th3;
            }
            this.unknownFields = newOutput.toByteString();
        }
    }

    /* compiled from: JvmProtoBuf.java */
    /* loaded from: classes3.dex */
    public static final class d extends g implements o {
        public static final d j;
        public static p<d> k = new C0321a();
        private int bitField0_;
        private b field_;
        private c getter_;
        private byte memoizedIsInitialized;
        private int memoizedSerializedSize;
        private c setter_;
        private c syntheticMethod_;
        private final d0.e0.p.d.m0.i.c unknownFields;

        /* compiled from: JvmProtoBuf.java */
        /* renamed from: d0.e0.p.d.m0.f.a0.a$d$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static class C0321a extends d0.e0.p.d.m0.i.b<d> {
            @Override // d0.e0.p.d.m0.i.p
            public d parsePartialFrom(d0.e0.p.d.m0.i.d dVar, d0.e0.p.d.m0.i.e eVar) throws InvalidProtocolBufferException {
                return new d(dVar, eVar, null);
            }
        }

        /* compiled from: JvmProtoBuf.java */
        /* loaded from: classes3.dex */
        public static final class b extends g.b<d, b> implements o {
            public int k;
            public b l = b.getDefaultInstance();
            public c m = c.getDefaultInstance();
            public c n = c.getDefaultInstance();
            public c o = c.getDefaultInstance();

            public d buildPartial() {
                d dVar = new d(this, null);
                int i = this.k;
                int i2 = 1;
                if ((i & 1) != 1) {
                    i2 = 0;
                }
                dVar.field_ = this.l;
                if ((i & 2) == 2) {
                    i2 |= 2;
                }
                dVar.syntheticMethod_ = this.m;
                if ((i & 4) == 4) {
                    i2 |= 4;
                }
                dVar.getter_ = this.n;
                if ((i & 8) == 8) {
                    i2 |= 8;
                }
                dVar.setter_ = this.o;
                dVar.bitField0_ = i2;
                return dVar;
            }

            public b mergeField(b bVar) {
                if ((this.k & 1) != 1 || this.l == b.getDefaultInstance()) {
                    this.l = bVar;
                } else {
                    this.l = b.newBuilder(this.l).mergeFrom(bVar).buildPartial();
                }
                this.k |= 1;
                return this;
            }

            public b mergeGetter(c cVar) {
                if ((this.k & 4) != 4 || this.n == c.getDefaultInstance()) {
                    this.n = cVar;
                } else {
                    this.n = c.newBuilder(this.n).mergeFrom(cVar).buildPartial();
                }
                this.k |= 4;
                return this;
            }

            public b mergeSetter(c cVar) {
                if ((this.k & 8) != 8 || this.o == c.getDefaultInstance()) {
                    this.o = cVar;
                } else {
                    this.o = c.newBuilder(this.o).mergeFrom(cVar).buildPartial();
                }
                this.k |= 8;
                return this;
            }

            public b mergeSyntheticMethod(c cVar) {
                if ((this.k & 2) != 2 || this.m == c.getDefaultInstance()) {
                    this.m = cVar;
                } else {
                    this.m = c.newBuilder(this.m).mergeFrom(cVar).buildPartial();
                }
                this.k |= 2;
                return this;
            }

            @Override // d0.e0.p.d.m0.i.n.a
            public d build() {
                d buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw new UninitializedMessageException(buildPartial);
            }

            /* JADX WARN: Can't rename method to resolve collision */
            @Override // d0.e0.p.d.m0.i.g.b
            public b clone() {
                return new b().mergeFrom(buildPartial());
            }

            public b mergeFrom(d dVar) {
                if (dVar == d.getDefaultInstance()) {
                    return this;
                }
                if (dVar.hasField()) {
                    mergeField(dVar.getField());
                }
                if (dVar.hasSyntheticMethod()) {
                    mergeSyntheticMethod(dVar.getSyntheticMethod());
                }
                if (dVar.hasGetter()) {
                    mergeGetter(dVar.getGetter());
                }
                if (dVar.hasSetter()) {
                    mergeSetter(dVar.getSetter());
                }
                setUnknownFields(getUnknownFields().concat(dVar.unknownFields));
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:14:0x001d  */
            @Override // d0.e0.p.d.m0.i.a.AbstractC0335a, d0.e0.p.d.m0.i.n.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public d0.e0.p.d.m0.f.a0.a.d.b mergeFrom(d0.e0.p.d.m0.i.d r3, d0.e0.p.d.m0.i.e r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    d0.e0.p.d.m0.i.p<d0.e0.p.d.m0.f.a0.a$d> r1 = d0.e0.p.d.m0.f.a0.a.d.k     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                    d0.e0.p.d.m0.f.a0.a$d r3 = (d0.e0.p.d.m0.f.a0.a.d) r3     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                    if (r3 == 0) goto Le
                    r2.mergeFrom(r3)
                Le:
                    return r2
                Lf:
                    r3 = move-exception
                    goto L1b
                L11:
                    r3 = move-exception
                    d0.e0.p.d.m0.i.n r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                    d0.e0.p.d.m0.f.a0.a$d r4 = (d0.e0.p.d.m0.f.a0.a.d) r4     // Catch: java.lang.Throwable -> Lf
                    throw r3     // Catch: java.lang.Throwable -> L19
                L19:
                    r3 = move-exception
                    r0 = r4
                L1b:
                    if (r0 == 0) goto L20
                    r2.mergeFrom(r0)
                L20:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.f.a0.a.d.b.mergeFrom(d0.e0.p.d.m0.i.d, d0.e0.p.d.m0.i.e):d0.e0.p.d.m0.f.a0.a$d$b");
            }
        }

        static {
            d dVar = new d();
            j = dVar;
            dVar.g();
        }

        public d(g.b bVar, C0317a aVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
            this.memoizedSerializedSize = -1;
            this.unknownFields = bVar.getUnknownFields();
        }

        public static d getDefaultInstance() {
            return j;
        }

        public static b newBuilder(d dVar) {
            return newBuilder().mergeFrom(dVar);
        }

        public final void g() {
            this.field_ = b.getDefaultInstance();
            this.syntheticMethod_ = c.getDefaultInstance();
            this.getter_ = c.getDefaultInstance();
            this.setter_ = c.getDefaultInstance();
        }

        public b getField() {
            return this.field_;
        }

        public c getGetter() {
            return this.getter_;
        }

        @Override // d0.e0.p.d.m0.i.n
        public int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if ((this.bitField0_ & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeMessageSize(1, this.field_);
            }
            if ((this.bitField0_ & 2) == 2) {
                i2 += CodedOutputStream.computeMessageSize(2, this.syntheticMethod_);
            }
            if ((this.bitField0_ & 4) == 4) {
                i2 += CodedOutputStream.computeMessageSize(3, this.getter_);
            }
            if ((this.bitField0_ & 8) == 8) {
                i2 += CodedOutputStream.computeMessageSize(4, this.setter_);
            }
            int size = this.unknownFields.size() + i2;
            this.memoizedSerializedSize = size;
            return size;
        }

        public c getSetter() {
            return this.setter_;
        }

        public c getSyntheticMethod() {
            return this.syntheticMethod_;
        }

        public boolean hasField() {
            return (this.bitField0_ & 1) == 1;
        }

        public boolean hasGetter() {
            return (this.bitField0_ & 4) == 4;
        }

        public boolean hasSetter() {
            return (this.bitField0_ & 8) == 8;
        }

        public boolean hasSyntheticMethod() {
            return (this.bitField0_ & 2) == 2;
        }

        @Override // d0.e0.p.d.m0.i.o
        public final boolean isInitialized() {
            byte b2 = this.memoizedIsInitialized;
            if (b2 == 1) {
                return true;
            }
            if (b2 == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // d0.e0.p.d.m0.i.n
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            getSerializedSize();
            if ((this.bitField0_ & 1) == 1) {
                codedOutputStream.writeMessage(1, this.field_);
            }
            if ((this.bitField0_ & 2) == 2) {
                codedOutputStream.writeMessage(2, this.syntheticMethod_);
            }
            if ((this.bitField0_ & 4) == 4) {
                codedOutputStream.writeMessage(3, this.getter_);
            }
            if ((this.bitField0_ & 8) == 8) {
                codedOutputStream.writeMessage(4, this.setter_);
            }
            codedOutputStream.writeRawBytes(this.unknownFields);
        }

        public static b newBuilder() {
            return new b();
        }

        @Override // d0.e0.p.d.m0.i.n
        public b newBuilderForType() {
            return newBuilder();
        }

        @Override // d0.e0.p.d.m0.i.n
        public b toBuilder() {
            return newBuilder(this);
        }

        public d() {
            this.memoizedIsInitialized = (byte) -1;
            this.memoizedSerializedSize = -1;
            this.unknownFields = d0.e0.p.d.m0.i.c.j;
        }

        public d(d0.e0.p.d.m0.i.d dVar, d0.e0.p.d.m0.i.e eVar, C0317a aVar) throws InvalidProtocolBufferException {
            this.memoizedIsInitialized = (byte) -1;
            this.memoizedSerializedSize = -1;
            g();
            c.b newOutput = d0.e0.p.d.m0.i.c.newOutput();
            CodedOutputStream newInstance = CodedOutputStream.newInstance(newOutput, 1);
            boolean z2 = false;
            while (!z2) {
                try {
                    try {
                        int readTag = dVar.readTag();
                        if (readTag != 0) {
                            c.b bVar = null;
                            b.C0319b builder = null;
                            c.b builder2 = null;
                            c.b builder3 = null;
                            if (readTag == 10) {
                                builder = (this.bitField0_ & 1) == 1 ? this.field_.toBuilder() : builder;
                                b bVar2 = (b) dVar.readMessage(b.k, eVar);
                                this.field_ = bVar2;
                                if (builder != null) {
                                    builder.mergeFrom(bVar2);
                                    this.field_ = builder.buildPartial();
                                }
                                this.bitField0_ |= 1;
                            } else if (readTag == 18) {
                                builder2 = (this.bitField0_ & 2) == 2 ? this.syntheticMethod_.toBuilder() : builder2;
                                c cVar = (c) dVar.readMessage(c.k, eVar);
                                this.syntheticMethod_ = cVar;
                                if (builder2 != null) {
                                    builder2.mergeFrom(cVar);
                                    this.syntheticMethod_ = builder2.buildPartial();
                                }
                                this.bitField0_ |= 2;
                            } else if (readTag == 26) {
                                builder3 = (this.bitField0_ & 4) == 4 ? this.getter_.toBuilder() : builder3;
                                c cVar2 = (c) dVar.readMessage(c.k, eVar);
                                this.getter_ = cVar2;
                                if (builder3 != null) {
                                    builder3.mergeFrom(cVar2);
                                    this.getter_ = builder3.buildPartial();
                                }
                                this.bitField0_ |= 4;
                            } else if (readTag == 34) {
                                bVar = (this.bitField0_ & 8) == 8 ? this.setter_.toBuilder() : bVar;
                                c cVar3 = (c) dVar.readMessage(c.k, eVar);
                                this.setter_ = cVar3;
                                if (bVar != null) {
                                    bVar.mergeFrom(cVar3);
                                    this.setter_ = bVar.buildPartial();
                                }
                                this.bitField0_ |= 8;
                            } else if (!dVar.skipField(readTag, newInstance)) {
                            }
                        }
                        z2 = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2.getMessage()).setUnfinishedMessage(this);
                    }
                } catch (Throwable th) {
                    try {
                        newInstance.flush();
                    } catch (IOException unused) {
                    } catch (Throwable th2) {
                        this.unknownFields = newOutput.toByteString();
                        throw th2;
                    }
                    this.unknownFields = newOutput.toByteString();
                    throw th;
                }
            }
            try {
                newInstance.flush();
            } catch (IOException unused2) {
            } catch (Throwable th3) {
                this.unknownFields = newOutput.toByteString();
                throw th3;
            }
            this.unknownFields = newOutput.toByteString();
        }
    }

    /* compiled from: JvmProtoBuf.java */
    /* loaded from: classes3.dex */
    public static final class e extends g implements o {
        public static final e j;
        public static p<e> k = new C0322a();
        private int localNameMemoizedSerializedSize;
        private List<Integer> localName_;
        private byte memoizedIsInitialized;
        private int memoizedSerializedSize;
        private List<c> record_;
        private final d0.e0.p.d.m0.i.c unknownFields;

        /* compiled from: JvmProtoBuf.java */
        /* renamed from: d0.e0.p.d.m0.f.a0.a$e$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static class C0322a extends d0.e0.p.d.m0.i.b<e> {
            @Override // d0.e0.p.d.m0.i.p
            public e parsePartialFrom(d0.e0.p.d.m0.i.d dVar, d0.e0.p.d.m0.i.e eVar) throws InvalidProtocolBufferException {
                return new e(dVar, eVar, null);
            }
        }

        /* compiled from: JvmProtoBuf.java */
        /* loaded from: classes3.dex */
        public static final class b extends g.b<e, b> implements o {
            public int k;
            public List<c> l = Collections.emptyList();
            public List<Integer> m = Collections.emptyList();

            public e buildPartial() {
                e eVar = new e(this, null);
                if ((this.k & 1) == 1) {
                    this.l = Collections.unmodifiableList(this.l);
                    this.k &= -2;
                }
                eVar.record_ = this.l;
                if ((this.k & 2) == 2) {
                    this.m = Collections.unmodifiableList(this.m);
                    this.k &= -3;
                }
                eVar.localName_ = this.m;
                return eVar;
            }

            @Override // d0.e0.p.d.m0.i.n.a
            public e build() {
                e buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw new UninitializedMessageException(buildPartial);
            }

            /* JADX WARN: Can't rename method to resolve collision */
            @Override // d0.e0.p.d.m0.i.g.b
            public b clone() {
                return new b().mergeFrom(buildPartial());
            }

            public b mergeFrom(e eVar) {
                if (eVar == e.getDefaultInstance()) {
                    return this;
                }
                if (!eVar.record_.isEmpty()) {
                    if (this.l.isEmpty()) {
                        this.l = eVar.record_;
                        this.k &= -2;
                    } else {
                        if ((this.k & 1) != 1) {
                            this.l = new ArrayList(this.l);
                            this.k |= 1;
                        }
                        this.l.addAll(eVar.record_);
                    }
                }
                if (!eVar.localName_.isEmpty()) {
                    if (this.m.isEmpty()) {
                        this.m = eVar.localName_;
                        this.k &= -3;
                    } else {
                        if ((this.k & 2) != 2) {
                            this.m = new ArrayList(this.m);
                            this.k |= 2;
                        }
                        this.m.addAll(eVar.localName_);
                    }
                }
                setUnknownFields(getUnknownFields().concat(eVar.unknownFields));
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:14:0x001d  */
            @Override // d0.e0.p.d.m0.i.a.AbstractC0335a, d0.e0.p.d.m0.i.n.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public d0.e0.p.d.m0.f.a0.a.e.b mergeFrom(d0.e0.p.d.m0.i.d r3, d0.e0.p.d.m0.i.e r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    d0.e0.p.d.m0.i.p<d0.e0.p.d.m0.f.a0.a$e> r1 = d0.e0.p.d.m0.f.a0.a.e.k     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                    d0.e0.p.d.m0.f.a0.a$e r3 = (d0.e0.p.d.m0.f.a0.a.e) r3     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                    if (r3 == 0) goto Le
                    r2.mergeFrom(r3)
                Le:
                    return r2
                Lf:
                    r3 = move-exception
                    goto L1b
                L11:
                    r3 = move-exception
                    d0.e0.p.d.m0.i.n r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                    d0.e0.p.d.m0.f.a0.a$e r4 = (d0.e0.p.d.m0.f.a0.a.e) r4     // Catch: java.lang.Throwable -> Lf
                    throw r3     // Catch: java.lang.Throwable -> L19
                L19:
                    r3 = move-exception
                    r0 = r4
                L1b:
                    if (r0 == 0) goto L20
                    r2.mergeFrom(r0)
                L20:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.f.a0.a.e.b.mergeFrom(d0.e0.p.d.m0.i.d, d0.e0.p.d.m0.i.e):d0.e0.p.d.m0.f.a0.a$e$b");
            }
        }

        /* compiled from: JvmProtoBuf.java */
        /* loaded from: classes3.dex */
        public static final class c extends g implements o {
            public static final c j;
            public static p<c> k = new C0323a();
            private int bitField0_;
            private byte memoizedIsInitialized;
            private int memoizedSerializedSize;
            private EnumC0324c operation_;
            private int predefinedIndex_;
            private int range_;
            private int replaceCharMemoizedSerializedSize;
            private List<Integer> replaceChar_;
            private Object string_;
            private int substringIndexMemoizedSerializedSize;
            private List<Integer> substringIndex_;
            private final d0.e0.p.d.m0.i.c unknownFields;

            /* compiled from: JvmProtoBuf.java */
            /* renamed from: d0.e0.p.d.m0.f.a0.a$e$c$a  reason: collision with other inner class name */
            /* loaded from: classes3.dex */
            public static class C0323a extends d0.e0.p.d.m0.i.b<c> {
                @Override // d0.e0.p.d.m0.i.p
                public c parsePartialFrom(d0.e0.p.d.m0.i.d dVar, d0.e0.p.d.m0.i.e eVar) throws InvalidProtocolBufferException {
                    return new c(dVar, eVar, null);
                }
            }

            /* compiled from: JvmProtoBuf.java */
            /* loaded from: classes3.dex */
            public static final class b extends g.b<c, b> implements o {
                public int k;
                public int m;
                public int l = 1;
                public Object n = "";
                public EnumC0324c o = EnumC0324c.NONE;
                public List<Integer> p = Collections.emptyList();
                public List<Integer> q = Collections.emptyList();

                public c buildPartial() {
                    c cVar = new c(this, null);
                    int i = this.k;
                    int i2 = 1;
                    if ((i & 1) != 1) {
                        i2 = 0;
                    }
                    cVar.range_ = this.l;
                    if ((i & 2) == 2) {
                        i2 |= 2;
                    }
                    cVar.predefinedIndex_ = this.m;
                    if ((i & 4) == 4) {
                        i2 |= 4;
                    }
                    cVar.string_ = this.n;
                    if ((i & 8) == 8) {
                        i2 |= 8;
                    }
                    cVar.operation_ = this.o;
                    if ((this.k & 16) == 16) {
                        this.p = Collections.unmodifiableList(this.p);
                        this.k &= -17;
                    }
                    cVar.substringIndex_ = this.p;
                    if ((this.k & 32) == 32) {
                        this.q = Collections.unmodifiableList(this.q);
                        this.k &= -33;
                    }
                    cVar.replaceChar_ = this.q;
                    cVar.bitField0_ = i2;
                    return cVar;
                }

                public b setOperation(EnumC0324c cVar) {
                    Objects.requireNonNull(cVar);
                    this.k |= 8;
                    this.o = cVar;
                    return this;
                }

                public b setPredefinedIndex(int i) {
                    this.k |= 2;
                    this.m = i;
                    return this;
                }

                public b setRange(int i) {
                    this.k |= 1;
                    this.l = i;
                    return this;
                }

                @Override // d0.e0.p.d.m0.i.n.a
                public c build() {
                    c buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw new UninitializedMessageException(buildPartial);
                }

                /* JADX WARN: Can't rename method to resolve collision */
                @Override // d0.e0.p.d.m0.i.g.b
                public b clone() {
                    return new b().mergeFrom(buildPartial());
                }

                public b mergeFrom(c cVar) {
                    if (cVar == c.getDefaultInstance()) {
                        return this;
                    }
                    if (cVar.hasRange()) {
                        setRange(cVar.getRange());
                    }
                    if (cVar.hasPredefinedIndex()) {
                        setPredefinedIndex(cVar.getPredefinedIndex());
                    }
                    if (cVar.hasString()) {
                        this.k |= 4;
                        this.n = cVar.string_;
                    }
                    if (cVar.hasOperation()) {
                        setOperation(cVar.getOperation());
                    }
                    if (!cVar.substringIndex_.isEmpty()) {
                        if (this.p.isEmpty()) {
                            this.p = cVar.substringIndex_;
                            this.k &= -17;
                        } else {
                            if ((this.k & 16) != 16) {
                                this.p = new ArrayList(this.p);
                                this.k |= 16;
                            }
                            this.p.addAll(cVar.substringIndex_);
                        }
                    }
                    if (!cVar.replaceChar_.isEmpty()) {
                        if (this.q.isEmpty()) {
                            this.q = cVar.replaceChar_;
                            this.k &= -33;
                        } else {
                            if ((this.k & 32) != 32) {
                                this.q = new ArrayList(this.q);
                                this.k |= 32;
                            }
                            this.q.addAll(cVar.replaceChar_);
                        }
                    }
                    setUnknownFields(getUnknownFields().concat(cVar.unknownFields));
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:14:0x001d  */
                @Override // d0.e0.p.d.m0.i.a.AbstractC0335a, d0.e0.p.d.m0.i.n.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct add '--show-bad-code' argument
                */
                public d0.e0.p.d.m0.f.a0.a.e.c.b mergeFrom(d0.e0.p.d.m0.i.d r3, d0.e0.p.d.m0.i.e r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        d0.e0.p.d.m0.i.p<d0.e0.p.d.m0.f.a0.a$e$c> r1 = d0.e0.p.d.m0.f.a0.a.e.c.k     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                        d0.e0.p.d.m0.f.a0.a$e$c r3 = (d0.e0.p.d.m0.f.a0.a.e.c) r3     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                        if (r3 == 0) goto Le
                        r2.mergeFrom(r3)
                    Le:
                        return r2
                    Lf:
                        r3 = move-exception
                        goto L1b
                    L11:
                        r3 = move-exception
                        d0.e0.p.d.m0.i.n r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                        d0.e0.p.d.m0.f.a0.a$e$c r4 = (d0.e0.p.d.m0.f.a0.a.e.c) r4     // Catch: java.lang.Throwable -> Lf
                        throw r3     // Catch: java.lang.Throwable -> L19
                    L19:
                        r3 = move-exception
                        r0 = r4
                    L1b:
                        if (r0 == 0) goto L20
                        r2.mergeFrom(r0)
                    L20:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.f.a0.a.e.c.b.mergeFrom(d0.e0.p.d.m0.i.d, d0.e0.p.d.m0.i.e):d0.e0.p.d.m0.f.a0.a$e$c$b");
                }
            }

            /* compiled from: JvmProtoBuf.java */
            /* renamed from: d0.e0.p.d.m0.f.a0.a$e$c$c  reason: collision with other inner class name */
            /* loaded from: classes3.dex */
            public enum EnumC0324c implements h.a {
                NONE(0),
                INTERNAL_TO_CLASS_ID(1),
                DESC_TO_CLASS_ID(2);
                
                private final int value;

                EnumC0324c(int i) {
                    this.value = i;
                }

                @Override // d0.e0.p.d.m0.i.h.a
                public final int getNumber() {
                    return this.value;
                }

                public static EnumC0324c valueOf(int i) {
                    if (i == 0) {
                        return NONE;
                    }
                    if (i == 1) {
                        return INTERNAL_TO_CLASS_ID;
                    }
                    if (i != 2) {
                        return null;
                    }
                    return DESC_TO_CLASS_ID;
                }
            }

            static {
                c cVar = new c();
                j = cVar;
                cVar.l();
            }

            public c(g.b bVar, C0317a aVar) {
                super(bVar);
                this.substringIndexMemoizedSerializedSize = -1;
                this.replaceCharMemoizedSerializedSize = -1;
                this.memoizedIsInitialized = (byte) -1;
                this.memoizedSerializedSize = -1;
                this.unknownFields = bVar.getUnknownFields();
            }

            public static c getDefaultInstance() {
                return j;
            }

            public static b newBuilder(c cVar) {
                return newBuilder().mergeFrom(cVar);
            }

            public EnumC0324c getOperation() {
                return this.operation_;
            }

            public int getPredefinedIndex() {
                return this.predefinedIndex_;
            }

            public int getRange() {
                return this.range_;
            }

            public int getReplaceCharCount() {
                return this.replaceChar_.size();
            }

            public List<Integer> getReplaceCharList() {
                return this.replaceChar_;
            }

            @Override // d0.e0.p.d.m0.i.n
            public int getSerializedSize() {
                int i = this.memoizedSerializedSize;
                if (i != -1) {
                    return i;
                }
                int computeInt32Size = (this.bitField0_ & 1) == 1 ? CodedOutputStream.computeInt32Size(1, this.range_) + 0 : 0;
                if ((this.bitField0_ & 2) == 2) {
                    computeInt32Size += CodedOutputStream.computeInt32Size(2, this.predefinedIndex_);
                }
                if ((this.bitField0_ & 8) == 8) {
                    computeInt32Size += CodedOutputStream.computeEnumSize(3, this.operation_.getNumber());
                }
                int i2 = 0;
                for (int i3 = 0; i3 < this.substringIndex_.size(); i3++) {
                    i2 += CodedOutputStream.computeInt32SizeNoTag(this.substringIndex_.get(i3).intValue());
                }
                int i4 = computeInt32Size + i2;
                if (!getSubstringIndexList().isEmpty()) {
                    i4 = i4 + 1 + CodedOutputStream.computeInt32SizeNoTag(i2);
                }
                this.substringIndexMemoizedSerializedSize = i2;
                int i5 = 0;
                for (int i6 = 0; i6 < this.replaceChar_.size(); i6++) {
                    i5 += CodedOutputStream.computeInt32SizeNoTag(this.replaceChar_.get(i6).intValue());
                }
                int i7 = i4 + i5;
                if (!getReplaceCharList().isEmpty()) {
                    i7 = i7 + 1 + CodedOutputStream.computeInt32SizeNoTag(i5);
                }
                this.replaceCharMemoizedSerializedSize = i5;
                if ((this.bitField0_ & 4) == 4) {
                    i7 += CodedOutputStream.computeBytesSize(6, getStringBytes());
                }
                int size = this.unknownFields.size() + i7;
                this.memoizedSerializedSize = size;
                return size;
            }

            public String getString() {
                Object obj = this.string_;
                if (obj instanceof String) {
                    return (String) obj;
                }
                d0.e0.p.d.m0.i.c cVar = (d0.e0.p.d.m0.i.c) obj;
                String stringUtf8 = cVar.toStringUtf8();
                if (cVar.isValidUtf8()) {
                    this.string_ = stringUtf8;
                }
                return stringUtf8;
            }

            public d0.e0.p.d.m0.i.c getStringBytes() {
                Object obj = this.string_;
                if (!(obj instanceof String)) {
                    return (d0.e0.p.d.m0.i.c) obj;
                }
                d0.e0.p.d.m0.i.c copyFromUtf8 = d0.e0.p.d.m0.i.c.copyFromUtf8((String) obj);
                this.string_ = copyFromUtf8;
                return copyFromUtf8;
            }

            public int getSubstringIndexCount() {
                return this.substringIndex_.size();
            }

            public List<Integer> getSubstringIndexList() {
                return this.substringIndex_;
            }

            public boolean hasOperation() {
                return (this.bitField0_ & 8) == 8;
            }

            public boolean hasPredefinedIndex() {
                return (this.bitField0_ & 2) == 2;
            }

            public boolean hasRange() {
                return (this.bitField0_ & 1) == 1;
            }

            public boolean hasString() {
                return (this.bitField0_ & 4) == 4;
            }

            @Override // d0.e0.p.d.m0.i.o
            public final boolean isInitialized() {
                byte b2 = this.memoizedIsInitialized;
                if (b2 == 1) {
                    return true;
                }
                if (b2 == 0) {
                    return false;
                }
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }

            public final void l() {
                this.range_ = 1;
                this.predefinedIndex_ = 0;
                this.string_ = "";
                this.operation_ = EnumC0324c.NONE;
                this.substringIndex_ = Collections.emptyList();
                this.replaceChar_ = Collections.emptyList();
            }

            @Override // d0.e0.p.d.m0.i.n
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                getSerializedSize();
                if ((this.bitField0_ & 1) == 1) {
                    codedOutputStream.writeInt32(1, this.range_);
                }
                if ((this.bitField0_ & 2) == 2) {
                    codedOutputStream.writeInt32(2, this.predefinedIndex_);
                }
                if ((this.bitField0_ & 8) == 8) {
                    codedOutputStream.writeEnum(3, this.operation_.getNumber());
                }
                if (getSubstringIndexList().size() > 0) {
                    codedOutputStream.writeRawVarint32(34);
                    codedOutputStream.writeRawVarint32(this.substringIndexMemoizedSerializedSize);
                }
                for (int i = 0; i < this.substringIndex_.size(); i++) {
                    codedOutputStream.writeInt32NoTag(this.substringIndex_.get(i).intValue());
                }
                if (getReplaceCharList().size() > 0) {
                    codedOutputStream.writeRawVarint32(42);
                    codedOutputStream.writeRawVarint32(this.replaceCharMemoizedSerializedSize);
                }
                for (int i2 = 0; i2 < this.replaceChar_.size(); i2++) {
                    codedOutputStream.writeInt32NoTag(this.replaceChar_.get(i2).intValue());
                }
                if ((this.bitField0_ & 4) == 4) {
                    codedOutputStream.writeBytes(6, getStringBytes());
                }
                codedOutputStream.writeRawBytes(this.unknownFields);
            }

            public static b newBuilder() {
                return new b();
            }

            @Override // d0.e0.p.d.m0.i.n
            public b newBuilderForType() {
                return newBuilder();
            }

            @Override // d0.e0.p.d.m0.i.n
            public b toBuilder() {
                return newBuilder(this);
            }

            public c() {
                this.substringIndexMemoizedSerializedSize = -1;
                this.replaceCharMemoizedSerializedSize = -1;
                this.memoizedIsInitialized = (byte) -1;
                this.memoizedSerializedSize = -1;
                this.unknownFields = d0.e0.p.d.m0.i.c.j;
            }

            public c(d0.e0.p.d.m0.i.d dVar, d0.e0.p.d.m0.i.e eVar, C0317a aVar) throws InvalidProtocolBufferException {
                this.substringIndexMemoizedSerializedSize = -1;
                this.replaceCharMemoizedSerializedSize = -1;
                this.memoizedIsInitialized = (byte) -1;
                this.memoizedSerializedSize = -1;
                l();
                CodedOutputStream newInstance = CodedOutputStream.newInstance(d0.e0.p.d.m0.i.c.newOutput(), 1);
                boolean z2 = false;
                boolean z3 = false;
                while (!z2) {
                    try {
                        try {
                            int readTag = dVar.readTag();
                            if (readTag != 0) {
                                if (readTag == 8) {
                                    this.bitField0_ |= 1;
                                    this.range_ = dVar.readInt32();
                                } else if (readTag == 16) {
                                    this.bitField0_ |= 2;
                                    this.predefinedIndex_ = dVar.readInt32();
                                } else if (readTag == 24) {
                                    int readEnum = dVar.readEnum();
                                    EnumC0324c valueOf = EnumC0324c.valueOf(readEnum);
                                    if (valueOf == null) {
                                        newInstance.writeRawVarint32(readTag);
                                        newInstance.writeRawVarint32(readEnum);
                                    } else {
                                        this.bitField0_ |= 8;
                                        this.operation_ = valueOf;
                                    }
                                } else if (readTag == 32) {
                                    if (!(z3 & true)) {
                                        this.substringIndex_ = new ArrayList();
                                        z3 |= true;
                                    }
                                    this.substringIndex_.add(Integer.valueOf(dVar.readInt32()));
                                } else if (readTag == 34) {
                                    int pushLimit = dVar.pushLimit(dVar.readRawVarint32());
                                    if (!(z3 & true) && dVar.getBytesUntilLimit() > 0) {
                                        this.substringIndex_ = new ArrayList();
                                        z3 |= true;
                                    }
                                    while (dVar.getBytesUntilLimit() > 0) {
                                        this.substringIndex_.add(Integer.valueOf(dVar.readInt32()));
                                    }
                                    dVar.popLimit(pushLimit);
                                } else if (readTag == 40) {
                                    if (!(z3 & true)) {
                                        this.replaceChar_ = new ArrayList();
                                        z3 |= true;
                                    }
                                    this.replaceChar_.add(Integer.valueOf(dVar.readInt32()));
                                } else if (readTag == 42) {
                                    int pushLimit2 = dVar.pushLimit(dVar.readRawVarint32());
                                    if (!(z3 & true) && dVar.getBytesUntilLimit() > 0) {
                                        this.replaceChar_ = new ArrayList();
                                        z3 |= true;
                                    }
                                    while (dVar.getBytesUntilLimit() > 0) {
                                        this.replaceChar_.add(Integer.valueOf(dVar.readInt32()));
                                    }
                                    dVar.popLimit(pushLimit2);
                                } else if (readTag == 50) {
                                    d0.e0.p.d.m0.i.c readBytes = dVar.readBytes();
                                    this.bitField0_ |= 4;
                                    this.string_ = readBytes;
                                } else if (!dVar.skipField(readTag, newInstance)) {
                                }
                            }
                            z2 = true;
                        } catch (Throwable th) {
                            if (z3 & true) {
                                this.substringIndex_ = Collections.unmodifiableList(this.substringIndex_);
                            }
                            if (z3 & true) {
                                this.replaceChar_ = Collections.unmodifiableList(this.replaceChar_);
                            }
                            try {
                                newInstance.flush();
                            } catch (IOException unused) {
                                throw th;
                            } finally {
                            }
                        }
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2.getMessage()).setUnfinishedMessage(this);
                    }
                }
                if (z3 & true) {
                    this.substringIndex_ = Collections.unmodifiableList(this.substringIndex_);
                }
                if (z3 & true) {
                    this.replaceChar_ = Collections.unmodifiableList(this.replaceChar_);
                }
                try {
                    newInstance.flush();
                } catch (IOException unused2) {
                } finally {
                }
            }
        }

        static {
            e eVar = new e();
            j = eVar;
            eVar.record_ = Collections.emptyList();
            eVar.localName_ = Collections.emptyList();
        }

        public e(g.b bVar, C0317a aVar) {
            super(bVar);
            this.localNameMemoizedSerializedSize = -1;
            this.memoizedIsInitialized = (byte) -1;
            this.memoizedSerializedSize = -1;
            this.unknownFields = bVar.getUnknownFields();
        }

        public static e getDefaultInstance() {
            return j;
        }

        public static b newBuilder(e eVar) {
            return newBuilder().mergeFrom(eVar);
        }

        public static e parseDelimitedFrom(InputStream inputStream, d0.e0.p.d.m0.i.e eVar) throws IOException {
            return (e) ((d0.e0.p.d.m0.i.b) k).parseDelimitedFrom(inputStream, eVar);
        }

        public List<Integer> getLocalNameList() {
            return this.localName_;
        }

        public List<c> getRecordList() {
            return this.record_;
        }

        @Override // d0.e0.p.d.m0.i.n
        public int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            for (int i3 = 0; i3 < this.record_.size(); i3++) {
                i2 += CodedOutputStream.computeMessageSize(1, this.record_.get(i3));
            }
            int i4 = 0;
            for (int i5 = 0; i5 < this.localName_.size(); i5++) {
                i4 += CodedOutputStream.computeInt32SizeNoTag(this.localName_.get(i5).intValue());
            }
            int i6 = i2 + i4;
            if (!getLocalNameList().isEmpty()) {
                i6 = i6 + 1 + CodedOutputStream.computeInt32SizeNoTag(i4);
            }
            this.localNameMemoizedSerializedSize = i4;
            int size = this.unknownFields.size() + i6;
            this.memoizedSerializedSize = size;
            return size;
        }

        @Override // d0.e0.p.d.m0.i.o
        public final boolean isInitialized() {
            byte b2 = this.memoizedIsInitialized;
            if (b2 == 1) {
                return true;
            }
            if (b2 == 0) {
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }

        @Override // d0.e0.p.d.m0.i.n
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            getSerializedSize();
            for (int i = 0; i < this.record_.size(); i++) {
                codedOutputStream.writeMessage(1, this.record_.get(i));
            }
            if (getLocalNameList().size() > 0) {
                codedOutputStream.writeRawVarint32(42);
                codedOutputStream.writeRawVarint32(this.localNameMemoizedSerializedSize);
            }
            for (int i2 = 0; i2 < this.localName_.size(); i2++) {
                codedOutputStream.writeInt32NoTag(this.localName_.get(i2).intValue());
            }
            codedOutputStream.writeRawBytes(this.unknownFields);
        }

        public static b newBuilder() {
            return new b();
        }

        @Override // d0.e0.p.d.m0.i.n
        public b newBuilderForType() {
            return newBuilder();
        }

        @Override // d0.e0.p.d.m0.i.n
        public b toBuilder() {
            return newBuilder(this);
        }

        public e() {
            this.localNameMemoizedSerializedSize = -1;
            this.memoizedIsInitialized = (byte) -1;
            this.memoizedSerializedSize = -1;
            this.unknownFields = d0.e0.p.d.m0.i.c.j;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public e(d0.e0.p.d.m0.i.d dVar, d0.e0.p.d.m0.i.e eVar, C0317a aVar) throws InvalidProtocolBufferException {
            this.localNameMemoizedSerializedSize = -1;
            this.memoizedIsInitialized = (byte) -1;
            this.memoizedSerializedSize = -1;
            this.record_ = Collections.emptyList();
            this.localName_ = Collections.emptyList();
            CodedOutputStream newInstance = CodedOutputStream.newInstance(d0.e0.p.d.m0.i.c.newOutput(), 1);
            boolean z2 = false;
            boolean z3 = false;
            while (!z2) {
                try {
                    try {
                        int readTag = dVar.readTag();
                        if (readTag != 0) {
                            if (readTag == 10) {
                                if (!z3 || !true) {
                                    this.record_ = new ArrayList();
                                    z3 |= true;
                                }
                                this.record_.add(dVar.readMessage(c.k, eVar));
                            } else if (readTag == 40) {
                                if (!(z3 & true)) {
                                    this.localName_ = new ArrayList();
                                    z3 |= true;
                                }
                                this.localName_.add(Integer.valueOf(dVar.readInt32()));
                            } else if (readTag == 42) {
                                int pushLimit = dVar.pushLimit(dVar.readRawVarint32());
                                if (!(z3 & true) && dVar.getBytesUntilLimit() > 0) {
                                    this.localName_ = new ArrayList();
                                    z3 |= true;
                                }
                                while (dVar.getBytesUntilLimit() > 0) {
                                    this.localName_.add(Integer.valueOf(dVar.readInt32()));
                                }
                                dVar.popLimit(pushLimit);
                            } else if (!dVar.skipField(readTag, newInstance)) {
                            }
                        }
                        z2 = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2.getMessage()).setUnfinishedMessage(this);
                    }
                } catch (Throwable th) {
                    if (z3 && true) {
                        this.record_ = Collections.unmodifiableList(this.record_);
                    }
                    if (z3 & true) {
                        this.localName_ = Collections.unmodifiableList(this.localName_);
                    }
                    try {
                        newInstance.flush();
                    } catch (IOException unused) {
                        throw th;
                    } finally {
                    }
                }
            }
            if (z3 && true) {
                this.record_ = Collections.unmodifiableList(this.record_);
            }
            if (z3 & true) {
                this.localName_ = Collections.unmodifiableList(this.localName_);
            }
            try {
                newInstance.flush();
            } catch (IOException unused2) {
            } finally {
            }
        }
    }

    static {
        d0.e0.p.d.m0.f.d defaultInstance = d0.e0.p.d.m0.f.d.getDefaultInstance();
        c defaultInstance2 = c.getDefaultInstance();
        c defaultInstance3 = c.getDefaultInstance();
        w.b bVar = w.b.t;
        a = g.newSingularGeneratedExtension(defaultInstance, defaultInstance2, defaultInstance3, null, 100, bVar, c.class);
        f3356b = g.newSingularGeneratedExtension(i.getDefaultInstance(), c.getDefaultInstance(), c.getDefaultInstance(), null, 100, bVar, c.class);
        i defaultInstance4 = i.getDefaultInstance();
        w.b bVar2 = w.b.n;
        c = g.newSingularGeneratedExtension(defaultInstance4, 0, null, null, 101, bVar2, Integer.class);
        d = g.newSingularGeneratedExtension(n.getDefaultInstance(), d.getDefaultInstance(), d.getDefaultInstance(), null, 100, bVar, d.class);
        e = g.newSingularGeneratedExtension(n.getDefaultInstance(), 0, null, null, 101, bVar2, Integer.class);
        f = g.newRepeatedGeneratedExtension(q.getDefaultInstance(), d0.e0.p.d.m0.f.b.getDefaultInstance(), null, 100, bVar, false, d0.e0.p.d.m0.f.b.class);
        h = g.newRepeatedGeneratedExtension(s.getDefaultInstance(), d0.e0.p.d.m0.f.b.getDefaultInstance(), null, 100, bVar, false, d0.e0.p.d.m0.f.b.class);
        i = g.newSingularGeneratedExtension(d0.e0.p.d.m0.f.c.getDefaultInstance(), 0, null, null, 101, bVar2, Integer.class);
        j = g.newRepeatedGeneratedExtension(d0.e0.p.d.m0.f.c.getDefaultInstance(), n.getDefaultInstance(), null, 102, bVar, false, n.class);
        k = g.newSingularGeneratedExtension(d0.e0.p.d.m0.f.c.getDefaultInstance(), 0, null, null, 103, bVar2, Integer.class);
        l = g.newSingularGeneratedExtension(d0.e0.p.d.m0.f.c.getDefaultInstance(), 0, null, null, 104, bVar2, Integer.class);
        m = g.newSingularGeneratedExtension(l.getDefaultInstance(), 0, null, null, 101, bVar2, Integer.class);
        n = g.newRepeatedGeneratedExtension(l.getDefaultInstance(), n.getDefaultInstance(), null, 102, bVar, false, n.class);
    }

    public static void registerAllExtensions(d0.e0.p.d.m0.i.e eVar) {
        eVar.add(a);
        eVar.add(f3356b);
        eVar.add(c);
        eVar.add(d);
        eVar.add(e);
        eVar.add(f);
        eVar.add(g);
        eVar.add(h);
        eVar.add(i);
        eVar.add(j);
        eVar.add(k);
        eVar.add(l);
        eVar.add(m);
        eVar.add(n);
    }
}
