package d0.e0.p.d.m0.f.a0.b;

import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: JvmBytecodeBinaryVersion.kt */
/* loaded from: classes3.dex */
public final class c extends d0.e0.p.d.m0.f.z.a {
    public static final c f = new c(1, 0, 3);

    /* compiled from: JvmBytecodeBinaryVersion.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    static {
        new a(null);
        new c(new int[0]);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public c(int... r4) {
        /*
            r3 = this;
            java.lang.String r0 = "numbers"
            d0.z.d.m.checkNotNullParameter(r4, r0)
            int r0 = r4.length
            int[] r0 = new int[r0]
            int r1 = r4.length
            r2 = 0
            java.lang.System.arraycopy(r4, r2, r0, r2, r1)
            r3.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.f.a0.b.c.<init>(int[]):void");
    }
}
