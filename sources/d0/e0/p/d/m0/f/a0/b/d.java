package d0.e0.p.d.m0.f.a0.b;

import d0.e0.p.d.m0.f.z.b;
/* compiled from: JvmFlags.kt */
/* loaded from: classes3.dex */
public final class d {
    public static final d a = new d();

    /* renamed from: b  reason: collision with root package name */
    public static final b.C0334b f3358b = b.d.booleanFirst();
    public static final b.C0334b c;

    static {
        b.C0334b booleanFirst = b.d.booleanFirst();
        c = booleanFirst;
        b.d.booleanAfter(booleanFirst);
    }

    public final b.C0334b getIS_MOVED_FROM_INTERFACE_COMPANION() {
        return f3358b;
    }
}
