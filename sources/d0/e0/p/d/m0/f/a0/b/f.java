package d0.e0.p.d.m0.f.a0.b;

import d0.z.d.m;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: JvmMetadataVersion.kt */
/* loaded from: classes3.dex */
public final class f extends d0.e0.p.d.m0.f.z.a {
    public static final f f = new f(1, 4, 2);
    public final boolean g;

    /* compiled from: JvmMetadataVersion.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    static {
        new a(null);
        new f(new int[0]);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public f(int[] r4, boolean r5) {
        /*
            r3 = this;
            java.lang.String r0 = "versionArray"
            d0.z.d.m.checkNotNullParameter(r4, r0)
            int r0 = r4.length
            int[] r0 = new int[r0]
            int r1 = r4.length
            r2 = 0
            java.lang.System.arraycopy(r4, r2, r0, r2, r1)
            r3.<init>(r0)
            r3.g = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.f.a0.b.f.<init>(int[], boolean):void");
    }

    public boolean isCompatible() {
        boolean z2;
        if (getMajor() == 1 && getMinor() == 0) {
            return false;
        }
        if (this.g) {
            z2 = a(f);
        } else {
            int major = getMajor();
            f fVar = f;
            z2 = major == fVar.getMajor() && getMinor() <= fVar.getMinor() + 1;
        }
        return z2;
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public f(int... iArr) {
        this(iArr, false);
        m.checkNotNullParameter(iArr, "numbers");
    }
}
