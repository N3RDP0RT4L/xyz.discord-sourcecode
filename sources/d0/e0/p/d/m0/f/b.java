package d0.e0.p.d.m0.f;

import com.google.android.material.shadow.ShadowDrawableWrapper;
import d0.e0.p.d.m0.i.c;
import d0.e0.p.d.m0.i.d;
import d0.e0.p.d.m0.i.e;
import d0.e0.p.d.m0.i.g;
import d0.e0.p.d.m0.i.h;
import d0.e0.p.d.m0.i.o;
import d0.e0.p.d.m0.i.p;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import kotlin.reflect.jvm.internal.impl.protobuf.CodedOutputStream;
import kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException;
import kotlin.reflect.jvm.internal.impl.protobuf.UninitializedMessageException;
/* compiled from: ProtoBuf.java */
/* loaded from: classes3.dex */
public final class b extends g implements o {
    public static final b j;
    public static p<b> k = new a();
    private List<C0325b> argument_;
    private int bitField0_;
    private int id_;
    private byte memoizedIsInitialized;
    private int memoizedSerializedSize;
    private final d0.e0.p.d.m0.i.c unknownFields;

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static class a extends d0.e0.p.d.m0.i.b<b> {
        @Override // d0.e0.p.d.m0.i.p
        public b parsePartialFrom(d dVar, e eVar) throws InvalidProtocolBufferException {
            return new b(dVar, eVar, null);
        }
    }

    /* compiled from: ProtoBuf.java */
    /* renamed from: d0.e0.p.d.m0.f.b$b  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0325b extends g implements o {
        public static final C0325b j;
        public static p<C0325b> k = new a();
        private int bitField0_;
        private byte memoizedIsInitialized;
        private int memoizedSerializedSize;
        private int nameId_;
        private final d0.e0.p.d.m0.i.c unknownFields;
        private c value_;

        /* compiled from: ProtoBuf.java */
        /* renamed from: d0.e0.p.d.m0.f.b$b$a */
        /* loaded from: classes3.dex */
        public static class a extends d0.e0.p.d.m0.i.b<C0325b> {
            @Override // d0.e0.p.d.m0.i.p
            public C0325b parsePartialFrom(d dVar, e eVar) throws InvalidProtocolBufferException {
                return new C0325b(dVar, eVar, null);
            }
        }

        /* compiled from: ProtoBuf.java */
        /* renamed from: d0.e0.p.d.m0.f.b$b$b  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0326b extends g.b<C0325b, C0326b> implements o {
            public int k;
            public int l;
            public c m = c.getDefaultInstance();

            public C0325b buildPartial() {
                C0325b bVar = new C0325b(this, null);
                int i = this.k;
                int i2 = 1;
                if ((i & 1) != 1) {
                    i2 = 0;
                }
                bVar.nameId_ = this.l;
                if ((i & 2) == 2) {
                    i2 |= 2;
                }
                bVar.value_ = this.m;
                bVar.bitField0_ = i2;
                return bVar;
            }

            public C0326b mergeValue(c cVar) {
                if ((this.k & 2) != 2 || this.m == c.getDefaultInstance()) {
                    this.m = cVar;
                } else {
                    this.m = c.newBuilder(this.m).mergeFrom(cVar).buildPartial();
                }
                this.k |= 2;
                return this;
            }

            public C0326b setNameId(int i) {
                this.k |= 1;
                this.l = i;
                return this;
            }

            @Override // d0.e0.p.d.m0.i.n.a
            public C0325b build() {
                C0325b buildPartial = buildPartial();
                if (buildPartial.isInitialized()) {
                    return buildPartial;
                }
                throw new UninitializedMessageException(buildPartial);
            }

            /* JADX WARN: Can't rename method to resolve collision */
            @Override // d0.e0.p.d.m0.i.g.b
            public C0326b clone() {
                return new C0326b().mergeFrom(buildPartial());
            }

            public C0326b mergeFrom(C0325b bVar) {
                if (bVar == C0325b.getDefaultInstance()) {
                    return this;
                }
                if (bVar.hasNameId()) {
                    setNameId(bVar.getNameId());
                }
                if (bVar.hasValue()) {
                    mergeValue(bVar.getValue());
                }
                setUnknownFields(getUnknownFields().concat(bVar.unknownFields));
                return this;
            }

            /* JADX WARN: Removed duplicated region for block: B:14:0x001d  */
            @Override // d0.e0.p.d.m0.i.a.AbstractC0335a, d0.e0.p.d.m0.i.n.a
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public d0.e0.p.d.m0.f.b.C0325b.C0326b mergeFrom(d0.e0.p.d.m0.i.d r3, d0.e0.p.d.m0.i.e r4) throws java.io.IOException {
                /*
                    r2 = this;
                    r0 = 0
                    d0.e0.p.d.m0.i.p<d0.e0.p.d.m0.f.b$b> r1 = d0.e0.p.d.m0.f.b.C0325b.k     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                    java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                    d0.e0.p.d.m0.f.b$b r3 = (d0.e0.p.d.m0.f.b.C0325b) r3     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                    if (r3 == 0) goto Le
                    r2.mergeFrom(r3)
                Le:
                    return r2
                Lf:
                    r3 = move-exception
                    goto L1b
                L11:
                    r3 = move-exception
                    d0.e0.p.d.m0.i.n r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                    d0.e0.p.d.m0.f.b$b r4 = (d0.e0.p.d.m0.f.b.C0325b) r4     // Catch: java.lang.Throwable -> Lf
                    throw r3     // Catch: java.lang.Throwable -> L19
                L19:
                    r3 = move-exception
                    r0 = r4
                L1b:
                    if (r0 == 0) goto L20
                    r2.mergeFrom(r0)
                L20:
                    throw r3
                */
                throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.f.b.C0325b.C0326b.mergeFrom(d0.e0.p.d.m0.i.d, d0.e0.p.d.m0.i.e):d0.e0.p.d.m0.f.b$b$b");
            }
        }

        /* compiled from: ProtoBuf.java */
        /* renamed from: d0.e0.p.d.m0.f.b$b$c */
        /* loaded from: classes3.dex */
        public static final class c extends g implements o {
            public static final c j;
            public static p<c> k = new a();
            private b annotation_;
            private int arrayDimensionCount_;
            private List<c> arrayElement_;
            private int bitField0_;
            private int classId_;
            private double doubleValue_;
            private int enumValueId_;
            private int flags_;
            private float floatValue_;
            private long intValue_;
            private byte memoizedIsInitialized;
            private int memoizedSerializedSize;
            private int stringValue_;
            private EnumC0328c type_;
            private final d0.e0.p.d.m0.i.c unknownFields;

            /* compiled from: ProtoBuf.java */
            /* renamed from: d0.e0.p.d.m0.f.b$b$c$a */
            /* loaded from: classes3.dex */
            public static class a extends d0.e0.p.d.m0.i.b<c> {
                @Override // d0.e0.p.d.m0.i.p
                public c parsePartialFrom(d dVar, e eVar) throws InvalidProtocolBufferException {
                    return new c(dVar, eVar, null);
                }
            }

            /* compiled from: ProtoBuf.java */
            /* renamed from: d0.e0.p.d.m0.f.b$b$c$b  reason: collision with other inner class name */
            /* loaded from: classes3.dex */
            public static final class C0327b extends g.b<c, C0327b> implements o {
                public int k;
                public long m;
                public float n;
                public double o;
                public int p;
                public int q;
                public int r;
                public int u;
                public int v;
                public EnumC0328c l = EnumC0328c.BYTE;

                /* renamed from: s  reason: collision with root package name */
                public b f3363s = b.getDefaultInstance();
                public List<c> t = Collections.emptyList();

                public c buildPartial() {
                    c cVar = new c(this, null);
                    int i = this.k;
                    int i2 = 1;
                    if ((i & 1) != 1) {
                        i2 = 0;
                    }
                    cVar.type_ = this.l;
                    if ((i & 2) == 2) {
                        i2 |= 2;
                    }
                    cVar.intValue_ = this.m;
                    if ((i & 4) == 4) {
                        i2 |= 4;
                    }
                    cVar.floatValue_ = this.n;
                    if ((i & 8) == 8) {
                        i2 |= 8;
                    }
                    cVar.doubleValue_ = this.o;
                    if ((i & 16) == 16) {
                        i2 |= 16;
                    }
                    cVar.stringValue_ = this.p;
                    if ((i & 32) == 32) {
                        i2 |= 32;
                    }
                    cVar.classId_ = this.q;
                    if ((i & 64) == 64) {
                        i2 |= 64;
                    }
                    cVar.enumValueId_ = this.r;
                    if ((i & 128) == 128) {
                        i2 |= 128;
                    }
                    cVar.annotation_ = this.f3363s;
                    if ((this.k & 256) == 256) {
                        this.t = Collections.unmodifiableList(this.t);
                        this.k &= -257;
                    }
                    cVar.arrayElement_ = this.t;
                    if ((i & 512) == 512) {
                        i2 |= 256;
                    }
                    cVar.arrayDimensionCount_ = this.u;
                    if ((i & 1024) == 1024) {
                        i2 |= 512;
                    }
                    cVar.flags_ = this.v;
                    cVar.bitField0_ = i2;
                    return cVar;
                }

                public C0327b mergeAnnotation(b bVar) {
                    if ((this.k & 128) != 128 || this.f3363s == b.getDefaultInstance()) {
                        this.f3363s = bVar;
                    } else {
                        this.f3363s = b.newBuilder(this.f3363s).mergeFrom(bVar).buildPartial();
                    }
                    this.k |= 128;
                    return this;
                }

                public C0327b setArrayDimensionCount(int i) {
                    this.k |= 512;
                    this.u = i;
                    return this;
                }

                public C0327b setClassId(int i) {
                    this.k |= 32;
                    this.q = i;
                    return this;
                }

                public C0327b setDoubleValue(double d) {
                    this.k |= 8;
                    this.o = d;
                    return this;
                }

                public C0327b setEnumValueId(int i) {
                    this.k |= 64;
                    this.r = i;
                    return this;
                }

                public C0327b setFlags(int i) {
                    this.k |= 1024;
                    this.v = i;
                    return this;
                }

                public C0327b setFloatValue(float f) {
                    this.k |= 4;
                    this.n = f;
                    return this;
                }

                public C0327b setIntValue(long j) {
                    this.k |= 2;
                    this.m = j;
                    return this;
                }

                public C0327b setStringValue(int i) {
                    this.k |= 16;
                    this.p = i;
                    return this;
                }

                public C0327b setType(EnumC0328c cVar) {
                    Objects.requireNonNull(cVar);
                    this.k |= 1;
                    this.l = cVar;
                    return this;
                }

                @Override // d0.e0.p.d.m0.i.n.a
                public c build() {
                    c buildPartial = buildPartial();
                    if (buildPartial.isInitialized()) {
                        return buildPartial;
                    }
                    throw new UninitializedMessageException(buildPartial);
                }

                /* JADX WARN: Can't rename method to resolve collision */
                @Override // d0.e0.p.d.m0.i.g.b
                public C0327b clone() {
                    return new C0327b().mergeFrom(buildPartial());
                }

                public C0327b mergeFrom(c cVar) {
                    if (cVar == c.getDefaultInstance()) {
                        return this;
                    }
                    if (cVar.hasType()) {
                        setType(cVar.getType());
                    }
                    if (cVar.hasIntValue()) {
                        setIntValue(cVar.getIntValue());
                    }
                    if (cVar.hasFloatValue()) {
                        setFloatValue(cVar.getFloatValue());
                    }
                    if (cVar.hasDoubleValue()) {
                        setDoubleValue(cVar.getDoubleValue());
                    }
                    if (cVar.hasStringValue()) {
                        setStringValue(cVar.getStringValue());
                    }
                    if (cVar.hasClassId()) {
                        setClassId(cVar.getClassId());
                    }
                    if (cVar.hasEnumValueId()) {
                        setEnumValueId(cVar.getEnumValueId());
                    }
                    if (cVar.hasAnnotation()) {
                        mergeAnnotation(cVar.getAnnotation());
                    }
                    if (!cVar.arrayElement_.isEmpty()) {
                        if (this.t.isEmpty()) {
                            this.t = cVar.arrayElement_;
                            this.k &= -257;
                        } else {
                            if ((this.k & 256) != 256) {
                                this.t = new ArrayList(this.t);
                                this.k |= 256;
                            }
                            this.t.addAll(cVar.arrayElement_);
                        }
                    }
                    if (cVar.hasArrayDimensionCount()) {
                        setArrayDimensionCount(cVar.getArrayDimensionCount());
                    }
                    if (cVar.hasFlags()) {
                        setFlags(cVar.getFlags());
                    }
                    setUnknownFields(getUnknownFields().concat(cVar.unknownFields));
                    return this;
                }

                /* JADX WARN: Removed duplicated region for block: B:14:0x001d  */
                @Override // d0.e0.p.d.m0.i.a.AbstractC0335a, d0.e0.p.d.m0.i.n.a
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct add '--show-bad-code' argument
                */
                public d0.e0.p.d.m0.f.b.C0325b.c.C0327b mergeFrom(d0.e0.p.d.m0.i.d r3, d0.e0.p.d.m0.i.e r4) throws java.io.IOException {
                    /*
                        r2 = this;
                        r0 = 0
                        d0.e0.p.d.m0.i.p<d0.e0.p.d.m0.f.b$b$c> r1 = d0.e0.p.d.m0.f.b.C0325b.c.k     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                        java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                        d0.e0.p.d.m0.f.b$b$c r3 = (d0.e0.p.d.m0.f.b.C0325b.c) r3     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                        if (r3 == 0) goto Le
                        r2.mergeFrom(r3)
                    Le:
                        return r2
                    Lf:
                        r3 = move-exception
                        goto L1b
                    L11:
                        r3 = move-exception
                        d0.e0.p.d.m0.i.n r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                        d0.e0.p.d.m0.f.b$b$c r4 = (d0.e0.p.d.m0.f.b.C0325b.c) r4     // Catch: java.lang.Throwable -> Lf
                        throw r3     // Catch: java.lang.Throwable -> L19
                    L19:
                        r3 = move-exception
                        r0 = r4
                    L1b:
                        if (r0 == 0) goto L20
                        r2.mergeFrom(r0)
                    L20:
                        throw r3
                    */
                    throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.f.b.C0325b.c.C0327b.mergeFrom(d0.e0.p.d.m0.i.d, d0.e0.p.d.m0.i.e):d0.e0.p.d.m0.f.b$b$c$b");
                }
            }

            /* compiled from: ProtoBuf.java */
            /* renamed from: d0.e0.p.d.m0.f.b$b$c$c  reason: collision with other inner class name */
            /* loaded from: classes3.dex */
            public enum EnumC0328c implements h.a {
                BYTE(0),
                CHAR(1),
                SHORT(2),
                INT(3),
                LONG(4),
                FLOAT(5),
                DOUBLE(6),
                BOOLEAN(7),
                STRING(8),
                CLASS(9),
                ENUM(10),
                ANNOTATION(11),
                ARRAY(12);
                
                private final int value;

                EnumC0328c(int i) {
                    this.value = i;
                }

                @Override // d0.e0.p.d.m0.i.h.a
                public final int getNumber() {
                    return this.value;
                }

                public static EnumC0328c valueOf(int i) {
                    switch (i) {
                        case 0:
                            return BYTE;
                        case 1:
                            return CHAR;
                        case 2:
                            return SHORT;
                        case 3:
                            return INT;
                        case 4:
                            return LONG;
                        case 5:
                            return FLOAT;
                        case 6:
                            return DOUBLE;
                        case 7:
                            return BOOLEAN;
                        case 8:
                            return STRING;
                        case 9:
                            return CLASS;
                        case 10:
                            return ENUM;
                        case 11:
                            return ANNOTATION;
                        case 12:
                            return ARRAY;
                        default:
                            return null;
                    }
                }
            }

            static {
                c cVar = new c();
                j = cVar;
                cVar.o();
            }

            public c(g.b bVar, d0.e0.p.d.m0.f.a aVar) {
                super(bVar);
                this.memoizedIsInitialized = (byte) -1;
                this.memoizedSerializedSize = -1;
                this.unknownFields = bVar.getUnknownFields();
            }

            public static c getDefaultInstance() {
                return j;
            }

            public static C0327b newBuilder(c cVar) {
                return newBuilder().mergeFrom(cVar);
            }

            public b getAnnotation() {
                return this.annotation_;
            }

            public int getArrayDimensionCount() {
                return this.arrayDimensionCount_;
            }

            public c getArrayElement(int i) {
                return this.arrayElement_.get(i);
            }

            public int getArrayElementCount() {
                return this.arrayElement_.size();
            }

            public List<c> getArrayElementList() {
                return this.arrayElement_;
            }

            public int getClassId() {
                return this.classId_;
            }

            public double getDoubleValue() {
                return this.doubleValue_;
            }

            public int getEnumValueId() {
                return this.enumValueId_;
            }

            public int getFlags() {
                return this.flags_;
            }

            public float getFloatValue() {
                return this.floatValue_;
            }

            public long getIntValue() {
                return this.intValue_;
            }

            @Override // d0.e0.p.d.m0.i.n
            public int getSerializedSize() {
                int i = this.memoizedSerializedSize;
                if (i != -1) {
                    return i;
                }
                int computeEnumSize = (this.bitField0_ & 1) == 1 ? CodedOutputStream.computeEnumSize(1, this.type_.getNumber()) + 0 : 0;
                if ((this.bitField0_ & 2) == 2) {
                    computeEnumSize += CodedOutputStream.computeSInt64Size(2, this.intValue_);
                }
                if ((this.bitField0_ & 4) == 4) {
                    computeEnumSize += CodedOutputStream.computeFloatSize(3, this.floatValue_);
                }
                if ((this.bitField0_ & 8) == 8) {
                    computeEnumSize += CodedOutputStream.computeDoubleSize(4, this.doubleValue_);
                }
                if ((this.bitField0_ & 16) == 16) {
                    computeEnumSize += CodedOutputStream.computeInt32Size(5, this.stringValue_);
                }
                if ((this.bitField0_ & 32) == 32) {
                    computeEnumSize += CodedOutputStream.computeInt32Size(6, this.classId_);
                }
                if ((this.bitField0_ & 64) == 64) {
                    computeEnumSize += CodedOutputStream.computeInt32Size(7, this.enumValueId_);
                }
                if ((this.bitField0_ & 128) == 128) {
                    computeEnumSize += CodedOutputStream.computeMessageSize(8, this.annotation_);
                }
                for (int i2 = 0; i2 < this.arrayElement_.size(); i2++) {
                    computeEnumSize += CodedOutputStream.computeMessageSize(9, this.arrayElement_.get(i2));
                }
                if ((this.bitField0_ & 512) == 512) {
                    computeEnumSize += CodedOutputStream.computeInt32Size(10, this.flags_);
                }
                if ((this.bitField0_ & 256) == 256) {
                    computeEnumSize += CodedOutputStream.computeInt32Size(11, this.arrayDimensionCount_);
                }
                int size = this.unknownFields.size() + computeEnumSize;
                this.memoizedSerializedSize = size;
                return size;
            }

            public int getStringValue() {
                return this.stringValue_;
            }

            public EnumC0328c getType() {
                return this.type_;
            }

            public boolean hasAnnotation() {
                return (this.bitField0_ & 128) == 128;
            }

            public boolean hasArrayDimensionCount() {
                return (this.bitField0_ & 256) == 256;
            }

            public boolean hasClassId() {
                return (this.bitField0_ & 32) == 32;
            }

            public boolean hasDoubleValue() {
                return (this.bitField0_ & 8) == 8;
            }

            public boolean hasEnumValueId() {
                return (this.bitField0_ & 64) == 64;
            }

            public boolean hasFlags() {
                return (this.bitField0_ & 512) == 512;
            }

            public boolean hasFloatValue() {
                return (this.bitField0_ & 4) == 4;
            }

            public boolean hasIntValue() {
                return (this.bitField0_ & 2) == 2;
            }

            public boolean hasStringValue() {
                return (this.bitField0_ & 16) == 16;
            }

            public boolean hasType() {
                return (this.bitField0_ & 1) == 1;
            }

            @Override // d0.e0.p.d.m0.i.o
            public final boolean isInitialized() {
                byte b2 = this.memoizedIsInitialized;
                if (b2 == 1) {
                    return true;
                }
                if (b2 == 0) {
                    return false;
                }
                if (!hasAnnotation() || getAnnotation().isInitialized()) {
                    for (int i = 0; i < getArrayElementCount(); i++) {
                        if (!getArrayElement(i).isInitialized()) {
                            this.memoizedIsInitialized = (byte) 0;
                            return false;
                        }
                    }
                    this.memoizedIsInitialized = (byte) 1;
                    return true;
                }
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }

            public final void o() {
                this.type_ = EnumC0328c.BYTE;
                this.intValue_ = 0L;
                this.floatValue_ = 0.0f;
                this.doubleValue_ = ShadowDrawableWrapper.COS_45;
                this.stringValue_ = 0;
                this.classId_ = 0;
                this.enumValueId_ = 0;
                this.annotation_ = b.getDefaultInstance();
                this.arrayElement_ = Collections.emptyList();
                this.arrayDimensionCount_ = 0;
                this.flags_ = 0;
            }

            @Override // d0.e0.p.d.m0.i.n
            public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
                getSerializedSize();
                if ((this.bitField0_ & 1) == 1) {
                    codedOutputStream.writeEnum(1, this.type_.getNumber());
                }
                if ((this.bitField0_ & 2) == 2) {
                    codedOutputStream.writeSInt64(2, this.intValue_);
                }
                if ((this.bitField0_ & 4) == 4) {
                    codedOutputStream.writeFloat(3, this.floatValue_);
                }
                if ((this.bitField0_ & 8) == 8) {
                    codedOutputStream.writeDouble(4, this.doubleValue_);
                }
                if ((this.bitField0_ & 16) == 16) {
                    codedOutputStream.writeInt32(5, this.stringValue_);
                }
                if ((this.bitField0_ & 32) == 32) {
                    codedOutputStream.writeInt32(6, this.classId_);
                }
                if ((this.bitField0_ & 64) == 64) {
                    codedOutputStream.writeInt32(7, this.enumValueId_);
                }
                if ((this.bitField0_ & 128) == 128) {
                    codedOutputStream.writeMessage(8, this.annotation_);
                }
                for (int i = 0; i < this.arrayElement_.size(); i++) {
                    codedOutputStream.writeMessage(9, this.arrayElement_.get(i));
                }
                if ((this.bitField0_ & 512) == 512) {
                    codedOutputStream.writeInt32(10, this.flags_);
                }
                if ((this.bitField0_ & 256) == 256) {
                    codedOutputStream.writeInt32(11, this.arrayDimensionCount_);
                }
                codedOutputStream.writeRawBytes(this.unknownFields);
            }

            public static C0327b newBuilder() {
                return new C0327b();
            }

            @Override // d0.e0.p.d.m0.i.n
            public C0327b newBuilderForType() {
                return newBuilder();
            }

            @Override // d0.e0.p.d.m0.i.n
            public C0327b toBuilder() {
                return newBuilder(this);
            }

            public c() {
                this.memoizedIsInitialized = (byte) -1;
                this.memoizedSerializedSize = -1;
                this.unknownFields = d0.e0.p.d.m0.i.c.j;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public c(d dVar, e eVar, d0.e0.p.d.m0.f.a aVar) throws InvalidProtocolBufferException {
                this.memoizedIsInitialized = (byte) -1;
                this.memoizedSerializedSize = -1;
                o();
                CodedOutputStream newInstance = CodedOutputStream.newInstance(d0.e0.p.d.m0.i.c.newOutput(), 1);
                boolean z2 = false;
                boolean z3 = false;
                while (!z2) {
                    try {
                        try {
                            int readTag = dVar.readTag();
                            switch (readTag) {
                                case 0:
                                    break;
                                case 8:
                                    int readEnum = dVar.readEnum();
                                    EnumC0328c valueOf = EnumC0328c.valueOf(readEnum);
                                    if (valueOf == null) {
                                        newInstance.writeRawVarint32(readTag);
                                        newInstance.writeRawVarint32(readEnum);
                                        continue;
                                    } else {
                                        this.bitField0_ |= 1;
                                        this.type_ = valueOf;
                                    }
                                case 16:
                                    this.bitField0_ |= 2;
                                    this.intValue_ = dVar.readSInt64();
                                    continue;
                                case 29:
                                    this.bitField0_ |= 4;
                                    this.floatValue_ = dVar.readFloat();
                                    continue;
                                case 33:
                                    this.bitField0_ |= 8;
                                    this.doubleValue_ = dVar.readDouble();
                                    continue;
                                case 40:
                                    this.bitField0_ |= 16;
                                    this.stringValue_ = dVar.readInt32();
                                    continue;
                                case 48:
                                    this.bitField0_ |= 32;
                                    this.classId_ = dVar.readInt32();
                                    continue;
                                case 56:
                                    this.bitField0_ |= 64;
                                    this.enumValueId_ = dVar.readInt32();
                                    continue;
                                case 66:
                                    c builder = (this.bitField0_ & 128) == 128 ? this.annotation_.toBuilder() : null;
                                    b bVar = (b) dVar.readMessage(b.k, eVar);
                                    this.annotation_ = bVar;
                                    if (builder != null) {
                                        builder.mergeFrom(bVar);
                                        this.annotation_ = builder.buildPartial();
                                    }
                                    this.bitField0_ |= 128;
                                    continue;
                                case 74:
                                    if (!(z3 & true)) {
                                        this.arrayElement_ = new ArrayList();
                                        z3 |= true;
                                    }
                                    this.arrayElement_.add(dVar.readMessage(k, eVar));
                                    continue;
                                case 80:
                                    this.bitField0_ |= 512;
                                    this.flags_ = dVar.readInt32();
                                    continue;
                                case 88:
                                    this.bitField0_ |= 256;
                                    this.arrayDimensionCount_ = dVar.readInt32();
                                    continue;
                                default:
                                    if (!dVar.skipField(readTag, newInstance)) {
                                        break;
                                    } else {
                                        continue;
                                    }
                            }
                            z2 = true;
                        } catch (InvalidProtocolBufferException e) {
                            throw e.setUnfinishedMessage(this);
                        } catch (IOException e2) {
                            throw new InvalidProtocolBufferException(e2.getMessage()).setUnfinishedMessage(this);
                        }
                    } catch (Throwable th) {
                        if (z3 & true) {
                            this.arrayElement_ = Collections.unmodifiableList(this.arrayElement_);
                        }
                        try {
                            newInstance.flush();
                        } catch (IOException unused) {
                            throw th;
                        } finally {
                        }
                    }
                }
                if (z3 & true) {
                    this.arrayElement_ = Collections.unmodifiableList(this.arrayElement_);
                }
                try {
                    newInstance.flush();
                } catch (IOException unused2) {
                } finally {
                }
            }
        }

        static {
            C0325b bVar = new C0325b();
            j = bVar;
            bVar.nameId_ = 0;
            bVar.value_ = c.getDefaultInstance();
        }

        public C0325b(g.b bVar, d0.e0.p.d.m0.f.a aVar) {
            super(bVar);
            this.memoizedIsInitialized = (byte) -1;
            this.memoizedSerializedSize = -1;
            this.unknownFields = bVar.getUnknownFields();
        }

        public static C0325b getDefaultInstance() {
            return j;
        }

        public static C0326b newBuilder(C0325b bVar) {
            return newBuilder().mergeFrom(bVar);
        }

        public int getNameId() {
            return this.nameId_;
        }

        @Override // d0.e0.p.d.m0.i.n
        public int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if ((this.bitField0_ & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeInt32Size(1, this.nameId_);
            }
            if ((this.bitField0_ & 2) == 2) {
                i2 += CodedOutputStream.computeMessageSize(2, this.value_);
            }
            int size = this.unknownFields.size() + i2;
            this.memoizedSerializedSize = size;
            return size;
        }

        public c getValue() {
            return this.value_;
        }

        public boolean hasNameId() {
            return (this.bitField0_ & 1) == 1;
        }

        public boolean hasValue() {
            return (this.bitField0_ & 2) == 2;
        }

        @Override // d0.e0.p.d.m0.i.o
        public final boolean isInitialized() {
            byte b2 = this.memoizedIsInitialized;
            if (b2 == 1) {
                return true;
            }
            if (b2 == 0) {
                return false;
            }
            if (!hasNameId()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            } else if (!hasValue()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            } else if (!getValue().isInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            } else {
                this.memoizedIsInitialized = (byte) 1;
                return true;
            }
        }

        @Override // d0.e0.p.d.m0.i.n
        public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
            getSerializedSize();
            if ((this.bitField0_ & 1) == 1) {
                codedOutputStream.writeInt32(1, this.nameId_);
            }
            if ((this.bitField0_ & 2) == 2) {
                codedOutputStream.writeMessage(2, this.value_);
            }
            codedOutputStream.writeRawBytes(this.unknownFields);
        }

        public static C0326b newBuilder() {
            return new C0326b();
        }

        @Override // d0.e0.p.d.m0.i.n
        public C0326b newBuilderForType() {
            return newBuilder();
        }

        @Override // d0.e0.p.d.m0.i.n
        public C0326b toBuilder() {
            return newBuilder(this);
        }

        public C0325b() {
            this.memoizedIsInitialized = (byte) -1;
            this.memoizedSerializedSize = -1;
            this.unknownFields = d0.e0.p.d.m0.i.c.j;
        }

        public C0325b(d dVar, e eVar, d0.e0.p.d.m0.f.a aVar) throws InvalidProtocolBufferException {
            this.memoizedIsInitialized = (byte) -1;
            this.memoizedSerializedSize = -1;
            boolean z2 = false;
            this.nameId_ = 0;
            this.value_ = c.getDefaultInstance();
            c.b newOutput = d0.e0.p.d.m0.i.c.newOutput();
            CodedOutputStream newInstance = CodedOutputStream.newInstance(newOutput, 1);
            while (!z2) {
                try {
                    try {
                        int readTag = dVar.readTag();
                        if (readTag != 0) {
                            if (readTag == 8) {
                                this.bitField0_ |= 1;
                                this.nameId_ = dVar.readInt32();
                            } else if (readTag == 18) {
                                c.C0327b builder = (this.bitField0_ & 2) == 2 ? this.value_.toBuilder() : null;
                                c cVar = (c) dVar.readMessage(c.k, eVar);
                                this.value_ = cVar;
                                if (builder != null) {
                                    builder.mergeFrom(cVar);
                                    this.value_ = builder.buildPartial();
                                }
                                this.bitField0_ |= 2;
                            } else if (!dVar.skipField(readTag, newInstance)) {
                            }
                        }
                        z2 = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    } catch (IOException e2) {
                        throw new InvalidProtocolBufferException(e2.getMessage()).setUnfinishedMessage(this);
                    }
                } catch (Throwable th) {
                    try {
                        newInstance.flush();
                    } catch (IOException unused) {
                    } catch (Throwable th2) {
                        this.unknownFields = newOutput.toByteString();
                        throw th2;
                    }
                    this.unknownFields = newOutput.toByteString();
                    throw th;
                }
            }
            try {
                newInstance.flush();
            } catch (IOException unused2) {
            } catch (Throwable th3) {
                this.unknownFields = newOutput.toByteString();
                throw th3;
            }
            this.unknownFields = newOutput.toByteString();
        }
    }

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static final class c extends g.b<b, c> implements o {
        public int k;
        public int l;
        public List<C0325b> m = Collections.emptyList();

        public b buildPartial() {
            b bVar = new b(this, null);
            int i = 1;
            if ((this.k & 1) != 1) {
                i = 0;
            }
            bVar.id_ = this.l;
            if ((this.k & 2) == 2) {
                this.m = Collections.unmodifiableList(this.m);
                this.k &= -3;
            }
            bVar.argument_ = this.m;
            bVar.bitField0_ = i;
            return bVar;
        }

        public c setId(int i) {
            this.k |= 1;
            this.l = i;
            return this;
        }

        @Override // d0.e0.p.d.m0.i.n.a
        public b build() {
            b buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw new UninitializedMessageException(buildPartial);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // d0.e0.p.d.m0.i.g.b
        public c clone() {
            return new c().mergeFrom(buildPartial());
        }

        public c mergeFrom(b bVar) {
            if (bVar == b.getDefaultInstance()) {
                return this;
            }
            if (bVar.hasId()) {
                setId(bVar.getId());
            }
            if (!bVar.argument_.isEmpty()) {
                if (this.m.isEmpty()) {
                    this.m = bVar.argument_;
                    this.k &= -3;
                } else {
                    if ((this.k & 2) != 2) {
                        this.m = new ArrayList(this.m);
                        this.k |= 2;
                    }
                    this.m.addAll(bVar.argument_);
                }
            }
            setUnknownFields(getUnknownFields().concat(bVar.unknownFields));
            return this;
        }

        /* JADX WARN: Removed duplicated region for block: B:14:0x001d  */
        @Override // d0.e0.p.d.m0.i.a.AbstractC0335a, d0.e0.p.d.m0.i.n.a
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public d0.e0.p.d.m0.f.b.c mergeFrom(d0.e0.p.d.m0.i.d r3, d0.e0.p.d.m0.i.e r4) throws java.io.IOException {
            /*
                r2 = this;
                r0 = 0
                d0.e0.p.d.m0.i.p<d0.e0.p.d.m0.f.b> r1 = d0.e0.p.d.m0.f.b.k     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                d0.e0.p.d.m0.f.b r3 = (d0.e0.p.d.m0.f.b) r3     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                if (r3 == 0) goto Le
                r2.mergeFrom(r3)
            Le:
                return r2
            Lf:
                r3 = move-exception
                goto L1b
            L11:
                r3 = move-exception
                d0.e0.p.d.m0.i.n r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                d0.e0.p.d.m0.f.b r4 = (d0.e0.p.d.m0.f.b) r4     // Catch: java.lang.Throwable -> Lf
                throw r3     // Catch: java.lang.Throwable -> L19
            L19:
                r3 = move-exception
                r0 = r4
            L1b:
                if (r0 == 0) goto L20
                r2.mergeFrom(r0)
            L20:
                throw r3
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.f.b.c.mergeFrom(d0.e0.p.d.m0.i.d, d0.e0.p.d.m0.i.e):d0.e0.p.d.m0.f.b$c");
        }
    }

    static {
        b bVar = new b();
        j = bVar;
        bVar.id_ = 0;
        bVar.argument_ = Collections.emptyList();
    }

    public b(g.b bVar, d0.e0.p.d.m0.f.a aVar) {
        super(bVar);
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = bVar.getUnknownFields();
    }

    public static b getDefaultInstance() {
        return j;
    }

    public static c newBuilder(b bVar) {
        return newBuilder().mergeFrom(bVar);
    }

    public C0325b getArgument(int i) {
        return this.argument_.get(i);
    }

    public int getArgumentCount() {
        return this.argument_.size();
    }

    public List<C0325b> getArgumentList() {
        return this.argument_;
    }

    public int getId() {
        return this.id_;
    }

    @Override // d0.e0.p.d.m0.i.n
    public int getSerializedSize() {
        int i = this.memoizedSerializedSize;
        if (i != -1) {
            return i;
        }
        int computeInt32Size = (this.bitField0_ & 1) == 1 ? CodedOutputStream.computeInt32Size(1, this.id_) + 0 : 0;
        for (int i2 = 0; i2 < this.argument_.size(); i2++) {
            computeInt32Size += CodedOutputStream.computeMessageSize(2, this.argument_.get(i2));
        }
        int size = this.unknownFields.size() + computeInt32Size;
        this.memoizedSerializedSize = size;
        return size;
    }

    public boolean hasId() {
        return (this.bitField0_ & 1) == 1;
    }

    @Override // d0.e0.p.d.m0.i.o
    public final boolean isInitialized() {
        byte b2 = this.memoizedIsInitialized;
        if (b2 == 1) {
            return true;
        }
        if (b2 == 0) {
            return false;
        }
        if (!hasId()) {
            this.memoizedIsInitialized = (byte) 0;
            return false;
        }
        for (int i = 0; i < getArgumentCount(); i++) {
            if (!getArgument(i).isInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
        }
        this.memoizedIsInitialized = (byte) 1;
        return true;
    }

    @Override // d0.e0.p.d.m0.i.n
    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        getSerializedSize();
        if ((this.bitField0_ & 1) == 1) {
            codedOutputStream.writeInt32(1, this.id_);
        }
        for (int i = 0; i < this.argument_.size(); i++) {
            codedOutputStream.writeMessage(2, this.argument_.get(i));
        }
        codedOutputStream.writeRawBytes(this.unknownFields);
    }

    public static c newBuilder() {
        return new c();
    }

    @Override // d0.e0.p.d.m0.i.n
    public c newBuilderForType() {
        return newBuilder();
    }

    @Override // d0.e0.p.d.m0.i.n
    public c toBuilder() {
        return newBuilder(this);
    }

    public b() {
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = d0.e0.p.d.m0.i.c.j;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public b(d dVar, e eVar, d0.e0.p.d.m0.f.a aVar) throws InvalidProtocolBufferException {
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        boolean z2 = false;
        this.id_ = 0;
        this.argument_ = Collections.emptyList();
        CodedOutputStream newInstance = CodedOutputStream.newInstance(d0.e0.p.d.m0.i.c.newOutput(), 1);
        boolean z3 = false;
        while (!z2) {
            try {
                try {
                    try {
                        int readTag = dVar.readTag();
                        if (readTag != 0) {
                            if (readTag == 8) {
                                this.bitField0_ |= 1;
                                this.id_ = dVar.readInt32();
                            } else if (readTag == 18) {
                                if (!(z3 & true)) {
                                    this.argument_ = new ArrayList();
                                    z3 |= true;
                                }
                                this.argument_.add(dVar.readMessage(C0325b.k, eVar));
                            } else if (!dVar.skipField(readTag, newInstance)) {
                            }
                        }
                        z2 = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    }
                } catch (IOException e2) {
                    throw new InvalidProtocolBufferException(e2.getMessage()).setUnfinishedMessage(this);
                }
            } catch (Throwable th) {
                if (z3 & true) {
                    this.argument_ = Collections.unmodifiableList(this.argument_);
                }
                try {
                    newInstance.flush();
                } catch (IOException unused) {
                    throw th;
                } finally {
                }
            }
        }
        if (z3 & true) {
            this.argument_ = Collections.unmodifiableList(this.argument_);
        }
        try {
            newInstance.flush();
        } catch (IOException unused2) {
        } finally {
        }
    }
}
