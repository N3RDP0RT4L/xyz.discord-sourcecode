package d0.e0.p.d.m0.f;

import d0.e0.p.d.m0.f.l;
import d0.e0.p.d.m0.f.o;
import d0.e0.p.d.m0.f.p;
import d0.e0.p.d.m0.i.c;
import d0.e0.p.d.m0.i.d;
import d0.e0.p.d.m0.i.e;
import d0.e0.p.d.m0.i.g;
import d0.e0.p.d.m0.i.o;
import d0.e0.p.d.m0.i.p;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import kotlin.reflect.jvm.internal.impl.protobuf.CodedOutputStream;
import kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException;
import kotlin.reflect.jvm.internal.impl.protobuf.UninitializedMessageException;
/* compiled from: ProtoBuf.java */
/* loaded from: classes3.dex */
public final class m extends g.d<m> implements o {
    public static final m j;
    public static p<m> k = new a();
    private int bitField0_;
    private List<c> class__;
    private byte memoizedIsInitialized;
    private int memoizedSerializedSize;
    private l package_;
    private o qualifiedNames_;
    private p strings_;
    private final c unknownFields;

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static class a extends d0.e0.p.d.m0.i.b<m> {
        @Override // d0.e0.p.d.m0.i.p
        public m parsePartialFrom(d dVar, e eVar) throws InvalidProtocolBufferException {
            return new m(dVar, eVar, null);
        }
    }

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static final class b extends g.c<m, b> implements o {
        public int m;
        public p n = p.getDefaultInstance();
        public o o = o.getDefaultInstance();
        public l p = l.getDefaultInstance();
        public List<c> q = Collections.emptyList();

        public m buildPartial() {
            m mVar = new m(this, null);
            int i = this.m;
            int i2 = 1;
            if ((i & 1) != 1) {
                i2 = 0;
            }
            mVar.strings_ = this.n;
            if ((i & 2) == 2) {
                i2 |= 2;
            }
            mVar.qualifiedNames_ = this.o;
            if ((i & 4) == 4) {
                i2 |= 4;
            }
            mVar.package_ = this.p;
            if ((this.m & 8) == 8) {
                this.q = Collections.unmodifiableList(this.q);
                this.m &= -9;
            }
            mVar.class__ = this.q;
            mVar.bitField0_ = i2;
            return mVar;
        }

        public b mergePackage(l lVar) {
            if ((this.m & 4) != 4 || this.p == l.getDefaultInstance()) {
                this.p = lVar;
            } else {
                this.p = l.newBuilder(this.p).mergeFrom(lVar).buildPartial();
            }
            this.m |= 4;
            return this;
        }

        public b mergeQualifiedNames(o oVar) {
            if ((this.m & 2) != 2 || this.o == o.getDefaultInstance()) {
                this.o = oVar;
            } else {
                this.o = o.newBuilder(this.o).mergeFrom(oVar).buildPartial();
            }
            this.m |= 2;
            return this;
        }

        public b mergeStrings(p pVar) {
            if ((this.m & 1) != 1 || this.n == p.getDefaultInstance()) {
                this.n = pVar;
            } else {
                this.n = p.newBuilder(this.n).mergeFrom(pVar).buildPartial();
            }
            this.m |= 1;
            return this;
        }

        @Override // d0.e0.p.d.m0.i.n.a
        public m build() {
            m buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw new UninitializedMessageException(buildPartial);
        }

        @Override // d0.e0.p.d.m0.i.g.b
        public b clone() {
            return new b().mergeFrom(buildPartial());
        }

        public b mergeFrom(m mVar) {
            if (mVar == m.getDefaultInstance()) {
                return this;
            }
            if (mVar.hasStrings()) {
                mergeStrings(mVar.getStrings());
            }
            if (mVar.hasQualifiedNames()) {
                mergeQualifiedNames(mVar.getQualifiedNames());
            }
            if (mVar.hasPackage()) {
                mergePackage(mVar.getPackage());
            }
            if (!mVar.class__.isEmpty()) {
                if (this.q.isEmpty()) {
                    this.q = mVar.class__;
                    this.m &= -9;
                } else {
                    if ((this.m & 8) != 8) {
                        this.q = new ArrayList(this.q);
                        this.m |= 8;
                    }
                    this.q.addAll(mVar.class__);
                }
            }
            b(mVar);
            setUnknownFields(getUnknownFields().concat(mVar.unknownFields));
            return this;
        }

        /* JADX WARN: Removed duplicated region for block: B:14:0x001d  */
        @Override // d0.e0.p.d.m0.i.a.AbstractC0335a, d0.e0.p.d.m0.i.n.a
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public d0.e0.p.d.m0.f.m.b mergeFrom(d0.e0.p.d.m0.i.d r3, d0.e0.p.d.m0.i.e r4) throws java.io.IOException {
            /*
                r2 = this;
                r0 = 0
                d0.e0.p.d.m0.i.p<d0.e0.p.d.m0.f.m> r1 = d0.e0.p.d.m0.f.m.k     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                d0.e0.p.d.m0.f.m r3 = (d0.e0.p.d.m0.f.m) r3     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                if (r3 == 0) goto Le
                r2.mergeFrom(r3)
            Le:
                return r2
            Lf:
                r3 = move-exception
                goto L1b
            L11:
                r3 = move-exception
                d0.e0.p.d.m0.i.n r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                d0.e0.p.d.m0.f.m r4 = (d0.e0.p.d.m0.f.m) r4     // Catch: java.lang.Throwable -> Lf
                throw r3     // Catch: java.lang.Throwable -> L19
            L19:
                r3 = move-exception
                r0 = r4
            L1b:
                if (r0 == 0) goto L20
                r2.mergeFrom(r0)
            L20:
                throw r3
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.f.m.b.mergeFrom(d0.e0.p.d.m0.i.d, d0.e0.p.d.m0.i.e):d0.e0.p.d.m0.f.m$b");
        }
    }

    static {
        m mVar = new m();
        j = mVar;
        mVar.o();
    }

    public m(g.c cVar, d0.e0.p.d.m0.f.a aVar) {
        super(cVar);
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = cVar.getUnknownFields();
    }

    public static m getDefaultInstance() {
        return j;
    }

    public static b newBuilder(m mVar) {
        return newBuilder().mergeFrom(mVar);
    }

    public static m parseFrom(InputStream inputStream, e eVar) throws IOException {
        return (m) ((d0.e0.p.d.m0.i.b) k).parseFrom(inputStream, eVar);
    }

    public c getClass_(int i) {
        return this.class__.get(i);
    }

    public int getClass_Count() {
        return this.class__.size();
    }

    public List<c> getClass_List() {
        return this.class__;
    }

    public l getPackage() {
        return this.package_;
    }

    public o getQualifiedNames() {
        return this.qualifiedNames_;
    }

    @Override // d0.e0.p.d.m0.i.n
    public int getSerializedSize() {
        int i = this.memoizedSerializedSize;
        if (i != -1) {
            return i;
        }
        int computeMessageSize = (this.bitField0_ & 1) == 1 ? CodedOutputStream.computeMessageSize(1, this.strings_) + 0 : 0;
        if ((this.bitField0_ & 2) == 2) {
            computeMessageSize += CodedOutputStream.computeMessageSize(2, this.qualifiedNames_);
        }
        if ((this.bitField0_ & 4) == 4) {
            computeMessageSize += CodedOutputStream.computeMessageSize(3, this.package_);
        }
        for (int i2 = 0; i2 < this.class__.size(); i2++) {
            computeMessageSize += CodedOutputStream.computeMessageSize(4, this.class__.get(i2));
        }
        int size = this.unknownFields.size() + c() + computeMessageSize;
        this.memoizedSerializedSize = size;
        return size;
    }

    public p getStrings() {
        return this.strings_;
    }

    public boolean hasPackage() {
        return (this.bitField0_ & 4) == 4;
    }

    public boolean hasQualifiedNames() {
        return (this.bitField0_ & 2) == 2;
    }

    public boolean hasStrings() {
        return (this.bitField0_ & 1) == 1;
    }

    @Override // d0.e0.p.d.m0.i.o
    public final boolean isInitialized() {
        byte b2 = this.memoizedIsInitialized;
        if (b2 == 1) {
            return true;
        }
        if (b2 == 0) {
            return false;
        }
        if (hasQualifiedNames() && !getQualifiedNames().isInitialized()) {
            this.memoizedIsInitialized = (byte) 0;
            return false;
        } else if (!hasPackage() || getPackage().isInitialized()) {
            for (int i = 0; i < getClass_Count(); i++) {
                if (!getClass_(i).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            if (!b()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        } else {
            this.memoizedIsInitialized = (byte) 0;
            return false;
        }
    }

    public final void o() {
        this.strings_ = p.getDefaultInstance();
        this.qualifiedNames_ = o.getDefaultInstance();
        this.package_ = l.getDefaultInstance();
        this.class__ = Collections.emptyList();
    }

    @Override // d0.e0.p.d.m0.i.n
    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        getSerializedSize();
        g.d<MessageType>.a e = e();
        if ((this.bitField0_ & 1) == 1) {
            codedOutputStream.writeMessage(1, this.strings_);
        }
        if ((this.bitField0_ & 2) == 2) {
            codedOutputStream.writeMessage(2, this.qualifiedNames_);
        }
        if ((this.bitField0_ & 4) == 4) {
            codedOutputStream.writeMessage(3, this.package_);
        }
        for (int i = 0; i < this.class__.size(); i++) {
            codedOutputStream.writeMessage(4, this.class__.get(i));
        }
        e.writeUntil(200, codedOutputStream);
        codedOutputStream.writeRawBytes(this.unknownFields);
    }

    public static b newBuilder() {
        return new b();
    }

    @Override // d0.e0.p.d.m0.i.o
    public m getDefaultInstanceForType() {
        return j;
    }

    @Override // d0.e0.p.d.m0.i.n
    public b newBuilderForType() {
        return newBuilder();
    }

    @Override // d0.e0.p.d.m0.i.n
    public b toBuilder() {
        return newBuilder(this);
    }

    public m() {
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = c.j;
    }

    /* JADX WARN: Finally extract failed */
    /* JADX WARN: Multi-variable type inference failed */
    public m(d dVar, e eVar, d0.e0.p.d.m0.f.a aVar) throws InvalidProtocolBufferException {
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        o();
        c.b newOutput = c.newOutput();
        CodedOutputStream newInstance = CodedOutputStream.newInstance(newOutput, 1);
        boolean z2 = false;
        boolean z3 = false;
        while (!z2) {
            try {
                try {
                    try {
                        int readTag = dVar.readTag();
                        if (readTag != 0) {
                            l.b bVar = null;
                            p.b builder = null;
                            o.b builder2 = null;
                            if (readTag == 10) {
                                builder = (this.bitField0_ & 1) == 1 ? this.strings_.toBuilder() : builder;
                                p pVar = (p) dVar.readMessage(p.k, eVar);
                                this.strings_ = pVar;
                                if (builder != null) {
                                    builder.mergeFrom(pVar);
                                    this.strings_ = builder.buildPartial();
                                }
                                this.bitField0_ |= 1;
                            } else if (readTag == 18) {
                                builder2 = (this.bitField0_ & 2) == 2 ? this.qualifiedNames_.toBuilder() : builder2;
                                o oVar = (o) dVar.readMessage(o.k, eVar);
                                this.qualifiedNames_ = oVar;
                                if (builder2 != null) {
                                    builder2.mergeFrom(oVar);
                                    this.qualifiedNames_ = builder2.buildPartial();
                                }
                                this.bitField0_ |= 2;
                            } else if (readTag == 26) {
                                bVar = (this.bitField0_ & 4) == 4 ? this.package_.toBuilder() : bVar;
                                l lVar = (l) dVar.readMessage(l.k, eVar);
                                this.package_ = lVar;
                                if (bVar != null) {
                                    bVar.mergeFrom(lVar);
                                    this.package_ = bVar.buildPartial();
                                }
                                this.bitField0_ |= 4;
                            } else if (readTag == 34) {
                                if (!(z3 & true)) {
                                    this.class__ = new ArrayList();
                                    z3 |= true;
                                }
                                this.class__.add(dVar.readMessage(c.k, eVar));
                            } else if (!f(dVar, newInstance, eVar, readTag)) {
                            }
                        }
                        z2 = true;
                    } catch (IOException e) {
                        throw new InvalidProtocolBufferException(e.getMessage()).setUnfinishedMessage(this);
                    }
                } catch (InvalidProtocolBufferException e2) {
                    throw e2.setUnfinishedMessage(this);
                }
            } catch (Throwable th) {
                if (z3 & true) {
                    this.class__ = Collections.unmodifiableList(this.class__);
                }
                try {
                    newInstance.flush();
                } catch (IOException unused) {
                    this.unknownFields = newOutput.toByteString();
                    d();
                    throw th;
                } catch (Throwable th2) {
                    this.unknownFields = newOutput.toByteString();
                    throw th2;
                }
            }
        }
        if (z3 & true) {
            this.class__ = Collections.unmodifiableList(this.class__);
        }
        try {
            newInstance.flush();
        } catch (IOException unused2) {
            this.unknownFields = newOutput.toByteString();
            d();
        } catch (Throwable th3) {
            this.unknownFields = newOutput.toByteString();
            throw th3;
        }
    }
}
