package d0.e0.p.d.m0.f;

import d0.e0.p.d.m0.i.c;
import d0.e0.p.d.m0.i.d;
import d0.e0.p.d.m0.i.e;
import d0.e0.p.d.m0.i.g;
import d0.e0.p.d.m0.i.h;
import d0.e0.p.d.m0.i.o;
import d0.e0.p.d.m0.i.p;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import kotlin.reflect.jvm.internal.impl.protobuf.CodedOutputStream;
import kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException;
import kotlin.reflect.jvm.internal.impl.protobuf.UninitializedMessageException;
/* compiled from: ProtoBuf.java */
/* loaded from: classes3.dex */
public final class s extends g.d<s> implements o {
    public static final s j;
    public static p<s> k = new a();
    private int bitField0_;
    private int id_;
    private byte memoizedIsInitialized;
    private int memoizedSerializedSize;
    private int name_;
    private boolean reified_;
    private final d0.e0.p.d.m0.i.c unknownFields;
    private int upperBoundIdMemoizedSerializedSize;
    private List<Integer> upperBoundId_;
    private List<q> upperBound_;
    private c variance_;

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static class a extends d0.e0.p.d.m0.i.b<s> {
        @Override // d0.e0.p.d.m0.i.p
        public s parsePartialFrom(d dVar, e eVar) throws InvalidProtocolBufferException {
            return new s(dVar, eVar, null);
        }
    }

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static final class b extends g.c<s, b> implements o {
        public int m;
        public int n;
        public int o;
        public boolean p;
        public c q = c.INV;
        public List<q> r = Collections.emptyList();

        /* renamed from: s  reason: collision with root package name */
        public List<Integer> f3380s = Collections.emptyList();

        public s buildPartial() {
            s sVar = new s(this, null);
            int i = this.m;
            int i2 = 1;
            if ((i & 1) != 1) {
                i2 = 0;
            }
            sVar.id_ = this.n;
            if ((i & 2) == 2) {
                i2 |= 2;
            }
            sVar.name_ = this.o;
            if ((i & 4) == 4) {
                i2 |= 4;
            }
            sVar.reified_ = this.p;
            if ((i & 8) == 8) {
                i2 |= 8;
            }
            sVar.variance_ = this.q;
            if ((this.m & 16) == 16) {
                this.r = Collections.unmodifiableList(this.r);
                this.m &= -17;
            }
            sVar.upperBound_ = this.r;
            if ((this.m & 32) == 32) {
                this.f3380s = Collections.unmodifiableList(this.f3380s);
                this.m &= -33;
            }
            sVar.upperBoundId_ = this.f3380s;
            sVar.bitField0_ = i2;
            return sVar;
        }

        public b setId(int i) {
            this.m |= 1;
            this.n = i;
            return this;
        }

        public b setName(int i) {
            this.m |= 2;
            this.o = i;
            return this;
        }

        public b setReified(boolean z2) {
            this.m |= 4;
            this.p = z2;
            return this;
        }

        public b setVariance(c cVar) {
            Objects.requireNonNull(cVar);
            this.m |= 8;
            this.q = cVar;
            return this;
        }

        @Override // d0.e0.p.d.m0.i.n.a
        public s build() {
            s buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw new UninitializedMessageException(buildPartial);
        }

        @Override // d0.e0.p.d.m0.i.g.b
        public b clone() {
            return new b().mergeFrom(buildPartial());
        }

        public b mergeFrom(s sVar) {
            if (sVar == s.getDefaultInstance()) {
                return this;
            }
            if (sVar.hasId()) {
                setId(sVar.getId());
            }
            if (sVar.hasName()) {
                setName(sVar.getName());
            }
            if (sVar.hasReified()) {
                setReified(sVar.getReified());
            }
            if (sVar.hasVariance()) {
                setVariance(sVar.getVariance());
            }
            if (!sVar.upperBound_.isEmpty()) {
                if (this.r.isEmpty()) {
                    this.r = sVar.upperBound_;
                    this.m &= -17;
                } else {
                    if ((this.m & 16) != 16) {
                        this.r = new ArrayList(this.r);
                        this.m |= 16;
                    }
                    this.r.addAll(sVar.upperBound_);
                }
            }
            if (!sVar.upperBoundId_.isEmpty()) {
                if (this.f3380s.isEmpty()) {
                    this.f3380s = sVar.upperBoundId_;
                    this.m &= -33;
                } else {
                    if ((this.m & 32) != 32) {
                        this.f3380s = new ArrayList(this.f3380s);
                        this.m |= 32;
                    }
                    this.f3380s.addAll(sVar.upperBoundId_);
                }
            }
            b(sVar);
            setUnknownFields(getUnknownFields().concat(sVar.unknownFields));
            return this;
        }

        /* JADX WARN: Removed duplicated region for block: B:14:0x001d  */
        @Override // d0.e0.p.d.m0.i.a.AbstractC0335a, d0.e0.p.d.m0.i.n.a
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public d0.e0.p.d.m0.f.s.b mergeFrom(d0.e0.p.d.m0.i.d r3, d0.e0.p.d.m0.i.e r4) throws java.io.IOException {
            /*
                r2 = this;
                r0 = 0
                d0.e0.p.d.m0.i.p<d0.e0.p.d.m0.f.s> r1 = d0.e0.p.d.m0.f.s.k     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                d0.e0.p.d.m0.f.s r3 = (d0.e0.p.d.m0.f.s) r3     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                if (r3 == 0) goto Le
                r2.mergeFrom(r3)
            Le:
                return r2
            Lf:
                r3 = move-exception
                goto L1b
            L11:
                r3 = move-exception
                d0.e0.p.d.m0.i.n r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                d0.e0.p.d.m0.f.s r4 = (d0.e0.p.d.m0.f.s) r4     // Catch: java.lang.Throwable -> Lf
                throw r3     // Catch: java.lang.Throwable -> L19
            L19:
                r3 = move-exception
                r0 = r4
            L1b:
                if (r0 == 0) goto L20
                r2.mergeFrom(r0)
            L20:
                throw r3
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.f.s.b.mergeFrom(d0.e0.p.d.m0.i.d, d0.e0.p.d.m0.i.e):d0.e0.p.d.m0.f.s$b");
        }
    }

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public enum c implements h.a {
        IN(0),
        OUT(1),
        INV(2);
        
        private final int value;

        c(int i) {
            this.value = i;
        }

        @Override // d0.e0.p.d.m0.i.h.a
        public final int getNumber() {
            return this.value;
        }

        public static c valueOf(int i) {
            if (i == 0) {
                return IN;
            }
            if (i == 1) {
                return OUT;
            }
            if (i != 2) {
                return null;
            }
            return INV;
        }
    }

    static {
        s sVar = new s();
        j = sVar;
        sVar.r();
    }

    public s(g.c cVar, d0.e0.p.d.m0.f.a aVar) {
        super(cVar);
        this.upperBoundIdMemoizedSerializedSize = -1;
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = cVar.getUnknownFields();
    }

    public static s getDefaultInstance() {
        return j;
    }

    public static b newBuilder(s sVar) {
        return newBuilder().mergeFrom(sVar);
    }

    public int getId() {
        return this.id_;
    }

    public int getName() {
        return this.name_;
    }

    public boolean getReified() {
        return this.reified_;
    }

    @Override // d0.e0.p.d.m0.i.n
    public int getSerializedSize() {
        int i = this.memoizedSerializedSize;
        if (i != -1) {
            return i;
        }
        int computeInt32Size = (this.bitField0_ & 1) == 1 ? CodedOutputStream.computeInt32Size(1, this.id_) + 0 : 0;
        if ((this.bitField0_ & 2) == 2) {
            computeInt32Size += CodedOutputStream.computeInt32Size(2, this.name_);
        }
        if ((this.bitField0_ & 4) == 4) {
            computeInt32Size += CodedOutputStream.computeBoolSize(3, this.reified_);
        }
        if ((this.bitField0_ & 8) == 8) {
            computeInt32Size += CodedOutputStream.computeEnumSize(4, this.variance_.getNumber());
        }
        for (int i2 = 0; i2 < this.upperBound_.size(); i2++) {
            computeInt32Size += CodedOutputStream.computeMessageSize(5, this.upperBound_.get(i2));
        }
        int i3 = 0;
        for (int i4 = 0; i4 < this.upperBoundId_.size(); i4++) {
            i3 += CodedOutputStream.computeInt32SizeNoTag(this.upperBoundId_.get(i4).intValue());
        }
        int i5 = computeInt32Size + i3;
        if (!getUpperBoundIdList().isEmpty()) {
            i5 = i5 + 1 + CodedOutputStream.computeInt32SizeNoTag(i3);
        }
        this.upperBoundIdMemoizedSerializedSize = i3;
        int size = this.unknownFields.size() + c() + i5;
        this.memoizedSerializedSize = size;
        return size;
    }

    public q getUpperBound(int i) {
        return this.upperBound_.get(i);
    }

    public int getUpperBoundCount() {
        return this.upperBound_.size();
    }

    public List<Integer> getUpperBoundIdList() {
        return this.upperBoundId_;
    }

    public List<q> getUpperBoundList() {
        return this.upperBound_;
    }

    public c getVariance() {
        return this.variance_;
    }

    public boolean hasId() {
        return (this.bitField0_ & 1) == 1;
    }

    public boolean hasName() {
        return (this.bitField0_ & 2) == 2;
    }

    public boolean hasReified() {
        return (this.bitField0_ & 4) == 4;
    }

    public boolean hasVariance() {
        return (this.bitField0_ & 8) == 8;
    }

    @Override // d0.e0.p.d.m0.i.o
    public final boolean isInitialized() {
        byte b2 = this.memoizedIsInitialized;
        if (b2 == 1) {
            return true;
        }
        if (b2 == 0) {
            return false;
        }
        if (!hasId()) {
            this.memoizedIsInitialized = (byte) 0;
            return false;
        } else if (!hasName()) {
            this.memoizedIsInitialized = (byte) 0;
            return false;
        } else {
            for (int i = 0; i < getUpperBoundCount(); i++) {
                if (!getUpperBound(i).isInitialized()) {
                    this.memoizedIsInitialized = (byte) 0;
                    return false;
                }
            }
            if (!b()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }
    }

    public final void r() {
        this.id_ = 0;
        this.name_ = 0;
        this.reified_ = false;
        this.variance_ = c.INV;
        this.upperBound_ = Collections.emptyList();
        this.upperBoundId_ = Collections.emptyList();
    }

    @Override // d0.e0.p.d.m0.i.n
    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        getSerializedSize();
        g.d<MessageType>.a e = e();
        if ((this.bitField0_ & 1) == 1) {
            codedOutputStream.writeInt32(1, this.id_);
        }
        if ((this.bitField0_ & 2) == 2) {
            codedOutputStream.writeInt32(2, this.name_);
        }
        if ((this.bitField0_ & 4) == 4) {
            codedOutputStream.writeBool(3, this.reified_);
        }
        if ((this.bitField0_ & 8) == 8) {
            codedOutputStream.writeEnum(4, this.variance_.getNumber());
        }
        for (int i = 0; i < this.upperBound_.size(); i++) {
            codedOutputStream.writeMessage(5, this.upperBound_.get(i));
        }
        if (getUpperBoundIdList().size() > 0) {
            codedOutputStream.writeRawVarint32(50);
            codedOutputStream.writeRawVarint32(this.upperBoundIdMemoizedSerializedSize);
        }
        for (int i2 = 0; i2 < this.upperBoundId_.size(); i2++) {
            codedOutputStream.writeInt32NoTag(this.upperBoundId_.get(i2).intValue());
        }
        e.writeUntil(1000, codedOutputStream);
        codedOutputStream.writeRawBytes(this.unknownFields);
    }

    public static b newBuilder() {
        return new b();
    }

    @Override // d0.e0.p.d.m0.i.o
    public s getDefaultInstanceForType() {
        return j;
    }

    @Override // d0.e0.p.d.m0.i.n
    public b newBuilderForType() {
        return newBuilder();
    }

    @Override // d0.e0.p.d.m0.i.n
    public b toBuilder() {
        return newBuilder(this);
    }

    public s() {
        this.upperBoundIdMemoizedSerializedSize = -1;
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = d0.e0.p.d.m0.i.c.j;
    }

    /* JADX WARN: Finally extract failed */
    /* JADX WARN: Multi-variable type inference failed */
    public s(d dVar, e eVar, d0.e0.p.d.m0.f.a aVar) throws InvalidProtocolBufferException {
        this.upperBoundIdMemoizedSerializedSize = -1;
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        r();
        c.b newOutput = d0.e0.p.d.m0.i.c.newOutput();
        CodedOutputStream newInstance = CodedOutputStream.newInstance(newOutput, 1);
        boolean z2 = false;
        boolean z3 = false;
        while (!z2) {
            try {
                try {
                    try {
                        int readTag = dVar.readTag();
                        if (readTag != 0) {
                            if (readTag == 8) {
                                this.bitField0_ |= 1;
                                this.id_ = dVar.readInt32();
                            } else if (readTag == 16) {
                                this.bitField0_ |= 2;
                                this.name_ = dVar.readInt32();
                            } else if (readTag == 24) {
                                this.bitField0_ |= 4;
                                this.reified_ = dVar.readBool();
                            } else if (readTag == 32) {
                                int readEnum = dVar.readEnum();
                                c valueOf = c.valueOf(readEnum);
                                if (valueOf == null) {
                                    newInstance.writeRawVarint32(readTag);
                                    newInstance.writeRawVarint32(readEnum);
                                } else {
                                    this.bitField0_ |= 8;
                                    this.variance_ = valueOf;
                                }
                            } else if (readTag == 42) {
                                if (!(z3 & true)) {
                                    this.upperBound_ = new ArrayList();
                                    z3 |= true;
                                }
                                this.upperBound_.add(dVar.readMessage(q.k, eVar));
                            } else if (readTag == 48) {
                                if (!(z3 & true)) {
                                    this.upperBoundId_ = new ArrayList();
                                    z3 |= true;
                                }
                                this.upperBoundId_.add(Integer.valueOf(dVar.readInt32()));
                            } else if (readTag == 50) {
                                int pushLimit = dVar.pushLimit(dVar.readRawVarint32());
                                if (!(z3 & true) && dVar.getBytesUntilLimit() > 0) {
                                    this.upperBoundId_ = new ArrayList();
                                    z3 |= true;
                                }
                                while (dVar.getBytesUntilLimit() > 0) {
                                    this.upperBoundId_.add(Integer.valueOf(dVar.readInt32()));
                                }
                                dVar.popLimit(pushLimit);
                            } else if (!f(dVar, newInstance, eVar, readTag)) {
                            }
                        }
                        z2 = true;
                    } catch (IOException e) {
                        throw new InvalidProtocolBufferException(e.getMessage()).setUnfinishedMessage(this);
                    }
                } catch (InvalidProtocolBufferException e2) {
                    throw e2.setUnfinishedMessage(this);
                }
            } catch (Throwable th) {
                if (z3 & true) {
                    this.upperBound_ = Collections.unmodifiableList(this.upperBound_);
                }
                if (z3 & true) {
                    this.upperBoundId_ = Collections.unmodifiableList(this.upperBoundId_);
                }
                try {
                    newInstance.flush();
                } catch (IOException unused) {
                    this.unknownFields = newOutput.toByteString();
                    d();
                    throw th;
                } catch (Throwable th2) {
                    this.unknownFields = newOutput.toByteString();
                    throw th2;
                }
            }
        }
        if (z3 & true) {
            this.upperBound_ = Collections.unmodifiableList(this.upperBound_);
        }
        if (z3 & true) {
            this.upperBoundId_ = Collections.unmodifiableList(this.upperBoundId_);
        }
        try {
            newInstance.flush();
        } catch (IOException unused2) {
            this.unknownFields = newOutput.toByteString();
            d();
        } catch (Throwable th3) {
            this.unknownFields = newOutput.toByteString();
            throw th3;
        }
    }
}
