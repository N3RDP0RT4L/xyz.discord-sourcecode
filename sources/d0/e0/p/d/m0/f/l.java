package d0.e0.p.d.m0.f;

import d0.e0.p.d.m0.f.t;
import d0.e0.p.d.m0.f.w;
import d0.e0.p.d.m0.i.c;
import d0.e0.p.d.m0.i.d;
import d0.e0.p.d.m0.i.e;
import d0.e0.p.d.m0.i.g;
import d0.e0.p.d.m0.i.o;
import d0.e0.p.d.m0.i.p;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import kotlin.reflect.jvm.internal.impl.protobuf.CodedOutputStream;
import kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException;
import kotlin.reflect.jvm.internal.impl.protobuf.UninitializedMessageException;
/* compiled from: ProtoBuf.java */
/* loaded from: classes3.dex */
public final class l extends g.d<l> implements o {
    public static final l j;
    public static p<l> k = new a();
    private int bitField0_;
    private List<i> function_;
    private byte memoizedIsInitialized;
    private int memoizedSerializedSize;
    private List<n> property_;
    private List<r> typeAlias_;
    private t typeTable_;
    private final c unknownFields;
    private w versionRequirementTable_;

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static class a extends d0.e0.p.d.m0.i.b<l> {
        @Override // d0.e0.p.d.m0.i.p
        public l parsePartialFrom(d dVar, e eVar) throws InvalidProtocolBufferException {
            return new l(dVar, eVar, null);
        }
    }

    /* compiled from: ProtoBuf.java */
    /* loaded from: classes3.dex */
    public static final class b extends g.c<l, b> implements o {
        public int m;
        public List<i> n = Collections.emptyList();
        public List<n> o = Collections.emptyList();
        public List<r> p = Collections.emptyList();
        public t q = t.getDefaultInstance();
        public w r = w.getDefaultInstance();

        public l buildPartial() {
            l lVar = new l(this, null);
            int i = this.m;
            int i2 = 1;
            if ((i & 1) == 1) {
                this.n = Collections.unmodifiableList(this.n);
                this.m &= -2;
            }
            lVar.function_ = this.n;
            if ((this.m & 2) == 2) {
                this.o = Collections.unmodifiableList(this.o);
                this.m &= -3;
            }
            lVar.property_ = this.o;
            if ((this.m & 4) == 4) {
                this.p = Collections.unmodifiableList(this.p);
                this.m &= -5;
            }
            lVar.typeAlias_ = this.p;
            if ((i & 8) != 8) {
                i2 = 0;
            }
            lVar.typeTable_ = this.q;
            if ((i & 16) == 16) {
                i2 |= 2;
            }
            lVar.versionRequirementTable_ = this.r;
            lVar.bitField0_ = i2;
            return lVar;
        }

        public b mergeTypeTable(t tVar) {
            if ((this.m & 8) != 8 || this.q == t.getDefaultInstance()) {
                this.q = tVar;
            } else {
                this.q = t.newBuilder(this.q).mergeFrom(tVar).buildPartial();
            }
            this.m |= 8;
            return this;
        }

        public b mergeVersionRequirementTable(w wVar) {
            if ((this.m & 16) != 16 || this.r == w.getDefaultInstance()) {
                this.r = wVar;
            } else {
                this.r = w.newBuilder(this.r).mergeFrom(wVar).buildPartial();
            }
            this.m |= 16;
            return this;
        }

        @Override // d0.e0.p.d.m0.i.n.a
        public l build() {
            l buildPartial = buildPartial();
            if (buildPartial.isInitialized()) {
                return buildPartial;
            }
            throw new UninitializedMessageException(buildPartial);
        }

        @Override // d0.e0.p.d.m0.i.g.b
        public b clone() {
            return new b().mergeFrom(buildPartial());
        }

        public b mergeFrom(l lVar) {
            if (lVar == l.getDefaultInstance()) {
                return this;
            }
            if (!lVar.function_.isEmpty()) {
                if (this.n.isEmpty()) {
                    this.n = lVar.function_;
                    this.m &= -2;
                } else {
                    if ((this.m & 1) != 1) {
                        this.n = new ArrayList(this.n);
                        this.m |= 1;
                    }
                    this.n.addAll(lVar.function_);
                }
            }
            if (!lVar.property_.isEmpty()) {
                if (this.o.isEmpty()) {
                    this.o = lVar.property_;
                    this.m &= -3;
                } else {
                    if ((this.m & 2) != 2) {
                        this.o = new ArrayList(this.o);
                        this.m |= 2;
                    }
                    this.o.addAll(lVar.property_);
                }
            }
            if (!lVar.typeAlias_.isEmpty()) {
                if (this.p.isEmpty()) {
                    this.p = lVar.typeAlias_;
                    this.m &= -5;
                } else {
                    if ((this.m & 4) != 4) {
                        this.p = new ArrayList(this.p);
                        this.m |= 4;
                    }
                    this.p.addAll(lVar.typeAlias_);
                }
            }
            if (lVar.hasTypeTable()) {
                mergeTypeTable(lVar.getTypeTable());
            }
            if (lVar.hasVersionRequirementTable()) {
                mergeVersionRequirementTable(lVar.getVersionRequirementTable());
            }
            b(lVar);
            setUnknownFields(getUnknownFields().concat(lVar.unknownFields));
            return this;
        }

        /* JADX WARN: Removed duplicated region for block: B:14:0x001d  */
        @Override // d0.e0.p.d.m0.i.a.AbstractC0335a, d0.e0.p.d.m0.i.n.a
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public d0.e0.p.d.m0.f.l.b mergeFrom(d0.e0.p.d.m0.i.d r3, d0.e0.p.d.m0.i.e r4) throws java.io.IOException {
            /*
                r2 = this;
                r0 = 0
                d0.e0.p.d.m0.i.p<d0.e0.p.d.m0.f.l> r1 = d0.e0.p.d.m0.f.l.k     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                java.lang.Object r3 = r1.parsePartialFrom(r3, r4)     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                d0.e0.p.d.m0.f.l r3 = (d0.e0.p.d.m0.f.l) r3     // Catch: java.lang.Throwable -> Lf kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException -> L11
                if (r3 == 0) goto Le
                r2.mergeFrom(r3)
            Le:
                return r2
            Lf:
                r3 = move-exception
                goto L1b
            L11:
                r3 = move-exception
                d0.e0.p.d.m0.i.n r4 = r3.getUnfinishedMessage()     // Catch: java.lang.Throwable -> Lf
                d0.e0.p.d.m0.f.l r4 = (d0.e0.p.d.m0.f.l) r4     // Catch: java.lang.Throwable -> Lf
                throw r3     // Catch: java.lang.Throwable -> L19
            L19:
                r3 = move-exception
                r0 = r4
            L1b:
                if (r0 == 0) goto L20
                r2.mergeFrom(r0)
            L20:
                throw r3
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.f.l.b.mergeFrom(d0.e0.p.d.m0.i.d, d0.e0.p.d.m0.i.e):d0.e0.p.d.m0.f.l$b");
        }
    }

    static {
        l lVar = new l();
        j = lVar;
        lVar.r();
    }

    public l(g.c cVar, d0.e0.p.d.m0.f.a aVar) {
        super(cVar);
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = cVar.getUnknownFields();
    }

    public static l getDefaultInstance() {
        return j;
    }

    public static b newBuilder(l lVar) {
        return newBuilder().mergeFrom(lVar);
    }

    public static l parseFrom(InputStream inputStream, e eVar) throws IOException {
        return (l) ((d0.e0.p.d.m0.i.b) k).parseFrom(inputStream, eVar);
    }

    public i getFunction(int i) {
        return this.function_.get(i);
    }

    public int getFunctionCount() {
        return this.function_.size();
    }

    public List<i> getFunctionList() {
        return this.function_;
    }

    public n getProperty(int i) {
        return this.property_.get(i);
    }

    public int getPropertyCount() {
        return this.property_.size();
    }

    public List<n> getPropertyList() {
        return this.property_;
    }

    @Override // d0.e0.p.d.m0.i.n
    public int getSerializedSize() {
        int i = this.memoizedSerializedSize;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.function_.size(); i3++) {
            i2 += CodedOutputStream.computeMessageSize(3, this.function_.get(i3));
        }
        for (int i4 = 0; i4 < this.property_.size(); i4++) {
            i2 += CodedOutputStream.computeMessageSize(4, this.property_.get(i4));
        }
        for (int i5 = 0; i5 < this.typeAlias_.size(); i5++) {
            i2 += CodedOutputStream.computeMessageSize(5, this.typeAlias_.get(i5));
        }
        if ((this.bitField0_ & 1) == 1) {
            i2 += CodedOutputStream.computeMessageSize(30, this.typeTable_);
        }
        if ((this.bitField0_ & 2) == 2) {
            i2 += CodedOutputStream.computeMessageSize(32, this.versionRequirementTable_);
        }
        int size = this.unknownFields.size() + c() + i2;
        this.memoizedSerializedSize = size;
        return size;
    }

    public r getTypeAlias(int i) {
        return this.typeAlias_.get(i);
    }

    public int getTypeAliasCount() {
        return this.typeAlias_.size();
    }

    public List<r> getTypeAliasList() {
        return this.typeAlias_;
    }

    public t getTypeTable() {
        return this.typeTable_;
    }

    public w getVersionRequirementTable() {
        return this.versionRequirementTable_;
    }

    public boolean hasTypeTable() {
        return (this.bitField0_ & 1) == 1;
    }

    public boolean hasVersionRequirementTable() {
        return (this.bitField0_ & 2) == 2;
    }

    @Override // d0.e0.p.d.m0.i.o
    public final boolean isInitialized() {
        byte b2 = this.memoizedIsInitialized;
        if (b2 == 1) {
            return true;
        }
        if (b2 == 0) {
            return false;
        }
        for (int i = 0; i < getFunctionCount(); i++) {
            if (!getFunction(i).isInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
        }
        for (int i2 = 0; i2 < getPropertyCount(); i2++) {
            if (!getProperty(i2).isInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
        }
        for (int i3 = 0; i3 < getTypeAliasCount(); i3++) {
            if (!getTypeAlias(i3).isInitialized()) {
                this.memoizedIsInitialized = (byte) 0;
                return false;
            }
        }
        if (hasTypeTable() && !getTypeTable().isInitialized()) {
            this.memoizedIsInitialized = (byte) 0;
            return false;
        } else if (!b()) {
            this.memoizedIsInitialized = (byte) 0;
            return false;
        } else {
            this.memoizedIsInitialized = (byte) 1;
            return true;
        }
    }

    public final void r() {
        this.function_ = Collections.emptyList();
        this.property_ = Collections.emptyList();
        this.typeAlias_ = Collections.emptyList();
        this.typeTable_ = t.getDefaultInstance();
        this.versionRequirementTable_ = w.getDefaultInstance();
    }

    @Override // d0.e0.p.d.m0.i.n
    public void writeTo(CodedOutputStream codedOutputStream) throws IOException {
        getSerializedSize();
        g.d<MessageType>.a e = e();
        for (int i = 0; i < this.function_.size(); i++) {
            codedOutputStream.writeMessage(3, this.function_.get(i));
        }
        for (int i2 = 0; i2 < this.property_.size(); i2++) {
            codedOutputStream.writeMessage(4, this.property_.get(i2));
        }
        for (int i3 = 0; i3 < this.typeAlias_.size(); i3++) {
            codedOutputStream.writeMessage(5, this.typeAlias_.get(i3));
        }
        if ((this.bitField0_ & 1) == 1) {
            codedOutputStream.writeMessage(30, this.typeTable_);
        }
        if ((this.bitField0_ & 2) == 2) {
            codedOutputStream.writeMessage(32, this.versionRequirementTable_);
        }
        e.writeUntil(200, codedOutputStream);
        codedOutputStream.writeRawBytes(this.unknownFields);
    }

    public static b newBuilder() {
        return new b();
    }

    @Override // d0.e0.p.d.m0.i.o
    public l getDefaultInstanceForType() {
        return j;
    }

    @Override // d0.e0.p.d.m0.i.n
    public b newBuilderForType() {
        return newBuilder();
    }

    @Override // d0.e0.p.d.m0.i.n
    public b toBuilder() {
        return newBuilder(this);
    }

    public l() {
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        this.unknownFields = c.j;
    }

    /* JADX WARN: Finally extract failed */
    /* JADX WARN: Multi-variable type inference failed */
    public l(d dVar, e eVar, d0.e0.p.d.m0.f.a aVar) throws InvalidProtocolBufferException {
        this.memoizedIsInitialized = (byte) -1;
        this.memoizedSerializedSize = -1;
        r();
        c.b newOutput = c.newOutput();
        CodedOutputStream newInstance = CodedOutputStream.newInstance(newOutput, 1);
        boolean z2 = false;
        boolean z3 = false;
        while (!z2) {
            try {
                try {
                    try {
                        int readTag = dVar.readTag();
                        if (readTag != 0) {
                            if (readTag == 26) {
                                if (!z3 || !true) {
                                    this.function_ = new ArrayList();
                                    z3 |= true;
                                }
                                this.function_.add(dVar.readMessage(i.k, eVar));
                            } else if (readTag == 34) {
                                if (!(z3 & true)) {
                                    this.property_ = new ArrayList();
                                    z3 |= true;
                                }
                                this.property_.add(dVar.readMessage(n.k, eVar));
                            } else if (readTag != 42) {
                                w.b bVar = null;
                                t.b builder = null;
                                if (readTag == 242) {
                                    builder = (this.bitField0_ & 1) == 1 ? this.typeTable_.toBuilder() : builder;
                                    t tVar = (t) dVar.readMessage(t.k, eVar);
                                    this.typeTable_ = tVar;
                                    if (builder != null) {
                                        builder.mergeFrom(tVar);
                                        this.typeTable_ = builder.buildPartial();
                                    }
                                    this.bitField0_ |= 1;
                                } else if (readTag == 258) {
                                    bVar = (this.bitField0_ & 2) == 2 ? this.versionRequirementTable_.toBuilder() : bVar;
                                    w wVar = (w) dVar.readMessage(w.k, eVar);
                                    this.versionRequirementTable_ = wVar;
                                    if (bVar != null) {
                                        bVar.mergeFrom(wVar);
                                        this.versionRequirementTable_ = bVar.buildPartial();
                                    }
                                    this.bitField0_ |= 2;
                                } else if (!f(dVar, newInstance, eVar, readTag)) {
                                }
                            } else {
                                if (!(z3 & true)) {
                                    this.typeAlias_ = new ArrayList();
                                    z3 |= true;
                                }
                                this.typeAlias_.add(dVar.readMessage(r.k, eVar));
                            }
                        }
                        z2 = true;
                    } catch (InvalidProtocolBufferException e) {
                        throw e.setUnfinishedMessage(this);
                    }
                } catch (IOException e2) {
                    throw new InvalidProtocolBufferException(e2.getMessage()).setUnfinishedMessage(this);
                }
            } catch (Throwable th) {
                if (z3 && true) {
                    this.function_ = Collections.unmodifiableList(this.function_);
                }
                if (z3 & true) {
                    this.property_ = Collections.unmodifiableList(this.property_);
                }
                if (z3 & true) {
                    this.typeAlias_ = Collections.unmodifiableList(this.typeAlias_);
                }
                try {
                    newInstance.flush();
                } catch (IOException unused) {
                    this.unknownFields = newOutput.toByteString();
                    d();
                    throw th;
                } catch (Throwable th2) {
                    this.unknownFields = newOutput.toByteString();
                    throw th2;
                }
            }
        }
        if (z3 && true) {
            this.function_ = Collections.unmodifiableList(this.function_);
        }
        if (z3 & true) {
            this.property_ = Collections.unmodifiableList(this.property_);
        }
        if (z3 & true) {
            this.typeAlias_ = Collections.unmodifiableList(this.typeAlias_);
        }
        try {
            newInstance.flush();
        } catch (IOException unused2) {
            this.unknownFields = newOutput.toByteString();
            d();
        } catch (Throwable th3) {
            this.unknownFields = newOutput.toByteString();
            throw th3;
        }
    }
}
