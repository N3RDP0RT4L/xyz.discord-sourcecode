package d0.e0.p.d.m0.k;

import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.m;
import kotlin.jvm.functions.Function1;
/* compiled from: OverridingUtil.java */
/* loaded from: classes3.dex */
public final class l implements Function1<b, Boolean> {
    public final /* synthetic */ m j;

    public l(m mVar) {
        this.j = mVar;
    }

    public Boolean invoke(b bVar) {
        return Boolean.valueOf(bVar.getContainingDeclaration() == this.j);
    }
}
