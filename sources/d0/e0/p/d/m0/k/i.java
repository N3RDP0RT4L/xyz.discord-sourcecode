package d0.e0.p.d.m0.k;

import d0.e0.p.d.m0.c.b;
import d0.z.d.m;
/* compiled from: OverridingStrategy.kt */
/* loaded from: classes3.dex */
public abstract class i extends j {
    public abstract void conflict(b bVar, b bVar2);

    @Override // d0.e0.p.d.m0.k.j
    public void inheritanceConflict(b bVar, b bVar2) {
        m.checkNotNullParameter(bVar, "first");
        m.checkNotNullParameter(bVar2, "second");
        conflict(bVar, bVar2);
    }

    @Override // d0.e0.p.d.m0.k.j
    public void overrideConflict(b bVar, b bVar2) {
        m.checkNotNullParameter(bVar, "fromSuper");
        m.checkNotNullParameter(bVar2, "fromCurrent");
        conflict(bVar, bVar2);
    }
}
