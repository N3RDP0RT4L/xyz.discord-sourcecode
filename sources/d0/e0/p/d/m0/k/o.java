package d0.e0.p.d.m0.k;

import d0.e0.p.d.m0.c.b;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: OverridingUtil.java */
/* loaded from: classes3.dex */
public final class o implements Function1<b, Unit> {
    public final /* synthetic */ j j;
    public final /* synthetic */ b k;

    public o(j jVar, b bVar) {
        this.j = jVar;
        this.k = bVar;
    }

    public Unit invoke(b bVar) {
        this.j.inheritanceConflict(this.k, bVar);
        return Unit.a;
    }
}
