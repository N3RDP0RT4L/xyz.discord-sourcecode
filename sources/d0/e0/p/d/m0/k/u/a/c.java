package d0.e0.p.d.m0.k.u.a;

import b.d.b.a.a;
import d0.e0.p.d.m0.b.h;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.j1;
import d0.e0.p.d.m0.n.l1.g;
import d0.e0.p.d.m0.n.l1.j;
import d0.e0.p.d.m0.n.w0;
import d0.t.n;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
/* compiled from: CapturedTypeConstructor.kt */
/* loaded from: classes3.dex */
public final class c implements b {
    public final w0 a;

    /* renamed from: b  reason: collision with root package name */
    public j f3442b;

    public c(w0 w0Var) {
        m.checkNotNullParameter(w0Var, "projection");
        this.a = w0Var;
        getProjection().getProjectionKind();
        j1 j1Var = j1.INVARIANT;
    }

    @Override // d0.e0.p.d.m0.n.u0
    public h getBuiltIns() {
        h builtIns = getProjection().getType().getConstructor().getBuiltIns();
        m.checkNotNullExpressionValue(builtIns, "projection.type.constructor.builtIns");
        return builtIns;
    }

    @Override // d0.e0.p.d.m0.n.u0
    public Void getDeclarationDescriptor() {
        return null;
    }

    public final j getNewTypeConstructor() {
        return this.f3442b;
    }

    @Override // d0.e0.p.d.m0.n.u0
    public List<z0> getParameters() {
        return n.emptyList();
    }

    @Override // d0.e0.p.d.m0.k.u.a.b
    public w0 getProjection() {
        return this.a;
    }

    @Override // d0.e0.p.d.m0.n.u0
    public Collection<c0> getSupertypes() {
        c0 c0Var;
        if (getProjection().getProjectionKind() == j1.OUT_VARIANCE) {
            c0Var = getProjection().getType();
        } else {
            c0Var = getBuiltIns().getNullableAnyType();
        }
        m.checkNotNullExpressionValue(c0Var, "if (projection.projectionKind == Variance.OUT_VARIANCE)\n            projection.type\n        else\n            builtIns.nullableAnyType");
        return d0.t.m.listOf(c0Var);
    }

    @Override // d0.e0.p.d.m0.n.u0
    public boolean isDenotable() {
        return false;
    }

    public final void setNewTypeConstructor(j jVar) {
        this.f3442b = jVar;
    }

    public String toString() {
        StringBuilder R = a.R("CapturedTypeConstructor(");
        R.append(getProjection());
        R.append(')');
        return R.toString();
    }

    @Override // d0.e0.p.d.m0.n.u0
    public c refine(g gVar) {
        m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
        w0 refine = getProjection().refine(gVar);
        m.checkNotNullExpressionValue(refine, "projection.refine(kotlinTypeRefiner)");
        return new c(refine);
    }
}
