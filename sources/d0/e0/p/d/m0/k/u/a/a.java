package d0.e0.p.d.m0.k.u.a;

import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.n1.c;
import d0.e0.p.d.m0.n.t;
import d0.e0.p.d.m0.n.w0;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: CapturedTypeConstructor.kt */
/* loaded from: classes3.dex */
public final class a extends j0 implements c {
    public final w0 k;
    public final b l;
    public final boolean m;
    public final g n;

    public /* synthetic */ a(w0 w0Var, b bVar, boolean z2, g gVar, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(w0Var, (i & 2) != 0 ? new c(w0Var) : bVar, (i & 4) != 0 ? false : z2, (i & 8) != 0 ? g.f.getEMPTY() : gVar);
    }

    @Override // d0.e0.p.d.m0.c.g1.a
    public g getAnnotations() {
        return this.n;
    }

    @Override // d0.e0.p.d.m0.n.c0
    public List<w0> getArguments() {
        return n.emptyList();
    }

    @Override // d0.e0.p.d.m0.n.c0
    public i getMemberScope() {
        i createErrorScope = t.createErrorScope("No member resolution should be done on captured type, it used only during constraint system resolution", true);
        m.checkNotNullExpressionValue(createErrorScope, "createErrorScope(\n            \"No member resolution should be done on captured type, it used only during constraint system resolution\", true\n        )");
        return createErrorScope;
    }

    @Override // d0.e0.p.d.m0.n.c0
    public boolean isMarkedNullable() {
        return this.m;
    }

    @Override // d0.e0.p.d.m0.n.j0
    public String toString() {
        StringBuilder R = b.d.b.a.a.R("Captured(");
        R.append(this.k);
        R.append(')');
        R.append(isMarkedNullable() ? "?" : "");
        return R.toString();
    }

    @Override // d0.e0.p.d.m0.n.c0
    public b getConstructor() {
        return this.l;
    }

    @Override // d0.e0.p.d.m0.n.j0, d0.e0.p.d.m0.n.i1
    public a makeNullableAsSpecified(boolean z2) {
        return z2 == isMarkedNullable() ? this : new a(this.k, getConstructor(), z2, getAnnotations());
    }

    @Override // d0.e0.p.d.m0.n.i1, d0.e0.p.d.m0.n.c0
    public a refine(d0.e0.p.d.m0.n.l1.g gVar) {
        m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
        w0 refine = this.k.refine(gVar);
        m.checkNotNullExpressionValue(refine, "typeProjection.refine(kotlinTypeRefiner)");
        return new a(refine, getConstructor(), isMarkedNullable(), getAnnotations());
    }

    @Override // d0.e0.p.d.m0.n.j0, d0.e0.p.d.m0.n.i1
    public a replaceAnnotations(g gVar) {
        m.checkNotNullParameter(gVar, "newAnnotations");
        return new a(this.k, getConstructor(), isMarkedNullable(), gVar);
    }

    public a(w0 w0Var, b bVar, boolean z2, g gVar) {
        m.checkNotNullParameter(w0Var, "typeProjection");
        m.checkNotNullParameter(bVar, "constructor");
        m.checkNotNullParameter(gVar, "annotations");
        this.k = w0Var;
        this.l = bVar;
        this.m = z2;
        this.n = gVar;
    }
}
