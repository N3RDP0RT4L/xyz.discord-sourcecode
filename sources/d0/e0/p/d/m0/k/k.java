package d0.e0.p.d.m0.k;

import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.p0;
import d0.e0.p.d.m0.c.q;
import d0.e0.p.d.m0.c.q0;
import d0.e0.p.d.m0.c.t;
import d0.e0.p.d.m0.c.x;
import d0.e0.p.d.m0.c.y;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.k.f;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.e0;
import d0.e0.p.d.m0.n.l1.f;
import d0.e0.p.d.m0.n.l1.g;
import d0.e0.p.d.m0.n.l1.m;
import d0.e0.p.d.m0.n.u0;
import d0.e0.p.d.m0.p.j;
import d0.t.u;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.Set;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
/* compiled from: OverridingUtil.java */
/* loaded from: classes3.dex */
public class k {
    public static final List<f> a = u.toList(ServiceLoader.load(f.class, f.class.getClassLoader()));

    /* renamed from: b  reason: collision with root package name */
    public static final k f3440b;
    public static final f.a c;
    public final g d;
    public final f.a e;

    /* compiled from: OverridingUtil.java */
    /* loaded from: classes3.dex */
    public static class a implements f.a {
        public static /* synthetic */ void a(int i) {
            Object[] objArr = new Object[3];
            if (i != 1) {
                objArr[0] = "a";
            } else {
                objArr[0] = "b";
            }
            objArr[1] = "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil$1";
            objArr[2] = "equals";
            throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", objArr));
        }

        @Override // d0.e0.p.d.m0.n.l1.f.a
        public boolean equals(u0 u0Var, u0 u0Var2) {
            if (u0Var == null) {
                a(0);
                throw null;
            } else if (u0Var2 != null) {
                return u0Var.equals(u0Var2);
            } else {
                a(1);
                throw null;
            }
        }
    }

    /* compiled from: OverridingUtil.java */
    /* loaded from: classes3.dex */
    public static class b implements Function2<D, D, Pair<d0.e0.p.d.m0.c.a, d0.e0.p.d.m0.c.a>> {
        /* JADX WARN: Incorrect types in method signature: (TD;TD;)Lkotlin/Pair<Ld0/e0/p/d/m0/c/a;Ld0/e0/p/d/m0/c/a;>; */
        public Pair invoke(d0.e0.p.d.m0.c.a aVar, d0.e0.p.d.m0.c.a aVar2) {
            return new Pair(aVar, aVar2);
        }
    }

    /* compiled from: OverridingUtil.java */
    /* loaded from: classes3.dex */
    public static class c implements Function1<d0.e0.p.d.m0.c.b, d0.e0.p.d.m0.c.a> {
        public d0.e0.p.d.m0.c.b invoke(d0.e0.p.d.m0.c.b bVar) {
            return bVar;
        }
    }

    /* compiled from: OverridingUtil.java */
    /* loaded from: classes3.dex */
    public static class d {
        public static final d a = new d(a.OVERRIDABLE, "SUCCESS");

        /* renamed from: b  reason: collision with root package name */
        public final a f3441b;

        /* compiled from: OverridingUtil.java */
        /* loaded from: classes3.dex */
        public enum a {
            OVERRIDABLE,
            INCOMPATIBLE,
            CONFLICT
        }

        public d(a aVar, String str) {
            if (aVar == null) {
                a(3);
                throw null;
            } else if (str != null) {
                this.f3441b = aVar;
            } else {
                a(4);
                throw null;
            }
        }

        /* JADX WARN: Removed duplicated region for block: B:24:0x0038  */
        /* JADX WARN: Removed duplicated region for block: B:25:0x003b  */
        /* JADX WARN: Removed duplicated region for block: B:26:0x0040  */
        /* JADX WARN: Removed duplicated region for block: B:27:0x0045  */
        /* JADX WARN: Removed duplicated region for block: B:29:0x0049  */
        /* JADX WARN: Removed duplicated region for block: B:34:0x005a  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public static /* synthetic */ void a(int r10) {
            /*
                r0 = 4
                r1 = 3
                r2 = 2
                r3 = 1
                if (r10 == r3) goto Lf
                if (r10 == r2) goto Lf
                if (r10 == r1) goto Lf
                if (r10 == r0) goto Lf
                java.lang.String r4 = "@NotNull method %s.%s must not return null"
                goto L11
            Lf:
                java.lang.String r4 = "Argument for @NotNull parameter '%s' of %s.%s must not be null"
            L11:
                if (r10 == r3) goto L1b
                if (r10 == r2) goto L1b
                if (r10 == r1) goto L1b
                if (r10 == r0) goto L1b
                r5 = 2
                goto L1c
            L1b:
                r5 = 3
            L1c:
                java.lang.Object[] r5 = new java.lang.Object[r5]
                java.lang.String r6 = "success"
                java.lang.String r7 = "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil$OverrideCompatibilityInfo"
                r8 = 0
                if (r10 == r3) goto L31
                if (r10 == r2) goto L31
                if (r10 == r1) goto L2e
                if (r10 == r0) goto L31
                r5[r8] = r7
                goto L35
            L2e:
                r5[r8] = r6
                goto L35
            L31:
                java.lang.String r9 = "debugMessage"
                r5[r8] = r9
            L35:
                switch(r10) {
                    case 1: goto L45;
                    case 2: goto L45;
                    case 3: goto L45;
                    case 4: goto L45;
                    case 5: goto L40;
                    case 6: goto L3b;
                    default: goto L38;
                }
            L38:
                r5[r3] = r6
                goto L47
            L3b:
                java.lang.String r6 = "getDebugMessage"
                r5[r3] = r6
                goto L47
            L40:
                java.lang.String r6 = "getResult"
                r5[r3] = r6
                goto L47
            L45:
                r5[r3] = r7
            L47:
                if (r10 == r3) goto L5a
                if (r10 == r2) goto L55
                if (r10 == r1) goto L50
                if (r10 == r0) goto L50
                goto L5e
            L50:
                java.lang.String r6 = "<init>"
                r5[r2] = r6
                goto L5e
            L55:
                java.lang.String r6 = "conflict"
                r5[r2] = r6
                goto L5e
            L5a:
                java.lang.String r6 = "incompatible"
                r5[r2] = r6
            L5e:
                java.lang.String r4 = java.lang.String.format(r4, r5)
                if (r10 == r3) goto L70
                if (r10 == r2) goto L70
                if (r10 == r1) goto L70
                if (r10 == r0) goto L70
                java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
                r10.<init>(r4)
                goto L75
            L70:
                java.lang.IllegalArgumentException r10 = new java.lang.IllegalArgumentException
                r10.<init>(r4)
            L75:
                throw r10
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.k.k.d.a(int):void");
        }

        public static d conflict(String str) {
            if (str != null) {
                return new d(a.CONFLICT, str);
            }
            a(2);
            throw null;
        }

        public static d incompatible(String str) {
            if (str != null) {
                return new d(a.INCOMPATIBLE, str);
            }
            a(1);
            throw null;
        }

        public static d success() {
            d dVar = a;
            if (dVar != null) {
                return dVar;
            }
            a(0);
            throw null;
        }

        public a getResult() {
            a aVar = this.f3441b;
            if (aVar != null) {
                return aVar;
            }
            a(5);
            throw null;
        }
    }

    /* compiled from: OverridingUtil.java */
    /* loaded from: classes3.dex */
    public class e extends d0.e0.p.d.m0.n.l1.a {
        public final Map<u0, u0> j;

        public e(Map<u0, u0> map) {
            super(true, true, true, k.this.d);
            this.j = map;
        }

        public static /* synthetic */ void a(int i) {
            Object[] objArr = new Object[3];
            if (i == 1 || i == 3) {
                objArr[0] = "b";
            } else {
                objArr[0] = "a";
            }
            objArr[1] = "kotlin/reflect/jvm/internal/impl/resolve/OverridingUtil$OverridingUtilTypeCheckerContext";
            if (i == 2 || i == 3) {
                objArr[2] = "areEqualTypeConstructorsByAxioms";
            } else {
                objArr[2] = "areEqualTypeConstructors";
            }
            throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", objArr));
        }

        /* JADX WARN: Code restructure failed: missing block: B:17:0x0035, code lost:
            if (r0.equals(r6) != false) goto L21;
         */
        /* JADX WARN: Code restructure failed: missing block: B:20:0x003d, code lost:
            if (r3.equals(r5) != false) goto L21;
         */
        /* JADX WARN: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
        @Override // d0.e0.p.d.m0.n.l1.a
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public boolean areEqualTypeConstructors(d0.e0.p.d.m0.n.u0 r5, d0.e0.p.d.m0.n.u0 r6) {
            /*
                r4 = this;
                r0 = 0
                r1 = 0
                if (r5 == 0) goto L55
                r2 = 1
                if (r6 == 0) goto L51
                boolean r3 = super.areEqualTypeConstructors(r5, r6)
                if (r3 != 0) goto L4f
                if (r5 == 0) goto L4a
                if (r6 == 0) goto L45
                d0.e0.p.d.m0.k.k r0 = d0.e0.p.d.m0.k.k.this
                d0.e0.p.d.m0.n.l1.f$a r0 = r0.e
                boolean r0 = r0.equals(r5, r6)
                if (r0 == 0) goto L1c
                goto L3f
            L1c:
                java.util.Map<d0.e0.p.d.m0.n.u0, d0.e0.p.d.m0.n.u0> r0 = r4.j
                if (r0 != 0) goto L21
                goto L41
            L21:
                java.lang.Object r0 = r0.get(r5)
                d0.e0.p.d.m0.n.u0 r0 = (d0.e0.p.d.m0.n.u0) r0
                java.util.Map<d0.e0.p.d.m0.n.u0, d0.e0.p.d.m0.n.u0> r3 = r4.j
                java.lang.Object r3 = r3.get(r6)
                d0.e0.p.d.m0.n.u0 r3 = (d0.e0.p.d.m0.n.u0) r3
                if (r0 == 0) goto L37
                boolean r6 = r0.equals(r6)
                if (r6 != 0) goto L3f
            L37:
                if (r3 == 0) goto L41
                boolean r5 = r3.equals(r5)
                if (r5 == 0) goto L41
            L3f:
                r5 = 1
                goto L42
            L41:
                r5 = 0
            L42:
                if (r5 == 0) goto L50
                goto L4f
            L45:
                r5 = 3
                a(r5)
                throw r0
            L4a:
                r5 = 2
                a(r5)
                throw r0
            L4f:
                r1 = 1
            L50:
                return r1
            L51:
                a(r2)
                throw r0
            L55:
                a(r1)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.k.k.e.areEqualTypeConstructors(d0.e0.p.d.m0.n.u0, d0.e0.p.d.m0.n.u0):boolean");
        }
    }

    static {
        a aVar = new a();
        c = aVar;
        f3440b = new k(aVar, g.a.a);
    }

    public k(f.a aVar, g gVar) {
        if (aVar == null) {
            a(4);
            throw null;
        } else if (gVar != null) {
            this.e = aVar;
            this.d = gVar;
        } else {
            a(5);
            throw null;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:100:0x019b  */
    /* JADX WARN: Removed duplicated region for block: B:101:0x01a1  */
    /* JADX WARN: Removed duplicated region for block: B:102:0x01a7  */
    /* JADX WARN: Removed duplicated region for block: B:103:0x01ab  */
    /* JADX WARN: Removed duplicated region for block: B:104:0x01af  */
    /* JADX WARN: Removed duplicated region for block: B:105:0x01b3  */
    /* JADX WARN: Removed duplicated region for block: B:106:0x01b7  */
    /* JADX WARN: Removed duplicated region for block: B:107:0x01bd  */
    /* JADX WARN: Removed duplicated region for block: B:108:0x01c1  */
    /* JADX WARN: Removed duplicated region for block: B:109:0x01c7  */
    /* JADX WARN: Removed duplicated region for block: B:110:0x01cd  */
    /* JADX WARN: Removed duplicated region for block: B:111:0x01d3  */
    /* JADX WARN: Removed duplicated region for block: B:112:0x01d9  */
    /* JADX WARN: Removed duplicated region for block: B:113:0x01de  */
    /* JADX WARN: Removed duplicated region for block: B:114:0x01e3  */
    /* JADX WARN: Removed duplicated region for block: B:115:0x01e8  */
    /* JADX WARN: Removed duplicated region for block: B:116:0x01ed  */
    /* JADX WARN: Removed duplicated region for block: B:117:0x01f2  */
    /* JADX WARN: Removed duplicated region for block: B:118:0x01f7  */
    /* JADX WARN: Removed duplicated region for block: B:119:0x01fc  */
    /* JADX WARN: Removed duplicated region for block: B:120:0x0201  */
    /* JADX WARN: Removed duplicated region for block: B:121:0x0206  */
    /* JADX WARN: Removed duplicated region for block: B:122:0x020b  */
    /* JADX WARN: Removed duplicated region for block: B:123:0x020e  */
    /* JADX WARN: Removed duplicated region for block: B:124:0x0211  */
    /* JADX WARN: Removed duplicated region for block: B:125:0x0216  */
    /* JADX WARN: Removed duplicated region for block: B:126:0x0219  */
    /* JADX WARN: Removed duplicated region for block: B:127:0x021e  */
    /* JADX WARN: Removed duplicated region for block: B:128:0x0221  */
    /* JADX WARN: Removed duplicated region for block: B:129:0x0226  */
    /* JADX WARN: Removed duplicated region for block: B:130:0x022b  */
    /* JADX WARN: Removed duplicated region for block: B:131:0x0230  */
    /* JADX WARN: Removed duplicated region for block: B:134:0x023a A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:141:0x0249  */
    /* JADX WARN: Removed duplicated region for block: B:15:0x002d A[FALL_THROUGH] */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0041  */
    /* JADX WARN: Removed duplicated region for block: B:29:0x004c A[FALL_THROUGH] */
    /* JADX WARN: Removed duplicated region for block: B:32:0x0055  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x005b  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x0061  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x0067  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x006d  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x0073  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x0079  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x007f  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x0085  */
    /* JADX WARN: Removed duplicated region for block: B:41:0x008b  */
    /* JADX WARN: Removed duplicated region for block: B:42:0x0091  */
    /* JADX WARN: Removed duplicated region for block: B:43:0x0097  */
    /* JADX WARN: Removed duplicated region for block: B:44:0x009d  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x00a3  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x00a9  */
    /* JADX WARN: Removed duplicated region for block: B:47:0x00af  */
    /* JADX WARN: Removed duplicated region for block: B:48:0x00b5  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x00bb  */
    /* JADX WARN: Removed duplicated region for block: B:50:0x00c1  */
    /* JADX WARN: Removed duplicated region for block: B:51:0x00c7  */
    /* JADX WARN: Removed duplicated region for block: B:52:0x00cd  */
    /* JADX WARN: Removed duplicated region for block: B:53:0x00d3  */
    /* JADX WARN: Removed duplicated region for block: B:54:0x00d9  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x00df  */
    /* JADX WARN: Removed duplicated region for block: B:56:0x00e5  */
    /* JADX WARN: Removed duplicated region for block: B:57:0x00ea  */
    /* JADX WARN: Removed duplicated region for block: B:58:0x00ef  */
    /* JADX WARN: Removed duplicated region for block: B:59:0x00f4  */
    /* JADX WARN: Removed duplicated region for block: B:60:0x00f9  */
    /* JADX WARN: Removed duplicated region for block: B:61:0x00fe  */
    /* JADX WARN: Removed duplicated region for block: B:62:0x0103  */
    /* JADX WARN: Removed duplicated region for block: B:63:0x0108  */
    /* JADX WARN: Removed duplicated region for block: B:64:0x010d  */
    /* JADX WARN: Removed duplicated region for block: B:65:0x0112  */
    /* JADX WARN: Removed duplicated region for block: B:66:0x0117  */
    /* JADX WARN: Removed duplicated region for block: B:67:0x011c  */
    /* JADX WARN: Removed duplicated region for block: B:68:0x0121  */
    /* JADX WARN: Removed duplicated region for block: B:69:0x0126  */
    /* JADX WARN: Removed duplicated region for block: B:70:0x012b  */
    /* JADX WARN: Removed duplicated region for block: B:71:0x012e  */
    /* JADX WARN: Removed duplicated region for block: B:72:0x0133  */
    /* JADX WARN: Removed duplicated region for block: B:73:0x0138  */
    /* JADX WARN: Removed duplicated region for block: B:74:0x013d  */
    /* JADX WARN: Removed duplicated region for block: B:77:0x0157 A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:98:0x018f  */
    /* JADX WARN: Removed duplicated region for block: B:99:0x0195  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static /* synthetic */ void a(int r22) {
        /*
            Method dump skipped, instructions count: 1282
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.k.k.a(int):void");
    }

    public static void c(d0.e0.p.d.m0.c.b bVar, Set<d0.e0.p.d.m0.c.b> set) {
        if (bVar == null) {
            a(15);
            throw null;
        } else if (set == null) {
            a(16);
            throw null;
        } else if (bVar.getKind().isReal()) {
            set.add(bVar);
        } else if (!bVar.getOverriddenDescriptors().isEmpty()) {
            for (d0.e0.p.d.m0.c.b bVar2 : bVar.getOverriddenDescriptors()) {
                c(bVar2, set);
            }
        } else {
            throw new IllegalStateException("No overridden descriptors found for (fake override) " + bVar);
        }
    }

    public static k create(g gVar, f.a aVar) {
        if (gVar == null) {
            a(2);
            throw null;
        } else if (aVar != null) {
            return new k(aVar, gVar);
        } else {
            a(3);
            throw null;
        }
    }

    public static k createWithTypeRefiner(g gVar) {
        if (gVar != null) {
            return new k(c, gVar);
        }
        a(1);
        throw null;
    }

    public static List<c0> d(d0.e0.p.d.m0.c.a aVar) {
        q0 extensionReceiverParameter = aVar.getExtensionReceiverParameter();
        ArrayList arrayList = new ArrayList();
        if (extensionReceiverParameter != null) {
            arrayList.add(extensionReceiverParameter.getType());
        }
        for (c1 c1Var : aVar.getValueParameters()) {
            arrayList.add(c1Var.getType());
        }
        return arrayList;
    }

    /* JADX WARN: Code restructure failed: missing block: B:29:0x0063, code lost:
        r2 = r0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:69:0x00f0, code lost:
        if (r4 == false) goto L71;
     */
    /* JADX WARN: Code restructure failed: missing block: B:70:0x00f2, code lost:
        r0 = d0.e0.p.d.m0.c.t.h;
     */
    /* JADX WARN: Code restructure failed: missing block: B:71:0x00f5, code lost:
        r0 = d0.e0.p.d.m0.c.t.g;
     */
    /* JADX WARN: Code restructure failed: missing block: B:72:0x00f7, code lost:
        r0 = ((d0.e0.p.d.m0.c.b) selectMostSpecificMember(r6, new d0.e0.p.d.m0.k.k.c())).copy(r12, r2, r0, d0.e0.p.d.m0.c.b.a.FAKE_OVERRIDE, false);
        r13.setOverriddenDescriptors(r0, r6);
        r13.addFakeOverride(r0);
     */
    /* JADX WARN: Code restructure failed: missing block: B:73:0x0111, code lost:
        return;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static void e(java.util.Collection<d0.e0.p.d.m0.c.b> r11, d0.e0.p.d.m0.c.e r12, d0.e0.p.d.m0.k.j r13) {
        /*
            Method dump skipped, instructions count: 328
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.k.k.e(java.util.Collection, d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.k.j):void");
    }

    public static <H> Collection<H> extractMembersOverridableInBothWays(H h, Collection<H> collection, Function1<H, d0.e0.p.d.m0.c.a> function1, Function1<H, Unit> function12) {
        if (h == null) {
            a(97);
            throw null;
        } else if (collection == null) {
            a(98);
            throw null;
        } else if (function1 == null) {
            a(99);
            throw null;
        } else if (function12 != null) {
            ArrayList arrayList = new ArrayList();
            arrayList.add(h);
            d0.e0.p.d.m0.c.a invoke = function1.invoke(h);
            Iterator<H> it = collection.iterator();
            while (it.hasNext()) {
                H next = it.next();
                d0.e0.p.d.m0.c.a invoke2 = function1.invoke(next);
                if (h == next) {
                    it.remove();
                } else {
                    d.a bothWaysOverridability = getBothWaysOverridability(invoke, invoke2);
                    if (bothWaysOverridability == d.a.OVERRIDABLE) {
                        arrayList.add(next);
                        it.remove();
                    } else if (bothWaysOverridability == d.a.CONFLICT) {
                        function12.invoke(next);
                        it.remove();
                    }
                }
            }
            return arrayList;
        } else {
            a(100);
            throw null;
        }
    }

    public static <D extends d0.e0.p.d.m0.c.a> Set<D> filterOutOverridden(Set<D> set) {
        if (set != null) {
            return filterOverrides(set, !set.isEmpty() && d0.e0.p.d.m0.k.x.a.isTypeRefinementEnabled(d0.e0.p.d.m0.k.x.a.getModule(set.iterator().next())), null, new b());
        }
        a(6);
        throw null;
    }

    public static <D> Set<D> filterOverrides(Set<D> set, boolean z2, Function0<?> function0, Function2<? super D, ? super D, Pair<d0.e0.p.d.m0.c.a, d0.e0.p.d.m0.c.a>> function2) {
        if (set == null) {
            a(7);
            throw null;
        } else if (function2 == null) {
            a(8);
            throw null;
        } else if (set.size() <= 1) {
            return set;
        } else {
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            for (Object obj : set) {
                if (function0 != null) {
                    function0.invoke();
                }
                Iterator it = linkedHashSet.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        linkedHashSet.add(obj);
                        break;
                    }
                    Pair<d0.e0.p.d.m0.c.a, d0.e0.p.d.m0.c.a> invoke = function2.invoke(obj, (Object) it.next());
                    d0.e0.p.d.m0.c.a component1 = invoke.component1();
                    d0.e0.p.d.m0.c.a component2 = invoke.component2();
                    if (overrides(component1, component2, z2, true)) {
                        it.remove();
                    } else if (overrides(component2, component1, z2, true)) {
                        break;
                    }
                }
            }
            return linkedHashSet;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:24:0x0040  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static d0.e0.p.d.m0.c.u findMaxVisibility(java.util.Collection<? extends d0.e0.p.d.m0.c.b> r5) {
        /*
            r0 = 0
            if (r5 == 0) goto L58
            boolean r1 = r5.isEmpty()
            if (r1 == 0) goto Lc
            d0.e0.p.d.m0.c.u r5 = d0.e0.p.d.m0.c.t.k
            return r5
        Lc:
            java.util.Iterator r1 = r5.iterator()
        L10:
            r2 = r0
        L11:
            boolean r3 = r1.hasNext()
            if (r3 == 0) goto L33
            java.lang.Object r3 = r1.next()
            d0.e0.p.d.m0.c.b r3 = (d0.e0.p.d.m0.c.b) r3
            d0.e0.p.d.m0.c.u r3 = r3.getVisibility()
            if (r2 != 0) goto L25
        L23:
            r2 = r3
            goto L11
        L25:
            java.lang.Integer r4 = d0.e0.p.d.m0.c.t.compare(r3, r2)
            if (r4 != 0) goto L2c
            goto L10
        L2c:
            int r4 = r4.intValue()
            if (r4 <= 0) goto L11
            goto L23
        L33:
            if (r2 != 0) goto L36
            return r0
        L36:
            java.util.Iterator r5 = r5.iterator()
        L3a:
            boolean r1 = r5.hasNext()
            if (r1 == 0) goto L57
            java.lang.Object r1 = r5.next()
            d0.e0.p.d.m0.c.b r1 = (d0.e0.p.d.m0.c.b) r1
            d0.e0.p.d.m0.c.u r1 = r1.getVisibility()
            java.lang.Integer r1 = d0.e0.p.d.m0.c.t.compare(r2, r1)
            if (r1 == 0) goto L56
            int r1 = r1.intValue()
            if (r1 >= 0) goto L3a
        L56:
            return r0
        L57:
            return r2
        L58:
            r5 = 107(0x6b, float:1.5E-43)
            a(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.k.k.findMaxVisibility(java.util.Collection):d0.e0.p.d.m0.c.u");
    }

    public static boolean g(d0.e0.p.d.m0.c.a aVar, c0 c0Var, d0.e0.p.d.m0.c.a aVar2, c0 c0Var2, Pair<m, d0.e0.p.d.m0.n.l1.a> pair) {
        if (c0Var == null) {
            a(72);
            throw null;
        } else if (c0Var2 != null) {
            return pair.getFirst().isSubtypeOf(pair.getSecond(), c0Var.unwrap(), c0Var2.unwrap());
        } else {
            a(74);
            throw null;
        }
    }

    public static d getBasicOverridabilityProblem(d0.e0.p.d.m0.c.a aVar, d0.e0.p.d.m0.c.a aVar2) {
        boolean z2;
        d dVar;
        if (aVar == null) {
            a(38);
            throw null;
        } else if (aVar2 != null) {
            boolean z3 = aVar instanceof x;
            if ((z3 && !(aVar2 instanceof x)) || (((z2 = aVar instanceof n0)) && !(aVar2 instanceof n0))) {
                return d.incompatible("Member kind mismatch");
            }
            if (!z3 && !z2) {
                throw new IllegalArgumentException("This type of CallableDescriptor cannot be checked for overridability: " + aVar);
            } else if (!aVar.getName().equals(aVar2.getName())) {
                return d.incompatible("Name mismatch");
            } else {
                boolean z4 = false;
                boolean z5 = aVar.getExtensionReceiverParameter() == null;
                if (aVar2.getExtensionReceiverParameter() == null) {
                    z4 = true;
                }
                if (z5 != z4) {
                    dVar = d.incompatible("Receiver presence mismatch");
                } else if (aVar.getValueParameters().size() != aVar2.getValueParameters().size()) {
                    dVar = d.incompatible("Value parameter number mismatch");
                } else {
                    dVar = null;
                }
                if (dVar != null) {
                    return dVar;
                }
                return null;
            }
        } else {
            a(39);
            throw null;
        }
    }

    public static d.a getBothWaysOverridability(d0.e0.p.d.m0.c.a aVar, d0.e0.p.d.m0.c.a aVar2) {
        k kVar = f3440b;
        d.a result = kVar.isOverridableBy(aVar2, aVar, null).getResult();
        d.a result2 = kVar.isOverridableBy(aVar, aVar2, null).getResult();
        d.a aVar3 = d.a.OVERRIDABLE;
        if (result == aVar3 && result2 == aVar3) {
            return aVar3;
        }
        d.a aVar4 = d.a.CONFLICT;
        return (result == aVar4 || result2 == aVar4) ? aVar4 : d.a.INCOMPATIBLE;
    }

    public static Set<d0.e0.p.d.m0.c.b> getOverriddenDeclarations(d0.e0.p.d.m0.c.b bVar) {
        if (bVar != null) {
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            c(bVar, linkedHashSet);
            return linkedHashSet;
        }
        a(13);
        throw null;
    }

    public static boolean h(q qVar, q qVar2) {
        Integer compare = t.compare(qVar.getVisibility(), qVar2.getVisibility());
        return compare == null || compare.intValue() >= 0;
    }

    public static boolean isMoreSpecific(d0.e0.p.d.m0.c.a aVar, d0.e0.p.d.m0.c.a aVar2) {
        if (aVar == null) {
            a(65);
            throw null;
        } else if (aVar2 != null) {
            c0 returnType = aVar.getReturnType();
            c0 returnType2 = aVar2.getReturnType();
            if (!h(aVar, aVar2)) {
                return false;
            }
            Pair<m, d0.e0.p.d.m0.n.l1.a> f = f3440b.f(aVar.getTypeParameters(), aVar2.getTypeParameters());
            if (aVar instanceof x) {
                return g(aVar, returnType, aVar2, returnType2, f);
            }
            if (aVar instanceof n0) {
                n0 n0Var = (n0) aVar;
                n0 n0Var2 = (n0) aVar2;
                p0 setter = n0Var.getSetter();
                p0 setter2 = n0Var2.getSetter();
                if (!((setter == null || setter2 == null) ? true : h(setter, setter2))) {
                    return false;
                }
                if (!n0Var.isVar() || !n0Var2.isVar()) {
                    return (n0Var.isVar() || !n0Var2.isVar()) && g(aVar, returnType, aVar2, returnType2, f);
                }
                return f.getFirst().equalTypes(f.getSecond(), returnType.unwrap(), returnType2.unwrap());
            }
            StringBuilder R = b.d.b.a.a.R("Unexpected callable: ");
            R.append(aVar.getClass());
            throw new IllegalArgumentException(R.toString());
        } else {
            a(66);
            throw null;
        }
    }

    public static boolean isVisibleForOverride(y yVar, y yVar2) {
        if (yVar == null) {
            a(55);
            throw null;
        } else if (yVar2 != null) {
            return !t.isPrivate(yVar2.getVisibility()) && t.isVisibleIgnoringReceiver(yVar2, yVar);
        } else {
            a(56);
            throw null;
        }
    }

    public static <D extends d0.e0.p.d.m0.c.a> boolean overrides(D d2, D d3, boolean z2, boolean z3) {
        if (d2 == null) {
            a(11);
            throw null;
        } else if (d3 == null) {
            a(12);
            throw null;
        } else if (!d2.equals(d3) && d0.e0.p.d.m0.k.b.a.areEquivalent(d2.getOriginal(), d3.getOriginal(), z2, z3)) {
            return true;
        } else {
            d0.e0.p.d.m0.c.a original = d3.getOriginal();
            for (d0.e0.p.d.m0.c.a aVar : d0.e0.p.d.m0.k.e.getAllOverriddenDescriptors(d2)) {
                if (d0.e0.p.d.m0.k.b.a.areEquivalent(original, aVar, z2, z3)) {
                    return true;
                }
            }
            return false;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:27:0x0069  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x0071  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x0076  */
    /* JADX WARN: Removed duplicated region for block: B:41:0x009b  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static void resolveUnknownVisibilityForMember(d0.e0.p.d.m0.c.b r6, kotlin.jvm.functions.Function1<d0.e0.p.d.m0.c.b, kotlin.Unit> r7) {
        /*
            r0 = 0
            if (r6 == 0) goto Lb9
            java.util.Collection r1 = r6.getOverriddenDescriptors()
            java.util.Iterator r1 = r1.iterator()
        Lb:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L23
            java.lang.Object r2 = r1.next()
            d0.e0.p.d.m0.c.b r2 = (d0.e0.p.d.m0.c.b) r2
            d0.e0.p.d.m0.c.u r3 = r2.getVisibility()
            d0.e0.p.d.m0.c.u r4 = d0.e0.p.d.m0.c.t.g
            if (r3 != r4) goto Lb
            resolveUnknownVisibilityForMember(r2, r7)
            goto Lb
        L23:
            d0.e0.p.d.m0.c.u r1 = r6.getVisibility()
            d0.e0.p.d.m0.c.u r2 = d0.e0.p.d.m0.c.t.g
            if (r1 == r2) goto L2c
            return
        L2c:
            java.util.Collection r1 = r6.getOverriddenDescriptors()
            d0.e0.p.d.m0.c.u r2 = findMaxVisibility(r1)
            if (r2 != 0) goto L38
        L36:
            r2 = r0
            goto L67
        L38:
            d0.e0.p.d.m0.c.b$a r3 = r6.getKind()
            d0.e0.p.d.m0.c.b$a r4 = d0.e0.p.d.m0.c.b.a.FAKE_OVERRIDE
            if (r3 != r4) goto L63
            java.util.Iterator r1 = r1.iterator()
        L44:
            boolean r3 = r1.hasNext()
            if (r3 == 0) goto L67
            java.lang.Object r3 = r1.next()
            d0.e0.p.d.m0.c.b r3 = (d0.e0.p.d.m0.c.b) r3
            d0.e0.p.d.m0.c.z r4 = r3.getModality()
            d0.e0.p.d.m0.c.z r5 = d0.e0.p.d.m0.c.z.ABSTRACT
            if (r4 == r5) goto L44
            d0.e0.p.d.m0.c.u r3 = r3.getVisibility()
            boolean r3 = r3.equals(r2)
            if (r3 != 0) goto L44
            goto L36
        L63:
            d0.e0.p.d.m0.c.u r2 = r2.normalize()
        L67:
            if (r2 != 0) goto L71
            if (r7 == 0) goto L6e
            r7.invoke(r6)
        L6e:
            d0.e0.p.d.m0.c.u r1 = d0.e0.p.d.m0.c.t.e
            goto L72
        L71:
            r1 = r2
        L72:
            boolean r3 = r6 instanceof d0.e0.p.d.m0.c.i1.c0
            if (r3 == 0) goto L9b
            r3 = r6
            d0.e0.p.d.m0.c.i1.c0 r3 = (d0.e0.p.d.m0.c.i1.c0) r3
            r3.setVisibility(r1)
            d0.e0.p.d.m0.c.n0 r6 = (d0.e0.p.d.m0.c.n0) r6
            java.util.List r6 = r6.getAccessors()
            java.util.Iterator r6 = r6.iterator()
        L86:
            boolean r1 = r6.hasNext()
            if (r1 == 0) goto Lb8
            java.lang.Object r1 = r6.next()
            d0.e0.p.d.m0.c.m0 r1 = (d0.e0.p.d.m0.c.m0) r1
            if (r2 != 0) goto L96
            r3 = r0
            goto L97
        L96:
            r3 = r7
        L97:
            resolveUnknownVisibilityForMember(r1, r3)
            goto L86
        L9b:
            boolean r7 = r6 instanceof d0.e0.p.d.m0.c.i1.q
            if (r7 == 0) goto La5
            d0.e0.p.d.m0.c.i1.q r6 = (d0.e0.p.d.m0.c.i1.q) r6
            r6.setVisibility(r1)
            goto Lb8
        La5:
            d0.e0.p.d.m0.c.i1.b0 r6 = (d0.e0.p.d.m0.c.i1.b0) r6
            r6.setVisibility(r1)
            d0.e0.p.d.m0.c.n0 r7 = r6.getCorrespondingProperty()
            d0.e0.p.d.m0.c.u r7 = r7.getVisibility()
            if (r1 == r7) goto Lb8
            r7 = 0
            r6.setDefault(r7)
        Lb8:
            return
        Lb9:
            r6 = 105(0x69, float:1.47E-43)
            a(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.k.k.resolveUnknownVisibilityForMember(d0.e0.p.d.m0.c.b, kotlin.jvm.functions.Function1):void");
    }

    public static <H> H selectMostSpecificMember(Collection<H> collection, Function1<H, d0.e0.p.d.m0.c.a> function1) {
        H h;
        boolean z2;
        if (collection == null) {
            a(76);
            throw null;
        } else if (function1 == null) {
            a(77);
            throw null;
        } else if (collection.size() == 1) {
            H h2 = (H) u.first(collection);
            if (h2 != null) {
                return h2;
            }
            a(78);
            throw null;
        } else {
            ArrayList arrayList = new ArrayList(2);
            List map = u.map(collection, function1);
            H h3 = (H) u.first(collection);
            d0.e0.p.d.m0.c.a invoke = function1.invoke(h3);
            for (H h4 : collection) {
                d0.e0.p.d.m0.c.a invoke2 = function1.invoke(h4);
                if (invoke2 == null) {
                    a(69);
                    throw null;
                } else if (map != null) {
                    Iterator it = map.iterator();
                    while (true) {
                        if (it.hasNext()) {
                            if (!isMoreSpecific(invoke2, (d0.e0.p.d.m0.c.a) it.next())) {
                                z2 = false;
                                break;
                            }
                        } else {
                            z2 = true;
                            break;
                        }
                    }
                    if (z2) {
                        arrayList.add(h4);
                    }
                    if (isMoreSpecific(invoke2, invoke) && !isMoreSpecific(invoke, invoke2)) {
                        h3 = h4;
                    }
                } else {
                    a(70);
                    throw null;
                }
            }
            if (arrayList.isEmpty()) {
                if (h3 != null) {
                    return h3;
                }
                a(79);
                throw null;
            } else if (arrayList.size() == 1) {
                H h5 = (H) u.first((Iterable<? extends Object>) arrayList);
                if (h5 != null) {
                    return h5;
                }
                a(80);
                throw null;
            } else {
                Iterator it2 = arrayList.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        h = null;
                        break;
                    }
                    h = (H) it2.next();
                    if (!d0.e0.p.d.m0.n.y.isFlexible(function1.invoke(h).getReturnType())) {
                        break;
                    }
                }
                if (h != null) {
                    return h;
                }
                H h6 = (H) u.first((Iterable<? extends Object>) arrayList);
                if (h6 != null) {
                    return h6;
                }
                a(82);
                throw null;
            }
        }
    }

    public final boolean b(c0 c0Var, c0 c0Var2, Pair<m, d0.e0.p.d.m0.n.l1.a> pair) {
        if (c0Var == null) {
            a(44);
            throw null;
        } else if (c0Var2 == null) {
            a(45);
            throw null;
        } else if (pair != null) {
            if (e0.isError(c0Var) && e0.isError(c0Var2)) {
                return true;
            }
            return pair.getFirst().equalTypes(pair.getSecond(), c0Var.unwrap(), c0Var2.unwrap());
        } else {
            a(46);
            throw null;
        }
    }

    public final Pair<m, d0.e0.p.d.m0.n.l1.a> f(List<z0> list, List<z0> list2) {
        e eVar;
        if (list == null) {
            a(40);
            throw null;
        } else if (list2 != null) {
            m mVar = new m(this.d);
            if (list == null) {
                a(42);
                throw null;
            } else if (list2 != null) {
                if (list.isEmpty()) {
                    eVar = new e(null);
                } else {
                    HashMap hashMap = new HashMap();
                    for (int i = 0; i < list.size(); i++) {
                        hashMap.put(list.get(i).getTypeConstructor(), list2.get(i).getTypeConstructor());
                    }
                    eVar = new e(hashMap);
                }
                return new Pair<>(mVar, eVar);
            } else {
                a(43);
                throw null;
            }
        } else {
            a(41);
            throw null;
        }
    }

    public void generateOverridesInFunctionGroup(d0.e0.p.d.m0.g.e eVar, Collection<? extends d0.e0.p.d.m0.c.b> collection, Collection<? extends d0.e0.p.d.m0.c.b> collection2, d0.e0.p.d.m0.c.e eVar2, j jVar) {
        if (eVar == null) {
            a(50);
            throw null;
        } else if (collection == null) {
            a(51);
            throw null;
        } else if (collection2 == null) {
            a(52);
            throw null;
        } else if (eVar2 == null) {
            a(53);
            throw null;
        } else if (jVar != null) {
            LinkedHashSet<d0.e0.p.d.m0.c.b> linkedHashSet = new LinkedHashSet(collection);
            for (d0.e0.p.d.m0.c.b bVar : collection2) {
                if (bVar == null) {
                    a(57);
                    throw null;
                } else if (collection == null) {
                    a(58);
                    throw null;
                } else if (eVar2 == null) {
                    a(59);
                    throw null;
                } else if (jVar != null) {
                    ArrayList arrayList = new ArrayList(collection.size());
                    j create = j.create();
                    for (d0.e0.p.d.m0.c.b bVar2 : collection) {
                        d.a result = isOverridableBy(bVar2, bVar, eVar2).getResult();
                        boolean isVisibleForOverride = isVisibleForOverride(bVar, bVar2);
                        int ordinal = result.ordinal();
                        if (ordinal == 0) {
                            if (isVisibleForOverride) {
                                create.add(bVar2);
                            }
                            arrayList.add(bVar2);
                        } else if (ordinal == 2) {
                            if (isVisibleForOverride) {
                                jVar.overrideConflict(bVar2, bVar);
                            }
                            arrayList.add(bVar2);
                        }
                    }
                    jVar.setOverriddenDescriptors(bVar, create);
                    linkedHashSet.removeAll(arrayList);
                } else {
                    a(60);
                    throw null;
                }
            }
            if (eVar2 == null) {
                a(62);
                throw null;
            } else if (jVar != null) {
                if (linkedHashSet.size() < 2 ? true : u.all(linkedHashSet, new l(((d0.e0.p.d.m0.c.b) linkedHashSet.iterator().next()).getContainingDeclaration()))) {
                    for (d0.e0.p.d.m0.c.b bVar3 : linkedHashSet) {
                        e(Collections.singleton(bVar3), eVar2, jVar);
                    }
                    return;
                }
                LinkedList linkedList = new LinkedList(linkedHashSet);
                while (!linkedList.isEmpty()) {
                    d0.e0.p.d.m0.c.b findMemberWithMaxVisibility = t.findMemberWithMaxVisibility(linkedList);
                    if (findMemberWithMaxVisibility != null) {
                        e(extractMembersOverridableInBothWays(findMemberWithMaxVisibility, linkedList, new n(), new o(jVar, findMemberWithMaxVisibility)), eVar2, jVar);
                    } else {
                        a(102);
                        throw null;
                    }
                }
            } else {
                a(64);
                throw null;
            }
        } else {
            a(54);
            throw null;
        }
    }

    public d isOverridableBy(d0.e0.p.d.m0.c.a aVar, d0.e0.p.d.m0.c.a aVar2, d0.e0.p.d.m0.c.e eVar) {
        if (aVar == null) {
            a(17);
            throw null;
        } else if (aVar2 != null) {
            d isOverridableBy = isOverridableBy(aVar, aVar2, eVar, false);
            if (isOverridableBy != null) {
                return isOverridableBy;
            }
            a(19);
            throw null;
        } else {
            a(18);
            throw null;
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:43:0x00c9, code lost:
        r15.remove();
     */
    /* JADX WARN: Removed duplicated region for block: B:108:0x00cf A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:50:0x00de A[LOOP:1: B:27:0x0071->B:50:0x00de, LOOP_END] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public d0.e0.p.d.m0.k.k.d isOverridableByWithoutExternalConditions(d0.e0.p.d.m0.c.a r18, d0.e0.p.d.m0.c.a r19, boolean r20) {
        /*
            Method dump skipped, instructions count: 422
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.k.k.isOverridableByWithoutExternalConditions(d0.e0.p.d.m0.c.a, d0.e0.p.d.m0.c.a, boolean):d0.e0.p.d.m0.k.k$d");
    }

    public d isOverridableBy(d0.e0.p.d.m0.c.a aVar, d0.e0.p.d.m0.c.a aVar2, d0.e0.p.d.m0.c.e eVar, boolean z2) {
        f.a aVar3 = f.a.CONFLICTS_ONLY;
        if (aVar == null) {
            a(20);
            throw null;
        } else if (aVar2 != null) {
            d isOverridableByWithoutExternalConditions = isOverridableByWithoutExternalConditions(aVar, aVar2, z2);
            boolean z3 = isOverridableByWithoutExternalConditions.getResult() == d.a.OVERRIDABLE;
            for (f fVar : a) {
                if (fVar.getContract() != aVar3 && (!z3 || fVar.getContract() != f.a.SUCCESS_ONLY)) {
                    int ordinal = fVar.isOverridable(aVar, aVar2, eVar).ordinal();
                    if (ordinal == 0) {
                        z3 = true;
                    } else if (ordinal == 1) {
                        d conflict = d.conflict("External condition failed");
                        if (conflict != null) {
                            return conflict;
                        }
                        a(22);
                        throw null;
                    } else if (ordinal == 2) {
                        d incompatible = d.incompatible("External condition");
                        if (incompatible != null) {
                            return incompatible;
                        }
                        a(23);
                        throw null;
                    }
                }
            }
            if (!z3) {
                return isOverridableByWithoutExternalConditions;
            }
            for (f fVar2 : a) {
                if (fVar2.getContract() == aVar3) {
                    int ordinal2 = fVar2.isOverridable(aVar, aVar2, eVar).ordinal();
                    if (ordinal2 == 0) {
                        StringBuilder R = b.d.b.a.a.R("Contract violation in ");
                        R.append(fVar2.getClass().getName());
                        R.append(" condition. It's not supposed to end with success");
                        throw new IllegalStateException(R.toString());
                    } else if (ordinal2 == 1) {
                        d conflict2 = d.conflict("External condition failed");
                        if (conflict2 != null) {
                            return conflict2;
                        }
                        a(25);
                        throw null;
                    } else if (ordinal2 == 2) {
                        d incompatible2 = d.incompatible("External condition");
                        if (incompatible2 != null) {
                            return incompatible2;
                        }
                        a(26);
                        throw null;
                    }
                }
            }
            d success = d.success();
            if (success != null) {
                return success;
            }
            a(27);
            throw null;
        } else {
            a(21);
            throw null;
        }
    }
}
