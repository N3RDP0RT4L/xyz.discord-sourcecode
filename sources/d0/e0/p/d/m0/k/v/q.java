package d0.e0.p.d.m0.k.v;

import d0.e0.p.d.m0.b.h;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.l1.g;
import d0.e0.p.d.m0.n.u0;
import d0.t.n;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
/* compiled from: IntegerValueTypeConstructor.kt */
/* loaded from: classes3.dex */
public final class q implements u0 {
    @Override // d0.e0.p.d.m0.n.u0
    public h getBuiltIns() {
        throw null;
    }

    @Override // d0.e0.p.d.m0.n.u0
    public Void getDeclarationDescriptor() {
        return null;
    }

    @Override // d0.e0.p.d.m0.n.u0
    public List<z0> getParameters() {
        return n.emptyList();
    }

    @Override // d0.e0.p.d.m0.n.u0
    public Collection<c0> getSupertypes() {
        return null;
    }

    @Override // d0.e0.p.d.m0.n.u0
    public boolean isDenotable() {
        return false;
    }

    @Override // d0.e0.p.d.m0.n.u0
    public u0 refine(g gVar) {
        m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
        return this;
    }

    public String toString() {
        return "IntegerValueType(0)";
    }
}
