package d0.e0.p.d.m0.k.v;

import d0.e0.p.d.m0.g.a;
import d0.z.d.m;
/* compiled from: ClassLiteralValue.kt */
/* loaded from: classes3.dex */
public final class f {
    public final a a;

    /* renamed from: b  reason: collision with root package name */
    public final int f3444b;

    public f(a aVar, int i) {
        m.checkNotNullParameter(aVar, "classId");
        this.a = aVar;
        this.f3444b = i;
    }

    public final a component1() {
        return this.a;
    }

    public final int component2() {
        return this.f3444b;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        return m.areEqual(this.a, fVar.a) && this.f3444b == fVar.f3444b;
    }

    public final int getArrayNestedness() {
        return this.f3444b;
    }

    public final a getClassId() {
        return this.a;
    }

    public int hashCode() {
        return (this.a.hashCode() * 31) + this.f3444b;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        int arrayNestedness = getArrayNestedness();
        for (int i = 0; i < arrayNestedness; i++) {
            sb.append("kotlin/Array<");
        }
        sb.append(getClassId());
        int arrayNestedness2 = getArrayNestedness();
        for (int i2 = 0; i2 < arrayNestedness2; i2++) {
            sb.append(">");
        }
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }
}
