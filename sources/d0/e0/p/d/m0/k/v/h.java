package d0.e0.p.d.m0.k.v;

import d0.e0.p.d.m0.b.i;
import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.n.j0;
import d0.t.k;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.List;
import kotlin.jvm.functions.Function1;
/* compiled from: ConstantValueFactory.kt */
/* loaded from: classes3.dex */
public final class h {
    public static final h a = new h();

    /* compiled from: ConstantValueFactory.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<c0, d0.e0.p.d.m0.n.c0> {
        public final /* synthetic */ d0.e0.p.d.m0.n.c0 $type;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(d0.e0.p.d.m0.n.c0 c0Var) {
            super(1);
            this.$type = c0Var;
        }

        public final d0.e0.p.d.m0.n.c0 invoke(c0 c0Var) {
            m.checkNotNullParameter(c0Var, "it");
            return this.$type;
        }
    }

    /* compiled from: ConstantValueFactory.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function1<c0, d0.e0.p.d.m0.n.c0> {
        public final /* synthetic */ i $componentType;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(i iVar) {
            super(1);
            this.$componentType = iVar;
        }

        public final d0.e0.p.d.m0.n.c0 invoke(c0 c0Var) {
            m.checkNotNullParameter(c0Var, "module");
            j0 primitiveArrayKotlinType = c0Var.getBuiltIns().getPrimitiveArrayKotlinType(this.$componentType);
            m.checkNotNullExpressionValue(primitiveArrayKotlinType, "module.builtIns.getPrimitiveArrayKotlinType(componentType)");
            return primitiveArrayKotlinType;
        }
    }

    public final d0.e0.p.d.m0.k.v.b a(List<?> list, i iVar) {
        List<Object> list2 = u.toList(list);
        ArrayList arrayList = new ArrayList();
        for (Object obj : list2) {
            g<?> createConstantValue = createConstantValue(obj);
            if (createConstantValue != null) {
                arrayList.add(createConstantValue);
            }
        }
        return new d0.e0.p.d.m0.k.v.b(arrayList, new b(iVar));
    }

    public final d0.e0.p.d.m0.k.v.b createArrayValue(List<? extends g<?>> list, d0.e0.p.d.m0.n.c0 c0Var) {
        m.checkNotNullParameter(list, "value");
        m.checkNotNullParameter(c0Var, "type");
        return new d0.e0.p.d.m0.k.v.b(list, new a(c0Var));
    }

    public final g<?> createConstantValue(Object obj) {
        if (obj instanceof Byte) {
            return new d(((Number) obj).byteValue());
        }
        if (obj instanceof Short) {
            return new v(((Number) obj).shortValue());
        }
        if (obj instanceof Integer) {
            return new m(((Number) obj).intValue());
        }
        if (obj instanceof Long) {
            return new s(((Number) obj).longValue());
        }
        if (obj instanceof Character) {
            return new e(((Character) obj).charValue());
        }
        if (obj instanceof Float) {
            return new l(((Number) obj).floatValue());
        }
        if (obj instanceof Double) {
            return new i(((Number) obj).doubleValue());
        }
        if (obj instanceof Boolean) {
            return new c(((Boolean) obj).booleanValue());
        }
        if (obj instanceof String) {
            return new w((String) obj);
        }
        if (obj instanceof byte[]) {
            return a(k.toList((byte[]) obj), i.BYTE);
        }
        if (obj instanceof short[]) {
            return a(k.toList((short[]) obj), i.SHORT);
        }
        if (obj instanceof int[]) {
            return a(k.toList((int[]) obj), i.INT);
        }
        if (obj instanceof long[]) {
            return a(k.toList((long[]) obj), i.LONG);
        }
        if (obj instanceof char[]) {
            return a(k.toList((char[]) obj), i.CHAR);
        }
        if (obj instanceof float[]) {
            return a(k.toList((float[]) obj), i.FLOAT);
        }
        if (obj instanceof double[]) {
            return a(k.toList((double[]) obj), i.DOUBLE);
        }
        if (obj instanceof boolean[]) {
            return a(k.toList((boolean[]) obj), i.BOOLEAN);
        }
        if (obj == null) {
            return new t();
        }
        return null;
    }
}
