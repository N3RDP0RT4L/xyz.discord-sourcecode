package d0.e0.p.d.m0.k.v;

import d0.e0.p.d.m0.b.h;
import d0.e0.p.d.m0.c.c0;
import d0.z.d.m;
import java.util.List;
import kotlin.jvm.functions.Function1;
/* compiled from: constantValues.kt */
/* loaded from: classes3.dex */
public final class b extends g<List<? extends g<?>>> {

    /* renamed from: b  reason: collision with root package name */
    public final Function1<c0, d0.e0.p.d.m0.n.c0> f3443b;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public b(List<? extends g<?>> list, Function1<? super c0, ? extends d0.e0.p.d.m0.n.c0> function1) {
        super(list);
        m.checkNotNullParameter(list, "value");
        m.checkNotNullParameter(function1, "computeType");
        this.f3443b = function1;
    }

    @Override // d0.e0.p.d.m0.k.v.g
    public d0.e0.p.d.m0.n.c0 getType(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "module");
        d0.e0.p.d.m0.n.c0 invoke = this.f3443b.invoke(c0Var);
        if (!h.isArray(invoke) && !h.isPrimitiveArray(invoke)) {
            h.isUnsignedArrayType(invoke);
        }
        return invoke;
    }
}
