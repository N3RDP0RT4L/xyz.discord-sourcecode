package d0.e0.p.d.m0.k.v;

import andhook.lib.xposed.ClassUtils;
import d0.e0.p.d.m0.c.w;
import d0.e0.p.d.m0.g.a;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.t;
import d0.o;
import d0.z.d.m;
import kotlin.Pair;
/* compiled from: constantValues.kt */
/* loaded from: classes3.dex */
public final class j extends g<Pair<? extends a, ? extends e>> {

    /* renamed from: b  reason: collision with root package name */
    public final a f3445b;
    public final e c;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public j(a aVar, e eVar) {
        super(o.to(aVar, eVar));
        m.checkNotNullParameter(aVar, "enumClassId");
        m.checkNotNullParameter(eVar, "enumEntryName");
        this.f3445b = aVar;
        this.c = eVar;
    }

    public final e getEnumEntryName() {
        return this.c;
    }

    @Override // d0.e0.p.d.m0.k.v.g
    public c0 getType(d0.e0.p.d.m0.c.c0 c0Var) {
        m.checkNotNullParameter(c0Var, "module");
        d0.e0.p.d.m0.c.e findClassAcrossModuleDependencies = w.findClassAcrossModuleDependencies(c0Var, this.f3445b);
        j0 j0Var = null;
        if (findClassAcrossModuleDependencies != null) {
            if (!d0.e0.p.d.m0.k.e.isEnumClass(findClassAcrossModuleDependencies)) {
                findClassAcrossModuleDependencies = null;
            }
            if (findClassAcrossModuleDependencies != null) {
                j0Var = findClassAcrossModuleDependencies.getDefaultType();
            }
        }
        if (j0Var != null) {
            return j0Var;
        }
        StringBuilder R = b.d.b.a.a.R("Containing class for error-class based enum entry ");
        R.append(this.f3445b);
        R.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
        R.append(this.c);
        j0 createErrorType = t.createErrorType(R.toString());
        m.checkNotNullExpressionValue(createErrorType, "createErrorType(\"Containing class for error-class based enum entry $enumClassId.$enumEntryName\")");
        return createErrorType;
    }

    @Override // d0.e0.p.d.m0.k.v.g
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f3445b.getShortClassName());
        sb.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
        sb.append(this.c);
        return sb.toString();
    }
}
