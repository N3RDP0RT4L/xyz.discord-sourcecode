package d0.e0.p.d.m0.k.v;

import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.t;
import d0.z.d.m;
import kotlin.Unit;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: constantValues.kt */
/* loaded from: classes3.dex */
public abstract class k extends g<Unit> {

    /* renamed from: b  reason: collision with root package name */
    public static final a f3446b = new a(null);

    /* compiled from: constantValues.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final k create(String str) {
            m.checkNotNullParameter(str, "message");
            return new b(str);
        }
    }

    /* compiled from: constantValues.kt */
    /* loaded from: classes3.dex */
    public static final class b extends k {
        public final String c;

        public b(String str) {
            m.checkNotNullParameter(str, "message");
            this.c = str;
        }

        @Override // d0.e0.p.d.m0.k.v.g
        public String toString() {
            return this.c;
        }

        @Override // d0.e0.p.d.m0.k.v.g
        public j0 getType(c0 c0Var) {
            m.checkNotNullParameter(c0Var, "module");
            j0 createErrorType = t.createErrorType(this.c);
            m.checkNotNullExpressionValue(createErrorType, "createErrorType(message)");
            return createErrorType;
        }
    }

    public k() {
        super(Unit.a);
    }

    @Override // d0.e0.p.d.m0.k.v.g
    public Unit getValue() {
        throw new UnsupportedOperationException();
    }
}
