package d0.e0.p.d.m0.k.v;

import b.d.b.a.a;
import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.n.j0;
import d0.z.d.m;
/* compiled from: constantValues.kt */
/* loaded from: classes3.dex */
public final class w extends g<String> {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public w(String str) {
        super(str);
        m.checkNotNullParameter(str, "value");
    }

    @Override // d0.e0.p.d.m0.k.v.g
    public String toString() {
        return a.G(a.O('\"'), getValue(), '\"');
    }

    @Override // d0.e0.p.d.m0.k.v.g
    public j0 getType(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "module");
        j0 stringType = c0Var.getBuiltIns().getStringType();
        m.checkNotNullExpressionValue(stringType, "module.builtIns.stringType");
        return stringType;
    }
}
