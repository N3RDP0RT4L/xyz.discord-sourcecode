package d0.e0.p.d.m0.k.v;

import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.j0;
import d0.t.n;
import d0.z.d.m;
import java.util.Collection;
/* compiled from: PrimitiveTypeUtil.kt */
/* loaded from: classes3.dex */
public final class u {
    public static final Collection<c0> getAllSignedLiteralTypes(d0.e0.p.d.m0.c.c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        return n.listOf((Object[]) new j0[]{c0Var.getBuiltIns().getIntType(), c0Var.getBuiltIns().getLongType(), c0Var.getBuiltIns().getByteType(), c0Var.getBuiltIns().getShortType()});
    }
}
