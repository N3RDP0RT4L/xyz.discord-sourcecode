package d0.e0.p.d.m0.k.v;

import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.n.j0;
import d0.z.d.m;
/* compiled from: constantValues.kt */
/* loaded from: classes3.dex */
public final class d extends p<Byte> {
    public d(byte b2) {
        super(Byte.valueOf(b2));
    }

    @Override // d0.e0.p.d.m0.k.v.g
    public String toString() {
        return getValue().intValue() + ".toByte()";
    }

    @Override // d0.e0.p.d.m0.k.v.g
    public j0 getType(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "module");
        j0 byteType = c0Var.getBuiltIns().getByteType();
        m.checkNotNullExpressionValue(byteType, "module.builtIns.byteType");
        return byteType;
    }
}
