package d0.e0.p.d.m0.k.v;

import d0.e0.p.d.m0.b.h;
import d0.e0.p.d.m0.b.k;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.w;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.d0;
import d0.e0.p.d.m0.n.e0;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.j1;
import d0.e0.p.d.m0.n.t;
import d0.e0.p.d.m0.n.w0;
import d0.e0.p.d.m0.n.y0;
import d0.t.u;
import d0.z.d.m;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: constantValues.kt */
/* loaded from: classes3.dex */
public final class r extends g<b> {

    /* renamed from: b  reason: collision with root package name */
    public static final a f3448b = new a(null);

    /* compiled from: constantValues.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final g<?> create(c0 c0Var) {
            m.checkNotNullParameter(c0Var, "argumentType");
            if (e0.isError(c0Var)) {
                return null;
            }
            c0 c0Var2 = c0Var;
            int i = 0;
            while (h.isArray(c0Var2)) {
                c0Var2 = ((w0) u.single((List<? extends Object>) c0Var2.getArguments())).getType();
                m.checkNotNullExpressionValue(c0Var2, "type.arguments.single().type");
                i++;
            }
            d0.e0.p.d.m0.c.h declarationDescriptor = c0Var2.getConstructor().getDeclarationDescriptor();
            if (declarationDescriptor instanceof e) {
                d0.e0.p.d.m0.g.a classId = d0.e0.p.d.m0.k.x.a.getClassId(declarationDescriptor);
                return classId == null ? new r(new b.a(c0Var)) : new r(classId, i);
            } else if (!(declarationDescriptor instanceof z0)) {
                return null;
            } else {
                d0.e0.p.d.m0.g.a aVar = d0.e0.p.d.m0.g.a.topLevel(k.a.f3189b.toSafe());
                m.checkNotNullExpressionValue(aVar, "topLevel(StandardNames.FqNames.any.toSafe())");
                return new r(aVar, 0);
            }
        }
    }

    /* compiled from: constantValues.kt */
    /* loaded from: classes3.dex */
    public static abstract class b {

        /* compiled from: constantValues.kt */
        /* loaded from: classes3.dex */
        public static final class a extends b {
            public final c0 a;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public a(c0 c0Var) {
                super(null);
                m.checkNotNullParameter(c0Var, "type");
                this.a = c0Var;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof a) && m.areEqual(this.a, ((a) obj).a);
            }

            public final c0 getType() {
                return this.a;
            }

            public int hashCode() {
                return this.a.hashCode();
            }

            public String toString() {
                StringBuilder R = b.d.b.a.a.R("LocalClass(type=");
                R.append(this.a);
                R.append(')');
                return R.toString();
            }
        }

        /* compiled from: constantValues.kt */
        /* renamed from: d0.e0.p.d.m0.k.v.r$b$b  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0345b extends b {
            public final f a;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C0345b(f fVar) {
                super(null);
                m.checkNotNullParameter(fVar, "value");
                this.a = fVar;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof C0345b) && m.areEqual(this.a, ((C0345b) obj).a);
            }

            public final int getArrayDimensions() {
                return this.a.getArrayNestedness();
            }

            public final d0.e0.p.d.m0.g.a getClassId() {
                return this.a.getClassId();
            }

            public final f getValue() {
                return this.a;
            }

            public int hashCode() {
                return this.a.hashCode();
            }

            public String toString() {
                StringBuilder R = b.d.b.a.a.R("NormalClass(value=");
                R.append(this.a);
                R.append(')');
                return R.toString();
            }
        }

        public b() {
        }

        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public r(b bVar) {
        super(bVar);
        m.checkNotNullParameter(bVar, "value");
    }

    public final c0 getArgumentType(d0.e0.p.d.m0.c.c0 c0Var) {
        m.checkNotNullParameter(c0Var, "module");
        b value = getValue();
        if (value instanceof b.a) {
            return ((b.a) getValue()).getType();
        }
        if (value instanceof b.C0345b) {
            f value2 = ((b.C0345b) getValue()).getValue();
            d0.e0.p.d.m0.g.a component1 = value2.component1();
            int component2 = value2.component2();
            e findClassAcrossModuleDependencies = w.findClassAcrossModuleDependencies(c0Var, component1);
            if (findClassAcrossModuleDependencies == null) {
                j0 createErrorType = t.createErrorType("Unresolved type: " + component1 + " (arrayDimensions=" + component2 + ')');
                m.checkNotNullExpressionValue(createErrorType, "createErrorType(\"Unresolved type: $classId (arrayDimensions=$arrayDimensions)\")");
                return createErrorType;
            }
            j0 defaultType = findClassAcrossModuleDependencies.getDefaultType();
            m.checkNotNullExpressionValue(defaultType, "descriptor.defaultType");
            c0 replaceArgumentsWithStarProjections = d0.e0.p.d.m0.n.o1.a.replaceArgumentsWithStarProjections(defaultType);
            for (int i = 0; i < component2; i++) {
                replaceArgumentsWithStarProjections = c0Var.getBuiltIns().getArrayType(j1.INVARIANT, replaceArgumentsWithStarProjections);
                m.checkNotNullExpressionValue(replaceArgumentsWithStarProjections, "module.builtIns.getArrayType(Variance.INVARIANT, type)");
            }
            return replaceArgumentsWithStarProjections;
        }
        throw new NoWhenBranchMatchedException();
    }

    @Override // d0.e0.p.d.m0.k.v.g
    public c0 getType(d0.e0.p.d.m0.c.c0 c0Var) {
        m.checkNotNullParameter(c0Var, "module");
        d0 d0Var = d0.a;
        g empty = g.f.getEMPTY();
        e kClass = c0Var.getBuiltIns().getKClass();
        m.checkNotNullExpressionValue(kClass, "module.builtIns.kClass");
        return d0.simpleNotNullType(empty, kClass, d0.t.m.listOf(new y0(getArgumentType(c0Var))));
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public r(f fVar) {
        this(new b.C0345b(fVar));
        m.checkNotNullParameter(fVar, "value");
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public r(d0.e0.p.d.m0.g.a aVar, int i) {
        this(new f(aVar, i));
        m.checkNotNullParameter(aVar, "classId");
    }
}
