package d0.e0.p.d.m0.k.v;

import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.n.j0;
import d0.z.d.m;
/* compiled from: constantValues.kt */
/* loaded from: classes3.dex */
public final class t extends g<Void> {
    public t() {
        super(null);
    }

    @Override // d0.e0.p.d.m0.k.v.g
    public j0 getType(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "module");
        j0 nullableNothingType = c0Var.getBuiltIns().getNullableNothingType();
        m.checkNotNullExpressionValue(nullableNothingType, "module.builtIns.nullableNothingType");
        return nullableNothingType;
    }
}
