package d0.e0.p.d.m0.k.v;

import d0.e0.p.d.m0.c.g1.c;
import d0.e0.p.d.m0.n.c0;
import d0.z.d.m;
/* compiled from: constantValues.kt */
/* loaded from: classes3.dex */
public final class a extends g<c> {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public a(c cVar) {
        super(cVar);
        m.checkNotNullParameter(cVar, "value");
    }

    @Override // d0.e0.p.d.m0.k.v.g
    public c0 getType(d0.e0.p.d.m0.c.c0 c0Var) {
        m.checkNotNullParameter(c0Var, "module");
        return getValue().getType();
    }
}
