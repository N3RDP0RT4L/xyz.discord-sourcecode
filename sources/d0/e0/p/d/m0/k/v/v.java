package d0.e0.p.d.m0.k.v;

import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.n.j0;
import d0.z.d.m;
/* compiled from: constantValues.kt */
/* loaded from: classes3.dex */
public final class v extends p<Short> {
    public v(short s2) {
        super(Short.valueOf(s2));
    }

    @Override // d0.e0.p.d.m0.k.v.g
    public String toString() {
        return getValue().intValue() + ".toShort()";
    }

    @Override // d0.e0.p.d.m0.k.v.g
    public j0 getType(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "module");
        j0 shortType = c0Var.getBuiltIns().getShortType();
        m.checkNotNullExpressionValue(shortType, "module.builtIns.shortType");
        return shortType;
    }
}
