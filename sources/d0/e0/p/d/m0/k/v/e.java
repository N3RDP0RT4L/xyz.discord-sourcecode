package d0.e0.p.d.m0.k.v;

import b.d.b.a.a;
import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.n.j0;
import d0.z.d.m;
/* compiled from: constantValues.kt */
/* loaded from: classes3.dex */
public final class e extends p<Character> {
    public e(char c) {
        super(Character.valueOf(c));
    }

    @Override // d0.e0.p.d.m0.k.v.g
    public String toString() {
        String str;
        Object[] objArr = new Object[2];
        boolean z2 = false;
        objArr[0] = Integer.valueOf(getValue().charValue());
        char charValue = getValue().charValue();
        if (charValue == '\b') {
            str = "\\b";
        } else if (charValue == '\t') {
            str = "\\t";
        } else if (charValue == '\n') {
            str = "\\n";
        } else if (charValue == '\f') {
            str = "\\f";
        } else if (charValue == '\r') {
            str = "\\r";
        } else {
            byte type = (byte) Character.getType(charValue);
            if (!(type == 0 || type == 13 || type == 14 || type == 15 || type == 16 || type == 18 || type == 19)) {
                z2 = true;
            }
            str = z2 ? String.valueOf(charValue) : "?";
        }
        objArr[1] = str;
        return a.N(objArr, 2, "\\u%04X ('%s')", "java.lang.String.format(this, *args)");
    }

    @Override // d0.e0.p.d.m0.k.v.g
    public j0 getType(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "module");
        j0 charType = c0Var.getBuiltIns().getCharType();
        m.checkNotNullExpressionValue(charType, "module.builtIns.charType");
        return charType;
    }
}
