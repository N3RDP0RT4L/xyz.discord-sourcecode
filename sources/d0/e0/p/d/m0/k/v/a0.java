package d0.e0.p.d.m0.k.v;

import d0.e0.p.d.m0.b.k;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.w;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.t;
import d0.z.d.m;
/* compiled from: constantValues.kt */
/* loaded from: classes3.dex */
public final class a0 extends b0<Short> {
    public a0(short s2) {
        super(Short.valueOf(s2));
    }

    @Override // d0.e0.p.d.m0.k.v.g
    public c0 getType(d0.e0.p.d.m0.c.c0 c0Var) {
        m.checkNotNullParameter(c0Var, "module");
        e findClassAcrossModuleDependencies = w.findClassAcrossModuleDependencies(c0Var, k.a.f3194f0);
        j0 defaultType = findClassAcrossModuleDependencies == null ? null : findClassAcrossModuleDependencies.getDefaultType();
        if (defaultType != null) {
            return defaultType;
        }
        j0 createErrorType = t.createErrorType("Unsigned type UShort not found");
        m.checkNotNullExpressionValue(createErrorType, "createErrorType(\"Unsigned type UShort not found\")");
        return createErrorType;
    }

    @Override // d0.e0.p.d.m0.k.v.g
    public String toString() {
        return getValue().intValue() + ".toUShort()";
    }
}
