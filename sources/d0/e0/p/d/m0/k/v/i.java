package d0.e0.p.d.m0.k.v;

import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.n.j0;
import d0.z.d.m;
/* compiled from: constantValues.kt */
/* loaded from: classes3.dex */
public final class i extends g<Double> {
    public i(double d) {
        super(Double.valueOf(d));
    }

    @Override // d0.e0.p.d.m0.k.v.g
    public String toString() {
        return getValue().doubleValue() + ".toDouble()";
    }

    @Override // d0.e0.p.d.m0.k.v.g
    public j0 getType(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "module");
        j0 doubleType = c0Var.getBuiltIns().getDoubleType();
        m.checkNotNullExpressionValue(doubleType, "module.builtIns.doubleType");
        return doubleType;
    }
}
