package d0.e0.p.d.m0.k.v;

import d0.e0.p.d.m0.n.c0;
import d0.z.d.m;
import kotlin.jvm.functions.Function1;
/* compiled from: IntegerLiteralTypeConstructor.kt */
/* loaded from: classes3.dex */
public final class o extends d0.z.d.o implements Function1<c0, CharSequence> {
    public static final o j = new o();

    public o() {
        super(1);
    }

    public final CharSequence invoke(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "it");
        return c0Var.toString();
    }
}
