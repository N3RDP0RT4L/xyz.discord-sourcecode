package d0.e0.p.d.m0.k;

import d0.e0.p.d.m0.c.b;
import d0.z.d.m;
import java.util.Collection;
/* compiled from: VisibilityUtil.kt */
/* loaded from: classes3.dex */
public final class t {
    public static final b findMemberWithMaxVisibility(Collection<? extends b> collection) {
        Integer compare;
        m.checkNotNullParameter(collection, "descriptors");
        collection.isEmpty();
        b bVar = null;
        for (b bVar2 : collection) {
            if (bVar == null || ((compare = d0.e0.p.d.m0.c.t.compare(bVar.getVisibility(), bVar2.getVisibility())) != null && compare.intValue() < 0)) {
                bVar = bVar2;
            }
        }
        m.checkNotNull(bVar);
        return bVar;
    }
}
