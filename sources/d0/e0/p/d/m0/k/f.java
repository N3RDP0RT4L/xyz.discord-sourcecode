package d0.e0.p.d.m0.k;

import d0.e0.p.d.m0.c.e;
/* compiled from: ExternalOverridabilityCondition.java */
/* loaded from: classes3.dex */
public interface f {

    /* compiled from: ExternalOverridabilityCondition.java */
    /* loaded from: classes3.dex */
    public enum a {
        CONFLICTS_ONLY,
        SUCCESS_ONLY,
        BOTH
    }

    /* compiled from: ExternalOverridabilityCondition.java */
    /* loaded from: classes3.dex */
    public enum b {
        OVERRIDABLE,
        CONFLICT,
        INCOMPATIBLE,
        UNKNOWN
    }

    a getContract();

    b isOverridable(d0.e0.p.d.m0.c.a aVar, d0.e0.p.d.m0.c.a aVar2, e eVar);
}
