package d0.e0.p.d.m0.k;

import d0.e0.p.d.m0.c.a;
import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.d;
import d0.e0.p.d.m0.c.d1;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.o0;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.j1;
import d0.t.u;
import d0.z.d.m;
import java.util.List;
/* compiled from: inlineClassesUtils.kt */
/* loaded from: classes3.dex */
public final class g {
    static {
        new b("kotlin.jvm.JvmInline");
    }

    public static final boolean isGetterOfUnderlyingPropertyOfInlineClass(a aVar) {
        m.checkNotNullParameter(aVar, "<this>");
        if (aVar instanceof o0) {
            n0 correspondingProperty = ((o0) aVar).getCorrespondingProperty();
            m.checkNotNullExpressionValue(correspondingProperty, "correspondingProperty");
            if (isUnderlyingPropertyOfInlineClass(correspondingProperty)) {
                return true;
            }
        }
        return false;
    }

    public static final boolean isInlineClass(d0.e0.p.d.m0.c.m mVar) {
        m.checkNotNullParameter(mVar, "<this>");
        if (mVar instanceof e) {
            e eVar = (e) mVar;
            if (eVar.isInline() || eVar.isValue()) {
                return true;
            }
        }
        return false;
    }

    public static final boolean isInlineClassType(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        h declarationDescriptor = c0Var.getConstructor().getDeclarationDescriptor();
        if (declarationDescriptor == null) {
            return false;
        }
        return isInlineClass(declarationDescriptor);
    }

    public static final boolean isUnderlyingPropertyOfInlineClass(d1 d1Var) {
        m.checkNotNullParameter(d1Var, "<this>");
        if (d1Var.getExtensionReceiverParameter() != null) {
            return false;
        }
        d0.e0.p.d.m0.c.m containingDeclaration = d1Var.getContainingDeclaration();
        m.checkNotNullExpressionValue(containingDeclaration, "this.containingDeclaration");
        if (!isInlineClass(containingDeclaration)) {
            return false;
        }
        c1 underlyingRepresentation = underlyingRepresentation((e) containingDeclaration);
        return m.areEqual(underlyingRepresentation == null ? null : underlyingRepresentation.getName(), d1Var.getName());
    }

    public static final c0 substitutedUnderlyingType(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        c1 unsubstitutedUnderlyingParameter = unsubstitutedUnderlyingParameter(c0Var);
        if (unsubstitutedUnderlyingParameter == null) {
            return null;
        }
        return d0.e0.p.d.m0.n.c1.create(c0Var).substitute(unsubstitutedUnderlyingParameter.getType(), j1.INVARIANT);
    }

    public static final c1 underlyingRepresentation(e eVar) {
        d unsubstitutedPrimaryConstructor;
        List<c1> valueParameters;
        m.checkNotNullParameter(eVar, "<this>");
        if (!isInlineClass(eVar) || (unsubstitutedPrimaryConstructor = eVar.getUnsubstitutedPrimaryConstructor()) == null || (valueParameters = unsubstitutedPrimaryConstructor.getValueParameters()) == null) {
            return null;
        }
        return (c1) u.singleOrNull((List<? extends Object>) valueParameters);
    }

    public static final c1 unsubstitutedUnderlyingParameter(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        h declarationDescriptor = c0Var.getConstructor().getDeclarationDescriptor();
        if (!(declarationDescriptor instanceof e)) {
            declarationDescriptor = null;
        }
        e eVar = (e) declarationDescriptor;
        if (eVar == null) {
            return null;
        }
        return underlyingRepresentation(eVar);
    }
}
