package d0.e0.p.d.m0.k;

import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.t;
import kotlin.jvm.functions.Function1;
/* compiled from: OverridingUtil.java */
/* loaded from: classes3.dex */
public final class m implements Function1<b, Boolean> {
    public final /* synthetic */ e j;

    public m(e eVar) {
        this.j = eVar;
    }

    public Boolean invoke(b bVar) {
        return Boolean.valueOf(!t.isPrivate(bVar.getVisibility()) && t.isVisibleIgnoringReceiver(bVar, this.j));
    }
}
