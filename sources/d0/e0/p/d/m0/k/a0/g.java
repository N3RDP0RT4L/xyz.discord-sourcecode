package d0.e0.p.d.m0.k.a0;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.i;
import d0.e0.p.d.m0.c.y0;
import d0.e0.p.d.m0.d.b.b;
import d0.e0.p.d.m0.g.e;
import d0.t.n;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import kotlin.jvm.functions.Function1;
/* compiled from: InnerClassesScopeWrapper.kt */
/* loaded from: classes3.dex */
public final class g extends j {

    /* renamed from: b  reason: collision with root package name */
    public final i f3430b;

    public g(i iVar) {
        m.checkNotNullParameter(iVar, "workerScope");
        this.f3430b = iVar;
    }

    @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.i
    public Set<e> getClassifierNames() {
        return this.f3430b.getClassifierNames();
    }

    @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.l
    public h getContributedClassifier(e eVar, b bVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        h contributedClassifier = this.f3430b.getContributedClassifier(eVar, bVar);
        if (contributedClassifier == null) {
            return null;
        }
        d0.e0.p.d.m0.c.e eVar2 = contributedClassifier instanceof d0.e0.p.d.m0.c.e ? (d0.e0.p.d.m0.c.e) contributedClassifier : null;
        if (eVar2 != null) {
            return eVar2;
        }
        if (contributedClassifier instanceof y0) {
            return (y0) contributedClassifier;
        }
        return null;
    }

    @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.i
    public Set<e> getFunctionNames() {
        return this.f3430b.getFunctionNames();
    }

    @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.i
    public Set<e> getVariableNames() {
        return this.f3430b.getVariableNames();
    }

    public String toString() {
        return m.stringPlus("Classes from ", this.f3430b);
    }

    @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.l
    public List<h> getContributedDescriptors(d dVar, Function1<? super e, Boolean> function1) {
        m.checkNotNullParameter(dVar, "kindFilter");
        m.checkNotNullParameter(function1, "nameFilter");
        d restrictedToKindsOrNull = dVar.restrictedToKindsOrNull(d.a.getCLASSIFIERS_MASK());
        if (restrictedToKindsOrNull == null) {
            return n.emptyList();
        }
        Collection<d0.e0.p.d.m0.c.m> contributedDescriptors = this.f3430b.getContributedDescriptors(restrictedToKindsOrNull, function1);
        ArrayList arrayList = new ArrayList();
        for (Object obj : contributedDescriptors) {
            if (obj instanceof i) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }
}
