package d0.e0.p.d.m0.k.a0;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.t0;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.p;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.p.i;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Pair;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: TypeIntersectionScope.kt */
/* loaded from: classes3.dex */
public final class o extends d0.e0.p.d.m0.k.a0.a {

    /* renamed from: b  reason: collision with root package name */
    public static final a f3436b = new a(null);
    public final i c;

    /* compiled from: TypeIntersectionScope.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final i create(String str, Collection<? extends c0> collection) {
            m.checkNotNullParameter(str, "message");
            m.checkNotNullParameter(collection, "types");
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(collection, 10));
            for (c0 c0Var : collection) {
                arrayList.add(c0Var.getMemberScope());
            }
            i<i> listOfNonEmptyScopes = d0.e0.p.d.m0.o.n.a.listOfNonEmptyScopes(arrayList);
            i createOrSingle$descriptors = d0.e0.p.d.m0.k.a0.b.f3423b.createOrSingle$descriptors(str, listOfNonEmptyScopes);
            return listOfNonEmptyScopes.size() <= 1 ? createOrSingle$descriptors : new o(str, createOrSingle$descriptors, null);
        }
    }

    /* compiled from: TypeIntersectionScope.kt */
    /* loaded from: classes3.dex */
    public static final class b extends d0.z.d.o implements Function1<d0.e0.p.d.m0.c.a, d0.e0.p.d.m0.c.a> {
        public static final b j = new b();

        public b() {
            super(1);
        }

        public final d0.e0.p.d.m0.c.a invoke(d0.e0.p.d.m0.c.a aVar) {
            m.checkNotNullParameter(aVar, "<this>");
            return aVar;
        }
    }

    /* compiled from: TypeIntersectionScope.kt */
    /* loaded from: classes3.dex */
    public static final class c extends d0.z.d.o implements Function1<t0, d0.e0.p.d.m0.c.a> {
        public static final c j = new c();

        public c() {
            super(1);
        }

        public final d0.e0.p.d.m0.c.a invoke(t0 t0Var) {
            m.checkNotNullParameter(t0Var, "<this>");
            return t0Var;
        }
    }

    /* compiled from: TypeIntersectionScope.kt */
    /* loaded from: classes3.dex */
    public static final class d extends d0.z.d.o implements Function1<n0, d0.e0.p.d.m0.c.a> {
        public static final d j = new d();

        public d() {
            super(1);
        }

        public final d0.e0.p.d.m0.c.a invoke(n0 n0Var) {
            m.checkNotNullParameter(n0Var, "<this>");
            return n0Var;
        }
    }

    public o(String str, i iVar, DefaultConstructorMarker defaultConstructorMarker) {
        this.c = iVar;
    }

    public static final i create(String str, Collection<? extends c0> collection) {
        return f3436b.create(str, collection);
    }

    @Override // d0.e0.p.d.m0.k.a0.a
    public i a() {
        return this.c;
    }

    @Override // d0.e0.p.d.m0.k.a0.a, d0.e0.p.d.m0.k.a0.l
    public Collection<d0.e0.p.d.m0.c.m> getContributedDescriptors(d0.e0.p.d.m0.k.a0.d dVar, Function1<? super e, Boolean> function1) {
        m.checkNotNullParameter(dVar, "kindFilter");
        m.checkNotNullParameter(function1, "nameFilter");
        Collection<d0.e0.p.d.m0.c.m> contributedDescriptors = super.getContributedDescriptors(dVar, function1);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (Object obj : contributedDescriptors) {
            if (((d0.e0.p.d.m0.c.m) obj) instanceof d0.e0.p.d.m0.c.a) {
                arrayList.add(obj);
            } else {
                arrayList2.add(obj);
            }
        }
        Pair pair = new Pair(arrayList, arrayList2);
        return u.plus(p.selectMostSpecificInEachOverridableGroup((List) pair.component1(), b.j), (Iterable) ((List) pair.component2()));
    }

    @Override // d0.e0.p.d.m0.k.a0.a, d0.e0.p.d.m0.k.a0.i
    public Collection<t0> getContributedFunctions(e eVar, d0.e0.p.d.m0.d.b.b bVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        return p.selectMostSpecificInEachOverridableGroup(super.getContributedFunctions(eVar, bVar), c.j);
    }

    @Override // d0.e0.p.d.m0.k.a0.a, d0.e0.p.d.m0.k.a0.i
    public Collection<n0> getContributedVariables(e eVar, d0.e0.p.d.m0.d.b.b bVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        return p.selectMostSpecificInEachOverridableGroup(super.getContributedVariables(eVar, bVar), d.j);
    }
}
