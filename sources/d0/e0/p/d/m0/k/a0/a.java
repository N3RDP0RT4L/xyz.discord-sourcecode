package d0.e0.p.d.m0.k.a0;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.t0;
import d0.e0.p.d.m0.d.b.b;
import d0.e0.p.d.m0.g.e;
import d0.z.d.m;
import java.util.Collection;
import java.util.Set;
import kotlin.jvm.functions.Function1;
/* compiled from: AbstractScopeAdapter.kt */
/* loaded from: classes3.dex */
public abstract class a implements i {
    public abstract i a();

    public final i getActualScope() {
        if (a() instanceof a) {
            return ((a) a()).getActualScope();
        }
        return a();
    }

    @Override // d0.e0.p.d.m0.k.a0.i
    public Set<e> getClassifierNames() {
        return a().getClassifierNames();
    }

    @Override // d0.e0.p.d.m0.k.a0.l
    public h getContributedClassifier(e eVar, b bVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        return a().getContributedClassifier(eVar, bVar);
    }

    @Override // d0.e0.p.d.m0.k.a0.l
    public Collection<d0.e0.p.d.m0.c.m> getContributedDescriptors(d dVar, Function1<? super e, Boolean> function1) {
        m.checkNotNullParameter(dVar, "kindFilter");
        m.checkNotNullParameter(function1, "nameFilter");
        return a().getContributedDescriptors(dVar, function1);
    }

    @Override // d0.e0.p.d.m0.k.a0.i
    public Collection<t0> getContributedFunctions(e eVar, b bVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        return a().getContributedFunctions(eVar, bVar);
    }

    @Override // d0.e0.p.d.m0.k.a0.i
    public Collection<n0> getContributedVariables(e eVar, b bVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        return a().getContributedVariables(eVar, bVar);
    }

    @Override // d0.e0.p.d.m0.k.a0.i
    public Set<e> getFunctionNames() {
        return a().getFunctionNames();
    }

    @Override // d0.e0.p.d.m0.k.a0.i
    public Set<e> getVariableNames() {
        return a().getVariableNames();
    }
}
