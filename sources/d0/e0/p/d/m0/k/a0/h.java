package d0.e0.p.d.m0.k.a0;

import d0.e0.p.d.m0.m.j;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.jvm.functions.Function0;
/* compiled from: LazyScopeAdapter.kt */
/* loaded from: classes3.dex */
public final class h extends d0.e0.p.d.m0.k.a0.a {

    /* renamed from: b  reason: collision with root package name */
    public final j<i> f3431b;

    /* compiled from: LazyScopeAdapter.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function0<i> {
        public final /* synthetic */ Function0<i> $getScope;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        /* JADX WARN: Multi-variable type inference failed */
        public a(Function0<? extends i> function0) {
            super(0);
            this.$getScope = function0;
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final i invoke() {
            i invoke = this.$getScope.invoke();
            return invoke instanceof d0.e0.p.d.m0.k.a0.a ? ((d0.e0.p.d.m0.k.a0.a) invoke).getActualScope() : invoke;
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ h(d0.e0.p.d.m0.m.o r1, kotlin.jvm.functions.Function0 r2, int r3, kotlin.jvm.internal.DefaultConstructorMarker r4) {
        /*
            r0 = this;
            r3 = r3 & 1
            if (r3 == 0) goto Lb
            d0.e0.p.d.m0.m.o r1 = d0.e0.p.d.m0.m.f.f3486b
            java.lang.String r3 = "NO_LOCKS"
            d0.z.d.m.checkNotNullExpressionValue(r1, r3)
        Lb:
            r0.<init>(r1, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.k.a0.h.<init>(d0.e0.p.d.m0.m.o, kotlin.jvm.functions.Function0, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public h(Function0<? extends i> function0) {
        this(null, function0, 1, null);
        m.checkNotNullParameter(function0, "getScope");
    }

    @Override // d0.e0.p.d.m0.k.a0.a
    public i a() {
        return this.f3431b.invoke();
    }

    public h(d0.e0.p.d.m0.m.o oVar, Function0<? extends i> function0) {
        m.checkNotNullParameter(oVar, "storageManager");
        m.checkNotNullParameter(function0, "getScope");
        this.f3431b = oVar.createLazyValue(new a(function0));
    }
}
