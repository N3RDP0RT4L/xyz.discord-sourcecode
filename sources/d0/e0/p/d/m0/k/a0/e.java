package d0.e0.p.d.m0.k.a0;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.t0;
import d0.e0.p.d.m0.c.x;
import d0.e0.p.d.m0.k.a0.l;
import d0.e0.p.d.m0.k.k;
import d0.e0.p.d.m0.m.j;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.p.i;
import d0.t.n;
import d0.t.r;
import d0.t.u;
import d0.z.d.a0;
import d0.z.d.o;
import d0.z.d.y;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.reflect.KProperty;
/* compiled from: GivenFunctionsMemberScope.kt */
/* loaded from: classes3.dex */
public abstract class e extends j {

    /* renamed from: b  reason: collision with root package name */
    public static final /* synthetic */ KProperty<Object>[] f3428b = {a0.property1(new y(a0.getOrCreateKotlinClass(e.class), "allDescriptors", "getAllDescriptors()Ljava/util/List;"))};
    public final d0.e0.p.d.m0.c.e c;
    public final j d;

    /* compiled from: GivenFunctionsMemberScope.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function0<List<? extends m>> {
        public a() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final List<? extends m> invoke() {
            List<x> a = e.this.a();
            return u.plus((Collection) a, (Iterable) e.access$createFakeOverrides(e.this, a));
        }
    }

    public e(d0.e0.p.d.m0.m.o oVar, d0.e0.p.d.m0.c.e eVar) {
        d0.z.d.m.checkNotNullParameter(oVar, "storageManager");
        d0.z.d.m.checkNotNullParameter(eVar, "containingClass");
        this.c = eVar;
        this.d = oVar.createLazyValue(new a());
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r5v4 */
    /* JADX WARN: Type inference failed for: r5v6, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r5v7, types: [java.util.ArrayList] */
    public static final List access$createFakeOverrides(e eVar, List list) {
        ?? r5;
        Objects.requireNonNull(eVar);
        ArrayList arrayList = new ArrayList(3);
        Collection<c0> supertypes = eVar.c.getTypeConstructor().getSupertypes();
        d0.z.d.m.checkNotNullExpressionValue(supertypes, "containingClass.typeConstructor.supertypes");
        ArrayList arrayList2 = new ArrayList();
        for (c0 c0Var : supertypes) {
            r.addAll(arrayList2, l.a.getContributedDescriptors$default(c0Var.getMemberScope(), null, null, 3, null));
        }
        ArrayList arrayList3 = new ArrayList();
        Iterator it = arrayList2.iterator();
        while (it.hasNext()) {
            Object next = it.next();
            if (next instanceof b) {
                arrayList3.add(next);
            }
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        Iterator it2 = arrayList3.iterator();
        while (it2.hasNext()) {
            Object next2 = it2.next();
            d0.e0.p.d.m0.g.e name = ((b) next2).getName();
            Object obj = linkedHashMap.get(name);
            if (obj == null) {
                obj = new ArrayList();
                linkedHashMap.put(name, obj);
            }
            ((List) obj).add(next2);
        }
        for (Map.Entry entry : linkedHashMap.entrySet()) {
            d0.e0.p.d.m0.g.e eVar2 = (d0.e0.p.d.m0.g.e) entry.getKey();
            LinkedHashMap linkedHashMap2 = new LinkedHashMap();
            for (Object obj2 : (List) entry.getValue()) {
                Boolean valueOf = Boolean.valueOf(((b) obj2) instanceof x);
                Object obj3 = linkedHashMap2.get(valueOf);
                if (obj3 == null) {
                    obj3 = new ArrayList();
                    linkedHashMap2.put(valueOf, obj3);
                }
                ((List) obj3).add(obj2);
            }
            for (Map.Entry entry2 : linkedHashMap2.entrySet()) {
                boolean booleanValue = ((Boolean) entry2.getKey()).booleanValue();
                List list2 = (List) entry2.getValue();
                k kVar = k.f3440b;
                if (booleanValue) {
                    r5 = new ArrayList();
                    for (Object obj4 : list) {
                        if (d0.z.d.m.areEqual(((x) obj4).getName(), eVar2)) {
                            r5.add(obj4);
                        }
                    }
                } else {
                    r5 = n.emptyList();
                }
                kVar.generateOverridesInFunctionGroup(eVar2, list2, r5, eVar.c, new f(arrayList, eVar));
            }
        }
        return d0.e0.p.d.m0.p.a.compact(arrayList);
    }

    public abstract List<x> a();

    public final List<m> b() {
        return (List) d0.e0.p.d.m0.m.n.getValue(this.d, this, f3428b[0]);
    }

    @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.l
    public Collection<m> getContributedDescriptors(d dVar, Function1<? super d0.e0.p.d.m0.g.e, Boolean> function1) {
        d0.z.d.m.checkNotNullParameter(dVar, "kindFilter");
        d0.z.d.m.checkNotNullParameter(function1, "nameFilter");
        return !dVar.acceptsKinds(d.n.getKindMask()) ? n.emptyList() : b();
    }

    @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.i
    public Collection<t0> getContributedFunctions(d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.d.b.b bVar) {
        d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        d0.z.d.m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        List<m> b2 = b();
        i iVar = new i();
        for (Object obj : b2) {
            if ((obj instanceof t0) && d0.z.d.m.areEqual(((t0) obj).getName(), eVar)) {
                iVar.add(obj);
            }
        }
        return iVar;
    }

    @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.i
    public Collection<n0> getContributedVariables(d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.d.b.b bVar) {
        d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        d0.z.d.m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        List<m> b2 = b();
        i iVar = new i();
        for (Object obj : b2) {
            if ((obj instanceof n0) && d0.z.d.m.areEqual(((n0) obj).getName(), eVar)) {
                iVar.add(obj);
            }
        }
        return iVar;
    }
}
