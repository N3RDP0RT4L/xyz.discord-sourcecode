package d0.e0.p.d.m0.k.a0;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: MemberScope.kt */
/* loaded from: classes3.dex */
public final class d {
    public static final a a;

    /* renamed from: b  reason: collision with root package name */
    public static int f3425b = 1;
    public static final int c;
    public static final int d;
    public static final int e;
    public static final int f;
    public static final int g;
    public static final int h;
    public static final int i;
    public static final int j;
    public static final int k;
    public static final int l;
    public static final d m;
    public static final d n;
    public static final d o;
    public static final d p;
    public static final d q;
    public static final List<a.C0342a> r;

    /* renamed from: s  reason: collision with root package name */
    public static final List<a.C0342a> f3426s;
    public final List<c> t;
    public final int u;

    /* compiled from: MemberScope.kt */
    /* loaded from: classes3.dex */
    public static final class a {

        /* compiled from: MemberScope.kt */
        /* renamed from: d0.e0.p.d.m0.k.a0.d$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0342a {
            public final int a;

            /* renamed from: b  reason: collision with root package name */
            public final String f3427b;

            public C0342a(int i, String str) {
                m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
                this.a = i;
                this.f3427b = str;
            }

            public final int getMask() {
                return this.a;
            }

            public final String getName() {
                return this.f3427b;
            }
        }

        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public static final int access$nextMask(a aVar) {
            Objects.requireNonNull(aVar);
            int i = d.f3425b;
            d.f3425b <<= 1;
            return i;
        }

        public final int getALL_KINDS_MASK() {
            return d.i;
        }

        public final int getCALLABLES_MASK() {
            return d.l;
        }

        public final int getCLASSIFIERS_MASK() {
            return d.j;
        }

        public final int getFUNCTIONS_MASK() {
            return d.g;
        }

        public final int getNON_SINGLETON_CLASSIFIERS_MASK() {
            return d.c;
        }

        public final int getPACKAGES_MASK() {
            return d.f;
        }

        public final int getSINGLETON_CLASSIFIERS_MASK() {
            return d.d;
        }

        public final int getTYPE_ALIASES_MASK() {
            return d.e;
        }

        public final int getVALUES_MASK() {
            return d.k;
        }

        public final int getVARIABLES_MASK() {
            return d.h;
        }
    }

    static {
        a.C0342a aVar;
        a.C0342a aVar2;
        a aVar3 = new a(null);
        a = aVar3;
        c = a.access$nextMask(aVar3);
        d = a.access$nextMask(aVar3);
        e = a.access$nextMask(aVar3);
        f = a.access$nextMask(aVar3);
        g = a.access$nextMask(aVar3);
        h = a.access$nextMask(aVar3);
        i = a.access$nextMask(aVar3) - 1;
        j = aVar3.getNON_SINGLETON_CLASSIFIERS_MASK() | aVar3.getSINGLETON_CLASSIFIERS_MASK() | aVar3.getTYPE_ALIASES_MASK();
        k = aVar3.getSINGLETON_CLASSIFIERS_MASK() | aVar3.getFUNCTIONS_MASK() | aVar3.getVARIABLES_MASK();
        l = aVar3.getFUNCTIONS_MASK() | aVar3.getVARIABLES_MASK();
        m = new d(aVar3.getALL_KINDS_MASK(), null, 2, null);
        n = new d(aVar3.getCALLABLES_MASK(), null, 2, null);
        new d(aVar3.getNON_SINGLETON_CLASSIFIERS_MASK(), null, 2, null);
        new d(aVar3.getSINGLETON_CLASSIFIERS_MASK(), null, 2, null);
        new d(aVar3.getTYPE_ALIASES_MASK(), null, 2, null);
        o = new d(aVar3.getCLASSIFIERS_MASK(), null, 2, null);
        new d(aVar3.getPACKAGES_MASK(), null, 2, null);
        p = new d(aVar3.getFUNCTIONS_MASK(), null, 2, null);
        q = new d(aVar3.getVARIABLES_MASK(), null, 2, null);
        new d(aVar3.getVALUES_MASK(), null, 2, null);
        Field[] fields = d.class.getFields();
        m.checkNotNullExpressionValue(fields, "T::class.java.fields");
        ArrayList<Field> arrayList = new ArrayList();
        for (Field field : fields) {
            if (Modifier.isStatic(field.getModifiers())) {
                arrayList.add(field);
            }
        }
        ArrayList arrayList2 = new ArrayList();
        for (Field field2 : arrayList) {
            Object obj = field2.get(null);
            d dVar = obj instanceof d ? (d) obj : null;
            if (dVar != null) {
                int kindMask = dVar.getKindMask();
                String name = field2.getName();
                m.checkNotNullExpressionValue(name, "field.name");
                aVar2 = new a.C0342a(kindMask, name);
            } else {
                aVar2 = null;
            }
            if (aVar2 != null) {
                arrayList2.add(aVar2);
            }
        }
        r = arrayList2;
        Field[] fields2 = d.class.getFields();
        m.checkNotNullExpressionValue(fields2, "T::class.java.fields");
        ArrayList arrayList3 = new ArrayList();
        for (Field field3 : fields2) {
            if (Modifier.isStatic(field3.getModifiers())) {
                arrayList3.add(field3);
            }
        }
        ArrayList<Field> arrayList4 = new ArrayList();
        for (Object obj2 : arrayList3) {
            if (m.areEqual(((Field) obj2).getType(), Integer.TYPE)) {
                arrayList4.add(obj2);
            }
        }
        ArrayList arrayList5 = new ArrayList();
        for (Field field4 : arrayList4) {
            Object obj3 = field4.get(null);
            Objects.requireNonNull(obj3, "null cannot be cast to non-null type kotlin.Int");
            int intValue = ((Integer) obj3).intValue();
            if (intValue == ((-intValue) & intValue)) {
                String name2 = field4.getName();
                m.checkNotNullExpressionValue(name2, "field.name");
                aVar = new a.C0342a(intValue, name2);
            } else {
                aVar = null;
            }
            if (aVar != null) {
                arrayList5.add(aVar);
            }
        }
        f3426s = arrayList5;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public d(int i2, List<? extends c> list) {
        m.checkNotNullParameter(list, "excludes");
        this.t = list;
        for (c cVar : list) {
            i2 &= ~cVar.getFullyExcludedDescriptorKinds();
        }
        this.u = i2;
    }

    public final boolean acceptsKinds(int i2) {
        return (i2 & this.u) != 0;
    }

    public final List<c> getExcludes() {
        return this.t;
    }

    public final int getKindMask() {
        return this.u;
    }

    public final d restrictedToKindsOrNull(int i2) {
        int i3 = i2 & this.u;
        if (i3 == 0) {
            return null;
        }
        return new d(i3, this.t);
    }

    public String toString() {
        Object obj;
        boolean z2;
        Iterator<T> it = r.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (((a.C0342a) obj).getMask() == getKindMask()) {
                z2 = true;
                continue;
            } else {
                z2 = false;
                continue;
            }
            if (z2) {
                break;
            }
        }
        a.C0342a aVar = (a.C0342a) obj;
        String name = aVar == null ? null : aVar.getName();
        if (name == null) {
            List<a.C0342a> list = f3426s;
            ArrayList arrayList = new ArrayList();
            for (a.C0342a aVar2 : list) {
                String name2 = acceptsKinds(aVar2.getMask()) ? aVar2.getName() : null;
                if (name2 != null) {
                    arrayList.add(name2);
                }
            }
            name = u.joinToString$default(arrayList, " | ", null, null, 0, null, null, 62, null);
        }
        StringBuilder W = b.d.b.a.a.W("DescriptorKindFilter(", name, ", ");
        W.append(this.t);
        W.append(')');
        return W.toString();
    }

    public /* synthetic */ d(int i2, List list, int i3, DefaultConstructorMarker defaultConstructorMarker) {
        this(i2, (i3 & 2) != 0 ? n.emptyList() : list);
    }
}
