package d0.e0.p.d.m0.k.a0;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.f;
import d0.e0.p.d.m0.c.t0;
import d0.e0.p.d.m0.d.b.b;
import d0.e0.p.d.m0.k.d;
import d0.e0.p.d.m0.m.j;
import d0.e0.p.d.m0.p.i;
import d0.t.n;
import d0.z.d.a0;
import d0.z.d.o;
import d0.z.d.y;
import java.util.List;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.reflect.KProperty;
/* compiled from: StaticScopeForKotlinEnum.kt */
/* loaded from: classes3.dex */
public final class m extends j {

    /* renamed from: b  reason: collision with root package name */
    public static final /* synthetic */ KProperty<Object>[] f3434b = {a0.property1(new y(a0.getOrCreateKotlinClass(m.class), "functions", "getFunctions()Ljava/util/List;"))};
    public final e c;
    public final j d;

    /* compiled from: StaticScopeForKotlinEnum.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function0<List<? extends t0>> {
        public a() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final List<? extends t0> invoke() {
            return n.listOf((Object[]) new t0[]{d.createEnumValueOfMethod(m.this.c), d.createEnumValuesMethod(m.this.c)});
        }
    }

    public m(d0.e0.p.d.m0.m.o oVar, e eVar) {
        d0.z.d.m.checkNotNullParameter(oVar, "storageManager");
        d0.z.d.m.checkNotNullParameter(eVar, "containingClass");
        this.c = eVar;
        eVar.getKind();
        f fVar = f.ENUM_CLASS;
        this.d = oVar.createLazyValue(new a());
    }

    @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.l
    public Void getContributedClassifier(d0.e0.p.d.m0.g.e eVar, b bVar) {
        d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        d0.z.d.m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        return null;
    }

    @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.l
    public List<t0> getContributedDescriptors(d dVar, Function1<? super d0.e0.p.d.m0.g.e, Boolean> function1) {
        d0.z.d.m.checkNotNullParameter(dVar, "kindFilter");
        d0.z.d.m.checkNotNullParameter(function1, "nameFilter");
        return (List) d0.e0.p.d.m0.m.n.getValue(this.d, this, f3434b[0]);
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.i
    public i<t0> getContributedFunctions(d0.e0.p.d.m0.g.e eVar, b bVar) {
        d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        d0.z.d.m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        i<t0> iVar = new i<>();
        for (Object obj : (List) d0.e0.p.d.m0.m.n.getValue(this.d, this, f3434b[0])) {
            if (d0.z.d.m.areEqual(((t0) obj).getName(), eVar)) {
                iVar.add(obj);
            }
        }
        return iVar;
    }
}
