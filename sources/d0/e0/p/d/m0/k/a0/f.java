package d0.e0.p.d.m0.k.a0;

import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.k.i;
import d0.e0.p.d.m0.k.k;
import java.util.ArrayList;
/* compiled from: GivenFunctionsMemberScope.kt */
/* loaded from: classes3.dex */
public final class f extends i {
    public final /* synthetic */ ArrayList<m> a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ e f3429b;

    public f(ArrayList<m> arrayList, e eVar) {
        this.a = arrayList;
        this.f3429b = eVar;
    }

    @Override // d0.e0.p.d.m0.k.j
    public void addFakeOverride(b bVar) {
        d0.z.d.m.checkNotNullParameter(bVar, "fakeOverride");
        k.resolveUnknownVisibilityForMember(bVar, null);
        this.a.add(bVar);
    }

    @Override // d0.e0.p.d.m0.k.i
    public void conflict(b bVar, b bVar2) {
        d0.z.d.m.checkNotNullParameter(bVar, "fromSuper");
        d0.z.d.m.checkNotNullParameter(bVar2, "fromCurrent");
        throw new IllegalStateException(("Conflict in scope of " + this.f3429b.c + ": " + bVar + " vs " + bVar2).toString());
    }
}
