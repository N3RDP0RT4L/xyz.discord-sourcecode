package d0.e0.p.d.m0.k.a0;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.t0;
import d0.e0.p.d.m0.d.b.b;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.p.d;
import d0.t.n;
import d0.z.d.m;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import kotlin.jvm.functions.Function1;
/* compiled from: MemberScopeImpl.kt */
/* loaded from: classes3.dex */
public abstract class j implements i {
    @Override // d0.e0.p.d.m0.k.a0.i
    public Set<e> getClassifierNames() {
        return null;
    }

    @Override // d0.e0.p.d.m0.k.a0.l
    public h getContributedClassifier(e eVar, b bVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        return null;
    }

    @Override // d0.e0.p.d.m0.k.a0.l
    public Collection<d0.e0.p.d.m0.c.m> getContributedDescriptors(d dVar, Function1<? super e, Boolean> function1) {
        m.checkNotNullParameter(dVar, "kindFilter");
        m.checkNotNullParameter(function1, "nameFilter");
        return n.emptyList();
    }

    @Override // d0.e0.p.d.m0.k.a0.i
    public Collection<? extends t0> getContributedFunctions(e eVar, b bVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        return n.emptyList();
    }

    @Override // d0.e0.p.d.m0.k.a0.i
    public Collection<? extends n0> getContributedVariables(e eVar, b bVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        return n.emptyList();
    }

    @Override // d0.e0.p.d.m0.k.a0.i
    public Set<e> getFunctionNames() {
        Collection<d0.e0.p.d.m0.c.m> contributedDescriptors = getContributedDescriptors(d.p, d.alwaysTrue());
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        for (Object obj : contributedDescriptors) {
            if (obj instanceof t0) {
                e name = ((t0) obj).getName();
                m.checkNotNullExpressionValue(name, "it.name");
                linkedHashSet.add(name);
            }
        }
        return linkedHashSet;
    }

    @Override // d0.e0.p.d.m0.k.a0.i
    public Set<e> getVariableNames() {
        Collection<d0.e0.p.d.m0.c.m> contributedDescriptors = getContributedDescriptors(d.q, d.alwaysTrue());
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        for (Object obj : contributedDescriptors) {
            if (obj instanceof t0) {
                e name = ((t0) obj).getName();
                m.checkNotNullExpressionValue(name, "it.name");
                linkedHashSet.add(name);
            }
        }
        return linkedHashSet;
    }
}
