package d0.e0.p.d.m0.k.a0;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.t0;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.p.i;
import d0.t.k;
import d0.t.n;
import d0.t.n0;
import d0.t.r;
import d0.z.d.m;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ChainedMemberScope.kt */
/* loaded from: classes3.dex */
public final class b implements i {

    /* renamed from: b  reason: collision with root package name */
    public static final a f3423b = new a(null);
    public final String c;
    public final i[] d;

    /* compiled from: ChainedMemberScope.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final i create(String str, Iterable<? extends i> iterable) {
            m.checkNotNullParameter(str, "debugName");
            m.checkNotNullParameter(iterable, "scopes");
            i iVar = new i();
            for (i iVar2 : iterable) {
                if (iVar2 != i.b.f3433b) {
                    if (iVar2 instanceof b) {
                        r.addAll(iVar, ((b) iVar2).d);
                    } else {
                        iVar.add(iVar2);
                    }
                }
            }
            return createOrSingle$descriptors(str, iVar);
        }

        public final i createOrSingle$descriptors(String str, List<? extends i> list) {
            m.checkNotNullParameter(str, "debugName");
            m.checkNotNullParameter(list, "scopes");
            int size = list.size();
            if (size == 0) {
                return i.b.f3433b;
            }
            if (size == 1) {
                return list.get(0);
            }
            Object[] array = list.toArray(new i[0]);
            Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
            return new b(str, (i[]) array, null);
        }
    }

    public b(String str, i[] iVarArr, DefaultConstructorMarker defaultConstructorMarker) {
        this.c = str;
        this.d = iVarArr;
    }

    @Override // d0.e0.p.d.m0.k.a0.i
    public Set<e> getClassifierNames() {
        return k.flatMapClassifierNamesOrNull(k.asIterable(this.d));
    }

    @Override // d0.e0.p.d.m0.k.a0.l
    public h getContributedClassifier(e eVar, d0.e0.p.d.m0.d.b.b bVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        i[] iVarArr = this.d;
        int length = iVarArr.length;
        h hVar = null;
        int i = 0;
        while (i < length) {
            i iVar = iVarArr[i];
            i++;
            h contributedClassifier = iVar.getContributedClassifier(eVar, bVar);
            if (contributedClassifier != null) {
                if (!(contributedClassifier instanceof d0.e0.p.d.m0.c.i) || !((d0.e0.p.d.m0.c.i) contributedClassifier).isExpect()) {
                    return contributedClassifier;
                }
                if (hVar == null) {
                    hVar = contributedClassifier;
                }
            }
        }
        return hVar;
    }

    @Override // d0.e0.p.d.m0.k.a0.l
    public Collection<d0.e0.p.d.m0.c.m> getContributedDescriptors(d dVar, Function1<? super e, Boolean> function1) {
        m.checkNotNullParameter(dVar, "kindFilter");
        m.checkNotNullParameter(function1, "nameFilter");
        i[] iVarArr = this.d;
        int length = iVarArr.length;
        if (length == 0) {
            return n.emptyList();
        }
        int i = 0;
        if (length == 1) {
            return iVarArr[0].getContributedDescriptors(dVar, function1);
        }
        Collection<d0.e0.p.d.m0.c.m> collection = null;
        int length2 = iVarArr.length;
        while (i < length2) {
            i iVar = iVarArr[i];
            i++;
            collection = d0.e0.p.d.m0.o.n.a.concat(collection, iVar.getContributedDescriptors(dVar, function1));
        }
        return collection == null ? n0.emptySet() : collection;
    }

    @Override // d0.e0.p.d.m0.k.a0.i
    public Collection<t0> getContributedFunctions(e eVar, d0.e0.p.d.m0.d.b.b bVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        i[] iVarArr = this.d;
        int length = iVarArr.length;
        if (length == 0) {
            return n.emptyList();
        }
        int i = 0;
        if (length == 1) {
            return iVarArr[0].getContributedFunctions(eVar, bVar);
        }
        Collection<t0> collection = null;
        int length2 = iVarArr.length;
        while (i < length2) {
            i iVar = iVarArr[i];
            i++;
            collection = d0.e0.p.d.m0.o.n.a.concat(collection, iVar.getContributedFunctions(eVar, bVar));
        }
        return collection == null ? n0.emptySet() : collection;
    }

    @Override // d0.e0.p.d.m0.k.a0.i
    public Collection<d0.e0.p.d.m0.c.n0> getContributedVariables(e eVar, d0.e0.p.d.m0.d.b.b bVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        i[] iVarArr = this.d;
        int length = iVarArr.length;
        if (length == 0) {
            return n.emptyList();
        }
        int i = 0;
        if (length == 1) {
            return iVarArr[0].getContributedVariables(eVar, bVar);
        }
        Collection<d0.e0.p.d.m0.c.n0> collection = null;
        int length2 = iVarArr.length;
        while (i < length2) {
            i iVar = iVarArr[i];
            i++;
            collection = d0.e0.p.d.m0.o.n.a.concat(collection, iVar.getContributedVariables(eVar, bVar));
        }
        return collection == null ? n0.emptySet() : collection;
    }

    @Override // d0.e0.p.d.m0.k.a0.i
    public Set<e> getFunctionNames() {
        i[] iVarArr = this.d;
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        for (i iVar : iVarArr) {
            r.addAll(linkedHashSet, iVar.getFunctionNames());
        }
        return linkedHashSet;
    }

    @Override // d0.e0.p.d.m0.k.a0.i
    public Set<e> getVariableNames() {
        i[] iVarArr = this.d;
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        for (i iVar : iVarArr) {
            r.addAll(linkedHashSet, iVar.getVariableNames());
        }
        return linkedHashSet;
    }

    public String toString() {
        return this.c;
    }
}
