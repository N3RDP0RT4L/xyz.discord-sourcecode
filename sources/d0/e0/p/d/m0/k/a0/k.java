package d0.e0.p.d.m0.k.a0;

import d0.e0.p.d.m0.g.e;
import d0.t.r;
import d0.z.d.m;
import java.util.HashSet;
import java.util.Set;
/* compiled from: MemberScope.kt */
/* loaded from: classes3.dex */
public final class k {
    public static final Set<e> flatMapClassifierNamesOrNull(Iterable<? extends i> iterable) {
        m.checkNotNullParameter(iterable, "<this>");
        HashSet hashSet = new HashSet();
        for (i iVar : iterable) {
            Set<e> classifierNames = iVar.getClassifierNames();
            if (classifierNames == null) {
                return null;
            }
            r.addAll(hashSet, classifierNames);
        }
        return hashSet;
    }
}
