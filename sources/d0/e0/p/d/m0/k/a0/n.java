package d0.e0.p.d.m0.k.a0;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.t0;
import d0.e0.p.d.m0.c.w0;
import d0.e0.p.d.m0.d.b.b;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.a0.l;
import d0.e0.p.d.m0.k.u.a.d;
import d0.e0.p.d.m0.n.c1;
import d0.e0.p.d.m0.n.z0;
import d0.g;
import d0.z.d.o;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import kotlin.Lazy;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: SubstitutingScope.kt */
/* loaded from: classes3.dex */
public final class n implements i {

    /* renamed from: b  reason: collision with root package name */
    public final i f3435b;
    public final c1 c;
    public Map<m, m> d;
    public final Lazy e = g.lazy(new a());

    /* compiled from: SubstitutingScope.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function0<Collection<? extends m>> {
        public a() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final Collection<? extends m> invoke() {
            n nVar = n.this;
            return nVar.a(l.a.getContributedDescriptors$default(nVar.f3435b, null, null, 3, null));
        }
    }

    public n(i iVar, c1 c1Var) {
        d0.z.d.m.checkNotNullParameter(iVar, "workerScope");
        d0.z.d.m.checkNotNullParameter(c1Var, "givenSubstitutor");
        this.f3435b = iVar;
        z0 substitution = c1Var.getSubstitution();
        d0.z.d.m.checkNotNullExpressionValue(substitution, "givenSubstitutor.substitution");
        this.c = d.wrapWithCapturingSubstitution$default(substitution, false, 1, null).buildSubstitutor();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final <D extends m> Collection<D> a(Collection<? extends D> collection) {
        if (this.c.isEmpty() || collection.isEmpty()) {
            return collection;
        }
        LinkedHashSet newLinkedHashSetWithExpectedSize = d0.e0.p.d.m0.p.a.newLinkedHashSetWithExpectedSize(collection.size());
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            newLinkedHashSetWithExpectedSize.add(b((m) it.next()));
        }
        return newLinkedHashSetWithExpectedSize;
    }

    public final <D extends m> D b(D d) {
        if (this.c.isEmpty()) {
            return d;
        }
        if (this.d == null) {
            this.d = new HashMap();
        }
        Map<m, m> map = this.d;
        d0.z.d.m.checkNotNull(map);
        m mVar = map.get(d);
        if (mVar == null) {
            if (d instanceof w0) {
                mVar = ((w0) d).substitute(this.c);
                if (mVar != null) {
                    map.put(d, mVar);
                } else {
                    throw new AssertionError("We expect that no conflict should happen while substitution is guaranteed to generate invariant projection, but " + d + " substitution fails");
                }
            } else {
                throw new IllegalStateException(d0.z.d.m.stringPlus("Unknown descriptor in scope: ", d).toString());
            }
        }
        return (D) mVar;
    }

    @Override // d0.e0.p.d.m0.k.a0.i
    public Set<e> getClassifierNames() {
        return this.f3435b.getClassifierNames();
    }

    @Override // d0.e0.p.d.m0.k.a0.l
    public h getContributedClassifier(e eVar, b bVar) {
        d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        d0.z.d.m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        h contributedClassifier = this.f3435b.getContributedClassifier(eVar, bVar);
        if (contributedClassifier == null) {
            return null;
        }
        return (h) b(contributedClassifier);
    }

    @Override // d0.e0.p.d.m0.k.a0.l
    public Collection<m> getContributedDescriptors(d dVar, Function1<? super e, Boolean> function1) {
        d0.z.d.m.checkNotNullParameter(dVar, "kindFilter");
        d0.z.d.m.checkNotNullParameter(function1, "nameFilter");
        return (Collection) this.e.getValue();
    }

    @Override // d0.e0.p.d.m0.k.a0.i
    public Collection<? extends t0> getContributedFunctions(e eVar, b bVar) {
        d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        d0.z.d.m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        return a(this.f3435b.getContributedFunctions(eVar, bVar));
    }

    @Override // d0.e0.p.d.m0.k.a0.i
    public Collection<? extends n0> getContributedVariables(e eVar, b bVar) {
        d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        d0.z.d.m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        return a(this.f3435b.getContributedVariables(eVar, bVar));
    }

    @Override // d0.e0.p.d.m0.k.a0.i
    public Set<e> getFunctionNames() {
        return this.f3435b.getFunctionNames();
    }

    @Override // d0.e0.p.d.m0.k.a0.i
    public Set<e> getVariableNames() {
        return this.f3435b.getVariableNames();
    }
}
