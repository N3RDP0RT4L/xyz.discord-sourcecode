package d0.e0.p.d.m0.k.a0;

import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.d.b.b;
import d0.e0.p.d.m0.g.e;
import java.util.Collection;
import kotlin.jvm.functions.Function1;
/* compiled from: ResolutionScope.kt */
/* loaded from: classes3.dex */
public interface l {

    /* compiled from: ResolutionScope.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ Collection getContributedDescriptors$default(l lVar, d dVar, Function1 function1, int i, Object obj) {
            if (obj == null) {
                if ((i & 1) != 0) {
                    dVar = d.m;
                }
                Function1<e, Boolean> function12 = function1;
                if ((i & 2) != 0) {
                    function12 = i.a.getALL_NAME_FILTER();
                }
                return lVar.getContributedDescriptors(dVar, function12);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getContributedDescriptors");
        }
    }

    h getContributedClassifier(e eVar, b bVar);

    Collection<m> getContributedDescriptors(d dVar, Function1<? super e, Boolean> function1);
}
