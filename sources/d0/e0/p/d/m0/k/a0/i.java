package d0.e0.p.d.m0.k.a0;

import d0.e0.p.d.m0.c.t0;
import d0.e0.p.d.m0.g.e;
import d0.t.n0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Collection;
import java.util.Set;
import kotlin.jvm.functions.Function1;
/* compiled from: MemberScope.kt */
/* loaded from: classes3.dex */
public interface i extends l {
    public static final a a = a.a;

    /* compiled from: MemberScope.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public static final /* synthetic */ a a = new a();

        /* renamed from: b  reason: collision with root package name */
        public static final Function1<e, Boolean> f3432b = C0343a.j;

        /* compiled from: MemberScope.kt */
        /* renamed from: d0.e0.p.d.m0.k.a0.i$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0343a extends o implements Function1<e, Boolean> {
            public static final C0343a j = new C0343a();

            public C0343a() {
                super(1);
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Boolean invoke(e eVar) {
                return Boolean.valueOf(invoke2(eVar));
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final boolean invoke2(e eVar) {
                m.checkNotNullParameter(eVar, "it");
                return true;
            }
        }

        public final Function1<e, Boolean> getALL_NAME_FILTER() {
            return f3432b;
        }
    }

    /* compiled from: MemberScope.kt */
    /* loaded from: classes3.dex */
    public static final class b extends j {

        /* renamed from: b  reason: collision with root package name */
        public static final b f3433b = new b();

        @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.i
        public Set<e> getClassifierNames() {
            return n0.emptySet();
        }

        @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.i
        public Set<e> getFunctionNames() {
            return n0.emptySet();
        }

        @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.i
        public Set<e> getVariableNames() {
            return n0.emptySet();
        }
    }

    Set<e> getClassifierNames();

    Collection<? extends t0> getContributedFunctions(e eVar, d0.e0.p.d.m0.d.b.b bVar);

    Collection<? extends d0.e0.p.d.m0.c.n0> getContributedVariables(e eVar, d0.e0.p.d.m0.d.b.b bVar);

    Set<e> getFunctionNames();

    Set<e> getVariableNames();
}
