package d0.e0.p.d.m0.k.a0.p;

import b.d.b.a.a;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.n.j0;
import d0.z.d.m;
/* compiled from: ImplicitClassReceiver.kt */
/* loaded from: classes3.dex */
public class c implements d, f {
    public final e a;

    /* renamed from: b  reason: collision with root package name */
    public final e f3438b;

    public c(e eVar, c cVar) {
        m.checkNotNullParameter(eVar, "classDescriptor");
        this.a = eVar;
        this.f3438b = eVar;
    }

    public boolean equals(Object obj) {
        e eVar = this.a;
        e eVar2 = null;
        c cVar = obj instanceof c ? (c) obj : null;
        if (cVar != null) {
            eVar2 = cVar.a;
        }
        return m.areEqual(eVar, eVar2);
    }

    @Override // d0.e0.p.d.m0.k.a0.p.f
    public final e getClassDescriptor() {
        return this.a;
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    public String toString() {
        StringBuilder R = a.R("Class{");
        R.append(getType());
        R.append('}');
        return R.toString();
    }

    @Override // d0.e0.p.d.m0.k.a0.p.d
    public j0 getType() {
        j0 defaultType = this.a.getDefaultType();
        m.checkNotNullExpressionValue(defaultType, "classDescriptor.defaultType");
        return defaultType;
    }
}
