package d0.e0.p.d.m0.k;

import d0.e0.p.d.m0.c.m;
import d0.z.d.o;
import kotlin.jvm.functions.Function2;
/* compiled from: DescriptorEquivalenceForOverrides.kt */
/* loaded from: classes3.dex */
public final class c extends o implements Function2<m, m, Boolean> {
    public static final c j = new c();

    public c() {
        super(2);
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Boolean invoke(m mVar, m mVar2) {
        return Boolean.valueOf(invoke2(mVar, mVar2));
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final boolean invoke2(m mVar, m mVar2) {
        return false;
    }
}
