package d0.e0.p.d.m0.k;

import d0.e0.p.d.m0.p.j;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: overridingUtils.kt */
/* loaded from: classes3.dex */
public final class p {

    /* compiled from: overridingUtils.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<H, Unit> {
        public final /* synthetic */ j<H> $conflictedHandles;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(j<H> jVar) {
            super(1);
            this.$conflictedHandles = jVar;
        }

        @Override // kotlin.jvm.functions.Function1
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(H h) {
            AbstractCollection abstractCollection = this.$conflictedHandles;
            m.checkNotNullExpressionValue(h, "it");
            abstractCollection.add(h);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static final <H> Collection<H> selectMostSpecificInEachOverridableGroup(Collection<? extends H> collection, Function1<? super H, ? extends d0.e0.p.d.m0.c.a> function1) {
        m.checkNotNullParameter(collection, "<this>");
        m.checkNotNullParameter(function1, "descriptorByHandle");
        if (collection.size() <= 1) {
            return collection;
        }
        LinkedList linkedList = new LinkedList(collection);
        j create = j.j.create();
        while (!linkedList.isEmpty()) {
            Object first = u.first((List<? extends Object>) linkedList);
            j create2 = j.j.create();
            Collection<? super H> extractMembersOverridableInBothWays = k.extractMembersOverridableInBothWays(first, linkedList, function1, new a(create2));
            m.checkNotNullExpressionValue(extractMembersOverridableInBothWays, "val conflictedHandles = SmartSet.create<H>()\n\n        val overridableGroup =\n            OverridingUtil.extractMembersOverridableInBothWays(nextHandle, queue, descriptorByHandle) { conflictedHandles.add(it) }");
            if (extractMembersOverridableInBothWays.size() != 1 || !create2.isEmpty()) {
                Object obj = (Object) k.selectMostSpecificMember(extractMembersOverridableInBothWays, function1);
                m.checkNotNullExpressionValue(obj, "selectMostSpecificMember(overridableGroup, descriptorByHandle)");
                d0.e0.p.d.m0.c.a invoke = function1.invoke(obj);
                for (Object obj2 : extractMembersOverridableInBothWays) {
                    m.checkNotNullExpressionValue(obj2, "it");
                    if (!k.isMoreSpecific(invoke, function1.invoke(obj2))) {
                        create2.add(obj2);
                    }
                }
                if (!create2.isEmpty()) {
                    create.addAll(create2);
                }
                create.add(obj);
            } else {
                Object single = u.single(extractMembersOverridableInBothWays);
                m.checkNotNullExpressionValue(single, "overridableGroup.single()");
                create.add(single);
            }
        }
        return create;
    }
}
