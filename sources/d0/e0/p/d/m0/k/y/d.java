package d0.e0.p.d.m0.k.y;

import androidx.exifinterface.media.ExifInterface;
import b.d.b.a.a;
import com.swift.sandhook.annotation.MethodReflectParams;
import d0.e0.p.d.m0.b.i;
import d0.e0.p.d.m0.g.b;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
/* compiled from: JvmPrimitiveType.java */
/* loaded from: classes3.dex */
public enum d {
    BOOLEAN(i.BOOLEAN, MethodReflectParams.BOOLEAN, "Z", "java.lang.Boolean"),
    CHAR(i.CHAR, MethodReflectParams.CHAR, "C", "java.lang.Character"),
    BYTE(i.BYTE, MethodReflectParams.BYTE, "B", "java.lang.Byte"),
    SHORT(i.SHORT, MethodReflectParams.SHORT, ExifInterface.LATITUDE_SOUTH, "java.lang.Short"),
    INT(i.INT, MethodReflectParams.INT, "I", "java.lang.Integer"),
    FLOAT(i.FLOAT, MethodReflectParams.FLOAT, "F", "java.lang.Float"),
    LONG(i.LONG, "long", "J", "java.lang.Long"),
    DOUBLE(i.DOUBLE, MethodReflectParams.DOUBLE, "D", "java.lang.Double");
    
    public static final Set<b> r = new HashSet();

    /* renamed from: s  reason: collision with root package name */
    public static final Map<String, d> f3451s = new HashMap();
    public static final Map<i, d> t = new EnumMap(i.class);
    public static final Map<String, d> u = new HashMap();
    private final String desc;
    private final String name;
    private final i primitiveType;
    private final b wrapperFqName;

    static {
        d[] values = values();
        for (int i = 0; i < 8; i++) {
            d dVar = values[i];
            r.add(dVar.getWrapperFqName());
            f3451s.put(dVar.getJavaKeywordName(), dVar);
            t.put(dVar.getPrimitiveType(), dVar);
            u.put(dVar.getDesc(), dVar);
        }
    }

    d(i iVar, String str, String str2, String str3) {
        if (iVar != null) {
            this.primitiveType = iVar;
            this.name = str;
            this.desc = str2;
            this.wrapperFqName = new b(str3);
            return;
        }
        f(6);
        throw null;
    }

    /* JADX WARN: Removed duplicated region for block: B:11:0x0015  */
    /* JADX WARN: Removed duplicated region for block: B:15:0x0020  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0025  */
    /* JADX WARN: Removed duplicated region for block: B:17:0x002a  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x002f  */
    /* JADX WARN: Removed duplicated region for block: B:19:0x0034  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x0039  */
    /* JADX WARN: Removed duplicated region for block: B:21:0x003c  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x0045 A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:33:0x0066  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x006b  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x0070  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x0075  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x007d A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:41:0x0082  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static /* synthetic */ void f(int r7) {
        /*
            r0 = 4
            r1 = 2
            if (r7 == r1) goto Lc
            if (r7 == r0) goto Lc
            switch(r7) {
                case 10: goto Lc;
                case 11: goto Lc;
                case 12: goto Lc;
                case 13: goto Lc;
                default: goto L9;
            }
        L9:
            java.lang.String r2 = "Argument for @NotNull parameter '%s' of %s.%s must not be null"
            goto Le
        Lc:
            java.lang.String r2 = "@NotNull method %s.%s must not return null"
        Le:
            if (r7 == r1) goto L17
            if (r7 == r0) goto L17
            switch(r7) {
                case 10: goto L17;
                case 11: goto L17;
                case 12: goto L17;
                case 13: goto L17;
                default: goto L15;
            }
        L15:
            r3 = 3
            goto L18
        L17:
            r3 = 2
        L18:
            java.lang.Object[] r3 = new java.lang.Object[r3]
            java.lang.String r4 = "kotlin/reflect/jvm/internal/impl/resolve/jvm/JvmPrimitiveType"
            r5 = 0
            switch(r7) {
                case 1: goto L3c;
                case 2: goto L39;
                case 3: goto L34;
                case 4: goto L39;
                case 5: goto L2f;
                case 6: goto L2a;
                case 7: goto L3c;
                case 8: goto L2f;
                case 9: goto L25;
                case 10: goto L39;
                case 11: goto L39;
                case 12: goto L39;
                case 13: goto L39;
                default: goto L20;
            }
        L20:
            java.lang.String r6 = "className"
            r3[r5] = r6
            goto L40
        L25:
            java.lang.String r6 = "wrapperClassName"
            r3[r5] = r6
            goto L40
        L2a:
            java.lang.String r6 = "primitiveType"
            r3[r5] = r6
            goto L40
        L2f:
            java.lang.String r6 = "desc"
            r3[r5] = r6
            goto L40
        L34:
            java.lang.String r6 = "type"
            r3[r5] = r6
            goto L40
        L39:
            r3[r5] = r4
            goto L40
        L3c:
            java.lang.String r6 = "name"
            r3[r5] = r6
        L40:
            java.lang.String r5 = "get"
            r6 = 1
            if (r7 == r1) goto L61
            if (r7 == r0) goto L61
            switch(r7) {
                case 10: goto L5c;
                case 11: goto L57;
                case 12: goto L52;
                case 13: goto L4d;
                default: goto L4a;
            }
        L4a:
            r3[r6] = r4
            goto L63
        L4d:
            java.lang.String r4 = "getWrapperFqName"
            r3[r6] = r4
            goto L63
        L52:
            java.lang.String r4 = "getDesc"
            r3[r6] = r4
            goto L63
        L57:
            java.lang.String r4 = "getJavaKeywordName"
            r3[r6] = r4
            goto L63
        L5c:
            java.lang.String r4 = "getPrimitiveType"
            r3[r6] = r4
            goto L63
        L61:
            r3[r6] = r5
        L63:
            switch(r7) {
                case 1: goto L75;
                case 2: goto L77;
                case 3: goto L75;
                case 4: goto L77;
                case 5: goto L70;
                case 6: goto L6b;
                case 7: goto L6b;
                case 8: goto L6b;
                case 9: goto L6b;
                case 10: goto L77;
                case 11: goto L77;
                case 12: goto L77;
                case 13: goto L77;
                default: goto L66;
            }
        L66:
            java.lang.String r4 = "isWrapperClassName"
            r3[r1] = r4
            goto L77
        L6b:
            java.lang.String r4 = "<init>"
            r3[r1] = r4
            goto L77
        L70:
            java.lang.String r4 = "getByDesc"
            r3[r1] = r4
            goto L77
        L75:
            r3[r1] = r5
        L77:
            java.lang.String r2 = java.lang.String.format(r2, r3)
            if (r7 == r1) goto L88
            if (r7 == r0) goto L88
            switch(r7) {
                case 10: goto L88;
                case 11: goto L88;
                case 12: goto L88;
                case 13: goto L88;
                default: goto L82;
            }
        L82:
            java.lang.IllegalArgumentException r7 = new java.lang.IllegalArgumentException
            r7.<init>(r2)
            goto L8d
        L88:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            r7.<init>(r2)
        L8d:
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.k.y.d.f(int):void");
    }

    public static d get(String str) {
        if (str != null) {
            d dVar = f3451s.get(str);
            if (dVar != null) {
                return dVar;
            }
            throw new AssertionError(a.v("Non-primitive type name passed: ", str));
        }
        f(1);
        throw null;
    }

    public String getDesc() {
        String str = this.desc;
        if (str != null) {
            return str;
        }
        f(12);
        throw null;
    }

    public String getJavaKeywordName() {
        String str = this.name;
        if (str != null) {
            return str;
        }
        f(11);
        throw null;
    }

    public i getPrimitiveType() {
        i iVar = this.primitiveType;
        if (iVar != null) {
            return iVar;
        }
        f(10);
        throw null;
    }

    public b getWrapperFqName() {
        b bVar = this.wrapperFqName;
        if (bVar != null) {
            return bVar;
        }
        f(13);
        throw null;
    }

    public static d get(i iVar) {
        if (iVar != null) {
            d dVar = t.get(iVar);
            if (dVar != null) {
                return dVar;
            }
            f(4);
            throw null;
        }
        f(3);
        throw null;
    }
}
