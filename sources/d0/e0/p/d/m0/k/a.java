package d0.e0.p.d.m0.k;

import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.e0;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.c.z;
import d0.e0.p.d.m0.k.a0.d;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.k.a0.l;
import d0.t.n;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
/* compiled from: SealedClassInheritorsProvider.kt */
/* loaded from: classes3.dex */
public final class a extends s {
    public static final a a = new a();

    /* JADX WARN: Multi-variable type inference failed */
    public static final void a(e eVar, LinkedHashSet<e> linkedHashSet, i iVar, boolean z2) {
        for (m mVar : l.a.getContributedDescriptors$default(iVar, d.o, null, 2, null)) {
            if (mVar instanceof e) {
                e eVar2 = (e) mVar;
                if (e.isDirectSubclass(eVar2, eVar)) {
                    linkedHashSet.add(mVar);
                }
                if (z2) {
                    i unsubstitutedInnerClassesScope = eVar2.getUnsubstitutedInnerClassesScope();
                    d0.z.d.m.checkNotNullExpressionValue(unsubstitutedInnerClassesScope, "descriptor.unsubstitutedInnerClassesScope");
                    a(eVar, linkedHashSet, unsubstitutedInnerClassesScope, z2);
                }
            }
        }
    }

    public Collection<e> computeSealedSubclasses(e eVar, boolean z2) {
        m mVar;
        m mVar2;
        d0.z.d.m.checkNotNullParameter(eVar, "sealedClass");
        if (eVar.getModality() != z.SEALED) {
            return n.emptyList();
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        if (!z2) {
            mVar = eVar.getContainingDeclaration();
        } else {
            Iterator<m> it = d0.e0.p.d.m0.k.x.a.getParents(eVar).iterator();
            while (true) {
                if (!it.hasNext()) {
                    mVar2 = null;
                    break;
                }
                mVar2 = it.next();
                if (mVar2 instanceof e0) {
                    break;
                }
            }
            mVar = mVar2;
        }
        if (mVar instanceof e0) {
            a(eVar, linkedHashSet, ((e0) mVar).getMemberScope(), z2);
        }
        i unsubstitutedInnerClassesScope = eVar.getUnsubstitutedInnerClassesScope();
        d0.z.d.m.checkNotNullExpressionValue(unsubstitutedInnerClassesScope, "sealedClass.unsubstitutedInnerClassesScope");
        a(eVar, linkedHashSet, unsubstitutedInnerClassesScope, true);
        return linkedHashSet;
    }
}
