package d0.e0.p.d.m0.k;

import d0.e0.p.d.m0.c.b;
import d0.z.d.m;
import java.util.Collection;
/* compiled from: OverridingStrategy.kt */
/* loaded from: classes3.dex */
public abstract class j {
    public abstract void addFakeOverride(b bVar);

    public abstract void inheritanceConflict(b bVar, b bVar2);

    public abstract void overrideConflict(b bVar, b bVar2);

    public void setOverriddenDescriptors(b bVar, Collection<? extends b> collection) {
        m.checkNotNullParameter(bVar, "member");
        m.checkNotNullParameter(collection, "overridden");
        bVar.setOverriddenDescriptors(collection);
    }
}
