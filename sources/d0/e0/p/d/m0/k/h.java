package d0.e0.p.d.m0.k;

import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.l;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.x;
import d0.e0.p.d.m0.c.y0;
import java.util.Comparator;
/* compiled from: MemberComparator.java */
/* loaded from: classes3.dex */
public class h implements Comparator<m> {
    public static final h j = new h();

    public static int a(m mVar) {
        if (e.isEnumEntry(mVar)) {
            return 8;
        }
        if (mVar instanceof l) {
            return 7;
        }
        if (mVar instanceof n0) {
            return ((n0) mVar).getExtensionReceiverParameter() == null ? 6 : 5;
        }
        if (mVar instanceof x) {
            return ((x) mVar).getExtensionReceiverParameter() == null ? 4 : 3;
        }
        if (mVar instanceof e) {
            return 2;
        }
        return mVar instanceof y0 ? 1 : 0;
    }

    public int compare(m mVar, m mVar2) {
        Integer num;
        int a = a(mVar2) - a(mVar);
        if (a != 0) {
            num = Integer.valueOf(a);
        } else if (!e.isEnumEntry(mVar) || !e.isEnumEntry(mVar2)) {
            int compareTo = mVar.getName().compareTo(mVar2.getName());
            num = compareTo != 0 ? Integer.valueOf(compareTo) : null;
        } else {
            num = 0;
        }
        if (num != null) {
            return num.intValue();
        }
        return 0;
    }
}
