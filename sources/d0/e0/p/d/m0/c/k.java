package d0.e0.p.d.m0.c;
/* compiled from: ConstUtil.kt */
/* loaded from: classes3.dex */
public final class k {
    /* JADX WARN: Code restructure failed: missing block: B:10:0x0020, code lost:
        return false;
     */
    /* JADX WARN: Code restructure failed: missing block: B:11:0x0022, code lost:
        return true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:5:0x0011, code lost:
        if (d0.e0.p.d.m0.b.o.isUnsignedType(r1) != false) goto L6;
     */
    /* JADX WARN: Code restructure failed: missing block: B:7:0x0017, code lost:
        if (d0.e0.p.d.m0.n.e1.isNullableType(r1) != false) goto L8;
     */
    /* JADX WARN: Code restructure failed: missing block: B:9:0x001d, code lost:
        if (d0.e0.p.d.m0.b.h.isString(r1) == false) goto L10;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final boolean canBeUsedForConstVal(d0.e0.p.d.m0.n.c0 r1) {
        /*
            java.lang.String r0 = "<this>"
            d0.z.d.m.checkNotNullParameter(r1, r0)
            boolean r0 = d0.e0.p.d.m0.b.h.isPrimitiveType(r1)
            if (r0 != 0) goto L13
            d0.e0.p.d.m0.b.o r0 = d0.e0.p.d.m0.b.o.a
            boolean r0 = d0.e0.p.d.m0.b.o.isUnsignedType(r1)
            if (r0 == 0) goto L19
        L13:
            boolean r0 = d0.e0.p.d.m0.n.e1.isNullableType(r1)
            if (r0 == 0) goto L22
        L19:
            boolean r1 = d0.e0.p.d.m0.b.h.isString(r1)
            if (r1 == 0) goto L20
            goto L22
        L20:
            r1 = 0
            goto L23
        L22:
            r1 = 1
        L23:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.c.k.canBeUsedForConstVal(d0.e0.p.d.m0.n.c0):boolean");
    }
}
