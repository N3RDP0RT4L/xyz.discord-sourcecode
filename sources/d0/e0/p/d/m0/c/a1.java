package d0.e0.p.d.m0.c;

import d0.e0.p.d.m0.k.e;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.t;
import d0.e0.p.d.m0.n.u0;
import d0.e0.p.d.m0.n.w0;
import d0.f0.q;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.jvm.functions.Function1;
import kotlin.sequences.Sequence;
/* compiled from: typeParameterUtils.kt */
/* loaded from: classes3.dex */
public final class a1 {

    /* compiled from: typeParameterUtils.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<m, Boolean> {
        public static final a j = new a();

        public a() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Boolean invoke(m mVar) {
            return Boolean.valueOf(invoke2(mVar));
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final boolean invoke2(m mVar) {
            m.checkNotNullParameter(mVar, "it");
            return mVar instanceof d0.e0.p.d.m0.c.a;
        }
    }

    /* compiled from: typeParameterUtils.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function1<m, Boolean> {
        public static final b j = new b();

        public b() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Boolean invoke(m mVar) {
            return Boolean.valueOf(invoke2(mVar));
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final boolean invoke2(m mVar) {
            m.checkNotNullParameter(mVar, "it");
            return !(mVar instanceof l);
        }
    }

    /* compiled from: typeParameterUtils.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function1<m, Sequence<? extends z0>> {
        public static final c j = new c();

        public c() {
            super(1);
        }

        public final Sequence<z0> invoke(m mVar) {
            m.checkNotNullParameter(mVar, "it");
            List<z0> typeParameters = ((d0.e0.p.d.m0.c.a) mVar).getTypeParameters();
            m.checkNotNullExpressionValue(typeParameters, "it as CallableDescriptor).typeParameters");
            return u.asSequence(typeParameters);
        }
    }

    public static final l0 a(c0 c0Var, i iVar, int i) {
        i iVar2 = null;
        if (iVar == null || t.isError(iVar)) {
            return null;
        }
        int size = iVar.getDeclaredTypeParameters().size() + i;
        if (!iVar.isInner()) {
            if (size != c0Var.getArguments().size()) {
                e.isLocal(iVar);
            }
            return new l0(iVar, c0Var.getArguments().subList(i, c0Var.getArguments().size()), null);
        }
        List<w0> subList = c0Var.getArguments().subList(i, size);
        m containingDeclaration = iVar.getContainingDeclaration();
        if (containingDeclaration instanceof i) {
            iVar2 = (i) containingDeclaration;
        }
        return new l0(iVar, subList, a(c0Var, iVar2, size));
    }

    public static final l0 buildPossiblyInnerType(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        h declarationDescriptor = c0Var.getConstructor().getDeclarationDescriptor();
        return a(c0Var, declarationDescriptor instanceof i ? (i) declarationDescriptor : null, 0);
    }

    public static final List<z0> computeConstructorTypeParameters(i iVar) {
        List<z0> list;
        m mVar;
        u0 typeConstructor;
        m.checkNotNullParameter(iVar, "<this>");
        List<z0> declaredTypeParameters = iVar.getDeclaredTypeParameters();
        m.checkNotNullExpressionValue(declaredTypeParameters, "declaredTypeParameters");
        if (!(iVar.isInner() || (iVar.getContainingDeclaration() instanceof d0.e0.p.d.m0.c.a))) {
            return declaredTypeParameters;
        }
        List list2 = q.toList(q.flatMap(q.filter(q.takeWhile(d0.e0.p.d.m0.k.x.a.getParents(iVar), a.j), b.j), c.j));
        Iterator<m> it = d0.e0.p.d.m0.k.x.a.getParents(iVar).iterator();
        while (true) {
            list = null;
            if (!it.hasNext()) {
                mVar = null;
                break;
            }
            mVar = it.next();
            if (mVar instanceof e) {
                break;
            }
        }
        e eVar = (e) mVar;
        if (!(eVar == null || (typeConstructor = eVar.getTypeConstructor()) == null)) {
            list = typeConstructor.getParameters();
        }
        if (list == null) {
            list = n.emptyList();
        }
        if (!list2.isEmpty() || !list.isEmpty()) {
            List<z0> plus = u.plus((Collection) list2, (Iterable) list);
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(plus, 10));
            for (z0 z0Var : plus) {
                m.checkNotNullExpressionValue(z0Var, "it");
                arrayList.add(new d0.e0.p.d.m0.c.c(z0Var, iVar, declaredTypeParameters.size()));
            }
            return u.plus((Collection) declaredTypeParameters, (Iterable) arrayList);
        }
        List<z0> declaredTypeParameters2 = iVar.getDeclaredTypeParameters();
        m.checkNotNullExpressionValue(declaredTypeParameters2, "declaredTypeParameters");
        return declaredTypeParameters2;
    }
}
