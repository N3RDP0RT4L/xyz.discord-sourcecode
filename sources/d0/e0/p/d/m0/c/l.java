package d0.e0.p.d.m0.c;

import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.c1;
import java.util.List;
/* compiled from: ConstructorDescriptor.java */
/* loaded from: classes3.dex */
public interface l extends x {
    e getConstructedClass();

    @Override // d0.e0.p.d.m0.c.x, d0.e0.p.d.m0.c.n, d0.e0.p.d.m0.c.m
    i getContainingDeclaration();

    @Override // d0.e0.p.d.m0.c.a
    c0 getReturnType();

    @Override // d0.e0.p.d.m0.c.a
    List<z0> getTypeParameters();

    boolean isPrimary();

    @Override // d0.e0.p.d.m0.c.x, d0.e0.p.d.m0.c.w0
    l substitute(c1 c1Var);
}
