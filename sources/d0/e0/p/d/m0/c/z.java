package d0.e0.p.d.m0.c;

import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: Modality.kt */
/* loaded from: classes3.dex */
public enum z {
    FINAL,
    SEALED,
    OPEN,
    ABSTRACT;
    
    public static final a j = new a(null);

    /* compiled from: Modality.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final z convertFromFlags(boolean z2, boolean z3, boolean z4) {
            if (z2) {
                return z.SEALED;
            }
            if (z3) {
                return z.ABSTRACT;
            }
            if (z4) {
                return z.OPEN;
            }
            return z.FINAL;
        }
    }
}
