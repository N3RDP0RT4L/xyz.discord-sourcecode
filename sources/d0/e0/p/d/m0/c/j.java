package d0.e0.p.d.m0.c;

import d0.e0.p.d.m0.n.c0;
import d0.z.d.m;
/* compiled from: ConstUtil.kt */
/* loaded from: classes3.dex */
public final class j {
    public static final boolean canBeUsedForConstVal(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "type");
        return k.canBeUsedForConstVal(c0Var);
    }
}
