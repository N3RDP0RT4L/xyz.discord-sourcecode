package d0.e0.p.d.m0.c;

import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.u0;
/* compiled from: ClassifierDescriptor.java */
/* loaded from: classes3.dex */
public interface h extends n {
    j0 getDefaultType();

    @Override // d0.e0.p.d.m0.c.m
    h getOriginal();

    u0 getTypeConstructor();
}
