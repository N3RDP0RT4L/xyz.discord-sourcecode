package d0.e0.p.d.m0.c.h1;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.d;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.t0;
import d0.e0.p.d.m0.n.c0;
import d0.t.n;
import d0.z.d.m;
import java.util.Collection;
/* compiled from: AdditionalClassPartsProvider.kt */
/* loaded from: classes3.dex */
public interface a {

    /* compiled from: AdditionalClassPartsProvider.kt */
    /* renamed from: d0.e0.p.d.m0.c.h1.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0292a implements a {
        public static final C0292a a = new C0292a();

        @Override // d0.e0.p.d.m0.c.h1.a
        public Collection<d> getConstructors(e eVar) {
            m.checkNotNullParameter(eVar, "classDescriptor");
            return n.emptyList();
        }

        @Override // d0.e0.p.d.m0.c.h1.a
        public Collection<t0> getFunctions(d0.e0.p.d.m0.g.e eVar, e eVar2) {
            m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkNotNullParameter(eVar2, "classDescriptor");
            return n.emptyList();
        }

        @Override // d0.e0.p.d.m0.c.h1.a
        public Collection<d0.e0.p.d.m0.g.e> getFunctionsNames(e eVar) {
            m.checkNotNullParameter(eVar, "classDescriptor");
            return n.emptyList();
        }

        @Override // d0.e0.p.d.m0.c.h1.a
        public Collection<c0> getSupertypes(e eVar) {
            m.checkNotNullParameter(eVar, "classDescriptor");
            return n.emptyList();
        }
    }

    Collection<d> getConstructors(e eVar);

    Collection<t0> getFunctions(d0.e0.p.d.m0.g.e eVar, e eVar2);

    Collection<d0.e0.p.d.m0.g.e> getFunctionsNames(e eVar);

    Collection<c0> getSupertypes(e eVar);
}
