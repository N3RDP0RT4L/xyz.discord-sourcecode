package d0.e0.p.d.m0.c.h1;

import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.g.a;
import java.util.Collection;
/* compiled from: ClassDescriptorFactory.kt */
/* loaded from: classes3.dex */
public interface b {
    e createClass(a aVar);

    Collection<e> getAllContributedClassesIfPossible(d0.e0.p.d.m0.g.b bVar);

    boolean shouldCreateClass(d0.e0.p.d.m0.g.b bVar, d0.e0.p.d.m0.g.e eVar);
}
