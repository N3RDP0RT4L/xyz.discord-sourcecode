package d0.e0.p.d.m0.c.k1.b;

import b.d.b.a.a;
import d0.e0.p.d.m0.c.k1.b.f;
import d0.e0.p.d.m0.e.a.k0.y;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.e;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.List;
/* compiled from: ReflectJavaTypeParameter.kt */
/* loaded from: classes3.dex */
public final class x extends n implements f, y {
    public final TypeVariable<?> a;

    public x(TypeVariable<?> typeVariable) {
        m.checkNotNullParameter(typeVariable, "typeVariable");
        this.a = typeVariable;
    }

    public boolean equals(Object obj) {
        return (obj instanceof x) && m.areEqual(this.a, ((x) obj).a);
    }

    @Override // d0.e0.p.d.m0.e.a.k0.d
    public c findAnnotation(b bVar) {
        return f.a.findAnnotation(this, bVar);
    }

    @Override // d0.e0.p.d.m0.c.k1.b.f
    public AnnotatedElement getElement() {
        TypeVariable<?> typeVariable = this.a;
        if (typeVariable instanceof AnnotatedElement) {
            return (AnnotatedElement) typeVariable;
        }
        return null;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.t
    public e getName() {
        e identifier = e.identifier(this.a.getName());
        m.checkNotNullExpressionValue(identifier, "identifier(typeVariable.name)");
        return identifier;
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    @Override // d0.e0.p.d.m0.e.a.k0.d
    public boolean isDeprecatedInJavaDoc() {
        return f.a.isDeprecatedInJavaDoc(this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        a.i0(x.class, sb, ": ");
        sb.append(this.a);
        return sb.toString();
    }

    @Override // d0.e0.p.d.m0.e.a.k0.d
    public List<c> getAnnotations() {
        return f.a.getAnnotations(this);
    }

    @Override // d0.e0.p.d.m0.e.a.k0.y
    public List<l> getUpperBounds() {
        Type[] bounds = this.a.getBounds();
        m.checkNotNullExpressionValue(bounds, "typeVariable.bounds");
        ArrayList arrayList = new ArrayList(bounds.length);
        for (Type type : bounds) {
            arrayList.add(new l(type));
        }
        l lVar = (l) u.singleOrNull((List<? extends Object>) arrayList);
        return m.areEqual(lVar == null ? null : lVar.getReflectType(), Object.class) ? n.emptyList() : arrayList;
    }
}
