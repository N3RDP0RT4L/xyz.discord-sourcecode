package d0.e0.p.d.m0.c.k1.a;

import d0.z.d.m;
/* compiled from: ReflectJavaClassFinder.kt */
/* loaded from: classes3.dex */
public final class e {
    public static final Class<?> tryLoadClass(ClassLoader classLoader, String str) {
        m.checkNotNullParameter(classLoader, "<this>");
        m.checkNotNullParameter(str, "fqName");
        try {
            return Class.forName(str, false, classLoader);
        } catch (ClassNotFoundException unused) {
            return null;
        }
    }
}
