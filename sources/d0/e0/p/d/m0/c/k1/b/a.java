package d0.e0.p.d.m0.c.k1.b;

import d0.z.d.m;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
/* compiled from: ReflectJavaMember.kt */
/* loaded from: classes3.dex */
public final class a {
    public static final a a = new a();

    /* renamed from: b  reason: collision with root package name */
    public static C0296a f3261b;

    /* compiled from: ReflectJavaMember.kt */
    /* renamed from: d0.e0.p.d.m0.c.k1.b.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0296a {
        public final Method a;

        /* renamed from: b  reason: collision with root package name */
        public final Method f3262b;

        public C0296a(Method method, Method method2) {
            this.a = method;
            this.f3262b = method2;
        }

        public final Method getGetName() {
            return this.f3262b;
        }

        public final Method getGetParameters() {
            return this.a;
        }
    }

    public final C0296a buildCache(Member member) {
        m.checkNotNullParameter(member, "member");
        Class<?> cls = member.getClass();
        try {
            return new C0296a(cls.getMethod("getParameters", new Class[0]), b.getSafeClassLoader(cls).loadClass("java.lang.reflect.Parameter").getMethod("getName", new Class[0]));
        } catch (NoSuchMethodException unused) {
            return new C0296a(null, null);
        }
    }

    public final List<String> loadParameterNames(Member member) {
        Method getName;
        m.checkNotNullParameter(member, "member");
        C0296a aVar = f3261b;
        if (aVar == null) {
            aVar = buildCache(member);
            f3261b = aVar;
        }
        Method getParameters = aVar.getGetParameters();
        if (getParameters == null || (getName = aVar.getGetName()) == null) {
            return null;
        }
        Object invoke = getParameters.invoke(member, new Object[0]);
        Objects.requireNonNull(invoke, "null cannot be cast to non-null type kotlin.Array<*>");
        Object[] objArr = (Object[]) invoke;
        ArrayList arrayList = new ArrayList(objArr.length);
        for (Object obj : objArr) {
            Object invoke2 = getName.invoke(obj, new Object[0]);
            Objects.requireNonNull(invoke2, "null cannot be cast to non-null type kotlin.String");
            arrayList.add((String) invoke2);
        }
        return arrayList;
    }
}
