package d0.e0.p.d.m0.c.k1.b;

import b.d.b.a.a;
import d0.e0.p.d.m0.c.k1.b.w;
import d0.e0.p.d.m0.e.a.k0.i;
import d0.e0.p.d.m0.e.a.k0.j;
import d0.e0.p.d.m0.e.a.k0.x;
import d0.e0.p.d.m0.g.b;
import d0.t.n;
import d0.t.o;
import d0.z.d.m;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
/* compiled from: ReflectJavaClassifierType.kt */
/* loaded from: classes3.dex */
public final class l extends w implements j {

    /* renamed from: b  reason: collision with root package name */
    public final Type f3266b;
    public final i c;

    public l(Type type) {
        i iVar;
        m.checkNotNullParameter(type, "reflectType");
        this.f3266b = type;
        Type reflectType = getReflectType();
        if (reflectType instanceof Class) {
            iVar = new j((Class) reflectType);
        } else if (reflectType instanceof TypeVariable) {
            iVar = new x((TypeVariable) reflectType);
        } else if (reflectType instanceof ParameterizedType) {
            Type rawType = ((ParameterizedType) reflectType).getRawType();
            Objects.requireNonNull(rawType, "null cannot be cast to non-null type java.lang.Class<*>");
            iVar = new j((Class) rawType);
        } else {
            StringBuilder R = a.R("Not a classifier type (");
            R.append(reflectType.getClass());
            R.append("): ");
            R.append(reflectType);
            throw new IllegalStateException(R.toString());
        }
        this.c = iVar;
    }

    @Override // d0.e0.p.d.m0.c.k1.b.w, d0.e0.p.d.m0.e.a.k0.d
    public d0.e0.p.d.m0.e.a.k0.a findAnnotation(b bVar) {
        m.checkNotNullParameter(bVar, "fqName");
        return null;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.d
    public Collection<d0.e0.p.d.m0.e.a.k0.a> getAnnotations() {
        return n.emptyList();
    }

    @Override // d0.e0.p.d.m0.e.a.k0.j
    public i getClassifier() {
        return this.c;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.j
    public String getClassifierQualifiedName() {
        throw new UnsupportedOperationException(m.stringPlus("Type not found: ", getReflectType()));
    }

    @Override // d0.e0.p.d.m0.e.a.k0.j
    public String getPresentableText() {
        return getReflectType().toString();
    }

    @Override // d0.e0.p.d.m0.c.k1.b.w
    public Type getReflectType() {
        return this.f3266b;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.j
    public List<x> getTypeArguments() {
        List<Type> parameterizedTypeArguments = b.getParameterizedTypeArguments(getReflectType());
        w.a aVar = w.a;
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(parameterizedTypeArguments, 10));
        for (Type type : parameterizedTypeArguments) {
            arrayList.add(aVar.create(type));
        }
        return arrayList;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.d
    public boolean isDeprecatedInJavaDoc() {
        return false;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.j
    public boolean isRaw() {
        Type reflectType = getReflectType();
        if (!(reflectType instanceof Class)) {
            return false;
        }
        TypeVariable[] typeParameters = ((Class) reflectType).getTypeParameters();
        m.checkNotNullExpressionValue(typeParameters, "getTypeParameters()");
        return (typeParameters.length == 0) ^ true;
    }
}
