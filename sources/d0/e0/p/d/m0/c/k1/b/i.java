package d0.e0.p.d.m0.c.k1.b;

import d0.e0.p.d.m0.c.k1.b.w;
import d0.e0.p.d.m0.e.a.k0.a;
import d0.e0.p.d.m0.e.a.k0.f;
import d0.t.n;
import d0.z.d.m;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;
import java.util.Collection;
/* compiled from: ReflectJavaArrayType.kt */
/* loaded from: classes3.dex */
public final class i extends w implements f {

    /* renamed from: b  reason: collision with root package name */
    public final Type f3265b;
    public final w c;
    public final Collection<a> d;

    public i(Type type) {
        w wVar;
        m.checkNotNullParameter(type, "reflectType");
        this.f3265b = type;
        if (type instanceof GenericArrayType) {
            w.a aVar = w.a;
            Type genericComponentType = ((GenericArrayType) type).getGenericComponentType();
            m.checkNotNullExpressionValue(genericComponentType, "genericComponentType");
            wVar = aVar.create(genericComponentType);
        } else {
            if (type instanceof Class) {
                Class cls = (Class) type;
                if (cls.isArray()) {
                    w.a aVar2 = w.a;
                    Class<?> componentType = cls.getComponentType();
                    m.checkNotNullExpressionValue(componentType, "getComponentType()");
                    wVar = aVar2.create(componentType);
                }
            }
            StringBuilder R = b.d.b.a.a.R("Not an array type (");
            R.append(type.getClass());
            R.append("): ");
            R.append(type);
            throw new IllegalArgumentException(R.toString());
        }
        this.c = wVar;
        this.d = n.emptyList();
    }

    @Override // d0.e0.p.d.m0.e.a.k0.d
    public Collection<a> getAnnotations() {
        return this.d;
    }

    @Override // d0.e0.p.d.m0.c.k1.b.w
    public Type getReflectType() {
        return this.f3265b;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.d
    public boolean isDeprecatedInJavaDoc() {
        return false;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.f
    public w getComponentType() {
        return this.c;
    }
}
