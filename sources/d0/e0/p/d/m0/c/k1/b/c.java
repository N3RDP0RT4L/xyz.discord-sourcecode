package d0.e0.p.d.m0.c.k1.b;

import d0.e0.p.d.m0.c.k1.b.d;
import d0.e0.p.d.m0.e.a.k0.a;
import d0.e0.p.d.m0.e.a.k0.b;
import d0.e0.p.d.m0.g.e;
import d0.z.d.m;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
/* compiled from: ReflectJavaAnnotation.kt */
/* loaded from: classes3.dex */
public final class c extends n implements a {
    public final Annotation a;

    public c(Annotation annotation) {
        m.checkNotNullParameter(annotation, "annotation");
        this.a = annotation;
    }

    public boolean equals(Object obj) {
        return (obj instanceof c) && m.areEqual(this.a, ((c) obj).a);
    }

    public final Annotation getAnnotation() {
        return this.a;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.a
    public Collection<b> getArguments() {
        Method[] declaredMethods = d0.z.a.getJavaClass(d0.z.a.getAnnotationClass(this.a)).getDeclaredMethods();
        m.checkNotNullExpressionValue(declaredMethods, "annotation.annotationClass.java.declaredMethods");
        ArrayList arrayList = new ArrayList(declaredMethods.length);
        for (Method method : declaredMethods) {
            d.a aVar = d.a;
            Object invoke = method.invoke(getAnnotation(), new Object[0]);
            m.checkNotNullExpressionValue(invoke, "method.invoke(annotation)");
            arrayList.add(aVar.create(invoke, e.identifier(method.getName())));
        }
        return arrayList;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.a
    public d0.e0.p.d.m0.g.a getClassId() {
        return b.getClassId(d0.z.a.getJavaClass(d0.z.a.getAnnotationClass(this.a)));
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    @Override // d0.e0.p.d.m0.e.a.k0.a
    public boolean isFreshlySupportedTypeUseAnnotation() {
        return a.C0306a.isFreshlySupportedTypeUseAnnotation(this);
    }

    @Override // d0.e0.p.d.m0.e.a.k0.a
    public boolean isIdeExternalAnnotation() {
        return a.C0306a.isIdeExternalAnnotation(this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        b.d.b.a.a.i0(c.class, sb, ": ");
        sb.append(this.a);
        return sb.toString();
    }

    @Override // d0.e0.p.d.m0.e.a.k0.a
    public j resolve() {
        return new j(d0.z.a.getJavaClass(d0.z.a.getAnnotationClass(this.a)));
    }
}
