package d0.e0.p.d.m0.c.k1.a;

import andhook.lib.xposed.ClassUtils;
import d0.e0.p.d.m0.c.k1.b.j;
import d0.e0.p.d.m0.e.a.k0.g;
import d0.e0.p.d.m0.e.a.k0.u;
import d0.e0.p.d.m0.e.a.s;
import d0.e0.p.d.m0.g.a;
import d0.e0.p.d.m0.g.b;
import d0.g0.t;
import d0.z.d.m;
import java.util.Set;
/* compiled from: ReflectJavaClassFinder.kt */
/* loaded from: classes3.dex */
public final class d implements s {
    public final ClassLoader a;

    public d(ClassLoader classLoader) {
        m.checkNotNullParameter(classLoader, "classLoader");
        this.a = classLoader;
    }

    @Override // d0.e0.p.d.m0.e.a.s
    public g findClass(s.a aVar) {
        m.checkNotNullParameter(aVar, "request");
        a classId = aVar.getClassId();
        b packageFqName = classId.getPackageFqName();
        m.checkNotNullExpressionValue(packageFqName, "classId.packageFqName");
        String asString = classId.getRelativeClassName().asString();
        m.checkNotNullExpressionValue(asString, "classId.relativeClassName.asString()");
        String replace$default = t.replace$default(asString, (char) ClassUtils.PACKAGE_SEPARATOR_CHAR, (char) ClassUtils.INNER_CLASS_SEPARATOR_CHAR, false, 4, (Object) null);
        if (!packageFqName.isRoot()) {
            replace$default = packageFqName.asString() + ClassUtils.PACKAGE_SEPARATOR_CHAR + replace$default;
        }
        Class<?> tryLoadClass = e.tryLoadClass(this.a, replace$default);
        if (tryLoadClass != null) {
            return new j(tryLoadClass);
        }
        return null;
    }

    @Override // d0.e0.p.d.m0.e.a.s
    public u findPackage(b bVar) {
        m.checkNotNullParameter(bVar, "fqName");
        return new d0.e0.p.d.m0.c.k1.b.u(bVar);
    }

    @Override // d0.e0.p.d.m0.e.a.s
    public Set<String> knownClassNamesInPackage(b bVar) {
        m.checkNotNullParameter(bVar, "packageFqName");
        return null;
    }
}
