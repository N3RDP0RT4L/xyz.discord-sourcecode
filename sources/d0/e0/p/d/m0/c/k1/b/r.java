package d0.e0.p.d.m0.c.k1.b;

import b.d.b.a.a;
import d0.e0.p.d.m0.c.f1;
import d0.e0.p.d.m0.c.k1.b.f;
import d0.e0.p.d.m0.c.k1.b.t;
import d0.e0.p.d.m0.e.a.k0.a0;
import d0.e0.p.d.m0.e.a.k0.q;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.g.g;
import d0.t.k;
import d0.t.u;
import d0.z.d.m;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Member;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
/* compiled from: ReflectJavaMember.kt */
/* loaded from: classes3.dex */
public abstract class r extends n implements f, t, q {
    public final List<a0> a(Type[] typeArr, Annotation[][] annotationArr, boolean z2) {
        String str;
        m.checkNotNullParameter(typeArr, "parameterTypes");
        m.checkNotNullParameter(annotationArr, "parameterAnnotations");
        ArrayList arrayList = new ArrayList(typeArr.length);
        List<String> loadParameterNames = a.a.loadParameterNames(getMember());
        Integer valueOf = loadParameterNames == null ? null : Integer.valueOf(loadParameterNames.size());
        int intValue = valueOf == null ? 0 : valueOf.intValue() - typeArr.length;
        int length = typeArr.length - 1;
        if (length >= 0) {
            int i = 0;
            while (true) {
                int i2 = i + 1;
                w create = w.a.create(typeArr[i]);
                if (loadParameterNames == null) {
                    str = null;
                } else {
                    str = (String) u.getOrNull(loadParameterNames, i + intValue);
                    if (str == null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("No parameter with index ");
                        sb.append(i);
                        sb.append('+');
                        sb.append(intValue);
                        sb.append(" (name=");
                        sb.append(getName());
                        sb.append(" type=");
                        sb.append(create);
                        sb.append(") in ");
                        throw new IllegalStateException(a.K(sb, loadParameterNames, "@ReflectJavaMember").toString());
                    }
                }
                arrayList.add(new y(create, annotationArr[i], str, z2 && i == k.getLastIndex(typeArr)));
                if (i2 > length) {
                    break;
                }
                i = i2;
            }
        }
        return arrayList;
    }

    public boolean equals(Object obj) {
        return (obj instanceof r) && m.areEqual(getMember(), ((r) obj).getMember());
    }

    @Override // d0.e0.p.d.m0.e.a.k0.d
    public c findAnnotation(b bVar) {
        return f.a.findAnnotation(this, bVar);
    }

    @Override // d0.e0.p.d.m0.c.k1.b.f
    public AnnotatedElement getElement() {
        return (AnnotatedElement) getMember();
    }

    public abstract Member getMember();

    @Override // d0.e0.p.d.m0.c.k1.b.t
    public int getModifiers() {
        return getMember().getModifiers();
    }

    @Override // d0.e0.p.d.m0.e.a.k0.t
    public e getName() {
        String name = getMember().getName();
        e identifier = name == null ? null : e.identifier(name);
        if (identifier != null) {
            return identifier;
        }
        e eVar = g.a;
        m.checkNotNullExpressionValue(eVar, "NO_NAME_PROVIDED");
        return eVar;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.s
    public f1 getVisibility() {
        return t.a.getVisibility(this);
    }

    public int hashCode() {
        return getMember().hashCode();
    }

    @Override // d0.e0.p.d.m0.e.a.k0.s
    public boolean isAbstract() {
        return t.a.isAbstract(this);
    }

    @Override // d0.e0.p.d.m0.e.a.k0.d
    public boolean isDeprecatedInJavaDoc() {
        return f.a.isDeprecatedInJavaDoc(this);
    }

    @Override // d0.e0.p.d.m0.e.a.k0.s
    public boolean isFinal() {
        return t.a.isFinal(this);
    }

    @Override // d0.e0.p.d.m0.e.a.k0.s
    public boolean isStatic() {
        return t.a.isStatic(this);
    }

    public String toString() {
        return getClass().getName() + ": " + getMember();
    }

    @Override // d0.e0.p.d.m0.e.a.k0.d
    public List<c> getAnnotations() {
        return f.a.getAnnotations(this);
    }

    @Override // d0.e0.p.d.m0.e.a.k0.q
    public j getContainingClass() {
        Class<?> declaringClass = getMember().getDeclaringClass();
        m.checkNotNullExpressionValue(declaringClass, "member.declaringClass");
        return new j(declaringClass);
    }
}
