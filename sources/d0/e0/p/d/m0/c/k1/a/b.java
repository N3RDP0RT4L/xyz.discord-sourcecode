package d0.e0.p.d.m0.c.k1.a;

import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.c.v0;
import d0.z.d.m;
import java.lang.annotation.Annotation;
/* compiled from: ReflectAnnotationSource.kt */
/* loaded from: classes3.dex */
public final class b implements u0 {

    /* renamed from: b  reason: collision with root package name */
    public final Annotation f3255b;

    public b(Annotation annotation) {
        m.checkNotNullParameter(annotation, "annotation");
        this.f3255b = annotation;
    }

    public final Annotation getAnnotation() {
        return this.f3255b;
    }

    @Override // d0.e0.p.d.m0.c.u0
    public v0 getContainingFile() {
        v0 v0Var = v0.a;
        m.checkNotNullExpressionValue(v0Var, "NO_SOURCE_FILE");
        return v0Var;
    }
}
