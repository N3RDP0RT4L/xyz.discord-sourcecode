package d0.e0.p.d.m0.c.k1.b;

import d0.e0.p.d.m0.e.a.k0.m;
import d0.e0.p.d.m0.g.a;
import d0.e0.p.d.m0.g.e;
/* compiled from: ReflectJavaAnnotationArguments.kt */
/* loaded from: classes3.dex */
public final class o extends d implements m {
    public final Enum<?> c;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public o(e eVar, Enum<?> r3) {
        super(eVar);
        d0.z.d.m.checkNotNullParameter(r3, "value");
        this.c = r3;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.m
    public e getEntryName() {
        return e.identifier(this.c.name());
    }

    @Override // d0.e0.p.d.m0.e.a.k0.m
    public a getEnumClassId() {
        Class<?> cls = this.c.getClass();
        if (!cls.isEnum()) {
            cls = cls.getEnclosingClass();
        }
        d0.z.d.m.checkNotNullExpressionValue(cls, "enumClass");
        return b.getClassId(cls);
    }
}
