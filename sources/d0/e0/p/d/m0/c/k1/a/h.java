package d0.e0.p.d.m0.c.k1.a;

import andhook.lib.xposed.ClassUtils;
import d0.e0.p.d.m0.g.a;
import d0.g0.t;
import d0.z.d.m;
/* compiled from: ReflectKotlinClassFinder.kt */
/* loaded from: classes3.dex */
public final class h {
    public static final String access$toRuntimeFqName(a aVar) {
        String asString = aVar.getRelativeClassName().asString();
        m.checkNotNullExpressionValue(asString, "relativeClassName.asString()");
        String replace$default = t.replace$default(asString, (char) ClassUtils.PACKAGE_SEPARATOR_CHAR, (char) ClassUtils.INNER_CLASS_SEPARATOR_CHAR, false, 4, (Object) null);
        if (aVar.getPackageFqName().isRoot()) {
            return replace$default;
        }
        return aVar.getPackageFqName() + ClassUtils.PACKAGE_SEPARATOR_CHAR + replace$default;
    }
}
