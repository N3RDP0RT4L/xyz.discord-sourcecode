package d0.e0.p.d.m0.c.k1.a;

import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.d0;
import d0.e0.p.d.m0.c.x0;
import d0.e0.p.d.m0.d.b.c;
import d0.e0.p.d.m0.e.a.g0.f;
import d0.e0.p.d.m0.e.a.g0.j;
import d0.e0.p.d.m0.e.a.i0.c;
import d0.e0.p.d.m0.e.a.i0.f;
import d0.e0.p.d.m0.e.a.l0.d;
import d0.e0.p.d.m0.e.a.t;
import d0.e0.p.d.m0.e.b.c;
import d0.e0.p.d.m0.e.b.e;
import d0.e0.p.d.m0.e.b.g;
import d0.e0.p.d.m0.e.b.n;
import d0.e0.p.d.m0.e.b.v;
import d0.e0.p.d.m0.k.z.b;
import d0.e0.p.d.m0.l.b.i;
import d0.e0.p.d.m0.l.b.k;
import d0.e0.p.d.m0.m.o;
import d0.z.d.m;
/* compiled from: RuntimeModuleData.kt */
/* loaded from: classes3.dex */
public final class l {
    public static final e makeDeserializationComponentsForJava(c0 c0Var, o oVar, d0 d0Var, f fVar, n nVar, d0.e0.p.d.m0.e.b.f fVar2) {
        m.checkNotNullParameter(c0Var, "module");
        m.checkNotNullParameter(oVar, "storageManager");
        m.checkNotNullParameter(d0Var, "notFoundClasses");
        m.checkNotNullParameter(fVar, "lazyJavaPackageFragmentProvider");
        m.checkNotNullParameter(nVar, "reflectKotlinClassFinder");
        m.checkNotNullParameter(fVar2, "deserializedDescriptorResolver");
        return new e(oVar, c0Var, k.a.a, new g(nVar, fVar2), new c(c0Var, d0Var, oVar, nVar), fVar, d0Var, j.f3258b, c.a.a, i.a.getDEFAULT(), d0.e0.p.d.m0.n.l1.l.f3501b.getDefault());
    }

    public static final f makeLazyJavaPackageFragmentFromClassLoaderProvider(ClassLoader classLoader, c0 c0Var, o oVar, d0 d0Var, n nVar, d0.e0.p.d.m0.e.b.f fVar, d0.e0.p.d.m0.e.a.i0.i iVar, v vVar) {
        m.checkNotNullParameter(classLoader, "classLoader");
        m.checkNotNullParameter(c0Var, "module");
        m.checkNotNullParameter(oVar, "storageManager");
        m.checkNotNullParameter(d0Var, "notFoundClasses");
        m.checkNotNullParameter(nVar, "reflectKotlinClassFinder");
        m.checkNotNullParameter(fVar, "deserializedDescriptorResolver");
        m.checkNotNullParameter(iVar, "singleModuleClassResolver");
        m.checkNotNullParameter(vVar, "packagePartProvider");
        d0.e0.p.d.m0.p.e eVar = d0.e0.p.d.m0.p.e.f3535b;
        d0.e0.p.d.m0.e.a.c cVar = new d0.e0.p.d.m0.e.a.c(oVar, eVar);
        d dVar = new d(classLoader);
        j jVar = j.a;
        m.checkNotNullExpressionValue(jVar, "DO_NOTHING");
        j jVar2 = j.f3258b;
        d0.e0.p.d.m0.e.a.g0.g gVar = d0.e0.p.d.m0.e.a.g0.g.a;
        m.checkNotNullExpressionValue(gVar, "EMPTY");
        f.a aVar = f.a.a;
        b bVar = new b(oVar, d0.t.n.emptyList());
        m mVar = m.a;
        x0.a aVar2 = x0.a.a;
        c.a aVar3 = c.a.a;
        d0.e0.p.d.m0.b.j jVar3 = new d0.e0.p.d.m0.b.j(c0Var, d0Var);
        c.a aVar4 = c.a.a;
        return new d0.e0.p.d.m0.e.a.i0.f(new d0.e0.p.d.m0.e.a.i0.b(oVar, dVar, nVar, fVar, jVar, jVar2, gVar, aVar, bVar, mVar, iVar, vVar, aVar2, aVar3, c0Var, jVar3, cVar, new d0.e0.p.d.m0.e.a.l0.l(cVar, eVar, new d(aVar4)), t.a.a, aVar4, d0.e0.p.d.m0.n.l1.l.f3501b.getDefault(), eVar));
    }
}
