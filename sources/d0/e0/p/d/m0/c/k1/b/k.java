package d0.e0.p.d.m0.c.k1.b;

import d0.e0.p.d.m0.e.a.k0.h;
import d0.e0.p.d.m0.e.a.k0.x;
import d0.e0.p.d.m0.g.e;
import d0.z.d.m;
/* compiled from: ReflectJavaAnnotationArguments.kt */
/* loaded from: classes3.dex */
public final class k extends d implements h {
    public final Class<?> c;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public k(e eVar, Class<?> cls) {
        super(eVar);
        m.checkNotNullParameter(cls, "klass");
        this.c = cls;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.h
    public x getReferencedType() {
        return w.a.create(this.c);
    }
}
