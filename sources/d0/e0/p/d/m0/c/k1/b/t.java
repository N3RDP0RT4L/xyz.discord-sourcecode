package d0.e0.p.d.m0.c.k1.b;

import d0.e0.p.d.m0.c.e1;
import d0.e0.p.d.m0.c.f1;
import d0.e0.p.d.m0.c.j1.b;
import d0.e0.p.d.m0.c.j1.c;
import d0.e0.p.d.m0.e.a.k0.s;
import d0.z.d.m;
import java.lang.reflect.Modifier;
/* compiled from: ReflectJavaModifierListOwner.kt */
/* loaded from: classes3.dex */
public interface t extends s {

    /* compiled from: ReflectJavaModifierListOwner.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public static f1 getVisibility(t tVar) {
            m.checkNotNullParameter(tVar, "this");
            int modifiers = tVar.getModifiers();
            if (Modifier.isPublic(modifiers)) {
                return e1.h.c;
            }
            if (Modifier.isPrivate(modifiers)) {
                return e1.e.c;
            }
            if (Modifier.isProtected(modifiers)) {
                return Modifier.isStatic(modifiers) ? c.c : b.c;
            }
            return d0.e0.p.d.m0.c.j1.a.c;
        }

        public static boolean isAbstract(t tVar) {
            m.checkNotNullParameter(tVar, "this");
            return Modifier.isAbstract(tVar.getModifiers());
        }

        public static boolean isFinal(t tVar) {
            m.checkNotNullParameter(tVar, "this");
            return Modifier.isFinal(tVar.getModifiers());
        }

        public static boolean isStatic(t tVar) {
            m.checkNotNullParameter(tVar, "this");
            return Modifier.isStatic(tVar.getModifiers());
        }
    }

    int getModifiers();
}
