package d0.e0.p.d.m0.c.k1.a;

import d0.e0.p.d.m0.c.k1.b.b;
import d0.z.d.m;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
/* compiled from: ReflectKotlinClass.kt */
/* loaded from: classes3.dex */
public final class n {
    public static final n a = new n();

    public final String constructorDesc(Constructor<?> constructor) {
        m.checkNotNullParameter(constructor, "constructor");
        StringBuilder sb = new StringBuilder();
        sb.append("(");
        Class<?>[] parameterTypes = constructor.getParameterTypes();
        m.checkNotNullExpressionValue(parameterTypes, "constructor.parameterTypes");
        int length = parameterTypes.length;
        int i = 0;
        while (i < length) {
            Class<?> cls = parameterTypes[i];
            i++;
            m.checkNotNullExpressionValue(cls, "parameterType");
            sb.append(b.getDesc(cls));
        }
        sb.append(")V");
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "sb.toString()");
        return sb2;
    }

    public final String fieldDesc(Field field) {
        m.checkNotNullParameter(field, "field");
        Class<?> type = field.getType();
        m.checkNotNullExpressionValue(type, "field.type");
        return b.getDesc(type);
    }

    public final String methodDesc(Method method) {
        m.checkNotNullParameter(method, "method");
        StringBuilder sb = new StringBuilder();
        sb.append("(");
        Class<?>[] parameterTypes = method.getParameterTypes();
        m.checkNotNullExpressionValue(parameterTypes, "method.parameterTypes");
        int length = parameterTypes.length;
        int i = 0;
        while (i < length) {
            Class<?> cls = parameterTypes[i];
            i++;
            m.checkNotNullExpressionValue(cls, "parameterType");
            sb.append(b.getDesc(cls));
        }
        sb.append(")");
        Class<?> returnType = method.getReturnType();
        m.checkNotNullExpressionValue(returnType, "method.returnType");
        sb.append(b.getDesc(returnType));
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "sb.toString()");
        return sb2;
    }
}
