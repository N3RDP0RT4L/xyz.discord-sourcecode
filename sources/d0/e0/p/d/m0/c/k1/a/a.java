package d0.e0.p.d.m0.c.k1.a;

import d0.e0.p.d.m0.c.i1.n;
import d0.e0.p.d.m0.e.b.b0.a;
import d0.e0.p.d.m0.e.b.f;
import d0.e0.p.d.m0.e.b.o;
import d0.e0.p.d.m0.e.b.p;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.k.a0.b;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.k.y.c;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
/* compiled from: PackagePartScopeCache.kt */
/* loaded from: classes3.dex */
public final class a {
    public final f a;

    /* renamed from: b  reason: collision with root package name */
    public final g f3254b;
    public final ConcurrentHashMap<d0.e0.p.d.m0.g.a, i> c = new ConcurrentHashMap<>();

    public a(f fVar, g gVar) {
        m.checkNotNullParameter(fVar, "resolver");
        m.checkNotNullParameter(gVar, "kotlinClassFinder");
        this.a = fVar;
        this.f3254b = gVar;
    }

    public final i getPackagePartScope(f fVar) {
        Collection<p> collection;
        m.checkNotNullParameter(fVar, "fileClass");
        ConcurrentHashMap<d0.e0.p.d.m0.g.a, i> concurrentHashMap = this.c;
        d0.e0.p.d.m0.g.a classId = fVar.getClassId();
        i iVar = concurrentHashMap.get(classId);
        if (iVar == null) {
            b packageFqName = fVar.getClassId().getPackageFqName();
            m.checkNotNullExpressionValue(packageFqName, "fileClass.classId.packageFqName");
            if (fVar.getClassHeader().getKind() == a.EnumC0312a.MULTIFILE_CLASS) {
                List<String> multifilePartNames = fVar.getClassHeader().getMultifilePartNames();
                collection = new ArrayList();
                for (String str : multifilePartNames) {
                    d0.e0.p.d.m0.g.a aVar = d0.e0.p.d.m0.g.a.topLevel(c.byInternalName(str).getFqNameForTopLevelClassMaybeWithDollars());
                    m.checkNotNullExpressionValue(aVar, "topLevel(JvmClassName.byInternalName(partName).fqNameForTopLevelClassMaybeWithDollars)");
                    p findKotlinClass = o.findKotlinClass(this.f3254b, aVar);
                    if (findKotlinClass != null) {
                        collection.add(findKotlinClass);
                    }
                }
            } else {
                collection = d0.t.m.listOf(fVar);
            }
            n nVar = new n(this.a.getComponents().getModuleDescriptor(), packageFqName);
            ArrayList arrayList = new ArrayList();
            for (p pVar : collection) {
                i createKotlinPackagePartScope = this.a.createKotlinPackagePartScope(nVar, pVar);
                if (createKotlinPackagePartScope != null) {
                    arrayList.add(createKotlinPackagePartScope);
                }
            }
            List list = u.toList(arrayList);
            b.a aVar2 = d0.e0.p.d.m0.k.a0.b.f3423b;
            i create = aVar2.create("package " + packageFqName + " (" + fVar + ')', list);
            i putIfAbsent = concurrentHashMap.putIfAbsent(classId, create);
            iVar = putIfAbsent != null ? putIfAbsent : create;
        }
        m.checkNotNullExpressionValue(iVar, "cache.getOrPut(fileClass.classId) {\n        val fqName = fileClass.classId.packageFqName\n\n        val parts =\n            if (fileClass.classHeader.kind == KotlinClassHeader.Kind.MULTIFILE_CLASS)\n                fileClass.classHeader.multifilePartNames.mapNotNull { partName ->\n                    val classId = ClassId.topLevel(JvmClassName.byInternalName(partName).fqNameForTopLevelClassMaybeWithDollars)\n                    kotlinClassFinder.findKotlinClass(classId)\n                }\n            else listOf(fileClass)\n\n        val packageFragment = EmptyPackageFragmentDescriptor(resolver.components.moduleDescriptor, fqName)\n\n        val scopes = parts.mapNotNull { part ->\n            resolver.createKotlinPackagePartScope(packageFragment, part)\n        }.toList()\n\n        ChainedMemberScope.create(\"package $fqName ($fileClass)\", scopes)\n    }");
        return iVar;
    }
}
