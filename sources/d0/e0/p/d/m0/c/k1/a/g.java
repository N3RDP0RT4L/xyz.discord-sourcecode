package d0.e0.p.d.m0.c.k1.a;

import d0.e0.p.d.m0.b.k;
import d0.e0.p.d.m0.e.b.n;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.l.b.d0.a;
import d0.e0.p.d.m0.l.b.d0.d;
import d0.z.d.m;
import java.io.InputStream;
/* compiled from: ReflectKotlinClassFinder.kt */
/* loaded from: classes3.dex */
public final class g implements n {
    public final ClassLoader a;

    /* renamed from: b  reason: collision with root package name */
    public final d f3257b = new d();

    public g(ClassLoader classLoader) {
        m.checkNotNullParameter(classLoader, "classLoader");
        this.a = classLoader;
    }

    public final n.a a(String str) {
        f create;
        Class<?> tryLoadClass = e.tryLoadClass(this.a, str);
        if (tryLoadClass == null || (create = f.a.create(tryLoadClass)) == null) {
            return null;
        }
        return new n.a.b(create, null, 2, null);
    }

    @Override // d0.e0.p.d.m0.l.b.s
    public InputStream findBuiltInsData(b bVar) {
        m.checkNotNullParameter(bVar, "packageFqName");
        if (!bVar.startsWith(k.k)) {
            return null;
        }
        return this.f3257b.loadResource(a.m.getBuiltInsFilePath(bVar));
    }

    @Override // d0.e0.p.d.m0.e.b.n
    public n.a findKotlinClassOrContent(d0.e0.p.d.m0.g.a aVar) {
        m.checkNotNullParameter(aVar, "classId");
        return a(h.access$toRuntimeFqName(aVar));
    }

    @Override // d0.e0.p.d.m0.e.b.n
    public n.a findKotlinClassOrContent(d0.e0.p.d.m0.e.a.k0.g gVar) {
        m.checkNotNullParameter(gVar, "javaClass");
        b fqName = gVar.getFqName();
        String asString = fqName == null ? null : fqName.asString();
        if (asString == null) {
            return null;
        }
        return a(asString);
    }
}
