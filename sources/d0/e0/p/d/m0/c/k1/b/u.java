package d0.e0.p.d.m0.c.k1.b;

import d0.e0.p.d.m0.e.a.k0.a;
import d0.e0.p.d.m0.e.a.k0.g;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.e;
import d0.t.n;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
import kotlin.jvm.functions.Function1;
/* compiled from: ReflectJavaPackage.kt */
/* loaded from: classes3.dex */
public final class u extends n implements d0.e0.p.d.m0.e.a.k0.u {
    public final b a;

    public u(b bVar) {
        m.checkNotNullParameter(bVar, "fqName");
        this.a = bVar;
    }

    public boolean equals(Object obj) {
        return (obj instanceof u) && m.areEqual(getFqName(), ((u) obj).getFqName());
    }

    @Override // d0.e0.p.d.m0.e.a.k0.d
    public a findAnnotation(b bVar) {
        m.checkNotNullParameter(bVar, "fqName");
        return null;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.u
    public Collection<g> getClasses(Function1<? super e, Boolean> function1) {
        m.checkNotNullParameter(function1, "nameFilter");
        return n.emptyList();
    }

    @Override // d0.e0.p.d.m0.e.a.k0.u
    public b getFqName() {
        return this.a;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.u
    public Collection<d0.e0.p.d.m0.e.a.k0.u> getSubPackages() {
        return n.emptyList();
    }

    public int hashCode() {
        return getFqName().hashCode();
    }

    @Override // d0.e0.p.d.m0.e.a.k0.d
    public boolean isDeprecatedInJavaDoc() {
        return false;
    }

    public String toString() {
        return u.class.getName() + ": " + getFqName();
    }

    @Override // d0.e0.p.d.m0.e.a.k0.d
    public List<a> getAnnotations() {
        return n.emptyList();
    }
}
