package d0.e0.p.d.m0.c.k1.b;

import d0.e0.p.d.m0.b.i;
import d0.e0.p.d.m0.e.a.k0.a;
import d0.e0.p.d.m0.k.y.d;
import d0.t.n;
import d0.z.d.m;
import java.lang.reflect.Type;
import java.util.Collection;
/* compiled from: ReflectJavaPrimitiveType.kt */
/* loaded from: classes3.dex */
public final class v extends w implements d0.e0.p.d.m0.e.a.k0.v {

    /* renamed from: b  reason: collision with root package name */
    public final Class<?> f3267b;
    public final Collection<a> c = n.emptyList();

    public v(Class<?> cls) {
        m.checkNotNullParameter(cls, "reflectType");
        this.f3267b = cls;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.d
    public Collection<a> getAnnotations() {
        return this.c;
    }

    @Override // d0.e0.p.d.m0.c.k1.b.w
    public Type getReflectType() {
        return this.f3267b;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.v
    public i getType() {
        if (m.areEqual(this.f3267b, Void.TYPE)) {
            return null;
        }
        return d.get(this.f3267b.getName()).getPrimitiveType();
    }

    @Override // d0.e0.p.d.m0.e.a.k0.d
    public boolean isDeprecatedInJavaDoc() {
        return false;
    }
}
