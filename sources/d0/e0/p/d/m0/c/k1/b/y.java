package d0.e0.p.d.m0.c.k1.b;

import d0.e0.p.d.m0.e.a.k0.a0;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.e;
import d0.z.d.m;
import java.lang.annotation.Annotation;
import java.util.List;
/* compiled from: ReflectJavaValueParameter.kt */
/* loaded from: classes3.dex */
public final class y extends n implements a0 {
    public final w a;

    /* renamed from: b  reason: collision with root package name */
    public final Annotation[] f3268b;
    public final String c;
    public final boolean d;

    public y(w wVar, Annotation[] annotationArr, String str, boolean z2) {
        m.checkNotNullParameter(wVar, "type");
        m.checkNotNullParameter(annotationArr, "reflectAnnotations");
        this.a = wVar;
        this.f3268b = annotationArr;
        this.c = str;
        this.d = z2;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.a0
    public e getName() {
        String str = this.c;
        if (str == null) {
            return null;
        }
        return e.guessByFirstCharacter(str);
    }

    @Override // d0.e0.p.d.m0.e.a.k0.d
    public boolean isDeprecatedInJavaDoc() {
        return false;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.a0
    public boolean isVararg() {
        return this.d;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(y.class.getName());
        sb.append(": ");
        sb.append(isVararg() ? "vararg " : "");
        sb.append(getName());
        sb.append(": ");
        sb.append(getType());
        return sb.toString();
    }

    @Override // d0.e0.p.d.m0.e.a.k0.d
    public c findAnnotation(b bVar) {
        m.checkNotNullParameter(bVar, "fqName");
        return g.findAnnotation(this.f3268b, bVar);
    }

    @Override // d0.e0.p.d.m0.e.a.k0.d
    public List<c> getAnnotations() {
        return g.getAnnotations(this.f3268b);
    }

    @Override // d0.e0.p.d.m0.e.a.k0.a0
    public w getType() {
        return this.a;
    }
}
