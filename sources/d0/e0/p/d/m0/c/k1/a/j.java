package d0.e0.p.d.m0.c.k1.a;

import b.d.b.a.a;
import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.l.b.p;
import d0.z.d.m;
import java.util.List;
/* compiled from: RuntimeErrorReporter.kt */
/* loaded from: classes3.dex */
public final class j implements p {

    /* renamed from: b  reason: collision with root package name */
    public static final j f3258b = new j();

    @Override // d0.e0.p.d.m0.l.b.p
    public void reportCannotInferVisibility(b bVar) {
        m.checkNotNullParameter(bVar, "descriptor");
        throw new IllegalStateException(m.stringPlus("Cannot infer visibility for ", bVar));
    }

    @Override // d0.e0.p.d.m0.l.b.p
    public void reportIncompleteHierarchy(e eVar, List<String> list) {
        m.checkNotNullParameter(eVar, "descriptor");
        m.checkNotNullParameter(list, "unresolvedSuperClasses");
        StringBuilder R = a.R("Incomplete hierarchy for class ");
        R.append(eVar.getName());
        R.append(", unresolved classes ");
        R.append(list);
        throw new IllegalStateException(R.toString());
    }
}
