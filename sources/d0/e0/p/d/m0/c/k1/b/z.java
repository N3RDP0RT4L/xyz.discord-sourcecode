package d0.e0.p.d.m0.c.k1.b;

import d0.e0.p.d.m0.c.k1.b.w;
import d0.e0.p.d.m0.e.a.k0.a;
import d0.e0.p.d.m0.e.a.k0.b0;
import d0.t.k;
import d0.t.n;
import d0.z.d.m;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import java.util.Collection;
/* compiled from: ReflectJavaWildcardType.kt */
/* loaded from: classes3.dex */
public final class z extends w implements b0 {

    /* renamed from: b  reason: collision with root package name */
    public final WildcardType f3269b;
    public final Collection<a> c = n.emptyList();

    public z(WildcardType wildcardType) {
        m.checkNotNullParameter(wildcardType, "reflectType");
        this.f3269b = wildcardType;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.d
    public Collection<a> getAnnotations() {
        return this.c;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.b0
    public w getBound() {
        Type[] upperBounds = this.f3269b.getUpperBounds();
        Type[] lowerBounds = this.f3269b.getLowerBounds();
        if (upperBounds.length > 1 || lowerBounds.length > 1) {
            throw new UnsupportedOperationException(m.stringPlus("Wildcard types with many bounds are not yet supported: ", this.f3269b));
        } else if (lowerBounds.length == 1) {
            w.a aVar = w.a;
            m.checkNotNullExpressionValue(lowerBounds, "lowerBounds");
            Object single = k.single(lowerBounds);
            m.checkNotNullExpressionValue(single, "lowerBounds.single()");
            return aVar.create((Type) single);
        } else if (upperBounds.length != 1) {
            return null;
        } else {
            m.checkNotNullExpressionValue(upperBounds, "upperBounds");
            Type type = (Type) k.single(upperBounds);
            if (m.areEqual(type, Object.class)) {
                return null;
            }
            w.a aVar2 = w.a;
            m.checkNotNullExpressionValue(type, "ub");
            return aVar2.create(type);
        }
    }

    @Override // d0.e0.p.d.m0.c.k1.b.w
    public Type getReflectType() {
        return this.f3269b;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.d
    public boolean isDeprecatedInJavaDoc() {
        return false;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.b0
    public boolean isExtends() {
        Type[] upperBounds = this.f3269b.getUpperBounds();
        m.checkNotNullExpressionValue(upperBounds, "reflectType.upperBounds");
        return !m.areEqual(k.firstOrNull(upperBounds), Object.class);
    }
}
