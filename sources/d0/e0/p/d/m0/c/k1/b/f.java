package d0.e0.p.d.m0.c.k1.b;

import d0.e0.p.d.m0.e.a.k0.d;
import d0.e0.p.d.m0.g.b;
import d0.t.n;
import d0.z.d.m;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.util.List;
/* compiled from: ReflectJavaAnnotationOwner.kt */
/* loaded from: classes3.dex */
public interface f extends d {

    /* compiled from: ReflectJavaAnnotationOwner.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public static c findAnnotation(f fVar, b bVar) {
            Annotation[] declaredAnnotations;
            m.checkNotNullParameter(fVar, "this");
            m.checkNotNullParameter(bVar, "fqName");
            AnnotatedElement element = fVar.getElement();
            if (element == null || (declaredAnnotations = element.getDeclaredAnnotations()) == null) {
                return null;
            }
            return g.findAnnotation(declaredAnnotations, bVar);
        }

        public static List<c> getAnnotations(f fVar) {
            m.checkNotNullParameter(fVar, "this");
            AnnotatedElement element = fVar.getElement();
            Annotation[] declaredAnnotations = element == null ? null : element.getDeclaredAnnotations();
            return declaredAnnotations == null ? n.emptyList() : g.getAnnotations(declaredAnnotations);
        }

        public static boolean isDeprecatedInJavaDoc(f fVar) {
            m.checkNotNullParameter(fVar, "this");
            return false;
        }
    }

    AnnotatedElement getElement();
}
