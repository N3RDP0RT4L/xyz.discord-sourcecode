package d0.e0.p.d.m0.c.k1.b;

import d0.e0.p.d.m0.e.a.k0.x;
import d0.e0.p.d.m0.g.b;
import d0.z.d.m;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ReflectJavaType.kt */
/* loaded from: classes3.dex */
public abstract class w implements x {
    public static final a a = new a(null);

    /* compiled from: ReflectJavaType.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final w create(Type type) {
            w wVar;
            m.checkNotNullParameter(type, "type");
            boolean z2 = type instanceof Class;
            if (z2) {
                Class cls = (Class) type;
                if (cls.isPrimitive()) {
                    return new v(cls);
                }
            }
            if ((type instanceof GenericArrayType) || (z2 && ((Class) type).isArray())) {
                wVar = new i(type);
            } else {
                wVar = type instanceof WildcardType ? new z((WildcardType) type) : new l(type);
            }
            return wVar;
        }
    }

    public boolean equals(Object obj) {
        return (obj instanceof w) && m.areEqual(getReflectType(), ((w) obj).getReflectType());
    }

    @Override // d0.e0.p.d.m0.e.a.k0.d
    public d0.e0.p.d.m0.e.a.k0.a findAnnotation(b bVar) {
        return x.a.findAnnotation(this, bVar);
    }

    public abstract Type getReflectType();

    public int hashCode() {
        return getReflectType().hashCode();
    }

    public String toString() {
        return getClass().getName() + ": " + getReflectType();
    }
}
