package d0.e0.p.d.m0.c.k1.b;

import d0.e0.p.d.m0.e.a.k0.a;
import d0.e0.p.d.m0.e.a.k0.c;
import d0.z.d.m;
import java.lang.annotation.Annotation;
/* compiled from: ReflectJavaAnnotationArguments.kt */
/* loaded from: classes3.dex */
public final class e extends d implements c {
    public final Annotation c;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public e(d0.e0.p.d.m0.g.e eVar, Annotation annotation) {
        super(eVar);
        m.checkNotNullParameter(annotation, "annotation");
        this.c = annotation;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.c
    public a getAnnotation() {
        return new c(this.c);
    }
}
