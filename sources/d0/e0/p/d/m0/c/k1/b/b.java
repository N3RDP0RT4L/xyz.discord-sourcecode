package d0.e0.p.d.m0.c.k1.b;

import andhook.lib.xposed.ClassUtils;
import androidx.exifinterface.media.ExifInterface;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.e0.c;
import d0.f0.q;
import d0.g0.t;
import d0.t.h0;
import d0.t.k;
import d0.t.n;
import d0.z.c.d;
import d0.z.c.e;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import java.lang.reflect.Array;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function10;
import kotlin.jvm.functions.Function11;
import kotlin.jvm.functions.Function12;
import kotlin.jvm.functions.Function13;
import kotlin.jvm.functions.Function14;
import kotlin.jvm.functions.Function15;
import kotlin.jvm.functions.Function16;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function22;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.functions.Function4;
import kotlin.jvm.functions.Function5;
import kotlin.jvm.functions.Function6;
import kotlin.jvm.functions.Function7;
import kotlin.jvm.functions.Function8;
import kotlin.jvm.functions.Function9;
import kotlin.sequences.Sequence;
/* compiled from: reflectClassUtil.kt */
/* loaded from: classes3.dex */
public final class b {
    public static final List<c<? extends Object>> a;

    /* renamed from: b  reason: collision with root package name */
    public static final Map<Class<? extends Object>, Class<? extends Object>> f3263b;
    public static final Map<Class<? extends Object>, Class<? extends Object>> c;
    public static final Map<Class<? extends d0.c<?>>, Integer> d;

    /* compiled from: reflectClassUtil.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<ParameterizedType, ParameterizedType> {
        public static final a j = new a();

        public a() {
            super(1);
        }

        public final ParameterizedType invoke(ParameterizedType parameterizedType) {
            m.checkNotNullParameter(parameterizedType, "it");
            Type ownerType = parameterizedType.getOwnerType();
            if (ownerType instanceof ParameterizedType) {
                return (ParameterizedType) ownerType;
            }
            return null;
        }
    }

    /* compiled from: reflectClassUtil.kt */
    /* renamed from: d0.e0.p.d.m0.c.k1.b.b$b  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0297b extends o implements Function1<ParameterizedType, Sequence<? extends Type>> {
        public static final C0297b j = new C0297b();

        public C0297b() {
            super(1);
        }

        public final Sequence<Type> invoke(ParameterizedType parameterizedType) {
            m.checkNotNullParameter(parameterizedType, "it");
            Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
            m.checkNotNullExpressionValue(actualTypeArguments, "it.actualTypeArguments");
            return k.asSequence(actualTypeArguments);
        }
    }

    static {
        int i = 0;
        List<c<? extends Object>> listOf = n.listOf((Object[]) new c[]{a0.getOrCreateKotlinClass(Boolean.TYPE), a0.getOrCreateKotlinClass(Byte.TYPE), a0.getOrCreateKotlinClass(Character.TYPE), a0.getOrCreateKotlinClass(Double.TYPE), a0.getOrCreateKotlinClass(Float.TYPE), a0.getOrCreateKotlinClass(Integer.TYPE), a0.getOrCreateKotlinClass(Long.TYPE), a0.getOrCreateKotlinClass(Short.TYPE)});
        a = listOf;
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(listOf, 10));
        Iterator<T> it = listOf.iterator();
        while (it.hasNext()) {
            c cVar = (c) it.next();
            arrayList.add(d0.o.to(d0.z.a.getJavaObjectType(cVar), d0.z.a.getJavaPrimitiveType(cVar)));
        }
        f3263b = h0.toMap(arrayList);
        List<c<? extends Object>> list = a;
        ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(list, 10));
        Iterator<T> it2 = list.iterator();
        while (it2.hasNext()) {
            c cVar2 = (c) it2.next();
            arrayList2.add(d0.o.to(d0.z.a.getJavaPrimitiveType(cVar2), d0.z.a.getJavaObjectType(cVar2)));
        }
        c = h0.toMap(arrayList2);
        List listOf2 = n.listOf((Object[]) new Class[]{Function0.class, Function1.class, Function2.class, Function3.class, Function4.class, Function5.class, Function6.class, Function7.class, Function8.class, Function9.class, Function10.class, Function11.class, Function12.class, Function13.class, Function14.class, Function15.class, Function16.class, d0.z.c.a.class, d0.z.c.b.class, d0.z.c.c.class, d.class, e.class, Function22.class});
        ArrayList arrayList3 = new ArrayList(d0.t.o.collectionSizeOrDefault(listOf2, 10));
        for (Object obj : listOf2) {
            i++;
            if (i < 0) {
                n.throwIndexOverflow();
            }
            arrayList3.add(d0.o.to((Class) obj, Integer.valueOf(i)));
        }
        d = h0.toMap(arrayList3);
    }

    public static final Class<?> createArrayType(Class<?> cls) {
        m.checkNotNullParameter(cls, "<this>");
        return Array.newInstance(cls, 0).getClass();
    }

    public static final d0.e0.p.d.m0.g.a getClassId(Class<?> cls) {
        m.checkNotNullParameter(cls, "<this>");
        if (cls.isPrimitive()) {
            throw new IllegalArgumentException(m.stringPlus("Can't compute ClassId for primitive type: ", cls));
        } else if (!cls.isArray()) {
            if (cls.getEnclosingMethod() == null && cls.getEnclosingConstructor() == null) {
                String simpleName = cls.getSimpleName();
                m.checkNotNullExpressionValue(simpleName, "simpleName");
                if (!(simpleName.length() == 0)) {
                    Class<?> declaringClass = cls.getDeclaringClass();
                    d0.e0.p.d.m0.g.a createNestedClassId = declaringClass == null ? null : getClassId(declaringClass).createNestedClassId(d0.e0.p.d.m0.g.e.identifier(cls.getSimpleName()));
                    if (createNestedClassId == null) {
                        createNestedClassId = d0.e0.p.d.m0.g.a.topLevel(new d0.e0.p.d.m0.g.b(cls.getName()));
                    }
                    m.checkNotNullExpressionValue(createNestedClassId, "declaringClass?.classId?.createNestedClassId(Name.identifier(simpleName)) ?: ClassId.topLevel(FqName(name))");
                    return createNestedClassId;
                }
            }
            d0.e0.p.d.m0.g.b bVar = new d0.e0.p.d.m0.g.b(cls.getName());
            return new d0.e0.p.d.m0.g.a(bVar.parent(), d0.e0.p.d.m0.g.b.topLevel(bVar.shortName()), true);
        } else {
            throw new IllegalArgumentException(m.stringPlus("Can't compute ClassId for array type: ", cls));
        }
    }

    public static final String getDesc(Class<?> cls) {
        m.checkNotNullParameter(cls, "<this>");
        if (m.areEqual(cls, Void.TYPE)) {
            return ExifInterface.GPS_MEASUREMENT_INTERRUPTED;
        }
        String name = createArrayType(cls).getName();
        m.checkNotNullExpressionValue(name, "createArrayType().name");
        String substring = name.substring(1);
        m.checkNotNullExpressionValue(substring, "(this as java.lang.String).substring(startIndex)");
        return t.replace$default(substring, (char) ClassUtils.PACKAGE_SEPARATOR_CHAR, (char) MentionUtilsKt.SLASH_CHAR, false, 4, (Object) null);
    }

    public static final List<Type> getParameterizedTypeArguments(Type type) {
        m.checkNotNullParameter(type, "<this>");
        if (!(type instanceof ParameterizedType)) {
            return n.emptyList();
        }
        ParameterizedType parameterizedType = (ParameterizedType) type;
        if (parameterizedType.getOwnerType() != null) {
            return q.toList(q.flatMap(d0.f0.n.generateSequence(type, a.j), C0297b.j));
        }
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        m.checkNotNullExpressionValue(actualTypeArguments, "actualTypeArguments");
        return k.toList(actualTypeArguments);
    }

    public static final Class<?> getPrimitiveByWrapper(Class<?> cls) {
        m.checkNotNullParameter(cls, "<this>");
        return f3263b.get(cls);
    }

    public static final ClassLoader getSafeClassLoader(Class<?> cls) {
        m.checkNotNullParameter(cls, "<this>");
        ClassLoader classLoader = cls.getClassLoader();
        if (classLoader != null) {
            return classLoader;
        }
        ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
        m.checkNotNullExpressionValue(systemClassLoader, "getSystemClassLoader()");
        return systemClassLoader;
    }

    public static final Class<?> getWrapperByPrimitive(Class<?> cls) {
        m.checkNotNullParameter(cls, "<this>");
        return c.get(cls);
    }

    public static final boolean isEnumClassOrSpecializedEnumEntryClass(Class<?> cls) {
        m.checkNotNullParameter(cls, "<this>");
        return Enum.class.isAssignableFrom(cls);
    }
}
