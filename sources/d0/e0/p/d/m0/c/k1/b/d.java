package d0.e0.p.d.m0.c.k1.b;

import d0.e0.p.d.m0.e.a.k0.b;
import d0.e0.p.d.m0.g.e;
import d0.z.d.m;
import java.lang.annotation.Annotation;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ReflectJavaAnnotationArguments.kt */
/* loaded from: classes3.dex */
public abstract class d implements b {
    public static final a a = new a(null);

    /* renamed from: b  reason: collision with root package name */
    public final e f3264b;

    /* compiled from: ReflectJavaAnnotationArguments.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final d create(Object obj, e eVar) {
            m.checkNotNullParameter(obj, "value");
            return b.isEnumClassOrSpecializedEnumEntryClass(obj.getClass()) ? new o(eVar, (Enum) obj) : obj instanceof Annotation ? new e(eVar, (Annotation) obj) : obj instanceof Object[] ? new h(eVar, (Object[]) obj) : obj instanceof Class ? new k(eVar, (Class) obj) : new q(eVar, obj);
        }
    }

    public d(e eVar) {
        this.f3264b = eVar;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.b
    public e getName() {
        return this.f3264b;
    }
}
