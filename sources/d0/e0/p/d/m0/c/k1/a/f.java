package d0.e0.p.d.m0.c.k1.a;

import andhook.lib.xposed.ClassUtils;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.e0.p.d.m0.e.b.b0.b;
import d0.e0.p.d.m0.e.b.p;
import d0.g0.t;
import d0.z.d.m;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ReflectKotlinClass.kt */
/* loaded from: classes3.dex */
public final class f implements p {
    public static final a a = new a(null);

    /* renamed from: b  reason: collision with root package name */
    public final Class<?> f3256b;
    public final d0.e0.p.d.m0.e.b.b0.a c;

    /* compiled from: ReflectKotlinClass.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final f create(Class<?> cls) {
            m.checkNotNullParameter(cls, "klass");
            b bVar = new b();
            c.a.loadClassAnnotations(cls, bVar);
            d0.e0.p.d.m0.e.b.b0.a createHeader = bVar.createHeader();
            if (createHeader == null) {
                return null;
            }
            return new f(cls, createHeader, null);
        }
    }

    public f(Class cls, d0.e0.p.d.m0.e.b.b0.a aVar, DefaultConstructorMarker defaultConstructorMarker) {
        this.f3256b = cls;
        this.c = aVar;
    }

    public boolean equals(Object obj) {
        return (obj instanceof f) && m.areEqual(this.f3256b, ((f) obj).f3256b);
    }

    @Override // d0.e0.p.d.m0.e.b.p
    public d0.e0.p.d.m0.e.b.b0.a getClassHeader() {
        return this.c;
    }

    @Override // d0.e0.p.d.m0.e.b.p
    public d0.e0.p.d.m0.g.a getClassId() {
        return d0.e0.p.d.m0.c.k1.b.b.getClassId(this.f3256b);
    }

    public final Class<?> getKlass() {
        return this.f3256b;
    }

    @Override // d0.e0.p.d.m0.e.b.p
    public String getLocation() {
        String name = this.f3256b.getName();
        m.checkNotNullExpressionValue(name, "klass.name");
        return m.stringPlus(t.replace$default(name, (char) ClassUtils.PACKAGE_SEPARATOR_CHAR, (char) MentionUtilsKt.SLASH_CHAR, false, 4, (Object) null), ".class");
    }

    public int hashCode() {
        return this.f3256b.hashCode();
    }

    @Override // d0.e0.p.d.m0.e.b.p
    public void loadClassAnnotations(p.c cVar, byte[] bArr) {
        m.checkNotNullParameter(cVar, "visitor");
        c.a.loadClassAnnotations(this.f3256b, cVar);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        b.d.b.a.a.i0(f.class, sb, ": ");
        sb.append(this.f3256b);
        return sb.toString();
    }

    @Override // d0.e0.p.d.m0.e.b.p
    public void visitMembers(p.d dVar, byte[] bArr) {
        m.checkNotNullParameter(dVar, "visitor");
        c.a.visitMembers(this.f3256b, dVar);
    }
}
