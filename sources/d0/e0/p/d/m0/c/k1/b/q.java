package d0.e0.p.d.m0.c.k1.b;

import d0.e0.p.d.m0.e.a.k0.o;
import d0.e0.p.d.m0.g.e;
import d0.z.d.m;
/* compiled from: ReflectJavaAnnotationArguments.kt */
/* loaded from: classes3.dex */
public final class q extends d implements o {
    public final Object c;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public q(e eVar, Object obj) {
        super(eVar);
        m.checkNotNullParameter(obj, "value");
        this.c = obj;
    }

    @Override // d0.e0.p.d.m0.e.a.k0.o
    public Object getValue() {
        return this.c;
    }
}
