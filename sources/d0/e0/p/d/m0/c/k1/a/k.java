package d0.e0.p.d.m0.c.k1.a;

import d0.e0.p.d.m0.b.q.f;
import d0.e0.p.d.m0.b.q.o;
import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.d0;
import d0.e0.p.d.m0.c.i0;
import d0.e0.p.d.m0.c.i1.y;
import d0.e0.p.d.m0.e.a.g0.g;
import d0.e0.p.d.m0.e.a.i0.f;
import d0.e0.p.d.m0.e.b.v;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.y.b;
import d0.e0.p.d.m0.l.b.j;
import d0.e0.p.d.m0.l.b.k;
import d0.e0.p.d.m0.n.l1.l;
import d0.t.n;
import d0.z.d.m;
import kotlin.Unit;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: RuntimeModuleData.kt */
/* loaded from: classes3.dex */
public final class k {
    public static final a a = new a(null);

    /* renamed from: b  reason: collision with root package name */
    public final j f3259b;
    public final d0.e0.p.d.m0.c.k1.a.a c;

    /* compiled from: RuntimeModuleData.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final k create(ClassLoader classLoader) {
            f makeLazyJavaPackageFragmentFromClassLoaderProvider;
            m.checkNotNullParameter(classLoader, "classLoader");
            d0.e0.p.d.m0.m.f fVar = new d0.e0.p.d.m0.m.f("RuntimeModuleData");
            d0.e0.p.d.m0.b.q.f fVar2 = new d0.e0.p.d.m0.b.q.f(fVar, f.a.FROM_DEPENDENCIES);
            e special = e.special("<runtime module for " + classLoader + '>');
            m.checkNotNullExpressionValue(special, "special(\"<runtime module for $classLoader>\")");
            y yVar = new y(special, fVar, fVar2, null, null, null, 56, null);
            fVar2.setBuiltInsModule(yVar);
            fVar2.initialize(yVar, true);
            g gVar = new g(classLoader);
            d0.e0.p.d.m0.e.b.f fVar3 = new d0.e0.p.d.m0.e.b.f();
            d0.e0.p.d.m0.e.a.i0.j jVar = new d0.e0.p.d.m0.e.a.i0.j();
            d0 d0Var = new d0(fVar, yVar);
            makeLazyJavaPackageFragmentFromClassLoaderProvider = l.makeLazyJavaPackageFragmentFromClassLoaderProvider(classLoader, yVar, fVar, d0Var, gVar, fVar3, jVar, (r17 & 128) != 0 ? v.a.a : null);
            d0.e0.p.d.m0.e.b.e makeDeserializationComponentsForJava = l.makeDeserializationComponentsForJava(yVar, fVar, d0Var, makeLazyJavaPackageFragmentFromClassLoaderProvider, gVar, fVar3);
            fVar3.setComponents(makeDeserializationComponentsForJava);
            g gVar2 = g.a;
            m.checkNotNullExpressionValue(gVar2, "EMPTY");
            b bVar = new b(makeLazyJavaPackageFragmentFromClassLoaderProvider, gVar2);
            jVar.setResolver(bVar);
            ClassLoader classLoader2 = Unit.class.getClassLoader();
            m.checkNotNullExpressionValue(classLoader2, "stdlibClassLoader");
            o oVar = new o(fVar, new g(classLoader2), yVar, d0Var, fVar2.getCustomizer(), fVar2.getCustomizer(), k.a.a, l.f3501b.getDefault(), new d0.e0.p.d.m0.k.z.b(fVar, n.emptyList()));
            yVar.setDependencies(yVar);
            yVar.initialize(new d0.e0.p.d.m0.c.i1.j(n.listOf((Object[]) new i0[]{bVar.getPackageFragmentProvider(), oVar})));
            return new k(makeDeserializationComponentsForJava.getComponents(), new d0.e0.p.d.m0.c.k1.a.a(fVar3, gVar), null);
        }
    }

    public k(j jVar, d0.e0.p.d.m0.c.k1.a.a aVar, DefaultConstructorMarker defaultConstructorMarker) {
        this.f3259b = jVar;
        this.c = aVar;
    }

    public final j getDeserialization() {
        return this.f3259b;
    }

    public final c0 getModule() {
        return this.f3259b.getModuleDescriptor();
    }

    public final d0.e0.p.d.m0.c.k1.a.a getPackagePartScopeCache() {
        return this.c;
    }
}
