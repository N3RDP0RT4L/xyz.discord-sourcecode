package d0.e0.p.d.m0.c.k1.a;

import andhook.lib.HookHelper;
import d0.e0.p.d.m0.b.i;
import d0.e0.p.d.m0.b.k;
import d0.e0.p.d.m0.c.k1.b.b;
import d0.e0.p.d.m0.e.b.p;
import d0.e0.p.d.m0.g.a;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.v.f;
import d0.e0.p.d.m0.k.y.d;
import d0.z.d.m;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.Set;
/* compiled from: ReflectKotlinClass.kt */
/* loaded from: classes3.dex */
public final class c {
    public static final c a = new c();

    public final f a(Class<?> cls) {
        int i = 0;
        while (cls.isArray()) {
            i++;
            cls = cls.getComponentType();
            m.checkNotNullExpressionValue(cls, "currentClass.componentType");
        }
        if (!cls.isPrimitive()) {
            a classId = b.getClassId(cls);
            d0.e0.p.d.m0.b.q.c cVar = d0.e0.p.d.m0.b.q.c.a;
            d0.e0.p.d.m0.g.b asSingleFqName = classId.asSingleFqName();
            m.checkNotNullExpressionValue(asSingleFqName, "javaClassId.asSingleFqName()");
            a mapJavaToKotlin = cVar.mapJavaToKotlin(asSingleFqName);
            if (mapJavaToKotlin != null) {
                classId = mapJavaToKotlin;
            }
            return new f(classId, i);
        } else if (m.areEqual(cls, Void.TYPE)) {
            a aVar = a.topLevel(k.a.e.toSafe());
            m.checkNotNullExpressionValue(aVar, "topLevel(StandardNames.FqNames.unit.toSafe())");
            return new f(aVar, i);
        } else {
            i primitiveType = d.get(cls.getName()).getPrimitiveType();
            m.checkNotNullExpressionValue(primitiveType, "get(currentClass.name).primitiveType");
            if (i > 0) {
                a aVar2 = a.topLevel(primitiveType.getArrayTypeFqName());
                m.checkNotNullExpressionValue(aVar2, "topLevel(primitiveType.arrayTypeFqName)");
                return new f(aVar2, i - 1);
            }
            a aVar3 = a.topLevel(primitiveType.getTypeFqName());
            m.checkNotNullExpressionValue(aVar3, "topLevel(primitiveType.typeFqName)");
            return new f(aVar3, i);
        }
    }

    public final void b(p.c cVar, Annotation annotation) {
        Class<?> javaClass = d0.z.a.getJavaClass(d0.z.a.getAnnotationClass(annotation));
        p.a visitAnnotation = cVar.visitAnnotation(b.getClassId(javaClass), new b(annotation));
        if (visitAnnotation != null) {
            c(visitAnnotation, annotation, javaClass);
        }
    }

    public final void c(p.a aVar, Annotation annotation, Class<?> cls) {
        Set set;
        Method[] declaredMethods = cls.getDeclaredMethods();
        m.checkNotNullExpressionValue(declaredMethods, "annotationType.declaredMethods");
        int length = declaredMethods.length;
        int i = 0;
        while (i < length) {
            Method method = declaredMethods[i];
            i++;
            try {
                Object invoke = method.invoke(annotation, new Object[0]);
                m.checkNotNull(invoke);
                e identifier = e.identifier(method.getName());
                m.checkNotNullExpressionValue(identifier, "identifier(method.name)");
                Class<?> cls2 = invoke.getClass();
                if (m.areEqual(cls2, Class.class)) {
                    aVar.visitClassLiteral(identifier, a((Class) invoke));
                } else {
                    set = i.a;
                    if (set.contains(cls2)) {
                        aVar.visit(identifier, invoke);
                    } else if (b.isEnumClassOrSpecializedEnumEntryClass(cls2)) {
                        if (!cls2.isEnum()) {
                            cls2 = cls2.getEnclosingClass();
                        }
                        m.checkNotNullExpressionValue(cls2, "if (clazz.isEnum) clazz else clazz.enclosingClass");
                        a classId = b.getClassId(cls2);
                        e identifier2 = e.identifier(((Enum) invoke).name());
                        m.checkNotNullExpressionValue(identifier2, "identifier((value as Enum<*>).name)");
                        aVar.visitEnum(identifier, classId, identifier2);
                    } else if (Annotation.class.isAssignableFrom(cls2)) {
                        Class<?>[] interfaces = cls2.getInterfaces();
                        m.checkNotNullExpressionValue(interfaces, "clazz.interfaces");
                        Class<?> cls3 = (Class) d0.t.k.single(interfaces);
                        m.checkNotNullExpressionValue(cls3, "annotationClass");
                        p.a visitAnnotation = aVar.visitAnnotation(identifier, b.getClassId(cls3));
                        if (visitAnnotation != null) {
                            c(visitAnnotation, (Annotation) invoke, cls3);
                        }
                    } else if (cls2.isArray()) {
                        p.b visitArray = aVar.visitArray(identifier);
                        if (visitArray != null) {
                            Class<?> componentType = cls2.getComponentType();
                            if (componentType.isEnum()) {
                                m.checkNotNullExpressionValue(componentType, "componentType");
                                a classId2 = b.getClassId(componentType);
                                Object[] objArr = (Object[]) invoke;
                                int length2 = objArr.length;
                                int i2 = 0;
                                while (i2 < length2) {
                                    Object obj = objArr[i2];
                                    i2++;
                                    Objects.requireNonNull(obj, "null cannot be cast to non-null type kotlin.Enum<*>");
                                    e identifier3 = e.identifier(((Enum) obj).name());
                                    m.checkNotNullExpressionValue(identifier3, "identifier((element as Enum<*>).name)");
                                    visitArray.visitEnum(classId2, identifier3);
                                }
                            } else if (m.areEqual(componentType, Class.class)) {
                                Object[] objArr2 = (Object[]) invoke;
                                int length3 = objArr2.length;
                                int i3 = 0;
                                while (i3 < length3) {
                                    Object obj2 = objArr2[i3];
                                    i3++;
                                    Objects.requireNonNull(obj2, "null cannot be cast to non-null type java.lang.Class<*>");
                                    visitArray.visitClassLiteral(a((Class) obj2));
                                }
                            } else {
                                Object[] objArr3 = (Object[]) invoke;
                                int length4 = objArr3.length;
                                int i4 = 0;
                                while (i4 < length4) {
                                    Object obj3 = objArr3[i4];
                                    i4++;
                                    visitArray.visit(obj3);
                                }
                            }
                            visitArray.visitEnd();
                        }
                    } else {
                        throw new UnsupportedOperationException("Unsupported annotation argument value (" + cls2 + "): " + invoke);
                    }
                }
            } catch (IllegalAccessException unused) {
            }
        }
        aVar.visitEnd();
    }

    public final void loadClassAnnotations(Class<?> cls, p.c cVar) {
        m.checkNotNullParameter(cls, "klass");
        m.checkNotNullParameter(cVar, "visitor");
        Annotation[] declaredAnnotations = cls.getDeclaredAnnotations();
        m.checkNotNullExpressionValue(declaredAnnotations, "klass.declaredAnnotations");
        int length = declaredAnnotations.length;
        int i = 0;
        while (i < length) {
            Annotation annotation = declaredAnnotations[i];
            i++;
            m.checkNotNullExpressionValue(annotation, "annotation");
            b(cVar, annotation);
        }
        cVar.visitEnd();
    }

    public final void visitMembers(Class<?> cls, p.d dVar) {
        String str;
        String str2;
        int i;
        int i2;
        Constructor<?>[] constructorArr;
        int i3;
        Method[] methodArr;
        m.checkNotNullParameter(cls, "klass");
        m.checkNotNullParameter(dVar, "memberVisitor");
        Method[] declaredMethods = cls.getDeclaredMethods();
        m.checkNotNullExpressionValue(declaredMethods, "klass.declaredMethods");
        int length = declaredMethods.length;
        int i4 = 0;
        while (true) {
            str = "annotations";
            if (i4 >= length) {
                break;
            }
            Method method = declaredMethods[i4];
            i4++;
            e identifier = e.identifier(method.getName());
            m.checkNotNullExpressionValue(identifier, "identifier(method.name)");
            n nVar = n.a;
            m.checkNotNullExpressionValue(method, "method");
            p.e visitMethod = dVar.visitMethod(identifier, nVar.methodDesc(method));
            if (visitMethod == null) {
                methodArr = declaredMethods;
                i3 = length;
            } else {
                Annotation[] declaredAnnotations = method.getDeclaredAnnotations();
                m.checkNotNullExpressionValue(declaredAnnotations, "method.declaredAnnotations");
                int length2 = declaredAnnotations.length;
                int i5 = 0;
                while (i5 < length2) {
                    Annotation annotation = declaredAnnotations[i5];
                    i5++;
                    m.checkNotNullExpressionValue(annotation, "annotation");
                    b(visitMethod, annotation);
                }
                Annotation[][] parameterAnnotations = method.getParameterAnnotations();
                m.checkNotNullExpressionValue(parameterAnnotations, "method.parameterAnnotations");
                int length3 = parameterAnnotations.length;
                int i6 = 0;
                while (i6 < length3) {
                    Annotation[] annotationArr = parameterAnnotations[i6];
                    i6++;
                    m.checkNotNullExpressionValue(annotationArr, str);
                    int length4 = annotationArr.length;
                    int i7 = 0;
                    while (i7 < length4) {
                        declaredMethods = declaredMethods;
                        Annotation annotation2 = annotationArr[i7];
                        i7++;
                        Class<?> javaClass = d0.z.a.getJavaClass(d0.z.a.getAnnotationClass(annotation2));
                        length = length;
                        a classId = b.getClassId(javaClass);
                        m.checkNotNullExpressionValue(annotation2, "annotation");
                        p.a visitParameterAnnotation = visitMethod.visitParameterAnnotation(i6, classId, new b(annotation2));
                        if (visitParameterAnnotation != null) {
                            c(visitParameterAnnotation, annotation2, javaClass);
                        }
                    }
                }
                methodArr = declaredMethods;
                i3 = length;
                visitMethod.visitEnd();
            }
            declaredMethods = methodArr;
            length = i3;
        }
        Constructor<?>[] declaredConstructors = cls.getDeclaredConstructors();
        m.checkNotNullExpressionValue(declaredConstructors, "klass.declaredConstructors");
        int length5 = declaredConstructors.length;
        int i8 = 0;
        while (i8 < length5) {
            Constructor<?> constructor = declaredConstructors[i8];
            int i9 = i8 + 1;
            e special = e.special(HookHelper.constructorName);
            m.checkNotNullExpressionValue(special, "special(\"<init>\")");
            n nVar2 = n.a;
            m.checkNotNullExpressionValue(constructor, "constructor");
            p.e visitMethod2 = dVar.visitMethod(special, nVar2.constructorDesc(constructor));
            if (visitMethod2 == null) {
                constructorArr = declaredConstructors;
                i = length5;
                i2 = i9;
                str2 = str;
            } else {
                Annotation[] declaredAnnotations2 = constructor.getDeclaredAnnotations();
                m.checkNotNullExpressionValue(declaredAnnotations2, "constructor.declaredAnnotations");
                int length6 = declaredAnnotations2.length;
                int i10 = 0;
                while (i10 < length6) {
                    Annotation annotation3 = declaredAnnotations2[i10];
                    i10++;
                    m.checkNotNullExpressionValue(annotation3, "annotation");
                    b(visitMethod2, annotation3);
                }
                Annotation[][] parameterAnnotations2 = constructor.getParameterAnnotations();
                m.checkNotNullExpressionValue(parameterAnnotations2, "parameterAnnotations");
                if (!(parameterAnnotations2.length == 0)) {
                    int length7 = constructor.getParameterTypes().length - parameterAnnotations2.length;
                    int length8 = parameterAnnotations2.length;
                    int i11 = 0;
                    while (i11 < length8) {
                        Annotation[] annotationArr2 = parameterAnnotations2[i11];
                        i11++;
                        m.checkNotNullExpressionValue(annotationArr2, str);
                        int length9 = annotationArr2.length;
                        int i12 = 0;
                        while (i12 < length9) {
                            declaredConstructors = declaredConstructors;
                            Annotation annotation4 = annotationArr2[i12];
                            i12++;
                            length5 = length5;
                            Class<?> javaClass2 = d0.z.a.getJavaClass(d0.z.a.getAnnotationClass(annotation4));
                            i9 = i9;
                            int i13 = i11 + length7;
                            length7 = length7;
                            a classId2 = b.getClassId(javaClass2);
                            str = str;
                            m.checkNotNullExpressionValue(annotation4, "annotation");
                            p.a visitParameterAnnotation2 = visitMethod2.visitParameterAnnotation(i13, classId2, new b(annotation4));
                            if (visitParameterAnnotation2 != null) {
                                c(visitParameterAnnotation2, annotation4, javaClass2);
                            }
                        }
                    }
                }
                constructorArr = declaredConstructors;
                i = length5;
                i2 = i9;
                str2 = str;
                visitMethod2.visitEnd();
            }
            declaredConstructors = constructorArr;
            i8 = i2;
            length5 = i;
            str = str2;
        }
        Field[] declaredFields = cls.getDeclaredFields();
        m.checkNotNullExpressionValue(declaredFields, "klass.declaredFields");
        int length10 = declaredFields.length;
        int i14 = 0;
        while (i14 < length10) {
            Field field = declaredFields[i14];
            i14++;
            e identifier2 = e.identifier(field.getName());
            m.checkNotNullExpressionValue(identifier2, "identifier(field.name)");
            n nVar3 = n.a;
            m.checkNotNullExpressionValue(field, "field");
            p.c visitField = dVar.visitField(identifier2, nVar3.fieldDesc(field), null);
            if (visitField != null) {
                Annotation[] declaredAnnotations3 = field.getDeclaredAnnotations();
                m.checkNotNullExpressionValue(declaredAnnotations3, "field.declaredAnnotations");
                int length11 = declaredAnnotations3.length;
                int i15 = 0;
                while (i15 < length11) {
                    Annotation annotation5 = declaredAnnotations3[i15];
                    i15++;
                    m.checkNotNullExpressionValue(annotation5, "annotation");
                    b(visitField, annotation5);
                }
                visitField.visitEnd();
            }
        }
    }
}
