package d0.e0.p.d.m0.c;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
/* compiled from: Visibility.kt */
/* loaded from: classes3.dex */
public abstract class f1 {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f3219b;

    public f1(String str, boolean z2) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        this.a = str;
        this.f3219b = z2;
    }

    public Integer compareTo(f1 f1Var) {
        m.checkNotNullParameter(f1Var, "visibility");
        return e1.a.compareLocal$compiler_common(this, f1Var);
    }

    public String getInternalDisplayName() {
        return this.a;
    }

    public final boolean isPublicAPI() {
        return this.f3219b;
    }

    public f1 normalize() {
        return this;
    }

    public final String toString() {
        return getInternalDisplayName();
    }
}
