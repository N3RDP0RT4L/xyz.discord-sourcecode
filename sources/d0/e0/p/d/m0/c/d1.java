package d0.e0.p.d.m0.c;

import d0.e0.p.d.m0.k.v.g;
/* compiled from: VariableDescriptor.java */
/* loaded from: classes3.dex */
public interface d1 extends b1 {
    g<?> getCompileTimeInitializer();

    boolean isConst();

    boolean isLateInit();

    boolean isVar();
}
