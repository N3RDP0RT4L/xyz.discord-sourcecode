package d0.e0.p.d.m0.c;

import d0.e0.p.d.m0.c.n;
import d0.e0.p.d.m0.n.c1;
/* compiled from: Substitutable.kt */
/* loaded from: classes3.dex */
public interface w0<T extends n> {
    T substitute(c1 c1Var);
}
