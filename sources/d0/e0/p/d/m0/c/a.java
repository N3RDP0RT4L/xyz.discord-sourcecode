package d0.e0.p.d.m0.c;

import d0.e0.p.d.m0.n.c0;
import java.util.Collection;
import java.util.List;
/* compiled from: CallableDescriptor.java */
/* loaded from: classes3.dex */
public interface a extends n, q, w0<a> {

    /* compiled from: CallableDescriptor.java */
    /* renamed from: d0.e0.p.d.m0.c.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public interface AbstractC0290a<V> {
    }

    q0 getDispatchReceiverParameter();

    q0 getExtensionReceiverParameter();

    @Override // d0.e0.p.d.m0.c.m
    a getOriginal();

    Collection<? extends a> getOverriddenDescriptors();

    c0 getReturnType();

    List<z0> getTypeParameters();

    <V> V getUserData(AbstractC0290a<V> aVar);

    List<c1> getValueParameters();

    boolean hasSynthesizedParameterNames();
}
