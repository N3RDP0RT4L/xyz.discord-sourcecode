package d0.e0.p.d.m0.c.j1;

import d0.e0.p.d.m0.c.e1;
import d0.e0.p.d.m0.c.f1;
/* compiled from: JavaVisibilities.kt */
/* loaded from: classes3.dex */
public final class c extends f1 {
    public static final c c = new c();

    public c() {
        super("protected_static", true);
    }

    @Override // d0.e0.p.d.m0.c.f1
    public String getInternalDisplayName() {
        return "protected/*protected static*/";
    }

    @Override // d0.e0.p.d.m0.c.f1
    public f1 normalize() {
        return e1.g.c;
    }
}
