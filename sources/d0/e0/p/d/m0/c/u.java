package d0.e0.p.d.m0.c;

import d0.e0.p.d.m0.k.a0.p.d;
import d0.z.d.m;
/* compiled from: DescriptorVisibility.kt */
/* loaded from: classes3.dex */
public abstract class u {
    public final Integer compareTo(u uVar) {
        m.checkNotNullParameter(uVar, "visibility");
        return getDelegate().compareTo(uVar.getDelegate());
    }

    public abstract f1 getDelegate();

    public abstract String getInternalDisplayName();

    public final boolean isPublicAPI() {
        return getDelegate().isPublicAPI();
    }

    public abstract boolean isVisible(d dVar, q qVar, m mVar);

    public abstract u normalize();

    public final String toString() {
        return getDelegate().toString();
    }
}
