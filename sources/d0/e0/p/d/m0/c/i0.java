package d0.e0.p.d.m0.c;

import d0.e0.p.d.m0.g.b;
import java.util.Collection;
/* compiled from: PackageFragmentProvider.kt */
/* loaded from: classes3.dex */
public interface i0 extends f0 {
    void collectPackageFragments(b bVar, Collection<e0> collection);
}
