package d0.e0.p.d.m0.c;

import d0.e0.p.d.m0.n.c1;
/* compiled from: ClassConstructorDescriptor.kt */
/* loaded from: classes3.dex */
public interface d extends l {
    @Override // d0.e0.p.d.m0.c.x, d0.e0.p.d.m0.c.b, d0.e0.p.d.m0.c.a, d0.e0.p.d.m0.c.m
    d getOriginal();

    @Override // d0.e0.p.d.m0.c.l, d0.e0.p.d.m0.c.x, d0.e0.p.d.m0.c.w0
    d substitute(c1 c1Var);
}
