package d0.e0.p.d.m0.c;

import d0.e0.p.d.m0.c.g1.a;
import d0.e0.p.d.m0.g.e;
/* compiled from: DeclarationDescriptor.java */
/* loaded from: classes3.dex */
public interface m extends a {
    <R, D> R accept(o<R, D> oVar, D d);

    m getContainingDeclaration();

    e getName();

    m getOriginal();
}
