package d0.e0.p.d.m0.c;

import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.n.l1.g;
import d0.z.d.o;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: ScopesHolderForClass.kt */
/* loaded from: classes3.dex */
public final class s0 extends o implements Function0<T> {
    public final /* synthetic */ r0<T> this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public s0(r0<T> r0Var) {
        super(0);
        this.this$0 = r0Var;
    }

    /* JADX WARN: Incorrect return type in method signature: ()TT; */
    @Override // kotlin.jvm.functions.Function0
    public final i invoke() {
        g gVar;
        Function1 function1 = this.this$0.d;
        gVar = this.this$0.e;
        return (i) function1.invoke(gVar);
    }
}
