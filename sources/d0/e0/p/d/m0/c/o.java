package d0.e0.p.d.m0.c;
/* compiled from: DeclarationDescriptorVisitor.java */
/* loaded from: classes3.dex */
public interface o<R, D> {
    R visitClassDescriptor(e eVar, D d);

    R visitConstructorDescriptor(l lVar, D d);

    R visitFunctionDescriptor(x xVar, D d);

    R visitModuleDeclaration(c0 c0Var, D d);

    R visitPackageFragmentDescriptor(e0 e0Var, D d);

    R visitPackageViewDescriptor(j0 j0Var, D d);

    R visitPropertyDescriptor(n0 n0Var, D d);

    R visitPropertyGetterDescriptor(o0 o0Var, D d);

    R visitPropertySetterDescriptor(p0 p0Var, D d);

    R visitReceiverParameterDescriptor(q0 q0Var, D d);

    R visitTypeAliasDescriptor(y0 y0Var, D d);

    R visitTypeParameterDescriptor(z0 z0Var, D d);

    R visitValueParameterDescriptor(c1 c1Var, D d);
}
