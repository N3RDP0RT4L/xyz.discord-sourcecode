package d0.e0.p.d.m0.c;

import d0.e0.p.d.m0.c.e1;
import d0.e0.p.d.m0.c.i1.i0;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.o.g;
import d0.t.n0;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.ServiceLoader;
/* compiled from: DescriptorVisibilities.java */
/* loaded from: classes3.dex */
public class t {
    public static final u a;

    /* renamed from: b  reason: collision with root package name */
    public static final u f3272b;
    public static final u c;
    public static final u d;
    public static final u e;
    public static final u f;
    public static final u g;
    public static final u h;
    public static final u i;
    public static final Map<u, Integer> j;
    public static final u k;
    public static final d0.e0.p.d.m0.k.a0.p.d l = new a();
    public static final d0.e0.p.d.m0.k.a0.p.d m = new b();
    @Deprecated
    public static final d0.e0.p.d.m0.k.a0.p.d n = new c();
    public static final d0.e0.p.d.m0.o.g o;
    public static final Map<f1, u> p;

    /* compiled from: DescriptorVisibilities.java */
    /* loaded from: classes3.dex */
    public static class a implements d0.e0.p.d.m0.k.a0.p.d {
        @Override // d0.e0.p.d.m0.k.a0.p.d
        public c0 getType() {
            throw new IllegalStateException("This method should not be called");
        }
    }

    /* compiled from: DescriptorVisibilities.java */
    /* loaded from: classes3.dex */
    public static class b implements d0.e0.p.d.m0.k.a0.p.d {
        @Override // d0.e0.p.d.m0.k.a0.p.d
        public c0 getType() {
            throw new IllegalStateException("This method should not be called");
        }
    }

    /* compiled from: DescriptorVisibilities.java */
    /* loaded from: classes3.dex */
    public static class c implements d0.e0.p.d.m0.k.a0.p.d {
        @Override // d0.e0.p.d.m0.k.a0.p.d
        public c0 getType() {
            throw new IllegalStateException("This method should not be called");
        }
    }

    /* compiled from: DescriptorVisibilities.java */
    /* loaded from: classes3.dex */
    public static class d extends r {
        public d(f1 f1Var) {
            super(f1Var);
        }

        public static /* synthetic */ void a(int i) {
            Object[] objArr = new Object[3];
            if (i == 1) {
                objArr[0] = "what";
            } else if (i != 2) {
                objArr[0] = "descriptor";
            } else {
                objArr[0] = "from";
            }
            objArr[1] = "kotlin/reflect/jvm/internal/impl/descriptors/DescriptorVisibilities$1";
            if (i == 1 || i == 2) {
                objArr[2] = "isVisible";
            } else {
                objArr[2] = "hasContainingSourceFile";
            }
            throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", objArr));
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r5v0, types: [d0.e0.p.d.m0.c.q, d0.e0.p.d.m0.c.m] */
        /* JADX WARN: Type inference failed for: r5v2, types: [d0.e0.p.d.m0.c.m] */
        /* JADX WARN: Type inference failed for: r5v3, types: [d0.e0.p.d.m0.c.m] */
        /* JADX WARN: Type inference failed for: r5v4, types: [d0.e0.p.d.m0.c.m] */
        @Override // d0.e0.p.d.m0.c.u
        public boolean isVisible(d0.e0.p.d.m0.k.a0.p.d dVar, q qVar, m mVar) {
            if (qVar == 0) {
                a(1);
                throw null;
            } else if (mVar != null) {
                if (d0.e0.p.d.m0.k.e.isTopLevelDeclaration(qVar)) {
                    if (mVar != null) {
                        if (d0.e0.p.d.m0.k.e.getContainingSourceFile(mVar) != v0.a) {
                            return t.inSameFile(qVar, mVar);
                        }
                    } else {
                        a(0);
                        throw null;
                    }
                }
                if (qVar instanceof d0.e0.p.d.m0.c.l) {
                    d0.e0.p.d.m0.c.i containingDeclaration = ((d0.e0.p.d.m0.c.l) qVar).getContainingDeclaration();
                    if (d0.e0.p.d.m0.k.e.isSealedClass(containingDeclaration) && d0.e0.p.d.m0.k.e.isTopLevelDeclaration(containingDeclaration) && (mVar instanceof d0.e0.p.d.m0.c.l) && d0.e0.p.d.m0.k.e.isTopLevelDeclaration(mVar.getContainingDeclaration()) && t.inSameFile(qVar, mVar)) {
                        return true;
                    }
                }
                while (qVar != 0) {
                    qVar = qVar.getContainingDeclaration();
                    if (qVar instanceof d0.e0.p.d.m0.c.e) {
                        if (!d0.e0.p.d.m0.k.e.isCompanionObject(qVar)) {
                            break;
                        }
                    }
                    if (qVar instanceof e0) {
                        break;
                    }
                }
                if (qVar == 0) {
                    return false;
                }
                while (mVar != null) {
                    if (qVar == mVar) {
                        return true;
                    }
                    if (mVar instanceof e0) {
                        return (qVar instanceof e0) && qVar.getFqName().equals(((e0) mVar).getFqName()) && d0.e0.p.d.m0.k.e.areInSameModule(mVar, qVar);
                    }
                    mVar = mVar.getContainingDeclaration();
                }
                return false;
            } else {
                a(2);
                throw null;
            }
        }
    }

    /* compiled from: DescriptorVisibilities.java */
    /* loaded from: classes3.dex */
    public static class e extends r {
        public e(f1 f1Var) {
            super(f1Var);
        }

        public static /* synthetic */ void a(int i) {
            Object[] objArr = new Object[3];
            if (i != 1) {
                objArr[0] = "what";
            } else {
                objArr[0] = "from";
            }
            objArr[1] = "kotlin/reflect/jvm/internal/impl/descriptors/DescriptorVisibilities$2";
            objArr[2] = "isVisible";
            throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", objArr));
        }

        @Override // d0.e0.p.d.m0.c.u
        public boolean isVisible(d0.e0.p.d.m0.k.a0.p.d dVar, q qVar, m mVar) {
            m parentOfType;
            if (qVar == null) {
                a(0);
                throw null;
            } else if (mVar != null) {
                if (t.a.isVisible(dVar, qVar, mVar)) {
                    if (dVar == t.m) {
                        return true;
                    }
                    if (!(dVar == t.l || (parentOfType = d0.e0.p.d.m0.k.e.getParentOfType(qVar, d0.e0.p.d.m0.c.e.class)) == null || !(dVar instanceof d0.e0.p.d.m0.k.a0.p.f))) {
                        return ((d0.e0.p.d.m0.k.a0.p.f) dVar).getClassDescriptor().getOriginal().equals(parentOfType.getOriginal());
                    }
                }
                return false;
            } else {
                a(1);
                throw null;
            }
        }
    }

    /* compiled from: DescriptorVisibilities.java */
    /* loaded from: classes3.dex */
    public static class f extends r {
        public f(f1 f1Var) {
            super(f1Var);
        }

        public static /* synthetic */ void a(int i) {
            Object[] objArr = new Object[3];
            if (i == 1) {
                objArr[0] = "from";
            } else if (i == 2) {
                objArr[0] = "whatDeclaration";
            } else if (i != 3) {
                objArr[0] = "what";
            } else {
                objArr[0] = "fromClass";
            }
            objArr[1] = "kotlin/reflect/jvm/internal/impl/descriptors/DescriptorVisibilities$3";
            if (i == 2 || i == 3) {
                objArr[2] = "doesReceiverFitForProtectedVisibility";
            } else {
                objArr[2] = "isVisible";
            }
            throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", objArr));
        }

        /* JADX WARN: Code restructure failed: missing block: B:41:0x0079, code lost:
            if (d0.e0.p.d.m0.n.r.isDynamic(r0) == false) goto L43;
         */
        @Override // d0.e0.p.d.m0.c.u
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public boolean isVisible(d0.e0.p.d.m0.k.a0.p.d r7, d0.e0.p.d.m0.c.q r8, d0.e0.p.d.m0.c.m r9) {
            /*
                r6 = this;
                java.lang.Class<d0.e0.p.d.m0.c.e> r0 = d0.e0.p.d.m0.c.e.class
                r1 = 0
                r2 = 0
                if (r8 == 0) goto L91
                r3 = 1
                if (r9 == 0) goto L8d
                d0.e0.p.d.m0.c.m r4 = d0.e0.p.d.m0.k.e.getParentOfType(r8, r0)
                d0.e0.p.d.m0.c.e r4 = (d0.e0.p.d.m0.c.e) r4
                d0.e0.p.d.m0.c.m r9 = d0.e0.p.d.m0.k.e.getParentOfType(r9, r0, r2)
                d0.e0.p.d.m0.c.e r9 = (d0.e0.p.d.m0.c.e) r9
                if (r9 != 0) goto L18
                return r2
            L18:
                if (r4 == 0) goto L2f
                boolean r5 = d0.e0.p.d.m0.k.e.isCompanionObject(r4)
                if (r5 == 0) goto L2f
                d0.e0.p.d.m0.c.m r4 = d0.e0.p.d.m0.k.e.getParentOfType(r4, r0)
                d0.e0.p.d.m0.c.e r4 = (d0.e0.p.d.m0.c.e) r4
                if (r4 == 0) goto L2f
                boolean r4 = d0.e0.p.d.m0.k.e.isSubclass(r9, r4)
                if (r4 == 0) goto L2f
                return r3
            L2f:
                d0.e0.p.d.m0.c.q r4 = d0.e0.p.d.m0.k.e.unwrapFakeOverrideToAnyDeclaration(r8)
                d0.e0.p.d.m0.c.m r0 = d0.e0.p.d.m0.k.e.getParentOfType(r4, r0)
                d0.e0.p.d.m0.c.e r0 = (d0.e0.p.d.m0.c.e) r0
                if (r0 != 0) goto L3c
                return r2
            L3c:
                boolean r0 = d0.e0.p.d.m0.k.e.isSubclass(r9, r0)
                if (r0 == 0) goto L84
                if (r4 == 0) goto L7f
                d0.e0.p.d.m0.k.a0.p.d r0 = d0.e0.p.d.m0.c.t.n
                if (r7 != r0) goto L49
                goto L7c
            L49:
                boolean r0 = r4 instanceof d0.e0.p.d.m0.c.b
                if (r0 != 0) goto L4e
                goto L7b
            L4e:
                boolean r0 = r4 instanceof d0.e0.p.d.m0.c.l
                if (r0 == 0) goto L53
                goto L7b
            L53:
                d0.e0.p.d.m0.k.a0.p.d r0 = d0.e0.p.d.m0.c.t.m
                if (r7 != r0) goto L58
                goto L7b
            L58:
                d0.e0.p.d.m0.k.a0.p.d r0 = d0.e0.p.d.m0.c.t.l
                if (r7 == r0) goto L7c
                if (r7 != 0) goto L5f
                goto L7c
            L5f:
                boolean r0 = r7 instanceof d0.e0.p.d.m0.k.a0.p.e
                if (r0 == 0) goto L6b
                r0 = r7
                d0.e0.p.d.m0.k.a0.p.e r0 = (d0.e0.p.d.m0.k.a0.p.e) r0
                d0.e0.p.d.m0.n.c0 r0 = r0.getThisType()
                goto L6f
            L6b:
                d0.e0.p.d.m0.n.c0 r0 = r7.getType()
            L6f:
                boolean r1 = d0.e0.p.d.m0.k.e.isSubtypeOfClass(r0, r9)
                if (r1 != 0) goto L7b
                boolean r0 = d0.e0.p.d.m0.n.r.isDynamic(r0)
                if (r0 == 0) goto L7c
            L7b:
                r2 = 1
            L7c:
                if (r2 == 0) goto L84
                return r3
            L7f:
                r7 = 2
                a(r7)
                throw r1
            L84:
                d0.e0.p.d.m0.c.m r9 = r9.getContainingDeclaration()
                boolean r7 = r6.isVisible(r7, r8, r9)
                return r7
            L8d:
                a(r3)
                throw r1
            L91:
                a(r2)
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.c.t.f.isVisible(d0.e0.p.d.m0.k.a0.p.d, d0.e0.p.d.m0.c.q, d0.e0.p.d.m0.c.m):boolean");
        }
    }

    /* compiled from: DescriptorVisibilities.java */
    /* loaded from: classes3.dex */
    public static class g extends r {
        public g(f1 f1Var) {
            super(f1Var);
        }

        public static /* synthetic */ void a(int i) {
            Object[] objArr = new Object[3];
            if (i != 1) {
                objArr[0] = "what";
            } else {
                objArr[0] = "from";
            }
            objArr[1] = "kotlin/reflect/jvm/internal/impl/descriptors/DescriptorVisibilities$4";
            objArr[2] = "isVisible";
            throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", objArr));
        }

        @Override // d0.e0.p.d.m0.c.u
        public boolean isVisible(d0.e0.p.d.m0.k.a0.p.d dVar, q qVar, m mVar) {
            if (qVar == null) {
                a(0);
                throw null;
            } else if (mVar != null) {
                if (!d0.e0.p.d.m0.k.e.getContainingModule(mVar).shouldSeeInternalsOf(d0.e0.p.d.m0.k.e.getContainingModule(qVar))) {
                    return false;
                }
                return t.o.isInFriendModule(qVar, mVar);
            } else {
                a(1);
                throw null;
            }
        }
    }

    /* compiled from: DescriptorVisibilities.java */
    /* loaded from: classes3.dex */
    public static class h extends r {
        public h(f1 f1Var) {
            super(f1Var);
        }

        public static /* synthetic */ void a(int i) {
            Object[] objArr = new Object[3];
            if (i != 1) {
                objArr[0] = "what";
            } else {
                objArr[0] = "from";
            }
            objArr[1] = "kotlin/reflect/jvm/internal/impl/descriptors/DescriptorVisibilities$5";
            objArr[2] = "isVisible";
            throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", objArr));
        }

        @Override // d0.e0.p.d.m0.c.u
        public boolean isVisible(d0.e0.p.d.m0.k.a0.p.d dVar, q qVar, m mVar) {
            if (qVar == null) {
                a(0);
                throw null;
            } else if (mVar != null) {
                return true;
            } else {
                a(1);
                throw null;
            }
        }
    }

    /* compiled from: DescriptorVisibilities.java */
    /* loaded from: classes3.dex */
    public static class i extends r {
        public i(f1 f1Var) {
            super(f1Var);
        }

        public static /* synthetic */ void a(int i) {
            Object[] objArr = new Object[3];
            if (i != 1) {
                objArr[0] = "what";
            } else {
                objArr[0] = "from";
            }
            objArr[1] = "kotlin/reflect/jvm/internal/impl/descriptors/DescriptorVisibilities$6";
            objArr[2] = "isVisible";
            throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", objArr));
        }

        @Override // d0.e0.p.d.m0.c.u
        public boolean isVisible(d0.e0.p.d.m0.k.a0.p.d dVar, q qVar, m mVar) {
            if (qVar == null) {
                a(0);
                throw null;
            } else if (mVar == null) {
                a(1);
                throw null;
            } else {
                throw new IllegalStateException("This method shouldn't be invoked for LOCAL visibility");
            }
        }
    }

    /* compiled from: DescriptorVisibilities.java */
    /* loaded from: classes3.dex */
    public static class j extends r {
        public j(f1 f1Var) {
            super(f1Var);
        }

        public static /* synthetic */ void a(int i) {
            Object[] objArr = new Object[3];
            if (i != 1) {
                objArr[0] = "what";
            } else {
                objArr[0] = "from";
            }
            objArr[1] = "kotlin/reflect/jvm/internal/impl/descriptors/DescriptorVisibilities$7";
            objArr[2] = "isVisible";
            throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", objArr));
        }

        @Override // d0.e0.p.d.m0.c.u
        public boolean isVisible(d0.e0.p.d.m0.k.a0.p.d dVar, q qVar, m mVar) {
            if (qVar == null) {
                a(0);
                throw null;
            } else if (mVar == null) {
                a(1);
                throw null;
            } else {
                throw new IllegalStateException("Visibility is unknown yet");
            }
        }
    }

    /* compiled from: DescriptorVisibilities.java */
    /* loaded from: classes3.dex */
    public static class k extends r {
        public k(f1 f1Var) {
            super(f1Var);
        }

        public static /* synthetic */ void a(int i) {
            Object[] objArr = new Object[3];
            if (i != 1) {
                objArr[0] = "what";
            } else {
                objArr[0] = "from";
            }
            objArr[1] = "kotlin/reflect/jvm/internal/impl/descriptors/DescriptorVisibilities$8";
            objArr[2] = "isVisible";
            throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", objArr));
        }

        @Override // d0.e0.p.d.m0.c.u
        public boolean isVisible(d0.e0.p.d.m0.k.a0.p.d dVar, q qVar, m mVar) {
            if (qVar == null) {
                a(0);
                throw null;
            } else if (mVar != null) {
                return false;
            } else {
                a(1);
                throw null;
            }
        }
    }

    /* compiled from: DescriptorVisibilities.java */
    /* loaded from: classes3.dex */
    public static class l extends r {
        public l(f1 f1Var) {
            super(f1Var);
        }

        public static /* synthetic */ void a(int i) {
            Object[] objArr = new Object[3];
            if (i != 1) {
                objArr[0] = "what";
            } else {
                objArr[0] = "from";
            }
            objArr[1] = "kotlin/reflect/jvm/internal/impl/descriptors/DescriptorVisibilities$9";
            objArr[2] = "isVisible";
            throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", objArr));
        }

        @Override // d0.e0.p.d.m0.c.u
        public boolean isVisible(d0.e0.p.d.m0.k.a0.p.d dVar, q qVar, m mVar) {
            if (qVar == null) {
                a(0);
                throw null;
            } else if (mVar != null) {
                return false;
            } else {
                a(1);
                throw null;
            }
        }
    }

    static {
        d dVar = new d(e1.e.c);
        a = dVar;
        e eVar = new e(e1.f.c);
        f3272b = eVar;
        f fVar = new f(e1.g.c);
        c = fVar;
        g gVar = new g(e1.b.c);
        d = gVar;
        h hVar = new h(e1.h.c);
        e = hVar;
        i iVar = new i(e1.d.c);
        f = iVar;
        j jVar = new j(e1.a.c);
        g = jVar;
        k kVar = new k(e1.c.c);
        h = kVar;
        l lVar = new l(e1.i.c);
        i = lVar;
        Collections.unmodifiableSet(n0.setOf((Object[]) new u[]{dVar, eVar, gVar, iVar}));
        HashMap newHashMapWithExpectedSize = d0.e0.p.d.m0.p.a.newHashMapWithExpectedSize(4);
        newHashMapWithExpectedSize.put(eVar, 0);
        newHashMapWithExpectedSize.put(dVar, 0);
        newHashMapWithExpectedSize.put(gVar, 1);
        newHashMapWithExpectedSize.put(fVar, 1);
        newHashMapWithExpectedSize.put(hVar, 2);
        j = Collections.unmodifiableMap(newHashMapWithExpectedSize);
        k = hVar;
        Iterator it = ServiceLoader.load(d0.e0.p.d.m0.o.g.class, d0.e0.p.d.m0.o.g.class.getClassLoader()).iterator();
        o = it.hasNext() ? (d0.e0.p.d.m0.o.g) it.next() : g.a.a;
        p = new HashMap();
        b(dVar);
        b(eVar);
        b(fVar);
        b(gVar);
        b(hVar);
        b(iVar);
        b(jVar);
        b(kVar);
        b(lVar);
    }

    /* JADX WARN: Removed duplicated region for block: B:26:0x0042  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x0045  */
    /* JADX WARN: Removed duplicated region for block: B:29:0x004a  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x004f  */
    /* JADX WARN: Removed duplicated region for block: B:31:0x0052  */
    /* JADX WARN: Removed duplicated region for block: B:32:0x0057  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x005c  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x0061  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x0066  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x006b  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x0070  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x007a  */
    /* JADX WARN: Removed duplicated region for block: B:41:0x0080  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static /* synthetic */ void a(int r8) {
        /*
            r0 = 16
            if (r8 == r0) goto L7
            java.lang.String r1 = "Argument for @NotNull parameter '%s' of %s.%s must not be null"
            goto L9
        L7:
            java.lang.String r1 = "@NotNull method %s.%s must not return null"
        L9:
            r2 = 3
            r3 = 2
            if (r8 == r0) goto Lf
            r4 = 3
            goto L10
        Lf:
            r4 = 2
        L10:
            java.lang.Object[] r4 = new java.lang.Object[r4]
            java.lang.String r5 = "kotlin/reflect/jvm/internal/impl/descriptors/DescriptorVisibilities"
            r6 = 1
            r7 = 0
            if (r8 == r6) goto L3a
            if (r8 == r2) goto L3a
            r2 = 5
            if (r8 == r2) goto L3a
            r2 = 7
            if (r8 == r2) goto L3a
            switch(r8) {
                case 9: goto L3a;
                case 10: goto L35;
                case 11: goto L30;
                case 12: goto L35;
                case 13: goto L30;
                case 14: goto L2b;
                case 15: goto L2b;
                case 16: goto L28;
                default: goto L23;
            }
        L23:
            java.lang.String r2 = "what"
            r4[r7] = r2
            goto L3e
        L28:
            r4[r7] = r5
            goto L3e
        L2b:
            java.lang.String r2 = "visibility"
            r4[r7] = r2
            goto L3e
        L30:
            java.lang.String r2 = "second"
            r4[r7] = r2
            goto L3e
        L35:
            java.lang.String r2 = "first"
            r4[r7] = r2
            goto L3e
        L3a:
            java.lang.String r2 = "from"
            r4[r7] = r2
        L3e:
            java.lang.String r2 = "toDescriptorVisibility"
            if (r8 == r0) goto L45
            r4[r6] = r5
            goto L47
        L45:
            r4[r6] = r2
        L47:
            switch(r8) {
                case 2: goto L70;
                case 3: goto L70;
                case 4: goto L6b;
                case 5: goto L6b;
                case 6: goto L66;
                case 7: goto L66;
                case 8: goto L61;
                case 9: goto L61;
                case 10: goto L5c;
                case 11: goto L5c;
                case 12: goto L57;
                case 13: goto L57;
                case 14: goto L52;
                case 15: goto L4f;
                case 16: goto L74;
                default: goto L4a;
            }
        L4a:
            java.lang.String r2 = "isVisible"
            r4[r3] = r2
            goto L74
        L4f:
            r4[r3] = r2
            goto L74
        L52:
            java.lang.String r2 = "isPrivate"
            r4[r3] = r2
            goto L74
        L57:
            java.lang.String r2 = "compare"
            r4[r3] = r2
            goto L74
        L5c:
            java.lang.String r2 = "compareLocal"
            r4[r3] = r2
            goto L74
        L61:
            java.lang.String r2 = "findInvisibleMember"
            r4[r3] = r2
            goto L74
        L66:
            java.lang.String r2 = "inSameFile"
            r4[r3] = r2
            goto L74
        L6b:
            java.lang.String r2 = "isVisibleWithAnyReceiver"
            r4[r3] = r2
            goto L74
        L70:
            java.lang.String r2 = "isVisibleIgnoringReceiver"
            r4[r3] = r2
        L74:
            java.lang.String r1 = java.lang.String.format(r1, r4)
            if (r8 == r0) goto L80
            java.lang.IllegalArgumentException r8 = new java.lang.IllegalArgumentException
            r8.<init>(r1)
            goto L85
        L80:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            r8.<init>(r1)
        L85:
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.c.t.a(int):void");
    }

    public static void b(u uVar) {
        p.put(uVar.getDelegate(), uVar);
    }

    public static Integer compare(u uVar, u uVar2) {
        if (uVar == null) {
            a(12);
            throw null;
        } else if (uVar2 != null) {
            Integer compareTo = uVar.compareTo(uVar2);
            if (compareTo != null) {
                return compareTo;
            }
            Integer compareTo2 = uVar2.compareTo(uVar);
            if (compareTo2 != null) {
                return Integer.valueOf(-compareTo2.intValue());
            }
            return null;
        } else {
            a(13);
            throw null;
        }
    }

    public static q findInvisibleMember(d0.e0.p.d.m0.k.a0.p.d dVar, q qVar, m mVar) {
        q findInvisibleMember;
        if (qVar == null) {
            a(8);
            throw null;
        } else if (mVar != null) {
            for (q qVar2 = (q) qVar.getOriginal(); qVar2 != null && qVar2.getVisibility() != f; qVar2 = (q) d0.e0.p.d.m0.k.e.getParentOfType(qVar2, q.class)) {
                if (!qVar2.getVisibility().isVisible(dVar, qVar2, mVar)) {
                    return qVar2;
                }
            }
            if (!(qVar instanceof i0) || (findInvisibleMember = findInvisibleMember(dVar, ((i0) qVar).getUnderlyingConstructorDescriptor(), mVar)) == null) {
                return null;
            }
            return findInvisibleMember;
        } else {
            a(9);
            throw null;
        }
    }

    public static boolean inSameFile(m mVar, m mVar2) {
        if (mVar == null) {
            a(6);
            throw null;
        } else if (mVar2 != null) {
            v0 containingSourceFile = d0.e0.p.d.m0.k.e.getContainingSourceFile(mVar2);
            if (containingSourceFile != v0.a) {
                return containingSourceFile.equals(d0.e0.p.d.m0.k.e.getContainingSourceFile(mVar));
            }
            return false;
        } else {
            a(7);
            throw null;
        }
    }

    public static boolean isPrivate(u uVar) {
        if (uVar != null) {
            return uVar == a || uVar == f3272b;
        }
        a(14);
        throw null;
    }

    public static boolean isVisibleIgnoringReceiver(q qVar, m mVar) {
        if (qVar == null) {
            a(2);
            throw null;
        } else if (mVar != null) {
            return findInvisibleMember(m, qVar, mVar) == null;
        } else {
            a(3);
            throw null;
        }
    }

    public static u toDescriptorVisibility(f1 f1Var) {
        if (f1Var != null) {
            u uVar = p.get(f1Var);
            if (uVar != null) {
                return uVar;
            }
            throw new IllegalArgumentException("Inapplicable visibility: " + f1Var);
        }
        a(15);
        throw null;
    }
}
