package d0.e0.p.d.m0.c;

import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.k.a0.i;
import d0.z.d.m;
import java.util.List;
/* compiled from: PackageViewDescriptor.kt */
/* loaded from: classes3.dex */
public interface j0 extends m {

    /* compiled from: PackageViewDescriptor.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public static boolean isEmpty(j0 j0Var) {
            m.checkNotNullParameter(j0Var, "this");
            return j0Var.getFragments().isEmpty();
        }
    }

    b getFqName();

    List<e0> getFragments();

    i getMemberScope();

    c0 getModule();

    boolean isEmpty();
}
