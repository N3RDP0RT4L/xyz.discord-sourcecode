package d0.e0.p.d.m0.c.g1;

import andhook.lib.HookHelper;
import d0.e0.p.d.m0.c.g1.c;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.v.g;
import d0.e0.p.d.m0.n.c0;
import java.util.Map;
/* compiled from: AnnotationDescriptorImpl.java */
/* loaded from: classes3.dex */
public class d implements c {
    public final c0 a;

    /* renamed from: b  reason: collision with root package name */
    public final Map<e, g<?>> f3220b;
    public final u0 c;

    public d(c0 c0Var, Map<e, g<?>> map, u0 u0Var) {
        if (c0Var == null) {
            a(0);
            throw null;
        } else if (map == null) {
            a(1);
            throw null;
        } else if (u0Var != null) {
            this.a = c0Var;
            this.f3220b = map;
            this.c = u0Var;
        } else {
            a(2);
            throw null;
        }
    }

    public static /* synthetic */ void a(int i) {
        String str = (i == 3 || i == 4 || i == 5) ? "@NotNull method %s.%s must not return null" : "Argument for @NotNull parameter '%s' of %s.%s must not be null";
        Object[] objArr = new Object[(i == 3 || i == 4 || i == 5) ? 2 : 3];
        if (i == 1) {
            objArr[0] = "valueArguments";
        } else if (i == 2) {
            objArr[0] = "source";
        } else if (i == 3 || i == 4 || i == 5) {
            objArr[0] = "kotlin/reflect/jvm/internal/impl/descriptors/annotations/AnnotationDescriptorImpl";
        } else {
            objArr[0] = "annotationType";
        }
        if (i == 3) {
            objArr[1] = "getType";
        } else if (i == 4) {
            objArr[1] = "getAllValueArguments";
        } else if (i != 5) {
            objArr[1] = "kotlin/reflect/jvm/internal/impl/descriptors/annotations/AnnotationDescriptorImpl";
        } else {
            objArr[1] = "getSource";
        }
        if (!(i == 3 || i == 4 || i == 5)) {
            objArr[2] = HookHelper.constructorName;
        }
        String format = String.format(str, objArr);
        if (i == 3 || i == 4 || i == 5) {
            throw new IllegalStateException(format);
        }
    }

    @Override // d0.e0.p.d.m0.c.g1.c
    public Map<e, g<?>> getAllValueArguments() {
        Map<e, g<?>> map = this.f3220b;
        if (map != null) {
            return map;
        }
        a(4);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.g1.c
    public b getFqName() {
        return c.a.getFqName(this);
    }

    @Override // d0.e0.p.d.m0.c.g1.c
    public u0 getSource() {
        u0 u0Var = this.c;
        if (u0Var != null) {
            return u0Var;
        }
        a(5);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.g1.c
    public c0 getType() {
        c0 c0Var = this.a;
        if (c0Var != null) {
            return c0Var;
        }
        a(3);
        throw null;
    }

    public String toString() {
        return d0.e0.p.d.m0.j.c.f3411b.renderAnnotation(this, null);
    }
}
