package d0.e0.p.d.m0.c.g1;

import d0.e0.p.d.m0.b.h;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.v.g;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.j0;
import d0.i;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Map;
import kotlin.Lazy;
import kotlin.jvm.functions.Function0;
/* compiled from: BuiltInAnnotationDescriptor.kt */
/* loaded from: classes3.dex */
public final class j implements c {
    public final h a;

    /* renamed from: b  reason: collision with root package name */
    public final b f3224b;
    public final Map<e, g<?>> c;
    public final Lazy d = d0.g.lazy(i.PUBLICATION, new a());

    /* compiled from: BuiltInAnnotationDescriptor.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function0<j0> {
        public a() {
            super(0);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final j0 invoke() {
            return j.this.a.getBuiltInClassByFqName(j.this.getFqName()).getDefaultType();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public j(h hVar, b bVar, Map<e, ? extends g<?>> map) {
        m.checkNotNullParameter(hVar, "builtIns");
        m.checkNotNullParameter(bVar, "fqName");
        m.checkNotNullParameter(map, "allValueArguments");
        this.a = hVar;
        this.f3224b = bVar;
        this.c = map;
    }

    @Override // d0.e0.p.d.m0.c.g1.c
    public Map<e, g<?>> getAllValueArguments() {
        return this.c;
    }

    @Override // d0.e0.p.d.m0.c.g1.c
    public b getFqName() {
        return this.f3224b;
    }

    @Override // d0.e0.p.d.m0.c.g1.c
    public u0 getSource() {
        u0 u0Var = u0.a;
        m.checkNotNullExpressionValue(u0Var, "NO_SOURCE");
        return u0Var;
    }

    @Override // d0.e0.p.d.m0.c.g1.c
    public c0 getType() {
        Object value = this.d.getValue();
        m.checkNotNullExpressionValue(value, "pyright 2010-2017 JetBrains s.r.o.\n *\n * Licensed under the Apache License, Version 2.0 (the \"License\");\n * you may not use this file except in compliance with the License.\n * You may obtain a copy of the License at\n *\n * http://www.apache.org/licenses/LICENSE-2.0\n *\n * Unless required by applicable law or agreed to in writing, software\n * distributed under the License is distributed on an \"AS IS\" BASIS,\n * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n * See the License for the specific language governing permissions and\n * limitations under the License.\n */\n\npackage org.jetbrains.kotlin.descriptors.annotations\n\nimport org.jetbrains.kotlin.builtins.KotlinBuiltIns\nimport org.jetbrains.kotlin.descriptors.SourceElement\nimport org.jetbrains.kotlin.name.FqName\nimport org.jetbrains.kotlin.name.Name\nimport org.jetbrains.kotlin.resolve.constants.ConstantValue\nimport org.jetbrains.kotlin.types.KotlinType\nimport kotlin.LazyThreadSafetyMode.PUBLICATION\n\nclass BuiltInAnnotationDescriptor(\n        private val builtIns: KotlinBuiltIns,\n        override val fqName: FqName,\n        override val allValueArguments: Map<Name, ConstantValue<*>>\n) : AnnotationDescriptor {\n    override val type: KotlinType by lazy(PUBLICATION) {\n        builtIns.getBuiltInClassByFqName(fqName).defaultType\n    }");
        return (c0) value;
    }
}
