package d0.e0.p.d.m0.c.g1;

import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.k.v.g;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.t;
import d0.z.d.m;
import java.util.Map;
/* compiled from: AnnotationDescriptor.kt */
/* loaded from: classes3.dex */
public interface c {

    /* compiled from: AnnotationDescriptor.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public static b getFqName(c cVar) {
            m.checkNotNullParameter(cVar, "this");
            e annotationClass = d0.e0.p.d.m0.k.x.a.getAnnotationClass(cVar);
            if (annotationClass == null) {
                return null;
            }
            if (t.isError(annotationClass)) {
                annotationClass = null;
            }
            if (annotationClass == null) {
                return null;
            }
            return d0.e0.p.d.m0.k.x.a.fqNameOrNull(annotationClass);
        }
    }

    Map<d0.e0.p.d.m0.g.e, g<?>> getAllValueArguments();

    b getFqName();

    u0 getSource();

    c0 getType();
}
