package d0.e0.p.d.m0.c.g1;

import d0.f0.q;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.jvm.functions.Function1;
import kotlin.sequences.Sequence;
/* compiled from: Annotations.kt */
/* loaded from: classes3.dex */
public final class k implements g {
    public final List<g> j;

    /* compiled from: Annotations.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<g, c> {
        public final /* synthetic */ d0.e0.p.d.m0.g.b $fqName;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(d0.e0.p.d.m0.g.b bVar) {
            super(1);
            this.$fqName = bVar;
        }

        public final c invoke(g gVar) {
            m.checkNotNullParameter(gVar, "it");
            return gVar.findAnnotation(this.$fqName);
        }
    }

    /* compiled from: Annotations.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function1<g, Sequence<? extends c>> {
        public static final b j = new b();

        public b() {
            super(1);
        }

        public final Sequence<c> invoke(g gVar) {
            m.checkNotNullParameter(gVar, "it");
            return u.asSequence(gVar);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public k(List<? extends g> list) {
        m.checkNotNullParameter(list, "delegates");
        this.j = list;
    }

    @Override // d0.e0.p.d.m0.c.g1.g
    public c findAnnotation(d0.e0.p.d.m0.g.b bVar) {
        m.checkNotNullParameter(bVar, "fqName");
        return (c) q.firstOrNull(q.mapNotNull(u.asSequence(this.j), new a(bVar)));
    }

    @Override // d0.e0.p.d.m0.c.g1.g
    public boolean hasAnnotation(d0.e0.p.d.m0.g.b bVar) {
        m.checkNotNullParameter(bVar, "fqName");
        for (g gVar : u.asSequence(this.j)) {
            if (gVar.hasAnnotation(bVar)) {
                return true;
            }
        }
        return false;
    }

    @Override // d0.e0.p.d.m0.c.g1.g
    public boolean isEmpty() {
        List<g> list = this.j;
        if ((list instanceof Collection) && list.isEmpty()) {
            return true;
        }
        for (g gVar : list) {
            if (!gVar.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Iterable
    public Iterator<c> iterator() {
        return q.flatMap(u.asSequence(this.j), b.j).iterator();
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public k(g... gVarArr) {
        this(d0.t.k.toList(gVarArr));
        m.checkNotNullParameter(gVarArr, "delegates");
    }
}
