package d0.e0.p.d.m0.c.g1;

import d0.e0.p.d.m0.b.h;
import d0.e0.p.d.m0.b.k;
import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.v.b;
import d0.e0.p.d.m0.k.v.j;
import d0.e0.p.d.m0.k.v.w;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.j1;
import d0.t.h0;
import d0.t.n;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.jvm.functions.Function1;
/* compiled from: annotationUtil.kt */
/* loaded from: classes3.dex */
public final class f {
    public static final e a;

    /* renamed from: b */
    public static final e f3222b;
    public static final e c;
    public static final e d;
    public static final e e;

    /* compiled from: annotationUtil.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<c0, d0.e0.p.d.m0.n.c0> {
        public final /* synthetic */ h $this_createDeprecatedAnnotation;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(h hVar) {
            super(1);
            this.$this_createDeprecatedAnnotation = hVar;
        }

        public final d0.e0.p.d.m0.n.c0 invoke(c0 c0Var) {
            m.checkNotNullParameter(c0Var, "module");
            j0 arrayType = c0Var.getBuiltIns().getArrayType(j1.INVARIANT, this.$this_createDeprecatedAnnotation.getStringType());
            m.checkNotNullExpressionValue(arrayType, "module.builtIns.getArrayType(Variance.INVARIANT, stringType)");
            return arrayType;
        }
    }

    static {
        e identifier = e.identifier("message");
        m.checkNotNullExpressionValue(identifier, "identifier(\"message\")");
        a = identifier;
        e identifier2 = e.identifier("replaceWith");
        m.checkNotNullExpressionValue(identifier2, "identifier(\"replaceWith\")");
        f3222b = identifier2;
        e identifier3 = e.identifier("level");
        m.checkNotNullExpressionValue(identifier3, "identifier(\"level\")");
        c = identifier3;
        e identifier4 = e.identifier("expression");
        m.checkNotNullExpressionValue(identifier4, "identifier(\"expression\")");
        d = identifier4;
        e identifier5 = e.identifier("imports");
        m.checkNotNullExpressionValue(identifier5, "identifier(\"imports\")");
        e = identifier5;
    }

    public static final c createDeprecatedAnnotation(h hVar, String str, String str2, String str3) {
        m.checkNotNullParameter(hVar, "<this>");
        m.checkNotNullParameter(str, "message");
        m.checkNotNullParameter(str2, "replaceWith");
        m.checkNotNullParameter(str3, "level");
        j jVar = new j(hVar, k.a.w, h0.mapOf(d0.o.to(d, new w(str2)), d0.o.to(e, new b(n.emptyList(), new a(hVar)))));
        d0.e0.p.d.m0.g.b bVar = k.a.u;
        e eVar = c;
        d0.e0.p.d.m0.g.a aVar = d0.e0.p.d.m0.g.a.topLevel(k.a.v);
        m.checkNotNullExpressionValue(aVar, "topLevel(StandardNames.FqNames.deprecationLevel)");
        e identifier = e.identifier(str3);
        m.checkNotNullExpressionValue(identifier, "identifier(level)");
        return new j(hVar, bVar, h0.mapOf(d0.o.to(a, new w(str)), d0.o.to(f3222b, new d0.e0.p.d.m0.k.v.a(jVar)), d0.o.to(eVar, new j(aVar, identifier))));
    }

    public static /* synthetic */ c createDeprecatedAnnotation$default(h hVar, String str, String str2, String str3, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = "";
        }
        if ((i & 4) != 0) {
            str3 = "WARNING";
        }
        return createDeprecatedAnnotation(hVar, str, str2, str3);
    }
}
