package d0.e0.p.d.m0.c.g1;

import d0.t.n;
import d0.z.d.m;
import java.util.Iterator;
import java.util.List;
/* compiled from: Annotations.kt */
/* loaded from: classes3.dex */
public interface g extends Iterable<c>, d0.z.d.g0.a {
    public static final a f = a.a;

    /* compiled from: Annotations.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public static final /* synthetic */ a a = new a();

        /* renamed from: b  reason: collision with root package name */
        public static final g f3223b = new C0291a();

        /* compiled from: Annotations.kt */
        /* renamed from: d0.e0.p.d.m0.c.g1.g$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0291a implements g {
            @Override // d0.e0.p.d.m0.c.g1.g
            public Void findAnnotation(d0.e0.p.d.m0.g.b bVar) {
                m.checkNotNullParameter(bVar, "fqName");
                return null;
            }

            @Override // d0.e0.p.d.m0.c.g1.g
            public boolean hasAnnotation(d0.e0.p.d.m0.g.b bVar) {
                return b.hasAnnotation(this, bVar);
            }

            @Override // d0.e0.p.d.m0.c.g1.g
            public boolean isEmpty() {
                return true;
            }

            @Override // java.lang.Iterable
            public Iterator<c> iterator() {
                return n.emptyList().iterator();
            }

            public String toString() {
                return "EMPTY";
            }
        }

        public final g create(List<? extends c> list) {
            m.checkNotNullParameter(list, "annotations");
            return list.isEmpty() ? f3223b : new h(list);
        }

        public final g getEMPTY() {
            return f3223b;
        }
    }

    /* compiled from: Annotations.kt */
    /* loaded from: classes3.dex */
    public static final class b {
        public static c findAnnotation(g gVar, d0.e0.p.d.m0.g.b bVar) {
            c cVar;
            m.checkNotNullParameter(gVar, "this");
            m.checkNotNullParameter(bVar, "fqName");
            Iterator<c> it = gVar.iterator();
            while (true) {
                if (!it.hasNext()) {
                    cVar = null;
                    break;
                }
                cVar = it.next();
                if (m.areEqual(cVar.getFqName(), bVar)) {
                    break;
                }
            }
            return cVar;
        }

        public static boolean hasAnnotation(g gVar, d0.e0.p.d.m0.g.b bVar) {
            m.checkNotNullParameter(gVar, "this");
            m.checkNotNullParameter(bVar, "fqName");
            return gVar.findAnnotation(bVar) != null;
        }
    }

    c findAnnotation(d0.e0.p.d.m0.g.b bVar);

    boolean hasAnnotation(d0.e0.p.d.m0.g.b bVar);

    boolean isEmpty();
}
