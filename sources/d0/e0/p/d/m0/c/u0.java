package d0.e0.p.d.m0.c;
/* compiled from: SourceElement.java */
/* loaded from: classes3.dex */
public interface u0 {
    public static final u0 a = new a();

    /* compiled from: SourceElement.java */
    /* loaded from: classes3.dex */
    public static class a implements u0 {
        @Override // d0.e0.p.d.m0.c.u0
        public v0 getContainingFile() {
            return v0.a;
        }

        public String toString() {
            return "NO_SOURCE";
        }
    }

    v0 getContainingFile();
}
