package d0.e0.p.d.m0.c;

import d0.e0.p.d.m0.n.j0;
/* compiled from: TypeAliasDescriptor.kt */
/* loaded from: classes3.dex */
public interface y0 extends i {
    e getClassDescriptor();

    j0 getExpandedType();

    j0 getUnderlyingType();
}
