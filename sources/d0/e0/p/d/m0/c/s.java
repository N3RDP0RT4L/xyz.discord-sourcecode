package d0.e0.p.d.m0.c;

import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.a0.i;
import d0.z.d.m;
/* compiled from: descriptorUtil.kt */
/* loaded from: classes3.dex */
public final class s {
    public static final e resolveClassByFqName(c0 c0Var, b bVar, d0.e0.p.d.m0.d.b.b bVar2) {
        h hVar;
        i unsubstitutedInnerClassesScope;
        m.checkNotNullParameter(c0Var, "<this>");
        m.checkNotNullParameter(bVar, "fqName");
        m.checkNotNullParameter(bVar2, "lookupLocation");
        if (bVar.isRoot()) {
            return null;
        }
        b parent = bVar.parent();
        m.checkNotNullExpressionValue(parent, "fqName.parent()");
        i memberScope = c0Var.getPackage(parent).getMemberScope();
        e shortName = bVar.shortName();
        m.checkNotNullExpressionValue(shortName, "fqName.shortName()");
        h contributedClassifier = memberScope.getContributedClassifier(shortName, bVar2);
        e eVar = contributedClassifier instanceof e ? (e) contributedClassifier : null;
        if (eVar != null) {
            return eVar;
        }
        b parent2 = bVar.parent();
        m.checkNotNullExpressionValue(parent2, "fqName.parent()");
        e resolveClassByFqName = resolveClassByFqName(c0Var, parent2, bVar2);
        if (resolveClassByFqName == null || (unsubstitutedInnerClassesScope = resolveClassByFqName.getUnsubstitutedInnerClassesScope()) == null) {
            hVar = null;
        } else {
            e shortName2 = bVar.shortName();
            m.checkNotNullExpressionValue(shortName2, "fqName.shortName()");
            hVar = unsubstitutedInnerClassesScope.getContributedClassifier(shortName2, bVar2);
        }
        if (hVar instanceof e) {
            return (e) hVar;
        }
        return null;
    }
}
