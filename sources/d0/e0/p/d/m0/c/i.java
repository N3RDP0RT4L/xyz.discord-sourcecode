package d0.e0.p.d.m0.c;

import java.util.List;
/* compiled from: ClassifierDescriptorWithTypeParameters.java */
/* loaded from: classes3.dex */
public interface i extends h, q, y, w0<i> {
    List<z0> getDeclaredTypeParameters();

    boolean isInner();
}
