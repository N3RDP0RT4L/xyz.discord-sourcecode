package d0.e0.p.d.m0.c;

import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.u0;
import d0.z.d.m;
import java.util.Collection;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: SupertypeLoopChecker.kt */
/* loaded from: classes3.dex */
public interface x0 {

    /* compiled from: SupertypeLoopChecker.kt */
    /* loaded from: classes3.dex */
    public static final class a implements x0 {
        public static final a a = new a();

        /* JADX WARN: Multi-variable type inference failed */
        @Override // d0.e0.p.d.m0.c.x0
        public Collection<c0> findLoopsInSupertypesAndDisconnect(u0 u0Var, Collection<? extends c0> collection, Function1<? super u0, ? extends Iterable<? extends c0>> function1, Function1<? super c0, Unit> function12) {
            m.checkNotNullParameter(u0Var, "currentTypeConstructor");
            m.checkNotNullParameter(collection, "superTypes");
            m.checkNotNullParameter(function1, "neighbors");
            m.checkNotNullParameter(function12, "reportLoop");
            return collection;
        }
    }

    Collection<c0> findLoopsInSupertypesAndDisconnect(u0 u0Var, Collection<? extends c0> collection, Function1<? super u0, ? extends Iterable<? extends c0>> function1, Function1<? super c0, Unit> function12);
}
