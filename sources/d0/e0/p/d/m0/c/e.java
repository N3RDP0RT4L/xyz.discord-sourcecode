package d0.e0.p.d.m0.c;

import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.z0;
import java.util.Collection;
import java.util.List;
/* compiled from: ClassDescriptor.java */
/* loaded from: classes3.dex */
public interface e extends g, i {
    e getCompanionObjectDescriptor();

    Collection<d> getConstructors();

    @Override // d0.e0.p.d.m0.c.n, d0.e0.p.d.m0.c.m
    m getContainingDeclaration();

    List<z0> getDeclaredTypeParameters();

    @Override // d0.e0.p.d.m0.c.h
    j0 getDefaultType();

    f getKind();

    i getMemberScope(z0 z0Var);

    z getModality();

    @Override // d0.e0.p.d.m0.c.m
    e getOriginal();

    Collection<e> getSealedSubclasses();

    i getStaticScope();

    q0 getThisAsReceiverParameter();

    i getUnsubstitutedInnerClassesScope();

    i getUnsubstitutedMemberScope();

    d getUnsubstitutedPrimaryConstructor();

    u getVisibility();

    boolean isCompanionObject();

    boolean isData();

    boolean isFun();

    boolean isInline();

    boolean isValue();
}
