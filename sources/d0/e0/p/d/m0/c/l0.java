package d0.e0.p.d.m0.c;

import d0.e0.p.d.m0.n.w0;
import d0.z.d.m;
import java.util.List;
/* compiled from: typeParameterUtils.kt */
/* loaded from: classes3.dex */
public final class l0 {
    public final i a;

    /* renamed from: b  reason: collision with root package name */
    public final List<w0> f3270b;
    public final l0 c;

    /* JADX WARN: Multi-variable type inference failed */
    public l0(i iVar, List<? extends w0> list, l0 l0Var) {
        m.checkNotNullParameter(iVar, "classifierDescriptor");
        m.checkNotNullParameter(list, "arguments");
        this.a = iVar;
        this.f3270b = list;
        this.c = l0Var;
    }

    public final List<w0> getArguments() {
        return this.f3270b;
    }

    public final i getClassifierDescriptor() {
        return this.a;
    }

    public final l0 getOuterType() {
        return this.c;
    }
}
