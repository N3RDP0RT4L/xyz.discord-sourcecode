package d0.e0.p.d.m0.c;

import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.m.j;
import d0.e0.p.d.m0.m.n;
import d0.e0.p.d.m0.m.o;
import d0.e0.p.d.m0.n.l1.g;
import d0.e0.p.d.m0.n.u0;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.y;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
/* compiled from: ScopesHolderForClass.kt */
/* loaded from: classes3.dex */
public final class r0<T extends i> {
    public static final a a = new a(null);

    /* renamed from: b  reason: collision with root package name */
    public static final /* synthetic */ KProperty<Object>[] f3271b = {a0.property1(new y(a0.getOrCreateKotlinClass(r0.class), "scopeForOwnerModule", "getScopeForOwnerModule()Lorg/jetbrains/kotlin/resolve/scopes/MemberScope;"))};
    public final e c;
    public final Function1<g, T> d;
    public final g e;
    public final j f;

    /* compiled from: ScopesHolderForClass.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final <T extends i> r0<T> create(e eVar, o oVar, g gVar, Function1<? super g, ? extends T> function1) {
            m.checkNotNullParameter(eVar, "classDescriptor");
            m.checkNotNullParameter(oVar, "storageManager");
            m.checkNotNullParameter(gVar, "kotlinTypeRefinerForOwnerModule");
            m.checkNotNullParameter(function1, "scopeFactory");
            return new r0<>(eVar, oVar, function1, gVar, null);
        }
    }

    /* compiled from: ScopesHolderForClass.kt */
    /* loaded from: classes3.dex */
    public static final class b extends d0.z.d.o implements Function0<T> {
        public final /* synthetic */ g $kotlinTypeRefiner;
        public final /* synthetic */ r0<T> this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(r0<T> r0Var, g gVar) {
            super(0);
            this.this$0 = r0Var;
            this.$kotlinTypeRefiner = gVar;
        }

        @Override // kotlin.jvm.functions.Function0
        public final T invoke() {
            return (T) this.this$0.d.invoke(this.$kotlinTypeRefiner);
        }
    }

    public r0(e eVar, o oVar, Function1 function1, g gVar, DefaultConstructorMarker defaultConstructorMarker) {
        this.c = eVar;
        this.d = function1;
        this.e = gVar;
        this.f = oVar.createLazyValue(new s0(this));
    }

    public final T getScope(g gVar) {
        m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
        if (!gVar.isRefinementNeededForModule(d0.e0.p.d.m0.k.x.a.getModule(this.c))) {
            return (T) n.getValue(this.f, this, f3271b[0]);
        }
        u0 typeConstructor = this.c.getTypeConstructor();
        m.checkNotNullExpressionValue(typeConstructor, "classDescriptor.typeConstructor");
        if (!gVar.isRefinementNeededForTypeConstructor(typeConstructor)) {
            return (T) n.getValue(this.f, this, f3271b[0]);
        }
        return (T) gVar.getOrPutScopeForClass(this.c, new b(this, gVar));
    }
}
