package d0.e0.p.d.m0.c.i1;

import d0.e0.p.d.m0.c.e0;
import d0.e0.p.d.m0.c.f0;
import d0.e0.p.d.m0.c.h0;
import d0.e0.p.d.m0.c.i0;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.e;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import kotlin.jvm.functions.Function1;
/* compiled from: CompositePackageFragmentProvider.kt */
/* loaded from: classes3.dex */
public final class j implements i0 {
    public final List<f0> a;

    /* JADX WARN: Multi-variable type inference failed */
    public j(List<? extends f0> list) {
        m.checkNotNullParameter(list, "providers");
        this.a = list;
        list.size();
        u.toSet(list).size();
    }

    @Override // d0.e0.p.d.m0.c.i0
    public void collectPackageFragments(b bVar, Collection<e0> collection) {
        m.checkNotNullParameter(bVar, "fqName");
        m.checkNotNullParameter(collection, "packageFragments");
        for (f0 f0Var : this.a) {
            h0.collectPackageFragmentsOptimizedIfPossible(f0Var, bVar, collection);
        }
    }

    @Override // d0.e0.p.d.m0.c.f0
    public List<e0> getPackageFragments(b bVar) {
        m.checkNotNullParameter(bVar, "fqName");
        ArrayList arrayList = new ArrayList();
        for (f0 f0Var : this.a) {
            h0.collectPackageFragmentsOptimizedIfPossible(f0Var, bVar, arrayList);
        }
        return u.toList(arrayList);
    }

    @Override // d0.e0.p.d.m0.c.f0
    public Collection<b> getSubPackagesOf(b bVar, Function1<? super e, Boolean> function1) {
        m.checkNotNullParameter(bVar, "fqName");
        m.checkNotNullParameter(function1, "nameFilter");
        HashSet hashSet = new HashSet();
        for (f0 f0Var : this.a) {
            hashSet.addAll(f0Var.getSubPackagesOf(bVar, function1));
        }
        return hashSet;
    }
}
