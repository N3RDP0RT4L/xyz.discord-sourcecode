package d0.e0.p.d.m0.c.i1;

import d0.e0.p.d.m0.c.g1.b;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.v;
import d0.z.d.m;
/* compiled from: FieldDescriptorImpl.kt */
/* loaded from: classes3.dex */
public final class p extends b implements v {
    public final n0 k;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public p(g gVar, n0 n0Var) {
        super(gVar);
        m.checkNotNullParameter(gVar, "annotations");
        m.checkNotNullParameter(n0Var, "correspondingProperty");
        this.k = n0Var;
    }
}
