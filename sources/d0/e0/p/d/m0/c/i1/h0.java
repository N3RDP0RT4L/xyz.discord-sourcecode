package d0.e0.p.d.m0.c.i1;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.j0;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.a0.c;
import d0.e0.p.d.m0.k.a0.d;
import d0.e0.p.d.m0.k.a0.j;
import d0.e0.p.d.m0.p.a;
import d0.t.n;
import d0.t.n0;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import kotlin.jvm.functions.Function1;
/* compiled from: SubpackagesScope.kt */
/* loaded from: classes3.dex */
public class h0 extends j {

    /* renamed from: b  reason: collision with root package name */
    public final c0 f3238b;
    public final b c;

    public h0(c0 c0Var, b bVar) {
        m.checkNotNullParameter(c0Var, "moduleDescriptor");
        m.checkNotNullParameter(bVar, "fqName");
        this.f3238b = c0Var;
        this.c = bVar;
    }

    @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.i
    public Set<e> getClassifierNames() {
        return n0.emptySet();
    }

    @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.l
    public Collection<d0.e0.p.d.m0.c.m> getContributedDescriptors(d dVar, Function1<? super e, Boolean> function1) {
        m.checkNotNullParameter(dVar, "kindFilter");
        m.checkNotNullParameter(function1, "nameFilter");
        if (!dVar.acceptsKinds(d.a.getPACKAGES_MASK())) {
            return n.emptyList();
        }
        if (this.c.isRoot() && dVar.getExcludes().contains(c.b.a)) {
            return n.emptyList();
        }
        Collection<b> subPackagesOf = this.f3238b.getSubPackagesOf(this.c, function1);
        ArrayList arrayList = new ArrayList(subPackagesOf.size());
        for (b bVar : subPackagesOf) {
            e shortName = bVar.shortName();
            m.checkNotNullExpressionValue(shortName, "subFqName.shortName()");
            if (function1.invoke(shortName).booleanValue()) {
                m.checkNotNullParameter(shortName, ModelAuditLogEntry.CHANGE_KEY_NAME);
                j0 j0Var = null;
                if (!shortName.isSpecial()) {
                    c0 c0Var = this.f3238b;
                    b child = this.c.child(shortName);
                    m.checkNotNullExpressionValue(child, "fqName.child(name)");
                    j0 j0Var2 = c0Var.getPackage(child);
                    if (!j0Var2.isEmpty()) {
                        j0Var = j0Var2;
                    }
                }
                a.addIfNotNull(arrayList, j0Var);
            }
        }
        return arrayList;
    }
}
