package d0.e0.p.d.m0.c.i1;

import d0.e0.p.d.m0.b.h;
import d0.e0.p.d.m0.c.b0;
import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.f0;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.j0;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.m.f;
import d0.e0.p.d.m0.m.o;
import d0.t.h0;
import d0.t.k;
import d0.t.n;
import d0.t.n0;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Lazy;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.jvm.internal.impl.descriptors.InvalidModuleException;
/* compiled from: ModuleDescriptorImpl.kt */
/* loaded from: classes3.dex */
public final class y extends k implements c0 {
    public final o l;
    public final h m;
    public final Map<b0<?>, Object> n;
    public w o;
    public f0 p;
    public boolean q;
    public final d0.e0.p.d.m0.m.h<d0.e0.p.d.m0.g.b, j0> r;

    /* renamed from: s  reason: collision with root package name */
    public final Lazy f3250s;

    /* compiled from: ModuleDescriptorImpl.kt */
    /* loaded from: classes3.dex */
    public static final class a extends d0.z.d.o implements Function0<j> {
        public a() {
            super(0);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final j invoke() {
            w wVar = y.this.o;
            y yVar = y.this;
            if (wVar != null) {
                List<y> allDependencies = wVar.getAllDependencies();
                allDependencies.contains(y.this);
                for (y yVar2 : allDependencies) {
                    y.access$isInitialized(yVar2);
                }
                ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(allDependencies, 10));
                for (y yVar3 : allDependencies) {
                    f0 f0Var = yVar3.p;
                    m.checkNotNull(f0Var);
                    arrayList.add(f0Var);
                }
                return new j(arrayList);
            }
            StringBuilder R = b.d.b.a.a.R("Dependencies of module ");
            R.append(yVar.b());
            R.append(" were not set before querying module content");
            throw new AssertionError(R.toString());
        }
    }

    /* compiled from: ModuleDescriptorImpl.kt */
    /* loaded from: classes3.dex */
    public static final class b extends d0.z.d.o implements Function1<d0.e0.p.d.m0.g.b, j0> {
        public b() {
            super(1);
        }

        public final j0 invoke(d0.e0.p.d.m0.g.b bVar) {
            m.checkNotNullParameter(bVar, "fqName");
            y yVar = y.this;
            return new s(yVar, bVar, yVar.l);
        }
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public y(e eVar, o oVar, h hVar, d0.e0.p.d.m0.h.a aVar) {
        this(eVar, oVar, hVar, aVar, null, null, 48, null);
        m.checkNotNullParameter(eVar, "moduleName");
        m.checkNotNullParameter(oVar, "storageManager");
        m.checkNotNullParameter(hVar, "builtIns");
    }

    public /* synthetic */ y(e eVar, o oVar, h hVar, d0.e0.p.d.m0.h.a aVar, Map map, e eVar2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(eVar, oVar, hVar, (i & 8) != 0 ? null : aVar, (i & 16) != 0 ? h0.emptyMap() : map, (i & 32) != 0 ? null : eVar2);
    }

    public static final boolean access$isInitialized(y yVar) {
        return yVar.p != null;
    }

    @Override // d0.e0.p.d.m0.c.m
    public <R, D> R accept(d0.e0.p.d.m0.c.o<R, D> oVar, D d) {
        return (R) c0.a.accept(this, oVar, d);
    }

    public void assertValid() {
        if (!isValid()) {
            throw new InvalidModuleException(m.stringPlus("Accessing invalid module descriptor ", this));
        }
    }

    public final String b() {
        String eVar = getName().toString();
        m.checkNotNullExpressionValue(eVar, "name.toString()");
        return eVar;
    }

    @Override // d0.e0.p.d.m0.c.c0
    public h getBuiltIns() {
        return this.m;
    }

    @Override // d0.e0.p.d.m0.c.c0
    public <T> T getCapability(b0<T> b0Var) {
        m.checkNotNullParameter(b0Var, "capability");
        return (T) this.n.get(b0Var);
    }

    @Override // d0.e0.p.d.m0.c.m
    public d0.e0.p.d.m0.c.m getContainingDeclaration() {
        return c0.a.getContainingDeclaration(this);
    }

    @Override // d0.e0.p.d.m0.c.c0
    public List<c0> getExpectedByModules() {
        w wVar = this.o;
        if (wVar != null) {
            return wVar.getDirectExpectedByDependencies();
        }
        StringBuilder R = b.d.b.a.a.R("Dependencies of module ");
        R.append(b());
        R.append(" were not set");
        throw new AssertionError(R.toString());
    }

    @Override // d0.e0.p.d.m0.c.c0
    public j0 getPackage(d0.e0.p.d.m0.g.b bVar) {
        m.checkNotNullParameter(bVar, "fqName");
        assertValid();
        return (j0) ((f.m) this.r).invoke(bVar);
    }

    public final f0 getPackageFragmentProvider() {
        assertValid();
        return (j) this.f3250s.getValue();
    }

    @Override // d0.e0.p.d.m0.c.c0
    public Collection<d0.e0.p.d.m0.g.b> getSubPackagesOf(d0.e0.p.d.m0.g.b bVar, Function1<? super e, Boolean> function1) {
        m.checkNotNullParameter(bVar, "fqName");
        m.checkNotNullParameter(function1, "nameFilter");
        assertValid();
        return getPackageFragmentProvider().getSubPackagesOf(bVar, function1);
    }

    public final void initialize(f0 f0Var) {
        m.checkNotNullParameter(f0Var, "providerForModuleContent");
        this.p = f0Var;
    }

    public boolean isValid() {
        return this.q;
    }

    public final void setDependencies(w wVar) {
        m.checkNotNullParameter(wVar, "dependencies");
        w wVar2 = this.o;
        this.o = wVar;
    }

    @Override // d0.e0.p.d.m0.c.c0
    public boolean shouldSeeInternalsOf(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "targetModule");
        if (m.areEqual(this, c0Var)) {
            return true;
        }
        w wVar = this.o;
        m.checkNotNull(wVar);
        return u.contains(wVar.getModulesWhoseInternalsAreVisible(), c0Var) || getExpectedByModules().contains(c0Var) || c0Var.getExpectedByModules().contains(this);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public y(e eVar, o oVar, h hVar, d0.e0.p.d.m0.h.a aVar, Map<b0<?>, ? extends Object> map, e eVar2) {
        super(g.f.getEMPTY(), eVar);
        m.checkNotNullParameter(eVar, "moduleName");
        m.checkNotNullParameter(oVar, "storageManager");
        m.checkNotNullParameter(hVar, "builtIns");
        m.checkNotNullParameter(map, "capabilities");
        this.l = oVar;
        this.m = hVar;
        if (eVar.isSpecial()) {
            Map<b0<?>, Object> mutableMap = h0.toMutableMap(map);
            this.n = mutableMap;
            mutableMap.put(d0.e0.p.d.m0.n.l1.h.getREFINER_CAPABILITY(), new d0.e0.p.d.m0.n.l1.o(null));
            this.q = true;
            this.r = oVar.createMemoizedFunction(new b());
            this.f3250s = d0.g.lazy(new a());
            return;
        }
        throw new IllegalArgumentException(m.stringPlus("Module name must be special: ", eVar));
    }

    public final void setDependencies(y... yVarArr) {
        m.checkNotNullParameter(yVarArr, "descriptors");
        setDependencies(k.toList(yVarArr));
    }

    public final void setDependencies(List<y> list) {
        m.checkNotNullParameter(list, "descriptors");
        setDependencies(list, n0.emptySet());
    }

    public final void setDependencies(List<y> list, Set<y> set) {
        m.checkNotNullParameter(list, "descriptors");
        m.checkNotNullParameter(set, "friends");
        setDependencies(new x(list, set, n.emptyList(), n0.emptySet()));
    }
}
