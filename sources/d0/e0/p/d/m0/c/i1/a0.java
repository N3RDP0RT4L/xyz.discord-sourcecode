package d0.e0.p.d.m0.c.i1;

import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.e0;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.o;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.g.b;
import d0.z.d.m;
/* compiled from: PackageFragmentDescriptorImpl.kt */
/* loaded from: classes3.dex */
public abstract class a0 extends l implements e0 {
    public final b n;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public a0(c0 c0Var, b bVar) {
        super(c0Var, g.f.getEMPTY(), bVar.shortNameOrSpecial(), u0.a);
        m.checkNotNullParameter(c0Var, "module");
        m.checkNotNullParameter(bVar, "fqName");
        this.n = bVar;
    }

    @Override // d0.e0.p.d.m0.c.m
    public <R, D> R accept(o<R, D> oVar, D d) {
        m.checkNotNullParameter(oVar, "visitor");
        return oVar.visitPackageFragmentDescriptor(this, d);
    }

    @Override // d0.e0.p.d.m0.c.e0
    public final b getFqName() {
        return this.n;
    }

    @Override // d0.e0.p.d.m0.c.i1.l, d0.e0.p.d.m0.c.p
    public u0 getSource() {
        u0 u0Var = u0.a;
        m.checkNotNullExpressionValue(u0Var, "NO_SOURCE");
        return u0Var;
    }

    @Override // d0.e0.p.d.m0.c.i1.k
    public String toString() {
        return m.stringPlus("package ", this.n);
    }

    @Override // d0.e0.p.d.m0.c.i1.l, d0.e0.p.d.m0.c.m
    public c0 getContainingDeclaration() {
        return (c0) super.getContainingDeclaration();
    }
}
