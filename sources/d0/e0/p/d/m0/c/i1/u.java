package d0.e0.p.d.m0.c.i1;

import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.n.l1.g;
import d0.e0.p.d.m0.n.z0;
import d0.z.d.m;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ModuleAwareClassDescriptor.kt */
/* loaded from: classes3.dex */
public abstract class u implements e {
    public static final a j = new a(null);

    /* compiled from: ModuleAwareClassDescriptor.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final i getRefinedMemberScopeIfPossible$descriptors(e eVar, z0 z0Var, g gVar) {
            m.checkNotNullParameter(eVar, "<this>");
            m.checkNotNullParameter(z0Var, "typeSubstitution");
            m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
            u uVar = eVar instanceof u ? (u) eVar : null;
            if (uVar != null) {
                return uVar.getMemberScope(z0Var, gVar);
            }
            i memberScope = eVar.getMemberScope(z0Var);
            m.checkNotNullExpressionValue(memberScope, "this.getMemberScope(\n                typeSubstitution\n            )");
            return memberScope;
        }

        public final i getRefinedUnsubstitutedMemberScopeIfPossible$descriptors(e eVar, g gVar) {
            m.checkNotNullParameter(eVar, "<this>");
            m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
            u uVar = eVar instanceof u ? (u) eVar : null;
            if (uVar != null) {
                return uVar.getUnsubstitutedMemberScope(gVar);
            }
            i unsubstitutedMemberScope = eVar.getUnsubstitutedMemberScope();
            m.checkNotNullExpressionValue(unsubstitutedMemberScope, "this.unsubstitutedMemberScope");
            return unsubstitutedMemberScope;
        }
    }

    public abstract i getMemberScope(z0 z0Var, g gVar);

    public abstract i getUnsubstitutedMemberScope(g gVar);
}
