package d0.e0.p.d.m0.c.i1;

import andhook.lib.HookHelper;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.g1.b;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.j.c;
/* compiled from: DeclarationDescriptorImpl.java */
/* loaded from: classes3.dex */
public abstract class k extends b implements m {
    public final e k;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public k(g gVar, e eVar) {
        super(gVar);
        if (gVar == null) {
            a(0);
            throw null;
        } else if (eVar != null) {
            this.k = eVar;
        } else {
            a(1);
            throw null;
        }
    }

    public static /* synthetic */ void a(int i) {
        String str = (i == 2 || i == 3 || i == 5 || i == 6) ? "@NotNull method %s.%s must not return null" : "Argument for @NotNull parameter '%s' of %s.%s must not be null";
        Object[] objArr = new Object[(i == 2 || i == 3 || i == 5 || i == 6) ? 2 : 3];
        switch (i) {
            case 1:
                objArr[0] = ModelAuditLogEntry.CHANGE_KEY_NAME;
                break;
            case 2:
            case 3:
            case 5:
            case 6:
                objArr[0] = "kotlin/reflect/jvm/internal/impl/descriptors/impl/DeclarationDescriptorImpl";
                break;
            case 4:
                objArr[0] = "descriptor";
                break;
            default:
                objArr[0] = "annotations";
                break;
        }
        if (i == 2) {
            objArr[1] = "getName";
        } else if (i == 3) {
            objArr[1] = "getOriginal";
        } else if (i == 5 || i == 6) {
            objArr[1] = "toString";
        } else {
            objArr[1] = "kotlin/reflect/jvm/internal/impl/descriptors/impl/DeclarationDescriptorImpl";
        }
        if (!(i == 2 || i == 3)) {
            if (i == 4) {
                objArr[2] = "toString";
            } else if (!(i == 5 || i == 6)) {
                objArr[2] = HookHelper.constructorName;
            }
        }
        String format = String.format(str, objArr);
        if (i == 2 || i == 3 || i == 5 || i == 6) {
            throw new IllegalStateException(format);
        }
    }

    @Override // d0.e0.p.d.m0.c.m
    public e getName() {
        e eVar = this.k;
        if (eVar != null) {
            return eVar;
        }
        a(2);
        throw null;
    }

    public m getOriginal() {
        return this;
    }

    public String toString() {
        return toString(this);
    }

    public static String toString(m mVar) {
        if (mVar != null) {
            try {
                String str = c.c.render(mVar) + "[" + mVar.getClass().getSimpleName() + "@" + Integer.toHexString(System.identityHashCode(mVar)) + "]";
                if (str != null) {
                    return str;
                }
                a(5);
                throw null;
            } catch (Throwable unused) {
                String str2 = mVar.getClass().getSimpleName() + " " + mVar.getName();
                if (str2 != null) {
                    return str2;
                }
                a(6);
                throw null;
            }
        } else {
            a(4);
            throw null;
        }
    }
}
