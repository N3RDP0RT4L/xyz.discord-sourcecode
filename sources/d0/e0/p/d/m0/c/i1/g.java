package d0.e0.p.d.m0.c.i1;

import andhook.lib.HookHelper;
import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.d;
import d0.e0.p.d.m0.c.l;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.c.o;
import d0.e0.p.d.m0.c.q0;
import d0.e0.p.d.m0.c.u;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.c.x;
import d0.e0.p.d.m0.c.z;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.g.e;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
/* compiled from: ClassConstructorDescriptorImpl.java */
/* loaded from: classes3.dex */
public class g extends q implements d {
    public static final e M = e.special(HookHelper.constructorName);
    public final boolean N;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public g(d0.e0.p.d.m0.c.e eVar, l lVar, d0.e0.p.d.m0.c.g1.g gVar, boolean z2, b.a aVar, u0 u0Var) {
        super(eVar, lVar, gVar, M, aVar, u0Var);
        if (eVar == null) {
            a(0);
            throw null;
        } else if (gVar == null) {
            a(1);
            throw null;
        } else if (aVar == null) {
            a(2);
            throw null;
        } else if (u0Var != null) {
            this.N = z2;
        } else {
            a(3);
            throw null;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:12:0x0018  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:17:0x0028  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x002d  */
    /* JADX WARN: Removed duplicated region for block: B:19:0x0032  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x0037  */
    /* JADX WARN: Removed duplicated region for block: B:21:0x003a  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x003f  */
    /* JADX WARN: Removed duplicated region for block: B:23:0x0044  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x0049  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x004e  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x0053  */
    /* JADX WARN: Removed duplicated region for block: B:29:0x005a  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x0076  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x007d  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x0082  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x0087  */
    /* JADX WARN: Removed duplicated region for block: B:41:0x008c  */
    /* JADX WARN: Removed duplicated region for block: B:42:0x0091  */
    /* JADX WARN: Removed duplicated region for block: B:43:0x0096  */
    /* JADX WARN: Removed duplicated region for block: B:44:0x009b  */
    /* JADX WARN: Removed duplicated region for block: B:47:0x00a5 A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:49:0x00aa  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static /* synthetic */ void a(int r8) {
        /*
            Method dump skipped, instructions count: 324
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.c.i1.g.a(int):void");
    }

    public static g create(d0.e0.p.d.m0.c.e eVar, d0.e0.p.d.m0.c.g1.g gVar, boolean z2, u0 u0Var) {
        if (eVar == null) {
            a(4);
            throw null;
        } else if (gVar == null) {
            a(5);
            throw null;
        } else if (u0Var != null) {
            return new g(eVar, null, gVar, z2, b.a.DECLARATION, u0Var);
        } else {
            a(6);
            throw null;
        }
    }

    @Override // d0.e0.p.d.m0.c.i1.q, d0.e0.p.d.m0.c.m
    public <R, D> R accept(o<R, D> oVar, D d) {
        return oVar.visitConstructorDescriptor(this, d);
    }

    public q0 calculateDispatchReceiverParameter() {
        d0.e0.p.d.m0.c.e containingDeclaration = getContainingDeclaration();
        if (!containingDeclaration.isInner()) {
            return null;
        }
        m containingDeclaration2 = containingDeclaration.getContainingDeclaration();
        if (containingDeclaration2 instanceof d0.e0.p.d.m0.c.e) {
            return ((d0.e0.p.d.m0.c.e) containingDeclaration2).getThisAsReceiverParameter();
        }
        return null;
    }

    /* renamed from: e */
    public g b(m mVar, x xVar, b.a aVar, e eVar, d0.e0.p.d.m0.c.g1.g gVar, u0 u0Var) {
        if (mVar == null) {
            a(21);
            throw null;
        } else if (aVar == null) {
            a(22);
            throw null;
        } else if (gVar == null) {
            a(23);
            throw null;
        } else if (u0Var != null) {
            b.a aVar2 = b.a.DECLARATION;
            if (aVar == aVar2 || aVar == b.a.SYNTHESIZED) {
                return new g((d0.e0.p.d.m0.c.e) mVar, this, gVar, this.N, aVar2, u0Var);
            }
            throw new IllegalStateException("Attempt at creating a constructor that is not a declaration: \ncopy from: " + this + "\nnewOwner: " + mVar + "\nkind: " + aVar);
        } else {
            a(24);
            throw null;
        }
    }

    @Override // d0.e0.p.d.m0.c.l
    public d0.e0.p.d.m0.c.e getConstructedClass() {
        d0.e0.p.d.m0.c.e containingDeclaration = getContainingDeclaration();
        if (containingDeclaration != null) {
            return containingDeclaration;
        }
        a(16);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.i1.q, d0.e0.p.d.m0.c.x, d0.e0.p.d.m0.c.b, d0.e0.p.d.m0.c.a
    public Collection<? extends x> getOverriddenDescriptors() {
        Set emptySet = Collections.emptySet();
        if (emptySet != null) {
            return emptySet;
        }
        a(19);
        throw null;
    }

    public g initialize(List<c1> list, u uVar, List<z0> list2) {
        if (list == null) {
            a(10);
            throw null;
        } else if (uVar == null) {
            a(11);
            throw null;
        } else if (list2 != null) {
            initialize(null, calculateDispatchReceiverParameter(), list2, list, null, z.FINAL, uVar);
            return this;
        } else {
            a(12);
            throw null;
        }
    }

    @Override // d0.e0.p.d.m0.c.l
    public boolean isPrimary() {
        return this.N;
    }

    @Override // d0.e0.p.d.m0.c.i1.q, d0.e0.p.d.m0.c.b
    public void setOverriddenDescriptors(Collection<? extends b> collection) {
        if (collection == null) {
            a(20);
            throw null;
        }
    }

    @Override // d0.e0.p.d.m0.c.i1.q, d0.e0.p.d.m0.c.b
    public d copy(m mVar, z zVar, u uVar, b.a aVar, boolean z2) {
        d dVar = (d) super.copy(mVar, zVar, uVar, aVar, z2);
        if (dVar != null) {
            return dVar;
        }
        a(25);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.i1.l, d0.e0.p.d.m0.c.m
    public d0.e0.p.d.m0.c.e getContainingDeclaration() {
        d0.e0.p.d.m0.c.e eVar = (d0.e0.p.d.m0.c.e) super.getContainingDeclaration();
        if (eVar != null) {
            return eVar;
        }
        a(15);
        throw null;
    }

    public g initialize(List<c1> list, u uVar) {
        if (list == null) {
            a(13);
            throw null;
        } else if (uVar != null) {
            initialize(list, uVar, getContainingDeclaration().getDeclaredTypeParameters());
            return this;
        } else {
            a(14);
            throw null;
        }
    }

    @Override // d0.e0.p.d.m0.c.i1.q, d0.e0.p.d.m0.c.x, d0.e0.p.d.m0.c.w0
    public d substitute(d0.e0.p.d.m0.n.c1 c1Var) {
        if (c1Var != null) {
            return (d) super.substitute(c1Var);
        }
        a(18);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.i1.q, d0.e0.p.d.m0.c.i1.l, d0.e0.p.d.m0.c.i1.k, d0.e0.p.d.m0.c.m
    public d getOriginal() {
        d dVar = (d) super.getOriginal();
        if (dVar != null) {
            return dVar;
        }
        a(17);
        throw null;
    }
}
