package d0.e0.p.d.m0.c.i1;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.k.a0.p.c;
import d0.e0.p.d.m0.k.a0.p.d;
/* compiled from: LazyClassReceiverParameterDescriptor.java */
/* loaded from: classes3.dex */
public class r extends c {
    public final e m;
    public final c n;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public r(e eVar) {
        super(g.f.getEMPTY());
        if (eVar != null) {
            this.m = eVar;
            this.n = new c(eVar, null);
            return;
        }
        a(0);
        throw null;
    }

    public static /* synthetic */ void a(int i) {
        String str = (i == 1 || i == 2) ? "@NotNull method %s.%s must not return null" : "Argument for @NotNull parameter '%s' of %s.%s must not be null";
        Object[] objArr = new Object[(i == 1 || i == 2) ? 2 : 3];
        if (i == 1 || i == 2) {
            objArr[0] = "kotlin/reflect/jvm/internal/impl/descriptors/impl/LazyClassReceiverParameterDescriptor";
        } else if (i != 3) {
            objArr[0] = "descriptor";
        } else {
            objArr[0] = "newOwner";
        }
        if (i == 1) {
            objArr[1] = "getValue";
        } else if (i != 2) {
            objArr[1] = "kotlin/reflect/jvm/internal/impl/descriptors/impl/LazyClassReceiverParameterDescriptor";
        } else {
            objArr[1] = "getContainingDeclaration";
        }
        if (!(i == 1 || i == 2)) {
            if (i != 3) {
                objArr[2] = HookHelper.constructorName;
            } else {
                objArr[2] = "copy";
            }
        }
        String format = String.format(str, objArr);
        if (i == 1 || i == 2) {
            throw new IllegalStateException(format);
        }
    }

    @Override // d0.e0.p.d.m0.c.m
    public m getContainingDeclaration() {
        e eVar = this.m;
        if (eVar != null) {
            return eVar;
        }
        a(2);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.q0
    public d getValue() {
        c cVar = this.n;
        if (cVar != null) {
            return cVar;
        }
        a(1);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.i1.k
    public String toString() {
        StringBuilder R = a.R("class ");
        R.append(this.m.getName());
        R.append("::this");
        return R.toString();
    }
}
