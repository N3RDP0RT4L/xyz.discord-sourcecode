package d0.e0.p.d.m0.c.i1;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.c.m0;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.p0;
import d0.e0.p.d.m0.c.q0;
import d0.e0.p.d.m0.c.t;
import d0.e0.p.d.m0.c.u;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.c.v;
import d0.e0.p.d.m0.c.x;
import d0.e0.p.d.m0.c.z;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.v.g;
import d0.e0.p.d.m0.m.k;
import d0.e0.p.d.m0.n.c1;
import d0.e0.p.d.m0.n.j1;
import d0.e0.p.d.m0.n.o;
import d0.e0.p.d.m0.p.j;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
/* compiled from: PropertyDescriptorImpl.java */
/* loaded from: classes3.dex */
public class c0 extends n0 implements n0 {
    public final boolean A;
    public q0 B;
    public q0 C;
    public List<z0> D;
    public d0 E;
    public p0 F;
    public boolean G;
    public v H;
    public v I;
    public final z q;
    public u r;

    /* renamed from: s  reason: collision with root package name */
    public Collection<? extends n0> f3231s;
    public final n0 t;
    public final b.a u;
    public final boolean v;
    public final boolean w;

    /* renamed from: x  reason: collision with root package name */
    public final boolean f3232x;

    /* renamed from: y  reason: collision with root package name */
    public final boolean f3233y;

    /* renamed from: z  reason: collision with root package name */
    public final boolean f3234z;

    /* compiled from: PropertyDescriptorImpl.java */
    /* loaded from: classes3.dex */
    public class a {
        public m a;

        /* renamed from: b  reason: collision with root package name */
        public z f3235b;
        public u c;
        public b.a e;
        public q0 h;
        public e i;
        public d0.e0.p.d.m0.n.c0 j;
        public n0 d = null;
        public d0.e0.p.d.m0.n.z0 f = d0.e0.p.d.m0.n.z0.a;
        public boolean g = true;

        public a() {
            this.a = c0.this.getContainingDeclaration();
            this.f3235b = c0.this.getModality();
            this.c = c0.this.getVisibility();
            this.e = c0.this.getKind();
            this.h = c0.this.B;
            this.i = c0.this.getName();
            this.j = c0.this.getType();
        }

        public static /* synthetic */ void a(int i) {
            String str = (i == 1 || i == 2 || i == 3 || i == 5 || i == 7 || i == 9 || i == 11 || i == 19 || i == 13 || i == 14 || i == 16 || i == 17) ? "@NotNull method %s.%s must not return null" : "Argument for @NotNull parameter '%s' of %s.%s must not be null";
            Object[] objArr = new Object[(i == 1 || i == 2 || i == 3 || i == 5 || i == 7 || i == 9 || i == 11 || i == 19 || i == 13 || i == 14 || i == 16 || i == 17) ? 2 : 3];
            switch (i) {
                case 1:
                case 2:
                case 3:
                case 5:
                case 7:
                case 9:
                case 11:
                case 13:
                case 14:
                case 16:
                case 17:
                case 19:
                    objArr[0] = "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl$CopyConfiguration";
                    break;
                case 4:
                    objArr[0] = "type";
                    break;
                case 6:
                    objArr[0] = "modality";
                    break;
                case 8:
                    objArr[0] = "visibility";
                    break;
                case 10:
                    objArr[0] = "kind";
                    break;
                case 12:
                    objArr[0] = "typeParameters";
                    break;
                case 15:
                    objArr[0] = "substitution";
                    break;
                case 18:
                    objArr[0] = ModelAuditLogEntry.CHANGE_KEY_NAME;
                    break;
                default:
                    objArr[0] = "owner";
                    break;
            }
            if (i == 1) {
                objArr[1] = "setOwner";
            } else if (i == 2) {
                objArr[1] = "setOriginal";
            } else if (i == 3) {
                objArr[1] = "setPreserveSourceElement";
            } else if (i == 5) {
                objArr[1] = "setReturnType";
            } else if (i == 7) {
                objArr[1] = "setModality";
            } else if (i == 9) {
                objArr[1] = "setVisibility";
            } else if (i == 11) {
                objArr[1] = "setKind";
            } else if (i == 19) {
                objArr[1] = "setName";
            } else if (i == 13) {
                objArr[1] = "setTypeParameters";
            } else if (i == 14) {
                objArr[1] = "setDispatchReceiverParameter";
            } else if (i == 16) {
                objArr[1] = "setSubstitution";
            } else if (i != 17) {
                objArr[1] = "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl$CopyConfiguration";
            } else {
                objArr[1] = "setCopyOverrides";
            }
            switch (i) {
                case 1:
                case 2:
                case 3:
                case 5:
                case 7:
                case 9:
                case 11:
                case 13:
                case 14:
                case 16:
                case 17:
                case 19:
                    break;
                case 4:
                    objArr[2] = "setReturnType";
                    break;
                case 6:
                    objArr[2] = "setModality";
                    break;
                case 8:
                    objArr[2] = "setVisibility";
                    break;
                case 10:
                    objArr[2] = "setKind";
                    break;
                case 12:
                    objArr[2] = "setTypeParameters";
                    break;
                case 15:
                    objArr[2] = "setSubstitution";
                    break;
                case 18:
                    objArr[2] = "setName";
                    break;
                default:
                    objArr[2] = "setOwner";
                    break;
            }
            String format = String.format(str, objArr);
            if (i == 1 || i == 2 || i == 3 || i == 5 || i == 7 || i == 9 || i == 11 || i == 19 || i == 13 || i == 14 || i == 16 || i == 17) {
                throw new IllegalStateException(format);
            }
        }

        public n0 build() {
            q0 q0Var;
            f0 f0Var;
            d0 d0Var;
            e0 e0Var;
            k<g<?>> kVar;
            c0 c0Var = c0.this;
            Objects.requireNonNull(c0Var);
            b.a aVar = b.a.FAKE_OVERRIDE;
            p pVar = null;
            m mVar = this.a;
            z zVar = this.f3235b;
            u uVar = this.c;
            n0 n0Var = this.d;
            b.a aVar2 = this.e;
            e eVar = this.i;
            u0 u0Var = u0.a;
            c0 b2 = c0Var.b(mVar, zVar, uVar, n0Var, aVar2, eVar, u0Var);
            List<z0> typeParameters = c0Var.getTypeParameters();
            ArrayList arrayList = new ArrayList(typeParameters.size());
            c1 substituteTypeParameters = o.substituteTypeParameters(typeParameters, this.f, b2, arrayList);
            d0.e0.p.d.m0.n.c0 c0Var2 = this.j;
            j1 j1Var = j1.OUT_VARIANCE;
            d0.e0.p.d.m0.n.c0 substitute = substituteTypeParameters.substitute(c0Var2, j1Var);
            if (substitute == null) {
                return null;
            }
            q0 q0Var2 = this.h;
            if (q0Var2 != null) {
                q0Var = q0Var2.substitute(substituteTypeParameters);
                if (q0Var == null) {
                    return null;
                }
            } else {
                q0Var = null;
            }
            q0 q0Var3 = c0Var.C;
            if (q0Var3 != null) {
                d0.e0.p.d.m0.n.c0 substitute2 = substituteTypeParameters.substitute(q0Var3.getType(), j1.IN_VARIANCE);
                if (substitute2 == null) {
                    return null;
                }
                f0Var = new f0(b2, new d0.e0.p.d.m0.k.a0.p.b(b2, substitute2, c0Var.C.getValue()), c0Var.C.getAnnotations());
            } else {
                f0Var = null;
            }
            b2.setType(substitute, arrayList, q0Var, f0Var);
            d0 d0Var2 = c0Var.E;
            if (d0Var2 == null) {
                d0Var = null;
            } else {
                d0.e0.p.d.m0.c.g1.g annotations = d0Var2.getAnnotations();
                z zVar2 = this.f3235b;
                u visibility = c0Var.E.getVisibility();
                if (this.e == aVar && t.isPrivate(visibility.normalize())) {
                    visibility = t.h;
                }
                u uVar2 = visibility;
                boolean isDefault = c0Var.E.isDefault();
                boolean isExternal = c0Var.E.isExternal();
                boolean isInline = c0Var.E.isInline();
                b.a aVar3 = this.e;
                n0 n0Var2 = this.d;
                d0Var = new d0(b2, annotations, zVar2, uVar2, isDefault, isExternal, isInline, aVar3, n0Var2 == null ? null : n0Var2.getGetter(), u0Var);
            }
            if (d0Var != null) {
                d0.e0.p.d.m0.n.c0 returnType = c0Var.E.getReturnType();
                d0Var.setInitialSignatureDescriptor(c0.c(substituteTypeParameters, c0Var.E));
                d0Var.initialize(returnType != null ? substituteTypeParameters.substitute(returnType, j1Var) : null);
            }
            p0 p0Var = c0Var.F;
            if (p0Var == null) {
                e0Var = null;
            } else {
                d0.e0.p.d.m0.c.g1.g annotations2 = p0Var.getAnnotations();
                z zVar3 = this.f3235b;
                u visibility2 = c0Var.F.getVisibility();
                if (this.e == aVar && t.isPrivate(visibility2.normalize())) {
                    visibility2 = t.h;
                }
                u uVar3 = visibility2;
                boolean isDefault2 = c0Var.F.isDefault();
                boolean isExternal2 = c0Var.F.isExternal();
                boolean isInline2 = c0Var.F.isInline();
                b.a aVar4 = this.e;
                n0 n0Var3 = this.d;
                e0Var = new e0(b2, annotations2, zVar3, uVar3, isDefault2, isExternal2, isInline2, aVar4, n0Var3 == null ? null : n0Var3.getSetter(), u0Var);
            }
            if (e0Var != null) {
                List<d0.e0.p.d.m0.c.c1> substitutedValueParameters = q.getSubstitutedValueParameters(e0Var, c0Var.F.getValueParameters(), substituteTypeParameters, false, false, null);
                if (substitutedValueParameters == null) {
                    b2.setSetterProjectedOut(true);
                    substitutedValueParameters = Collections.singletonList(e0.createSetterParameter(e0Var, d0.e0.p.d.m0.k.x.a.getBuiltIns(this.a).getNothingType(), c0Var.F.getValueParameters().get(0).getAnnotations()));
                }
                if (substitutedValueParameters.size() == 1) {
                    e0Var.setInitialSignatureDescriptor(c0.c(substituteTypeParameters, c0Var.F));
                    e0Var.initialize(substitutedValueParameters.get(0));
                } else {
                    throw new IllegalStateException();
                }
            }
            v vVar = c0Var.H;
            p pVar2 = vVar == null ? null : new p(vVar.getAnnotations(), b2);
            v vVar2 = c0Var.I;
            if (vVar2 != null) {
                pVar = new p(vVar2.getAnnotations(), b2);
            }
            b2.initialize(d0Var, e0Var, pVar2, pVar);
            if (this.g) {
                j create = j.create();
                for (n0 n0Var4 : c0Var.getOverriddenDescriptors()) {
                    create.add(n0Var4.substitute(substituteTypeParameters));
                }
                b2.setOverriddenDescriptors(create);
            }
            if (c0Var.isConst() && (kVar = c0Var.p) != null) {
                b2.setCompileTimeInitializer(kVar);
            }
            return b2;
        }

        public a setCopyOverrides(boolean z2) {
            this.g = z2;
            return this;
        }

        public a setKind(b.a aVar) {
            if (aVar != null) {
                this.e = aVar;
                return this;
            }
            a(10);
            throw null;
        }

        public a setModality(z zVar) {
            if (zVar != null) {
                this.f3235b = zVar;
                return this;
            }
            a(6);
            throw null;
        }

        public a setOriginal(b bVar) {
            this.d = (n0) bVar;
            return this;
        }

        public a setOwner(m mVar) {
            if (mVar != null) {
                this.a = mVar;
                return this;
            }
            a(0);
            throw null;
        }

        public a setSubstitution(d0.e0.p.d.m0.n.z0 z0Var) {
            if (z0Var != null) {
                this.f = z0Var;
                return this;
            }
            a(15);
            throw null;
        }

        public a setVisibility(u uVar) {
            if (uVar != null) {
                this.c = uVar;
                return this;
            }
            a(8);
            throw null;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public c0(m mVar, n0 n0Var, d0.e0.p.d.m0.c.g1.g gVar, z zVar, u uVar, boolean z2, e eVar, b.a aVar, u0 u0Var, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8) {
        super(mVar, gVar, eVar, null, z2, u0Var);
        if (mVar == null) {
            a(0);
            throw null;
        } else if (gVar == null) {
            a(1);
            throw null;
        } else if (zVar == null) {
            a(2);
            throw null;
        } else if (uVar == null) {
            a(3);
            throw null;
        } else if (eVar == null) {
            a(4);
            throw null;
        } else if (aVar == null) {
            a(5);
            throw null;
        } else if (u0Var != null) {
            this.f3231s = null;
            this.q = zVar;
            this.r = uVar;
            this.t = n0Var == null ? this : n0Var;
            this.u = aVar;
            this.v = z3;
            this.w = z4;
            this.f3232x = z5;
            this.f3233y = z6;
            this.f3234z = z7;
            this.A = z8;
        } else {
            a(6);
            throw null;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:18:0x002a  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x0035  */
    /* JADX WARN: Removed duplicated region for block: B:23:0x003a  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x003f  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0044  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x0049  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x004e  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x0053  */
    /* JADX WARN: Removed duplicated region for block: B:29:0x0058  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x005d  */
    /* JADX WARN: Removed duplicated region for block: B:31:0x0062  */
    /* JADX WARN: Removed duplicated region for block: B:32:0x0067  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x006a  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x006f  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x0074  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x0079  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x007e  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x0083  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x0088  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x008d  */
    /* JADX WARN: Removed duplicated region for block: B:43:0x0094  */
    /* JADX WARN: Removed duplicated region for block: B:58:0x00cf  */
    /* JADX WARN: Removed duplicated region for block: B:60:0x00d6  */
    /* JADX WARN: Removed duplicated region for block: B:61:0x00db  */
    /* JADX WARN: Removed duplicated region for block: B:62:0x00e0  */
    /* JADX WARN: Removed duplicated region for block: B:63:0x00e5  */
    /* JADX WARN: Removed duplicated region for block: B:64:0x00ea  */
    /* JADX WARN: Removed duplicated region for block: B:65:0x00ef  */
    /* JADX WARN: Removed duplicated region for block: B:66:0x00f4  */
    /* JADX WARN: Removed duplicated region for block: B:67:0x00f9  */
    /* JADX WARN: Removed duplicated region for block: B:68:0x00fe  */
    /* JADX WARN: Removed duplicated region for block: B:71:0x0108 A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:76:0x0113  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static /* synthetic */ void a(int r11) {
        /*
            Method dump skipped, instructions count: 488
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.c.i1.c0.a(int):void");
    }

    public static x c(c1 c1Var, m0 m0Var) {
        if (m0Var == null) {
            a(26);
            throw null;
        } else if (m0Var.getInitialSignatureDescriptor() != null) {
            return m0Var.getInitialSignatureDescriptor().substitute(c1Var);
        } else {
            return null;
        }
    }

    public static c0 create(m mVar, d0.e0.p.d.m0.c.g1.g gVar, z zVar, u uVar, boolean z2, e eVar, b.a aVar, u0 u0Var, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8) {
        if (mVar == null) {
            a(7);
            throw null;
        } else if (gVar == null) {
            a(8);
            throw null;
        } else if (zVar == null) {
            a(9);
            throw null;
        } else if (uVar == null) {
            a(10);
            throw null;
        } else if (eVar == null) {
            a(11);
            throw null;
        } else if (aVar == null) {
            a(12);
            throw null;
        } else if (u0Var != null) {
            return new c0(mVar, null, gVar, zVar, uVar, z2, eVar, aVar, u0Var, z3, z4, z5, z6, z7, z8);
        } else {
            a(13);
            throw null;
        }
    }

    @Override // d0.e0.p.d.m0.c.m
    public <R, D> R accept(d0.e0.p.d.m0.c.o<R, D> oVar, D d) {
        return oVar.visitPropertyDescriptor(this, d);
    }

    public c0 b(m mVar, z zVar, u uVar, n0 n0Var, b.a aVar, e eVar, u0 u0Var) {
        if (mVar == null) {
            a(27);
            throw null;
        } else if (zVar == null) {
            a(28);
            throw null;
        } else if (uVar == null) {
            a(29);
            throw null;
        } else if (aVar == null) {
            a(30);
            throw null;
        } else if (eVar != null) {
            return new c0(mVar, n0Var, getAnnotations(), zVar, uVar, isVar(), eVar, aVar, u0Var, isLateInit(), isConst(), isExpect(), isActual(), isExternal(), isDelegated());
        } else {
            a(31);
            throw null;
        }
    }

    @Override // d0.e0.p.d.m0.c.n0
    public List<m0> getAccessors() {
        ArrayList arrayList = new ArrayList(2);
        d0 d0Var = this.E;
        if (d0Var != null) {
            arrayList.add(d0Var);
        }
        p0 p0Var = this.F;
        if (p0Var != null) {
            arrayList.add(p0Var);
        }
        return arrayList;
    }

    @Override // d0.e0.p.d.m0.c.n0
    public v getBackingField() {
        return this.H;
    }

    @Override // d0.e0.p.d.m0.c.n0
    public v getDelegateField() {
        return this.I;
    }

    @Override // d0.e0.p.d.m0.c.i1.m0, d0.e0.p.d.m0.c.a
    public q0 getDispatchReceiverParameter() {
        return this.B;
    }

    @Override // d0.e0.p.d.m0.c.i1.m0, d0.e0.p.d.m0.c.a
    public q0 getExtensionReceiverParameter() {
        return this.C;
    }

    @Override // d0.e0.p.d.m0.c.b
    public b.a getKind() {
        b.a aVar = this.u;
        if (aVar != null) {
            return aVar;
        }
        a(34);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.y
    public z getModality() {
        z zVar = this.q;
        if (zVar != null) {
            return zVar;
        }
        a(19);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.a
    public Collection<? extends n0> getOverriddenDescriptors() {
        Collection<? extends n0> collection = this.f3231s;
        if (collection == null) {
            collection = Collections.emptyList();
        }
        if (collection != null) {
            return collection;
        }
        a(36);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.i1.m0, d0.e0.p.d.m0.c.a
    public d0.e0.p.d.m0.n.c0 getReturnType() {
        d0.e0.p.d.m0.n.c0 type = getType();
        if (type != null) {
            return type;
        }
        a(18);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.n0
    public p0 getSetter() {
        return this.F;
    }

    @Override // d0.e0.p.d.m0.c.i1.m0, d0.e0.p.d.m0.c.a
    public List<z0> getTypeParameters() {
        List<z0> list = this.D;
        if (list != null) {
            return list;
        }
        StringBuilder R = b.d.b.a.a.R("typeParameters == null for ");
        R.append(toString());
        throw new IllegalStateException(R.toString());
    }

    @Override // d0.e0.p.d.m0.c.q, d0.e0.p.d.m0.c.y
    public u getVisibility() {
        u uVar = this.r;
        if (uVar != null) {
            return uVar;
        }
        a(20);
        throw null;
    }

    public void initialize(d0 d0Var, p0 p0Var) {
        initialize(d0Var, p0Var, null, null);
    }

    @Override // d0.e0.p.d.m0.c.y
    public boolean isActual() {
        return this.f3233y;
    }

    @Override // d0.e0.p.d.m0.c.d1
    public boolean isConst() {
        return this.w;
    }

    @Override // d0.e0.p.d.m0.c.n0
    public boolean isDelegated() {
        return this.A;
    }

    @Override // d0.e0.p.d.m0.c.y
    public boolean isExpect() {
        return this.f3232x;
    }

    @Override // d0.e0.p.d.m0.c.y
    public boolean isExternal() {
        return this.f3234z;
    }

    @Override // d0.e0.p.d.m0.c.d1
    public boolean isLateInit() {
        return this.v;
    }

    public boolean isSetterProjectedOut() {
        return this.G;
    }

    public a newCopyBuilder() {
        return new a();
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // d0.e0.p.d.m0.c.b
    public void setOverriddenDescriptors(Collection<? extends b> collection) {
        if (collection != 0) {
            this.f3231s = collection;
        } else {
            a(35);
            throw null;
        }
    }

    public void setSetterProjectedOut(boolean z2) {
        this.G = z2;
    }

    public void setType(d0.e0.p.d.m0.n.c0 c0Var, List<? extends z0> list, q0 q0Var, q0 q0Var2) {
        if (c0Var == null) {
            a(14);
            throw null;
        } else if (list != null) {
            setOutType(c0Var);
            this.D = new ArrayList(list);
            this.C = q0Var2;
            this.B = q0Var;
        } else {
            a(15);
            throw null;
        }
    }

    public void setVisibility(u uVar) {
        if (uVar != null) {
            this.r = uVar;
        } else {
            a(16);
            throw null;
        }
    }

    @Override // d0.e0.p.d.m0.c.b
    public n0 copy(m mVar, z zVar, u uVar, b.a aVar, boolean z2) {
        n0 build = newCopyBuilder().setOwner(mVar).setOriginal(null).setModality(zVar).setVisibility(uVar).setKind(aVar).setCopyOverrides(z2).build();
        if (build != null) {
            return build;
        }
        a(37);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.n0
    public d0 getGetter() {
        return this.E;
    }

    public void initialize(d0 d0Var, p0 p0Var, v vVar, v vVar2) {
        this.E = d0Var;
        this.F = p0Var;
        this.H = vVar;
        this.I = vVar2;
    }

    @Override // d0.e0.p.d.m0.c.w0
    public n0 substitute(c1 c1Var) {
        if (c1Var != null) {
            return c1Var.isEmpty() ? this : newCopyBuilder().setSubstitution(c1Var.getSubstitution()).setOriginal(getOriginal()).build();
        }
        a(22);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.i1.l, d0.e0.p.d.m0.c.i1.k, d0.e0.p.d.m0.c.m
    public n0 getOriginal() {
        n0 n0Var = this.t;
        n0 original = n0Var == this ? this : n0Var.getOriginal();
        if (original != null) {
            return original;
        }
        a(33);
        throw null;
    }
}
