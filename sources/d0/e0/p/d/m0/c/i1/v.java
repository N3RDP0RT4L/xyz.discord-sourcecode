package d0.e0.p.d.m0.c.i1;

import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.n.l1.g;
import d0.e0.p.d.m0.n.z0;
import d0.z.d.m;
/* compiled from: ModuleAwareClassDescriptor.kt */
/* loaded from: classes3.dex */
public final class v {
    public static final i getRefinedMemberScopeIfPossible(e eVar, z0 z0Var, g gVar) {
        m.checkNotNullParameter(eVar, "<this>");
        m.checkNotNullParameter(z0Var, "typeSubstitution");
        m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
        return u.j.getRefinedMemberScopeIfPossible$descriptors(eVar, z0Var, gVar);
    }

    public static final i getRefinedUnsubstitutedMemberScopeIfPossible(e eVar, g gVar) {
        m.checkNotNullParameter(eVar, "<this>");
        m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
        return u.j.getRefinedUnsubstitutedMemberScopeIfPossible$descriptors(eVar, gVar);
    }
}
