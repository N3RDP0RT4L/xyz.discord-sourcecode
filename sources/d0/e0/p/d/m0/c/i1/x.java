package d0.e0.p.d.m0.c.i1;

import d0.z.d.m;
import java.util.List;
import java.util.Set;
/* compiled from: ModuleDescriptorImpl.kt */
/* loaded from: classes3.dex */
public final class x implements w {
    public final List<y> a;

    /* renamed from: b  reason: collision with root package name */
    public final Set<y> f3249b;
    public final List<y> c;

    public x(List<y> list, Set<y> set, List<y> list2, Set<y> set2) {
        m.checkNotNullParameter(list, "allDependencies");
        m.checkNotNullParameter(set, "modulesWhoseInternalsAreVisible");
        m.checkNotNullParameter(list2, "directExpectedByDependencies");
        m.checkNotNullParameter(set2, "allExpectedByDependencies");
        this.a = list;
        this.f3249b = set;
        this.c = list2;
    }

    @Override // d0.e0.p.d.m0.c.i1.w
    public List<y> getAllDependencies() {
        return this.a;
    }

    @Override // d0.e0.p.d.m0.c.i1.w
    public List<y> getDirectExpectedByDependencies() {
        return this.c;
    }

    @Override // d0.e0.p.d.m0.c.i1.w
    public Set<y> getModulesWhoseInternalsAreVisible() {
        return this.f3249b;
    }
}
