package d0.e0.p.d.m0.c.i1;

import andhook.lib.HookHelper;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.k.a0.p.d;
/* compiled from: ReceiverParameterDescriptorImpl.java */
/* loaded from: classes3.dex */
public class f0 extends c {
    public final m m;
    public final d n;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public f0(m mVar, d dVar, g gVar) {
        super(gVar);
        if (mVar == null) {
            a(0);
            throw null;
        } else if (dVar == null) {
            a(1);
            throw null;
        } else if (gVar != null) {
            this.m = mVar;
            this.n = dVar;
        } else {
            a(2);
            throw null;
        }
    }

    public static /* synthetic */ void a(int i) {
        String str = (i == 3 || i == 4) ? "@NotNull method %s.%s must not return null" : "Argument for @NotNull parameter '%s' of %s.%s must not be null";
        Object[] objArr = new Object[(i == 3 || i == 4) ? 2 : 3];
        if (i == 1) {
            objArr[0] = "value";
        } else if (i == 2) {
            objArr[0] = "annotations";
        } else if (i == 3 || i == 4) {
            objArr[0] = "kotlin/reflect/jvm/internal/impl/descriptors/impl/ReceiverParameterDescriptorImpl";
        } else if (i != 5) {
            objArr[0] = "containingDeclaration";
        } else {
            objArr[0] = "newOwner";
        }
        if (i == 3) {
            objArr[1] = "getValue";
        } else if (i != 4) {
            objArr[1] = "kotlin/reflect/jvm/internal/impl/descriptors/impl/ReceiverParameterDescriptorImpl";
        } else {
            objArr[1] = "getContainingDeclaration";
        }
        if (!(i == 3 || i == 4)) {
            if (i != 5) {
                objArr[2] = HookHelper.constructorName;
            } else {
                objArr[2] = "copy";
            }
        }
        String format = String.format(str, objArr);
        if (i == 3 || i == 4) {
            throw new IllegalStateException(format);
        }
    }

    @Override // d0.e0.p.d.m0.c.m
    public m getContainingDeclaration() {
        m mVar = this.m;
        if (mVar != null) {
            return mVar;
        }
        a(4);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.q0
    public d getValue() {
        d dVar = this.n;
        if (dVar != null) {
            return dVar;
        }
        a(3);
        throw null;
    }
}
