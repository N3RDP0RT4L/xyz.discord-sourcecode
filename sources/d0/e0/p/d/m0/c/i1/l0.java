package d0.e0.p.d.m0.c.i1;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.d1;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.t;
import d0.e0.p.d.m0.c.u;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.n.c0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Lazy;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ValueParameterDescriptorImpl.kt */
/* loaded from: classes3.dex */
public class l0 extends m0 implements c1 {
    public static final a o = new a(null);
    public final int p;
    public final boolean q;
    public final boolean r;

    /* renamed from: s  reason: collision with root package name */
    public final boolean f3240s;
    public final c0 t;
    public final c1 u;

    /* compiled from: ValueParameterDescriptorImpl.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final l0 createWithDestructuringDeclarations(d0.e0.p.d.m0.c.a aVar, c1 c1Var, int i, g gVar, e eVar, c0 c0Var, boolean z2, boolean z3, boolean z4, c0 c0Var2, u0 u0Var, Function0<? extends List<? extends d1>> function0) {
            m.checkNotNullParameter(aVar, "containingDeclaration");
            m.checkNotNullParameter(gVar, "annotations");
            m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkNotNullParameter(c0Var, "outType");
            m.checkNotNullParameter(u0Var, "source");
            if (function0 == null) {
                return new l0(aVar, c1Var, i, gVar, eVar, c0Var, z2, z3, z4, c0Var2, u0Var);
            }
            return new b(aVar, c1Var, i, gVar, eVar, c0Var, z2, z3, z4, c0Var2, u0Var, function0);
        }
    }

    /* compiled from: ValueParameterDescriptorImpl.kt */
    /* loaded from: classes3.dex */
    public static final class b extends l0 {
        public final Lazy v;

        /* compiled from: ValueParameterDescriptorImpl.kt */
        /* loaded from: classes3.dex */
        public static final class a extends o implements Function0<List<? extends d1>> {
            public a() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            public final List<? extends d1> invoke() {
                return b.this.getDestructuringVariables();
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(d0.e0.p.d.m0.c.a aVar, c1 c1Var, int i, g gVar, e eVar, c0 c0Var, boolean z2, boolean z3, boolean z4, c0 c0Var2, u0 u0Var, Function0<? extends List<? extends d1>> function0) {
            super(aVar, c1Var, i, gVar, eVar, c0Var, z2, z3, z4, c0Var2, u0Var);
            m.checkNotNullParameter(aVar, "containingDeclaration");
            m.checkNotNullParameter(gVar, "annotations");
            m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkNotNullParameter(c0Var, "outType");
            m.checkNotNullParameter(u0Var, "source");
            m.checkNotNullParameter(function0, "destructuringVariables");
            this.v = d0.g.lazy(function0);
        }

        @Override // d0.e0.p.d.m0.c.i1.l0, d0.e0.p.d.m0.c.c1
        public c1 copy(d0.e0.p.d.m0.c.a aVar, e eVar, int i) {
            m.checkNotNullParameter(aVar, "newOwner");
            m.checkNotNullParameter(eVar, "newName");
            g annotations = getAnnotations();
            m.checkNotNullExpressionValue(annotations, "annotations");
            c0 type = getType();
            m.checkNotNullExpressionValue(type, "type");
            boolean declaresDefaultValue = declaresDefaultValue();
            boolean isCrossinline = isCrossinline();
            boolean isNoinline = isNoinline();
            c0 varargElementType = getVarargElementType();
            u0 u0Var = u0.a;
            m.checkNotNullExpressionValue(u0Var, "NO_SOURCE");
            return new b(aVar, null, i, annotations, eVar, type, declaresDefaultValue, isCrossinline, isNoinline, varargElementType, u0Var, new a());
        }

        public final List<d1> getDestructuringVariables() {
            return (List) this.v.getValue();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public l0(d0.e0.p.d.m0.c.a aVar, c1 c1Var, int i, g gVar, e eVar, c0 c0Var, boolean z2, boolean z3, boolean z4, c0 c0Var2, u0 u0Var) {
        super(aVar, gVar, eVar, c0Var, u0Var);
        m.checkNotNullParameter(aVar, "containingDeclaration");
        m.checkNotNullParameter(gVar, "annotations");
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(c0Var, "outType");
        m.checkNotNullParameter(u0Var, "source");
        this.p = i;
        this.q = z2;
        this.r = z3;
        this.f3240s = z4;
        this.t = c0Var2;
        this.u = c1Var == null ? this : c1Var;
    }

    public static final l0 createWithDestructuringDeclarations(d0.e0.p.d.m0.c.a aVar, c1 c1Var, int i, g gVar, e eVar, c0 c0Var, boolean z2, boolean z3, boolean z4, c0 c0Var2, u0 u0Var, Function0<? extends List<? extends d1>> function0) {
        return o.createWithDestructuringDeclarations(aVar, c1Var, i, gVar, eVar, c0Var, z2, z3, z4, c0Var2, u0Var, function0);
    }

    @Override // d0.e0.p.d.m0.c.m
    public <R, D> R accept(d0.e0.p.d.m0.c.o<R, D> oVar, D d) {
        m.checkNotNullParameter(oVar, "visitor");
        return oVar.visitValueParameterDescriptor(this, d);
    }

    @Override // d0.e0.p.d.m0.c.c1
    public c1 copy(d0.e0.p.d.m0.c.a aVar, e eVar, int i) {
        m.checkNotNullParameter(aVar, "newOwner");
        m.checkNotNullParameter(eVar, "newName");
        g annotations = getAnnotations();
        m.checkNotNullExpressionValue(annotations, "annotations");
        c0 type = getType();
        m.checkNotNullExpressionValue(type, "type");
        boolean declaresDefaultValue = declaresDefaultValue();
        boolean isCrossinline = isCrossinline();
        boolean isNoinline = isNoinline();
        c0 varargElementType = getVarargElementType();
        u0 u0Var = u0.a;
        m.checkNotNullExpressionValue(u0Var, "NO_SOURCE");
        return new l0(aVar, null, i, annotations, eVar, type, declaresDefaultValue, isCrossinline, isNoinline, varargElementType, u0Var);
    }

    @Override // d0.e0.p.d.m0.c.c1
    public boolean declaresDefaultValue() {
        return this.q && ((d0.e0.p.d.m0.c.b) getContainingDeclaration()).getKind().isReal();
    }

    @Override // d0.e0.p.d.m0.c.d1
    public Void getCompileTimeInitializer() {
        return null;
    }

    @Override // d0.e0.p.d.m0.c.c1
    public int getIndex() {
        return this.p;
    }

    @Override // d0.e0.p.d.m0.c.a
    public Collection<c1> getOverriddenDescriptors() {
        Collection<? extends d0.e0.p.d.m0.c.a> overriddenDescriptors = getContainingDeclaration().getOverriddenDescriptors();
        m.checkNotNullExpressionValue(overriddenDescriptors, "containingDeclaration.overriddenDescriptors");
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(overriddenDescriptors, 10));
        for (d0.e0.p.d.m0.c.a aVar : overriddenDescriptors) {
            arrayList.add(aVar.getValueParameters().get(getIndex()));
        }
        return arrayList;
    }

    @Override // d0.e0.p.d.m0.c.c1
    public c0 getVarargElementType() {
        return this.t;
    }

    @Override // d0.e0.p.d.m0.c.q, d0.e0.p.d.m0.c.y
    public u getVisibility() {
        u uVar = t.f;
        m.checkNotNullExpressionValue(uVar, "LOCAL");
        return uVar;
    }

    @Override // d0.e0.p.d.m0.c.c1
    public boolean isCrossinline() {
        return this.r;
    }

    @Override // d0.e0.p.d.m0.c.c1
    public boolean isNoinline() {
        return this.f3240s;
    }

    @Override // d0.e0.p.d.m0.c.d1
    public boolean isVar() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.i1.l, d0.e0.p.d.m0.c.m
    public d0.e0.p.d.m0.c.a getContainingDeclaration() {
        return (d0.e0.p.d.m0.c.a) super.getContainingDeclaration();
    }

    @Override // d0.e0.p.d.m0.c.w0
    public c1 substitute(d0.e0.p.d.m0.n.c1 c1Var) {
        m.checkNotNullParameter(c1Var, "substitutor");
        if (c1Var.isEmpty()) {
            return this;
        }
        throw new UnsupportedOperationException();
    }

    @Override // d0.e0.p.d.m0.c.i1.l, d0.e0.p.d.m0.c.i1.k, d0.e0.p.d.m0.c.m
    public c1 getOriginal() {
        c1 c1Var = this.u;
        return c1Var == this ? this : c1Var.getOriginal();
    }
}
