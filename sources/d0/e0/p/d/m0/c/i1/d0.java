package d0.e0.p.d.m0.c.i1;

import andhook.lib.HookHelper;
import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.o;
import d0.e0.p.d.m0.c.o0;
import d0.e0.p.d.m0.n.c0;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
/* compiled from: PropertyGetterDescriptorImpl.java */
/* loaded from: classes3.dex */
public class d0 extends b0 implements o0 {
    public c0 v;
    public final o0 w;

    /* JADX WARN: Illegal instructions before constructor call */
    /* JADX WARN: Multi-variable type inference failed */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public d0(d0.e0.p.d.m0.c.n0 r13, d0.e0.p.d.m0.c.g1.g r14, d0.e0.p.d.m0.c.z r15, d0.e0.p.d.m0.c.u r16, boolean r17, boolean r18, boolean r19, d0.e0.p.d.m0.c.b.a r20, d0.e0.p.d.m0.c.o0 r21, d0.e0.p.d.m0.c.u0 r22) {
        /*
            r12 = this;
            r0 = 0
            if (r13 == 0) goto L63
            if (r14 == 0) goto L5d
            if (r15 == 0) goto L57
            if (r16 == 0) goto L51
            if (r20 == 0) goto L4b
            if (r22 == 0) goto L45
            java.lang.String r0 = "<get-"
            java.lang.StringBuilder r0 = b.d.b.a.a.R(r0)
            d0.e0.p.d.m0.g.e r1 = r13.getName()
            r0.append(r1)
            java.lang.String r1 = ">"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            d0.e0.p.d.m0.g.e r6 = d0.e0.p.d.m0.g.e.special(r0)
            r1 = r12
            r2 = r15
            r3 = r16
            r4 = r13
            r5 = r14
            r7 = r17
            r8 = r18
            r9 = r19
            r10 = r20
            r11 = r22
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11)
            if (r21 == 0) goto L40
            r1 = r12
            r0 = r21
            goto L42
        L40:
            r0 = r12
            r1 = r0
        L42:
            r1.w = r0
            return
        L45:
            r1 = r12
            r2 = 5
            a(r2)
            throw r0
        L4b:
            r1 = r12
            r2 = 4
            a(r2)
            throw r0
        L51:
            r1 = r12
            r2 = 3
            a(r2)
            throw r0
        L57:
            r1 = r12
            r2 = 2
            a(r2)
            throw r0
        L5d:
            r1 = r12
            r2 = 1
            a(r2)
            throw r0
        L63:
            r1 = r12
            r2 = 0
            a(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.c.i1.d0.<init>(d0.e0.p.d.m0.c.n0, d0.e0.p.d.m0.c.g1.g, d0.e0.p.d.m0.c.z, d0.e0.p.d.m0.c.u, boolean, boolean, boolean, d0.e0.p.d.m0.c.b$a, d0.e0.p.d.m0.c.o0, d0.e0.p.d.m0.c.u0):void");
    }

    public static /* synthetic */ void a(int i) {
        String str = (i == 6 || i == 7 || i == 8) ? "@NotNull method %s.%s must not return null" : "Argument for @NotNull parameter '%s' of %s.%s must not be null";
        Object[] objArr = new Object[(i == 6 || i == 7 || i == 8) ? 2 : 3];
        switch (i) {
            case 1:
                objArr[0] = "annotations";
                break;
            case 2:
                objArr[0] = "modality";
                break;
            case 3:
                objArr[0] = "visibility";
                break;
            case 4:
                objArr[0] = "kind";
                break;
            case 5:
                objArr[0] = "source";
                break;
            case 6:
            case 7:
            case 8:
                objArr[0] = "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyGetterDescriptorImpl";
                break;
            default:
                objArr[0] = "correspondingProperty";
                break;
        }
        if (i == 6) {
            objArr[1] = "getOverriddenDescriptors";
        } else if (i == 7) {
            objArr[1] = "getValueParameters";
        } else if (i != 8) {
            objArr[1] = "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyGetterDescriptorImpl";
        } else {
            objArr[1] = "getOriginal";
        }
        if (!(i == 6 || i == 7 || i == 8)) {
            objArr[2] = HookHelper.constructorName;
        }
        String format = String.format(str, objArr);
        if (i == 6 || i == 7 || i == 8) {
            throw new IllegalStateException(format);
        }
    }

    @Override // d0.e0.p.d.m0.c.m
    public <R, D> R accept(o<R, D> oVar, D d) {
        return oVar.visitPropertyGetterDescriptor(this, d);
    }

    @Override // d0.e0.p.d.m0.c.x, d0.e0.p.d.m0.c.b, d0.e0.p.d.m0.c.a
    public Collection<? extends o0> getOverriddenDescriptors() {
        return b(true);
    }

    @Override // d0.e0.p.d.m0.c.a
    public c0 getReturnType() {
        return this.v;
    }

    @Override // d0.e0.p.d.m0.c.a
    public List<c1> getValueParameters() {
        List<c1> emptyList = Collections.emptyList();
        if (emptyList != null) {
            return emptyList;
        }
        a(7);
        throw null;
    }

    public void initialize(c0 c0Var) {
        if (c0Var == null) {
            c0Var = getCorrespondingProperty().getType();
        }
        this.v = c0Var;
    }

    @Override // d0.e0.p.d.m0.c.i1.b0, d0.e0.p.d.m0.c.i1.l, d0.e0.p.d.m0.c.i1.k, d0.e0.p.d.m0.c.m
    public o0 getOriginal() {
        o0 o0Var = this.w;
        if (o0Var != null) {
            return o0Var;
        }
        a(8);
        throw null;
    }
}
