package d0.e0.p.d.m0.c.i1;

import andhook.lib.HookHelper;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.q0;
import d0.e0.p.d.m0.c.y0;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.k.a0.n;
import d0.e0.p.d.m0.m.j;
import d0.e0.p.d.m0.m.o;
import d0.e0.p.d.m0.n.c1;
import d0.e0.p.d.m0.n.d0;
import d0.e0.p.d.m0.n.e1;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.l1.g;
import d0.e0.p.d.m0.n.z0;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: AbstractClassDescriptor.java */
/* loaded from: classes3.dex */
public abstract class a extends u {
    public final e k;
    public final j<j0> l;
    public final j<i> m;
    public final j<q0> n;

    /* compiled from: AbstractClassDescriptor.java */
    /* renamed from: d0.e0.p.d.m0.c.i1.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public class C0293a implements Function0<j0> {

        /* compiled from: AbstractClassDescriptor.java */
        /* renamed from: d0.e0.p.d.m0.c.i1.a$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public class C0294a implements Function1<g, j0> {
            public C0294a() {
            }

            public j0 invoke(g gVar) {
                h refineDescriptor = gVar.refineDescriptor(a.this);
                if (refineDescriptor == null) {
                    return a.this.l.invoke();
                }
                if (refineDescriptor instanceof y0) {
                    return d0.computeExpandedType((y0) refineDescriptor, e1.getDefaultTypeProjections(refineDescriptor.getTypeConstructor().getParameters()));
                }
                if (refineDescriptor instanceof u) {
                    return e1.makeUnsubstitutedType(refineDescriptor.getTypeConstructor().refine(gVar), ((u) refineDescriptor).getUnsubstitutedMemberScope(gVar), this);
                }
                return refineDescriptor.getDefaultType();
            }
        }

        public C0293a() {
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public j0 invoke() {
            a aVar = a.this;
            return e1.makeUnsubstitutedType(aVar, aVar.getUnsubstitutedMemberScope(), new C0294a());
        }
    }

    /* compiled from: AbstractClassDescriptor.java */
    /* loaded from: classes3.dex */
    public class b implements Function0<i> {
        public b() {
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public i invoke() {
            return new d0.e0.p.d.m0.k.a0.g(a.this.getUnsubstitutedMemberScope());
        }
    }

    /* compiled from: AbstractClassDescriptor.java */
    /* loaded from: classes3.dex */
    public class c implements Function0<q0> {
        public c() {
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public q0 invoke() {
            return new r(a.this);
        }
    }

    public a(o oVar, e eVar) {
        if (oVar == null) {
            a(0);
            throw null;
        } else if (eVar != null) {
            this.k = eVar;
            this.l = oVar.createLazyValue(new C0293a());
            this.m = oVar.createLazyValue(new b());
            this.n = oVar.createLazyValue(new c());
        } else {
            a(1);
            throw null;
        }
    }

    public static /* synthetic */ void a(int i) {
        String str = (i == 2 || i == 3 || i == 4 || i == 5 || i == 8 || i == 11 || i == 13 || i == 15 || i == 16 || i == 18 || i == 19) ? "@NotNull method %s.%s must not return null" : "Argument for @NotNull parameter '%s' of %s.%s must not be null";
        Object[] objArr = new Object[(i == 2 || i == 3 || i == 4 || i == 5 || i == 8 || i == 11 || i == 13 || i == 15 || i == 16 || i == 18 || i == 19) ? 2 : 3];
        switch (i) {
            case 1:
                objArr[0] = ModelAuditLogEntry.CHANGE_KEY_NAME;
                break;
            case 2:
            case 3:
            case 4:
            case 5:
            case 8:
            case 11:
            case 13:
            case 15:
            case 16:
            case 18:
            case 19:
                objArr[0] = "kotlin/reflect/jvm/internal/impl/descriptors/impl/AbstractClassDescriptor";
                break;
            case 6:
            case 12:
                objArr[0] = "typeArguments";
                break;
            case 7:
            case 10:
                objArr[0] = "kotlinTypeRefiner";
                break;
            case 9:
            case 14:
                objArr[0] = "typeSubstitution";
                break;
            case 17:
                objArr[0] = "substitutor";
                break;
            default:
                objArr[0] = "storageManager";
                break;
        }
        if (i == 2) {
            objArr[1] = "getName";
        } else if (i == 3) {
            objArr[1] = "getOriginal";
        } else if (i == 4) {
            objArr[1] = "getUnsubstitutedInnerClassesScope";
        } else if (i == 5) {
            objArr[1] = "getThisAsReceiverParameter";
        } else if (i == 8 || i == 11 || i == 13 || i == 15) {
            objArr[1] = "getMemberScope";
        } else if (i == 16) {
            objArr[1] = "getUnsubstitutedMemberScope";
        } else if (i == 18) {
            objArr[1] = "substitute";
        } else if (i != 19) {
            objArr[1] = "kotlin/reflect/jvm/internal/impl/descriptors/impl/AbstractClassDescriptor";
        } else {
            objArr[1] = "getDefaultType";
        }
        switch (i) {
            case 2:
            case 3:
            case 4:
            case 5:
            case 8:
            case 11:
            case 13:
            case 15:
            case 16:
            case 18:
            case 19:
                break;
            case 6:
            case 7:
            case 9:
            case 10:
            case 12:
            case 14:
                objArr[2] = "getMemberScope";
                break;
            case 17:
                objArr[2] = "substitute";
                break;
            default:
                objArr[2] = HookHelper.constructorName;
                break;
        }
        String format = String.format(str, objArr);
        if (i == 2 || i == 3 || i == 4 || i == 5 || i == 8 || i == 11 || i == 13 || i == 15 || i == 16 || i == 18 || i == 19) {
            throw new IllegalStateException(format);
        }
    }

    @Override // d0.e0.p.d.m0.c.m
    public <R, D> R accept(d0.e0.p.d.m0.c.o<R, D> oVar, D d) {
        return oVar.visitClassDescriptor(this, d);
    }

    @Override // d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.h
    public j0 getDefaultType() {
        j0 invoke = this.l.invoke();
        if (invoke != null) {
            return invoke;
        }
        a(19);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.i1.u
    public i getMemberScope(z0 z0Var, g gVar) {
        if (z0Var == null) {
            a(9);
            throw null;
        } else if (gVar == null) {
            a(10);
            throw null;
        } else if (z0Var.isEmpty()) {
            i unsubstitutedMemberScope = getUnsubstitutedMemberScope(gVar);
            if (unsubstitutedMemberScope != null) {
                return unsubstitutedMemberScope;
            }
            a(11);
            throw null;
        } else {
            return new n(getUnsubstitutedMemberScope(gVar), c1.create(z0Var));
        }
    }

    @Override // d0.e0.p.d.m0.c.m
    public e getName() {
        e eVar = this.k;
        if (eVar != null) {
            return eVar;
        }
        a(2);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.m
    public d0.e0.p.d.m0.c.e getOriginal() {
        return this;
    }

    @Override // d0.e0.p.d.m0.c.e
    public q0 getThisAsReceiverParameter() {
        q0 invoke = this.n.invoke();
        if (invoke != null) {
            return invoke;
        }
        a(5);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.e
    public i getUnsubstitutedInnerClassesScope() {
        i invoke = this.m.invoke();
        if (invoke != null) {
            return invoke;
        }
        a(4);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.e
    public i getUnsubstitutedMemberScope() {
        i unsubstitutedMemberScope = getUnsubstitutedMemberScope(d0.e0.p.d.m0.k.x.a.getKotlinTypeRefiner(d0.e0.p.d.m0.k.e.getContainingModule(this)));
        if (unsubstitutedMemberScope != null) {
            return unsubstitutedMemberScope;
        }
        a(16);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.w0
    public d0.e0.p.d.m0.c.e substitute(c1 c1Var) {
        if (c1Var != null) {
            return c1Var.isEmpty() ? this : new t(this, c1Var);
        }
        a(17);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.e
    public i getMemberScope(z0 z0Var) {
        if (z0Var != null) {
            i memberScope = getMemberScope(z0Var, d0.e0.p.d.m0.k.x.a.getKotlinTypeRefiner(d0.e0.p.d.m0.k.e.getContainingModule(this)));
            if (memberScope != null) {
                return memberScope;
            }
            a(15);
            throw null;
        }
        a(14);
        throw null;
    }
}
