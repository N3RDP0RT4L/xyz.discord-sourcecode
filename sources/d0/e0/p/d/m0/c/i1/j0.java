package d0.e0.p.d.m0.c.i1;

import andhook.lib.HookHelper;
import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.d;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.q0;
import d0.e0.p.d.m0.c.u;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.c.x;
import d0.e0.p.d.m0.c.y0;
import d0.e0.p.d.m0.c.z;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.m.o;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.c1;
import d0.e0.p.d.m0.n.j1;
import d0.e0.p.d.m0.n.m0;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.y;
import java.util.List;
import java.util.Objects;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
/* compiled from: TypeAliasConstructorDescriptor.kt */
/* loaded from: classes3.dex */
public final class j0 extends q implements i0 {
    public static final a M = new a(null);
    public static final /* synthetic */ KProperty<Object>[] N = {a0.property1(new y(a0.getOrCreateKotlinClass(j0.class), "withDispatchReceiver", "getWithDispatchReceiver()Lorg/jetbrains/kotlin/descriptors/impl/TypeAliasConstructorDescriptor;"))};
    public final o O;
    public final y0 P;
    public d Q;

    /* compiled from: TypeAliasConstructorDescriptor.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public static final c1 access$getTypeSubstitutorForUnderlyingClass(a aVar, y0 y0Var) {
            Objects.requireNonNull(aVar);
            if (y0Var.getClassDescriptor() == null) {
                return null;
            }
            return c1.create(y0Var.getExpandedType());
        }

        public final i0 createIfAvailable(o oVar, y0 y0Var, d dVar) {
            d substitute;
            m.checkNotNullParameter(oVar, "storageManager");
            m.checkNotNullParameter(y0Var, "typeAliasDescriptor");
            m.checkNotNullParameter(dVar, "constructor");
            q0 q0Var = null;
            c1 create = y0Var.getClassDescriptor() == null ? null : c1.create(y0Var.getExpandedType());
            if (create == null || (substitute = dVar.substitute(create)) == null) {
                return null;
            }
            g annotations = dVar.getAnnotations();
            b.a kind = dVar.getKind();
            m.checkNotNullExpressionValue(kind, "constructor.kind");
            u0 source = y0Var.getSource();
            m.checkNotNullExpressionValue(source, "typeAliasDescriptor.source");
            j0 j0Var = new j0(oVar, y0Var, substitute, null, annotations, kind, source, null);
            List<d0.e0.p.d.m0.c.c1> substitutedValueParameters = q.getSubstitutedValueParameters(j0Var, dVar.getValueParameters(), create);
            if (substitutedValueParameters == null) {
                return null;
            }
            d0.e0.p.d.m0.n.j0 lowerIfFlexible = d0.e0.p.d.m0.n.y.lowerIfFlexible(substitute.getReturnType().unwrap());
            d0.e0.p.d.m0.n.j0 defaultType = y0Var.getDefaultType();
            m.checkNotNullExpressionValue(defaultType, "typeAliasDescriptor.defaultType");
            d0.e0.p.d.m0.n.j0 withAbbreviation = m0.withAbbreviation(lowerIfFlexible, defaultType);
            q0 dispatchReceiverParameter = dVar.getDispatchReceiverParameter();
            if (dispatchReceiverParameter != null) {
                q0Var = d0.e0.p.d.m0.k.d.createExtensionReceiverParameterForCallable(j0Var, create.safeSubstitute(dispatchReceiverParameter.getType(), j1.INVARIANT), g.f.getEMPTY());
            }
            j0Var.initialize(q0Var, null, y0Var.getDeclaredTypeParameters(), substitutedValueParameters, withAbbreviation, z.FINAL, y0Var.getVisibility());
            return j0Var;
        }
    }

    /* compiled from: TypeAliasConstructorDescriptor.kt */
    /* loaded from: classes3.dex */
    public static final class b extends d0.z.d.o implements Function0<j0> {
        public final /* synthetic */ d $underlyingConstructorDescriptor;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(d dVar) {
            super(0);
            this.$underlyingConstructorDescriptor = dVar;
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final j0 invoke() {
            o storageManager = j0.this.getStorageManager();
            y0 typeAliasDescriptor = j0.this.getTypeAliasDescriptor();
            d dVar = this.$underlyingConstructorDescriptor;
            j0 j0Var = j0.this;
            g annotations = dVar.getAnnotations();
            b.a kind = this.$underlyingConstructorDescriptor.getKind();
            m.checkNotNullExpressionValue(kind, "underlyingConstructorDescriptor.kind");
            u0 source = j0.this.getTypeAliasDescriptor().getSource();
            m.checkNotNullExpressionValue(source, "typeAliasDescriptor.source");
            j0 j0Var2 = new j0(storageManager, typeAliasDescriptor, dVar, j0Var, annotations, kind, source, null);
            j0 j0Var3 = j0.this;
            d dVar2 = this.$underlyingConstructorDescriptor;
            c1 access$getTypeSubstitutorForUnderlyingClass = a.access$getTypeSubstitutorForUnderlyingClass(j0.M, j0Var3.getTypeAliasDescriptor());
            if (access$getTypeSubstitutorForUnderlyingClass == null) {
                return null;
            }
            q0 dispatchReceiverParameter = dVar2.getDispatchReceiverParameter();
            j0Var2.initialize(null, dispatchReceiverParameter == null ? null : dispatchReceiverParameter.substitute(access$getTypeSubstitutorForUnderlyingClass), j0Var3.getTypeAliasDescriptor().getDeclaredTypeParameters(), j0Var3.getValueParameters(), j0Var3.getReturnType(), z.FINAL, j0Var3.getTypeAliasDescriptor().getVisibility());
            return j0Var2;
        }
    }

    public j0(o oVar, y0 y0Var, d dVar, i0 i0Var, g gVar, b.a aVar, u0 u0Var) {
        super(y0Var, i0Var, gVar, e.special(HookHelper.constructorName), aVar, u0Var);
        this.O = oVar;
        this.P = y0Var;
        setActual(getTypeAliasDescriptor().isActual());
        oVar.createNullableLazyValue(new b(dVar));
        this.Q = dVar;
    }

    public /* synthetic */ j0(o oVar, y0 y0Var, d dVar, i0 i0Var, g gVar, b.a aVar, u0 u0Var, DefaultConstructorMarker defaultConstructorMarker) {
        this(oVar, y0Var, dVar, i0Var, gVar, aVar, u0Var);
    }

    @Override // d0.e0.p.d.m0.c.i1.q
    public q b(d0.e0.p.d.m0.c.m mVar, x xVar, b.a aVar, e eVar, g gVar, u0 u0Var) {
        m.checkNotNullParameter(mVar, "newOwner");
        m.checkNotNullParameter(aVar, "kind");
        m.checkNotNullParameter(gVar, "annotations");
        m.checkNotNullParameter(u0Var, "source");
        return new j0(this.O, getTypeAliasDescriptor(), getUnderlyingConstructorDescriptor(), this, gVar, b.a.DECLARATION, u0Var);
    }

    @Override // d0.e0.p.d.m0.c.l
    public d0.e0.p.d.m0.c.e getConstructedClass() {
        d0.e0.p.d.m0.c.e constructedClass = getUnderlyingConstructorDescriptor().getConstructedClass();
        m.checkNotNullExpressionValue(constructedClass, "underlyingConstructorDescriptor.constructedClass");
        return constructedClass;
    }

    @Override // d0.e0.p.d.m0.c.i1.q, d0.e0.p.d.m0.c.a
    public c0 getReturnType() {
        c0 returnType = super.getReturnType();
        m.checkNotNull(returnType);
        return returnType;
    }

    public final o getStorageManager() {
        return this.O;
    }

    public y0 getTypeAliasDescriptor() {
        return this.P;
    }

    @Override // d0.e0.p.d.m0.c.i1.i0
    public d getUnderlyingConstructorDescriptor() {
        return this.Q;
    }

    @Override // d0.e0.p.d.m0.c.l
    public boolean isPrimary() {
        return getUnderlyingConstructorDescriptor().isPrimary();
    }

    @Override // d0.e0.p.d.m0.c.i1.q, d0.e0.p.d.m0.c.b
    public i0 copy(d0.e0.p.d.m0.c.m mVar, z zVar, u uVar, b.a aVar, boolean z2) {
        m.checkNotNullParameter(mVar, "newOwner");
        m.checkNotNullParameter(zVar, "modality");
        m.checkNotNullParameter(uVar, "visibility");
        m.checkNotNullParameter(aVar, "kind");
        x build = newCopyBuilder().setOwner(mVar).setModality(zVar).setVisibility(uVar).setKind(aVar).setCopyOverrides(z2).build();
        Objects.requireNonNull(build, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.impl.TypeAliasConstructorDescriptor");
        return (i0) build;
    }

    @Override // d0.e0.p.d.m0.c.i1.l, d0.e0.p.d.m0.c.m
    public y0 getContainingDeclaration() {
        return getTypeAliasDescriptor();
    }

    @Override // d0.e0.p.d.m0.c.i1.q, d0.e0.p.d.m0.c.x, d0.e0.p.d.m0.c.w0
    public i0 substitute(c1 c1Var) {
        m.checkNotNullParameter(c1Var, "substitutor");
        x substitute = super.substitute(c1Var);
        Objects.requireNonNull(substitute, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.impl.TypeAliasConstructorDescriptorImpl");
        j0 j0Var = (j0) substitute;
        c1 create = c1.create(j0Var.getReturnType());
        m.checkNotNullExpressionValue(create, "create(substitutedTypeAliasConstructor.returnType)");
        d substitute2 = getUnderlyingConstructorDescriptor().getOriginal().substitute(create);
        if (substitute2 == null) {
            return null;
        }
        j0Var.Q = substitute2;
        return j0Var;
    }

    @Override // d0.e0.p.d.m0.c.i1.q, d0.e0.p.d.m0.c.i1.l, d0.e0.p.d.m0.c.i1.k, d0.e0.p.d.m0.c.m
    public i0 getOriginal() {
        return (i0) super.getOriginal();
    }
}
