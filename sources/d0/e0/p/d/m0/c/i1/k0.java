package d0.e0.p.d.m0.c.i1;

import andhook.lib.HookHelper;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.c.x0;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.x.a;
import d0.e0.p.d.m0.m.o;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.e0;
import d0.e0.p.d.m0.n.j1;
import java.util.ArrayList;
import java.util.List;
import kotlin.jvm.functions.Function1;
/* compiled from: TypeParameterDescriptorImpl.java */
/* loaded from: classes3.dex */
public class k0 extends f {
    public final Function1<c0, Void> t;
    public final List<c0> u;
    public boolean v;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public k0(m mVar, g gVar, boolean z2, j1 j1Var, e eVar, int i, u0 u0Var, Function1<c0, Void> function1, x0 x0Var, o oVar) {
        super(oVar, mVar, gVar, eVar, j1Var, z2, i, u0Var, x0Var);
        if (mVar == null) {
            a(19);
            throw null;
        } else if (gVar == null) {
            a(20);
            throw null;
        } else if (j1Var == null) {
            a(21);
            throw null;
        } else if (eVar == null) {
            a(22);
            throw null;
        } else if (u0Var == null) {
            a(23);
            throw null;
        } else if (x0Var == null) {
            a(24);
            throw null;
        } else if (oVar != null) {
            this.u = new ArrayList(1);
            this.v = false;
            this.t = function1;
        } else {
            a(25);
            throw null;
        }
    }

    public static /* synthetic */ void a(int i) {
        String str = (i == 5 || i == 28) ? "@NotNull method %s.%s must not return null" : "Argument for @NotNull parameter '%s' of %s.%s must not be null";
        Object[] objArr = new Object[(i == 5 || i == 28) ? 2 : 3];
        switch (i) {
            case 1:
            case 7:
            case 13:
            case 20:
                objArr[0] = "annotations";
                break;
            case 2:
            case 8:
            case 14:
            case 21:
                objArr[0] = "variance";
                break;
            case 3:
            case 9:
            case 15:
            case 22:
                objArr[0] = ModelAuditLogEntry.CHANGE_KEY_NAME;
                break;
            case 4:
            case 11:
            case 18:
            case 25:
                objArr[0] = "storageManager";
                break;
            case 5:
            case 28:
                objArr[0] = "kotlin/reflect/jvm/internal/impl/descriptors/impl/TypeParameterDescriptorImpl";
                break;
            case 6:
            case 12:
            case 19:
            default:
                objArr[0] = "containingDeclaration";
                break;
            case 10:
            case 16:
            case 23:
                objArr[0] = "source";
                break;
            case 17:
                objArr[0] = "supertypeLoopsResolver";
                break;
            case 24:
                objArr[0] = "supertypeLoopsChecker";
                break;
            case 26:
                objArr[0] = "bound";
                break;
            case 27:
                objArr[0] = "type";
                break;
        }
        if (i == 5) {
            objArr[1] = "createWithDefaultBound";
        } else if (i != 28) {
            objArr[1] = "kotlin/reflect/jvm/internal/impl/descriptors/impl/TypeParameterDescriptorImpl";
        } else {
            objArr[1] = "resolveUpperBounds";
        }
        switch (i) {
            case 5:
            case 28:
                break;
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
                objArr[2] = "createForFurtherModification";
                break;
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
                objArr[2] = HookHelper.constructorName;
                break;
            case 26:
                objArr[2] = "addUpperBound";
                break;
            case 27:
                objArr[2] = "reportSupertypeLoopError";
                break;
            default:
                objArr[2] = "createWithDefaultBound";
                break;
        }
        String format = String.format(str, objArr);
        if (i == 5 || i == 28) {
            throw new IllegalStateException(format);
        }
    }

    public static k0 createForFurtherModification(m mVar, g gVar, boolean z2, j1 j1Var, e eVar, int i, u0 u0Var, o oVar) {
        if (mVar == null) {
            a(6);
            throw null;
        } else if (gVar == null) {
            a(7);
            throw null;
        } else if (j1Var == null) {
            a(8);
            throw null;
        } else if (eVar == null) {
            a(9);
            throw null;
        } else if (u0Var == null) {
            a(10);
            throw null;
        } else if (oVar != null) {
            return createForFurtherModification(mVar, gVar, z2, j1Var, eVar, i, u0Var, null, x0.a.a, oVar);
        } else {
            a(11);
            throw null;
        }
    }

    public static z0 createWithDefaultBound(m mVar, g gVar, boolean z2, j1 j1Var, e eVar, int i, o oVar) {
        if (mVar == null) {
            a(0);
            throw null;
        } else if (gVar == null) {
            a(1);
            throw null;
        } else if (j1Var == null) {
            a(2);
            throw null;
        } else if (eVar == null) {
            a(3);
            throw null;
        } else if (oVar != null) {
            k0 createForFurtherModification = createForFurtherModification(mVar, gVar, z2, j1Var, eVar, i, u0.a, oVar);
            createForFurtherModification.addUpperBound(a.getBuiltIns(mVar).getDefaultBound());
            createForFurtherModification.setInitialized();
            return createForFurtherModification;
        } else {
            a(4);
            throw null;
        }
    }

    public void addUpperBound(c0 c0Var) {
        if (c0Var != null) {
            e();
            if (!e0.isError(c0Var)) {
                this.u.add(c0Var);
                return;
            }
            return;
        }
        a(26);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.i1.f
    public void c(c0 c0Var) {
        if (c0Var != null) {
            Function1<c0, Void> function1 = this.t;
            if (function1 != null) {
                function1.invoke(c0Var);
                return;
            }
            return;
        }
        a(27);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.i1.f
    public List<c0> d() {
        if (this.v) {
            List<c0> list = this.u;
            if (list != null) {
                return list;
            }
            a(28);
            throw null;
        }
        StringBuilder R = b.d.b.a.a.R("Type parameter descriptor is not initialized: ");
        R.append(f());
        throw new IllegalStateException(R.toString());
    }

    public final void e() {
        if (this.v) {
            StringBuilder R = b.d.b.a.a.R("Type parameter descriptor is already initialized: ");
            R.append(f());
            throw new IllegalStateException(R.toString());
        }
    }

    public final String f() {
        return getName() + " declared in " + d0.e0.p.d.m0.k.e.getFqName(getContainingDeclaration());
    }

    public void setInitialized() {
        e();
        this.v = true;
    }

    public static k0 createForFurtherModification(m mVar, g gVar, boolean z2, j1 j1Var, e eVar, int i, u0 u0Var, Function1<c0, Void> function1, x0 x0Var, o oVar) {
        if (mVar == null) {
            a(12);
            throw null;
        } else if (gVar == null) {
            a(13);
            throw null;
        } else if (j1Var == null) {
            a(14);
            throw null;
        } else if (eVar == null) {
            a(15);
            throw null;
        } else if (u0Var == null) {
            a(16);
            throw null;
        } else if (x0Var == null) {
            a(17);
            throw null;
        } else if (oVar != null) {
            return new k0(mVar, gVar, z2, j1Var, eVar, i, u0Var, function1, x0Var, oVar);
        } else {
            a(18);
            throw null;
        }
    }
}
