package d0.e0.p.d.m0.c.i1;

import andhook.lib.HookHelper;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.d;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.t;
import d0.e0.p.d.m0.c.t0;
import d0.e0.p.d.m0.c.u;
import d0.e0.p.d.m0.c.z;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.k.k;
import d0.e0.p.d.m0.m.f;
import d0.e0.p.d.m0.m.h;
import d0.e0.p.d.m0.m.j;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.u0;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: EnumEntrySyntheticClassDescriptor.java */
/* loaded from: classes3.dex */
public class o extends h {
    public final u0 r;

    /* renamed from: s  reason: collision with root package name */
    public final i f3241s;
    public final j<Set<e>> t;
    public final g u;

    /* compiled from: EnumEntrySyntheticClassDescriptor.java */
    /* loaded from: classes3.dex */
    public class a extends d0.e0.p.d.m0.k.a0.j {

        /* renamed from: b  reason: collision with root package name */
        public final h<e, Collection<? extends t0>> f3242b;
        public final h<e, Collection<? extends n0>> c;
        public final j<Collection<m>> d;
        public final /* synthetic */ o e;

        /* compiled from: EnumEntrySyntheticClassDescriptor.java */
        /* renamed from: d0.e0.p.d.m0.c.i1.o$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public class C0295a implements Function1<e, Collection<? extends t0>> {
            public C0295a(o oVar) {
            }

            public Collection<? extends t0> invoke(e eVar) {
                a aVar = a.this;
                Objects.requireNonNull(aVar);
                if (eVar != null) {
                    return aVar.c(eVar, aVar.b().getContributedFunctions(eVar, d0.e0.p.d.m0.d.b.d.FOR_NON_TRACKED_SCOPE));
                }
                a.a(8);
                throw null;
            }
        }

        /* compiled from: EnumEntrySyntheticClassDescriptor.java */
        /* loaded from: classes3.dex */
        public class b implements Function1<e, Collection<? extends n0>> {
            public b(o oVar) {
            }

            public Collection<? extends n0> invoke(e eVar) {
                a aVar = a.this;
                Objects.requireNonNull(aVar);
                if (eVar != null) {
                    return aVar.c(eVar, aVar.b().getContributedVariables(eVar, d0.e0.p.d.m0.d.b.d.FOR_NON_TRACKED_SCOPE));
                }
                a.a(4);
                throw null;
            }
        }

        /* compiled from: EnumEntrySyntheticClassDescriptor.java */
        /* loaded from: classes3.dex */
        public class c implements Function0<Collection<m>> {
            public c(o oVar) {
            }

            @Override // kotlin.jvm.functions.Function0
            public Collection<m> invoke() {
                a aVar = a.this;
                Objects.requireNonNull(aVar);
                HashSet hashSet = new HashSet();
                for (e eVar : aVar.e.t.invoke()) {
                    d0.e0.p.d.m0.d.b.d dVar = d0.e0.p.d.m0.d.b.d.FOR_NON_TRACKED_SCOPE;
                    hashSet.addAll(aVar.getContributedFunctions(eVar, dVar));
                    hashSet.addAll(aVar.getContributedVariables(eVar, dVar));
                }
                return hashSet;
            }
        }

        /* compiled from: EnumEntrySyntheticClassDescriptor.java */
        /* loaded from: classes3.dex */
        public class d extends d0.e0.p.d.m0.k.i {
            public final /* synthetic */ Set a;

            public d(a aVar, Set set) {
                this.a = set;
            }

            public static /* synthetic */ void a(int i) {
                Object[] objArr = new Object[3];
                if (i == 1) {
                    objArr[0] = "fromSuper";
                } else if (i != 2) {
                    objArr[0] = "fakeOverride";
                } else {
                    objArr[0] = "fromCurrent";
                }
                objArr[1] = "kotlin/reflect/jvm/internal/impl/descriptors/impl/EnumEntrySyntheticClassDescriptor$EnumEntryScope$4";
                if (i == 1 || i == 2) {
                    objArr[2] = "conflict";
                } else {
                    objArr[2] = "addFakeOverride";
                }
                throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", objArr));
            }

            @Override // d0.e0.p.d.m0.k.j
            public void addFakeOverride(d0.e0.p.d.m0.c.b bVar) {
                if (bVar != null) {
                    k.resolveUnknownVisibilityForMember(bVar, null);
                    this.a.add(bVar);
                    return;
                }
                a(0);
                throw null;
            }

            @Override // d0.e0.p.d.m0.k.i
            public void conflict(d0.e0.p.d.m0.c.b bVar, d0.e0.p.d.m0.c.b bVar2) {
                if (bVar == null) {
                    a(1);
                    throw null;
                } else if (bVar2 == null) {
                    a(2);
                    throw null;
                }
            }
        }

        public a(o oVar, d0.e0.p.d.m0.m.o oVar2) {
            if (oVar2 != null) {
                this.e = oVar;
                this.f3242b = oVar2.createMemoizedFunction(new C0295a(oVar));
                this.c = oVar2.createMemoizedFunction(new b(oVar));
                this.d = oVar2.createLazyValue(new c(oVar));
                return;
            }
            a(0);
            throw null;
        }

        /* JADX WARN: Removed duplicated region for block: B:16:0x0022  */
        /* JADX WARN: Removed duplicated region for block: B:20:0x002d  */
        /* JADX WARN: Removed duplicated region for block: B:21:0x0032  */
        /* JADX WARN: Removed duplicated region for block: B:22:0x0037  */
        /* JADX WARN: Removed duplicated region for block: B:23:0x003c  */
        /* JADX WARN: Removed duplicated region for block: B:24:0x0041  */
        /* JADX WARN: Removed duplicated region for block: B:25:0x0046  */
        /* JADX WARN: Removed duplicated region for block: B:26:0x0049  */
        /* JADX WARN: Removed duplicated region for block: B:27:0x004e  */
        /* JADX WARN: Removed duplicated region for block: B:30:0x005d  */
        /* JADX WARN: Removed duplicated region for block: B:43:0x008b  */
        /* JADX WARN: Removed duplicated region for block: B:45:0x0090  */
        /* JADX WARN: Removed duplicated region for block: B:46:0x0095  */
        /* JADX WARN: Removed duplicated region for block: B:47:0x009a  */
        /* JADX WARN: Removed duplicated region for block: B:48:0x009d  */
        /* JADX WARN: Removed duplicated region for block: B:49:0x00a0  */
        /* JADX WARN: Removed duplicated region for block: B:50:0x00a5  */
        /* JADX WARN: Removed duplicated region for block: B:51:0x00a8  */
        /* JADX WARN: Removed duplicated region for block: B:52:0x00ad  */
        /* JADX WARN: Removed duplicated region for block: B:55:0x00b5 A[ADDED_TO_REGION] */
        /* JADX WARN: Removed duplicated region for block: B:59:0x00be  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public static /* synthetic */ void a(int r13) {
            /*
                Method dump skipped, instructions count: 346
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.c.i1.o.a.a(int):void");
        }

        public final i b() {
            i memberScope = this.e.getTypeConstructor().getSupertypes().iterator().next().getMemberScope();
            if (memberScope != null) {
                return memberScope;
            }
            a(9);
            throw null;
        }

        public final <D extends d0.e0.p.d.m0.c.b> Collection<? extends D> c(e eVar, Collection<? extends D> collection) {
            if (eVar == null) {
                a(10);
                throw null;
            } else if (collection != null) {
                LinkedHashSet linkedHashSet = new LinkedHashSet();
                k.f3440b.generateOverridesInFunctionGroup(eVar, collection, Collections.emptySet(), this.e, new d(this, linkedHashSet));
                return linkedHashSet;
            } else {
                a(11);
                throw null;
            }
        }

        @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.i
        public Set<e> getClassifierNames() {
            Set<e> emptySet = Collections.emptySet();
            if (emptySet != null) {
                return emptySet;
            }
            a(18);
            throw null;
        }

        @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.l
        public Collection<m> getContributedDescriptors(d0.e0.p.d.m0.k.a0.d dVar, Function1<? super e, Boolean> function1) {
            if (dVar == null) {
                a(13);
                throw null;
            } else if (function1 != null) {
                Collection<m> invoke = this.d.invoke();
                if (invoke != null) {
                    return invoke;
                }
                a(15);
                throw null;
            } else {
                a(14);
                throw null;
            }
        }

        @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.i
        public Collection<? extends t0> getContributedFunctions(e eVar, d0.e0.p.d.m0.d.b.b bVar) {
            if (eVar == null) {
                a(5);
                throw null;
            } else if (bVar != null) {
                Collection<? extends t0> collection = (Collection) ((f.m) this.f3242b).invoke(eVar);
                if (collection != null) {
                    return collection;
                }
                a(7);
                throw null;
            } else {
                a(6);
                throw null;
            }
        }

        @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.i
        public Collection<? extends n0> getContributedVariables(e eVar, d0.e0.p.d.m0.d.b.b bVar) {
            if (eVar == null) {
                a(1);
                throw null;
            } else if (bVar != null) {
                Collection<? extends n0> collection = (Collection) ((f.m) this.c).invoke(eVar);
                if (collection != null) {
                    return collection;
                }
                a(3);
                throw null;
            } else {
                a(2);
                throw null;
            }
        }

        @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.i
        public Set<e> getFunctionNames() {
            Set<e> invoke = this.e.t.invoke();
            if (invoke != null) {
                return invoke;
            }
            a(17);
            throw null;
        }

        @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.i
        public Set<e> getVariableNames() {
            Set<e> invoke = this.e.t.invoke();
            if (invoke != null) {
                return invoke;
            }
            a(19);
            throw null;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public o(d0.e0.p.d.m0.m.o oVar, d0.e0.p.d.m0.c.e eVar, c0 c0Var, e eVar2, j<Set<e>> jVar, g gVar, d0.e0.p.d.m0.c.u0 u0Var) {
        super(oVar, eVar, eVar2, u0Var, false);
        if (oVar == null) {
            a(6);
            throw null;
        } else if (c0Var == null) {
            a(8);
            throw null;
        } else if (eVar2 == null) {
            a(9);
            throw null;
        } else if (jVar == null) {
            a(10);
            throw null;
        } else if (gVar == null) {
            a(11);
            throw null;
        } else if (u0Var != null) {
            this.u = gVar;
            this.r = new d0.e0.p.d.m0.n.i(this, Collections.emptyList(), Collections.singleton(c0Var), oVar);
            this.f3241s = new a(this, oVar);
            this.t = jVar;
        } else {
            a(12);
            throw null;
        }
    }

    public static /* synthetic */ void a(int i) {
        String str;
        int i2;
        switch (i) {
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
                str = "@NotNull method %s.%s must not return null";
                break;
            default:
                str = "Argument for @NotNull parameter '%s' of %s.%s must not be null";
                break;
        }
        switch (i) {
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
                i2 = 2;
                break;
            default:
                i2 = 3;
                break;
        }
        Object[] objArr = new Object[i2];
        switch (i) {
            case 1:
                objArr[0] = "enumClass";
                break;
            case 2:
            case 9:
                objArr[0] = ModelAuditLogEntry.CHANGE_KEY_NAME;
                break;
            case 3:
            case 10:
                objArr[0] = "enumMemberNames";
                break;
            case 4:
            case 11:
                objArr[0] = "annotations";
                break;
            case 5:
            case 12:
                objArr[0] = "source";
                break;
            case 6:
            default:
                objArr[0] = "storageManager";
                break;
            case 7:
                objArr[0] = "containingClass";
                break;
            case 8:
                objArr[0] = "supertype";
                break;
            case 13:
                objArr[0] = "kotlinTypeRefiner";
                break;
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
                objArr[0] = "kotlin/reflect/jvm/internal/impl/descriptors/impl/EnumEntrySyntheticClassDescriptor";
                break;
        }
        switch (i) {
            case 14:
                objArr[1] = "getUnsubstitutedMemberScope";
                break;
            case 15:
                objArr[1] = "getStaticScope";
                break;
            case 16:
                objArr[1] = "getConstructors";
                break;
            case 17:
                objArr[1] = "getTypeConstructor";
                break;
            case 18:
                objArr[1] = "getKind";
                break;
            case 19:
                objArr[1] = "getModality";
                break;
            case 20:
                objArr[1] = "getVisibility";
                break;
            case 21:
                objArr[1] = "getAnnotations";
                break;
            case 22:
                objArr[1] = "getDeclaredTypeParameters";
                break;
            case 23:
                objArr[1] = "getSealedSubclasses";
                break;
            default:
                objArr[1] = "kotlin/reflect/jvm/internal/impl/descriptors/impl/EnumEntrySyntheticClassDescriptor";
                break;
        }
        switch (i) {
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
                objArr[2] = HookHelper.constructorName;
                break;
            case 13:
                objArr[2] = "getUnsubstitutedMemberScope";
                break;
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
                break;
            default:
                objArr[2] = "create";
                break;
        }
        String format = String.format(str, objArr);
        switch (i) {
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
                throw new IllegalStateException(format);
            default:
                throw new IllegalArgumentException(format);
        }
    }

    public static o create(d0.e0.p.d.m0.m.o oVar, d0.e0.p.d.m0.c.e eVar, e eVar2, j<Set<e>> jVar, g gVar, d0.e0.p.d.m0.c.u0 u0Var) {
        if (oVar == null) {
            a(0);
            throw null;
        } else if (eVar == null) {
            a(1);
            throw null;
        } else if (eVar2 == null) {
            a(2);
            throw null;
        } else if (jVar == null) {
            a(3);
            throw null;
        } else if (gVar == null) {
            a(4);
            throw null;
        } else if (u0Var != null) {
            return new o(oVar, eVar, eVar.getDefaultType(), eVar2, jVar, gVar, u0Var);
        } else {
            a(5);
            throw null;
        }
    }

    @Override // d0.e0.p.d.m0.c.g1.a
    public g getAnnotations() {
        g gVar = this.u;
        if (gVar != null) {
            return gVar;
        }
        a(21);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.e
    public d0.e0.p.d.m0.c.e getCompanionObjectDescriptor() {
        return null;
    }

    @Override // d0.e0.p.d.m0.c.e
    public Collection<d> getConstructors() {
        List emptyList = Collections.emptyList();
        if (emptyList != null) {
            return emptyList;
        }
        a(16);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.i
    public List<z0> getDeclaredTypeParameters() {
        List<z0> emptyList = Collections.emptyList();
        if (emptyList != null) {
            return emptyList;
        }
        a(22);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.e
    public d0.e0.p.d.m0.c.f getKind() {
        return d0.e0.p.d.m0.c.f.ENUM_ENTRY;
    }

    @Override // d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.y
    public z getModality() {
        return z.FINAL;
    }

    @Override // d0.e0.p.d.m0.c.e
    public Collection<d0.e0.p.d.m0.c.e> getSealedSubclasses() {
        List emptyList = Collections.emptyList();
        if (emptyList != null) {
            return emptyList;
        }
        a(23);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.e
    public i getStaticScope() {
        i.b bVar = i.b.f3433b;
        if (bVar != null) {
            return bVar;
        }
        a(15);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.h
    public u0 getTypeConstructor() {
        u0 u0Var = this.r;
        if (u0Var != null) {
            return u0Var;
        }
        a(17);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.i1.u
    public i getUnsubstitutedMemberScope(d0.e0.p.d.m0.n.l1.g gVar) {
        if (gVar != null) {
            i iVar = this.f3241s;
            if (iVar != null) {
                return iVar;
            }
            a(14);
            throw null;
        }
        a(13);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.e
    public d getUnsubstitutedPrimaryConstructor() {
        return null;
    }

    @Override // d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.q, d0.e0.p.d.m0.c.y
    public u getVisibility() {
        u uVar = t.e;
        if (uVar != null) {
            return uVar;
        }
        a(20);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.y
    public boolean isActual() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.e
    public boolean isCompanionObject() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.e
    public boolean isData() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.y
    public boolean isExpect() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.e
    public boolean isFun() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.e
    public boolean isInline() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.i
    public boolean isInner() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.e
    public boolean isValue() {
        return false;
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("enum entry ");
        R.append(getName());
        return R.toString();
    }
}
