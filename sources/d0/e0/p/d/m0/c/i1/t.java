package d0.e0.p.d.m0.c.i1;

import d0.e0.p.d.m0.c.d;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.f;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.c.o;
import d0.e0.p.d.m0.c.q0;
import d0.e0.p.d.m0.c.z;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.k.a0.n;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.c1;
import d0.e0.p.d.m0.n.d0;
import d0.e0.p.d.m0.n.e1;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.j1;
import d0.e0.p.d.m0.n.u0;
import d0.t.u;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.jvm.functions.Function1;
/* compiled from: LazySubstitutingClassDescriptor.java */
/* loaded from: classes3.dex */
public class t extends u {
    public final u k;
    public final c1 l;
    public c1 m;
    public List<z0> n;
    public List<z0> o;
    public u0 p;

    /* compiled from: LazySubstitutingClassDescriptor.java */
    /* loaded from: classes3.dex */
    public class a implements Function1<z0, Boolean> {
        public a(t tVar) {
        }

        public Boolean invoke(z0 z0Var) {
            return Boolean.valueOf(!z0Var.isCapturedFromOuterDeclaration());
        }
    }

    public t(u uVar, c1 c1Var) {
        this.k = uVar;
        this.l = c1Var;
    }

    /* JADX WARN: Removed duplicated region for block: B:39:0x0069  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x006e  */
    /* JADX WARN: Removed duplicated region for block: B:41:0x0073  */
    /* JADX WARN: Removed duplicated region for block: B:42:0x0078  */
    /* JADX WARN: Removed duplicated region for block: B:43:0x007d  */
    /* JADX WARN: Removed duplicated region for block: B:44:0x0082  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x0087  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x008c  */
    /* JADX WARN: Removed duplicated region for block: B:47:0x0091  */
    /* JADX WARN: Removed duplicated region for block: B:48:0x0094  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x0099  */
    /* JADX WARN: Removed duplicated region for block: B:50:0x009e  */
    /* JADX WARN: Removed duplicated region for block: B:51:0x00a3  */
    /* JADX WARN: Removed duplicated region for block: B:52:0x00a8  */
    /* JADX WARN: Removed duplicated region for block: B:53:0x00ad  */
    /* JADX WARN: Removed duplicated region for block: B:54:0x00b2  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x00b7  */
    /* JADX WARN: Removed duplicated region for block: B:56:0x00ba  */
    /* JADX WARN: Removed duplicated region for block: B:57:0x00bd  */
    /* JADX WARN: Removed duplicated region for block: B:59:0x00c1 A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:71:0x00de A[ADDED_TO_REGION] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static /* synthetic */ void a(int r15) {
        /*
            Method dump skipped, instructions count: 310
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.c.i1.t.a(int):void");
    }

    @Override // d0.e0.p.d.m0.c.m
    public <R, D> R accept(o<R, D> oVar, D d) {
        return oVar.visitClassDescriptor(this, d);
    }

    public final c1 b() {
        if (this.m == null) {
            if (this.l.isEmpty()) {
                this.m = this.l;
            } else {
                List<z0> parameters = this.k.getTypeConstructor().getParameters();
                this.n = new ArrayList(parameters.size());
                this.m = d0.e0.p.d.m0.n.o.substituteTypeParameters(parameters, this.l.getSubstitution(), this, this.n);
                this.o = u.filter(this.n, new a(this));
            }
        }
        return this.m;
    }

    @Override // d0.e0.p.d.m0.c.g1.a
    public g getAnnotations() {
        g annotations = this.k.getAnnotations();
        if (annotations != null) {
            return annotations;
        }
        a(18);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.e
    public e getCompanionObjectDescriptor() {
        return this.k.getCompanionObjectDescriptor();
    }

    @Override // d0.e0.p.d.m0.c.e
    public Collection<d> getConstructors() {
        Collection<d> constructors = this.k.getConstructors();
        ArrayList arrayList = new ArrayList(constructors.size());
        for (d dVar : constructors) {
            arrayList.add(((d) dVar.newCopyBuilder().setOriginal(dVar.getOriginal()).setModality(dVar.getModality()).setVisibility(dVar.getVisibility()).setKind(dVar.getKind()).setCopyOverrides(false).build()).substitute(b()));
        }
        return arrayList;
    }

    @Override // d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.n, d0.e0.p.d.m0.c.m
    public m getContainingDeclaration() {
        m containingDeclaration = this.k.getContainingDeclaration();
        if (containingDeclaration != null) {
            return containingDeclaration;
        }
        a(21);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.i
    public List<z0> getDeclaredTypeParameters() {
        b();
        List<z0> list = this.o;
        if (list != null) {
            return list;
        }
        a(29);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.h
    public j0 getDefaultType() {
        j0 simpleTypeWithNonTrivialMemberScope = d0.simpleTypeWithNonTrivialMemberScope(getAnnotations(), getTypeConstructor(), e1.getDefaultTypeProjections(getTypeConstructor().getParameters()), false, getUnsubstitutedMemberScope());
        if (simpleTypeWithNonTrivialMemberScope != null) {
            return simpleTypeWithNonTrivialMemberScope;
        }
        a(16);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.e
    public f getKind() {
        f kind = this.k.getKind();
        if (kind != null) {
            return kind;
        }
        a(24);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.i1.u
    public i getMemberScope(d0.e0.p.d.m0.n.z0 z0Var, d0.e0.p.d.m0.n.l1.g gVar) {
        if (z0Var == null) {
            a(5);
            throw null;
        } else if (gVar != null) {
            i memberScope = this.k.getMemberScope(z0Var, gVar);
            if (!this.l.isEmpty()) {
                return new n(memberScope, b());
            }
            if (memberScope != null) {
                return memberScope;
            }
            a(7);
            throw null;
        } else {
            a(6);
            throw null;
        }
    }

    @Override // d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.y
    public z getModality() {
        z modality = this.k.getModality();
        if (modality != null) {
            return modality;
        }
        a(25);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.m
    public d0.e0.p.d.m0.g.e getName() {
        d0.e0.p.d.m0.g.e name = this.k.getName();
        if (name != null) {
            return name;
        }
        a(19);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.e
    public Collection<e> getSealedSubclasses() {
        Collection<e> sealedSubclasses = this.k.getSealedSubclasses();
        if (sealedSubclasses != null) {
            return sealedSubclasses;
        }
        a(30);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.p
    public d0.e0.p.d.m0.c.u0 getSource() {
        return d0.e0.p.d.m0.c.u0.a;
    }

    @Override // d0.e0.p.d.m0.c.e
    public i getStaticScope() {
        i staticScope = this.k.getStaticScope();
        if (staticScope != null) {
            return staticScope;
        }
        a(15);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.e
    public q0 getThisAsReceiverParameter() {
        throw new UnsupportedOperationException();
    }

    @Override // d0.e0.p.d.m0.c.h
    public u0 getTypeConstructor() {
        u0 typeConstructor = this.k.getTypeConstructor();
        if (!this.l.isEmpty()) {
            if (this.p == null) {
                c1 b2 = b();
                Collection<c0> supertypes = typeConstructor.getSupertypes();
                ArrayList arrayList = new ArrayList(supertypes.size());
                for (c0 c0Var : supertypes) {
                    arrayList.add(b2.substitute(c0Var, j1.INVARIANT));
                }
                this.p = new d0.e0.p.d.m0.n.i(this, this.n, arrayList, d0.e0.p.d.m0.m.f.f3486b);
            }
            u0 u0Var = this.p;
            if (u0Var != null) {
                return u0Var;
            }
            a(1);
            throw null;
        } else if (typeConstructor != null) {
            return typeConstructor;
        } else {
            a(0);
            throw null;
        }
    }

    @Override // d0.e0.p.d.m0.c.e
    public i getUnsubstitutedInnerClassesScope() {
        i unsubstitutedInnerClassesScope = this.k.getUnsubstitutedInnerClassesScope();
        if (unsubstitutedInnerClassesScope != null) {
            return unsubstitutedInnerClassesScope;
        }
        a(27);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.e
    public i getUnsubstitutedMemberScope() {
        i unsubstitutedMemberScope = getUnsubstitutedMemberScope(d0.e0.p.d.m0.k.x.a.getKotlinTypeRefiner(d0.e0.p.d.m0.k.e.getContainingModule(this.k)));
        if (unsubstitutedMemberScope != null) {
            return unsubstitutedMemberScope;
        }
        a(12);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.e
    public d getUnsubstitutedPrimaryConstructor() {
        return this.k.getUnsubstitutedPrimaryConstructor();
    }

    @Override // d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.q, d0.e0.p.d.m0.c.y
    public d0.e0.p.d.m0.c.u getVisibility() {
        d0.e0.p.d.m0.c.u visibility = this.k.getVisibility();
        if (visibility != null) {
            return visibility;
        }
        a(26);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.y
    public boolean isActual() {
        return this.k.isActual();
    }

    @Override // d0.e0.p.d.m0.c.e
    public boolean isCompanionObject() {
        return this.k.isCompanionObject();
    }

    @Override // d0.e0.p.d.m0.c.e
    public boolean isData() {
        return this.k.isData();
    }

    @Override // d0.e0.p.d.m0.c.y
    public boolean isExpect() {
        return this.k.isExpect();
    }

    @Override // d0.e0.p.d.m0.c.y
    public boolean isExternal() {
        return this.k.isExternal();
    }

    @Override // d0.e0.p.d.m0.c.e
    public boolean isFun() {
        return this.k.isFun();
    }

    @Override // d0.e0.p.d.m0.c.e
    public boolean isInline() {
        return this.k.isInline();
    }

    @Override // d0.e0.p.d.m0.c.i
    public boolean isInner() {
        return this.k.isInner();
    }

    @Override // d0.e0.p.d.m0.c.e
    public boolean isValue() {
        return this.k.isValue();
    }

    @Override // d0.e0.p.d.m0.c.i1.u
    public i getUnsubstitutedMemberScope(d0.e0.p.d.m0.n.l1.g gVar) {
        if (gVar != null) {
            i unsubstitutedMemberScope = this.k.getUnsubstitutedMemberScope(gVar);
            if (!this.l.isEmpty()) {
                return new n(unsubstitutedMemberScope, b());
            }
            if (unsubstitutedMemberScope != null) {
                return unsubstitutedMemberScope;
            }
            a(14);
            throw null;
        }
        a(13);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.w0
    public e substitute(c1 c1Var) {
        if (c1Var != null) {
            return c1Var.isEmpty() ? this : new t(this, c1.createChainedSubstitutor(c1Var.getSubstitution(), b().getSubstitution()));
        }
        a(22);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.m
    public e getOriginal() {
        e original = this.k.getOriginal();
        if (original != null) {
            return original;
        }
        a(20);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.e
    public i getMemberScope(d0.e0.p.d.m0.n.z0 z0Var) {
        if (z0Var != null) {
            i memberScope = getMemberScope(z0Var, d0.e0.p.d.m0.k.x.a.getKotlinTypeRefiner(d0.e0.p.d.m0.k.e.getContainingModule(this)));
            if (memberScope != null) {
                return memberScope;
            }
            a(11);
            throw null;
        }
        a(10);
        throw null;
    }
}
