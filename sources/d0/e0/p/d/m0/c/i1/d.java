package d0.e0.p.d.m0.c.i1;

import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.l1.g;
import d0.z.d.o;
import kotlin.jvm.functions.Function1;
/* compiled from: AbstractTypeAliasDescriptor.kt */
/* loaded from: classes3.dex */
public final class d extends o implements Function1<g, j0> {
    public final /* synthetic */ e this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public d(e eVar) {
        super(1);
        this.this$0 = eVar;
    }

    public final j0 invoke(g gVar) {
        h refineDescriptor = gVar.refineDescriptor(this.this$0);
        if (refineDescriptor == null) {
            return null;
        }
        return refineDescriptor.getDefaultType();
    }
}
