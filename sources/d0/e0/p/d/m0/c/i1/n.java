package d0.e0.p.d.m0.c.i1;

import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.k.a0.i;
import d0.z.d.m;
/* compiled from: EmptyPackageFragmentDesciptor.kt */
/* loaded from: classes3.dex */
public final class n extends a0 {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public n(c0 c0Var, b bVar) {
        super(c0Var, bVar);
        m.checkNotNullParameter(c0Var, "module");
        m.checkNotNullParameter(bVar, "fqName");
    }

    @Override // d0.e0.p.d.m0.c.e0
    public i.b getMemberScope() {
        return i.b.f3433b;
    }
}
