package d0.e0.p.d.m0.c.i1;

import andhook.lib.HookHelper;
import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.o;
import d0.e0.p.d.m0.c.p0;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.x.a;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.j0;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
/* compiled from: PropertySetterDescriptorImpl.java */
/* loaded from: classes3.dex */
public class e0 extends b0 implements p0 {
    public c1 v;
    public final p0 w;

    /* JADX WARN: Illegal instructions before constructor call */
    /* JADX WARN: Multi-variable type inference failed */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public e0(d0.e0.p.d.m0.c.n0 r13, d0.e0.p.d.m0.c.g1.g r14, d0.e0.p.d.m0.c.z r15, d0.e0.p.d.m0.c.u r16, boolean r17, boolean r18, boolean r19, d0.e0.p.d.m0.c.b.a r20, d0.e0.p.d.m0.c.p0 r21, d0.e0.p.d.m0.c.u0 r22) {
        /*
            r12 = this;
            r0 = 0
            if (r13 == 0) goto L63
            if (r14 == 0) goto L5d
            if (r15 == 0) goto L57
            if (r16 == 0) goto L51
            if (r20 == 0) goto L4b
            if (r22 == 0) goto L45
            java.lang.String r0 = "<set-"
            java.lang.StringBuilder r0 = b.d.b.a.a.R(r0)
            d0.e0.p.d.m0.g.e r1 = r13.getName()
            r0.append(r1)
            java.lang.String r1 = ">"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            d0.e0.p.d.m0.g.e r6 = d0.e0.p.d.m0.g.e.special(r0)
            r1 = r12
            r2 = r15
            r3 = r16
            r4 = r13
            r5 = r14
            r7 = r17
            r8 = r18
            r9 = r19
            r10 = r20
            r11 = r22
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11)
            if (r21 == 0) goto L40
            r1 = r12
            r0 = r21
            goto L42
        L40:
            r0 = r12
            r1 = r0
        L42:
            r1.w = r0
            return
        L45:
            r1 = r12
            r2 = 5
            a(r2)
            throw r0
        L4b:
            r1 = r12
            r2 = 4
            a(r2)
            throw r0
        L51:
            r1 = r12
            r2 = 3
            a(r2)
            throw r0
        L57:
            r1 = r12
            r2 = 2
            a(r2)
            throw r0
        L5d:
            r1 = r12
            r2 = 1
            a(r2)
            throw r0
        L63:
            r1 = r12
            r2 = 0
            a(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.c.i1.e0.<init>(d0.e0.p.d.m0.c.n0, d0.e0.p.d.m0.c.g1.g, d0.e0.p.d.m0.c.z, d0.e0.p.d.m0.c.u, boolean, boolean, boolean, d0.e0.p.d.m0.c.b$a, d0.e0.p.d.m0.c.p0, d0.e0.p.d.m0.c.u0):void");
    }

    public static /* synthetic */ void a(int i) {
        String str;
        int i2;
        switch (i) {
            case 10:
            case 11:
            case 12:
            case 13:
                str = "@NotNull method %s.%s must not return null";
                break;
            default:
                str = "Argument for @NotNull parameter '%s' of %s.%s must not be null";
                break;
        }
        switch (i) {
            case 10:
            case 11:
            case 12:
            case 13:
                i2 = 2;
                break;
            default:
                i2 = 3;
                break;
        }
        Object[] objArr = new Object[i2];
        switch (i) {
            case 1:
            case 9:
                objArr[0] = "annotations";
                break;
            case 2:
                objArr[0] = "modality";
                break;
            case 3:
                objArr[0] = "visibility";
                break;
            case 4:
                objArr[0] = "kind";
                break;
            case 5:
                objArr[0] = "source";
                break;
            case 6:
                objArr[0] = "parameter";
                break;
            case 7:
                objArr[0] = "setterDescriptor";
                break;
            case 8:
                objArr[0] = "type";
                break;
            case 10:
            case 11:
            case 12:
            case 13:
                objArr[0] = "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertySetterDescriptorImpl";
                break;
            default:
                objArr[0] = "correspondingProperty";
                break;
        }
        switch (i) {
            case 10:
                objArr[1] = "getOverriddenDescriptors";
                break;
            case 11:
                objArr[1] = "getValueParameters";
                break;
            case 12:
                objArr[1] = "getReturnType";
                break;
            case 13:
                objArr[1] = "getOriginal";
                break;
            default:
                objArr[1] = "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertySetterDescriptorImpl";
                break;
        }
        switch (i) {
            case 6:
                objArr[2] = "initialize";
                break;
            case 7:
            case 8:
            case 9:
                objArr[2] = "createSetterParameter";
                break;
            case 10:
            case 11:
            case 12:
            case 13:
                break;
            default:
                objArr[2] = HookHelper.constructorName;
                break;
        }
        String format = String.format(str, objArr);
        switch (i) {
            case 10:
            case 11:
            case 12:
            case 13:
                throw new IllegalStateException(format);
            default:
                throw new IllegalArgumentException(format);
        }
    }

    public static l0 createSetterParameter(p0 p0Var, c0 c0Var, g gVar) {
        if (p0Var == null) {
            a(7);
            throw null;
        } else if (c0Var == null) {
            a(8);
            throw null;
        } else if (gVar != null) {
            return new l0(p0Var, null, 0, gVar, e.special("<set-?>"), c0Var, false, false, false, null, u0.a);
        } else {
            a(9);
            throw null;
        }
    }

    @Override // d0.e0.p.d.m0.c.m
    public <R, D> R accept(o<R, D> oVar, D d) {
        return oVar.visitPropertySetterDescriptor(this, d);
    }

    @Override // d0.e0.p.d.m0.c.x, d0.e0.p.d.m0.c.b, d0.e0.p.d.m0.c.a
    public Collection<? extends p0> getOverriddenDescriptors() {
        return b(false);
    }

    @Override // d0.e0.p.d.m0.c.a
    public c0 getReturnType() {
        j0 unitType = a.getBuiltIns(this).getUnitType();
        if (unitType != null) {
            return unitType;
        }
        a(12);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.a
    public List<c1> getValueParameters() {
        c1 c1Var = this.v;
        if (c1Var != null) {
            List<c1> singletonList = Collections.singletonList(c1Var);
            if (singletonList != null) {
                return singletonList;
            }
            a(11);
            throw null;
        }
        throw new IllegalStateException();
    }

    public void initialize(c1 c1Var) {
        if (c1Var != null) {
            this.v = c1Var;
        } else {
            a(6);
            throw null;
        }
    }

    @Override // d0.e0.p.d.m0.c.i1.b0, d0.e0.p.d.m0.c.i1.l, d0.e0.p.d.m0.c.i1.k, d0.e0.p.d.m0.c.m
    public p0 getOriginal() {
        p0 p0Var = this.w;
        if (p0Var != null) {
            return p0Var;
        }
        a(13);
        throw null;
    }
}
