package d0.e0.p.d.m0.c.i1;

import andhook.lib.HookHelper;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.a;
import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.d1;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.i1.l0;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.c.o;
import d0.e0.p.d.m0.c.q0;
import d0.e0.p.d.m0.c.t;
import d0.e0.p.d.m0.c.u;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.c.x;
import d0.e0.p.d.m0.c.z;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.j1;
import d0.e0.p.d.m0.p.i;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.jvm.functions.Function0;
/* compiled from: FunctionDescriptorImpl.java */
/* loaded from: classes3.dex */
public abstract class q extends l implements x {
    public boolean A;
    public boolean B;
    public boolean C;
    public boolean D;
    public boolean E;
    public boolean F;
    public Collection<? extends x> G;
    public volatile Function0<Collection<x>> H;
    public final x I;
    public final b.a J;
    public x K;
    public Map<a.AbstractC0290a<?>, Object> L;
    public List<z0> n;
    public List<c1> o;
    public c0 p;
    public q0 q;
    public q0 r;

    /* renamed from: s  reason: collision with root package name */
    public z f3243s;
    public u t;
    public boolean u;
    public boolean v;
    public boolean w;

    /* renamed from: x  reason: collision with root package name */
    public boolean f3244x;

    /* renamed from: y  reason: collision with root package name */
    public boolean f3245y;

    /* renamed from: z  reason: collision with root package name */
    public boolean f3246z;

    /* compiled from: FunctionDescriptorImpl.java */
    /* loaded from: classes3.dex */
    public class a implements Function0<Collection<x>> {
        public final /* synthetic */ d0.e0.p.d.m0.n.c1 j;

        public a(d0.e0.p.d.m0.n.c1 c1Var) {
            this.j = c1Var;
        }

        @Override // kotlin.jvm.functions.Function0
        public Collection<x> invoke() {
            i iVar = new i();
            for (x xVar : q.this.getOverriddenDescriptors()) {
                iVar.add(xVar.substitute(this.j));
            }
            return iVar;
        }
    }

    /* compiled from: FunctionDescriptorImpl.java */
    /* loaded from: classes3.dex */
    public static class b implements Function0<List<d1>> {
        public final /* synthetic */ List j;

        public b(List list) {
            this.j = list;
        }

        @Override // kotlin.jvm.functions.Function0
        public List<d1> invoke() {
            return this.j;
        }
    }

    /* compiled from: FunctionDescriptorImpl.java */
    /* loaded from: classes3.dex */
    public class c implements x.a<x> {
        public d0.e0.p.d.m0.n.z0 a;

        /* renamed from: b  reason: collision with root package name */
        public m f3247b;
        public z c;
        public u d;
        public x e;
        public b.a f;
        public List<c1> g;
        public q0 h;
        public q0 i;
        public c0 j;
        public e k;
        public boolean l;
        public boolean m;
        public boolean n;
        public boolean o;
        public boolean p;
        public List<z0> q;
        public g r;

        /* renamed from: s  reason: collision with root package name */
        public boolean f3248s;
        public Map<a.AbstractC0290a<?>, Object> t;
        public Boolean u;
        public boolean v;
        public final /* synthetic */ q w;

        public c(q qVar, d0.e0.p.d.m0.n.z0 z0Var, m mVar, z zVar, u uVar, b.a aVar, List<c1> list, q0 q0Var, c0 c0Var, e eVar) {
            if (z0Var == null) {
                a(0);
                throw null;
            } else if (mVar == null) {
                a(1);
                throw null;
            } else if (zVar == null) {
                a(2);
                throw null;
            } else if (uVar == null) {
                a(3);
                throw null;
            } else if (aVar == null) {
                a(4);
                throw null;
            } else if (list == null) {
                a(5);
                throw null;
            } else if (c0Var != null) {
                this.w = qVar;
                this.e = null;
                this.i = qVar.r;
                this.l = true;
                this.m = false;
                this.n = false;
                this.o = false;
                this.p = qVar.isHiddenToOvercomeSignatureClash();
                this.q = null;
                this.r = null;
                this.f3248s = qVar.isHiddenForResolutionEverywhereBesideSupercalls();
                this.t = new LinkedHashMap();
                this.u = null;
                this.v = false;
                this.a = z0Var;
                this.f3247b = mVar;
                this.c = zVar;
                this.d = uVar;
                this.f = aVar;
                this.g = list;
                this.h = q0Var;
                this.j = c0Var;
                this.k = eVar;
            } else {
                a(6);
                throw null;
            }
        }

        public static /* synthetic */ void a(int i) {
            String str;
            int i2;
            switch (i) {
                case 8:
                case 10:
                case 12:
                case 14:
                case 15:
                case 17:
                case 19:
                case 21:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 33:
                case 35:
                case 37:
                case 38:
                case 39:
                    str = "@NotNull method %s.%s must not return null";
                    break;
                case 9:
                case 11:
                case 13:
                case 16:
                case 18:
                case 20:
                case 22:
                case 32:
                case 34:
                case 36:
                default:
                    str = "Argument for @NotNull parameter '%s' of %s.%s must not be null";
                    break;
            }
            switch (i) {
                case 8:
                case 10:
                case 12:
                case 14:
                case 15:
                case 17:
                case 19:
                case 21:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 33:
                case 35:
                case 37:
                case 38:
                case 39:
                    i2 = 2;
                    break;
                case 9:
                case 11:
                case 13:
                case 16:
                case 18:
                case 20:
                case 22:
                case 32:
                case 34:
                case 36:
                default:
                    i2 = 3;
                    break;
            }
            Object[] objArr = new Object[i2];
            switch (i) {
                case 1:
                    objArr[0] = "newOwner";
                    break;
                case 2:
                    objArr[0] = "newModality";
                    break;
                case 3:
                    objArr[0] = "newVisibility";
                    break;
                case 4:
                case 13:
                    objArr[0] = "kind";
                    break;
                case 5:
                    objArr[0] = "newValueParameterDescriptors";
                    break;
                case 6:
                    objArr[0] = "newReturnType";
                    break;
                case 7:
                    objArr[0] = "owner";
                    break;
                case 8:
                case 10:
                case 12:
                case 14:
                case 15:
                case 17:
                case 19:
                case 21:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 33:
                case 35:
                case 37:
                case 38:
                case 39:
                    objArr[0] = "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration";
                    break;
                case 9:
                    objArr[0] = "modality";
                    break;
                case 11:
                    objArr[0] = "visibility";
                    break;
                case 16:
                    objArr[0] = ModelAuditLogEntry.CHANGE_KEY_NAME;
                    break;
                case 18:
                case 20:
                    objArr[0] = "parameters";
                    break;
                case 22:
                    objArr[0] = "type";
                    break;
                case 32:
                    objArr[0] = "additionalAnnotations";
                    break;
                case 34:
                default:
                    objArr[0] = "substitution";
                    break;
                case 36:
                    objArr[0] = "userDataKey";
                    break;
            }
            switch (i) {
                case 8:
                    objArr[1] = "setOwner";
                    break;
                case 9:
                case 11:
                case 13:
                case 16:
                case 18:
                case 20:
                case 22:
                case 32:
                case 34:
                case 36:
                default:
                    objArr[1] = "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration";
                    break;
                case 10:
                    objArr[1] = "setModality";
                    break;
                case 12:
                    objArr[1] = "setVisibility";
                    break;
                case 14:
                    objArr[1] = "setKind";
                    break;
                case 15:
                    objArr[1] = "setCopyOverrides";
                    break;
                case 17:
                    objArr[1] = "setName";
                    break;
                case 19:
                    objArr[1] = "setValueParameters";
                    break;
                case 21:
                    objArr[1] = "setTypeParameters";
                    break;
                case 23:
                    objArr[1] = "setReturnType";
                    break;
                case 24:
                    objArr[1] = "setExtensionReceiverParameter";
                    break;
                case 25:
                    objArr[1] = "setDispatchReceiverParameter";
                    break;
                case 26:
                    objArr[1] = "setOriginal";
                    break;
                case 27:
                    objArr[1] = "setSignatureChange";
                    break;
                case 28:
                    objArr[1] = "setPreserveSourceElement";
                    break;
                case 29:
                    objArr[1] = "setDropOriginalInContainingParts";
                    break;
                case 30:
                    objArr[1] = "setHiddenToOvercomeSignatureClash";
                    break;
                case 31:
                    objArr[1] = "setHiddenForResolutionEverywhereBesideSupercalls";
                    break;
                case 33:
                    objArr[1] = "setAdditionalAnnotations";
                    break;
                case 35:
                    objArr[1] = "setSubstitution";
                    break;
                case 37:
                    objArr[1] = "putUserData";
                    break;
                case 38:
                    objArr[1] = "getSubstitution";
                    break;
                case 39:
                    objArr[1] = "setJustForTypeSubstitution";
                    break;
            }
            switch (i) {
                case 7:
                    objArr[2] = "setOwner";
                    break;
                case 8:
                case 10:
                case 12:
                case 14:
                case 15:
                case 17:
                case 19:
                case 21:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 33:
                case 35:
                case 37:
                case 38:
                case 39:
                    break;
                case 9:
                    objArr[2] = "setModality";
                    break;
                case 11:
                    objArr[2] = "setVisibility";
                    break;
                case 13:
                    objArr[2] = "setKind";
                    break;
                case 16:
                    objArr[2] = "setName";
                    break;
                case 18:
                    objArr[2] = "setValueParameters";
                    break;
                case 20:
                    objArr[2] = "setTypeParameters";
                    break;
                case 22:
                    objArr[2] = "setReturnType";
                    break;
                case 32:
                    objArr[2] = "setAdditionalAnnotations";
                    break;
                case 34:
                    objArr[2] = "setSubstitution";
                    break;
                case 36:
                    objArr[2] = "putUserData";
                    break;
                default:
                    objArr[2] = HookHelper.constructorName;
                    break;
            }
            String format = String.format(str, objArr);
            switch (i) {
                case 8:
                case 10:
                case 12:
                case 14:
                case 15:
                case 17:
                case 19:
                case 21:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 33:
                case 35:
                case 37:
                case 38:
                case 39:
                    throw new IllegalStateException(format);
                case 9:
                case 11:
                case 13:
                case 16:
                case 18:
                case 20:
                case 22:
                case 32:
                case 34:
                case 36:
                default:
                    throw new IllegalArgumentException(format);
            }
        }

        @Override // d0.e0.p.d.m0.c.x.a
        public x build() {
            return this.w.c(this);
        }

        public c setHasSynthesizedParameterNames(boolean z2) {
            this.u = Boolean.valueOf(z2);
            return this;
        }

        public c setJustForTypeSubstitution(boolean z2) {
            this.v = z2;
            return this;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        /* renamed from: setAdditionalAnnotations */
        public x.a<x> setAdditionalAnnotations2(g gVar) {
            if (gVar != null) {
                this.r = gVar;
                return this;
            }
            a(32);
            throw null;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        /* renamed from: setCopyOverrides */
        public x.a<x> setCopyOverrides2(boolean z2) {
            this.l = z2;
            return this;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        /* renamed from: setDispatchReceiverParameter */
        public x.a<x> setDispatchReceiverParameter2(q0 q0Var) {
            this.i = q0Var;
            return this;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        /* renamed from: setDropOriginalInContainingParts */
        public x.a<x> setDropOriginalInContainingParts2() {
            this.o = true;
            return this;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        /* renamed from: setExtensionReceiverParameter */
        public x.a<x> setExtensionReceiverParameter2(q0 q0Var) {
            this.h = q0Var;
            return this;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        /* renamed from: setHiddenForResolutionEverywhereBesideSupercalls */
        public x.a<x> setHiddenForResolutionEverywhereBesideSupercalls2() {
            this.f3248s = true;
            return this;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        /* renamed from: setHiddenToOvercomeSignatureClash */
        public x.a<x> setHiddenToOvercomeSignatureClash2() {
            this.p = true;
            return this;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        /* renamed from: setKind */
        public x.a<x> setKind2(b.a aVar) {
            if (aVar != null) {
                this.f = aVar;
                return this;
            }
            a(13);
            throw null;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        /* renamed from: setModality */
        public x.a<x> setModality2(z zVar) {
            if (zVar != null) {
                this.c = zVar;
                return this;
            }
            a(9);
            throw null;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        /* renamed from: setName */
        public x.a<x> setName2(e eVar) {
            if (eVar != null) {
                this.k = eVar;
                return this;
            }
            a(16);
            throw null;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        /* renamed from: setOriginal */
        public x.a<x> setOriginal2(d0.e0.p.d.m0.c.b bVar) {
            this.e = (x) bVar;
            return this;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        /* renamed from: setOwner */
        public x.a<x> setOwner2(m mVar) {
            if (mVar != null) {
                this.f3247b = mVar;
                return this;
            }
            a(7);
            throw null;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        /* renamed from: setPreserveSourceElement */
        public x.a<x> setPreserveSourceElement2() {
            this.n = true;
            return this;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        /* renamed from: setReturnType */
        public x.a<x> setReturnType2(c0 c0Var) {
            if (c0Var != null) {
                this.j = c0Var;
                return this;
            }
            a(22);
            throw null;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        /* renamed from: setSignatureChange */
        public x.a<x> setSignatureChange2() {
            this.m = true;
            return this;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        /* renamed from: setSubstitution */
        public x.a<x> setSubstitution2(d0.e0.p.d.m0.n.z0 z0Var) {
            if (z0Var != null) {
                this.a = z0Var;
                return this;
            }
            a(34);
            throw null;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        public x.a<x> setTypeParameters(List<z0> list) {
            if (list != null) {
                this.q = list;
                return this;
            }
            a(20);
            throw null;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        public x.a<x> setValueParameters(List<c1> list) {
            if (list != null) {
                this.g = list;
                return this;
            }
            a(18);
            throw null;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        /* renamed from: setVisibility */
        public x.a<x> setVisibility2(u uVar) {
            if (uVar != null) {
                this.d = uVar;
                return this;
            }
            a(11);
            throw null;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public q(m mVar, x xVar, g gVar, e eVar, b.a aVar, u0 u0Var) {
        super(mVar, gVar, eVar, u0Var);
        if (mVar == null) {
            a(0);
            throw null;
        } else if (gVar == null) {
            a(1);
            throw null;
        } else if (eVar == null) {
            a(2);
            throw null;
        } else if (aVar == null) {
            a(3);
            throw null;
        } else if (u0Var != null) {
            this.t = t.i;
            this.u = false;
            this.v = false;
            this.w = false;
            this.f3244x = false;
            this.f3245y = false;
            this.f3246z = false;
            this.A = false;
            this.B = false;
            this.C = false;
            this.D = false;
            this.E = true;
            this.F = false;
            this.G = null;
            this.H = null;
            this.K = null;
            this.L = null;
            this.I = xVar == null ? this : xVar;
            this.J = aVar;
        } else {
            a(4);
            throw null;
        }
    }

    public static /* synthetic */ void a(int i) {
        String str;
        int i2;
        switch (i) {
            case 8:
            case 12:
            case 13:
            case 14:
            case 16:
            case 17:
            case 18:
            case 19:
            case 21:
            case 24:
            case 25:
                str = "@NotNull method %s.%s must not return null";
                break;
            case 9:
            case 10:
            case 11:
            case 15:
            case 20:
            case 22:
            case 23:
            default:
                str = "Argument for @NotNull parameter '%s' of %s.%s must not be null";
                break;
        }
        switch (i) {
            case 8:
            case 12:
            case 13:
            case 14:
            case 16:
            case 17:
            case 18:
            case 19:
            case 21:
            case 24:
            case 25:
                i2 = 2;
                break;
            case 9:
            case 10:
            case 11:
            case 15:
            case 20:
            case 22:
            case 23:
            default:
                i2 = 3;
                break;
        }
        Object[] objArr = new Object[i2];
        switch (i) {
            case 1:
                objArr[0] = "annotations";
                break;
            case 2:
                objArr[0] = ModelAuditLogEntry.CHANGE_KEY_NAME;
                break;
            case 3:
                objArr[0] = "kind";
                break;
            case 4:
                objArr[0] = "source";
                break;
            case 5:
                objArr[0] = "typeParameters";
                break;
            case 6:
            case 26:
            case 28:
                objArr[0] = "unsubstitutedValueParameters";
                break;
            case 7:
            case 9:
                objArr[0] = "visibility";
                break;
            case 8:
            case 12:
            case 13:
            case 14:
            case 16:
            case 17:
            case 18:
            case 19:
            case 21:
            case 24:
            case 25:
                objArr[0] = "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl";
                break;
            case 10:
                objArr[0] = "unsubstitutedReturnType";
                break;
            case 11:
                objArr[0] = "extensionReceiverParameter";
                break;
            case 15:
                objArr[0] = "overriddenDescriptors";
                break;
            case 20:
                objArr[0] = "originalSubstitutor";
                break;
            case 22:
            case 27:
            case 29:
                objArr[0] = "substitutor";
                break;
            case 23:
                objArr[0] = "configuration";
                break;
            default:
                objArr[0] = "containingDeclaration";
                break;
        }
        switch (i) {
            case 8:
                objArr[1] = "initialize";
                break;
            case 9:
            case 10:
            case 11:
            case 15:
            case 20:
            case 22:
            case 23:
            default:
                objArr[1] = "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl";
                break;
            case 12:
                objArr[1] = "getOverriddenDescriptors";
                break;
            case 13:
                objArr[1] = "getModality";
                break;
            case 14:
                objArr[1] = "getVisibility";
                break;
            case 16:
                objArr[1] = "getTypeParameters";
                break;
            case 17:
                objArr[1] = "getValueParameters";
                break;
            case 18:
                objArr[1] = "getOriginal";
                break;
            case 19:
                objArr[1] = "getKind";
                break;
            case 21:
                objArr[1] = "newCopyBuilder";
                break;
            case 24:
                objArr[1] = "copy";
                break;
            case 25:
                objArr[1] = "getSourceToUseForCopy";
                break;
        }
        switch (i) {
            case 5:
            case 6:
            case 7:
                objArr[2] = "initialize";
                break;
            case 8:
            case 12:
            case 13:
            case 14:
            case 16:
            case 17:
            case 18:
            case 19:
            case 21:
            case 24:
            case 25:
                break;
            case 9:
                objArr[2] = "setVisibility";
                break;
            case 10:
                objArr[2] = "setReturnType";
                break;
            case 11:
                objArr[2] = "setExtensionReceiverParameter";
                break;
            case 15:
                objArr[2] = "setOverriddenDescriptors";
                break;
            case 20:
                objArr[2] = "substitute";
                break;
            case 22:
                objArr[2] = "newCopyBuilder";
                break;
            case 23:
                objArr[2] = "doSubstitute";
                break;
            case 26:
            case 27:
            case 28:
            case 29:
                objArr[2] = "getSubstitutedValueParameters";
                break;
            default:
                objArr[2] = HookHelper.constructorName;
                break;
        }
        String format = String.format(str, objArr);
        switch (i) {
            case 8:
            case 12:
            case 13:
            case 14:
            case 16:
            case 17:
            case 18:
            case 19:
            case 21:
            case 24:
            case 25:
                throw new IllegalStateException(format);
            case 9:
            case 10:
            case 11:
            case 15:
            case 20:
            case 22:
            case 23:
            default:
                throw new IllegalArgumentException(format);
        }
    }

    public static List<c1> getSubstitutedValueParameters(x xVar, List<c1> list, d0.e0.p.d.m0.n.c1 c1Var) {
        if (list == null) {
            a(26);
            throw null;
        } else if (c1Var != null) {
            return getSubstitutedValueParameters(xVar, list, c1Var, false, false, null);
        } else {
            a(27);
            throw null;
        }
    }

    public <R, D> R accept(o<R, D> oVar, D d) {
        return oVar.visitFunctionDescriptor(this, d);
    }

    public abstract q b(m mVar, x xVar, b.a aVar, e eVar, g gVar, u0 u0Var);

    public x c(c cVar) {
        u0 u0Var;
        f0 f0Var;
        q0 q0Var;
        c0 substitute;
        boolean z2;
        if (cVar != null) {
            boolean[] zArr = new boolean[1];
            g composeAnnotations = cVar.r != null ? d0.e0.p.d.m0.c.g1.i.composeAnnotations(getAnnotations(), cVar.r) : getAnnotations();
            m mVar = cVar.f3247b;
            x xVar = cVar.e;
            b.a aVar = cVar.f;
            e eVar = cVar.k;
            if (cVar.n) {
                u0Var = (xVar != null ? xVar : getOriginal()).getSource();
            } else {
                u0Var = u0.a;
            }
            u0 u0Var2 = u0Var;
            if (u0Var2 != null) {
                q b2 = b(mVar, xVar, aVar, eVar, composeAnnotations, u0Var2);
                List<z0> list = cVar.q;
                if (list == null) {
                    list = getTypeParameters();
                }
                zArr[0] = zArr[0] | (!list.isEmpty());
                ArrayList arrayList = new ArrayList(list.size());
                d0.e0.p.d.m0.n.c1 substituteTypeParameters = d0.e0.p.d.m0.n.o.substituteTypeParameters(list, cVar.a, b2, arrayList, zArr);
                if (substituteTypeParameters == null) {
                    return null;
                }
                q0 q0Var2 = cVar.h;
                if (q0Var2 != null) {
                    c0 substitute2 = substituteTypeParameters.substitute(q0Var2.getType(), j1.IN_VARIANCE);
                    if (substitute2 == null) {
                        return null;
                    }
                    f0 f0Var2 = new f0(b2, new d0.e0.p.d.m0.k.a0.p.b(b2, substitute2, cVar.h.getValue()), cVar.h.getAnnotations());
                    zArr[0] = (substitute2 != cVar.h.getType()) | zArr[0];
                    f0Var = f0Var2;
                } else {
                    f0Var = null;
                }
                q0 q0Var3 = cVar.i;
                if (q0Var3 != null) {
                    q0 substitute3 = q0Var3.substitute(substituteTypeParameters);
                    if (substitute3 == null) {
                        return null;
                    }
                    zArr[0] = zArr[0] | (substitute3 != cVar.i);
                    q0Var = substitute3;
                } else {
                    q0Var = null;
                }
                List<c1> substitutedValueParameters = getSubstitutedValueParameters(b2, cVar.g, substituteTypeParameters, cVar.o, cVar.n, zArr);
                if (substitutedValueParameters == null || (substitute = substituteTypeParameters.substitute(cVar.j, j1.OUT_VARIANCE)) == null) {
                    return null;
                }
                zArr[0] = zArr[0] | (substitute != cVar.j);
                if (!zArr[0] && cVar.v) {
                    return this;
                }
                b2.initialize(f0Var, q0Var, arrayList, substitutedValueParameters, substitute, cVar.c, cVar.d);
                b2.setOperator(this.u);
                b2.setInfix(this.v);
                b2.setExternal(this.w);
                b2.setInline(this.f3244x);
                b2.setTailrec(this.f3245y);
                b2.setSuspend(this.D);
                b2.setExpect(this.f3246z);
                b2.setActual(this.A);
                b2.setHasStableParameterNames(this.E);
                b2.B = cVar.p;
                b2.C = cVar.f3248s;
                Boolean bool = cVar.u;
                if (bool != null) {
                    z2 = bool.booleanValue();
                } else {
                    z2 = this.F;
                }
                b2.setHasSynthesizedParameterNames(z2);
                if (!cVar.t.isEmpty() || this.L != null) {
                    Map<a.AbstractC0290a<?>, Object> map = cVar.t;
                    Map<a.AbstractC0290a<?>, Object> map2 = this.L;
                    if (map2 != null) {
                        for (Map.Entry<a.AbstractC0290a<?>, Object> entry : map2.entrySet()) {
                            if (!map.containsKey(entry.getKey())) {
                                map.put(entry.getKey(), entry.getValue());
                            }
                        }
                    }
                    if (map.size() == 1) {
                        b2.L = Collections.singletonMap(map.keySet().iterator().next(), map.values().iterator().next());
                    } else {
                        b2.L = map;
                    }
                }
                if (cVar.m || getInitialSignatureDescriptor() != null) {
                    b2.K = (getInitialSignatureDescriptor() != null ? getInitialSignatureDescriptor() : this).substitute(substituteTypeParameters);
                }
                if (cVar.l && !getOriginal().getOverriddenDescriptors().isEmpty()) {
                    if (cVar.a.isEmpty()) {
                        Function0<Collection<x>> function0 = this.H;
                        if (function0 != null) {
                            b2.H = function0;
                        } else {
                            b2.setOverriddenDescriptors(getOverriddenDescriptors());
                        }
                    } else {
                        b2.H = new a(substituteTypeParameters);
                    }
                }
                return b2;
            }
            a(25);
            throw null;
        }
        a(23);
        throw null;
    }

    public c d(d0.e0.p.d.m0.n.c1 c1Var) {
        if (c1Var != null) {
            return new c(this, c1Var.getSubstitution(), getContainingDeclaration(), getModality(), getVisibility(), getKind(), getValueParameters(), getExtensionReceiverParameter(), getReturnType(), null);
        }
        a(22);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.a
    public q0 getDispatchReceiverParameter() {
        return this.r;
    }

    @Override // d0.e0.p.d.m0.c.a
    public q0 getExtensionReceiverParameter() {
        return this.q;
    }

    @Override // d0.e0.p.d.m0.c.x
    public x getInitialSignatureDescriptor() {
        return this.K;
    }

    @Override // d0.e0.p.d.m0.c.b
    public b.a getKind() {
        b.a aVar = this.J;
        if (aVar != null) {
            return aVar;
        }
        a(19);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.y
    public z getModality() {
        z zVar = this.f3243s;
        if (zVar != null) {
            return zVar;
        }
        a(13);
        throw null;
    }

    public Collection<? extends x> getOverriddenDescriptors() {
        Function0<Collection<x>> function0 = this.H;
        if (function0 != null) {
            this.G = function0.invoke();
            this.H = null;
        }
        Collection<? extends x> collection = this.G;
        if (collection == null) {
            collection = Collections.emptyList();
        }
        if (collection != null) {
            return collection;
        }
        a(12);
        throw null;
    }

    public c0 getReturnType() {
        return this.p;
    }

    @Override // d0.e0.p.d.m0.c.a
    public List<z0> getTypeParameters() {
        List<z0> list = this.n;
        if (list != null) {
            return list;
        }
        throw new IllegalStateException("typeParameters == null for " + this);
    }

    @Override // d0.e0.p.d.m0.c.a
    public <V> V getUserData(a.AbstractC0290a<V> aVar) {
        Map<a.AbstractC0290a<?>, Object> map = this.L;
        if (map == null) {
            return null;
        }
        return (V) map.get(aVar);
    }

    @Override // d0.e0.p.d.m0.c.a
    public List<c1> getValueParameters() {
        List<c1> list = this.o;
        if (list != null) {
            return list;
        }
        a(17);
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.q, d0.e0.p.d.m0.c.y
    public u getVisibility() {
        u uVar = this.t;
        if (uVar != null) {
            return uVar;
        }
        a(14);
        throw null;
    }

    public boolean hasStableParameterNames() {
        return this.E;
    }

    @Override // d0.e0.p.d.m0.c.a
    public boolean hasSynthesizedParameterNames() {
        return this.F;
    }

    public q initialize(q0 q0Var, q0 q0Var2, List<? extends z0> list, List<c1> list2, c0 c0Var, z zVar, u uVar) {
        if (list == null) {
            a(5);
            throw null;
        } else if (list2 == null) {
            a(6);
            throw null;
        } else if (uVar != null) {
            this.n = d0.t.u.toList(list);
            this.o = d0.t.u.toList(list2);
            this.p = c0Var;
            this.f3243s = zVar;
            this.t = uVar;
            this.q = q0Var;
            this.r = q0Var2;
            for (int i = 0; i < list.size(); i++) {
                z0 z0Var = list.get(i);
                if (z0Var.getIndex() != i) {
                    throw new IllegalStateException(z0Var + " index is " + z0Var.getIndex() + " but position is " + i);
                }
            }
            for (int i2 = 0; i2 < list2.size(); i2++) {
                c1 c1Var = list2.get(i2);
                if (c1Var.getIndex() != i2 + 0) {
                    throw new IllegalStateException(c1Var + "index is " + c1Var.getIndex() + " but position is " + i2);
                }
            }
            return this;
        } else {
            a(7);
            throw null;
        }
    }

    @Override // d0.e0.p.d.m0.c.y
    public boolean isActual() {
        return this.A;
    }

    @Override // d0.e0.p.d.m0.c.y
    public boolean isExpect() {
        return this.f3246z;
    }

    public boolean isExternal() {
        return this.w;
    }

    @Override // d0.e0.p.d.m0.c.x
    public boolean isHiddenForResolutionEverywhereBesideSupercalls() {
        return this.C;
    }

    @Override // d0.e0.p.d.m0.c.x
    public boolean isHiddenToOvercomeSignatureClash() {
        return this.B;
    }

    @Override // d0.e0.p.d.m0.c.x
    public boolean isInfix() {
        if (this.v) {
            return true;
        }
        for (x xVar : getOriginal().getOverriddenDescriptors()) {
            if (xVar.isInfix()) {
                return true;
            }
        }
        return false;
    }

    public boolean isInline() {
        return this.f3244x;
    }

    @Override // d0.e0.p.d.m0.c.x
    public boolean isOperator() {
        if (this.u) {
            return true;
        }
        for (x xVar : getOriginal().getOverriddenDescriptors()) {
            if (xVar.isOperator()) {
                return true;
            }
        }
        return false;
    }

    @Override // d0.e0.p.d.m0.c.x
    public boolean isSuspend() {
        return this.D;
    }

    public boolean isTailrec() {
        return this.f3245y;
    }

    public x.a<? extends x> newCopyBuilder() {
        return d(d0.e0.p.d.m0.n.c1.a);
    }

    public <V> void putInUserDataMap(a.AbstractC0290a<V> aVar, Object obj) {
        if (this.L == null) {
            this.L = new LinkedHashMap();
        }
        this.L.put(aVar, obj);
    }

    public void setActual(boolean z2) {
        this.A = z2;
    }

    public void setExpect(boolean z2) {
        this.f3246z = z2;
    }

    public void setExternal(boolean z2) {
        this.w = z2;
    }

    public void setHasStableParameterNames(boolean z2) {
        this.E = z2;
    }

    public void setHasSynthesizedParameterNames(boolean z2) {
        this.F = z2;
    }

    public void setInfix(boolean z2) {
        this.v = z2;
    }

    public void setInline(boolean z2) {
        this.f3244x = z2;
    }

    public void setOperator(boolean z2) {
        this.u = z2;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public void setOverriddenDescriptors(Collection<? extends d0.e0.p.d.m0.c.b> collection) {
        if (collection != 0) {
            this.G = collection;
            Iterator it = collection.iterator();
            while (it.hasNext()) {
                if (((x) it.next()).isHiddenForResolutionEverywhereBesideSupercalls()) {
                    this.C = true;
                    return;
                }
            }
            return;
        }
        a(15);
        throw null;
    }

    public void setReturnType(c0 c0Var) {
        if (c0Var != null) {
            this.p = c0Var;
        } else {
            a(10);
            throw null;
        }
    }

    public void setSuspend(boolean z2) {
        this.D = z2;
    }

    public void setTailrec(boolean z2) {
        this.f3245y = z2;
    }

    public void setVisibility(u uVar) {
        if (uVar != null) {
            this.t = uVar;
        } else {
            a(9);
            throw null;
        }
    }

    @Override // d0.e0.p.d.m0.c.b
    public x copy(m mVar, z zVar, u uVar, b.a aVar, boolean z2) {
        x build = newCopyBuilder().setOwner(mVar).setModality(zVar).setVisibility(uVar).setKind(aVar).setCopyOverrides(z2).build();
        if (build != null) {
            return build;
        }
        a(24);
        throw null;
    }

    /* JADX WARN: Type inference failed for: r2v5, types: [d0.e0.p.d.m0.c.i1.q$c] */
    @Override // d0.e0.p.d.m0.c.x, d0.e0.p.d.m0.c.w0
    public x substitute(d0.e0.p.d.m0.n.c1 c1Var) {
        if (c1Var != null) {
            return c1Var.isEmpty() ? this : d(c1Var).setOriginal2((d0.e0.p.d.m0.c.b) getOriginal()).setPreserveSourceElement2().setJustForTypeSubstitution(true).build();
        }
        a(20);
        throw null;
    }

    public static List<c1> getSubstitutedValueParameters(x xVar, List<c1> list, d0.e0.p.d.m0.n.c1 c1Var, boolean z2, boolean z3, boolean[] zArr) {
        if (list == null) {
            a(28);
            throw null;
        } else if (c1Var != null) {
            ArrayList arrayList = new ArrayList(list.size());
            for (c1 c1Var2 : list) {
                c0 type = c1Var2.getType();
                j1 j1Var = j1.IN_VARIANCE;
                c0 substitute = c1Var.substitute(type, j1Var);
                c0 varargElementType = c1Var2.getVarargElementType();
                c0 substitute2 = varargElementType == null ? null : c1Var.substitute(varargElementType, j1Var);
                if (substitute == null) {
                    return null;
                }
                if (!((substitute == c1Var2.getType() && varargElementType == substitute2) || zArr == null)) {
                    zArr[0] = true;
                }
                arrayList.add(l0.createWithDestructuringDeclarations(xVar, z2 ? null : c1Var2, c1Var2.getIndex(), c1Var2.getAnnotations(), c1Var2.getName(), substitute, c1Var2.declaresDefaultValue(), c1Var2.isCrossinline(), c1Var2.isNoinline(), substitute2, z3 ? c1Var2.getSource() : u0.a, c1Var2 instanceof l0.b ? new b(((l0.b) c1Var2).getDestructuringVariables()) : null));
            }
            return arrayList;
        } else {
            a(29);
            throw null;
        }
    }

    @Override // d0.e0.p.d.m0.c.i1.l, d0.e0.p.d.m0.c.i1.k, d0.e0.p.d.m0.c.m
    public x getOriginal() {
        x xVar = this.I;
        x original = xVar == this ? this : xVar.getOriginal();
        if (original != null) {
            return original;
        }
        a(18);
        throw null;
    }
}
