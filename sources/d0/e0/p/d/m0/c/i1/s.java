package d0.e0.p.d.m0.c.i1;

import d0.e0.p.d.m0.c.e0;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.h0;
import d0.e0.p.d.m0.c.j0;
import d0.e0.p.d.m0.k.a0.b;
import d0.e0.p.d.m0.k.a0.h;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.m.j;
import d0.e0.p.d.m0.m.n;
import d0.t.u;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import d0.z.d.y;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.jvm.functions.Function0;
import kotlin.reflect.KProperty;
/* compiled from: LazyPackageViewDescriptorImpl.kt */
/* loaded from: classes3.dex */
public final class s extends k implements j0 {
    public static final /* synthetic */ KProperty<Object>[] l = {a0.property1(new y(a0.getOrCreateKotlinClass(s.class), "fragments", "getFragments()Ljava/util/List;"))};
    public final y m;
    public final d0.e0.p.d.m0.g.b n;
    public final j o;
    public final i p;

    /* compiled from: LazyPackageViewDescriptorImpl.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function0<List<? extends e0>> {
        public a() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final List<? extends e0> invoke() {
            return h0.packageFragments(s.this.getModule().getPackageFragmentProvider(), s.this.getFqName());
        }
    }

    /* compiled from: LazyPackageViewDescriptorImpl.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function0<i> {
        public b() {
            super(0);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final i invoke() {
            if (s.this.getFragments().isEmpty()) {
                return i.b.f3433b;
            }
            List<e0> fragments = s.this.getFragments();
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(fragments, 10));
            for (e0 e0Var : fragments) {
                arrayList.add(e0Var.getMemberScope());
            }
            List plus = u.plus((Collection<? extends h0>) arrayList, new h0(s.this.getModule(), s.this.getFqName()));
            b.a aVar = d0.e0.p.d.m0.k.a0.b.f3423b;
            StringBuilder R = b.d.b.a.a.R("package view scope for ");
            R.append(s.this.getFqName());
            R.append(" in ");
            R.append(s.this.getModule().getName());
            return aVar.create(R.toString(), plus);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public s(y yVar, d0.e0.p.d.m0.g.b bVar, d0.e0.p.d.m0.m.o oVar) {
        super(g.f.getEMPTY(), bVar.shortNameOrSpecial());
        m.checkNotNullParameter(yVar, "module");
        m.checkNotNullParameter(bVar, "fqName");
        m.checkNotNullParameter(oVar, "storageManager");
        this.m = yVar;
        this.n = bVar;
        this.o = oVar.createLazyValue(new a());
        this.p = new h(oVar, new b());
    }

    @Override // d0.e0.p.d.m0.c.m
    public <R, D> R accept(d0.e0.p.d.m0.c.o<R, D> oVar, D d) {
        m.checkNotNullParameter(oVar, "visitor");
        return oVar.visitPackageViewDescriptor(this, d);
    }

    public boolean equals(Object obj) {
        j0 j0Var = obj instanceof j0 ? (j0) obj : null;
        return j0Var != null && m.areEqual(getFqName(), j0Var.getFqName()) && m.areEqual(getModule(), j0Var.getModule());
    }

    @Override // d0.e0.p.d.m0.c.j0
    public d0.e0.p.d.m0.g.b getFqName() {
        return this.n;
    }

    @Override // d0.e0.p.d.m0.c.j0
    public List<e0> getFragments() {
        return (List) n.getValue(this.o, this, l[0]);
    }

    @Override // d0.e0.p.d.m0.c.j0
    public i getMemberScope() {
        return this.p;
    }

    public int hashCode() {
        return getFqName().hashCode() + (getModule().hashCode() * 31);
    }

    @Override // d0.e0.p.d.m0.c.j0
    public boolean isEmpty() {
        return j0.a.isEmpty(this);
    }

    @Override // d0.e0.p.d.m0.c.m
    public j0 getContainingDeclaration() {
        if (getFqName().isRoot()) {
            return null;
        }
        y module = getModule();
        d0.e0.p.d.m0.g.b parent = getFqName().parent();
        m.checkNotNullExpressionValue(parent, "fqName.parent()");
        return module.getPackage(parent);
    }

    @Override // d0.e0.p.d.m0.c.j0
    public y getModule() {
        return this.m;
    }
}
