package d0.e0.p.d.m0.c.i1;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.b.h;
import d0.e0.p.d.m0.c.d;
import d0.e0.p.d.m0.c.i1.j0;
import d0.e0.p.d.m0.c.u;
import d0.e0.p.d.m0.c.y0;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.l.b.e0.l;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.e1;
import d0.e0.p.d.m0.n.i1;
import d0.e0.p.d.m0.n.l1.g;
import d0.e0.p.d.m0.n.u0;
import d0.t.n;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.jvm.functions.Function1;
/* compiled from: AbstractTypeAliasDescriptor.kt */
/* loaded from: classes3.dex */
public abstract class e extends l implements y0 {
    public final u n;
    public List<? extends z0> o;
    public final b p = new b();

    /* compiled from: AbstractTypeAliasDescriptor.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<i1, Boolean> {
        public a() {
            super(1);
        }

        /* JADX WARN: Code restructure failed: missing block: B:10:0x002a, code lost:
            if (((r5 instanceof d0.e0.p.d.m0.c.z0) && !d0.z.d.m.areEqual(((d0.e0.p.d.m0.c.z0) r5).getContainingDeclaration(), r0)) != false) goto L12;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public final java.lang.Boolean invoke(d0.e0.p.d.m0.n.i1 r5) {
            /*
                r4 = this;
                java.lang.String r0 = "type"
                d0.z.d.m.checkNotNullExpressionValue(r5, r0)
                boolean r0 = d0.e0.p.d.m0.n.e0.isError(r5)
                r1 = 1
                r2 = 0
                if (r0 != 0) goto L2d
                d0.e0.p.d.m0.c.i1.e r0 = d0.e0.p.d.m0.c.i1.e.this
                d0.e0.p.d.m0.n.u0 r5 = r5.getConstructor()
                d0.e0.p.d.m0.c.h r5 = r5.getDeclarationDescriptor()
                boolean r3 = r5 instanceof d0.e0.p.d.m0.c.z0
                if (r3 == 0) goto L29
                d0.e0.p.d.m0.c.z0 r5 = (d0.e0.p.d.m0.c.z0) r5
                d0.e0.p.d.m0.c.m r5 = r5.getContainingDeclaration()
                boolean r5 = d0.z.d.m.areEqual(r5, r0)
                if (r5 != 0) goto L29
                r5 = 1
                goto L2a
            L29:
                r5 = 0
            L2a:
                if (r5 == 0) goto L2d
                goto L2e
            L2d:
                r1 = 0
            L2e:
                java.lang.Boolean r5 = java.lang.Boolean.valueOf(r1)
                return r5
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.c.i1.e.a.invoke(d0.e0.p.d.m0.n.i1):java.lang.Boolean");
        }
    }

    /* compiled from: AbstractTypeAliasDescriptor.kt */
    /* loaded from: classes3.dex */
    public static final class b implements u0 {
        public b() {
        }

        @Override // d0.e0.p.d.m0.n.u0
        public h getBuiltIns() {
            return d0.e0.p.d.m0.k.x.a.getBuiltIns(getDeclarationDescriptor());
        }

        @Override // d0.e0.p.d.m0.n.u0
        public List<z0> getParameters() {
            List list = ((l) e.this).f3470z;
            if (list != null) {
                return list;
            }
            m.throwUninitializedPropertyAccessException("typeConstructorParameters");
            throw null;
        }

        @Override // d0.e0.p.d.m0.n.u0
        public Collection<c0> getSupertypes() {
            Collection<c0> supertypes = getDeclarationDescriptor().getUnderlyingType().getConstructor().getSupertypes();
            m.checkNotNullExpressionValue(supertypes, "declarationDescriptor.underlyingType.constructor.supertypes");
            return supertypes;
        }

        @Override // d0.e0.p.d.m0.n.u0
        public boolean isDenotable() {
            return true;
        }

        @Override // d0.e0.p.d.m0.n.u0
        public u0 refine(g gVar) {
            m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
            return this;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("[typealias ");
            R.append(getDeclarationDescriptor().getName().asString());
            R.append(']');
            return R.toString();
        }

        @Override // d0.e0.p.d.m0.n.u0
        public y0 getDeclarationDescriptor() {
            return e.this;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public e(d0.e0.p.d.m0.c.m mVar, d0.e0.p.d.m0.c.g1.g gVar, d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.c.u0 u0Var, u uVar) {
        super(mVar, gVar, eVar, u0Var);
        m.checkNotNullParameter(mVar, "containingDeclaration");
        m.checkNotNullParameter(gVar, "annotations");
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(u0Var, "sourceElement");
        m.checkNotNullParameter(uVar, "visibilityImpl");
        this.n = uVar;
    }

    @Override // d0.e0.p.d.m0.c.m
    public <R, D> R accept(d0.e0.p.d.m0.c.o<R, D> oVar, D d) {
        m.checkNotNullParameter(oVar, "visitor");
        return oVar.visitTypeAliasDescriptor(this, d);
    }

    @Override // d0.e0.p.d.m0.c.i
    public List<z0> getDeclaredTypeParameters() {
        List list = this.o;
        if (list != null) {
            return list;
        }
        m.throwUninitializedPropertyAccessException("declaredTypeParametersImpl");
        throw null;
    }

    public final Collection<i0> getTypeAliasConstructors() {
        l lVar = (l) this;
        d0.e0.p.d.m0.c.e classDescriptor = lVar.getClassDescriptor();
        if (classDescriptor == null) {
            return n.emptyList();
        }
        Collection<d> constructors = classDescriptor.getConstructors();
        m.checkNotNullExpressionValue(constructors, "classDescriptor.constructors");
        ArrayList arrayList = new ArrayList();
        for (d dVar : constructors) {
            j0.a aVar = j0.M;
            d0.e0.p.d.m0.m.o oVar = lVar.q;
            m.checkNotNullExpressionValue(dVar, "it");
            i0 createIfAvailable = aVar.createIfAvailable(oVar, this, dVar);
            if (createIfAvailable != null) {
                arrayList.add(createIfAvailable);
            }
        }
        return arrayList;
    }

    @Override // d0.e0.p.d.m0.c.h
    public u0 getTypeConstructor() {
        return this.p;
    }

    @Override // d0.e0.p.d.m0.c.q, d0.e0.p.d.m0.c.y
    public u getVisibility() {
        return this.n;
    }

    public final void initialize(List<? extends z0> list) {
        m.checkNotNullParameter(list, "declaredTypeParameters");
        this.o = list;
    }

    @Override // d0.e0.p.d.m0.c.y
    public boolean isActual() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.y
    public boolean isExpect() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.y
    public boolean isExternal() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.i
    public boolean isInner() {
        return e1.contains(((l) this).getUnderlyingType(), new a());
    }

    @Override // d0.e0.p.d.m0.c.i1.k
    public String toString() {
        return m.stringPlus("typealias ", getName().asString());
    }

    @Override // d0.e0.p.d.m0.c.i1.l, d0.e0.p.d.m0.c.i1.k, d0.e0.p.d.m0.c.m
    public y0 getOriginal() {
        return (y0) super.getOriginal();
    }
}
