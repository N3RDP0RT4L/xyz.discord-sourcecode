package d0.e0.p.d.m0.c;

import d0.e0.p.d.m0.m.o;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.j1;
import d0.e0.p.d.m0.n.n1.m;
import d0.e0.p.d.m0.n.u0;
import java.util.List;
/* compiled from: TypeParameterDescriptor.java */
/* loaded from: classes3.dex */
public interface z0 extends h, m {
    int getIndex();

    @Override // d0.e0.p.d.m0.c.h, d0.e0.p.d.m0.c.m
    z0 getOriginal();

    o getStorageManager();

    @Override // d0.e0.p.d.m0.c.h
    u0 getTypeConstructor();

    List<c0> getUpperBounds();

    j1 getVariance();

    boolean isCapturedFromOuterDeclaration();

    boolean isReified();
}
