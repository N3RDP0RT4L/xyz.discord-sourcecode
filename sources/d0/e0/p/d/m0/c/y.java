package d0.e0.p.d.m0.c;
/* compiled from: MemberDescriptor.java */
/* loaded from: classes3.dex */
public interface y extends n, q {
    z getModality();

    u getVisibility();

    boolean isActual();

    boolean isExpect();

    boolean isExternal();
}
