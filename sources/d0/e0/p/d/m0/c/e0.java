package d0.e0.p.d.m0.c;

import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.k.a0.i;
/* compiled from: PackageFragmentDescriptor.kt */
/* loaded from: classes3.dex */
public interface e0 extends g {
    @Override // d0.e0.p.d.m0.c.n, d0.e0.p.d.m0.c.m
    c0 getContainingDeclaration();

    b getFqName();

    i getMemberScope();
}
