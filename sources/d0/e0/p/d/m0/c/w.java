package d0.e0.p.d.m0.c;

import d0.f0.n;
import d0.f0.q;
import d0.z.d.a0;
import d0.z.d.j;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.jvm.functions.Function1;
import kotlin.reflect.KDeclarationContainer;
/* compiled from: findClassInModule.kt */
/* loaded from: classes3.dex */
public final class w {

    /* compiled from: findClassInModule.kt */
    /* loaded from: classes3.dex */
    public /* synthetic */ class a extends j implements Function1<d0.e0.p.d.m0.g.a, d0.e0.p.d.m0.g.a> {
        public static final a j = new a();

        public a() {
            super(1);
        }

        @Override // d0.z.d.d, kotlin.reflect.KCallable
        public final String getName() {
            return "getOuterClassId";
        }

        @Override // d0.z.d.d
        public final KDeclarationContainer getOwner() {
            return a0.getOrCreateKotlinClass(d0.e0.p.d.m0.g.a.class);
        }

        @Override // d0.z.d.d
        public final String getSignature() {
            return "getOuterClassId()Lorg/jetbrains/kotlin/name/ClassId;";
        }

        public final d0.e0.p.d.m0.g.a invoke(d0.e0.p.d.m0.g.a aVar) {
            m.checkNotNullParameter(aVar, "p0");
            return aVar.getOuterClassId();
        }
    }

    /* compiled from: findClassInModule.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function1<d0.e0.p.d.m0.g.a, Integer> {
        public static final b j = new b();

        public b() {
            super(1);
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final int invoke2(d0.e0.p.d.m0.g.a aVar) {
            m.checkNotNullParameter(aVar, "it");
            return 0;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Integer invoke(d0.e0.p.d.m0.g.a aVar) {
            return Integer.valueOf(invoke2(aVar));
        }
    }

    public static final e findClassAcrossModuleDependencies(c0 c0Var, d0.e0.p.d.m0.g.a aVar) {
        m.checkNotNullParameter(c0Var, "<this>");
        m.checkNotNullParameter(aVar, "classId");
        h findClassifierAcrossModuleDependencies = findClassifierAcrossModuleDependencies(c0Var, aVar);
        if (findClassifierAcrossModuleDependencies instanceof e) {
            return (e) findClassifierAcrossModuleDependencies;
        }
        return null;
    }

    /* JADX WARN: Removed duplicated region for block: B:31:0x00e3  */
    /* JADX WARN: Removed duplicated region for block: B:43:0x0142  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final d0.e0.p.d.m0.c.h findClassifierAcrossModuleDependencies(d0.e0.p.d.m0.c.c0 r11, d0.e0.p.d.m0.g.a r12) {
        /*
            Method dump skipped, instructions count: 324
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.c.w.findClassifierAcrossModuleDependencies(d0.e0.p.d.m0.c.c0, d0.e0.p.d.m0.g.a):d0.e0.p.d.m0.c.h");
    }

    public static final e findNonGenericClassAcrossDependencies(c0 c0Var, d0.e0.p.d.m0.g.a aVar, d0 d0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        m.checkNotNullParameter(aVar, "classId");
        m.checkNotNullParameter(d0Var, "notFoundClasses");
        e findClassAcrossModuleDependencies = findClassAcrossModuleDependencies(c0Var, aVar);
        return findClassAcrossModuleDependencies != null ? findClassAcrossModuleDependencies : d0Var.getClass(aVar, q.toList(q.map(n.generateSequence(aVar, a.j), b.j)));
    }

    public static final y0 findTypeAliasAcrossModuleDependencies(c0 c0Var, d0.e0.p.d.m0.g.a aVar) {
        m.checkNotNullParameter(c0Var, "<this>");
        m.checkNotNullParameter(aVar, "classId");
        h findClassifierAcrossModuleDependencies = findClassifierAcrossModuleDependencies(c0Var, aVar);
        if (findClassifierAcrossModuleDependencies instanceof y0) {
            return (y0) findClassifierAcrossModuleDependencies;
        }
        return null;
    }
}
