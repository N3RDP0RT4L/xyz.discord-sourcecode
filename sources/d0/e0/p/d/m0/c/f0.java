package d0.e0.p.d.m0.c;

import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.e;
import java.util.Collection;
import java.util.List;
import kotlin.jvm.functions.Function1;
/* compiled from: PackageFragmentProvider.kt */
/* loaded from: classes3.dex */
public interface f0 {
    List<e0> getPackageFragments(b bVar);

    Collection<b> getSubPackagesOf(b bVar, Function1<? super e, Boolean> function1);
}
