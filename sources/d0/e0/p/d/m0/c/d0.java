package d0.e0.p.d.m0.c;

import androidx.exifinterface.media.ExifInterface;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.d0.f;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.i1.k0;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.m.f;
import d0.e0.p.d.m0.m.h;
import d0.e0.p.d.m0.m.o;
import d0.e0.p.d.m0.n.i;
import d0.e0.p.d.m0.n.j1;
import d0.t.c0;
import d0.t.m0;
import d0.t.n;
import d0.t.n0;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.jvm.functions.Function1;
import kotlin.ranges.IntRange;
/* compiled from: NotFoundClasses.kt */
/* loaded from: classes3.dex */
public final class d0 {
    public final o a;

    /* renamed from: b  reason: collision with root package name */
    public final c0 f3215b;
    public final h<d0.e0.p.d.m0.g.b, e0> c;
    public final h<a, e> d;

    /* compiled from: NotFoundClasses.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public final d0.e0.p.d.m0.g.a a;

        /* renamed from: b  reason: collision with root package name */
        public final List<Integer> f3216b;

        public a(d0.e0.p.d.m0.g.a aVar, List<Integer> list) {
            m.checkNotNullParameter(aVar, "classId");
            m.checkNotNullParameter(list, "typeParametersCount");
            this.a = aVar;
            this.f3216b = list;
        }

        public final d0.e0.p.d.m0.g.a component1() {
            return this.a;
        }

        public final List<Integer> component2() {
            return this.f3216b;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return m.areEqual(this.a, aVar.a) && m.areEqual(this.f3216b, aVar.f3216b);
        }

        public int hashCode() {
            return this.f3216b.hashCode() + (this.a.hashCode() * 31);
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("ClassRequest(classId=");
            R.append(this.a);
            R.append(", typeParametersCount=");
            R.append(this.f3216b);
            R.append(')');
            return R.toString();
        }
    }

    /* compiled from: NotFoundClasses.kt */
    /* loaded from: classes3.dex */
    public static final class b extends d0.e0.p.d.m0.c.i1.h {
        public final boolean r;

        /* renamed from: s  reason: collision with root package name */
        public final List<z0> f3217s;
        public final i t;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(o oVar, m mVar, e eVar, boolean z2, int i) {
            super(oVar, mVar, eVar, u0.a, false);
            m.checkNotNullParameter(oVar, "storageManager");
            m.checkNotNullParameter(mVar, "container");
            m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
            this.r = z2;
            IntRange until = f.until(0, i);
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(until, 10));
            Iterator<Integer> it = until.iterator();
            while (it.hasNext()) {
                int nextInt = ((c0) it).nextInt();
                arrayList.add(k0.createWithDefaultBound(this, g.f.getEMPTY(), false, j1.INVARIANT, e.identifier(m.stringPlus(ExifInterface.GPS_DIRECTION_TRUE, Integer.valueOf(nextInt))), nextInt, oVar));
            }
            this.f3217s = arrayList;
            this.t = new i(this, a1.computeConstructorTypeParameters(this), m0.setOf(d0.e0.p.d.m0.k.x.a.getModule(this).getBuiltIns().getAnyType()), oVar);
        }

        @Override // d0.e0.p.d.m0.c.g1.a
        public g getAnnotations() {
            return g.f.getEMPTY();
        }

        @Override // d0.e0.p.d.m0.c.e
        public e getCompanionObjectDescriptor() {
            return null;
        }

        @Override // d0.e0.p.d.m0.c.e
        public Collection<d0.e0.p.d.m0.c.d> getConstructors() {
            return n0.emptySet();
        }

        @Override // d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.i
        public List<z0> getDeclaredTypeParameters() {
            return this.f3217s;
        }

        @Override // d0.e0.p.d.m0.c.e
        public f getKind() {
            return f.CLASS;
        }

        @Override // d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.y
        public z getModality() {
            return z.FINAL;
        }

        @Override // d0.e0.p.d.m0.c.e
        public Collection<e> getSealedSubclasses() {
            return n.emptyList();
        }

        @Override // d0.e0.p.d.m0.c.i1.u
        public d0.e0.p.d.m0.k.a0.i getUnsubstitutedMemberScope(d0.e0.p.d.m0.n.l1.g gVar) {
            m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
            return i.b.f3433b;
        }

        @Override // d0.e0.p.d.m0.c.e
        public d0.e0.p.d.m0.c.d getUnsubstitutedPrimaryConstructor() {
            return null;
        }

        @Override // d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.q, d0.e0.p.d.m0.c.y
        public u getVisibility() {
            u uVar = t.e;
            m.checkNotNullExpressionValue(uVar, "PUBLIC");
            return uVar;
        }

        @Override // d0.e0.p.d.m0.c.y
        public boolean isActual() {
            return false;
        }

        @Override // d0.e0.p.d.m0.c.e
        public boolean isCompanionObject() {
            return false;
        }

        @Override // d0.e0.p.d.m0.c.e
        public boolean isData() {
            return false;
        }

        @Override // d0.e0.p.d.m0.c.y
        public boolean isExpect() {
            return false;
        }

        @Override // d0.e0.p.d.m0.c.i1.h, d0.e0.p.d.m0.c.y
        public boolean isExternal() {
            return false;
        }

        @Override // d0.e0.p.d.m0.c.e
        public boolean isFun() {
            return false;
        }

        @Override // d0.e0.p.d.m0.c.e
        public boolean isInline() {
            return false;
        }

        @Override // d0.e0.p.d.m0.c.i
        public boolean isInner() {
            return this.r;
        }

        @Override // d0.e0.p.d.m0.c.e
        public boolean isValue() {
            return false;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("class ");
            R.append(getName());
            R.append(" (not found)");
            return R.toString();
        }

        @Override // d0.e0.p.d.m0.c.e
        public i.b getStaticScope() {
            return i.b.f3433b;
        }

        @Override // d0.e0.p.d.m0.c.h
        public d0.e0.p.d.m0.n.i getTypeConstructor() {
            return this.t;
        }
    }

    /* compiled from: NotFoundClasses.kt */
    /* loaded from: classes3.dex */
    public static final class c extends d0.z.d.o implements Function1<a, e> {
        public c() {
            super(1);
        }

        public final e invoke(a aVar) {
            m.checkNotNullParameter(aVar, "$dstr$classId$typeParametersCount");
            d0.e0.p.d.m0.g.a component1 = aVar.component1();
            List<Integer> component2 = aVar.component2();
            if (!component1.isLocal()) {
                d0.e0.p.d.m0.g.a outerClassId = component1.getOuterClassId();
                g gVar = outerClassId == null ? null : d0.this.getClass(outerClassId, u.drop(component2, 1));
                if (gVar == null) {
                    h hVar = d0.this.c;
                    d0.e0.p.d.m0.g.b packageFqName = component1.getPackageFqName();
                    m.checkNotNullExpressionValue(packageFqName, "classId.packageFqName");
                    gVar = (g) ((f.m) hVar).invoke(packageFqName);
                }
                g gVar2 = gVar;
                boolean isNestedClass = component1.isNestedClass();
                o oVar = d0.this.a;
                e shortClassName = component1.getShortClassName();
                m.checkNotNullExpressionValue(shortClassName, "classId.shortClassName");
                Integer num = (Integer) u.firstOrNull((List<? extends Object>) component2);
                return new b(oVar, gVar2, shortClassName, isNestedClass, num == null ? 0 : num.intValue());
            }
            throw new UnsupportedOperationException(m.stringPlus("Unresolved local class: ", component1));
        }
    }

    /* compiled from: NotFoundClasses.kt */
    /* loaded from: classes3.dex */
    public static final class d extends d0.z.d.o implements Function1<d0.e0.p.d.m0.g.b, e0> {
        public d() {
            super(1);
        }

        public final e0 invoke(d0.e0.p.d.m0.g.b bVar) {
            m.checkNotNullParameter(bVar, "fqName");
            return new d0.e0.p.d.m0.c.i1.n(d0.this.f3215b, bVar);
        }
    }

    public d0(o oVar, c0 c0Var) {
        m.checkNotNullParameter(oVar, "storageManager");
        m.checkNotNullParameter(c0Var, "module");
        this.a = oVar;
        this.f3215b = c0Var;
        this.c = oVar.createMemoizedFunction(new d());
        this.d = oVar.createMemoizedFunction(new c());
    }

    public final e getClass(d0.e0.p.d.m0.g.a aVar, List<Integer> list) {
        m.checkNotNullParameter(aVar, "classId");
        m.checkNotNullParameter(list, "typeParametersCount");
        return (e) ((f.m) this.d).invoke(new a(aVar, list));
    }
}
