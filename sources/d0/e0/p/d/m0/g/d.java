package d0.e0.p.d.m0.g;

import d0.g0.t;
import d0.z.d.m;
/* compiled from: FqNamesUtil.kt */
/* loaded from: classes3.dex */
public final class d {
    public static final boolean isSubpackageOf(b bVar, b bVar2) {
        m.checkNotNullParameter(bVar, "<this>");
        m.checkNotNullParameter(bVar2, "packageName");
        if (m.areEqual(bVar, bVar2) || bVar2.isRoot()) {
            return true;
        }
        String asString = bVar.asString();
        m.checkNotNullExpressionValue(asString, "this.asString()");
        String asString2 = bVar2.asString();
        m.checkNotNullExpressionValue(asString2, "packageName.asString()");
        return t.startsWith$default(asString, asString2, false, 2, null) && asString.charAt(asString2.length()) == '.';
    }

    public static final boolean isValidJavaFqName(String str) {
        h hVar = h.AFTER_DOT;
        if (str == null) {
            return false;
        }
        h hVar2 = h.BEGINNING;
        int i = 0;
        while (i < str.length()) {
            char charAt = str.charAt(i);
            i++;
            int ordinal = hVar2.ordinal();
            if (ordinal != 0) {
                if (ordinal != 1) {
                    if (ordinal != 2) {
                        continue;
                    }
                } else if (charAt == '.') {
                    hVar2 = hVar;
                } else if (!Character.isJavaIdentifierPart(charAt)) {
                    return false;
                }
            }
            if (!Character.isJavaIdentifierPart(charAt)) {
                return false;
            }
            hVar2 = h.MIDDLE;
        }
        return hVar2 != hVar;
    }

    public static final b tail(b bVar, b bVar2) {
        m.checkNotNullParameter(bVar, "<this>");
        m.checkNotNullParameter(bVar2, "prefix");
        if (!isSubpackageOf(bVar, bVar2) || bVar2.isRoot()) {
            return bVar;
        }
        if (m.areEqual(bVar, bVar2)) {
            b bVar3 = b.a;
            m.checkNotNullExpressionValue(bVar3, "ROOT");
            return bVar3;
        }
        String asString = bVar.asString();
        m.checkNotNullExpressionValue(asString, "asString()");
        String substring = asString.substring(bVar2.asString().length() + 1);
        m.checkNotNullExpressionValue(substring, "(this as java.lang.String).substring(startIndex)");
        return new b(substring);
    }
}
