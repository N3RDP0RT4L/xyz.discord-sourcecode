package d0.e0.p.d.m0.g;

import com.discord.models.domain.ModelAuditLogEntry;
/* compiled from: SpecialNames.java */
/* loaded from: classes3.dex */
public class g {
    public static final e a = e.special("<no name provided>");

    /* renamed from: b  reason: collision with root package name */
    public static final e f3397b = e.identifier("Companion");
    public static final e c = e.identifier("no_name_in_PSI_3d19d79d_1ba9_4cd0_b7f5_b46aa3cd5d40");

    static {
        e.special("<root package>");
        e.special("<anonymous>");
    }

    public static /* synthetic */ void a(int i) {
        String str = i != 1 ? "@NotNull method %s.%s must not return null" : "Argument for @NotNull parameter '%s' of %s.%s must not be null";
        Object[] objArr = new Object[i != 1 ? 2 : 3];
        if (i != 1) {
            objArr[0] = "kotlin/reflect/jvm/internal/impl/name/SpecialNames";
        } else {
            objArr[0] = ModelAuditLogEntry.CHANGE_KEY_NAME;
        }
        if (i != 1) {
            objArr[1] = "safeIdentifier";
        } else {
            objArr[1] = "kotlin/reflect/jvm/internal/impl/name/SpecialNames";
        }
        if (i == 1) {
            objArr[2] = "isSafeIdentifier";
        }
        String format = String.format(str, objArr);
        if (i == 1) {
            throw new IllegalArgumentException(format);
        }
    }

    public static boolean isSafeIdentifier(e eVar) {
        if (eVar != null) {
            return !eVar.asString().isEmpty() && !eVar.isSpecial();
        }
        a(1);
        throw null;
    }

    public static e safeIdentifier(e eVar) {
        if (eVar == null || eVar.isSpecial()) {
            eVar = c;
        }
        if (eVar != null) {
            return eVar;
        }
        a(0);
        throw null;
    }
}
