package d0.e0.p.d.m0.g;

import andhook.lib.HookHelper;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.t.k;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import kotlin.jvm.functions.Function1;
/* compiled from: FqNameUnsafe.java */
/* loaded from: classes3.dex */
public final class c {
    public static final e a = e.special("<root>");

    /* renamed from: b  reason: collision with root package name */
    public static final Pattern f3396b = Pattern.compile("\\.");
    public static final Function1<String, e> c = new a();
    public final String d;
    public transient b e;
    public transient c f;
    public transient e g;

    /* compiled from: FqNameUnsafe.java */
    /* loaded from: classes3.dex */
    public static class a implements Function1<String, e> {
        public e invoke(String str) {
            return e.guessByFirstCharacter(str);
        }
    }

    public c(String str, b bVar) {
        if (str != null) {
            this.d = str;
            this.e = bVar;
            return;
        }
        a(0);
        throw null;
    }

    public static /* synthetic */ void a(int i) {
        String str;
        int i2;
        switch (i) {
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 17:
                str = "@NotNull method %s.%s must not return null";
                break;
            case 9:
            case 15:
            case 16:
            default:
                str = "Argument for @NotNull parameter '%s' of %s.%s must not be null";
                break;
        }
        switch (i) {
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 17:
                i2 = 2;
                break;
            case 9:
            case 15:
            case 16:
            default:
                i2 = 3;
                break;
        }
        Object[] objArr = new Object[i2];
        if (i != 1) {
            switch (i) {
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 17:
                    objArr[0] = "kotlin/reflect/jvm/internal/impl/name/FqNameUnsafe";
                    break;
                case 9:
                    objArr[0] = ModelAuditLogEntry.CHANGE_KEY_NAME;
                    break;
                case 15:
                    objArr[0] = "segment";
                    break;
                case 16:
                    objArr[0] = "shortName";
                    break;
                default:
                    objArr[0] = "fqName";
                    break;
            }
        } else {
            objArr[0] = "safe";
        }
        switch (i) {
            case 4:
                objArr[1] = "asString";
                break;
            case 5:
            case 6:
                objArr[1] = "toSafe";
                break;
            case 7:
            case 8:
                objArr[1] = "parent";
                break;
            case 9:
            case 15:
            case 16:
            default:
                objArr[1] = "kotlin/reflect/jvm/internal/impl/name/FqNameUnsafe";
                break;
            case 10:
            case 11:
                objArr[1] = "shortName";
                break;
            case 12:
            case 13:
                objArr[1] = "shortNameOrSpecial";
                break;
            case 14:
                objArr[1] = "pathSegments";
                break;
            case 17:
                objArr[1] = "toString";
                break;
        }
        switch (i) {
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 17:
                break;
            case 9:
                objArr[2] = "child";
                break;
            case 15:
                objArr[2] = "startsWith";
                break;
            case 16:
                objArr[2] = "topLevel";
                break;
            default:
                objArr[2] = HookHelper.constructorName;
                break;
        }
        String format = String.format(str, objArr);
        switch (i) {
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 17:
                throw new IllegalStateException(format);
            case 9:
            case 15:
            case 16:
            default:
                throw new IllegalArgumentException(format);
        }
    }

    public static c topLevel(e eVar) {
        if (eVar != null) {
            return new c(eVar.asString(), b.a.toUnsafe(), eVar);
        }
        a(16);
        throw null;
    }

    public String asString() {
        String str = this.d;
        if (str != null) {
            return str;
        }
        a(4);
        throw null;
    }

    public final void b() {
        int lastIndexOf = this.d.lastIndexOf(46);
        if (lastIndexOf >= 0) {
            this.g = e.guessByFirstCharacter(this.d.substring(lastIndexOf + 1));
            this.f = new c(this.d.substring(0, lastIndexOf));
            return;
        }
        this.g = e.guessByFirstCharacter(this.d);
        this.f = b.a.toUnsafe();
    }

    public c child(e eVar) {
        String str;
        if (eVar != null) {
            if (isRoot()) {
                str = eVar.asString();
            } else {
                str = this.d + "." + eVar.asString();
            }
            return new c(str, this, eVar);
        }
        a(9);
        throw null;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof c) && this.d.equals(((c) obj).d);
    }

    public int hashCode() {
        return this.d.hashCode();
    }

    public boolean isRoot() {
        return this.d.isEmpty();
    }

    public boolean isSafe() {
        return this.e != null || asString().indexOf(60) < 0;
    }

    public c parent() {
        c cVar = this.f;
        if (cVar != null) {
            if (cVar != null) {
                return cVar;
            }
            a(7);
            throw null;
        } else if (!isRoot()) {
            b();
            c cVar2 = this.f;
            if (cVar2 != null) {
                return cVar2;
            }
            a(8);
            throw null;
        } else {
            throw new IllegalStateException("root");
        }
    }

    public List<e> pathSegments() {
        List<e> emptyList = isRoot() ? Collections.emptyList() : k.map(f3396b.split(this.d), c);
        if (emptyList != null) {
            return emptyList;
        }
        a(14);
        throw null;
    }

    public e shortName() {
        e eVar = this.g;
        if (eVar != null) {
            if (eVar != null) {
                return eVar;
            }
            a(10);
            throw null;
        } else if (!isRoot()) {
            b();
            e eVar2 = this.g;
            if (eVar2 != null) {
                return eVar2;
            }
            a(11);
            throw null;
        } else {
            throw new IllegalStateException("root");
        }
    }

    public e shortNameOrSpecial() {
        if (isRoot()) {
            e eVar = a;
            if (eVar != null) {
                return eVar;
            }
            a(12);
            throw null;
        }
        e shortName = shortName();
        if (shortName != null) {
            return shortName;
        }
        a(13);
        throw null;
    }

    public boolean startsWith(e eVar) {
        if (eVar == null) {
            a(15);
            throw null;
        } else if (isRoot()) {
            return false;
        } else {
            int indexOf = this.d.indexOf(46);
            String str = this.d;
            String asString = eVar.asString();
            if (indexOf == -1) {
                indexOf = this.d.length();
            }
            return str.regionMatches(0, asString, 0, indexOf);
        }
    }

    public b toSafe() {
        b bVar = this.e;
        if (bVar == null) {
            b bVar2 = new b(this);
            this.e = bVar2;
            if (bVar2 != null) {
                return bVar2;
            }
            a(6);
            throw null;
        } else if (bVar != null) {
            return bVar;
        } else {
            a(5);
            throw null;
        }
    }

    public String toString() {
        String asString = isRoot() ? a.asString() : this.d;
        if (asString != null) {
            return asString;
        }
        a(17);
        throw null;
    }

    public c(String str) {
        if (str != null) {
            this.d = str;
        } else {
            a(2);
            throw null;
        }
    }

    public c(String str, c cVar, e eVar) {
        if (str != null) {
            this.d = str;
            this.f = cVar;
            this.g = eVar;
            return;
        }
        a(3);
        throw null;
    }
}
