package d0.e0.p.d.m0.g;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
/* compiled from: Name.java */
/* loaded from: classes3.dex */
public final class e implements Comparable<e> {
    public final String j;
    public final boolean k;

    public e(String str, boolean z2) {
        if (str != null) {
            this.j = str;
            this.k = z2;
            return;
        }
        f(0);
        throw null;
    }

    public static /* synthetic */ void f(int i) {
        String str = (i == 1 || i == 2) ? "@NotNull method %s.%s must not return null" : "Argument for @NotNull parameter '%s' of %s.%s must not be null";
        Object[] objArr = new Object[(i == 1 || i == 2) ? 2 : 3];
        if (i == 1 || i == 2) {
            objArr[0] = "kotlin/reflect/jvm/internal/impl/name/Name";
        } else {
            objArr[0] = ModelAuditLogEntry.CHANGE_KEY_NAME;
        }
        if (i == 1) {
            objArr[1] = "asString";
        } else if (i != 2) {
            objArr[1] = "kotlin/reflect/jvm/internal/impl/name/Name";
        } else {
            objArr[1] = "getIdentifier";
        }
        switch (i) {
            case 1:
            case 2:
                break;
            case 3:
                objArr[2] = "identifier";
                break;
            case 4:
                objArr[2] = "isValidIdentifier";
                break;
            case 5:
                objArr[2] = "special";
                break;
            case 6:
                objArr[2] = "guessByFirstCharacter";
                break;
            default:
                objArr[2] = HookHelper.constructorName;
                break;
        }
        String format = String.format(str, objArr);
        if (i == 1 || i == 2) {
            throw new IllegalStateException(format);
        }
    }

    public static e guessByFirstCharacter(String str) {
        if (str == null) {
            f(6);
            throw null;
        } else if (str.startsWith("<")) {
            return special(str);
        } else {
            return identifier(str);
        }
    }

    public static e identifier(String str) {
        if (str != null) {
            return new e(str, false);
        }
        f(3);
        throw null;
    }

    public static boolean isValidIdentifier(String str) {
        if (str == null) {
            f(4);
            throw null;
        } else if (str.isEmpty() || str.startsWith("<")) {
            return false;
        } else {
            for (int i = 0; i < str.length(); i++) {
                char charAt = str.charAt(i);
                if (charAt == '.' || charAt == '/' || charAt == '\\') {
                    return false;
                }
            }
            return true;
        }
    }

    public static e special(String str) {
        if (str == null) {
            f(5);
            throw null;
        } else if (str.startsWith("<")) {
            return new e(str, true);
        } else {
            throw new IllegalArgumentException(a.v("special name must start with '<': ", str));
        }
    }

    public String asString() {
        String str = this.j;
        if (str != null) {
            return str;
        }
        f(1);
        throw null;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof e)) {
            return false;
        }
        e eVar = (e) obj;
        return this.k == eVar.k && this.j.equals(eVar.j);
    }

    public String getIdentifier() {
        if (!this.k) {
            String asString = asString();
            if (asString != null) {
                return asString;
            }
            f(2);
            throw null;
        }
        throw new IllegalStateException("not identifier: " + this);
    }

    public int hashCode() {
        return (this.j.hashCode() * 31) + (this.k ? 1 : 0);
    }

    public boolean isSpecial() {
        return this.k;
    }

    public String toString() {
        return this.j;
    }

    public int compareTo(e eVar) {
        return this.j.compareTo(eVar.j);
    }
}
