package d0.e0.p.d.m0.b;

import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.g.a;
import d0.t.u;
import d0.z.d.m;
import java.util.Set;
/* compiled from: CompanionObjectMappingUtils.kt */
/* loaded from: classes3.dex */
public final class d {
    public static final boolean isMappedIntrinsicCompanionObject(c cVar, e eVar) {
        m.checkNotNullParameter(cVar, "<this>");
        m.checkNotNullParameter(eVar, "classDescriptor");
        if (d0.e0.p.d.m0.k.e.isCompanionObject(eVar)) {
            Set<a> classIds = cVar.getClassIds();
            a classId = d0.e0.p.d.m0.k.x.a.getClassId(eVar);
            if (u.contains(classIds, classId == null ? null : classId.getOuterClassId())) {
                return true;
            }
        }
        return false;
    }
}
