package d0.e0.p.d.m0.b;

import d0.e0.p.d.m0.g.e;
import d0.g;
import d0.t.n0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Set;
import kotlin.Lazy;
import kotlin.jvm.functions.Function0;
/* compiled from: PrimitiveType.kt */
/* loaded from: classes3.dex */
public enum i {
    BOOLEAN("Boolean"),
    CHAR("Char"),
    BYTE("Byte"),
    SHORT("Short"),
    INT("Int"),
    FLOAT("Float"),
    LONG("Long"),
    DOUBLE("Double");
    
    public static final Set<i> j;
    private final Lazy arrayTypeFqName$delegate;
    private final e arrayTypeName;
    private final Lazy typeFqName$delegate;
    private final e typeName;

    /* compiled from: PrimitiveType.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function0<d0.e0.p.d.m0.g.b> {
        public b() {
            super(0);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final d0.e0.p.d.m0.g.b invoke() {
            d0.e0.p.d.m0.g.b child = k.l.child(i.this.getArrayTypeName());
            m.checkNotNullExpressionValue(child, "BUILT_INS_PACKAGE_FQ_NAME.child(arrayTypeName)");
            return child;
        }
    }

    /* compiled from: PrimitiveType.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function0<d0.e0.p.d.m0.g.b> {
        public c() {
            super(0);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final d0.e0.p.d.m0.g.b invoke() {
            d0.e0.p.d.m0.g.b child = k.l.child(i.this.getTypeName());
            m.checkNotNullExpressionValue(child, "BUILT_INS_PACKAGE_FQ_NAME.child(this.typeName)");
            return child;
        }
    }

    static {
        i iVar = CHAR;
        i iVar2 = BYTE;
        i iVar3 = SHORT;
        i iVar4 = INT;
        i iVar5 = FLOAT;
        i iVar6 = LONG;
        i iVar7 = DOUBLE;
        new Object(null) { // from class: d0.e0.p.d.m0.b.i.a
        };
        j = n0.setOf((Object[]) new i[]{iVar, iVar2, iVar3, iVar4, iVar5, iVar6, iVar7});
    }

    i(String str) {
        e identifier = e.identifier(str);
        m.checkNotNullExpressionValue(identifier, "identifier(typeName)");
        this.typeName = identifier;
        e identifier2 = e.identifier(m.stringPlus(str, "Array"));
        m.checkNotNullExpressionValue(identifier2, "identifier(\"${typeName}Array\")");
        this.arrayTypeName = identifier2;
        d0.i iVar = d0.i.PUBLICATION;
        this.typeFqName$delegate = g.lazy(iVar, new c());
        this.arrayTypeFqName$delegate = g.lazy(iVar, new b());
    }

    public final d0.e0.p.d.m0.g.b getArrayTypeFqName() {
        return (d0.e0.p.d.m0.g.b) this.arrayTypeFqName$delegate.getValue();
    }

    public final e getArrayTypeName() {
        return this.arrayTypeName;
    }

    public final d0.e0.p.d.m0.g.b getTypeFqName() {
        return (d0.e0.p.d.m0.g.b) this.typeFqName$delegate.getValue();
    }

    public final e getTypeName() {
        return this.typeName;
    }
}
