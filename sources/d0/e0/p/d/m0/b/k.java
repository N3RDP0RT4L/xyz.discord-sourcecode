package d0.e0.p.d.m0.b;

import androidx.core.app.NotificationCompat;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.c;
import d0.e0.p.d.m0.g.e;
import d0.t.n;
import d0.t.n0;
import d0.z.d.m;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
/* compiled from: StandardNames.kt */
/* loaded from: classes3.dex */
public final class k {

    /* renamed from: b  reason: collision with root package name */
    public static final e f3187b;
    public static final e c;
    public static final b d;
    public static final b e;
    public static final b f;
    public static final b g;
    public static final b i;
    public static final e k;
    public static final b l;
    public static final b m;
    public static final b n;
    public static final b o;
    public static final Set<b> p;
    public static final k a = new k();
    public static final b h = new b("kotlin.Result");
    public static final List<String> j = n.listOf((Object[]) new String[]{"KProperty", "KMutableProperty", "KFunction", "KSuspendFunction"});

    /* compiled from: StandardNames.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public static final b A;
        public static final b B;
        public static final b C;
        public static final b D;
        public static final b E;
        public static final b F;
        public static final b G;
        public static final b H;
        public static final b I;
        public static final b J;
        public static final b K;
        public static final b L;
        public static final b M;
        public static final b N;
        public static final b O;
        public static final b P;
        public static final b Q;
        public static final b R;
        public static final b S;
        public static final b T;
        public static final b U;
        public static final b V;
        public static final b W;
        public static final c X = reflect("KClass");
        public static final c Y;
        public static final d0.e0.p.d.m0.g.a Z;
        public static final a a;

        /* renamed from: a0  reason: collision with root package name */
        public static final b f3188a0;

        /* renamed from: b  reason: collision with root package name */
        public static final c f3189b;

        /* renamed from: b0  reason: collision with root package name */
        public static final b f3190b0;
        public static final c c;

        /* renamed from: c0  reason: collision with root package name */
        public static final b f3191c0;
        public static final c d;

        /* renamed from: d0  reason: collision with root package name */
        public static final b f3192d0;
        public static final c e;

        /* renamed from: e0  reason: collision with root package name */
        public static final d0.e0.p.d.m0.g.a f3193e0;
        public static final c f;

        /* renamed from: f0  reason: collision with root package name */
        public static final d0.e0.p.d.m0.g.a f3194f0;
        public static final c g;

        /* renamed from: g0  reason: collision with root package name */
        public static final d0.e0.p.d.m0.g.a f3195g0;
        public static final c h;

        /* renamed from: h0  reason: collision with root package name */
        public static final d0.e0.p.d.m0.g.a f3196h0;
        public static final c i;

        /* renamed from: i0  reason: collision with root package name */
        public static final b f3197i0;
        public static final c j;

        /* renamed from: j0  reason: collision with root package name */
        public static final b f3198j0;
        public static final c k;
        public static final b k0;
        public static final c l;
        public static final b l0;
        public static final c m;
        public static final Set<e> m0;
        public static final c n;
        public static final Set<e> n0;
        public static final c o;
        public static final Map<c, i> o0;
        public static final c p;
        public static final Map<c, i> p0;
        public static final c q;
        public static final c r;

        /* renamed from: s  reason: collision with root package name */
        public static final b f3199s;
        public static final b t;
        public static final b u;
        public static final b v;
        public static final b w;

        /* renamed from: x  reason: collision with root package name */
        public static final b f3200x;

        /* renamed from: y  reason: collision with root package name */
        public static final b f3201y;

        /* renamed from: z  reason: collision with root package name */
        public static final b f3202z;

        static {
            a aVar = new a();
            a = aVar;
            c unsafe = aVar.c("Any").toUnsafe();
            m.checkNotNullExpressionValue(unsafe, "fqName(simpleName).toUnsafe()");
            f3189b = unsafe;
            c unsafe2 = aVar.c("Nothing").toUnsafe();
            m.checkNotNullExpressionValue(unsafe2, "fqName(simpleName).toUnsafe()");
            c = unsafe2;
            c unsafe3 = aVar.c("Cloneable").toUnsafe();
            m.checkNotNullExpressionValue(unsafe3, "fqName(simpleName).toUnsafe()");
            d = unsafe3;
            aVar.c("Suppress");
            c unsafe4 = aVar.c("Unit").toUnsafe();
            m.checkNotNullExpressionValue(unsafe4, "fqName(simpleName).toUnsafe()");
            e = unsafe4;
            c unsafe5 = aVar.c("CharSequence").toUnsafe();
            m.checkNotNullExpressionValue(unsafe5, "fqName(simpleName).toUnsafe()");
            f = unsafe5;
            c unsafe6 = aVar.c("String").toUnsafe();
            m.checkNotNullExpressionValue(unsafe6, "fqName(simpleName).toUnsafe()");
            g = unsafe6;
            c unsafe7 = aVar.c("Array").toUnsafe();
            m.checkNotNullExpressionValue(unsafe7, "fqName(simpleName).toUnsafe()");
            h = unsafe7;
            c unsafe8 = aVar.c("Boolean").toUnsafe();
            m.checkNotNullExpressionValue(unsafe8, "fqName(simpleName).toUnsafe()");
            i = unsafe8;
            c unsafe9 = aVar.c("Char").toUnsafe();
            m.checkNotNullExpressionValue(unsafe9, "fqName(simpleName).toUnsafe()");
            j = unsafe9;
            c unsafe10 = aVar.c("Byte").toUnsafe();
            m.checkNotNullExpressionValue(unsafe10, "fqName(simpleName).toUnsafe()");
            k = unsafe10;
            c unsafe11 = aVar.c("Short").toUnsafe();
            m.checkNotNullExpressionValue(unsafe11, "fqName(simpleName).toUnsafe()");
            l = unsafe11;
            c unsafe12 = aVar.c("Int").toUnsafe();
            m.checkNotNullExpressionValue(unsafe12, "fqName(simpleName).toUnsafe()");
            m = unsafe12;
            c unsafe13 = aVar.c("Long").toUnsafe();
            m.checkNotNullExpressionValue(unsafe13, "fqName(simpleName).toUnsafe()");
            n = unsafe13;
            c unsafe14 = aVar.c("Float").toUnsafe();
            m.checkNotNullExpressionValue(unsafe14, "fqName(simpleName).toUnsafe()");
            o = unsafe14;
            c unsafe15 = aVar.c("Double").toUnsafe();
            m.checkNotNullExpressionValue(unsafe15, "fqName(simpleName).toUnsafe()");
            p = unsafe15;
            c unsafe16 = aVar.c("Number").toUnsafe();
            m.checkNotNullExpressionValue(unsafe16, "fqName(simpleName).toUnsafe()");
            q = unsafe16;
            c unsafe17 = aVar.c("Enum").toUnsafe();
            m.checkNotNullExpressionValue(unsafe17, "fqName(simpleName).toUnsafe()");
            r = unsafe17;
            m.checkNotNullExpressionValue(aVar.c("Function").toUnsafe(), "fqName(simpleName).toUnsafe()");
            f3199s = aVar.c("Throwable");
            t = aVar.c("Comparable");
            b bVar = k.o;
            m.checkNotNullExpressionValue(bVar.child(e.identifier("IntRange")).toUnsafe(), "RANGES_PACKAGE_FQ_NAME.child(Name.identifier(simpleName)).toUnsafe()");
            m.checkNotNullExpressionValue(bVar.child(e.identifier("LongRange")).toUnsafe(), "RANGES_PACKAGE_FQ_NAME.child(Name.identifier(simpleName)).toUnsafe()");
            u = aVar.c("Deprecated");
            aVar.c("DeprecatedSinceKotlin");
            v = aVar.c("DeprecationLevel");
            w = aVar.c("ReplaceWith");
            f3200x = aVar.c("ExtensionFunctionType");
            f3201y = aVar.c("ParameterName");
            f3202z = aVar.c("Annotation");
            A = aVar.a("Target");
            B = aVar.a("AnnotationTarget");
            C = aVar.a("AnnotationRetention");
            D = aVar.a("Retention");
            E = aVar.a("Repeatable");
            F = aVar.a("MustBeDocumented");
            G = aVar.c("UnsafeVariance");
            aVar.c("PublishedApi");
            H = aVar.b("Iterator");
            I = aVar.b("Iterable");
            J = aVar.b("Collection");
            K = aVar.b("List");
            L = aVar.b("ListIterator");
            M = aVar.b("Set");
            b b2 = aVar.b("Map");
            N = b2;
            b child = b2.child(e.identifier("Entry"));
            m.checkNotNullExpressionValue(child, "map.child(Name.identifier(\"Entry\"))");
            O = child;
            P = aVar.b("MutableIterator");
            Q = aVar.b("MutableIterable");
            R = aVar.b("MutableCollection");
            S = aVar.b("MutableList");
            T = aVar.b("MutableListIterator");
            U = aVar.b("MutableSet");
            b b3 = aVar.b("MutableMap");
            V = b3;
            b child2 = b3.child(e.identifier("MutableEntry"));
            m.checkNotNullExpressionValue(child2, "mutableMap.child(Name.identifier(\"MutableEntry\"))");
            W = child2;
            a aVar2 = a;
            reflect("KCallable");
            reflect("KProperty0");
            reflect("KProperty1");
            reflect("KProperty2");
            reflect("KMutableProperty0");
            reflect("KMutableProperty1");
            reflect("KMutableProperty2");
            c reflect = reflect("KProperty");
            Y = reflect;
            reflect("KMutableProperty");
            d0.e0.p.d.m0.g.a aVar3 = d0.e0.p.d.m0.g.a.topLevel(reflect.toSafe());
            m.checkNotNullExpressionValue(aVar3, "topLevel(kPropertyFqName.toSafe())");
            Z = aVar3;
            reflect("KDeclarationContainer");
            b c2 = aVar2.c("UByte");
            f3188a0 = c2;
            b c3 = aVar2.c("UShort");
            f3190b0 = c3;
            b c4 = aVar2.c("UInt");
            f3191c0 = c4;
            b c5 = aVar2.c("ULong");
            f3192d0 = c5;
            d0.e0.p.d.m0.g.a aVar4 = d0.e0.p.d.m0.g.a.topLevel(c2);
            m.checkNotNullExpressionValue(aVar4, "topLevel(uByteFqName)");
            f3193e0 = aVar4;
            d0.e0.p.d.m0.g.a aVar5 = d0.e0.p.d.m0.g.a.topLevel(c3);
            m.checkNotNullExpressionValue(aVar5, "topLevel(uShortFqName)");
            f3194f0 = aVar5;
            d0.e0.p.d.m0.g.a aVar6 = d0.e0.p.d.m0.g.a.topLevel(c4);
            m.checkNotNullExpressionValue(aVar6, "topLevel(uIntFqName)");
            f3195g0 = aVar6;
            d0.e0.p.d.m0.g.a aVar7 = d0.e0.p.d.m0.g.a.topLevel(c5);
            m.checkNotNullExpressionValue(aVar7, "topLevel(uLongFqName)");
            f3196h0 = aVar7;
            f3197i0 = aVar2.c("UByteArray");
            f3198j0 = aVar2.c("UShortArray");
            k0 = aVar2.c("UIntArray");
            l0 = aVar2.c("ULongArray");
            i.values();
            HashSet newHashSetWithExpectedSize = d0.e0.p.d.m0.p.a.newHashSetWithExpectedSize(8);
            i[] values = i.values();
            int i2 = 0;
            for (int i3 = 0; i3 < 8; i3++) {
                newHashSetWithExpectedSize.add(values[i3].getTypeName());
            }
            m0 = newHashSetWithExpectedSize;
            i.values();
            HashSet newHashSetWithExpectedSize2 = d0.e0.p.d.m0.p.a.newHashSetWithExpectedSize(8);
            i[] values2 = i.values();
            for (int i4 = 0; i4 < 8; i4++) {
                newHashSetWithExpectedSize2.add(values2[i4].getArrayTypeName());
            }
            n0 = newHashSetWithExpectedSize2;
            i.values();
            HashMap newHashMapWithExpectedSize = d0.e0.p.d.m0.p.a.newHashMapWithExpectedSize(8);
            i[] values3 = i.values();
            int i5 = 0;
            while (i5 < 8) {
                i iVar = values3[i5];
                i5++;
                String asString = iVar.getTypeName().asString();
                m.checkNotNullExpressionValue(asString, "primitiveType.typeName.asString()");
                b child3 = k.l.child(e.identifier(asString));
                m.checkNotNullExpressionValue(child3, "BUILT_INS_PACKAGE_FQ_NAME.child(Name.identifier(simpleName))");
                c unsafe18 = child3.toUnsafe();
                m.checkNotNullExpressionValue(unsafe18, "fqName(simpleName).toUnsafe()");
                newHashMapWithExpectedSize.put(unsafe18, iVar);
            }
            o0 = newHashMapWithExpectedSize;
            i.values();
            HashMap newHashMapWithExpectedSize2 = d0.e0.p.d.m0.p.a.newHashMapWithExpectedSize(8);
            i[] values4 = i.values();
            while (i2 < 8) {
                i iVar2 = values4[i2];
                i2++;
                String asString2 = iVar2.getArrayTypeName().asString();
                m.checkNotNullExpressionValue(asString2, "primitiveType.arrayTypeName.asString()");
                b child4 = k.l.child(e.identifier(asString2));
                m.checkNotNullExpressionValue(child4, "BUILT_INS_PACKAGE_FQ_NAME.child(Name.identifier(simpleName))");
                c unsafe19 = child4.toUnsafe();
                m.checkNotNullExpressionValue(unsafe19, "fqName(simpleName).toUnsafe()");
                newHashMapWithExpectedSize2.put(unsafe19, iVar2);
            }
            p0 = newHashMapWithExpectedSize2;
        }

        public static final c reflect(String str) {
            m.checkNotNullParameter(str, "simpleName");
            c unsafe = k.i.child(e.identifier(str)).toUnsafe();
            m.checkNotNullExpressionValue(unsafe, "KOTLIN_REFLECT_FQ_NAME.child(Name.identifier(simpleName)).toUnsafe()");
            return unsafe;
        }

        public final b a(String str) {
            b child = k.m.child(e.identifier(str));
            m.checkNotNullExpressionValue(child, "ANNOTATION_PACKAGE_FQ_NAME.child(Name.identifier(simpleName))");
            return child;
        }

        public final b b(String str) {
            b child = k.n.child(e.identifier(str));
            m.checkNotNullExpressionValue(child, "COLLECTIONS_PACKAGE_FQ_NAME.child(Name.identifier(simpleName))");
            return child;
        }

        public final b c(String str) {
            b child = k.l.child(e.identifier(str));
            m.checkNotNullExpressionValue(child, "BUILT_INS_PACKAGE_FQ_NAME.child(Name.identifier(simpleName))");
            return child;
        }
    }

    static {
        e identifier = e.identifier("values");
        m.checkNotNullExpressionValue(identifier, "identifier(\"values\")");
        f3187b = identifier;
        e identifier2 = e.identifier("valueOf");
        m.checkNotNullExpressionValue(identifier2, "identifier(\"valueOf\")");
        c = identifier2;
        b bVar = new b("kotlin.coroutines");
        d = bVar;
        b child = bVar.child(e.identifier("experimental"));
        m.checkNotNullExpressionValue(child, "COROUTINES_PACKAGE_FQ_NAME_RELEASE.child(Name.identifier(\"experimental\"))");
        e = child;
        m.checkNotNullExpressionValue(child.child(e.identifier("intrinsics")), "COROUTINES_PACKAGE_FQ_NAME_EXPERIMENTAL.child(Name.identifier(\"intrinsics\"))");
        b child2 = child.child(e.identifier("Continuation"));
        m.checkNotNullExpressionValue(child2, "COROUTINES_PACKAGE_FQ_NAME_EXPERIMENTAL.child(Name.identifier(\"Continuation\"))");
        f = child2;
        b child3 = bVar.child(e.identifier("Continuation"));
        m.checkNotNullExpressionValue(child3, "COROUTINES_PACKAGE_FQ_NAME_RELEASE.child(Name.identifier(\"Continuation\"))");
        g = child3;
        b bVar2 = new b("kotlin.reflect");
        i = bVar2;
        e identifier3 = e.identifier("kotlin");
        m.checkNotNullExpressionValue(identifier3, "identifier(\"kotlin\")");
        k = identifier3;
        b bVar3 = b.topLevel(identifier3);
        m.checkNotNullExpressionValue(bVar3, "topLevel(BUILT_INS_PACKAGE_NAME)");
        l = bVar3;
        b child4 = bVar3.child(e.identifier("annotation"));
        m.checkNotNullExpressionValue(child4, "BUILT_INS_PACKAGE_FQ_NAME.child(Name.identifier(\"annotation\"))");
        m = child4;
        b child5 = bVar3.child(e.identifier("collections"));
        m.checkNotNullExpressionValue(child5, "BUILT_INS_PACKAGE_FQ_NAME.child(Name.identifier(\"collections\"))");
        n = child5;
        b child6 = bVar3.child(e.identifier("ranges"));
        m.checkNotNullExpressionValue(child6, "BUILT_INS_PACKAGE_FQ_NAME.child(Name.identifier(\"ranges\"))");
        o = child6;
        m.checkNotNullExpressionValue(bVar3.child(e.identifier(NotificationCompat.MessagingStyle.Message.KEY_TEXT)), "BUILT_INS_PACKAGE_FQ_NAME.child(Name.identifier(\"text\"))");
        b child7 = bVar3.child(e.identifier("internal"));
        m.checkNotNullExpressionValue(child7, "BUILT_INS_PACKAGE_FQ_NAME.child(Name.identifier(\"internal\"))");
        p = n0.setOf((Object[]) new b[]{bVar3, child5, child6, child4, bVar2, child7, bVar});
    }

    public static final d0.e0.p.d.m0.g.a getFunctionClassId(int i2) {
        return new d0.e0.p.d.m0.g.a(l, e.identifier(getFunctionName(i2)));
    }

    public static final String getFunctionName(int i2) {
        return m.stringPlus("Function", Integer.valueOf(i2));
    }

    public static final b getPrimitiveFqName(i iVar) {
        m.checkNotNullParameter(iVar, "primitiveType");
        b child = l.child(iVar.getTypeName());
        m.checkNotNullExpressionValue(child, "BUILT_INS_PACKAGE_FQ_NAME.child(primitiveType.typeName)");
        return child;
    }

    public static final String getSuspendFunctionName(int i2) {
        return m.stringPlus(d0.e0.p.d.m0.b.p.c.SuspendFunction.getClassNamePrefix(), Integer.valueOf(i2));
    }

    public static final boolean isPrimitiveArray(c cVar) {
        m.checkNotNullParameter(cVar, "arrayFqName");
        return a.p0.get(cVar) != null;
    }
}
