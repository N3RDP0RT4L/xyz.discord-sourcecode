package d0.e0.p.d.m0.b;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.b.k;
import d0.e0.p.d.m0.b.p.c;
import d0.e0.p.d.m0.c.g1.c;
import d0.e0.p.d.m0.c.g1.j;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.v.w;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.d0;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.w0;
import d0.e0.p.d.m0.p.a;
import d0.o;
import d0.t.g0;
import d0.t.h0;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
/* compiled from: functionTypes.kt */
/* loaded from: classes3.dex */
public final class g {
    public static final j0 createFunctionType(h hVar, d0.e0.p.d.m0.c.g1.g gVar, c0 c0Var, List<? extends c0> list, List<e> list2, c0 c0Var2, boolean z2) {
        m.checkNotNullParameter(hVar, "builtIns");
        m.checkNotNullParameter(gVar, "annotations");
        m.checkNotNullParameter(list, "parameterTypes");
        m.checkNotNullParameter(c0Var2, "returnType");
        List<w0> functionTypeArgumentProjections = getFunctionTypeArgumentProjections(c0Var, list, list2, c0Var2, hVar);
        int size = list.size();
        if (c0Var != null) {
            size++;
        }
        d0.e0.p.d.m0.c.e functionDescriptor = getFunctionDescriptor(hVar, size, z2);
        if (c0Var != null) {
            gVar = withExtensionFunctionAnnotation(gVar, hVar);
        }
        d0 d0Var = d0.a;
        return d0.simpleNotNullType(gVar, functionDescriptor, functionTypeArgumentProjections);
    }

    public static final e extractParameterNameFromFunctionTypeArgument(c0 c0Var) {
        String str;
        m.checkNotNullParameter(c0Var, "<this>");
        c findAnnotation = c0Var.getAnnotations().findAnnotation(k.a.f3201y);
        if (findAnnotation == null) {
            return null;
        }
        Object singleOrNull = u.singleOrNull(findAnnotation.getAllValueArguments().values());
        w wVar = singleOrNull instanceof w ? (w) singleOrNull : null;
        if (wVar == null || (str = (String) wVar.getValue()) == null || !e.isValidIdentifier(str)) {
            str = null;
        }
        if (str == null) {
            return null;
        }
        return e.identifier(str);
    }

    public static final d0.e0.p.d.m0.c.e getFunctionDescriptor(h hVar, int i, boolean z2) {
        m.checkNotNullParameter(hVar, "builtIns");
        d0.e0.p.d.m0.c.e suspendFunction = z2 ? hVar.getSuspendFunction(i) : hVar.getFunction(i);
        m.checkNotNullExpressionValue(suspendFunction, "if (isSuspendFunction) builtIns.getSuspendFunction(parameterCount) else builtIns.getFunction(parameterCount)");
        return suspendFunction;
    }

    public static final List<w0> getFunctionTypeArgumentProjections(c0 c0Var, List<? extends c0> list, List<e> list2, c0 c0Var2, h hVar) {
        e eVar;
        m.checkNotNullParameter(list, "parameterTypes");
        m.checkNotNullParameter(c0Var2, "returnType");
        m.checkNotNullParameter(hVar, "builtIns");
        int i = 0;
        ArrayList arrayList = new ArrayList(list.size() + (c0Var != null ? 1 : 0) + 1);
        a.addIfNotNull(arrayList, c0Var == null ? null : d0.e0.p.d.m0.n.o1.a.asTypeProjection(c0Var));
        for (Object obj : list) {
            i++;
            if (i < 0) {
                n.throwIndexOverflow();
            }
            c0 c0Var3 = (c0) obj;
            if (list2 == null || (eVar = list2.get(i)) == null || eVar.isSpecial()) {
                eVar = null;
            }
            if (eVar != null) {
                b bVar = k.a.f3201y;
                e identifier = e.identifier(ModelAuditLogEntry.CHANGE_KEY_NAME);
                String asString = eVar.asString();
                m.checkNotNullExpressionValue(asString, "name.asString()");
                c0Var3 = d0.e0.p.d.m0.n.o1.a.replaceAnnotations(c0Var3, d0.e0.p.d.m0.c.g1.g.f.create(u.plus(c0Var3.getAnnotations(), new j(hVar, bVar, g0.mapOf(o.to(identifier, new w(asString)))))));
            }
            arrayList.add(d0.e0.p.d.m0.n.o1.a.asTypeProjection(c0Var3));
        }
        arrayList.add(d0.e0.p.d.m0.n.o1.a.asTypeProjection(c0Var2));
        return arrayList;
    }

    public static final d0.e0.p.d.m0.b.p.c getFunctionalClassKind(d0.e0.p.d.m0.c.m mVar) {
        m.checkNotNullParameter(mVar, "<this>");
        if (!(mVar instanceof d0.e0.p.d.m0.c.e) || !h.isUnderKotlinPackage(mVar)) {
            return null;
        }
        d0.e0.p.d.m0.g.c fqNameUnsafe = d0.e0.p.d.m0.k.x.a.getFqNameUnsafe(mVar);
        if (!fqNameUnsafe.isSafe() || fqNameUnsafe.isRoot()) {
            return null;
        }
        c.a aVar = d0.e0.p.d.m0.b.p.c.j;
        String asString = fqNameUnsafe.shortName().asString();
        m.checkNotNullExpressionValue(asString, "shortName().asString()");
        b parent = fqNameUnsafe.toSafe().parent();
        m.checkNotNullExpressionValue(parent, "toSafe().parent()");
        return aVar.getFunctionalClassKind(asString, parent);
    }

    public static final c0 getReceiverTypeFromFunctionType(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        isBuiltinFunctionalType(c0Var);
        if (c0Var.getAnnotations().findAnnotation(k.a.f3200x) != null) {
            return ((w0) u.first((List<? extends Object>) c0Var.getArguments())).getType();
        }
        return null;
    }

    public static final c0 getReturnTypeFromFunctionType(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        isBuiltinFunctionalType(c0Var);
        c0 type = ((w0) u.last((List<? extends Object>) c0Var.getArguments())).getType();
        m.checkNotNullExpressionValue(type, "arguments.last().type");
        return type;
    }

    public static final List<w0> getValueParameterTypesFromFunctionType(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        isBuiltinFunctionalType(c0Var);
        List<w0> arguments = c0Var.getArguments();
        return arguments.subList(isBuiltinExtensionFunctionalType(c0Var) ? 1 : 0, arguments.size() - 1);
    }

    public static final boolean isBuiltinExtensionFunctionalType(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        if (!isBuiltinFunctionalType(c0Var)) {
            return false;
        }
        return c0Var.getAnnotations().findAnnotation(k.a.f3200x) != null;
    }

    public static final boolean isBuiltinFunctionalClassDescriptor(d0.e0.p.d.m0.c.m mVar) {
        m.checkNotNullParameter(mVar, "<this>");
        d0.e0.p.d.m0.b.p.c functionalClassKind = getFunctionalClassKind(mVar);
        return functionalClassKind == d0.e0.p.d.m0.b.p.c.Function || functionalClassKind == d0.e0.p.d.m0.b.p.c.SuspendFunction;
    }

    public static final boolean isBuiltinFunctionalType(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        h declarationDescriptor = c0Var.getConstructor().getDeclarationDescriptor();
        return m.areEqual(declarationDescriptor == null ? null : Boolean.valueOf(isBuiltinFunctionalClassDescriptor(declarationDescriptor)), Boolean.TRUE);
    }

    public static final boolean isFunctionType(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        h declarationDescriptor = c0Var.getConstructor().getDeclarationDescriptor();
        return (declarationDescriptor == null ? null : getFunctionalClassKind(declarationDescriptor)) == d0.e0.p.d.m0.b.p.c.Function;
    }

    public static final boolean isSuspendFunctionType(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        h declarationDescriptor = c0Var.getConstructor().getDeclarationDescriptor();
        return (declarationDescriptor == null ? null : getFunctionalClassKind(declarationDescriptor)) == d0.e0.p.d.m0.b.p.c.SuspendFunction;
    }

    public static final d0.e0.p.d.m0.c.g1.g withExtensionFunctionAnnotation(d0.e0.p.d.m0.c.g1.g gVar, h hVar) {
        m.checkNotNullParameter(gVar, "<this>");
        m.checkNotNullParameter(hVar, "builtIns");
        b bVar = k.a.f3200x;
        return gVar.hasAnnotation(bVar) ? gVar : d0.e0.p.d.m0.c.g1.g.f.create(u.plus(gVar, new j(hVar, bVar, h0.emptyMap())));
    }
}
