package d0.e0.p.d.m0.b;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.e0;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.g.a;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.e1;
import d0.t.h0;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Set;
/* compiled from: UnsignedType.kt */
/* loaded from: classes3.dex */
public final class o {
    public static final o a = new o();

    /* renamed from: b  reason: collision with root package name */
    public static final Set<e> f3204b;
    public static final HashMap<a, a> c;
    public static final HashMap<a, a> d;
    public static final Set<e> e;

    static {
        n[] values = n.values();
        ArrayList arrayList = new ArrayList(4);
        int i = 0;
        for (int i2 = 0; i2 < 4; i2++) {
            arrayList.add(values[i2].getTypeName());
        }
        f3204b = u.toSet(arrayList);
        m[] values2 = m.values();
        ArrayList arrayList2 = new ArrayList(4);
        for (int i3 = 0; i3 < 4; i3++) {
            arrayList2.add(values2[i3].getTypeName());
        }
        u.toSet(arrayList2);
        c = new HashMap<>();
        d = new HashMap<>();
        h0.hashMapOf(d0.o.to(m.UBYTEARRAY, e.identifier("ubyteArrayOf")), d0.o.to(m.USHORTARRAY, e.identifier("ushortArrayOf")), d0.o.to(m.UINTARRAY, e.identifier("uintArrayOf")), d0.o.to(m.ULONGARRAY, e.identifier("ulongArrayOf")));
        n[] values3 = n.values();
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        for (int i4 = 0; i4 < 4; i4++) {
            linkedHashSet.add(values3[i4].getArrayClassId().getShortClassName());
        }
        e = linkedHashSet;
        n[] values4 = n.values();
        while (i < 4) {
            n nVar = values4[i];
            i++;
            c.put(nVar.getArrayClassId(), nVar.getClassId());
            d.put(nVar.getClassId(), nVar.getArrayClassId());
        }
    }

    public static final boolean isUnsignedType(c0 c0Var) {
        h declarationDescriptor;
        m.checkNotNullParameter(c0Var, "type");
        if (!e1.noExpectedType(c0Var) && (declarationDescriptor = c0Var.getConstructor().getDeclarationDescriptor()) != null) {
            return a.isUnsignedClass(declarationDescriptor);
        }
        return false;
    }

    public final a getUnsignedClassIdByArrayClassId(a aVar) {
        m.checkNotNullParameter(aVar, "arrayClassId");
        return c.get(aVar);
    }

    public final boolean isShortNameOfUnsignedArray(e eVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        return e.contains(eVar);
    }

    public final boolean isUnsignedClass(d0.e0.p.d.m0.c.m mVar) {
        m.checkNotNullParameter(mVar, "descriptor");
        d0.e0.p.d.m0.c.m containingDeclaration = mVar.getContainingDeclaration();
        return (containingDeclaration instanceof e0) && m.areEqual(((e0) containingDeclaration).getFqName(), k.l) && f3204b.contains(mVar.getName());
    }
}
