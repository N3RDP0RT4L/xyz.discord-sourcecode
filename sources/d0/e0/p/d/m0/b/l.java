package d0.e0.p.d.m0.b;

import androidx.exifinterface.media.ExifInterface;
import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.f;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.i1.k0;
import d0.e0.p.d.m0.c.i1.n;
import d0.e0.p.d.m0.c.i1.z;
import d0.e0.p.d.m0.c.u;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.m.o;
import d0.e0.p.d.m0.n.d0;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.j1;
import d0.e0.p.d.m0.n.o1.a;
import d0.e0.p.d.m0.n.t;
import d0.e0.p.d.m0.n.w0;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
/* compiled from: suspendFunctionTypes.kt */
/* loaded from: classes3.dex */
public final class l {
    public static final z a;

    /* renamed from: b  reason: collision with root package name */
    public static final z f3203b;

    static {
        c0 errorModule = t.getErrorModule();
        m.checkNotNullExpressionValue(errorModule, "getErrorModule()");
        n nVar = new n(errorModule, k.e);
        f fVar = f.INTERFACE;
        e shortName = k.f.shortName();
        u0 u0Var = u0.a;
        o oVar = d0.e0.p.d.m0.m.f.f3486b;
        z zVar = new z(nVar, fVar, false, false, shortName, u0Var, oVar);
        d0.e0.p.d.m0.c.z zVar2 = d0.e0.p.d.m0.c.z.ABSTRACT;
        zVar.setModality(zVar2);
        u uVar = d0.e0.p.d.m0.c.t.e;
        zVar.setVisibility(uVar);
        g.a aVar = g.f;
        g empty = aVar.getEMPTY();
        j1 j1Var = j1.IN_VARIANCE;
        zVar.setTypeParameterDescriptors(d0.t.m.listOf(k0.createWithDefaultBound(zVar, empty, false, j1Var, e.identifier(ExifInterface.GPS_DIRECTION_TRUE), 0, oVar)));
        zVar.createTypeConstructor();
        a = zVar;
        c0 errorModule2 = t.getErrorModule();
        m.checkNotNullExpressionValue(errorModule2, "getErrorModule()");
        z zVar3 = new z(new n(errorModule2, k.d), fVar, false, false, k.g.shortName(), u0Var, oVar);
        zVar3.setModality(zVar2);
        zVar3.setVisibility(uVar);
        zVar3.setTypeParameterDescriptors(d0.t.m.listOf(k0.createWithDefaultBound(zVar3, aVar.getEMPTY(), false, j1Var, e.identifier(ExifInterface.GPS_DIRECTION_TRUE), 0, oVar)));
        zVar3.createTypeConstructor();
        f3203b = zVar3;
    }

    public static final boolean isContinuation(b bVar, boolean z2) {
        if (z2) {
            return m.areEqual(bVar, k.g);
        }
        return m.areEqual(bVar, k.f);
    }

    public static final j0 transformSuspendFunctionToRuntimeFunctionType(d0.e0.p.d.m0.n.c0 c0Var, boolean z2) {
        d0.e0.p.d.m0.n.u0 u0Var;
        j0 createFunctionType;
        m.checkNotNullParameter(c0Var, "suspendFunType");
        g.isSuspendFunctionType(c0Var);
        h builtIns = a.getBuiltIns(c0Var);
        g annotations = c0Var.getAnnotations();
        d0.e0.p.d.m0.n.c0 receiverTypeFromFunctionType = g.getReceiverTypeFromFunctionType(c0Var);
        List<w0> valueParameterTypesFromFunctionType = g.getValueParameterTypesFromFunctionType(c0Var);
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(valueParameterTypesFromFunctionType, 10));
        for (w0 w0Var : valueParameterTypesFromFunctionType) {
            arrayList.add(w0Var.getType());
        }
        d0 d0Var = d0.a;
        g empty = g.f.getEMPTY();
        if (z2) {
            u0Var = f3203b.getTypeConstructor();
        } else {
            u0Var = a.getTypeConstructor();
        }
        d0.e0.p.d.m0.n.u0 u0Var2 = u0Var;
        m.checkNotNullExpressionValue(u0Var2, "if (isReleaseCoroutines) FAKE_CONTINUATION_CLASS_DESCRIPTOR_RELEASE.typeConstructor\n                    else FAKE_CONTINUATION_CLASS_DESCRIPTOR_EXPERIMENTAL.typeConstructor");
        List plus = d0.t.u.plus((Collection<? extends j0>) arrayList, d0.simpleType$default(empty, u0Var2, d0.t.m.listOf(a.asTypeProjection(g.getReturnTypeFromFunctionType(c0Var))), false, null, 16, null));
        j0 nullableAnyType = a.getBuiltIns(c0Var).getNullableAnyType();
        m.checkNotNullExpressionValue(nullableAnyType, "suspendFunType.builtIns.nullableAnyType");
        createFunctionType = g.createFunctionType(builtIns, annotations, receiverTypeFromFunctionType, plus, null, nullableAnyType, (r14 & 64) != 0 ? false : false);
        return createFunctionType.makeNullableAsSpecified(c0Var.isMarkedNullable());
    }
}
