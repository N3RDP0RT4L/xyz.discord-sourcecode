package d0.e0.p.d.m0.b;

import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.f0;
import d0.e0.p.d.m0.c.h1.b;
import d0.e0.p.d.m0.c.h1.c;
import d0.g;
import d0.i;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ServiceLoader;
import kotlin.Lazy;
import kotlin.jvm.functions.Function0;
/* compiled from: BuiltInsLoader.kt */
/* loaded from: classes3.dex */
public interface a {
    public static final C0285a a = C0285a.a;

    /* compiled from: BuiltInsLoader.kt */
    /* renamed from: d0.e0.p.d.m0.b.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0285a {
        public static final /* synthetic */ C0285a a = new C0285a();

        /* renamed from: b  reason: collision with root package name */
        public static final Lazy<a> f3181b = g.lazy(i.PUBLICATION, C0286a.j);

        /* compiled from: BuiltInsLoader.kt */
        /* renamed from: d0.e0.p.d.m0.b.a$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0286a extends o implements Function0<a> {
            public static final C0286a j = new C0286a();

            public C0286a() {
                super(0);
            }

            /* JADX WARN: Can't rename method to resolve collision */
            @Override // kotlin.jvm.functions.Function0
            public final a invoke() {
                ServiceLoader load = ServiceLoader.load(a.class, a.class.getClassLoader());
                m.checkNotNullExpressionValue(load, "implementations");
                a aVar = (a) u.firstOrNull(load);
                if (aVar != null) {
                    return aVar;
                }
                throw new IllegalStateException("No BuiltInsLoader implementation was found. Please ensure that the META-INF/services/ is not stripped from your application and that the Java virtual machine is not running under a security manager");
            }
        }

        public final a getInstance() {
            return f3181b.getValue();
        }
    }

    f0 createPackageFragmentProvider(d0.e0.p.d.m0.m.o oVar, c0 c0Var, Iterable<? extends b> iterable, c cVar, d0.e0.p.d.m0.c.h1.a aVar, boolean z2);
}
