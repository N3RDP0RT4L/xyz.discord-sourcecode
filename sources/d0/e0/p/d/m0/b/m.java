package d0.e0.p.d.m0.b;

import d0.e0.p.d.m0.g.a;
import d0.e0.p.d.m0.g.e;
/* JADX WARN: Init of enum j can be incorrect */
/* JADX WARN: Init of enum k can be incorrect */
/* JADX WARN: Init of enum l can be incorrect */
/* JADX WARN: Init of enum m can be incorrect */
/* compiled from: UnsignedType.kt */
/* loaded from: classes3.dex */
public enum m {
    UBYTEARRAY(r1),
    USHORTARRAY(r2),
    UINTARRAY(r4),
    ULONGARRAY(r6);
    
    private final a classId;
    private final e typeName;

    static {
        d0.z.d.m.checkNotNullExpressionValue(a.fromString("kotlin/UByteArray"), "fromString(\"kotlin/UByteArray\")");
        d0.z.d.m.checkNotNullExpressionValue(a.fromString("kotlin/UShortArray"), "fromString(\"kotlin/UShortArray\")");
        d0.z.d.m.checkNotNullExpressionValue(a.fromString("kotlin/UIntArray"), "fromString(\"kotlin/UIntArray\")");
        d0.z.d.m.checkNotNullExpressionValue(a.fromString("kotlin/ULongArray"), "fromString(\"kotlin/ULongArray\")");
    }

    m(a aVar) {
        this.classId = aVar;
        e shortClassName = aVar.getShortClassName();
        d0.z.d.m.checkNotNullExpressionValue(shortClassName, "classId.shortClassName");
        this.typeName = shortClassName;
    }

    public final e getTypeName() {
        return this.typeName;
    }
}
