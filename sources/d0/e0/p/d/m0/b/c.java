package d0.e0.p.d.m0.b;

import d0.e0.p.d.m0.b.k;
import d0.e0.p.d.m0.g.a;
import d0.e0.p.d.m0.g.b;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
/* compiled from: CompanionObjectMapping.kt */
/* loaded from: classes3.dex */
public final class c {
    public static final c a = new c();

    /* renamed from: b  reason: collision with root package name */
    public static final Set<a> f3182b;

    static {
        Set<i> set = i.j;
        k kVar = k.a;
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(set, 10));
        for (i iVar : set) {
            arrayList.add(k.getPrimitiveFqName(iVar));
        }
        b safe = k.a.g.toSafe();
        m.checkNotNullExpressionValue(safe, "string.toSafe()");
        List plus = u.plus((Collection<? extends b>) arrayList, safe);
        b safe2 = k.a.i.toSafe();
        m.checkNotNullExpressionValue(safe2, "_boolean.toSafe()");
        List plus2 = u.plus((Collection<? extends b>) plus, safe2);
        b safe3 = k.a.r.toSafe();
        m.checkNotNullExpressionValue(safe3, "_enum.toSafe()");
        List<b> plus3 = u.plus((Collection<? extends b>) plus2, safe3);
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        for (b bVar : plus3) {
            linkedHashSet.add(a.topLevel(bVar));
        }
        f3182b = linkedHashSet;
    }

    public final Set<a> allClassesWithIntrinsicCompanions() {
        return f3182b;
    }

    public final Set<a> getClassIds() {
        return f3182b;
    }
}
