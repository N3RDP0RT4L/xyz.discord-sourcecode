package d0.e0.p.d.m0.b.p;

import d0.e0.p.d.m0.c.x;
import d0.e0.p.d.m0.k.a0.e;
import d0.e0.p.d.m0.m.o;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
/* compiled from: FunctionClassScope.kt */
/* loaded from: classes3.dex */
public final class d extends e {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public d(o oVar, b bVar) {
        super(oVar, bVar);
        m.checkNotNullParameter(oVar, "storageManager");
        m.checkNotNullParameter(bVar, "containingClass");
    }

    @Override // d0.e0.p.d.m0.k.a0.e
    public List<x> a() {
        int ordinal = ((b) this.c).getFunctionKind().ordinal();
        if (ordinal == 0) {
            return d0.t.m.listOf(e.M.create((b) this.c, false));
        }
        if (ordinal != 1) {
            return n.emptyList();
        }
        return d0.t.m.listOf(e.M.create((b) this.c, true));
    }
}
