package d0.e0.p.d.m0.b.p;

import androidx.exifinterface.media.ExifInterface;
import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.i1.g0;
import d0.e0.p.d.m0.c.i1.l0;
import d0.e0.p.d.m0.c.i1.q;
import d0.e0.p.d.m0.c.q0;
import d0.e0.p.d.m0.c.t;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.c.x;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.j1;
import d0.e0.p.d.m0.o.j;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.t.z;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: FunctionInvokeDescriptor.kt */
/* loaded from: classes3.dex */
public final class e extends g0 {
    public static final a M = new a(null);

    /* compiled from: FunctionInvokeDescriptor.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final e create(b bVar, boolean z2) {
            String str;
            m.checkNotNullParameter(bVar, "functionClass");
            List<z0> declaredTypeParameters = bVar.getDeclaredTypeParameters();
            e eVar = new e(bVar, null, b.a.DECLARATION, z2, null);
            q0 thisAsReceiverParameter = bVar.getThisAsReceiverParameter();
            List<? extends z0> emptyList = n.emptyList();
            ArrayList arrayList = new ArrayList();
            for (Object obj : declaredTypeParameters) {
                if (!(((z0) obj).getVariance() == j1.IN_VARIANCE)) {
                    break;
                }
                arrayList.add(obj);
            }
            Iterable<z> withIndex = u.withIndex(arrayList);
            ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(withIndex, 10));
            for (z zVar : withIndex) {
                int index = zVar.getIndex();
                z0 z0Var = (z0) zVar.getValue();
                String asString = z0Var.getName().asString();
                m.checkNotNullExpressionValue(asString, "typeParameter.name.asString()");
                if (m.areEqual(asString, ExifInterface.GPS_DIRECTION_TRUE)) {
                    str = "instance";
                } else if (m.areEqual(asString, ExifInterface.LONGITUDE_EAST)) {
                    str = "receiver";
                } else {
                    str = asString.toLowerCase();
                    m.checkNotNullExpressionValue(str, "(this as java.lang.String).toLowerCase()");
                }
                g empty = g.f.getEMPTY();
                d0.e0.p.d.m0.g.e identifier = d0.e0.p.d.m0.g.e.identifier(str);
                m.checkNotNullExpressionValue(identifier, "identifier(name)");
                j0 defaultType = z0Var.getDefaultType();
                m.checkNotNullExpressionValue(defaultType, "typeParameter.defaultType");
                u0 u0Var = u0.a;
                m.checkNotNullExpressionValue(u0Var, "NO_SOURCE");
                ArrayList arrayList3 = arrayList2;
                arrayList3.add(new l0(eVar, null, index, empty, identifier, defaultType, false, false, false, null, u0Var));
                arrayList2 = arrayList3;
            }
            eVar.initialize((q0) null, thisAsReceiverParameter, emptyList, (List<c1>) arrayList2, (c0) ((z0) u.last((List<? extends Object>) declaredTypeParameters)).getDefaultType(), d0.e0.p.d.m0.c.z.ABSTRACT, t.e);
            eVar.setHasSynthesizedParameterNames(true);
            return eVar;
        }
    }

    public e(d0.e0.p.d.m0.c.m mVar, e eVar, b.a aVar, boolean z2) {
        super(mVar, eVar, g.f.getEMPTY(), j.g, aVar, u0.a);
        setOperator(true);
        setSuspend(z2);
        setHasStableParameterNames(false);
    }

    public /* synthetic */ e(d0.e0.p.d.m0.c.m mVar, e eVar, b.a aVar, boolean z2, DefaultConstructorMarker defaultConstructorMarker) {
        this(mVar, eVar, aVar, z2);
    }

    @Override // d0.e0.p.d.m0.c.i1.g0, d0.e0.p.d.m0.c.i1.q
    public q b(d0.e0.p.d.m0.c.m mVar, x xVar, b.a aVar, d0.e0.p.d.m0.g.e eVar, g gVar, u0 u0Var) {
        m.checkNotNullParameter(mVar, "newOwner");
        m.checkNotNullParameter(aVar, "kind");
        m.checkNotNullParameter(gVar, "annotations");
        m.checkNotNullParameter(u0Var, "source");
        return new e(mVar, (e) xVar, aVar, isSuspend());
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v12, types: [d0.e0.p.d.m0.c.i1.q$c, java.lang.Object] */
    @Override // d0.e0.p.d.m0.c.i1.q
    public x c(q.c cVar) {
        boolean z2;
        boolean z3;
        d0.e0.p.d.m0.g.e eVar;
        boolean z4;
        m.checkNotNullParameter(cVar, "configuration");
        e eVar2 = (e) super.c(cVar);
        if (eVar2 == 0) {
            return null;
        }
        List<c1> valueParameters = eVar2.getValueParameters();
        m.checkNotNullExpressionValue(valueParameters, "substituted.valueParameters");
        boolean z5 = true;
        if (!(valueParameters instanceof Collection) || !valueParameters.isEmpty()) {
            for (c1 c1Var : valueParameters) {
                c0 type = c1Var.getType();
                m.checkNotNullExpressionValue(type, "it.type");
                if (d0.e0.p.d.m0.b.g.extractParameterNameFromFunctionTypeArgument(type) != null) {
                    z4 = true;
                    continue;
                } else {
                    z4 = false;
                    continue;
                }
                if (z4) {
                    z2 = false;
                    break;
                }
            }
        }
        z2 = true;
        if (z2) {
            return eVar2;
        }
        List<c1> valueParameters2 = eVar2.getValueParameters();
        m.checkNotNullExpressionValue(valueParameters2, "substituted.valueParameters");
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(valueParameters2, 10));
        for (c1 c1Var2 : valueParameters2) {
            c0 type2 = c1Var2.getType();
            m.checkNotNullExpressionValue(type2, "it.type");
            arrayList.add(d0.e0.p.d.m0.b.g.extractParameterNameFromFunctionTypeArgument(type2));
        }
        int size = eVar2.getValueParameters().size() - arrayList.size();
        List<c1> valueParameters3 = eVar2.getValueParameters();
        m.checkNotNullExpressionValue(valueParameters3, "valueParameters");
        ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(valueParameters3, 10));
        for (c1 c1Var3 : valueParameters3) {
            d0.e0.p.d.m0.g.e name = c1Var3.getName();
            m.checkNotNullExpressionValue(name, "it.name");
            int index = c1Var3.getIndex();
            int i = index - size;
            if (i >= 0 && (eVar = (d0.e0.p.d.m0.g.e) arrayList.get(i)) != null) {
                name = eVar;
            }
            arrayList2.add(c1Var3.copy(eVar2, name, index));
        }
        q.c d = eVar2.d(d0.e0.p.d.m0.n.c1.a);
        if (!arrayList.isEmpty()) {
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                if (((d0.e0.p.d.m0.g.e) it.next()) == null) {
                    z3 = true;
                    continue;
                } else {
                    z3 = false;
                    continue;
                }
                if (z3) {
                    break;
                }
            }
        }
        z5 = false;
        ?? original = d.setHasSynthesizedParameterNames(z5).setValueParameters((List<c1>) arrayList2).setOriginal2((b) eVar2.getOriginal());
        m.checkNotNullExpressionValue(original, "newCopyBuilder(TypeSubstitutor.EMPTY)\n                .setHasSynthesizedParameterNames(parameterNames.any { it == null })\n                .setValueParameters(newValueParameters)\n                .setOriginal(original)");
        x c = super.c(original);
        m.checkNotNull(c);
        return c;
    }

    @Override // d0.e0.p.d.m0.c.i1.q, d0.e0.p.d.m0.c.y
    public boolean isExternal() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.i1.q, d0.e0.p.d.m0.c.x
    public boolean isInline() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.i1.q, d0.e0.p.d.m0.c.x
    public boolean isTailrec() {
        return false;
    }
}
