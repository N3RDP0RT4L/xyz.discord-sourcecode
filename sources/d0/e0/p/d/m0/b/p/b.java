package d0.e0.p.d.m0.b.p;

import d0.e0.p.d.m0.b.k;
import d0.e0.p.d.m0.c.d;
import d0.e0.p.d.m0.c.e0;
import d0.e0.p.d.m0.c.f;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.i1.k0;
import d0.e0.p.d.m0.c.t;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.c.w;
import d0.e0.p.d.m0.c.x0;
import d0.e0.p.d.m0.c.z;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.m.o;
import d0.e0.p.d.m0.n.d0;
import d0.e0.p.d.m0.n.j1;
import d0.e0.p.d.m0.n.y0;
import d0.t.c0;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.ranges.IntRange;
/* compiled from: FunctionClassDescriptor.kt */
/* loaded from: classes3.dex */
public final class b extends d0.e0.p.d.m0.c.i1.a {
    public static final d0.e0.p.d.m0.g.a o = new d0.e0.p.d.m0.g.a(k.l, e.identifier("Function"));
    public static final d0.e0.p.d.m0.g.a p = new d0.e0.p.d.m0.g.a(k.i, e.identifier("KFunction"));
    public final o q;
    public final e0 r;

    /* renamed from: s  reason: collision with root package name */
    public final c f3206s;
    public final int t;
    public final C0287b u = new C0287b(this);
    public final d v;
    public final List<z0> w;

    /* compiled from: FunctionClassDescriptor.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    static {
        new a(null);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public b(o oVar, e0 e0Var, c cVar, int i) {
        super(oVar, cVar.numberedClassName(i));
        m.checkNotNullParameter(oVar, "storageManager");
        m.checkNotNullParameter(e0Var, "containingDeclaration");
        m.checkNotNullParameter(cVar, "functionKind");
        this.q = oVar;
        this.r = e0Var;
        this.f3206s = cVar;
        this.t = i;
        this.v = new d(oVar, this);
        ArrayList arrayList = new ArrayList();
        IntRange intRange = new IntRange(1, i);
        ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(intRange, 10));
        Iterator<Integer> it = intRange.iterator();
        while (it.hasNext()) {
            b(arrayList, this, j1.IN_VARIANCE, m.stringPlus("P", Integer.valueOf(((c0) it).nextInt())));
            arrayList2.add(Unit.a);
        }
        b(arrayList, this, j1.OUT_VARIANCE, "R");
        this.w = u.toList(arrayList);
    }

    public static final void b(ArrayList<z0> arrayList, b bVar, j1 j1Var, String str) {
        arrayList.add(k0.createWithDefaultBound(bVar, g.f.getEMPTY(), false, j1Var, e.identifier(str), arrayList.size(), bVar.q));
    }

    @Override // d0.e0.p.d.m0.c.g1.a
    public g getAnnotations() {
        return g.f.getEMPTY();
    }

    public final int getArity() {
        return this.t;
    }

    @Override // d0.e0.p.d.m0.c.e
    public Void getCompanionObjectDescriptor() {
        return null;
    }

    @Override // d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.i
    public List<z0> getDeclaredTypeParameters() {
        return this.w;
    }

    public final c getFunctionKind() {
        return this.f3206s;
    }

    @Override // d0.e0.p.d.m0.c.e
    public f getKind() {
        return f.INTERFACE;
    }

    @Override // d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.y
    public z getModality() {
        return z.ABSTRACT;
    }

    @Override // d0.e0.p.d.m0.c.p
    public u0 getSource() {
        u0 u0Var = u0.a;
        m.checkNotNullExpressionValue(u0Var, "NO_SOURCE");
        return u0Var;
    }

    @Override // d0.e0.p.d.m0.c.h
    public d0.e0.p.d.m0.n.u0 getTypeConstructor() {
        return this.u;
    }

    @Override // d0.e0.p.d.m0.c.i1.u
    public i getUnsubstitutedMemberScope(d0.e0.p.d.m0.n.l1.g gVar) {
        m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
        return this.v;
    }

    @Override // d0.e0.p.d.m0.c.e
    public Void getUnsubstitutedPrimaryConstructor() {
        return null;
    }

    @Override // d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.q, d0.e0.p.d.m0.c.y
    public d0.e0.p.d.m0.c.u getVisibility() {
        d0.e0.p.d.m0.c.u uVar = t.e;
        m.checkNotNullExpressionValue(uVar, "PUBLIC");
        return uVar;
    }

    @Override // d0.e0.p.d.m0.c.y
    public boolean isActual() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.e
    public boolean isCompanionObject() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.e
    public boolean isData() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.y
    public boolean isExpect() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.y
    public boolean isExternal() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.e
    public boolean isFun() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.e
    public boolean isInline() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.i
    public boolean isInner() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.e
    public boolean isValue() {
        return false;
    }

    public String toString() {
        String asString = getName().asString();
        m.checkNotNullExpressionValue(asString, "name.asString()");
        return asString;
    }

    /* compiled from: FunctionClassDescriptor.kt */
    /* renamed from: d0.e0.p.d.m0.b.p.b$b  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public final class C0287b extends d0.e0.p.d.m0.n.b {
        public final /* synthetic */ b c;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public C0287b(b bVar) {
            super(bVar.q);
            m.checkNotNullParameter(bVar, "this$0");
            this.c = bVar;
        }

        @Override // d0.e0.p.d.m0.n.g
        public Collection<d0.e0.p.d.m0.n.c0> a() {
            List<d0.e0.p.d.m0.g.a> list;
            int ordinal = this.c.getFunctionKind().ordinal();
            if (ordinal == 0) {
                list = d0.t.m.listOf(b.o);
            } else if (ordinal == 1) {
                list = d0.t.m.listOf(b.o);
            } else if (ordinal == 2) {
                list = n.listOf((Object[]) new d0.e0.p.d.m0.g.a[]{b.p, new d0.e0.p.d.m0.g.a(k.l, c.Function.numberedClassName(this.c.getArity()))});
            } else if (ordinal == 3) {
                list = n.listOf((Object[]) new d0.e0.p.d.m0.g.a[]{b.p, new d0.e0.p.d.m0.g.a(k.d, c.SuspendFunction.numberedClassName(this.c.getArity()))});
            } else {
                throw new NoWhenBranchMatchedException();
            }
            d0.e0.p.d.m0.c.c0 containingDeclaration = this.c.r.getContainingDeclaration();
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(list, 10));
            for (d0.e0.p.d.m0.g.a aVar : list) {
                d0.e0.p.d.m0.c.e findClassAcrossModuleDependencies = w.findClassAcrossModuleDependencies(containingDeclaration, aVar);
                if (findClassAcrossModuleDependencies != null) {
                    List<z0> takeLast = u.takeLast(getParameters(), findClassAcrossModuleDependencies.getTypeConstructor().getParameters().size());
                    ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(takeLast, 10));
                    for (z0 z0Var : takeLast) {
                        arrayList2.add(new y0(z0Var.getDefaultType()));
                    }
                    d0 d0Var = d0.a;
                    arrayList.add(d0.simpleNotNullType(g.f.getEMPTY(), findClassAcrossModuleDependencies, arrayList2));
                } else {
                    throw new IllegalStateException(("Built-in class " + aVar + " not found").toString());
                }
            }
            return u.toList(arrayList);
        }

        @Override // d0.e0.p.d.m0.n.g
        public x0 d() {
            return x0.a.a;
        }

        @Override // d0.e0.p.d.m0.n.u0
        public List<z0> getParameters() {
            return this.c.w;
        }

        @Override // d0.e0.p.d.m0.n.u0
        public boolean isDenotable() {
            return true;
        }

        public String toString() {
            return getDeclarationDescriptor().toString();
        }

        @Override // d0.e0.p.d.m0.n.b, d0.e0.p.d.m0.n.g, d0.e0.p.d.m0.n.u0
        public b getDeclarationDescriptor() {
            return this.c;
        }
    }

    @Override // d0.e0.p.d.m0.c.e
    public List<d> getConstructors() {
        return n.emptyList();
    }

    @Override // d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.n, d0.e0.p.d.m0.c.m
    public e0 getContainingDeclaration() {
        return this.r;
    }

    @Override // d0.e0.p.d.m0.c.e
    public List<d0.e0.p.d.m0.c.e> getSealedSubclasses() {
        return n.emptyList();
    }

    @Override // d0.e0.p.d.m0.c.e
    public i.b getStaticScope() {
        return i.b.f3433b;
    }
}
