package d0.e0.p.d.m0.b.p;

import d0.e0.p.d.m0.b.k;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.e;
import d0.g0.t;
import d0.z.d.m;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* JADX WARN: Init of enum m can be incorrect */
/* JADX WARN: Init of enum n can be incorrect */
/* compiled from: FunctionClassKind.kt */
/* loaded from: classes3.dex */
public enum c {
    Function(k.l, "Function"),
    SuspendFunction(k.d, "SuspendFunction"),
    KFunction(r4, "KFunction"),
    KSuspendFunction(r4, "KSuspendFunction");
    
    public static final a j = new a(null);
    private final String classNamePrefix;
    private final b packageFqName;

    /* compiled from: FunctionClassKind.kt */
    /* loaded from: classes3.dex */
    public static final class a {

        /* compiled from: FunctionClassKind.kt */
        /* renamed from: d0.e0.p.d.m0.b.p.c$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0288a {
            public final c a;

            /* renamed from: b  reason: collision with root package name */
            public final int f3207b;

            public C0288a(c cVar, int i) {
                m.checkNotNullParameter(cVar, "kind");
                this.a = cVar;
                this.f3207b = i;
            }

            public final c component1() {
                return this.a;
            }

            public final int component2() {
                return this.f3207b;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof C0288a)) {
                    return false;
                }
                C0288a aVar = (C0288a) obj;
                return this.a == aVar.a && this.f3207b == aVar.f3207b;
            }

            public final c getKind() {
                return this.a;
            }

            public int hashCode() {
                return (this.a.hashCode() * 31) + this.f3207b;
            }

            public String toString() {
                StringBuilder R = b.d.b.a.a.R("KindWithArity(kind=");
                R.append(this.a);
                R.append(", arity=");
                return b.d.b.a.a.z(R, this.f3207b, ')');
            }
        }

        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final c byClassNamePrefix(b bVar, String str) {
            m.checkNotNullParameter(bVar, "packageFqName");
            m.checkNotNullParameter(str, "className");
            c[] values = c.values();
            for (int i = 0; i < 4; i++) {
                c cVar = values[i];
                if (m.areEqual(cVar.getPackageFqName(), bVar) && t.startsWith$default(str, cVar.getClassNamePrefix(), false, 2, null)) {
                    return cVar;
                }
            }
            return null;
        }

        public final c getFunctionalClassKind(String str, b bVar) {
            m.checkNotNullParameter(str, "className");
            m.checkNotNullParameter(bVar, "packageFqName");
            C0288a parseClassName = parseClassName(str, bVar);
            if (parseClassName == null) {
                return null;
            }
            return parseClassName.getKind();
        }

        /* JADX WARN: Removed duplicated region for block: B:23:0x0058 A[RETURN] */
        /* JADX WARN: Removed duplicated region for block: B:24:0x0059  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public final d0.e0.p.d.m0.b.p.c.a.C0288a parseClassName(java.lang.String r9, d0.e0.p.d.m0.g.b r10) {
            /*
                r8 = this;
                java.lang.String r0 = "className"
                d0.z.d.m.checkNotNullParameter(r9, r0)
                java.lang.String r0 = "packageFqName"
                d0.z.d.m.checkNotNullParameter(r10, r0)
                d0.e0.p.d.m0.b.p.c r10 = r8.byClassNamePrefix(r10, r9)
                r0 = 0
                if (r10 != 0) goto L12
                return r0
            L12:
                java.lang.String r1 = r10.getClassNamePrefix()
                int r1 = r1.length()
                java.lang.String r9 = r9.substring(r1)
                java.lang.String r1 = "(this as java.lang.String).substring(startIndex)"
                d0.z.d.m.checkNotNullExpressionValue(r9, r1)
                int r1 = r9.length()
                r2 = 1
                r3 = 0
                if (r1 != 0) goto L2d
                r1 = 1
                goto L2e
            L2d:
                r1 = 0
            L2e:
                if (r1 == 0) goto L32
            L30:
                r9 = r0
                goto L56
            L32:
                int r1 = r9.length()
                r4 = 0
                r5 = 0
            L38:
                if (r4 >= r1) goto L52
                char r6 = r9.charAt(r4)
                int r4 = r4 + 1
                int r6 = r6 + (-48)
                if (r6 < 0) goto L4a
                r7 = 9
                if (r6 > r7) goto L4a
                r7 = 1
                goto L4b
            L4a:
                r7 = 0
            L4b:
                if (r7 != 0) goto L4e
                goto L30
            L4e:
                int r5 = r5 * 10
                int r5 = r5 + r6
                goto L38
            L52:
                java.lang.Integer r9 = java.lang.Integer.valueOf(r5)
            L56:
                if (r9 != 0) goto L59
                return r0
            L59:
                int r9 = r9.intValue()
                d0.e0.p.d.m0.b.p.c$a$a r0 = new d0.e0.p.d.m0.b.p.c$a$a
                r0.<init>(r10, r9)
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.b.p.c.a.parseClassName(java.lang.String, d0.e0.p.d.m0.g.b):d0.e0.p.d.m0.b.p.c$a$a");
        }
    }

    static {
        b bVar = k.i;
    }

    c(b bVar, String str) {
        this.packageFqName = bVar;
        this.classNamePrefix = str;
    }

    public final String getClassNamePrefix() {
        return this.classNamePrefix;
    }

    public final b getPackageFqName() {
        return this.packageFqName;
    }

    public final e numberedClassName(int i) {
        e identifier = e.identifier(m.stringPlus(this.classNamePrefix, Integer.valueOf(i)));
        m.checkNotNullExpressionValue(identifier, "identifier(\"$classNamePrefix$arity\")");
        return identifier;
    }
}
