package d0.e0.p.d.m0.b.p;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.b.f;
import d0.e0.p.d.m0.b.p.c;
import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.e0;
import d0.e0.p.d.m0.c.h1.b;
import d0.e0.p.d.m0.m.o;
import d0.g0.t;
import d0.g0.w;
import d0.t.n0;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
/* compiled from: BuiltInFictitiousFunctionClassFactory.kt */
/* loaded from: classes3.dex */
public final class a implements b {
    public final o a;

    /* renamed from: b  reason: collision with root package name */
    public final c0 f3205b;

    public a(o oVar, c0 c0Var) {
        m.checkNotNullParameter(oVar, "storageManager");
        m.checkNotNullParameter(c0Var, "module");
        this.a = oVar;
        this.f3205b = c0Var;
    }

    @Override // d0.e0.p.d.m0.c.h1.b
    public e createClass(d0.e0.p.d.m0.g.a aVar) {
        m.checkNotNullParameter(aVar, "classId");
        if (aVar.isLocal() || aVar.isNestedClass()) {
            return null;
        }
        String asString = aVar.getRelativeClassName().asString();
        m.checkNotNullExpressionValue(asString, "classId.relativeClassName.asString()");
        if (!w.contains$default((CharSequence) asString, (CharSequence) "Function", false, 2, (Object) null)) {
            return null;
        }
        d0.e0.p.d.m0.g.b packageFqName = aVar.getPackageFqName();
        m.checkNotNullExpressionValue(packageFqName, "classId.packageFqName");
        c.a.C0288a parseClassName = c.j.parseClassName(asString, packageFqName);
        if (parseClassName == null) {
            return null;
        }
        c component1 = parseClassName.component1();
        int component2 = parseClassName.component2();
        List<e0> fragments = this.f3205b.getPackage(packageFqName).getFragments();
        ArrayList arrayList = new ArrayList();
        for (Object obj : fragments) {
            if (obj instanceof d0.e0.p.d.m0.b.b) {
                arrayList.add(obj);
            }
        }
        ArrayList arrayList2 = new ArrayList();
        for (Object obj2 : arrayList) {
            if (obj2 instanceof f) {
                arrayList2.add(obj2);
            }
        }
        e0 e0Var = (f) u.firstOrNull((List<? extends Object>) arrayList2);
        if (e0Var == null) {
            e0Var = (d0.e0.p.d.m0.b.b) u.first((List<? extends Object>) arrayList);
        }
        return new b(this.a, e0Var, component1, component2);
    }

    @Override // d0.e0.p.d.m0.c.h1.b
    public Collection<e> getAllContributedClassesIfPossible(d0.e0.p.d.m0.g.b bVar) {
        m.checkNotNullParameter(bVar, "packageFqName");
        return n0.emptySet();
    }

    @Override // d0.e0.p.d.m0.c.h1.b
    public boolean shouldCreateClass(d0.e0.p.d.m0.g.b bVar, d0.e0.p.d.m0.g.e eVar) {
        m.checkNotNullParameter(bVar, "packageFqName");
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        String asString = eVar.asString();
        m.checkNotNullExpressionValue(asString, "name.asString()");
        return (t.startsWith$default(asString, "Function", false, 2, null) || t.startsWith$default(asString, "KFunction", false, 2, null) || t.startsWith$default(asString, "SuspendFunction", false, 2, null) || t.startsWith$default(asString, "KSuspendFunction", false, 2, null)) && c.j.parseClassName(asString, bVar) != null;
    }
}
