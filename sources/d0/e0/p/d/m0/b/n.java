package d0.e0.p.d.m0.b;

import d0.e0.p.d.m0.g.a;
import d0.e0.p.d.m0.g.e;
import d0.z.d.m;
/* JADX WARN: Init of enum j can be incorrect */
/* JADX WARN: Init of enum k can be incorrect */
/* JADX WARN: Init of enum l can be incorrect */
/* JADX WARN: Init of enum m can be incorrect */
/* compiled from: UnsignedType.kt */
/* loaded from: classes3.dex */
public enum n {
    UBYTE(r1),
    USHORT(r2),
    UINT(r4),
    ULONG(r6);
    
    private final a arrayClassId;
    private final a classId;
    private final e typeName;

    static {
        m.checkNotNullExpressionValue(a.fromString("kotlin/UByte"), "fromString(\"kotlin/UByte\")");
        m.checkNotNullExpressionValue(a.fromString("kotlin/UShort"), "fromString(\"kotlin/UShort\")");
        m.checkNotNullExpressionValue(a.fromString("kotlin/UInt"), "fromString(\"kotlin/UInt\")");
        m.checkNotNullExpressionValue(a.fromString("kotlin/ULong"), "fromString(\"kotlin/ULong\")");
    }

    n(a aVar) {
        this.classId = aVar;
        e shortClassName = aVar.getShortClassName();
        m.checkNotNullExpressionValue(shortClassName, "classId.shortClassName");
        this.typeName = shortClassName;
        this.arrayClassId = new a(aVar.getPackageFqName(), e.identifier(m.stringPlus(shortClassName.asString(), "Array")));
    }

    public final a getArrayClassId() {
        return this.arrayClassId;
    }

    public final a getClassId() {
        return this.classId;
    }

    public final e getTypeName() {
        return this.typeName;
    }
}
