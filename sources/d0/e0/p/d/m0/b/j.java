package d0.e0.p.d.m0.b;

import d0.e0.p.d.m0.b.k;
import d0.e0.p.d.m0.c.d0;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.w;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.d.b.d;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.o0;
import d0.g0.t;
import d0.t.u;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import d0.z.d.y;
import java.util.List;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
/* compiled from: ReflectionTypes.kt */
/* loaded from: classes3.dex */
public final class j {
    public static final b a = new b(null);

    /* renamed from: b  reason: collision with root package name */
    public static final /* synthetic */ KProperty<Object>[] f3186b;
    public final d0 c;
    public final Lazy d;
    public final a e = new a(1);

    /* compiled from: ReflectionTypes.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public final int a;

        public a(int i) {
            this.a = i;
        }

        public final e getValue(j jVar, KProperty<?> kProperty) {
            m.checkNotNullParameter(jVar, "types");
            m.checkNotNullParameter(kProperty, "property");
            return j.access$find(jVar, t.capitalize(kProperty.getName()), this.a);
        }
    }

    /* compiled from: ReflectionTypes.kt */
    /* loaded from: classes3.dex */
    public static final class b {
        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final c0 createKPropertyStarType(d0.e0.p.d.m0.c.c0 c0Var) {
            m.checkNotNullParameter(c0Var, "module");
            e findClassAcrossModuleDependencies = w.findClassAcrossModuleDependencies(c0Var, k.a.Z);
            if (findClassAcrossModuleDependencies == null) {
                return null;
            }
            d0.e0.p.d.m0.n.d0 d0Var = d0.e0.p.d.m0.n.d0.a;
            g empty = g.f.getEMPTY();
            List<z0> parameters = findClassAcrossModuleDependencies.getTypeConstructor().getParameters();
            m.checkNotNullExpressionValue(parameters, "kPropertyClass.typeConstructor.parameters");
            Object single = u.single((List<? extends Object>) parameters);
            m.checkNotNullExpressionValue(single, "kPropertyClass.typeConstructor.parameters.single()");
            return d0.e0.p.d.m0.n.d0.simpleNotNullType(empty, findClassAcrossModuleDependencies, d0.t.m.listOf(new o0((z0) single)));
        }
    }

    /* compiled from: ReflectionTypes.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function0<i> {
        public final /* synthetic */ d0.e0.p.d.m0.c.c0 $module;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(d0.e0.p.d.m0.c.c0 c0Var) {
            super(0);
            this.$module = c0Var;
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final i invoke() {
            return this.$module.getPackage(k.i).getMemberScope();
        }
    }

    static {
        KProperty<Object>[] kPropertyArr = new KProperty[9];
        kPropertyArr[1] = a0.property1(new y(a0.getOrCreateKotlinClass(j.class), "kClass", "getKClass()Lorg/jetbrains/kotlin/descriptors/ClassDescriptor;"));
        kPropertyArr[2] = a0.property1(new y(a0.getOrCreateKotlinClass(j.class), "kProperty", "getKProperty()Lorg/jetbrains/kotlin/descriptors/ClassDescriptor;"));
        kPropertyArr[3] = a0.property1(new y(a0.getOrCreateKotlinClass(j.class), "kProperty0", "getKProperty0()Lorg/jetbrains/kotlin/descriptors/ClassDescriptor;"));
        kPropertyArr[4] = a0.property1(new y(a0.getOrCreateKotlinClass(j.class), "kProperty1", "getKProperty1()Lorg/jetbrains/kotlin/descriptors/ClassDescriptor;"));
        kPropertyArr[5] = a0.property1(new y(a0.getOrCreateKotlinClass(j.class), "kProperty2", "getKProperty2()Lorg/jetbrains/kotlin/descriptors/ClassDescriptor;"));
        kPropertyArr[6] = a0.property1(new y(a0.getOrCreateKotlinClass(j.class), "kMutableProperty0", "getKMutableProperty0()Lorg/jetbrains/kotlin/descriptors/ClassDescriptor;"));
        kPropertyArr[7] = a0.property1(new y(a0.getOrCreateKotlinClass(j.class), "kMutableProperty1", "getKMutableProperty1()Lorg/jetbrains/kotlin/descriptors/ClassDescriptor;"));
        kPropertyArr[8] = a0.property1(new y(a0.getOrCreateKotlinClass(j.class), "kMutableProperty2", "getKMutableProperty2()Lorg/jetbrains/kotlin/descriptors/ClassDescriptor;"));
        f3186b = kPropertyArr;
    }

    public j(d0.e0.p.d.m0.c.c0 c0Var, d0 d0Var) {
        m.checkNotNullParameter(c0Var, "module");
        m.checkNotNullParameter(d0Var, "notFoundClasses");
        this.c = d0Var;
        this.d = d0.g.lazy(d0.i.PUBLICATION, new c(c0Var));
        new a(1);
        new a(1);
        new a(2);
        new a(3);
        new a(1);
        new a(2);
        new a(3);
    }

    public static final e access$find(j jVar, String str, int i) {
        Objects.requireNonNull(jVar);
        d0.e0.p.d.m0.g.e identifier = d0.e0.p.d.m0.g.e.identifier(str);
        m.checkNotNullExpressionValue(identifier, "identifier(className)");
        h contributedClassifier = ((i) jVar.d.getValue()).getContributedClassifier(identifier, d.FROM_REFLECTION);
        e eVar = contributedClassifier instanceof e ? (e) contributedClassifier : null;
        return eVar == null ? jVar.c.getClass(new d0.e0.p.d.m0.g.a(k.i, identifier), d0.t.m.listOf(Integer.valueOf(i))) : eVar;
    }

    public final e getKClass() {
        return this.e.getValue(this, f3186b[1]);
    }
}
