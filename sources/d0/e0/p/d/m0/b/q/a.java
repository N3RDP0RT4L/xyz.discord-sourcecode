package d0.e0.p.d.m0.b.q;

import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.i1.g0;
import d0.e0.p.d.m0.c.q0;
import d0.e0.p.d.m0.c.t;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.c.x;
import d0.e0.p.d.m0.c.z;
import d0.e0.p.d.m0.k.a0.e;
import d0.e0.p.d.m0.m.o;
import d0.e0.p.d.m0.n.c0;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: CloneableClassScope.kt */
/* loaded from: classes3.dex */
public final class a extends e {
    public static final C0289a e = new C0289a(null);
    public static final d0.e0.p.d.m0.g.e f;

    /* compiled from: CloneableClassScope.kt */
    /* renamed from: d0.e0.p.d.m0.b.q.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0289a {
        public C0289a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final d0.e0.p.d.m0.g.e getCLONE_NAME() {
            return a.f;
        }
    }

    static {
        d0.e0.p.d.m0.g.e identifier = d0.e0.p.d.m0.g.e.identifier("clone");
        m.checkNotNullExpressionValue(identifier, "identifier(\"clone\")");
        f = identifier;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public a(o oVar, d0.e0.p.d.m0.c.e eVar) {
        super(oVar, eVar);
        m.checkNotNullParameter(oVar, "storageManager");
        m.checkNotNullParameter(eVar, "containingClass");
    }

    @Override // d0.e0.p.d.m0.k.a0.e
    public List<x> a() {
        g0 create = g0.create(this.c, g.f.getEMPTY(), e.getCLONE_NAME(), b.a.DECLARATION, u0.a);
        create.initialize((q0) null, this.c.getThisAsReceiverParameter(), n.emptyList(), n.emptyList(), (c0) d0.e0.p.d.m0.k.x.a.getBuiltIns(this.c).getAnyType(), z.OPEN, t.c);
        return d0.t.m.listOf(create);
    }
}
