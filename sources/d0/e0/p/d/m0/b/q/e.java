package d0.e0.p.d.m0.b.q;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.b.k;
import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.e0;
import d0.e0.p.d.m0.c.f;
import d0.e0.p.d.m0.c.i1.i;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.c.z;
import d0.e0.p.d.m0.m.j;
import d0.e0.p.d.m0.m.n;
import d0.t.m0;
import d0.t.n0;
import d0.t.u;
import d0.z.d.a0;
import d0.z.d.o;
import d0.z.d.y;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
/* compiled from: JvmBuiltInClassDescriptorFactory.kt */
/* loaded from: classes3.dex */
public final class e implements d0.e0.p.d.m0.c.h1.b {
    public static final b a = new b(null);

    /* renamed from: b  reason: collision with root package name */
    public static final /* synthetic */ KProperty<Object>[] f3210b = {a0.property1(new y(a0.getOrCreateKotlinClass(e.class), "cloneable", "getCloneable()Lorg/jetbrains/kotlin/descriptors/impl/ClassDescriptorImpl;"))};
    public static final d0.e0.p.d.m0.g.b c = k.l;
    public static final d0.e0.p.d.m0.g.e d;
    public static final d0.e0.p.d.m0.g.a e;
    public final c0 f;
    public final Function1<c0, m> g;
    public final j h;

    /* compiled from: JvmBuiltInClassDescriptorFactory.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<c0, d0.e0.p.d.m0.b.b> {
        public static final a j = new a();

        public a() {
            super(1);
        }

        public final d0.e0.p.d.m0.b.b invoke(c0 c0Var) {
            d0.z.d.m.checkNotNullParameter(c0Var, "module");
            List<e0> fragments = c0Var.getPackage(e.c).getFragments();
            ArrayList arrayList = new ArrayList();
            for (Object obj : fragments) {
                if (obj instanceof d0.e0.p.d.m0.b.b) {
                    arrayList.add(obj);
                }
            }
            return (d0.e0.p.d.m0.b.b) u.first((List<? extends Object>) arrayList);
        }
    }

    /* compiled from: JvmBuiltInClassDescriptorFactory.kt */
    /* loaded from: classes3.dex */
    public static final class b {
        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final d0.e0.p.d.m0.g.a getCLONEABLE_CLASS_ID() {
            return e.e;
        }
    }

    /* compiled from: JvmBuiltInClassDescriptorFactory.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function0<i> {
        public final /* synthetic */ d0.e0.p.d.m0.m.o $storageManager;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(d0.e0.p.d.m0.m.o oVar) {
            super(0);
            this.$storageManager = oVar;
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final i invoke() {
            i iVar = new i((m) e.this.g.invoke(e.this.f), e.d, z.ABSTRACT, f.INTERFACE, d0.t.m.listOf(e.this.f.getBuiltIns().getAnyType()), u0.a, false, this.$storageManager);
            iVar.initialize(new d0.e0.p.d.m0.b.q.a(this.$storageManager, iVar), n0.emptySet(), null);
            return iVar;
        }
    }

    static {
        d0.e0.p.d.m0.g.c cVar = k.a.d;
        d0.e0.p.d.m0.g.e shortName = cVar.shortName();
        d0.z.d.m.checkNotNullExpressionValue(shortName, "cloneable.shortName()");
        d = shortName;
        d0.e0.p.d.m0.g.a aVar = d0.e0.p.d.m0.g.a.topLevel(cVar.toSafe());
        d0.z.d.m.checkNotNullExpressionValue(aVar, "topLevel(StandardNames.FqNames.cloneable.toSafe())");
        e = aVar;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public e(d0.e0.p.d.m0.m.o oVar, c0 c0Var, Function1<? super c0, ? extends m> function1) {
        d0.z.d.m.checkNotNullParameter(oVar, "storageManager");
        d0.z.d.m.checkNotNullParameter(c0Var, "moduleDescriptor");
        d0.z.d.m.checkNotNullParameter(function1, "computeContainingDeclaration");
        this.f = c0Var;
        this.g = function1;
        this.h = oVar.createLazyValue(new c(oVar));
    }

    @Override // d0.e0.p.d.m0.c.h1.b
    public d0.e0.p.d.m0.c.e createClass(d0.e0.p.d.m0.g.a aVar) {
        d0.z.d.m.checkNotNullParameter(aVar, "classId");
        if (d0.z.d.m.areEqual(aVar, a.getCLONEABLE_CLASS_ID())) {
            return (i) n.getValue(this.h, this, f3210b[0]);
        }
        return null;
    }

    @Override // d0.e0.p.d.m0.c.h1.b
    public Collection<d0.e0.p.d.m0.c.e> getAllContributedClassesIfPossible(d0.e0.p.d.m0.g.b bVar) {
        d0.z.d.m.checkNotNullParameter(bVar, "packageFqName");
        if (d0.z.d.m.areEqual(bVar, c)) {
            return m0.setOf((i) n.getValue(this.h, this, f3210b[0]));
        }
        return n0.emptySet();
    }

    @Override // d0.e0.p.d.m0.c.h1.b
    public boolean shouldCreateClass(d0.e0.p.d.m0.g.b bVar, d0.e0.p.d.m0.g.e eVar) {
        d0.z.d.m.checkNotNullParameter(bVar, "packageFqName");
        d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        return d0.z.d.m.areEqual(eVar, d) && d0.z.d.m.areEqual(bVar, c);
    }

    public /* synthetic */ e(d0.e0.p.d.m0.m.o oVar, c0 c0Var, Function1 function1, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(oVar, c0Var, (i & 4) != 0 ? a.j : function1);
    }
}
