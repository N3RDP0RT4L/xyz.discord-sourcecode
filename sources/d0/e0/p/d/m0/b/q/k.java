package d0.e0.p.d.m0.b.q;

import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.e.a.i0.l.f;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.p.b;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
/* compiled from: JvmBuiltInsCustomizer.kt */
/* loaded from: classes3.dex */
public final class k implements b.c<e> {
    public final /* synthetic */ g a;

    public k(g gVar) {
        this.a = gVar;
    }

    public final Iterable<e> getNeighbors(e eVar) {
        Collection<c0> supertypes = eVar.getTypeConstructor().getSupertypes();
        m.checkNotNullExpressionValue(supertypes, "it.typeConstructor.supertypes");
        g gVar = this.a;
        ArrayList arrayList = new ArrayList();
        for (c0 c0Var : supertypes) {
            h declarationDescriptor = c0Var.getConstructor().getDeclarationDescriptor();
            f fVar = null;
            h original = declarationDescriptor == null ? null : declarationDescriptor.getOriginal();
            e eVar2 = original instanceof e ? (e) original : null;
            if (eVar2 != null) {
                fVar = gVar.a(eVar2);
            }
            if (fVar != null) {
                arrayList.add(fVar);
            }
        }
        return arrayList;
    }
}
