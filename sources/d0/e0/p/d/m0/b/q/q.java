package d0.e0.p.d.m0.b.q;

import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.o1.a;
import d0.e0.p.d.m0.n.v0;
import d0.t.h0;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
/* compiled from: mappingUtil.kt */
/* loaded from: classes3.dex */
public final class q {
    public static final v0 createMappedTypeParametersSubstitution(e eVar, e eVar2) {
        m.checkNotNullParameter(eVar, "from");
        m.checkNotNullParameter(eVar2, "to");
        eVar.getDeclaredTypeParameters().size();
        eVar2.getDeclaredTypeParameters().size();
        v0.a aVar = v0.f3514b;
        List<z0> declaredTypeParameters = eVar.getDeclaredTypeParameters();
        m.checkNotNullExpressionValue(declaredTypeParameters, "from.declaredTypeParameters");
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(declaredTypeParameters, 10));
        for (z0 z0Var : declaredTypeParameters) {
            arrayList.add(z0Var.getTypeConstructor());
        }
        List<z0> declaredTypeParameters2 = eVar2.getDeclaredTypeParameters();
        m.checkNotNullExpressionValue(declaredTypeParameters2, "to.declaredTypeParameters");
        ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(declaredTypeParameters2, 10));
        for (z0 z0Var2 : declaredTypeParameters2) {
            j0 defaultType = z0Var2.getDefaultType();
            m.checkNotNullExpressionValue(defaultType, "it.defaultType");
            arrayList2.add(a.asTypeProjection(defaultType));
        }
        return v0.a.createByConstructorsMap$default(aVar, h0.toMap(u.zip(arrayList, arrayList2)), false, 2, null);
    }
}
