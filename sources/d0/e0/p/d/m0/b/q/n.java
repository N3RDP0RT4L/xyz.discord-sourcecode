package d0.e0.p.d.m0.b.q;

import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.e;
import d0.z.d.o;
import kotlin.jvm.functions.Function1;
/* compiled from: JvmBuiltInsCustomizer.kt */
/* loaded from: classes3.dex */
public final class n extends o implements Function1<b, Boolean> {
    public final /* synthetic */ g this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public n(g gVar) {
        super(1);
        this.this$0 = gVar;
    }

    public final Boolean invoke(b bVar) {
        boolean z2;
        d dVar;
        if (bVar.getKind() == b.a.DECLARATION) {
            dVar = this.this$0.c;
            if (dVar.isMutable((e) bVar.getContainingDeclaration())) {
                z2 = true;
                return Boolean.valueOf(z2);
            }
        }
        z2 = false;
        return Boolean.valueOf(z2);
    }
}
