package d0.e0.p.d.m0.b.q;

import d0.e0.p.d.m0.b.h;
import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.m.j;
import d0.e0.p.d.m0.m.n;
import d0.t.u;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import d0.z.d.y;
import kotlin.jvm.functions.Function0;
import kotlin.reflect.KProperty;
/* compiled from: JvmBuiltIns.kt */
/* loaded from: classes3.dex */
public final class f extends h {
    public static final /* synthetic */ KProperty<Object>[] f = {a0.property1(new y(a0.getOrCreateKotlinClass(f.class), "customizer", "getCustomizer()Lorg/jetbrains/kotlin/builtins/jvm/JvmBuiltInsCustomizer;"))};
    public Function0<b> g;
    public final j h;

    /* compiled from: JvmBuiltIns.kt */
    /* loaded from: classes3.dex */
    public enum a {
        FROM_DEPENDENCIES,
        FROM_CLASS_LOADER,
        FALLBACK
    }

    /* compiled from: JvmBuiltIns.kt */
    /* loaded from: classes3.dex */
    public static final class b {
        public final c0 a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f3211b;

        public b(c0 c0Var, boolean z2) {
            m.checkNotNullParameter(c0Var, "ownerModuleDescriptor");
            this.a = c0Var;
            this.f3211b = z2;
        }

        public final c0 getOwnerModuleDescriptor() {
            return this.a;
        }

        public final boolean isAdditionalBuiltInsFeatureSupported() {
            return this.f3211b;
        }
    }

    /* compiled from: JvmBuiltIns.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function0<g> {
        public final /* synthetic */ d0.e0.p.d.m0.m.o $storageManager;

        /* compiled from: JvmBuiltIns.kt */
        /* loaded from: classes3.dex */
        public static final class a extends o implements Function0<b> {
            public final /* synthetic */ f this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public a(f fVar) {
                super(0);
                this.this$0 = fVar;
            }

            /* JADX WARN: Can't rename method to resolve collision */
            @Override // kotlin.jvm.functions.Function0
            public final b invoke() {
                Function0 function0 = this.this$0.g;
                if (function0 != null) {
                    b bVar = (b) function0.invoke();
                    this.this$0.g = null;
                    return bVar;
                }
                throw new AssertionError("JvmBuiltins instance has not been initialized properly");
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(d0.e0.p.d.m0.m.o oVar) {
            super(0);
            this.$storageManager = oVar;
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final g invoke() {
            d0.e0.p.d.m0.c.i1.y builtInsModule = f.this.getBuiltInsModule();
            m.checkNotNullExpressionValue(builtInsModule, "builtInsModule");
            return new g(builtInsModule, this.$storageManager, new a(f.this));
        }
    }

    /* compiled from: JvmBuiltIns.kt */
    /* loaded from: classes3.dex */
    public static final class d extends o implements Function0<b> {
        public final /* synthetic */ boolean $isAdditionalBuiltInsFeatureSupported;
        public final /* synthetic */ c0 $moduleDescriptor;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public d(c0 c0Var, boolean z2) {
            super(0);
            this.$moduleDescriptor = c0Var;
            this.$isAdditionalBuiltInsFeatureSupported = z2;
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final b invoke() {
            return new b(this.$moduleDescriptor, this.$isAdditionalBuiltInsFeatureSupported);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public f(d0.e0.p.d.m0.m.o oVar, a aVar) {
        super(oVar);
        m.checkNotNullParameter(oVar, "storageManager");
        m.checkNotNullParameter(aVar, "kind");
        this.h = oVar.createLazyValue(new c(oVar));
        int ordinal = aVar.ordinal();
        if (ordinal == 1) {
            d(false);
        } else if (ordinal == 2) {
            d(true);
        }
    }

    @Override // d0.e0.p.d.m0.b.h
    public d0.e0.p.d.m0.c.h1.a e() {
        return getCustomizer();
    }

    @Override // d0.e0.p.d.m0.b.h
    public Iterable g() {
        Iterable<d0.e0.p.d.m0.c.h1.b> g = super.g();
        m.checkNotNullExpressionValue(g, "super.getClassDescriptorFactories()");
        d0.e0.p.d.m0.m.o oVar = this.e;
        if (oVar != null) {
            m.checkNotNullExpressionValue(oVar, "storageManager");
            d0.e0.p.d.m0.c.i1.y builtInsModule = getBuiltInsModule();
            m.checkNotNullExpressionValue(builtInsModule, "builtInsModule");
            return u.plus(g, new e(oVar, builtInsModule, null, 4, null));
        }
        h.a(5);
        throw null;
    }

    public final g getCustomizer() {
        return (g) n.getValue(this.h, this, f[0]);
    }

    @Override // d0.e0.p.d.m0.b.h
    public d0.e0.p.d.m0.c.h1.c h() {
        return getCustomizer();
    }

    public final void initialize(c0 c0Var, boolean z2) {
        m.checkNotNullParameter(c0Var, "moduleDescriptor");
        setPostponedSettingsComputation(new d(c0Var, z2));
    }

    public final void setPostponedSettingsComputation(Function0<b> function0) {
        m.checkNotNullParameter(function0, "computation");
        Function0<b> function02 = this.g;
        this.g = function0;
    }
}
