package d0.e0.p.d.m0.b.q;

import d0.e0.p.d.m0.b.h;
import d0.e0.p.d.m0.b.k;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.k.x.a;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.e1;
import d0.t.m0;
import d0.t.n;
import d0.t.n0;
import d0.z.d.m;
import java.util.Collection;
/* compiled from: JavaToKotlinClassMapper.kt */
/* loaded from: classes3.dex */
public final class d {
    public static final d a = new d();

    public static /* synthetic */ e mapJavaToKotlin$default(d dVar, b bVar, h hVar, Integer num, int i, Object obj) {
        if ((i & 4) != 0) {
            num = null;
        }
        return dVar.mapJavaToKotlin(bVar, hVar, num);
    }

    public final e convertMutableToReadOnly(e eVar) {
        m.checkNotNullParameter(eVar, "mutable");
        b mutableToReadOnly = c.a.mutableToReadOnly(d0.e0.p.d.m0.k.e.getFqName(eVar));
        if (mutableToReadOnly != null) {
            e builtInClassByFqName = a.getBuiltIns(eVar).getBuiltInClassByFqName(mutableToReadOnly);
            m.checkNotNullExpressionValue(builtInClassByFqName, "descriptor.builtIns.getBuiltInClassByFqName(oppositeClassFqName)");
            return builtInClassByFqName;
        }
        throw new IllegalArgumentException("Given class " + eVar + " is not a mutable collection");
    }

    public final e convertReadOnlyToMutable(e eVar) {
        m.checkNotNullParameter(eVar, "readOnly");
        b readOnlyToMutable = c.a.readOnlyToMutable(d0.e0.p.d.m0.k.e.getFqName(eVar));
        if (readOnlyToMutable != null) {
            e builtInClassByFqName = a.getBuiltIns(eVar).getBuiltInClassByFqName(readOnlyToMutable);
            m.checkNotNullExpressionValue(builtInClassByFqName, "descriptor.builtIns.getBuiltInClassByFqName(oppositeClassFqName)");
            return builtInClassByFqName;
        }
        throw new IllegalArgumentException("Given class " + eVar + " is not a read-only collection");
    }

    public final boolean isMutable(e eVar) {
        m.checkNotNullParameter(eVar, "mutable");
        return c.a.isMutable(d0.e0.p.d.m0.k.e.getFqName(eVar));
    }

    public final boolean isReadOnly(e eVar) {
        m.checkNotNullParameter(eVar, "readOnly");
        return c.a.isReadOnly(d0.e0.p.d.m0.k.e.getFqName(eVar));
    }

    public final e mapJavaToKotlin(b bVar, h hVar, Integer num) {
        d0.e0.p.d.m0.g.a aVar;
        m.checkNotNullParameter(bVar, "fqName");
        m.checkNotNullParameter(hVar, "builtIns");
        if (num == null || !m.areEqual(bVar, c.a.getFUNCTION_N_FQ_NAME())) {
            aVar = c.a.mapJavaToKotlin(bVar);
        } else {
            k kVar = k.a;
            aVar = k.getFunctionClassId(num.intValue());
        }
        if (aVar != null) {
            return hVar.getBuiltInClassByFqName(aVar.asSingleFqName());
        }
        return null;
    }

    public final Collection<e> mapPlatformClass(b bVar, h hVar) {
        m.checkNotNullParameter(bVar, "fqName");
        m.checkNotNullParameter(hVar, "builtIns");
        e mapJavaToKotlin$default = mapJavaToKotlin$default(this, bVar, hVar, null, 4, null);
        if (mapJavaToKotlin$default == null) {
            return n0.emptySet();
        }
        b readOnlyToMutable = c.a.readOnlyToMutable(a.getFqNameUnsafe(mapJavaToKotlin$default));
        if (readOnlyToMutable == null) {
            return m0.setOf(mapJavaToKotlin$default);
        }
        e builtInClassByFqName = hVar.getBuiltInClassByFqName(readOnlyToMutable);
        m.checkNotNullExpressionValue(builtInClassByFqName, "builtIns.getBuiltInClassByFqName(kotlinMutableAnalogFqName)");
        return n.listOf((Object[]) new e[]{mapJavaToKotlin$default, builtInClassByFqName});
    }

    public final boolean isMutable(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "type");
        e classDescriptor = e1.getClassDescriptor(c0Var);
        return classDescriptor != null && isMutable(classDescriptor);
    }

    public final boolean isReadOnly(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "type");
        e classDescriptor = e1.getClassDescriptor(c0Var);
        return classDescriptor != null && isReadOnly(classDescriptor);
    }
}
