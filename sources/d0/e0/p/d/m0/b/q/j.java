package d0.e0.p.d.m0.b.q;

import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.e.a.g0.g;
import d0.e0.p.d.m0.e.a.i0.l.f;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.jvm.functions.Function0;
/* compiled from: JvmBuiltInsCustomizer.kt */
/* loaded from: classes3.dex */
public final class j extends o implements Function0<e> {
    public final /* synthetic */ f $javaAnalogueDescriptor;
    public final /* synthetic */ e $kotlinMutableClassIfContainer;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public j(f fVar, e eVar) {
        super(0);
        this.$javaAnalogueDescriptor = fVar;
        this.$kotlinMutableClassIfContainer = eVar;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final e invoke() {
        f fVar = this.$javaAnalogueDescriptor;
        g gVar = g.a;
        m.checkNotNullExpressionValue(gVar, "EMPTY");
        return fVar.copy$descriptors_jvm(gVar, this.$kotlinMutableClassIfContainer);
    }
}
