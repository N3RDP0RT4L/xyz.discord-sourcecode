package d0.e0.p.d.m0.b.q;

import d0.e0.p.d.m0.b.h;
import d0.e0.p.d.m0.c.h1.c;
import d0.e0.p.d.m0.m.f;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: JvmBuiltInsCustomizer.kt */
/* loaded from: classes3.dex */
public final class b extends h {
    public static final a f = new a(null);
    public static final h g = new b();

    /* compiled from: JvmBuiltInsCustomizer.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final h getInstance() {
            return b.g;
        }
    }

    public b() {
        super(new f("FallbackBuiltIns"));
        d(true);
    }

    @Override // d0.e0.p.d.m0.b.h
    public /* bridge */ /* synthetic */ c h() {
        return c.a.a;
    }
}
