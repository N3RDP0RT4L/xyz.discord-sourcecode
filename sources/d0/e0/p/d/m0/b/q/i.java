package d0.e0.p.d.m0.b.q;

import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.j0;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.jvm.functions.Function0;
/* compiled from: JvmBuiltInsCustomizer.kt */
/* loaded from: classes3.dex */
public final class i extends o implements Function0<c0> {
    public final /* synthetic */ g this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public i(g gVar) {
        super(0);
        this.this$0 = gVar;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final c0 invoke() {
        j0 anyType = this.this$0.f3212b.getBuiltIns().getAnyType();
        m.checkNotNullExpressionValue(anyType, "moduleDescriptor.builtIns.anyType");
        return anyType;
    }
}
