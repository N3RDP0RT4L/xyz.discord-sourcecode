package d0.e0.p.d.m0.b.q;

import andhook.lib.xposed.ClassUtils;
import d0.e0.p.d.m0.b.i;
import d0.e0.p.d.m0.b.k;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.d;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.g.g;
import d0.g0.s;
import d0.g0.w;
import d0.t.n;
import d0.z.d.m;
import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
/* compiled from: JavaToKotlinClassMap.kt */
/* loaded from: classes3.dex */
public final class c {
    public static final c a;

    /* renamed from: b  reason: collision with root package name */
    public static final String f3208b;
    public static final String c;
    public static final String d;
    public static final String e;
    public static final d0.e0.p.d.m0.g.a f;
    public static final b g;
    public static final d0.e0.p.d.m0.g.a h;
    public static final HashMap<d0.e0.p.d.m0.g.c, d0.e0.p.d.m0.g.a> i = new HashMap<>();
    public static final HashMap<d0.e0.p.d.m0.g.c, d0.e0.p.d.m0.g.a> j = new HashMap<>();
    public static final HashMap<d0.e0.p.d.m0.g.c, b> k = new HashMap<>();
    public static final HashMap<d0.e0.p.d.m0.g.c, b> l = new HashMap<>();
    public static final List<a> m;

    /* compiled from: JavaToKotlinClassMap.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public final d0.e0.p.d.m0.g.a a;

        /* renamed from: b  reason: collision with root package name */
        public final d0.e0.p.d.m0.g.a f3209b;
        public final d0.e0.p.d.m0.g.a c;

        public a(d0.e0.p.d.m0.g.a aVar, d0.e0.p.d.m0.g.a aVar2, d0.e0.p.d.m0.g.a aVar3) {
            m.checkNotNullParameter(aVar, "javaClass");
            m.checkNotNullParameter(aVar2, "kotlinReadOnly");
            m.checkNotNullParameter(aVar3, "kotlinMutable");
            this.a = aVar;
            this.f3209b = aVar2;
            this.c = aVar3;
        }

        public final d0.e0.p.d.m0.g.a component1() {
            return this.a;
        }

        public final d0.e0.p.d.m0.g.a component2() {
            return this.f3209b;
        }

        public final d0.e0.p.d.m0.g.a component3() {
            return this.c;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return m.areEqual(this.a, aVar.a) && m.areEqual(this.f3209b, aVar.f3209b) && m.areEqual(this.c, aVar.c);
        }

        public final d0.e0.p.d.m0.g.a getJavaClass() {
            return this.a;
        }

        public int hashCode() {
            int hashCode = this.f3209b.hashCode();
            return this.c.hashCode() + ((hashCode + (this.a.hashCode() * 31)) * 31);
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("PlatformMutabilityMapping(javaClass=");
            R.append(this.a);
            R.append(", kotlinReadOnly=");
            R.append(this.f3209b);
            R.append(", kotlinMutable=");
            R.append(this.c);
            R.append(')');
            return R.toString();
        }
    }

    static {
        c cVar = new c();
        a = cVar;
        StringBuilder sb = new StringBuilder();
        d0.e0.p.d.m0.b.p.c cVar2 = d0.e0.p.d.m0.b.p.c.Function;
        sb.append(cVar2.getPackageFqName().toString());
        sb.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
        sb.append(cVar2.getClassNamePrefix());
        f3208b = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        d0.e0.p.d.m0.b.p.c cVar3 = d0.e0.p.d.m0.b.p.c.KFunction;
        sb2.append(cVar3.getPackageFqName().toString());
        sb2.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
        sb2.append(cVar3.getClassNamePrefix());
        c = sb2.toString();
        StringBuilder sb3 = new StringBuilder();
        d0.e0.p.d.m0.b.p.c cVar4 = d0.e0.p.d.m0.b.p.c.SuspendFunction;
        sb3.append(cVar4.getPackageFqName().toString());
        sb3.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
        sb3.append(cVar4.getClassNamePrefix());
        d = sb3.toString();
        StringBuilder sb4 = new StringBuilder();
        d0.e0.p.d.m0.b.p.c cVar5 = d0.e0.p.d.m0.b.p.c.KSuspendFunction;
        sb4.append(cVar5.getPackageFqName().toString());
        sb4.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
        sb4.append(cVar5.getClassNamePrefix());
        e = sb4.toString();
        d0.e0.p.d.m0.g.a aVar = d0.e0.p.d.m0.g.a.topLevel(new b("kotlin.jvm.functions.FunctionN"));
        m.checkNotNullExpressionValue(aVar, "topLevel(FqName(\"kotlin.jvm.functions.FunctionN\"))");
        f = aVar;
        b asSingleFqName = aVar.asSingleFqName();
        m.checkNotNullExpressionValue(asSingleFqName, "FUNCTION_N_CLASS_ID.asSingleFqName()");
        g = asSingleFqName;
        d0.e0.p.d.m0.g.a aVar2 = d0.e0.p.d.m0.g.a.topLevel(new b("kotlin.reflect.KFunction"));
        m.checkNotNullExpressionValue(aVar2, "topLevel(FqName(\"kotlin.reflect.KFunction\"))");
        h = aVar2;
        m.checkNotNullExpressionValue(d0.e0.p.d.m0.g.a.topLevel(new b("kotlin.reflect.KClass")), "topLevel(FqName(\"kotlin.reflect.KClass\"))");
        cVar.e(Class.class);
        d0.e0.p.d.m0.g.a aVar3 = d0.e0.p.d.m0.g.a.topLevel(k.a.I);
        m.checkNotNullExpressionValue(aVar3, "topLevel(FqNames.iterable)");
        b bVar = k.a.Q;
        b packageFqName = aVar3.getPackageFqName();
        b packageFqName2 = aVar3.getPackageFqName();
        m.checkNotNullExpressionValue(packageFqName2, "kotlinReadOnly.packageFqName");
        b tail = d.tail(bVar, packageFqName2);
        int i2 = 0;
        d0.e0.p.d.m0.g.a aVar4 = new d0.e0.p.d.m0.g.a(packageFqName, tail, false);
        d0.e0.p.d.m0.g.a aVar5 = d0.e0.p.d.m0.g.a.topLevel(k.a.H);
        m.checkNotNullExpressionValue(aVar5, "topLevel(FqNames.iterator)");
        b bVar2 = k.a.P;
        b packageFqName3 = aVar5.getPackageFqName();
        b packageFqName4 = aVar5.getPackageFqName();
        m.checkNotNullExpressionValue(packageFqName4, "kotlinReadOnly.packageFqName");
        d0.e0.p.d.m0.g.a aVar6 = new d0.e0.p.d.m0.g.a(packageFqName3, d.tail(bVar2, packageFqName4), false);
        d0.e0.p.d.m0.g.a aVar7 = d0.e0.p.d.m0.g.a.topLevel(k.a.J);
        m.checkNotNullExpressionValue(aVar7, "topLevel(FqNames.collection)");
        b bVar3 = k.a.R;
        b packageFqName5 = aVar7.getPackageFqName();
        b packageFqName6 = aVar7.getPackageFqName();
        m.checkNotNullExpressionValue(packageFqName6, "kotlinReadOnly.packageFqName");
        d0.e0.p.d.m0.g.a aVar8 = new d0.e0.p.d.m0.g.a(packageFqName5, d.tail(bVar3, packageFqName6), false);
        d0.e0.p.d.m0.g.a aVar9 = d0.e0.p.d.m0.g.a.topLevel(k.a.K);
        m.checkNotNullExpressionValue(aVar9, "topLevel(FqNames.list)");
        b bVar4 = k.a.S;
        b packageFqName7 = aVar9.getPackageFqName();
        b packageFqName8 = aVar9.getPackageFqName();
        m.checkNotNullExpressionValue(packageFqName8, "kotlinReadOnly.packageFqName");
        d0.e0.p.d.m0.g.a aVar10 = new d0.e0.p.d.m0.g.a(packageFqName7, d.tail(bVar4, packageFqName8), false);
        d0.e0.p.d.m0.g.a aVar11 = d0.e0.p.d.m0.g.a.topLevel(k.a.M);
        m.checkNotNullExpressionValue(aVar11, "topLevel(FqNames.set)");
        b bVar5 = k.a.U;
        b packageFqName9 = aVar11.getPackageFqName();
        b packageFqName10 = aVar11.getPackageFqName();
        m.checkNotNullExpressionValue(packageFqName10, "kotlinReadOnly.packageFqName");
        d0.e0.p.d.m0.g.a aVar12 = new d0.e0.p.d.m0.g.a(packageFqName9, d.tail(bVar5, packageFqName10), false);
        d0.e0.p.d.m0.g.a aVar13 = d0.e0.p.d.m0.g.a.topLevel(k.a.L);
        m.checkNotNullExpressionValue(aVar13, "topLevel(FqNames.listIterator)");
        b bVar6 = k.a.T;
        b packageFqName11 = aVar13.getPackageFqName();
        b packageFqName12 = aVar13.getPackageFqName();
        m.checkNotNullExpressionValue(packageFqName12, "kotlinReadOnly.packageFqName");
        d0.e0.p.d.m0.g.a aVar14 = new d0.e0.p.d.m0.g.a(packageFqName11, d.tail(bVar6, packageFqName12), false);
        b bVar7 = k.a.N;
        d0.e0.p.d.m0.g.a aVar15 = d0.e0.p.d.m0.g.a.topLevel(bVar7);
        m.checkNotNullExpressionValue(aVar15, "topLevel(FqNames.map)");
        b bVar8 = k.a.V;
        b packageFqName13 = aVar15.getPackageFqName();
        b packageFqName14 = aVar15.getPackageFqName();
        m.checkNotNullExpressionValue(packageFqName14, "kotlinReadOnly.packageFqName");
        d0.e0.p.d.m0.g.a aVar16 = new d0.e0.p.d.m0.g.a(packageFqName13, d.tail(bVar8, packageFqName14), false);
        d0.e0.p.d.m0.g.a createNestedClassId = d0.e0.p.d.m0.g.a.topLevel(bVar7).createNestedClassId(k.a.O.shortName());
        m.checkNotNullExpressionValue(createNestedClassId, "topLevel(FqNames.map).createNestedClassId(FqNames.mapEntry.shortName())");
        b bVar9 = k.a.W;
        b packageFqName15 = createNestedClassId.getPackageFqName();
        b packageFqName16 = createNestedClassId.getPackageFqName();
        m.checkNotNullExpressionValue(packageFqName16, "kotlinReadOnly.packageFqName");
        List<a> listOf = n.listOf((Object[]) new a[]{new a(cVar.e(Iterable.class), aVar3, aVar4), new a(cVar.e(Iterator.class), aVar5, aVar6), new a(cVar.e(Collection.class), aVar7, aVar8), new a(cVar.e(List.class), aVar9, aVar10), new a(cVar.e(Set.class), aVar11, aVar12), new a(cVar.e(ListIterator.class), aVar13, aVar14), new a(cVar.e(Map.class), aVar15, aVar16), new a(cVar.e(Map.Entry.class), createNestedClassId, new d0.e0.p.d.m0.g.a(packageFqName15, d.tail(bVar9, packageFqName16), false))});
        m = listOf;
        cVar.d(Object.class, k.a.f3189b);
        cVar.d(String.class, k.a.g);
        cVar.d(CharSequence.class, k.a.f);
        cVar.c(Throwable.class, k.a.f3199s);
        cVar.d(Cloneable.class, k.a.d);
        cVar.d(Number.class, k.a.q);
        cVar.c(Comparable.class, k.a.t);
        cVar.d(Enum.class, k.a.r);
        cVar.c(Annotation.class, k.a.f3202z);
        for (a aVar17 : listOf) {
            c cVar6 = a;
            Objects.requireNonNull(cVar6);
            d0.e0.p.d.m0.g.a component1 = aVar17.component1();
            d0.e0.p.d.m0.g.a component2 = aVar17.component2();
            d0.e0.p.d.m0.g.a component3 = aVar17.component3();
            cVar6.a(component1, component2);
            b asSingleFqName2 = component3.asSingleFqName();
            m.checkNotNullExpressionValue(asSingleFqName2, "mutableClassId.asSingleFqName()");
            HashMap<d0.e0.p.d.m0.g.c, d0.e0.p.d.m0.g.a> hashMap = j;
            d0.e0.p.d.m0.g.c unsafe = asSingleFqName2.toUnsafe();
            m.checkNotNullExpressionValue(unsafe, "kotlinFqNameUnsafe.toUnsafe()");
            hashMap.put(unsafe, component1);
            b asSingleFqName3 = component2.asSingleFqName();
            m.checkNotNullExpressionValue(asSingleFqName3, "readOnlyClassId.asSingleFqName()");
            b asSingleFqName4 = component3.asSingleFqName();
            m.checkNotNullExpressionValue(asSingleFqName4, "mutableClassId.asSingleFqName()");
            HashMap<d0.e0.p.d.m0.g.c, b> hashMap2 = k;
            d0.e0.p.d.m0.g.c unsafe2 = component3.asSingleFqName().toUnsafe();
            m.checkNotNullExpressionValue(unsafe2, "mutableClassId.asSingleFqName().toUnsafe()");
            hashMap2.put(unsafe2, asSingleFqName3);
            HashMap<d0.e0.p.d.m0.g.c, b> hashMap3 = l;
            d0.e0.p.d.m0.g.c unsafe3 = asSingleFqName3.toUnsafe();
            m.checkNotNullExpressionValue(unsafe3, "readOnlyFqName.toUnsafe()");
            hashMap3.put(unsafe3, asSingleFqName4);
        }
        d0.e0.p.d.m0.k.y.d[] values = d0.e0.p.d.m0.k.y.d.values();
        int i3 = 0;
        while (i3 < 8) {
            d0.e0.p.d.m0.k.y.d dVar = values[i3];
            i3++;
            c cVar7 = a;
            d0.e0.p.d.m0.g.a aVar18 = d0.e0.p.d.m0.g.a.topLevel(dVar.getWrapperFqName());
            m.checkNotNullExpressionValue(aVar18, "topLevel(jvmType.wrapperFqName)");
            k kVar = k.a;
            i primitiveType = dVar.getPrimitiveType();
            m.checkNotNullExpressionValue(primitiveType, "jvmType.primitiveType");
            d0.e0.p.d.m0.g.a aVar19 = d0.e0.p.d.m0.g.a.topLevel(k.getPrimitiveFqName(primitiveType));
            m.checkNotNullExpressionValue(aVar19, "topLevel(StandardNames.getPrimitiveFqName(jvmType.primitiveType))");
            cVar7.a(aVar18, aVar19);
        }
        for (d0.e0.p.d.m0.g.a aVar20 : d0.e0.p.d.m0.b.c.a.allClassesWithIntrinsicCompanions()) {
            c cVar8 = a;
            StringBuilder R = b.d.b.a.a.R("kotlin.jvm.internal.");
            R.append(aVar20.getShortClassName().asString());
            R.append("CompanionObject");
            d0.e0.p.d.m0.g.a aVar21 = d0.e0.p.d.m0.g.a.topLevel(new b(R.toString()));
            m.checkNotNullExpressionValue(aVar21, "topLevel(FqName(\"kotlin.jvm.internal.\" + classId.shortClassName.asString() + \"CompanionObject\"))");
            d0.e0.p.d.m0.g.a createNestedClassId2 = aVar20.createNestedClassId(g.f3397b);
            m.checkNotNullExpressionValue(createNestedClassId2, "classId.createNestedClassId(SpecialNames.DEFAULT_NAME_FOR_COMPANION_OBJECT)");
            cVar8.a(aVar21, createNestedClassId2);
        }
        int i4 = 0;
        while (true) {
            int i5 = i4 + 1;
            c cVar9 = a;
            d0.e0.p.d.m0.g.a aVar22 = d0.e0.p.d.m0.g.a.topLevel(new b(m.stringPlus("kotlin.jvm.functions.Function", Integer.valueOf(i4))));
            m.checkNotNullExpressionValue(aVar22, "topLevel(FqName(\"kotlin.jvm.functions.Function$i\"))");
            k kVar2 = k.a;
            cVar9.a(aVar22, k.getFunctionClassId(i4));
            cVar9.b(new b(m.stringPlus(c, Integer.valueOf(i4))), h);
            if (i5 >= 23) {
                break;
            }
            i4 = i5;
        }
        while (true) {
            int i6 = i2 + 1;
            d0.e0.p.d.m0.b.p.c cVar10 = d0.e0.p.d.m0.b.p.c.KSuspendFunction;
            c cVar11 = a;
            cVar11.b(new b(m.stringPlus(cVar10.getPackageFqName().toString() + ClassUtils.PACKAGE_SEPARATOR_CHAR + cVar10.getClassNamePrefix(), Integer.valueOf(i2))), h);
            if (i6 >= 22) {
                b safe = k.a.c.toSafe();
                m.checkNotNullExpressionValue(safe, "nothing.toSafe()");
                cVar11.b(safe, cVar11.e(Void.class));
                return;
            }
            i2 = i6;
        }
    }

    public final void a(d0.e0.p.d.m0.g.a aVar, d0.e0.p.d.m0.g.a aVar2) {
        HashMap<d0.e0.p.d.m0.g.c, d0.e0.p.d.m0.g.a> hashMap = i;
        d0.e0.p.d.m0.g.c unsafe = aVar.asSingleFqName().toUnsafe();
        m.checkNotNullExpressionValue(unsafe, "javaClassId.asSingleFqName().toUnsafe()");
        hashMap.put(unsafe, aVar2);
        b asSingleFqName = aVar2.asSingleFqName();
        m.checkNotNullExpressionValue(asSingleFqName, "kotlinClassId.asSingleFqName()");
        HashMap<d0.e0.p.d.m0.g.c, d0.e0.p.d.m0.g.a> hashMap2 = j;
        d0.e0.p.d.m0.g.c unsafe2 = asSingleFqName.toUnsafe();
        m.checkNotNullExpressionValue(unsafe2, "kotlinFqNameUnsafe.toUnsafe()");
        hashMap2.put(unsafe2, aVar);
    }

    public final void b(b bVar, d0.e0.p.d.m0.g.a aVar) {
        HashMap<d0.e0.p.d.m0.g.c, d0.e0.p.d.m0.g.a> hashMap = j;
        d0.e0.p.d.m0.g.c unsafe = bVar.toUnsafe();
        m.checkNotNullExpressionValue(unsafe, "kotlinFqNameUnsafe.toUnsafe()");
        hashMap.put(unsafe, aVar);
    }

    public final void c(Class<?> cls, b bVar) {
        d0.e0.p.d.m0.g.a e2 = e(cls);
        d0.e0.p.d.m0.g.a aVar = d0.e0.p.d.m0.g.a.topLevel(bVar);
        m.checkNotNullExpressionValue(aVar, "topLevel(kotlinFqName)");
        a(e2, aVar);
    }

    public final void d(Class<?> cls, d0.e0.p.d.m0.g.c cVar) {
        b safe = cVar.toSafe();
        m.checkNotNullExpressionValue(safe, "kotlinFqName.toSafe()");
        c(cls, safe);
    }

    public final d0.e0.p.d.m0.g.a e(Class<?> cls) {
        if (!cls.isPrimitive()) {
            cls.isArray();
        }
        Class<?> declaringClass = cls.getDeclaringClass();
        if (declaringClass == null) {
            d0.e0.p.d.m0.g.a aVar = d0.e0.p.d.m0.g.a.topLevel(new b(cls.getCanonicalName()));
            m.checkNotNullExpressionValue(aVar, "topLevel(FqName(clazz.canonicalName))");
            return aVar;
        }
        d0.e0.p.d.m0.g.a createNestedClassId = e(declaringClass).createNestedClassId(e.identifier(cls.getSimpleName()));
        m.checkNotNullExpressionValue(createNestedClassId, "classId(outer).createNestedClassId(Name.identifier(clazz.simpleName))");
        return createNestedClassId;
    }

    public final boolean f(d0.e0.p.d.m0.g.c cVar, String str) {
        Integer intOrNull;
        String asString = cVar.asString();
        m.checkNotNullExpressionValue(asString, "kotlinFqName.asString()");
        String substringAfter = w.substringAfter(asString, str, "");
        return (substringAfter.length() > 0) && !w.startsWith$default((CharSequence) substringAfter, '0', false, 2, (Object) null) && (intOrNull = s.toIntOrNull(substringAfter)) != null && intOrNull.intValue() >= 23;
    }

    public final b getFUNCTION_N_FQ_NAME() {
        return g;
    }

    public final List<a> getMutabilityMappings() {
        return m;
    }

    public final boolean isMutable(d0.e0.p.d.m0.g.c cVar) {
        HashMap<d0.e0.p.d.m0.g.c, b> hashMap = k;
        Objects.requireNonNull(hashMap, "null cannot be cast to non-null type kotlin.collections.Map<K, *>");
        return hashMap.containsKey(cVar);
    }

    public final boolean isReadOnly(d0.e0.p.d.m0.g.c cVar) {
        HashMap<d0.e0.p.d.m0.g.c, b> hashMap = l;
        Objects.requireNonNull(hashMap, "null cannot be cast to non-null type kotlin.collections.Map<K, *>");
        return hashMap.containsKey(cVar);
    }

    public final d0.e0.p.d.m0.g.a mapJavaToKotlin(b bVar) {
        m.checkNotNullParameter(bVar, "fqName");
        return i.get(bVar.toUnsafe());
    }

    public final d0.e0.p.d.m0.g.a mapKotlinToJava(d0.e0.p.d.m0.g.c cVar) {
        m.checkNotNullParameter(cVar, "kotlinFqName");
        if (!f(cVar, f3208b) && !f(cVar, d)) {
            if (!f(cVar, c) && !f(cVar, e)) {
                return j.get(cVar);
            }
            return h;
        }
        return f;
    }

    public final b mutableToReadOnly(d0.e0.p.d.m0.g.c cVar) {
        return k.get(cVar);
    }

    public final b readOnlyToMutable(d0.e0.p.d.m0.g.c cVar) {
        return l.get(cVar);
    }
}
