package d0.e0.p.d.m0.b.q;

import d0.e0.p.d.m0.b.h;
import d0.e0.p.d.m0.b.q.f;
import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.d0;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.g1.f;
import d0.e0.p.d.m0.c.s;
import d0.e0.p.d.m0.c.t0;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.c.w;
import d0.e0.p.d.m0.c.x;
import d0.e0.p.d.m0.c.z;
import d0.e0.p.d.m0.e.b.t;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.k.k;
import d0.e0.p.d.m0.m.j;
import d0.e0.p.d.m0.m.n;
import d0.e0.p.d.m0.n.c1;
import d0.e0.p.d.m0.n.f0;
import d0.e0.p.d.m0.n.j0;
import d0.t.n0;
import d0.t.u;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import d0.z.d.y;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.reflect.KProperty;
/* compiled from: JvmBuiltInsCustomizer.kt */
/* loaded from: classes3.dex */
public final class g implements d0.e0.p.d.m0.c.h1.a, d0.e0.p.d.m0.c.h1.c {
    public static final /* synthetic */ KProperty<Object>[] a = {a0.property1(new y(a0.getOrCreateKotlinClass(g.class), "settings", "getSettings()Lorg/jetbrains/kotlin/builtins/jvm/JvmBuiltIns$Settings;")), a0.property1(new y(a0.getOrCreateKotlinClass(g.class), "cloneableType", "getCloneableType()Lorg/jetbrains/kotlin/types/SimpleType;")), a0.property1(new y(a0.getOrCreateKotlinClass(g.class), "notConsideredDeprecation", "getNotConsideredDeprecation()Lorg/jetbrains/kotlin/descriptors/annotations/Annotations;"))};

    /* renamed from: b  reason: collision with root package name */
    public final c0 f3212b;
    public final d0.e0.p.d.m0.b.q.d c = d0.e0.p.d.m0.b.q.d.a;
    public final j d;
    public final d0.e0.p.d.m0.n.c0 e;
    public final j f;
    public final d0.e0.p.d.m0.m.a<d0.e0.p.d.m0.g.b, e> g;
    public final j h;

    /* compiled from: JvmBuiltInsCustomizer.kt */
    /* loaded from: classes3.dex */
    public enum a {
        HIDDEN,
        VISIBLE,
        NOT_CONSIDERED,
        DROP
    }

    /* compiled from: JvmBuiltInsCustomizer.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function0<j0> {
        public final /* synthetic */ d0.e0.p.d.m0.m.o $storageManager;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(d0.e0.p.d.m0.m.o oVar) {
            super(0);
            this.$storageManager = oVar;
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final j0 invoke() {
            return w.findNonGenericClassAcrossDependencies(g.this.b().getOwnerModuleDescriptor(), e.a.getCLONEABLE_CLASS_ID(), new d0(this.$storageManager, g.this.b().getOwnerModuleDescriptor())).getDefaultType();
        }
    }

    /* compiled from: JvmBuiltInsCustomizer.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function1<i, Collection<? extends t0>> {
        public final /* synthetic */ d0.e0.p.d.m0.g.e $name;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(d0.e0.p.d.m0.g.e eVar) {
            super(1);
            this.$name = eVar;
        }

        public final Collection<t0> invoke(i iVar) {
            m.checkNotNullParameter(iVar, "it");
            return iVar.getContributedFunctions(this.$name, d0.e0.p.d.m0.d.b.d.FROM_BUILTINS);
        }
    }

    /* compiled from: JvmBuiltInsCustomizer.kt */
    /* loaded from: classes3.dex */
    public static final class d extends o implements Function0<d0.e0.p.d.m0.c.g1.g> {
        public d() {
            super(0);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final d0.e0.p.d.m0.c.g1.g invoke() {
            return d0.e0.p.d.m0.c.g1.g.f.create(d0.t.m.listOf(f.createDeprecatedAnnotation$default(g.this.f3212b.getBuiltIns(), "This member is not fully supported by Kotlin compiler, so it may be absent or have different signature in next major version", null, null, 6, null)));
        }
    }

    public g(c0 c0Var, d0.e0.p.d.m0.m.o oVar, Function0<f.b> function0) {
        m.checkNotNullParameter(c0Var, "moduleDescriptor");
        m.checkNotNullParameter(oVar, "storageManager");
        m.checkNotNullParameter(function0, "settingsComputation");
        this.f3212b = c0Var;
        this.d = oVar.createLazyValue(function0);
        d0.e0.p.d.m0.c.i1.i iVar = new d0.e0.p.d.m0.c.i1.i(new h(c0Var, new d0.e0.p.d.m0.g.b("java.io")), d0.e0.p.d.m0.g.e.identifier("Serializable"), z.ABSTRACT, d0.e0.p.d.m0.c.f.INTERFACE, d0.t.m.listOf(new f0(oVar, new i(this))), u0.a, false, oVar);
        iVar.initialize(i.b.f3433b, n0.emptySet(), null);
        j0 defaultType = iVar.getDefaultType();
        m.checkNotNullExpressionValue(defaultType, "mockSerializableClass.defaultType");
        this.e = defaultType;
        this.f = oVar.createLazyValue(new b(oVar));
        this.g = oVar.createCacheWithNotNullValues();
        this.h = oVar.createLazyValue(new d());
    }

    public final d0.e0.p.d.m0.e.a.i0.l.f a(e eVar) {
        if (h.isAny(eVar) || !h.isUnderKotlinPackage(eVar)) {
            return null;
        }
        d0.e0.p.d.m0.g.c fqNameUnsafe = d0.e0.p.d.m0.k.x.a.getFqNameUnsafe(eVar);
        if (!fqNameUnsafe.isSafe()) {
            return null;
        }
        d0.e0.p.d.m0.g.a mapKotlinToJava = d0.e0.p.d.m0.b.q.c.a.mapKotlinToJava(fqNameUnsafe);
        d0.e0.p.d.m0.g.b asSingleFqName = mapKotlinToJava == null ? null : mapKotlinToJava.asSingleFqName();
        if (asSingleFqName == null) {
            return null;
        }
        e resolveClassByFqName = s.resolveClassByFqName(b().getOwnerModuleDescriptor(), asSingleFqName, d0.e0.p.d.m0.d.b.d.FROM_BUILTINS);
        if (resolveClassByFqName instanceof d0.e0.p.d.m0.e.a.i0.l.f) {
            return (d0.e0.p.d.m0.e.a.i0.l.f) resolveClassByFqName;
        }
        return null;
    }

    public final f.b b() {
        return (f.b) n.getValue(this.d, this, a[0]);
    }

    @Override // d0.e0.p.d.m0.c.h1.a
    public Collection<d0.e0.p.d.m0.c.d> getConstructors(e eVar) {
        e mapJavaToKotlin$default;
        boolean z2;
        boolean z3;
        boolean z4;
        m.checkNotNullParameter(eVar, "classDescriptor");
        if (eVar.getKind() != d0.e0.p.d.m0.c.f.CLASS || !b().isAdditionalBuiltInsFeatureSupported()) {
            return d0.t.n.emptyList();
        }
        d0.e0.p.d.m0.e.a.i0.l.f a2 = a(eVar);
        if (!(a2 == null || (mapJavaToKotlin$default = d0.e0.p.d.m0.b.q.d.mapJavaToKotlin$default(this.c, d0.e0.p.d.m0.k.x.a.getFqNameSafe(a2), d0.e0.p.d.m0.b.q.b.f.getInstance(), null, 4, null)) == null)) {
            c1 buildSubstitutor = q.createMappedTypeParametersSubstitution(mapJavaToKotlin$default, a2).buildSubstitutor();
            List<d0.e0.p.d.m0.c.d> constructors = a2.getConstructors();
            ArrayList arrayList = new ArrayList();
            Iterator<T> it = constructors.iterator();
            while (true) {
                boolean z5 = false;
                if (!it.hasNext()) {
                    break;
                }
                Object next = it.next();
                d0.e0.p.d.m0.c.d dVar = (d0.e0.p.d.m0.c.d) next;
                if (dVar.getVisibility().isPublicAPI()) {
                    Collection<d0.e0.p.d.m0.c.d> constructors2 = mapJavaToKotlin$default.getConstructors();
                    m.checkNotNullExpressionValue(constructors2, "defaultKotlinVersion.constructors");
                    if (!constructors2.isEmpty()) {
                        for (d0.e0.p.d.m0.c.d dVar2 : constructors2) {
                            m.checkNotNullExpressionValue(dVar2, "it");
                            if (k.getBothWaysOverridability(dVar2, dVar.substitute(buildSubstitutor)) == k.d.a.OVERRIDABLE) {
                                z4 = true;
                                continue;
                            } else {
                                z4 = false;
                                continue;
                            }
                            if (z4) {
                                z2 = false;
                                break;
                            }
                        }
                    }
                    z2 = true;
                    if (z2) {
                        if (dVar.getValueParameters().size() == 1) {
                            List<d0.e0.p.d.m0.c.c1> valueParameters = dVar.getValueParameters();
                            m.checkNotNullExpressionValue(valueParameters, "valueParameters");
                            d0.e0.p.d.m0.c.h declarationDescriptor = ((d0.e0.p.d.m0.c.c1) u.single((List<? extends Object>) valueParameters)).getType().getConstructor().getDeclarationDescriptor();
                            if (m.areEqual(declarationDescriptor == null ? null : d0.e0.p.d.m0.k.x.a.getFqNameUnsafe(declarationDescriptor), d0.e0.p.d.m0.k.x.a.getFqNameUnsafe(eVar))) {
                                z3 = true;
                                if (!z3 && !h.isDeprecated(dVar) && !p.a.getHIDDEN_CONSTRUCTOR_SIGNATURES().contains(t.signature(d0.e0.p.d.m0.e.b.w.a, a2, d0.e0.p.d.m0.e.b.u.computeJvmDescriptor$default(dVar, false, false, 3, null)))) {
                                    z5 = true;
                                }
                            }
                        }
                        z3 = false;
                        if (!z3) {
                            z5 = true;
                        }
                    }
                }
                if (z5) {
                    arrayList.add(next);
                }
            }
            ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(arrayList, 10));
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                d0.e0.p.d.m0.c.d dVar3 = (d0.e0.p.d.m0.c.d) it2.next();
                x.a<? extends x> newCopyBuilder = dVar3.newCopyBuilder();
                newCopyBuilder.setOwner(eVar);
                newCopyBuilder.setReturnType(eVar.getDefaultType());
                newCopyBuilder.setPreserveSourceElement();
                newCopyBuilder.setSubstitution(buildSubstitutor.getSubstitution());
                if (!p.a.getVISIBLE_CONSTRUCTOR_SIGNATURES().contains(t.signature(d0.e0.p.d.m0.e.b.w.a, a2, d0.e0.p.d.m0.e.b.u.computeJvmDescriptor$default(dVar3, false, false, 3, null)))) {
                    newCopyBuilder.setAdditionalAnnotations((d0.e0.p.d.m0.c.g1.g) n.getValue(this.h, this, a[2]));
                }
                x build = newCopyBuilder.build();
                Objects.requireNonNull(build, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.ClassConstructorDescriptor");
                arrayList2.add((d0.e0.p.d.m0.c.d) build);
            }
            return arrayList2;
        }
        return d0.t.n.emptyList();
    }

    /* JADX WARN: Code restructure failed: missing block: B:73:0x0266, code lost:
        if (r2 != 3) goto L79;
     */
    /* JADX WARN: Removed duplicated region for block: B:87:0x01f0 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:89:0x014e A[SYNTHETIC] */
    @Override // d0.e0.p.d.m0.c.h1.a
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public java.util.Collection<d0.e0.p.d.m0.c.t0> getFunctions(d0.e0.p.d.m0.g.e r14, d0.e0.p.d.m0.c.e r15) {
        /*
            Method dump skipped, instructions count: 661
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.b.q.g.getFunctions(d0.e0.p.d.m0.g.e, d0.e0.p.d.m0.c.e):java.util.Collection");
    }

    @Override // d0.e0.p.d.m0.c.h1.a
    public Collection<d0.e0.p.d.m0.n.c0> getSupertypes(e eVar) {
        m.checkNotNullParameter(eVar, "classDescriptor");
        d0.e0.p.d.m0.g.c fqNameUnsafe = d0.e0.p.d.m0.k.x.a.getFqNameUnsafe(eVar);
        p pVar = p.a;
        if (!pVar.isArrayOrPrimitiveArray(fqNameUnsafe)) {
            return pVar.isSerializableInJava(fqNameUnsafe) ? d0.t.m.listOf(this.e) : d0.t.n.emptyList();
        }
        j0 j0Var = (j0) n.getValue(this.f, this, a[1]);
        m.checkNotNullExpressionValue(j0Var, "cloneableType");
        return d0.t.n.listOf((Object[]) new d0.e0.p.d.m0.n.c0[]{j0Var, this.e});
    }

    @Override // d0.e0.p.d.m0.c.h1.c
    public boolean isFunctionAvailable(e eVar, t0 t0Var) {
        m.checkNotNullParameter(eVar, "classDescriptor");
        m.checkNotNullParameter(t0Var, "functionDescriptor");
        d0.e0.p.d.m0.e.a.i0.l.f a2 = a(eVar);
        if (a2 == null || !t0Var.getAnnotations().hasAnnotation(d0.e0.p.d.m0.c.h1.d.getPLATFORM_DEPENDENT_ANNOTATION_FQ_NAME())) {
            return true;
        }
        if (!b().isAdditionalBuiltInsFeatureSupported()) {
            return false;
        }
        String computeJvmDescriptor$default = d0.e0.p.d.m0.e.b.u.computeJvmDescriptor$default(t0Var, false, false, 3, null);
        d0.e0.p.d.m0.e.a.i0.l.h unsubstitutedMemberScope = a2.getUnsubstitutedMemberScope();
        d0.e0.p.d.m0.g.e name = t0Var.getName();
        m.checkNotNullExpressionValue(name, "functionDescriptor.name");
        Collection<t0> contributedFunctions = unsubstitutedMemberScope.getContributedFunctions(name, d0.e0.p.d.m0.d.b.d.FROM_BUILTINS);
        if (!(contributedFunctions instanceof Collection) || !contributedFunctions.isEmpty()) {
            for (t0 t0Var2 : contributedFunctions) {
                if (m.areEqual(d0.e0.p.d.m0.e.b.u.computeJvmDescriptor$default(t0Var2, false, false, 3, null), computeJvmDescriptor$default)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override // d0.e0.p.d.m0.c.h1.a
    public Set<d0.e0.p.d.m0.g.e> getFunctionsNames(e eVar) {
        d0.e0.p.d.m0.e.a.i0.l.h unsubstitutedMemberScope;
        m.checkNotNullParameter(eVar, "classDescriptor");
        if (!b().isAdditionalBuiltInsFeatureSupported()) {
            return n0.emptySet();
        }
        d0.e0.p.d.m0.e.a.i0.l.f a2 = a(eVar);
        Set<d0.e0.p.d.m0.g.e> set = null;
        if (!(a2 == null || (unsubstitutedMemberScope = a2.getUnsubstitutedMemberScope()) == null)) {
            set = unsubstitutedMemberScope.getFunctionNames();
        }
        return set == null ? n0.emptySet() : set;
    }
}
