package d0.e0.p.d.m0.b.q;

import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.d0;
import d0.e0.p.d.m0.c.h1.b;
import d0.e0.p.d.m0.c.h1.c;
import d0.e0.p.d.m0.d.b.c;
import d0.e0.p.d.m0.e.b.n;
import d0.e0.p.d.m0.l.b.a;
import d0.e0.p.d.m0.l.b.d;
import d0.e0.p.d.m0.l.b.i;
import d0.e0.p.d.m0.l.b.j;
import d0.e0.p.d.m0.l.b.k;
import d0.e0.p.d.m0.l.b.p;
import d0.e0.p.d.m0.l.b.q;
import d0.e0.p.d.m0.l.b.t;
import d0.e0.p.d.m0.n.l1.l;
import d0.z.d.m;
/* compiled from: JvmBuiltInsPackageFragmentProvider.kt */
/* loaded from: classes3.dex */
public final class o extends a {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public o(d0.e0.p.d.m0.m.o oVar, n nVar, c0 c0Var, d0 d0Var, d0.e0.p.d.m0.c.h1.a aVar, c cVar, k kVar, l lVar, d0.e0.p.d.m0.k.z.a aVar2) {
        super(oVar, nVar, c0Var);
        m.checkNotNullParameter(oVar, "storageManager");
        m.checkNotNullParameter(nVar, "finder");
        m.checkNotNullParameter(c0Var, "moduleDescriptor");
        m.checkNotNullParameter(d0Var, "notFoundClasses");
        m.checkNotNullParameter(aVar, "additionalClassPartsProvider");
        m.checkNotNullParameter(cVar, "platformDependentDeclarationFilter");
        m.checkNotNullParameter(kVar, "deserializationConfiguration");
        m.checkNotNullParameter(lVar, "kotlinTypeChecker");
        m.checkNotNullParameter(aVar2, "samConversionResolver");
        d0.e0.p.d.m0.l.b.m mVar = new d0.e0.p.d.m0.l.b.m(this);
        d0.e0.p.d.m0.l.b.d0.a aVar3 = d0.e0.p.d.m0.l.b.d0.a.m;
        d dVar = new d(c0Var, d0Var, aVar3);
        t.a aVar4 = t.a.a;
        p pVar = p.a;
        m.checkNotNullExpressionValue(pVar, "DO_NOTHING");
        j jVar = new j(oVar, c0Var, kVar, mVar, dVar, this, aVar4, pVar, c.a.a, q.a.a, d0.t.n.listOf((Object[]) new b[]{new d0.e0.p.d.m0.b.p.a(oVar, c0Var), new e(oVar, c0Var, null, 4, null)}), d0Var, i.a.getDEFAULT(), aVar, cVar, aVar3.getExtensionRegistry(), lVar, aVar2, null, 262144, null);
        m.checkNotNullParameter(jVar, "<set-?>");
        this.d = jVar;
    }
}
