package d0.e0.p.d.m0.b.q;

import d0.e0.p.d.m0.b.q.g;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.e.b.t;
import d0.e0.p.d.m0.e.b.w;
import d0.e0.p.d.m0.p.b;
import d0.z.d.m;
import kotlin.jvm.internal.Ref$ObjectRef;
/* compiled from: JvmBuiltInsCustomizer.kt */
/* loaded from: classes3.dex */
public final class l extends b.AbstractC0368b<e, g.a> {
    public final /* synthetic */ String a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ Ref$ObjectRef<g.a> f3213b;

    public l(String str, Ref$ObjectRef<g.a> ref$ObjectRef) {
        this.a = str;
        this.f3213b = ref$ObjectRef;
    }

    /* JADX WARN: Type inference failed for: r0v4, types: [T, d0.e0.p.d.m0.b.q.g$a] */
    /* JADX WARN: Type inference failed for: r0v5, types: [T, d0.e0.p.d.m0.b.q.g$a] */
    /* JADX WARN: Type inference failed for: r0v6, types: [T, d0.e0.p.d.m0.b.q.g$a] */
    public boolean beforeChildren(e eVar) {
        m.checkNotNullParameter(eVar, "javaClassDescriptor");
        String signature = t.signature(w.a, eVar, this.a);
        p pVar = p.a;
        if (pVar.getHIDDEN_METHOD_SIGNATURES().contains(signature)) {
            this.f3213b.element = g.a.HIDDEN;
        } else if (pVar.getVISIBLE_METHOD_SIGNATURES().contains(signature)) {
            this.f3213b.element = g.a.VISIBLE;
        } else if (pVar.getDROP_LIST_METHOD_SIGNATURES().contains(signature)) {
            this.f3213b.element = g.a.DROP;
        }
        return this.f3213b.element == null;
    }

    @Override // d0.e0.p.d.m0.p.b.d
    public g.a result() {
        g.a aVar = this.f3213b.element;
        return aVar == null ? g.a.NOT_CONSIDERED : aVar;
    }
}
