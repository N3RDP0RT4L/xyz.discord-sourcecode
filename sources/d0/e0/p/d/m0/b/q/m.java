package d0.e0.p.d.m0.b.q;

import d0.e0.p.d.m0.p.b;
/* compiled from: JvmBuiltInsCustomizer.kt */
/* loaded from: classes3.dex */
public final class m implements b.c<d0.e0.p.d.m0.c.b> {
    public static final m a = new m();

    public final Iterable<d0.e0.p.d.m0.c.b> getNeighbors(d0.e0.p.d.m0.c.b bVar) {
        return bVar.getOriginal().getOverriddenDescriptors();
    }
}
