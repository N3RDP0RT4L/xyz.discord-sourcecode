package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.z0;
import d0.z.d.m;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: TypeSubstitution.kt */
/* loaded from: classes3.dex */
public final class z extends z0 {

    /* renamed from: b  reason: collision with root package name */
    public final z0[] f3516b;
    public final w0[] c;
    public final boolean d;

    public /* synthetic */ z(z0[] z0VarArr, w0[] w0VarArr, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(z0VarArr, w0VarArr, (i & 4) != 0 ? false : z2);
    }

    @Override // d0.e0.p.d.m0.n.z0
    public boolean approximateContravariantCapturedTypes() {
        return this.d;
    }

    @Override // d0.e0.p.d.m0.n.z0
    public w0 get(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "key");
        h declarationDescriptor = c0Var.getConstructor().getDeclarationDescriptor();
        z0 z0Var = declarationDescriptor instanceof z0 ? (z0) declarationDescriptor : null;
        if (z0Var == null) {
            return null;
        }
        int index = z0Var.getIndex();
        z0[] z0VarArr = this.f3516b;
        if (index >= z0VarArr.length || !m.areEqual(z0VarArr[index].getTypeConstructor(), z0Var.getTypeConstructor())) {
            return null;
        }
        return this.c[index];
    }

    public final w0[] getArguments() {
        return this.c;
    }

    public final z0[] getParameters() {
        return this.f3516b;
    }

    @Override // d0.e0.p.d.m0.n.z0
    public boolean isEmpty() {
        return this.c.length == 0;
    }

    public z(z0[] z0VarArr, w0[] w0VarArr, boolean z2) {
        m.checkNotNullParameter(z0VarArr, "parameters");
        m.checkNotNullParameter(w0VarArr, "arguments");
        this.f3516b = z0VarArr;
        this.c = w0VarArr;
        this.d = z2;
        int length = z0VarArr.length;
        int length2 = w0VarArr.length;
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public z(java.util.List<? extends d0.e0.p.d.m0.c.z0> r9, java.util.List<? extends d0.e0.p.d.m0.n.w0> r10) {
        /*
            r8 = this;
            java.lang.String r0 = "parameters"
            d0.z.d.m.checkNotNullParameter(r9, r0)
            java.lang.String r0 = "argumentsList"
            d0.z.d.m.checkNotNullParameter(r10, r0)
            r0 = 0
            d0.e0.p.d.m0.c.z0[] r1 = new d0.e0.p.d.m0.c.z0[r0]
            java.lang.Object[] r9 = r9.toArray(r1)
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.Array<T>"
            java.util.Objects.requireNonNull(r9, r1)
            r3 = r9
            d0.e0.p.d.m0.c.z0[] r3 = (d0.e0.p.d.m0.c.z0[]) r3
            d0.e0.p.d.m0.n.w0[] r9 = new d0.e0.p.d.m0.n.w0[r0]
            java.lang.Object[] r9 = r10.toArray(r9)
            java.util.Objects.requireNonNull(r9, r1)
            r4 = r9
            d0.e0.p.d.m0.n.w0[] r4 = (d0.e0.p.d.m0.n.w0[]) r4
            r5 = 0
            r6 = 4
            r7 = 0
            r2 = r8
            r2.<init>(r3, r4, r5, r6, r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.n.z.<init>(java.util.List, java.util.List):void");
    }
}
