package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.c.g1.g;
import d0.z.d.m;
/* compiled from: KotlinTypeFactory.kt */
/* loaded from: classes3.dex */
public final class h extends n {
    public final g l;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public h(j0 j0Var, g gVar) {
        super(j0Var);
        m.checkNotNullParameter(j0Var, "delegate");
        m.checkNotNullParameter(gVar, "annotations");
        this.l = gVar;
    }

    @Override // d0.e0.p.d.m0.n.m, d0.e0.p.d.m0.c.g1.a
    public g getAnnotations() {
        return this.l;
    }

    @Override // d0.e0.p.d.m0.n.m
    public h replaceDelegate(j0 j0Var) {
        m.checkNotNullParameter(j0Var, "delegate");
        return new h(j0Var, getAnnotations());
    }
}
