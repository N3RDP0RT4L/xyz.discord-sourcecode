package d0.e0.p.d.m0.n;

import d0.z.d.m;
import kotlin.NoWhenBranchMatchedException;
/* compiled from: flexibleTypes.kt */
/* loaded from: classes3.dex */
public final class y {
    public static final v asFlexibleType(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        return (v) c0Var.unwrap();
    }

    public static final boolean isFlexible(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        return c0Var.unwrap() instanceof v;
    }

    public static final j0 lowerIfFlexible(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        i1 unwrap = c0Var.unwrap();
        if (unwrap instanceof v) {
            return ((v) unwrap).getLowerBound();
        }
        if (unwrap instanceof j0) {
            return (j0) unwrap;
        }
        throw new NoWhenBranchMatchedException();
    }

    public static final j0 upperIfFlexible(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        i1 unwrap = c0Var.unwrap();
        if (unwrap instanceof v) {
            return ((v) unwrap).getUpperBound();
        }
        if (unwrap instanceof j0) {
            return (j0) unwrap;
        }
        throw new NoWhenBranchMatchedException();
    }
}
