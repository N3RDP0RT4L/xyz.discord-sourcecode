package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.c.g1.a;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.n.l1.g;
import d0.e0.p.d.m0.n.l1.q;
import d0.e0.p.d.m0.n.n1.h;
import java.util.List;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: KotlinType.kt */
/* loaded from: classes3.dex */
public abstract class c0 implements a, h {
    public int j;

    public c0() {
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof c0)) {
            return false;
        }
        c0 c0Var = (c0) obj;
        return isMarkedNullable() == c0Var.isMarkedNullable() && q.a.strictEqualTypes(unwrap(), c0Var.unwrap());
    }

    public abstract List<w0> getArguments();

    public abstract u0 getConstructor();

    public abstract i getMemberScope();

    public final int hashCode() {
        int i;
        int i2 = this.j;
        if (i2 != 0) {
            return i2;
        }
        if (e0.isError(this)) {
            i = super.hashCode();
        } else {
            int hashCode = getArguments().hashCode();
            i = (isMarkedNullable() ? 1 : 0) + ((hashCode + (getConstructor().hashCode() * 31)) * 31);
        }
        this.j = i;
        return i;
    }

    public abstract boolean isMarkedNullable();

    public abstract c0 refine(g gVar);

    public abstract i1 unwrap();

    public c0(DefaultConstructorMarker defaultConstructorMarker) {
    }
}
