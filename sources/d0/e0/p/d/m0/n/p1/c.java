package d0.e0.p.d.m0.n.p1;

import d0.e0.p.d.m0.k.u.a.b;
import d0.e0.p.d.m0.n.j1;
import d0.e0.p.d.m0.n.u0;
import d0.e0.p.d.m0.n.v0;
import d0.e0.p.d.m0.n.w0;
import d0.e0.p.d.m0.n.y0;
import d0.z.d.m;
/* compiled from: CapturedTypeApproximation.kt */
/* loaded from: classes3.dex */
public final class c extends v0 {
    @Override // d0.e0.p.d.m0.n.v0
    public w0 get(u0 u0Var) {
        m.checkNotNullParameter(u0Var, "key");
        b bVar = u0Var instanceof b ? (b) u0Var : null;
        if (bVar == null) {
            return null;
        }
        if (bVar.getProjection().isStarProjection()) {
            return new y0(j1.OUT_VARIANCE, bVar.getProjection().getType());
        }
        return bVar.getProjection();
    }
}
