package d0.e0.p.d.m0.n.p1;

import d0.z.d.m;
/* compiled from: CapturedTypeApproximation.kt */
/* loaded from: classes3.dex */
public final class a<T> {
    public final T a;

    /* renamed from: b  reason: collision with root package name */
    public final T f3506b;

    public a(T t, T t2) {
        this.a = t;
        this.f3506b = t2;
    }

    public final T component1() {
        return this.a;
    }

    public final T component2() {
        return this.f3506b;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof a)) {
            return false;
        }
        a aVar = (a) obj;
        return m.areEqual(this.a, aVar.a) && m.areEqual(this.f3506b, aVar.f3506b);
    }

    public final T getLower() {
        return this.a;
    }

    public final T getUpper() {
        return this.f3506b;
    }

    public int hashCode() {
        T t = this.a;
        int i = 0;
        int hashCode = (t == null ? 0 : t.hashCode()) * 31;
        T t2 = this.f3506b;
        if (t2 != null) {
            i = t2.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("ApproximationBounds(lower=");
        R.append(this.a);
        R.append(", upper=");
        R.append(this.f3506b);
        R.append(')');
        return R.toString();
    }
}
