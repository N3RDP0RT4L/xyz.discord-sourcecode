package d0.e0.p.d.m0.n.p1;

import d0.e0.p.d.m0.b.h;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.k.u.a.d;
import d0.e0.p.d.m0.n.a1;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.c1;
import d0.e0.p.d.m0.n.d0;
import d0.e0.p.d.m0.n.e1;
import d0.e0.p.d.m0.n.g1;
import d0.e0.p.d.m0.n.i1;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.j1;
import d0.e0.p.d.m0.n.u0;
import d0.e0.p.d.m0.n.w0;
import d0.e0.p.d.m0.n.y;
import d0.e0.p.d.m0.n.y0;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;
import kotlin.jvm.functions.Function1;
/* compiled from: CapturedTypeApproximation.kt */
/* loaded from: classes3.dex */
public final class b {

    /* compiled from: CapturedTypeApproximation.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<i1, Boolean> {
        public static final a j = new a();

        public a() {
            super(1);
        }

        public final Boolean invoke(i1 i1Var) {
            m.checkNotNullExpressionValue(i1Var, "it");
            return Boolean.valueOf(d.isCaptured(i1Var));
        }
    }

    public static final c0 a(c0 c0Var, List<d> list) {
        y0 y0Var;
        c0Var.getArguments().size();
        list.size();
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(list, 10));
        for (d dVar : list) {
            j1 j1Var = j1.OUT_VARIANCE;
            j1 j1Var2 = j1.INVARIANT;
            dVar.isConsistent();
            if (!m.areEqual(dVar.getInProjection(), dVar.getOutProjection())) {
                j1 variance = dVar.getTypeParameter().getVariance();
                j1 j1Var3 = j1.IN_VARIANCE;
                if (variance != j1Var3) {
                    if (h.isNothing(dVar.getInProjection()) && dVar.getTypeParameter().getVariance() != j1Var3) {
                        if (j1Var == dVar.getTypeParameter().getVariance()) {
                            j1Var = j1Var2;
                        }
                        y0Var = new y0(j1Var, dVar.getOutProjection());
                    } else if (h.isNullableAny(dVar.getOutProjection())) {
                        if (j1Var3 != dVar.getTypeParameter().getVariance()) {
                            j1Var2 = j1Var3;
                        }
                        y0Var = new y0(j1Var2, dVar.getInProjection());
                    } else {
                        if (j1Var == dVar.getTypeParameter().getVariance()) {
                            j1Var = j1Var2;
                        }
                        y0Var = new y0(j1Var, dVar.getOutProjection());
                    }
                    arrayList.add(y0Var);
                }
            }
            y0Var = new y0(dVar.getInProjection());
            arrayList.add(y0Var);
        }
        return a1.replace$default(c0Var, arrayList, null, null, 6, null);
    }

    public static final d0.e0.p.d.m0.n.p1.a<c0> approximateCapturedTypes(c0 c0Var) {
        Object obj;
        d dVar;
        m.checkNotNullParameter(c0Var, "type");
        if (y.isFlexible(c0Var)) {
            d0.e0.p.d.m0.n.p1.a<c0> approximateCapturedTypes = approximateCapturedTypes(y.lowerIfFlexible(c0Var));
            d0.e0.p.d.m0.n.p1.a<c0> approximateCapturedTypes2 = approximateCapturedTypes(y.upperIfFlexible(c0Var));
            d0 d0Var = d0.a;
            return new d0.e0.p.d.m0.n.p1.a<>(g1.inheritEnhancement(d0.flexibleType(y.lowerIfFlexible(approximateCapturedTypes.getLower()), y.upperIfFlexible(approximateCapturedTypes2.getLower())), c0Var), g1.inheritEnhancement(d0.flexibleType(y.lowerIfFlexible(approximateCapturedTypes.getUpper()), y.upperIfFlexible(approximateCapturedTypes2.getUpper())), c0Var));
        }
        u0 constructor = c0Var.getConstructor();
        boolean z2 = true;
        if (d.isCaptured(c0Var)) {
            w0 projection = ((d0.e0.p.d.m0.k.u.a.b) constructor).getProjection();
            c0 type = projection.getType();
            m.checkNotNullExpressionValue(type, "typeProjection.type");
            c0 makeNullableIfNeeded = e1.makeNullableIfNeeded(type, c0Var.isMarkedNullable());
            m.checkNotNullExpressionValue(makeNullableIfNeeded, "makeNullableIfNeeded(this, type.isMarkedNullable)");
            int ordinal = projection.getProjectionKind().ordinal();
            if (ordinal == 1) {
                j0 nullableAnyType = d0.e0.p.d.m0.n.o1.a.getBuiltIns(c0Var).getNullableAnyType();
                m.checkNotNullExpressionValue(nullableAnyType, "type.builtIns.nullableAnyType");
                return new d0.e0.p.d.m0.n.p1.a<>(makeNullableIfNeeded, nullableAnyType);
            } else if (ordinal == 2) {
                j0 nothingType = d0.e0.p.d.m0.n.o1.a.getBuiltIns(c0Var).getNothingType();
                m.checkNotNullExpressionValue(nothingType, "type.builtIns.nothingType");
                c0 makeNullableIfNeeded2 = e1.makeNullableIfNeeded((c0) nothingType, c0Var.isMarkedNullable());
                m.checkNotNullExpressionValue(makeNullableIfNeeded2, "makeNullableIfNeeded(this, type.isMarkedNullable)");
                return new d0.e0.p.d.m0.n.p1.a<>(makeNullableIfNeeded2, makeNullableIfNeeded);
            } else {
                throw new AssertionError(m.stringPlus("Only nontrivial projections should have been captured, not: ", projection));
            }
        } else if (c0Var.getArguments().isEmpty() || c0Var.getArguments().size() != constructor.getParameters().size()) {
            return new d0.e0.p.d.m0.n.p1.a<>(c0Var, c0Var);
        } else {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            List<w0> arguments = c0Var.getArguments();
            List<z0> parameters = constructor.getParameters();
            m.checkNotNullExpressionValue(parameters, "typeConstructor.parameters");
            for (Pair pair : u.zip(arguments, parameters)) {
                w0 w0Var = (w0) pair.component1();
                z0 z0Var = (z0) pair.component2();
                m.checkNotNullExpressionValue(z0Var, "typeParameter");
                int ordinal2 = c1.combine(z0Var.getVariance(), w0Var).ordinal();
                if (ordinal2 == 0) {
                    c0 type2 = w0Var.getType();
                    m.checkNotNullExpressionValue(type2, "type");
                    c0 type3 = w0Var.getType();
                    m.checkNotNullExpressionValue(type3, "type");
                    dVar = new d(z0Var, type2, type3);
                } else if (ordinal2 == 1) {
                    c0 type4 = w0Var.getType();
                    m.checkNotNullExpressionValue(type4, "type");
                    j0 nullableAnyType2 = d0.e0.p.d.m0.k.x.a.getBuiltIns(z0Var).getNullableAnyType();
                    m.checkNotNullExpressionValue(nullableAnyType2, "typeParameter.builtIns.nullableAnyType");
                    dVar = new d(z0Var, type4, nullableAnyType2);
                } else if (ordinal2 == 2) {
                    j0 nothingType2 = d0.e0.p.d.m0.k.x.a.getBuiltIns(z0Var).getNothingType();
                    m.checkNotNullExpressionValue(nothingType2, "typeParameter.builtIns.nothingType");
                    c0 type5 = w0Var.getType();
                    m.checkNotNullExpressionValue(type5, "type");
                    dVar = new d(z0Var, nothingType2, type5);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
                if (w0Var.isStarProjection()) {
                    arrayList.add(dVar);
                    arrayList2.add(dVar);
                } else {
                    d0.e0.p.d.m0.n.p1.a<c0> approximateCapturedTypes3 = approximateCapturedTypes(dVar.getInProjection());
                    d0.e0.p.d.m0.n.p1.a<c0> approximateCapturedTypes4 = approximateCapturedTypes(dVar.getOutProjection());
                    d0.e0.p.d.m0.n.p1.a aVar = new d0.e0.p.d.m0.n.p1.a(new d(dVar.getTypeParameter(), approximateCapturedTypes3.component2(), approximateCapturedTypes4.component1()), new d(dVar.getTypeParameter(), approximateCapturedTypes3.component1(), approximateCapturedTypes4.component2()));
                    arrayList.add((d) aVar.component1());
                    arrayList2.add((d) aVar.component2());
                }
            }
            if (!arrayList.isEmpty()) {
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    if (!((d) it.next()).isConsistent()) {
                        break;
                    }
                }
            }
            z2 = false;
            if (z2) {
                obj = d0.e0.p.d.m0.n.o1.a.getBuiltIns(c0Var).getNothingType();
                m.checkNotNullExpressionValue(obj, "type.builtIns.nothingType");
            } else {
                obj = a(c0Var, arrayList);
            }
            return new d0.e0.p.d.m0.n.p1.a<>(obj, a(c0Var, arrayList2));
        }
    }

    public static final w0 approximateCapturedTypesIfNecessary(w0 w0Var, boolean z2) {
        if (w0Var == null) {
            return null;
        }
        if (w0Var.isStarProjection()) {
            return w0Var;
        }
        c0 type = w0Var.getType();
        m.checkNotNullExpressionValue(type, "typeProjection.type");
        if (!e1.contains(type, a.j)) {
            return w0Var;
        }
        j1 projectionKind = w0Var.getProjectionKind();
        m.checkNotNullExpressionValue(projectionKind, "typeProjection.projectionKind");
        if (projectionKind == j1.OUT_VARIANCE) {
            return new y0(projectionKind, approximateCapturedTypes(type).getUpper());
        }
        if (z2) {
            return new y0(projectionKind, approximateCapturedTypes(type).getLower());
        }
        c1 create = c1.create(new c());
        m.checkNotNullExpressionValue(create, "create(object : TypeConstructorSubstitution() {\n        override fun get(key: TypeConstructor): TypeProjection? {\n            val capturedTypeConstructor = key as? CapturedTypeConstructor ?: return null\n            if (capturedTypeConstructor.projection.isStarProjection) {\n                return TypeProjectionImpl(Variance.OUT_VARIANCE, capturedTypeConstructor.projection.type)\n            }\n            return capturedTypeConstructor.projection\n        }\n    })");
        return create.substituteWithoutApproximation(w0Var);
    }
}
