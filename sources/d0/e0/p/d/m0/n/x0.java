package d0.e0.p.d.m0.n;
/* compiled from: TypeProjectionBase.java */
/* loaded from: classes3.dex */
public abstract class x0 implements w0 {
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof w0)) {
            return false;
        }
        w0 w0Var = (w0) obj;
        return isStarProjection() == w0Var.isStarProjection() && getProjectionKind() == w0Var.getProjectionKind() && getType().equals(w0Var.getType());
    }

    public int hashCode() {
        int hashCode = getProjectionKind().hashCode();
        if (e1.noExpectedType(getType())) {
            return (hashCode * 31) + 19;
        }
        return (hashCode * 31) + (isStarProjection() ? 17 : getType().hashCode());
    }

    public String toString() {
        if (isStarProjection()) {
            return "*";
        }
        if (getProjectionKind() == j1.INVARIANT) {
            return getType().toString();
        }
        return getProjectionKind() + " " + getType();
    }
}
