package d0.e0.p.d.m0.n;

import b.d.b.a.a;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.n.l1.g;
import d0.e0.p.d.m0.n.t;
import d0.z.d.m;
import java.util.List;
import kotlin.jvm.functions.Function1;
/* compiled from: KotlinTypeFactory.kt */
/* loaded from: classes3.dex */
public final class k0 extends j0 {
    public final u0 k;
    public final List<w0> l;
    public final boolean m;
    public final i n;
    public final Function1<g, j0> o;

    /* JADX WARN: Multi-variable type inference failed */
    public k0(u0 u0Var, List<? extends w0> list, boolean z2, i iVar, Function1<? super g, ? extends j0> function1) {
        m.checkNotNullParameter(u0Var, "constructor");
        m.checkNotNullParameter(list, "arguments");
        m.checkNotNullParameter(iVar, "memberScope");
        m.checkNotNullParameter(function1, "refinedTypeFactory");
        this.k = u0Var;
        this.l = list;
        this.m = z2;
        this.n = iVar;
        this.o = function1;
        if (getMemberScope() instanceof t.d) {
            StringBuilder R = a.R("SimpleTypeImpl should not be created for error type: ");
            R.append(getMemberScope());
            R.append('\n');
            R.append(getConstructor());
            throw new IllegalStateException(R.toString());
        }
    }

    @Override // d0.e0.p.d.m0.c.g1.a
    public d0.e0.p.d.m0.c.g1.g getAnnotations() {
        return d0.e0.p.d.m0.c.g1.g.f.getEMPTY();
    }

    @Override // d0.e0.p.d.m0.n.c0
    public List<w0> getArguments() {
        return this.l;
    }

    @Override // d0.e0.p.d.m0.n.c0
    public u0 getConstructor() {
        return this.k;
    }

    @Override // d0.e0.p.d.m0.n.c0
    public i getMemberScope() {
        return this.n;
    }

    @Override // d0.e0.p.d.m0.n.c0
    public boolean isMarkedNullable() {
        return this.m;
    }

    @Override // d0.e0.p.d.m0.n.j0, d0.e0.p.d.m0.n.i1
    public j0 makeNullableAsSpecified(boolean z2) {
        if (z2 == isMarkedNullable()) {
            return this;
        }
        if (z2) {
            return new h0(this);
        }
        return new g0(this);
    }

    @Override // d0.e0.p.d.m0.n.j0, d0.e0.p.d.m0.n.i1
    public j0 replaceAnnotations(d0.e0.p.d.m0.c.g1.g gVar) {
        m.checkNotNullParameter(gVar, "newAnnotations");
        return gVar.isEmpty() ? this : new h(this, gVar);
    }

    @Override // d0.e0.p.d.m0.n.i1, d0.e0.p.d.m0.n.c0
    public j0 refine(g gVar) {
        m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
        j0 invoke = this.o.invoke(gVar);
        return invoke == null ? this : invoke;
    }
}
