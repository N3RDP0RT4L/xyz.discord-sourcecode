package d0.e0.p.d.m0.n.m1;

import andhook.lib.HookHelper;
import d0.e0.p.d.m0.c.a;
import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.i1.g0;
import d0.e0.p.d.m0.c.i1.q;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.c.q0;
import d0.e0.p.d.m0.c.t0;
import d0.e0.p.d.m0.c.u;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.c.x;
import d0.e0.p.d.m0.c.z;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.t;
import d0.e0.p.d.m0.n.z0;
import java.util.Collection;
import java.util.List;
/* compiled from: ErrorSimpleFunctionDescriptorImpl.java */
/* loaded from: classes3.dex */
public class a extends g0 {

    /* compiled from: ErrorSimpleFunctionDescriptorImpl.java */
    /* renamed from: d0.e0.p.d.m0.n.m1.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public class C0363a implements x.a<t0> {
        public C0363a() {
        }

        /* JADX WARN: Removed duplicated region for block: B:104:0x0151  */
        /* JADX WARN: Removed duplicated region for block: B:30:0x004c  */
        /* JADX WARN: Removed duplicated region for block: B:34:0x0058  */
        /* JADX WARN: Removed duplicated region for block: B:35:0x005d  */
        /* JADX WARN: Removed duplicated region for block: B:36:0x0062  */
        /* JADX WARN: Removed duplicated region for block: B:37:0x0067  */
        /* JADX WARN: Removed duplicated region for block: B:38:0x006c  */
        /* JADX WARN: Removed duplicated region for block: B:39:0x0071  */
        /* JADX WARN: Removed duplicated region for block: B:40:0x0076  */
        /* JADX WARN: Removed duplicated region for block: B:41:0x007b  */
        /* JADX WARN: Removed duplicated region for block: B:42:0x0080  */
        /* JADX WARN: Removed duplicated region for block: B:43:0x0085  */
        /* JADX WARN: Removed duplicated region for block: B:44:0x008a  */
        /* JADX WARN: Removed duplicated region for block: B:47:0x009e  */
        /* JADX WARN: Removed duplicated region for block: B:78:0x0109  */
        /* JADX WARN: Removed duplicated region for block: B:80:0x010e  */
        /* JADX WARN: Removed duplicated region for block: B:81:0x0111  */
        /* JADX WARN: Removed duplicated region for block: B:82:0x0116  */
        /* JADX WARN: Removed duplicated region for block: B:83:0x011b  */
        /* JADX WARN: Removed duplicated region for block: B:84:0x0120  */
        /* JADX WARN: Removed duplicated region for block: B:85:0x0123  */
        /* JADX WARN: Removed duplicated region for block: B:86:0x0126  */
        /* JADX WARN: Removed duplicated region for block: B:87:0x0129  */
        /* JADX WARN: Removed duplicated region for block: B:88:0x012c  */
        /* JADX WARN: Removed duplicated region for block: B:89:0x012f  */
        /* JADX WARN: Removed duplicated region for block: B:90:0x0132  */
        /* JADX WARN: Removed duplicated region for block: B:93:0x013a A[ADDED_TO_REGION] */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public static /* synthetic */ void a(int r24) {
            /*
                Method dump skipped, instructions count: 566
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.n.m1.a.C0363a.a(int):void");
        }

        @Override // d0.e0.p.d.m0.c.x.a
        public x.a<t0> setAdditionalAnnotations(g gVar) {
            if (gVar != null) {
                return this;
            }
            a(29);
            throw null;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        public x.a<t0> setCopyOverrides(boolean z2) {
            return this;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        public x.a<t0> setDispatchReceiverParameter(q0 q0Var) {
            return this;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        public x.a<t0> setDropOriginalInContainingParts() {
            return this;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        public x.a<t0> setExtensionReceiverParameter(q0 q0Var) {
            return this;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        public x.a<t0> setHiddenForResolutionEverywhereBesideSupercalls() {
            return this;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        public x.a<t0> setHiddenToOvercomeSignatureClash() {
            return this;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        public x.a<t0> setKind(b.a aVar) {
            if (aVar != null) {
                return this;
            }
            a(6);
            throw null;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        public x.a<t0> setModality(z zVar) {
            if (zVar != null) {
                return this;
            }
            a(2);
            throw null;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        public x.a<t0> setName(e eVar) {
            if (eVar != null) {
                return this;
            }
            a(9);
            throw null;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        public x.a<t0> setOriginal(b bVar) {
            return this;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        public x.a<t0> setOwner(m mVar) {
            if (mVar != null) {
                return this;
            }
            a(0);
            throw null;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        public x.a<t0> setPreserveSourceElement() {
            return this;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        public x.a<t0> setReturnType(c0 c0Var) {
            if (c0Var != null) {
                return this;
            }
            a(19);
            throw null;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        public x.a<t0> setSignatureChange() {
            return this;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        public x.a<t0> setSubstitution(z0 z0Var) {
            if (z0Var != null) {
                return this;
            }
            a(13);
            throw null;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        public x.a<t0> setTypeParameters(List<d0.e0.p.d.m0.c.z0> list) {
            if (list != null) {
                return this;
            }
            a(17);
            throw null;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        public x.a<t0> setValueParameters(List<c1> list) {
            if (list != null) {
                return this;
            }
            a(11);
            throw null;
        }

        @Override // d0.e0.p.d.m0.c.x.a
        public x.a<t0> setVisibility(u uVar) {
            if (uVar != null) {
                return this;
            }
            a(4);
            throw null;
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // d0.e0.p.d.m0.c.x.a
        public t0 build() {
            return a.this;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public a(d0.e0.p.d.m0.c.e eVar, t.d dVar) {
        super(eVar, null, g.f.getEMPTY(), e.special("<ERROR FUNCTION>"), b.a.DECLARATION, u0.a);
        if (eVar == null) {
            a(0);
            throw null;
        } else if (dVar != null) {
        } else {
            a(1);
            throw null;
        }
    }

    public static /* synthetic */ void a(int i) {
        String str = (i == 6 || i == 7) ? "@NotNull method %s.%s must not return null" : "Argument for @NotNull parameter '%s' of %s.%s must not be null";
        Object[] objArr = new Object[(i == 6 || i == 7) ? 2 : 3];
        switch (i) {
            case 1:
                objArr[0] = "ownerScope";
                break;
            case 2:
                objArr[0] = "newOwner";
                break;
            case 3:
                objArr[0] = "kind";
                break;
            case 4:
                objArr[0] = "annotations";
                break;
            case 5:
                objArr[0] = "source";
                break;
            case 6:
            case 7:
                objArr[0] = "kotlin/reflect/jvm/internal/impl/types/error/ErrorSimpleFunctionDescriptorImpl";
                break;
            case 8:
                objArr[0] = "overriddenDescriptors";
                break;
            default:
                objArr[0] = "containingDeclaration";
                break;
        }
        if (i == 6) {
            objArr[1] = "createSubstitutedCopy";
        } else if (i != 7) {
            objArr[1] = "kotlin/reflect/jvm/internal/impl/types/error/ErrorSimpleFunctionDescriptorImpl";
        } else {
            objArr[1] = "copy";
        }
        switch (i) {
            case 2:
            case 3:
            case 4:
            case 5:
                objArr[2] = "createSubstitutedCopy";
                break;
            case 6:
            case 7:
                break;
            case 8:
                objArr[2] = "setOverriddenDescriptors";
                break;
            default:
                objArr[2] = HookHelper.constructorName;
                break;
        }
        String format = String.format(str, objArr);
        if (i == 6 || i == 7) {
            throw new IllegalStateException(format);
        }
    }

    @Override // d0.e0.p.d.m0.c.i1.g0, d0.e0.p.d.m0.c.i1.q
    public q b(m mVar, x xVar, b.a aVar, e eVar, g gVar, u0 u0Var) {
        if (mVar == null) {
            a(2);
            throw null;
        } else if (aVar == null) {
            a(3);
            throw null;
        } else if (gVar != null) {
            return this;
        } else {
            a(4);
            throw null;
        }
    }

    @Override // d0.e0.p.d.m0.c.i1.g0, d0.e0.p.d.m0.c.i1.q, d0.e0.p.d.m0.c.b
    public t0 copy(m mVar, z zVar, u uVar, b.a aVar, boolean z2) {
        return this;
    }

    @Override // d0.e0.p.d.m0.c.i1.q, d0.e0.p.d.m0.c.a
    public <V> V getUserData(a.AbstractC0290a<V> aVar) {
        return null;
    }

    @Override // d0.e0.p.d.m0.c.i1.q, d0.e0.p.d.m0.c.x
    public boolean isSuspend() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.i1.g0, d0.e0.p.d.m0.c.i1.q, d0.e0.p.d.m0.c.x, d0.e0.p.d.m0.c.t0
    public x.a<? extends t0> newCopyBuilder() {
        return new C0363a();
    }

    @Override // d0.e0.p.d.m0.c.i1.q, d0.e0.p.d.m0.c.b
    public void setOverriddenDescriptors(Collection<? extends b> collection) {
        if (collection == null) {
            a(8);
            throw null;
        }
    }
}
