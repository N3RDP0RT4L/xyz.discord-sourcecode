package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.c.x0;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.m.j;
import d0.e0.p.d.m0.n.l1.h;
import d0.i;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: AbstractTypeConstructor.kt */
/* loaded from: classes3.dex */
public abstract class g implements u0 {
    public final j<b> a;

    /* compiled from: AbstractTypeConstructor.kt */
    /* loaded from: classes3.dex */
    public final class a implements u0 {
        public final d0.e0.p.d.m0.n.l1.g a;

        /* renamed from: b  reason: collision with root package name */
        public final Lazy f3496b;
        public final /* synthetic */ g c;

        /* compiled from: AbstractTypeConstructor.kt */
        /* renamed from: d0.e0.p.d.m0.n.g$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0359a extends o implements Function0<List<? extends c0>> {
            public final /* synthetic */ g this$1;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C0359a(g gVar) {
                super(0);
                this.this$1 = gVar;
            }

            @Override // kotlin.jvm.functions.Function0
            public final List<? extends c0> invoke() {
                return h.refineTypes(a.this.a, this.this$1.getSupertypes());
            }
        }

        public a(g gVar, d0.e0.p.d.m0.n.l1.g gVar2) {
            m.checkNotNullParameter(gVar, "this$0");
            m.checkNotNullParameter(gVar2, "kotlinTypeRefiner");
            this.c = gVar;
            this.a = gVar2;
            this.f3496b = d0.g.lazy(i.PUBLICATION, new C0359a(gVar));
        }

        public boolean equals(Object obj) {
            return this.c.equals(obj);
        }

        @Override // d0.e0.p.d.m0.n.u0
        public d0.e0.p.d.m0.b.h getBuiltIns() {
            d0.e0.p.d.m0.b.h builtIns = this.c.getBuiltIns();
            m.checkNotNullExpressionValue(builtIns, "this@AbstractTypeConstructor.builtIns");
            return builtIns;
        }

        @Override // d0.e0.p.d.m0.n.u0
        public d0.e0.p.d.m0.c.h getDeclarationDescriptor() {
            return this.c.getDeclarationDescriptor();
        }

        @Override // d0.e0.p.d.m0.n.u0
        public List<z0> getParameters() {
            List<z0> parameters = this.c.getParameters();
            m.checkNotNullExpressionValue(parameters, "this@AbstractTypeConstructor.parameters");
            return parameters;
        }

        public int hashCode() {
            return this.c.hashCode();
        }

        @Override // d0.e0.p.d.m0.n.u0
        public boolean isDenotable() {
            return this.c.isDenotable();
        }

        @Override // d0.e0.p.d.m0.n.u0
        public u0 refine(d0.e0.p.d.m0.n.l1.g gVar) {
            m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
            return this.c.refine(gVar);
        }

        public String toString() {
            return this.c.toString();
        }

        @Override // d0.e0.p.d.m0.n.u0
        public List<c0> getSupertypes() {
            return (List) this.f3496b.getValue();
        }
    }

    /* compiled from: AbstractTypeConstructor.kt */
    /* loaded from: classes3.dex */
    public static final class b {
        public final Collection<c0> a;

        /* renamed from: b  reason: collision with root package name */
        public List<? extends c0> f3497b = d0.t.m.listOf(t.c);

        /* JADX WARN: Multi-variable type inference failed */
        public b(Collection<? extends c0> collection) {
            m.checkNotNullParameter(collection, "allSupertypes");
            this.a = collection;
        }

        public final Collection<c0> getAllSupertypes() {
            return this.a;
        }

        public final List<c0> getSupertypesWithoutCycles() {
            return this.f3497b;
        }

        public final void setSupertypesWithoutCycles(List<? extends c0> list) {
            m.checkNotNullParameter(list, "<set-?>");
            this.f3497b = list;
        }
    }

    /* compiled from: AbstractTypeConstructor.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function0<b> {
        public c() {
            super(0);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final b invoke() {
            return new b(g.this.a());
        }
    }

    /* compiled from: AbstractTypeConstructor.kt */
    /* loaded from: classes3.dex */
    public static final class d extends o implements Function1<Boolean, b> {
        public static final d j = new d();

        public d() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ b invoke(Boolean bool) {
            return invoke(bool.booleanValue());
        }

        public final b invoke(boolean z2) {
            return new b(d0.t.m.listOf(t.c));
        }
    }

    /* compiled from: AbstractTypeConstructor.kt */
    /* loaded from: classes3.dex */
    public static final class e extends o implements Function1<b, Unit> {

        /* compiled from: AbstractTypeConstructor.kt */
        /* loaded from: classes3.dex */
        public static final class a extends o implements Function1<u0, Iterable<? extends c0>> {
            public final /* synthetic */ g this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public a(g gVar) {
                super(1);
                this.this$0 = gVar;
            }

            public final Iterable<c0> invoke(u0 u0Var) {
                m.checkNotNullParameter(u0Var, "it");
                return g.access$computeNeighbours(this.this$0, u0Var, false);
            }
        }

        /* compiled from: AbstractTypeConstructor.kt */
        /* loaded from: classes3.dex */
        public static final class b extends o implements Function1<c0, Unit> {
            public final /* synthetic */ g this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public b(g gVar) {
                super(1);
                this.this$0 = gVar;
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(c0 c0Var) {
                invoke2(c0Var);
                return Unit.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(c0 c0Var) {
                m.checkNotNullParameter(c0Var, "it");
                this.this$0.f(c0Var);
            }
        }

        public e() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(b bVar) {
            invoke2(bVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(b bVar) {
            m.checkNotNullParameter(bVar, "supertypes");
            Collection<c0> findLoopsInSupertypesAndDisconnect = g.this.d().findLoopsInSupertypesAndDisconnect(g.this, bVar.getAllSupertypes(), new a(g.this), new b(g.this));
            List<c0> list = null;
            if (findLoopsInSupertypesAndDisconnect.isEmpty()) {
                c0 b2 = g.this.b();
                findLoopsInSupertypesAndDisconnect = b2 == null ? null : d0.t.m.listOf(b2);
                if (findLoopsInSupertypesAndDisconnect == null) {
                    findLoopsInSupertypesAndDisconnect = n.emptyList();
                }
            }
            Objects.requireNonNull(g.this);
            g gVar = g.this;
            if (findLoopsInSupertypesAndDisconnect instanceof List) {
                list = (List) findLoopsInSupertypesAndDisconnect;
            }
            if (list == null) {
                list = u.toList(findLoopsInSupertypesAndDisconnect);
            }
            bVar.setSupertypesWithoutCycles(gVar.e(list));
        }
    }

    public g(d0.e0.p.d.m0.m.o oVar) {
        m.checkNotNullParameter(oVar, "storageManager");
        this.a = oVar.createLazyValueWithPostCompute(new c(), d.j, new e());
    }

    public static final Collection access$computeNeighbours(g gVar, u0 u0Var, boolean z2) {
        Objects.requireNonNull(gVar);
        List list = null;
        g gVar2 = u0Var instanceof g ? (g) u0Var : null;
        if (gVar2 != null) {
            list = u.plus((Collection) gVar2.a.invoke().getAllSupertypes(), (Iterable) gVar2.c(z2));
        }
        if (list != null) {
            return list;
        }
        Collection<c0> supertypes = u0Var.getSupertypes();
        m.checkNotNullExpressionValue(supertypes, "supertypes");
        return supertypes;
    }

    public abstract Collection<c0> a();

    public c0 b() {
        return null;
    }

    public Collection<c0> c(boolean z2) {
        return n.emptyList();
    }

    public abstract x0 d();

    public List<c0> e(List<c0> list) {
        m.checkNotNullParameter(list, "supertypes");
        return list;
    }

    public void f(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "type");
    }

    @Override // d0.e0.p.d.m0.n.u0
    public abstract d0.e0.p.d.m0.c.h getDeclarationDescriptor();

    @Override // d0.e0.p.d.m0.n.u0
    public u0 refine(d0.e0.p.d.m0.n.l1.g gVar) {
        m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
        return new a(this, gVar);
    }

    @Override // d0.e0.p.d.m0.n.u0
    public List<c0> getSupertypes() {
        return this.a.invoke().getSupertypesWithoutCycles();
    }
}
