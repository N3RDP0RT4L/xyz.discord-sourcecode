package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.b.h;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.n.l1.g;
import d0.e0.p.d.m0.n.n1.l;
import java.util.Collection;
import java.util.List;
/* compiled from: TypeConstructor.java */
/* loaded from: classes3.dex */
public interface u0 extends l {
    h getBuiltIns();

    d0.e0.p.d.m0.c.h getDeclarationDescriptor();

    List<z0> getParameters();

    Collection<c0> getSupertypes();

    boolean isDenotable();

    u0 refine(g gVar);
}
