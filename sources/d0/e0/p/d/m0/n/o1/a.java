package d0.e0.p.d.m0.n.o1;

import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.y0;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.n.a1;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.d0;
import d0.e0.p.d.m0.n.e1;
import d0.e0.p.d.m0.n.g1;
import d0.e0.p.d.m0.n.i1;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.j1;
import d0.e0.p.d.m0.n.l1.f;
import d0.e0.p.d.m0.n.o0;
import d0.e0.p.d.m0.n.v;
import d0.e0.p.d.m0.n.w0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.functions.Function1;
/* compiled from: TypeUtils.kt */
/* loaded from: classes3.dex */
public final class a {

    /* compiled from: TypeUtils.kt */
    /* renamed from: d0.e0.p.d.m0.n.o1.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0364a extends o implements Function1<i1, Boolean> {
        public static final C0364a j = new C0364a();

        public C0364a() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Boolean invoke(i1 i1Var) {
            return Boolean.valueOf(invoke2(i1Var));
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final boolean invoke2(i1 i1Var) {
            m.checkNotNullParameter(i1Var, "it");
            h declarationDescriptor = i1Var.getConstructor().getDeclarationDescriptor();
            if (declarationDescriptor == null) {
                return false;
            }
            return a.isTypeAliasParameter(declarationDescriptor);
        }
    }

    /* compiled from: TypeUtils.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function1<i1, Boolean> {
        public static final b j = new b();

        public b() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Boolean invoke(i1 i1Var) {
            return Boolean.valueOf(invoke2(i1Var));
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final boolean invoke2(i1 i1Var) {
            m.checkNotNullParameter(i1Var, "it");
            h declarationDescriptor = i1Var.getConstructor().getDeclarationDescriptor();
            if (declarationDescriptor == null) {
                return false;
            }
            return (declarationDescriptor instanceof y0) || (declarationDescriptor instanceof z0);
        }
    }

    public static final w0 asTypeProjection(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        return new d0.e0.p.d.m0.n.y0(c0Var);
    }

    public static final boolean contains(c0 c0Var, Function1<? super i1, Boolean> function1) {
        m.checkNotNullParameter(c0Var, "<this>");
        m.checkNotNullParameter(function1, "predicate");
        return e1.contains(c0Var, function1);
    }

    public static final boolean containsTypeAliasParameters(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        return contains(c0Var, C0364a.j);
    }

    public static final w0 createProjection(c0 c0Var, j1 j1Var, z0 z0Var) {
        m.checkNotNullParameter(c0Var, "type");
        m.checkNotNullParameter(j1Var, "projectionKind");
        if ((z0Var == null ? null : z0Var.getVariance()) == j1Var) {
            j1Var = j1.INVARIANT;
        }
        return new d0.e0.p.d.m0.n.y0(j1Var, c0Var);
    }

    public static final d0.e0.p.d.m0.b.h getBuiltIns(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        d0.e0.p.d.m0.b.h builtIns = c0Var.getConstructor().getBuiltIns();
        m.checkNotNullExpressionValue(builtIns, "constructor.builtIns");
        return builtIns;
    }

    /* JADX WARN: Code restructure failed: missing block: B:16:0x0050, code lost:
        r3 = r2;
     */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:21:0x0051 A[EDGE_INSN: B:21:0x0051->B:17:0x0051 ?: BREAK  , SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:5:0x0023  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final d0.e0.p.d.m0.n.c0 getRepresentativeUpperBound(d0.e0.p.d.m0.c.z0 r7) {
        /*
            java.lang.String r0 = "<this>"
            d0.z.d.m.checkNotNullParameter(r7, r0)
            java.util.List r0 = r7.getUpperBounds()
            java.lang.String r1 = "upperBounds"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            r0.isEmpty()
            java.util.List r0 = r7.getUpperBounds()
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            java.util.Iterator r0 = r0.iterator()
        L1c:
            boolean r2 = r0.hasNext()
            r3 = 0
            if (r2 == 0) goto L51
            java.lang.Object r2 = r0.next()
            r4 = r2
            d0.e0.p.d.m0.n.c0 r4 = (d0.e0.p.d.m0.n.c0) r4
            d0.e0.p.d.m0.n.u0 r4 = r4.getConstructor()
            d0.e0.p.d.m0.c.h r4 = r4.getDeclarationDescriptor()
            boolean r5 = r4 instanceof d0.e0.p.d.m0.c.e
            if (r5 == 0) goto L39
            r3 = r4
            d0.e0.p.d.m0.c.e r3 = (d0.e0.p.d.m0.c.e) r3
        L39:
            r4 = 0
            if (r3 != 0) goto L3d
            goto L4e
        L3d:
            d0.e0.p.d.m0.c.f r5 = r3.getKind()
            d0.e0.p.d.m0.c.f r6 = d0.e0.p.d.m0.c.f.INTERFACE
            if (r5 == r6) goto L4e
            d0.e0.p.d.m0.c.f r3 = r3.getKind()
            d0.e0.p.d.m0.c.f r5 = d0.e0.p.d.m0.c.f.ANNOTATION_CLASS
            if (r3 == r5) goto L4e
            r4 = 1
        L4e:
            if (r4 == 0) goto L1c
            r3 = r2
        L51:
            d0.e0.p.d.m0.n.c0 r3 = (d0.e0.p.d.m0.n.c0) r3
            if (r3 != 0) goto L68
            java.util.List r7 = r7.getUpperBounds()
            d0.z.d.m.checkNotNullExpressionValue(r7, r1)
            java.lang.Object r7 = d0.t.u.first(r7)
            java.lang.String r0 = "upperBounds.first()"
            d0.z.d.m.checkNotNullExpressionValue(r7, r0)
            r3 = r7
            d0.e0.p.d.m0.n.c0 r3 = (d0.e0.p.d.m0.n.c0) r3
        L68:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.n.o1.a.getRepresentativeUpperBound(d0.e0.p.d.m0.c.z0):d0.e0.p.d.m0.n.c0");
    }

    public static final boolean isSubtypeOf(c0 c0Var, c0 c0Var2) {
        m.checkNotNullParameter(c0Var, "<this>");
        m.checkNotNullParameter(c0Var2, "superType");
        return f.a.isSubtypeOf(c0Var, c0Var2);
    }

    public static final boolean isTypeAliasParameter(h hVar) {
        m.checkNotNullParameter(hVar, "<this>");
        return (hVar instanceof z0) && (((z0) hVar).getContainingDeclaration() instanceof y0);
    }

    public static final boolean isTypeParameter(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        return e1.isTypeParameter(c0Var);
    }

    public static final c0 makeNotNullable(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        c0 makeNotNullable = e1.makeNotNullable(c0Var);
        m.checkNotNullExpressionValue(makeNotNullable, "makeNotNullable(this)");
        return makeNotNullable;
    }

    public static final c0 makeNullable(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        c0 makeNullable = e1.makeNullable(c0Var);
        m.checkNotNullExpressionValue(makeNullable, "makeNullable(this)");
        return makeNullable;
    }

    public static final c0 replaceAnnotations(c0 c0Var, g gVar) {
        m.checkNotNullParameter(c0Var, "<this>");
        m.checkNotNullParameter(gVar, "newAnnotations");
        return (!c0Var.getAnnotations().isEmpty() || !gVar.isEmpty()) ? c0Var.unwrap().replaceAnnotations(gVar) : c0Var;
    }

    public static final c0 replaceArgumentsWithStarProjections(c0 c0Var) {
        j0 j0Var;
        m.checkNotNullParameter(c0Var, "<this>");
        i1 unwrap = c0Var.unwrap();
        if (unwrap instanceof v) {
            d0 d0Var = d0.a;
            v vVar = (v) unwrap;
            j0 lowerBound = vVar.getLowerBound();
            if (!lowerBound.getConstructor().getParameters().isEmpty() && lowerBound.getConstructor().getDeclarationDescriptor() != null) {
                List<z0> parameters = lowerBound.getConstructor().getParameters();
                m.checkNotNullExpressionValue(parameters, "constructor.parameters");
                ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(parameters, 10));
                for (z0 z0Var : parameters) {
                    arrayList.add(new o0(z0Var));
                }
                lowerBound = a1.replace$default(lowerBound, arrayList, null, 2, null);
            }
            j0 upperBound = vVar.getUpperBound();
            if (!upperBound.getConstructor().getParameters().isEmpty() && upperBound.getConstructor().getDeclarationDescriptor() != null) {
                List<z0> parameters2 = upperBound.getConstructor().getParameters();
                m.checkNotNullExpressionValue(parameters2, "constructor.parameters");
                ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(parameters2, 10));
                for (z0 z0Var2 : parameters2) {
                    arrayList2.add(new o0(z0Var2));
                }
                upperBound = a1.replace$default(upperBound, arrayList2, null, 2, null);
            }
            j0Var = d0.flexibleType(lowerBound, upperBound);
        } else if (unwrap instanceof j0) {
            j0 j0Var2 = (j0) unwrap;
            boolean isEmpty = j0Var2.getConstructor().getParameters().isEmpty();
            j0Var = j0Var2;
            if (!isEmpty) {
                h declarationDescriptor = j0Var2.getConstructor().getDeclarationDescriptor();
                j0Var = j0Var2;
                if (declarationDescriptor != null) {
                    List<z0> parameters3 = j0Var2.getConstructor().getParameters();
                    m.checkNotNullExpressionValue(parameters3, "constructor.parameters");
                    ArrayList arrayList3 = new ArrayList(d0.t.o.collectionSizeOrDefault(parameters3, 10));
                    for (z0 z0Var3 : parameters3) {
                        arrayList3.add(new o0(z0Var3));
                    }
                    j0Var = a1.replace$default(j0Var2, arrayList3, null, 2, null);
                }
            }
        } else {
            throw new NoWhenBranchMatchedException();
        }
        return g1.inheritEnhancement(j0Var, unwrap);
    }

    public static final boolean requiresTypeAliasExpansion(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        return contains(c0Var, b.j);
    }
}
