package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.e0;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.k.e;
import d0.e0.p.d.m0.k.x.a;
import d0.e0.p.d.m0.m.o;
import d0.e0.p.d.m0.p.i;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
/* compiled from: AbstractClassTypeConstructor.java */
/* loaded from: classes3.dex */
public abstract class b extends g implements u0 {

    /* renamed from: b  reason: collision with root package name */
    public int f3491b;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public b(o oVar) {
        super(oVar);
        if (oVar != null) {
            this.f3491b = 0;
            return;
        }
        g(0);
        throw null;
    }

    /* JADX WARN: Removed duplicated region for block: B:23:0x0033  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x003f  */
    /* JADX WARN: Removed duplicated region for block: B:29:0x0045  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static /* synthetic */ void g(int r9) {
        /*
            r0 = 4
            r1 = 3
            r2 = 1
            if (r9 == r2) goto Lc
            if (r9 == r1) goto Lc
            if (r9 == r0) goto Lc
            java.lang.String r3 = "Argument for @NotNull parameter '%s' of %s.%s must not be null"
            goto Le
        Lc:
            java.lang.String r3 = "@NotNull method %s.%s must not return null"
        Le:
            r4 = 2
            if (r9 == r2) goto L17
            if (r9 == r1) goto L17
            if (r9 == r0) goto L17
            r5 = 3
            goto L18
        L17:
            r5 = 2
        L18:
            java.lang.Object[] r5 = new java.lang.Object[r5]
            java.lang.String r6 = "kotlin/reflect/jvm/internal/impl/types/AbstractClassTypeConstructor"
            r7 = 0
            if (r9 == r2) goto L2f
            if (r9 == r4) goto L2a
            if (r9 == r1) goto L2f
            if (r9 == r0) goto L2f
            java.lang.String r8 = "storageManager"
            r5[r7] = r8
            goto L31
        L2a:
            java.lang.String r8 = "descriptor"
            r5[r7] = r8
            goto L31
        L2f:
            r5[r7] = r6
        L31:
            if (r9 == r2) goto L3f
            if (r9 == r1) goto L3a
            if (r9 == r0) goto L3a
            r5[r2] = r6
            goto L43
        L3a:
            java.lang.String r6 = "getAdditionalNeighboursInSupertypeGraph"
            r5[r2] = r6
            goto L43
        L3f:
            java.lang.String r6 = "getBuiltIns"
            r5[r2] = r6
        L43:
            if (r9 == r2) goto L54
            if (r9 == r4) goto L50
            if (r9 == r1) goto L54
            if (r9 == r0) goto L54
            java.lang.String r6 = "<init>"
            r5[r4] = r6
            goto L54
        L50:
            java.lang.String r6 = "hasMeaningfulFqName"
            r5[r4] = r6
        L54:
            java.lang.String r3 = java.lang.String.format(r3, r5)
            if (r9 == r2) goto L64
            if (r9 == r1) goto L64
            if (r9 == r0) goto L64
            java.lang.IllegalArgumentException r9 = new java.lang.IllegalArgumentException
            r9.<init>(r3)
            goto L69
        L64:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            r9.<init>(r3)
        L69:
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.n.b.g(int):void");
    }

    public static boolean h(h hVar) {
        if (hVar != null) {
            return !t.isError(hVar) && !e.isLocal(hVar);
        }
        g(2);
        throw null;
    }

    @Override // d0.e0.p.d.m0.n.g
    public c0 b() {
        if (d0.e0.p.d.m0.b.h.isSpecialClassWithNoSupertypes(getDeclarationDescriptor())) {
            return null;
        }
        return getBuiltIns().getAnyType();
    }

    @Override // d0.e0.p.d.m0.n.g
    public Collection<c0> c(boolean z2) {
        m containingDeclaration = getDeclarationDescriptor().getContainingDeclaration();
        if (!(containingDeclaration instanceof d0.e0.p.d.m0.c.e)) {
            List emptyList = Collections.emptyList();
            if (emptyList != null) {
                return emptyList;
            }
            g(3);
            throw null;
        }
        i iVar = new i();
        d0.e0.p.d.m0.c.e eVar = (d0.e0.p.d.m0.c.e) containingDeclaration;
        iVar.add(eVar.getDefaultType());
        d0.e0.p.d.m0.c.e companionObjectDescriptor = eVar.getCompanionObjectDescriptor();
        if (z2 && companionObjectDescriptor != null) {
            iVar.add(companionObjectDescriptor.getDefaultType());
        }
        return iVar;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!((obj instanceof u0) && obj.hashCode() == hashCode())) {
            return false;
        }
        u0 u0Var = (u0) obj;
        if (u0Var.getParameters().size() != getParameters().size()) {
            return false;
        }
        d0.e0.p.d.m0.c.e declarationDescriptor = getDeclarationDescriptor();
        h declarationDescriptor2 = u0Var.getDeclarationDescriptor();
        if (!h(declarationDescriptor) || ((declarationDescriptor2 != null && !h(declarationDescriptor2)) || !(declarationDescriptor2 instanceof d0.e0.p.d.m0.c.e))) {
            return false;
        }
        d0.e0.p.d.m0.c.e eVar = (d0.e0.p.d.m0.c.e) declarationDescriptor2;
        if (declarationDescriptor.getName().equals(eVar.getName())) {
            m containingDeclaration = declarationDescriptor.getContainingDeclaration();
            for (m containingDeclaration2 = eVar.getContainingDeclaration(); containingDeclaration != null && containingDeclaration2 != null; containingDeclaration2 = containingDeclaration2.getContainingDeclaration()) {
                if (containingDeclaration instanceof c0) {
                    return containingDeclaration2 instanceof c0;
                }
                if (!(containingDeclaration2 instanceof c0)) {
                    if (containingDeclaration instanceof e0) {
                        if ((containingDeclaration2 instanceof e0) && ((e0) containingDeclaration).getFqName().equals(((e0) containingDeclaration2).getFqName())) {
                            return true;
                        }
                    } else if (!(containingDeclaration2 instanceof e0) && containingDeclaration.getName().equals(containingDeclaration2.getName())) {
                        containingDeclaration = containingDeclaration.getContainingDeclaration();
                    }
                }
            }
            return true;
        }
        return false;
    }

    @Override // d0.e0.p.d.m0.n.u0
    public d0.e0.p.d.m0.b.h getBuiltIns() {
        d0.e0.p.d.m0.b.h builtIns = a.getBuiltIns(getDeclarationDescriptor());
        if (builtIns != null) {
            return builtIns;
        }
        g(1);
        throw null;
    }

    @Override // d0.e0.p.d.m0.n.g, d0.e0.p.d.m0.n.u0
    public abstract d0.e0.p.d.m0.c.e getDeclarationDescriptor();

    public final int hashCode() {
        int i;
        int i2 = this.f3491b;
        if (i2 != 0) {
            return i2;
        }
        d0.e0.p.d.m0.c.e declarationDescriptor = getDeclarationDescriptor();
        if (h(declarationDescriptor)) {
            i = e.getFqName(declarationDescriptor).hashCode();
        } else {
            i = System.identityHashCode(this);
        }
        this.f3491b = i;
        return i;
    }
}
