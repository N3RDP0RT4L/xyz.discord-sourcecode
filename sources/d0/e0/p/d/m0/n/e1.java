package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.n.l1.g;
import d0.e0.p.d.m0.p.j;
import d0.t.u;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.jvm.functions.Function1;
/* compiled from: TypeUtils.java */
/* loaded from: classes3.dex */
public class e1 {
    public static final j0 a = t.createErrorTypeWithCustomDebugName("DONT_CARE");

    /* renamed from: b  reason: collision with root package name */
    public static final j0 f3494b = t.createErrorType("Cannot be inferred");
    public static final j0 c = new a("NO_EXPECTED_TYPE");
    public static final j0 d = new a("UNIT_EXPECTED_TYPE");

    /* compiled from: TypeUtils.java */
    /* loaded from: classes3.dex */
    public static class a extends m {
        public final String k;

        public a(String str) {
            this.k = str;
        }

        /* JADX WARN: Removed duplicated region for block: B:23:0x0036  */
        /* JADX WARN: Removed duplicated region for block: B:26:0x003e  */
        /* JADX WARN: Removed duplicated region for block: B:28:0x0044  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public static /* synthetic */ void a(int r9) {
            /*
                r0 = 4
                r1 = 1
                if (r9 == r1) goto L9
                if (r9 == r0) goto L9
                java.lang.String r2 = "Argument for @NotNull parameter '%s' of %s.%s must not be null"
                goto Lb
            L9:
                java.lang.String r2 = "@NotNull method %s.%s must not return null"
            Lb:
                r3 = 3
                r4 = 2
                if (r9 == r1) goto L13
                if (r9 == r0) goto L13
                r5 = 3
                goto L14
            L13:
                r5 = 2
            L14:
                java.lang.Object[] r5 = new java.lang.Object[r5]
                java.lang.String r6 = "kotlin/reflect/jvm/internal/impl/types/TypeUtils$SpecialType"
                r7 = 0
                if (r9 == r1) goto L30
                if (r9 == r4) goto L2b
                if (r9 == r3) goto L26
                if (r9 == r0) goto L30
                java.lang.String r8 = "newAnnotations"
                r5[r7] = r8
                goto L32
            L26:
                java.lang.String r8 = "kotlinTypeRefiner"
                r5[r7] = r8
                goto L32
            L2b:
                java.lang.String r8 = "delegate"
                r5[r7] = r8
                goto L32
            L30:
                r5[r7] = r6
            L32:
                java.lang.String r7 = "refine"
                if (r9 == r1) goto L3e
                if (r9 == r0) goto L3b
                r5[r1] = r6
                goto L42
            L3b:
                r5[r1] = r7
                goto L42
            L3e:
                java.lang.String r6 = "toString"
                r5[r1] = r6
            L42:
                if (r9 == r1) goto L56
                if (r9 == r4) goto L52
                if (r9 == r3) goto L4f
                if (r9 == r0) goto L56
                java.lang.String r3 = "replaceAnnotations"
                r5[r4] = r3
                goto L56
            L4f:
                r5[r4] = r7
                goto L56
            L52:
                java.lang.String r3 = "replaceDelegate"
                r5[r4] = r3
            L56:
                java.lang.String r2 = java.lang.String.format(r2, r5)
                if (r9 == r1) goto L64
                if (r9 == r0) goto L64
                java.lang.IllegalArgumentException r9 = new java.lang.IllegalArgumentException
                r9.<init>(r2)
                goto L69
            L64:
                java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
                r9.<init>(r2)
            L69:
                throw r9
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.n.e1.a.a(int):void");
        }

        @Override // d0.e0.p.d.m0.n.m
        public j0 getDelegate() {
            throw new IllegalStateException(this.k);
        }

        @Override // d0.e0.p.d.m0.n.m, d0.e0.p.d.m0.n.i1, d0.e0.p.d.m0.n.c0
        public a refine(g gVar) {
            if (gVar != null) {
                return this;
            }
            a(3);
            throw null;
        }

        @Override // d0.e0.p.d.m0.n.m
        public m replaceDelegate(j0 j0Var) {
            if (j0Var == null) {
                a(2);
                throw null;
            }
            throw new IllegalStateException(this.k);
        }

        @Override // d0.e0.p.d.m0.n.j0
        public String toString() {
            String str = this.k;
            if (str != null) {
                return str;
            }
            a(1);
            throw null;
        }

        @Override // d0.e0.p.d.m0.n.j0, d0.e0.p.d.m0.n.i1
        public j0 makeNullableAsSpecified(boolean z2) {
            throw new IllegalStateException(this.k);
        }

        @Override // d0.e0.p.d.m0.n.j0, d0.e0.p.d.m0.n.i1
        public j0 replaceAnnotations(d0.e0.p.d.m0.c.g1.g gVar) {
            if (gVar == null) {
                a(0);
                throw null;
            }
            throw new IllegalStateException(this.k);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:100:0x017d  */
    /* JADX WARN: Removed duplicated region for block: B:101:0x0182  */
    /* JADX WARN: Removed duplicated region for block: B:102:0x0187  */
    /* JADX WARN: Removed duplicated region for block: B:103:0x018c  */
    /* JADX WARN: Removed duplicated region for block: B:104:0x0191  */
    /* JADX WARN: Removed duplicated region for block: B:105:0x0194  */
    /* JADX WARN: Removed duplicated region for block: B:106:0x0199  */
    /* JADX WARN: Removed duplicated region for block: B:107:0x019e  */
    /* JADX WARN: Removed duplicated region for block: B:108:0x01a1  */
    /* JADX WARN: Removed duplicated region for block: B:109:0x01a4  */
    /* JADX WARN: Removed duplicated region for block: B:110:0x01a7  */
    /* JADX WARN: Removed duplicated region for block: B:111:0x01ac  */
    /* JADX WARN: Removed duplicated region for block: B:112:0x01af  */
    /* JADX WARN: Removed duplicated region for block: B:113:0x01b2  */
    /* JADX WARN: Removed duplicated region for block: B:114:0x01b7  */
    /* JADX WARN: Removed duplicated region for block: B:117:0x01c1 A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:129:0x01da  */
    /* JADX WARN: Removed duplicated region for block: B:32:0x0053  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x005f  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x0065  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x006b  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x0071  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x0077  */
    /* JADX WARN: Removed duplicated region for block: B:41:0x007d  */
    /* JADX WARN: Removed duplicated region for block: B:42:0x0082  */
    /* JADX WARN: Removed duplicated region for block: B:43:0x0087  */
    /* JADX WARN: Removed duplicated region for block: B:44:0x008c  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x0091  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x0096  */
    /* JADX WARN: Removed duplicated region for block: B:47:0x009b  */
    /* JADX WARN: Removed duplicated region for block: B:48:0x00a0  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x00a5  */
    /* JADX WARN: Removed duplicated region for block: B:50:0x00aa  */
    /* JADX WARN: Removed duplicated region for block: B:51:0x00af  */
    /* JADX WARN: Removed duplicated region for block: B:52:0x00b4  */
    /* JADX WARN: Removed duplicated region for block: B:53:0x00b9  */
    /* JADX WARN: Removed duplicated region for block: B:54:0x00be  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x00c3  */
    /* JADX WARN: Removed duplicated region for block: B:56:0x00c8  */
    /* JADX WARN: Removed duplicated region for block: B:57:0x00cd  */
    /* JADX WARN: Removed duplicated region for block: B:58:0x00d2  */
    /* JADX WARN: Removed duplicated region for block: B:59:0x00d7  */
    /* JADX WARN: Removed duplicated region for block: B:62:0x00e9  */
    /* JADX WARN: Removed duplicated region for block: B:83:0x0123  */
    /* JADX WARN: Removed duplicated region for block: B:85:0x0128  */
    /* JADX WARN: Removed duplicated region for block: B:86:0x012e  */
    /* JADX WARN: Removed duplicated region for block: B:87:0x0134  */
    /* JADX WARN: Removed duplicated region for block: B:88:0x013a  */
    /* JADX WARN: Removed duplicated region for block: B:89:0x0140  */
    /* JADX WARN: Removed duplicated region for block: B:90:0x0146  */
    /* JADX WARN: Removed duplicated region for block: B:91:0x014c  */
    /* JADX WARN: Removed duplicated region for block: B:92:0x0152  */
    /* JADX WARN: Removed duplicated region for block: B:93:0x0158  */
    /* JADX WARN: Removed duplicated region for block: B:94:0x015e  */
    /* JADX WARN: Removed duplicated region for block: B:95:0x0164  */
    /* JADX WARN: Removed duplicated region for block: B:96:0x0169  */
    /* JADX WARN: Removed duplicated region for block: B:97:0x016e  */
    /* JADX WARN: Removed duplicated region for block: B:98:0x0173  */
    /* JADX WARN: Removed duplicated region for block: B:99:0x0178  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static /* synthetic */ void a(int r24) {
        /*
            Method dump skipped, instructions count: 776
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.n.e1.a(int):void");
    }

    public static boolean acceptsNullable(c0 c0Var) {
        if (c0Var == null) {
            a(28);
            throw null;
        } else if (c0Var.isMarkedNullable()) {
            return true;
        } else {
            return y.isFlexible(c0Var) && acceptsNullable(y.asFlexibleType(c0Var).getUpperBound());
        }
    }

    public static boolean b(c0 c0Var, Function1<i1, Boolean> function1, j<c0> jVar) {
        v vVar = null;
        if (function1 == null) {
            a(44);
            throw null;
        } else if (c0Var == null) {
            return false;
        } else {
            i1 unwrap = c0Var.unwrap();
            if (noExpectedType(c0Var)) {
                return function1.invoke(unwrap).booleanValue();
            }
            if (jVar != null && jVar.contains(c0Var)) {
                return false;
            }
            if (function1.invoke(unwrap).booleanValue()) {
                return true;
            }
            if (jVar == null) {
                jVar = j.create();
            }
            jVar.add(c0Var);
            if (unwrap instanceof v) {
                vVar = (v) unwrap;
            }
            if (vVar != null && (b(vVar.getLowerBound(), function1, jVar) || b(vVar.getUpperBound(), function1, jVar))) {
                return true;
            }
            if ((unwrap instanceof k) && b(((k) unwrap).getOriginal(), function1, jVar)) {
                return true;
            }
            u0 constructor = c0Var.getConstructor();
            if (constructor instanceof a0) {
                for (c0 c0Var2 : ((a0) constructor).getSupertypes()) {
                    if (b(c0Var2, function1, jVar)) {
                        return true;
                    }
                }
                return false;
            }
            for (w0 w0Var : c0Var.getArguments()) {
                if (!w0Var.isStarProjection()) {
                    if (b(w0Var.getType(), function1, jVar)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    public static boolean contains(c0 c0Var, Function1<i1, Boolean> function1) {
        if (function1 != null) {
            return b(c0Var, function1, null);
        }
        a(43);
        throw null;
    }

    public static c0 createSubstitutedSupertype(c0 c0Var, c0 c0Var2, c1 c1Var) {
        if (c0Var == null) {
            a(20);
            throw null;
        } else if (c0Var2 == null) {
            a(21);
            throw null;
        } else if (c1Var != null) {
            c0 substitute = c1Var.substitute(c0Var2, j1.INVARIANT);
            if (substitute != null) {
                return makeNullableIfNeeded(substitute, c0Var.isMarkedNullable());
            }
            return null;
        } else {
            a(22);
            throw null;
        }
    }

    public static e getClassDescriptor(c0 c0Var) {
        if (c0Var != null) {
            h declarationDescriptor = c0Var.getConstructor().getDeclarationDescriptor();
            if (declarationDescriptor instanceof e) {
                return (e) declarationDescriptor;
            }
            return null;
        }
        a(30);
        throw null;
    }

    public static List<w0> getDefaultTypeProjections(List<z0> list) {
        if (list != null) {
            ArrayList arrayList = new ArrayList(list.size());
            for (z0 z0Var : list) {
                arrayList.add(new y0(z0Var.getDefaultType()));
            }
            List<w0> list2 = u.toList(arrayList);
            if (list2 != null) {
                return list2;
            }
            a(17);
            throw null;
        }
        a(16);
        throw null;
    }

    public static List<c0> getImmediateSupertypes(c0 c0Var) {
        if (c0Var != null) {
            c1 create = c1.create(c0Var);
            Collection<c0> supertypes = c0Var.getConstructor().getSupertypes();
            ArrayList arrayList = new ArrayList(supertypes.size());
            for (c0 c0Var2 : supertypes) {
                c0 createSubstitutedSupertype = createSubstitutedSupertype(c0Var, c0Var2, create);
                if (createSubstitutedSupertype != null) {
                    arrayList.add(createSubstitutedSupertype);
                }
            }
            return arrayList;
        }
        a(18);
        throw null;
    }

    public static z0 getTypeParameterDescriptorOrNull(c0 c0Var) {
        if (c0Var == null) {
            a(62);
            throw null;
        } else if (c0Var.getConstructor().getDeclarationDescriptor() instanceof z0) {
            return (z0) c0Var.getConstructor().getDeclarationDescriptor();
        } else {
            return null;
        }
    }

    public static boolean hasNullableSuperType(c0 c0Var) {
        if (c0Var == null) {
            a(29);
            throw null;
        } else if (c0Var.getConstructor().getDeclarationDescriptor() instanceof e) {
            return false;
        } else {
            for (c0 c0Var2 : getImmediateSupertypes(c0Var)) {
                if (isNullableType(c0Var2)) {
                    return true;
                }
            }
            return false;
        }
    }

    public static boolean isDontCarePlaceholder(c0 c0Var) {
        return c0Var != null && c0Var.getConstructor() == a.getConstructor();
    }

    public static boolean isNullableType(c0 c0Var) {
        if (c0Var == null) {
            a(27);
            throw null;
        } else if (c0Var.isMarkedNullable()) {
            return true;
        } else {
            if (y.isFlexible(c0Var) && isNullableType(y.asFlexibleType(c0Var).getUpperBound())) {
                return true;
            }
            if (m0.isDefinitelyNotNullType(c0Var)) {
                return false;
            }
            if (isTypeParameter(c0Var)) {
                return hasNullableSuperType(c0Var);
            }
            u0 constructor = c0Var.getConstructor();
            if (constructor instanceof a0) {
                for (c0 c0Var2 : constructor.getSupertypes()) {
                    if (isNullableType(c0Var2)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    public static boolean isTypeParameter(c0 c0Var) {
        if (c0Var == null) {
            a(59);
            throw null;
        } else if (getTypeParameterDescriptorOrNull(c0Var) != null) {
            return true;
        } else {
            c0Var.getConstructor();
            return false;
        }
    }

    public static c0 makeNotNullable(c0 c0Var) {
        if (c0Var != null) {
            return makeNullableAsSpecified(c0Var, false);
        }
        a(2);
        throw null;
    }

    public static c0 makeNullable(c0 c0Var) {
        if (c0Var != null) {
            return makeNullableAsSpecified(c0Var, true);
        }
        a(1);
        throw null;
    }

    public static c0 makeNullableAsSpecified(c0 c0Var, boolean z2) {
        if (c0Var != null) {
            i1 makeNullableAsSpecified = c0Var.unwrap().makeNullableAsSpecified(z2);
            if (makeNullableAsSpecified != null) {
                return makeNullableAsSpecified;
            }
            a(4);
            throw null;
        }
        a(3);
        throw null;
    }

    public static j0 makeNullableIfNeeded(j0 j0Var, boolean z2) {
        if (j0Var == null) {
            a(5);
            throw null;
        } else if (z2) {
            j0 makeNullableAsSpecified = j0Var.makeNullableAsSpecified(true);
            if (makeNullableAsSpecified != null) {
                return makeNullableAsSpecified;
            }
            a(6);
            throw null;
        } else if (j0Var != null) {
            return j0Var;
        } else {
            a(7);
            throw null;
        }
    }

    public static w0 makeStarProjection(z0 z0Var) {
        if (z0Var != null) {
            return new o0(z0Var);
        }
        a(45);
        throw null;
    }

    public static j0 makeUnsubstitutedType(h hVar, i iVar, Function1<g, j0> function1) {
        if (!t.isError(hVar)) {
            return makeUnsubstitutedType(hVar.getTypeConstructor(), iVar, function1);
        }
        j0 createErrorType = t.createErrorType("Unsubstituted type for " + hVar);
        if (createErrorType != null) {
            return createErrorType;
        }
        a(11);
        throw null;
    }

    public static boolean noExpectedType(c0 c0Var) {
        if (c0Var != null) {
            return c0Var == c || c0Var == d;
        }
        a(0);
        throw null;
    }

    public static c0 makeNullableIfNeeded(c0 c0Var, boolean z2) {
        if (c0Var == null) {
            a(8);
            throw null;
        } else if (z2) {
            return makeNullable(c0Var);
        } else {
            if (c0Var != null) {
                return c0Var;
            }
            a(9);
            throw null;
        }
    }

    public static j0 makeUnsubstitutedType(u0 u0Var, i iVar, Function1<g, j0> function1) {
        if (u0Var == null) {
            a(12);
            throw null;
        } else if (iVar == null) {
            a(13);
            throw null;
        } else if (function1 != null) {
            j0 simpleTypeWithNonTrivialMemberScope = d0.simpleTypeWithNonTrivialMemberScope(d0.e0.p.d.m0.c.g1.g.f.getEMPTY(), u0Var, getDefaultTypeProjections(u0Var.getParameters()), false, iVar, function1);
            if (simpleTypeWithNonTrivialMemberScope != null) {
                return simpleTypeWithNonTrivialMemberScope;
            }
            a(15);
            throw null;
        } else {
            a(14);
            throw null;
        }
    }
}
