package d0.e0.p.d.m0.n.l1;

import d0.e0.p.d.m0.k.v.n;
import d0.e0.p.d.m0.n.a0;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.i1;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.m0;
import d0.e0.p.d.m0.n.y;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: IntersectionType.kt */
/* loaded from: classes3.dex */
public final class v {
    public static final v a = new v();

    /* JADX WARN: Failed to restore enum class, 'enum' modifier removed */
    /* compiled from: IntersectionType.kt */
    /* loaded from: classes3.dex */
    public static final class a extends Enum<a> {
        public static final a j;
        public static final a k;
        public static final a l;
        public static final a m;
        public static final /* synthetic */ a[] n;

        /* compiled from: IntersectionType.kt */
        /* renamed from: d0.e0.p.d.m0.n.l1.v$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0362a extends a {
            public C0362a(String str, int i) {
                super(str, i, null);
            }

            @Override // d0.e0.p.d.m0.n.l1.v.a
            public a combine(i1 i1Var) {
                m.checkNotNullParameter(i1Var, "nextType");
                return f(i1Var);
            }
        }

        /* compiled from: IntersectionType.kt */
        /* loaded from: classes3.dex */
        public static final class b extends a {
            public b(String str, int i) {
                super(str, i, null);
            }

            @Override // d0.e0.p.d.m0.n.l1.v.a
            public b combine(i1 i1Var) {
                m.checkNotNullParameter(i1Var, "nextType");
                return this;
            }
        }

        /* compiled from: IntersectionType.kt */
        /* loaded from: classes3.dex */
        public static final class c extends a {
            public c(String str, int i) {
                super(str, i, null);
            }

            @Override // d0.e0.p.d.m0.n.l1.v.a
            public a combine(i1 i1Var) {
                m.checkNotNullParameter(i1Var, "nextType");
                return f(i1Var);
            }
        }

        /* compiled from: IntersectionType.kt */
        /* loaded from: classes3.dex */
        public static final class d extends a {
            public d(String str, int i) {
                super(str, i, null);
            }

            @Override // d0.e0.p.d.m0.n.l1.v.a
            public a combine(i1 i1Var) {
                m.checkNotNullParameter(i1Var, "nextType");
                a f = f(i1Var);
                return f == a.k ? this : f;
            }
        }

        static {
            c cVar = new c("START", 0);
            j = cVar;
            C0362a aVar = new C0362a("ACCEPT_NULL", 1);
            k = aVar;
            d dVar = new d("UNKNOWN", 2);
            l = dVar;
            b bVar = new b("NOT_NULL", 3);
            m = bVar;
            n = new a[]{cVar, aVar, dVar, bVar};
        }

        public a(String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
        }

        public static a valueOf(String str) {
            m.checkNotNullParameter(str, "value");
            return (a) Enum.valueOf(a.class, str);
        }

        public static a[] values() {
            a[] aVarArr = n;
            a[] aVarArr2 = new a[aVarArr.length];
            System.arraycopy(aVarArr, 0, aVarArr2, 0, aVarArr.length);
            return aVarArr2;
        }

        public abstract a combine(i1 i1Var);

        public final a f(i1 i1Var) {
            m.checkNotNullParameter(i1Var, "<this>");
            return i1Var.isMarkedNullable() ? k : n.a.isSubtypeOfAny(i1Var) ? m : l;
        }
    }

    public static final boolean access$isStrictSupertype(v vVar, c0 c0Var, c0 c0Var2) {
        Objects.requireNonNull(vVar);
        m mVar = l.f3501b.getDefault();
        return mVar.isSubtypeOf(c0Var, c0Var2) && !mVar.isSubtypeOf(c0Var2, c0Var);
    }

    /* JADX WARN: Removed duplicated region for block: B:27:0x0050 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.util.Collection<d0.e0.p.d.m0.n.j0> a(java.util.Collection<? extends d0.e0.p.d.m0.n.j0> r8, kotlin.jvm.functions.Function2<? super d0.e0.p.d.m0.n.j0, ? super d0.e0.p.d.m0.n.j0, java.lang.Boolean> r9) {
        /*
            r7 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>(r8)
            java.util.Iterator r8 = r0.iterator()
            java.lang.String r1 = "filteredTypes.iterator()"
            d0.z.d.m.checkNotNullExpressionValue(r8, r1)
        Le:
            boolean r1 = r8.hasNext()
            if (r1 == 0) goto L57
            java.lang.Object r1 = r8.next()
            d0.e0.p.d.m0.n.j0 r1 = (d0.e0.p.d.m0.n.j0) r1
            boolean r2 = r0.isEmpty()
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L23
            goto L51
        L23:
            java.util.Iterator r2 = r0.iterator()
        L27:
            boolean r5 = r2.hasNext()
            if (r5 == 0) goto L51
            java.lang.Object r5 = r2.next()
            d0.e0.p.d.m0.n.j0 r5 = (d0.e0.p.d.m0.n.j0) r5
            if (r5 == r1) goto L4d
            java.lang.String r6 = "lower"
            d0.z.d.m.checkNotNullExpressionValue(r5, r6)
            java.lang.String r6 = "upper"
            d0.z.d.m.checkNotNullExpressionValue(r1, r6)
            java.lang.Object r5 = r9.invoke(r5, r1)
            java.lang.Boolean r5 = (java.lang.Boolean) r5
            boolean r5 = r5.booleanValue()
            if (r5 == 0) goto L4d
            r5 = 1
            goto L4e
        L4d:
            r5 = 0
        L4e:
            if (r5 == 0) goto L27
            r3 = 1
        L51:
            if (r3 == 0) goto Le
            r8.remove()
            goto Le
        L57:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.n.l1.v.a(java.util.Collection, kotlin.jvm.functions.Function2):java.util.Collection");
    }

    public final j0 intersectTypes$descriptors(List<? extends j0> list) {
        m.checkNotNullParameter(list, "types");
        list.size();
        ArrayList<i1> arrayList = new ArrayList();
        for (j0 j0Var : list) {
            if (j0Var.getConstructor() instanceof a0) {
                Collection<c0> supertypes = j0Var.getConstructor().getSupertypes();
                m.checkNotNullExpressionValue(supertypes, "type.constructor.supertypes");
                ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(supertypes, 10));
                for (c0 c0Var : supertypes) {
                    m.checkNotNullExpressionValue(c0Var, "it");
                    j0 upperIfFlexible = y.upperIfFlexible(c0Var);
                    if (j0Var.isMarkedNullable()) {
                        upperIfFlexible = upperIfFlexible.makeNullableAsSpecified(true);
                    }
                    arrayList2.add(upperIfFlexible);
                }
                arrayList.addAll(arrayList2);
            } else {
                arrayList.add(j0Var);
            }
        }
        a aVar = a.j;
        for (i1 i1Var : arrayList) {
            aVar = aVar.combine(i1Var);
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            j0 j0Var2 = (j0) it.next();
            if (aVar == a.m) {
                if (j0Var2 instanceof i) {
                    j0Var2 = m0.withNotNullProjection((i) j0Var2);
                }
                j0Var2 = m0.makeSimpleTypeDefinitelyNotNullOrNotNull$default(j0Var2, false, 1, null);
            }
            linkedHashSet.add(j0Var2);
        }
        if (linkedHashSet.size() == 1) {
            return (j0) u.single(linkedHashSet);
        }
        new w(linkedHashSet);
        Collection<j0> a2 = a(linkedHashSet, new x(this));
        ((ArrayList) a2).isEmpty();
        j0 findIntersectionType = n.a.findIntersectionType(a2);
        if (findIntersectionType != null) {
            return findIntersectionType;
        }
        Collection<j0> a3 = a(a2, new y(l.f3501b.getDefault()));
        ArrayList arrayList3 = (ArrayList) a3;
        arrayList3.isEmpty();
        return arrayList3.size() < 2 ? (j0) u.single(a3) : new a0(linkedHashSet).createType();
    }
}
