package d0.e0.p.d.m0.n.l1;

import d0.e0.p.d.m0.b.h;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.i1;
import d0.e0.p.d.m0.n.w0;
import d0.g;
import d0.i;
import d0.t.n;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: NewCapturedType.kt */
/* loaded from: classes3.dex */
public final class j implements d0.e0.p.d.m0.k.u.a.b {
    public final w0 a;

    /* renamed from: b  reason: collision with root package name */
    public Function0<? extends List<? extends i1>> f3500b;
    public final j c;
    public final z0 d;
    public final Lazy e;

    /* compiled from: NewCapturedType.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function0<List<? extends i1>> {
        public final /* synthetic */ List<i1> $supertypes;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        /* JADX WARN: Multi-variable type inference failed */
        public a(List<? extends i1> list) {
            super(0);
            this.$supertypes = list;
        }

        @Override // kotlin.jvm.functions.Function0
        public final List<? extends i1> invoke() {
            return this.$supertypes;
        }
    }

    /* compiled from: NewCapturedType.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function0<List<? extends i1>> {
        public b() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final List<? extends i1> invoke() {
            Function0 function0 = j.this.f3500b;
            if (function0 == null) {
                return null;
            }
            return (List) function0.invoke();
        }
    }

    /* compiled from: NewCapturedType.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function0<List<? extends i1>> {
        public final /* synthetic */ List<i1> $supertypes;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        /* JADX WARN: Multi-variable type inference failed */
        public c(List<? extends i1> list) {
            super(0);
            this.$supertypes = list;
        }

        @Override // kotlin.jvm.functions.Function0
        public final List<? extends i1> invoke() {
            return this.$supertypes;
        }
    }

    /* compiled from: NewCapturedType.kt */
    /* loaded from: classes3.dex */
    public static final class d extends o implements Function0<List<? extends i1>> {
        public final /* synthetic */ g $kotlinTypeRefiner;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public d(g gVar) {
            super(0);
            this.$kotlinTypeRefiner = gVar;
        }

        @Override // kotlin.jvm.functions.Function0
        public final List<? extends i1> invoke() {
            List<i1> supertypes = j.this.getSupertypes();
            g gVar = this.$kotlinTypeRefiner;
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(supertypes, 10));
            for (i1 i1Var : supertypes) {
                arrayList.add(i1Var.refine(gVar));
            }
            return arrayList;
        }
    }

    public j(w0 w0Var, Function0<? extends List<? extends i1>> function0, j jVar, z0 z0Var) {
        m.checkNotNullParameter(w0Var, "projection");
        this.a = w0Var;
        this.f3500b = function0;
        this.c = jVar;
        this.d = z0Var;
        this.e = g.lazy(i.PUBLICATION, new b());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!m.areEqual(j.class, obj == null ? null : obj.getClass())) {
            return false;
        }
        Objects.requireNonNull(obj, "null cannot be cast to non-null type org.jetbrains.kotlin.types.checker.NewCapturedTypeConstructor");
        j jVar = (j) obj;
        j jVar2 = this.c;
        if (jVar2 == null) {
            jVar2 = this;
        }
        j jVar3 = jVar.c;
        if (jVar3 != null) {
            jVar = jVar3;
        }
        return jVar2 == jVar;
    }

    @Override // d0.e0.p.d.m0.n.u0
    public h getBuiltIns() {
        c0 type = getProjection().getType();
        m.checkNotNullExpressionValue(type, "projection.type");
        return d0.e0.p.d.m0.n.o1.a.getBuiltIns(type);
    }

    @Override // d0.e0.p.d.m0.n.u0
    public d0.e0.p.d.m0.c.h getDeclarationDescriptor() {
        return null;
    }

    @Override // d0.e0.p.d.m0.n.u0
    public List<z0> getParameters() {
        return n.emptyList();
    }

    @Override // d0.e0.p.d.m0.k.u.a.b
    public w0 getProjection() {
        return this.a;
    }

    public int hashCode() {
        j jVar = this.c;
        return jVar == null ? super.hashCode() : jVar.hashCode();
    }

    public final void initializeSupertypes(List<? extends i1> list) {
        m.checkNotNullParameter(list, "supertypes");
        Function0<? extends List<? extends i1>> function0 = this.f3500b;
        this.f3500b = new c(list);
    }

    @Override // d0.e0.p.d.m0.n.u0
    public boolean isDenotable() {
        return false;
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("CapturedType(");
        R.append(getProjection());
        R.append(')');
        return R.toString();
    }

    @Override // d0.e0.p.d.m0.n.u0
    public List<i1> getSupertypes() {
        List<i1> list = (List) this.e.getValue();
        return list == null ? n.emptyList() : list;
    }

    @Override // d0.e0.p.d.m0.n.u0
    public j refine(g gVar) {
        m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
        w0 refine = getProjection().refine(gVar);
        m.checkNotNullExpressionValue(refine, "projection.refine(kotlinTypeRefiner)");
        d dVar = this.f3500b == null ? null : new d(gVar);
        j jVar = this.c;
        if (jVar == null) {
            jVar = this;
        }
        return new j(refine, dVar, jVar, this.d);
    }

    public /* synthetic */ j(w0 w0Var, Function0 function0, j jVar, z0 z0Var, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(w0Var, (i & 2) != 0 ? null : function0, (i & 4) != 0 ? null : jVar, (i & 8) != 0 ? null : z0Var);
    }

    public /* synthetic */ j(w0 w0Var, List list, j jVar, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(w0Var, list, (i & 4) != 0 ? null : jVar);
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public j(w0 w0Var, List<? extends i1> list, j jVar) {
        this(w0Var, new a(list), jVar, null, 8, null);
        m.checkNotNullParameter(w0Var, "projection");
        m.checkNotNullParameter(list, "supertypes");
    }
}
