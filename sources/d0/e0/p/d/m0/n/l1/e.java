package d0.e0.p.d.m0.n.l1;

import d0.e0.p.d.m0.n.d0;
import d0.e0.p.d.m0.n.e0;
import d0.e0.p.d.m0.n.i1;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.r;
import d0.e0.p.d.m0.n.t;
import d0.e0.p.d.m0.n.v;
import d0.e0.p.d.m0.n.y;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;
/* compiled from: IntersectionType.kt */
/* loaded from: classes3.dex */
public final class e {
    public static final i1 intersectTypes(List<? extends i1> list) {
        j0 j0Var;
        m.checkNotNullParameter(list, "types");
        int size = list.size();
        if (size == 0) {
            throw new IllegalStateException("Expected some types".toString());
        } else if (size == 1) {
            return (i1) u.single((List<? extends Object>) list);
        } else {
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
            boolean z2 = false;
            boolean z3 = false;
            for (i1 i1Var : list) {
                z2 = z2 || e0.isError(i1Var);
                if (i1Var instanceof j0) {
                    j0Var = (j0) i1Var;
                } else if (!(i1Var instanceof v)) {
                    throw new NoWhenBranchMatchedException();
                } else if (r.isDynamic(i1Var)) {
                    return i1Var;
                } else {
                    j0Var = ((v) i1Var).getLowerBound();
                    z3 = true;
                }
                arrayList.add(j0Var);
            }
            if (z2) {
                j0 createErrorType = t.createErrorType(m.stringPlus("Intersection of error types: ", list));
                m.checkNotNullExpressionValue(createErrorType, "createErrorType(\"Intersection of error types: $types\")");
                return createErrorType;
            } else if (!z3) {
                return v.a.intersectTypes$descriptors(arrayList);
            } else {
                ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(list, 10));
                for (i1 i1Var2 : list) {
                    arrayList2.add(y.upperIfFlexible(i1Var2));
                }
                d0 d0Var = d0.a;
                v vVar = v.a;
                return d0.flexibleType(vVar.intersectTypes$descriptors(arrayList), vVar.intersectTypes$descriptors(arrayList2));
            }
        }
    }
}
