package d0.e0.p.d.m0.n.l1;

import d0.e0.p.d.m0.n.c0;
import d0.z.d.m;
/* compiled from: utils.kt */
/* loaded from: classes3.dex */
public final class r {
    public final c0 a;

    /* renamed from: b  reason: collision with root package name */
    public final r f3503b;

    public r(c0 c0Var, r rVar) {
        m.checkNotNullParameter(c0Var, "type");
        this.a = c0Var;
        this.f3503b = rVar;
    }

    public final r getPrevious() {
        return this.f3503b;
    }

    public final c0 getType() {
        return this.a;
    }
}
