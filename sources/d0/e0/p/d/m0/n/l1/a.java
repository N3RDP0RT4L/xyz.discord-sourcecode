package d0.e0.p.d.m0.n.l1;

import d0.e0.p.d.m0.k.v.n;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.c1;
import d0.e0.p.d.m0.n.f;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.j1;
import d0.e0.p.d.m0.n.l1.c;
import d0.e0.p.d.m0.n.l1.g;
import d0.e0.p.d.m0.n.n1.b;
import d0.e0.p.d.m0.n.n1.d;
import d0.e0.p.d.m0.n.n1.e;
import d0.e0.p.d.m0.n.n1.h;
import d0.e0.p.d.m0.n.n1.i;
import d0.e0.p.d.m0.n.n1.j;
import d0.e0.p.d.m0.n.n1.k;
import d0.e0.p.d.m0.n.n1.l;
import d0.e0.p.d.m0.n.n1.r;
import d0.e0.p.d.m0.n.u0;
import d0.e0.p.d.m0.n.v0;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ClassicTypeCheckerContext.kt */
/* loaded from: classes3.dex */
public class a extends f implements c {
    public static final C0360a e = new C0360a(null);
    public final boolean f;
    public final boolean g;
    public final boolean h;
    public final g i;

    /* compiled from: ClassicTypeCheckerContext.kt */
    /* renamed from: d0.e0.p.d.m0.n.l1.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0360a {

        /* compiled from: ClassicTypeCheckerContext.kt */
        /* renamed from: d0.e0.p.d.m0.n.l1.a$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0361a extends f.b.a {
            public final /* synthetic */ c a;

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ c1 f3499b;

            public C0361a(c cVar, c1 c1Var) {
                this.a = cVar;
                this.f3499b = c1Var;
            }

            @Override // d0.e0.p.d.m0.n.f.b
            public i transformType(f fVar, h hVar) {
                m.checkNotNullParameter(fVar, "context");
                m.checkNotNullParameter(hVar, "type");
                c cVar = this.a;
                c0 safeSubstitute = this.f3499b.safeSubstitute((c0) cVar.lowerBoundIfFlexible(hVar), j1.INVARIANT);
                m.checkNotNullExpressionValue(safeSubstitute, "substitutor.safeSubstitute(\n                        type.lowerBoundIfFlexible() as KotlinType,\n                        Variance.INVARIANT\n                    )");
                i asSimpleType = cVar.asSimpleType(safeSubstitute);
                m.checkNotNull(asSimpleType);
                return asSimpleType;
            }
        }

        public C0360a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final f.b.a classicSubstitutionSupertypePolicy(c cVar, i iVar) {
            m.checkNotNullParameter(cVar, "<this>");
            m.checkNotNullParameter(iVar, "type");
            if (iVar instanceof j0) {
                return new C0361a(cVar, v0.f3514b.create((c0) iVar).buildSubstitutor());
            }
            throw new IllegalArgumentException(b.access$errorMessage(iVar).toString());
        }
    }

    public /* synthetic */ a(boolean z2, boolean z3, boolean z4, g gVar, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(z2, (i & 2) != 0 ? true : z3, (i & 4) != 0 ? true : z4, (i & 8) != 0 ? g.a.a : gVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public boolean areEqualTypeConstructors(l lVar, l lVar2) {
        m.checkNotNullParameter(lVar, "c1");
        m.checkNotNullParameter(lVar2, "c2");
        if (!(lVar instanceof u0)) {
            throw new IllegalArgumentException(b.access$errorMessage(lVar).toString());
        } else if (lVar2 instanceof u0) {
            return areEqualTypeConstructors((u0) lVar, (u0) lVar2);
        } else {
            throw new IllegalArgumentException(b.access$errorMessage(lVar2).toString());
        }
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public int argumentsCount(h hVar) {
        return c.a.argumentsCount(this, hVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public j asArgumentList(i iVar) {
        return c.a.asArgumentList(this, iVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public d0.e0.p.d.m0.n.n1.c asCapturedType(i iVar) {
        return c.a.asCapturedType(this, iVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public d asDefinitelyNotNullType(i iVar) {
        return c.a.asDefinitelyNotNullType(this, iVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public e asDynamicType(d0.e0.p.d.m0.n.n1.f fVar) {
        return c.a.asDynamicType(this, fVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public d0.e0.p.d.m0.n.n1.f asFlexibleType(h hVar) {
        return c.a.asFlexibleType(this, hVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n, d0.e0.p.d.m0.n.l1.c
    public i asSimpleType(h hVar) {
        return c.a.asSimpleType(this, hVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public k asTypeArgument(h hVar) {
        return c.a.asTypeArgument(this, hVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public i captureFromArguments(i iVar, b bVar) {
        return c.a.captureFromArguments(this, iVar, bVar);
    }

    @Override // d0.e0.p.d.m0.n.l1.c
    public h createFlexibleType(i iVar, i iVar2) {
        return c.a.createFlexibleType(this, iVar, iVar2);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public k getArgument(h hVar, int i) {
        return c.a.getArgument(this, hVar, i);
    }

    @Override // d0.e0.p.d.m0.n.d1
    public d0.e0.p.d.m0.g.c getClassFqNameUnsafe(l lVar) {
        return c.a.getClassFqNameUnsafe(this, lVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public d0.e0.p.d.m0.n.n1.m getParameter(l lVar, int i) {
        return c.a.getParameter(this, lVar, i);
    }

    @Override // d0.e0.p.d.m0.n.d1
    public d0.e0.p.d.m0.b.i getPrimitiveArrayType(l lVar) {
        return c.a.getPrimitiveArrayType(this, lVar);
    }

    @Override // d0.e0.p.d.m0.n.d1
    public d0.e0.p.d.m0.b.i getPrimitiveType(l lVar) {
        return c.a.getPrimitiveType(this, lVar);
    }

    @Override // d0.e0.p.d.m0.n.d1
    public h getRepresentativeUpperBound(d0.e0.p.d.m0.n.n1.m mVar) {
        return c.a.getRepresentativeUpperBound(this, mVar);
    }

    @Override // d0.e0.p.d.m0.n.d1
    public h getSubstitutedUnderlyingType(h hVar) {
        return c.a.getSubstitutedUnderlyingType(this, hVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public h getType(k kVar) {
        return c.a.getType(this, kVar);
    }

    @Override // d0.e0.p.d.m0.n.d1
    public d0.e0.p.d.m0.n.n1.m getTypeParameterClassifier(l lVar) {
        return c.a.getTypeParameterClassifier(this, lVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public r getVariance(k kVar) {
        return c.a.getVariance(this, kVar);
    }

    @Override // d0.e0.p.d.m0.n.d1
    public boolean hasAnnotation(h hVar, d0.e0.p.d.m0.g.b bVar) {
        return c.a.hasAnnotation(this, hVar, bVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.q
    public boolean identicalArguments(i iVar, i iVar2) {
        return c.a.identicalArguments(this, iVar, iVar2);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public h intersectTypes(List<? extends h> list) {
        return c.a.intersectTypes(this, list);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public boolean isAnyConstructor(l lVar) {
        return c.a.isAnyConstructor(this, lVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public boolean isClassTypeConstructor(l lVar) {
        return c.a.isClassTypeConstructor(this, lVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public boolean isCommonFinalClassConstructor(l lVar) {
        return c.a.isCommonFinalClassConstructor(this, lVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public boolean isDenotable(l lVar) {
        return c.a.isDenotable(this, lVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public boolean isError(h hVar) {
        return c.a.isError(this, hVar);
    }

    @Override // d0.e0.p.d.m0.n.f
    public boolean isErrorTypeEqualsToAnything() {
        return this.f;
    }

    @Override // d0.e0.p.d.m0.n.d1
    public boolean isInlineClass(l lVar) {
        return c.a.isInlineClass(this, lVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public boolean isIntegerLiteralTypeConstructor(l lVar) {
        return c.a.isIntegerLiteralTypeConstructor(this, lVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public boolean isIntersection(l lVar) {
        return c.a.isIntersection(this, lVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public boolean isMarkedNullable(i iVar) {
        return c.a.isMarkedNullable((c) this, iVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public boolean isNothingConstructor(l lVar) {
        return c.a.isNothingConstructor(this, lVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public boolean isNullableType(h hVar) {
        return c.a.isNullableType(this, hVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public boolean isPrimitiveType(i iVar) {
        return c.a.isPrimitiveType(this, iVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public boolean isProjectionNotNull(d0.e0.p.d.m0.n.n1.c cVar) {
        return c.a.isProjectionNotNull(this, cVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public boolean isStarProjection(k kVar) {
        return c.a.isStarProjection(this, kVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public boolean isStubType(i iVar) {
        return c.a.isStubType(this, iVar);
    }

    @Override // d0.e0.p.d.m0.n.f
    public boolean isStubTypeEqualsToAnything() {
        return this.g;
    }

    @Override // d0.e0.p.d.m0.n.d1
    public boolean isUnderKotlinPackage(l lVar) {
        return c.a.isUnderKotlinPackage(this, lVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n, d0.e0.p.d.m0.n.l1.c
    public i lowerBound(d0.e0.p.d.m0.n.n1.f fVar) {
        return c.a.lowerBound(this, fVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public h lowerType(d0.e0.p.d.m0.n.n1.c cVar) {
        return c.a.lowerType(this, cVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public h makeDefinitelyNotNullOrNotNull(h hVar) {
        return c.a.makeDefinitelyNotNullOrNotNull(this, hVar);
    }

    @Override // d0.e0.p.d.m0.n.d1
    public h makeNullable(h hVar) {
        return c.a.makeNullable(this, hVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public i original(d dVar) {
        return c.a.original(this, dVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public int parametersCount(l lVar) {
        return c.a.parametersCount(this, lVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public Collection<h> possibleIntegerTypes(i iVar) {
        return c.a.possibleIntegerTypes(this, iVar);
    }

    @Override // d0.e0.p.d.m0.n.f
    public h prepareType(h hVar) {
        m.checkNotNullParameter(hVar, "type");
        if (hVar instanceof c0) {
            return l.f3501b.getDefault().transformToNewType(((c0) hVar).unwrap());
        }
        throw new IllegalArgumentException(b.access$errorMessage(hVar).toString());
    }

    @Override // d0.e0.p.d.m0.n.f
    public h refineType(h hVar) {
        m.checkNotNullParameter(hVar, "type");
        if (hVar instanceof c0) {
            return this.i.refineType((c0) hVar);
        }
        throw new IllegalArgumentException(b.access$errorMessage(hVar).toString());
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public Collection<h> supertypes(l lVar) {
        return c.a.supertypes(this, lVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n, d0.e0.p.d.m0.n.l1.c
    public l typeConstructor(i iVar) {
        return c.a.typeConstructor((c) this, iVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n, d0.e0.p.d.m0.n.l1.c
    public i upperBound(d0.e0.p.d.m0.n.n1.f fVar) {
        return c.a.upperBound(this, fVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public h withNullability(h hVar, boolean z2) {
        return c.a.withNullability(this, hVar, z2);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public r getVariance(d0.e0.p.d.m0.n.n1.m mVar) {
        return c.a.getVariance(this, mVar);
    }

    @Override // d0.e0.p.d.m0.n.f
    public f.b.a substitutionSupertypePolicy(i iVar) {
        m.checkNotNullParameter(iVar, "type");
        return e.classicSubstitutionSupertypePolicy(this, iVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n, d0.e0.p.d.m0.n.l1.c
    public i withNullability(i iVar, boolean z2) {
        return c.a.withNullability((c) this, iVar, z2);
    }

    public a(boolean z2, boolean z3, boolean z4, g gVar) {
        m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
        this.f = z2;
        this.g = z3;
        this.h = z4;
        this.i = gVar;
    }

    public boolean areEqualTypeConstructors(u0 u0Var, u0 u0Var2) {
        m.checkNotNullParameter(u0Var, "a");
        m.checkNotNullParameter(u0Var2, "b");
        return u0Var instanceof n ? ((n) u0Var).checkConstructor(u0Var2) : u0Var2 instanceof n ? ((n) u0Var2).checkConstructor(u0Var) : m.areEqual(u0Var, u0Var2);
    }
}
