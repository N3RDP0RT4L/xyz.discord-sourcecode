package d0.e0.p.d.m0.n.l1;

import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.n.l1.c;
import d0.e0.p.d.m0.n.n1.d;
import d0.e0.p.d.m0.n.n1.e;
import d0.e0.p.d.m0.n.n1.f;
import d0.e0.p.d.m0.n.n1.h;
import d0.e0.p.d.m0.n.n1.i;
import d0.e0.p.d.m0.n.n1.k;
import d0.e0.p.d.m0.n.n1.l;
import d0.e0.p.d.m0.n.n1.m;
import d0.e0.p.d.m0.n.n1.r;
/* compiled from: NewKotlinTypeChecker.kt */
/* loaded from: classes3.dex */
public final class p implements c {
    public static final p a = new p();

    @Override // d0.e0.p.d.m0.n.n1.n
    public boolean areEqualTypeConstructors(l lVar, l lVar2) {
        return c.a.areEqualTypeConstructors(this, lVar, lVar2);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public int argumentsCount(h hVar) {
        return c.a.argumentsCount(this, hVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public d asDefinitelyNotNullType(i iVar) {
        return c.a.asDefinitelyNotNullType(this, iVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public e asDynamicType(f fVar) {
        return c.a.asDynamicType(this, fVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public f asFlexibleType(h hVar) {
        return c.a.asFlexibleType(this, hVar);
    }

    @Override // d0.e0.p.d.m0.n.l1.c
    public i asSimpleType(h hVar) {
        return c.a.asSimpleType(this, hVar);
    }

    @Override // d0.e0.p.d.m0.n.l1.c
    public h createFlexibleType(i iVar, i iVar2) {
        return c.a.createFlexibleType(this, iVar, iVar2);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public k getArgument(h hVar, int i) {
        return c.a.getArgument(this, hVar, i);
    }

    @Override // d0.e0.p.d.m0.n.d1
    public d0.e0.p.d.m0.g.c getClassFqNameUnsafe(l lVar) {
        return c.a.getClassFqNameUnsafe(this, lVar);
    }

    @Override // d0.e0.p.d.m0.n.d1
    public d0.e0.p.d.m0.b.i getPrimitiveArrayType(l lVar) {
        return c.a.getPrimitiveArrayType(this, lVar);
    }

    @Override // d0.e0.p.d.m0.n.d1
    public d0.e0.p.d.m0.b.i getPrimitiveType(l lVar) {
        return c.a.getPrimitiveType(this, lVar);
    }

    @Override // d0.e0.p.d.m0.n.d1
    public h getRepresentativeUpperBound(m mVar) {
        return c.a.getRepresentativeUpperBound(this, mVar);
    }

    @Override // d0.e0.p.d.m0.n.d1
    public h getSubstitutedUnderlyingType(h hVar) {
        return c.a.getSubstitutedUnderlyingType(this, hVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public h getType(k kVar) {
        return c.a.getType(this, kVar);
    }

    @Override // d0.e0.p.d.m0.n.d1
    public m getTypeParameterClassifier(l lVar) {
        return c.a.getTypeParameterClassifier(this, lVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public r getVariance(k kVar) {
        return c.a.getVariance(this, kVar);
    }

    @Override // d0.e0.p.d.m0.n.d1
    public boolean hasAnnotation(h hVar, b bVar) {
        return c.a.hasAnnotation(this, hVar, bVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.q
    public boolean identicalArguments(i iVar, i iVar2) {
        return c.a.identicalArguments(this, iVar, iVar2);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public boolean isClassTypeConstructor(l lVar) {
        return c.a.isClassTypeConstructor(this, lVar);
    }

    @Override // d0.e0.p.d.m0.n.d1
    public boolean isInlineClass(l lVar) {
        return c.a.isInlineClass(this, lVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public boolean isIntegerLiteralTypeConstructor(l lVar) {
        return c.a.isIntegerLiteralTypeConstructor(this, lVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public boolean isMarkedNullable(h hVar) {
        return c.a.isMarkedNullable(this, hVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public boolean isNothingConstructor(l lVar) {
        return c.a.isNothingConstructor(this, lVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public boolean isNullableType(h hVar) {
        return c.a.isNullableType(this, hVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public boolean isPrimitiveType(i iVar) {
        return c.a.isPrimitiveType(this, iVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public boolean isStarProjection(k kVar) {
        return c.a.isStarProjection(this, kVar);
    }

    @Override // d0.e0.p.d.m0.n.d1
    public boolean isUnderKotlinPackage(l lVar) {
        return c.a.isUnderKotlinPackage(this, lVar);
    }

    @Override // d0.e0.p.d.m0.n.l1.c
    public i lowerBound(f fVar) {
        return c.a.lowerBound(this, fVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public i lowerBoundIfFlexible(h hVar) {
        return c.a.lowerBoundIfFlexible(this, hVar);
    }

    @Override // d0.e0.p.d.m0.n.d1
    public h makeNullable(h hVar) {
        return c.a.makeNullable(this, hVar);
    }

    public d0.e0.p.d.m0.n.f newBaseTypeCheckerContext(boolean z2, boolean z3) {
        return c.a.newBaseTypeCheckerContext(this, z2, z3);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public l typeConstructor(h hVar) {
        return c.a.typeConstructor(this, hVar);
    }

    @Override // d0.e0.p.d.m0.n.l1.c
    public i upperBound(f fVar) {
        return c.a.upperBound(this, fVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public i upperBoundIfFlexible(h hVar) {
        return c.a.upperBoundIfFlexible(this, hVar);
    }

    @Override // d0.e0.p.d.m0.n.l1.c
    public i withNullability(i iVar, boolean z2) {
        return c.a.withNullability((c) this, iVar, z2);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public boolean isMarkedNullable(i iVar) {
        return c.a.isMarkedNullable((c) this, iVar);
    }

    @Override // d0.e0.p.d.m0.n.l1.c
    public l typeConstructor(i iVar) {
        return c.a.typeConstructor((c) this, iVar);
    }
}
