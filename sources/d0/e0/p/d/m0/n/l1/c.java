package d0.e0.p.d.m0.n.l1;

import d0.e0.p.d.m0.b.k;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.k.g;
import d0.e0.p.d.m0.k.v.n;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.d0;
import d0.e0.p.d.m0.n.d1;
import d0.e0.p.d.m0.n.e0;
import d0.e0.p.d.m0.n.e1;
import d0.e0.p.d.m0.n.i1;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.j1;
import d0.e0.p.d.m0.n.k;
import d0.e0.p.d.m0.n.m0;
import d0.e0.p.d.m0.n.n1.b;
import d0.e0.p.d.m0.n.n1.d;
import d0.e0.p.d.m0.n.n1.e;
import d0.e0.p.d.m0.n.n1.f;
import d0.e0.p.d.m0.n.n1.h;
import d0.e0.p.d.m0.n.n1.i;
import d0.e0.p.d.m0.n.n1.j;
import d0.e0.p.d.m0.n.n1.l;
import d0.e0.p.d.m0.n.n1.o;
import d0.e0.p.d.m0.n.n1.p;
import d0.e0.p.d.m0.n.n1.r;
import d0.e0.p.d.m0.n.q;
import d0.e0.p.d.m0.n.u0;
import d0.e0.p.d.m0.n.v;
import d0.e0.p.d.m0.n.w0;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
/* compiled from: ClassicTypeSystemContext.kt */
/* loaded from: classes3.dex */
public interface c extends d1, p {

    /* compiled from: ClassicTypeSystemContext.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public static boolean areEqualTypeConstructors(c cVar, l lVar, l lVar2) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(lVar, "c1");
            m.checkNotNullParameter(lVar2, "c2");
            if (!(lVar instanceof u0)) {
                throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + lVar + ", " + a0.getOrCreateKotlinClass(lVar.getClass())).toString());
            } else if (lVar2 instanceof u0) {
                return m.areEqual(lVar, lVar2);
            } else {
                throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + lVar2 + ", " + a0.getOrCreateKotlinClass(lVar2.getClass())).toString());
            }
        }

        public static int argumentsCount(c cVar, h hVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(hVar, "receiver");
            if (hVar instanceof c0) {
                return ((c0) hVar).getArguments().size();
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + hVar + ", " + a0.getOrCreateKotlinClass(hVar.getClass())).toString());
        }

        public static j asArgumentList(c cVar, i iVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(iVar, "receiver");
            if (iVar instanceof j0) {
                return (j) iVar;
            }
            StringBuilder X = b.d.b.a.a.X("ClassicTypeSystemContext couldn't handle: ", iVar, ", ");
            X.append(a0.getOrCreateKotlinClass(iVar.getClass()));
            throw new IllegalArgumentException(X.toString().toString());
        }

        public static d0.e0.p.d.m0.n.n1.c asCapturedType(c cVar, i iVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(iVar, "receiver");
            if (!(iVar instanceof j0)) {
                StringBuilder X = b.d.b.a.a.X("ClassicTypeSystemContext couldn't handle: ", iVar, ", ");
                X.append(a0.getOrCreateKotlinClass(iVar.getClass()));
                throw new IllegalArgumentException(X.toString().toString());
            } else if (iVar instanceof i) {
                return (i) iVar;
            } else {
                return null;
            }
        }

        public static d asDefinitelyNotNullType(c cVar, i iVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(iVar, "receiver");
            if (!(iVar instanceof j0)) {
                StringBuilder X = b.d.b.a.a.X("ClassicTypeSystemContext couldn't handle: ", iVar, ", ");
                X.append(a0.getOrCreateKotlinClass(iVar.getClass()));
                throw new IllegalArgumentException(X.toString().toString());
            } else if (iVar instanceof k) {
                return (k) iVar;
            } else {
                return null;
            }
        }

        public static e asDynamicType(c cVar, f fVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(fVar, "receiver");
            if (!(fVar instanceof v)) {
                throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + fVar + ", " + a0.getOrCreateKotlinClass(fVar.getClass())).toString());
            } else if (fVar instanceof q) {
                return (q) fVar;
            } else {
                return null;
            }
        }

        public static f asFlexibleType(c cVar, h hVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(hVar, "receiver");
            if (hVar instanceof c0) {
                i1 unwrap = ((c0) hVar).unwrap();
                if (unwrap instanceof v) {
                    return (v) unwrap;
                }
                return null;
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + hVar + ", " + a0.getOrCreateKotlinClass(hVar.getClass())).toString());
        }

        public static i asSimpleType(c cVar, h hVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(hVar, "receiver");
            if (hVar instanceof c0) {
                i1 unwrap = ((c0) hVar).unwrap();
                if (unwrap instanceof j0) {
                    return (j0) unwrap;
                }
                return null;
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + hVar + ", " + a0.getOrCreateKotlinClass(hVar.getClass())).toString());
        }

        public static d0.e0.p.d.m0.n.n1.k asTypeArgument(c cVar, h hVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(hVar, "receiver");
            if (hVar instanceof c0) {
                return d0.e0.p.d.m0.n.o1.a.asTypeProjection((c0) hVar);
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + hVar + ", " + a0.getOrCreateKotlinClass(hVar.getClass())).toString());
        }

        public static i captureFromArguments(c cVar, i iVar, b bVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(iVar, "type");
            m.checkNotNullParameter(bVar, "status");
            if (iVar instanceof j0) {
                return k.captureFromArguments((j0) iVar, bVar);
            }
            StringBuilder X = b.d.b.a.a.X("ClassicTypeSystemContext couldn't handle: ", iVar, ", ");
            X.append(a0.getOrCreateKotlinClass(iVar.getClass()));
            throw new IllegalArgumentException(X.toString().toString());
        }

        public static h createFlexibleType(c cVar, i iVar, i iVar2) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(iVar, "lowerBound");
            m.checkNotNullParameter(iVar2, "upperBound");
            if (!(iVar instanceof j0)) {
                throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + cVar + ", " + a0.getOrCreateKotlinClass(cVar.getClass())).toString());
            } else if (iVar2 instanceof j0) {
                d0 d0Var = d0.a;
                return d0.flexibleType((j0) iVar, (j0) iVar2);
            } else {
                throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + cVar + ", " + a0.getOrCreateKotlinClass(cVar.getClass())).toString());
            }
        }

        public static d0.e0.p.d.m0.n.n1.k getArgument(c cVar, h hVar, int i) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(hVar, "receiver");
            if (hVar instanceof c0) {
                return ((c0) hVar).getArguments().get(i);
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + hVar + ", " + a0.getOrCreateKotlinClass(hVar.getClass())).toString());
        }

        public static d0.e0.p.d.m0.g.c getClassFqNameUnsafe(c cVar, l lVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(lVar, "receiver");
            if (lVar instanceof u0) {
                d0.e0.p.d.m0.c.h declarationDescriptor = ((u0) lVar).getDeclarationDescriptor();
                Objects.requireNonNull(declarationDescriptor, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.ClassDescriptor");
                return d0.e0.p.d.m0.k.x.a.getFqNameUnsafe((d0.e0.p.d.m0.c.e) declarationDescriptor);
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + lVar + ", " + a0.getOrCreateKotlinClass(lVar.getClass())).toString());
        }

        public static d0.e0.p.d.m0.n.n1.m getParameter(c cVar, l lVar, int i) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(lVar, "receiver");
            if (lVar instanceof u0) {
                z0 z0Var = ((u0) lVar).getParameters().get(i);
                m.checkNotNullExpressionValue(z0Var, "this.parameters[index]");
                return z0Var;
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + lVar + ", " + a0.getOrCreateKotlinClass(lVar.getClass())).toString());
        }

        public static d0.e0.p.d.m0.b.i getPrimitiveArrayType(c cVar, l lVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(lVar, "receiver");
            if (lVar instanceof u0) {
                d0.e0.p.d.m0.c.h declarationDescriptor = ((u0) lVar).getDeclarationDescriptor();
                Objects.requireNonNull(declarationDescriptor, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.ClassDescriptor");
                return d0.e0.p.d.m0.b.h.getPrimitiveArrayType((d0.e0.p.d.m0.c.e) declarationDescriptor);
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + lVar + ", " + a0.getOrCreateKotlinClass(lVar.getClass())).toString());
        }

        public static d0.e0.p.d.m0.b.i getPrimitiveType(c cVar, l lVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(lVar, "receiver");
            if (lVar instanceof u0) {
                d0.e0.p.d.m0.c.h declarationDescriptor = ((u0) lVar).getDeclarationDescriptor();
                Objects.requireNonNull(declarationDescriptor, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.ClassDescriptor");
                return d0.e0.p.d.m0.b.h.getPrimitiveType((d0.e0.p.d.m0.c.e) declarationDescriptor);
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + lVar + ", " + a0.getOrCreateKotlinClass(lVar.getClass())).toString());
        }

        public static h getRepresentativeUpperBound(c cVar, d0.e0.p.d.m0.n.n1.m mVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(mVar, "receiver");
            if (mVar instanceof z0) {
                return d0.e0.p.d.m0.n.o1.a.getRepresentativeUpperBound((z0) mVar);
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + mVar + ", " + a0.getOrCreateKotlinClass(mVar.getClass())).toString());
        }

        public static h getSubstitutedUnderlyingType(c cVar, h hVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(hVar, "receiver");
            if (hVar instanceof c0) {
                return g.substitutedUnderlyingType((c0) hVar);
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + hVar + ", " + a0.getOrCreateKotlinClass(hVar.getClass())).toString());
        }

        public static h getType(c cVar, d0.e0.p.d.m0.n.n1.k kVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(kVar, "receiver");
            if (kVar instanceof w0) {
                return ((w0) kVar).getType().unwrap();
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + kVar + ", " + a0.getOrCreateKotlinClass(kVar.getClass())).toString());
        }

        public static d0.e0.p.d.m0.n.n1.m getTypeParameterClassifier(c cVar, l lVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(lVar, "receiver");
            if (lVar instanceof u0) {
                d0.e0.p.d.m0.c.h declarationDescriptor = ((u0) lVar).getDeclarationDescriptor();
                if (declarationDescriptor instanceof z0) {
                    return (z0) declarationDescriptor;
                }
                return null;
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + lVar + ", " + a0.getOrCreateKotlinClass(lVar.getClass())).toString());
        }

        public static r getVariance(c cVar, d0.e0.p.d.m0.n.n1.k kVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(kVar, "receiver");
            if (kVar instanceof w0) {
                j1 projectionKind = ((w0) kVar).getProjectionKind();
                m.checkNotNullExpressionValue(projectionKind, "this.projectionKind");
                return o.convertVariance(projectionKind);
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + kVar + ", " + a0.getOrCreateKotlinClass(kVar.getClass())).toString());
        }

        public static boolean hasAnnotation(c cVar, h hVar, d0.e0.p.d.m0.g.b bVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(hVar, "receiver");
            m.checkNotNullParameter(bVar, "fqName");
            if (hVar instanceof c0) {
                return ((c0) hVar).getAnnotations().hasAnnotation(bVar);
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + hVar + ", " + a0.getOrCreateKotlinClass(hVar.getClass())).toString());
        }

        public static boolean identicalArguments(c cVar, i iVar, i iVar2) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(iVar, "a");
            m.checkNotNullParameter(iVar2, "b");
            if (!(iVar instanceof j0)) {
                StringBuilder X = b.d.b.a.a.X("ClassicTypeSystemContext couldn't handle: ", iVar, ", ");
                X.append(a0.getOrCreateKotlinClass(iVar.getClass()));
                throw new IllegalArgumentException(X.toString().toString());
            } else if (iVar2 instanceof j0) {
                return ((j0) iVar).getArguments() == ((j0) iVar2).getArguments();
            } else {
                StringBuilder X2 = b.d.b.a.a.X("ClassicTypeSystemContext couldn't handle: ", iVar2, ", ");
                X2.append(a0.getOrCreateKotlinClass(iVar2.getClass()));
                throw new IllegalArgumentException(X2.toString().toString());
            }
        }

        public static h intersectTypes(c cVar, List<? extends h> list) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(list, "types");
            return e.intersectTypes(list);
        }

        public static boolean isAnyConstructor(c cVar, l lVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(lVar, "receiver");
            if (lVar instanceof u0) {
                return d0.e0.p.d.m0.b.h.isTypeConstructorForGivenClass((u0) lVar, k.a.f3189b);
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + lVar + ", " + a0.getOrCreateKotlinClass(lVar.getClass())).toString());
        }

        public static boolean isClassTypeConstructor(c cVar, l lVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(lVar, "receiver");
            if (lVar instanceof u0) {
                return ((u0) lVar).getDeclarationDescriptor() instanceof d0.e0.p.d.m0.c.e;
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + lVar + ", " + a0.getOrCreateKotlinClass(lVar.getClass())).toString());
        }

        public static boolean isCommonFinalClassConstructor(c cVar, l lVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(lVar, "receiver");
            if (lVar instanceof u0) {
                d0.e0.p.d.m0.c.h declarationDescriptor = ((u0) lVar).getDeclarationDescriptor();
                d0.e0.p.d.m0.c.e eVar = declarationDescriptor instanceof d0.e0.p.d.m0.c.e ? (d0.e0.p.d.m0.c.e) declarationDescriptor : null;
                return (eVar == null || !d0.e0.p.d.m0.c.a0.isFinalClass(eVar) || eVar.getKind() == d0.e0.p.d.m0.c.f.ENUM_ENTRY || eVar.getKind() == d0.e0.p.d.m0.c.f.ANNOTATION_CLASS) ? false : true;
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + lVar + ", " + a0.getOrCreateKotlinClass(lVar.getClass())).toString());
        }

        public static boolean isDenotable(c cVar, l lVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(lVar, "receiver");
            if (lVar instanceof u0) {
                return ((u0) lVar).isDenotable();
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + lVar + ", " + a0.getOrCreateKotlinClass(lVar.getClass())).toString());
        }

        public static boolean isError(c cVar, h hVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(hVar, "receiver");
            if (hVar instanceof c0) {
                return e0.isError((c0) hVar);
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + hVar + ", " + a0.getOrCreateKotlinClass(hVar.getClass())).toString());
        }

        public static boolean isInlineClass(c cVar, l lVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(lVar, "receiver");
            if (lVar instanceof u0) {
                d0.e0.p.d.m0.c.h declarationDescriptor = ((u0) lVar).getDeclarationDescriptor();
                Boolean bool = null;
                d0.e0.p.d.m0.c.e eVar = declarationDescriptor instanceof d0.e0.p.d.m0.c.e ? (d0.e0.p.d.m0.c.e) declarationDescriptor : null;
                if (eVar != null) {
                    bool = Boolean.valueOf(g.isInlineClass(eVar));
                }
                return m.areEqual(bool, Boolean.TRUE);
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + lVar + ", " + a0.getOrCreateKotlinClass(lVar.getClass())).toString());
        }

        public static boolean isIntegerLiteralTypeConstructor(c cVar, l lVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(lVar, "receiver");
            if (lVar instanceof u0) {
                return lVar instanceof n;
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + lVar + ", " + a0.getOrCreateKotlinClass(lVar.getClass())).toString());
        }

        public static boolean isIntersection(c cVar, l lVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(lVar, "receiver");
            if (lVar instanceof u0) {
                return lVar instanceof d0.e0.p.d.m0.n.a0;
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + lVar + ", " + a0.getOrCreateKotlinClass(lVar.getClass())).toString());
        }

        public static boolean isMarkedNullable(c cVar, h hVar) {
            return p.a.isMarkedNullable(cVar, hVar);
        }

        public static boolean isNothingConstructor(c cVar, l lVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(lVar, "receiver");
            if (lVar instanceof u0) {
                return d0.e0.p.d.m0.b.h.isTypeConstructorForGivenClass((u0) lVar, k.a.c);
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + lVar + ", " + a0.getOrCreateKotlinClass(lVar.getClass())).toString());
        }

        public static boolean isNullableType(c cVar, h hVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(hVar, "receiver");
            if (hVar instanceof c0) {
                return e1.isNullableType((c0) hVar);
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + hVar + ", " + a0.getOrCreateKotlinClass(hVar.getClass())).toString());
        }

        public static boolean isPrimitiveType(c cVar, i iVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(iVar, "receiver");
            if (iVar instanceof c0) {
                return d0.e0.p.d.m0.b.h.isPrimitiveType((c0) iVar);
            }
            StringBuilder X = b.d.b.a.a.X("ClassicTypeSystemContext couldn't handle: ", iVar, ", ");
            X.append(a0.getOrCreateKotlinClass(iVar.getClass()));
            throw new IllegalArgumentException(X.toString().toString());
        }

        public static boolean isProjectionNotNull(c cVar, d0.e0.p.d.m0.n.n1.c cVar2) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(cVar2, "receiver");
            if (cVar2 instanceof i) {
                return ((i) cVar2).isProjectionNotNull();
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + cVar2 + ", " + a0.getOrCreateKotlinClass(cVar2.getClass())).toString());
        }

        public static boolean isStarProjection(c cVar, d0.e0.p.d.m0.n.n1.k kVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(kVar, "receiver");
            if (kVar instanceof w0) {
                return ((w0) kVar).isStarProjection();
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + kVar + ", " + a0.getOrCreateKotlinClass(kVar.getClass())).toString());
        }

        public static boolean isStubType(c cVar, i iVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(iVar, "receiver");
            if (iVar instanceof j0) {
                return false;
            }
            StringBuilder X = b.d.b.a.a.X("ClassicTypeSystemContext couldn't handle: ", iVar, ", ");
            X.append(a0.getOrCreateKotlinClass(iVar.getClass()));
            throw new IllegalArgumentException(X.toString().toString());
        }

        public static boolean isUnderKotlinPackage(c cVar, l lVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(lVar, "receiver");
            if (lVar instanceof u0) {
                d0.e0.p.d.m0.c.h declarationDescriptor = ((u0) lVar).getDeclarationDescriptor();
                return m.areEqual(declarationDescriptor == null ? null : Boolean.valueOf(d0.e0.p.d.m0.b.h.isUnderKotlinPackage(declarationDescriptor)), Boolean.TRUE);
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + lVar + ", " + a0.getOrCreateKotlinClass(lVar.getClass())).toString());
        }

        public static i lowerBound(c cVar, f fVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(fVar, "receiver");
            if (fVar instanceof v) {
                return ((v) fVar).getLowerBound();
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + fVar + ", " + a0.getOrCreateKotlinClass(fVar.getClass())).toString());
        }

        public static i lowerBoundIfFlexible(c cVar, h hVar) {
            return p.a.lowerBoundIfFlexible(cVar, hVar);
        }

        public static h lowerType(c cVar, d0.e0.p.d.m0.n.n1.c cVar2) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(cVar2, "receiver");
            if (cVar2 instanceof i) {
                return ((i) cVar2).getLowerType();
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + cVar2 + ", " + a0.getOrCreateKotlinClass(cVar2.getClass())).toString());
        }

        public static h makeDefinitelyNotNullOrNotNull(c cVar, h hVar) {
            i1 makeDefinitelyNotNullOrNotNull$default;
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(hVar, "receiver");
            if (hVar instanceof i1) {
                makeDefinitelyNotNullOrNotNull$default = m0.makeDefinitelyNotNullOrNotNull$default((i1) hVar, false, 1, null);
                return makeDefinitelyNotNullOrNotNull$default;
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + hVar + ", " + a0.getOrCreateKotlinClass(hVar.getClass())).toString());
        }

        public static h makeNullable(c cVar, h hVar) {
            return d1.a.makeNullable(cVar, hVar);
        }

        public static d0.e0.p.d.m0.n.f newBaseTypeCheckerContext(c cVar, boolean z2, boolean z3) {
            m.checkNotNullParameter(cVar, "this");
            return new d0.e0.p.d.m0.n.l1.a(z2, z3, false, null, 12, null);
        }

        public static i original(c cVar, d dVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(dVar, "receiver");
            if (dVar instanceof d0.e0.p.d.m0.n.k) {
                return ((d0.e0.p.d.m0.n.k) dVar).getOriginal();
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + dVar + ", " + a0.getOrCreateKotlinClass(dVar.getClass())).toString());
        }

        public static int parametersCount(c cVar, l lVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(lVar, "receiver");
            if (lVar instanceof u0) {
                return ((u0) lVar).getParameters().size();
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + lVar + ", " + a0.getOrCreateKotlinClass(lVar.getClass())).toString());
        }

        public static Collection<h> possibleIntegerTypes(c cVar, i iVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(iVar, "receiver");
            l typeConstructor = cVar.typeConstructor(iVar);
            if (typeConstructor instanceof n) {
                return ((n) typeConstructor).getPossibleTypes();
            }
            StringBuilder X = b.d.b.a.a.X("ClassicTypeSystemContext couldn't handle: ", iVar, ", ");
            X.append(a0.getOrCreateKotlinClass(iVar.getClass()));
            throw new IllegalArgumentException(X.toString().toString());
        }

        public static Collection<h> supertypes(c cVar, l lVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(lVar, "receiver");
            if (lVar instanceof u0) {
                Collection<c0> supertypes = ((u0) lVar).getSupertypes();
                m.checkNotNullExpressionValue(supertypes, "this.supertypes");
                return supertypes;
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + lVar + ", " + a0.getOrCreateKotlinClass(lVar.getClass())).toString());
        }

        public static l typeConstructor(c cVar, h hVar) {
            return p.a.typeConstructor(cVar, hVar);
        }

        public static i upperBound(c cVar, f fVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(fVar, "receiver");
            if (fVar instanceof v) {
                return ((v) fVar).getUpperBound();
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + fVar + ", " + a0.getOrCreateKotlinClass(fVar.getClass())).toString());
        }

        public static i upperBoundIfFlexible(c cVar, h hVar) {
            return p.a.upperBoundIfFlexible(cVar, hVar);
        }

        public static i withNullability(c cVar, i iVar, boolean z2) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(iVar, "receiver");
            if (iVar instanceof j0) {
                return ((j0) iVar).makeNullableAsSpecified(z2);
            }
            StringBuilder X = b.d.b.a.a.X("ClassicTypeSystemContext couldn't handle: ", iVar, ", ");
            X.append(a0.getOrCreateKotlinClass(iVar.getClass()));
            throw new IllegalArgumentException(X.toString().toString());
        }

        public static boolean isMarkedNullable(c cVar, i iVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(iVar, "receiver");
            if (iVar instanceof j0) {
                return ((j0) iVar).isMarkedNullable();
            }
            StringBuilder X = b.d.b.a.a.X("ClassicTypeSystemContext couldn't handle: ", iVar, ", ");
            X.append(a0.getOrCreateKotlinClass(iVar.getClass()));
            throw new IllegalArgumentException(X.toString().toString());
        }

        public static l typeConstructor(c cVar, i iVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(iVar, "receiver");
            if (iVar instanceof j0) {
                return ((j0) iVar).getConstructor();
            }
            StringBuilder X = b.d.b.a.a.X("ClassicTypeSystemContext couldn't handle: ", iVar, ", ");
            X.append(a0.getOrCreateKotlinClass(iVar.getClass()));
            throw new IllegalArgumentException(X.toString().toString());
        }

        public static r getVariance(c cVar, d0.e0.p.d.m0.n.n1.m mVar) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(mVar, "receiver");
            if (mVar instanceof z0) {
                j1 variance = ((z0) mVar).getVariance();
                m.checkNotNullExpressionValue(variance, "this.variance");
                return o.convertVariance(variance);
            }
            throw new IllegalArgumentException(("ClassicTypeSystemContext couldn't handle: " + mVar + ", " + a0.getOrCreateKotlinClass(mVar.getClass())).toString());
        }

        public static h withNullability(c cVar, h hVar, boolean z2) {
            m.checkNotNullParameter(cVar, "this");
            m.checkNotNullParameter(hVar, "receiver");
            if (hVar instanceof i) {
                return cVar.withNullability((i) hVar, z2);
            }
            if (hVar instanceof f) {
                f fVar = (f) hVar;
                return cVar.createFlexibleType(cVar.withNullability(cVar.lowerBound(fVar), z2), cVar.withNullability(cVar.upperBound(fVar), z2));
            }
            throw new IllegalStateException("sealed".toString());
        }
    }

    @Override // 
    i asSimpleType(h hVar);

    h createFlexibleType(i iVar, i iVar2);

    @Override // 
    i lowerBound(f fVar);

    @Override // 
    l typeConstructor(i iVar);

    @Override // 
    i upperBound(f fVar);

    @Override // 
    i withNullability(i iVar, boolean z2);
}
