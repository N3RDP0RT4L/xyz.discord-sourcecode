package d0.e0.p.d.m0.n.l1;

import d0.e0.p.d.m0.c.b0;
import d0.e0.p.d.m0.n.c0;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
/* compiled from: KotlinTypeRefiner.kt */
/* loaded from: classes3.dex */
public final class h {
    public static final b0<o<g>> a = new b0<>("KotlinTypeRefiner");

    public static final b0<o<g>> getREFINER_CAPABILITY() {
        return a;
    }

    public static final List<c0> refineTypes(g gVar, Iterable<? extends c0> iterable) {
        m.checkNotNullParameter(gVar, "<this>");
        m.checkNotNullParameter(iterable, "types");
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(iterable, 10));
        for (c0 c0Var : iterable) {
            arrayList.add(gVar.refineType(c0Var));
        }
        return arrayList;
    }
}
