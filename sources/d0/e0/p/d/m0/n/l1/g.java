package d0.e0.p.d.m0.n.l1;

import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.n.u0;
import d0.z.d.m;
import java.util.Collection;
import kotlin.jvm.functions.Function0;
/* compiled from: KotlinTypeRefiner.kt */
/* loaded from: classes3.dex */
public abstract class g {

    /* compiled from: KotlinTypeRefiner.kt */
    /* loaded from: classes3.dex */
    public static final class a extends g {
        public static final a a = new a();

        @Override // d0.e0.p.d.m0.n.l1.g
        public e findClassAcrossModuleDependencies(d0.e0.p.d.m0.g.a aVar) {
            m.checkNotNullParameter(aVar, "classId");
            return null;
        }

        @Override // d0.e0.p.d.m0.n.l1.g
        public <S extends i> S getOrPutScopeForClass(e eVar, Function0<? extends S> function0) {
            m.checkNotNullParameter(eVar, "classDescriptor");
            m.checkNotNullParameter(function0, "compute");
            return function0.invoke();
        }

        @Override // d0.e0.p.d.m0.n.l1.g
        public boolean isRefinementNeededForModule(c0 c0Var) {
            m.checkNotNullParameter(c0Var, "moduleDescriptor");
            return false;
        }

        @Override // d0.e0.p.d.m0.n.l1.g
        public boolean isRefinementNeededForTypeConstructor(u0 u0Var) {
            m.checkNotNullParameter(u0Var, "typeConstructor");
            return false;
        }

        @Override // d0.e0.p.d.m0.n.l1.g
        public e refineDescriptor(d0.e0.p.d.m0.c.m mVar) {
            m.checkNotNullParameter(mVar, "descriptor");
            return null;
        }

        @Override // d0.e0.p.d.m0.n.l1.g
        public Collection<d0.e0.p.d.m0.n.c0> refineSupertypes(e eVar) {
            m.checkNotNullParameter(eVar, "classDescriptor");
            Collection<d0.e0.p.d.m0.n.c0> supertypes = eVar.getTypeConstructor().getSupertypes();
            m.checkNotNullExpressionValue(supertypes, "classDescriptor.typeConstructor.supertypes");
            return supertypes;
        }

        @Override // d0.e0.p.d.m0.n.l1.g
        public d0.e0.p.d.m0.n.c0 refineType(d0.e0.p.d.m0.n.c0 c0Var) {
            m.checkNotNullParameter(c0Var, "type");
            return c0Var;
        }
    }

    public abstract e findClassAcrossModuleDependencies(d0.e0.p.d.m0.g.a aVar);

    public abstract <S extends i> S getOrPutScopeForClass(e eVar, Function0<? extends S> function0);

    public abstract boolean isRefinementNeededForModule(c0 c0Var);

    public abstract boolean isRefinementNeededForTypeConstructor(u0 u0Var);

    public abstract h refineDescriptor(d0.e0.p.d.m0.c.m mVar);

    public abstract Collection<d0.e0.p.d.m0.n.c0> refineSupertypes(e eVar);

    public abstract d0.e0.p.d.m0.n.c0 refineType(d0.e0.p.d.m0.n.c0 c0Var);
}
