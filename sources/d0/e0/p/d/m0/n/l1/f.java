package d0.e0.p.d.m0.n.l1;

import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.u0;
/* compiled from: KotlinTypeChecker.java */
/* loaded from: classes3.dex */
public interface f {
    public static final f a = l.f3501b.getDefault();

    /* compiled from: KotlinTypeChecker.java */
    /* loaded from: classes3.dex */
    public interface a {
        boolean equals(u0 u0Var, u0 u0Var2);
    }

    boolean equalTypes(c0 c0Var, c0 c0Var2);

    boolean isSubtypeOf(c0 c0Var, c0 c0Var2);
}
