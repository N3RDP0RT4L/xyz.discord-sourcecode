package d0.e0.p.d.m0.n.l1;

import d0.e0.p.d.m0.k.k;
import d0.e0.p.d.m0.k.u.a.c;
import d0.e0.p.d.m0.k.v.q;
import d0.e0.p.d.m0.n.a0;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.d0;
import d0.e0.p.d.m0.n.e;
import d0.e0.p.d.m0.n.e1;
import d0.e0.p.d.m0.n.g1;
import d0.e0.p.d.m0.n.i1;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.j1;
import d0.e0.p.d.m0.n.n1.b;
import d0.e0.p.d.m0.n.o1.a;
import d0.e0.p.d.m0.n.u0;
import d0.e0.p.d.m0.n.v;
import d0.e0.p.d.m0.n.w0;
import d0.t.n;
import d0.t.o;
import java.util.ArrayList;
import java.util.Collection;
import kotlin.NoWhenBranchMatchedException;
/* compiled from: NewKotlinTypeChecker.kt */
/* loaded from: classes3.dex */
public final class m implements l {
    public final g c;
    public final k d;

    public m(g gVar) {
        d0.z.d.m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
        this.c = gVar;
        k createWithTypeRefiner = k.createWithTypeRefiner(getKotlinTypeRefiner());
        d0.z.d.m.checkNotNullExpressionValue(createWithTypeRefiner, "createWithTypeRefiner(kotlinTypeRefiner)");
        this.d = createWithTypeRefiner;
    }

    @Override // d0.e0.p.d.m0.n.l1.f
    public boolean equalTypes(c0 c0Var, c0 c0Var2) {
        d0.z.d.m.checkNotNullParameter(c0Var, "a");
        d0.z.d.m.checkNotNullParameter(c0Var2, "b");
        return equalTypes(new a(false, false, false, getKotlinTypeRefiner(), 6, null), c0Var.unwrap(), c0Var2.unwrap());
    }

    @Override // d0.e0.p.d.m0.n.l1.l
    public g getKotlinTypeRefiner() {
        return this.c;
    }

    @Override // d0.e0.p.d.m0.n.l1.l
    public k getOverridingUtil() {
        return this.d;
    }

    @Override // d0.e0.p.d.m0.n.l1.f
    public boolean isSubtypeOf(c0 c0Var, c0 c0Var2) {
        d0.z.d.m.checkNotNullParameter(c0Var, "subtype");
        d0.z.d.m.checkNotNullParameter(c0Var2, "supertype");
        return isSubtypeOf(new a(true, false, false, getKotlinTypeRefiner(), 6, null), c0Var.unwrap(), c0Var2.unwrap());
    }

    public final j0 transformToNewType(j0 j0Var) {
        c0 type;
        d0.z.d.m.checkNotNullParameter(j0Var, "type");
        u0 constructor = j0Var.getConstructor();
        boolean z2 = true;
        boolean z3 = false;
        a0 a0Var = null;
        r6 = null;
        i1 unwrap = null;
        c0 c0Var = null;
        if (constructor instanceof c) {
            c cVar = (c) constructor;
            w0 projection = cVar.getProjection();
            if (projection.getProjectionKind() != j1.IN_VARIANCE) {
                z2 = false;
            }
            if (!z2) {
                projection = null;
            }
            if (!(projection == null || (type = projection.getType()) == null)) {
                unwrap = type.unwrap();
            }
            i1 i1Var = unwrap;
            if (cVar.getNewTypeConstructor() == null) {
                w0 projection2 = cVar.getProjection();
                Collection<c0> supertypes = cVar.getSupertypes();
                ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(supertypes, 10));
                for (c0 c0Var2 : supertypes) {
                    arrayList.add(c0Var2.unwrap());
                }
                cVar.setNewTypeConstructor(new j(projection2, arrayList, null, 4, null));
            }
            b bVar = b.FOR_SUBTYPING;
            j newTypeConstructor = cVar.getNewTypeConstructor();
            d0.z.d.m.checkNotNull(newTypeConstructor);
            return new i(bVar, newTypeConstructor, i1Var, j0Var.getAnnotations(), j0Var.isMarkedNullable(), false, 32, null);
        } else if (constructor instanceof q) {
            Collection<c0> supertypes2 = ((q) constructor).getSupertypes();
            ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(supertypes2, 10));
            for (c0 c0Var3 : supertypes2) {
                c0 makeNullableAsSpecified = e1.makeNullableAsSpecified(c0Var3, j0Var.isMarkedNullable());
                d0.z.d.m.checkNotNullExpressionValue(makeNullableAsSpecified, "makeNullableAsSpecified(it, type.isMarkedNullable)");
                arrayList2.add(makeNullableAsSpecified);
            }
            a0 a0Var2 = new a0(arrayList2);
            d0 d0Var = d0.a;
            return d0.simpleTypeWithNonTrivialMemberScope(j0Var.getAnnotations(), a0Var2, n.emptyList(), false, j0Var.getMemberScope());
        } else if (!(constructor instanceof a0) || !j0Var.isMarkedNullable()) {
            return j0Var;
        } else {
            a0 a0Var3 = (a0) constructor;
            Collection<c0> supertypes3 = a0Var3.getSupertypes();
            ArrayList arrayList3 = new ArrayList(o.collectionSizeOrDefault(supertypes3, 10));
            for (c0 c0Var4 : supertypes3) {
                arrayList3.add(a.makeNullable(c0Var4));
                z3 = true;
            }
            if (z3) {
                c0 alternativeType = a0Var3.getAlternativeType();
                if (alternativeType != null) {
                    c0Var = a.makeNullable(alternativeType);
                }
                a0Var = new a0(arrayList3).setAlternative(c0Var);
            }
            if (a0Var != null) {
                a0Var3 = a0Var;
            }
            return a0Var3.createType();
        }
    }

    public final boolean equalTypes(a aVar, i1 i1Var, i1 i1Var2) {
        d0.z.d.m.checkNotNullParameter(aVar, "<this>");
        d0.z.d.m.checkNotNullParameter(i1Var, "a");
        d0.z.d.m.checkNotNullParameter(i1Var2, "b");
        return e.a.equalTypes(aVar, i1Var, i1Var2);
    }

    public final boolean isSubtypeOf(a aVar, i1 i1Var, i1 i1Var2) {
        d0.z.d.m.checkNotNullParameter(aVar, "<this>");
        d0.z.d.m.checkNotNullParameter(i1Var, "subType");
        d0.z.d.m.checkNotNullParameter(i1Var2, "superType");
        return e.isSubtypeOf$default(e.a, aVar, i1Var, i1Var2, false, 8, null);
    }

    public i1 transformToNewType(i1 i1Var) {
        i1 i1Var2;
        d0.z.d.m.checkNotNullParameter(i1Var, "type");
        if (i1Var instanceof j0) {
            i1Var2 = transformToNewType((j0) i1Var);
        } else if (i1Var instanceof v) {
            v vVar = (v) i1Var;
            j0 transformToNewType = transformToNewType(vVar.getLowerBound());
            j0 transformToNewType2 = transformToNewType(vVar.getUpperBound());
            if (transformToNewType == vVar.getLowerBound() && transformToNewType2 == vVar.getUpperBound()) {
                i1Var2 = i1Var;
            } else {
                d0 d0Var = d0.a;
                i1Var2 = d0.flexibleType(transformToNewType, transformToNewType2);
            }
        } else {
            throw new NoWhenBranchMatchedException();
        }
        return g1.inheritEnhancement(i1Var2, i1Var);
    }
}
