package d0.e0.p.d.m0.n.l1;

import b.d.b.a.a;
import d0.e0.p.d.m0.j.c;
import d0.e0.p.d.m0.k.u.a.d;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.e1;
import d0.e0.p.d.m0.n.j1;
import d0.e0.p.d.m0.n.p1.b;
import d0.e0.p.d.m0.n.u0;
import d0.e0.p.d.m0.n.v0;
import d0.e0.p.d.m0.n.w0;
import d0.z.d.m;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.List;
/* compiled from: utils.kt */
/* loaded from: classes3.dex */
public final class z {
    public static final String a(u0 u0Var) {
        StringBuilder sb = new StringBuilder();
        b(m.stringPlus("type: ", u0Var), sb);
        b(m.stringPlus("hashCode: ", Integer.valueOf(u0Var.hashCode())), sb);
        b(m.stringPlus("javaClass: ", u0Var.getClass().getCanonicalName()), sb);
        for (d0.e0.p.d.m0.c.m declarationDescriptor = u0Var.getDeclarationDescriptor(); declarationDescriptor != null; declarationDescriptor = declarationDescriptor.getContainingDeclaration()) {
            b(m.stringPlus("fqName: ", c.f3411b.render(declarationDescriptor)), sb);
            b(m.stringPlus("javaClass: ", declarationDescriptor.getClass().getCanonicalName()), sb);
        }
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }

    public static final StringBuilder b(String str, StringBuilder sb) {
        m.checkNotNullParameter(str, "<this>");
        m.checkNotNullParameter(sb, "$this_anonymous");
        sb.append(str);
        m.checkNotNullExpressionValue(sb, "append(value)");
        sb.append('\n');
        m.checkNotNullExpressionValue(sb, "append('\\n')");
        return sb;
    }

    public static final c0 findCorrespondingSupertype(c0 c0Var, c0 c0Var2, u uVar) {
        boolean z2;
        boolean z3;
        j1 j1Var = j1.INVARIANT;
        m.checkNotNullParameter(c0Var, "subtype");
        m.checkNotNullParameter(c0Var2, "supertype");
        m.checkNotNullParameter(uVar, "typeCheckingProcedureCallbacks");
        ArrayDeque arrayDeque = new ArrayDeque();
        arrayDeque.add(new r(c0Var, null));
        u0 constructor = c0Var2.getConstructor();
        while (!arrayDeque.isEmpty()) {
            r rVar = (r) arrayDeque.poll();
            c0 type = rVar.getType();
            u0 constructor2 = type.getConstructor();
            s sVar = (s) uVar;
            if (sVar.assertEqualTypeConstructors(constructor2, constructor)) {
                boolean isMarkedNullable = type.isMarkedNullable();
                for (r previous = rVar.getPrevious(); previous != null; previous = previous.getPrevious()) {
                    c0 type2 = previous.getType();
                    List<w0> arguments = type2.getArguments();
                    if (!(arguments instanceof Collection) || !arguments.isEmpty()) {
                        for (w0 w0Var : arguments) {
                            if (w0Var.getProjectionKind() != j1Var) {
                                z3 = true;
                                continue;
                            } else {
                                z3 = false;
                                continue;
                            }
                            if (z3) {
                                z2 = true;
                                break;
                            }
                        }
                    }
                    z2 = false;
                    if (z2) {
                        c0 safeSubstitute = d.wrapWithCapturingSubstitution$default(v0.f3514b.create(type2), false, 1, null).buildSubstitutor().safeSubstitute(type, j1Var);
                        m.checkNotNullExpressionValue(safeSubstitute, "TypeConstructorSubstitution.create(currentType)\n                            .wrapWithCapturingSubstitution().buildSubstitutor()\n                            .safeSubstitute(substituted, Variance.INVARIANT)");
                        type = b.approximateCapturedTypes(safeSubstitute).getUpper();
                    } else {
                        type = v0.f3514b.create(type2).buildSubstitutor().safeSubstitute(type, j1Var);
                        m.checkNotNullExpressionValue(type, "{\n                    TypeConstructorSubstitution.create(currentType)\n                            .buildSubstitutor()\n                            .safeSubstitute(substituted, Variance.INVARIANT)\n                }");
                    }
                    isMarkedNullable = isMarkedNullable || type2.isMarkedNullable();
                }
                u0 constructor3 = type.getConstructor();
                if (sVar.assertEqualTypeConstructors(constructor3, constructor)) {
                    return e1.makeNullableAsSpecified(type, isMarkedNullable);
                }
                StringBuilder R = a.R("Type constructors should be equals!\nsubstitutedSuperType: ");
                R.append(a(constructor3));
                R.append(", \n\nsupertype: ");
                R.append(a(constructor));
                R.append(" \n");
                R.append(sVar.assertEqualTypeConstructors(constructor3, constructor));
                throw new AssertionError(R.toString());
            }
            for (c0 c0Var3 : constructor2.getSupertypes()) {
                m.checkNotNullExpressionValue(c0Var3, "immediateSupertype");
                arrayDeque.add(new r(c0Var3, rVar));
            }
        }
        return null;
    }
}
