package d0.e0.p.d.m0.n.l1;

import d0.e0.p.d.m0.k.k;
import d0.e0.p.d.m0.n.l1.g;
/* compiled from: NewKotlinTypeChecker.kt */
/* loaded from: classes3.dex */
public interface l extends f {

    /* renamed from: b  reason: collision with root package name */
    public static final a f3501b = a.a;

    /* compiled from: NewKotlinTypeChecker.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public static final /* synthetic */ a a = new a();

        /* renamed from: b  reason: collision with root package name */
        public static final m f3502b = new m(g.a.a);

        public final m getDefault() {
            return f3502b;
        }
    }

    g getKotlinTypeRefiner();

    k getOverridingUtil();
}
