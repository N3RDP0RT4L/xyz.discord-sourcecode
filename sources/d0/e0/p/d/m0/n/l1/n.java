package d0.e0.p.d.m0.n.l1;

import d0.e0.p.d.m0.n.c;
import d0.e0.p.d.m0.n.f;
import d0.e0.p.d.m0.n.i1;
import d0.e0.p.d.m0.n.y;
import d0.z.d.m;
/* compiled from: NewKotlinTypeChecker.kt */
/* loaded from: classes3.dex */
public final class n {
    public static final n a = new n();

    public final boolean isSubtypeOfAny(i1 i1Var) {
        m.checkNotNullParameter(i1Var, "type");
        return c.a.hasNotNullSupertype(p.a.newBaseTypeCheckerContext(false, true), y.lowerIfFlexible(i1Var), f.b.C0358b.a);
    }
}
