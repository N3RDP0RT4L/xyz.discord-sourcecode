package d0.e0.p.d.m0.n.l1;

import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.n.i1;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.n1.b;
import d0.e0.p.d.m0.n.n1.c;
import d0.e0.p.d.m0.n.t;
import d0.e0.p.d.m0.n.w0;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: NewCapturedType.kt */
/* loaded from: classes3.dex */
public final class i extends j0 implements c {
    public final b k;
    public final j l;
    public final i1 m;
    public final g n;
    public final boolean o;
    public final boolean p;

    public /* synthetic */ i(b bVar, j jVar, i1 i1Var, g gVar, boolean z2, boolean z3, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(bVar, jVar, i1Var, (i & 8) != 0 ? g.f.getEMPTY() : gVar, (i & 16) != 0 ? false : z2, (i & 32) != 0 ? false : z3);
    }

    @Override // d0.e0.p.d.m0.c.g1.a
    public g getAnnotations() {
        return this.n;
    }

    @Override // d0.e0.p.d.m0.n.c0
    public List<w0> getArguments() {
        return n.emptyList();
    }

    public final b getCaptureStatus() {
        return this.k;
    }

    public final i1 getLowerType() {
        return this.m;
    }

    @Override // d0.e0.p.d.m0.n.c0
    public d0.e0.p.d.m0.k.a0.i getMemberScope() {
        d0.e0.p.d.m0.k.a0.i createErrorScope = t.createErrorScope("No member resolution should be done on captured type!", true);
        m.checkNotNullExpressionValue(createErrorScope, "createErrorScope(\"No member resolution should be done on captured type!\", true)");
        return createErrorScope;
    }

    @Override // d0.e0.p.d.m0.n.c0
    public boolean isMarkedNullable() {
        return this.o;
    }

    public final boolean isProjectionNotNull() {
        return this.p;
    }

    @Override // d0.e0.p.d.m0.n.c0
    public j getConstructor() {
        return this.l;
    }

    public i(b bVar, j jVar, i1 i1Var, g gVar, boolean z2, boolean z3) {
        m.checkNotNullParameter(bVar, "captureStatus");
        m.checkNotNullParameter(jVar, "constructor");
        m.checkNotNullParameter(gVar, "annotations");
        this.k = bVar;
        this.l = jVar;
        this.m = i1Var;
        this.n = gVar;
        this.o = z2;
        this.p = z3;
    }

    @Override // d0.e0.p.d.m0.n.j0, d0.e0.p.d.m0.n.i1
    public i makeNullableAsSpecified(boolean z2) {
        return new i(this.k, getConstructor(), this.m, getAnnotations(), z2, false, 32, null);
    }

    @Override // d0.e0.p.d.m0.n.i1, d0.e0.p.d.m0.n.c0
    public i refine(g gVar) {
        m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
        b bVar = this.k;
        j refine = getConstructor().refine(gVar);
        i1 i1Var = this.m;
        return new i(bVar, refine, i1Var == null ? null : gVar.refineType(i1Var).unwrap(), getAnnotations(), isMarkedNullable(), false, 32, null);
    }

    @Override // d0.e0.p.d.m0.n.j0, d0.e0.p.d.m0.n.i1
    public i replaceAnnotations(g gVar) {
        m.checkNotNullParameter(gVar, "newAnnotations");
        return new i(this.k, getConstructor(), this.m, gVar, isMarkedNullable(), false, 32, null);
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public i(b bVar, i1 i1Var, w0 w0Var, z0 z0Var) {
        this(bVar, new j(w0Var, null, null, z0Var, 6, null), i1Var, null, false, false, 56, null);
        m.checkNotNullParameter(bVar, "captureStatus");
        m.checkNotNullParameter(w0Var, "projection");
        m.checkNotNullParameter(z0Var, "typeParameter");
    }
}
