package d0.e0.p.d.m0.n.l1;

import b.d.b.a.a;
import d0.z.d.a0;
/* compiled from: ClassicTypeCheckerContext.kt */
/* loaded from: classes3.dex */
public final class b {
    public static final String access$errorMessage(Object obj) {
        StringBuilder R = a.R("ClassicTypeCheckerContext couldn't handle ");
        R.append(a0.getOrCreateKotlinClass(obj.getClass()));
        R.append(' ');
        R.append(obj);
        return R.toString();
    }
}
