package d0.e0.p.d.m0.n.l1;

import d0.e0.p.d.m0.n.d;
import d0.e0.p.d.m0.n.i1;
import d0.z.d.m;
/* compiled from: NewKotlinTypeChecker.kt */
/* loaded from: classes3.dex */
public final class q {
    public static final q a = new q();

    public final boolean strictEqualTypes(i1 i1Var, i1 i1Var2) {
        m.checkNotNullParameter(i1Var, "a");
        m.checkNotNullParameter(i1Var2, "b");
        return d.a.strictEqualTypes(p.a, i1Var, i1Var2);
    }
}
