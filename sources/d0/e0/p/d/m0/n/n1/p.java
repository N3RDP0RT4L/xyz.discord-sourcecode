package d0.e0.p.d.m0.n.n1;

import d0.e0.p.d.m0.n.n1.n;
/* compiled from: TypeSystemContext.kt */
/* loaded from: classes3.dex */
public interface p extends n {

    /* compiled from: TypeSystemContext.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public static boolean isMarkedNullable(p pVar, h hVar) {
            return n.a.isMarkedNullable(pVar, hVar);
        }

        public static i lowerBoundIfFlexible(p pVar, h hVar) {
            return n.a.lowerBoundIfFlexible(pVar, hVar);
        }

        public static l typeConstructor(p pVar, h hVar) {
            return n.a.typeConstructor(pVar, hVar);
        }

        public static i upperBoundIfFlexible(p pVar, h hVar) {
            return n.a.upperBoundIfFlexible(pVar, hVar);
        }
    }
}
