package d0.e0.p.d.m0.n.n1;

import d0.e0.p.d.m0.n.j1;
import d0.z.d.m;
import kotlin.NoWhenBranchMatchedException;
/* compiled from: TypeSystemContext.kt */
/* loaded from: classes3.dex */
public final class o {
    public static final r convertVariance(j1 j1Var) {
        m.checkNotNullParameter(j1Var, "<this>");
        int ordinal = j1Var.ordinal();
        if (ordinal == 0) {
            return r.INV;
        }
        if (ordinal == 1) {
            return r.IN;
        }
        if (ordinal == 2) {
            return r.OUT;
        }
        throw new NoWhenBranchMatchedException();
    }
}
