package d0.e0.p.d.m0.n.n1;
/* compiled from: TypeSystemContext.kt */
/* loaded from: classes3.dex */
public enum r {
    IN("in"),
    OUT("out"),
    INV("");
    
    private final String presentation;

    r(String str) {
        this.presentation = str;
    }

    @Override // java.lang.Enum
    public String toString() {
        return this.presentation;
    }
}
