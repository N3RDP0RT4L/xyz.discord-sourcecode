package d0.e0.p.d.m0.n;

import d0.z.d.m;
/* compiled from: KotlinTypeFactory.kt */
/* loaded from: classes3.dex */
public final class g0 extends n {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public g0(j0 j0Var) {
        super(j0Var);
        m.checkNotNullParameter(j0Var, "delegate");
    }

    @Override // d0.e0.p.d.m0.n.m, d0.e0.p.d.m0.n.c0
    public boolean isMarkedNullable() {
        return false;
    }

    @Override // d0.e0.p.d.m0.n.m
    public g0 replaceDelegate(j0 j0Var) {
        m.checkNotNullParameter(j0Var, "delegate");
        return new g0(j0Var);
    }
}
