package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.k.a0.i;
import java.util.List;
/* compiled from: KotlinType.kt */
/* loaded from: classes3.dex */
public abstract class k1 extends c0 {
    public k1() {
        super(null);
    }

    public abstract c0 a();

    @Override // d0.e0.p.d.m0.c.g1.a
    public g getAnnotations() {
        return a().getAnnotations();
    }

    @Override // d0.e0.p.d.m0.n.c0
    public List<w0> getArguments() {
        return a().getArguments();
    }

    @Override // d0.e0.p.d.m0.n.c0
    public u0 getConstructor() {
        return a().getConstructor();
    }

    @Override // d0.e0.p.d.m0.n.c0
    public i getMemberScope() {
        return a().getMemberScope();
    }

    public boolean isComputed() {
        return true;
    }

    @Override // d0.e0.p.d.m0.n.c0
    public boolean isMarkedNullable() {
        return a().isMarkedNullable();
    }

    public String toString() {
        return isComputed() ? a().toString() : "<Not computed yet>";
    }

    @Override // d0.e0.p.d.m0.n.c0
    public final i1 unwrap() {
        c0 a = a();
        while (a instanceof k1) {
            a = ((k1) a).a();
        }
        return (i1) a;
    }
}
