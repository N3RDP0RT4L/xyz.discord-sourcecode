package d0.e0.p.d.m0.n;

import d0.z.d.m;
/* compiled from: TypeCapabilities.kt */
/* loaded from: classes3.dex */
public final class t0 {
    public static final j getCustomTypeVariable(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        i1 unwrap = c0Var.unwrap();
        j jVar = unwrap instanceof j ? (j) unwrap : null;
        if (jVar != null && jVar.isTypeVariable()) {
            return jVar;
        }
        return null;
    }

    public static final boolean isCustomTypeVariable(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        i1 unwrap = c0Var.unwrap();
        j jVar = unwrap instanceof j ? (j) unwrap : null;
        if (jVar == null) {
            return false;
        }
        return jVar.isTypeVariable();
    }
}
