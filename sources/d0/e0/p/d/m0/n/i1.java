package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.n.l1.g;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: KotlinType.kt */
/* loaded from: classes3.dex */
public abstract class i1 extends c0 {
    public i1() {
        super(null);
    }

    public abstract i1 makeNullableAsSpecified(boolean z2);

    @Override // d0.e0.p.d.m0.n.c0
    public abstract i1 refine(g gVar);

    public abstract i1 replaceAnnotations(d0.e0.p.d.m0.c.g1.g gVar);

    @Override // d0.e0.p.d.m0.n.c0
    public final i1 unwrap() {
        return this;
    }

    public i1(DefaultConstructorMarker defaultConstructorMarker) {
        super(null);
    }
}
