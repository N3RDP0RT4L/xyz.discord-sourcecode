package d0.e0.p.d.m0.n;

import b.d.b.a.a;
import d0.e0.p.d.m0.n.f;
import d0.e0.p.d.m0.n.n1.h;
import d0.e0.p.d.m0.n.n1.i;
import d0.e0.p.d.m0.n.n1.l;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayDeque;
import java.util.Set;
/* compiled from: AbstractTypeChecker.kt */
/* loaded from: classes3.dex */
public final class c {
    public static final c a = new c();

    public final boolean a(f fVar, i iVar, l lVar) {
        if (fVar.isNothing(iVar)) {
            return true;
        }
        if (fVar.isMarkedNullable(iVar)) {
            return false;
        }
        if (!fVar.isStubTypeEqualsToAnything() || !fVar.isStubType(iVar)) {
            return fVar.areEqualTypeConstructors(fVar.typeConstructor(iVar), lVar);
        }
        return true;
    }

    public final boolean hasNotNullSupertype(f fVar, i iVar, f.b bVar) {
        m.checkNotNullParameter(fVar, "<this>");
        m.checkNotNullParameter(iVar, "type");
        m.checkNotNullParameter(bVar, "supertypesPolicy");
        if (!((fVar.isClassType(iVar) && !fVar.isMarkedNullable(iVar)) || fVar.isDefinitelyNotNullType(iVar))) {
            fVar.initialize();
            ArrayDeque<i> supertypesDeque = fVar.getSupertypesDeque();
            m.checkNotNull(supertypesDeque);
            Set<i> supertypesSet = fVar.getSupertypesSet();
            m.checkNotNull(supertypesSet);
            supertypesDeque.push(iVar);
            while (!supertypesDeque.isEmpty()) {
                if (supertypesSet.size() <= 1000) {
                    i pop = supertypesDeque.pop();
                    m.checkNotNullExpressionValue(pop, "current");
                    if (supertypesSet.add(pop)) {
                        f.b bVar2 = fVar.isMarkedNullable(pop) ? f.b.c.a : bVar;
                        if (!(!m.areEqual(bVar2, f.b.c.a))) {
                            bVar2 = null;
                        }
                        if (bVar2 == null) {
                            continue;
                        } else {
                            for (h hVar : fVar.supertypes(fVar.typeConstructor(pop))) {
                                i transformType = bVar2.transformType(fVar, hVar);
                                if ((fVar.isClassType(transformType) && !fVar.isMarkedNullable(transformType)) || fVar.isDefinitelyNotNullType(transformType)) {
                                    fVar.clear();
                                } else {
                                    supertypesDeque.add(transformType);
                                }
                            }
                            continue;
                        }
                    }
                } else {
                    StringBuilder X = a.X("Too many supertypes for type: ", iVar, ". Supertypes = ");
                    X.append(u.joinToString$default(supertypesSet, null, null, null, 0, null, null, 63, null));
                    throw new IllegalStateException(X.toString().toString());
                }
            }
            fVar.clear();
            return false;
        }
        return true;
    }

    public final boolean hasPathByNotMarkedNullableNodes(f fVar, i iVar, l lVar) {
        m.checkNotNullParameter(fVar, "<this>");
        m.checkNotNullParameter(iVar, "start");
        m.checkNotNullParameter(lVar, "end");
        if (a(fVar, iVar, lVar)) {
            return true;
        }
        fVar.initialize();
        ArrayDeque<i> supertypesDeque = fVar.getSupertypesDeque();
        m.checkNotNull(supertypesDeque);
        Set<i> supertypesSet = fVar.getSupertypesSet();
        m.checkNotNull(supertypesSet);
        supertypesDeque.push(iVar);
        while (!supertypesDeque.isEmpty()) {
            if (supertypesSet.size() <= 1000) {
                i pop = supertypesDeque.pop();
                m.checkNotNullExpressionValue(pop, "current");
                if (supertypesSet.add(pop)) {
                    f.b bVar = fVar.isMarkedNullable(pop) ? f.b.c.a : f.b.C0358b.a;
                    if (!(!m.areEqual(bVar, f.b.c.a))) {
                        bVar = null;
                    }
                    if (bVar == null) {
                        continue;
                    } else {
                        for (h hVar : fVar.supertypes(fVar.typeConstructor(pop))) {
                            i transformType = bVar.transformType(fVar, hVar);
                            if (a(fVar, transformType, lVar)) {
                                fVar.clear();
                                return true;
                            }
                            supertypesDeque.add(transformType);
                        }
                        continue;
                    }
                }
            } else {
                StringBuilder X = a.X("Too many supertypes for type: ", iVar, ". Supertypes = ");
                X.append(u.joinToString$default(supertypesSet, null, null, null, 0, null, null, 63, null));
                throw new IllegalStateException(X.toString().toString());
            }
        }
        fVar.clear();
        return false;
    }

    public final boolean isPossibleSubtype(f fVar, i iVar, i iVar2) {
        m.checkNotNullParameter(fVar, "context");
        m.checkNotNullParameter(iVar, "subType");
        m.checkNotNullParameter(iVar2, "superType");
        if (fVar.isMarkedNullable(iVar2) || fVar.isDefinitelyNotNullType(iVar)) {
            return true;
        }
        if (((iVar instanceof d0.e0.p.d.m0.n.n1.c) && fVar.isProjectionNotNull((d0.e0.p.d.m0.n.n1.c) iVar)) || hasNotNullSupertype(fVar, iVar, f.b.C0358b.a)) {
            return true;
        }
        if (!fVar.isDefinitelyNotNullType(iVar2) && !hasNotNullSupertype(fVar, iVar2, f.b.d.a) && !fVar.isClassType(iVar)) {
            return hasPathByNotMarkedNullableNodes(fVar, iVar, fVar.typeConstructor(iVar2));
        }
        return false;
    }
}
