package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.c.z0;
import d0.t.h0;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: TypeSubstitution.kt */
/* loaded from: classes3.dex */
public abstract class v0 extends z0 {

    /* renamed from: b  reason: collision with root package name */
    public static final a f3514b = new a(null);

    /* compiled from: TypeSubstitution.kt */
    /* loaded from: classes3.dex */
    public static final class a {

        /* compiled from: TypeSubstitution.kt */
        /* renamed from: d0.e0.p.d.m0.n.v0$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0365a extends v0 {
            public final /* synthetic */ Map<u0, w0> c;
            public final /* synthetic */ boolean d;

            /* JADX WARN: Multi-variable type inference failed */
            public C0365a(Map<u0, ? extends w0> map, boolean z2) {
                this.c = map;
                this.d = z2;
            }

            @Override // d0.e0.p.d.m0.n.z0
            public boolean approximateCapturedTypes() {
                return this.d;
            }

            @Override // d0.e0.p.d.m0.n.v0
            public w0 get(u0 u0Var) {
                m.checkNotNullParameter(u0Var, "key");
                return this.c.get(u0Var);
            }

            @Override // d0.e0.p.d.m0.n.z0
            public boolean isEmpty() {
                return this.c.isEmpty();
            }
        }

        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public static /* synthetic */ v0 createByConstructorsMap$default(a aVar, Map map, boolean z2, int i, Object obj) {
            if ((i & 2) != 0) {
                z2 = false;
            }
            return aVar.createByConstructorsMap(map, z2);
        }

        public final z0 create(c0 c0Var) {
            m.checkNotNullParameter(c0Var, "kotlinType");
            return create(c0Var.getConstructor(), c0Var.getArguments());
        }

        public final v0 createByConstructorsMap(Map<u0, ? extends w0> map) {
            m.checkNotNullParameter(map, "map");
            return createByConstructorsMap$default(this, map, false, 2, null);
        }

        public final v0 createByConstructorsMap(Map<u0, ? extends w0> map, boolean z2) {
            m.checkNotNullParameter(map, "map");
            return new C0365a(map, z2);
        }

        public final z0 create(u0 u0Var, List<? extends w0> list) {
            m.checkNotNullParameter(u0Var, "typeConstructor");
            m.checkNotNullParameter(list, "arguments");
            List<z0> parameters = u0Var.getParameters();
            m.checkNotNullExpressionValue(parameters, "typeConstructor.parameters");
            z0 z0Var = (z0) u.lastOrNull((List<? extends Object>) parameters);
            if (!m.areEqual(z0Var == null ? null : Boolean.valueOf(z0Var.isCapturedFromOuterDeclaration()), Boolean.TRUE)) {
                return new z(parameters, list);
            }
            List<z0> parameters2 = u0Var.getParameters();
            m.checkNotNullExpressionValue(parameters2, "typeConstructor.parameters");
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(parameters2, 10));
            for (z0 z0Var2 : parameters2) {
                arrayList.add(z0Var2.getTypeConstructor());
            }
            return createByConstructorsMap$default(this, h0.toMap(u.zip(arrayList, list)), false, 2, null);
        }
    }

    public static final z0 create(u0 u0Var, List<? extends w0> list) {
        return f3514b.create(u0Var, list);
    }

    public static final v0 createByConstructorsMap(Map<u0, ? extends w0> map) {
        return f3514b.createByConstructorsMap(map);
    }

    @Override // d0.e0.p.d.m0.n.z0
    public w0 get(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "key");
        return get(c0Var.getConstructor());
    }

    public abstract w0 get(u0 u0Var);
}
