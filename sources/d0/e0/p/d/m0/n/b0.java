package d0.e0.p.d.m0.n;

import d0.u.a;
import java.util.Comparator;
/* compiled from: Comparisons.kt */
/* loaded from: classes3.dex */
public final class b0<T> implements Comparator<T> {
    @Override // java.util.Comparator
    public final int compare(T t, T t2) {
        return a.compareValues(((c0) t).toString(), ((c0) t2).toString());
    }
}
