package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.i;
import d0.e0.p.d.m0.c.z0;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
/* compiled from: StarProjectionImpl.kt */
/* loaded from: classes3.dex */
public final class p0 {

    /* compiled from: StarProjectionImpl.kt */
    /* loaded from: classes3.dex */
    public static final class a extends v0 {
        public final /* synthetic */ List<u0> c;

        /* JADX WARN: Multi-variable type inference failed */
        public a(List<? extends u0> list) {
            this.c = list;
        }

        @Override // d0.e0.p.d.m0.n.v0
        public w0 get(u0 u0Var) {
            m.checkNotNullParameter(u0Var, "key");
            if (!this.c.contains(u0Var)) {
                return null;
            }
            h declarationDescriptor = u0Var.getDeclarationDescriptor();
            Objects.requireNonNull(declarationDescriptor, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.TypeParameterDescriptor");
            return e1.makeStarProjection((z0) declarationDescriptor);
        }
    }

    public static final c0 starProjectionType(z0 z0Var) {
        m.checkNotNullParameter(z0Var, "<this>");
        List<z0> parameters = ((i) z0Var.getContainingDeclaration()).getTypeConstructor().getParameters();
        m.checkNotNullExpressionValue(parameters, "classDescriptor.typeConstructor.parameters");
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(parameters, 10));
        for (z0 z0Var2 : parameters) {
            arrayList.add(z0Var2.getTypeConstructor());
        }
        c1 create = c1.create(new a(arrayList));
        List<c0> upperBounds = z0Var.getUpperBounds();
        m.checkNotNullExpressionValue(upperBounds, "this.upperBounds");
        c0 substitute = create.substitute((c0) u.first((List<? extends Object>) upperBounds), j1.OUT_VARIANCE);
        if (substitute != null) {
            return substitute;
        }
        j0 defaultBound = d0.e0.p.d.m0.k.x.a.getBuiltIns(z0Var).getDefaultBound();
        m.checkNotNullExpressionValue(defaultBound, "builtIns.defaultBound");
        return defaultBound;
    }
}
