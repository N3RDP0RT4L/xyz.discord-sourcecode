package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.k.a0.i;
import java.util.List;
/* compiled from: SpecialTypes.kt */
/* loaded from: classes3.dex */
public abstract class m extends j0 {
    @Override // d0.e0.p.d.m0.c.g1.a
    public g getAnnotations() {
        return getDelegate().getAnnotations();
    }

    @Override // d0.e0.p.d.m0.n.c0
    public List<w0> getArguments() {
        return getDelegate().getArguments();
    }

    @Override // d0.e0.p.d.m0.n.c0
    public u0 getConstructor() {
        return getDelegate().getConstructor();
    }

    public abstract j0 getDelegate();

    @Override // d0.e0.p.d.m0.n.c0
    public i getMemberScope() {
        return getDelegate().getMemberScope();
    }

    @Override // d0.e0.p.d.m0.n.c0
    public boolean isMarkedNullable() {
        return getDelegate().isMarkedNullable();
    }

    public abstract m replaceDelegate(j0 j0Var);

    @Override // d0.e0.p.d.m0.n.i1, d0.e0.p.d.m0.n.c0
    public j0 refine(d0.e0.p.d.m0.n.l1.g gVar) {
        d0.z.d.m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
        return replaceDelegate((j0) gVar.refineType(getDelegate()));
    }
}
