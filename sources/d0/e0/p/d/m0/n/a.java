package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.c.g1.g;
import d0.z.d.m;
/* compiled from: SpecialTypes.kt */
/* loaded from: classes3.dex */
public final class a extends m {
    public final j0 k;
    public final j0 l;

    public a(j0 j0Var, j0 j0Var2) {
        m.checkNotNullParameter(j0Var, "delegate");
        m.checkNotNullParameter(j0Var2, "abbreviation");
        this.k = j0Var;
        this.l = j0Var2;
    }

    public final j0 getAbbreviation() {
        return this.l;
    }

    @Override // d0.e0.p.d.m0.n.m
    public j0 getDelegate() {
        return this.k;
    }

    public final j0 getExpandedType() {
        return this.k;
    }

    @Override // d0.e0.p.d.m0.n.m
    public a replaceDelegate(j0 j0Var) {
        m.checkNotNullParameter(j0Var, "delegate");
        return new a(j0Var, this.l);
    }

    @Override // d0.e0.p.d.m0.n.j0, d0.e0.p.d.m0.n.i1
    public a makeNullableAsSpecified(boolean z2) {
        return new a(this.k.makeNullableAsSpecified(z2), this.l.makeNullableAsSpecified(z2));
    }

    @Override // d0.e0.p.d.m0.n.j0, d0.e0.p.d.m0.n.i1
    public a replaceAnnotations(g gVar) {
        m.checkNotNullParameter(gVar, "newAnnotations");
        return new a(this.k.replaceAnnotations(gVar), this.l);
    }

    @Override // d0.e0.p.d.m0.n.m, d0.e0.p.d.m0.n.i1, d0.e0.p.d.m0.n.c0
    public a refine(d0.e0.p.d.m0.n.l1.g gVar) {
        m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
        return new a((j0) gVar.refineType(this.k), (j0) gVar.refineType(this.l));
    }
}
