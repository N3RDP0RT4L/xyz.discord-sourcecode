package d0.e0.p.d.m0.n;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.b.k;
import d0.e0.p.d.m0.g.b;
import kotlin.jvm.functions.Function1;
/* compiled from: TypeSubstitutor.java */
/* loaded from: classes3.dex */
public final class b1 implements Function1<b, Boolean> {
    public Boolean invoke(b bVar) {
        if (bVar != null) {
            return Boolean.valueOf(!bVar.equals(k.a.G));
        }
        throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", ModelAuditLogEntry.CHANGE_KEY_NAME, "kotlin/reflect/jvm/internal/impl/types/TypeSubstitutor$1", "invoke"));
    }
}
