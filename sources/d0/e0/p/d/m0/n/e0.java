package d0.e0.p.d.m0.n;

import d0.z.d.m;
/* compiled from: KotlinType.kt */
/* loaded from: classes3.dex */
public final class e0 {
    public static final boolean isError(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        i1 unwrap = c0Var.unwrap();
        return (unwrap instanceof s) || ((unwrap instanceof v) && (((v) unwrap).getDelegate() instanceof s));
    }

    public static final boolean isNullable(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        return e1.isNullableType(c0Var);
    }
}
