package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.i1.v;
import d0.e0.p.d.m0.c.y0;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.k.v.n;
import d0.e0.p.d.m0.n.l1.g;
import d0.e0.p.d.m0.n.s0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import java.util.Objects;
import kotlin.jvm.functions.Function1;
/* compiled from: KotlinTypeFactory.kt */
/* loaded from: classes3.dex */
public final class d0 {
    public static final d0 a = new d0();

    /* compiled from: KotlinTypeFactory.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1 {
        public static final a j = new a();

        public a() {
            super(1);
        }

        public final Void invoke(g gVar) {
            m.checkNotNullParameter(gVar, "$noName_0");
            return null;
        }
    }

    /* compiled from: KotlinTypeFactory.kt */
    /* loaded from: classes3.dex */
    public static final class b {
        public final j0 a;

        /* renamed from: b */
        public final u0 f3493b;

        public b(j0 j0Var, u0 u0Var) {
            this.a = j0Var;
            this.f3493b = u0Var;
        }

        public final j0 getExpandedType() {
            return this.a;
        }

        public final u0 getRefinedConstructor() {
            return this.f3493b;
        }
    }

    /* compiled from: KotlinTypeFactory.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function1<g, j0> {
        public final /* synthetic */ d0.e0.p.d.m0.c.g1.g $annotations;
        public final /* synthetic */ List<w0> $arguments;
        public final /* synthetic */ u0 $constructor;
        public final /* synthetic */ boolean $nullable;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        /* JADX WARN: Multi-variable type inference failed */
        public c(u0 u0Var, List<? extends w0> list, d0.e0.p.d.m0.c.g1.g gVar, boolean z2) {
            super(1);
            d0.this = r1;
            this.$constructor = u0Var;
            this.$arguments = list;
            this.$annotations = gVar;
            this.$nullable = z2;
        }

        public final j0 invoke(g gVar) {
            m.checkNotNullParameter(gVar, "refiner");
            b access$refineConstructor = d0.access$refineConstructor(d0.this, this.$constructor, gVar, this.$arguments);
            if (access$refineConstructor == null) {
                return null;
            }
            j0 expandedType = access$refineConstructor.getExpandedType();
            if (expandedType != null) {
                return expandedType;
            }
            d0.e0.p.d.m0.c.g1.g gVar2 = this.$annotations;
            u0 refinedConstructor = access$refineConstructor.getRefinedConstructor();
            m.checkNotNull(refinedConstructor);
            return d0.simpleType(gVar2, refinedConstructor, this.$arguments, this.$nullable, gVar);
        }
    }

    /* compiled from: KotlinTypeFactory.kt */
    /* loaded from: classes3.dex */
    public static final class d extends o implements Function1<g, j0> {
        public final /* synthetic */ d0.e0.p.d.m0.c.g1.g $annotations;
        public final /* synthetic */ List<w0> $arguments;
        public final /* synthetic */ u0 $constructor;
        public final /* synthetic */ i $memberScope;
        public final /* synthetic */ boolean $nullable;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        /* JADX WARN: Multi-variable type inference failed */
        public d(u0 u0Var, List<? extends w0> list, d0.e0.p.d.m0.c.g1.g gVar, boolean z2, i iVar) {
            super(1);
            d0.this = r1;
            this.$constructor = u0Var;
            this.$arguments = list;
            this.$annotations = gVar;
            this.$nullable = z2;
            this.$memberScope = iVar;
        }

        public final j0 invoke(g gVar) {
            m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
            b access$refineConstructor = d0.access$refineConstructor(d0.this, this.$constructor, gVar, this.$arguments);
            if (access$refineConstructor == null) {
                return null;
            }
            j0 expandedType = access$refineConstructor.getExpandedType();
            if (expandedType != null) {
                return expandedType;
            }
            d0.e0.p.d.m0.c.g1.g gVar2 = this.$annotations;
            u0 refinedConstructor = access$refineConstructor.getRefinedConstructor();
            m.checkNotNull(refinedConstructor);
            return d0.simpleTypeWithNonTrivialMemberScope(gVar2, refinedConstructor, this.$arguments, this.$nullable, this.$memberScope);
        }
    }

    static {
        a aVar = a.j;
    }

    public static final b access$refineConstructor(d0 d0Var, u0 u0Var, g gVar, List list) {
        b bVar;
        Objects.requireNonNull(d0Var);
        h declarationDescriptor = u0Var.getDeclarationDescriptor();
        h refineDescriptor = declarationDescriptor == null ? null : gVar.refineDescriptor(declarationDescriptor);
        if (refineDescriptor == null) {
            return null;
        }
        if (refineDescriptor instanceof y0) {
            bVar = new b(computeExpandedType((y0) refineDescriptor, list), null);
        } else {
            u0 refine = refineDescriptor.getTypeConstructor().refine(gVar);
            m.checkNotNullExpressionValue(refine, "descriptor.typeConstructor.refine(kotlinTypeRefiner)");
            bVar = new b(null, refine);
        }
        return bVar;
    }

    public static final j0 computeExpandedType(y0 y0Var, List<? extends w0> list) {
        m.checkNotNullParameter(y0Var, "<this>");
        m.checkNotNullParameter(list, "arguments");
        return new q0(s0.a.a, false).expand(r0.a.create(null, y0Var, list), d0.e0.p.d.m0.c.g1.g.f.getEMPTY());
    }

    public static final i1 flexibleType(j0 j0Var, j0 j0Var2) {
        m.checkNotNullParameter(j0Var, "lowerBound");
        m.checkNotNullParameter(j0Var2, "upperBound");
        return m.areEqual(j0Var, j0Var2) ? j0Var : new w(j0Var, j0Var2);
    }

    public static final j0 integerLiteralType(d0.e0.p.d.m0.c.g1.g gVar, n nVar, boolean z2) {
        m.checkNotNullParameter(gVar, "annotations");
        m.checkNotNullParameter(nVar, "constructor");
        List emptyList = d0.t.n.emptyList();
        i createErrorScope = t.createErrorScope("Scope for integer literal type", true);
        m.checkNotNullExpressionValue(createErrorScope, "createErrorScope(\"Scope for integer literal type\", true)");
        return simpleTypeWithNonTrivialMemberScope(gVar, nVar, emptyList, z2, createErrorScope);
    }

    public static final j0 simpleNotNullType(d0.e0.p.d.m0.c.g1.g gVar, e eVar, List<? extends w0> list) {
        m.checkNotNullParameter(gVar, "annotations");
        m.checkNotNullParameter(eVar, "descriptor");
        m.checkNotNullParameter(list, "arguments");
        u0 typeConstructor = eVar.getTypeConstructor();
        m.checkNotNullExpressionValue(typeConstructor, "descriptor.typeConstructor");
        return simpleType$default(gVar, typeConstructor, list, false, null, 16, null);
    }

    public static final j0 simpleType(d0.e0.p.d.m0.c.g1.g gVar, u0 u0Var, List<? extends w0> list, boolean z2, g gVar2) {
        i iVar;
        m.checkNotNullParameter(gVar, "annotations");
        m.checkNotNullParameter(u0Var, "constructor");
        m.checkNotNullParameter(list, "arguments");
        if (!gVar.isEmpty() || !list.isEmpty() || z2 || u0Var.getDeclarationDescriptor() == null) {
            d0 d0Var = a;
            h declarationDescriptor = u0Var.getDeclarationDescriptor();
            if (declarationDescriptor instanceof z0) {
                iVar = declarationDescriptor.getDefaultType().getMemberScope();
            } else if (declarationDescriptor instanceof e) {
                if (gVar2 == null) {
                    gVar2 = d0.e0.p.d.m0.k.x.a.getKotlinTypeRefiner(d0.e0.p.d.m0.k.x.a.getModule(declarationDescriptor));
                }
                if (list.isEmpty()) {
                    iVar = v.getRefinedUnsubstitutedMemberScopeIfPossible((e) declarationDescriptor, gVar2);
                } else {
                    iVar = v.getRefinedMemberScopeIfPossible((e) declarationDescriptor, v0.f3514b.create(u0Var, list), gVar2);
                }
            } else if (declarationDescriptor instanceof y0) {
                iVar = t.createErrorScope(m.stringPlus("Scope for abbreviation: ", ((y0) declarationDescriptor).getName()), true);
                m.checkNotNullExpressionValue(iVar, "createErrorScope(\"Scope for abbreviation: ${descriptor.name}\", true)");
            } else if (u0Var instanceof a0) {
                iVar = ((a0) u0Var).createScopeForKotlinType();
            } else {
                throw new IllegalStateException("Unsupported classifier: " + declarationDescriptor + " for constructor: " + u0Var);
            }
            return simpleTypeWithNonTrivialMemberScope(gVar, u0Var, list, z2, iVar, new c(u0Var, list, gVar, z2));
        }
        h declarationDescriptor2 = u0Var.getDeclarationDescriptor();
        m.checkNotNull(declarationDescriptor2);
        j0 defaultType = declarationDescriptor2.getDefaultType();
        m.checkNotNullExpressionValue(defaultType, "constructor.declarationDescriptor!!.defaultType");
        return defaultType;
    }

    public static /* synthetic */ j0 simpleType$default(d0.e0.p.d.m0.c.g1.g gVar, u0 u0Var, List list, boolean z2, g gVar2, int i, Object obj) {
        if ((i & 16) != 0) {
            gVar2 = null;
        }
        return simpleType(gVar, u0Var, list, z2, gVar2);
    }

    public static final j0 simpleTypeWithNonTrivialMemberScope(d0.e0.p.d.m0.c.g1.g gVar, u0 u0Var, List<? extends w0> list, boolean z2, i iVar) {
        m.checkNotNullParameter(gVar, "annotations");
        m.checkNotNullParameter(u0Var, "constructor");
        m.checkNotNullParameter(list, "arguments");
        m.checkNotNullParameter(iVar, "memberScope");
        k0 k0Var = new k0(u0Var, list, z2, iVar, new d(u0Var, list, gVar, z2, iVar));
        return gVar.isEmpty() ? k0Var : new h(k0Var, gVar);
    }

    public static final j0 simpleTypeWithNonTrivialMemberScope(d0.e0.p.d.m0.c.g1.g gVar, u0 u0Var, List<? extends w0> list, boolean z2, i iVar, Function1<? super g, ? extends j0> function1) {
        m.checkNotNullParameter(gVar, "annotations");
        m.checkNotNullParameter(u0Var, "constructor");
        m.checkNotNullParameter(list, "arguments");
        m.checkNotNullParameter(iVar, "memberScope");
        m.checkNotNullParameter(function1, "refinedTypeFactory");
        k0 k0Var = new k0(u0Var, list, z2, iVar, function1);
        return gVar.isEmpty() ? k0Var : new h(k0Var, gVar);
    }
}
