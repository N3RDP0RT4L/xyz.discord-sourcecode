package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.n.l1.i;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
/* compiled from: SpecialTypes.kt */
/* loaded from: classes3.dex */
public final class m0 {
    public static final j0 a(c0 c0Var) {
        a0 a0Var;
        u0 constructor = c0Var.getConstructor();
        a0 a0Var2 = constructor instanceof a0 ? (a0) constructor : null;
        if (a0Var2 == null) {
            return null;
        }
        Collection<c0> supertypes = a0Var2.getSupertypes();
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(supertypes, 10));
        boolean z2 = false;
        for (c0 c0Var2 : supertypes) {
            if (e1.isNullableType(c0Var2)) {
                c0Var2 = makeDefinitelyNotNullOrNotNull$default(c0Var2.unwrap(), false, 1, null);
                z2 = true;
            }
            arrayList.add(c0Var2);
        }
        if (!z2) {
            a0Var = null;
        } else {
            c0 alternativeType = a0Var2.getAlternativeType();
            if (alternativeType == null) {
                alternativeType = null;
            } else if (e1.isNullableType(alternativeType)) {
                alternativeType = makeDefinitelyNotNullOrNotNull$default(alternativeType.unwrap(), false, 1, null);
            }
            a0Var = new a0(arrayList).setAlternative(alternativeType);
        }
        if (a0Var == null) {
            return null;
        }
        return a0Var.createType();
    }

    public static final a getAbbreviatedType(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        i1 unwrap = c0Var.unwrap();
        if (unwrap instanceof a) {
            return (a) unwrap;
        }
        return null;
    }

    public static final j0 getAbbreviation(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        a abbreviatedType = getAbbreviatedType(c0Var);
        if (abbreviatedType == null) {
            return null;
        }
        return abbreviatedType.getAbbreviation();
    }

    public static final boolean isDefinitelyNotNullType(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        return c0Var.unwrap() instanceof k;
    }

    public static final i1 makeDefinitelyNotNullOrNotNull(i1 i1Var, boolean z2) {
        m.checkNotNullParameter(i1Var, "<this>");
        k makeDefinitelyNotNull$descriptors = k.k.makeDefinitelyNotNull$descriptors(i1Var, z2);
        if (makeDefinitelyNotNull$descriptors != null) {
            return makeDefinitelyNotNull$descriptors;
        }
        j0 a = a(i1Var);
        return a == null ? i1Var.makeNullableAsSpecified(false) : a;
    }

    public static /* synthetic */ i1 makeDefinitelyNotNullOrNotNull$default(i1 i1Var, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = false;
        }
        return makeDefinitelyNotNullOrNotNull(i1Var, z2);
    }

    public static final j0 makeSimpleTypeDefinitelyNotNullOrNotNull(j0 j0Var, boolean z2) {
        m.checkNotNullParameter(j0Var, "<this>");
        k makeDefinitelyNotNull$descriptors = k.k.makeDefinitelyNotNull$descriptors(j0Var, z2);
        if (makeDefinitelyNotNull$descriptors != null) {
            return makeDefinitelyNotNull$descriptors;
        }
        j0 a = a(j0Var);
        return a == null ? j0Var.makeNullableAsSpecified(false) : a;
    }

    public static /* synthetic */ j0 makeSimpleTypeDefinitelyNotNullOrNotNull$default(j0 j0Var, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = false;
        }
        return makeSimpleTypeDefinitelyNotNullOrNotNull(j0Var, z2);
    }

    public static final j0 withAbbreviation(j0 j0Var, j0 j0Var2) {
        m.checkNotNullParameter(j0Var, "<this>");
        m.checkNotNullParameter(j0Var2, "abbreviatedType");
        return e0.isError(j0Var) ? j0Var : new a(j0Var, j0Var2);
    }

    public static final i withNotNullProjection(i iVar) {
        m.checkNotNullParameter(iVar, "<this>");
        return new i(iVar.getCaptureStatus(), iVar.getConstructor(), iVar.getLowerType(), iVar.getAnnotations(), iVar.isMarkedNullable(), true);
    }
}
